/*
 *
 * [ File Version : 1.9 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>
#include <stdio.h>
#ifdef COPY_ENCRYPTED_TEXT_AS_DECRYPTED
#include <string>  // for string
#include <sstream> // for istringstream
#endif

#include "Map.h"
#include "Strings.h"
#include "Protect.h"
#include "VerificStream.h" // VIPER #6665

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

Protect::~Protect()
{
    ResetProtectData() ;
    _directive_map = 0 ;
}

unsigned Protect::encrypt(const char *in_buf, char *out_buf, unsigned size)
{
    if(!in_buf || !out_buf) return 0 ;
    memcpy(out_buf, in_buf, size) ;
    return size ;
}

unsigned Protect::decrypt(const char *in_buf, char *out_buf, unsigned size)
{
    if(!in_buf || !out_buf) return 0 ;
#ifdef COPY_ENCRYPTED_TEXT_AS_DECRYPTED
    // Copy the encrypted text as if it has been decrypted (for testing?):
    memcpy(out_buf, in_buf, size) ;
    return size ;
#else
    // Cannot decrypt, so, return empty string and size as 0:
    memset(out_buf, 0, size) ; // set all bytes to '\0'
    size = 0 ;

    // Check if plug in set for the `pragma protect
    if (!_directive_map) {
        (void) Protect::SetDirective("data_block", in_buf) ;
        char *decrypted_str = decrypt() ;
        if (decrypted_str) {
            size = Strings::len(decrypted_str) ;
            memcpy(out_buf, decrypted_str, size) ;
        }
        Strings::free(decrypted_str) ;
        ResetProtectData() ;
    }
    return size ;
#endif
}

//CARBON_BEGIN
unsigned Protect::decrypt(const char *in_buf, char *out_buf, unsigned size, linefile_type /*unused lf*/ )
{
    return decrypt(in_buf, out_buf, size);
}
//CARBON_END

char *Protect::decrypt()
{
    if (!_directive_map) return 0 ;
#ifdef COPY_ENCRYPTED_TEXT_AS_DECRYPTED
    // Copy the encrypted text as if it has been decrypted (for testing?):
    return Strings::save((char *)_directive_map->GetValue("data_block")) ;
#else
    // Cannot decrypt, so, return 0:
    return 0 ;
#endif
}

// VIPER #6665: Block-by-block decryption of encrypted text using streams
verific_stream *
Protect::get_decryption_stream_for_string(const char *in_buf, unsigned /*size*/)
{
    if (!in_buf) return 0 ;

#ifdef COPY_ENCRYPTED_TEXT_AS_DECRYPTED
    // Create the stream with the encrypted text as if it has been decrypted (for testing?):
    return (new verific_istream(new std::istringstream(std::string(in_buf)))) ; // Two un-named objects!
#else
    // Cannot decrypt, so, return 0:
    return 0 ;
#endif
}

// VIPER #6665: Block-by-block decryption of encrypted text using streams
verific_stream *
Protect::get_decryption_stream()
{
    if (!_directive_map) return 0 ;

#ifdef COPY_ENCRYPTED_TEXT_AS_DECRYPTED
    // Create the stream with the encrypted text as if it has been decrypted (for testing?):
    const char *encrypted_str = (const char *)_directive_map->GetValue("data_block") ;
    if (!encrypted_str) return 0 ; // Cannot decrypt without the encrypted string!
    return (new verific_istream(new std::istringstream(std::string(encrypted_str)))) ; // Two un-named objects!
#else
    // Cannot decrypt, so, return 0:
    return 0 ;
#endif
}

unsigned Protect::SetDirective(const char *directive, const char *value)
{
    if (!directive || !value) return 0 ;
    if (!_directive_map) _directive_map = new Map(STRING_HASH_CASE_INSENSITIVE) ;

    return _directive_map->Insert(Strings::save(directive), Strings::save(value), 0, 1) ; // force insert
}

const char *Protect::GetDirectiveValue(const char *directive)
{
    return (directive && _directive_map) ? (const char *)_directive_map->GetValue(directive) : 0 ;
}

void Protect::ResetProtectData()
{
    MapIter mi ;
    char *directive ;
    char *value ;
    FOREACH_MAP_ITEM(_directive_map, mi, &directive, &value) {
        Strings::free(directive) ;
        Strings::free(value) ;
    }
    delete _directive_map ;
    _directive_map = 0 ;
}

