/*
 *
 * [ File Version : 1.84 - 2014/02/05 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_LIBRARY_H_
#define _VERIFIC_VERI_LIBRARY_H_

#include <iosfwd> // iostream inclusion needed for PrettyPrint
using std::ostream ;

#include "VeriTreeNode.h" // For VeriLibraryDecl

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Map ;
class Array ;
class Set ;

class VeriModule ;
class VeriScope ;
class SaveRestore ;
// macros to iterate over all modules in a library

// To iterate over all modules in a library :
// use FOREACH_VERILOG_MODULE_IN_LIBRARY :
//       argument       type            function
//        MI        :   MapIter         An iterator
//        MOD       :   VeriModule *    Returned Pointer to the module
//                                      parse tree (include VeriModule.h
//                                      before using member functions)
//        LIB       :   VeriLibrary *   The library where the modules are
//                                      to be looked up
#define FOREACH_VERILOG_MODULE_IN_LIBRARY(LIB,MI,MOD) if (LIB) FOREACH_MAP_ITEM((LIB)->GetModuleTable(), MI, 0, &(MOD))

/* -------------------------------------------------------------- */

/*
    This class represents a "library". Libraries are associated with
    a name, and this is stored in _library_name, the default value
    of which is "work". This class also has a table of modules and
    configurations.
*/
class VFC_DLL_PORT VeriLibrary
{
public:
    explicit VeriLibrary(const char *name) ;

    // Pretty print tree node
    virtual void            PrettyPrint(ostream& os, unsigned level) const ;

    // Interface routine to VeriLibrary
    Map *                   GetModuleTable() const                  { return _module_table ; }  // Get the module table (char*name -> VeriModule* table).
    const char *            GetName() const                         { return _name ; }          // The name of the library

    // APIs for addition/removal of modules in the library
    virtual unsigned        AddModule(VeriModule *module, unsigned refuse_on_error = 1) ; // add module to the library, but refuse unit if there was an error and 'refuse_on_error' is set.
    void                    RemoveSimpleModules() ; // remove and delete all modules without parameters
    void                    RemoveAllModules() ; // remove all modules from this library
    void                    RemoveUncompiledModules() ; // remove modules that are not compiled (elaborated)
    unsigned                RemoveModule(const char *name) ; // Old routine : remove and delete a module in this lib. New is to just call 'delete' on the module (which will self-detach).
    unsigned                DetachModule(VeriModule *module) ; // remove module from library. Call 'delete' on the module if you want it deleted. 'delete' will remove itself from the library it is in.

    // API to find a module in this library
    VeriModule *            GetModule(const char *module_name, unsigned case_sensitive, unsigned restore = 1) ; // Get a module with name "module_name" in this library
    VeriModule *            GetModuleAnywhere(const char *module_name, unsigned case_sensitive, unsigned restore = 1) ; // brute force way to find a module. Not LRM compliant.

    virtual Array *         GetLibExt() const                       { return 0 ; } // Array of char* of registered lib extensions

    // Virtual methods to check if this library is a user library/tmp library
    virtual unsigned        IsUserLibrary() const                   { return 0 ; }
    virtual unsigned        IsTmpLibrary() const                    { return 0 ; }
    virtual unsigned        IsTickUseLib() const                    { return 0 ; }
    virtual unsigned        IsTickUseLibLib() const                 { return 0 ; }
    virtual unsigned        IsEmptyUseLib() const                   { return 0 ; }

    // The main API to elaborate a library.
    unsigned                StaticElaborate(Array *created_top_modules = 0) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    explicit VeriLibrary(SaveRestore &save_restore) ;

protected:
    VeriLibrary() ; // Default constructor can only be called from derived classes

public:
    virtual ~VeriLibrary() ;

private:
    // Prevent compiler from defining the following
    VeriLibrary(const VeriLibrary &) ;            // Purposely leave unimplemented
    VeriLibrary& operator=(const VeriLibrary &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Persistence (Binary restore via SaveRestore class)
    virtual void            Save(SaveRestore &save_restore) const ;
    static void             SaveLibPtr(SaveRestore &save_restore, const VeriLibrary *lib) ;
    static VeriLibrary *    RestoreLibPtr(SaveRestore &save_restore) ;

    // API s for addition of -y/-v options
    virtual void            AddDirOption(const char * /*dir*/)      { } // Register a -y library name (a directory)
    virtual void            AddFileOption(const char * /*file*/)    { } // Register a -v library name (a file)
    virtual void            AddLibOption(const char * /*lib*/)      { } // Register a `uselib lib=<lib> name (a logical library name)
    virtual void            AddLibExt(const char * /*libext*/)      { } // Register a file-name extension for -y
    virtual void            SetLibReScan(unsigned /*lib_rescan*/)   { } // Set -librescan option to user library (VIPER #2132)

    // Other operations on VeriLibrary
    virtual void            Process() ;
    virtual unsigned        ParseFile(const char *filename) ; // Parse file "filename"

    virtual void            AddModuleName(const char * /*module_name*/)     { }
    virtual void            RemoveModuleName(const char * /*module_name*/)  { }
    virtual void            RemoveAllModuleNames()                          { }
    virtual unsigned        IsUnresolvedModule(const char * /*module_name*/) const { return 1 ; /* by default all modules are required */ } // VIPER #8405
    virtual VeriLibrary *   GetWorkingLibrary() const                       { return 0 ; }

    // Internal routine for Verific's use only (VIPER #4933):
    virtual void            AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) const ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #7105: Get the VHDL unit from `uselib lib directive:
    virtual VhdlPrimaryUnit *GetVhdlUnitFromTickUseLibLib(const char * /*unit_name*/, char ** /*arch_name*/) const           { return 0 ; }
#endif

protected:
    // Internal routine :
    virtual VeriModule *    GetModuleInternal(const char *module_name, unsigned case_sensitive, unsigned restore) ; // Return argument specified module from this library
protected:
    char        *_name ; // name of the library
    //  This is a map of char * ( name ) against VeriModule *.
    //  This is the table of named modules inside the library. The
    //  module pointers are stored against their names.
    Map         *_module_table ;

    // VIPER #5035 : Maintain a set to store the name of modules
    // not defined in this library, but is searched here using
    // GetMiodule
    Set         *_bb_list ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriLibrary

/* -------------------------------------------------------------- */

class VeriFileDirLibOption ;
class VeriTmpLibrary ;

class VFC_DLL_PORT VeriUserLibrary : public VeriLibrary
{
public:
    explicit VeriUserLibrary(unsigned from_tick_uselib = 0) ;
    explicit VeriUserLibrary(const char *name, unsigned from_tick_uselib = 0) ;
    // Persistence (Binary restore via SaveRestore class)
    explicit VeriUserLibrary(SaveRestore &save_restore) ;

    virtual ~VeriUserLibrary() ;

private:
    // Prevent compiler from defining the following
    VeriUserLibrary(const VeriUserLibrary &) ;            // Purposely leave unimplemented
    VeriUserLibrary& operator=(const VeriUserLibrary &) ; // Purposely leave unimplemented

public:
    // Access Functions
    virtual void            AddLibExt(const char *libext) ;
    virtual Array *         GetLibExt() const ;
    virtual void            SetLibReScan(unsigned lib_rescan_active) ; // Set -librescan option to user library (VIPER #2132)

    // If uselib-lib, following API will update a local <mod-name, lib-name> cache,
    // and not own this module. Original user lib will still own the module.
    // If not uselib-lib, base class AddModule will be called.
    virtual unsigned        AddModule(VeriModule *module, unsigned refuse_on_error = 1) ; // add module to the library, but refuse unit if there was an error and 'refuse_on_error' is set.

    // API s for adding -y/-v options
    void                    InsertFileDirLibOption(const VeriFileDirLibOption *option ) ;
    virtual void            AddDirOption(const char *dir ) ;
    virtual void            AddFileOption(const char *file );
    virtual void            AddLibOption(const char *lib );

    // Other operations on library
    virtual void            Process() ;
    virtual void            AddModuleName(const char *module_name) ;
    virtual void            RemoveModuleName(const char *module_name) ;
    virtual void            RemoveAllModuleNames() ;
    virtual unsigned        IsUnresolvedModule(const char *module_name) const ; // VIPER #8405

    virtual unsigned        IsUserLibrary() const       { return 1 ; } // This is a user library
    virtual unsigned        IsTickUseLib() const        { return _tick_uselib ; } // Is this library from `uselib?
    virtual unsigned        IsTickUseLibLib() const     { return _tick_uselib_lib ; } // `uselib lib support
    virtual unsigned        IsEmptyUseLib() const ;

    virtual void            Save(SaveRestore &save_restore) const ;

    // Internal routine for Verific's use only (VIPER #4933):
    virtual void            AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) const ;

private:
    // Private functions for internal use
    unsigned                ProcessFileOption(const VeriFileDirLibOption *file_dir, VeriLibrary * work_library) ; // Process file option to find unresolved modules
    unsigned                ProcessDirOption(const VeriFileDirLibOption *file_dir, VeriLibrary *work_library) ;   // Process dir option to find unresolved modules
    unsigned                ProcessLibOption(const VeriFileDirLibOption *file_dir, VeriLibrary *work_library) ;   // Process lib option to find unresolved modules
    unsigned                PickRequiredModules(VeriLibrary *lib, VeriLibrary *work_library) ;   // Select required modules from 'lib' where lib option logical library resides.
    unsigned                PickRequiredModules(VeriTmpLibrary *tmp_lib, VeriLibrary * work_library, unsigned starting_pos, unsigned v_file_option) ;   // Select required modules from 'tmp_lib' where file/dir option specific library file(s) are analyzed.
    unsigned                HasUnresolvedModules() const ; // Check if we have some unresolved modules to bind
    void                    RemoveNewModuleNames() ; // VIPER 2132: Remove module names found while processing -y/-v options
    void                    RemoveDirFileStatus() ; // VIPER #7404: Remove the cached directory content/file status

    unsigned                PickRequiredModulesInternal(const char *module_name, Set *dependent_scopes, const VeriTmpLibrary *tmp_lib, VeriLibrary *work_library, unsigned starting_position, unsigned v_file_option) ; // VIPER #8343
    unsigned                ResolveModuleNamesFromLibrary(VeriLibrary *work_lib, unsigned ignore_tick_uselib) ; // VIPER #8343: Quickly resolve unresolved modules form the specified work library

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
public:
    // VIPER #7105: Get the VHDL unit from `uselib lib directive:
    virtual VhdlPrimaryUnit *GetVhdlUnitFromTickUseLibLib(const char *unit_name, char **arch_name) const ;
#endif

protected:
    virtual VeriModule *    GetModuleInternal(const char *module_name, unsigned case_sensitive, unsigned restore) ; // Return argument specified module from this library

protected:
    Array    *_all_files_dirs ;         // Array of classes (VeriFileDirLibOption) to store -v/-y options
    Array    *_libext ;                 // Library extension
    Map      *_module_names_to_lookup ; // char *instantiated_module_name->integer flag to indicate resolved (0)/unresolved (1) module association. Helps to avoid holes and maintain order
    unsigned  _num_modules_to_lookup ;  // VIPER #7740: Number of modules to look up in this user-library.
    Set      *_module_names_added ;     // VIPER 2132 : Set of unresolved module names found while -y/-v processing
    unsigned  _lib_rescan_active:1 ;    // Flag to indicate if +librescan is active
    unsigned  _tick_uselib:1 ;          // This library is created from `uselib specification in source text
    unsigned  _tick_uselib_lib:1 ;      // It is `uselib lib : it is error to mix with other Verilog XL options of `uselib namely file or dir or libext. This flag is kept to save the iterations during check
    unsigned  _is_processing:1 ;        // VIPER #5424: Flag to indicate whether we are processing this user-library.
    Map      *_uselib_lib_mod_lib_map ; // Modules pulled in from uselib-lib logical library. This user-lib will not own those modules, this module_name-lib_name map will rescue us finding such modules in future
    Map      *_dir_file_status ;        // Map of char * directory vs. Map of char * file name vs its 'stat' status.
#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    Map      *_modules_in_file ;        // Map from char* filename -> Set of modules defined in the file
#endif
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriUserLibrary

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriTmpLibrary : public VeriLibrary
{
public:
    explicit VeriTmpLibrary(VeriLibrary *l) ;
    virtual ~VeriTmpLibrary() ;

public:
    virtual unsigned        IsTmpLibrary() const        { return 1 ; } // This is a tmp library
    virtual VeriLibrary *   GetWorkingLibrary() const   { return _work_lib ; }
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected:
    VeriLibrary *_work_lib ; // Back pointer to working design library, for which user libraries are processed (VIPER #4957)

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTmpLibrary

/* -------------------------------------------------------------- */

////////////////////////////////////////////////////////////////////
//            Classes for -y/-v specific Library
////////////////////////////////////////////////////////////////////

/*
   This class is the base class to represent each -v/file and -y/dir option.
*/
class VFC_DLL_PORT VeriFileDirLibOption
{
public:
    explicit VeriFileDirLibOption(const char *name) ;
    // Persistence (Binary restore via SaveRestore class)
    explicit VeriFileDirLibOption(SaveRestore &save_restore) ;
    virtual ~VeriFileDirLibOption() ;

private:
    // Prevent compiler from defining the following
    VeriFileDirLibOption() ;                                     // Purposely leave unimplemented
    VeriFileDirLibOption(const VeriFileDirLibOption &) ;            // Purposely leave unimplemented
    VeriFileDirLibOption& operator=(const VeriFileDirLibOption &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    virtual unsigned    IsFileOption() const    { return 0 ; }
    virtual unsigned    IsDirOption()  const    { return 0 ; }
    virtual unsigned    IsLibOption() const     { return 0 ; }

    char *              GetName() const         { return _file_dir_name ; }

    virtual void        Save(SaveRestore &save_restore) const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
private:
    char *_file_dir_name ; // Name of file or directory
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriFileDirLibOption

/* -------------------------------------------------------------- */

/*
   Class to represent each -v option of command line and file option of `uselib
*/
class VFC_DLL_PORT VeriFileOption : public VeriFileDirLibOption
{
public:
    explicit VeriFileOption(const char *name):VeriFileDirLibOption(name) { } ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriFileOption(SaveRestore &save_restore) ;

    virtual unsigned    IsFileOption() const    { return 1 ; }
    virtual void        Save(SaveRestore &save_restore) const ;
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriFileOption

/* -------------------------------------------------------------- */

/*
   Class to represent each -y option of command line and dir option of `uselib.
*/
class VFC_DLL_PORT VeriDirOption : public VeriFileDirLibOption
{
public:
    explicit VeriDirOption(const char *name):VeriFileDirLibOption(name) { } ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriDirOption(SaveRestore &save_restore) ;

    virtual unsigned    IsDirOption() const     { return 1 ; }

    virtual void        Save(SaveRestore &save_restore) const ;
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDirOption
/* -------------------------------------------------------------- */

/*
   Class to represent each lib option of `uselib.
*/
class VFC_DLL_PORT VeriLibOption : public VeriFileDirLibOption
{
public:
    explicit VeriLibOption(const char *name):VeriFileDirLibOption(name) { } ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriLibOption(SaveRestore &save_restore) ;

    virtual unsigned    IsLibOption() const     { return 1 ; }

    virtual void        Save(SaveRestore &save_restore) const ;
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriLibOption

/* -------------------------------------------------------------- */

/*
   VIPER #2726: Class to represent Verilog 2000 library declaration for library mapping
   An object of this class is created whenever a "library" construct is encountered.
   It stores the pattern and the name of the library. When we analyzing a file, we
   match all the patterns of all the library declarations. If a definite match is
   found, the modules from the file is stored into that library.
*/
class VFC_DLL_PORT VeriLibraryDecl : public VeriTreeNode
{
public:
    VeriLibraryDecl(char *name, char *pattern, linefile_type linefile = 0) ;   // single pattern
    VeriLibraryDecl(char *name, Array *patterns, linefile_type linefile = 0) ; // multiple patterns

    VeriLibraryDecl(const VeriLibraryDecl &tree_node, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriLibraryDecl(SaveRestore &save_restore) ;
    virtual ~VeriLibraryDecl() ;

private:
    // Prevent compiler from defining the following
    VeriLibraryDecl() ;                                   // Purposely leave unimplemented
    VeriLibraryDecl(const VeriLibraryDecl &) ;            // Purposely leave unimplemented;
    VeriLibraryDecl &operator=(const VeriLibraryDecl &) ; // Purposely leave unimplemented;

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const                      { return ID_VERILIBRARYDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Local accessor functions:
    const char *                GetName() const                         { return _name ; }
    Array *                     GetPatterns() const                     { return _patterns ; }

    // Matches the file name with its patterns. Returns 1 if matches, 0 otherwise.
    unsigned                    MatchPatterns(const char *file_name) const ;

private:
    // Private functions for internal use:

    // Main function to format a file path or pattern. What it does is:
    //   1. Makes the path absolute.
    //   2. Compacts the wild cards but keeps the meaning unchanged.
    //
    // If the flag 'make_absolute_using_cwd' is set we make it absolute from current
    // working directory, otherwise we use the path of the file in which this library
    // was declared (line-file information of that file helps to get the path from
    // current working directory).
    //
    // It returns a newly allocated string and the caller of this function is responsible
    // to free that string using Strings::free():
    char *              GetFormattedPath(const char *path, unsigned make_absolute_using_cwd) const ;

    // Replaces consecutive repeated sequence of *'s with a single occurrence.
    // Also replaces "?*?" with "??" by skipping the '*' character in-between
    // and "*?" with "?*". Returns 1 if replaced, 0 otherwise:
    unsigned            CompactWildCards(char *path) const ;  // Modifies "path" in-place

    // Matches the a pattern with a file_name using wild card pattern matching rule.
    // Returns the precedence of the matching or 0 if not matched. The precedence rules are
    // defined in IEEE 1364 LRM section 13.2.1.1 "File path resolution":
    // If a file name potentially matches multiple file path specifications, the path specifications
    // shall be resolved in the following order:
    //   (a) File path specifications which end with an explicit filename  = 3 (highest precedence)
    //   (b) File path specifications which end with a wildcarded filename = 2
    //   (c) File path specifications which end with a directory           = 1 (lowest precedence)
    // If a file name matches path specifications in multiple library definitions (after the above
    // resolution rules have been applied), it shall be an error.
    unsigned            WildMatch(const char *pattern, const char *file_name) const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected:
    char  *_name ;      // Name of the library
    Array *_patterns ;  // Array of char * pattern of file paths
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriLibraryDecl

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_LIBRARY_H_
