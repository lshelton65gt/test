/*
 *
 * [ File Version : 1.248 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * val Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>   // for strstr
#include <stdio.h>    // for rename
#include <sstream>    // for ostringstream
#include <fstream>    // for ostream and ifstream

using namespace std ;

#include "Message.h"
#include "Map.h"
#include "Strings.h"
#include "FileSystem.h"

#include "veri_file.h"
#include "VeriModule.h"
#include "VeriStatement.h"
#include "VeriModuleItem.h"
#include "VeriMisc.h"
#include "VeriLibrary.h"
#include "VeriId.h"
#include "VeriExpression.h"
#include "VeriConstVal.h"
#include "VeriScope.h"

#include "veri_tokens.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// VIPER #6878: Flag to control pretty print of attributes and pragmas (default is print as what was specified in RTL):
/* static */ unsigned VeriNode::_attr_print_mode = (unsigned)veri_file::PRINT_AS_ORIGINAL ;

// Issue 2135
// Define the maximum number of items of a comma-separated list, printed on a single line,
#define MAX_COMMA_LIST_COUNT 15

// Define the bellow macro to print some info of module binding.
// This is only useful after static elaboration.
//#define PRETTY_PRINT_MODULE_INSTANCE_BINDING_INFO

// Viper #8188 / 8208: used linefile based IsTickProtectedNode
#define RETURN_IF_TICK_PROTECTED \
    if (IsTickProtectedNode()) {           \
        f << "// protected rtl " << endl ; \
        return ; \
    }

//--------------------------------------------------------------
// #included from veri_file.h
//--------------------------------------------------------------

unsigned
veri_file::PrettyPrint(const char *file_name, const char *module_name, const char *lib_name, attr_pretty_print_mode attr_mode)
{
    if (!file_name) return 0 ;

    // Divert from standard library name default :
    // If 0, we will print all libraries into one file...
    // if (!lib_name) lib_name = "work" ;

    Map *all_libs = 0 ;
    VeriLibrary *lib = 0 ;
    if (!lib_name && !module_name) {
        all_libs = AllLibraries() ;
    } else if (module_name && !lib_name) {
        lib_name = "work" ;
    }

    // Viper: 3142 - all verific msg should have msg-id
    VeriTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    if (lib_name) {
        // Pick up the library :
        lib = GetLibrary(lib_name) ;
        if (!lib) {
            tmp_print_node.Warning("library '%s' is empty", lib_name) ;
            // Message::Warning(0,"library '", lib_name, "' is empty") ;
            return 0 ;
        }
    }

    VeriModule *module = 0 ;
    if (module_name) {
        // Find the module, by name
        module = (lib) ? lib->GetModule(module_name, 1) : 0 ;
        if (!module) {
            // Message::Error(0,"module '",module_name,"' not found in library ", lib_name) ;
            tmp_print_node.Error("The module %s was not found in library %s", module_name, lib_name) ;
            return 0 ;
        }
    }

    // Open the file for writing
    ofstream f(file_name,ios::out) ;
    if (!f.rdbuf()->is_open()) {
        tmp_print_node.Error("cannot open file %s", file_name) ;
        // Message::Error(0,"cannot open file ",file_name) ;
        return 0 ;
    }

    // VIPER #4581: Save the current default nettype setting:
    // VIPER #4581 (#7128): This is not required anymore, we directly print it now.
    //unsigned default_nettype = GetDefaultNettype() ;
    //SetDefaultNettype(VERI_WIRE) ; // Set it to the default

    // VIPER #6878: Set the attribute/pragma pretty print mode:
    VeriNode::_attr_print_mode = (unsigned)attr_mode ;

    if (module) {
        // Pretty-print the unit into this file
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Pretty printing module ", module->Name(), " to file ",file_name) ;
        tmp_print_node.Comment("Pretty printing module %s to file %s", module->Name(), file_name) ;

        module->PrettyPrint(f,0) ;
    } else if (lib && lib_name) {
        // Print all modules in this library
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Pretty printing all modules in library ", lib_name, " to file ",file_name) ;
        tmp_print_node.Comment("Pretty printing all modules in library %s to file %s", lib_name, file_name) ;

        lib->PrettyPrint(f,0) ;
    } else {
        // Print all modules in all libraries
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Pretty printing all modules in all libraries to file ",file_name) ;
        tmp_print_node.Comment("Pretty printing all modules in all libraries to file %s", file_name) ;

        MapIter mi ;
        FOREACH_MAP_ITEM(all_libs, mi, 0, &lib) {
            // Do not print 'std' library
            if (!lib || Strings::compare(lib->GetName(), "std")) continue ;
            lib->PrettyPrint(f, 0) ;
        }
    }

    // VIPER #4581: Restore the saved default nettype setting:
    // VIPER #4581 (#7128): This is not required anymore, we directly print it now.
    //SetDefaultNettype(default_nettype) ;

    return 1 ;
}

/* static */ char *
veri_file::GetOutputFileNameForPrintDesign(const char *design_file, const char *suffix, const char *output_directory)
{
    if (!design_file) return 0 ;
    if (!output_directory && !suffix) return 0 ;

    VeriTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // If output_directory is specified, construct new output file path
    char *output_file_name = 0 ;
    if (output_directory) {
        char *cwd  = FileSystem::Convert2AbsolutePath(".", 1 /*compact*/) ;
        char *file = FileSystem::Convert2AbsolutePath(design_file, 1 /*compact*/) ;
        if (!file || !cwd) {
            Strings::free(file) ; // JJ 130830 memleak fix
            Strings::free(cwd) ;
            return 0 ; // Something wrong happened
        }
#ifdef WINDOWS
        // Windows is case-insensitive, so lower case paths for strstr comparison only
        (void) Strings::strtolower(cwd) ;  // lower-casing string in place
        (void) Strings::strtolower(file) ; // lower-casing string in place
#endif
        // Determine if 'design_file' resides underneath CWD directory tree.
        if (file == strstr(file, cwd)) {
            // File resides under CWD
            output_file_name = Strings::save(output_directory, "/", file + Strings::len(cwd) + 1) ;
        } else {
            // This file resides external to CWD directory tree.  Because of this, we need
            // to create a new path.  Lop off network mount specifier and add "EXTERNAL".
#ifdef WINDOWS
            if (file[0] == '\\' && file[1] == '\\') {
                output_file_name = Strings::save(output_directory, "/EXTERNAL/", file + 2) ;
            } else if (file[1] == ':') {
                output_file_name = Strings::save(output_directory, "/EXTERNAL/", file + 3) ;
            } else {
                VERIFIC_ASSERT(0) ;
            }
#else
            output_file_name = Strings::save(output_directory, "/EXTERNAL/", file + 1) ;
#endif
        }

        Strings::free(cwd) ;
        Strings::free(file) ;

        // Now create the directory path
        char *dir = FileSystem::DirectoryPath(output_file_name) ;
        if (!FileSystem::Mkdir(dir)) {
            tmp_print_node.Error("cannot create %s due to mkdir failure", output_file_name) ;
            Strings::free(output_file_name) ;
            Strings::free(dir) ;
            return 0 ;
        }
        Strings::free(dir) ;
    }

    // If suffix is specified, append it
    if (suffix) {
        char *tmp = output_file_name ;
        output_file_name = Strings::save(((tmp) ? tmp : design_file), suffix) ;
        Strings::free(tmp) ;
    }

    return output_file_name ;
}

unsigned
veri_file::PrettyPrintDesign(const char *suffix, const char *output_directory, const char *module_name, const char *lib_name)
{
    if (!suffix && !output_directory) return 0 ;

    Map *all_libs = 0 ;
    VeriLibrary *lib = 0 ;
    if (!lib_name && !module_name) {
        all_libs = AllLibraries() ;
    } else if (module_name && !lib_name) {
        lib_name = "work" ;
    }

    VeriTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    if (lib_name) {
        // Pick up the library :
        lib = GetLibrary(lib_name) ;
        if (!lib) {
            tmp_print_node.Warning("library '%s' is empty", lib_name) ;
            return 0 ;
        }

        if (!module_name) {
            all_libs = new Map(STRING_HASH) ;
            (void) all_libs->Insert(lib_name, lib) ;
        }
    }

    VeriModule *module = 0 ;
    if (module_name) {
        // Find the module, by name
        module = (lib) ? lib->GetModule(module_name, 1) : 0 ;
        if (!module) {
            tmp_print_node.Error("The module %s was not found in library %s", module_name, lib_name) ;
            return 0 ;
        }
    }

    // VIPER #4581: Save the current default nettype setting:
    // VIPER #4581 (#7128): This is not required anymore, we directly print it now.
    //unsigned default_nettype = GetDefaultNettype() ;
    //SetDefaultNettype(VERI_WIRE) ; // Set it to the default

    if (module) {
        const char *original_file = LineFile::GetAbsFileName(module->Linefile()) ;
        char *file_name = GetOutputFileNameForPrintDesign(original_file, suffix, output_directory) ;
        ofstream f(file_name, ios::out) ;
        if (!f.rdbuf()->is_open()) {
            tmp_print_node.Error("cannot open file %s", file_name) ;
            Strings::free(file_name) ;
            return 0 ;
        }
        tmp_print_node.Comment("Pretty printing module %s to file %s", module->Name(), file_name) ;
        Strings::free(file_name) ;

        module->PrettyPrint(f,0) ;
        f.close() ;
    } else {
        Map file_ordered_modules(NUM_HASH) ;
        MapIter mi ;
        FOREACH_MAP_ITEM(all_libs, mi, 0, &lib) {
            // Do not print 'std' library
            if (!lib || Strings::compare(lib->GetName(), "std")) continue ;

            MapIter mii ;
            FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, mii, module) {
                if (!module) continue ;
                linefile_type lf = module->Linefile() ;
                // VIPER #7167: Hash with file id instead of line-file info:
                Array *module_list = (Array *)file_ordered_modules.GetValue((void *)(unsigned long)LineFile::GetFileId(lf)) ;
                if (!module_list) {
                    module_list = new Array() ;
                    (void) file_ordered_modules.Insert((void *)(unsigned long)LineFile::GetFileId(lf), module_list) ;
                }
                module_list->Insert(module) ;
            }
        }

        // VIPER #7167: Print all modules from the same file to a single file:
        Array *module_list = 0 ;
        unsigned long file_id = 0 ;
        FOREACH_MAP_ITEM(&file_ordered_modules, mi, &file_id, &module_list) {
            //if (!file_id || !module_list) continue ;

            const char *original_file = LineFile::GetAbsFileNameFromId((unsigned)file_id);
            char *file_name = GetOutputFileNameForPrintDesign(original_file, suffix, output_directory) ;
            ofstream f(file_name, ios::out) ;
            if (!f.rdbuf()->is_open()) {
                tmp_print_node.Error("cannot open file %s", file_name) ;
                Strings::free(file_name) ;
                delete module_list ;
                continue ;
            }

            unsigned idx ;
            FOREACH_ARRAY_ITEM(module_list, idx, module) {
                if (!module) continue ;
                tmp_print_node.Comment("Pretty printing module %s to file %s", module->Name(), file_name) ;
                module->PrettyPrint(f,0) ;
            }
            Strings::free(file_name) ;
            f.close() ;
            delete module_list ;
        }

        if (lib_name) delete all_libs ;
    }

    // VIPER #4581: Restore the saved default nettype setting:
    // VIPER #4581 (#7128): This is not required anymore, we directly print it now.
    //SetDefaultNettype(default_nettype) ;

    return 1 ;
}

//--------------------------------------------------------------
// #included from VeriTreeNode.h
//--------------------------------------------------------------

void
VeriNode::PrintIdentifier(ostream &f, const char *str)
{
    if (!str || !*str) return ;
    // Get correctly formatted output name
    char *tmp = MakeVerilogName(str) ; // A string is allocated within here
    if (tmp) f << tmp ;
    Strings::free(tmp) ;
}

void
VeriNode::PrettyPrintCommaList(const Array *list, ostream &f, unsigned level)
{
    // Pretty-print a comma-separated list without new lines. Mostly for declarations (ports, parameters, identifiers etc).
    VeriTreeNode *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(list, i, item) {
        if (i) f << ", " ; // put extra space behind comma.

        // Issue 2135 : cap the number of items printed on a single line
        if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

        // This is a comma-separated list, so call each item WOSemi (otherwise a semi might be written at the end).
        if (!item) continue ;
        item->PrettyPrintWOSemi(f,level) ;
    }
}

void
VeriNode::PrettyPrintExprList(const Array *list, ostream &f, unsigned level)
{
    // Pretty-print a comma-separated list of expressions (also accepts other nodes).
    // Plain "," separator : no space behind it.
    VeriExpression *item ; // Make it expression as it is printing expression list
    unsigned i ;
    FOREACH_ARRAY_ITEM(list, i, item) {
        if (i) f << "," ; // singe ",". No space between the items.

        // Issue 2135 : cap the number of items printed on a single line
        if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

        // This is a comma-separated list, so call each item WOSemi (otherwise a semi might be written at the end).
        if (!item) continue ;
        if (item->IsRange()) f << "[" ; // condition of pattern case item is value range list
        else if (item->IsEventWithAt()) f << "@(" ; // VIPER #5958: Print @ for event expressions
        item->PrettyPrintWOSemi(f,level) ;
        if (item->IsRange()) f << "]" ; // condition of pattern case item is value range list
        else if (item->IsEventWithAt()) f << ")" ;
    }
}

//--------------------------------------------------------------
// #included from VeriModule.h
//--------------------------------------------------------------

void
VeriModule::PrettyPrint(ostream &f, unsigned level) const
{
    if (IsRootModule() && (!_module_items || !_module_items->Size())) return ;

    // Start myself on a new line
    f << endl ;

    RETURN_IF_TICK_PROTECTED ;

    // VIPER #2917: Set current analysis mode to be that of this module:
    unsigned prev_dialect = veri_file::GetAnalysisMode() ;
    veri_file::SetAnalysisMode(_analysis_dialect) ;

    // Print the predefined directives :
    // VIPER #4581: If the last module printed didn't have default_nettype as 'wire' print this default_nettype.
    // We set the default_nettype to veri_file after we have printed the current module, so it is used next time.
    // VIPER #4581 (#7128): Always print the default nettype if set, we set it only when user has specified it.
    //if (_default_nettype && ((_default_nettype != VERI_WIRE) || (veri_file::GetDefaultNettype() != VERI_WIRE))) {
    //    // This module or the last module printed didn't have 'wire' and default_nettype, print it for this module:
    //    f << "`default_nettype " << PrintToken(_default_nettype) << endl ;
    //}
    //veri_file::SetDefaultNettype(_default_nettype) ; // VIPER #4581: Set the default_nettype of this module to veri_file
    if (_default_nettype) f << "`default_nettype " << PrintToken(_default_nettype) << endl ; // VIPER #4581 (#7128): Simply print it
    if (_is_celldefine) f << "`celldefine" << endl ;
    if (_unconnected_drive) f << "`unconnected_drive " << PrintToken(_unconnected_drive) << endl ;
    if (_timescale) f << "`timescale " << _timescale << endl ;

    // VIPER #7379: Print the optional compiler directives:
    if (_delay_mode_distributed)  f << "`delay_mode_distributed" << endl ;
    if (_delay_mode_path)         f << "`delay_mode_path" << endl ;
    if (_delay_mode_unit)         f << "`delay_mode_unit" << endl ;
    if (_delay_mode_zero)         f << "`delay_mode_zero" << endl ;
    if (_default_decay_time)      f << "`default_decay_time " << _default_decay_time << endl ;
    if (_default_trireg_strength) f << "`default_trireg_strength " << _default_trireg_strength << endl ;

    // for root module, only print the module items (at same level)
    if (IsRootModule()) {
        // VeriModule Items
        VeriModuleItem *mi ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_module_items, i, mi) {
            // Write them with one level indent deeper
            if (mi) mi->PrettyPrint(f,level) ;
        }
    } else {
        // Print the indent
        f << PrintLevel(level) ;

        // Print comments
        PrettyPrintPreComments(f, level) ;

        // Print attributes (if they exist)
        PrettyPrintAttributes(f, level) ;

        if (GetQualifier(VERI_EXTERN)) f << PrintToken(VERI_EXTERN) << " " ;
        // Keyword
        if (_id && _id->IsUdp()) {
            f << "primitive " ;
        } else {
            f << "module " ;
        }

        // Print lifetime if present :
        if (GetQualifier(VERI_STATIC)) f << PrintToken(VERI_STATIC) << " " ;
        if (GetQualifier(VERI_AUTOMATIC)) f << PrintToken(VERI_AUTOMATIC) << " " ;

        // module name
        if (_id) _id->PrettyPrint(f, level) ; f << " " ;
#ifdef PRETTY_PRINT_MODULE_INSTANCE_BINDING_INFO
        if (_id) f << "/* id = " << _id ;
        VeriLibrary *module_lib = _id->GetModuleLibrary() ;
        if (module_lib) f << ", lib = " << module_lib->GetName() ;
        f << " */ " ;
#endif

        unsigned i ;

        if (_package_import_decls) {
            VeriTreeNode *item ;
            FOREACH_ARRAY_ITEM(_package_import_decls, i, item) {
                // Issue 2135 : cap the number of items printed on a single line
                if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

                // This is a comma-separated list, so call each item WOSemi (otherwise a semi might be written at the end).
                if (!item) continue ;
                item->PrettyPrint(f,level) ;
            }
        }
        // list of parameters
        if (_parameter_connects) {
            f << "#(" ;
            PrettyPrintCommaList(_parameter_connects, f, level+1) ;
            f << ") " ;
        }

        // list of ports
        if (_port_connects) {
            f << "(" ;
            PrettyPrintCommaList(_port_connects, f, level+1) ;
            f << ") " ;
        }
        PrettyPrintPragmas(f, level) ; // VIPER #6878
        f << ";" << endl ;

        // VeriModule Items
        VeriModuleItem *mi ;
        FOREACH_ARRAY_ITEM(_module_items, i, mi) {
            // Write them with one level indent deeper
            if (mi) mi->PrettyPrint(f,level+1) ;
        }

        // Print post-item comments
        PrettyPrintPostComments(f, level, 0/*no linefeed if no comments*/) ;

        // Close module
        if (!GetQualifier(VERI_EXTERN)) {
            // Do not print endmodule/endprimitive for extern modules
            f << PrintLevel(level) ;
            if (_id && _id->IsUdp()) {
                f << "endprimitive" << endl ;
            } else {
                f << "endmodule" << endl ;
            }
        }
        f << endl ;
    }

    // close flag-directives if we can :
    if (_is_celldefine) f << "`endcelldefine" << endl ;
    if (_unconnected_drive) f << "`nounconnected_drive" << endl ;

    f << endl ; // extra linefeed.

    // VIPER #2917: Restore previous analysis mode:
    veri_file::SetAnalysisMode(prev_dialect) ;
}

void
VeriConfiguration::PrettyPrint(ostream &f, unsigned level) const
{
    // Start myself on a new line
    f << endl ;

    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Keyword
    f << PrintToken(VERI_CONFIG) << " " ;

    // config name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;

    f << ";" ;
    f << endl ;

    // VIPER #7416 : Print the local parameter declarations
    VeriModuleItem *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // 'design' statement for top level modules
    if (_top_modules && _top_modules->Size()) {
        f << PrintLevel(level+1) ;
        f << PrintToken(VERI_DESIGN) ;
        f << " " ;
        VeriName *module_name ;
        FOREACH_ARRAY_ITEM(_top_modules, i, module_name) {
            if (!module_name) continue ;
            module_name->PrettyPrint(f, level) ;
            f << " " ;
        }
        f << ";" << endl ;
    }

    // Configuration rules
    VeriConfigRule *rule ;
    FOREACH_ARRAY_ITEM(_config_rules, i, rule) {
        if (rule) rule->PrettyPrint(f, level+1) ;
    }

    // Print post-item comments
    PrettyPrintPostComments(f, level, 0/*no linefeed if no comments*/) ;

    // Close config
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCONFIG) << endl ;

    f << endl ; // extra linefeed
}
void VeriPrimitive::PrettyPrint(ostream &f, unsigned level) const
{
    VeriModule::PrettyPrint(f, level) ;
}

void
VeriInterface::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (GetQualifier(VERI_EXTERN)) f << PrintToken(VERI_EXTERN) << " " ;
    // Keyword
    f << PrintToken(VERI_INTERFACE) << " " ;

    // module name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;

    unsigned i ;

    if (_package_import_decls) {
        VeriTreeNode *item ;
        FOREACH_ARRAY_ITEM(_package_import_decls, i, item) {
            // Issue 2135 : cap the number of items printed on a single line
            if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

            // This is a comma-separated list, so call each item WOSemi (otherwise a semi might be written at the end).
            if (!item) continue ;
            item->PrettyPrint(f,level) ;
        }
    }
    // list of parameters
    if (_parameter_connects) {
        f << "#(" ;
        PrettyPrintCommaList(_parameter_connects, f, level+1) ;
        f << ") " ;
    }

    // list of ports
    if (_port_connects) {
        f << "(" ;
        PrettyPrintCommaList(_port_connects, f, level+1) ;
        f << ") " ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << ";" ;
    f << endl ;

    // VeriModule Items
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // Print post-item comments
    PrettyPrintPostComments(f, level, 0/*no linefeed if no comments*/) ;

    // Close interface
    if (!GetQualifier(VERI_EXTERN)) {
        f << PrintLevel(level) ;
        f << PrintToken(VERI_ENDINTERFACE) ;
    }
    f << endl ;
}

void
VeriProgram::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (GetQualifier(VERI_EXTERN)) f << PrintToken(VERI_EXTERN) << " " ;

    // Keyword
    f << PrintToken(VERI_PROGRAM) << " " ;

    // module name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;

    unsigned i ;

    // list of parameters
    if (_parameter_connects) {
        f << "#(" ;
        PrettyPrintCommaList(_parameter_connects, f, level+1) ;
        f << ") " ;
    }

    // list of ports
    if (_port_connects) {
        f << "(" ;
        PrettyPrintCommaList(_port_connects, f, level+1) ;
        f << ") " ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << ";" ;
    f << endl ;

    // VeriModule Items
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // Print post-item comments
    PrettyPrintPostComments(f, level, 0/*no linefeed if no comments*/) ;

    // Close program
    if (!GetQualifier(VERI_EXTERN)) {
        f << PrintLevel(level) ;
        f << PrintToken(VERI_ENDPROGRAM) ;
    }
    f << endl ;
}

// VIPER #5710: (vcs_compatibility: always_checker) PrettyPrint for checker:
void
VeriChecker::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Keyword
    f << PrintToken(VERI_CHECKER) << " " ;

    // checker name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;

    unsigned i ;

    // list of ports
    if (_port_connects) {
        f << "(" ;
        PrettyPrintCommaList(_port_connects, f, level+1) ;
        f << ") " ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << ";" ;
    f << endl ;

    // VeriModule Items
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // Print post-item comments
    PrettyPrintPostComments(f, level, 0/*no linefeed if no comments*/) ;

    // Close module
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCHECKER) << endl ;
}

void
VeriPackage::PrettyPrint(ostream &f, unsigned level) const
{
    if (IsRootModule() && (!_module_items || !_module_items->Size())) return ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: do not print the importing verilog package, converted from vhdl package
    if (_is_vhdl_package) return ;
#endif

    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    unsigned i ;

    // for root module, only print moduleitems (at same level)
    if (IsRootModule()) {
        // VIPER #5119 : Print timescale as the first item of root module
        if (_timescale) f << "`timescale " << _timescale << endl ;
        // VeriModule Items
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) {
            // Write them with one level indent deeper
            if (item) item->PrettyPrint(f,level) ;
        }
    } else {
        // Keyword
        f << PrintToken(VERI_PACKAGE) << " " ;

        // Print lifetime if present :
        if (GetQualifier(VERI_STATIC)) f << PrintToken(VERI_STATIC) << " " ;
        if (GetQualifier(VERI_AUTOMATIC)) f << PrintToken(VERI_AUTOMATIC) << " " ;

        // package name
        if (_id) { _id->PrettyPrint(f, level) ; f << " " ; }

        PrettyPrintPragmas(f, level) ; // VIPER #6878
        f << ";" << endl ;

        // VeriModule Items
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) {
            // Write them with one level indent deeper
            if (item) item->PrettyPrint(f,level+1) ;
        }

        // Print post-item comments
        PrettyPrintPostComments(f, level, 0/*no linefeed if no comments*/) ;

        // Close package
        f << PrintLevel(level) ;
        f << PrintToken(VERI_ENDPACKAGE) ;
    }
    f << endl ;
}

void
VeriDPIFunctionDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    f << PrintToken(_type) ; // _type must be there
    f << " \"" << _spec_string << "\" " ; // _spec_string must be there
    f << PrintToken(_property) << " " ;
    if (_c_identifier) {
        PrintIdentifier(f, _c_identifier) ;
        f << " = " ;
    }
    VeriFunctionDecl::PrettyPrintWOSemi(f, level) ;
    f << ";" << endl ;
}

void
VeriDPITaskDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    f << PrintToken(_type) ; // _type must be there
    f << " \"" << _spec_string << "\" " ; // _spec_string must be there
    f << PrintToken(_property) << " " ;
    if (_c_identifier) {
        PrintIdentifier(f, _c_identifier) ;
        f << " = " ;
    }
    VeriTaskDecl::PrettyPrintWOSemi(f, level) ;
    f << ";" << endl ;
}

//--------------------------------------------------------------
// #included from VeriStatement.h
//--------------------------------------------------------------

void
VeriNullStatement::PrettyPrint(ostream &f, unsigned level) const
{
    // Call the WOSemi form, wrapped in a level print at start and semi at end.
    f << PrintLevel(level) ;
    PrettyPrintWOSemi(f,level) ;

    f << ";" ;

    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriNullStatement::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    // Print assignment statement as a item on a list (as in a for-loop).
    // So don't print the semicolon (at end), and don't print the level (at start)

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;
}

void
VeriBlockingAssign::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    // Print assignment statement as a item on a list (as in a for-loop).
    // So don't print the semicolon (at end), and don't print the level (at start)

    // f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    /* target */
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " " << PrintToken(_oper_type) << " " ;

    /* options */
    if (_control) {
        _control->PrettyPrint(f,level) ;
        f << " " ;
    }

    /* value */
    if (_val) _val->PrettyPrint(f,level) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    // f << " ;" << endl ;
}

void
VeriBlockingAssign::PrettyPrint(ostream &f, unsigned level) const
{
    // Call the WOSemi form, wrapped in a level print at start and semi at end.
    f << PrintLevel(level) ;
    PrettyPrintWOSemi(f,level) ;
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriNonBlockingAssign::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    /* target */
    if (_lval) _lval->PrettyPrint(f,level) ;
    f << " " << PrintToken(VERI_LEQ) << " " ;

    /* options */
    if (_control) {
        _control->PrettyPrint(f,level) ;
        f << " " ;
    }

    /* separate options and value */
    f << " " ;

    /* value */
    if (_val) _val->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriGenVarAssign::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    PrettyPrintWOSemi(f,level) ;
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriGenVarAssign::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    // Print assignment statement as a item on a list (as in a for-loop).
    // So don't print the semicolon (at end), and don't print the level (at start)
    // f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    /* target */
    PrintIdentifier(f, (_name) ? _name : (_id) ? _id->Name() : 0) ;
    f << " " << PrintToken(_oper_type) << " " ;

    /* value */
    if (_value) _value->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    // f << " ;" << endl ;
}

void
VeriAssign::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_ASSIGN) << " " ;
    if (_assign) _assign->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriDeAssign::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_DEASSIGN) << " " ;
    if (_lval) _lval->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriForce::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_FORCE) << " " ;
    if (_assign) _assign->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriRelease::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_RELEASE) << " " ;
    if (_lval) _lval->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriTaskEnable::PrettyPrint(ostream &f, unsigned level) const
{
    // Call the WOSemi routine
    PrettyPrintWOSemi(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

// Task Enable is also treated as production item, used in
// randsequence statements where semicolon is not allowed.
void
VeriTaskEnable::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    if (_task_name) _task_name->PrettyPrint(f, level) ;
    f << " " ;

    if (_args) {
        f << "(" ;
        PrettyPrintExprList(_args, f, level+1) ;
        f << ")" ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    // Do not print this semi colon ie. : f << " ;" ;
}
void
VeriSystemTaskEnable::PrettyPrint(ostream &f, unsigned level) const
{
    // Call the WOSemi routine
    PrettyPrintWOSemi(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriSystemTaskEnable::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // PrintIdentifier is not called here, since system call names can't be escaped.or hierarchical
    f << "$" << _name << " " ;

    if (_args) {
        f << "(" ;
        PrettyPrintExprList(_args, f, level+1) ;
        f << ")" ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    //f << " ;" ;
    //PrettyPrintPostComments(f, level) ;
}

void
VeriDelayControlStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    if (_delay) {
        f << PrintLevel(level) ;
        /* Its one expression. Not a normal delay structure */
        // Issue 2221 : always put parenthesis around the delay expression
        if (_is_cycle_delay) {
            f << "##(" ;
        } else {
            if (_delay->IsStep()) {
                f << "#" ; // VIPER #6961 : Donot put '1step' literal within parenthesis
            } else {
                f << "#(" ;
            }
        }
        _delay->PrettyPrint(f,level) ;
        if (_delay->IsStep()) {
            f << " " ; // VIPER #6961 : No parenthesis for 1step
        } else {
            f << ") " ;
        }
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    if (_stmt) {
        if (_stmt->IsNullStatement()) {
            f << ";" << endl ;
        } else {
            _stmt->PrettyPrint(f,(_delay) ? 0 : level) ;
        }
    }
    else f << ";" << endl ;
}

void
VeriEventControlStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    if (_at) {
        f << PrintLevel(level) ;
        f << "@(" ;

        /* 'or' separated list of expressions */
        if (_at->Size() == 0) {
            // Implicit event_expression list
            f << "*" ;
        } else {
            unsigned i ;
            VeriExpression *event_expr ;
            FOREACH_ARRAY_ITEM(_at, i, event_expr) {
                if (i) f << " or " ;

                // Issue 2437 : Cap the number of items printed on a single line
                if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

                if (event_expr) event_expr->PrettyPrint(f,level) ;
            }
        }
        f << ")" ;
        PrettyPrintPragmas(f, level) ; // VIPER #6878
        //if (_stmt) f << endl ;
    } else {
        PrettyPrintPragmas(f, level) ; // VIPER #6878
    }

    PrettyPrintPostComments(f, level, ((_at && _stmt && !_stmt->IsNullStatement())?1:0)) ;

    if (_stmt) {
        if (_stmt->IsNullStatement()) {
            f << ";" << endl ;
        } else {
            _stmt->PrettyPrint(f,level) ;
        }
    }
    else f << ";" << endl ;
}

void
VeriConditionalStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    if (GetQualifier(VERI_UNIQUE)) f << PrintToken(VERI_UNIQUE) << " " ; // unique if
    if (GetQualifier(VERI_UNIQUE0)) f << PrintToken(VERI_UNIQUE0) << " " ; // unique0 if
    if (GetQualifier(VERI_PRIORITY)) f << PrintToken(VERI_PRIORITY) << " " ; // priority if

    f << PrintToken(VERI_IF) << " (" ;
    if (_if_expr) _if_expr->PrettyPrint(f,level) ;
    f << ") " ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ; /* statements start on a new line */

    if (_then_stmt) {
        if (_then_stmt->IsNullStatement()) {
            f << PrintLevel(level + 1) ;
            f << "; \n" ;
        } else {
            _then_stmt->PrettyPrint(f,level+1) ;
        }
    } else {
        f << PrintLevel(level + 1) ;
        f << "; \n" ;
    }

    if (_else_stmt) {
        f << PrintLevel(level) << "else" << endl ;
        if (_else_stmt->IsNullStatement()) {
            f << PrintLevel(level + 1) ;
            f << "; \n" ;
        } else {
            _else_stmt->PrettyPrint(f,level+1) ;
        }
    }
}

// 'if' statement is  also treated as randsequence if-else, used in
// randsequence statements where semicolon is not allowed after the
// 'then' and 'else' statements.
void
VeriConditionalStatement::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    if (GetQualifier(VERI_UNIQUE)) f << PrintToken(VERI_UNIQUE) << " " ; // unique if
    if (GetQualifier(VERI_UNIQUE0)) f << PrintToken(VERI_UNIQUE0) << " " ; // unique0 if
    if (GetQualifier(VERI_PRIORITY)) f << PrintToken(VERI_PRIORITY) << " " ; // priority if

    f << PrintToken(VERI_IF) << " (" ;
    if (_if_expr) _if_expr->PrettyPrint(f,level) ;
    f << ") " ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ; /* statements start on a new line */

    if (_then_stmt) {
        _then_stmt->PrettyPrintWOSemi(f,level+1) ;
        f << endl ;
    } else {
        f << PrintLevel(level + 1) ;
        f << "; \n" ;
    }

    if (_else_stmt) {
        f << PrintLevel(level) << "else" << endl ;
        _else_stmt->PrettyPrintWOSemi(f,level+1) ;
    }
}

void
VeriCaseStatement::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print valid qualifiers on a case statement :
    if (GetQualifier(VERI_UNIQUE)) f << PrintToken(VERI_UNIQUE) << " " ; // unique case
    if (GetQualifier(VERI_UNIQUE0)) f << PrintToken(VERI_UNIQUE0) << " " ; // unique0 case
    if (GetQualifier(VERI_PRIORITY)) f << PrintToken(VERI_PRIORITY) << " " ; // priority case
    f << PrintToken(_case_style) ;
    if (_cond) { // Print braces if condition exists (randcase has no condition)
        f << " (" ;
        _cond->PrettyPrint(f,level) ;
        f << ")" ;
    }
    // Print keyword 'matches' or 'inside' to mark it as case..inside or pattern matching case statement
    if (_type) f << " " << PrintToken(_type) << " " ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Do NOT check IsFullCase/IsParallelCase here, since they also check the attributes.
    // Attributes are printed already, so no need to do that again :
    // Only check the pragma bit-flags (set by pragma's) :
    if (_is_full_case || _is_parallel_case) {
        f << " // synopsys" ;
        if (_is_full_case) f << " full_case" ;
        if (_is_parallel_case) f << " parallel_case" ;
    }

    // VIPER #2494: Don't print (ending) comments here, print a newline instead
    f << endl ;

    unsigned i ;
    VeriCaseItem *ci ;
    /* The object itself is the array */
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        /* There is no separator */
        if (ci) ci->PrettyPrint(f,level) ;
    }

    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCASE) << " " ;

    // VIPER #2494: Print the post comment after the 'endcase' keyword
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriForever::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_FOREVER) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    if (_stmt) {
        _stmt->PrettyPrint(f, level+1) ;
    } else {
        f << PrintLevel(level+1) ;
        f << ";" << endl ;
    }
}

void
VeriRepeat::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_REPEAT) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    if (_stmt) {
        _stmt->PrettyPrint(f, level+1) ;
    } else {
        f << PrintLevel(level+1) ;
        f << ";" << endl ;
    }
}

// 'repeat' statement is  also treated as randsequence repeat, used in
// randsequence statements where semicolon is not allowed after the
// 'repeat' statement.
void
VeriRepeat::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_REPEAT) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    if (_stmt) _stmt->PrettyPrintWOSemi(f, level+1) ;
}

void
VeriWhile::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_WHILE) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    if (_stmt) {
        _stmt->PrettyPrint(f, level+1) ;
    } else {
        f << PrintLevel(level+1) ;
        f << ";" << endl ;
    }
}

void
VeriFor::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_FOR) << " (" ;

    // For SV, there can be multiple initial statements, separated by comma :
    PrettyPrintCommaList(_initials, f, level) ;
    f << " ; " ;
    // Condition
    if (_cond) _cond->PrettyPrint(f, 0) ;
    f << " ; " ;
    // Comma-separated list of repetition statements :
    PrettyPrintCommaList(_reps, f, level) ;
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;

    // Print the statement itself, one level deeper :
    if (_stmt) {
        _stmt->PrettyPrint(f, level+1) ;
    } else { // VIPER #2761 : No statement, should be null statement i.e. only ';'
        f << PrintLevel(level) ;
        f << ";" << endl ;
    }
}

void
VeriWait::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_WAIT) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    // Print "fork" only for "wait fork" statements
    if (!_cond && !_stmt) {
        f <<" "<<PrintToken(VERI_FORK) << " ;" << endl ;
    } else {
        f<< " (" ;
        if (_cond) _cond->PrettyPrint(f,level) ;
        f << ") " ;
        if (_stmt) {
            if (_stmt->IsNullStatement()) {
                f << ";" << endl ;
            } else {
                _stmt->PrettyPrint(f, level+1) ;
            }
        } else {
            f << ";" << endl ;
        }
    }
}

void
VeriDisable::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_DISABLE) << " " ;
    if (_task_block_name) {
        _task_block_name->PrettyPrint(f,level) ;
    } else { // Print fork keywords if no task/block name exists
        f << PrintToken(VERI_FORK) << " " ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriEventTrigger::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // VIPER #4031 : Print proper event trigger token :
    f << PrintToken(_trigger_oper) << " " ;
    // VIPER #4673: Print the delay or event control information:
    if (_control) _control->PrettyPrint(f,level) ;
    if (_event_name) _event_name->PrettyPrint(f,level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriSeqBlock::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_BEGIN) ;

    if (_label) { f << " : " ; _label->PrettyPrint(f, level) ; }
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Start decls and statements on a new line
    f << endl ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decl_items, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }
    FOREACH_ARRAY_ITEM(_stmts, i, item) {
        if (item) {
            item->PrettyPrint(f,level+1) ;
        } else { // VIPER 2346 : It is null statement, print ";"
             f << PrintLevel(level+1) ;
             f << " ; " << endl ;
        }
    }

    f << PrintLevel(level) ;
    f << PrintToken(VERI_END) ;

    // VIPER #3196 (part in the baseline): Print post comments after the 'end' token:
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriParBlock::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(VERI_FORK) ;

    if (_label) { f << " : " ; _label->PrettyPrint(f, level) ; }
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Start decls and statements on a new line
    f << endl ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decl_items, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }
    FOREACH_ARRAY_ITEM(_stmts, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }

    f << PrintLevel(level) ;
    // Print closing token :
    if (_closing_token) {
        f << PrintToken(_closing_token) ;
    } else {
        f << PrintToken(VERI_JOIN) ;
    }

    // VIPER #3196 (part in the baseline): Print post comments after the closing token:
    PrettyPrintPostComments(f, level, 1) ;
}

// VIPER #5710(vcs_compatibility: always_checker(for checker instantiation inside procedural code)):
void
VeriSequentialInstantiation::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_module_name) _module_name->PrettyPrint(f,level) ;
    f << " " ;

    // Instances : comma-sep list of (declared) identifiers (with their port lists) :
    PrettyPrintCommaList(&_insts, f, level+1) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriAssertion::PrettyPrint(ostream &f, unsigned level) const
{
    // a 'immediate' assertion statements
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    if (_assert_cover_property==VERI_EXPECT) {
        // This is a 3.1a expect_property_statement
        f  << PrintToken(_assert_cover_property) << " " ;
    } else if (_assert_cover_property) {
        // This is a (concurrent) 'assert/cover/assume property'. Print keyword 'property' after this :
        if (_final_property) { //==VERI_PROPERTY) {
            f << PrintToken(_assert_cover_property) << " " << PrintToken(_final_property) << " " ;
        } else {
            f << PrintToken(_assert_cover_property) << " " ;
        }
    } else {
        // This is a immediate assertion statement
        f << PrintToken(VERI_ASSERT) << " " ;
    }

    if (_is_deferred) f << "#0 " ; // print deferred assertion's syntax

    f << "(" ;
    if (_property_spec) _property_spec->PrettyPrint(f,level) ;
    f << ") " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Now print the action block : 'if' and 'else' statements.
    if (!_then_stmt && !_else_stmt) {
        // If there is neither a 'then', nor an 'else' statement,
        // then we should only print the closing semicolon.
        f << ";" ;
    }

    if (_then_stmt && _then_stmt->IsNullStatement()) f << ";" ;

    // Print statements starting on a new line
    PrettyPrintPostComments(f, level, 1) ;

    if (_then_stmt && !_then_stmt->IsNullStatement()) {
        _then_stmt->PrettyPrint(f, level+1) ;
    }
    if (_else_stmt) {
        f << PrintLevel(level) ;
        f << PrintToken(VERI_ELSE) << endl ;
        if (_else_stmt->IsNullStatement()) {
            f << ";" ;
        } else {
            _else_stmt->PrettyPrint(f, level+1) ;
        }
    }
}

void
VeriJumpStatement::PrettyPrint(ostream &f, unsigned level) const
{
    // a 'return', 'break' or 'continue' statement
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    f << PrintToken(_jump_token) << " " ;

    if (_expr) _expr->PrettyPrint(f, level) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << ";" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriDoWhile::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print statement
    f << PrintToken(VERI_DO) << endl ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    if (_stmt) {
        _stmt->PrettyPrint(f, level+1) ;
    } else {
        f << PrintLevel(level+1) ;
        f << ";" << endl ;
    }

    // Print while (cond) ;
    f << PrintLevel(level) << PrintToken(VERI_WHILE) << " " ;
    f << "(" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" ;
    f << ";" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriForeach::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print statement
    f << PrintToken(VERI_FOREACH) << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << "(" ;
    if (_array) _array->PrettyPrint(f, level) ;

    f << "[" ;
    PrettyPrintCommaList(_loop_indexes, f, level) ;
    f << "])" << endl ;
    if (_stmt) _stmt->PrettyPrint(f, level+1) ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriWaitOrder::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print statement
    f << PrintToken(VERI_WAIT_ORDER) << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << "(" ;
    PrettyPrintCommaList(_events, f, level) ; // Print event list
    f << ")" << endl ;

    if (_then_stmt) {
        if (_then_stmt->IsNullStatement()) {
            f << PrintLevel(level + 1) ;
            f << "; " ; // _then_stmt is null_statement
        } else {
            _then_stmt->PrettyPrint(f, level+1) ; // Print then statement
        }
    }

    if (_else_stmt) {
        f << PrintLevel(level) ;

        f << PrintToken(VERI_ELSE) << endl ;
        _else_stmt->PrettyPrint(f, level+1) ; // Print else statement
    }
    if (!_then_stmt && !_else_stmt) {
        f << PrintLevel(level + 1) ;
        f << "; " ; // _then_stmt is null_statement
    }
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriRandsequence::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print Randsequence
    f << PrintToken(VERI_RANDSEQUENCE) << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    f<< "(" ;
    if (_start_prod) _start_prod->PrettyPrint(f, level) ; // Pretty print the starting production
    f<< ")" << endl ;

    // Print the production list
    unsigned i ;
    VeriProduction *prod ;
    FOREACH_ARRAY_ITEM(_productions, i, prod) {
        if (prod) prod->PrettyPrint(f,level+1) ;
    }

    // Print EndSequence
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDSEQUENCE) << endl ;
}

void
VeriCodeBlock::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    f << "{" << endl ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print the list of declarations
    unsigned i ;
    VeriModuleItem *mod_item ;
    FOREACH_ARRAY_ITEM(_decls, i, mod_item) {
        if (mod_item) mod_item->PrettyPrint(f,level+1) ;
    }

    // Print the list of statements
    VeriModuleItem *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << PrintLevel(level) ;
    f << "}" ;
}
void
VeriArrayMethodCall::PrettyPrint(ostream &f, unsigned level) const
{
    // Call the WOSemi routine
    PrettyPrintWOSemi(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriArrayMethodCall::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print statement
    if (_left) _left->PrettyPrintWOSemi(f, 0) ; // Print left hand side
    f << " " << PrintToken(VERI_WITH) << " (" ;

    if (_right) _right->PrettyPrint(f, level) ; // Print right hand side
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
}
void
VeriInlineConstraintStmt::PrettyPrint(ostream &f, unsigned level) const
{
    // Call the WOSemi routine
    PrettyPrintWOSemi(f, level) ;
    f << ";" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriInlineConstraintStmt::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments/attributes before the statement itself (if they exist)
    PrettyPrintPreStatement(f, level) ;

    // Print attributes (if they exist)
    //PrettyPrintAttributes(f, level) ; // printed from PrettyPrintPreStatement

    // Print statement
    if (_left) _left->PrettyPrintWOSemi(f, 0) ; // Print left hand side
    f << " " ;
    // Print constraint block
    if (_constraint_block) {
        f << PrintToken(VERI_WITH) << " " ;
        f << "{ " ;
        // Print the _constraint_block. A ";" separated list of expressions :
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_constraint_block, i, expr) {
            if (expr) expr->PrettyPrintWithSemi(f, level+1) ;
        }
        f << PrintLevel(level) << "} " ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
}

void
VeriStatement::PrettyPrintPreStatement(ostream &f, unsigned level) const
{
    // Write stuff before the statement itself gets printed.

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print the statement label if it exists :
    if (_opening_label) {
        _opening_label->PrettyPrint(f,level) ;
        f << " " << PrintToken(VERI_COLON) << " " ;
    }
}

//--------------------------------------------------------------
// #included from VeriModuleItem.h
//--------------------------------------------------------------

void
VeriDataDecl::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    // Print declaration as a item on a list
    // So don't print the semicolon (at end), and don't print the level (at start)

    // f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // VIPER #5661 : Print direction before all qualifiers
    // Print direction if applicable :
    if (GetDir()) f << PrintToken(GetDir()) << " " ;

    // First print import/export, can happen for modport-ports (VIPER 3769, 3451)
    if (GetQualifier(VERI_IMPORT)) f << PrintToken(VERI_IMPORT) << " " ;
    if (GetQualifier(VERI_EXPORT)) f << PrintToken(VERI_EXPORT) << " " ;
    // Now Print valid qualifiers on a data declaration :
    if (GetQualifier(VERI_CONST)) f << PrintToken(VERI_CONST) << " " ; // constant_decl
    // property_qualifiers :
    if (GetQualifier(VERI_RAND)) f << PrintToken(VERI_RAND) << " " ;
    if (GetQualifier(VERI_RANDC)) f << PrintToken(VERI_RANDC) << " " ;
    // class_qualifiers :
    if (GetQualifier(VERI_PROTECTED)) f << PrintToken(VERI_PROTECTED) << " " ;
    if (GetQualifier(VERI_LOCAL)) f << PrintToken(VERI_LOCAL) << " " ;
    // var : 'var' should come before lifetime
    if (GetQualifier(VERI_VAR)) f << PrintToken(VERI_VAR) << " " ;
    // lifetime :
    if (GetQualifier(VERI_STATIC)) f << PrintToken(VERI_STATIC) << " " ;
    if (GetQualifier(VERI_AUTOMATIC)) f << PrintToken(VERI_AUTOMATIC) << " " ;
    // virtual : Don't print virtual keyword for type definition
    if (GetQualifier(VERI_VIRTUAL) && !IsTypeAlias()) f << PrintToken(VERI_VIRTUAL) << " " ;

    // Print Declaration token if applicable :
    switch (_decl_type) {
    case VERI_PARAMETER :
    case VERI_LOCALPARAM :
    case VERI_SPECPARAM :
    case VERI_GENVAR :
    case VERI_TYPEDEF :
    case VERI_NETTYPE :
        f << PrintToken(_decl_type) << " " ;
        break ;
    default :
        break ;
    }

    // VIPER 3267 : direction 'const' means 'const ref'
    if (GetDir()==VERI_CONST) f << PrintToken(VERI_REF) << " " ;

    // If virtual keyword exists for this type declaration, print it before data type
    //if (GetQualifier(VERI_VIRTUAL) && (_decl_type == VERI_TYPEDEF)) f << PrintToken(VERI_VIRTUAL) << " " ;
        // Print data type :
        if (_data_type) {
            _data_type->PrettyPrint(f,level) ;
        } else if ((_decl_type==VERI_TYPEDEF) && (_ids.Size()==1)) {
            // VIPER #7951 (print forward type): For forward typedef, write type of/from the identifier:
            VeriIdDef *id = (VeriIdDef *)_ids.GetFirst() ;
            unsigned id_type = (id) ? id->Type() : 0 ;
            switch (id_type) {
            case VERI_ENUM:
            case VERI_UNION:
            case VERI_CLASS:
            case VERI_STRUCT:
            case VERI_COVERGROUP:
                // Write only class/enum/struct/union/covergroup. Others may be illegal:
                f << PrintToken(id_type) << " " ;
                break ;
            case VERI_INTERFACE:
                f << "interface class " ; // Only VERI_INTERFACE keyword is stored, so do not use PrintToken here
                break ;
            default: break ;
            }
        }

    // Print identifiers :
    // Cannot use 'PrettyPrintCommaList' since we need to do tests on the id's (issue 1634), and we want to do some more formatting.
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        // Print a comma separator between the identifiers
        if (i) f << ", " ;

        // Issue 2135 : cap the number of items printed on a single line
        if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

        if (!id) continue ;

        // VIPER Issue 2222 and 1634 : the initial value should only be printed once,
        // even though the identifier can show up in two 'declarations' (one as a port decl, one as a reg/net decl).
        // or only as one (port decl OR net/reg decl). This (possible double-decl) is only the case for ports. Not for other ids.
        // Print the initial value only on the decl which defines the data-type set as back-pointer to the id.
        // That back-pointer is always set, to a single data decl, so we are cool :
        // Initial value will always get printed, and printed only once :
        // Viper 5733. Illegal PP output for multiple declared ports
        unsigned packed_datatype = _data_type && _data_type->PackedDimension() ;
        if ((id->GetInitialValue() || (id->GetDimensions() && (IsAms() || !packed_datatype))) && id->IsPort() && id->GetDataType()!=_data_type) {
            // There is an initial value on this port, but the id's datatype is not pointer back to me.
            // So print only the identifier name. not the initial value :
            PrintIdentifier(f, id->GetName()) ;
            continue ;
        }

        // If this second or later identifier has an initial value, print it at a new line at level+1.
        // This nicely organizes identifiers with initial values (parameters) in a column.
        if (i && id->GetInitialValue() && (i%MAX_COMMA_LIST_COUNT)) {
            f << endl << PrintLevel(level+1) ;
        }
        id->PrettyPrint(f,level+1) ;
    }

    if (_resolution_function) {
        f << " with " ;
        _resolution_function->PrettyPrint(f,level+1) ;
    }
    // f << " ; " << endl ;
}

void
VeriDataDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print declaration.
    f << PrintLevel(level) ;

    // VIPER #6182: Return from here, otherwise we may print an empty semi-colon:
    RETURN_IF_TICK_PROTECTED ;

    PrettyPrintWOSemi(f,level) ;

    f << " ; " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriNetDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // NetDecl has very specific syntax :
    //
    //   'net_type' 'strength' 'vectored|scalared' 'signing' 'range(s)' 'delay' id_list ;
    //
    // Our _data_type field contains the net_type, the signing and the range,
    // but since the 'strength' needs to be printed in the middle of that,
    // we cannot use _data_type->PrettyPrint(). We need to use the individual
    // accessor functions on the data_type to create a syntactically correct net declaration :

    unsigned net_type = (_data_type) ? _data_type->GetType() : VERI_WIRE ;
    f << PrintToken(net_type) << " " ;

    // strength
    if (_strength) _strength->PrettyPrint(f, level) ;

    // vectored|scalared
    if (GetQualifier(VERI_VECTORED)) f << PrintToken(VERI_VECTORED) << " " ;
    if (GetQualifier(VERI_SCALARED)) f << PrintToken(VERI_SCALARED) << " " ;

    // Print net data type separately if data type is VeriNetDataType:
    if (_data_type && _data_type->NetType()) {
        VeriDataType *data_type = (_data_type) ? _data_type->GetDataType() : 0 ;
        if (data_type) data_type->PrettyPrint(f,level) ;
    } else {
        // signing
        if (_data_type && _data_type->GetSigning()) f << PrintToken(_data_type->GetSigning()) << " " ;

        // range(s)
        VeriRange *data_type_dim = (_data_type) ? _data_type->GetDimensions() : 0 ;
        if (data_type_dim) {
            f << "[" ;
            data_type_dim->PrettyPrint(f,level) ;
            f << "]" ;
            f << " " ;
        }
    }

    // delay
    if (_delay) {
        VeriExpression *single_delay = (_delay->Size() == 1) ? (VeriExpression*)_delay->At(0): 0 ;
        // VIPER #6961 : If delay is 1step, print without parenthesis
        if (single_delay && single_delay->IsStep()) {
            f << "#" ; single_delay->PrettyPrint(f,level) ; f << " " ;
        } else {
            f << "#(" ;
            PrettyPrintExprList(_delay, f, level+1) ;
            f << ") " ;
        }
    }

    // Print identifiers :
    //PrettyPrintCommaList(&_ids, f, level+1) ;
    // Cannot use 'PrettyPrintCommaList' since we want to do some more formatting simiar to VeriDataDecl
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        // Print a comma separator between the identifiers
        if (i) f << ", " ;

        // Issue 2135 : cap the number of items printed on a single line
        if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

        if (!id) continue ;

        // VIPER Issue 2222 and 1634 : the initial value should only be printed once,
        // even though the identifier can show up in two 'declarations' (one as a port decl, one as a reg/net decl).
        // or only as one (port decl OR net/reg decl). This (possible double-decl) is only the case for ports. Not for other ids.
        // Print the initial value only on the decl which defines the data-type set as back-pointer to the id.
        // That back-pointer is always set, to a single data decl, so we are cool :
        // Initial value will always get printed, and printed only once :
        // VIPER #7103, #5733. Illegal PP output for multiple declared ports
        unsigned packed_datatype = _data_type && _data_type->PackedDimension() ;
        if ((id->GetInitialValue() || (id->GetDimensions() && (IsAms() || !packed_datatype))) && id->IsPort() && id->GetDataType()!=_data_type) {
            // There is an initial value on this port, but the id's datatype is not pointer back to me.
            // So print only the identifier name. not the initial value :
            PrintIdentifier(f, id->GetName()) ;
            continue ;
        }

        // If this second or later identifier has an initial value, print it at a new line at level+1.
        // This nicely organizes identifiers with initial values (parameters) in a column.
        if (i && id->GetInitialValue() && (i%MAX_COMMA_LIST_COUNT)) {
            f << endl << PrintLevel(level+1) ;
        }
        id->PrettyPrint(f,level+1) ;
    }

    f << " ; " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriFunctionDecl::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print valid qualifiers on a function/task declaration :
    // prototypes :
    if (GetQualifier(VERI_IMPORT)) f << PrintToken(VERI_IMPORT) << " " ;
    if (GetQualifier(VERI_EXPORT)) f << PrintToken(VERI_EXPORT) << " " ;
    if (GetQualifier(VERI_EXTERN)) f << PrintToken(VERI_EXTERN) << " " ;
    if (_name && !_name->IsHierName()) {
        // VIPER #4915: Do not print qualifiers for external definition of class methods
        // method_qualifiers :
        // VIPER #3824 : Allow 'pure' as qualifier to method
        if (GetQualifier(VERI_PURE)) f << PrintToken(VERI_PURE) << " " ;
        if (GetQualifier(VERI_VIRTUAL)) f << PrintToken(VERI_VIRTUAL) << " " ;
        // class_qualifiers :
        if (GetQualifier(VERI_PROTECTED)) f << PrintToken(VERI_PROTECTED) << " " ;
        if (GetQualifier(VERI_LOCAL)) f << PrintToken(VERI_LOCAL) << " " ;
        // lifetime (static/automatic) allowed AFTER the 'function' token (token _automatic_type),
        // but also allowed before the function token (set as (class) qualifier).
        if (GetQualifier(VERI_AUTOMATIC)) f << PrintToken(VERI_AUTOMATIC) << " " ;
        if (GetQualifier(VERI_STATIC)) f << PrintToken(VERI_STATIC) << " " ;
    }

    // VIper 7975: For extern C functions do not print the function keyword
    // Do not do this check for DPI method which also uses this function to
    // do pretty print
    if (_verilog_method || IsDPIMethod()) f << PrintToken(VERI_FUNCTION) << " " ;
    if (_automatic_type) f << PrintToken(_automatic_type) << " " ; // lifetime

    if (_data_type) {
        // Viper #5388: Print virtual keyword from data type if set
        //if  (_data_type->IsVirtualInterface()) f << PrintToken(VERI_VIRTUAL) << " " ;
        _data_type->PrettyPrint(f, level) ;
    }

    if (_name) _name->PrettyPrint(f, level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    if (_ansi_io_list) {
        f << "( ";
        PrettyPrintCommaList(_ansi_io_list, f, level+1) ;
        f << " ) ";
    }
}

void
VeriFunctionDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ; // VIPER #8428: Return if protected item

    f << PrintLevel(level) ;
    PrettyPrintWOSemi(f,level) ;

    // Always print the semi-colon:
    f << " ; " ;

    // Print comments
    PrettyPrintPostComments(f, level, 1) ;

    // For prototype task/function decl : it's a prototype if there are no statements and no decls arrays.
    if (!_decls && !_stmts) return ; // done : don't print the body..

    // Start the body :

    // decls and statements can still be empty arrays here..
    // decls
    unsigned i ;
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (decl_item) decl_item->PrettyPrint(f,level+1) ;
    }

    // and statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }

    // Print a ";" if there were no statements but there is a statement array ,
    // i.e function contains one null statement
    if (_stmts && _stmts->Size()==0) f << PrintLevel(level+1) << "; // NOOP" << endl ;

    // Close this thing.
    f << PrintLevel(level) << PrintToken(VERI_ENDFUNCTION) ;
    f << endl ; // there is no semi. Just a newline.
}

void
VeriTaskDecl::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print valid qualifiers on a function/task declaration :
    // prototypes :
    if (GetQualifier(VERI_IMPORT)) f << PrintToken(VERI_IMPORT) << " " ;
    if (GetQualifier(VERI_EXPORT)) f << PrintToken(VERI_EXPORT) << " " ;
    if (GetQualifier(VERI_EXTERN)) f << PrintToken(VERI_EXTERN) << " " ;
    if (GetQualifier(VERI_FORKJOIN)) f << PrintToken(VERI_FORKJOIN) << " " ;
    if (_name && !_name->IsHierName()) {
        // VIPER #4915: Do not print qualifiers for external definition of class methods
        // method_qualifiers :
        // VIPER #3824 : Allow 'pure' as qualifier to method
        if (GetQualifier(VERI_PURE)) f << PrintToken(VERI_PURE) << " " ;
        if (GetQualifier(VERI_VIRTUAL)) f << PrintToken(VERI_VIRTUAL) << " " ;
        // class_qualifiers :
        if (GetQualifier(VERI_PROTECTED)) f << PrintToken(VERI_PROTECTED) << " " ;
        if (GetQualifier(VERI_LOCAL)) f << PrintToken(VERI_LOCAL) << " " ;
        // lifetime (static/automatic) normally served AFTER the 'task' token (_automatic_type)
        // but also allowed before the task token (set as (class) qualifier).
        if (GetQualifier(VERI_AUTOMATIC)) f << PrintToken(VERI_AUTOMATIC) << " " ;
        if (GetQualifier(VERI_STATIC)) f << PrintToken(VERI_STATIC) << " " ;
    }

    f << PrintToken(VERI_TASK) << " " ;

    f << PrintToken(_automatic_type) << " " ; // lifetime

    if (_name) _name->PrettyPrint(f, level) ;
    //if (_task) _task->PrettyPrint(f, level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    // Argument list : only print () if there are arguments....
    // Viper: 7975: A2.7 defines that parentheisis printing is mandatory
    // However this restriction is removed in 2012.(non-terminal task_prototype)
    // Similar restriction applies also to function decleration, However interestingly
    // this was already implemented for functions. Remember to remove the restriction
    // for function also in 2012 mode.
    if (_ansi_io_list) {
        f << "(";
        PrettyPrintCommaList(_ansi_io_list, f, level+1) ;
        f << ")";
    }
}

void
VeriTaskDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    PrettyPrintWOSemi(f,level) ;

    // Always print the semi-colon:
    f << " ; " ;

    // Print comments
    PrettyPrintPostComments(f, level, 1) ;

    // For prototype task/function decl : it's a prototype if there are no statements and no decls arrays.
    if (!_decls && !_stmts) return ; // done : don't print the body..

    // Start the body :

    // decls and statements can still be empty arrays here..
    // decls
    unsigned i ;
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (decl_item) decl_item->PrettyPrint(f,level+1) ;
    }

    // and statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->PrettyPrint(f,level+1) ;
    }

    // Print a ";" if there were no statements..
    if (!_stmts || _stmts->Size()==0) f << PrintLevel(level+1) << "; // NOOP" << endl ;

    // close this thing..
    f << PrintLevel(level) << PrintToken(VERI_ENDTASK) ;
    f << endl ; // there is no semi. Just a newline.
}

void
VeriDefParam::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_DEFPARAM) << " " ;

    // List of comma-separated defparameter assignments :
    PrettyPrintCommaList(_defparam_assigns, f, level+1) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriContinuousAssign::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_ASSIGN) << " " ;

    if (_strength) _strength->PrettyPrint(f,level) ;

    if (_delay) {
        VeriExpression *single_delay = (_delay->Size() == 1) ? (VeriExpression*)_delay->At(0): 0 ;
        // VIPER #6961 : If delay is 1step, print without parenthesis
        if (single_delay && single_delay->IsStep()) {
            f << "#" ; single_delay->PrettyPrint(f,level) ; f << " " ;
        } else {
            f << "#(" ;
            PrettyPrintExprList(_delay, f, level+1) ;
            f << ") " ;
        }
    }

    // Print the net assignment(s) : comma-separated list.
    PrettyPrintCommaList(_net_assigns, f, level+1) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriGateInstantiation::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(_inst_type) << " " ;

    if (_strength) _strength->PrettyPrint(f,level) ;

    if (_delay) {
        VeriExpression *single_delay = (_delay->Size() == 1) ? (VeriExpression*)_delay->At(0): 0 ;
        // VIPER #6961 : If delay is 1step, print without parenthesis
        if (single_delay && single_delay->IsStep()) {
            f << "#" ; single_delay->PrettyPrint(f,level) ; f << " " ;
        } else {
            f << "#(" ;
            PrettyPrintExprList(_delay, f, level+1) ;
            f << ") " ;
        }
    }

    // Instances : comma-sep list of (declared) identifiers (with their port lists) :
    PrettyPrintCommaList(_insts, f, level+1) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriModuleInstantiation::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_module_name) _module_name->PrettyPrint(f,level) ;
    f << " " ;
#ifdef PRETTY_PRINT_MODULE_INSTANCE_BINDING_INFO
    VeriIdDef *resolved_mod = (_module_name) ? _module_name->GetId() : 0 ;
    if (resolved_mod) {
        f << "/* id = " << resolved_mod << ", " ;
        VeriLibrary *module_lib = resolved_mod->GetModuleLibrary() ;
        if (module_lib) f << module_lib->GetName() << "." ;
        f << resolved_mod->Name() << " */ " ;
    }
#endif

    if (_strength) _strength->PrettyPrint(f,level) ;

    if (_param_values && (_param_values->Size() > 0 )) {
        f << "#(" ;
        PrettyPrintExprList(_param_values, f, level+1) ;
        f << ") " ;
    }

    // Instances : comma-sep list of (declared) identifiers (with their port lists) :
    PrettyPrintCommaList(&_insts, f, level+1) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriSpecifyBlock::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

   f << PrintToken(VERI_SPECIFY) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;

    VeriModuleItem *mi ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_specify_items, i, mi) {
        if (mi) mi->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << PrintToken(VERI_ENDSPECIFY) << endl ;
}

void
VeriPathDecl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    if (_cond) {    // state-dependent path
        f << "if (";
        _cond->PrettyPrint(f, level) ;
        f << ") " ;
    } else if (_ifnone) {   // ifnone statement
        f << PrintToken(VERI_IFNONE) << " " ;
    }

    // Print path declaration
    if (_path) _path->PrettyPrint(f, level) ;

    f << PrintToken(VERI_EQUAL) << " (" ;

    // Print delays
    PrettyPrintExprList(_delay, f, level+1) ;

    f << ") ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriSystemTimingCheck::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    // System task names can't be escaped or hierarchical, so don't call VeriNode::PrintIdentifier here.
    f << "$" << _task_name << " " ;

    if (_args) {
        f << "(" ;
        PrettyPrintExprList(_args, f, level+1) ;
        f << ")" ;
    }
    f << " ; " << endl ;
}

void
VeriInitialConstruct::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_INITIAL) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    if (_stmt) {
        _stmt->PrettyPrint(f,level+1) ;
    } else {
        f << PrintLevel(level+1) ;
        f << ";" << endl ;
    }
}

void
VeriAlwaysConstruct::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print the right 'always' (may be 'always_comb', 'always_latch', 'always_ff', 'final' or 'always'):
    if (GetQualifier(VERI_ALWAYS_COMB)) {
        f << PrintToken(VERI_ALWAYS_COMB) ;
    } else if (GetQualifier(VERI_ALWAYS_LATCH)) {
        f << PrintToken(VERI_ALWAYS_LATCH) ;
    } else if (GetQualifier(VERI_ALWAYS_FF)) {
        f << PrintToken(VERI_ALWAYS_FF) ;
    } else if (GetQualifier(VERI_FINAL)) {
        f << PrintToken(VERI_FINAL) ;
    } else {
        f << PrintToken(VERI_ALWAYS) ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    if (_stmt) {
        _stmt->PrettyPrint(f,level+1) ;
    } else {
        f << PrintLevel(level+1) ;
        f << ";" << endl ;
    }
}

void
VeriGenerateConstruct::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_GENERATE) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << PrintToken(VERI_ENDGENERATE) << endl ;
}

void
VeriGenerateConditional::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    PrettyPrintPreComments(f, level) ;
    f << PrintToken(VERI_IF) << " (" ;
    _if_expr->PrettyPrint(f,level) ;
    f << ") " ;
    PrettyPrintPostComments(f, level, 1) ; // statements start on a new line
    if (_then_item) {
        VeriIdDef *then_blk_id = (_then_scope) ? _then_scope->GetOwner(): 0 ;
        if (then_blk_id) {
            f << PrintLevel(level +1) ;
            f << PrintToken(VERI_BEGIN) ;
            f << ": " ; then_blk_id->PrettyPrint(f, level) ; f << endl ;
        }
        _then_item->PrettyPrint(f,then_blk_id ? (level+2) : (level+1)) ;
        if (then_blk_id) {
            f << PrintLevel(level+1) ;
            f << PrintToken(VERI_END) ; f << endl ;
        }
    } else {
        // Viper 5695. illegal PP output when _then_item is NULL
        f << PrintLevel(level) ;
        f << ";" << endl ;
    }

    if (_else_item) {
        f << PrintLevel(level) << "else" << endl ;
        VeriIdDef *else_blk_id = (_else_scope) ? _else_scope->GetOwner(): 0 ;
        if (else_blk_id) {
            f << PrintLevel(level+1) ;
            f << PrintToken(VERI_BEGIN) ;
            f << ": " ; else_blk_id->PrettyPrint(f, level) ; f << endl ;
        }
        _else_item->PrettyPrint(f,else_blk_id ? (level+2) : (level+1)) ;
        if (else_blk_id) {
            f << PrintLevel(level+1) ;
            f << PrintToken(VERI_END) ; f << endl ;
        }
    }
}

void
VeriGenerateCase::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    PrettyPrintPreComments(f, level) ;
    f << PrintToken(_case_style) << " (" ;
    if (_cond) _cond->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPostComments(f, level, 1) ;

    unsigned i ;
    VeriCaseItem *ci ;
    // The object itself is the array
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        // There is no separator
        if (ci) ci->PrettyPrint(f,level) ;
    }

    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCASE) << endl ;
}

void
VeriGenerateFor::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    PrettyPrintPreComments(f, level) ;

    // Print 'for' items on one line : FIX ME : printing too many ';'s.
    f << PrintToken(VERI_FOR) << " (" ;
    if (_initial) _initial->PrettyPrintWOSemi(f,0) ;
    f << ";" ;
    if (_cond) _cond->PrettyPrint(f,0) ;
    f << ";" ;
    if (_rep) _rep->PrettyPrintWOSemi(f,0) ;
    f << ")" << endl ;

    // Print the generate block :
    f << PrintLevel(level) ;
    f << PrintToken(VERI_BEGIN) ;

    if (_block_id) {
        f << " : " ; _block_id->PrettyPrint(f, level) ;
    }
    f << endl ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_END) ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriGenerateBlock::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    f << PrintToken(VERI_BEGIN) ;

    if (_block_id) {
        f << " : " ; _block_id->PrettyPrint(f, level) ;
    }
    f << endl ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_END) << endl ;
}

void
VeriTable::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_TABLE) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;

    unsigned i ;
    const char *r ;
    const char *entry ;
    FOREACH_ARRAY_ITEM(_table_entries, i, entry) {
        if (!entry) continue ;
        f << PrintLevel(level) ;

        // VIPER 3340 : Every character in the entry could be a double-char edge character.
        // So we now have to print characters one by one :
        r = entry ;
        while (r && *r!='\0') {
            if (PrintUdpEdgeChar(*r)) {
                f << PrintUdpEdgeChar(*r) ; // printed as double-char string : "(..)"
            } else {
                f << *r ; // printed as single-char value or edge ('f', '0', '1', '*' etc).
            }
            r++ ;
        }
        // close the entry
        f << " ;" << endl ;
    }

    f << PrintLevel(level) << PrintToken(VERI_ENDTABLE) << endl ;
}

// Resolve for System Verilog classes :

void
VeriClass::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print keyword "virtual" for a virtual class
    if (GetQualifier(VERI_VIRTUAL)) f << PrintToken(VERI_VIRTUAL) << " " ; // VIPER 2609
    unsigned interface_class = 0 ;
    // Print keyword "interface" for a interface class
    if (GetQualifier(VERI_INTERFACE)) {
        f << PrintToken(VERI_INTERFACE) << " " ;
        interface_class = 1 ;
    }

    // Keyword
    f << PrintToken(VERI_CLASS) << " " ;

    // module name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    unsigned i ;

    // list of parameters
    if (_parameter_connects) {
        f << "#(" ;
        PrettyPrintCommaList(_parameter_connects, f, level+1) ;
        f << ") " ;
    }

    if (_base_class_name) {
        f << PrintToken(VERI_EXTENDS) << " " ;
        _base_class_name->PrettyPrint(f, level) ;
        f << " " ;
    }
    if (_interface_classes && _interface_classes->Size()) {
        if (interface_class) {
            f << PrintToken(VERI_EXTENDS) << " " ;
        } else {
            f << PrintToken(VERI_IMPLEMENTS) << " " ;
        }
        PrettyPrintCommaList(_interface_classes, f, 0) ;
    }

    f << ";" ;
    PrettyPrintPostComments(f, level, 1) ;

    // VeriModule Items
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // Close module
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDCLASS) << endl ;
}

void
VeriPropertyDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Keyword
    f << PrintToken(VERI_PROPERTY) << " " ;

    // module name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    unsigned i ;

    // Issue 2540: formals are now a full ansi_port_list.
    if (_ansi_io_list) {
        f << "( ";
        PrettyPrintCommaList(_ansi_io_list, f, level+1) ;
        f << " ) ";
    }

    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;

    // The declarations
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decls, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // The (property) spec :
    if (_spec) {
        f << PrintLevel(level) ;
        _spec->PrettyPrint(f, level+1) ;
        f << "; " << endl ; // Close spec with a semicolon and a new line
    }

    // Close module
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDPROPERTY) << endl ;
}

void
VeriSequenceDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Keyword
    f << PrintToken(VERI_SEQUENCE) << " " ;

    // module name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    unsigned i ;

    // Issue 2540: formals are now a full ansi_port_list.
    if (_ansi_io_list) {
        f << "( ";
        PrettyPrintCommaList(_ansi_io_list, f, level+1) ;
        f << " ) ";
    }

#if 0
    // VeriBinaryOper already determines which operators (like VERI_(EVENT)_OR ) should not print parenthesis.
    // So this code should be obsolete.

    // list of formals (ports)
    if (_formals) {
        f << "(" ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(_formals, i, id) {
            if (i) f << ", " ;
            if (id) {
                //id->PrettyPrint(f,level+1) ;
                // Don't print the () if the initial value is a binary operator. So we need to handle that
                PrintIdentifier(f, id->Name()) ; // Print the name of this identifier
                VeriExpression *init_val = id->GetInitialValue() ;
                if (init_val) {
                    f << " " << PrintToken(VERI_EQUAL) << " " ;
                    // Get the left and right of this expression. They will be valid, if the initial value
                    // is a binary operator. So, we check that
                    VeriExpression *lhs = init_val->GetLeft() ;
                    VeriExpression *rhs = init_val->GetRight() ;
                    if (init_val->OperType() && lhs && rhs) {
                        // Print the left and then the type and then the right side of the expression
                        lhs->PrettyPrint(f, level+1) ;
                        f << " " << PrintToken(init_val->OperType()) << " " ;
                        rhs->PrettyPrint(f, level+1) ;
                    } else {
                        // Otherwise, its not a binary operator, just print it normally
                        init_val->PrettyPrint(f, level) ;
                    }
                }
            }
        }
        f << ") " ;
    }
#endif

    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;

    // The declarations
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decls, i, item) {
        // Write them with one level indent deeper
        if (item) item->PrettyPrint(f,level+1) ;
    }

    // The (sequence) implementation spec :
    if (_spec) {
        f << PrintLevel(level) ;
        _spec->PrettyPrint(f, level+1) ;
        f << "; " << endl ; // Close spec with a semicolon and a new line
    }

    // Close module
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDSEQUENCE) << endl ;
}

void
VeriModport::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print opening token
    f << PrintToken(VERI_MODPORT) << " " ;

    // Print the modport decls :
    PrettyPrintCommaList(_modport_decls, f, level+1) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << "; " ; // Close spec with a semicolon and a new line
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriModportDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // ModportDecl does NOT start with indent and is NOT closed with semi.
    // That formatting is done in VeriModport.

    if (_id) _id->PrettyPrint(f, level) ; f << " " ;

    // List of modport port decls :
    f << "(" ;
    PrettyPrintCommaList(_modport_port_decls, f, level+1) ;
    f << ")" ;
}

void
VeriClockingDecl::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Check if this has a 'default' qualifier :
    if (GetQualifier(VERI_DEFAULT)) f << PrintToken(VERI_DEFAULT) << " " ;

    // VIPER #5710: (vcs_compatibility: always_checker) Check if this has a 'global' qualifier :
    if (GetQualifier(VERI_GLOBAL)) f << PrintToken(VERI_GLOBAL) << " " ;

    // Print opening token
    f << PrintToken(VERI_CLOCKING) << " " ;

    // Print the clock name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Print the clock expression (in parenthesis) :
    if (_clocking_event) {
        //f << "@(" ;
        _clocking_event->PrettyPrint(f, level) ;
        //f << ")" ;
    }
}
void
VeriClockingDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    // Print the header without ending semi-colon
    PrettyPrintWOSemi(f, level) ;

    // Semi :
    f << ";" ;
    PrettyPrintPostComments(f, level, 1) ;

    if (_clocking_items) {
        // Print the clocking items ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_clocking_items, i, item) {
            if (item) item->PrettyPrint(f, level+1) ;
        }
    }

    // Close with endclocking token only if the clocking contains a clocking event or id (VIPER 2529)
    if (_clocking_event) {
        f << PrintLevel(level) << PrintToken(VERI_ENDCLOCKING) << endl ;
    }
}

void
VeriConstraintDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // VIPER #6819: Print qualifier extern/pure :
    if (GetQualifier(VERI_EXTERN)) f << PrintToken(VERI_EXTERN) << " " ;
    if (GetQualifier(VERI_PURE)) f << PrintToken(VERI_PURE) << " " ;

    // Print qualifier local/static :
    if (GetQualifier(VERI_STATIC)) f << PrintToken(VERI_STATIC) << " " ;
    if (GetQualifier(VERI_LOCAL)) f << PrintToken(VERI_LOCAL) << " " ;
    if (GetQualifier(VERI_WITH)) f << PrintToken(VERI_WITH) << " " ;
    if (GetQualifier(VERI_DEFAULT)) f << PrintToken(VERI_DEFAULT) << " " ;

    // Print opening token
    f << PrintToken(VERI_CONSTRAINT) << " " ;

    // Print the constraint name
    if (_name) _name->PrettyPrint(f, level) ; f << " " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Print constraint blocks (if there).
    // These will not be there for a proto-type constraint decl.
    if (_constraint_blocks) {
        f << "{ " ;
        // Print the _constraint_blocks. A ";" separated list of expressions :
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_constraint_blocks, i, expr) {
            // VIPER #2074: Always print a semi-colon after every constraint expression
            // excluding 'if' operator. The following routine prints a semicolon for
            // all expressions other than 'if-operator' expression. This is done to
            // make the output legal SV.
            if (expr) expr->PrettyPrintWithSemi(f, level+1) ;
        }
        f << PrintLevel(level) << "} " ;
    } else { // For proto-type constraint decl.
        // Semi and end
        f << ";" ;
    }
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriBindDirective::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print opening token
    f << PrintToken(VERI_BIND) << " " ;

    // Print the target module name :
    if (_target_mod_name) f << _target_mod_name << " : " ;

    // Print the instance list
    PrettyPrintCommaList(_target_inst_list, f, level) ;

    // Print a space to differentiate between the instance list
    // and the instantiation
    f << " " ;

    if (_instantiation) {
        PrettyPrintPragmas(f, level) ; // VIPER #6878
        // Print the instantiation (at level 0, so we don't indent like crasy)
        // Also, since this is a module item itself, the semicolon/newline is already printed
        _instantiation->PrettyPrint(f,0) ;
    } else {
        PrettyPrintPragmas(f, level) ; // VIPER #6878
        f << ";" ;
        PrettyPrintPostComments(f, level, 1) ;
    }
}
void
VeriOperatorBinding::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print opening token
    f << PrintToken(VERI_BIND) << " " ;

    // Print the operator name
    if (_oper_id) f << PrintToken(_oper_id->GetOperatorType()) << " " ;

    // Start function prototype with function keyword
    f << PrintToken(VERI_FUNCTION) << " " ;

    // Print return type of function
    if (_data_type) _data_type->PrettyPrint(f, level) ;

    // Print function name
    if (_func_name) _func_name->PrettyPrint(f, level) ;

    // Print formals of function prototype
    f << "(" ;
    PrettyPrintExprList(_formals, f, level) ;
    f << ") " ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << "; " ;

    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriForeachOperator::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f <<  PrintToken(VERI_FOREACH) << " " ;

    // foreach operator always has parenthesis around array identifier and loop vars.
    f << "(" ;

    // Print array name
    if (_array_name) _array_name->PrettyPrint(f, level) ;

    f << "[" ; // Start of loop vars

    // Print loop index variables
    PrettyPrintCommaList(_loop_indexes, f, level) ;

    f << " ]" ; // End of loop vars
    f << " ) " ;

    // Print constraint set
    if (_constraint_set) _constraint_set->PrettyPrint(f, level) ;

    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriForeachOperator::PrettyPrintWithSemi(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f <<  PrintToken(VERI_FOREACH) << " " ;

    // foreach operator always has parenthesis around array identifier and loop vars.
    f << "(" ;

    // Print array name
    if (_array_name) _array_name->PrettyPrint(f, level) ;

    f << "[" ; // Start of loop vars

    // Print loop index variables
    PrettyPrintCommaList(_loop_indexes, f, level) ;

    f << " ]" ; // End of loop vars
    f << " ) " ;

    // Print constraint set
    if (_constraint_set) _constraint_set->PrettyPrintWithSemi(f, level) ;

    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriNetAlias::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_ALIAS) << " " ;

    if (_left) _left->PrettyPrint(f, level) ; // Print left member

    unsigned i ;
    VeriExpression *member ;
    FOREACH_ARRAY_ITEM(_members, i, member) {
        f << " " << PrintToken(VERI_EQUAL) << " " ; // Print "=" token
        if (member) member->PrettyPrint(f, level) ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << ";" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriTimeUnit::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(_type) << " " ; // Print VERI_TIMEUNIT or VERI_TIMEPRECISION
    if (_literal) _literal->PrettyPrint(f, level) ; // Print time literal

    // VIPER #8304 : Print time precision if present
    if (_time_precision) {
        f << " / " ;
        _time_precision->PrettyPrint(f, level) ; // Print time literal
    }

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << "; " ;

    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriClockingSigDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Print clocking signal declaration
    if (!_ids.Size()) f << PrintToken(VERI_DEFAULT) << " " ; // It is default skew
    if (_clk_dir) _clk_dir->PrettyPrint(f, level) ; // Print clocking direction
    if (_o_clk_dir) _o_clk_dir->PrettyPrint(f, level) ; // Print clocking direction
    // Print id list :
    PrettyPrintCommaList(&_ids, f, level+1) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;

    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriCovergroup::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_COVERGROUP) << " " ; // Print covergroup keyword
    if (_id) _id->PrettyPrint(f, level) ; // Print identifier
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    if (_port_connects) { // Print port list
        f << "(" ;
        PrettyPrintExprList(_port_connects, f, level+1) ;
        f << ") " ;
    }
    if (_event) {
        // Do not print the '@' sign for VERI_STEPCONTROL_OP, since for that the sign is
        // '@@' and it is stored as the edge in the event and will be printed from there:
        //if (!_event->IsEdge(VERI_STEPCONTROL_OP)) f << " @(" ;
        _event->PrettyPrint(f, level) ; // Print event
        // Do not print the closing parenthesis also (already printed in the event):
        //if (!_event->IsEdge(VERI_STEPCONTROL_OP)) f << " )" ;
    }
    if (_sample_func) {
        f << " " << PrintToken(VERI_WITH) << " " ;
        _sample_func->PrettyPrint(f, 0) ;
    } else {
        f << " ;" << endl ;
    }

    // Print coverage spec or option list
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->PrettyPrint(f, level+1) ;
    }
    f << PrintLevel(level) ;
    f << PrintToken(VERI_ENDGROUP) ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriCoverageOption::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_option) _option->PrettyPrint(f, level) ; // Print option
    f << " " << PrintToken(VERI_EQUAL) << " " ;
    if (_value) _value->PrettyPrint(f, level) ; // Print value
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriCoverageSpec::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;
    if (_id) { // Print label if any
        _id->PrettyPrint(f, level) ;
        f << " : " ;
    }
    f << PrintToken(_type) << " "; // Print coverpoint or cross
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    PrettyPrintCommaList(_point_list, f, level) ; // Print cover points

    if (_iff_expr) {
        f << " "<<PrintToken(VERI_IFF) << " ( " ;
        _iff_expr->PrettyPrint(f, level) ; // Print 'iff' expression
        f << " ) " ;
    }

    if (_bins) { // Print bins if exists
        f << " {" << endl ;
        unsigned i ;
        VeriExpression *bin ;
        FOREACH_ARRAY_ITEM(_bins, i, bin) {
            if (bin) bin->PrettyPrint(f, level+1) ;
        }
        f << PrintLevel(level) << " }" ;
    } else {
        f << " ; " ; // No bins specified, print ';'
    }
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriBinDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;
    if (_wildcard) f << PrintToken(_wildcard) << " " ; // Print 'wildcard' keyword
    if (_bins_keyword) f << PrintToken(_bins_keyword) << " " ; // Print bins keyword
    if (_id) _id->PrettyPrint(f, level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ; // Terminate with ';'
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriImportDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;
    f << PrintToken(VERI_IMPORT) << " " ; // Print 'import' keyword

    // Print package import items
    PrettyPrintCommaList(_items, f, level) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ; // Terminate with ';'
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriLetDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    // Keyword
    f << PrintToken(VERI_LET) << " " ;

    // Name
    if (_id) _id->PrettyPrint(f, level) ; f << " " ;

    // Ansi port list
    if (_ansi_io_list) {
        f << "( ";
        PrettyPrintCommaList(_ansi_io_list, f, level+1) ;
        f << " ) ";
    }
    // Expression
    if (_expr) {
        f << "= " ;
        _expr->PrettyPrint(f, level) ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriDefaultDisableIff::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    f << PrintToken(VERI_DEFAULT) << " " ;

    f << PrintToken(VERI_DISABLE) << " " ;
    f << PrintToken(VERI_IFF) << " " ;

    // Expression
    if (_expr_or_dist) _expr_or_dist->PrettyPrint(f, level) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriExportDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    RETURN_IF_TICK_PROTECTED ;

    // Print comments
    PrettyPrintPreComments(f, level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;
    f << PrintToken(VERI_EXPORT) << " " ; // Print 'export' keyword

    // Print package import items
    PrettyPrintCommaList(_items, f, level) ;

    if (!_items) { // null _items means *::*
        f << "*::*" ;
    }

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    f << " ;" ; // Terminate with ';'
    PrettyPrintPostComments(f, level, 1) ;
}

//////////////////////////////////////////////////////////////////////
// PrettyPrint of Comments. Common for every ModuleItem
//////////////////////////////////////////////////////////////////////

void
VeriModuleItem::PrettyPrintPreComments(ostream &f, unsigned level) const
{
    Array *comment_arr = GetComments() ;
    if (!comment_arr) return ;

    // Print the comments that came in before the start of this module item.

    // If USE_STARTING_LINEFILE is not used, then we should look for the first line of the item.
    // For most module items, we can use its own linefile info for that, since they typically are on one line.
    // However, for modules and other items that span multiple lines, the linefile info of the starting line should really be used.
    // For that, use the identifier of the item. That is a good heuristic for the fist line.
    unsigned this_line_no = LineFile::GetLineNo(Linefile()) ;
#ifndef VERILOG_USE_STARTING_LINEFILE_INFO
    VeriIdDef *id = GetId() ;
    if (id) this_line_no = LineFile::GetLineNo(id->Linefile()) ;
#endif
    // Also take the file id of this module item:
    unsigned this_file_id = LineFile::GetFileId(Linefile()) ;

    // now print only the
    unsigned i ;
    VeriCommentNode *node ;
    unsigned is_block_comment = 0 ;
    FOREACH_ARRAY_ITEM(comment_arr, i, node) {
        if (!node) continue ;

        // Always print pre-comments from different files:
        unsigned comment_line_no = node->GetStartingLineNumber() ;
        if ((comment_line_no >= this_line_no) &&
            (this_file_id == LineFile::GetFileId(node->Linefile()))) continue ;

        f << node->GetCommentString() ;
        is_block_comment = node->IsBlockComment() ;
        // If the comment was a line comment, then a newline occurred, and we should output 'level'
        // If it was a block-comment, then we should first start on a new line
        // before printing this next (comment or statement)
        if (is_block_comment) f << endl ;
        // Either way, a newline occurred.
        f << PrintLevel(level) ;
    }
}

void
VeriModuleItem::PrettyPrintPostComments(ostream &f, unsigned level, unsigned print_linefeed /* = 1 */) const
{
    Array *comment_arr = GetComments() ;
    if (!comment_arr) {
        if (print_linefeed) f << endl ;
        return ;
    }

    // Print the comments that came in after the start of this module item.

    // Use the same linenumber calculation as used in PreComments(), otherwise some comments will fall through the cracks.
    unsigned this_line_no = LineFile::GetLineNo(Linefile()) ;
#ifndef VERILOG_USE_STARTING_LINEFILE_INFO
    VeriIdDef *id = GetId() ;
    if (id) this_line_no = LineFile::GetLineNo(id->Linefile()) ;
#endif
    // Also take the file id of this module item:
    unsigned this_file_id = LineFile::GetFileId(Linefile()) ;

    unsigned i ;
    VeriCommentNode *node ;
    unsigned is_block_comment = 1 ;
    FOREACH_ARRAY_ITEM(comment_arr, i, node) {
        if (!node) continue ;

        // Do not print post-comments from different files:
        unsigned comment_line_no = node->GetStartingLineNumber() ;
        if ((comment_line_no < this_line_no) ||
            (this_file_id != LineFile::GetFileId(node->Linefile()))) continue ;

        // If the comment and the statement was in different lines, print them in diferent lines:
        // NOTE: Currently we print a leading newline for the post comments for the modules.
        // May be we should keep track of the line of the last comment printed and check that
        // with the line of this comment instead of checking with the line of 'this'
        if (comment_line_no != this_line_no) {
            // Only print this newline if the last comment was a block comment.
            // Short comments had the newline as the last character in itself:
            if (is_block_comment) f << endl ;
            f << PrintLevel(level) ;
        }
        is_block_comment = node->IsBlockComment() ;
        f << node->GetCommentString() ;
    }
    // If the last comment is a block comment or there is no comment at
    // all then print a new line. The caller will be responsible for
    // printing the level
    if (is_block_comment && print_linefeed) f << endl ;
}
void
VeriPulseControl::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;
    // Print the token 'pulse_control_style' or 'showcancelled'
    if (_pulse_control_style) f << PrintToken(_pulse_control_style) << " " ;
    // Pretty print of the output paths
    PrettyPrintCommaList(_path_list, f, level) ;
    f << " ; " ;
    PrettyPrintPostComments(f, level, 1) ;
}

//--------------------------------------------------------------
// #included from VeriMisc.h
//--------------------------------------------------------------

void
VeriStrength::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "(" << PrintToken(_lval) ;
    if (_rval) {
        /* If rval is not there, this will be a charge_value or single drive_value */
        f << "," << PrintToken(_rval) ;
    }
    f << ")" ;
}

void
VeriNetRegAssign::PrettyPrint(ostream &f, unsigned level) const
{
    if (_lval) _lval->PrettyPrint(f,level) ;
    if (_val) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _val->PrettyPrint(f,level) ;
    }
}

void
VeriDefParamAssign::PrettyPrint(ostream &f, unsigned level) const
{
    if (_lval) _lval->PrettyPrint(f,level) ;
    if (_val) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _val->PrettyPrint(f,level) ;
    }
}

void
VeriCaseItem::PrettyPrint(ostream &f, unsigned level) const
{
    /* Each item starts at a new line */
    f << PrintLevel(level) ;

    if (_conditions && (_conditions->Size() > 0)) {
        // List of expressions :
        PrettyPrintExprList(_conditions, f, level+1) ;
    } else {
        /* 'default' case */
        f << PrintToken(VERI_DEFAULT) ;
    }
    /* Print statement on new line, one level deeper */
    f << " : " << endl ;
    if (_stmt) {
        if (_stmt->IsNullStatement()) {
            f << PrintLevel(level+1) << "; " << endl ;
        } else {
            _stmt->PrettyPrint(f,level+1) ;
        }
        /* VeriStatement already ends with a newline */
    } else {
        /* Print semicolon only (null), and end with a newline (as a normal statement) */
        f << PrintLevel(level+1) << "; " << endl ;
    }
}

void
VeriGenerateCaseItem::PrettyPrint(ostream &f, unsigned level) const
{
    /* Each item starts at a new line */
    f << PrintLevel(level) ;

    if (_conditions) {
        // List of expressions :
        PrettyPrintExprList(_conditions, f, level+1) ;
    } else {
        /* 'default' case */
        f << PrintToken(VERI_DEFAULT) ;
    }
    /* Print statement on new line, one level deeper */
    f << " : " << endl ;
    VeriIdDef *blk_id = (_scope) ? _scope->GetOwner(): 0 ;
    if (blk_id) {
        f << PrintLevel(level+1) ;
        f << PrintToken(VERI_BEGIN) ;
        f << ": " ; blk_id->PrettyPrint(f, level) ; f << endl ;
    }
    if (_item) {
        _item->PrettyPrint(f,(blk_id) ? (level+2): (level+1)) ;
        /* VeriStatement already ends with a newline */
    } else {
        /* Print semicolon only (null), and end with a newline (as a normal statement) */
        f << PrintLevel((blk_id) ? (level+2): (level+1)) << "; " << endl ;
    }
    if (blk_id) {
        f << PrintLevel(level+1) ;
        f << PrintToken(VERI_END) ; f << endl ;
    }
}

void
VeriPath::PrettyPrint(ostream& f, unsigned level) const
{
    int bSimplePath = (_data_source) ? 0 : 1 ;
    int bComplexPath = !bSimplePath ;
    unsigned is_edge_list_path = 0 ;

    VeriExpression *item ; // Make it expression as it is printing expression list
    unsigned i ;
    FOREACH_ARRAY_ITEM(_in_terminals, i, item) {
        if (item && item->IsEdge(0)) {
            is_edge_list_path = 1 ;
            break ;
        }
    }

    f << "(" ;
    if ((bComplexPath || is_edge_list_path) && _edge) f << PrintToken(_edge) << " ";

    PrettyPrintExprList(_in_terminals, f, level+1) ;

    f << " " ;
    if (_polarity_operator) f << PrintToken(_polarity_operator) ;
    f << PrintToken(_path_token) << " " ;

    if (bComplexPath) f << "(" ;
    PrettyPrintExprList(_out_terminals, f, level+1) ;

    if (bComplexPath) {
        f << " " ;
        if (_out_polarity_operator) f << PrintToken(_out_polarity_operator) ;
        f << ": ";
        if (_data_source) _data_source->PrettyPrint(f, level) ;
        f << ")" ;
    }

    f << ") " ;
}

void
VeriDelayOrEventControl::PrettyPrint(ostream &f, unsigned level) const
{
    if (_delay_control) {
        // Issue 2221 : always put parenthesis around the delay expression
        if (_is_cycle_delay) {
            f << "##(" ;
        } else {
            if (_delay_control->IsStep()) {
                f << "#" ; // VIPER #6961 : Donot put '1step' literal within parenthesis
            } else {
                f << "#(" ;
            }
        }
        _delay_control->PrettyPrint(f,level) ;
        if (_delay_control->IsStep()) {
            f << " " ; // VIPER #6961 : Donot put parenthesis
        } else {
            f << ") " ;
        }
    }
    if (_event_control) {
        if (_repeat_event) {
            f << "repeat (" ;
            _repeat_event->PrettyPrint(f,level) ;
            f << ") " ;
        }
        f << "@(" ;

        /* 'or' separated list of expressions */
        unsigned i ;
        VeriExpression *event_expr ;
        FOREACH_ARRAY_ITEM(_event_control, i, event_expr) {
            if (i) f << " or " ;
            if (event_expr) event_expr->PrettyPrint(f,level) ;
        }

        // VIPER #7656: For @(*) we create an empty _event_control. Print so:
        if (!_event_control->Size()) f << "*" ;

        f << ")" ;
    }
}

void
VeriInstanceConfig::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    // Keyword
    f << PrintToken(VERI_INSTANCE) << " " ;

    // Instance name
    if (_inst_ref) _inst_ref->PrettyPrint(f, level) ; f << " " ;

    if (_use_clause) {
         // Config use clause
        _use_clause->PrettyPrint(f, level) ;
    } else {
        // Config liblist
        f << PrintToken(VERI_LIBLIST) << " " ;
        unsigned i ;
        char *lib_name ;
        FOREACH_ARRAY_ITEM(_liblist, i, lib_name) {
            if (!lib_name) continue ;
            f << lib_name << " " ;
        }
    }

    f << ";" << endl ;
}

void
VeriCellConfig::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    // Keyword
    f << PrintToken(VERI_CELL) << " " ;

    // Instance name
    if (_cell_ref) _cell_ref->PrettyPrint(f, level) ; f << " " ;

    if (_use_clause) {
        // Config use clause
        _use_clause->PrettyPrint(f, level) ;
    } else {
        // Config liblist
        f << PrintToken(VERI_LIBLIST) << " " ;
        unsigned i ;
        char *lib_name ;
        FOREACH_ARRAY_ITEM(_liblist, i, lib_name) {
            if (!lib_name) continue ;
            f << lib_name << " " ;
        }
    }

    f << ";" << endl ;
}

void
VeriDefaultConfig::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the indent
    f << PrintLevel(level) ;

    // Keyword
    f << PrintToken(VERI_DEFAULT) << " " ;

    // Config liblist
    f << PrintToken(VERI_LIBLIST) << " " ;
    unsigned i ;
    char *lib_name ;
    FOREACH_ARRAY_ITEM(_liblist, i, lib_name) {
        if (!lib_name) continue ;
        f << lib_name << " " ;
    }

    f << ";" << endl ;
}

void
VeriUseClause::PrettyPrint(ostream &f, unsigned level) const
{
    // Keyword
    f << PrintToken(VERI_USE) << " " ;

    // Cell name reference
    if (_cell_ref) _cell_ref->PrettyPrint(f,level) ;

    // VIPER #7416 : Print parameter value assignment if present
    if (_param_values && (_param_values->Size() > 0 )) {
        f << " #(" ;
        PrettyPrintExprList(_param_values, f, level+1) ;
        f << ") " ;
    }

    // Ending config
    if (_is_config) f << " : " << PrintToken(VERI_CONFIG) ;

    f << " " ;
}

void
VeriClockingDirection::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintToken(_dir) << " " ; // Print direction
    if (_edge) f << PrintToken(_edge) << " " ; // Print edge specification
    if (_delay_control) _delay_control->PrettyPrint(f,level) ; // Print delay control
}

void
VeriProduction::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintLevel(level) ;

    if (_data_type) _data_type->PrettyPrint(f, level) ; // Pretty print the type of production

    if (_id) _id->PrettyPrint(f, level) ; // Pretty print the production id

    // Print the list of formals
    // This is a tf_port_list (comma-separated ansi port decls).
    // Print as such :
    if (_formals) {
        f << " ( ";
        PrettyPrintCommaList(_formals, f, level+1) ;
        f << " )";
    }

    // Print the ":" separator :
    f << " :" << endl ;

    // Print the list of items.
    // This is a | separated list of VeriProductionItem's :
    unsigned i ;
    VeriProductionItem *prod_item ;
    FOREACH_ARRAY_ITEM(_items, i, prod_item) {
        if (i) {
            f << PrintLevel(level + 1) ; // Print the level for indentation
            f <<  "|" << endl ;
        }
        if (prod_item) prod_item->PrettyPrint(f, level + 1) ;
    }
    f << PrintLevel(level + 1) ;
    f << ";" << endl ;
}

void
VeriProductionItem::PrettyPrint(ostream &f, unsigned level) const
{
    // Print rand join
    if (_is_rand_join) {
        f << PrintLevel(level) ;
        f << PrintToken(VERI_RAND) << " " << PrintToken(VERI_JOIN) ;
        if (_expr) {
            (void) PrintLevel(level) ;
            f << "(" ;
            _expr->PrettyPrint(f, level + 1) ; // Print the expression if it exists
            f << ")" ;
        }
        f << endl ; // Print line feed
    }

    // Print the list of production items
    unsigned i;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_items, i, stmt) {
        if (!stmt) continue ;
        stmt->PrettyPrintWOSemi(f, level) ;
        // Print new lines less than total number of production items
        if (_items->Size() > (i + 1)) f << endl ;
    }
    if (!_w_spec)  f << endl ;

    // Print the weight spec
    if (_w_spec) {
        f << PrintToken(VERI_COLON_EQUAL) << " " ;
        _w_spec->PrettyPrint(f, level) ;
        f << endl ; // Print line feed
    }

    // Print the code block
    if (_code_blk) {
        _code_blk->PrettyPrint(f, level) ;
        f << endl ;
    }
}
//--------------------------------------------------------------
// #included from VeriLibrary.h
//--------------------------------------------------------------

void
VeriLibrary::PrettyPrint(ostream& os, unsigned level) const
{
    VeriModule *module ;
    MapIter map_iter ;
    // VIPER #6141: Print non-root-module packages first, they are independent, since they cannot use the items declared in root-module ($unit):
    FOREACH_MAP_ITEM(_module_table, map_iter, 0, &module) if (module && module->IsPackage() && !module->IsRootModule()) module->PrettyPrint(os, level) ;
    // VIPER #6141: Now print the root-module ($unit) and the modules, these can use items declared in root-module ($unit) and the packages:
    FOREACH_MAP_ITEM(_module_table, map_iter, 0, &module) {
        if (module && (module->IsRootModule() || !module->IsPackage())) {
            // This is either a root module or a normal module:
            module->PrettyPrint(os, level) ;
        }
    }
}

void VeriLibraryDecl::PrettyPrint(ostream &/*os*/, unsigned /*level*/) const { } // Print nothing

//--------------------------------------------------------------
// #included from VeriId.h
//--------------------------------------------------------------

void
VeriIdDef::PrettyPrint(ostream &f, unsigned level) const
{
    // NOTE: The following function VeriNode::PrintIdentifier is now the primary
    // way to pretty print identifiers.  This function does a hierarchical check
    // and escaped identifier check, and prints accordingly.
    // VIPER #5727 : If this name is created for unnamed generate block, use
    // original name instead of Name()
    const char *name = Name() ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        if (GetOrigName()) name = GetOrigName() ;
    }
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK

    // VIPER #5903: Print the comments for this identifier.
    PrettyPrintPreComments(f, level) ;
    PrintIdentifier(f, name) ;
    PrettyPrintPostComments(f, level, 1) ;
    //PrettyPrintPragmas(f, level, 1 /* print attributes too, as pragma */) ;
    PrettyPrintPragmas(f, level) ;
}

void
VeriVariable::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the id name with optional initial value and dimensions
    VeriIdDef::PrettyPrint(f, level) ; // Print _name
    if (_dimensions) {
        f << " " ;
        // Print all dimensions (list of ranges)
        f << "[" ;
        _dimensions->PrettyPrint(f,level) ;
        f << "]" ;
    }
    if (_initial_value) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _initial_value->PrettyPrint(f, level) ;
    }
}

void
VeriInstId::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the id name with optional initial value and dimensions
    if (_name) {
        VeriIdDef::PrettyPrint(f, level) ; // Print _name
        f << " " ;
    }

    if (_dimensions) {
        f << "[" ;
        _dimensions->PrettyPrint(f,level) ;
        f << "]" ;
        f << " " ;
    }

    // Port connection list ()'s are always printed. Even when assoc list empty
    f << "(" ;
    // Check me : could print as 'ExprList' to avoid the extra space behind ", "..
    PrettyPrintCommaList(&_port_connects, f, level+1) ;
    f << ")" ;
}

void VeriModuleId::PrettyPrint(ostream &f, unsigned level) const        { VeriIdDef::PrettyPrint(f, level) ; }
void VeriConfigurationId::PrettyPrint(ostream &f, unsigned level) const { VeriModuleId::PrettyPrint(f, level) ; }
void VeriUdpId::PrettyPrint(ostream &f, unsigned level) const           { VeriModuleId::PrettyPrint(f, level) ; }
void VeriTaskId::PrettyPrint(ostream &f, unsigned level) const          { VeriIdDef::PrettyPrint(f, level) ; }
void VeriFunctionId::PrettyPrint(ostream &f, unsigned level) const      { VeriIdDef::PrettyPrint(f, level) ; }

void
VeriGenVarId::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the id name with initial value
    VeriIdDef::PrettyPrint(f, level) ; // Print _name
    if (_initial_value) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _initial_value->PrettyPrint(f, level) ;
    }
}

void
VeriParamId::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the id name with initial value
    VeriIdDef::PrettyPrint(f, level) ; // Print _name

    // Print the dimensions
    if (_dimensions) {
        f << " [" ;
        _dimensions->PrettyPrint(f,level) ;
        f << "]" ;
    }
    // Print initial value
    if (_initial_value) {
        f << " " << PrintToken(VERI_EQUAL) << " " ;
        _initial_value->PrettyPrint(f, level) ;
    }
}

void VeriBlockId::PrettyPrint(ostream &f, unsigned level) const         { VeriIdDef::PrettyPrint(f, level) ; }
void VeriInterfaceId::PrettyPrint(ostream &f, unsigned level) const     { VeriModuleId::PrettyPrint(f, level) ; }
void VeriProgramId::PrettyPrint(ostream &f, unsigned level) const       { VeriModuleId::PrettyPrint(f, level) ; }
// VIPER #5710: (vcs_compatibility: always_checker) PrettyPrint for checker identifier:
void VeriCheckerId::PrettyPrint(ostream &f, unsigned level) const       { VeriModuleId::PrettyPrint(f, level) ; }
void VeriTypeId::PrettyPrint(ostream &f, unsigned level) const          { VeriVariable::PrettyPrint(f, level) ; }
void VeriModportId::PrettyPrint(ostream &f, unsigned level) const       { VeriIdDef::PrettyPrint(f, level) ; }
void VeriOperatorId::PrettyPrint(ostream &f, unsigned level) const      { VeriIdDef::PrettyPrint(f, level) ; }
void VeriClockingId::PrettyPrint(ostream &f, unsigned level) const      { VeriIdDef::PrettyPrint(f, level) ; }
void VeriProductionId::PrettyPrint(ostream &f, unsigned level) const    { VeriIdDef::PrettyPrint(f, level) ; }
void VeriLetId::PrettyPrint(ostream &f, unsigned level) const    { VeriIdDef::PrettyPrint(f, level) ; }

void VeriNamedPort::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintToken(VERI_DOT) ;
    VeriIdDef::PrettyPrint(f, level) ; // Print port name
    f << "(" ;
    if (_port_expr) _port_expr->PrettyPrint(f, 0) ; // Print port expression
    f << ") " ;
}
void VeriPrototypeId::PrettyPrint(ostream &f, unsigned level) const
{
    if (_actual_name) {
        _actual_name->PrettyPrint(f, level) ;
    } else {
        VeriIdDef::PrettyPrint(f, level) ;
    }
}

//--------------------------------------------------------------
// #included from VeriExpression.h
//--------------------------------------------------------------

void
VeriIdRef::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    PrintIdentifier(f, (_name) ? _name : _id->Name()) ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriIndexedId::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    if (_idx) {
        f << "[" ;
        _idx->PrettyPrint(f,level) ;
        f << "]" ;
    }
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriSelectedName::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // Pretty-print the prefix :
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    // Print a "." separator
    if (_prefix && _suffix) f << PrintToken(VERI_DOT) ;
    // Print the suffix name :
    if (_suffix) {
        // VIPER #2917 & #4961: If the suffix came from a token just print it:
        if (_suffix_is_token) {
            f << _suffix ;
        } else {
            PrintIdentifier(f, _suffix) ;
        }
    }
    // Print comments if there.
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriIndexedMemoryId::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_prefix) _prefix->PrettyPrint(f, level) ;

    unsigned i ;
    VeriExpression *index ;
    FOREACH_ARRAY_ITEM(_indexes, i, index) {
        f << "[" ;
        if (index) index->PrettyPrint(f, level) ;
        f << "]" ;
    }
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriIndexedExpr::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;

    // Pretty-print the prefix:
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    // Pretty-print the index
    if (_idx) {
        f << "[" ;
        _idx->PrettyPrint(f,level) ;
        f << "]" ;
    }

    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriScopeName::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // Pretty-print the prefix :
    if (_prefix) _prefix->PrettyPrint(f,level) ;
    // Print a "::" separator when _suffix exists or _suffix and params not exists i.e p::*
    if (_suffix || !_param_values) f << PrintToken(VERI_COLON_COLON) ;
    // Print the suffix name :
    if (_suffix) {
        // VIPER #2917 & #4961: If the suffix came from a token just print it:
        if (_suffix_is_token) {
            f << _suffix ;
        } else {
            // VIPER #5386 (test_12): Print the suffix-id name if it is available:
            // FIXME: We should have set the _suffix from _suffix_id in Copy routine
            // but could not do that because we create structures that, if set _suffix
            // from _suffix_id will generate wrong pretty printed output for selected
            // name. So, change only scope name pretty print here, not the selected name.
            PrintIdentifier(f, (_suffix_id) ? _suffix_id->Name() : _suffix) ;
        }
    } else if (!_param_values) {
        f << " *" ; // No _suffix and no _param_values means wildcard import
    }

    if (_param_values && (_param_values->Size() > 0 )) {
        f << " #(" ;
        PrettyPrintExprList(_param_values, f, level+1) ;
        f << ") " ;
    }
    if (_modport_name) {
        f << "."  ;
        _modport_name->PrettyPrint(f, level) ;
    }
    // Print comments if there.
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriKeyword::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f << PrintToken(_token) ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriConcat::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f << "{" ;
    PrettyPrintExprList(_exprs, f, level+1) ;
    f << "}" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriMultiConcat::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f << "{" ;
    if (_repeat) _repeat->PrettyPrint(f,level) ;
    f << "{" ;
    PrettyPrintExprList(_exprs, f, level+1) ;
    f << "}}" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriFunctionCall::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_func_name) _func_name->PrettyPrint(f,level) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_args) {
        f << "(" ;
        PrettyPrintExprList(_args, f, level+1) ;
        f << ")" ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriSystemFunctionCall::PrettyPrint(ostream &f, unsigned level) const
{
    // System function call names can't be escaped or hierarchical, so don't call VeriNode::PrintIdentifier.
    f << "$" << _name ;
    if (_args) {
        f << "(" ;
        PrettyPrintExprList(_args, f, level+1) ;
        f << ")" ;
    }
}

void
VeriMinTypMaxExpr::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f << "(" ;
    if (_min_expr) _min_expr->PrettyPrint(f,level) ;
    f << ":" ;
    if (_typ_expr) _typ_expr->PrettyPrint(f,level) ;
    f << ":" ;
    if (_max_expr) _max_expr->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriUnaryOperator::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;

    // VIPER #2588: Need special handling of these unary operators:
    switch (_oper_type) {
    case VERI_FIRST_MATCH:
    case VERI_STRONG :
    case VERI_WEAK :
        f << PrintToken(_oper_type) ;
        f << "(" ;
        if (_arg) _arg->PrettyPrint(f,level) ;
        f << ")" ;
        return ;
    case VERI_NOT:
    case VERI_BEGIN:
    case VERI_END:
        f << "(" ;
        f << PrintToken(_oper_type) ;
        f << " " ;
        if (_arg) _arg->PrettyPrint(f,level) ;
        f << ")" ;
        return ;
    case VERI_SOFT: // VIPER #8056: for keyword soft
        f << PrintToken(_oper_type) << " " ; // print keyword 'soft'
        if (_arg) _arg->PrettyPrint(f,level) ; // print left operand
        return ;
    case VERI_DISABLE: // VIPER #8056: print 'disable' for disable soft
        // Special handling for disable soft operator
        f << PrintToken(_oper_type) << " soft " ; // print operator 'disable soft'
        if (_arg) _arg->PrettyPrint(f,level) ; // print left operand
        return;
    default:
        break ;
    }

    // VIPER #2588: Parenthesize the whole unary operator:
    f << "(" ;
    f << PrintToken(_oper_type) ;
    PrettyPrintPostComments(f, level, 1) ;

    // Print attributes (if they exist)
    Map *attributes = GetAttributes() ;
    if (attributes) f << " " ;
    PrettyPrintAttributes(f, level) ;
    if (attributes) f << " " ;

    if (_arg) _arg->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
}

void
VeriBinaryOperator::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned print_parenthesized = 1 ; // by default, print ()'s around this call.

    PrettyPrintPreComments(f, level) ;

    if (_oper_type == VERI_DISABLE) {
        // Special handling for disable iff operator
        f << PrintToken(_oper_type) << " iff " ; // print operator 'disable iff'
        f << "( " ;
        if (_left) _left->PrettyPrint(f,level) ; // print left operand
        f << ") " ;
        PrettyPrintPostComments(f, level, 1) ;
        // Print attributes (if they exist)
        PrettyPrintAttributes(f, level) ;
        if (_right) _right->PrettyPrint(f,level) ; // print right operand
        PrettyPrintPragmas(f, level) ; // VIPER #6878
        return ;
    }
    // VIPER #5710 : VCS Compatibility issue (if_in_property) : Abort properties
    switch (_oper_type) {
    case VERI_ACCEPT_ON :
    case VERI_REJECT_ON :
    case VERI_SYNC_ACCEPT_ON :
    case VERI_SYNC_REJECT_ON :
        // Print the token first
        f << PrintToken(_oper_type) ;
        // Print condition
        f << "( " ;
        if (_left) _left->PrettyPrint(f,level) ;
        f << ") " ;
        // Print property expression
        if (_right) _right->PrettyPrint(f,level) ;
        return ;
    // VIPER #5710 : VCS Compatibility issue (if_in_property) : nexttime properties
    case VERI_NEXTTIME :
    case VERI_S_NEXTTIME :
    case VERI_EVENTUALLY :
    case VERI_S_EVENTUALLY :
    case VERI_ALWAYS :
    case VERI_S_ALWAYS :
        f << PrintToken(_oper_type) ;
        if (_left) {
            f << "[ " ;
            _left->PrettyPrint(f,level) ;
            f << "] " ;
        }
        f << "(" ;
        if (_right) _right->PrettyPrint(f,level) ;
        f << ")" ;
        return ;
    default :
        break ;
    }

    // The following operators should NOT be printed with parenthesis, unless they were explicitly parenthesized.
    switch (_oper_type) {
        // constraints (CHECK ME : not sure if these are still used, and in this way) :
    case VERI_RIGHTARROW :
    case VERI_EDGE_AMPERSAND :
    case VERI_MATCHES :
        // The sequence/property operators :
    case VERI_OR :
    case VERI_SEQ_OR :
    case VERI_AND :
    case VERI_SEQ_AND :
    case VERI_PROPERTY_OR :
    case VERI_PROPERTY_AND :
    case VERI_CONSECUTIVE_REPEAT :
    case VERI_NON_CONSECUTIVE_REPEAT :
    case VERI_GOTO_REPEAT :
    case VERI_OVERLAPPED_IMPLICATION :
    case VERI_NON_OVERLAPPED_IMPLICATION :
    case VERI_OVERLAPPED_FOLLOWED_BY :
    case VERI_NON_OVERLAPPED_FOLLOWED_BY :
    case VERI_INTERSECT :
    case VERI_THROUGHOUT :
    case VERI_WITHIN :
    case VERI_SEQ_SP_AND : // For testing
    case VERI_SEQ_FM_AND : // For testing
        print_parenthesized = IsParenthesized() ;
        break ;
    default : break ;
    }
    // Parenthesize the call to clarify precedence
    if (print_parenthesized) f << "(" ;
    if (_left) _left->PrettyPrint(f,level) ;
    if ((_oper_type == VERI_CONSECUTIVE_REPEAT) && _right && _right->IsRange() &&
        !_right->GetLeft() && !_right->GetRight()) {
        // VIPER #6329 & #6357: Special consecutive repeat with empty range,
        // Do not print the token, it will be printed from the range itself:
        f << " [" ;
    } else // else part contains only a single line below
    f << " " << PrintToken(_oper_type) << " " ;
    PrettyPrintPostComments(f, level, 1) ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_right) _right->PrettyPrint(f,level) ;

    PrettyPrintPragmas(f, level) ; // VIPER #6878

    // Print the closing tokens for these 'repeat' SV binary operators
    switch(_oper_type) {
    case VERI_CONSECUTIVE_REPEAT    :
    case VERI_NON_CONSECUTIVE_REPEAT: // This is an intentional fall through
    case VERI_GOTO_REPEAT : f << "]"; break ;
    default : break ;
    }

    if (print_parenthesized) f << ")" ;
}

void
VeriBinaryOperator::PrettyPrintWithSemi(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_oper_type != VERI_RIGHTARROW) {
        // Call the normal PrettyPrintWithSemi routine for non-implication operators:
        VeriExpression::PrettyPrintWithSemi(f, level) ;
        return ;
    }

    // Here it it VERI_RIGHTARROW which is '->' (implication operator).
    if (_left) _left->PrettyPrint(f,level) ;
    f << " " << PrintToken(_oper_type) << " " ;
    PrettyPrintPostComments(f, level, 1) ;
    PrettyPrintAttributes(f, level) ; // Print attributes (if they exist)
    if (_right) _right->PrettyPrintWithSemi(f,level) ; // Call the PrettyPrintWithSemi routine
    PrettyPrintPragmas(f, level) ; // VIPER #6878
}

void
VeriQuestionColon::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    /* Parenthesize the result to get precedence right */
    f << "(" ;
    if (_if_expr) {
        if (_if_expr->IsCondPredicate() || _if_expr->IsPatternMatch()) f << "(" ; // Parenthesize condition if it is patern matching
        _if_expr->PrettyPrint(f,level) ;
        if (_if_expr->IsCondPredicate() || _if_expr->IsPatternMatch()) f << ")" ; // Parenthesize condition if it is patern matching
    }
    f << " ? " ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_then_expr) _then_expr->PrettyPrint(f,level) ;
    f << " : " ;
    if (_else_expr) _else_expr->PrettyPrint(f,level) ;
    f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriEventExpression::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintToken(_edge) << " " ;
    if (_expr) _expr->PrettyPrint(f,level) ;
    if (_iff_condition) {
        f << " iff " ;
        _iff_condition->PrettyPrint(f,level) ;
    }
}

void
VeriPortConnect::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_port_ref_name) {
        f << "." ; PrintIdentifier(f, _port_ref_name) ; f << "(" ;
    }
    if (_connection) {
        _connection->PrettyPrint(f,level) ;
    }
    // named connection always has ()
    if (_port_ref_name) f << ")" ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriPortOpen::PrettyPrint(ostream &f, unsigned level) const
{
    // Verilog prints nothing for an open port except attributes

    PrettyPrintPreComments(f, level) ;
    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriAnsiPortDecl::PrettyPrint(ostream &f, unsigned level) const
{
    // Start on a newline :
    f << endl ;
    f << PrintLevel(level) ;

    // Print attributes (if they exist)
    PrettyPrintPreComments(f, level) ;
    PrettyPrintAttributes(f, level) ;
    if (_is_local) f << PrintToken(VERI_LOCAL) << " " ;

    // VIPER #5661 : Print direction before qualifiers
    // Print direction
    if (_dir) f << PrintToken(_dir) << " " ;

    if (_qualifier && (_qualifier != VERI_VIRTUAL)) f << PrintToken(_qualifier) << " " ; // Print 'var' or 'virtual' keyword

    // VIPER 3267 : direction 'const' means 'const ref'
    if (_dir==VERI_CONST) f << PrintToken(VERI_REF) << " " ;

        // Print data type :
        if (_data_type) _data_type->PrettyPrint(f,level) ;
    // Print the data type
    //if (_data_type) _data_type->PrettyPrint(f, level) ;

    // Print the identifiers
    // VIPER #2949: Cannot use 'PrettyPrintCommaList' since we don't want to print the initial values of the ids:
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        // Print a comma separator between the identifiers
        if (i) f << ", " ;

        // Issue 2135 : cap the number of items printed on a single line
        if (i && ((i%MAX_COMMA_LIST_COUNT)==0)) f << endl << PrintLevel(level+1) ;

        if (!id) continue ;

        // VIPER Issue 2222 and 1634 : the initial value should only be printed once,
        // even though the identifier can show up in two 'declarations' (one as a port decl, one as a reg/net decl).
        // or only as one (port decl OR net/reg decl). This (possible double-decl) is only the case for ports. Not for other ids.
        // Print the initial value only on the decl which defines the data-type set as back-pointer to the id.
        // That back-pointer is always set, to a single data decl, so we are cool :
        // Initial value will always get printed, and printed only once :
        if (id->GetInitialValue() && (id->GetDataType()!=_data_type)) {
            // There is an initial value on this port, but the id's datatype is not pointer back to me.
            // So print only the identifier name. not the initial value :
            PrintIdentifier(f, id->GetName()) ;
            VeriRange *dimensions = id->GetDimensions() ;
            // Print the dimensions if this is a memory id:
            if (dimensions) {
                f << " [" ;
                dimensions->PrettyPrint(f,level+1) ;
                f << "]" ;
            }
            continue ;
        }

        id->PrettyPrint(f,level+1) ;
    }

    PrettyPrintPragmas(f, level) ; // VIPER #6878
    // Print comments
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriTimingCheckEvent::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    if (_edge) {
        f << PrintToken(_edge) << " " ;
        // Print edge control specifiers
        if (_edge_desc_str) {
            f << "[" << _edge_desc_str << "] " ;
        }
    }
    if (_terminal_desc) _terminal_desc->PrettyPrint(f, 0) ;

    if (_condition) {
        f << " " << PrintToken(VERI_EDGE_AMPERSAND) << " " ;
        _condition->PrettyPrint(f, 0) ;
    }
}

void
VeriRange::PrettyPrint(ostream &f, unsigned level) const
{
    // []'s must be printed at the caller level,
    // because a Range is now a VeriExpression (and indexed name pretty-print already print the []'s)
    // f << "[" ;
    if (_left) _left->PrettyPrint(f,level) ;

    if (!_part_select_token) {
        f << ":" ; // classical Verilog 95 range
    } else if (_part_select_token == VERI_NONE) {
        // don't print anything
    } else if ((_part_select_token == VERI_PLUS) || (_part_select_token == VERI_MUL)) {
        // VIPER #6329 & #6357: Do not print spaces, these are special ranges used in sequence expression:
        f << PrintToken(_part_select_token) ;
    } else {
        // Verilog 2000 part-select and System Verilog other tokens..
        // Historically, we separate the token with spaces on left/right side,
        // to avoid syntax problems with adjacent tokens.
        f << " " << PrintToken(_part_select_token) << " " ;
    }
    if (_right) _right->PrettyPrint(f,level) ;

    if (_next) {
        // Print the next range, but first close previous and open new :
        f << "][" ;
        _next->PrettyPrint(f,level) ;
    }
    // f << "]" ;
}

void
VeriDataType::PrettyPrint(ostream &f, unsigned level) const
{
    // print type, signing and dimensions, in that order :
    if (_type) f << PrintToken(_type) << " " ;
    if (_signing) f << PrintToken(_signing) << " " ;

    if (_dimensions) {
        f << "[" ;
        _dimensions->PrettyPrint(f, level) ;
        f << "] " ;
    }
}
void
VeriNetDataType::PrettyPrint(ostream &f, unsigned level) const
{
    // Print net type
    if (_type) f << PrintToken(_type) << " " ;
    if (_data_type) _data_type->PrettyPrint(f, level) ;
}
// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
void
VeriPathPulseVal::PrettyPrint(ostream &f, unsigned level) const
{
    if (!_reject_limit) return ; // Return from function as no reject limit means no error limit as well.

    if (_error_limit) f << "(" ;
    _reject_limit->PrettyPrint(f, level) ;

    if (_error_limit) {
        f << "," ;
        _error_limit->PrettyPrint(f, level) ;
        f << ")" ;
    }
}

// System Verilog additions

void
VeriTypeRef::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // VCS Compatibility issue (virtual_interface) : Print virtual keyword
    // from this data type :
    if (_virtual_token) f << PrintToken(_virtual_token) << " " ;

    // print type, signing and dimensions, in that order :
    if (_type_name) _type_name->PrettyPrint(f,level) ;
    f << " " ;
    PrettyPrintPostComments(f, level, 1) ;

    // Actually _signing is not allowed on named type references..
    if (_signing) f << PrintToken(_signing) << " " ;

    if (_dimensions) {
        f << "[" ;
        _dimensions->PrettyPrint(f, level) ;
        f << "] " ;
    }
}

void
VeriStructUnion::PrettyPrint(ostream &f, unsigned level) const
{
    // First print the '_type' (STRUCT or UNION)
    f << PrintToken(_type) << " " ;

    // Print 'tagged' qualifier
    if (_is_tagged) f << PrintToken(VERI_TAGGED) << " " ;

    // Print 'packed' qualifier
    if (_is_packed) f << PrintToken(VERI_PACKED) << " " ;

    // Print signing :
    if (_signing) f << PrintToken(_signing) << " " ;

    // Print the declarations in {}'s. Indent level, because the members (module items) will start on newlines.
    f << "{" << endl ;
    unsigned i ;
    VeriDataDecl *decl ;
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        if (!decl) continue ;
        decl->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) ;
    f << "} " ;

    // Print the dimensions :
    if (_dimensions) {
        f << "[" ;
        _dimensions->PrettyPrint(f,level) ;
        f << "] " ;
    }
    // always leave a space at the end. After all, this is a expression item.
}

void
VeriEnum::PrettyPrint(ostream &f, unsigned level) const
{
    // First print the enum token :
    f << PrintToken(VERI_ENUM) << " " ;

    // Now print the base data type (type, signing, dimensions) :
    if (_base_type) _base_type->PrettyPrint(f,level) ;

    // then print the enum-ids in {}s'.
    f << "{ " ;
    PrettyPrintCommaList(_ids, f, level+1) ;
    f << "} " ;

    // VIPER #5710: (vcs_compatibility-9000388573): Enum type may have packed dimensions
    // Print the dimensions :
    if (_dimensions) {
        f << "[" ;
        _dimensions->PrettyPrint(f,level) ;
        f << "] " ;
    }
    // always leave a space at the end. After all, this is a expression item.
}

void
VeriDotStar::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << PrintToken(VERI_DOTSTAR) << " " ;
}

void
VeriIfOperator::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f << PrintToken(VERI_IF) << " " ;

    // IF operator always has parenthesis around if expression.
    f << "(" ;
    if (_if_expr) _if_expr->PrettyPrint(f,level) ;
    f << ") " ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;
    PrettyPrintPragmas(f, level) ; // VIPER #6878

    if (_then_expr) _then_expr->PrettyPrint(f,level) ;

    f << " " ; // Print an extra space for proper distinction of the else token
    if (_else_expr) {
        // Terminate the previous constraint set before printing an 'else'

        // RD : IfOperator for 'property' should NOT have any semicolon.
        // IfOPerator for constraint set should.
        // We don't know which one this is. Assume it is a (SVA) 'property' if.
        // FIX ME : constraint-set 'if' should get its own class...
        // f << " ; " ;
        f << PrintToken(VERI_ELSE) << " " ;
        _else_expr->PrettyPrint(f,level) ;
    }

    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriSequenceConcat::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // Parenthesize the result to clarify precedence.
    // VIPER 2582/5118 : SequenceConcat (derived from binary operator) is now equipped with IsParenthesized() flag :
    unsigned print_parenthesis = IsParenthesized() ;
    if (print_parenthesis) f << "(" ;
    if (_left) _left->PrettyPrint(f,level) ;

    f << " " << PrintToken(_oper_type) ; // This will be '##'

    // Print the cycle delay range right after the token :
    if (_cycle_delay_range) {
        // This can be an expression (print ()'s) or a range (print []'s).
        // Test here :
        if (_cycle_delay_range->IsRange()) {
            f << "[" ;
            _cycle_delay_range->PrettyPrint(f,level) ;
            f << "]" ;
        } else {
            // Braces must enclose system function calls.
            // ##$countones(a) is syntax error! ##($countones(a)) is not.
            f << "(" ;
            _cycle_delay_range->PrettyPrint(f,level) ;
            f << ")" ;
        }
    }
    f << " " ;

    PrettyPrintPostComments(f, level, 1) ;

    if (_right) _right->PrettyPrint(f,level) ;

    if (print_parenthesis) f << ")" ;
}

void
VeriClockedSequence::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // Parenthesize the result to clarify clock flow.
    // VIPER 2582/5118 : ClockedSequence (derived from unary operator) is now equipped with IsParenthesized() flag :
    unsigned print_parenthesis = IsParenthesized() ;
    if (print_parenthesis) f << "(" ;

    //f << PrintToken(_oper_type) ; // Will be "@"

    PrettyPrintPostComments(f, level, 1) ;

    // Print the event expression right behind it :
    if (_event_expr) {
        //f << "(" ;
        _event_expr->PrettyPrint(f, level) ;
        //f << ")" ;
    }
    f << " " ;

    if (_arg) _arg->PrettyPrint(f,level) ;

    if (print_parenthesis) f << ")" ;
}

void
VeriAssignInSequence::PrettyPrint(ostream &f, unsigned level) const
{
    // Parenthesize the result (it was parsed within parenthesis, see bison file).
    f << "(" ;
    // First print the expression :
    if (_arg) _arg->PrettyPrint(f,level) ;

    if (_stmts) f << "," ; // there is always a comma between the arg and the statements (if they are there)

    // Then print the statements (Without the semicolons and without the newlines)
    PrettyPrintCommaList(_stmts, f, level+1) ;
    f << ")" ;
}

void
VeriDistOperator::PrettyPrint(ostream &f, unsigned level) const
{
    // Don't parenthesize dist operator..

    // First print the expression :
    if (_arg) _arg->PrettyPrint(f,level) ;

    // Print the token :
    f << " " << PrintToken(VERI_DIST) << " " ;

    // Then print the dist items in {}'s
    f << "{ " ;
    unsigned i ;
    VeriExpression *dist_item ;
    FOREACH_ARRAY_ITEM(_dist_list, i, dist_item) {
        if (!dist_item) continue ;
        if (i) f << "," ; // It's a comma-separated list..
        // VIPER #2074: Print the left and the right dist items from here for these two operators
        switch(dist_item->OperType()) { // Check the operator type
        case VERI_COLON_EQUAL  :
        case VERI_COLON_NEQUAL :
        {
            VeriExpression *left = dist_item->GetLeft() ;
            VeriExpression *right = dist_item->GetRight() ;
            if (left) { // If of type VeriRange then print '[' ']'
                if (left->IsRange()) {
                    f << "[" ;
                    left->PrettyPrint(f, level) ;
                    f << "]" ;
                } else {
                    left->PrettyPrint(f, level) ;
                }
            }
            f << " " ;
            f << PrintToken(dist_item->OperType()) ;
            f << " " ;
            if (right) right->PrettyPrint(f, level) ;
            break ;
        }
        default :
            dist_item->PrettyPrint(f, level+1) ;
            break ;
        }
    }
    f << "}" ;
}

void
VeriSolveBefore::PrettyPrint(ostream &f, unsigned level) const
{
    // Print the solve token :
    f << PrintToken(VERI_SOLVE) << " " ;

    // Print the solve_list (comma-separated) :
    // List of expressions :
    PrettyPrintExprList(_solve_list, f, level+1) ;

    // Print the before token :
    f << " " << PrintToken(VERI_BEFORE) << " " ;

    // Print the before list (comma-separated) :
    // List of expressions :
    PrettyPrintExprList(_before_list, f, level+1) ;
}

void
VeriCast::PrettyPrint(ostream &f, unsigned level) const
{
    // Print target type :
    if (_target_type) _target_type->PrettyPrint(f,level) ;
    // ' token :
    f << "'" ;
    // and the expression (parenthesized if not a concat?) :
    f << "(" ;
    if (_expr) _expr->PrettyPrint(f,level) ;
    f << ")" ;
}

void
VeriNew::PrettyPrint(ostream &f, unsigned level) const
{
    // Print 'new' token :
    f << PrintToken(VERI_NEW) << " " ;

    // Print 'size' expression :
    if (_size_expr) {
        f << "[" ;
        _size_expr->PrettyPrint(f,level) ;
        f << "] " ;
    }

    // print the arguments :
    if (_args) {
        // VIPER #3793: Print the parenthesis only if it is not a copy constructor:
        if (!_copy_constructor) f << "(" ;
        // List of expressions :
        PrettyPrintExprList(_args, f, level) ;
        if (!_copy_constructor) f << ") " ;
    }
}

void
VeriConcatItem::PrettyPrint(ostream &f, unsigned level) const
{
    // print member label :
    if (_member_label) {
        _member_label->PrettyPrint(f,level) ;
    } else {
        // default label :
        f << PrintToken(VERI_DEFAULT) << " " ;
    }

    f << PrintToken(VERI_COLON) << " " ;

    // print the expression :
    if (_expr) _expr->PrettyPrint(f,level) ;
}

void
VeriNull::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << PrintToken(VERI_NULL) ;
}
void
VeriDollar::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << PrintToken(VERI_DOLLAR) ;
}
void
VeriAssignmentPattern::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_type) _type->PrettyPrint(f, level) ;
    f << "'{" ;
    PrettyPrintExprList(_exprs, f, level+1) ;
    f << "}" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriMultiAssignmentPattern::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_type) _type->PrettyPrint(f, level) ;
    f << "'{" ;
    if (_repeat) _repeat->PrettyPrint(f,level) ;
    f << "{" ;
    PrettyPrintExprList(_exprs, f, level+1) ;
    f << "}}" ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriStreamingConcat::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f << "{ " ;
    f << PrintToken(_stream_operator) ; // Print stream operator
    if (_slice_size) _slice_size->PrettyPrint(f,level) ; // Print slice size
    f << "{" ;
    PrettyPrintExprList(_exprs, f, level+1) ;
    f << "}}" ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriCondPredicate::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // First print the prefix
    if (_left) _left->PrettyPrint(f, level) ;
    // Print '&&&' operator
    f << " " << PrintToken(VERI_EDGE_AMPERSAND) << " " ;
    if (_right) _right->PrettyPrint(f, level) ;
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriDotName::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // It is a pattern .<variable_identifier>
    if (_name) {
        f << PrintToken(VERI_DOT) ;
        _name->PrettyPrint(f, level) ; // Print variable name
    }
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriTaggedUnion::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    // Parenthesize the result to clarify precedence
    //f << "( " << PrintToken(VERI_TAGGED) << " " ;
    f << PrintToken(VERI_TAGGED) << " " ;

    if (_member) _member->PrettyPrint(f, level) ; // Print member name
    f << " " ;
    if (_expr) _expr->PrettyPrint(f, level) ; // Print member value
    //f << ") " ;
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriWithExpr::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    if (_left) _left->PrettyPrint(f, level) ; // Print left hand side
    f << " " << PrintToken(VERI_WITH) << " " ;
    unsigned is_range = (_right && _right->IsRange()) ? 1 : 0 ;
    if (is_range) { // VeriRange does not print []
        f << "[ " ; // Print '[' here
    } else {
        f << "( " ;  // Right hand side parenthesized expression
    }
    if (_right) _right->PrettyPrint(f, level) ; // Print right hand side
    if (is_range) {
        f << "] " ; // Print closing bracket
    } else {
        f << ") " ; // Print closing parenthesis
    }
    PrettyPrintPostComments(f, level, 1) ;
}
void
VeriInlineConstraint::PrettyPrint(ostream &f, unsigned level) const
{
    if (_left) _left->PrettyPrint(f, level) ; // Print left hand side
    // Print constraint block
    if (_constraint_block) {
        // Print with keyword if constraint block exists
        f << " " << PrintToken(VERI_WITH) << " " ;
    }
    unsigned i ;
    VeriExpression *expr ;
    if (_id_refs) {
        // VIPER #6756: PrettyPrint the list of id-refs:
        f << "( " ;
        FOREACH_ARRAY_ITEM(_id_refs, i, expr) {
            if (i) f << ", " ;
            if (expr) expr->PrettyPrint(f, level+1) ;
        }
        f << " ) " ;
    }
    if (_constraint_block) {
        f << "{ " ;
        // Print the _constraint_block. A ";" separated list of expressions :
        FOREACH_ARRAY_ITEM(_constraint_block, i, expr) {
            if (expr) expr->PrettyPrintWithSemi(f, level+1) ;
        }
        f << "} " ;
    }
}

// VIPER #2074: Print semicolon recursively for 'then' and 'else' expressions.
void
VeriIfOperator::PrettyPrintWithSemi(ostream &f, unsigned level) const
{
    PrettyPrintPreComments(f, level) ;
    f <<  PrintToken(VERI_IF) << " " ;

    // IF operator always has parenthesis around if expression.
    f << "(" ;
    if (_if_expr) _if_expr->PrettyPrint(f,level) ;
    f << ") " ;

    // Print attributes (if they exist)
    PrettyPrintAttributes(f, level) ;

    if (_then_expr) _then_expr->PrettyPrintWithSemi(f,level) ;

    if (_else_expr) {
        // Terminate the previous constraint set before printing an 'else'
        f << PrintToken(VERI_ELSE) << " " ;
        _else_expr->PrettyPrintWithSemi(f,level) ;
    }
    PrettyPrintPragmas(f, level) ; // VIPER #6878
    PrettyPrintPostComments(f, level, 1) ;
}

void
VeriExpression::PrettyPrintWithSemi(ostream &f, unsigned level) const // VIPER #2074: Print Semicolon after constraint expression
{
    PrettyPrint(f, level) ; // Called for constraint expressions
    f << " ; " ;
}

void
VeriOpenRangeBinValue::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VeriExpression *val_range ;
    f << "{ " ; // Print the starting bracket for the value range list
    FOREACH_ARRAY_ITEM (_open_range_list, i, val_range) {
        if (i)  f << ", " ; // Print a comma only after a value range
        if (val_range) {
            if (val_range->IsRange()) f << "[ " ;
            val_range->PrettyPrint(f, level) ; // Print the value range
            if (val_range->IsRange()) f << " ]" ;
        }
    }
    f << " }" ; // Print the ending bracket

    // Print the optional 'iff' expression if it is present
    if (_iff_expr) {
        f << " " << PrintToken(VERI_IFF) << " " ;
        f << "( " ;
        _iff_expr->PrettyPrint(f, level) ;
        f << " )" ;
    }
}
void
VeriTransBinValue::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VeriTransSet *trans_set ;
    FOREACH_ARRAY_ITEM (_trans_list, i, trans_set) {
        if (i) f << ", " ; // Print a comma only if the trans_set is preceded by another one
        if (trans_set) trans_set->PrettyPrint(f, level) ; // Print the transition list specified in bin value
    }

    // Print the optional 'iff' expression if it is present
    if (_iff_expr) {
        f << " " << PrintToken(VERI_IFF) << " " ;
        f << "( " ;
        _iff_expr->PrettyPrint(f, level) ;
        f << " )" ;
    }
}
void
VeriDefaultBinValue::PrettyPrint(ostream &f, unsigned level) const
{
    // Print 'default'
    f << PrintToken(VERI_DEFAULT) << " " ;

    // Print 'sequence' if present
    if (_sequence) f << PrintToken(_sequence) ;

    // Print the optional 'iff' expression if it is present
    if (_iff_expr) {
        f << " " << PrintToken(VERI_IFF) << " " ;
        f << "( " ;
        _iff_expr->PrettyPrint(f, level) ;
        f << " )" ;
    }
}
void
VeriSelectBinValue::PrettyPrint(ostream &f, unsigned level) const
{
    if (_select_expr) _select_expr->PrettyPrint(f, level) ; // Print the select expression

    // Print the optional 'iff' expression if it is present
    if (_iff_expr) {
        f << " " << PrintToken(VERI_IFF) << " " ;
        f << "( " ;
        _iff_expr->PrettyPrint(f, level) ;
        f << " )" ;
    }
}
void
VeriTransSet::PrettyPrint(ostream &f, unsigned level) const
{
    f << "( " ; // Print openning bracket for trans set
    unsigned i ;
    VeriTransRangeList *range_list ;
    FOREACH_ARRAY_ITEM (_trans_range_list, i, range_list) {
        if (i) f << " " << PrintToken(VERI_LEADTO) << " " ;
        if (range_list) range_list->PrettyPrint(f, level) ; // Print the transition list specified in bin value
    }
    f <<" )" ; // Print closing bracket for trans set
}
void
VeriTransRangeList::PrettyPrint(ostream &f, unsigned level) const
{
    unsigned i ;
    VeriExpression *val_range ; // Print a comma only after a value range
    FOREACH_ARRAY_ITEM (_trans_item, i, val_range) {
        if (i) f << ", " ;
        if (val_range) {
            if (val_range->IsRange()) f << "[ " ;
            val_range->PrettyPrint(f, level) ; // Print the value range
            if (val_range->IsRange()) f << "] " ;
        }
    }
    if (_repetition)  _repetition->PrettyPrint(f, level) ; // Print the repetition of transition item
}
void
VeriSelectCondition::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintToken(VERI_BINSOF) << "( " ;
    if (_bins_expr) _bins_expr->PrettyPrint(f, level) ;
    f << " ) " ;
    if (_range_list) f << PrintToken(VERI_INTERSECT) << "{ " ; // Print 'intersect' only if value range is present
    unsigned i ;
    VeriExpression *val_range ;
    FOREACH_ARRAY_ITEM (_range_list, i, val_range) {
        if (i)  f << ", " ; // Print a ',' after a value range in the list
        if (!val_range) continue ;
        if (val_range->IsRange()) f << "[ " ;
        val_range->PrettyPrint(f, level) ; // Print the value range
        if (val_range->IsRange()) f << " ]" ;
    }
    if (_range_list) f << " } " ;
}
void
VeriTypeOperator::PrettyPrint(ostream &f, unsigned level) const
{
    f << PrintToken(VERI_TYPE) << "( " ;
    if (_expr_or_data_type) _expr_or_data_type->PrettyPrint(f, level) ;
    f << " ) " ;
}
void
VeriPatternMatch::PrettyPrint(ostream &f, unsigned level) const
{
    if (_left) _left->PrettyPrint(f, level) ;
    f << " " << PrintToken(VERI_MATCHES) << " " ;
    if (_right) _right->PrettyPrint(f, level) ;
}
void
VeriConstraintSet::PrettyPrint(ostream &f, unsigned level) const
{
    f << "{" ;
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        f << " " ;
        expr->PrettyPrintWithSemi(f, level) ;
    }
    f << " } " ;
}

//////////////////////////////////////////////////////////////////////
// Pretty Print VeriExpression base-class routines
//////////////////////////////////////////////////////////////////////

// Pretty-print attributes

void
VeriTreeNode::PrettyPrintAttributes(ostream &f, unsigned level) const
{
    // VIPER #6878: All attributes and pragmas will be printed as pragma, return from here:
    if (_attr_print_mode == (unsigned)veri_file::PRINT_AS_PRAGMA) return ;

    Map *attributes = GetAttributes() ;
    if (!attributes) return ;

    // Attributes are stored as (char*)key-(VeriExpression*)value pairs in Map.
    MapIter mi ;
    char *attr_name ;
    VeriExpression *expr ;

    unsigned cnt = 0 ;
    FOREACH_MAP_ITEM(attributes, mi, &attr_name, &expr) {
        if (!attr_name || (*attr_name == ' ')) continue ; // Internal attribute

        // VIPER #6878: Check if we need to print pragmas as pragma ('expr' having no linefile is a pragma):
        if ((_attr_print_mode == (unsigned)veri_file::PRINT_AS_ORIGINAL) && expr && !expr->Linefile()) continue ;

        f << ((cnt) ? ", " : "(* ") ;
        // print name :
        // VIPER #6143: Go through PrintIdentifier so that we can escape illegal names
        // as well as keywords for the current dialect which was originally escaped:
        PrintIdentifier(f, attr_name) ;
        if (expr) {
            // and value :
            f << "=" ;
            expr->PrettyPrint(f, level) ;
        }
        cnt++ ;
    }
    if (cnt) f << " *) " ;
}

// VIPER #6878: Pretty-print pragmas. We set NULL linefile on the value (VeriExpression)
// which are from pragma. Values from actual attributes have proper linefile info.
void
VeriTreeNode::PrettyPrintPragmas(ostream &f, unsigned level, unsigned print_attrs_too /*=0*/) const
{
    // VIPER #6878: All attributes and pragmas will be printed as attribute, return from here:
    // Note: If 'print_attrs_too' is set, we need to print all attributes in pragma form.
    if (!print_attrs_too && (_attr_print_mode == (unsigned)veri_file::PRINT_AS_ATTRIBUTE)) return ;

    Map *attributes = GetAttributes() ;
    if (!attributes) return ;

    // Attributes are stored as (char*)key-(VeriExpression*)value pairs in Map.
    MapIter mi ;
    char *attr_name ;
    VeriExpression *expr ;

    unsigned cnt = 0 ;
    FOREACH_MAP_ITEM(attributes, mi, &attr_name, &expr) {
        if (!attr_name || (*attr_name == ' ')) continue ; // Internal attribute
        //
        // VIPER #6878: Check if we need to print pragmas as pragma ('expr' having no linefile is a pragma):
        // If 'print_attrs_too' is set, we need to print all attributes in pragma form, so, do not skip any.
        if (!print_attrs_too && (_attr_print_mode == (unsigned)veri_file::PRINT_AS_ORIGINAL) && (!expr || expr->Linefile())) continue ;

        f << ((cnt) ? ", " : " /* verific ") ;
        // print name :
        // VIPER #6143: Go through PrintIdentifier so that we can escape illegal names
        // as well as keywords for the current dialect which was originally escaped:
        PrintIdentifier(f, attr_name) ;
        // and value :
        f << "=" ;
        expr->PrettyPrint(f, level) ;
        cnt++ ;
    }
    if (cnt) f << " */ " ;
}

// VIPER #5903: Print pre-comments for an expression node:
void
VeriTreeNode::PrettyPrintPreComments(ostream &f, unsigned level) const
{
    Array *comment_arr = GetComments() ;
    if (!comment_arr) return ;

    // Print the comments that came in before this expression.

    // Get the line number of this expression:
    unsigned this_line_no = LineFile::GetLineNo(Linefile()) ;
    // Also take the file id:
    unsigned this_file_id = LineFile::GetFileId(Linefile()) ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned this_col = (Linefile()) ? Linefile()->GetLeftCol() : 0 ;
#endif

    unsigned i ;
    VeriCommentNode *node ;
    unsigned is_line_comment = 0 ;
    FOREACH_ARRAY_ITEM(comment_arr, i, node) {
        if (!node) continue ;

        unsigned comment_line_no = node->GetStartingLineNumber() ;
        if ((comment_line_no > this_line_no) &&
            (this_file_id == LineFile::GetFileId(node->Linefile()))) continue ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        unsigned comment_col = (node->Linefile()) ? node->Linefile()->GetRightCol() : 0 ;
        if ((comment_line_no == this_line_no) && (comment_col > this_col) &&
            (this_file_id == LineFile::GetFileId(node->Linefile()))) continue ;
#else
        if ((comment_line_no == this_line_no) &&
            (this_file_id == LineFile::GetFileId(node->Linefile()))) continue ;
#endif

        // push the comment out
        f << node->GetCommentString() ;
        is_line_comment = (node->IsBlockComment()) ? 0 : 1 ;
        // If the last comment was a line comment, then a newline occurred, and we should output 'level'
        if (is_line_comment) {
            f << PrintLevel(level) ;
        } else {
            f << " " ; // print a space after the comment.
        }
    }
}

// VIPER #5903: Print post-comments for an expression node:
void
VeriTreeNode::PrettyPrintPostComments(ostream &f, unsigned level, unsigned /* print_linefeed */) const
{
    Array *comment_arr = GetComments() ;
    if (!comment_arr) return ;

    // Print the comments that came in after this expression.

    // Get the line number of this expression:
    unsigned this_line_no = LineFile::GetLineNo(Linefile()) ;
    // Also take the file id:
    unsigned this_file_id = LineFile::GetFileId(Linefile()) ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    unsigned this_col = (Linefile()) ? Linefile()->GetLeftCol() : 0 ;
#endif

    unsigned i ;
    unsigned printed = 0 ;
    VeriCommentNode *node ;
    unsigned is_line_comment = 0 ;
    FOREACH_ARRAY_ITEM(comment_arr, i, node) {
        if (!node) continue ;

        unsigned comment_line_no = node->GetStartingLineNumber() ;
        if ((comment_line_no < this_line_no) ||
            (this_file_id != LineFile::GetFileId(node->Linefile()))) continue ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        unsigned comment_col = (node->Linefile()) ? node->Linefile()->GetRightCol() : 0 ;
        if ((comment_line_no == this_line_no) && (comment_col <= this_col) &&
            (this_file_id == LineFile::GetFileId(node->Linefile()))) continue ;
#endif

        // push the comment out
        if (!printed) f << " " ; // print a space before the first comment.
        printed = 1 ;
        f << node->GetCommentString() ;
        is_line_comment = (node->IsBlockComment()) ? 0 : 1 ;
        // If the last comment was a line comment, then a newline occurred, and we should output 'level'
        if (is_line_comment) f << PrintLevel(level) ;
    }
}

//--------------------------------------------------------------
// #included from VeriConstVal.h
//--------------------------------------------------------------

void
VeriIntVal::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << _num ;
}

void
VeriRealVal::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    char *image = Strings::dtoa(_num) ;
    // VIPER #5248 (test63): Adjustments for infinity/NaN values which may be created
    // out of static elaboration, write them as division by zero of double number:
    // CHECKME: How do you know its a inf/NaN other than comparing as strings?
    if (Strings::compare_nocase(image, "inf")) {
        Strings::free(image) ;
        image = Strings::save("(1.0 / 0 /* Infinity */)") ;
    } else if (Strings::compare_nocase(image, "-inf")) {
        Strings::free(image) ;
        image = Strings::save("(-1.0 / 0 /* -Infinity */)") ;
    } else if (Strings::compare_nocase(image, "nan")) {
        Strings::free(image) ;
        image = Strings::save("(0.0 / 0 /* NaN */)") ;
    }
    f << image ;
    Strings::free(image) ;
}

#define GET_BIT(S,B) ((S)?(((S)[(B)/8]>>((B)%8))&1):0)

void
VeriConstVal::PrettyPrint(ostream &f, unsigned /*level*/) const
{
#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & #2770: Preserve the original value string:
    if (_orig_value) {
        // If we have the original string, print it and return.
        // For strings, we never stored anything in '_orig_value'.
        // Strings are stored as-it-is normally.
        f << _orig_value ;
        return ;
    }
#endif

    // Print as a sized bit-array
    // Note : If originally a 'string' constant was used, it will now
    // be written as a bit-pattern.

    if (_is_string) {
        char *str = Image() ;
        f << str ;
        Strings::free(str) ;
        return ;
    }

    // First check whether the value is unsized bit or number
    // Then we don't check for the signed-ness, only print ' or 'b
    if (_is_unsized_bit) {
        // like '1, don't print the size or the base
        f << "'" ;
    } else if (_is_unsized_number) {
        // like 'b1001, don't print the size, only print the base
        f << (_is_signed ? "'sb" : "'b" );
    } else {
        f << _size << (_is_signed ? "'sb" : "'b") ;
    }

    // Go MSB to LSB. MSB is highest index. 0 the lowest.
    unsigned print_the_rest = 0 ; // Flag used to control printing of remaining bits
    char last_value = 0 ;         // Last bit encountered.  Used to determine when to start printing.
    char current_value ;
    unsigned i = _size ;
    while (i-- != 0) {
        // Determine the value of this bit
        if (GET_BIT(_xvalue,i))         { current_value = 'x' ; }
        else if (GET_BIT(_zvalue,i))    { current_value = 'z' ; }
        else if (GET_BIT(_value,i))     { current_value = '1' ; }
        else                            { current_value = '0' ; }
        // Determine if you need to print this bit or trim it.
        if (print_the_rest || (current_value == '1' && !_is_unsized_bit) || (last_value && (last_value != current_value))) { // Don't print anything if it's redundant
            if (!print_the_rest) {
                // Print last_value if last_value is defined and it's not the same as the current_value
                if (last_value && (last_value != current_value)) f << last_value ;
                // Force printing of remainder bits
                print_the_rest = 1 ;
            }
            f << current_value ;
        }
        last_value = current_value ;
    }
    // If nothing was printed, then print the last_value.  This is the case
    // for all 0's, x's, z's (and 1's only _is_unsized_bit).
    if (!print_the_rest) f << last_value ;
}
void
VeriTimeLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << _literal ;
}

void
VeriCaseOperator::PrettyPrint(ostream &f, unsigned level) const
{
    f << endl << PrintLevel(level+1) ;

    // Print comments before the statement itself
    PrettyPrintPreComments(f, level+1) ;

    f << PrintToken(VERI_CASE) ;
    if (_cond) { // Print braces if condition exists (randcase has no condition)
        f << " (" ;
        _cond->PrettyPrint(f,level+1) ;
        f << ")" ;
    }

    // VIPER #2494: Don't print (ending) comments here, print a newline instead
    f << endl ;

    unsigned i ;
    VeriCaseOperatorItem *ci ;
    /* The object itself is the array */
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        /* There is no separator */
        if (ci) ci->PrettyPrint(f,level+1) ;
    }

    f << PrintLevel(level+1) ;
    f << PrintToken(VERI_ENDCASE) << " " ;

    // VIPER #2494: Print the post comment after the 'endcase' keyword
    PrettyPrintPostComments(f, level+1, 1) ;
}

void
VeriCaseOperatorItem::PrettyPrint(ostream &f, unsigned level) const
{
    /* Each item starts at a new line */
    f << PrintLevel(level+1) ;

    if (_conditions && (_conditions->Size() > 0)) {
        // List of expressions :
        PrettyPrintExprList(_conditions, f, level+1) ;
    } else {
        /* 'default' case */
        f << PrintToken(VERI_DEFAULT) ;
    }

    /* Print property expression on new line, one level deeper */
    f << " : " ;
    if (_property_expr) _property_expr->PrettyPrint(f,level+1) ;

    /* Print semicolon as a normal statement */
    f << "; " << endl ;
}

