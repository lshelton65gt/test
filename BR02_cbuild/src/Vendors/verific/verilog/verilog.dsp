# Microsoft Developer Studio Project File - Name="verilog" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=verilog - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "verilog.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "verilog.mak" CFG="verilog - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "verilog - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "verilog - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "verilog - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /I "." /I "../containers" /I "../util" /I "../vhdl" /I "../hier_tree" /I "../sqlmanager" /D "NDEBUG" /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "_WINDOWS" /YX /FD /Zm200 /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "verilog - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Z7 /Od /I "." /I "../containers" /I "../util" /I "../vhdl" /I "../hier_tree" /I "../sqlmanager" /D "_DEBUG" /D "WIN32" /D "WINDOWS" /D "_CRT_SECURE_NO_WARNINGS" /D "_WINDOWS" /YX /FD /Zm200 /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "verilog - Win32 Release"
# Name "verilog - Win32 Debug"
# Begin Group "Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\veri_file.h
# End Source File
# Begin Source File

SOURCE=.\veri_tokens.h
# End Source File
# Begin Source File

SOURCE=.\VeriBaseValue_Stat.h
# End Source File
# Begin Source File

SOURCE=.\VeriClassIds.h
# End Source File
# Begin Source File

SOURCE=.\VeriCompileFlags.h
# End Source File
# Begin Source File

SOURCE=.\VeriConstVal.h
# End Source File
# Begin Source File

SOURCE=.\VeriCopy.h
# End Source File
# Begin Source File

SOURCE=.\VeriElab_Stat.h
# End Source File
# Begin Source File

SOURCE=.\VeriExplicitStateMachine.h
# End Source File
# Begin Source File

SOURCE=.\VeriExpression.h
# End Source File
# Begin Source File

SOURCE=.\VeriId.h
# End Source File
# Begin Source File

SOURCE=.\VeriLibrary.h
# End Source File
# Begin Source File

SOURCE=.\VeriMisc.h
# End Source File
# Begin Source File

SOURCE=.\VeriModule.h
# End Source File
# Begin Source File

SOURCE=.\VeriModuleItem.h
# End Source File
# Begin Source File

SOURCE=.\VeriScope.h
# End Source File
# Begin Source File

SOURCE=.\VeriRuntimeFlags.h
# End Source File
# Begin Source File

SOURCE=.\VeriStatement.h
# End Source File
# Begin Source File

SOURCE=.\VeriTreeNode.h
# End Source File
# Begin Source File

SOURCE=.\VeriUtil_Stat.h
# End Source File
# Begin Source File

SOURCE=.\VeriVarUsage.h
# End Source File
# Begin Source File

SOURCE=.\VeriVisitor.h
# End Source File
# End Group
# Begin Group "Analysis"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\veri_file.cpp
# End Source File
# Begin Source File

SOURCE=.\veri_lex.cpp
# End Source File
# Begin Source File

SOURCE=.\veri_yacc.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriReplace.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriBinarySaveRestore.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriConstVal.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriCopy.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriCrossLanguageConversion.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriExplicitStateMachine.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriExpression.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriId.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriLibrary.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriMisc.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriModule.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriModuleItem.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriPrettyPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriScope.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriStatement.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriTreeNode.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriVarUsage.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriVisitor.cpp
# End Source File
# End Group
# Begin Group "Static Elaboration"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\veri_file_Stat.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriBaseValue_Stat.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriElab_Stat.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriExprEval_Stat.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriExprWidth_Stat.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriGenElab_Stat.cpp
# End Source File
# Begin Source File

SOURCE=.\VeriUtil_Stat.cpp
# End Source File
# End Group
# End Target
# End Project
