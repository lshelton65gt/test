/*
 *
 * [ File Version : 1.26 - 2014/02/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>
#include <stdio.h>   // for sprintf
#include <time.h>    // for timestamp
#include <stdlib.h>  // for strtod, strtoul

#include "Strings.h"
#include "Array.h"

#include "VeriLibrary.h"
#include "VeriModule.h"
#include "VeriId.h"
#include "VeriExpression.h"
#include "VeriScope.h"
#include "VeriTreeNode.h"
#include "veri_tokens.h"
#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "VeriElab_Stat.h"

#include "../vhdl/vhdl_file.h"
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/VhdlName.h"
#include "../vhdl/VhdlIdDef.h"
#include "../vhdl/VhdlDeclaration.h"
#include "../vhdl/VhdlScope.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Convert verilog package to vhdl package

// For scalar type return vhdl scalar type name, for vector type return vhdl vector type name
// eg: for 'logic' return 'std_logic' and for 'logic[]' return 'std_logic_vector'
char* VeriNode::GetTokenName(unsigned type, unsigned has_dim)
{
    switch(type) {
    case VERI_BIT:
    {
        if (!has_dim) return Strings::save("bit") ;
        return Strings::save("bit_vector") ;
    }

    case VERI_LOGIC:
    case VERI_REG:
    {
        if (!has_dim) return Strings::save("std_logic") ;
        return Strings::save("std_logic_vector") ;
    }

    case VERI_REAL:
    case VERI_SHORTREAL: return Strings::save("real") ;

    case VERI_BYTE:
    case VERI_INT:
    case VERI_SHORTINT:
    case VERI_LONGINT: return Strings::save("integer") ;

    default: return 0 ;
    }
}

// Create unique name:
char *VeriNode::CreateUniqueIdName(const VhdlScope *vhdl_scope, VeriScope *veri_scope, const char *name)
{
    char *temp_name = Strings::save(name) ;
    VeriScope *old_scope = veri_scope ;
    unsigned number = 0 ;
    while(1)
    {
        if (old_scope && vhdl_scope && (vhdl_scope->FindSingleObject(temp_name) || old_scope->Find(temp_name))) {
            char *num = Strings::itoa((int)number) ;
            Strings::free(temp_name) ;
            temp_name = Strings::save(name, "_", num) ;
            number++ ;
            Strings::free(num) ;
        } else {
            return temp_name ;
        }
    }
    return 0 ;
}

// Create element declaration:
unsigned VeriDataDecl::CreateElementDecl(Array *decl_part, Array *record_decls, VhdlScope *record_scope, VhdlScope *vhdl_scope, VeriScope *veri_scope, VhdlTypeId *struct_id)
{
    if (!decl_part || !record_decls || !record_scope || !vhdl_scope || !veri_scope || !struct_id || !_data_type) return 0 ;

    if (_data_type->IsUnionType()) { // do not convert union type
        unsigned type = _data_type->Type() ;
        Info("ignore unsupported data type %s during package conversion", PrintToken(type)) ;
        return 0 ;
    }

    // Get the name of struct_id:
    char *name = vhdl_file::CreateVhdlEntityName(struct_id->Name()) ;

    // For structure type data type create element declaration:
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

        // Declare the vhdl element ids:
        char *elem_name = vhdl_file::CreateVhdlEntityName(id->GetName()) ;

        VhdlSubtypeIndication *subtype_indication = 0 ;
        if (!_data_type->IsTypeRef() && (_data_type->IsStructType() || _data_type->IsEnumType())) { // for enum/structure declarations in structure
            // Create vhdl declarations those will be declared out side the record declaration:
            char *prefix = Strings::save(elem_name, "_", name) ; // Concat the structure name with the id name
            // Search the name in vhdl and verilog scope, if find create a new name
            char *new_name = CreateUniqueIdName(vhdl_scope, veri_scope, prefix) ;
            Strings::free(prefix) ;

            VhdlTypeId *unique_id = new VhdlTypeId(new_name) ; // Create type id
            unique_id->SetLinefile(id->Linefile()) ;
            (void) vhdl_scope->Declare(unique_id) ; // Declare the id in new vhdl scope

            VhdlTypeDef *type_def = _data_type->ConvertToTypeDef(decl_part, vhdl_scope, veri_scope, unique_id) ; // Create typedef

            VhdlDeclaration *vhdl_decl = new VhdlFullTypeDecl(unique_id, type_def) ; // Create fulltype declaration
            vhdl_decl->SetLinefile(Linefile()) ;
            decl_part->InsertLast(vhdl_decl) ; // Create a new declaration and declare it in vhdl scope and use its reference in record

            // Create subtype indication
            subtype_indication = new VhdlIdRef(unique_id) ; // Create reference of the declarations declared outside record declaration
            subtype_indication->SetLinefile(Linefile()) ;
        } else { // Create subtype_indication of array/simple datatypes
            subtype_indication = _data_type->ConvertDataType(vhdl_scope, veri_scope) ;
        }

        if (!subtype_indication) { // unsupported data types like string, chandle etc
            Strings::free(elem_name) ;
            Strings::free(name) ;
            return 0 ;
        }

        if (id->GetDimensions()) { // for array type:
            // First declare a new data declaration outside the record scope,
            // in vhdl scope and then use its reference in the element declaration
            // eg: verilog declaration is:
            //     typedef struct { bit a; bit [3:0][1:0] b[2:0][3:0] ;} st_pack
            // corresponding vhdl declaration is:
            //     type b_st_pack is array (2 downto 0,3 downto 0) of bit_vector(7 downto 0);
            //     type st_pack_0 is record
            //             a : bit; b : b_st_pack;
            //     end record ;
            // Create vhdl declarations those will be declared out side the record declaration:
            char *prefix = Strings::save(elem_name, "_", name) ; // Concat the structure name with the id name
            // Search the name in vhdl and verilog scope, if find create a new name
            char *new_name = CreateUniqueIdName(vhdl_scope, veri_scope, prefix) ;
            Strings::free(prefix) ;

            VhdlTypeId *new_unique_id = new VhdlTypeId(new_name) ; // Create type id
            new_unique_id->SetLinefile(id->Linefile()) ;
            (void) vhdl_scope->Declare(new_unique_id) ; // Declare the id in new vhdl scope

            // Unpacked array type: need to create a vhdl array typedef
            Array *index_constriant = new Array() ;
            VeriRange *range = id->GetDimensions() ;
            while(range) {
                VhdlRange *vhdl_range = VhdlNode::CreateConversionRange(range->Linefile()) ;
                index_constriant->InsertLast(vhdl_range) ;
                range = range->GetNext() ;
            }

            VhdlTypeDef *type_def = new VhdlArrayTypeDef(index_constriant, subtype_indication) ; // Create array typedef
            type_def->SetLinefile(Linefile()) ;
            VhdlDeclaration *vhdl_decl = new VhdlFullTypeDecl(new_unique_id, type_def) ; // Create fulltype declaration
            vhdl_decl->SetLinefile(Linefile()) ;
            decl_part->InsertLast(vhdl_decl) ; // Add declaration in the vhdl scope

            subtype_indication = 0 ;
            subtype_indication = new VhdlIdRef(new_unique_id) ; // Create reference of the declaration, declared outside of structure
            subtype_indication->SetLinefile(Linefile()) ;
        }

        // Declare the vhdl element ids:
        // For structure type data type create element declaration:
        VhdlElementId *element_id = new VhdlElementId(elem_name) ;
        element_id->SetLinefile(id->Linefile()) ;
        (void) record_scope->Declare(element_id) ;

        // Element id list
        Array *vhdl_id_list = new Array() ;
        vhdl_id_list->InsertLast(element_id) ;

        // Create element declaration
        VhdlElementDecl *elem_decl = new VhdlElementDecl(vhdl_id_list, subtype_indication) ;
        elem_decl->SetLinefile(Linefile()) ;
        record_decls->InsertLast(elem_decl) ;
    }

    Strings::free(name) ;
    return 1 ;
}

VhdlDeclaration* VeriDataDecl::ConvertDataDecl(Array *decl_part, VhdlScope *vhdl_scope, VeriScope *veri_scope)
{
    if (!decl_part || !vhdl_scope || !veri_scope) return 0 ;
    if (!IsTypeAlias()) return 0 ; // Only process type declarations

    if (_data_type && _data_type->IsUnionType()) { // do not convert union type
        unsigned type = _data_type->Type() ;
        Info("ignore unsupported data type %s during package conversion", PrintToken(type)) ;
        return 0 ;
    }

    VeriIdDef *pkg_id = veri_scope->GetOwner() ;
    VeriModuleItem *pkg = pkg_id && pkg_id->IsPackage() ? pkg_id->GetModuleItem() : 0 ;
    VeriLibrary *lib = pkg ? pkg->GetLibrary() : 0 ;
    const char *lib_name = lib ? lib->GetName() : 0 ;
    const char *pkg_name = pkg_id ? pkg_id->GetName() : 0 ;

    if (!lib_name || !pkg_name) return 0 ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;
        char *name = vhdl_file::CreateVhdlEntityName(id->GetName()) ;
        if (vhdl_scope->FindSingleObject(name)) { // Already processed during processing the forward declaration
            // Eg: typedef T1; // forward declaration
            //     typedef T1 T2 ;
            //     typedef int T1 ;
            // During processing the first declaration, T1 is declared in vhdl scope, so when we come to process
            // the 3rd declaration, we will find the id to be already declared in vhdl scope and will not proceed further.
            Strings::free(name) ;
            return 0 ;
        }

        // For forward type declaration get the data_type of the type id, otherwise proceed with _data_type
        // Eg: typedef T1 ; // forward declaration
        //     typedef T1 T2 ;
        //     typedef int T1 ;
        // when come to process T1, we do not have _data_type, so get its data_type from the type id.
        VeriDataType *data_type = _data_type ? _data_type : id->GetDataType() ;
        if (!data_type) {
            Strings::free(name) ;
            continue ;
        }

        VhdlSubtypeIndication *subtype_indication = 0 ;
        VhdlTypeDef *type_def = 0 ;

        VhdlTypeId *vhdl_type_id = 0 ;
        if (!data_type->IsTypeRef() && (data_type->IsStructType() || data_type->IsEnumType())) { // For structure/enum declaration
            vhdl_type_id = new VhdlTypeId(name) ;
            // For array/enum/structure declarations create fulltype/subtype declaration:
            type_def = data_type->ConvertToTypeDef(decl_part, vhdl_scope, veri_scope, vhdl_type_id) ; // create typedef for enum/structure data type
            if (!type_def) { // structure contains some unsupported data types
                delete vhdl_type_id ;
                return 0 ;
            }
            vhdl_type_id->SetLinefile(id->Linefile()) ;
            (void) vhdl_scope->Declare(vhdl_type_id) ;
            vhdl_type_id->SetVerilogPkgName(pkg_name) ;
            vhdl_type_id->SetVerilogPkgLibName(lib_name) ;
        }

        if (type_def) { // For structure and enum type
            if (id->GetDimensions()) { // If array structure type, then first declare a new type declaration for this structure
                // and use its reference for structure declaration:
                // eg: verilog declaration is:
                //     typedef struct { bit a; logic [1:0] b ; } st_pack[2:0] ;
                // corresponding vhdl declaration is:
                //     type st_pack2 is array (2 downto 0) of st_pack2_0;
                //     type st_pack2_0 is record
                //         a : bit; b : std_logic_vector(1 downto 0);
                //     end record ;
                char *prefix = Strings::save(name, "_", "0") ; // Concat the structure name with the id name
                // Search the name in vhdl and verilog, if find create a new name
                char *new_name = CreateUniqueIdName(vhdl_scope, veri_scope, prefix) ;
                Strings::free(prefix) ;

                VhdlTypeId *unique_id = new VhdlTypeId(new_name) ; // Create type id
                unique_id->SetLinefile(id->Linefile()) ;
                (void) vhdl_scope->Declare(unique_id) ; // Declare the id in new vhdl scope

                VhdlDeclaration *vhdl_decl = new VhdlFullTypeDecl(unique_id, type_def) ; // Create fulltype declaration
                vhdl_decl->SetLinefile(Linefile()) ;
                decl_part->InsertLast(vhdl_decl) ; // Create a new declaration and declare it in vhdl scope and use its reference in record

                // Create subtype indication
                subtype_indication = new VhdlIdRef(unique_id) ; // Create reference of declaration, newly declared in vhdl_scope
                subtype_indication->SetLinefile(Linefile()) ;
            } else {
                VhdlDeclaration *vhdl_decl = new VhdlFullTypeDecl(vhdl_type_id, type_def) ; // Create fulltype declaration
                vhdl_decl->SetLinefile(Linefile()) ;
                return vhdl_decl ;
            }
        } else { // For array/simple type
            subtype_indication = data_type->ConvertDataType(vhdl_scope, veri_scope) ;
        }

        if (!subtype_indication) { // unsupported data types like string, chandle etc
            Strings::free(name) ;
            return 0 ;
        }

        if (id->GetDimensions()) { // Unpacked array type: need to create a vhdl array typedef
            // eg: verilog declaration is: typedef bit [7:0] arr [1:0] ;
            // corresponding vhdl declaration is: type arr is array (1 downto 0) of bit_vector(7 downto 0);
            if (!vhdl_type_id) {
                vhdl_type_id = new VhdlTypeId(name) ;
                vhdl_type_id->SetLinefile(id->Linefile()) ;
                (void) vhdl_scope->Declare(vhdl_type_id) ;
                vhdl_type_id->SetVerilogPkgName(pkg_name) ;
                vhdl_type_id->SetVerilogPkgLibName(lib_name) ;
            }

            Array *index_constriant = new Array() ;
            VeriRange *range = id->GetDimensions() ;
            while(range) {
                VhdlRange *vhdl_range = VhdlNode::CreateConversionRange(range->Linefile()) ;
                index_constriant->InsertLast(vhdl_range) ;
                range = range->GetNext() ;
            }
            type_def = new VhdlArrayTypeDef(index_constriant, subtype_indication) ; // Create array typedef
            type_def->SetLinefile(Linefile()) ;
            VhdlDeclaration *vhdl_decl = new VhdlFullTypeDecl(vhdl_type_id, type_def) ; // Create fulltype declaration
            vhdl_decl->SetLinefile(Linefile()) ;
            return vhdl_decl ;
        } else { // Create vhdl subtype declaration for non array data type:
            // eg: verilog declaration is: typedef enum { red, green, blue, yellow } en ; typedef en en1 ;
            // corresponding vhdl declaration is: type en is (red,green,blue,yellow); subtype en1 is en;
            VhdlSubtypeId *subtype_id = new VhdlSubtypeId(name) ;
            subtype_id->SetLinefile(id->Linefile()) ;
            (void) vhdl_scope->Declare(subtype_id) ;
            VhdlDeclaration *vhdl_decl = new VhdlSubtypeDecl(subtype_id, subtype_indication) ;
            vhdl_decl->SetLinefile(Linefile()) ;
            return vhdl_decl ;
        }
    }
    return 0 ;
}

// Convert verilog data type to corresponding vhdl data type
VhdlSubtypeIndication* VeriDataType::ConvertDataType(VhdlScope *vhdl_scope, VeriScope *veri_scope)
{
    if (!vhdl_scope || !veri_scope) return 0 ;

    char *type_name = 0 ;
    // data dype has no dimension, return scalar type: eg. for 'logic' return 'std_logic'
    // data type has dimension: return vector type: eg. for 'logic[]' return 'std_logic_vector'
    type_name = _dimensions ? VeriNode::GetTokenName(_type, 1) : VeriNode::GetTokenName(_type, 0) ;
    if (!type_name) {
        Info("ignore unsupported data type %s during package conversion", PrintToken(_type)) ;
        return 0 ;
    }

    if (!_dimensions) {
        VhdlIdRef *subtype_indication = new VhdlIdRef(type_name) ;
        subtype_indication->SetLinefile(Linefile()) ;
        return subtype_indication ;
    } else { // for packed multiple data type marge the dimensions and create a single dimension
        VhdlIdRef *prefix = new VhdlIdRef(type_name) ;
        Array *assoc_list = new Array() ;
        VhdlRange *vhdl_range = VhdlNode::CreateConversionRange(_dimensions->Linefile()) ;
        assoc_list->InsertLast(vhdl_range) ;
        VhdlIndexedName *index_name = new VhdlIndexedName(prefix, assoc_list) ; // create indexed name
        index_name->SetLinefile(Linefile()) ;
        return index_name ;
    }
}

// Convert verilog data type to corresponding vhdl data type
VhdlSubtypeIndication* VeriTypeRef::ConvertDataType(VhdlScope *vhdl_scope, VeriScope* /*veri_scope*/)
{
    if (!vhdl_scope) return 0 ;

    VeriIdDef *id = GetId() ;
    if (!id) return 0 ;

    char *name = vhdl_file::CreateVhdlEntityName(id->Name()) ;
    VhdlIdDef *vhdl_id = vhdl_scope->FindSingleObject(name) ; // Find the id in vhdl scope
    VhdlIdRef *subtype_indication = 0 ;
    if (vhdl_id) {
        subtype_indication = new VhdlIdRef(vhdl_id) ;
        subtype_indication->SetLinefile(Linefile()) ;
    } else { // reference of unsupported data types like chandle, string etc
        Strings::free(name) ;
        return 0 ;
    }
    Strings::free(name) ;
    return subtype_indication ;
}

// Convert verilog structure data type to corresponding vhdl record data type
VhdlTypeDef* VeriStructUnion::ConvertToTypeDef(Array *decl_part, VhdlScope *vhdl_scope, VeriScope *veri_scope, VhdlTypeId *struct_id)
{
    if (!decl_part || !vhdl_scope || !veri_scope || !struct_id) return 0 ;

    unsigned decl_item_size = decl_part->Size() ;

    // Create a record scope as the child of vhdl scope:
    VhdlScope *record_scope = new VhdlScope(vhdl_scope, struct_id) ;

    // Create element declarations:
    Array *record_decls = new Array(_decls ? _decls->Size() : 2) ;
    unsigned i ;
    VeriDataDecl *decl ;
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        if (!decl) continue ;
        // Structure contains some unsupported data types like string/chandle, then we do not
        // convert the structure and delete the element declarations that are already declared
        // Otherwise create element declarations
        if (!decl->CreateElementDecl(decl_part, record_decls, record_scope, vhdl_scope, veri_scope, struct_id)) {
            unsigned j ;
            VhdlDeclaration *record_decl ;
            FOREACH_ARRAY_ITEM(record_decls, j, record_decl) {
                if (!record_decl) continue ;
                unsigned k ;
                VhdlIdDef *id ;
                Array *ids = record_decl->GetIds() ;
                FOREACH_ARRAY_ITEM(ids, k, id) {
                    if (!id) continue ;
                    record_scope->Undeclare(id) ;
                }
                delete record_decl ;
            }
            delete record_scope ;

            for(j=decl_part->Size(); j>decl_item_size ; j++) {
                VhdlDeclaration *prev_decl = (VhdlDeclaration*) decl_part->At(j-1) ;
                if (!prev_decl) continue ;
                unsigned k ;
                VhdlIdDef *id ;
                Array *ids = prev_decl->GetIds() ;
                FOREACH_ARRAY_ITEM(ids, k, id) {
                    if (!id) continue ;
                    vhdl_scope->Undeclare(id) ;
                }
                delete prev_decl ;
            }
            delete record_decls ;
            return 0 ; // unsupported data types
        }
    }

    // Create record typedef:
    VhdlTypeDef *record_data_type = new VhdlRecordTypeDef(record_decls, record_scope) ;
    record_data_type->SetLinefile(Linefile()) ;
    return record_data_type ;
}

// Convert verilog enum data type to corresponding vhdl enum data type
VhdlTypeDef* VeriEnum::ConvertToTypeDef(Array* /*decl_part*/, VhdlScope *vhdl_scope, VeriScope* /*veri_scope*/, VhdlTypeId* /*struct_id*/)
{
    if (!vhdl_scope) return 0 ;

    Array *enum_ids = new Array(_ids ? _ids->Size() : 2) ;
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        char *enum_name = vhdl_file::CreateVhdlEntityName(id->Name()) ;
        VhdlEnumerationId *enum_id = new VhdlEnumerationId(enum_name) ;
        enum_id->SetLinefile(id->Linefile()) ;
        (void) vhdl_scope->Declare(enum_id) ;
        enum_ids->InsertLast(enum_id) ;
    }

    // Create enum data type
    VhdlTypeDef *enum_data_type = new VhdlEnumerationTypeDef(enum_ids) ;
    enum_data_type->SetLinefile(Linefile()) ;
    return enum_data_type ;
}

VhdlPrimaryUnit* VeriPackage::ConvertModule(const char *lib_name) const
{
    // Take the lib_name from verilog module
    const char *library_name = _parent_library ? _parent_library->GetName() : lib_name ;
    if (!library_name) return 0 ; // If no such module or library exist then return 0
    VhdlPackageDecl *package = 0 ;

    // Keep the verilog scope:
    VeriScope *veri_scope = GetScope() ? GetScope() : 0 ;

    // First, reset the scope to 0, then create a new scope for the library unit
    VhdlScope *save_scope = VhdlTreeNode::_present_scope ;
    VhdlTreeNode::_present_scope = 0 ;
    VhdlScope::Push() ;

    // Now create a 'context_clause' tree node (an Array of context items).
    Array *context_clause = new Array() ;

    // Define library names 'STD' and 'WORK'
    Array *libs = new Array() ;

    // Declare library identifier 'std'
    VhdlIdDef *id = new VhdlLibraryId(Strings::save("std")) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    // Declare library identifier 'ieee'
    id = new VhdlLibraryId(Strings::save("ieee")) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    // Declare library identifier for work library
    id = new VhdlLibraryId(Strings::save(library_name)) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    // Set them in the parse tree
    context_clause->InsertLast(new VhdlLibraryClause(libs, 0 /*implicitly declared*/)) ;

    // Create id for entity
    VhdlPackageId *package_id = new VhdlPackageId(vhdl_file::CreateVhdlEntityName(Name())) ;
    package_id->SetLinefile(Linefile()) ;

    VhdlTreeNode::_present_scope->SetOwner(package_id) ;

    // Include std.standard.all ; with a use-clause
    VhdlName *n = new VhdlSelectedName(
                new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                new VhdlIdRef(Strings::save("standard"))),
                new VhdlAll()) ;
    Array *use_clauses = new Array() ;
    use_clauses->InsertLast(n) ;
    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;

    // Include std.std_logic_1164.all ; with a use-clause
    VhdlName *n1 = new VhdlSelectedName(
                new VhdlSelectedName(new VhdlIdRef(Strings::save("ieee")),
                new VhdlIdRef(Strings::save("std_logic_1164"))),
                new VhdlAll()) ;
    use_clauses = new Array() ;
    use_clauses->InsertLast(n1) ;
    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;

    (void) VhdlTreeNode::_present_scope->Declare(package_id) ;

    // Array of declared package items:
    Array *vhdl_decl_part = new Array() ;
    VeriDataDecl *decl ;
    unsigned i ;
    // Convert all import declarations and type declarations
    FOREACH_ARRAY_ITEM(_module_items, i, decl) {
        if (!decl) continue ;
        // Convert the packages that are imported in the container packages:
        // Consider the following example:
        // verilog declaration is:
        //     package pkg1 ;
        //         typedef bit my_type ;
        //     endpackage
        // The package pkg1 is imported in the following package
        //     package pkg ;
        //     import pkg1::* ;
        //         typedef my_type my_type1 ;
        //     endpackage

        // corresponding vhdl declaration is:
        //    package pkg1 is
        //        subtype my_type is bit;
        // end pkg1 ;
        // The second package importing the above package is:
        //     use work.pkg1.all;
        //     package pkg is
        //         subtype my_type1 is my_type;
        //     end pkg ;
        // For using package elements of one package into another we need to convert
        // the imported packages before converting the container one. After converting
        // the imported packages we create use clauses and add those use clauses in
        // context_clause of the container package. Then go to convert the container package.
        if (decl->IsImportDecl()) { // convert the import declarations
            VhdlPrimaryUnit *vhdl_package = 0 ;
            Array *import_items = decl->GetItems() ;
            unsigned j ;
            VeriScopeName *import_item ;
            FOREACH_ARRAY_ITEM(import_items, j, import_item) {
                if (!import_item) continue ;

                VeriName *prefix = import_item->GetPrefix() ;
                VeriIdDef *prefix_id = prefix ? prefix->GetId() : 0 ;

                if (prefix_id && prefix_id->IsPackage()) {
                    VeriModule *veri_package = prefix_id->GetModule() ;
                    // Get the package name:
                    if (veri_package) vhdl_package = veri_package->ConvertModule(0) ;
                    if (!vhdl_package) continue ;

                    // Import declaration contains suffix: create use clause as 'use lib.pkg.suffix'
                    // Import declaration contains no siffix: create use clause as 'use lib.pkg.all'
                    VhdlDesignator *vhdl_suffix_name = 0 ;
                    if (import_item->GetSuffix()) {
                        vhdl_suffix_name = new VhdlIdRef(Strings::save(import_item->GetSuffix())) ;
                    } else {
                        vhdl_suffix_name = new VhdlAll() ;
                    }

                    // Get the library of the converted vhdl package
                    VhdlLibrary *vhdl_lib = vhdl_package->GetOwningLib() ;
                    char *vhdl_lib_name = Strings::save(vhdl_lib ? vhdl_lib->Name() : "work") ;

                    char *prefix_name = Strings::save(prefix_id->Name()) ;
                    // Create use clauses and add to context_clause:
                    VhdlSelectedName *selected_name = new VhdlSelectedName(
                                                  new VhdlSelectedName(new VhdlIdRef(vhdl_lib_name),
                                                  new VhdlIdRef(prefix_name)), vhdl_suffix_name) ;

                    use_clauses = new Array() ;
                    use_clauses->InsertLast(selected_name) ;
                    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;
                }
            }
        }

        // Convert the other declarations:
        if (decl->IsTypeAlias()) {
            VhdlDeclaration *data_decl = decl->ConvertDataDecl(vhdl_decl_part, VhdlTreeNode::_present_scope, veri_scope) ;
            if (data_decl) vhdl_decl_part->InsertLast(data_decl) ;
        }
    }

    // Create new package declaration
    package = new VhdlPackageDecl(context_clause, package_id, 0, 0, vhdl_decl_part, VhdlScope::Pop()) ;
    package->SetLinefile(Linefile()) ;
    VhdlTreeNode::_present_scope = save_scope ;
    package->SetVerilogModule(1) ; // Set the flag to denote that this package has been obtained from a Verilog package

    return package ;
}

// Viper 6895 : Find the Vhdl Type corresponding to the converted Verilog type
VhdlIdDef *VeriTypeId::GetVhdlTypeFromPackage()
{
    VhdlPrimaryUnit *vhdl_package = VeriNode::GetVhdlUnitForVerilogInstance(_vhdl_package_name, _vhdl_lib_name, 1, 0) ;
    if (!vhdl_package) return 0 ;
    Array *decl_part = vhdl_package->GetDeclPart() ;
    // Resolve any attribute specifications
    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(decl_part, i, decl) {
        if (!decl) continue ;
        VhdlIdDef *type_id = decl->GetTypeId() ;
        if (type_id && Strings::compare(GetName(), type_id->Name())) {
            return type_id ;
        }
    }
    return 0 ;
}
#endif

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION

// Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
unsigned VeriNode::IsUniqueNocaseName(const Array *list1, const Array *list2/*Viper 5133*/, const char *name)
{
     // Viper 5133 added checks to check both in the _parameters and _ports
     unsigned i ;
     VeriIdDef *id ;
     FOREACH_ARRAY_ITEM(list1, i, id) {
         if (!id) continue ;
         const char *id_name = id->GetName() ;
         if (!id_name) continue ;
         if (Strings::compare(name,id_name) == 1) continue ; // Viper 5000
         if (Strings::compare_nocase(name, id_name) == 1) return 0 ;
     }
     i = 0 ;
     FOREACH_ARRAY_ITEM(list2, i, id) {
         if (!id) continue ;
         const char *id_name = id->GetName() ;
         if (!id_name) continue ;
         if (Strings::compare(name,id_name) == 1) continue ; // Viper 5000
         if (Strings::compare_nocase(name, id_name) == 1) return 0 ;
     }
     return 1 ;
}

// This function escapes the name from Verilog if required
char *
VeriNode::CreateEscapedNameFromVerilog(const char *str, unsigned is_generic, const Array* ports, const Array* parameters)
{
    if (!str) return 0 ;
    (void) is_generic ;
    char *escaped_id_name = Strings::save(str) ;
    unsigned is_escaped = 0 ;
    if (RuntimeFlags::GetVar("veri_conv_entity_port_param_name_based_on_scope")) {
        // Viper 8005: Implement this flag to use IsUniqueNocaseName to detemine escaping.
        if (Strings::containupper(escaped_id_name) &&
            !IsUniqueNocaseName(is_generic ? parameters : ports, is_generic ? ports : parameters, escaped_id_name)) is_escaped = 1 ;
    } else {
        // Viper 7667: Create escape name in the converted entity if it contains a upper case
        if (Strings::containupper(escaped_id_name)) is_escaped = 1 ;
    }

    Strings::free(escaped_id_name) ;
    escaped_id_name = VhdlTreeNode::CreateLegalIdentifier(str) ;
    if (is_escaped && escaped_id_name && escaped_id_name[0] != '\\' && escaped_id_name[Strings::len(escaped_id_name) - 1] != '\\') {
        char *tmp = escaped_id_name ;
        escaped_id_name = Strings::save("\\", escaped_id_name, "\\") ;
        Strings::free(tmp) ;
    }
#ifndef VHDL_PRESERVE_ID_CASE
    if (!is_escaped) escaped_id_name = Strings::strtolower(escaped_id_name) ;
#endif
    return escaped_id_name ;
}

// Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922

// Conversion routines for names and literals
// Viper 7225: These are used to convert actuals during
// module instantiation conversion
VhdlName *VeriIdRef::ConvertName() const
{
    VhdlName* conv_name = new VhdlIdRef(Strings::save(_name)) ;
    conv_name->SetLinefile(Linefile()) ;
    return conv_name ;
}

VhdlName *VeriSelectedName::ConvertName() const
{
    VhdlName *prefix = _prefix ? _prefix->ConvertName() : 0 ;
    VhdlDesignator *suffix = _suffix ? new VhdlIdRef(Strings::save(_suffix)) : 0 ;
    VhdlName* conv_name = new VhdlSelectedName(prefix, suffix) ;
    conv_name->SetLinefile(Linefile()) ;
    return conv_name ;
}

VhdlName* VeriIndexedId::ConvertName() const
{
    VhdlName *prefix = _prefix ? _prefix->ConvertName() : 0 ;
    Array *assoc_list = new Array(1) ;
    if (_idx) {
        if (_idx->FullId()) {
            assoc_list->InsertLast(_idx->ConvertName()) ;
        } else if (_idx->IsRange()) {
            assoc_list->InsertLast(_idx->ConvertRange()) ;
        } else {
            assoc_list->InsertLast(_idx->ConvertExpr()) ;
        }
    }
    VhdlName *conv_name = new VhdlIndexedName(prefix, assoc_list) ;
    conv_name->SetLinefile(Linefile()) ;
    return conv_name ;
}
VhdlRange* VeriRange::ConvertRange() const
{
    if (_left && _right) {
        verific_int64 left_bound = 0 ;
        verific_int64 right_bound = 0 ;
        VhdlExpression* left_expr = 0 ;
        VhdlExpression* right_expr = 0 ;
        VeriIdDef *left_id = _left->FullId() ;
        VeriIdDef *right_id = _right->FullId() ;

        if (_left->IsConst()) {
            left_bound = _left->Integer() ;
            left_expr = new VhdlInteger(left_bound) ;
            left_expr->SetLinefile(_left->Linefile()) ;
        } else if (left_id){
            left_expr = new VhdlIdRef(Strings::save(left_id->Name())) ;
        }

        if (_right->IsConst()) {
            right_bound = _right->Integer() ;
            right_expr = new VhdlInteger(right_bound) ;
            right_expr->SetLinefile(_right->Linefile()) ;
        } else if (right_id) {
            right_expr = new VhdlIdRef(Strings::save(right_id->Name())) ;
        }

        if (_left->IsConst() && _right->IsConst()) {
            delete right_expr ;
            delete left_expr ;
            VhdlRange* conv_range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;
            if (conv_range) conv_range->SetLinefile(Linefile()) ;
            return conv_range ;
        } else if (left_expr && right_expr) {
            VhdlRange* conv_range = VhdlTreeNode::CreateVhdlRangeWithExpr(left_expr, right_expr) ;
            if (conv_range) conv_range->SetLinefile(Linefile()) ;
            return conv_range ;
        } else {
            // JJ: need to clean up in this case, did not assign new memory.
            if (left_expr) delete left_expr ;
            if (right_expr) delete right_expr ;
        }
    }
    return 0 ;
}

VhdlExpression* VeriIntVal::ConvertExpr() const
{
    VhdlExpression *conv_expr = new VhdlInteger((verific_int64)(_num)) ;
    conv_expr->SetLinefile(Linefile()) ;
    return conv_expr ;
}

VhdlExpression* VeriRealVal::ConvertExpr() const
{
    VhdlExpression *conv_expr = new VhdlReal(_num) ;
    conv_expr->SetLinefile(Linefile()) ;
    return conv_expr ;
}
#endif

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// Viper 6972: Perform Semantic checks in static elaboration for Vhdl units instantiated
// in Verilog. We do not perform size mismatch checks between formals and actuals here.
void
VeriVhdlNode::CheckInstantiation(const VeriModuleInstantiation *instance) const
{
    // Get the instantiated module for this instance:
    VhdlPrimaryUnit *unit = GetVhdlUnit() ;
    if (unit && unit->IsPrimaryUnit()) {
        if (!instance || unit->GetCompileAsBlackbox()) return ; // Can't do anything without these
        CheckVhdlSemantics(unit, instance) ;
    }
}

void VeriPseudoTreeNode::CheckVhdlSemantics(const VhdlPrimaryUnit *unit, const VeriModuleInstantiation *instance)
{
    if (!unit || !instance) return ;

    // in a map against formal name
    // We need to carry-out some semantic checks on the formal-actual pair here:
    Array *instance_ids = instance->GetIds() ;
    VhdlIdDef *unit_id = unit->Id() ;
    VhdlScope *unit_scope = (unit_id) ? unit_id->LocalScope() : 0 ;
    Array *unit_ports = (unit_id) ? unit_id->GetPorts() : 0 ;
    unsigned i ;
    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(instance_ids, i, inst) {
        if (!inst) continue ;
        // Get the connected actuals from the instance identifier:
        Array *actuals = inst->GetPortConnects() ;
        unsigned j ;
        VeriExpression *actual ;
        FOREACH_ARRAY_ITEM(actuals, j, actual) {
            if (!actual) continue ;
            VhdlIdDef *formal_id = 0 ;
            // Get the corresponding formal port (identifier and/or name) and its size:
            if (unit_id && unit_scope) {
                // Find the formal
                const char *formal_name = actual->NamedFormal() ;
                if (formal_name) {
                    // This is a (SV-style) named formal.
                    formal_id = unit_scope->FindSingleObjectLocal(actual->NamedFormal()) ;
                    //formal = unit->GetPort(actual->NamedFormal()) ;
                    // VIPER #6024: Do not produce the error in case the found net is implicitly declared:
                    if (formal_id && !formal_id->IsPort()) formal_id = 0 ;
                } else {
                    // This is a ordered formal
                    if (j>=((unit_ports) ? unit_ports->Size() : 0)) {
                        break ; // no use checking more.
                    }
                    formal_id = (unit_ports) ? (VhdlIdDef*)unit_ports->At(j) : 0 ;
                }
            }
            if (!formal_id) continue ; // Something bad happened
            VeriExpression *actual_expr = actual->GetConnection() ; // get actual expression
            CheckVhdlOutputPortConnection(formal_id, actual_expr) ;
        }
    }
}

void
VeriPseudoTreeNode::CheckVhdlOutputPortConnection(const VhdlIdDef *formal_id, const VeriExpression *actual_expr)
{
    // VIPER #3577: Check for illegal output port connection with operators:
    // Legal output port connections are: hier-id, indexed-id and concat
    VeriIdDef *actual_id = (actual_expr) ? actual_expr->GetId(): 0 ;
    // Viper 8192: For dot-star search in the scope to find the actual id. Function GetActualIdForFormal does this
    if (actual_expr && !actual_id && formal_id && actual_expr->IsDotStar()) actual_id = actual_expr->GetActualIdForFormal(formal_id->Name(), 1/*is_vhdl*/) ;
    if (formal_id && !formal_id->IsInput() && actual_expr && !actual_expr->IsOpen() && // Ignore open ports
        ((actual_id && actual_id->IsFunction()) ||
         (!actual_id && !actual_expr->IsHierName()))) { // VIPER #5590: Ignore unresolved hier-names
        if (!actual_expr->IsConcat()) {
            // GetId() will return valid for idref, indexed id etc but not for operators:
            actual_expr->Error("illegal output port connection to %s", formal_id->Name()) ;
        } else {
            // Viper 4861 check individually for concats
            unsigned i ;
            VeriExpression *expr ;
            Array* exprs = actual_expr->GetExpressions() ;
            FOREACH_ARRAY_ITEM(exprs, i, expr) {
                if (expr) CheckVhdlOutputPortConnection(formal_id, expr) ;
            }
        }
    }
}
#endif

VeriIdDef* VeriModule::GetParamFromVhdlName(const char* name) const
{
    if (!name) return 0 ;
    VeriIdDef* formal_id = GetParam(name) ;
    if (formal_id) return formal_id ;
    // Find the parameter in this module :
    // Because of dual-language support, and top-level parameter support, we need
    // to make the following GetParam(...) do a case-insensitive lookup. (VIPER #1721)
    // The following is may be very slow!
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_parameters, i, id) {
        if (Strings::compare_nocase(id->Name(), name)) {
            formal_id = id ;
            break ;
        }
    }
    if (formal_id) return formal_id ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // It is possible that the generic name is escaped.. Unescape it
    // and then look for it.
    char *unescaped_name = VhdlNode::UnescapeVhdlName(name) ;
    if (unescaped_name) {
        // Get rid of the front and the trailing escape characters
        // Now look for it.
        formal_id = GetParam(unescaped_name) ;
        if (!formal_id) {
            // Ok.. Now case insensitive search
            FOREACH_ARRAY_ITEM(_parameters, i, id) {
                if (Strings::compare_nocase(id->Name(), unescaped_name)) {
                    formal_id = id ;
                    break ;
                }
            }
        }
    }
    if (formal_id) return formal_id ;

    if (unescaped_name) {
        // Still not found.. Last try.. Escape the name
        // in verilog and then look for it...
        char *param_name = Strings::save("\\", unescaped_name, " ") ;
        // param name is verilog escaped name..
        formal_id = GetParam(param_name) ;
        if (!formal_id) {
            FOREACH_ARRAY_ITEM(_parameters, i, id) {
                if (Strings::compare_nocase(id->Name(), param_name)) {
                    formal_id = id ;
                    break ;
                }
            }
        }
        Strings::free(param_name) ;
    }

    if (unescaped_name) Strings::free(unescaped_name) ;
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
    return formal_id ;
}

VeriIdDef* VeriModule::GetPortFromVhdlName(const char* name) const
{
    if (!name) return 0 ;
    VeriIdDef* formal_id = GetPort(name) ;
    if (formal_id) return formal_id ;
    // Find the parameter in this module :
    // Because of dual-language support, and top-level parameter support, we need
    // to make the following GetParam(...) do a case-insensitive lookup. (VIPER #1721)
    // The following is may be very slow!
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ports, i, id) {
        if (Strings::compare_nocase(id->Name(), name)) {
            formal_id = id ;
            break ;
        }
    }
    if (formal_id) return formal_id ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // It is possible that the generic name is escaped.. Unescape it
    // and then look for it.
    char *unescaped_name = VhdlNode::UnescapeVhdlName(name) ;
    if (unescaped_name) {
        // Get rid of the front and the trailing escape characters
        // Now look for it.
        formal_id = GetPort(unescaped_name) ;
        if (!formal_id) {
            // Ok.. Now case insensitive search
            FOREACH_ARRAY_ITEM(_ports, i, id) {
                if (Strings::compare_nocase(id->Name(), unescaped_name)) {
                    formal_id = id ;
                    break ;
                }
            }
        }
    }
    if (formal_id) return formal_id ;

    if (unescaped_name) {
        // Still not found.. Last try.. Escape the name
        // in verilog and then look for it...
        char *port_name = Strings::save("\\", unescaped_name, " ") ;
        // param name is verilog escaped name..
        formal_id = GetPort(port_name) ;
        if (!formal_id) {
            FOREACH_ARRAY_ITEM(_ports, i, id) {
                if (Strings::compare_nocase(id->Name(), port_name)) {
                    formal_id = id ;
                    break ;
                }
            }
        }
        Strings::free(port_name) ;
    }

    if (unescaped_name) Strings::free(unescaped_name) ;
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
    return formal_id ;
}
