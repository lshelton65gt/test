/*
 *
 * [ File Version : 1.394 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_MODULEITEM_H_
#define _VERIFIC_VERI_MODULEITEM_H_

// As of 8/2004, VeriStatement (sequential stmts) is derived from VeriModuleItem (concurrent statements)
// These two classes have a lot in common, so it is natural to derive them.
// Makes parser cleaner also.
// The choice of VeriModuleItem as the base class (as opposed to VeriStatement) is somewhat arbitrary.
#include "VeriTreeNode.h"
#include "Array.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Map ;

class VeriDataType ;
class VeriRange ;
class VeriStrength ;
class VeriStatement ;
class VeriGenVarAssign ;
class VeriPath ;
class VeriIdDef ;
class VeriIdRef ;
class VeriExpression ;
class VeriRangeOrType ;
class VeriDataFlow ;
class VeriScope ;
class VeriLibrary ;
class VeriName ;

class Library ;
class Netlist ;
class Net ;
class VeriElabGenerate ;
class VeriBaseValue ;
class ValueTable ;
class VeriPseudoTreeNode ;
class VhdlComponentInstantiationStatement ;
class VhdlInstantiatedUnit ;

class VeriVarUsageInfo ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896:
class VhdlScope ;
class VhdlTypeId ;
class VhdlDeclaration ;
class VhdlElementDecl ;
#endif

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriModuleItem  : public VeriTreeNode
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Cast to ModuleInstance
    virtual VeriModuleInstantiation* CastModuleInstance() { return 0 ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    // Resolve this item (analysis at module parsing time).
    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;

    // Variable usage analysis (analysis after all modules are parsed)
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsModule() const                        { return 0 ; } // Test if this module item is a VeriModule
    virtual unsigned            IsRootModule() const                    { return 0 ; }
    virtual unsigned            IsStatement() const                     { return 0 ; } // Test if this module item is a (sequential) VeriStatement
    virtual unsigned            IsDataDecl() const                      { return 0 ; } // Test if this is a data declaration (IOdecl, ParamDecl, RegDecl, NetDecl etc).
    virtual unsigned            IsTypeAlias() const                     { return 0 ; } // _data_type is VERI_TYPEDEF or VERI_NETTYPE
    virtual unsigned            IsInstantiation() const                 { return 0 ; } // Test if this is a (module) instantiation.
    // Tests for various data declarations :
    virtual unsigned            IsParamDecl() const                     { return 0 ; } // Item was previously a VeriParamDecl.. now a VeriDataDecl.
    virtual unsigned            IsLocalParamDecl() const                { return 0 ; } // Item was previously a VeriLocalParamDecl.. now a VeriDataDecl.
    virtual unsigned            IsSpecParamDecl() const                 { return 0 ; } // Item was previously a VeriSpecParamDecl.. now a VeriDataDecl.
    virtual unsigned            IsRegDecl() const                       { return 0 ; } // Item was previously a VeriRegDecl.. now a VeriDataDecl.
    virtual unsigned            IsNetDecl() const                       { return 0 ; } // VeriNetDecl class still there.
    virtual unsigned            IsIODecl() const                        { return 0 ; } // Item was previously a VeriIODecl.. now a VeriDataDecl.
    virtual unsigned            IsGenVarDecl() const                    { return 0 ; } // Item was previously a VeriGenVarDecl.. now a VeriDataDecl.
    virtual unsigned            IsEventDecl() const                     { return 0 ; } // Item was previously a VeriEventDecl.. now a VeriDataDecl.
    // Test that this module item is a generate-for construct (a generate loop).
    virtual unsigned            IsGenerateFor() const                   { return 0 ; } // test if this is a generate-for construct.
    // Test that this module item is a generate-block construct (a begin...end block in generate construct).
    virtual unsigned            IsGenerateBlock() const                 { return 0 ; } // test if this is a generate-block construct.
    virtual unsigned            IsGenerateConditional() const           { return 0 ; } // test if this is a generate if/case construct
    virtual unsigned            IsGenerateConstruct() const             { return 0 ; } // test if this is a generate endgenerate construct
    virtual unsigned            IsPropertyDecl() const                  { return 0 ; }  // Tests if this module item is a VeriPropertyDecl
    virtual unsigned            IsSequenceDecl() const                  { return 0 ; }  // Tests if this module item is a VeriSequenceDecl
    virtual unsigned            IsConstraintDecl()                      { return 0 ; }  // Tests if this module item is a VeriConstraintDecl
    virtual unsigned            IsClass() const                         { return 0 ; }  // Tests if this module item is a VeriClass
    virtual unsigned            IsCovergroup() const                    { return 0 ; }  // Tests if this module item is a VeriCovergroup
    virtual unsigned            IsProgram() const                       { return 0 ; }  // Tests if this module item is a VeriProgram
    virtual unsigned            IsAnonymousProgram() const              { return 0 ; }  // VIPER #4701 : Return 1 for anonymous program
    virtual unsigned            IsInterface() const                     { return 0 ; }  // Tests if this module item is a VeriInterface
    virtual unsigned            IsPrimitive() const                     { return 0 ; } // Tests if this module item is a primitive module
    virtual unsigned            IsFunctionDecl() const                  { return 0 ; }  // Tests if this module item is a VeriFunctionDecl
    virtual unsigned            IsTaskDecl() const                      { return 0 ; }  // Tests if this module item is a VeriTaskDecl
    virtual unsigned            IsPackage() const                       { return 0 ; }  // Tests if this module item is a VeriPackage
    virtual unsigned            IsChecker() const                       { return 0 ; }  // Tests if this module item is a VeriChecker
    virtual unsigned            IsBindDirective() const                 { return 0 ; }  // Tests if this module item is a VeriBindDirective
    virtual unsigned            IsTable() const                         { return 0 ; }  // Tests if this module item is a VeriTable
    virtual unsigned            IsConfiguration() const                 { return 0 ; }  // Tests if this module item is a VeriConfiguration
    virtual unsigned            IsClocking() const                      { return 0 ; }  //  Tests if this module item is a VeriClockingDecl
    virtual unsigned            IsLetDecl() const                       { return 0 ; }  // Tests if this module item is a VeriLetDecl
    virtual unsigned            IsConcurrentAssertion() const           { return 0 ; } // Return 1 for concurrent assertion statement
    // VIPER #6922: Marking a statement as sequential block:
    virtual unsigned            IsSeqBlock() const                      { return 0 ; }
    virtual unsigned            IsParBlock() const                      { return 0 ; }

    // VIPER #6896: Conversion of verilog package to vhdl package:
    virtual unsigned            IsImportDecl() const                    { return 0 ; }  // Return 1 for import declaration
    virtual unsigned            IsExportDecl() const                    { return 0 ; }  // Return 1 for export declaration
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl package to verilog one:
    virtual unsigned            IsVhdlPackage() const                   { return 0 ; } // Flag indicating that the processing package is a converted verilog package from a vhdl package
    virtual unsigned            IsConvertedToVhdlPackage() const        { return 0 ; } // Flag to indicate that this verilog package is converted to vhdl
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION

    // VIPER #3276: APIs to check whether this function is called as constant and can be a constant in nature
    virtual unsigned            CanBeConstFunction() const              { return 0 ; }  // Tests if this module item (VeriFunctionDecl) can be a constant function, only set after Resolve step is over
    virtual unsigned            IsCalledAsConstFunction() const         { return 0 ; }  // Tests if this module item (VeriFunctionDecl) is called as a constant function, only set after Resolve step is over
    virtual unsigned            IsVerilogMethod() const                 { return 1 ; }  // Defined on VeriFunctionDecl
    virtual unsigned            IsDPIMethod() const                     { return 0 ; }

    virtual VeriDataDecl *      DataDeclCast() const                    { return 0 ; }

    // VIPER #7249: Set label, we are about to invalidate a _label pointer
    virtual void                SetId(VeriIdDef* /*label*/)             { } // Only for sequential and parallel block

    // Virtual accessor methods
    virtual VeriScope *         GetScope() const                        { return 0 ; } // The local scope (if any) of this module item
    virtual unsigned            GetType() const                         { return 0 ; } // Added for VIPER #4244, to know the correct type of VeriCoverageSpec
    virtual Array *             GetPorts() const                        { return 0 ; } // For task/function/module decls. Return array of VeriIdDef* of their ports in order.
    virtual Array *             GetParameters() const                   { return 0 ; } // Parameter id's in order Array of VeriIdDef.
    virtual Array *             GetItems() const                        { return 0 ; } // For module/interface/program/class/generates. Return array of VeriModuleItem*, item list.
    virtual Array *             GetIds() const                          { return 0 ; } // Various VeriModuleItem derivatives (declarations) have id lists (list of declared identifiers).
    virtual VeriIdDef *         GetId() const                           { return 0 ; } // Various VeriModuleItem derivatives (declarations) have a single id (declared identifier).
    virtual VeriDataType *      GetDataType() const                     { return 0 ; } // Various VeriModuleItem derivatives (declarations) have a data type (explicitly set by user).
    virtual unsigned            GetDeclType() const                     { return 0 ; } // Various VeriModuleItem derivatives (declarations) have the declaration type (see tokens below)
    virtual VeriName *          GetBaseClassName() const                { return 0 ; } // For VeriClass. Returns the base class name
    virtual Array *             GetInterfaceClasses() const             { return 0 ; }
    virtual VeriName *          GetSubprogramName() const               { return 0 ; } // Get full name of subprogram. Name will be like class_name::subpro for external subprogram def (4038)
    virtual Array *             GetDefParamAssigns() const              { return 0 ; } // Array of VeriDefParamAssign's.
    virtual VeriIdDef  *        GetBaseClassId(unsigned & /*is_base_typeparam*/) { return 0 ; }
    virtual unsigned            GetInstType() const                     { return 0 ; }
    virtual VeriExpression *    GetSpec() const                         { return 0 ; }      // The (sequence/property) spec (implementation of this named sequence)
    virtual VeriStatement *     GetStmt() const                         { return 0 ; } // VIPER #6667
    virtual VeriStrength *      GetStrength() const                     { return 0 ; } // VIPER #8502
    virtual VeriIdDef *         GetOpeningLabel() const                 { return 0 ; } // Return label of a statement (SV only)
    virtual VeriExpression *    GetExpression() const                   { return 0 ; }  // The expression of this let declaration
    virtual unsigned            GetAnalysisDialect() const              { return 0 ; } // mode (dialect) how this module was parsed. Encoding of dialects is in veri_file (defined only in VeriModule)

    virtual VeriName *          GetResolutionFunction() const           { return 0 ; }  // SV 1800-2012 section 6.6.7 user defined nettype with resolution functions : defined on VeriDataDecl

    // VIPER #3369
    virtual void                ClearId()                               { } // Deafault catcher for module items having no id
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    virtual VeriScope *         GetOwningScope() const                  { return 0 ; } // Return the container scope
#endif
    virtual unsigned            GetAutomaticType() const                { return 0 ; } // defined for VeriTaskDecl/VeriFuncDecl
    virtual unsigned            HasDelayOrEventControl() const          { return 0 ; } // defined for VeriTaskDecl
    virtual VeriTable *         GetPrimitiveTable() const               { return 0 ; } // Defined on VeriPrimitive/VeriTable classes
    virtual Array *             GetIOList() const                       { return 0 ; } // list of formal ansi-port decls defined for VeriFunctionDecl

    // Modifier methods
    virtual void                AddDataDecl(VeriIdDef * /*id*/)         { } // Only defined in VeriDataDecl
    virtual unsigned            IsClassInstantiated() const             { return 0 ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const = 0 ; // Pure virtual function to copy module item
    virtual VeriModuleItem *    CopyWithName(const char * /*name*/, VeriMapForCopy & /*id_map_table*/, unsigned /*add_copied_obj*/) { return 0 ; } // Copy module item with the first argument specific name
    virtual Array *             InlineDataType(VeriMapForCopy & /*id_map_table*/) const { return 0 ; } // Copy data declaration, with uniquified/inlined data_type for each identifier.  Needed for VIPER #7277

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildBy(VeriModuleItem * /*old_item*/, const Array * /*new_items*/) { return 0 ; } // Virtual routine to replace module/interface instantiation array  by bunch of single instantiations

    // VIPER #6432 : Add default argument to these ReplaceChildXXX Apis, so that caller can determine
    // whether they want to delete old parse tree or not. By default, old parse tree is delete, but
    // if ReplaceChildXXX is called passing 0 as third argument, old parse tree will not be deleted.
    unsigned                    ReplaceChildName(VeriName *old_parent, VeriName *new_parent, unsigned delete_old_node=1) { return ReplaceChildNameInternal(old_parent, new_parent, delete_old_node) ; } // Delete old name, and put new name
    unsigned                    ReplaceChildId(VeriIdDef *old_id, VeriIdDef *new_id, unsigned delete_old_node=1) { return ReplaceChildIdInternal(old_id, new_id, delete_old_node) ; } // Delete old id, and put new id
    unsigned                    ReplaceChildExpr(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node=1) { return ReplaceChildExprInternal(old_expr, new_expr, delete_old_node) ; } // Delete 'old_expr', and puts new expression in its place
    unsigned                    ReplaceChildStmt(VeriStatement *old_statement, VeriStatement *new_statement, unsigned delete_old_node=1) { return ReplaceChildStmtInternal(old_statement, new_statement, delete_old_node) ; } // Delete old statement, and put new statement
    unsigned                    ReplaceChildModuleItem(VeriModuleItem *old_decl, VeriModuleItem *new_decl, unsigned delete_old_node=1) { return ReplaceChildModuleItemInternal(old_decl, new_decl, delete_old_node) ; } // Delete old declaration item, and put new declaration item
    unsigned                    ReplaceChildDataType(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node=1) { return ReplaceChildDataTypeInternal(old_data, new_data, delete_old_node) ; } // Delete 'old_data' specific data type, and puts new data type in its place

    unsigned                    ReplaceChildClockingDir(VeriClockingDirection *old_dir, VeriClockingDirection *new_dir, unsigned delete_old_node=1) { return ReplaceChildClockingDirInternal(old_dir, new_dir, delete_old_node) ; } // Delete 'old_dir', and puts new clocking direction in its place.

    virtual VeriModule *        GetOriginalModule() const { return 0; } // Get the module from which this module is copied. Implemented for VeriModule
    virtual const char *        GetOriginalModuleName() const { return 0 ; }  // Get the module name from which this module is copied
    virtual unsigned            IsElaborated() const { return 0 ; }
    virtual const char *        GetOriginalClassName() const   { return 0 ; }
    // VIPER #8360: Will work only when runtime flag 'veri_add_attribute_to_array_instance_created_instance_id' is set
    unsigned                    IsArrayInstanceElaborationCreated() const ; // Returns 1 if this is instantiation, created from static elaboration of array instance (array instance to multiple single instances)

    // Find the instantiated module (only does something for module instantiations)^M
    virtual VeriModule *        GetInstantiatedModule() const { return 0 ; } // Find the instantiated VeriModule, and return it.
    //VeriModule *                GetMaster() const             { return GetInstantiatedModule() ; } // Backward compatibility routine. Will be removed at some point.

    virtual unsigned            IsTimeUnitDecl()  const        { return 0 ; }

    // Local/Virtual Accessor Methods to parse-tree sub-nodes for time literal
    virtual VeriExpression *    GetTimeLiteral() const         { return 0 ; }
    virtual unsigned            GetTimeUnitType() const        { return 0 ; }
    virtual VeriExpression *    GetTimePrecision() const       { return 0 ; } // Returns optional time precision if specified in timeunit declaration
    virtual VeriExpression *    GetClockingEvent() const       { return 0 ; } // Defined for VeriClockingDecl

    // Qualifiers (static, automatic, virtual, signed, unsigned etc, etc). Mostly used for SV.
    // Qualifiers are accessed as tokens (VERI_AUTOMATIC etc), but are all stored
    // as bit-flags in a unsigned (32 bit) field in the common VeriModuleItem class.
    // A full list of applicable qualifiers is available in VeriModuleItem.cpp.
    void                        SetQualifier(unsigned token) ;
    virtual unsigned            GetQualifier(unsigned token) const ; // Viper #6948: made virtual
    virtual VeriIdDef  *        GetNewFunctionId()              { return 0 ; }  // Only for class
    virtual void                SetNewFunctionId(VeriIdDef * /*new_id*/) { }  // id for the new function

    // Get (Set) the library in which this module is compiled
    virtual VeriLibrary *       GetLibrary() const                  { return 0 ; } // defined for VeriModule
    virtual void                SetLibrary(VeriLibrary * /*lib*/)        {  } // defined for VeriModule

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected : // Abstract class
    VeriModuleItem() ;

    VeriModuleItem(const VeriModuleItem &mod_item, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriModuleItem(SaveRestore &save_restore) ;

public:
    virtual ~VeriModuleItem() ;

private:
    // Prevent compiler from defining the following
    VeriModuleItem(const VeriModuleItem &) ;            // Purposely leave unimplemented
    VeriModuleItem& operator=(const VeriModuleItem &) ; // Purposely leave unimplemented
public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual VeriIdDef *         FindSingleGenVarId() const { return 0 ; } // VIPER #6688 : Defined for for-generate

    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const { PrettyPrint(f,level) ; } // Default catch for Print statement without semi colon at end. Differs from default for only a few classes.
    virtual void                CollectRandRandCVars(Map * /*rand_randc*/) { } ;
    virtual unsigned            HasStaticMembers() { return 0 ; }
    virtual void                SetCalledAsConstFunction()              { /* do nothing */ }
    virtual void                PopulateToBeImplementedMethods(Map * /*methods*/, unsigned /*ignore_interface_classes*/) const {} ;
    virtual void                ValidateImplementedMethod(VeriModuleItem * /*subprog_decl*/, Map * /*methods*/, unsigned /*virtual_class*/, unsigned /*ignore_pure_virtual*/) {}

    // VIPER #5954 : Identifier for non-ansi ports
    virtual VeriIdDef *         DeclareNonAnsiPort(const char * /*name*/, unsigned /*dir*/, VeriExpression * /*port_expr*/, unsigned /* port_idx */) { return 0 ; } // Declare non-ansi port (defined only for VeriModule)
    virtual VeriIdDef *         GetPort(const char * /*name*/) const { return 0 ; } // Get port from module

    // Accumulate declared identifiers VeriIdDefs (ports, parameters) in order of declaration over multiple module items, into the provided array.
    virtual void                AccumulatePorts(Array& /*ports*/) const { } ;
    virtual void                AccumulateParameters(Array& /*parameters*/, unsigned /*has_ansi_parameter_list*/, const VeriIdDef * /*mod_id*/) const { } ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    // VIPER #5727 : Internal routine for unnamed generate block handling
    virtual void ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_no, Set *block_names, Set *ignore_scopes) ;
    VeriIdDef *  CreateUnnamedBlockId(VeriScope *enclosing_scope, unsigned gen_blk_num, int branch_num, const VeriIdDef *block_id, const VeriTreeNode *from, Set *ignore_scopes) const ;
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK

    virtual void                DeleteOnlyBlock()                       {} // Delete generate block without its members
    virtual VeriModuleItem *    GetDataDecl(VeriIdDef * /*vid*/, unsigned /*is_constraint*/) const { return 0 ; }
    virtual void                SetTheModule(VeriModule * /*module*/)   { }
    virtual VeriScope *         GetPortScope() const  { return 0 ; } // Returns scope generated for to store named port like (.a(expr)) (exists in relaxed checking mode)
    virtual char *              GetTimeUnitDefinedTimeScale() const { return 0 ; } // VIPER #8304: Returns allocated timescale creating from timeunit and timeprecision declaration
    // VIPER #4286: Update modports module item back pointer (we do it after restoring a full interface decl):
    virtual void                UpdateModportPorts()                    { /* virtual catcher: do nothing */ }
    // Verific internal routine:
    virtual unsigned            ContainsInterfaceMethod() const         { return 0 ; }
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896:
    virtual VhdlDeclaration*    ConvertDataDecl(Array* /*decl_part*/, VhdlScope* /*vhdl_scope*/, VeriScope* /*veri_scope*/) { return 0 ; }
    virtual unsigned            CreateElementDecl(Array* /*decl_part*/, Array* /*record_decls*/, VhdlScope* /*record_scope*/, VhdlScope* /*vhdl_scope*/, VeriScope* /*veri_scope*/, VhdlTypeId* /*struct_id*/) { return 0 ; } // for struct elements
#endif

    unsigned                    ReplaceChildByInternal(Array *items, VeriModuleItem *old_item, const Array *new_items) ; // Non-virtual internal routine to replace module/interface instantiation array  by bunch of single instantiations
    // Internal routines :
    virtual unsigned            ReplaceChildNameInternal(VeriName * /*old_parent*/, VeriName * /*new_parent*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old name, and put new name
    virtual unsigned            ReplaceChildIdInternal(VeriIdDef * /*old_id*/, VeriIdDef * /*new_id*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old id, and put new id
     virtual unsigned           ReplaceChildExprInternal(VeriExpression * /*old_expr*/, VeriExpression * /*new_expr*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_expr', and puts new expression in its place
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement * /*old_statement*/, VeriStatement * /*new_statement*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old statement, and put new statement
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem * /*old_decl*/, VeriModuleItem * /*new_decl*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old declaration item, and put new declaration item
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType * /*old_data*/, VeriDataType * /*new_data*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_data' specific data type, and puts new data type in its place
    virtual unsigned            ReplaceChildClockingDirInternal(VeriClockingDirection * /*old_dir*/, VeriClockingDirection * /*new_dir*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_dir', and puts new clocking direction in its place.
    virtual void                ElaboratePortConnections(VeriScope * /*scope_of_instantiation*/) {}
    virtual VeriIdDef *         GetParentClassId(unsigned & /*is_base_typeparam*/) { return 0 ; }

    // Internal routines for Static Elaboration
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set * /*defparams*/) { return 1 ; }
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ; // Copy module item and replace genvar with proper value
    virtual VeriModuleItem *    StaticElaborateGenerateInternal(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ; // Copy module item and replace genvar with proper value
    virtual VeriBaseValue *     StaticElaborateFunction(Array * /*arg_list*/, ValueTable * /*n*/, unsigned /*expect_nonconst*/)   { return 0 ; } // Defined for function decl, Evaluate constant function
    virtual unsigned            StaticElaborateStmt(ValueTable * /*eval*/, unsigned /*expect_nonconst*/)                          { return 0 ; } // Execute a statement (hidden as a VeriModuleItem)

    // Elaborates .* port connections of module instantiations and set 'is_prameterized_construct' if the generate construct or array instances are parameterized
    virtual void                ProcessModuleItem(VeriScope * /* container_scope */, unsigned & /* is_prameterized_construct */) { }
    virtual void                SetIsElaborated()    { }
    virtual void                SetStaticElaborated(){ }

    virtual unsigned            Detach(VeriModuleItem * /*node*/)                                   { return 0 ; } // Detach the argument specific module item from its container.

    virtual unsigned            PortWidthDir(const char * /*port_name*/, unsigned /*port_pos*/, unsigned * /*dir*/, VeriIdDef ** /*master_id*/, const char ** /* formal_name */) { return 0 ; } // virtual routine to get module/interface port dir and size
    virtual void                StaticReplaceConstantExpr(unsigned /*is_const_index_context*/) {} // Replace range bounds, default value, bit/part/slice expressions
    virtual void                ConvertToMultipleSingleItems(Array * /*list*/, VeriModuleItem * /*container*/) {} // Convert comma separated module items to multiple single items

    unsigned                    InitializeDecls(const Array *decls, ValueTable *table, unsigned expect_nonconst) const ; // Initializes the given declarations
    unsigned                    ElaborateStatements(const Array *statements, ValueTable *table, unsigned expect_nonconst) const ; // Elaborates the given statements, returns the status
    virtual VeriIdDef *         StaticElaborateClass(Array *param_list, VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new) ;
    virtual void                SetClassInstantiated()         {}
    virtual unsigned            IsContainCopiedVersion() const { return 0 ; }
    virtual VeriModuleItem *    CopyWithDefaultParameter()     { return 0 ; }  // Copy class with default parameter setting or by appending _default
    virtual void                SetContainCopiedVersion()      {}
    virtual unsigned            IsResolved() const             { return 0 ; } // Defined for VeriClass
    VeriIdDef *                 BindExportedSubprogram(VeriName *name, const Array *pseudo_nodes) ;
    virtual void                CheckModportExportImport() const        { /* virtual catcher */ } // VIPER #7915
    virtual void                SetIsProcessing()              {}
    virtual unsigned            IsProcessing()                 { return 0 ; }
    virtual void                ResetIsProcessing()            {}

    virtual void                ProcessModuleInstInLibrary(VeriLibrary * /*lib*/, const VeriScope * /*module_scope*/)   { } // Process -v/-y/uselib library for this module item (only does something for module instantiations).

    virtual unsigned            PopulateExplicitlyInstantiatedModules(Set & /*top_modules*/)        { return 0 ; } // Populate explicitly instantiated modules in argument specific set

    virtual void                AccumulateInstantiatedModuleNames(Set & /*mod_names*/, Set & /*done_libs*/, Set & /*def_mods*/) { } // Internal routine to find instantiated module names iterating over module items, defined for instantiation, case-generate, if-generate, generate-block, generate
    Array *                     ProcessPostComments(Array *comments) ;
    virtual unsigned            AttachCommentToLastChild(const VeriCommentNode * /*comment*/)  { return 0 ; /* VIPER #7560: virtual catcher: do nothing */ }
    static unsigned             AttachCommentToLastChildInArray(const Array *children, const VeriCommentNode *comment) ; // VIPER #7560: Non-virtual routine to do the actual work
    void                        PrettyPrintPreComments(ostream &f, unsigned level) const ;
    void                        PrettyPrintPostComments(ostream &f, unsigned level, unsigned print_linefeed) const ;

    // Label Checking (made virtual, so it can apply to statements also). Absorbes the label_name.
    void                        ClosingLabel(char *label_name, VeriIdDef *last_label=0) const ; // Checks if this label_name matches the ModuleItem declared identifier. Applies only to some ModuleItems.
    virtual void                RemoveFromHierModule()         { } ; //Removes the _instantiation from the hier_module/hier_unit. for VeriBindDirective
    virtual void                CheckDecl(VeriModuleItem *f_decl)  const ;
    virtual char *              CreateNameWithDefaultparam(Array * /*param_list*/) const { return 0 ; }

public:
    // don't use these (for internal use only) :
    static unsigned             IndexOfQualifier(unsigned token) ;
    unsigned                    GetAllQualifiers() const                { return _qualifiers ; }

protected:
    // don't use this (for internal use only) :
    void                        SetAllQualifiers(unsigned qualifiers)   { _qualifiers = qualifiers ; }

    // VIPER #3855: Routine to elaborate generate items that are in the module scope:
    static unsigned             StaticElaborateModuleItemInternal(const Array *module_items, VeriModuleItem *generate, VeriModuleItem *mod, VeriLibrary *lib, const Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;

public:
    static unsigned             IsFirstLessThanSecondLiteral(const char *timeunit_lit, const char *timeprecision_lit) ; // VIPER #8304 : Compare time unit and time literal

protected:
    // _attributes moved to VeriNode static map. No of nodes with attribute should be small
    // compared to total number of nodes. It does not make sense from memory perspective to
    // keep a member on each object for such sparse data
    // Map         *_attributes ; // char* -> VeriExpression* mapping
    // _comment_arr moved to VeriNode static map for memory efficiency
    // Array       *_comment_arr ;
    unsigned     _qualifiers ; // Up to 32 bit flags, used to flag info on this ModuleItem.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriModuleItem

/* -------------------------------------------------------------- */

/* ******************* Decls as VeriModule Items ******************* */

// Since 9/2004, the following classes are merged into 1 single : VeriDataDecl.

// class VeriParamDecl : public VeriModuleItem
// class VeriLocalParamDecl : public VeriModuleItem
// class VeriIODecl : public VeriModuleItem
// class VeriRegDecl : public VeriModuleItem
// class VeriGenVarDecl : public VeriModuleItem
// class VeriEventDecl : public VeriModuleItem
// class VeriSpecParamDecl : public VeriModuleItem

// The 'style' of data declaration is still available as a token VeriDataDecl::GetDeclType(). See return token assignment below.
// All existing APIs on the base class (VeriModuleItem) still work (on VeriDataDecl)

class VFC_DLL_PORT VeriDataDecl : public VeriModuleItem
{
    // Most data declarations have a 'data_type', with 'range(s)' (packed dimensions), and a list of (declared) identifiers.
    // This class covers these :
    //    paramdecl           decl_type : VERI_PARAMETER
    //    local_paramdecl     decl_type : VERI_LOCALPARAM
    //    variable_decl (regs)decl_type : VERI_REG // for lack of a better token
    //    net_decl            decl_type : VERI_WIRE // for lack of a better token
    //    specparam_decl      decl_type : VERI_SPECPARAM
    //    io_decl             decl_type : direction (VERI_INOUT,VERI_IN,VERI_OUT...)
    //    genvar_decl         decl_type : VERI_GENVAR
    // and for System Verilog :
    //    type_decl (typedef) decl_type : VERI_TYPEDEF
    //    struct_union_member decl_type : VERI_STRUCT // for lack of a better token
    // possibly merge later also 'gate_instantion' and 'module_instantiation'.
    //    type_decl (nettype) decl_type : VERI_NETTYPE // 1800-2012 : 6.6.7
public:
    VeriDataDecl(unsigned decl_type, VeriDataType *data_type, Array *ids, VeriName *resolution_function = 0) ;
    VeriDataDecl(unsigned decl_type, VeriDataType *data_type, VeriIdDef *id, VeriName *resolution_function = 0) ; // constructor to start with a single id.

    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIDATADECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    // Resolve this item (resolve identifier references, do analysis-semantic checks etc).
    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Tests for various data declarations :
    virtual unsigned            IsDataDecl() const              { return 1 ; } // Test if this is a data declaration (IOdecl, ParamDecl, RegDecl, NetDecl etc).
    virtual unsigned            IsParamDecl() const ;       // Item was previously a VeriParamDecl.. now a VeriDataDecl.
    virtual unsigned            IsLocalParamDecl() const ;  // Item was previously a VeriLocalParamDecl.. now a VeriDataDecl.
    virtual unsigned            IsSpecParamDecl() const ;   // Item was previously a VeriSpecParamDecl.. now a VeriDataDecl.
    virtual unsigned            IsRegDecl() const ;         // Item was previously a VeriRegDecl.. now a VeriDataDecl.
    virtual unsigned            IsIODecl() const ;          // Item was previously a VeriIODecl.. now a VeriDataDecl.
    virtual unsigned            IsGenVarDecl() const ;      // Item was previously a VeriGenVarDecl.. now a VeriDataDecl.
    virtual unsigned            IsEventDecl() const ;       // Item was previously a VeriEventDecl.. now a VeriDataDecl.

    virtual unsigned            IsInterconnectType() const ; // data_type->GetType() == VERI_INTERCONNECT

    virtual VeriDataDecl *      DataDeclCast() const            { return const_cast<VeriDataDecl*>(this) ; }

    virtual unsigned            IsTypeAlias() const ;       // _decl_type is VERI_TYPEDEF or VERI_NETTYPE

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Modifier methods
    void                        SetDir(unsigned dir) ;       // Set the direction of this IO declaration and all of its ids to 'dir'. This makes this declaration a IO decl.
    virtual void                AddDataDecl(VeriIdDef *id) ; // Add another id to the list

    // Accessor Methods
    virtual VeriDataType *      GetDataType() const             { return _data_type ; } // The 'data_type' of this declaration (VERI_REG/WIRE etc in 2001, but more complex in System Verilog) plus 'packed' dimensions, and 'signing'.
    virtual Array *             GetIds() const                  { return (Array *)&_ids ; }   // The list of declared identifiers
    virtual unsigned            GetDeclType() const             { return _decl_type ; } // The declaration type (see tokens above)
    unsigned                    GetDir() const ; // direction (only applicable for IO declarations). Return 0 if no valid direction is given..
    virtual VeriName *          GetResolutionFunction() const   { return _resolution_function ; }  // SV 1800-2012 section 6.6.7 user defined nettype with resolution functions

    // Backward compatibility Accessor methods (these used to work on VeriRegDecl, VeriIODecl, VeriParamDecl, etc etc.
    unsigned                    GetSignedType() const ;
    VeriRange *                 GetRange() const ;
    unsigned                    GetParamType() const ;
    unsigned                    GetRegType() const ;
    unsigned                    GetIOType() const ;
    unsigned                    GetNetType() const ;

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy data declaration
    virtual Array *             InlineDataType(VeriMapForCopy &id_map_table) const ; // Copy data declaration, with uniquified/inlined data_type for each identifier. Needed for VIPER #7277

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDataDecl(const VeriDataDecl &data_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDataDecl(SaveRestore &save_restore) ;

    ~VeriDataDecl() ;

private:
    // Prevent compiler from defining the following
    VeriDataDecl() ;                                // Purposely leave unimplemented
    VeriDataDecl(const VeriDataDecl &) ;            // Purposely leave unimplemented
    VeriDataDecl& operator=(const VeriDataDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Accumulate declared identifiers VeriIdDefs (ports, parameters) in order of declaration into the provided array.
    virtual void                AccumulatePorts(Array& ports) const ;
    virtual void                AccumulateParameters(Array &parameters, unsigned has_ansi_parameter_list, const VeriIdDef *mod_id)  const ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896:
    virtual VhdlDeclaration*    ConvertDataDecl(Array *decl_part, VhdlScope *vhdl_scope, VeriScope *veri_scope) ;
    virtual unsigned            CreateElementDecl(Array *decl_part, Array *record_decls, VhdlScope *record_scope, VhdlScope *vhdl_scope, VeriScope *veri_scope, VhdlTypeId *struct_id) ; // for struct elements
#endif
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node) ; // Delete 'old_data' specific data type, and puts new data type in its place

    // Internal routines for Static Elaboration
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ;
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set *defparams) ;
    virtual unsigned            StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst) ;

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriDataType *_data_type ; // The 'data' type (VERI_REG/WIRE etc in 2001, but more complex in System Verilog) plus 'packed' dimensions, and 'signing'.
    Array         _ids ;       // The declared identifiers
    unsigned      _decl_type ; // The sort of identifiers we are declaring (see token list above). Stores 'direction' token for IO decls.

    // SV 1800-2012 section 6.6.7 user defined nettype with resolution functions
    VeriName     *_resolution_function ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDataDecl

/* -------------------------------------------------------------- */

/*
   10/2004 : NetDecl is now derived from VeriDataDecl. Every variable should have a VeriDataType pointer,
     so that initialization and elaboration can be done in a consistent way.
   NetDecl is derived from VeriDataDecl, because of the extra 'strength' and 'delay' info,
   and the different way in which the data type syntax is organized in net decls.
   The data_type in the VeriNetDecl contains the 'net_type', the (packed) 'range' and the 'signing' info.
*/
class VFC_DLL_PORT VeriNetDecl : public VeriDataDecl
{
public:
    VeriNetDecl(unsigned net_type, VeriStrength *strength, VeriRange *range, Array *delay, Array *ids, unsigned signed_type = 0) ; // traditional constructor
    VeriNetDecl(VeriDataType *data_type, VeriStrength *strength, Array *delay, Array *ids) ; // new constructor

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERINETDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsNetDecl() const           { return 1 ; } // VeriNetDecl class still there.

    // Accessor methods
    virtual VeriStrength *      GetStrength() const         { return _strength ; }
    Array *                     GetDelay() const            { return _delay ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy net declaration
    virtual Array *             InlineDataType(VeriMapForCopy &id_map_table) const ; // Copy data declaration, with uniquified/inlined data_type for each identifier. Needed for VIPER #7277

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNetDecl(const VeriNetDecl &net_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriNetDecl(SaveRestore &save_restore) ;

    virtual ~VeriNetDecl() ;

private:
    // Prevent compiler from defining the following
    VeriNetDecl() ;                               // Purposely leave unimplemented
    VeriNetDecl(const VeriNetDecl &) ;            // Purposely leave unimplemented
    VeriNetDecl& operator=(const VeriNetDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node) ; // Delete 'old_stre', and puts new strength in its place.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new delay expression in its place.

    // Internal routine(s) for Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

private:
    void Validate() ; // VIPER #6996 : Semantic checks
protected:
// Old members moved to VeriDataDecl base class.
//    unsigned       _net_type:16 ;
//    unsigned       _signed_type:16 ; // Actually the (VERI_SIGNED/VERI_UNSIGNED) token. For Verilog 2000. 0 for Verilog 95.
    VeriStrength    *_strength ;
//    VeriRange     *_range ;
    Array           *_delay ;   // Array of VeriExpression*. The delay : #(<delay1>,<delay2>,...)
//    Array         *_ids ;     // Array of VeriIdDef*. The declared identifiers.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNetDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriFunctionDecl : public VeriModuleItem
{
public:
// Since 8/2004, FunctionDecl has an Array of VeriStatements (GetStatements()) rather than a single one
//    VeriFunctionDecl(unsigned signed_type, unsigned automatic_type, VeriRange *range, unsigned type, VeriIdDef *func, Array *ansi_io_list, Array *decls, VeriStatement *stmt, VeriScope *scope) ;
// Since 9/2004, Function Decl uses a DataType to combine 'signing', 'range', and 'type'.
//    VeriFunctionDecl(unsigned signed_type, unsigned automatic_type, VeriRange *range, unsigned type, VeriIdDef *func, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) ;
// FIX ME : 'atomatic' should be a 'qualifier' flag.
    VeriFunctionDecl(unsigned automatic, VeriDataType *data_type, VeriIdDef *func, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) ;
    VeriFunctionDecl(unsigned automatic, VeriDataType *data_type, VeriName *func, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const                  { return ID_VERIFUNCTIONDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ; // can happen for task/function import/export decls
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriIdDef *         GetId() const                       { return _func ; } // VIPER 2607. Get the function id.
    VeriIdDef *                 GetFunctionId() const               { return _func ; }
    virtual VeriName *          GetSubprogramName() const           { return _name ; } // Get full name of subprogram. Name will be like class_name::subpro for external subprogram def
    virtual unsigned            GetAutomaticType() const            { return _automatic_type ; } // VERI_AUTOMATIC or 0.
    virtual VeriDataType *      GetDataType() const                 { return _data_type ; } // contains 'range', 'signing' and 'type' info for the return type of the function
    Array *                     GetAnsiIOList() const               { return _ansi_io_list ; } // list of formal ansi-port decls (id's (with direction,datatype etc)).
    virtual Array *             GetIOList() const                   { return _ansi_io_list ; } // list of formal ansi-port decls (id's (with direction,datatype etc)).
    Array *                     GetDeclarations() const             { return _decls ; } // local declarations (VeriModuleItems)
//    VeriStatement *GetStmt() const          { return _stmt ; }
    Array *                     GetStatements() const               { return _stmts ; } // the statements (VeriStatement)
    virtual VeriScope *         GetScope() const                    { return _scope ; }
    virtual Array *             GetPorts() const                    { return _ports ; } // ordered Array of VeriIdDef's (of the formal ports). Created in constructor.
// Backward compatibility accessor methods before there was a data type:
    VeriRange *                 GetRange() const ;
    unsigned                    GetType() const ;
    unsigned                    GetSignedType() const ;
    virtual unsigned            IsFunctionDecl() const              { return 1 ; }  // Affirms that this module item is a VeriFunctionDecl
    // VIPER #3276: APIs to check whether this function is called as constant and can be a constant in nature
    virtual unsigned            CanBeConstFunction() const          { return _can_be_const_func ; }
    virtual unsigned            IsCalledAsConstFunction() const     { return _is_const_called ; }
    virtual unsigned            IsVerilogMethod() const             { return _verilog_method ; }

    // VIPER #3369
    virtual void                ClearId()                           { _func = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy function declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriFunctionDecl(const VeriFunctionDecl &func_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriFunctionDecl(SaveRestore &save_restore) ;

    virtual ~VeriFunctionDecl() ;

private:
    // Prevent compiler from defining the following
    VeriFunctionDecl() ;                                    // Purposely leave unimplemented
    VeriFunctionDecl(const VeriFunctionDecl &) ;            // Purposely leave unimplemented
    VeriFunctionDecl& operator=(const VeriFunctionDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    void                        SetAutomaticType(unsigned automatic_type) { _automatic_type = automatic_type ; } // VERI_AUTOMATIC or 0.
    virtual void                SetCalledAsConstFunction()          { _is_const_called = 1 ; }
    void                        SetNotVerilogMethod()               { _verilog_method = 0 ; }
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node) ;              // Delete 'old_data', and puts new data type as the return type of function
    virtual unsigned            ReplaceChildAnsiPortDeclInternal(VeriAnsiPortDecl *old_port, VeriAnsiPortDecl *new_port, unsigned delete_old_node) ;  // Delete 'old_port', and puts new ansi port declaration in its place
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ;        // Delete 'old_item', and puts new local variable declaration in its place
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ;                // Delete 'old_stmt', and puts new function statement in its place.

    // Internal routines for Static Elaboration:
    virtual VeriBaseValue *     StaticElaborateFunction(Array * arg_list, ValueTable *n, unsigned expect_nonconst) ;
    unsigned                    InitializePorts(const Array *arg_list, ValueTable *prev_tab, ValueTable *cur_tab, unsigned expect_nonconst) const ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;

private :
    void                        Init() ; // Routine to initialize the body of this class

protected:
    explicit VeriFunctionDecl(VeriFunctionDecl *decl) ; // Internally used in VeriDPIFunctionDecl class ctor (absorbes/deletes 'decl')

protected:
    VeriIdDef       *_func ;                // function identifier.
    VeriName        *_name ;                // Name of the function
//    VeriRange     *_range ;               // return range
//    unsigned       _type:10 ;             // return type VERI_INTEGER VERI_REAL VERI_TIME etc
//    unsigned       _signed_type:10 ;      // 0 for Verilog 95. VERI_SIGNED/UNSIGNED for Verilog 2000
    unsigned         _automatic_type:10 ;   // 0 for Verilog 95.  VERI_AUTOMATIC for Verilog 2000. Should be a 'qualifier' flag.
    unsigned         _can_be_const_func:1 ; // Flag to indicate whether the function can potentially be a constant function.
    unsigned         _is_const_called:1 ;   // Flag to indicate that this function declaration is called as constant function
    unsigned         _verilog_method:1 ;    // Flag to indicate that this function declaration is a VCS specific extern function - VIPER #5248 (extern)
    VeriDataType    *_data_type ;           // the function return data type (range,signing, and type).
    Array           *_ansi_io_list ;        // Array of VeriAnsiPortDecl*. Function arguments declared.
    Array           *_decls ;               // Array of VeriModuleItem*. The declarations in the function.
//    VeriStatement *_stmt ;                // The function (body) statement.
// Since 8/2004, VeriFunctionDecl has an Array of VeriStatements (GetStatements()) rather than a single one
    Array           *_stmts ;               // Array of VeriStatement*. The function (body) statements.
    VeriScope       *_scope ;               // The function-scope (symbol table).
// Created at analysis time :
    Array           *_ports ;               // Array of VeriIdDef*. List of function arguments in ORDER. Created in constructor from 'decls' and 'ansi_io_list'..
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriFunctionDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriTaskDecl : public VeriModuleItem
{
public:
// Since 8/2004, VeriTaskDecl has an Array of VeriStatements (GetStatements()) rather than a single one
//    VeriTaskDecl(unsigned automatic_type, VeriIdDef *task, Array *ansi_io_list, Array *decls, VeriStatement *stmt, VeriScope *scope) ;
    VeriTaskDecl(unsigned automatic_type, VeriIdDef *task, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) ;
    VeriTaskDecl(unsigned automatic_type, VeriName *task, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERITASKDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ; // can happen for task/function import/export decls
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriIdDef *         GetId() const                   { return _task ; } // VIPER 2607. Get the task id.
    VeriIdDef *                 GetTaskId() const               { return _task ; }
    virtual VeriName *          GetSubprogramName() const       { return _name ; } // Get full name of subprogram. Name will be like class_name::subpro for external subprogram def
    virtual unsigned            IsTaskDecl() const              { return 1 ; }  // Tests if this module item is a VeriTaskDecl
    virtual unsigned            GetAutomaticType() const        { return _automatic_type ; }
    virtual unsigned            HasDelayOrEventControl() const  { return _contains_delay_or_event_control ; }
    Array *                     GetAnsiIOList() const           { return _ansi_io_list ; }
    Array *                     GetDeclarations() const         { return _decls ; }
//    VeriStatement *           GetStmt() const                 { return _stmt ; }
    Array *                     GetStatements() const           { return _stmts ; }
    virtual VeriScope *         GetScope() const                { return _scope ; }
    virtual Array *             GetPorts() const                { return _ports ; }

    // VIPER #3369
    virtual void                ClearId()                      { _task = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy task declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTaskDecl(const VeriTaskDecl &task_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTaskDecl(SaveRestore &save_restore) ;

    virtual ~VeriTaskDecl() ;

private:
    // Prevent compiler from defining the following
    VeriTaskDecl() ;                                // Purposely leave unimplemented
    VeriTaskDecl(const VeriTaskDecl &) ;            // Purposely leave unimplemented
    VeriTaskDecl& operator=(const VeriTaskDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    void                        SetAutomaticType(unsigned automatic_type) { _automatic_type = automatic_type ; }

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildAnsiPortDeclInternal(VeriAnsiPortDecl *old_port, VeriAnsiPortDecl *new_port, unsigned delete_old_node) ; // Delete 'old_port', and puts new ansi port declaration in its place
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from local variable declartions, and puts new declaration in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' from task statements, and puts new statement in its place

    // Internal routines for Static Elaboration:
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;

private :
    void                        Init() ;

protected:
    explicit VeriTaskDecl(VeriTaskDecl *decl) ; // Internally used in VeriDPITaskDecl class ctor (absorbes/deletes 'decl')

protected:
    VeriIdDef       *_task ;                // Task identifier
    VeriName        *_name ;
    unsigned         _automatic_type:10 ;   // 0 for Verilog 95.  VERI_AUTOMATIC for Verilog 2000
    unsigned         _contains_delay_or_event_control:1 ; //flag to store if the preceding stmt in a seq block is delay or event control Viper 3956, 4420
    Array           *_ansi_io_list ;        // Array of VeriAnsiPortDecl*. Task arguments declared.
    Array           *_decls ;               // Array of VeriModuleItem*. The declarations in the function.
//    VeriStatement   *_stmt ;              // The task (body) statement.
// Since 8/2004, VeriTaskDecl has an Array of VeriStatements (GetStatements()) rather than a single one
    Array           *_stmts ;               // Array of VeriStatement*. The task (body) statements.
    VeriScope       *_scope ;               // The function-scope (symbol table).
// Created at analysis time :
    Array           *_ports ;               // Array of VeriIdDef*. List of function arguments in ORDER. Created in constructor from 'decls' and 'ansi_io_list'..
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTaskDecl

/* -------------------------------------------------------------- */

/* ******************* More VeriModule Items *********************** */

class VFC_DLL_PORT VeriDefParam : public VeriModuleItem
{
public:
    explicit VeriDefParam(Array *param_assigns) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIDEFPARAM ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    // virtual Array *GetIds() const { return _ids ; } // No longer valid : VeriDefParam now contains Array of VeriDefParamAssign's. No longer any ids defined.
    virtual Array *             GetDefParamAssigns() const      { return _defparam_assigns ; } // Array of VeriDefParamAssign's.

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy defparam construct

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDefParam(const VeriDefParam &def_param, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDefParam(SaveRestore &save_restore) ;

    virtual ~VeriDefParam() ;

private:
    // Prevent compiler from defining the following
    VeriDefParam() ;                                // Purposely leave unimplemented
    VeriDefParam(const VeriDefParam &) ;            // Purposely leave unimplemented
    VeriDefParam& operator=(const VeriDefParam &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDefParamAssignInternal(VeriDefParamAssign *old_assign, VeriDefParamAssign *new_assign, unsigned delete_old_node) ; // Delete 'old-assign' specific defparam assignment from assign list, and puts new assignment in its place

    // Internal routines for Static Elaboration
    void                        DeleteIfProcessed(VeriPseudoTreeNode *mod, const Map *mod_map_tab, const Map *top_nodes) ; // Delete processed defparam
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual void                ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container) ; // Convert comma separated module items to multiple single items
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ; // Copy module item and replace genvar with proper value

protected:
// 11/2004 : _ids replaced by _defparam_assigns. DefParam's are now identifier references, not definitions.
//    Array     *_ids ;              // Array of VeriIdDef*. The declared identifiers. Actually these should not be 'declared', but 'referenced'. LRM is confusing here.
    Array       *_defparam_assigns ; // Array of VeriDefParamAssign*. The assignments to an (external) parameter, from a local expression.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDefParam

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriContinuousAssign : public VeriModuleItem
{
public:
    VeriContinuousAssign(VeriStrength *strength, Array *delay, Array *net_assigns) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERICONTINUOUSASSIGN ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriStrength *      GetStrength() const             { return _strength ; }
    Array *                     GetDelay() const                { return _delay ; }
    Array *                     GetNetAssigns() const           { return _net_assigns ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy continuous assignment

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriContinuousAssign(const VeriContinuousAssign &const_assign, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriContinuousAssign(SaveRestore &save_restore) ;

    virtual ~VeriContinuousAssign() ;

private:
    // Prevent compiler from defining the following
    VeriContinuousAssign() ;                                        // Purposely leave unimplemented
    VeriContinuousAssign(const VeriContinuousAssign &) ;            // Purposely leave unimplemented
    VeriContinuousAssign& operator=(const VeriContinuousAssign &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node) ; // Delete 'old_stre' specific strength, and puts new strength in its place.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new delay expression in its place
    virtual unsigned            ReplaceChildNetRegAssignInternal(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node) ; // Delete 'old_assign' specific net assignment, and puts new assignment in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /*top_mod_nodes*/, Set * /*defparams*/) ;

protected:
    VeriStrength    *_strength ;
    Array           *_delay ;       // Array of VeriExpression*. The delay : #(<delay1>,<delay2>,...)
    Array           *_net_assigns ; // Array of VeriNetRegAssign*.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriContinuousAssign

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGateInstantiation : public VeriModuleItem
{
public:
    VeriGateInstantiation(unsigned inst_type, VeriStrength *strength, Array *delay, Array *insts) ; // multiple instances. Normal constructor
    VeriGateInstantiation(unsigned inst_type, VeriStrength *strength, Array *delay, const VeriInstId *single_inst, Array *additional_insts) ; // single instance, with possible additional insts. Constructor called from bison.

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIGATEINSTANTIATION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual unsigned            GetInstType() const             { return _inst_type ; }
    virtual VeriStrength *      GetStrength() const             { return _strength ; }
    Array *                     GetDelay() const                { return _delay ; }
    Array *                     GetInstances() const            { return _insts ; }
    virtual Array *             GetIds() const                  { return _insts ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy gate instantiation

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGateInstantiation(const VeriGateInstantiation &gate_inst, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGateInstantiation(SaveRestore &save_restore) ;

    virtual ~VeriGateInstantiation() ;

private:
    // Prevent compiler from defining the following
    VeriGateInstantiation() ;                                         // Purposely leave unimplemented
    VeriGateInstantiation(const VeriGateInstantiation &) ;            // Purposely leave unimplemented
    VeriGateInstantiation& operator=(const VeriGateInstantiation &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node) ; // Delete 'old_stre' specific strength, and puts new strength in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new delay expression in its place
    virtual unsigned            ReplaceChildInstIdInternal(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node) ; // Delete 'old_inst' from instance list, and puts new instance in its place

    // Internal routines for Static Elaboration
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set *defparams) ;
    unsigned                    ElaborateArrayInst(VeriModuleItem *mod, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab) ;
    void                        BreakInstantiationAndPopulateArray(Array *list) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual void                ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container) ; // Convert comma separated module items to multiple single items

protected:
    unsigned         _inst_type ;   // The gate-type. As VERI token. VERI_AND, VERI_PULLUP etc.
    VeriStrength    *_strength ;    // The gate-strength
    Array           *_delay ;       // Array of VeriExpression*. The delay : #(<delay1>,<delay2>,...)
    Array           *_insts ;       // Array of VeriInstId*. The instantiation(s) itself.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGateInstantiation

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriModuleInstantiation : public VeriModuleItem
{
public:
    VeriModuleInstantiation(char *module_name, VeriStrength *strength, Array *param_values, Array *insts) ; // multiple instances. Normal constructor
    VeriModuleInstantiation(char *module_name, VeriStrength *strength, Array *param_values, const VeriInstId *single_inst, Array *additional_insts) ; // single instance, with possible additional insts. Constructor called from bison.
    VeriModuleInstantiation(VeriName *module_name, VeriStrength *strength, Array *param_values, const VeriInstId *single_inst, Array *additional_insts) ; // single instance, with possible additional insts. Constructor called from bison.

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIMODULEINSTANTIATION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Cast to VeriModuleItemInstance
    virtual VeriModuleInstantiation* CastModuleInstance() { return (static_cast<class VeriModuleInstantiation*>(this)) ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsInstantiation() const         { return 1 ; } // Test if this is a (module) instantiation.

    // Find the module that is being instantiated here.
    virtual VeriModule *        GetInstantiatedModule() const ; // Find the instantiated VeriModule, and return it.

    // Accessor methods
    VeriName *                  GetModuleNameRef() const        { return _module_name ; }
    virtual VeriStrength *      GetStrength() const             { return _strength ; }
    Array *                     GetParamValues() const          { return _param_values ; }
    Array *                     GetInstances() const            { return (Array *)&_insts ; }
    const char *                GetModuleName() const ;  // backward compatible accessor
    // Virtual accessors :
    virtual Array *             GetIds() const                  { return (Array *)&_insts ; }  // ModuleInstantiation now contains an array of VeriInstId (VeriIdDef's).

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy module instantiation as module item
    VeriModuleInstantiation *   Copy(VeriMapForCopy &id_map_table) const ; // Copy module instantiation as instantiation

    // Parse tree manipulation routine : VIPER #6129
    unsigned                    AddParamRef(const char *formal_param_name, VeriExpression *actual, VeriScope *module_scope) ;

    VhdlPrimaryUnit *           GetBoundedUnit(char **arch_name=0) const ;

    VeriModule *                GetBoundedModule() const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriModuleInstantiation(const VeriModuleInstantiation &mod_inst, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriModuleInstantiation(SaveRestore &save_restore) ;

    virtual ~VeriModuleInstantiation() ;

private:
    // Prevent compiler from defining the following
    VeriModuleInstantiation() ;                                           // Purposely leave unimplemented
    VeriModuleInstantiation(const VeriModuleInstantiation &) ;            // Purposely leave unimplemented
    VeriModuleInstantiation& operator=(const VeriModuleInstantiation &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Verific routines for user-library (-v/-y/uselib) setting
    VeriLibrary *               GetLibrary() const              { return _library ; } // The library in which the module should be found (uselib/-v/-y). If 0, we look in the work library.
    virtual void                SetLibrary(VeriLibrary *lib) ;
    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Insert instantiated module name in the argument specific set

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node) ; // Delete 'old_stre' from instantiation strength, and puts new strength in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from param override list, and puts new param override in its place
    virtual unsigned            ReplaceChildInstIdInternal(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node) ; // Delete 'old_inst' from instance list, and puts new instance in its place

    // Internal routines for Static Elaboration :
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;
    void                        BreakInstantiationAndPopulateArray(Array *arr) ;
    virtual void                ProcessModuleItem(VeriScope *container_scope, unsigned &is_prameterized_construct) ;
    virtual void                ElaboratePortConnections(VeriScope *scope_of_instantiation) ;
    void                        SetModuleName(const char *name) ; // Set (change) the name of the instantiated module :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual void                ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container) ; // Convert comma separated module items to multiple single items
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ; // Copy module item and replace genvar with proper value

    VhdlComponentInstantiationStatement*   ConvertInstance(VhdlInstantiatedUnit *instantiated_unit, const VeriInstId *module_instId) const ; //Convert to Vhdl Instance

private:
    unsigned                    VarUsagePortConnect(VeriVarUsageInfo *info, unsigned i, VeriExpression *expr, const VeriModule *module, void *void_unit, unsigned array_inst_dims, Set &connected_formals, VeriExpression **found_dot_star, const VeriIdDef *inst_id) const ; // VIPER #6508

protected:
    VeriName        *_module_name ;     // unresolved instantiated module name (resolving to VeriIdDef* happens during elaboration).
    VeriStrength    *_strength ;        // The module strength (UDP instances only)
    Array           *_param_values ;    // Array of VeriExpression*. The parameter assoociations. #(<paramexpr1>,<paramexpr2>, ..). In Verilog 2000 : could contain PortConnects too. : #(.paramname(<paramexpr>), ...)
    Array            _insts ;           // Array of VeriInstId*. The instantiation(s) itself.
// Verific internal use:
    VeriLibrary     *_library ;         // The library in which the module should be found (uselib/-v/-y). If 0, we look in the work library.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriModuleInstantiation

/* -------------------------------------------------------------- */

/* **************** Specify Section *************************** */
// Actually, this is all part of a specparam block

class VFC_DLL_PORT VeriSpecifyBlock : public VeriModuleItem
{
public:
    VeriSpecifyBlock(Array *specify_items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISPECIFYBLOCK ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    Array *                     GetSpecifyItems() const     { return _specify_items ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy specify block

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSpecifyBlock(const VeriSpecifyBlock &spec_block, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSpecifyBlock(SaveRestore &save_restore) ;

    virtual ~VeriSpecifyBlock() ;

private:
    // Prevent compiler from defining the following
    VeriSpecifyBlock() ;                                    // Purposely leave unimplemented
    VeriSpecifyBlock(const VeriSpecifyBlock &) ;            // Purposely leave unimplemented
    VeriSpecifyBlock& operator=(const VeriSpecifyBlock &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from item list of specific block, and puts new specific block item in its place

    // Internal static elaboration routine:
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array     *_specify_items ;  // Array of VeriModuleItem*. The specify items.
    VeriScope *_scope ;          // The specify-block-scope. A symbol table.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSpecifyBlock

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriPathDecl : public VeriModuleItem
{
public:
    VeriPathDecl(unsigned ifnone, VeriExpression *cond, VeriPath *path, Array *delays) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPATHDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    unsigned                    IsIfNone() const            { return _ifnone ; }
    VeriExpression *            GetCondition() const        { return _cond ; }
    VeriPath *                  GetPath() const             { return _path ; }
    Array *                     GetDelay() const            { return _delay ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy path declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPathDecl(const VeriPathDecl &path_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriPathDecl(SaveRestore &save_restore) ;

    virtual ~VeriPathDecl() ;

private:
    // Prevent compiler from defining the following
    VeriPathDecl() ;                                // Purposely leave unimplemented
    VeriPathDecl(const VeriPathDecl &) ;            // Purposely leave unimplemented
    VeriPathDecl& operator=(const VeriPathDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from path condition and delay values, and puts new expression in its place.
    virtual unsigned            ReplaceChildPathInternal(VeriPath *old_path, VeriPath *new_path, unsigned delete_old_node) ; // Delete 'old_path', and puts new path in its place

    // Internal static elaboration routine:
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected :
    unsigned        _ifnone:1 ; // 1 if IFNONE path. 0 otherwise
    VeriExpression  *_cond ;    // Non-zero if a state-dependent path
    VeriPath        *_path ;    // The path.
    Array           *_delay ;   // Array of VeriExpression*. The path-delay-value : (<delay1>,<delay2>,...)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPathDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriSystemTimingCheck : public VeriModuleItem
{
public:
    VeriSystemTimingCheck(char *task_name, Array *args) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISYSTEMTIMINGCHECK ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    char *                      GetTaskName() const         { return _task_name ; }
    Array *                     GetArgs() const             { return _args ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy system timimg check

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSystemTimingCheck(const VeriSystemTimingCheck &sys_time_check, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSystemTimingCheck(SaveRestore &save_restore) ;

    virtual ~VeriSystemTimingCheck() ;

private:
    // Prevent compiler from defining the following
    VeriSystemTimingCheck() ;                                         // Purposely leave unimplemented
    VeriSystemTimingCheck(const VeriSystemTimingCheck &) ;            // Purposely leave unimplemented
    VeriSystemTimingCheck& operator=(const VeriSystemTimingCheck &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new timimg argument in its place.

    // Internal static elaboration routine:
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    char    *_task_name ;
    Array   *_args ; // Array of VeriExpression*. Should not contain VeriPortConnect. The system timing arguments.
// Set during construction :
    unsigned     _function_type ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSystemTimingCheck

/* -------------------------------------------------------------- */

/* ****************** More VeriModule Items ********************* */

class VFC_DLL_PORT VeriInitialConstruct : public VeriModuleItem
{
public:
    explicit VeriInitialConstruct(VeriStatement *stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIINITIALCONSTRUCT ; } // Defined in VeriClassIds.h
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriStatement *     GetStmt() const             { return _stmt ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy initial construct

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriInitialConstruct(const VeriInitialConstruct &initial_con, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriInitialConstruct(SaveRestore &save_restore) ;

    virtual ~VeriInitialConstruct() ;

private:
    // Prevent compiler from defining the following
    VeriInitialConstruct() ;                                        // Purposely leave unimplemented
    VeriInitialConstruct(const VeriInitialConstruct &) ;            // Purposely leave unimplemented
    VeriInitialConstruct& operator=(const VeriInitialConstruct &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt', and puts new statement in its place.

    virtual unsigned            IsProceduralArea() const  { return 1 ; } // VIPER #4241
    // Internal routines for Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;

protected:
    VeriStatement *_stmt ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriInitialConstruct

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriAlwaysConstruct : public VeriModuleItem
{
public:
    explicit VeriAlwaysConstruct(VeriStatement *stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIALWAYSCONSTRUCT ; } // Defined in VeriClassIds.h
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriStatement *     GetStmt() const             { return _stmt ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy always construct

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriAlwaysConstruct(const VeriAlwaysConstruct &always_con, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriAlwaysConstruct(SaveRestore &save_restore) ;

    virtual ~VeriAlwaysConstruct() ;

private:
    // Prevent compiler from defining the following
    VeriAlwaysConstruct() ;                                       // Purposely leave unimplemented
    VeriAlwaysConstruct(const VeriAlwaysConstruct &) ;            // Purposely leave unimplemented
    VeriAlwaysConstruct& operator=(const VeriAlwaysConstruct &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt', and puts new statement in its place

    // VIPER #4241 : APIs to check procedural construct or special always constructs
    virtual unsigned            IsProceduralArea() const  { return 1 ; }
    virtual unsigned            GetSpecialAlwaysToken() const ;

    // Internal routines for Static Elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;

protected:
    VeriStatement *_stmt ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAlwaysConstruct

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGenerateConstruct : public VeriModuleItem
{ // Verilog 2000
public:
    explicit VeriGenerateConstruct(Array *items) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIGENERATECONSTRUCT ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Test
    virtual unsigned            IsGenerateConstruct() const { return 1 ; } // test if this is a generate endgenerate construct

    // Accessor methods
    virtual Array *             GetItems() const            { return _items ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy generate construct

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGenerateConstruct(const VeriGenerateConstruct &gen_con, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGenerateConstruct(SaveRestore &save_restore) ;

    virtual ~VeriGenerateConstruct() ;

private:
    // Prevent compiler from defining the following
    VeriGenerateConstruct() ;                                         // Purposely leave unimplemented
    VeriGenerateConstruct(const VeriGenerateConstruct &) ;            // Purposely leave unimplemented
    VeriGenerateConstruct& operator=(const VeriGenerateConstruct &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;// Process -v/-y/uselib library for this module item (only does something for module instantiations).

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Look into generate items to find module instantiations
    virtual void                AccumulateParameters(Array &parameters, unsigned has_ansi_parameter_list, const VeriIdDef *mod_id)  const ; // VIPER #7603: Simulators allow overwriting parameters declared within generate ... endgenerate from instances

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from generate item list, and puts new generate item in its place.

    // Internal routines for Static Elaboration :
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;
    virtual unsigned            Detach(VeriModuleItem *node) ;
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item

protected:
    Array *_items ; // Array of VeriModuleItem*
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGenerateConstruct

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGenerateConditional : public VeriModuleItem
{ // Verilog 2000
public:
    VeriGenerateConditional(VeriExpression *if_expr, VeriModuleItem *then_item, VeriModuleItem *else_item, VeriScope *then_scope, VeriScope *else_scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIGENERATECONDITIONAL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsGenerateConditional() const           { return 1 ; }

    // Set item:
    void                        SetThenItem(VeriModuleItem *item) ; // #6550: Replace the old generate conditional then item with the newer one:
    void                        SetElseItem(VeriModuleItem *item) ; // #6550: Replace the old generate conditional else item with the newer one:

    // Accessor methods
    VeriExpression *            GetIfExpr() const           { return _if_expr ; }
    VeriModuleItem *            GetThenItem() const         { return _then_item ; }
    VeriModuleItem *            GetElseItem() const         { return _else_item ; }
    VeriScope *                 GetThenScope() const        { return _then_scope ; }
    VeriScope *                 GetElseScope() const        { return _else_scope ; }

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy if-generate as module item
    VeriGenerateConditional *   Copy(VeriMapForCopy &id_map_table) const ; // Copy if-generate as generate conditional

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGenerateConditional(const VeriGenerateConditional &gen_cond, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGenerateConditional(SaveRestore &save_restore) ;

    virtual ~VeriGenerateConditional() ;

private:
    // Prevent compiler from defining the following
    VeriGenerateConditional() ;                                           // Purposely leave unimplemented
    VeriGenerateConditional(const VeriGenerateConditional &) ;            // Purposely leave unimplemented
    VeriGenerateConditional& operator=(const VeriGenerateConditional &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Look into the then and else statements for module instantiations
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    // VIPER #5727 : Internal routine for unnamed generate block handling
    virtual void ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_no, Set *block_names, Set *ignore_scopes) ;
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from then/else item, and puts new item in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expression as new condition

    // Internal routines for Static Elaboration :
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ;
    virtual void                ProcessModuleItem(VeriScope *container_scope, unsigned &is_prameterized_construct) ;

protected:
    VeriExpression  *_if_expr ;
    VeriModuleItem  *_then_item ;
    VeriModuleItem  *_else_item ;
    // Introduce local scope for two branches of if-generate, as only
    // one branch will be alive after elaboration
    // Added for bug #1702 and #1923
    VeriScope       *_then_scope ;
    VeriScope       *_else_scope ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGenerateConditional

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGenerateCase : public VeriModuleItem
{ // Verilog 2000
public:
    VeriGenerateCase(unsigned case_style, VeriExpression *cond, Array *case_items) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIGENERATECASE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsGenerateConditional() const           { return 1 ; }

    // Accessor methods
    unsigned                    GetCaseStyle() const        { return _case_style ; }
    VeriExpression *            GetCondition() const        { return _cond ; }
    Array *                     GetCaseItems() const        { return _case_items ; }

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy case generate as module item
    VeriGenerateCase *          Copy(VeriMapForCopy &id_map_table) const ; // Copy case-generate

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGenerateCase(const VeriGenerateCase &gen_case, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGenerateCase(SaveRestore &save_restore) ;

    virtual ~VeriGenerateCase() ;

private:
    // Prevent compiler from defining the following
    VeriGenerateCase() ;                                    // Purposely leave unimplemented
    VeriGenerateCase(const VeriGenerateCase &) ;            // Purposely leave unimplemented
    VeriGenerateCase& operator=(const VeriGenerateCase &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Look into case items for module instantiations
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    // VIPER #5727 : Internal routine for unnamed generate block handling
    virtual void ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_no, Set *block_names, Set *ignore_scopes) ;
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildGenerateCaseItemInternal(VeriGenerateCaseItem *old_item, VeriGenerateCaseItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from case-generate items, and puts new case-item in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new case condition in its place

    // Internal routines for Static Elaboration
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ;
    virtual void                ProcessModuleItem(VeriScope *container_scope, unsigned &is_prameterized_construct) ;

protected:
    unsigned         _case_style ;
    VeriExpression  *_cond ;
    Array           *_case_items ; // array of VeriGenerateCaseItem
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGenerateCase

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGenerateFor : public VeriModuleItem
{ // Verilog 2000
public:
    VeriGenerateFor(VeriModuleItem *initial, VeriExpression *cond, VeriModuleItem *rep, VeriIdDef *block_id, Array *items, VeriScope *scope, VeriScope *loop_scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIGENERATEFOR ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsGenerateFor() const       { return 1 ; } // test if this is a generate-for construct.

    // Accessor methods
    VeriModuleItem *            GetInitial() const          { return _initial ; } // Will be VeriGenVarAssign only for pure Verilog 2000 (not for SV).
    VeriExpression *            GetCondition() const        { return _cond ; }
    VeriModuleItem *            GetRepetition() const       { return _rep ; } // Will be VeriGenVarAssign only for pure Verilog 2000 (not for SV).
    VeriIdDef *                 GetBlockId() const          { return _block_id ; }
    virtual VeriIdDef *         GetId() const               { return _block_id ; }   // VIPER #2607, #4211 & #4212: Get the block label.
    virtual Array *             GetItems() const            { return _items ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }
    VeriScope *                 GetLoopScope() const        { return _loop_scope ; } // Get the scope where in-line declaration of loop control variable occurs
    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy for-generate

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGenerateFor(const VeriGenerateFor &gen_for, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGenerateFor(SaveRestore &save_restore) ;

    virtual ~VeriGenerateFor() ;

private:
    // Prevent compiler from defining the following
    VeriGenerateFor() ;                                   // Purposely leave unimplemented
    VeriGenerateFor(const VeriGenerateFor &) ;            // Purposely leave unimplemented
    VeriGenerateFor& operator=(const VeriGenerateFor &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual VeriIdDef *         FindSingleGenVarId() const ; // VIPER #6688

    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Look into for-generate items for module instantiations
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    // VIPER #5727 : Internal routine for unnamed generate block handling
    virtual void ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_no, Set *block_names, Set *ignore_scopes) ;
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from initial assignment and for-generate items, and puts new item in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new final condition in its place

    // Internal routines for Static Elaboration
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ;
    virtual void                ProcessModuleItem(VeriScope *container_scope, unsigned &is_prameterized_construct) ;

protected:
    // From the for-loop statements :
    VeriModuleItem      *_initial ;
    VeriExpression      *_cond ;
    VeriModuleItem      *_rep ;
    // From the generate block (is expanded) :
    VeriIdDef           *_block_id ;    // The generate block identifier. (Not Optional)
    Array               *_items ;       // Array of VeriModuleItem*. The generate items.
    VeriScope           *_scope ;       // The local scope inside the generate block.
    VeriScope           *_loop_scope ;  // The scope where in-line declaration of loop control variable occurs
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGenerateFor

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGenerateBlock : public VeriModuleItem
{ // Verilog 2000. Do we need a scope and decl area ?
public:
    VeriGenerateBlock(VeriIdDef *block_id, Array *items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIGENERATEBLOCK ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsGenerateBlock() const     { return 1 ; } // test if this is a generate-block construct.

    // Accessor methods
    VeriIdDef *                 GetBlockId() const          { return _block_id ; }
    virtual VeriIdDef *         GetId() const               { return _block_id ; }   // VIPER #2607, #4211 & #4212: Get the block label.
    virtual Array *             GetItems() const            { return _items ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy generate block

    virtual unsigned            IsElaborated() const { return _is_elaborated ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGenerateBlock(const VeriGenerateBlock &gen_block, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGenerateBlock(SaveRestore &save_restore) ;

    virtual ~VeriGenerateBlock() ;

private:
    // Prevent compiler from defining the following
    VeriGenerateBlock() ;                                     // Purposely leave unimplemented
    VeriGenerateBlock(const VeriGenerateBlock &) ;            // Purposely leave unimplemented
    VeriGenerateBlock& operator=(const VeriGenerateBlock &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Look into the block items for module instantiations
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    // VIPER #5727 : Internal routine for unnamed generate block handling
    virtual void ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_no, Set *block_names, Set *ignore_scopes) ;
    void         ProcessDesignSpecifiedBlockId(VeriScope *enclosing_scope, int branch_num, const Set *block_names) ;
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    virtual void                DeleteOnlyBlock() ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from block items, and puts new item in its place
    virtual unsigned            ReplaceChildBy(VeriModuleItem *old_item, const Array *new_items) ; // Virtual routine to replace module/interface instantiation array  by bunch of single instantiations

    // Internal routines for Static Elaboration
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ; // VIPER #5612
    virtual void                StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope) ;
    virtual void                ProcessModuleItem(VeriScope *container_scope, unsigned &is_prameterized_construct) ;
    virtual void                SetIsElaborated()    { _is_elaborated = 1 ; }
    virtual void                SetStaticElaborated(){ _is_elaborated = 1 ; }
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // VIPER #6326

protected:
    VeriIdDef *_block_id ;  // The generate block identifier. Optional.
    Array     *_items ;     // Array of VeriModuleItem*. The generate items.
    VeriScope *_scope ;     // The local scope inside the generate block.
    unsigned   _is_elaborated:1 ; // Flag to indicate whether this is already elaborated or not
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGenerateBlock

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriTable : public VeriModuleItem
{
public:
    explicit VeriTable(Array *table_entries) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const                  { return ID_VERITABLE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor method
    Array *                     GetTableEntries() const             { return _table_entries ; }
    // back-pointer to the module in which the table appears :
    VeriModule *                GetTheModule() const                { return _the_module ; }
    // VIPER #4532: Add API to get the table style:
    unsigned                    IsCombinational() const             { return _is_comb ; }
    unsigned                    IsSequential() const                { return _is_seq ; }
    // Made this a virtual routine to set 'this' backpointer while binary restore
    virtual void                SetTheModule(VeriModule *module)    { _the_module = module ; }
    virtual unsigned            IsTable() const                     { return 1 ; }
    virtual VeriTable *         GetPrimitiveTable() const           { return const_cast<VeriTable *>(this) ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy table

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTable(const VeriTable &table, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTable(SaveRestore &save_restore) ;

    virtual ~VeriTable() ;

private:
    // Prevent compiler from defining the following
    VeriTable() ;                             // Purposely leave unimplemented
    VeriTable(const VeriTable &) ;            // Purposely leave unimplemented
    VeriTable& operator=(const VeriTable &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Internal static elaboration routine
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item

public:
    Array       *_table_entries ; // Array of char*    (rows in table : characters 01?rf etc).
    VeriModule  *_the_module ;    // Back-pointer to the module in which this table appears. Set at construction time.
    unsigned     _is_comb:1 ;     // Bit flag to indicate that the inferred table style is combinational
    unsigned     _is_seq:1 ;      // Bit flag to indicate that the inferred table style is sequential
    //unsigned     _un_used:30 ;    // Unused bits available
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTable

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriPulseControl : public VeriModuleItem
{
public:
    VeriPulseControl(unsigned pulse_control_style, Array *path_list) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPULSECONTROL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    unsigned                    GetPulseControlStyle() const       { return _pulse_control_style ; }
    Array *                     GetPathList() const         { return _path_list ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy path declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPulseControl(const VeriPulseControl &pulse_ctrl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriPulseControl(SaveRestore &save_restore) ;

    virtual ~VeriPulseControl() ;

private:
    // Prevent compiler from defining the following
    VeriPulseControl() ;                                    // Purposely leave unimplemented
    VeriPulseControl(const VeriPulseControl &) ;            // Purposely leave unimplemented
    VeriPulseControl& operator=(const VeriPulseControl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its plac

    // Internal static elaboration routine:
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) {} // Cannot be generate item
    virtual void                StaticReplaceConstantExpr(unsigned /*is_const_index_context*/) {} ; // Replace range bounds, default value, bit/part/slice expressions

protected :
    unsigned        _pulse_control_style ; // VERI_PULSESTYLE_ONEVENT/VERI_PULSESTYLE_ONDETECT/VERI_SHOWCANCELLED/VERI_NOSHOWCANCELLED
    Array           *_path_list ;   // Array of VeriExpression * of output paths
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPulseControl

/* -------------------------------------------------------------- */

/*
   VeriClass :
   Implements class declaration : "class <id> [#(parameters..)] [extends <class_name>] ; <class_items> endclass "
*/
class VFC_DLL_PORT VeriClass  : public VeriModuleItem
{
public:
    VeriClass(VeriIdDef *id, Array *parameter_connects, VeriName *base_class_name, Array *module_items, VeriScope *scope, Array *interface_classes =0, unsigned interface_class=0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERICLASS ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const                   { return _id ; } // The id (simple name) of this class.
    Array *                     GetParameterConnects() const    { return _parameter_connects ; } // declaration of parameters of this class.. Array of VeriModuleItem
    virtual VeriName *          GetBaseClassName() const        { return _base_class_name ; } // name of the base class of this class
    virtual Array *             GetInterfaceClasses() const     { return _interface_classes ; }
    virtual Array *             GetItems() const                { return _items ; } // The class items (declarations etc). Array of VeriModuleItem
    virtual VeriScope *         GetScope() const                { return _scope ; } // The scope (declared id's) in this class.
    // Ordered list of locally declared IdDef's :
    virtual Array *             GetParameters() const           { return _parameters ; } // Parameter id's in order Array of VeriIdDef.
    virtual unsigned            IsClass() const                 { return 1 ; }  // Affirms that this module item is a VeriClass

    // VIPER #3369
    virtual void                ClearId()                       { _id = 0 ; }  // Clear the id
    virtual VeriIdDef  *        GetNewFunctionId()              { return _new_id ; }  // id for the new function
    virtual void                SetNewFunctionId(VeriIdDef *new_id) { _new_id = new_id ; }  // id for the new function
    virtual VeriIdDef  *        GetBaseClassId(unsigned &is_base_typeparam) ;
    virtual VeriModuleItem *    GetDataDecl(VeriIdDef *vid, unsigned is_constraint) const ;

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy class as module item
    virtual VeriModuleItem *    CopyWithName(const char *name, VeriMapForCopy &id_map_table, unsigned add_copied_obj) ; // Copy module item with the first argument specific name

    virtual unsigned            IsClassInstantiated() const    { return _is_instantiated ; }
    virtual const char *        GetOriginalClassName() const   { return _orig_class_name ; }
    virtual VeriIdDef *         GetParentClassId(unsigned &is_base_typeparam) ;
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    virtual VeriScope *         GetOwningScope()const          { return _owning_scope ; } // Return the container scope
#endif
    virtual unsigned            VarUsage(VeriVarUsageInfo* /*info*/, unsigned action) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriClass(const VeriClass &veri_class, VeriMapForCopy &id_map_table) ; //  Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriClass(SaveRestore &save_restore) ;

    virtual ~VeriClass() ;

private:
    // Prevent compiler from defining the following
    VeriClass() ;                             // Purposely leave unimplemented
    VeriClass(const VeriClass &) ;            // Purposely leave unimplemented
    VeriClass& operator=(const VeriClass &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is base class name, and puts new base class name in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from parameter port list/class items, and puts new item in its place.

    virtual void                SetIsProcessing()              { _is_processing = 1 ; }
    virtual unsigned            IsProcessing()                 { return _is_processing ; }
    virtual void                ResetIsProcessing()            { _is_processing = 0 ; }
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriIdDef *         StaticElaborateClass(Array *param_list, VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new) ;
    virtual void                SetClassInstantiated()         { _is_instantiated = 1 ; }
    virtual unsigned            IsContainCopiedVersion() const { return _contain_copied_version ; }
    virtual void                SetContainCopiedVersion()      { _contain_copied_version = 1 ; }
    virtual unsigned            IsResolved() const             { return _is_resolved ; }
    virtual VeriModuleItem *    CopyWithDefaultParameter() ; // Copy class with default parameter setting or by appending _default

    virtual char *              CreateNameWithDefaultparam(Array *param_list) const ;

    virtual void                CollectRandRandCVars(Map *rand_randc) ;
    virtual unsigned            HasStaticMembers() { return _has_static_members ; }
    virtual void                PopulateToBeImplementedMethods(Map *methods, unsigned ignore_interface_classes) const ;
    virtual void                ValidateImplementedMethod(VeriModuleItem *subprog_decl, Map *methods, unsigned virtual_class, unsigned ignore_pure_virtual) ;

protected:
    VeriIdDef   *_id ;                  // The identifier of this class (the simple name).
    VeriIdDef   *_new_id ;              // The identifier of the new function of this class
    Array       *_parameter_connects ;  // Array of VeriParamDecl (a VeriModuleItem). The parameters.
    VeriName    *_base_class_name ;     // Name of the base class of this class (class derivation).
    Array       *_interface_classes ;   // Array of VeriName*, List of implements classes for normal class and list of extends classes for interface class
    Array       *_items ;               // Array of VeriModuleItem. The contents of this construct.
    VeriScope   *_scope ;               // The scope (with locally declared identifiers) of this construct
// Created at analysis time :
    Array       *_parameters ;          // Array of VeriIdDef*. List of function arguments in ORDER. Created in constructor from 'parameter_connects'
    unsigned    _has_static_members:1 ;
    unsigned    _is_processing:1 ;       // Flag to mark processing is started already (for recursion)
    unsigned    _is_instantiated:1 ;     // Flag to indicate instantiated class : set in elaboration
    unsigned    _contain_copied_version:1 ; // It will be 1, if this class is copied to get a new other one
    unsigned    _is_resolved:1 ; // Flag to test if this class body is resolved or not
    char        *_orig_class_name ;      // Name of design file specific class name from which this class is copied
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    VeriScope   *_owning_scope ;        // Scope where class is declared
#endif
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriClass

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriPropertyDecl  : public VeriModuleItem
{
public:
    VeriPropertyDecl(VeriIdDef *id, Array *formals, Array *decls, VeriExpression *spec, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPROPERTYDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    // Variable usage analysis (analysis after all modules are parsed)
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // tests
    virtual unsigned            IsPropertyDecl() const      { return 1 ; }          // Affrims that this module item is a VeriPropertyDecl

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const               { return _id ; }        // The 'name'
    Array *                     GetAnsiIOList() const       { return _ansi_io_list ; } // list of formal ansi-port decls (id's (with direction,datatype etc)).
//    Array *                     GetFormals() const          { return _formals ; }   // The formals (Array of VeriIdDefs)
    Array *                     GetDeclarations() const     { return _decls ; }     // The declarations (Array of VeriModuleItem)
    virtual VeriExpression *    GetSpec() const             { return _spec ; }      // The (property) spec (implementation of this named property)
    virtual VeriScope *         GetScope() const            { return _scope ; }     // Local scope (containing formals and declared Ids).
    virtual Array *             GetPorts() const            { return _ports ; } // Array of VeriIdDef's: the property ports, in order of declaration. Created in constructor.
    virtual VeriExpression *    GetActual(VeriIdDef *formal) ;

    // VIPER #3369
    virtual void                ClearId()                      { _id = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy property declaration as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPropertyDecl(const VeriPropertyDecl &property_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriPropertyDecl(SaveRestore &save_restore) ;

    virtual ~VeriPropertyDecl() ;

private:
    // Prevent compiler from defining the following
    VeriPropertyDecl() ;                                    // Purposely leave unimplemented
    VeriPropertyDecl(const VeriPropertyDecl &) ;            // Purposely leave unimplemented
    VeriPropertyDecl& operator=(const VeriPropertyDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new property spec in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from declaration list, and puts new declaration in its place.

    // Static elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef       *_id ;      // The identifier of this construct (the name).
    Array           *_ansi_io_list ; // Array of VeriAnsiPortDecl*. Function arguments declared.
    Array           *_decls ;   // Array of VeriModuleItem. The contents of this construct.
    VeriExpression  *_spec ;    // The property spec. (Implementation of this named property)
    VeriScope       *_scope ;   // The scope (with locally declared identifiers) of this construct
// Created at analysis time
    Array           *_ports ; // Array of VeriIdDef. The formals of this construct.
    unsigned         _is_processing ; // Flag to indicate that this declaration is now processing
    Net             *_feedback_net ; // Used to strore the feedback net in case of recursion
    Map             *_formal_to_actual ; //strores the connected actuals for every formal
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPropertyDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriSequenceDecl  : public VeriModuleItem
{
public:
    VeriSequenceDecl(VeriIdDef *id, Array *ansi_io_list, Array *decls, VeriExpression *spec, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISEQUENCEDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    // Variable usage analysis (analysis after all modules are parsed)
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsSequenceDecl() const      { return 1 ; }          // Affirms that this module item is a VeriSequenceDecl

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const               { return _id ; }        // The 'name'
    Array *                     GetAnsiIOList() const       { return _ansi_io_list ; } // list of formal ansi-port decls (id's (with direction,datatype etc)).
//    Array *                     GetFormals() const          { return _formals ; }   // The formals (Array of VeriIdDefs)
    Array *                     GetDeclarations() const     { return _decls ; }     // The declarations (Array of VeriModuleItem)
    virtual VeriExpression *    GetSpec() const             { return _spec ; }      // The (sequence) spec (implementation of this named sequence)
    virtual VeriScope *         GetScope() const            { return _scope ; }     // Local scope (containing formals and declared Ids).
    virtual Array *             GetPorts() const            { return _ports ; } // Array of VeriIdDef's: the sequence ports, in order of declaration. Created in constructor.

    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy sequence declaration as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSequenceDecl(const VeriSequenceDecl &sequence_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSequenceDecl(SaveRestore &save_restore) ;

    virtual ~VeriSequenceDecl() ;

private:
    // Prevent compiler from defining the following
    VeriSequenceDecl() ;                                    // Purposely leave unimplemented
    VeriSequenceDecl(const VeriSequenceDecl &) ;            // Purposely leave unimplemented
    VeriSequenceDecl& operator=(const VeriSequenceDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new sequence spec in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from declaration list, and puts new declaration in its place

    // Static elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef       *_id ;      // The identifier of this construct (the name).
    Array           *_ansi_io_list ; // Array of VeriAnsiPortDecl*. Function arguments declared.
    Array           *_decls ;   // Array of VeriModuleItem. The contents of this construct.
    VeriExpression  *_spec ;    // The sequence spec. (Implementation of this named sequence)
    VeriScope       *_scope ;   // The scope (with locally declared identifiers) of this construct
// Created at analysis time
    Array           *_ports ; // Array of VeriIdDef. The formals of this construct.
    unsigned         _is_processing ; // Flag to indicate that this declaration is now processing
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSequenceDecl

/* -------------------------------------------------------------- */

/*
  VeriModport is a container of modport declarations. "modport <modport_decl>, <modport_decl>.... ;"
*/
class VFC_DLL_PORT VeriModport  : public VeriModuleItem
{
public:
    explicit VeriModport(Array *modport_decls) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIMODPORT ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual unsigned            VarUsage(VeriVarUsageInfo * info, unsigned action) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    Array *                     GetModportDecls() const         { return _modport_decls ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy modport construct as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriModport(const VeriModport &modport_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriModport(SaveRestore &save_restore) ;

    virtual ~VeriModport() ;

private:
    // Prevent compiler from defining the following
    VeriModport() ;                               // Purposely leave unimplemented
    VeriModport(const VeriModport &) ;            // Purposely leave unimplemented
    VeriModport& operator=(const VeriModport &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // VIPER #4286: Update modports module item back pointer (we do it after restoring a full interface decl):
    virtual void                UpdateModportPorts() ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModportDeclInternal(VeriModportDecl *old_modport_decl, VeriModportDecl *new_modport_decl, unsigned delete_old_node) ; // Delete 'old_modport_decl' from modport declaration list, and puts new modport declaration in its place.

    // Static Elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual void                CheckModportExportImport() const ; // VIPER #7915

protected:
    Array     *_modport_decls ; // Array of VeriModportDecl.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriModport

/* -------------------------------------------------------------- */

/*
  VeriModDecl is declaration of a modport : "<modport_id> ( modport_port_decl_list ) "
*/
class VFC_DLL_PORT VeriModportDecl  : public VeriModuleItem
{
public:
    VeriModportDecl(VeriIdDef *id, Array *modport_port_decls, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIMODPORTDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual unsigned            VarUsage(VeriVarUsageInfo * info, unsigned action) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const               { return _id ; }
    Array *                     GetModportPortDecls() const { return _modport_port_decls ; }
    virtual VeriScope *         GetScope() const            { return _scope ; } // Scope of VeriModportPortId's in this modport decl.
    virtual Array *             GetPorts() const            { return _ports ; } // Array of VeriIdDef's: the modportports, in order of declaration. Created in constructor.

    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id
    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy modport decl as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriModportDecl(const VeriModportDecl &modport_port_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriModportDecl(SaveRestore &save_restore) ;

    virtual ~VeriModportDecl() ;

private:
    // Prevent compiler from defining the following
    VeriModportDecl() ;                                   // Purposely leave unimplemented
    VeriModportDecl(const VeriModportDecl &) ;            // Purposely leave unimplemented
    VeriModportDecl& operator=(const VeriModportDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Verific internal routine:
    virtual unsigned            ContainsInterfaceMethod() const ;

    // VIPER #4286: Update modports module item back pointer (we do it after restoring a full interface decl):
    virtual void                UpdateModportPorts() ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from modport port declaration list, and puts new modport port declaration in its place

    // Internal static elaboration routine:
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual void                CheckModportExportImport() const ; // VIPER #7915

protected:
    VeriIdDef *_id ; // The declared modport.
    Array     *_modport_port_decls ; // Array of VeriModuleItem.. (mostly DataDecl's of ModportPort's (prototype id's with a direction).
    VeriScope *_scope ;
// Additional members :
    Array     *_ports ;      // Array of VeriIdDef*. List of modport_ports ids in ORDER. Created in constructor from 'modport_port_decls'
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriModportDecl

/* -------------------------------------------------------------- */

/*
   VeriClockingDecl implement a (default) named, or (default) unnamed clock declaration.
   The 'default' keyword is set as a VERI_DEFAULT 'qualifier' on this module item.
*/
class VFC_DLL_PORT VeriClockingDecl  : public VeriModuleItem
{
public:
    VeriClockingDecl(VeriIdDef *id, VeriExpression *clocking_event, Array *clocking_items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICLOCKINGDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ; // Print clocking decl without printing semicolon after header

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual unsigned            IsClocking() const          { return 1 ; }  //  Tests if this module item is a VeriClockingDecl

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const               { return _id ; }             // The 'name'
    virtual VeriExpression *    GetClockingEvent() const    { return _clocking_event ; } // The clock expression
    Array *                     GetClockingItems() const    { return _clocking_items ; } // The clocking data declarations (Array of VeriModuleItem)
    virtual VeriScope *         GetScope() const            { return _scope ; }          // Local scope (containing formals and declared Ids).

    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id

    virtual unsigned            VarUsage(VeriVarUsageInfo * /*info*/, unsigned action) ;

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy clocking declaration as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriClockingDecl(const VeriClockingDecl &modport_port_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriClockingDecl(SaveRestore &save_restore) ;

    virtual ~VeriClockingDecl() ;

private:
    // Prevent compiler from defining the following
    VeriClockingDecl() ;                                    // Purposely leave unimplemented
    VeriClockingDecl(const VeriClockingDecl &) ;            // Purposely leave unimplemented
    VeriClockingDecl& operator=(const VeriClockingDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new clocking event in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from clocking items, and puts new clocking item in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef       *_id ;              // The identifier of this construct (the name).
    VeriExpression  *_clocking_event ;  // The clock expression
    Array           *_clocking_items ;  // The clocking data declarations (Array of VeriModuleItem)
    VeriScope       *_scope ;           // The scope (with locally declared identifiers) of this construct
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriClockingDecl

/* -------------------------------------------------------------- */

/*
   VeriConstraintDecl is declaration of a constraint : "constraint <constraint_id> { constraint_blocks } "
   constraint blocks are expressions, and there is no declaration done inside this constraint decl
   (other than the id declaration). So no scope is created.
   So essentially, a constraint decl groups a set of constraint expressions under a single identifier name.
*/
class VFC_DLL_PORT VeriConstraintDecl  : public VeriModuleItem
{
public:
    VeriConstraintDecl(VeriIdDef *id, Array *constraint_blocks) ;
    VeriConstraintDecl(VeriName *name, Array *constraint_blocks, VeriScope *scope) ; // Added for VIPER: 4179

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERICONSTRAINTDECL ; } // Defined in VeriClassIds.h
    virtual unsigned            IsConstraintDecl()              { return 1 ; }

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const                   { return _id ; } // The constraint identifier.
    Array *                     GetConstraintBlocks() const     { return _constraint_blocks ; } // Array of VeriExpression (the constraint expressions)
    virtual VeriScope *         GetScope() const                { return _scope ; }
    VeriName *                  GetConstraintName() const       { return _name ; }
    virtual unsigned            VarUsage(VeriVarUsageInfo * info, unsigned action) ;

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy constraint declaration as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConstraintDecl(const VeriConstraintDecl &constraint_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriConstraintDecl(SaveRestore &save_restore) ;

    virtual ~VeriConstraintDecl() ;

private:
    // Prevent compiler from defining the following
    VeriConstraintDecl() ;                                      // Purposely leave unimplemented
    VeriConstraintDecl(const VeriConstraintDecl &) ;            // Purposely leave unimplemented
    VeriConstraintDecl& operator=(const VeriConstraintDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from constraint block list, and puts new constraint block in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id

protected:
    VeriIdDef *_id ;                // The declared constraint identifier
    Array     *_constraint_blocks ; // Array of VeriExpression..
    VeriScope *_scope ;             // Back pointer to container scope (VIPER: 4179)
    VeriName  *_name ;              // Full name, can be scope name (VIPER: 4179)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConstraintDecl

/* -------------------------------------------------------------- */

/*
   A VeriBindDirective implements the constructs
            "bind <hier_name> <bind_instantiation>"
            "bind <id_ref> : <target_instance_list> <bind_instantiation>"
   The hierarchical name should resolve to a module indentifier or instance path.
   The bind instantiation is an instantiation of a module, program or interface.
*/
class VFC_DLL_PORT VeriBindDirective  : public VeriModuleItem
{
public:
    VeriBindDirective(const VeriName *target_scope, VeriModuleItem *instantiation, VeriScope *bind_scope) ;
    VeriBindDirective(char *target_mod_name, Array *target_inst_list, VeriModuleItem *instantiation, VeriScope *bind_scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIBINDDIRECTIVE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Resolve (analysis at module parsing time)
    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis (analysis after all modules are parsed)
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    const char *                GetTargetModuleName() const   { return _target_mod_name ; } // The reference to a module path
    VeriModuleItem *            GetInstantiation() const      { return _instantiation ; } // The instantiation of a module, program or interface
    Array *                     GetTargetInstanceList() const { return _target_inst_list ; } // The list of target instances
    virtual unsigned            IsBindDirective() const       { return 1 ; }  // Tests if this module item is a VeriBindDirective
    virtual VeriScope *         GetScope() const              { return _scope ; }  // Returns the local scope

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy bind directive as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriBindDirective(const VeriBindDirective &bind_directive, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriBindDirective(SaveRestore &save_restore) ;

    virtual ~VeriBindDirective() ;

private:
    // Prevent compiler from defining the following
    VeriBindDirective() ;                                     // Purposely leave unimplemented
    VeriBindDirective(const VeriBindDirective &) ;            // Purposely leave unimplemented
    VeriBindDirective& operator=(const VeriBindDirective &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ; // VIPER #6560: Process -v/-y/uselib library for this module item (only does something for module instantiations).
    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Insert instantiated module name in the argument specific set

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', and puts new name as a reference to a module or instance path
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' specific instantiation, and puts new instantiation in its place.

    // Internal routine for static elaboration
    void                        DeleteIfProcessed(VeriPseudoTreeNode *mod, const Map *mod_map_tab, const Map *top_nodes) ; // Delete processed bind directive
    virtual void                ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container) ; // Convert comma separated module items to multiple single items

    // VIPER #7968: Mark and check statically elaborated bind directive
    void                        SetStaticElaborated() { _is_static_elaborated = 1 ; }
    unsigned                    IsStaticElaborated() const { return _is_static_elaborated ; }

    virtual void                RemoveFromHierModule() ; // Removes the _instantiation from the hier_module/hier_unit

protected:
    char            *_target_mod_name ;  // The reference to a module path
    Array           *_target_inst_list ; // The bind target instance list
    VeriModuleItem  *_instantiation ;    // The instantiation of a module, program or interface
    VeriScope       *_scope ;            // Local scope containing only the bind instance id(s).
    unsigned         _is_static_elaborated ; // VIPER #7968: Mark the processed bind instance
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBindDirective

/* -------------------------------------------------------------- */

/*
   A VeriOperatorBinding implements the construct
   "bind <overload_operator> function <data_type> function_id ( overload_proto_formals) "
*/
class VFC_DLL_PORT VeriOperatorBinding  : public VeriModuleItem
{
public:
    VeriOperatorBinding(VeriIdDef *oper_id, VeriDataType *return_type, VeriName *func_name, Array *formals) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIOPERATORBINDING ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriIdDef *         GetId() const               { return _oper_id ; }    // identifier created for token '_operator_type' associated with a function prototype
    virtual VeriDataType *      GetDataType() const         { return _data_type ; }  // contains 'range', 'signing' and 'type' info for the return type of the function
    VeriName *                  GetFunctionName() const     { return _func_name ; }  // The reference to a function associated with overload operator
    Array *                     GetFormals() const          { return _formals ; }    // The list of formal data types (VeriDataType*), contains at most two elements

    // VIPER #3369
    virtual void                ClearId()                  { _oper_id = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy operator binding as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriOperatorBinding(const VeriOperatorBinding &bind_directive, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriOperatorBinding(SaveRestore &save_restore) ;

    virtual ~VeriOperatorBinding() ;

private:
    // Prevent compiler from defining the following
    VeriOperatorBinding() ;                                       // Purposely leave unimplemented
    VeriOperatorBinding(const VeriOperatorBinding &) ;            // Purposely leave unimplemented
    VeriOperatorBinding& operator=(const VeriOperatorBinding &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', and puts new name as a reference to a module or instance path
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node) ; // Delete 'old_data' specific data type, and puts new data type in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef     *_oper_id ;   // Identifier declared in scope to represent this overload operator
    VeriDataType  *_data_type ; // contains 'range', 'signing' and 'type' info for the return type of the function
    VeriName      *_func_name ; // The reference to a function associated with overload operator
    Array         *_formals ;   // The list of formal data types (VeriDataType*) for this operator, contains at most two elements (array of VeriDataType*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriOperatorBinding

/* -------------------------------------------------------------- */

/*
   A VeriNetAias implements the construct
   "alias <net_lvalue> = <net_lvalue> { = <net_lvalue>} ; "
*/
class VFC_DLL_PORT VeriNetAlias  : public VeriModuleItem
{
public:
    VeriNetAlias(VeriExpression *net_lvalue, Array *net_lvalues) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERINETALIAS ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    VeriExpression *            GetLeftMember() const       { return _left ; }
    Array *                     GetMembers() const          { return _members ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy signal aliasing as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNetAlias(const VeriNetAlias &net_alias, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriNetAlias(SaveRestore &save_restore) ;

    virtual ~VeriNetAlias() ;

private:
    // Prevent compiler from defining the following
    VeriNetAlias() ;                                // Purposely leave unimplemented
    VeriNetAlias(const VeriNetAlias &) ;            // Purposely leave unimplemented
    VeriNetAlias& operator=(const VeriNetAlias &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpressionInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place (for backward compatibility only)
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place

    // Static Elaboration (internal routines) :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set *defparams) ;

protected:
    VeriExpression *_left ;    // Left most member of alias list
    Array          *_members ; // Remaining members of alias list (Array of VeriExpression*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNetAlias

/* -------------------------------------------------------------- */

/*
   A VeriTimeUnit implements the constructs
    "timeunit <time_literal> [ / <time_literal>] ;"
   and
    "timeprecision <time_literal> ;"
*/
class VFC_DLL_PORT VeriTimeUnit  : public VeriModuleItem
{
public:
    VeriTimeUnit(unsigned type, VeriExpression *time_literal, VeriExpression *time_precision=0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERITIMEUNIT ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Object locator
    virtual unsigned            IsTimeUnitDecl() const              { return 1 ; }

    // Local/Virtual Accessor Methods to parse-tree sub-nodes
    virtual VeriExpression *    GetTimeLiteral() const      { return _literal ; }
    virtual unsigned            GetTimeUnitType() const     { return _type ; }
    unsigned                    GetDeclType() const         { return _type ; }
    virtual VeriExpression *    GetTimePrecision() const    { return _time_precision ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy & id_map_table) const ; // Copy signal aliasing as module item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTimeUnit(const VeriTimeUnit &time_unit, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTimeUnit(SaveRestore &save_restore) ;

    virtual ~VeriTimeUnit() ;

private:
    // Prevent compiler from defining the following
    VeriTimeUnit() ;                                // Purposely leave unimplemented
    VeriTimeUnit(const VeriTimeUnit &) ;            // Purposely leave unimplemented
    VeriTimeUnit& operator=(const VeriTimeUnit &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpressionInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place (for backward compatibility only : should be removed)
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place

private :
    void                        CheckTimeLiteral(const VeriExpression *literal, unsigned time_precision) ; // VIPER #8304
protected:
    unsigned        _type ; // VERI_TIMEUNIT or VERI_TIMEPRECISION token, represent timeunit or timeprecision decl
    VeriExpression *_literal ; // Time literal (VeriTimeLiteral*)
    VeriExpression *_time_precision ; // optional time precision (VeriTimeLiteral*)(VIPER #8304)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTimeUnit

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling signal declaration inside clocking declaration
   VeriClockingSigDecl implements the construct
    <clocking_direction> <list_of_clocking_decl_assign> ;
    default <default_skew> ;
   This is one type of signal declaration, so this class is derived from VeriDataDecl
*/
class VFC_DLL_PORT VeriClockingSigDecl : public VeriDataDecl
{
public:
    VeriClockingSigDecl(VeriClockingDirection *clocking_dir, VeriClockingDirection *o_clocking_dir, Array *ids) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICLOCKINGSIGDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriClockingDirection *     GetClockingDirection() const       { return _clk_dir ; }
    VeriClockingDirection *     GetOutputClockingDirection() const { return _o_clk_dir ; }
    unsigned                    IsDefaultSkew() const              { return (_ids.Size()) ? 0:1 ; } // This class represents default skew when no identifiers are specified

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table)const ; // Copy net declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriClockingSigDecl(const VeriClockingSigDecl &clocking_sig_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriClockingSigDecl(SaveRestore &save_restore) ;

    virtual ~VeriClockingSigDecl() ;

private:
    // Prevent compiler from defining the following
    VeriClockingSigDecl() ;                               // Purposely leave unimplemented
    VeriClockingSigDecl(const VeriClockingSigDecl &) ;            // Purposely leave unimplemented
    VeriClockingSigDecl& operator=(const VeriClockingSigDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildClockingDirInternal(VeriClockingDirection *old_dir, VeriClockingDirection *new_dir, unsigned delete_old_node) ; // Delete 'old_dir', and puts new clocking direction in its place.

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriClockingDirection *_clk_dir ;   // Clocking direction (direction and skew)
    VeriClockingDirection *_o_clk_dir ; // Clocking direction of output
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriClockingSigDecl

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling covergroup declaration
   VeriCovergroup implements the construct
   covergroup <covergroup_identifier> [([ <tf_port_list>])] [coverage_event] ;
      { coverage_spec_or_option }
   endgroup
*/
class VFC_DLL_PORT VeriCovergroup : public VeriModuleItem
{
public:
    VeriCovergroup(VeriIdDef *id, Array *port_list, VeriExpression *event, Array *items, VeriScope *scope) ;
    VeriCovergroup(VeriIdDef *id, Array *port_list, VeriExpression *event, VeriModuleItem *sample_func, Array *items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICOVERGROUP ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    // Accessor methods
    virtual VeriIdDef *         GetId() const               { return _id ; } // covergroup declaration identifier
    Array *                     GetPortConnects() const     { return _port_connects ; }
    VeriExpression *            GetCoverageEvent() const    { return _event ; }
    VeriModuleItem *            GetSampleFunction() const   { return _sample_func ; }
    virtual Array *             GetItems() const            { return _items ; } // Return array of VeriModuleItem*, item list.
    virtual VeriScope *         GetScope() const            { return _scope ; } // The local scope (if any) of this module item
    virtual Array *             GetPorts() const            { return _ports ; } // Return array of VeriIdDef* of their ports in order.
    virtual unsigned            IsCovergroup() const        { return 1 ; }

    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy covergroup

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCovergroup(const VeriCovergroup &covergroup_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCovergroup(SaveRestore &save_restore) ;

    virtual ~VeriCovergroup() ;

private:
    // Prevent compiler from defining the following
    VeriCovergroup() ;                                  // Purposely leave unimplemented
    VeriCovergroup(const VeriCovergroup &) ;            // Purposely leave unimplemented
    VeriCovergroup& operator=(const VeriCovergroup &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression* new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_stmt', and puts new statement in its place.

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef       *_id ;            // Covergroup id
    Array           *_port_connects ; // Port list (array of VeriExpression*)
    VeriExpression  *_event ;         // Coverage event
    VeriModuleItem  *_sample_func ;   // sample function (P1800-2009)
    Array           *_items ;         // List of coverage spec or options (array of VeriModuleItem*)
    VeriScope       *_scope ;         // Scope of covergroup
// Created at analysis time :
    Array           *_ports ;         // Array of VeriIdDef*. List of function arguments in ORDER. Created in constructor from 'decls' and 'ansi_io_list'..
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCovergroup

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling coverage option
   VeriCoverageOption implements the construct
    option.<member_identifier> = <expression>
    type_option.<member_identifier> = <expression>
*/
class VFC_DLL_PORT VeriCoverageOption : public VeriModuleItem
{
public:
    VeriCoverageOption(VeriName *option, VeriExpression *value) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICOVERAGEOPTION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    // Accessor methods
    VeriName *                  GetOption() const           { return _option ; } // Get option name
    VeriExpression *            GetValue() const            { return _value ; }  // Get value of the option

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy coverage option

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCoverageOption(const VeriCoverageOption &coverage_option, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCoverageOption(SaveRestore &save_restore) ;

    virtual ~VeriCoverageOption() ;

private:
    // Prevent compiler from defining the following
    VeriCoverageOption() ;                                      // Purposely leave unimplemented
    VeriCoverageOption(const VeriCoverageOption &) ;            // Purposely leave unimplemented
    VeriCoverageOption& operator=(const VeriCoverageOption &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression* new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', and puts new name in its place.

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriName       *_option ; // Option
    VeriExpression *_value ;  // Value of the option
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCoverageOption

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling coverage spec i.e coverage point and cross coverage
   VeriCoverageSpec implements the construct
      [ <cover_point_identifier> :] coverpoint <expression>          [iff (<expression>)] <bins_or_empty>
   |  [ <cover_point_identifier> :] cross      <list_of_coverpoints> [iff (<expression>)] <select_bins_or_empty>
*/
class VFC_DLL_PORT VeriCoverageSpec : public VeriModuleItem
{
public:
    VeriCoverageSpec(unsigned type, VeriIdDef *id, VeriExpression *expr, VeriExpression *iff_expr, Array *bins, VeriScope *scope) ;
    VeriCoverageSpec(unsigned type, VeriIdDef *id, Array *coverpoints, VeriExpression *iff_expr, Array *bins, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICOVERAGESPEC ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    // Accessor methods
    virtual unsigned            GetType() const             { return _type ; } // Type of coverage VERI_COVERPOINT or VERI_CROSS
    virtual VeriIdDef *         GetId() const               { return _id ; }
    Array *                     GetExprList() const         { return _point_list ; } // Get coverpoint specific expression (array size 1) for coverpoint or coverpoint list for cross cover
    VeriExpression *            GetIffExpression() const    { return _iff_expr ; }
    Array *                     GetBins() const             { return _bins ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }
    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy coverage spec

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCoverageSpec(const VeriCoverageSpec &coverage_spec, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCoverageSpec(SaveRestore &save_restore) ;

    virtual ~VeriCoverageSpec() ;

private:
    // Prevent compiler from defining the following
    VeriCoverageSpec() ;                                    // Purposely leave unimplemented
    VeriCoverageSpec(const VeriCoverageSpec &) ;            // Purposely leave unimplemented
    VeriCoverageSpec& operator=(const VeriCoverageSpec &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression* new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item', and puts new item in its place.

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned        _type ;       // Type of coverage VERI_COVERPOINT or VERI_CROSS
    VeriIdDef      *_id ;         // Coverage point identifier
    Array          *_point_list ; // Point expression list (array of VeriExpression*)
    VeriExpression *_iff_expr ;   // 'iff' expression
    Array          *_bins ;       // List of bins or options (array of VeriModuleItem*)
    VeriScope      *_scope ;      // Scope of coverpoint
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCoverageSpec

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling bin declaration
   VeriBinDecl implements the construct
    [ wildcard ] <bins_keyword> <bin_identifier> [[[ <expression>]]] = { <open_range_list> } [ iff (expression)]
   | [ wildcard ] <bins_keyword> <bin_identifier> [[]] = <trans_list> [ iff (<expression>)]
   | <bins_keyword> <bin_identifier> [[[ <expression>]]] = default [ iff (<expression>)]
   | <bins_keyword> <bin_identifier> = default sequence [ iff (<expression>)]

   Right hand side of '=' will be treated as initial value of bin_identifier
*/
class VFC_DLL_PORT VeriBinDecl : public VeriModuleItem
{
public:
    VeriBinDecl(unsigned wildcard, unsigned bins_keyword, VeriIdDef *bin_id) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIBINDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    // Accessor methods
    unsigned                    GetWildcard() const         { return _wildcard ; } // Return VERI_WILDCARD token if specified in declaration
    unsigned                    GetBinsKeyword() const      { return _bins_keyword ; } // Return bin keyword (VERI_BINS, VERI_ILLEGAL_BINS, VERI_IGNOREBINS)
    virtual VeriIdDef *         GetId() const               { return _id ; } // Return bins identifier

    // VIPER #3369
    virtual void                ClearId()                  { _id = 0 ; }  // Clear the id

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy bin declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriBinDecl(const VeriBinDecl &bin_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriBinDecl(SaveRestore &save_restore) ;

    virtual ~VeriBinDecl() ;

private:
    // Prevent compiler from defining the following
    VeriBinDecl() ;                               // Purposely leave unimplemented
    VeriBinDecl(const VeriBinDecl &) ;            // Purposely leave unimplemented
    VeriBinDecl& operator=(const VeriBinDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned    _wildcard ;     // VERI_WILDCARD token or 0
    unsigned    _bins_keyword ; // Bins keyword VERI_BINS, VERI_ILLEGAL_BINS or VERI_IGNOREBINS
    VeriIdDef  *_id ;           // Bins identifier (Has a initial value)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBinDecl

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling package import declaration
   VeriImportDecl implements the construct
   :: import <package_import_item> {, <package_import_item>} ";"

   package_import_item :: <package_identifier> :: <identifier>
                          <package_identifier> :: *
*/
class VFC_DLL_PORT VeriImportDecl : public VeriModuleItem
{
public:
    explicit VeriImportDecl(Array *import_items) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIIMPORTDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    //virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // VIPER #6896: Conversion of verilog package to vhdl package:
    virtual unsigned            IsImportDecl() const        { return 1 ; }

    // Accessor methods
    virtual Array *             GetItems() const            { return _items ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy import declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriImportDecl(const VeriImportDecl &import_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriImportDecl(SaveRestore &save_restore) ;

    virtual ~VeriImportDecl() ;

private:
    // Prevent compiler from defining the following
    VeriImportDecl() ;                                  // Purposely leave unimplemented
    VeriImportDecl(const VeriImportDecl &) ;            // Purposely leave unimplemented
    VeriImportDecl& operator=(const VeriImportDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is base class name, and puts new base class name in its place.
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set * /*defparams*/) ;

protected:
    Array *_items ; // Array of package import items (array of VeriScopeName*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriImportDecl

/* -------------------------------------------------------------- */

/*
   VIPER #5710 : Std 1800-2009 addition
   VeriLetDecl implements the construct
   let let_identifier (let_port_list) = expression ";"
*/
class VFC_DLL_PORT VeriLetDecl : public VeriModuleItem
{
public:
    VeriLetDecl(VeriIdDef *let_id, Array *let_port_list, VeriExpression *expr, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERILETDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    virtual unsigned            VarUsage(VeriVarUsageInfo *info, unsigned action) ;

    virtual unsigned            IsLetDecl() const           { return 1 ; }

    // Accessor methods
    virtual VeriIdDef *         GetId() const               { return _id ; } // The name
    Array *                     GetAnsiIOList() const       { return _ansi_io_list ; } // List of formal ansi-port decls (id's (with direction,datatype etc)).
    virtual VeriExpression *    GetExpression() const       { return _expr ; }  // The expression of this let declaration
    virtual VeriScope *         GetScope() const            { return _scope ; } // Local scope (containing formals)
    virtual Array *             GetPorts() const            { return _ports ; } // Array of VeriIdDef's: the let formals, in order of declaration. Created in constructor.

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy let declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriLetDecl(const VeriLetDecl &let_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriLetDecl(SaveRestore &save_restore) ;

    virtual ~VeriLetDecl() ;

private:
    // Prevent compiler from defining the following
    VeriLetDecl() ;                               // Purposely leave unimplemented
    VeriLetDecl(const VeriLetDecl &) ;            // Purposely leave unimplemented
    VeriLetDecl& operator=(const VeriLetDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new sequence spec in its place.

    // Static elaboration internal routines :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriBaseValue *     StaticElaborateFunction(Array *arg_list, ValueTable *table, unsigned expect_nonconst) ;

protected:
    VeriIdDef      *_id ; // Let identifier
    Array          *_ansi_io_list ; // Array of VeriAnsiPortDecl*. Formal arguments
    VeriExpression *_expr ; // Value of let
    VeriScope      *_scope ; // The scope (with locally declared identifiers)
// Created at analysis time :
    Array          *_ports ; // Array of VeriIdDef. The formals of this construct
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriLetDecl

/* -------------------------------------------------------------- */

/*
   VIPER #5710 : Std 1800-2009 addition
   VeriDefaultDisableIff implements the construct
   default disable iff expression_or_dist ";"
*/
class VFC_DLL_PORT VeriDefaultDisableIff : public VeriModuleItem
{
public:
    explicit VeriDefaultDisableIff(VeriExpression *expr_or_dist) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDEFAULTDISABLEIFF ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    virtual VeriExpression *    GetExpression() const       { return _expr_or_dist ; }  // The expression of default disable iff condition

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy default disable iff declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDefaultDisableIff(const VeriDefaultDisableIff &disable_iff, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDefaultDisableIff(SaveRestore &save_restore) ;

    virtual ~VeriDefaultDisableIff() ;

private:
    // Prevent compiler from defining the following
    VeriDefaultDisableIff() ;                                         // Purposely leave unimplemented
    VeriDefaultDisableIff(const VeriDefaultDisableIff &) ;            // Purposely leave unimplemented
    VeriDefaultDisableIff& operator=(const VeriDefaultDisableIff &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place.

    // Static elaboration internal routines :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_expr_or_dist ; // default disable iff condition
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDefaultDisableIff

/* -------------------------------------------------------------- */

/*
   Viper #5845: Std 1800-2009 addition for modeling package export declaration
   VeriExportDecl implements the construct
   :: export <package_import_item> {, <package_import_item>} ";"
     | export *::* ";"

   package_import_item :: <package_identifier> :: <identifier>
                          <package_identifier> :: *
*/
class VFC_DLL_PORT VeriExportDecl : public VeriModuleItem
{
public:
    explicit VeriExportDecl(Array *import_items) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIEXPORTDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual unsigned            IsExportDecl() const        { return 1 ; }  // Return 1 for export declaration

    // Variable usage analysis
    //virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    // Accessor methods
    virtual Array *             GetItems() const            { return _items ; }

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy import declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriExportDecl(const VeriExportDecl &import_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriExportDecl(SaveRestore &save_restore) ;

    virtual ~VeriExportDecl() ;

private:
    // Prevent compiler from defining the following
    VeriExportDecl() ;                                  // Purposely leave unimplemented
    VeriExportDecl(const VeriExportDecl &) ;            // Purposely leave unimplemented
    VeriExportDecl& operator=(const VeriExportDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is base class name, and puts new base class name in its place.

protected:
    Array *_items ; // Array of package export items (array of VeriScopeName*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriExportDecl

/* -------------------------------------------------------------- */

/*
   VIPER #6387: IEEE 1800 addition of Direct Programming Interface (DPI)
   export/import task/function declarations.
   dpi_import_export ::= // from A.2.6
         import dpi_spec_string [ dpi_function_import_property ] [ c_identifier = ] dpi_function_proto ;
       | import dpi_spec_string [ dpi_task_import_property ] [ c_identifier = ] dpi_task_proto ;
       | export dpi_spec_string [ c_identifier = ] function function_identifier ;
       | export dpi_spec_string [ c_identifier = ] task task_identifier ;
   dpi_spec_string ::= "DPI-C" | "DPI"
   dpi_function_import_property ::= context | pure
   dpi_task_import_property ::= context
   dpi_function_proto ::= function_prototype
   dpi_task_proto ::= task_prototype
   function_prototype ::= function data_type_or_void function_identifier ( [ tf_port_list ] )
   task_prototype ::= task task_identifier ( [ tf_port_list ] ) // from A.2.7
*/
class VFC_DLL_PORT VeriDPIFunctionDecl : public VeriFunctionDecl
{
public:
    VeriDPIFunctionDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriFunctionDecl *func_proto) ;
    VeriDPIFunctionDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriName *func, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDPIFUNCTIONDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    virtual unsigned            IsDPIMethod() const             { return 1 ; }

    // Accessor methods
    unsigned                    GetDPIType() const              { return _type ; }          // Returns either VERI_IMPORT or VERI_EXPORT
    const char *                GetDPISpec() const              { return _spec_string ; }   // Possible values "DPI" or "DPI-C"
    unsigned                    GetDPIProperty() const          { return _property ; }      // Returns either VERI_CONTEXT or VERI_PURE or 0
    const char *                GetDPICIdentifierName() const   { return _c_identifier ; }  // The identifier recognized in the foreign language side (C?)

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy import declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDPIFunctionDecl(const VeriDPIFunctionDecl &dpi_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDPIFunctionDecl(SaveRestore &save_restore) ;

    virtual ~VeriDPIFunctionDecl() ;

private:
    // Prevent compiler from defining the following
    VeriDPIFunctionDecl() ;                                       // Purposely leave unimplemented
    VeriDPIFunctionDecl(const VeriDPIFunctionDecl &) ;            // Purposely leave unimplemented
    VeriDPIFunctionDecl& operator=(const VeriDPIFunctionDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Internal routines for Static Elaboration:
    virtual VeriBaseValue *     StaticElaborateFunction(Array * /*arg_list*/, ValueTable * /*n*/, unsigned /*expect_nonconst*/) { return 0 ; }
    //unsigned                    InitializePorts(const Array * /*arg_list*/, ValueTable * /*prev_tab*/, ValueTable * /*cur_tab*/) const { return 0 ; }
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /*top_mod_nodes*/, Set * /*defparams*/) { return 1 ; } // Return 1 here otherwise elaboration fails

protected:
    unsigned   _type ;          // Token: VERI_IMPORT or VERI_EXPORT
    char      *_spec_string ;   // Possible values: "DPI" or "DPI-C"
    unsigned   _property ;      // Token: VERI_CONTEXT or VERI_PURE
    char      *_c_identifier ;  // The identifier name recognized in the foreign language side (C)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDPIFunctionDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriDPITaskDecl : public VeriTaskDecl
{
public:
    VeriDPITaskDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriTaskDecl *task_proto) ;
    VeriDPITaskDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriName *task, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDPITASKDECL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    virtual unsigned            IsDPIMethod() const             { return 1 ; }

    // Accessor methods
    unsigned                    GetDPIType() const              { return _type ; }          // Returns either VERI_IMPORT or VERI_EXPORT
    const char *                GetDPISpec() const              { return _spec_string ; }   // Possible values "DPI" or "DPI-C"
    unsigned                    GetDPIProperty() const          { return _property ; }      // Returns either VERI_CONTEXT or 0
    const char *                GetDPICIdentifierName() const   { return _c_identifier ; }  // The identifier recognized in the foreign language side (C)

    // Parse tree copy routine :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy import declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDPITaskDecl(const VeriDPITaskDecl &dpi_decl, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDPITaskDecl(SaveRestore &save_restore) ;

    virtual ~VeriDPITaskDecl() ;

private:
    // Prevent compiler from defining the following
    VeriDPITaskDecl() ;                                   // Purposely leave unimplemented
    VeriDPITaskDecl(const VeriDPITaskDecl &) ;            // Purposely leave unimplemented
    VeriDPITaskDecl& operator=(const VeriDPITaskDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /*top_mod_nodes*/, Set * /*defparams*/) { return 1 ; } // Return 1 here otherwise elaboration fails

protected:
    unsigned   _type ;          // Token: VERI_IMPORT or VERI_EXPORT
    char      *_spec_string ;   // Possible values: "DPI" or "DPI-C"
    unsigned   _property ;      // Token: VERI_CONTEXT
    char      *_c_identifier ;  // The identifier name recognized in the foreign language side (C)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDPITaskDecl

/* -------------------------------------------------------------- */
#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_MODULEITEM_H_
