/*
 *
 * [ File Version : 1.568 - 2014/02/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_EXPRESSION_H_
#define _VERIFIC_VERI_EXPRESSION_H_

#include "VeriTreeNode.h"
#include "Strings.h"
//CARBON_BEGIN
#include "Map.h"
//CARBON_END

#ifdef VERILOG_USE_NAME_EXTENDED_EXPRESSION
#include "SaveRestore.h"

#include "Array.h"

#endif // VERILOG_USE_NAME_EXTENDED_EXPRESSION

#include <fstream>

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;

class Netlist ;
class Instance ;

class VeriIdDef ;
class VeriName ;
class VeriRange ;
class VeriScope ;
class VeriDataFlow ;
class ValueTable;
class VeriBaseValue;
class VeriDataType ;
class HierIdWithDeclScope ;
class VeriElabArrayInst ;
class VeriPseudoTreeNode ;
class ModuleVisit ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
class VhdlIdDef ;
class VhdlExpression ;
class VhdlRange ;
class VhdlName ;
#endif

class VeriVarUsageInfo ;
class VeriTypeInfo ;
class VeriConstraint ;

class VeriCaseOperatorItem ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896:
class VhdlScope ;
class VhdlTypeDef ;
class VhdlTypeId ;
class VhdlSubtypeIndication ;
#endif

// Handy macro's
#undef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#undef ABS
#define ABS(s) (((s) > 0) ? (s) : -(s))
// VIPER #3821: Always use this macro to check if 'i' is outside the range 'l:r':
#undef CHECK_INDEX
#define CHECK_INDEX(i,l,r)  (((i)>(l) && (i)>(r)) || ((i)<(l) && (i)<(r)))

// Macros for Verilog context size/sign modeling in a single 'int' containing both sign and size info :
// These are only used during elaboration.
#define MAKE_CONTEXT_SIZE_SIGN(size,sign) ((sign) ? (-((int)(size))) : (int)(size))             // returns a 'int' containing both sign and size info
#define GET_CONTEXT_SIZE(size_sign) (((size_sign) < 0) ? (unsigned)(-(size_sign)) : (unsigned)(size_sign)) // return the 'unsigned' size from a size_sign 'int'
#define GET_CONTEXT_SIGN(size_sign) (((size_sign) < 0) ? 1 : 0)                  // return the sign (1 for 'signed') from a size_sign 'int'

/* -------------------------------------------------------------- */

template <class T>
class VFC_DLL_PORT VeriNameExtendedExpression : public T
{
public:
    VeriNameExtendedExpression(T *obj, char *name) : T(*obj, 1), _extended_name(name)
    {
#ifdef VERILOG_SHARE_STRINGS
        _extended_name = Strings::CreateConstantString(name) ;
        Strings::free(name) ;
#endif
        delete obj ;
    }

public:
    // virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERI_NAME_EXTENDED_EXPRESSION ; }
    virtual const char *        NamedFormal()   const       { return _extended_name ; } // Return the formal name (for a port-connect only)
    virtual unsigned            IsNameExtendedClass() const { return 1 ; }

#ifdef VERILOG_USE_NAME_EXTENDED_EXPRESSION

    void PrettyPrint(ostream &f, unsigned level) const
    {
        if (_extended_name) {
            f << "." ; T::PrintIdentifier(f, _extended_name) ; f << "(" ;
        }
        T::PrettyPrint(f,level) ;
        if (_extended_name) f << ")" ;
    }

public:
    virtual VeriExpression *CopyExpression(VeriMapForCopy &id_map_table) const { return new VeriNameExtendedExpression<T>(*this, id_map_table) ; }  // virtual function to copy any expression

    virtual VeriExpression *CopyBaseExpression(VeriMapForCopy &id_map_table) const { return T::CopyExpression(id_map_table) ; }

#endif // VERILOG_USE_NAME_EXTENDED_EXPRESSION

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriNameExtendedExpression()
    {
#ifndef VERILOG_SHARE_STRINGS
        Strings::free(_extended_name) ;
#endif
       _extended_name = 0 ;
    }

#ifdef VERILOG_USE_NAME_EXTENDED_EXPRESSION
    void AccumulatePorts(Array &ports) const
    {
        // Must be a port-expression of named form, (VeriPortConnect)
        // or a sliced/indexed/concat port expression.
        // That is always a single port, but since we do not have an identifier, insert 0 here.
        ports.InsertLast(0) ;
    }

    // Persistence (Binary save via SaveRestore class)
    virtual void Save(SaveRestore &save_restore) const
    {
        save_restore.SaveInteger(T::GetClassId()) ;
        T::Save(save_restore) ;
        save_restore.SaveString(_extended_name) ;
    }

protected:
    VeriNameExtendedExpression(const VeriNameExtendedExpression &veri_expr, VeriMapForCopy &id_map_table)
        : T(veri_expr, id_map_table), _extended_name(0)
    {
#ifdef VERILOG_SHARE_STRINGS
        _extended_name = Strings::CreateConstantString(veri_expr._extended_name) ;
#else
        _extended_name = Strings::save(veri_expr._extended_name) ;
#endif
    }
#endif // VERILOG_USE_NAME_EXTENDED_EXPRESSION
private:
#ifdef VERILOG_SHARE_STRINGS
    const char *_extended_name ;
#else
    char       *_extended_name ;
#endif
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNameExtendedExpression

/* -------------------------------------------------------------- */

/*
    VeriExpression is a base class for all Verilog expression parse tree nodes.
    This varies from constants (VeriConst), name references (VeriIdRef, VeriIndexedId etc)
    to operators (VeriUnaryOperator, VeriBinaryOperator) and odd-balls like
    port associations (VeriPortConnect) which appears in port-association lists
    of instantiations and '95 style module port lists, and VeriAnsiPortDecl (which appears in module port list).

    Important elaboration routines on an expression are Elaborate(), which creates an elaborated value
    for the expression in the present dataflow area. As if evaluating the expression
    And Assign() which assigns a value to the (RHS) expression.
*/

class VFC_DLL_PORT VeriExpression  : public VeriTreeNode
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    virtual VeriConst *         ConstCast() const          { return 0 ;}  // Default catcher

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    // Variable usage analysis (post design-parsing analysis/semantic checks)
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Resolve (during module-parsing) semantic checks
    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;   // Resolve an expression (done after analysis of a module). Any unresolved identifier references will be resolved (point to the visible VeriIdDef with that name, in the proper scope).

    // Tests:
    virtual unsigned            IsConst() const            { return 0 ; }  // See if this expression node is a constant.
    virtual unsigned            IsUnsizedConst() const     { return 0 ; }  // Indicate that this constant is an unsized bit like '1 or unsized number like 'b11. Return 0 for anything else.
    virtual unsigned            IsUnsizedBit() const       { return 0 ; }  // Indicate that this constant is an unsized bit like '1. Return 0 for anything else.
    virtual unsigned            IsUnsizedNumber() const    { return 0 ; }  // Indicate that this constant is an unsized number like 'b11. Return 0 for anything else.
    virtual unsigned            HasXZ() const              { return 0 ; }  // See if constant has x or z.
    virtual unsigned            IsRange() const            { return 0 ; }  // See if this expression represents a range
    virtual unsigned            IsIdRef() const            { return 0 ; }  // Check if this name is a simple id reference
    virtual unsigned            IsHierName() const         { return 0 ; }  // Returns '1' if 'this' is a (dot-separated) hierarchical name reference
    virtual unsigned            IsScopeName() const        { return 0 ; }  // Returns '1' if 'this' is a ('::'-separated) hierarchical name reference
    virtual unsigned            IsConcat() const           { return 0 ; }  // Returns '1' if 'this' is a concatenation
    virtual unsigned            IsMultiConcat() const      { return 0 ; }  // Returns '1' if 'this' is a multi-concatenation
    virtual unsigned            IsAssignPattern() const    { return 0 ; }  // Check if assignment pattern
    virtual unsigned            IsMultiAssignPattern() const { return 0 ; }  // Check if multi-assignment pattern
    virtual unsigned            IsEdge(unsigned /*edge_type*/) const { return 0 ; } // Check if the expression is preceded by a POSEDGE/NEGEDGE edge type, or (by default) any edge type.
    virtual unsigned            IsOpen() const             { return 0 ; }  // Check if this expression is an open port association.
    virtual unsigned            IsDotStar() const          { return 0 ; }  // Check if this expression is a .* style port connection (System Verilog only)
    virtual unsigned            IsAnsiPortDecl() const     { return 0 ; }  // Check if this is an ANSI (port) declaration.
    virtual unsigned            IsNull() const             { return 0 ; }  // Check if this expression is a null literal (System Verilog only)
    virtual unsigned            IsDollar() const           { return 0 ; }  // Check if this expression is a dollar literal.
    virtual unsigned            IsSelectCondition() const  { return 0 ; }  // Return 1 if this is a select condition else return 0
    virtual unsigned            IsQuestionColon() const    { return 0 ; }  // Return 1 if this is a ?: operator
    virtual unsigned            IsIfOperator() const       { return 0 ; }  // Return 1 if this is a (System Verilog) if-operator
    virtual unsigned            IsCaseOperator() const     { return 0 ; }  // Return 1 if this is a (System Verilog) case-operator
    virtual unsigned            IsCondPredicate() const    { return 0 ; }  // Return 1 if this is a (System Verilog) condpredicate
    virtual unsigned            IsClockedSequence() const  { return 0 ; }  // Return 1 if this is a (System Verilog) clocked sequence
    virtual unsigned            IsPatternMatch() const     { return 0 ; }  // Return 1 if this is a (System Verilog) pattern match
    virtual unsigned            IsReal() const             { return 0 ; }  // Return 1 if this is a real value, valid for VeriRealVal only
    virtual unsigned            IsInt() const              { return 0 ; }  // Return 1 if this is a integer value, valid for VeriIntVal only
    virtual unsigned            IsTaggedUnion() const      { return 0 ; }  // Return 1 if this is a (Syatem Verilog) tagged union
    virtual unsigned            IsConcatItem() const       { return 0 ; }  // Return 1 if this is a (Syatem Verilog) <member> : <value>
    virtual unsigned            IsStreamingConcat() const  { return 0 ; }  // Return 1 if this is a (System Verilog) streaming concatenation
    virtual unsigned            IsFunctionCall() const     { return 0 ; }  // Return 1 if this is a function call
    virtual unsigned            IsNewExpr() const          { return 0 ; }  // Return 1 if this is a 'new' expression (allocator)
    virtual unsigned            IsInvolvingReal() ; // VIPER #5048: Returns 1 if the expression or any of its sub-expressions involves real
    virtual unsigned            IsSolveBefore()            { return 0 ; }  // Return 1 if this is a VeriSolveBefore. Viper #5277
    virtual unsigned            IsSequenceConcat() const   { return 0 ; }  // Return 1 if this is a VeriSequenceConcat.
    virtual unsigned            IsDelayOrEventControl() const { return 0 ; } // Return 1 if this is a VeriDelayOrEventControl
    virtual unsigned            IsTypeOperator() const     { return 0 ; }
    virtual unsigned            IsMinTypMax() const        { return 0 ; }
    virtual unsigned            IsEventWithAt() const      { return 0 ; }
    virtual unsigned            IsStep() const             { return 0 ; }  // VIPER #6961 : For 1step

    // Misc. Get routines :
    virtual const char *        NamedFormal() const        { return 0 ; } // To get the formal name of a 'port-connection', or 0 if this is not a port-connection.
    virtual VeriExpression *    GetConnection() const      { return const_cast<VeriExpression*>(this) ; } // return the 'actual' expression, only for a 'port-connection' that is different from 'this'.
    virtual VeriExpression *    GetMemberLabel() const     { return 0 ; } // The expression which identifies the member label. If 0, is is the 'default' label.
    virtual VeriExpression *    GetExpr() const            { return 0 ; } // The expression which is casted.
    virtual VeriDataType *      GetDataType() const        { return 0 ; } // Return type of an expression. Currently returns only for identifier references
    virtual VeriScope *         GetScope() const           { return 0 ; } // Return scope of an expression if any. Only some SV expressions have a scope.
    virtual Array *             GetExpressions() const     { return 0 ; }  // Return array of VeriExpressions for concat and multi-concat
    virtual VeriScope *         GetDotStarScope() const    { return 0 ; } // Back-pointer to the scope in which this .* construct appears. Used to find the missing actuals.
    virtual VeriExpression *    GetEventExpression() const { return 0 ; } // The clocking event for the sequence (argument) returns for ClockedExpression
    virtual VeriExpression *    GetCycleDelayRange() const { return 0 ; } // int expr or range defining number of cycles between left-end and right-start.
    virtual Array *             GetEventControl() const    { return 0 ; } // Event control from VeriDelayOrEventControl
    virtual VeriExpression *    GetRepeat() const          { return 0 ; } // Get repeat VeriExpression of multi-concat

    // Extra routines :
    virtual char *              Image() const              { return 0 ; }  // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.
    virtual int                 Integer() const            { return 0 ; }  // For VeriConst expression nodes only : create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here. Maybe assert ?
    virtual unsigned            Type() const               { return 0 ; }  // Get the base data type token (VERI_REG, VERI_WIRE, VERI_INTEGER, etc) for VeriDataType. Works after Resolve().
    virtual Array *             GetIds() const             { return 0 ; }  // Several 'expression' items (such as ANSI port decls) contain a list of declared identifiers.
    virtual Map *               GetEnumLiterals() const    { return 0 ; }

    // For type-expressions (System Verilog)
    virtual unsigned            IsDataType() const         { return 0 ; }  // Returns '1' if 'this' is a type expression (System Verilog VeriDataType).
    virtual VeriDataType *      DataTypeCast() const       { return 0 ; }  // Dynamic Cast to a VeriDataType class (returns non-zero only if this expression is a data type).
    virtual VeriRange *         RangeCast() const          { return 0 ; }  // Dynamic Cast to a VeriRange class (returns non-zero only if this expression is a range) .
    virtual VeriName *          NameCast() const           { return 0 ; }  // Dynamic Cast to a VeriName class (returns non-zero only if this expression is a VeriName).
    virtual VeriAnsiPortDecl *  AnsiPortDeclCast() const   { return 0 ; }  // Dynamic Cast to a VeriAnsiPortDecl class (returns non-zero only if this expression is a VeriAnsiPortDecl).
    VeriExpression *            ExprCast() const           { return const_cast<VeriExpression*>(this) ; }

    // for names (VeriName objects : VeriIdRef/VeriIndexedId/VeriSelectedName..)
    virtual VeriIdDef *         FullId() const             { return 0 ; }  // For VeriIdRef expression nodes only : return pointer to the reference iddef. 0 for all other nodes.

    virtual const char *        GetName() const            { return 0 ; }  // Get unresolved name (a string representative of the id-reference in this expression). Only works for VeriName classes.
    virtual VeriIdDef *         GetId() const              { return 0 ; }  // Get resolved referenced identifier of a name

    virtual VeriName *          GetPrefix() const          { return 0 ; }  // Return prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's, etc ...
    virtual VeriExpression *    GetIndexExpr() const       { return 0 ; }  // Get the index expression of only a VeriIndexedName. This is also a test if this is a selected name.
    virtual Array *             GetIndexes() const         { return 0 ; }  // Get array of VeriExpression. The index expressions of only a VeriIndexedMemoryId.
    virtual const char *        GetSuffix() const          { return 0 ; }  // Get the unresolved suffix name (a const char*) of only a VeriSelectedName. This is also a test if this is a selected name.
    virtual unsigned            GetFunctionType() const    { return 0 ; }  // Get a VERI (function) token of the function of a system call (VeriSystemCall), or method of a VeriSelectedName. 0 if not known
    virtual unsigned            IsGlobalClockingFunction() const { return 0 ; }  // Return true if it is a global clocking function
    virtual unsigned            GetToken() const           { return 0 ; }  // Can be one of VERI_INTERFACE, VERI_THIS, VERI_SUPER, VERI_DOLLAR_ROOT or VERI_DOLLAR_UNIT for veriKeyword
    virtual unsigned            GetEdgeToken() const       { return 0 ; } // VERI_POSEDGE,VERI_NEGEDGE or 0(for both edges),defined for VeriEventExpression
    virtual VeriExpression *    GetIffCondition() const    { return 0 ; }  // A level condition under which the edge is tested. Only set for SV.

    // Return the port direction for a port expression
    virtual unsigned            PortExpressionDir() const ;
    virtual unsigned            IsConstExpr() const        { return 0 ; }

    // For ranges and binary operators :
    virtual unsigned            GetPartSelectToken() const { return 0 ; }  // Part-select token for ranges.
    virtual unsigned            OperType() const           { return 0 ; }  // Type of operator in this node (returned as VERI token). For ranges return the separator token (VERI_COLON by default for a ":").
    virtual VeriExpression *    GetLeft() const            { return 0 ; }  // Left side of a binary operator (or range).
    virtual VeriExpression *    GetRight() const           { return 0 ; }  // Right side of a binary operator (or range).
    virtual VeriExpression *    GetArg() const             { return 0 ; }  // Argument of a unary operator.
    virtual unsigned            IsParenthesized() const    { return 0 ; }  // flag that tells of (binary/unary) operator was parenthesized when parsing.
    virtual void                SetParenthesized()         { }             // Set flag that tells of operator was parenthesized when parsing.
    // for rare cases of parse tree manipulation : change the argument of a unary or binary operator or range
    virtual void                SetArg(VeriExpression * /*expr*/)  { }
    virtual void                SetLeft(VeriExpression * /*expr*/)  { }
    virtual void                SetRight(VeriExpression * /*expr*/)  { }
    virtual void                SetPrefix(VeriName * /*prefix*/)  { }
    virtual VeriName *          ResetPrefix()          { return 0 ; } // Return immediate prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's.
    virtual void                ResetPrefixId() {}

    // operand of unary operator
    virtual unsigned            IsInlineConstraint() const { return 0 ; }

    // Routines to obtain full type information from expressions
    virtual unsigned            PackedDimension() const    { return 0 ; }  // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const  { return 0 ; }  // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const    { return 0 ; }  // Return the base type (built-in type/struct/union) looking through named types
    virtual VeriDataType *      GetBaseType() const        { return 0 ; }  // Returns base type of enum literals
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) = 0 ; // Check the type of expression
    virtual unsigned            IsSingleValueRange() const { return 0 ; } // Implemented for Verirange
    virtual unsigned            HasDollar() const          { return 0 ; } // Implemented for VeriDollar, VeriRange and VeriBinaryOperator

    virtual VeriIdDef *         GetActualIdForFormal(const char* /*formal_name*/, unsigned /*is_vhdl*/) const { return 0 ; } // Get the actual id with the name of the formal
    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const = 0 ; // Pure virtual function to copy any expression
    virtual VeriExpression *    CopyBaseExpression(VeriMapForCopy &id_map_table) const { return CopyExpression(id_map_table) ; }

    // Specific parse-tree manipulation routines.
    // VIPER #6432 : Add default argument to these ReplaceChildXXX Apis, so that caller can determine
    // whether they want to delete old parse tree or not. By default, old parse tree is delete, but
    // if ReplaceChildXXX is called passing 0 as third argument, old parse tree will not be deleted.
    unsigned                    ReplaceChildExpr(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node = 1) { return ReplaceChildExprInternal(old_expr, new_expr, delete_old_node) ; } // Delete old expression, and put new expression
    unsigned                    ReplaceChildName(VeriName *old_name, VeriName *new_name, unsigned delete_old_node=1) { return ReplaceChildNameInternal(old_name, new_name, delete_old_node) ; } // Delete old name, and puts new name
    unsigned                    ReplaceChildDataType(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node=1) { return ReplaceChildDataTypeInternal(old_data, new_data, delete_old_node) ; } // Delete 'old_data' and puts new data type in its place
    unsigned                    ReplaceChildRange(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node=1) { return ReplaceChildRangeInternal(old_range, new_range, delete_old_node) ; } // Delete 'old_range' if it is next range, and puts new range as new next range
    unsigned                    ReplaceChildModuleItem(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node=1) { return ReplaceChildModuleItemInternal(old_item, new_item, delete_old_node) ; } // Delete 'old_item' specific data declaration from struct/union, and puts new declaration in its place
    unsigned                    ReplaceChildStmt(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node=1) { return ReplaceChildStmtInternal(old_stmt, new_stmt, delete_old_node) ; } // Delete 'old_stmt' specific statement, and puts new statement in its place

    // VIPER #7424 : Added argument 'unsigned expect_nonconst'. If these routines
    // are called on expression with the new argument as '1', routine will return 0
    // without producing any error for non-constant expression and will return
    // evaluated value/expression for constant expression.
    VeriBaseValue *             StaticEvaluate(int context_size, ValueTable *n, VeriConstraint *target_constraint = 0, unsigned expect_nonconst = 0) const ;
    VeriExpression *            StaticEvaluateToExpr(int context_size, ValueTable *n, unsigned expect_nonconst = 0) const ; // Evaluate constant expression and part of data type, returns expression

    virtual int                 GetMsbOfRange() const                       { return 0 ; }  // Return MSB of the single range
    virtual int                 GetLsbOfRange() const                       { return 0 ; }  // Return LSB of the single range
    virtual unsigned            GetWidth(int * /*msb*/, int * /*lsb*/, ValueTable * /*table*/) const     { return 0 ; } // Width of a range only.
    virtual verific_uint64      GetWidth(verific_int64 * /*msb*/, verific_int64 * /*lsb*/, ValueTable * /*table*/) const     { return 0 ; } // Viper #7711: Width of a range only, in 64 bits
    virtual VeriBaseValue *     StaticEvaluateConcatElement(int context_size, ValueTable *val_table, VeriConstraint * target_constraint, unsigned expect_nonconst) const ; // VIPER #7902
    virtual unsigned            StaticReplaceConstantExprForConcatElement(unsigned is_const_index_context) ; // VIPER #7902

    // VIPER #6396: Pass 'ValueTable' to the following routine to evaluate expr containing genvar
    VeriConstraint *            EvaluateConstraint(ValueTable *tab = 0, VeriDataFlow *df = 0) { return EvaluateConstraintInternal(tab, df) ; } // VIPER #8345: Add a second argument 'df'

    // Attribute methods
    // _attributes moved to VeriNode static map. No of nodes with attribute should be small
    // compared to total number of nodes. It does not make sense from memory perspective to
    // keep a member on each object for such sparse data
    // VeriExpression             *GetAttribute(const char *attr_name) const ; // get the attribute expression for this attribute (by name) on this node
    // Map *                       GetAttributes() const                   { return _attributes ; }
    // void                        SetAttributes(Map *attr)                { VERIFIC_ASSERT(!_attributes) ; _attributes = attr ; }
    // void                        AddAttributes(Map *attr) ; // Absorb the Map (char*->VeriConst) of attribute tree nodes to the (attributed) expression.
    // void                        PrettyPrintAttributes(ostream &f, unsigned level) const ;

    // Comment preservation (only active if compile flag is set)
    // Moved to VeriNode for effective memory usage
    // void                        AddComments(Array *arr) ;
    // Array *                     GetComments() const ;

    // find the VERI-token for a system function (char*) name. Return if system function is not know. Full list in tokens.h.
    static unsigned             GetFuncToken(const char *name, const VeriExpression *from=0) ; // VIPER #8138: Added 'from' to check if the function token can be applied in the context of the given expression
    static void                 ResetNameToTokens() ;
    static unsigned             IsSystemTaskFunction(unsigned token) ; // VIPER #8138: Check if the given function token is a system task/function
    static unsigned             IsBuiltInMethod(unsigned token) ; // VIPER #8138: Check if the given function token is a built-in method
    static unsigned             GetFuncTokenOfClass(const char *func_name) ; // Return tokens for built-in methods that can be declared within class

    unsigned                    IsConstRange() const ;
    virtual unsigned            IsHierarchicalName() const  { return 0 ; } // VIPER #7581: check for constant hiername
// instantiation to a logic type variable.
    virtual Array *             GetArgs() const             { return 0 ; } // Defined for functions VeriNew
    virtual Array *             SetArgs(Array * /*args*/)   { return 0 ; }          // Set args to zero
    virtual Array *             GetDecls() const            { return 0 ; } // Defined for VeriStructUnion
    virtual VeriExpression *    GetSizeExpr() const         { return 0 ; } // Defined for functions VeriNew

    virtual VeriExpression *    GetTargetType() const        { return 0 ; } // Get the type of the assignment pattern expression/cast/multi-assignment pattern

    virtual unsigned            SetTargetType(VeriExpression * /*new_type*/) { return 0 ; /* virtual catcher: failed */ } // VIPER #7936: Set the type of the assignment pattern expression/cast/multi-assignment pattern

    virtual VeriExpression *    GetSvaClkExpr() const       { return 0 ; } // The resolved clock expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    VeriExpression();

    VeriExpression(const VeriExpression &veri_expr, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriExpression(SaveRestore &save_restore) ;

protected: // Abstract class
    VeriExpression(VeriExpression &veri_expr, unsigned use_original_linefile):VeriTreeNode(veri_expr, use_original_linefile) {}

public:
    virtual VeriExpression *GetNameExtendedExpression(char * /*name*/) { VERIFIC_ASSERT(0) ; return 0 ; }
    virtual ~VeriExpression();

private:
    // Prevent compiler from defining the following
    VeriExpression(const VeriExpression &) ;            // Purposely leave unimplemented
    VeriExpression& operator=(const VeriExpression &) ; // Purposely leave unimplemented

public:
    // Find out the size of the expression.
    virtual unsigned            FindSize()                { return 0 ; } // Default catcher
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                PrettyPrintWithSemi(ostream &f, unsigned level) const ; // Called from within VeriConstraintDecl's PrettyPrint routine // VIPER #2074
    virtual void                VarUsageMethod(VeriVarUsageInfo* /*info*/, unsigned /*action*/, Array * /*arguments*/) { } // Only for (method) selected names : resolve as a function call with (VeriExpression) arguments.
    virtual void                ResolveOnehot(VeriScope *scope, veri_environment environment) ; // Resolve an expression that is the plain RHS or LHS of an assignment or comparison. This could be a valid onehot expression (IdRef or constant). Otherwise, same as Resolve().
    virtual void                ResolveAnsiPortDecl(VeriScope *scope, unsigned *previous_direction, unsigned *previous_kind, unsigned default_kind, unsigned *previous_type, unsigned default_type) ; // Resolve AnsiPortDecl (needs previous type/direction). If not an ansi port decl, resolve normal with environment PORT_EXPRESSION. // VIPER #5949
    virtual void                ResolveLabel(VeriScope *scope, veri_environment environment, VeriExpression *target) ; // Resolves labels of the concat items of assignment patterns
    virtual void                ResolveArrayMethod(VeriScope *scope, veri_environment environment) ; // Resolve array methods
    virtual VeriName *          GetTypeName() const             { return 0 ; } // Return VeriName if this is a named type reference.
    virtual void                LinkOnehot(VeriIdDef *onehot_var) ;        // Discard onehot variable if expression is not appropriate for onehot compare/assignment.
    virtual void                SetEventWithAt()           { }
    virtual void                SetName(char * /*name*/)   { } // Change reference name (for simple names only). Absorbs the char*.
    virtual void                SetId(VeriIdDef * /*id*/)  { } // Change reference id (for simple names only)
    virtual void                SetQualifier(unsigned /*token*/) { } // Set qualifier for ANSI port lists (similar to ModuleItem)
    virtual void                SetAsLocal()    { } // port(s) declared as 'local'
    virtual void                SetFunctionType(unsigned /*f*/) { }
    virtual void                SetPartSelectToken(unsigned /*token*/)  { }  // VIPER #6895
    virtual void                SetConstraintBlock(Array * /*blk*/) {}
    virtual void                SetIdentifierList(Array * /*refs*/) {} // VIPER #6756: List of identifiers for inline-constraints
    virtual void                SetScope(VeriScope *)     {}
    void                        CheckAssignmentContext(VeriExpression *value, veri_type_env environment) ; // Check type in assignment like context
    void                        CheckAssignment(VeriExpression *value, unsigned context) ; // Check type for blocking/non-blocking/continuous assignment
    unsigned                    CheckAssociation(VeriIdDef *formal, veri_type_env context) ; // Check type for formal/actual association
    unsigned                    IsEqualLiteral(const VeriExpression *other) const ; // VIPER #8251: Check for equal literal value/types
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    void                        CheckAssociationVhdl(const VhdlIdDef *formal, veri_type_env context) ; // Check type for formal/actual association
#endif
    VeriTypeInfo *              CreateTypeWithoutError() ; // Routine to create type for an expression without producing any error(VIPER #4740)
    virtual void                CheckIntegralArgument(VeriExpression * /*expr*/, veri_type_env /*environment*/) { } // VIPER #5887: Check for the type of the argument and initial value of parameter type argument
    // Viper 5959 Check for bit/part select inside Concat
    virtual unsigned            ContainsPartBitSelect() const  { return 0 ; }
    virtual unsigned            HasInterfaceInstOrPort() const ;

    // Collect id's declared in a port expression :
    virtual void                AccumulatePorts(Array &ports) const ;      // Accumulate VeriIdDef's from ANSI port decls and full-id port definitions.

    // Declare the implicit net with the given type if that is not declared and return the declared id (VeriVariable):
    virtual VeriIdDef *         DeclareImplicitNet(VeriScope * /* scope */, unsigned /* net_type */) const { return 0 ; } // virtual catcher

    // Internal routine to get the range width (check if bounds are constants)
    virtual unsigned            RangeWidth()               { return 0 ; } // Implemented for VeriRange

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    virtual void                CollectAllIds(Set *ids) ;            // Collect all ids in ids
    virtual void                MarkInterfaceTypePort(const VeriScope *scope) ; // VIPER #3672 : Identify interface type non-ansi port and set dir
    virtual void                ResolveNewCall(VeriIdDef * /*func_id*/) { } // Defined for VeriNew
    virtual unsigned            IsCopyConstructor() const   { return 0 ; } // Defined for VeriNew
    // Internal routines :
    virtual unsigned            ReplaceChildExprInternal(VeriExpression * /*old_expr*/, VeriExpression * /*new_expr*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old expression, and put new expression
    virtual unsigned            ReplaceChildNameInternal(VeriName * /*old_name*/, VeriName * /*new_name*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old name, and puts new name
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType * /*old_data*/, VeriDataType * /*new_data*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_data' and puts new data type in its place
    virtual unsigned            ReplaceChildRangeInternal(VeriRange * /*old_range*/, VeriRange * /*new_range*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_range' if it is next range, and puts new range as new next range
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem * /*old_item*/, VeriModuleItem * /*new_item*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_item' specific data declaration from struct/union, and puts new declaration in its place
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement * /*old_stmt*/, VeriStatement * /*new_stmt*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_stmt' specific statement, and puts new statement in its place
    // Static Elaboration
    virtual char *              StaticImage(VeriScope * /*scope*/, const char * /*prefix*/)     { return 0 ; }
    virtual void                ResolveHierId(HierIdWithDeclScope * /*hier_id*/, VeriPseudoTreeNode * /*start_node*/, Map * /*top_nodes*/, Map * /*mod_map_tab*/, unsigned /*is_sup_error*/, unsigned /*depth*/) {}
    void                        ResolveName(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *start_node, Map *top_nodes, Map *mod_map_tab, unsigned depth) ;
    virtual unsigned            Bind(HierIdWithDeclScope * /*hier_id*/, VeriPseudoTreeNode * /*defined_node*/, unsigned /*depth*/, const Map * /*top_nodes*/, const Map * /*mod_map_tab*/, unsigned /*donot_modify*/) { return 0 ; }
    // Internal routines to evaluate expressions with target constraint :
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const = 0;
    // VIPER #6041 : Add another argument VeriIdDef to this routine :
    virtual VeriExpression *    StaticEvaluateToExprInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual VeriBaseValue *     StaticEvaluateUnpackedArrayConcat(VeriConstraint * /*target_constraint*/, ValueTable * /*val_table*/, unsigned /*expect_nonconst*/) const { return 0 ; }

    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode * /*module_scope*/, unsigned is_overwritten) ;
    void                        CheckVisibilityOfType(const VeriIdDef *param_id,const VeriIdDef *type_id, const VeriPseudoTreeNode *module_node) const ; // VIPER #7601
    virtual unsigned            StaticAssign(ValueTable *table, VeriBaseValue *value, unsigned expect_nonconst) ;
    //virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const { return 0 ; } // Virtual routine to get size/sign of any expression node, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const                { } //Virtual routine to check for zero or negative size of any array

    virtual Array *             SplitTerm(unsigned /*master_width*/, const VeriIdDef * /*formal_id*/, unsigned /*instance_count*/, VeriPseudoTreeNode * /*node*/, Map * /*top_nodes*/, Map * /*mod_map_tab*/, unsigned * /*select_in_up_dim*/) { return 0 ; }
    char *                      IndexToString(ValueTable *value_table) const ; // retruns evaluated index expression in char* format

    virtual VeriScope *         GetDefinedScope(const VeriPseudoTreeNode * /*module_node*/, unsigned /*consider_node_for_dt*/)               { return 0 ; } // Get data type defined scope (Works for data types only).

    virtual void                StaticReplaceConstantExpr(unsigned /*is_const_index_context*/) {}; // Replace range bounds' default value and bit/part/slice expressions
    virtual unsigned            NonAnsiPortDir() const ; // VIPER #3066 : Routine to get direction of non-ansi style port
    // Internal routine for Viper #5615 :
    virtual VeriExpression *    ReplaceArrayInstanceReference(VeriNetRegAssign *assign, VeriModuleItem *instantiated_module, VeriPseudoTreeNode *node, Map *mod_map_tab, Map *top_mod_nodes, VeriNetAlias *net_alias) ;
    virtual void                StaticEvaluateByChoice(VeriBaseValue *value, ValueTable *val_table, VeriConstraint *constraint, unsigned expect_nonconst, unsigned &cannot_evaluate, unsigned check_validity, Set *done) ;
    virtual VeriDataType *      StaticElaborateClassType(VeriScope * /*instance_scope*/, Array * /*container_items*/, VeriModuleItem * /*from_decl*/, Map * /*mod_map_tab*/) { return 0 ; }
    virtual VeriName *          StaticElaborateClassName(VeriScope * /*instance_scope*/, Array * /*container_items*/, VeriModuleItem * /*from_decl*/, Map * /*mod_map_tab*/, VeriMapForCopy * /*old2new*/, unsigned /*replace_typeid*/) { return 0 ; }
    //virtual VeriBaseValue *     EvaluateParamArrayExpr(int context_size, ValueTable *n, VeriConstraint *target_constraint) const ; // Evaluate value of parameter array

    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    VeriBaseValue *             ApplyContextOnValue(VeriBaseValue *val, const VeriIdDef *id, verific_int64 context_size, ValueTable *n) const ;
    unsigned                    ContainsNotVisibleEnum() const ; // VIPER #7905
    virtual VeriBaseValue *     StaticEvaluateForAssignmentPattern(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst, unsigned check_validity) const ;
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ; // VIPER #8345: Add a second argument 'df'
    void                        CheckArraySize(VeriExpression* value) ;
    // APIs for constraint checking :
    void                        CheckAssignmentContextConstraint(VeriExpression *value, veri_type_env env) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;
    virtual unsigned            IsSizedPackedExpr() ; // VIPER #8384

    void                        CheckConstRangeArraySize(VeriExpression* value) ;
    //void                        CheckUnpackedArraySize(const VeriIdDef* formal) const ;
    //void                        CheckPackedArraySize(const VeriIdDef *formal) const ;
    void                        CheckUnpackedArrayAssignment(const VeriExpression *against) const ;
    // Verific internal routine to check if it can be treated as assignment pattern: virtual catcher
    virtual unsigned            CanBeAssignmentPattern(const VeriTypeInfo * /*context*/) const { return 0 ; }
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlExpression *    ConvertExpr() const  { return 0 ; }
    virtual VhdlRange *         ConvertRange() const { return 0 ; }
    virtual VhdlName *          ConvertName() const  { return 0 ; }
#endif

    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type
protected:
    // static table of known system functions and methods :
    static Map *                GetNameToTokens()       { return _name_to_token ; } // Get a static Map of char*->VERI_TOKEN for known system function calls
    static Map *_name_to_token ;   // a static Map of char*->VERI_TOKEN for known System Function calls
//CARBON_BEGIN
 public:
    // a public method that allows for release of this memory from an external call
    static void ReleaseNameToTokensData() { delete _name_to_token; _name_to_token = NULL; }
//CARBON_END

protected:
    // _attributes moved to VeriNode static map. No of nodes with attribute should be small
    // compared to total number of nodes. It does not make sense from memory perspective to
    // keep a member on each object for such sparse data
    // Map        *_attributes ;  // Attributes on this VeriExpression :  char* -> VeriExpression* mapping
    // _comment_arr moved to VeriNode static map for memory efficiency
    // Array      *_comment_arr ; // Comments attached to this VeriExpression node
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriExpression

/* -------------------------------------------------------------- */

/* ********** Name Reference **************** */

/*
   Abstract class: base class for all named classes (IdRef, IndexedId, IndexedMemoryId, and later HierId (selected name).
*/
class VFC_DLL_PORT VeriName : public VeriExpression
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VeriClassIds.h

// Default to VeriExpression functions :
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) = 0; // Resolve a name that is the prefix of a IndexedNamed or SelectedName
//    virtual void ResolveOnehot(VeriScope *scope, unsigned environment) ; // Resolve an expression that is the plain RHS or LHS of an assignment or comparison. This could be a valid onehot expression (IdRef or constant). Otherwise, same as Resolve().
//    virtual void LinkOnehot(VeriIdDef *onehot_var) ; // Discard onehot variable if expression is not appropriate for onehot compare/assignment.
    virtual void                ResolveScopeName(VeriScope *scope, veri_environment environment) ; // Resolve scope name i.e class_name::name

//    virtual unsigned PortExpressionDir() const ;    // Return the port direction for a ('95 style) port expression

    // Virtual accessor functions.
    virtual VeriName *          NameCast() const       { return const_cast<VeriName*>(this) ; }  // Dynamic Cast to a VeriName class (returns non-zero only if this expression is a VeriName).
    virtual VeriIdDef *         FullId() const         { return 0 ; } // For full identifier references only (not indexed) : return pointer to the reference iddef. 0 for all other nodes.
    virtual const char *        GetName() const = 0 ;                 // Get a char* name for the closest name reference in this VeriName. Suffix name for hierarchical name, prefix name of indexed name.
    virtual VeriIdDef *         GetId() const = 0 ;                   // Get resolved identifier in this name (the id for VeriIdRef, the suffix id for a hierarchical name(SelectedName), or prefix of indexed name (IndexedId))
    virtual void                SetName(char* name) = 0 ;             // Change reference name
    virtual void                SetId(VeriIdDef *id) = 0 ;            // Change reference id
    virtual void                SetPrefixId(VeriIdDef * /*id*/) {}
    virtual VeriName *          GetPrefix() const      { return 0 ; } // Return immediate prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's.
    virtual VeriName *          ResetPrefix()          { return 0 ; } // Return immediate prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's.
    virtual void                SetPrefix(VeriName * /*prefix*/)  { } // Set immediate prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's.
    virtual VeriExpression *    GetIndexExpr() const   { return 0 ; } // Get the index expression of only a VeriIndexedName. This is also a test if this is a selected name.
    virtual const char *        GetSuffix() const      { return 0 ; } // Get the unresolved suffix name (a const char*) of only a VeriSelectedName. This is also a test if this is a selected name.
    virtual unsigned            GetToken() const       { return 0 ; } // Get the token for a VeriKeyword node, can be one of VERI_INTERFACE, VERI_THIS or VERI_SUPER

    virtual unsigned            IsRange() const        { return 0 ; } // See if this expression represents a range
    virtual unsigned            IsHierName() const     { return 0 ; } // Returns '1' if 'this' is a hierarchical name reference (contains a VeriSelectedName)
    // Late tree addition
    virtual VeriName *          SetParamValues(Array *values) ; // Add argument specific parameter overwrite list to VeriScopeName
    virtual void                SetModportName(VeriName *s) ;
    virtual VeriName *          GetModportName() const  { return 0 ; } // Return modport name
    virtual Array *             GetParamValues() const  { return 0 ; }

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const { return CopyName(id_map_table) ; } // Copy name reference and returns VeriExpression
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const = 0 ; // Copy name reference and returns VeriName

    virtual Array *             GetArgs() const             { return 0 ; } // Defined for functions
    virtual Array *             SetArgs(Array * /*args*/)   { return 0 ; }          // Set args to zero
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    VeriName() ;

    VeriName(const VeriName &name, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriName(SaveRestore &save_restore) ;

    VeriName(VeriName &veri_name, unsigned use_original_linefile):VeriExpression(veri_name, use_original_linefile) {}

public:
    virtual ~VeriName() ;

private:
    // Prevent compiler from defining the following
    VeriName(const VeriName &) ;            // Purposely leave unimplemented
    VeriName& operator=(const VeriName &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual VeriTypeInfo *      CreateTypeForMethods(Array * /*arguments*/, unsigned /*is_task*/, veri_type_env /*environment*/) { return 0 ; }
    VeriTypeInfo *              CreateTypeForMethodsInt(VeriName *prefix, unsigned function_type, const Array *arguments, unsigned is_task, veri_type_env environment) const ;
    virtual void                VarUsageMethodInternal(VeriVarUsageInfo* info, unsigned action, VeriName *prefix, unsigned function_type, Array *arguments) ; // Only for (method) selected names : resolve as a function call with (VeriExpression) arguments.
    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const = 0; // Pure virtual routine to evaluate name reference
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst) = 0;

    virtual VeriName *          GetModifiedPrefix(unsigned /*merge_cnt*/, VeriIdDef * /*prefix_id*/)       { return 0 ; }

    // Create partial tree for configurations
    virtual VeriPseudoTreeNode *CreateTreeNode(VeriPseudoTreeNode * /* node */, const char * /* suffix */, VeriUseClause * /* use_clause */, Array * /* liblist */) const { return 0 ; } // Virtual catcher
    unsigned                    StaticAssignIndexed(Array *indexes, VeriBaseValue *prefix_value, ValueTable *table, VeriBaseValue *val, VeriConstraint *prefix_constraint, unsigned expect_nonconst) const ;
    VeriBaseValue*              StaticEvaluateIndexed(VeriBaseValue *prefix_val, VeriConstraint *constraint, Array *indexes, ValueTable *table, unsigned expect_nonconst) const ;
    virtual VeriName *          StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned /*replace_typeid*/) ;
    virtual VeriDataType *      StaticElaborateInterfaceBasedType(VeriScope *instance_scope, Map *mod_map_tab, unsigned is_typedef) ; // VIPER #7508
    virtual void                SetSuffixIsToken()          { } // VIPER #2917 & #4961: Defined on VeriSelectedName
    // VIPER #6550: For renamin selected names of new generated module item during explicit state m/c generation
    virtual VeriName *          GetMergedPrefix(unsigned /*seq_blk_count*/, VeriIdDef* /*prefix_id*/) { return 0 ; }
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlName *    ConvertName() const { return 0 ; }
#endif

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriIdRef : public VeriName
{
public:
    explicit VeriIdRef(char *name) ; // unresolved reference (absorbs 'name').
    explicit VeriIdRef(VeriIdDef *id) ; // resolved reference

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIIDREF; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    // Routines to obtain full type information from expressions
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ;   // Return the base type (built-in type/struct/union) looking through named types
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsIdRef() const      { return 1 ; }    // Check if this name is a simple id reference
    virtual unsigned            IsHierName() const      { return 0 ; } // Check if this name is a hierarchical name
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression
    virtual unsigned            IsDollar() const ;  // Check if this expression is a dollar literal/sequence-property formal with '$' used as actual.
    virtual unsigned            IsEdge(unsigned edge_type) const ;         // Check if actual associated with this the expression is preceded by a POSEDGE/NEGEDGE edge type, or (by default) any edge type.

    virtual unsigned            PortExpressionDir() const ; // Return the port direction for a port expression

    virtual VeriDataType *      GetDataType() const ; // Return type of an expression. Currently returns only for identifier references

    // Virtual accessor functions
    virtual VeriIdDef *         FullId() const          { return _id ; }      // For VeriIdRef expression nodes : return pointer to the reference iddef. 0 for all other nodes.
    virtual const char *        GetName() const ;      // Get unresolved name or name of resolved id
    virtual VeriIdDef *         GetId() const           { return _id ; } // Get resolved referenced identifier
    virtual void                SetName(char* name) ;  // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id

    // Parse tree copy routines
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy identifier reference and returns VeriName*
    VeriIdRef *                 Copy(VeriMapForCopy &id_map_table) const ; // Non virtual copy routine to get duplicate VeriIdRef*

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriIdRef(const VeriIdRef &id_ref, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriIdRef(SaveRestore &save_restore) ;

    virtual ~VeriIdRef() ;
    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriIdRef>(this, name) ; }

protected:
    VeriIdRef(VeriIdRef &base, unsigned use_original_linefile):VeriName(base, use_original_linefile),_name(base._name),_id(base._id) {base._name=0 ;base._id=0 ;}
private:
    // Prevent compiler from defining the following
    VeriIdRef() ;                             // Purposely leave unimplemented
    VeriIdRef(const VeriIdRef &) ;            // Purposely leave unimplemented
    VeriIdRef& operator=(const VeriIdRef &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ResolveOnehot(VeriScope *scope, veri_environment environment) ; // Resolve an expression that is the plain RHS or LHS of an assignment or comparison. This could be a valid onehot expression (IdRef or constant). Otherwise, same as Resolve().
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) ; // Resolve a name that is the prefix of a IndexedNamed or SelectedName
    virtual void                ResolveScopeName(VeriScope *scope, veri_environment environment) ; // Resolve scope name i.e class_name::name
    virtual void                ResolveLabel(VeriScope *scope, veri_environment environment, VeriExpression *target) ; // Resolves id-ref labels of the concat items of assignment patterns
    virtual void                MarkInterfaceTypePort(const VeriScope *scope) ; // VIPER #3672 : Identify interface type non-ansi port and set dir
    virtual void                LinkOnehot(VeriIdDef *onehot_var) ; // Discard onehot variable if expression is not appropriate for onehot compare/assignment.
    virtual void                VarUsageMethod(VeriVarUsageInfo* info, unsigned action, Array *arguments) ; // Only for (method) selected names : resolve as a function call with (VeriExpression) arguments.
    virtual void                AccumulatePorts(Array &ports) const ; // Accumulate VeriIdDef's from ANSI port decls and full-id port definitions.
    // Declare the implicit net with the given type if that is not declared and return the declared id:
    virtual VeriIdDef *         DeclareImplicitNet(VeriScope *scope, unsigned net_type) const ;

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            FindSize() ;
#endif
    // Late tree addition
    virtual VeriName *          SetParamValues(Array *values) ; // Add argument specific parameter overwrite list to VeriScopeName
    virtual VeriTypeInfo *      CreateTypeForMethods(Array *arguments, unsigned is_task, veri_type_env environment) ;

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ; // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size/sign of the expression. For signed expression returns negative of expression size , Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const                { } //Virtual routine to check for zero or negative size of any array

    // Internal routines :
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst) ;
    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                ResolveHierId(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *start_node, Map *top_nodes, Map *mod_map_tab, unsigned is_sup_error, unsigned depth) ;
    virtual unsigned            Bind(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *defined_node, unsigned depth, const Map *top_nodes, const Map *mod_map_tab, unsigned donot_modify) ;
    virtual VeriName *          GetModifiedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual VeriScope *         GetDefinedScope(const VeriPseudoTreeNode *module_node, unsigned consider_node_for_dt) ;

    virtual VeriPseudoTreeNode *CreateTreeNode(VeriPseudoTreeNode *node, const char *suffix, VeriUseClause *use_clause, Array *liblist) const ;
    virtual VeriName *          StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned replace_typeid) ;
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    // VIPER #6550: For renamin selected names of new generated module item during explicit state m/c generation
    virtual VeriName *          GetMergedPrefix(unsigned seq_blk_count, VeriIdDef *prefix_id) ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlName *    ConvertName() const ;
#endif
private:
    // Use before declaration check: VIPER #4379, 5609, 2775, 3121, 5712, 6151, 6599, 3503, 5604
    unsigned                    UsedBeforeDeclare(const VeriScope *scope, veri_environment environment) const ;

protected:
#ifdef VERILOG_SHARE_STRINGS
    const char  *_name ; // Unresolved prefix name
#else
    char        *_name ; // Unresolved prefix name
#endif
    VeriIdDef   *_id ;   // The resolved identifier definition that is referred here
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriIdRef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriIndexedId : public VeriName
{
public:
    VeriIndexedId(char *name, VeriExpression *index) ; // unresolved reference (absorbs 'name').
    VeriIndexedId(VeriIdDef *id, VeriExpression *index) ; // resolved reference
    VeriIndexedId(VeriName *prefix, VeriExpression *index) ; // new constructor

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIINDEXEDID; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) ; // Resolve a name that is the prefix of a IndexedNamed or SelectedName

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsHierName() const      { return (_prefix) ? _prefix->IsHierName() : 0 ; } // Check if this name contains a hierarchical name (in the prefix)

    virtual unsigned            PortExpressionDir() const ; // Return the port direction for a port expression
    virtual VeriDataType *      GetDataType() const ; // Return type of an expression. Currently returns only for identifier references

    // Routines to obtain full type information from expressions
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ;   // Return the base type (built-in type/struct/union) looking through named types
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Accessor functions
    virtual VeriName *          GetPrefix() const       { return _prefix ; } // The prefix name
    virtual VeriExpression *    GetIndexExpr() const    { return _idx ; }  // Get index expression
    // Virtual accessors :
    virtual const char *        GetName() const         { return (_prefix) ? _prefix->GetName() : 0 ; } // Get prefix name in char form
    virtual VeriIdDef *         GetId() const           { return _id ; }   // Get resolved referenced (prefix) identifier.
    virtual void                SetName(char* name) ;  // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id
    void                        SetIdx(VeriExpression* idx) { _idx = idx ; }  // Change reference name

    // Parse tree copy routines :
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy index name

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriIndexedId(const VeriIndexedId &index_id, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriIndexedId(SaveRestore &save_restore) ;

    virtual ~VeriIndexedId() ;
    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriIndexedId>(this, name) ; }

protected:
    VeriIndexedId(VeriIndexedId &indexed_id, unsigned use_original_linefile):VeriName(indexed_id,use_original_linefile),_prefix(indexed_id._prefix),_idx(indexed_id._idx),_id(indexed_id._id) {indexed_id._prefix=0 ; indexed_id._idx=0 ;indexed_id._id=0 ;}

private:
    // Prevent compiler from defining the following
    VeriIndexedId() ;                                 // Purposely leave unimplemented
    VeriIndexedId(const VeriIndexedId &) ;            // Purposely leave unimplemented
    VeriIndexedId& operator=(const VeriIndexedId &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Declare the implicit net with the given type if that is not declared and return the declared id:
    virtual VeriIdDef *         DeclareImplicitNet(VeriScope *scope, unsigned net_type) const { return (_prefix) ? _prefix->DeclareImplicitNet(scope, net_type) : 0 ; }

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    virtual void                MarkInterfaceTypePort(const VeriScope *scope) ; // VIPER #3672 : Identify interface type non-ansi port and set dir
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            FindSize() ;
#endif

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete old index expression, and puts new index expression
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete old prefix name, and puts new prefix name

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // evaluate index expression
    virtual VeriExpression *    StaticEvaluateToExprInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst);
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size, sign. For signed expression returns (-size_of_expr), Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                ResolveHierId(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *start_node, Map *top_nodes, Map *mod_map_tab, unsigned is_sup_error, unsigned depth) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual unsigned            Bind(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *defined_node, unsigned depth, const Map *top_nodes, const Map *mod_map_tab, unsigned donot_modify) ;
    virtual VeriName *          GetModifiedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual VeriPseudoTreeNode *CreateTreeNode(VeriPseudoTreeNode *node, const char *suffix, VeriUseClause *use_clause, Array *liblist) const ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    // VIPER #6550: For renamin selected names of new generated module item during explicit state m/c generation
    virtual VeriName *          GetMergedPrefix(unsigned seq_blk_count, VeriIdDef *prefix_id) ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlName *    ConvertName() const ;
#endif

protected:
    VeriName        *_prefix ;  // prefix name
    VeriExpression  *_idx ;     // The index expression (VeriRange for part-select)
// Set at Resolve time :
    VeriIdDef       *_id ;      // resolved prefix identifier
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriIndexedId

/* -------------------------------------------------------------- */

/*
   A VeriSelectedId implements an (single dot separated) 'hierarchical' name  : "<name> . suffix"
   In Verilog (95/2001), a hierarchical name can be :
     (1) Any identifier, visible by selection (prefix is a hierarchical name itself)
         In this case, both prefix_id and suffix_id will be set during Resolve, unless
         the hierarchical name cannot be resolved during analysis
         (if the first id in the prefix is a module which is not yet analyzed)
   In System Verilog, a hierarchical name can be something else too :
     (2) A dot-operator on an object: prefix_id points to the (class) object
         and suffix_id points to the function (iddef) which is operating on the object.
     (3) A built-in function on an object: prefix_id points to the (class/array) object (if an identifier),
         and suffix_id is 0.
         The built-in function type (_func) is set to the function token ('left', 'right', 'len', 'bits' etc)
     (4) A struct/union field on an object: prefix_id points to the (structure) object (if an identifier),
         and suffix_id points to the (structure) member identifier.

   The prefix can be any VeriName : A IdRef, a IndexedId or a SelectedId.
   During 'Resolve', we set the prefix_id/suffix_id/_func fields, if the name
   can be resolved.

   Previously, selected name was implemented as a space-separated IdRef, but with the
   extended System Verilog syntax/semantics, a separate class is needed.
   We made all existing API routines (on a VeriName/VeriIdRef) backward compatible
   for hierarchical names.
*/
class VFC_DLL_PORT VeriSelectedName : public VeriName
{
public:
    VeriSelectedName(VeriName *prefix, char *suffix) ; // unresolved reference (absorbs 'suffix').

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERISELECTEDNAME; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
// Assume SelectedName cannot be used as onehot expression :
//    virtual void              ResolveOnehot(VeriScope *scope, unsigned environment) ; // Resolve an expression that is the plain RHS or LHS of an assignment or comparison. This could be a valid onehot expression (IdRef or constant). Otherwise, same as Resolve().
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) ; // Resolve a name that is the prefix of a IndexedNamed or SelectedName
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual void                VarUsageMethod(VeriVarUsageInfo* info, unsigned action, Array *arguments) ; // Only for (method) selected names : resolve as a function call with (VeriExpression) arguments.

    virtual unsigned            IsHierName() const ; // Check if this name is a hierarchical name. Selected name always is.

    virtual unsigned            PortExpressionDir() const ;    // Return the port direction for a port expression
    virtual VeriDataType *      GetDataType() const ; // Return type of an expression. Currently returns only for identifier references

    // Routines to obtain full type information from expressions
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ; // Return the base type (built-in type/struct/union) looking through named types
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Virtual accessor routines :
    virtual VeriIdDef *         FullId() const ;     // Get resolved id (if this selected name denotes an identifier)
    virtual const char *        GetName() const ;                                   // Get unreferenced name or name of referenced id
    virtual VeriIdDef *         GetId() const ;     // Get resolved referenced (suffix) identifier
    virtual void                SetName(char* name) ;                               // Change reference name
    virtual void                SetId(VeriIdDef *id) ;                              // Change reference id
    virtual void                SetPrefixId(VeriIdDef *id) ;
    virtual VeriName *          GetPrefix() const       { return _prefix ; }        // The prefix name
    virtual VeriName *          ResetPrefix()           { VeriName* pref = _prefix; _prefix = 0 ; return pref ; }        // The prefix name
    virtual void                ResetPrefixId() ;
    virtual void                SetPrefix(VeriName *prefix) ;
    virtual const char *        GetSuffix() const       { return _suffix ; }        // The suffix name (in char*)
    virtual unsigned            GetFunctionType() const { return _function_type ; } // Get token for (function) method (SV only). Alternative for if _suffix_id is not there.
    virtual void                SetFunctionType(unsigned f) { _function_type = f ; }

    // Parse tree copy routines
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy selected name

    void                        SetSuffixId(VeriIdDef *suffix_id) { _suffix_id = suffix_id ; }
    virtual void                SetSuffixIsToken()                { _suffix_is_token = 1 ; }
    virtual unsigned            GetSuffixIsToken()                { return _suffix_is_token ; }

    void                        SetCrossLangName()                { _cross_lang_name = 1 ; }
    unsigned                    IsCrossLangName() const           { return _cross_lang_name ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSelectedName(const VeriSelectedName &index_id, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSelectedName(SaveRestore &save_restore) ;
    virtual ~VeriSelectedName() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriSelectedName>(this, name) ; }

protected:
    VeriSelectedName(VeriSelectedName &selected_name, unsigned use_original_linefile):VeriName(selected_name,use_original_linefile),_prefix(selected_name._prefix),_suffix(selected_name._suffix),_prefix_id(selected_name._prefix_id),_suffix_id(selected_name._suffix_id),_function_type(selected_name._function_type),_suffix_is_token(selected_name._suffix_is_token),_cross_lang_name(selected_name._cross_lang_name) {selected_name._prefix=0 ;selected_name._suffix=0 ; selected_name._prefix_id=0; selected_name._suffix_id=0; }

private:
    // Prevent compiler from defining the following
    VeriSelectedName() ;                                    // Purposely leave unimplemented
    VeriSelectedName(const VeriSelectedName &) ;            // Purposely leave unimplemented
    VeriSelectedName& operator=(const VeriSelectedName &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ResolveArrayMethod(VeriScope *scope, veri_environment environment) ; // Resolve array methods
//    virtual void              LinkOnehot(VeriIdDef *onehot_var) ; // Discard onehot variable if expression is not appropriate for onehot compare/assignment.
    virtual VeriTypeInfo *      CreateTypeForMethods(Array *arguments, unsigned is_task, veri_type_env environment) ;
    virtual unsigned            IsHierarchicalName() const ; // VIPER #7581: check for constant hiername

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' and puts new prefix name in its place.

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ; // Evaluate constant expression
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst) ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size/sign. For signed expression returns negative of expression size, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                ResolveHierId(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *start_node, Map *top_nodes, Map *mod_map_tab, unsigned is_sup_error, unsigned depth) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual unsigned            Bind(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *defined_node, unsigned depth, const Map *top_nodes, const Map *mod_map_tab, unsigned donot_modify) ;
    virtual VeriName *          GetModifiedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual VeriPseudoTreeNode *CreateTreeNode(VeriPseudoTreeNode *node, const char *suffix, VeriUseClause *use_clause, Array *liblist) const ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      StaticElaborateInterfaceBasedType(VeriScope *instance_scope, Map *mod_map_tab, unsigned is_typedef) ; // VIPER #7508

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    // VIPER #6550: For renamin selected names of new generated module item during explicit state m/c generation
    virtual VeriName *          GetMergedPrefix(unsigned seq_blk_count, VeriIdDef *prefix_id) ;
    void                        ModifyPrefix(VeriName *new_prefix) ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlName *    ConvertName() const ;
#endif
protected:
    VeriName  *_prefix ;        // The prefix name.
#ifdef VERILOG_SHARE_STRINGS
    const char *_suffix ;        // Unresolved suffix name
#else
    char      *_suffix ;        // Unresolved suffix name
#endif
// Set during Resolve :
    VeriIdDef *_prefix_id ;     // The resolved identifier definition that is referred in the prefix (if an identifier)
    VeriIdDef *_suffix_id ;     // The resolved identifier definition that is referred here
    unsigned   _function_type:30 ;  // Token indicating a possible (built-in) method on the prefix. (identifies method in absense of a suffix_id). SV only.
    unsigned   _suffix_is_token:1 ; // VIPER #2917 & #4961: Flag to indicate that the suffix came from a lexer token.
    unsigned   _cross_lang_name:1 ; // VIPER #5624: Flag to indicate that this name contains cross language elements
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSelectedName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriIndexedMemoryId : public VeriName
{
    // Index-select or part-select of a memory (multiple dimensions)
public:
    VeriIndexedMemoryId(char *name, Array *indexes) ; // unresolved reference, absorbs 'name'
    VeriIndexedMemoryId(VeriIdDef *id, Array *indexes) ; // resolved reference
    VeriIndexedMemoryId(VeriName *prefix, Array *indexes) ; // new constructor

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIINDEXEDMEMORYID; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) ; // Resolve a name that is the prefix of a IndexedNamed or SelectedName

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsHierName() const      { return (_prefix) ? _prefix->IsHierName() : 0 ; } // Returns '1' if 'this' is hierarchical name reference

    virtual unsigned            PortExpressionDir() const ; // Return the port direction for a port expression
    virtual VeriDataType *      GetDataType() const ; // Return type of an expression. Currently returns only for identifier references

    // Routines to obtain full type information from expressions
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ; // Return the base type (built-in type/struct/union) looking through named types
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Accessor functions
    virtual VeriName *          GetPrefix() const       { return _prefix ; }  // The prefix name
    virtual Array *             GetIndexes() const      { return _indexes ; } // Get array of VeriExpression. The index expressions

    // Virtual accessor functions :
    virtual const char *        GetName() const         { return (_prefix) ? _prefix->GetName() : 0 ; } // Get prefix name
    virtual VeriIdDef *         GetId() const           { return _id ; } // Get resolved referenced (prefix) identifier
    virtual void                SetName(char* name) ;  // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id

    // Parse tree copy routines
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy the indexed memory identifier reference

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriIndexedMemoryId(const VeriIndexedMemoryId &index_mem_id, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriIndexedMemoryId(SaveRestore &save_restore) ;
    virtual ~VeriIndexedMemoryId() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriIndexedMemoryId>(this, name) ; }

protected:
    VeriIndexedMemoryId(VeriIndexedMemoryId &indexed_memory_id, unsigned use_original_linefile):VeriName(indexed_memory_id,use_original_linefile),_prefix(indexed_memory_id._prefix),_indexes(indexed_memory_id._indexes),_id(indexed_memory_id._id) {indexed_memory_id._prefix=0 ; indexed_memory_id._indexes=0 ;indexed_memory_id._id=0;}

private:
    // Prevent compiler from defining the following
    VeriIndexedMemoryId() ;                                       // Purposely leave unimplemented
    VeriIndexedMemoryId(const VeriIndexedMemoryId &) ;            // Purposely leave unimplemented
    VeriIndexedMemoryId& operator=(const VeriIndexedMemoryId &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Declare the implicit net with the given type if that is not declared and return the declared id:
    virtual VeriIdDef *         DeclareImplicitNet(VeriScope *scope, unsigned net_type) const { return (_prefix) ? _prefix->DeclareImplicitNet(scope, net_type) : 0 ; }

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    virtual void                MarkInterfaceTypePort(const VeriScope *scope) ; // VIPER #3672 : Identify interface type non-ansi port and set dir

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If 'old_expr' is any index expression, deletes 'old_expr and puts 'new_expr' in its place
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // VIPER #6440: Delete old prefix name, and puts new prefix name

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual VeriExpression *    StaticEvaluateToExprInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst) ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size/sign. For signed expression returns negative of expression size, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                ResolveHierId(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *start_node, Map *top_nodes, Map *mod_map_tab, unsigned is_sup_error, unsigned depth) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual unsigned            Bind(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *defined_node, unsigned depth, const Map *top_nodes, const Map *mod_map_tab, unsigned donot_modify) ;
    virtual VeriName *          GetModifiedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual VeriPseudoTreeNode *CreateTreeNode(VeriPseudoTreeNode *node, const char *suffix, VeriUseClause *use_clause, Array *liblist) const ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    // VIPER #6550: For renamin selected names of new generated module item during explicit state m/c generation
    virtual VeriName *          GetMergedPrefix(unsigned seq_blk_count, VeriIdDef *prefix_id) ;
protected:
    VeriName    *_prefix ;  // Unresolved prefix name
    Array       *_indexes ; // Array of VeriExpression. The index expressions.
    VeriIdDef   *_id ;      // Resolved prefix identifier
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriIndexedMemoryId

/* -------------------------------------------------------------- */
/*
   VeriIndexedExpr implements indexing on expressions (currently on concat/multi-concat)
   The IEEE 1800-2009 LRM Section 11.4.12 allows a concatenations to be indexed like
       byte a, b ;
       bit [1:0] c ;
       c = {a + b}[1:0]; // 2 lsb's of sum of a and b
   This class represents "{a + b}[1:0]" here.
   We should really have the prefix type as VeriConcat but since
   SV LRM may allow other type of expressions to be indexed in
   future, we now use any VeriExpression to be prefix.
   Note that we already have two types of indexed expressions
   VeriIndexedId and VeriIndexedMemoryId derived from VeriName.
   Those two are specialized classes and kept for compatibility.
*/
class VFC_DLL_PORT VeriIndexedExpr : public VeriExpression
{
public:
    VeriIndexedExpr(VeriExpression *prefix, VeriExpression *index) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIINDEXEDEXPR ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            PortExpressionDir() const { return (_prefix) ? _prefix->PortExpressionDir() : 0 ; }

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Accessor functions
    VeriExpression *            GetPrefixExpr() const   { return _prefix ; } // The prefix expression
    virtual VeriExpression *    GetIndexExpr() const    { return _idx ; }  // Get index expression

    // Parse tree copy routines:
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy index name

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriIndexedExpr(const VeriIndexedExpr &indexed_expr, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriIndexedExpr(SaveRestore &save_restore) ;

    virtual ~VeriIndexedExpr() ;
    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriIndexedExpr>(this, name) ; }

protected:
    VeriIndexedExpr(VeriIndexedExpr &indexed_expr, unsigned use_original_linefile) : VeriExpression(indexed_expr, use_original_linefile), _prefix(indexed_expr._prefix), _idx(indexed_expr._idx) { indexed_expr._prefix = 0 ; indexed_expr._idx = 0 ; }

private:
    // Prevent compiler from defining the following
    VeriIndexedExpr() ;                                   // Purposely leave unimplemented
    VeriIndexedExpr(const VeriIndexedExpr &) ;            // Purposely leave unimplemented
    VeriIndexedExpr& operator=(const VeriIndexedExpr &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                MarkInterfaceTypePort(const VeriScope *scope) { if (_prefix) _prefix->MarkInterfaceTypePort(scope) ; }

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete old index expression, and puts new index expression

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // evaluate index expression
    virtual VeriExpression *    StaticEvaluateToExprInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size, sign. For signed expression returns (-size_of_expr), Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    VeriExpression  *_prefix ;  // Prefix expression (only VeriConcat and VeriMultiConcat as of 10/2010)
    VeriExpression  *_idx ;     // The index expression (only VeriRange as of 10/2010)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriIndexedExpr

/* -------------------------------------------------------------- */

/*
   VeriScopeName implements (a simple '::' separated) class type i.e
   ps_class_identifier [ parameter_value_assignment]
     { :: class_identifier [parameter_value_assignment]}

   This class can also be used to represent package import item
     package_identifier :: identifier
   | package_identifier :: *
*/
class VFC_DLL_PORT VeriScopeName : public VeriSelectedName
{
public:
    VeriScopeName(VeriName *prefix, char *suffix, Array *param_value_assignment) ;  // Absorbs 'suffix'

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERISCOPENAME; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                ResolveScopeName(VeriScope *scope, veri_environment environment) ; // Resolve scope name i.e class_name::name
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned /*allow_range_index*/) { ResolveScopeName(scope, environment) ; } // Resolve a name that is the prefix of a IndexedNamed or SelectedName

    virtual unsigned            IsScopeName() const     { return 1 ; }  // Returns '1' if 'this' is a ('::'-separated) hierarchical name reference

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual void                VarUsage(VeriVarUsageInfo *info, unsigned action) ; // VIPER #5238
    virtual Array *             GetParamValues() const  { return _param_values ; }
    virtual VeriIdDef *         GetId() const ; // Get resolved referenced (suffix) identifier if _suffix exists
    virtual const char *        GetName() const         { return _suffix ? _suffix : (_prefix ? _prefix->GetName(): 0) ; }        // Get referenced name
    // Late tree addition
    virtual VeriName *          SetParamValues(Array *vals) ; // Add parameter overwrite list
    virtual void                SetModportName(VeriName *s) ;
    virtual VeriName *          GetModportName() const  { return _modport_name ; } // Return modport name
    virtual void                SetName(char* name) ;  // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id

    // Parse tree copy routines
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy scope name

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriScopeName(const VeriScopeName &scope_name, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriScopeName(SaveRestore &save_restore) ;

    virtual ~VeriScopeName() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriScopeName>(this, name) ; }

protected:
    VeriScopeName(VeriScopeName& scope_name, unsigned use_original_linefile):VeriSelectedName(scope_name,use_original_linefile),_param_values(scope_name._param_values),_modport_name(scope_name._modport_name) {scope_name._param_values=0 ;}

private:
    // Prevent compiler from defining the following
    VeriScopeName() ;                                 // Purposely leave unimplemented
    VeriScopeName(const VeriScopeName &) ;            // Purposely leave unimplemented
    VeriScopeName& operator=(const VeriScopeName &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If any concat element is 'old_expr, deletes 'old_expr' and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriName *          StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned replace_typeid) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    Array     *_param_values ;  // Parameter value assignment list (array of VeriExpression*)
    VeriName  *_modport_name ;  // SV-2009 feature : modport along with parameter overwrite #(3).modport_name (VIPER #5710)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriScopeName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriKeyword : public VeriIdRef
{
    // Interface keyword for generic interface modport ports and 'this' or 'super' keywords
public:
    explicit VeriKeyword(unsigned token) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const         { return ID_VERIKEYWORD ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment /* environment */) ;
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo *info, unsigned action) ;

    // Virtual accessor functions :
    virtual unsigned            GetToken() const           { return _token ; }  // Can be one of VERI_INTERFACE, VERI_THIS, VERI_SUPER, VERI_DOLLAR_ROOT or VERI_DOLLAR_UNIT or VERI_LOCAL

    // Parse tree copy routines
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy the indexed memory identifier reference

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriKeyword(const VeriKeyword &index_mem_id, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriKeyword(SaveRestore &save_restore) ;

    virtual ~VeriKeyword() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriKeyword>(this, name) ; }

protected:
    VeriKeyword(VeriKeyword& keyword, unsigned use_original_linefile):VeriIdRef(keyword,use_original_linefile),_token(keyword._token){}

private:
    // Prevent compiler from defining the following
    VeriKeyword() ;                               // Purposely leave unimplemented
    VeriKeyword(const VeriKeyword &) ;            // Purposely leave unimplemented
    VeriKeyword& operator=(const VeriKeyword &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) ;
    virtual void                ResolveScopeName(VeriScope *scope, veri_environment environment) ; // Resolve scope name $unit::name
    virtual void                ResolveOnehot(VeriScope *scope, veri_environment environment) { Resolve(scope, environment) ; } // VIPER #3823 : Overwrite this routine for keyword to avoid calling of VeriIdRef::ResolveOnehot
    // Return 0 from here so that we don't declare keywords like 'this', 'super' etc:
    virtual VeriIdDef *         DeclareImplicitNet(VeriScope * /* scope */, unsigned /* net_type */) const { return 0 ; }

protected:
    unsigned     _token ;   // Token: 'interface', 'this' or 'super' or local
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriKeyword

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriConcat : public VeriExpression
{
public:
    explicit VeriConcat(Array *exprs) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERICONCAT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            PortExpressionDir() const ;    // Return the port direction for a port expression

    virtual unsigned            IsConcat() const        { return 1 ; }  // Check if concatenation
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Accessor functions
    virtual Array *             GetExpressions() const  { return _exprs ; } // Get array of VeriExpressions
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy concat expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConcat(const VeriConcat &concat, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriConcat(SaveRestore &save_restore) ;

    virtual ~VeriConcat() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriConcat>(this, name) ; }

protected:
    VeriConcat(VeriConcat &veri_concat, unsigned use_original_linefile):VeriExpression(veri_concat, use_original_linefile),_exprs(veri_concat._exprs) {veri_concat._exprs=0 ;}

private:
    // Prevent compiler from defining the following
    VeriConcat() ;                              // Purposely leave unimplemented
    VeriConcat(const VeriConcat &) ;            // Purposely leave unimplemented
    VeriConcat& operator=(const VeriConcat &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    static VeriTypeInfo *       CheckConcatType(const Array *exprs, VeriTypeInfo *expected_type, veri_type_env environment) ; // Routine to check expression array
    VeriTypeInfo *              CheckUnpackedArrayConcat(VeriTypeInfo *expected_type, veri_type_env environment) const ;

    // Collect the ids in a set
    virtual void                CollectAllIds(Set *ids) ; // Viper 4829 : Routine to collect all ids in Set ids

    // Verific internal routine to check if it can be treated as assignment pattern:
    virtual unsigned            CanBeAssignmentPattern(const VeriTypeInfo *context) const ;

    // Viper 5959 Check for bit/part select inside Concat
    virtual unsigned            ContainsPartBitSelect() const ;
    virtual unsigned            HasInterfaceInstOrPort() const ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            FindSize() ;
#endif
    virtual void                MarkInterfaceTypePort(const VeriScope *scope) ; // VIPER #3672 : Identify interface type non-ansi port and set dir

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If any concat element is 'old_expr, deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst);
    static VeriBaseValue *      StaticEvaluateConcatExprArr(int context_size, const Array *exprs, ValueTable *n, unsigned expect_nonconst) ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. For signed expression returns negative of expression size, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            NonAnsiPortDir() const ; // VIPER #3066 : Routine to get direction of non-ansi style port
    // Internal routine for Viper #5615:
    virtual VeriExpression *    ReplaceArrayInstanceReference(VeriNetRegAssign *assign, VeriModuleItem *instantiated_module, VeriPseudoTreeNode *node, Map *mod_map_tab, Map *top_mod_nodes, VeriNetAlias *net_alias) ;
    //virtual VeriBaseValue *     EvaluateParamArrayExpr(int /*context_size*/, ValueTable *n, VeriConstraint * /*target_constraint*/) const ; // Evaluate value of parameter array
    virtual VeriBaseValue *     StaticEvaluateUnpackedArrayConcat(VeriConstraint *target_constraint, ValueTable *val_table, unsigned expect_nonconst) const ;
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;  // constraint checking
protected:
    Array *_exprs ; // Array of VeriExpression*
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConcat

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriMultiConcat : public VeriExpression
{
public:
    VeriMultiConcat(VeriExpression *repeat, Array *exprs) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIMULTICONCAT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsMultiConcat() const      { return 1 ; }        // Returns '1' if 'this' is a multi-concatenation
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Accessor functions
    virtual VeriExpression *    GetRepeat() const           { return _repeat ; } // Get repeat VeriExpression
    virtual Array *             GetExpressions() const      { return _exprs ; }  // Get array of VeriExpressions

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy multiconcatenation

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriMultiConcat(const VeriMultiConcat &multi_concat, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriMultiConcat(SaveRestore &save_restore) ;

    virtual ~VeriMultiConcat() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriMultiConcat>(this, name) ; }

protected:
    VeriMultiConcat(VeriMultiConcat &veri_mconcat, unsigned use_original_linefile):VeriExpression(veri_mconcat, use_original_linefile),_repeat(veri_mconcat._repeat),_exprs(veri_mconcat._exprs) {veri_mconcat._exprs=0 ; veri_mconcat._repeat=0 ;}

private:
    // Prevent compiler from defining the following
    VeriMultiConcat() ;                                   // Purposely leave unimplemented
    VeriMultiConcat(const VeriMultiConcat &) ;            // Purposely leave unimplemented
    VeriMultiConcat& operator=(const VeriMultiConcat &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Viper 5959 Check for bit/part select inside Concat
    virtual unsigned            ContainsPartBitSelect() const ;

    // Verific internal routine to check if it can be treated as assignment pattern:
    virtual unsigned            CanBeAssignmentPattern(const VeriTypeInfo *context) const ;
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If 'old_expr' is a child of multi-concat, delete 'old_expr' and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. For signed expression returns negative of expression size, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    //virtual VeriBaseValue *     EvaluateParamArrayExpr(int /*context_size*/, ValueTable *n, VeriConstraint * /*target_constraint*/) const ; // Evaluate value of parameter array
    virtual VeriBaseValue *     StaticEvaluateUnpackedArrayConcat(VeriConstraint *target_constraint, ValueTable *val_table, unsigned expect_nonconst) const ;
    VeriBaseValue *             StaticEvaluateBody(VeriBaseValue *c_val, int context_size, ValueTable *val_table, VeriConstraint * target_constraint, unsigned expect_nonconst) const ; // VIPER #7902
    virtual VeriBaseValue *     StaticEvaluateConcatElement(int context_size, ValueTable *val_table, VeriConstraint * target_constraint, unsigned expect_nonconst) const ; // VIPER #7902
    virtual unsigned            StaticReplaceConstantExprForConcatElement(unsigned is_const_index_context) ; // VIPER #7902
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;  // constraint checking
protected:
    VeriExpression  *_repeat ;
    Array           *_exprs ; // Array of VeriExpression
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriMultiConcat

/* -------------------------------------------------------------- */

/*
   VIPER 2178 : Derive this class from VeriName so that it can be used as prefix
   of hierarchical name.
*/
class VFC_DLL_PORT VeriFunctionCall : public VeriName
{
public:
// 6/2004 : Change in class : name reference changed from char* to VeriName field.
//    VeriFunctionCall(char *name, Array *args) ; // unresolved reference, absorb 'name'.
//    VeriFunctionCall(VeriIdDef *func, Array *args) ; // resolved reference
    VeriFunctionCall(VeriName *func_name, Array *args) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIFUNCTIONCALL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index) ; // Resolve a name that is the prefix of a IndexedNamed or SelectedName
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsFunctionCall() const      { return 1 ; }
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression: VIPER #7581

    // Local and virtual Accessor functions
    VeriName *                  GetFunctionName() const     { return _func_name ; }
    virtual unsigned            GetFunctionType() const     { return _function_type ; } // Get a VERI token of the function of the system call. 0 if not known
    virtual void                SetFunctionType(unsigned f) { _function_type = f ; }
    virtual Array *             GetArgs() const             { return _args ; } // Get array of VeriExpression*. Can contain VeriPortConnect. The function arguments
    virtual Array *             SetArgs(Array* args)        { Array *ret_args = _args ; _args = args ; return ret_args ; }          // Set args to zero
    virtual VeriName *          GetPrefix() const           { return _func_name ; }

    // back-ward compatible virtual accessor functions
    virtual const char *        GetName() const             { return (_func_name) ? _func_name->GetName() : 0 ; }  // Get unresolved name of the function call
    virtual VeriIdDef *         GetId() const               { return (_func_name) ? _func_name->GetId() : 0 ; }   // Get resolved function identifier
    virtual void                SetName(char* name) ;  // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id

    // Parse tree copy routines
    virtual VeriName *          CopyName(VeriMapForCopy &id_map_table) const; // Copy function call

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriFunctionCall(const VeriFunctionCall &func_call, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriFunctionCall(SaveRestore &save_restore) ;

    virtual ~VeriFunctionCall() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriFunctionCall>(this, name) ; }

protected:
    VeriFunctionCall(VeriFunctionCall &function_call, unsigned use_original_linefile):VeriName(function_call, use_original_linefile),_func_name(function_call._func_name), _args(function_call._args),_function_type(function_call._function_type) {function_call._func_name=0 ; function_call._args=0 ;}

private:
    // Prevent compiler from defining the following
    VeriFunctionCall() ;                                    // Purposely leave unimplemented
    VeriFunctionCall(const VeriFunctionCall &) ;            // Purposely leave unimplemented
    VeriFunctionCall& operator=(const VeriFunctionCall &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ResolveArrayMethod(VeriScope *scope, veri_environment environment) ; // Resolve array methods
    virtual unsigned            IsHierarchicalName() const ; // VIPER #7581: check for constant hiername

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific function call argument and puts new argument 'new_expr' in its place
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is the name of this function call and puts 'new_name' in its place.

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign, returns negative of expression size for signed expression, Viper #7711:return  in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticAssign(ValueTable * /*eval*/, VeriBaseValue * /*value*/, unsigned /*expect_nonconst*/) ;
    virtual void                ResolveHierId(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *start_node, Map *top_nodes, Map *mod_map_tab, unsigned is_sup_error, unsigned depth) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual unsigned            Bind(HierIdWithDeclScope *hier_id, VeriPseudoTreeNode *defined_node, unsigned depth, const Map *top_nodes, const Map *mod_map_tab, unsigned donot_modify) ;
    virtual VeriName *          GetModifiedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id) ;
    // Function call can be base class name with arguments :
    virtual VeriName *          StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned replace_typeid) ;

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

private:
    VeriBaseValue *             StaticEvaluateBuiltInMethods(ValueTable *val_table, unsigned expect_nonconst) const ;

protected:
    VeriName    *_func_name ;
    Array       *_args ;     // Array of VeriExpression*. Can contain VeriPortConnect. The function arguments.
    unsigned   _function_type ; // Token indicating a possible (built-in) method on the prefix. (identifies method in absense of a suffix_id). SV only.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriFunctionCall

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriSystemFunctionCall : public VeriExpression
{
public:
    VeriSystemFunctionCall(char *name, Array *args) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISYSTEMFUNCTIONCALL; } // Defined in VeriClassIds.h
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ; // VIPER 2490 : implemented for system function call, otherwise its argument specific ids will not be treated as used
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression: VIPER #7581
    virtual unsigned            IsEdge(unsigned edge_type) const ; // Note: Works only with RTL elaboration product for $global_clock and after VarUsage and if _global_clock_expr is set

    // Accessor functions
    virtual const char *        GetName() const             { return _name; }           // Get unresolved system function name
    virtual Array *             GetArgs() const             { return _args ; }          // Get array of VeriExpression*. Should not contain VeriPortConnect. The system function arguments
    virtual Array *             SetArgs(Array *args)        { Array *ret_args = _args ; _args = args ; return ret_args ; }          // Set args to zero
    virtual unsigned            GetFunctionType() const     { return _function_type ; } // Get a VERI token of the function of the system call. 0 if not known

    virtual unsigned            IsGlobalClockingFunction() const  ; // Return true if it is a global clocking function
    virtual VeriExpression *    GetSvaClkExpr() const       { return _sva_clock_expression ; } // The resolved clock expression
    virtual void                SetFunctionType(unsigned f) { _function_type = f ; }

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy system function call

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSystemFunctionCall(const VeriSystemFunctionCall &sys_func_call, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSystemFunctionCall(SaveRestore &save_restore) ;

    virtual ~VeriSystemFunctionCall() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriSystemFunctionCall>(this, name) ; }

protected:
    VeriSystemFunctionCall(VeriSystemFunctionCall &veri_sfc, unsigned use_original_linefile):VeriExpression(veri_sfc, use_original_linefile),_name(veri_sfc._name), _args(veri_sfc._args),
_sva_clock_expression(veri_sfc._sva_clock_expression),
_function_type(veri_sfc._function_type) {veri_sfc._name=0 ; veri_sfc._args=0 ;
veri_sfc._sva_clock_expression = 0 ;
}

private:
    // Prevent compiler from defining the following
    VeriSystemFunctionCall() ;                                          // Purposely leave unimplemented
    VeriSystemFunctionCall(const VeriSystemFunctionCall &) ;            // Purposely leave unimplemented
    VeriSystemFunctionCall& operator=(const VeriSystemFunctionCall &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                CheckIntegralArgument(VeriExpression *expr, veri_type_env environment) ; // VIPER #5887: Check for the type of the argument and initial value of parameter type argument

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific argument, and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /* context_size*/, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. For signed expression returns negative of expression size, Viper #7711:return  in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

    unsigned                    FunctionType() const { return _function_type ; }

protected:
    char        *_name ;          // Unresolved system function name.
    Array       *_args ;          // Array of VeriExpression*. Should not contain VeriPortConnect. The system function arguments.
    VeriExpression *_sva_clock_expression ; // Viper 6456: Store the sva clock expression
// Create at constructor time :
    unsigned     _function_type ; // A VERI token of the function of the system call. 0 if not known.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSystemFunctionCall

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriMinTypMaxExpr : public VeriExpression
{
public:
    VeriMinTypMaxExpr(VeriExpression *expr_min, VeriExpression *expr_typ, VeriExpression *expr_max) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIMINTYPMAXEXPR; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    virtual unsigned            IsMinTypMax() const     { return 1 ; }

    VeriExpression *            GetMinExpr() const      { return _min_expr ; }
    VeriExpression *            GetTypExpr() const      { return _typ_expr ; }
    VeriExpression *            GetMaxExpr() const      { return _max_expr ; }

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy mintypmax expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriMinTypMaxExpr(const VeriMinTypMaxExpr &min_typ_max, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriMinTypMaxExpr(SaveRestore &save_restore) ;

    virtual ~VeriMinTypMaxExpr() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriMinTypMaxExpr>(this, name) ; }

protected:
    VeriMinTypMaxExpr(VeriMinTypMaxExpr &veri_mtm, unsigned use_original_linefile):VeriExpression(veri_mtm, use_original_linefile),_min_expr(veri_mtm._min_expr),_typ_expr(veri_mtm._typ_expr),_max_expr(veri_mtm._max_expr) {veri_mtm._min_expr=0 ;veri_mtm._typ_expr=0 ; veri_mtm._max_expr=0 ;}

private:
    // Prevent compiler from defining the following
    VeriMinTypMaxExpr() ;                                     // Purposely leave unimplemented
    VeriMinTypMaxExpr(const VeriMinTypMaxExpr &) ;            // Purposely leave unimplemented
    VeriMinTypMaxExpr& operator=(const VeriMinTypMaxExpr &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is min/typ/max expression, and puts 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    VeriExpression *_min_expr ;
    VeriExpression *_typ_expr ;
    VeriExpression *_max_expr ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriMinTypMaxExpr

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriUnaryOperator : public VeriExpression
{
public:
    VeriUnaryOperator(unsigned oper_type, VeriExpression *arg) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIUNARYOPERATOR; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression
    virtual char *              Image() const ;     // VIPER #6594 (issue 131): Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    // Accessor functions
    virtual unsigned            OperType() const        { return _oper_type ; }
    virtual VeriExpression *    GetArg() const          { return _arg ; }
    virtual void                SetArg(VeriExpression *expr)  { _arg = expr ; }  // for rare cases of parse tree manipulation : change the argument.
    virtual unsigned            IsParenthesized() const { return _parenthesized ; }
    virtual void                SetParenthesized()      { _parenthesized = 1 ; }             // Set flag that tells of operator was parenthesized when parsing. Only used for SV sequence/property operators.

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy unary operator

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriUnaryOperator(const VeriUnaryOperator &unary_op, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriUnaryOperator(SaveRestore &save_restore) ;

    virtual ~VeriUnaryOperator() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriUnaryOperator>(this, name) ; }

protected:
    VeriUnaryOperator(VeriUnaryOperator &veri_uop, unsigned use_original_linefile):VeriExpression(veri_uop, use_original_linefile),_oper_type(veri_uop._oper_type),_parenthesized(veri_uop._parenthesized),_arg(veri_uop._arg) {veri_uop._arg=0 ;}

private:
    // Prevent compiler from defining the following
    VeriUnaryOperator() ;                                     // Purposely leave unimplemented
    VeriUnaryOperator(const VeriUnaryOperator &) ;            // Purposely leave unimplemented
    VeriUnaryOperator& operator=(const VeriUnaryOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is the operand, and puts 'new_expr' in its place as new operand

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    static VeriBaseValue *      StaticEvaluateUnary(const VeriBaseValue *oprnd, int context_size, unsigned oper_type) ;

public:
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign, returns negative of expression size for signed expression, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    unsigned         _oper_type:12 ; // VERI token
    unsigned         _parenthesized:1 ; // flag that this operator had parenthesis during parsing. Only set/used for SV sequence/property semantics.
    VeriExpression  *_arg ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriUnaryOperator

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriBinaryOperator : public VeriExpression
{
public:
    VeriBinaryOperator(unsigned oper_type, VeriExpression *left, VeriExpression *right) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIBINARYOPERATOR; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression
    virtual unsigned            HasDollar() const ; // Check both sides of the operator for dollar

    // Accessor methods
    virtual unsigned            OperType() const        { return _oper_type ; }
    virtual VeriExpression *    GetLeft() const         { return _left ; }
    virtual VeriExpression *    GetRight() const        { return _right ; }
    virtual unsigned            IsParenthesized() const { return _parenthesized ; }
    virtual void                SetParenthesized()      { _parenthesized = 1 ; }             // Set flag that tells of operator was parenthesized when parsing. Only used for SV sequence/property operators.
    // Parse-tree manipulation methods:
    virtual void                SetLeft(VeriExpression *expr)  { _left = expr ; } // for cases of parse tree manipulation : change the left operand
    virtual void                SetRight(VeriExpression *expr) { _right = expr ; } // for cases of parse tree manipulation : change the left operand

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy binary operator

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriBinaryOperator(const VeriBinaryOperator &bin_op, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriBinaryOperator(SaveRestore &save_restore) ;

    virtual ~VeriBinaryOperator() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriBinaryOperator>(this, name) ; }

protected:
    VeriBinaryOperator(VeriBinaryOperator &veri_bop, unsigned use_original_linefile):VeriExpression(veri_bop, use_original_linefile),_oper_type(veri_bop._oper_type),_parenthesized(veri_bop._parenthesized),_left(veri_bop._left),_right(veri_bop._right) {veri_bop._left=0 ;veri_bop._right=0 ;}

private:
    // Prevent compiler from defining the following
    VeriBinaryOperator() ;                                      // Purposely leave unimplemented
    VeriBinaryOperator(const VeriBinaryOperator &) ;            // Purposely leave unimplemented
    VeriBinaryOperator& operator=(const VeriBinaryOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                PrettyPrintWithSemi(ostream &f, unsigned level) const ;
    virtual unsigned            IsHierarchicalName() const ; // VIPER #7581: check for constant hiername
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr if it is one operand, and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. Returns negative of expression size for signed expression, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    static VeriBaseValue *      StaticEvaluateBinary(VeriBaseValue *lVal, VeriBaseValue *rVal, int context_size, unsigned oper_type) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    unsigned         _oper_type:12 ; // VERI token
    unsigned         _parenthesized:1 ; // flag that this operator had parenthesis during parsing. Only set/used for SV sequence/property semantics.
    VeriExpression  *_left ;
    VeriExpression  *_right ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBinaryOperator

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriQuestionColon : public VeriExpression
{
public:
    VeriQuestionColon(VeriExpression *if_expr, VeriExpression *then_expr, VeriExpression *else_expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIQUESTIONCOLON; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                ResolveOnehot(VeriScope *scope, veri_environment environment) ;
    virtual void                LinkOnehot(VeriIdDef *onehot_var) ; // Discard onehot variable if expression is not appropriate for onehot compare/assignment.

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsQuestionColon() const    { return 1 ; }  // Return 1 if this is a ?: operator

    // Accessor functions
    VeriExpression *            GetIfExpr() const       { return _if_expr ; }
    VeriExpression *            GetThenExpr() const     { return _then_expr ; }
    VeriExpression *            GetElseExpr() const     { return _else_expr ; }

    void                        SetIfExpr(VeriExpression *if_expr)          { _if_expr = if_expr ; }
    void                        SetThenExpr(VeriExpression *then_expr)      { _then_expr = then_expr ; }
    void                        SetElseExpr(VeriExpression *else_expr)      { _else_expr = else_expr ; }

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy question colon

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriQuestionColon(const VeriQuestionColon &ques_colon, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriQuestionColon(SaveRestore &save_restore) ;

    virtual ~VeriQuestionColon() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriQuestionColon>(this, name) ; }

protected:
    VeriQuestionColon(VeriQuestionColon &veri_qcolon, unsigned use_original_linefile):VeriExpression(veri_qcolon, use_original_linefile),_if_expr(veri_qcolon._if_expr),_then_expr(veri_qcolon._then_expr),_else_expr(veri_qcolon._else_expr) {veri_qcolon._if_expr=0 ;veri_qcolon._then_expr=0 ;veri_qcolon._else_expr=0 ;}

private:
    // Prevent compiler from defining the following
    VeriQuestionColon() ;                                     // Purposely leave unimplemented
    VeriQuestionColon(const VeriQuestionColon &) ;            // Purposely leave unimplemented
    VeriQuestionColon& operator=(const VeriQuestionColon &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it matches with if/then/else expression, and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const; // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. For signed expression returns negative of expression size, Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    VeriExpression *_if_expr ;
    VeriExpression *_then_expr ;
    VeriExpression *_else_expr ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriQuestionColon

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriEventExpression : public VeriExpression
{
public:
    VeriEventExpression(unsigned edge, VeriExpression *expr, VeriExpression *iff_condition=0 /* iff_condition is only active for SV*/) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIEVENTEXPRESSION; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    virtual unsigned            IsEdge(unsigned edge_type) const ; // Check if the expression is preceded by a POSEDGE/NEGEDGE edge type, or (by default) any edge type.

    // Verific internal routine: VIPER #5958: Event expression with a precesing '@' in non-context area:
    virtual unsigned            IsEventWithAt() const       { return _event_with_at ; }
    virtual void                SetEventWithAt()            { _event_with_at = 1 ; }

    virtual VeriIdDef *         FullId() const              { return (_expr) ? _expr->FullId() : 0 ; }     // For VeriIdRef expression nodes only.
    virtual const char *        GetName() const             { return (_expr) ? _expr->GetName() : 0 ; }
    virtual VeriName *          GetPrefix() const           { return (_expr) ? _expr->GetPrefix() : 0 ; }  // Return prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's, etc ...
    virtual unsigned            IsHierName() const          { return (_expr) ? _expr->IsHierName() : 0 ; } // Returns '1' if 'this' is a hierarchical name reference

    // Accessor functions
    virtual unsigned            GetEdgeToken() const        { return _edge ; }          // VERI_POSEDGE, VERI_NEGEDGE or 0 (for both edges).
    virtual VeriExpression *    GetExpr() const             { return _expr ; }          // Expression from which the edges are taken.
    virtual VeriExpression *    GetIffCondition() const     { return _iff_condition ; } // A level condition under which the edge is tested. Only set for SV.

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy event expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriEventExpression(const VeriEventExpression &event_expr, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriEventExpression(SaveRestore &save_restore) ;

    virtual ~VeriEventExpression() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriEventExpression>(this, name) ; }

protected:
    VeriEventExpression(VeriEventExpression &veri_event, unsigned use_original_linefile):VeriExpression(veri_event, use_original_linefile),_edge(veri_event._edge),_expr(veri_event._expr),_iff_condition(veri_event._iff_condition),_event_with_at(veri_event._event_with_at) {veri_event._expr=0 ;veri_event._iff_condition=0 ;}

private:
    // Prevent compiler from defining the following
    VeriEventExpression() ;                                       // Purposely leave unimplemented
    VeriEventExpression(const VeriEventExpression &) ;            // Purposely leave unimplemented
    VeriEventExpression& operator=(const VeriEventExpression &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ;  // Delete 'old_expr' if it is event expression or iff condition, and puts 'new_expr' in its place.

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned        _edge ;          // VERI token. 0 if no edge.
    VeriExpression *_expr ;          // The expression from which the edges are taken.
    VeriExpression *_iff_condition ; // A level condition under which the edge is tested. Only set for SV.
    unsigned        _event_with_at:1 ; // VIPER #5958: Event expression with a preceding '@' in non-context area
    //unsigned        _un_used_bit:31 ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriEventExpression

/* -------------------------------------------------------------- */

/*
   Class to create a named association of a (named) port to a port expression
   Used in module instantiations (ports and parameters and function/task arguments),
   and in '95 style 'named-port' module port lists,
*/
class VFC_DLL_PORT VeriPortConnect : public VeriExpression
{
public:
    VeriPortConnect(char *port_ref_name, VeriExpression *connection) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPORTCONNECT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Return the port direction for a port expression
    virtual unsigned            PortExpressionDir() const ;
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression

    // virtual Accessor functions
    virtual const char *        NamedFormal()   const       { return _port_ref_name ; } // Return the formal name (for a port-connect only)
    virtual VeriExpression *    GetConnection() const       { return _connection ; }    // Return the 'actual' expression, for a port-connect only

    // local Accessor function
    const char *                GetNamedFormal() const      { return _port_ref_name ; } // To be consistent with the accessor naming

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy port connect

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPortConnect(const VeriPortConnect &port_connect, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriPortConnect(SaveRestore &save_restore) ;

    virtual ~VeriPortConnect() ;

private:
    // Prevent compiler from defining the following
    VeriPortConnect() ;                                   // Purposely leave unimplemented
    VeriPortConnect(const VeriPortConnect &) ;            // Purposely leave unimplemented
    VeriPortConnect& operator=(const VeriPortConnect &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                MarkInterfaceTypePort(const VeriScope *scope) ; // VIPER #3672 : Identify interface type non-ansi port and set dir

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new connection at its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual VeriExpression *    StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
#ifdef VERILOG_SHARE_STRINGS
    const char      *_port_ref_name ;
#else
    char            *_port_ref_name ;
#endif
    VeriExpression  *_connection ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPortConnect

/* -------------------------------------------------------------- */

/*
   Class to indicate that a port association is 'open'
*/
class VFC_DLL_PORT VeriPortOpen : public VeriExpression
{
public:
    VeriPortOpen() ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIPORTOPEN; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsOpen() const          { return 1 ; }  // Check if this expression is an open port association.

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    virtual unsigned            PortExpressionDir() const ; // Return the port direction for a port expression
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy open port connection

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPortOpen(const VeriPortOpen &port_open, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriPortOpen(SaveRestore &save_restore) ;

    virtual ~VeriPortOpen() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriPortOpen>(this, name) ; }

protected:
    VeriPortOpen(VeriPortOpen &veri_popen, unsigned use_original_linefile):VeriExpression(veri_popen, use_original_linefile){}

private:
    // Prevent compiler from defining the following
    VeriPortOpen(const VeriPortOpen &) ;            // Purposely leave unimplemented
    VeriPortOpen& operator=(const VeriPortOpen &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ;

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPortOpen

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriAnsiPortDecl : public VeriExpression
{
    // Verilog 2000 ANSI Port Declarations
    // Since syntax mixes with Port Expressions,
    // it has to be a VeriExpression item.
public:
// old constructors : data type was spread over separate fields :
//    VeriAnsiPortDecl(unsigned dir, unsigned io_type, unsigned signed_type, VeriRange *range, Array *ids) ;
//    VeriAnsiPortDecl(unsigned dir, unsigned io_type, unsigned signed_type, VeriRange *range, VeriIdDef *id) ; // constructor with single id
// new constructors (which use data type instead of separate fields)
    VeriAnsiPortDecl(unsigned dir, VeriDataType *data_type, Array *ids) ;
    VeriAnsiPortDecl(unsigned dir, VeriDataType *data_type, VeriIdDef *id) ; // constructor with single id

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIANSIPORTDECL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                ResolveAnsiPortDecl(VeriScope *scope, unsigned *previous_direction, unsigned *previous_kind, unsigned default_kind, unsigned *previous_type, unsigned default_type) ; // VIPER #5949

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    virtual unsigned            IsAnsiPortDecl() const      { return 1 ; }  // Check if this is an ANSI (port) declaration.
    virtual VeriAnsiPortDecl *  AnsiPortDeclCast() const    { return const_cast<VeriAnsiPortDecl*>(this) ; }

    void                        AddPortDecl(VeriIdDef *id) ; // add another port to the declaration list.

    // Accessor functions
    unsigned                    GetDir() const              { return _dir ; }         // Get Port direction
    // Set Port direction - the direction is VERI_INTERFACE for generic interface type ports.
    // Static Elaboration changes it to proper interface so, need to clear it. If we create proper
    // data structutre for generic interface it will not be required any more.
    void                        SetDir(unsigned dir)        { _dir  = dir ; }
    VeriDataType *              GetDataType() const         { return _data_type ; }   // Get the data type. contains signing, type, and range
    virtual Array *             GetIds() const              { return _ids ; }         // Get array of VeriIdDef. The declared port identifiers
    // unsigned                    IsTaskFunctionPort() const  { return _is_task_function_port ; } // Check whether the port is declared in task or function
    // Qualifiers : AnsiPortList can only have one qualifier (VERI_VAR or VERI_VIRTUAL). So we can use a simplified form of qualifier setting.
    unsigned                    GetQualifier(unsigned token) const        { return (_qualifier==token) ? 1 : 0 ; } // Return VERI_VAR and VERI_VIRTUAL token.
    virtual void                SetQualifier(unsigned token)    { _qualifier = token ; }

    virtual void                SetAsLocal()    { _is_local = 1 ; }
    unsigned                    IsLocal() const { return _is_local ; }

    virtual unsigned            IsInterconnectType() const ; // GetType() == VERI_INTERCONNECT

    // Backward compatibility API routines :
//    unsigned GetIOType() const          { return _io_type ; }     // Get 0 for Verilog 95. For Verilog 2000 use type token : VERI_REG VERI_WIRE etc
//    unsigned GetSignedType() const      { return _signed_type ; } // Get 0 for Verilog 95, For Verilog 2000 use the VERI_SIGNED token here
//    VeriRange *GetRange() const         { return _range ; }       // Get range

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy ansi port declaration

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriAnsiPortDecl(const VeriAnsiPortDecl &ansi_port_decl, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriAnsiPortDecl(SaveRestore &save_restore) ;

    virtual ~VeriAnsiPortDecl() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriAnsiPortDecl>(this, name) ; }

protected:
    VeriAnsiPortDecl(VeriAnsiPortDecl &ansi_port_decl, unsigned use_original_linefile):VeriExpression(ansi_port_decl, use_original_linefile),_dir(ansi_port_decl._dir),
    _is_local(ansi_port_decl._is_local),
    _data_type(ansi_port_decl._data_type), _ids(ansi_port_decl._ids)
,_qualifier(ansi_port_decl._qualifier)
{ansi_port_decl._data_type=0 ; ansi_port_decl._ids=0 ;}

private:
    // Prevent compiler from defining the following
    VeriAnsiPortDecl() ;                                    // Purposely leave unimplemented
    VeriAnsiPortDecl(const VeriAnsiPortDecl &) ;            // Purposely leave unimplemented
    VeriAnsiPortDecl& operator=(const VeriAnsiPortDecl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Collect declared port ids :
    virtual void                AccumulatePorts(Array &ports) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node) ; // Delete 'old_data' if it is the data type of this decl, and puts new data type in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned      _dir:10 ;          // Port direction (VERI_INPUT etc) as a token.
    unsigned      _is_local:1 ;      // Flag to indicate if VERI_LOCAL is used for this declaration
// Old field, no longer needed:
//    unsigned      _is_task_function_port:1 ; // Port declared in function or task
    VeriDataType *_data_type ;       // The ports data type. Contains direction, signing, type, and range
// old fields, replaced by data_type:
//    unsigned    _io_type:10 ;      // 0 for Verilog 95. For Verilog 2000 use type token : VERI_REG VERI_WIRE etc.
//    unsigned    _signed_type:10 ;  // 0 for Verilog 95, For Verilog 2000 use the VERI_SIGNED token here.
//    VeriRange  *_range ;           // (packed) dimension(s).
    Array        *_ids ;             // Array of VeriIdDef. The declared port identifiers.
    unsigned      _qualifier ;       // Tokens VERI_VAR and VRI_VIRTUAL
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAnsiPortDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriTimingCheckEvent : public VeriExpression
{
public:
    explicit VeriTimingCheckEvent(VeriExpression *terminal_desc, unsigned edge = 0, VeriExpression *condition = 0, char *edge_descriptors = 0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERITIMINGCHECKEVENT; } // Defined in VeriClassIds.h
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor methods
    unsigned                    GetEdgeToken() const        { return _edge ; }
    char *                      GetEdgeDescStr() const      { return _edge_desc_str ; } // If defined, it will be either 2 or 4 bytes long.
    VeriExpression *            GetTerminalDesc() const     { return _terminal_desc ; }
    VeriExpression *            GetCondition() const        { return _condition ; }

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy timing check event

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTimingCheckEvent(const VeriTimingCheckEvent &time_check_event, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTimingCheckEvent(SaveRestore &save_restore) ;

    virtual ~VeriTimingCheckEvent() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTimingCheckEvent>(this, name) ; }

protected:
    VeriTimingCheckEvent(VeriTimingCheckEvent &timing_check, unsigned use_original_linefile):VeriExpression(timing_check, use_original_linefile),_edge(timing_check._edge),_edge_desc_str(timing_check._edge_desc_str),_terminal_desc(timing_check._terminal_desc),_condition(timing_check._condition) {timing_check._edge_desc_str=0 ;timing_check._terminal_desc=0 ;timing_check._condition=0 ;}

private:
    // Prevent compiler from defining the following
    VeriTimingCheckEvent() ;                                        // Purposely leave unimplemented
    VeriTimingCheckEvent(const VeriTimingCheckEvent &) ;            // Purposely leave unimplemented
    VeriTimingCheckEvent& operator=(const VeriTimingCheckEvent &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Declare the implicit net with the given type if that is not declared and return the declared id:
    virtual VeriIdDef *         DeclareImplicitNet(VeriScope *scope, unsigned net_type) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is the child node of this class, and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned        _edge ;             // posedge, negedge, edge (optional)
    char           *_edge_desc_str ;    // 01, 10, [zZxX][01], [01][zZxX] - "edge" control specifier string (optional)
    VeriExpression *_terminal_desc ;    // Terminal descriptor
    VeriExpression *_condition ;        // Timing check condition (optional)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTimingCheckEvent

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriRange : public VeriExpression
{
public:
    // Range has 'left' and 'right' expression, a 'type-of-range' token, and a 'next' pointer for multi-dimensional ranges.
    VeriRange(VeriExpression *left, VeriExpression *right, unsigned part_select_token=0, VeriRange *next=0);

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIRANGE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; // Resolve id references (done after analysis of a module)
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Late parse-tree additions : add a dimension to this range
    void                        SetNext(VeriRange *next) ; // essentially : add a new dimension to this range. Appends at the end of the list.
    void                        SetNextNull()                   { _next = 0 ; } // Method to make the next element of this linklist element null. FIX ME : merge into SetNext().
    virtual VeriRange *         RangeCast() const            { return const_cast<VeriRange*>(this) ; }  // Dynamic Cast to a VeriRange class (returns non-zero only if this expression is a range) .

    // Tests
    virtual unsigned            IsRange() const                 { return 1 ; }

    // Accessor methods
    virtual VeriExpression *    GetLeft() const                 { return _left ; }
    virtual VeriExpression *    GetRight() const                { return _right ; }
    virtual unsigned            GetPartSelectToken() const      { return _part_select_token; }
    virtual void                SetPartSelectToken(unsigned part_select_token)   { _part_select_token = part_select_token ; } // VIPER #6895
    VeriRange *                 GetNext() const                 { return _next ; }
    VeriExpression *            TakeLeft() ; // VIPER #8003: Take the left expression
    // Parse-tree manipulation methods:
    virtual void                SetLeft(VeriExpression *expr)  { _left = expr ; } // for cases of parse tree manipulation : change the left operand
    virtual void                SetRight(VeriExpression *expr) { _right = expr ; } // for cases of parse tree manipulation : change the left operand

    // Parse tree modification : Set bounds and next field null
    void                        SetFieldsNull() ;

    // Internal routine to get the range width (check if bounds are constants)
    virtual unsigned            RangeWidth() ;
    virtual unsigned            IsSingleValueRange() const ;
    virtual unsigned            HasDollar() const ; // Check both the sides of the range for $
    virtual char *              Image() const ;     // VIPER #7743 (issue 2): Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    // Check if this range is of dynamic size like: [] or [$] or [int]
    unsigned                    IsDynamicRange() const ;

    // Parse tree copy routines:
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const { return CopyRange(id_map_table) ; } // Copy range
    virtual VeriRange *         CopyRange(VeriMapForCopy &id_map_table) const; // Copy range

    virtual int                 GetMsbOfRange() const ; // Return MSB of the single range
    virtual int                 GetLsbOfRange() const ; // Return LSB of the single range
    virtual unsigned            GetWidth(int *msb, int *lsb, ValueTable *table) const ;
    virtual verific_uint64      GetWidth(verific_int64 * /*msb*/, verific_int64 * /*lsb*/, ValueTable * /*table*/)  const ; // Viper #7711: in 64 bits
    unsigned                    GetUnpackedWidth() const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriRange(const VeriRange &range, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriRange(SaveRestore &save_restore) ;

    virtual ~VeriRange();

private:
    // Prevent compiler from defining the following
    VeriRange() ;                             // Purposely leave unimplemented
    VeriRange(const VeriRange &) ;            // Purposely leave unimplemented
    VeriRange& operator=(const VeriRange &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            FindSize()                      { return RangeWidth() ; }
    virtual VhdlRange *         ConvertRange() const ;
#endif

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is lsb/msb, and puts 'new_expr' in its place as new lsb/msvb
    virtual unsigned            ReplaceChildRangeInternal(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node) ; // Delete 'old_range' if it is next range, and puts new range as new next range

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: return in 64 bits
    virtual void                CheckForZeroOrNegArrayWidth() const ;   //Virtual routine to check for zero or negative size of any array
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ;

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    unsigned                    CheckRangeBound(const VeriBaseValue *bound) const ; // VIPER #2879 : Routine to check illegal expression as range bounds

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected :

    VeriExpression *_left ;
    VeriExpression *_right ;
    unsigned        _part_select_token ; // 0 for plain range [left:right]. For Verilog 2000 : VERI_PARTSELECT_UP / DOWN. More choices for SV.
    VeriRange      *_next ; // points to next range in multidimensional range.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriRange

/* -------------------------------------------------------------- */

/*
   VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
*/
class VFC_DLL_PORT VeriPathPulseVal  : public VeriExpression
{
public:
    // DataType has packed dimensions, signing, base toke (token).
    VeriPathPulseVal(VeriExpression *reject_limit, VeriExpression *error_limit) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIPATHPULSEVAL ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    VeriExpression *            GetRejectLimit() const          { return _reject_limit ; }
    VeriExpression *            GetErrorLimit() const           { return _error_limit ; }

    // Parse tree copy :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPathPulseVal(const VeriPathPulseVal &path_pulse_val, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriPathPulseVal(SaveRestore &save_restore) ;

    virtual ~VeriPathPulseVal() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriPathPulseVal>(this, name) ; }

protected:
    VeriPathPulseVal(VeriPathPulseVal &path_pulse, unsigned use_original_linefile):VeriExpression(path_pulse, use_original_linefile),_reject_limit(path_pulse._reject_limit), _error_limit(path_pulse._error_limit){path_pulse._reject_limit=0 ;path_pulse._error_limit=0 ;}

private:
    // Prevent compiler from defining the following
    VeriPathPulseVal() ;                                    // Purposely leave unimplemented
    VeriPathPulseVal(const VeriPathPulseVal &) ;            // Purposely leave unimplemented
    VeriPathPulseVal& operator=(const VeriPathPulseVal &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is the child node of this class, and puts 'new_expr' in its place

    // Static Elaboration
    virtual VeriBaseValue*      StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // Probably not needed
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /* context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression of the reject value only
    virtual void                StaticReplaceConstantExpr(unsigned /*is_const_index_context*/) ; // Replace range bounds' default value and bit/part/slice expressions
    
protected :
    VeriExpression *_reject_limit ; // the reject-limit value for the PATHPULSE
    VeriExpression *_error_limit ; // the error-limit value for the PATHPULSE
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPathPulseVal
/* -------------------------------------------------------------- */

/*
   A Data Type (as defined in System Verilog LRM). Represents a reference to a type. (a Type Expression).
   This can be a simple data types reference (reg, integer etc), (for Verilog 95/2000)
   as well as named type reference (similar to a VeriIdRef) and more complex data types
   such as struct/union and enums (in System Verilog).
   A datatype should always include a field for 'packed' (array) dimensions and 'signing'.
   VeriDataType is derived from VeriExpression, since it can appear in type-assignments (for type-parameters in System Verilog)

   VeriDataType replaces the individual 'range', 'signing' and 'type' fields that used to be on
   the declaration module items (VeriRegDecl/NetDecl/ParamDecl etc).
*/
class VFC_DLL_PORT VeriDataType  : public VeriExpression
{
public:
    // DataType has packed dimensions, signing, base toke (token).
    VeriDataType(unsigned type, unsigned signing, VeriRange *dimensions);

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIDATATYPE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; // Resolve id references (done after analysis of a module)

    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual VeriTypeInfo *      CreateType(VeriIdDef * /*for_id*/) ;

    virtual unsigned            IsDataType() const              { return 1 ; }  // Returns '1' if 'this' is a type expression (System Verilog VeriDataType).
    virtual unsigned            IsNetDataType() const           { return 0 ; } // Return 1 for net data type i.e wire logic [2:0] x
    virtual VeriDataType *      DataTypeCast() const            { return const_cast<VeriDataType*>(this) ; }  // Dynamic Cast to a VeriDataType class (returns non-zero only if this expression is a data type).

    // Late parse-tree additions (set members, post-construction) :
    void                        SetDimensions(VeriRange *dimensions) ; // Set the dimensions as a parse tree member.
    virtual void                SetDecls(Array *decls) ; // Set the struct-union element decls as a parse tree member.
    virtual Array *             TakeDecls() ; // take out the struct-union element decls
    VeriRange *                 TakeDimensions() ; // take out the dimensions
    void                        SetSigning(unsigned signing)    { _signing = signing ; }
    void                        SetType(unsigned type)          { _type = type ; } // should not be needed..
    // Late qualifier addition
    virtual void                SetPacked()                     {} // VIPER 2473 : Store packed qualifier for structure/union
    virtual void                SetTagged()                     {} // Set tagged qualifier for union

    virtual unsigned            IsTagged() const                { return 0 ; } // Return 1 for tagged union
    virtual unsigned            IsEnumType() const              { return 0 ; } // True only for enum type. Don't go through type-refs
    virtual unsigned            IsUnionType() const ; // Return 1 for union type
    virtual unsigned            IsStructType() const ; // Return 1 for Struct type
    virtual unsigned            IsIntegerType() const ; // Return 1 for Integer type
    virtual unsigned            IsInterconnectType() const ; // GetType() == VERI_INTERCONNECT
    virtual unsigned            IsPacked() const                { return 0 ; } // Return 1 for packed data type or packed struct union type
    virtual unsigned            IsTypeRef() const               { return 0 ; } // Return 1 for VeriTypeRef only

    // Useful Tests, which work after Resolve (analysis) has been run.
    // For Verilog 2001, these routines are trivial, but for SV, they look through named types.
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ; // Return the base type (built-in type/struct/union) looking through named types
    virtual unsigned            BaseTypeSize() const ; // Return the bit size of base type (not considering packed dimension) VIPER #2838
    virtual unsigned            Type() const ;      // Return the base scaler type (or struct/enum) of this data type.
    virtual unsigned            Sign() const ;      // Return 1 for a signed data type, 0 for unsigned.
    virtual VeriExpression *    GetIndexExpr() const            { return _dimensions ; }  // Temporary : Get dimensions of data type (as in an expression(s)) (style of an indexed name)). This is to avoid another virtual accessor.
    virtual char *              Image() const ;     // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.
    virtual unsigned            NetType() const                 { return 0 ; } // Return only for VeriNetDataType
    virtual unsigned            IsNetType() const ; // VIPER #5949: Check for the net type of the data_type

    virtual VeriIdDef *         GetMember(const char * /*member_name*/) const { return 0 ; } // Return struct-member by name (if data type is a structure/union)
    virtual VeriScope *         GetMemberScope() const          { return 0 ; } // Scope of declared identifiers. Only for struct/union/typerefs in SV.
    virtual Array *             GetEnums() const                { return 0 ; } // Return the array of enumeration identifiers. Only for enums (and typerefs of enums) in SV
    virtual VeriRange *         GetDimensionAt(unsigned dimension) ;     // Return the range which is 'dimension' dimensions deep into this data type. GetDimensionAt(0) returns the MSB dimension.
    virtual VeriExpression *    GetArg() const                  { return 0 ; } // Return the data type or expression for the concerned VeriTypeOperator
    virtual unsigned            IsCircular(Set & /* defined_type_ids */) const { return 0 ; }     // Returns 1 if the datatype uses an id from the set to declare an item, like recursive structure/union or typedefs.
    virtual unsigned            IsUnpacked() const ; // VIPER 2473 : Test if data type is unpacked

    // Local Accessor methods
    virtual VeriRange *         GetDimensions()                 { return _dimensions ; } // List of dimensions. Replaces GetRange() on declaration items.
    virtual unsigned            GetType() const                 { return _type ; }       // A token (VERI_REG, VERI_WIRE, VERI_INTEGER, etc) Replaces GetParamType/GetNetType etc on delcaration items)
    virtual unsigned            GetSigning() const              { return _signing ; }    // A token (VERI_SIGNED/VERI_UNSIGNED) Replaced GetSignedType() on declaration items.
    virtual VeriDataType *      GetDataType() const             { return const_cast<VeriDataType*>(this) ; } // Return type of an expression. Currently returns only for identifier references
    virtual unsigned            IsVirtualInterface() const ;
    void                        SetIsVirtualInterface() ;
    virtual void                SetVirtualToken()                {}
    virtual VeriName *          GetDisciplineName() const        { return 0 ; } // Returns discipline name
    virtual unsigned            IsDisciplinedType() const        { return 0 ; }
    virtual Map *               GetElementIds() const            { return 0 ; } // Return the element name vs its id (for stuct union only)

    virtual VeriName*           GetClassName() ;
    virtual VeriModuleItem*     GetClassItem() ;

    // Parse tree copy :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const { return CopyDataType(id_map_table) ; } // Copy data type as expression
    virtual VeriDataType *      CopyDataType(VeriMapForCopy &id_map_table) const ; // Copy data type as data type
    virtual VeriDataType *      InlineDataType(VeriMapForCopy &id_map_table) const ; // return a uniquified copy, with all its internal elements (for structures) inlined.  Needed for VIPER #7277

    // DataType elaboration..

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDataType(const VeriDataType &data_type, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDataType(SaveRestore &save_restore) ;

    virtual ~VeriDataType();

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriDataType>(this, name) ; }

protected:
    VeriDataType(VeriDataType &datatype, unsigned use_original_linefile):VeriExpression(datatype, use_original_linefile),_type(datatype._type),_signing(datatype._signing),
    _is_virtual_interface(datatype._is_virtual_interface),
    _is_processing(datatype._is_processing), _dimensions(datatype._dimensions) {datatype._dimensions=0 ;}

private:
    // Prevent compiler from defining the following
    VeriDataType() ;                                // Purposely leave unimplemented
    VeriDataType(const VeriDataType &) ;            // Purposely leave unimplemented
    VeriDataType& operator=(const VeriDataType &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Convert verilog package to vhdl package
    virtual VhdlSubtypeIndication*   ConvertDataType(VhdlScope *vhdl_scope, VeriScope *veri_scope) ;
    virtual VhdlTypeDef*             ConvertToTypeDef(Array* /*decl_part*/, VhdlScope* /*vhdl_scope*/, VeriScope* /*veri_scope*/, VhdlTypeId* /*id*/) { return 0 ; } // For struct/enum/idref
#endif

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildRangeInternal(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node) ; // Delete 'old_range' if it is the dimension of this data type, and puts new range in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ; // Not sure if needed for type expression..
    virtual VeriExpression *    StaticEvaluateToExprInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds' default value and bit/part/slice expressions
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual VeriDataType *      StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab) ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      ReduceAndAddDimension(unsigned reduce_dim_by, VeriRange *to_add_range, VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type

    // VIPER #8332: API for the flag to check whether we are processing the type now:
    void                        SetIsProcessing()                { _is_processing = 1 ; }
    void                        ClearIsProcessing()              { _is_processing = 0 ; }
    unsigned                    IsProcessing() const             { return _is_processing ; }

protected :
    unsigned   _type:15 ;    // VERI token (VERI_REG, VERI_INTEGER, etc)
    unsigned   _signing:15 ; // VERI token (VERI_SIGNED, VERI_UNSIGNED)
    unsigned   _is_virtual_interface:1 ; // Flag to indicate virtual interface (VIPER #3786)
    unsigned   _is_processing:1 ; // VIPER #8332: Flag to indicate that the data type is being processed (required for enum initialization)
    VeriRange *_dimensions ; // Linked-list of ranges to indicate packed dimensions.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDataType

/* -------------------------------------------------------------- */
/*
   VIPER #2475 & 2648 : Net declaration with data type
   A VeriNetDataType represents data type of net in System Verilog
   Data type of net contains nettype like (wire, wand etc) and any 4-state data type
   Example -
   wire logic x ;
   For the above net declaration VeriNetDataType will contain 'wire logic'
*/
class VFC_DLL_PORT VeriNetDataType : public VeriDataType
{
public:
    VeriNetDataType(unsigned nettype, VeriDataType *data_type) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERINETDATATYPE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Useful API tests, valid after Resolve completed.
    virtual VeriTypeInfo *      CreateType(VeriIdDef *for_id) ;
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            BaseTypeSize() const ; // Return the bit size of base type (not considering packed dimension) VIPER #2838
    virtual unsigned            Type() const ;              // Return VERI_ENUM (for any enumeration type)
    virtual unsigned            IsNetType() const      { return 1 ; }
    virtual unsigned            IsUnionType() const ; // Return 1 for union type
    virtual unsigned            IsStructType() const ; // Return 1 for Struct type
    virtual unsigned            IsIntegerType() const ; // Return 1 for Integer type
    virtual unsigned            IsNetDataType() const   { return 1 ; } // Return 1 for net data type i.e wire logic [2:0] x
    virtual unsigned            IsUnpacked() const ;
    virtual unsigned            IsPacked() const ;  // Return 1 for packed type or packed struct union type
    virtual unsigned            GetType() const { return Type() ; } // A token (VERI_REG, VERI_WIRE, VERI_INTEGER, etc) Replaces GetParamType/GetNetType etc on delcaration items)
    virtual unsigned            Sign() const ;              // Return 1 for a signed data type, 0 for unsigned.

    virtual VeriRange *         GetDimensionAt(unsigned dimension) ;
    virtual VeriRange *         GetDimensions() ;     // List of dimensions. Replaces GetRange() on declaration items.
    virtual VeriIdDef *         GetMember(const char *member_name) const ; // return struct-member by name (if data type is a structure/union)
    virtual VeriScope *         GetMemberScope() const ; // scope of all declared identifiers. Only for struct/union/typerefs in SV.
    virtual Array *             GetEnums() const ;       // Return the array of enumeration identifiers. Only for enums (and typerefs of enums) in SV

    // Local Accessor methods :
    virtual VeriDataType *      GetDataType() const    { return _data_type ; }
    virtual unsigned            NetType() const        { return _type ; }

    // Parse tree copy :
    virtual VeriDataType *      CopyDataType(VeriMapForCopy &id_map_table) const ; // Copy nettype

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNetDataType(const VeriNetDataType &veri_enum, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriNetDataType(SaveRestore &save_restore) ;

    virtual ~VeriNetDataType() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriNetDataType>(this, name) ; }

protected:
    VeriNetDataType(VeriNetDataType &net_datatype, unsigned use_original_linefile):VeriDataType(net_datatype, use_original_linefile),_data_type(net_datatype._data_type) {net_datatype._data_type=0 ;}

private:
    // Prevent compiler from defining the following
    VeriNetDataType() ;                               // Purposely leave unimplemented
    VeriNetDataType(const VeriNetDataType &) ;            // Purposely leave unimplemented
    VeriNetDataType& operator=(const VeriNetDataType &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType *old_type, VeriDataType *new_type, unsigned delete_old_node) ; // Delete 'old_type' specific base type, and puts new type in its place

    // Static elaboration routines
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds' default value and bit/part/slice expressions
    virtual VeriScope *         GetDefinedScope(const VeriPseudoTreeNode *module_node, unsigned consider_node_for_dt) ; // VIPER #4734
    virtual VeriDataType *      StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab) ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      ReduceAndAddDimension(unsigned reduce_dim_by, VeriRange *to_add_range, VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type

protected:
    VeriDataType *_data_type ; // Data type associated with net
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNetDataType

/* -------------------------------------------------------------- */

/*
   A VeriTypeRef is like a VeriIdRef of a named type identifier. Includes (packed) 'dimensions' from VeriDataType.
*/
class VFC_DLL_PORT VeriTypeRef : public VeriDataType
{
public:
    VeriTypeRef(char *name, unsigned signing, VeriRange *dimensions) ; // unresolved reference (absorbs 'name').
    VeriTypeRef(VeriIdDef *id, unsigned signing, VeriRange *dimensions) ; // resolved reference
    VeriTypeRef(VeriName *type_name, unsigned signing, VeriRange *dimensions) ; // new constructor
    VeriTypeRef(unsigned virtual_keyword, VeriName *type_name) ; // new constructor for virtual interface data type

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERITYPEREF; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Useful API tests
    virtual VeriTypeInfo *      CreateType(VeriIdDef *for_id) ;
    virtual unsigned            PackedDimension() const ; // return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ; // Return the base type (built-in type/struct/union) looking through named types
    virtual unsigned            BaseTypeSize() const ; // Return the bit size of base type (not considering packed dimension) VIPER #2838
    virtual unsigned            Type() const ; // return the base scaler type (or struct/enum) of this data type.
    virtual unsigned            GetType() const          { return Type() ; }       // A token (VERI_REG, VERI_WIRE, VERI_INTEGER, etc) Replaces GetParamType/GetNetType etc on declaration items)
    virtual unsigned            IsNetType() const ; // VIPER #5949: Check for the net type of the data_type
    virtual unsigned            Sign() const ; // return 1 for a signed data type, 0 for unsigned.
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    virtual VeriIdDef *         GetMember(const char *member_name) const ; // return struct-member by name (if data type is a structure/union)
    virtual VeriScope *         GetMemberScope() const ; // scope of all declared identifiers. Only for struct/union/typerefs in SV.
    virtual Array *             GetEnums() const ;       // Return the array of enumeration identifiers. Only for enums (and typerefs of enums) in SV
    virtual VeriRange *         GetDimensionAt(unsigned dimension) ; // return the range which is 'dimension' dimensions deep into this data type. GetDimensionAt(0) returns the MSB dimension.
    virtual unsigned            IsCircular(Set &defined_type_ids) const ;  // Returns 1 if the datatype uses an id from the set to declare an item, like recursive structure/union or typedefs.
    virtual unsigned            IsUnpacked() const ; // VIPER 2473 : Test if data type is unpacked
    virtual unsigned            IsPacked() const ;  // Return 1 for packed data type or packed struct union type
    // Type-refs can have more packed dimensions, but we cannot return both of them. So, use the base class method:
    //virtual VeriRange *         GetDimensions()                 { return _dimensions ; } // List of dimensions. Replaces GetRange() on declaration items.

    // Virtual Accessor functions :
    virtual VeriName *          GetTypeName() const     { return _type_name ; } // the name (can be a hierarchical type name, or a simple identifier reference)
    virtual VeriIdDef *         GetId() const           { return _id ; }   // Get resolved referenced (prefix) identifier.
    // duplicated from indexed name, to get virtual function access :
    virtual const char *        GetName() const         { return (_type_name) ? _type_name->GetName() : 0 ; } // Get prefix name in char* form
    virtual VeriName *          GetPrefix() const       { return _type_name ; } // The prefix name (like an indexed name)
    virtual void                SetName(char *name)     { if (_type_name) _type_name->SetName(name) ; } // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id
    virtual unsigned            IsUnionType() const ; // Return 1 for union type
    virtual unsigned            IsStructType() const ; // Return 1 for Struct type
    virtual unsigned            IsIntegerType() const ; // Return 1 for Integer type
    virtual unsigned            IsTagged() const ;
    virtual unsigned            IsEnumType() const ;
    virtual unsigned            IsTypeRef() const       { return 1 ; }
    virtual unsigned            IsVirtualInterface() const ;
    virtual void                SetVirtualToken() ;
    virtual VeriName*           GetClassName() ;
    virtual VeriModuleItem*     GetClassItem() ;

    // Parse tree copy :
    virtual VeriDataType *      CopyDataType(VeriMapForCopy &id_map_table) const ; // Copy type reference
    virtual VeriDataType *      InlineDataType(VeriMapForCopy &id_map_table) const ; // return a uniquified copy, with all its internal elements (for structures) inlined.  Needed for VIPER #7277

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTypeRef(const VeriTypeRef &data_type,VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTypeRef(SaveRestore &save_restore) ;

    virtual ~VeriTypeRef() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTypeRef>(this, name) ; }

protected:
    VeriTypeRef(VeriTypeRef &veri_typeref, unsigned use_original_linefile):VeriDataType(veri_typeref,use_original_linefile), _type_name(veri_typeref._type_name),
    _virtual_token(veri_typeref._virtual_token),
    _id(veri_typeref._id) {veri_typeref._type_name=0 ;veri_typeref._id=0 ;}

private:
    // Prevent compiler from defining the following
    VeriTypeRef() ;                               // Purposely leave unimplemented
    VeriTypeRef(const VeriTypeRef &) ;            // Purposely leave unimplemented
    VeriTypeRef& operator=(const VeriTypeRef &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Convert verilog package to vhdl package
    virtual VhdlSubtypeIndication*   ConvertDataType(VhdlScope *vhdl_scope, VeriScope *veri_scope) ;
#endif

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is the type name, and puts new type name in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete old index expression, and puts new index expression

    // Static Elaboration
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ;
    virtual VeriScope *         GetDefinedScope(const VeriPseudoTreeNode *module_node, unsigned consider_node_for_dt) ;
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /* context_size*/, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual VeriDataType *      StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab) ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      ReduceAndAddDimension(unsigned reduce_dim_by, VeriRange *to_add_range, VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type
protected:
    VeriName    *_type_name ; // The name (can be a hierarchical type name, or a simple identifier reference)
    unsigned     _virtual_token ; // Store the keyword virtual
// Set during Resolve :
    VeriIdDef   *_id ;        // Resolved type identifier
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTypeRef

/* -------------------------------------------------------------- */

/*
   A VeriStructUnion is the data_type 'struct' or 'union' construct in System Verilog
   Inherits '(packed)dimensions', 'signing' and 'type' (VERI_STRUCT or VERI_UNION) from the VeriDataType class.
   On top of that, it has a scope, and an array of 'members' (VeriDataDecl's (VeriModuleItems)) with the member declarations.
   and optionally the VERI_PACKED token. We don't store the 'packed' token yet...We should once we start serious packed-type checking.
   We also collect the declared member identifiers in a members Array,
   so they can be accessed by order, or in Array form...
   Access of the members by name can also done by looking into the 'scope' of this data type.
*/
class VFC_DLL_PORT VeriStructUnion : public VeriDataType
{
public:
    VeriStructUnion(unsigned type/*VERI_STRUCT or VERI_UNION*/, unsigned signing, VeriRange *dimensions, Array *decls, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERISTRUCTUNION; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual void                VarUsage(VeriVarUsageInfo *info, unsigned action) ;

    // Late qualifier addition
    virtual void                SetPacked()             { _is_packed = 1 ; } // VIPER 2473 : Store packed qualifier
    virtual void                SetTagged()             { _is_tagged = 1 ; } // Set tagged qualifier

    virtual void                SetDecls(Array *decls) ; // Set the struct-union element decls as a parse tree member.
    virtual Array *             TakeDecls() ; // take out the struct-union element decls

    // Local Accessor methods :
    virtual Array *             GetDecls() const        { return _decls ; } // Array of VeriDataDecl (VeriModuleItem) which contains the member declarations.
    virtual VeriScope *         GetScope() const        { return _scope ; } // The local scope which contains the member identifiers (accessible by name).
    virtual Array *             GetIds() const          { return _ids ; }   // Get array of VeriIdDef. The declared struct/union members
    virtual Map *               GetElementIds() const   { return _ele_ids ; } // Return the element name vs its id (for stuct union only)

    // Useful API tests, valid after Resolve completed.
    virtual VeriTypeInfo *      CreateType(VeriIdDef * /*for_id*/) ;
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            BaseTypeSize() const ; // Return the bit size of base type (not considering packed dimension) VIPER #2838
    virtual unsigned            Type() const ;              // Return VERI_STRUCT/VERI_UNION (for any struct or union)
    virtual unsigned            Sign() const ;              // Return 1 for a signed data type, 0 for unsigned.

    virtual VeriIdDef *         GetMember(const char *member_name) const ;  // Return struct-member by name (if data type is a structure/union)
    virtual VeriScope *         GetMemberScope() const  { return _scope ; } // Scope of all declared identifiers. Only for struct/union/typerefs in SV.
    // virtual VeriRange *      GetDimensionAt(unsigned dimension) const ;  // Drop to VeriDataType::GetDimensionAt, since struct/union itself has no Range dimension.
    virtual unsigned            IsCircular(Set &defined_type_ids) const ;  // Returns 1 if the datatype uses an id from the set to declare an item, like recursive structure/union or typedefs.
    virtual unsigned            IsUnpacked() const ; // VIPER 2473 : Test if data type is unpacked
    virtual unsigned            IsPacked() const        { return _is_packed ; } // Return 1 if packed struct/union
    virtual unsigned            IsTagged() const        { return _is_tagged ; } // Return 1 for tagged union
    virtual unsigned            IsUnionType() const ; // Return 1 for union type
    virtual unsigned            IsStructType() const ; // Return 1 for Struct type
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    // Parse tree copy :
    virtual VeriDataType *      CopyDataType(VeriMapForCopy &id_map_table) const ; // Copy struct/union
    virtual VeriDataType *      InlineDataType(VeriMapForCopy &id_map_table) const ; // return a uniquified copy, with all its internal elements (for structures) inlined.  Needed for VIPER #7277

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriStructUnion(const VeriStructUnion &struct_union, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriStructUnion(SaveRestore &save_restore) ;

    virtual ~VeriStructUnion() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriStructUnion>(this, name) ; }

protected:
    VeriStructUnion(VeriStructUnion &str_union, unsigned use_original_linefile):VeriDataType(str_union,use_original_linefile),_decls(str_union._decls),_scope(str_union._scope),_ids(str_union._ids),_is_packed(str_union._is_packed),_is_tagged(str_union._is_tagged), _contain_typeparam_type_ele(str_union._contain_typeparam_type_ele),_ele_types(str_union._ele_types), _ele_ids(str_union._ele_ids) {str_union._decls=0 ;str_union._scope=0 ;str_union._ids=0 ; str_union._ele_types = 0 ; str_union._ele_ids = 0 ; }

private:
    // Prevent compiler from defining the following
    VeriStructUnion() ;                                   // Purposely leave unimplemented
    VeriStructUnion(const VeriStructUnion &) ;            // Purposely leave unimplemented
    VeriStructUnion& operator=(const VeriStructUnion &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Convert verilog package to vhdl package
    virtual VhdlSubtypeIndication*   ConvertDataType(VhdlScope* /*vhdl_scope*/, VeriScope* /*veri_scope*/) { return 0 ; }
    virtual VhdlTypeDef*             ConvertToTypeDef(Array *decl_part, VhdlScope *vhdl_scope, VeriScope *veri_scope, VhdlTypeId*id) ;
#endif

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' specific data declaration, and puts new declaration in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // probably not needed..
    virtual VeriScope *         GetDefinedScope(const VeriPseudoTreeNode * /*module_node*/, unsigned /*consider_node_for_dt*/) ;
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /* context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      ReduceAndAddDimension(unsigned reduce_dim_by, VeriRange *to_add_range, VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type

protected:
    Array     *_decls ; // Array of VeriDataDecl (VeriModuleItem) which contains the member declarations.
    VeriScope *_scope ; // The local scope which contains the members (accessible by name).
//
    Array     *_ids ;   // Array of VeriIdDefs, which are the declared enum constants.
    unsigned   _is_packed:1 ; // Flag to indicate packed qualifier (VIPER 2473)
    unsigned   _is_tagged:1 ; // Flag to indicate tagged qualifier
    // Members for internal usage :
    unsigned   _contain_typeparam_type_ele:1 ; // Flag to indicate if any element is type parameter type/unresolved named type
    Map       *_ele_types ; // Map of element id VeriIdDef* vs its VeriTypeInfo*(VIPER #6434)
    Map       *_ele_ids ;  // Map of element name in char* vs its VeriIdDef* (VIPER #6434)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriStructUnion

/* -------------------------------------------------------------- */

/*
   A VeriEnum is the data_type 'enum' construct in System Verilog
   VIPER 2336 : It no longer inherits 'signing', 'type' and 'dimensions' from the VeriDataType class. (If set in the 'enum' construct)
   It can have user defined type as base type. To support that its structure is changed.
   Now it will have a field VeriDataType* to store its base type.
   On top of that, it has an Array of VeriIdDef identifiers which are all enumeration constants.
   Enum does NOT have a scope, since the enum constants are declared in the scope the construct appears in.
   They are 'directly' visible (as opposed to 'struct' members which are visible by selection only.
   VIPER #5710: (vcs_compatibility-9000388573): Enum may have packed dimension, so add a
   new default argument for enum costructor.
*/
class VFC_DLL_PORT VeriEnum : public VeriDataType
{
public:
    VeriEnum(unsigned type, unsigned signing, VeriRange *dimensions, Array *ids, VeriRange *pkd_dim = 0) ;
    VeriEnum(VeriDataType *base_type, Array *ids, VeriRange *pkd_dim = 0) ; // new constructor (VIPER #2336)

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERIENUM; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Useful API tests, valid after Resolve completed.
    virtual VeriTypeInfo *      CreateType(VeriIdDef *for_id) ;
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            BaseTypeSize() const ; // Return the bit size of base type (not considering packed dimension) VIPER #2838
    virtual unsigned            Type() const ;              // Return VERI_ENUM (for any enumeration type)
    virtual unsigned            GetType() const { return Type() ; } // A token (VERI_REG, VERI_WIRE, VERI_INTEGER, etc) Replaces GetParamType/GetNetType etc on declaration items)
    virtual unsigned            Sign() const ;              // Return 1 for a signed data type, 0 for unsigned.

    virtual Array *             GetEnums() const                { return _ids ; } // Return the array of enumeration identifiers. Only for enums (and typerefs of enums) in SV
    virtual VeriRange *         GetDimensionAt(unsigned dimension) ;
    virtual VeriRange *         GetDimensions() ;     // List of dimensions. Replaces GetRange() on declaration items.

    // Local Accessor methods :
    virtual Array *             GetIds() const { return _ids ; } // Array of VeriIdDefs, which are the declared enum constants.
    virtual VeriDataType *      GetBaseType() const { return _base_type ; } // Returns base type of enum literals
    virtual unsigned            IsUnpacked() const ; // VIPER 2473 : Test if data type is unpacked
    virtual unsigned            IsEnumType() const   { return 1 ; } // True only for enum type. Don't go through type-refs
    virtual unsigned            IsIntegerType() const ; // Return 1 for Integer type
    virtual Map *               GetEnumLiterals() const  { return _enum_ids ; }

    // Parse tree copy :
    virtual VeriDataType *      CopyDataType(VeriMapForCopy &id_map_table) const ; // Copy enum definition

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo *info, unsigned action) ; // VIPER #7914

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriEnum(const VeriEnum &veri_enum, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriEnum(SaveRestore &save_restore) ;

    virtual ~VeriEnum() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriEnum>(this, name) ; }

protected:
    VeriEnum(VeriEnum &veri_enum, unsigned use_original_linefile):VeriDataType(veri_enum,use_original_linefile),_base_type(veri_enum._base_type),_ids(veri_enum._ids),_enum_ids(veri_enum._enum_ids) {veri_enum._base_type=0 ;veri_enum._ids=0 ;veri_enum._enum_ids=0 ;}

private:
    // Prevent compiler from defining the following
    VeriEnum() ;                            // Purposely leave unimplemented
    VeriEnum(const VeriEnum &) ;            // Purposely leave unimplemented
    VeriEnum& operator=(const VeriEnum &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Convert verilog package to vhdl package
    virtual VhdlSubtypeIndication*   ConvertDataType(VhdlScope* /*vhdl_scope*/, VeriScope* /*veri_scope*/) { return 0 ; }
    virtual VhdlTypeDef*             ConvertToTypeDef(Array *decl_part, VhdlScope *vhdl_scope, VeriScope *veri_scope, VhdlTypeId *id) ;
#endif

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType *old_type, VeriDataType *new_type, unsigned delete_old_node) ; // Delete 'old_type' specific base type, and puts new type in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration routines
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // probably not needed..
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual VeriDataType *      StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab) ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      ReduceAndAddDimension(unsigned reduce_dim_by, VeriRange *to_add_range, VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456

    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type

protected:
    VeriDataType *_base_type ; // Base data type of enumerate types
    Array        *_ids ;       // Array of VeriIdDefs, which are the declared enum constants.
    Map          *_enum_ids ;  // map of enum literal name char*  vs VeriIdDef*
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriEnum

/* -------------------------------------------------------------- */

// Various new expression classes :
class VFC_DLL_PORT VeriDotStar : public VeriExpression
{
public:
    VeriDotStar() ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDOTSTAR; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Tests :
    virtual unsigned            IsDotStar() const           { return 1 ; }

    // Accessor routines :
    virtual VeriScope *         GetDotStarScope() const     { return _dot_star_scope ; } // Back-pointer to the scope in which this .* construct appears. Used to find the missing actuals.

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy dot star expression

    virtual VeriIdDef *         GetActualIdForFormal(const char* formal_name, unsigned is_vhdl) const ; // Get the actual id with the name of the formal

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDotStar(const VeriDotStar &dot_star, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDotStar(SaveRestore &save_restore) ;

    virtual ~VeriDotStar() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriDotStar>(this, name) ; }

protected:
    VeriDotStar(VeriDotStar &dot_star, unsigned use_original_linefile):VeriExpression(dot_star,use_original_linefile),_dot_star_scope(dot_star._dot_star_scope) {dot_star._dot_star_scope=0 ;}

private:
    // Prevent compiler from defining the following
    VeriDotStar(const VeriDotStar &) ;            // Purposely leave unimplemented
    VeriDotStar& operator=(const VeriDotStar &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Static elaboration routines
    //virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const      { return 0 ; } // TOBEDONE, or not needed..
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // Probably not needed..

protected:
    // back-pointer to the scope in which this .* construct appears.
    // This back-pointer is set during Resolve time.
    // This can then be used at elaboration time to find the missing actuals.
    VeriScope *_dot_star_scope ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDotStar

/* -------------------------------------------------------------- */

/*
   VeriIfOper is the System Verilog 'if' 'else' operator, used for constraints and SVA expressions.
*/
class VFC_DLL_PORT VeriIfOperator : public VeriQuestionColon
{
public:
    // It's semantics are pretty similar to the ?: operator used for normal expressions,
    // but its syntax is different. So derive from VeriQuestionColon (inherit everything),
    // but override a few functions :
    VeriIfOperator(VeriExpression *if_expr, VeriExpression *then_expr, VeriExpression *else_expr) ;

    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERIIFOPERATOR; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsIfOperator() const       { return 1 ; }  // Return 1 if this is a (System Verilog) if-operator

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy if operator

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriIfOperator(const VeriIfOperator &if_op, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriIfOperator(SaveRestore &save_restore) ;

    virtual ~VeriIfOperator() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriIfOperator>(this, name) ; }

protected:
    VeriIfOperator(VeriIfOperator &if_oper, unsigned use_original_linefile):VeriQuestionColon(if_oper, use_original_linefile){}

private:
    // Prevent compiler from defining the following
    VeriIfOperator() ;                                  // Purposely leave unimplemented
    VeriIfOperator(const VeriIfOperator &) ;            // Purposely leave unimplemented
    VeriIfOperator& operator=(const VeriIfOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                PrettyPrintWithSemi(ostream &f, unsigned level) const ; // Called from within VeriConstraintDecl's PrettyPrint routine // VIPER #2074

    // Most of these are probably taken care of in base class :
    // Static elaboration routines
    // virtual int              StaticSizeSign() const ; // TOBEDONE, or not needed..
    // virtual VeriBaseValue *  StaticEvaluate(int context_size, ValueTable *n) const ; // probably not needed..

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriIfOperator

/* -------------------------------------------------------------- */

/*
   VIPER #5758: VeriCaseOperator is the System Verilog Assertion 'case' operator, used for constraints and SVA expressions.
*/
class VFC_DLL_PORT VeriCaseOperator : public VeriExpression
{
public:
    VeriCaseOperator(VeriExpression *cond, Array *case_items) ;

     // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICASEOPERATOR ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsCaseOperator() const     { return 1 ; }  // Return 1 if this is a (System Verilog) case-operator

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo *info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetCondition() const        { return _cond ; }
    Array *                     GetCaseItems() const        { return _case_items ; }

    // Parse tree copy routine :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy case operator as expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCaseOperator(const VeriCaseOperator &case_oper, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCaseOperator(SaveRestore &save_restore) ;

    virtual ~VeriCaseOperator() ;

private:
    // Prevent compiler from defining the following
    VeriCaseOperator() ;                                    // Purposely leave unimplemented
    VeriCaseOperator(const VeriCaseOperator &) ;            // Purposely leave unimplemented
    VeriCaseOperator& operator=(const VeriCaseOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if its is case condition, and puts new condition in its place.
    virtual unsigned            ReplaceChildCaseOperatorItemInternal(VeriCaseOperatorItem *old_case, VeriCaseOperatorItem *new_case, unsigned delete_old_node) ; // Delete 'old_case' from case item list, and puts new case item in its place.

    // Static elaboration routines
    //virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const      { return 0 ; } // TOBEDONE, or not needed..
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // Probably not needed..
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_cond ;
    Array          *_case_items ;   // Array of VeriCaseOperatorItem
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCaseOperator

/* -------------------------------------------------------------- */

/*
   A VeriSequenceConcat implements the expression " <sequence> ##(num_or_range) <sequence> "
   Both must be sequences. left side is optional. If missing, it represents sequence '1' (true).
   It is a essentially a binary operator, with a third (cycle number) expression in the middle.
   So derive from binary operator (set oper_type to VERI_CYCLE_DELAY_OP)
*/
class VFC_DLL_PORT VeriSequenceConcat : public VeriBinaryOperator
{
public:
    VeriSequenceConcat(VeriExpression *left, VeriExpression *cycle_delay_range, VeriExpression *right) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERISEQUENCECONCAT; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual unsigned            IsSequenceConcat() const   { return 1 ; }

    // Accessor methods
    virtual VeriExpression *    GetCycleDelayRange() const { return _cycle_delay_range ; } // int expr or range defining number of cycles between left-end and right-start.

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy sequence concatenation

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSequenceConcat(const VeriSequenceConcat &seq_concat, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSequenceConcat(SaveRestore &save_restore) ;

    virtual ~VeriSequenceConcat() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriSequenceConcat>(this, name) ; }

protected:
    VeriSequenceConcat(VeriSequenceConcat &seq_concat, unsigned use_original_linefile):VeriBinaryOperator(seq_concat, use_original_linefile),_cycle_delay_range(seq_concat._cycle_delay_range) {seq_concat._cycle_delay_range=0 ;}
private:
    // Prevent compiler from defining the following
    VeriSequenceConcat() ;                                      // Purposely leave unimplemented
    VeriSequenceConcat(const VeriSequenceConcat &) ;            // Purposely leave unimplemented
    VeriSequenceConcat& operator=(const VeriSequenceConcat &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

   // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific child expression, and puts new expression in its place

    // Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
//    virtual VeriBaseValue *   StaticEvaluate(int context_size, ValueTable *n) const { return 0 ; } // not needed.. base class will do the work.
//    virtual int               StaticSizeSign() const { return 0 ; } // // not needed.. base class will do the work.

protected:
    VeriExpression *_cycle_delay_range ; // int expr or range defining number of cycles between left-end and right-start.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSequenceConcat

/* -------------------------------------------------------------- */

/*
   VeriClockedSequence implements clocked sequence " @(posedge clk) <sequence> "
   This is essentially a unary operator, with a clocking event extra.
   Could implement as a binary operator, but the syntax is different (@ sign comes in front),
   and also this is somewhat of a special operation. So make this separate class for it.
*/
class VFC_DLL_PORT VeriClockedSequence : public VeriUnaryOperator
{
public:
    VeriClockedSequence(VeriExpression *event_expr, VeriExpression *sequence) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICLOCKEDSEQUENCE; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsClockedSequence() const  { return 1 ; }  // Return 1 if this is a (System Verilog) clocked sequence

    // Accessor methods
    virtual VeriExpression *    GetEventExpression() const  { return _event_expr ; } // The clocking event for the sequence (argument)..
    // inherit the rest (GetArg()..) from VeriUnaryOper

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy clocked sequence

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriClockedSequence(const VeriClockedSequence &clock_seq, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriClockedSequence(SaveRestore &save_restore) ;

    virtual ~VeriClockedSequence() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriClockedSequence>(this, name) ; }

protected:
    VeriClockedSequence(VeriClockedSequence &clk_seq, unsigned use_original_linefile):VeriUnaryOperator(clk_seq, use_original_linefile),_event_expr(clk_seq._event_expr) {clk_seq._event_expr=0 ;}

private:
    // Prevent compiler from defining the following
    VeriClockedSequence() ;                                       // Purposely leave unimplemented
    VeriClockedSequence(const VeriClockedSequence &) ;            // Purposely leave unimplemented
    VeriClockedSequence& operator=(const VeriClockedSequence &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific event expression/sequence, and puts new expression in its place.

    // Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
//    virtual VeriBaseValue *   StaticEvaluate(int context_size, ValueTable *n) const { return 0 ; } // not needed.. base class will do the work.
//    virtual int               StaticSizeSign() const { return 0 ; } // // not needed.. base class will do the work.

protected:
    VeriExpression *_event_expr ; // the clocking event for the sequence (argument)..
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriClockedSequence

/* -------------------------------------------------------------- */

/*
   VeriAssignmentsInSequence implements the "," operation as a sequence : " <expr> , a=5, c++ "
   Semantics are that if 'expr' is true, assignments can be made to local variables,
   before this sequence starts.. (result is 'expr' at all times).
   The value of the local variables can be used later in the sequence..

   We implement this as a unary operator, with a statement list attached..
*/
class VFC_DLL_PORT VeriAssignInSequence : public VeriUnaryOperator
{
public:
    VeriAssignInSequence(VeriExpression *expr, Array *stmts) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIASSIGNINSEQUENCE; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor methods
    Array *                     GetStatements() const   { return _stmts ; } // The assignment statements to be executed when 'expr' holds.
    // inherit the rest (GetArg()..) from VeriUnaryOper

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy sequence

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriAssignInSequence(const VeriAssignInSequence &assign_seq, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriAssignInSequence(SaveRestore &save_restore) ;

    virtual ~VeriAssignInSequence() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriAssignInSequence>(this, name) ; }

protected:
    VeriAssignInSequence(VeriAssignInSequence &assign_in_seq, unsigned use_original_linefile):VeriUnaryOperator(assign_in_seq, use_original_linefile),_stmts(assign_in_seq._stmts) {assign_in_seq._stmts=0 ;}

private:
    // Prevent compiler from defining the following
    VeriAssignInSequence() ;                                        // Purposely leave unimplemented
    VeriAssignInSequence(const VeriAssignInSequence &) ;            // Purposely leave unimplemented
    VeriAssignInSequence& operator=(const VeriAssignInSequence &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' specific statement from statement list, and puts new statement in its place

    // Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
//    virtual VeriBaseValue *   StaticEvaluate(int context_size, ValueTable *n) const { return 0 ; } // not needed.. base class will do the work.
//    virtual int               StaticSizeSign() const { return 0 ; } // // not needed.. base class will do the work.
protected:
    Array *_stmts ; // The assignment statements to be executed when 'expr' holds.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAssignInSequence

/* -------------------------------------------------------------- */

/*
   VeriDistOperator implements a 'dist' operation : " <expr> dist { dist_list } "
   This is essentially a unary operator of expr on LHS, and a list of identifiers on the right.
   Used in sequences, and in constraints.
   The dist_list is a list of value_ranges or expressions, optionally with weight :
   "value_range := expr" or "value_rang /= expr" implemented with binary operators.
   So dist_list is an Array of VeriExpression.
   VeriDistOperator returns TRUE if the value of the (LHS) expression is contained in the set (on the RHS).
*/
class VFC_DLL_PORT VeriDistOperator : public VeriUnaryOperator
{
public:
    VeriDistOperator(VeriExpression *expr, Array *dist_list) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDISTOPERATOR; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    virtual void                VarUsage(VeriVarUsageInfo * info, unsigned action) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor methods
    Array *                     GetDistList() const         { return _dist_list ; } // The dist items (Array of VeriExpression) on RHS of the dist token
    // inherit the rest (GetArg()..) from VeriUnaryOper

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy distribution operator

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDistOperator(const VeriDistOperator &dist_op, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDistOperator(SaveRestore &save_restore) ;

    virtual ~VeriDistOperator() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriDistOperator>(this, name) ; }

protected:
    VeriDistOperator(VeriDistOperator &dist_oper, unsigned use_original_linefile):VeriUnaryOperator(dist_oper, use_original_linefile),_dist_list(dist_oper._dist_list) {dist_oper._dist_list=0 ;}

private:
    // Prevent compiler from defining the following
    VeriDistOperator() ;                                    // Purposely leave unimplemented
    VeriDistOperator(const VeriDistOperator &) ;            // Purposely leave unimplemented
    VeriDistOperator& operator=(const VeriDistOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific child expression, and puts new expression in its place

    // Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

//    virtual VeriBaseValue *   StaticEvaluate(int context_size, ValueTable *n) const { return 0 ; } // not needed.. base class will do the work.
//    virtual int               StaticSizeSign() const { return 0 ; } // // not needed.. base class will do the work.

protected:
    Array *_dist_list ; // The dist items (Array of VeriExpression) on RHS of the dist token
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDistOperator

/* -------------------------------------------------------------- */

/*
   VeriSolveBefore implements the variable ordering " solve <id_list> before <id_list> " construct.
   id_list is a list of random variables in VeriIdRef (or general VeriName/VeriExpression) form.
   This construct appears within constraint expressions, for random value constraints.
   All identifiers in the id_lists must have 'rand' ('VERI_RAND' token) in their declarations.
*/
class VFC_DLL_PORT VeriSolveBefore : public VeriExpression
{
public:
    VeriSolveBefore(Array *solve_list, Array *before_list) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISOLVEBEFORE; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    virtual unsigned            IsSolveBefore()             { return 1 ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    // VarUsage
    virtual void                VarUsage(VeriVarUsageInfo * info, unsigned action) ;

    // Accessor methods
    Array *                     GetSolveList() const        { return _solve_list ; }  // Array of VeriName's, indicating the variables which need to be solved
    Array *                     GetBeforeList() const       { return _before_list ; } // Array of VeriName's, indicating the variables which need to be solved before the solved list

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy solve before expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSolveBefore(const VeriSolveBefore & solve_before, VeriMapForCopy & id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSolveBefore(SaveRestore &save_restore) ;

    virtual ~VeriSolveBefore() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriSolveBefore>(this, name) ; }

protected:
    VeriSolveBefore(VeriSolveBefore &solve_before, unsigned use_original_linefile):VeriExpression(solve_before, use_original_linefile),_solve_list(solve_before._solve_list),_before_list(solve_before._before_list){solve_before._solve_list=0 ; solve_before._before_list=0 ;}

private:
    // Prevent compiler from defining the following
    VeriSolveBefore() ;                                   // Purposely leave unimplemented
    VeriSolveBefore(const VeriSolveBefore &) ;            // Purposely leave unimplemented
    VeriSolveBefore& operator=(const VeriSolveBefore &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' specific child variable name, and puts new variable name in its place

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // Not needed ?
    //virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const { return 0 ; } // Not needed ?

protected:
    Array *_solve_list ;  // Array of VeriName's, indicating the variables which need to be solved
    Array *_before_list ; // Array of VeriName's, indicating the variables which need to be solved before the solved list
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSolveBefore

/* -------------------------------------------------------------- */

/*
   VeriCast implements the casting operation "  <simple_data_type>'expr "
   Target data type is a VeriDataType expression, and expression is 'expr'.
   Target data type can also be a numeric constant (number of bits) (VeriConst),
   so we make the target_type a VeriExpression.
   We could implement this as a Binary operator, but since it is so specific in behavior,
   its better to create a separate class for it.
*/
class VFC_DLL_PORT VeriCast : public VeriExpression
{
public:
    VeriCast(VeriExpression *target_type, VeriExpression *expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICAST; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor methods
    virtual VeriExpression *    GetTargetType() const       { return _target_type ; } // The type to which we will cast (or numeric constant for 'number of bits')
    virtual VeriExpression *    GetExpr() const             { return _expr ; }        // The expression which is casted.
    virtual unsigned            IsConstExpr() const ; // VIPER #7864: returns 1 for constant expression

    virtual unsigned            SetTargetType(VeriExpression *new_type) ; // VIPER #7936

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy casting

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCast(const VeriCast &cast, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCast(SaveRestore &save_restore) ;

    virtual ~VeriCast() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriCast>(this, name) ; }

protected:
    VeriCast(VeriCast &veri_cast, unsigned use_original_linefile):VeriExpression(veri_cast, use_original_linefile),_target_type(veri_cast._target_type),_expr(veri_cast._expr){veri_cast._target_type=0 ; veri_cast._expr=0 ;}

private:
    // Prevent compiler from defining the following
    VeriCast() ;                            // Purposely leave unimplemented
    VeriCast(const VeriCast &) ;            // Purposely leave unimplemented
    VeriCast& operator=(const VeriCast &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific target/source expression, and puts new expression inits place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ;
    virtual VeriExpression *    StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint * target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    VeriExpression *_target_type ; // The type to which we will cast (or numeric constant expression)
    VeriExpression *_expr ; // The expression which is casted.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCast

/* -------------------------------------------------------------- */

/*
   VeriNew implements a 'new' expression (create a new value for a associative array) :
         " new [ size_expr ] ( list_of_args ) "
   Essentially, this is an operator call (similar to function_call) to operator "new",
   but there are some specialties : 'size_expr' denotes the number of objects to create (as an array-of-objects).
   and 'list_of_args' can be something different too (can denote a single variable....Whatever that means).
*/
class VFC_DLL_PORT VeriNew : public VeriExpression
{
public:
    VeriNew(VeriExpression *size_expr, Array *args) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERINEW; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsNewExpr() const           { return 1 ; } // Returns 1 for new expression

    // Accessor methods
    virtual VeriExpression *    GetSizeExpr() const         { return _size_expr ; } // The number of elements to create of this object..
    virtual Array *             GetArgs() const             { return _args ; }      // The arguments (to the 'new' operator).
    virtual Array *             SetArgs(Array *args)        { Array *ret_args = _args ; _args = args ; return ret_args ; }          // Set args to zero
    void                        SetAsCopyConstructor()      { _copy_constructor = 1 ; } // It is a copy constructor instead of the class method 'new' call
    virtual unsigned            IsCopyConstructor() const   { return _copy_constructor ; }

    // Resolve the Newfunctioncall
    virtual void                ResolveNewCall(VeriIdDef *func_id) ;

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy new expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNew(const VeriNew &veri_new, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriNew(SaveRestore &save_restore) ;

    virtual ~VeriNew() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriNew>(this, name) ; }

protected:
    VeriNew(VeriNew &veri_new, unsigned use_original_linefile):VeriExpression(veri_new, use_original_linefile),_size_expr(veri_new._size_expr),_args(veri_new._args),_copy_constructor(veri_new._copy_constructor) {veri_new._size_expr=0 ; veri_new._args=0 ;}

private:
    // Prevent compiler from defining the following
    VeriNew() ;                           // Purposely leave unimplemented
    VeriNew(const VeriNew &) ;            // Purposely leave unimplemented
    VeriNew& operator=(const VeriNew &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific size/argument expression, and puts new expression in its place.

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const { return 0 ; } // TOBEDONE
    //virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const { return 0 ; } // TOBEDONE
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression  *_size_expr ; // The number of elements to create for this object..
    Array           *_args ;      // The arguments (to the 'new' operator).
    unsigned         _copy_constructor:1 ; // Flag to indicate whether it is calling the copy constructor instead of the class method 'new'
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNew

/* -------------------------------------------------------------- */

/*
   VeriConcatItem implements the System Verilog concatenation item
     "member_label : const_expr "  or
     "default : const_expr "
   This is an 'item' similar to case-item/generate-case-item.
   The 'member_label' can be a constant expression or type_identifier (or variable_identifier), or keyword 'default'.
   The 'default' member label is represented with 'member_label==0'.
*/
class VFC_DLL_PORT VeriConcatItem : public VeriExpression
{
public:
    VeriConcatItem(VeriExpression *member_label, VeriExpression *expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICONCATITEM; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor methods
    virtual VeriExpression *    GetMemberLabel() const      { return _member_label ; } // The expression which identifies the member label. If 0, is is the 'default' label.
    virtual VeriExpression *    GetExpr() const             { return _expr ; }         // The expression which is casted.
    virtual unsigned            IsConcatItem() const        { return 1 ; }  // Return 1 if this is a (Syatem Verilog) <member> : <value>

    // Parse tree copy routines :
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy concat item

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConcatItem(const VeriConcatItem &concat_item, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriConcatItem(SaveRestore &save_restore) ;

    virtual ~VeriConcatItem() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriConcatItem>(this, name) ; }

protected:
    VeriConcatItem(VeriConcatItem &concat_item, unsigned use_original_linefile):VeriExpression(concat_item, use_original_linefile),_member_label(concat_item._member_label),_expr(concat_item._expr) {concat_item._member_label=0 ;concat_item._expr=0 ;}

private:
    // Prevent compiler from defining the following
    VeriConcatItem() ;                                  // Purposely leave unimplemented
    VeriConcatItem(const VeriConcatItem &) ;            // Purposely leave unimplemented
    VeriConcatItem& operator=(const VeriConcatItem &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific member label/concat element, and puts new expression in its place.

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int, ValueTable *, VeriConstraint *, unsigned ) const ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual void                StaticEvaluateByChoice(VeriBaseValue *value, ValueTable *val_table, VeriConstraint *constraint, unsigned expect_nonconst, unsigned &cannot_evaluate, unsigned check_validity, Set *done) ;
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;  // constraint checking

protected:
    VeriExpression *_member_label ; // The expression which identifies the member label. If 0, is is the 'default' label.
    VeriExpression *_expr ; // The expression which is the concat element.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConcatItem

/* -------------------------------------------------------------- */

/*
   VeriNull implements the System Verilog 'null' literal.
*/
class VFC_DLL_PORT VeriNull : public VeriExpression
{
public:
    VeriNull() ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERINULL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsNull() const          { return 1 ; } // Check if this expression is a null literal.

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy open port connection

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNull(const VeriNull &null_literal, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriNull(SaveRestore &save_restore) ;

    virtual ~VeriNull() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriNull>(this, name) ; }

protected:
    VeriNull(VeriNull &veri_null, unsigned use_original_linefile):VeriExpression(veri_null, use_original_linefile){}

private:
    // Prevent compiler from defining the following
    VeriNull(const VeriNull &) ;            // Purposely leave unimplemented
    VeriNull& operator=(const VeriNull &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // FIX ME : TOBEDONE
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ;  // Viper #7711: in 64 bits

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNull

/* -------------------------------------------------------------- */

/*
   VeriDollar implements System Verilog '$' expression
*/
class VFC_DLL_PORT VeriDollar : public VeriExpression
{
public:
    VeriDollar() ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIDOLLAR ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsDollar() const        { return 1 ; } // Check if this expression is a dollar literal.
    virtual unsigned            IsConstExpr() const ; // returns 1 for constant expression
    virtual unsigned            HasDollar() const       { return 1 ; } // It is $ itself, so return 1

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const; // Copy dollar

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDollar(const VeriDollar &dollar, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDollar(SaveRestore &save_restore) ;

    virtual ~VeriDollar() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriDollar>(this, name) ; }

protected:
    VeriDollar(VeriDollar &veri_dollar, unsigned use_original_linefile):VeriExpression(veri_dollar, use_original_linefile){}

private:
    // Prevent compiler from defining the following
    VeriDollar(const VeriDollar &) ;            // Purposely leave unimplemented
    VeriDollar& operator=(const VeriDollar &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const ;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711: in 64 bits

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDollar

/* -------------------------------------------------------------- */
/*
   Std 1800 addition for modeling assignment patterns
   A VeriAssignmentPattern implements the IEEE 1800 construct :  <data_type> '{ <concat_elements> }
   This construct behaves very much like a VeriConcat (without the data_type), and that is why we derive it from there.
   Only difference pertains to the interpretation of the elements :
   In a normal concatenation, all elements are self-determined, and can appear anywhere.
   In an assignment pattern, there is always a 'data_type' in the context, or in the prefix,
   which determines the size and sign of each individual element.
*/
class VFC_DLL_PORT VeriAssignmentPattern : public VeriConcat
{
public:
    VeriAssignmentPattern(VeriExpression *type, Array *exprs) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIASSIGNMENTPATTERN ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsAssignPattern() const        { return 1 ; }  // Check if assignment pattern

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Routines to get full type information :
    virtual unsigned            PackedDimension() const ;   // Return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // Return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ;   // Return the base type (built-in type/struct/union) looking through named types
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
// First one if from the base class :
//    Array *                     GetExpressions() const  { return _exprs ; } // Get array of VeriExpressions
//    VeriExpression *            GetType() const         { return _type ; }  // Get the type of the assignment pattern expression
    virtual VeriExpression *    GetTargetType() const       { return _type ; } // Get the type of the assignment pattern expression

    virtual unsigned            SetTargetType(VeriExpression *new_type) ; // VIPER #7936

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriAssignmentPattern(const VeriAssignmentPattern &assign_pattern, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriAssignmentPattern(SaveRestore &save_restore) ;

    virtual ~VeriAssignmentPattern() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriAssignmentPattern>(this, name) ; }

protected:
    VeriAssignmentPattern(VeriAssignmentPattern &veri_assignment_pattern, unsigned use_original_linefile):VeriConcat(veri_assignment_pattern, use_original_linefile),_type(veri_assignment_pattern._type){veri_assignment_pattern._type=0 ;}

private:
    // Prevent compiler from defining the following
    VeriAssignmentPattern() ;                                         // Purposely leave unimplemented
    VeriAssignmentPattern(const VeriAssignmentPattern &) ;            // Purposely leave unimplemented
    VeriAssignmentPattern& operator=(const VeriAssignmentPattern &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    static unsigned             CheckAssignmentPatternType(const Array *exprs, VeriTypeInfo *expected_type, veri_type_env environment, const VeriTreeNode *from) ; // Routine to check expression array
    // Verific internal routine to check if it can be treated as assignment pattern: always return 1
    virtual unsigned            CanBeAssignmentPattern(const VeriTypeInfo * /*context*/) const { return 1 ; }

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If any assign_pattern element is 'old_expr, deletes 'old_expr' and puts 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ; // Evaluate constant expression
    virtual unsigned            StaticAssign(ValueTable * /*eval*/, VeriBaseValue * /*value*/, unsigned /*expect_nonconst*/) { return 0 ; }
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. For signed expression returns negative of expression size, Viper #7711: return in 64 bits
    virtual VeriExpression *    StaticEvaluateToExprInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    //virtual VeriBaseValue *     EvaluateParamArrayExpr(int context_size, ValueTable *n, VeriConstraint *target_constraint) const ; // Evaluate value of parameter array
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;  // constraint checking
    virtual VeriBaseValue *     StaticEvaluateForAssignmentPattern(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst, unsigned check_validity) const ;

protected:
    VeriExpression  *_type ; // should be of a data type as it stores the target type for  assignment pattern expressions
//    Array           *_exprs ; // Array of VeriExpression* // This is in the VeriConcat base class
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAssignmentPattern

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling multi-assignment patterns
   A VeriMultiAssignmentPattern implements the IEEE 1800 construct :  <data_type> '{ <repeat> { <concat_elements> } }
   This construct behaves very much like a VeriMultiConcat (without the data_type), and that is why we derive it from there.
   Only difference pertains to the interpretation of the elements :
   In a normal concatenation, all elements are self-determined, and can appear anywhere.
   In a assignment pattern, there is always a 'data_type' in the context (or in the prefix),
   which determines the size and sign of each individual element.
*/
class VFC_DLL_PORT VeriMultiAssignmentPattern : public VeriMultiConcat
{
public:
    VeriMultiAssignmentPattern(VeriExpression *type, VeriExpression *repeat, Array *exprs) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const           { return ID_VERIMULTIASSIGNMENTPATTERN ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsMultiAssignPattern() const  { return 1 ; }  // Check if multi-assignment pattern

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
// First two are from the base class :
//    VeriExpression *            GetRepeat() const           { return _repeat ; } // Get repeat VeriExpression
//    Array *                     GetExpressions() const      { return _exprs ; }  // Get array of VeriExpressions
//    VeriExpression *            GetType() const             { return _type ; }   // Get the type of the multi assignment pattern expression
    virtual VeriExpression *    GetTargetType() const         { return _type ; } // Get the type of the assignment pattern expression

    virtual unsigned            SetTargetType(VeriExpression *new_type) ; // VIPER #7936

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy multiconcatenation

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriMultiAssignmentPattern(const VeriMultiAssignmentPattern &multi_concat, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriMultiAssignmentPattern(SaveRestore &save_restore) ;

    virtual ~VeriMultiAssignmentPattern() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriMultiAssignmentPattern>(this, name) ; }

protected:
    VeriMultiAssignmentPattern(VeriMultiAssignmentPattern &base, unsigned use_original_linefile):VeriMultiConcat(base, use_original_linefile),_type(base._type){base._type=0 ;}

private:
    // Prevent compiler from defining the following
    VeriMultiAssignmentPattern() ;                                              // Purposely leave unimplemented
    VeriMultiAssignmentPattern(const VeriMultiAssignmentPattern &) ;            // Purposely leave unimplemented
    VeriMultiAssignmentPattern& operator=(const VeriMultiAssignmentPattern &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Verific internal routine to check if it can be treated as assignment pattern: always return 1
    virtual unsigned            CanBeAssignmentPattern(const VeriTypeInfo * /*context*/) const { return 1 ; }

    static unsigned             CheckMultiAssignmentPatternType(const Array *exprs, const VeriTypeInfo *passing_type, veri_type_env environment, const VeriTreeNode *from) ;
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If 'old_expr' is a child of multi assignment pattern, delete 'old_expr' and put 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static Elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ; // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size and sign. For signed expression returns negative of expression size, Viper #7711: return in 64 bits

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    //virtual VeriBaseValue *     EvaluateParamArrayExpr(int context_size, ValueTable *n, VeriConstraint *target_constraint) const ; // Evaluate value of parameter array
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;  // constraint checking
    virtual VeriBaseValue *     StaticEvaluateForAssignmentPattern(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst, unsigned check_validity) const ;

protected:
    VeriExpression  *_type ; // should be of a data type as it stores the target type for  assignment pattern expressions
//    VeriExpression  *_repeat ; // This is in the VeriMultiConcat base class
//    Array           *_exprs ; // Array of VeriExpression // This is in the VeriMultiConcat base class
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriMultiAssignmentPattern

/* -------------------------------------------------------------- */
/*
   VeriForeachOperator is the System Verilog 'foreach' operator, used for constraints.
*/
class VFC_DLL_PORT VeriForeachOperator : public VeriExpression
{
public:
    VeriForeachOperator(VeriName *array_name, Array *loop_indexs, VeriExpression *constraint_set, VeriScope *scope) ;

    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERIFOREACHOPERATOR; } // Defined in VeriClassIds.h

    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    VeriName *                  GetArrayName() const     { return _array_name ; }
    Array *                     GetLoopIndexes() const   { return _loop_indexes ; }
    VeriExpression *            GetConstraintSet() const { return _constraint_set ; }
    virtual VeriScope *         GetScope() const         { return _scope ; }

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy foreach operator

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriForeachOperator(const VeriForeachOperator &if_op, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriForeachOperator(SaveRestore &save_restore) ;

    virtual ~VeriForeachOperator() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriForeachOperator>(this, name) ; }

protected:
    VeriForeachOperator(VeriForeachOperator &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_array_name(base._array_name),_loop_indexes(base._loop_indexes),_constraint_set(base._constraint_set),_scope(base._scope), _implicit_decl_list(base._implicit_decl_list) {base._array_name=0 ;base._loop_indexes=0 ;base._constraint_set=0 ;base._scope=0 ; base._implicit_decl_list = 0 ; }

private:
    // Prevent compiler from defining the following
    VeriForeachOperator() ;                                       // Purposely leave unimplemented
    VeriForeachOperator(const VeriForeachOperator &) ;            // Purposely leave unimplemented
    VeriForeachOperator& operator=(const VeriForeachOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                PrettyPrintWithSemi(ostream &f, unsigned level) const ;
    // Static elaboration routines
    //virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const { return 0 ; }
    virtual VeriBaseValue *  StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // probably not needed..
    virtual void             StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriName       *_array_name ;      // Array over which iteration is done
    Array          *_loop_indexes ;    // Array of loop index variables array of VeriIdDefs
    VeriExpression *_constraint_set ;  // Constraint set
    VeriScope      *_scope ;           // Scope created by this operator
    Array          *_implicit_decl_list ; // List of implicit declarations for array index (list of VeriDataDecl*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriForeachOperator

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling streaming concatenation (VIPER 2338)
   A VeriStreamingConcat implements the IEEE 1800 construct :
     { stream_operator [ slice_size] { stream_expression {, stream_expression}}}
   This construct behaves very much like a VeriConcat when stream operator is '>>' and
   no slice size is specified, so we derive it from VeriConcat. Difference pertains
   if slice size is greater than 1 and operator is '<<'.
*/
class VFC_DLL_PORT VeriStreamingConcat : public VeriConcat
{
public:
    VeriStreamingConcat(unsigned stream_operator, VeriExpression *slice_size, Array *stream_exprs) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERISTREAMINGCONCAT ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsStreamingConcat() const  { return 1 ; }

    // Accessor functions
    virtual unsigned            OperType() const        { return _stream_operator ; }  // Type of operator
    VeriExpression *            GetSliceSize() const    { return _slice_size ; }       // Get the expression representing slice size (data type or expression)

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriStreamingConcat(const VeriStreamingConcat &streaming_concat, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriStreamingConcat(SaveRestore &save_restore) ;

    virtual ~VeriStreamingConcat() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriStreamingConcat>(this, name) ; }

protected:
    VeriStreamingConcat(VeriStreamingConcat &base, unsigned use_original_linefile):VeriConcat(base, use_original_linefile),_stream_operator(base._stream_operator),_slice_size(base._slice_size) {base._slice_size=0 ;}

private:
    // Prevent compiler from defining the following
    VeriStreamingConcat() ;                                       // Purposely leave unimplemented
    VeriStreamingConcat(const VeriStreamingConcat &) ;            // Purposely leave unimplemented
    VeriStreamingConcat& operator=(const VeriStreamingConcat &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If any streaming concat element is 'old_expr, deletes 'old_expr' and puts 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const ; // Evaluate constant expression
    virtual unsigned            StaticAssign(ValueTable *eval, VeriBaseValue *value, unsigned expect_nonconst) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    char *                      StaticStreamingRight2Left(char *str, ValueTable *val_table, unsigned expect_nonconst) const ; // Internal routine for streaming concat
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // VIPER #5772: Returns size and sign. For signed expression returns negative of expression size, Viper #7711: return in 64 bits
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    unsigned        _stream_operator:12 ; // Token VERI_LSHIFT or VERI_RSHIFT
    VeriExpression *_slice_size ;      // Number of bits in each slice of data to be streamed (VeriDataType or VeriExpression)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriStreamingConcat

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling cond_predicate (pattern matching)
   A VeriCondPredicate implements the construct :
    <expression_or_cond_pattern> { "&&&" <expression_or_cond_pattern>}"

    This '&&&' separated expression_or_cond_pattern is implemented in the
    same way as '.' separated selected name
*/
class VFC_DLL_PORT VeriCondPredicate : public VeriExpression
{
public:
    VeriCondPredicate(VeriExpression *left, VeriExpression *right) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERICONDPREDICATE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual VeriExpression *    GetLeft() const         { return _left ; }
    virtual VeriExpression *    GetRight() const        { return _right ; }
    virtual VeriScope *         GetScope() const ; // Return scope of the CondPredicate arguments pattern (if any).
    virtual unsigned            IsCondPredicate() const { return 1 ; }  // Return 1 if this is a (System Verilog) condpredicate
    // Parse-tree manipulation methods:
    virtual void                SetLeft(VeriExpression *expr)  { _left = expr ; } // for cases of parse tree manipulation : change the left operand
    virtual void                SetRight(VeriExpression *expr) { _right = expr ; } // for cases of parse tree manipulation : change the left operand

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCondPredicate(const VeriCondPredicate &cond_predicate, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCondPredicate(SaveRestore &save_restore) ;

    virtual ~VeriCondPredicate() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriCondPredicate>(this, name) ; }

protected:
    VeriCondPredicate(VeriCondPredicate &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_left(base._left),_right(base._right) {base._left=0 ;base._right=0 ;}

private:
    // Prevent compiler from defining the following
    VeriCondPredicate() ;                                     // Purposely leave unimplemented
    VeriCondPredicate(const VeriCondPredicate &) ;            // Purposely leave unimplemented
    VeriCondPredicate& operator=(const VeriCondPredicate &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // If any item of cond predicate is 'old_expr, deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const ; // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_left ;  // Left operand (can be simple expression or another VeriCondPredicate)
    VeriExpression *_right ; // Right operand (expr / expr matches pattern)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCondPredicate

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling pattern ".<variable_identifier>"
*/
class VFC_DLL_PORT VeriDotName : public VeriExpression
{
public:
    explicit VeriDotName(VeriName *name) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIDOTNAME ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    VeriName *                  GetVarName() const      { return _name ; } // Get variable name

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDotName(const VeriDotName &dot_name, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDotName(SaveRestore &save_restore) ;

    virtual ~VeriDotName() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriDotName>(this, name) ; }

protected:
    VeriDotName(VeriDotName &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_name(base._name) {base._name=0 ;}

private:
    // Prevent compiler from defining the following
    VeriDotName() ;                               // Purposely leave unimplemented
    VeriDotName(const VeriDotName &) ;            // Purposely leave unimplemented
    VeriDotName& operator=(const VeriDotName &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // deletes 'old_name' if it is variable name and puts 'new_name' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression

protected:
    VeriName *_name ; // variable name
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDotName

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling tagged union expression :
   A VeriTaggedUnion implements the construct
    "tagged <member_identifier> [ <expression>]"
*/
class VFC_DLL_PORT VeriTaggedUnion : public VeriExpression
{
public:
    VeriTaggedUnion(VeriName *member_identifier, VeriExpression *expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERITAGGEDUNION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    VeriName *                  GetMember() const       { return _member ; } // Get member name
    virtual VeriExpression *    GetExpr() const         { return _expr ; }   // Get expression
    virtual unsigned            IsTaggedUnion() const   { return 1 ; } // Tagged union expression

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTaggedUnion(const VeriTaggedUnion &tagged_union, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTaggedUnion(SaveRestore &save_restore) ;

    virtual ~VeriTaggedUnion() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTaggedUnion>(this, name) ; }

protected:
    VeriTaggedUnion(VeriTaggedUnion &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_member(base._member),_expr(base._expr) {base._member=0 ;base._expr=0 ;}

private:
    // Prevent compiler from defining the following
    VeriTaggedUnion() ;                                   // Purposely leave unimplemented
    VeriTaggedUnion(const VeriTaggedUnion &) ;            // Purposely leave unimplemented
    VeriTaggedUnion& operator=(const VeriTaggedUnion &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Deletes 'old_name' and puts 'new_name' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Returns size/sign of the expression. For signed expression returns negative of expression size
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriName       *_member ;   // Member name
    VeriExpression *_expr ;     // Expression
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTaggedUnion

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling 'with' expression
   The following flavors of 'with' expressions call occur in design
    "<function_call> with (<expression>)"
    "<function_call> with <constraint_block>"
    "<expression> with [ <array_range_expression>]"
   Left hand side of 'with' keyword in the above three flavors can be represented
   by an expression, but right hand side can be expression (range is derived from
   expression) or array of expressions (constraint block).
   To represent this by a single class we need three fields
   VeriExpression *_left, VeriExpression *_right, Array *_constraint_exprs
   but one field will be null always (either _right or _constraint_exprs)
   Proper utilization of memory can be done if we define an abstract class 'VeriWith'
   and derive two classes from it (one with expression and another with array as field)

   Abstract class VeriWith to represent 'with' expression
*/
class VFC_DLL_PORT VeriWith : public VeriExpression
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;

    // Accessor functions
    virtual VeriExpression *    GetLeft() const             { return _left ; } // Get left hand side of 'with'
    virtual Array *             GetConstraintBlock() const  { return 0 ; }   // Get constraint block if used at RHS of 'with'
    virtual VeriScope *         GetScope() const            { return _scope ; }
    virtual void                SetScope(VeriScope *s)      { _scope = s ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    explicit VeriWith(VeriExpression *left) ;

    VeriWith(const VeriWith &with_expr, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriWith(SaveRestore &save_restore) ;

    VeriWith(VeriWith &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_left(base._left),_scope(base._scope) {base._scope=0 ;base._scope=0 ;}
public:
    virtual ~VeriWith() ;

private:
    // Prevent compiler from defining the following
    VeriWith() ;                            // Purposely leave unimplemented
    VeriWith(const VeriWith &) ;            // Purposely leave unimplemented
    VeriWith& operator=(const VeriWith &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // VIPER #8227

protected:
    VeriExpression *_left ;     // Left hand side of 'with' expression
    VeriScope      *_scope ;    // Scope local to this expression. Iterator variables are declared here
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriWith

/* -------------------------------------------------------------- */

/*
   VeriWithExpr implements 'with' expression
    "<function_call> with ( <expression>)"
    "<expression> with [ <array_range_expression>]"
*/
class VFC_DLL_PORT VeriWithExpr : public VeriWith
{
public:
    VeriWithExpr(VeriExpression *left, VeriExpression *right) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIWITHEXPR ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual VeriExpression *    GetRight() const        { return _right ; }   // Get right expression

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriWithExpr(const VeriWithExpr &with_expr, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriWithExpr(SaveRestore &save_restore) ;

    virtual ~VeriWithExpr() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriWithExpr>(this, name) ; }

protected:
    VeriWithExpr(VeriWithExpr &base, unsigned use_original_linefile):VeriWith(base, use_original_linefile),_right(base._right) {base._right=0 ;}

private:
    // Prevent compiler from defining the following
    VeriWithExpr() ;                                // Purposely leave unimplemented
    VeriWithExpr(const VeriWithExpr &) ;            // Purposely leave unimplemented
    VeriWithExpr& operator=(const VeriWithExpr &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // VIPER #8227
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_right ;     // Right hand side of 'with'
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriWithExpr

/* -------------------------------------------------------------- */

/*
   VeriInlineConstraint implements the inline constraints
    "<function_call> with <constraint_block>"
*/
class VFC_DLL_PORT VeriInlineConstraint : public VeriWith
{
public:
    VeriInlineConstraint(VeriExpression *left, Array *constraint_exprs) ;
    VeriInlineConstraint(VeriExpression *left, Array *id_refs, Array *constraint_exprs) ; // New constructor for VIPER #6756

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIINLINECONSTRAINT ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual unsigned            IsInlineConstraint() const { return 1 ; }
    virtual void                SetConstraintBlock(Array *blk) ;

    // Accessor functions
    virtual Array *             GetConstraintBlock() const  { return _constraint_block ; }   // Get right expression
    Array *                     GetIdentifierList() const   { return _id_refs ; }   // VIPER #6756: Get list of identifiers for inline-constraints
    virtual void                SetIdentifierList(Array *refs) ; // VIPER #6756: List of identifiers for inline-constraints

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriInlineConstraint(const VeriInlineConstraint &inline_constraint, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriInlineConstraint(SaveRestore &save_restore) ;

    virtual ~VeriInlineConstraint() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriInlineConstraint>(this, name) ; }

protected:
    VeriInlineConstraint(VeriInlineConstraint &base, unsigned use_original_linefile):VeriWith(base, use_original_linefile),_id_refs(base._id_refs),_constraint_block(base._constraint_block) {base._id_refs=0 ;base._constraint_block=0 ;}

private:
    // Prevent compiler from defining the following
    VeriInlineConstraint() ;                                        // Purposely leave unimplemented
    VeriInlineConstraint(const VeriInlineConstraint &) ;            // Purposely leave unimplemented
    VeriInlineConstraint& operator=(const VeriInlineConstraint &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

private:
    void                        CheckIdentifierList() const ; // VIPER #6756: Check list of identifiers for inline-constraints: they must be simple id-refs

protected:
    Array *_id_refs ;              // VIPER #6756: List of identifiers (VeriIdRef *) for inline-constraints (Array of VeriExpression *)
    Array *_constraint_block ;     // Right hand side of 'with' (array of VeriExpression*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriInlineConstraint

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling bins value i.e the values associated with a bin :
   A VeriBinValue implements the construct

     { <open_range_list> } [ iff (expression)]
   | <trans_list> [ iff (<expression>)]
   | default [ iff (<expression>)]
   | default sequence [ iff (<expression>)]

   Bins value can be of four different types having common portion "iff (expression)"
   So, we will treat this class 'VeriBinValue' as base class of bins value and it will
   contain common portion of four bin value types i.e "iff (expression)".
   We will derive three classes from 'VeriBinValue' to store four flavors of
   bins value.
   This bin value is derived from VeriExpression so that we can use VeriVariable
   as bin identifier and add bin value as initial value of bin identifier.
*/
class VFC_DLL_PORT VeriBinValue : public VeriExpression
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;

    // Accessor functions
    VeriExpression *            GetIffExpression() const    { return _iff_expr ; }
    virtual Array *             GetOpenRangeList() const    { return 0 ; } // Get open range list, if specified in bin value
    virtual Array *             GetTransList() const        { return 0 ; } // Get transition list, if specified in  bin value
    virtual VeriExpression *    GetSelectExpr() const       { return 0 ; } // Get select expression specified in selection bin value
    virtual unsigned            IsDefault() const           { return 0 ; } // Return 1 if keyword 'default' is specified in bin value
    virtual unsigned            IsDefaultSequence() const   { return 0 ; } // Return 1, if keywords 'default sequence' is specified in bin value

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    explicit VeriBinValue(VeriExpression *iff_expr) ;

    VeriBinValue(const VeriBinValue &bin_value, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriBinValue(SaveRestore &save_restore) ;

    VeriBinValue(VeriBinValue &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_iff_expr(base._iff_expr) {base._iff_expr=0 ;}
public:
    virtual ~VeriBinValue() ;

private:
    // Prevent compiler from defining the following
    VeriBinValue() ;                                // Purposely leave unimplemented
    VeriBinValue(const VeriBinValue &) ;            // Purposely leave unimplemented
    VeriBinValue& operator=(const VeriBinValue &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_iff_expr ;  // 'iff' expression
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBinValue

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling open range bin :
   A VeriOpenRangeBinValue implements the construct
     { <open_range_list> } [ iff (expression)]
*/
class VFC_DLL_PORT VeriOpenRangeBinValue : public VeriBinValue
{
public:
    VeriOpenRangeBinValue(Array *open_range_list, VeriExpression *iff_expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIOPENRANGEBINVALUE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual Array *             GetOpenRangeList() const{ return _open_range_list ; } // Get open range list specified in bin value

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriOpenRangeBinValue(const VeriOpenRangeBinValue &open_range_bin, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriOpenRangeBinValue(SaveRestore &save_restore) ;

    virtual ~VeriOpenRangeBinValue() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriOpenRangeBinValue>(this, name) ; }

protected:
    VeriOpenRangeBinValue(VeriOpenRangeBinValue &base, unsigned use_original_linefile):VeriBinValue(base, use_original_linefile),_open_range_list(base._open_range_list) {base._open_range_list=0 ;}
private:
    // Prevent compiler from defining the following
    VeriOpenRangeBinValue() ;                                         // Purposely leave unimplemented
    VeriOpenRangeBinValue(const VeriOpenRangeBinValue &) ;            // Purposely leave unimplemented
    VeriOpenRangeBinValue& operator=(const VeriOpenRangeBinValue &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array *_open_range_list ; // Open range list specified in bin value (array of VeriExpression*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriOpenRangeBinValue

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling transition bin value:
   A VeriTransitionBinValue implements the construct
     <trans_list> [ iff (expression)]
*/
class VFC_DLL_PORT VeriTransBinValue : public VeriBinValue
{
public:
    VeriTransBinValue(Array *trans_list, VeriExpression *iff_expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERITRANSBINVALUE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual Array *             GetTransList() const    { return _trans_list ; } // Get transition list

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTransBinValue(const VeriTransBinValue &trans_bin, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTransBinValue(SaveRestore &save_restore) ;

    virtual ~VeriTransBinValue() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTransBinValue>(this, name) ; }

protected:
    VeriTransBinValue(VeriTransBinValue &base, unsigned use_original_linefile):VeriBinValue(base, use_original_linefile),_trans_list(base._trans_list) {base._trans_list=0 ;}
private:
    // Prevent compiler from defining the following
    VeriTransBinValue() ;                                     // Purposely leave unimplemented
    VeriTransBinValue(const VeriTransBinValue &) ;            // Purposely leave unimplemented
    VeriTransBinValue& operator=(const VeriTransBinValue &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array *_trans_list ; // Transition list specified in bin value (array of VeriTransSet*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTransBinValue

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling default bin value:
   A VeriDefaultBinValue implements the construct
     default [ iff (expression)]
     default sequence [ iff (expression)]
*/
class VFC_DLL_PORT VeriDefaultBinValue : public VeriBinValue
{
public:
    VeriDefaultBinValue(unsigned sequence, VeriExpression *iff_expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDEFAULTBINVALUE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions/Checks
    virtual unsigned            IsDefault() const           { return 1 ;} // Return 1 as keyword 'default' is always present for this class
    virtual unsigned            IsDefaultSequence() const   { return (_sequence) ? 1 : 0 ; } // Return 1, if keywords 'default sequence' are specified in bin value

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDefaultBinValue(const VeriDefaultBinValue &default_bin, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDefaultBinValue(SaveRestore &save_restore) ;

    virtual ~VeriDefaultBinValue() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriDefaultBinValue>(this, name) ; }

protected:
    VeriDefaultBinValue(VeriDefaultBinValue &base, unsigned use_original_linefile):VeriBinValue(base, use_original_linefile),_sequence(base._sequence) {}
private:
    // Prevent compiler from defining the following
    VeriDefaultBinValue() ;                                       // Purposely leave unimplemented
    VeriDefaultBinValue(const VeriDefaultBinValue &) ;            // Purposely leave unimplemented
    VeriDefaultBinValue& operator=(const VeriDefaultBinValue &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression

protected:
    unsigned _sequence ; // Token for keyword VERI_SEQUENCE
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDefaultBinValue

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling select bin value (used in cross cover):
   A VeriSelectBinValue implements the construct
     <select_expr> [ iff (expression)]
*/
class VFC_DLL_PORT VeriSelectBinValue : public VeriBinValue
{
public:
    VeriSelectBinValue(VeriExpression *select_expr, VeriExpression *iff_expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERISELECTBINVALUE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual VeriExpression *    GetSelectExpr() const   { return _select_expr ; } // Get select expression specified in selection bin value

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSelectBinValue(const VeriSelectBinValue &select_bin, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSelectBinValue(SaveRestore &save_restore) ;

    virtual ~VeriSelectBinValue() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriSelectBinValue>(this, name) ; }

protected:
    VeriSelectBinValue(VeriSelectBinValue &base, unsigned use_original_linefile):VeriBinValue(base, use_original_linefile),_select_expr(base._select_expr) {base._select_expr=0 ;}
private:
    // Prevent compiler from defining the following
    VeriSelectBinValue() ;                                      // Purposely leave unimplemented
    VeriSelectBinValue(const VeriSelectBinValue &) ;            // Purposely leave unimplemented
    VeriSelectBinValue& operator=(const VeriSelectBinValue &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_select_expr ; // Select expression specified in selection bin
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSelectBinValue

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling transition set :
   A VeriTransSet implements the construct
     <trans_range_list> { => <trans_range_list> }
*/
class VFC_DLL_PORT VeriTransSet : public VeriExpression
{
public:
    explicit VeriTransSet(Array *array_of_trans_range_list);

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERITRANSSET ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    Array *                     GetTransRangeList() const   { return _trans_range_list ; } // Get '=>' token separated transition range list

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTransSet(const VeriTransSet &trans_set, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTransSet(SaveRestore &save_restore) ;

    virtual ~VeriTransSet() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTransSet>(this, name) ; }

protected:
    VeriTransSet(VeriTransSet &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_trans_range_list(base._trans_range_list) {base._trans_range_list=0 ;}
private:
    // Prevent compiler from defining the following
    VeriTransSet() ;                                // Purposely leave unimplemented
    VeriTransSet(const VeriTransSet &) ;            // Purposely leave unimplemented
    VeriTransSet& operator=(const VeriTransSet &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array *_trans_range_list ; // Array of trans range list (VeriTransRangeList*)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTransSet

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling transition range list :
   A VeriTransRangeList implements the construct
      <trans_item>
    | <trans_item> [* <repeat_range> ]
    | <trans_item> [-> <repeat_range> ]
    | <trans_item> [= <repeat_range> ]
*/
class VFC_DLL_PORT VeriTransRangeList : public VeriExpression
{
public:
    VeriTransRangeList(Array *trans_item, VeriExpression *repetition);

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERITRANSRANGELIST ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    Array *                     GetTransItem() const        { return _trans_item ; } // Get trans item i.e range list
    VeriExpression *            GetRepetition() const       { return _repetition ; } // Get repetition (binary operator representing consecutive repeat, goto repeat and non-consecutive repeat)

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTransRangeList(const VeriTransRangeList &trans_range_list, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTransRangeList(SaveRestore &save_restore) ;

    virtual ~VeriTransRangeList() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTransRangeList>(this, name) ; }

protected:
    VeriTransRangeList(VeriTransRangeList &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_trans_item(base._trans_item),_repetition(base._repetition) {base._trans_item=0 ;base._repetition=0 ;}
private:
    // Prevent compiler from defining the following
    VeriTransRangeList() ;                                      // Purposely leave unimplemented
    VeriTransRangeList(const VeriTransRangeList &) ;            // Purposely leave unimplemented
    VeriTransRangeList& operator=(const VeriTransRangeList &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array          *_trans_item ; // Trans item i.e range list (array of VeriExpression*)
    VeriExpression *_repetition ; // Repetition of transition item
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTransRangeList

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling select condition of selection bin (cross cover)  :
   A VeriSelectCondition implements the construct
     binsof ( <bins_expression>) [intersect { <open_range_list>}]
*/
class VFC_DLL_PORT VeriSelectCondition : public VeriExpression
{
public:
    VeriSelectCondition(VeriExpression *bins_expr, Array *open_range_list) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISELECTCONDITION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsSelectCondition() const   { return 1 ; } // Return 1 if this is a select condition else return 0

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    VeriExpression *            GetBinsExpr() const         { return _bins_expr ; }
    Array *                     GetOpenRangeList() const    { return _range_list ; }

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy assignment pattern expression

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSelectCondition(const VeriSelectCondition &select_cond, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSelectCondition(SaveRestore &save_restore) ;

    virtual ~VeriSelectCondition() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriSelectCondition>(this, name) ; }

protected:
    VeriSelectCondition(VeriSelectCondition &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_bins_expr(base._bins_expr),_range_list(base._range_list) {base._bins_expr=0 ;base._range_list=0 ;}

private:
    // Prevent compiler from defining the following
    VeriSelectCondition() ;                                       // Purposely leave unimplemented
    VeriSelectCondition(const VeriSelectCondition &) ;            // Purposely leave unimplemented
    VeriSelectCondition& operator=(const VeriSelectCondition &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_bins_expr ; // Bins expression
    Array          *_range_list ; // Open range list
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSelectCondition

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling type references
   A VeriTypeOperator implements the construct
        type (<expression>)
        type (<data_type>)
*/
class VFC_DLL_PORT VeriTypeOperator : public VeriDataType
{
public:
    explicit VeriTypeOperator(VeriExpression *expression) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERITYPEOPERATOR ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor functions
    virtual VeriExpression *    GetArg() const              { return _expr_or_data_type ; } // Return the data type or expression for the concerned VeriTypeOperator

    // Useful APIs
    virtual unsigned            IsTagged() const ;
    virtual unsigned            IsEnumType() const ;
    virtual unsigned            IsUnionType() const ; // Return 1 for union type
    virtual unsigned            IsStructType() const ; // Return 1 for Struct type
    virtual unsigned            IsIntegerType() const ;
    virtual unsigned            IsUnpacked() const ; // VIPER 2473 : Test if data type is unpacked
    virtual unsigned            IsVirtualInterface() const ;
    virtual unsigned            PackedDimension() const ; // return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
    virtual unsigned            UnPackedDimension() const ; // return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
    virtual VeriDataType *      GetBaseDataType() const ; // Return the base type (built-in type/struct/union) looking through named types
    virtual unsigned            BaseTypeSize() const ; // Return the bit size of base type (not considering packed dimension) VIPER #2838
    virtual VeriTypeInfo *      CreateType(VeriIdDef * /*for_id*/) ;
    virtual unsigned            Type() const ; // return the base scaler type (or struct/enum) of this data type.
    virtual unsigned            GetType() const          { return Type() ; }       // A token (VERI_REG, VERI_WIRE, VERI_INTEGER, etc) Replaces GetParamType/GetNetType etc on declaration items)
    // VIPER #6810 : Overwrite net type specific routines
    virtual unsigned            NetType() const ;
    virtual unsigned            IsNetType() const ;
    virtual unsigned            Sign() const ; // return 1 for a signed data type, 0 for unsigned.
    virtual char *              Image() const ; // Create a string image for it in Verilog syntax. Return 0 for any nonconstant tree node here.
    virtual unsigned            IsTypeOperator() const   { return 1 ; }

    virtual VeriIdDef *         GetMember(const char *member_name) const ; // return struct-member by name (if data type is a structure/union)
    virtual VeriScope *         GetMemberScope() const ; // scope of all declared identifiers. Only for struct/union/typerefs in SV.
    virtual Array *             GetEnums() const ;       // Return the array of enumeration identifiers. Only for enums (and typerefs of enums) in SV
    virtual VeriRange *         GetDimensionAt(unsigned dimension) ; // return the range which is 'dimension' dimensions deep into this data type. GetDimensionAt(0) returns the MSB dimension.
    virtual VeriRange *         GetDimensions() ;
    virtual unsigned            GetSigning() const ;
    virtual unsigned            IsCircular(Set &defined_type_ids) const ;  // Returns 1 if the datatype uses an id from the set to declare an item, like recursive structure/union or typedefs.

    // Virtual Accessor functions :
    virtual VeriName *          GetTypeName() const ; // the name (can be a hierarchical type name, or a simple identifier reference)
    virtual VeriIdDef *         GetId() const ; // Get resolved referenced (prefix) identifier.
    // duplicated from indexed name, to get virtual   fction access :
    virtual const char *        GetName() const ; // Get prefix name in char* form
    virtual VeriName *          GetPrefix() const ; // The prefix name (like an indexed name)
    virtual void                SetName(char *name) ; // Change reference name
    virtual void                SetId(VeriIdDef *id) ; // Change reference id

    // Parse tree copy routines
    virtual VeriDataType *      CopyDataType(VeriMapForCopy &id_map_table) const ; // Copy type operator

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTypeOperator(const VeriTypeOperator &type_op, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTypeOperator(SaveRestore &save_restore) ;

    virtual ~VeriTypeOperator() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriTypeOperator>(this, name) ; }

protected:
    VeriTypeOperator(VeriTypeOperator &base, unsigned use_original_linefile):VeriDataType(base, use_original_linefile),_expr_or_data_type(base._expr_or_data_type) {base._expr_or_data_type=0 ;}

private:
    // Prevent compiler from defining the following
    VeriTypeOperator() ;                                    // Purposely leave unimplemented
    VeriTypeOperator(const VeriTypeOperator &) ;            // Purposely leave unimplemented
    VeriTypeOperator& operator=(const VeriTypeOperator &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    virtual VeriRange *         CreateDimensions(unsigned only_unpacked) const ; // VIPER #7607: Verific internal use only

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual VeriScope *         GetDefinedScope(const VeriPseudoTreeNode *module_node, unsigned consider_node_for_dt) ;
    virtual VeriExpression *    StaticEvaluateToExprInternal(int /* context_size*/, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const ; // Evaluate constant expression and part of data type, returns expression
    virtual VeriExpression *    StaticProcessInitialValue(VeriIdDef *param_id, VeriPseudoTreeNode *expr_nod, const VeriPseudoTreeNode *module_scope, unsigned is_overwritten) ;
    virtual char *              StaticImage(VeriScope *scope, const char *prefix) ;
    virtual VeriDataType *      StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab) ;
    virtual VeriDataType *      ExtractDataType(VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriDataType *      ReduceAndAddDimension(unsigned reduce_dim_by, VeriRange *to_add_range, VeriScope *container_scope, Array *container_items, VeriModuleItem *from_data_decl) ; // VIPER #7456
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

    virtual VeriDataType *      CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const ; // VIPER #5335 : Return allocated  data type

protected:
    VeriExpression *_expr_or_data_type ; // Stores the expression or data type
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTypeOperator

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling pattern matching
   A VeriPatternMatch implements the construct
        <expr> matches <pattern>
*/
class VFC_DLL_PORT VeriPatternMatch : public VeriExpression
{
public:
    VeriPatternMatch(VeriExpression *expr, VeriExpression *pattern, VeriScope *pattern_scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPATTERNMATCH ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual VeriExpression *    GetLeft() const              { return _left ; } // Return the left expression
    virtual VeriExpression *    GetRight() const             { return _right ; } // Return the right expression
    virtual VeriScope *         GetScope() const             { return _scope ; }
    virtual unsigned            IsPatternMatch() const       { return 1 ; }  // Return 1 if this is a (System Verilog) pattern match
    // Parse-tree manipulation methods:
    virtual void                SetLeft(VeriExpression *expr)  { _left = expr ; } // for cases of parse tree manipulation : change the left operand
    virtual void                SetRight(VeriExpression *expr) { _right = expr ; } // for cases of parse tree manipulation : change the left operand

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy pattern match

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPatternMatch(const VeriPatternMatch &pattern_match, VeriMapForCopy &id_map_table) ; // Copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriPatternMatch(SaveRestore &save_restore) ;

    virtual ~VeriPatternMatch() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriPatternMatch>(this, name) ; }

protected:
    VeriPatternMatch(VeriPatternMatch &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_left(base._left),_right(base._right),_scope(base._scope) {base._left=0 ;base._right=0 ;base._scope=0 ;}

private:
    // Prevent compiler from defining the following
    VeriPatternMatch() ;                                    // Purposely leave unimplemented
    VeriPatternMatch(const VeriPatternMatch &) ;            // Purposely leave unimplemented
    VeriPatternMatch& operator=(const VeriPatternMatch &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_left ; // Left of matches keyword
    VeriExpression *_right ; // Pattern which is to be matched with _left
    VeriScope      *_scope ; // Scope where pattern (_right) specified implicit pattern identifiers are declared
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPatternMatch

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling constraint set
   A VeriConstraintSet implements the construct
        constraint set : <constraint expr>
                       | <constraint set> <constraint expr>
   VIPER #3301: Support bracketed constraint set expressions.
*/
class VFC_DLL_PORT VeriConstraintSet : public VeriExpression
{
public:
    explicit VeriConstraintSet(Array *exprs) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICONSTRAINTSET ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    // Accessor functions
    virtual Array *             GetExpressions() const       { return _exprs ; } // Return the expressions

    // Parse tree copy routines
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const ; // Copy pattern match

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConstraintSet(const VeriConstraintSet &contraint_set, VeriMapForCopy &id_map_table) ; // Copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriConstraintSet(SaveRestore &save_restore) ;

    virtual ~VeriConstraintSet() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriConstraintSet>(this, name) ; }

protected:
    VeriConstraintSet(VeriConstraintSet &base, unsigned use_original_linefile):VeriExpression(base, use_original_linefile),_exprs(base._exprs) {base._exprs=0 ;}

private:
    // Prevent compiler from defining the following
    VeriConstraintSet() ;                                     // Purposely leave unimplemented
    VeriConstraintSet(const VeriConstraintSet &) ;            // Purposely leave unimplemented
    VeriConstraintSet& operator=(const VeriConstraintSet &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                PrettyPrintWithSemi(ostream &f, unsigned level) const { PrettyPrint(f, level) ; }
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Deletes 'old_expr' and puts 'new_expr' in its place

    // Static elaboration
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; } // Evaluate constant expression
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array *_exprs ; // Constraint set expressions
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConstraintSet

/* -------------------------------------------------------------- */

/*
   VIPER #6380 : This class will be created for clocking event used in clocked-sequence
   or as actual to some system functions. So derive it from VeriExpression and hence
   class is moved here from VeriMisc.h
*/
class VFC_DLL_PORT VeriDelayOrEventControl : public VeriExpression
{
public:
    VeriDelayOrEventControl(VeriExpression *delay_control, VeriExpression *repeat_event, Array *event_control) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDELAYOREVENTCONTROL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; // Resolve id references (done after analysis of a module)
    // Variable usage analysis (VIPER #6084)
    virtual void                VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env /*environment*/) { return 0 ; }

    // Accessor methods
    VeriExpression *            GetDelayControl() const     { return _delay_control ; }
    VeriExpression *            GetRepeatEvent() const      { return _repeat_event ; }
    virtual Array *             GetEventControl() const     { return _event_control ; }
    unsigned                    IsCycleDelay()    const     { return _is_cycle_delay ; }
    void                        SetIsCycleDelay()           { _is_cycle_delay = 1 ; }
    virtual unsigned            IsEdge(unsigned edge_type) const ;  // Check if the expression is preceded by a POSEDGE/NEGEDGE edge type, or (by default) any edge type.
    virtual unsigned            GetEdgeToken() const ;     // VERI_POSEDGE, VERI_NEGEDGE or 0 (for both edges).
    virtual VeriExpression *    GetExpr() const ;          // Expression from which the edges are taken.
    virtual VeriExpression *    GetIffCondition() const ;  // A level condition under which the edge is tested. Only set for SV.
    virtual VeriIdDef *         FullId() const ;           // For VeriIdRef expression nodes only.
    virtual const char *        GetName() const ;
    virtual VeriName *          GetPrefix() const ;  // Return prefix name for VeriIndexedId's, VeriSelectedName's, VeriIndexedMemoryId's, etc ...
    virtual unsigned            IsHierName() const ;  // Returns '1' if 'this' is a hierarchical name reference
    virtual unsigned            IsDelayOrEventControl() const { return 1 ; } // Return 1 if this is a VeriDelayOrEventControl

    // Parse tree copy routine :
    VeriDelayOrEventControl *   Copy(VeriMapForCopy &id_map_table)const; // Copy delay or event control
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table) const { return Copy(id_map_table) ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDelayOrEventControl(const VeriDelayOrEventControl &delay_event_control, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDelayOrEventControl(SaveRestore &save_restore) ;

    virtual ~VeriDelayOrEventControl() ;

private:
    // Prevent compiler from defining the following
    VeriDelayOrEventControl() ;                                           // Purposely leave unimplemented
    VeriDelayOrEventControl(const VeriDelayOrEventControl &) ;            // Purposely leave unimplemented
    VeriDelayOrEventControl& operator=(const VeriDelayOrEventControl &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is delay control/repeat event/any event expression, and puts new expression in its place

    // Static elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

    // Internal routines to evaluate expressions with target constraint :
    virtual VeriBaseValue *     StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const { return 0 ; }

protected:
    VeriExpression  *_delay_control ; // Delay expression :  # <expr>
    VeriExpression  *_repeat_event ;  // Repeat expression :   repeat <expr> @...
    Array           *_event_control ; // Array of VeriExpression*. The event expressions : @(<expr1>,<expr2>,<expr3>..).
    unsigned        _is_cycle_delay:1 ; // Viper 4504 Flag to store this is cycle delay
//#*# VERIFIC_END DOCUMENT_INTERNAL

} ; // class VeriDelayOrEventControl
/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_EXPRESSION_H_
