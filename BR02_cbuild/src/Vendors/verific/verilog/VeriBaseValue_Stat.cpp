/*
 *
 * [ File Version : 1.132 - 2014/02/27 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriModuleItem.h"

#include <string.h>
#include <stdio.h>
#include <math.h>    // for floor
#include "Array.h"
#include "VeriId.h"
#include "Map.h"
#include "Set.h"
#include "VeriBaseValue_Stat.h"
#include "VeriUtil_Stat.h"
#include "Strings.h"
#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "VeriExpression.h"
#include "veri_tokens.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

unsigned VeriInteger::GetSign() const
{
    return 1 ;
}

unsigned VeriReal::GetSign() const
{
    return 1 ;
}

unsigned VeriBasedNumber::GetSign() const
{
    unsigned sign = 0 ;
    char* s = strpbrk(_based_num, "sS") ;
    if (s) sign = 1 ;
    return sign ;
}
unsigned VeriEnumVal::GetSign() const
{
    return (_id) ? _id->IsSigned() : 0 ; // Return sign of enumeration literal
}
int VeriBasedNumber::GetIntegerValue() const
{
    unsigned sign = 0 ;
    char* str = GetBinaryValue(&sign) ;
    if (!str || strpbrk(str, "xXzZ")) {
        Strings::free(str) ;
        return 0 ;
    }
    int x = ConstStrEvaluation::BinToInt(str, sign) ;
    Strings::free(str) ;
    return x ;
}
int VeriEnumVal::GetIntegerValue() const
{
    // Value of enum literal is the value of its initial expression
    VeriBaseValue *val = GetValue() ; // Get value of initial expression
    int int_val = val ? val->GetIntegerValue() : 0 ; // Get int equivalent of its initial value
    delete val ;
    return int_val ;
}
char* VeriInteger::GetBinaryValue(unsigned* sign) const
{
    char* str = ConstStrEvaluation::IntToBin(GetIntegerValue()) ;
    if (sign) *sign = 1 ;
    return str ;
}
verific_uint64 VeriInteger::Get64bitUnsigned() const
{
    return ((verific_uint64)GetIntegerValue()) ;
}
verific_int64 VeriInteger::Get64bitInteger() const
{
    return ((verific_int64)GetIntegerValue()) ;
}
verific_uint64 VeriReal::Get64bitUnsigned() const
{
    return ((verific_uint64)VeriNode::RoundToNearestInteger(GetRealValue())) ;
}
verific_int64 VeriReal::Get64bitInteger() const
{
    return ((verific_int64)VeriNode::RoundToNearestInteger(GetRealValue())) ;
}
verific_uint64 VeriBasedNumber::Get64bitUnsigned() const
{
    unsigned sign ;
    char *bin_str = GetBinaryValue(&sign) ;
    verific_uint64 value = ConstStrEvaluation::BinToUnsigned64(bin_str) ;
    Strings::free(bin_str) ;
    return value ;
}
verific_int64 VeriBasedNumber::Get64bitInteger() const
{
    unsigned sign = 0 ;
    char *bin_str = GetBinaryValue(&sign) ;
    verific_int64 value = ConstStrEvaluation::BinToInt64(bin_str, sign) ;
    Strings::free(bin_str) ;
    return value ;
}
int VeriAsciiString::GetIntegerValue() const
{
    unsigned sign = 0 ;
    char *bin_str = GetBinaryValue(&sign) ;

    if (!bin_str || strpbrk(bin_str, "xXzZ")) {
        Strings::free(bin_str) ;
        return atoi(_str) ;
    }
    int x = ConstStrEvaluation::BinToInt(bin_str) ;
    Strings::free(bin_str) ;
    return x ;
}
verific_uint64 VeriAsciiString::Get64bitUnsigned() const
{
    unsigned sign ;
    char *bin_str = GetBinaryValue(&sign) ;
    if (!bin_str || strpbrk(bin_str, "xXzZ")) {
        Strings::free(bin_str) ;
        return ((verific_uint64)GetRealValue()) ;
    }
    verific_uint64 value = ConstStrEvaluation::BinToUnsigned64(bin_str) ;
    Strings::free(bin_str) ;
    return value ;
    //return ((verific_uint64)GetRealValue()) ;
}
verific_int64 VeriAsciiString::Get64bitInteger() const
{
    unsigned sign = 0 ;
    char *bin_str = GetBinaryValue(&sign) ;
    if (!bin_str || strpbrk(bin_str, "xXzZ")) {
        Strings::free(bin_str) ;
        return ((verific_int64)GetRealValue()) ;
    }
    verific_int64 value = ConstStrEvaluation::BinToInt64(bin_str, sign) ;
    Strings::free(bin_str) ;
    return value ;
    //return ((verific_int64)GetRealValue()) ;
}
double VeriAsciiString::GetRealValue() const
{
    if (GetSign()) return (double)Get64bitInteger() ;
    unsigned sign ;
    char *bin_str = GetBinaryValue(&sign) ;
    if (!bin_str || strpbrk(bin_str, "xXzZ")) {
        Strings::free(bin_str) ;
        return atof(_str) ;
    }
    double value = ConstStrEvaluation::BinToUnsignedDouble(bin_str, 0) ;
    Strings::free(bin_str) ;
    return value ;
}
verific_uint64 VeriMinTypMaxValue::Get64bitUnsigned() const
{
    return (_typ) ? _typ->Get64bitUnsigned() : 0 ;
}
verific_int64 VeriMinTypMaxValue::Get64bitInteger() const
{
    return (_typ) ? _typ->Get64bitInteger() : 0 ;
}
verific_uint64 VeriArrayValue::Get64bitUnsigned() const
{
    unsigned sign ;
    char *bin_str = GetBinaryValue(&sign) ;
    verific_uint64 value = ConstStrEvaluation::BinToUnsigned64(bin_str) ;
    Strings::free(bin_str) ;
    return value ;
}
verific_int64 VeriArrayValue::Get64bitInteger() const
{
    unsigned sign = 0 ;
    char *bin_str = GetBinaryValue(&sign) ;
    verific_int64 value = ConstStrEvaluation::BinToInt64(bin_str, sign) ;
    Strings::free(bin_str) ;
    return value ;
}
verific_uint64 VeriEnumVal::Get64bitUnsigned() const
{
    unsigned sign ;
    char *bin_str = GetBinaryValue(&sign) ;
    verific_uint64 value = ConstStrEvaluation::BinToUnsigned64(bin_str) ;
    Strings::free(bin_str) ;
    return value ;
}
verific_int64 VeriEnumVal::Get64bitInteger() const
{
    unsigned sign = 0 ;
    char *bin_str = GetBinaryValue(&sign) ;
    verific_int64 value = ConstStrEvaluation::BinToInt64(bin_str, sign) ;
    Strings::free(bin_str) ;
    return value ;
}
char *VeriBasedNumber::GetBinaryValue(unsigned *sign) const
{
    char *str = ConstStrEvaluation::VeriStringToBinary(_based_num, sign) ;
    return str ;
}

char *VeriAsciiString::GetBinaryValue(unsigned *sign) const
{
    char *str = ConstStrEvaluation::AsciiToBin(_str, _size) ;
    if (sign) *sign = 0 ;
    return str ;
}
char *VeriEnumVal::GetBinaryValue(unsigned *sign) const
{
    // Value of enum literal is the value of its initial expression
    VeriBaseValue *val = GetValue() ; // Get value of initial expression
    char *bin_val = val ? val->GetBinaryValue(sign) : 0 ; // Get binary equivalent of its initial value
    delete val ;
    return bin_val ;
}

VeriBaseValue::VeriBaseValue() {}
VeriBaseValue::~VeriBaseValue() {}

VeriInteger::VeriInteger(int i) :
    VeriBaseValue(),
    _ival(0)
{
    _ival = Strings::itoa(i) ;
}

VeriInteger::VeriInteger(const char *num) :
    VeriBaseValue(),
    _ival(0)
{
    _ival = Strings::save(num) ;
}

VeriInteger::~VeriInteger()
{
    Strings::free(_ival) ;
    _ival = 0 ;
}

VeriReal::VeriReal(const char *d) :
    VeriBaseValue(),
    _real(0)
{
    _real = Strings::save(d) ;
}

// Here some treatments are done so that the accuracy is not lost.

VeriReal::VeriReal(double d) :
    VeriBaseValue(),
    _real(0)
{
    static char data [512] ;
    sprintf (data, "%-100.75g", d) ;
    if (strchr (data, '.') == 0) {
        char* last_loc = strchr (data, ' ') ;
        if (last_loc) {
            *last_loc = '.' ;
            *(last_loc+1) = '0' ;
            *(last_loc+2) = '\0' ;
        }
    } else {
        char* last_loc = strchr (data, ' ') ;
        if (last_loc) *(last_loc) = '\0' ;
    }
    _real = Strings::save(data) ;
}

VeriReal::~VeriReal()
{
    Strings::free(_real) ;
    _real = 0 ;
}
double VeriReal::GetRealValue() const
{
    double zero = 0.0 ; // Avoid Windows build error: use a variable
    if (strncmp(_real, "inf", 3)==0) {
        // This is a +inf value, return so, do no call strtod which make create
        // MEMORY CORRUPTION as: electra_extra_tests/code_coverage/test315
        return (1.0/zero) ; // Any +ve number divided by 0 (real domain) is +inf
    } else if (strncmp(_real, "-inf", 4)==0) {
        // This is a -inf value, return so, do no call strtod which make create
        // MEMORY CORRUPTION as: VIPER #5248/18Dec_issues/test63
        return (-1.0/zero) ; // Any -ve number divided by 0 (real domain) is -inf
    } else if (strncmp(_real, "nan", 3)==0) {
        // This is Not A Number, return so, do no call strtod:
        return (0.0/zero) ; // 0 divided by 0 (real domain) is NaN
    }

    char *end_val ;
    double d = strtod(_real, &end_val) ;
    if (end_val) {
        switch(*end_val) {
        case 'T': d = d*1e12 ; break ;
        case 'G': d = d*1e9 ; break ;
        case 'M': d = d*1e6 ; break ;
        case 'K': d = d*1e3 ; break ;
        case 'k': d = d*1e3 ; break ;
        case 'm': d = d*1e-3 ; break ;
        case 'u': d = d*1e-6 ; break ;
        case 'n': d = d*1e-9 ; break ;
        case 'p': d = d*1e-12 ; break ;
        case 'f': d = d*1e-15 ; break ;
        case 'a': d = d*1e-18 ; break ;
        default: break ;
        }
    }
    return d ;
}

int VeriReal::GetIntegerValue() const
{
    return (int)VeriNode::RoundToNearestInteger(GetRealValue()) ;
}

VeriBasedNumber::VeriBasedNumber(const char *x) :
    VeriBaseValue(),
    _based_num(0)
{
    _based_num = Strings::save(x) ;
}

VeriBasedNumber::VeriBasedNumber(verific_uint64 val) :
    VeriBaseValue(),
    _based_num(0)
{
    char *bin_str = ConstStrEvaluation::Unsigned64ToBin(val) ;
    _based_num = Strings::save("64'b", bin_str) ;
    Strings::free(bin_str) ;
}

VeriBasedNumber::~VeriBasedNumber()
{
    Strings::free(_based_num) ;
    _based_num = 0 ;
}

double VeriBasedNumber::GetRealValue() const
{
    unsigned sign = 0 ;
    char* str = GetBinaryValue(&sign) ;
    if (!str || strpbrk(str, "xXzZ")) {
        Strings::free(str) ;
        return 0 ;
    }
    //if (GetSign()) return (double)Get64bitInteger() ;
    //unsigned sign ;
    //char *bin_str = GetBinaryValue(&sign) ;
    double value = ConstStrEvaluation::BinToUnsignedDouble(str, sign) ;
    Strings::free(str) ;
    return value ;
}

VeriAsciiString::VeriAsciiString(const char* x) :
    VeriBaseValue(),
    _str(0),
    _size(0)
{
    _str = Strings::save(x) ;
    _size = Strings::len(_str) ;
}
// VIPER #7397 : New constructor to store string length in value
VeriAsciiString::VeriAsciiString(const char* x, unsigned len) :
    VeriBaseValue(),
    _str(0),
    _size(len)
{
    VERIFIC_ASSERT(len >= Strings::len(x)) ;
    _str = Strings::allocate(len+1) ;
    unsigned i ;
    for (i=0; i < len; i++) {
        _str[i] = x[i] ;
    }
    _str[i] = '\0' ;
}
VeriAsciiString::~VeriAsciiString()
{
    Strings::free(_str) ;
    _str = 0 ;
}

VeriEnumVal::VeriEnumVal(VeriIdDef *id):
    VeriBaseValue(),
    _id(id)
{ }
VeriEnumVal::~VeriEnumVal() { _id = 0 ; }

double VeriEnumVal::GetRealValue() const
{
    // Value of enum literal is the value of its initial expression
    VeriBaseValue *val = GetValue() ; // Get value of initial expression
    double real_val = val ? val->GetRealValue() : 0 ; // Get real value
    delete val ;
    return real_val ;
}
const char * VeriEnumVal::GetString() const
{
    // Value of enum literal is the value of its initial expression
    // Do not get the string from the value, deleting the value corrupts the string:
    // See test case: code_coverage/test478
    //VeriBaseValue *val = GetValue() ; // Get value of initial expression
    //const char *str_val = val ? val->GetString() : 0 ; // Get real value
    //delete val ;
    // CHECKME: Currently it does not regress on any design we have, may have to check:
    const char *str_val = (_id) ? _id->Name() : 0 ; // Get the name of the id instead
    return str_val ;
}
VeriBaseValue * VeriEnumVal::GetValue() const
{
    if (!_id) return 0 ;
    // Value can be obtained from its initial value
    VeriExpression *init_val = _id->GetInitialValue() ; // Get initial value of this literal
    if (!init_val) return 0 ;
    int size_sign = (int)_id->StaticSizeSignInternal(0, 0) ;
    VeriBaseValue *val = init_val->StaticEvaluateInternal(size_sign, 0, 0, 0) ; // Evaluate the value

    if (val && val->Type() == INTEGER) {
        unsigned id_size = 0 ;
        unsigned id_sign = (size_sign < 0) ;
        // Looks like this is not required and may yield incorrect result:
        //if (_id->IsArray()) {
        //    int msb = 0, lsb = 0 ;
        //    id_size = _id->GetPackedWidth(&msb, &lsb) ;
        //} else {
            int ini_val_ss = (int)init_val->StaticSizeSignInternal(0, 0) ;
            id_size = (unsigned)ABS(ini_val_ss) ;
        //}
        unsigned b_sign = 0 ;
        char *str = val->GetBinaryValue(&b_sign) ;
        if (str) {
            unsigned len = (unsigned)Strings::len(str) ;
            // Get proper value of enum id giving precedence to range size
            if (len != id_size) {
                char *tmp = str ;
                str = ConstStrEvaluation::GetPaddedString(str, id_size, id_sign) ;
                Strings::free(tmp) ;
                len = (unsigned)Strings::len(str) ;
                char *prefix = Strings::itoa((int)id_size) ;
                char *val_str = Strings::save(prefix, (id_sign) ? "'sb" : "'b", str) ;
                delete val ;
                val = new VeriBasedNumber(val_str) ;
                Strings::free(val_str) ;
                Strings::free(prefix) ;
            }
            unsigned ret_size = GET_CONTEXT_SIZE(size_sign) ;
            unsigned sign = GET_CONTEXT_SIGN(size_sign) ;
            if (len != ret_size) {
                char *tmp = str ;
                str = ConstStrEvaluation::GetPaddedString(str, ret_size, sign) ;
                Strings::free(tmp) ;
                len = (unsigned)Strings::len(str) ;
                char *prefix = Strings::itoa((int)ret_size) ;
                char *val_str = Strings::save(prefix, (sign) ? "'sb" : "'b", str) ;
                delete val ;
                val = new VeriBasedNumber(val_str) ;
                Strings::free(val_str) ;
                Strings::free(prefix) ;
            }
        }
        Strings::free(str) ;
    }
    return val ;
}

unsigned VeriReal::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (REAL != val->Type())) return 0 ;
    if (do_not_check_type && val->HasXZ()) return 0 ; // Real never matches with x/z
    // FIXME: Should do: fabs(GetRealValue()-val->GetRealValue()) < 0.0001
    return (GetRealValue() == val->GetRealValue()) ;
}

unsigned VeriInteger::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (INTEGER != val->Type())) return 0 ;
    if (do_not_check_type && val->HasXZ()) return 0 ; // Integer never matches with x/z
    return (GetIntegerValue() == val->GetIntegerValue()) ;
}

unsigned VeriBasedNumber::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (BASEDNUM != val->Type())) return 0 ;
    unsigned this_sign = 0 ;
    unsigned val_sign = 0 ;
    // Don't just compare the strings, compare their binary value.
    // GetString() returns the actual based number string like 4'sb10.
    //return (Strings::compare(GetString(), val->GetString())) ;
    char *this_str = GetBinaryValue(&this_sign) ;
    char *val_str = val->GetBinaryValue(&val_sign) ;
    if (do_not_check_type) {
        // VIPER #3280: Adjust the size to the max-size of the two:
        unsigned this_len = Strings::len(this_str) ;
        unsigned val_len = Strings::len(val_str) ;
        if (this_len > val_len) {
            char *new_val_str = ConstStrEvaluation::GetPaddedString(val_str, this_len, val_sign) ;
            Strings::free(val_str) ;
            val_str = new_val_str ;
        } else if (val_len > this_len) {
            char *new_this_str = ConstStrEvaluation::GetPaddedString(this_str, val_len, this_sign) ;
            Strings::free(this_str) ;
            this_str = new_this_str ;
        }
    }
    unsigned ret_val = Strings::compare(this_str, val_str) ;
    Strings::free(val_str) ;
    Strings::free(this_str) ;
    // Sign does not matter: (2'b10 == 2'sb10) is true!
    return ret_val ;
}

unsigned VeriAsciiString::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (ASCIISTR != val->Type())) return 0 ;
    //return (Strings::compare(GetString(), val->GetString())) ;
    // VIPER #7397: string can have '\0', so do not use Strings::compare
    unsigned other_len = val->GetStringSize() ;
    if (_size != other_len) return 0 ; // Length mismatch
    const char *other_str = val->GetString() ;
    if (!other_str) return 0 ;
    unsigned i ;
    for (i =0 ; i < _size; i++) {
        if (_str[i] != other_str[i]) return 0 ; // Not same
    }
    return 1 ; // Equal
}

unsigned VeriEnumVal::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (ENUM != val->Type())) return 0 ; // values are not equal
    return (_id == val->GetEnumId()) ;
}
unsigned VeriMinTypMaxValue::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (MINTYPMAX != val->Type())) return 0 ;

    int match_count = 0 ;
    if (_min && _min->IsEqual(val->GetMinimum(), do_not_check_type)) match_count++ ;
    if (_typ && _typ->IsEqual(val->GetTypical(), do_not_check_type)) match_count++ ;
    if (_max && _max->IsEqual(val->GetMaximum(), do_not_check_type)) match_count++ ;

    if (match_count == 3) return 1 ;
    return 0 ;
}
unsigned VeriArrayValue::IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const
{
    if (!val) return 0 ;
    if (!do_not_check_type && (ARRAY != val->Type())) return 0 ;

    Array *other_values = val->GetValues() ;
    if (_values && other_values && (_values->Size() != other_values->Size())) return 0 ;
    unsigned i ;
    VeriBaseValue *val1, *val2 ;
    FOREACH_ARRAY_ITEM(_values, i, val1) {
        val2 = other_values ? (VeriBaseValue*)other_values->At(i) : 0 ;
        if (val1 && !val1->IsEqual(val2, do_not_check_type)) return 0 ;
    }
    return 1 ;
}
VeriMinTypMaxValue::VeriMinTypMaxValue(VeriBaseValue *min, VeriBaseValue *typ, VeriBaseValue *max) :
    VeriBaseValue(),
    _min(0),
    _typ(0),
    _max(0)
{
    if (min) _min = min->Copy() ;
    if (typ) _typ = typ->Copy() ;
    if (max) _max = max->Copy() ;
}

VeriMinTypMaxValue::VeriMinTypMaxValue(VeriBaseValue *val) :
    VeriBaseValue(),
    _min(0),
    _typ(0),
    _max(0)
{
    if (val) {
        _min = val->Copy() ;
        _typ = val->Copy() ;
        _max = val->Copy() ;
    }
}

VeriMinTypMaxValue::~VeriMinTypMaxValue()
{
    delete _min ;
    delete _typ ;
    delete _max ;
}

VeriBaseValue* VeriMinTypMaxValue::Copy()
{
    return new VeriMinTypMaxValue(_min, _typ, _max) ;
}

int VeriMinTypMaxValue::GetIntegerValue() const
{
    return (_typ ? _typ->GetIntegerValue(): 0) ;
}

char* VeriMinTypMaxValue::GetBinaryValue(unsigned* b_sign) const
{
    return (_typ ? _typ->GetBinaryValue(b_sign): 0) ;
}

double VeriMinTypMaxValue::GetRealValue()const
{
    return (_typ ? _typ->GetRealValue(): 0) ;
}

unsigned VeriMinTypMaxValue::GetSign()const
{
    return _typ ? _typ->GetSign(): 0 ;
}
VeriArrayValue::VeriArrayValue(Array *vals, unsigned is_concat) :
    VeriBaseValue(),
    _values(vals),
    _is_concat(is_concat),
    _is_struct(0)
{ }
VeriArrayValue::~VeriArrayValue()
{
    unsigned i ;
    VeriBaseValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) delete val ;
    delete _values ;
}
VeriBaseValue *VeriArrayValue::Copy()
{
    Array *new_vals = 0 ;
    if (_values) {
        new_vals = new Array(_values->Size()) ;
        unsigned i ;
        VeriBaseValue *val ;
        FOREACH_ARRAY_ITEM(_values, i, val) new_vals->InsertLast(val ? val->Copy() : 0) ;
    }
    VeriBaseValue *r = new VeriArrayValue(new_vals, _is_concat) ;
    if (_is_struct) r->SetIsStructure() ;
    return r ;
}
// VIPER #2992
// Get the binary equivalent of the elements of the array.
char *VeriArrayValue::GetBinaryValue(unsigned *sign) const
{
    if (!_values) return 0 ;

    unsigned status = 1 ;
    char *str = 0 ;
    unsigned i ;
    VeriBaseValue *ele = 0 ;
    char *val_str = Strings::save("") ;
    FOREACH_ARRAY_ITEM(_values, i, ele) {
        // Evaluate each expression.
        str = ele ? ele->GetBinaryValue(sign) : 0 ;
        if (!str) {
            //Error("%s is not a constant", "expression") ;
            status = 0 ;
            break ;
        }
        char *tmp = val_str ;
        val_str = Strings::save(val_str, str) ;
        Strings::free(tmp) ;
        Strings::free(str) ;
    }

    if (status == 1) return val_str ;

    Strings::free(val_str) ;
    return 0 ;
}
// VIPER #2992
// Get the integer equivalent of the elements of the array.
int VeriArrayValue::GetIntegerValue() const
{
    unsigned sign = 0 ;
    char* str = GetBinaryValue(&sign) ;
    if (!str || strpbrk(str, "xXzZ")) {
        Strings::free(str) ;
        return 0 ;
    }
    int x = ConstStrEvaluation::BinToInt(str) ;
    Strings::free(str) ;
    return x ;
}
VeriExpression *VeriInteger::ToExpression(const VeriTreeNode *from) const
{
    return ToConstVal(from) ;
}
// Create VeriConst from VeriBaseValue : viper 2056
VeriConst *VeriInteger::ToConstVal(const VeriTreeNode *from) const
{
    VeriConst *val = new VeriIntVal(GetIntegerValue()) ;
    if (from) val->SetLinefile(from->Linefile()) ;
    return val ;
}

VeriExpression *VeriReal::ToExpression(const VeriTreeNode *from) const
{
    return ToConstVal(from) ;
}
VeriConst *VeriReal::ToConstVal(const VeriTreeNode *from) const
{
    VeriConst *val = new VeriRealVal(GetRealValue()) ;
    if (from) val->SetLinefile(from->Linefile()) ;
    return val ;
}

VeriExpression *VeriBasedNumber::ToExpression(const VeriTreeNode *from) const
{
    return ToConstVal(from) ;
}
VeriConst *VeriBasedNumber::ToConstVal(const VeriTreeNode *from) const
{
    unsigned sign = 0 ;
    char *bin_val = GetBinaryValue(&sign) ;
    char *base_bin = sign ? Strings::save("'sb", bin_val) : Strings::save("'b", bin_val) ;
    VeriConst *val = new VeriConstVal(base_bin, VERI_BASED_NUM, (unsigned)Strings::len(bin_val)) ;
    Strings::free(base_bin) ;
    Strings::free(bin_val) ;
    if (from) val->SetLinefile(from->Linefile()) ;
    return val ;
}
VeriExpression *VeriAsciiString::ToExpression(const VeriTreeNode *from) const
{
    return ToConstVal(from) ;
}
VeriConst *VeriAsciiString::ToConstVal(const VeriTreeNode *from) const
{
    // VIPER #5107: Convert the string to printable Verilog format:
    //char *my_str = Strings::save(_str) ;
    //char *val_str = VeriNode::MakeVerilogPrintable(my_str) ;
    // Create the const-val with this modified string:
    //VeriConst *val = new VeriConstVal(val_str, VERI_STRING) ;
    // VIPER #7397: Use 'Image()' to create 'VeriConstVal'
    char *image = Image() ;
    VeriConst *val = new VeriConstVal(image, VERI_STRING) ;
    Strings::free(image) ;
    if (from) val->SetLinefile(from->Linefile()) ;
    return val ;
}
VeriExpression *VeriMinTypMaxValue::ToExpression(const VeriTreeNode *from) const
{
    VeriExpression *min_expr = _min ? _min->ToExpression(from) : 0 ;
    VeriExpression *typ_expr = _typ ? _typ->ToExpression(from) : 0 ;
    VeriExpression *max_expr = _max ? _max->ToExpression(from) : 0 ;

    VeriExpression *min_typ_max_expr = new VeriMinTypMaxExpr(min_expr, typ_expr, max_expr) ;
    if (from) min_typ_max_expr->SetLinefile(from->Linefile()) ;
    return min_typ_max_expr ;
}
VeriConst *VeriMinTypMaxValue::ToConstVal(const VeriTreeNode * /* from */) const
{
    return 0 ;
}
VeriExpression* VeriArrayValue::ToExpression(const VeriTreeNode *from) const
{
    if (!_values) return 0 ;
    Array *exprs = new Array(_values->Size()) ;
    unsigned i ;
    VeriBaseValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) {
        exprs->InsertLast(val ? val->ToExpression(from) : 0 ) ;
    }
    VeriExpression *ret_val = 0 ;
    // If the contents of VeriArrayValue is/are VeriConcat then return a VeriConcat element
    if (_is_concat) {
        ret_val = new VeriConcat(exprs) ;
    } else {
        // If the contents of VeriArrayValue is/are not VeriConcat then return a VeriAssignmentPattern element
        ret_val = new VeriAssignmentPattern(0, exprs) ;
    }
    if (from) ret_val->SetLinefile(from->Linefile()) ;
    return ret_val ;
}
VeriConst *VeriArrayValue::ToConstVal(const VeriTreeNode * /* from */) const
{
    return 0 ;
}
VeriExpression *VeriEnumVal::ToExpression(const VeriTreeNode *from) const
{
    VeriIdRef *ref_val = new VeriIdRef(_id) ;
    if (from) ref_val->SetLinefile(from->Linefile()) ;
    return ref_val ;
}
VeriConst *VeriEnumVal::ToConstVal(const VeriTreeNode *from) const
{
    // Value of enum literal is the value of its initial expression
    VeriBaseValue *val = GetValue() ; // Get value of initial expression
    // FIXME : Is it be constant value of this enumeration literal
    VeriConst *const_val = val ? val->ToConstVal(from) : 0 ; // Get const equivalent of the value
    delete val ;
    return const_val ;
}
ValueTable::ValueTable() :
    _val_table(0),
    _block_id_stack(0),
    _disabled_block_id(0),
    _jump_token(0),
    _evaluated_indexes(0),
    _inside_blocking_assign(0)
{
    _val_table = new Map(POINTER_HASH) ;
}

ValueTable::~ValueTable()
{
    MapIter i ;
    VeriBaseValue *val = 0 ;
    FOREACH_MAP_ITEM(_val_table, i, 0, &val) delete val ;
    delete _val_table ;
    delete _block_id_stack ;
    _disabled_block_id = 0 ;

    // Delete all the evaluated indexes:
    ClearAllEvaluatedIndexes() ;
}

// This method inserts a VeriBaseValue against a VeriIdDef.

unsigned ValueTable::Insert(VeriIdDef *id, VeriBaseValue *val)
{
    if (!id || !_val_table) return 0 ;

    VeriBaseValue *insert_val = 0 ;
    if (!val) {
        // VIPER #4310: Support for multi-dimensional array in constant function:
        // Create value for any variable from constraint:
        VeriConstraint *constraint = id->EvaluateConstraintInternal(this, 0, 0) ;
        if (!constraint) {
            return 0 ;
        }
        insert_val = constraint->CreateBaseValue(id->Sign()) ;
        delete constraint ;
    } else {
        if ((val->Type() == REAL) || (id->Type() == VERI_STRINGTYPE)
                || (val->GetEnumId())
                || (val->Type() == ARRAY)
             ) {
            insert_val = val->Copy() ;
        } else {
            unsigned b_sign = 0 ;
            unsigned i ;
            unsigned lidx = 0 ;
            unsigned ridx = 0 ;
            unsigned size = 0 ;
            unsigned id_sign = 0 ;

            int tmp = (int)id->StaticSizeSignInternal(0, 0) ;
            size = (unsigned)ABS(tmp) ;
            id_sign = (tmp < 0) ? 1 : 0 ;
            lidx = (size > 1) ? size - 1 : 0 ;
            ridx = 0 ;

            char *rstr = val->GetBinaryValue(&b_sign) ;
            unsigned val_size = Strings::len(rstr) ;
            if (rstr && size && (size > val_size)) {
                // VIPER #7119: We will apply signing of the id on the value with CreateValue(),
                // so, adjust the size of the value with max size of id and value:
                char *tmp2 = rstr ;
                rstr = ConstStrEvaluation::GetPaddedString(rstr, size, b_sign) ;
                Strings::free(tmp2) ;
            }

            char *lstr = Strings::allocate(((unsigned long)(size + 1))*sizeof(char)) ;
            lstr[size] = '\0' ;
            for(i = 0; i < size; i++) lstr[i] = '0' ;

            insert_val = CreateValue(lstr, rstr, lidx, ridx, lidx, ridx, id_sign) ;
            Strings::free(lstr) ;
            Strings::free(rstr) ;
        }
    }
    if (insert_val){
        VeriBaseValue *old_val = (VeriBaseValue*) (_val_table->GetValue(id)) ;
        VeriTypeInfo *type = id->CreateType(0) ;
        VeriTypeInfo *base_type = (type) ? type->BaseType(): 0 ;
        // The value to be assigned to identifier may have different sign or state(2/4 state),
        // Impose the sign/state of identifier to the value. If identifier is 2 state, convert
        // any x/z present in value to 0:
        unsigned is_2_state = 0 ;
        if (base_type && base_type->Is2State()) is_2_state = 1 ;
        if (base_type) insert_val->ImposeSignState(base_type->IsSigned(), is_2_state) ;
        delete type ;
        (void) _val_table->Insert(id, insert_val, 1) ;
        // Remove the old value if present
        delete old_val ;
    }
    return 1 ;
}

// Function Name: ValueTable::CreateValue
// Arguments  : 1) char* lstr - The value of the id modified
//              2) char* rstr - The value that the id is to be modified with
//              3) int lidx   - The left index of the sliced id where the modification is to be done
//              4) int ridx   - The right index of the id where the modification is to be done
//              5) int msb    - The msb of the Range having value lstr
//              6) int lsb    - The lsb of the Range having value lstr
// Return Type: VeriBaseValue*
// Description: The function takes a string lstr and modifies a part of the string with rstr.
//              The location of the modification is determined by lidx, ridx, msb, and lsb values.
//              For Example:
//                reg [2:0] r = 3'b0 ;
//                r[1] = 1'b1 ;
//              Here r is to be modified from 000 to 010. So CreateValue will be called with lstr = "000"
//              and rstr = "1". The new value of lstr will be "010".
VeriBaseValue *ValueTable::CreateValue(char *lstr, const char *rstr, unsigned lidx, unsigned ridx, unsigned msb, unsigned lsb, unsigned sign) const
{
    if (!lstr || !rstr) return 0 ;

    (void) msb ; // Prevent "unused" compile warning

    VeriBaseValue *ret_value = 0 ;
    int i = 0 ;

    unsigned length = (unsigned)Strings::len(lstr) ;
    if (!length) return 0 ;
    length = length - 1 ;

    int idx = (int)(Strings::len(rstr) - 1) ;

    if (lidx < ridx) {
        VERIFIC_ASSERT(msb < lsb) ;
        for(i = (int)ridx; i >= (int)lidx; i--) {
            if (idx >= 0) {
                lstr[length - (lsb - (unsigned)i)] = rstr[idx] ;
            } else {
                break ;
            }
            idx-- ;
        }
    } else if(lidx > ridx) {
        VERIFIC_ASSERT(msb > lsb) ;
        int act_idx = (int)(length - (ridx -lsb)) ;
        for(i = (int)ridx; i <= (int)lidx; i++) {
            if (idx >= 0) {
                lstr[act_idx] = rstr[idx] ;
            } else {
                break ;
            }
            act_idx-- ;
            idx-- ;
        }
    } else if (lidx==ridx) { // For eg. when we have x[10] = in[0] ; then lidx=ridx=10 // VIPER 2492
        if (msb>=lsb) lstr[length - (ridx -lsb)] = rstr[idx] ;
        if (msb<lsb) lstr[length - (lsb - (unsigned)ridx)] = rstr[idx] ;
    }

    char *prefix = Strings::itoa((int) Strings::len(lstr)) ;
    char* valstr = Strings::save(prefix, (sign ? "'sb" : "'b"), lstr) ;
    ret_value = new VeriBasedNumber(valstr) ;
    Strings::free(valstr) ;
    Strings::free(prefix) ;

    return ret_value ;
}

// Returns the VeriBaseValue correspoding to the entry 'id' in the Map.
VeriBaseValue* ValueTable::FetchValue(const VeriIdDef *id) const
{
    return ((_val_table) ? (VeriBaseValue*) _val_table->GetValue(id) : 0) ;
}

unsigned ValueTable::RemoveValue(const VeriIdDef *id)
{
    if (!_val_table || !id) return 0 ;

    // Remove the value of this id from the Map after getting it:
    VeriBaseValue *value = (VeriBaseValue *)_val_table->GetValue(id) ;
    unsigned result = _val_table->Remove(id) ;
    delete value ; // Delete the value, we created it

    return result ;
}

void
ValueTable::PushBlockId(const VeriIdDef *id)
{
    if (!id) return ;

    if (!_block_id_stack) _block_id_stack = new Set(POINTER_HASH) ;
    (void) _block_id_stack->Insert(id) ;
}

void
ValueTable::PopBlockId(const VeriIdDef *id)
{
    if (!id || !_block_id_stack) return ;

    (void) _block_id_stack->Remove(id) ;

    // Check if this is the block which was disabled, enable it:
    if (_disabled_block_id == id) _disabled_block_id = 0 ;
}

void
ValueTable::DisableBlock(const VeriIdDef *id)
{
    // Cannot disable tasks in static elaboration, only disable blocks:
    if (!id || !id->IsBlock()) return ;

    // Check whether we are actually inside the block we need to disable.
    // We cannot disable block if we are within the block.
    if (!_block_id_stack || !_block_id_stack->GetItem(id)) return ;

    // Disable this block from now on:
    _disabled_block_id = id ;
}

// VIPER #2482, #3511: Assignment operator (like &=) support with indexed operands having
// operators with side effects (like ++/--) in the index. Index should be evaluated only once:
void
ValueTable::InsertEvaluatedIndexValue(const VeriExpression *expr, VeriBaseValue *val)
{
    if (!expr || !val) return ;
    if (!_inside_blocking_assign) return ; // Only accept inside blocking assignment

    if (!_evaluated_indexes) _evaluated_indexes = new Map(POINTER_HASH) ;

    if (_evaluated_indexes->GetItem(expr)) return ; // Already there
    (void) _evaluated_indexes->Insert(expr, val->Copy()) ;
}

VeriBaseValue *
ValueTable::GetEvaluatedIndexValue(const VeriExpression *expr) const
{
    if (!expr || !_evaluated_indexes) return 0 ;

    VeriBaseValue * val = (VeriBaseValue *)_evaluated_indexes->GetValue(expr) ;
    return (val) ? val->Copy() : 0 ;
}

void
ValueTable::ClearAllEvaluatedIndexes()
{
    MapIter mi ;
    VeriBaseValue *val ;
    FOREACH_MAP_ITEM(_evaluated_indexes, mi, 0, &val) delete val ;
    delete _evaluated_indexes ;
    _evaluated_indexes = 0 ;
}

VeriBaseValue *VeriConstraint::CreateBaseValue(unsigned sign) const
{
     unsigned size = NumOfBits() ;
     // VIPER #4496 : Do not proceed for 0 size constraint
     if (size == 0) return 0 ; // constraint of size 0, return 0
     // Create 'x' of required 'size'
     char *str = Strings::allocate(((unsigned long)(size + 1))* sizeof(char)) ;
     str[size] = '\0' ;
     unsigned i ;
     for(i = 0; i < size; i++) str[i] = 'x' ;
     char *prefix = Strings::itoa((int)size) ;
     char* valstr = Strings::save(prefix, (sign ? "'sb" : "'b"), str) ;
     VeriBaseValue *val = new VeriBasedNumber(valstr) ;
     Strings::free(valstr) ;
     Strings::free(prefix) ;
     Strings::free(str) ;
     return val ;
}
VeriBaseValue *VeriScalarTypeConstraint::CreateBaseValue(unsigned sign) const
{
    if (_type == VERI_STRINGTYPE) {
        return new VeriAsciiString("", 0) ;
    }
    return VeriConstraint::CreateBaseValue(sign) ;
}
VeriBaseValue *VeriEnumConstraint::CreateBaseValue(unsigned sign) const
{
    return VeriConstraint::CreateBaseValue(sign) ;
}
VeriBaseValue *VeriRangeConstraint::CreateBaseValue(unsigned /*sign*/) const { return 0 ; }
VeriBaseValue *VeriArrayConstraint::CreateBaseValue(unsigned sign) const
{
    // For packed array, create 'x' of array size
    if (IsPacked()) {
        return VeriConstraint::CreateBaseValue(sign) ;
    }
    VeriBaseValue *ret_val = 0 ;
    unsigned length = Length() ;
    if (!length) return 0 ; // Cannot create value of size 0

    // Create array value of length 'length'. Each element of the array will be
    // value for that element
    unsigned i ;
    VeriBaseValue *ele_val = _element_constraint ? _element_constraint->CreateBaseValue(0): 0 ;
    Array *arr = new Array(length) ;
    VeriBaseValue *v ;
    for (i = 0; i < length; i++) {
        if (!i) {
            v = ele_val ;
        } else {
            v = ele_val ? ele_val->Copy(): 0 ;
        }
        arr->InsertLast(v) ;
    }
    ret_val = new VeriArrayValue(arr, 0) ;
    return ret_val ;
}
VeriBaseValue *VeriRecordConstraint::CreateBaseValue(unsigned /*sign*/) const
{
    if (!_element_constraints) return 0 ; // No element, cannot create value
    VeriBaseValue *ret_val = 0 ;

    // Create an array value for this structure/union. Each element will represent
    // each element of structure/union
    Array *arr = new Array(_element_constraints->Size()) ;
    unsigned i ;
    VeriConstraint *element_constraint ;
    VeriBaseValue *ele_val ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
        ele_val = (element_constraint) ? element_constraint->CreateBaseValue(0): 0 ;
        arr->InsertLast(ele_val) ;
    }
    ret_val = new VeriArrayValue(arr, 0) ;
    ret_val->SetIsStructure() ;
    return ret_val ;
}
char *VeriInteger::Image() const
{
    return Strings::save(_ival) ;
}
char *VeriReal::Image() const
{
    return Strings::save(_real) ;
}
char *VeriBasedNumber::Image() const
{
    return Strings::save(_based_num) ;
}
char *VeriAsciiString::Image() const
{
    unsigned len = _size  ;
    char *result = Strings::allocate(len+3) ; // Allocate more spaces for enclosing quotes
    result[len+2] = '\0' ;
    unsigned tmp_len = len+3 ;
    char r ;
    unsigned i ;
    unsigned idx = 0 ;
    // VeriConstVal has special handling for "\0" and "\\", so here
    // convert one character '\0' and '\\' to two characters '\' and '0' or '\'
    for (i=0; i < len; i++) {
        r = _str[i] ;
        switch (r) {
        case '\0' :
        {
            result[idx] = '\0' ;
            char *tmp = result ;
            result = Strings::allocate(tmp_len+1) ;
            tmp_len = tmp_len + 1 ;
            memcpy(result, tmp, idx+1) ;
            result[idx++] = '\\' ;
            result[idx++] = '0' ;
            Strings::free(tmp) ;
            break ;
        }
        case '\\' :
        {
            result[idx] = '\0' ;
            char *tmp = result ;
            result = Strings::allocate(tmp_len+1) ;
            tmp_len = tmp_len + 1 ;
            memcpy(result, tmp, idx+1) ;
            result[idx++] = '\\' ;
            result[idx++] = '\\' ;
            Strings::free(tmp) ;
            break ;
        }
        default :
        {
            result[idx++] = r ;
            break ;
        }
        }
    }
    result[idx] = '\0' ;
    return result ;
}
char *VeriMinTypMaxValue::Image() const
{
    if (!_min || !_typ || !_max) return 0 ;
    char *min = _min->Image() ;
    char *typ = _typ->Image() ;
    char *max = _max->Image() ;
    char *val = Strings::save(min, ":", typ, ":", max) ;
    Strings::free(min) ;
    Strings::free(typ) ;
    Strings::free(max) ;
    return val ;
}
char *VeriArrayValue::Image() const
{
    if (!_values) return 0 ;
    unsigned i ;
    VeriBaseValue *val ;
    char *str = (_is_concat) ? Strings::save("{"): Strings::save("'{") ;
    char *tmp = 0 ;
    FOREACH_ARRAY_ITEM(_values, i, val) {
        char *ele_str = val ? val->Image() : 0 ;
        if (!ele_str) continue ;
        tmp = str ;
        str = Strings::save(str, (i ? "," : ""), ele_str) ;
        Strings::free(ele_str) ;
        Strings::free(tmp) ;
    }
    tmp = str;
    str = Strings::save(str, "}") ;
    Strings::free(tmp) ;
    return str ;
}
char *VeriEnumVal::Image() const
{
    VERIFIC_ASSERT(_id) ;
    return Strings::save(_id->Name()) ;
}
unsigned VeriBasedNumber::HasXZ() const
{
    unsigned sign = 0 ;
    char *bin_val = GetBinaryValue(&sign) ;
    unsigned ret_val = ConstStrEvaluation::StrHasXZ(bin_val) ;
    Strings::free(bin_val) ;
    return ret_val ;
}
unsigned VeriAsciiString::HasXZ() const
{
    // ASCII string cannot have binary x or z in it!
    return 0 ;
}
unsigned VeriMinTypMaxValue::HasXZ() const
{
    unsigned has_x = _min ? _min->HasXZ(): 0 ;
    if (!has_x) has_x = _typ ? _typ->HasXZ(): 0 ;
    if (!has_x) has_x = _max ? _max->HasXZ(): 0 ;
    return has_x ;
}
unsigned VeriArrayValue::HasXZ() const
{
    unsigned i ;
    VeriBaseValue *val ;
    FOREACH_ARRAY_ITEM(_values, i, val) {
        if (val && val->HasXZ()) return 1 ;
    }
    return 0 ;
}
unsigned VeriEnumVal::HasXZ() const
{
    // Value of enum literal is the value of its initial expression
    VeriBaseValue *val = GetValue() ; // Get value of initial expression
    // Check if initial value of this literal contains x/z
    unsigned has_xz = val ? val->HasXZ() : 0 ;
    delete val ;
    return has_xz ;
}
VeriBaseValue *VeriBaseValue::GetValueAt(unsigned /*pos*/) { return 0 ; }
VeriBaseValue *VeriBaseValue::TakeValueAt(unsigned /*pos*/) { return 0 ; }
Array *VeriBaseValue::TakeValues() { return 0 ; }
VeriBaseValue *VeriArrayValue::GetValueAt(unsigned pos)
{
    if (_values && (_values->Size() > pos)) return (VeriBaseValue*)_values->At(pos) ;
    return 0 ;
}
VeriBaseValue *VeriArrayValue::TakeValueAt(unsigned pos)
{
    VeriBaseValue *v = (_values && (_values->Size() > pos)) ? (VeriBaseValue*)_values->At(pos) : 0 ;
    if (v && _values) _values->Insert(pos, 0) ; // Put null at value's position
    return v ;
}
Array *VeriArrayValue::TakeValues()
{
    Array *vals = _values ;
    _values = 0 ;
    return vals ;
}
void VeriBaseValue::SetValueAt(VeriBaseValue * /*v*/, unsigned /*pos*/) {}
void VeriArrayValue::SetValueAt(VeriBaseValue *v, unsigned pos)
{
    if (!_values || (_values->Size() <= pos)) return ; // Outside of its range

    VeriBaseValue *exist = (VeriBaseValue*)_values->At(pos) ;
    delete exist ;
    _values->Insert(pos, v) ;
}
unsigned VeriBaseValue::IsEmpty() const { return 0 ; } // virtual catcher for every value except array value
unsigned VeriArrayValue::IsEmpty() const
{
    unsigned i ;
    VeriBaseValue *ele ;
    FOREACH_ARRAY_ITEM(_values, i, ele) {
        if (!ele) continue ;
        if (!ele->IsEmpty()) return 0 ;
    }
    return 1 ;
}

// Adjust size/sign of 'this' and return a created value absorb 'this'
VeriBaseValue *VeriBaseValue::Adjust(unsigned size, unsigned sign, unsigned use_self_sign, unsigned two_state)
{
    unsigned this_sign = 0 ;
    char *str = GetBinaryValue(&this_sign) ;
    // VIPER #4749: Can't adjust the value if new size is 0 or cannot get the binary string:
    if (!str || !size) return this ;
    if ((size == Strings::len(str)) && // Size already matches
        (use_self_sign || (this_sign == sign))) { // Sign already matches
        if (!two_state) {
            Strings::free(str) ;
            return this ; // Not two state, ignore x/z in 'this'
        }
        unsigned xz = ConstStrEvaluation::StrHasXZ(str) ;
        if (!xz) {
            Strings::free(str) ;
            return this ; // No x/z in 'this': conforms to 'two_state'
        }
    }

    if (!use_self_sign) this_sign = sign ;
    char *ret_str = ConstStrEvaluation::GetPaddedString(str, size, this_sign) ;
    Strings::free(str) ;
    if (!ret_str) { return this ; }
    char *prefix = Strings::itoa((int)size) ;
    if (two_state) { // Convert x/z to 0 if they exists
        unsigned i ;
        for(i = 0; i < size; i++) {
            switch(ret_str[i]) {
            case 'x':
            case 'Z':
            case 'X':
            case 'z': ret_str[i] = '0' ; break ;
            default : break ;
            }
        }
    }
    char *bin_str = Strings::save(prefix, (this_sign? "'sb" : "'b"), ret_str) ;
    VeriBaseValue *ret_val = new VeriBasedNumber(bin_str) ;
    Strings::free(ret_str) ;
    Strings::free(bin_str) ;
    Strings::free(prefix) ;
    delete this ;
    return ret_val ;
}

// VIPER #5503: Adjust size of 'this' (return a created value absorbing 'this').
VeriBaseValue *VeriAsciiString::Adjust(unsigned size, unsigned /*sign*/, unsigned /*use_self_sign*/, unsigned /*two_state*/)
{
    if (!size) return this ;

    if (size % 8) return this ; //  Cannot adjust string if it is not multiple of 8

    size = size/8 ;
    if (size >= _size) return this ; // Size already matches or greater

    // If size of this is greater than target size then truncate the size
    char *ret_str = Strings::allocate(size+1) ;
    unsigned rest_size = _size - size ;
    char *idx_start = _str + rest_size ;
    (void) memcpy(ret_str, idx_start, size) ;
    ret_str[size] = '\0' ;
    VeriBaseValue *ret_val = new VeriAsciiString(ret_str, size) ;
    Strings::free(ret_str) ;
    delete this ;
    return ret_val ;
}

unsigned VeriBaseValue::IsAnyFieldEmpty() const { return 0 ; } // Virtual catcher for all values except array value (non empty)
unsigned VeriArrayValue::IsAnyFieldEmpty() const
{
    unsigned i ;
    VeriBaseValue *v ;
    FOREACH_ARRAY_ITEM(_values, i, v) {
        if (!v || v->IsAnyFieldEmpty()) return 1 ;
    }
    return 0 ;
}
VeriBaseValue *VeriConstraint::CreateEmptyValue(unsigned /*upto_bit*/) { return 0 ; }
VeriBaseValue *VeriScalarTypeConstraint::CreateEmptyValue(unsigned /*upto_bit*/)
{
    // This is an element. If we have to go upto bit label, create value here
    // otherwise return 0
    VeriBaseValue *result = 0 ;
    //if (upto_bit) {
    unsigned num_of_bits = NumOfBits() ;
    // if (num_of_bits > 1) {
    if (num_of_bits > 0) { // VIPER #8240: Craete value if num_of_bits is greater than 0
        unsigned i ;
        Array *values = new Array(num_of_bits) ;
        for (i = 0; i < num_of_bits; i++) values->InsertLast(0) ;
        result = new VeriArrayValue(values, 0) ;
    }
    //}
    return result ;
}
VeriBaseValue *VeriEnumConstraint::CreateEmptyValue(unsigned /*upto_bit*/)
{
    // This is an element. If we have to go upto bit label, create value here
    // otherwise return 0
    VeriBaseValue *result = 0 ;
    //if (upto_bit) {
    unsigned num_of_bits = NumOfBits() ;
    // VIPER #8050 : It does not make sense to go bit level for enum. So
    // create array value of size 1
    //Array *values = new Array(num_of_bits) ;
    //values->InsertLast(0) ;
    //result = new VeriArrayValue(values, 0) ;
    if (num_of_bits > 1) {
        unsigned i ;
        Array *values = new Array(num_of_bits) ;
        for (i = 0; i < num_of_bits; i++) values->InsertLast(0) ;
        result = new VeriArrayValue(values, 0) ;
    }
    //}
    return result ;
}
VeriBaseValue *VeriRangeConstraint::CreateEmptyValue(unsigned /*upto_bit*/)
{
    return 0 ;
}
VeriBaseValue *VeriArrayConstraint::CreateEmptyValue(unsigned upto_bit)
{
    // If it is packed array and we are not allowed to go to bit-label,
    // consider it as an element. So return 0.
    // VIPER #7960 : If this array has only one packed dimension and we
    // are not allowed to go to bit-label, consider this as an element. Otherwise
    // recurse
    //if (IsPacked() && _element_constraint && _element_constraint->IsScalarTypeConstraint() && !upto_bit) return 0 ;

    // It is an array. Create an array value of index constraint times length
    unsigned range_width = Length() ;
    Array *values = new Array(range_width) ;
    unsigned i ;
    for (i = 0 ; i < range_width; i++) {
        values->InsertLast(_element_constraint ? _element_constraint->CreateEmptyValue(upto_bit): 0) ;
    }

    return new VeriArrayValue(values, 0) ;
}
VeriBaseValue *VeriRecordConstraint::CreateEmptyValue(unsigned /*upto_bit*/)
{
    if (!_element_constraints) return 0 ;
    if ((_type == VERI_UNION) && IsPacked()) {
        return 0 ;
        // This represents an union constraint, call the same routine on the full constraint:
        //return (_full_constraint) ? _full_constraint->CreateEmptyValue(upto_bit) : 0 ;
    }

    // Create an array value with length equals to the element count of this record
    unsigned element_count = _element_constraints->Size() ;
    Array *values = new Array(element_count) ;
    unsigned i ;
    VeriConstraint *element_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
        //values->InsertLast(element_constraint ? element_constraint->CreateEmptyValue(upto_bit) : 0) ;
        // Always create bit level value for record constraint
        values->InsertLast(element_constraint ? element_constraint->CreateEmptyValue(1) : 0) ;
        if (_type == VERI_UNION) break ; // VIPER #7960 : Consider one element for union
    }
    VeriBaseValue *r = new VeriArrayValue(values, 0) ;
    r->SetIsStructure() ;
    return r ;
}

// Check argument specified index is within array bounds :
unsigned VeriConstraint::StaticContains(VeriBaseValue * /*idx*/) const  { return 0 ; }
unsigned VeriEnumConstraint::StaticContains(VeriBaseValue *idx) const
{
    return _base ? _base->StaticContains(idx): 0 ;
}
unsigned VeriScalarTypeConstraint::StaticContains(VeriBaseValue *idx) const
{
    if (!idx) return 0 ;
    int index = idx->GetIntegerValue() ;
    // Value is OUT of range if its position is below the lowest, or above the highest.
    // Range of scalar types are defined as 'size -1 downto 0'
    int msb = (int)NumOfBits() - 1 ;
    int lsb = 0 ;

    return !((msb < index) || (index < lsb)) ;
}
unsigned VeriArrayConstraint::StaticContains(VeriBaseValue *idx) const { return (_index_constraint) ? _index_constraint->StaticContains(idx) : 0 ; }
unsigned VeriRangeConstraint::StaticContains(VeriBaseValue *idx) const
{
    if (!idx) return 0 ;
    int index = idx->GetIntegerValue() ;
    // Value is OUT of range if its position is below the lowest, or above the highest.
    return (IsTo()) ? !((_right < index) || (index < _left)) : !((_left < index) || (index < _right)) ;
}

unsigned VeriConstraint::StaticFillSpecifiedElements(VeriBaseValue * /*result*/, VeriExpression * /*expr*/, VeriConstraint * /*label_constraint*/, VeriBaseValue * /*elem_val*/, unsigned  /*pos*/, ValueTable * /*tab*/, VeriConstraint * /*target_constraint*/, unsigned &cannot_evaluate, unsigned /*expect_nonconst*/, unsigned /*check_validity*/, Set * /*done*/)
{ (void) cannot_evaluate ; return 0 ; }
unsigned VeriScalarTypeConstraint::StaticFillSpecifiedElements(VeriBaseValue *result, VeriExpression *expr, VeriConstraint *label_constraint, VeriBaseValue *elem_val, unsigned pos, ValueTable *tab, VeriConstraint *target_constraint, unsigned &cannot_evaluate, unsigned expect_nonconst, unsigned check_validity, Set * /*done*/)
{
    VeriBaseValue *this_val = 0 ;
    if (this == target_constraint) {
        this_val = result ;
    } else {
        VeriBaseValue *exist = result ? result->GetValueAt(pos): 0 ;
        // Ignore existing field only for default : value
        if (exist && !label_constraint && !exist->IsEmpty()) return 1 ;
        this_val = exist ;
    }

    // Type exists in label :
    if (label_constraint) {
        unsigned matched = 0 ;
        if (label_constraint->IsMatching(this)) {
            if (result) result->SetValueAt(elem_val ? elem_val->Copy(): 0, pos) ;
            matched = 1 ;
        } else if (label_constraint->SubTypeOf(this) && target_constraint && !target_constraint->IsRecordConstraint()) {
            // Target type is 'byte', but label specifies 'bit'. As 'byte' is an
            // array of 'bit' s, we can calculate this
            matched = 1 ;
            if (target_constraint->IsScalarTypeConstraint()) {
                // Target is scalar type. Our value aray is number of bits length
                // i.e. 'result' is array value for this scalar type and we have to
                // fill it
                this_val = result ;
            } else if (!this_val) { // This type is not bit-blasted
                // Create empty value for every bit
                this_val = CreateEmptyValue(1) ;
                if (result) result->SetValueAt(this_val, pos) ;
            }
            if (!this_val) return 0 ; // Error somewhere
            unsigned sub_type_size = label_constraint->NumOfBits() ;
            unsigned this_type_size = NumOfBits() ;
            unsigned i, j ;
            unsigned idx = 0 ;
            for (i = 0; i < this_type_size; i= i +sub_type_size) {
                unsigned sign = 0 ;
                VeriBaseValue *copy_val = elem_val ? elem_val->Copy(): 0 ;
                VeriBaseValue *v = copy_val ? copy_val->Adjust(sub_type_size, 0, 1, 0) : 0 ;
                char *str = v ? v->GetBinaryValue(&sign): 0 ;
                delete v ;
                if (!str || (elem_val && elem_val->Type() == REAL)) {
                    expr->Error("illegal context for real expression") ;
                    Strings::free(str) ;
                    return 0 ;
                }
                for (j = 0; j < sub_type_size; j++) {
                    // Allow overwriting
                    //if (this_val->GetValueAt(idx)) {
                        //idx++ ;
                        //continue ;
                    //}
                    char val_str[2] ;
                    val_str[0] = str[j] ;
                    val_str[1] = 0 ;
                    char *x = Strings::save("1'b", val_str) ;
                    this_val->SetValueAt(new VeriBasedNumber(x), idx) ;
                    Strings::free(x) ;
                    idx++ ;
                }
                Strings::free(str) ;
            }
        }
        return matched ;
    }
    unsigned label_size = NumOfBits() ;
    verific_int64 expr_size_sign = expr->StaticSizeSignInternal(tab, this) ;
    unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
    int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
    expr_size = MAX(expr_size, label_size) ;
    expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
    // Default label :
    VeriBaseValue *value = expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, tab, this, expect_nonconst, check_validity) ;
    unsigned value_allocated = 0 ; // need to track allocation to value for error-out state
    if (!value && check_validity) {
        value = new VeriInteger(0) ;
        value_allocated = 1 ;
    }
    if (!value) {
        cannot_evaluate = 1 ; // VIPER #8207 : propgate information that expression is not evaluated
        return 0 ; // Error :
    }
    unsigned size = label_size ; //NumOfBits() ;
    if (target_constraint && target_constraint->IsScalarTypeConstraint()) {
        if (!this_val) { // This type is not bit-blasted
            // Create empty value for every bit
            this_val = CreateEmptyValue(1) ;
            if (result) result->SetValueAt(this_val, pos) ;
        }
        if (!this_val) {
            if (value_allocated) delete value ; // to avoid leak in error state
            return 0 ; // Some error
        }
        // Target scalar, so every bit of target will be default value
        // Adjust default value to fit size 1
        // VIPER #8070: Do not call with 'self-sign == 1',
        // Bit of scalar type constraint is always unsigned:
        VeriBaseValue *tmp = value->Adjust(1, 0, 0, 0) ;
        unsigned i ;
        for (i = 0; i < size; i++) {
            if (this_val->GetValueAt(i)) continue ; // Already filled
            this_val->SetValueAt(tmp ? tmp->Copy(): 0, i) ;
        }
        delete tmp ;
    } else {
        if (result) result->SetValueAt(value, pos) ; // Set default value
        else if (value_allocated) delete value ; // otherwise leaked
    }
    return 1 ;
}
unsigned VeriEnumConstraint::StaticFillSpecifiedElements(VeriBaseValue *result, VeriExpression *expr, VeriConstraint *label_constraint, VeriBaseValue *elem_val, unsigned pos, ValueTable *tab, VeriConstraint *target_constraint, unsigned &cannot_evaluate, unsigned expect_nonconst, unsigned check_validity, Set * /*done*/)
{
    if (!result) return 0 ;
    VeriBaseValue *exist = result->GetValueAt(pos) ;
    // Ignore existing field only for default : value
    if (exist && !label_constraint && !exist->IsEmpty()) return 1 ;

    // Type exists in label :
    if (label_constraint) {
        unsigned matched = 0 ;
        if (label_constraint->IsMatching(this)) {
            result->SetValueAt(elem_val ? elem_val->Copy(): 0, pos) ;
            matched = 1 ;
        }
        return matched ;
    }
    unsigned label_size = NumOfBits() ;
    verific_int64 expr_size_sign = expr->StaticSizeSignInternal(tab, this) ;
    unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
    int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
    expr_size = MAX(expr_size, label_size) ;
    expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
    // default label :
    VeriBaseValue *value = expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, tab, this, expect_nonconst, check_validity) ;
    if (!value && check_validity) value = new VeriInteger(0) ;
    if (!value) {
        cannot_evaluate = 1 ; // VIPER #8207 : propgate information that expression is not evaluated
        return 0 ; // Error :
    }
    if (target_constraint == this) {
        // VIPER #8050 : If target constraint is this enum constraint, we need to set default
        // value to its bits
        unsigned i ;
        unsigned size = NumOfBits() ;
        for (i = 0; i < size; i++) {
            if (result->GetValueAt(i)) continue ; // Already filled
            result->SetValueAt(value ? value->Copy(): 0, i) ;
        }
        delete value ;
    } else {
        result->SetValueAt(value, pos) ; // Set default value
    }
    return 1 ;
}
unsigned VeriRangeConstraint::StaticFillSpecifiedElements(VeriBaseValue * /*result*/, VeriExpression * /*expr*/, VeriConstraint * /*label_constraint*/, VeriBaseValue * /*elem_val*/, unsigned  /*pos*/, ValueTable * /*tab*/, VeriConstraint * /*target_constraint*/, unsigned &cannot_evaluate, unsigned /*expect_nonconst*/, unsigned /*check_validity*/, Set * /*done*/)
{
    (void) cannot_evaluate ; return 0 ;
}
unsigned VeriArrayConstraint::StaticFillSpecifiedElements(VeriBaseValue *result, VeriExpression *expr, VeriConstraint *label_constraint, VeriBaseValue *elem_val, unsigned pos, ValueTable *tab, VeriConstraint *target_constraint, unsigned &cannot_evaluate, unsigned expect_nonconst, unsigned check_validity, Set *done)
{
    VeriBaseValue *this_val = 0 ;
    if (this == target_constraint) {
        this_val = result ;
    } else {
        this_val = result ? result->GetValueAt(pos): 0 ;
    }
    if (!_element_constraint) return 0 ;
    // Packed array and we are evaluating default label, evaluate here
    // if target is not packed array/scalar type. In those cases we have to
    // recurse further :
    //if (target_constraint && IsPacked() && !label_constraint && !target_constraint->IsScalarTypeConstraint() && !target_constraint->IsPacked()) {
    // VIPER #7960 : If it is packed array with one dimension and target is not
    // scalar-constraint, do not recurse
    //if (target_constraint && IsPacked() && !label_constraint && !(target_constraint->IsScalarTypeConstraint() || (target_constraint->IsArrayConstraint() && target_constraint->IsPacked() && (target_constraint->PackedDimCount()==1))) && _element_constraint->IsScalarTypeConstraint()) {
    // VIPER #8050 : Do not recurse for packed array of enum
    VeriConstraint *arr_element_constraint = target_constraint ? target_constraint->ElementConstraint(): 0 ;
    if (target_constraint && IsPacked() && !label_constraint && !(target_constraint->IsScalarTypeConstraint() || ((target_constraint->IsArrayConstraint() && target_constraint->IsPacked() && arr_element_constraint && arr_element_constraint->IsScalarTypeConstraint()) && ((target_constraint->PackedDimCount()==1) || _element_constraint->IsEnumConstraint()))) && _element_constraint->IsScalarTypeConstraint()) {
        VeriBaseValue *exist = result ? result->GetValueAt(pos): 0 ;
        if (exist && !exist->IsEmpty()) return 1 ; // Already filled
        unsigned label_size = NumOfBits() ;
        verific_int64 expr_size_sign = expr->StaticSizeSignInternal(tab, this) ;
        unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
        int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
        expr_size = MAX(expr_size, label_size) ;
        expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;

        VeriBaseValue *value = expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, tab, this, expect_nonconst, check_validity) ;
        unsigned value_allocated = 0 ; // keep track for error out
        if (!value && check_validity) {
            value = new VeriInteger(0) ;
            value_allocated = 1 ;
        }
        if (!value) {
            cannot_evaluate = 1 ; // VIPER #8207 : propgate information that expression is not evaluated
            return 0 ; // Error :
        }
        if (result) result->SetValueAt(value, pos) ;
        else if (value_allocated) delete value ; // if not handed off, free memory.
        return 1 ;
    }
    // Type exists in label :
    if (label_constraint && label_constraint->IsMatching(this)) {
        if (done) (void) done->Insert(this) ;
        if (result) result->SetValueAt(elem_val ? elem_val->Copy():0, pos) ;
        return 1 ;
    } else if (label_constraint && target_constraint && IsPacked() && target_constraint->IsRecordConstraint()) {
        // If data type is specified as label in structure literal, that type
        // should match with member type. Recurse through array elements not needed:
        return 0 ;
    }
    // If this constraint is already fiiled by another datatype : value, do not
    // recurse
    if (done && done->GetItem(this)) return 0 ;

    if (!this_val && label_constraint && (label_constraint->NumOfBits() == 1)) {
        // If bit level value is not created and label indicates that bit level value
        // is required, create an empty value for this constraint
        this_val = CreateEmptyValue(1) ;
        if (result) result->SetValueAt(this_val, pos) ;
    }
    // Each element has same element :
    // Call this function recursively for index constraint length times :
    unsigned length = Length() ;
    unsigned i ;
    unsigned matched = 0 ;
    VeriBaseValue *ele_val = this_val ;
    for (i = 0 ; i < length; i++) {
        if (_element_constraint->StaticFillSpecifiedElements(ele_val, expr, label_constraint, elem_val, i, tab, target_constraint, cannot_evaluate, expect_nonconst, check_validity, done)) matched = 1 ;
    }
    return matched ;
}
unsigned VeriRecordConstraint::StaticFillSpecifiedElements(VeriBaseValue *result, VeriExpression *expr, VeriConstraint *label_constraint, VeriBaseValue *elem_val, unsigned pos, ValueTable *tab, VeriConstraint *target_constraint, unsigned &cannot_evaluate, unsigned expect_nonconst, unsigned check_validity, Set *done)
{
    // Type exists in label :
    if (label_constraint && label_constraint->IsMatching(this)) {
        if (done) (void) done->Insert(this) ;
        if (result) result->SetValueAt(elem_val ? elem_val->Copy():0, pos) ;
        return 1 ;
    }
    // If this constraint is already fiiled by another datatype : value, do not
    // recurse
    if (done && done->GetItem(this)) return 0 ;
    // VIPER #7610 : Check if this type is same as value type and we are evaluating
    // default label, apply value here
    VeriConstraint *this_constraint = (_type == VERI_UNION && _full_constraint) ? _full_constraint : this ;
    VeriConstraint *expr_constraint = expr->EvaluateConstraintInternal(tab, 0) ;
    //if (expr_constraint && target_constraint && this_constraint->IsPacked() && !label_constraint && expr_constraint->IsAssignmentCompatible(this_constraint, expr, VeriTreeNode::NO_ENV)) {
    // VIPER #7960 : Do not recurse into packed union
    if ((_type == VERI_UNION) && IsPacked() && expr_constraint->IsAssignmentCompatible(this_constraint, expr, VeriTreeNode::NO_ENV)) {
        delete expr_constraint ;
        VeriBaseValue *exist = result ? result->GetValueAt(pos): 0 ;
        // Ignore existing field only for default : value
        if (exist && !label_constraint && !exist->IsEmpty()) return 1 ; // Already filled
        unsigned label_size = NumOfBits() ;
        verific_int64 expr_size_sign = expr->StaticSizeSignInternal(tab, this) ;
        unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
        int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
        expr_size = MAX(expr_size, label_size) ;
        expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
        VeriBaseValue *value = expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, tab, this, expect_nonconst, check_validity) ;
        unsigned value_allocated = 0 ; // keep track for error out state
        if (!value && check_validity) {
            value = new VeriInteger(0) ;
            value_allocated = 1 ;
        }
        if (!value) {
            cannot_evaluate = 1 ; // VIPER #8207 : propgate information that expression is not evaluated
            return 0 ;
        }
        if (result) result->SetValueAt(value, pos) ;
        else if (value_allocated) delete value ; // if not assigned, free memory
        return 1 ;
    }
    delete expr_constraint ;
    // VIPER #7485 : If this is union, work with '_full_constraint'
    //if (_type == VERI_UNION && _full_constraint) {
        //return _full_constraint->StaticFillSpecifiedElements(result, expr, label_constraint, elem_val, pos, tab, target_constraint) ;
    //}
    VeriBaseValue *this_val = 0 ;
    if (this == target_constraint) {
        this_val = result ;
    } else {
        this_val = result ? result->GetValueAt(pos): 0 ;
    }
    // Type exists in label :
    //if (label_constraint && label_constraint->IsMatching(this)) {
        //result->SetValueAt(elem_val ? elem_val->Copy():0, pos) ;
        //return 1 ;
    //}
    // Traverse all elements and fill values :
    unsigned i ;
    unsigned matched = 0 ;
    VeriConstraint *element_constraint ;
    VeriBaseValue *ele_val = this_val ;

    if (label_constraint && label_constraint->IsRecordConstraint()) {
        unsigned has_equal_type = 0 ;
        FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
            if (!element_constraint) continue ;
            if (label_constraint->IsEqual(element_constraint)) {
                has_equal_type = 1 ;
                if (element_constraint->StaticFillSpecifiedElements(ele_val, expr, label_constraint, elem_val, i, tab, target_constraint, cannot_evaluate, expect_nonconst, check_validity, done)) matched = 1 ;
            }
        }
        if (has_equal_type) return matched ;
    }
    FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
        if (!element_constraint) continue ;
        if (element_constraint->StaticFillSpecifiedElements(ele_val, expr, label_constraint, elem_val, i, tab, target_constraint, cannot_evaluate, expect_nonconst, check_validity, done)) matched = 1 ;
        if (_type == VERI_UNION) break ;
    }
    return matched ;
}

// VIPER #4045 : Convert any value to string value. Needed for type cast to string
VeriBaseValue *VeriBaseValue::ToAsciiString()
{
    delete this ;
    return 0 ;
}
VeriBaseValue *VeriInteger::ToAsciiString()
{
    // Get the integer value first
    int int_val = GetIntegerValue() ;
    // Convert the int to char:
    char s[2] ;
    s[0] = (char)int_val ;
    s[1] = 0 ;

    VeriBaseValue *ret_val = new VeriAsciiString(s, 1) ;
    delete this ;
    return ret_val ;
}
VeriBaseValue *VeriReal::ToAsciiString()
{
    // Get the real value first
    double val = GetRealValue() ;
    // Convert the real to string:
    char s[2] ;
    s[0] = (char)val ;
    s[1] = 0 ;

    VeriBaseValue *ret_val = new VeriAsciiString(s, 1) ;
    delete this ;
    return ret_val ;
}
VeriBaseValue *VeriBasedNumber::ToAsciiString()
{
    // Get the integer value first
    int int_val = GetIntegerValue() ;
    // Convert the int to string:
    char s[2] ;
    s[0] = (char)int_val ;
    s[1] = 0 ;

    VeriBaseValue *ret_val = new VeriAsciiString(s, 1) ;
    delete this ;
    return ret_val ;
}
VeriBaseValue *VeriAsciiString::ToAsciiString()
{
    return this ;
}

VeriBaseValue *
VeriBaseValue::TryConvertToInteger()
{
    // Either cannot convert it or is not required (for VeriInteger):
    return this ;
}

VeriBaseValue *
VeriReal::TryConvertToInteger()
{
    double rval = GetRealValue() ;
    if (rval != (double)((int)rval)) return this ; // This is a real fraction like 2.3

    delete this ;
    return (new VeriInteger((int)rval)) ;
}

VeriBaseValue *
VeriBasedNumber::TryConvertToInteger()
{
    if (HasXZ()) return this ; // Cannot convert to integer

    int ival = GetIntegerValue() ;
    delete this ;
    return (new VeriInteger(ival)) ;
}

VeriBaseValue *VeriBaseValue::ToArray(unsigned array_size, unsigned num_of_bits)
{
    unsigned b_sign = 0 ;

    // Convert value to binary equivalent
    char *str = GetBinaryValue(&b_sign) ;

    // Extend/truncate to required size :
    if (Strings::len(str) != num_of_bits) {
        char *tmp = str ;
        str = ConstStrEvaluation::GetPaddedString(str, num_of_bits, b_sign) ;
        Strings::free(tmp) ;
    }

    unsigned i, idx = 0 ;
    unsigned count = array_size ;
    unsigned each_val_size = num_of_bits/array_size ;

    Array *arr = new Array(array_size) ;
    char *part_str = 0 ;
    while(count) { // Iterate array size
        part_str = Strings::allocate(each_val_size+1) ;
        part_str[0] = 0 ;
        // Extract 'each_val_size' elements from 'str'
        for (i = 0; i < each_val_size; i++) {
            part_str[i] = str[idx++] ;
        }
        part_str[i] = 0 ;
        char *prefix = Strings::itoa((int)each_val_size) ;
        char *val_str = Strings::save(prefix, "'b", part_str) ;
        VeriBaseValue *v = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        Strings::free(prefix) ;
        Strings::free(part_str) ;
        arr->InsertLast(v) ; // Insert created value to array
        count-- ;
    }
    Strings::free(str) ;
    return new VeriArrayValue(arr) ;
}
VeriBaseValue *VeriArrayValue::ToArray(unsigned array_size, unsigned num_of_bits)
{
    if (_values && _values->Size() == array_size) return Copy() ;
    return VeriBaseValue::ToArray(array_size, num_of_bits) ;
}

VeriBaseValue *VeriBaseValue::ToBasedNum(unsigned /*preserve_arr*/)
{
    unsigned b_sign = 0 ;

    char *str = GetBinaryValue(&b_sign) ;
    if (!str) return 0 ;
    unsigned len = Strings::len(str) ;

    char *prefix = Strings::itoa((int)len) ;
    char *val_str = Strings::save(prefix, (b_sign) ? "'sb": "'b", str) ;
    VeriBaseValue *v = new VeriBasedNumber(val_str) ;
    Strings::free(val_str) ;
    Strings::free(prefix) ;
    Strings::free(str) ;
    delete this ;
    return v ;
}
VeriBaseValue *VeriArrayValue::ToBasedNum(unsigned preserve_arr)
{
    if (!_values) return 0 ;

    if (!preserve_arr) return VeriBaseValue::ToBasedNum(preserve_arr) ;
    unsigned i ;
    VeriBaseValue *ele, *new_ele ;
    FOREACH_ARRAY_ITEM(_values, i, ele) {
        new_ele = (ele) ? ele->ToBasedNum(preserve_arr): 0 ;
        if (new_ele != ele) _values->Insert(i, new_ele) ;
    }
    return this ;
}
// Base assignment : Assign argument specified value on this:
unsigned VeriBaseValue::Assign(VeriBaseValue * /*val*/, unsigned /*value_size*/, unsigned /*lsb_bit_offset*/)
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
unsigned VeriBasedNumber::Assign(VeriBaseValue *val, unsigned value_size, unsigned lsb_bit_offset)
{
    unsigned b_sign = 0 ;
    char *s = GetBinaryValue(&b_sign) ;
    if (!s) return 0 ;

    unsigned tmp = (unsigned)Strings::len(s) ;
    unsigned blen = tmp ;
    if (!blen) blen =  32 ;

    if (!value_size && !lsb_bit_offset) {
        // Assume this is a full target-value assignment :
        value_size = blen ;
    }
     // If offset+value_size is too large for this value, chop-off the part that would drop out.
    if (lsb_bit_offset >= blen) {
        // The entire value drops outside the target range.
        // bail out
        Strings::free(s) ;
        return 1;
    }
    if (lsb_bit_offset+value_size > blen) {
        // A part of the value drops outside.
        // Chop off the offending part (chop-off will happen on MSB part, which is correct).
        value_size = blen - lsb_bit_offset ;
    }
    // If value_size is (still) zero here, then we will not assign anything :
    if (!value_size) { Strings::free(s) ; return 1 ; }

    unsigned sign = 0 ;
    char *value_str = val->GetBinaryValue(&sign) ;
    if (Strings::len(value_str) != value_size) {
        char *tmp2 = value_str ;
        value_str = ConstStrEvaluation::GetPaddedString(tmp2, value_size, sign) ;
        Strings::free(tmp2) ;
    }
    // Change of value will start from (blen - (lsb_bit_offset+value_size))
    unsigned start_pos = (blen - (lsb_bit_offset+value_size)) ;
    unsigned i ;
    for (i = 0; i < value_size; i++) {
        s[start_pos++] = value_str[i] ;
    }
    char *prfix = Strings::itoa((int)blen) ;
    char *val_str = Strings::save(prfix, (b_sign) ? "'sb": "'b", s) ;
    Strings::free(_based_num) ;
    _based_num = val_str ;
    Strings::free(prfix) ;
    Strings::free(s) ;
    Strings::free(value_str) ;
    return 1 ;
}
// Value evaluation : take a portion from value of size 'value_size'. The value
// extracted should have an offset of 'lsb_bit_offset' from lsb.
VeriBaseValue *VeriBaseValue::Evaluate(unsigned value_size, unsigned lsb_bit_offset)
{
    if (!value_size && !lsb_bit_offset) {
        return Copy() ;
    }
    unsigned sign = 0 ;
    char *str = GetBinaryValue(&sign) ;
    if (!str) return 0 ;
    unsigned size = (unsigned)Strings::len(str) ;

    // If size of the value of this id is 1, we can return the value itself
    if (size == 1) {
        Strings::free(str) ;
        return Copy() ;
    }

    char *val_str = 0 ;
    VeriBaseValue *ret_val = 0 ;
    // If offset+value_size is too large for this value, chop-off the part that would drop out
    if (lsb_bit_offset >= size) {
       // The entire value drops outside the target range.
       // LRM 4.2.1 : out-of-bound indexing is allowed, but return x.
       val_str = ConstStrEvaluation::VeriGetXOfWidth(value_size) ;
       char *prefix = Strings::itoa((int)value_size) ;
       char *s = Strings::save(prefix, "'b", val_str) ;
       ret_val = new VeriBasedNumber(s) ;
       Strings::free(s) ;
       Strings::free(prefix) ;
       Strings::free(val_str) ;
       Strings::free(str) ;
       return ret_val ;
    }
    // If the requested slice exceeds the MSB (left) side, then we need to x-extend.
    // Record how many x bits need to be pre-pended :
    unsigned num_of_msb_xes = 0 ;
    if (lsb_bit_offset+value_size > size) {
        // A part of the value drops outside.
        // Chop off the offending part (chop-off will happen on MSB part, which is correct).
        num_of_msb_xes = lsb_bit_offset+value_size - size ;
        // Adjust value_size so that the copy constructor below does not go out of array bounds :
        value_size = size - lsb_bit_offset ;
    }

    if (!value_size) {
        Strings::free(str) ;
        return 0 ;
    }
    // Calculate the strating position from which 'value_size' amount of bits need
    // to be returned.
    unsigned start_pos = size - (lsb_bit_offset+value_size) ;
    char *new_str = Strings::allocate(value_size + 4) ;
    unsigned i ;
    for (i = 0; i < value_size; i++) {
        new_str[i] = str[start_pos++] ;
    }
    new_str[i] = 0 ;
    if (num_of_msb_xes) {
        char *x_str = Strings::allocate(num_of_msb_xes +1) ;
        for (i = 0; i < num_of_msb_xes; i++) {
            x_str[i] = 'x' ;
        }
        x_str[i] = 0 ;
        char *tmp = new_str ;
        new_str = Strings::save(x_str, tmp) ;
        Strings::free(tmp) ;
        Strings::free(x_str) ;
    }
    char *prefix = Strings::itoa((int)(num_of_msb_xes+value_size)) ;
    char *v_str = Strings::save(prefix, "'b", new_str) ;
    Strings::free(prefix) ;
    Strings::free(new_str) ;
    Strings::free(str) ;
    ret_val = new VeriBasedNumber(v_str) ;
    Strings::free(v_str) ;
    return ret_val ;
}
VeriBaseValue *VeriReal::Evaluate(unsigned value_size, unsigned lsb_bit_offset)
{
    if (!value_size && !lsb_bit_offset) return Copy() ;
    return 0 ; // Cannot select on real
}

// Impose argument specified sign and 2/4 state information on 'this':
// 1. Make sign/unsigned
// 2. Convert x/z to 0 for 2 state
void VeriBaseValue::ImposeSignState(unsigned /*sign*/, unsigned /*is_2_state*/)
{ }
void VeriArrayValue::ImposeSignState(unsigned sign, unsigned is_2_state)
{
    unsigned i ;
    VeriBaseValue *ele ;
    FOREACH_ARRAY_ITEM(_values, i, ele) {
        if (ele) ele->ImposeSignState(sign, is_2_state) ;
    }
}
void VeriMinTypMaxValue::ImposeSignState(unsigned sign, unsigned is_2_state)
{
    if (_min) _min->ImposeSignState(sign, is_2_state) ;
    if (_typ) _typ->ImposeSignState(sign, is_2_state) ;
    if (_max) _max->ImposeSignState(sign, is_2_state) ;
}
void VeriBasedNumber::ImposeSignState(unsigned sign, unsigned is_2_state)
{
    unsigned value_sign = GetSign() ;
    unsigned has_xZ = HasXZ() ;

    // If sign of this value is same as required sign, no need to change sign
    // If value does not contain x/z, no need to change state. Again if value contains
    // x/z, but this value is for 4 state variable, no need to change state.
    if ((sign == value_sign) && (!has_xZ || !is_2_state)) return ;

    // Need to change value :
    unsigned bsign = 0 ;
    char *str = GetBinaryValue(&bsign) ;

    unsigned size = Strings::len(str) ;

    // If value contains x/z and this is for 2 state variable, convert x/z to 0 :
    if (has_xZ && is_2_state) {
        unsigned i ;
        for(i = 0; i < size; i++) {
            switch(str[i]) {
            case 'x':
            case 'X':
            case 'z':
            case 'Z': str[i] = '0' ; break ;
            default : break ;
            }
        }
    }
    char *prefix = Strings::itoa((int)size) ;
    char *val_str = Strings::save(prefix, (sign ? "'sb": "'b"), str) ;
    Strings::free(prefix) ;
    Strings::free(str) ;
    Strings::free(_based_num) ;
    _based_num = val_str ;
}

verific_int64 VeriInteger::StaticSizeSign() const
{
    return -((int)sizeof(int)*8) ;
}

verific_int64 VeriReal::StaticSizeSign() const
{
    return -((int)sizeof(double)*8) ;
}

verific_int64 VeriBasedNumber::StaticSizeSign() const
{
    int result = Strings::atoi(_based_num) ;
    return (GetSign()) ? -result : result ;
}

verific_int64 VeriAsciiString::StaticSizeSign() const
{
    //return (int)(Strings::len(_str))*8 ; // Always unsigned
    return (verific_int64)(_size)*8 ;
}

verific_int64 VeriMinTypMaxValue::StaticSizeSign() const
{
    return  (_typ) ? _typ->StaticSizeSign() : 0 ;
}

verific_int64 VeriArrayValue::StaticSizeSign() const
{
    return (_values) ? (verific_int64)_values->Size() : 0 ; // CHECKME: Always unsigned?
}

verific_int64 VeriEnumVal::StaticSizeSign() const
{
    return (_id) ? _id->StaticSizeSignInternal(0, 0) : 0 ;
}

VeriBaseValue *
VeriInteger::Increment(int by_number)
{
    int n = GetIntegerValue() ;
    VeriBaseValue *incr = new VeriInteger(n+by_number) ;
    delete this ;
    return incr ;
}

VeriBaseValue *
VeriReal::Increment(int by_number)
{
    double d = GetRealValue() ;
    VeriBaseValue *incr = new VeriReal(d+by_number) ;
    delete this ;
    return incr ;
}

VeriBaseValue *
VeriBasedNumber::Increment(int by_number)
{
    if (HasXZ()) return this ;
    unsigned sign = 0 ;
    // Get the binary value of 'this' and add 1 to it:
    char *bin_this = ConstStrEvaluation::VeriStringToBinary(_based_num, &sign) ;

    unsigned len = Strings::len(bin_this) ;
    len++ ; // We need one extra bit for the overflow, if any
    char *tmp = ConstStrEvaluation::GetPaddedString(bin_this, len, sign) ;
    Strings::free(bin_this) ;
    bin_this = tmp ;

    unsigned neg = 0 ;
    if (by_number < 0) { neg = 1 ; by_number = -by_number ; }
    char *incr_num = ConstStrEvaluation::IntToBin(by_number) ;
    char *bin_incr = ConstStrEvaluation::GetPaddedString(incr_num, len, 0 /* unsigned */) ;
    char *add = (neg) ? ConstStrEvaluation::StrSub(bin_this, bin_incr, '0' /* carry-in */) :
                        ConstStrEvaluation::StrAdd(bin_this, bin_incr, '0' /* carry-in */) ;
    if (add && (add[0] == '0')) {
        // Didn't overflow, remove the extra bit we added above:
        tmp = Strings::save(add+1) ;
        Strings::free(add) ;
        add = tmp ;
    }
    // Format the base value string:
    char *size = Strings::itoa((int)Strings::len(add)) ;
    char *res = Strings::save(size, ((sign) ? "'sb" : "'b"), add) ;
    Strings::free(size) ;
    Strings::free(add) ;
    Strings::free(bin_incr) ;
    Strings::free(incr_num) ;
    Strings::free(bin_this) ;
    if (!res) return this ;
    // Create the final base value:
    VeriBaseValue *incr = new VeriBasedNumber(res) ;
    Strings::free(res) ;
    delete this ;
    return incr ;
}

VeriBaseValue *
VeriAsciiString::Increment(int by_number)
{
    // Get the binary value of 'this' and add 1 to it:
    char *bin_this = ConstStrEvaluation::AsciiToBin(_str) ;
    char *incr_num = ConstStrEvaluation::IntToBin(by_number) ;
    char *bin_incr = ConstStrEvaluation::GetPaddedString(incr_num, Strings::len(bin_this), 0 /* unsigned */) ;
    char *add = ConstStrEvaluation::StrAdd(bin_this, bin_incr, '0' /* carry-in */) ;
    char *res = ConstStrEvaluation::BinToAscii(add) ;
    Strings::free(add) ;
    Strings::free(bin_incr) ;
    Strings::free(incr_num) ;
    Strings::free(bin_this) ;
    if (!res) return this ;
    // Create the final base value:
    VeriBaseValue *incr = new VeriAsciiString(res, Strings::len(res)) ;
    Strings::free(res) ;
    delete this ;
    return incr ;
}

VeriBaseValue *
VeriMinTypMaxValue::Increment(int by_number)
{
    // CHECKME: Only increment the typical value.
    if (_typ) _typ = _typ->Increment(by_number) ;
    return this ;
}

VeriBaseValue *
VeriArrayValue::Increment(int /* by_number */)
{
    // FIXME: What can we do here?
    return this ;
}

VeriBaseValue *
VeriEnumVal::Increment(int by_number)
{
    VeriBaseValue *val = GetValue() ;
    if (!val) return this ;
    VeriBaseValue *incr = val->Increment(by_number) ;
    delete this ;
    return incr ;
}

// VIPER #7397: Concat all ascii strings to create a new one. Also adjust size if needed
VeriBaseValue * VeriBaseValue::ConcatAsciiStrings(const Array *val_array, unsigned retsize)
{
    if (!val_array || !val_array->Size()) return 0 ;

    // First calculate the length of concatenated string
    unsigned length = 0 ;
    unsigned i ;
    VeriBaseValue *val ;
    FOREACH_ARRAY_ITEM(val_array, i, val) {
        if (!val) continue ;
        length = length + val->GetStringSize() ;
    }
    // Allocate the memory
    char *result = Strings::allocate(length+1) ;
    char *runner = result ;
    // Now concatenate the strings
    FOREACH_ARRAY_ITEM(val_array, i, val) {
        if (!val) continue ;
        const char *str = val->GetString() ;
        unsigned len = val->GetStringSize() ;
        (void) memcpy(runner, str, len) ;
        runner += len ;
    }
    result[length] = '\0' ;
    VeriBaseValue *ret_val = new VeriAsciiString(result, length) ;
    Strings::free(result) ; // mem-leak fix
    return ret_val->Adjust(retsize, 0, 0, 0) ;
}
// VIPER #8037 : Check and adjust value with target constraint
VeriBaseValue* VeriBaseValue::AdjustAndCheck(VeriConstraint *id_constraint, const VeriIdDef *id, const VeriTreeNode *from)
{
    if (!id_constraint) return 0 ;
    unsigned target_num_bits = id_constraint->NumOfBits() ;
    if (id && id->IsParam() && !id->GetDataType() && id->GetDimensions() && VeriNode::InRelaxedCheckingMode()) {
        // In relaxed checking mode, produce warning for size mismatch of array elements for
        // parameters having unpacked dimension, but no type
        VeriExpression *expr = ToExpression(from) ;
        verific_int64 val_size_sign = expr ? expr->StaticSizeSignInternal(0, id_constraint): 0 ;
        verific_uint64 val_size = GET_CONTEXT_SIZE(val_size_sign) ;
        if (val_size && target_num_bits && (target_num_bits != val_size)) {
            if (from) from->Warning("array element widths (%d, %d) don't match", target_num_bits, val_size) ;
        }
        delete expr ;
    }
    // VIPER #8070: If this is not integer's size, adjust to the size specified:
    if ((Type()==INTEGER) && target_num_bits && id_constraint->IsSigned() && (target_num_bits == (sizeof(int)*8))) {
        return 0 ;
    } else {
        return Adjust(target_num_bits, id_constraint->IsSigned(), 0, id_constraint->Is2State()) ;
    }
}
VeriBaseValue* VeriArrayValue::AdjustAndCheck(VeriConstraint *id_constraint, const VeriIdDef *id, const VeriTreeNode *from)
{
    if (!_values || !id_constraint) return 0 ;
    unsigned element_size = 0 ;
    if (id_constraint->IsEnumConstraint() || id_constraint->IsScalarTypeConstraint()) {
        unsigned num_bits = id_constraint->NumOfBits() ;
        if (num_bits == _values->Size()) element_size = 1 ;
    }
    unsigned i ;
    VeriBaseValue *ele_val, *new_val = 0 ;
    VeriConstraint *ele_constraint ;
    FOREACH_ARRAY_ITEM(_values, i, ele_val) {
        ele_constraint = id_constraint->ElementConstraintAt(i) ;
        if (ele_constraint) {
            new_val = ele_val ? ele_val->AdjustAndCheck(ele_constraint, id, from): 0 ;
        } else if (element_size) {
            new_val = ele_val ? ele_val->Adjust(1, 0, 0, id_constraint->Is2State()): 0 ;
        }
        if (new_val) _values->Insert(i, new_val) ;
    }
    return 0 ;
}

