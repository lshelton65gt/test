/*
 *
 * [ File Version : 1.93 - 2014/03/13 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_CLASSIDS_H_
#define _VERIFIC_VERI_CLASSIDS_H_

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

// The following enum contains all non-abstract verilog class ids derived from VeriTreeNode.
// We use GetClassIds() instead RTTI, because ANSI's C++ RTTI is too expensive (extra item
// in virtual function table).

enum VERI_CLASS_ID
{
    ID_VERI_BEGIN_TABLE = 0xc0000000, // The first marker (unused) - specifies starting value

    // Class Ids for Verilog 95 and 2001 follow:
    ID_VERIPATHDECL,                ID_VERIPARAMID,                 ID_VERINETDECL,
    ID_VERIPORTOPEN,                ID_VERIDISABLE,                 ID_VERITABLE,
    ID_VERITASKDECL,                ID_VERIQUESTIONCOLON,           ID_VERIBINARYOPERATOR,
    ID_VERIDEFPARAMASSIGN,          ID_VERITREENODE,                ID_VERICONDITIONALSTATEMENT,
    ID_VERIREALVAL,                 ID_VERISTRENGTH,                ID_VERISYSTEMFUNCTIONCALL,
    ID_VERIINSTID,                  ID_VERIINITIALCONSTRUCT,        ID_VERIFUNCTIONDECL,
    ID_VERIPORTCONNECT,             ID_VERIMULTICONCAT,             ID_VERIGENERATEBLOCK,
    ID_VERIMEMORYID,                ID_VERIDELAYOREVENTCONTROL,     ID_VERIGENVARASSIGN,
    ID_VERIEVENTEXPRESSION,         ID_VERIEVENTTRIGGER,            ID_VERIDEFPARAM,
    ID_VERIGENERATECASEITEM,        ID_VERISPECIFYBLOCK,            ID_VERINONBLOCKINGASSIGN,
    ID_VERIBLOCKID,                 ID_VERICASESTATEMENT,           ID_VERISYSTEMTIMINGCHECK,
    ID_VERIGENERATECONSTRUCT,       ID_VERIIDREF,                   ID_VERISEQBLOCK,
    ID_VERIEVENTCONTROLSTATEMENT,   ID_VERIRELEASE,                 ID_VERICASEITEM,
    ID_VERIINDEXEDID,               ID_VERICONCAT,                  ID_VERIANSIPORTDECL,
    ID_VERIFOR,                     ID_VERIDELAYCONTROLSTATEMENT,   ID_VERICONST,
    ID_VERITIMINGCHECKEVENT,        ID_VERIMODULEID,                ID_VERIUNARYOPERATOR,
    ID_VERIGENERATECASE,            ID_VERIIDDEF,                   ID_VERIGENVARID,
    ID_VERIBLOCKINGASSIGN,          ID_VERITASKENABLE,              ID_VERIUDPID,
    ID_VERIVARIABLE,                ID_VERIREPEAT,                  ID_VERIFOREVER,
    ID_VERIMODULEINSTANTIATION,     ID_VERICONTINUOUSASSIGN,        ID_VERIGENERATECONDITIONAL,
    ID_VERIMINTYPMAXEXPR,           ID_VERIALWAYSCONSTRUCT,         ID_VERIWHILE,
    ID_VERIWAIT,                    ID_VERIINTVAL,                  ID_VERIGATEINSTANTIATION,
    ID_VERIPARBLOCK,                ID_VERISYSTEMTASKENABLE,        ID_VERIDEASSIGN,
    ID_VERITASKID,                  ID_VERIFUNCTIONCALL,            ID_VERIGENERATEFOR,
    ID_VERINETREGASSIGN,            ID_VERIPATH,                    ID_VERIFORCE,
    ID_VERIRANGE,                   ID_VERIPRIMITIVE,               ID_VERIFUNCTIONID,
    ID_VERIMODULE,                  ID_VERICONSTVAL,                ID_VERIASSIGN,
    ID_VERIINDEXEDMEMORYID,         ID_VERIDATATYPE,                ID_VERIDATADECL,
    ID_VERISELECTEDNAME,            ID_VERICOMMENTNODE,             ID_VERILIBRARYDECL,
    ID_VERICONFIGURATION,           ID_VERIINSTANCECONFIG,          ID_VERICELLCONFIG,
    ID_VERIDEFAULTCONFIG,           ID_VERIUSECLAUSE,               ID_VERICONFIGURATIONID,
    ID_VERIPULSECONTROL,            ID_VERIPATHPULSEVAL,            ID_VERINETDATATYPE,
    ID_VERIPATHPULSEVALPORTS,

    // System Verilog specific class-ids.
    ID_VERIINTERFACE,               ID_VERIPROGRAM,                 ID_VERICLASS,
    ID_VERIPROPERTYDECL,            ID_VERISEQUENCEDECL,
    ID_VERISTRUCTUNION,             ID_VERIENUM,                    ID_VERIASSERTION,
    ID_VERIIFOPERATOR,              ID_VERIMODPORT,                 ID_VERIMODPORTDECL,
    ID_VERIJUMPSTATEMENT,           ID_VERIDOTSTAR,                 ID_VERISEQUENCECONCAT,
    ID_VERICLOCKEDSEQUENCE,         ID_VERIASSIGNINSEQUENCE,        ID_VERIDOWHILE,
    ID_VERICLOCKINGDECL,            ID_VERICONSTRAINTDECL,          ID_VERIDISTOPERATOR,
    ID_VERISOLVEBEFORE,             ID_VERICAST,                    ID_VERINEW,
    ID_VERICONCATITEM,              ID_VERIBINDDIRECTIVE,           ID_VERIINTERFACEID,
    ID_VERIPROGRAMID,               ID_VERITYPEID,
    ID_VERIMODPORTID,               ID_VERINULL,                    ID_VERITIMELITERAL,
    ID_VERIDOLLAR,                  ID_VERIPACKAGE,                 ID_VERIINDEXEDEXPR,
    ID_VERIASSIGNMENTPATTERN,
    ID_VERIMULTIASSIGNMENTPATTERN,
    ID_VERIOPERATORID,              ID_VERIOPERATORBINDING,
    ID_VERIFOREACHOPERATOR,         ID_VERISTREAMINGCONCAT,         ID_VERINETALIAS,
    ID_VERITIMEUNIT,                ID_VERIWAITORDER,               ID_VERIFOREACH,
    ID_VERICONDPREDICATE,
    ID_VERIDOTNAME,
    ID_VERITAGGEDUNION,
    ID_VERICLOCKINGID,              ID_VERICLOCKINGDIRECTION,       ID_VERICLOCKINGSIGDECL,
    ID_VERIARRAYMETHODCALL,         ID_VERIINLINECONSTRAINTSTMT,    ID_VERIWITHEXPR,
    ID_VERIINLINECONSTRAINT,        ID_VERICOVERGROUP,
    ID_VERICOVERAGEOPTION,          ID_VERICOVERAGESPEC,            ID_VERIBINDECL,
    ID_VERIOPENRANGEBINVALUE,       ID_VERITRANSBINVALUE,           ID_VERIDEFAULTBINVALUE,
    ID_VERISELECTBINVALUE,          ID_VERITRANSSET,                ID_VERITRANSRANGELIST,
    ID_VERISELECTCONDITION,         ID_VERIRANDSEQUENCE,            ID_VERICODEBLOCK,
    ID_VERIPRODUCTION,              ID_VERIPRODUCTIONITEM,          ID_VERIPRODUCTIONID,
    ID_VERITYPEOPERATOR,            ID_VERIIMPORTDECL,              ID_VERISCOPENAME,
    ID_VERIPROTOTYPEID,
    ID_VERINAMEDPORT,               ID_VERIPATTERNMATCH,            ID_VERIBINSID,
    ID_VERICOVGOPTIONID,
    ID_VERISEQPROPERTYFORMAL,
    ID_VERICONSTRAINTSET,
    // SV-2009 specific class ids
    ID_VERILETDECL,                 ID_VERILETID,                   ID_VERICHECKERID,
    ID_VERICHECKER,                 ID_VERIDEFAULTDISABLEIFF,       ID_VERISEQUENTIALINSTANTIATION,
    ID_VERIEXPORTDECL,              ID_VERICASEOPERATOR,            ID_VERICASEOPERATORITEM,
    ID_VERITYPEREF,                 ID_VERIKEYWORD,

    ID_VERI_NAME_EXTENDED_EXPRESSION,

    ID_VERIDPIFUNCTIONDECL,         ID_VERIDPITASKDECL,             ID_VERIEXTERNFORKJOINTASKID,

    ID_VERINULLSTATEMENT,           ID_VERILABELID,
    ID_VERI_END_TABLE  // The last marker (unused)
};

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_CLASSIDS_H_
