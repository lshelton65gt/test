/*
 *
 * [ File Version : 1.103 - 2014/03/13 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_VISITOR_H_
#define _VERIFIC_VERI_VISITOR_H_

#include "VerificSystem.h"
#include "VeriCompileFlags.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// VIPER #4959: Define visitor class with different method names for
// different classes to improve functionality of the visitor pattern.
//
// The macro 'VERI_VISIT' defines the header of the visitor routine:
//     T is the type, ie, the class for which it is being defined
//     N is the name of the object of type T inside the visitor routine
// The macro 'VERI_VISIT_NODE' defines the call to the visitor routine:
//    T is the type, ie, the class for which it is being called
//    N is the name of the object of type T with which it is called
// The macro 'VERI_ACCEPT' defines the body of the Accept routine of a class:
//    T is the type, ie, the class to which this Accept routine belongs
//    V is the name of the object of Visitor with which it is called
#ifdef VERILOG_SPECIALIZED_VISITORS
    #define VERI_VISIT(T,N)       Visit##T(T &N)
    #define VERI_VISIT_NODE(T,N)  Visit##T(N)
#else
    #define VERI_VISIT(T,N)       Visit(T &N)
    #define VERI_VISIT_NODE(T,N)  Visit((T &)N)
#endif
#define VERI_ACCEPT(T,V)          (V).VERI_VISIT_NODE(T,(*this))

// The following are forward declarations of all parse-tree classes that
// the visitor method of VeriVisitor class accepts:

// Forward declarations from VeriTreeNode.h ---------------
class VeriNode ;               class VeriTreeNode ;             class VeriCommentNode ;
// Forward declarations from VeriModule.h -----------------
class VeriModule ;             class VeriPrimitive ;            class VeriConfiguration ;
// Forward declarations from VeriExpression.h -------------
class VeriExpression ;         class VeriName ;                 class VeriIdRef ;
class VeriIndexedId ;          class VeriSelectedName ;         class VeriIndexedMemoryId ;
class VeriConcat ;             class VeriMultiConcat ;          class VeriFunctionCall ;
class VeriSystemFunctionCall ; class VeriMinTypMaxExpr ;        class VeriUnaryOperator ;
class VeriBinaryOperator ;     class VeriQuestionColon ;        class VeriEventExpression ;
class VeriPortConnect ;        class VeriPortOpen ;             class VeriAnsiPortDecl ;
class VeriTimingCheckEvent ;   class VeriDataType ;             class VeriPathPulseVal ;
class VeriNetDataType ;
// Forward declarations from VeriId.h ---------------------
class VeriIdDef ;              class VeriVariable ;             //class VeriMemoryId ;
class VeriInstId ;             class VeriModuleId ;             class VeriUdpId ;
class VeriTaskId ;             class VeriFunctionId ;           class VeriGenVarId ;
class VeriParamId ;            class VeriBlockId ;              class VeriConfigurationId ;
// Forward declarations from VeriMisc.h -------------------
class VeriRange ;              class VeriStrength ;             class VeriNetRegAssign ;
/* class VeriInst ; */         class VeriPath ;                 class VeriCaseItem ;
class VeriGenerateCaseItem ;   class VeriDelayOrEventControl ;  class VeriDefParamAssign ;
class VeriConfigRule ;         class VeriInstanceConfig ;       class VeriCellConfig ;
class VeriDefaultConfig ;      class VeriUseClause ;            class VeriPathPulseValPorts ;
// Forward declarations from VeriModuleItem.h -------------
class VeriModuleItem ;         class VeriDataDecl ;             class VeriNetDecl ;
class VeriFunctionDecl ;       class VeriTaskDecl ;             class VeriDefParam ;
class VeriContinuousAssign ;   class VeriGateInstantiation ;    class VeriModuleInstantiation ;
class VeriSpecifyBlock ;       class VeriPathDecl ;
class VeriSystemTimingCheck ;  class VeriInitialConstruct ;     class VeriAlwaysConstruct ;
class VeriGenerateConstruct ;  class VeriGenerateConditional ;  class VeriGenerateCase ;
class VeriGenerateFor ;        class VeriGenerateBlock ;        class VeriTable ;
class VeriPulseControl ;
// Forward declarations from VeriStatement.h --------------
class VeriStatement ;          class VeriBlockingAssign ;       class VeriNonBlockingAssign ;
class VeriGenVarAssign ;       class VeriAssign ;               class VeriDeAssign ;
class VeriForce ;              class VeriRelease ;              class VeriTaskEnable ;
class VeriSystemTaskEnable ;   class VeriDelayControlStatement ;class VeriEventControlStatement ;
class VeriParBlock ;           class VeriCaseStatement ;        class VeriLoop ;
class VeriForever ;            class VeriRepeat ;               class VeriWhile ;
class VeriFor ;                class VeriWait ;                 class VeriDisable ;
class VeriEventTrigger ;       class VeriSeqBlock ;             class VeriConditionalStatement ;
class VeriNullStatement ;
// Forward declarations from VeriConstVal.h ---------------
class VeriConst ;              class VeriConstVal ;             class VeriIntVal ;
class VeriRealVal ;
// Forward declarations from VeriLibrary.h ---------------
class VeriLibraryDecl ;

class Map ;
class Array ;
// Forward declaration of System verilog class
class VeriInterface ;          class VeriProgram ;              class VeriClass ;
class VeriPropertyDecl ;       class VeriSequenceDecl ;         class VeriModport ;
class VeriModportDecl ;        class VeriClockingDecl ;         class VeriConstraintDecl ;
class VeriStructUnion ;        class VeriEnum ;                 class VeriDotStar ;
class VeriIfOperator ;         class VeriSequenceConcat ;       class VeriClockedSequence ;
class VeriAssignInSequence ;   class VeriDistOperator ;         class VeriSolveBefore ;
class VeriCast ;               class VeriNew ;                  class VeriConcatItem ;
class VeriAssertion ;          class VeriJumpStatement ;        class VeriDoWhile ;
class VeriBindDirective ;      class VeriInterfaceId ;          class VeriModportId ;
class VeriProgramId ;          class VeriTypeId ;
class VeriNull ;               class VeriTimeLiteral ;          class VeriDollar ;
class VeriPackage ;
class VeriChecker ;            class VeriCheckerId ;            class VeriSequentialInstantiation ;
class VeriAssignmentPattern ;    class VeriMultiAssignmentPattern ;
class VeriOperatorId ;         class VeriOperatorBinding ;      class VeriForeachOperator ;
class VeriStreamingConcat ;    class VeriNetAlias ;             class VeriTimeUnit ;
class VeriForeach ;            class VeriWaitOrder ;            class VeriCondPredicate ;
class VeriDotName ;
class VeriTaggedUnion ;          class VeriClockingId ;
class VeriClockingDirection ;  class VeriClockingSigDecl ;
class VeriInlineConstraint ;   class VeriWithStmt ;             class VeriArrayMethodCall ;
class VeriInlineConstraintStmt ;
class VeriClockingDirection ;  class VeriClockingSigDecl ;      class VeriWith ;
class VeriWithExpr ;           class VeriInlineConstraint ;     class VeriWithStmt ;
class VeriArrayMethodCall ;    class VeriInlineConstraintStmt ; class VeriCovergroup ;
class VeriCoverageOption ;     class VeriCoverageSpec ;         class VeriBinDecl ;
class VeriBinValue ;           class VeriOpenRangeBinValue ;    class VeriTransBinValue ;
class VeriDefaultBinValue ;    class VeriSelectBinValue ;       class VeriTransSet ;
class VeriTransRangeList ;     class VeriSelectCondition  ;
class VeriRandsequence ;       class VeriCodeBlock ;            class VeriProduction ;
class VeriProductionItem ;     class VeriProductionId ;         class VeriTypeOperator ;
class VeriNamedPort ;          class VeriPatternMatch ;         class VeriCovgOptionId ;
class VeriSeqPropertyFormal ;
class VeriScopeName ;            class VeriConstraintSet ;
class VeriImportDecl ;         class VeriLetDecl ;              class VeriLetId ;
class VeriDefaultDisableIff ;  class VeriBinsId ;               class VeriExportDecl ;
class VeriCaseOperator ;       class VeriCaseOperatorItem ;     class VeriIndexedExpr ;
class VeriDPIFunctionDecl ;    class VeriDPITaskDecl ;          class VeriExternForkjoinTaskId ;
class VeriLabelId ;
class VeriTypeRef ;            class VeriKeyword ;              class VeriPrototypeId ;

/* -------------------------------------------------------------- */

/*
    A Visitor class lets you define a new operation on a tree (or data structure)
    without changing the classes of the elements on which it operates (tree nodes).
    In essence, it lets you keep related operations together by defining them in
    one class. To get this to work, the following must occur:

    1) Declare a abstract base Visitor class which contains a "void VERI_VISIT(<type>&)"
       method for every <type> of element (VeriNode) the Visitor will traverse.
       This has already been done, and is located here in this file (VeriVisitor.h).

    2) Declare a "void Accept(VeriVisitor &)" method for every element (VeriTreeNode) class.

    3) To create your particular Visitor class, derive from VeriVisitor, and
       implement only the VERI_VISIT() methods needed for your particular algorithm.

    4) Please note that starting from April 2009 release VERI_VISIT is a macro defined above.
       Depending on the compile switch VERILOG_SPECIALIZED_VISITORS the definition of
       the macro VERI_VISIT changes. Please look into VeriCompileFlags.h for more details.
*/

class VFC_DLL_PORT VeriVisitor
{
protected:
    VeriVisitor() { } ;

public:
    virtual ~VeriVisitor() { } ;

private:
    // Prevent compiler from defining the following
    VeriVisitor(const VeriVisitor &) ;            // Purposely leave unimplemented
    VeriVisitor& operator=(const VeriVisitor &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // The following class definitions can be found in VeriTreeNode.h
    virtual void VERI_VISIT(VeriTreeNode, node) ;
    virtual void VERI_VISIT(VeriCommentNode, node) ;

    // The following class definitions can be found in VeriModule.h
    virtual void VERI_VISIT(VeriModule, node) ;
    virtual void VERI_VISIT(VeriPrimitive, node) ;
    virtual void VERI_VISIT(VeriConfiguration, node) ;

    // The following class definitions can be found in VeriExpression.h
    virtual void VERI_VISIT(VeriExpression, node) ;
    virtual void VERI_VISIT(VeriName, node) ;
    virtual void VERI_VISIT(VeriIdRef, node) ;
    virtual void VERI_VISIT(VeriIndexedId, node) ;
    virtual void VERI_VISIT(VeriSelectedName, node) ;
    virtual void VERI_VISIT(VeriIndexedMemoryId, node) ;
    virtual void VERI_VISIT(VeriConcat, node) ;
    virtual void VERI_VISIT(VeriMultiConcat, node) ;
    virtual void VERI_VISIT(VeriFunctionCall, node) ;
    virtual void VERI_VISIT(VeriSystemFunctionCall, node) ;
    virtual void VERI_VISIT(VeriMinTypMaxExpr, node) ;
    virtual void VERI_VISIT(VeriUnaryOperator, node) ;
    virtual void VERI_VISIT(VeriBinaryOperator, node) ;
    virtual void VERI_VISIT(VeriQuestionColon, node) ;
    virtual void VERI_VISIT(VeriEventExpression, node) ;
    virtual void VERI_VISIT(VeriPortConnect, node) ;
    virtual void VERI_VISIT(VeriPortOpen, node) ;
    virtual void VERI_VISIT(VeriAnsiPortDecl, node) ;
    virtual void VERI_VISIT(VeriTimingCheckEvent, node) ;
    virtual void VERI_VISIT(VeriDataType, node) ;
    virtual void VERI_VISIT(VeriNetDataType, node) ; // VIPER #4896
    virtual void VERI_VISIT(VeriPathPulseVal, node) ;

    // The following class definitions can be found in VeriId.h
    virtual void VERI_VISIT(VeriIdDef, node) ;
    virtual void VERI_VISIT(VeriVariable, node) ;
    //virtual void VERI_VISIT(VeriMemoryId, node) ;
    virtual void VERI_VISIT(VeriInstId, node) ;
    virtual void VERI_VISIT(VeriModuleId, node) ;
    virtual void VERI_VISIT(VeriUdpId, node) ;
    virtual void VERI_VISIT(VeriConfigurationId, node) ;
    virtual void VERI_VISIT(VeriTaskId, node) ;
    virtual void VERI_VISIT(VeriFunctionId, node) ;
    virtual void VERI_VISIT(VeriGenVarId, node) ;
    virtual void VERI_VISIT(VeriParamId, node) ;
    virtual void VERI_VISIT(VeriBlockId, node) ;

    // The following class definitions can be found in VeriMisc.h
    virtual void VERI_VISIT(VeriRange, node) ;
    virtual void VERI_VISIT(VeriStrength, node) ;
    virtual void VERI_VISIT(VeriNetRegAssign, node) ;
    virtual void VERI_VISIT(VeriDefParamAssign, node) ;
    virtual void VERI_VISIT(VeriCaseItem, node) ;
    virtual void VERI_VISIT(VeriGenerateCaseItem, node) ;
    virtual void VERI_VISIT(VeriPath, node) ;
    virtual void VERI_VISIT(VeriDelayOrEventControl, node) ;
    virtual void VERI_VISIT(VeriConfigRule, node) ;
    virtual void VERI_VISIT(VeriInstanceConfig, node) ;
    virtual void VERI_VISIT(VeriCellConfig, node) ;
    virtual void VERI_VISIT(VeriDefaultConfig, node) ;
    virtual void VERI_VISIT(VeriUseClause, node) ;
#ifdef VERILOG_PATHPULSE_PORTS
    virtual void VERI_VISIT(VeriPathPulseValPorts, node) ;
#endif

    // The following class definitions can be found in VeriModuleItem.h
    virtual void VERI_VISIT(VeriModuleItem, node) ;
    virtual void VERI_VISIT(VeriDataDecl, node) ;
    virtual void VERI_VISIT(VeriNetDecl, node) ;
    virtual void VERI_VISIT(VeriFunctionDecl, node) ;
    virtual void VERI_VISIT(VeriTaskDecl, node) ;
    virtual void VERI_VISIT(VeriDefParam, node) ;
    virtual void VERI_VISIT(VeriContinuousAssign, node) ;
    virtual void VERI_VISIT(VeriGateInstantiation, node) ;
    virtual void VERI_VISIT(VeriModuleInstantiation, node) ;
    virtual void VERI_VISIT(VeriSpecifyBlock, node) ;
    virtual void VERI_VISIT(VeriPathDecl, node) ;
    virtual void VERI_VISIT(VeriSystemTimingCheck, node) ;
    virtual void VERI_VISIT(VeriInitialConstruct, node) ;
    virtual void VERI_VISIT(VeriAlwaysConstruct, node) ;
    virtual void VERI_VISIT(VeriGenerateConstruct, node) ;
    virtual void VERI_VISIT(VeriGenerateConditional, node) ;
    virtual void VERI_VISIT(VeriGenerateCase, node) ;
    virtual void VERI_VISIT(VeriGenerateFor, node) ;
    virtual void VERI_VISIT(VeriGenerateBlock, node) ;
    virtual void VERI_VISIT(VeriTable, node) ;
    virtual void VERI_VISIT(VeriPulseControl, node) ;

    // The following class definitions can be found in VeriStatement.h
    virtual void VERI_VISIT(VeriStatement, node) ;
    virtual void VERI_VISIT(VeriNullStatement, node) ;
    virtual void VERI_VISIT(VeriBlockingAssign, node) ;
    virtual void VERI_VISIT(VeriNonBlockingAssign, node) ;
    virtual void VERI_VISIT(VeriGenVarAssign, node) ;
    virtual void VERI_VISIT(VeriAssign, node) ;
    virtual void VERI_VISIT(VeriDeAssign, node) ;
    virtual void VERI_VISIT(VeriForce, node) ;
    virtual void VERI_VISIT(VeriRelease, node) ;
    virtual void VERI_VISIT(VeriTaskEnable, node) ;
    virtual void VERI_VISIT(VeriSystemTaskEnable, node) ;
    virtual void VERI_VISIT(VeriDelayControlStatement, node) ;
    virtual void VERI_VISIT(VeriEventControlStatement, node) ;
    virtual void VERI_VISIT(VeriConditionalStatement, node) ;
    virtual void VERI_VISIT(VeriCaseStatement, node) ;
    virtual void VERI_VISIT(VeriLoop, node) ;
    virtual void VERI_VISIT(VeriForever, node) ;
    virtual void VERI_VISIT(VeriRepeat, node) ;
    virtual void VERI_VISIT(VeriWhile, node) ;
    virtual void VERI_VISIT(VeriFor, node) ;
    virtual void VERI_VISIT(VeriWait, node) ;
    virtual void VERI_VISIT(VeriDisable, node) ;
    virtual void VERI_VISIT(VeriEventTrigger, node) ;
    virtual void VERI_VISIT(VeriSeqBlock, node) ;
    virtual void VERI_VISIT(VeriParBlock, node) ;

    // The following class definitions can be found in VeriConstVal.h
    virtual void VERI_VISIT(VeriConst, node) ;
    virtual void VERI_VISIT(VeriConstVal, node) ;
    virtual void VERI_VISIT(VeriIntVal, node) ;
    virtual void VERI_VISIT(VeriRealVal, node) ;

    // The following class definitions can be found in VeriLibrary.h
    virtual void VERI_VISIT(VeriLibraryDecl, node) ;

    // The System Verilog Class
    virtual void VERI_VISIT(VeriInterface, node) ;
    virtual void VERI_VISIT(VeriProgram, node) ;
    virtual void VERI_VISIT(VeriClass, node) ;
    virtual void VERI_VISIT(VeriPropertyDecl, node) ;
    virtual void VERI_VISIT(VeriSequenceDecl, node) ;
    virtual void VERI_VISIT(VeriModport, node) ;
    virtual void VERI_VISIT(VeriModportDecl, node) ;
    virtual void VERI_VISIT(VeriClockingDecl, node) ;
    virtual void VERI_VISIT(VeriConstraintDecl, node) ;
    virtual void VERI_VISIT(VeriBindDirective, node) ;
    virtual void VERI_VISIT(VeriPackage, node) ; // Std SV 1800 addition
    virtual void VERI_VISIT(VeriChecker, node) ; // Std SV 1800 addition
    virtual void VERI_VISIT(VeriSequentialInstantiation, node) ; // Std SV 1800 addition
    virtual void VERI_VISIT(VeriTypeRef, node) ;
    virtual void VERI_VISIT(VeriKeyword, node) ;
    virtual void VERI_VISIT(VeriStructUnion, node) ;
    virtual void VERI_VISIT(VeriEnum, node) ;
    virtual void VERI_VISIT(VeriDotStar, node) ;
    virtual void VERI_VISIT(VeriIfOperator, node) ;
    virtual void VERI_VISIT(VeriSequenceConcat, node) ;
    virtual void VERI_VISIT(VeriClockedSequence, node) ;
    virtual void VERI_VISIT(VeriAssignInSequence, node) ;
    virtual void VERI_VISIT(VeriDistOperator, node) ;
    virtual void VERI_VISIT(VeriSolveBefore, node) ;
    virtual void VERI_VISIT(VeriCast, node) ;
    virtual void VERI_VISIT(VeriNew, node) ;
    virtual void VERI_VISIT(VeriConcatItem, node) ;
    virtual void VERI_VISIT(VeriAssertion, node) ;
    virtual void VERI_VISIT(VeriJumpStatement, node) ;
    virtual void VERI_VISIT(VeriDoWhile, node) ;
    virtual void VERI_VISIT(VeriInterfaceId, node) ;
    virtual void VERI_VISIT(VeriProgramId, node) ;
    virtual void VERI_VISIT(VeriCheckerId, node) ;
    virtual void VERI_VISIT(VeriTypeId, node) ;
    virtual void VERI_VISIT(VeriModportId, node) ;
    virtual void VERI_VISIT(VeriNull, node) ;
    virtual void VERI_VISIT(VeriDollar, node) ;
    virtual void VERI_VISIT(VeriAssignmentPattern, node) ;
    virtual void VERI_VISIT(VeriMultiAssignmentPattern, node) ;
    virtual void VERI_VISIT(VeriTimeLiteral, node) ;
    virtual void VERI_VISIT(VeriOperatorId, node) ;
    virtual void VERI_VISIT(VeriOperatorBinding, node) ;
    virtual void VERI_VISIT(VeriForeachOperator, node) ;
    virtual void VERI_VISIT(VeriStreamingConcat, node) ;
    virtual void VERI_VISIT(VeriNetAlias, node) ;
    virtual void VERI_VISIT(VeriTimeUnit, node) ;
    virtual void VERI_VISIT(VeriForeach, node) ;
    virtual void VERI_VISIT(VeriWaitOrder, node) ;
    virtual void VERI_VISIT(VeriCondPredicate, node) ;
    virtual void VERI_VISIT(VeriDotName, node) ;
    virtual void VERI_VISIT(VeriTaggedUnion, node) ;
    virtual void VERI_VISIT(VeriClockingId, node) ;
    virtual void VERI_VISIT(VeriClockingDirection, node) ;
    virtual void VERI_VISIT(VeriClockingSigDecl, node) ;
    virtual void VERI_VISIT(VeriRandsequence, node) ;
    virtual void VERI_VISIT(VeriCodeBlock, node) ;
    virtual void VERI_VISIT(VeriProduction, node) ;
    virtual void VERI_VISIT(VeriProductionItem, node) ;
    virtual void VERI_VISIT(VeriProductionId, node) ;
    virtual void VERI_VISIT(VeriWith, node) ;
    virtual void VERI_VISIT(VeriWithExpr, node) ;
    virtual void VERI_VISIT(VeriInlineConstraint, node) ;
    virtual void VERI_VISIT(VeriWithStmt, node) ;
    virtual void VERI_VISIT(VeriArrayMethodCall, node) ;
    virtual void VERI_VISIT(VeriInlineConstraintStmt, node) ;
    virtual void VERI_VISIT(VeriCovergroup, node) ;
    virtual void VERI_VISIT(VeriCoverageOption, node) ;
    virtual void VERI_VISIT(VeriCoverageSpec, node) ;
    virtual void VERI_VISIT(VeriBinDecl, node) ;
    virtual void VERI_VISIT(VeriBinValue, node) ;
    virtual void VERI_VISIT(VeriOpenRangeBinValue, node) ;
    virtual void VERI_VISIT(VeriTransBinValue, node) ;
    virtual void VERI_VISIT(VeriDefaultBinValue, node) ;
    virtual void VERI_VISIT(VeriSelectBinValue, node) ;
    virtual void VERI_VISIT(VeriTransSet, node) ;
    virtual void VERI_VISIT(VeriTransRangeList, node) ;
    virtual void VERI_VISIT(VeriSelectCondition, node) ;
    virtual void VERI_VISIT(VeriTypeOperator, node) ;
    virtual void VERI_VISIT(VeriImportDecl, node) ;
    virtual void VERI_VISIT(VeriLetDecl, node) ;
    virtual void VERI_VISIT(VeriDefaultDisableIff, node) ;
    virtual void VERI_VISIT(VeriExportDecl, node) ;
    virtual void VERI_VISIT(VeriScopeName, node) ;
    virtual void VERI_VISIT(VeriNamedPort, node) ;
    virtual void VERI_VISIT(VeriPatternMatch, node) ;
    virtual void VERI_VISIT(VeriConstraintSet, node) ;
    virtual void VERI_VISIT(VeriCovgOptionId, node) ;
    virtual void VERI_VISIT(VeriSeqPropertyFormal, node) ;
    virtual void VERI_VISIT(VeriBinsId, node) ;
    virtual void VERI_VISIT(VeriLetId, node) ;
    virtual void VERI_VISIT(VeriCaseOperator, node) ;
    virtual void VERI_VISIT(VeriCaseOperatorItem, node) ;
    virtual void VERI_VISIT(VeriIndexedExpr, node) ;
    virtual void VERI_VISIT(VeriDPIFunctionDecl, node) ;
    virtual void VERI_VISIT(VeriDPITaskDecl, node) ;
    virtual void VERI_VISIT(VeriExternForkjoinTaskId, node) ;
    virtual void VERI_VISIT(VeriLabelId, node) ;
    virtual void VERI_VISIT(VeriPrototypeId, node) ;

protected:
    // Utility Traverse Methods
    virtual void TraverseNode(VeriTreeNode *node) ;
    virtual void TraverseAttributes(const Map *attrs) ;
    virtual void TraverseArray(const Array *array) ;
} ; // class VeriVisitor

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_VISITOR_H_
