/*
 *
 * [ File Version : 1.917 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>   // for strchr
#include <ctype.h>    // for isalpha, isdigit
#include <stdio.h>    // for sprintf
#include <math.h>     // for floor
#include <stdlib.h>   // for strtod, strtol

#include "veri_file.h"
#include "veri_tokens.h"

#include "Strings.h"
#include "Map.h"
#include "Set.h"
#include "Message.h"

#include "VeriTreeNode.h"
#include "VeriScope.h"
#include "VeriId.h"
#include "VeriMisc.h"
#include "VeriExpression.h"
#include "VeriConstVal.h"

#include "VeriModule.h"
#include "VeriModuleItem.h"

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
#include "../vhdl/vhdl_file.h"
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/VhdlCopy.h"
#endif

#include "../vhdl/VhdlName.h"
#include "../vhdl/VhdlStatement.h"
#include "VeriBaseValue_Stat.h"

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Conversion of vhdl package to verilog one
#include "../vhdl/vhdl_file.h"
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/VhdlIdDef.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/************************ VeriNode ******************************/

Map *VeriNode::_msgs = 0 ;
Map *VeriNode::_relaxed_msgs = 0 ;
Map *VeriNode::_udp_edge_chars = 0 ;
Map *VeriNode::_verilog_keywords = 0 ;
VeriExpression *VeriNode::_sva_clock_expr = 0 ;
VeriExpression *VeriNode::_default_disable_iff_cond = 0 ;
unsigned VeriNode::_contains_disable_iff_cond = 0 ;
unsigned VeriNode::_contains_event_control = 0 ;
unsigned VeriNode::_contains_delay_control = 0 ;
unsigned VeriNode::_contains_even_id_in_body = 0 ;
unsigned VeriNode::_multiple_inferred_clock = 0 ;
unsigned VeriNode::_in_property_expr = 0 ;
unsigned VeriNode::_is_static_elab = 0 ;
Map *VeriNode::_ref_count_map = 0 ;
unsigned  VeriNode::_has_delay_or_event_control = 0 ;
unsigned  VeriNode::_potential_always_loop = 0 ; // VIPER #7506
unsigned VeriNode::_under_default_clock = 0 ;

// Initialization of static variables :
Map *VeriNode::_attribute_map = 0 ;
Map *VeriNode::_map_of_comment_arr = 0 ;
VeriScope *VeriNode::_present_scope = 0 ;

VeriNode::VeriNode()
{
}

VeriNode::~VeriNode()
{
}

/************************ VeriTreeNode ******************************/

VeriTreeNode::VeriTreeNode(linefile_type linefile) :
    VeriNode(),
    _linefile(linefile)
{
    // If no linefile is given, take the one from the last syntax rule parsed
    // The constructor most likely applies to the the last syntax rule, so pick that info up.
    if (!_linefile) _linefile = veri_file::GetRuleLineFile() ;

    if (_linefile && IsTickProtectedRtlRegion(1)) {
        // This is protected RTL. Modify the _linefile to carry only filename
        // and 0 as the line number. This is a special linefile to denote the
        // protected RTL region
        const char *file_name = LineFile::GetFileName(_linefile) ;
        if (file_name) _linefile = LineFile::EncodeLineFile(file_name, 0) ;
    }
}

VeriTreeNode::VeriTreeNode(VeriTreeNode &base, unsigned copy_original_linefile):VeriNode(),_linefile(0)
{
    _linefile = copy_original_linefile ? base._linefile : veri_file::GetRuleLineFile() ;

    // Move comments  from 'base' to 'this' node :
    AddComments(base.TakeComments()) ;

    // Move attributes from 'base' to 'this' node :
    AddAttributes(base.TakeAttributes()) ;
}

VeriTreeNode::~VeriTreeNode()
{
    // VIPER #6906: Attributes and comments are now deleted from VeriTreeNode destructor only:
    DeleteAttributes() ;
    DeleteComments(TakeComments()) ;
}

// Tree-accessable analysis mode backward compatibility routines (analysis_mode setting is now in veri_file)
unsigned VeriNode::IsVeri95()          { return (veri_file::GetAnalysisMode() == veri_file::VERILOG_95) ? 1 : 0 ; }
unsigned VeriNode::IsVeri2001()        { return (veri_file::GetAnalysisMode() == veri_file::VERILOG_2K) ? 1 : 0 ; }
unsigned VeriNode::IsSystemVeri2005()  { return (veri_file::GetAnalysisMode() == veri_file::SYSTEM_VERILOG_2005) ? 1 : 0 ; }
unsigned VeriNode::IsSystemVeri2009()  { return (veri_file::GetAnalysisMode() == veri_file::SYSTEM_VERILOG_2009) ? 1 : 0 ; } // SYSV 2009 only
unsigned VeriNode::IsSystemVeri2012()  { return (veri_file::GetAnalysisMode() == veri_file::SYSTEM_VERILOG) ? 1 : 0 ; } // SYSV 2012 only
unsigned VeriNode::IsSystemVeri()      { return (IsSystemVeri2012() || IsSystemVeri2005() || IsSystemVeri2009()) ? 1 : 0 ; } // All System Verilog dialects
unsigned VeriNode::IsSystemVeri2009OrLater() { return (IsSystemVeri() && (!IsSystemVeri2005())) ? 1 : 0 ; } // All System Verilog dialects
unsigned VeriNode::IsPSL()             { return (veri_file::GetAnalysisMode() == veri_file::VERILOG_PSL) ? 1 : 0 ; }
unsigned VeriNode::IsAms()             { return (veri_file::GetAnalysisMode() == veri_file::VERILOG_AMS) ? 1 : 0 ; }
void     VeriNode::SetVeri95()         { veri_file::SetAnalysisMode(veri_file::VERILOG_95) ; }
void     VeriNode::SetVeri2001()       { veri_file::SetAnalysisMode(veri_file::VERILOG_2K) ; }
void     VeriNode::SetSystemVeri2005() { veri_file::SetAnalysisMode(veri_file::SYSTEM_VERILOG_2005) ; }
void     VeriNode::SetSystemVeri2009() { veri_file::SetAnalysisMode(veri_file::SYSTEM_VERILOG_2009) ; }
void     VeriNode::SetSystemVeri()     { veri_file::SetAnalysisMode(veri_file::SYSTEM_VERILOG) ; }
void     VeriNode::SetPSL()            { veri_file::SetAnalysisMode(veri_file::VERILOG_PSL) ; }
void     VeriNode::SetAms()            { veri_file::SetAnalysisMode(veri_file::VERILOG_AMS) ; }

/* static */ Set *VeriNode::_system_tasks = 0 ;

//
/* static */ void VeriNode::CheckStreamingConcat(const VeriExpression *lval, const VeriExpression* val)
{
    // VIPER #4800 : Produce error if streaming concatenation is larger than
    // other side of assignment.

    if (lval && val && (lval->IsStreamingConcat() || val->IsStreamingConcat())) {
        verific_int64 lsize = lval->StaticSizeSignInternal(0, 0 /* constraint? */) ;
        verific_uint64 left = GET_CONTEXT_SIZE(lsize) ;
        verific_int64 rsize = val->StaticSizeSignInternal(0, 0 /* constraint? */) ;
        verific_uint64 right = GET_CONTEXT_SIZE(rsize) ;
        if (lval->IsStreamingConcat() && left && right && (left > right)) {
            lval->Error("width does not match, stream larger than %s", "value") ;
        }
        if (val->IsStreamingConcat() && left && right && (right > left)) {
            val->Error ("width does not match, stream larger than %s", "target") ;
        }
    }
}

/* static */ unsigned
VeriNode::IsSystemTask(const char *name)
{
    if (!name) return 0 ;

    if (!_system_tasks) {
        _system_tasks = new Set(STRING_HASH, 127) ;

        // Verilog 95/2001:
        _system_tasks->Insert("display") ;
        _system_tasks->Insert("strobe") ;
        _system_tasks->Insert("displayb") ;
        _system_tasks->Insert("strobeb") ;
        _system_tasks->Insert("displayh") ;
        _system_tasks->Insert("strobeh") ;
        _system_tasks->Insert("displayo") ;
        _system_tasks->Insert("strobeo") ;
        _system_tasks->Insert("monitor") ;
        _system_tasks->Insert("write") ;
        _system_tasks->Insert("monitorb") ;
        _system_tasks->Insert("writeb") ;
        _system_tasks->Insert("monitorh") ;
        _system_tasks->Insert("writeh") ;
        _system_tasks->Insert("monitoro") ;
        _system_tasks->Insert("writeo") ;
        _system_tasks->Insert("monitoroff") ;
        _system_tasks->Insert("monitoron") ;

        _system_tasks->Insert("fclose") ;
        //_system_tasks->Insert("fopen") ; // Both? Confusing LRM
        _system_tasks->Insert("fdisplay") ;
        _system_tasks->Insert("fstrobe") ;
        _system_tasks->Insert("fdisplayb") ;
        _system_tasks->Insert("fstrobeb") ;
        _system_tasks->Insert("fdisplayh") ;
        _system_tasks->Insert("fstrobeh") ;
        _system_tasks->Insert("fdisplayo") ;
        _system_tasks->Insert("fstrobeo") ;
        //_system_tasks->Insert("fgetc") ; // Both? Confusing LRM
        //_system_tasks->Insert("ungetc") ; // Both? Confusing LRM
        _system_tasks->Insert("fflush") ;
        //_system_tasks->Insert("ferror") ; // Both? Confusing LRM
        //_system_tasks->Insert("fgets") ; // Both? Confusing LRM
        //_system_tasks->Insert("rewind") ; // Both? Confusing LRM
        _system_tasks->Insert("fmonitor") ;
        _system_tasks->Insert("fwrite") ;
        _system_tasks->Insert("fmonitorb") ;
        _system_tasks->Insert("fwriteb") ;
        _system_tasks->Insert("fmonitorh") ;
        _system_tasks->Insert("fwriteh") ;
        _system_tasks->Insert("fmonitoro") ;
        _system_tasks->Insert("fwriteo") ;
        _system_tasks->Insert("readmemb") ;
        _system_tasks->Insert("readmemh") ;
        _system_tasks->Insert("swrite") ;
        _system_tasks->Insert("swriteb") ;
        _system_tasks->Insert("swriteo") ;
        _system_tasks->Insert("swriteh") ;
        //_system_tasks->Insert("sformat") ; // Both? Confusing LRM
        _system_tasks->Insert("sdf_annotate") ;
        //_system_tasks->Insert("fscanf") ; // Both? Confusing LRM
        //_system_tasks->Insert("fsscanf") ; // Both? Confusing LRM
        //_system_tasks->Insert("fread") ; // Both? Confusing LRM
        //_system_tasks->Insert("ftell") ; // Both? Confusing LRM
        //_system_tasks->Insert("fseek") ; // Both? Confusing LRM

        _system_tasks->Insert("printtimescale") ;
        _system_tasks->Insert("timeformat") ;

        _system_tasks->Insert("finish") ;
        _system_tasks->Insert("stop") ;

        /* IEEE 1364 LRM, section 15.1: Although they begin with a $, timing checks
           are not system tasks. The leading $ is present because of historical reasons,
           and timing checks shall not be confused with system tasks. In particular,
           no system task can appear in a specify block, and no timing check can appear
           in procedural code.

           List in the section 17 includes them (the following) as timing check tasks!! */

        _system_tasks->Insert("hold") ;
        _system_tasks->Insert("nochange") ;
        _system_tasks->Insert("period") ;
        _system_tasks->Insert("recovery") ;
        _system_tasks->Insert("setup") ;
        _system_tasks->Insert("setuphold") ;
        _system_tasks->Insert("skew") ;
        _system_tasks->Insert("width") ;
        _system_tasks->Insert("removal") ;
        _system_tasks->Insert("recrem") ;
        _system_tasks->Insert("timeskew") ;
        _system_tasks->Insert("fullskew") ;

        _system_tasks->Insert("async$and$array") ;
        _system_tasks->Insert("async$and$plane") ;
        _system_tasks->Insert("async$nand$array") ;
        _system_tasks->Insert("async$nand$plane") ;
        _system_tasks->Insert("async$or$array") ;
        _system_tasks->Insert("async$or$plane") ;
        _system_tasks->Insert("async$nor$array") ;
        _system_tasks->Insert("async$nor$plane") ;
        _system_tasks->Insert("sync$and$array") ;
        _system_tasks->Insert("sync$and$plane") ;
        _system_tasks->Insert("sync$nand$array") ;
        _system_tasks->Insert("sync$nand$plane") ;
        _system_tasks->Insert("sync$or$array") ;
        _system_tasks->Insert("sync$or$plane") ;
        _system_tasks->Insert("sync$nor$array") ;
        _system_tasks->Insert("sync$nor$plane") ;

        _system_tasks->Insert("q_initialize") ;
        _system_tasks->Insert("q_add") ;
        _system_tasks->Insert("q_remove") ;
        //_system_tasks->Insert("q_full") ;  // System function
        _system_tasks->Insert("q_exam") ;

        // Not sure
        //_system_tasks->Insert("test$plusargs") ;
        //_system_tasks->Insert("value$plusargs") ;

        _system_tasks->Insert("dumpvars") ;
        _system_tasks->Insert("dumpall") ;
        _system_tasks->Insert("dumpon") ;
        _system_tasks->Insert("dumpoff") ;
        _system_tasks->Insert("dumpfile") ;
        _system_tasks->Insert("dumplimit") ;
        _system_tasks->Insert("dumpflush") ;
        _system_tasks->Insert("dumpports") ;
        _system_tasks->Insert("dumpportson") ;
        _system_tasks->Insert("dumpportsoff") ;
        _system_tasks->Insert("dumpportsall") ;
        _system_tasks->Insert("dumpportslimit") ;
        _system_tasks->Insert("dumpportsflush") ;

        // System Verilog:
        _system_tasks->Insert("assertkill") ;
        _system_tasks->Insert("assertoff") ;
        _system_tasks->Insert("asserton") ;

        _system_tasks->Insert("fatal") ;
        _system_tasks->Insert("error") ;
        _system_tasks->Insert("warning") ;
        _system_tasks->Insert("info") ;

        //_system_tasks->Insert("cast") ; // Both System function and system task
        _system_tasks->Insert("exit") ;

        _system_tasks->Insert("writememb") ;
        _system_tasks->Insert("writememh") ;

        // Not properly mentioned in the IEEE 1800 LRM:
        //_system_tasks->Insert("set_coverage_db_name") ;
        //_system_tasks->Insert("load_coverage_db") ;
    }

    return (_system_tasks->GetItem(name) ? 1 : 0) ;
}

/* static */ void
VeriNode::ResetSystemTasksTable()
{
    delete _system_tasks ;
    _system_tasks = 0 ;
}

unsigned VeriTreeNode::IsTickProtectedNode() const
{
    // old way
    if (GetQualifier(VERI_TICK_PROTECTED)) return 1 ;

    // Viper #8188 / 8208
    // VHDL LRM 1076_2008 Section 24.1
    // We reserve Linefile field state (file_name && !line_number) for protected nodes.
    return LineFile::IsProtectedLinefile(_linefile) ;
}

/************************ PrettyPrint support ***********************/

/* 80 spaces */
static const char *spaces = "                                                                                " ;

const char *
VeriNode::PrintLevel(unsigned level)
{
    /* Return a pointer to a string of level*4 characters */
    /* Don't overflow on the fixed-length spaces constant */
    unsigned space = (unsigned)Strings::len(spaces) ;
    if (4*level > space) return spaces ;
    return spaces+space-4*level ;
}

const char *
VeriNode::PrintToken(unsigned veri_token)
{
    // Get a string for a token
    switch (veri_token) {
    case VERI_OPAREN :      return "(" ;
    case VERI_CPAREN :      return ")" ;
    case VERI_MODULUS :     return "%" ;
    case VERI_PLUS :        return "+" ;
    case VERI_MIN :         return "-" ;
    case VERI_MUL :         return "*" ;
    case VERI_DIV :         return "/" ;
    case VERI_GT :          return ">" ;
    case VERI_LT :          return "<" ;
    case VERI_GEQ :         return ">=" ;
    case VERI_LEQ :         return "<=" ;
    case VERI_CASEEQ :      return "===" ;
    case VERI_CASENEQ :     return "!==" ;
    case VERI_LOGEQ :       return "==" ;
    case VERI_LOGNEQ :      return "!=" ;
    case VERI_LOGNOT :      return "!" ;
    case VERI_LOGAND :      return "&&" ;
    case VERI_LOGOR :       return "||" ;
    case VERI_REDNOT :      return "~" ;
    case VERI_REDAND :      return "&" ;
    case VERI_REDOR :       return "|" ;
    case VERI_REDXOR :      return "^" ;
    case VERI_REDNAND :     return "~&" ;
    case VERI_REDNOR :      return "~|" ;
    case VERI_REDXNOR :     return "~^" ;
    case VERI_LSHIFT :      return "<<" ;
    case VERI_RSHIFT :      return ">>" ;
    case VERI_QUESTION :    return "?" ;
    case VERI_COLON :       return ":" ;
    case VERI_ALLPATH :     return "*>" ;
    case VERI_LEADTO :      return "=>" ;
    case VERI_RIGHTARROW :  return "->" ;

    case VERI_ALWAYS :      return "always" ;
    case VERI_AND :         return "and" ;
    case VERI_SEQ_AND :     return "and" ;
    case VERI_PROPERTY_AND :return "and" ;
    case VERI_LOG_EQUIVALENCE : return "<->" ;
    case VERI_ASSIGN :      return "assign" ;
    case VERI_BEGIN :       return "begin" ;
    case VERI_BUF :         return "buf" ;
    case VERI_BUFIF0 :      return "bufif0" ;
    case VERI_BUFIF1 :      return "bufif1" ;
    case VERI_CASE :        return "case" ;
    case VERI_CASEX :       return "casex" ;
    case VERI_CASEZ :       return "casez" ;
    case VERI_CMOS :        return "cmos" ;
    case VERI_DEASSIGN :    return "deassign" ;
    case VERI_DEFAULT :     return "default" ;
    case VERI_DEFPARAM :    return "defparam" ;
    case VERI_DISABLE :     return "disable" ;
    case VERI_EDGE :        return "edge" ;
    case VERI_ELSE :        return "else" ;
    case VERI_END :         return "end" ;
    case VERI_ENDCASE :     return "endcase" ;
    case VERI_ENDFUNCTION : return "endfunction" ;
    case VERI_ENDMODULE :   return "endmodule" ;
    case VERI_ENDPRIMITIVE : return "endprimitive" ;
    case VERI_ENDSPECIFY :  return "endspecify" ;
    case VERI_ENDTABLE :    return "endtable" ;
    case VERI_ENDTASK :     return "endtask" ;
    case VERI_EVENT :       return "event" ;
    case VERI_FOR :         return "for" ;
    case VERI_FORCE :       return "force" ;
    case VERI_FOREVER :     return "forever" ;
    case VERI_FORK :        return "fork" ;
    case VERI_FUNCTION :    return "function" ;
    case VERI_HIGHZ0 :      return "highz0" ;
    case VERI_HIGHZ1 :      return "highz1" ;
    case VERI_IF :          return "if" ;
    case VERI_IFNONE :      return "ifnone" ;
    case VERI_INITIAL :     return "initial" ;
    case VERI_INOUT :       return "inout" ;
    case VERI_INPUT :       return "input" ;
    case VERI_INTEGER :     return "integer" ;
    case VERI_JOIN :        return "join" ;
    case VERI_LARGE :       return "large" ;
    case VERI_MACROMODULE : return "macromodule" ;
    case VERI_MEDIUM :      return "medium" ;
    case VERI_MODULE :      return "module" ;
    case VERI_NAND :        return "nand" ;
    case VERI_NEGEDGE :     return "negedge" ;
    case VERI_NMOS :        return "nmos" ;
    case VERI_NOR :         return "nor" ;
    case VERI_NOT :         return "not" ;
    case VERI_NOTIF0 :      return "notif0" ;
    case VERI_NOTIF1 :      return "notif1" ;
    case VERI_OR :          return "or" ;
    case VERI_SEQ_OR :      return "or" ;
    case VERI_PROPERTY_OR : return "or" ;
    case VERI_OUTPUT :      return "output" ;
    case VERI_PARAMETER :   return "parameter" ;
    case VERI_PMOS :        return "pmos" ;
    case VERI_POSEDGE :     return "posedge" ;
    case VERI_PRIMITIVE :   return "primitive" ;
    case VERI_PULL0 :       return "pull0" ;
    case VERI_PULL1 :       return "pull1" ;
    case VERI_PULLDOWN :    return "pulldown" ;
    case VERI_PULLUP :      return "pullup" ;
    case VERI_RCMOS :       return "rcmos" ;
    case VERI_REAL :        return "real" ;
    case VERI_REALTIME :    return "realtime" ;
    case VERI_REG :         return "reg" ;
    case VERI_RELEASE :     return "release" ;
    case VERI_REPEAT :      return "repeat" ;
    case VERI_RNMOS :       return "rnmos" ;
    case VERI_RPMOS :       return "rpmos" ;
    case VERI_RTRAN :       return "rtran" ;
    case VERI_RTRANIF0 :    return "rtranif0" ;
    case VERI_RTRANIF1 :    return "rtranif1" ;
    case VERI_SCALARED :    return "scalared" ;
    case VERI_SMALL :       return "small" ;
    case VERI_SPECIFY :     return "specify" ;
    case VERI_SPECPARAM :   return "specparam" ;
    case VERI_STRONG0 :     return "strong0" ;
    case VERI_STRONG1 :     return "strong1" ;
    case VERI_SUPPLY0 :     return "supply0" ;
    case VERI_SUPPLY1 :     return "supply1" ;
    case VERI_TABLE :       return "table" ;
    case VERI_TASK :        return "task" ;
    case VERI_TIME :        return "time" ;
    case VERI_TRAN :        return "tran" ;
    case VERI_TRANIF0 :     return "tranif0" ;
    case VERI_TRANIF1 :     return "tranif1" ;
    case VERI_TRI :         return "tri" ;
    case VERI_TRI0 :        return "tri0" ;
    case VERI_TRI1 :        return "tri1" ;
    case VERI_TRIAND :      return "triand" ;
    case VERI_TRIOR :       return "trior" ;
    case VERI_TRIREG :      return "trireg" ;
    case VERI_VECTORED :    return "vectored" ;
    case VERI_WAIT :        return "wait" ;
    case VERI_WAND :        return "wand" ;
    case VERI_WEAK0 :       return "weak0" ;
    case VERI_WEAK1 :       return "weak1" ;
    case VERI_WHILE :       return "while" ;
    case VERI_WIRE :        return "wire" ;
    case VERI_WOR :         return "wor" ;
    case VERI_XNOR :        return "xnor" ;
    case VERI_XOR :         return "xor" ;

    case VERI_OCUR :        return "{" ;
    case VERI_CCUR :        return "}" ;
    case VERI_OBRACK :      return "[" ;
    case VERI_CBRACK :      return "]" ;
    case VERI_SEMI :        return " ;" ;
    case VERI_DOT :         return "." ;
    case VERI_COMMA :       return "," ;
    case VERI_AT :          return "@" ;
    case VERI_EQUAL :       return "=" ;
    case VERI_EQUAL_ASSIGN: return "=" ;
    case VERI_POUND :       return "#" ;

    case VERI_EDGE_AMPERSAND : return "&&&" ;

    // Verilog 2000 added tokens :
    case VERI_PARTSELECT_UP : return "+:" ;
    case VERI_PARTSELECT_DOWN : return "-:" ;
    case VERI_ARITLSHIFT :  return "<<<" ;
    case VERI_ARITRSHIFT :  return ">>>" ;
    case VERI_POWER :       return "**" ;
    case VERI_OATTR :       return "(*" ;
    case VERI_CATTR :       return "*)" ;

    // Verilog 2000 added keywords :
    case VERI_CONFIG :      return "config" ;
    case VERI_ENDCONFIG :   return "endconfig" ;
    case VERI_DESIGN :      return "design" ;
    case VERI_INSTANCE :    return "instance" ;
    case VERI_INCDIR :      return "incdir" ;
    case VERI_INCLUDE :     return "include" ;
    case VERI_CELL :        return "cell" ;
    case VERI_USE :         return "use" ;
    case VERI_LIBRARY :     return "library" ;
    case VERI_LIBLIST :     return "liblist" ;
    case VERI_GENERATE :    return "generate" ;
    case VERI_ENDGENERATE : return "endgenerate" ;
    case VERI_GENVAR :      return "genvar" ;
    case VERI_LOCALPARAM :  return "localparam" ;
    case VERI_SIGNED :      return "signed" ;
    case VERI_UNSIGNED :    return "unsigned" ;
    case VERI_AUTOMATIC :   return "automatic" ;
    case VERI_PULSESTYLE_ONEVENT :  return "pulsestyle_onevent" ;
    case VERI_PULSESTYLE_ONDETECT : return "pulsestyle_ondetect" ;
    case VERI_SHOWCANCELLED :       return "showcancelled" ;
    case VERI_NOSHOWCANCELLED :     return "noshowcancelled" ;

    // Keywords common between multiple dialects
    case VERI_STRINGTYPE :  return "string" ; // common between SV and AMS
    case VERI_CROSS :       return "cross" ; // common between SV and AMS
    case VERI_CAST_OP :         return "'" ;

    // System Verilog added tokens
    case VERI_DOTSTAR  :        return ".*" ;
    case VERI_STEPCONTROL_OP :  return "@@" ;
    case VERI_INC_OP :          return "++" ;
    case VERI_DEC_OP :          return "--" ;
    case VERI_PLUS_ASSIGN  :    return "+=" ;
    case VERI_MIN_ASSIGN  :     return "-=" ;
    case VERI_OVERLAPPED_IMPLICATION  :     return "|->" ;
    case VERI_NON_OVERLAPPED_IMPLICATION  : return "|=>" ;
    case VERI_OVERLAPPED_FOLLOWED_BY  :     return "#-#" ;
    case VERI_NON_OVERLAPPED_FOLLOWED_BY  : return "#=#" ;
    case VERI_CYCLE_DELAY_OP  : return "##" ;
    case VERI_DOLLAR  :         return "$" ;
    case VERI_CONSECUTIVE_REPEAT  :     return "[*" ;
    case VERI_NON_CONSECUTIVE_REPEAT  : return "[=" ;  // Modified token as per SV LRM Std 1800
    case VERI_GOTO_REPEAT  :    return "[->" ; // Modified token as per SV LRM Std 1800
    case VERI_MUL_ASSIGN :      return "*=" ;
    case VERI_DIV_ASSIGN :      return "/=" ;
    case VERI_MOD_ASSIGN :      return "%=" ;
    case VERI_AND_ASSIGN :      return "&=" ;
    case VERI_OR_ASSIGN :       return "|=" ;
    case VERI_XOR_ASSIGN :      return "^=" ;
    case VERI_LSHIFT_ASSIGN :   return "<<=" ;
    case VERI_RSHIFT_ASSIGN :   return ">>=" ;
    case VERI_ALSHIFT_ASSIGN :  return "<<<=" ;
    case VERI_ARSHIFT_ASSIGN :  return ">>>=" ;
    case VERI_WILDEQUALITY :    return "==?" ; // Printing the token as defined in SV Std 1800
    case VERI_WILDINEQUALITY :  return "!=?" ; // Printing the token as defined in SV Std 1800
    // Found ad-hoc in 3.1 LRM
    case VERI_COLON_COLON :     return "::" ;
    case VERI_COLON_EQUAL :     return":=" ;
    case VERI_COLON_NEQUAL :    return ":/" ;
    case VERI_RIGHTARRAW_ARROW: return "->>" ;

    // System Verilog 3.0 keywords
    case VERI_ALWAYS_COMB : return "always_comb" ;
    case VERI_ALWAYS_FF :   return "always_ff" ;
    case VERI_ALWAYS_LATCH :return "always_latch" ;
    case VERI_ASSERT :      return "assert" ;
    case VERI_ASSERT_STROBE:return "assert_strobe" ;
    case VERI_BIT :         return "bit" ;
    case VERI_BREAK :       return "break" ;
    case VERI_BYTE :        return "byte" ;
    case VERI_CHANGED :     return "changed" ;
    case VERI_CHAR :        return "char" ;
    case VERI_CONST :       return "const" ;
    case VERI_CONTINUE :    return "continue" ;
    case VERI_DO :          return "do" ;
    case VERI_ENDINTERFACE: return "endinterface" ;
    case VERI_ENUM :        return "enum" ;
    case VERI_EXPORT :      return "export" ;
    case VERI_EXTERN :      return "extern" ;
    case VERI_FORKJOIN :    return "forkjoin" ;
    case VERI_RETURN :      return "return" ;
    case VERI_IFF :         return "iff" ;
    case VERI_IMPORT :      return "import" ;
    case VERI_INT :         return "int" ;
    case VERI_INTERFACE :   return "interface" ;
    case VERI_LOGIC :       return "logic" ;
    case VERI_LONGINT :     return "longint" ;
    case VERI_LONGREAL :    return "longreal" ;
    case VERI_MODPORT :     return "modport" ;
    case VERI_PACKED :      return "packed" ;
    case VERI_PRIORITY :    return "priority" ;
    case VERI_SHORTINT :    return "shortint" ;
    case VERI_SHORTREAL :   return "shortreal" ;
    case VERI_STATIC :      return "static" ;
    case VERI_STRUCT :      return "struct" ;
    case VERI_TIMEPRECISION:return "timeprecision" ;
    case VERI_TIMEUNIT :    return "timeunit" ;
    case VERI_TYPE :        return "type" ;
    case VERI_TYPEDEF :     return "typedef" ;
    case VERI_NETTYPE :     return "nettype" ;
    case VERI_INTERCONNECT : return "interconnect" ;
    case VERI_UNION :       return "union" ;
    case VERI_UNIQUE :      return "unique" ;
    case VERI_VOID :        return "void" ;

    // System Verilog 3.1 keywords
    case VERI_ALIAS :       return "alias" ;
    case VERI_BEFORE :      return "before" ;
    case VERI_BIND :        return "bind" ;
    case VERI_CHANDLE :     return "chandle" ;
    case VERI_CLASS :       return "class" ;
    case VERI_CLOCKING :    return "clocking" ;
    case VERI_CONSTRAINT :  return "constraint" ;
    case VERI_CONTEXT :     return "context" ;
    case VERI_COVER :       return "cover" ;
    case VERI_DIST :        return "dist" ;
    case VERI_ENDCLASS :    return "endclass" ;
    case VERI_ENDCLOCKING : return "endclocking" ;
    case VERI_ENDPROGRAM :  return "endprogram" ;
    case VERI_ENDPROPERTY : return "endproperty" ;
    case VERI_ENDSEQUENCE : return "endsequence" ;
    case VERI_EXTENDS :     return "extends" ;
    case VERI_FINAL :       return "final" ;
    case VERI_FIRST_MATCH : return "first_match" ;
    case VERI_INSIDE :      return "inside" ;
    case VERI_INTERSECT :   return "intersect" ;
    case VERI_JOIN_ANY :    return "join_any" ;
    case VERI_JOIN_NONE :   return "join_none" ;
    case VERI_LOCAL :       return "local" ;
    case VERI_NEW :         return "new" ;
    case VERI_NULL :        return "null" ;
    case VERI_PROGRAM :     return "program" ;
    case VERI_PROPERTY :    return "property" ;
    case VERI_PROTECTED :   return "protected" ;
    case VERI_PUBLIC :      return "public" ;
    case VERI_PURE :        return "pure" ;
    case VERI_RAND :        return "rand" ;
    case VERI_RANDC :       return "randc" ;
    case VERI_REF :         return "ref" ;
    case VERI_SEQUENCE :    return "sequence" ;
    case VERI_SOLVE :       return "solve" ;
    // case VERI_STRINGTYPE :  return "string" ; // common with AMS
    case VERI_SUPER :       return "super" ;
    case VERI_THIS :        return "this" ;
    case VERI_THROUGHOUT :  return "throughout" ;
    case VERI_VAR :         return "var" ;
    case VERI_VIRTUAL :     return "virtual" ;
    case VERI_WAIT_ORDER :  return "wait_order" ;
    case VERI_WITH :        return "with" ;
    case VERI_WITHIN :      return "within" ;

    // System Verilog 3.1a keywords
    case VERI_ASSUME :      return "assume" ;
    case VERI_BINS :        return "bins" ;
    case VERI_BINSOF :      return "binsof" ;
    case VERI_COVERGROUP :  return "covergroup" ;
    case VERI_COVERPOINT :  return "coverpoint" ;
    /// case VERI_CROSS :       return "cross" ;  // common with AMS
    case VERI_ENDGROUP :    return "endgroup" ;
    case VERI_ENDPACKAGE :  return "endpackage" ;
    case VERI_EXPECT :      return "expect" ;
    case VERI_FOREACH :     return "foreach" ;
    case VERI_IGNOREBINS :  return "ignore_bins" ;
    case VERI_ILLEGAL_BINS: return "illegal_bins " ;
    case VERI_MATCHES :     return "matches" ;
    case VERI_PACKAGE :     return "package" ;
    case VERI_RANDCASE :    return "randcase" ;
    case VERI_RANDSEQUENCE: return "randsequence " ;
    case VERI_TAGGED :      return "tagged" ;
    case VERI_WILDCARD :    return "wildcard" ;
    case VERI_UWIRE :       return "uwire" ; // Verilog 2005 addition (VIPER 3829)
    case VERI_DOLLAR_ROOT : return "$root" ; // not real keyword, but it is a token
    case VERI_DOLLAR_UNIT : return "$unit" ; // not real keyword, but it is a token
    // P1800-2009 tokens :
    case VERI_LET :            return "let" ; // keyword let
    case VERI_GLOBAL :         return "global" ; // keyword global
    case VERI_CHECKER :        return "checker" ; // keyword checker
    case VERI_ENDCHECKER :     return "endchecker" ; // keyword endchecker
    case VERI_UNTYPED :        return "untyped" ;
    case VERI_UNIQUE0 :        return "unique0" ;
    // Property operators :
    case VERI_ACCEPT_ON :      return "accept_on" ;
    case VERI_REJECT_ON :      return "reject_on" ;
    case VERI_SYNC_ACCEPT_ON : return "sync_accept_on" ;
    case VERI_SYNC_REJECT_ON : return "sync_reject_on" ;
    case VERI_NEXTTIME :       return "nexttime" ;
    case VERI_S_NEXTTIME :     return "s_nexttime" ;
    case VERI_IMPLIES :        return "implies" ;
    case VERI_UNTIL :          return "until" ;
    case VERI_S_UNTIL :        return "s_until" ;
    case VERI_UNTIL_WITH :     return "until_with" ;
    case VERI_S_UNTIL_WITH :   return "s_until_with" ;
    case VERI_EVENTUALLY :     return "eventually" ;
    case VERI_RESTRICT :       return "restrict" ; // VIPER #6373: New 2009 assertion
    case VERI_S_EVENTUALLY :   return "s_eventually" ;
    case VERI_S_ALWAYS :       return "s_always" ;
    case VERI_STRONG :         return "strong" ;
    case VERI_WEAK :           return "weak" ;
    case VERI_SOFT :           return "soft" ; // VIPER #8056
    case VERI_IMPLEMENTS :     return "implements" ; // IEEE-2012 keyword

    // Added for default nettype :
    case VERI_NONE : return "none" ;

    // Tokens added for System Calls
    // This should probably be changed to an enumeration type.

    // LRM Verilog 2000. Section 17.  System Tasks and Functions
    //
    // Display Tasks
    case VERI_SYS_CALL_DISPLAY            : return "display" ;
    case VERI_SYS_CALL_DISPLAYB           : return "displayb" ;
    case VERI_SYS_CALL_DISPLAYH           : return "displayh" ;
    case VERI_SYS_CALL_DISPLAYO           : return "displayo" ;
    case VERI_SYS_CALL_MONITOR            : return "monitor" ;
    case VERI_SYS_CALL_MONITORB           : return "monitorb" ;
    case VERI_SYS_CALL_MONITORH           : return "monitorh" ;
    case VERI_SYS_CALL_MONITORO           : return "monitoro" ;
    case VERI_SYS_CALL_MONITOROFF         : return "monitoroff" ;
    case VERI_SYS_CALL_STROBE             : return "strobe" ;
    case VERI_SYS_CALL_STROBEB            : return "strobeb" ;
    case VERI_SYS_CALL_STROBEH            : return "strobeh" ;
    case VERI_SYS_CALL_STROBEO            : return "strobeo" ;
    case VERI_SYS_CALL_WRITE              : return "write" ;
    case VERI_SYS_CALL_WRITEB             : return "writeb" ;
    case VERI_SYS_CALL_WRITEH             : return "writeh" ;
    case VERI_SYS_CALL_WRITEO             : return "writeo" ;
    case VERI_SYS_CALL_MONITORON          : return "monitoron" ;
    // VCD system tasks
    case VERI_SYS_CALL_DUMPVARS           : return "dumpvars" ; // Added later, along with SV Std 1800 additions
    case VERI_SYS_CALL_DUMPALL            : return "dumpall" ;
    case VERI_SYS_CALL_DUMPON             : return "dumpon" ;
    case VERI_SYS_CALL_DUMPOFF            : return "dumpoff" ;
    case VERI_SYS_CALL_DUMPFILE           : return "dumpfile" ;
    case VERI_SYS_CALL_DUMPLIMIT          : return "dumplimit" ;
    case VERI_SYS_CALL_DUMPFLUSH          : return "dumpflush" ;
    case VERI_SYS_CALL_DUMPPORTS          : return "dumpports" ;
    case VERI_SYS_CALL_DUMPPORTSON        : return "dumpportson" ;
    case VERI_SYS_CALL_DUMPPORTSOFF       : return "dumpportsoff" ;
    case VERI_SYS_CALL_DUMPPORTSALL       : return "dumpportsall" ;
    case VERI_SYS_CALL_DUMPPORTSLIMIT     : return "dumpportslimit" ;
    case VERI_SYS_CALL_DUMPPORTSFLUSH     : return "dumpportsflush" ;
    // File I/O
    case VERI_SYS_CALL_FCLOSE             : return "fclose" ;
    case VERI_SYS_CALL_FDISPLAY           : return "fdisplay" ;
    case VERI_SYS_CALL_FDISPLAYB          : return "fdisplayb" ;
    case VERI_SYS_CALL_FDISPLAYH          : return "fdisplayh" ;
    case VERI_SYS_CALL_FDISPLAYO          : return "fdisplayo" ;
    case VERI_SYS_CALL_FGETC              : return "fgetc" ;
    case VERI_SYS_CALL_FGETS              : return "fgets" ;
    case VERI_SYS_CALL_FMONITOR           : return "fmonitor" ;
    case VERI_SYS_CALL_FMONITORB          : return "fmonitorb" ;
    case VERI_SYS_CALL_FMONITORH          : return "fmonitorh" ;
    case VERI_SYS_CALL_FMONITORO          : return "fmonitoro" ;
    case VERI_SYS_CALL_READMEMB           : return "readmemb" ;
    case VERI_SYS_CALL_SWRITE             : return "swrite" ;
    case VERI_SYS_CALL_SWRITEO            : return "swriteo" ;
    case VERI_SYS_CALL_SFORMAT            : return "sformat" ;
    case VERI_SYS_CALL_FSCANF             : return "fscanf" ;
    case VERI_SYS_CALL_FREAD              : return "fread" ;
    case VERI_SYS_CALL_FSEEK              : return "fseek" ;
    case VERI_SYS_CALL_FOPEN              : return "fopen" ;
    case VERI_SYS_CALL_FSTROBE            : return "fstrobe" ;
    case VERI_SYS_CALL_FSTROBEB           : return "fstrobeb" ;
    case VERI_SYS_CALL_FSTROBEH           : return "fstrobeh" ;
    case VERI_SYS_CALL_FSTROBEO           : return "fstrobeo" ;
    case VERI_SYS_CALL_UNGETC             : return "ungetc" ;
    case VERI_SYS_CALL_FERROR             : return "ferror" ;
    case VERI_SYS_CALL_REWIND             : return "rewind" ;
    case VERI_SYS_CALL_FWRITE             : return "fwrite" ;
    case VERI_SYS_CALL_FWRITEB            : return "fwriteb" ;
    case VERI_SYS_CALL_FWRITEH            : return "fwriteh" ;
    case VERI_SYS_CALL_FWRITEO            : return "fwriteo" ;
    case VERI_SYS_CALL_READMEMH           : return "readmemh" ;
    case VERI_SYS_CALL_SWRITEB            : return "swriteb" ;
    case VERI_SYS_CALL_SWRITEH            : return "swriteh" ;
    case VERI_SYS_CALL_SDF_ANNOTATE       : return "sdf_annotate" ;
    case VERI_SYS_CALL_SSCANF             : return "sscanf" ;
    case VERI_SYS_CALL_FTELL              : return "ftell" ;
    // Timescale tasks
    case VERI_SYS_CALL_PRINTTIMESCALE     : return "printtimescale" ;
    case VERI_SYS_CALL_TIMEFORMAT         : return "timeformat" ;
    // Simulation control tasks
    case VERI_SYS_CALL_FINISH             : return "finish" ;
    case VERI_SYS_CALL_STOP               : return "stop" ;
    // PLA modeling tasks
    case VERI_SYS_CALL_ASYNC__AND__ARRAY  : return "async$and$array" ;
    case VERI_SYS_CALL_ASYNC__NAND__ARRAY : return "async$nand$array" ;
    case VERI_SYS_CALL_ASYNC__OR__ARRAY   : return "async$or$array" ;
    case VERI_SYS_CALL_ASYNC__NOR__ARRAY  : return "async$nor$array" ;
    case VERI_SYS_CALL_SYNC__AND__ARRAY   : return "sync$and$array" ;
    case VERI_SYS_CALL_SYNC__NAND__ARRAY  : return "sync$nand$array" ;
    case VERI_SYS_CALL_SYNC__OR__ARRAY    : return "sync$or$array" ;
    case VERI_SYS_CALL_SYNC__NOR__ARRAY   : return "sync$nor$array" ;
    case VERI_SYS_CALL_ASYNC__AND__PLANE  : return "async$and$plane" ;
    case VERI_SYS_CALL_ASYNC__NAND__PLANE : return "async$nand$plane" ;
    case VERI_SYS_CALL_ASYNC__OR__PLANE   : return "async$or$plane" ;
    case VERI_SYS_CALL_ASYNC__NOR__PLANE  : return "async$nor$plane" ;
    case VERI_SYS_CALL_SYNC__AND__PLANE   : return "sync$and$plane" ;
    case VERI_SYS_CALL_SYNC__NAND__PLANE  : return "sync$nand$plane" ;
    case VERI_SYS_CALL_SYNC__OR__PLANE    : return "sync$or$plane" ;
    case VERI_SYS_CALL_SYNC__NOR__PLANE   : return "sync$nor$plane" ;
    // Stochastic Analysis tasks
    case VERI_SYS_CALL_Q_INITIALIZE       : return "q_initialize" ;
    case VERI_SYS_CALL_Q_REMOVE           : return "q_remove" ;
    case VERI_SYS_CALL_Q_EXAM             : return "q_exam" ;
    case VERI_SYS_CALL_Q_ADD              : return "q_add" ;
    case VERI_SYS_CALL_Q_FULL             : return "q_full" ;
    // Simulation time functions
    case VERI_SYS_CALL_REALTIME           : return "realtime" ;
    case VERI_SYS_CALL_TIME               : return "time" ;
    case VERI_SYS_CALL_STIME              : return "stime" ;
    // Conversion functions
    case VERI_SYS_CALL_BITSTOREAL         : return "bitstoreal" ;
    case VERI_SYS_CALL_ITOR               : return "itor" ;
    case VERI_SYS_CALL_SIGNED             : return "signed" ;
    case VERI_SYS_CALL_REALTOBITS         : return "realtobits" ;
    case VERI_SYS_CALL_RTOI               : return "rtoi" ;
    case VERI_SYS_CALL_UNSIGNED           : return "unsigned" ;
    // Probabilistic distribution functions
    case VERI_SYS_CALL_DIST_CHI_SQUARE    : return "dist_chi_square" ;
    case VERI_SYS_CALL_DIST_EXPONENTIAL   : return "dist_exponential" ;
    case VERI_SYS_CALL_DIST_POISSON       : return "dist_poisson" ;
    case VERI_SYS_CALL_DIST_UNIFORM       : return "dist_uniform" ;
    case VERI_SYS_CALL_DIST_ERLANG        : return "dist_erlang" ;
    case VERI_SYS_CALL_DIST_NORMAL        : return "dist_normal" ;
    case VERI_SYS_CALL_DIST_T             : return "dist_t" ;
    case VERI_SYS_CALL_RANDOM             : return "random" ;
    // Command line input
    case VERI_SYS_CALL_TEST__PLUSARGS     : return "test$plusargs" ;
    case VERI_SYS_CALL_VALUE__PLUSARGS    : return "value$plusargs" ;
    // LRM Verilog 2000. Section 15 Timing Checks
    case VERI_SYS_CALL_SETUP              : return "setup" ;
    case VERI_SYS_CALL_HOLD               : return "hold" ;
    case VERI_SYS_CALL_SETUPHOLD          : return "setuphold" ;
    case VERI_SYS_CALL_RECOVERY           : return "recovery" ;
    case VERI_SYS_CALL_REMOVAL            : return "removal" ;
    case VERI_SYS_CALL_RECREM             : return "recrem" ;
    case VERI_SYS_CALL_SKEW               : return "skew" ;
    case VERI_SYS_CALL_TIMESKEW           : return "timeskew" ;
    case VERI_SYS_CALL_FULLSKEW           : return "fullskew" ;
    case VERI_SYS_CALL_PERIOD             : return "period" ;
    case VERI_SYS_CALL_WIDTH              : return "width" ;
    case VERI_SYS_CALL_NOCHANGE           : return "nochange" ;

    //
    // System Verilog (3.1) system calls
    //
    case VERI_SYS_CALL_ASSERTKILL      : return "assertkill" ;
    case VERI_SYS_CALL_ASSERTOFF       : return "assertoff" ;
    case VERI_SYS_CALL_ASSERTON        : return "asserton" ;
    case VERI_SYS_CALL_BITS            : return "bits" ;
    case VERI_SYS_CALL_BITSTOSHORTREAL : return "bitstoshortreal" ;
    case VERI_SYS_CALL_CAST            : return "cast" ;
    case VERI_SYS_CALL_COUNTONES       : return "countones" ;
    case VERI_SYS_CALL_DIMENSIONS      : return "dimensions" ;
    case VERI_SYS_CALL_ERROR           : return "error" ;
    case VERI_SYS_CALL_EXIT            : return "exit" ;
    case VERI_SYS_CALL_FATAL           : return "fatal" ;
    case VERI_SYS_CALL_FELL            : return "fell" ;
    case VERI_SYS_CALL_HIGH            : return "high" ;
    case VERI_SYS_CALL_INCREMENT       : return "increment" ;
    case VERI_SYS_CALL_INFO            : return "info" ;
    case VERI_SYS_CALL_INSET           : return "inset" ;  // 3.1 only ? Does not show up in 3.1a.
    case VERI_SYS_CALL_INSETZ          : return "insetz" ; // 3.1 only ? Does not show up in 3.1a.
    case VERI_SYS_CALL_ISUNKNOWN       : return "isunknown" ;
    case VERI_SYS_CALL_LEFT            : return "left" ;
    case VERI_SYS_CALL_LENGTH          : return "length" ; // 3.1 only ? Does not show up in 3.1a.
    case VERI_SYS_CALL_LOW             : return "low" ;
    case VERI_SYS_CALL_ONEHOT          : return "onehot" ;
    case VERI_SYS_CALL_PAST            : return "past" ;
    case VERI_SYS_CALL_RIGHT           : return "right" ;
    case VERI_SYS_CALL_ROOT            : return "root" ;
    case VERI_SYS_CALL_ROSE            : return "rose" ;
    case VERI_SYS_CALL_SRANDOM         : return "srandom" ;
    case VERI_SYS_CALL_STABLE          : return "stable" ;
    case VERI_SYS_CALL_URANDOM         : return "urandom" ;
    case VERI_SYS_CALL_URANDOM_RANGE   : return "urandom_range" ;
    case VERI_SYS_CALL_WARNING         : return "warning" ;

    // System Verilog 3.1a system call additions :
    case VERI_SYS_CALL_ONEHOT0              : return "onehot0" ;
    case VERI_SYS_CALL_COVERAGE_CONTROL     : return "coverage_control" ;
    case VERI_SYS_CALL_COVERAGE_MERGE       : return "coverage_merge" ;
    case VERI_SYS_CALL_COVERAGE_SAVE        : return "coverage_save" ;
    case VERI_SYS_CALL_GET_COVERAGE         : return "get_coverage" ;
    case VERI_SYS_CALL_ISUNBOUNDED          : return "isunbounded" ;
    case VERI_SYS_CALL_LOAD_COVERAGE_DB     : return "load_coverage_db" ;
    case VERI_SYS_CALL_SAMPLED              : return "sampled" ;
    case VERI_SYS_CALL_SET_COVERAGE_DB_NAME : return "set_coverage_db_name" ;
    case VERI_SYS_CALL_SIZE                 : return "size" ;
    case VERI_SYS_CALL_TYPENAME             : return "typename" ;
    case VERI_SYS_CALL_TYPEOF               : return "typeof" ;
    case VERI_SYS_CALL_UNIT                 : return "unit" ;
    case VERI_SYS_CALL_WRITEMEMB            : return "writememb" ;
    case VERI_SYS_CALL_WRITEMEMH            : return "writememh" ;

    // System Verilog (3.1) methods
    case VERI_METHOD_ATOBIN          : return "atobin" ;
    case VERI_METHOD_ATOHEX          : return "atohex" ;
    case VERI_METHOD_ATOI            : return "atoi" ;
    case VERI_METHOD_ATOOCT          : return "atooct" ;
    case VERI_METHOD_ATOREAL         : return "atoreal" ;
    case VERI_METHOD_BACK            : return "back" ;
    case VERI_METHOD_BINTOA          : return "bintoa" ;
    case VERI_METHOD_CLEAR           : return "clear" ;
    case VERI_METHOD_COMPARE         : return "compare" ;
    case VERI_METHOD_CONSTRAINT_MODE : return "constraint_mode" ;
    case VERI_METHOD_DATA            : return "data" ;
    case VERI_METHOD_DELETE          : return "delete" ;
    case VERI_METHOD_EMPTY           : return "empty" ;
    case VERI_METHOD_EQ              : return "eq" ;
    case VERI_METHOD_ERASE           : return "erase" ;
    case VERI_METHOD_ERASE_RANGE     : return "erase_range" ;
    case VERI_METHOD_EXISTS          : return "exists" ;
    case VERI_METHOD_FIRST           : return "first" ;
    case VERI_METHOD_FRONT           : return "front" ;
    case VERI_METHOD_GETC            : return "getc" ;
    case VERI_METHOD_HEXTOA          : return "hextoa" ;
    case VERI_METHOD_ICOMPARE        : return "icompare" ;
    case VERI_METHOD_INSERT          : return "insert" ;
    case VERI_METHOD_INSERT_RANGE    : return "insert_range" ;
    case VERI_METHOD_ITOA            : return "itoa" ;
    case VERI_METHOD_LAST            : return "last" ;
    case VERI_METHOD_LEN             : return "len" ;
    case VERI_METHOD_NAME            : return "name" ;
    case VERI_METHOD_NEQ             : return "neq" ;
    case VERI_METHOD_NEXT            : return "next" ;
    case VERI_METHOD_NUM             : return "num" ;
    case VERI_METHOD_OCTTOA          : return "octtoa" ;
    case VERI_METHOD_POP_BACK        : return "pop_back" ;
    case VERI_METHOD_POP_FRONT       : return "pop_front" ;
    case VERI_METHOD_POST_RANDOMIZE  : return "post_randomize" ;
    case VERI_METHOD_PRE_RANDOMIZE   : return "pre_randomize" ;
    case VERI_METHOD_PREV            : return "prev" ;
    case VERI_METHOD_PURGE           : return "purge" ;
    case VERI_METHOD_PUSH_BACK       : return "push_back" ;
    case VERI_METHOD_PUSH_FRONT      : return "push_front" ;
    case VERI_METHOD_PUTC            : return "putc" ;
    case VERI_METHOD_RAND_MODE       : return "rand_mode" ;
    case VERI_METHOD_RANDOMIZE       : return "randomize" ;
    case VERI_METHOD_REALTOA         : return "realtoa" ;
    case VERI_METHOD_SET             : return "set" ;
    case VERI_METHOD_SIZE            : return "size" ;
    case VERI_METHOD_START           : return "start" ;
    case VERI_METHOD_SUBSTR          : return "substr" ;
    case VERI_METHOD_SWAP            : return "swap" ;
    case VERI_METHOD_TOLOWER         : return "tolower" ;
    case VERI_METHOD_TOUPPER         : return "toupper" ;

    // System Verilog 3.1a method additions :
    case VERI_METHOD_AND              : return "and" ;
    case VERI_METHOD_AWAIT            : return "await" ;
    case VERI_METHOD_FIND_FIRST       : return "find_first" ;
    case VERI_METHOD_FIND_FIRST_INDEX : return "find_first_index" ;
    case VERI_METHOD_FIND_INDEX       : return "find_index" ;
    case VERI_METHOD_FIND_LAST        : return "find_last" ;
    case VERI_METHOD_FIND_LAST_INDEX  : return "find_last_index" ;
    case VERI_METHOD_FINISH           : return "finish" ;
    case VERI_METHOD_FIND             : return "find" ;
    case VERI_METHOD_GET_RANDSTATE    : return "get_randstate" ;
    case VERI_METHOD_KILL             : return "kill" ;
    case VERI_METHOD_MAX              : return "max" ;
    case VERI_METHOD_MIN              : return "min" ;
    case VERI_METHOD_NEW              : return "new" ;
    case VERI_METHOD_OR               : return "or" ;
    case VERI_METHOD_PROCESS          : return "process" ;
    case VERI_METHOD_PRODUCT          : return "product" ;
    case VERI_METHOD_RESUME           : return "resume" ;
    case VERI_METHOD_REVERSE          : return "reverse" ;
    case VERI_METHOD_RSORT            : return "rsort" ;
    case VERI_METHOD_SELF             : return "self" ;
    case VERI_METHOD_SET_RANDSTATE    : return "set_randstate" ;
    case VERI_METHOD_SHUFFLE          : return "shuffle" ;
    case VERI_METHOD_SIE              : return "sie" ;
    case VERI_METHOD_SORT             : return "sort" ;
    case VERI_METHOD_STATUS           : return "status" ;
    case VERI_METHOD_SUM              : return "sum" ;
    case VERI_METHOD_SUSPEND          : return "suspend" ;
    case VERI_METHOD_UNIQUE           : return "unique" ;
    case VERI_METHOD_UNIQUE_INDEX     : return "unique_index" ;
    case VERI_METHOD_XOR              : return "xor" ;
    case VERI_METHOD_ENDED            : return "ended" ;
    case VERI_METHOD_MATCHED          : return "matched" ;

    // System Verilog Std 1800 additions :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS  : return "unpacked_dimensions" ;
    case VERI_SYS_CALL_COVERAGE_GET_MAX     : return "coverage_get_max" ;
    case VERI_SYS_CALL_COVERAGE_GET         : return "coverage_get" ;
    case VERI_METHOD_INDEX                  : return "index" ; // VIPER 2546 : Consider 'index' as method
    case VERI_METHOD_GET_COVERAGE           : return "get_coverage" ;
    case VERI_METHOD_GET_INST_COVERAGE      : return "get_inst_coverage" ;
    case VERI_METHOD_SET_INST_NAME          : return "set_inst_name" ;
    case VERI_METHOD_STOP                   : return "stop" ;
    case VERI_METHOD_SAMPLE                 : return "sample" ;
    case VERI_METHOD_TRIGGERED              : return "triggered" ;
    case VERI_SYS_CALL_FFLUSH               : return "fflush" ;
    case VERI_SYS_CALL_FEOF                 : return "feof" ;
    // Moved these function to VERILOG_2000 mode
    // LRM section 17.11.1/17.11.2 Viper 6807
    // Math functions (20.8)
    case VERI_SYS_CALL_CLOG2                : return "clog2" ;
    case VERI_SYS_CALL_LN                   : return "ln" ;
    case VERI_SYS_CALL_LOG10                : return "log10" ;
    case VERI_SYS_CALL_EXP                  : return "exp" ;
    case VERI_SYS_CALL_SQRT                 : return "sqrt" ;
    case VERI_SYS_CALL_POW                  : return "pow" ;
    case VERI_SYS_CALL_FLOOR                : return "floor" ;
    case VERI_SYS_CALL_CEIL                 : return "ceil" ;
    case VERI_SYS_CALL_SIN                  : return "sin" ;
    case VERI_SYS_CALL_COS                  : return "cos" ;
    case VERI_SYS_CALL_TAN                  : return "tan" ;
    case VERI_SYS_CALL_ASIN                 : return "asin" ;
    case VERI_SYS_CALL_ACOS                 : return "acos" ;
    case VERI_SYS_CALL_ATAN                 : return "atan" ;
    case VERI_SYS_CALL_ATAN2                : return "atan2" ;
    case VERI_SYS_CALL_HYPOT                : return "hypot" ;
    case VERI_SYS_CALL_SINH                 : return "sinh" ;
    case VERI_SYS_CALL_COSH                 : return "cosh" ;
    case VERI_SYS_CALL_TANH                 : return "tanh" ;
    case VERI_SYS_CALL_ASINH                : return "asinh" ;
    case VERI_SYS_CALL_ACOSH                : return "acosh" ;
    case VERI_SYS_CALL_ATANH                : return "atanh" ;
    // Assertion action control tasks (20.12)
    case VERI_SYS_CALL_ASSERTPASSON         : return "assertpasson" ;
    case VERI_SYS_CALL_ASSERTPASSOFF        : return "assertpassoff" ;
    case VERI_SYS_CALL_ASSERTFAILON         : return "assertfailon" ;
    case VERI_SYS_CALL_ASSERTFAILOFF        : return "assertfailoff" ;
    case VERI_SYS_CALL_ASSERTNONVACUOUSON   : return "assertnonvacuouson" ;
    case VERI_SYS_CALL_ASSERTVACUOUSOFF     : return "assertvacuousoff" ;
    // Assertion functions (20.13)
    case VERI_SYS_CALL_PAST_GCLK            : return "past_gclk" ;
    case VERI_SYS_CALL_FELL_GCLK            : return "fell_gclk" ;
    case VERI_SYS_CALL_CHANGED_GCLK         : return "changed_gclk" ;
    case VERI_SYS_CALL_RISING_GCLK          : return "rising_gclk" ;
    case VERI_SYS_CALL_STEADY_GCLK          : return "steady_gclk" ;
    case VERI_SYS_CALL_CHANGED              : return "changed" ;
    case VERI_SYS_CALL_ROSE_GCLK            : return "rose_gclk" ;
    case VERI_SYS_CALL_STABLE_GCLK          : return "stable_gclk" ;
    case VERI_SYS_CALL_FUTURE_GCLK          : return "future_gclk" ;
    case VERI_SYS_CALL_FALLING_GCLK         : return "falling_gclk" ;
    case VERI_SYS_CALL_CHANGING_GCLK        : return "changing_gclk" ;
    // System Verilog Std 1800(2009) additions :
    case VERI_SYS_CALL_GLOBAL_CLOCK         : return "global_clock" ;
    case VERI_SYS_CALL_PAST_GLOBAL_CLOCK    : return "past_glk" ;
    case VERI_SYS_CALL_ROSE_GLOBAL_CLOCK    : return "rose_glk" ;
    case VERI_SYS_CALL_FELL_GLOBAL_CLOCK    : return "fell_glk" ;
    case VERI_SYS_CALL_STABLE_GLOBAL_CLOCK  : return "stable_glk" ;
    case VERI_SYS_CALL_CHANGED_GLOBAL_CLOCK : return "changed_glk" ;
    case VERI_SYS_CALL_FUTURE_GLOBAL_CLOCK  : return "future_glk" ;
    case VERI_SYS_CALL_RISING_GLOBAL_CLOCK  : return "rising_glk" ;
    case VERI_SYS_CALL_FALLING_GLOBAL_CLOCK : return "falling_glk" ;
    case VERI_SYS_CALL_STEADY_GLOBAL_CLOCK  : return "steady_glk" ;
    case VERI_SYS_CALL_CHANGING_GLOBAL_CLOCK: return "changing_glk" ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895:
    case VERI_UNCONSTRAINT                  : return "/*unconstraint*/" ;
#endif
    // VIPER #8433: Add the following missing tokens: begin
    case VERI_CONSECUTIVE_REPEAT_MUL        : return "[*]" ;
    case VERI_CONSECUTIVE_REPEAT_PLUS       : return "[+]" ;
    case VERI_SYS_CALL_SHORTREALTOBITS      : return "shortrealtobits" ;
    case VERI_WITH_DEFAULT                  : return "with default" ;
    // VIPER #8433: Add the following missing tokens: end
    default : return "" ;
    }
}

const char *
VeriTreeNode::PrintEnvironment(veri_environment environment)
{
    switch(environment) {
    case VERI_EXPRESSION                  :
    case VERI_EXPRESSION_UNDECL_USE_GOOD  : return "expression" ;            // normal RHS expression
    case VERI_ABORT_EXPRESSION            : return "abort_expression" ;      // abort conditional expression
    case VERI_LET_EXPRESSION              : return "let_expression" ;        // normal RHS expression
    case VERI_STRING_TARGET               : return "expression" ;            // VIPER # 4259
    case VERI_LVALUE                      : return "lvalue" ;                // LHS assignment to a reg or net
    case VERI_PORT_EXPRESSION             : return "port_expression" ;       // module port expression list
    case VERI_PORT_CONNECTION             : return "port_connection" ;       // port connection in instantiation
    case VERI_GATE_INOUT_PORT_CONNECTION  : return "gate_inout_port_connection" ;  // port connection in trans gate instantiation
    case VERI_CONSTANT_EXPRESSION         : return "constant_expression" ;   // any constant expression
    case VERI_PARAM_INITIAL_EXP           : return "param_initial_expression" ; //initial value of parameter
    case VERI_INTEGRAL_CONSTANT           : return "integral_constant" ;     // only integral constant
    case VERI_DELAY_CONTROL               : return "delay_control" ;         // delay (control) expression
    case VERI_EVENT_CONTROL               :
    case VERI_EVENT_CONTROL_UNDECL_USE_GOOD: return "event_control" ;         // @(..) event (control) expression
    case VERI_SEQ_PROP_INIT               : return "sequence_property_default_actual_argument" ;
    case VERI_NET_LVALUE                  :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD  : return "net_lvalue" ;            // LHS assignment to a net
    case VERI_REG_LVALUE                  : return "reg_lvalue" ;            // LHS assignment to a reg
    case VERI_CONSTRUCTOR_LVALUE          : return "constructor_lvalue" ;
    case VERI_FORCE_LVALUE                : return "force_lvalue" ;          // LHS assignment to a reg/net in a force statement
    case VERI_GENVAR_LVALUE               : return "loop_idx_assign_lvalue" ;// LHS assignment to a genvar in a generate for-loop
    case VERI_UPWARD_SCOPE_NAME           : return "upward_scope_name" ;     // function, task, and named block upward name referencing
    case VERI_SYSTEM_TASK_ARG             : return "system_task_arg" ;       // system task argument handling
    case VERI_PARAM_VALUE                 : return "param_expression" ;      // value of parameters
    case VERI_BINS_INIT_VALUE             : return "bins_init_value" ;       // value of parameters
    case VERI_TYPE_ENV                    : return "data_type" ;             // data type
    case VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR : return "terminal_descriptor" ; // timing check terminal descriptor
    case VERI_TIMING_CHECK_REGISTER_IDENTIFIER : return "register_identifier" ; // timing check register identifier
    case VERI_FORK_JOIN_NONE_IN_FUNC      : return "fork join_none" ;           // fork join_none inside function
    case VERI_FORK_JOIN                   : return "fork join" ;                // fork join block
    case VERI_ALWAYS_BLOCK                : return "always" ;                // always block
    case VERI_INITIAL_BLOCK               : return "initial" ;               // initial block
    case VERI_RAND_VAR                    : return "random_variables" ;      // rand variables
    case VERI_PROPERTY_ENV                : return "property" ;   // property expression for SVA
    case VERI_SEQUENCE_ENV                : return "sequence" ;   // sequence expression for SVA
    case VERI_CONSTRAINT_EXPR             : return "constraint_expression" ; // constraint expression handling
    case VERI_INLINE_CONSTRAINT_EXPR      : return "constraint_expression" ; // constraint expression handling
    case VERI_CLASS_NAME                  : return "class_name" ;            // class name
    case VERI_BIN_VAL_ENV                 : return "bin_value" ;             // bin value
    case VERI_PRODUCTION_ENV              : return "production" ;            // productions in randsequence
    case VERI_SPECIAL_ALWAYS              : return "always" ;                // always_comb and always_latch
    case VERI_ALWAYS_FF_BLOCK             : return "always_ff" ;             // always_ff
    case VERI_ACTION_BLOCK                : return "action_block"  ;         // action block
    case VERI_VALUE_RANGE                 : return "value_range" ;           // value range of distribution
    case VERI_FINAL_BLOCK                 : return "final_block" ;           // final construct
    case VERI_DEFERRED_ASSERT             : return "deferred_assertion" ;    // deferred immediate assertion (SV-2009)
    case VERI_PROPERTY_ARG                : return "property_argument" ;     // actual of property instance
    case VERI_PSL_PROPERTY_ENV            : return "psl_property" ;          // Property environment?
    case VERI_PSL_SEQUENCE_ENV            : return "psl_sequence" ;          // Sequence environment?
    case VERI_BRANCH_TERM                 : return "branch_terminal" ;       // branch terminal
    default                               : return "" ;                      // no environment
    }
}

/* static */ void
VeriNode::InitializeVerilogKeywords()
{
    if (_verilog_keywords) return ; // Already initialized

    _verilog_keywords = new Map(STRING_HASH, 397) ;

    // Verilog 95 keywords:
    (void) _verilog_keywords->Insert("always",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("and",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("assign",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("begin",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("buf",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("bufif0",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("bufif1",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("case",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("casex",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("casez",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("cmos",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("deassign",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("default",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("defparam",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("disable",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("edge",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("else",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("end",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endcase",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endfunction",         (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endmodule",           (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endprimitive",        (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endspecify",          (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endtable",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("endtask",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("event",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("for",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("force",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("forever",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("fork",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("function",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("highz0",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("highz1",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("if",                  (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("ifnone",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("initial",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("inout",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("input",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("integer",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("join",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("large",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("macromodule",         (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("medium",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("module",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("nand",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("negedge",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("nmos",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("nor",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("not",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("notif0",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("notif1",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("or",                  (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("output",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("parameter",           (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("pmos",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("posedge",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("primitive",           (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("pull0",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("pull1",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("pulldown",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("pullup",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("rcmos",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("real",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("realtime",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("reg",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("release",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("repeat",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("rnmos",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("rpmos",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("rtran",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("rtranif0",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("rtranif1",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("scalared",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("small",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("specify",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("specparam",           (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("strong0",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("strong1",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("supply0",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("supply1",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("table",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("task",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("time",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("tran",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("tranif0",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("tranif1",             (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("tri",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("tri0",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("tri1",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("triand",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("trior",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("trireg",              (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("vectored",            (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("wait",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("wand",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("weak0",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("weak1",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("while",               (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("wire",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("wor",                 (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("xnor",                (void *)(unsigned long)veri_file::VERILOG_95) ;
    (void) _verilog_keywords->Insert("xor",                 (void *)(unsigned long)veri_file::VERILOG_95) ;

    // Verilog 2001 keywords:
    (void) _verilog_keywords->Insert("automatic",           (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("config",              (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("endconfig",           (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("endgenerate",         (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("generate",            (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("genvar",              (void *)(unsigned long)veri_file::VERILOG_2K) ;
    //(void) _verilog_keywords->Insert("incdir",              (void *)(unsigned long)veri_file::VERILOG_2K) ; // Not keyword as per 1364 LRM
    //(void) _verilog_keywords->Insert("include",             (void *)(unsigned long)veri_file::VERILOG_2K) ; // Not keyword as per 1364 LRM
    (void) _verilog_keywords->Insert("library",             (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("localparam",          (void *)(unsigned long)veri_file::VERILOG_2K) ;
    //(void) _verilog_keywords->Insert("noshowcancelled",     (void *)(unsigned long)veri_file::VERILOG_2K) ; // Not keyword as per 1364 LRM
    //(void) _verilog_keywords->Insert("pulsestyle_onevent",  (void *)(unsigned long)veri_file::VERILOG_2K) ; // Not keyword as per 1364 LRM
    //(void) _verilog_keywords->Insert("pulsestyle_ondetect", (void *)(unsigned long)veri_file::VERILOG_2K) ; // Not keyword as per 1364 LRM
    //(void) _verilog_keywords->Insert("showcancelled",       (void *)(unsigned long)veri_file::VERILOG_2K) ; // Not keyword as per 1364 LRM
    (void) _verilog_keywords->Insert("signed",              (void *)(unsigned long)veri_file::VERILOG_2K) ;
    (void) _verilog_keywords->Insert("unsigned",            (void *)(unsigned long)veri_file::VERILOG_2K) ;
    //(void) _verilog_keywords->Insert("uwire",               (void *)(unsigned long)veri_file::VERILOG_2K) ;
    // Following five names are configuration keywords (only active when inside configuration):
//    (void) _verilog_keywords->Insert("cell",                (void *)(unsigned long)veri_file::VERILOG_2K) ;
//    (void) _verilog_keywords->Insert("design",              (void *)(unsigned long)veri_file::VERILOG_2K) ;
//    (void) _verilog_keywords->Insert("instance",            (void *)(unsigned long)veri_file::VERILOG_2K) ;
//    (void) _verilog_keywords->Insert("liblist",             (void *)(unsigned long)veri_file::VERILOG_2K) ;
//    (void) _verilog_keywords->Insert("use",                 (void *)(unsigned long)veri_file::VERILOG_2K) ;

    // System Verilog Keywords:
    (void) _verilog_keywords->Insert("always_comb",         (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("always_ff",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("always_latch",        (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("assert",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("assert_strobe",       (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("bit",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("break",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("byte",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    if (InRelaxedCheckingMode()) {
        (void) _verilog_keywords->Insert("char",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ; // Keywords in VCS & Verific but not ModelSim & not in 1800 LRM
    }
    (void) _verilog_keywords->Insert("const",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("continue",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("do",                  (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endinterface",        (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("enum",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("export",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("extern",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("forkjoin",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("return",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("iff",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("import",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("int",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("interface",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("logic",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("longint",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("modport",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("packed",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("priority",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("shortint",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("shortreal",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("static",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("struct",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("timeprecision",       (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("timeunit",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("type",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("typedef",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("union",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("unique",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("void",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("uwire",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("alias",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("before",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("bind",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("chandle",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("class",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("clocking",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("constraint",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("context",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("cover",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("dist",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endclass",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endclocking",         (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endprogram",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endproperty",         (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endsequence",         (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("extends",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("final",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("first_match",         (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("inside",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("intersect",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("join_any",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("join_none",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("local",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("new",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("null",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("program",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("property",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("protected",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("pure",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("rand",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("randc",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("ref",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("sequence",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("solve",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("string",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("super",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("this",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("throughout",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("var",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("virtual",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("wait_order",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("with",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("within",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("assume",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("bins",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("binsof",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("covergroup",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("coverpoint",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("cross",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endgroup",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endpackage",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("expect",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("foreach",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("ignore_bins",         (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("illegal_bins",        (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("matches",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("package",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("randcase",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("randsequence",        (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("tagged",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("wildcard",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;

    // VIPER #7967: Added the following system verilog(IEEE 1800 - 2009) keywords in the _verilog_keywords table:
    (void) _verilog_keywords->Insert("checker",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("endchecker",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("global",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("let",                 (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("accept_on",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("reject_on",           (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("sync_accept_on",      (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("sync_reject_on",      (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("nexttime",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("s_nexttime",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("implies",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("until",               (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("s_until",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("until_with",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("eventually",          (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("restrict",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("s_always",            (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("s_eventually",        (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("strong",              (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("unique0",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("untyped",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("weak",                (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("nettype",             (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;
    (void) _verilog_keywords->Insert("interconnect",        (void *)(unsigned long)veri_file::SYSTEM_VERILOG) ;

    // Verilog AMS Keywords:
    (void) _verilog_keywords->Insert("nature",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("endnature",           (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("inf",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("discipline",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("enddiscipline",       (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("ground",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("wreal",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("connectmodule",       (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("analog",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("from",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("exclude",             (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("branch",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("ac_stim",             (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("analysis",            (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("flicker_noise",       (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("noise_table",         (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("white_noise",         (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("abs",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("ceil",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("exp",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("floor",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("hypot",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("ln",                  (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("log",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("max",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("min",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("pow",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("sqrt",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("acos",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("acosh",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("asin",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("asinh",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("atan",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("atanh",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("atan2",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("cos",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("cosh",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("sin",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("sinh",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("tan",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("tanh",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("above",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("cross",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("initial_step",        (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("final_step",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("timer",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("ddt",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("idt",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("idtmod",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("ddx",                 (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("limexp",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("absdelay",            (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("laplace_nd",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("laplace_np",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("laplace_zd",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("laplace_zp",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("transition",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("slew",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("zi_nd",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("zi_np",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("zi_zd",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("zi_zp",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("last_crossing",       (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("potential",           (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("flow",                (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("domain",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("discrete",            (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("continuous",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("connectrules",        (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("endconnectrules",     (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("connect",             (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("split",               (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("merged",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("resolveto",           (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("driver_update",       (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("net_resolution",      (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("paramset",            (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("endparamset",         (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("aliasparam",          (void *)(unsigned long)veri_file::VERILOG_AMS) ;
    (void) _verilog_keywords->Insert("string",              (void *)(unsigned long)veri_file::VERILOG_AMS) ;

    // Verilog PSL keywords:
    (void) _verilog_keywords->Insert("A",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("AF",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("AG",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("AX",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("abort",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("assert",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("assume",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("assume_guarantee",    (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("before",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("before!",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("before!_",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("before_",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("boolean",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("clock",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("const",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("cover",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("countones",           (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("E",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("EF",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("EG",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("EX",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("endpoint",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("eventually!",         (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("F",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("fairness",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("fell",                (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("forall",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("G",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("in",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("inf",                 (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("inherit",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("isunknown",           (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("never",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next",                (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next!",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_a",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_a!",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_e",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_e!",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_event",          (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_event!",         (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_event_a!",       (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_event_a",        (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_event_e!",       (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("next_event_e",        (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("onehot",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("onehot0",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("prev",                (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("property",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("report",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("restrict",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("restrict_guarantee",  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("rose",                (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("sequence",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("stable",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("strong",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("U",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("until",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("until!",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("until!_",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("until_",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("vmode",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("vprop",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("vunit",               (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("W",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("whilenot",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("whilenot!",           (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("whilenot!_",          (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("whilenot_",           (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("within",              (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("within!",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("within!_",            (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("within_",             (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("X",                   (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    (void) _verilog_keywords->Insert("X!",                  (void *)(unsigned long)veri_file::VERILOG_PSL) ;
    // Special SV mentions (not required as they are differently handled): $root, $unit
}

/* static */ void
VeriNode::ResetVerilogKeywordMap()
{
    delete _verilog_keywords ;
    _verilog_keywords = 0 ;
}

/* static */
unsigned
VeriNode::IsEscapedIdentifier(const char *str)
{
    // Determine if string is an escaped identifier.  If it is,
    // return 1, else, return 0.
    if (!str || !*str) return 0 ;

    // First character should be [a-zA-Z_]
    if (!isalpha(*str) && (*str != '_')) return 1 ;

    // VIPER #2917: Check for dialect specific Verilog keywords:
    if (IsVerilogKeyword(str, veri_file::GetAnalysisMode())) return 1 ;

    // Following characters should be [a-zA-Z0-9_$]
    while (*++str) {
        switch (*str) {
        case ' ' : // SPECIAL CASE : Ignore spaces, since we use them as periods in hierarchical
                   // internal name representation.  Also they are not a valid escaped id character.
        case '_' :
        case '$' : break ;

        default :
            if (!isalpha(*str) && !isdigit(*str)) return 1 ;
            break ;
        }
    }

    return 0 ;  // string is not an escaped identifier
}

/* static */ unsigned
VeriNode::IsVerilogKeyword(const char *str, unsigned in_verilog_mode)
{
    if (!str) return 0 ;

    if (!_verilog_keywords) InitializeVerilogKeywords() ;
    VERIFIC_ASSERT(_verilog_keywords) ;

    MapItem *item = _verilog_keywords->GetItem(str) ;
    unsigned result = 0 ;
    if (item) {
        unsigned saved_mode = veri_file::GetAnalysisMode() ;
        veri_file::SetAnalysisMode(in_verilog_mode) ;

        switch ((unsigned long)item->Value()) {
        case veri_file::VERILOG_95    : result = 1 ; break ; // Verilog 95 keywords are always keyword
        case veri_file::VERILOG_2K    : if (IsVeri2001() || IsSystemVeri() || IsAms() || IsPSL()) result = 1 ; break ;
        case veri_file::SYSTEM_VERILOG: if (IsSystemVeri()) result = 1 ; break ; // CHECKME: SV+AMS or SV+PSL?
        case veri_file::VERILOG_AMS   : if (IsAms()) result = 1 ; break ;
        case veri_file::VERILOG_PSL   : if (IsPSL()) result = 1 ; break ;
        default: break ; // Never a keyword
        }

        veri_file::SetAnalysisMode(saved_mode) ;
    }
    return result ;
}

/* static */
char *
VeriNode::MakeVerilogName(const char *str)
{
    // This routine takes an incoming string, assuming that it's some sort of identifier
    // name, replace any spaces to periods (in the case of hierarchical ids), and
    // will escape the string (adding a space at the end) if the string contains
    // illegal characters.
    if (!str || !*str) return 0 ;
    char *result = 0 ; // return string handle

    // Is this a hierarchical identifier?
    if (strchr(str, ' ')) {
        // This is a hierarchical identifier!  Must check each individual
        // identifier to see if it should be escaped.
        Array ids ;
        char *copy = Strings::save(str) ;
        char *start = copy ;
        char *sp = strchr(start, ' ') ;
        do {
            if (sp) *sp = '\0' ;
            if (IsEscapedIdentifier(start)) {
                ids.InsertLast(Strings::save("\\", start, " ")) ;
            } else {
                ids.InsertLast(Strings::save(start)) ;
            }
            if (sp) start = sp + 1 ;
            sp = strchr(start, ' ') ;
        } while (sp) ;
        // Check last identifier
        if (IsEscapedIdentifier(start)) {
            ids.InsertLast(Strings::save("\\", start, " ")) ;
        } else {
            ids.InsertLast(Strings::save(start)) ;
        }
        Strings::free(copy) ;
        int i ;
        char *tmp ;
        FOREACH_ARRAY_ITEM(&ids, i, sp) {
            if (!i) {
                result = sp ; // Absorb
            } else {
                tmp = result ;
                result = Strings::save(tmp, ".", sp) ;
                Strings::free(sp) ; // Must free this
                Strings::free(tmp) ; // Must free this
            }
        }
    } else {
        // This is not a hierarchical identifier!
        if (IsEscapedIdentifier(str)) {
            result = Strings::save("\\", str, " ") ;
        } else {
            result = Strings::save(str) ;
        }
    }
    return result ;
}

/************************ UDP Table character encoding *****************/

char
VeriNode::GetCleanUdpEdge(char udp_char)
{
    switch (udp_char) {
    // rising edge
    case 'r' : // 01
    case 'p' : // 01 0x x1
    case 'B' : // 01
    case 'D' : // 0? == 01 00 0x
    case 'E' : // 0b == 01 00
    case 'Q' : // ?1 == 01 x1 11
    case 'V' : // b1 == 10 00
        return 'r' ;
    // falling edge :
    case 'f' : // 10
    case 'n' : // 10 1x x0
    case 'F' : // 10
    case 'I' : // 1? == 10 1x 11
    case 'J' : // 1b == 10 11
    case 'P' : // ?0 == 10 00 x0
    case 'U' : // b0 == 10 00
        return 'f' ;
    default :
        break ;
    }
    return 0 ; // this is not an edge, or does not a clean (0->1 or 1->0) edge.
}

unsigned
VeriNode::IsUdpXEdge(char udp_char)
{
    // Check if this species a 'dirty' edge : to or from 'x'.
    // Should NOT include edges that contain also clean edges.
    switch (udp_char) {
    case 'C' :
    case 'H' :
    case 'K' :
    case 'L' :
    case 'M' :
    case 'N' :
    case 'O' :
    case 'R' :
    case 'W' :
        return 1 ;
    default :
        break ;
    }
    return 0 ;
}

const char *
VeriNode::PrintUdpEdgeChar(char udp_char)
{
    if (!udp_char) return 0 ;

    switch (udp_char) {
    case 'A' : return "(00)" ;
    case 'B' : return "(01)" ;
    case 'C' : return "(0x)" ;
    case 'D' : return "(0?)" ;
    case 'E' : return "(0b)" ;

    case 'F' : return "(10)" ;
    case 'G' : return "(11)" ;
    case 'H' : return "(1x)" ;
    case 'I' : return "(1?)" ;
    case 'J' : return "(1b)" ;

    case 'K' : return "(x0)" ;
    case 'L' : return "(x1)" ;
    case 'M' : return "(xx)" ;
    case 'N' : return "(x?)" ;
    case 'O' : return "(xb)" ;

    case 'P' : return "(?0)" ;
    case 'Q' : return "(?1)" ;
    case 'R' : return "(?x)" ;
    case 'S' : return "(\?\?)" ; // ?? is a trigraph. Escape both ?'s.
    case 'T' : return "(?b)" ;

    case 'U' : return "(b0)" ;
    case 'V' : return "(b1)" ;
    case 'W' : return "(bx)" ;
    case 'X' : return "(b?)" ;
    case 'Y' : return "(bb)" ;

    default : return 0 ;
    }
}

/* static */ void
VeriNode::PopulateTransitionSetForChar(char c, Set &c_set)
{ // Viper #5812/3444
    switch(c) {
    case '?':
        (void) c_set.Insert((void *)'x') ;

    case 'b':
        (void) c_set.Insert((void *)'0') ;
        (void) c_set.Insert((void *)'1') ;
        break ;
    case '*':
    case 'S': // any value change
        (void) c_set.Insert((void *)'B') ;
        (void) c_set.Insert((void *)'C') ;
        (void) c_set.Insert((void *)'F') ;
        (void) c_set.Insert((void *)'H') ;
        (void) c_set.Insert((void *)'K') ;
        (void) c_set.Insert((void *)'L') ;
        break ;
    case 'r':
        (void) c_set.Insert((void *)'B') ;
        break ;
    case 'f':
        (void) c_set.Insert((void *)'F') ;
        break ;
    case 'p':
        (void) c_set.Insert((void *)'B') ;
        (void) c_set.Insert((void *)'C') ;
        (void) c_set.Insert((void *)'L') ;
        break ;
    case 'n':
        (void) c_set.Insert((void *)'F') ;
        (void) c_set.Insert((void *)'H') ;
        (void) c_set.Insert((void *)'K') ;
        break ;
    case 'D':
        (void) c_set.Insert((void *)'C') ;

    case 'E':
        (void) c_set.Insert((void *)'B') ;
        break ;
    case 'I':
        (void) c_set.Insert((void *)'H') ;

    case 'J':
        (void) c_set.Insert((void *)'F') ;
        break ;
    case 'N':
    case 'O':
        (void) c_set.Insert((void *)'K') ;
        (void) c_set.Insert((void *)'L') ;
        break ;
    case 'P':
        (void) c_set.Insert((void *)'F') ;
        (void) c_set.Insert((void *)'K') ;
        break ;
    case 'Q':
        (void) c_set.Insert((void *)'B') ;
        (void) c_set.Insert((void *)'L') ;
        break ;
    case 'R':
        (void) c_set.Insert((void *)'C') ;
        (void) c_set.Insert((void *)'H') ;
        break ;
    case 'T':
        (void) c_set.Insert((void *)'B') ;
        (void) c_set.Insert((void *)'F') ;
        (void) c_set.Insert((void *)'L') ;
        (void) c_set.Insert((void *)'K') ;
        break ;
    case 'U':
        (void) c_set.Insert((void *)'F') ;
        break ;
    case 'V':
        (void) c_set.Insert((void *)'B') ;
        break ;
    case 'W':
        (void) c_set.Insert((void *)'C') ;
        (void) c_set.Insert((void *)'H') ;
        break ;
    case 'X':
        (void) c_set.Insert((void *)'B') ;
        (void) c_set.Insert((void *)'C') ;
        (void) c_set.Insert((void *)'F') ;
        (void) c_set.Insert((void *)'H') ;
        break ;
    case 'Y':
        (void) c_set.Insert((void *)'B') ;
        (void) c_set.Insert((void *)'F') ;
        break ;
    default:
        (void) c_set.Insert((void *)(long)c) ;
        break ;
    }

    return ;
}

unsigned
VeriNode::AreUdpCharsOverlapping(char c1, char c2)
{ // Viper #5812/3444
    if (c1 == c2) return 1 ;
    if (!c1 || !c2) return 0 ;

    Set c1_set(NUM_HASH) ;
    PopulateTransitionSetForChar(c1, c1_set) ;

    Set c2_set(NUM_HASH) ;
    PopulateTransitionSetForChar(c2, c2_set) ;

    long c ;
    SetIter si ;
    FOREACH_SET_ITEM(&c1_set, si, &c) {
        if (c2_set.Get((void *)c)) return 1 ;
    }

    return 0 ;
}

char
VeriNode::GetUdpEdgeChar(const char *udp_char)
{
    if (!_udp_edge_chars) {
        _udp_edge_chars = new Map(STRING_HASH, 29) ;

        _udp_edge_chars->Insert("(00)",(void*)'A') ;
        _udp_edge_chars->Insert("(01)",(void*)'B') ;
        _udp_edge_chars->Insert("(0x)",(void*)'C') ;
        _udp_edge_chars->Insert("(0?)",(void*)'D') ;
        _udp_edge_chars->Insert("(0b)",(void*)'E') ;

        _udp_edge_chars->Insert("(10)",(void*)'F') ;
        _udp_edge_chars->Insert("(11)",(void*)'G') ;
        _udp_edge_chars->Insert("(1x)",(void*)'H') ;
        _udp_edge_chars->Insert("(1?)",(void*)'I') ;
        _udp_edge_chars->Insert("(1b)",(void*)'J') ;

        _udp_edge_chars->Insert("(x0)",(void*)'K') ;
        _udp_edge_chars->Insert("(x1)",(void*)'L') ;
        _udp_edge_chars->Insert("(xx)",(void*)'M') ;
        _udp_edge_chars->Insert("(x?)",(void*)'N') ;
        _udp_edge_chars->Insert("(xb)",(void*)'O') ;

        _udp_edge_chars->Insert("(?0)",(void*)'P') ;
        _udp_edge_chars->Insert("(?1)",(void*)'Q') ;
        _udp_edge_chars->Insert("(?x)",(void*)'R') ;
        _udp_edge_chars->Insert("(\?\?)",(void*)'S') ; // ?? is a trigraph. Escape both ?'s.
        _udp_edge_chars->Insert("(?b)",(void*)'T') ;

        _udp_edge_chars->Insert("(b0)",(void*)'U') ;
        _udp_edge_chars->Insert("(b1)",(void*)'V') ;
        _udp_edge_chars->Insert("(bx)",(void*)'W') ;
        _udp_edge_chars->Insert("(b?)",(void*)'X') ;
        _udp_edge_chars->Insert("(bb)",(void*)'Y') ;
    }

    return (char)((long)_udp_edge_chars->GetValue(udp_char)) ;
}

/*static*/ void
VeriNode::ResetUdpEdgeCharMap()
{
    delete _udp_edge_chars ;
    _udp_edge_chars = 0 ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
/************************ Mixed language handling  *****************/
VhdlPrimaryUnit *VeriNode::GetVhdlUnitForVerilogInstance(const char *entity_name, const char *lib_name, unsigned search_from_loptions, char ** architecture) // Pick up the vhdl entity instantiated inside a verilog module
{
    // Viper 6422: added argument architecture with default value zero: This argument is used
    // to store the architecture name if it is a part of the entity_name.
    VhdlPrimaryUnit *vhdl_unit = 0 ;
    if (!entity_name) return vhdl_unit ;

    // Viper # 3216 : extension for dual language support..
    // Check following :
    // (1) module name may be escaped.. if so, it may not need escaping in
    //     vhdl. Strip off the escaping and search in vhdl.
    // (2) Entity name may need escaping in vhdl. Escape it and search.
    // (3) module name may need escaping in both vhdl and verilog. Strip off
    //     verilog escaping and do vhdl escaping before searching for vhdl.
    char *ename = Strings::save(entity_name) ;
    char *ename_orig = ename ;

    unsigned is_escaped = (ename[0] == '\\' && ename[Strings::len(ename) - 1] == ' ') ;
    if (is_escaped) {
        // check for (1) above
        ename += 1 ;
        ename[Strings::len(ename) - 1] = '\0' ;
    }

    // if escaped, module name is already modified
    // now check for (2) and (3)
    char *ename_escaped = Strings::save("\\", ename, "\\") ;

    // Viper 7100 : search from loptions afront. This is the same used in GetModuleInstantiation and
    // also done by popular simulators.
    if (search_from_loptions) {
        // VIPER #2762 : Find vhdl unit from -L option specified libraries
        // VIPER #5035 : Pass argument so that following routine does not search verilog
        // part again:
        vhdl_unit = vhdl_file::GetUnitFromLOptions(entity_name, 1, 1 /*exclude verilog*/) ;

        // Viper # 3216
        if (!vhdl_unit && is_escaped) vhdl_unit = vhdl_file::GetUnitFromLOptions(ename, 1, 1 /*exclude verilog*/) ;
        if (!vhdl_unit) vhdl_unit = vhdl_file::GetUnitFromLOptions(ename_escaped, 1, 1 /*exclude verilog*/) ;
    }

    if (vhdl_unit) {
        Strings::free(ename_escaped) ; // Memory leak fix
        Strings::free(ename_orig) ; // Memory leak fix
        return vhdl_unit ;
    }

    // If not found in -L options, find in instantiating module's library
    // and then in all vhdl libraries
    if (lib_name) {
        VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(lib_name,1) ;
        vhdl_unit = vhdl_lib->GetPrimUnit(entity_name) ;

        // Viper # 3216
        if (!vhdl_unit && is_escaped) vhdl_unit = vhdl_lib->GetPrimUnit(ename) ;
        if (!vhdl_unit) vhdl_unit = vhdl_lib->GetPrimUnit(ename_escaped) ;
    }

    // VIPER #1904/2279 : If vhdl unit is not initially found,
    // look to see if module_name contains vhdl library path
    // RD: There is a testcase in 2279 with an escaped name
    // instantiation which should trigger this code.
    // Not clear if ModelSim supports this..
    if (!vhdl_unit) {
        // Since unit was not found, determine if unit name includes
        // library name (look for '.')
        if (strchr(entity_name, '.')) {
            // Library name is included.  Isolate it and do a lookup.
            char *vhdl_lib_name = Strings::save(entity_name) ;
            char *unit_name = strchr(vhdl_lib_name, '.') ;
            VERIFIC_ASSERT(unit_name) ;
            *unit_name = '\0' ; // Truncate it.
            // Now do a lookup again
            VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(vhdl_lib_name,1) ;
            vhdl_unit = vhdl_lib->GetPrimUnit(++unit_name, 1/*restore*/) ;
          
            if (!vhdl_unit) {
                // Viper# 3262 : module name may also contain the *architecture name
                // e.g. work.entity_name(arch_name) We need to extract arch name and
                // pass it to elaboration for getting the correct secondary unit
                char *arch_name = strrchr(unit_name, '(') ;
                // Viper 6250: an escaped entity name may contain a '(' as a part of
                // its name and hence may not indicate a *architecture name
                if (arch_name && *unit_name == '\\') {
                    char *escape_char =  strrchr(unit_name, '\\') ;
                    if (escape_char && arch_name < escape_char) arch_name = 0 ;
                }
                if (arch_name) {
                    if (architecture) {
                        *architecture = Strings::save(arch_name + 1) ;
                        (*architecture)[Strings::len(*architecture) -1] = 0 ;
                    }
                    *arch_name = '\0' ; // Truncate it.
                    vhdl_unit = vhdl_lib->GetPrimUnit(unit_name, 1/*restore*/) ;
                }
            }
            // Viper # 3216 : TODO : unit_name may be escaped or need escaping
            Strings::free(vhdl_lib_name) ;
        }
    }

    if (!vhdl_unit) {
        // Viper# 3262 : Since unit was not found, determine if unit name
        // includes *architecture name but not library name
        char *arch_name = strrchr(ename, '(') ;
        // Viper 6250: an escaped entity name may contain a '(' as a part of
        // its name and hence may not indicate a *architecture name
        if (arch_name && *ename == '\\') {
            char *escape_char = strrchr(ename, '\\') ;
            if (escape_char && arch_name < escape_char) arch_name = 0 ;
        }
        if (arch_name) {
            if (architecture) {
                *architecture = Strings::save(arch_name + 1) ;
                (*architecture)[Strings::len(*architecture) -1] = 0 ;
            }
            *arch_name = '\0' ; // Truncate it.
            // Following may cause false positives with mixed language and escaping
            vhdl_unit = GetVhdlUnitForVerilogInstance(ename, lib_name, search_from_loptions) ;
        }
    }

    Strings::free(ename_orig) ;
    Strings::free(ename_escaped) ;

    if (!vhdl_unit && architecture && *architecture) {
        Strings::free(*architecture) ;
        *architecture = 0 ;
    }

    return vhdl_unit ;
}
#endif

// VIPER #6594 (issue 131): Retired with addition of veri_file::AnalyzeExpr() API.
// Equivalent call is: veri_file::AnalyzeExpr(value, veri_file::GetAnalysisMode()) ;
// Viper 3395 Function to convert string to expression
#if 0
/*static*/ VeriExpression *
VeriNode::StringToExpression(const char *value)
{
    VeriExpression *actual = 0 ;
    unsigned len = (unsigned)Strings::len(value) ;
    // Create the correct VeriExpression tree for the actual :
    if (len>1 && (value[0]=='"') && (value[len-1]=='"')) {
        // String literal
        // Strip first/last char and create a VeriConstVal :
        char *tmp = Strings::save(value+1) ;
        tmp[Strings::len(tmp)-1] = '\0' ; // Strip last char
        actual = new VeriConstVal(tmp,VERI_STRING) ;
        Strings::free(tmp) ;
    } else if (value[0]=='\'') {
        // Unsized Bit String
        actual = new VeriConstVal(value, VERI_BASED_NUM) ;
    } else if (isdigit(value[0]) && ::strchr(value,'\'')) {
        // Sized Bit String
        actual = new VeriConstVal(value, VERI_SIZED_BASED_NUM) ;
    } else if (isdigit(value[0]) || (value[0]=='-') || (value[0]=='+')) {
        // A number (integer or real)
        char *remain = 0 ;
        if (::strchr(value,'.')) {
            // Real Literal ?
            actual = new VeriRealVal(strtod(value,&remain)) ;
        } else {
            // Integer Literal ?
            actual = new VeriIntVal((int)strtol(value,&remain,10)) ;
        }
        // Issue 1878 : Check legal number
        if (remain && (remain[0]!='\0')) {
            delete actual ;
            actual = 0 ; // give error below
        }
    } else if (value[0] == '{' && value[len-1] == '}') {
        // VeriConcat . This was introduced for Viper 3395
        Array* exprs = new Array() ;
        char* tmp = Strings::save(value) ;
        char *curr_str = tmp ;
        unsigned i = 0 ;
        unsigned j = 0 ;
        while (value[i] != '\0') {
            if (value[i] == ',' || value[i] == '}') {
               curr_str[j] = '\0' ;
               if (curr_str[0] == '{') {
                   curr_str++ ;
                   j-- ;
               }
               exprs->InsertLast(StringToExpression(curr_str)) ;
               curr_str = curr_str + j + 1 ;
               j = 0 ;
               i++ ;
               continue ;
            }
            i++ ;
            j++ ;
        }
        actual = new VeriConcat(exprs) ;
        Strings::free(tmp) ;
    }
    return actual ;
}
#endif

/************************ Pragma handling  ************************/

Map *
VeriNode::PragmasToAttributes(Map *attributes, Map *pragmas)
{
    // Take pragma's (char*->char*) from the 'pragmas' table,
    // and set as Expression*'s into 'attributes' table (char*->VeriExpression*)
    // If 'attributes' does not exist, create it (if needed).
    // Absorb both 'attributes' and 'pragmas' tables.
    // Return 'attributes' (possibly adjusted)
    if (!pragmas) return attributes ; // no need to change anything.

    // Create attribute table (Map) if it doesn't exist
    if (!attributes) attributes = new Map(STRING_HASH) ;
    MapIter mi ;
    char *name, *val ;
    VeriConst *value ;
    VeriExpression *expr ;
    // Transfer all pragmas to attribute table
    FOREACH_MAP_ITEM(pragmas, mi, &name, &val) {
        // If the key already exists, delete value, and force overwrite later on
        value = (VeriConst*)attributes->GetValue(name) ;
        if (value) delete value ;
        // pragma 'val' can be a string, a (Verilog) number, or a identifier (defined in verilog.l).
        if (!val) {
            // Insert a NULL. Will turn to a '1' when we execute it (in ApplyAttributes).
            //(void) attributes->Insert(name, 0, 1/*force_overwrite*/) ;
            expr = 0 ;
        } else if ((val[0] != '"') && ::strchr(val,'\'')) {
            // VIPER #6096: Treat attribute value as base number when starting character is not "
            // We have a Verilog based number here.
            //(void) attributes->Insert(name, new VeriConstVal(val, VERI_BASED_NUM), 1/*force_overwrite*/) ;
            expr = new VeriConstVal(val, VERI_BASED_NUM) ;
        } else if (isdigit(val[0]) && isdigit(val[Strings::len(val)-1])) {
            // We have a unsigned number here.
            //(void) attributes->Insert(name, new VeriIntVal(Strings::atoi(val)), 1/*force_overwrite*/) ;
            expr = new VeriIntVal(Strings::atoi(val)) ;
        } else {
            // Anything else is saved as a string value, including time literals (VIPER #2191)

            // We need to first strip off any opening/closing double quotes (if they exist)
            unsigned sz = (unsigned)Strings::len(val) ;
            if (val[0] == '"' && val[sz-1] == '"') {
                val[sz-1] = '\0' ;
                char *p = val ;
                while (*(p++)) { *(p-1) = *p ; }
            }
            //(void) attributes->Insert(name, new VeriConstVal(val, VERI_STRING), 1/*force_overwrite*/) ; // absorb name
            expr = new VeriConstVal(val, VERI_STRING) ;
        }
        // VIPER #6878: Make this pragma identifiable as pragma in attribute list:
        //VERIFIC_ASSERT(expr) ; // This holds, but comment it out!
        if (expr) expr->SetLinefile(0) ; // Reset linefile info
        (void) attributes->Insert(name, expr, 1/*force_overwrite*/) ;
        Strings::free(val) ; // delete val
    }
    delete pragmas ; // all values are absorbed.
    return attributes ;
}

/***************** Verilog Instance to Vhdl insctance **************/
//Function to convert VeriModuleInstantiation to VhdlComponentInstantiationStatement
VhdlComponentInstantiationStatement* VeriModuleInstantiation::ConvertInstance(VhdlInstantiatedUnit* instantiated_unit, const VeriInstId *module_instId) const
{
    unsigned i ;
    const char *formal_name ;
    VeriConst *const_val = 0 ;
    VeriBaseValue *b_val ;

    VeriExpression *actual ;
    Array *generic_clause = new Array() ;
    //VeriModule *module = GetInstantiatedModule() ;
    //if (!module) return 0 ;
    // Iterate through the array of parameters
    FOREACH_ARRAY_ITEM(_param_values, i, actual) {
        if (!actual) continue ; // 'open' parameter association ?
        formal_name = actual->NamedFormal() ;
        actual = actual->GetConnection() ;
        if (!actual || actual->IsOpen()) continue ;
        if (VeriNode::IsStaticElab()) {
            b_val = actual->StaticEvaluateInternal(0, 0, 0, 0) ; // evaluate actual
            if (!b_val) {
                actual->Error("value for parameter %s is not constant", (formal_name) ? formal_name : "unknown") ;
                break ;
            }
            const_val = b_val->ToConstVal(actual) ; // Convert to VeriConst
            delete b_val ;
        }
        if (!const_val) continue ;
        // Set the 'actual' line/file on the new constval :
        const_val->SetLinefile(actual->Linefile()) ;
        VhdlExpression *vhdl_actual = 0 ;
        if (const_val->IsString()) {
            char *image = const_val->Image() ;
            vhdl_actual = new VhdlStringLiteral(Strings::save(image)) ; // Convert to VhdlExpression*
        } else if (const_val->IsReal()) {
            char *image = const_val->Image() ; // Find the string equivalent
            double init_val = Strings::atof(image) ; // Convert the string to real
            vhdl_actual = new VhdlReal(init_val) ; // Convert to VhdlExpression*
        } else { // if the constant expression is an integer
            // Ignore parameters having values as sized constants where the size of the
            // constant is greater than 32 bits.
            if ((const_val->IsUnsizedConst() == 0) && (const_val->Size(0) > 32)) continue ;
            verific_int64 init_val = const_val->Integer() ;
            vhdl_actual = new VhdlInteger(init_val) ; // Convert to VhdlExpression*
        }
        VhdlDiscreteRange *generic_ref =  vhdl_actual ;
        if (formal_name) {
            VhdlIdRef *vhdl_formal = new VhdlIdRef(Strings::save(formal_name)) ;
            generic_ref = new VhdlAssocElement(vhdl_formal, vhdl_actual) ;
            // Viper 5889 Set that this assoc is from verilog actual
            generic_ref->SetFromVerilog() ;
        }
        delete const_val ;
        const_val = 0 ;
        generic_clause->InsertLast(generic_ref) ;
    }

    // VIPER #6988: Support .* style port connection in bined instance
    // We create a set containing the names of the formal ports of the module that
    // is instantiated. Then remove the port names that are named-formal/open-port/positional-port,
    // so the names for formal ports that are left in the set are for *. style.
    // Then for each port connections we check for *. port and create a portref with vhdl formal(IdRef with formal name)
    // vrs vhdl actual (IdRef with formal name). For other port connections we create portref with
    // vhdl formal expr(IdRef with formal name) vrs vhdl actual expr (IdRef with veri_actual->GetName()).

    // Get the module that is instantiated
    VeriModule *master = GetInstantiatedModule() ;
    if (!master) {
        delete generic_clause ;
        delete instantiated_unit ; // Memory leak fix
        return 0 ;
    }
    // Get the formal ports of the module
    Array *formals = master->GetPorts() ;

    // Get the port connects of the module
    Array *exact_formals = master->GetPortConnects() ;
    if (!exact_formals) {
        delete generic_clause ;
        delete instantiated_unit ; // Memory leak fix
        return 0 ;
    }
    // Check if all declared formal ports can be processed for .name and .* style
    // port connection in instantiation. Some of the Verilog95 style port declarations cannot be processed!
    VeriExpression *port ;
    unsigned is_ansi_port = 0 ;
    FOREACH_ARRAY_ITEM(exact_formals, i, port) {
        if (!port) continue ;
        if (port->IsAnsiPortDecl()) is_ansi_port = 1 ;
        break ;
    }

    // If formal is named port like .a(p), expand the .* present in instantiation
    Set formal_names(STRING_HASH, exact_formals->Size()) ;
    VeriIdDef *formal_id ;
    if (is_ansi_port) {
        FOREACH_ARRAY_ITEM(formals, i, formal_id) {
            if (!formal_id) continue ;
            (void) formal_names.Insert(formal_id->Name()) ;
        }
    } else {
        FOREACH_ARRAY_ITEM(exact_formals, i, port) {
            if (!port) continue ;
            formal_name = port->NamedFormal() ;
            if (formal_name) port = port->GetConnection() ;
            formal_id = (port) ? port->FullId(): 0 ;
            if (formal_name || formal_id) { // named port or identifier reference
                (void) formal_names.Insert(formal_name ? formal_name : (formal_id) ? formal_id->Name(): "") ;
            } else { // concat '{a, b}/indexed a[3:0] /open/.* in port list
                delete generic_clause ;
                delete instantiated_unit ; // Memory leak fix
                return 0 ; // Can't process this type of port declaration  for .name or .* style connection
            }
        }
    }

    // Create a Set to store the formal names of the ports
    Set formal_port_names(STRING_HASH, formal_names.Size()) ;
    // Insert the port names into the set
    SetIter si ;
    FOREACH_SET_ITEM(&formal_names, si, &formal_name) {
        if (!formal_name) continue ;
        (void) formal_port_names.Insert(formal_name) ;
    }

    // For module instance port_connects:
    VeriExpression *connection ;
    Array* port_connects = module_instId->GetPortConnects() ;
    unsigned j ;
    // Now, convert all the .name style port connection to .name(name) style
    // and delete the port named from the Set that should not be processed for .*
    FOREACH_ARRAY_ITEM_BACK(port_connects, j, connection) {
        if (!connection) continue ;

        if (!connection->IsDotStar() && ((!connection->IsNameExtendedClass() && connection->IsOpen()) || // Open port connection : assuming positional
                       (!connection->NamedFormal()))) { // Normal expression connected, assuming positional
                // We should not process an open or positional port connection for .*. So, try to remove it
            if (formal_port_names.Size() > j) {
                const char *port_name = (const char *)formal_port_names.GetAt(j) ;
                (void) formal_port_names.Remove(port_name) ;
            }
        } else if (!connection->IsDotStar() && connection->NamedFormal()) {
            // Assume it to be a .name style port connection
            const char *port_name = connection->NamedFormal() ;
            if (formal_port_names.GetItem(port_name)) {
                // We should not process this .name() style port connection for .*
                // Remove this also
                (void) formal_port_names.Remove(port_name) ;
            } else if (formal_names.GetItem(port_name)) {
                // Not in the current Set but the port was there. So,
                // it is already connected, produce a warning message about this:
                Warning("port %s is already connected", port_name) ;
            } else {
                Warning("cannot find port %s on this module", port_name) ;
            }
        }
    }

    Array *port_clause = new Array() ;
    // Iterate through the array of ports
    FOREACH_ARRAY_ITEM(port_connects, i, connection) {
        if (!connection) continue ;
        VhdlExpression *vhdl_actual = 0 ;
        VhdlExpression *port_ref = 0 ;
        // Check whether it is named connection or not
        formal_name = connection->NamedFormal() ;
        VeriExpression *veri_actual = (connection->NamedFormal()) ? connection->GetConnection() : connection ;
        if (!veri_actual) continue ;
        if (veri_actual->IsDotStar()) { // for .* stype port
            // We have got a dot-star port connection
            // Create the port connection for non-existing one. Remove the .* port connection from the Array.
            if (!formal_port_names.Size()) {
                // .* exists in port connection, but all formal identifiers
                // are already there, so do nothing for .* port
                continue ;
            }

            // Create name=>name port-ref for .* style ports:
            // ie. create a portref with vhdl formal(IdRef with formal name) vrs vhdl actual (IdRef with formal name).
            char *name ;
            FOREACH_SET_ITEM(&formal_port_names, si, &name) {
                if (!name) continue ;
                vhdl_actual = new VhdlIdRef(Strings::save(name)) ;
                port_ref =  vhdl_actual ;
                VhdlIdRef *vhdl_formal = new VhdlIdRef(Strings::save(name)) ;
                port_ref = new VhdlAssocElement(vhdl_formal, vhdl_actual) ;
                port_clause->InsertLast(port_ref) ;
                (void) formal_port_names.Remove(name) ;
            }
        } else {
            // Viper 7225: In order to support hierarchical names we convert the verilog selected name
            // to a Vhdl selected name and then call FindExternalObject to get the bounded ID. Once the
            // ID is obtained we create a new vhdl actual with the ID. Conversions for names are only supported
            // we do not convert expressions acros languages.

            // For other port connections we create portref with vhdl formal expr(IdRef with formal name)
            // vrs vhdl actual expr (IdRef with veri_actual->GetName()).
            VhdlIdDef *vhdl_id = 0 ;
            VhdlName *vhdl_actual_name = veri_actual->ConvertName() ;
            Array *assoc_list = vhdl_actual_name ? vhdl_actual_name->GetAssocList() : 0 ;
            VhdlName* orig_vhdl_actual_name = vhdl_actual_name ;
            if (assoc_list && vhdl_actual_name) {
                // the assoc list would be added later.
                vhdl_actual_name = vhdl_actual_name->GetPrefix() ;
            }
            if (vhdl_actual_name) {
                VhdlScope *save_scope = VhdlTreeNode::_present_scope ;
                vhdl_id = vhdl_actual_name->FindExternalObject(VhdlTreeNode::GetVhdlAccent(), 0) ;
                VhdlTreeNode::_present_scope = save_scope ;

                // if the id is a port or generic then bind the actual instead of formal.
                // if the assoc list is present then we create a indexed name only if the
                // actual is a name.
                if (vhdl_id && (vhdl_id->IsPort() || vhdl_id->IsGeneric())) {
                    VhdlName* inst_name = vhdl_actual_name->GetPrefix() ;
                    VhdlIdDef *inst_id = inst_name ? inst_name->FindExternalObject(VhdlTreeNode::GetVhdlAccent(), 0) : 0 ;
                    if (inst_id && inst_id->IsLabel()) { // component/block/generate label
                        VhdlStatement *labeled_stmt = inst_id->GetStatement() ;
                        VhdlExpression *vhdl_actual_expr = labeled_stmt ?  labeled_stmt->GetActualExpression(vhdl_id) : 0 ;
                        VhdlMapForCopy old2new ;
                        vhdl_actual = vhdl_actual_expr ? vhdl_actual_expr->CopyExpression(old2new) : 0 ;
                    }
                }
                if (!vhdl_id) {
                    vhdl_actual = orig_vhdl_actual_name ;
                } else {
                    if (assoc_list) {
                        vhdl_id = vhdl_actual ? vhdl_actual->GetId() : vhdl_id ;
                        vhdl_actual = new VhdlIndexedName(new VhdlIdRef(vhdl_id), assoc_list ) ;
                    } else if (!vhdl_actual) {
                        vhdl_actual = new VhdlIdRef(vhdl_id) ;
                    }
                }
            }

            // The vhdl_id cannot be resolved so create unresolved vhdl actual
            if (!vhdl_actual) {
                vhdl_actual = new VhdlIdRef(Strings::save(veri_actual->GetName())) ;
            }

            vhdl_actual->SetLinefile(veri_actual->Linefile()) ;

            port_ref =  vhdl_actual ;
            if (formal_name) {
                VhdlIdRef *vhdl_formal = new VhdlIdRef(Strings::save(formal_name)) ;
                vhdl_formal->SetLinefile(instantiated_unit->Linefile()) ;
                port_ref = new VhdlAssocElement(vhdl_formal, vhdl_actual) ;
                port_ref->SetLinefile(veri_actual->Linefile()) ;
            }
            port_clause->InsertLast(port_ref) ;
            if (vhdl_id) delete vhdl_actual_name ; // if id is not found we use the original name hence cannot delete it
        }
    }

    VhdlComponentInstantiationStatement* unit_inst = new VhdlComponentInstantiationStatement(instantiated_unit, generic_clause, port_clause) ;
    unit_inst->SetLinefile(instantiated_unit->Linefile()) ;
    return (unit_inst) ;
}

/************* Reference count handling for identifier (#4977) **********/
// In yacc identifier to be declared is parsed as hier_ref, so rule is
//   hier_ref hier_ref ";"
// and in hier_ref rule we try to resolve the identifiers to do proper resolve
// for declaration like   P P ; // when type 'P' is defined in upper scope
// If we have  'wire x' when x is declared in wildcard imported package
// 'x' is parsed by hier_ref rule and Find is called to resolve 'x'. Find
// make this 'x' explicitly imported as when referenced wildcard imported
// objects become explicitly visible. Now we try to declare 'x' in present scope
// we find that it is already declared and produce error.
//
// To get rid of this, following map is maintained to store reference count of
// identifier. For decl like 'wire x' reference count will be 2 (one from
// Find called from hier_ref rule and 2 nd from FindLocal while declaring the id.
// If reference count becomes other than '2' we will considered identifier as
// previously used/declared.

// Increase reference count : We will only use this table when identifier from
// wildcard imported package will be used. So reference count will be 1 from 0
// only when we will make the identifier reference explicit in _imported_scope
// (when id from ::* imported package is used, it becomes explitly imported)
// If any identifier is explicitly imported, that will not be entered in this map.
// From 'FindExplicitlyImported' API we will increase reference count only when
// reference count already exists. This will prevent entry for explicitly
// imported ids.
void VeriNode::IncreaseReferenceCount(const VeriIdDef *id, unsigned inc_if_gt_1)
{
    if (!id) return ;
    if (!_ref_count_map) _ref_count_map = new Map(POINTER_HASH) ;
    unsigned long ref_count = (unsigned long)_ref_count_map->GetValue(id) ;
    if ((ref_count == 0 && !inc_if_gt_1) || ((ref_count >= 1) && inc_if_gt_1)) {
       ref_count++ ;
       (void) _ref_count_map->Insert(id, (void*)ref_count, 1) ;
    }
}
unsigned VeriNode::GetReferenceCount(const VeriIdDef *id)
{
    if (!id) return 0 ;
    unsigned long ref_count = (_ref_count_map) ? (unsigned long)_ref_count_map->GetValue(id): 0 ;
    return ref_count ;
}
void VeriNode::ResetReferenceCountMap()
{
    delete _ref_count_map ;
    _ref_count_map = 0 ;
}

/* static */ double
VeriNode::RoundToNearestInteger(double val)
{
    // VIPER #5059: Round values >= D.5 to D+1:
    // Verilog AMS 2.3 LRM section 4.2.1.1, IEEE 1364 LRM section 2.5.3: Conversion:
    // "Real numbers shall be converted to integers by rounding the real number to the
    // nearest integer, rather than by truncating it. Implicit conversion shall take
    // place when a real number is assigned to an integer. The ties shall be rounded
    // away from zero". So, do not truncate. Use the following instead:
    // DD: 10/2010: Simplified unsing floor/ceil appropriately (rev 1.596):
    double ret_val = val ;
    if (val >= 0) {
        ret_val = floor(val+0.5) ; // Adjusting +ve numbers:  1.2 =>  1;  1.6 =>  2
    } else {
        ret_val = ceil(val-0.5) ;  // Adjusting -ve numbers: -1.2 => -1; -1.6 => -2
    }
    return ret_val ; // ret_val is now an integer in real domain
}

/* Strings cannot go beyond one line in Verilog ! */
/* All escaped characters LRM 2.6.3 are taken care of */
/* Have to replace the escaped characters by their ASCII equivalent */
/* Special handling for \0 inside strings: keep them as \\0 for processing later */
/* static */ char *
VeriNode::MakeVerilogPrintable(char *str)
{
    if (!str) return 0 ;

    char *newest = str ;
    char *runner = str ;

    while (*runner != '\0') {
        if (*runner == '\\') {
            // escaped character. Look at the next one :
            runner++ ;
            switch (*runner) {
                case 'n' : *newest = '\n' ; newest++ ; break ;
                case 't' : *newest = '\t' ; newest++ ; break ;
                case 'r' : *newest = '\r' ; newest++ ; break ;
                // CHECKME: Also include 'v', 'f', 'a' etc?
                case '\n' :
                    // VIPER 2602 : skip escaped newline altogether, do not take anything:
                    //runner++ ; *newest = *runner ;
                    break ;
                default : {
                    if (*runner >= '0' && *runner <= '7') {
                        // Viper #3591 : Support escaped character - octal values
                        // Change the octal value to corresponding ascii value
                        int val = *runner - '0' ;
                        if (*(runner+1) >= '0' && *(runner+1) <= '7') {
                            ++runner ;
                            val = val * 8 + *runner - '0' ;
                        }
                        if (*(runner+1) >= '0' && *(runner+1) <= '7') {
                            ++runner ;
                            val = val * 8 + *runner - '0' ;
                        }

                        if (!val) {
                            // Verilog AMS 2.3 section 3.3 string data type: All "\0" characters
                            // in the string literal are ignored (i.e., removed from the string):
                            if (VeriNode::IsAms()) break ; // Ignore "\0" in string in AMS mode

                            // VIPER #4483: Support '\0' inside strings:
                            // Restore "\0" as-it-is here, we will process it from constructor of VeriConstVal.
                            *newest = '\\' ; newest++ ;
                            val = '0' ;
                        }

                        *newest = (char)val ; newest++ ; break ;
                    } else {
                        if (*runner == '\\') {
                            // VIPER #4737: Keep escaped back-slashes as it is here.
                            // We will process it from constructor of VeriConstVal:
                            *newest = '\\' ;
                            newest++ ;
                        }
                        *newest = *runner ; newest++ ; break ;
                    }
                }
            }
        } else {
            *newest = *runner ; newest++ ;
        }
        runner++ ;
    }
    unsigned l = (*(newest-1) == '"') ? 1 : 0 ; // Need to strip last quote character, if any
    *(newest-l) = '\0' ; // Close the string and strip last quote character along
    return (*str == '"') ? str+1 : str ; // Ignore the the first quote character
}

/************************ Comment handling  ************************/

// Comment node operations
VeriCommentNode::VeriCommentNode(linefile_type starting_linefile) :
    VeriTreeNode(starting_linefile),
    _str(0)
{
    // Comment node initialized with 'starting' linefile rather than with 'ending' linefile.
}

VeriCommentNode::~VeriCommentNode()
{
    Strings::free(_str) ;
    _str=0 ;
}

// Appends string with the existing comment.
void VeriCommentNode::AppendComment(const char *str)
{
    if (!str) return ; // Nothing to append

    if (!_str) {
        // We don't have any coment, just copy the argument string
        _str = Strings::save(str) ;
    } else {
        // Get the current string:
        char *old = _str ;
        // Store the argument string with the existing string.
        _str = Strings::save(old, str) ;
        // Free the previous string
        Strings::free(old) ;
    }
    // FIXME: should we check for the newline character except at the end?
    // We need to remove them, so that it does not produce any illegal output.
}

// Return the comment string.
const char *VeriCommentNode::GetCommentString() const
{
    return _str ;
}

// Return the starting line number.
unsigned VeriCommentNode::GetStartingLineNumber() const
{
    linefile_type line_file = StartingLinefile() ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // If we are storing column info, return the left line:
    return line_file ? line_file->GetLeftLine() : 0 ;
#else
    // This returns the right line if we are storing columns!
    return LineFile::GetLineNo(line_file) ;
#endif
}

// Returns 1 if the comment is a block comment.
unsigned VeriCommentNode::IsBlockComment() const
{
    if (!_str) return 0 ;
    if ((_str[0] == '/') && (_str[1] == '*')) return 1 ;
    return 0 ;
}

void
VeriCommentNode::AdjustClosingLinefile(unsigned right_line, unsigned right_col)
{
    // Check that we can adjust the linefile correctly:
    if (!right_line || !right_col) return ;
    linefile_type com_lf = Linefile() ;
    if (!com_lf) return ; // No existing linefile info!

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Adjust only the right side of the linefile:
    com_lf->SetRightLine(right_line) ;
    com_lf->SetRightCol(right_col) ;
#endif
}

// Returns 1 if the comment is closed, 0 otherwise!
unsigned
VeriCommentNode::IsCommentClosed() const
{
    if (!_str) return 1 ; // No comment string yet, return 0?

    unsigned len = (unsigned)Strings::len(_str) ;
    if (_str[0] == '/') {
        if ((_str[1] == '/') && (_str[len-1] == '\n')) return 1 ; // short comment terminated
        if ((_str[1] == '*') && (_str[len-2] == '*') && (_str[len-1] == '/')) return 1 ; // block comment terminated
    }

    // Comment not yet finished or is an invalid comment
    return 0 ;
}

/************************ Comments Preservation ******************************************/
Map *
VeriNode::GetMapOfCommentsArray()
{
    return _map_of_comment_arr ;
}

void
VeriNode::FreeMapOfCommentsArray()
{
    if (!_map_of_comment_arr) return ;

    MapIter mi ;
    Array *comments ;
    FOREACH_MAP_ITEM(_map_of_comment_arr, mi, 0, &comments) {
        DeleteComments(comments) ;
    }

    delete _map_of_comment_arr ;
    _map_of_comment_arr = 0 ;
}

void
VeriNode::AddComments(Array *comments, unsigned pre) const
{
    if (!comments) return ;

    // Create the global static map if not there :
    if (!_map_of_comment_arr) _map_of_comment_arr = new Map(POINTER_HASH) ;

    // Attempt to insert comments directly :
    if (_map_of_comment_arr->Insert(this, (void *)comments)) {
        return ;
    }

    // Here, we failed to insert comments. This means there are existing 'comments' on this node.
    // Pick these up and append/prepend :
    Array *comment_array = GetComments() ;
    VERIFIC_ASSERT(comment_array) ; // should be there, or else Insert() would not have failed.

    // Append/Prepend it into the existing array, depending on the 'pre' argument :
    if (pre) {
        comment_array->Prepend(comments) ;
    } else {
        comment_array->Append(comments) ;
    }

    // Don't forget to delete the incoming array
    delete comments ;
    comments = 0 ; // JJ: assign to zero so callers can null check and aviod cores
}

Array *
VeriNode::GetComments() const
{
    if (!_map_of_comment_arr) return 0 ; // no comments anywhere
    return (Array *) _map_of_comment_arr->GetValue(this) ;
}

Array *
VeriNode::TakeComments() const
{
    Map *map_of_comment_arr = GetMapOfCommentsArray() ;
    if (!map_of_comment_arr) return 0 ; // compile flag not enabled

    Array *comments = (Array *) map_of_comment_arr->GetValue(this) ;
    if (!comments) return 0 ; // no comments on this node

    // If there were comments on this node, then remove the node from the global map.
    (void) map_of_comment_arr->Remove(this) ;

    // free the global map, if empty
    if (map_of_comment_arr->Size() == 0) FreeMapOfCommentsArray() ;

    return comments ;
}

// This is called from destructors of VeriModule, VeriModuleItem, VeriExpression
// and VeriStatement. This method deletes an array of comment nodes.
void
VeriNode::DeleteComments(Array *comments)
{
    if (!comments) return ;

    unsigned i ;
    VeriCommentNode *node ;
    FOREACH_ARRAY_ITEM(comments, i, node) delete node ;
    delete comments ;
}

/************************ Attribute Handling *********************************************/
Map *
VeriNode::GetAttributeMap()
{
    return _attribute_map ;
}

void
VeriNode::FreeAttributeMap()
{
    if (!_attribute_map) return ;

    MapIter mi ;
    Map *attributes ;
    FOREACH_MAP_ITEM(_attribute_map, mi, 0, &attributes) {
        MapIter mapIter ;
        char *key ;
        VeriExpression *expr ;
        // This map contains a char*->Expression mapping.
        FOREACH_MAP_ITEM(attributes, mapIter, &key, &expr) {
            Strings::free(key) ;
            delete expr ;
        }

        delete attributes ;
    }

    delete _attribute_map ;
    _attribute_map = 0 ;
}

void
VeriNode::SetAttributes(Map *attrs) const
{
    AddAttributes(attrs) ;
}

Map *
VeriNode::GetAttributes() const // get the map of attribute_name->VeriExpression for this this node
{
    if (!_attribute_map) return 0 ;
    return (Map *)_attribute_map->GetValue(this) ;
}

VeriExpression *
VeriNode::GetAttribute(const char *attr_name) const
{
    if (!_attribute_map) return 0 ;

    // Pick up attributes for this node :
    Map *attrs = GetAttributes() ;
    if (!attrs) return 0 ;

    return (VeriExpression*)attrs->GetValue(attr_name) ;
}

void
VeriNode::AddAttributes(Map *attrs) const // Absorb the Map (char*->VeriConst) of attribute tree nodes to the (attributed) expression.
{
    if (!attrs) return ;

    // Make sure we have a (static) attribute map
    if (!_attribute_map) _attribute_map = new Map(POINTER_HASH) ;

    if (_attribute_map->Insert(this, (void *)attrs)) {
        // Here, there was no prior Map of attributes for this node.
        // The 'attrs' Map went in, and we are done.
        return ;
    }

    // Could not insert attributes for this node. It must have contained attributes already.
    // Need to add attributes one by one. Get the existing map :
    Map *attributes = GetAttributes() ;
    VERIFIC_ASSERT(attributes) ; // should be there, or else Insert would not have failed.

    // Move all attr's out of the incoming Map, then delete it.
    char *key ;
    VeriExpression *val ;
    MapIter mi ;
    MapItem * mi_temp ;
    FOREACH_MAP_ITEM(attrs, mi, &key, &val) {
        // VIPER #2494: We don't want repeated attributes. Check to see if key already exists
        // If it does then we need to delete the current key and value before overwriting them.
        mi_temp = attributes->GetItem(key) ;
        if (mi_temp) {
            char *att_key = (char *)mi_temp->Key() ;
            VeriExpression *att_val = (VeriConst *)mi_temp->Value() ;
            (void) attributes->Remove(att_key) ;
            Strings::free(att_key) ; // free the key
            delete att_val ; // delete the value
        }
        (void) attributes->Insert(key, val, 1) ; // force overwrite
    }

    delete attrs ;
}

void
VeriNode::DeleteAttributes() const
{
    // Get the map of attributes for this node :
    Map *attrs = GetAttributes() ;
    if (!attrs) return ;

    // Since there were attributes, the _attribute_map must be there :
    VERIFIC_ASSERT(_attribute_map) ;

    // Free the attributes themselves :
    MapIter mapIter ;
    char *key ;
    VeriExpression *expr ;
    // This map contains a char*->Expression mapping.
    FOREACH_MAP_ITEM(attrs, mapIter, &key, &expr) {
        Strings::free(key) ;
        delete expr ;
    }
    // And the table they are in :
    delete attrs ;

    // And remove this entry from the static attributes map :
    (void) _attribute_map->Remove(this) ;

    // free the parent map if empty
    if (_attribute_map->Size() == 0) FreeAttributeMap() ;
}

Map *
VeriNode::TakeAttributes() const
{
    Map *attrs = GetAttributes() ; // Get the attribute table for this node
    if (!attrs) return 0 ; // there were no attributes for this node.

    (void) _attribute_map->Remove(this) ; // and remove the entry from _attribute_map.

    // free the parent map if empty
    if (_attribute_map->Size() == 0) FreeAttributeMap() ;

    // Return the table :
    return attrs ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Conversion of vhdl package to verilog one
VeriPackage *VeriNode::ConvertVhdlToVerilogPackage(const char *pkg_name, const char *pkg_lib)
{
    VeriPackage *package = 0 ;
    VhdlPrimaryUnit *vhdl_package = VeriNode::GetVhdlUnitForVerilogInstance(pkg_name, pkg_lib, 1, 0) ;
    VhdlIdDef *vhdl_pkg_id = vhdl_package ? vhdl_package->Id() : 0 ;
    if (vhdl_pkg_id && vhdl_pkg_id->IsPackage()) {
        if (vhdl_package) package = vhdl_package->ConvertPackage() ; // Add the converted package to vhdl_package's library
    }
    return package ;
}
#endif

void
VeriTreeNode::ResolveAttributes(VeriScope *scope) const
{
    Map *attributes = GetAttributes() ;

    // Need to resolve attribute expressions
    MapIter mi ;
    char *name ;
    VeriExpression *expr ;
    FOREACH_MAP_ITEM(attributes, mi, &name, &expr) {
        if (expr) expr->Resolve(scope, VeriTreeNode::VERI_CONSTANT_EXPRESSION) ; // VIPER 2536 : attribute expression should be constant
    }
}

/************************ Error reporting from a tree node  ******************************/

// New style messages : with varargs and message id :

void
VeriTreeNode::Comment(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_COMMENT, GetMessageId(format), Linefile(), format, args) ;
    va_end(args) ;
}

void
VeriTreeNode::Error(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    // Get msg_id for this message :
    const char *msg_id = GetMessageId(format) ;
    // Set the msg_type for this message :
    msg_type_t msg_type = VERIFIC_ERROR ;
    if (InRelaxedCheckingMode()) msg_type = GetRelaxedMessageType(msg_id,msg_type) ;
    // Push the message into the message handler :
    Message::MsgVaList(msg_type, msg_id, Linefile(), format, args) ;
    va_end(args) ;
}

//CARBON_BEGIN
void VeriTreeNode::Alert(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    // Get msg_id for this message :
    const char *msg_id = GetMessageId(format) ;
    // Set the msg_type for this message :
    msg_type_t msg_type = VERIFIC_ALERT ;
    if (InRelaxedCheckingMode()) msg_type = GetRelaxedMessageType(msg_id,msg_type) ;
    // Push the message into the message handler :
    Message::MsgVaList(msg_type, msg_id, Linefile(), format, args) ;
    va_end(args) ;
}
//CARBON_END

void
VeriTreeNode::Warning(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    // Get msg_id for this message :
    const char *msg_id = GetMessageId(format) ;
    // Set the msg_type for this message :
    msg_type_t msg_type = VERIFIC_WARNING ;
    if (InRelaxedCheckingMode()) msg_type = GetRelaxedMessageType(msg_id,msg_type) ;
    // Push the message into the message handler :
    Message::MsgVaList(msg_type, msg_id, Linefile(), format, args) ;
    va_end(args) ;
}

void
VeriTreeNode::Info(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_INFO, GetMessageId(format), Linefile(), format, args) ;
    va_end(args) ;
}

const char *
VeriNode::GetMessageId(const char *msg)
{
    if (!msg) return 0 ;
    if (!_msgs) {
        _msgs = new Map(STRING_HASH, 967) ; // Table size : choose prime number for good hashing. Enough initial storage until message VERI-1800.
//CARBON_BEGIN
        // put carbon specific messages here (at the top of the list to reduce merge conflicts), all carbon specifc messages must use IDs in the range 9000-9999
        _msgs->Insert("ignoring invalid define item \"%s\" from command line specification: %s","VERI-9000");  // 3/1/2014 added
//CARBON_END

        _msgs->Insert("all ports in concatenation should have the same direction", "VERI-1000") ;
        _msgs->Insert("assign statement not supported for synthesis", "VERI-1001") ;
        _msgs->Insert("cannot access memory %s directly", "VERI-1002") ;
// retired 4/13/05 : _msgs->Insert("cannot assign to a constant", "VERI-1003") ;
// retired 4/13/05 : _msgs->Insert("cannot assign to generate variable %s here", "VERI-1004") ;
// retired 4/13/05 : _msgs->Insert("cannot assign to indexed %s", "VERI-1005") ;
        _msgs->Insert("cannot assign to memory %s directly", "VERI-1006") ;
        _msgs->Insert("cannot assign to non-variable %s", "VERI-1007") ;
        _msgs->Insert("cannot assign to %s", "VERI-1008") ;
// retired 4/13/05 : _msgs->Insert("cannot assign to sliced %s", "VERI-1009") ;
        _msgs->Insert("cannot find port %s on this module", "VERI-1010") ;
        _msgs->Insert("cannot index into non-array %s", "VERI-1011") ;
        _msgs->Insert("cannot open file %s", "VERI-1012") ; // retired 4/13/05, revived 8/2007 for $readmemb support.
// retired 4/13/05 : _msgs->Insert("cannot part-select non-array %s", "VERI-1013") ;
        _msgs->Insert("cannot re-define predefined macro %s", "VERI-1014") ;
        _msgs->Insert("cannot set both range and type on function declaration", "VERI-1015") ;
        _msgs->Insert("cannot undefine predefined macro %s", "VERI-1016") ;
        _msgs->Insert("case condition never applies due to comparison with x or z", "VERI-1017") ;
        _msgs->Insert("compiling module %s", "VERI-1018") ;
        _msgs->Insert("deassign statement not supported for synthesis", "VERI-1019") ;
        _msgs->Insert("default case should appear only once", "VERI-1020") ;
// Supported  _msgs->Insert("disable statement ignored", "VERI-1021") ;
        _msgs->Insert("edge illegal in combinational table : %s", "VERI-1022") ;
        _msgs->Insert("`else without `if", "VERI-1023") ;
        _msgs->Insert("`elsif without `if", "VERI-1024") ;
        _msgs->Insert("empty table will create an empty box UDP", "VERI-1025") ;
        _msgs->Insert("`endif without `if", "VERI-1026") ;
// misprint   _msgs->Insert("Error: state stack now", "VERI-1027") ;
// retired 4/13/05 : _msgs->Insert("statement outside always construct is not supported for synthesis", "VERI-1028") ;
        _msgs->Insert("event trigger ignored", "VERI-1029") ;
        _msgs->Insert("every UDP port must be a scalar", "VERI-1030") ;
        _msgs->Insert("force statement not supported for synthesis", "VERI-1031") ;
// supported : _msgs->Insert("forever statement not supported", "VERI-1032") ; // VIPER issue 2140
// obsolete : _msgs->Insert("function %s is not declared", "VERI-1033") ;
        _msgs->Insert("generate case expression is not constant", "VERI-1034") ;
        _msgs->Insert("generate case item condition is not constant", "VERI-1035") ;
        _msgs->Insert("generate condition is not constant", "VERI-1036") ;
// retired 4/13/05 : _msgs->Insert("hierarchical name %s is not yet supported", "VERI-1037") ;
        _msgs->Insert("illegal character in binary number", "VERI-1038") ;
        _msgs->Insert("illegal character in decimal number", "VERI-1039") ;
        _msgs->Insert("illegal character in octal number", "VERI-1040") ;
        _msgs->Insert("illegal character %s in UDP table", "VERI-1041") ;
        _msgs->Insert("illegal expression in target", "VERI-1042") ;
        _msgs->Insert("illegal hexadecimal number", "VERI-1043") ;
        _msgs->Insert("illegal operation with real number", "VERI-1044") ;
        _msgs->Insert("illegal operator for real numbers", "VERI-1045") ;
        _msgs->Insert("illegal operator on integer number", "VERI-1046") ;
        _msgs->Insert("illegal operator on real number", "VERI-1047") ;
        _msgs->Insert("illegal port-expression", "VERI-1048") ;
        _msgs->Insert("/* in comment", "VERI-1049") ;
// retired 8/2/05  : _msgs->Insert("incorrect number of arguments into function call", "VERI-1050") ;
// retired 4/13/05 : _msgs->Insert("incorrect number of arguments into task enable", "VERI-1051") ;
        _msgs->Insert("incorrect number of inputs for table entry : %s", "VERI-1052") ;
        _msgs->Insert("incorrect number of macro arguments", "VERI-1053") ;
        _msgs->Insert("incorrect output field length found in table : %s", "VERI-1054") ;
        _msgs->Insert("incorrect use of predefined macro %s. Expected %s", "VERI-1055") ;
        _msgs->Insert("index is always out of bounds for array %s", "VERI-1056") ;
// replaced by 1216 : _msgs->Insert("index of %s is out of range for array range", "VERI-1057") ;
// replaced by 1216 : _msgs->Insert("index of %s out of address bounds", "VERI-1058") ;
// replaced by 1216 : _msgs->Insert("index of %s out of address bounds ; returning x", "VERI-1059") ;
        _msgs->Insert("'initial' construct ignored", "VERI-1060") ;
        _msgs->Insert("inout ports not allowed on a UDP", "VERI-1061") ;
// retired 4/13/05  : _msgs->Insert("(* inside attribute", "VERI-1062") ;
        _msgs->Insert("instantiating unknown module %s", "VERI-1063") ;
// replaced by 1216 : _msgs->Insert("left index of part-select of %s is out of address bounds", "VERI-1064") ;
// replaced by 1216 : _msgs->Insert("left index of part-select of %s is out of address bounds ; returning x", "VERI-1065") ;
        _msgs->Insert("loop count limit of %d exceeded; condition is never false", "VERI-1066") ;
        _msgs->Insert("missing arguments for macro %s", "VERI-1067") ;
        _msgs->Insert("missing compiler directive", "VERI-1068") ;
        _msgs->Insert("missing 'present state' field in sequential table : %s", "VERI-1069") ;
        _msgs->Insert("mixed blocking and non-blocking assignments on %s is not supported", "VERI-1070") ;
        _msgs->Insert("module does not expect any parameters", "VERI-1071") ;
        _msgs->Insert("module %s ignored due to previous errors", "VERI-1072") ;
        _msgs->Insert("module %s remains a blackbox, due to errors in its contents", "VERI-1073") ;
// supported 7/2005 : _msgs->Insert("multidimensional variable indexed arrays not supported", "VERI-1074") ;
        _msgs->Insert("name-based port connection not allowed in gate-instantiation", "VERI-1075") ;
        _msgs->Insert("near %s : illegal character in macro parameter", "VERI-1076") ;
        _msgs->Insert("near %s : illegal macro parameter", "VERI-1077") ;
        _msgs->Insert("need at least one output and one input to this gate", "VERI-1078") ;
        _msgs->Insert("need %d terminals to this gate", "VERI-1079") ;
        _msgs->Insert("non-constant loop condition not supported for %s", "VERI-1080") ;
// retired 4/13/05 : _msgs->Insert("non-constant %s is not IEEE standard", "VERI-1081") ;
// retired 9/28/05 : _msgs->Insert("no support for array of instantiations", "VERI-1082") ;
        _msgs->Insert("no support for instantiation of gate %s", "VERI-1083") ;
        _msgs->Insert("no support for synthesis of mixed edge and level triggers. Assume level triggers only.", "VERI-1084") ;
// obsolete : _msgs->Insert("no support for multi-dimensional array indexing yet", "VERI-1085") ;
// obsolete : _msgs->Insert("no support for multi-dimensional array part-select yet", "VERI-1086") ;
        _msgs->Insert("no support for multiple terminals on pullup/down sources", "VERI-1087") ;
        _msgs->Insert("no support for non-constant operation with this operator", "VERI-1088") ;
        _msgs->Insert("no support for real number in ?: operator", "VERI-1089") ;
        _msgs->Insert("not supported use of real number", "VERI-1090") ;
        _msgs->Insert("output or inout not allowed in functions", "VERI-1091") ;
        _msgs->Insert("parallel (fork-join) block ignored", "VERI-1092") ;
        _msgs->Insert("parameter %s is not defined in this module", "VERI-1093") ;
        _msgs->Insert("parameter %s depends on uninitialized variable", "VERI-1094") ;
        _msgs->Insert("part-select has a negative or zero size", "VERI-1095") ;
        _msgs->Insert("part-select of memory %s is not allowed", "VERI-1096") ;
// retired 4/13/05 : _msgs->Insert("port %s is already declared", "VERI-1097") ;
        _msgs->Insert("port %s is already defined", "VERI-1098") ;
        _msgs->Insert("present state ':' output field separator misplaced : %s", "VERI-1099") ;
        _msgs->Insert("procedural assignment to a non-register %s is not permitted", "VERI-1100") ;
        _msgs->Insert("range index cannot be a real number", "VERI-1101") ;
        _msgs->Insert("range index cannot contain 'x' or 'z'", "VERI-1102") ;
        _msgs->Insert("range index value is not constant", "VERI-1103") ;
        _msgs->Insert("release statement not supported for synthesis", "VERI-1104") ;
        _msgs->Insert("repetition multiplier contains x", "VERI-1105") ;
        _msgs->Insert("repetition multiplier is not constant", "VERI-1106") ;
        _msgs->Insert("repetition multiplier must be positive", "VERI-1107") ;
        _msgs->Insert("replacing existing cell %s", "VERI-1108") ;
        _msgs->Insert("replacing unknown cell %s with a valid definition", "VERI-1109") ;
// replaced by 1216        _msgs->Insert("right index of part-select of %s is out of address bounds", "VERI-1110") ;
// replaced by 1216        _msgs->Insert("right index of part-select of %s is out of address bounds ; returning x", "VERI-1111") ;
        _msgs->Insert("%s cannot be used in expression", "VERI-1112") ;
        _msgs->Insert("sequential table entry found in combinational table : %s", "VERI-1113") ;
// replaced by 1131       _msgs->Insert("%s has %d dimensions", "VERI-1114") ;
        _msgs->Insert("%s in port-expression is not a port", "VERI-1115") ;
        _msgs->Insert("%s is already declared", "VERI-1116") ;
// obsolete : VERI-1116 replaces this       _msgs->Insert("%s is already declared as net", "VERI-1117") ;
// obsolete : VERI-1116 replaces this       _msgs->Insert("%s is already declared as reg", "VERI-1118") ;
        _msgs->Insert("%s is already defined", "VERI-1119") ;
        _msgs->Insert("%s is not a function", "VERI-1120") ;
        _msgs->Insert("%s is not a genvar", "VERI-1121") ;
        _msgs->Insert("%s is not a memory", "VERI-1122") ;
        _msgs->Insert("%s is not an array of vectors", "VERI-1123") ;
        _msgs->Insert("%s is not an event", "VERI-1124") ;
        _msgs->Insert("no definition for port %s", "VERI-1125") ; // VIPER #8172: Improve error message
        _msgs->Insert("%s is not a task", "VERI-1126") ;
        _msgs->Insert("%s is not a task or block", "VERI-1127") ;
        _msgs->Insert("%s is not declared", "VERI-1128") ;
// obsolete : _msgs->Insert("%s is not synthesizable since it does not hold its value under NOT(clock-edge) condition", "VERI-1129") ;
        _msgs->Insert("%s is not valid in an expression", "VERI-1130") ;
        _msgs->Insert("%s needs %d dimensions", "VERI-1131") ;
// obsolete : is allowed in LRM        _msgs->Insert("%s was previously declared as an array port", "VERI-1132") ;
// obsolete : VERI-1203/1204 replaces       _msgs->Insert("port %s is not a vector ; range ignored", "VERI-1133") ;
// obsolete : VERI-1116 replaces this       _msgs->Insert("%s was previously declared as net", "VERI-1134") ;
// obsolete : VERI-1116 replaces this       _msgs->Insert("%s was previously declared as reg", "VERI-1135") ;
        _msgs->Insert("%s was previously declared with a different range", "VERI-1136") ;
        _msgs->Insert("syntax error near %s", "VERI-1137") ;
        _msgs->Insert("unexpected EOF", "VERI-1138") ;
        _msgs->Insert("synthesis of 'real' values is not supported", "VERI-1139") ;
        _msgs->Insert("system call %s expects one argument", "VERI-1140") ;
        _msgs->Insert("system function call %s not supported", "VERI-1141") ;
        _msgs->Insert("system task %s ignored for synthesis", "VERI-1142") ;
        _msgs->Insert("system timing check ignored", "VERI-1143") ;
        _msgs->Insert("table entry without ':' IO separator : %s", "VERI-1144") ;
        _msgs->Insert("task call outside of sequential construct is not allowed", "VERI-1145") ;
// replaced by 1128  _msgs->Insert("task %s is not declared", "VERI-1146") ;
// retired 4/13/05 : _msgs->Insert("this module item ignored", "VERI-1147") ;
        _msgs->Insert("too many actuals in instantiation", "VERI-1148") ;
// retired with VIPER #7574 :  _msgs->Insert("too many macro arguments", "VERI-1149") ;
        _msgs->Insert("too many parameters for module instance %s", "VERI-1150") ;
        _msgs->Insert("UDP output must be the first port", "VERI-1151") ;
        _msgs->Insert("UDP has no output", "VERI-1152") ;
        _msgs->Insert("unconnected terminal to this gate", "VERI-1153") ;
        _msgs->Insert("unexpected EOF : missing `endif", "VERI-1154") ;
// retired 4/13/05 : _msgs->Insert("use array (memory) notation for vector of multibit values", "VERI-1155") ;
// retired 4/13/05 : _msgs->Insert("use multidimensional array notation for vectors of multibit values", "VERI-1156") ;
        _msgs->Insert("use of events is not supported; event ignored", "VERI-1157") ;
        _msgs->Insert("use of undefined macro %s", "VERI-1158") ;
        _msgs->Insert("value for parameter %s is not constant", "VERI-1159") ;
        _msgs->Insert("wait statement ignored", "VERI-1160") ;
// Added after first run :
// obsolete (implement transistors now)       _msgs->Insert("gate %s implemented as a wire", "VERI-1161") ;
        _msgs->Insert("port connections cannot be mixed ordered and named", "VERI-1162") ; // re-instated after VIPER 2358
        _msgs->Insert("unknown nettype %s ignored", "VERI-1163") ;
        _msgs->Insert("unknown unconnected_drive %s ignored", "VERI-1164") ;
        _msgs->Insert("part-select direction is opposite from prefix index direction", "VERI-1165") ;
        _msgs->Insert("case condition never applies", "VERI-1166") ;
        _msgs->Insert("if-condition does not match any sensitivity list edge", "VERI-1167") ;
        _msgs->Insert("trailing ordered association ignored in unknown module instantiation", "VERI-1168") ;
        _msgs->Insert("vector size is larger than 2**%d bits", "VERI-1169") ;
        _msgs->Insert("memory size is larger than 2**%d bits", "VERI-1170") ;
        _msgs->Insert("port %s is not defined", "VERI-1171") ;
        _msgs->Insert("external reference %s remains unresolved", "VERI-1172") ; // changed from "external reference %s not supported for synthesis" now that it is supported.
        _msgs->Insert("full_case directive is effective : might cause synthesis - simulation differences", "VERI-1173") ;
        _msgs->Insert("parallel_case directive is effective : might cause synthesis - simulation differences", "VERI-1174") ;

// 10/2002 added
// 1/2005 : obsolete        _msgs->Insert("recursive function calls are currently not supported", "VERI-1175") ;
// 1/2005 : obsolete        _msgs->Insert("recursive task calls are currently not supported", "VERI-1176") ;
        _msgs->Insert("see declaration of %s", "VERI-1177") ;
        _msgs->Insert("indexing (of %s) is not allowed in a constant expression", "VERI-1178") ;
// Retired 8/2004. No longer true.        _msgs->Insert("empty argument is not IEEE standard", "VERI-1179") ;
        _msgs->Insert("constant is not allowed here", "VERI-1180") ;
        _msgs->Insert("function call %s is not allowed here", "VERI-1181") ;
        _msgs->Insert("%s expects %d arguments", "VERI-1182") ;
        _msgs->Insert("system function call %s is not allowed here", "VERI-1183") ;
        _msgs->Insert("module %s does not have a parameter named %s", "VERI-1184") ;
        _msgs->Insert("module %s does not have %d parameters", "VERI-1185") ;
        _msgs->Insert("unknown literal value %s for parameter %s ignored", "VERI-1186") ;
        _msgs->Insert("%s is not declared under prefix %s", "VERI-1187") ;
        _msgs->Insert("%s is not a constant", "VERI-1188") ;
        _msgs->Insert("*/ outside comment", "VERI-1189") ;
        _msgs->Insert("attempt to divide by 0; returning x", "VERI-1190") ;

// 11/2002 added
// covered by 1091        _msgs->Insert("a function must not have an output or inout type argument", "VERI-1191") ;
        _msgs->Insert("a function must have at least one input", "VERI-1192") ;

// 12/2002 added
        _msgs->Insert("variable %s is too small to store all FSM states. FSM not extracted", "VERI-1193") ;

// 1/2003 added
        _msgs->Insert("edge descriptors are invalid here", "VERI-1194") ;
        _msgs->Insert("concurrent assignment to a non-net %s is not permitted", "VERI-1195") ;
        _msgs->Insert("%s %s cannot be overwritten", "VERI-1196") ;

// 2/2003 added
        _msgs->Insert("port declaration not allowed in %s with formal port declaration list", "VERI-1197") ;
        _msgs->Insert("function/task declaration not allowed inside generate loop", "VERI-1198") ;
        _msgs->Insert("parameter declaration becomes local in %s with formal parameter declaration list", "VERI-1199") ;
        _msgs->Insert("'ifdef/'ifndef block exceeds nesting limit of %d", "VERI-1200") ;
        _msgs->Insert("unmatched %s translate/synthesis off pragma found; matching pair with same keywords is required", "VERI-1201") ; // message changed after VIPER 3312/3314
        _msgs->Insert("block comment was not closed", "VERI-1202") ;

// 3/2003 added
        // (resurrected scalar-array port check, as warnings) :
        _msgs->Insert("%s was previously declared without a range", "VERI-1203") ;
        _msgs->Insert("%s was previously declared with a range", "VERI-1204") ;

        _msgs->Insert("previous syntax error may be due to incorrect attribute placement", "VERI-1205") ;
        _msgs->Insert("overwriting previous definition of module %s", "VERI-1206") ;

// 4/2003 added
        _msgs->Insert("instantiating unknown empty module %s", "VERI-1207") ;
        _msgs->Insert("literal value truncated to fit in %d bits", "VERI-1208") ;
        _msgs->Insert("expression size %d truncated to fit in target size %d", "VERI-1209") ;

// 5/2003 added
        _msgs->Insert("actual bit length %d differs from formal bit length %d", "VERI-1210") ; // VIPER #6991: For Warning
        _msgs->Insert("value for attribute %s is not constant", "VERI-1211") ;
        _msgs->Insert("block identifier is required on this block", "VERI-1212") ; // message was without id
        _msgs->Insert("variable %s is too small to store FSM state %d. FSM not extracted", "VERI-1213") ;
        _msgs->Insert("assignment to input %s","VERI-1214") ;
        _msgs->Insert("assignment to constant %s","VERI-1215") ;
        _msgs->Insert("index %d is out of range [%d:%d] for %s", "VERI-1216") ;

// 6/2003 added
// Retired 8/2004. No longer true.        _msgs->Insert("system task enable with empty argument list between parentheses is not IEEE standard", "VERI-1217") ;

// 8/2003 added
        _msgs->Insert("defparam under generate scope cannot change parameter value outside of hierarchy", "VERI-1218") ;
        _msgs->Insert("value of %s depends on itself", "VERI-1219") ;
        _msgs->Insert("using initial value of %s since it is never assigned", "VERI-1220") ;

// 10/2003 added
        _msgs->Insert("%s should be on the sensitivity list", "VERI-1221") ;
        _msgs->Insert("edge of vector %s is only sensitive to the LSB", "VERI-1222") ;
        _msgs->Insert("constant expression cannot contain a hierarchical identifier", "VERI-1223") ; // LRM A.8.4
        _msgs->Insert("functions cannot contain non-blocking assignments", "VERI-1224") ; // LRM 10.3.4
        _msgs->Insert("functions cannot enable tasks", "VERI-1225") ; // LRM 10.3.4
        _msgs->Insert("functions cannot contain time-controlled statements", "VERI-1226") ; // LRM 10.3.4

// 1/2004 added
        _msgs->Insert("module %s is not yet analyzed", "VERI-1227") ;

// 3/2004 added
        _msgs->Insert("macro %s causes infinite loop", "VERI-1228") ;
        _msgs->Insert("module instantiation should have an instance name", "VERI-1229") ; // issue 1755.

// 4/2004 added
// replaced by 1055        _msgs->Insert("incorrect `line directive syntax", "VERI-1230") ; // issue 1786.
        _msgs->Insert("going to vhdl side to elaborate module %s", "VERI-1231") ;
        _msgs->Insert("back to verilog to continue elaboration", "VERI-1232") ;
        _msgs->Insert("re-analyze unit %s since unit %s is overwritten or removed", "VERI-1233") ;
// retired 4/13/05 : _msgs->Insert("library %s is empty", "VERI-1234") ;
// retired 8/02/06 : _msgs->Insert("circular dependency found in defparam", "VERI-1235") ;
        _msgs->Insert("illegal recursive design instantiation, instance name %s", "VERI-1236") ;
// obsolete : _msgs->Insert("hierarchical reference of element of automatic function or task %s", "VERI-1237") ;
        _msgs->Insert("illegal defparam %s in module %s", "VERI-1238") ;
// retired 4/10/06 : _msgs->Insert("unresolved identifier %s in design %s", "VERI-1239") ;
        _msgs->Insert("constant expression required", "VERI-1240") ;
        _msgs->Insert("non-static generate-for loop. Unroll failed", "VERI-1241") ;
        _msgs->Insert("premature EOF found in %s section", "VERI-1242") ;
        _msgs->Insert("operator %s is only allowed in a %s", "VERI-1243") ;
        _msgs->Insert("stack overflow on recursion via %s, limit of %d has exceeded", "VERI-1244") ;

// 5/2004 added
        _msgs->Insert("cannot open include file %s", "VERI-1245") ; // VIPER #1816

// 8/2004 added
        _msgs->Insert("%s does not accept dimensions", "VERI-1246") ;
        _msgs->Insert("%s does not accept an initial value", "VERI-1247") ;

// 9/2004 added (mostly System Verilog related messages) :
        _msgs->Insert("signing ignored on type %s", "VERI-1248") ;
// retired 4/14/06 : _msgs->Insert("range ignored on type %s", "VERI-1249") ;
        _msgs->Insert("%s statement is illegal here", "VERI-1250") ;
        _msgs->Insert("absolute path name not allowed for <> include files", "VERI-1251") ;
        _msgs->Insert("mismatch in closing label %s; expected %s", "VERI-1252") ;
        _msgs->Insert("illegal duplicate name created during elaboration, multiple declarations created for '%s'", "VERI-1253") ; // VIPER #1702 and #1923
// retired 4/13/05 : _msgs->Insert("range [%s] not yet supported for synthesis", "VERI-1254") ;
        _msgs->Insert("operator %s not yet supported for synthesis", "VERI-1255") ;

// 10/2004 added (mostly System Verilog related messages) :
        _msgs->Insert("cannot have packed dimensions of unpacked type %s", "VERI-1256") ;

// 11/2004 added
        _msgs->Insert("the actual is illegally connected to inout/output port of module '%s', instance '%s' ", "VERI-1257") ;
        _msgs->Insert("the actual is illegally connected to inout/output port of gate '%s'", "VERI-1258") ;
        _msgs->Insert("X or Z in value for 2-state enum %s is not allowed", "VERI-1259") ;
        _msgs->Insert("cannot create implicit enum value for %s since previous value contains X or Z", "VERI-1260") ;
        _msgs->Insert("illegal recursive module instantiation of %s", "VERI-1261") ; // VIPER #1954
        _msgs->Insert("%s is not a parameter", "VERI-1262") ;
        _msgs->Insert("size mismatch in enum definition for literal '%s'", "VERI-1263") ;

// 12/2004 added :
        _msgs->Insert("%s has no port called %s", "VERI-1264") ;                    // SV named port connections
        _msgs->Insert("port %s is already connected", "VERI-1265") ;                // SV named port connections
        _msgs->Insert("port %s is not connected on %s call %s", "VERI-1266") ;      // (SV) unconnected ports. Message changed after Issue 2542.
        _msgs->Insert("argument of %s should be an identifier", "VERI-1267") ;      // system calls
        _msgs->Insert("dimension for %s is out of bounds", "VERI-1268") ;           // dimension numbers (in system calls)
        _msgs->Insert("%s is not supported for static elaboration", "VERI-1269") ;  // Static elab only
        _msgs->Insert("concatenation member label not yet supported; label ignored", "VERI-1270") ; // temporary
        _msgs->Insert("cannot operate on uninitialized genvar", "VERI-1271") ;     // temporary

// 1/2005 added
        _msgs->Insert("statement outside sequential area is not supported for synthesis", "VERI-1272") ;
        _msgs->Insert("hierarchical name %s into function or task is not supported for synthesis", "VERI-1273") ;

// 2/2005 added
        _msgs->Insert("formal port %s expects a modport or interface instance actual", "VERI-1274") ; // SV modports/interface assocs
        _msgs->Insert("formal port %s cannot connect to a modport or interface instance actual", "VERI-1275") ; // SV modports/interface assocs
        _msgs->Insert("formal port %s of type %s does not match with actual type %s", "VERI-1276") ; // SV modports/interface assocs
        _msgs->Insert("expression not allowed on %s statement for %s", "VERI-1277") ; // SV jump statements
        _msgs->Insert("%s statement for %s expects an expression", "VERI-1278") ; // SV jump statements
        _msgs->Insert("multiple event control statements not supported for synthesis", "VERI-1279") ; // nested or multiple @(something) stmts.
        _msgs->Insert("external disable constructs not supported for synthesis", "VERI-1280") ; // disable statement disabling code outside its own sequential area

// 4/2005 added :
        _msgs->Insert("%s is not a type", "VERI-1281") ; // System Verilog type expression
        _msgs->Insert("loop count limit of %d exceeded; forever never breaks", "VERI-1282") ; // Static elaboration
        _msgs->Insert("external reference %s not supported for static elaboration", "VERI-1283") ;
        _msgs->Insert("value %G is out of range", "VERI-1284") ; // Verilog AMS
        _msgs->Insert("expression cannot be negative", "VERI-1285") ; // SV

// 5/2005 added :
        _msgs->Insert("unknown qualifier %s in `default_discipline %s ignored", "VERI-1286") ; // Verilog AMS
        _msgs->Insert("%s attribute of nature is already declared", "VERI-1287") ; // Verilog AMS
        _msgs->Insert("module %s is not a %s unit", "VERI-1288") ; // PSL
        _msgs->Insert("invalid context for genvar %s", "VERI-1289") ; // Static Elaboration VIPER #2074
        // The next four messages are added as an effect of code-review
        _msgs->Insert("incompatible number of unpacked dimensions in instantiation", "VERI-1290") ; // Static Elab: array instantiation
        _msgs->Insert("size of the corresponding unpacked dimensions do not match", "VERI-1291") ; // Static Elab: array instantiation
        // VERI-1292 retired for VIPER #3265 - we don't really need this check:
        //_msgs->Insert("packed size mismatch between formal and actual", "VERI-1292") ; // Static Elab: array instantiation
        _msgs->Insert("unpacked dimension mismatch between formal and actual", "VERI-1293") ; // Static Elab: array instantiation

// 6/2005 added :
        _msgs->Insert("empty port in module declaration", "VERI-1294") ; // VIPER #2092
        _msgs->Insert("macro %s redefined", "VERI-1295") ; // VIPER #2091
        _msgs->Insert("ignoring parameter override for %s, since it has already been set", "VERI-1296") ;
        _msgs->Insert("cycle range must be ascending", "VERI-1297") ; // SVA
        _msgs->Insert("clock for system call '%s' missing in this context", "VERI-1298") ; // SVA

// 7/2005 added :
        _msgs->Insert("system call %s not allowed in this dialect. Use System Verilog mode", "VERI-1299") ; // SV
        // message ID 1300 deliberately left open, for custom-message.
        _msgs->Insert("prefix of method %s should be %s", "VERI-1301") ; // SV
        _msgs->Insert("method %s not supported for synthesis", "VERI-1302") ; // SV
        _msgs->Insert("return value of subprogram call %s is ignored", "VERI-1303") ; // function called as task.
        _msgs->Insert("%s may be used uninitialized in static subprogram %s and create unintended latch behavior", "VERI-1304") ; // latches from subprograms. Issue 1394.
        //_msgs->Insert("defparam contains array instance reference, ignoring defparam", "VERI-1305") ; // VIPER #2128 : obsolete as defparam with array instance ref is supported

// 8/2005 added :
        _msgs->Insert("hierarchical branch index should be a constant", "VERI-1306") ; // hierarchical names
// 08/2013 : obsolate _msgs->Insert("no support for actual union/struct/enum types yet", "VERI-1307") ; // SV parameter type passing
// 9/2005 : obsolete _msgs->Insert("no support for actual types with dimensions yet", "VERI-1308") ;  // SV parameter type passing
        _msgs->Insert("actual for type parameter %s should be a data type", "VERI-1309") ; // SV parameter type passing
        _msgs->Insert("%s is declared here", "VERI-1310") ; // general info message
        _msgs->Insert("operator %s is not allowed in %s", "VERI-1311") ; // SVA operator context resolving
        _msgs->Insert("invalid use of void function %s", "VERI-1312") ; // void function usage
        _msgs->Insert("illegal concurrent assertion in task/function", "VERI-1313") ; // concurrent assertion usage
        _msgs->Insert("automatic variable not allowed in module", "VERI-1314") ; // automatic variable declaration

// 9/2005 added :
        _msgs->Insert("property instance %s is not allowed in sequence expression", "VERI-1315") ; // automatic variable declaration

// 11/2005 added :
        _msgs->Insert("concurrent assertion statement not allowed inside a looping statement", "VERI-1316") ; // VIPER #2242. System Verilog
        _msgs->Insert("assignment operator %s not allowed outside procedural statements", "VERI-1317") ; // System Verilog
        _msgs->Insert("variable %s might have multiple concurrent drivers", "VERI-1318") ; // System Verilog
        _msgs->Insert("duplicate member values in enum definition for enum literal %s", "VERI-1319") ; // enum literal value check

// 12/2005 added :
        _msgs->Insert("concatenation with unsized literal; will interpret as 32 bits", "VERI-1320") ; // VIPER 2290
        _msgs->Insert("event expression is not allowed here", "VERI-1321") ; // VIPER 2180

// 1/2006 added :
        _msgs->Insert("prefix of assignment pattern must be a data type", "VERI-1322") ; // target type of assignment expression

// 2/2006 added :
        _msgs->Insert("instance '%s' already declared in the bind target scope", "VERI-1323") ; // illegal bind instantiation addition

// 3/2006 added :
        _msgs->Insert("illegal circular dependency found through %s", "VERI-1324") ; // recursive structure, union or typedef definition
        _msgs->Insert("non-net port %s cannot be of mode %s", "VERI-1325") ; // error out on variable output/inout ports..
        _msgs->Insert("%s is not a subprogram", "VERI-1326") ; // prefix of subprogram should be subprogram.
        _msgs->Insert("this feature is not yet supported", "VERI-1327") ; // for (systen verilog) features not yet supported,

// 4/2006 added :
        _msgs->Insert("analyzing included file %s", "VERI-1328") ;
        _msgs->Insert("second declaration of %s ignored", "VERI-1329") ;
        _msgs->Insert("actual bit length %d differs from formal bit length %d for port %s", "VERI-1330") ; // (VIPER #2340); VIPER #6991: For Warning

// 5/2006 added :
        _msgs->Insert("%s is not declared within package %s", "VERI-1331") ; // Import item
        _msgs->Insert("%s is not an instance of %s", "VERI-1332") ;
        _msgs->Insert("%s is not yet supported for synthesis", "VERI-1333") ; // VIPER 2474: SV constructs : synthesis not yet implemented
        _msgs->Insert("illegal left hand side of with clause", "VERI-1334") ; // Array method with 'with expression'
        _msgs->Insert("%s in always_comb/always_latch is not allowed", "VERI-1335") ; // VIPER 2447 : event/timing control in always_comb/always_latch
        _msgs->Insert("unpacked member %s is not allowed in packed struct/union", "VERI-1336") ; // VIPER 2473 : check for packed qualifier
        _msgs->Insert("illegal select on unpacked structure/union type variable %s", "VERI-1337") ; // VIPER 2473 : select on unpacked struct/union

// 6/2006 added :
        _msgs->Insert("range expressions not allowed in hierarchical pathnames", "VERI-1338") ; // VIPER 2473 : select on unpacked struct/union

// 7/2006 added :
        _msgs->Insert("too many delays in gate instantiation", "VERI-1339") ; // VIPER 2541 : validate delay counts
        _msgs->Insert("illegal context for real expression", "VERI-1340") ; // VIPER 2539 : cycle delay cannot be real
// 09/2006 : obsolete        _msgs->Insert("illegal expression in assignment pattern item label", "VERI-1341") ; // VIPER 2554 : only id-ref/type-ref/constants are allowed in assignment pattern label
        _msgs->Insert("self-reference to %s ignored", "VERI-1342") ; // VIPER 2583

// 8/2006 added :
        _msgs->Insert("zero or negative value for size", "VERI-1343") ; // VIPER 2600 : size for unpacked arrays should be positive
        _msgs->Insert("enumeration range must be a non-negative integer value", "VERI-1344") ; // VIPER 2623 : for enumeration range like 'enum { a[2.5:-4] }'
        _msgs->Insert("alias bit length %d differs from actual bit length %d", "VERI-1345") ; // VIPER 2474 : alias sizing mismatch.

// 9/2006 added :
        _msgs->Insert("only [expr1:expr2] syntax is allowed for packed ranges of types", "VERI-1346") ; // VIPER 2427 : support dimension in actual for type param

// 10/2006 added :
        _msgs->Insert("concatenation member label not allowed in multiple assignment pattern", "VERI-1347") ; // type checking
        _msgs->Insert("an enum variable may only be assigned to same enum typed variable or one of its values", "VERI-1348") ; // VIPER #2249 : type checking for enumerated types
        _msgs->Insert("cannot assign %s type to %s type", "VERI-1349") ; // packed/unpacked mismatch in assignment
        _msgs->Insert("incompatible unpacked dimensions in assignment", "VERI-1350") ; // number of unpacked dimensions mismatch in assignment
        _msgs->Insert("illegal concat label for %s", "VERI-1351") ; // validation of concat label
        _msgs->Insert("too many indices into %s", "VERI-1352") ; // validation of indexed memory id
        _msgs->Insert("invalid target for field:value initialization", "VERI-1353") ; // VIPER #2619 : validation of concat label in concatenation
// retired 11/16/06 :  _msgs->Insert("illegal context for default: value", "VERI-1354") ; // validation of default : value

// 12/2006 added :
        _msgs->Insert("ignoring -incdir options for this library declaration", "VERI-1355") ; // Don't really understand "-incdir" ignoring it
        _msgs->Insert("cannot format %s %s to standard absolute path form", "VERI-1356") ; // Error formatting path or pattern for matching
        _msgs->Insert("comparing file %s to library pattern %s: did not match", "VERI-1357") ; // Matching file with library pattern: did not match
        _msgs->Insert("comparing file %s to library pattern %s: matched library %s", "VERI-1358") ; // Matching file with library pattern: matched with a library
        _msgs->Insert("file %s matched multiple library patterns", "VERI-1359") ; // File matched with multiple library pattern
        _msgs->Insert("polarity not allowed before %s in edge-sensitive paths", "VERI-1360") ; // VIPER 2746
        _msgs->Insert("operator overloading is not supported yet", "VERI-1361") ; // VIPER #2733 : support for operator overloading

// 1/2007 added :
        _msgs->Insert("%s is already implicitly declared on line %d", "VERI-1362") ; // VIPER 2775 (partly), VIPER #8172: Improve error message
        _msgs->Insert("range is not allowed in a prefix", "VERI-1363") ; // code cleanup..
        _msgs->Insert("sequence match items ignored for synthesis", "VERI-1364") ; // VIPER 2240
// duplicate of 1361 : _msgs->Insert("operator overloading is not supported yet", "VERI-1365") ; // VIPER #2733 : support for operator overloading
        _msgs->Insert("multiple configuration default clauses are not allowed", "VERI-1366") ; // VIPER #2739: Multiple default library list specified
        _msgs->Insert("configuration with multiple designs cannot be instantiated", "VERI-1367") ; // VIPER #2739: Configuration with multiple designs cannot be instantiated
        _msgs->Insert("configuration cell clause cannot contain a cell library and a liblist", "VERI-1368") ; // VIPER #2739: Cell clause cannot caontain both library name and liblist

// 2/2007 added :
        _msgs->Insert("illegal recursive design specified through configuration %s", "VERI-1369") ; // VIPER #2739: Configuration specified instance not found in design
        _msgs->Insert("array element widths (%d, %d) don't match", "VERI-1370") ; // VIPER #2838 : bit size of base type mismatch in assignment like context

// 3/2007 added :
        _msgs->Insert("inconsistent dimension in declaration", "VERI-1371") ; // VIPER #2899 : unpacked dimension mismatch in multiple declarations
        _msgs->Insert("redeclaration of ansi port %s is not allowed", "VERI-1372") ; // VIPER #2890 : ansi port re-declared
        _msgs->Insert("%s not allowed in this dialect. Use v2k mode", "VERI-1373") ; // VIPER #2766 : error out on 95 style features

// 4/2007 added :
        _msgs->Insert("incompatible complex type assignment", "VERI-1374") ; // VIPER #2960 : incompatible type assigned to one another (chandle in this issue)
        _msgs->Insert("illegal format specifier %c for %s", "VERI-1375") ; // VIPER #2790 : format specifier is illegal
        _msgs->Insert("missing or empty argument against format specification for %s", "VERI-1376") ; // VIPER #2790 : number of expected arguments is more than what specified
        _msgs->Insert("bit-select or part-select is not allowed in a %s statement for non-net %s", "VERI-1377") ; // VIPER 2948 : LHS force, Viper 4682, LHS procedural continuous assign
        _msgs->Insert("assignment to const variable %s", "VERI-1378") ; // VIPER 2959 : SV const variable assignment
        _msgs->Insert("%s was previously declared as type", "VERI-1379") ; // VIPER #2994 : An identifier was previously declared as type but now used as variable
        _msgs->Insert("declarations not allowed in unnamed block", "VERI-1380") ; // VIPER #3026 : declarations in unnamed block
        _msgs->Insert("cannot select %s inside %s due to dimension mismatch", "VERI-1381") ; // VIPER 2980: Illegal selection/indexing
        _msgs->Insert("illegal initial value of %s port %s for %s %s ignored", "VERI-1382") ; // VIPER #2949 : Semantic check on initial value for module/function/task
        _msgs->Insert("indexed range expression contains whitespace between %s and :", "VERI-1383") ; // VIPER #3019: Allow indexed select range with white spaces with this warning message

// 05/2007 added :
        _msgs->Insert("illegal context for %s", "VERI-1384") ; // VIPER #3054 : Specific error for illegal context of assignment pattern/tagged union
        _msgs->Insert("%s not allowed in this dialect. Use system verilog mode", "VERI-1385") ; // VIPER #3056 : error out on sv style features
        _msgs->Insert("number of loop variables in the FOREACH loop are more than the array dimensions", "VERI-1386") ; // VIPER #3083 : error out if the number of loop variables is greater than the array dimensions
        _msgs->Insert("UDP primitive %s does not have a table", "VERI-1387") ; // VIPER 3028
        _msgs->Insert("illegal use of `uselib directive : cannot mix lib with file|dir|libext", "VERI-1388") ; // `uselib lib support
        _msgs->Insert("cannot index into unpacked base of %s", "VERI-1389") ; // VIPER #3062 : split term reached to an unpacked base type
        _msgs->Insert("%s is visible via multiple package imports", "VERI-1390") ; // VIPER #3135
        _msgs->Insert("clocking block signal %s can be driven only with a non-blocking assignment", "VERI-1391") ; // VIPER #3191

// 06/2007 added:
        _msgs->Insert("%s was forward declared as %s type not as %s", "VERI-1392") ; // VIPER #3228: Forward and actual declaration of type do not match
        _msgs->Insert("cannot have packed dimensions of type %s", "VERI-1393") ; // VIPER #3202
        _msgs->Insert("forward typedef %s was not defined in the scope where it is declared", "VERI-1394") ; // VIPER #3031: Forward declaration was not actually defined
        _msgs->Insert("incorrect number of port association in instantiation of vhdl entity %s", "VERI-1395") ; // VIPER #3248 : Check for mixed language port consistency
        _msgs->Insert("binding vhdl entity %s does not have port %s", "VERI-1396") ; // VIPER #3248 : Check for mixed language port consistency
        _msgs->Insert("size mismatch in mixed language port association, vhdl port %s", "VERI-1397") ; // VIPER #3248 : Check for mixed language port consistency
        _msgs->Insert("illegal reference to automatic variable %s", "VERI-1398") ; // VIPER #3236 : Check for automatic variable reference
        _msgs->Insert("automatic variable %s cannot be written in this context", "VERI-1399") ; // VIPER #3272 : automatic variable as lvalue
        _msgs->Insert("parameter connections cannot be mixed ordered and named", "VERI-1400") ; // VIPER #3293 :
        _msgs->Insert("decimal constant %s is too large, using %d instead", "VERI-1401") ; // VIPER #3173 : Check for overflow in constant decimal value

// 07/2007 added:
        _msgs->Insert("multiple overrides for parameter %s", "VERI-1402") ; // VIPER #3330 : Should error out when a parameter overridden more than once
        _msgs->Insert("%s expects at least %d arguments", "VERI-1403") ; // VIPER #3289 : sys timing check task symantic check
        _msgs->Insert("%s expects at most %d arguments", "VERI-1404") ; // VIPER #3289 : sys timing check task symantic check
        _msgs->Insert("notifier variable %s must be a single bit register", "VERI-1405") ; // VIPER #3289 : sys timing check task symantic check
        _msgs->Insert("part-select %s is not allowed in declaration", "VERI-1406") ; // VIPER #3270 : Illegal partselect in declaration
        _msgs->Insert("attribute target identifier %s not found in this scope", "VERI-1407") ; // VIPER #3239 : about identifier pragmas
        _msgs->Insert("parallel path description may only have single input terminal descriptor and single output terminal descriptor", "VERI-1408") ; // VIPER # 3398 : V2K LRM 14.2.2
        _msgs->Insert("top-level design unit %s specified more than once, ignoring %s of library %s", "VERI-1409") ; // Module with same name is provided multiple times as top
        _msgs->Insert("an object or type with name %s already exists in Covergroup scope", "VERI-1410") ; // VIPER #2881 : Illegal 'option'/'type_option' declaration within covergroup
        _msgs->Insert("keyword '%s' is not allowed here in this mode of verilog", "VERI-1411") ; // VIPER #3366: Keyword 'unsigned' is not allowed in non-SV mode, its just a reserved work in Verilog 2001
        _msgs->Insert("single value range is not allowed in this mode of verilog", "VERI-1412") ; // VIPER #3403: SV specific single value range [n] used in non-SV mode.

// 08/2007 added :
        _msgs->Insert("%s in compilation scope are not allowed", "VERI-1413") ; // IEEE 1800 restriction statements and module instantiations are not allowed in compilation scope.
        _msgs->Insert("illegal reference to chandle type", "VERI-1414") ; // Illegal use of chandle type
        _msgs->Insert("expression should be of type queue or element of queue", "VERI-1415") ; // Illegal queue elements
        _msgs->Insert("illegal concatenation of real expression", "VERI-1416") ; // concatenation with real element
        _msgs->Insert("illegal concatenation of unpacked value", "VERI-1417") ; // concatenation with unpacked element
        _msgs->Insert("assignment pattern is illegal for use with %s port %s", "VERI-1418") ; // illegal use of assignment pattern
        _msgs->Insert("illegal operand for operator %s", "VERI-1419") ; // illegal operand for binary/unary operator
        _msgs->Insert("enum data type cannot be used with %s operator", "VERI-1420") ; // enum operand for ++/--
        _msgs->Insert("invalid operator %s in assignment to enum", "VERI-1421") ; // assignment operator +=/-= with enum type operand
        _msgs->Insert("illegal cast operation", "VERI-1422") ; // illegal casting
        _msgs->Insert("new expression can only be assigned to a class/covergroup handle/dynamic array", "VERI-1423") ; // illegal use of new expression
        _msgs->Insert("argument should be dynamic array", "VERI-1424") ; // illegal argument for new expression
        _msgs->Insert("illegal context for $", "VERI-1425") ; // illegal use of $
        _msgs->Insert("use '{index : value} syntax in associative array literal", "VERI-1426") ; // illegal associative array literal
        _msgs->Insert("number of elements does not match with the type", "VERI-1427") ; // illegal assignment pattern
        _msgs->Insert("illegal operand with streaming operator", "VERI-1428") ; // illegal operand with streaming operator. Viper 7112
        _msgs->Insert("illegal stream expression", "VERI-1429") ; // illegal element for streaming operator
        _msgs->Insert("illegal enumeration base type", "VERI-1430") ; // unpacked base for enum
// retired 3/14/04 :      _msgs->Insert("real/integral value required for parameter %s", "VERI-1431") ; // real/integral value for parameters with no type
        _msgs->Insert("dimension %d of %s cannot have data type", "VERI-1432") ; // illegal syntax for associative array
        _msgs->Insert("illegal empty dimension %d of %s", "VERI-1433") ; // illegal syntax for dynamic array
        _msgs->Insert("$ in dimension %d for %s", "VERI-1434") ; // illegal syntax for queue
        _msgs->Insert("cannot mix event and non-event types in this operation", "VERI-1435") ; // illegal use of event data type
        _msgs->Insert("cannot mix assoc and non-assoc arrays", "VERI-1436") ; // illegal use of associative array
        _msgs->Insert("assigning a %s type to a string requires a cast", "VERI-1437") ; // illegal value assignment to string type
        _msgs->Insert("cannot assign a string to %s type", "VERI-1438") ; // illegal target for string value
        _msgs->Insert("arrays have different elements", "VERI-1439") ; // different element for array assignment
        _msgs->Insert("associative array keys do not match", "VERI-1440") ; // different keys for associative array assignment
        _msgs->Insert("illegal slicing of associative array", "VERI-1441") ; // illegal slicing for associative array
        _msgs->Insert("illegal index for associative array", "VERI-1442") ; // illegal index for associative array
        _msgs->Insert("illegal reference to %s", "VERI-1443") ; // indexing on named type
        _msgs->Insert("illegal usage of signing with unpacked struct/union type", "VERI-1444") ; // signing on unpacked struct/union
        _msgs->Insert("event expressions must result in a singular type", "VERI-1445") ; // unpacked event expression
        _msgs->Insert("only genvar can be assigned here", "VERI-1446") ; // VIPER #3237
        //_msgs->Insert("variable %s must be a single bit reg", "VERI-1447") ; // VIPER #3290 : type of udp variable
        _msgs->Insert("illegal data type for udp register %s", "VERI-1447") ; // VIPER #3290 : type of udp variable
        //_msgs->Insert("port %s must be a single bit %s", "VERI-1448") ; // type of UDP ports
        _msgs->Insert("illegal data type for udp port %s", "VERI-1448") ; // type of UDP ports
        _msgs->Insert("%s cannot be declared within UDP %s", "VERI-1449") ; // illegal object defined inside UDP
        _msgs->Insert("first argument of %s should be a string", "VERI-1450") ; // VIPER 3341
        _msgs->Insert("second argument of %s should be a memory name", "VERI-1451") ; // VIPER 3341

// 09/2007 added :
        _msgs->Insert("element index %d into %s is out of bounds", "VERI-1452") ; // VIPER #3274: Error out if constant index expression is out of bounds
        _msgs->Insert("port %s is not connected to this instance", "VERI-1453") ; // VIPER #3230: actual of a formal port is not specified
        _msgs->Insert("same genvar %s cannot control this nested for generate loop", "VERI-1454") ; // VIPER #3523: Nested for loops cannot use the same genvar object
        _msgs->Insert("actual and formal for a ref must be equivalent types", "VERI-1455") ; // actual/formal checking for ref port
        _msgs->Insert("field %s of packed struct/union may not be declared rand or randc", "VERI-1456") ; // check for random variable
        _msgs->Insert("invalid data type for random variable %s", "VERI-1457") ; // illegal data type for random variable
        _msgs->Insert("illegal reference to event type", "VERI-1458") ; // illegal reference to event type
        _msgs->Insert("illegal argument in %s, require %s", "VERI-1459") ; // illegal argument in system function call
        _msgs->Insert("cyclic reference to SVA %s %s", "VERI-1460") ; // illegal cyclic reference to sequence/property
        _msgs->Insert("aliasing can be done only on net types", "VERI-1461") ; // VIPER #3555
        _msgs->Insert("nets connected in alias statement must be type compatible", "VERI-1462") ; // VIPER #3555
        _msgs->Insert("illegal output port connection to %s", "VERI-1463") ; // VIPER #3577: Check for illegal port connection to non-input ports

// 10/2007 added
// Retiered with #3481 fix        _msgs->Insert("foreach statement not supported for synthesis", "VERI-1464") ; // VIPER #3481
        _msgs->Insert("generate loop index %s is not defined as a genvar", "VERI-1465") ; // VIPER #3277

// 11/2007 added
        _msgs->Insert("assignment under multiple single edges is not supported for synthesis", "VERI-1466") ; // Vipers #2311, #3566 and many others
        _msgs->Insert("width does not match, stream larger than %s", "VERI-1467") ; // Vipers #3509

// 12/2007 added
        _msgs->Insert("this type of verilog connection is not supported for vhdl port", "VERI-1468") ; // Vipers #3368
        _msgs->Insert("no loop index variable found in foreach statement", "VERI-1469") ; // VIPER #3481: support foreach elaboration
        _msgs->Insert("foreach statement on associative/dynamic array is not supported", "VERI-1470") ; // VIPER #3481: support foreach elaboration
        // VIPER #3731: Compiler directive `begin_keywords/`end_keywords support:
        _msgs->Insert("unknown version specifier string %s, `begin_keywords ignored", "VERI-1471") ;
        _msgs->Insert("`end_keywords without `begin_keywords", "VERI-1472") ;
        _msgs->Insert("unmatched `begin_keywords directive in compilation unit", "VERI-1473") ;
        _msgs->Insert("%s is not a valid constant function call", "VERI-1474") ;      // VIPER #3276: Invalid constant function call
        _msgs->Insert("global or hierarchical references may not be used in a constant function", "VERI-1475") ; // VIPER #3276: Constant function call uses global/hier-ref

// 01/2008 added :
        _msgs->Insert("elements of %s cannot be mixed ordered and named", "VERI-1476") ; // VIPER #3742 :
        _msgs->Insert("more than one default value found in literal", "VERI-1477") ; // VIPER #3742 :
        _msgs->Insert("some element(s) in %s is missing", "VERI-1478") ; // VIPER #3742 :
        _msgs->Insert("expression has %d elements; expected %d", "VERI-1479") ; // VIPER #3742 :
        _msgs->Insert("out of range index label for array literal", "VERI-1480") ; // VIPER #3742 :
        _msgs->Insert("assignment pattern key %s is already covered", "VERI-1481") ; // VIPER #3742 :

        _msgs->Insert("Analyzing Verilog file %s", "VERI-1482") ; // VIPER #3142 :
        _msgs->Insert("Verilog file %s ignored due to errors", "VERI-1483") ; // VIPER #3142 :
        _msgs->Insert("Analyzing Verilog flavor PSL file %s", "VERI-1484") ; // VIPER #3142 :
        _msgs->Insert("Verilog flavor PSL file %s ignored due to errors", "VERI-1485") ; // VIPER #3142 :
        _msgs->Insert("module %s in library %s is not yet analyzed", "VERI-1486") ; // VIPER #3142 :
        _msgs->Insert("PSL unit %s cannot be elaborated by itself", "VERI-1487") ; // VIPER #3142 :
        _msgs->Insert("Reading Verilog file %s", "VERI-1488") ; // VIPER #3142 :
        _msgs->Insert("      Resolving module %s", "VERI-1489") ; // VIPER #3142 :
        _msgs->Insert("Pretty printing module %s to file %s", "VERI-1490") ; // VIPER #3142 :
        _msgs->Insert("Pretty printing all modules in library %s to file %s", "VERI-1491") ; // VIPER #3142 :
        _msgs->Insert("Pretty printing all modules in all libraries to file %s", "VERI-1492") ; // VIPER #3142
        _msgs->Insert("The library %s was not found", "VERI-1493") ; // VIPER #3142
        _msgs->Insert("No library search path could be found. Please set a search path to where module can be saved", "VERI-1494") ; // VIPER #3142
        _msgs->Insert("Save failed due to mkdir failure", "VERI-1495") ; // VIPER #3142
        _msgs->Insert("Ignoring elaborated Verilog module which cannot be saved %s.%s", "VERI-1496") ; // VIPER #3142
        _msgs->Insert("The module %s was not found in library %s", "VERI-1497") ; // VIPER #3142
        _msgs->Insert("%s does not contain a known Verilog parse-tree format", "VERI-1498") ; // VIPER #3142
        _msgs->Insert("%s has persistence version number %s, which is outside the range 0x0ab00029 to %s that this executable can restore", "VERI-1499") ; // VIPER #3142
        _msgs->Insert("%s was previously created using a PSL-enabled Verilog analyzer, since the current Verilog analyzer is not PSL-enabled, there is a high probability that a restore error will occur due to unknown PSL constructs", "VERI-1500") ; // VIPER #3142
        _msgs->Insert("%s was previously created using a AMS-enabled Verilog analyzer, since the current Verilog analyzer is not AMS-enabled, there is a high probability that a restore error will occur due to unknown AMS constructs", "VERI-1501") ; // VIPER #3142
        _msgs->Insert("%s was previously created using a System Verilog-enabled Verilog analyzer, since the current Verilog analyzer is not System Verilog-enabled, there is a high probability that a restore error will occur due to unknown System Verilog constructs", "VERI-1502") ; // VIPER #3142
        _msgs->Insert("Restoring Verilog parse-tree %s.%s from %s", "VERI-1503") ; // VIPER #3142
        _msgs->Insert("%s.%s failed to restore", "VERI-1504") ; // VIPER #3142
        _msgs->Insert("Verilog library search path not set; cannot restore", "VERI-1505") ; // VIPER #3142
        _msgs->Insert("%s does not contain a known VERI parse-tree format", "VERI-1506") ; // VIPER #3142
        _msgs->Insert("The path \"%s\" does not exist", "VERI-1507") ; // VIPER #3142
        _msgs->Insert("The default veri library search path is now \"%s\"", "VERI-1508") ; // VIPER #3142
        _msgs->Insert("The veri library search path for library \"%s\" is now \"%s\"", "VERI-1509") ; // VIPER #3142
        _msgs->Insert("This parse-tree is too large to be saved", "VERI-1510") ; // VIPER #3142
        _msgs->Insert("A Verilog parse-tree node is trying to be created from a SaveRestore object that is in save mode!", "VERI-1511") ; // VIPER #3142
        _msgs->Insert("Error from RegisterDependencies", "VERI-1512") ; // VIPER #3142
        _msgs->Insert("Registering Dependencies Error: %s", "VERI-1513") ; // VIPER #3142
        _msgs->Insert("Registering Dependencies Error: The %s '%s' could not be found during restore", "VERI-1514") ; // VIPER #3142
        _msgs->Insert("%s needs to be re-saved since %s.%s changed", "VERI-1515") ; // VIPER #3142
        _msgs->Insert("Verilog reader: User Interrupt. Cleaning up....", "VERI-1516") ; // VIPER #3142
        _msgs->Insert("cannot open verilog file %s", "VERI-1517") ; // VIPER #3142
        _msgs->Insert("library '%s' is empty", "VERI-1518") ; // VIPER #3142
        _msgs->Insert("first instantiation was done here", "VERI-1519") ; // VIPER #3142

// 2/2008 added :
        _msgs->Insert("illegal net data type", "VERI-1520") ; // VIPER #2475 & 2648
        _msgs->Insert("net data types must be 4 state", "VERI-1521") ; // VIPER #2475 & 2648
        _msgs->Insert("%s net arrays are not allowed", "VERI-1522") ; // VIPER #2475 & 2648
        _msgs->Insert("Saving Verilog parse-tree %s.%s into %s", "VERI-1523") ; // VIPER #3142
        _msgs->Insert("illegal index expression", "VERI-1524") ; // VIPER #3854
        _msgs->Insert("return type of extern method %s does not match prototype", "VERI-1525") ; // VIPER #2665 & 2667
        _msgs->Insert("formal name of extern method %s does not match prototype", "VERI-1526") ; // VIPER #2665 & 2667
        _msgs->Insert("formal type of extern method %s does not match prototype", "VERI-1527") ; // VIPER #2665 & 2667
        _msgs->Insert("default value of %s is not allowed in extern method definition", "VERI-1528") ; // VIPER #2665 & 2667
        _msgs->Insert("formal count of extern method %s does not match prototype", "VERI-1529") ; // VIPER #2665 & 2667
        _msgs->Insert("cannot select inside %s using hierarchical reference", "VERI-1530") ; // VIPER #3846
        // retired with VIPER #7703 _msgs->Insert("compiler directive %s is not allowed here", "VERI-1531") ; // VIPER #3873

// 3/2008 added :
        _msgs->Insert("class does not expect any parameter", "VERI-1532") ; // VIPER #3828
        _msgs->Insert("class %s does not have a parameter named %s", "VERI-1533") ; // VIPER #3828
        _msgs->Insert("too many parameters for class instance", "VERI-1534") ; // VIPER #3828

// 4/2008 added :
        _msgs->Insert("concurrent assertion after timimg control is not allowed", "VERI-1535") ; // VIPER #3956
        _msgs->Insert("illegal concurrent assertion in action block", "VERI-1536") ; // VIPER #3953

// 5/2008 added :
        _msgs->Insert("local variable %s referenced in expression where it does not flow", "VERI-1537") ; // VIPER #3958 & 3960
        _msgs->Insert("illegal reference to %s %s", "VERI-1538") ; // SVA semantics
        _msgs->Insert("method %s is not allowed in disable iff condition", "VERI-1539") ; // SVA semantics  VIPER #3961
        _msgs->Insert("explicit clocking event must be specified for %s in disable iff condition", "VERI-1540") ; // SVA semantics  VIPER #3961
        _msgs->Insert("illegal context for disable iff", "VERI-1541") ; // SVA semantics  VIPER #3948
        _msgs->Insert("illegal function call with %s argument in assertion expression", "VERI-1542") ; // SVA semantics  VIPER #3962
        _msgs->Insert("the application of ended or triggered to a sequence %s to which local variable is being passed should be maximal boolean expression", "VERI-1543") ; // SVA semantics
        _msgs->Insert("local variable %s can be passed only as entire actual argument to sequence on which ended or triggered is applied", "VERI-1544") ; // SVA semantics VIPER #3946
        _msgs->Insert("hierarchical access to local variable %s is illegal", "VERI-1545") ; // SVA semantics VIPER #3959
        _msgs->Insert("local variable passed to formal %s of sequence %s to which ended or triggered is applied. The local variable does not flow out of the sequence", "VERI-1546") ; // SVA semantics
        _msgs->Insert("not operator cannot be applied to recursive property", "VERI-1547") ; // SVA semantics VIPER #3949
        _msgs->Insert("static methods cannot be virtual", "VERI-1548") ; // VIPER #4030
        _msgs->Insert("illegal reference to non-static %s from static function", "VERI-1549") ; // VIPER #4030
        _msgs->Insert("instance constant %s cannot be declared static", "VERI-1550") ; // VIPER #4039
        _msgs->Insert("access to %s member %s from outside a class context is illegal", "VERI-1551") ; // VIPER #4043
        _msgs->Insert("access to %s member %s from %s class context is illegal", "VERI-1552") ; // VIPER #4043, #6241

// 06/2008 added :
        _msgs->Insert("illegal edge on named event", "VERI-1553") ; // VIPER #4063
        _msgs->Insert("module %s having interface port(s) (%s) cannot be elaborated by itself", "VERI-1554") ; // VIPER #4078 (updated for VIPER #7324)
        //_msgs->Insert("array instantiation of interfaces is not yet supported", "VERI-1555") ; // Retired: now we suport array instantiation of interfaces.

// 07/2008 added :
        _msgs->Insert("%s is an unknown type", "VERI-1556") ; // VIPER #4073 :
        _msgs->Insert("elements must both be signed or both be unsigned", "VERI-1557") ; // VIPER #4121
        _msgs->Insert("elements must both be 2-state or both be 4-state", "VERI-1558") ; // VIPER #4121

// 08/2008 added
        _msgs->Insert("statement label and block identifier are not allowed together","VERI-1559") ; // VIPER #4205
        _msgs->Insert("%s in final block is illegal", "VERI-1560") ; // VIPER #4204
        _msgs->Insert("repeat expression should evaluate to non-negative value", "VERI-1561") ; // VIPER #4231
        _msgs->Insert("illegal actual arguments passed in recursive property %s", "VERI-1562") ; // VIPER 4227 & 2275
        _msgs->Insert("%s in always_comb/always_latch/always_ff is not allowed", "VERI-1563") ; // VIPER #4241
        _msgs->Insert("all identifiers need to have initial value", "VERI-1564") ; // VIPER #4262
        _msgs->Insert(".* token can only appear at most once in port list", "VERI-1565") ; // VIPER #4257

// 09/2008 added:
        _msgs->Insert("non-net variable cannot be connected to inout port %s", "VERI-1566") ; // VIPER #4255
        _msgs->Insert("virtual interface %s cannot be used as port", "VERI-1567") ; // VIPER #3786
        _msgs->Insert("unique if/case has matching case items", "VERI-1568") ; // VIPER #4062

// 10/2008 added:
        _msgs->Insert("%s statement not allowed within fork..join", "VERI-1569") ; // VIPER #4323 & VIPER #4830
        _msgs->Insert("illegal SVA sequence/property expression", "VERI-1570") ; // VIPER #4337
        _msgs->Insert("the call to super.new must be the first statement in the constructor", "VERI-1571") ; // Class Synthesis support VIPER 4351
        //_msgs->Insert("not a synthesizable class declaration", "VERI-1572") ; // VIPER 4351
        _msgs->Insert("object handle as arguments to %s not valid for synthesis", "VERI-1573") ; // VIPER 4351, VIPER 5874
        _msgs->Insert("multiple definitions of new in class %s, taking the first one", "VERI-1574") ; // VIPER 4351
        _msgs->Insert("class assignment is not synthesizable", "VERI-1575") ; // VIPER 4351
        _msgs->Insert("expression not valid for synthesis", "VERI-1576") ; // VIPER 4351

// 11/2008 added:
        _msgs->Insert("invalid assignment value for genvar %s", "VERI-1577") ; // VIPER #4386
        _msgs->Insert("system functions/tasks may not be used in a constant function", "VERI-1578") ; // VIPER 4380) ;

// 12/2008 added:
        _msgs->Insert("illegal reference to %s from static function/task", "VERI-1579") ; // VIPER 4450) ;
        _msgs->Insert("illegal reference to non-static id %s from class scope", "VERI-1580") ; // VIPER #4455
        _msgs->Insert("step cannot be used in %s", "VERI-1581") ; // VIPER #4489
        _msgs->Insert("expression with variable or undeterminable width as argument to $bits", "VERI-1582") ; // VIPER #4469
        _msgs->Insert("z value in udp table entry is illegal and will be treated as x", "VERI-1583") ; // VIPER #4532

// 01/2009 added:
        _msgs->Insert("ref type port %s cannot be left unconnected", "VERI-1584") ; // VIPER #4580
        _msgs->Insert("left hand side of inside operator is not singular expression", "VERI-1585") ; // VIPER #4579
        _msgs->Insert("number of ticks for $past must be 1 or greater", "VERI-1586") ; // VIPER #4586
        _msgs->Insert("second argument of $%s must be a memory", "VERI-1587") ; // VIPER #4584
        _msgs->Insert("positional arguments must occur before named arguments", "VERI-1588") ; // VIPER #4618

// 02/2009 added:
        _msgs->Insert("illegal output symbol '%c' for combinational udp", "VERI-1589") ; // VIPER #4707
        _msgs->Insert("illegal current state symbol '%c' for sequential udp", "VERI-1590") ; // VIPER #4707
        _msgs->Insert("illegal next state symbol '%c' for sequential udp", "VERI-1591") ; // VIPER #4707
        _msgs->Insert("combinational udp port %s cannot be reg", "VERI-1592") ; // VIPER #4707
        //_msgs->Insert("failed to find name '%s' in the specified scope", "VERI-1593") ; // VIPER #4601
        _msgs->Insert("%s %s in subclass cannot override %s in superclass", "VERI-1593") ; // VIPER #4728
        _msgs->Insert("return type of %s in subclass does not match with return type in superclass", "VERI-1594") ; // VIPER #4728
        _msgs->Insert("port count %d of %s in subclass does not match with port count %d in superclass", "VERI-1595") ; // VIPER #4728
        _msgs->Insert("type of argument %s of virtual method %s does not match with type of argument in superclass", "VERI-1596") ; // VIPER #4728
        //_msgs->Insert("builtin package '%s' cannot be replaced", "VERI-1597") ; // VIPER #4774. Changed message text as following
        _msgs->Insert("replacing builtin package '%s' violates IEEE 1800 LRM", "VERI-1597") ; // VIPER #5248 (test_288)

// 03/2009 added:
        _msgs->Insert("cannot have multiple initializations in udp", "VERI-1598") ; // VIPER #4661
        _msgs->Insert("packed dimension must specify a range", "VERI-1599") ; // VIPER #4885
        _msgs->Insert("task attached to the sequence cannot have out and inout formal arguments", "VERI-1600") ; // VIPER #4908
        _msgs->Insert("illegal input symbol '%c' for udp", "VERI-1601") ; // VIPER #4878
        _msgs->Insert("cannot set both signing and void type on function decl", "VERI-1602") ; // VIPER #4897
        _msgs->Insert("sequential udp output port %s must be reg", "VERI-1603") ; // VIPER #4930
        _msgs->Insert("cannot bind module %s, mixed language library vl not compiled", "VERI-1604") ; // Mixed Lang Code Clean up
        _msgs->Insert("binary dump database must be encrypted to secure `protected RTL", "VERI-1605") ;

// 04/2009 added:
        _msgs->Insert("non-module variable %s cannot be initialized at declaration", "VERI-1606") ; // VIPER #4943
        _msgs->Insert("invalid declaration of built-in method %s", "VERI-1607") ; // VIPER #4833
        _msgs->Insert("cannot re-specify direction for signal %s to %s because it was specified as input", "VERI-1608") ; // VIPER #4851
        _msgs->Insert("method new cannot have return type", "VERI-1609") ; // VIPER #4798
        _msgs->Insert("invalid use of virtual keyword for built-in method %s", "VERI-1610") ; // VIPER #4805
        _msgs->Insert("name of port declaration in module definition does not match that in extern module", "VERI-1611") ; // Extern module
        _msgs->Insert("port %s should exactly match its corresponding port in extern module", "VERI-1612") ; // Extern module
        _msgs->Insert("actual module definition does not have port %s", "VERI-1613") ; // Extern module
        _msgs->Insert("illegal actual value for ref port %s for %s", "VERI-1614") ; // VIPER #4906
        _msgs->Insert("combinational udp cannot have initial statement", "VERI-1615") ; // VIPER #5017
        _msgs->Insert("identifier %s is not in udp port list", "VERI-1616") ; // VIPER #5017
        _msgs->Insert("direction of port %s does not match that in extern module declaration", "VERI-1617") ; // VIPER #4808

// 05/2009 added:
        _msgs->Insert("scalared or vectored keyword can be used with vectored data types", "VERI-1618") ; // VIPER #4828
        _msgs->Insert("a void type may only be used in a tagged union", "VERI-1619") ; // VIPER #4826
        _msgs->Insert("expected a system function, not system task $%s", "VERI-1620") ; // VIPER #2641
        _msgs->Insert("only one event control is allowed in an always_ff block", "VERI-1621") ; // VIPER #4890
        _msgs->Insert("an always_ff block must have one and only one event control", "VERI-1622") ; // VIPER #4890, 7323
        _msgs->Insert("static qualifier for constraint block %s does not match prototype", "VERI-1623") ; // VIPER #4837
        _msgs->Insert("weight in randcase is negative", "VERI-1624") ; // VIPER #4835
        _msgs->Insert("illegal real type expression for coverpoint expression", "VERI-1625") ; // VIPER #4844
        _msgs->Insert("method never defined for %s %s", "VERI-1626") ; // VIPER #4797
        _msgs->Insert("port %s must not be declared to be an array", "VERI-1627") ; // VIPER #4656
        _msgs->Insert("enum member %s has value that is outside the representable range of the enum", "VERI-1628") ; // VIPER #4893
        _msgs->Insert("enum literal %s width (%d) must match enum width (%d)", "VERI-1629") ; // VIPER #4796
        //_msgs->Insert("multiple dimension port %s's connection not supported in mixed language flow", "VERI-1630") ; // VIPER #5087
        _msgs->Insert("a distribution expression in constraint must contain at least one random variable", "VERI-1631") ; // VIPER 4715
        _msgs->Insert("randc variables cannot be used in distribution and solve-before", "VERI-1632") ; // VIPER 4715, VIPER 4836
        _msgs->Insert("non rand variable %s cannot be used in solve before", "VERI-1633") ; // VIPER 4997

// 06/2009 added:
        _msgs->Insert("illegal void cast of task enable %s", "VERI-1634") ; // VIPER 5133
        _msgs->Insert("dynamic variables shall not be written with non-blocking assignments", "VERI-1635") ; //VIPER #4995
        _msgs->Insert("first argument level of system task %s must be a positive integer", "VERI-1636") ; //VIPER #5136
        _msgs->Insert("genvar %s should be declared within module where it is used", "VERI-1637") ;
        _msgs->Insert("recursive analog function call %s is not permitted", "VERI-1638") ; // AMS 2.3
        _msgs->Insert("second argument of %s must be either global or instance", "VERI-1639") ; // AMS 2.3

// 07/2009 added:
        _msgs->Insert("cannot access %s member %s of class %s in scope of %s", "VERI-1640") ; // VIPER 4596
        _msgs->Insert("null as source expression is not allowed here", "VERI-1641") ; // VIPER #5131
        _msgs->Insert("default clocking specified multiple times", "VERI-1642") ; // VIPER #4842
        _msgs->Insert("another default clocking is specified here", "VERI-1643") ; // VIPER #4842
        _msgs->Insert("unpacked value/target cannot be used in assignment", "VERI-1644") ; // VIPER #5159
        _msgs->Insert("prefix of randomize is not a class id", "VERI-1645") ; // VIPER #5174
        _msgs->Insert("member %s of unpacked structure/union may not be declared rand or randc", "VERI-1646") ; // VIPER #4804
        _msgs->Insert("variable %s is written by both continuous and procedural assignments", "VERI-1647") ; // VIPER #5178
        _msgs->Insert("illegal argument to dumpvars", "VERI-1648") ; // VIPER #5171
        _msgs->Insert("prefix %s of constraint_mode is not the object handle containing constraint block", "VERI-1649") ; // VIPER #5161
        _msgs->Insert("aliasing an individual signal %s to itself is not allowed", "VERI-1650") ; // VIPER #4829
        _msgs->Insert("multiple statement function/task without begin/end not supported in this mode of Verilog", "VERI-1651") ; // VIPER #4762
        _msgs->Insert("incomplete structure literal, no value is specified for field %s", "VERI-1652") ; // VIPER #5160

// 08/2009 added:
        _msgs->Insert("reference to type %s through instance %s is not allowed here", "VERI-1653") ; // VIPER #5204
        _msgs->Insert("port has different sizes in different instances of this array", "VERI-1654") ; // VIPER #5208
        _msgs->Insert("prefix %s of rand_mode is not a object handle or a variable of type rand or randc", "VERI-1655") ; // VIPER #4834
        _msgs->Insert("call to access function %s with repeated argument %s", "VERI-1656") ; // AMS
        _msgs->Insert("type reference expression may not be hierarchical name or elements of dynamic type", "VERI-1657") ; // VIPER #4578
        _msgs->Insert("interface type objects cannot be declared within class", "VERI-1658") ; // VIPER #5232
        _msgs->Insert("unexpected %s", "VERI-1659") ; // VIPER #4807
        _msgs->Insert("new timescale value (%s) conflicts with an existing value", "VERI-1660") ; // VIPER #4807
        _msgs->Insert("error in timescale or timeprecision statement. <timeprecision> must be at least as precise as <timeunit>", "VERI-1661") ; // VIPER #4807
        _msgs->Insert("%s literal not a power of 10", "VERI-1662") ; // VIPER #4807
        _msgs->Insert("expected simple identifier as loop variable", "VERI-1663") ; // VIPER 5407 and others : give error for incorrect loop variable declaration
// VIPER 5248 superset parsing messages. Insert new ones here (after 1663) :
        _msgs->Insert("cast without ' violates IEEE 1800 syntax","VERI-1668") ;
        _msgs->Insert("edge path description violates IEEE 1800 syntax","VERI-1669") ;
        _msgs->Insert("gate instantiation in root scope violates IEEE 1800 syntax","VERI-1670") ;
        _msgs->Insert("package import in class violates IEEE 1800 syntax","VERI-1671") ;
        _msgs->Insert("hierarchical name in identifier list violates IEEE 1800 syntax","VERI-1672") ;
        _msgs->Insert("enum without type violates IEEE 1800 syntax","VERI-1673") ;
        // _msgs->Insert("enum char violates IEEE 1800 syntax","VERI-1674") ;
//        _msgs->Insert("missing parameter assignment violates IEEE 1800 syntax","VERI-1675") ; // Not clear if this is really dis-allowed.
        _msgs->Insert("empty loop variable assignment statement violates IEEE 1800 syntax","VERI-1676") ;
// end of VIPER 5248 superset parsing messages
        _msgs->Insert("%s is not a valid package or class type prefix for '::'", "VERI-1677") ; //VIPER #5238

// 09/2009 added:
        _msgs->Insert("primitive %s connection must be a scalar var or net", "VERI-1678") ; // VIPER #5247
        _msgs->Insert("posedge or negedge on real is illegal", "VERI-1679") ; // VIPER #5253
        _msgs->Insert("multiple packed dimensions are not allowed in this mode of verilog", "VERI-1680") ; // VIPER #5274
        _msgs->Insert("module instantiation of %s is not allowed in interface %s", "VERI-1681") ; // VIPER #4666
        _msgs->Insert("solve before expression in constraint set violates IEEE 1800 syntax","VERI-1682") ; //VIPER #5277
        _msgs->Insert("prefix of array method %s should not be %s","VERI-1683") ; //VIPER #5314
        _msgs->Insert("multiple definitions of %s","VERI-1684") ; //VIPER #5316
        _msgs->Insert("%s is not supported for synthesis","VERI-1685") ; //VIPER #5315
        _msgs->Insert("multiple exports of %s","VERI-1686") ; // export/import support
        _msgs->Insert("dynamic range in assignment not supported for synthesis","VERI-1687") ; // VIPER 5306

// 10/2009 added:
        _msgs->Insert("poorly formed formal for macro definition %s","VERI-1688") ; // VIPER #5379
        _msgs->Insert(".* in port not allowed without declaration of extern %s","VERI-1689") ; // VIPER 5299
        _msgs->Insert("%s expressions not allowed in argument of %s randomize call","VERI-1690") ; // VIPER #5416 & #5810
        _msgs->Insert("assert/assume/cover property statement in root scope violates IEEE 1800 syntax","VERI-1691") ; // VIPER 5248
        _msgs->Insert("clocked expression not valid in function/task argument","VERI-1692") ;
        _msgs->Insert("clocked expression not valid in property/sequence argument, violates IEEE 1800 syntax","VERI-1693") ;
        _msgs->Insert("modport declaration id without direction not allowed, violates IEEE 1800 syntax","VERI-1694") ;
        _msgs->Insert("open value range with both bounds as '$' violates IEEE 1800 syntax","VERI-1695") ;
        _msgs->Insert("argument %d of task %s is implicitly defined, violates IEEE 1800 syntax","VERI-1696") ;
        _msgs->Insert("missing '{', violates IEEE 1800 syntax","VERI-1697") ;
        _msgs->Insert("forward declaration of covergroup violates IEEE 1800 syntax","VERI-1698") ; // VIPER 5248
        _msgs->Insert("alias declaration in program violates IEEE 1800 syntax","VERI-1699") ; // VIPER 5248
        // _msgs->Insert("char as concat label violates IEEE 1800 syntax","VERI-1700") ; // VIPER 5248
        _msgs->Insert("parameter declaration inside specify block violates IEEE 1800 syntax","VERI-1701") ; // VIPER 5248
        _msgs->Insert("using %s.%s in modport violates IEEE 1800 syntax", "VERI-1702") ; // VIPER #5248
        _msgs->Insert("%s must be defined as a clocking id", "VERI-1703") ; // VIPER #5248
        _msgs->Insert("modport port direction mismatch", "VERI-1704") ; // VIPER #5248
        _msgs->Insert("unexpected <'>, violates IEEE 1800 syntax","VERI-1705") ; // VIPER 5248
        _msgs->Insert("unexpected default, violates IEEE 1800 syntax","VERI-1706") ; // VIPER 5248
        _msgs->Insert("case statement with no case item violates IEEE 1800 syntax", "VERI-1707") ; // VIPER #5248
        _msgs->Insert("unexpected %s, violates IEEE 1800 syntax","VERI-1708") ;
        _msgs->Insert("%s in select expression violates IEEE 1800 syntax","VERI-1709") ; // VIPER 5248
        _msgs->Insert("local qualifier before constraint declaration violates IEEE 1800 syntax","VERI-1710") ; // VIPER 5248
        _msgs->Insert("solve-before in foreach violates IEEE 1800 syntax","VERI-1711") ; // VIPER 5248
        _msgs->Insert("struct/union/enum as slice size in streaming concatenation violates IEEE 1800 syntax","VERI-1712") ; // VIPER 5248
        _msgs->Insert("life times of extern declaration and prototypes do not match","VERI-1713") ;// vcs-verific incompatibility
        _msgs->Insert("unexpected local, violates IEEE 1800 syntax","VERI-1714") ;// vcs-verific incompatibility
        _msgs->Insert("illegal call to %s in constraint block","VERI-1715") ;// Viper 5438
        _msgs->Insert("only automatic functions allowed in constraint block","VERI-1716") ;// Viper 5438
        _msgs->Insert("only input and const ref ports allowed in function called from constraint block","VERI-1717") ;// Viper 5438
        _msgs->Insert("event type variable %s not allowed here","VERI-1718") ;// Viper 5438
        _msgs->Insert("new expression can only be assigned to a class/covergroup","VERI-1719") ; // Viper 5475
        _msgs->Insert("unexpected final, violates IEEE 1800 syntax","VERI-1720") ;// vcs-verific incompatibility

// 11/2009 added :
        _msgs->Insert("system function call as initial value of type parameter violates IEEE 1800 syntax","VERI-1721") ;// vcs-verific incompatibility
        _msgs->Insert("bracketed constraint block violates IEEE 1800 syntax", "VERI-1722") ; // VIPER #5248 (foreach)
        _msgs->Insert("dpi task import property 'pure' violates IEEE 1800 syntax", "VERI-1723") ; // VIPER #5248 (pure)
        _msgs->Insert("loop statement with empty body is not permitted in this mode of verilog", "VERI-1724") ; // VIPER #5540
        _msgs->Insert("%s expects %d arguments, violates IEEE 1800 syntax", "VERI-1725") ; // VIPER #5248 (test_297)
        _msgs->Insert("extern function declaration %s violates IEEE 1800 syntax", "VERI-1726") ; // VIPER #5248 (9000079760/Cimeron_directc etc)
        _msgs->Insert("wildcard in cross coverage violates IEEE 1800 syntax", "VERI-1727") ; // VIPER #5248 (wildcard)
        _msgs->Insert("non integral type variables are not allowed in constraint declaration", "VERI-1728") ; // VIPER #5359
        // _msgs->Insert("ignoring `begin_keywords since it tries to upgrade the language mode", "VERI-1729") ; // VIPER #5386: allow upgrade
        _msgs->Insert("`begin_keywords used to upgrade the language mode", "VERI-1729") ;  // VIPER #5386: So, changed the message text
        _msgs->Insert("`begin_keywords directives cannot be specified inside a design unit", "VERI-1730") ; // VIPER #5386
        _msgs->Insert("only branch/analog net can be assigned in contribution statement", "VERI-1731") ; // AMS
        _msgs->Insert("branch contribution is only allowed in analog domain", "VERI-1732") ; // AMS
        _msgs->Insert("inconsistent dimension in declaration, allowing under AMS mode", "VERI-1733") ; // AMS VIPER 5563
        _msgs->Insert("empty parameter not allowed in macro definition", "VERI-1734") ; // VIPER #5580
        _msgs->Insert("encountered duplicate formal name %s, actual value %s will be ignored", "VERI-1735") ; // VIPER #5580
        _msgs->Insert("illegal multiconcat expression in target", "VERI-1736") ; // VIPER #5301 (test_50)

// 12/2009 added :
        _msgs->Insert("specparam %s not allowed here", "VERI-1737") ; // VIPER #5573
        _msgs->Insert("instantiation of paramset is not yet supported in elaboration", "VERI-1738") ; // AMS
        _msgs->Insert("loop variable declaration is not allowed in this mode of verilog", "VERI-1739") ; // VIPER #5594
        _msgs->Insert("out-of-block function declaration %s violates IEEE 1800 semantic", "VERI-1740") ; // VIPER #5248 (test_12)
        _msgs->Insert("positional notation should not be mixed with other type of notation", "VERI-1741") ; // VIPER #5586
        _msgs->Insert("%s delay form with this gate violates IEEE 1364 LRM", "VERI-1742") ; // VIPER #5248 (test_315)
        _msgs->Insert("empty concatenation not allowed here", "VERI-1743") ; // VIPER #5579
        _msgs->Insert("illegal expression in type operator", "VERI-1744") ;
        _msgs->Insert("illegal prefix for %s", "VERI-1745") ; // VIPER 5353
        _msgs->Insert("size casting a real expression violates IEEE 1800 LRM", "VERI-1746") ; // VIPER #5248 (test_41)
        _msgs->Insert("%s is not valid in branch terminal", "VERI-1747") ; // AMS
        _msgs->Insert("solve id-list before id-list hard violates IEEE 1800 syntax", "VERI-1748") ; // VIPER #5248 (hard)

// 01/2010 added :
        _msgs->Insert("strength without assignment is not allowed in net declaration", "VERI-1749") ; // VIPER #5648
        _msgs->Insert("illegal assignment of structure literals to union", "VERI-1750") ; // VIPER #5248 (test_47)
        _msgs->Insert("both strengths required for %s", "VERI-1751") ; // VIPER #5625, VIPER #5677
        _msgs->Insert("unknown pragma '%s' value '%s' ignored", "VERI-1752") ; // VIPER 5628
        _msgs->Insert("assignment sub expression is allowed only in System Verilog mode", "VERI-1753") ; // VIPER #5674
        _msgs->Insert("invalid initialization in declaration", "VERI-1754") ; // VIPER #5667
        _msgs->Insert("unexpected with default, violates IEEE 1800 syntax", "VERI-1755") ; // VIPER #5248 (with_def_constraints)
        _msgs->Insert("elaboration system task %s violates IEEE 1800 syntax", "VERI-1756") ; // VIPER #5248 (dollar_warning)
        _msgs->Insert("min-typ-max expression in argument violates IEEE 1800 syntax", "VERI-1757") ; // VIPER #5248 (display_close_brace)
        _msgs->Insert("macro name %s starting with a number violates Verilog syntax", "VERI-1758") ; // VIPER #5248 (macro_numstart)
        _msgs->Insert("empty modport port declaration violates IEEE 1800 syntax", "VERI-1759") ; // VIPER #5248 (modport_close_brace)
        _msgs->Insert("packed union contains members of different size", "VERI-1760") ; // VIPER #5593
        _msgs->Insert("size mismatch in connection", "VERI-1761") ; // VIPER #5543 (VIPER #7015 : Rectified spelling)

// 02/2010 added :
        _msgs->Insert("%s in enum literal range violates IEEE 1800 syntax", "VERI-1762") ; // VIPER #5284 (RANGE etc)
        _msgs->Insert("the sva directive is not sensitive to clock", "VERI-1763") ; // VIPER #2627
        _msgs->Insert("timeunit/precision declaration with a space before unit violates IEEE 1800 syntax", "VERI-1764") ; // VCS Compatibility issue (Exar_svtb and timeunit)
        _msgs->Insert("loop variable declaration without initialization violates IEEE 1800 syntax", "VERI-1765") ; // 5717 : VCS Compatibility issue
        _msgs->Insert("edge must be specified for the contextually inferred clock", "VERI-1766") ; // VIPER #3955
        _msgs->Insert("the clock flow cannot change in the RHS of %s operator", "VERI-1767") ; // VIPER #4181
        _msgs->Insert("illegal access of %s inside class %s", "VERI-1768") ; // VIPER #5452

// 03/2010 added :
        _msgs->Insert("type identifier %s used in illegal context", "VERI-1769") ; // VIPER #5806
        _msgs->Insert("empty item in case item list violates IEEE 1364 syntax", "VERI-1770") ; // VIPER #5814
        _msgs->Insert("type comparison cannot be done with non-type expressions", "VERI-1771") ; // VIPER #5819
        _msgs->Insert("illegal operator %s for type operand", "VERI-1772") ; // VIPER #5819
        _msgs->Insert("white space between %s and %s violates IEEE 1800 syntax", "VERI-1773") ; // VIPER #5248 (open_sqr_coveragebins)
        _msgs->Insert("udp table entry %s conflicts with earlier entry", "VERI-1774") ; // VIPER #5812

        _msgs->Insert("invalid edge specification %s in udp", "VERI-1775") ; // VIPER #5812: not related but added semantic check during this fix
        _msgs->Insert("direction is not allowed in formal argument of let", "VERI-1776") ; // VCS Compatibility issue (let_in_interface) : VIPER #5710

// 04/2010 added :
        // obselute after Viper 6303 Fix
        //_msgs->Insert("default disable iff is not yet supported for synthesis", "VERI-1777") ; // VCS Compatibility issue (disable) : VIPER #5710
        _msgs->Insert("instantiation is not allowed in sequential area except checker instantiation", "VERI-1778") ; // VCS Compatibility issue (always_checker): VIPER #5710
        _msgs->Insert("intra-assignment delay control is not allowed in an assignment to a clocking", "VERI-1779") ; // VIPER #5832
        _msgs->Insert("multiple dimension interface instance not supported for synthesis", "VERI-1780") ; // VIPER #3983
        _msgs->Insert("illegal use of parameter %s with unbounded value", "VERI-1781") ; // VIPER #5887
        _msgs->Insert("invalid expression in system call %s", "VERI-1782") ; // VIPER #5887

// 05/2010 added :
        _msgs->Insert("%s needs at least %d arguments", "VERI-1783") ; // VIPER #5248(vcs_compatibility: 9000387999)
        _msgs->Insert("space between colon and equal in dist operator violates IEEE 1800 syntax", "VERI-1784") ; // VIPER #5248(vcs_compatibility: 9000388571)
        _msgs->Insert("strength is not allowed in %s switch", "VERI-1785") ; // VIPER #5945
        _msgs->Insert("Verilog expression ignored due to errors", "VERI-1786") ; // Expression parsing support :
        _msgs->Insert("only subroutine call can be used in deferred assertion", "VERI-1787") ; // Action block of deferred assertion
        _msgs->Insert("continuing %s %s declaration in modport violates IEEE 1800 syntax", "VERI-1788") ; // VIPER #5248 (9000388871)

// 06/2010 added :
        _msgs->Insert("only simple module name as bind target are supported for rtl synthesis", "VERI-1789") ; // VIPER #5964
        _msgs->Insert("incorrect use of macro %s, argument '%s' was not closed", "VERI-1790") ; // VIPER #6001
        _msgs->Insert("root scope declaration is not allowed in verilog 95/2K mode", "VERI-1791") ; // VIPER #5979
        _msgs->Insert("extra braces around constraint expressions violates IEEE 1800 syntax", "VERI-1792") ; // VIPER #5856
        _msgs->Insert("cannot assign to analog net %s", "VERI-1793") ; // VIPER #6002

// 07/2010 added :
        _msgs->Insert("parameter %s has no actual or default value", "VERI-1794") ; // SV-2009 feature (parameter with no default value)
        _msgs->Insert("cycle delay in assignment only defined under default clock", "VERI-1795") ; // VIPER #4840
        _msgs->Insert("system function call %s with non-constant argument is not synthesizable", "VERI-1796") ; // VIPER #2637
        _msgs->Insert("replication is not allowed in unpacked array concatenation", "VERI-1797") ; // VIPER #6008 : SV-2009 : Unpacked array concat
        _msgs->Insert("wrong element type in unpacked array concatenation", "VERI-1798") ; // VIPER #6008 : SV-2009 : Unpacked array concat
        _msgs->Insert("illegal reference to program variable %s from outside program block", "VERI-1799") ; // VIPER #4820 (#4843)
        _msgs->Insert("references to the %s are not allowed in packages", "VERI-1800") ; // VIPER #6046
        _msgs->Insert("recursive `include via file %s", "VERI-1801") ; // VIPER #6044
        _msgs->Insert("previous error may be due to too many nested include file", "VERI-1802") ; // VIPER #6044
        _msgs->Insert("illegal blocking assignment of non-program variable %s", "VERI-1804") ; // VIPER #4495
        _msgs->Insert("illegal non-blocking assignment of program variable %s", "VERI-1805") ; // VIPER #4495

// 08/2010 added :
        _msgs->Insert("analog net %s is not valid in an expression", "VERI-1806") ; // VIPER #6091
        _msgs->Insert("cannot replace genvar expression like genvar_ref[non_const_index]", "VERI-1807") ; // VIPER #6095

// 09/2010 added :
        _msgs->Insert("name conflict during elaboration for %s", "VERI-1808") ; // VIPER #6080
        _msgs->Insert("automatic variable %s is not allowed in procedural continuous assignments", "VERI-1809") ; // VIPER #6110
        _msgs->Insert("genvar %s is already used in this scope", "VERI-1810") ; // VIPER #6081
        _msgs->Insert("illegal specify input/output terminal descriptor in PATHPULSE", "VERI-1811") ; // VIPER #6119
        _msgs->Insert("parameter %s is already connected", "VERI-1812") ; // VIPER #6129
        _msgs->Insert("illegal %s port in function %s used in procedural continuous assignment statement", "VERI-1813") ; // VIPER #4606
        _msgs->Insert("illegal %s port in function %s used in event expression", "VERI-1814") ; // VIPER #4606
        _msgs->Insert("dynamic range is not allowed in this mode of verilog", "VERI-1815") ; // VIPER #6153

// 10/2010 added :
        _msgs->Insert("real type port is not allowed in this mode of verilog", "VERI-1816") ; // VIPER #6177

// 11/2010 added :
        _msgs->Insert("parameter initial value cannot be omitted in this mode of verilog", "VERI-1817") ; // VIPER #6222
        _msgs->Insert("initial value of parameter %s is omitted", "VERI-1818") ; // VIPER #6222

// 12/2010 added :
        _msgs->Insert("digits must follow decimal point in real %s", "VERI-1819") ; // VIPER #6271

// 01/2011 added :
        _msgs->Insert("expression size is larger than 2**%d bits", "VERI-1820") ; // VIPER Issue #6312

// 02/2011 added :
        _msgs->Insert("modport item %s must refer to an item in interface %s", "VERI-1821") ; // VIPER Issue #6344
        _msgs->Insert("type must be specified for local variable formal argument", "VERI-1822") ; // VIPER Issue #6333
        _msgs->Insert("keyword local is missing for port item with direction", "VERI-1823") ; // VIPER Issue #6333
        _msgs->Insert("default actual argument for inout/output port %s is not allowed", "VERI-1824") ; // VIPER Issue #6334
        _msgs->Insert("illegal type for local variable formal argument", "VERI-1825") ; // VIPER Issue #6336
        _msgs->Insert("clocking event not allowed in clocking block", "VERI-1826") ; // VIPER Issue #6369
        _msgs->Insert("checker instance is not allowed in fork-join block", "VERI-1827") ; // VIPER Issue #5611
        _msgs->Insert("checker cannot be elaborated by itself", "VERI-1828") ; // VIPER Issue #5611

// 03/2011 added :
        _msgs->Insert("empty arguments are allowed only in system verilog", "VERI-1829") ; // VIPER Issue #6395
        _msgs->Insert("unbounded range is not allowed with %s operator", "VERI-1830") ; // VIPER Issue #6352
        _msgs->Insert("error in protected region near %s", "VERI-1831") ; // VIPER #6071
        _msgs->Insert("%s %s is deprecated in this mode of verilog", "VERI-1832") ; // VIPER #6409 & #6558
        _msgs->Insert("Preprocessed output written into file %s", "VERI-1833") ; // Verilog pre-processor
        _msgs->Insert("unknown port direction %s found for port %s. Assume input", "VERI-1834") ;
        _msgs->Insert("unknown signal type %s found for added signal %s. Assume wire", "VERI-1835") ;
        _msgs->Insert("cannot omit port identifier for this declarations", "VERI-1836") ; // VIPER #6423
        _msgs->Insert("first argument of $%s is invalid, expecting 0, 1 or 2", "VERI-1837") ; // VIPER #6428
        _msgs->Insert("multiple dimension class instance not supported for synthesis", "VERI-1838") ; // VIPER #6446
        _msgs->Insert("initial value not honored for array class instance", "VERI-1839") ; // VIPER #6446

// 04/2011 added :
        _msgs->Insert("modport %s contains interface method, using full interface", "VERI-1840") ; // VIPER #6461
        _msgs->Insert("the system function %s not sensitive to clock", "VERI-1841") ; // VIPER #6482
        _msgs->Insert("ignoring incorrect/unknown option: %s", "VERI-1842") ; // -f file processing

// 05/2011 added :
        _msgs->Insert("system function call %s inside abort function should have explicit clock", "VERI-1843") ; // #VIPER 6491
        _msgs->Insert("no clock inferred, multiple event control found", "VERI-1844") ; // VIPER #6477
        _msgs->Insert("no clock inferred, delay control found", "VERI-1845") ; // VIPER #6477
        _msgs->Insert("no clock inferred, event control id used in procedural code", "VERI-1846") ; // VIPER #6477
        _msgs->Insert("no clock inferred, multiple inferred clock", "VERI-1847") ; // VIPER #6477

// 06/2011 added :
        _msgs->Insert("empty parameter assignment not allowed", "VERI-1848") ; // VIPER #6591
        //_msgs->Insert("not converting statement without event control", "VERI-1850") ; // VIPER #6550: commented out for VIPER #7422
        _msgs->Insert("multiple clocking is not yet supported", "VERI-1851") ; // VIPER #6550
        _msgs->Insert("possible infinite loop detected", "VERI-1853") ; // VIPER #6550, VIPER #7805: Change message info
        // _msgs->Insert("single event control, no need to convert to explicit state machine", "VERI-1854") ; // VIPER #6550: commented out for VIPER #7422
        _msgs->Insert("%s is not assigned in analog event control", "VERI-1855") ; // VIPER #6571/6572

// 07/2011 added :
        _msgs->Insert("inside operator not allowed with casex or casez", "VERI-1856") ; // VIPER #6233 related
        _msgs->Insert("ignoring non-constant initial value of static variable %s", "VERI-1859") ; // VIPER #6596 & #6629
        _msgs->Insert("forward typedef %s is already fully defined", "VERI-1860") ; // VIPER #6609 (test1.sv)
        _msgs->Insert("use of %s #(params) here violates IEEE 1800 syntax", "VERI-1861") ; // VIPER #6609 (test2.sv)
        _msgs->Insert("only trireg nets can have a charge strength specification", "VERI-1862") ; // VIPER #6600
        // _msgs->Insert("multiple dimensions in gate instantiation ignored", "VERI-1863") ; // VIPER #6598 // retired with VIPER 7679: new message VERI-1960
        _msgs->Insert("invalid size of integer constant literal", "VERI-1864") ; // VIPER #6601
        _msgs->Insert("only one terminal allowed as the source or destination of parallel paths", "VERI-1865") ; // VIPER #6669

// 08/2011 added :
        _msgs->Insert("%s is not a predefined system timing check", "VERI-1866") ; // VIPER #6674
        _msgs->Insert("fixed width part select is not allowed in verilog 95 mode", "VERI-1867") ; // VIPER #6691
        _msgs->Insert("illegal use of x or z character in decimal number", "VERI-1868") ; // VIPER #6715
        _msgs->Insert("multiple definitions of global clocking are not allowed", "VERI-1869") ; // VIPER #6736
        _msgs->Insert("global clocking referenced without declaration", "VERI-1870") ; // VIPER #6738

// 09/2011 added :
        _msgs->Insert("module %s having generic interface port(s) (%s) cannot be elaborated by itself", "VERI-1871") ; // VIPER #6594 (issue 13) (updated for VIPER #7324)
        _msgs->Insert("rand join production control must have at least two production items", "VERI-1872") ; // VIPER #6758

// 10/2011 added :
        _msgs->Insert("compilation unit has not been closed before this analyze call", "VERI-1873") ; // VIPER #6790
        _msgs->Insert("assignment to non clocking block signal/variable %s, not allowed in cycle delay intra assignment", "VERI-1874") ; // VIPER #6781
        _msgs->Insert("identifier %s is used before its declaration", "VERI-1875") ; // VIPER #4379, 5609, 2775, 3121, 5712, 6151, 6599, 3503, 5604

// 11/2011 added :
        _msgs->Insert("use casting to use streaming concatenation with other operators", "VERI-1876") ; // VIPER #6876
        _msgs->Insert("assertion operator %s is not yet supported", "VERI-1877") ; // VIPER #6785
        _msgs->Insert("non consecutive repeat and goto repeat are not supported", "VERI-1878") ; // VIPER #6785
        _msgs->Insert("infinite time sequences are not supported", "VERI-1879") ; // VIPER #6785
        _msgs->Insert("parameterized expression is not supported, use literals", "VERI-1880") ; // VIPER #6785
        _msgs->Insert("library %s is not present", "VERI-1881") ; // VIPER #6785
        _msgs->Insert("bad operand to set membership operator", "VERI-1882") ; // VIPER #6894
        _msgs->Insert("cannot find mapped module %s, falling back to default evaluation", "VERI-1883") ; // VIPER #6898
        _msgs->Insert("cannot find return_port_name %s in module %s, falling back to default evaluation", "VERI-1884") ; // VIPER #6898
        _msgs->Insert("cannot map %s having output port to module %s in sequential area", "VERI-1885") ; // VIPER #6898
        _msgs->Insert("cannot map return_port_name of a %s, falling back to default evaluation", "VERI-1886") ; // VIPER #6898

// 12/2011 added :
        _msgs->Insert("cross language complex defparam, not supported yet", "VERI-1887") ; // VIPER #6909
        _msgs->Insert("incorrect number of dimensions in instantiation", "VERI-1888") ; // VIPER #6895
        _msgs->Insert("number of unpacked dimensions %d does not match the number of dimensions %d of VHDL unconstrained array", "VERI-1889") ; // VIPER #6895
        _msgs->Insert("illegal to define packed dimensions for vhdl unconstrained arrays", "VERI-1890") ; // VIPER #6895

// 01/2012 added :
        _msgs->Insert("illegal actual for event/sequence/property formal %s", "VERI-1891") ; // VIPER #6785
        _msgs->Insert("ignore unsupported data type %s during package conversion", "VERI-1892") ; // VIPER #6896
        _msgs->Insert("type %s is used before its declaration", "VERI-1893") ; // VIPER #5179
        _msgs->Insert("illegal reference of %s inside interface %s", "VERI-1894") ; // VIPER #6984
        _msgs->Insert("task or function %s with ref arguments must be automatic", "VERI-1895") ; // VIPER #6983
        _msgs->Insert("illegal index value for %s", "VERI-1896") ; // VIPER #6985
        _msgs->Insert("dynamic range is not allowed in net declaration", "VERI-1897") ; // VIPER #6996
        _msgs->Insert("ground %s should have continuous discipline", "VERI-1898") ;

// 02/2012 added :
        _msgs->Insert("behavior inside always_comb block does not represent combinational logic", "VERI-1899") ; // VIPER Issue #6948
        _msgs->Insert("cannot instantiate object of virtual class %s", "VERI-1900") ; // VIPER Issue #6994
        _msgs->Insert("recursive instantiation of property/sequence is not supported", "VERI-1901") ; // VIPER Issue #6785
        _msgs->Insert("width of actual %d differs from width of formal %d", "VERI-1902") ; // VIPER #6991: Changed from VERI-1210 for Error
        _msgs->Insert("width of actual %d differs from width of formal %d for port %s", "VERI-1903") ; // VIPER #6991: Changed from VERI-1330 for Error
        _msgs->Insert("TBDM: cannot remove %s having multiple declarations with multiple ids", "VERI-1904") ; // VIPER #7036

// 03/2012 added :
        _msgs->Insert("converting %s to %s", "VERI-1905") ; // VIPER Issue #7070

// 04/2012 added :
        _msgs->Insert("net type must be explicitly specified for '%s' when default_nettype is none", "VERI-1906") ; // VIPER Issue #7126
        _msgs->Insert("previous definition of module %s is static elaborated, cannot overwrite", "VERI-1907") ; // VIPER Issue #7141
        _msgs->Insert("no associated actual for formal %s", "VERI-1908") ; // VIPER Issue #7148

// 05/2012 added :
        _msgs->Insert("cannot create %s due to mkdir failure", "VERI-1909") ; // For PrettyPrintDesign()
        _msgs->Insert("invalid operator %s for psl expression", "VERI-1910") ; // For PSL
        _msgs->Insert("can only use boolean expressions as condition for %s", "VERI-1911") ; // For PSL
        _msgs->Insert("cannot elaborate PSL unit %s by itself", "VERI-1912") ; // For PSL
        _msgs->Insert("illegal instantiation: %s %s cannot be instantiated in this way", "VERI-1913") ; // VIPER #7210

// 06/2012 added :
        _msgs->Insert("package export %s::%s failed, name not found", "VERI-1914") ; // VIPER #7232
        _msgs->Insert("package export %s::%s failed, name locally declared", "VERI-1915") ; // VIPER #7232
        //_msgs->Insert("not converting statement with no event or edged expression", "VERI-1916") ; // VIPER #6667: commented out for VIPER #7422
        _msgs->Insert("wait statement must be followed by event control statement", "VERI-1917") ; // VIPER #6667

// 08/2012 added :
        _msgs->Insert("illegal case item comparison of %s type with %s type", "VERI-1918") ; // VIPER #7331
        _msgs->Insert("non-void function %s called as a task without void casting", "VERI-1919") ; // VIPER #7368
        _msgs->Insert("all elements of structure are not type equivalent, cannot use multi assignment pattern", "VERI-1920") ; // VIPER #7104
        _msgs->Insert("not converting statement with edged named event", "VERI-1921") ; // VIPER #6667
        _msgs->Insert("not converting statement with mixture of clocking and wait event", "VERI-1922") ; // VIPER #6667
        _msgs->Insert("function/task declaration in root scope violates verilog AMS syntax", "VERI-1923") ; // VIPER #7406

// 09/2012 added :
        _msgs->Insert("variable %s is driven by invalid combination of procedural drivers", "VERI-1924") ; // VIPER #4241
        _msgs->Insert("only named parameter assignment is allowed in use clause", "VERI-1925") ; // VIPER #7416
        _msgs->Insert("only local parameter declarations are allowed in configuration", "VERI-1926") ; // VIPER #7416

// 10/2012 added :
        _msgs->Insert("port %s remains unconnected for this instance", "VERI-1927") ; // VIPER #7466: Similar to VERI-1453 but for warning only

// 11/2012 added :
        _msgs->Insert("module %s does not have a parameter named %s to override", "VERI-1928") ; // VIPER #7466: Similar to VERI-1184 but for warning only
        _msgs->Insert("struct/union element %s is already declared", "VERI-1929") ; // VIPER #7487:
        _msgs->Insert("select index %d into %s is out of bounds", "VERI-1930") ; // VIPER #6991: Similar to VERI-1452 but for warning message only
        _msgs->Insert("potential always loop found", "VERI-1931") ; // VIPER #7506: Warn for potential always loop: possible infinite loop
        _msgs->Insert("illegal interface based typedef", "VERI-1932") ; // VIPER #7508:
        _msgs->Insert("hierarchical name in type reference is not allowed", "VERI-1933") ; // VIPER #7508:

// 12/2012 added :
        // VIPER #7559: Add Psl specific messages in the message table:
        _msgs->Insert("high bound of the range must be a statically computable number greater or equal to low bound", "VERI-1934") ;
        _msgs->Insert("must be a finite range", "VERI-1935") ;
        _msgs->Insert("range low bound must be a statically computable positive number", "VERI-1936") ;
        _msgs->Insert("range low bound must be a statically computable non-negative number", "VERI-1937") ;
        _msgs->Insert("count must be a statically computable positive number", "VERI-1938") ;
        _msgs->Insert("count must be a statically computable non-negative number", "VERI-1939") ;
        _msgs->Insert("count must be statically computable integer expression", "VERI-1940") ;
        _msgs->Insert("can only use boolean expressions", "VERI-1941") ;
        _msgs->Insert("can only use boolean expressions with union operator", "VERI-1942") ;
        _msgs->Insert("can only use boolean expressions as condition", "VERI-1943") ;
        _msgs->Insert("can only use boolean expressions as terminating condition", "VERI-1944") ;
        _msgs->Insert("can only use a boolean expressions as condition", "VERI-1945") ;
        _msgs->Insert("Fairness directives are not supported", "VERI-1946") ;
        _msgs->Insert("elaboration of directive failed", "VERI-1947") ;
        _msgs->Insert("VeriPslId::Assign called with df = 0", "VERI-1948") ;
        _msgs->Insert("this PSL directive not supported", "VERI-1949") ;
        _msgs->Insert("prev() can't be used on psl expressions", "VERI-1950") ;
        _msgs->Insert("unterminated string literal continues onto next line", "VERI-1951") ; // VIPER #7527
        _msgs->Insert("range must be bounded by constant expressions", "VERI-1952") ; // VIPER #7581
        _msgs->Insert("type identifier %s is not visible from module %s, pretty printed output will be illegal", "VERI-1953") ; // VIPER #7601
        _msgs->Insert("parameter declaration is not allowed here in this mode of verilog", "VERI-1954") ; // VIPER #7603

// 01/2013 added :
        _msgs->Insert("case %sequality operator always evaluates to %s due to comparison with x or z", "VERI-1955") ; // VIPER #7632
        _msgs->Insert("overwriting previous value of parameter %s", "VERI-1956") ; // VIPER #7571
        _msgs->Insert("Verilog module '%s' not found in SQL database '%s'; cannot restore", "VERI-1957") ;
        // obsolete : _msgs->Insert("parameter overwriting from configuration is not yet supported in static elaboration", "VERI-1958") ; // VIPER #7416

// 02/2013 added :
        _msgs->Insert("%s shift count >= width of value", "VERI-1959") ; // VIPER #7698
        _msgs->Insert("multiple dimensions in %s instantiation is not allowed in this mode of Verilog", "VERI-1960") ; // VIPER #7679

// 03/2013 added :
        _msgs->Insert("resolution function %s must be automatic", "VERI-1961") ; // SV 1800-2012: section 6.6.7
        _msgs->Insert("resolution function %s must have single input argument", "VERI-1962") ; // SV 1800-2012: section 6.6.7
        _msgs->Insert("argument %s of resolution function %s must be dynamic array", "VERI-1963") ; // SV 1800-2012: section 6.6.7
        _msgs->Insert("argument %s of resolution function %s must be an input", "VERI-1964") ; // SV 1800-2012: section 6.6.7
        _msgs->Insert("%s is not a valid resolution function for type %s : %s", "VERI-1965") ; // SV 1800-2012: section 6.6.7
        // obsolete : _msgs->Insert("generic/parameter override using veri_file::ElaborateMixedHdlMultipleTopsStatic routine is not supported yet", "VERI-1966") ; // VIPER #7727
        _msgs->Insert("previous declaration of %s is from here", "VERI-1967") ; // VIPER #7773
        _msgs->Insert("saving module %s.%s into %s", "VERI-1968") ; // SQLManager support
        _msgs->Insert("module %s not found in library %s", "VERI-1969") ; // SQLManager support
        _msgs->Insert("ignoring %s statement, set runtime flag veri_convert_assume_and_cover_statement to synthesize it", "VERI-1970") ; // VIPER #7744

// 04/2013 added :
        _msgs->Insert("%s driven by this %s block should not be driven by any other process", "VERI-1971") ; // VIPER #7825
        _msgs->Insert("another %s driver of %s is from here", "VERI-1972") ; // VIPER #7825
        _msgs->Insert("method triggered/matched is not allowed in argument of sampled value function", "VERI-1973") ; // VIPER #7834
        _msgs->Insert("use of local variable %s in sampled value function is illegal", "VERI-1974") ; // VIPER #7834

// 05/2013 added :
        _msgs->Insert("modport port %s does not support declared direction %s", "VERI-1975") ; // VIPER #7889
        _msgs->Insert("clocking argument for %s is not of type event control", "VERI-1976") ; // VIPER #7882
        _msgs->Insert("task/function %s not exported/externed in interface %s", "VERI-1977") ; // VIPER #7891
        _msgs->Insert("export/import task/function %s is not defined", "VERI-1978") ; // VIPER #7915

// 06/2013 added :
        _msgs->Insert("illegal to use all zero replications in concat", "VERI-1979") ; // VIPER #7902

// 07/2013 added :
        _msgs->Insert("default value for %s is not allowed in extern method body when not specified in class", "VERI-1980") ; // VIPER #7973
        _msgs->Insert("mismatch in default argument value for %s, %s in prototype and %s in extern method body", "VERI-1981") ; // VIPER #7973
        _msgs->Insert("%s block cannot have a label both before and after the %s keyword", "VERI-1982") ; // VIPER #7978
        // obsolete : _msgs->Insert("parameter overwriting from configuration is not yet supported in RTL synthesis", "VERI-1983") ; // VIPER #3043
        _msgs->Insert("configuration has multiple instance clause for same instance", "VERI-1984") ; // VIPER #3043
        // obsolete : _msgs->Insert("changing array instances from configuration is not yet supported in RTL synthesis", "VERI-1985") ; // VIPER #3043
        _msgs->Insert("instance clause specifying hierarchical path cannot refer inside another configuration", "VERI-1986") ; // VIPER #3043

// 08/2013 added :
        _msgs->Insert("struct/union member field %s cannot be initialized at declaration", "VERI-1987") ; // VIPER #8052
        _msgs->Insert("empty statement in %s", "VERI-1988") ; // VIPER #8065

// 09/2013 added :
        _msgs->Insert("module instantiation cannot be converted to interface instantiation", "VERI-1989") ;
        _msgs->Insert("bit stream casting is only allowed on same size data", "VERI-1990") ; // VIPER #7301
        _msgs->Insert("invalid rhs in generate loop initialization assignment", "VERI-1991") ; // VIPER #8109

// 10/2013 added :
        _msgs->Insert("illegal randomize method", "VERI-1992") ;  // VIPER #8149
        _msgs->Insert("value of parameter %s cannot contain a hierarchical identifier", "VERI-1993") ;  // VIPER #8200

// 11/2013 added :
        _msgs->Insert("unsatisfiable sequence in assertion", "VERI-1994") ;  // VIPER #8232
        _msgs->Insert("unique/priority if/case is not full", "VERI-1995") ;  // VIPER #8177/2908
        _msgs->Insert("cannot find cell %s in logical library %s", "VERI-1996") ;  // VIPER #8183
        _msgs->Insert("cannot find cell %s in liblist", "VERI-1997") ;  // VIPER #8183
        _msgs->Insert("invalid use of genvar %s", "VERI-1998") ;  // VIPER #8157
// HierTree Elaboration Specific message added:
        _msgs->Insert("module %s does not have a port named %s", "VERI-1999") ; // VIPER #7466/Hier Tree Elab: Similar to VERI-1184 but for warning only for port
        _msgs->Insert("module does not expect any ports", "VERI-2000") ; // hier_tree
        _msgs->Insert("multiple dimensions in instance %s is not supported yet", "VERI-2001") ; // hier_tree
        _msgs->Insert("no name for scope owner: hier tree creation aborted", "VERI-2002") ; // hier_tree
        _msgs->Insert("analyze with runtime flag veri_create_name_for_unnamed_gen_block", "VERI-2003") ; // hier_tree
        _msgs->Insert("ignoring blackbox marking in configuration %s", "VERI-2004") ; // hier_tree
        _msgs->Insert("parameter port list is not allowed with .* in port list", "VERI-2005") ; // VIPER #8261
        _msgs->Insert("hier tree creation not supported for vhdl design, ignoring bind directive", "VERI-2006") ; // hier tree creation specific
        _msgs->Insert("bind target specified module %s is not defined", "VERI-2007") ; // hier tree creation specific
        _msgs->Insert("incompatible assignment", "VERI-2008") ; // VIPER #8240
        _msgs->Insert("each member expression of assignment pattren should have a bitstream datatype", "VERI-2009") ; // VIPER #8240
        _msgs->Insert("element of assignment pattern has number of bits %d; expected %d", "VERI-2010") ; // VIPER #8240

// 12/2013 added :
        _msgs->Insert("checker formal argument '%s' may not be of interface type", "VERI-2011") ; // LRM 2009, 17.2
        _msgs->Insert("type parameter %s cannot be overridden with a defparam statement", "VERI-2012") ; // VIPER #8273
        _msgs->Insert("variable %s must explicitly be declared as automatic or static", "VERI-2013") ; // VIPER #8339
        _msgs->Insert("generic interconnect may specify at most one delay value", "VERI-2014") ;
        _msgs->Insert("generic interconnect must not specify any drive strength", "VERI-2015") ;
        _msgs->Insert("generic interconnect must not specify any data type", "VERI-2016") ;
        _msgs->Insert("generic interconnect must not specify initial assignment expression", "VERI-2017") ;

// 01/2014 added :
        _msgs->Insert("type reference expression may not have hierarchical references", "VERI-2018") ; // VIPER #8346
        _msgs->Insert("type reference expression may not have references to elements of dynamic objects", "VERI-2019") ; // VIPER #8346
        _msgs->Insert("use of char as keyword violates IEEE 1800 syntax", "VERI-2020") ; // VIPER #8350
        _msgs->Insert("connection type is incompatible with formal type for port %s in instance %s", "VERI-2021") ; // VIPER #8255
        _msgs->Insert("task/function %s is not exported/externed in interface %s", "VERI-2022") ; // VIPER #8375
        _msgs->Insert("parameter declared inside generate block shall be treated as localparam", "VERI-2023") ; // 1800-2012 section 27.2

// 02/2014 added :
        _msgs->Insert("empty range with constant bounds, low-bound and high-bound may be reversed", "VERI-2024") ; // VIPER #8364
        _msgs->Insert("connection to port %s has dimension of width %d, but instance array dimension is %d", "VERI-2025") ; // VIPER #8379
        _msgs->Insert("array port %s is of size %d, must be passed an identical array", "VERI-2026") ; // VIPER #8416
        _msgs->Insert("named elements cannot be used in %s used as left hand side of assignment-like context", "VERI-2027") ; // VIPER #8407
        _msgs->Insert("verilog expression cannot be converted to vhdl expression", "VERI-2028") ; // hier_tree

// 03/2014 added :
        _msgs->Insert("real data type is not a legal type of an associative array index", "VERI-2029") ;
        _msgs->Insert("index %s is out of range [%d:%d] for %s", "VERI-2030") ; // Viper #8479
        _msgs->Insert("%s is not enabled in this product", "VERI-2031") ; // VIPER #8515
        _msgs->Insert("illegal nested interface class %s within %s", "VERI-2032") ; // Interface class (SV 2012)
        _msgs->Insert("illegal extends, class %s may not extend an interface class", "VERI-2033") ; // Interface class (SV 2012)
        _msgs->Insert("only interface class can be referenced in %s clause", "VERI-2034") ; // Interface class (SV 2012)
        _msgs->Insert("illegal use of new with interface/abstract class", "VERI-2035") ; // Interface class (SV 2012)
        _msgs->Insert("pure virtual method %s can exist only in abstract class", "VERI-2036") ; // Interface class (SV 2012)
        _msgs->Insert("%s %s in subclass %s cannot override %s in superclass", "VERI-2037") ; // Interface class (SV 2012)
        _msgs->Insert("formal count of virtual method %s does not match prototype", "VERI-2038") ; // Interface class (SV 2012)
        _msgs->Insert("type of argument %s for virtual method %s in subclass %s does not match the type of argument in superclass", "VERI-2039") ; // Interface class (SV 2012)
        _msgs->Insert("class %s must implement/redeclare %s from interface/base classes", "VERI-2040") ; // Interface class (SV 2012)
        _msgs->Insert("the name %s must be defined in %s due to having conflicting visibility in multiple base classes", "VERI-2041") ; // Interface class (SV 2012)
        _msgs->Insert("call to rand_mode/constraint_mode on interface class handle is illegal", "VERI-2042") ; // Interface class (SV 2012)
        _msgs->Insert("elements must have same number of bits", "VERI-2043") ; // Interface class (SV 2012)
        _msgs->Insert("return type for virtual method %s in class %s is not type equivalent to the superclass method", "VERI-2044") ; // Interface class (SV 2012)
        _msgs->Insert("argument name %s for virtual method %s in subclass %s does not match the argument name %s in superclass", "VERI-2045") ; // Interface class (SV 2012)
    }

    return (const char *)_msgs->GetValue(msg) ;
}

/*static*/ void
VeriNode::ResetMessageMap()
{
    delete _msgs ;
    _msgs = 0 ;
}

void
VeriNode::SetRelaxedChecking(unsigned on)
{
    // flip 'relaxed' language checking mode on or off
    // This mode is to accept contructs beyond that LRM, to match 'forgiving' simulators.
    if (!on) {
        delete _relaxed_msgs ;
        _relaxed_msgs = 0 ;
        return ;
    }

    if (_relaxed_msgs) return ;

    // Create the relaxed_msg table
    _relaxed_msgs = new Map(STRING_HASH, 103) ; // Table size : choose number large enough to store all 'relaxed' messages

    // Insert all messages (msg_id->message_mode) that need to be relaxed.
    // Typically, we still issue 'warning' for LRM violations, but customers may reduce this to 'ignore'.
    msg_type_t default_relaxed_type = VERIFIC_WARNING ;
    msg_type_t ignore_relaxed_type = VERIFIC_IGNORE ;

// Superset parsing issues (messages replace previous 'syntax error') :
    _relaxed_msgs->Insert("VERI-1663", (void*)default_relaxed_type) ;
//    _relaxed_msgs->Insert("VERI-1664", (void*)default_relaxed_type) ; // Message id does not exist
//    _relaxed_msgs->Insert("VERI-1665", (void*)default_relaxed_type) ; // Message id does not exist
//    _relaxed_msgs->Insert("VERI-1666", (void*)default_relaxed_type) ; // Message id does not exist
//    _relaxed_msgs->Insert("VERI-1667", (void*)default_relaxed_type) ; // Message id does not exist
    _relaxed_msgs->Insert("VERI-1668", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1669", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1670", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1671", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1672", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1673", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1674", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1675", (void*)default_relaxed_type) ; // Message commented out
    _relaxed_msgs->Insert("VERI-1676", (void*)default_relaxed_type) ;
// Semantic check messages downgraded (most of these used to be 'error') :
    _relaxed_msgs->Insert("VERI-1148", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1165", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1223", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1293", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1347", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1349", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1419", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1427", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1428", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1430", (void*)default_relaxed_type) ;
//    _relaxed_msgs->Insert("VERI-1534", (void*)default_relaxed_type) ; // Added later, duplicate removed
    _relaxed_msgs->Insert("VERI-1016", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1015", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1014", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1158", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1063", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1066", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1060", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1070", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1071", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1222", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1199", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1206", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1212", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1214", (void*)ignore_relaxed_type) ; //Viper 5301 verify_init_task_ref_aa
    _relaxed_msgs->Insert("VERI-1215", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1225", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1233", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1248", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1251", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1264", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1265", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1289", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1211", (void*)default_relaxed_type) ;
    // _relaxed_msgs->Insert("VERI-1335", (void*)default_relaxed_type) ; // VIPER 4639 shows that this one should be an error
    _relaxed_msgs->Insert("VERI-1339", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1343", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1348", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1353", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1360", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1362", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1376", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1377", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1381", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1383", (void*)default_relaxed_type) ; // DD: 08/11: Always invoked as warning, the tool in question however produces error! same as VERI-1867.
    _relaxed_msgs->Insert("VERI-1387", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1216", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1401", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1403", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1411", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1415", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1420", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1198", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1313", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1314", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1316", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1444", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1557", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1558", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1622", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1439", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1736", (void*)default_relaxed_type) ;
    _relaxed_msgs->Insert("VERI-1463", (void*)default_relaxed_type) ; // VIPER #5602 : should be warning
    _relaxed_msgs->Insert("VERI-1615", (void*)default_relaxed_type) ; // VIPER #5248 (test_25) : should be warning
    _relaxed_msgs->Insert("VERI-1535", (void*)default_relaxed_type) ; // VIPER #5301 (test_26) : should be warning
    _relaxed_msgs->Insert("VERI-1597", (void*)default_relaxed_type) ; // VIPER #5301 (test_288) : should be warning
    _relaxed_msgs->Insert("VERI-1746", (void*)default_relaxed_type) ; // VIPER #5301 (test_41) : should be warning
    _relaxed_msgs->Insert("VERI-1750", (void*)default_relaxed_type) ; // VIPER #5301 (test_47) : should be warning
    _relaxed_msgs->Insert("VERI-1539", (void*)default_relaxed_type) ; // VIPER #5301 (test43) : should be warning
    _relaxed_msgs->Insert("VERI-1323", (void*)default_relaxed_type) ; // VIPER #5301 (test40) : should be warning
    _relaxed_msgs->Insert("VERI-1580", (void*)default_relaxed_type) ; // VIPER #5301 (test37) : should be warning
    _relaxed_msgs->Insert("VERI-1542", (void*)default_relaxed_type) ; // VIPER #5301 (test34) : should be warning
    _relaxed_msgs->Insert("VERI-1540", (void*)default_relaxed_type) ; // VIPER #5301 (test33) : should be warning
    _relaxed_msgs->Insert("VERI-1648", (void*)default_relaxed_type) ; // VIPER #5301 (test31) : should be warning
    _relaxed_msgs->Insert("VERI-1532", (void*)default_relaxed_type) ; // VIPER #5301 (test14) : should be warning
    _relaxed_msgs->Insert("VERI-1533", (void*)default_relaxed_type) ; // VIPER #5301 (test14) : should be warning
    _relaxed_msgs->Insert("VERI-1534", (void*)default_relaxed_type) ; // VIPER #5301 (test14) : should be warning
    _relaxed_msgs->Insert("VERI-1636", (void*)default_relaxed_type) ; // VIPER #5301 (test23) : should be warning
    _relaxed_msgs->Insert("VERI-1262", (void*)default_relaxed_type) ; // VIPER #5301 (test6) : should be warning
    _relaxed_msgs->Insert("VERI-1683", (void*)default_relaxed_type) ; // VIPER #5301 (unique_index) : should be warning
    _relaxed_msgs->Insert("VERI-1524", (void*)default_relaxed_type) ; // VIPER #5301 (test17) : should be warning
    _relaxed_msgs->Insert("VERI-1442", (void*)default_relaxed_type) ; // VIPER #5301 (test17) : should be warning
    _relaxed_msgs->Insert("VERI-1716", (void*)default_relaxed_type) ; // VIPER #5301 (nonauto_func_in_const) : should be warning
    _relaxed_msgs->Insert("VERI-1655", (void*)default_relaxed_type) ; // VIPER #5301 (rand_mode) : should be warning
    _relaxed_msgs->Insert("VERI-1437", (void*)default_relaxed_type) ; // VIPER #5717 (test3.sv) : should be warning
    _relaxed_msgs->Insert("VERI-1684", (void*)default_relaxed_type) ; // VIPER #5301 (time_unit) : should be warning
    _relaxed_msgs->Insert("VERI-1196", (void*)default_relaxed_type) ; // VIPER #5301 (test41) : should be warning
    // VIPER #5710: According to LRM: IEEE P1800(2009) property instance is not allowed in sequence expression,
    // but for compatibility we allow property instance in sequence expression in relaxed checkin mode:
    _relaxed_msgs->Insert("VERI-1315", (void*)default_relaxed_type) ; // VIPER #5710 (property_hyphen) : should be warning
    _relaxed_msgs->Insert("VERI-1226", (void*)default_relaxed_type) ; // VIPER #5992 : should be warning
    _relaxed_msgs->Insert("VERI-1815", (void*)default_relaxed_type) ; // VIPER #6153 : should be warning
    _relaxed_msgs->Insert("VERI-1868", (void*)default_relaxed_type) ; // VIPER #6715 : should be warning in relaxed mode
    _relaxed_msgs->Insert("VERI-1906", (void*)ignore_relaxed_type) ;  // VIPER #7126 : violation ignored in relaxed mode
    _relaxed_msgs->Insert("VERI-1184", (void*)default_relaxed_type) ; // VIPER #7466 : should be warning in relaxed mode
}

/*static*/ void
VeriNode::ResetRelaxedMessageMap()
{
    delete _relaxed_msgs ;
    _relaxed_msgs = 0 ;
}

msg_type_t
VeriNode::GetRelaxedMessageType(const char *msg_id, msg_type_t orig_msg_type)
{
    if (!_relaxed_msgs || !msg_id) return orig_msg_type ;
    msg_type_t msg_type = (enum msg_type_t)(unsigned long)_relaxed_msgs->GetValue(msg_id) ;
    if (msg_type) return msg_type ;
    return orig_msg_type ;
}

// VIPER #6640: Move all built-in type size/sign info into one place.
/* static */ unsigned
VeriNode::GetSizeOfBuiltInType(unsigned type, unsigned *sign /*=0*/)
{
    if (!type) return 0 ;

    // Default type : size 1 bit.
    // FIX ME : maybe should default to 0 now that we listed 1-bit types below.
    unsigned result = 1 ;
    unsigned my_sign = 0 ;

    // Multiply by the base type (if applicable) :
    switch (type) {
    // All net types are 1 bit :
    case VERI_WIRE      :
    case VERI_TRI       :
    case VERI_TRI1      :
    case VERI_SUPPLY0   :
    case VERI_WAND      :
    case VERI_TRIAND    :
    case VERI_TRIOR     :
    case VERI_TRI0      :
    case VERI_SUPPLY1   :
    case VERI_WOR       :
    case VERI_TRIREG    : result = 1 ; break ;
    case VERI_UWIRE     : result = 1 ; break ; /* Verilog 2005 addition*/

    // Register types   :
    case VERI_INTEGER   : result = 32 ; my_sign = 1 ; break ;
    case VERI_TIME      : result = 64 ; my_sign = 0 ; break ;
    // Real variables not really supported for synthesis, but I think these are 64 bit in Verilog ?
    case VERI_REAL      : // This is an intentional fall through
    case VERI_REALTIME  : result = 64 ; my_sign = 1 ; break ;

    // register single bit type :
    case VERI_REG       : result = 1 ; break ;
    // SV register bit types :
    case VERI_LOGIC     : result = 1 ; break ;
    case VERI_BIT       : result = 1 ; break ;
    // LRM 3.3 scalar data types
    case VERI_BYTE      : result = 8 ;  my_sign = 1 ; break ;
    case VERI_SHORTINT  : result = 16 ; my_sign = 1 ; break ;
    case VERI_INT       : result = 32 ; my_sign = 1 ; break ;
    case VERI_LONGINT   : result = 64 ; my_sign = 1 ; break ;
    case VERI_SHORTREAL : result = 32 ; my_sign = 1 ; break ;
    case VERI_CHAR      : result = 8 ;  my_sign = 1 ; break ; // SV 3.0 only
    case VERI_CHANDLE   : result = 8*(sizeof(void*)) ; break ; // a pointer...
    // Explicitly 0 bit data types :
    case VERI_VOID      : result = 0 ; break ;
    case VERI_STRINGTYPE: result = 0 ; break ; // dynamically allocated...

    default :
        break ; // a single-bit type
    }

    if (sign) *sign = my_sign ;
    return result ;
}

/* static */ void
VeriNode::SetStaticElab()
{
    _is_static_elab = 1 ;
}

