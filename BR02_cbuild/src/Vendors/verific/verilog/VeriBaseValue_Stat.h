/*
 *
 * [ File Version : 1.75 - 2014/01/13 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_BASE_VALUE_H_
#define _VERIFIC_VERI_BASE_VALUE_H_

#include <stdlib.h>
#include "VeriCompileFlags.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// Forward declarations
class VeriTreeNode ;
class VeriConst ;
class VeriIdDef ;
class VeriExpression ;
class VeriConstraint ;

enum veri_value_type
{
    INTEGER,
    REAL,
    BASEDNUM,
    ASCIISTR,
    ENUM,
    ARRAY,
    MINTYPMAX
} ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriBaseValue
{
public:
    // Accessor methods :
    virtual int                 GetIntegerValue() const = 0 ;          // Returns the integer value.
    virtual verific_uint64      Get64bitUnsigned() const = 0 ;         // Returns 64bit unsigned integer value.
    virtual verific_int64       Get64bitInteger() const = 0 ;          // Returns 64bit integer value.
    virtual char *              GetBinaryValue(unsigned *) const = 0 ; // Returns the binary value.
    virtual double              GetRealValue() const = 0 ;             // Returns the real value.
    virtual unsigned            GetSign() const = 0 ;                  // Returns sign of the value.
    virtual const char *        GetString() const = 0 ;                // Returns a const char *of the value.
    virtual VeriBaseValue *     GetMinimum() const      { return 0 ; } // Returns the minimum value for min-typ-max value.
    virtual VeriBaseValue *     GetTypical() const      { return 0 ; } // Returns the typical value for min-typ-max value.
    virtual VeriBaseValue *     GetMaximum() const      { return 0 ; } // Returns the maximum value for min-typ-max value.
    virtual Array *             GetValues() const       { return 0 ; } // Returns values for array type value
    virtual VeriIdDef *         GetEnumId() const       { return 0 ; } // Return enum literal for enum value
    virtual VeriBaseValue *     GetValue() const        { return 0 ; } // Return evaluated value of enum

    // Size-sign of a value
    virtual verific_int64       StaticSizeSign() const  { return 0 ; } // Virtual catcher

    // Type extraction routine :
    virtual veri_value_type     Type() const = 0 ; // Returns veri_value_type.

    // Value copy routine :
    virtual VeriBaseValue *     Copy() = 0 ; // Copy a value and returns the copied value.

    // Conversion routines :
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const = 0 ; // Construct expression from value.
    virtual char *              Image() const = 0 ; // Returns char* equivalent of value
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const = 0 ; // Construct ConstVal from value: needed for viper 2056
    virtual VeriBaseValue *     ToAsciiString() ; // VIPER #4045 : Convert to string value
    virtual VeriBaseValue *     TryConvertToInteger() ; // VIPER #5089: Try to convert to VeriInteger, if not possible, return 'this'
    virtual VeriBaseValue *     Increment(int by_number) = 0 ; // Increment the value by the given number

    // Utilities :
    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const = 0 ; // Returns '1' if 'this' and the argument specific value are equal.
    virtual unsigned            HasXZ() const = 0 ; // Returns 1 if value contains 'x'/'z'
    // VIPER #5503: Make Adjust routin virtual for overload in class VeriAsciiString:
    virtual VeriBaseValue *     Adjust(unsigned size, unsigned sign, unsigned use_self_sign, unsigned two_state) ; // Adjust size/sign of 'this' and return. If last argument is 1, second argument is ignored
    virtual VeriBaseValue *     GetValueAt(unsigned pos) ;
    virtual VeriBaseValue *     TakeValueAt(unsigned pos) ;
    virtual Array *             TakeValues() ;
    virtual void                SetValueAt(VeriBaseValue *v, unsigned pos) ;
    virtual unsigned            IsEmpty() const ;
    virtual unsigned            IsAnyFieldEmpty() const ;
    virtual VeriBaseValue *     ToArray(unsigned array_size, unsigned num_of_bits) ; // 'this' remains unchanged
    virtual VeriBaseValue *     ToBasedNum(unsigned preserve_arr) ; // delete 'this'
    virtual unsigned            Assign(VeriBaseValue *val, unsigned value_size, unsigned lsb_bit_offset) ;
    virtual VeriBaseValue *     Evaluate(unsigned value_size, unsigned lsb_bit_offset) ;
    virtual void                ImposeSignState(unsigned sign, unsigned is_2state) ;
    virtual void                SetIsStructure()  { }
    virtual unsigned            IsStructValue() const  { return 0 ; }
    virtual unsigned            GetStringSize() const  { return 0 ; }
    virtual unsigned            IsConcatValue() const  { return 0 ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    VeriBaseValue() ;

public:
    virtual ~VeriBaseValue() ;

private:
    // Prevent compiler from defining the following
    VeriBaseValue(const VeriBaseValue &) ;            // Purposely leave unimplemented
    VeriBaseValue& operator=(const VeriBaseValue &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    static VeriBaseValue *ConcatAsciiStrings(const Array *val_array, unsigned ret_size) ; // VIPER #7397: Concatenate ascii strings
    virtual VeriBaseValue *AdjustAndCheck(VeriConstraint *id_constraint, const VeriIdDef *id, const VeriTreeNode *from) ; // VIPER #8037
private:
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBaseValue

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriInteger : public VeriBaseValue
{
public:
    explicit VeriInteger(int i) ;
    explicit VeriInteger(const char *num) ;

    virtual veri_value_type     Type() const                    { return INTEGER ; }
    virtual int                 GetIntegerValue() const         { return atoi(_ival) ; }
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned*) const ;
    virtual VeriBaseValue *     Copy()                          { return new VeriInteger(_ival) ; }
    virtual unsigned            GetSign() const ;
    virtual const char *        GetString() const               { return _ival ; }
    virtual double              GetRealValue() const            { return atof(_ival) ; }
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual char *              Image() const ;
    virtual unsigned            HasXZ() const                   { return 0 ; }
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual VeriBaseValue *     ToAsciiString() ; // VIPER #4045 : Convert to string value
    virtual verific_int64       StaticSizeSign() const ;
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriInteger() ;

private:
    // Prevent compiler from defining the following
    VeriInteger() ;                               // Purposely leave unimplemented
    VeriInteger(const VeriInteger &) ;            // Purposely leave unimplemented
    VeriInteger& operator=(const VeriInteger &) ; // Purposely leave unimplemented

private:
    char *_ival ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriInteger

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriReal : public VeriBaseValue
{
public:
    explicit VeriReal(double d) ;
    explicit VeriReal(const char *d) ;

    virtual veri_value_type     Type() const                        { return REAL ; }
    virtual int                 GetIntegerValue() const ;
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned *) const    { return 0 ; }
    virtual unsigned            GetSign() const ;
    virtual double              GetRealValue() const ;
    virtual const char *        GetString() const                   { return _real ; }
    virtual verific_int64       StaticSizeSign() const ;

    virtual VeriBaseValue *     Copy()                              { return new VeriReal(_real) ; }

    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual char *              Image()  const;
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual VeriBaseValue *     ToAsciiString() ; // VIPER #4045 : Convert to string value
    virtual VeriBaseValue *     TryConvertToInteger() ; // VIPER #5089: Try to convert to VeriInteger, if not possible, return 'this'
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual unsigned            HasXZ() const                       { return 0 ; }
    virtual VeriBaseValue *     Evaluate(unsigned value_size, unsigned lsb_bit_offset) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriReal() ;

private:
    // Prevent compiler from defining the following
    VeriReal() ;                            // Purposely leave unimplemented
    VeriReal(const VeriReal &) ;            // Purposely leave unimplemented
    VeriReal& operator=(const VeriReal &) ; // Purposely leave unimplemented

private:
    char *_real ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriReal

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriBasedNumber : public VeriBaseValue
{
public:
    explicit VeriBasedNumber(const char *x) ;
    explicit VeriBasedNumber(verific_uint64 val) ;

    virtual veri_value_type     Type() const            { return BASEDNUM ; }

    virtual int                 GetIntegerValue() const ;
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned *) const ;
    virtual unsigned            GetSign() const ;
    virtual double              GetRealValue() const ;
    virtual const char *        GetString() const       { return _based_num ; }
    virtual verific_int64       StaticSizeSign() const ;

    virtual VeriBaseValue *     Copy()                  { return new VeriBasedNumber(_based_num) ; }

    virtual char *              Image()  const;
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual VeriBaseValue *     ToAsciiString() ; // VIPER #4045 : Convert to string value
    virtual VeriBaseValue *     TryConvertToInteger() ; // VIPER #5089: Try to convert to VeriInteger, if not possible, return 'this'
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual unsigned            HasXZ() const ;
    virtual unsigned            Assign(VeriBaseValue *val, unsigned value_size, unsigned lsb_bit_offset) ;
    virtual void                ImposeSignState(unsigned sign, unsigned is_2state) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriBasedNumber() ;

private:
    // Prevent compiler from defining the following
    VeriBasedNumber() ;                                   // Purposely leave unimplemented
    VeriBasedNumber(const VeriBasedNumber &) ;            // Purposely leave unimplemented
    VeriBasedNumber& operator=(const VeriBasedNumber &) ; // Purposely leave unimplemented

public:
private:
    char *_based_num ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBasedNumber

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriAsciiString : public VeriBaseValue
{
public:
    explicit VeriAsciiString(const char *s) ;
    VeriAsciiString(const char *s, unsigned len) ; // VIPER #7397 : Store string with length as string can have '\0'

    virtual veri_value_type     Type() const                { return ASCIISTR ; }

    virtual int                 GetIntegerValue() const ; //    { return atoi(_str) ; }
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned *) const ;
    virtual unsigned            GetSign() const             { return 0 ; }
    virtual double              GetRealValue() const ; //       { return atof(_str) ; }
    virtual const char *        GetString() const           { return _str ; }
    virtual verific_int64       StaticSizeSign() const ;

    virtual VeriBaseValue *     Copy()                      { return new VeriAsciiString(_str) ; }

    virtual char *              Image()  const;
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual VeriBaseValue *     Adjust(unsigned size, unsigned /*sign*/, unsigned /*use_self_sign*/, unsigned /*two_state*/) ; // VIPER #5503: Adjust size of 'this' and return.
    virtual VeriBaseValue *     ToAsciiString() ; // VIPER #4045 : Convert to string value
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual unsigned            HasXZ() const ;
    virtual unsigned            GetStringSize() const { return _size ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriAsciiString() ;

private:
    // Prevent compiler from defining the following
    VeriAsciiString() ;                                   // Purposely leave unimplemented
    VeriAsciiString(const VeriAsciiString &) ;            // Purposely leave unimplemented
    VeriAsciiString& operator=(const VeriAsciiString &) ; // Purposely leave unimplemented

private:
    char    *_str ;
    unsigned _size ; // Length of the string
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAsciiString

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriMinTypMaxValue : public VeriBaseValue
{
public:
    VeriMinTypMaxValue(VeriBaseValue *min, VeriBaseValue *typ, VeriBaseValue *max) ;

    virtual veri_value_type     Type() const            { return MINTYPMAX ; }

    virtual int                 GetIntegerValue() const ;
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned *) const ;
    virtual unsigned            GetSign() const ;
    virtual VeriBaseValue *     GetMinimum() const      { return _min ; } // Returns the minimum value.
    virtual VeriBaseValue *     GetTypical() const      { return _typ ; } // Returns the typical value.
    virtual VeriBaseValue *     GetMaximum() const      { return _max ; } // Returns the maximum value.
    virtual double              GetRealValue() const ;
    virtual const char *        GetString() const       { return 0 ; }
    virtual verific_int64       StaticSizeSign() const ;

    virtual VeriBaseValue *     Copy() ;

    virtual char *              Image()  const;
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual unsigned            HasXZ() const ;
    virtual void                ImposeSignState(unsigned sign, unsigned is_2state) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    explicit VeriMinTypMaxValue(VeriBaseValue *val) ;
    virtual ~VeriMinTypMaxValue() ;

private:
    // Prevent compiler from defining the following
    VeriMinTypMaxValue() ;                                      // Purposely leave unimplemented
    VeriMinTypMaxValue(const VeriMinTypMaxValue &) ;            // Purposely leave unimplemented
    VeriMinTypMaxValue& operator=(const VeriMinTypMaxValue &) ; // Purposely leave unimplemented

private:
   VeriBaseValue *_min ;
   VeriBaseValue *_typ ;
   VeriBaseValue *_max ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriMinTypMaxValue

/* -------------------------------------------------------------- */

/*
  Array type value, represent parameter array/struct-union type parameter.
*/
class VFC_DLL_PORT VeriArrayValue : public VeriBaseValue
{
public:
    explicit VeriArrayValue(Array *vals, unsigned is_concat=1) ;

    virtual veri_value_type     Type() const                     { return ARRAY ; }

    virtual int                 GetIntegerValue() const          ; // VIPER #2992
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned *) const ; // VIPER #2992
    virtual unsigned            GetSign() const                  { return 0 ; }
    virtual double              GetRealValue() const             { return 0 ; }
    virtual const char *        GetString() const                { return 0 ; }
    virtual Array *             GetValues() const                { return _values ; }
    virtual verific_int64       StaticSizeSign() const ;

    virtual VeriBaseValue *     Copy() ;

    virtual char *              Image()  const;
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual unsigned            HasXZ() const ;
    virtual VeriBaseValue *     GetValueAt(unsigned pos) ;
    virtual VeriBaseValue *     TakeValueAt(unsigned pos) ;
    virtual Array *             TakeValues() ;
    virtual void                SetValueAt(VeriBaseValue *v, unsigned pos) ;
    virtual unsigned            IsEmpty() const ;
    virtual unsigned            IsAnyFieldEmpty() const ;
    virtual VeriBaseValue *     ToArray(unsigned array_size, unsigned num_of_bits) ; // 'this' remains unchanged
    virtual VeriBaseValue *     ToBasedNum(unsigned preserve_arr) ; // delete 'this'
    virtual void                ImposeSignState(unsigned sign, unsigned is_2state) ;
    virtual void                SetIsStructure()  { _is_struct = 1 ; }
    virtual unsigned            IsStructValue() const  { return _is_struct ; }
    virtual unsigned            IsConcatValue() const  { return _is_concat ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriArrayValue() ;

private:
    // Prevent compiler from defining the following
    VeriArrayValue() ;                                  // Purposely leave unimplemented
    VeriArrayValue(const VeriArrayValue &) ;            // Purposely leave unimplemented
    VeriArrayValue& operator=(const VeriArrayValue &) ; // Purposely leave unimplemented

public :
    virtual VeriBaseValue *AdjustAndCheck(VeriConstraint *id_constraint, const VeriIdDef *id, const VeriTreeNode *from) ; // VIPER #8037
private:
    Array *_values ; // array of VeriBaseValue *, all of same type
    unsigned _is_concat:1 ; // flag showing whether the VeriArrayValue contains concat elements or assignment patterns
    unsigned _is_struct:1 ; // flag showing whether this is for structure (VIPER #5742)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriArrayValue

/* -------------------------------------------------------------- */

/*
   Class to represent enumeration literal
*/
class VFC_DLL_PORT VeriEnumVal : public VeriBaseValue
{
public:
    explicit VeriEnumVal(VeriIdDef *id) ;

    virtual veri_value_type     Type() const                    { return ENUM ; }
    virtual int                 GetIntegerValue() const ;
    virtual verific_uint64      Get64bitUnsigned() const ;
    virtual verific_int64       Get64bitInteger() const ;
    virtual char *              GetBinaryValue(unsigned*) const ;
    virtual double              GetRealValue() const ;
    virtual VeriBaseValue *     Copy()                          { return new VeriEnumVal(_id) ; }
    virtual unsigned            GetSign() const ;
    virtual const char *        GetString() const ;
    virtual VeriIdDef *         GetEnumId() const               { return _id ; }
    virtual VeriBaseValue *     GetValue() const ; // Return evaluated value of this enum
    virtual VeriExpression *    ToExpression(const VeriTreeNode *from) const ;
    virtual unsigned            IsEqual(const VeriBaseValue *val, unsigned do_not_check_type) const ;
    virtual char *              Image()  const;
    virtual unsigned            HasXZ() const ;
    virtual VeriConst *         ToConstVal(const VeriTreeNode *from) const ;
    virtual verific_int64       StaticSizeSign() const ;
    virtual VeriBaseValue *     Increment(int by_number) ; // Increment the value by the given number

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    virtual ~VeriEnumVal() ;

private:
    // Prevent compiler from defining the following
    VeriEnumVal() ;                               // Purposely leave unimplemented
    VeriEnumVal(const VeriEnumVal &) ;            // Purposely leave unimplemented
    VeriEnumVal& operator=(const VeriEnumVal &) ; // Purposely leave unimplemented

private:
    VeriIdDef *_id ; // Enum literal
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriEnumVal

/* -------------------------------------------------------------- */

/*
   Local class required in constant function evaluation. It basically
   contains a Map, and strores VeriIdDef *vs. VeriBaseValue. After
   executing each statement in a constant function this table gets
   updated and thereby evaluates the value of the function.
*/

class Set ;
class Map ;
class VeriIdDef ;

class VFC_DLL_PORT ValueTable
{
public:
    ValueTable() ;

    // Creates a VeriBaseValue with the strings and the indexes provided.
    VeriBaseValue *     CreateValue(char *lstr, const char *rstr, unsigned start, unsigned stop, unsigned msb, unsigned lsb, unsigned sign) const ;

    // Inserts a VeriBaseValue in the Map with id as a key.
    unsigned            Insert(VeriIdDef *id, VeriBaseValue *val) ;

    // Returns the VeriBaseValue from the Map against the key 'id'.
    VeriBaseValue *     FetchValue(const VeriIdDef *id) const ;
    // Removes and deletes the VeriBaseValue from the Map against the key 'id'
    unsigned            RemoveValue(const VeriIdDef *id) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    ~ValueTable() ;

private:
    // Prevent compiler from defining the following
    ValueTable(const ValueTable &) ;            // Purposely leave unimplemented
    ValueTable& operator=(const ValueTable &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Whenever we get a return, break or continue statement in functions or in some
    // loop etc the statements following it upto to 'endfunction'or the whole loop or
    // upto end of the loop should not be executed.
    // To accomplish this behavior we use the next two routines.
    // Whenever we encounter a return statement, we call SetJumpToken() with the
    // appropriate token of the type of the jump statement and every statement checks
    // whether it is inside dead code or not, if it is inside dead code - it does not execute.
    inline unsigned     GetJumpToken() const          { return _jump_token ; }
    void                SetJumpToken(unsigned token)  { _jump_token = token ;  }

    // VIPER #4431: Support disable in static elaboration (only block ids are disabled, static elab does not deal with tasks)
    void                PushBlockId(const VeriIdDef *id) ;  // Pushes the current block id into the stack Set
    void                PopBlockId(const VeriIdDef *id) ;   // Pops the current block id from the stack Set and clears the disabled block
    void                DisableBlock(const VeriIdDef *id) ; // Disables the block only if it is within the stack
    inline unsigned     IsDisabled() const            { return (_disabled_block_id) ? 1 : 0 ;  }
    inline unsigned     IsDeadCode() const            { return (_jump_token || _disabled_block_id) ? 1 : 0 ;  }

    // VIPER #2482, #3511: Assignment operator (including inc/dec operators) support inside index:
    unsigned            InsideBlockingAssign() const           { return _inside_blocking_assign ; }
    void                SetInsideBlockingAssign(unsigned stat) { _inside_blocking_assign = stat ; }
    void                InsertEvaluatedIndexValue(const VeriExpression *expr, VeriBaseValue *val) ;
    VeriBaseValue *     GetEvaluatedIndexValue(const VeriExpression *expr) const ;
    void                ClearAllEvaluatedIndexes() ;
private:
    Map             *_val_table ;
    Set             *_block_id_stack ;    // VIPER #4431: The set of block ids (VeriIdDef *) in order of nesting
    const VeriIdDef *_disabled_block_id ; // VIPER #4431: The block id that is currently disabled
    unsigned         _jump_token ;        // Stores 0, VERI_CONTINUE, VERI_BREAK or VERI_RETURN
    // VIPER #2482, #3511: Assignment operator (including inc/dec operators) support inside index:
    Map             *_evaluated_indexes ;       // The indexes that are already evaluated
    unsigned         _inside_blocking_assign ;  // Flag to indicate whether we are inside blocing assign statement or not

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class ValueTable

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_BASE_VALUE_H_
