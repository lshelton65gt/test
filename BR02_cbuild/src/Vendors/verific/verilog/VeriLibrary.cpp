/*
 *
 * [ File Version : 1.144 - 2014/03/06 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // for sprintf(...)
#include <ctype.h>   // for tolower

#include <string.h> // for strchr and strstr

#include "Strings.h"
#include "FileSystem.h"
#include "Map.h"
#include "Set.h"
#include "Array.h"
#include "Message.h"

#include "veri_file.h"
#include "VeriScope.h"

#include "VeriId.h"
#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "VeriLibrary.h"
#include "veri_tokens.h" // Tokens (unsigned numbers) are used in parse tree

#include "../util/SaveRestore.h"

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
#include "../vhdl/vhdl_file.h"
#include "../vhdl/VhdlUnits.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// VIPER #7404: If the following compile flag is defined, we try to avoid calling stat()
// on the required files to resolve -y modules. Instead we read the directory
// content first and try to resolve the modules from the available files.
#define VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM

// VIPER #8485 : If the following compile flag is defined and the '-y' option is given,
// Then the files with name 'lower_case_module_name + extension' and
// then the file with name 'upper_case_module_name + extension' is searched for
// unresolved modules if file name 'module_name + extension' is not found.
// This is done only if it is compiled under an OS other than Windows.
//#define VERILOG_CASE_INSENSITIVE_LIBRARY_FILE_SEARCH

// True if the CHARACTER 'c' is a path separator
#define IS_PATH_SEPARATOR(c)      ((c)=='/')
// Get the pointer to the first path separator in STRING 's'
#define GET_PATH_SEPARATOR(s)     (::strchr(s, '/'))
// "..." is hierarchical wildcard (matches any number of hierarchical directories)
#define ANY_HIERARCHICAL_PATH_STR "..."

// Function Name: VeriFileDirLibOption::VeriFileDirLibOption
// Arguments    : 1) const char * name - The name of the library
// Description  : The constructor for VeriFileDirLibOption
VeriFileDirLibOption::VeriFileDirLibOption(const char * name) :
    _file_dir_name(0)
{
    _file_dir_name = Strings::save(name) ;
    // Remove trailing slashes from the dir name:
    char *idx = _file_dir_name + Strings::len(_file_dir_name) ;
    if (idx) idx-- ;
    while (idx && (*idx == '/')) {
        *(idx--) = '\0' ;
    }
}

// Function Name: VeriFileDirLibOption::~VeriFileDirLibOption
// Description  : The virtual destructor for VeriFileDirLibOption. Cleanup the name
VeriFileDirLibOption::~VeriFileDirLibOption()
{
    Strings::free(_file_dir_name) ;
    _file_dir_name = 0 ;
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

// Function Name: VeriLibrary::VeriLibrary
// Arguments    : None
// Description  : The default constructor for VeriLibrary. The name is chosen
//                such that no library with the same name already exists.
//                The name is of the form "library %d", where %d is an integer.
//                This is a protected constructor of VeriLibrary and hence can
//                only be called by its derived class VeriUserLibrary. In other
//                words, we cannot create a VeriLibrary with this constructor.
VeriLibrary::VeriLibrary() :
    _name(0),
    _module_table(0),
    _bb_list(0)
{
   _module_table = new Map(STRING_HASH) ;
   _name = Strings::allocate(Strings::len("library ") + 20) ;

   int i = 0 ;
   while(++i) {
       sprintf(_name, "library %d", i) ;
       if (!veri_file::GetLibrary(_name)) break ;
   }
}

// Function Name: VeriLibrary::VeriLibrary
// Arguments    : 1) const char * name - The name of the library
// Description  : The constructor for VeriLibrary
VeriLibrary::VeriLibrary(const char *name) :
    _name(0),
    _module_table(0),
    _bb_list(0)
{
   _name = Strings::save(name) ;
   _module_table = new Map(STRING_HASH) ;
}

// Function Name: VeriLibrary::~VeriLibrary
// Description  : The virtual destructor for the class. It deletes the name
//              and the Map of module names.
VeriLibrary::~VeriLibrary()
{
    VeriModule * module ;
    MapIter mi ;
    // First run over the library, and reset their library back pointer.
    // This essentially detaches them from the library, while keeping them in the library table.
    // Then delete them, and finally reset the library table.
    // This avoids recursive dependent module deletion (with warnings) during the module destructor.
    FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi,module) {
        module->SetLibrary(0) ;
    }
    // Then iterate and really delete them.
    FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi,module) {
        delete module ;
    }

    // delete the module table
    delete _module_table ;
    _module_table = 0 ;

    // Delete the black-box list
    SetIter si ;
    char *bb ;
    FOREACH_SET_ITEM(_bb_list, si, &bb) Strings::free(bb) ;
    delete _bb_list ;
    _bb_list = 0 ;

    // delete the library name
    Strings::free(_name) ;
    _name = 0 ;

    // VIPER #6400: Rest the global work lib pointer if it is being deleted:
    if (veri_file::GetWorkLib() == this) veri_file::SetWorkLib(0) ;
}

// Function Name: VeriLibrary::GetModule
// Arguments    : 1) const char * module_name - The name of the module to
//                   retrieve from the the VeriLibrary object
// Return Type  : VeriModule *
// Description  : The function takes a string module_name and looks up for a
//                VeriModule* in _module_table having name module_name. If such a
//                VeriModule * is found, it is returned. Otherwise, the return
//                value is 0.
VeriModule *
VeriLibrary::GetModule(const char *module_name, unsigned case_sensitive, unsigned restore)
{
    // Call virtual routine to get module from library
    return GetModuleInternal(module_name, case_sensitive, restore) ;
}
VeriModule *
VeriLibrary::GetModuleInternal(const char *module_name, unsigned case_sensitive, unsigned restore)
{
    if (!module_name) return 0 ;

    // 'restore' used if release does have verilog binary save/restore
    (void) restore ;  // To prevent unused argument compile warning

    // VIPER #5035 :If name of module to be searched is in black-box list, do not further serach this
    // module, return 0. As it is already searched in this library.
    // CHECKME: May be we should not return 0 from here if 'restore' is not set.
    // Since in that case, the module may be in disk as binary dump sdb file.
    // Then we cannot differentiate between "module not in this library" and
    // "module in this library but saved in disk only" and we may have to keep
    // a Map instead of Set to have the status of module in here (VIPER #6706).
    if (case_sensitive && _bb_list && _bb_list->Get(module_name)) return 0 ;

    // Look for the module by name :
    VeriModule *module = (_module_table) ? (VeriModule*)_module_table->GetValue(module_name) : 0 ;
    if (module) return module ;

    if (!case_sensitive) {
        // WARNING : Case-insensitive search is SLOW. Have to visit every module
        MapIter mi ;
        FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi, module) {
            if (Strings::compare_nocase(module->Name(), module_name)) return module ;
        }
    }
    if (restore) {
        VeriModule *mod = 0 ;
        // Attempt to do a restore of module from a SDB file
        if (veri_file::Restore(GetName(), module_name, 1 /*silent*/)) {
            // Now return the restored module
            mod = GetModule(module_name, case_sensitive, 0 /* do not try to restore */) ;
            if (mod) return mod ;
        }

        // VIPER #4410 & #4551: Case insensitive search of library in mapped library paths:
        if (!case_sensitive) {
            // The control will come here only if no such module has been found.
            // Now, do a case insensitive search.
#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
            // VIPER #6310: Try to find the actual name from index file for lib-wide mode.
            // Then use that name to restore the actual module, since it is case insensitive:
            const char *actual_name = veri_file::FindCaseInsensitiveInLibFile(GetName(), module_name) ;
            if (actual_name && veri_file::Restore(GetName(), actual_name)) {
                // Now return the restored module
                mod = GetModule(actual_name, 1 /*case sensitive now*/, 0 /* do not try to restore */) ;
                if (mod) return mod ;
            }
#endif // VERILOG_USE_LIB_WIDE_SAVERESTORE

            // VIPER 3194: For a case insensitive search for a module which needs
            // to be restored, do the following:
            //   1. open the library directory
            //   2. get the file names
            //   3. do a case insensitive search to find proper dump files
            //   4. for a successfull match, restore the module

            // Open the library directory
            char *lib_path = 0 ;
            if (veri_file::GetLibraryPath(_name)) {
                lib_path = Strings::save(veri_file::GetLibraryPath(_name)) ;
            } else if (veri_file::GetDefaultLibraryPath()) {
                lib_path = Strings::save(veri_file::GetDefaultLibraryPath(), "/", _name) ;
            }

            Array files ;
            char *file ;
            unsigned i ;
            FileSystem::ReadDirectory(lib_path, &files) ;
            FOREACH_ARRAY_ITEM(&files, i, file) {
                char *mod_name_found = SaveRestore::GetDesignName(file) ; // VIPER #3465 : get the module name
                if (Strings::compare_nocase(module_name, mod_name_found)) { // compare the module names case insensitive
                    if (veri_file::Restore(_name, mod_name_found, 1 /*silent*/)) {
                        mod = GetModule(mod_name_found, case_sensitive, 0 /* do not try to restore */) ;
                    }
                    Strings::free(mod_name_found) ; // Free the module name
                    break ;
                }
                Strings::free(mod_name_found) ; // Free the module name
            }
            // Memory leak fix: Free all file names:
            FOREACH_ARRAY_ITEM(&files, i, file) Strings::free(file) ; // Free the file name

            Strings::free(lib_path) ;

            return mod ; // Return the module pointer in case of success or else return 0
        }
    }
    // VIPER #4957 : If this is temporary library (created for -v/y processing)
    // search associated design library for the module
    if (IsTmpLibrary()) {
        VeriLibrary *work_lib = GetWorkingLibrary() ;
        if (work_lib) return work_lib->GetModule(module_name, case_sensitive, restore) ;
    }
    // VIPER #5035 : Module 'module_name' is not present in this library, to stop further
    // searching when GetModule is called again with the same again, insert this name in
    // a set.
    if (!_bb_list) _bb_list = new Set(STRING_HASH) ;
    // Insert copied name, as caller can delete 'module_name' after calling this routine
    char *module_name_copy = Strings::save(module_name) ;
    if (!_bb_list->Insert(module_name_copy)) Strings::free(module_name_copy) ;

    return 0 ; // Nothing found
}

// VIPER #2755 : Search a design unit by name in all libraries starting from invoking one
VeriModule *
VeriLibrary::GetModuleAnywhere(const char *module_name, unsigned case_sensitive, unsigned restore)
{
    // Check in this library first
    VeriModule *module = GetModule(module_name, case_sensitive, restore) ;
    if (module) return module ; // Return, if found

    // Check other libraries
    VeriLibrary *lib ;
    MapIter mi ;
    FOREACH_VERILOG_LIBRARY(mi, lib) {
        if (!lib || (lib == this)) continue ; // Don't find in this library again
        module = lib->GetModule(module_name, case_sensitive, restore) ;
        if (module) return module ;
    }
    return 0 ; // Nothing found
}

// Function Name: VeriLibrary::AddModule
// Arguments    : 1) VeriModule * module - The module to be inserted in the Library
// Return Type  : unsigned
// Description  : The function returns 1 on success, 0 otherwise (if the module
//              could not be added and was deleted in the process.
//              'module' is added to the _module_table.
//              If another module with the same name was already present in
//              in the library, then we issue an warning, delete the existing one
//              and put the new module in its place.
unsigned VeriLibrary::AddModule(VeriModule *module, unsigned refuse_on_error /*=1*/)
{
    if (!module) return 0 ;

    // Refuse to store it if there were errors.  If 'refuse_on_error' is not set, then this was called
    // during a SDB restore.
    if (refuse_on_error && Message::ErrorCount()) {
        // Don't insert modules with errors
        module->Error("module %s ignored due to previous errors", module->Name()) ;
        delete module ;
        return 0 ;
    }

    //VeriModule *exist = VeriLibrary::GetModule(module->GetName()) ;
    VeriModule *exist = (_module_table) ? (VeriModule*)_module_table->GetValue(module->GetName()) : 0 ;

    // VIPER 5282: If this is a 'tmp' library, then allow multiple modules with same name,
    // and pick the first one : So, refuse to overwrite an existing module :
    if (exist && IsTmpLibrary()) {
        delete module ;
        return 0 ;
    }

    // VIPER #3212 : 'module' can be in the used by list of 'exist' which is to be
    // deleted here. So store the name of module first
    char *module_name = 0 ;
    if (exist) {
        if (exist->IsStaticElaborated()) {
            // VIPER #7141: Do not overwrite a static elaborated module which may have references elsewhere.
            // Deleting the existing module may lead to memory corruptions. Produce error and delete the new module:
            module->Error("previous definition of module %s is static elaborated, cannot overwrite", module->GetName()) ;
            delete module ;
            return 0 ;
        } else {
            //Issue warning that a module with the same name already exists
            module->Warning("overwriting previous definition of module %s", module->GetName()) ;

            // Remove it from module table
            if (_module_table) (void) _module_table->Remove(module->Name()) ;
            module_name = Strings::save(module->Name()) ;
            exist->SetLibrary(0) ; // Set library pointer to 0, so that while deleting 'exist' newly added module cannot be removed
            // delete exist ; #3212 : Do not delete here to stop core dump
        }
    }

    unsigned allow_force_insert = 0 ;

    // Finally insert the module in the table :
    if (_module_table) (void) _module_table->Insert(module->GetName(), module, 0, allow_force_insert) ;

    // And set the library back pointer in this module :
    module->SetLibrary(this) ;

    // VIPER #5035 : If may be the case that module which is added now in library is
    // in black-box list of this library, so try to remove added module from bb_list
    if (_bb_list) {
        char *n = (char*)_bb_list->Get(module->GetName()) ;
        (void) _bb_list->Remove(n) ;
        Strings::free(n) ;
    }

    // #3212 : Delete existing module after adding new module. It will help
    // to delete and remove 'module' from library when 'module' is in used by
    // list of 'exist'.
    delete exist ;

    // #3212 : Try to find 'module' in library. If it is not found, it indicates
    // that 'module' is deleted while deleting 'exist'. In that case we will not
    // traverse the module items
    unsigned already_deleted = 0 ; // Flag to indicate if 'module' is already deleted
    if (module_name) { // It is module overwritten
        if (!_module_table || !_module_table->GetValue(module_name)) { // 'module' is not in library
            already_deleted = 1 ; // 'module' is deleted with 'exist'
        }
    }
    Strings::free(module_name) ;
    if (already_deleted) return 0 ; // if module already deleted, then no use to continue. Return with 'not-inserted' status.

    // VIPER #7337: Check and mark if this module is to be marked as black box:
    if (!module->GetCompileAsBlackbox() && veri_file::CheckCompileAsBlackbox(module->GetName())) module->SetCompileAsBlackbox() ;

    // Do some special things for this module (process instantiations for uselib support).
    // VIPER #7337: Do not resolve instances from -y/-v, if the module is marked as brack box:
    VeriLibrary *lib = (IsUserLibrary()) ? this : 0 ;
    unsigned process_module_insts = (IsTmpLibrary()) ? 0 : 1 ;
#ifndef VERILOG_QUICK_PARSE_V_FILES
    if (RuntimeFlags::GetVar("verilog_ignore_unnecessary_modules_in_v_files")) {
        // VIPER #8405: We are going to ignore some modules, so, need to
        // know which other modules becomes required after parsing a module.
        // So, we need to immediately process the module instances for -v files:
        if (veri_file::IsProcessingUserLibrary()==2) process_module_insts = 1 ;
        // Use the current -y/-v library so that the unresolved module names are added there:
        if (!lib) lib = veri_file::GetCurrentUserLibrary() ;
    }
#endif
    if (process_module_insts && veri_file::GetCurrentUserLibrary() && !module->GetCompileAsBlackbox()) {
        Array *arr = module->GetModuleItems() ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(arr, i, item) {
            if (!item) continue ;
            item->ProcessModuleInstInLibrary(lib, module->GetScope()) ;
        }
    }

    return 1 ;
}

// Function Name: VeriLibrary::DetachModule
// Arguments    : 1) VeriModule * module - The module to be detached from a Library
// Return Type  : unsigned
// Description  : The function returns 1 if successfully detached, 0 otherwise
//                (if the module was not found in the library).
//                The VeriModule object itself is NOT deleted. Call VeriModule::delete
//                if you want the module to be deleted. VeriModule::delete will
//                detach (remove) itself from the library it is in (if in any library).
unsigned VeriLibrary::DetachModule(VeriModule *module)
{
    if (!module) return 0 ; // no module to remove
    if (!_module_table) return 0 ; // module is not here.

    // remove it from the table :
    // Multiple paramset can be in _module_table, remove 'module' only
    unsigned result = 0 ;
    MapItem *item ;
    FOREACH_SAME_KEY_MAPITEM(_module_table, module->GetName(), item) {
        if (module == (VeriModule*)item->Value()) {
            result = _module_table->Remove(item) ;
            break ;
        }
    }
    //unsigned result = _module_table->Remove(module->GetName()) ;
    if (result) {
        // re-set the library pointer in the module
        module->SetLibrary(0) ;
    }
    return result ;
}

// Function Name: VeriLibrary::RemoveModule
// Arguments    : 1) const char * name - The name of the module to
//                be removed
// Return Type  : unsigned
// Description  : Remove a module named "name" from _module_table. It
//                such a module was present the return value is 1. Otherwise
//                the return value is 0.
//                NOTE: This is an old routine. To remove a module from a library, simply call delete on it.
unsigned VeriLibrary::RemoveModule(const char *name)
{
    if (!name) return 0 ;
    VeriModule *module = VeriLibrary::GetModule(name, 1, 0 /* do not restore*/) ;
    if (!module) return 0 ; // module not there.

    delete module ; // will self-detach from this library.
    return 1 ;
}

// Function Name: VeriLibrary::RemoveSimpleModules
// Arguments    : None
// Return Type  : void
// Description  : Remove only the modules without parameters
void VeriLibrary::RemoveSimpleModules()
{
    MapIter i ;
    VeriModule *module ;
    char *key ;

    // First run over the library, and reset their library back pointer.
    // This essentially detaches them from the library, while keeping them in the library table.
    // Then delete them, and remove their entry in the table.
    // This avoids recursive dependent module deletion (with warnings) during the module destructor.
    FOREACH_MAP_ITEM(_module_table, i, &key, &module) {
        // don't delete modules with parameters..
        if (module->GetParameters()) continue ;
        // Some more modules that are not considered 'simple' :
        if (module->IsPslUnit()) continue ;
        if (module->IsInterface()) continue ;
        if (module->IsProgram()) continue ;
        if (module->IsPackage()) continue ;
        if (module->HasInterfacePorts()) continue ;
        // Reset the library back pointer
        module->SetLibrary(0) ;
    }

    // Run over the table and delete the parse trees,
    // and remove their entry in the table.
    FOREACH_MAP_ITEM(_module_table, i, &key, &module) {
        if (module->GetLibrary()) continue ; // We did not remove this one.

        (void) _module_table->Remove(key) ; // remove entry from table
        delete module ; // delete the module now.
    }
}

// Function Name: VeriLibrary::RemoveAllModules
// Arguments    : None
// Return Type  : void
// Description  : Clear the _module_table
void VeriLibrary::RemoveAllModules()
{
    MapIter mi ;
    VeriModule *module ;
    // First run over the library, and reset their library back pointer.
    // This essentially detaches them from the library, while keeping them in the library table.
    // Then delete them, and finally reset the library table.
    // This avoids recursive dependent module deletion (with warnings) during the module destructor.
    FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi,module) {
        module->SetLibrary(0) ;
    }
    // Then iterate and really delete them.
    //FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi,module) {
    // VIPER #4224 : Delete modules starting from back. This will make sure that
    // dependent modules will be deleted prior to packages on which the modules
    // are dependent.
    FOREACH_MAP_ITEM_BACK(_module_table, mi, 0, &module) {
        delete module ;
    }
    if (_module_table) _module_table->Reset() ; // Make sure to remove the entries.

    // VIPER #6706: Since all modules are removed, reset the black-box list too.
    // No use of the list any more - as of now, each module is black box here.
    SetIter si ;
    char *bb ;
    FOREACH_SET_ITEM(_bb_list, si, &bb) Strings::free(bb) ;
    delete _bb_list ; _bb_list = 0 ;
}

// Function Name: VeriLibrary::RemoveUncompiledModules
// Arguments    : None
// Return Type    : void
// Description  : Remove modules that are not compiled
void VeriLibrary::RemoveUncompiledModules()
{
    MapIter i ;
    VeriModule *module ;
    char *key ;
    // First run over the library, and reset their library back pointer.
    // This essentially detaches them from the library, while keeping them in the library table.
    // Then delete them, and remove their entry in the table.
    // This avoids recursive dependent module deletion (with warnings) during the module destructor.
    FOREACH_MAP_ITEM(_module_table, i, &key, &module) {
        if (module->IsCompiled()) continue ;
        // Reset the library back pointer
        module->SetLibrary(0) ;
    }
    // Run over the table and delete the parse trees,
    // and remove their entry in the table.
    FOREACH_MAP_ITEM(_module_table, i, &key, &module) {
        if (module->IsCompiled()) continue ; // or use GetLibrary() (if NULL, delete the tree).

        (void) _module_table->Remove(key) ; // remove entry from table
        delete module ; // delete the module now.
    }
}
// Function Name: VeriTmpLibrary::VeriTmpLibrary
VeriTmpLibrary::VeriTmpLibrary(VeriLibrary *l)
    :VeriLibrary()
    , _work_lib(l)
{
}
VeriTmpLibrary::~VeriTmpLibrary() { _work_lib = 0 ; }

// Function Name: VeriUserLibrary::VeriUserLibrary - The default constructor
VeriUserLibrary::VeriUserLibrary(unsigned tick_uselib):VeriLibrary(),
    _all_files_dirs(0),
    _libext(0),
    _module_names_to_lookup(0),
    _num_modules_to_lookup(0), // VIPER #7740
    _module_names_added(0),
    _lib_rescan_active(0),
    _tick_uselib(tick_uselib),
    _tick_uselib_lib(0),
    _is_processing(0),
    _uselib_lib_mod_lib_map(0),
    _dir_file_status(0)
#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    , _modules_in_file(0)
#endif
{
}

// Function Name: VeriUserLibrary::VeriUserLibrary - The constructor
// Arguments    : 1) char * name - The name of the library
VeriUserLibrary::VeriUserLibrary(const char *name, unsigned tick_uselib):VeriLibrary(name),
    _all_files_dirs(0),
    _libext(0),
    _module_names_to_lookup(0),
    _num_modules_to_lookup(0), // VIPER #7740
    _module_names_added(0),
    _lib_rescan_active(0),
    _tick_uselib(tick_uselib),
    _tick_uselib_lib(0),
    _is_processing(0),
    _uselib_lib_mod_lib_map(0),
    _dir_file_status(0)
#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    , _modules_in_file(0)
#endif
{
}

// Function Name: VeriUserLibrary::~VeriUserLibrary
// Description  : The virtual destructor. Do all necessary cleanup
VeriUserLibrary::~VeriUserLibrary()
{
    MapIter mi ;
#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    Set *defined_modules ;
    FOREACH_MAP_ITEM(_modules_in_file, mi, 0, &defined_modules) {
        SetIter si ;
        char *mod_name ;
        FOREACH_SET_ITEM(defined_modules, si, &mod_name) Strings::free(mod_name) ;
        delete defined_modules ;
    }
    delete _modules_in_file ;
    _modules_in_file = 0 ;
#endif

    unsigned i = 0 ;
    VeriFileDirLibOption *option ;
    FOREACH_ARRAY_ITEM(_all_files_dirs, i, option)  delete option ;
    delete _all_files_dirs ;

    char *ext ;
    FOREACH_ARRAY_ITEM(_libext, i, ext) {
        Strings::free(ext) ;
    }
    delete _libext ;

    VeriUserLibrary::RemoveAllModuleNames() ;
    _module_names_to_lookup = 0 ;
    RemoveNewModuleNames() ;
    _module_names_added = 0 ;

    if (_uselib_lib_mod_lib_map) {
        char *key ;
        char *val ;
        MapIter iter ;
        FOREACH_MAP_ITEM(_uselib_lib_mod_lib_map, iter, &key, &val) {
            Strings::free(key) ;
            Strings::free(val) ;
        }
        delete _uselib_lib_mod_lib_map ;
        _uselib_lib_mod_lib_map = 0 ;
    }

    RemoveDirFileStatus() ;

    if (veri_file::GetCurrentUserLibrary() == this) veri_file::SetCurrentUserLibrary(0) ;
    if (veri_file::GetYVUserLibrary() == this) veri_file::SetYVUserLibrary(0) ;
}

// Function Name: VeriUserLibrary::InsertFileDirLibOption
// Arguments    : 1) VeriFileDirLibOption * option - The file/dir option that
//              represents  file or directory in the 'uselib statement or
//              -y -v option
// Return Type  : void
// Description  : Insert the VeriFileDir option in the array _all_files_dirs
void VeriUserLibrary::InsertFileDirLibOption(const VeriFileDirLibOption *option)
{
    if (!_all_files_dirs) _all_files_dirs = new Array() ;
    _all_files_dirs->InsertLast(option) ;
}

// Function Name: VeriUserLibrary::GetLibExt
// Arguments    : None
// Return Type  : void
// Description  : Get the extension for the user library
Array *VeriUserLibrary::GetLibExt() const
{
    return _libext ;
}

// Function Name: VeriUserLibrary::AddLibExt
// Arguments    : 1) char * libtxt - The library extension
// Return Type  : void
// Description  : Adds the extension for the user library
void VeriUserLibrary::AddLibExt(const char *libext)
{
    // `uselib lib support
    if (_tick_uselib_lib) {
        VeriTreeNode dummy_node ;
        dummy_node.Error("illegal use of `uselib directive : cannot mix lib with file|dir|libext") ;
    }
    // end `uselib lib support

    if(!_libext) _libext = new Array(1) ;
    _libext->InsertLast(Strings::save(libext)) ;
}

// VIPER 2132 : Set -librescan option to user library.
// This option specifies always starting the search for unresolved module definitions
// with the first library specified on command line or `uselib
void VeriUserLibrary::SetLibReScan(unsigned lib_rescan_active)
{
    _lib_rescan_active = lib_rescan_active ;
}

// Function Name: VeriLibrary::Process
// Arguments    : None
// Return Type  : void
// Description  : This function processes the library. Do nothing for
//                VeriLibrary
void VeriLibrary::Process()
{
}

// Function Name: VeriLibrary::ParseFile
// Arguments    : None
// Return Type  : void
// Description  : This function parses a (-v/-y/uselib) file into the library.
unsigned VeriLibrary::ParseFile(const char *filename)
{
    // VIPER #2680 : Check error status before calling 'Analyze'. 'Analyze' clears
    // error count
    if (Message::ErrorCount()) return 0 ;

    // Analyze the file (use same dialect (mode) as currently set.
    // VIPER #5657: Use default analysis mode if set, otherwise we current analysis mode.
    // File extension based mode will be taken care of from the AnalyzeInternal rotuine:
    unsigned analysis_mode = veri_file::GetDefaultAnalysisMode() ;
    if (analysis_mode == veri_file::UNDEFINED) analysis_mode = veri_file::GetAnalysisMode() ;
    unsigned success = veri_file::AnalyzeLibFile(filename, analysis_mode, GetName()) ;

    // Clean out the library if something went wrong :
    if (!success) RemoveAllModules() ;

    return success ;
}

// Function Name: VeriUserLibrary::Process
// Arguments    : None
// Return Type  : void
// Description  : This function processes the user library.
//                1. Check if there are module names to be looked up in this
//                   library. If there are no such names just return
//                2. Next parse each file of the FileDirOptions in a temporary
//                   library
//                3. Create a new list of "wanted modules" from the new module
//                   instantiations that occur inside the old "wanted modules".
//                   This is done by ProcessModuleInstInLibrary
//                4. Check if the required modules are in the temporary library.
//                   If it is found, move the module from the temporary library
//                   to this library.
//                5. Continue the process till there are no more "wanted
//                   modules" or no module has been resolved in the last pass.
void VeriUserLibrary::Process()
{
    // For use libs, process the library iff there is at least one module name not yet resolved
    if (!HasUnresolvedModules()) {
        RemoveAllModuleNames() ;
        RemoveNewModuleNames() ;
        return ;
    }

    // VIPER #5424: Recursion breaker: Do not process a user library if it is already
    // being processed. And set the processing flag to indicate we are processing it:
    if (_is_processing) return ;
    _is_processing = 1 ;

    // Pick up the current work and user libraries
    VeriLibrary *cur_lib = veri_file::GetWorkLib() ;
    VeriLibrary *cur_use_lib  = veri_file::GetCurrentUserLibrary() ;

    // VIPER #8343: Quicky check and remove unresolved modules which are in work library:
    // This is required to stop un-necessary iteration over all the -y/-v files once again
    // when size of _module_names_to_lookup is greater that number of module in -v files.
    // In such case, we only iterate over the modules of -v file and never actually remove
    // the modules which will normally be resolved from work library otherwise.
    (void) ResolveModuleNamesFromLibrary(cur_lib, 1 /* ignore `uselib in the beginning */) ;

    // VIPER #5932: Get previous status and set the current status of processing user library flag:
    unsigned prev_status = veri_file::SetProcessingUserLibrary(1) ;

    // Temporarily set current work and user libraries to 'this'.
    VeriLibrary *work_library = cur_lib ;
    veri_file::SetCurrentUserLibrary(this) ;
    veri_file::SetWorkLib(this) ;

    VeriFileDirLibOption *file_dir = 0 ;
    int i = 0 ;

    unsigned module_is_resolved = 0 ;
    unsigned ignore_unnecessary_modules_in_v_files = 0 ;
#ifndef VERILOG_QUICK_PARSE_V_FILES
    ignore_unnecessary_modules_in_v_files = RuntimeFlags::GetVar("verilog_ignore_unnecessary_modules_in_v_files") ? 1 : 0 ;
#endif
    do {
        module_is_resolved = 0 ;
        FOREACH_ARRAY_ITEM(_all_files_dirs, i, file_dir) {
            if (_lib_rescan_active || // VIPER 2132 : librescan is active
                // VIPER #8405: for -v files with ignore mode, create this Set.
                // We need to immediately know if some more unresolved modules added.
                (file_dir->IsFileOption() && ignore_unnecessary_modules_in_v_files)) {
                // Create a new set to store unresolved module names found during
                // following option processing
                RemoveNewModuleNames() ;
                _module_names_added = new Set(STRING_HASH) ;
            }
            if (file_dir->IsFileOption()) {
                // This is the file option
                (void) veri_file::SetProcessingUserLibrary(2) ; // VIPER #8405: Processing -v files
                if (ProcessFileOption(file_dir, work_library)) module_is_resolved = 1 ;
                (void) veri_file::SetProcessingUserLibrary(1) ;
            } else if (file_dir->IsDirOption()) {
                // This is a dir option
                //(void) veri_file::SetProcessingUserLibrary(3) ; // Processing -y dirs
                if (ProcessDirOption(file_dir, work_library)) module_is_resolved = 1 ;
                //(void) veri_file::SetProcessingUserLibrary(1) ;
            } else if (file_dir->IsLibOption()) {
                // This is a lib option
                //(void) veri_file::SetProcessingUserLibrary(4) ; // Processing `uselib options
                if (ProcessLibOption(file_dir, work_library)) module_is_resolved = 1 ;
                //(void) veri_file::SetProcessingUserLibrary(1) ;
            }
            // VIPER 2132 : If any module is resolved in the above option and new
            // module names are added, for active librescan option, process -y/-v options
            // starting from beginning
            if (module_is_resolved && _lib_rescan_active && _module_names_added && _module_names_added->Size()) break ;
        }

        // VIPER #8343: Check work library again for resolved modules:
        // CHECKME: We need not call it if we traversed over the _module_names_to_lookup Map.
        (void) ResolveModuleNamesFromLibrary(cur_lib /* work library */, 0 /* do not ignore `uselib at the end */) ;
    } while (module_is_resolved && HasUnresolvedModules()) ;

    // Uselib processing is over, clear names from maps
    RemoveAllModuleNames() ;
    RemoveNewModuleNames() ;
    RemoveDirFileStatus() ; // VIPER #7404: Cleanup
    veri_file::SetWorkLib(cur_lib) ;
    veri_file::SetCurrentUserLibrary(cur_use_lib) ;
    // VIPER #5932: Set the previous status to processing user library:
    (void) veri_file::SetProcessingUserLibrary(prev_status) ;

    _is_processing = 0 ; // VIPER #5424: Reset it, we are done.
}

unsigned
VeriUserLibrary::ResolveModuleNamesFromLibrary(VeriLibrary *work_lib, unsigned ignore_tick_uselib)
{
    if (!work_lib || !HasUnresolvedModules()) return 0 ;
    if (ignore_tick_uselib && _tick_uselib) return 0 ;

    // VIPER #8343: Check and remove unresolved module names which are in work library:
    unsigned module_resolved = 0 ;
    MapIter mi ;
    const char *module_name ;
    unsigned long unresolved_module ;
    FOREACH_MAP_ITEM(_module_names_to_lookup, mi, &module_name, &unresolved_module) {
        if (unresolved_module && module_name && (work_lib->GetModule(module_name, 1) || veri_file::GetModuleFromLOptions(module_name))) {
            RemoveModuleName(module_name) ;
            module_resolved++ ;
        }
    }

    return module_resolved ;
}

// Returns '1' if we have some unresolved modules to bind, returns '0' otherwise
unsigned
VeriUserLibrary::HasUnresolvedModules() const
{
    if (!_module_names_to_lookup) return 0 ;

    // VIPER #7740: Speed up by keeping an unsigned number, no need to check the Map:
    //MapIter mi ;
    //unsigned long unresolved_module ;
    //FOREACH_MAP_ITEM(_module_names_to_lookup, mi, 0, &unresolved_module) {
    //    if (unresolved_module) return 1 ;
    //}
    //return 0 ;
    return _num_modules_to_lookup ;
}

// Process File option to bind unresolved modules.
unsigned VeriUserLibrary::ProcessFileOption(const VeriFileDirLibOption *file_dir, VeriLibrary * work_library)
{
    if (!file_dir) return 0 ;

    // If there are no more unresolved modules, then we should not even parse the file
    if (!HasUnresolvedModules()) return 0 ;

    char * filename =  file_dir->GetName() ;

    // Check if the file exists in the status map:
#ifdef VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM
    // VIPER #7404: Cannot use '.', collides with '-y .' option with this compile flag
    const char *dir = "" ; // Use empty string instead
#else
    const char *dir = "." ; // Current directory
#endif
    Map *dir_map = (_dir_file_status) ? (Map *)_dir_file_status->GetValue(dir) : 0 ;
    MapItem *file_item = (dir_map) ? dir_map->GetItem(filename) : 0 ;
    unsigned file_exists = (file_item) ? (unsigned)(unsigned long)file_item->Value() : 0 ;
    if (!dir_map || !file_item) {
        // Does not exist in the status map, check it in the hard way:
        if (!_dir_file_status) _dir_file_status = new Map(STRING_HASH_FILE_PATH) ;
        if (!dir_map) {
            dir_map = new Map(STRING_HASH_FILE_PATH) ;
            (void) _dir_file_status->Insert(Strings::save(dir), dir_map) ;
        }

        if (FileSystem::IsFile(filename)) {
            (void) dir_map->Insert(Strings::save(filename), (void *)1L /* valid, exists */) ;
            file_exists = 1 ;
        } else {
            (void) dir_map->Insert(Strings::save(filename), (void *)0L /* does not exist or invalid */) ;
        }
    //} else {
    //    Message::PrintLine("Skipping 'stat' on file ", filename, " (it ", (file_exists)?"exists":"does not exist", ")") ;
    }

    if (!file_exists) {
        // VIPER #4720: If the library file cannot be opened then issue a WARNING message:
        VeriTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        tmp_print_node.Warning("cannot open verilog file %s", filename) ;
        return 0 ; // Cannot find the library file
    }

#ifdef VERILOG_QUICK_PARSE_V_FILES
    Set *defined_modules = 0 ;
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
    if (!_modules_in_file) _modules_in_file = new Map(STRING_HASH) ;
    defined_modules = (Set *)_modules_in_file->GetValue(filename) ;
    if (defined_modules) {
        VERIFIC_ASSERT(_module_names_to_lookup) ;
        unsigned found_module =  0 ;
        SetIter si ;
        const char *module_name ;
        FOREACH_SET_ITEM(defined_modules, si, &module_name) if (_module_names_to_lookup->GetValue(module_name)) { found_module = 1 ; break ; }
        if (!found_module) return 0 ; // No undefined module is defined in this file, skip it
    } else {
        defined_modules = new Set(STRING_HASH) ;
#endif
        unsigned undefined_module_in_file = veri_file::HasUndefinedModuleInFile(filename, _module_names_to_lookup, defined_modules) ;
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
        if (!veri_file::HasActiveMacroDefRef()) {
            // This file has no macro def/ref, store the name of the modules defined in there:
            _modules_in_file->Insert(filename, defined_modules) ;
        } else {
            // Clean-up:
            SetIter si ;
            char *module_name ;
            FOREACH_SET_ITEM(defined_modules, si, &module_name) Strings::free(module_name) ;
            delete defined_modules ; defined_modules = 0 ;
        }
#endif

        if (undefined_module_in_file) {
            veri_file::ClearActiveMacros() ; // Clear all the active macros, they will be redone
            // We need this file, so parse the file in full below...
        } else {
            veri_file::ApplyActiveMacros() ; // Apply the macros for the above file, they should stay
            return 0 ; // No undefined module is defined in this file, skip it
        }
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
    }
#endif
#endif // VERILOG_QUICK_PARSE_V_FILES

    VeriTmpLibrary *tmp_lib = new VeriTmpLibrary(work_library) ; // the temporary lib to parse the file into
    veri_file::AddLibrary(tmp_lib) ;

    unsigned module_is_resolved = 0 ;
#ifndef VERILOG_QUICK_PARSE_V_FILES
    if (!RuntimeFlags::GetVar("verilog_ignore_unnecessary_modules_in_v_files")) {
#endif
        if (!tmp_lib->ParseFile(filename)) {
            veri_file::RemoveLibrary(tmp_lib) ; // error in parsing
            delete tmp_lib ;
            return 0 ;
        }

        // Now find the modules that were still unresolved.
        module_is_resolved = PickRequiredModules(tmp_lib, work_library, 0, 1 /* v-file-option */) ;
#ifndef VERILOG_QUICK_PARSE_V_FILES
    } else {
        // VIPER #8405: Parse this file in loop untill all modules from this
        // files are resolved. VCS seems to be doing this same.
        unsigned rescan_same_file = 0 ;
        do {
            Set ignored_modules(STRING_HASH) ;
            veri_file::SetIgnoredModuleNames(&ignored_modules) ;
            if (rescan_same_file) Message::StartIgnoreAllMessages() ;
            unsigned result = tmp_lib->ParseFile(filename) ;
            if (rescan_same_file) Message::StopIgnoreAllMessages() ;
            veri_file::SetIgnoredModuleNames(0) ;
            if (!result) {
                veri_file::RemoveLibrary(tmp_lib) ; // error in parsing
                delete tmp_lib ;
                return 0 ;
            }

            // Now find the modules that were still unresolved.
            module_is_resolved |= PickRequiredModules(tmp_lib, work_library, 0, 1 /* v-file-option */) ;
            rescan_same_file = veri_file::HasAnyModuleInIgnoredList(_module_names_added, &ignored_modules) ;
            SetIter si ;
            char *name ;
            FOREACH_SET_ITEM(&ignored_modules, si, &name) Strings::free(name) ;
        } while (rescan_same_file) ;
    }
#endif

    veri_file::RemoveLibrary(tmp_lib) ;
    delete tmp_lib ;
    return module_is_resolved ;
}

// `uselib lib support
// Process lib option to bind unresolved modules.
unsigned VeriUserLibrary::ProcessLibOption(const VeriFileDirLibOption *file_dir_lib, VeriLibrary * work_library)
{
    if (!file_dir_lib) return 0 ;

    // If there are no more unresolved modules, then we should not even parse the file
    if (!HasUnresolvedModules()) return 0 ;

    char * libname =  file_dir_lib->GetName() ;
    VeriLibrary *uselib_lib = veri_file::GetLibrary(libname) ;

    // Now find the modules that were still unresolved.
    unsigned module_is_resolved = PickRequiredModules(uselib_lib, work_library) ;

    return module_is_resolved ;
}

// If uselib-lib, following API will update a local <mod-name, lib-name>
// cache, and not own this module. Original user lib will still own the
// module. If not uselib-lib, base class AddModule will be called.
unsigned VeriUserLibrary::AddModule(VeriModule *module, unsigned refuse_on_error /*=1*/)
{
    if (!module) return 0 ;

    if (refuse_on_error && Message::ErrorCount()) {
        // Don't insert modules with errors
        module->Error("module %s ignored due to previous errors", module->Name()) ;
        delete module ;
        return 0 ;
    }

    if (!_tick_uselib_lib) return VeriLibrary::AddModule(module) ;

    VeriLibrary *mod_lib = module->GetLibrary() ;
    if (!mod_lib) return 0 ;

    const char *mod_lib_name = mod_lib->GetName() ;
    if (!mod_lib_name) return 0 ;

    if (!_uselib_lib_mod_lib_map) _uselib_lib_mod_lib_map = new Map(STRING_HASH) ;

    const char *mod_name = module->GetName() ;
    const char *lib_name =  (const char *) _uselib_lib_mod_lib_map->GetValue(mod_name) ;
    if (lib_name && !Strings::compare(lib_name, mod_lib_name)) {
        //Issue warning that a module with the same name already exists
        module->Warning("overwriting previous definition of module %s", mod_name) ;

        // Remove it from _uselib_lib_mod_lib_map
        MapItem *mi = _uselib_lib_mod_lib_map->GetItem(mod_name) ;
        VERIFIC_ASSERT(mi) ; // Must exists here
        char *key = (char *)mi->Key() ;
        char *val = (char *)mi->Value() ;
        (void) _uselib_lib_mod_lib_map->Remove(mod_name) ;
        Strings::free(key) ;
        Strings::free(val) ;
    }

    (void) _uselib_lib_mod_lib_map->Insert(Strings::save(mod_name), Strings::save(mod_lib_name)) ;

    // VIPER #7337: Check and mark if this module is to be marked as black box:
    // Since this module is already added in a library, no need to check it here, again.
    //if (!module->GetCompileAsBlackbox() && veri_file::CheckCompileAsBlackbox(module->GetName())) module->SetCompileAsBlackbox() ;

    // Do some special things for this module (process instantiations for uselib support).
    // VIPER #7337: Do not resolve instances from -y/-v, if the module is marked as brack box:
    if (!IsTmpLibrary() && veri_file::GetCurrentUserLibrary() && !module->GetCompileAsBlackbox()) {
        Array *arr = module->GetModuleItems() ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(arr, i, item) {
            if (!item) continue ;
            item->ProcessModuleInstInLibrary((IsUserLibrary()) ? this : 0, module->GetScope()) ;
        }
    }

    return 1 ;
}

// Select required modules from the lib option specific logical library.
unsigned VeriUserLibrary::PickRequiredModules(VeriLibrary *lib, VeriLibrary *work_library)
{
    unsigned module_is_resolved = 0 ;
    if (!lib) return module_is_resolved ;

    Set dependent_scopes(POINTER_HASH) ;
    VeriScope *module_scope ;
    char *module_name ;
    unsigned long valid_module_name ;
    MapIter iter ;
    FOREACH_MAP_ITEM(_module_names_to_lookup, iter, &module_name, &valid_module_name) {
        if (!valid_module_name) continue ;

        if (VeriLibrary::GetModuleInternal(module_name, 1, 1) || (_uselib_lib_mod_lib_map && _uselib_lib_mod_lib_map->GetValue(module_name))) {
            // VIPER #6679: CHECKME: Also ignore if veri_file::GetModuleFromLOptions(module_name)?
            // module name exists in this library.So ignore it
            RemoveModuleName(module_name) ;
            continue ;
        }

        VeriModule *module = lib->GetModule(module_name, 1) ;
        if (module) {
            //Module is found.
            module_is_resolved = 1 ;

            // Find all the scopes (recursively) on which this module depends:
            module_scope = module->GetScope() ;
            if (module_scope) module_scope->DependentScopes(dependent_scopes) ;

            // Viper: 3142 - all verific msg should have msg-id
            // Message::PrintLine("      Resolving module ",module->GetName()) ;
            VeriTreeNode tmp_print_node ;
            tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
            tmp_print_node.Comment("      Resolving module %s", module->GetName()) ;

            (void) AddModule(module) ; // and add to this library

            // VIPER 5182: We have to call Resolve now that we choose it, since that was not called when the module went into the tmp library.
            // VIPER #5939: The module was not in a temp library, it was in a non-temp local library.
            // So, Resolve was already called on it, do not call it again here:
            //module->ResolveBody() ;

            RemoveModuleName(module_name) ;
            continue ;
        }

        // VIPER #3048: Look into 'work' library if this library was specified from `uselib
        // VIPER #6679: Also check if this module is in -L specific libraries:
        if (_tick_uselib && ((work_library && work_library->GetModule(module_name, 1)) || veri_file::GetModuleFromLOptions(module_name))) {
            // module exists in work library. So ignnore it
            RemoveModuleName(module_name) ;
        }
    }

    // Now, pick the module on which other modules which are taken depends:
    SetIter si ;
    FOREACH_SET_ITEM(&dependent_scopes, si, &module_scope) {
        VeriIdDef *mod_id = (module_scope) ? module_scope->GetOwner() : 0 ;
        VeriModule *mod = (mod_id) ? mod_id->GetModule() : 0 ;
        if (!mod || (mod->GetLibrary() != lib)) continue ;
        (void) lib->DetachModule(mod) ; // remove the handle from the module table
        (void) AddModule(mod) ; // and add to this library
        // VIPER 5182: We have to call Resolve now that we choose it, since that was not called when the module went into the tmp library.
        // VIPER #5939: The module was not in a temp library, it was in a non-temp local library.
        // So, Resolve was already called on it, do not call it again here:
        //mod->ResolveBody() ;
    }

    return module_is_resolved ;
}

unsigned VeriUserLibrary::ProcessDirOption(const VeriFileDirLibOption *file_dir, VeriLibrary *work_library)
{
    char *dir = file_dir->GetName() ;
    if (!_libext) {
        _libext = new Array(1) ;
        _libext->InsertLast(Strings::save(".v")) ;
    }

    if (!_dir_file_status) _dir_file_status = new Map(STRING_HASH_FILE_PATH) ;
    Map *dir_map = (Map *)_dir_file_status->GetValue(dir) ;
    if (!dir_map) {
        dir_map = new Map(STRING_HASH_FILE_PATH) ;
        (void) _dir_file_status->Insert(Strings::save(dir), dir_map) ;
    }

    unsigned i ;

#ifdef VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM
    // VIPER #7404: Cache full directory content before processing the unresolved modules.
    // We cache it once per directory. This caching improves runtime when the -y directory
    // resides over remote/slow NFS mounts.
    Array files ;
    char *file ;
    FileSystem::ReadDirectory(dir, &files) ;
    FOREACH_ARRAY_ITEM(&files, i, file) {
        // Memory leak fix: Do not insert into the map if it is already there:
        if (!dir_map->GetItem(file) && !(((file[0]=='.') && !file[1]) || ((file[0]=='.') && (file[1]=='.') && !file[2]))) { // Ignore . and ..
            // File exists, valid regular file will be checked later below:
            // Insert (relative) file name only, no directory part:
            (void) dir_map->Insert(Strings::save(file), (void *)2L /* exists, validity/file-type not checked */) ;
        }
        Strings::free(file) ; // Free file name
    }

    if (!dir_map->Size()) return 0 ; // Not a valid directory or is an empty one, no need to check further
#endif // VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM

    // VIPER #7404: Create a Set from the Array of libext to avoid duplicate libext:
    Set lib_ext_set(STRING_HASH_FILE_PATH) ;
    char *lib_ext = 0 ;
    FOREACH_ARRAY_ITEM(_libext, i, lib_ext) (void) lib_ext_set.Insert(lib_ext) ;

    unsigned module_is_resolved = 0 ;
    MapIter iter ;
    char * module_name ;
    unsigned long valid_module_name ;
    FOREACH_MAP_ITEM(_module_names_to_lookup, iter, &module_name, &valid_module_name) {
        if (!valid_module_name) continue ;

        // VIPER #3107 : Do not try to resolve this option added instantiated module in this directory
        if (_lib_rescan_active && _module_names_added && _module_names_added->GetItem(module_name)) continue ;

// RD added this 1/14/2004 : Check if the module is in the work library.
        // VIPER #6679: Also check if this module is in -L specific libraries:
        if ((work_library && work_library->GetModule(module_name, 1)) || veri_file::GetModuleFromLOptions(module_name)){
            // module exists in work library. So ignnore it
            RemoveModuleName(module_name) ;
            continue ;
        }
// RD end.

        char * file_name = 0 ;
        SetIter e ;
        unsigned is_valid_ext = 0 ;
        // Create the filename
        FOREACH_SET_ITEM(&lib_ext_set, e, &lib_ext) {
            if (is_valid_ext) break ;

#ifdef VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM // Already checked the full directory content above, no need to check again
            file_name = Strings::save(module_name, lib_ext) ; // Create relative file name (without directory part)
            is_valid_ext = (unsigned)(unsigned long)dir_map->GetValue(file_name) ;

#if defined(VERILOG_CASE_INSENSITIVE_LIBRARY_FILE_SEARCH) && !defined(WINDOWS)
            // VIPER #8485 : Since file name in Windows is case insensitive so, it will be automatically handled by Windows.
            // If the file name 'module_name + extension' is not found then,
            // The file name 'lower_case_module_name + extension' is searched.
            // Otherwise, the file name 'upper_case_module_name + extension' is searched.

            if (!is_valid_ext) {
                char *module_name_lower = Strings::strtolower(Strings::save(module_name)) ;
                Strings::free(file_name) ;
                file_name = Strings::save(module_name_lower, lib_ext) ; // Create relative file name (without directory part)
                Strings::free(module_name_lower) ;
                is_valid_ext = (unsigned)(unsigned long)dir_map->GetValue(file_name) ;
            }

            if (!is_valid_ext) {
                char *module_name_upper = Strings::strtoupper(Strings::save(module_name)) ;
                Strings::free(file_name) ;
                file_name = Strings::save(module_name_upper, lib_ext) ; // Create relative file name (without directory part)
                Strings::free(module_name_upper) ;
                is_valid_ext = (unsigned)(unsigned long)dir_map->GetValue(file_name) ;
            }
#endif
            if (is_valid_ext==2) {
                // VIPER #7404: Do not use stat() here, it mnay take time too. Instead, we suppress the ERROR message
                // "cannot open verilog file %s" (VERI-1517) below so that we do not produce it incorrectly:

                //VERIFIC_ASSERT(file_item && dir_map) ; // Must be valid, since is_valid_ext is taken from those
                //if (FileSystem::IsFile(file_name)) {
                //    is_valid_ext = 1 ;
                //    (void) dir_map->Insert(file_item->Key(), (void *)1L /* valid, exists */, 1 /* force overwrite */) ;
                //} else {
                //    is_valid_ext = 0 ;
                //    (void) dir_map->Insert(file_item->Key(), (void *)0L /* valid, exists */, 1 /* force overwrite */) ;
                //}
#else
            //Check if the dir ends with a '/'
            // No need to check for trailing slashes, we removed from the ctor of VeriFileDirLibOption
            //if (dir[Strings::len(dir)-1] == '/') {
            //    file_name = Strings::save(dir, module_name, lib_ext) ;
            //} else {
            file_name = Strings::save(dir, "/", module_name, lib_ext) ;
            //}
            // Check if the file exists
            // First check it in the file status map:
            //Map *dir_map = (_dir_file_status) ? (Map *)_dir_file_status->GetValue(dir) : 0 ; // Already found above
            MapItem *file_item = dir_map->GetItem(file_name) ;
            is_valid_ext = (file_item) ? (unsigned)(unsigned long)file_item->Value() : 0 ;

            if (!file_item) {
                // Does not exist in the status map, check it in the hard way:
                //if (!_dir_file_status) _dir_file_status = new Map(STRING_HASH_FILE_PATH) ;
                //if (!dir_map) {
                //    dir_map = new Map(STRING_HASH_FILE_PATH) ;
                //    (void) _dir_file_status->Insert(Strings::save(dir), dir_map) ;
                //}
                // Also insert the status into the status map for later check:
                if (FileSystem::IsFile(file_name)) {
                    // File exists
                    is_valid_ext = 1 ;
                    (void) dir_map->Insert(Strings::save(file_name), (void *)1L /* valid, exists */) ;
                } else {
#if defined(VERILOG_CASE_INSENSITIVE_LIBRARY_FILE_SEARCH) && !defined(WINDOWS)
                    // VIPER #8485: Since file name in Windows is case insensitive so, it will be automatically handled by Windows.
                    // If the file name 'module_name + extension' is not found then,
                    // The file name 'lower_case_module_name + extension' is searched.
                    // Otherwise, the file name 'upper_case_module_name + extension' is searched.

                    char *module_name_lower = Strings::strtolower(Strings::save(module_name)) ;
                    Strings::free(file_name) ;
                    file_name = Strings::save(dir, "/", module_name_lower, lib_ext) ;
                    Strings::free(module_name_lower) ;

                    if (FileSystem::IsFile(file_name)) {
                        // File exists
                        is_valid_ext = 1 ;
                        (void) dir_map->Insert(Strings::save(file_name), (void *)1L /* valid, exists */) ;
                    } else {
                        char *module_name_upper = Strings::strtoupper(Strings::save(module_name)) ;
                        Strings::free(file_name) ;
                        file_name = Strings::save(dir, "/", module_name_upper, lib_ext) ;
                        Strings::free(module_name_upper) ;

                        if (FileSystem::IsFile(file_name)) {
                            // File exists
                            is_valid_ext = 1 ;
                            (void) dir_map->Insert(Strings::save(file_name), (void *)1L /* valid, exists */) ;
                        } else {
#endif
                            (void) dir_map->Insert(Strings::save(file_name), (void *)0L /* does not exist or invalid */) ;
#if defined(VERILOG_CASE_INSENSITIVE_LIBRARY_FILE_SEARCH) && !defined(WINDOWS)
                        }
                    }
#endif
                }
#endif // VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM
            //} else {
            //    Message::PrintLine("Skipping 'stat' on file ", file_name, " (it ", (is_valid_ext)?"exists":"does not exist", ")") ;
            }

            if (is_valid_ext) {
                // Process the file
                // The temporary library where the file
                // will be parsed
                VeriTmpLibrary *tmp_lib = new VeriTmpLibrary(work_library) ;
                veri_file::AddLibrary(tmp_lib) ;

#ifdef VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM
                // VIPER #7404: Ignore "cannot open verilog file %s" message, we are not doing stat() on the file.
                // So, it may be a directory or other non-regular file. Avoid the error here:
                msg_type_t org_type = Message::GetMessageType("VERI-1517") ; // Save the current type
                (void) Message::SetMessageType("VERI-1517", VERIFIC_IGNORE) ;

                // Add the directory part to the filename now that we are going to analyze it:
                char *tmp = file_name ;
                file_name = Strings::save(dir, "/", file_name) ;
                Strings::free(tmp) ;
#endif
                if (!tmp_lib->ParseFile(file_name)){
#ifdef VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM
                    // VIPER #8129: Properly restore the message type:
                    if (org_type==VERIFIC_NONE) Message::ClearMessageType("VERI-1517") ; else (void) Message::SetMessageType("VERI-1517", org_type) ; // Restore old type
#endif
                    Strings::free(file_name) ;
                    veri_file::RemoveLibrary(tmp_lib) ;
                    delete tmp_lib ;
                    continue ;
                }
#ifdef VERILOG_IMPROVE_Y_DIR_PROCESSING_RUNTIME_FOR_NETWORK_FILE_SYSTEM
                // VIPER #8129: Properly restore the message type:
                if (org_type==VERIFIC_NONE) Message::ClearMessageType("VERI-1517") ; else (void) Message::SetMessageType("VERI-1517", org_type) ; // Restore old type
#endif

                Strings::free(file_name) ;

                // Check to see if "module_name" exists
                Map *tmp_lib_module_table = tmp_lib->GetModuleTable() ;
                VeriModule *tmp_module = (tmp_lib_module_table) ? (VeriModule *)tmp_lib_module_table->GetValue(module_name) : 0 ;
                if (!tmp_module) {
                    veri_file::RemoveLibrary(tmp_lib) ;
                    delete tmp_lib ;
                    continue ;
                }

                // pick required modules from tmp_lib
                // VIPER #3999 : In directory option consider the whole file, donot ignore modules defined before the
                // current required module 'module_name'
                // FIXME : When the changed behavior will be accepted for all, we can remove the last argument of
                // routine 'PickRequiredModules'
                // VIPER #7239: Looks like all the simulators have changed their behaviour now and do not ignore
                // the modules before the module to be resolved. So, it is time we change our behaviour too:
                // VIPER #7740: With fix for this issue, the above functionality has also been disabled inside the function.
#if 0 // Disable it now (VIPER #7239)
                if (PickRequiredModules(tmp_lib, work_library, ((tmp_lib_module_table) ? tmp_lib_module_table->IndexOf(module_name) : 0), 0 /* not v-file-option */)) {
#else
                if (PickRequiredModules(tmp_lib, work_library, 0, 0 /* not v-file-option */)) {
#endif
                    module_is_resolved = 1 ;
                }

                veri_file::RemoveLibrary(tmp_lib) ;
                delete tmp_lib ;
            } else {
                Strings::free(file_name) ;
            }
        }
    }

    return module_is_resolved ;
}

// Select required modules from the library where file/dir
// option specific file(s) are analyzed.
unsigned VeriUserLibrary::PickRequiredModules(VeriTmpLibrary *tmp_lib, VeriLibrary *work_library, unsigned starting_position, unsigned v_file_option)
{
    unsigned module_is_resolved = 0 ;
    if (!tmp_lib) return module_is_resolved ;

    Set dependent_scopes(POINTER_HASH) ;
    VeriScope *module_scope ;
    Map *tmp_lib_module_table = tmp_lib->GetModuleTable() ;
    char *module_name ;
    unsigned long valid_module_name ;
    MapIter iter ;

    // VIPER #8343: Iterate over the smaller Map:
    if (tmp_lib_module_table && _module_names_to_lookup && (tmp_lib_module_table->Size() < _module_names_to_lookup->Size())) {
        unsigned old_size = _module_names_to_lookup->Size() ;
        VeriModule *module ;
        FOREACH_MAP_ITEM(tmp_lib_module_table, iter, &module_name, &module) {
            if (!module_name || !module) continue ;
            valid_module_name = (unsigned long)_module_names_to_lookup->GetValue(module_name) ;
            if (!valid_module_name) continue ;

            module_is_resolved += PickRequiredModulesInternal(module_name, &dependent_scopes, tmp_lib, work_library, starting_position, v_file_option) ;
        }

        // VIPER #3999: We may have added further unresolved modules from above PickRequiredModulesInternal() call.
        // Some of them could be defined in the current file itself. Resolve them here now:
        unsigned new_size = _module_names_to_lookup->Size() ;
        while (new_size > old_size) {
            MapItem *item ;
            // Only check the newly added module names:
            for (unsigned i=old_size; i<new_size; i++) {
                item = _module_names_to_lookup->GetItemAt(i) ;
                VERIFIC_ASSERT(item) ;
                valid_module_name = (unsigned long)item->Value() ;
                if (!valid_module_name) continue ;

                module_name = (char *)item->Key() ;
                module_is_resolved += PickRequiredModulesInternal(module_name, &dependent_scopes, tmp_lib, work_library, starting_position, v_file_option) ;
            }
            old_size = new_size ;
            new_size = _module_names_to_lookup->Size() ;
        }
    } else {
        FOREACH_MAP_ITEM(_module_names_to_lookup, iter, &module_name, &valid_module_name) {
            if (!valid_module_name) continue ;

            module_is_resolved += PickRequiredModulesInternal(module_name, &dependent_scopes, tmp_lib, work_library, starting_position, v_file_option) ;
        }
    }

    // Now, pick the module on which other modules which are taken depends:
    SetIter si ;
    FOREACH_SET_ITEM(&dependent_scopes, si, &module_scope) {
        VeriIdDef *mod_id = (module_scope) ? module_scope->GetOwner() : 0 ;
        VeriModule *mod = (mod_id) ? mod_id->GetModule() : 0 ;
        if (!mod || (mod->GetLibrary() != tmp_lib)) continue ;
        (void) tmp_lib->DetachModule(mod) ; // remove the handle from the module table
        // VIPER #6223: If module cannot be added into the library, skip further processing:
        if (!AddModule(mod)) continue ; // and add to this library
        // VIPER 5182: We have to call Resolve now that we choose it, since that was not called when the module went into the tmp library.
        mod->ResolveBody() ;
    }
    // Viper 6100: If the original design do not contain a root scope declaraion
    // but the library file has a root scope declaration but no module definition
    // then the root module is created in the tmp library and as there is no dependent
    // module (because there is no module defined in the file) this gets deleted. This
    // results in a new scope to be created every time this scenario occurs. And hence
    // we get undeclared error.
    if (veri_file::IsMultiFileCompilationUnitMode()) {
        MapIter mi ;
        VeriModule *module ;
        FOREACH_VERILOG_MODULE_IN_LIBRARY(tmp_lib, mi,module) {
            if (!module) continue ;
            if (module->IsRootModule()) {
                (void) tmp_lib->DetachModule(module) ; // remove the handle from the module table
                (void) veri_file::AddRootModuleToLib(module, this) ; // and add to this library
                break ;
            }
        }
    }

    return module_is_resolved ;
}

unsigned
VeriUserLibrary::PickRequiredModulesInternal(const char *module_name, Set *dependent_scopes, const VeriTmpLibrary *tmp_lib, VeriLibrary *work_library, unsigned /*starting_position*/, unsigned v_file_option)
{
    unsigned module_is_resolved = 0 ;
    VeriScope *module_scope ;

    Map *tmp_lib_module_table = (tmp_lib) ? tmp_lib->GetModuleTable() : 0 ;

#if 0
    // Looks like we do not need this code anymore, commented out:
    if (_module_table && _module_table->GetValue(module_name)) {
        // module name exists in this library.So ignore it
        RemoveModuleName(module_name) ;
        return module_is_resolved ;
    }
#endif

    // VIPER #3048: Look into 'work' library if this library was not specified from `uselib
    // VIPER #6679: Also check if this module is in -L specific libraries:
    if (!_tick_uselib && ((work_library && work_library->GetModule(module_name, 1)) || veri_file::GetModuleFromLOptions(module_name))) {
        // module exists in work library. So ignnore it
        RemoveModuleName(module_name) ;
        return module_is_resolved ;
    }

    // VIPER #6908: Do not try to resolve -v file option added instantiated module in this library (from this file) when librescan is active:
    // Looks like other tools does this only for -v file option. For -y dir option they continue to resolve.
    if (_lib_rescan_active && v_file_option && _module_names_added && _module_names_added->GetItem(module_name)) return module_is_resolved ;

    VeriModule *module = (tmp_lib_module_table) ? (VeriModule*)tmp_lib_module_table->GetValue(module_name) : 0 ;
    // Only pick modules after the starting position specified:
    // VIPER #7740: Disable checking position of module, we anyway disabled the functionality with VIPER #7239:
    if (module && tmp_lib_module_table /*&& (tmp_lib_module_table->IndexOf(module_name) >= starting_position)*/) {
        //Module is found.
        module_is_resolved = 1 ;

        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("      Resolving module ",module->GetName()) ;
        VeriTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        tmp_print_node.Comment("      Resolving module %s", module->GetName()) ;

        // Move the module to this library
        (void) tmp_lib_module_table->Remove(module_name) ; // remove the handle from the module table
        // VIPER #6223: If module cannot be added into the library, skip further processing:
        if (!AddModule(module)) return module_is_resolved ; // and add to this library

        // VIPER 5182: We have to call Resolve now that we choose it, since that was not called when the module went into the tmp library.
        module->ResolveBody() ;

        // Find all the scopes (recursively) on which this module depends (after we resolved the body):
        module_scope = module->GetScope() ;
        if (module_scope && dependent_scopes) module_scope->DependentScopes(*dependent_scopes) ;

        RemoveModuleName(module_name) ;
        return module_is_resolved ;
    }

    // VIPER #3048: Look into 'work' library if this library was specified from `uselib
    // VIPER #6679: Also check if this module is in -L specific libraries:
    if (_tick_uselib && ((work_library && work_library->GetModule(module_name, 1)) || veri_file::GetModuleFromLOptions(module_name))) {
        // module exists in work library. So ignnore it
        RemoveModuleName(module_name) ;
    }

    return module_is_resolved ;
}

// Function Name: VeriUserLibrary::AddModuleName
// Arguments    : 1) const char * name- name of the module
// Return Type  : void
// Description  : Add the module name in the set _module_names_to_lookup
void
VeriUserLibrary::AddModuleName(const char *module_name)
{
    if (!_module_names_to_lookup) _module_names_to_lookup = new Map(STRING_HASH) ;

    // Insert only if the name was not present earlier
    if (!_module_names_to_lookup->GetItem(module_name)) {
        (void) _module_names_to_lookup->Insert(Strings::save(module_name), (void*)1L) ; // insert 1 (long for hash table) to indicate this is a required module
        // VIPER #7740: We are adding a module name to be looked up, increase by one:
        _num_modules_to_lookup++;
        // VIPER 2132 : We are processing uselib
        // VIPER #3107 : Add module name if it is not already in '_module_names_to_lookup'
        if (_module_names_added) (void) _module_names_added->Insert(Strings::save(module_name)) ; // Add module name
    }
}

// Function Name: VeriUserLibrary::RemoveModuleName
// Arguments    : 1) const char * module_name - name of the module
// Return Type  : void
// Description  : Insert '0' as the value of the argument specific key.
//                Value '0' indicates that this key specific module is
//                resolved.
void
VeriUserLibrary::RemoveModuleName(const char *module_name)
{
    if (!_module_names_to_lookup) return ;

    // Get the inserted key and force-overwrite with that key.
    MapItem *item = _module_names_to_lookup->GetItem(module_name) ;
    char *saved_module_name = item ? (char*)item->Key() : 0 ;
    if (saved_module_name) {
        // Do a 'force-overwrite' (third argument is 1) to overwrite the 'value' under this key if it exists.
        (void) _module_names_to_lookup->Insert(saved_module_name, (void*)0L, 1) ; // insert 0 (long for hash table) to indicate this module no longer is needed.
        // VIPER #7740: This module is resolved, decrease by one:
        _num_modules_to_lookup--;
    }

    // VIPER 2132 : Module is resolved, remove from '_module_names_added' set
    if (_module_names_added) {
        SetItem *sitem = _module_names_added->GetItem(module_name) ;
        saved_module_name = sitem ? (char*)sitem->Key() : 0 ;
        (void) _module_names_added->Remove(saved_module_name) ;
        Strings::free(saved_module_name) ;
    }
}

// Function Name: VeriUserLibrary::RemoveAllModuleNames
// Arguments    : none
// Return Type  : void
// Description  : Removes all module names from _module_names_to_lookup
//                and deletes the set
void
VeriUserLibrary::RemoveAllModuleNames()
{
    if (!_module_names_to_lookup) return ;

    char *key ;
    MapIter iter ;
    FOREACH_MAP_ITEM(_module_names_to_lookup, iter, &key, 0) {
        Strings::free(key) ;
    }

    delete _module_names_to_lookup ;
    _module_names_to_lookup = 0 ;
    _num_modules_to_lookup = 0 ; // VIPER #7740
}

// VIPER #8405: Check if this module is required:
unsigned
VeriUserLibrary::IsUnresolvedModule(const char *module_name) const
{
    if (_tick_uselib && IsEmptyUseLib()) return 1 ; // Treat all modules are required for empty `uselib

    if (_module_names_to_lookup && _module_names_to_lookup->GetValue(module_name)) return 1 ; // This module is required

    return 0 ; // Otherwise, this module is not required
}

// VIPER 2132 : Remove module names added in a separate set while processing -y/-v options
void
VeriUserLibrary::RemoveNewModuleNames()
{
    if (!_module_names_added) return ; // Nothing to remove

    char *key ;
    SetIter si ;
    FOREACH_SET_ITEM(_module_names_added, si, &key) Strings::free(key) ; // Free items

    delete _module_names_added ; // Delete the set
    _module_names_added = 0 ;
}

void
VeriUserLibrary::RemoveDirFileStatus()
{
    MapIter mi ;
    char *dir ;
    Map *file_stat ;
    FOREACH_MAP_ITEM(_dir_file_status, mi, &dir, &file_stat) {
        Strings::free(dir) ;
        MapIter mii ;
        char *file ;
        FOREACH_MAP_ITEM(file_stat, mii, &file, 0) Strings::free(file) ;
        delete file_stat ;
    }
    delete _dir_file_status ;
    _dir_file_status = 0 ;
}

// Function Name: VeriUserLibrary::GetModuleInternal
// Arguments    : const char * module_name - Name of module
// Return Type  : VeriModule *
// Description  : This function takes a module name and returns the VeriModule*
//                that has name module_name inside this library. If the library
//                has not been processed earlier, it is processed first.
VeriModule *
VeriUserLibrary::GetModuleInternal(const char *module_name, unsigned case_sensitive, unsigned restore)
{
    // If there are unresolved module names in this library,
    // then we need to first resolve there (search -v/-y/uselib files/directories) :
    if (_module_names_to_lookup) {
        Process() ;
        //RemoveAllModuleNames() ;
    }

    if (!_uselib_lib_mod_lib_map || !_tick_uselib_lib) return VeriLibrary::GetModuleInternal(module_name, case_sensitive, restore) ;

    const char *lib_name = (const char *) _uselib_lib_mod_lib_map->GetValue(module_name) ;
    if (!lib_name) return 0 ;

    VeriLibrary *uselib_lib = veri_file::GetLibrary(lib_name) ;
    if (!uselib_lib) return 0 ;

    return uselib_lib->GetModule(module_name, case_sensitive, restore) ;
}

void
VeriUserLibrary::AddDirOption(const char *dir )
{
    // `uselib lib support
    if (_tick_uselib_lib) {
        VeriTreeNode dummy_node ;
        dummy_node.Error("illegal use of `uselib directive : cannot mix lib with file|dir|libext") ;
    }
    // end `uselib lib support

    VeriDirOption *option = new VeriDirOption(dir) ;
    InsertFileDirLibOption(option) ;
}

void
VeriUserLibrary::AddFileOption(const char *file )
{
    // `uselib lib support
    if (_tick_uselib_lib) {
        VeriTreeNode dummy_node ;
        dummy_node.Error("illegal use of `uselib directive : cannot mix lib with file|dir|libext") ;
    }
    // end `uselib lib support

    VeriFileOption *option = new VeriFileOption(file) ;
    InsertFileDirLibOption(option) ;
}

// `uselib lib support
void
VeriUserLibrary::AddLibOption(const char *lib )
{
    if ((!_tick_uselib_lib && _all_files_dirs && _all_files_dirs->Size() != 0) || (_libext && _libext->Size() != 0)) {
        // This lib has other non-`uselib-lib elements
        VeriTreeNode dummy_node ;
        dummy_node.Error("illegal use of `uselib directive : cannot mix lib with file|dir|libext") ;
    }

    VeriLibOption *option = new VeriLibOption(lib) ;
    InsertFileDirLibOption(option) ;

    _tick_uselib_lib = 1 ;
}

VeriLibraryDecl::VeriLibraryDecl(char *name, char *pattern, linefile_type linefile /* = 0 */) : VeriTreeNode(linefile),
    _name(name),         // absorb the string
    _patterns(0)
{
    // Create the Array of patterns of size 1 for a single pattern:
    _patterns = new Array(1) ;

    // Format the patterns to absolute path form for wild card matching. This also
    // removes the ../ or ./ from the path, so that matching becomes simpler.
    char *formatted_pattern = GetFormattedPath(pattern, 0 /* make absolute path using directory of the current file */) ;
    if (formatted_pattern) {
        _patterns->Insert(formatted_pattern) ; // Insert the formatted pattern
        Strings::free(pattern) ; // Free the original pattern
    } else {
        Error("cannot format %s %s to standard absolute path form", "pattern", pattern) ;
        _patterns->Insert(pattern) ; // Insert the original pattern
    }
}

VeriLibraryDecl::VeriLibraryDecl(char *name, Array *patterns, linefile_type linefile /* = 0 */) : VeriTreeNode(linefile),
    _name(name),         // absorb the string
    _patterns(patterns)  // absorb the Array
{
    // Format the patterns to absolute path form for wild card matching. This also
    // removes the ../ or ./ from the path, so that matching becomes simpler.
    char *formatted_pattern = 0 ;
    unsigned i ;
    char *pattern ;
    FOREACH_ARRAY_ITEM(_patterns, i, pattern) {
        if (!pattern) continue ;
        // Format the pattern using the directory of the current file:
        formatted_pattern = GetFormattedPath(pattern, 0 /* make absolute path using directory of the current file */) ;
        if (!formatted_pattern) {
            Error("cannot format %s %s to standard absolute path form", "pattern", pattern) ;
            continue ;
        }
        // Overwrite the pattern with this one:
        _patterns->Insert(i, formatted_pattern) ;
        // Free the original pattern:
        Strings::free(pattern) ;
    }
}

VeriLibraryDecl::~VeriLibraryDecl()
{
    Strings::free(_name) ;
    _name = 0 ;

    // Free the patterns and the container Array:
    unsigned i ;
    char *pattern ;
    FOREACH_ARRAY_ITEM(_patterns, i, pattern) Strings::free(pattern) ;
    delete _patterns ;
}

unsigned
VeriLibraryDecl::MatchPatterns(const char *file_name) const
{
    if (!file_name || !_patterns || !_patterns->Size()) return 0 ;

    unsigned max_precedence = 0 ;        // Maximum precedence in which we found a match
//    unsigned same_precedence_match = 0 ; // Whether the maximum precedence is matched more than once

    char *formatted_file_name = GetFormattedPath(file_name, 1 /* make absolute path using CWD */) ;
    if (!formatted_file_name) {
        Error("cannot format %s %s to standard absolute path form", "file", file_name) ;
        return 0 ; // Can't continue without a formatted file path
    }

    // Match each of the patterns in this library declaration:
    unsigned i ;
    char *pattern ;
    FOREACH_ARRAY_ITEM(_patterns, i, pattern) {
        if (!pattern) continue ;

        // Compare the absolute forms using wild card matching, this returns the precedence of the match.
        unsigned precedence = WildMatch(pattern, formatted_file_name) ;

        // Check for match, if not matched, precedence is zero:
        if (precedence) {
            // Note: ModelSim seem to never print this message. Not even with -libmap_verbose option.
            Info("comparing file %s to library pattern %s: matched library %s", formatted_file_name, pattern, _name) ;
            if (precedence > max_precedence) {
                // Found a new maximum precedence match:
                max_precedence = precedence ; // Set this to the new maximum
//                same_precedence_match = 0 ;   // Reset this flag to avoid illegal error
//            } else if (precedence == max_precedence) { // Note: 'precedence' is not zero here
//                same_precedence_match = 1 ;   // Maximum precedence match has another match
            }
        } else {
            // Note: ModelSim only prints this info message with -libmap_verbose option
            Info("comparing file %s to library pattern %s: did not match", formatted_file_name, pattern) ;
        }
    }

    // Clean up the formatted file path:
    Strings::free(formatted_file_name) ;

    // Do not error out, both are matched in the same 'this' library:
    // If we are matched more than once for the maximum precedence match, error out here:
//    if (same_precedence_match) Error("file %s matched multiple library patterns", file_name) ;

    // Return this local maximum matched precedence from here, so that we can check simialr
    // situation also in the upper level caller and produce the error message there:
    return max_precedence ;
}

// This routine does three things to simplify the path keeping the meaning same:
// 1. Makes the path absolute (either from current directory or the libmap directory)
//    and also conpact it by removing /./ and /../ and adjusting it necessarily.
// 2. compact the wild cards like: "**" to "*" and "*?" to "?*" (thus "?*?" to "??*")
//    and also the path separator characters.
char *
VeriLibraryDecl::GetFormattedPath(const char *file_name, unsigned make_absolute_using_cwd) const
{
    if (!file_name) return 0 ;

    // 1. First make the path absolute:
    char *file_path_name = 0 ;
    if (!make_absolute_using_cwd) {
        // Here, we need to make the path absolute by using the path of the file where
        // "this" was declared. So, get the name of the file from line-file information:
        const char *current_filename = LineFile::GetFileName(Linefile()) ;
        if (!current_filename) return 0 ; // Can't do anything without the file name itself

        // Find the last occurrence of the directory separator in the 'current_filename':
        const char *sep = 0 ;
        for (sep = current_filename + Strings::len(current_filename) ; sep >= current_filename ; sep--) {
            // Check the last directory separator:
            if (IS_PATH_SEPARATOR(*sep)) break ;
        }
        if (sep < current_filename) sep = 0 ;

        if (sep) {
            // Since 'sep' is valid, it has a directory part, take only that directory:
            char *dir_name = Strings::save(current_filename) ;
            dir_name[sep - current_filename] = '\0' ; // Strip off the file name
            file_path_name = Strings::save(dir_name, "/", file_name) ;
            Strings::free(dir_name) ;
        }
    }

    // Now make this file path or file name absolute using CWD and compact the path:
    char *full_name = FileSystem::Convert2AbsolutePath(((file_path_name) ? file_path_name : file_name), 1 /* compact */) ;
    Strings::free(file_path_name) ; // Free the created file path, if any

    // 2. Now, compact the wild cards so that we can match easily:
    while (CompactWildCards(full_name)) {
        /* If the string is modified, we can still compact it */
    }

    return full_name ;
}

// It compacts the wild cards like: "**" to "*" and "*?" to "?*" (thus "?*?" to "??*")
// and also the path separator characters.
unsigned
VeriLibraryDecl::CompactWildCards(char *path) const
{
    if (!path) return 0 ;

    unsigned org_idx = 0 ;
    unsigned new_idx = 0 ;
    unsigned modified = 0 ;

    while (path[org_idx] != '\0') {
        // Check if it is a '*' character:
        if (path[org_idx] == '*') {
            // Check the previous character whether it was also a '*' character:
            if (new_idx && (path[new_idx-1] == '*')) {
                org_idx++ ;    // Skip this consecutive repeated '*' character
                modified = 1 ; // The string will be modified since we skipped it
                continue ;
            }

            // Current character is '*', check whether the next character is a '?'.
            // Then we will replace this occurrence of "*?" with "?*":
            if (path[org_idx+1] == '?') {
                path[new_idx++] = '?' ;
                path[new_idx++] = '*' ;
                org_idx += 2 ; // Skip both the '*' and the '?' characters, we have alredy taken them in
                modified = 1 ; // The string will be modified since we interchanged '*' and '?'
                continue ;
            }
        } else if (IS_PATH_SEPARATOR(path[org_idx])) {
            // Check the previous character whether it was also a path separator:
            if (new_idx && IS_PATH_SEPARATOR(path[new_idx-1])) {
                org_idx++ ;    // Skip this consecutive repeated path separator
                modified = 1 ; // The string will be modified since we skipped it
                continue ;
            }
        }

        // Copy this character (once for repeated *'s):
        path[new_idx++] = path[org_idx++] ;
    }

    path[new_idx] = '\0' ; // Terminate the string here

    return modified ; // Return the status
}

// Actual routine to match the wild cards with a file path. See Section 13.2.1 on
// "Specifying libraries" of IEEE Verilog 1364 LRM for the wild cards and the rules.
unsigned
VeriLibraryDecl::WildMatch(const char *pattern, const char *file_name) const
{
    if (!pattern || !file_name) return 0 ;

    // "pattern" should not have any substring which is: "/../", we have already removed it.

    unsigned file_name_has_wildcard = 0 ;

    // Try matching the pattern with the file name:
    while ((*pattern != '\0') && (*file_name != '\0')) {
        // Skip all matching characters that are not wild cards:
        if ((*pattern != '*') && (*pattern != '?') &&
#ifdef WINDOWS
            // VIPER #7139: FileSystem::Convert2AbsolutePath does not lower the path names anymore, use tolower here:
            (::tolower(*pattern) == ::tolower(*file_name))
#else
            ((*pattern) == (*file_name))
#endif
        ) {
            // Reset this flag if we got a path separator:
            if (IS_PATH_SEPARATOR(*file_name)) file_name_has_wildcard = 0 ;
            // Skip the character in both pattern and file name:
            pattern++ ;
            file_name++ ;
            continue ; // Check the next set or characters
        }

        // Check whether we are at '*' or at the "..." specification, which matches
        // to any number of directories in the hierarchy below:
        if ((*pattern == '*') || (pattern == ::strstr(pattern, ANY_HIERARCHICAL_PATH_STR))) {
            // If the "*/" was at this position of the pattern:
            if ((*pattern == '*') && IS_PATH_SEPARATOR(*(pattern+1))) {
                // File name should resume after a path separator:
                const char *tmp = 0;
                // Get the next path separator pointer from 'file_name' into in 'tmp':
                tmp = GET_PATH_SEPARATOR(file_name /* starting at */) ;
                if (!tmp) return 0 ; // No path separator found in the file name: do not match
                file_name = tmp ;
                pattern++ ; // Skip this '*'
                continue ;
            }

            // If '*' was the last character in the pattern:
            if ((*pattern == '*') && (*(pattern+1) == '\0')) {
                // File name should not have a path separator in it:
                if (GET_PATH_SEPARATOR(file_name /* starting at */)) return 0 ; // Do not match, it has a path separator

                return 2 ; // Pattern ends with a wild card, precedence of match is 2
            }

            // Need to skip this part and find where the rest part which matches with the file name:
            if (*pattern == '*') {
                pattern++ ; // Skip this '*'
                file_name_has_wildcard = 1 ; // Set this flag here
            } else {
                pattern += Strings::len(ANY_HIERARCHICAL_PATH_STR) ; // Skip this "..."
            }

            if ((*pattern) == '\0') {
                // Don't know what to do if nothing more to find
                return 0 ; // Probably they do not match
            }

            // Find out the part after this wild card which match both in file name and in pattern.
            // Copy the pattern over to a temporary string to find the matching part:
            char *temp = Strings::save(pattern) ;

            // Terminate at the next wild card:
            char *tp = ::strchr(temp, '*') ;
            if (tp) *tp ='\0' ;       // Terminate at '*'
            tp = ::strchr(temp, '?') ;
            if (tp) *tp ='\0' ;       // Terminate at '?'
            // Get the next path separator pointer from '(temp+1)' into in 'tp':
            tp = GET_PATH_SEPARATOR((temp+1) /* starting at */) ;
            if (tp) *tp ='\0' ;       // Terminate at '/' or '\\'
            tp = ::strstr(temp, ANY_HIERARCHICAL_PATH_STR) ;
            if (tp) *tp ='\0' ;       // Terminate at '...'

            if ((*temp) == '\0') {
                // Don't know what to do if no matching substring is found:
                Strings::free(temp) ; // Clean up
                return 0 ; // Probably they do not match
            }

            if (IS_PATH_SEPARATOR(*temp) && *(temp+1) == '\0') {
                // After this ..., there is a wild card, so skip the next directory
                // in file name and try matching with the pattern:
                Strings::free(temp) ; // Clean up
                const char *next_dir = 0 ;
                next_dir = GET_PATH_SEPARATOR(file_name /* starting at */) ;
                while (next_dir) {
                    next_dir++ ; // Go past the path separator character
                    // Try to match this path with the pattern:
                    unsigned match = WildMatch(pattern+1, next_dir) ;
                    if (match) return match ; // If matched, return
                    // Otherwise try from the next directory in file name:
                    next_dir = GET_PATH_SEPARATOR(next_dir /* starting at */) ;
                }

                return 0 ; // Do not match
            }

            // Now the part after "..." or '*' and before any of the above wild cards
            // should be present in the file name to match, check it:
            const char *resume_at = ::strstr(file_name, temp) ;
            Strings::free(temp) ; // Clean up
            if (!resume_at) return 0 ; // No substring present, they do not match

            if (*(pattern-1) == '*') {
                // If pattern was '*' then it cannot include a path separator, check it:
                const char *p = file_name ;
                while (resume_at > p) {
                    if (IS_PATH_SEPARATOR(*p)) return 0 ; // Do not match
                    p++ ; // Check the next character
                }
            }

            // Start from this matching position and check the rest
            file_name = resume_at ;
        } else if (*pattern == '?') {
            // Check whether it is a part of the path, not a separator:
            if (IS_PATH_SEPARATOR(*file_name)) return 0;

            // Skip one character each in both pattern and the file name:
            pattern++ ;
            file_name++ ;
            file_name_has_wildcard = 1 ; // Set this flag here
        } else {
            // If the two character does not match, and the pattern is not "..." or '*',
            // then probably they do not match. No need to check further, return from here:
            return 0 ;
        }
    }

    // At-least one of the pattern of file name ended here.
    if (*pattern == '\0') {
        // Check, if both: return according to the precedence:
        if (*file_name == '\0') return ((file_name_has_wildcard) ? 2 : 3) ;

        // Check if the last character in pattern was a path separator:
        if (IS_PATH_SEPARATOR(*(pattern-1))) {
            // In that case check whether the left over file name contains any path
            // separator, if it is, then they do not match:
            if (GET_PATH_SEPARATOR(file_name /* starting at */)) return 0 ;

            // No more path separator present in the file name, they matches:
            return 1 ; // Pattern ends with a directory, lowest precedence match
        }
    }

    // Here they do not match:
    return 0 ;
}

unsigned
VeriUserLibrary::IsEmptyUseLib() const
{
    return (!_all_files_dirs || !_all_files_dirs->Size()) ? 1 : 0 ;
}

// VIPER #4933: Verific's internal routine to get the instantiated module names:
void
VeriLibrary::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) const
{
    if (!done_libs.Insert(this)) return ; // Already done
    if (!_module_table || !_module_table->Size()) return ; // No modules in this library

    VeriModule *module ;
    MapIter mi ;
    // Traverse all modules of library 'work_lib'
    FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi, module) {
        if (!module) continue ;
        if (module->GetQualifier(VERI_EXTERN)) continue ;
        // Populate instantiated modules' names for each module
        module->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    }

    // Ignore the 'def_mods' set for non-user-library.
}

void
VeriUserLibrary::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) const
{
    if (!done_libs.Insert(this)) return ; // Already done
    if (!_module_table || !_module_table->Size()) return ; // No modules in this library

    // Create a local sets of instantiated and defined module names:
    Set this_lib_mod_names(STRING_HASH) ;
    Set other_def_mods(STRING_HASH) ;

    VeriModule *module ;
    MapIter mi ;
    // Traverse all modules of library 'work_lib'
    FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi, module) {
        if (!module) continue ;
        if (module->GetQualifier(VERI_EXTERN)) continue ;
        // Populate instantiated modules' names for each module
        module->AccumulateInstantiatedModuleNames(this_lib_mod_names, done_libs, other_def_mods) ;
    }

    // Now, store only the unresolved instantiated module names into 'mod_names':
    SetIter si ;
    const char *name ;
    FOREACH_SET_ITEM(&this_lib_mod_names, si, &name) {
        // VIPER #3048: Look for both this library and all defined modules below:
        if (!_module_table->GetItem(name) && !other_def_mods.GetItem(name)) {
            // This module is not locally resolved within this library:
            (void) mod_names.Insert(name) ;
        }
    }

    // Now fill-in the locally defined modules in this user library. All the modules in
    // the user library is always used otherwise they would have got deleted. So, they
    // may be used up in the hierarchy if it came from another user library (VIPER #3048).
    FOREACH_VERILOG_MODULE_IN_LIBRARY(this, mi, module) if (module) (void) def_mods.Insert(module->Name()) ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
VhdlPrimaryUnit *
VeriUserLibrary::GetVhdlUnitFromTickUseLibLib(const char *unit_name, char **arch_name) const
{
    if (!_tick_uselib_lib || !unit_name) return 0 ;

    VhdlPrimaryUnit *vhdl_unit = 0 ;

    // VIPER #7105: Get the VHDL unit from `uselib lib directive:
    unsigned i ;
    VeriFileDirLibOption *lib_option ;
    FOREACH_ARRAY_ITEM(_all_files_dirs, i, lib_option) {
        if (!lib_option || !lib_option->IsLibOption()) continue ;
        const char *lib_name = lib_option->GetName() ; // Name of the library
        vhdl_unit = VeriNode::GetVhdlUnitForVerilogInstance(unit_name, lib_name, 0, arch_name) ; // Get the primary unit from the VHDL library
        if (vhdl_unit) break ; // Found. FIXME: If more than one unit in different `uselib lib
    }

    return vhdl_unit ;
}
#endif

