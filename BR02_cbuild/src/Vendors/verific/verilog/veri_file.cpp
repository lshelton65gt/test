/*
 *
 * [ File Version : 1.306 - 2014/03/20 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>    // For strrchr, strtok
#include <stdio.h>     // For sprintf

#include <ctype.h>     // For isalnum

#include "veri_file.h"
#include "Array.h"
#include "Strings.h"
#include "RuntimeFlags.h" // for run-time flags
#include "Map.h"
#include "Set.h"
#include "Message.h"
#include "FileSystem.h"
#include "LineFile.h"
#include "Protect.h"

#include "VeriModule.h"
#include "VeriId.h"
#include "VeriExpression.h"
#include "VeriLibrary.h"
#include "VeriScope.h"
#include "veri_tokens.h"
#include "VeriElab_Stat.h" // for VeriStaticElaborator::ResetStatics
#include "VeriVarUsage.h"  // for VeriVarUsageInfo::ResetAssignedInfo

#ifdef VERILOG_FLEX_READ_FROM_STREAM
#include <VerificStream.h> // For reading from streams
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// VIPER #7175: Comment string to be added with VeriModule::TbdmAdd*() and VeriModule::TbdmChange*() APIs:
// We made this programmable by adding API veri_file::SetTbdmCommentString(). This is the default string:
#define VERILOG_TBDM_COMMENT_STRING "/* Verific */"

/************************ Static Variables ******************/
// RD: 5/2012: Many runtime flags moved to utility "RuntimeFlags".

// Here we place all the libraries that we encounter. We start off
// with the work library.
Map * veri_file::_all_libraries = 0 ; // new Map(STRING_HASH) ;

// _parsed_expr stores all the newly parsed expression parsed by veri expression parser
VeriExpression * veri_file::_parsed_expr = 0 ;
VeriLibrary * veri_file::_work_library = 0 ;
VeriLibrary * veri_file::_current_user_library = 0 ;
// VIPER #5659: The -y/-v specific user library:
VeriLibrary * veri_file::_yv_user_library = 0 ;

// Dialect used during analysis :
unsigned veri_file::_analysis_mode = VERILOG_2K ; // Verilog 2001 is the default
unsigned veri_file::_default_analysis_mode = UNDEFINED ; // VIPER #5434: Default analysis mode (undefined by default)

// Compilation unit Mode, used during analysis :
unsigned veri_file::_compilation_unit_mode = MFCU ; // multi-file compilation unit is default

// VIPER #5932: Flag to indicate that we are processing -y/-v/uselib
unsigned veri_file::_processing_user_library = 0 ;

// Viper #6212: begin
// RD: 2/2011: Set runtime flags on by default, so that compile flags are by default effective
// RD: 6/2012: Many run-time variables moved to utility "RuntimeFlags".
// /*static*/ unsigned veri_file::_extract_mp_rams = 1 ; // Default on // RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.
// /*static*/ unsigned veri_file::_extract_rams = 1 ; // Default on // RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.
// Viper #6212: end

// The initialization of the macro variables
Map * veri_file::_macro_predefs = 0 ;
Map * veri_file::_macro_defs    = 0 ;
//CARBON_BEGIN
Map * veri_file::_carbon_macro_defs    = 0 ;
//CARBON_END
Map * veri_file::_macro_argdefs = 0 ;
Map * veri_file::_cmdline_macro_defs = 0 ;
#ifdef VERILOG_QUICK_PARSE_V_FILES
// Macros under quick parsing -v files:
Map * veri_file::_active_macro_defs    = 0 ;
Map * veri_file::_active_macro_argdefs = 0 ;
Set * veri_file::_active_macro_undefs = 0 ;
// Flag the quick parsing process:
unsigned veri_file::_quick_parsing = 0 ;
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
unsigned veri_file::_has_active_macro_ref = 0 ;
#endif
#endif // VERILOG_QUICK_PARSE_V_FILES

/* static */ Map *veri_file::_macro_def_linefiles  = 0 ; // VIPER #7459: char* -> linefile_type of macro defs
//CARBON_BEGIN
/* static */ Map *veri_file::_carbon_macro_def_linefiles  = 0 ; // VIPER #7459: char* -> linefile_type of macro defs
//CARBON_END
/* static */ Map *veri_file::_macro_ref_linefiles = 0 ;  // VIPER #7459: Store macro ref -> def locations

// Arrays that store user-given directory names :
Array *veri_file::_include_dirs = 0 ;

//CARBON_BEGIN
// Arrays that store user-given pragma trigger names :
Array *veri_file::_pragma_triggers = 0 ;
//CARBON_END

// Callback functions, registered by user :
unsigned (*veri_file::_interrupt_func)(linefile_type) = 0 ;
unsigned (*veri_file::_compile_as_blackbox_func)(const char *) = 0 ;

// System Verilog root module
VeriModule *veri_file::_root_module = 0 ;

Map* veri_file::_included_files = 0 ;

// Verilog 2000 library mapping items:
Array *veri_file::_library_decls = 0 ;
Map *veri_file::_matched_files = 0 ;

// VIPER #4115: File extension to language dialect selection:
Map *veri_file::_file_ext_vs_mode = 0 ;

// VIPER #2932. Initialize array for holding the file names during analysis.
Array *veri_file::_file_names = 0 ;

Set *veri_file::_ordered_libs = 0 ; // -L options :

// VIPER #7175: Comment string to be added with VeriModule::TbdmAdd*() and VeriModule::TbdmChange*() APIs:
/* static */ char *veri_file::_tbdm_comment_string = 0 ; //Strings::save(VERILOG_TBDM_COMMENT_STRING) ;

// VIPER : 2947/4152 - This variable, if set by user, will
// control the loop limit during elaboration.
// RD: 6/2012: Many run-time variables moved to utility "RuntimeFlags".
// unsigned veri_file::_loop_limit = 0 ; // RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.

// VIPER #7125: This variable, if set by user, will control the stack limit during elaboration.
// unsigned veri_file::_max_stack_depth = 0 ; // RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.

// VIPER #5980: Dynamic control for error message: array size is
// larger than 2**_max_array_size (VERI-1170)
// unsigned veri_file::_max_array_size = 23 ; // default value similar to vhdl
Array *veri_file::_top_mods = 0 ; // Top level modules

// unsigned veri_file::_minimum_ram_size = 0 ; // Viper #3903 // RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.

Protect *veri_file::_protect = 0 ;
Protect *veri_file::_tick_protect = 0 ;
Protect *veri_file::_pragma_protect = 0 ;

#ifdef VERILOG_FLEX_READ_FROM_STREAM
// VIPER #5726: Read from streams:
verific_stream *(*veri_file::_stream_callback_func)(const char *file_name) = 0 ;
#endif

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
// VIPER #5932: The Array into which we accumulate all the linefile objects created:
/* static */ Array *veri_file::_linefile_structs = 0 ;
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
/* static */ VeriStaticElaborator *veri_file::_static_elab = 0 ;
/* static */ unsigned veri_file::_ignore_library_mapping = 0 ; // VIPER #8123

/* static */ Set *veri_file::_ignored_module_names = 0 ; // VIPER #8405

/************************ Macro Access ******************/

unsigned
veri_file::DefineMacro(const char *macro_name, const char *macro_value, const char *macro_arguments, linefile_type lf)
{
    if (!macro_name) return 0 ;

    // Check if the macro has been defined on the  command line or not. This is
    // to prevent unnecessary insertion of command line macro in '_macro_defs'.
    if (_cmdline_macro_defs && _cmdline_macro_defs->GetItem(macro_name)) return 0 ;

    // Check for infinite recursion is being done at usage time now (VIPER #1859)
    char *key   = Strings::save(macro_name) ;
    char *value = Strings::save(macro_value) ;
    //CARBON_BEGIN
    char *carbon_key   = Strings::save(macro_name) ;
    char *carbon_value = Strings::save(macro_value) ;
    //CARBON_END
    char *args  = Strings::save(macro_arguments) ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    // If we are quick parsing, then do not touch the main macro
    // store. Instead insert into the active macro Maps:
    if (_quick_parsing) {
        // First undefine (remove only) the macro, if any:
        unsigned result = UndefineActiveMacro(macro_name, 1 /* remove only */) ;

        // Define new one in active store:
        if (!_active_macro_defs) _active_macro_defs = new Map(STRING_HASH) ;
        (void) _active_macro_defs->Insert(key, value) ;

        // active_macro_argdefs : shares 'key' with 'active_macro_defs'
        if (!_active_macro_argdefs) _active_macro_argdefs = new Map(STRING_HASH) ;
        if (args) (void) _active_macro_argdefs->Insert(key, args) ;

        // Return 1, if it is redefined (counting main macro store), 0 otherwise:
        if (!result && _macro_defs && _macro_defs->GetItem(macro_name)) result = 1 ;
        return result ;
    }
#endif // VERILOG_QUICK_PARSE_V_FILES
    // Undefine existing one (if there)
    unsigned result = UndefineMacro(macro_name) ;
    // Define new one
    if (!_macro_defs) _macro_defs = new Map(STRING_HASH) ;
    (void) _macro_defs->Insert(key, value) ;
    //CARBON_BEGIN 
    //We need to maintain separate macro table (and keep different line/file but same name macros, for future report in dumpHierarchy carbon flow.
    //Verific version of this containers used as standard requires and deleted at the end of compilation unit, so we need a specific version of this mapping to maintain backword compatability with Interra flow
    //create needed containers. We keep line/file info as a key - as this is unique and there can be only 1 macro in given line/file
    if ( lf ) {
        if (!_carbon_macro_def_linefiles) _carbon_macro_def_linefiles = new Map(POINTER_HASH) ;
        if (!_carbon_macro_defs) _carbon_macro_defs = new Map(POINTER_HASH) ;
        // if lf is null then this means that this method was called for an undefined macro (to control number of messages printed about this macro)
        // for such undefined macros there is no need to save any info in the _carbon_macro_def_linefiles
        MapIter mi ;
        linefile_type c_lf;
        char *c_name ;
        const char* abs_file_name = lf->GetAbsFileName();

        // first search to see if this macro (with name macro_name aka carbon_key) has been defined before in this same file, if so remeove
        // old definition.
        // (for this we need line/file info we don't do it in method:  UndefineMacro as verific does for it's containers)
        FOREACH_MAP_ITEM(_carbon_macro_def_linefiles, mi, &c_lf, &c_name) {
            // if c_lf is null this means that it is for a macro that was not defined, we can skip the search for it
            if ( c_lf ) {
                const char* c_abs_file_name = c_lf->GetAbsFileName();
                // replace (remove old) macro if it has same macro name and same macro file name (to match-up with Interra)
                if (Strings::compare(c_name, carbon_key) && Strings::compare(c_abs_file_name, abs_file_name)) {
                    MapItem* item_key = _carbon_macro_def_linefiles->GetItem(c_lf);
                    MapItem* item_value = _carbon_macro_defs->GetItem(c_lf);
                    // Save the key/value strings before the 'Remove' routine changes them.
                    char *key = (char*)item_key->Value() ;
                    char *value = (char*)item_value->Value() ;
                    // delete the macro entry
                    (void) _carbon_macro_defs->Remove(c_lf) ;
                    (void) _carbon_macro_def_linefiles->Remove(c_lf) ;
                    // delete key/value pair
                    Strings::free(key) ;
                    Strings::free(value) ;
                    break;
                }
            }
        }
        // Push new macro information.
        (void) _carbon_macro_def_linefiles->Insert((void *)lf, carbon_key) ; 
        (void) _carbon_macro_defs->Insert((void*)lf, carbon_value) ;
    }
    //CARBON_END

    // macro_argdefs : shares 'key' with 'macro_defs'
    if (!_macro_argdefs) _macro_argdefs = new Map(STRING_HASH) ;
    if (args) (void) _macro_argdefs->Insert(key, args) ;

    if (!_macro_def_linefiles) _macro_def_linefiles = new Map(STRING_HASH) ;
    (void) _macro_def_linefiles->Insert(key, (void *)lf) ;

    return result ; // 1 if it is overwritten, 0 if it is newly defined
}

unsigned
veri_file::DefineCmdLineMacro(const char *macro_name, const char *macro_value) // VIPER 2407
{
    if (!macro_name) return 0 ;

    unsigned result = 0 ;
    // Undefine existing command-line macro (if there)
    if (_cmdline_macro_defs) {
        MapItem *item = _cmdline_macro_defs->GetItem(macro_name) ;
        if (item) {
            char *key = (char*)item->Key() ;
            char *value = (char*)item->Value() ;
            // delete the macro entry
            (void) _cmdline_macro_defs->Remove(macro_name) ;
            // delete key/value pair
            Strings::free(key) ;
            Strings::free(value) ;
            result = 1 ;
        }
    }

    // Check for infinite recursion is being done at usage time now (VIPER #1859)
    char *key   = Strings::save(macro_name) ;
    char *value = Strings::save(macro_value) ;

    // Define new one
    if (!_cmdline_macro_defs) _cmdline_macro_defs = new Map(STRING_HASH) ;
    (void) _cmdline_macro_defs->Insert(key, value) ;

    return result ; // 1 if it is overwritten, 0 if it is newly defined
}

unsigned
veri_file::VerifyMacro(const char *macro_name, const char *macro_text, Set *visited_args /*=0*/)
{
    if (!macro_name) return 0 ;
#ifdef VERILOG_QUICK_PARSE_V_FILES
    if ((!_macro_defs && !_active_macro_defs && !_cmdline_macro_defs) || !macro_text)
#else
    if ((!_macro_defs && !_cmdline_macro_defs) || !macro_text)
#endif
    {
        return 1 ; // This macro is legal (undefined macro).
    }

    Set *del_visited_args = 0 ;
    if (!visited_args) {
        visited_args = new Set(STRING_HASH) ;
        del_visited_args = visited_args ;
    }

    // VIPER #6855 & #7029: Insert the macro name so that we automatically check with it:
    (void) visited_args->Insert(macro_name) ;

    // Check if this macro causes an infinite macro recursion situation - VIPER #1758.
    // This check is now being done here (at usage time) instead of in veri_file::DefineMacro. - VIPER #1859.
    const char *p = macro_text ;
    // Search for macro call in current macro text.
    while (p && *p && (*p != '`')) {
        // VIPER #4522: Adjustment to bypass macro calls within long comments in macro body:
        if (strncmp(p, "/*", 2) == 0) {
            // Skip everything until comment terminating */ characters
            while (*p && !((*p == '*') && (*(p+1) == '/'))) p++ ;
            if (*p=='\0') break ;
            p++ ; // Skip the ending *, '/' will be ignored next
        } else if ((*p == '\\') && (((p - macro_text) < 1) || ((*(p-1) != '\\') && (*(p-1) != '`')))) {
            // VIPER #5414: Skip everything inside extended id:
            p++ ; // Skip the slash
            while (p && *p && !isspace(*p)) p++ ;
            if (*p=='\0') break ;
        } else if ((*p == '"') && (((p - macro_text) < 1) || ((*(p-1) != '\\') && (*(p-1) != '`')))) {
            // VIPER #5414: Skip everything inside string:
            p++ ; // Skip the "
            while (p && *p && ((*p != '"') || (*(p-1) != '\\'))) p++ ;
            if (*p=='\0') break ;
        }
        p++ ;
    }

    while (p && *p) {
        // Increment past '`'
        p++ ;
        // Parse and verify macro call
        char *arg_name = Strings::save(p) ;
        char *pp = arg_name ;
        // AM 04/06/25 '::' scope resolution operator is removed before
        // 'isalnum', because in some system (g++ version 2.96) 'isalnum' is
        // defined as a macro.
        while (pp && (isalnum(*pp) || (*pp == '_'))) pp++ ;
        if (pp) *pp = '\0' ;  // Terminate end of macro name

        // VIPER #6855 & #7029: Following code is no more required, the Set below works for it.
        //if (Strings::compare(arg_name, macro_name)) {
        //    VeriTreeNode tmp_node ;
        //    tmp_node.Error("macro %s causes infinite loop", macro_name) ;
        //    Strings::free(arg_name) ;
        //    delete del_visited_args ;
        //    return 0 ;
        //}

        // VIPER #6855 & #7029: Keep track of visited arguments for recursion check:
        if (!visited_args->Insert(arg_name)) {
            VeriTreeNode tmp_node ;
            tmp_node.Error("macro %s causes infinite loop", macro_name) ;
            Strings::free(arg_name) ;
            delete del_visited_args ;
            return 0 ;
        }

        // Verify macro text of given macro
#ifdef VERILOG_QUICK_PARSE_V_FILES
        if (_active_macro_undefs && _active_macro_undefs->GetItem(arg_name)) {
            Strings::free(arg_name) ;
            delete del_visited_args ;
            return 0 ; // Active undefined, do not verify
        }
        // Verify macro in active Maps if it is there. Do not verify in the main macro store in that case:
        MapItem *mi = (_active_macro_defs) ? _active_macro_defs->GetItem(arg_name) : 0 ;
        if ((mi /* active defined */ && !VerifyMacro(macro_name, (char *)mi->Value(), visited_args))
                || (!mi /* not active defined */ && _macro_defs && !VerifyMacro(macro_name, (char*)_macro_defs->GetValue(arg_name), visited_args))
                || (_cmdline_macro_defs && !VerifyMacro(macro_name, (char*)_cmdline_macro_defs->GetValue(arg_name), visited_args))) // VIPER 2407
#else
        if ((_macro_defs && !VerifyMacro(macro_name, (char*)_macro_defs->GetValue(arg_name), visited_args))
                || (_cmdline_macro_defs && !VerifyMacro(macro_name, (char*)_cmdline_macro_defs->GetValue(arg_name), visited_args))) // VIPER 2407
#endif
        {
            Strings::free(arg_name) ;
            delete del_visited_args ;
            return 0 ;
        }

        // VIPER #6855 & #7029: Remove this argument name here:
        (void) visited_args->Remove(arg_name) ;

        Strings::free(arg_name) ;
        // Find next macro call (if it exists)
        while (p && *p && (*p != '`')) {
            // VIPER #4522: Adjustment to bypass macro calls within long comments in macro body:
            if (strncmp(p, "/*", 2) == 0) {
                // Skip everything until comment terminating */ characters
                while (*p && !((*p == '*') && (*(p+1) == '/'))) p++ ;
                if (*p=='\0') break ;
                p++ ; // Skip the ending *, '/' will be ignored next
            } else if ((*p == '\\') && (((p - macro_text) < 1) || ((*(p-1) != '\\') && (*(p-1) != '`')))) {
                // VIPER #5414: Skip everything inside extended id:
                p++ ; // Skip the slash
                while (p && *p && !isspace(*p)) p++ ;
                if (*p=='\0') break ;
            } else if ((*p == '"') && (((p - macro_text) < 1) || ((*(p-1) != '\\') && (*(p-1) != '`')))) {
                // VIPER #5414: Skip everything inside string:
                p++ ; // Skip the "
                while (p && *p && ((*p != '"') || (*(p-1) != '\\'))) p++ ;
                if (*p=='\0') break ;
            }
            p++ ;
        }
    }

    delete del_visited_args ;

    return 1 ; // This macro is legal
}

unsigned
veri_file::UndefineMacro(const char *macro_name)
{
    if (!macro_name) return 0 ;

    MapItem *item ;

    unsigned ret_status = 0 ;
#ifdef VERILOG_QUICK_PARSE_V_FILES
    if (_quick_parsing) {
        ret_status = UndefineActiveMacro(macro_name, 0 /* full undefine */) ;

        // Return 1, if it was defined (counting main macro store), 0 otherwise:
        if (!ret_status && ((_macro_defs && _macro_defs->GetItem(macro_name)) ||
                 (_cmdline_macro_defs && _cmdline_macro_defs->GetItem(macro_name)))) ret_status = 1 ;

#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
        _has_active_macro_ref = 1 ; // This is a macro reference
#endif

        return ret_status ;
    }
#endif // VERILOG_QUICK_PARSE_V_FILES

    if (_macro_argdefs) {
        item = _macro_argdefs->GetItem(macro_name) ;
        if (item) {
            // _macro_argdefs shares key with _macro_defs, so only need to delete value.
            // _macro_def (below) will delete key.
            char *value = (char*)item->Value() ;
            // Remove the macro entry
            (void) _macro_argdefs->Remove(macro_name) ;
            // Free value string
            Strings::free(value) ;
        }
    }

    // VIPER #7459: Remove the location of the definition too (linefile should not be deleted):
    if (_macro_def_linefiles) (void) _macro_def_linefiles->Remove(macro_name) ;

    if (_macro_defs) {
        item = _macro_defs->GetItem(macro_name) ;
        if (item) {
            // Save the key/value strings before the 'Remove' routine changes them.
            char *key = (char*)item->Key() ;
            char *value = (char*)item->Value() ;

            // delete the macro entry
            (void) _macro_defs->Remove(macro_name) ;

            // delete key/value pair
            Strings::free(key) ;
            Strings::free(value) ;
            ret_status = 1 ;
        }
    }
    // VIPER #4621 : Undefine macro from command line also :
    if (_cmdline_macro_defs) {
        item = _cmdline_macro_defs->GetItem(macro_name) ;
        if (item) {
            // Save the key/value strings before the 'Remove' routine changes them.
            char *key = (char*)item->Key() ;
            char *value = (char*)item->Value() ;

            // delete the macro entry
            (void)_cmdline_macro_defs->Remove(macro_name) ;

            // delete key/value pair
            Strings::free(key) ;
            Strings::free(value) ;
            ret_status = 1 ;
        }
    }

    return ret_status ; // It was not there
}

void
veri_file::UndefineAllMacros()
{
    UndefineUserDefinedMacros() ; // Clear out all user defined macros
    UndefineCmdLineMacros() ; // Clear out all command line macros

    // Leave predefined macro table untouched.
}

// VIPER #4356: Clear macros defined through command line
void
veri_file::UndefineCmdLineMacros()
{
    MapIter mi ;
    char *key, *value ;
    // Remove the macros defined from command-line
    if (_cmdline_macro_defs) { // VIPER 2407
        FOREACH_MAP_ITEM(_cmdline_macro_defs, mi, &key, &value) {
            Strings::free(key) ;
            Strings::free(value) ;
        }
        delete _cmdline_macro_defs ;
        _cmdline_macro_defs = 0 ;
    }
}
//CARBON_BEGIN
void
veri_file::UndefineCarbonUserDefinedMacros()
{
    // need to be careful - lf shouldn't be deleted anyway
    MapIter mi ;
    linefile_type lf;
    char *key, *value ;
    FOREACH_MAP_ITEM(_carbon_macro_defs, mi, &lf, &value) {
        Strings::free(value) ;
    }
    delete _carbon_macro_defs ;
    _carbon_macro_defs = 0 ;
    FOREACH_MAP_ITEM(_carbon_macro_def_linefiles, mi, &lf, &key) {
        Strings::free(key) ;
    }
    // VIPER #7459: Remove the location of the definition too (linefile should not be deleted):
    delete _carbon_macro_def_linefiles ;
    _carbon_macro_def_linefiles = 0 ;
}
//CARBON_END

void
veri_file::UndefineUserDefinedMacros()
{
    MapIter mi ;
    char *key, *value ;
    FOREACH_MAP_ITEM(_macro_argdefs, mi, &key, &value) {
        Strings::free(value) ;
    }
    delete _macro_argdefs ;
    _macro_argdefs = 0 ;

    FOREACH_MAP_ITEM(_macro_defs, mi, &key, &value) {
        Strings::free(key) ;
        Strings::free(value) ;
    }
    delete _macro_defs ;
    _macro_defs = 0 ;

    // VIPER #7459: Remove the location of the definition too (linefile should not be deleted):
    delete _macro_def_linefiles ;
    _macro_def_linefiles = 0 ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    ClearActiveMacros() ;
#endif // VERILOG_QUICK_PARSE_V_FILES
}

/*static*/ void
veri_file::UndefineMacroPreDefs()
{
    delete _macro_predefs ;
    _macro_predefs = 0 ;
}

unsigned
veri_file::IsPredefinedMacro(const char *name)
{
    if (!name) return 0 ;
    if (!_macro_predefs) InitPredefMacros() ;
    return (_macro_predefs->GetItem(name) ? 1 : 0) ;
}

unsigned
veri_file::IsDefinedMacro(const char *name)
{
    if (!name) return 0 ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
    if (_quick_parsing) _has_active_macro_ref = 1 ; // This is a macro reference
#endif

    // First check if it is undefined in active store:
    if (_active_macro_undefs && _active_macro_undefs->GetItem(name)) return 0 ; // Undefined
#endif

    // Return 1 if the macro definition resides in the '_cmdline_macro_defs' map
    if (_cmdline_macro_defs && _cmdline_macro_defs->GetItem(name)) return 1 ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    // Check if it is defined in active store:
    if (_active_macro_defs && _active_macro_defs->GetItem(name)) return 1 ;
#endif

    // Look for the macro definition in the '_macro_defs' map
    if (_macro_defs && _macro_defs->GetItem(name)) return 1 ;

    // Pre-defined macro's are also 'defined
    if (!_macro_predefs) InitPredefMacros() ;
    if (_macro_predefs->GetItem(name)) return 1 ;

    return 0 ;
}

unsigned
veri_file::IsCmdLineDefinedMacro(const char *macro_name)
{
    if (!macro_name) return 0 ;

#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    if (_quick_parsing) _has_active_macro_ref = 1 ; // This is a macro reference
#endif

    return (_cmdline_macro_defs && _cmdline_macro_defs->GetItem(macro_name)) ? 1 : 0 ;
}

const char *
veri_file::MacroKey(const char *macro_name)
{
    if (!macro_name) return 0 ;

    MapItem *mi ;
    // Get the 'key' (name pointer) of the macro, if (pre)defined
    if (!_macro_predefs) InitPredefMacros() ;
    mi = _macro_predefs->GetItem(macro_name) ;
    if (mi) return (const char*)mi->Key() ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    // First check if it is undefined in active store:
    if (_active_macro_undefs && _active_macro_undefs->GetItem(macro_name)) return 0 ; // Undefined
#endif

    mi = (_cmdline_macro_defs) ? _cmdline_macro_defs->GetItem(macro_name) : 0 ;
    if (mi) return (const char*)mi->Key() ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    mi = (_active_macro_defs) ? _active_macro_defs->GetItem(macro_name) : 0 ;
    if (mi) return (const char*)mi->Key() ;
#endif

    mi = (_macro_defs) ? _macro_defs->GetItem(macro_name) : 0 ;
    if (mi) return (const char*)mi->Key() ;

    return 0 ;
}

const char *
veri_file::MacroValue(const char *macro_name)
{
    if (!macro_name) return 0 ;

    if (!_macro_predefs) InitPredefMacros() ;

    MapItem *mi = _macro_predefs->GetItem(macro_name) ;
    if (mi) return (const char*)mi->Value() ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    // First check if it is undefined in active store:
    if (_active_macro_undefs && _active_macro_undefs->GetItem(macro_name)) return 0 ; // Undefined
#endif

    mi = (_cmdline_macro_defs) ? _cmdline_macro_defs->GetItem(macro_name) : 0 ;
    if (mi) return (const char*)mi->Value() ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    mi = (_active_macro_defs) ? _active_macro_defs->GetItem(macro_name) : 0 ;
    if (mi) return (const char*)mi->Value() ;
#endif

    if (_macro_defs) {
        return (const char*)_macro_defs->GetValue(macro_name) ;
    }

    return 0 ;
}
//CARBON_BEGIN
const char *
veri_file::CarbonMacroValue(linefile_type lf)
{
    if (!lf) return 0 ;

    if (_carbon_macro_defs) {
        return (const char*)_carbon_macro_defs->GetValue(lf) ;
    }

    return 0 ;
}
//CARBON_END

const char *
veri_file::MacroArgs(const char *macro_name)
{
    if (!macro_name) return 0 ;

#ifdef VERILOG_QUICK_PARSE_V_FILES
    // First check if it is undefined in active store:
    if (_active_macro_undefs && _active_macro_undefs->GetItem(macro_name)) return 0 ; // Undefined

    MapItem *mi = (_active_macro_argdefs) ? _active_macro_argdefs->GetItem(macro_name) : 0 ;
    if (mi) return (const char*)mi->Value() ;
#endif

    return (_macro_argdefs) ? (const char*)_macro_argdefs->GetValue(macro_name) : 0 ;
}

void
veri_file::InitPredefMacros()
{
    // List all predefined macros here, so that user will not overwrite them

    if (!_macro_predefs) {
        // Initialize table
        _macro_predefs = new Map(STRING_HASH, 31) ;

        // IEEE 1364-1995 Section 16 compiler directives
        _macro_predefs->Insert("celldefine",0) ;
        _macro_predefs->Insert("endcelldefine",0) ;
        _macro_predefs->Insert("default_nettype","wire|tri|tri0|wand|triand|tri1|wor|trior|trireg") ;
        _macro_predefs->Insert("define","<macro_name>") ;
        _macro_predefs->Insert("undef","<macro_name>") ;
        _macro_predefs->Insert("ifdef","<macro_name>") ;
        _macro_predefs->Insert("else",0) ;
        _macro_predefs->Insert("endif",0) ;
        _macro_predefs->Insert("include","\"<filename>\"") ;
        _macro_predefs->Insert("resetall",0) ;
        _macro_predefs->Insert("timescale","<timeunit>/<time_precision>") ;
        _macro_predefs->Insert("unconnected_drive","pull1|pull0") ;
        _macro_predefs->Insert("nounconnected_drive",0) ;

        // IEEE 1364-1995 Annex G compiler directives
        _macro_predefs->Insert("default_decay_time","infinite|<constant> ; argument without spaces") ;
        _macro_predefs->Insert("default_trireg_strength","<constant> ; argument without spaces") ;
        _macro_predefs->Insert("delay_mode_distributed",0) ;
        _macro_predefs->Insert("delay_mode_path",0) ;
        _macro_predefs->Insert("delay_mode_unit",0) ;
        _macro_predefs->Insert("delay_mode_zero",0) ;

        // Verilog 2000 additions
        _macro_predefs->Insert("ifndef","<macro_name>") ;
        _macro_predefs->Insert("elsifdef","<macro_name>") ;
        _macro_predefs->Insert("elsifndef","<macro_name>") ;
        _macro_predefs->Insert("line","<number> \"<filename>\" <level>") ; // VIPER #1786 fix.
        _macro_predefs->Insert("file","\"<filename>\"") ;

        // XL additions
        _macro_predefs->Insert("protected",0) ;
        _macro_predefs->Insert("endprotected",0) ;
        _macro_predefs->Insert("protect",0) ; // this one is 'known', but will be ignored
        _macro_predefs->Insert("endprotect",0) ; // this one is 'known', but will be ignored

//        // Supported by other tools:
//        _macro_predefs->Insert("accelerate",0) ;
//        _macro_predefs->Insert("noaccelerate",0) ;
//        _macro_predefs->Insert("autoexpand_vectornets",0) ;
//        _macro_predefs->Insert("expand_vectornets",0) ;
//        _macro_predefs->Insert("noexpand_vectornets",0) ;
//        _macro_predefs->Insert("remove_gatenames",0) ;
//        _macro_predefs->Insert("noremove_gatenames",0) ;
//        _macro_predefs->Insert("remove_netnames",0) ;
//        _macro_predefs->Insert("noremove_netnames",0) ;
//        _macro_predefs->Insert("enable_portfaults",0) ;
//        _macro_predefs->Insert("disable_portfaults",0) ;
//        _macro_predefs->Insert("suppress_faults",0) ;
//        _macro_predefs->Insert("nosuppress_faults",0) ;
//        _macro_predefs->Insert("inline",0) ;
//        _macro_predefs->Insert("noinline",0) ;
//        _macro_predefs->Insert("portcoerce",0) ;
//        _macro_predefs->Insert("noportcoerce",0) ;
//        _macro_predefs->Insert("default_rswitch_strength",0) ;
//        _macro_predefs->Insert("default_switch_strength",0) ;
//        _macro_predefs->Insert("pre_16a_paths",0) ;
//        _macro_predefs->Insert("end_pre_16a_paths",0) ;
    }
}

/************************ Parsing itself ******************/

unsigned
veri_file::Analyze(const char *file_name, unsigned verilog_mode, const char *lib_name, unsigned cu_mode)
{
    if (!file_name) return 0 ; // nothing to analyze

    if (cu_mode != NO_MODE) {
        // If user specified a mode sfcu/mfcu, set that
        _compilation_unit_mode = cu_mode ; // Set compilation unit
    } else {
        // No mode is specified. Set default mode accoring to verilog mode.
        // Default mode in verilog 95/2001 is mfcu and in system verilog sfcu (LRM IEEE 1800-2005, section 19.3).
        // VIPER #6053: SYSTEM_VERILOG_2005/2009 should be treated similar to SYSTEM_VERILOG.
        _compilation_unit_mode = ((verilog_mode >= SYSTEM_VERILOG_2005) && (verilog_mode <= SYSTEM_VERILOG)) ? SFCU : MFCU ;
    }

    // Viper: 3142 - all verific msg should have msg-id
    // Message::PrintLine("Verilog file ", file_name, " ignored due to errors") ;
    VeriTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // VIPER #6790: If we are switching library, check whether compilation unit is not closed:
    VeriLibrary *root_lib = (_root_module) ? _root_module->GetLibrary() : 0 ;
    VeriScope *root_scope = (_root_module) ? _root_module->GetScope() : 0 ;
    if ((verilog_mode > VERILOG_2K) && ((root_lib && !Strings::compare(root_lib->GetName(), lib_name)) ||
        (root_scope && !root_scope->IsStdPackageVisible() && GetModule("std", 1, "std", 0)))) {
        // Here either library is switched, or the std package is not visible though it is
        // already available in the memory (parse tree). Something wrong, produce error:
        tmp_print_node.Error("compilation unit has not been closed before this analyze call") ;
        _root_module = 0 ; // Do not close the compilation unit, just reset _root_module
    }

    if (!AnalyzeInternal(file_name, verilog_mode, lib_name)) return 0;

    if (Message::ErrorCount()) {
        tmp_print_node.Comment("Verilog file %s ignored due to errors",file_name) ;

        // Modules with errors will not have been added
        return 0 ; // Error occurred during parsing
    }
    return 1 ;
}

// VIPER 3782: This function is a standalone verilog expression parser. It takes the input in the form of
// a string. The verilog_mode is also passed to define the mode in which the expression needs to be parsed.
VeriExpression *
veri_file::AnalyzeExpr(const char *expr, unsigned verilog_mode, const linefile_type line_file)
{
    if (!expr) return 0 ; // nothing to analyze

    // first reset _parsed expr
    _parsed_expr = 0 ;

    // Do not clean the error count, instead store the count in a variable:
    //Message::ClearErrorCount() ;
    unsigned error_count = Message::ErrorCount() ;

    if (!AnalyzeExprInternal(expr, verilog_mode, line_file)) return 0;

    // Check if error count increased because of an error in the expression analyzed here:
    if (Message::ErrorCount() > error_count) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Verilog file ", file_name, " ignored due to errors") ;
        VeriTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Comment("Verilog expression ignored due to errors") ;

        // Modules with errors will not have been added
        return 0 ; // Error occurred during parsing
    }
    return _parsed_expr ;
}
// Analyze -y/-v specified library file :
unsigned
veri_file::AnalyzeLibFile(const char *file_name, unsigned verilog_mode, const char *lib_name)
{
    if (!file_name) return 0 ; // nothing to analyze

    if (!AnalyzeInternal(file_name, verilog_mode, lib_name)) {
        return 0 ;
    }

    if (Message::ErrorCount()) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Verilog file ", file_name, " ignored due to errors") ;
        VeriTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Comment("Verilog file %s ignored due to errors",file_name) ;

        // Modules with errors will not have been added
        return 0 ; // Error occurred during parsing
    }
    return 1 ;
}

// VIPER #8515: Check the language mode with the available product:
/* static */ unsigned
veri_file::CheckLanguageModeAndAvailableProducts(unsigned verilog_mode)
{
    (void) verilog_mode ; // Avoid compiler warning

    unsigned status = 1 ;

    if (verilog_mode == VERILOG_AMS) {
        // AMS product is not enabled, error out:
        VeriTreeNode tmp_node(0) ;
        tmp_node.Error("%s is not enabled in this product", "AMS") ;
        status = 0 ;
    }

    if (verilog_mode == VERILOG_PSL) {
        // PSL product is not enabled, error out:
        VeriTreeNode tmp_node(0) ;
        tmp_node.Error("%s is not enabled in this product", "PSL") ;
        status = 0 ;
    }

    return status ; // 0 = illegal mode for this product, 1 = can be parsed
}

// Internal routine to analyze a file. Basically it is added to avoid code repetation :
unsigned
veri_file::AnalyzeInternal(const char *file_name, unsigned verilog_mode, const char *lib_name)
{
    if (!file_name) return 0 ; // nothing to analyze

    // Set Verilog mode (dialect). Encoding : 0:verilog95, 1:verilog2k, 2:system_verilog, 3:Verilog-AMS, 4:Verilog-PSL.
    // VIPER #4115: File extension to language dialect selection:
    const char *extension = strrchr(file_name, '.') ;
    unsigned user_set_mode = GetModeFromFileExt(extension) ;
    if (user_set_mode != UNDEFINED) verilog_mode = user_set_mode ; // Override mode with user set mode
    SetAnalysisMode(verilog_mode) ;

    (void) CheckLanguageModeAndAvailableProducts(verilog_mode) ; // VIPER #8515

    // Read the named file and return 0 if reading failed (for all modules in the file).
    Message::ClearErrorCount() ;

    // VIPER #8123 : Ignore library mapping if -work option is specified in
    // analyze. This is according to IEEE 1800-2012 LRM section 33.3.1.1
    if (lib_name) {
        _ignore_library_mapping = 1 ; // VIPER #8123 : Set flag to ignore library mapping
    } else {
        // VIPER #2726: Get the library mapping for this file:
        const char *mapped_lib_name = veri_file::GetMatchedLibrary(file_name) ;
        // If it has a library specified, always use that overriding the work library:
        if (mapped_lib_name) lib_name = mapped_lib_name ;
    }

    // Set default library name and set work library
    if(!lib_name) lib_name = "work" ;
    _work_library = GetLibrary(lib_name, 1) ;

    // Start tokenizer (open file etc)
    if (!StartFlex(file_name)) {
        _ignore_library_mapping = 0 ; // VIPER #8123 : Reset ignore library mapping flag
        return 0 ;
    }

    // VIPER #2932: This message is now being issued by the veri_file::StartFlex
    // routine in verilog.l
    // Message::PrintLine("Analyzing Verilog file ",file_name) ;

    StartYacc() ;
    (void) Parse() ;
    EndYacc() ;

    (void) EndFlex() ;

    _ignore_library_mapping = 0 ; // VIPER #8123 : Reset ignore library mapping flag
    return 1 ;
}

// Internal routine to analyze a file. Basically it is added to avoid code repetation :
unsigned
veri_file::AnalyzeExprInternal(const char *expr, unsigned verilog_mode, const linefile_type line_file)
{
    if (!expr) return 0 ; // nothing to analyze

    (void) CheckLanguageModeAndAvailableProducts(verilog_mode) ; // VIPER #8515

    // Set Verilog mode (dialect). Encoding : 0:verilog95, 1:verilog2k, 2:system_verilog, 3:Verilog-AMS, 4:Verilog-PSL.
    unsigned old_analysis_mode = GetAnalysisMode() ; // Store previous analysis mode
    SetAnalysisMode(verilog_mode) ;
    // Start tokenizer (open file etc)
    if (!StartFlexExpr(expr, line_file)) {
        SetAnalysisMode(old_analysis_mode) ;
        return 0 ;
    }

    StartYacc() ;
    (void) Parse() ;
    EndYacc() ;

    (void) EndFlex() ;

    SetAnalysisMode(old_analysis_mode) ; // Pop analysis mode
    return 1 ;
}

// VIPER #2932: API to analyze multiple files.
unsigned
veri_file::AnalyzeMultipleFiles(const Array *file_names, unsigned verilog_mode, const char *lib_name, unsigned cu_mode)
{
    // Check if the array of file names is empty or not
    if (!file_names) return 0 ;

    if (cu_mode != NO_MODE) {
        // If user specified a mode sfcu/mfcu, set that
        _compilation_unit_mode = cu_mode ; // Set compilation unit
    } else {
        // No mode is specified. Set default mode accoring to verilog mode.
        // Default mode in verilog 95/2001 is mfcu and in system verilog sfcu (LRM IEEE 1800-2005, section 19.3).
        // VIPER #6053: SYSTEM_VERILOG_2005/2009 should be treated similar to SYSTEM_VERILOG.
        _compilation_unit_mode = ((verilog_mode >= SYSTEM_VERILOG_2005) && (verilog_mode <= SYSTEM_VERILOG)) ? SFCU : MFCU ;
    }

    // Viper: 3142 - all verific msg should have msg-id
    // Message::PrintLine("Verilog file ", file_name, " ignored due to errors") ;
    VeriTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // VIPER #6790: If we are switching library, check whether compilation unit is not closed:
    VeriLibrary *root_lib = (_root_module) ? _root_module->GetLibrary() : 0 ;
    VeriScope *root_scope = (_root_module) ? _root_module->GetScope() : 0 ;
    if ((verilog_mode > VERILOG_2K) && ((root_lib && !Strings::compare(root_lib->GetName(), lib_name)) ||
        (root_scope && !root_scope->IsStdPackageVisible() && GetModule("std", 1, "std", 0)))) {
        // Here either library is switched, or the std package is not visible though it is
        // already available in the memory (parse tree). Something wrong, produce error:
        tmp_print_node.Error("compilation unit has not been closed before this analyze call") ;
        _root_module = 0 ; // Do not close the compilation unit, just reset _root_module
    }

    // Set the veri_file array containing the name of the files to be analyzed
    _file_names = new Array (*file_names) ;
    const char *file_name = (_file_names && _file_names->Size()) ? (const char*) _file_names->GetFirst() : 0 ;

    // Set Verilog mode (dialect). Encoding : 0:verilog95, 1:verilog2k, 2:system_verilog, 3:Verilog-AMS, 4:Verilog-PSL.
    // VIPER #4115: File extension to language dialect selection:
    const char *extension = (file_name) ? strrchr(file_name, '.') : 0 ;
    unsigned user_set_mode = GetModeFromFileExt(extension) ;
    if (user_set_mode != UNDEFINED) verilog_mode = user_set_mode ; // Override mode with user set mode
    SetAnalysisMode(verilog_mode) ;

    (void) CheckLanguageModeAndAvailableProducts(verilog_mode) ; // VIPER #8515

    // Read all the named file and return 0 if reading failed (for all modules in all the file).
    Message::ClearErrorCount() ;

    // VIPER #8123 : Ignore library mapping if -work option is specified in
    // analyze. This is according to IEEE 1800-2012 LRM section 33.3.1.1
    if (lib_name) _ignore_library_mapping = 1 ; // VIPER #8123 : Set flag to ignore library mapping

    // Set default library name and set work library
    if(!lib_name) lib_name = "work" ;
    _work_library = GetLibrary(lib_name, 1) ;

    // Start tokenizer (open file etc)
    if (!StartFlex(file_name)) {
        ResetFileNames() ;
        _ignore_library_mapping = 0 ; // VIPER #8123 : Reset ignore library mapping flag
        return 0 ;
    }

    // VIPER #2932: This message is now being issued by the veri_file::StartFlex
    // routine in verilog.l
    // Message::PrintLine("Analyzing Verilog file ",file_name) ;

    StartYacc() ;
    (void) Parse() ;
    EndYacc() ;

    (void) EndFlex() ;

    // Clean up the allocated memory space
    ResetFileNames() ;
    _ignore_library_mapping = 0 ; // VIPER #8123 : Reset ignore library mapping flag

    // Process user libraries (-v/-y processing) :
    ProcessUserLibraries() ;

    // VIPER #3825 : Remove include directories now. It is not part of
    // 'EndCompilationUnit'
    RemoveAllIncludeDirs() ; // Remove the include directory

    // End the compilation unit
    EndCompilationUnit() ;

    return Message::ErrorCount() ? 0 : 1 ;
}

unsigned
veri_file::AnalyzeFull(const char *work_lib)
{
    // Perform semantic checks and cross-module resolving of all parsed modules.
    // This routine should be run after entire design is parsed.
    // Elaboration always starts with running this routine.
    // Reset the current user library (the -y/-v/uselib library used during analysis)
    veri_file::SetCurrentUserLibrary(0) ;

    if (!work_lib) work_lib = "work" ;
    // Find/Set the work library where design units will be found.
    _work_library = GetLibrary(work_lib, 1) ;

    Message::ClearErrorCount() ;

    // Analyze all (not yet elaborated) modules in the Verilog tree
    MapIter i ;
    VeriModule *module ;
    FOREACH_VERILOG_MODULE_IN_LIBRARY(_work_library, i,module) {
        if (!module) continue ;
        if (module->IsAnalyzed()) continue ;
        // Perform full analysis and instantiation resolving.
        module->AnalyzeFull() ;
        // Check me : what to do if there is an error in a module..
    }

    if (Message::ErrorCount()) {
        // Errors will have been given already.
        return 0 ;
    }

    return 1 ;
}

// Routine to process all -y/-v options :
void
veri_file::ProcessUserLibraries()
{
    unsigned cached = FileSystem::CacheCwd() ;

    // All -y/-v files will be compiled under only one compilation unit
    _compilation_unit_mode = MFCU ;
    VeriLibrary *user_lib ;
    MapIter mi ;
    FOREACH_VERILOG_LIBRARY(mi, user_lib) {
        if (user_lib) user_lib->Process() ;
    }

    if (RuntimeFlags::GetVar("veri_move_yv_modules_into_work_library")) {
        // FIXME: In case of error count, Library::AddModule rejects the module being added.
        if (_work_library && !Message::ErrorCount()) {
            // Reset current user library so that it does not get populated again:
            VeriLibrary *current_user_lib = veri_file::GetCurrentUserLibrary() ;
            veri_file::SetCurrentUserLibrary(0) ;
            // VIPER #5816: Move all -y/-v user library modules over to work library:
            FOREACH_VERILOG_LIBRARY(mi, user_lib) {
                if (!user_lib || !user_lib->IsUserLibrary() ||
                    user_lib->IsTickUseLib() || (user_lib == _work_library)) continue ;

                // Do not move `uselib user library modules though, since `uselib
                // has more priority than the work library (VIPER #5080).
                VeriModule *module ;
                MapIter mii ;
                FOREACH_VERILOG_MODULE_IN_LIBRARY(user_lib, mii, module) {
                    if (!module) continue ;

                    // Check whether the module already exists in work library
                    // Mostly existing module will be a root module, if not skip it:
                    if (_work_library->GetModule(module->Name(), 1) && !module->IsRootModule()) {
                    //if (_work_library->GetModule(module->Name(), 1)) VERIFIC_ASSERT(module->IsRootModule()) ;
                        // FIXME: How did the module come in this non `uselib -y/-v library if it is in work library?
                        // Continue from here, the above assert statement did not hit, though when enabled.
                        continue ;
                    }

                    // Detach the module from the user library:
                    if (!user_lib->DetachModule(module)) continue ; // Cannot remove it

                    // Then add the module to work library, use appropriate API:
                    if (module->IsRootModule()) {
                        (void) AddRootModuleToWorkLib(module) ;
                    } else {
                        (void) _work_library->AddModule(module) ;
                    }
                }
            }
            veri_file::SetCurrentUserLibrary(current_user_lib) ;
        }
    }

    RemoveAllYDirsVFiles() ; // Re-set the current_user_library to NULL

    if (cached) FileSystem::ResetCacheCwd() ;
}

// Routine to be callled after end of one compilation unit :
// This will do all sorts of cleaning stuffs
// 1. Remove include directory
// 2. Process root module
// 3. Undefine user defined macros (command line macros remains)
// 4. Reset compiler directive `default_nettype and `timescale
void
veri_file::EndCompilationUnit()
{
    // VIPER #5119 : Attach last parsed timescale with the root module
    if (_root_module && _root_module->IsRootModule() && veri_file::GetTimeScale()) {
        _root_module->SetTimeScale(veri_file::GetTimeScale()) ;
    }
    // VIPER #3825 : Do not remove include directories after each compilation unit.
    // It will be propagated in sfcu mode
    //RemoveAllIncludeDirs() ; // Remove the include directory
    ProcessRootModule() ; // DO process the root module. This is the end of a compilation unit.
    UndefineUserDefinedMacros() ;    // Undefine all user defined macros
    // VIPER #5063 : Reset compiler directives
    SetDefaultNettype(0) ;
    SetTimeScale(0) ;
    ResetOptionalCompilerDirectives() ; // VIPER #7379
}

/*********************  Pragma Trigger handling ***********************/
//CARBON_BEGIN
void veri_file::AddPragmaTrigger(const char *trigger)
{
    if (!trigger) return ;
    if (!_pragma_triggers) _pragma_triggers = new Array() ;
    _pragma_triggers->InsertLast(Strings::save(trigger)) ;
}

void veri_file::RemoveAllPragmaTriggers()
{
    if (!_pragma_triggers) return ;
    unsigned i ;
    char *exist ;
    FOREACH_ARRAY_ITEM(_pragma_triggers, i, exist) {
        Strings::free( exist ) ;
    }
    delete _pragma_triggers ;
    _pragma_triggers = 0 ;
}

unsigned
veri_file::IsPragmaTrigger(const char * id)
{
    Verific::Array* pt_list = Verific::veri_file::PragmaTriggers() ;
    unsigned i ;
    char * upt;
    FOREACH_ARRAY_ITEM(pt_list, i, upt) {
        if (Strings::compare(id, upt)) {
            return 1; 
        }
    }
    return 0;
}
//CARBON_END

/*********************  Include Directory handling ***********************/

void veri_file::AddIncludeDir(const char *dir)
{
    if (!dir) return ;
    if (!_include_dirs) _include_dirs = new Array() ;
    _include_dirs->InsertLast(Strings::save(dir)) ;
    // Since _include_dirs is static, these guys get removed in RemoveAllModules())
}

void veri_file::RemoveIncludeDir(const char *dir)
{
    if (!dir) return ;
    unsigned i ;
    char *exist ;
    FOREACH_ARRAY_ITEM(_include_dirs, i, exist) {
        if (Strings::compare(exist, dir)) {
            Strings::free( exist ) ; // delete the string
            _include_dirs->Remove(i) ; // remove the element.
        }
    }
}

char *
veri_file::IncludeFileName(const char *filename, linefile_type linefile)
{
    if (!filename || !linefile) return 0 ;

    char *full_name = 0 ;

    if (RuntimeFlags::GetVar("veri_include_ansi_file_search")) {
        // Use ANSI C/C++ #include search algorithm (relative to CWD of file which
        // contains include statement) (VIPER #1817)

        // - If filename is an absolute path and file exists, return it.
        if (FileSystem::IsAbsolutePath(filename)) {
            // This is an absolute path!  Now check to see if it exists.
            if (FileSystem::IsFile(filename)) {
                // It exists. Return the full file name :
                return Strings::save(filename) ;
            } else {
                return 0 ; // File does not exist!
            }
        }

        // Get filename of file from where `include resides
        const char *current_filename = LineFile::GetFileName(linefile) ;
        if (!current_filename) return 0 ;

        // Determine directory path of current_filename
        char *included_from_dir = FileSystem::DirectoryPath(current_filename) ;
        if (included_from_dir) {
            // Add filename to include_from_dir path.
            full_name = Strings::save(included_from_dir, "/", filename) ;
        } else {
            // There was no included_from_dir path, so interpret as "./".
            full_name = Strings::save(filename) ;
        }
        Strings::free(included_from_dir) ;

        // Now check if this file resides in the directory associated with
        // the incoming linefile information.
        if (FileSystem::IsFile(full_name)) {
            // Return (saved version) of the file name :
            return full_name ;
        }

        Strings::free(full_name) ;
    }

    // - Now check if this file resides in the current directory.
    if (FileSystem::IsFile(filename)) {
        // return (saved version) of the file name :
        return Strings::save(filename) ;
    }

    // - Now check include directories, in order :
    unsigned i ;
    const char *dir ;
    FOREACH_ARRAY_ITEM(_include_dirs, i, dir) {
        // Create the full file name
        full_name = Strings::save(dir,"/",filename) ;
        // Check if this file exists :
        if (FileSystem::IsFile(full_name)) {
            // It exists. Return the full file name :
            return full_name ;
        }
        Strings::free(full_name) ;
    }
    return 0 ;
}

void veri_file::RemoveAllIncludeDirs()
{
    if (!_include_dirs) return ;
    unsigned i ;
    char *exist ;
    FOREACH_ARRAY_ITEM(_include_dirs, i, exist) {
        Strings::free( exist ) ;
    }
    delete _include_dirs ;
    _include_dirs = 0 ;
}

/************************ Verilog XL Complience ******************/

void veri_file::AddYDir(const char *dir)
{
    if (!dir) return ;

    if(!_yv_user_library) {
        _yv_user_library = new VeriUserLibrary() ;
        AddLibrary(_yv_user_library) ;
    }
    _yv_user_library->AddDirOption(dir) ;
    _current_user_library = _yv_user_library ;
}

void veri_file::AddVFile(const char *file)
{
    if (!file) return ;

    if(!_yv_user_library) {
        _yv_user_library = new VeriUserLibrary() ;
        AddLibrary(_yv_user_library) ;
    }
    _yv_user_library->AddFileOption(file) ;
    _current_user_library = _yv_user_library ;
}

void veri_file::AddLibExt(const char *lib_ext)
{
    if (!lib_ext) return;

    if(!_yv_user_library) {
        _yv_user_library = new VeriUserLibrary() ;
        AddLibrary(_yv_user_library) ;
    }
    _yv_user_library->AddLibExt(lib_ext) ;
    _current_user_library = _yv_user_library ;
}
// Set -librescan option to user library
void veri_file::SetLibReScan(unsigned lib_rescan_active)
{
    if (!_yv_user_library) {
        _yv_user_library = new VeriUserLibrary() ;
        AddLibrary(_yv_user_library) ;
    }
    _yv_user_library->SetLibReScan(lib_rescan_active) ;
    _current_user_library = _yv_user_library ;
}

void veri_file::RemoveAllYDirsVFiles()
{
    // Re-set the current_user_library to NULL.
    // This will prevent any more access / usage of the current user library.
    _current_user_library = 0 ;
    // VIPER #5659: Reset the -y/-v user library also.
    // CHECKME: May be we should not reset it, but to be backward compatible we do this here:
    _yv_user_library = 0 ;
}

const char *
veri_file::TopModule()
{
    // FIX ME : for now, just take last one in there
    MapIter i ;
    VeriModule *module ;
    char *key ;
    Map *all_modules = AllModules() ;
    // Walk BACK so that we find the last analyzed, uncompiled module.
    FOREACH_MAP_ITEM_BACK(all_modules,i,&key,&module) {
        if (module->IsCompiled()) continue ; // skip already compiled modules.
        if (module->IsPslUnit()) continue ; // PSL unit cannot be top-level
        if (module->IsInterface()) continue ; // interface cannot be top-level
        if (module->IsPackage()) continue ; // package cannot be top-level
        return module->Name() ;
    }
    return 0 ;
}

/************************ Relaxed Languag Checking mode *************/

void
veri_file::SetRelaxedLanguageChecking(unsigned on)
{
    // relaxed syntax/semantic checking.
    VeriNode::SetRelaxedChecking(on) ;
}

/************************ Interrupt handling ************************/

void
veri_file::RegisterInterrupt(unsigned(*interrupt_func)(linefile_type))
{
    _interrupt_func = interrupt_func ;
}

unsigned
veri_file::CheckForInterrupt(linefile_type linefile)
{
    // Check if we should stop processing :
    if (Message::ErrorCount()) return 1 ;
    if (!_interrupt_func) return 0 ;

    // Call user interrupt function :
    if (_interrupt_func(linefile)) {
        VeriTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Error("Verilog reader: User Interrupt. Cleaning up....") ;
        // Message::Error(0,"Verilog reader: User Interrupt. Cleaning up....") ;
        // This will set the message error counter, so we will not call
        // the user _interrupt_func any more until the error count is cleared.
        return 1 ;
    }
    return 0 ;
}

void
veri_file::Interrupt()
{
    // Directly Interrupt the elaborator or analyzer.
    // Do this by issuing an error message.
    // This will gracefully end the elaborator :
    // It will stop gracefully and clean up behind itself.
    VeriTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    tmp_print_node.Error("Verilog reader: User Interrupt. Cleaning up....") ;
    // Message::Error(0,"Verilog reader: User Interrupt. Cleaning up....") ;
}

/************************ Black-box compilation handling *****************/
void
veri_file::RegisterCompileAsBlackbox(unsigned(*compile_as_blackbox_func)(const char *))
{
    _compile_as_blackbox_func = compile_as_blackbox_func ;
}

unsigned
veri_file::CheckCompileAsBlackbox(const char *module_name)
{
    if (!_compile_as_blackbox_func) return 0 ;
    if (!module_name) return 0 ;
    // Call the user-registered function :
    return _compile_as_blackbox_func(module_name) ;
}

/*********************  Parsed VeriModule handling ***********************/

VeriModule *
veri_file::GetModule(const char *name, unsigned case_sensitive, const char *lib_name, unsigned restore)
{
    if (!lib_name) return 0 ;

    // Pick up the library by name
    VeriLibrary *lib = GetLibrary(lib_name, restore/*create if not there only if restore is set*/, case_sensitive) ;

    // Pick up the module from this library
    return (lib) ? lib->GetModule(name, case_sensitive, restore) : 0 ;
}

void
veri_file::AddModule(VeriModule *module)
{
    if (_work_library) (void) _work_library->AddModule(module) ;
}

Map *veri_file::AllModules(const char *lib_name)
{
    if (!lib_name) return 0 ;
    VeriLibrary *lib = GetLibrary(lib_name) ;
    return lib ? lib->GetModuleTable() : 0 ;
}

unsigned
veri_file::RemoveModule(const char *name, const char *lib_name)
{
    if (!lib_name) return 0 ;
    VeriLibrary *lib = GetLibrary(lib_name) ;
    return lib ? lib->RemoveModule(name) : 0 ;
}

void
veri_file::RemoveSimpleModules()
{
    MapIter iter ;
    VeriLibrary *lib ;
    FOREACH_VERILOG_LIBRARY(iter, lib) {
        if(lib) lib->RemoveSimpleModules() ;
    }
}

void
veri_file::RemoveAllModules()
{
    // Normally, the root module will be part of a library, so we'll take care of it in the loop below.
    // If accidentally the pointer is still there and its not part of a library, then explicitly remove it now (before the loop).
    if (_root_module && !_root_module->GetLibrary()) delete _root_module ;
    // reset root module pointer.
    _root_module = 0 ;

    // Delete all modules in all libraries.
    MapIter iter ;
    VeriLibrary *lib ;
    FOREACH_VERILOG_LIBRARY(iter, lib) {
        if(lib) lib->RemoveAllModules() ;
    }

    // cleanup the global following static maps
    VeriNode::FreeAttributeMap() ;
    VeriNode::FreeMapOfCommentsArray() ;
}

void
veri_file::RemoveUncompiledModules()
{
    MapIter iter ;
    VeriLibrary *lib ;
    FOREACH_VERILOG_LIBRARY(iter, lib) {
        if(lib) lib->RemoveUncompiledModules() ;
    }
}

/////////////////////////////////////////////////////////
//          Additions for Library
////////////////////////////////////////////////////////

// Function Name: veri_file::AddLibrary
// Arguments    : 1) VeriLibrary *library
// Return Type  : void
// Description  : Add the library to the Map _all_libraries
void veri_file::AddLibrary(const VeriLibrary *library)
{
    if (!library) return ;
    if (!_all_libraries) _all_libraries = new Map(STRING_HASH) ;
    (void) _all_libraries->Insert(library->GetName(), library) ;
}

void veri_file::RemoveLibrary(const VeriLibrary *library)
{
    if (!library || !_all_libraries) return ;
    const char *name = library->GetName() ;
    (void) _all_libraries->Remove(name) ;
}

// VIPER #6199: Delete library APIs: Before calling this make sure that
// there is no reference to the library being deleted in the parse tree
// otherwise, you corrupt the parse tree. This is user responsibility:
/* static */ void
veri_file::DeleteLibrary(const char *name)
{
    if (!name || !_all_libraries) return ;

    VeriLibrary *library = (VeriLibrary *)_all_libraries->GetValue(name) ;
    if (!library) return ; // No library found

    // Remove from the library map:
    (void)_all_libraries->Remove(name) ;

    // Reset static pointers:
    if (library == _work_library) _work_library = 0 ;
    if (library == _current_user_library) _current_user_library = 0 ;
    if (library == _yv_user_library) _yv_user_library = 0 ;
    // Reset root-module pointer too, in case that library is being deleted:
    if (_root_module && (_root_module->GetLibrary() == library)) _root_module = 0 ;

    // Finally, delete the library:
    delete library ;
}

/* static */ void
veri_file::DeleteAllLibraries()
{
    // Memory leak fix: Process root module before deleting the libraries:
    if (_root_module && !_root_module->GetLibrary()) {
        // The root module is created but is not added to any library yet, so, delete it:
        delete _root_module ;
        _root_module = 0 ;
    }

    MapIter mi ;
    VeriLibrary *library ;
    // Delete all the libaries:
    FOREACH_MAP_ITEM(_all_libraries, mi, 0, &library) delete library ;

    // Reset the static pointers:
    _work_library = 0 ;
    _current_user_library = 0 ;
    _yv_user_library = 0 ;

    // Reset the store, do not delete it:
    if (_all_libraries) _all_libraries->Reset() ;
}

/* static */ void
veri_file::ResetAllLibraries()
{
    if (!_all_libraries) return ;
    VERIFIC_ASSERT(!_all_libraries->Size()) ;
    delete _all_libraries ;
    _all_libraries = 0 ;
}

// Function Name: veri_file::GetLibrary
// Arguments    : 1) const char *library_name - Name of the library
// Return Type  : VeriLibrary *
// Description  : Look up for VeriLibrary * having name library_name in
//              the Map _all_libraries. If found, the library is returned.
//              Otherwise if create_if_needed flag is 1,a new VeriLibrary *
//              is created, added to _all_libraries, and the new library is
//              returned.
//              If create_if_needed flag is 0 and library was not found in
//              _all_libraries, the return value is 0.
VeriLibrary *
veri_file::GetLibrary(const char *library_name, unsigned create_if_needed, unsigned case_sensitive)
{
    if (!library_name) return 0 ;

    if (!_all_libraries) _all_libraries = new Map(STRING_HASH) ;

    // Pick it up case-sensitively
    VeriLibrary *library = (VeriLibrary*)_all_libraries->GetValue(library_name) ;
    if (library) return library ;

    if (!case_sensitive) {
        // Search hard (case-insensitive) to find this library :
        MapIter mi ;
        // VIPER #2755 : Use a local variable for iteration. Otherwise we will
        // return last library of the list when 'create_if_needed' is 0 and no
        // library named 'library_name' exists
        VeriLibrary *lib ;
        FOREACH_VERILOG_LIBRARY(mi,lib) {
            if (!lib) continue ;
            if (Strings::compare_nocase(lib->GetName(),library_name)) {
                return lib ;
            }
        }

        // VIPER #4410 & #4551: Case insensitive search of library in mapped library paths:
        if (create_if_needed) {
            // Do a case insensitive search in saved libraries
            const char *found_lib_name = 0 ;
            if (veri_file::GetLibraryPath(library_name)) {
                found_lib_name = library_name ;
            } else {
                // Search in all library paths
                char *lib_name, *dir_name ;
                FOREACH_MAP_ITEM (_map_library_paths, mi, &lib_name, &dir_name) {
                    if (Strings::compare_nocase(lib_name, library_name)) {
                        found_lib_name = lib_name ;
                        break ;
                    }
                }
            }

            if (found_lib_name) {
                library = new VeriLibrary(found_lib_name) ;
                (void) _all_libraries->Insert(library->GetName(), library) ;
                return library ;
            }

            // If not found in specified library path, we have to search in default path
            char *lib_path = veri_file::GetDefaultLibraryPath() ;
            char *disk_lib_name = 0 ;
            Array files ;
            char *file ;
            unsigned i ;
            FileSystem::ReadDirectory(lib_path, &files) ;
            FOREACH_ARRAY_ITEM(&files, i, file) {
                if (Strings::compare_nocase(file, library_name)) {
                    // Check whether it is directory :
                    if (FileSystem::IsDir(file)) {
                        // VIPER #4967: Copy the string here, otherwise we corrupt:
                        disk_lib_name = Strings::save(file) ;
                        break ;
                    }
                }
            }
            // Now free up allocated file names
            FOREACH_ARRAY_ITEM(&files, i, file) { Strings::free(file) ;  }

            if (disk_lib_name) {
                library = new VeriLibrary(disk_lib_name) ;
                Strings::free(disk_lib_name) ; disk_lib_name = 0 ; // VIPER #4967: clean-up
                (void) _all_libraries->Insert(library->GetName(), library) ;
                return library ;
            }
        }
    }

    if (create_if_needed) {
        library = new VeriLibrary(library_name) ;
        (void) _all_libraries->Insert(library->GetName(), library) ;
    }

    return library ;
}

// Returns the work library corresponding to the file name specified by the library
// mapping information, if any. If no file name is specified it just returns the
// _work_library static pointer set by the last veri_file::SetWorkLib() call.
/* static */ VeriLibrary *
veri_file::GetWorkLib(const char *file_name /* = 0 */)
{
    // If no file name is given, return the global work library:
    if (!file_name) return _work_library ;

    // VIPER #2726: Search the library mapping for the mapped library for this file:
    const char *mapped_lib_name = GetMatchedLibrary(file_name) ;

    // If it has a library specified, return that library:
    if (mapped_lib_name) return GetLibrary(mapped_lib_name, 1 /* create if needed */) ;

    // Otherwise, return the global work library:
    return _work_library ;
}

/******************** -L option Setting ********************************/

void veri_file::AddLOption(const char *lib_name)
{
    if (!lib_name) return ; // Nothing to add
    if (!_ordered_libs) _ordered_libs = new Set(STRING_HASH) ;

    // Add library name in library list, do not insert if already in
    if (!_ordered_libs->GetItem(lib_name)) (void)_ordered_libs->Insert(Strings::save(lib_name)) ;
}

void veri_file::RemoveAllLOptions()
{
    SetIter si ;
    char *lib_name ;
    FOREACH_SET_ITEM(_ordered_libs, si, &lib_name) Strings::free(lib_name) ;
    delete _ordered_libs ;
    _ordered_libs = 0 ;
}

VeriModule * veri_file::GetModuleFromLOptions(const char *unit_name, unsigned case_sensitive, unsigned restore)
{
    if (!unit_name || !_ordered_libs) return 0 ; // No name specified, return 0

    SetIter si ;
    char *lib_name ;
    VeriModule *module = 0 ;
    FOREACH_SET_ITEM(_ordered_libs, si, &lib_name) { // Iterate over ordered libraries
        if (!lib_name) continue ;
        VeriLibrary *lib = GetLibrary(lib_name, restore /* create if 'restore' is 1*/) ;
        module = lib ? lib->GetModule(unit_name, case_sensitive, restore) : 0 ; // Get the unit in library
        if (module) return module ; // Found, return
    }
    return 0 ;
}

/* static */ VeriModule *
veri_file::GetPackageFromLOptions(const char *package_name, unsigned case_sensitive, unsigned restore)
{
    if (!package_name || !_ordered_libs) return 0 ; // No name specified, return 0

    SetIter si ;
    char *lib_name ;
    VeriModule *module = 0 ;
    FOREACH_SET_ITEM(_ordered_libs, si, &lib_name) { // Iterate over ordered libraries
        if (!lib_name) continue ;
        VeriLibrary *lib = GetLibrary(lib_name, restore /* create if 'restore' is 1*/) ;
        module = lib ? lib->GetModule(package_name, case_sensitive, restore) : 0 ; // Get the unit in library
        if (module && module->IsPackage()) return module ; // Found package, return
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        // VIPER #6895: Find package from vhdl also and convert it:
        module = VeriNode::ConvertVhdlToVerilogPackage(package_name, lib_name) ;
        if (module && module->IsPackage()) return module ;
#endif
    }
    return 0 ;
}

VeriModule *
veri_file::GetRootModule(unsigned create_if_needed)
{
    // Return (pointer to) the root module if we have it
    if (_root_module) return _root_module ;

    if (!create_if_needed) return 0 ; // if we should not create one, then return 0.

    // Only SystemVerilog and AMS will actually create a root module, because their syntax allows root items
    // Create root module if this is the first time.
    // FIX ME : We are in transition between 3.1a and 1800 here.
    // IEEE 1800 defines $unit to be the old root scope. $unit is now a 'compilation scope'.
    // Besides that, there can be multiple compilation scopes in a design.
    // We should provide a mechanism for multiple compilation scopes, and also store these guys (as package) into the current work library.

    // Do not immediately store this root module in a library, for a few reasons :
    //  (1) It is very likely that the root module is not needed (if no rootitems in this compilation unit).
    //      In that case, we remove it after compilation unit is done, which would leave a 'gap' in the modules (in library) map.
    //      That is a bit ugly, because it could mess-up order of modules in a library.
    //  (2) Typically we only add a module to a library if it is free of analysis errors.
    //      Can only conclude that after we analyzed, so do not do it now.
    //      Not sure if it is bad if this rule is violated.
    //  (3) We do not know which library this guy should end up in...
    //
    // On the other hand, adding the root module to the library immediately has advantages too :
    //  (1) IF there, it will be the first module inserted in the library. This is good for pretty-print.
    //  (2) right now is as good a time as ever to know the library.
    //  (3) we would not need to rename the root module afterwards.
    //
    // 8/2007: Decided to NOT store root module in the library.
    // Only add it to the library if a module is using it, and something is defined in it.
    // Do that in VeriModule::AddModuleItem() so that add it when the first root module item appears.
    // We can also give it a name then.
    // That (not adding to library right away) is also important for root modules from -v/-y parsing (VIPER 3092)

#if 0
    // Try storing it in a library.
    VeriLibrary *work_lib = GetWorkLib() ; // check me : use file name ?
    if (!work_lib) return 0 ; // cannot do anything without a library..?

    // First create a unique name for it..
    unsigned unit_nr = 1 ;
    char *unit_name = Strings::save("$unit_<a big number>") ;
    sprintf(unit_name,"$unit_%d",unit_nr) ;
    // Lookup this unit :
    VeriModule *exist = work_lib->GetModule(unit_name, 1/*case_sensitive*/, 0/*do not restore*/) ;
    // CHECK ME : check on disk also ?
    while (exist) {
        unit_nr++ ;
        sprintf(unit_name,"$unit_%d",unit_nr) ;
        exist = work_lib->GetModule(unit_name, 1/*case_sensitive*/, 0/*do not restore*/) ;
    }
#endif

    // Create the identifier. Call this thing $unit for now
    char *unit_name = Strings::save("$unit") ;
    VeriIdDef *root_id = new VeriModuleId(unit_name) ;

    // Create identifier and scope. No items yet
    VeriScope *root_scope = new VeriScope(0,root_id) ;

    // The root scope ($unit) is actually considered a package. It can be used like a package in prefix of a scope name.
    _root_module = new VeriPackage(root_id,0,root_scope) ;
    // VIPER #5686 : Package is system verilog construct, so set system verilog
    // as _analysis_dialect. If file to be analyzed is verilog95/2k, root module
    // is created with veri_file::GetAnalysisMode which is 95/2k. So overwrite
    // dialect now.
    if (veri_file::GetAnalysisMode() != veri_file::VERILOG_AMS) {
        _root_module->SetAnalysisDialect(veri_file::SYSTEM_VERILOG) ;
    }

#if 0
    // This code is moved to the constructor of VeriPackage for VIPER #5426
    // VIPER #3231, 3232, 3233, 3234 : Get 'std' package and set that package as
    // an wildcard imported package of root module. This will make std package
    // visible to all root items and modules :

    // Get current message status
    msg_type_t VERI_1500 = Message::GetMessageType("VERI-1500") ;
    msg_type_t VERI_1501 = Message::GetMessageType("VERI-1501") ;
    msg_type_t VERI_1502 = Message::GetMessageType("VERI-1502") ;
    msg_type_t VERI_1503 = Message::GetMessageType("VERI-1503") ;
    msg_type_t VERI_1508 = Message::GetMessageType("VERI-1508") ;
    msg_type_t VERI_1509 = Message::GetMessageType("VERI-1509") ;
    // Suppress the messages produced during restoration of std package
    (void) Message::SetMessageType("VERI-1500", VERIFIC_IGNORE) ;
    (void) Message::SetMessageType("VERI-1501", VERIFIC_IGNORE) ;
    (void) Message::SetMessageType("VERI-1502", VERIFIC_IGNORE) ;
    (void) Message::SetMessageType("VERI-1503", VERIFIC_IGNORE) ;
    (void) Message::SetMessageType("VERI-1508", VERIFIC_IGNORE) ;
    (void) Message::SetMessageType("VERI-1509", VERIFIC_IGNORE) ;

    unsigned analysis_mode = _analysis_mode ;
    VeriModule *std_package = GetModule("std", 1, "std", 1 /* restore */) ;
    _analysis_mode = analysis_mode ;

    // Restore message status back :
    if (VERI_1500==VERIFIC_NONE) Message::ClearMessageType("VERI-1500") ; else (void) Message::SetMessageType("VERI-1500", VERI_1500) ;
    if (VERI_1501==VERIFIC_NONE) Message::ClearMessageType("VERI-1501") ; else (void) Message::SetMessageType("VERI-1501", VERI_1501) ;
    if (VERI_1502==VERIFIC_NONE) Message::ClearMessageType("VERI-1502") ; else (void) Message::SetMessageType("VERI-1502", VERI_1502) ;
    if (VERI_1503==VERIFIC_NONE) Message::ClearMessageType("VERI-1503") ; else (void) Message::SetMessageType("VERI-1503", VERI_1503) ;
    if (VERI_1508==VERIFIC_NONE) Message::ClearMessageType("VERI-1508") ; else (void) Message::SetMessageType("VERI-1508", VERI_1508) ;
    if (VERI_1509==VERIFIC_NONE) Message::ClearMessageType("VERI-1509") ; else (void) Message::SetMessageType("VERI-1509", VERI_1509) ;

    if (std_package) {
        // Make 'std' package implicitly imported in root module by adding package 's
        // scope in the imported list of root scope
        root_scope->AddImportItem(std_package->GetScope(), 0, _root_module) ;
    }
#endif

    // Flag this as a 'root' module :
    _root_module->SetIsRootModule() ;

    // and store it in the work library :
    //(void) work_lib->AddModule(_root_module) ;

    // return a pointer to this one:
    return _root_module ;
}

/* static */ void
veri_file::ResetRootModule()
{
    // Simply reset the static pointer to 0:
    _root_module = 0 ;
}

void
veri_file::ProcessRootModule()
{
    // root module is always created at the start of parsing, since we do not know up-front if it is needed or not.
    // After parsing (of a compilation unit; a set of files) is done, we need to decide what to do with it.
    //
    // If it was not needed at all (no statements in the root module), then we want to get rid of it.
    // That is not entirely trivial, since all analyzed modules have the root module scope as their own scope 'owner'.
    // Also they registered dependencies to the root module.
    // So here we do the work to nicely remove the root module from the top of the scope tree, and delete it.
    //
    // If the root module WAS used, then we want to store it somewhere.
    // We will store it in the (work) library... Under a unique name...
    // We assume that it will never have to be looked-up again, so the name can change now.
    //
    // Either way, we want to re-set the _root_module pointer, because the root module is now done with.
    if (!_root_module) return ; // no root module to process.

    // Check if this is really a root module.
    if (!_root_module->IsRootModule()) {
        // module is not a root module. We should not be pointing to this.
        _root_module = 0 ;
        return ;
    }

    // VIPER #3031: Check for incomplete forward typedef in the root module:
    //VeriScope *root_scope = _root_module->GetScope() ;
    //if (root_scope) root_scope->CheckIncompleteTypedef() ;

    // VIPER #4315: Do not call Resolve() here. Resolve Id-ref in VarUsage instead.
    // If we call Resolve(), CanBeOneHot() may be set wrongly on a variable which is
    // never reset later because its usage will not be Resolve()d again in other modules.
    // VIPER #6485: Re(-re)solve root module to resolve all the module items
    // at once. We resolved them one by one, but that may not be sufficient.
    // VIPER #8045 : Re-resolve root items to handle forwrd type defs
    _root_module->Resolve(0 /* no scope */, VeriTreeNode::VERI_UNDEF_ENV) ;

    // VIPER #3031: Check for incomplete forward typedef in the root module:
    VeriScope *root_scope = _root_module->GetScope() ;
    if (root_scope) root_scope->CheckIncompleteTypedef() ;
    // VIPER #6526: Call the scaled down version of the Resolve() routine to at-least
    // do some priliminary work which does not create conflict with flag settings:
    _root_module->ProcessRootModule() ;

    // Check if this root scope is already added to a library.
    // If so, then we leave it in place.
    // If not, then there was nothing in it, and we should delete it.
    if (!_root_module->GetLibrary()) {
        // If someone put a dependency in place, but there is nothing in the root module
        // then that dependent module will also be deleted.
        // That is intended, because we do not want stale pointer to remain hanging around.
        // VIPER #4198 & #4523: Reset the veri_file::_root_module pointer before deleting itself.
        // This is required to avoid memory leak - we may create another root module from the
        // destructor of the object of the root module if veri_file::_root_module is not reset:
        VeriModule *root = _root_module ;
        _root_module = 0 ;
        // We do not really nead the following code, since deleting the root module will do it anyway.
        // VIPER #5963: Reset the _upper scope pointer of all the children
        // This root scope has nothing in it and is going to be deleted.
        // This scope is going to be deleted which will do this automatically:
        //if (root_scope && !VeriNode::InRelaxedCheckingMode()) {
        //    // VIPER #5963: Do this only under compatibility mode:
        //    // So, we do not remove the child scope in compatibility mode.
        //    // Only reset the upper pointer if we are in normal mode:
        //    Set *root_children = root_scope->GetChildrenScopes() ;
        //    SetIter si ;
        //    VeriScope *scope ;
        //    FOREACH_SET_ITEM(root_children, si, &scope) {
        //        root_scope->RemoveChildScope(scope) ;
        //    }
        //}
        delete root ;
    }
    // Either way, reset the _root_module pointer,
    // because we are at the end of a compilation unit.
    _root_module = 0 ;
}
// Routine to add root module in work library.
unsigned veri_file::AddRootModuleToWorkLib(VeriModule *root_tree)
{
    return AddRootModuleToLib(root_tree, veri_file::GetWorkLib()) ;
}

/* static */ unsigned
veri_file::AddRootModuleToLib(VeriModule *root_tree, VeriLibrary *library)
{
    if (!library || !root_tree || root_tree->GetLibrary()) return 0 ; // It is already in library

    // First create a unique name for the unit..
    // FIX ME : We might create one root module per file, so that
    // we can keep unique names based on file.
    // Or we could move this code to VeriLibrary::AddModule() so that we add the root module
    // to a library based on first encountered module (that uses it) name...
    unsigned unit_nr = 1 ;
    char *unit_name = Strings::save("$unit_<a big number>") ;
    sprintf(unit_name,"$unit_%d",unit_nr) ;
    // Lookup this unit :
    VeriModule *exist = library->GetModule(unit_name, 1/*case_sensitive*/, 0/*do not restore*/) ;
    // CHECK ME : check on disk also ?
    while (exist) {
        unit_nr++ ;
        sprintf(unit_name,"$unit_%d",unit_nr) ;
        exist = library->GetModule(unit_name, 1/*case_sensitive*/, 0/*do not restore*/) ;
    }

    VeriIdDef *root_id = root_tree->GetId() ;

    // Set the name of the module to that name.
    // That is possible, since this is a root module not in a library,
    // so it is not yet stored in any hash table (I hope).
    if (root_id) root_id->SetName(unit_name) ;
    Strings::free(unit_name) ; // JJ: 130108 need to free this memory now that SetName no longer absorbs

    // Add module to the work library :
    return library->AddModule(root_tree) ;
}
// Returns the top level design units (allocated array of VeriModule*) of the argument
// specific library. If the library name is not specified, top level design units  of
// the 'work' library are returned. For System Verilog designs, if $root has explicit
// instantiations, then those instantiated design units are to be considered as top
// level design units. In normal Verilog mode, uninstantiated modules are considered
// as top level.
Array *
veri_file::GetTopModules(const char *work_lib)
{
    Set top_mods(POINTER_HASH) ;
    // The following Set is used to store the names of all instantiated modules
    Set master_name_tab(STRING_HASH) ;
    Set defined_mod_name_tab(STRING_HASH) ; // Will be ignored

    VeriModule *module ;
    MapIter mi ;
    VeriLibrary *lib = GetLibrary(work_lib) ;
    // Traverse all modules of library 'work_lib'
    Set done_libs(POINTER_HASH) ; // The libraries which we already traversed
    if (lib) lib->AccumulateInstantiatedModuleNames(master_name_tab, done_libs, defined_mod_name_tab) ;

    FOREACH_VERILOG_MODULE_IN_LIBRARY(lib, mi, module) {
        // Rule out module derivatives that cannot be top-level :
        if (!module) continue ;
        if (module->IsPackage() || module->IsInterface() || module->IsPslUnit()) continue ; // packages or interfaces or psl units cannot be top-level
        if (module->GetQualifier(VERI_EXTERN)) continue ;
        // Check if the design unit has been instantiated
        if (!master_name_tab.GetItem(module->Name())) {
            (void) top_mods.Insert(module) ; // design unit has not been instantiated, so it is top level
        }
    }

    if (!top_mods.Size()) return 0 ; // no top-level design units found at all.

    // Now set-up an array of top-level design units from the 'top_mods' Set :
    Array *top_modules = new Array(top_mods.Size()) ;
    SetIter si ;
    FOREACH_SET_ITEM(&top_mods, si, &module) {
        top_modules->InsertLast(module) ;
    }
    return top_modules ;
}

/* ----------------- Verilog 2000 Library Mapping ----------------- */

/* static */ void
veri_file::AddLibraryDecl(const VeriLibraryDecl *lib_decl)
{
    if (!lib_decl) return ;

    // Create the array if it is not there:
    if (!_library_decls) _library_decls = new Array() ;

    // Insert the library declaration into the Array:
    _library_decls->InsertLast(lib_decl) ;

    // Need to clear the 0 entries of the '_matched_files' Map from here. Otherwise,
    // we would not be able to properly match the files in a situation like:
    //
    //     // file test.v            |    // file incl.v
    //     library lib1 "incl.v" ;   |    library lib2 "test.v" ;
    //     `include "incl.v"         |    module incl ;
    //     module test ;             |    endmodule
    //     endmodule                 |
    //
    // Here we would not be able to store the module 'test' in library lib2 without
    // clearing the 0 content of the already matched files whenever we get a library
    // declaration. (It seems that other standard tools also behaves in this way and
    // they stores 'incl' in 'lib1' and 'test' in work library).
    MapIter mi ;
    const char *file ;
    const char *lib ;
    FOREACH_MAP_ITEM(_matched_files, mi, &file, &lib) {
        // Remove this 0 value entry from this Map:
        if (!lib) (void) _matched_files->Remove(file) ;
    }
}

/* static */ void
veri_file::ClearAllLibraryDecls()
{
    // Delete the library decls and the container array:
    unsigned i ;
    VeriLibraryDecl *lib ;
    FOREACH_ARRAY_ITEM(_library_decls, i, lib) delete lib ;
    delete _library_decls ;
    _library_decls = 0 ; // Reset it so that we don't corrupt

    // Clear the matched entries also:
    delete _matched_files ;
    _matched_files = 0 ; // Reset it so that we don't corrupt
}

/* static */ const char *
veri_file::GetMatchedLibrary(const char *file_name)
{
    // VIPER #8123 : Ignore library mapping if '_ignore_library_mapping' is set.
    // According to IEEE 1800-2012 LRM 33.3.1.1 library mapping should be ignored
    // if -work is explicitly specified during analysis
    if (_ignore_library_mapping) return 0 ;

    if (!file_name || !_library_decls || !_library_decls->Size()) return 0 ;

    // First try the already matched entries, if any:
    if (_matched_files) {
        MapItem *lib = _matched_files->GetItem(file_name) ;
        // If found a match, return it:
        if (lib) return (const char *)lib->Value() ;
    }

    unsigned max_precedence = 0 ;        // Maximum precedence in which we found a match
    unsigned same_precedence_match = 0 ; // Whether the maximum precedence is matched more than once
    VeriLibraryDecl *matched_lib = 0 ;   // The library that matched at maximum precedence

    // Match the patterns with this file name:
    unsigned i ;
    VeriLibraryDecl *lib ;
    FOREACH_ARRAY_ITEM(_library_decls, i, lib) {
        if (!lib) continue ;

        // Match the file name with the patterns of this library, this returns the precedence of the match.
        unsigned precedence = lib->MatchPatterns(file_name) ;

        // Check for match, if not matched, precedence is zero:
        if (precedence > max_precedence) {
            // Found a new maximum precedence match:
            max_precedence = precedence ; // Set this to the new maximum
            same_precedence_match = 0 ;   // Reset this flag to avoid illegal error
            matched_lib = lib ;           // Store this maximum precedence matching library
        } else if (precedence && (precedence == max_precedence)) {
            // Only treat it as a another match, if the libraries are different:
            VERIFIC_ASSERT(lib && matched_lib) ; // Both of these should be set here, we have two matches
            // Match the name of the two matching libraries:
            if (!Strings::compare(lib->GetName(), matched_lib->GetName())) {
                // The two libraries are different, so, maximum precedence has another match
                same_precedence_match = 1 ;
            }
        }
    }

    // Error out if we have more than one matching library entry in same precedence for this file:
    if (same_precedence_match) {
        VERIFIC_ASSERT(matched_lib) ; // It must be there for matching_entries > 1
        matched_lib->Error("file %s matched multiple library patterns", file_name) ;
    }

    // Get the name of the matching library:
    const char *lib_name = (matched_lib) ? matched_lib->GetName() : 0 ;

    // Insert this library name against the file name:
    if (!_matched_files) _matched_files = new Map(STRING_HASH) ;
    // The library name could be 0. We clear the 0 value entries whenever we get a
    // library declaration (from veri_file::AddLibraryDecl) to be always updated.
    (void) _matched_files->Insert(file_name, lib_name /* lib_name could be ZERO */) ;

    // Return the name of the matched library or 0:
    return lib_name ;
}

// Store any encountered Verilog `include directive statements in static Map (VIPER #1990/2712)
void
veri_file::AddIncludedFile(const char *file_included, linefile_type included_from)
{
    if (!included_from || !file_included) return ;

    // Create Map if it doesn't already exist.
    if (!_included_files) _included_files = new Map(STRING_HASH) ;

    // Now check if the linefile 'included_from' has been already registered.  This may occur
    // due to cyclic `includes.  In this case, do not register it again; return instead.
    MapItem *item ;
    linefile_type lf ;
    FOREACH_SAME_KEY_MAPITEM(_included_files, (void*)file_included, item) {
        if (!item) continue ;
        lf = (linefile_type)item->Value() ;
        // Now check if previously registered `include matches current 'included_from'
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
        if ((lf->GetFileId()    == included_from->GetFileId())
            && (lf->GetRightLine() == included_from->GetRightLine())
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            && (lf->GetLeftCol()   == included_from->GetLeftCol())
            && (lf->GetRightCol()  == included_from->GetRightCol())
            && (lf->GetLeftLine()  == included_from->GetLeftLine())
#endif
) {
            return ;
        }
#else
        if (lf == included_from) return ;
#endif
    }

    // At this point we know this file_name->linefile_type pair needs to be stored.

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    included_from = included_from->Copy() ; // Need to make a copy when column info is enabled.
#endif

    // Do a forced-insert of this (char*)file_name->(linefile_type)included_from pair.
    (void) _included_files->Insert(Strings::save(file_included), (void*)included_from, 0, 1) ; // force_insert (allow duplicate keys)
}

void
veri_file::ClearIncludedFiles()
{
    // Clear out (and delete) all contents of _included_files Map
    MapIter mi ;
    char *name ;
    linefile_type lf ;
    FOREACH_MAP_ITEM(_included_files, mi, &name, &lf) {
        Strings::free(name) ;
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
        // VIPER #4175: Do not delete the linefile, otherwise we have a stale
        // pointer in the '_col_line_structs' static array in LineFile.cpp file:
        //delete lf ;
#endif
    }
    delete _included_files ;
    _included_files = 0 ;
}

char *
veri_file::GetEffectiveTimeScale(const VeriModule *mod)
{
    if (!mod) return 0 ; // module must be provided

    // mod_timescale is whatever we parsed from the design
    // this is the `timescale value that is in force for
    // this module, could be null if there was no `timescale
    const char *mod_timescale = mod->GetTimeScale() ;

    // Now set the mod_timescale in the veri_file so that
    // GetUserTimeScale can access it
    veri_file::SetTimeScale(mod_timescale) ;

    // GetUserTimeScale now knows all three values for the
    // timescale manipulation: the global timescale, the
    // module timescale and the default timescale. The
    // assumption is, no one changed the global and default
    // timescale from the time this module was elaborated
    return veri_file::GetUserTimeScale() ;
}

// VIPER #4115: File extension to language dialect selection:
/* static */ void
veri_file::AddFileExtMode(const char *file_extension, unsigned verilog_mode)
{
    if (!file_extension || (verilog_mode > UNDEFINED)) return ;

    if (!_file_ext_vs_mode) _file_ext_vs_mode = new Map(STRING_HASH) ;

    // If we already have an existing mode mapped with this extension remove if first:
    // CHECKME: Should we return wthout doing anything from here if it already mapped?
    RemoveFileExt(file_extension) ;

    // Insert this file extension -> mode mapping:
    (void) _file_ext_vs_mode->Insert(Strings::save(file_extension), (void *)((unsigned long)verilog_mode)) ;
}

/* static */ unsigned
veri_file::GetModeFromFileExt(const char *file_extension)
{
    if (!file_extension || !_file_ext_vs_mode) return UNDEFINED ;

    // Return veri_file::UNDEFINED if it is not mapped:
    MapItem *exist = _file_ext_vs_mode->GetItem(file_extension) ;
    return (exist) ? (unsigned)((unsigned long)exist->Value()) : (unsigned)UNDEFINED ;
}

/* static */ void
veri_file::RemoveFileExt(const char *file_extension)
{
    if (!_file_ext_vs_mode || !file_extension) return ;

    MapItem *exist = _file_ext_vs_mode->GetItem(file_extension) ;
    char *exist_key = (exist) ? (char *)exist->Key() : 0 ;
    (void) _file_ext_vs_mode->Remove(exist) ;
    Strings::free(exist_key) ;
}

/* static */ void
veri_file::ClearAllFileExt()
{
    MapIter mi ;
    char *extension ;
    FOREACH_MAP_ITEM(_file_ext_vs_mode, mi, &extension, 0) Strings::free(extension) ;
    // Now, delete the map store:
    delete _file_ext_vs_mode ;
    _file_ext_vs_mode = 0 ;
}

#ifdef VERILOG_FLEX_READ_FROM_STREAM
// VIPER #5726: Read from streams:
/* static */ void
veri_file::RegisterFlexStreamCallBack(verific_stream *(*stream_func)(const char *file_name))
{
    _stream_callback_func = stream_func ;
}

/* static */ verific_stream *
veri_file::GetUserFlexStream(const char *file_name)
{
    if (!_stream_callback_func) return 0 ;

    return _stream_callback_func(file_name) ;
}
#endif

#ifdef VERILOG_QUICK_PARSE_V_FILES
/* static */ void
veri_file::ApplyActiveMacros()
{
    // Apply the accumulated macros under -v file quick parsing
    // into the main macro maps, we are done with quick parsing:
    MapIter mi ;
    const char *key ;
    const char *value ;
    const char *args ;
    FOREACH_MAP_ITEM(_active_macro_defs, mi, &key, &value) {
        // Get the argument of the macro:
        args = (_active_macro_argdefs) ? (const char *)_active_macro_argdefs->GetValue(key) : 0 ;
        // Call the routine to define the macro:
        (void) DefineMacro(key, value, args) ;
    }

    SetIter si ;
    FOREACH_SET_ITEM(_active_macro_undefs, si, &key) {
        // Undefine the macros:
        (void) UndefineMacro(key) ;
    }

    // Done, clear the active macro store:
    ClearActiveMacros() ;
}

/* static */ void
veri_file::ClearActiveMacros()
{
    // Clear the accumulated macros under -v file quick parsing:
    MapIter mi ;
    char *key ;
    char *value ;
    // First clear the argument Map:
    FOREACH_MAP_ITEM(_active_macro_argdefs, mi, &key, &value) {
        // Only free the value, key is owned by _active_macro_defs:
        Strings::free(value) ;
    }
    delete _active_macro_argdefs ;
    _active_macro_argdefs = 0 ;

    // Now, clear the macro body Map:
    FOREACH_MAP_ITEM(_active_macro_defs, mi, &key, &value) {
        // Free both the key and value:
        Strings::free(key) ;
        Strings::free(value) ;
    }
    delete _active_macro_defs ;
    _active_macro_defs = 0 ;

    // Now, clear the undefined macros:
    SetIter si ;
    FOREACH_SET_ITEM(_active_macro_undefs, si, &key) {
        Strings::free(key) ;
    }
    delete _active_macro_undefs ;
    _active_macro_undefs = 0 ;

#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
    _has_active_macro_ref = 0 ; // Also clear this flag now
#endif
}

/* static */ unsigned
veri_file::UndefineActiveMacro(const char *macro_name, unsigned remove_only /* = 0 */)
{
    MapItem *item ;
    if (_active_macro_argdefs) {
        item = _active_macro_argdefs->GetItem(macro_name) ;
        if (item) {
            // _active_macro_argdefs shares key with _active_macro_defs,
            // so only need to delete value. Below, we will delete the key:
            char *value = (char*)item->Value() ;
            // Remove the macro entry
            (void) _active_macro_argdefs->Remove(macro_name) ;
            // Free value string
            Strings::free(value) ;
        }
    }

    unsigned ret_status = 0 ;
    if (_active_macro_defs) {
        item = _active_macro_defs->GetItem(macro_name) ;
        if (item) {
            char *key = (char*)item->Key() ;
            char *value = (char*)item->Value() ;
            // Remove the macro entry
            (void) _active_macro_defs->Remove(key) ;
            // Delete both key and value
            Strings::free(key) ;
            Strings::free(value) ;
            ret_status = 1 ;
        }
    }

    // If we should only remove the macro, then we are done, return from here:
    if (remove_only) return ret_status ;

    // Here we need to undefine this macro in active store by inserting into the Set:
    if (!_active_macro_undefs) _active_macro_undefs = new Set(STRING_HASH) ;

    // Insert by copying the string, we will own it:
    char *key = Strings::save(macro_name) ;
    if (!_active_macro_undefs->Insert(key)) {
        // Already undefined:
        Strings::free(key) ;
    }

    return ret_status ;
}

#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
/* static */ unsigned
veri_file::HasActiveMacroDefRef()
{
    return (_has_active_macro_ref || (_active_macro_defs && _active_macro_defs->Size())) ? 1 : 0 ;
}
#endif
#endif // VERILOG_QUICK_PARSE_V_FILES

/* static */ Array *
veri_file::TakeLinefileStructures()
{
    Array *arr = 0 ;
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    if (_processing_user_library && !veri_file::GetDesignUnitDepth()) {
        // VIPER #5932: Here, we are processing a user library, and not inside any
        // module ie, it is not called from a nested module, so return the Array now:
        arr = _linefile_structs ;
        _linefile_structs = 0 ;
    }
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    return arr ;
}

/* static */ void
veri_file::SetupLinefileStructures()
{
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    if (_processing_user_library && (veri_file::GetDesignUnitDepth()==1)) {
        // VIPER #5932: Here, we are processing a user library, and not inside
        // a module ie, it is not a nested module, so create the Array now:
        VERIFIC_ASSERT(!_linefile_structs) ; // This must not be set already
        _linefile_structs = new Array() ;
    }
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
}

// VIPER #6550: Generate Explicit State Machine: For generating explicit state machine from implicit state machine
// we follow the following algorithm:
// 1) Traverse the parse tree with VeriFsmAlwaysVisitor.
// 2) For always/initial construct:
//       - check for some limitations and conditions.
//       - if conditions are fulfilled do the following:
//             1. Generate control flow graph with a new visitor VeriFsmProcessVisitor.
//                   1) For generating CFG we need to declare 4 classes- statement node for
//                      plain statements, state node for event statements, conditional node
//                      for if-else/case/loop statements and end node for scope end of conditional
//                      /loop/sequential block statements.
//                   2) During CFG generation each time we visit a node we check for event control
//                      within the block, otherwise we store the whole block of statements in statement node.
//                   3) For loop/conditional node we have more than one child: process each branch
//                      and create new parse tree branch for each branch and store the parent pointer
//                      in the conditional/loop node.
//                   4) Set an end node at the end of each branch of conditional node and else branch
//                      of loop node.
//                   5) Create a dummy statement node as the top node of the cfg.
//                   6) Set the last node as the parent of the child of the parent node.
//             2. Collect all the root node vrs their module item in a map.
//             3. If any one of the always constructs of the processing module cannot be converted
//                then do not proceed further with the other always constructs.
//             4. In the first created cfg we do not process the event trigger and event wait statements,
//                process them by traversing the cfg and rearrange the cfg as required.
//             5. Count the total number of events.
//             6. For each state nodes generate case item and store them in an array of items.
//             7. Generate a reset block for first time starting the simulation.
//             8. Generate a conditional statement with the reset and set block(case statement).
//             9. Keep the reset block in a compile flag.
//             10. Create an event control statement with the clocking event of the original parse tree.
//             11. Create an initial construct initializing the reset/state variables for generate items
//                 otherwise add the initialization statements in the module scope.
//             12. Create an always construct.
//             13. Create a sequential block with the above always and initial construct for generate items
//             14. Return the new module item(above sequential block for generate items/other wise
//                 the above always construct for replacing the old one.
//             15. Delete the CFG.
// 3) If we can able to create an always construct then replace the old item with it.
// 4) Return the new item.
unsigned
veri_file::GenerateExplicitStateMachines(const char *module_name, const char *lib_name, unsigned reset)
{
    unsigned module_item_replaced = 0 ;
    if (!module_name) {
        MapIter i ;
        VeriModule *module ;
        char *key ;
        Map *all_modules = AllModules(lib_name) ;
        // Walk BACK so that we find the last analyzed, uncompiled module.
        FOREACH_MAP_ITEM_BACK(all_modules, i, &key, &module) {
            module_item_replaced = module->GenerateExplicitStateMachine(reset) ;
            module->SetModuleItemReplaced(module_item_replaced) ;
        }
    } else {
        VeriModule *module = GetModule(module_name, /*case_sensitive*/1, lib_name) ;
        if (module) {
            module_item_replaced = module->GenerateExplicitStateMachine(reset) ;
            module->SetModuleItemReplaced(module_item_replaced) ;
        }
    }

    if (module_item_replaced) return 1 ;

    return 0 ;
}

/* static */ void
veri_file::Reset()
{
    ResetParser() ;

    veri_file::RemoveAllModules() ;
    veri_file::ResetAllLibraries() ;
    veri_file::ResetFileNames() ;
    veri_file::UndefineMacroPreDefs() ;
    // CARBON_BEGIN
    veri_file::UndefineCarbonUserDefinedMacros();
    // CARBON_END

    VeriNode::FreeMapOfCommentsArray() ;
    VeriNode::ResetSystemTasksTable() ;
    VeriNode::ResetVerilogKeywordMap() ;
    VeriNode::ResetUdpEdgeCharMap() ;
    VeriNode::ResetMessageMap() ;
    VeriNode::ResetRelaxedMessageMap() ;

    VeriExpression::ResetNameToTokens() ;
    VeriNode::ResetReferenceCountMap() ;
    veri_file::ClearTopModuleList() ;
    VeriStaticElaborator::ResetStatics() ;
    VeriStaticElaborator::ResetIdsUsedInTypeOp() ;
    VeriStaticElaborator::ResetEnumIdVsNewType() ;
    VeriVarUsageInfo::ResetAssignedInfo() ;

}

void
veri_file::ResetParser()
{
    ClearAllFileExt() ;                 // Clear extension to language dialect mapping like +verilog2001ext+ext
    UndefineAllMacros() ;               // Clear out all macro definitions
    RemoveMacroRefLinefiles() ;         // VIPER #7459: Remove macro location info
    ResetAllLibraryPaths() ;            // Clear all library to directory mapping
    ClearDefaultLibraryPath() ;         // Clear default library to directory mapping
    ClearAllLibraryDecls() ;            // Clear all library constructs
    ClearIncludedFiles() ;              // Clear out previous stored `included location information
    RemoveAllIncludeDirs() ;            // Clear out all incdir
    //CARBON_BEGIN
    RemoveAllPragmaTriggers() ;            // Clear out all incdir
    //CARBON_END
    RemoveAllYDirsVFiles() ;            // Clear out -y/-v settings
    RemoveAllLOptions() ;               // Clear out -L settings

    DeleteAllLibraries() ;              // Delete all libraries including all modules
    // Following two are not required since the above did the work
    //ResetRootModule() ;                 // Reset the root module pointer
    //RemoveAllModules() ;                // Delete all module parse trees

    // CHECKME: Should we also reset the following?
    // If we need to, we have to somehow set it to the default value which may
    // differ from customer to customer. So, have to use MACRO for default value
    // and have to provide a Reset*() API for them. Or have to do this in some other way:
    //ResetLoopLimit() ;                  // Reset loop limit
    //SetMaxArraySize(23) ;               // Reset maximum array size to default
    //SetMaxStackDepth(0) ;               // Reset maximum stack depth
    //SetRelaxedLanguageChecking(0) ;     // Reset relaxed checking mode
    //SetIgnoreTranslateOff(0) ;          // Reset ignore translate-off handling
    //SetIgnoreAllPragmas(0) ;            // Reset ignore all pragma handling
    //SetUpperCaseIds(0) ;                // Reset making all ids to upper case
    //SetMinimumRamSize(0) ;              // Reset minimum RAM size
    //SetProtectionObject(0) ;            // Set decipher object for binary save/restore
    //SetTickProtectObject(0) ;           // Set decipher object for `protected envelop decryption
    //SetPragmaProtectObject(0) ;         // Set decipher object for IEEE 1800 protected envelop decryption
}

/* static */ void
veri_file::ResetFileNames()
{
    delete _file_names ;
    _file_names = 0 ;
}

/* static */ void
veri_file::SetTbdmCommentString(const char *comment)
{
    Strings::free(_tbdm_comment_string) ;
    _tbdm_comment_string = Strings::save(comment) ;
}

/* static */ const char *
veri_file::GetTbdmCommentString()
{
    if (!_tbdm_comment_string) _tbdm_comment_string = Strings::save(VERILOG_TBDM_COMMENT_STRING) ;
    return _tbdm_comment_string ;
}

// VIPER #7459: Store macro ref -> def locations.
/* static */ void
veri_file::AddMacroRefLinefile(const char *macro_name, linefile_type ref)
{
    if (!macro_name || !ref) return ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (!_macro_ref_linefiles) _macro_ref_linefiles = new Map(POINTER_HASH) ;
#else
    if (!_macro_ref_linefiles) _macro_ref_linefiles = new Map(NUM_HASH) ;
#endif

    // Get the macro def linefile:
    linefile_type def = (_macro_def_linefiles) ? (linefile_type)_macro_def_linefiles->GetValue(macro_name) : 0 ;

    // This macro ref/def location info is not very useful if we are not storing column info.
    (void) _macro_ref_linefiles->Insert((void *)ref, (void *)def) ;
}

/* static */ void
veri_file::RemoveMacroRefLinefiles()
{
    delete _macro_ref_linefiles ;
    _macro_ref_linefiles = 0 ;
}

// VIPER #8405: Check if the given module is in unresolved modules list in the current -y/-v library:
/* static */ unsigned
veri_file::IsUnresolvedModule(const char *module_name)
{
    if (!module_name) return 0 ;

    if (_current_user_library && _current_user_library->IsUnresolvedModule(module_name)) return 1 ;

    return 0 ;
}

// VIPER #8405: Set of ignored modules from parser:
/* static */ void
veri_file::SetIgnoredModuleNames(Set *list_of_module_names)
{
    _ignored_module_names = list_of_module_names ;
}

// VIPER #8405: Add the given module name into the ignored Set:
/* static */ void
veri_file::AddToIgnoredModuleNames(const char *module_name)
{
    if (!_ignored_module_names || !module_name) return ;

    //Message::Msg(VERIFIC_INFO, 0, 0, "Ignoring module %s", module_name) ;

    char *name = Strings::save(module_name) ;
    if (!_ignored_module_names->Insert(name)) Strings::free(name) ;
}

// VIPER #8405: Check if any of the newly required modules are already ignored:
/* static */ unsigned
veri_file::HasAnyModuleInIgnoredList(const Set *new_modules_added, const Set *ignored_module_names)
{
    if (!ignored_module_names || !new_modules_added) return 0 ;
    if (!ignored_module_names->Size() || !new_modules_added->Size()) return 0 ;

    const Set *list1 = ignored_module_names ;
    const Set *list2 = new_modules_added ;
    if (ignored_module_names->Size() > new_modules_added->Size()) {
         list1 = new_modules_added ;
         list2 = ignored_module_names ;
    }

    SetIter si ;
    const char *mod_name ;
    FOREACH_SET_ITEM(list1, si, &mod_name) {
        if (list2->GetItem(mod_name)) return 1 ; // Found one, return
    }

    return 0 ; // No name matches
}

