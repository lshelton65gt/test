%{
/*
 *
 * [ File Version : 1.810 - 2014/03/28 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

////////////////////////////////////////////////////////////////////////////
//
//                        DEVELOPERS NOTE
//
// Verific recommends that you do not change this file, so that you do NOT
// need to re-run bison either. Verific ran bison on it already, which
// resulted in the file veri_yacc.cpp, which is included in this directory.
//
// If you need to make a custom change to this .y file, or have another
// reason why you need to re-run bison, then make sure you use bison 1.35
// or later, and note that Verific cannot guarantee that the created veri_yacc.cpp
// file will behave properly or even compile on various compilers.
//
// This file contains the %locations directive to keep track
// of accurate line/file info for all bison rules. This is not supported
// in bison 1.28, so use bison 1.35 or later to compile this file.
//
// Use
//    bison -v -d -l -p veri -o veri_yacc.cpp verilog.y
//     mv yeri_yacc.c veri_yacc.cpp
//     mv veri_yacc.h veri_yacc.h
// To create a valid cpp file for compilation with the flex-generated tokenizer
// and the rest of the analyzer code.
//
////////////////////////////////////////////////////////////////////////////
// Bison 1.35 forgets to rename this (global) var :
#define yylloc  verilloc
#define YYLTYPE_IS_TRIVIAL 1 // to fix stack-grow problem in Bison 1.35
// Declare main parser functions, since yacc forgets that due to renaming
extern void verierror(const char *s) ;
extern int verilex(void) ;
extern int veriparse(void) ;

// Files that Bison forgot to include :
#include <string.h>   // For memcpy
#include <stdio.h>    // sprintf
#include <sstream>    // for ostringstream

#include "veri_file.h"
#include "veri_tokens.h" // For YYLTYPE structure

#include "VerificSystem.h"

#include "Map.h"
#include "Array.h"
#include "Message.h"
#include "Strings.h"

#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "VeriStatement.h"
#include "VeriId.h"
#include "VeriConstVal.h"
#include "VeriExpression.h"
#include "VeriMisc.h"
#include "VeriScope.h"
#include "VeriLibrary.h"
#include "VeriCopy.h"

#include "VeriBaseValue_Stat.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif
// Control how deep the parser stack can become before a stack overflow occurs.
// We got an overflow on a madeup design after we supported extern module.
// Make it hundred thousand for now to avoid the overflow. Please note that
// how large it be there will be a point where the stack overflows!
#define YYMAXDEPTH 100000

//CARBON_BEGIN
 void* gLastDeclVeriId;  // the last declared verilog id
//CARBON_END

// UDP table symbols passed through a string
#define MAX_UDP_SYMBOLS 1000
static char tmp_buf[MAX_UDP_SYMBOLS] ;
static unsigned tmp_buf_idx ;
// VIPER #8496: Flag to indicate an edge symbol in UDP table entry:
static unsigned udp_edge_indicator_found ;

// Scope container
// static Array *scope_stack = 0 ;
static VeriScope *present_scope = 0 ;

// Added for Viper #3584 : look for more comments below
static Map *pragma_id_map = 0 ;

// Added for Vipers 5972 and 6009
static Map *previous_pragma_table = 0 ;
unsigned set_previous_pragma_table = 0 ; // this is not so nice. consider cleanup

// Added for Viper #3931 : attribute instance before timescale
static Array *attr_inst_arr = 0 ;

// VIPER #4842: Track multiple default clocking declarations:
static Map *default_clockings = 0 ; // Map of VeriScope * -> VeriClockingDecl *

// Context sensitive syntax : Implicit nets :
static unsigned implicit_nets_allowed = 0 ;

// VIPER #5300: Context information for extends ... endextends block
static unsigned within_class = 0 ;
// VIPER #5248 (open_sqr_coveragebins): Context information for bins repeat tokens
static unsigned within_bins_repeat = 0 ;

// Adding this flag to skip creating name_extended_expression with attributes
// as the attribute could be associated with either the inner or the outer expr
// it may be wise to keep them separate with PortConnect wrapper calss
static unsigned port_connect_has_attribute = 0 ;

// Prevent certain sections of code from resolving identifiers during parsing :
static unsigned resolve_idrefs = 1 ;
static unsigned within_extern_method = 0 ;
static unsigned within_seq_property = 0 ;
static unsigned parsing_prototypes = 0 ; // VIPER #6423: flag to indicate whether we are parsing task/function prototypes

// VIPER #5244: It is not an error to import a package in that package itself.
// Flag to indicate that currently package is parsing. We need it as VeriModuleId
// is created for package also. So scope and its owner cannot indicate whether we
// are within package untill package constructor is called:
static unsigned parsing_package = 0 ;
static unsigned export_decl = 0 ; // VIPER #5845 : Support of export declaration

// VIPER #6966 : To find label of a statement while we are in it
static Array *last_label = 0 ;

// VIPER #7714: Implement stack mechanism for last label to handle nested labeled statements properly:
static void push_last_label(VeriIdDef *label) { if (!last_label) last_label = new Array() ; last_label->InsertLast(label) ; }
static VeriIdDef *pop_last_label()            { return (last_label && last_label->Size()) ? (VeriIdDef *)last_label->RemoveLast() : 0 ; }
static VeriIdDef *get_last_label()            { return (last_label && last_label->Size()) ? (VeriIdDef *)last_label->GetLast() : 0 ; }

// VIPER #7069 : To create different identifier for forkjoin task id
static unsigned extern_forkjoin = 0 ;

// VIPER #7487 : To produce error for duplicate member inside struct/union
static unsigned inside_struct_union = 0 ;

// VIPER #8109 : To indicate we are parsing generate-item of for-generate
static unsigned for_gen_started = 0 ;
/******************************* Column/Line/File Info in rules **********************/

// Since 9/2004, we let flex and bison always work together to process location
// information (column/line/file info) of parsed tokens.
// This is done because of three reasons :
//    (1) in bison, we want accurate line info for the constructs (syntax rules) parsed
//        (not relying on just the last token parsed, but generate info based on the
//        sub-rules per bison rule).
//        The only way to do that is to enable location processing, and use the
//        location range info (yyloc in bison).
//    (2) We have compile switch VERIFIC_LINEFILE_INCLUDES_COLUMNS, which requires
//        us to process location (token/rule start/end column/line) info upon at compile time.
//    (3) Bison 1.375 and later do not allow compile-time control over location processing.

// Need these 4 variables to pick-up rule line/column info from YYLLOC_DEFAULT,
// since the variable that bison uses (yyloc) is a local variable, invisible outside 'parse()'.
static unsigned first_line = 0 ;
static unsigned first_column = 0 ;
static unsigned last_line = 0 ;
static unsigned last_column = 0 ;
static unsigned file_id = 0 ;
// VIPER #5675: Flag to track when Rhs[0] is initialized:
static unsigned rhs_0_initialized = 0 ;
static int design_unit_depth = 0 ; // Keep track of how many nested design units are processing now
static int config_is_identifier = 0 ; // VIPER #6079: Current status/context of the 'config' string in input RTL.

// Overwrite YYLLOC_DEFAULT (executed just before a rule action executes),
// to calculate and store the rule's column info so it can be
// picked up by constructors in the rule's actions.
// NOTE: This only works for Bison version 1.35 or newer.
// Fix Bison problem for rules with 0 args. Need to take yylloc from flex directly.

// Issue 2216 : do not use line/file info from empty sub-rules.
// We need some line/file info for empty subrules, so pick up next token as normal (yylloc)
// but set their last column/line to first column/line. This way, empty rules will
// never cover text into the next token (which is not part of the subrule).
// Bison 2.00 initializes columns to 0. We initialize to 1. Need to correct this here (test for 0 column).
// VIPER #5608, #5675: Update the empty rule line/column/file using Rhs[0] only when it is initialized.
// It is initialized after bison matches any rule, and whenever bison matches a rule it calls this macro.
// So, at the first call of this macro Rhs[0] may be uninitialized but after that it is initialized.
#ifdef VERILOG_USE_STARTING_LINEFILE_INFO
#define YYLLOC_DEFAULT(Current, Rhs, N)                 \
    if ((N)==0) {                                       \
        if (yylloc.first_column==0) { (yylloc.first_column)++ ; (yylloc.last_column)++ ; } \
        Current                 = yylloc ;              \
        if (rhs_0_initialized) {                        \
            Current.last_line   = (Rhs)[0].last_line ;  \
            Current.last_column = (Rhs)[0].last_column ;\
            Current.file_id     = (Rhs)[0].file_id ;    \
        } else {                                        \
            Current.last_line   = 1 ;                   \
            Current.last_column = 1 ;                   \
            Current.file_id     = 0 ;                   \
        }                                               \
    } else {                                            \
        Current.first_line   = (Rhs)[1].first_line ;    \
        Current.first_column = (Rhs)[1].first_column ;  \
        Current.last_line    = (Rhs)[N].last_line ;     \
        Current.last_column  = (Rhs)[N].last_column ;   \
        Current.file_id      = (Rhs)[1].file_id ;       \
    }                                                   \
    first_line   = Current.first_line ;                 \
    first_column = Current.first_column ;               \
    last_line    = Current.last_line ;                  \
    last_column  = Current.last_column ;                \
    file_id      = Current.file_id ;                    \
    rhs_0_initialized  = 1 ; // Now it is initialized
    // set static variables, for pick-up of 'last rule location info'..
#else
#define YYLLOC_DEFAULT(Current, Rhs, N)                 \
    if ((N)==0) {                                       \
        if (yylloc.first_column==0) { (yylloc.first_column)++ ; (yylloc.last_column)++ ; } \
        Current                 = yylloc ;              \
        if (rhs_0_initialized) {                        \
            Current.last_line   = (Rhs)[0].last_line ;  \
            Current.last_column = (Rhs)[0].last_column ;\
            Current.file_id     = (Rhs)[0].file_id ;    \
        } else {                                        \
            Current.last_line   = 1 ;                   \
            Current.last_column = 1 ;                   \
            Current.file_id     = 0 ;                   \
        }                                               \
    } else {                                            \
        Current.first_line   = (Rhs)[1].first_line ;    \
        Current.first_column = (Rhs)[1].first_column ;  \
        Current.last_line    = (Rhs)[N].last_line ;     \
        Current.last_column  = (Rhs)[N].last_column ;   \
        Current.file_id      = (Rhs)[N].file_id ;       \
    }                                                   \
    first_line   = Current.first_line ;                 \
    first_column = Current.first_column ;               \
    last_line    = Current.last_line ;                  \
    last_column  = Current.last_column ;                \
    file_id      = Current.file_id ;                    \
    rhs_0_initialized  = 1 ; // Now it is initialized
    // set static variables, for pick-up of 'last rule location info'..
#endif // VERILOG_USE_STARTING_LINEFILE_INFO
    
/******************************* Routines used locally **********************/
// VIPER 2573, 2595, 2603 : Added an argument so that we can create empty scope
// i.e. scope having no variable declaration in it.
static void
push_scope(VeriIdDef *owner, unsigned empty = 0, unsigned st_union_scope=0) // VIPER 2573, 2595, 2603
{
    for_gen_started = 0 ; // VIPER #8109 : Reset for-gen flag
    // Create a new scope, as derived from present_scope :
    present_scope = (empty) ? new VeriScope(empty, present_scope) : new VeriScope(present_scope, owner) ;
    // VIPER #5357 : Mark structure/union scope
    if (st_union_scope && present_scope) present_scope->SetStructUnionScope() ;
    // VIPER #4977 : Reset reference count map at the starting of new scope
    VeriNode::ResetReferenceCountMap() ;
}

static void cleanup_treenode_array(Array *tree_node_array)
{
    unsigned i ;
    VeriTreeNode *node ;
    FOREACH_ARRAY_ITEM(tree_node_array, i, node) delete node ;
    delete tree_node_array ;
}
//CARBON_BEGIN
/*
static void shut_verific_up(){
  Message::SetMaxErrorCount(Message::ErrorCount());
}
*/
//CARBON_END

// VIPER #3931 : Routines to allow attribute instance before timescale.
// If attribute instance appears before timescale, we will store that attribute
// in a static array. After parsing module/module-item/statement when we add
// attribute to them, we will also add these stored attribute to that parsed
// parse tree and will reset the static array.

// Store attribute appears before timescale in global static array :
static void
store_attribute(const Map *attr)
{
    if (!attr) return ;
    if (!attr_inst_arr) attr_inst_arr = new Array(2) ;
    attr_inst_arr->InsertLast(attr) ;
}
// Append stored attributes to the parsed module/module-item/statement :
static void
append_attributes(const VeriModuleItem *item)
{
    if (!item || !attr_inst_arr) return ;

    unsigned i ;
    Map *attr ;
    FOREACH_ARRAY_ITEM(attr_inst_arr, i, attr) {
        if (attr) item->AddAttributes(attr) ;
    }
    delete attr_inst_arr ;
    attr_inst_arr = 0 ;
}

static VeriDataType *
create_net_data_type(unsigned net_type, VeriName *name_ref, unsigned signing, VeriRange *dims)
{
    if (!name_ref) return new VeriDataType(net_type, signing, dims) ;
    // In system verilog create VeriNetDataType
    return new VeriNetDataType(net_type, new VeriTypeRef(name_ref, signing, dims)) ;
}
// Added following for Viper #3584 : Remember the meta comment pragma attributes
// when they are given BEFORE the actual objects (that they apply on) are declared
// They are remembered into the static map pragma_id_map, and at the end of the
// scope, this map is processed in this API for the entries against the scope
static void
ProcessPragmasForScope(VeriScope *ps, unsigned cleanup_everything)
{
    if (!ps || !pragma_id_map) return ;

    Array pragma_processed_items ; // Need to remove the items from map

    MapItem *iter ;
    FOREACH_SAME_KEY_MAPITEM(pragma_id_map, ps, iter) {
        Array *pragma_info = (Array *) iter->Value() ;
        pragma_processed_items.Insert(iter) ;

        if (!pragma_info) continue ;

        VERIFIC_ASSERT(pragma_info->Size() == 4) ;

        char *id_name = (char *) pragma_info->At(0) ;
        char *attr_name = (char *) pragma_info->At(1) ;
        char *attr_val = (char *) pragma_info->At(2) ;
        VeriTreeNode *tmp_node = (VeriTreeNode *) pragma_info->At(3) ;

        VeriIdDef *id = ps->Find(id_name) ;
        if (!id) { // Viper: 3876
            // maybe it is a module. These are defined in the work library :
            VeriLibrary *work_lib = veri_file::GetWorkLib() ;
            VeriModule *module = work_lib ? work_lib->GetModule(id_name, 1/*case-sensitive*/, 0/* do not restore*/) : 0 ;
            id = module ? module->GetId() : 0 ;
        } // End : Viper: 3876

        if (!id) {
            tmp_node->Warning("attribute target identifier %s not found in this scope", id_name) ;
        } else {
            id->AddIdPragma(attr_name,attr_val) ;
        }

        Strings::free(id_name) ;
        Strings::free(attr_name) ;
        Strings::free(attr_val) ;

        delete tmp_node ;
        delete pragma_info ;
    }

    unsigned i ;
    FOREACH_ARRAY_ITEM(&pragma_processed_items, i, iter) (void) pragma_id_map->Remove(iter) ;

    if (cleanup_everything) { // end-of-compilation-unit
        Array *pragma_info = 0 ;
        MapIter mi ;
        FOREACH_MAP_ITEM(pragma_id_map, mi, 0, &pragma_info) {
            VERIFIC_ASSERT(pragma_info->Size() == 4) ;

            char *id_name = (char *) pragma_info->At(0) ;
            char *attr_name = (char *) pragma_info->At(1) ;
            char *attr_val = (char *) pragma_info->At(2) ;
            VeriTreeNode *tmp_node = (VeriTreeNode *) pragma_info->At(3) ;

            Strings::free(id_name) ;
            Strings::free(attr_name) ;
            Strings::free(attr_val) ;

            delete tmp_node ;
            delete pragma_info ;
        }

        pragma_id_map->Reset() ;
    }

    if (pragma_id_map->Size() == 0) {
        delete pragma_id_map ;
        pragma_id_map = 0 ;
    }

    return ;
}

static VeriScope *
pop_scope(unsigned from_package = 0)
{
    if (!present_scope) return 0 ; // only possible in messed-up syntax errors
    // pop scope to upper one, but return the (present) subscope
    VeriScope *ps = present_scope ;
    present_scope = present_scope->Upper() ;

    // VIPER #3031: We are going out of this scope, check whether all the forward
    // declarations inside this scope are actually defined within it or not:
    // VIPER #4134 : Do not check incomplete type definition for package scope.
    // Full definition can be inside module where this type is imported. (4.9 last line)
    if (!from_package) ps->CheckIncompleteTypedef() ;
    // VIPER #7232 : Validate the exported items
    ps->CheckExportedItems() ;
    // VIPER #4977 : Reset reference count map at the end of scope
    VeriNode::ResetReferenceCountMap() ;

    ProcessPragmasForScope(ps, 0) ; // Viper #3584

    // VIPER #4842: Remove any reference to default clocking for this scope.
    // We don't need it anymore. So, removing it will not increase memory usage.
    if (default_clockings) (void) default_clockings->Remove(ps) ;

    return ps ;
}

// VIPER #2907 : Add an default argument to indicate first argument specified
// id-def is a port identifier, require to check redeclaration of port
static VeriIdDef *
declare_net_reg(VeriVariable *id, VeriExpression *init_val=0, unsigned port_id = 0, VeriName *name = 0)
{
    if (!id) return 0 ;
    (void) name ; // to prevent unused argument compiler warnings
    // Check if there is an existing identifier by that name
    VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(id->Name()) : 0 ;
    // VIPER #4977 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '2' refers that it is only used
    // to declare identifier. In id_ref rule ref count increase by 1 and in above
    // FindLocal by another '1'.  Reference count > 2 means actually referenced.
    // So do not make it explicitly defined when ref count = 2
    // VIPER #5912 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '1' means in id_ref rule for this
    // decl_id ref count increase by 1, but in FindLocal it is not increased as
    // import construct is in upper scope :
    unsigned ref_count = VeriNode::GetReferenceCount(exist ? exist : (name ? name->GetId(): 0)) ;
    if ((exist && (ref_count == 2)) || (!exist && (ref_count == 1))) {
        if (present_scope) (void) present_scope->UndeclareExplicitImport(exist ? exist : (name ? name->GetId(): 0)) ;
        exist = 0 ;
    }
    if (exist) {
        // Legal ito re-declare a variable (or net) in some cases (port declarations).
        // So let this pass through, and error out only once we know if there is a real violation.
        unsigned error_warn_reported = 0 ;
        if (!exist->IsVar()) {
            // If its not a variable, error out
            id->Error("%s is already declared", id->Name()) ;
            exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
            error_warn_reported = 1 ;
        } else if (exist->IsImplicitNet()) {
            // VIPER 2775 : warn if this net is implicitly declared
            unsigned line_no = LineFile::GetLineNo(exist->Linefile()) ;
            id->Warning("%s is already implicitly declared on line %d", id->Name(), line_no) ; // VIPER #8172: improve error message
            // exist->Info("%s is declared here",exist->Name()) ;
            error_warn_reported = 1 ;
        } else if (exist->IsAnsiPort()) {
            // VIPER #2890: Error out for redeclaration of ANSI ports:
            id->Warning("redeclaration of ansi port %s is not allowed", id->Name()) ;
            //exist->Info("%s is declared here", exist->Name()) ;
            error_warn_reported = 1 ;
        } else if (exist->IsPort() && port_id) {
            // VIPER #2907 : Issue error if direction is specified in redeclaration of port
            id->Error("port %s is already defined", id->Name()) ;
            error_warn_reported = 1 ;
            //CARBON_BEGIN
            if (name -> GetId()) {name -> GetId() -> SetLinefile(id -> Linefile());} //use line file from redeclaration
            //CARBON_END
        } else if (!port_id && exist->Type()) {
            // Declared variable has a declared 'type' already. This means we are now re-declaring this identifier.
            // Add !port_id check for if port is defined after net is declared
            // VIPER 5960 / 6955 need to catch re-declaration here, so we know what to do with initial value
            // VIPER #7487 : Produce error for duplicate struct/union member
            if (inside_struct_union) {
                id->Error("struct/union element %s is already declared", id->Name()) ;
            } else {
                id->Warning("%s is already declared", id->Name()) ;
                exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
            }
            id->Warning("second declaration of %s ignored", id->Name()) ;
            exist->Info("%s is declared here",exist->Name()) ;
            error_warn_reported = 1 ;
        }

        // VIPER #2899 : Issue error if existing identifier contains unpacked dimension,
        // but current identifier 'id' does not or vice versa
        if ((exist->IsMemory() && !id->IsMemory()) || (!exist->IsMemory() && id->IsMemory())) {
            if (exist->PackedDimension() || exist->UnPackedDimension()) {
                    id->Error("inconsistent dimension in declaration") ;
                    error_warn_reported = 1 ;
            }
        }

        // VIPER 1578 - Clear implicit net setting (if set)
        exist->ClearImplicitNet() ;

        // Viper 5516/5563 Add the dimensios of id to exist
        // VIPER #7704: Set the unpacked dimensions for ANSI ports, even if we produced error/warning above.
        // We do this for initial value. Also, other simulators preserve the unpacked dimensions:
        if ((!error_warn_reported || exist->IsAnsiPort()) && id->GetDimensions() && !exist->GetDimensions()) {
            exist->SetMemoryRange(id->GetDimensions()) ;
            id->SetMemoryRangeNull() ;
        }

        // VIPER 5960 / 6955 : set the initial value onto the surviving existing identifier
        // VIPER #7103: Set the initial value for ANSI ports, even if we produced error/warning above:
        if ((!error_warn_reported || exist->IsAnsiPort()) && init_val && !exist->GetInitialValue()) {
            exist->SetInitialValue(init_val) ;
        } else {
            delete init_val ;
        }

        // delete the new identifier (it was not stored in the scope).
        delete id ;

        //CARBON_BEGIN
        gLastDeclVeriId = exist;
        //CARBON_END
        // and always return the existing one
        return exist ;
    }

    // Set initial value onto this identifier
    if (init_val) id->SetInitialValue(init_val) ;
    if (name) id->SetLinefile(name->Linefile()) ; // VIPER #7405: Set the linefile from the declaration name

    // Otherwise, declare a new one here, and return it.
    if (present_scope && !present_scope->Declare(id)) {
        delete id ;
        id = 0 ;
    }
    //CARBON_BEGIN
    gLastDeclVeriId = id ; 
    //CARBON_END
    return id ;
}

static VeriInstId *
declare_inst (VeriInstId *id)
{
    if (!id) return 0 ;
    // Check if there is an existing identifier by that name
    VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(id->Name()) : 0 ;
    // VIPER #4977 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '2' refers that it is only used
    // to declare identifier. In id_ref rule ref count increase by 1 and in above
    // FindLocal by another '1'.  Reference count > 2 means actually referenced.
    // So do not make it explicitly defined when ref count = 2
    // VIPER #5912 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '1' means in id_ref rule for this
    // decl_id ref count increase by 1, but in FindLocal it is not increased as
    // import construct is in upper scope :
    unsigned ref_count = VeriNode::GetReferenceCount(exist ? exist : id) ;
    if ((exist && (ref_count == 2)) || (!exist && (ref_count == 1))) {
        if (present_scope) (void) present_scope->UndeclareExplicitImport(exist ? exist : id) ;
        exist = 0 ;
    }

    // Instances always get declared and stored in scope.
    // Unnamed instances go into the scope without checking (force-insert).
    // Named instances do a check and then report error if existing.
    // Either way, we let the id go through.

    // Try to declare in the scope :
    VeriIdDef *existing_id = 0 ;
    if (present_scope && !present_scope->Declare(id, 1/*force_insert*/, &existing_id)) {
        // It went in, but it was previously declared.
        if (!id->Name()) {
            // unnamed instance. OK to have collisions.
        } else {
            id->Error("%s is already declared", id->Name()) ;
            if (existing_id) existing_id->Info("previous declaration of %s is from here", existing_id->Name()) ; // VIPER #7773
        }
    }
    return id ; // always return previous identifier.
}

static VeriIdDef *
declare_id (VeriIdDef *id)
{
    if (!id) return 0 ;

    // Try to declare in the scope :
    VeriIdDef *exist = 0 ;
    if (present_scope && !present_scope->Declare(id, 0 /* no force insert */, &exist)) {
        id->Error("%s is already declared", id->Name()) ;
        if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        delete id ;
        return 0 ;
    }
    return id ;
}

// VIPER 2595, 2603 : Don't declare block id defined in the design in empty scope.
// But, we should declare internal block ids in empty scopes for proper
// implementation of break, continue.
static VeriIdDef *
declare_block_id (VeriIdDef *id)
{
    if (!id) return 0 ;
    // Try to declare in the scope :
    VeriIdDef *exist = 0 ;
    if (present_scope && !present_scope->DeclareBlockId(id, 0 /* no force insert */, &exist)) {
        id->Error("%s is already declared", id->Name()) ;
        if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        delete id ;
        return 0 ;
    }
    return id ;
}

static VeriName *
create_indexed_name(VeriName *name, VeriRange *indexes)
{
    // Create id-ref/indexed-name/indexed-memory from VeriRange arguments
    if (!indexes) return name ; // No indexes, return name

    // RD: 1/07: Moved the syntax/semantic checks of range arguments into the Resolve routines.
    // So this routine now becomes rather trivial : just convert range to expression (if range has only one arg).
    VeriName *result = 0 ;
    if (!indexes->GetNext()) {
        // Only one index, create VeriIndexedId
        // It can be '[left:right]' or '[expr]'. For '[expr]' only 'expr' is stored, not range
        VeriExpression *left = indexes->GetLeft() ;
        VeriExpression *right = indexes->GetRight() ;
        if ((left && right) || (!left && !right)) {
            // VIPER 4488. Section 4.2.2 1364 LRM
            if (!left && !right && indexes->GetPartSelectToken() == VERI_NONE) {
                indexes->Error("syntax error near %s", "]") ;
                delete indexes ;
                delete name ;
                return 0 ;
            }
            // index '[left:right]' or '[]' / '[*]'. So keep the range :
            result = new VeriIndexedId(name, indexes) ;
        } else {
            // index is '[expr]'. Insert as an expression :
            // Extract bound and use that to create 'VeriIndexedId'
            result = new VeriIndexedId(name, left ? left : right) ;
            indexes->SetFieldsNull() ; // because its argument (left or right) has just changed owner.
            delete indexes ; // Delete empty 'indexes'
        }
    } else {
        // Multiple indexes, create VeriIndexedMemoryId
        Array *idxs = new Array(2) ; // Create array to store indexes

        // Traverse each 'VeriRange' in link list and create an array
        while (indexes) {
            // Pick up 'next' range for later
            VeriRange *next = indexes->GetNext() ;

            // Get left/right side to see if this is a 'expression' or a true range.
            VeriExpression *left = indexes->GetLeft() ;
            VeriExpression *right = indexes->GetRight() ;

            if ((left && right) || (!left && !right)) {
                // VIPER 5392. Section 4.2.2 1364 LRM
                if (!left && !right && indexes->GetPartSelectToken() == VERI_NONE) {
                    indexes->Error("syntax error near %s", "]") ;
                    delete indexes ;
                    delete name ;
                    cleanup_treenode_array(idxs) ;
                    return 0 ;
                }
                // Set 'next' field to null, so that range is by itself :
                indexes->SetNextNull() ;
                idxs->InsertLast(indexes) ;
            } else {
                // Normal expression. Use the expression and insert :
                idxs->InsertLast(left ? left : right) ;
                indexes->SetFieldsNull() ; // because its argument (left or right) has just changed owner.
                delete indexes ;
            }
            indexes = next ;
        }
        // JJ: need to clean up Array* idxs if empty
        if (idxs->Size()) {
            result = new VeriIndexedMemoryId(name, idxs) ;
        } else {
            result = name ;
            delete idxs ;
        }
    }
    return result ;
}

static VeriIdDef *
declare_port(VeriName *name, VeriRange *unpacked_dims, VeriExpression *init_val)
{
    // Create a port (VeriIdDef) from a VeriName.
    // This should be a simple name (simple identifier).

    // If port name is hierarchical identifier, issue error
    if (!name) return 0 ;
    if (name->IsHierName()) {
        name->Error("syntax error near %s", name->GetName()) ;
        return 0 ;
    }

    // 'name' is simple name, create port identifier with it
    char *id_name = Strings::save(name->GetName()) ; // Save simple name

    // Create a VeriVariable.
    VeriVariable *id = 0 ;
    if (within_seq_property) {
        id = new VeriSeqPropertyFormal(id_name, unpacked_dims) ;
    } else {
        id = new VeriVariable(id_name, unpacked_dims) ;
    }

    // Set line/file info on id to the line/file info from 'name' :
    id->SetLinefile(name->Linefile()) ;

    // And declare it (as a net/reg) :
    VeriIdDef *ret_id =  declare_net_reg(id, init_val, 1 /* 'id' is port identifier */, name) ;

    // Now delete 'name'. It was only a placeholder for the name.
    delete name ;

    return ret_id ;
}

static VeriIdDef *
declare_param_id(VeriName *name, VeriRange *mem_range, VeriExpression *init_val)
{
    // Create a parameter (VeriIdDef) from a VeriName.
    // This should be a simple name (simple identifier).

    // If 'name' is hierarchical identifier, it is error 'parameter p.x = 10'
    if (!name) return 0 ;
    if (name->IsHierName()) {
        name->Error("syntax error near %s", name->GetName()) ;
        return 0 ;
    }
    // Check if there is an existing identifier by that name
    VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(name->GetName()) : 0 ;
    // VIPER #4977 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '2' refers that it is only used
    // to declare identifier. In id_ref rule ref count increase by 1 and in above
    // FindLocal by another '1'.  Reference count > 2 means actually referenced.
    // So do not make it explicitly defined when ref count = 2
    // VIPER #5912 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '1' means in id_ref rule for this
    // decl_id ref count increase by 1, but in FindLocal it is not increased as
    // import construct is in upper scope :
    unsigned ref_count = VeriNode::GetReferenceCount(exist ? exist : name->GetId()) ;
    if ((exist && (ref_count == 2)) || (!exist && (ref_count == 1))) {
        if (present_scope) (void) present_scope->UndeclareExplicitImport(exist? exist : name->GetId()) ;
        exist = 0 ;
    }

    // Declare param id with only simple name
    VeriIdDef *id = declare_id(new VeriParamId(Strings::save(name->GetName()))) ;

    // Set line/file info on id to the line/file info from 'name' :
    // VIPER #2686 : Added null check for 'id'. 'declare_id' will return null
    // if any object of same name is already declared in this design scope
    if (id) {
        id->SetLinefile(name->Linefile()) ;
        // Set memory range and initial value :
        if (mem_range) id->SetMemoryRange(mem_range) ; // Set memory range
        if (init_val) {
            id->SetInitialValue(init_val) ; // Set initial value
        } else {
            if (VeriNode::IsVeri95() || VeriNode::IsVeri2001() || VeriNode::IsSystemVeri2005()) {
                // VIPER #6222: Only IEEE 1800 (2009) allow param declaration without initial value:
                id->Error("parameter initial value cannot be omitted in this mode of verilog") ;
            } else {
                // VIPER #6222: Accept parameter declaration without initial value in IEEE 1800 (2009) mode:
                // But do produce a warning, because all tools produce error, with this warning
                // in place, user may easily upgrade it to error and match other tools.
                id->Warning("initial value of parameter %s is omitted", id->Name()) ;
            }
        }
    } else {
        delete mem_range ;
        delete init_val ;
    }
    delete name ; // Delete incoming name
    return id ;
}

static VeriDataType *
create_net_port_type(VeriDataType *net_type, VeriDataType *data_type)
{
    if (!net_type) return data_type ; // Normal data type
    if (!data_type) return net_type ; // Normal data type

    // Both types exist. So first type should only be net type.
    // Check, if 'net_type' contains anything other than nettype, error
    if (net_type->IsNetDataType()) {
        net_type->Error("illegal net data type") ;
        delete net_type ;
        return data_type ;
    }
    unsigned nettype = net_type->GetType() ;
    if (net_type->GetSigning() || net_type->GetDimensions()) {
        net_type->Error("illegal net data type") ;
    }
    delete net_type ;
    return new VeriNetDataType(nettype, data_type) ;
}

// VIPER# 4179 : Support for out of class constraint declaration.
// This function is added only to support out of class constraint declaration.
static VeriScope *
create_constraint_id(VeriName *name, unsigned only_extern_decl, unsigned is_static)
{
    if (!name) return 0 ; // Nothing to do without name
    VeriIdDef *constraint_id = 0 ;
    VeriScope *constraint_scope = present_scope ;

    // 'name' can only be class_scope::constraint_name or constraint_name
    if (!name->IsHierName()) { // simple name
        // VIPER #4716: Only extern constraints are allowed as module item. Produce
        // error if 'only_extern_decl' is 1 and this is simple name.
        if (only_extern_decl) {
            name->Error("syntax error near %s", name->GetName()) ;
            return 0 ;
        }
        char *constraint_name = Strings::save(name->GetName()) ;
        // Check if there is an existing identifier by that name
        VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(constraint_name) : 0 ;
        // VIPER #4977 : We parse identifier to be declared as reference and resolve
        // reference using 'Find'. Reference count '2' refers that it is only used
        // to declare identifier. In id_ref rule ref count increase by 1 and in above
        // FindLocal by another '1'.  Reference count > 2 means actually referenced.
        // So do not make it explicitly defined when ref count = 2
        // VIPER #5912 : We parse identifier to be declared as reference and resolve
        // reference using 'Find'. Reference count '1' means in id_ref rule for this
        // decl_id ref count increase by 1, but in FindLocal it is not increased as
        // import construct is in upper scope :
        unsigned ref_count = VeriNode::GetReferenceCount(exist ? exist : name->GetId()) ;
        if ((exist && (ref_count == 2)) || (!exist && (ref_count == 1))) {
            if (present_scope) (void) present_scope->UndeclareExplicitImport(exist ? exist : name->GetId()) ;
            exist = 0 ;
        }
        if (constraint_name)  constraint_id = declare_id(new VeriVariable(constraint_name)) ;
        if (!is_static && constraint_id) constraint_id->SetIsAutomatic() ;
        name->SetId(constraint_id) ;
        if (constraint_id) constraint_id->SetLinefile(name->Linefile()) ;
        return constraint_scope ;
    } else if (!name->IsScopeName()) { // dot separated name
        name->Error("syntax error near %s", name->GetName()) ;
        return 0 ;
    } else { // constraint defined in a class
        char *constraint_name = 0 ;
        VeriName *prefix = name->GetPrefix() ;
        const char *suffix = name->GetSuffix() ;
        while(prefix) {
            if (prefix->IsHierName() && !prefix->IsScopeName()) {
                // 'prefix' contains dot-separated name : Error
                prefix->Error("syntax error near %s", prefix->GetName()) ;
                break ;
            }
            if (!constraint_name) {
                // Do not create iddef for class_name::constraint_name. Try to find class first and then
                // constraint from there. Class declaration should be in the same scope as out-of-block
                // constraint declaration.
                VeriIdDef *class_id = (present_scope) ? present_scope->FindLocal(prefix->GetName()) : 0 ;
                if (class_id && class_id->IsClass()) {
                    VeriScope *class_scope = class_id->LocalScope() ;
                    // Find constraint in class scope :
                    constraint_scope = class_scope ;
                    constraint_id = (class_scope) ? class_scope->FindLocal(suffix): 0 ;
                    // VIPER #4837 : Produce error if extern constraint decl contains
                    // static qualifier, but prototype does not have that.
                    if (constraint_id) {
                        if (is_static && constraint_id->IsAutomatic()) {
                            name->Error("static qualifier for constraint block %s does not match prototype", constraint_id->Name()) ;
                        }
                        if (!is_static) constraint_id->SetIsAutomatic() ;
                    } else {
                        name->Error("%s is not declared", suffix) ;
                        constraint_id = 0 ;
                    }
                } else { // Prefix not a class, Error :
                    prefix->Error("%s is not a type", prefix->GetName()) ;
                }
            } else { // Illegal syntax
                prefix->Error("syntax error near %s", prefix->GetName()) ;
                break ;
            }
            prefix = prefix->GetPrefix() ;
        }
    }
    if (constraint_id) name->SetId(constraint_id) ;
    return constraint_scope ;
}

// VIPER #7070 : If elements contain 'label:value', return 1
unsigned contain_concat_item(Array *elements)
{
    if (!elements) return 0 ;
    unsigned i ;
    VeriExpression *ele ;
    FOREACH_ARRAY_ITEM(elements, i, ele) {
        if (ele && (ele->IsConcatItem() || ((ele->IsAssignPattern() || ele->IsMultiAssignPattern()) && !ele->GetTargetType()))) return 1 ;
    }
    return 0 ;
}
// VIPER #7410 : Set base class scope if possible to catch illegal identifier
// resolving in earlier.
void set_base_class_scope(VeriName *base_name)
{
    VeriTypeInfo *base_type = base_name->CheckType(0, VeriTreeNode::NO_ENV) ;
    VeriIdDef *base_class = base_type ? base_type->GetNameId(): 0 ;
    if (base_class && !base_class->IsClass()) base_class = 0 ;
    if (present_scope && base_class && base_class->LocalScope()) {
        present_scope->SetBaseClassScope(base_class->LocalScope()) ;
    }
    delete base_type ;
}

// VIPER #2665 & 2667 : Support for out-of-block function declaration :
// VIPER #2703 : Return char* equivalent of argument specified 'name'.
// FIXME : We should create appropriate VeriIdDef for different function names :
//     interface.func_name
//     class1::func_name
//     simple_name
// VIPER #7410 : This method will create method id and create a new scope using
// that function. So no need to return method identifier
static void
create_method_id(VeriName *name, unsigned is_task=0)
{
    if (!name) return ; // Nothing to do without name
    VeriIdDef *func_id = 0 ;

    // 'name' can be i) simple name, ii) interface_name.func_name
    // iii) class_scope::func_name
    if (!name->IsHierName()) { // simple name
        char *func_name = Strings::save(name->GetName()) ;
        unsigned within_modport = 0 ;
        // Check if there is an existing identifier by that name
        VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(func_name) : 0 ;
        // VIPER #4977 : We parse identifier to be declared as reference and resolve
        // reference using 'Find'. Reference count '2' refers that it is only used
        // to declare identifier. In id_ref rule ref count increase by 1 and in above
        // FindLocal by another '1'.  Reference count > 2 means actually referenced.
        // So do not make it explicitly defined when ref count = 2
        // VIPER #5912 : We parse identifier to be declared as reference and resolve
        // reference using 'Find'. Reference count '1' means in id_ref rule for this
        // decl_id ref count increase by 1, but in FindLocal it is not increased as
        // import construct is in upper scope :
        unsigned ref_count = VeriNode::GetReferenceCount(exist ? exist : name->GetId()) ;
        if ((exist && (ref_count == 2)) || (!exist && (ref_count == 1))) {
            if (present_scope) (void) present_scope->UndeclareExplicitImport(exist ? exist : name->GetId()) ;
            exist = 0 ;
        }
        // VIPER #4777, 4786, 4698 etc. If task/function prototypes are declared within modport declaration, create
        // prototype ids for task/function. These will be linked with actual definitions.
        VeriScope *scope =  present_scope ;
        VeriIdDef *owner = scope->GetOwner() ;
        if (owner && owner->IsModport()) within_modport = 1 ;
        if (within_modport) func_id = declare_id(new VeriPrototypeId(func_name)) ;
        // VIPER #5549: Try to create method id if we are not within modport:
        if (!func_id && !within_modport) {
            if (is_task) {
                // VIPER #7069 : For extern fork-join task, create different iddef
                if (extern_forkjoin) {
                    func_id = declare_id(new VeriExternForkjoinTaskId(func_name)) ;
                } else {
                    func_id = declare_id(new VeriTaskId(func_name)) ;
                }
            } else {
                // VIPER #5705: Allow more than one extern declaration, keep the first one:
                VeriIdDef *fn_id = (present_scope) ? present_scope->FindLocal(func_name) : 0 ;
                VeriModuleItem *body = (fn_id) ? fn_id->GetModuleItem() : 0 ;
                if (body && body->GetQualifier(VERI_EXTERN) && !body->IsVerilogMethod()) {
                    // Method already declared as extern "C", return that id:
                    Strings::free(func_name) ;
                    push_scope(fn_id) ;
                    return ;
                }
                func_id = declare_id(new VeriFunctionId(func_name)) ;
            }
        }
        name->SetId(func_id) ;
        if (func_id) func_id->SetLinefile(name->Linefile()) ;
        push_scope(func_id) ;
        return ;
    }
    char *func_name = 0 ;
    VeriScope *container_class_scope = 0 ;
    // Name contains prefix and suffix.
    // If the name is dot-separated, there must only be two dot-separated name
    if (!name->IsScopeName()) { // dot-separated name
        VeriName *prefix = name->GetPrefix() ;
        if (!prefix || prefix->IsHierName()) {
            // Hier name contains more than two elements, error
            name->Error("syntax error near %s", name->GetName()) ;
        } else { // Valid function name
            // FIX ME : interface prefix.. Should declare into that scope ?
            func_name = Strings::save(prefix->GetName(), " ", name->GetSuffix()) ;
            if (is_task) {
                func_id = declare_id(new VeriTaskId(func_name)) ;
            } else {
                func_id = declare_id(new VeriFunctionId(func_name)) ;
            }
            if (func_id) func_id->SetLinefile(name->Linefile()) ;
        }
    } else { // function defined in a class
        VeriName *prefix = name->GetPrefix() ;
        const char *suffix = name->GetSuffix() ;
        while(prefix) {
            if (prefix->IsHierName() && !prefix->IsScopeName()) {
                // 'prefix' contains dot-separated name : Error
                prefix->Error("syntax error near %s", prefix->GetName()) ;
                Strings::free(func_name) ;
                func_name = 0 ;
                break ;
            }
            // FIX ME : extern-class prefix.. reference into that scope ?
            if (!func_name) {
                // Do not create iddef for class_name::method_name. Try to find class first and then
                // method from there. Class declaration should be in the same scope as out-of-block
                // function declaration.
                VeriIdDef *class_id = (present_scope) ? present_scope->FindLocal(prefix->GetName()) : 0 ;
                // VIPER #5248 (test_12): Function name may reference to a nested class, try getting the id directly:
                if (!class_id) {
                    class_id = prefix->GetId() ;
                    // IEEE 1800 LRM section 7.22: Out-of-block declarations must be declared in the same scope
                    // as the class declaration. Since we cannot find the class in 'present_scope' but could
                    // find the resolved id from the hier-name, it must be a out-of-block declaration that does
                    // not follow the above LRM restriction, produce a warning message:
                    // VIPER #7980: Make this an error, otherwise save/restore may fail:
                    if (class_id) prefix->Error("out-of-block function declaration %s violates IEEE 1800 semantic", name->GetName()) ;
                }
                if (class_id && class_id->IsClass()) {
                    VeriScope *class_scope = class_id->LocalScope() ;
                    // Find method in class scope :
                    func_id = (class_scope) ? class_scope->FindLocal(suffix): 0 ;
                    if (!func_id) { // No member of that name in class
                        name->Error("%s is not declared", suffix) ;
                        func_id = 0 ;
                        name->SetId(0) ; // VIPER #8156
                    } else if (is_task && !func_id->IsTask()) { // Required task, but it is not task
                        name->Error("%s is not a task", suffix) ;
                        func_id = 0 ;
                        name->SetId(0) ; // VIPER #8156
                    } else if (!is_task && !func_id->IsFunction()) { // Required function, but it is not a function
                        prefix->Error("%s is not a function", suffix) ;
                        func_id = 0 ;
                        name->SetId(0) ; // VIPER #8156
                    } else { // Method is declared within class
                        // Check whether method is declared as extern, or not
                        VeriModuleItem *body = func_id->GetModuleItem() ;
                        if (body && body->GetQualifier(VERI_EXTERN)) { // Method declared as extern
                            // VIPER #7410: We are setting upper of extern function to
                            // class scope here. So no need to identify extern function
                            // to stop resolving identifier reference inside that
                            // So VIPER #5147 will remain fixed
                            //within_extern_method = 1 ;

                            VeriScope *old_scope = func_id->LocalScope() ;
                            if (old_scope) old_scope->SetOwner(0) ; // Viper #5500

                            prefix->SetId(class_id) ; // Set class id in prefix
                            container_class_scope = class_scope ;
                        } else { // Method is not extern: multiple declarations
                            name->Error("%s is already declared", suffix) ;
                            func_id->Info("previous declaration of %s is from here", func_id->Name()) ; // VIPER #7773
                            func_id = 0 ;
                            name->SetId(0) ; // Reset subprogram name
                        }
                        // VIPER #5248 (test_12): Found the function id, break out of the loop:
                        break ;
                    }
                } else { // Prefix not a class, Error :
                    prefix->Error("%s is not a type", prefix->GetName()) ;
                }
            } else { // Illegal syntax
                prefix->Error("syntax error near %s", prefix->GetName()) ;
                Strings::free(func_name) ;
                func_name = 0 ;
                break ;
            }
            prefix = prefix->GetPrefix() ;
        }
    }

    // VIPER 3059 : return a valid string if we could not create one.
    //if (!func_name) func_name = Strings::save(name->GetName()) ;
    
    name->SetId(func_id) ; // VIPER #8156: Set null id too
    push_scope(func_id) ;
    // For out-of-block function declaration set upper scope of function as class scope
    if (func_id && present_scope && container_class_scope) present_scope->SetUpper(container_class_scope) ;
}
// VIPER #2665 & 2667 : Check prototype of class method with body.
// 1. Return type should be same in both prototype and body.
// 2. Type of formals should be same in both prototype and body.
// 3. Number of formals should be same in both prototype and body.
static void
process_out_of_block_method(const VeriName *func_name, VeriScope *func_scope, const Array *port_list, Array *decl_list, VeriDataType *func_dtype)
{
    if (!func_name || !func_name->GetId() || !func_scope) return ; // Error, must be

    (void) port_list ; // to prevent unused argument compiler warnings
    (void) func_dtype ; // to prevent unused argument compiler warnings
    (void) decl_list ; // to prevent unused argument compiler warnings

    // For out-of-block method, method identifier should already have a body (prototype)
    VeriIdDef *func_id = func_name->GetId() ;
    VeriModuleItem *body = (func_id) ? func_id->GetModuleItem(): 0 ;
    if (!body) return ; // Not a out-of-block function
    // Get scope of prototype
    // VIPER #7410 : Upper scope of out-of-block function is already set in 'create_method_id'
    //VeriScope *prototype_scope = body->GetScope() ;

    // Get class scope where prototype is defined
    //VeriIdDef *class_id = (prototype_scope) ? prototype_scope->GetClass() : 0 ;
    //VeriScope *class_scope = (class_id) ? class_id->LocalScope() : 0 ;

    // Set the upper scope of this out-of-block function to class scope
    //func_scope->SetUpper(class_scope) ;

    // Check the return type between prototype and full definition :
    VeriDataType *proto_ret_type = func_id->GetDataType() ;
    VeriTypeInfo *p_type = (proto_ret_type) ? proto_ret_type->CreateType(0) : 0 ;
    VeriTypeInfo *f_type = (func_dtype) ? func_dtype->CreateType(0) : 0 ;

    // VIPER #4103 : Check types if both exists. One type can be null because of
    // unresolved name type :
    //if ((p_type && !f_type) || (!p_type && f_type) || (p_type && f_type && !f_type->IsEquivalent(p_type))) {
    if (p_type && f_type && !f_type->IsEquivalent(p_type, 0, 0, 0)) {
        func_name->Error("return type of extern method %s does not match prototype", func_id->Name()) ;
    }
    func_id->ClearDataType() ; // Clear old data type, will be set in body

    delete p_type ;
    delete f_type ;

    // Now check the formals of prototype with body :
    // Get ports of from prototype
    Array *proto_ports = func_id->GetPorts() ;

    // Calculate port count of prototype
    unsigned proto_port_count = (proto_ports) ? proto_ports->Size() : 0 ;

    // Viper 5509. Added the decl list to function arguments. so that
    // if port_list is null then do accumulate ports from the moduleitems
    // Accumulate the ports
    Array ports ;

    unsigned i ;
    if (port_list && port_list->Size()) {
        VeriExpression *port_decl ;
        FOREACH_ARRAY_ITEM(port_list, i, port_decl) {
            if (!port_decl) continue ;
            port_decl->AccumulatePorts(ports) ;
        }
    } else {
        VeriModuleItem *decl_item ;
        FOREACH_ARRAY_ITEM(decl_list, i, decl_item) {
            if (!decl_item) continue ;
            decl_item->AccumulatePorts(ports) ;
        }
    }

    unsigned j;
    VeriIdDef *port ;
    VeriIdDef *proto_port ;
    unsigned body_port_count = 0 ;
    // Iterate over the port list of the body :
    FOREACH_ARRAY_ITEM (&ports, j, port) {
        if (!port) continue ;
        body_port_count++ ;
        // Try to get corresponding port from prototype
        proto_port = (proto_ports && proto_ports->Size() >= body_port_count) ? (VeriIdDef*)proto_ports->At(body_port_count - 1) : 0 ;
        if (!proto_port) continue ;

        // Compare their names :
        if (!Strings::compare(port->Name(), proto_port->Name())) {
            port->Error("formal name of extern method %s does not match prototype", func_id->Name()) ;
        }
        // Compare their types
        VeriTypeInfo *port_type = port->CreateType(0) ;
        VeriTypeInfo *proto_port_type = proto_port->CreateType(0) ;
        if (port_type && !port_type->IsUnresolvedNameType() && !port_type->IsTypeParameterType() &&
                proto_port_type && !proto_port_type->IsUnresolvedNameType() &&
                !proto_port_type->IsTypeParameterType() && !proto_port_type->IsEquivalent(port_type, 0, 0, 0)) {
            port->Error("formal type of extern method %s does not match prototype", func_id->Name()) ;
        }
        delete port_type ;
        delete proto_port_type ;
        // VIPER #7973 : IEEE 1800-2012 section 8.24 says 'A default argument
        // value specified in prototype may be omitted in the out of block
        // declaration. If a default argument value is specified in out-of-block
        // declaration, then there shall be a syntactically identical default
        // argument value specified in the prototype.
        VeriExpression *proto_port_init_val = proto_port->GetInitialValue() ;
        VeriExpression *port_init_val = port->GetInitialValue() ;

        if (proto_port_init_val && !port_init_val) { // Legal
            // Copy initial value from prototype and set that to port
            VeriMapForCopy old2new ;
            port->SetInitialValue(proto_port_init_val->CopyExpression(old2new)) ;
        } else if (!proto_port_init_val && port_init_val) { // Error
            port->Error("default value for %s is not allowed in extern method body when not specified in class", port->Name()) ;
        } else if (proto_port_init_val && port_init_val) { // Both should be same
            std::ostringstream os1 ; // Dynamically allocated string buffer
            proto_port_init_val->PrettyPrint(os1, 0) ;
            char *proto_init_str = Strings::save(os1.str().c_str()) ;
            std::ostringstream os2 ; // Dynamically allocated string buffer
            port_init_val->PrettyPrint(os2, 0) ;
            char *port_init_str = Strings::save(os2.str().c_str()) ;
            if (Strings::compare(proto_init_str, port_init_str)==0) {
                port->Warning("mismatch in default argument value for %s, %s in prototype and %s in extern method body", port->Name(), proto_init_str, port_init_str) ;
            }
            Strings::free(proto_init_str) ;
            Strings::free(port_init_str) ;
        }
        // In extern method definition, initial value of formal is not allowed :
        //if (port->GetInitialValue()) {
            //port->Warning("default value of %s is not allowed in extern method definition", port->Name()) ;
        //}
        //VeriExpression *init = proto_port->TakeInitialValue() ;
        //if (init) port->SetInitialValue(init) ;
    }
    // Produce error, if prototype and body have formal count mismatch :
    if (body_port_count != proto_port_count) {
        func_name->Error("formal count of extern method %s does not match prototype", func_id->Name()) ;
    }
}

// VIPER #2475 & 2648 : Extract simple net name from complex/simple name-references
// Returns allocated string and absorbs coming parse tree
static char *
extract_net_name(const VeriName *name)
{
    if (!name) return 0 ; // Error some where
    // If 'name' is hierarchical identifier, it is error 'wire int p.x ;'
    if (name->IsHierName()) {
        name->Error("syntax error near %s", name->GetName()) ;
        //delete name ;
        return 0 ;
    }
    const char *net_name = name->GetName() ;
    char *ret_name = Strings::save(net_name) ;
    //delete name ;
    return ret_name ;
}

static VeriIdDef *
declare_type_id (VeriName *name, VeriRange *dims) // Change prototype to process rule change for 'type_identifier'
{
    // create a type-identifier from this (reference) name.
    if (!name) {
        delete dims ;
        return 0 ;
    }

    // If 'name' is hierarchical identifier, it is error 'typedef int p.x ;'
    if (name->IsHierName()) {
        name->Error("syntax error near %s", name->GetName()) ;
        delete name ;
        delete dims ;
        return 0 ;
    }

    // Check if there is an existing identifier by that name
    VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(name->GetName()) : 0 ;
    // VIPER #4977 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '2' refers that it is only used
    // to declare identifier. In id_ref rule ref count increase by 1 and in above
    // FindLocal by another '1'.  Reference count > 2 means actually referenced.
    // So do not make it explicitly defined when ref count = 2
    // VIPER #5912 : We parse identifier to be declared as reference and resolve
    // reference using 'Find'. Reference count '1' means in id_ref rule for this
    // decl_id ref count increase by 1, but in FindLocal it is not increased as
    // import construct is in upper scope :
    unsigned ref_count = VeriNode::GetReferenceCount(exist ? exist : name->GetId()) ;
    if ((exist && (ref_count == 2)) || (!exist && (ref_count == 1))) {
        if (present_scope) (void) present_scope->UndeclareExplicitImport(exist ? exist : name->GetId()) ;
        exist = 0 ;
    }
    if (exist) {
        // Legal if exist is a typedef (and not a type parameter)
        if (!exist->IsType() || exist->IsParam()) {
            // The id declared as type was originally declared but not as typedef:
            name->Error("%s is already declared", name->GetName()) ;
            exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        }
        delete name ;
        // Set the unpacked dimension to this id (we have always declared it as memory id for forward decl)
        if (dims) exist->SetMemoryRange(dims) ;
        return exist ;
    }

    // 'name' is simple name (we checked it already above), create iddef
    char *id_name = Strings::save(name->GetName()) ; // Save simple name

    // Always use a memory-id for type declarations, because it might be declared with unpacked
    // dimensions later if it is a forward declaration. Don't try to save one field by going to a VeriVariable.
    VeriIdDef *id = new VeriTypeId(id_name, dims) ;

    // Set line/file info on id to the line/file info from 'name' :
    id->SetLinefile(name->Linefile()) ;

    delete name ; // Delete incoming name

    // Declare the new one here, and return it.
    if (present_scope) (void) present_scope->Declare(id) ;
    return id ;
}

static VeriIdDef *
declare_clocking_id(char *name)
{
    if (!name) return 0 ;
    // Check if there is an existing identifier by that name
    VeriIdDef *exist = (present_scope) ? present_scope->FindLocal(name) : 0 ;
    if (exist) {
        // Legal if exist is a clocking identifier
        if (!exist->IsClocking()) {
            // The id declared as clocking was originally declared but not as clocking:
            VeriTreeNode node ;
            node.Error("%s is already declared", name) ;
            exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        }
        Strings::free(name) ;
        return exist ;
    }

    // Create clocking identifier
    VeriIdDef *id = new VeriClockingId(name) ;

    // Declare the new one here, and return it.
    if (present_scope) (void) present_scope->Declare(id) ;
    return id ;
}

static VeriIdDef *
declare_forced (VeriIdDef *id)
{
    if (!id) return 0 ;

    // Some identifiers always need to be declared in a scope.
    // Example is nested module id's : they are already part of a tree,
    // and are declared after the module is parsed.
    // Do a check and then report error if existing. But always let the identifier through.
    // Either way, we let the id go through.
    // CHECK ME : This could be the default way of handling identifiers
    // (rather than declare_id, which deletes an id if there is a previous one declared).

    // Try to declare in the scope :
    // Allow both extern module and actual module definition in scope.
    VeriIdDef *exist = 0 ; //(present_scope) ? present_scope->FindLocal(id->Name()) : 0 ;
    if (present_scope && !present_scope->Declare(id, 1/*force_insert*/, &exist)) {
        VeriModule *exist_mod = (exist) ? exist->GetModule(): 0 ;
        // It went in, but it was previously declared.
        if (!id->Name()) {
            // unnamed identifier. OK to have collisions.
        } else if (!(exist_mod && exist_mod->GetQualifier(VERI_EXTERN))){
            // Do not produce error if existing one is extern module
            id->Error("%s is already declared", id->Name()) ;
            if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        }
    }
    return id ; // always return previous identifier.
}

static VeriIdDef *
declare_operator(VeriIdDef *id)
{
    if (!id) return 0 ;

    // overloaded operator ids created by operator binding construct
    // always get declared and stored in scope without checking (force-insert).

    // Try to declare in the scope :
    if (present_scope) (void) present_scope->Declare(id, 1/*force_insert*/) ;
    return id ; // always return incoming identifier.
}
static VeriIdDef *
declare_enum_id(VeriIdDef *id)
{
    if (!id) return 0 ;

    // VIPER #5357: If runtime flag 'veri_declare_enums_in_struct_union_scope' is
    // set or relaxed-checking mode is set, declare enum in present_scope.
    // Otherwise declare enums in non-struct/union scope traversing upwards
    // from present_scope
    if (RuntimeFlags::GetVar("veri_declare_enums_in_struct_union_scope") ||
         VeriNode::InRelaxedCheckingMode()) {
        return declare_id(id) ;
    }
    VeriScope *scope = present_scope ;
    while (scope) {
        if (scope->IsStructUnionScope()) {
            scope = scope->Upper() ;
        } else {
            break ;
        }
    }

    VeriIdDef *exist = 0 ;
    if (scope && !scope->Declare(id, 0 /* no force insert */, &exist)) {
        id->Error("%s is already declared", id->Name()) ;
        if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        delete id ;
        return 0 ;
    }
    return id ;
}

static Array *
declare_enum_ids(const VeriName *name, const VeriRange *range, const VeriExpression *init_val)
{
    if (!name) return 0 ;

    Array *ids = new Array(1) ;

    if (range) { // Range is specified, so multiple enum literals are to be created
        VeriExpression *left = range->GetLeft() ;
        VeriExpression *right = range->GetRight() ;
        int lsb = 0, msb = 0 ;
        if (!right) { // Range specified as [N] : Generates name0 to nameN-1
            msb = 0 ;
            lsb = left ? (left->Integer() - 1) : 0 ;
        } else { // Range is [N:M] : Genartes nameN to nameM
            msb = left ? left->Integer() : 0 ;
            lsb = right ? right->Integer() : 0 ;
        }
        // VIPER #2623: The msb of the range should not be a negative number:
        // We produce warning messages but other standard tools produce errors.
        // We can always change the warning to error any time, if required.
        if (msb < 0) {
            range->Warning("enumeration range must be a non-negative integer value") ;
            msb = 0 ; // Set the negative msb to 0 and continue:
        }
        // Check lsb of the range also for negative value:
        if (lsb < 0) {
            range->Warning("enumeration range must be a non-negative integer value") ;
            lsb = 0 ; // Set the negative lsb to 0 and continue:
        }

        // VIPER #2623: Issue an warning message if the bound(s) are real (not integer):
        if (left && left->IsReal()) range->Warning("enumeration range must be a non-negative integer value") ;
        if (right && right->IsReal()) range->Warning("enumeration range must be a non-negative integer value") ;

        // Now create literals, declare those and insert those in array
        int i = 0 ;
        for (i = msb; (msb > lsb) ? (i >= lsb) : (i <= lsb); (msb > lsb) ? i--: i++) {
            char *idx = Strings::itoa(i) ;
            char *enum_name = Strings::save(name->GetName(), idx) ;
            Strings::free(idx) ;
            VeriIdDef *enum_id = new VeriParamId(enum_name) ;
            enum_id->SetLinefile(name->Linefile()) ;
            enum_id = declare_enum_id(enum_id) ;
            // VIPER #2623: 'enum_id' may be 0 (zero) if it is already declared
            if (enum_id) {
                if ((i == msb) && init_val) {
                    VeriMapForCopy id_map_table ;
                    enum_id->SetInitialValue(init_val->CopyExpression(id_map_table)) ;
                    init_val = 0 ;
                }
                ids->InsertLast(enum_id) ;
            }
        }
    } else {
        // If enum literal is declared without specifying range, create one enum literal,
        // set initial value to it (if exists)
        VeriIdDef *id = new VeriParamId(Strings::save(name->GetName())) ;
        id->SetLinefile(name->Linefile()) ;
        id = declare_enum_id(id) ;
        // VIPER #2623: 'enum_id' may be 0 (zero) if it is already declared
        if (id) {
            if (init_val) {
                VeriMapForCopy id_map_table ;
                id->SetInitialValue(init_val->CopyExpression(id_map_table)) ; // Set initial value
            }
            ids->InsertLast(id) ;
        }
    }
    return ids ;
}

// VIPER #7389 (baseline part): Create Array of VeriExpressions for enum declarations.
// Then declare all enum ids at once from that Array and delete the Array of expressions.
static Array *
declare_all_enum_ids(Array *enum_exprs)
{
    Array *enum_ids = 0 ;
    Array *tmp_ids ;
    VeriExpression *init_val ;
    VeriExpression *name ;

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(enum_exprs, i, expr) {
        if (!expr) continue ;

        init_val = 0 ;
        name = expr ;
        if (expr->OperType()==VERI_EQUAL_ASSIGN) {
            // This has a initial value:
            init_val = expr->GetRight() ;
            name = expr->GetLeft() ;
        }
        VeriExpression *idx = name->GetIndexExpr() ;
        tmp_ids = declare_enum_ids(name->NameCast(), ((idx)?idx->RangeCast():0), init_val) ;
        if (!enum_ids) {
            enum_ids = tmp_ids ;
        } else {
            enum_ids->Append(tmp_ids) ;
            delete tmp_ids ;
        }
        delete expr ;
    }
    delete enum_exprs ;

    return enum_ids ;
}

static VeriIdDef *
declare_loop_var_id(VeriExpression *name)
{
    // create and declare a loop variable identifier from this (reference) name.
    if (!name) return 0 ; // no identifier declared

    // If 'name' is not a simple id-ref', this is an error
    if (!name->IsIdRef() || !name->GetName()) {
        name->Error("expected simple identifier as loop variable") ;
        delete name ;
        return 0 ;
    }

    // Create the identifier :
    char *loop_var_name = Strings::save(name->GetName()) ;
    VeriIdDef *id = new VeriVariable(loop_var_name) ;
    id->SetLinefile(name->Linefile()) ;

    // delete the incoming expression :
    delete name ;

    // Now declare this identifier :
    return declare_id(id) ;
}

// Routines for extern module handling : Vipers #2560, #3285, #4474, #4475, #4768
// #4771, #4772, #4689 and #4808

// Decide which parameters should go with actual module definition:
// 1. If no extern module exists for 'mod_id', return parameter list of actual module
// 2. If .* is not specified in port list of actual module definition, return
//    parameter list of actual module definition
// 3. If parameter port list exists in extern module, but not in actual definition
//    return parameter list of extern module.
// 4. If parameter port list exists in actual module definition, use that
static Array *
get_param_list(const VeriIdDef *mod_id, Array *param_list, const Array *port_list)
{
    if (!mod_id) return 0 ;
    (void) port_list ;
    VeriModule *extern_mod = 0 ;
    extern_mod = mod_id->GetExternModule() ;
    // If extern module does not exists for 'mod_id', return current parameter list
    if (!extern_mod) return param_list ;

    // Check whether .* exists in port list
    unsigned dot_star_in_port_list = 0 ;
    unsigned i ;
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(port_list, i, port_expr) {
        if (port_expr && port_expr->IsDotStar()) {
            dot_star_in_port_list = 1 ;
            break ;
        }
    }
    if (!dot_star_in_port_list) return param_list ; // FIXME : Need to check port name/type with extern module
    Array *param_connects = extern_mod->GetParameterConnects() ;

    // If parameter list exists in extern module, but not in actual definition, return
    // parameter connects from extern module
    // VIPER #8261 : Always take parameter port list from extern module
    if (param_connects && param_connects->Size()) { // && (!param_list || !param_list->Size())) {
        VeriDataDecl *decl ;
        FOREACH_ARRAY_ITEM(param_list, i, decl) delete decl ;
        delete param_list ;
        extern_mod->ClearParameterConnects() ;
        // FIXME: Need to set param list null in extern module
        return param_connects ;
    }
    return param_list ;
}
// If extern module not exists, return import list of actual definition
// If package import list exists in extern module, but not in actual definition
// return package import list from extern module
static Array *
get_package_import_list(const VeriIdDef *mod_id, Array *package_import_list)
{
    if (!mod_id) return 0 ;
    VeriModule *extern_mod = 0 ;
    extern_mod = mod_id->GetExternModule() ;
    // If extern module does not exists for 'mod_id', return current parameter list
    if (!extern_mod) return package_import_list ;

    Array *extern_package_import_list = extern_mod->GetPackageImportDecls() ;

    // If package import list exists in extern module, but not in actual definition, return
    // package import list from extern module
    if (extern_package_import_list && extern_package_import_list->Size() && (!package_import_list || !package_import_list->Size())) {
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(package_import_list, i, item) delete item ;
        extern_mod->ClearPackageImportList() ;
        delete package_import_list ;
        return extern_package_import_list ;
    }
    return package_import_list ;
}

static Array *
get_port_list(const VeriIdDef *mod_id, Array *port_list)
{
    (void) mod_id ;
    VeriModule *extern_mod = (mod_id) ? mod_id->GetExternModule(): 0 ;
    // If extern module does not exists for 'mod_id', return current parameter list
    if (!extern_mod) return port_list ;

    Array *port_connects = extern_mod->GetPortConnects() ;

    // If parameter list exists in extern module, but not in actual definition, return
    // parameter connects from extern module
    if (port_connects && port_connects->Size()) {
        unsigned i ;
        VeriExpression *decl ;
        unsigned is_dot_star = 0 ;
        FOREACH_ARRAY_ITEM(port_list, i, decl) {
            if (decl && decl->IsDotStar()) {
                is_dot_star = 1 ;
            }
        }
        if (is_dot_star || !port_list || !port_list->Size()) {
            FOREACH_ARRAY_ITEM(port_list, i, decl) delete decl ;
            delete port_list ; // VIPER #6003 (test1.v): Leak fix
            extern_mod->ClearPortConnects() ;
            return port_connects ;
        }
    }
    return port_list ;
}
static void
extract_scope_info_from_extern_mod(const VeriIdDef *mod_id, Array *param_list, const Array *port_list)
{
    (void) mod_id ;
    (void) param_list ;
    (void) port_list ;
    // Get scope of this module
    VeriScope *mod_scope = mod_id ? mod_id->LocalScope() : 0 ;
    VeriModule *extern_mod = mod_id ? mod_id->GetExternModule(): 0 ;

    VeriScope *extern_mod_scope = (extern_mod) ? extern_mod->GetScope(): 0 ;
    //if (!extern_mod || !extern_mod_scope) return ;

    // Check whether .* exists in port list
    unsigned dot_star_in_port_list = 0 ;
    unsigned i ;
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(port_list, i, port_expr) {
        if (port_expr && port_expr->IsDotStar()) {
            dot_star_in_port_list = 1 ;
            // VIPER #5299 : Produce error if .* port exists without extern declaration
            if (!extern_mod) {
                if (mod_id) mod_id->Error(".* in port not allowed without declaration of extern %s", mod_id->Name()) ;
            }
            break ;
        }
    }
    if (!dot_star_in_port_list || !extern_mod || !extern_mod_scope) {
        return ; // FIXME : Need to check port name/type with extern module
    }
    // VIPER #8261 : Parameter port list is not allowed with .* in port list in module decl
    if (dot_star_in_port_list && param_list && param_list->Size()) {
        if (mod_id) mod_id->Error("parameter port list is not allowed with .* in port list") ;
        // Delete the parameter declarations from actual module
        unsigned j ;
        VeriDataDecl *decl ;
        FOREACH_ARRAY_ITEM(param_list, i, decl) {
            Array *ids = decl ? decl->GetIds(): 0 ;
            if (!ids) {
                delete decl ; continue ;
            }
            Array stored_ids(*ids) ;
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id) continue ;
                VeriIdDef *exist = extern_mod_scope->FindLocal(id->Name()) ;
                if (exist) {
                    id->Error("%s is already declared", id->Name()) ;
                    if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ;
                }
                (void) mod_scope->Undeclare(id) ;
            }
            delete decl ;
            FOREACH_ARRAY_ITEM(&stored_ids, j, id) delete id ;
        }
        param_list->Reset() ;
    }

    // Undeclare identifiers declared inside extern module and declare those
    // in actual module scope
    Map *id_map = extern_mod_scope->GetThisScope() ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(id_map, mi, 0, &id) {
        if (!id) continue ;
        // Take parameter only when actual definition does not have any parameter
        // VIPER #8261 : Always use parameters from extern module
        //if (id->IsParam() && param_list && param_list->Size()) continue ;

        (void) extern_mod_scope->Undeclare(id) ;
        VeriIdDef *exist = 0 ;
        if (!mod_scope->Declare(id, 0 /* no force insert */, &exist)) {
            id->Error("%s is already declared", id->Name()) ;
            if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        }
    }
}

static VeriExpression *
create_port_connect(char *name, VeriExpression *expr)
{
#ifdef VERILOG_USE_NAME_EXTENDED_EXPRESSION
    // VIPER #5599: Add null check for expr otherwise we may crash after some syntax errors:
    if (!port_connect_has_attribute && expr && !expr->GetComments() && !expr->GetAttributes()) return expr->GetNameExtendedExpression(name) ;
#endif
    port_connect_has_attribute = 0 ;
    return new VeriPortConnect(name, expr) ;
}

// VIPER #3384 and VIPER #2881: Declare objects of type VeriStructUnion for 'option' and 'type_option'
// in covergroup, coverpoint and covercross.
// FIXME: The 'type_options' are static in nature (ref LRM IEEE Std 1800, 18.6.1) and cannot be modified
// per via an instance of covergroup.
static void
create_data_decl_for_covergroup(unsigned type, const char *str, Array *arr)
{
    if (!arr) return ;
    arr->Insert(new VeriDataDecl(VERI_STRUCT, new VeriDataType(type,0,0), declare_id(new VeriVariable(Strings::save(str))))) ; // bit per_instance
}
static void
create_point_cross_options()
{
    VeriIdDef *cross_type_option = declare_id(new VeriCovgOptionId(Strings::save("type_option"))) ;
    push_scope(0) ; // Create new scope
    Array *struct_union_member_list = new Array() ;

    create_data_decl_for_covergroup(VERI_INT, "weight", struct_union_member_list) ; // int weight

    create_data_decl_for_covergroup(VERI_INT, "goal", struct_union_member_list) ; // int goal

    create_data_decl_for_covergroup(VERI_STRINGTYPE, "comment", struct_union_member_list) ; // string comment

    VeriDataType *struct_type = new VeriStructUnion(VERI_STRUCT,0,0,struct_union_member_list,pop_scope()) ;
    if (cross_type_option) cross_type_option->SetDataType(struct_type) ;
    else delete struct_type ; // if not using it, don't want to leak it
}
static void
create_covergroup_options()
{
    // Create a structure which contains all the attributes for covergroup options
    VeriIdDef *cg_option = declare_id(new VeriCovgOptionId(Strings::save("option"))) ;

    push_scope(0) ; // push the scope
    Array *struct_union_member_list = new Array() ;

    create_data_decl_for_covergroup(VERI_STRINGTYPE, "name", struct_union_member_list) ; // string name

    create_data_decl_for_covergroup(VERI_INT, "weight", struct_union_member_list) ; // int weight

    create_data_decl_for_covergroup(VERI_INT, "goal", struct_union_member_list) ; // int goal

    create_data_decl_for_covergroup(VERI_STRINGTYPE, "comment", struct_union_member_list) ; // string comment

    create_data_decl_for_covergroup(VERI_INT, "at_least", struct_union_member_list) ; // int at_least

    create_data_decl_for_covergroup(VERI_INT, "auto_bin_max", struct_union_member_list) ; // int auto_bin_max

    create_data_decl_for_covergroup(VERI_INT, "cross_num_print_missing", struct_union_member_list) ; // int cross_num_print_missing

    create_data_decl_for_covergroup(VERI_BIT, "detect_overlap", struct_union_member_list) ; // int detect_overlap

    create_data_decl_for_covergroup(VERI_BIT, "per_instance", struct_union_member_list) ; // int per_instance

    VeriStructUnion *struct_type = new VeriStructUnion(VERI_STRUCT,0,0,struct_union_member_list,pop_scope()) ;
    if (cg_option) cg_option->SetDataType(struct_type) ;
    else delete struct_type ; // if not using it, don't want to leak it
    //VeriDataDecl *data_decl = new VeriDataDecl(VERI_STRUCT, struct_type, cg_option) ;

    // Now create another structure for type_options
    VeriIdDef *cg_type_option = declare_id(new VeriCovgOptionId(Strings::save("type_option"))) ;
    push_scope(0) ; // Create new scope
    struct_union_member_list = new Array() ;

    create_data_decl_for_covergroup(VERI_INT, "weight", struct_union_member_list) ; // int weight

    create_data_decl_for_covergroup(VERI_INT, "goal", struct_union_member_list) ; // int goal

    create_data_decl_for_covergroup(VERI_STRINGTYPE, "comment", struct_union_member_list) ; // string comment

    create_data_decl_for_covergroup(VERI_BIT, "strobe", struct_union_member_list) ; // int per_instance

    VeriStructUnion *struct_type1 = new VeriStructUnion(VERI_STRUCT,0,0,struct_union_member_list,pop_scope()) ;
    if (cg_type_option) cg_type_option->SetDataType(struct_type1) ;
    else delete struct_type1 ; // if not using it, don't want to leak it
}
void
create_coverpoint_options()
{
    // Create a structure which contains all the attributes for covergroup options
    VeriIdDef *point_option = declare_id(new VeriCovgOptionId(Strings::save("option"))) ;

    push_scope(0) ; // push the scope
    Array *struct_union_member_list = new Array() ;

    create_data_decl_for_covergroup(VERI_INT, "weight", struct_union_member_list) ; // int weight

    create_data_decl_for_covergroup(VERI_INT, "goal", struct_union_member_list) ; // int goal

    create_data_decl_for_covergroup(VERI_STRINGTYPE, "comment", struct_union_member_list) ; // string comment

    create_data_decl_for_covergroup(VERI_INT, "at_least", struct_union_member_list) ; // int at_least

    create_data_decl_for_covergroup(VERI_INT, "auto_bin_max", struct_union_member_list) ; // int auto_bin_max

    create_data_decl_for_covergroup(VERI_BIT, "detect_overlap", struct_union_member_list) ; // int detect_overlap

    VeriStructUnion *struct_type = new VeriStructUnion(VERI_STRUCT,0,0,struct_union_member_list,pop_scope()) ;
    if (point_option) point_option->SetDataType(struct_type) ;
    else delete struct_type ;
    //VeriDataDecl *data_decl = new VeriDataDecl(VERI_STRUCT, struct_type, cg_option) ;

    // Now create another structure for type_options
    create_point_cross_options() ;
}
static void
create_covercross_options()
{
    // Create a structure which contains all the attributes for covergroup options
    VeriIdDef *cross_option = declare_id(new VeriCovgOptionId(Strings::save("option"))) ;

    push_scope(0) ; // push the scope
    Array *struct_union_member_list = new Array() ;

    create_data_decl_for_covergroup(VERI_INT, "weight", struct_union_member_list) ; // int weight

    create_data_decl_for_covergroup(VERI_INT, "goal", struct_union_member_list) ; // int goal

    // VIPER #4664 : Type of 'comment' will be VERI_STRINGTYPE instead of VERI_STRING
    create_data_decl_for_covergroup(VERI_STRINGTYPE, "comment", struct_union_member_list) ; // string comment

    create_data_decl_for_covergroup(VERI_INT, "at_least", struct_union_member_list) ; // int at_least

    create_data_decl_for_covergroup(VERI_INT, "cross_num_print_missing", struct_union_member_list) ; // int cross_num_print_missing

    VeriStructUnion *struct_type = new VeriStructUnion(VERI_STRUCT,0,0,struct_union_member_list,pop_scope()) ;
    if (cross_option) cross_option->SetDataType(struct_type) ;
    else delete struct_type ; // if not using it, don't want to leak it
    //VeriDataDecl *data_decl = new VeriDataDecl(VERI_STRUCT, struct_type, cg_option) ;

    // Now create another structure for type_options
    create_point_cross_options() ;
}

static void
insert_attr_into_map(Map *attr_list, char *key, VeriExpression *val)
{
    if (!attr_list || !key) {
        Strings::free(key) ;
        delete val ;
        return ;
    }

    // Check whether it is already in the attribute list Map:
    MapItem *existing_attr = attr_list->GetItem(key) ;
    char *old_key = (existing_attr) ? (char *)existing_attr->Key() : 0 ;
    VeriExpression *old_val = (existing_attr) ? (VeriExpression *)existing_attr->Value() : 0 ;

    // VIPER #4280: Insert the new key/value pair by overwriting the previous one:
    (void) attr_list->Insert(key, val, 1 /* force overwrite */) ;

    // And delete the previous key/value:
    Strings::free(old_key) ;
    delete old_val ;
}

// VIPER #6113: Update the ending linefile of the given object with linefile of current rule.
void update_end_linefile(VeriTreeNode *node)
{
    linefile_type lf = (node) ? node->Linefile() : 0 ;
    if (!lf) return ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    lf->SetRightCol(last_column) ;
    lf->SetRightLine(last_line) ;
#else
#ifndef VERILOG_USE_STARTING_LINEFILE_INFO
    lf = LineFile::SetLineNo(lf, last_line) ;
#endif
#endif
}

// VIPER #6952: Update the starting linefile of the given object with linefile of current rule.
void update_start_linefile(VeriTreeNode *node, YYLTYPE *from_lf=0)
{
    linefile_type lf = (node) ? node->Linefile() : 0 ;
    if (!lf) return ;

    unsigned left_line = (from_lf) ? from_lf->first_line : first_line ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    lf->SetLeftCol((from_lf) ? from_lf->first_column : first_column) ;
    lf->SetLeftLine(left_line) ;
#else
#ifdef VERILOG_USE_STARTING_LINEFILE_INFO
    lf = LineFile::SetLineNo(lf, left_line) ;
#else
    (void) left_line ; // Avoid compiler warning: unused variable
#endif
#endif
}

// VIPER #4842: Check for multiple clocking id in same scope:
static unsigned
check_default_clocking(const VeriModuleItem *new_default_clocking)
{
    VERIFIC_ASSERT(new_default_clocking) ;

    // Create the Map if it does not already exist:
    if (!default_clockings) default_clockings = new Map(POINTER_HASH) ;

    // Get any previously declared default clocking in this scope:
    VeriModuleItem *other_default_clocking = (VeriModuleItem *)default_clockings->GetValue(present_scope) ;

    // Get the ids, if any, for both the previous and new default clocking decls:
    VeriIdDef *other_id = (other_default_clocking) ? other_default_clocking->GetId() : 0 ;
    VeriIdDef *new_id = new_default_clocking->GetId() ;

    // Now, check if the two default clockings are different (their ids are different):
    if (other_default_clocking && ((!other_id && !new_id) || (other_id != new_id))) {
        VeriTreeNode *msg_node = (new_id) ? (VeriTreeNode *)new_id : (VeriTreeNode *)other_default_clocking ;
        msg_node->Error("default clocking specified multiple times") ;
        msg_node = (other_id) ? (VeriTreeNode *)other_id : (VeriTreeNode *)other_default_clocking ;
        msg_node->Info("another default clocking is specified here") ; // VIPER #7009
        return 1 ;
    }

    // Insert this default clocking so that we can check it later if needed:
    if (present_scope) (void) default_clockings->Insert(present_scope, new_default_clocking) ;

    return 0 ;
}

static void
set_linefile_from_rule(VeriTreeNode &node, YYLTYPE linefile)
{
    linefile_type lf = node.Linefile() ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    lf->SetLeftLine(linefile.first_line) ;
    lf->SetLeftCol(linefile.first_column) ;
    lf->SetRightLine(linefile.last_line) ;
    lf->SetRightCol(linefile.last_column) ;
    lf->SetFileId(linefile.file_id) ;
#else
#ifdef VERILOG_USE_STARTING_LINEFILE_INFO
    lf = LineFile::SetLineNo(lf, linefile.first_line) ;
#else
    lf = LineFile::SetLineNo(lf, linefile.last_line) ;
#endif
    lf = LineFile::SetFileId(lf, linefile.file_id) ;
    node.SetLinefile(lf) ;
#endif
}

static void
add_pre_comments_to_node(VeriTreeNode *node, Array *comments)
{
    if (!comments) return ; // Nothing to do
    if (node) {
        node->AddComments(comments) ; // Add the comments to the node
    } else {
        VeriNode::DeleteComments(comments) ; // Delete comments to fix memory leak
    }
}

static void
add_post_comments_to_node(VeriModuleItem *node, Array *comments)
{
    if (!comments) return ; // Nothing to do
    if (node) {
        veri_file::SetComments(node->ProcessPostComments(comments)) ;
    } else {
        VeriNode::DeleteComments(comments) ; // Delete comments to fix memory leak
    }
}
// VIPER #8109 : Create iimplicit localparam declaration in for-generate
static void
create_implicit_localparam(VeriModuleItem *for_init)
{
    if (!VeriNode::IsSystemVeri() || !for_init) return ; // Do not create localparam for non-SV design
    // IEEE 1800-2012 LEM section 27.4 says 'Within the generate block of loop-gnerate
    // construct, there is an implicit localparam declaration. This is an integer
    // parameter that has the same name and type as the loop index variable'

    // Extract the genvar name from for-generate initialization
    VeriIdDef *genvar_id = for_init->GetId() ;
    if (!genvar_id) {
        Array *ids = for_init->GetIds() ;
        genvar_id = (ids && (ids->Size()==1)) ? (VeriIdDef*)ids->GetFirst(): 0 ;
    }
    if (!genvar_id) return ; // Cannot find genvar id

    VeriIdDef *id = declare_id(new VeriParamId(Strings::save(genvar_id->GetName()))) ;
    if (!id) return ;
    id->SetParamType(VERI_LOCALPARAM) ;
    id->SetImplicitLocalParam(1) ;

    // Initial value of this localparam will be reference of genvar-id
    VeriIdRef *init_val = new VeriIdRef(genvar_id) ;
    init_val->SetLinefile(id->Linefile()) ;
    id->SetInitialValue(init_val) ;
}
// VIPER #8303 : Label statement creates a named begin end block
static void process_labeled_stmt(VeriIdDef *label_id, VeriModuleItem *stmt)
{
    if (!label_id) return ;

    VeriScope *label_scope = label_id->GetScope() ;
    if (!label_scope) return ; // Label has no scope, no need to do anything
    pop_scope() ; // Pop scope of label
    VeriScope *stmt_scope = stmt ? stmt->GetScope(): 0 ;
    if (!stmt_scope || (stmt_scope==label_scope)) {
        // Label is applied to a statement having no scope.
        label_scope->ResetEmptyScope() ;
        return ; // Scope of label will be scope of non-scoped statement
    }
    // Statement has a scope
    if (label_scope->IsEmptyScope()) {
        // Nothing is declared in label-scope. So we can keep only the scope of
        // statement.
        VeriScope *parent = label_scope->Upper() ;
        if (parent) parent->RemoveChildScope(label_scope) ;
        stmt_scope->SetUpper(parent) ;
        label_id->SetScope(0) ;
    } else {
        // Something is declared in label scope. So we can keep both scopes alive
    }
}

// VIPER #8003: foreach loop variable syntax:
#define GET_DIMENSION_RANGE(dim, ran) \
if (ran.array) { \
     VeriTreeNode tmp_node ; \
     tmp_node.Error("syntax error near %s", "[") ; \
     delete ran.array ; ran.array = 0 ; \
} \
dim = ran.range ; \

%}

// Since 9/2004, we let flex and bison always work together to process location
// information (column/line/file info) of parsed tokens. (see comments elsewhere in this file)
%locations

%union {
    char        *str ;
    unsigned     unsigned_number ;
    double       real_number ;
    char         character ;

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

    VN::VeriTreeNode            *tree_node ; // General node

    VN::VeriModule              *module ;
    VN::VeriExpression          *expr ;
    VN::VeriName                *name ;
    VN::VeriDataType            *data_type ;
    VN::VeriStatement           *stmt ;
    VN::VeriModuleItem          *module_item ;
    VN::VeriTable               *table ;
    VN::VeriIdDef               *id_def ;
    VN::Array                   *array ;
    VN::Map                     *map ;
    VN::VeriRange               *range ;
    VN::VeriNetRegAssign        *net_reg_assign ;
    VN::VeriDefParamAssign      *defparam_assign ;
    VN::VeriInstId              *inst ;
    VN::VeriStrength            *strength ;
    VN::VeriPath                *path ;
    VN::VeriStatement           *statement ;
    VN::VeriDelayOrEventControl *delay_or_event_control ;
    VN::VeriScope               *scope ;
    VN::VeriConfigRule          *config ;
    VN::VeriUseClause           *use_clause ;
    VN::VeriClockingDirection   *clocking_dir ;
    VN::VeriProductionItem      *production_item ;
    struct {VN::VeriName *name; VN::Array *array; }    name_array_struct ; // structure with two pointers
    struct dim_struct {VN::VeriRange *range; VN::Array *array; }    range_array_struct ; // structure with two pointers
}

%start source_text

/***************************************************************/
/* All Tokens                                                  */
/***************************************************************/

%token
 /* dummy internal token for expression parsing*/
 VERI_BEGIN_EXPR
 VERI_OPAREN       "("
 VERI_CPAREN       ")"
 VERI_MODULUS
 VERI_PLUS
 VERI_MIN
 VERI_MUL          "*" // VIPER #2074: avoid syntax error, use "*" also
 VERI_DIV
 VERI_GT
 VERI_LT
 VERI_GEQ
 VERI_LEQ
 VERI_CASEEQ
 VERI_CASENEQ
 VERI_LOGEQ
 VERI_LOGNEQ
 VERI_LOGNOT        "!"
 VERI_LOGAND        "&&"
 VERI_LOGOR         "||"
 VERI_REDNOT
 VERI_REDAND        "&"
 VERI_REDOR         "|"
 VERI_REDXOR
 VERI_REDNAND
 VERI_REDNOR
 VERI_REDXNOR

 VERI_LSHIFT
 VERI_RSHIFT
 VERI_QUESTION      "?"
 VERI_COLON         ":"
 VERI_ALLPATH
 VERI_LEADTO
 VERI_NBASSIGN
 VERI_RIGHTARROW    /* "->" */

 VERI_ALWAYS
 VERI_AND
 VERI_ASSIGN
 VERI_BEGIN
 VERI_BUF
 VERI_BUFIF0
 VERI_BUFIF1
 VERI_CASE
 VERI_CASEX
 VERI_CASEZ
 VERI_CMOS
 VERI_DEASSIGN
 VERI_DEFAULT       "default"
 VERI_DEFPARAM
 VERI_DISABLE
 VERI_EDGE
 VERI_EDGEDESCRIPTOR
 VERI_ELSE
 VERI_END
 VERI_ENDCASE
 VERI_ENDFUNCTION
 VERI_ENDMODULE
 VERI_ENDPRIMITIVE
 VERI_ENDSPECIFY
 VERI_ENDTABLE
 VERI_ENDTASK
 VERI_EVENT
 VERI_FOR
 VERI_FORCE
 VERI_FOREVER
 VERI_FORK
 VERI_FUNCTION
 VERI_HIGHZ0
 VERI_HIGHZ1
 VERI_IF
 VERI_IFNONE
 VERI_INITIAL
 VERI_INOUT
 VERI_INPUT
 VERI_INTEGER
 VERI_JOIN
 VERI_LARGE
 VERI_MACROMODULE
 VERI_MEDIUM
 VERI_MODULE
 VERI_NAND
 VERI_NEGEDGE
 VERI_NMOS
 VERI_NONE // 'none' wiretype
 VERI_NOR
 VERI_NOT
 VERI_NOTIF0
 VERI_NOTIF1
 VERI_OR
 VERI_OUTPUT
 VERI_PARAMETER
 VERI_PMOS
 VERI_POSEDGE
 VERI_PRIMITIVE
 VERI_PULL0
 VERI_PULL1
 VERI_PULLDOWN
 VERI_PULLUP
 VERI_RCMOS
 VERI_REAL
 VERI_REALTIME
 VERI_REG
 VERI_RELEASE
 VERI_REPEAT
 VERI_RNMOS
 VERI_RPMOS
 VERI_RTRAN
 VERI_RTRANIF0
 VERI_RTRANIF1
 VERI_SCALARED
 VERI_SMALL
 VERI_SPECIFY
 VERI_SPECPARAM
 VERI_STRONG0
 VERI_STRONG1
 VERI_SUPPLY0
 VERI_SUPPLY1
 VERI_TABLE
 VERI_TASK
 VERI_TIME
 VERI_TRAN
 VERI_TRANIF0
 VERI_TRANIF1
 VERI_TRI
 VERI_TRI0
 VERI_TRI1
 VERI_TRIAND
 VERI_TRIOR
 VERI_TRIREG
 VERI_VECTORED
 VERI_WAIT
 VERI_WAND
 VERI_WEAK0
 VERI_WEAK1
 VERI_WHILE
 VERI_WIRE
 VERI_WOR
 VERI_XNOR
 VERI_XOR

 VERI_ID
 VERI_NUM
 VERI_BASE
 VERI_BASED_NUM
 VERI_SIZED_BASED_NUM
 VERI_REAL_NUM
 VERI_STRING

 VERI_SYSTEM_TASK
 VERI_OCUR            "{"
 VERI_CCUR            "}"
 VERI_OBRACK          "["
 VERI_CBRACK          "]"
 VERI_SEMI            ";"
 VERI_DOT             "."
 VERI_COMMA           ","
 VERI_AT              "@"
 VERI_EQUAL           "="
 VERI_EQUAL_ASSIGN   // "=" used in assignment
 VERI_POUND           "#"
 VERI_EDGE_AMPERSAND  "&&&"

 VERI_UDP_EDGE
 VERI_UDP_WILD_EDGE
 VERI_UDP_IOSEP
 VERI_UDP_LINESEP
 VERI_UDP_VAL

 /* Verilog 2000 token additions */
 VERI_PARTSELECT_UP
 VERI_PARTSELECT_DOWN
 VERI_ARITLSHIFT
 VERI_ARITRSHIFT
 VERI_POWER
 VERI_OATTR            "(*"
 VERI_CATTR            "*)"

 /* Verilog 2000 keyword additions */
 VERI_CONFIG
 VERI_ENDCONFIG
 VERI_DESIGN
 VERI_INSTANCE
 VERI_INCDIR
 VERI_INCLUDE
 VERI_CELL
 VERI_USE
 VERI_LIBRARY
 VERI_LIBLIST
 VERI_GENERATE
 VERI_ENDGENERATE
 VERI_GENVAR
 VERI_LOCALPARAM
 VERI_SIGNED
 VERI_UNSIGNED
 VERI_AUTOMATIC
 VERI_PULSESTYLE_ONEVENT
 VERI_PULSESTYLE_ONDETECT
 VERI_SHOWCANCELLED
 VERI_NOSHOWCANCELLED
 VERI_TICK_PROTECTED

 /* extra tokens needed for multiple dialects */
 VERI_STRINGTYPE /* AMS and SV use this */
 VERI_CROSS /* AMS and SV use this */
 VERI_CAST_OP            "'"    /* AMS and SV use this */
 VERI_TYPE  /* token used for environment checking */

 VERI_UWIRE /* Verilog 2005/SV : VIPER 3829 */

 /* Verilog System 3.0 keyword additions */
 VERI_ALWAYS_COMB
 VERI_ALWAYS_FF
 VERI_ALWAYS_LATCH
 VERI_ASSERT
 VERI_ASSERT_STROBE
 VERI_BIT
 VERI_BREAK
 VERI_BYTE
 // VERI_CELL // common with V2K
 VERI_CHANGED
 VERI_CHAR
 VERI_CONST
 VERI_CONTINUE
 VERI_DO
 // VERI_ENDCONFIG // common with V2K
 VERI_ENDINTERFACE
 VERI_ENUM
 VERI_EXPORT
 VERI_EXTERN
 VERI_FORKJOIN
 VERI_RETURN
 VERI_IFF
 VERI_IMPORT
 VERI_INT
 VERI_INTERFACE
 VERI_LOGIC
 VERI_LONGINT
 VERI_LONGREAL
 VERI_MODPORT
 VERI_PACKED
 VERI_PRIORITY
 VERI_SHORTINT
 VERI_SHORTREAL
 VERI_STATIC
 VERI_STRUCT
 VERI_TIMEPRECISION
 VERI_TIMEUNIT
 // VERI_TYPE
 VERI_TYPEDEF
 VERI_UNION
 VERI_UNIQUE
 // VERI_UNSIGNED // common with V2K
 VERI_VOID
 VERI_PROCESS
 VERI_DOTSTAR
 VERI_DOLLAR_ROOT
 VERI_UNSIZED_LITERAL
 VERI_INC_OP
 VERI_DEC_OP
 VERI_PLUS_ASSIGN
 VERI_MIN_ASSIGN
 VERI_MUL_ASSIGN
 VERI_DIV_ASSIGN
 VERI_MOD_ASSIGN
 VERI_AND_ASSIGN
 VERI_OR_ASSIGN
 VERI_XOR_ASSIGN
 VERI_LSHIFT_ASSIGN
 VERI_RSHIFT_ASSIGN
 VERI_ALSHIFT_ASSIGN
 VERI_ARSHIFT_ASSIGN

 /* Verilog System 3.1 keyword additions */
 VERI_ALIAS
 VERI_BEFORE
 VERI_BIND
 VERI_CHANDLE
 VERI_CLASS
 VERI_CLOCKING
 // VERI_CAST_OP            "'"  //Common with AMS
 VERI_COLON_COLON        "::"
 VERI_COLON_EQUAL        ":="
 VERI_COLON_NEQUAL       ":/"
 VERI_CONSTRAINT
 VERI_CONTEXT
 VERI_DIST
 VERI_ENDCLASS
 VERI_ENDCLOCKING
 VERI_ENDPROGRAM
 VERI_EXTENDS
 VERI_FINAL
 VERI_INSIDE
 VERI_JOIN_ANY
 VERI_JOIN_NONE
 VERI_LOCAL
 VERI_NEW
 VERI_NULL
 VERI_PROGRAM
 VERI_PROTECTED
 VERI_PUBLIC
 VERI_PURE
 VERI_RAND
 VERI_RANDC
 VERI_RIGHTARRAW_ARROW   "->>"
 VERI_REF
 VERI_SOLVE
 VERI_STEPCONTROL_OP     "@@"
 // VERI_STRINGTYPE    /* Both Ams and SV need this - defined above */
 VERI_SUPER
 VERI_THIS
 VERI_VAR
 VERI_VIRTUAL
 VERI_WAIT_ORDER
 VERI_WITH
 VERI_WILDEQUALITY
 VERI_WILDINEQUALITY
 VERI_PROPERTY
 VERI_ENDPROPERTY
 VERI_COVER
 VERI_OVERLAPPED_IMPLICATION     /* |-> */
 VERI_NON_OVERLAPPED_IMPLICATION /* |=> */
 VERI_OVERLAPPED_FOLLOWED_BY     /* #-# */
 VERI_NON_OVERLAPPED_FOLLOWED_BY /* #=# */
 VERI_CYCLE_DELAY_OP             "##"
 VERI_DOLLAR                     /* $  */
 VERI_CONSECUTIVE_REPEAT         "[* "
 VERI_CONSECUTIVE_REPEAT_MUL     "[*]"   /* VIPER #6329 & #6357 */
 VERI_CONSECUTIVE_REPEAT_PLUS    "[+]"   /* VIPER #6329 & #6357 */
 VERI_NON_CONSECUTIVE_REPEAT     "[*="
 VERI_GOTO_REPEAT                "[*->"
 VERI_INTERSECT
 VERI_FIRST_MATCH
 VERI_THROUGHOUT
 VERI_WITHIN
 VERI_ENDSEQUENCE
 VERI_SEQUENCE
 VERI_LOG_EQUIVALENCE            /* "<->" */   /* VIPER #5910 */

 /* Verilog System 3.1a keyword additions */
 VERI_ASSUME
 VERI_BINS
 VERI_BINSOF
 VERI_COVERGROUP
 VERI_COVERPOINT
 // VERI_CROSS // common with AMS
 VERI_ENDGROUP
 VERI_ENDPACKAGE
 VERI_EXPECT
 VERI_FOREACH
 VERI_IGNOREBINS
 VERI_ILLEGAL_BINS
 VERI_MATCHES
 VERI_PACKAGE
 VERI_RANDCASE
 VERI_RANDSEQUENCE
 VERI_TAGGED
 VERI_TIMELITERAL
 VERI_WILDCARD

 /* System Verilog 1800 keyword additions */
 VERI_DOLLAR_UNIT
 VERI_WITH_DEFAULT // Special token to parse 'with default' (with_def_constraints: VCS Compatibility issue)
 /* System Verilog 1800-2009 keyword additions */
 VERI_LET
 VERI_GLOBAL
 VERI_CHECKER
 VERI_ENDCHECKER
 VERI_ACCEPT_ON
 VERI_REJECT_ON
 VERI_SYNC_ACCEPT_ON
 VERI_SYNC_REJECT_ON
 VERI_NEXTTIME
 VERI_S_NEXTTIME
 VERI_IMPLIES
 VERI_UNTIL
 VERI_S_UNTIL
 VERI_UNTIL_WITH
 VERI_S_UNTIL_WITH
 VERI_EVENTUALLY
 VERI_RESTRICT
 VERI_S_ALWAYS
 VERI_S_EVENTUALLY
 VERI_STRONG
 VERI_UNIQUE0
 VERI_UNTYPED
 VERI_WEAK

 /* System Verilog 1800-2012 keyword additions */
 VERI_NETTYPE
 VERI_SOFT
 VERI_INTERCONNECT
 VERI_IMPLEMENTS

 /* pragma token */
 VERI_OPT_PRAGMA

 /* comment token */
 VERI_OPT_COMMENT

 /* Token to force lex to read a token */
 VERI_FORCE_READ

 VERI_TICK_TIMESCALE

 /* fatal error token */
 VERI_FATAL_ERROR

/***************************************************************/
/* Rule types                                                  */
/***************************************************************/

/* --------- Verilog 95/2000 rule types ---------------------------- */

%type <str>
    VERI_ID VERI_SYSTEM_TASK VERI_BASE VERI_BASED_NUM VERI_STRING
    VERI_EDGEDESCRIPTOR VERI_LIBRARY
    system_task_id system_func_id
    library_id_ref
    file_path edge_desc_list
    opt_closing_label
    time_spec

%destructor { Strings::free($$) ; }
    VERI_ID VERI_SYSTEM_TASK VERI_BASE VERI_BASED_NUM VERI_STRING
    VERI_EDGEDESCRIPTOR VERI_LIBRARY
    system_task_id system_func_id
    library_id_ref
    file_path edge_desc_list
    opt_closing_label
    time_spec

%type <real_number>
    VERI_REAL_NUM

%type <character>
    symbol VERI_UDP_VAL VERI_UDP_EDGE VERI_UDP_WILD_EDGE

%type <tree_node>
    case_item generate_case_item

%type <module>
    module_decl udp_decl config_decl

%type <module_item>
    root_item plain_root_item
    module_item plain_module_item
    function_item_decl plain_function_item_decl
    block_item_decl plain_block_item_decl
    specify_item plain_specify_item
    generate_item plain_generate_item generate_item_or_null
    non_generic_port_decl non_port_module_item module_or_generate_item module_common_item module_or_generate_item_decl
    data_decl variable_decl simple_variable_decl

    parameter_override continuous_assign
    gate_instantiation module_instantiation udp_instantiation specify_block initial_construct
    always_construct parameter_decl local_parameter_decl io_decl net_decl
    ansi_parameter_decl opt_udp_initial_statement udp_port_decl
    task_decl extended_task_decl function_decl extended_function_decl specparameter_decl path_decl
    system_timing_check generate_construct genvar_decl
    generate_conditional generate_case
    generate_for_loop generate_block
    genvar_assignment genvar_decl_assignment
    module_or_checker_item checker_or_generate_item module_or_checker_item_decl

%type <scope> then_scope if_token

%type <table> table

%type <array>
    delay opt_delay /*indexes*/ block_statements expr_list
    opt_list_of_ports ports module_items port_refs list_of_port_ids
    list_of_net_ids list_of_arguments list_of_variable_ids
    function_item_decls additional_insts
    port_connection_list table_entries list_of_net_assignments
    event_control_expr event_control block_item_decls specify_items mintypmax_expr_list
    list_of_specparam_decls term_list edge_term_list opt_list_of_parameters
    timing_check_args path_delay_value concat_elements
    variable_assignments variable_decl_or_assignments
    list_of_param_assignments list_of_defparam_assignments parameter_value_assignment
    list_of_param_decls parameters plain_tf_port_decl_list
    config_statements liblist file_path_specs opt_incdirs design_id_ref_list
    opt_tf_port_decl_list tf_port_decl_list ansi_port_decl_list case_items generate_case_items
    generate_items list_of_genvar_ids opt_case_items
    opt_comment pre_comment statements_or_null opt_statements_or_null
    //design_statement list_of_func_arguments opt_udp_port_decls udp_port_decls
    design_statement opt_udp_port_decls udp_port_decls
    list_of_udp_port_ids event_control_without_id opt_list_of_package_imports
    local_param_decls opt_local_param_decls

%destructor { cleanup_treenode_array($$) ; }
    ports list_of_arguments variable_decl_or_assignments variable_assignments

%type <map>
    attr_inst real_attr_inst attr_list opt_pragma pre_pragma

%type <id_def>
    module_decl_id //parameter_decl_id
    opt_block_label opt_seq_block_label block_decl_id specparam_decl_id
    udp_decl_id config_decl_id
    genvar_decl_id

    variable_identifier net_identifier
    port_identifier param_decl param_port_decl specparam_decl ansi_port_identifier

%destructor { delete $$ ; }
    module_decl_id

%type <name>
    //id_ref hier_ref task_id function_id task_block_id port_ref delay_event_hier_ref
    id_ref hier_ref task_id task_block_id port_ref delay_event_hier_ref
    function_call_prefix instance_id_ref cell_id_ref named_type function_decl_id task_decl_id
    param_value_hier_ref event_control_hier_ref extended_expr extended_lvalue

%destructor { delete ($$) ; }
    //id_ref hier_ref task_id function_id task_block_id port_ref delay_event_hier_ref
    id_ref hier_ref task_id task_block_id port_ref delay_event_hier_ref
    function_call_prefix instance_id_ref cell_id_ref named_type function_decl_id task_decl_id
    param_value_hier_ref event_control_hier_ref

%type <expr>
    port ansi_port_decl port_connection plain_connection tf_port_decl
    delay_value plain_ansi_port_decl delay_value_udp
    param_assignment argument
    opt_port_expr delay_control
    event_expr_item parenthesized_event_expr_item term timing_check_arg
    lvalue expr primary concatenation multi_concatenation concat_element
    string mintypmax_expr operator_expr number
    system_function_call cond_predicate
    //expr_or_tagged_id_expr func_argument function_call
    expr_or_tagged_id_expr function_call
    opt_index_or_range //or_event_expr

%destructor { delete ($$) ; }
    lvalue primary operator_expr expr concatenation multi_concatenation
    concat_element string mintypmax_expr number system_function_call
    //expr_or_tagged_id_expr func_argument function_call
    expr_or_tagged_id_expr function_call
    opt_index_or_range argument

%type <data_type>
    data_type port_type data_type_without_type_ref data_type_or_void port_type_opt
    tf_port_type

%destructor { delete ($$) ; }
    data_type port_type data_type_without_type_ref data_type_or_void port_type_opt
    tf_port_type

%type <statement>
    statement plain_statement statement_or_null
    blocking_assignment non_blocking_assignment operator_assignment
    conditional_statement case_statement udp_blocking_assign
    loop_statement wait_statement event_trigger disable_statement
    seq_block par_block plain_seq_block plain_par_block
    procedural_continuous_assignment task_enable task_enable_prefix system_task_enable
    procedural_timing_control_statement loop_var_assignment

%destructor { delete ($$) ; }
    statement plain_statement statement_or_null
    blocking_assignment non_blocking_assignment operator_assignment
    conditional_statement wait_statement event_trigger disable_statement
    procedural_continuous_assignment task_enable task_enable_prefix system_task_enable
    procedural_timing_control_statement loop_var_assignment loop_statement

%type <unsigned_number>
    VERI_NUM
    net_type simple_type opt_vec_scal opt_signing signing opt_lifetime lifetime
    drive_strength0 drive_strength1 charge_val inst_type edge_id edge_pol
    drive_val path_desc opt_polarity case_token direction
    timing_check_event_control join_keyword
    assignment_operator opt_var opt_config  vec_scal opt_semi param_type

%type <range>
    /*dimension*/ // opt_dimension: replaced with opt_index_or_range (VIPER #7545) // packed_dimension : Merged with dimension
    /*dimensions*/ opt_dimensions //packed_dimensions opt_packed_dimensions : Merged with dimension
    indexed_range_with_spaces

%destructor { delete ($$) ; }
    /*dimension dimensions*/ opt_dimensions
    indexed_range_with_spaces

 // VIPER #8003: Structure of a VeriRange and a Array:
%type <range_array_struct> dimensions dimension

%type <net_reg_assign>
    net_reg_assignment net_assignment

%type <defparam_assign>
    defparam_assignment

%type <inst> inst unnamed_inst named_inst
%type <strength> strength opt_strength
%type <path> path_description
%type <delay_or_event_control> delay_or_event_control

%type <config> config_rule_statement

%type <use_clause> use_clause_rule

%type <array> mc_concat_elements pattern_list

%type <expr> pattern pattern_element
/* --------- System Verilog rule types ---------------------------- */

%type <unsigned_number>
    opt_virtual opt_static_local_default
    class_qualifier import_export
    unique_priority opt_default
    opt_dpi_import_property
    force_token_read force_sv_implication
    virtual_interface property_final

    // System Verilog 1800 additions
    bins_keyword stream_operator clocking_direction
    opt_random_qualifier opt_tagged opt_wildcard operator

    direct_c_type abort_properties hash_zero

%type <str>
    VERI_UNSIZED_LITERAL class_scope VERI_TIMELITERAL
    opt_id_ref

%destructor { Strings::free($$) ; }
    VERI_UNSIZED_LITERAL class_scope VERI_TIMELITERAL
    opt_id_ref

%type <tree_node>
    // System Verilog 1800 additions
    case_pattern_item case_inside_item randcase_item
    production rs_case_item property_case_item

%type <scope> matches_token constraint_block_start

%type <name>
    opt_extends_class
    // System Verilog 1800 additions
    bind_target_instance bins_expr
    opt_production_id package_import_item
    constraint_decl_id opt_dot_name enum_decl_id
    interface_class_type

%destructor { delete ($$) ; }
    opt_extends_class
    // System Verilog 1800 additions
    bind_target_instance bins_expr
    opt_production_id package_import_item
    constraint_decl_id opt_dot_name enum_decl_id

%type <range>
    // System Verilog 1800 addition
    opt_range wildcard_dimensions
    pkd_dim pkd_dims opt_pkd_dims // packed dimension for enum type

%destructor { delete ($$) ; }
    opt_range wildcard_dimensions
    pkd_dim pkd_dims opt_pkd_dims // packed dimension for enum type

%type <expr>
    time_literal value_range dist_item
    event_expr //actual_event_expr actual_arg_expr
    sequence_spec property_spec
    property_expr sequence_expr clocking_event
    cycle_delay_range cycle_delay_const_range_expr const_or_range_expr
    inc_or_dec_expr /*clocking_skew*/ cycle_delay
    constraint_block constraint_expr constraint_set extended_constraint_set
    casting_type new_expr index
    // System Verilog 1800 additions
    repeat_range opt_repeat_item opt_iff_expr opt_coverage_event
    hierarchical_btf_identifier block_event_expr select_expr select_condition
    streaming_concatenation slice_size expression_or_dist
    pattern_without_expr weight_spec trans_set trans_range_list
    opt_expr bin_value qc_cond_predicate
    tagged_id_expr bound_expr enum_identifier
    extern_func_arg enum_range_expr timeunit_literal bracket_range_expr bracket_range_dollar
    bracket_expr ps_port_decl ps_initial_expr sequence_expr_wo_expr edge_id_expr
    opt_time_precision

%destructor { delete ($$) ; }
    time_literal value_range dist_item
    event_expr inc_or_dec_expr
    cycle_delay_range cycle_delay_const_range_expr const_or_range_expr
    casting_type index
    repeat_range opt_repeat_item

%type <module>
    interface_decl program_decl package_decl checker_decl anonymous_program_decl

%type <data_type>
    func_data_type type_reference func_data_type_or_void
    struct_union enum_type extern_func_type bit_or_reg_type ps_port_type

%type <module_item>
    timeunits_decl
    interface_item plain_interface_item
    non_port_interface_item interface_or_generate_item
    program_item plain_program_item
    class_decl class_item plain_class_item
    class_property_method class_constraint
    constraint_decl extern_constraint_decl constraint_prototype
    modport_decl modport_item modport_port_decl modport_tf_port
    property_decl sequence_decl concurrent_assertion_item concurrent_assertion_item_decl
    assertion_variable_declaration net_alias
    dpi_import_export dpi_function_proto dpi_task_proto
    combinational_construct latch_construct ff_construct final_construct
    struct_union_member named_function_proto named_task_proto method_prototype
    bind_directive bind_instantiation extern_tf_decl
    type_decl net_type_decl clocking_decl clocking_item loop_var_decl
    package_import_declaration overload_declaration

    // System Verilog 1800 additions
    package_item plain_package_item anonymous_program_item
    covergroup_decl coverage_spec_or_option coverage_spec coverage_option
    cover_point cover_cross bins_or_options bins_selection_or_option plain_anonymous_program_item
    checker_item plain_checker_item module_or_chcker_common_item

    extern_function_declaration opt_sample_func let_declaration package_export_decl
    named_type_var_decl interface_class_method_prototype interface_class_item
    plain_interface_class_item

%type <statement>
    assert_cover_property_statement jump_statement
    procedural_assertion_item immediate_assert_statement expect_property_statement
    clocking_drive sequence_item inc_or_dec_stmt
    unique_priority_case_statement unique_priority_conditional_statement

    // System Verilog 1800 additions
    randsequence_statement randcase_statement production_item rs_code_block
    rs_prod statement_wo_end_comment
    elaboration_system_task sequential_instantiation deferred_assertion

%type <array>
    interface_items program_items class_items
    id_list dist_list constraint_blocks real_constraint_blocks
    assertion_variable_decl_list func_blocking_assignment_list
    struct_union_member_list enum_decl_list
    modport_port_decl_list modport_item_list opt_modport_port_decl_list
    value_range_list list_of_type_assignments
    clocking_items clocking_decl_assigns variable_decls
    list_of_net_lvalues opt_property_tf_port_decl_list

    // System Verilog 1800 additions
    trans_list bind_target_instance_list
    opt_parameter_value_assignment package_items package_import_items
    anonymous_program_items opt_coverage_spec_or_option_list
    bins_or_options_list select_bins_or_emtpy  opt_bins_selection_or_option_list
    overload_proto_formals opt_list_of_arguments loop_variables
    hier_id_list case_pattern_items
    case_inside_items bins_or_empty  array_of_trans_range_list
    rs_prod_list randcase_item_list production_list rs_rule_list
    production_item_list rs_case_item_list class_parameter_value_assignment
    modport_clocking_import_port_decl checker_items
    opt_exprs exprs /*opt_loop_variables*/ /*foreach_expr_arr foreach_expr_element*/

    opt_list_of_extern_func_args list_of_extern_func_args list_of_ps_port_ids
    ps_port_decl_list list_of_package_imports
    property_case_item_list expression_or_dist_list opt_implements_class
    interface_class_type_list opt_extends_classes interface_class_items

%destructor { cleanup_treenode_array($$) ; }
    class_items interface_class_items

%type <id_def>
    statement_label
    interface_decl_id
    program_decl_id
    class_decl_id
    property_decl_id sequence_decl_id
    modport_decl_id modport_port_decl_id
    type_assignment type_port_assignment type_identifier forward_type_identifier
    clocking_decl_assign opt_clocking_decl_id clock_signal_decl_id

    // System Verilog 1800 additions
    package_decl_id covergroup_decl_id production_id overload_operator
   /* bins_lvalue */ opt_cover_point_id loop_variable_identifier checker_decl_id

    let_decl_id ps_port_identifier
    
%type <clocking_dir> direction_skew clocking_skew

%type <production_item> rs_rule rs_production_list

 // Structure of a VeriName and a Array :
%type <name_array_struct> foreach_array_ref_loop_vars

%destructor { delete $$.name ; delete $$.array ; }
    foreach_array_ref_loop_vars

%type <range> range

%destructor { delete ($$) ; }
    range

%type <unsigned_number> opt_dir

%type <array> constraint_exprs
%type <array> brac_constraint_exprs

/***************************************************************/
/* Operator Precedence rules in increasing order of precedence */
/***************************************************************/

// VIPER #6756: Not exactly sure if this precedence is correct or not here
// but without this we cannot resolve the shift/reduce conflict we get.
%nonassoc veri_no_ocur
%nonassoc VERI_OCUR

// VIPER 5497/5869 : 'if', 'else' in an expression should have lowest precedence of all (property/sequence) operators.
%nonassoc VERI_IF

%nonassoc veri_no_else     // force classic conflict else to stick to inner if.
%nonassoc VERI_ELSE

// Force some minor ambiquities, using high-precedence tokens
// ambiguities occur mostly for statements that do not end with a specific token, and are in a list with itself.
%nonassoc veri_no_comment  // force comments to attach to inner statement
%nonassoc VERI_OPT_COMMENT

%nonassoc veri_force_token_read
%nonassoc VERI_FORCE_READ

%nonassoc veri_no_colon    // force closing labels to inner statement

%nonassoc veri_no_cparen   // force ()'ed sequence expression not to be interpreted as a ()'ed mintypmaxexp.
%nonassoc VERI_CPAREN

// Precendence of System Verilog property/sequence operators
// Property operator precedence. Follow Table 16-3 in SV 1800-2009 :
%nonassoc  VERI_ALWAYS VERI_S_ALWAYS VERI_EVENTUALLY VERI_S_EVENTUALLY VERI_ACCEPT_ON VERI_REJECT_ON VERI_SYNC_ACCEPT_ON VERI_SYNC_REJECT_ON
// Property_expr operators should have lower precedence than sequence_expr operators.
// Have to make exception for AND/OR, which can be both sequence of property operator.
// Make them sequence operator precedence....

// property operators (table in SV 1800-2005 LRM 17.7.8 p.231).
%right VERI_OVERLAPPED_IMPLICATION VERI_NON_OVERLAPPED_IMPLICATION VERI_OVERLAPPED_FOLLOWED_BY VERI_NON_OVERLAPPED_FOLLOWED_BY

// Clocking events : @clk <expr>
// Precedence must VERY low :
// we want @clk <expr> |-> <expr> to be
//  resolved as @clk (<expr> |-> <expr>),
//  not as (@clk <expr>) |-> <expr>.
// VIPER 2582/5118 : to get expression tree built correct, clocks need to have slightly higher precedence than implication operations.
// This means we HAVE TO interpret the above expression as follows :
//       (@clk <expr>) |-> <expr>.
// This introduces a problem with clock flow, which needs to be fixed during elaboration.
%left VERI_CLOCKING_EVENT

// more property/sequence operators from Table 16-3 (SV 1800-2009)
%right VERI_UNTIL VERI_S_UNTIL VERI_UNTIL_WITH VERI_S_UNTIL_WITH VERI_IMPLIES
%right VERI_IFF
%left VERI_OR       // both a property and a sequence operator
%left VERI_AND      // both a property and a sequence operator
%nonassoc VERI_NOT VERI_NEXTTIME VERI_S_NEXTTIME // unary operator for properties. Higher prio than or/and.
%left VERI_INTERSECT
%left VERI_WITHIN
%right VERI_THROUGHOUT
%left "##"
%left VERI_CONSECUTIVE_REPEAT VERI_CONSECUTIVE_REPEAT_MUL VERI_CONSECUTIVE_REPEAT_PLUS VERI_NON_CONSECUTIVE_REPEAT VERI_GOTO_REPEAT
%left "," // for assignment.

// Precedence of Verilog operators
// In fact, this is an implementation of SV 1800-2009 table 11-2.
%right VERI_RIGHTARROW VERI_LOG_EQUIVALENCE
%right veri_sv_implication  // SV operators "->" and "<->" also appear in PSL (and have diffent precedence). Need this precedence token to force SV implication rule
%right VERI_QUESTION VERI_COLON
%left VERI_LOGOR
%left VERI_LOGAND
%left VERI_REDOR VERI_REDNOR
%left VERI_REDXOR VERI_REDXNOR
%left VERI_REDAND VERI_REDNAND
%left VERI_LOGEQ VERI_LOGNEQ VERI_CASEEQ VERI_CASENEQ VERI_WILDEQUALITY VERI_WILDINEQUALITY
%left VERI_INSIDE VERI_DIST  // SV operators: Table 11-2 in SV 1800-2009 (draft 9) puts inside and dist operations at same level as < <= => and >. Put them slightly lower for convenience.
%left VERI_LT VERI_GT VERI_LEQ VERI_GEQ
%left VERI_LSHIFT VERI_RSHIFT VERI_ARITLSHIFT VERI_ARITRSHIFT
%left VERI_PLUS VERI_MIN
%left VERI_MUL VERI_DIV VERI_MODULUS
%left VERI_POWER
%left VERI_LOGNOT VERI_REDNOT
%left VERI_INC_OP VERI_DEC_OP VERI_TAGGED // to resolve  ++ a -- b - c ++
%left VERI_WITH // VIPER 5528 : 'with' operator needs high precendence, so that prefix becomes a 'name' (rather than a whole expression).

%nonassoc VERI_UNARY_OPER // override default precedence for tokens used as unary oper

%%

/******************************* Source Text **********************/

source_text :
    | descriptions
    | VERI_BEGIN_EXPR expr
      { //Expression Parsing special token to resolve conflicts
         veri_file::SetParsedExpr((VeriExpression*)($2)) ;
      }
    | VERI_BEGIN_EXPR data_type_without_type_ref
      { //Expression Parsing special token to resolve conflicts
        // Viper 6964 allow initial expression of type parameters
         veri_file::SetParsedExpr((VeriExpression*)($2)) ;
      }
    ;
descriptions :description
    | descriptions description
    ;

description :
      root_item { /* VIPER #3954: Avoid default action $$ = $1 by providing an empty action block */ }
    | timeunits_decl // global time units
      {  // Add as item to the root module
         if ($1 && !VeriNode::IsSystemVeri()) {
             // Viper 5979 : Error out if not run in sysverilog mode
             $1->Warning("root scope declaration is not allowed in verilog 95/2K mode") ;
         }
         if ($1) {
             VeriModule *root_module = veri_file::GetRootModule(1/*create if needed*/) ;
             if (root_module) (void) root_module->AddModuleItem($1) ;
             // root_module will always be there (we create it if needed)
         }
      }
    | library_decl
    | include_statement
    | attr_inst timescale_specification
      {
         // VIPER #7703: Timescale is now set in 'timescale_specification' rule itself
         //veri_file::SetTimeScale($2) ; Strings::free($2) ;
         store_attribute($1) ; // Store attribute to global array
      }
    ;

// Parse and ignore extern function declarations. Ref: How to handle  extern "C"
extern_function_declaration: VERI_EXTERN opt_access_mode extern_func_type
      function_decl_id { $<scope>$ = present_scope ; create_method_id($4) ; }
      "(" opt_list_of_extern_func_args ")" ";"
      {
          VeriTreeNode tmp ;
          VeriIdDef *func_id = ($4) ? ($4)->GetId() : 0 ;
          VeriModuleItem *body = (func_id) ? func_id->GetModuleItem() : 0 ;
          VeriScope *func_scope = present_scope ;
          present_scope = $<scope>5 ;
          if (body && body->GetQualifier(VERI_EXTERN) && !body->IsVerilogMethod()) {
              // VIPER #5705: Allow more than one extern declaration, keep the first one:
              //tmp.Warning("%s is already delcared, second extern declaration ignored", func_id->Name()) ;
              //VeriScope *scope = pop_scope() ;
              delete ($3) ; // Delete data type
              delete ($4) ; // Delete function name (VeriName)
              // Delete the scope and all ids created with this decl
              unsigned i ;
              VeriExpression *port ;
              FOREACH_ARRAY_ITEM(($7), i, port) {
                  delete port ;
              }
              delete ($7) ;
              delete func_scope ;
              $$ = 0 ;
          } else {
              tmp.Warning("extern function declaration %s violates IEEE 1800 syntax", ($4) ? ($4)->GetName() : "<unknown>") ;
              VeriFunctionDecl *func_decl = new VeriFunctionDecl(0, $3, $4, $7, 0, 0, func_scope) ;
              func_decl->SetQualifier(VERI_EXTERN) ;
              func_decl->SetNotVerilogMethod() ;
              $$ = func_decl ;
          }
      }
    ;

opt_access_mode: /* empty */
    | VERI_STRING /* must be "A" or "C" */
      {
          if (!Strings::compare(($1), "A") && !Strings::compare(($1), "C")) {
              VeriTreeNode tmp ;
              tmp.Error("syntax error near %s", ($1)) ;
          }
          Strings::free($1) ;
      }
    ;

extern_func_type:
      VERI_VOID          { $$ = new VeriDataType(VERI_VOID, 0, 0) ; }
    | VERI_REG           { $$ = new VeriDataType(VERI_REG, 0, 0) ; }
    | VERI_BIT           { $$ = new VeriDataType(VERI_BIT, 0, 0) ; }
    | direct_c_type      { $$ = new VeriDataType($1, 0, 0) ; }
    | VERI_BIT dimension { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriDataType(VERI_BIT, 0, dim) ; }
    ;

direct_c_type:
      VERI_INT  { $$ = VERI_INT ; }
    | VERI_REAL { $$ = VERI_REAL ; }
    | VERI_ID /* Must be "pointer". Used VERI_ID to avoid adding as keyword */
      {
          if (!Strings::compare(($1), "pointer")) {
              VeriTreeNode tmp ;
              tmp.Error("syntax error near %s", ($1)) ;
          }
          Strings::free($1) ;
          // FIXME: Treat it as 'int' for now
          $$ = VERI_INT ;
      }
    | VERI_STRINGTYPE { $$ = VERI_STRINGTYPE ; }
    ;

opt_list_of_extern_func_args:  { $$ = new Array(1) ; /* create an empty array to print () */ }
    | list_of_extern_func_args { $$ = $1 ; }
    ;

list_of_extern_func_args: extern_func_arg
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_extern_func_args "," extern_func_arg
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

extern_func_arg:
      opt_dir bit_or_reg_type opt_id_ref /* NULL or "array" or id. Merged them to avoid bison conflict */
      {
          if (!($3) || Strings::compare(($3), "array")) {
              // Argument ($3) is type (array): Ignore it for now (FIXME):
              $$ = new VeriAnsiPortDecl((($1) ? ($1) : VERI_INPUT), $2, (VeriIdDef *)0 /* no id! */) ;
          } else {
              // Argument ($3) is id:
              $$ = new VeriAnsiPortDecl((($1) ? ($1) : VERI_INPUT), $2, ($3) ? declare_net_reg(new VeriVariable($3)) : (VeriIdDef *)0 /* no id! */) ;
          }
      }
    | opt_dir bit_or_reg_type veri_array VERI_ID
      { $$ = new VeriAnsiPortDecl((($1) ? ($1) : VERI_INPUT), $2, declare_net_reg(new VeriVariable($4))) ; }
    | opt_dir bit_or_reg_type veri_array dimension opt_id_ref
      {
          VeriIdDef *id = 0 ;
          if ($5) {
              VeriRange *dim = 0 ;
              GET_DIMENSION_RANGE(dim, $4) ;
              id = declare_net_reg(new VeriVariable($5, dim)) ;
          } else {
              // 'id' remains 0 here
              VeriRange *dim = 0 ;
              GET_DIMENSION_RANGE(dim, $4) ;
              delete dim ;
          }
          // FIXME: "array" is ignored
          $$ = new VeriAnsiPortDecl((($1) ? ($1) : VERI_INPUT), $2, id /* id can be 0 */) ;
      }
    | opt_dir direct_c_type opt_id_ref
      { $$ = new VeriAnsiPortDecl((($1) ? ($1) : VERI_INPUT), new VeriDataType($2, 0, 0), ($3) ? declare_net_reg(new VeriVariable($3)) : (VeriIdDef *)0 /* no id! */) ; }
    ;

opt_id_ref:   { $$ = 0 ; }
    | VERI_ID { $$ = $1 ; }
    ;

bit_or_reg_type:
      VERI_BIT           { $$ = new VeriDataType(VERI_BIT, 0, 0) ; }
    | VERI_BIT dimension
      {
          // FIXME: Make this non-standard packed range non-empty (otherwise we error out):
          VeriRange *dim = 0 ;
          GET_DIMENSION_RANGE(dim, $2) ;
          if (dim && !dim->GetLeft() && !dim->GetRight()) dim->SetLeft(new VeriDollar()) ; // Set with $
          $$ = new VeriDataType(VERI_BIT, 0, dim) ;
      }
    | VERI_REG           { $$ = new VeriDataType(VERI_REG, 0, 0) ; }
    | VERI_REG dimension
      {
          // FIXME: Make this non-standard packed range non-empty (otherwise we error out):
          VeriRange *dim = 0 ;
          GET_DIMENSION_RANGE(dim, $2) ;
          if (dim && !dim->GetLeft() && !dim->GetRight()) dim->SetLeft(new VeriDollar()) ; // Set with $
          $$ = new VeriDataType(VERI_REG, 0, dim) ;
      }
    ;

veri_array: VERI_ID /* Must be "array". Used VERI_ID to avoid adding as keyword */
      {
          if (!Strings::compare(($1), "array")) {
              VeriTreeNode tmp ;
              tmp.Error("syntax error near %s", ($1)) ;
          }
          Strings::free($1) ;
          // FIXME: Ignore this for now
      }
    ;

root_item : attr_inst pre_comment plain_root_item
      {
         // VIPER #6982: Add attributes to plan root items before resolving the module.
         // Attribute instance can be specified for modules/programs/interfaces, so have to add
         // those attributes before resolving the interface itself to resolve the attributes.
         // Otherwise those attributes will not be resolved and cannot produce error if
         // non constant expressions are used.
         if (attr_inst_arr && $3) append_attributes($3) ; // Append global attribute to parse tree
         if ($1 && $3) ($3)->AddAttributes($1) ;
         // VIPER #7637: This is an inplace action block, cannot update linefile from here:
         //if ($1) update_start_linefile($3) ; // VIPER #7563

         // Add module and root items to library and root module before parsing
         // optional comment and closing label to avoid yywrap being call before
         // AddModule/AddModuleItem. It will help us to end compilation unit in yywrap
         unsigned is_module = (($3) && ($3)->IsModule()) ? 1 : 0 ;
         // VIPER #4701 : Anonymous program cannot be added to library as it has
         // no name. It should be added to root package
         if (($3) && ($3)->IsAnonymousProgram()) is_module = 0 ;
         if (is_module) {
             // FIX ME : 'dirty' : cast to VeriModule now that we know it is one.
             // Should do this via a virtual function call.
             VeriModule *module = (VeriModule*)($3) ;

             // Find the library for this module
             VeriLibrary *work_lib = veri_file::GetWorkLib(LineFile::GetFileName(module->Linefile())) ;

             // VIPER #4774 side effect: Do not allow package 'std' to be analyzed into a library
             // not having name 'std'. We may get into trouble while resolving references to 'std':
             if (module && module->IsPackage() &&
                 Strings::compare("std", module->GetName()) &&
                 work_lib && !Strings::compare("std", work_lib->GetName())) {
                 // Package named 'std' is being analyzed into a library not having name 'std'.
                 // ModelSim produces error message. We do same (section 8.10.1 of 1800 LRM):
                 // VIPER #5248 (test_288): Need to allow under compatibility mode:
                 //module->Error ("builtin package '%s' cannot be replaced", "std") ;
                 module->Error("replacing builtin package '%s' violates IEEE 1800 LRM", "std") ;
                 //delete ($3) ; $3 = 0 ; // CHECKME: Delete the package?
             }

             // Since VIPER 5182, Resolve is called explicitly here, and not for modules that go into a temporary library.
             // Resolve is no longer part of the VeriModule constructor.
             if (work_lib && !work_lib->IsTmpLibrary()) {
                 if (module) module->ResolveBody() ;
             }

             // Now add the module to the library.
             // If that faiils, it is deleted, so reset the pointer.
             if (work_lib && !work_lib->AddModule(module)) $3 = 0 ;
         } else {
             // root item that is not a module. Should happen only for SystemVerilog or AMS.
             // Non-modules end up in the root module (The compilation unit).
             // modules themselves end up in the work library.
             // This destinction is only relevant for System Verilog.
             // Add to the root module.
             if (!VeriNode::IsSystemVeri() && !VeriNode::IsAms() && $3) {
                 // Viper 5979 : Error out if not run in sysverilog or ams mode
                 $3->Warning("root scope declaration is not allowed in verilog 95/2K mode") ;
             }
             // VIPER #7406 : Produce error if function/task is declared in root
             // scope in verilog 95/2k or ams mode
             if ($3 && ($3->IsFunctionDecl() || $3->IsTaskDecl()) && VeriNode::IsAms()) {
                 $3->Error("function/task declaration in root scope violates verilog AMS syntax") ;
             }
             VeriModule *root_module = veri_file::GetRootModule(1/*create if needed*/) ;
             if (root_module) {
                 // VCS Compatibility issue (block_after_checker): If module item
                 // is clocking declaration and AddModuleItem delete the clocking
                 // decl, global default_clockings may contain stale pointer. So
                 // remove that
                 unsigned need_to_remove = 0 ;
                 if ($3 && $3->IsClocking()) {
                     if (default_clockings && ((VeriModuleItem*)default_clockings->GetValue(present_scope)) == $3) {
                         need_to_remove = 1 ;
                     }
                 }
                 if (!root_module->AddModuleItem($3)) {
                     $3 = 0 ;
                     if (need_to_remove && default_clockings) (void) default_clockings->Remove(present_scope) ;
                 }
             }
         }
      } opt_comment opt_closing_label
      { $$ = $3 ;
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $5) ;
        // VIPER #7637: Update the linefile from here instead of the inplace action block:
        if ($1) update_start_linefile($$) ; // VIPER #7563
        if ($6) update_end_linefile($$) ; // VIPER #6113
        if ($$ && $6) ($$)->ClosingLabel($6) ; else Strings::free($6) ; // Memory leak fix: code_coverage/test187
      }
    ;

plain_root_item :
      module_decl
      {  // module decl as root item.
         $$ = $1 ; // pass it up for attribute insertion and insertion into the library.
      }
    | udp_decl
      {  $$ = $1 ; }
    | config_decl
      {  $$ = $1 ; }
    | interface_decl
      {  $$ = $1 ; }
    | program_decl
      {  $$ = $1 ; }
    | package_decl
      {  $$ = $1 ; }
    | checker_decl
      {  $$ = $1 ; }
// Rest of 'root' level declarations go into the regular (compilation unit) scope :
// VIPER #3855: Removed parsing of statement in root scope because it was creating conflicts
// with for, if and case generates. Statements in root is not allowed and we used to error out any way.
//    | plain_statement
//      { $$ = $1 ; /* implicit statement to moduleitem conversion */ if ($$) ($$)->Error("%s in compilation scope are not allowed", "statements") ; }
//    | module_instantiation  // moved to 'module_common_item'
    | parameter_decl // not officially allowed by LRM, but vcs supports it..
    | local_parameter_decl
    | class_decl
    | module_common_item
      { $$ = $1 ;
        if (($$) && ($$)->IsInstantiation()) ($$)->Error("%s in compilation scope are not allowed", "module instantiations") ;
        // Viper #4891 : Produce error if clocking declaration exists in root
        if (($$) && ($$)->IsClocking()) ($$)->Error("%s in compilation scope are not allowed", "clocking declarations") ;
      }
    | anonymous_program_decl  // System Verilog Std 1800 addition
      { $$ = $1 ; }
    | concurrent_assertion_item_decl // added here, since 'concurrent_assertion_item' is no longer visible from 'root_item'
    | assert_cover_property_statement
      { $$ = $1 ;
        if ($$) ($$)->Warning("assert/assume/cover property statement in root scope violates IEEE 1800 syntax") ;
      }
    | statement_label assert_cover_property_statement
      { $$ = $2 ;
        if ($2) ($2)->SetOpeningLabel($1) ;
        if ($$) ($$)->Warning("assert/assume/cover property statement in root scope violates IEEE 1800 syntax") ;
        process_labeled_stmt($1, $2) ; // VIPER #8303
        (void) pop_last_label() ; //last_label = 0 ; // reset last_label
      }
// VIPER 5248 : LRM violation, VCS compliance : gate instantiations in root items.
    | gate_instantiation
      { $$ = $1 ;
        if ($$) ($$)->Warning("gate instantiation in root scope violates IEEE 1800 syntax") ;
      }
    | extern_function_declaration
    ;

// VIPER #3873: Parse `timescale from bison to support use of macros:
timescale_specification :
      VERI_TICK_TIMESCALE time_spec VERI_DIV time_spec
      {
          // VIPER #7703: Set the timescale from here:
          char *this_timescale = Strings::save($2, "/", $4) ;
          veri_file::SetTimeScale(this_timescale) ;
          Strings::free(this_timescale) ;
          Strings::free($2) ;
          Strings::free($4) ;
      }
    ;

time_spec : // V2K LRM 19.8: timescale arguments
    VERI_NUM VERI_ID
      {   // if time unit is not part of timeliteral token, then parse as individual token :
          // LRM (v2k) 19.8: time_spec can only have an integer before the time unit identifier (CHECK : integer must be power of 10)
          // Calling itoa makes a large unsigned number to -ve (integer), use it anyway for now.
          char *val = Strings::itoa($1) ;
          $$ = Strings::save(val, $2) ;
          if (Strings::compare_nocase($2, "step")) {
              VeriTreeNode node ;
              node.Error("step cannot be used in %s", "timescale") ;
          }
          // VIPER #4559: Free the strings after use:
          Strings::free(val) ;
          Strings::free($2) ;
      }
    | VERI_TIMELITERAL // SV adds time literal <num><id> as one token
      {
          $$ = $1 ;
          if (strstr($1, "step")) {
              VeriTreeNode node ;
              node.Error("step cannot be used in %s", "timescale") ;
          }
      }
    ;

/******************************* Module and Primitives **********************/

module_keyword : VERI_MODULE | VERI_MACROMODULE ;

module_decl :
      module_keyword { design_unit_depth++ ; config_is_identifier++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; }
        opt_lifetime module_decl_id opt_pragma { push_scope($4) ; }
        opt_list_of_package_imports
        opt_list_of_parameters
        opt_list_of_ports
      ";" { extract_scope_info_from_extern_mod($4, $8, $9) ; }opt_pragma
        module_items VERI_ENDMODULE
      {
        design_unit_depth-- ; config_is_identifier-- ;
        Array *param_list = get_param_list($4, $8, $9) ;
        Array *port_list = get_port_list($4, $9) ; ($9) = 0 ;
        $$ = new VeriModule($4,param_list,port_list,$13,pop_scope()) ;
        // Set qualifiers
        ($$)->SetQualifier($3) ;
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale

        // Clear all comments that might be left over after module is parsed (and were not attached to something inside)
        // With this, we assume that comments AFTER the module do not apply to the module.
        // VIPER 2952 : this is not right. Comments after the last module item (but before the endmodule keyword) would be deleted.
        // Before we delete comments, we need to let them go through the post-comment processing 'root_item' rule.
        // VeriNode::DeleteComments(veri_file::TakeComments()) ;

        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the port list
        if ($12) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$12)) ; // pragma's from after the header
        Array *import_list = get_package_import_list($4, $7) ;
        if (import_list) ($$)->SetPackageImportDecls(import_list) ;
      }

    | VERI_EXTERN attr_inst module_keyword opt_lifetime module_decl_id opt_pragma { push_scope($5) ; if ($5) $5->SetIsExtern() ; }
        opt_list_of_package_imports
        opt_list_of_parameters
        opt_list_of_ports
      ";"
      {
        $$ = new VeriModule($5,$9,$10,0,pop_scope()) ;

        // Set qualifiers
        ($$)->SetQualifier(VERI_EXTERN) ;
        ($$)->SetQualifier($4) ;

        // Add extra pragmas and attributes
        if ($2) ($$)->AddAttributes($2) ;
        if ($6) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$6)) ; // pragma's from before the port list
        if ($8) ($$)->SetPackageImportDecls($8) ;
      }
    ;

opt_list_of_package_imports : { $$ = 0 ; }
    | list_of_package_imports
    ;

list_of_package_imports : package_import_declaration
      { $$ = new Array(1) ; if ($1) ($$)->InsertLast($1) ; }
    | list_of_package_imports package_import_declaration
      { $$ = $1 ; if ($2) ($$)->InsertLast($2) ; }
    ;

interface_decl :
      VERI_INTERFACE { design_unit_depth++ ; config_is_identifier++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; }
        opt_lifetime interface_decl_id opt_pragma { push_scope($4) ; }
        opt_list_of_package_imports
        opt_list_of_parameters
        opt_list_of_ports
      ";" { extract_scope_info_from_extern_mod($4, $8, $9) ; } opt_pragma
        interface_items
      VERI_ENDINTERFACE
      {
        design_unit_depth-- ; config_is_identifier-- ;
        Array *param_list = get_param_list($4, $8, $9) ;
        Array *port_list = get_port_list($4, $9) ; ($9) = 0 ;
        $$ = new VeriInterface($4,param_list,port_list,$13,pop_scope()) ;
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale

        // Set qualifiers
        ($$)->SetQualifier($3) ;

        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the port list
        if ($12) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$12)) ; // pragma's from after the header
        Array *import_list = get_package_import_list($4, $7) ;
        if (import_list) ($$)->SetPackageImportDecls(import_list) ;
      }
    | VERI_EXTERN VERI_INTERFACE opt_lifetime interface_decl_id opt_pragma { push_scope($4) ; if ($4) $4->SetIsExtern() ;}
        opt_list_of_package_imports
        opt_list_of_parameters
        opt_list_of_ports
      ";"
      {
        $$ = new VeriInterface($4,$8,$9,0,pop_scope()) ;

        // Set qualifiers
        ($$)->SetQualifier(VERI_EXTERN) ;
        ($$)->SetQualifier($3) ;
        if ($7) ($$)->SetPackageImportDecls($7) ;
      }
    ;

program_decl :
      VERI_PROGRAM { design_unit_depth++ ; config_is_identifier++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; }
        opt_lifetime program_decl_id opt_pragma { push_scope($4) ; }
        opt_list_of_package_imports
        opt_list_of_parameters
        opt_list_of_ports
      ";" { extract_scope_info_from_extern_mod($4, $8, $9) ; } opt_pragma
        program_items
      VERI_ENDPROGRAM
      {
        design_unit_depth-- ; config_is_identifier-- ;
        Array *param_list = get_param_list($4, $8, $9) ;
        Array *port_list = get_port_list($4, $9) ; ($9) = 0 ;
        $$ = new VeriProgram($4,param_list,port_list,$13,pop_scope()) ;
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale
        // Set qualifiers
        ($$)->SetQualifier($3) ;
        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the port list
        if ($12) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$12)) ; // pragma's from after the header
        Array *import_list = get_package_import_list($4, $7) ;
        if (import_list) ($$)->SetPackageImportDecls(import_list) ;
      }
    | VERI_EXTERN VERI_PROGRAM opt_lifetime program_decl_id opt_pragma { push_scope($4) ; if ($4) $4->SetIsExtern() ;}
        opt_list_of_package_imports
        opt_list_of_parameters
        opt_list_of_ports
      ";"
      {
        $$ = new VeriProgram($4,$8,$9,0,pop_scope()) ;
        // Set qualifiers
        ($$)->SetQualifier(VERI_EXTERN) ;
        ($$)->SetQualifier($3) ;
        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the parameter list
        if ($7) ($$)->SetPackageImportDecls($7) ;
      }
    ;

// VIPER #5710: (vcs-compatibility: always_checker):
// Checker is a new buliding block that specially encapsulate assertion items:
checker_decl :
      VERI_CHECKER { design_unit_depth++ ; /* config_is_identifier++ ; */ $<str>$ = Strings::save(veri_file::GetTimeScale()) ; }
        opt_lifetime checker_decl_id opt_pragma { within_seq_property = 1 ; push_scope($4) ; }
        opt_property_tf_port_decl_list { within_seq_property = 0 ; } // VIPER #6512: Reset the flag here
      ";" opt_pragma
        checker_items
      VERI_ENDCHECKER
      {
        design_unit_depth-- ; /* config_is_identifier-- ; */
        $$ = new VeriChecker($4,$7,$11,pop_scope()) ;
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale
        // Set qualifiers
        ($$)->SetQualifier($3) ;
        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the port list
        if ($10) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$10)) ; // pragma's from after the header
        //within_seq_property = 0 ; // VIPER #6512: Already reset above
      }
    ;

class_decl :
      opt_virtual VERI_CLASS opt_lifetime class_decl_id opt_pragma { push_scope($4) ; }
        opt_list_of_parameters
        opt_extends_class // fix me : need to enter into this guy's scope. Bummer that this is AFTER the parameter decls..
        opt_implements_class
      ";" opt_pragma
        class_items
      VERI_ENDCLASS
      {
        // VIPER #5300: Flag to indicate that we are inside class (adjust for nested classes)
        if (within_class) within_class-- ;
        $$ = new VeriClass($4,$7,$8,$12,pop_scope(), $9) ;
        // Set qualifiers
        ($$)->SetQualifier($1) ;
        ($$)->SetQualifier($3) ;
        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the port list
        if ($11) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$11)) ; // pragma's from after the header
      }
    | VERI_INTERFACE VERI_CLASS class_decl_id opt_pragma { push_scope($3) ; }
       opt_list_of_parameters
       opt_extends_classes
       ";" opt_pragma
         interface_class_items
       VERI_ENDCLASS
      {
        if (within_class) within_class-- ;
        $$ = new VeriClass($3, $6, 0, $10, pop_scope(), $7, 1) ;
        // Set qualifiers
        //($$)->SetQualifier(VERI_INTERFACE) ;
        // Add pragmas from halfway..
        if ($4) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$4)) ; // pragma's from before the port list
        if ($9) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$9)) ; // pragma's from after the header
        // IEEE 1800-2012 section 8.26 : An interface class shall not be nested
        // within another class
        VeriIdDef *containing_class = (present_scope) ? present_scope->GetOwner(): 0 ;
        if (within_class && $$) ($$)->Error("illegal nested interface class %s within %s", ($3) ? ($3)->Name(): "", containing_class ? containing_class->Name(): "") ;
      }
    ;

opt_virtual : { $$ = 0 ; }
    | VERI_VIRTUAL { $$ = VERI_VIRTUAL ; }
    ;

opt_extends_class : { $$ = 0 ; }
    | VERI_EXTENDS hier_ref opt_list_of_arguments // FIXME : should include 'opt_list_of_arguments'
      { $$ = ($3) ? new VeriFunctionCall($2, $3) : $2 ; //} // VIPER #4032 : Store list of arguments in function call
        set_base_class_scope($$) ; }
    | VERI_EXTENDS hier_ref parameter_value_assignment opt_list_of_arguments// FIXME : Should include 'parameter_value_assignment'
      { $$ = ($4 && $2) ? new VeriFunctionCall((($2) ? ($2)->SetParamValues($3): $2), $4) : (($2) ? ($2)->SetParamValues($3) : $2) ; //}
        set_base_class_scope($$) ; }
    ;

opt_list_of_arguments :
      { $$ = 0 ; }
    | "(" list_of_arguments ")"
      { $$ = $2 ; }
    ;

opt_implements_class : { $$ = 0 ; }
    | VERI_IMPLEMENTS interface_class_type_list
      { $$ = $2 ; }
    ;

interface_class_type_list : interface_class_type
      {  $$ = new Array() ; ($$)->InsertLast($1) ; }
    | interface_class_type_list VERI_COMMA interface_class_type
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

interface_class_type : hier_ref opt_parameter_value_assignment
      { $$ = ($1 && $2) ? ($1)->SetParamValues($2): $1 ; }
    ;

opt_extends_classes : { $$ = 0 ; }
    | VERI_EXTENDS interface_class_type_list
      { $$ = $2 ; }
    ;

// timeunits decl

timeunits_decl : // Do them one by one, as module items
      // VIPER #8304 : Allow optional time precision
      VERI_TIMEUNIT timeunit_literal opt_time_precision ";"
      { $$ = new VeriTimeUnit(VERI_TIMEUNIT, $2, $3) ; }
    | VERI_TIMEPRECISION timeunit_literal ";"
      { $$ = new VeriTimeUnit(VERI_TIMEPRECISION, $2) ; }
    ;

opt_time_precision : { $$ = 0 ; }
    | VERI_DIV timeunit_literal
      { $$ = $2 ; }
    ;

timeunit_literal : time_literal
     // VCS Compatibility issue (Exar_svtb and timeunit) : allow space between
     // number and unit
    | VERI_NUM VERI_ID
      {
         // Calling itoa makes a large unsigned number to -ve (integer), use it anyway for now.
         char *num_str = Strings::itoa($1) ;
         $$ = new VeriTimeLiteral(Strings::save(num_str, $2)) ;
         Strings::free(num_str) ;
         Strings::free($2) ;
         $$->Warning("timeunit/precision declaration with a space before unit violates IEEE 1800 syntax") ;
      }
    ;

 /******************************* Module Parameters and Ports **********************/

opt_list_of_ports :
      { $$ = 0 ; }
    | "(" ports ")"
      { $$ = $2 ; }
    | "(" ansi_port_decl_list ")"
      { $$ = $2 ; }
    | "(" VERI_DOTSTAR ")"
      { $$ = new Array() ; ($$)->InsertLast(new VeriDotStar()) ; }
    ;

opt_list_of_parameters :
      { $$ = 0 ; }
    | "#" "(" parameters ")"
      { $$ = $3 ; }
    | "#" "(" ")" // Std 1800 addition
      { $$ = 0 ; }
    ;

parameters : ansi_parameter_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | param_port_decl
      { $$ = new Array() ; ($$)->InsertLast(new VeriDataDecl(VERI_PARAMETER, 0, $1)) ; }
    | parameters "," param_port_decl
      {
          $$ = $1 ;
          // Single parameter after a ansi_param_decl list : add this id to the last param_port_decl in the list :
          VeriDataDecl *last_decl = (VeriDataDecl*) ($$)->GetLast() ;
          if (last_decl) last_decl->AddDataDecl($3) ;
          //update_end_linefile(last_decl) ; // VIPER #6943: Ending linefile updated in AddDataDecl()
      }
    | parameters "," ansi_parameter_decl
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

ansi_parameter_decl :
  // We can only parse one 'id' (param_port_decl) in this rule.
  // Later ones can be added at next rule up.
  // This is because id's are separated by a comma, and so are ansi_param_decl's.
      param_type param_port_decl
      { $$ = new VeriDataDecl($1, 0, $2) ; /* don't set a datatype, since this parameter (no type) must adjust to type of its value */ }
    | param_type dimensions param_port_decl
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriDataDecl($1, new VeriDataType(0,0,dim), $3) ; }
    | param_type signing opt_dimensions param_port_decl
      { $$ = new VeriDataDecl($1, new VeriDataType(0,$2,$3), $4) ; }
    | param_type data_type param_port_decl
      { $$ = new VeriDataDecl($1, $2, $3) ; }
      // named data type :
    | param_type hier_ref opt_dimensions param_port_decl
      { $$ = new VeriDataDecl($1, new VeriTypeRef($2,0,$3), $4) ; }
    | param_type hier_ref parameter_value_assignment param_port_decl
      { $$ = new VeriDataDecl($1, new VeriTypeRef(($2) ? ($2)->SetParamValues($3): $2, 0, 0), $4) ; }
      // type-parameters :
    | param_type VERI_TYPE type_port_assignment
      { $$ = new VeriDataDecl($1, new VeriDataType(VERI_TYPE,0,0), $3) ; }
    | data_type param_port_decl        // Std 1800 addition
      { $$ = new VeriDataDecl(VERI_PARAMETER, $1, $2) ; }
    | hier_ref opt_dimensions param_port_decl   // Std 1800 addition
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriTypeRef($1,0,$2), $3) ; }
    | hier_ref parameter_value_assignment param_port_decl   // Std 1800 addition
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriTypeRef(($1) ? ($1)->SetParamValues($2): $1, 0, 0), $3) ; }
    | VERI_TYPE type_port_assignment   // Std 1800 addition
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriDataType(VERI_TYPE,0,0), $2) ; }
    ;

// SV 1800-2009 feature : Parameter port declaration can have localparam (6.20.4)
param_type : VERI_PARAMETER  { $$ = VERI_PARAMETER ; }
    | VERI_LOCALPARAM        { $$ = VERI_LOCALPARAM ; }
    ;

ansi_port_decl_list : ansi_port_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
      // VIPER #2089: parse attributes in front of ansi port declaration:
    | real_attr_inst ansi_port_decl
      { $$ = new Array() ; ($$)->InsertLast($2) ; if ($1 && $2) ($2)->AddAttributes($1) ; update_start_linefile($2) ; /* VIPER #7563 */ }
    | ansi_port_decl_list "," pre_comment ansi_port_identifier opt_pragma opt_comment // VIPER #3689 & #6799: Preserve comments beside and before ANSI port decls
      {
          $$ = $1 ;
          // Single port after a ansi_port_decl list : add this id to the last port_decl in the list :
          VeriAnsiPortDecl *last_decl = (VeriAnsiPortDecl*) ($$)->GetLast() ;
          if (last_decl) {
              last_decl->AddPortDecl($4) ;
              if ($5) last_decl->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // VIPER #4018
          }
          add_pre_comments_to_node(last_decl, $3) ; // VIPER #6799
          add_pre_comments_to_node(last_decl, $6) ; // VIPER #3689
      }
    | ansi_port_decl_list "," ansi_port_decl
      { $$ = $1 ; ($$)->InsertLast($3) ; }
      // VIPER #2089: parse attributes in front of ansi port declaration:
    | ansi_port_decl_list "," real_attr_inst ansi_port_decl
      { $$ = $1 ; ($$)->InsertLast($4) ; if ($3 && $4) ($4)->AddAttributes($3) ; update_start_linefile($4, &(@3)) ; /* VIPER #7563 */ }
    ;

ansi_port_decl : pre_comment plain_ansi_port_decl opt_pragma opt_comment // VIPER #3689 & #6799: Preserve comments beside and before ANSI port decls
      {
          $$ = $2 ;
          if ($$) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$3)) ; // VIPER #4018
          add_pre_comments_to_node($$, $1) ; // VIPER #6799
          add_pre_comments_to_node($$, $4) ; // VIPER #3689
      }
    ;

plain_ansi_port_decl:
  // We can only parse one 'id' (port_identifier) in this rule.
  // Later ones can be added at next rule up.
  // This is because id's are separated by a comma, and so are ansi_port_decl's.
      // untyped :
      direction opt_var ansi_port_identifier // without port type
      {
        $$ = new VeriAnsiPortDecl($1,0,$3) ;
        if ($2 && $$) ($$)->SetQualifier($2) ; // Viper #5511
      }
      // with type :
    | direction opt_var port_type ansi_port_identifier
      {
        $$ = new VeriAnsiPortDecl($1,$3,$4) ;
        if ($2 && $$) ($$)->SetQualifier($2) ; // Viper #5511
      }
      // without direction (new in 3.0/3.1) for task/function ports
    | opt_var port_type ansi_port_identifier
      {   $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,$2,$3) ; }
      // named type :
      // VIPER #4733 : Allow net type ports with data type :
    | direction opt_var port_type hier_ref dimensions ansi_port_identifier
      {
        VeriRange *dim = 0 ;
        GET_DIMENSION_RANGE(dim, $5) ;
        VeriDataType *dtype = create_net_port_type($3, new VeriTypeRef($4, 0, dim)) ;
        $$ = new VeriAnsiPortDecl($1,dtype, $6) ;
      }
    | direction opt_var hier_ref dimensions ansi_port_identifier
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $4) ; $$ = new VeriAnsiPortDecl($1,new VeriTypeRef($3, 0, dim), $5) ; }
    | direction opt_var port_type hier_ref ansi_port_identifier
      { VeriDataType *dtype = create_net_port_type($3, new VeriTypeRef($4, 0, 0)) ;
        $$ = new VeriAnsiPortDecl($1,dtype, $5) ; }
    | direction opt_var hier_ref ansi_port_identifier
      { $$ = new VeriAnsiPortDecl($1,new VeriTypeRef($3, 0, 0), $4) ; }
    | direction opt_var port_type hier_ref class_parameter_value_assignment ansi_port_identifier
      { VeriDataType *dtype = create_net_port_type($3, new VeriTypeRef(($4) ? ($4)->SetParamValues($5): 0, 0, 0)) ;
        $$ = new VeriAnsiPortDecl($1,dtype, $6) ; }
    | direction opt_var hier_ref class_parameter_value_assignment ansi_port_identifier
      { $$ = new VeriAnsiPortDecl($1,new VeriTypeRef(($3) ? ($3)->SetParamValues($4): 0, 0, 0), $5) ; }
    | VERI_INTERFACE ansi_port_identifier // general interface reference port
      { $$ = new VeriAnsiPortDecl(VERI_INTERFACE,new VeriTypeRef((VeriName*)0,0,0),$2) ; /* create empty 'typeref' template */ }
    | VERI_INTERFACE "." VERI_ID ansi_port_identifier // general modport reference port
      { $$ = new VeriAnsiPortDecl(0,new VeriTypeRef(new VeriSelectedName(new VeriKeyword(VERI_INTERFACE),$3),0,0),$4) ; }
// Here is the 'interface_port_declaration' as appearing in a port list of module, program, interface
    | hier_ref dimensions ansi_port_identifier // also port without direction and named type
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriAnsiPortDecl(0,new VeriTypeRef($1, 0, dim), $3) ; }
    | opt_var port_type hier_ref dimensions ansi_port_identifier // also port without direction and named type
      {
        VeriRange *dim = 0 ;
        GET_DIMENSION_RANGE(dim, $4) ;
        VeriDataType *dtype = create_net_port_type($2, new VeriTypeRef($3, 0, dim)) ;
        $$ = new VeriAnsiPortDecl(0,dtype, $5) ;
      }
    | VERI_VAR hier_ref dimensions ansi_port_identifier // also port without direction and named type. VIPER 5485
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $3) ; $$ = new VeriAnsiPortDecl(0,new VeriTypeRef($2, 0, dim), $4) ; }
    | hier_ref ansi_port_identifier // also port without direction and named type
      { $$ = new VeriAnsiPortDecl(0,new VeriTypeRef($1, 0, 0), $2) ; }
    | opt_var port_type hier_ref ansi_port_identifier // also port without direction and named type
      { VeriDataType *dtype = create_net_port_type($2, new VeriTypeRef($3, 0, 0)) ;
        $$ = new VeriAnsiPortDecl(0,dtype, $4) ; }
    |  VERI_VAR hier_ref ansi_port_identifier // also port without direction and named type. VIPER 5485
      { $$ = new VeriAnsiPortDecl(0,new VeriTypeRef($2, 0, 0), $3) ; }
    | hier_ref class_parameter_value_assignment ansi_port_identifier // also port without direction and named type
      { $$ = new VeriAnsiPortDecl(0,new VeriTypeRef(($1) ? ($1)->SetParamValues($2): $1, 0,0), $3) ; }
    | opt_var port_type hier_ref class_parameter_value_assignment ansi_port_identifier // also port without direction and named type
      { VeriDataType *dtype = create_net_port_type($2, new VeriTypeRef(($3) ? ($3)->SetParamValues($4): $3, 0,0)) ;
        $$ = new VeriAnsiPortDecl(0,dtype, $5) ; }
    | VERI_VAR hier_ref class_parameter_value_assignment ansi_port_identifier // also port without direction and named type. VIPER 5485
      { $$ = new VeriAnsiPortDecl(0,new VeriTypeRef(($2) ? ($2)->SetParamValues($3): 0, 0, 0), $4) ; }
    ;

class_parameter_value_assignment : /* Needed to remove conflict for 'class_name # a.b .c(x)': Allow parameter overwrite with braces */
      "#" "(" list_of_param_assignments ")" // normal list (named/ordered) of parameter values.
      { $$ = $3 ; }
    ;

// Verilog 95 style port (expression) list
// Note that here the expressions will reference id's that are not yet declared. So we want to set resolve_idrefs to 0.
// However, bison rules do not allow us to insert a action subrule, so we let resolving happen, but
// re-set any potential references in the Resolve routine for port lists.
ports : port
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | ports "," port
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

// VIPER 2740 : add opt_comment to port expressions.
// VIPER #6799: Had to add 'pre_comment' here to avoid conflict in 'ansi_port_decl' rule with 'pre_comment'
port :  pre_comment opt_port_expr opt_comment
      { $$ = $2 ; add_pre_comments_to_node($$, $1) ; add_pre_comments_to_node($$, $3) ; }
    | "." VERI_ID "(" opt_port_expr ")"
      { $$ = create_port_connect($2, $4) ; }
    ;

opt_port_expr :
      { $$ = new VeriPortOpen() ; /* Port expression can be NULL */ }
    | port_ref
      { $$ = $1 ; }
    | "{" port_refs "}"
      { $$ = new VeriConcat($2) ; }
    ;

port_refs : port_ref
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | port_refs "," port_ref
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

port_ref : hier_ref opt_dimensions
       { $$ = create_indexed_name($1, $2) ; }
    ;

/******************************* Module Items **********************/

module_items :
      { $$ = 0 ; }
    | module_items module_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

// module item can have attributes and comments around it, and many items have a closing label.
module_item : attr_inst pre_comment pre_pragma plain_module_item opt_comment opt_closing_label
      { $$ = $4 ;
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        if ($3 && $$) ($$)->AddAttributes(($$)->PragmasToAttributes(0, $3)) ; // Viper #6009/5972:
        add_post_comments_to_node($$, $5) ;
        if ($6 && $$) ($$)->ClosingLabel($6) ;
        if ($6) update_end_linefile($$) ; // VIPER #6113

        // VIPER #6838: Need to apply pragmas from after the module item which are specified
        // in the same line of this module item. Note that if there are multiple
        // pragmas in multiple line, the following does not work. But for simple
        // cases with single line pragma on the same line works properly.
        // Check if the pragma is on the same line as this module-item:
        
        //CARBON_BEGIN
        //if (($$) && ((unsigned)(@4).last_line == veri_file::GetLastPragmaLine())) { OldVerificVersion
	if (($$) && ((unsigned)(@4).last_line <= veri_file::GetLastPragmaLine())) {
        //CARBON_END
            // Found some pragma on the same line: Take and add them on the module item:
            ($$)->AddAttributes(($$)->PragmasToAttributes(0, veri_file::TakePragmas())) ;
        }
      }
    | attr_inst ";"
      {
        $$ = new VeriNullStatement() ; // VIPER #8419: create null statement
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | attr_inst timescale_specification
      {
          store_attribute($1) ; // Store attribute to global array
          // VIPER #3873: Parse and error out for `timscale inside module, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($2) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    ;

plain_module_item :
      non_generic_port_decl
    | non_port_module_item
    | timeunits_decl // needed for module filling
    ;

non_generic_port_decl :
      io_decl
//    | ref_decl // included in 'io_decl'
//    | interface_port_decl // included in 'io_decl'
    ;

non_port_module_item :
      module_or_generate_item
    | generate_construct
    | local_parameter_decl
    | parameter_decl
    | specify_block
    | specparameter_decl
    | program_decl
      { $$ = $1 ; /* a nested program. */
        // force-declare in scope :
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    //| class_decl
    | module_decl
      { $$ = $1 ; /* a nested module. */
        // force-declare in scope :
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | interface_decl  // Std 1800 addition
      { $$ = $1 ; /* a nested interface. */
        // force-declare in scope :
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | checker_decl  // Std 1800 addition
      { $$ = $1 ; /* a nested checker. */
        // force-declare in scope :
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    ;

module_or_generate_item :
      module_or_checker_item
    | module_common_item
    | parameter_override
    | continuous_assign
    | gate_instantiation
    | udp_instantiation  // unnamed instantiations
//    | module_instantiation // named instantiations : moved to 'module_common_item'
    //| initial_construct // moved to 'module_or_checker_item'
    //| always_construct  // moved to 'module_or_checker_item'
//    | table // only in a udp table
    | combinational_construct
    | latch_construct
    | ff_construct
    | net_alias
    //| final_construct // moved to 'module_or_checker_item'
    //| concurrent_assertion_item // moved here from 'module_common_item', moved to 'module_or_checker_item'
    | class_decl // moved here from 'non_port_module_item' (VIPER #4040)
    // VIPER #5612: Moved the following 3 rules here since generate items are not allowed in root scope:
    // VIPER #3855: Added following 3 rules as module item as per BNF and section 19.12.5 of IEEE 1800 LRM
    // Added them out-side of SV mode because we had to remove them from plain_generate_item because of conflicts
    //| generate_conditional // moved to 'checker_or_generate_item'
    //| generate_case // moved to 'checker_or_generate_item'
    //| generate_for_loop // moved to 'checker_or_generate_item'
    | checker_or_generate_item
    ;

module_or_checker_item :
      initial_construct
    | always_construct
    | final_construct
    | concurrent_assertion_item
    ;

checker_or_generate_item :
      generate_conditional
    | generate_case
    | generate_for_loop
    | generate_block // VIPER #5612: Added it to support begin ... end generate block too
    ;

module_common_item :
      module_or_generate_item_decl
    | module_instantiation // named instantiations. Moved here from 'root_item' and 'module_or_generate_item'.
//    | interface_instantiation // same syntax as module instantiation. This is the reason we moved module instantiation to 'common_item'.
//    | program_instantiation   // same syntax as module instantiation
//    | concurrent_assertion_item // moved to 'interface_or_generate_item' and 'module_or_generate_item', because it collides with 'assertion' statements if it were visible from 'root_item'
    | bind_directive
    | module_or_chcker_common_item
    //| overload_declaration // System Verilog Std 1800 addition, moved to 'module_or_chcker_common_item'
    //| clocking_decl  // Moved from module_or_generate_item_decl, so that it (module_or_generate_item_decl) can be used in package_items, moved to 'module_or_chcker_common_item'
    //| elaboration_system_task { $$ = $1 ; } // VIPER #5248 (dollar_warning): Allow elaboration system task,  moved to 'module_or_chcker_common_item'
    // VIPER #3855: Added following 3 rules as module item as per BNF and section 19.12.5 of IEEE 1800 LRM
    // Added them out-side of SV mode because we had to remove them from plain_generate_item because of conflicts
    // VIPER #5512: generate for/if/case/block are only allowed within module, not as root item, moved in appropriate place
    //| generate_conditional
    //| generate_case
    //| generate_for_loop
    ;

module_or_chcker_common_item :
      overload_declaration
    | clocking_decl
    | elaboration_system_task { $$ = $1 ; }
    ;

elaboration_system_task:
      system_task_enable ";"
      {
          $$ = $1 ;
          VeriSystemTaskEnable *task = (VeriSystemTaskEnable *)($1) ;
          unsigned task_type = (task) ? (task)->GetFunctionType() : 0 ;
          // VIPER #5248 (dollar_warning): The following are elaboration system tasks.
          // And these are allowed as module common item inside VCS, so, add them here:
          VeriTreeNode tmp ;
          switch(task_type) {
          case VERI_SYS_CALL_FATAL  : // fatal
          case VERI_SYS_CALL_ERROR  : // error
          case VERI_SYS_CALL_WARNING: // warning
          case VERI_SYS_CALL_INFO   : // info
              if (!VeriNode::IsSystemVeri2009OrLater()) {
                  // VIPER #7651: This is now part of IEEE-1800 (2009) LRM, so allow it only in SV-2009 mode.
                  // Continue to produce warning message in other dialects:
                  tmp.Warning("elaboration system task %s violates IEEE 1800 syntax", VeriNode::PrintToken(task_type)) ;
              }
              break ;
          default:
              char *token = Strings::save("$", (task)?task->GetName():"<unknown>") ;
              tmp.Error("syntax error near %s", token) ;
              Strings::free(token) ;
              delete ($$) ;
              $$ = 0 ;
              break ;
          }
      }
    ;

module_or_generate_item_decl :
      module_or_checker_item_decl
    | net_decl
    //| data_decl // moved to 'module_or_checker_item_decl'
    //| genvar_decl // moved to 'module_or_checker_item_decl'
    | task_decl
    | extended_task_decl
    | extended_function_decl
    //| function_decl // moved to 'module_or_checker_item_decl'
    | dpi_import_export
    | extern_constraint_decl
//    | extern_method_decl // class-specific function/task decl : moved into function/task decl.
    //| covergroup_decl /* System Verilog 1800 addition moved to 'module_or_checker_item_decl'*/
    ;

module_or_checker_item_decl :
      data_decl
    | genvar_decl
    | function_decl
    | covergroup_decl
    ;

parameter_override : VERI_DEFPARAM list_of_defparam_assignments ";"
       { $$ = new VeriDefParam($2) ; }
    ;

bind_directive :
      // 2476, 2624 : need new scope around entire bind_instantiation
      // Also. Prevent any resolving from happening during a bind directive
      // Do this later when the bind instantiation is placed in context. Otherwise, we will accidentally pick up wrong iderefs.
      VERI_BIND bind_target_instance { push_scope(0) ; resolve_idrefs = 0 ; } bind_instantiation // module_instantiation already ends with a semi.
      { $$ = new VeriBindDirective($2,$4, pop_scope()) ; resolve_idrefs = 1 ; }
    | VERI_BIND VERI_ID ":" bind_target_instance_list  { push_scope(0) ; resolve_idrefs = 0 ; } bind_instantiation // module_instantiation already ends with a semi
      { $$ = new VeriBindDirective($2,$4,$6, pop_scope()) ; resolve_idrefs = 1 ; }
    ;

overload_declaration :
      VERI_BIND overload_operator VERI_FUNCTION func_data_type id_ref "(" overload_proto_formals ")" ";" // IEEE Std 1800 addition
      { $$ = new VeriOperatorBinding($2, $4, $5, $7) ; }
    ;

overload_operator :
      VERI_PLUS    { $$ = declare_operator(new VeriOperatorId(VERI_PLUS)) ; }
    | VERI_MIN     { $$ = declare_operator(new VeriOperatorId(VERI_MIN)) ; }
    | VERI_MUL     { $$ = declare_operator(new VeriOperatorId(VERI_MUL)) ; }
    | VERI_DIV     { $$ = declare_operator(new VeriOperatorId(VERI_DIV)) ; }
    | VERI_GT      { $$ = declare_operator(new VeriOperatorId(VERI_GT)) ; }
    | VERI_LT      { $$ = declare_operator(new VeriOperatorId(VERI_LT)) ; }
    | VERI_GEQ     { $$ = declare_operator(new VeriOperatorId(VERI_GEQ)) ; }
    | VERI_LEQ     { $$ = declare_operator(new VeriOperatorId(VERI_LEQ)) ; }
    | VERI_LOGEQ   { $$ = declare_operator(new VeriOperatorId(VERI_LOGEQ)) ; }
    | VERI_LOGNEQ  { $$ = declare_operator(new VeriOperatorId(VERI_LOGNEQ)) ; }
    | VERI_MODULUS { $$ = declare_operator(new VeriOperatorId(VERI_MODULUS)) ; }
    | VERI_INC_OP  { $$ = declare_operator(new VeriOperatorId(VERI_INC_OP)) ; }
    | VERI_DEC_OP  { $$ = declare_operator(new VeriOperatorId(VERI_DEC_OP)) ; }
    | VERI_POWER   { $$ = declare_operator(new VeriOperatorId(VERI_POWER)) ; }
    | VERI_EQUAL   { $$ = declare_operator(new VeriOperatorId(VERI_EQUAL)) ; }
    ;

func_data_type_or_void :
      func_data_type
    | VERI_VOID
      { $$ = new VeriDataType(VERI_VOID, 0, 0) ;}
    ;

func_data_type :
      data_type
    | hier_ref opt_dimensions
      { $$ = new VeriTypeRef($1, 0, $2) ; }
    | hier_ref parameter_value_assignment
      { $$ = new VeriTypeRef(($1) ? ($1)->SetParamValues($2): $1, 0, 0) ; }
    ;

overload_proto_formals :
      func_data_type
      { $$ = new Array() ; $$->InsertLast($1) ; }
    | func_data_type "," func_data_type
      { $$ = new Array() ; $$->InsertLast($1) ; $$->InsertLast($3) ; }
    ;

bind_target_instance_list :
      bind_target_instance
      {  $$ = new Array() ; ($$)->InsertLast($1) ; }
    | bind_target_instance_list "," bind_target_instance
      {  $$ = ($1) ? $1 : new Array() ; if ($3) ($$)->InsertLast($3) ; }
    ;

bind_target_instance :
      hier_ref opt_dimensions
      { $$ = create_indexed_name($1, $2) ; }
    ;

bind_instantiation :
      module_instantiation
//    | program_instantiation // same syntax as module instantiation
//    | interface_instantiation // same syntax as module instantiation
    ;

/******************************* Interface Items **********************/

interface_items :
      { $$ = 0 ; }
    | interface_items interface_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

// interface item can have attributes and comments around it, and sometimes a closing label.
interface_item : attr_inst pre_comment plain_interface_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create a null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | attr_inst timescale_specification
      {
          store_attribute($1) ; // Store attribute to global array
          // VIPER #3873 & #5127: Parse and error out for `timscale inside interface, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($2) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    ;

plain_interface_item :
      non_generic_port_decl
    | non_port_interface_item
    | timeunits_decl // needed for module filling
    ;

non_port_interface_item :
      interface_or_generate_item
    | generate_construct // actually should be interface_generate_construct
    // VIPER #6033: Allow generate items also
    | generate_conditional
    | generate_case
    | generate_for_loop
    | generate_block
//         | local_parameter_decl // not official
//         | parameter_decl // not official
//         | specify_block // not official
    //| specparameter_decl // VIPER #4850 : Specparam cannot be declared inside interface
    | class_decl
    | parameter_override // VIPER #5395 : Allow defparam inside interface
    | program_decl
      { // nested program declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | interface_decl
      { // nested interface declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | checker_decl
      { // nested checker declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    ;

interface_or_generate_item : // FIX ME : merge with module_or_generate_item
      module_common_item
    | continuous_assign
    | initial_construct
    | always_construct
    | local_parameter_decl
    | parameter_decl
    | combinational_construct
    | latch_construct
    | ff_construct
    | modport_decl
    | extern_tf_decl // extern function
    | final_construct
    | concurrent_assertion_item // moved here from 'module_common_item', since it should not be visi
    | net_alias
    ;

extern_tf_decl :
      VERI_EXTERN { extern_forkjoin = 1 ; } method_prototype // identical to a class property.. Works because it's in different area.
      { $$ = $3 ; if ($$) ($$)->SetQualifier(VERI_EXTERN) ; update_start_linefile($$) ; extern_forkjoin = 0 ;}
    | VERI_EXTERN VERI_FORKJOIN { extern_forkjoin = 1 ; } named_task_proto ";"
      { $$ = $4 ; if ($$) ($$)->SetQualifier(VERI_EXTERN) ; if ($$) ($$)->SetQualifier(VERI_FORKJOIN) ; update_start_linefile($$) ; extern_forkjoin = 0 ; }
    ;

/******************************* Program Items **********************/

program_items :
      { $$ = 0 ; }
    | program_items program_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

// program item can have attributes and comments around it, and a closing label.
program_item : attr_inst pre_comment plain_program_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create a null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | timeunits_decl // needed for module filling
    ;

plain_program_item :
      continuous_assign
    | module_or_generate_item_decl
    | specparameter_decl
    | local_parameter_decl
    | parameter_decl
    | initial_construct
    | concurrent_assertion_item
    | class_decl
    | clocking_decl // Moved from module_or_generate_item_decl, so that it (module_or_generate_item_decl) can be used in package_items
    | final_construct // Std 1800 addition
    | generate_construct // Std 1800 addition
    | generate_conditional   // Std 1800 addition
    | generate_case // Std 1800 addition
    | generate_for_loop // Std 1800 addition
    | generate_block // VIPER #5612: Added it to support begin ... end generate block too
    | io_decl // VIPER #4766: Add input output delcaration rule in program grammar
    | checker_decl
      { // nested checker declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | timescale_specification
      {
          // VIPER #5245: Parse and error out for `timscale inside program, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($1) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    | net_alias
      {
          $$ = $1 ;
          if ($$) ($$)->Warning("alias declaration in program violates IEEE 1800 syntax") ;
      }
    ;

/******************************* Checker Items **********************/

// VIPER #5710: (vcs-compatibility: always_checker):
checker_items :
      { $$ = 0 ; }
    | checker_items checker_item
      { $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

// checker item can have attributes and comments around it, and many items have a closing label.
checker_item :
    attr_inst pre_comment plain_checker_item opt_comment opt_closing_label
      {
        $$ = $3 ;
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create a null statement
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    ;

plain_checker_item :
      module_or_checker_item
    | module_or_chcker_common_item
    | checker_or_generate_item
    | module_or_checker_item_decl
    | VERI_RAND data_decl
      { $$ = $2 ; if ($$) $$->SetQualifier(VERI_RAND) ; update_start_linefile($$) ; } // rand qualifier on check variables
    | generate_construct
    | checker_decl
      { // nested checker declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | module_instantiation
    ;

/******************************* Class Items **********************/

class_items :
      { $$ = 0 ; }
    | class_items class_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

// class item can have attributes and comments around it, and a closing label.
class_item : attr_inst pre_comment plain_class_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create a null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | timeunits_decl // needed for module filling
    ;

plain_class_item :
      class_property_method
    | class_constraint
    // 3.1a additions :
//    | type_decl // already in 'data_decl' of 'class_property_method'
    | class_decl // nested classes
//    | timeunits_decl // already included in class_item
    | covergroup_decl
// VIPER 5248 : LRM Violation, VCS compliance : package import inside a class.
    | package_import_declaration
      {
          $$ = $1 ;
          if ($$) ($$)->Warning("package import in class violates IEEE 1800 syntax") ;
      }
// VCS Compatibility issue (class_localparam): According to LRM P1800(2009) parameter/
// localparameter declaration is allowed within class definition.
    | local_parameter_decl
    | parameter_decl
    ;

interface_class_items :
      { $$ = 0 ; }
    | interface_class_items interface_class_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

interface_class_item : attr_inst pre_comment plain_interface_class_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create a null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | timeunits_decl // needed for module filling
    ;

plain_interface_class_item :
      VERI_PURE VERI_VIRTUAL interface_class_method_prototype
      { $$ = $3 ; if ($$) ($$)->SetQualifier(VERI_PURE) ; if ($$) ($$)->SetQualifier(VERI_VIRTUAL) ; update_start_linefile($$) ; }
    | type_decl
    | local_parameter_decl
    | parameter_decl
    ;

interface_class_method_prototype :
      named_task_proto ";"
      { $$ = $1 ; }
    | named_function_proto ";"
      { $$ = $1 ; }
    ;

class_constraint :
      constraint_prototype
    | constraint_decl
    ;

class_property_method :
// class_property :
// 'data_decl' includes some qualifiers (const/lifetime), which collide with qualifiers here.
// So split into plain 'variable_decl' and 'type_decl', and add qualifiers here :
      simple_variable_decl
    | named_type_var_decl
    | VERI_CONST class_property_method
      { $$ = $2 ; if ($$) ($$)->SetQualifier(VERI_CONST) ; update_start_linefile($$) ; }
    | type_decl
// now class_method :
    | task_decl
    | function_decl
    | VERI_EXTERN method_prototype
      { $$ = $2 ; if ($$) ($$)->SetQualifier(VERI_EXTERN) ; update_start_linefile($$) ; }
    // VIPER #3824 : Allow 'pure' as qualifier to method
    | VERI_PURE method_prototype
      { $$ = $2 ; if ($$) ($$)->SetQualifier(VERI_PURE) ; update_start_linefile($$) ; }
// recursive call for qualifiers :
    | class_qualifier class_property_method
      {
          $$ = $2 ;
          if ($$) {
              ($$)->SetQualifier($1) ;
              // VIPER #8395: IEEE 1800-2012 section 8.10 says static methods cannot be
              // virtual. 'class_property_method' can be variable declaration too. So
              // check only methods (task/function)
              if ((($$)->IsFunctionDecl() || ($$)->IsTaskDecl()) &&($$)->GetQualifier(VERI_VIRTUAL) && ($$)->GetQualifier(VERI_STATIC)) {
                  // VIPER #7980: virtual and static are not allowed on a single method, BNF has the restriction:
                  ($$)->Error("static methods cannot be virtual") ;
              }
          }
          update_start_linefile($$) ;
      }
    ;

method_prototype :
      named_task_proto ";"
      { $$ = $1 ; }
    | named_function_proto ";"
      { $$ = $1 ; }
    | class_qualifier method_prototype // recursive call for qualifiers
      {
          $$ = $2 ;
          if ($$) {
              ($$)->SetQualifier($1) ;
              if (($$)->GetQualifier(VERI_VIRTUAL) && ($$)->GetQualifier(VERI_STATIC)) {
                  // VIPER #7980: virtual and static are not allowed on a single method, BNF has the restriction:
                  ($$)->Error("static methods cannot be virtual") ;
              }
          }
          update_start_linefile($$) ;
      }
    ;

class_qualifier : // actually method_qualifiers, class_qualifiers and lifetime merged in one.
      VERI_VIRTUAL   { $$ = VERI_VIRTUAL ; }
    | VERI_VIRTUAL VERI_INTERFACE { $$ = VERI_VIRTUAL ; }  // For virtual interface 1800
    | VERI_PROTECTED { $$ = VERI_PROTECTED ; }
    | VERI_LOCAL     { $$ = VERI_LOCAL ; }
    | VERI_RAND      { $$ = VERI_RAND ; }
    | VERI_RANDC     { $$ = VERI_RANDC ; }
    | VERI_STATIC    { $$ = VERI_STATIC ; }
    | VERI_AUTOMATIC { $$ = VERI_AUTOMATIC ; } // added since we need 'lifetime' on variable_decl..
    ;

/******************************* constraints **********************/

// constraint decl and proto. End with a semi ?

constraint_decl : // a class item (module item). // Since there are no declarations inside, a constraint decl does not need a scope.
      // Note : constraint decl does NOT end with a semi !
      opt_static_local_default VERI_CONSTRAINT constraint_decl_id constraint_block_start
     {
         VeriScope *constraint_scope = create_constraint_id($3, 0, ($1)? 1: 0) ; // Added for VIPER# 4179
         // VIPER #5093 : Set constraint scope as present scope before parsing
         // constraint block, so that scopes created inside contraint block become
         // child scopes of constraint scope.
         if (constraint_scope) present_scope = constraint_scope ;

      } constraint_blocks "}"
      {
        $$ = new VeriConstraintDecl($3,$6,present_scope) ; //($$)->SetQualifier($1) ;
        if ($1 == VERI_WITH_DEFAULT) {
            ($$)->SetQualifier(VERI_WITH) ;
            ($$)->SetQualifier(VERI_DEFAULT) ;
        } else {
            ($$)->SetQualifier($1) ;
        }
        present_scope = $4 ;
      }
    ;

constraint_block_start : "{"
      {  $$ = present_scope ; }
    ;

constraint_prototype : // a class item
      // Note : constraint prototype does NOT end with a semi !
      // VIPER #5195: SV 3.1 LRM did not have the ending semi-colon, IEEE 1800 has it:
      opt_static_local_default VERI_CONSTRAINT constraint_decl_id VERI_SEMI
      {
         VeriScope *constraint_scope = create_constraint_id($3, 0, ($1)? 1: 0) ; // Added for VIPER# 4179
         $$ = new VeriConstraintDecl($3,0,constraint_scope) ;
         if ($1 == VERI_WITH_DEFAULT) {
             ($$)->SetQualifier(VERI_WITH) ;
             ($$)->SetQualifier(VERI_DEFAULT) ;
         } else {
             ($$)->SetQualifier($1) ;
         }
      }
    | VERI_EXTERN opt_static_local_default VERI_CONSTRAINT constraint_decl_id VERI_SEMI // VIPER #6819: Added VERI_EXTERN
      {
         VeriScope *constraint_scope = create_constraint_id($4, 0, ($2)?1:0) ; // Added for VIPER# 4179
         $$ = new VeriConstraintDecl($4,0,constraint_scope) ;
         ($$)->SetQualifier(VERI_EXTERN) ; // VIPER #6819
         if ($2 == VERI_WITH_DEFAULT) {
             ($$)->SetQualifier(VERI_WITH) ;
             ($$)->SetQualifier(VERI_DEFAULT) ;
         } else {
             ($$)->SetQualifier($2) ;
         }
      }
    | VERI_PURE opt_static_local_default VERI_CONSTRAINT constraint_decl_id VERI_SEMI // VIPER #6819: Added VERI_PURE
      {
         VeriScope *constraint_scope = create_constraint_id($4, 0, ($2)?1:0) ; // Added for VIPER# 4179
         $$ = new VeriConstraintDecl($4,0,constraint_scope) ;
         ($$)->SetQualifier(VERI_PURE) ; // VIPER #6819
         if ($2 == VERI_WITH_DEFAULT) {
             ($$)->SetQualifier(VERI_WITH) ;
             ($$)->SetQualifier(VERI_DEFAULT) ;
         } else {
             ($$)->SetQualifier($2) ;
         }
      }
    ;

extern_constraint_decl : // a module item
      // Note : constraint decl does NOT end with a semi !
      // Modified for VIPER# 4179
      opt_static_local_default VERI_CONSTRAINT constraint_decl_id constraint_block_start
      {
           VeriScope *constraint_scope = create_constraint_id($3, 1, $1 ? 1:0) ;
           // VIPER #5093 : Set constraint scope as present scope before parsing
           // constraint block, so that scopes created inside contraint block become
           // child scopes of constraint scope.
           if (constraint_scope) present_scope = constraint_scope ;
      }  constraint_blocks "}"
      {
        $$ = new VeriConstraintDecl($3,$6,present_scope) ;
        present_scope = $4 ;
        if ($1 == VERI_WITH_DEFAULT) {
            ($$)->SetQualifier(VERI_WITH) ;
            ($$)->SetQualifier(VERI_DEFAULT) ;
        } else {
            ($$)->SetQualifier($1) ;
        }
      }
    ;

opt_static_local_default : { $$ = 0 ; }
    | VERI_STATIC { $$ = VERI_STATIC ; }
    | VERI_LOCAL
       {
          $$ = VERI_LOCAL  ;
          VeriTreeNode tmp_node ;
          tmp_node.Warning("local qualifier before constraint declaration violates IEEE 1800 syntax") ;
       }
    | VERI_DEFAULT
      {
          $$ = VERI_DEFAULT ;
          VeriTreeNode tmp ;
          tmp.Warning("unexpected default, violates IEEE 1800 syntax") ;
      }
    | VERI_WITH VERI_DEFAULT // VCS Compatibility issue (with_def_constraints)
      {
          $$ = VERI_WITH_DEFAULT ;
          VeriTreeNode tmp ;
          tmp.Warning("unexpected with default, violates IEEE 1800 syntax") ;
      }
    ;

constraint_blocks : { $$ = new Array() ; /* create an empty Array so that we knwo that the { } were specified */ }
    | real_constraint_blocks { $$ = $1 ; }
    ;

real_constraint_blocks: constraint_block
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | real_constraint_blocks constraint_block
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
      // VIPER #5248 (foreach/if): Allow out-of-LRM syntax (bracketed constrain block):
    | "{" real_constraint_blocks "}"
      {
          VeriTreeNode tmp ;
          tmp.Warning("bracketed constraint block violates IEEE 1800 syntax") ;
          $$ = $2 ;
      }
    | real_constraint_blocks "{" real_constraint_blocks "}"
      {
          VeriTreeNode tmp ;
          tmp.Warning("bracketed constraint block violates IEEE 1800 syntax") ;
          $$ = $1 ;
          ($$)->Append($3) ;
          delete ($3) ;
      }
    | error
      { $$ = 0 ; }
    ;

constraint_block : /* expression */
      VERI_SOLVE id_list VERI_BEFORE id_list opt_id_ref /* VIPER #5248 (hard): must be a 'hard' */ ";"
      {
          $$ = new VeriSolveBefore($2,$4) ;
          if ($5) {
              // VIPER #5248 (hard): Check for 'hard' here:
              if (!Strings::compare(($5), "hard")) {
                  ($$)->Error("syntax error near %s", ($5)) ;
              } else {
                  ($$)->Warning("solve id-list before id-list hard violates IEEE 1800 syntax") ;
              }
              // CHECKME: Not included in parse tree (yet):
              Strings::free($5) ;
          }
      }
    | constraint_expr
      { $$ = $1 ; }
    ;

constraint_expr :
      expression_or_dist ";"
      { $$ = $1 ; }
    | VERI_SOFT expression_or_dist ";" // VIPER #8056: support new keyword 'soft'
      { $$ = new VeriUnaryOperator(VERI_SOFT, $2) ; } //  create unary operator with operator typp VERI_SOFT
    // VIPER #3301: Force bison to use precedence of "force_sv_implication" to read '->' in this context before the PSL rule for "->"
    //| expr force_sv_implication "->" constraint_set
    | expr force_sv_implication VERI_RIGHTARROW constraint_set
      { $$ = new VeriBinaryOperator(VERI_RIGHTARROW, $1, $4) ; } // implication
    | VERI_IF "(" expr ")" constraint_set %prec veri_no_else
      { $$ = new VeriIfOperator($3,$5,0) ; }
    | VERI_IF "(" expr ")" constraint_set VERI_ELSE constraint_set
      { $$ = new VeriIfOperator($3,$5,$7) ; }
    | VERI_FOREACH { push_scope(0) ; } "(" foreach_array_ref_loop_vars ")" extended_constraint_set // Std 1800 addition
      { $$ = new VeriForeachOperator(($4).name, ($4).array, $6, pop_scope()) ; }
    | VERI_DISABLE VERI_SOFT hier_ref ";" // VIPER #8056: support 'disable soft'
      { $$ = new VeriUnaryOperator(VERI_DISABLE, $3) ; } // create unary operator with operator type VERI_DISABLE
    ;

force_sv_implication : { $$ = yychar ; } %prec veri_sv_implication
    | VERI_FORCE_READ { VERIFIC_ASSERT(0) ; } // non-existing token can never match
    ;

  // VIPER 5407, 4161, 3837, 2666 :
  // For 'foreach_array_ref' Draft 9 2009 SV LRM allows ANY hierarchical name, including [dimension]'s in prefix
  // So parse "array_ref [ loop_variables ] " for a FOREACH construct in one rule, to avoid conflicts with []'s in hier_ref name.
  // Need to make sure to parse 'expr' explicitly here as first argument, since there is overlap with parsing [dimension] in hier_ref.
  // VIPER 4110 : make sure to declare the loop variables before we leave this rule (before loop body starts). Otherwise loop body will resolve incorrect identifiers.
foreach_array_ref_loop_vars :
      //hier_ref "[" expr "]"
      hier_ref dimensions
      {
        // VIPER #8003: Support the following foreach loop variable index list syntax:
        // foreach (arr[i]) begin
        //     foreach (arr[i][j])
        //         foreach (arr[i][j, k])
        //         end
        //     end
        // end
        // Here we added rule for dimensions to foreach_array_ref_loop_vars.
        // Here we declare
        // 1) the loop variable at the last index for syntax arr[i][j]: eg. j for arr[i][j].
        // 2) the loop variables for syntax arr[i. j].
        // Now we craete a IndexedName/IndexedMemoryName with hier_ref and dimension.
        // Then set this name to name field and loopvars array to array field for foreach_array_ref_loop_vars.
        VeriName *prefix = $1 ;
        VeriRange *dim = ($2).range ;
        Array *loopvars = ($2).array ;

        if (!dim) ($$).name = prefix ;

        // Always declare the last loop indexed variable.
        // Error out if we have right expression.
        // Collect the left expressions of each dimension.
        // Then create indexed/memory indexed name with prefix(hier_ref) and
        // loop indexes(removing the last loop index) and declare the last loop index
        // (if it is not already declared as in case of foreach(arr[i,j]).
        // These two situations are as follows:
        // If we have dimension but no loopvars ie. we have situation like
        // foreach(arr[i][j]) we declare j as loop index variable and
        // create a indexed name with i and prefix(hier_ref).
        // If we have both dimension and loop variables ie. we have
        // situation like foreach(arr[i][j,k]), we create a indexed name with i.
        VeriRange *tmp_dim = dim ;
        VeriTreeNode tmp_node ;
        Array dim_arr ;
        while(tmp_dim) {
            VeriExpression *right = tmp_dim->GetRight() ;
            if (right) {
                tmp_node.Error("syntax error near %s", ":") ;
            }
            dim_arr.InsertLast(tmp_dim->TakeLeft()) ;
            tmp_dim = tmp_dim->GetNext() ;
        }

        if (dim && !loopvars) {
            VeriExpression *loop_var = dim_arr.Size() ? (VeriExpression *)dim_arr.RemoveLast() : 0 ;
            loopvars = new Array() ;
            if (loop_var) loopvars->InsertFirst(declare_loop_var_id(loop_var)) ;
        }
        ($$).array = loopvars ;

        if (dim_arr.Size() == 1) {
            VeriExpression *index = (VeriExpression *)dim_arr.RemoveLast() ;
            VeriIndexedId *indexed_name = new VeriIndexedId(prefix, index) ;
            ($$).name = indexed_name ;
        }
        if (dim_arr.Size() > 1) {
            Array *indexes = new Array(dim_arr) ;
            VeriIndexedMemoryId *indexed_name = new VeriIndexedMemoryId(prefix, indexes) ;
            ($$).name = indexed_name ;
        }
        delete dim ;
      }
    ;

loop_variables :
       { $$ = new Array() ; if ($$) ($$)->InsertLast(0) ; }
    | id_ref
       { $$ = new Array() ; if ($$) ($$)->InsertLast(declare_loop_var_id($1)) ; }
    | loop_variables ","
       { $$ = $1 ; if ($$) $$->InsertLast(0) ; }
    | loop_variables "," id_ref
       { $$ = $1 ; if ($$) ($$)->InsertLast(declare_loop_var_id($3)) ; }
   ;

extended_constraint_set :
     constraint_set
   | VERI_SOLVE id_list VERI_BEFORE id_list opt_id_ref /* VIPER #5248 (hard): must be a 'hard' */ ";"
      {
          $$ = new VeriSolveBefore($2,$4) ;
          if ($$) $$->Warning("solve-before in foreach violates IEEE 1800 syntax") ;
          if ($5) {
              // VIPER #5248 (hard): Check for 'hard' here:
              if (!Strings::compare(($5), "hard")) {
                  ($$)->Error("syntax error near %s", ($5)) ;
              } else {
                  ($$)->Warning("solve id-list before id-list hard violates IEEE 1800 syntax") ;
              }
              // CHECKME: Not included in parse tree (yet):
              Strings::free($5) ;
          }
      }
   ;

constraint_set :
      constraint_expr
      // VIPER #3301: Support bracketed constraint set expressions.
      // Note: The following rule collides with concat and multi-concat and multi-exprs
      // because we have moved some PSL specific expressions in the same level as concat
      // to solve another bison conflict with SV and PSL. That yields this new conflict.
      // Please see the rule 'sere_beyond_expr' and the comment there for more details.
      // So, it will not work without pre-processing and if both PSL and SV are enabled.
    | "{" brac_constraint_exprs "}"
      { $$ = new VeriConstraintSet($2) ; }
    ;

brac_constraint_exprs :
      constraint_exprs
    | "{" brac_constraint_exprs "}"
      {
        $$ = $2 ;
        if ($2) {
            VeriTreeNode tmp ;
            tmp.Warning("extra braces around constraint expressions violates IEEE 1800 syntax") ; // Viper 5856
        }
      }
    ;

constraint_exprs :
      constraint_block
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | constraint_exprs constraint_block
      {
        $$ = $1 ; if ($$) ($$)->InsertLast($2) ;
        if ($2 && $2->IsSolveBefore()) $2->Warning("solve before expression in constraint set violates IEEE 1800 syntax") ; //Viper 5277
      }
    ;

// id_list used for 'solve - before' and 'cross' constructs.
// VIPER 5248 : LRM allows only simple identifiers, but some simulators allows full primary (with hier name and even method calls with ()'s
id_list : primary
      { $$ = new Array() ; ($$)->InsertLast($1) ;
        if (($1) && ($1)->IsHierName()) {
            ($1)->Warning("hierarchical name in identifier list violates IEEE 1800 syntax") ;
        }
      }
    | id_list "," primary
      { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ;
        if (($3) && ($3)->IsHierName()) {
            ($3)->Warning("hierarchical name in identifier list violates IEEE 1800 syntax") ;
        }
      }
    | error
      { $$ = 0 ; }
    ;

dist_list : dist_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | dist_list "," dist_item
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

dist_item : value_range ":=" expr
      { $$ = new VeriBinaryOperator(VERI_COLON_EQUAL, $1, $3) ; }
    // VIPER #5248 : VCS Compatibility issue (9000388571) : Allow space between
    // colon and '='
    | value_range VERI_COLON VERI_EQUAL expr
      { $$ = new VeriBinaryOperator(VERI_COLON_EQUAL, $1, $4) ;
        if ($$) $$->Warning("space between colon and equal in dist operator violates IEEE 1800 syntax") ;
      }
   /* Token ":/" collides with normal verilog "<case_item>:// some comment". (Causes regressions in normal Verilog) */
//    | value_range ":/" expr
//      { $$ = new VeriBinaryOperator(VERI_COLON_NEQUAL, $1, $3) ; }
    | value_range VERI_COLON VERI_DIV expr // Parse ":/" as two tokens ":" and "/" until VCS supports constraint decls.
      { $$ = new VeriBinaryOperator(VERI_COLON_NEQUAL, $1, $4) ; }
    | value_range
      { $$ = $1 ; }
    ;

value_range :
      expr
      { $$ = $1 ; }
    | "[" expr ":" expr "]"
      { $$ = new VeriRange($2,$4,0) ; }
    | "[" VERI_DOLLAR ":" expr "]" // #3499 : Consider '$' as bounds of value_range
      { $$ = new VeriRange(new VeriDollar(), $4, 0) ; }
    | "[" expr ":" VERI_DOLLAR "]" // #3499 : Consider '$' as bounds of value_range
      { $$ = new VeriRange($2,new VeriDollar(),0) ; }
    | "[" VERI_DOLLAR ":" VERI_DOLLAR "]" //vcs-verific incompatibility
      {
        $$ =  new VeriRange(new VeriDollar(),new VeriDollar(),0) ;
        if ($$) ($$)->Warning("open value range with both bounds as '$' violates IEEE 1800 syntax") ;
      }
    ;

value_range_list : // used for 'inside' operation.
      value_range
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | value_range_list "," value_range
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

/******************************* Declarations *********************/

data_decl :
      variable_decl
// System Verilog adds qualifiers (lifetime and constant), and a type_decl :
    | lifetime variable_decl
      { $$ = $2 ; if ($$) ($$)->SetQualifier($1) ; update_start_linefile($$) ; }
    | VERI_CONST variable_decl // constant_decl (actually a bit more restrictive:single id allowed only)
      { $$ = $2 ; if ($$) ($$)->SetQualifier(VERI_CONST) ; update_start_linefile($$) ; }
    | VERI_CONST lifetime variable_decl // 1800 Addition : Allow both 'const' and 'static'/'automatic' qualifier
      { $$ = $3 ; if ($$) ($$)->SetQualifier(VERI_CONST) ; if ($$) ($$)->SetQualifier($2) ; update_start_linefile($$) ; }
    | lifetime VERI_CONST variable_decl // 1800 Addition : Allow both 'const' and 'static'/'automatic' qualifier
      {
        // VIPER #8007: Support lifetime before const in data declaration in vcs relaxed checking mode:
        $$ = $3 ; if ($$) ($$)->SetQualifier(VERI_CONST) ; if ($$) ($$)->SetQualifier($1) ; update_start_linefile($$) ;
        if (($$) && !VeriNode::InRelaxedCheckingMode()) {
            ($$)->Warning("unexpected %s, violates IEEE 1800 syntax", VeriNode::PrintToken(VERI_CONST)) ;
        }
      }
    | VERI_CONST VERI_VAR lifetime variable_decl // 1800 Addition : Allow both 'const' and 'static'/'automatic' qualifier
      { $$ = $4 ; if ($$) ($$)->SetQualifier(VERI_CONST) ; if ($$) ($$)->SetQualifier($3) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; update_start_linefile($$) ; }
    |  VERI_VAR lifetime variable_decl // 1800 Addition : Allow both 'const' and 'static'/'automatic' qualifier
      { $$ = $3 ; if ($$) ($$)->SetQualifier($2) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; update_start_linefile($$) ; }
    | type_decl
    | net_type_decl
    | package_import_declaration // 3.1a
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | VERI_VIRTUAL VERI_INTERFACE hier_ref list_of_variable_ids ";" // 1800
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(VERI_VIRTUAL, $3), $4) ; }
    | VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name list_of_variable_ids ";" // 1800
      { VeriName *type_name = $3 ? $3->SetParamValues($4): $3 ; if ($5 && type_name) type_name->SetModportName($5) ; $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(VERI_VIRTUAL, type_name), $6) ; }
    | VERI_VIRTUAL hier_ref list_of_variable_ids ";" // 1800
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(VERI_VIRTUAL, $2), $3) ; }
    | VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name list_of_variable_ids ";" // 1800
      { VeriName *type_name = $2 ? $2->SetParamValues($3): $2 ; if ($4 && type_name) type_name->SetModportName($4) ; $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(VERI_VIRTUAL, type_name), $5) ; } // VIPER #5388: Use proper rule number in constructor
    ;

// VIPER #5710: VCS Compatibility issue (9000388243): optional modport name
// associated with virtual interface declaration :
opt_dot_name : { $$ = 0 ; }
    | VERI_DOT VERI_ID { $$ = new VeriIdRef($2) ; }
    ;

variable_decl :
       simple_variable_decl
    | hier_ref parameter_value_assignment list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(($1) ? $1->SetParamValues($2): $1,0,0), $3) ; }
     ;

named_type_var_decl :
        hier_ref class_parameter_value_assignment opt_dot_name list_of_variable_ids ";"
        { VeriName *type_name = ($1) ? $1->SetParamValues($2): $1 ; if ($3 && type_name) type_name->SetModportName($3) ; $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(type_name,0,0), $4) ; }
     ;

simple_variable_decl :
       data_type list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_REG, $1, $2) ; }
// variable_decl is a 'plain' variable declaration, without lifetime or const qualifiers.
// allowed qualifiers differ w.r.t. the context in which the variable_decl is used,
// so we set them in the context (data_decl), not here in the basic rule.
      // named data_type. Have to use hier_ref and spell-out the combinations,
      // because there is heavy similarity with named instantiation : module_id id [] ( ..
      // this variable decl :  type_id [] id
      // and with blocking assignment :  hier_ref [] = ..
      // Named data type takes NO signing. Only packed dimensions.
    | hier_ref list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef($1,0,0), $2) ; }
    | hier_ref dimensions list_of_variable_ids ";"
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef($1,0, dim), $3) ; }
    // VIPER #8368, VIPER #8369: : The data type is optional in this case. If a data type is not specified or if only a range and/or signing is specified,
    // then the data type is implicitly declared as logic.
    //| hier_ref parameter_value_assignment list_of_variable_ids ";"
      //{ $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(($1) ? $1->SetParamValues($2): $1,0,0), $3) ; }
    | VERI_VAR list_of_variable_ids ";" // 1800 addition : Addition of this rule to 'data_decl' produces conflict, so move it here
      {
        // $$ = new VeriDataDecl(VERI_REG, 0, $2) ; if ($$) ($$)->SetQualifier(VERI_VAR) ;
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM($2, i, id) {
            if (!id) continue ;
            id->SetKind(VERI_VAR) ;
            id->SetType(VERI_LOGIC) ;
        }
        VeriDataType *data_type =  new VeriDataType(0, 0, 0) ;
        $$ = new VeriDataDecl(VERI_REG, data_type, $2) ; if ($$) ($$)->SetQualifier(VERI_VAR) ;
      }
    //| VERI_VAR tf_port_type list_of_variable_ids ";" // 1800 addition : Addition of this rule to 'data_decl' produces conflict, so move it here
    //  { $$ = new VeriDataDecl(VERI_REG, $2, $3) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    | VERI_VAR signing opt_dimensions list_of_variable_ids ";"
      {
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM($4, i, id) {
            if (!id) continue ;
            id->SetKind(VERI_VAR) ;
            id->SetType(VERI_LOGIC) ;
        }
        VeriDataType *data_type =  new VeriDataType(0, $2, $3) ;
        $$ = new VeriDataDecl(VERI_REG, data_type, $4) ;  if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    | VERI_VAR dimensions list_of_variable_ids ";"
      {
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM($3, i, id) {
            if (!id) continue ;
            id->SetKind(VERI_VAR) ;
            id->SetType(VERI_LOGIC) ;
        }
        VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
        VeriDataType *data_type = new VeriDataType(0, 0, dim) ;
        $$ = new VeriDataDecl(VERI_REG, data_type, $3) ;  if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    | VERI_VAR data_type list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_REG, $2, $3) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    | VERI_VAR hier_ref dimensions list_of_variable_ids ";" // VIPER #3125 : named data type (IEEE SV LRM 4.2 : data type syntax)
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $3) ; $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef($2,0,dim), $4) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    | VERI_VAR hier_ref list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef($2,0,0), $3) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    | VERI_VAR hier_ref class_parameter_value_assignment list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef(($2) ? $2->SetParamValues($3): 0, 0, 0), $4) ; if ($$) ($$)->SetQualifier(VERI_VAR) ; }
    ;

genvar_decl : VERI_GENVAR list_of_genvar_ids ";"
      { $$ = new VeriDataDecl(VERI_GENVAR, 0/*no data type*/, $2) ; }
    ;

net_decl : net_type opt_strength opt_vec_scal list_of_net_ids ";"
      { veri_file::AddPragmas(previous_pragma_table) ;
        set_previous_pragma_table = 0 ;
        $$ = new VeriNetDecl(new VeriDataType($1,0,0),$2,0,$4) ;
        ($$)->SetQualifier($3) ; // vectored/scalared.
      }
    | net_type opt_strength opt_vec_scal signing opt_dimensions opt_delay list_of_net_ids ";"
     {  veri_file::AddPragmas(previous_pragma_table) ;
        set_previous_pragma_table = 0 ;
        $$ = new VeriNetDecl(new VeriDataType($1, $4, $5), $2, $6, $7) ;
        ($$)->SetQualifier($3) ; // vectored/scalared.
     }
    | net_type opt_strength opt_vec_scal dimensions opt_delay list_of_net_ids ";"
     {  veri_file::AddPragmas(previous_pragma_table) ;
        set_previous_pragma_table = 0 ;
        VeriRange *dim = 0 ;
        GET_DIMENSION_RANGE(dim, $4) ;
        $$ = new VeriNetDecl(new VeriDataType($1, 0, dim), $2, $5, $6) ;
        ($$)->SetQualifier($3) ; // vectored/scalared.
     }
    | net_type opt_strength opt_vec_scal delay list_of_net_ids ";"
     {  veri_file::AddPragmas(previous_pragma_table) ;
        set_previous_pragma_table = 0 ;
        $$ = new VeriNetDecl(new VeriDataType($1, 0, 0), $2, $4, $5) ;
        ($$)->SetQualifier($3) ; // vectored/scalared.
     }
    | net_type opt_strength opt_vec_scal data_type opt_delay list_of_net_ids ";"
     {  veri_file::AddPragmas(previous_pragma_table) ;
        set_previous_pragma_table = 0 ;
        $$ = new VeriNetDecl(new VeriNetDataType($1, $4), $2, $5, $6) ;
        ($$)->SetQualifier($3) ; // vectored/scalared.
     }
    | net_type opt_strength opt_vec_scal named_type opt_dimensions opt_delay list_of_net_ids ";"
     {
         veri_file::AddPragmas(previous_pragma_table) ;
         set_previous_pragma_table = 0 ;
         VeriDataType *dtype = create_net_data_type($1, $4, 0, $5) ;
         $$ = new VeriNetDecl(dtype, $2, $6, $7) ;
         ($$)->SetQualifier($3) ; // vectored/scalared.
     }
    ;

parameter_decl :
      // untyped
      VERI_PARAMETER list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_PARAMETER, 0, $2) ; /* don't set a datatype, so that this parameter (no type) will adjust to type of its value */ }
    | VERI_PARAMETER dimensions list_of_param_decls ";"
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriDataDecl(VERI_PARAMETER, new VeriDataType(0,0,dim), $3) ; }
    | VERI_PARAMETER signing opt_dimensions list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriDataType(0,$2,$3), $4) ; }
      // fixed data type
    | VERI_PARAMETER data_type list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_PARAMETER, $2, $3) ; }
      // named data type
    | VERI_PARAMETER hier_ref opt_dimensions list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriTypeRef($2,0,$3), $4) ; }
    | VERI_PARAMETER hier_ref parameter_value_assignment list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriTypeRef(($2) ? ($2)->SetParamValues($3):0,0,0), $4) ; }
      // type-parameter
    | VERI_PARAMETER VERI_TYPE list_of_type_assignments ";"
      { $$ = new VeriDataDecl(VERI_PARAMETER, new VeriDataType(VERI_TYPE,0,0), $3) ; }
    ;

local_parameter_decl :
      // untyped
      VERI_LOCALPARAM list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_LOCALPARAM, 0, $2) ; /* don't set a datatype, so that this parameter (no type) will adjust to type of its value */ }
    | VERI_LOCALPARAM dimensions list_of_param_decls ";"
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriDataDecl(VERI_LOCALPARAM, new VeriDataType(0,0,dim), $3) ; }
    | VERI_LOCALPARAM signing opt_dimensions list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_LOCALPARAM, new VeriDataType(0,$2,$3), $4) ; }
      // fixed data type
    | VERI_LOCALPARAM data_type list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_LOCALPARAM, $2, $3) ; }
      // named data type
    | VERI_LOCALPARAM hier_ref opt_dimensions list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_LOCALPARAM, new VeriTypeRef($2,0,$3), $4) ; }
    | VERI_LOCALPARAM hier_ref parameter_value_assignment list_of_param_decls ";"
      { $$ = new VeriDataDecl(VERI_LOCALPARAM, new VeriTypeRef(($2) ? ($2)->SetParamValues($3): $2,0,0), $4) ; }
      // type-parameter
    | VERI_LOCALPARAM VERI_TYPE list_of_type_assignments ";"
      { $$ = new VeriDataDecl(VERI_LOCALPARAM, new VeriDataType(VERI_TYPE,0,0), $3) ; }
    ;

list_of_param_decls : param_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_param_decls "," param_decl
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ;  }
    ;

param_decl : hier_ref VERI_EQUAL mintypmax_expr // XL accepts a mintypmax_expr here. Issue 1486.
      { $$ = declare_param_id($1, 0, $3); }
    | hier_ref dimensions VERI_EQUAL mintypmax_expr
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_param_id($1, dim, $4) ; }
    | hier_ref VERI_EQUAL VERI_DOLLAR // # 3499: '$' can be default value of parameter
      { $$ = declare_param_id($1,0, new VeriDollar()) ; }
      // VIPER #3128 : Allow type parameter without the 'type' keyword
    | hier_ref VERI_EQUAL data_type_without_type_ref // fixed data type, type_reference included in mintypmax_expr -> expr -> primary
      { $$ = declare_param_id($1, 0, $3) ; }
      // FIXME: The next rule conflicts with 'expr', we can't create type-ref
      // for type parameter, instead (indexed) id will be created
//    | hier_ref VERI_EQUAL hier_ref opt_dimensions // named data type
//      { $$ = declare_param_id($1, 0, new VeriTypeRef($3,0,$4)) ; }
    | hier_ref VERI_EQUAL hier_ref parameter_value_assignment // named data type
      { $$ = declare_param_id($1, 0, new VeriTypeRef((($3) ? ($3)->SetParamValues($4) : 0), 0, 0)) ; }
    ;

// SV 1800-2009 LRM section 6.20.1 : Parameter can be declared in parameter port list
// without any default value
param_port_decl : param_decl
       { $$ = $1 ; }
    | hier_ref
       { $$ = declare_param_id($1, 0, 0) ; }
    | hier_ref dimensions
       { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_param_id($1, dim, 0) ; }
    ;

io_decl :
      // fixed data type
     direction opt_var port_type_opt list_of_port_ids ";"
      {  $$ = new VeriDataDecl($1, $3, $4) ;
          if ($2) $$->SetQualifier(VERI_VAR) ;
      }
      // named data type :
    | direction opt_var port_type_opt hier_ref opt_dimensions list_of_port_ids ";"
      {
        VeriDataType *dtype = create_net_port_type($3, new VeriTypeRef($4, 0, $5)) ;
        $$ = new VeriDataDecl($1, dtype, $6) ; if ($2) $$->SetQualifier(VERI_VAR) ; /* VIPER #5661*/
      }
    | direction opt_var port_type_opt hier_ref class_parameter_value_assignment list_of_port_ids ";"
      { VeriDataType *dtype = create_net_port_type($3, new VeriTypeRef(($4) ? ($4)->SetParamValues($5): 0, 0, 0)) ;
        $$ = new VeriDataDecl($1, dtype, $6) ; if ($2) $$->SetQualifier(VERI_VAR) ; /* VIPER #5661*/ }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | direction VERI_VIRTUAL VERI_INTERFACE hier_ref list_of_port_ids ";" // SV Std 1800 addition
      { $$ = new VeriDataDecl($1, new VeriTypeRef(VERI_VIRTUAL, $4), $5) ; }
    | direction VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name list_of_port_ids ";" // SV Std 1800 addition
      { VeriName *type_name = $4 ? $4->SetParamValues($5):$4 ; if ($6 && type_name) type_name->SetModportName($6) ; $$ = new VeriDataDecl($1, new VeriTypeRef(VERI_VIRTUAL, type_name), $7) ; }
    | direction VERI_VIRTUAL hier_ref list_of_port_ids ";" // SV Std 1800 addition
      { $$ = new VeriDataDecl($1, new VeriTypeRef(VERI_VIRTUAL, $3), $4) ; }
    | direction VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name list_of_port_ids ";" // SV Std 1800 addition
      { VeriName *type_name = $3 ? $3->SetParamValues($4):$3 ; if ($5 && type_name) type_name->SetModportName($5) ; $$ = new VeriDataDecl($1, new VeriTypeRef(VERI_VIRTUAL, type_name), $6) ; }

// Here is the 'interface_port_declaration' as appearing as an interface item
// This collides exactly with a 'variable_decl' with 'data_type' being a named interface id.
//    | interface_ID list_of_interface_identifiers ";"
//    | interface_ID . modport_ID list_of_interface_identifiers ";"
    ;
port_type_opt : { $$ = 0 ; }
    | port_type
    ;

port_type :
      signing opt_dimensions
      { $$ = new VeriDataType(0, $1, $2) ; }
    | dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $1) ; $$ = new VeriDataType(0, 0, dim) ; }
    | data_type
    | net_type opt_signing opt_dimensions
      { $$ = new VeriDataType($1,$2,$3) ; }
    | net_type data_type
      { $$ = new VeriNetDataType($1, $2) ; }
    ;

tf_port_type :
       signing opt_dimensions
       { $$ = new VeriDataType(0, $1, $2) ; }
     | dimensions
       { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $1) ; $$ = new VeriDataType(0, 0, dim) ; }
     | data_type
     ;

direction : VERI_INPUT { $$ = VERI_INPUT ; }
    | VERI_OUTPUT      { $$ = VERI_OUTPUT ; }
    | VERI_INOUT       { $$ = VERI_INOUT ; }
    | VERI_REF            { $$ = VERI_REF ; }
    | VERI_CONST VERI_REF { $$ = VERI_CONST ; } // VIPER 3267 : need to distinguish between 'ref' and 'const ref'. So, direction VERI_CONST now means 'const ref'.
    ;

list_of_port_ids : port_identifier
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_port_ids "," port_identifier
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

ansi_port_identifier :
      port_identifier
      { $$ = $1 ; }
    | "." VERI_ID "(" opt_expr ")"
      { $$ = declare_net_reg(new VeriNamedPort($2, $4), 0, 1) ; } /* Use 'declare_net_reg' instead of 'declare_port': Both have same body only argument types different*/
    ;

opt_expr : { $$ = 0 ; }
    | expr { $$ = $1 ; }
    ;

port_identifier :
      hier_ref opt_comment
      { $$ = declare_port($1, 0, 0) ; add_pre_comments_to_node($$, $2) ; }
    | hier_ref opt_comment VERI_EQUAL expr_or_tagged_id_expr
      { $$ = declare_port($1, 0, $4) ; add_pre_comments_to_node($$, $2) ; }
    | hier_ref dimensions opt_comment
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_port($1, dim, 0) ; add_pre_comments_to_node($$, $3) ; }
    | hier_ref dimensions wildcard_dimensions opt_comment
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; if (dim && $3) (dim)->SetNext($3) ; $$ = declare_port($1, dim, 0) ; add_pre_comments_to_node($$, $4) ; }
    | hier_ref wildcard_dimensions opt_comment
      { $$ = declare_port($1, $2, 0) ; add_pre_comments_to_node($$, $3) ; }
    | hier_ref dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_port($1, dim, $5) ; add_pre_comments_to_node($$, $3) ; }
    | hier_ref dimensions wildcard_dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; if (dim && $3) (dim)->SetNext($3) ;  $$ = declare_port($1, dim, $6) ; add_pre_comments_to_node($$, $4) ; }
    | hier_ref wildcard_dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      { $$ = declare_port($1, $2, $5) ; add_pre_comments_to_node($$, $3) ; }
      // Issue 2540 : In LRM, Sequence/Property formals are now tf_ports. But LRM forgot that it should still allow event expressions in initial value..
    //| hier_ref opt_comment VERI_EQUAL actual_event_expr // VIPER 2180 : arguments of sequence can be event expression
      //{ $$ = declare_port($1, 0, $4) ; add_pre_comments_to_node($$, $2) ; }
    ;

/******************************* Type Declarations **********************/

type_decl :
      // forward type declaration
      VERI_TYPEDEF forward_type_identifier ";"
      { $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $2) ; }
    | VERI_TYPEDEF VERI_CLASS forward_type_identifier ";" // forward declaration of class
      { $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $3) ; if ($3) ($3)->SetType(VERI_CLASS) ; }
    | VERI_TYPEDEF VERI_INTERFACE VERI_CLASS forward_type_identifier ";" // forward declaration of class
      { $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $4) ; if ($4) ($4)->SetType(VERI_INTERFACE) ; }
    | VERI_TYPEDEF VERI_ENUM forward_type_identifier ";" // forward declaration of enum
      { $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $3) ; if ($3) ($3)->SetType(VERI_ENUM) ; }
    | VERI_TYPEDEF VERI_STRUCT forward_type_identifier ";" // forward declaration of struct
      { $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $3) ; if ($3) ($3)->SetType(VERI_STRUCT) ; }
    | VERI_TYPEDEF VERI_UNION forward_type_identifier ";" // forward declaration of union : VIPER 2258
      { $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $3) ; if ($3) ($3)->SetType(VERI_UNION) ; }
    | VERI_TYPEDEF VERI_COVERGROUP forward_type_identifier ";" // forward declaration of covergroup : VIPER #5248
      {
          $$ = new VeriDataDecl(VERI_TYPEDEF, 0/*no data type*/, $3) ; if ($3) ($3)->SetType(VERI_COVERGROUP) ;
          if ($$) $$->Warning("forward declaration of covergroup violates IEEE 1800 syntax") ;
      }
      // standard type declaration :
    | VERI_TYPEDEF data_type type_identifier ";"
      { $$ = new VeriDataDecl(VERI_TYPEDEF, $2, $3) ; }
      // typedef with named type (as always, cannot have signing, only dimensions)
    | VERI_TYPEDEF VERI_CLASS VERI_ID type_identifier ";"
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef($3,0,0), $4) ; if ($4) ($4)->SetType(VERI_CLASS) ; }
    | VERI_TYPEDEF hier_ref opt_dimensions type_identifier ";"
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef($2, 0, $3), $4) ; }
    | VERI_TYPEDEF hier_ref parameter_value_assignment type_identifier ";"
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef(($2) ? ($2)->SetParamValues($3): 0, 0, 0), $4) ; }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | VERI_TYPEDEF VERI_VIRTUAL VERI_INTERFACE hier_ref type_identifier ";" // SV Std 1800 addition
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef(VERI_VIRTUAL, $4), $5) ; }
    | VERI_TYPEDEF VERI_VIRTUAL VERI_INTERFACE hier_ref parameter_value_assignment type_identifier ";" // SV Std 1800 addition
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef(VERI_VIRTUAL, $4 ? $4->SetParamValues($5):$4), $6) ; }
    | VERI_TYPEDEF VERI_VIRTUAL hier_ref type_identifier ";" // SV Std 1800 addition
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef(VERI_VIRTUAL, $3), $4) ; }
    | VERI_TYPEDEF VERI_VIRTUAL hier_ref parameter_value_assignment type_identifier ";" // SV Std 1800 addition
      { $$ = new VeriDataDecl(VERI_TYPEDEF, new VeriTypeRef(VERI_VIRTUAL, $3 ? $3->SetParamValues($4):$3), $5) ; }
    ;

net_type_decl :
      VERI_NETTYPE data_type type_identifier ";"
      { $$ = new VeriDataDecl(VERI_NETTYPE, $2, $3) ; }
    | VERI_NETTYPE data_type type_identifier VERI_WITH hier_ref ";"
      { $$ = new VeriDataDecl(VERI_NETTYPE, $2, $3, $5) ; }
    | VERI_NETTYPE hier_ref type_identifier ";"
      { $$ = new VeriDataDecl(VERI_NETTYPE, new VeriTypeRef($2, 0, 0), $3) ; }
    | VERI_NETTYPE hier_ref type_identifier VERI_WITH hier_ref ";"
      { $$ = new VeriDataDecl(VERI_NETTYPE, new VeriTypeRef($2, 0, 0), $3, $5) ; }
    ;

forward_type_identifier :
      hier_ref // forward declaration does not allow unpacked dimensions
      { $$ = declare_type_id($1, 0 /* no dimension */) ; }
    ;

type_identifier :
      hier_ref //opt_dimensions
      { $$ = declare_type_id($1, 0) ; }
    | hier_ref dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_type_id($1, dim) ; }
    | hier_ref dimensions wildcard_dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; if (dim && $3) (dim)->SetNext($3); $$ = declare_type_id($1, dim) ; }
    | hier_ref wildcard_dimensions
      { $$ = declare_type_id($1, $2) ; }
    ;

list_of_type_assignments :
      type_assignment
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_type_assignments "," type_assignment
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

// Type-Parameter (type variable) decl :
type_assignment :
      VERI_ID VERI_EQUAL data_type // fixed data type
      { $$ = declare_id(new VeriParamId($1)) ; if ($$) ($$)->SetInitialValue($3) ; /* Type-parameter identifier and assignment */ }
    | VERI_ID VERI_EQUAL hier_ref opt_dimensions // named data type
      { $$ = declare_id(new VeriParamId($1)) ; if ($$) ($$)->SetInitialValue(new VeriTypeRef($3,0,$4)) ; }
    | VERI_ID VERI_EQUAL hier_ref parameter_value_assignment // named data type
      { $$ = declare_id(new VeriParamId($1)) ; if ($$) ($$)->SetInitialValue(new VeriTypeRef(($3) ? ($3)->SetParamValues($4) : 0, 0, 0)) ; }
    | VERI_ID VERI_EQUAL system_function_call
      { $$ = declare_id(new VeriParamId($1)) ; if ($$) ($$)->SetInitialValue($3) ;
        if ($$) $$->Warning("system function call as initial value of type parameter violates IEEE 1800 syntax") ;
      }
    // VCS Compatibility issue (virtual_interface) : Allow virtual interface as
    // initial value of type parameter
    | VERI_ID VERI_EQUAL VERI_VIRTUAL hier_ref
      { $$ = declare_id(new VeriParamId($1)) ; if ($$) ($$)->SetInitialValue(new VeriTypeRef(VERI_VIRTUAL, $4)) ; }
    | VERI_ID VERI_EQUAL VERI_VIRTUAL VERI_INTERFACE hier_ref
      { $$ = declare_id(new VeriParamId($1)) ; if ($$) ($$)->SetInitialValue(new VeriTypeRef(VERI_VIRTUAL, $5)) ; }
    ;

type_port_assignment : type_assignment
    | VERI_ID
      {
          $$ = declare_id(new VeriParamId($1)) ;
          if ($$) {
              if (VeriNode::IsVeri95() || VeriNode::IsVeri2001() || VeriNode::IsSystemVeri2005()) {
                  // VIPER #6222: Only IEEE 1800 (2009) allow param declaration without initial value:
                  ($$)->Error("parameter initial value cannot be omitted in this mode of verilog") ;
              } else {
                  // VIPER #6222: Accept parameter declaration without initial value in IEEE 1800 (2009) mode:
                  // But do produce a warning, because all tools produce error, with this warning
                  // in place, user may easily upgrade it to error and match other tools.
                  ($$)->Warning("initial value of parameter %s is omitted", ($$)->Name()) ;
              }
          }
      }
    ;

/******************************* Data Types *********************/

data_type : data_type_without_type_ref
    | type_reference
    ;

// VIPER #3569: Define a rule for data type without type-reference, we will use it whenever data type is used in the same level as expr.
data_type_without_type_ref : // merging the old 'reg' type with other variable types
// 'simple_type' includes various pre-defined (keyword) types, with different restrictions on signing and dimension(s).
// But we don't want to syntax error out on these restriction, so will do checks in the VeriDataType constructor or resolving.
      simple_type opt_signing opt_dimensions
      { $$ = new VeriDataType($1,$2,$3) ; }
    | VERI_EVENT
      { $$ = new VeriDataType(VERI_EVENT,0,0) ; } // All Event identifiers are now 'VeriVariable' with type VERI_EVENT.
//    | type_id // collides with 'id list_of_ids ;' of component instantiation. Seen to apply 'type_id' as separate rule each time 'data_type' is used.
//        { $$ = 0 ; }
//    | class_scope id_ref // collides with 'expr' in actual parameter list. FIX ME.
//      { $$ = 0 ; }

    | struct_union
    | enum_type
    //| VERI_CHAR opt_signing // A 3.0-only token... // VIPER #8350: Moved this rule under 'simple_type'
    //  { $$ = new VeriDataType(VERI_CHAR,$2,0) ; }
    | VERI_CHANDLE
      { $$ = new VeriDataType(VERI_CHANDLE,0,0) ; }
    //| type_reference // moved to data_type (this is now data_type_without_type_ref)
    | VERI_STRINGTYPE
      { $$ = new VeriDataType(VERI_STRINGTYPE,0,0) ; }  // for SV string datatype and AMS string type parameter!
    ;

struct_union :
     VERI_STRUCT VERI_PACKED opt_signing { push_scope(0, 0, 1/*struct scope*/) ; inside_struct_union = 1 ;} "{" struct_union_member_list "}"  opt_dimensions
      { $$ = new VeriStructUnion(VERI_STRUCT,$3,$8,$6,pop_scope()) ; if ($$) $$->SetPacked() ; inside_struct_union = 0 ; }
    | VERI_UNION opt_tagged VERI_PACKED opt_signing { push_scope(0, 0, 1/*union scope*/) ; inside_struct_union = 1 ; } "{" struct_union_member_list "}" opt_dimensions
      { $$ = new VeriStructUnion(VERI_UNION,$4,$9,$7,pop_scope()) ; if ($$ && $2) $$->SetTagged() ; if ($$) $$->SetPacked() ; inside_struct_union = 0 ; }
    | VERI_STRUCT opt_signing { push_scope(0, 0, 1/*struct scope*/) ; inside_struct_union = 1 ; } "{" struct_union_member_list "}" opt_dimensions
      { $$ = new VeriStructUnion(VERI_STRUCT,$2,$7,$5,pop_scope()) ; inside_struct_union = 0 ; }
    | VERI_UNION  opt_tagged opt_signing { push_scope(0, 0, 1/*union scope*/) ; inside_struct_union = 1 ; } "{" struct_union_member_list "}" opt_dimensions
      { $$ = new VeriStructUnion(VERI_UNION,$3,$8,$6,pop_scope()) ; if ($$ && $2) $$->SetTagged() ; inside_struct_union = 0 ; }
      ;

enum_type :
      // VIPER #5710: (vcs_compatibility-9000388573) LRM-P1800(2009) added a rule for enum type
      // where packed dimension is allowed, so we added 'opt_pkd_dims' to enum_type
      VERI_ENUM "{" enum_decl_list "}" opt_pkd_dims
      { $$ = new VeriEnum(0,0,0,declare_all_enum_ids($3),$5) ; }
    | VERI_ENUM simple_type opt_signing opt_dimensions "{" enum_decl_list "}" opt_pkd_dims
      { $$ = new VeriEnum($2,$3,$4,declare_all_enum_ids($6),$8) ; }
    | VERI_ENUM dimensions "{" enum_decl_list "}" opt_pkd_dims // This one (no element packed data type, but range given) is illegal in 3.1, but suppored in 3.0...?
      {
        VeriRange *dim = 0 ;
        GET_DIMENSION_RANGE(dim, $2) ;
        $$ = new VeriEnum(0,0,dim,declare_all_enum_ids($4),$6) ;
        // VIPER #7004 : Produce warning for LRM violation
        ($$)->Warning("enum without type violates IEEE 1800 syntax") ;
      }
    | VERI_ENUM hier_ref opt_dimensions  "{" enum_decl_list "}" opt_pkd_dims // 1800 addition : type id as enum base type
      { $$ = new VeriEnum(new VeriTypeRef($2, 0, $3), declare_all_enum_ids($5),$7) ; }
// VIPER 5248 : LRM violation, but VCS compliance : enum unsigned {...}
    | VERI_ENUM signing opt_dimensions "{" enum_decl_list "}" opt_pkd_dims
      {
          // VIPER #5248 (Enum_methods2): 'signed' means 'signed int' (2-state):
          $$ = new VeriEnum(VERI_INT,$2,$3,declare_all_enum_ids($5),$7) ;
          ($$)->Warning("enum without type violates IEEE 1800 syntax") ;
      }
// VIPER 5248 : LRM violation, but VCS compliance : enum char {...}
    // | VERI_ENUM VERI_CHAR opt_signing opt_dimensions "{" enum_decl_list "}" opt_pkd_dims // VIPER #8350: Moved this rule under 'simple_type'
    //   {
    //       $$ = new VeriEnum(VERI_CHAR,$3,$4,declare_all_enum_ids($6),$8) ;
    //       ($$)->Warning("enum char violates IEEE 1800 syntax") ;
    //   }
    ;

data_type_or_void : // VIPER #4493 : To allow data_type/void only in struct/union member and function's return type
     data_type
   | VERI_VOID
     { $$ = new VeriDataType(VERI_VOID,0,0) ; }
   ;

type_reference :
      VERI_TYPE "(" expr ")"  // Std 1800 addition
      { $$ = new VeriTypeOperator($3) ; }
    | VERI_TYPE "(" data_type_without_type_ref ")"  // Std 1800 addition, type_reference included in expr -> primary
      { $$ = new VeriTypeOperator($3) ; }
    | VERI_TYPE "(" hier_ref parameter_value_assignment ")"  // Std 1800 addition
      { $$ = new VeriTypeOperator(new VeriTypeRef(($3) ? ($3)->SetParamValues($4): 0, 0, 0)) ; }
    ;

simple_type :
      // integer_atom_types :
      VERI_INTEGER   { $$ = VERI_INTEGER ; }
    | VERI_SHORTINT  { $$ = VERI_SHORTINT ; }
    | VERI_INT       { $$ = VERI_INT ; }
    | VERI_LONGINT   { $$ = VERI_LONGINT ; }
    | VERI_BYTE      { $$ = VERI_BYTE ; }
    | VERI_CHAR  // VIPER #8350
      { $$ = VERI_CHAR ;
          VeriTreeNode tmp ;
          tmp.Warning("use of char as keyword violates IEEE 1800 syntax") ;
      }
      // integer_vector_types :
    | VERI_REG       { $$ = VERI_REG ; }
    | VERI_LOGIC     { $$ = VERI_LOGIC ; }
    | VERI_BIT       { $$ = VERI_BIT ; }
      // non-integer_types :
    | VERI_TIME      { $$ = VERI_TIME ; }
    | VERI_REAL      { $$ = VERI_REAL ; }
    | VERI_REALTIME  { $$ = VERI_REALTIME ; }
    | VERI_SHORTREAL { $$ = VERI_SHORTREAL ; }
    ;

named_type : id_ref { $$ = $1 ; }
    | named_type "::" VERI_ID { $$ = new VeriScopeName($1, $3, 0) ; }
    ;

opt_tagged :      { $$ = 0 ; } // 1800
    | VERI_TAGGED { $$ = VERI_TAGGED ; }
    ;

casting_type:
      simple_type
      { $$ = new VeriDataType($1,0,0) ; }
    // VIPER #4029 : Support class scope as casting type by changing VERI_ID to hier_ref
    //| VERI_ID
    | hier_ref
      { $$ = $1 ; /* normal id-ref: will create type ref from Resolve if needed */ }
    | number
      { $$ = $1 ; /* normal constant : the 'number-of-bits' to cast to */ }
    // VIPER #3826 : Allow string as casting type
    | VERI_STRINGTYPE
      { $$ = new VeriDataType(VERI_STRINGTYPE,0,0) ; }
    | VERI_SIGNED
      { $$ = new VeriDataType(VERI_SIGNED,0,0) ; /* just set signing in data type */ }
    | VERI_UNSIGNED
      { $$ = new VeriDataType(VERI_UNSIGNED,0,0) ; /* just set signing in data type */ }
    | VERI_CONST // SV-2009 (6.24.1) : const as casting type
      { $$ = new VeriDataType(VERI_CONST, 0, 0) ; /* just set const in data type */ }
    | type_reference
      { $$ = $1 ; }
    // VIPER #5095: Size casting can be of any constant primary and constant primary includes
    // genvar-id, parameter, specparam, parenthesised constant expressions etc.
    | multi_concatenation
    | streaming_concatenation // SV Std 1800 addition
      { $$ = $1 ; }
    | "{" "}" // SV Std 1800 addition for empty queue
      { $$ = new VeriConcat(0) ; }
    | casting_type "'" "(" expr ")"
      { $$ = new VeriCast($1,$4) ; }
    | casting_type "'" "{" concat_elements "}" // SV assignment_pattern_expression with type cast
      { $$ = new VeriAssignmentPattern($1,$4) ; }
    | casting_type "'" "{" expr "{" concat_elements "}" "}" // SV assignment_pattern_expression with type cast
      { $$ = new VeriMultiAssignmentPattern($1, $4, $6) ; }
    | VERI_UNSIZED_LITERAL
      { $$ = new VeriConstVal($1, VERI_UNSIZED_LITERAL, 1/*bit-size*/) ; Strings::free($1) ; }
    ;

enum_decl_list : enum_identifier
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | enum_decl_list "," enum_identifier
      { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

enum_identifier :
      enum_decl_id opt_range
      {
          $$ = $1 ;
          if ($2) { $$ = new VeriIndexedId(($1), ($2)) ; }
      }
    | enum_decl_id opt_range VERI_EQUAL expr
      {
          VeriExpression *lhs = $1 ;
          if ($2) { lhs = new VeriIndexedId(($1), ($2)) ; }
          $$ = new VeriBinaryOperator(VERI_EQUAL_ASSIGN, lhs, ($4)) ;
      }
    ;

opt_range :
      { $$ = 0 ; }
    | "[" enum_range_expr ":" enum_range_expr "]"
      { $$ = new VeriRange($2, $4, 0) ; }
    | "[" enum_range_expr "]"
      { $$ = new VeriRange($2, 0, VERI_NONE) ; }
    ;

enum_range_expr :
      number
      { $$ = $1 ; }
    | id_ref
      {
          // VIPER #5248 (RANGE etc): Allow identifier in the enum literal range:
          // VCS only allows the following here:
          //  - All local parameters
          //  - Parameters declared in root scope or packages
          VeriIdDef *id = ($1)->GetId() ;
          if (!id) {
              // Not defined in the currently visible scopes:
              ($1)->Error("%s is not declared", ($1)->GetName()) ;
          } else {
              if (!id->IsParam()) {
                  ($$)->Error("%s is not a constant", ($1)->GetName()) ;
                  id = 0 ; // Non-parameter ids are not allowed
              } else {
                  // Parameters of only package and root scope is allowed:
#ifdef VERILOG_ID_SCOPE_BACKPOINTER
                  // Note: We cannot check imported parameters from a package unless this compile switch is set.
                  VeriScope *owner_scope = id->GetOwningScope() ; // Get the owning scope
                  VeriIdDef *mod_id = (owner_scope) ? owner_scope->GetOwner() : 0 ;
                  if (!mod_id || (!mod_id->IsPackage() && !mod_id->IsRootModule()))
#else
                  // Get the CURRENT root scope:
                  VeriModule *mod = veri_file::GetRootModule() ;
                  VeriScope *root_scope = (mod) ? mod->GetScope() : 0 ;
                  // Get the CURRENT package scope:
                  VeriIdDef *mod_id = (present_scope) ? present_scope->GetContainingModule() : 0 ;
                  VeriScope *pkg_scope = (mod_id && mod_id->IsPackage()) ? mod_id->LocalScope() : 0 ;
                  if ((!root_scope || (root_scope->Find(id->Name()) != id)) && // Not declared in root scope and
                      (!pkg_scope  || (pkg_scope->Find(id->Name())  != id)))   // not declared in a package either
#endif
                  {
                      ($$)->Error("%s is not a constant", ($1)->GetName()) ;
                      id = 0 ; // Not going to evaluate it
                  }
              }
          }
          int n = 0 ;
          if (id) {
              // Evaluate the id-ref now and use the evaluated integer value:
              VeriBaseValue *val = ($1)->StaticEvaluateInternal(0, 0, 0, 0) ;
              //if (!val) ($$)->Error("cannot evaluate %s", id->Name()) ;
              if (val) n = val->GetIntegerValue() ; // Take integer value directly
              delete val ;
              ($1)->Warning("%s in enum literal range violates IEEE 1800 syntax", ($1)->GetName()) ;
          }
          delete ($1) ;
          $$ = new VeriIntVal(n) ;
      }
      // VIPER #5248 (RANGE etc): Allow '?:' operator in the enum literal range:
    | enum_range_expr VERI_QUESTION enum_range_expr VERI_COLON enum_range_expr
      {
          $$ = new VeriIntVal((($1)->Integer()) ? ($3)->Integer() : ($5)->Integer()) ;
          ($1)->Warning("%s in enum literal range violates IEEE 1800 syntax", "question-colon operator") ;
          delete ($1) ; delete ($3) ; delete ($5) ;
      }
      // VIPER #5248 (RANGE etc): Allow only +, -, * and / operators in the enum literal range:
    | enum_range_expr VERI_PLUS enum_range_expr
      {
          $$ = new VeriIntVal(($1)->Integer() + ($3)->Integer()) ;
          ($1)->Warning("%s in enum literal range violates IEEE 1800 syntax", "plus operator") ;
          delete ($1) ; delete ($3) ;
      }
    | enum_range_expr VERI_MIN enum_range_expr
      {
          $$ = new VeriIntVal(($1)->Integer() - ($3)->Integer()) ;
          ($1)->Warning("%s in enum literal range violates IEEE 1800 syntax", "minus operator") ;
          delete ($1) ; delete ($3) ;
      }
    | enum_range_expr VERI_MUL enum_range_expr
      {
          $$ = new VeriIntVal(($1)->Integer() * ($3)->Integer()) ;
          ($1)->Warning("%s in enum literal range violates IEEE 1800 syntax", "multplier operator") ;
          delete ($1) ; delete ($3) ;
      }
    | enum_range_expr VERI_DIV enum_range_expr
      {
          int left = ($1)->Integer() ;
          int right = ($3)->Integer() ;
          $$ = new VeriIntVal((right)?(left/right):0) ;
          ($1)->Warning("%s in enum literal range violates IEEE 1800 syntax", "division operator") ;
          delete ($1) ; delete ($3) ;
      }
    ;

// VIPER #6293: Struct/unit cannot be empty:
struct_union_member_list : struct_union_member
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | struct_union_member_list struct_union_member
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

struct_union_member :
      // Maybe create special purpose IdDef instead of VeriVariable ?
      attr_inst opt_random_qualifier data_type_or_void list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_STRUCT, $3, $4) ; if ($$ && $2) $$->SetQualifier($2) ; }
    | attr_inst opt_random_qualifier hier_ref opt_dimensions list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_STRUCT, new VeriTypeRef($3, 0, $4), $5) ; if ($$ && $2) $$->SetQualifier($2) ; }
    | attr_inst opt_random_qualifier hier_ref parameter_value_assignment list_of_variable_ids ";"
      { $$ = new VeriDataDecl(VERI_STRUCT, new VeriTypeRef(($3) ? ($3)->SetParamValues($4): $3, 0, 0), $5) ; if ($$ && $2) $$->SetQualifier($2) ; }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | attr_inst opt_random_qualifier VERI_VIRTUAL VERI_INTERFACE hier_ref list_of_variable_ids ";" // 1800
      { $$ = new VeriDataDecl(VERI_STRUCT, new VeriTypeRef(VERI_VIRTUAL, $5), $6) ; if ($$ && $2) $$->SetQualifier($2) ; }
    | attr_inst opt_random_qualifier VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name list_of_variable_ids ";" // 1800
      { VeriName *tname = $5 ? $5->SetParamValues($6):$5 ; if ($7 && tname) tname->SetModportName($7) ; $$ = new VeriDataDecl(VERI_STRUCT, new VeriTypeRef(VERI_VIRTUAL, tname), $8) ; if ($$ && $2) $$->SetQualifier($2) ; }
    | attr_inst opt_random_qualifier VERI_VIRTUAL hier_ref list_of_variable_ids ";" // 1800
      { $$ = new VeriDataDecl(VERI_STRUCT, new VeriTypeRef(VERI_VIRTUAL, $4), $5) ; if ($$ && $2) $$->SetQualifier($2) ; }
    | attr_inst opt_random_qualifier VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name list_of_variable_ids ";" // 1800
      { VeriName *tname = $4 ? $4->SetParamValues($5):$4 ; if ($6 && tname) tname->SetModportName($6) ; $$ = new VeriDataDecl(VERI_STRUCT, new VeriTypeRef(VERI_VIRTUAL, tname), $7) ; if ($$ && $2) $$->SetQualifier($2) ; }
    ;

opt_random_qualifier : { $$ = 0 ; }
    | VERI_RAND        { $$ = VERI_RAND ; }
    | VERI_RANDC       { $$ = VERI_RANDC ; }
    ;
    
list_of_defparam_assignments :
      defparam_assignment
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_defparam_assignments "," defparam_assignment
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ;  }
    | error
      { $$ = 0 ; }
    ;

defparam_assignment : hier_ref VERI_EQUAL expr
      { $$ = new VeriDefParamAssign($1,$3) ; }
    ;

list_of_variable_ids :
      variable_identifier
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_variable_ids "," variable_identifier
      { $$ = ($1) ? $1 :  new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

variable_identifier :
      hier_ref opt_comment
      { char *name = extract_net_name($1) ; $$ = name ? declare_net_reg(new VeriVariable(name), 0, 0, $1): 0 ; delete $1 ; add_pre_comments_to_node($$, $2) ; }
    | hier_ref dimensions opt_comment // variable dimensions
      {
          VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,dim), 0, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ;
          }
          delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    | hier_ref opt_comment VERI_EQUAL expr_or_tagged_id_expr
      {
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name), $4, 0, $1) ;
          } else {
              $$ = 0 ;
              delete $4 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $2) ;
      }
    | hier_ref dimensions wildcard_dimensions opt_comment
      {
          VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
          if (dim && $3) dim->SetNext($3) ;
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name, dim), 0, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ;
          }
          delete $1 ; add_pre_comments_to_node($$, $4) ;
      }
    | hier_ref wildcard_dimensions opt_comment
      {
        char *name = extract_net_name($1) ;
        if (name) {
            $$ = declare_net_reg(new VeriVariable(name,$2), 0, 0, $1) ;
        } else {
            $$ = 0 ;
            delete $2 ;
        }
        delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    | hier_ref dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      {
          VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,dim), $5, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ; delete $5 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    | hier_ref dimensions wildcard_dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      {
          VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
          if (dim && $3) dim->SetNext($3) ;
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,dim), $6, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ; delete $6 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $4) ;
      }
    | hier_ref wildcard_dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      {
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,$2), $5, 0, $1) ;
          } else {
              $$ = 0 ;
              delete $2 ; delete $5 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    | hier_ref opt_comment VERI_EQUAL new_expr // id = new ..
      {
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name), $4, 0, $1) ;
          } else {
              $$ = 0 ;
              delete $4 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $2) ;
      }
    | hier_ref dimensions opt_comment VERI_EQUAL new_expr // id [] = new ..
      {
          VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,dim), $5, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ; delete $5 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    | hier_ref dimensions wildcard_dimensions opt_comment VERI_EQUAL new_expr // id [] = new ..
      {
          VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ;
          if (dim && $3) dim->SetNext($3) ;
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,dim), $6, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ; delete $6 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $4) ;
      }
    | hier_ref wildcard_dimensions opt_comment VERI_EQUAL new_expr // id [] = new ..
      {
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name,$2), $5, 0, $1) ;
          } else {
              $$ = 0 ;
              delete $2 ; delete $5 ;
          }
          delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    | hier_ref parameter_value_assignment opt_comment VERI_EQUAL new_expr // type id #(...) = new .. Weird syntax. Probably error in 3.1 LRM. Should be "type #(...) id = new .." ?
      {
          char *name = extract_net_name($1) ;
          if (name) {
              $$ = declare_net_reg(new VeriVariable(name), $5, 0, $1) ; /* FIX ME : store parameter value assignments */
          } else {
              $$ = 0 ;
              delete $5 ;
          }
          unsigned i ; VeriExpression *expr ;
          FOREACH_ARRAY_ITEM($2, i, expr) delete expr ; delete $2 ;
          delete $1 ; add_pre_comments_to_node($$, $3) ;
      }
    ;

list_of_genvar_ids : genvar_decl_id
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_genvar_ids "," genvar_decl_id
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

dimensions : dimension
      {
        // VIPER #8003:
        //$$ = $1 ;
        ($$).range = ($1).range ;
        ($$).array = ($1).array ;
      }
    | dimension dimensions // Make a linked list. Some stack-work for bison.
      {
        // VIPER #8003:
        //$$ = $1 ;
        // Update array field:
        Array *arr1 = ($1).array ;
        Array *arr2 = ($2).array ;
        VeriTreeNode tmp_node  ;
        if(arr1) { // If we have arr1, it is an error.
            // We can only have array field for 'foreach'.
            // So if we have arr1 that means we have foreach(arr[i,j][k])
            // which is a syntax error.
            tmp_node.Error("syntax error near %s", "[") ;
            delete arr1 ;
        }
        ($$).array = arr2 ;

        // Update range field:
        ($$).range = ($1).range ;
        VeriRange *dim1 = ($$).range ;
        VeriRange *dim2 = ($2).range ;
        if (dim1 && dim2) (dim1)->SetNext(dim2) ;
        if (!dim1 && dim2) ($$).range = dim2 ;
      }
    ;

// VIPER #7545: Not required anymore, using opt_index_or_range.
//opt_dimension :
      //{ $$ = 0 ; }
    //| dimension
      //{ $$ = $1 ; }
    //;

//packed_dimensions : packed_dimension
      //{ $$ = $1 ; }
    //| packed_dimension packed_dimensions // Make a linked list. Some stack-work for bison.
      //{ $$ = $1 ; if ($2 && $$) ($$)->SetNext($2) ; }
    //;

//opt_packed_dimensions : { $$ = 0 ; } | packed_dimensions { $$ = $1 ; } ;
opt_dimensions : { $$ = 0 ; } | dimensions { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $1) ; $$ = dim ; } ;

//packed_dimension :
      //"[" expr ":" expr "]"
      //{ $$ = new VeriRange($2,$4,0) ; }
    //| "[" "]"
      //{ $$ = new VeriRange(0,0,VERI_NONE) ; }
    //;

wildcard_dimensions :
      "[* " "]" // consequtive repeat has "[*" token..
      { $$ = new VeriRange(0,0,VERI_MUL) ; }
    | "[* " "]" wildcard_dimensions
      { $$ = new VeriRange(0,0,VERI_MUL) ; if ($3) ($$)->SetNext($3) ; }
    | "[* " "]" dimensions
      { $$ = new VeriRange(0,0,VERI_MUL) ; VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $3) ; if (dim) ($$)->SetNext(dim) ; }
    | "[*]" // consequtive repeat has "[*]" token..
      { $$ = new VeriRange(0,0,VERI_MUL) ; }
    | "[*]" wildcard_dimensions
      { $$ = new VeriRange(0,0,VERI_MUL) ; if ($2) ($$)->SetNext($2) ; }
    | "[*]" dimensions
      { $$ = new VeriRange(0,0,VERI_MUL) ; VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; if (dim) ($$)->SetNext(dim) ; }
    ;

// All types of range and index are now parsed by this rule i.e packed/unpacked dimensions
// of any declarations, index of indexed name (var[9]) and range of part-select (var[9:0])
// Sematic checks on dimensions will be done in the Resolve() routines.
// VIPER #8003: Added two rules to 'dimension', changed 'dimension' to a stucture type
// with range and array fields.  For the rules specified for foreach only we set range field
// to zero and populate array field and on the other hand for all other rules we set array field
// to zero and range field to a new range.
dimension :
      "[" expr ":" expr "]"
      {
          ($$).range = new VeriRange($2,$4,0) ;
          ($$).array = 0 ;
      }
    | "[" expr VERI_PARTSELECT_UP expr "]" // Moved from 'range': can appear in select expression
      {
          ($$).range = new VeriRange($2,$4,VERI_PARTSELECT_UP) ;   /* Verilog 2000 added direction */
          ($$).array = 0 ;
      }
    | "[" expr VERI_PARTSELECT_DOWN expr "]"
      {
          ($$).range = new VeriRange($2,$4,VERI_PARTSELECT_DOWN) ; /* Verilog 2000 added direction */
          ($$).array = 0 ;
      }
      // Here goes 'unpacked' dimensions, 'associative_dimension' and 'variable_dimension'... Might be able to fine-tune...
    | "[" expr "]" // Same as [expr-1:0] ...
      {
          ($$).range = new VeriRange($2,0, VERI_NONE) ; // 'dimension_constant_expression (constantness always checked in Resolve()
          ($$).array = 0 ;
      }
    | "[" bound_expr "]"  // Special bound expression includes '$' (3499)
      {
          ($$).range = new VeriRange($2, 0, VERI_NONE) ;
          ($$).array = 0 ;
      }
    | "[" bound_expr ":" expr "]" // Special bound expression includes '$'  (3499)
      {
          ($$).range = new VeriRange($2,$4, 0) ; /* VIPER #6003 (test3.v): Use $2 to fix leak */
          ($$).array = 0 ;
      }
    | "[" expr ":" bound_expr "]"   // Special bound expression includes '$' (3499)
      {
          ($$).range = new VeriRange($2, $4, 0) ;
          ($$).array = 0 ;
      }
    // VIPER #8221 : Allow $ in both bounds
    | "[" bound_expr ":" bound_expr "]"   // Special bound expression includes '$' (3499)
      {
          ($$).range = new VeriRange($2, $4, 0) ;
          ($$).array = 0 ;
      }
    | "[" "]"
      {
          ($$).range = new VeriRange(0,0,VERI_NONE) ;
          ($$).array = 0 ;
      }
    | "[" "*" "]"
      {
          ($$).range = new VeriRange(0,0,VERI_MUL) ;
          ($$).array = 0 ;
      }
    | "[" hier_ref parameter_value_assignment "]" // Viper #5425: allow parameter override
      {
          ($$).range = new VeriRange(($2)? ($2)->SetParamValues($3):$2,0,VERI_NONE) ;
          ($$).array = 0 ;
      }
    // | "[* " "]" // consequtive repeat has "[*" token.. // Create separate rule 'wildcard_dimension' to avoid conflicts on "indexed_name [* sequence_expr"
    //  { $$ = new VeriRange(0,0,VERI_MUL) ; }
    | "[" data_type_without_type_ref "]" // fixed type ; associative dimension, type_reference included in expr -> primary
      {
          ($$).range = new VeriRange(0,$2,VERI_NONE) ; // store as range expression (with right-side a type-expression) for now.
          ($$).array = 0 ;
      }
    | "[" expr "," loop_variables "]" // VIPER #8003: Added rule for foreach
      {
          ($$).range = 0 ;
          Array *loopvars = $4 ; loopvars->InsertFirst(declare_loop_var_id($2)) ; ($$).array = loopvars ;
      }
    | "[" "," loop_variables "]" // VIPER #8003: Added rule for foreach
      {
          ($$).range = 0 ;
          Array *loopvars = $3 ; loopvars->InsertFirst(0) ; ($$).array = loopvars ;
      }
//    | "[" VERI_ID packed_dimensions "]" // named type ; associative dimension ; covered by 'expr'...mostly...
//      { $$ = new VeriRange(0,0,$2) ; }
    | indexed_range_with_spaces // VIPER #3019: Allow spaces between +/- and :
      {
          ($$).range = $1 ;
          ($$).array = 0 ;
      }
    ;

// VIPER #5710: (vcs_compatibility-9000388573) LRM-P1800(2009) added a rule for enum type
// where packed dimension is allowed, so we added 'opt_pkd_dims' to enum_type
// Rule for packed dimension used in 'enum type':
opt_pkd_dims: { $$ = 0 ; }
    | pkd_dims { $$ = $1 ; }
    ;

pkd_dims: pkd_dim
      { $$ = $1 ; }
    | pkd_dim pkd_dims // Make a linked list. Some stack-work for bison.
      { $$ = $1 ; if ($2 && $$) ($$)->SetNext($2) ; }
    ;

pkd_dim: "[" expr ":" expr "]"
      { $$ = new VeriRange($2,$4,0) ; }
    | "[" "]"
      { $$ = new VeriRange(0,0,VERI_NONE) ; }
    ;

operator : VERI_PLUS  { $$ = VERI_PLUS ; }
    | VERI_MIN        { $$ = VERI_MIN ; }
    ;

bound_expr : VERI_DOLLAR // #3499 : expression to represent bounds of range/index
      { $$ = new VeriDollar() ; }
    | VERI_DOLLAR operator attr_inst expr // Only support '$+1'/'$-1' type syntax
      { $$ = new VeriBinaryOperator($2, new VeriDollar(), $4) ; if ($3) $$->AddAttributes($3) ; }
    | "(" bound_expr ")"
      { $$ = $2 ; }
    ;

// VIPER #3019: Allow spaces between +/- and : for indexed part select with an warning message.
// This issue is in direct conflict with issue 1622. Still, ModelSim accepts this and Verific's
// policy is to be compliant with the most accepting simulators, as long as it makes sense.
indexed_range_with_spaces :
      "[" expr VERI_PLUS ":" expr "]"
      {
          $$ = new VeriRange($2,$5,VERI_PARTSELECT_UP) ;
          ($$)->Warning("indexed range expression contains whitespace between %s and :", "+") ;
      }
    | "[" expr VERI_MIN ":" expr "]"
      {
          $$ = new VeriRange($2,$5,VERI_PARTSELECT_DOWN) ;
          ($$)->Warning("indexed range expression contains whitespace between %s and :", "-") ;
      }
    ;

    // Previously, the rule below was
    // "VERI_PLUS/MIN ":""
    // It treated the operators VERI_PLUS and ":" separately
    // hence allowing expression like "+  :" or even "+ /*...*/ :"
    // VCSI gives error with such expressions. Hence, the rule is
    // changed (pedge_pol) to allow only the specific UP/DOWN tokens "+:" and "-:"
    // VIPER-1622
range :
      "[" expr ":" expr "]"
      { $$ = new VeriRange($2,$4,0) ; }
    | "[" expr VERI_PARTSELECT_UP expr "]"
      { $$ = new VeriRange($2,$4,VERI_PARTSELECT_UP) ;   /* Verilog 2000 added direction */ }
    | "[" expr VERI_PARTSELECT_DOWN expr "]"
      { $$ = new VeriRange($2,$4,VERI_PARTSELECT_DOWN) ; /* Verilog 2000 added direction */ }
    | indexed_range_with_spaces // VIPER #3019: Allow spaces between +/- and :
      { $$ = $1 ; }
    ;

index : "[" expr "]"
      { $$ = $2 ; }
    ;

opt_index_or_range : { $$ = 0 ; }
    | index { $$ = $1 ; }
    | range { $$ = $1 ; }
    ;

net_type : VERI_WIRE   { $$=VERI_WIRE ; }
    | VERI_TRI     { $$=VERI_TRI ; }
    | VERI_TRI1    { $$=VERI_TRI1 ; }
    | VERI_SUPPLY0 { $$=VERI_SUPPLY0 ; }
    | VERI_WAND    { $$=VERI_WAND ; }
    | VERI_TRIAND  { $$=VERI_TRIAND ; }
    | VERI_TRIOR   { $$=VERI_TRIOR ; }
    | VERI_TRI0    { $$=VERI_TRI0 ; }
    | VERI_SUPPLY1 { $$=VERI_SUPPLY1 ; }
    | VERI_WOR     { $$=VERI_WOR ; }
    | VERI_TRIREG  { $$=VERI_TRIREG ; }
    | VERI_UWIRE   { $$=VERI_UWIRE ; }   /* Verilog 2005 addition*/
    | VERI_INTERCONNECT  { $$ = VERI_INTERCONNECT ; }
    ;

opt_vec_scal :
    { $$ = 0 ;
      set_previous_pragma_table = 1 ;
      veri_file::AddPragmas(previous_pragma_table) ;
      previous_pragma_table = veri_file::TakePragmas() ;
    }
    | vec_scal
    { $$ = $1 ;
      set_previous_pragma_table = 1 ;
      veri_file::AddPragmas(previous_pragma_table) ;
      previous_pragma_table = veri_file::TakePragmas() ;
    }
    ;

vec_scal :
      VERI_VECTORED { $$=VERI_VECTORED ; }
    | VERI_SCALARED { $$=VERI_SCALARED ; }
    ;

signing : VERI_SIGNED { $$=VERI_SIGNED ; }
    | VERI_UNSIGNED { $$=VERI_UNSIGNED ; }
    ;

opt_signing : { $$ = 0 ; } | signing { $$ = $1 ; } ;

opt_lifetime : { $$ = 0 ; } | lifetime { $$ = $1 ; } ;

lifetime :
      VERI_AUTOMATIC { $$ = VERI_AUTOMATIC ; }
    | VERI_STATIC    { $$ = VERI_STATIC ; }
    ;

opt_delay :
    { $$ = 0 ;
      if (set_previous_pragma_table) {
        veri_file::AddPragmas(previous_pragma_table) ;
        previous_pragma_table = veri_file::TakePragmas() ;
      }
    }
    | delay
    { $$ = $1 ;
      if (set_previous_pragma_table) {
        veri_file::AddPragmas(previous_pragma_table) ;
        previous_pragma_table = veri_file::TakePragmas() ;
      }
    }
    ;

delay :  // delay is a list of delay values (could be list of mintypmax expressions)
      "#" delay_value
      {  $$ = new Array() ;
         ($$)->InsertLast($2) ;
      }
    | "#" "(" mintypmax_expr_list ")"
      { $$ = $3 ; }
    ;

opt_strength : { $$ = 0 ; }
    | strength
    ;

// Viper #5029 : Support only the valid strength combinations
strength: "(" drive_strength0 "," drive_strength1 ")"
      { $$ = new VeriStrength($2,$4) ; }
    | "(" drive_strength1 "," drive_strength0 ")"
      { $$ = new VeriStrength($2,$4) ; }
    | "(" drive_strength0 "," VERI_HIGHZ1 ")"
      { $$ = new VeriStrength($2,VERI_HIGHZ1) ; }
    | "(" drive_strength1 "," VERI_HIGHZ0 ")"
      { $$ = new VeriStrength($2,VERI_HIGHZ0) ; }
    | "(" VERI_HIGHZ1 "," drive_strength0 ")"
      { $$ = new VeriStrength(VERI_HIGHZ1,$4) ; }
    | "(" VERI_HIGHZ0 "," drive_strength1 ")"
      { $$ = new VeriStrength(VERI_HIGHZ0,$4) ; }
    | "(" drive_val ")"
      { $$ = new VeriStrength($2,0) ; }
    | "(" charge_val ")"
      { $$ = new VeriStrength($2,0) ; }
    ;

drive_strength0: VERI_SUPPLY0 { $$ = VERI_SUPPLY0 ; }
    | VERI_STRONG0   { $$ = VERI_STRONG0 ; }
    | VERI_PULL0     { $$ = VERI_PULL0 ; }
    | VERI_WEAK0     { $$ = VERI_WEAK0 ; }
    ;

drive_strength1: VERI_SUPPLY1   { $$ = VERI_SUPPLY1 ; }
    | VERI_STRONG1   { $$ = VERI_STRONG1 ; }
    | VERI_PULL1     { $$ = VERI_PULL1 ; }
    | VERI_WEAK1     { $$ = VERI_WEAK1 ; }
    ;

drive_val : drive_strength0 { $$ = $1 ; }
    | drive_strength1 { $$ = $1 ; }
    | VERI_HIGHZ1    { $$ = VERI_HIGHZ1 ; }
    | VERI_HIGHZ0    { $$ = VERI_HIGHZ0 ; }
    ;

/*
// Commented out for Viper #5029
strength: "(" drive_val "," drive_val ")"
      { $$ = new VeriStrength($2,$4) ; }
    | "(" drive_val ")"
      { $$ = new VeriStrength($2,0) ; }
    | "(" charge_val ")"
      { $$ = new VeriStrength($2,0) ; }
    ;

drive_val : VERI_SUPPLY0 { $$ = VERI_SUPPLY0 ; }
    | VERI_STRONG0   { $$ = VERI_STRONG0 ; }
    | VERI_PULL0     { $$ = VERI_PULL0 ; }
    | VERI_WEAK0     { $$ = VERI_WEAK0 ; }
    | VERI_SUPPLY1   { $$ = VERI_SUPPLY1 ; }
    | VERI_STRONG1   { $$ = VERI_STRONG1 ; }
    | VERI_PULL1     { $$ = VERI_PULL1 ; }
    | VERI_WEAK1     { $$ = VERI_WEAK1 ; }
    | VERI_HIGHZ1    { $$ = VERI_HIGHZ1 ; }
    | VERI_HIGHZ0    { $$ = VERI_HIGHZ0 ; }
    ;
*/

charge_val : VERI_SMALL  { $$ = VERI_SMALL ; }
    | VERI_MEDIUM    { $$ = VERI_MEDIUM ; }
    | VERI_LARGE     { $$ = VERI_LARGE ; }
    ;

list_of_net_ids :
      net_identifier opt_pragma
      { $$ = new Array() ; ($$)->InsertLast($1) ;
        if ($1 && $2) ($1)->AddAttributes(($1)->PragmasToAttributes(0, $2)) ;
      }
    | list_of_net_ids "," net_identifier opt_pragma
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ;
        if ($3 && $4) ($3)->AddAttributes(($3)->PragmasToAttributes(0, $4)) ;
      }
    | error
      { $$ = 0 ; }
    ;

net_identifier :
      named_type opt_comment
      {
          char *net_name = extract_net_name($1) ;
          $$ = (net_name) ? declare_net_reg(new VeriVariable(net_name), 0, 0, $1): 0 ;
          add_pre_comments_to_node($$, $2) ;
          delete $1 ;
      }
    | named_type dimensions opt_comment // unpacked_dimensions
      {
          char *net_name = extract_net_name($1) ;
          VeriRange *dim = 0 ;
          GET_DIMENSION_RANGE(dim, $2) ;
          if (net_name) {
              $$ = declare_net_reg(new VeriVariable(net_name,dim), 0, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ;
          }
          add_pre_comments_to_node($$, $3) ;
          delete $1 ;
      }
    | named_type opt_comment VERI_EQUAL expr_or_tagged_id_expr
      {
          char *net_name = extract_net_name($1) ;
          if (net_name) {
              $$ = declare_net_reg(new VeriVariable(net_name), $4, 0, $1) ;
          } else {
              $$ = 0 ;
              delete $4 ;
          }
          add_pre_comments_to_node($$, $2) ;
          delete $1 ;
      }
    | named_type dimensions opt_comment VERI_EQUAL expr_or_tagged_id_expr
      {
          char *net_name = extract_net_name($1) ;
          VeriRange *dim = 0 ;
          GET_DIMENSION_RANGE(dim, $2) ;
          if (net_name) {
              $$ = declare_net_reg(new VeriVariable(net_name,dim), $5, 0, $1) ;
          } else {
              $$ = 0 ;
              delete dim ; delete $5 ;
          }
          add_pre_comments_to_node($$, $3) ;
          delete $1 ;
      }
    ;

extended_function_decl :
         // vcs verific incompatibility. function declration can have opt_lifetime in the begining
      lifetime function_decl
      {
          VeriFunctionDecl *f_decl = (VeriFunctionDecl*)($2) ;
          if ($1 && f_decl) {
              f_decl->Warning("unexpected %s, violates IEEE 1800 syntax", VeriNode::PrintToken($1)) ;
              f_decl->SetAutomaticType($1) ;
              if ((($1)==VERI_AUTOMATIC) && f_decl->GetId()) f_decl->GetId()->SetIsAutomatic() ;
          }
          $$ = f_decl ;
      }
    ;

extended_task_decl :
         // vcs verific incompatibility. function declration can have opt_lifetime in the begining
      lifetime task_decl
      {
          VeriTaskDecl *t_decl = (VeriTaskDecl*)($2) ;
          if ($1 && t_decl) {
              t_decl->Warning("unexpected %s, violates IEEE 1800 syntax", VeriNode::PrintToken($1)) ;
              t_decl->SetAutomaticType($1) ;
              if ((($1)==VERI_AUTOMATIC) && t_decl->GetId()) t_decl->GetId()->SetIsAutomatic() ;
          }
          $$ = t_decl ;
      }
    ;

function_decl :
        // have to spell-out the various options before the 'decl_id' (due to SV extentions)
        // FIX ME : in SV, statements_or_null is optional !
        // no data type
      VERI_FUNCTION opt_lifetime opt_signing function_decl_id opt_pragma
      { $<scope>$ = present_scope ; create_method_id($4) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>6 ;
        VeriDataType *func_dtype = ($3) ? new VeriDataType(0,$3,0): 0 ;
        process_out_of_block_method($4, func_scope, $7, $10, func_dtype) ;// Required for out-of-block function
        $$ = new VeriFunctionDecl($2,func_dtype,$4,$7,$10,$11,func_scope) ;
        // Add attributes and pragmas
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the port list
        if ($9) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$9)) ; // pragma's from after the header
      }
    | VERI_FUNCTION opt_lifetime opt_signing dimensions function_decl_id opt_pragma
      { $<scope>$ = present_scope ; create_method_id($5) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>7 ;
        VeriRange *dim = 0 ;
        GET_DIMENSION_RANGE(dim, $4) ;
        VeriDataType *func_dtype = new VeriDataType(0,$3,dim) ;
        process_out_of_block_method($5, func_scope, $8, $11, func_dtype) ;// Required for out-of-block function
        $$ = new VeriFunctionDecl($2,func_dtype,$5,$8,$11,$12,func_scope) ;
        // Add attributes and pragmas
        if ($6) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$6)) ; // pragma's from before the port list
        if ($10) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$10)) ; // pragma's from after the header
      }
      // fixed data type
    | VERI_FUNCTION opt_lifetime opt_signing data_type_or_void function_decl_id opt_pragma
      { $<scope>$ = present_scope ; create_method_id($5) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>7 ;
        VeriDataType *func_dtype = $4 ;
        process_out_of_block_method($5, func_scope, $8, $11, func_dtype) ;// Required for out-of-block function
        // VIPER #4897: Set signing setting before constructor call
        // Add signing to the data_type :
        if ($3 && $4) ($4)->SetSigning($3) ;
        $$ = new VeriFunctionDecl($2,$4,$5,$8,$11,$12,func_scope) ;
        // Add attributes and pragmas
        if ($6) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$6)) ; // pragma's from before the port list
        if ($10) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$10)) ; // pragma's from after the header
      }
      // named data type
    | VERI_FUNCTION opt_lifetime opt_signing hier_ref opt_dimensions function_decl_id opt_pragma // named function type
      { $<scope>$ = present_scope ; create_method_id($6) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>8 ;
        VeriDataType *func_dtype = new VeriTypeRef($4,$3,$5) ;
        process_out_of_block_method($6, func_scope, $9, $12, func_dtype) ;// Required for out-of-block function
        // Actually, signing ($3) is not allowed for named type (I think)..
        $$ = new VeriFunctionDecl($2,func_dtype,$6,$9,$12,$13,func_scope) ;
        // Add attributes and pragmas
        if ($7) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$7)) ; // pragma's from before the port list
        if ($11) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$11)) ; // pragma's from after the header
      }
    // VIPER #3827 : Allow class with parameter overwrite as return type of function
    | VERI_FUNCTION opt_lifetime opt_signing hier_ref parameter_value_assignment function_decl_id opt_pragma // named function type
      { $<scope>$ = present_scope ; create_method_id($6) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>8 ;
        VeriDataType *func_dtype = new VeriTypeRef(($4) ? $4->SetParamValues($5): $4,$3,0) ;
        process_out_of_block_method($6, func_scope, $9, $12, func_dtype) ;// Required for out-of-block function
        // Actually, signing ($3) is not allowed for named type (I think)..
        $$ = new VeriFunctionDecl($2,func_dtype, $6,$9,$12,$13,func_scope) ;
        // Add attributes and pragmas
        if ($7) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$7)) ; // pragma's from before the port list
        if ($11) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$11)) ; // pragma's from after the header
      }
    // VIPER #5388: Allow virtual interface as return type of function
    | VERI_FUNCTION opt_lifetime opt_signing virtual_interface hier_ref opt_dimensions function_decl_id opt_pragma // named function type
      { $<scope>$ = present_scope ; create_method_id($7) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>9 ;
        VeriDataType *func_dtype = new VeriTypeRef($5,$3,$6) ;
        func_dtype->SetVirtualToken() ;
        process_out_of_block_method($7, func_scope, $10, $13, func_dtype) ;// Required for out-of-block function
        // Actually, signing ($3) is not allowed for named type (I think)..
        $$ = new VeriFunctionDecl($2,func_dtype,$7,$10,$13,$14,func_scope) ;
        // Add attributes and pragmas
        if ($8) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$8)) ; // pragma's from before the port list
        if ($12) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$12)) ; // pragma's from after the header
      }
    | VERI_FUNCTION opt_lifetime opt_signing virtual_interface hier_ref parameter_value_assignment function_decl_id opt_pragma // named function type
      { $<scope>$ = present_scope ; create_method_id($7) ; }
         opt_tf_port_decl_list ";" opt_pragma
         function_item_decls opt_statements_or_null VERI_ENDFUNCTION
      {
        within_extern_method = 0 ;
        VeriScope *func_scope = present_scope ;
        present_scope = $<scope>9 ;
        VeriDataType *func_dtype = new VeriTypeRef(($5) ? $5->SetParamValues($6): $5,$3,0) ;
        func_dtype->SetVirtualToken() ;
        process_out_of_block_method($7, func_scope, $10, $13, func_dtype) ;// Required for out-of-block function
        // Actually, signing ($3) is not allowed for named type (I think)..
        $$ = new VeriFunctionDecl($2,func_dtype, $7,$10,$13,$14,func_scope) ;
        // Add attributes and pragmas
        if ($8) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$8)) ; // pragma's from before the port list
        if ($12) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$12)) ; // pragma's from after the header
      }
    ;

virtual_interface : VERI_VIRTUAL { $$ = VERI_VIRTUAL ; }
    | VERI_VIRTUAL VERI_INTERFACE { $$ = VERI_VIRTUAL ; }
    ;

task_decl :
      VERI_TASK opt_lifetime task_decl_id opt_pragma
      { $<scope>$ = present_scope ; create_method_id($3, 1/* task*/) ; }
        opt_tf_port_decl_list ";" opt_pragma
        function_item_decls opt_statements_or_null VERI_ENDTASK
      {
        within_extern_method = 0 ;
        VeriScope *task_scope = present_scope ;
        present_scope = $<scope>5 ;
        process_out_of_block_method($3, task_scope, $6, $9, 0) ; // Required for out-of-block task
        $$ = new VeriTaskDecl($2,$3,$6,$9,$10,task_scope) ;
        // Add attributes and pragmas
        if ($4) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$4)) ; // pragma's from before the port list
        if ($8) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$8)) ; // pragma's from after the header
      }
    ;

opt_tf_port_decl_list :
      { $$ = 0 ; }
    | plain_tf_port_decl_list
      { $$ = $1 ; }
    ;

plain_tf_port_decl_list:
      "(" ")"
      { $$ = new Array() ; }
    | "(" tf_port_decl_list ")"
      { $$ = $2 ; }
    | "(" list_of_port_ids ")" // new in 3.1 : all identifiers have default direction/type. Set here ?
      { $$ = new Array() ; ($$)->InsertLast(new VeriAnsiPortDecl(0/*no direction:input?*/,0,$2)) ; }
    | "(" list_of_port_ids "," tf_port_decl_list ")" // defaults first, then normal tf_ports
      { $$ = new Array() ; ($$)->InsertLast(new VeriAnsiPortDecl(0/*no direction:input?*/,0,$2)) ; ($$)->Append($4) ; delete ($4) ; /* VIPER #6003 (test2.v): Leak fix */ }
    ;

    // VIPER #5710 : VCS Compatibility issue (prop_as_port_in_prop) :
    // Modified rules for sequence/property port declaration as per SV-1800 2009 LRM
opt_property_tf_port_decl_list :
      { $$ = 0 ; }
    | "(" ")"
      { $$ = new Array() ; }
    | "(" ps_port_decl_list ")"
      { $$ = $2 ; }
    //| "(" VERI_LOCAL tf_port_decl_list ")"
      //{
        //$$ = $3 ;
        //VeriTreeNode tmp ;
        //tmp.Warning("unexpected local, violates IEEE 1800 syntax") ;
      //}
    | "(" list_of_ps_port_ids ")" // new in 3.1 : all identifiers have default direction/type. Set here ?
      { $$ = new Array() ; ($$)->InsertLast(new VeriAnsiPortDecl(0/*no direction:input?*/,0,$2)) ; }
    | "(" list_of_ps_port_ids "," ps_port_decl_list ")" // defaults first, then normal tf_ports
      { $$ = new Array() ; ($$)->InsertLast(new VeriAnsiPortDecl(0/*no direction:input?*/,0,$2)) ; ($$)->Append($4) ; delete $4 ; }
    //| "(" list_of_port_ids "," VERI_LOCAL tf_port_decl_list ")" // defaults first, then normal tf_ports
      //{
        //$$ = new Array() ;
        //($$)->InsertLast(new VeriAnsiPortDecl(0/*no direction:input?*/,0,$2)) ;
        //($$)->Append($5) ;
        //VeriTreeNode tmp ;
        //tmp.Warning("unexpected local, violates IEEE 1800 syntax") ;
      //}
    ;
ps_port_decl_list :
      ps_port_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | VERI_LOCAL ps_port_decl
      { $$ = new Array() ; ($$)->InsertLast($2) ; if ($2) ($2)->SetAsLocal() ; }
    | real_attr_inst ps_port_decl
      { $$ = new Array() ; ($$)->InsertLast($2) ; if ($1 && $2) ($2)->AddAttributes($1) ; update_start_linefile($2) ; /* VIPER #7563 */ }
    | real_attr_inst VERI_LOCAL ps_port_decl
      { $$ = new Array() ; ($$)->InsertLast($3) ; if ($1 && $3) ($3)->AddAttributes($1) ; if ($3) ($3)->SetAsLocal() ; update_start_linefile($3) ; /* VIPER #7563 */ }
    | ps_port_decl_list "," ps_port_identifier
      {   // Single port after a ansi_port_decl list : add this id to the last port_decl in the list :
          $$ = $1 ;
          VeriAnsiPortDecl *last_decl = ($$) ? (VeriAnsiPortDecl*)($$)->GetLast() : 0 ;
          if (last_decl) last_decl->AddPortDecl($3) ;
      }
    | ps_port_decl_list "," ps_port_decl
      { $$ = $1 ; if ($$) ($$)->InsertLast($3) ; }
    | ps_port_decl_list "," VERI_LOCAL ps_port_decl
      { $$ = $1 ; if ($$) ($$)->InsertLast($4) ; if ($4) ($4)->SetAsLocal() ; }
    | ps_port_decl_list "," real_attr_inst ps_port_decl
      { $$ = $1 ; if ($$) ($$)->InsertLast($4) ; if ($3 && $4) ($4)->AddAttributes($3) ; update_start_linefile($4, &(@3)) ; /* VIPER #7563 */ }
    | ps_port_decl_list "," real_attr_inst VERI_LOCAL ps_port_decl
      { $$ = $1 ; if ($$) ($$)->InsertLast($5) ; if ($3 && $5) ($5)->AddAttributes($3) ; if ($5) ($5)->SetAsLocal() ; update_start_linefile($5, &(@3)) ; /* VIPER #7563 */ }
    ;

ps_port_decl :
      // We can only parse one 'id' (port_identifier) in this rule.
      // Later ones can be added at next rule up.
      // This is because id's are separated by a comma, and so are tf_port_decl's.
      // untyped :
      direction ps_port_identifier // without port type (default 'reg' in task/functions ?)
      {   $$ = new VeriAnsiPortDecl($1,0,$2) ; }
      // with type :
    | direction ps_port_type ps_port_identifier
      {   $$ = new VeriAnsiPortDecl($1,$2,$3) ; }
    // named type :
    | direction hier_ref opt_dimensions ps_port_identifier
      { $$ = new VeriAnsiPortDecl($1,new VeriTypeRef($2,0,$3),$4) ; }
    | direction hier_ref parameter_value_assignment ps_port_identifier
      { $$ = new VeriAnsiPortDecl($1,new VeriTypeRef(($2) ? ($2)->SetParamValues($3): 0, 0, 0), $4) ; }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | direction VERI_VIRTUAL VERI_INTERFACE hier_ref ps_port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, $4), $5) ; }
    | direction VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name ps_port_identifier // SV Std 1800 addition
      { VeriName *tname = $4 ? $4->SetParamValues($5):$4 ; if ($6 && tname) tname->SetModportName($6) ; $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, tname), $7) ; }
    | direction VERI_VIRTUAL hier_ref ps_port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, $3), $4) ; }
    | direction VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name ps_port_identifier // SV Std 1800 addition
      { VeriName *tname = $3 ? $3->SetParamValues($4):$3 ; if ($5 && tname) tname->SetModportName($5) ; $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, tname), $6) ; }
    // without direction (new in 3.0). But with explicit type
    | ps_port_type ps_port_identifier
      {   $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,$1,$2) ; }
    // named type, without direction
    | hier_ref opt_dimensions ps_port_identifier // also port without direction.
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef($1, 0, $2), $3) ; }
    | hier_ref parameter_value_assignment ps_port_identifier // also port without direction.
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(($1) ? ($1)->SetParamValues($2): 0, 0, 0), $3) ; }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | VERI_VIRTUAL VERI_INTERFACE hier_ref ps_port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, $3), $4) ;  }
    | VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name ps_port_identifier // SV Std 1800 addition
      { VeriName *tname = $3 ? $3->SetParamValues($4):$3 ; if ($5 && tname) tname->SetModportName($5) ; $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, tname), $6) ;  }
    | VERI_VIRTUAL hier_ref ps_port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, $2), $3) ;  }
    | VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name ps_port_identifier // SV Std 1800 addition
      { VeriName *tname = $2 ? $2->SetParamValues($3):$2 ; if ($4 && tname) tname->SetModportName($4) ; $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, tname), $5) ;  }
    ;

list_of_ps_port_ids : ps_port_identifier
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_ps_port_ids "," ps_port_identifier
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;
ps_port_identifier :
      hier_ref
      { $$ = declare_port($1, 0, 0) ; }
    | hier_ref VERI_EQUAL ps_initial_expr
      { $$ = declare_port($1, 0, $3) ; }
    | hier_ref dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_port($1, dim, 0) ; }
    | hier_ref dimensions wildcard_dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; if (dim && $3) (dim)->SetNext($3) ; $$ = declare_port($1, dim, 0) ; }
    | hier_ref wildcard_dimensions
      { $$ = declare_port($1, $2, 0) ; }
    | hier_ref dimensions VERI_EQUAL ps_initial_expr
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = declare_port($1, dim, $4) ; }
    | hier_ref dimensions wildcard_dimensions VERI_EQUAL ps_initial_expr
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; if (dim && $3) (dim)->SetNext($3) ;  $$ = declare_port($1, dim, $5) ; }
    | hier_ref wildcard_dimensions VERI_EQUAL ps_initial_expr
      { $$ = declare_port($1, $2, $4) ; }
      // Issue 2540 : In LRM, Sequence/Property formals are now tf_ports. But LRM forgot that it should still allow event expressions in initial value..
    //| hier_ref VERI_EQUAL actual_event_expr // VIPER 2180 : arguments of sequence can be event expression
      //{ $$ = declare_port($1, 0, $3) ; }
    ;

ps_initial_expr : tagged_id_expr
    //| clocking_event
    | edge_id_expr
    | sequence_expr
    | VERI_DOLLAR { $$ = new VeriDollar() ; } // '$' can be default argument of sequence/property declaration
    ;

ps_port_type :
      signing opt_dimensions
      { $$ = new VeriDataType(0, $1, $2) ; }
    | dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $1) ; $$ = new VeriDataType(0, 0, dim) ; }
    | data_type
    | VERI_SEQUENCE
      { $$ = new VeriDataType(VERI_SEQUENCE, 0, 0) ; }
    | VERI_PROPERTY
      { $$ = new VeriDataType(VERI_PROPERTY, 0, 0) ; }
    | VERI_UNTYPED
      { $$ = new VeriDataType(VERI_UNTYPED, 0, 0) ;  }
    ;

tf_port_decl_list :
      tf_port_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | real_attr_inst tf_port_decl
      { $$ = new Array() ; ($$)->InsertLast($2) ; if ($1 && $2) ($2)->AddAttributes($1) ; update_start_linefile($2) ; /* VIPER #7563 */ }
    | tf_port_decl_list "," port_identifier
      {   // Single port after a ansi_port_decl list : add this id to the last port_decl in the list :
          $$ = $1 ;
          VeriAnsiPortDecl *last_decl = ($$) ? (VeriAnsiPortDecl*)($$)->GetLast() : 0 ;
          if (last_decl) last_decl->AddPortDecl($3) ;
      }
    | tf_port_decl_list "," tf_port_decl
      { $$ = $1 ; if ($$) ($$)->InsertLast($3) ; }
    | tf_port_decl_list "," real_attr_inst tf_port_decl
      { $$ = $1 ; if ($$) ($$)->InsertLast($4) ; if ($3 && $4) ($4)->AddAttributes($3) ; update_start_linefile($4, &(@3)) ; /* VIPER #7563 */ }
    ;

tf_port_decl :
      // We can only parse one 'id' (port_identifier) in this rule.
      // Later ones can be added at next rule up.
      // This is because id's are separated by a comma, and so are tf_port_decl's.
      // untyped :
      direction opt_var port_identifier // without port type (default 'reg' in task/functions ?)
      {   $$ =  new VeriAnsiPortDecl($1,0,$3) ; if ($2) $$->SetQualifier($2) ; }
      // with type :
    | direction opt_var tf_port_type port_identifier
      {   $$ =  new VeriAnsiPortDecl($1,$3,$4) ; if ($2) $$->SetQualifier($2) ; }
    // named type :
    | direction opt_var hier_ref opt_dimensions port_identifier
      { $$ = new VeriAnsiPortDecl($1,new VeriTypeRef($3,0,$4),$5) ; if ($2 && $$) $$->SetQualifier($2) ; }
    | direction opt_var hier_ref parameter_value_assignment port_identifier
      { $$ = new VeriAnsiPortDecl($1,new VeriTypeRef(($3) ? ($3)->SetParamValues($4): 0, 0, 0), $5) ; if ($2 && $$) $$->SetQualifier($2) ; }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | direction VERI_VIRTUAL VERI_INTERFACE hier_ref port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, $4), $5) ; }
    | direction VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name port_identifier // SV Std 1800 addition
      { VeriName *tname = $4 ? $4->SetParamValues($5):$4 ; if ($6 && tname) tname->SetModportName($6) ; $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, tname), $7) ; }
    | direction VERI_VIRTUAL hier_ref port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, $3), $4) ; }
    | direction VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name port_identifier // SV Std 1800 addition
      { VeriName *tname = $3 ? $3->SetParamValues($4):$3 ; if ($5 && tname) tname->SetModportName($5) ; $$ = new VeriAnsiPortDecl($1, new VeriTypeRef(VERI_VIRTUAL, tname), $6) ; }
    // without direction (new in 3.0). But with explicit type
    | opt_var tf_port_type port_identifier
      {   $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,$2,$3) ; if ($1) $$->SetQualifier($1) ; }
    // named type, without direction
    | hier_ref opt_dimensions port_identifier // also port without direction.
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef($1, 0, $2), $3) ; }
    | hier_ref parameter_value_assignment port_identifier // also port without direction.
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(($1) ? ($1)->SetParamValues($2): 0, 0, 0), $3) ; }
    | VERI_VAR hier_ref opt_dimensions port_identifier // 1800 Addition var
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/, new VeriTypeRef($2, 0, $3), $4) ; if ($$) $$->SetQualifier(VERI_VAR) ; }
    | VERI_VAR hier_ref parameter_value_assignment port_identifier // 1800 Addition var
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/, new VeriTypeRef(($2) ? ($2)->SetParamValues($3):0, 0, 0), $4) ; if ($$) $$->SetQualifier(VERI_VAR) ; }
    | VERI_VAR port_identifier  // 1800 Addition
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/, 0, $2)  ; $$->SetQualifier(VERI_VAR) ; }
      // VIPER #4293 : Allow interface_name.modport for virtual interface decl :
    | VERI_VIRTUAL VERI_INTERFACE hier_ref port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, $3), $4) ;  }
    | VERI_VIRTUAL VERI_INTERFACE hier_ref class_parameter_value_assignment opt_dot_name port_identifier // SV Std 1800 addition
      { VeriName *tname = $3 ? $3->SetParamValues($4): $3 ; if ($5 && tname) tname->SetModportName($5) ; $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, tname), $6) ;  }
    | VERI_VIRTUAL hier_ref port_identifier // SV Std 1800 addition
      { $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, $2), $3) ;  }
    | VERI_VIRTUAL hier_ref class_parameter_value_assignment opt_dot_name port_identifier // SV Std 1800 addition
      { VeriName *tname = $2 ? $2->SetParamValues($3):$2 ; if ($4 && tname) tname->SetModportName($4) ; $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,new VeriTypeRef(VERI_VIRTUAL, tname), $5) ;  }
      // VIPER #6423: Port-id is optional for prototype declaraion:
    | direction opt_var tf_port_type
      {
          $$ =  new VeriAnsiPortDecl($1,$3,(Array *)0) ; if ($2) $$->SetQualifier($2) ;
          if (!parsing_prototypes) ($$)->Error("cannot omit port identifier for this declarations") ;
      }
    | opt_var tf_port_type
      {
          $$ = new VeriAnsiPortDecl(0/*no direction:input?*/,$2,(Array *)0) ; if ($1) $$->SetQualifier($1) ;
          if (!parsing_prototypes) ($$)->Error("cannot omit port identifier for this declarations") ;
      }
    ;

opt_var :       { $$ = 0 ; }
    | VERI_VAR  { $$ = VERI_VAR ; }
    ;

function_item_decls :
      { $$ = new Array() ; }
    | function_item_decls function_item_decl
      { $$ = $1 ; ($$)->InsertLast($2) ; }
    ;

// function_item_decl can have attributes and comments around it
function_item_decl : attr_inst pre_comment plain_function_item_decl opt_comment
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
      }
    ;

plain_function_item_decl :
      plain_block_item_decl
    | io_decl
//    | ref_decl // included in io decl
    ;

dpi_import_export : // VIPER #6387: Tree nodes are now properly created for DPI import/export function/tasks.
      VERI_IMPORT VERI_STRING opt_dpi_import_property dpi_function_proto ";"
      { $$ = new VeriDPIFunctionDecl(VERI_IMPORT, ($2), ($3), 0, (VeriFunctionDecl *)($4)) ; }
    | VERI_IMPORT VERI_STRING opt_dpi_import_property VERI_ID VERI_EQUAL dpi_function_proto ";"
      { $$ = new VeriDPIFunctionDecl(VERI_IMPORT, ($2), ($3), ($4), (VeriFunctionDecl *)($6)) ; }
    | VERI_EXPORT VERI_STRING VERI_FUNCTION id_ref ";"
      { push_scope(0) ; $$ = new VeriDPIFunctionDecl(VERI_EXPORT, ($2), 0, 0, ($4), pop_scope()) ; }
    | VERI_EXPORT VERI_STRING VERI_ID VERI_EQUAL VERI_FUNCTION id_ref ";"
      { push_scope(0) ; $$ = new VeriDPIFunctionDecl(VERI_EXPORT, ($2), 0, ($3), ($6), pop_scope()) ; }
      // VIPER #5126: Added grammer for import task:
    | VERI_IMPORT VERI_STRING opt_dpi_import_property dpi_task_proto ";"
      {
        if ($3 == VERI_PURE) {
            // VIPER #5248 (pure): DPI import task cannot have "pure" property, allow it in compatibility mode:
            VeriTreeNode tmp_node ;
            tmp_node.Warning("dpi task import property 'pure' violates IEEE 1800 syntax") ;
        }
        $$ = new VeriDPITaskDecl(VERI_IMPORT, ($2), ($3), 0, (VeriTaskDecl *)($4)) ;
      }
    | VERI_IMPORT VERI_STRING opt_dpi_import_property VERI_ID VERI_EQUAL dpi_task_proto ";"
      {
        if ($3 == VERI_PURE) {
            // VIPER #5248 (pure): DPI import task cannot have "pure" property, allow it in compatibility mode:
            VeriTreeNode tmp_node ;
            tmp_node.Warning("dpi task import property 'pure' violates IEEE 1800 syntax") ;
        }
        $$ = new VeriDPITaskDecl(VERI_IMPORT, ($2), ($3), $4, (VeriTaskDecl *)($6)) ;
      }
    | VERI_EXPORT VERI_STRING VERI_TASK id_ref ";"
      { push_scope(0) ; $$ = new VeriDPITaskDecl(VERI_EXPORT, ($2), 0, 0, ($4), pop_scope()) ; }
    | VERI_EXPORT VERI_STRING VERI_ID VERI_EQUAL VERI_TASK id_ref ";"
      { push_scope(0) ; $$ = new VeriDPITaskDecl(VERI_EXPORT, ($2), 0, ($3), ($6), pop_scope()) ; }
    ;

dpi_function_proto :
      named_function_proto
      { $$ = $1 ; }
    ;

dpi_task_proto :
      named_task_proto
      { $$ = $1 ; }
    ;

opt_dpi_import_property : { $$ = 0 ; }
    | VERI_CONTEXT { $$ = VERI_CONTEXT ; }
    | VERI_PURE    { $$ = VERI_PURE ; }
    ;

opt_closing_label : { $$ = 0 ; } %prec veri_no_colon // Empty for Verilog2k
    | ":" VERI_ID { $$ = $2 ; }
    | ":" VERI_NEW { $$ = Strings::save("new") ; } // Std 1800 addition for class constructor
    ;

statement_label : VERI_ID ":"
      {
          VeriIdDef *label_id = 0 ;
          if (VeriNode::IsSystemVeri2009OrLater()) {
              label_id = new VeriLabelId($1) ;
          } else {
              label_id = new VeriBlockId($1) ;
          }
          $$ = declare_block_id(label_id) ; /* LabelId ? */  push_last_label($$) ; //last_label = $$ ;
          if (VeriNode::IsSystemVeri2009OrLater() && !for_gen_started) {
              // VIPER #8303 : For SV 2009 or later label id creates a named begin end block
              push_scope(0, 1 /* empty scope */) ;
              if (present_scope) present_scope->SetOwner($$) ;
              if ($$) $$->SetScope(present_scope) ;
          }
          if (($$) && !VeriNode::IsSystemVeri()) {
              // VIPER #7492: Label is a SV specific construct, if it is used in non-SV mode, produce error:
              ($$)->Error("syntax error near %s", ":") ;
          }
      }
    ;

/******************************* Modport (interface) declarations **********************/

modport_decl :
      VERI_MODPORT modport_item_list ";"
      { $$ = new VeriModport($2) ; /* a module item */ }
    ;

modport_item_list : modport_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | modport_item_list "," modport_item
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

modport_item :
      // For a modport, we create an id, and give it a list of modport_ports in it declaration list.
      modport_decl_id { push_scope($1) ; } "(" opt_modport_port_decl_list ")"
      {
          // VIPER #5248 (modport_close_brace): Allow empty modport declaration with a warning:
          $$ = new VeriModportDecl($1,$4,pop_scope()) ;
          if (!($4)) ($$)->Warning("empty modport port declaration violates IEEE 1800 syntax") ;
      }
    ;

opt_modport_port_decl_list : { $$ = 0 ; }
    | modport_port_decl_list { $$ = $1 ; }
    ;

// modport_port_decl_list is a list of 'references' :
// Actual declarations must have happened elsewhere : check that in Resolve().
modport_port_decl_list : modport_port_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | real_attr_inst modport_port_decl
      { $$ = new Array() ; ($$)->InsertLast($2) ;  if ($1 && $2) ($2)->AddAttributes($1) ; update_start_linefile($2) ; /* VIPER #7563 */ }
    | modport_port_decl_id
      {
        $$ = new Array() ;
        VeriDataDecl *mp_decl = new VeriDataDecl(VERI_REF /* use 'ref' as direction */,0,$1) ;
        ($$)->InsertLast(mp_decl) ;
        if ($1) ($1)->Warning("modport declaration id without direction not allowed, violates IEEE 1800 syntax") ;
      }
    | modport_port_decl_list "," modport_port_decl_id
      {   $$ = ($1) ? $1 : new Array() ;
          // Single modport_port after a modport_decl list: add this id to the last port_decl in the list:
          // VIPER #5408: Add this to the last data decl (ignore import/export, clocking etc):
          unsigned i ;
          VeriModuleItem *item ;
          FOREACH_ARRAY_ITEM_BACK($1, i, item) { // traverse from back
              if (item && item->IsDataDecl()) {
                  item->AddDataDecl($3) ; // Add the id to this VeriDataDecl
                  $3 = 0 ; // And reset ($3) to indicate that we used it
                  break ; // Done
              }
          }
          if ($3) {
              // VIPER #5408: Could not find any data decl, create a brand new data decl for it:
              VeriDataDecl *mp_decl = new VeriDataDecl(VERI_REF /* use 'ref' as direction */, 0, $3) ;
              mp_decl->SetLinefile(($3)->Linefile()) ;
              ($$)->InsertLast(mp_decl) ; // And add it to the array of the decls
          }
      }
    | modport_port_decl_list "," /* FIXME: Can it have data type for functions? */ id_ref
      {
          // Get the last item of the array:
          VeriModuleItem *last_item = (($1) && ($1)->Size())? (VeriModuleItem*)($1)->GetLast() : 0 ;
          unsigned task = (last_item && last_item->IsTaskDecl()) ? 1 : 0 ; // Last item was task/function?
          $<scope>$ = present_scope ; create_method_id($3, task) ;
      } plain_tf_port_decl_list
      {
          // VIPER #5248 (9000388871): Support for additional export/import task/function declaration:
          $$ = ($1) ? ($1) : new Array(1) ;
          // Get the last item of the array:
          VeriModuleItem *last_item = (($1) && ($1)->Size())? (VeriModuleItem*)($1)->GetLast() : 0 ;
          unsigned task = (last_item && last_item->IsTaskDecl()) ? 1 : 0 ; // Last item was task/function?
          VeriScope *task_scope = present_scope ;
          present_scope = $<scope>4 ;
          VeriModuleItem *decl = 0 ;
          if (task) {
              decl = new VeriTaskDecl(0, $3, $5, 0, 0, task_scope) ;
          } else {
              decl = new VeriFunctionDecl(0, 0 /* data type? */, $3, $5, 0, 0, task_scope) ;
          }
          // Set the qualifier as 'export' or 'import':
          unsigned export_import = 0 ;
          if (last_item && last_item->GetQualifier(VERI_IMPORT)) export_import = VERI_IMPORT ;
          if (last_item && last_item->GetQualifier(VERI_EXPORT)) export_import = VERI_EXPORT ;
          decl->SetQualifier(export_import) ;
          ($$)->InsertLast(decl) ;
          within_extern_method = 0 ;
          VeriTreeNode tmp_node ;
          tmp_node.Warning("continuing %s %s declaration in modport violates IEEE 1800 syntax",
                                  VeriNode::PrintToken(export_import), (task)?"task":"function") ;
      }
    | modport_port_decl_list "," modport_tf_port
      {
        $$ = ($1) ? $1 : new Array() ;
        // Get the last item of the array
        VeriModuleItem *last_item = ($$ && $$->Size())? (VeriModuleItem*)($$)->GetLast() : 0 ;
        // VIPER 2620. Set the qualifier as 'export' or 'import'.
        if (last_item && last_item->GetQualifier(VERI_IMPORT)) ($3)->SetQualifier(VERI_IMPORT) ;
        if (last_item && last_item->GetQualifier(VERI_EXPORT)) ($3)->SetQualifier(VERI_EXPORT) ;
        ($$)->InsertLast($3) ;
      }
    | modport_port_decl_list "," modport_port_decl
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | modport_port_decl_list "," real_attr_inst modport_port_decl
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($4) ;  if ($3 && $4) ($4)->AddAttributes($3) ; update_start_linefile($4, &(@3)) ; /* VIPER #7563 */ }
      // Begin compatibility rules
    | modport_clocking_import_port_decl
      { $$ = $1 ; }
    | real_attr_inst modport_clocking_import_port_decl
      { $$ = $2 ; /* FIXME: apply attributes $1 to all items of $2 and update_start_linefile($2) ; VIPER #7563 */ }
    | modport_port_decl_list "," modport_clocking_import_port_decl
      { $$ = $1 ; if ($$) ($$)->Append($3) ; delete ($3) ; }
    | modport_port_decl_list "," real_attr_inst modport_clocking_import_port_decl
      { $$ = $1 ; if ($$) ($$)->Append($4) ; delete ($4) ; /* FIXME: apply attributes $3 to all items of $4 and update_start_linefile($4, &(@3)) ; VIPER #7563 */ }
      // End compatibility rules
    | error
      // VIPER 3038 : this error rule needed for bison to recover from the rule.
      // Otherwise, the error will go up to modport_item, and will there throw the scope out of whack.
      { $$ = 0 ; }
    ;

modport_port_decl :
      // Each modport_port is actually a newly created 'prototype' id.
      // These need to be resolved (set their real declaration pointer) during 'Resolve'.
      // If not resolved (original does not exist), Resolve will error out.
      // Otherwise, a pointer will be set from within the (modport_port) prototype id to its 'master' definition.
      // 'direction' always is only available from the (portotype) modport_port id.

      // modports_simple_ports_decl
      direction modport_port_decl_id // modport_port (prototype) iddef.
      { $$ = new VeriDataDecl($1,0,$2) ; } // might need a special module item here.
      // modports_hierarchical_ports_decl
//    | VERI_ID "." modport_port_decl_id  // Not allowed as per 1800
//      { $$ = new VeriDataDecl(0,new VeriTypeRef($1,0,0),$3) ; /* Special ModportPort decl ? */ }
//    | VERI_ID "[" expr "]" "." modport_port_decl_id  // Not allowed as per 1800
//      { $$ = new VeriDataDecl(0,new VeriTypeRef($1,0,new VeriRange($3,0,VERI_NONE)),$6) ; /* FIX ME : Special ModportPort decl ? */ }
      // modport_tf_ports_decl
    | import_export modport_tf_port
      { $$ = $2 ; if ($$) ($$)->SetQualifier($1) ; update_start_linefile($$) ; }
    | VERI_CLOCKING modport_port_decl_id //VERI_ID
      //{ $$ = new VeriClockingDecl(declare_id(new VeriClockingId($2)), 0, 0, 0) ; } // VIPER 2620.
      // VIPER #5524 : Create VeriPrototypeId instead of VeriClockingId for clocking
      { $$ = new VeriClockingDecl($2, 0, 0, 0) ; } // VIPER 2620.
    | import_export modport_port_decl_id // SV Std 1800 addition
      {
        // VIPER 3769 :  need to store it somehow. Create a data decl for this one.
        $$ = new VeriDataDecl(0,0,$2) ; // No direction to this 'modportport' (which is a 'function' or 'task' actually?).
        if ($$) ($$)->SetQualifier($1) ; // set import/export as a qualifier, as usual.
      }
    ;

modport_clocking_import_port_decl :
      import_export id_ref VERI_DOTSTAR // For "import clocking_id.*" syntax
      {
          $$ = new Array() ;
          VeriTreeNode tmp_node ;
          if (($1) == VERI_EXPORT) {
              //tmp_node.Error("modports can only export methods") ;
              tmp_node.Error("syntax error near %s", "export") ;
          } else {
              VeriIdDef *clocking_id = ($2) ? ($2)->GetId() : 0 ;
              if (!($2) || !clocking_id || !clocking_id->IsClocking()) {
                  // $2 (id_ref) must be defined and be a clocking id here:
                  tmp_node.Error("%s must be defined as a clocking id", ($2) ? ($2)->GetName() : "<unknown>") ;
              } else {
                  // Expand the .* inplace here and create the actual ports:
                  VeriScope *clocking_scope = clocking_id->LocalScope() ;
                  Map *scope_ids = (clocking_scope) ? clocking_scope->DeclArea() : 0 ;
                  MapIter mi ;
                  VeriIdDef *id ;
                  FOREACH_MAP_ITEM(scope_ids, mi, 0, &id) {
                      if (!id || !id->IsVar()) continue ;
                      // Create a hier-name with the clocking id with this id:
                      VeriIdRef *prefix = new VeriIdRef(Strings::save(($2)->GetName())) ;
                      prefix->SetLinefile(($2)->Linefile()) ;
                      VeriSelectedName *id_name = new VeriSelectedName(prefix, Strings::save(id->Name())) ;
                      id_name->SetLinefile(($2)->Linefile()) ;
                      VeriIdDef *import_id = declare_id(new VeriPrototypeId(id_name)) ;
                      import_id->SetActualId(id) ;
                      ($$)->Insert(new VeriDataDecl(id->Dir(), 0, import_id)) ;
                  }
                  tmp_node.Warning("using %s.%s in modport violates IEEE 1800 syntax", ($2)->GetName(), "*") ;
              }
          }
          delete ($2) ;
      }
    | direction id_ref "." VERI_ID // For "direction clocking_id.var" syntax
      {
          $$ = new Array() ;
          VeriTreeNode tmp_node ;

          VeriIdDef *clocking_id = ($2) ? ($2)->GetId() : 0 ;
          if (!($2) || !clocking_id || !clocking_id->IsClocking()) {
              // $2 (id_ref) must be defined and be a clocking id here:
              tmp_node.Error("%s must be defined as a clocking id", ($2) ? ($2)->GetName() : "<unknown>") ;
          } else {
              // Create the protype-id in-place here and create the actual port:
              VeriScope *clocking_scope = clocking_id->LocalScope() ;
              Map *scope_ids = (clocking_scope) ? clocking_scope->DeclArea() : 0 ;
              VeriIdDef *id = (scope_ids) ? (VeriIdDef *)scope_ids->GetValue($4) : 0 ;
              if (!id || !id->IsVar()) {
                  tmp_node.Error("%s is not declared under prefix %s", ($4), ($2)->GetName()) ;
              } else {
                  // Create a hier-name with the clocking id with this id:
                  VeriIdRef *prefix = new VeriIdRef(Strings::save(($2)->GetName())) ;
                  prefix->SetLinefile(($2)->Linefile()) ;
                  VeriSelectedName *id_name = new VeriSelectedName(prefix, Strings::save(id->Name())) ;
                  id_name->SetLinefile(($2)->Linefile()) ;
                  VeriIdDef *import_id = declare_id(new VeriPrototypeId(id_name)) ;
                  import_id->SetActualId(id) ;
                  if (id->Dir() != $1) {
                      ($2)->Error("modport port direction mismatch") ;
                  } else {
                      tmp_node.Warning("using %s.%s in modport violates IEEE 1800 syntax", ($2)->GetName(), ($4)) ;
                  }
                  ($$)->Insert(new VeriDataDecl(id->Dir(), 0, import_id)) ;
              }
          }
          delete ($2) ;
          Strings::free($4) ;
      }
    | import_export id_ref "." VERI_ID // VIPER #5790: For "import clocking_id.var" syntax
      {
          $$ = new Array() ;
          VeriTreeNode tmp_node ;
          if (($1) == VERI_EXPORT) {
              //tmp_node.Error("modports can only export methods") ;
              tmp_node.Error("syntax error near %s", "export") ;
          } else {
              VeriIdDef *clocking_id = ($2) ? ($2)->GetId() : 0 ;
              if (!($2) || !clocking_id || !clocking_id->IsClocking()) {
                  // $2 (id_ref) must be defined and be a clocking id here:
                  tmp_node.Error("%s must be defined as a clocking id", ($2) ? ($2)->GetName() : "<unknown>") ;
              } else {
                  // Create the protype-id in-place here and create the actual port:
                  VeriScope *clocking_scope = clocking_id->LocalScope() ;
                  Map *scope_ids = (clocking_scope) ? clocking_scope->DeclArea() : 0 ;
                  VeriIdDef *id = (scope_ids) ? (VeriIdDef *)scope_ids->GetValue($4) : 0 ;
                  if (!id || !id->IsVar()) {
                      tmp_node.Error("%s is not declared under prefix %s", ($4), ($2)->GetName()) ;
                  } else {
                      // Create a hier-name with the clocking id with this id:
                      VeriIdRef *prefix = new VeriIdRef(Strings::save(($2)->GetName())) ;
                      prefix->SetLinefile(($2)->Linefile()) ;
                      VeriSelectedName *id_name = new VeriSelectedName(prefix, Strings::save(id->Name())) ;
                      id_name->SetLinefile(($2)->Linefile()) ;
                      VeriIdDef *import_id = declare_id(new VeriPrototypeId(id_name)) ;
                      import_id->SetActualId(id) ;
                      ($$)->Insert(new VeriDataDecl(id->Dir(), 0, import_id)) ;
                      tmp_node.Warning("using %s.%s in modport violates IEEE 1800 syntax", ($2)->GetName(), ($4)) ;
                  }
              }
          }
          delete ($2) ;
          Strings::free($4) ;
      }
    ;

import_export :
      VERI_IMPORT { $$ = VERI_IMPORT ; }
    | VERI_EXPORT { $$ = VERI_EXPORT ; }
    ;

modport_tf_port :
      // this is an element of 'modport_port_decl_list', which is separated by ","'s.
      // So, we cannot parse the additional ","'ed 'named_function_proto' and 'named_task_proto'
      // arguments, since parser does not know if a function or a task, or a new modportport
      // comes up if a "," ID appears after this rule.
      // So we do NOT allow named_function/task proto's as separate items (without the 'import_export' label)
      named_task_proto
      { $$ = $1 ; /* module item to some 'modport_port_decl' item */ }
    | named_function_proto
      { $$ = $1 ; /* module item to some 'modport_port_decl' item */ }
    ;

/* For proto's : should just declare identifier ?. With ports ? */
named_function_proto :
      VERI_FUNCTION opt_lifetime opt_signing function_decl_id
      { $<scope>$ = present_scope ; create_method_id($4) ; parsing_prototypes = 1 ; }
      opt_tf_port_decl_list
      { $$ = new VeriFunctionDecl($2,($3) ? new VeriDataType(0,$3,0): 0,$4,$6,0,0,present_scope) ; present_scope = $<scope>5 ; within_extern_method = 0 ; parsing_prototypes = 0 ; }
    | VERI_FUNCTION opt_lifetime opt_signing dimensions function_decl_id
      { $<scope>$ = present_scope ; create_method_id($5) ; parsing_prototypes = 1 ; }
      opt_tf_port_decl_list  // Support range in return type
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $4) ; $$ = new VeriFunctionDecl($2,new VeriDataType(0,$3,dim),$5,$7,0,0,present_scope) ; present_scope = $<scope>6 ; within_extern_method = 0 ; parsing_prototypes = 0 ; }
    | VERI_FUNCTION opt_lifetime opt_signing data_type_or_void function_decl_id
      { $<scope>$ = present_scope ; create_method_id($5) ; parsing_prototypes = 1 ; }
      opt_tf_port_decl_list // fixed data type
      { $$ = new VeriFunctionDecl($2,$4,$5,$7,0,0,present_scope) ; present_scope = $<scope>6 ; if ($3 && $4) $4->SetSigning($3) ; within_extern_method = 0 ; parsing_prototypes = 0 ; }
    | VERI_FUNCTION opt_lifetime opt_signing hier_ref opt_dimensions function_decl_id
      { $<scope>$ = present_scope ; create_method_id($6) ; parsing_prototypes = 1 ; }
      opt_tf_port_decl_list // named function type
      { $$ = new VeriFunctionDecl($2,new VeriTypeRef($4,$3,$5),$6,$8,0,0,present_scope) ; present_scope = $<scope>7 ; within_extern_method = 0 ; parsing_prototypes = 0 ; }
    | VERI_FUNCTION opt_lifetime opt_signing hier_ref parameter_value_assignment function_decl_id
      { $<scope>$ = present_scope ; create_method_id($6) ; parsing_prototypes = 1 ; }
      opt_tf_port_decl_list // named function type
      { $$ = new VeriFunctionDecl($2,new VeriTypeRef(($4) ? $4->SetParamValues($5) : $4,$3,0),$6,$8,0,0,present_scope) ;  present_scope = $<scope>7; within_extern_method = 0 ; parsing_prototypes = 0 ; }

    //| VERI_FUNCTION signing dpi_function_proto
      //{ $$ = $2 ; /* set signing ? */ }
    ;

named_task_proto :
      VERI_TASK opt_lifetime task_decl_id { $<scope>$ = present_scope ; create_method_id($3, 1) ; parsing_prototypes = 1 ; } opt_tf_port_decl_list
      { $$ = new VeriTaskDecl($2,$3,$5,0,0,present_scope) ; present_scope = $<scope>4 ; within_extern_method = 0 ; parsing_prototypes = 0 ; }
    ;

/******************************* Assertions **********************/

concurrent_assertion_item :
      assert_cover_property_statement
      { $$ = $1 ; /* from VeriStatement to VeriModuleItem */ }
    | VERI_ID ":" assert_cover_property_statement
      {
          // Viper 5777: deferred the definition of statement label
          // so that no conflict is formed.
          $$ = $3 ;
          if ($3) {
              VeriIdDef *label_id = declare_block_id(new VeriBlockId($1)) ; /* LabelId ? */
              ($3)->SetOpeningLabel(label_id) ;
          }
      }
    | concurrent_assertion_item_decl
    ;

concurrent_assertion_item_decl :
      property_decl
    | sequence_decl
    | let_declaration
    ;

property_decl :
      VERI_PROPERTY property_decl_id { within_seq_property = 1 ; push_scope($2) ; } opt_property_tf_port_decl_list ";"  // issue 2540 : property can have a tf_port_decl_list (AnsiPortDecl's)
         assertion_variable_decl_list
         property_spec opt_semi /* semi colon illegal for 'if' and 'case' */
      VERI_ENDPROPERTY
      {
          $$ = new VeriPropertyDecl($2, $4, $6, $7, pop_scope()) ;
          within_seq_property = 0 ;
          if (!($8) && ($7)) {
              // VIPER #6449: 'property_spec' may preceed an event control in which case
              // it will be a VeriClockedSequence expression. It can also be a 'dissble iff'
              // expression in that case it will be a VeriBinaryOperator with operator type
              // set to VERI_DISABLE. So, extract the last expression in those two cases:
              VeriExpression *last_expr = ($7) ;
              if (last_expr && last_expr->IsClockedSequence())        last_expr = last_expr->GetArg() ;
              if (last_expr && (last_expr->OperType()==VERI_DISABLE)) last_expr = last_expr->GetRight() ;
              if (last_expr && !last_expr->IsIfOperator() && !last_expr->IsCaseOperator()) {
                  // No semicolon but the 'property_spec' is not an 'if' or 'case' operator, error
                  VeriTreeNode tmp_node ;
                  tmp_node.Error("syntax error near %s", VeriNode::PrintToken(VERI_ENDPROPERTY)) ;
              }
          }
      }
    ;

sequence_decl :
      VERI_SEQUENCE sequence_decl_id { within_seq_property = 1 ; push_scope($2) ; } opt_property_tf_port_decl_list ";" // issue 2540 : sequence can have a tf_port_decl_list (AnsiPortDecl's)
         assertion_variable_decl_list
         sequence_expr ";"
      VERI_ENDSEQUENCE
      { $$ = new VeriSequenceDecl($2, $4, $6, $7, pop_scope()) ; within_seq_property = 0 ;}
    ;

assert_cover_property_statement :
    // action block expanded for easy parse-trees
    // 'property_inst' is included : property_inst==property_spec->property_expr->sequence_expr->expr->primary->function_call
      VERI_ASSERT property_final "(" property_spec ")" attr_inst ";" // VIPER #7083: Used statement_wo_end_comment instead of statement_or_null
      { $$ = new VeriAssertion(VERI_ASSERT,$2,$4,0,0)  ; }
    | VERI_ASSERT property_final "(" property_spec ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSERT,$2,$4,$6,0)  ; }
    | VERI_ASSERT property_final "(" property_spec ")" statement_wo_end_comment VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSERT,$2,$4,$6,$8)  ; }
    | VERI_ASSERT property_final "(" property_spec ")" VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSERT,$2,$4,0,$7)  ; }
    | VERI_COVER property_final "(" property_spec ")" statement_or_null // cover has no else in action block
      { $$ = new VeriAssertion(VERI_COVER,$2,$4,$6,0)  ; }
      // 3.1a addition : 'assume property' :
      // VIPER #3978: Add action block (including else part) in 'assume property' just like 'assert property':
      // This is according to SV-2008 (draft4) and SV enhancements/bug tracking system Mantis 1460.
      // Also note that ModelSim 6.3 does not support this but VCS Y-2006.06-SP1-18 does.
    | VERI_ASSUME property_final "(" property_spec ")" attr_inst ";" // VIPER #7083: Used statement_wo_end_comment instead of statement_or_null
      { $$ = new VeriAssertion(VERI_ASSUME,$2,$4,0,0)  ; }
    | VERI_ASSUME property_final "(" property_spec ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSUME,$2,$4,$6,0)  ; }
    | VERI_ASSUME property_final "(" property_spec ")" statement_wo_end_comment VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSUME,$2,$4,$6,$8)  ; }
    | VERI_ASSUME property_final "(" property_spec ")" VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSUME,$2,$4,0,$7)  ; }
    | deferred_assertion { $$ = $1 ; }
    | VERI_COVER VERI_SEQUENCE "(" sequence_spec ")" statement_or_null // VIPER #6372
      { $$ = new VeriAssertion(VERI_COVER, VERI_SEQUENCE, $4, $6, 0) ; }
    | VERI_RESTRICT VERI_PROPERTY "(" property_spec ")" ";" // VIPER #6373
      { $$ = new VeriAssertion(VERI_RESTRICT, VERI_PROPERTY, $4, 0, 0) ; }
    ;

property_final :
      VERI_PROPERTY { $$ = VERI_PROPERTY ; }
    | VERI_FINAL
      {
          $$ = VERI_FINAL ;
          VeriTreeNode tmp ;
          tmp.Warning("unexpected final, violates IEEE 1800 syntax") ;
      }
    ;

expect_property_statement : // 3.1a
      VERI_EXPECT "(" property_spec ")" attr_inst ";" // VIPER #7083: Used statement_wo_end_comment instead of statement_or_null
      { $$ = new VeriAssertion(VERI_EXPECT,0,$3,0,0)  ; }
    | VERI_EXPECT "(" property_spec ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriAssertion(VERI_EXPECT,0,$3,$5,0)  ; }
    | VERI_EXPECT "(" property_spec ")" statement_wo_end_comment VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_EXPECT,0,$3,$5,$7)  ; }
    | VERI_EXPECT "(" property_spec ")" VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_EXPECT,0,$3,0,$6)  ; }
    ;

// Adjusted to the System Verilog 3.1a spec, which merges propery_spec and property_expr,
// but still keps "disable iff" separate.
// Also, the whole multi-clocking feature seems to have dis-appeared in 3.1...
// We still keep that in here, put it in 'sequence_expr'.

property_spec :
      property_expr
      { $$ = $1 ; }
    | VERI_DISABLE VERI_IFF "(" expression_or_dist ")" property_expr
      { $$ = new VeriBinaryOperator(VERI_DISABLE, $4, $6) ; }
    | clocking_event VERI_DISABLE VERI_IFF "(" expression_or_dist ")" property_expr
      { $$ = new VeriClockedSequence($1, new VeriBinaryOperator(VERI_DISABLE, $5, $7) ) ; }
    ;

expression_or_dist :
      expr
      { $$ = $1 ; }
// RD: 'dist' moved into expression (to accomodate parenthesized dist constructs :
//    | expr VERI_DIST "{" dist_list "}"
//      { $$ = new VeriDistOperator($1, $4) ; }
    ;

property_expr :
// merged 'property_expr' with 'sequence_expr', since syntactically they have too much overlap.
// Need to resolve semantics by context (for some operators : AND/OR/clocking_event).
      sequence_expr // %prec veri_no_cparen // will reduce to ( property_expr ) instead of ( sequence_expr )
      { $$ = $1 ; }
    ;

sequence_spec : sequence_expr // 3.1a : all (multi)clocking stuff in now inside sequence_expr
      { $$ = $1 ; }
    | VERI_DISABLE VERI_IFF "(" expression_or_dist ")" sequence_expr
      { $$ = new VeriBinaryOperator(VERI_DISABLE, $4, $6) ; }
    | clocking_event VERI_DISABLE VERI_IFF "(" expression_or_dist ")" sequence_expr
      { $$ = new VeriClockedSequence($1, new VeriBinaryOperator(VERI_DISABLE, $5, $7) ) ; }
    ;

sequence_expr :
     mintypmax_expr %prec veri_no_cparen // will reduce to ( sequence_expr ) instead of colliding with ( mintypmax_expr )  (in 'expr' of mintypmax_expr) if there is any doubt
      { $$ = $1 ; }
    | sequence_expr_wo_expr
    ;

sequence_expr_wo_expr :
      "(" sequence_expr ")"
      { $$ = $2 ;
        if ($$) ($$)->SetLinefile(veri_file::GetRuleLineFile()) ; // issue 2598: adjust linefile to include ()'s
        if ($$) ($$)->SetParenthesized() ; // VIPER 5118/2582 : flag expression as 'parenthesized' (effect only if it's an operator).
      }
// From 'property_expr' (merged with sequence_expr, due to heavy overlap in syntax).
    // VIPER #4011 : Do not make the precedence same as 'unary_oper' strength
    // Its precedence is lower than '##', within, intersect etc
    | VERI_NOT sequence_expr //%prec VERI_UNARY_OPER // result (and rhs) MUST be a property_expr. Not sure if precedence is 'unary_oper' strength.
      { $$ = new VeriUnaryOperator(VERI_NOT, $2) ; }
    | sequence_expr VERI_OVERLAPPED_IMPLICATION sequence_expr // result (and rhs) MUST be a property_expr
      {
          // Clock-flow fix for VIPER 5118/2582:
          // Now that clocking event has higher precedence than implication operators, a expression-wide clocking event can end up on the left of a implication operator.
          // To avoid regression, and to mimic SV 'leading clock' flow, move clocking event back from left arg to top of expression :
          // Only do this if the left side (sequence) was not parenthesized (else the clock flow cannot be moved)
          VeriExpression *clocked_seq = $1 ;
          if (clocked_seq && clocked_seq->IsClockedSequence() && !clocked_seq->IsParenthesized()) {
              // swap the clocking event to the top of the expression :
              VeriExpression *expr = clocked_seq->GetArg() ; // get the sequence that was clocked. and use that for the implication
              expr = new VeriBinaryOperator(VERI_OVERLAPPED_IMPLICATION, expr, $3) ;
              clocked_seq->SetArg(expr) ;
              $$ = clocked_seq ;
          } else {
              $$ = new VeriBinaryOperator(VERI_OVERLAPPED_IMPLICATION, $1, $3) ;
          }
      }
    | sequence_expr VERI_NON_OVERLAPPED_IMPLICATION sequence_expr // result (and rhs) MUST be a property_expr
      {
          // Clock-flow fix for VIPER 5118/2582:
          // Now that clocking event has highe precedence than implication operators, a expression-wide clocking event can end up on the left of a implication operator.
          // To avoid regression, and to mimic SV 'leading clock' flow, move clocking event back from left arg to top of expression :
          // Only do this if the left side (sequence) was not parenthesized (else the clock flow cannot be moved)
          VeriExpression *clocked_seq = $1 ;
          if (clocked_seq && clocked_seq->IsClockedSequence() && !clocked_seq->IsParenthesized()) {
              // swap the clocking event to the top of the expression :
              VeriExpression *expr = clocked_seq->GetArg() ; // get the sequence that was clocked. and use that for the implication
              expr = new VeriBinaryOperator(VERI_NON_OVERLAPPED_IMPLICATION, expr, $3) ;
              clocked_seq->SetArg(expr) ;
              $$ = clocked_seq ;
          } else {
              $$ = new VeriBinaryOperator(VERI_NON_OVERLAPPED_IMPLICATION, $1, $3) ;
          }
      }

    | VERI_IF "(" expression_or_dist ")" sequence_expr %prec veri_no_else // // new in 3.1a. result (and both sides) MUST be property_expr.
      { $$ = new VeriIfOperator($3,$5,0) ; }
    | VERI_IF "(" expression_or_dist ")" sequence_expr VERI_ELSE sequence_expr // new in 3.1a. result (and both sides) MUST be property_expr.
      { $$ = new VeriIfOperator($3,$5,$7) ; }
    | VERI_CASE "(" expression_or_dist ")" property_case_item_list VERI_ENDCASE
      { $$ = new VeriCaseOperator($3, $5) ; }

// Cycle delay ( ##(delay) sequence   or   sequence ##(delay) sequence )
    | cycle_delay_range sequence_expr %prec "##"
      { $$ = new VeriSequenceConcat(0,$1,$2) ; /* LRM 17.5 : sequence concat, left side is started by `true (1), or 'true'.. */ }
    | sequence_expr cycle_delay_range sequence_expr %prec "##"
      { $$ = new VeriSequenceConcat($1,$2,$3) ; /* Concat two sequences, with some delay in the middle (left associative?) */ }
// VIPER 2539 : Following rule is no longer present in P1800. Remove it
//  | sequence_expr "##" clocking_event sequence_expr // (clocked)sequence ## (clocked)sequence. Strict 3.1 syntax. But not sure if this is actually allowed, and what the semantcs are..
//    { $$ = new VeriSequenceConcat($1,0,new VeriClockedSequence($3,$4)) ; }

// VIPER 2539 : Assignment in sequence expression without enclosing parenthesis
// is not allowed. According to P1800 any sequence expression can be used before
// local variable assignment. So, commented following two rules and create a new
// rule "(" sequence_expr "," func_blocking_assignment_list ")"
 // | expr "," func_blocking_assignment_list   // covered by following rule
 //   { $$ = new VeriAssignInSequence($1,$3) ; }
 // | expr VERI_DIST "{" dist_list "}" "," func_blocking_assignment_list  // SV 1800
 //   { $$ = new VeriAssignInSequence(new VeriDistOperator($1, $4), $7) ; }
    | "(" sequence_expr "," func_blocking_assignment_list ")"  // covered by following rule
      { $$ = new VeriAssignInSequence($2,$4) ; }

// Sequence/Boolean repetition (boolean_abbrev, sequence_abbrev) left associative ?
    | sequence_expr "[* " const_or_range_expr "]"
      { $$ = new VeriBinaryOperator(VERI_CONSECUTIVE_REPEAT, $1, $3) ; }
    | sequence_expr "[*=" const_or_range_expr "]"
      { $$ = new VeriBinaryOperator(VERI_NON_CONSECUTIVE_REPEAT, $1, $3) ; }
    | sequence_expr "[*->" const_or_range_expr "]"
      { $$ = new VeriBinaryOperator(VERI_GOTO_REPEAT, $1, $3) ; }
    | sequence_expr "[*]" // VIPER #6329 & #6357
      {
          $$ = new VeriBinaryOperator(VERI_CONSECUTIVE_REPEAT, $1, new VeriRange(0,0,VERI_MUL)) ;
          if (!($$)->IsSystemVeri2009OrLater()) ($$)->Error("syntax error near %s", "[*") ; // VIPER #6389
      }
    | sequence_expr "[+]" // VIPER #6329 & #6357
      {
          $$ = new VeriBinaryOperator(VERI_CONSECUTIVE_REPEAT, $1, new VeriRange(0,0,VERI_PLUS)) ;
          if (!($$)->IsSystemVeri2009OrLater()) ($$)->Error("syntax error near %s", "[+") ; // VIPER #6389
      }

//    | sequence_inst
//      { $$ = 0 ; /* Sequence instance, covered by function_call in 'expr' */ }

    | sequence_expr VERI_OR sequence_expr // can also be a property_OR
      { $$ = new VeriBinaryOperator(VERI_OR, $1, $3) ; }
    | sequence_expr VERI_OR edge_id expr // can be used sequence/property argument/initial value
      { $$ = new VeriBinaryOperator(VERI_OR, $1, new VeriEventExpression($3, $4)) ; }
    | sequence_expr VERI_INTERSECT sequence_expr
      { $$ = new VeriBinaryOperator(VERI_INTERSECT, $1, $3) ; }
    | sequence_expr VERI_AND sequence_expr // can also be a property_AND
      { $$ = new VeriBinaryOperator(VERI_AND, $1, $3) ; }
    | VERI_FIRST_MATCH "(" sequence_expr ")"
      { $$ = new VeriUnaryOperator(VERI_FIRST_MATCH, $3) ; }
    | expr VERI_THROUGHOUT sequence_expr
      { $$ = new VeriBinaryOperator(VERI_THROUGHOUT, $1, $3) ; }
// 'dist' operator moved to 'expr'
//    | expr VERI_DIST "{" dist_list "}" VERI_THROUGHOUT sequence_expr // SV 1800 addition
//      { $$ = new VeriBinaryOperator(VERI_THROUGHOUT, new VeriDistOperator($1, $4), $7) ; }
    | sequence_expr VERI_WITHIN sequence_expr
      { $$ = new VeriBinaryOperator(VERI_WITHIN, $1, $3) ; }

    | clocking_event sequence_expr %prec VERI_CLOCKING_EVENT // 3.1a. Result can be property_expr too. Low precendence. See notes at VERI_CLOCKING_EVENT
      { $$ = new VeriClockedSequence($1, $2) ; }
// 'dist' operator moved to 'expr'
//    | expr VERI_DIST "{" dist_list "}" // New in 3.1a. Not sure about precedence ..
//      { $$ = new VeriDistOperator($1,$4) ; }
    // VIPER #7832 : Moved to 'expr'
    //| function_call_prefix "." VERI_ID // VIPER 2178: sequence_instance.ended / sequence_instance.matched
      //{ $$ = new VeriSelectedName($1, $3) ; } // Map to selected name.
    | sequence_expr VERI_OVERLAPPED_FOLLOWED_BY sequence_expr // result (and rhs) MUST be a property_expr
      {
          // Clock-flow fix for VIPER 5118/2582:
          // Now that clocking event has higher precedence than followed-by operators, a expression-wide clocking event can end up on the left of a followed-by operator.
          // To avoid regression, and to mimic SV 'leading clock' flow, move clocking event back from left arg to top of expression:
          // Only do this if the left side (sequence) was not parenthesized (else the clock flow cannot be moved)
          VeriExpression *clocked_seq = $1 ;
          if (clocked_seq && clocked_seq->IsClockedSequence() && !clocked_seq->IsParenthesized()) {
              // Swap the clocking event to the top of the expression:
              VeriExpression *expr = clocked_seq->GetArg() ; // Get the sequence that was clocked. and use that for the implication
              expr = new VeriBinaryOperator(VERI_OVERLAPPED_FOLLOWED_BY, expr, $3) ;
              clocked_seq->SetArg(expr) ;
              $$ = clocked_seq ;
          } else {
              $$ = new VeriBinaryOperator(VERI_OVERLAPPED_FOLLOWED_BY, $1, $3) ;
          }
      }
    | sequence_expr VERI_NON_OVERLAPPED_FOLLOWED_BY sequence_expr // result (and rhs) MUST be a property_expr
      {
          // Clock-flow fix for VIPER 5118/2582:
          // Now that clocking event has highe precedence than followed-by operators, a expression-wide clocking event can end up on the left of a followed-by operator.
          // To avoid regression, and to mimic SV 'leading clock' flow, move clocking event back from left arg to top of expression:
          // Only do this if the left side (sequence) was not parenthesized (else the clock flow cannot be moved)
          VeriExpression *clocked_seq = $1 ;
          if (clocked_seq && clocked_seq->IsClockedSequence() && !clocked_seq->IsParenthesized()) {
              // Swap the clocking event to the top of the expression :
              VeriExpression *expr = clocked_seq->GetArg() ; // Get the sequence that was clocked. and use that for the followed-by
              expr = new VeriBinaryOperator(VERI_NON_OVERLAPPED_FOLLOWED_BY, expr, $3) ;
              clocked_seq->SetArg(expr) ;
              $$ = clocked_seq ;
          } else {
              $$ = new VeriBinaryOperator(VERI_NON_OVERLAPPED_FOLLOWED_BY, $1, $3) ;
          }
      }
    // System verilog P1800-2009 property operators :
    // VIPER #5710 : VCS Compatibility issue (if_in_property), abort properties
    | abort_properties "(" expression_or_dist ")" sequence_expr
      { $$ = new VeriBinaryOperator($1, $3, $5) ; }
    | VERI_S_NEXTTIME sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_NEXTTIME, 0, $2) ; }
    // VIPER #5710 : VCS Compatibility issue (if_in_property), nexttime operators
    | VERI_NEXTTIME sequence_expr
      { $$ = new VeriBinaryOperator(VERI_NEXTTIME, 0, $2) ; }
    | VERI_S_NEXTTIME bracket_expr sequence_expr // Produces 12 shift/reduce conflicts with sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_NEXTTIME, $2, $3) ; }
    | VERI_NEXTTIME bracket_expr sequence_expr // Produces 12 shift/reduce conflicts with sequence_expr
      { $$ = new VeriBinaryOperator(VERI_NEXTTIME, $2, $3) ; }
    | sequence_expr VERI_IFF sequence_expr
      { $$ = new VeriBinaryOperator(VERI_IFF, $1, $3) ; }
    | sequence_expr VERI_IMPLIES sequence_expr
      { $$ = new VeriBinaryOperator(VERI_IMPLIES, $1, $3) ; }
      // VIPER #5710 : VCS Compatibility issue (iff_in_property), until operators
    | sequence_expr VERI_UNTIL sequence_expr
      { $$ = new VeriBinaryOperator(VERI_UNTIL, $1, $3) ; }
    | sequence_expr VERI_S_UNTIL sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_UNTIL, $1, $3) ; }
    | sequence_expr VERI_UNTIL_WITH sequence_expr
      { $$ = new VeriBinaryOperator(VERI_UNTIL_WITH, $1, $3) ; }
    | sequence_expr VERI_S_UNTIL_WITH sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_UNTIL_WITH, $1, $3) ; }
    | VERI_EVENTUALLY bracket_range_expr sequence_expr
      { $$ = new VeriBinaryOperator(VERI_EVENTUALLY, $2, $3) ; }
    | VERI_S_EVENTUALLY sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_EVENTUALLY, 0, $2) ; }
    | VERI_S_EVENTUALLY bracket_range_expr sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_EVENTUALLY, $2, $3) ; }
    | VERI_S_EVENTUALLY bracket_range_dollar sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_EVENTUALLY, $2, $3) ; }
    | VERI_ALWAYS sequence_expr
      { $$ = new VeriBinaryOperator(VERI_ALWAYS, 0, $2) ; }
    | VERI_ALWAYS bracket_range_expr sequence_expr
      { $$ = new VeriBinaryOperator(VERI_ALWAYS, $2, $3) ; }
    | VERI_ALWAYS bracket_range_dollar sequence_expr
      { $$ = new VeriBinaryOperator(VERI_ALWAYS, $2, $3) ; }
    | VERI_S_ALWAYS bracket_range_expr sequence_expr
      { $$ = new VeriBinaryOperator(VERI_S_ALWAYS, $2, $3) ; }
    | VERI_STRONG "(" sequence_expr ")"
      { $$ = new VeriUnaryOperator(VERI_STRONG, $3) ; }
    | VERI_WEAK "(" sequence_expr ")"
      { $$ = new VeriUnaryOperator(VERI_WEAK, $3) ; }
    ;

property_case_item_list :
      property_case_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | property_case_item_list property_case_item
      { $$ = $1 ; ($$)->InsertLast($2) ; }
    ;

property_case_item :
      expression_or_dist_list ":" property_spec ";" // CHECKME: semi-colon not required for 'if' and 'case'
      {
          $$ = new VeriCaseOperatorItem($1, $3) ;
      }
    | VERI_DEFAULT opt_colon property_spec ";" // CHECKME: semi-colon not required for 'if' and 'case'
      {
          $$ = new VeriCaseOperatorItem(0, $3) ;
      }
    ;

expression_or_dist_list :
      expression_or_dist
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | expression_or_dist_list "," expression_or_dist
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

abort_properties : VERI_ACCEPT_ON { $$ = VERI_ACCEPT_ON ; }
    | VERI_REJECT_ON              { $$ = VERI_REJECT_ON ; }
    | VERI_SYNC_ACCEPT_ON         { $$ = VERI_SYNC_ACCEPT_ON ; }
    | VERI_SYNC_REJECT_ON         { $$ = VERI_SYNC_REJECT_ON ; }
    ;

bracket_expr : "[" expr "]"
      { $$ = $2 ; } // Viper 6822. Use expression instead of range. Using range creates error later in RTL elaboration
    ;

bracket_range_expr : "[" expr ":" expr "]" // 'constant-ness checked in Resolve()
      { $$ = new VeriRange($2, $4, 0) ; }
    ;

bracket_range_dollar : "[" expr ":" VERI_DOLLAR "]" // '$' is no longer part of 'expr', so add this rule to indicate specific use of '$' (#3499)
      { $$ = new VeriRange($2, new VeriDollar(), 0) ; }
    ;

func_blocking_assignment_list : sequence_item  // a list of statements
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | func_blocking_assignment_list "," sequence_item
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

sequence_item :
      operator_assignment
    | inc_or_dec_stmt // new in 3.1a
    | task_enable // new in 3.1a
    | system_task_enable // VIPER 2539 : system_tf_call can be used
    ;

const_or_range_expr :
      expr // 'constant-ness' checked in Resolve()
    | cycle_delay_const_range_expr
    ;

cycle_delay_const_range_expr :
      expr ":" expr // 'constant-ness checked in Resolve()
      { $$ = new VeriRange($1, $3, 0) ; }
    | expr ":" VERI_DOLLAR // '$' is no longer part of 'expr', so add this rule to indicate specific use of '$' (#3499)
      { $$ = new VeriRange($1, new VeriDollar(), 0) ; }
    ;

cycle_delay_range : // semantics according to System Verilog 3.1a
      "##" VERI_NUM
       { $$ = new VeriIntVal((int)($2)) ; }
    | "##" VERI_REAL_NUM
       { $$ = new VeriRealVal($2) ; }
    | "##" id_ref // a parameter reference : has to be single id
       { $$ = $2 ; }
    | "##" "(" expr ")"
       { $$ = $3 ; }
    | "##" "[" cycle_delay_const_range_expr "]"
      { $$ = $3 ; }
    | "##" "[*]" // VIPER #6329 & #6357
      {
          $$ = new VeriRange(0,0,VERI_MUL) ;
          if (!($$)->IsSystemVeri2009OrLater()) ($$)->Error("syntax error near %s", "[*") ; // VIPER #6389
      }
    | "##" "[+]" // VIPER #6329 & #6357
      {
          $$ = new VeriRange(0,0,VERI_PLUS) ;
          if (!($$)->IsSystemVeri2009OrLater()) ($$)->Error("syntax error near %s", "[+") ; // VIPER #6389
      }
    ;

assertion_variable_decl_list :
      { $$ = 0 ; }
    | assertion_variable_decl_list assertion_variable_declaration
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

assertion_variable_declaration :
      variable_decl // 'data_type list_of_variable_ids' : // same as (plain) 'variable' decl.
      { $$ = $1 ; }
    | VERI_VIRTUAL VERI_INTERFACE named_type_var_decl // variable_decl does not include virtual interface decl
      { $$ = $3 ; if ($$) $$->SetQualifier(VERI_VIRTUAL) ; update_start_linefile($$) ; }
    | VERI_VIRTUAL named_type_var_decl // Is these two needed ?
      { $$ = $2 ; if ($$) $$->SetQualifier(VERI_VIRTUAL) ; update_start_linefile($$) ; }
    ;

// Clocking section items :
// VIPER #6380 : Create VeriDelayOrEventControl having array as argument for clocking event
clocking_event :
      "@" delay_event_hier_ref  // LRM states single id here, but VCSI accepts a hierarchical identifier.
      { Array *arr = new Array(1) ; arr->InsertLast($2) ; $$ = new VeriDelayOrEventControl(0, 0, arr) ; }
    | "@" "(" event_control_expr ")"
      { $$ = new VeriDelayOrEventControl(0, 0, $3) ; }
    | "@" system_func_id // VIPER #5710 (vcs_compatibility: clocking)
       {
        $$ = new VeriSystemFunctionCall($2,0) ;
        Array *arr = new Array(1) ; arr->InsertLast($$) ;
        $$ = new VeriDelayOrEventControl(0, 0, arr) ;
       }
    ;

/******************************* Instances ************************/

inst_type : VERI_AND    { $$ = VERI_AND ; }
    | VERI_NAND     { $$ = VERI_NAND ; }
    | VERI_OR       { $$ = VERI_OR ; }
    | VERI_NOR      { $$ = VERI_NOR ; }
    | VERI_XOR      { $$ = VERI_XOR ; }
    | VERI_XNOR     { $$ = VERI_XNOR ; }
    | VERI_BUF      { $$ = VERI_BUF ; }
    | VERI_NOT      { $$ = VERI_NOT ; }
    | VERI_BUFIF0   { $$ = VERI_BUFIF0 ; }
    | VERI_BUFIF1   { $$ = VERI_BUFIF1 ; }
    | VERI_NOTIF0   { $$ = VERI_NOTIF0 ; }
    | VERI_NOTIF1   { $$ = VERI_NOTIF1 ; }
    | VERI_NMOS     { $$ = VERI_NMOS ; }
    | VERI_PMOS     { $$ = VERI_PMOS ; }
    | VERI_RNMOS    { $$ = VERI_RNMOS ; }
    | VERI_RPMOS    { $$ = VERI_RPMOS ; }
    | VERI_TRAN     { $$ = VERI_TRAN ; }
    | VERI_RTRAN    { $$ = VERI_RTRAN ; }
    | VERI_TRANIF0  { $$ = VERI_TRANIF0 ; }
    | VERI_TRANIF1  { $$ = VERI_TRANIF1 ; }
    | VERI_RTRANIF0 { $$ = VERI_RTRANIF0 ; }
    | VERI_RTRANIF1 { $$ = VERI_RTRANIF1 ; }
    | VERI_CMOS     { $$ = VERI_CMOS ; }
    | VERI_RCMOS    { $$ = VERI_RCMOS ; }
    | VERI_PULLUP   { $$ = VERI_PULLUP ; }
    | VERI_PULLDOWN { $$ = VERI_PULLDOWN ; }
    ;

gate_instantiation :
       // Since 'strength' starts with a '(' and so does the connection_list in an instance,
       // we have so spell-out the strength/delay rules and cannot use opt_strengh opt_delay.
       // Same problem in module/udp instantiation.
      inst_type inst additional_insts ";"
      { $$ = new VeriGateInstantiation($1,0,0,$2,$3) ; }
    | inst_type delay inst additional_insts ";"
      { $$ = new VeriGateInstantiation($1,0,$2,$3,$4) ; }
    | inst_type strength opt_delay inst additional_insts ";"
      { $$ = new VeriGateInstantiation($1,$2,$3,$4,$5) ; }
    ;

module_instantiation:
      // Module instantiations MUST have an instance name. They cannot have 'strength' info.
      // So here are the rules that encompass all true module instatiations (and some named UDP instances)
      hier_ref named_inst additional_insts ";" // module or named UDP instance
      { $$ = new VeriModuleInstantiation($1,0,0,$2,$3) ; }
    | hier_ref parameter_value_assignment named_inst additional_insts ";" // module or named UDP instance
      { $$ = new VeriModuleInstantiation($1,0,$2,$3,$4) ; }
    ;

udp_instantiation:
      // UDP instantiations can, but do not have to have a name. They can have strength also.
      // The named ones end up in the 'module_instantiation' rule above.
      // Here, we need to carefully cover the remaining possible UDP instantiations.
      // This rule would collide with task_enable/function_call used in the same context.
      hier_ref unnamed_inst additional_insts ";" // unnamed UDP instance
      { $$ = new VeriModuleInstantiation($1,0,0,$2,$3) ; }
    | hier_ref parameter_value_assignment unnamed_inst additional_insts ";" // unnamed UDP instance. 'parameter_value_assignment' stands in for 'delay'. Otherwise we have a conflict.
      { $$ = new VeriModuleInstantiation($1,0,$2,$3,$4) ; }
    | hier_ref strength opt_delay inst additional_insts ";" // named/unnamed UDP instance with strength (and optional delay).
      { $$ = new VeriModuleInstantiation($1,$2,$3,$4,$5) ; }
    ;

// VIPER #5710(vcs_compatibility: always_checker(for checker instantiation inside procedural code)):
sequential_instantiation:
      // Sequential instantiations MUST have an instance name. They cannot have 'strength' info.
      // So here are the rules that encompass all true sequential instatiations
      hier_ref named_inst additional_insts ";" // sequential instantiation
      {
          if (($1) && !VeriNode::IsSystemVeri()) {
              // VIPER #7363: This is a SV specific construct, if it is instantiated in non-SV mode, produce error:
              ($1)->Error("syntax error near %s", ($1)->GetName()) ;
              // VIPER #7363 (re-opened): Simply producing error is not enough. We need to discard this construct
              // altogether to avoid further misleading error messages. So delete/undo the already created items:
              Array *insts = ($3) ? ($3) : new Array(1) ;
              insts->InsertLast($2) ;
              unsigned i ;
              VeriInstId *inst ;
              FOREACH_ARRAY_ITEM(insts, i, inst) {
                  if (present_scope) present_scope->Undeclare(inst) ;
                  delete inst ;
              }
              delete insts ;
              delete ($1) ;
              $$ = 0 ;
          } else {
              $$ = new VeriSequentialInstantiation($1,$2,$3) ;
          }
      }
    ;

parameter_value_assignment :
      "#" delay_value_udp // officially not allowed for user-module instantiation (need ()'s), but needed for delay on udp instantiations.
      { $$ = new Array() ; ($$)->InsertLast($2) ; }
    | "#" "(" list_of_param_assignments ")" // normal list (named/ordered) of parameter values.
      { $$ = $3 ; }
    ;

list_of_param_assignments :
      param_assignment
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_param_assignments "," param_assignment
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

param_assignment :
      mintypmax_expr  // mintypmax expr, because param_assignment can also be used in a UDP instantiation as delay parameter
      { $$ = $1 ; }
    | "." VERI_ID "(" ")"
      { $$ = new VeriPortConnect($2,new VeriPortOpen()) ; } /* Verilog 2000 : dangling 'named' parameter */
    | "." VERI_ID "(" expr ")"
      { $$ = new VeriPortConnect($2,$4) ; } /* Verilog 2000 : connected 'named' parameter */
      // for type-parameters :
    | data_type_without_type_ref // type_reference included in mintypmax_expr -> expr -> primary
      { $$ = $1 ; }
    // VIPER #3827 : Allow parameterized class with param overwrite as value of parameter
    | hier_ref parameter_value_assignment
      { $$ = new VeriTypeRef(($1) ? $1->SetParamValues($2): $1, 0, 0) ; }
//    | VERI_ID opt_dimensions // covered by 'expr' (mostly)
//      { $$ = 0 ; }
    // VCS Compatibility issue (virtual_interface): Allow virtual interface as
    // overwritten value of type parameter
    // VIPER #8014 : Allow parameter association with virtual interface
    | VERI_VIRTUAL hier_ref opt_parameter_value_assignment
      { $$ = new VeriTypeRef(VERI_VIRTUAL, $3 ? $2->SetParamValues($3): $2) ; }
    | VERI_VIRTUAL VERI_INTERFACE hier_ref opt_parameter_value_assignment
      { $$ = new VeriTypeRef(VERI_VIRTUAL, $4 ? $3->SetParamValues($4): $3) ; }
    | "." VERI_ID "(" data_type_without_type_ref ")" // type_reference included in mintypmax_expr -> expr -> primary
      { $$ = new VeriPortConnect($2,$4) ; }
      // VIPER #5561 : Allow class instantiation with parameter overwrite in named association
    | "." VERI_ID "(" hier_ref parameter_value_assignment ")"
      { VeriExpression *actual = new VeriTypeRef(($4) ? $4->SetParamValues($5): $4, 0, 0) ; $$ = new VeriPortConnect($2,actual) ; }
//    | "." VERI_ID "(" VERI_ID opt_dimensions ")" // covered by 'expr' (mostly)
//      { $$ = new VeriPortConnect($2,0) ; }
    // VCS Compatibility issue (virtual_interface): Allow virtual interface as
    // overwritten value of type parameter
    | "." VERI_ID "(" VERI_VIRTUAL hier_ref opt_parameter_value_assignment")"
      { $$ = new VeriPortConnect($2, new VeriTypeRef(VERI_VIRTUAL, $6 ? $5->SetParamValues($6): $5)) ; }
    | "." VERI_ID "(" VERI_VIRTUAL VERI_INTERFACE hier_ref opt_parameter_value_assignment")"
      { $$ = new VeriPortConnect($2, new VeriTypeRef(VERI_VIRTUAL, $7 ? $6->SetParamValues($7): $6)) ; }
    | VERI_DOLLAR
      { $$ = new VeriDollar() ; }
    | // VIPER #4784: Allow empty parameter assigment according to A.4.1.1 of IEEE 1800 LRM.
      // VIPER #5248 (Params_module_port, star187743): extended from empty list_of_param_assignments to empty (element) param assignment.
      { $$ = 0 ; /* Create an empty entry to differentiate between no override and empty override list */ }
    ;

inst: unnamed_inst
    | named_inst
    ;

unnamed_inst:
      "(" { implicit_nets_allowed = 1 ; } port_connection_list { implicit_nets_allowed = 0 ; } ")" // VIPER #3934: Put ')' at the end
      { $$ = declare_inst(new VeriInstId(0,0,$3)) ; /* NULL name for the identifier */ }

// System Verilog LRM essentially allows a range without an instance name for UDP instantiation. 2001 did not.
// There is heavy rule collision with data type declarations if a range is allowed in absense of (or before the) instance identifier.
// Since this is not allowed for module instantiation, AND there seems a error in the BNF for UDP instantiation (ranges mentioned twice)
// we assume it is a mistake, and that range(s) are only allowed on named instances.
//    | range "(" { implicit_nets_allowed = 1 ; } port_connection_list ")" { implicit_nets_allowed = 0 ; }
//      { $$ = declare_inst(new VeriInstId(0,$1,$4)) ; }
    ;

named_inst :
      hier_ref "(" { implicit_nets_allowed = 1 ; } port_connection_list { implicit_nets_allowed = 0 ; } ")" // VIPER #3934: Put ')' at the end
      {  // Cannot declare as inst before we see the "(". Collision with declarations.
         char *name = extract_net_name($1) ;
         if (name) {
             $$ = declare_inst(new VeriInstId(name,0,$4)) ;
         } else {
             $$ = 0 ;
             delete $4 ;
         }
         delete $1 ;
      }
    | hier_ref dimensions "(" { implicit_nets_allowed = 1 ; } port_connection_list { implicit_nets_allowed = 0 ; } ")" // VIPER #3934: Put ')' at the end
      {  // Cannot declare as inst before we see the "(". Collision with declarations.
         char *name = extract_net_name($1) ;
         VeriRange *dim = 0 ;
         GET_DIMENSION_RANGE(dim, $2) ;
         if (name) {
             $$ = declare_inst(new VeriInstId(name,dim,$5)) ;
         } else {
             $$ = 0 ;
             delete dim ; delete $5 ;
         }
         delete $1 ;
      }
     // Actually, parameterized class instantiations would end-up here (LRM examples only : BNF seems totally wrong)
     // Might need to consolidate module instantiations and data declarations..
    ;

additional_insts :
      { $$ = 0 ; }
    | additional_insts "," inst
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

port_connection_list : port_connection
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | port_connection_list "," port_connection
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

port_connection : plain_connection opt_comment
      { $$ = $1 ; add_pre_comments_to_node($$, $2) ; }
    | real_attr_inst { if ($1) port_connect_has_attribute = 1 ; } plain_connection opt_comment
      { $$ = $3 ; if ($1 && $$) ($$)->AddAttributes($1) ; update_start_linefile($$) /* VIPER #7563 */ ; add_pre_comments_to_node($$, $4) ; port_connect_has_attribute = 0 ; }
    ;

plain_connection : // we always want to return something, so that attributes / comments can be set
      { $$ = new VeriPortOpen() ; /* dangling 'ordered' pin */ }
    //| expr_or_tagged_id_expr // VIPER #2964 : Allow tagged union as actual
      //{ $$ = $1 ; /* connected 'ordered' pin */ }
    | "." VERI_ID "(" ")"
      { $$ = create_port_connect($2,new VeriPortOpen()) ; /* dangling 'named' pin */  }
    | "." VERI_ID "(" expr_or_tagged_id_expr ")"
      { $$ = create_port_connect($2,$4) ; /* connected 'named' pin */ }
    | "." VERI_ID "(" VERI_DOLLAR ")"
      { $$ = create_port_connect($2,new VeriDollar()) ; /* connected 'named' pin */ }
    | "." VERI_ID
      { $$ = new VeriPortConnect($2,new VeriIdRef(Strings::save($2))) ; /* new VeriDotName() ? */ }
    | VERI_DOTSTAR
      { $$ = new VeriDotStar() ; }
    | tagged_id_expr  // 'expr' is parsed through sequence_expr -> 'mintypmax_expr'
      { $$ = $1 ; /* connected 'ordered' */}
    | "." VERI_ID "(" clocking_event ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | "." VERI_ID "(" edge_id_expr ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | "." VERI_ID "(" sequence_expr_wo_expr ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | clocking_event // some system calls (rose,fell,past etc) allow clocking events in arguments..^M
      { $$ = $1 ; }
    | edge_id_expr
    | VERI_DOLLAR { $$ = new VeriDollar() ; }
    | sequence_expr
       {
          // VIPER #5248 (display_close_brace): Allow min-typ-max expression in argument:
          $$ = $1 ;
          if ($$ && ($$)->IsMinTypMax()) ($$)->Warning("min-typ-max expression in argument violates IEEE 1800 syntax") ;
       }
    ;

expr_list : expr_or_tagged_id_expr
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | expr_list "," expr_or_tagged_id_expr
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | expr_list ","
      { /* Someone missed an expression. Not really LRM compliant, but we accept it */
        $$ = $1 ;
        if ($$) ($$)->InsertLast(0)  ;
        // VIPER #5814: Produce a warning message for this incorrect syntax:
        VeriTreeNode tmp_node ;
        tmp_node.Warning("empty item in case item list violates IEEE 1364 syntax") ;
      }
    | error
      { $$ = 0 ; }
    ;

mintypmax_expr_list : mintypmax_expr
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | mintypmax_expr_list "," mintypmax_expr
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

delay_value_udp :
      VERI_NUM
       { $$ = new VeriIntVal((int)($1)) ; }
    | VERI_REAL_NUM
       { $$ = new VeriRealVal($1) ; }
    //| delay_event_hier_ref // VIPER 2273 : allow dot separated identifier in delay control
    | param_value_hier_ref // VIPER #4243 : Do not allow scope name here, it will create conflict with hier_ref rule
       { $$ = $1 ; }
    | time_literal
       { $$ = $1 ; }
    ;
delay_value :
      VERI_NUM
       { $$ = new VeriIntVal((int)($1)) ; }
    | VERI_REAL_NUM
       { $$ = new VeriRealVal($1) ; }
    | delay_event_hier_ref // VIPER 2273 : allow dot separated identifier in delay control
       { $$ = $1 ; }
    | time_literal
       { $$ = $1 ; }
    ;

/*********************** Generate Section ********************/

// Generate item can have attributes or comments around it
generate_item : attr_inst pre_comment plain_generate_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      // Issue 2346 : allow semicolon (in 'generate_item', even though VCS, ModelSim and LRM do not allow it.
      // Now generate_item_or_null is same as generate_item. Could merge in future.
      { $$ = new VeriNullStatement() ; // VIPER #8419: create null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    ;

// Statement_or_null is used if other statements have a (open) 'statement' at end of rule
generate_item_or_null :
      attr_inst pre_comment plain_generate_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ; // attributes
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ; // pre-comments
        add_post_comments_to_node($$, $4) ; // trailing comments, but NOT forced
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    ;

plain_generate_item :
      // VIPER #3855/#5512: Moved following 4 rules under module_or_generate_item
//    | generate_conditional
//    | generate_case
//    | generate_for_loop
//    | generate_block
       module_or_generate_item // NOTE : heavy syntax restrictions inside a generate construct. No nesting ! and no parameters allowed !

    // VIPER #6965: Allow localparam declaration within generate region. Verilog 2K-2001 does not allow it but Verilog 2K-2005 does.
    | local_parameter_decl  // VIPER 2657: SV allows local parameter declaration within generate block
    | modport_decl
    | parameter_decl        // VIPER 2657: SV allows parameter declaration within generate block
    | checker_decl          // VIPER #7641 : SV allows checker decl within generate
      { // nested checker declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    ;

generate_block : // issue 2145 : all generate blocks get a scope, regardless of the label
      VERI_BEGIN opt_block_label generate_items VERI_END
      { $$ = new VeriGenerateBlock($2,$3, pop_scope()) ; }
    | statement_label VERI_BEGIN opt_block_label generate_items VERI_END
      {
          $$ = new VeriGenerateBlock($1,$4, pop_scope()) ;
          // VIPER #7978: Error out if both the label before and after the begin keyrod are specified:
          if ($3) ($1)->Error("%s block cannot have a label both before and after the %s keyword", "generate", "begin") ;
          process_labeled_stmt($1, $$) ; // VIPER #8303
          (void) pop_last_label() ; /*last_label = 0 ;*/
      }
    ;

generate_items :
      { $$ = 0 ; }
    | generate_items generate_item
      { $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
//    | error // Bison 1.35 transition table exceeds if we enable this.
//      { $$ = 0 ; }
    ;

generate_conditional :
      VERI_IF if_scope "(" expr ")" generate_item_or_null %prec veri_no_else // Slightly lower precendence than 'else'
      { $$ = new VeriGenerateConditional($4,$6,0, pop_scope(), 0) ; }
    | VERI_IF if_scope "(" expr ")" generate_item_or_null VERI_ELSE then_scope generate_item_or_null
      { $$ = new VeriGenerateConditional($4,$6,$9, $8, pop_scope()) ;  }
    ;

if_scope : { push_scope(0) ; } // Just push into a new scope
    ;

then_scope : { $$ = pop_scope() ; push_scope(0) ; } // pop (and return) previous scope, and enter into new one.
    ;

generate_case : case_token "(" expr ")" generate_case_items VERI_ENDCASE
      { $$ = new VeriGenerateCase($1,$3,$5) ; }
    ;

generate_case_items : generate_case_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | generate_case_items generate_case_item
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

generate_case_item : expr_list ":" { push_scope(0) ; } generate_item_or_null
      { $$ = new VeriGenerateCaseItem($1, $4, pop_scope()) ; }
    | VERI_DEFAULT opt_colon { push_scope(0) ; } generate_item_or_null
      { $$ = new VeriGenerateCaseItem(0,$4, pop_scope()) ; }
    ;

generate_for_loop :
        VERI_FOR { push_scope(0,1); } "(" genvar_decl_assignment ";" expr ";" genvar_assignment ")" { push_scope(0) ; create_implicit_localparam($4) ; for_gen_started = 1 ; }
        generate_item // VIPER #4781 & 4330: Allow generate block and any other generate item here
      {
          // VIPER #8109: If generate-item of for-generate is generate block, that
          // is created using for-gen-scope, so use that
          VeriScope *body_scope = (($11) && ($11)->IsGenerateBlock()) ? $11->GetScope(): pop_scope() ;
          Array *arr = new Array(1) ;
          arr->InsertLast($11) ;
          $$ = new VeriGenerateFor($4,$6,$8,0,arr, body_scope, pop_scope()) ;
          for_gen_started = 0 ;
      }
    ;

genvar_decl_assignment :
      VERI_ID VERI_EQUAL expr
      {
          // VIPER #8109: Create VeriGenVarAssign with genvar-id so that we
          // can extract that in 'create_implicit_localparam' while creating
          // implicit localparam for that
          VeriIdDef *genvar_id = present_scope ? present_scope->Find($1): 0 ;
          // VIPER #3277: Check whether the genvar is already declared (before):
          //if (present_scope && !present_scope->Find($1)) {
          if (!genvar_id) {
              // Push out a warning message that the genvar is not declared:
              if ($3) $3->Error("generate loop index %s is not defined as a genvar", $1) ;
              // And implicitly declare it in the upper scope (this seems to be what
              // other tools do and error out for the genvar declared later):
              VeriScope *upper = present_scope->Upper() ;
              if (upper) {
                  genvar_id = new VeriGenVarId(Strings::save($1)) ;
                  (void) upper->Declare(genvar_id) ;
              }
          }
          if (genvar_id) {
              $$ = new VeriGenVarAssign(genvar_id,$3) ;
              Strings::free($1) ;
          } else {
              $$ = new VeriGenVarAssign($1,$3) ;
          }
      }
    | VERI_GENVAR genvar_decl_id VERI_EQUAL expr
      {  $$ = new VeriDataDecl(VERI_GENVAR, 0, $2) ; if ($2 && $4) ($2)->SetInitialValue($4) ; }
    ;

genvar_assignment :
      VERI_ID assignment_operator expr
      { $$ = new VeriGenVarAssign($1,$3,$2) ; }
    | inc_or_dec_stmt
      { $$ = $1 ; /* from statement to module item */ }
    ;

/******************************* UDPs *****************************/

udp_decl : VERI_PRIMITIVE { design_unit_depth++ ; config_is_identifier++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; }
      udp_decl_id opt_pragma { push_scope($3) ; }
    opt_list_of_ports ";"
    { extract_scope_info_from_extern_mod($3, 0, $6) ; /* VIPER #5088  */} opt_pragma // issue 2347 : should allow ANSI port style declarations
    opt_udp_port_decls opt_udp_initial_statement table VERI_ENDPRIMITIVE
      {
        design_unit_depth-- ; config_is_identifier-- ;
        // VIPER #5017: Re-structure udp rule to allow only io_decls and initial statement.
        Array *udp_items = $10 ;
        if ($11) {
            if (!udp_items) udp_items = new Array(1) ;
            udp_items->InsertLast($11) ; // initial statement
        }
        Array *port_list = get_port_list($3, $6) ; ($6) = 0 ;

        // the 'table' is a special item into the primitive :
        $$ = new VeriPrimitive($3,port_list,udp_items,$12,pop_scope()) ;
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale

        // Set extra pragmas
        if ($4) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$4)) ; // Pragmas before the header
        if ($9) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$9)) ; // Pragmas after the header
      }
    | VERI_EXTERN VERI_PRIMITIVE udp_decl_id opt_pragma
        { push_scope($3) ; if ($3) $3->SetIsExtern() ; }
    opt_list_of_ports ";" opt_pragma // issue 2347 : should allow ANSI port style declarations
      {
        $$ = new VeriPrimitive($3,$6,0,0,pop_scope()) ;
        ($$)->SetQualifier(VERI_EXTERN) ;

        // Set extra pragmas
        if ($4) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$4)) ; // Pragmas before the header
        if ($8) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$8)) ; // Pragmas after the header
      }
    ;

opt_udp_port_decls :
      { $$ = 0 ; }
    | udp_port_decls
      { $$ = $1 ; }
    ;

udp_port_decls :
      attr_inst udp_port_decl
      { $$ = new Array() ; ($$)->InsertLast($2) ; if ($1 && $2) ($2)->AddAttributes($1) ; if ($1) update_start_linefile($2) ; /* VIPER #7563 */ }
    | udp_port_decls attr_inst udp_port_decl
      { $$ = $1 ; ($$)->InsertLast($3) ; if ($2 && $3) ($3)->AddAttributes($2) ; if ($2) update_start_linefile($3, &(@2)) ; /* VIPER #7563 */ }
    ;

udp_port_decl :
      VERI_OUTPUT id_ref ";"
      { $$ = new VeriDataDecl(VERI_OUTPUT,0,declare_port($2,0,0)) ; }
    | VERI_OUTPUT VERI_REG id_ref ";"
      { $$ = new VeriDataDecl(VERI_OUTPUT,new VeriDataType(VERI_REG,0,0),declare_port($3,0,0)) ; }
    | VERI_OUTPUT VERI_REG id_ref VERI_EQUAL expr ";"
      {
          VeriIdDef *port = declare_port($3,0,$5) ;
          $$ = new VeriDataDecl(VERI_OUTPUT,new VeriDataType(VERI_REG,0,0),port) ;
      }
    | VERI_REG id_ref ";"
      {
          VeriVariable *reg_id = new VeriVariable(Strings::save(($2)->GetName())) ;
          reg_id->SetLinefile(($2)->Linefile()) ;
          $$ = new VeriDataDecl(0,new VeriDataType(VERI_REG,0,0),declare_net_reg(reg_id, 0, 0, $2)) ;
          delete $2 ;
      }
    | VERI_INPUT list_of_udp_port_ids ";"
      { $$ = new VeriDataDecl(VERI_INPUT,0,$2) ; }
    ;

list_of_udp_port_ids :
      id_ref
      { $$ = new Array() ; ($$)->InsertLast(declare_port($1,0,0)) ; }
    | list_of_udp_port_ids "," id_ref
      { $$ = $1 ; ($$)->InsertLast(declare_port($3,0,0)) ; }
    ;

opt_udp_initial_statement :
      { $$ = 0 ; }
    | VERI_INITIAL udp_blocking_assign
      { $$ = new VeriInitialConstruct($2) ; }
    ;

udp_blocking_assign :
      id_ref VERI_EQUAL number ";"
      { $$ = new VeriBlockingAssign($1,0,$3) ; }
    ;

table : // VIPER 2967 : allow table NOT to be there, so we won't error out if it is `protected
      { $$ = 0 ; }
    | VERI_TABLE table_entries VERI_ENDTABLE
      { $$ = new VeriTable($2) ; }
    ;

table_entries :
      { $$ = new Array() ; }
    | table_entries symbol_list VERI_UDP_IOSEP VERI_UDP_VAL VERI_UDP_LINESEP
      {
        VeriTreeNode tmp ;
        if ($4 == '?' || $4 == 'b' || $4 == 'B' || $4 == '-') tmp.Error("illegal output symbol '%c' for combinational udp", $4) ; // VIPER #4707

        tmp_buf[tmp_buf_idx++] = ':' ;
        tmp_buf[tmp_buf_idx++] = $4 ;
        tmp_buf[tmp_buf_idx++] = '\0' ;
        VERIFIC_ASSERT(tmp_buf_idx < MAX_UDP_SYMBOLS) ;
        $$ = $1 ;
        ($$)->InsertLast(Strings::save(tmp_buf)) ;
      }
    | table_entries symbol_list VERI_UDP_IOSEP VERI_UDP_VAL VERI_UDP_IOSEP VERI_UDP_VAL VERI_UDP_LINESEP
      {
        VeriTreeNode tmp ; // VIPER #4707
        if ($4 == '-') tmp.Error("illegal current state symbol '%c' for sequential udp", $4) ;
        if ($6 == '?' || $6 == 'b' || $6 == 'B') tmp.Error("illegal next state symbol '%c' for sequential udp", $6) ;

        tmp_buf[tmp_buf_idx++] = ':' ;
        tmp_buf[tmp_buf_idx++] = $4 ;
        tmp_buf[tmp_buf_idx++] = ':' ;
        tmp_buf[tmp_buf_idx++] = $6 ;
        tmp_buf[tmp_buf_idx++] = '\0' ;
        VERIFIC_ASSERT(tmp_buf_idx < MAX_UDP_SYMBOLS) ;
        $$ = $1 ;
        ($$)->InsertLast(Strings::save(tmp_buf)) ;
      }
    ;

symbol_list :
      { tmp_buf_idx=0 ; udp_edge_indicator_found = 0 ; /* VIPER #8496: Start a table entry */ }
    | symbol_list symbol
      {
        VeriTreeNode tmp ; // VIPER #4878
        if ($2 == '-') tmp.Error("illegal input symbol '%c' for udp", $2) ;
        tmp_buf[tmp_buf_idx++] = $2 ;
      }
    | error
    ;

symbol : VERI_UDP_VAL
    | VERI_UDP_EDGE
      {
          $$ = $1 ;
          if (udp_edge_indicator_found) {
              // VIPER #8496: Check for multiple edge character in UDP table entry:
              VeriTreeNode tmp ;
              tmp.Error("syntax error near %s", VeriNode::PrintUdpEdgeChar($1)) ;
              //tmp.Info("multiple edge inputs in udp table entry are not allowed") ;
          }
          udp_edge_indicator_found = 1 ; // VIPER #8496: An edge symbol specified
      }
    | VERI_UDP_WILD_EDGE
      {
          $$ = $1 ;
          if (udp_edge_indicator_found) {
              // VIPER #8496: Check for multiple edge character in UDP table entry:
              VeriTreeNode tmp ;
              char wild_edge_char[2] = {0} ; wild_edge_char[0] = $1 ;
              tmp.Error("syntax error near %s", wild_edge_char) ;
              //tmp.Info("multiple edge inputs in udp table entry are not allowed") ;
          }
          udp_edge_indicator_found = 1 ; // VIPER #8496: An edge symbol specified
      }
    ;

/*********************** Concurrent Statements ********************/

continuous_assign : VERI_ASSIGN opt_strength opt_delay list_of_net_assignments ";"
      { $$ = new VeriContinuousAssign($2,$3,$4) ; }
    ;

list_of_net_assignments : net_assignment
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_net_assignments "," net_assignment
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

net_assignment : { implicit_nets_allowed = (VeriNode::IsVeri95()) ? 0 : 1 ; /* VIPER #1771/2070 */ } lvalue { implicit_nets_allowed = 0 ; } VERI_EQUAL expr_or_tagged_id_expr
      { $$ = new VeriNetRegAssign($2,$5) ; }
    ;

net_reg_assignment : lvalue VERI_EQUAL expr_or_tagged_id_expr
      { $$ = new VeriNetRegAssign($1,$3) ; }
    ;

initial_construct : VERI_INITIAL statement_or_null
      { $$ = new VeriInitialConstruct($2) ;
        // VIPER #8174: Do not issue warning for initial construct: as empty statement is allowed in initial construct.
        // if (!$2) ($$)->Warning("empty statement in %s", "initial construct") ; */ /* VIPER #8065*/
      }
    ;

always_construct : VERI_ALWAYS statement_or_null
      { $$ = new VeriAlwaysConstruct($2) ; if (!$2 || $2->IsNullStatement()) ($$)->Warning("empty statement in %s", "always construct") ;  /* VIPER #8065*/}
    ;

generate_construct : VERI_GENERATE generate_items VERI_ENDGENERATE
      { $$ = new VeriGenerateConstruct($2) ; }
    ;

   // System Verilog additions
net_alias : // a module item
      VERI_ALIAS {implicit_nets_allowed = 1 ;} lvalue VERI_EQUAL list_of_net_lvalues ";" // VIPER 2611: allow implicit nets
      {
        $$ = new VeriNetAlias($3, $5) ;
        implicit_nets_allowed = 0 ; // Reset flag
      }
    ;

list_of_net_lvalues : lvalue
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | list_of_net_lvalues VERI_EQUAL lvalue
      { $$ = $1 ; if ($$) $$->InsertLast($3) ; }
    ;

combinational_construct :
      VERI_ALWAYS_COMB statement_or_null
      { $$ = new VeriAlwaysConstruct($2) ; ($$)->SetQualifier(VERI_ALWAYS_COMB) ; }
    ;

latch_construct :
      VERI_ALWAYS_LATCH statement_or_null
      { $$ = new VeriAlwaysConstruct($2) ; ($$)->SetQualifier(VERI_ALWAYS_LATCH) ; }
    ;

ff_construct :
      VERI_ALWAYS_FF statement_or_null
      { $$ = new VeriAlwaysConstruct($2) ; ($$)->SetQualifier(VERI_ALWAYS_FF) ; }
    ;

final_construct :
      VERI_FINAL statement_or_null // actually function_statement_or_null
      { $$ = new VeriAlwaysConstruct($2) ; ($$)->SetQualifier(VERI_FINAL) ; }
    ;

/*********************** Sequential Statements ********************/
opt_statements_or_null : // Allow no statement in function/task
       { $$ = 0 ; }
     | statements_or_null // Allow statement(s)
       { $$ = $1 ; }
     ;

statements_or_null : statement_or_null
      { $$ = new Array() ; if ($1) ($$)->InsertLast($1) ; }
    | statements_or_null statement_or_null
      { $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    ;

// Statement can have attributes before, and comments around it
// Statements do not always have a closing label. Handled by seq_block/par_block itself.
statement : attr_inst pre_comment plain_statement opt_comment
      { $$ = $3 ;
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ; // attributes
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ; // pre-comments
        add_post_comments_to_node($$, $4) ; // trailing comments
      }
    | attr_inst timescale_specification
      {
          store_attribute($1) ; // Store attribute to global array
          // VIPER #3873: Parse and error out for `timscale inside module, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($2) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    ;

// Statement_or_null is used if other statements have a (open) 'statement' at end of rule
// Don't use rule 'opt_comment' at the end here, since it will force calling statements
// to include any post-statement parsed tokens in their line/file info.
// The downside of this is that comments after a statement_or_null
// will drop to (be attached to) the next statement over.
statement_or_null : attr_inst pre_comment plain_statement
      { $$ = $3 ;
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ; // attributes
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ; // pre-comments

        Array *comments = veri_file::TakeComments() ; // pick-up any comments that might apply to this statement..
        add_post_comments_to_node($$, comments) ; // trailing comments, but NOT forced
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create null statemnet
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | attr_inst timescale_specification
      {
          store_attribute($1) ; // Store attribute to global array
          // VIPER #3873: Parse and error out for `timscale inside module, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($2) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    ;
    // VIPER #4714: Rule to parse statement without null statement and post-comments
statement_wo_end_comment : attr_inst pre_comment plain_statement
      { $$ = $3 ;
        if (attr_inst_arr && $$) append_attributes($$) ; // Append global attribute to parse tree
        if ($1 && $$) ($$)->AddAttributes($1) ; // attributes
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ; // pre-comments

        Array *comments = veri_file::TakeComments() ; // pick-up any comments that might apply to this statement..
        add_post_comments_to_node($$, comments) ; // trailing comments, but NOT forced
      }
    | attr_inst timescale_specification
      {
          store_attribute($1) ; // Store attribute to global array
          // VIPER #3873: Parse and error out for `timscale inside module, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($2) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    ;
plain_statement:
      blocking_assignment
      { $$ = $1 ; }
    | non_blocking_assignment
      { $$ = $1 ; }
    | procedural_continuous_assignment
    | task_enable ";" // VIPER 2539 : Parsed ";" here, task_enable without semi is used as sequence_match_item
      { $$ = $1 ; if ($$) ($$)->SetLinefile(veri_file::GetRuleLineFile()) ; /* VIPER #4298: adjust linefile to include ; */ }
    | system_task_enable ";" // VIPER 2539 : Parsed ";" here, , system_task_enable without semi is used as sequence_match_item
      { $$ = $1 ; if ($$) ($$)->SetLinefile(veri_file::GetRuleLineFile()) ; /* VIPER #4298: adjust linefile to include ; */ }
    | procedural_timing_control_statement
    | conditional_statement
    | case_statement
    | wait_statement
    | loop_statement
    | disable_statement
    | event_trigger
    | seq_block
    | par_block
    | unique_priority_case_statement
    | unique_priority_conditional_statement
//    | function_call ";"  // covered by task_enable
    | VERI_VOID "'" "(" task_enable ")" ";"
      { $$ = $4 ; if ($$) ($$)->SetQualifier(VERI_VOID) ; /* CHECKME: Update linefile? */ }
    | VERI_VOID "'" "(" system_task_enable ")" ";" // VIPER #4700 : Allow system task enable
      { $$ = $4 ; if ($$) ($$)->SetQualifier(VERI_VOID) ; /* CHECKME: Update linefile? */ }
    | jump_statement
    | procedural_assertion_item
    | clocking_drive
//    | inc_or_dec_stmt  // Flatened to the next four rules for inclussion of the semicolon (;).
//      { $$ = $1 }      // inc_or_dec_stmt is used elsewhere which should not include the ';'!
    | lvalue VERI_INC_OP ";"
      { $$ = new VeriBlockingAssign($1,0,0,VERI_INC_OP) ; }
    | lvalue VERI_DEC_OP ";"
      { $$ = new VeriBlockingAssign($1,0,0,VERI_DEC_OP) ; }
    | VERI_INC_OP attr_inst lvalue ";"
      { $$ = new VeriBlockingAssign(0,0,$3,VERI_INC_OP) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_DEC_OP attr_inst lvalue ";"
      { $$ = new VeriBlockingAssign(0,0,$3,VERI_DEC_OP) ; if ($2) ($$)->AddAttributes($2) ; }
    | expect_property_statement // 3.1a
    | statement_label plain_statement // statement with a label. Canot put this in 'statement' rule itself, because it would collide with module item prefix chain.
      {
          $$ = $2 ;
          if ($1 && $2 && $2->GetOpeningLabel()) { // VIPER #3542 : Error out if opening label already exists.
              $2->Error("syntax error near %s", $1->GetName()) ;
          } else if ($1 && $2 && $2->GetId()) {
              $2->Error("statement label and block identifier are not allowed together") ; // VIPER #4205 : Error out if both statementlabel and blockidentifier are used
          } else if ($2 && $1) {
              ($2)->SetOpeningLabel($1) ;
          }
          process_labeled_stmt($1, $2) ; // VIPER #8303
          update_start_linefile($$) ;
          (void) pop_last_label() ; //last_label = 0 ; // reset last_label
      }
    | randsequence_statement // SV Std 1800 addition
    | randcase_statement // SV Std 1800 addition
    | error
      { $$ = 0 ; }
    ;

blocking_assignment :
//      operator_assignment // Flattened to the next rule for inclussion of the semicolon (;). operator_assignment is used elsewhere which should not include the ';'!
      lvalue assignment_operator expr_or_tagged_id_expr ";"
      { $$ = new VeriBlockingAssign($1,0,$3,$2) ; }
    | lvalue VERI_EQUAL delay_or_event_control expr_or_tagged_id_expr ";"
      { $$ = new VeriBlockingAssign($1,$3,$4) ; }
    | lvalue VERI_EQUAL new_expr ";"
      { $$ = new VeriBlockingAssign($1,0,$3) ; }
// RD: old 3.1 rule. Moved to class_scope in 1800..
//    | hier_ref parameter_value_assignment VERI_EQUAL new_expr ";"
//      { $$ = new VeriBlockingAssign($1,0,$4) ; }
    //| class_scope hier_ref VERI_EQUAL new_expr ";" // 1800 : produces conflict with hier_ref
     // { $$ = 0 ; }
//    | class_id "." randomize_call VERI_WITH constraint_block // collides. Implement like this :
    //| hier_ref VERI_WITH constraint_block ";"
      //{ $$ = new VeriBlockingAssign($1,0,$3,VERI_WITH) ; }
    ;

new_expr :
      VERI_NEW force_token_read
      { $$ = new VeriNew(0,0) ; }
    | VERI_NEW force_token_read "[" expr "]"
      { $$ = new VeriNew($4,0) ; }
    | VERI_NEW force_token_read "[" expr "]" "(" list_of_arguments ")"
      { $$ = new VeriNew($4,$7) ; }
    | VERI_NEW force_token_read "(" argument "," list_of_arguments ")"
      { if ($6) $6->InsertFirst($4) ; $$ = new VeriNew(0,$6) ; }
      // VIPER #3793: Support for both "new (expr)" and "new expr" - note the parenthesis.
      // The next rule covers "(expr)" also. Note that the above rule ensure atleast 2 arguments.
      // If the next rule is "(expr)", ie, parenthesized, then it is a call to the class method
      // 'new'. If it is not parenthesized then it is a copy constructor of the respective class.
    | VERI_NEW force_token_read expr // Std 1800 Addition
      {
          Array *args = new Array(1) ;
          args->Insert($3) ;
          VeriNew *new_call = new VeriNew(0, args) ;
          // If it is not a open parenthesis then it is a copy constructor call:
          if (($2) != VERI_OPAREN) new_call->SetAsCopyConstructor() ;
          $$ = new_call ;
      }
    | VERI_NEW force_token_read "(" ")"
      { $$ = new VeriNew(0,new Array(1) /* to differentiate between 'new' and 'new()' */) ; }
      // VIPER #4331: To support the construct like "new (.val(args))" i.e. argument passing by
      // name for "new", the followings are added.
    | VERI_NEW force_token_read "(" "." VERI_ID "("")"")"
      {
        Array *args = new Array(1) ;
        VeriPortConnect *port_connect = new VeriPortConnect($5,new VeriPortOpen()) ;
        args->Insert(port_connect) ;
        $$ = new VeriNew(0,args) ;
      }
    | VERI_NEW force_token_read "(""." VERI_ID "(" expr_or_tagged_id_expr")"")"
      {
          Array *args = new Array(1) ;
          VeriPortConnect *port_connect = new VeriPortConnect($5,$7) ;
          args->Insert(port_connect) ;
          $$ = new VeriNew(0,args) ;
      }
    ;

force_token_read : { $$ = yychar ; } %prec veri_force_token_read
    | VERI_FORCE_READ { VERIFIC_ASSERT(0) ; } // non-existing token can never match
    ;

operator_assignment :
      lvalue assignment_operator expr
      { $$ = new VeriBlockingAssign($1,0,$3,$2) ; }
    ;

assignment_operator :
      VERI_EQUAL { $$ = VERI_EQUAL_ASSIGN ; }
    | VERI_PLUS_ASSIGN { $$ = VERI_PLUS_ASSIGN ; }
    | VERI_MIN_ASSIGN { $$ = VERI_MIN_ASSIGN ; }
    | VERI_MUL_ASSIGN { $$ = VERI_MUL_ASSIGN ; }
    | VERI_DIV_ASSIGN { $$ = VERI_DIV_ASSIGN ; }
    | VERI_MOD_ASSIGN { $$ = VERI_MOD_ASSIGN ; }
    | VERI_AND_ASSIGN { $$ = VERI_AND_ASSIGN ; }
    | VERI_OR_ASSIGN { $$ = VERI_OR_ASSIGN ; }
    | VERI_XOR_ASSIGN { $$ = VERI_XOR_ASSIGN ; }
    | VERI_LSHIFT_ASSIGN { $$ = VERI_LSHIFT_ASSIGN ; }
    | VERI_ALSHIFT_ASSIGN { $$ = VERI_ALSHIFT_ASSIGN ; }
    | VERI_RSHIFT_ASSIGN { $$ = VERI_RSHIFT_ASSIGN ; }
    | VERI_ARSHIFT_ASSIGN { $$ = VERI_ARSHIFT_ASSIGN ; }
    ;

non_blocking_assignment :
      lvalue VERI_LEQ  expr_or_tagged_id_expr ";"
      { $$ = new VeriNonBlockingAssign($1,0,$3) ; }
    | lvalue VERI_LEQ delay_or_event_control expr_or_tagged_id_expr ";"
      { $$ = new VeriNonBlockingAssign($1,$3,$4) ; }
    ;

procedural_continuous_assignment :
      VERI_ASSIGN net_reg_assignment ";"
      { $$ = new VeriAssign($2) ; }
    | VERI_DEASSIGN lvalue ";"
      { $$ = new VeriDeAssign($2) ; }
    | VERI_FORCE net_reg_assignment ";"
      { $$ = new VeriForce($2) ; }
    | VERI_RELEASE lvalue ";"
      { $$ = new VeriRelease($2) ; }
    ;

procedural_timing_control_statement :
      delay_control statement_or_null
      { $$ = new VeriDelayControlStatement($1,$2) ; }
    | event_control_without_id statement_or_null
      { $$ = new VeriEventControlStatement($1,$2) ; }
    | "@" event_control_hier_ref statement_or_null
      { Array *event_control = new Array(1) ; event_control->InsertLast($2) ; $$ = new VeriEventControlStatement(event_control, $3) ; }
    | "@" system_func_id statement_or_null
       {
        VeriExpression *expr = new VeriSystemFunctionCall($2,0) ;
        Array *event_control = new Array(1) ;
        event_control->InsertLast(expr) ;
        $$ = new VeriEventControlStatement(event_control, $3) ;
       }
    ;

task_enable :
      task_enable_prefix
      {
          $$ = $1 ;
          // VIPER #4242/#8519 (test2.sv for hierarchical tree based elaborate):
          // If this is randomize task call, create inline contraint for it,
          // so that we can add scope of prefix class as extended scope of inline
          // constraint's local scope. This will help us to resolve the arguments
          // of randomize call where class objects can be used.
          VeriName *prefix = ($1) ? $1->GetTaskName() : 0 ;
          if (prefix && (prefix->GetFunctionType() == VERI_METHOD_RANDOMIZE)) {
              $$ = new VeriInlineConstraintStmt($$, 0) ;
          }
      }
      ;

task_enable_prefix :
      task_id //  VIPER 2539 : Don't add ";" here, without semi is used in sequence_match_item and loop_var_assignment
      { $$ = new VeriTaskEnable($1,0) ; }
    | hier_ref "(" list_of_arguments ")"
      { $$ = new VeriTaskEnable($1,$3) ; }
    | hier_ref real_attr_inst "(" list_of_arguments ")"
      { $$ = new VeriTaskEnable($1,$4) ; ($$)->AddAttributes($2) ; }
    | hier_ref "(" list_of_arguments ")" VERI_WITH "{" constraint_blocks "}" // SV Std 1800
      { $$ = new VeriInlineConstraintStmt(new VeriTaskEnable($1, $3), $7) ; }
    | task_id VERI_WITH "{" constraint_blocks "}" // SV Std 1800
      { $$ = new  VeriInlineConstraintStmt(new VeriTaskEnable($1, 0), $4) ; }
    | hier_ref "(" list_of_arguments ")" VERI_WITH "(" expr ")" // SV Std 1800
      { $$ = new VeriArrayMethodCall(new VeriTaskEnable($1, $3), $7) ; }
    | task_id VERI_WITH "(" expr ")" // SV Std 1800
      { $$ = new VeriArrayMethodCall(new VeriTaskEnable($1, 0), $4) ; }
    // VIPER #8016: Support task-enable like x.y(a).z.b(r).c(3,5)
    | extended_lvalue
      { $$ = new VeriTaskEnable($1,0) ; }
    | extended_lvalue "(" list_of_arguments ")"
      { $$ = new VeriTaskEnable($1, $3) ; }
    ;

system_task_enable :
      system_task_id // VIPER 2539 : Don't add ";" here, without semi is used in sequence_match_item and loop_var_assignment
      { $$ = new VeriSystemTaskEnable($1,0) ; }
    | system_task_id "(" list_of_arguments ")"
      { $$ = new VeriSystemTaskEnable($1,$3) ; }
    | system_task_id "(" data_type_without_type_ref ")" // SV Std 1800 addition, type_reference included in expr -> primary
      {
          Array * temp_arr = new Array() ;
          temp_arr->InsertLast($3) ;
          $$ = new VeriSystemTaskEnable($1, temp_arr) ;
      }
    | system_task_id "(" data_type_without_type_ref "," expr ")" // SV Std 1800 addition, type_reference included in expr -> primary
      {
          Array * temp_arr = new Array() ;
          temp_arr->InsertLast($3) ;
          temp_arr->InsertLast($5) ;
          $$ = new VeriSystemTaskEnable($1, temp_arr) ;
      }
    ;

delay_or_event_control :
      delay_control
      { $$ = new VeriDelayOrEventControl($1,0,0) ; }
    | event_control
      { $$ = new VeriDelayOrEventControl(0,0,$1) ; }
    | VERI_REPEAT "(" expr ")" event_control
      { $$ = new VeriDelayOrEventControl(0,$3,$5) ; }
    ;

delay_control :
      "#" delay_value
      { $$ = $2 ; }
    | "#" "(" mintypmax_expr ")"
      { $$ = $3 ; }
    | "#" VERI_UNSIZED_LITERAL
      {
        $$ = new VeriConstVal($2, VERI_UNSIZED_LITERAL, 1/*bit-size*/) ; Strings::free($2) ;
        ($$)->Warning("unexpected <'>, violates IEEE 1800 syntax") ;
      }
    ;

event_control : // event expression in Array form (list of expressions).
      "@" delay_event_hier_ref // VCS and P1800 accepts a hier identifier here.
       { $$ = new Array() ; ($$)->InsertLast($2) ; }
    | event_control_without_id
       { $$ = $1 ; }
    | "@" system_func_id // VIPER #5710 (vcs_compatibility: clocking)
       {
        VeriExpression *expr = new VeriSystemFunctionCall($2,0) ;
        $$ = new Array() ; ($$)->InsertLast(expr) ;
       }

    ;

event_control_without_id :
      "@" "(" event_control_expr ")"
       { $$ = $3 ; }
    | "@" "*"
       { $$ = new Array() ; /* Verilog 2000 LRM @* not clear on spaces */ }
    | "@" "(" "*" ")"
       { $$ = new Array() ; /* Verilog 2000 LRM not clear on spaces */ }
    | "@" "(*" ")"
       { $$ = new Array() ; /* Verilog 2000 LRM not clear on spaces */ }
    | "@" "(" "*)"
       { $$ = new Array() ; /* Verilog 2000 LRM not clear on spaces */ }
    ;

// Here is a full event_expression, expressed as an Array of VeriExpression. Each element should be 'OR'ed.
// This definition is usefull for 'event_control', where a list of events is needed, rather than a single expression.
event_control_expr : event_expr_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | event_control_expr VERI_OR event_expr_item
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    | event_control_expr "," event_expr_item
      { $$ = $1 ; ($$)->InsertLast($3) ; /* Semantics in Verilog 2000 */ }
    ;

// Here is a full event_expression, expressed as a VeriExpression. OR is implemented as OR operator.
event_expr : event_expr_item
      { $$ = $1 ; }
    | event_expr VERI_OR event_expr_item
      { $$ = new VeriBinaryOperator(VERI_OR, $1, $3) ; }
    | event_expr "," event_expr_item
      { $$ = new VeriBinaryOperator(VERI_OR, $1, $3) ;  }
    ;

// Here is a 'partial' event expression : plain 'expr' is NOT covered, and also there is no "," allowed in the expression.
// This one is used where comma-separated lists are in place : actuals (assoc lists) and formal decls (initial values for formals)
//actual_event_expr :
      //edge_id_expr { $$ = $1 ; }
    //| or_event_expr { $$ = $1 ; }
    //;

edge_id_expr : edge_id expr
      { $$ = new VeriEventExpression($1, $2) ; }
    //| expr VERI_IFF expr // present in sequence expr
    //| expr VERI_OR event_expr_item // present in sequence expr
      //{ $$ = new VeriEventExpression(0, $1, $3) ; }
    | edge_id expr VERI_IFF expr
      { $$ = new VeriEventExpression($1, $2, $4) ; }
    | edge_id expr VERI_OR event_expr
      { $$ = new VeriBinaryOperator(VERI_OR, new VeriEventExpression($1, $2), $4) ; }
    | edge_id expr VERI_IFF expr VERI_OR event_expr
      { $$ = new VeriBinaryOperator(VERI_OR, new VeriEventExpression($1, $2, $4), $6) ; }
    ;

//or_event_expr : event_expr_item VERI_OR actual_arg_expr
      //{ $$ = new VeriBinaryOperator(VERI_OR, $1, $3) ; }
    //;

//actual_arg_expr :
      // LRM says full event expression, but we cannot have "," in there, since that is a formal list iitem separator.
      // So only accept OR operators as event expression separators.
      //event_expr_item
      //{ $$ = $1 ; }
    //| actual_arg_expr VERI_OR actual_arg_expr
      //{ $$ = new VeriBinaryOperator(VERI_OR, $1, $3) ; }
    //;

event_expr_item :
      expr // VIPER #8387: 'expr' is already parenthesized
      { $$ = new VeriEventExpression(0, $1) ; }
    | parenthesized_event_expr_item ;
    ;

parenthesized_event_expr_item :
      edge_id expr
      { $$ = new VeriEventExpression($1, $2) ; }
    | expr VERI_IFF expr
      { $$ = new VeriEventExpression(0, $1, $3) ; }
    | edge_id expr VERI_IFF expr
      { $$ = new VeriEventExpression($1, $2, $4) ; }
    | "(" parenthesized_event_expr_item ")"
      {
          $$ = $2 ;
          // VIPER #8387: SV 2009 allows parenthesized event control, but others not.
          // Even SV 2005 do not allow it. So, check and error out for them:
          if (!VeriNode::IsSystemVeri2009OrLater()) {
              // But was not available prior to SV 2009:
              VeriTreeNode tmp_node ;
              tmp_node.Error("syntax error near %s", "(") ;
          }
      }
    ;

if_token : VERI_IF { $$ = present_scope ; } ;

conditional_statement :
      if_token "(" cond_predicate ")" statement_or_null %prec veri_no_else // Slightly lower precendence than 'else'
      { $$ = new VeriConditionalStatement($3,$5,0) ; present_scope = $1 ; }
    | if_token "(" cond_predicate ")" statement_or_null VERI_ELSE
      { present_scope = $1; }
      statement_or_null
      { $$ = new VeriConditionalStatement($3,$5,$8) ; }
    ;

unique_priority_conditional_statement :
      unique_priority conditional_statement
      { $$ = $2 ; if ($$) ($$)->SetQualifier($1) ; update_start_linefile($$) ; } // VIPER #2929 : disallow use of both unique and priority keywords with same if statement
    ;

matches_token : VERI_MATCHES { push_scope(0) ; $$ = present_scope ; } ;

cond_predicate :
      expr
    | expr matches_token pattern // Std 1800 Addition
      { $$ = new VeriPatternMatch($1, $3, $2) ; }
    | cond_predicate "&&&" expr
      { $$ = new VeriCondPredicate($1, $3) ; }
    | cond_predicate "&&&" expr matches_token pattern
      { $$ = new VeriCondPredicate($1, new VeriPatternMatch($3, $5, $4)); }
    ;

pattern : // Std 1800 Addition
       expr_or_tagged_id_expr
      { $$ = $1 ; }
    |  pattern_without_expr
      { $$ = $1 ; }
    ;

pattern_without_expr : // Std 1800 Addition
      VERI_DOT VERI_ID
      { VeriIdRef *name = new VeriIdRef($2) ; $$ = new VeriDotName(name) ; (void) declare_id(new VeriVariable(Strings::save(name->GetName()))) /* Create pattern identifier*/ ; }
    | VERI_DOTSTAR
      { $$ = new VeriDotStar() ; }
    // Viper 2569 : added rule 'tagged_id_expr'
    // moved below to that
    //| VERI_TAGGED VERI_ID pattern
      //{ $$ = new VeriTaggedUnion(new VeriIdRef($2), $3) ; }
    //| "'" "{" pattern_list "}"
      //{ $$ = new VeriAssignmentPattern(0, $3) ; }
    //| "'" "{" expr "{" concat_elements "}" "}"    // IEEE 1800 assignment pattern
        //{ $$ = new VeriMultiAssignmentPattern(0,$3,$5) ; }
    ;

pattern_list : // Std 1800 Addition
      pattern_element
      {
          $$ = new Array() ;  if ($$) $$->InsertLast($1) ;
      }
    | pattern_list "," pattern_element
      {
          // VIPER #6300: Check that the last item inserted is a valid one:
          VeriExpression *last_item = (($1) && ($1)->Size()) ? (VeriExpression *)($1)->GetLast() : 0 ;
          // VIPER #5626 (test 5): Allow this in AMS mode but not in SV mode:
          // VIPER #8249 : Produce error if any item is not present
          if ((!last_item || !$3) && !VeriNode::IsAms()) {
              VeriTreeNode node ;
              node.Error("syntax error near %s", ",") ;
          }
          $$ = $1 ; if ($$) $$->InsertLast($3) ;
      }
    ;

pattern_element : // VIPER #3220 : Allow expr with patterns as assignment pattern element
      pattern
      { $$ = $1 ; }
    | pattern ":" pattern
      { $$ = new VeriConcatItem($1, $3) ; }
    | simple_type ":" pattern
      { $$ = new VeriConcatItem(new VeriDataType($1, 0, 0), $3) ; }
    //| VERI_CHAR ":" pattern  // VIPER #8350: Moved this rule under 'simple_type'
    //  {
    //      $$ = new VeriConcatItem(new VeriDataType(VERI_CHAR, 0, 0), $3) ;
    //      ($$)->Warning("char as concat label violates IEEE 1800 syntax") ;
    //  }
    | VERI_STRINGTYPE ":" pattern // VIPER #4730: Allow string type as pattern key
      { $$ = new VeriConcatItem(new VeriDataType(VERI_STRINGTYPE, 0, 0), $3) ; }
    | VERI_DEFAULT ":" pattern
      { $$ = new VeriConcatItem(0, $3) ; }
    | { $$ = 0 ; } // Allow null element in AMS mode and in SV for '{} like expr
    ;

unique_priority :
      VERI_UNIQUE  { $$ = VERI_UNIQUE ; }
    | VERI_UNIQUE0  { $$ = VERI_UNIQUE0 ; }
    | VERI_PRIORITY  { $$ = VERI_PRIORITY ; }
    ;

case_statement : case_token "(" expr ")" opt_pragma opt_case_items VERI_ENDCASE
      {
          VeriCaseStatement *stmt = new VeriCaseStatement($1,$3,$6) ;
          if ($5) {
              // VIPER #2797: Call ProcessCaseStmtPragmas to set the full_case/parallel_case pragma flags
              // on this case statement and to NOT add them as attributes (they'll be removed them from the pragmas)
              (stmt)->ProcessCaseStmtPragmas($5) ;
              // Set pragmas right after case-expression as attributes on this statement
              (stmt)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ;
          }
          $$ = stmt ;
      }
    | case_token "(" expr ")" VERI_MATCHES opt_pragma case_pattern_items VERI_ENDCASE
      {
          VeriCaseStatement *stmt = new VeriCaseStatement($1, $3, $7) ;
          stmt->SetCaseType(VERI_MATCHES) ;
          if ($6) {
              // VIPER #2797: Call ProcessCaseStmtPragmas to set the full_case/parallel_case pragma flags
              // on this case statement and to NOT add them as attributes (they'll be removed them from the pragmas)
              (stmt)->ProcessCaseStmtPragmas($6) ;
              // Set pragmas right after case-expression as attributes on this statement
              (stmt)->AddAttributes(VeriNode::PragmasToAttributes(0,$6)) ;
          }
          $$ = stmt ;
      }
    | case_token "(" expr ")" VERI_INSIDE opt_pragma case_inside_items VERI_ENDCASE
      {
          // IEEE 1800 (2009) LRM section 12.5 "Case statement" BNF shows casex/casez cannot be used with inside.
          VeriCaseStatement *stmt = new VeriCaseStatement(VERI_CASE/*$1*/, $3, $7) ;
          if (($1)!=VERI_CASE) stmt->Error("inside operator not allowed with casex or casez") ; // All tools error out
          stmt->SetCaseType(VERI_INSIDE) ;
          if ($6) {
              // VIPER #2797: Call ProcessCaseStmtPragmas to set the full_case/parallel_case pragma flags
              // on this case statement and to NOT add them as attributes (they'll be removed them from the pragmas)
              (stmt)->ProcessCaseStmtPragmas($6) ;
              // Set pragmas right after case-expression as attributes on this statement
              (stmt)->AddAttributes(VeriNode::PragmasToAttributes(0,$6)) ;
          }
          $$ = stmt ;
      }
    ;

unique_priority_case_statement :
      unique_priority case_statement
      { $$ = $2 ; if ($$) ($$)->SetQualifier($1) ; update_start_linefile($$) ; } // VIPER #2929 : disallow use of both unique and priority keywords with same case statement
    ;

case_token : VERI_CASEX { $$ = VERI_CASEX ; }
    | VERI_CASEZ    { $$ = VERI_CASEZ ; }
    | VERI_CASE     { $$ = VERI_CASE ; }
    ;

opt_case_items : /* empty */
      {
          $$ = 0 ;
          VeriTreeNode tmp_node ;
          tmp_node.Warning("case statement with no case item violates IEEE 1800 syntax") ;
      }
    | case_items
      { $$ = $1 ; }
    ;

case_items : case_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | case_items case_item
      { $$ = $1 ; ($$)->InsertLast($2) ; }
    ;

case_item : expr_list ":" statement_or_null
      { $$ = new VeriCaseItem($1, $3) ;  }
    | VERI_DEFAULT opt_colon statement_or_null
      { $$ = new VeriCaseItem(0,$3) ; }
    ;

case_pattern_items : { push_scope(0) ; } case_pattern_item
      { $$ = new Array() ; ($$)->InsertLast($2) ; }
    | case_pattern_items { push_scope(0) ; } case_pattern_item
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

case_pattern_item : pattern ":" statement_or_null
      {
          Array *patterns = new Array(1) ;
          patterns->InsertLast($1) ;
          $$ = new VeriCaseItem(patterns, $3, pop_scope()) ;
      }
    | pattern "&&&" expr ":" statement_or_null
      {
          Array *patterns = new Array(1) ;
          patterns->InsertLast(new VeriCondPredicate($1, $3)) ;
          $$ = new VeriCaseItem(patterns, $5, pop_scope()) ;
      }
    | VERI_DEFAULT opt_colon statement_or_null
      { $$ = new VeriCaseItem(0, $3, pop_scope()) ; }
    ;

case_inside_items : case_inside_item
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | case_inside_items case_inside_item
      { $$ = $1 ; ($$)->InsertLast($2) ; }
    ;

case_inside_item : value_range_list ":" statement_or_null
      { $$ = new VeriCaseItem($1, $3) ; }
    | VERI_DEFAULT opt_colon statement_or_null
      { $$ = new VeriCaseItem(0, $3) ; }
    ;

opt_colon : | ":" ;

loop_statement :  // VIPER 2573, 2603 : Mark the scope created for loop as empty
      VERI_FOREVER opt_pragma { push_scope(0, 1) ; } statement_or_null
      { $$ = new VeriForever($4, pop_scope()) ; if ($2) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$2)) ; /* pragma set for Viper #5598 */ }
    | VERI_REPEAT opt_pragma { push_scope(0, 1) ; } "(" expr ")" statement_or_null
      { $$ = new VeriRepeat($5,$7, pop_scope()) ; if ($2) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$2)) ; /* pragma set for Viper #5598 */ }
    | VERI_WHILE opt_pragma { push_scope(0, 1) ; } "(" expr ")" statement_or_null
      { $$ = new VeriWhile($5,$7, pop_scope()) ; if ($2) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$2)) ; /* pragma set for Viper #5598 */ }
      // VIPER issue 2111 : loop var decl needs to be in inner scope. (comply with new ANSII style)
    | VERI_FOR opt_pragma { push_scope(0, 1) ; } "(" variable_decl_or_assignments ";" expr ";" variable_assignments ")" statement_or_null
      { $$ = new VeriFor($5,$7,$9,$11, pop_scope()) ; if ($2) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$2)) ; /* pragma set for Viper #5598 */ }
      // Check me : for FOR loop, should the variable_decl_or_assignments appear inside, or outside the loop scope ? Got them outside now..
    | VERI_DO opt_pragma { push_scope(0, 1) ; } statement_or_null VERI_WHILE "(" expr ")" ";"
      { $$ = new VeriDoWhile($4,$7,pop_scope()) ; if ($2) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$2)) ; /* pragma set for Viper #5598 */ }
    | VERI_FOREACH opt_pragma { push_scope(0) ; } "(" foreach_array_ref_loop_vars ")"  statement // SV Std 1800 addition
      { $$= new VeriForeach(($5).name, ($5).array, $7, pop_scope()) ; if ($2) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$2)) ; /* pragma set for Viper #5598 */ }
    ;

variable_decl_or_assignments :
      variable_assignments
    | variable_decls
    // VIPER #8062 : Mixing of variable assignment and declaration is not allowed
    // IEEE 1800-2012 section 12.7.1
    //| variable_assignments "," variable_decls
      //{ $$ = ($1) ? $1 : new Array() ; ($$)->Append($3) ; delete $3 ; }
    ;

variable_assignments :
      loop_var_assignment
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
// Here is a glimps of System Verilog : comma-separated list of assignments allowed :
    | variable_assignments "," loop_var_assignment
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
      // LRM violation, VCS compliance : empty 'variable_assignments' is allowed.
    ;

loop_var_assignment :
      operator_assignment
    | inc_or_dec_stmt
    | task_enable // SV Std 1800 addition
    | system_task_enable // SV Std 1800 addition
// VIPER 5248 : LRM violation : empty loop_var_assignment should not be allowed
    |
      {
          VeriTreeNode tmp_node ;
          tmp_node.Warning("empty loop variable assignment statement violates IEEE 1800 syntax") ;
          $$ = 0 ;
      }
    ;

variable_decls :
      loop_var_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | variable_decls "," loop_variable_identifier // late addition
      { $$ = ($1) ? ($1) : new Array() ;
        VeriDataDecl *last = ($$ && ($$)->Size()) ? (VeriDataDecl*)($$)->GetLast() : 0 ;
        if (last) last->AddDataDecl($3) ;
      }
    | variable_decls "," loop_var_decl
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    ;

loop_var_decl :
// Fixed type or named type declaration :
      data_type loop_variable_identifier // includes assignments
      { $$ = new VeriDataDecl(VERI_REG,$1,$2) ; }
      // VIPER #4193: Allow dimensions of for-loop variables with named types (typedefs):
    | hier_ref opt_dimensions loop_variable_identifier // includes assignments
      { $$ = new VeriDataDecl(VERI_REG, new VeriTypeRef($1,0,$2),$3) ; }
    ;

// VIPER #4193: Can't use variable_identifier here because that uses VERI_ID and we had to
// change the 'loop_var_decl' rule to 'hier_ref' to use dimensions. To avoid collision we
// are forced to use 'hier_ref' here and thus the new rule 'loop_variable_identifier':
// Use the routine 'declare_port' here. It does nothing specific for port. May be we should change the name.
loop_variable_identifier :
      // VIPER #4831 : Only allow variables with initial value as loop variable declaration (P1800 10.5)
      hier_ref VERI_EQUAL expr_or_tagged_id_expr
      {
          $$ = declare_port($1, 0, $3) ;
          if ($$) ($$)->SetIsAutomatic() ; // VIPER #6973: Set loop control variable as automatic variable
      }
    | hier_ref dimensions VERI_EQUAL expr_or_tagged_id_expr
      {
          VeriRange *dim = 0 ;
          GET_DIMENSION_RANGE(dim, $2) ;
          $$ = declare_port($1, dim, $4) ;
          if ($$) ($$)->SetIsAutomatic() ; // VIPER #6973: Set loop control variable as automatic variable
      }
    // VIPER 5717 (VCS Compatibility issue) : Allow loop variable declaration
    // without initial expression :
    | hier_ref
      {
          $$ = declare_port($1, 0, 0) ;
          if ($$) ($$)->SetIsAutomatic () ; // VIPER #6973: Set loop control variable as automatic variable
          VeriTreeNode tmp ; tmp.Warning("loop variable declaration without initialization violates IEEE 1800 syntax") ;
      }
    | hier_ref dimensions
      {
          VeriRange *dim = 0 ;
          GET_DIMENSION_RANGE(dim, $2) ;
          $$ = declare_port($1, dim, 0) ;
          if ($$) ($$)->SetIsAutomatic() ; // VIPER #6973: Set loop control variable as automatic variable
          VeriTreeNode tmp ; tmp.Warning("loop variable declaration without initialization violates IEEE 1800 syntax") ;
      }
    ;

jump_statement :
      VERI_RETURN ";"
      { $$ = new VeriJumpStatement(VERI_RETURN,0) ; }
    | VERI_RETURN expr ";"
      { $$ = new VeriJumpStatement(VERI_RETURN,$2) ; }
    | VERI_BREAK ";"
      { $$ = new VeriJumpStatement(VERI_BREAK,0) ; }
    | VERI_CONTINUE ";"
      { $$ = new VeriJumpStatement(VERI_CONTINUE,0) ; }
    ;

procedural_assertion_item :
      assert_cover_property_statement
    | immediate_assert_statement
    | sequential_instantiation
    ;

immediate_assert_statement : // triggered by a (boolean) expression. Not by a property expression
      VERI_ASSERT "(" expr ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSERT,0,$3,$5,0) ; if ($$) ($$)->SetIsImmediateAssertion() ; }
    | VERI_ASSERT "(" expr ")"  attr_inst ";"  %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSERT,0,$3,0,0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    //| VERI_ASSERT "(" expr ")" statement_or_null VERI_ELSE statement_or_null
      // VIPER #4714: Do not allow ";" before else keyword. That will stop us to
      // parse legal construct like
      // if (expr)
      //   assert(expr1) ;
      // else
      //   stmt
    | VERI_ASSERT "(" expr ")" statement_wo_end_comment VERI_ELSE statement_or_null
      {
        VeriAssertion *assert_stmt =  new VeriAssertion(VERI_ASSERT,0,$3,$5,$7) ;
        //if (!assert_stmt->GetThenStmt()) assert_stmt->Error("syntax error near %s", "else") ;
        if (!$7) assert_stmt->Error("syntax error near %s", "else") ;
        $$ = assert_stmt ; if ($$) ($$)->SetIsImmediateAssertion() ;
      }
    | VERI_ASSERT "(" expr ")" VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSERT,0,$3,0,$6) ;
        if (!$6 && $$) ($$)->Error("syntax error near %s", "else") ;
        if ($$) ($$)->SetIsImmediateAssertion() ;
      }
      // VIPER #6424: Added the following six simple immediate assertions according to IEEE 1800 (2009) LRM section A.6.10
    | VERI_ASSUME "(" expr ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSUME, 0, $3, $5, 0, 0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_ASSUME "(" expr ")"  attr_inst ";"  %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSUME, 0, $3, 0, 0, 0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_ASSUME "(" expr ")" statement_wo_end_comment VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSUME, 0, $3, $5, $7, 0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_ASSUME "(" expr ")" VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSUME, 0, $3, 0, $6, 0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
      // VIPER #7083: cover ( expr ) syntax allows simple statement_or_null, no need to use statement_wo_end_comment:
    | VERI_COVER "(" expr ")" statement_or_null //statement_wo_end_comment
      { $$ = new VeriAssertion(VERI_COVER, 0, $3, $5, 0, 0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    //| VERI_COVER "(" expr ")" attr_inst ";"  %prec veri_no_else
    //  { $$ = new VeriAssertion(VERI_COVER, 0, $3, 0, 0, 0) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    ;

 // SV-2009 feature : Deferred assertion statement
deferred_assertion :
      VERI_ASSERT hash_zero "(" expr ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSERT,0,$4,$6,0, 1 /* deferred */) ; if ($$) ($$)->SetIsImmediateAssertion() ; }
    | VERI_ASSERT hash_zero "(" expr ")"  attr_inst ";"  %prec veri_no_else
      { $$ = new VeriAssertion(VERI_ASSERT,0,$4,0,0, 1 /* deferred */) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    //| VERI_ASSERT "(" expr ")" statement_or_null VERI_ELSE statement_or_null
      // VIPER #4714: Do not allow ";" before else keyword. That will stop us to
      // parse legal construct like
      // if (expr)
      //   assert(expr1) ;
      // else
      //   stmt
    | VERI_ASSERT hash_zero "(" expr ")" statement_wo_end_comment VERI_ELSE statement_or_null
      {
        VeriAssertion *assert_stmt =  new VeriAssertion(VERI_ASSERT,0,$4,$6,$8, 1 /* deferred */) ; // Viper 6528
        //if (!assert_stmt->GetThenStmt()) assert_stmt->Error("syntax error near %s", "else") ;
        if (!$8) assert_stmt->Error("syntax error near %s", "else") ;
        $$ = assert_stmt ; if ($$) ($$)->SetIsImmediateAssertion() ;
      }
    | VERI_ASSERT hash_zero "(" expr ")" VERI_ELSE statement_or_null
      { $$ = new VeriAssertion(VERI_ASSERT,0,$4,0,$7, 1 /* deferred */) ; // Viper 6528
        if (!$7 && $$) ($$)->Error("syntax error near %s", "else") ;
        if ($$) ($$)->SetIsImmediateAssertion() ;
      }
    | VERI_ASSUME hash_zero "(" expr ")" statement_wo_end_comment %prec veri_no_else
       { $$ = new VeriAssertion(VERI_ASSUME, 0, $4, $6, 0, 1 /* deferred*/) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_ASSUME  hash_zero "(" expr ")"  attr_inst ";"  %prec veri_no_else
       { $$ = new VeriAssertion(VERI_ASSUME, 0, $4, 0, 0, 1 /* deferred*/) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_ASSUME hash_zero "(" expr ")" statement_wo_end_comment VERI_ELSE statement_or_null
       { $$ = new VeriAssertion(VERI_ASSUME, 0, $4, $6, $8, 1 /* deferred*/) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_ASSUME hash_zero "(" expr ")" VERI_ELSE statement_or_null
       { $$ = new VeriAssertion(VERI_ASSUME, 0, $4, 0, $7, 1 /* deferred*/) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_COVER hash_zero "(" expr ")" statement_wo_end_comment
       { $$ = new VeriAssertion(VERI_COVER, 0, $4, $6, 0, 1 /* deferred*/) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    | VERI_COVER hash_zero "(" expr ")" attr_inst ";"  %prec veri_no_else
       { $$ = new VeriAssertion(VERI_COVER, 0, $4, 0, 0, 1 /* deferred*/) ; if ($$) ($$)->SetIsImmediateAssertion() ;}
    ;

hash_zero : "#" VERI_NUM  // VERI_NUM should be 0
       { $$ = $2 ;
         if ($2 != 0) {
             VeriTreeNode tmp ;
             tmp.Error("syntax error near %s", "#") ;
         }
       }
    ;
    
wait_statement :
      VERI_WAIT "(" expr ")" statement_or_null
      { $$ = new VeriWait($3,$5) ; }
    | VERI_WAIT VERI_FORK ";"
      { $$ = new VeriWait(0,0) ; /*somewhat counter-intuitive*/ }
// as always : action block expanded for easier parse trees
    | VERI_WAIT_ORDER "(" hier_id_list ")" attr_inst ";" // VIPER #7083: Used statement_wo_end_comment instead of statement_or_null
      { $$ = new VeriWaitOrder($3, 0, 0) ; }
    | VERI_WAIT_ORDER "(" hier_id_list ")" statement_wo_end_comment %prec veri_no_else
      { $$ = new VeriWaitOrder($3, $5, 0) ; }
    | VERI_WAIT_ORDER "(" hier_id_list ")" statement_wo_end_comment VERI_ELSE statement_or_null
      { $$ = new VeriWaitOrder($3, $5, $7) ; }
    | VERI_WAIT_ORDER "(" hier_id_list ")" VERI_ELSE statement_or_null
      { $$ = new VeriWaitOrder($3, 0, $6) ; }
    ;

hier_id_list :
      hier_ref
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | hier_id_list "," hier_ref
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    ;

event_trigger :
     // VIPER #4027 : Allow indexed expression in event trigger :
      //"->" hier_ref opt_dimensions ";"
      VERI_RIGHTARROW hier_ref opt_dimensions ";"
      { $$ = new VeriEventTrigger(VERI_RIGHTARROW, ($3) ? create_indexed_name($2, $3) : $2) ; }
    | "->>" hier_ref opt_dimensions ";"
      { $$ = new VeriEventTrigger(VERI_RIGHTARRAW_ARROW, ($3) ? create_indexed_name($2, $3) : $2) ; }
    | "->>" delay_or_event_control hier_ref opt_dimensions ";"
      { $$ = new VeriEventTrigger(VERI_RIGHTARRAW_ARROW, (($4) ? create_indexed_name($3, $4) : $3), $2) ; }
    ;

disable_statement :
      VERI_DISABLE task_block_id ";"
      { $$ = new VeriDisable($2) ; }
    | VERI_DISABLE VERI_FORK ";"
      { $$ = new VeriDisable(0) ; /*somewhat counter-intuitive*/ }
    ;

 // Blocks
opt_block_label : // issue 2145 : all generate blocks get a scope, regardless of the label
      // VIPER #8109 : If for-generate-item is a generate-block, do not create
      // a new scope for generate-block, use for-generate-scope
      { $$ = 0 ;  if (!for_gen_started) push_scope(0) ; for_gen_started = 0 ; }
    | ":" block_decl_id
      { $$ = $2 ;
        if (for_gen_started) {
            // Declare block id to the upper scope of 'present_scope'
            VeriScope *upper = present_scope ? present_scope->Upper(): 0 ;
            VeriIdDef *exist = 0 ;
            if (upper && !upper->DeclareBlockId($$, 0, &exist)) {
                if ($$) $$->Error("%s is already declared", ($$)->Name()) ;
                if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; //
                delete $$ ;
                $$ = 0 ;
            }
            if (present_scope) {
                present_scope->SetOwner($$) ;
                if ($$) $$->SetLocalScope(present_scope) ;
            }
        } else {
            $$ = declare_block_id($$) ;
            push_scope($$) ;
        }
        for_gen_started = 0 ;
      }
    ;

opt_seq_block_label : // issue 2513/2573 : no scope for unlabeled (unnamed) seq/par blocks.
    // issue 2595, 2603 : always create a empty scope for unnamed block
      { $$ = 0 ;  push_scope(0, 1) ; }
    | ":" block_decl_id
      { $$ = declare_block_id($2) ; push_scope($$) ; }
    ;

// Keep closing label separate for block rules, so parse tree still has tight linefile info
seq_block :
      plain_seq_block %prec veri_no_colon
      { $$ = $1 ; }
    | plain_seq_block ":" VERI_ID // write closing label with an explicit rule, so that line/file info remains tight on rule without closing label
      { $$ = $1 ; if ($$) ($$)->ClosingLabel($3, get_last_label() /*last_label*/) ; update_end_linefile($$) ; }
    ;

plain_seq_block : // issue 2145 : all blocks get a scope, regardless of the label. Issue 2513 : not for seq/par blocks.
      VERI_BEGIN opt_seq_block_label block_item_decls block_statements VERI_END
      { $$ = new VeriSeqBlock($2,$3,$4, pop_scope()) ; }
    | VERI_BEGIN opt_seq_block_label block_item_decls VERI_END
      { $$ = new VeriSeqBlock($2,$3,0, pop_scope()) ; }
    | VERI_BEGIN opt_seq_block_label block_statements VERI_END
      { $$ = new VeriSeqBlock($2,0,$3, pop_scope()) ; }
    | VERI_BEGIN opt_seq_block_label VERI_END
      { $$ = new VeriSeqBlock($2,0,0, pop_scope()) ; }
    ;

par_block :
      plain_par_block %prec veri_no_colon
      { $$ = $1 ; }
    | plain_par_block ":" VERI_ID // write closing label with an explicit rule, so that line/file info remains tight on rule without closing label
      { $$ = $1 ; if ($$) ($$)->ClosingLabel($3, get_last_label() /*last_label*/) ; update_end_linefile($$) ; }
    ;

plain_par_block : // issue 2145 : all blocks get a scope, regardless of the label. Issue 2513 : not for seq/par blocks.
      VERI_FORK opt_seq_block_label block_item_decls block_statements join_keyword
      { $$ = new VeriParBlock($2,$3,$4, pop_scope(),$5) ; }
    | VERI_FORK opt_seq_block_label block_item_decls join_keyword
      { $$ = new VeriParBlock($2,$3,0, pop_scope(),$4) ; }
    | VERI_FORK opt_seq_block_label block_statements join_keyword
      { $$ = new VeriParBlock($2,0,$3, pop_scope(),$4) ; }
    | VERI_FORK opt_seq_block_label join_keyword
      { $$ = new VeriParBlock($2,0,0, pop_scope(),$3) ; }
    ;

join_keyword : VERI_JOIN { $$ = VERI_JOIN ; }
    | VERI_JOIN_ANY { $$ = VERI_JOIN_ANY ; }
    | VERI_JOIN_NONE { $$ = VERI_JOIN_NONE ; }
    ;

block_statements :  statement
      { $$ = new Array() ; if ($1) ($$)->InsertLast($1) ; }
    | ";" // Because 'statement_or_null' is allowed in a block
      { $$ = 0 ; }
    | block_statements statement
      { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | block_statements ";" // VIPER 2346 : 'statement_or_null' is allowed in a block
      { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast(0) ; }
    ;

block_item_decls : block_item_decl
      { $$ = new Array() ; if ($1) ($$)->InsertLast($1) ; }
    | block_item_decls block_item_decl
      { $$ = ($1) ? ($1) : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    ;

// block_item_decl_statement can have attributes and comments around it
block_item_decl : attr_inst pre_comment plain_block_item_decl opt_comment
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
      }
    ;

plain_block_item_decl :
      data_decl
    | local_parameter_decl
    | parameter_decl
    | overload_declaration
    | let_declaration
    ;

/*********************** Clocking Domain *************************/

clocking_decl : // a module item
      opt_default VERI_CLOCKING opt_clocking_decl_id clocking_event ";"
      { push_scope($3) ; }
        clocking_items
      VERI_ENDCLOCKING
      {
          // VIPER #6302: Check and error out for already declared clocking id with a body:
          if (($3) && ($3)->GetModuleItem()) {
              VeriTreeNode tmp ; set_linefile_from_rule(tmp, @3) ;
              tmp.Error("%s is already declared", ($3)->Name()) ;
              ($3)->Info("previous declaration of %s is from here", ($3)->Name()) ; // VIPER #7773
          }
          $$ = new VeriClockingDecl($3,$4,$7,pop_scope()) ;
          // VIPER #4842: Check for multiple clocking declaration in same scope:
          if ($1 && !check_default_clocking($$)) ($$)->SetQualifier($1) ;
      }
    | opt_default VERI_CLOCKING opt_clocking_decl_id ";" // forward declaration (prototype) ? Collides with header of clocking_decl.
      {
          $$ = new VeriClockingDecl($3,0,0,0) ;
          // VIPER #4842: Check for multiple clocking declaration in same scope:
          if ($1 && !check_default_clocking($$)) ($$)->SetQualifier($1) ;
      }
      // VIPER #5710: (vcs-compatibility: always_checker):
      // IEEE-P1800(2009) LRM added a new clocking declaration global_clocking:
    | VERI_GLOBAL VERI_CLOCKING opt_clocking_decl_id clocking_event ";"
      { push_scope($3) ; }
      VERI_ENDCLOCKING
      {
          // VIPER #6302: Check and error out for already declared clocking id with a body:
          if (($3) && ($3)->GetModuleItem()) {
              VeriTreeNode tmp ; set_linefile_from_rule(tmp, @3) ;
              tmp.Error("%s is already declared", ($3)->Name()) ;
              ($3)->Info("previous declaration of %s is from here", ($3)->Name()) ; // VIPER #7773
          }
          $$ = new VeriClockingDecl($3,$4,0,pop_scope()) ;
          ($$)->SetQualifier(VERI_GLOBAL) ;
      }
    // VIPER #5710 : VCS Compatibility issue (disable) : Support for default disable iff
    | VERI_DEFAULT VERI_DISABLE VERI_IFF expression_or_dist ";"
      { $$ = new VeriDefaultDisableIff($4) ; }
    ;

opt_default : { $$ = 0 ; } | VERI_DEFAULT { $$ = VERI_DEFAULT ; } ;

clocking_items :
      { $$ = 0 ; }
    | clocking_items clocking_item
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

clocking_item : // a module item
      VERI_DEFAULT direction_skew ";" // i or o
      {
          $$ = new VeriClockingSigDecl($2, 0, 0) ;
          // VIPER #6652: inout direction is not allowed in default skew (only input or output is allowed):
          if (($2)->GetDirection()==VERI_INOUT) ($2)->Error("syntax error near %s", "inout") ;
      }
    | VERI_DEFAULT direction_skew direction_skew ";" // i and o
      {
          $$ = new VeriClockingSigDecl($2, $3, 0) ;
          // VIPER #6652: Check for directions - it should be "input" first and then "output":
          if (($2)->GetDirection()!=VERI_INPUT)       { ($2)->Error("syntax error near %s", ($2)->PrintToken(($2)->GetDirection())) ; }
          else if (($3)->GetDirection()!=VERI_OUTPUT) { ($3)->Error("syntax error near %s", ($3)->PrintToken(($3)->GetDirection())) ; }
      }
    | direction_skew clocking_decl_assigns ";" // i or o
      { $$ = new VeriClockingSigDecl($1,0,$2) ; }
    | direction_skew direction_skew clocking_decl_assigns ";" // i and o
      {
          $$ = new VeriClockingSigDecl($1,$2,$3) ;
          // VIPER #6652: Check for directions - it should be "input" first and then "output":
          if (($1)->GetDirection()!=VERI_INPUT)       { ($1)->Error("syntax error near %s", ($1)->PrintToken(($1)->GetDirection())) ; }
          else if (($2)->GetDirection()!=VERI_OUTPUT) { ($2)->Error("syntax error near %s", ($2)->PrintToken(($2)->GetDirection())) ; }
      }
    | attr_inst concurrent_assertion_item_decl opt_closing_label // Add optional closing label here
      { $$ = $2 ; if ($$ && $1) ($$)->AddAttributes($1) ; if ($$ && $3) ($$)->ClosingLabel($3) ; if ($3) update_end_linefile($$) ; /* VIPER #6113 */ if ($1) update_start_linefile($$) ; /* VIPER #7563 */ }
    ;

direction_skew :
      clocking_direction
      { $$ = new VeriClockingDirection($1, 0, 0) ; }
    | clocking_direction clocking_skew
      {
          $$ = $2 ; if ($$) $$->SetDirection($1) ;
          // VIPER #6652: inout direction cannot follow a clocking skew (2009 LRM section A.6.11).
          if (($1)==VERI_INOUT) ($$)->Error("syntax error near %s", "inout") ;
      }
    ;

clocking_decl_assigns :
      clocking_decl_assign
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | clocking_decl_assigns "," clocking_decl_assign
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    ;

clocking_decl_assign :
      clock_signal_decl_id
      { $$ = $1 ; /* signal identifier.. */ }
      // VIPER #4765 : Allow expression as initial value:
    | clock_signal_decl_id VERI_EQUAL expr
      { $$ = $1 ; if ($1) ($1)->SetInitialValue($3) ; }
    ;

clocking_skew : // an expression ?
      delay_control
      { $$ = new VeriClockingDirection(0, 0, new VeriDelayOrEventControl($1, 0, 0)) ; }
    | edge_id delay_control
      { $$ = new VeriClockingDirection(0, $1, new VeriDelayOrEventControl($2, 0, 0)) ; }
    | edge_id // SV Std 1800 addition
      { $$ = new VeriClockingDirection(0, $1, 0) ; }
    ;

clocking_drive : // a VeriStatement
      // have to do 'lvalue' to avoid collision with 'blocking/nonblocking assignment,
      // Also, if 'cycle_delay' is NOT there, this IS a non_blocking assignment.
      lvalue VERI_LEQ cycle_delay expr ";" /* clockvar reference */
      {
         // Viper 4504 : store cycle delay information in VeriDelayOrEventControl
         VeriDelayOrEventControl *delay = new VeriDelayOrEventControl($3, 0, 0) ;
         delay->SetIsCycleDelay() ;
         $$ = new VeriNonBlockingAssign($1, delay, $4) ; /* FIX ME : store cycle_delay' */
      }
    // Combine clockvar reference (cycle_delay lvalue VERI_LEQ expr) and procedural
    // timing control stmt (cycle_delay statement_or_null)
    | cycle_delay statement_or_null
      {
         // Viper 4504 : store cycle delay information in VeriDelayControlStatement
         VeriDelayControlStatement *delay =  new VeriDelayControlStatement($1, $2) ;
         delay->SetIsCycleDelay() ;
         $$ = delay ;
      }
    ;

cycle_delay : cycle_delay_range
    // cannot do a plain   "## expr", as the LRM says, because we get into the " expr expr " problem in 'clocking_drive'.
    // So use the more restrictive 'cycle_delay_range', which is used everywhere else.
      { $$ = $1 ; }
    ;

clocking_direction : // VIPER #6652: Only input, output and inout directions are allowed
      VERI_INPUT     { $$ = VERI_INPUT ; }
    | VERI_OUTPUT    { $$ = VERI_OUTPUT ; }
    | VERI_INOUT     { $$ = VERI_INOUT ; }
    ;

/*********************** Specify Section *************************/

specify_block : VERI_SPECIFY { /* push_scope(0) */ } specify_items VERI_ENDSPECIFY
      {
        /* ; Issue 1530/1069/1316/1753 : XL-IEEE difference : XL does NOT use a scope here (in specify block) */
        $$ = new VeriSpecifyBlock($3,0/*pop_scope()*/) ;
      }
    ;

specify_items :
      { $$ = new Array() ; }
    | specify_items specify_item
      { $$ = $1 ; ($$)->InsertLast($2) ; }
    ;

// specify_item can have attributes or comments around it
specify_item : attr_inst pre_comment plain_specify_item opt_comment
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ; // attributes
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ; // pre-comments
        add_post_comments_to_node($$, $4) ; // trailing comments
      }
    ;

plain_specify_item :
      specparameter_decl
    | path_decl
    | system_timing_check
    // Verilog 2000 additions :
    | VERI_PULSESTYLE_ONEVENT term_list ";"
      {
        $$ = new VeriPulseControl(VERI_PULSESTYLE_ONEVENT, $2) ;
      }
    | VERI_PULSESTYLE_ONDETECT term_list ";"
      {
        $$ = new VeriPulseControl(VERI_PULSESTYLE_ONDETECT, $2) ;
      }
    | VERI_SHOWCANCELLED term_list ";"
      {
        $$ = new VeriPulseControl(VERI_SHOWCANCELLED, $2) ;
      }
    | VERI_NOSHOWCANCELLED term_list ";"
      {
        $$ = new VeriPulseControl(VERI_NOSHOWCANCELLED, $2) ;
      }
    | parameter_decl
      {
          $$ = $1 ;
          if ($$) ($$)->Warning("parameter declaration inside specify block violates IEEE 1800 syntax") ;
      }
    ;

specparameter_decl :
      VERI_SPECPARAM list_of_specparam_decls ";"
      { $$ = new VeriDataDecl(VERI_SPECPARAM, 0,$2) ; /* don't create a data-type here, since default size should adjust to expression */ }
    | VERI_SPECPARAM dimension list_of_specparam_decls ";"
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriDataDecl(VERI_SPECPARAM, new VeriDataType(0,0,dim),$3) ; /* Verilog 2000 : optional range */ }
    ;

list_of_specparam_decls : specparam_decl
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_specparam_decls "," specparam_decl
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

specparam_decl : specparam_decl_id VERI_EQUAL mintypmax_expr
    {
      // Another IEEE LRM inconsistency.
      // mintypmax_expr as specparam value is not allowed by BNF
      // (only 'expression' is. But examples are given that state
      // the contrary. Allow it.
      $$ = $1 ;
      if ($1) {
          ($1)->SetInitialValue($3) ;
      } else {
          delete $3;
      }
    }
    | specparam_decl_id VERI_EQUAL "(" mintypmax_expr "," mintypmax_expr ")"
    {
      // This is to support PATHPULSE specparams with two limit values
      $$ = $1 ;
      // VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
      VeriPathPulseVal *path_pulse_val = new VeriPathPulseVal($4, $6) ;
      // FIX ME : check that the specparam is actually a pathpulse variable.
      if ($1) {
          ($1)->SetInitialValue(path_pulse_val) ;
      } else {
           delete path_pulse_val ;
      }
    }
    ;

path_decl : path_description VERI_EQUAL path_delay_value ";"
          { $$ = new VeriPathDecl(0,0,$1,$3) ; }
        | VERI_IF "(" expr ")" path_description VERI_EQUAL path_delay_value ";"
          { /* state_dependent path decl */ $$ = new VeriPathDecl(0,$3,$5,$7) ; }
        | VERI_IFNONE path_description VERI_EQUAL path_delay_value ";"
          {
              /* state_dependent path decl */
              // VIPER #7640: Only simple path without the edge is allowed here:
              // VIPER #7680: Do not produce the error in baseline, make it customer specific for VIPER #7640:
              //if (($2) && ($2)->GetEdge()) ($2)->Error("syntax error near %s", VeriNode::PrintToken(($2)->GetEdge())) ;
              $$ = new VeriPathDecl(1,0,$2,$4) ;
          }
    ;

path_description :
    /* simple path description */
     "(" term_list opt_polarity path_desc term_list ")"
      {
          $$ = new VeriPath($2,$3,$4,$5) ;
          // VIPER #6669: Multiple items not allowed in parallel path description:
          if ((($4)==VERI_LEADTO) && ((($2) && (($2)->Size()>1)) || (($5) && (($5)->Size()>1))))
              ($$)->Warning("only one terminal allowed as the source or destination of parallel paths") ;
      }

      // VIPER 3040 : catch SV token sequence "+=" and ">" as valid polarity path_desc.
      // Better would be to introduce tokens "+=>" and "-=>", because the separate token is really not allowed..
    | "(" term_list VERI_PLUS_ASSIGN VERI_GT term_list ")"
      { $$ = new VeriPath($2,VERI_PLUS,VERI_LEADTO,$5) ; }
    | "(" term_list VERI_MIN_ASSIGN VERI_GT term_list ")"
      { $$ = new VeriPath($2,VERI_MIN,VERI_LEADTO,$5) ; }

    /* edge-sensitive path description */
    | "(" term_list opt_polarity path_desc "(" term_list edge_pol expr ")" ")"
      { $$ = new VeriPath(0,$2,$3, $4,$6,$7,$8) ;
        /* VIPER 2746 : polarity not allowed before path desc */
        if ($3) ($$)->Warning("polarity not allowed before %s in edge-sensitive paths",VeriNode::PrintToken($4)) ;
        // VIPER #6669: Multiple items not allowed in parallel path description:
        if ((($4)==VERI_LEADTO) && ((($2) && (($2)->Size()>1)) || (($6) && (($6)->Size()>1))))
            ($$)->Warning("only one terminal allowed as the source or destination of parallel paths") ;
      }
    | "(" term_list VERI_PLUS_ASSIGN VERI_GT "(" term_list edge_pol expr ")" ")"
      { $$ = new VeriPath(0,$2,VERI_PLUS, VERI_LEADTO,$6,$7,$8) ;
        /* VIPER 2746 : polarity not allowed before path desc */
        ($$)->Warning("polarity not allowed before %s in edge-sensitive paths",VeriNode::PrintToken(VERI_LEADTO)) ;
      }
    | "(" term_list VERI_MIN_ASSIGN VERI_GT "(" term_list edge_pol expr ")" ")"
      { $$ = new VeriPath(0,$2,VERI_MIN, VERI_LEADTO,$6,$7,$8) ;
        /* VIPER 2746 : polarity not allowed before path desc */
        ($$)->Warning("polarity not allowed before %s in edge-sensitive paths",VeriNode::PrintToken(VERI_LEADTO)) ;
      }
    | "(" edge_id term_list opt_polarity path_desc "(" term_list edge_pol expr ")" ")"
      { $$ = new VeriPath($2,$3,$4,$5,$7,$8,$9) ;
        /* VIPER 2746 : polarity not allowed before path desc */
        if ($4) ($$)->Warning("polarity not allowed before %s in edge-sensitive paths",VeriNode::PrintToken($5)) ;
        // VIPER #6669: Multiple items not allowed in parallel path description:
        if ((($5)==VERI_LEADTO) && ((($3) && (($3)->Size()>1)) || (($7) && (($7)->Size()>1))))
            ($$)->Warning("only one terminal allowed as the source or destination of parallel paths") ;
      }
    | "(" edge_id term_list VERI_PLUS_ASSIGN VERI_GT "(" term_list edge_pol expr ")" ")"
      { $$ = new VeriPath($2,$3,VERI_PLUS,VERI_LEADTO,$7,$8,$9) ;
        /* VIPER 2746 : polarity not allowed before path desc */
        ($$)->Warning("polarity not allowed before %s in edge-sensitive paths",VeriNode::PrintToken(VERI_LEADTO)) ;
      }
    | "(" edge_id term_list VERI_MIN_ASSIGN VERI_GT "(" term_list edge_pol expr ")" ")"
      { $$ = new VeriPath($2,$3,VERI_MIN,VERI_LEADTO,$7,$8,$9) ;
        /* VIPER 2746 : polarity not allowed before path desc */
        ($$)->Warning("polarity not allowed before %s in edge-sensitive paths",VeriNode::PrintToken(VERI_LEADTO)) ;
      }
    | "(" edge_id term_list opt_polarity path_desc term_list ")"
      { $$ = new VeriPath($2,$3,$4,$5,$6,0,0) ;
        // VIPER 5248: Verilog superset parsing
        ($$)->Warning("edge path description violates IEEE 1800 syntax") ;
        // VIPER #6669: Multiple items not allowed in parallel path description:
        if ((($5)==VERI_LEADTO) && ((($3) && (($3)->Size()>1)) || (($6) && (($6)->Size()>1))))
            ($$)->Warning("only one terminal allowed as the source or destination of parallel paths") ;
      }
    | "(" edge_id term_list VERI_PLUS_ASSIGN VERI_GT term_list ")"
      { $$ = new VeriPath($2,$3,VERI_PLUS,VERI_LEADTO,$6,0,0) ;
        // VIPER 5248: Verilog superset parsing
        ($$)->Warning("edge path description violates IEEE 1800 syntax") ;
      }
    | "(" edge_id term_list VERI_MIN_ASSIGN VERI_GT term_list ")"
      { $$ = new VeriPath($2,$3,VERI_MIN,VERI_LEADTO,$6,0,0) ;
        // VIPER 5248: Verilog superset parsing
        ($$)->Warning("edge path description violates IEEE 1800 syntax") ;
      }
    | "(" edge_id edge_term_list opt_polarity path_desc term_list ")"
      { $$ = new VeriPath($2,$3,$4,$5,$6,0,0) ;
        // VIPER 5248: Verilog superset parsing
        ($$)->Warning("edge path description violates IEEE 1800 syntax") ;
        // VIPER #6669: Multiple items not allowed in parallel path description:
        if ((($5)==VERI_LEADTO) && ((($3) && (($3)->Size()>1)) || (($6) && (($6)->Size()>1))))
            ($$)->Warning("only one terminal allowed as the source or destination of parallel paths") ;
      }
    | "(" edge_id edge_term_list VERI_PLUS_ASSIGN VERI_GT term_list ")"
      { $$ = new VeriPath($2,$3,VERI_PLUS,VERI_LEADTO,$6,0,0) ;
        // VIPER 5248: Verilog superset parsing
        ($$)->Warning("edge path description violates IEEE 1800 syntax") ;
      }
    | "(" edge_id edge_term_list VERI_MIN_ASSIGN VERI_GT term_list ")"
      { $$ = new VeriPath($2,$3,VERI_MIN,VERI_LEADTO,$6,0,0) ;
        // VIPER 5248: Verilog superset parsing
        ($$)->Warning("edge path description violates IEEE 1800 syntax") ;
      }
    ;

edge_term_list : term_list "," edge_id term
      {
        //$$ = new Array() ;
        //$$->InsertLast($1) ;
        VeriEventExpression *event_expr = new VeriEventExpression($3, $4) ;
        $1->InsertLast(event_expr) ;
        $$ = $1 ;
      }
    | edge_term_list "," term
      {
        $1->InsertLast($3) ;
        $$ = $1 ;
      }
    | edge_term_list "," edge_id term
      {
        VeriEventExpression *event_expr = new VeriEventExpression($3, $4) ;
        $1->InsertLast(event_expr) ;
        $$ = $1 ;
      }
    ;
    // Here, we cannot do the same trick as with range partselects above.
    // In Verilog'95 mode, path edge polarity is allowed, and since PARTSELECT_UP/DOWN
    // are Verilog'2001 tokens, we should allow separate "+" and ":" tokens as valid here.
    // VIPER-1623
edge_pol : ":"      { $$ = 0 ; }
    | VERI_PARTSELECT_UP   { $$ = VERI_PLUS ; }
    | VERI_PARTSELECT_DOWN { $$ = VERI_MIN ; }
    | VERI_PLUS ":" { $$ = VERI_PLUS ; }
    | VERI_MIN  ":" { $$ = VERI_MIN ; }
    ;

edge_id : VERI_POSEDGE { $$ = VERI_POSEDGE ; }
    | VERI_NEGEDGE     { $$ = VERI_NEGEDGE ; }
    | VERI_EDGE
      {
          // VIPER #6748: 'edge' is also a valid transition in SV 2009
          if (!VeriNode::IsSystemVeri()) {
              // But was not available prior to SV 2009:
              VeriTreeNode tmp_node ;
              tmp_node.Error("syntax error near %s", "edge") ;
          }
          $$ = VERI_EDGE ;
      }
    ;

path_desc : VERI_LEADTO { $$ = VERI_LEADTO ; }
    | VERI_ALLPATH { $$ = VERI_ALLPATH ; }
    ;

term_list : term
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | term_list "," term
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

term : port_ref  // Just id, id[], or id[:]
      { $$ = $1 ; }
    ;

opt_polarity : { $$ = 0 ; }
    | VERI_PLUS { $$ = VERI_PLUS ; }
    | VERI_MIN  { $$ = VERI_MIN ; }
    ;

path_delay_value : mintypmax_expr_list
      { $$ = $1 ; }
    | "(" mintypmax_expr_list "," mintypmax_expr ")"
      // single element in parenthesis is already covered in first rule as ()'ed mintypmax_expr
      // So only accept multiple elements here
      { $$ = $2 ; if ($4 && $$) ($$)->InsertLast($4) ; }
    ;

system_timing_check : system_task_id "(" timing_check_args ")" ";"
      { $$ = new VeriSystemTimingCheck($1,$3) ; }
    ;

timing_check_args : timing_check_arg
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | timing_check_args "," timing_check_arg
      { $$ = $1 ; ($$)->InsertLast($3) ; }
    ;

timing_check_arg :
      { /* Allow empty timing check arguments */ $$ = 0 ; }
    | mintypmax_expr
      { $$ = new VeriTimingCheckEvent($1) ; }
    | expr VERI_EDGE_AMPERSAND expr
      { $$ = new VeriTimingCheckEvent($1,0,$3,0) ; }
    | timing_check_event_control edge_desc_list term
      { $$ = new VeriTimingCheckEvent($3,$1,0,$2) ; }
    | timing_check_event_control edge_desc_list term VERI_EDGE_AMPERSAND expr
      { $$ = new VeriTimingCheckEvent($3,$1,$5,$2) ; }
    ;

timing_check_event_control :
      VERI_POSEDGE  { $$ = VERI_POSEDGE ; }
    | VERI_NEGEDGE  { $$ = VERI_NEGEDGE ; }
    | VERI_EDGE     { $$ = VERI_EDGE ; }
    ;

edge_desc_list :    { $$ = 0 ; }
    | VERI_EDGEDESCRIPTOR   { $$ = $1 ; }
    ;

/*********************** Constant Expressions *****************************/

// Note : we do 'constant' analysis in Resolve(), after parse tree construction.
// So during parsing, we do not need to call 'const_expr' when a constant expression
// is required in the LRM. Where the LRM states const_expr, we use expr,
// and when the LRM uses const_mintypmaxexpr, we use mintypmaxexpr.

/*********************** Expressions *****************************/

number : VERI_NUM
      { $$ = new VeriIntVal((int)($1)) ; }
    | VERI_NUM VERI_BASE VERI_BASED_NUM
      {
        char *tmp = Strings::save($2, $3) ;
        $$ = new VeriConstVal(tmp,VERI_BASED_NUM, $1) ;
        Strings::free(tmp) ; Strings::free($2) ; Strings::free($3) ;
        // VIPER #6601: IEEE 1800 (2009) sec 5.7.1 size shall be specified as a non-zero
        // unsigned decimal number. Produce warning here, no other tools produce any message!
        // Note that size cannot be negative here since flex and bison rule do not allow it.
        if (!($1)) ($$)->Warning("invalid size of integer constant literal") ;
      }
    | VERI_BASE VERI_BASED_NUM
      {
        char *tmp = Strings::save($1, $2) ;
        $$ = new VeriConstVal(tmp,VERI_BASED_NUM) ;
        Strings::free(tmp) ; Strings::free($1) ; Strings::free($2) ;
      }
    | VERI_REAL_NUM
      { $$ = new VeriRealVal($1) ; }
    ;

/*********************** Expressions *****************************/

lvalue :
      hier_ref
      {
         $$ = $1 ;
         // Verilog 2000 : LHS of net-assignments allow implicit nets. Issue
         // Declare a implicit net if we are in lvalue of a non-blocking assignment (implicit_nets_allowed is set),
         // and the net is not visible (not declare up till this point).
         // Need to do this here, due to 'order' problems in Resolve()
         // (see VIPER issues 1289, 1771, 1847).
         if (implicit_nets_allowed && resolve_idrefs && ($1) && !($1)->GetId() && !($1)->IsHierName() && ($1)->GetName()) {
            // VIPER #7028: Check whether this 'id' cannot be found due to illegal import
            if (present_scope) (void) present_scope->FindImplicitlyImportedErrorCond(($1)->GetName(), $1) ;
            // Single identifier, not resolved in a section of code that allows implicit nets.
            // Also check that we do not have default nettype set to 'none' :
            // in that case we should NOT declare implicit nets.
            if (veri_file::GetDefaultNettype() != VERI_NONE ) {
                // Declare implicit net (LRM 3.5) :
                VeriIdDef *id = new VeriVariable(Strings::save(($1)->GetName())) ;
                id->SetImplicitNet() ;
                if (VeriNode::IsVeri95() || VeriNode::IsVeri2001()) {
                    id->SetType(veri_file::GetDefaultNettype()) ; // implicit net gets type of default_nettype setting.
                } else {
                    id->SetType(VERI_LOGIC) ;
                }
                id->SetKind(veri_file::GetDefaultNettype()) ; // VIPER #5949: implicit net gets kind of default_nettype setting.
                // Declare in present scope ?
                if (present_scope) (void) present_scope->Declare(id) ;
            }
         }
      }
    | hier_ref dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = create_indexed_name($1, dim) ; }
    | concatenation
      { $$ = $1 ; }
    | casting_type "'" "{" concat_elements "}"
      { $$ = new VeriAssignmentPattern($1, $4) ; } // assignment_pattern_expression with type cast
    | streaming_concatenation  // SV Std 1800 addition
      { $$ = $1 ; }
    // VIPER #8016: Support lvalue like x.y(a).z.b(r).c
    | extended_lvalue
      { $$ = $1 ; }
    ;

extended_lvalue : hier_ref  "(" list_of_arguments ")" "." VERI_ID opt_dimensions
        { $$ = $7 ? create_indexed_name(new VeriSelectedName(new VeriFunctionCall($1, $3), $6), $7) : new VeriSelectedName(new VeriFunctionCall($1, $3), $6) ; }
      | hier_ref real_attr_inst "(" list_of_arguments ")" "." VERI_ID opt_dimensions
        {
           VeriName *func_call = new VeriFunctionCall($1, $4) ; if ($2) (func_call)->AddAttributes($2) ;
           $$ = $8 ? create_indexed_name(new VeriSelectedName(func_call, $7), $8) : new VeriSelectedName(func_call, $7) ;
        }
      | extended_lvalue "." VERI_ID opt_dimensions
        { $$ = $4 ? create_indexed_name(new VeriSelectedName($1, $3), $4): new VeriSelectedName($1, $3) ; }
      | extended_lvalue "(" list_of_arguments ")" "." VERI_ID opt_dimensions
        {
            VeriName *f_call = new VeriFunctionCall($1, $3) ;
            $$ = $7 ? create_indexed_name(new VeriSelectedName(f_call, $6), $7): new VeriSelectedName(f_call, $6) ;
        }
      | extended_lvalue real_attr_inst "(" list_of_arguments ")" "." VERI_ID opt_dimensions
        {
            VeriName *f_call = new VeriFunctionCall($1, $4) ; if ($2) (f_call)->AddAttributes($2) ;
            $$ = $8 ? create_indexed_name(new VeriSelectedName(f_call, $7), $8): new VeriSelectedName(f_call, $7) ;
        }
      ;
// VIPER #2754 : Cond predicate in question-colon. Cannot have rule 'expr', since that overlaps with normal parenthesized expression.
qc_cond_predicate :
      expr matches_token pattern
      { $$ = new VeriPatternMatch($1, $3, $2) ; }
    | expr "&&&" expr
      { $$ = new VeriCondPredicate($1, $3) ; }
    | expr "&&&" expr matches_token pattern
      { $$ = new VeriCondPredicate($1, new VeriPatternMatch($3, $5, $4)) ; }
    | qc_cond_predicate "&&&" expr
      { $$ = new VeriCondPredicate($1, $3) ; }
    | qc_cond_predicate "&&&" expr matches_token pattern
      { $$ = new VeriCondPredicate($1, new VeriPatternMatch($3, $5, $4)) ; }
    ;

expr : primary
    //| string // VIPER #5398: moved to primary
    | operator_expr
    | expr "?" attr_inst expr ":" expr
      { $$ = new VeriQuestionColon($1,$4,$6) ; if ($3) ($$)->AddAttributes($3) ; }
   /* System Verilog extentions to expression */
    | "(" qc_cond_predicate ")" "?" attr_inst expr
      {
          // Extract the present_scope pointer which was set globally before parsing
          // rule 'qc_cond_predicate'. We will extract that from condition itself
          VeriExpression *left = $2 ; // Traverse the cond predicate
          VeriScope *scope = 0 ;
          while (left) {
              if (left->GetScope()) scope = left->GetScope() ;
              left = left->GetLeft() ; // Go to its left operand
          }
          // 'scope' is the scope first introduced for pattern matching in rule 'qc_cond_predicate'
          if (scope) present_scope = scope->Upper() ; // Set the upper of pattern scope as present scope
      } ":" expr
      { $$ = new VeriQuestionColon($2,$6,$9) ; if ($5) ($$)->AddAttributes($5) ; } // VIPER #2754 : Cond predicate in question-colon

    | inc_or_dec_expr
    | "(" lvalue assignment_operator expr_or_tagged_id_expr ")"
      { $$ = new VeriBinaryOperator($3,$2,$4) ; /* binary operator with the assignment tokens as oper types */ }
    | expr VERI_INSIDE instance_id_ref
      {
        $$ = new VeriBinaryOperator(VERI_INSIDE, $1, $3) ;
        ($$)->Warning("missing '{', violates IEEE 1800 syntax") ;
      }
    | expr VERI_INSIDE "{" value_range_list "}"
      { $$ = new VeriBinaryOperator(VERI_INSIDE, $1, new VeriConcat($4) /* Treat concat as a list of exprs and value range */) ; }
    // Viper 2569 : added rule 'tagged_id_expr'
    // Also wrong precedence with VERI_UNARY_OPER (?)
    //| VERI_TAGGED VERI_ID expr %prec VERI_UNARY_OPER
      //{ $$ = 0 ; }
// VIPER #5646: The following two rules are moved to 'primary':
//    | expr VERI_WITH "{"
//      { push_scope(0) ; /* VIPER #4503 : Create scope for inline-constraint*/}
//      constraint_blocks "}" // SV Std 1800 addition // FIXME : Only function call is allowed as left operand of VERI_WITH operator
//      {
//          // VIPER #4242 : Here 'expr' is only randomize function call and we are
//          // creating inline constraint witn empty constraint block for that. So
//          // when 'expr' is inline constraint, set constraint block in that parse
//          // tree. Otherwise create a new inline constraint.
//          if ($1 && $1->IsInlineConstraint()) {
//             $$ = $1 ;
//             $1->SetConstraintBlock($5) ;
//          } else {
//             $$ = new VeriInlineConstraint($1, $5) ;
//          }
//          // VIPER #4503 : Attached scope to VeriInlineConstraint
//          if ($$) ($$)->SetScope(pop_scope()) ;
//      }
//    | expr VERI_WITH "(" expr ")" // SV Std 1800
//      { $$ = new VeriWithExpr($1, $4) ; }
      // 'dist' operator moved here, since used in various expressions (constraint and sequences).
      // FIX ME : error out after parsing if used in wrong context..
    | expr VERI_DIST "{" dist_list "}"
      { $$ = new VeriDistOperator($1, $4) ; }
    // VIPER #7832 : Allow function_name(args).method as let expression
    // Example in IEEE 1800-2009 LRM section 9.4.4
    // FIXME : Need to add this in hier_ref rule, but cannot do that because of
    // conflicts with instantiation rule and others
    //| function_call_prefix "." VERI_ID opt_dimensions// VIPER 2178: sequence_instance.ended / sequence_instance.matched
     // { $$ = ($4) ? create_indexed_name(new VeriSelectedName($1, $3), $4): new VeriSelectedName($1, $3) ; } // Map to selected name.
    //| function_call_prefix "." VERI_ID "(" list_of_arguments ")"
     // { $$ = new VeriFunctionCall(new VeriSelectedName($1, $3), $5) ; }
    // VIPER #8016: Support function-call as prefix of hier-name i.e x.y(a).z.b(r).c
    // or x.y(a).z.b(r).c(u)
    | extended_expr { $$ = $1 ; }
    ;
extended_expr : extended_lvalue
       { $$ = $1 ; }
     | extended_lvalue "(" list_of_arguments ")"
       { $$ = new VeriFunctionCall($1, $3) ; }
     ;

// Added for Viper 2569
tagged_id_expr
    : VERI_TAGGED id_ref
      { $$ = new VeriTaggedUnion($2, 0) ; }
    | VERI_TAGGED id_ref pattern
      { $$ = new VeriTaggedUnion($2, $3) ; }
    // The following rules are replaced by the rule above
    // Note : This calls for more semantic/syntax
    // check in VeriTaggedUnion::Resolve.
    // This is the best that can be done, otherwise breaks
    // P1800/Positive/10.4.1.2
    //| VERI_TAGGED VERI_ID expr
      //{ $$ = new VeriTaggedUnion(new VeriIdRef($2), $3) ; }
    //| VERI_TAGGED VERI_ID tagged_id_expr
      //{ $$ = new VeriTaggedUnion(new VeriIdRef($2), $3) ; }
    // Added this for P1800/Positive/10.4.1.2
    | "(" tagged_id_expr ")"
      { $$ = $2; }
      // VIPER #2990 : Consider tagged union in question-colon operator
    | expr "?" attr_inst tagged_id_expr ":" expr_or_tagged_id_expr
      { $$ = new VeriQuestionColon($1,$4,$6) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr "?" attr_inst expr ":" tagged_id_expr
      { $$ = new VeriQuestionColon($1,$4,$6) ; if ($3) ($$)->AddAttributes($3) ; }
    | "(" qc_cond_predicate ")" "?" attr_inst tagged_id_expr
      {
          // Extract the present_scope pointer which was set globally before parsing
          // rule 'qc_cond_predicate'. We will extract that from condition itself
          VeriExpression *left = $2 ; // Traverse the cond predicate
          VeriScope *scope = 0 ;
          while (left) {
              if (left->GetScope()) scope = left->GetScope() ;
              left = left->GetLeft() ; // Go to its left operand
          }
          // 'scope' is the scope first introduced for pattern matching in rule 'qc_cond_predicate'
          if (scope) present_scope = scope->Upper() ; // Set the upper of pattern scope as present scope
      } ":" expr_or_tagged_id_expr
      { $$ = new VeriQuestionColon($2,$6,$9) ; if ($5) ($$)->AddAttributes($5) ; } // VIPER #2754 : Cond predicate in question-colon
    ;

// Viper 2569
// Note : pattern -> (expr | (tagged -> expr))
expr_or_tagged_id_expr :
      expr { $$ = $1 ; }
    | tagged_id_expr { $$ = $1; }
    ;

operator_expr :
    // To get precendece rules working, we need to expose all operator calls explicitly.
    VERI_PLUS attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_PLUS,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_MIN attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_MIN,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_LOGNOT attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_LOGNOT,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDNOT attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDNOT,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDAND attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDAND,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDNAND attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDNAND,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDOR attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDOR,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDNOR attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDNOR,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDXOR attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDXOR,$3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_REDXNOR attr_inst primary %prec VERI_UNARY_OPER
      { $$ = new VeriUnaryOperator(VERI_REDXNOR,$3) ; if ($2) ($$)->AddAttributes($2) ; }

   /* regular Verilog 95 binary operators */
    | expr VERI_PLUS attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_PLUS,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_MIN attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_MIN,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_MUL attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_MUL,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_DIV attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_DIV,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_MODULUS attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_MODULUS,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LOGEQ attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LOGEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LOGNEQ attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LOGNEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_CASEEQ attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_CASEEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_CASENEQ attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_CASENEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LOGAND attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LOGAND,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LOGOR attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LOGOR,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LT attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LT,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_GT attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_GT,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_GEQ attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_GEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LEQ attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_REDAND attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_REDAND,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_REDOR attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_REDOR,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_REDXOR attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_REDXOR,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_REDXNOR attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_REDXNOR,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LSHIFT attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LSHIFT,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_RSHIFT attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_RSHIFT,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }

   /* Verilog 2001 binary operators */
    | expr VERI_POWER attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_POWER,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_ARITLSHIFT attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_ARITLSHIFT,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_ARITRSHIFT attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_ARITRSHIFT,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }

    /* System Verilog 3.0 binary operators */
    | expr VERI_WILDEQUALITY attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_WILDEQUALITY,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_WILDINEQUALITY attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_WILDINEQUALITY,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
     /* System Verilog P1800-2009 binary operators (VIPER #5910/arrow) */
    | expr VERI_RIGHTARROW attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_RIGHTARROW,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }
    | expr VERI_LOG_EQUIVALENCE attr_inst expr
      { $$ = new VeriBinaryOperator(VERI_LOG_EQUIVALENCE,$1,$4) ; if ($3) ($$)->AddAttributes($3) ; }

    ;

inc_or_dec_expr :
      // implement these guys as binary operator, with either left or right expression only.
      lvalue VERI_INC_OP
      { $$ = new VeriBinaryOperator(VERI_INC_OP, $1, 0) ; }
    | lvalue VERI_DEC_OP
      { $$ = new VeriBinaryOperator(VERI_DEC_OP, $1, 0) ; }
    | VERI_INC_OP attr_inst lvalue
      { $$ = new VeriBinaryOperator(VERI_INC_OP, 0, $3) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_DEC_OP attr_inst lvalue
      { $$ = new VeriBinaryOperator(VERI_DEC_OP, 0, $3) ; if ($2) ($$)->AddAttributes($2) ; }
    ;

inc_or_dec_stmt :
      // implement these guys as blocking assignments, with either LHS or RHS expr, and special assignment operator
      lvalue VERI_INC_OP
      { $$ = new VeriBlockingAssign($1,0,0,VERI_INC_OP) ; }
    | lvalue VERI_DEC_OP
      { $$ = new VeriBlockingAssign($1,0,0,VERI_DEC_OP) ; }
    | VERI_INC_OP attr_inst lvalue
      { $$ = new VeriBlockingAssign(0,0,$3,VERI_INC_OP) ; if ($2) ($$)->AddAttributes($2) ; }
    | VERI_DEC_OP attr_inst lvalue
      { $$ = new VeriBlockingAssign(0,0,$3,VERI_DEC_OP) ; if ($2) ($$)->AddAttributes($2) ; }
    ;

primary :
      hier_ref
      {
         // Plain identifier reference in any expression (usage side)
         $$ = $1 ;

         // Declare a implicit net if we are in a port-connection list, and the net is not visible.
         // Need to do this here, due to 'order' problems in Resolve()
         // (see VIPER issues 1289, 1771, 1847).
         if (implicit_nets_allowed && resolve_idrefs && ($1) && !($1)->GetId() && !($1)->IsHierName() && ($1)->GetName()) {
            // Single identifier, not resolved in a section of code that allows implicit nets.
            // Also check that we do not have default nettype set to 'none' :
            // in that case we should NOT declare implicit nets.
            if (veri_file::GetDefaultNettype() != VERI_NONE ) {
                // Declare implicit net (LRM 3.5) :
                VeriIdDef *id = new VeriVariable(Strings::save(($1)->GetName())) ;
                id->SetImplicitNet() ;
                if (VeriNode::IsVeri95() || VeriNode::IsVeri2001()) {
                    id->SetType(veri_file::GetDefaultNettype()) ; // implicit net gets type of default_nettype setting.
                } else {
                    id->SetType(VERI_LOGIC) ;
                }
                id->SetKind(veri_file::GetDefaultNettype()) ; // VIPER #5949: implicit net gets kind of default_nettype setting.
                // Declare in present scope ?
                if (present_scope) (void) present_scope->Declare(id) ;
            }
         }
      }
    | hier_ref dimensions
      {
         VeriRange *dim = 0 ;
         GET_DIMENSION_RANGE(dim, $2) ;
         $$ = create_indexed_name($1, dim) ;

         // VIPER #5774 : Declare a implicit net if we are in a port-connection list, the net is not visible
         // and relaxed checking is on .
         // Need to do this here, due to 'order' problems in Resolve()
         // (see VIPER issues 1289, 1771, 1847).
         if (($$) && implicit_nets_allowed && resolve_idrefs && VeriNode::InRelaxedCheckingMode() && ($1) && !($1)->GetId() && !($1)->IsHierName() && ($1)->GetName()) {
            // Single identifier, not resolved in a section of code that allows implicit nets.
            // Also check that we do not have default nettype set to 'none' :
            // in that case we should NOT declare implicit nets.
            if (veri_file::GetDefaultNettype() != VERI_NONE ) {
                // Declare implicit net (LRM 3.5) :
                VeriIdDef *id = new VeriVariable(Strings::save(($1)->GetName())) ;
                id->SetImplicitNet() ;
                if (VeriNode::IsVeri95() || VeriNode::IsVeri2001()) {
                    id->SetType(veri_file::GetDefaultNettype()) ; // implicit net gets type of default_nettype setting.
                } else {
                    id->SetType(VERI_LOGIC) ;
                }
                id->SetKind(veri_file::GetDefaultNettype()) ; // VIPER #5949: implicit net gets kind of default_nettype setting.
                // Declare in present scope ?
                if (present_scope) (void) present_scope->Declare(id) ;
            }
         }
      }
    | number
    | string // VIPER #5398: Add in primary from expr
    | concatenation opt_index_or_range // VIPER #7545: Use opt_index_or_range instead of opt_dimension
      { $$ = ($2) ? new VeriIndexedExpr($1, $2) : $1 ; } // VIPER #6209: Create indexed concat (expression)
    | multi_concatenation opt_index_or_range // VIPER #7545: Use opt_index_or_range instead of opt_dimension
      { $$ = ($2) ? new VeriIndexedExpr($1, $2) : $1 ; } // VIPER #6209: Create indexed multi-concat (expression)
    | function_call
      { $$ = $1 ; }
    | system_function_call
    | casting_type "'" "(" expr ")"
      { $$ = new VeriCast($1,$4) ; }
    | simple_type "(" expr ")"
      { $$ = new VeriCast(new VeriDataType($1,0,0),$3) ;
        // VIPER 4987 : superset parsing
        ($$)->Warning("cast without ' violates IEEE 1800 syntax") ;
      }
    // VIPER #5095: casting_type does not include parenthesized expr due to bison conflicts, write it flattened here:
    | "(" mintypmax_expr ")" "'" "(" expr ")"  // SV assignment_pattern_expression with type cast
      { $$ = new VeriCast($2,$6) ; }
    | casting_type "'" "{" concat_elements "}" // SV assignment_pattern_expression with type cast
      { $$ = new VeriAssignmentPattern($1,$4) ; }
    // VIPER #5095: casting_type does not include parenthesized expr due to bison conflicts, write it flattened here:
    | "(" mintypmax_expr ")" "'" "{" concat_elements "}" // SV assignment_pattern_expression with type cast
      { $$ = new VeriAssignmentPattern($2,$6) ; }
    | casting_type "'" "{" expr "{" concat_elements "}" "}" // SV assignment_pattern_expression with type cast
      { $$ = new VeriMultiAssignmentPattern($1, $4, $6) ; }
    // VIPER #5095: casting_type does not include parenthesized expr due to bison conflicts, write it flattened here:
    | "(" mintypmax_expr ")" "'" "{" expr "{" concat_elements "}" "}" // SV assignment_pattern_expression with type cast
      { $$ = new VeriMultiAssignmentPattern($2, $6, $8) ; }
    | time_literal
      { $$ = $1 ; }
    | VERI_UNSIZED_LITERAL
      { $$ = new VeriConstVal($1, VERI_UNSIZED_LITERAL, 1/*bit-size*/) ; Strings::free($1) ; }
    | VERI_NULL
      { $$ = new VeriNull() ; }
    // VIPER 3499 : Do not consider '$' as part of any expression. Add rule for its specific use
    //| VERI_DOLLAR         // VIPER 2180. Std 1800 feature $ can be in primary.
     // { $$ = new VeriDollar() ; }
    | streaming_concatenation // SV Std 1800 addition
      { $$ = $1 ; }
    | type_reference // NOTE: Was producing conflict with type_reference in data_type - introduced a new rule data_type_without_type_ref for that
      { $$ = $1 ; }
    | "{" "}" // SV Std 1800 addition for empty queue
      { $$ = new VeriConcat(0) ; }
    | "(" mintypmax_expr ")"
      { $$ = $2 ;
        if ($$) ($$)->SetLinefile(veri_file::GetRuleLineFile()) ; // issue 2598: adjust linefile to include ()'s
        if ($$) ($$)->SetParenthesized() ; // VIPER 5118/2582 : flag expression as 'parenthesized'
      }
    // VIPER #5646: Added the following four rules here to associate "with" with the last primary:
    | primary VERI_WITH range    // Stream expression
      { $$ = new VeriWithExpr($1, $3) ; }
    | primary VERI_WITH index    // Stream expression
      { $$ = new VeriWithExpr($1, new VeriRange($3,0, VERI_NONE)) ; }
    | primary VERI_WITH "{"
      { push_scope(0) ; /* VIPER #4503 : Create scope for inline-constraint*/}
      constraint_blocks "}" // SV Std 1800 addition // FIXME : Only function call is allowed as left operand of VERI_WITH operator
      {
          // VIPER #4242 : Here 'expr' is only randomize function call and we are
          // creating inline constraint witn empty constraint block for that. So
          // when 'expr' is inline constraint, set constraint block in that parse
          // tree. Otherwise create a new inline constraint.
          if ($1 && $1->IsInlineConstraint()) {
             $$ = $1 ;
             $1->SetConstraintBlock($5) ;
             update_end_linefile($$) ;
          } else {
             $$ = new VeriInlineConstraint($1, $5) ;
          }
          // VIPER #4503 : Attached scope to VeriInlineConstraint
          if ($$) ($$)->SetScope(pop_scope()) ;
      }
      // VIPER #6756: Not exactly sure if this %prec veri_no_ocur is correct or not
      // but without this we cannot resolve the shift/reduce conflict we get.
    | primary VERI_WITH "(" opt_exprs ")" %prec veri_no_ocur // SV Std 1800
      {
          VeriExpression *right = (($4) && ($4)->Size()) ? (VeriExpression *)($4)->GetFirst() : 0 ;
          $$ = new VeriWithExpr($1, right) ;
          // Something wrong here: a single expression must be specified:
          if (!right) ($$)->Error("syntax error near %s", ")") ;
          else if (($4) && (($4)->Size()>1)) ($$)->Error("syntax error near %s", ",") ;
          delete ($4) ;
      }
      // VIPER #6756: Adjusted following rule to accomodate a list of expressions.
      // Actually, it should be id-refs but we get conflict with id-ref with the
      // above rule, so use expression list here. We are checking for id-ref in
      // the VeriInlineConstraint class (ctor and set) routines.
    | primary VERI_WITH "(" opt_exprs ")" "{"
      { push_scope(0) ; /* VIPER #4503 : Create scope for inline-constraint*/ }
      constraint_blocks "}"
      {
          // VIPER #4242 : Here 'expr' is only randomize function call and we are
          // creating inline constraint witn empty constraint block for that. So
          // when 'expr' is inline constraint, set constraint block in that parse
          // tree. Otherwise create a new inline constraint.
          if ($1 && $1->IsInlineConstraint()) {
             $$ = $1 ;
             $1->SetIdentifierList($4) ; // VIPER #6756: Set list of identifiers referenced
             $1->SetConstraintBlock($8) ;
             update_end_linefile($$) ;
          } else {
             $$ = new VeriInlineConstraint($1, $4, $8) ;
          }
          // VIPER #4503 : Attach scope to VeriInlineConstraint
          if ($$) ($$)->SetScope(pop_scope()) ;
      }
    ;

opt_exprs :   { $$ = 0 ; }
    | exprs   { $$ = $1 ; }
    ;

exprs : expr          { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | exprs "," expr  { $$ = ($1) ; ($$)->InsertLast($3) ; }
    ;

function_call_prefix :
      hier_ref "(" list_of_arguments ")"
      { $$ = new VeriFunctionCall($1,$3) ; }
    | hier_ref real_attr_inst "(" list_of_arguments ")"
      { $$ = new VeriFunctionCall($1,$4) ; if ($2) ($$)->AddAttributes($2) ; }
    ;

function_call :
      function_call_prefix
      {
          $$ = $1 ;
          // VIPER #4242 : If this is randomize function call, create inline
          // contraint for it, so that we can add scope of prefix class as
          // extended scope of inline constraint's local scope. This will help
          // us to resolve the arguments of randomize call where class objects
          // can be used.
          VeriName *prefix = ($1) ? $1->GetPrefix() : 0 ;
          if (prefix && prefix->GetFunctionType() == VERI_METHOD_RANDOMIZE) {
              $$ = new VeriInlineConstraint($$, 0) ;
          }
      }

 // | method_call  | randomize_call // handled from within rule for expr
    ;

system_function_call :
      system_func_id
      { $$ = new VeriSystemFunctionCall($1,0) ; }
    | system_func_id "(" list_of_arguments ")"
      { $$ = new VeriSystemFunctionCall($1,$3) ; }
    | system_func_id "(" data_type_without_type_ref ")" // SV Std 1800 addition, type_reference included in expr -> primary
      {
          Array * temp_arr = new Array() ;
          temp_arr->InsertLast($3) ;
          $$ = new VeriSystemFunctionCall($1, temp_arr) ;
      }
    | system_func_id "(" data_type_without_type_ref "," expr ")" ";" // SV Std 1800 addition, type_reference included in expr -> primary
      {
          Array * temp_arr = new Array() ;
          temp_arr->InsertLast($3) ;
          temp_arr->InsertLast($5) ;
          $$ = new VeriSystemFunctionCall($1, temp_arr) ;
      }
    ;

list_of_arguments : argument // even first argument may be dangling(SV 3.1a draft5 LRM, section 10.4.4)! Store it.
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | list_of_arguments "," argument // Dangling subsequent arguments also need to be stored.
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

//list_of_func_arguments : func_argument
      //{ $$ = new Array() ; ($$)->InsertLast($1) ; }
    //| list_of_func_arguments "," func_argument // Dangling subsequent arguments also need to be stored.
      //{ $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    //| error
      //{ $$ = 0 ; }
    //;

//func_argument : argument { $$ = $1 ; }
    //| VERI_DOLLAR { $$ = new VeriDollar() ; } // '$' can be argument of function call
    //;

argument :
      { $$ = 0 ; /* dangling 'ordered' subprogram argument */ }
    | "." VERI_ID "(" ")"
      { $$ = new VeriPortConnect($2,new VeriPortOpen()) ; /* dangling 'named' argument, create open port, we have to check for default value in this case(SV 3.1a draft5 LRM, section 10.4.4) */  }
    | "." VERI_ID "(" expr_or_tagged_id_expr ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | tagged_id_expr  // 'expr' is parsed through sequence_expr -> 'mintypmax_expr'
      { $$ = $1 ; /* connected 'ordered' subprogram argument */ }
    // VIPER #5710: VCS Compatibility issue (and_outside_prop) : sequence expression
    // and event expression as argument in sequence/property call
    | "." VERI_ID "(" clocking_event ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | "." VERI_ID "(" edge_id_expr ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | "." VERI_ID "(" sequence_expr_wo_expr ")"
      { $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    | clocking_event // some system calls (rose,fell,past etc) allow clocking events in arguments..^M
      { $$ = $1 ; }
    | edge_id_expr
    | "(" edge_id_expr ")" { $$ = $2 ; } // VIPER #8341: Allow parenthesized edge_id_expr
    | sequence_expr
      {
         // VIPER #5248 (display_close_brace): Allow min-typ-max expression in argument:
          $$ = $1 ;
          if ($$ && ($$)->IsMinTypMax()) ($$)->Warning("min-typ-max expression in argument violates IEEE 1800 syntax") ;
      }
    | VERI_DOLLAR { $$ = new VeriDollar() ; } // '$' can be argument of function call
    ;

//argument :
      //{ $$ = 0 ; /* dangling 'ordered' subprogram argument */ }
    //| expr ":" expr ":" expr
      //{
          //// VIPER #5248 (display_close_brace): Allow min-typ-max expression in argument:
          //$$ = new VeriMinTypMaxExpr($1,$3,$5) ;
          //($$)->Warning("min-typ-max expression in argument violates IEEE 1800 syntax") ;
      //}
    //| expr_or_tagged_id_expr
      //{ $$ = $1 ; /* connected 'ordered' subprogram argument */ }
////###IF PM_VERILOG_SYS_ANALYZER
    //| "." VERI_ID "(" ")"
      //{ $$ = new VeriPortConnect($2,new VeriPortOpen()) ; /* dangling 'named' argument, create open port, we have to check for default value in this case(SV 3.1a draft5 LRM, section 10.4.4) */  }
    //| "." VERI_ID "(" expr_or_tagged_id_expr ")"
      //{ $$ = new VeriPortConnect($2,$4) ; /* connected 'named' argument */ }
    //| clocking_event // some system calls (rose,fell,past etc) allow clocking events in arguments..
      //{ $$ = $1 ; }
    //| clocking_event sequence_expr %prec VERI_CLOCKING_EVENT // 3.1a. Result can be property_expr too. Low precendence. See notes at VERI_CLOCKING_EVENT
      //{ $$ = new VeriClockedSequence($1, $2) ; }
    //| actual_event_expr // VIPER 2180 : arguments of sequence can be event expression
////###ENDIF PM_VERILOG_SYS_ANALYZER
    //;

concatenation : "{" concat_elements "}"
      {
          // VIPER #7070 : If elements contain 'label:value', create assignment pattern
          if (contain_concat_item($2)) {
              $$ = new VeriAssignmentPattern(0, $2) ;
              ($$)->Warning("converting %s to %s", "concatenation", "assignment pattern") ;
          } else {
              $$ = new VeriConcat($2) ;
          }
      }
    | "'" "{" pattern_list "}"    // IEEE 1800 assignment pattern
      { $$ = new VeriAssignmentPattern(0,$3) ; }
      // LRM BNF does not allow empty assignment patterns but other tools do support them
      // for initializing or assigning to a queue like "reg my_q[$] = '{} ;".
      // This has been used in VMM 1.0.1 version specifically, so we need to parse this:
    //| "'" "{" "}"
      //{ $$ = new VeriAssignmentPattern(0,0) ; }
    ;

multi_concatenation : "{" expr "{" concat_elements "}" "}"
      {
          // VIPER #7070 : If elements contain 'label:value', create assignment pattern
          if (contain_concat_item($4)) {
              $$ = new VeriMultiAssignmentPattern(0, $2, $4) ;
              ($$)->Warning("converting %s to %s", "multi concatenation", "multi assignment pattern") ;
          } else {
              $$ = new VeriMultiConcat($2,$4) ;
          }
      }
    | "'" "{" expr "{" mc_concat_elements "}" "}"    // IEEE 1800 assignment pattern
      { $$ = new VeriMultiAssignmentPattern(0,$3,$5) ; }
    ;

mc_concat_elements :
      concat_element
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    |
      { $$ = new Array() ; ($$)->InsertLast(0) ; }
    | mc_concat_elements "," concat_element
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | mc_concat_elements ","
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast(0) ; }
    ;

concat_elements :
      concat_element
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | concat_elements "," concat_element
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    ;

concat_element :
      expr_or_tagged_id_expr
      { $$ = $1 ; }
    | expr ":" expr_or_tagged_id_expr
      {
            $$ = new VeriConcatItem($1,$3) ;
      }
    | simple_type ":" expr_or_tagged_id_expr
      // LRM BNF does not allow this, but examples show that at least the built-in keywords (int, bit etc) are allowed as member label
      { $$ = new VeriConcatItem(new VeriDataType($1,0,0),$3) ; }
    //| VERI_CHAR ":" expr_or_tagged_id_expr // VIPER #8350: Moved this rule under 'simple_type'
    //  {
    //      $$ = new VeriConcatItem(new VeriDataType(VERI_CHAR,0,0),$3) ;
    //      ($$)->Warning("char as concat label violates IEEE 1800 syntax") ;
    //  }
    | VERI_DEFAULT ":" expr_or_tagged_id_expr
      { $$ = new VeriConcatItem(0,$3) ; }
    //| expr ":" pattern_without_expr  // Part of pattern, move here to remove conflict
      //{ $$ = new VeriConcatItem($1,$3) ; }
// VIPER #5646: The following two rules are moved to 'primary':
//    | expr VERI_WITH range    // Stream expression
//      { $$ = new VeriWithExpr($1, $3) ; }
//    | expr VERI_WITH index    // Stream expression
//      { $$ = new VeriWithExpr($1, new VeriRange($3,0, VERI_NONE)) ; }
    ;

mintypmax_expr : expr
      { $$ = $1 ; }
    | expr ":" expr ":" expr
      { $$ = new VeriMinTypMaxExpr($1,$3,$5) ; }
    ;

string : VERI_STRING
      { $$ = new VeriConstVal($1,VERI_STRING) ; Strings::free($1) ; }
    ;

 // time_literal in SystemVerilog added as separate token (value and unit without spaces). This allows parsing behind # token and before an expression.
time_literal : VERI_TIMELITERAL
      { $$ = new VeriTimeLiteral($1) ; }
    ;

/********************* Attribute Instances **********************/

attr_inst :
      { $$ = 0 ; }
    | real_attr_inst
      { $$ = $1 ; }
    ;

real_attr_inst : "(*" attr_list "*)"
      { $$ = $2 ; }
    | real_attr_inst "(*" attr_list "*)"
      {   /* Another list... Do 'Map' Append. */
          $$ = $1 ;
          char *key ;
          VeriExpression *val ;
          MapIter mi ;
          FOREACH_MAP_ITEM($3, mi, &key, &val) {
              insert_attr_into_map(($$), key, val) ;
          }
          delete $3 ;
      }
    ;

attr_list :
      VERI_ID
      { $$ = new Map(STRING_HASH) ; insert_attr_into_map(($$), $1, new VeriIntVal(1)) ; /* V2K LRM 2.8 : attribute without value gets value (integer) 1 */ }
    | VERI_ID VERI_EQUAL expr
      { $$ = new Map(STRING_HASH) ; insert_attr_into_map(($$), $1, $3) ; }
    | attr_list "," VERI_ID
      { $$ = $1 ; insert_attr_into_map(($$), $3, new VeriIntVal(1)) ; /* V2K LRM 2.8 : attribute without value gets value (integer) 1 */ }
    | attr_list "," VERI_ID VERI_EQUAL expr
      { $$ = $1 ; insert_attr_into_map(($$), $3, $5) ; }
    | error
      { $$ = 0 ; }
    ;

/********************* Pick up pragmas from flex **********************/

 /* opt_pragma : read pragmas after a rule completes : force bison to read next token, using a non-existing token request */
opt_pragma :
      { $$ = veri_file::TakePragmas() ; /* pick-up all pragmas parsed since previous TakePragmas call */ }
    | VERI_OPT_PRAGMA
      { VERIFIC_ASSERT(0) ; /* force bison to read next token */ }
    ;

pre_pragma :
      { $$ = veri_file::TakePragmas() ; /* pick-up all pragmas parsed since previous TakePragmas call */
      }
    ;

/********************* Pick up comment from flex **********************/

 /* pre_comment : no need to force bison to read next token. just pick up with TakeComments() */
pre_comment :
       { $$ = veri_file::TakeComments() ; }
    ;

 /* opt_comment : read comments after a rule completes : force bison to read next token, using a non-existing token request */
opt_comment :
      { $$ = veri_file::TakeComments() ; } %prec veri_no_comment
    | VERI_OPT_COMMENT
      { VERIFIC_ASSERT(0) ; /* force bison to read next token */ }
    ;

/*********************** Identifiers *****************************/

/* identifier declarations */

//parameter_decl_id : VERI_ID { $$ = declare_id(new VeriParamId($1)) ;  } ;
// VIPER #2703 : Redefine the following rule to support 'hier_ref' as the
// return type of function declaration. Previous rule is producing conflicts
// when we use 'hier_ref' as return type of function declaration.
function_decl_id : hier_ref  // VIPER #2665 & 2667 : change type of this non-terminal
      { $$ = $1 ; }
    | hier_ref "::" VERI_NEW
      { $$ = new VeriScopeName($1, Strings::save("new"), 0) ; ($$)->SetSuffixIsToken() ; }
    | VERI_NEW // SV3.1a finally allows 'new' as function name
      { $$ = new VeriKeyword(VERI_NEW) ; }
    ;

// VIPER #2665 & 2667 : Change rule to behave same as function:
task_decl_id : hier_ref      { $$ = $1 ; }
    ;

// inst_decl_id : VERI_ID      { $$ = declare_id(new VeriInstId($1)) ;  } ; // cannot instantiate before the '(' starts.
block_decl_id : VERI_ID     { $$ = new VeriBlockId($1) ; } // VIPER #8109 : Declare it from usage : declare_block_id(new VeriBlockId($1)) ;  } ;
specparam_decl_id : VERI_ID { $$ = declare_id(new VeriParamId($1)) ;  } ;
                  
//CARBON_BEGIN
//genvar_decl_id : VERI_ID    { $$ = declare_id(new VeriGenVarId($1)) ; } ;  OldVerificVersion
genvar_decl_id : VERI_ID    { $$ = declare_id(new VeriGenVarId($1)) ; gLastDeclVeriId = $$ ;} ;
//CARBON_END

config_decl_id : VERI_ID    { $$ = new VeriConfigurationId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ;
// library_decl_id : VERI_ID   { $$ = $1 ; } ; // Not required after VIPER #5128 fix

//CARBON_BEGIN
//module_decl_id : VERI_ID    { $$ = new VeriModuleId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ; // Don't store in scope, until we know if it is nested. OldVerificVersion
module_decl_id : VERI_ID    { $$ = new VeriModuleId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ gLastDeclVeriId = $$ ; } ; // Don't store in scope, until we know if it is nested.
//CARBON_END

udp_decl_id : VERI_ID       { $$ = new VeriUdpId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ; // Don't store in scope, until we know if it is nested.

 // System Verilog IdDefs
interface_decl_id : VERI_ID  { $$ = new VeriInterfaceId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ; // interface is a kind-of Module. // Don't store in scope, until we know if it is nested.
program_decl_id : VERI_ID    { $$ = new VeriProgramId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ; // program is a kind-of Module. // Don't store in scope, until we know if it is nested.
checker_decl_id : VERI_ID    { $$ = new VeriCheckerId($1) ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ; // Checker is a kind-of Module. // Don't store in scope, until we know if it is nested.
package_decl_id : VERI_ID    { $$ = new VeriModuleId($1) ; $$->SetIsPackage() ; veri_file::SetupLinefileStructures() ; /* VIPER #5932 */ } ; // package is a kind-of Module. Set the flag to know it is a package Viper 5256 // Don't store in scope.

class_decl_id : VERI_ID      { $$ = declare_type_id(new VeriIdRef($1), 0 /* no dimension */) ; within_class++ ; } ;
              
//CARBON_BEGIN
//clock_signal_decl_id : VERI_ID     { $$ = declare_id(new VeriVariable($1)) ; /* fix me VeriClockSignalId($1) */ } OldVerificVersion
clock_signal_decl_id : VERI_ID     { $$ = declare_id(new VeriVariable($1)) ; gLastDeclVeriId = $$ ;/* fix me VeriClockSignalId($1) */ } ;
//CARBON_END

opt_clocking_decl_id : { $$ = 0 ; } | VERI_ID { $$ = declare_clocking_id($1) ; } ; //declare_id(new VeriClockingId($1)) ; } ;

property_decl_id : VERI_ID   { $$ = declare_id(new VeriFunctionId($1)) ; /* fix me VeriPropertyId($1) */ } ;
sequence_decl_id : VERI_ID   { $$ = declare_id(new VeriFunctionId($1)) ; /* fix me VeriSequenceId($1) */ } ;
enum_decl_id : VERI_ID       { $$ = new VeriIdRef($1) ; /* fix me : VeriEnumId($1) */ } ;
modport_decl_id : VERI_ID    { $$ = declare_id(new VeriModportId($1)) ; } ;
modport_port_decl_id : VERI_ID { $$ = declare_id(new VeriPrototypeId($1)) ; /* fix me : VeriModportPort($1) */ }
    | "." VERI_ID "(" opt_expr ")"
      { $$ = declare_id(new VeriNamedPort($2, $4)) ; }
    ;

constraint_decl_id : hier_ref { $$ = $1 ; }  // Added for VIPER# 4179
//constraint_decl_id : VERI_ID  { $$ = declare_id(new VeriVariable($1)) ; /* fix me VeriConstraintId($1) */ }
//    | VERI_ID "." VERI_ID   { $$ = declare_id(new VeriVariableId(Strings::save($1," ",$3)) ; Strings::free($1) ; Strings::free($3) ; } // interface prefix.. Should declare into that scope ?
//   | class_scope VERI_ID   { $$ = declare_id(new VeriVariable(Strings::save($1, $2))) ; Strings::free($1) ; Strings::free($2) ; } // extern-class prefix.. reference into that scope ? // commented for 4179
 ;

let_decl_id : VERI_ID    { $$ = declare_id(new VeriLetId($1)) ; }

/* identifier references */

// General, single identifier reference :
id_ref : VERI_ID

      {
          // First create a normal IdRef, so we have a parse tree node (needed for 'Find' : see VIPER 3135).
          $$ = new VeriIdRef($1) ; // unresolved.

          // First fix for Viper 2551, 2574, 2567 : Resolve identifier before parsing next token :
          // Do NOT do this for sections of code that have 'resolve_idrefs' set to 0.
          // VIPER #5147: If this reference is within extern method body, donot resolve
          // the reference, we should give priority to class body before the container
          VeriIdDef *id = (present_scope && resolve_idrefs && !within_extern_method) ? present_scope->Find(($$)->GetName(),/*from*/($$)) : 0 ;
          if (id && !id->IsModule()) { // exclude module ids, because we want a pure scope search.
              ($$)->SetId(id) ; // set resolved identifier.
              // Do not delete the name string. Other code relies on it to be there.
              // CHECK ME : We should be able now to reset name, as long as SetId(0) saves the name field.
              // ($$)->SetName(0) ; // Delete the string in the idref, since we do not need it any more.
          }
      }
      ;

task_id: hier_ref        { $$ = $1 ; /* task enable */ } ;
//function_id: hier_ref    { $$ = $1 ; /* function call */ } ;
task_block_id: hier_ref  { $$ = $1 ; /* disable stmt */ } ;

hier_ref : id_ref
      { $$ = $1 ; }
    | hier_ref "::" VERI_ID
      {
          $$ = new VeriScopeName($1, $3, 0) ;
          if (resolve_idrefs && !within_extern_method) {
              // VIPER #5248 (test_12): Try to resolve suffix of this scope name:
              VeriIdDef *prefix_id = ($1) ? ($1)->GetId() : 0 ;
              // VIPER #8082: If prefix of scope resolution operator "::" is
              // not found in present_scope, it must be package scope resolution
              // operator and so use 'ResolveScopeName' to find defined packages.
              if (!prefix_id) { // This must be package scope operator
                  // Find the package now
                  if ($1) $1->ResolveScopeName(present_scope, VeriTreeNode::VERI_UPWARD_SCOPE_NAME) ;
                  prefix_id = ($1) ? $1->GetId(): 0 ;
              }
              VeriScope *prefix_scope = (prefix_id) ? (prefix_id)->LocalScope() : 0 ;
              const char *suffix = ($3) ;
#ifdef VERILOG_SHARE_STRINGS
              // $3 may get deleted from constructor in case we use this compile flag.
              suffix = ($$)->GetSuffix() ; // Get the constant string instead
#endif
              VeriIdDef *suffix_id = (prefix_scope) ? prefix_scope->FindLocal(suffix) : 0 ;
              if (suffix_id) ($$)->SetId(suffix_id) ; // Set the resolved id
          }
      }
    | hier_ref parameter_value_assignment "::" VERI_ID
      { $$ = new VeriScopeName(($1) ? ($1)->SetParamValues($2): $1, $4, 0) ; }
    | VERI_THIS
      { $$ = new VeriKeyword(VERI_THIS) ; }
    | VERI_SUPER
      { $$ = new VeriKeyword(VERI_SUPER) ; }
    | VERI_DOLLAR_ROOT
      {
          $$ = new VeriKeyword(VERI_DOLLAR_ROOT) ;
          if (parsing_package) ($$)->Error("references to the %s are not allowed in packages", "instance hierarchy") ; // VIPER #6046
      }
      // According to IEEE 1800 LRM section A.9.3 on Identifiers : $unit must be referenced with ::
    | VERI_DOLLAR_UNIT "::" VERI_ID
      {
          $$ = new VeriScopeName(new VeriKeyword(VERI_DOLLAR_UNIT), $3, 0) ;
          if (parsing_package) ($$)->Error("references to the %s are not allowed in packages", "compilation unit") ; // VIPER #6046
      }
      // 3.1a : some methods collide with a predefined keyword. Accept here :
    | hier_ref "." VERI_AND
      { $$ = new VeriSelectedName($1,Strings::save("and")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref dimensions "." VERI_AND // VIPER #8084 : allow index in prefix
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriSelectedName(create_indexed_name($1, dim), Strings::save("and")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref "." VERI_OR
      { $$ = new VeriSelectedName($1,Strings::save("or")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref dimensions "." VERI_OR // VIPER #8084 : allow index in prefix
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriSelectedName(create_indexed_name($1, dim), Strings::save("or")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref "." VERI_XOR
      { $$ = new VeriSelectedName($1,Strings::save("xor")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref dimensions "." VERI_XOR // VIPER #8084 : allow index in prefix
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriSelectedName(create_indexed_name($1, dim), Strings::save("xor")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref "." VERI_UNIQUE
      { $$ = new VeriSelectedName($1,Strings::save("unique")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref dimensions "." VERI_UNIQUE // VIPER #5656 : allow index in prefix
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriSelectedName(create_indexed_name($1, dim), Strings::save("unique")) ; ($$)->SetSuffixIsToken() ; }
    | hier_ref "." VERI_NEW
      { $$ = new VeriSelectedName($1,Strings::save("new")) ; ($$)->SetSuffixIsToken() ; }
      // VIPER #6391: Parse scope names starting with "local" keyword (2009 LRM sec 18.7.1)
    | VERI_LOCAL "::" VERI_ID
      { $$ = new VeriScopeName(new VeriKeyword(VERI_LOCAL), $3, 0) ; }
    | VERI_LOCAL "::" VERI_SUPER
      { $$ = new VeriScopeName(new VeriKeyword(VERI_LOCAL), Strings::save("super"), 0) ; ($$)->SetSuffixIsToken() ; }
    | VERI_LOCAL "::" VERI_THIS
      { $$ = new VeriScopeName(new VeriKeyword(VERI_LOCAL), Strings::save("this"), 0) ; ($$)->SetSuffixIsToken() ; }
      // Normal hierarchical name :
    | hier_ref "." VERI_ID
      { $$ = new VeriSelectedName($1, $3) ; }
      // With prefix which includes a (constant expression) index :
    | hier_ref dimensions "." VERI_ID
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = new VeriSelectedName(create_indexed_name($1, dim),$4) ; }
    ;

/* VIPER 2273 : Dot separated identifier in delay control */
delay_event_hier_ref : id_ref
      { $$ = $1 ; }
    | VERI_THIS // Allow this.id in delay/event expression
      { $$ = new VeriKeyword(VERI_THIS) ; }
      // VIPER #4745 : Allow $unit::id as event expression
    | VERI_DOLLAR_UNIT "::" VERI_ID
      {
          $$ = new VeriScopeName(new VeriKeyword(VERI_DOLLAR_UNIT), $3, 0) ;
          if (parsing_package) ($$)->Error("references to the %s are not allowed in packages", "compilation unit") ; // VIPER #6046
      }
      // Normal hierarchical name :
    | delay_event_hier_ref "." VERI_ID
      { $$ = new VeriSelectedName($1,$3) ; }
      // VIPER #4243 : Allow scope name in delay control
    | delay_event_hier_ref "::" VERI_ID
      { $$ = new VeriScopeName($1, $3, 0) ; }
    ;

// VIPER #4970 : allow hier identifier with indexing and bit select as event control
event_control_hier_ref : id_ref
      { $$ = $1 ; }
    | id_ref dimensions
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = create_indexed_name($1, dim) ; }
    | VERI_THIS // Allow this.id in delay/event expression
      { $$ = new VeriKeyword(VERI_THIS) ; }
      // VIPER #4745 : Allow $unit::id as event expression
    | VERI_DOLLAR_UNIT "::" VERI_ID
      {
          $$ = new VeriScopeName(new VeriKeyword(VERI_DOLLAR_UNIT), $3, 0) ;
          if (parsing_package) ($$)->Error("references to the %s are not allowed in packages", "compilation unit") ; // VIPER #6046
      }
      // Normal hierarchical name :
    | event_control_hier_ref "." VERI_ID
      { $$ = new VeriSelectedName($1,$3) ; }
    | event_control_hier_ref "." VERI_ID dimensions // VIPER #7839: Allow indexing on selected name
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $4) ; $$ = create_indexed_name(new VeriSelectedName($1, $3), dim) ; }
      // VIPER #4243 : Allow scope name in delay control
    | event_control_hier_ref "::" VERI_ID
      { $$ = new VeriScopeName($1, $3, 0) ; }
    | event_control_hier_ref "::" VERI_ID dimensions // VIPER #7839: Allow indexing on scoped name
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $4) ; $$ = create_indexed_name(new VeriScopeName($1, $3, 0), dim) ; }
    ;

param_value_hier_ref : id_ref
      { $$ = $1 ; }
    | VERI_THIS // Allow this.id in delay/event expression
      { $$ = new VeriKeyword(VERI_THIS) ; }
      // Normal hierarchical name :
    | param_value_hier_ref "." VERI_ID
      { $$ = new VeriSelectedName($1,$3) ; }
    ;

/* special identifier references */

system_task_id : VERI_SYSTEM_TASK { $$ = $1 ; } ;
system_func_id : VERI_SYSTEM_TASK { $$ = $1 ; } ;

// Class scope : we could resolve the class scope itself here and now...
// FIXME : class_scope contains optional parameter_value_assignment as per IEEE 1800 sec A.2.2.1 We do not support this.
class_scope : VERI_ID opt_parameter_value_assignment "::"
      { $$ = Strings::save($1,"::") ; Strings::free($1) ; }
    | class_scope VERI_ID opt_parameter_value_assignment "::"
      { $$ = Strings::save($1,$2,"::") ; Strings::free($1) ; Strings::free($2) ; }
    | VERI_DOLLAR_UNIT "::"
      {
        $$ = 0 ;
        VeriTreeNode tmp_node ;
        tmp_node.Warning("this feature is not yet supported") ;
        //if (parsing_package) tmp_node.Error("references to the %s are not allowed in packages", "compilation unit") ; // VIPER #6046
      }
    ;

// FIXME : use of opt_parameter_value_assignment with class scope to support 1800 feature
// creates a problem, as opt_parameter_value_assignment should be of type Array
// while class_scope is of type string.
opt_parameter_value_assignment : { $$ = 0 ; }
    | parameter_value_assignment
    ;

opt_dir :          { $$ = 0 ; }
    | direction    { $$ = $1 ; }    // Does not allow SV ref and const ref
    ;

/******************************* Configs (Verilog 2000 - SV 3.1)  *****************************/

config_decl : VERI_CONFIG { design_unit_depth++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; } config_decl_id ";"
    { push_scope($3) ; }
    opt_local_param_decls // VIPER #7416
    design_statement
    config_statements VERI_ENDCONFIG
      {
          design_unit_depth-- ;
          // NOTE: Configuration declarations do not introduce any scope.
          $$ = new VeriConfiguration($3, $6, $7, $8, pop_scope()) ;
          ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale
      }
    ;

opt_local_param_decls : { $$ = 0 ; }
    | local_param_decls { $$ = $1 ; }
    ;

local_param_decls : local_parameter_decl
      { $$ = new Array(1) ; if ($1) $$->InsertLast($1) ; }
    | local_param_decls local_parameter_decl
      { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
    ;

design_statement : VERI_DESIGN design_id_ref_list ";"
      { $$ = $2 ; }
    ;

design_id_ref_list : cell_id_ref
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | design_id_ref_list cell_id_ref
      { $$ = ($1) ? ($1) : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

config_statements :
      { $$ = 0 ; }
    | config_statements config_rule_statement opt_semi
      // NOTE: LRM examples shows semi-colon but BNF does not.
      // Other tools seem to support both - with and without semi-colon.
      // So, we also support both in this case by using 'opt_semi':
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

opt_semi: { $$ = 0 ; }
    | ";" { $$ = VERI_SEMI ; }
    ;

config_rule_statement :
      VERI_DEFAULT VERI_LIBLIST liblist
      { $$ = new VeriDefaultConfig($3) ; }
    | VERI_INSTANCE instance_id_ref VERI_LIBLIST liblist
      { $$ = new VeriInstanceConfig($2, $4) ; }
    //| VERI_INSTANCE instance_id_ref VERI_USE cell_id_ref opt_config
    | VERI_INSTANCE instance_id_ref use_clause_rule
      { $$ = new VeriInstanceConfig($2, $3) ; }
    | VERI_CELL cell_id_ref VERI_LIBLIST liblist
      { $$ = new VeriCellConfig($2, $4) ; }
    //| VERI_CELL cell_id_ref VERI_USE cell_id_ref opt_config
    | VERI_CELL cell_id_ref use_clause_rule
      { $$ = new VeriCellConfig($2, $3) ; }
    ;

liblist : { $$ = 0 ; }
    | liblist library_id_ref
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

opt_config : { $$ = 0 ; }
    | ":" VERI_CONFIG
      { $$ = 1 ; }
    ;

library_id_ref: VERI_ID { $$ = $1 ; } ;

instance_id_ref:
      hier_ref
      { $$ = $1 ; }
    | hier_ref dimensions // FIXME: For array instance (NOT in BNF/LRM but other tools support this)
      { VeriRange *dim = 0 ; GET_DIMENSION_RANGE(dim, $2) ; $$ = create_indexed_name($1, dim) ; }
    ;

cell_id_ref:
      VERI_ID        // cell
      { $$ = new VeriIdRef($1) ; }
    | VERI_ID "." VERI_ID // library . cell
      { $$ = new VeriSelectedName(new VeriIdRef($1), $3) ; }
    ;

use_clause_rule :
      VERI_USE cell_id_ref opt_config
      { $$ = new VeriUseClause($2, $3) ; }
    | VERI_USE cell_id_ref parameter_value_assignment opt_config
      { $$ = new VeriUseClause($2, $4, $3) ; }
    | VERI_USE parameter_value_assignment opt_config
      { $$ = new VeriUseClause(0, $3, $2) ; }
    ;
/******************************* File Include (Verilog 2000)  *************************/

include_statement : VERI_INCLUDE file_path ";"
      {
          // Include this file in current source description:
          // CHECKME: This may use "incdir" to search the file.
          veri_file::IncludeFile($2) ;
          Strings::free($2) ;
      }
    ;

 /* CHECK ME : file_path not defined in LRM (always a string ?) */
file_path : VERI_STRING
       { $$ = $1 ; }
    ;

/******************************* Libraries (Verilog 2000)  *****************************/

library_decl : VERI_LIBRARY file_path_specs opt_incdirs ";"
      {
          // VIPER #5128: As a fix we now parse "library" token along with the identifier
          // from lexer itself. The value of the VERI_LIBRARY token is modified to be
          // string to accomodate the name of the identifier. So, we do not need the
          // 'library_decl_id' rule any more. Adjusted all the required code.

          // FIXME: Need to error out here if we have already parsed normal Verilog constructs
          // other than libmap: library map file must be the first source file on the command line

          if ($3) {
              VeriTreeNode here ; // Will pick-up the present line-file correctly
              here.Warning("ignoring -incdir options for this library declaration") ;
              // Free the -incdir arguments:
              unsigned i ;
              char *path ;
              FOREACH_ARRAY_ITEM(($3), i, path) Strings::free(path) ;
              delete ($3) ;
          }

          veri_file::AddLibraryDecl(new VeriLibraryDecl($1, $2)) ;
      }
    ;

file_path_specs: file_path
      { $$ = new Array() ; ($$)->InsertLast($1) ; }
    | file_path_specs "," file_path
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($3) ; }
    | error
      { $$ = 0 ; }
    ;

opt_incdirs :
      { $$ = 0 ; }
      // VIPER #5128: We now parse "-incdir" as a single token and return VERI_INCDIR
      // for that from lexer. This is also as specified in LRM. But we still have
      // "incdir" as a token to make it a reserve-word/keyword as LRM mentions.
    | VERI_INCDIR file_path_specs
      { $$ = $2 ; }
    ;

/******************************* System Verilog 1800 additions *****************************/
covergroup_decl : VERI_COVERGROUP covergroup_decl_id { push_scope($2) ; create_covergroup_options() ; } opt_tf_port_decl_list opt_coverage_event opt_sample_func ";"
      opt_coverage_spec_or_option_list
      VERI_ENDGROUP //opt_closing_label // VIPER #6377: Do not parse closing label here.
      { $$ = new VeriCovergroup($2, $4, $5, $6, $8, pop_scope()) ;
        // VIPER #6377: Closing labels are parsed up in the grammar.
        //if ($$ && $10) $$->ClosingLabel($10) ;
        //if ($10) update_end_linefile($$) ; // VIPER #6113
        if ($5 && $6) $6->Error("syntax error near %s", "with") ;
      }
    ;

covergroup_decl_id : VERI_ID // FIXME : covergroup_decl_id should be moved in the section containing all other 'decl_id's.
      { $$ = declare_type_id(new VeriIdRef($1), 0) ; }
    ;

opt_coverage_spec_or_option_list  :
      { $$ = 0 ; }
    | opt_coverage_spec_or_option_list coverage_spec_or_option
      { $$ = $1 ? $1 : new Array() ; if ($2) $$->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

coverage_spec_or_option  :
      attr_inst coverage_spec
      { $$ = $2 ; if ($$ && $1) $$->AddAttributes($1) ; if ($1) update_start_linefile($$) ; /* VIPER #7563 */ }
    | attr_inst coverage_option ";"
      { $$ = $2 ; if ($$ && $1) $$->AddAttributes($1) ;  if ($1) update_start_linefile($$) ; /* VIPER #7563 */ }
    ;

coverage_spec :
      cover_point
    | cover_cross
    ;

coverage_option :
      hier_ref "=" expr
      {
          // VIPER #6789: Simple signal assigment is not allowed here, must be hier-name:
          if (($1) && !($1)->IsHierName()) {
              ($1)->Error("syntax error near %s", "=") ;
          } else {
              // VIPER #6789: Even if it is hier-name, it must start with "option" or "type_option":
              VeriName *prefix = $1 ;
              while (prefix && prefix->GetPrefix()) prefix = prefix->GetPrefix() ;
              if (prefix && !Strings::compare(prefix->GetName(), "option") &&
                            !Strings::compare(prefix->GetName(), "type_option")) {
                  prefix->Error("syntax error near %s", prefix->GetName()) ; // Error
              }
          }
          $$ = new VeriCoverageOption($1, $3) ;
      }
    ;

opt_coverage_event : { $$ = 0 ; }    // Optional rule, so NULL allowed
    | clocking_event
      { $$ = new VeriEventExpression(0, $1) ; }
    | VERI_STEPCONTROL_OP "(" block_event_expr ")"
      { $$ = new VeriEventExpression(VERI_STEPCONTROL_OP, $3) ; }
    ;
 // VIPER #5710 : VCS Compatibility issue (near_with), allow sample function
opt_sample_func : { $$ = 0 ; }
    | VERI_WITH VERI_FUNCTION id_ref { push_scope(0) ; } opt_tf_port_decl_list
      { $$ = new VeriFunctionDecl(0, 0, $3, $5, 0, 0, pop_scope()) ; }
    ;

block_event_expr :
      block_event_expr VERI_OR block_event_expr
      { $$ = new VeriBinaryOperator(VERI_OR, $1, $3) ; }
    | VERI_BEGIN hierarchical_btf_identifier
      { $$ = new VeriUnaryOperator(VERI_BEGIN, $2) ; }
    | VERI_END hierarchical_btf_identifier
      { $$ = new VeriUnaryOperator(VERI_END, $2) ; }
    ;

hierarchical_btf_identifier :
      hier_ref // Includes both 'hierarchical_tf_identifier' and 'hierarchical_block_identifier' (both are hier-refs)
      { $$ = $1 ; }
    | hier_ref VERI_ID // Rule for optional 'class_scope'. FIXME : How it can point to task/func/block id
      { $$ = $1 ; Strings::free($2) ; }
    | hier_ref class_scope VERI_ID   // FIXME : How it can point to task/func/block id
      { $$ = $1 ; delete ($2) ; Strings::free($3) ; }
    ;

cover_point :
      opt_cover_point_id VERI_COVERPOINT { push_scope($1) ; create_coverpoint_options() ; } expr opt_iff_expr bins_or_empty
      { $$ = new VeriCoverageSpec(VERI_COVERPOINT, $1, $4, $5, $6, pop_scope()) ; }
    ;

opt_cover_point_id : { $$ = 0 ; }
    |  VERI_ID ":" { $$ = declare_block_id(new VeriBlockId($1)) ; }
    ;

opt_iff_expr : { $$ = 0 ; }
    | VERI_IFF "(" expr ")" { $$ = $3 ; }
    ;

bins_or_empty :
     "{" attr_inst bins_or_options_list "}"
      {
          $$ = $3 ;
          if ($2 && $3) {
              VeriModuleItem *last_item = (VeriModuleItem*)$3->GetLast() ;
              if (last_item) last_item->AddAttributes($2) ;
              update_start_linefile(last_item, &(@2)) ; // VIPER #7563
          }
      }
    | ";"
      { $$ = 0 ; }
    ;

bins_or_options_list :
      { $$ = 0 ; }
    | bins_or_options_list bins_or_options ";"
      { $$ = $1 ? $1 : new Array() ; if ($2) $$->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

bins_or_options :
      coverage_option
      { $$ = $1 ; }
    // VIPER #5196: Write bin_lvalue flat here so that the id is created after the initial value.
    // This will prevent resolving id-ref in initial value to the bin-lvalue itself.
    // FIXME : All types of 'bin_lvalue' is not legal for all types of 'bin_value'.
    //| opt_wildcard bins_keyword bins_lvalue "=" bin_value
    //  { $$ = new VeriBinDecl($1, $2, $3) ; if ($3) $3->SetInitialValue($5) ; }
    | opt_wildcard bins_keyword VERI_ID "=" bin_value
      {
        VeriIdDef *bin_id = declare_id(new VeriBinsId($3)) ;
        if (bin_id) bin_id->SetInitialValue($5) ;
        $$ = new VeriBinDecl($1, $2, bin_id) ;
      }
    | opt_wildcard bins_keyword VERI_ID "[" expr "]" "=" bin_value
      {
        VeriIdDef *bin_id = declare_id(new VeriBinsId($3)) ;
        if (bin_id) {
            bin_id->SetInitialValue($8) ;
            bin_id->SetMemoryRange(new VeriRange($5, 0, VERI_NONE)) ;
        }
        $$ = new VeriBinDecl($1, $2, bin_id) ;
      }
    | opt_wildcard bins_keyword VERI_ID "[" "]" "=" bin_value
      {
        VeriIdDef *bin_id = declare_id(new VeriBinsId($3));
        if (bin_id) {
            bin_id->SetInitialValue($7) ;
            bin_id->SetMemoryRange(new VeriRange(0, 0, VERI_NONE)) ;
        }
        $$ = new VeriBinDecl($1, $2, bin_id) ;
      }
    ;

opt_wildcard : { $$ = 0 ; }
    | VERI_WILDCARD { $$ = VERI_WILDCARD ; }
    ;
  /*
bins_lvalue :
      VERI_ID
      { $$ = declare_id(new VeriVariable($1)) ; }
    | VERI_ID "[" expr "]"
      { $$ = declare_id(new VeriVariable($1)) ; if ($$) $$->SetMemoryRange(new VeriRange($3, 0, VERI_NONE)) ; }
    | VERI_ID "[" "]"
      { $$ = declare_id(new VeriVariable($1)) ; if ($$) $$->SetMemoryRange(new VeriRange(0, 0, VERI_NONE)) ; }
    ;
  */
bins_keyword : VERI_BINS  { $$ = VERI_BINS ;         }
    | VERI_ILLEGAL_BINS   { $$ = VERI_ILLEGAL_BINS ; }
    | VERI_IGNOREBINS     { $$ = VERI_IGNOREBINS ;   }
    ;

bin_value :
      "{" value_range_list "}" opt_iff_expr
      { $$ = new VeriOpenRangeBinValue($2, $4) ; }
    | trans_list opt_iff_expr
      { $$ = new VeriTransBinValue($1, $2) ; }
    | VERI_DEFAULT opt_iff_expr
      { $$ = new VeriDefaultBinValue(0, $2) ; }
    | VERI_DEFAULT VERI_SEQUENCE opt_iff_expr
      { $$ = new VeriDefaultBinValue(VERI_SEQUENCE, $3) ; }
    ;

trans_list  :
      "(" trans_set ")"
      { $$ = new Array() ; ($$)->InsertLast($2) ; }
    | trans_list "," "(" trans_set ")"
      { $$ = ($1) ? $1 : new Array() ; ($$)->InsertLast($4) ; }
    ;

trans_set :
     array_of_trans_range_list
     { $$ = new VeriTransSet($1) ; }
    ;

array_of_trans_range_list :
      trans_range_list
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | array_of_trans_range_list VERI_LEADTO trans_range_list
      { $$ = $1 ? $1 : new Array() ; if ($$) $$->InsertLast($3) ; }
    ;

trans_range_list :
      { within_bins_repeat = 1 ; } value_range_list opt_repeat_item
      {
          // FIXME: Ideally 'within_bins_repeat' should be set in-between 'value_range_list'
          // and 'opt_repeat_item', so that while we parse only 'opt_repeat_item' we know
          // that we are parsing bins repeat items. But the trick does not work there since
          // bison need to read one token ahead to match with a rule. Thus 'opt_repeat_item'
          // are parsed in flex before setting 'within_bins_repeat' in that case. So, we had
          // to move the set statement of 'within_bins_repeat' before 'value_range_list'.
          within_bins_repeat = 0 ;
          $$ = new VeriTransRangeList($2, $3) ;
      }
    ;

opt_repeat_item :
      { $$ = 0 ; } // Optional rule, so NULL allowed
    | VERI_CONSECUTIVE_REPEAT repeat_range "]"  // FIXME : Two different tokens viz. "[*=" and "[=" O have the same token name associated with them viz VERI_CONSECUTIVE_REPEAT (as defined in lex). Same with "[* " and [*" , "[*->" and "[->".
      { $$ = new VeriBinaryOperator(VERI_CONSECUTIVE_REPEAT, 0, $2) ; }
    | VERI_GOTO_REPEAT repeat_range "]"
      { $$ = new VeriBinaryOperator(VERI_GOTO_REPEAT, 0, $2) ; }
    | VERI_NON_CONSECUTIVE_REPEAT repeat_range "]"
      { $$ = new VeriBinaryOperator(VERI_NON_CONSECUTIVE_REPEAT, 0, $2) ; }
    ;

repeat_range :
      expr
      { $$ = $1 ; }
    | expr ":" expr
      { $$ = new VeriRange($1,$3,0) ; }
    ;

cover_cross :
      opt_cover_point_id VERI_CROSS id_list { push_scope($1) ; create_covercross_options() ; } opt_iff_expr select_bins_or_emtpy
      // FIXME : id_list should be checked for a min. of 2 ids. If the size of the array is less than 2 then errror out.
      { $$ = new VeriCoverageSpec(VERI_CROSS, $1, $3, $5, $6, pop_scope()) ; }
    ;

select_bins_or_emtpy :
      "{" opt_bins_selection_or_option_list "}"
      { $$ = $2 ? $2 : new Array() ; }
    | ";"
      { $$ = 0 ; }
    ;

opt_bins_selection_or_option_list :
      { $$ = 0 ; }
    | opt_bins_selection_or_option_list bins_selection_or_option ";"
      { $$ = $1 ? $1 : new Array() ; if ($$) $$->InsertLast($2) ; }
    ;

bins_selection_or_option :
      attr_inst coverage_option
      { $$ = $2 ; if ($1 && $2) $2->AddAttributes($1) ; if ($1) update_start_linefile($$) ; /* VIPER #7563 */  }
    | attr_inst opt_wildcard bins_keyword VERI_ID "=" select_expr opt_iff_expr
      {
          VeriIdDef *bin_id = declare_id(new VeriVariable($4)) ;
          if (bin_id) bin_id->SetInitialValue(new VeriSelectBinValue($6, $7)) ;
          $$ = new VeriBinDecl($2, $3, bin_id) ;
          if ($1) ($$)->AddAttributes($1) ;
          if ($1) update_start_linefile($$) ; /* VIPER #7563 */
          if ($2 && $$) $$->Warning("wildcard in cross coverage violates IEEE 1800 syntax") ;
      }
    ;

select_expr :
      select_condition
      { $$ = $1 ; }
    | select_expr VERI_LOGEQ attr_inst expr
      {
          $$ = new VeriBinaryOperator(VERI_LOGEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ;
          if ($$) $$->Warning("%s in select expression violates IEEE 1800 syntax", "==") ;
      }
    | select_expr VERI_LOGNEQ attr_inst expr
      {
          $$ = new VeriBinaryOperator(VERI_LOGNEQ,$1,$4) ; if ($3) ($$)->AddAttributes($3) ;
          if ($$) $$->Warning("%s in select expression violates IEEE 1800 syntax", "!=") ;
      }
    | "!" select_condition
      { $$ = new VeriUnaryOperator(VERI_LOGNOT, $2) ; }
    | select_expr VERI_LOGAND select_expr
      { $$ = new VeriBinaryOperator(VERI_LOGAND, $1, $3) ; }
    | select_expr VERI_LOGOR select_expr
      { $$ = new VeriBinaryOperator(VERI_LOGOR, $1, $3) ; }
    | "(" select_expr ")"
      { $$ = $2 ; }
    ;

select_condition :
      VERI_BINSOF "(" bins_expr ")" VERI_INTERSECT "{" value_range_list "}"
      { $$ = new VeriSelectCondition($3, $7) ; }
    | VERI_BINSOF "(" bins_expr ")"
      { $$ = new VeriSelectCondition($3, 0) ; }
    | hier_ref
      {
         $$ = $1 ;
         if ($$) $$->Warning("%s in select expression violates IEEE 1800 syntax", "name reference") ;
      }
    ;

bins_expr :
      id_ref
      { $$ = $1 ; }
    | bins_expr "." VERI_ID // VIPER #5707 : Allow hierarchical name with any number of elements
      { $$ = new VeriSelectedName($1, $3) ; }
    ;

package_decl :
      VERI_PACKAGE { design_unit_depth++ ; config_is_identifier++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; }
      opt_lifetime package_decl_id opt_pragma
      {
          push_scope($4) ;
          // VIPER #4847: Packages cannot use root scope, so set upper to 0.
          // But save the current upper scope, we want to restore it back:
          $<scope>$ = present_scope->Upper() ;
          present_scope->SetUpper(0) ;
          parsing_package = 1 ; /* VIPER #5244 */
      } ";" opt_pragma
      package_items
      VERI_ENDPACKAGE
      {
        design_unit_depth-- ; config_is_identifier-- ;
        // VIPER #4847: Restore the upper scope, so that 'pop_scope' works correctly.
        // Upper scope of the package gets reset again in the package constructor itself:
        present_scope->SetUpper($<scope>6) ;
        $$ = new VeriPackage($4, $9, pop_scope(1 /* process package scope*/)) ;
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale
        // Add pragmas from halfway..
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from before the header
        if ($8) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$8)) ; // pragma's from after the header
        if ($3) ($$)->SetQualifier($3) ;
        parsing_package  = 0 ; // VIPER #5244
      }
    ;

package_items :
      { $$ = 0 ; }
    | package_items package_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

// VIPER 2534 : package item can have attributes and comments around it, and sometimes a closing label.
package_item : attr_inst pre_comment plain_package_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    | attr_inst timescale_specification
      {
          store_attribute($1) ; // Store attribute to global array
          // VIPER #3873 & #5127: Parse and error out for `timscale inside package, do not produce syntax error
          $$ = 0 ;
          // VIPER #7703: LRM does not stop specifying timescale within design units.
          // Simulators also stopped producing warning. So, do not produce warning here:
          //Strings::free($2) ;
          //VeriTreeNode tmp ;
          //tmp.Warning("compiler directive %s is not allowed here", "`timescale") ;
      }
    ;

plain_package_item : // VIPER 2534
      anonymous_program_decl
      { $$ = $1 ; }
    | timeunits_decl
    | module_or_generate_item_decl
    | class_decl
    | checker_decl
      { // nested checker declaration.
        $$ = $1 ;
        // force-declare id in scope
        (void) declare_forced(($$)?($$)->GetId():0) ;
      }
    | parameter_decl
    | local_parameter_decl
    | overload_declaration
    | concurrent_assertion_item_decl
    | package_export_decl // VIPER #5845
    ;

package_export_decl : VERI_EXPORT { export_decl = 1 ;} package_import_items ";"
      { $$ = new VeriExportDecl($3) ; export_decl = 0 ; }

    | VERI_EXPORT { export_decl = 1 ;} "*" "::" "*" ";" // madhurima
      { $$ = new VeriExportDecl(0 /* export all*/) ; export_decl = 0 ; if (present_scope) present_scope->AddExportItem(0 /* all pkgs */, 0 /* all ids*/, 0) ; /* VIPER #7232*/}
    ;

anonymous_program_decl :
      VERI_PROGRAM { design_unit_depth++ ; config_is_identifier++ ; $<str>$ = Strings::save(veri_file::GetTimeScale()) ; } opt_pragma ";" opt_pragma
      anonymous_program_items
      VERI_ENDPROGRAM
      {
        design_unit_depth-- ; config_is_identifier-- ;
        $$ = new VeriProgram(0, 0, 0,$6, 0) ; // Anonymous program items share the same scope as the package/compilation unit in which they are declared
        ($$)->SetTimeScale($<str>2) ; Strings::free($<str>2) ; // VIPER #7703: Set the timescale
        // Add pragmas from halfway..
        if ($3) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$3)) ; // pragma's from before the header
        if ($5) ($$)->AddAttributes(VeriNode::PragmasToAttributes(0,$5)) ; // pragma's from after the header
      }
    ;

anonymous_program_items :
      { $$ = 0 ; }
    | anonymous_program_items anonymous_program_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    | error
      { $$ = 0 ; }
    ;

anonymous_program_item : attr_inst pre_comment plain_anonymous_program_item opt_comment opt_closing_label
      { $$ = $3 ;
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ; // VIPER #7563
        add_pre_comments_to_node($$, $2) ;
        add_post_comments_to_node($$, $4) ;
        if ($5 && $$) ($$)->ClosingLabel($5) ;
        if ($5) update_end_linefile($$) ; // VIPER #6113
      }
    | attr_inst ";"
      { $$ = new VeriNullStatement() ; // VIPER #8419: create null statement
        if ($1 && $$) ($$)->AddAttributes($1) ;
        if ($1) update_start_linefile($$) ;
      }
    ;

plain_anonymous_program_item :
      task_decl
    | extended_task_decl
    | extended_function_decl
    | function_decl
    | class_decl
    | covergroup_decl
    ;

package_import_declaration :
      VERI_IMPORT package_import_items ";"
      { $$  = new VeriImportDecl($2) ; }
    ;

package_import_items :
      package_import_item
      { $$  = new Array() ; if ($$) $$->InsertLast($1) ; }
    | package_import_items "," package_import_item
      { $$  = $1 ; if ($$) $$->InsertLast($3) ; }
    ;

package_import_item :
      id_ref "::" VERI_ID
      {
          // Create a scoped name for the tree :
          VeriScopeName *import_item = new VeriScopeName($1, $3, 0) ;
          $$ = import_item ;

          // Identifier $3 of package made visible from present_scope
          // This code could move to 'Resolve' if resolve is done on statement by statement basis.
          const char *prefix = ($1) ? ($1)->GetName(): 0 ;
          const char *suffix = import_item->GetSuffix() ;

          // Make the scope links (link the package scope to the present scope).
          // Decide on which library to search for this package.
          const char *pkg_lib = "work" ; // The default library to find
          // VIPER #5248 (test_288): Try to get the package from the curent working library:
          // Even if the package name is 'std'. It may be redefined by user under compatibility mode:
          VeriLibrary *work_lib = veri_file::GetWorkLib(LineFile::GetFileName(($1)->Linefile())) ;
          if (work_lib) pkg_lib = work_lib->GetName() ;
          VeriModule *package = veri_file::GetModule(prefix, 1, pkg_lib) ;

          // VIPER #4774: Check whether the referred package is 'std'.
          if (!package && Strings::compare(prefix, "std")) {
              // Then search library 'std' too:
              package = veri_file::GetModule(prefix, 1, "std") ;
          }
          // VIPER #5244: It is not an error to import a package in that package itself.
          // If containing module is a package of same name as the prefix, it is
          // import of a package item within itself:
          unsigned same_owner_prefix = 0 ;
          //if (!package && parsing_package) {
          if (parsing_package) {
              VeriIdDef *containing_module = present_scope ? present_scope->GetContainingModule() : 0 ;
              if (containing_module && containing_module->IsModule()) {
                  if (Strings::compare(prefix, containing_module->GetName()))  same_owner_prefix = 1 ;
              }
          }
          // VIPER #5618: Check in -L specific libraries for the package in that order:
          if (!package && !same_owner_prefix) package = veri_file::GetPackageFromLOptions(prefix) ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
          // VIPER #6895: Find package from vhdl and convert it to verilog package
          if (!package && !same_owner_prefix) {
              package = VeriNode::ConvertVhdlToVerilogPackage(prefix, pkg_lib) ;
          }
#endif

          // If parsing package is imported within itself, do not set itself
          // or previously compiled same named package in using list.
          if (same_owner_prefix) {
              VeriIdDef *containing_module = present_scope ? present_scope->GetContainingModule() : 0 ;
              ($1)->SetId(containing_module) ;
          } else if (!package || !package->IsPackage()) { // Prefix is not a package
              ($1)->Error("%s is not declared", prefix) ;
          } else /* if (package) */ {
              ($1)->SetId(package->GetId()) ;
              VeriScope *package_scope = package->GetScope() ;
              // Check if 'id' is declared within package
              VeriIdDef *obj_id = package_scope ? package_scope->FindLocal(suffix) : 0 ;
              // VIPER #7232 : Check 'id' in exported items
              if (!obj_id) obj_id = package_scope ? package_scope->FindExported(suffix): 0 ;
              if (obj_id) {
                  // export Pkg::x ; means import Pkg::x ; is not already imported
                  if (present_scope) {
                      if (export_decl) {
                          // VIPER #7232 : Add export decl
                          present_scope->AddExportItem(package_scope, suffix, import_item) ;
                      } else {
                          present_scope->AddImportItem(package_scope, suffix, import_item) ;
                      }
                  }
                  import_item->SetId(obj_id) ; // Set suffix id
              } else { // 'id' is not declared within 'package', Error
                  if (export_decl) {
                      ($$)->Error("package export %s::%s failed, name not found",package->Name(), suffix) ;
                  } else {
                      ($$)->Error("%s is not declared within package %s", suffix, package->Name()) ;
                  }
              }
          }
      }
    | id_ref "::" "*"
      {
          // Create a scoped name for the tree :
          VeriScopeName *import_item = new VeriScopeName($1, 0, 0) ;
          $$ = import_item ;

          // All identifiers from the package made visible from present_scope
          // This code could move to 'Resolve' if resolve is done on statement by statement basis.
          const char *prefix = ($1)->GetName() ;

          // Make the scope links (link the package scope to the present scope).
          // Decide on which library to search for this package.
          const char *pkg_lib = "work" ; // The default library to find
          // VIPER #5248 (test_288): Try to get the package from the curent working library:
          // Even if the package name is 'std'. It may be redefined by user under compatibility mode:
          VeriLibrary *work_lib = veri_file::GetWorkLib(LineFile::GetFileName(($1)->Linefile())) ;
          if (work_lib) pkg_lib = work_lib->GetName() ;
          VeriModule *package = veri_file::GetModule(prefix, 1, pkg_lib) ;

          // VIPER #4774: Check whether the referred package is 'std'.
          if (!package && Strings::compare(prefix, "std")) {
              // Then search library 'std' too:
              package = veri_file::GetModule(prefix, 1, "std") ;
          }
          // VIPER #5244: It is not an error to import a package in that package itself.
          // If containing module is a package of same name as the prefix, it is
          // import of a package item within itself:
          unsigned same_owner_prefix = 0 ;
          //if (!package && parsing_package) {
          if (parsing_package) {
              VeriIdDef *containing_module = present_scope ? present_scope->GetContainingModule() : 0 ;
              if (containing_module && containing_module->IsModule()) {
                  if (Strings::compare(prefix, containing_module->GetName()))  same_owner_prefix = 1 ;
              }
          }
          // VIPER #5618: Check in -L specific libraries for the package in that order:
          if (!package && !same_owner_prefix) package = veri_file::GetPackageFromLOptions(prefix) ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
          // VIPER #6895: Find package from vhdl and convert it to verilog package
          if (!package && !same_owner_prefix) {
              package = VeriNode::ConvertVhdlToVerilogPackage(prefix, pkg_lib) ;
          }
#endif

          // If parsing package is imported within itself, do not set itself
          // or previously compiled same named package in using list.
          if (same_owner_prefix) {
              VeriIdDef *containing_module = present_scope ? present_scope->GetContainingModule() : 0 ;
              ($1)->SetId(containing_module) ;
          } else if (!package || !package->IsPackage()) { // Prefix is not a package
              ($1)->Error("%s is not declared", prefix) ;
          } else /* if (package) */ {
              ($1)->SetId(package->GetId()) ;
              VeriScope *package_scope = package->GetScope() ;
              // All identifiers of package 'package' is now visible to 'scope'
              // Register this
              if (present_scope) {
                  if (export_decl) {
                      // VIPER #7232 : Add export item
                      present_scope->AddExportItem(package_scope, 0 /* all ids*/, import_item) ;
                  } else {
                      present_scope->AddImportItem(package_scope, 0 /* all ids*/, import_item) ;
                  }
              }
          }
      }
    ;

streaming_concatenation :
      "{" stream_operator slice_size "{" concat_elements "}" "}"
      { $$ = new VeriStreamingConcat($2, $3, $5) ; }
    | "{" stream_operator "{" concat_elements "}" "}"
      { $$ = new VeriStreamingConcat($2, 0, $4) ; }
    ;

stream_operator :
      VERI_LSHIFT { $$ = VERI_LSHIFT ; }
    | VERI_RSHIFT { $$ = VERI_RSHIFT ; }
    ;

slice_size :
      simple_type { $$ = new VeriDataType($1, 0, 0) ; }
    | expr        { $$ = $1 ; }
    | struct_union
      {
          $$ = $1 ;
          VeriTreeNode tmp_node ;
          tmp_node.Warning("struct/union/enum as slice size in streaming concatenation violates IEEE 1800 syntax") ;
      }
    | enum_type
      {
          $$ = $1 ;
          VeriTreeNode tmp_node ;
          tmp_node.Warning("struct/union/enum as slice size in streaming concatenation violates IEEE 1800 syntax") ;
      }
    ;

randsequence_statement :
      VERI_RANDSEQUENCE { push_scope(0) ; } "(" opt_production_id ")" production_list VERI_ENDSEQUENCE
      { $$ = new VeriRandsequence($4, $6, pop_scope()) ; }
    ;

opt_production_id :
      { $$ = 0 ; }
    | id_ref
      { $$ = $1 ; }
    ;

production_list :
      production
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | production_list production
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    ;

production_id : VERI_ID { $$ = declare_id(new VeriProductionId($1)) ;} ; // FIXME : To be moved to area containing other ids under System Verilog product marker

production :
      func_data_type_or_void production_id { push_scope($2) ; } opt_tf_port_decl_list ":" rs_rule_list ";"
      { $$ = new VeriProduction($1, $2, $4, $6, pop_scope()) ; }
    | production_id { push_scope($1) ; } opt_tf_port_decl_list ":" rs_rule_list ";"
      { $$ = new VeriProduction(0, $1, $3, $5, pop_scope()) ; }
    ;

rs_rule_list :
      rs_rule
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | rs_rule_list "|" rs_rule
      {  $$ = ($1) ? $1 : new Array() ; if ($3) ($$)->InsertLast($3) ; }
    ;

rs_rule :
      rs_production_list ":=" weight_spec { push_scope(0); } rs_code_block
      { $$ = $1 ; if ($$) $$->SetWeightSpec($3) ; if ($$) $$->SetCodeBlock($5) ; update_end_linefile($$) ; }
    | rs_production_list ":=" weight_spec
      { $$ = $1 ; if ($$) $$->SetWeightSpec($3) ; update_end_linefile($$) ; }
    | rs_production_list
      { $$ = $1 ; }
    ;

rs_production_list :
      rs_prod_list
      { $$ = new VeriProductionItem(0, 0, $1) ; }
    | VERI_RAND VERI_JOIN "(" expr ")" production_item_list // The production_item_list should contain at least two production items
      { $$ = new VeriProductionItem(1, $4, $6) ; }
    | VERI_RAND VERI_JOIN production_item_list // The production_item_list should contain at least two production items
      { $$ = new VeriProductionItem(1, 0, $3) ; }
    ;

rs_prod_list:
      rs_prod
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | rs_prod_list rs_prod
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    ;

production_item_list :
      production_item
      { $$ = new Array() ; if ($$) ($$)->InsertLast($1) ; }
    | production_item_list production_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    ;

production_item :
      id_ref opt_list_of_arguments
      { $$ = new VeriTaskEnable($1, $2) ; }
    ;

rs_code_block :
      "{" block_item_decls block_statements "}" // Here block_item_decls can contain only data_decls
      { $$ = new VeriCodeBlock($2, $3, pop_scope()) ; }
    | "{" block_item_decls "}"
      { $$ = new VeriCodeBlock($2, 0, pop_scope()) ; }
    | "{"block_statements "}"
      { $$ = new VeriCodeBlock(0, $2, pop_scope()) ; }
    | "{" "}"
      { $$ = new VeriCodeBlock(0, 0, pop_scope()) ; }
    ;

rs_prod :
      production_item
      { $$ = $1 ; }
    | { push_scope(0) ; } rs_code_block
      { $$ = $2 ; }
    | VERI_IF "(" expr ")" production_item  %prec veri_no_else // rs_if_else
      { $$ = new VeriConditionalStatement($3, $5, 0) ; }
    | VERI_IF "(" expr ")" production_item VERI_ELSE production_item // rs_if_else
      { $$ = new VeriConditionalStatement($3, $5, $7) ; }
    | VERI_REPEAT { push_scope(0) ; } "(" expr ")" production_item // rs_repeat
      { $$ = new VeriRepeat($4, $6, pop_scope()) ; }
    | VERI_CASE "(" expr ")" rs_case_item_list VERI_ENDCASE // rs_case
      { $$ = new VeriCaseStatement(VERI_CASE, $3, $5) ; }
    ;

rs_case_item :
      expr_list ":" production_item ";"
      { $$ = new VeriCaseItem($1, $3) ; }
    | VERI_DEFAULT opt_colon production_item ";"
      { $$ = new VeriCaseItem(0, $3) ; }
    ;

rs_case_item_list :
      rs_case_item
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | rs_case_item_list rs_case_item
      {  $$ = ($1) ? $1 : new Array() ; if ($2) ($$)->InsertLast($2) ; }
    ;

weight_spec :
      number // only integral types are allowed
      { $$ = $1 ; }
    | hier_ref
      { $$ = $1 ; }
    | "(" expr ")"
      { $$ = $2 ; }
    ;

randcase_statement :
      VERI_RANDCASE randcase_item_list VERI_ENDCASE
      { $$ = new VeriCaseStatement(VERI_RANDCASE, 0, $2) ; }
    ;

randcase_item_list :
      randcase_item
      { $$ = new Array() ; if ($$) $$->InsertLast($1) ; }
    | randcase_item_list randcase_item
      { $$ = $1 ; if ($$) $$->InsertLast($2) ; }
    ;

randcase_item :
      expr ":" statement_or_null
      {
        Array *expr_list = new Array(1) ;
        expr_list->InsertLast($1) ;
        $$ = new VeriCaseItem(expr_list, $3) ;
      }
    ;

    /* VIPER #5710 : System verilog 1800-2009 addition */
let_declaration : VERI_LET let_decl_id { within_seq_property = 1 ; push_scope($2) ; } opt_tf_port_decl_list "=" expr ";"
       { $$ = new VeriLetDecl($2, $4, $6, pop_scope()) ; within_seq_property = 0 ; }
    ;

%%

void
veri_file::StartYacc()
{
    // VIPER #6109: Rest static globals:
    within_class = 0 ;
    within_bins_repeat = 0 ;
    within_extern_method = 0 ;
    within_seq_property = 0 ;

    tmp_buf_idx = 0 ;
    tmp_buf[tmp_buf_idx] = '\0' ;
    udp_edge_indicator_found = 0 ; // VIPER #8496: Reset flag
    implicit_nets_allowed = 0 ;
    port_connect_has_attribute = 0 ;
    resolve_idrefs = 1 ;
    parsing_package = 0 ;
    export_decl = 0 ;
    VERIFIC_ASSERT(!last_label) ; // VIPER #7714: Starts with 0 and always reset in EndYacc()

    first_line = 0 ;
    first_column = 0 ;
    last_line = 0 ;
    last_column = 0 ;
    file_id = 0 ; // VIPER #6114: Rest static global file_id

    rhs_0_initialized  = 0 ; // VIPER #5675: Start with Rhs[0] as uninitialized
    design_unit_depth = 0 ;
    config_is_identifier = 0 ; // VIPER #6079: Current status/context of the 'config' string in input RTL.

    // Set the scope to the root module scope
    // We have to create a (SystemVerilog/AMS) root module here, even if we parse in Verilog2k mode, because :
    // (1) if there is a root-item declaration parsed, then the identifiers in that item need to be stored in a scope (or else memory leak occurs)
    // (2) if there is a scope, then it needs to be owned by a module (or else memory leak occurs).
    // Only if you do not have the SystemVerilog or AMS product, then the GetRootModule() routine always returns 0,
    // because the syntax does not allow a root-item. So there is no risk for the two issues above to occur.
    veri_file::StartCompilationUnit() ;
}

linefile_type
veri_file::GetRuleLineFile()
{
    // Get the linefile info from the last 'rule' (as opposed to last token)

    // get it for the last token (since that includes the filename-id, which is not available here in bison)
    linefile_type result = GetLineFile() ;
    // VIPER #4002 side effect: Don't try to set line & file info if GetLineFile returns null:
    if (!result) return result ;
    // and adjust it to the info in the last rule :

    // Exact range info from the last rule is available in the bison-generated yylloc variable.
    // However, that one is local to routine 'yyparse', so we export it via
    // static variables, set in the overridden macro YYLLOC_DEFAULT.

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Use all location info to initialize the result to latest bison rule that was executed.
    // GetLineFile() already made a copy.
    result->SetLeftCol(first_column) ;
    result->SetLeftLine(first_line) ;
    result->SetRightCol(last_column) ;
    result->SetRightLine(last_line) ;
#else
    // Use only line info from the current (latest) bison rule executed:
    // Decide here which line number (first or last in the rule) should be set as the rule's line number..
#ifdef VERILOG_USE_STARTING_LINEFILE_INFO
    result = LineFile::SetLineNo(result, first_line) ;
#else
    result = LineFile::SetLineNo(result, last_line) ;
#endif

#endif

    // Set the file id also:
    result = LineFile::SetFileId(result, file_id) ;

    return result ;
}

VeriScope *
veri_file::GetPresentScope()
{
    // return the current scope :
    return present_scope ;
}

void
veri_file::EndYacc()
{
    ProcessPragmasForScope(present_scope, 1) ; // Viper #3584
    present_scope = 0 ; // VIPER #4523: Reset it, we are done with yacc
    delete last_label ; last_label = 0 ; // VIPER #7714: Delete it, so that the memory is released
}

int
veri_file::Parse()
{
#if YYDEBUG
    yydebug = 1 ;
#endif // YYDEBUG

    return yyparse() ;
}

// Added following for Viper #3584 : Remember the meta comment pragma attributes
// when they are given BEFORE the actual objects (that they apply on) are declared
// They are remembered into the static map pragma_id_map, and at the end of the
// scope, this map is processed for the entries against the scope
void
veri_file::AddPragmaToScope(const char *id_name, const char *attr_name, const char *attr_val)
{
    if (!present_scope) return ;
    if (!pragma_id_map) pragma_id_map = new Map(POINTER_HASH) ;

    Array *pragma_info = new Array(4) ;

    // Array holds attribute_target + attr_name + attr_val
    pragma_info->InsertLast(Strings::save(id_name)) ;
    pragma_info->InsertLast(Strings::save(attr_name)) ;
    pragma_info->InsertLast(Strings::save(attr_val)) ;

    VeriTreeNode *tmp_node = new VeriTreeNode(veri_file::GetLineFile()) ;
    pragma_info->InsertLast(tmp_node) ;

    (void)pragma_id_map->Insert(present_scope, pragma_info, 0, 1) ;

    return ;
}
void
veri_file::StartCompilationUnit()
{
     VeriModule *root_module = veri_file::GetRootModule(1/*create if needed*/) ;
     present_scope = (root_module) ? root_module->GetScope() : 0 ;
}

unsigned
veri_file::IsWithinClass()
{
    return within_class ;
}

unsigned
veri_file::IsWithinBinsRepeat()
{
    // VIPER #5248 (open_sqr_coveragebins): Return the bins repeat context only in compatibility mode:
    //return within_bins_repeat ;
    return (VeriNode::InRelaxedCheckingMode()) ? within_bins_repeat : 0 ;
}

/* static */ int
veri_file::GetDesignUnitDepth()
{
    // Return the current depth of the design unit
    // Usefull for checking whether we are within nested design unit or at $unit scope:
    return design_unit_depth ;
}

/* static */ int
veri_file::ConfigIsIdentifier()
{
    // VIPER #6079: Return the current status/context of the 'config' string in input RTL.
    // Usefull for checking whether we should treat 'config' as keyword or identifier:
    return config_is_identifier ;
}

/* static */ unsigned
veri_file::IsParserActive()
{
    // VIPER #7657: If this scope pointer is set, assume parser is in active state:
    // NOTE: Even in expression parsing flow, we set the present scope from StartYacc()
    // routine with the call to StartCompilationUnit(). So, it works even then.
    // We may want to use a specific flag, if required, which will be set
    // from StartYacc() and reset from EndYacc() to be more accurate.
    return (present_scope) ? 1 : 0 ;
}

