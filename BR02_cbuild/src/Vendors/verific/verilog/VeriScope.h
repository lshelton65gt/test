/*
 *
 * [ File Version : 1.112 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_SCOPE_H_
#define _VERIFIC_VERI_SCOPE_H_

// Include headers here..
#include "VerificSystem.h"
#include "VeriCompileFlags.h" // Verilog-specific compile flags

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// Define referenced classes here
class Map ;
class VeriMapForCopy ;
class Set ;
class Array ;

class VeriTreeNode ;
class VeriIdDef ;
class SaveRestore ;
class VeriStructUnion ;

/* -------------------------------------------------------------- */

/*
    VeriScope is a symbol table of locally declared identifiers.
    Stored char*name->VeriIdDef* in a (associated) Map.
    Scope structure of LRM 10.5 applies here :

    Upper level scope (containing this one) is accessible with Upper().
    Lower level scopes (contained in this) is accessible with SubScope(name)

    The identifier that owns (created) this scope is available with Owner()
    Owner is a module, a task, a function a named block, or 0 for an unnamed block.

    Regular search (direct visibility) is done with Find(name). This looks through
    the scope for the give identifier by name. Returns 0 if not found.
*/

class VFC_DLL_PORT VeriScope
{
public:
    explicit VeriScope(VeriScope *upper=0, VeriIdDef *owner=0) ; // Create a new level scope
    explicit VeriScope(unsigned empty, VeriScope *upper=0) ; // Create a new empty scope for unnamed seq/par block and loops : VIPER 2573, 2595, 2603

    // Add identifier to the scope
    unsigned            Declare(VeriIdDef *id, unsigned force_insert=0, VeriIdDef **existing_id=0) ;   // Add declared identifier to scope (return 1 if id by that name was not there, 0 if such a name is already declared here). if 'force_insert' is set, we always insert the identifier. Set existing_id if Declare() fails because of already declared name (VIPER #7773)
    char*               GenerateUniqueVarName(char* id_name) const ;   // Add identifier with unique name
    unsigned            DeclareBlockId(VeriIdDef *id, unsigned force_insert=0, VeriIdDef **existing_id=0) ; // VIPER 2595, 2573, 2603 : Declare block ids in non-empty scope. This should be called to declare only design defined block ids. Set existing_id if DeclareBlockId() fails because of already declared block name (VIPER #7773)
    unsigned            Undeclare(VeriIdDef *id) ;    // Remove identifier from scope

    // Access to various higher-level scopes
    VeriScope *         Upper() const { return _upper ; }   // Give pointer to the upper scope (The scope above 'this' one)
    VeriScope *         ModuleScope() ;                     // Give pointer to the 'module' level scope.
    VeriScope *         TopScope() ;                        // Give pointer to the upper most scope (could be scope of 'root_module' (SV) or simply a module.

    // Find identifiers in the scope
    VeriIdDef *         Find(const char *name, const VeriTreeNode *from = 0) ; // Find one object by name, traverse upward if not found local. Optionally give a treenode from which errors can be reported
    VeriIdDef *         FindLocal(const char *name) const ;                    // Find the object if declared in this immediate, local scope

    // Get a const char *name->VeriIdDef*  map of the locally declared identifiers :
    Map *               GetThisScope() const             { return _this_scope ; }
    Map *               DeclArea() const                 { return _this_scope ; }
    void                GetDeclaredIds(Set &ids) const ; // Inserts the declared ids in this scope and all sub scopes into the given Set

    // Dependencies : label that this scope uses another scope (by use clause or selected name)
    void                Using(VeriScope *other_scope) ;
    Set *               GetUsing() const                 { return _using ; }
    Set *               GetUsedBy() const                { return _used_by ; }
    void                NoLongerUsing(const VeriScope *other_scope, unsigned delete_if_empty =1) ;
    void                DependentScopes(Set &dependent) const ; // Get the scopes on which this scope depends into the Set

    // Child scopes (scopes that use 'this' scope as the '_upper' scope).
    void                AddChildScope(const VeriScope *) ;
    void                RemoveChildScope(VeriScope *) ;
    Set *               GetChildrenScopes() const        { return _children ; }

    // Manage external scope setting :
    void                AddExternalScope(VeriScope *scope) ; // Make external (out-of-this-module) scope available from here. Also does module-depenency management.

    // Modifier methods (used only internally)
    void                SetUpper(VeriScope *scope) ;  // Set upper scope member
    void                SetOwner(VeriIdDef *id)          { _owner = id ; }               // Set owner id member
    void                SetScopeTable(Map *scope_table)  { _this_scope = scope_table ; } // Set present-level scope table. This is very dangerous..

    // Find identifiers that own the scopes
    VeriIdDef *         GetOwner() const                 { return _owner ; } // Get the identifier that created this scope.
    VeriIdDef *         GetSubprogram() const ;       // Get the subprogram (task/function) that encompasses this scope. If any.
    VeriIdDef *         GetSequenceProperty() const ; // Get the sequence/property that encompasses this scope. If any.
    VeriIdDef *         GetBlockLabel() const ;       // Get the block label that most closely encompasses this scope. If any.
    VeriIdDef *         GetContainingModule() const ; // Get the module identifier that encompasses this scope. If any (it should)
    VeriScope *         GetDeclarationScopeOf(const VeriIdDef *id) ;

    // Support for SystemVerilog classes
    VeriIdDef *         GetClass() const ;            // Get the class identifier that encompasses this scope. If any
    void                CheckIncompleteTypedef() const ; // Check and error out for incomplete forward type declarations for this scope
    VeriIdDef *         FindExplicitlyImported(const char *name) const ; // Find 'name' in explicitly imported scopes
    void                SetExtendedScope(VeriScope *s)   { _extended_scope = s ; }
    VeriScope *         GetExtendedScope() const         { return _extended_scope ; }
    unsigned            IsStdPackageExplicitlyImported() const ;
    unsigned            IsStdPackageVisible() const ;
    void                SetBaseClassScope(VeriScope *scope)  { _base_class_scope = scope ; }
    VeriScope *         GetBaseClassScope() const            { return _base_class_scope ; }
    VeriIdDef *         FindInBaseClass(const char *name, const VeriTreeNode *from=0) const ; // Find 'name' in the base classes of this class
    VeriIdDef *         FindInInterfaceClasses(const char *name) const ; // Find 'name' in extended interface classes

    // Export/Import support :
    void                SetExportedSubprogScope(unsigned s)  { _exported_subprog_scope = s   ; }
    unsigned            IsExportedSubprogScope() const       { return _exported_subprog_scope ; }

    // VIPER #4987: Identify scope belonging to some generate construct
    void                SetGenerateScope() ;
    unsigned            IsGenerateScope() const              { return _generate_scope ; }

    // VIPER #6980: Identify scope belonging to some struct union construct
    void                SetStructUnionScope()                { _struct_union_scope = 1 ; }
    unsigned            IsStructUnionScope() const           { return _struct_union_scope ; }

    unsigned            IsEmptyScope() const                 { return _empty_scope ; }
    void                ResetEmptyScope()                    { _empty_scope = 0 ; }
    void                SetAsEmptyScope()                    { _empty_scope = 1 ; }
    unsigned            CanBeConsideredEmpty() const ;

    // Parse tree copy routine :
    VeriScope *         Copy(VeriMapForCopy &id_map_table) const ; // Copy scope

    void                AddImportItem(VeriScope *package_scope, const char *id, const VeriTreeNode *from) ; // Make argument specific 'id' visible to 'this' scope directly

    // VIPER #7986 : Copy the initial-value/port-connection-list/dimension of ids
    void                UpdateScope(const VeriScope *scope, VeriMapForCopy &id_map_table) const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriScope(const VeriScope &scope, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriScope(SaveRestore &save_restore) ;

    ~VeriScope(); // Delete a scope level
private:
    // Prevent compiler from defining the following
    VeriScope(const VeriScope &) ;            // Purposely leave unimplemented
    VeriScope& operator=(const VeriScope &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    unsigned            UndeclareExplicitImport(const VeriIdDef *id) ;
    // VIPER #6919 : Check for multiple import
    VeriIdDef *         FindImplicitlyImportedErrorCond(const char *name, const VeriTreeNode *from = 0) ; // Find one object by name, traverse upward if not found local. Optionally give a treenode from which errors can be reported

    VeriIdDef *         FindInUnnamedChildScopes(const char *name) const ;     // Find 'name' in child scopes having no owner (VIPER #3327)
    VeriIdDef *         FindLabelId(const char *name) const ;                  // VIPER #4788 & #4866: Find label (block) id with the given name, block ids can be declared in the upper scope of an empty scope

    // VIPER #8339: Routines to identify the scope as sequential block scope and loop scope
    void                SetSeqBlockScope(unsigned seq_block_scope)    { _seq_block_scope = seq_block_scope ; }
    unsigned            IsSeqBlockScope() const                       { return _seq_block_scope ; }
    void                SetLoopScope(unsigned loop_scope)             { _loop_scope = loop_scope ; }
    unsigned            IsLoopScope() const                           { return _loop_scope ; }

    // VIPER #4830: Verific internal routine to identify the scope as parallel block scope:
    void                SetParBlockScope()                   { _par_block_scope = 1 ; }
    unsigned            IsParBlockScope() const              { return _par_block_scope ; }
    void                UpdateScopeBackPointers(const VeriScope *old_scope, VeriMapForCopy &id_map_table) ;

    VeriIdDef *         FindDownwards(const char *name, VeriScope **s, unsigned find_all_children) ;
    void                CreateOrderedPkgList(Set *ordered_pkgs, Set *done) const ; // VIPER #7508

// Persistence (Binary restore via SaveRestore class)
    void                Save(SaveRestore &save_restore) const ;
    unsigned            RegisterIdDefs(SaveRestore &save_restore) const ; // Register the id-defs
    unsigned            RegisterDependencies(SaveRestore &save_restore) const ; // Regiter Dependencies
    void                SaveUsingSet(SaveRestore &save_restore) const ;
    void                RestoreUsingSet(SaveRestore &save_restore) ;
    unsigned long       CalculateSaveRestoreSignature() const ;
    void                AddExportItem(const VeriScope *package_scope, const char *id, const VeriTreeNode *from) ; // VIPER #7232 : Export construct handling
    void                CheckExportedItems() ; // VIPER #7232 : Check exported items
    VeriIdDef *         FindExported(const char *name) const ; // VIPER #7232 : Find in exported items

    // Local accessor methods only for Verific internal use:
    const Map *         GetImportedScopes() const       { return _imported_scopes ; }
    const Map *         GetExportedScopes() const       { return _exported_scopes ; }
    unsigned            IsStandardPackagesUsed() const  { return _is_std_pkg_used ; }

public:
    // VIPER #6434 : APIs to hash constraints for struct/union elements
    void                SetStructConstraints(Map *st_constraints) ;
    Map                *GetStructConstraints() const { return _struct_constraints ; }
    void                AddElementConstraints(const VeriTreeNode *node, const Array *constraints) ;
    Array              *GetElementConstraints(const VeriTreeNode *node) ;
    void                DeleteStructConstraints() ;
    void                SetStructConstraintsNull() ; // call SetStructConstraints(0) from this scope and all children scopes

private:
    VeriScope  *_upper ;      // Upper (directly visible) scope level
    VeriIdDef  *_owner ;      // Identifier that created the scope
    Map        *_this_scope ; // The present-level scope table

    // (External) dependencies : links to VeriScope's that this scope depends on, and scopes that depend on me. (via selected names/use clauses).
    Set        *_using ;      // Set of VeriScope* that this scope depends on. (external scopes called within this scope)
    Set        *_used_by ;    // Set of VeriScope* that depend on 'this' scope. (scopes that 'use' this scope).
    Set        *_children ;   // Set of VeriScope* for which this scope is the 'upper' scope (children scopes).
    Map        *_imported_scopes ; // Map of imported scopes vs Set of char* (identifier name)
    unsigned    _empty_scope:1 ; // Flag to indicate that this is unnamed and empty scope. It should be transparent
    unsigned    _generate_scope:1 ;  // VIPER #4987: Flag to indicate this this scope belongs to some generate construct
    unsigned    _struct_union_scope:1 ;  // VIPER #6980: Flag to indicate this this scope belongs to struct union
    unsigned    _seq_block_scope:1 ; // VIPER #8339: Flag to indicate whether this scope pertains to sequential block.
    unsigned    _loop_scope:1 ; // VIPER #8339: Flag to indicate whether this scope pertains to loop scope.
    unsigned    _is_std_pkg_used:1 ; // VIPER #4957 : Flag to indicate whether any id of package std is used in this scope
    unsigned    _par_block_scope:1 ; // VIPER #4830: Flag to indicate whether this scope pertains to fork ... join (VeriParBlock).
    unsigned    _exported_subprog_scope:1 ; // Flag to indicate that it is the scope of exported subprogram
    VeriScope  *_extended_scope ; // VIPER #3901 : Extension of scope for inline constraint
    VeriScope  *_base_class_scope ; // VIPER #4954: Scope of the base class of a class scope
    Map        *_struct_constraints ; // VIPER #6434 : Map of VeriStructUnion* vs Array of VeriConstraints (for struct elements)
    Map        *_exported_scopes ; // Map of exported scopes vs Map of char* (identifier name) vs linefile of export (VIPER #7232)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriScope

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_SCOPE_H_
