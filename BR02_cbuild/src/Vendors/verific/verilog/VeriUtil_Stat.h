/*
 *
 * [ File Version : 1.61 - 2014/01/29 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_UTIL_H_
#define _VERIFIC_VERI_UTIL_H_

#include "VerificSystem.h"
#include "VeriCompileFlags.h"
#include "Map.h"
#include "VeriModuleItem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// The folloing class and its static methods are a general utility
// methods, mostly used in VeriExprEvaluate.cpp. While constant
// expression evaluation, all the calculations are done in string
// domain. So these methods proved very handfull there.

//Forward Declarations
class ValueTable;
class VeriBaseValue;
class VeriIdDef;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ConstStrEvaluation
{
public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Static routines for expression evaluation in string domain
    static char *       VeriStringToBinary(char *instr, unsigned *pSigned, int context_size = -1) ; // Convert argument specific string to binary in char* form

    static char *       GetPaddedString(const char *str, unsigned size, unsigned bSign) ; // Returns padded string of supplied size.

    static char *       BinToBin(const char *instr) ;       // Converts a string representing a binary number to another string.
    static char *       DecToBin(const char *s) ;           // Converts binary to decimal.
    static char *       OctToBin(const char *s) ;           // Converts Octal to binary.
    static char *       HexToBin(const char *s) ;           // Converts Hex to binary.
    static char *       AsciiToBin(const char *asciistr, unsigned len=0) ;  // Converts ascii string to binary.
    static char *       IntToBin(int num) ;                 // Converts integer to binary string.
    static char *       BinToAscii(const char *str) ;       // Converts binary to ascii string.
    static int          BinToInt(const char *str, unsigned sign = 0) ;         // Converts binary to integer number.

    static char *       StrAdd(const char *left, const char *right, char cin) ; // Adds two strings and returns the result taking care of carray in
    static char *       StrInvert(const char *str) ;                        // Inverts a string.
    static char *       StrIncr(const char *str) ;                          // Increments the supplied string by one.
    static char *       Str2sComp(const char *str) ;                        // Returns 2's complement of a string representing a number.
    static char *       StrSub(const char *left, const char *right, char cin) ; // Subtracts two strings.
    static char *       StrShift(const char *str, int left, char padd) ;    // Shifts a string by one unit
    static char *       StrMultiply(const char *left, const char *right) ;  // Mutiplies two strings and returns the product.
    static char *       StrSignedMultiply(const char* left, const char* right) ; // Multiplies two signed strings and returns the produce.
    static char *       StrDivision(const char *left, const char *right, unsigned sign) ;  // Divide two strings
    static char *       StrReductOR(const char *str) ;                      // Returns reduction OR of the 'str'.
    static char *       StrReductAND(const char *str) ;                     // Returns reduction AND of the 'str'.
    static char *       StrReductXOR(const char *str) ;                     // Returns reduction XOR of the 'str'.
    static char *       StrLogicalNOT(const char *str) ;                    // Returns logical NOT of the 'str'.
    static char *       StrLogicalOR(const char *left, const char *right) ; // Returns logical OR of 'left' and 'right'.
    static char *       StrLogicalAND(const char *left, const char *right) ; // Returns logical AND of 'left' and 'right'.

    static char *       StrLT(const char *left, const char *right, unsigned sign) ;     // Computes less-than operator considering the 'sign'.
    static char *       StrGT(const char *left, const char *right, unsigned sign) ;     // Computes greater-than operator considering the 'sign'.
    static char *       StrGTE(const char *left, const char *right, unsigned sign) ;    // Computes greater-than-equal operator considering the 'sign'.
    static char *       StrLTE(const char *left, const char *right, unsigned sign) ;    // Computes less-than-equal operator considering the 'sign'.
    static char *       StrEQ(const char *left, const char *right, unsigned is_case) ;  // Computes case/logical equality opearator
    static char *       StrNEQ(const char *left, const char *right, unsigned is_case) ; // Computes case/logical not-equality opearator
    static char *       StrExp(const char *left, const char *right, unsigned l_sign, unsigned r_sign) ; //  Computes exponent operator
    static char *       StrMod(const char *left, const char *right, unsigned sign) ;    // Computes modulus operator

    static char *       StrBitXnor(const char *str1, const char *str2) ;    // Computes bitwise xnor
    static char *       StrBitXor(const char *str1, const char *str2) ;     // Computes bitwise xor
    static char *       StrBitOr(const char *str1, const char *str2) ;      // Computes bitwise or
    static char *       StrBitAnd(const char *str1, const char *str2) ;     // Computes bitwise and

    static char *       StrShift(const char *str1, int left, unsigned shiftby, char padd) ; // Left/right shift a string by 'shiftby'  unit

    static unsigned     StrIsZero(const char *str) ;     // Returns 1 if the string(binary) is all 0.
    static unsigned     StrIsReductOR(const char *str) ; // Returns 1 if any one character of the string is '1'.
    static unsigned     StrHasXZ(const char *str) ;      // Returns 1 if the 'str' has 'x' or 'z'.
    static unsigned     StrHasOnlyZ(const char *str);    // Returns 1 if the 'str' has 'z', but not 'x'

    static char *       VeriGetXOfWidth(unsigned size) ; // Returns a string of character 'x' of width 'size'.

    static char *       StrConditionalCombine(const char *str1, const char *str2) ;    // Combines bitwise for conditional operator (?:)

    // One bit operation character
    static char         CharAnd(char a, char b) ;
    static char         CharOr(char a, char b) ;
    static char         CharInvert(char a) ;
    static char         CharXor(char a, char b) ;

    // Calculates 'n' raised to the power of 2
    static int          Pow2(int n) ;

    // Convert between binary string and verific_uint64/verific_int64
    static verific_uint64  BinToUnsigned64(const char *bin_str) ;
    static char *          Unsigned64ToBin(verific_uint64 val) ;
    static verific_int64   BinToInt64(const char *bin_str, unsigned sign = 0) ;
    static double          BinToUnsignedDouble(const char *bin_str, unsigned sign) ;
    static char *          Int64ToBin(verific_int64 val) ;

    // VIPER #3076: (short) Real to Bits and Bits to (short) Real routines:
    static char *       RealToBits(double value) ;
    static double       BitsToReal(const char *value) ;
    static char *       ShortRealToBits(float value) ;
    static float        BitsToShortReal(const char *value) ;

    // VIPER #6548: Support for case/casex/casez evaluation (comparison):
    static unsigned     StrCaseEQ(const char *case_cond_str, const char *item_cond_str, unsigned case_style) ;
} ; // class ConstStrEvaluation

/* -------------------------------------------------------------- */

/*
   VIPER #4128 : If elements defined within structure or union are referred in
   assignment pattern/tagged union before the actual definition of structure/union
   and the module containing those is copied, reference of members will refer to
   previous members instead of copied one. So relink those inspecting the MapForCopy
   typedef AB;
   AB ab = '{str : "DF"};
   typedef struct{ reg[1:0][7:0] str ; } AB;
*/
class VFC_DLL_PORT ReResolveIds : public VeriVisitor
{
public :
    explicit ReResolveIds(VeriMapForCopy *copy_obj) ;
    ~ReResolveIds() ;

private:
    // Prevent compiler from defining the following
    ReResolveIds() ;                                // Purposely leave unimplemented
    ReResolveIds(const ReResolveIds &) ;            // Purposely leave unimplemented
    ReResolveIds& operator=(const ReResolveIds &) ; // Purposely leave unimplemented

public:
    virtual void VERI_VISIT(VeriIdRef, id_ref) ;

private:
    VeriMapForCopy *_copy_obj ;
} ; // ReResolveIds

/* -------------------------------------------------------------- */
#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_UTIL_H_
