/*
 *
 * [ File Version : 1.54 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_VARUSAGE_H_
#define _VERIFIC_VERI_VARUSAGE_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VeriModuleItem;
class VeriTreeNode;
class VeriIdDef;
class VeriName ;
class BitArray;
class Array;
class Map;
class Set;

/* -------------------------------------------------------------------------- */

/*
   Keeps a running record of all Signals and Variables that have
   been encountered as well as their current assignment/usage
   status
*/
class VFC_DLL_PORT VeriVarUsageInfo
{
public:
    explicit VeriVarUsageInfo(unsigned in_initial_clause=0) :
       _indices(0),
        _rand_randc_vars(0), _is_fully_assigned(0), _is_ever_assigned(0), _is_ever_used(0),
       _in_initial_clause(in_initial_clause), _num_edges(0), _in_functon(0), _in_conditional_generate(0), _in_always_initial(0)
       , _in_temporal_expr(0),
         _in_operand(0),
         _in_not_operand(0),
         _contains_rand(0),
         _contains_randc(0),
         _in_constraint_decl(0),
         _is_special_always(0)
        , _recursion_checker(0)
    {}

    explicit VeriVarUsageInfo(Map* id_table, unsigned in_initial_clause=0):
       _indices(id_table),
        _rand_randc_vars(0), _is_fully_assigned(0), _is_ever_assigned(0), _is_ever_used(0),
       _in_initial_clause(in_initial_clause), _num_edges(0), _in_functon(0), _in_conditional_generate(0), _in_always_initial(0)
        ,_in_temporal_expr(0),
         _in_operand(0),
         _in_not_operand(0),
         _contains_rand(0),
         _contains_randc(0),
         _in_constraint_decl(0),
         _is_special_always(0)
        , _recursion_checker(0)
    {}

    ~VeriVarUsageInfo() ;

private:
    // Prevent the compiler from implementing the following
    VeriVarUsageInfo(const VeriVarUsageInfo &ref);            // Purposely leave unimplemented
    VeriVarUsageInfo& operator=(const VeriVarUsageInfo &rhs); // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    VeriVarUsageInfo *  Copy() ; // creates new storage which is owned by the caller

    // Variable/signal related functions:
    unsigned            AddVariable(VeriIdDef* id) ;

    // Go through the Usage/Assignment arrays and mark each var/sig as being concurrently
    // used/assigned.  This is used a a post-process for every concurrent statement
    void                UpdateConcurrentVarStatus() const ;

    // combine usage/assignment info across multiple branches of IF or CASE regions
    // VIPER #3960 : Extended this combination for operands of seq/prop operators
    // For 'and' and 'intersect' we need to xor full assigned arrays for both operands.
    void                Combine(Array& array_of_info, unsigned xor_full_assigned = 0) ;

    // Methods related to the usage/assignment arrays
    void                SetFullyAssigned(unsigned indx) ;
    void                SetEverAssigned(unsigned indx) ;
    void                SetEverUsed(unsigned indx) ;

    unsigned            GetFullyAssigned(unsigned indx) const ;
    unsigned            GetEverAssigned(unsigned indx) const ;
    unsigned            GetEverUsed(unsigned indx) const ;

    void                ResetFullyAssigned(unsigned indx) ;
    void                ResetEverUsed(unsigned indx) ;

    // Flag to check if we are in an 'initial' clause (or in an 'always' clause') :
    unsigned            GetInInitialClause() const { return _in_initial_clause ; }

    // Flag to check if we are in an clocked area or not. Need number of edges that controls this area.
    unsigned            GetNumEdges() const { return _num_edges ; }
    void                SetNumEdges(unsigned num_edges) { _num_edges = num_edges ; }
    // Flag to check if we are in a 'function' :
    void                SetInFunction()       { _in_functon = 1 ; }
    unsigned            GetInFunction() const { return _in_functon ; }
    void                SetInConditionalGenerate()       { _in_conditional_generate = 1 ; }
    void                ResetInConditionalGenerate()     { _in_conditional_generate = 0 ; }
    unsigned            GetInConditionalGenerate() const { return _in_conditional_generate ; }
    unsigned            GetInAlwaysInitial() const       { return _in_always_initial ; }
    void                SetInAlwaysInitial(unsigned s=1) { _in_always_initial = s ; }
    void                SetInSpecialAlways()             { _is_special_always = 1 ; }
    unsigned            GetInSpecialAlways() const       { return _is_special_always ; }
    // For semantic checks of concurrent assertion :
    unsigned            GetInTemporalExpr() const        { return _in_temporal_expr ; }
    void                SetInTemporalExpr(unsigned s=1)  { _in_temporal_expr = s ; }
    unsigned            GetInOperand() const             { return _in_operand ; }
    void                SetInOperand()                   { _in_operand = 1 ; }
    unsigned            GetInNotOperand() const          { return _in_not_operand ; }
    void                SetInNotOperand()                { _in_not_operand = 1 ; }
    void                CombineForAssertionOp(unsigned oper_type, Array &info_arr) ;
    unsigned            DoesContainRand() const          { return _contains_rand ; }
    void                ContainsRand(unsigned reset = 1)                   { _contains_rand = reset ; }
    unsigned            DoesContainRandC() const         { return _contains_randc ; }
    void                ContainsRandC(unsigned reset = 1)                  { _contains_randc = reset ; }
    Map *               GetRandRandCVars() const         { return _rand_randc_vars ; }
    void                SetRandRandCVars(Map *rand_randc_vars)    { _rand_randc_vars = rand_randc_vars ; }
    void                SetInConstraintDecl(unsigned s=1) { _in_constraint_decl = s ; }
    unsigned            IsInConstraintDecl() const        { return _in_constraint_decl ; }

    unsigned            HasTaskIdInStack(VeriIdDef const *task_id) const ; // Viper #4060

    void                AddTaskIdInStack(VeriIdDef const *task_id) ; // Viper #4060

    void                RemoveTaskIdFromStack(VeriIdDef const *task_id) ; // Viper #4060

    unsigned            HasIdInStack(VeriIdDef const *id) const ; // Viper #6081

    void                AddIdInStack(VeriIdDef const *id) ; // Viper #6081

    void                RemoveIdFromStack(VeriIdDef const *id) ; // Viper #6081

    static void         InitializeWithinLoop() ;
    static void         PushWithinLoop() ;
    static void         PopWithinLoop() ;
    static unsigned     IsWithinLoop() ;

    static unsigned     InitializeVisitedSelectedNames() ;
    static void         DeleteVisitedSelectedNames() ;
    static void         AddVisitedSelectedName(const VeriName *name) ;
    static unsigned     IsVisitedSelectedName(const VeriName *name) ;

    static void         InitializeInSampledValueFunc() ;
    static void         PushInSampledValueFunc() ;
    static void         PopInSampledValueFunc() ;
    static unsigned     IsInSampledValueFunction() ;

public :
    // VIPER #4241 : Handling of assignment information from different areas
    void                SetGlobalAssignmentInfo(const VeriModuleItem *context) const ;
    static void         SetAssignedInfoForVars(VeriIdDef *id, unsigned assign_mode, const VeriTreeNode *context) ;
    static void         ResetAssignedInfo() ;
private :
    static Map * _assigned_ids_with_mode ; // VeriIdDef* vs unsigned long
    static Map * _assigned_ids_with_context ; // VeriIdDef * vs unsigned long (context)

private:
    Map         *_indices ;
    Map         *_rand_randc_vars ; // Map for VeriIdDef*->VERI_RAND/VERI_RANDC
    BitArray    *_is_fully_assigned ;
    BitArray    *_is_ever_assigned ;
    BitArray    *_is_ever_used ;
    unsigned     _in_initial_clause:1 ; // VIPER 3618 : need to know if we are in initial clause or not
    unsigned     _num_edges:10 ;    // VIPER 3603 : number of edges that control this area
    unsigned     _in_functon:1 ;    // VIPER #3101 : need to know if we are in function or not
    unsigned     _in_conditional_generate:1 ;    // VIPER #3908 : need to know if we are in
    unsigned     _in_always_initial:1 ;  // 7084: To check if an assertion is a procedural concurrent assertion
    // For semantic checks of concurrent assertion :
    unsigned     _in_temporal_expr:1 ;  // For SVA checks: need to know we are in concurrent assertion expression
    unsigned     _in_operand:1 ;        // For SVA checks: need to know we are in operand of binary/unary operator
    unsigned     _in_not_operand:1 ;    // For SVA checks: need to know we are in operand of property operator 'not'
    unsigned     _contains_rand:1 ;
    unsigned     _contains_randc:1 ;
    unsigned     _in_constraint_decl:1 ; // Flag to indicate constraint decl is processing
    unsigned     _is_special_always:1 ; // Viper 7627: to check timing controls in task in special always
    Set         *_recursion_checker ;
    static Set  *_visited_selected_names ;     // VIPER #6701
    static unsigned     _is_inside_loop ;      // VIPER #3736
    static unsigned     _in_sampled_value_fn ; // VIPER #7834 & #7082
} ; // class VeriVarUsageInfo

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_VARUSAGE_H_

