/*
 *
 * [ File Version : 1.366 - 2014/03/04 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h> // For strcmp & strstr

#include <stdio.h>  // For sprintf
#include <math.h>   // For fabs, pow

#include "VeriExpression.h"

#include "veri_file.h" // for GetTimeScale
#include "VeriBaseValue_Stat.h"
#include "VeriUtil_Stat.h"
#include "VeriCopy.h"
#include "veri_tokens.h"
#include "VeriMisc.h"
#include "VeriModuleItem.h"
#include "VeriModule.h"
#include "VeriConstVal.h"
#include "VeriStatement.h"
#include "VeriId.h"
#include "VeriScope.h"

#include "Set.h"
#include "Strings.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// DD: 12/23/2010: Increased this to 1000 from 150 to match RTL side:
// DD: 04/19/2012: Decreased to 900 to match RTL side (VIPER #7125):
// VIPER #7568: Default value is now set while adding runtime flag for recursion
//#define RECURSION_LIMIT 900

/* Get bit values in VeriConst's streams */
#define GET_BIT(S,B) ((S)?(((S)[(B)/8]>>((B)%8))&1):0)
// This is a local static function which is used both in VeriConcat and VeriMultiConcat. It evaluates the
// Array of expressions of both VeriConcat and VeriMultiConcat, and returns the value.
VeriBaseValue *VeriConcat::StaticEvaluateConcatExprArr(int context_size, const Array *exprs, ValueTable *n, unsigned expect_nonconst)
{
    if (!exprs) return 0 ;

    unsigned ret_size = (unsigned)ABS(context_size) ; // May be zero, for multi-concat!
    unsigned status = 1 ;
    Array val_array(exprs->Size()) ;
    VeriBaseValue *e_val = 0 ;
    char *str = 0 ;
    unsigned e_sign = 0 ;
    unsigned all_string = 1 ;
    unsigned all_mul_concat = 1 ;
    unsigned has_ele = 0 ;

    unsigned i ;
    VeriExpression *ele = 0 ;
    FOREACH_ARRAY_ITEM(exprs, i, ele) {
        // Evaluate each expression.
        // VIPER #7902 : Call separate routine to evaluate concat item to identity
        // that
        e_val = ele ? ele->StaticEvaluateConcatElement(0, n, 0, expect_nonconst): 0 ;
        if (ele && !ele->IsMultiConcat()) all_mul_concat = 0 ; // VIPER #7902
        if (ele) has_ele = 1 ;
        if (!e_val) {
            // VIPER #7902 : Allow null value inside concat, element may be
            // multi-concat with 0 replication multipler
            // 'ele' is not constant, ERROR
            //concat->Error("%s is not a constant", "concat expression") ;
            if (ele && ele->IsMultiConcat()) {
                // VIPER #8042 : Evaluate repeat expression to check whether
                // that is 0 or not. Ignore this multi-concat only when has 0
                // as replication multipler
                VeriExpression *repeat_expr = ele->GetRepeat() ;
                VeriBaseValue *rep = repeat_expr ? repeat_expr->StaticEvaluateInternal(0, n, 0, 0): 0 ;
                unsigned to_continue = 0 ;
                if (rep && (rep->GetIntegerValue()==0)) {
                    to_continue = 1 ;
                }
                delete rep ;
                if (to_continue) continue ;
                all_mul_concat = 0 ; // This is not multi-concat with 0 replication multipler
            }
            status = 0 ;
            break ;
        }
        val_array.InsertLast(e_val) ;
        if (e_val->Type() != ASCIISTR) all_string = 0 ;
    }

    // VIPER #7902: If all elements of concatenation are multi-concat with
    // 0 as replication multipler, produce warning
    if (all_mul_concat && has_ele && (val_array.Size() == 0)) {
        if (ele) ele->Warning("illegal to use all zero replications in concat") ;
    }
    Array str_array(val_array.Size()) ;
    if (status) {
        FOREACH_ARRAY_ITEM(&val_array, i, e_val) {
            str = (all_string) ? Strings::save(e_val->GetString()) : e_val->GetBinaryValue(&e_sign) ;
            if (!str) {
                // 'ele' is possibly a real, ERROR
                status = 0 ;
                break ;
            }
            str_array.InsertLast(str) ;
        }
    }

    VeriBaseValue *val = 0 ;
    if (status) {
        if (all_string) {
            val = VeriBaseValue::ConcatAsciiStrings(&val_array, ret_size) ;
        } else {
            char *val_str = Strings::save("") ;
            // Traverse the array, and perform strcat.
            FOREACH_ARRAY_ITEM(&str_array, i, str) {
                char *tmp = val_str ;
                val_str = Strings::save(val_str, str) ;
                Strings::free(tmp) ;
            }
            unsigned len = (unsigned)Strings::len(val_str) ;
            /*
            if (all_string) {
                // VIPER #3359 & #4755: If all concat items are strings, create a string value here:
                val = new VeriAsciiString(val_str, len) ;
                // VIPER #7397 : Adjust size of ascii string using Adjust API
                unsigned ret_str_size = ret_size ? ret_size/8 : 0 ;
                if (ret_str_size && (ret_str_size < len)) {
                    // Only truncate the string, do not extend the string by padding:
                    //char *temp = val_str ;
                    //val_str = ConstStrEvaluation::GetPaddedString(val_str, ret_size, 0) ;
                    //Strings::free(temp) ;
                    val = val->Adjust(ret_size, 0, 0, 0) ;
                }
            } else {
            */
            // if 'len' is not equal to the required expression size, perform string padding.
            if (ret_size && (len != ret_size)) {
                char *temp = val_str ;
                val_str = ConstStrEvaluation::GetPaddedString(val_str, ret_size, 0) ;
                Strings::free(temp) ;
                len = ret_size ;
            }
            char *prefix = Strings::itoa((int)len) ;
            char *x = Strings::save(prefix, "'b", val_str) ;
            // Create VeriBasedNumber, after evaluation.
            val = new VeriBasedNumber(x) ;
            Strings::free(x) ;
            Strings::free(prefix) ;

            Strings::free(val_str) ;
        }
    }

    FOREACH_ARRAY_ITEM(&str_array, i, str) Strings::free(str) ;
    FOREACH_ARRAY_ITEM(&val_array, i, e_val) delete e_val ;

    return val ;
}

VeriBaseValue *VeriConcat::StaticEvaluateUnpackedArrayConcat(VeriConstraint *target_constraint, ValueTable *val_table, unsigned expect_nonconst) const
{
    //if (!target_constraint) return 0 ;

    // VIPER #6008 : Support for unpacked array concatenation LRM SV-2009 10.10
    // Target can be fixed unpacked array, dynamic array or queue :
    // Unpacked array concatenation cannot contain replication,
    // defaulting and explicit typing
    // Every element shall represent one or many elements of the resulting array
    // value, interpreted as followings:
    // -- an item whose self-determined type is assignment-compatible with the
    //    element type of the target array shall represent a single element
    // -- an item whose self determined type is an array whose slowest varying
    //    dimension's element type is assignment compatible with the element type
    //    of target array shall represent as many elements as exist in that item
    // -- an item of any other type, or an item that has no self-determined type,
    //    shall be illegal except that the literal value null shall be legal if
    //    the target array's elements are of class type.
    Array *vals = new Array(_exprs ? _exprs->Size(): 2) ;
    unsigned i ;
    VeriExpression *item ;
    VeriBaseValue *val ;
    VeriConstraint *element_constraint = target_constraint ? target_constraint->ElementConstraint(): 0 ;
    unsigned all_mul_concat = 1 ;
    unsigned has_ele = 0 ;
    FOREACH_ARRAY_ITEM(_exprs, i, item) {
        if (!item) continue ;
        val = item->StaticEvaluateConcatElement(0 /* self determined size*/, val_table, 0, expect_nonconst) ;
        has_ele = 1 ;
        if (!item->IsMultiConcat()) all_mul_concat = 0 ; // VIPER #7902
        if (!val) {
            // VIPER #7902 : Allow null value inside concat, element may be
            // multi-concat with 0 replication multipler
            if (item->IsMultiConcat()) {
                // VIPER #8042 : Evaluate repeat expression to check whether
                // that is 0 or not. Ignore this multi-concat only when has 0
                // as replication multipler
                VeriExpression *repeat_expr = item->GetRepeat() ;
                VeriBaseValue *rep = repeat_expr ? repeat_expr->StaticEvaluateInternal(0, val_table, 0, 0): 0 ;
                unsigned to_continue = 0 ;
                if (rep && (rep->GetIntegerValue()==0)) {
                    to_continue = 1 ;
                }
                delete rep ;
                if (to_continue) continue ;
                all_mul_concat = 0 ; // Not multi-concat with 0 replication multipler
            }
            FOREACH_ARRAY_ITEM(vals, i, val) delete val ;
            delete vals ;
            return 0 ;
        }
        if ((val->Type() == ARRAY) && !(element_constraint && (element_constraint->IsArrayConstraint() || element_constraint->IsRecordConstraint()) && !element_constraint->IsPacked())) {
            Array *val_arr = val->TakeValues() ;
            vals->Append(val_arr) ;
            delete val_arr ;
            delete val ;
        } else {
            vals->InsertLast(val) ;
        }
    }
    // VIPER #7902: If all elements of concatenation are multi-concat with
    // 0 as replication multipler, produce warning
    if (all_mul_concat && has_ele && (vals->Size() == 0)) {
        Warning("illegal to use all zero replications in concat") ;
    }

    return new VeriArrayValue(vals, 0) ;
}
VeriBaseValue *VeriMultiConcat::StaticEvaluateUnpackedArrayConcat(VeriConstraint *target_constraint, ValueTable *val_table, unsigned expect_nonconst) const
{
    //if (!_repeat || !target_constraint) return 0 ;
    if (!_repeat ) return 0 ;

    VeriBaseValue *rep = _repeat->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
    if (!rep) return 0 ;
    int loop_count = rep->GetIntegerValue() ;
    delete rep ;
    if (loop_count <= 0) return 0 ;

    int i ;
    unsigned j ;
    VeriExpression *item ;
    VeriBaseValue *val = 0 ;
    VeriConstraint *element_constraint = target_constraint ? target_constraint->ElementConstraint(): 0 ;
    Array *values = new Array((unsigned)loop_count * (_exprs ? _exprs->Size(): 1)) ;
    for(i = 0; i < loop_count; i++) {
        FOREACH_ARRAY_ITEM(_exprs, j, item) {
            if (!item) continue ;
            val = item->StaticEvaluateInternal(0, val_table, element_constraint, expect_nonconst) ;
            if (!val) {
                FOREACH_ARRAY_ITEM(values, i, val) delete val ;
                delete values ;
                return 0 ;
            }
            if ((val->Type() == ARRAY) && !(element_constraint && (element_constraint->IsArrayConstraint() || element_constraint->IsRecordConstraint()) && !element_constraint->IsPacked())) {
                Array *val_arr = val->TakeValues() ;
                values->Append(val_arr) ;
                delete val_arr ;
                delete val ;
            } else {
                values->InsertLast(val) ;
            }
        }
    }
    // Pass a flag to VeriArrayValue class depeding on whether the expression is
    // multi concat or multi assignment pattern
    unsigned is_mult_concat_arr_val = 1 ; //!IsMultiAssignPattern() ;
    return new VeriArrayValue(values, is_mult_concat_arr_val) ;
}
///////////////////////////////////////////////////////////////////
//
//
// For the following methods, all the methods take context_size and ValueTable as arguments.
// ValueTable is a local class, it is required to evaluate a constant function. It has
// entries of VeriIdDef vs. VeriBaseValue. Initially all the function port identifiers,
// and locally defined identifiers are inserted in the class. Then after executing each
// statement, values for the identifiers are updated. At any point of time, if any global
// or non-constant identifiers are detected, the method produces an error.
// Evaluates a constant expression, and returns a VeriBaseValue. It takes 2 arguments :
// * context_size - context dependent size of the expression in which it is to be evaluated
// * ValueTable - A Map, used in sequential Verilog code (store identifier values).
//                Required for evaluation of constant functions. VeriBaseValue.h has details.

VeriBaseValue* VeriIdRef::StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_id) { // IdRef not yet resolved.
        // This may be a built-in method call like str.len() where str is a string type object!
        if (GetFuncToken(_name)) {
            // So, this is a method call without the parenthesis, try to evaluate the function.
            // For this create a temporary VeriFunctionCall object...
            VeriMapForCopy copy_tab ; // Map for copying 'this'
            VeriName *func_name = CopyName(copy_tab) ; // Copy myself, destructor of VeriFunctionCall will delete it.
            VeriFunctionCall built_in_method(func_name, 0 /* no argument */) ;
            // and call StaticEvaluateInternal() on this object and return the value, that will do the trick!
            // The automatic varibale built_in_method will be autmatically deleted and will delete the copied name.
            return (built_in_method.StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst)) ;
        }
        return 0 ;
    }

    if (_id->IsLetId()) { // This is let identifier reference, evaluate its expression
        VeriModuleItem *tree = _id->GetModuleItem() ;
        return (tree) ? tree->StaticElaborateFunction(0, n, expect_nonconst): 0 ;
    }

    // Check if it is a function call without any argument and parenthesis.
    if (_id->IsFunction()) {
        // This is a function call, first check if it has a corresponding value in the
        // value table, if yes, return that.
        // FIXME: recursive function call without parenthesis will not execute recursively,
        //   since we will return the value from the table if it already has one!
        if (n) {
            VeriBaseValue *prev_val = n->FetchValue(_id) ;
            if (prev_val) return prev_val->Copy() ; // Got it, return a copy of the value
        }

        // VIPER #2961: Do not try to evaluate this identifier reference as a function call.
        // We may be in trouble if this id-ref is used as an index in a indexed (memory) id:

        // VIPER #8011: Here we are referencing function call without argument(Function call without arg (no port, not within function itself)).
        // Check that the function call without argument is called within a function: if so we will not evaluate further, otherwise return 0.
        if (_id->CanCallWithoutArg() && _present_scope && _id != _present_scope->GetSubprogram()) { // we are not inside this function itself

            // So, this is a function call without any argument, we have to actually call the
            // function from here.  Create a VeriFunctionCall object without any argument,
            // call StaticEvaluateInternal() function on it and return the value returned by the StaticEvaluateInternal() function.
            VeriIdRef *func_call_ref = new VeriIdRef(_id) ;
            func_call_ref->SetLinefile(Linefile()) ;
            VeriFunctionCall func_call(func_call_ref, 0) ;

            // 'func_call' is an automatic variable - will be deleted automatically.
            // The dynamic VeriIdRef created will be deleted with the destructor of 'func_call'

            // Evaluate the function and return the value
            return func_call.StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst) ;
        }

        return 0 ; // VIPER #2961: return 0 instead to indicate failure
    }
    VeriDataType *dtype = _id->GetDataType() ;
    // Check if it is enum literal, then we will create enum value
    if (_id->IsParam() && dtype && dtype->GetEnums()) { // This is enum type variable/enum literal
        // FIXME : We have no direct API or anything to test whether it is enum literal/parameter of enum type
        // We will create enum value for only enum literals. So check whether it is
        // enum literal
        Map *ids = dtype->GetEnumLiterals() ;
        VeriIdDef *enum_id = (ids) ? (VeriIdDef*)ids->GetValue(_id->Name()): 0 ;
        if (enum_id == _id) return new VeriEnumVal(_id) ;
    }
    // Get sign/size of this IdRef
    // VIPER #7892 : This section is moved to 'ApplyContextOnValue' routine
    //unsigned ret_size = 0 ;
    //unsigned sign = 0 ;
    //if (!context_size) {
        //int size_sign = (int)StaticSizeSignInternal(n, target_constraint) ;
        //ret_size = GET_CONTEXT_SIZE(size_sign) ;
        //sign = GET_CONTEXT_SIGN(size_sign) ;
    //} else {
        //ret_size = GET_CONTEXT_SIZE(context_size) ;
        //sign = GET_CONTEXT_SIGN(context_size) ;
    //}

    // Get the size sign of this identifier reference:
    int id_size_sign = (int)_id->StaticSizeSignInternal(n, target_constraint) ;
    unsigned id_size = GET_CONTEXT_SIZE(id_size_sign) ;
    //unsigned id_sign = GET_CONTEXT_SIGN(id_size_sign) ;

    // Calculate the value of this IdRef
    VeriBaseValue *val = 0 ;
    if (_id->IsParam() && !_id->IsType()) { // Evaluate initial value only for value parameter
        VeriExpression *ini_val = _id->GetInitialValue() ;
        // VIPER #7597: Parameter with no initial value is legal
        if (!ini_val) return 0 ;
        //VERIFIC_ASSERT(ini_val) ;
        // Looks like this is not required and may yield incorrect result:
        //if (_id->IsArray()) {
        //    int msb = 0 ;
        //    int lsb = 0 ;
        //    id_size = _id->GetPackedWidth(&msb, &lsb) ;
        //}

        // VIPER #7132: Evaluate parameter initial value in context of max size of parameter
        // and value and with sign of the initial value itself:
        int val_size_sign = (int)ini_val->StaticSizeSignInternal(n, target_constraint) ;
        unsigned val_size = GET_CONTEXT_SIZE(val_size_sign) ;
        val_size = MAX(val_size, id_size) ;
        unsigned val_sign = GET_CONTEXT_SIGN(val_size_sign) ;
        val_size_sign = MAKE_CONTEXT_SIZE_SIGN(val_size, val_sign) ;

        // If initial value is signed but the signedness of the parameter is not specified
        // parameter should be treated as signed like in this case: parameter [3:0] p = 2'sb11 ;
        // VIPER #7132: None of the simulators treat p as signed in such case is prints value 15:
        // Not even parameter [1:0] p = 2'sb11 ; which prints 3. So, comment this line out now.
        //if (!id_sign && (ini_val->StaticSizeSignInternal(n) < 0)) id_sign = 1 ;

        // VIPER #3303 and similar issue #3320: Evaluate the initial value in the context of
        // size of the parameter identifier but consider sign extension of the value itself:
        // Update the size-sign since size and/or sign may have changed already above.
        // Since the value is already evaluated in the flow of static elaboration or is a constant
        // literal before, we can safely evaluate it in this size/sign for proper extension:
        // VIPER #7132: Correct context-size-sign is evaluated above as 'val_size_sign':
        //id_size_sign = MAKE_CONTEXT_SIZE_SIGN(id_size, id_sign) ;

        // If the identifier is a parameter identifier, its initial value must be a constant.
        VeriConstraint *constraint = _id->EvaluateConstraintInternal(n, 0, 0) ;
        val = ini_val->StaticEvaluateInternal(val_size_sign, n, constraint, expect_nonconst) ;
        delete constraint ;
        if (!val && dtype && (dtype->Type() == VERI_TYPE)) return 0 ; // ini_val may be any data type
    } else if ((_id->IsVar()) || (_id->IsPort()) || (_id->IsGenVar())) {
        // If the identifier is not a parameter identifier, it should have come while
        // evaluating a constant function. Then the identifier should have an entry
        // in the ValueTable, and the return value should be the entry against '_id' in the table.
        if (n) {
            val = n->FetchValue(_id) ;
            val = val ? val->Copy(): 0 ;
        }
        // If this is reference of formal argument of let construct, evaluate the actual
        if (_id->IsSequenceFormal()) val = _id->StaticEvaluateInternal(n, expect_nonconst) ;
    }

    return ApplyContextOnValue(val, _id, (verific_int64)context_size, n) ;
}
// VIPER #7892 : Routine to modify value 'val' according to the type of context
// i.e. 'id'. Here 'id' is mainly parameter. IEEE 1800-2012 section 23.10
VeriBaseValue *VeriExpression::ApplyContextOnValue(VeriBaseValue *val, const VeriIdDef *id, verific_int64 context_size, ValueTable *n) const
{
    // VIPER #3298: Adjust value to the context/id size for both parameter and non-parameters.
    // Previously we used to adjust values only for parameters, now we always adjust it

    if (!val || !id) return 0 ; // Do not assert, return 0
    if (val->Type() == ARRAY) {
        // VIPER #8037 : When evaluated value is array i.e. target is unpacked array or
        // structure, adjust elements following target constraint
        // Produce array element size mismatch warning in Relaxed-checking-mode for
        // parameters having no data type and unpacked dimensions
        if ((id->GetDataType() || id->GetDimensions()) && !val->IsConcatValue()) {
            VeriIdDef *non_const_id = const_cast<VeriIdDef*>(id) ; // Need to remove constant to call 'EvaluateConstraintInternal'
            VeriConstraint *id_constraint = non_const_id ? non_const_id->EvaluateConstraintInternal(0, 0, 0): 0 ;
            if (id_constraint) {
                VeriBaseValue *new_val = val->AdjustAndCheck(id_constraint, id, this) ;
                if (new_val) val = new_val ;
            }
            delete id_constraint ;
        }
        return val ;
    }

    VeriDataType *dtype = id->GetDataType() ;

    // IEEE 1800-2012 LRM section 23.10 : A value parameter with no type or
    // range specification shall default to the type and range of the final
    // override value assigned to the parameter. So no conversion is needed
    if (!dtype && id->IsParam()) return val ;

    unsigned ret_size = 0 ;
    unsigned sign = 0 ;
    if (!context_size) {
        int size_sign = (int)StaticSizeSignInternal(n, 0) ;
        ret_size = GET_CONTEXT_SIZE(size_sign) ;
        sign = GET_CONTEXT_SIGN(size_sign) ;
    } else {
        ret_size = GET_CONTEXT_SIZE(context_size) ;
        sign = GET_CONTEXT_SIGN(context_size) ;
    }

    // Get the size sign of this identifier reference:
    verific_int64 id_size_sign = id->StaticSizeSignInternal(n, 0) ;
    unsigned id_size = GET_CONTEXT_SIZE(id_size_sign) ;
    unsigned id_sign = GET_CONTEXT_SIGN(id_size_sign) ;

    unsigned id_type = id->Type() ;
    VeriDataType *data_type = id->GetDataType() ;
    if (data_type && data_type->NetType()) { // For net type variable get its data type
        data_type = data_type->GetDataType() ;
        id_type = data_type ? data_type->Type() : id->Type() ;
    }
    // VIPER #3005: Respect the context size/sign while evaluating the value:
    // If the value is of type string, we need to handle it specially:
    if (val->Type() == ASCIISTR) {
        if (id_type == VERI_STRINGTYPE) return val ;
        if (ret_size % 8) {
            // VIPER #4912: Cannot handle as string if it is not multiple of 8:
            char *str = ConstStrEvaluation::AsciiToBin(val->GetString(), val->GetStringSize()) ; // Convert to binary bit pattern
            char *new_str = ConstStrEvaluation::GetPaddedString(str, ret_size, 0 /* CHECKME: always unsigned? */) ;
            char *prefix = Strings::itoa((int)ret_size) ;
            char *val_str = Strings::save(prefix, "'b" /* CHECKME: always unsigned? */, new_str) ;
            delete val ;
            val = new VeriBasedNumber(val_str) ; // Create a based number
            Strings::free(val_str) ;
            Strings::free(prefix) ;
            Strings::free(new_str) ;
            Strings::free(str) ;
            return val ;
        }

        // Handle as string here:
        /*
        char *str = Strings::save(val->GetString()) ;
        ret_size = ret_size / 8 ; // for char (byte)
        // VIPER #4546: Adjustments for \0 in strings - increase size to accommodate the back-slash:
        char *tmp = str ;
        while ((tmp = strstr(tmp, "\\0"))) {
            ret_size++ ;
            tmp = tmp + 2 ;
        }
        unsigned len = Strings::len(str) ;
        // Truncate the value at starting side (MSB):
        char *new_str = (len > ret_size) ? str+(len-ret_size) : str ;
        delete val ;
        val = new VeriAsciiString(new_str) ;
        Strings::free(str) ;
        */
        // VIPER #7397: Resize ascii string using 'Adjust' API.
        val = val->Adjust(ret_size, 0, 0, 0) ;
        return val ;
    }
    // Keep the real value if the id does not have a data type:
    // VIPER #3344: Even if it has a data type but the data type does not have
    // a _type or _dimension treat it as if it had no data type at all:
    if ((!dtype || (!dtype->GetType() && !dtype->GetDimensions())) && (val->Type() == REAL)) return val ;
    // If 'id' is of type real, we must return a real value from here:
    // VIPER #3281: Treat realtime as real, so always return real value for it:
    if ((id_type == VERI_REAL) ||
        (id_type == VERI_SHORTREAL) || // VIPER #8421: Also check for shortreal type here
        (id_type == VERI_REALTIME)) {
        // Return the current value if it is of type real:
        if (val->Type() == REAL) return val ;
        // Otherwise create a real value and return:
        double real_val = val->GetRealValue() ;
        delete val ;
        return new VeriReal(real_val) ;
    }
    // VIPER #3244: Verilog 'time' data type is 64 bit unsigned integer:
    if (id_type == VERI_TIME) {
        // VIPER #7195: No need to do all these, just call Adjust:
        // Convert to integer value for time type identifier:
        //verific_uint64 time_val = val->Get64bitUnsigned() ;
        //delete val ;
        // VIPER #6704: Do not return from here, need to honour the context size/sign:
        //return new VeriBasedNumber(time_val) ;
        //val = new VeriBasedNumber(time_val) ;
        // VIPER #8202 : Use signedness of context to adjust it (IEEE P1800-2012 section 11.8.2)
        val = val->Adjust(VeriNode::GetSizeOfBuiltInType(VERI_TIME), sign /* unsigned */, 0, 0) ;
    }
    unsigned b_sign = 1 ;
    char *str = 0 ;
    if (val->Type() == REAL) {
        // For real values, take integer and then convert to string:
        // VIPER #8203 : Real value can be larger than 32 bits, handle that
        // Always use unsigned value to convert real to binary
        verific_uint64 real_to_int = val->Get64bitUnsigned() ;
        str = ConstStrEvaluation::Unsigned64ToBin(real_to_int) ;
    } else {
        str = val->GetBinaryValue(&b_sign) ;
    }
    if (!str) {
        delete val ;
        //Error("Non-constant value, evaluation failed\n") ;
        return 0 ;
    }

    if (id->IsGenVar() && val->HasXZ()) Error("invalid assignment value for genvar %s", id->Name()) ; // Viper #4386

    unsigned len = (unsigned)Strings::len(str) ;
    // Get proper value of param id giving precedence to range size
    if ((len != id_size) || (val->Type() == REAL) /* always adjust real values */) {
        char *tmp = str ;
        // VIPER #8203 : Always use sign of context for padding (IEEE P1800-2012 section 11.8.2)
        str = ConstStrEvaluation::GetPaddedString(str, id_size, sign) ;
        Strings::free(tmp) ;
        len = (unsigned)Strings::len(str) ;
        char *prefix = Strings::itoa((int)id_size) ;
        char *val_str = Strings::save(prefix, (id_sign) ? "'sb" : "'b", str) ;
        delete val ;
        val = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        Strings::free(prefix) ;
    }
    // Perform the nessary padding (size and sign, sign pad for only non-X/Z values).
    if ((len != ret_size) || (!val->HasXZ() && (b_sign != sign))) {
        char *tmp = str ;
        // VIPER #8203 : Always use sign of context for padding (IEEE P1800-2012 section 11.8.2
        str = ConstStrEvaluation::GetPaddedString(str, ret_size, sign) ;
        Strings::free(tmp) ;
        len = (unsigned)Strings::len(str) ;
        char *prefix = Strings::itoa((int)ret_size) ;
        char *val_str = Strings::save(prefix, (sign) ? "'sb" : "'b", str) ;
        delete val ;
        val = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        Strings::free(prefix) ;
    }
    Strings::free(str) ;

    return val ;
}

VeriBaseValue* VeriIndexedId::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_idx || !_id || !_prefix) return 0 ;

    // Evaluate the prefix expression
    VeriBaseValue *val = _prefix->StaticEvaluateInternal(0, val_table, target_constraint, expect_nonconst) ;
    if (!val) return 0 ;

    // VIPER #4872: Support indexing on string types:
    VeriDataType *id_data_type = _id->GetDataType() ;
    if (id_data_type && (id_data_type->GetType() == VERI_STRINGTYPE)) {
        VeriBaseValue *idx_val = _idx->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
        int index = (idx_val) ? idx_val->GetIntegerValue() : -1 ; // Default to -1
        delete idx_val ;
        const char *str = val->GetString() ; // It must be a ASCII string
        VeriBaseValue *result = 0 ;
        // Decide on the character to be returned against the given position and return its integer value:
        if (str && (index>=0) && (index<(int)Strings::len(str))) result = new VeriInteger(str[index]) ;
        delete val ;
        return result ;
    }

    VeriConstraint *constraint = _prefix->EvaluateConstraintInternal(val_table, 0) ;
    if (!constraint) constraint = _id->EvaluateConstraintInternal(val_table, 0, 0) ;

    Array indexes(1) ;
    indexes.InsertLast(_idx) ;
    VeriBaseValue *ret_val = StaticEvaluateIndexed(val, constraint, &indexes, val_table, expect_nonconst) ;
    delete val ;
    delete constraint ;
    if (ret_val && (ret_val->Type() == ARRAY)) return ret_val ;
    int node_size = (int)StaticSizeSignInternal(val_table, target_constraint) ;
    unsigned size = context_size ? (unsigned)ABS(context_size): (unsigned)ABS(node_size) ;
    unsigned sign = context_size ? (context_size < 0) : (node_size < 0);
    // Adjust it to context size/sign
    ret_val = ret_val ? ret_val->Adjust(size, sign, 0, 0): 0 ;

    return ret_val ;
}
VeriBaseValue* VeriName::StaticEvaluateIndexed(VeriBaseValue *prefix_value, VeriConstraint *constraint, Array *indexes, ValueTable *table, unsigned expect_nonconst) const
{
    VeriConstraint *range ;
    int left_bound = 0, right_bound = 0 ;
    unsigned value_size = 0, lsb_bit_offset = 0 ;
    unsigned elem_size = constraint ? constraint->NumOfBits(): 0 ;
    VeriConstraint *runner = constraint ;
    VeriExpression *index = 0 ;
    while (indexes->Size()) {
        index = (VeriExpression*)indexes->RemoveLast() ;
        if (!index) return 0 ;
        // Pick up the dimension into which we are indexing :
        range = runner ? runner->IndexConstraint(): 0 ; // dimension of the element
        // Get the range bounds. Inherit from the range, of size-1:0 if this is a scalar :
        left_bound = (range) ? range->Left() : (int)elem_size-1 ;
        right_bound = (range) ? range->Right() : 0 ;

        // Check how large the size of one element is. If this is a scalar, element size will always be 1 (or else we did calculations incorrectly). CHECK HERE
        elem_size = elem_size / (unsigned)((left_bound>right_bound) ? (left_bound-right_bound)+1 : (right_bound-left_bound)+1) ;
        runner = (runner) ? runner->ElementConstraint(): 0 ;

        // If no index constraint exists and bounds are 0, produce warning
        if (!left_bound && !right_bound && !range) { // ie. if we have a declaration like bit x ; and we try to access x[1]
            Warning("cannot index into non-array %s", GetName()) ;
            return 0 ;
        }
        int left_idx = 0, right_idx = 0 ;
        unsigned elements_in_index = 0 ;
        unsigned always_out_of_bounds = 0 ;
        if (index->IsRange()) {
            // VIPER #2482, #3511: Single evaluation is handled within GetWidth:
            elements_in_index = index->GetWidth(&left_idx, &right_idx, table) ;
            if (!elements_in_index) return 0 ;
        } else {
            VeriBaseValue *v = index->StaticEvaluateInternal(0, table, 0, expect_nonconst) ;
            if (!v) return 0 ; // VIPER #4478 : Return null for non-constant index
            // VIPER #2482, #3511: Insert the evaluated index value into the table:
            if (table) table->InsertEvaluatedIndexValue(index, v) ;
            left_idx = v->GetIntegerValue() ;
            right_idx = left_idx ;
            delete v ;
        }
        // Check bounds :
        if ((left_bound>right_bound && left_idx<right_idx) ||
            (left_bound<right_bound && left_idx>right_idx)) {
            if (!index->GetPartSelectToken()) {
                Error("index is always out of bounds for array %s", GetName()) ;
                always_out_of_bounds = 1 ; // VIPER #4543 set always out of bounds
            }
            // Swap the bounds.
            // This can happen legally for +:/-: part-selects, or if somebody suppressed the error above.
            int tmp = left_idx ;
            left_idx = right_idx ;
            right_idx = tmp ;
        }
        // Return from function if both the indexes are out of range
        if (CHECK_INDEX(left_idx, left_bound, right_bound) &&
                CHECK_INDEX(right_idx, left_bound, right_bound)) {
            Warning("index is always out of bounds for array %s", GetName()) ;
            always_out_of_bounds = 1 ; // VIPER #4543 set always out of bounds
            //return 0 ;
        }
        // Check if the accessed element are out of range or not. If so send a warning.
        if (CHECK_INDEX(left_idx, left_bound, right_bound)) {
            Warning("index %d is out of range [%d:%d] for %s", left_idx, left_bound, right_bound, GetName()) ;
        }
        // Check if the accessed element are out of range or not. If so send a warning.
        if (CHECK_INDEX(right_idx, left_bound, right_bound)) {
            Warning("index %d is out of range [%d:%d] for %s", right_idx, left_bound,  right_bound, GetName()) ;
        }
        if (indexes->Size() == 0) { // Last index
            unsigned offset = 0 ;
            if (index->IsRange()) {
                if (left_bound > right_bound) {
                    if (right_idx >= right_bound) {
                        offset = (unsigned)(right_idx - right_bound) ;
                    } else {
                        int x = right_bound - right_idx ;
                        x = (int)elements_in_index - x ;
                        // VIPER #4543: Check that we are not casting a -ve number to unsigned:
                        if (x >= 0) elements_in_index = (unsigned)x ;
                    }
                } else {
                    if (right_bound >= right_idx) {
                        offset = (unsigned)(right_bound - right_idx) ;
                    } else {
                        int x = right_idx - right_bound ;
                        // VIPER #4543: Check that we are not casting a -ve number to unsigned:
                        x = (int)elements_in_index - x ;
                        if (x >= 0) elements_in_index = (unsigned)x ;
                    }
                }

                // VIPER #4543: If it is always out-of-bounds, set offset to size so that all x's are returned for out-of-bounds index:
                if (always_out_of_bounds) offset = (unsigned)((left_bound>right_bound) ? (left_bound-right_bound)+1 : (right_bound-left_bound)+1) ;

                // offset in bits is multplied by element size, and added to existing lsb_offset :
                lsb_bit_offset = lsb_bit_offset + offset*elem_size ;
                value_size = elem_size*elements_in_index ;
            } else {
                offset = (unsigned)((left_bound>right_bound) ? left_idx - right_bound: right_bound - left_idx) ;
                lsb_bit_offset = lsb_bit_offset + elem_size*offset ;
                value_size = elem_size ;
            }
            // VIPER #5742 : If prefix_value is structure value, it is selection on structure
            // So convert the structure value to vector value and extract required bits
            if (prefix_value->IsStructValue()) {
                VeriBaseValue *copied_val = prefix_value->Copy() ;
                VeriBaseValue *flat_value = copied_val ? copied_val->ToBasedNum(0): 0 ;
                VeriBaseValue *ret_val = (flat_value) ? flat_value->Evaluate(value_size, lsb_bit_offset) : 0 ;
                delete flat_value ;
                return ret_val ;
            } else if (prefix_value->Type() == ARRAY) {
                if (right_idx == left_idx) {
                    int pos = (left_bound > right_bound) ? (left_bound-left_idx) : (left_idx -left_bound) ;
                    VeriBaseValue *ret_val = (pos >= 0) ? prefix_value->GetValueAt((unsigned)pos): 0 ;
                    return (ret_val) ? ret_val->Copy(): 0 ;
                } else {
                    Array *val_arr = new Array(value_size) ;
                    unsigned i ;
                    int index1 = 0 ;
                    VeriBaseValue *v ;
                    for (i=0; i < elements_in_index; i++) {
                        index1 = (left_bound > right_bound) ? ((left_bound-left_idx)+(int)i) : ((left_idx -left_bound)+(int)i) ;
                        if (index1 < 0) { // out of range selection, cannot assign anything
                            char *val_str = ConstStrEvaluation::VeriGetXOfWidth(elem_size) ;
                            char *prefix = Strings::itoa((int)elem_size) ;
                            char *s = Strings::save(prefix, "'b", val_str) ;
                            v = new VeriBasedNumber(s) ;
                            Strings::free(val_str) ;
                            Strings::free(prefix) ;
                            Strings::free(s) ;
                        } else {
                            v = prefix_value->GetValueAt((unsigned)index1) ;
                            v = v ? v->Copy(): 0 ;
                        }
                        val_arr->InsertLast(v) ;
                    }
                    return new VeriArrayValue(val_arr, 0) ;
                }
            }
            return prefix_value->Evaluate(value_size, lsb_bit_offset) ;
        } else {
            if (index->IsRange()) {
                index->Warning("range expressions not allowed in hierarchical pathnames") ;
                right_idx = left_idx ; // Only take the left range
            }
            if (prefix_value->Type() == ARRAY) {
                int pos = (left_bound > right_bound) ? (left_bound-left_idx) : (left_idx -left_bound) ;
                if (pos >= 0) prefix_value = prefix_value->GetValueAt((unsigned)pos) ;
            } else {
                unsigned offset = (unsigned)((left_bound>right_bound) ? left_idx - right_bound: right_bound - left_idx) ;
                lsb_bit_offset = lsb_bit_offset + elem_size*offset ;
                value_size = elem_size ;
            }
        }
    }
    return 0 ;
}

VeriBaseValue* VeriIndexedMemoryId::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint * target_constraint, unsigned expect_nonconst) const
{
    if (!_indexes || !_id || !_prefix) return 0 ;

    // Evaluate the prefix expression
    VeriBaseValue *val = _prefix->StaticEvaluateInternal(0, val_table, target_constraint, expect_nonconst) ;
    if (!val) return 0 ;

    // VIPER #4872: Support indexing on string types:
    VeriDataType *id_data_type = _id->GetDataType() ;
    if (id_data_type && (id_data_type->GetType() == VERI_STRINGTYPE)) {
        // Process the first index to index the string:
        VeriExpression *idx = (_indexes->Size()) ? (VeriExpression *)_indexes->GetFirst() : 0 ;
        VeriBaseValue *idx_val = (idx) ? idx->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        int index = (idx_val) ? idx_val->GetIntegerValue() : -1 ; // Default to -1
        delete idx_val ;
        const char *str = val->GetString() ; // It must be a ASCII string
        unsigned result = 0 ;
        // Decide on the character to be returned against the given position and return its integer value:
        if (str && (index>=0) && (index<(int)Strings::len(str))) result = (unsigned)((int)str[index]) ;
        delete val ;
        // Now process the second index to index the character resulting from previous index:
        idx = (_indexes->Size() > 1) ? (VeriExpression *)_indexes->At(1) : 0 ;
        idx_val = (idx) ? idx->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        index = (idx_val) ? idx_val->GetIntegerValue() : 0 ; // Default to 0
        delete idx_val ;
        result = (result & (1<<index)) ? 1 : 0 ; // Take the bit value at this position
        if (_indexes->Size() > 2) result = 0 ; // Further indexing is not possible
        return (new VeriInteger((int)result)) ;
    }

    VeriConstraint *constraint = _prefix->EvaluateConstraintInternal(val_table, 0) ;
    if (!constraint) constraint = _id->EvaluateConstraintInternal(val_table, 0, 0) ;

    Array indexes(1) ;
    VeriExpression *idx ;
    unsigned i ;
    FOREACH_ARRAY_ITEM_BACK(_indexes, i, idx) indexes.InsertLast(idx) ;
    VeriBaseValue *ret_val = StaticEvaluateIndexed(val, constraint, &indexes, val_table, expect_nonconst) ;
    delete val ;
    delete constraint ;
    if (ret_val && (ret_val->Type() == ARRAY)) return ret_val ;
    int node_size = (int)StaticSizeSignInternal(val_table, target_constraint) ;
    unsigned size = context_size ? (unsigned)ABS(context_size): (unsigned)ABS(node_size) ;
    unsigned sign = context_size ? (context_size < 0) : (node_size < 0);
    // Adjust it to context size/sign
    ret_val = ret_val ? ret_val->Adjust(size, sign, 0, 0): 0 ;
    return ret_val ;
}

VeriBaseValue* VeriConcat::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    // The array of expressions are sent to the local static function as an argument, along
    // with the context size and sign. The function returns the value evaluating the expressions.
    int ret_size = (context_size) ? context_size : (int)StaticSizeSignInternal(val_table, target_constraint) ;
    (void) target_constraint ;
    if (target_constraint && (target_constraint->IsArrayConstraint() || target_constraint->IsRecordConstraint()) && !target_constraint->IsPacked()) {
        return StaticEvaluateUnpackedArrayConcat(target_constraint, val_table, expect_nonconst) ;
    }
    return StaticEvaluateConcatExprArr(ret_size, _exprs, val_table, expect_nonconst) ;
}

// VIPER #7902 : Routine to evaluate elements of concatenation
VeriBaseValue *VeriExpression::StaticEvaluateConcatElement(int context_size, ValueTable *val_table, VeriConstraint * target_constraint, unsigned expect_nonconst) const
{
    return StaticEvaluateInternal(context_size, val_table, target_constraint, expect_nonconst) ;
}
VeriBaseValue* VeriMultiConcat::StaticEvaluateConcatElement(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_repeat || !_exprs) return 0 ;

    (void) target_constraint ;
    if (target_constraint && (target_constraint->IsArrayConstraint() || target_constraint->IsRecordConstraint()) && !target_constraint->IsPacked()) {
        return StaticEvaluateUnpackedArrayConcat(target_constraint, val_table, expect_nonconst) ;
    }

    // The Replication operator is evaluated.
    VeriBaseValue *c_val = _repeat->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
    if (!c_val) return 0 ;

    int num = c_val->GetIntegerValue() ;
    // VIPER #7902 : Allow 0 as replication multipler if multi-concat is
    // an element of concatenation. Ignore this multi-concat
    if (num == 0) {
        delete c_val ;
        return 0 ;
    }
    return StaticEvaluateBody(c_val, context_size, val_table, target_constraint, expect_nonconst) ;
}
VeriBaseValue* VeriMultiConcat::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint * target_constraint, unsigned expect_nonconst) const
{
    if (!_repeat || !_exprs) return 0 ;

    (void) target_constraint ;
    if (target_constraint && (target_constraint->IsArrayConstraint() || target_constraint->IsRecordConstraint()) && !target_constraint->IsPacked()) {
        return StaticEvaluateUnpackedArrayConcat(target_constraint, val_table, expect_nonconst) ;
    }

    // The Replication operator is evaluated.
    VeriBaseValue *c_val = _repeat->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
    if (!c_val) return 0 ;

    return StaticEvaluateBody(c_val, context_size, val_table, target_constraint, expect_nonconst) ;
}

// VIPER #7902 : Routine to evaluate body of multi-concat
VeriBaseValue *VeriMultiConcat::StaticEvaluateBody(VeriBaseValue *c_val, int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!c_val) return 0 ;
    if (c_val->HasXZ()) Warning("repetition multiplier contains x") ;
    int num = c_val->GetIntegerValue() ;
    // VIPER #7902 : Produce warning for 0 replication multipler when multi-concat
    // is not an element of concatenation
    if (num <= 0 ) {
        // Viper 6577 : Issue warning for zero or negative value of repetition count
        // Verilog95 : repetition must be constant; mode = IsVeri95
        // Verilog2k : repetition must be non zero constant (nothing is said about negative numbers) ; mode = IsVeri2001
        // Verilog-1364-2005: repetition must be non negative constant (zero is allowed) ; mode = IsSystemVeri
        // Popular simulators differ in giving warning or error. Hence we decided to throw error in this case.
        /* CARBON_BEGIN */
      bool ignoreZeroRepeatCountItems = (0 != RuntimeFlags::GetVar("veri_carbon_ignore_concat_items_with_0_repeat_count"));
      if ( ( 0 != num) || ( ! ignoreZeroRepeatCountItems ) )
        /* CARBON_END, careful here, the next line of code is the THEN branch of this if stmt */
        Warning("repetition multiplier must be positive") ;
        num = 0 ; // Set the replication operator's value forcibly to 0 in case it is negative
    }
    // The array of expressions are sent to the local static function as an argument,
    // along with the context size and sign. The function returns the val, 0ue evaluating the expressions.
    VeriBaseValue *con_val = VeriConcat::StaticEvaluateConcatExprArr(0, _exprs, val_table, expect_nonconst) ;
    if (!con_val) {
        delete c_val ;
        return 0 ;
    }

    unsigned sign = 0 ;
    char *str = c_val->GetBinaryValue(&sign) ;
    delete c_val ;
    if (ConstStrEvaluation::StrHasXZ(str)) { // Return from function if the replication operator contains 'x' or 'z' terms
        delete con_val ;
        Strings::free(str) ;
        //Error("The replication operator is not constant, evaluation failed.\n") ;
        return 0 ;
    }

    Strings::free(str) ;
    veri_value_type val_type = con_val->Type() ;
    // VIPER #3359 & #4755: Special treatment for string type values, do not convert them into binary stream:
    if (num && (val_type == ASCIISTR)) {
        // VIPER #7397: Do not convert values to string, do special handling
        int i ;
        Array val_arr((unsigned)num) ;
        for (i = 0; i < num; i++) val_arr.InsertLast(con_val) ;
        VeriBaseValue *ret_val = VeriBaseValue::ConcatAsciiStrings(&val_arr, GET_CONTEXT_SIZE(context_size)) ;
        delete con_val ;
        return ret_val ;
    }
    // VIPER #3359 & #4755: Special treatment for string type values, do not convert them into binary stream:
    str = (val_type == ASCIISTR) ? Strings::save(con_val->GetString()) : con_val->GetBinaryValue(&sign) ;
    delete con_val ;
    unsigned len = (unsigned)Strings::len(str) ;
    if (!len) {
        Strings::free(str);
        return 0 ;
    }

    // ret_str_size should depend on the context size, not the expr size.
    // unsigned ret_str_size = ((unsigned)num * len ) ? (unsigned)num * len : 1 ; // Even if 'num' is '0' then the size should at least be 1, to hold that '0' value

    // Get expression size/sign from context_size
    if (!context_size) context_size = (int)StaticSizeSignInternal(val_table, target_constraint) ;
    unsigned ret_size = GET_CONTEXT_SIZE(context_size) ;
    // Since this is a multi-concat, it is always unsigned.
    // Not need to extract the signed-ness from context_size

    // VIPER #3359 & #4755: For string type a single bit is of size 8, so proper adjust here:
    // VIPER #7397 : Handled above
    if (val_type == ASCIISTR) ret_size = ret_size / 8 ;
    if (!ret_size) ret_size = 1 ; // VIPER #2809
    char *ret_str = Strings::allocate(ret_size) ;

#if 0
    // Commented out the following lines as the value of ret_str
    // should depend on the context_size and the value should
    // be evaluated as per the context_size argument.

    // Perform the concatenation for the required number of times. Required
    // number is the number corresponding to the replication operator.
    if (num) {
        unsigned i, j ;
        for(i = 0; i < (unsigned)num; i++) {
            for (j = 0; j < len; j++) {
                ret_str[i*len+j] = str[j] ;
            }
        }
    } else {
        // VIPER #2809: Multiplier is 0, the value becomes a single '0':
        ret_str[0] = '0' ;
    }

    ret_str[ret_str_size] = 0 ;
    char *prefix = Strings::itoa((int)ret_str_size) ;
#endif

    ret_str[ret_size] = '\0' ;
    int ret_str_idx = (int) (ret_size - 1) ;

    // ret_str should be truncated if context_size is less than
    // num*len. It should be padded with '0' if context_size is
    // greater than num*len.
    if (num) {
        int i, j ;
        for(i = 0; i < num; i++) {
            if (ret_str_idx < 0) break ;
            for (j = (int) (len-1); j >= 0; j--) {
                if (ret_str_idx < 0) break ;
                ret_str[ret_str_idx] = str[j] ;
                --ret_str_idx ;
            }
        }
    } else {
      /* CARBON_BEGIN */
      bool ignoreZeroRepeatCountItems = (0 != Verific::RuntimeFlags::GetVar("veri_carbon_ignore_concat_items_with_0_repeat_count"));
      if ( ! ignoreZeroRepeatCountItems ) {
      /* CARBON_END */
        // VIPER #2809: Multiplier is 0, the value becomes a single '0':
        ret_str[ret_str_idx] = '0' ;
        --ret_str_idx ;
      /* CARBON_BEGIN */
      }
      /* CARBON_END */
    }

    // Always pad with zero, since concat is always unsigned.
    for (; ret_str_idx >= 0; --ret_str_idx)
        ret_str[ret_str_idx] = '0' ;

    VeriBaseValue *val = 0 ;
    if (val_type == ASCIISTR) {
        // VIPER #3359 & #4755: For string type values, create VeriAsciiString instead of VeriBasedNumber:
        val = new VeriAsciiString(ret_str) ;
    } else {
        char *prefix = Strings::itoa((int)ret_size) ;
        char *val_str = Strings::save(prefix, "'b", ret_str) ;
        val = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        Strings::free(prefix) ;
    }

    Strings::free(ret_str) ;
    Strings::free(str) ;
    return val ;
}

// All System function call does not return constant values. Array quering functions and $bits will be considered
VeriBaseValue* VeriSystemFunctionCall::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint * /*target_constraint*/, unsigned expect_nonconst) const
{
    // VIPER #3275: Do not null check these and return, we want to produce error message later:
    //if (!_name || !_args) return 0 ;

    // Pick up the first argument
    VeriExpression *arg = (_args && _args->Size()) ? (VeriExpression*)_args->At(0): 0 ;

    switch(_function_type) {
    case VERI_SYS_CALL_RTOI:
    {
        VeriBaseValue *arg_val = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!arg_val) return 0 ;
        double real_val = arg_val->GetRealValue() ;
        delete arg_val ;
        return new VeriInteger((int)real_val) ; // Directly truncate: IEEE 1800 (2009) section 20.5
    }
    case VERI_SYS_CALL_ITOR :
    {
        VeriBaseValue *arg_val = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!arg_val) return 0 ;
        int int_val = arg_val->GetIntegerValue() ;
        delete arg_val ;
        return new VeriReal((double)int_val) ; // Only conver to real type: IEEE 1800 (2009) section 20.5
    }
    case VERI_SYS_CALL_BITS:
    {
        // Viper 5603: Exclude the dynamic type arguments
        VeriTypeInfo *type_info = arg ? arg->CheckType(0, NO_ENV) : 0 ;
        if (type_info && type_info->IsDynamicType() && !type_info->IsClassType()) {
            delete type_info ;
            return 0 ;
        }
        delete type_info ;

        // Extract the size of the argument
        int size_sign = arg ? (int)arg->StaticSizeSignInternal(val_table, 0) : 0 ;
        return new VeriInteger(ABS(size_sign)) ;
    }
    case VERI_SYS_CALL_DIMENSIONS:
    {
        // Extract the array identifier of first argument
        VeriIdDef *array_id = arg ? arg->FullId(): 0 ;
        // VIPER #5512: If argument is unresolved hier-name, try to resolve that
        if (!array_id && arg && arg->IsHierName()) {
            arg->Resolve(0, VERI_UNDEF_ENV) ;
            array_id = arg->GetId() ;
        }
        // VIPER #5197: argument can be data type too, handle it here:
        VeriDataType *type_arg = (arg) ? arg->DataTypeCast() : 0 ;
        if (!array_id && !type_arg) return 0 ; // something bad is wrong
        // Find the number of dimensions in this identifier. Since this can be nested
        // into named types etc, use the GetDimensionAt() routine
        unsigned dim = 0 ;
        //while (array_id->GetDimensionAt(dim)) dim++ ;
        // VIPER #4327 : Evaluate the constraint of the array
        VeriConstraint *constraint = (type_arg) ? type_arg->EvaluateConstraintInternal(val_table, 0) : array_id->EvaluateConstraintInternal(val_table, 0, 0) ;
        VeriConstraint *arg_constraint = constraint ;
        // Viper 5828. Correct evaluation of $dimensions
        while(constraint && constraint->ElementConstraint() && constraint->IsArrayConstraint()) {
            dim++ ;
            constraint = constraint->ElementConstraint() ;
        }
        // Viper 5828. Packed dimension increase $dimension by 1
        // Viper 5847: We should not use NumOfBits
        // if (constraint && constraint->IsPacked() && constraint->NumOfBits() > 1) dim++ ;
        // LRM 20.7(IEEE 2009) for iteger data types with predifined dimensions should have $dimension equal to 1
        // Viper 5852: Section 20.7(IEEE 2009) says for string types $dimension should return 1
        if (constraint && ((constraint->IsPacked() && constraint->IsRecordConstraint()) || (constraint->ScalarType() == VERI_STRINGTYPE) || (constraint->IsIntegerDataType() && constraint->NumOfBits()>1))) dim++ ;

        delete arg_constraint ;
        return new VeriInteger((int)dim) ;
    }
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS : // SV Std 1800 addition
    {
        // Extract the array identifier of first argument
        VeriIdDef *array_id = arg ? arg->FullId(): 0 ;
        // VIPER #5512: If argument is unresolved hier-name, try to resolve that
        if (!array_id && arg && arg->IsHierName()) {
            arg->Resolve(0, VERI_UNDEF_ENV) ;
            array_id = arg->GetId() ;
        }
        // VIPER #5197: argument can be data type too, handle it here:
        VeriDataType *type_arg = (arg) ? arg->DataTypeCast() : 0 ;
        if (!array_id && !type_arg) return 0 ; // something bad is wrong
        // Find the number of unpacked dimensions in this identifier.
        unsigned dim = (type_arg) ? type_arg->UnPackedDimension() : array_id->UnPackedDimension() ;

        return new VeriInteger((int)dim) ;
    }
    case VERI_SYS_CALL_LEFT:
    case VERI_SYS_CALL_RIGHT:
    case VERI_SYS_CALL_LOW:
    case VERI_SYS_CALL_HIGH:
    case VERI_SYS_CALL_INCREMENT:
    case VERI_SYS_CALL_LENGTH:
    case VERI_SYS_CALL_SIZE:
    {
        // VIPER #4595 : Static evaluate should return 0 for array query
        // functions called on dynamic arrays
        VeriTypeInfo *type_info = 0 ;
        type_info = arg ? arg->CheckType(0, NO_ENV) : 0 ;
        if (type_info && type_info->IsDynamicType()) {
            delete type_info ;
            return 0 ;
        }
        delete type_info ;

        // Pick up the identifier in the first argument :
        VeriIdDef *array_id = arg ? arg->FullId(): 0 ;
        // VIPER #5512: If argument is unresolved hier-name, try to resolve that
        if (!array_id && arg && arg->IsHierName()) {
            arg->Resolve(0, VERI_UNDEF_ENV) ;
            array_id = arg->GetId() ;
        }
        // VIPER #5197: argument can be data type too, handle it here:
        VeriDataType *type_arg = (arg) ? arg->DataTypeCast() : 0 ;
        if (!array_id && !type_arg) return 0 ; // something bad is wrong

        // Second argument contains the dimension number whose information is to be returned
        VeriExpression *arg2 = (_args && _args->Size() > 1) ? (VeriExpression*)_args->At(1) : 0 ;

        int dim_num = 0 ;
        // VIPER #4562 : Detect out of bound dimension and produce 'x' for that :
        unsigned out_of_bound = 0 ;
        if (arg2) {
            // Extract the dimension
            VeriBaseValue *dim = arg2->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
            if (dim) {
                dim_num = dim->GetIntegerValue() ;
                // Actually, second arg '1' means the first dimension.  That is accessible
                // in GetDimensionAt() by index 0.  so decrement the dimension :
                //if (dim_num) dim_num-- ;
                dim_num-- ;
            } else {
                // non-constant dimension value.
                return 0 ;
                //arg2->Error("constant expression required") ;
            }
            delete dim ;
            if (dim_num < 0) {
                //arg2->Warning("dimension for %s is out of bounds",array_id->Name()) ;
                out_of_bound = 1 ;
                dim_num = 0 ;
            }
            if (array_id && (out_of_bound || !array_id->GetDimensionAt((unsigned)dim_num))) {
                arg2->Warning("dimension for %s is out of bounds",array_id->Name()) ;
            }
        }
        // VIPER #4327 : Evaluate the constraint of the array
        VeriConstraint *constraint = (type_arg) ? type_arg->EvaluateConstraintInternal(val_table, 0) : array_id->EvaluateConstraintInternal(val_table, 0, 0) ;
        VeriConstraint *arr_constraint = constraint ;
        VeriConstraint *orig_constraint = constraint ;
        // Go to the specific dimension
        int dim_count = dim_num ;
        while (dim_count > 0) {
            // VIPER #5828 : Evaluate the constraint of the array if it is array constraint only
            constraint = constraint ? constraint->ElementConstraint() : 0 ;
            dim_count-- ;
        }
        int dim = 0 ;
        // Viper 5864: According to LRM 20.7 IEEE 2009 array query functions should return x if the
        // first argument has $dimension equal to zero or the second arg is out of range.
        while(arr_constraint && arr_constraint->ElementConstraint() && arr_constraint->IsArrayConstraint()) {
            dim++ ;
            arr_constraint = arr_constraint->ElementConstraint() ;
        }
        // Viper 5828. Packed dimension increase $dimension by 1
        // Viper 5847: We should not use NumOfBits
        // if (constraint && constraint->IsPacked() && constraint->NumOfBits() > 1) dim++ ;
        // LRM 20.7(IEEE 2009) for iteger data types with predifined dimensions should have $dimension equal to 1
        // Viper 5852: Section 20.7(IEEE 2009) says for string types $dimension should return 1
        if (arr_constraint && ((arr_constraint->IsPacked() && arr_constraint->IsRecordConstraint()) || (arr_constraint->ScalarType() == VERI_STRINGTYPE) || (arr_constraint->IsIntegerDataType() && arr_constraint->NumOfBits()>1))) dim++ ;

        // VIPER 5864: Here dim represents the $dimension on the first argument.

        int left_int = 0, right_int = 0 ;
        if (constraint && !out_of_bound && dim) { // VIPER #4562 : Process constraint for non out of bound dimensions
            if (constraint->IsScalarTypeConstraint()) {
                unsigned size = constraint->NumOfBits() ;
                if (size > 1) {
                    left_int = (int)size - 1 ;
                    right_int = 0 ;
                } else {
                    out_of_bound = 1 ;
                }
            } else {
                left_int = constraint->Left() ;
                right_int = constraint->Right() ;
            }
        } else {
            out_of_bound = 1 ;
        }

        if (out_of_bound) {
            char *ret_str = ConstStrEvaluation::VeriGetXOfWidth((!context_size) ? 32 : (unsigned)ABS(context_size)) ;
            char *prefix = Strings::itoa((int)Strings::len(ret_str)) ;
            char *val_str = Strings::save(prefix, "'b", ret_str) ;
            VeriBaseValue *val = new VeriBasedNumber(val_str) ;
            Strings::free(ret_str) ;
            Strings::free(val_str) ;
            Strings::free(prefix) ;
            delete orig_constraint ;
            return val ;
        }
        delete orig_constraint ;

        // calculate result value :
        int result = 0 ;
        switch(_function_type) {
        case VERI_SYS_CALL_LEFT : result = left_int ; break ;
        case VERI_SYS_CALL_RIGHT : result = right_int ; break ;
        case VERI_SYS_CALL_LOW : result = (left_int > right_int) ? right_int : left_int ; break ;
        case VERI_SYS_CALL_HIGH : result = (left_int > right_int) ? left_int : right_int ; break ;
        case VERI_SYS_CALL_INCREMENT : result = (left_int >= right_int) ? 1 : -1 ; break ;
        case VERI_SYS_CALL_LENGTH : result = (left_int > right_int) ? ((left_int-right_int)+1) : ((right_int-left_int)+1) ; break ;
        case VERI_SYS_CALL_SIZE : result = (left_int > right_int) ? ((left_int-right_int)+1) : ((right_int-left_int)+1) ; break ;
        default: break ;
        }

        // return in VeriBaseValue form :
        return new VeriInteger(result) ;
    }
    case VERI_SYS_CALL_CAST:
    {
        // The is the system function $cast. It has two arguments. The first one is the
        // target and the second one is the value to be assigned on the target.
        VeriExpression *expr = (_args && (_args->Size() > 1)) ? (VeriExpression*)_args->At(1) : 0 ;

        // Evaluate the expression:
        VeriBaseValue *value = (expr) ? expr->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;

        // Assign the value on the target:
        unsigned result = (arg) ? arg->StaticAssign(val_table, value, expect_nonconst) : 0 ;
        delete value ;

        // Always return the result value of the assignment:
        return (new VeriInteger((int)result)) ;
    }
    // Can't easily support these two system functions, because other tools seem to
    // convert to and from IEEE 754 standard between real and bits. Since, we don't
    // have any mechanism to truncate/expand and store the bits of a floating point
    // number differently than a simple integer binary number we currently can't
    // support it here in static elaboration. If we need to support it, we need to
    // introduce a new value or add a flag to VeriBasedNumber class for it.
    case VERI_SYS_CALL_SHORTREALTOBITS:
    {
        // VIPER #3076: Evaluate short-real to bits:
        VeriBaseValue *value = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!value) return 0 ;
        // According to IEEE 1800-2012 section 6.12, shortreal is same as C float
        double d = value->GetRealValue() ;
        char *bits = ConstStrEvaluation::ShortRealToBits((float)d) ;
        char *val_str = Strings::save("32'b", bits) ;
        Strings::free(bits) ;
        delete value ;
        value = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        return value ;
    }
    case VERI_SYS_CALL_BITSTOSHORTREAL:
    {
        // VIPER #3076: Evaluate bits to short-real:
        VeriBaseValue *value = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!value) return 0 ;
        char *bin_str = value->GetBinaryValue(0) ;
        float r = ConstStrEvaluation::BitsToShortReal(bin_str) ;
        Strings::free(bin_str) ;
        delete value ;
        return (new VeriReal((double)r)) ;
    }
    case VERI_SYS_CALL_REALTOBITS:
    {
        // VIPER #3076: Evaluate real to bits:
        VeriBaseValue *value = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!value) return 0 ;
        double d = value->GetRealValue() ;
        char *bits = ConstStrEvaluation::RealToBits(d) ;
        char *val_str = Strings::save("64'b", bits) ;
        Strings::free(bits) ;
        delete value ;
        value = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        return value ;
    }
    case VERI_SYS_CALL_BITSTOREAL:
    {
        // VIPER #3076: Evaluate bits to real:
        VeriBaseValue *value = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!value) return 0 ;
        char *bin_str = value->GetBinaryValue(0) ;
        double r = ConstStrEvaluation::BitsToReal(bin_str) ;
        Strings::free(bin_str) ;
        delete value ;
        return (new VeriReal(r)) ;
    }
    case VERI_SYS_CALL_ISUNKNOWN:
    {
        // First evaluate the argument:
        VeriBaseValue *value = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
        if (!value) return 0 ;

        // Check and return the value to indicate whether this value has x/z:
        unsigned result = value->HasXZ() ;
        delete value ;
        return (new VeriInteger((int)result)) ;
    }
    case VERI_SYS_CALL_TYPENAME:
    {
        // VIPER #5248 (test60): Support $typename system function call:
        VeriIdDef *id = (arg) ? arg->GetId() : 0 ;
        VeriDataType *data_type = (id) ? id->GetDataType() : 0 ;
        unsigned type = (data_type) ? data_type->Type() : 0 ;
        return (new VeriAsciiString(PrintToken(type))) ;
    }
    case VERI_SYS_CALL_SIGNED:
    case VERI_SYS_CALL_UNSIGNED:
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        VERIFIC_ASSERT(arg) ; // Must hold since 'val' is valid here
        int arg_size_sign = (int)arg->StaticSizeSignInternal(val_table, 0) ;
        unsigned size = context_size ? GET_CONTEXT_SIZE(context_size) : GET_CONTEXT_SIZE(arg_size_sign) ;
        VeriBaseValue *result = 0 ;
        if (_function_type==VERI_SYS_CALL_SIGNED) {
            result = val ? val->Adjust(size, 1, 0, 0): 0 ; // Adjust absorbs val
        } else {
            result = val ? val->Adjust(size, 0, 0, 0): 0 ;
        }
        return result ;
    }
    // Moved these function to VERILOG_2000 mode
    // LRM section 17.11.1/17.11.2 Viper 6807
    // P1800-2009 math functions (20.8)
    case VERI_SYS_CALL_CLOG2 : // integer function
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Argument should be treated as unsigned value
        unsigned un_val = (val) ? (unsigned)val->GetIntegerValue(): 0 ;
        delete val ;
        // Viper 6962: LRM Section 20.8.1 for argument value 0 return 0
        // other wise It returns the ceiling of log base 2 of the argument.
        // The argument is always treated as unsigned and the result is integer.
        // if the MSB which is 1 is the kth bit the the result must be
        // k (if all other bits is zero) or k+ 1 (if any bit from 0th position to k-1th position is 1)
        int result = 0 ;
        while (un_val > 1) {
            // un_val & 1 checks if the LSB is one.
            // If LSB is one one is added to un_val so that
            // finally the MSB which is one shifts left by one.
            un_val = (un_val >> 1) + (un_val & 1) ;
            result++ ;
        }
        return new VeriInteger(result);
    }
    case VERI_SYS_CALL_LN : // real functions
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(log(real_val))) ; // Return natural log
    }
    case VERI_SYS_CALL_LOG10 :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(log10(real_val))) ; // Return 'log base 10'
    }
    case VERI_SYS_CALL_EXP :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(exp(real_val))) ;
    }
    case VERI_SYS_CALL_SQRT :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(sqrt(real_val))) ;
    }
    case VERI_SYS_CALL_POW :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val1 = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        arg = (_args && _args->Size() > 1) ? (VeriExpression*)_args->At(1): 0 ;
        if (!arg) return 0 ;
        val = arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
        double real_val2 = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(pow(real_val1, real_val2))) ;
    }
    case VERI_SYS_CALL_FLOOR :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(floor(real_val))) ;
    }
    case VERI_SYS_CALL_CEIL :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(ceil(real_val))) ;
    }
    case VERI_SYS_CALL_SIN :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(sin(real_val))) ;
    }
    case VERI_SYS_CALL_COS :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(cos(real_val))) ;
    }
    case VERI_SYS_CALL_TAN :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(tan(real_val))) ;
    }
    case VERI_SYS_CALL_ASIN :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(asin(real_val))) ;
    }
    case VERI_SYS_CALL_ACOS :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(acos(real_val))) ;
    }
    case VERI_SYS_CALL_ATAN :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(atan(real_val))) ;
    }
    case VERI_SYS_CALL_ATAN2 :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        arg = (_args && _args->Size() > 1) ? (VeriExpression*)_args->At(1): 0 ;
        if (!arg) return 0 ;
        val = arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
        double real_val2 = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(atan2(real_val, real_val2))) ;
    }
    case VERI_SYS_CALL_HYPOT :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        arg = (_args && _args->Size() > 1) ? (VeriExpression*)_args->At(1): 0 ;
        if (!arg) return 0 ;
        val = arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
        double real_val2 = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(hypot(real_val, real_val2))) ;
    }
    case VERI_SYS_CALL_SINH :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(sinh(real_val))) ;
    }
    case VERI_SYS_CALL_COSH :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(cosh(real_val))) ;
    }
    case VERI_SYS_CALL_TANH :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        return (new VeriReal(tanh(real_val))) ;
    }
    case VERI_SYS_CALL_ASINH :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        // Return the asinh of the value: don't use asinh() function since it is not available on all platforms
        double v = (exp(real_val) - exp(-real_val))/2 ;
        double temp = v + sqrt(pow(v, 2.0) + 1) ;
        return (new VeriReal(log(temp))) ;
    }
    case VERI_SYS_CALL_ACOSH :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = (val) ? val->GetRealValue(): 0 ;
        delete val ;
        // Return the acosh of the value: don't use acosh() function since it is not available on all platforms
        double v = (exp(real_val) + exp(-real_val))/2 ;
        double temp = v + sqrt(pow(v, 2.0) - 1) ;
        return (new VeriReal(log(temp))) ;
    }
    case VERI_SYS_CALL_ATANH :
    {
        // Evaluate argument using self size/sign
        VeriBaseValue *val = arg ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst): 0 ;
        if (!val) return 0 ;
        // Real value is accepted as argument
        double real_val = val->GetRealValue() ;
        delete val ;
        // Return the atanh of the value: don't use atanh() function since it is not available on all platforms
        double num = (exp(real_val) - exp(-real_val))/2;
        double den = (exp(real_val) + exp(-real_val))/2;
        double v = num/den;
        double temp = sqrt((1+v)/(1-v));
        return (new VeriReal(log(temp))) ;
    }
    case VERI_SYS_CALL_ISUNBOUNDED :
    {
        // VIPER #5652 : Support for isunbounded system function :
        // If argument is '$' or parameter reference whose initial value is '$',
        // we will return 1. Otherwise we will return 0
        VeriIdDef *arg_id = (arg) ? arg->GetId(): 0 ;
        VeriExpression *arg_val = (arg_id) ? arg_id->GetInitialValue(): 0 ;
        if ((arg && arg->IsDollar()) || (arg_val && arg_val->IsDollar())) {
            return new VeriInteger(1) ;
        }
        return new VeriInteger(0) ;
    }
    default: break ;
    }

    // VIPER #2868 (side effect): Error out for unsupported system function calls:
    // VIPER #7424 : Do not produce error if expression can be non constant
    if (!expect_nonconst) Error("system function call %s not supported", _name) ;
    return 0 ;
}

VeriBaseValue* VeriMinTypMaxExpr::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_min_expr || !_typ_expr || !_max_expr) return 0 ;
    VeriBaseValue *val = 0 ;

    // The minimum, typical and the maximum values are evaluated and a VeriMinTypMax value is created.

    VeriBaseValue *min_val = _min_expr->StaticEvaluateInternal(context_size, val_table, target_constraint, expect_nonconst) ;
    VeriBaseValue *typ_val = _typ_expr->StaticEvaluateInternal(context_size, val_table, target_constraint, expect_nonconst) ;
    VeriBaseValue *max_val = _max_expr->StaticEvaluateInternal(context_size, val_table, target_constraint, expect_nonconst) ;

    if (min_val && typ_val && max_val) val = new VeriMinTypMaxValue(min_val, typ_val, max_val) ;

    delete min_val ;
    delete typ_val ;
    delete max_val ;
    return val ;
}

VeriBaseValue* VeriUnaryOperator::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_arg) return 0 ;

    if(val_table && val_table->IsDeadCode()) return 0 ;  // We are in dead code!

    // Determine size of the argument
    unsigned arg_size = GET_CONTEXT_SIZE(context_size) ;
    unsigned arg_sign = GET_CONTEXT_SIGN(context_size) ;
    switch (_oper_type) {
    case VERI_PLUS :
    case VERI_MIN :
    case VERI_REDNOT :
        if (!context_size) {
            int arg_size_sign = (int)_arg->StaticSizeSignInternal(val_table, target_constraint) ;
            arg_size = GET_CONTEXT_SIZE(arg_size_sign) ;
            arg_sign = GET_CONTEXT_SIGN(arg_size_sign) ;
        }
        break ;

    case VERI_REDAND :
    case VERI_REDNAND :
    case VERI_REDOR :
    case VERI_REDNOR :
    case VERI_REDXOR :
    case VERI_REDXNOR :
    case VERI_LOGNOT :
        arg_size = 0 ; // Argument is self-determined
        arg_sign = 0 ;
        break ;
    case VERI_NOT:
    case VERI_FIRST_MATCH:

    default:
        // VIPER #7424: Donot produce warning when expression can be non constant expression
        if (!expect_nonconst) Warning("%s is not supported for static elaboration", PrintToken(_oper_type)) ;
        return 0 ;
    }

    int arg_size_sign = MAKE_CONTEXT_SIZE_SIGN(arg_size,arg_sign) ;
    // Evaluate the argument.
    VeriBaseValue *l_val = _arg->StaticEvaluateInternal(arg_size_sign, val_table, 0, expect_nonconst) ;
    if (!l_val) return 0 ;

    // VIPER #3176 (continuation): Adjust the evaluated value to the context size:
    if (arg_size && (l_val->Type() != ASCIISTR)) l_val = l_val->Adjust(arg_size, arg_sign, 0, 0) ;

    // Further processing is done to evaluate the unary operator.
    if (!arg_size_sign) arg_size_sign = (int)StaticSizeSignInternal(val_table, target_constraint) ;
    VeriBaseValue *val = StaticEvaluateUnary(l_val, arg_size_sign, _oper_type) ;

    delete l_val ;
    return val ;
}

/* static */
VeriBaseValue *VeriUnaryOperator::StaticEvaluateUnary(const VeriBaseValue *operand, int context_size, unsigned oper_type)
{
    if (!operand) return 0 ;

    VeriBaseValue *val = 0 ;
    // If the argument 'operand' is a VeriMinTypMax value, this function calls itself recursively with each value.
    if (operand->Type() == MINTYPMAX) {
        VeriBaseValue *new_min = StaticEvaluateUnary(operand->GetMinimum(), context_size, oper_type) ;
        VeriBaseValue *new_typ = StaticEvaluateUnary(operand->GetTypical(), context_size, oper_type) ;
        VeriBaseValue *new_max = StaticEvaluateUnary(operand->GetMaximum(), context_size, oper_type) ;

        if (new_min && new_typ && new_max) val = new VeriMinTypMaxValue(new_min, new_typ, new_max) ;

        delete new_min ;
        delete new_typ ;
        delete new_max ;
        return val ;
    }

    unsigned ret_size = GET_CONTEXT_SIZE(context_size) ;
    unsigned ret_sign = GET_CONTEXT_SIGN(context_size) ;

    veri_value_type ret_type  ;
    unsigned b_real = 0 ;
    if (operand->Type() == REAL) {
        // If any of the operands is real, computations cannot be done in string
        // domain, it has to be done in floating domain.
        ret_type = REAL ;
        b_real = 1 ;
    } else {
        ret_type = BASEDNUM ;
    }
    char* binstr = 0 ;
    char* tstr = 0 ;
    char* valstr = 0 ;
    unsigned is_bit_op = 0 ;
    double ret_dval = 0.0 ;
    unsigned b_sign = 0 ;
    binstr = operand->GetBinaryValue(&b_sign) ;
    if (!binstr && !b_real) return 0 ;

    switch (oper_type) {
    case VERI_PLUS:
        if (b_real) {
            ret_dval = operand->GetRealValue() ;
        } else {
            // Nothing to do for unary add, so just doing a strdup.
            valstr = Strings::save(binstr) ;
        }
        break ;
    case VERI_MIN:
        if (b_real) {
            ret_dval = -1 * operand->GetRealValue() ;
        } else {
            // If the operands has 'x' or 'z', return value should also be a string of 'x' of required size.
            if (ConstStrEvaluation::StrHasXZ(binstr)) {
                valstr = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
            } else {
                // Unary minus means the 2's complement of the actual number.
                valstr = ConstStrEvaluation::Str2sComp(binstr) ;
            }
        }
        break ;
    case VERI_LOGNOT:
        if (b_real) {
            ret_dval = !operand->GetRealValue() ;
        } else {
            // Performing logical NOT.
            valstr = ConstStrEvaluation::StrLogicalNOT(binstr) ;
            is_bit_op = 1 ;
        }
        break ;
    case VERI_REDNOT:
        if (b_real) break ; // operation not permitted in real operand
        // Performing bitwise NOT.
        valstr = ConstStrEvaluation::StrInvert(binstr) ;
        is_bit_op = 1 ;
        break ;
    case VERI_REDAND:
        if (b_real) break ; // operation not permitted in real operand
        // Performing reduced reduction AND.
        valstr = ConstStrEvaluation::StrReductAND(binstr) ;
        is_bit_op = 1 ;
        break ;
    case VERI_REDNAND:
        if (b_real) break ; // operation not permitted in real operand
        // Performing reduced reduction NAND.
        tstr = ConstStrEvaluation::StrReductAND(binstr) ;
        valstr = ConstStrEvaluation::StrInvert(tstr) ;
        Strings::free(tstr) ;
        is_bit_op = 1 ;
        break ;
    case VERI_REDOR:
        if (b_real) break ; // operation not permitted in real operand
        // Performing reduced reduction OR.
        valstr = ConstStrEvaluation::StrReductOR(binstr) ;
        is_bit_op = 1 ;
        break ;
    case VERI_REDNOR:
        if (b_real) break ; // operation not permitted in real operand
        // Performing reduced reduction NOR.
        tstr = ConstStrEvaluation::StrReductOR(binstr) ;
        valstr = ConstStrEvaluation::StrInvert(tstr) ;
        Strings::free(tstr) ;
        is_bit_op = 1 ;
        break ;
    case VERI_REDXOR:
        if (b_real) break ; // operation not permitted in real operand
        // Performing reduced reduction XOR.
        valstr = ConstStrEvaluation::StrReductXOR(binstr) ;
        is_bit_op = 1 ;
        break ;
    case VERI_REDXNOR:
        if (b_real) break ; // operation not permitted in real operand
        // Performing reduced reduction XNOR.
        tstr = ConstStrEvaluation::StrReductXOR(binstr) ;
        valstr = ConstStrEvaluation::StrInvert(tstr) ;
        Strings::free(tstr) ;
        is_bit_op = 1 ;
        break ;
    default:
/*  case VERI_FIRST_MATCH:
    case VERI_NOT:    // for PSL for SV assert
    case VERI_PSL_POSEDGE:
    case VERI_PSL_NEGEDGE:
    case VERI_PSL_ROSE:
    case VERI_PSL_FELL: */
        //Warning("%s is not supported for static elaboration", PrintToken(oper_type)) ;
        return 0 ;
    }

    if ((operand->Type() == INTEGER) && !is_bit_op) ret_type = INTEGER ;

    tstr = 0 ;
    if ((ret_type == INTEGER) && (!ConstStrEvaluation::StrHasXZ(valstr))) {
        if (valstr) val = new VeriInteger(ConstStrEvaluation::BinToInt(valstr, ret_sign)) ;
        Strings::free(valstr) ;
    } else if (ret_type == REAL) {
        val = new VeriReal(ret_dval) ;
    } else if (valstr) {
        unsigned len = (unsigned)Strings::len(valstr) ;
        // Performing necessary padding.
        if (len != ret_size) {
            char *tmp = valstr ;
            valstr = ConstStrEvaluation::GetPaddedString(tmp, ret_size, ret_sign) ;
            Strings::free(tmp) ;
        }
        char *prefix = Strings::itoa((int)ret_size) ;
        tstr = Strings::save(prefix, (ret_sign ? "'sb": "'b"), valstr) ;
        val = new VeriBasedNumber(tstr) ;
        Strings::free(valstr) ;
        Strings::free(tstr) ;
        Strings::free(prefix) ;
    }

    Strings::free(binstr) ;
    return val ;
}

VeriBaseValue* VeriBinaryOperator::StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    // Both of them should not be NULL at the same time. One of them can certainly be NULL if it is a pre
    // or post increment or decrement operator!
    if (!_left && !_right) return 0 ;

    // VIPER #5819: Support for type (type operator and $typeof system function) comparison:
    unsigned left_type_oper = (_left && (_left->IsTypeOperator() || (_left->GetFunctionType() == VERI_SYS_CALL_TYPEOF))) ? 1 : 0 ;
    unsigned right_type_oper = (_right && (_right->IsTypeOperator() || (_right->GetFunctionType() == VERI_SYS_CALL_TYPEOF))) ? 1 : 0 ;
    if (_left && _right && (left_type_oper || right_type_oper)) {
        // Some consistency check of the operands vs operators:
        if ((left_type_oper && !right_type_oper) ||
            (!left_type_oper && right_type_oper)) {
            // Only one of the operands is type operator error:
            VeriTreeNode *node = (left_type_oper) ? _right : _left ;
            node->Error("type comparison cannot be done with non-type expressions") ;
            return 0 ;
        } else if ((_oper_type != VERI_LOGEQ) && (_oper_type != VERI_LOGNEQ) &&
                   (_oper_type != VERI_CASEEQ) && (_oper_type != VERI_CASENEQ)) {
            Error("illegal operator %s for type operand", PrintToken(_oper_type)) ;
            return 0 ;
        }

        // Create the types of the two operands:
        VeriTypeInfo *left_type = _left->CheckType(0, NO_ENV) ;
        VeriTypeInfo *right_type = _right->CheckType(0, NO_ENV) ;
        // VIPER #8131 : Do not use 'IsEqual' for type matching. 'IsEqual' returns
        // false for 'reg == logic'. But according to LRM type of operands should
        // be matching type (1800-2012 section 6.22.1)
        unsigned equal = (left_type && right_type && left_type->IsEquivalent(right_type, 0, 0, 0)) ? 1 : 0 ;
        // VIPER #8131 : Check constraint of operands too to check size mismatch
        VeriConstraint *left_constraint = _left->EvaluateConstraintInternal(n, 0) ;
        VeriConstraint *right_constraint = _right->EvaluateConstraintInternal(n, 0) ;
        if (left_constraint && right_constraint && !left_constraint->IsMatching(right_constraint)) equal = 0 ;
        delete left_constraint ;
        delete right_constraint ;
        const char *val = ((equal && ((_oper_type == VERI_LOGEQ) || (_oper_type == VERI_CASEEQ))) ||
                           (!equal && ((_oper_type == VERI_LOGNEQ) || (_oper_type == VERI_CASENEQ)))) ? "1'b1" : "1'b0" ;
        delete left_type ; delete right_type ; // Cleanup
        return new VeriBasedNumber(val) ;
    }

    if (_oper_type == VERI_INSIDE) {
        // Viper 4603: Static Elaboration of inside operator. The
        // Semantics is defined in section 8.19 of P1800 LRM
        // The rhs contains a concat of expressions. If the lhs
        // satifies Set Membership on the rhs set then the operator
        // returns 1'b1 or else 1'b0 ; The semantics of comparison
        // is different for different type of expressions. Firstly
        // the concat is broken down to singular expressions.For non-
        // integral expressions use equality. For integral expressions
        // use wild equality. For range use within semantics i.e return
        // true if the left expression is within the range. For array
        // compare left expression with each element of array.

        if (!context_size) context_size = (int)StaticSizeSignInternal(n, target_constraint) ;

        if (!_left || !_right || !_right->IsConcat()) return 0 ;
        unsigned i ;
        VeriExpression *expr ;
        Array *exprs = _right->GetExpressions() ;
        Array values ;
        VeriMapForCopy old2new ;
        FOREACH_ARRAY_ITEM(exprs, i, expr) {
            if (!expr) continue ;
            if (expr->IsRange()) {
                if (expr->HasDollar()) {
                    expr->Warning("%s is not yet supported for synthesis", "$ in open range list of inside operator") ;
                    continue ;
                }
                int left_bound ;
                int right_bound ;
                (void) expr->GetWidth(&left_bound, &right_bound, n) ;
                if (left_bound > right_bound) {
                    expr->Warning("empty range with constant bounds, low-bound and high-bound may be reversed") ;
                    continue ;
                }

                VeriExpression* left_range = expr->GetLeft() ;
                VeriExpression* right_range = expr->GetRight() ;
                if (left_range && right_range) {
                    VeriExpression *geq = new VeriBinaryOperator(VERI_GEQ, _left->CopyExpression(old2new), left_range->CopyExpression(old2new)) ;
                    geq->SetLinefile(left_range->Linefile()) ;
                    VeriExpression *leq = new VeriBinaryOperator(VERI_LEQ, _left->CopyExpression(old2new), right_range->CopyExpression(old2new)) ;
                    leq->SetLinefile(left_range->Linefile()) ;
                    VeriExpression *logand = new VeriBinaryOperator(VERI_LOGAND, geq, leq) ;
                    logand->SetLinefile(left_range->Linefile()) ;
                    VeriBaseValue* cond = logand->StaticEvaluateInternal(context_size, n, 0, expect_nonconst) ;

                    if (cond) values.InsertLast(cond) ;
                    delete logand ;
                } else {
                    return 0 ;
                }
                continue ;
            }
            // VIPER #8364: Check type of expression and if expression is packed, add
            // one wild equality operator. We need to recurse for unpacked arrays
            VeriTypeInfo *expr_type = expr->CheckType(0, NO_ENV) ;
            if ((expr_type && (expr_type->IsPacked() || expr_type->IsBuiltInType())) || expr->IsConst()) {
                VeriBinaryOperator eq(VERI_WILDEQUALITY, _left->CopyExpression(old2new), expr->CopyExpression(old2new)) ;
                VeriBaseValue *cond = eq.StaticEvaluateInternal(context_size, n, 0, expect_nonconst) ;

                if (cond) values.InsertLast(cond) ;
            } else {
                verific_int64 lss = _left->StaticSizeSignInternal(n, 0 /* target_constraint? */) ;
                unsigned lsize = GET_CONTEXT_SIZE(lss) ;
                unsigned lsign = GET_CONTEXT_SIGN(lss) ;

                // When expression is unpacked, we need to traverse the elements to
                // apply wild equality for only sigular (packed) elements.
                VeriConstraint *expr_constraint = expr->EvaluateConstraintInternal(n, 0) ;
                VeriConstraint *ele_constraint = expr_constraint ;
                while (ele_constraint && ele_constraint->IsArrayConstraint() && !ele_constraint->IsPacked()) {
                   ele_constraint = ele_constraint->ElementConstraint() ;
                }
                unsigned ele_size = ele_constraint ? ele_constraint->NumOfBits() : lsize ;
                unsigned ele_sign = ele_constraint ? ele_constraint->IsSigned(): 0 ;
                lsign = (lsign && ele_sign) ? 1: 0 ;
                delete expr_constraint ;
                lsize = MAX(lsize, ele_size) ;
                int lsize_sign = MAKE_CONTEXT_SIZE_SIGN(lsize,lsign) ;
                VeriBaseValue* l = _left->StaticEvaluateInternal(lsize_sign, n, 0, expect_nonconst) ;
                VeriBaseValue *expr_val = expr->StaticEvaluateInternal(0, n, 0, expect_nonconst) ;
                if (!expr_val || !l) {
                    delete l ; delete expr_val ; return 0 ;
                }
                verific_int64 expr_ss = expr->StaticSizeSignInternal(n, 0 /* target_constraint? */) ;
                unsigned expr_size = GET_CONTEXT_SIZE(expr_ss) ;
                unsigned pos = 0 ;
                while (pos + ele_size <= expr_size) {
                    VeriBaseValue* split_val = expr_val->Evaluate(ele_size, pos) ;
                    pos += ele_size ;
                    VeriBaseValue *cond =  VeriBinaryOperator::StaticEvaluateBinary(l, split_val, context_size, VERI_WILDEQUALITY) ;
                    delete split_val ;
                    if (cond) values.InsertLast(cond) ;
                }
                delete l ;
                delete expr_val ;
            }
            delete expr_type ;
        }
        if (!values.Size()) return 0 ;
        if (values.Size()==1) return (VeriBaseValue*)values.At(0) ;
        VeriBaseValue *result = 0, *tmp = 0 ;
        VeriBaseValue *val ;
        VeriBinaryOperator logor(VERI_LOGOR,0,0) ;
        FOREACH_ARRAY_ITEM(&values, i, val) {
            if (!val) continue ;
            if (!i) {
                result = val ;
                continue ;
            }
            tmp = result ;
            result = result ? VeriBinaryOperator::StaticEvaluateBinary(result, val, context_size, VERI_LOGOR) : 0 ;
            delete tmp ;
            delete val ;
        }
        return result ;
    }

    if (!context_size) context_size = (int)StaticSizeSignInternal(n, target_constraint) ;
    // default left/right expression sizes/sign
    unsigned lsize = GET_CONTEXT_SIZE(context_size) ;
    unsigned rsize = lsize ;
    unsigned lsign = GET_CONTEXT_SIGN(context_size) ;
    unsigned rsign = lsign ;

    // VIPER #5048: Handling real operands: they do not contribute to evaluation size:
    unsigned real_lhs = (_left) ? _left->IsInvolvingReal() : 0 ;
    unsigned real_rhs = (_right) ? _right->IsInvolvingReal() : 0 ;
    if (real_lhs || real_rhs) {
        if (real_lhs) {
            lsize = 0 ;
        } else if (_left) {
            int ss = (int)_left->StaticSizeSignInternal(n, 0 /* target_constraint? */) ;
            lsize = GET_CONTEXT_SIZE(ss) ;
        }
        if (real_rhs) {
            rsize = 0 ;
        } else if (_right) {
            int ss = (int)_right->StaticSizeSignInternal(n, 0 /* target_constraint? */) ;
            rsize = GET_CONTEXT_SIZE(ss) ;
        }
    }

    // Find the 'left' and 'right' argument sizes for the operator. When the expression
    // result size is given in 'context_size' (context determined).  If result is
    // self-determined (context_size==0) then we need to set the determined size
    // of the arguments based on their self-determined size and the fact that they
    // are part of this expression.  Rules are in Verilog LRM 2000 Section 4.4.

    switch(_oper_type) {
    case VERI_EQUAL_ASSIGN:
        // Another odd-ball : plain assignment inside a expression. LRM 7.3.
        if (_right && _left) {
            int right_size_sign = (int)_right->StaticSizeSignInternal(n, 0 /* target_constraint? */) ;
            int left_size_sign = (int)_left->StaticSizeSignInternal(n, 0 /* target_constraint? */) ;

            // _right should be evaluated with rsign as context sign
            rsign = GET_CONTEXT_SIGN(right_size_sign) ;

            // _right should be evaluated with MAX(lsize, rsize)
            unsigned rsize_tmp = GET_CONTEXT_SIZE(right_size_sign) ;
            unsigned lsize_tmp = GET_CONTEXT_SIZE(left_size_sign) ;

            rsize = MAX(rsize_tmp, lsize_tmp) ;
        }
        break ;
    case VERI_PLUS :
    case VERI_MIN :
    case VERI_DIV :
    case VERI_MODULUS :
    case VERI_REDAND :
    case VERI_REDOR :
    case VERI_REDXOR :
    case VERI_REDXNOR :
    case VERI_MUL :
    case VERI_EQUAL:
    case VERI_INC_OP :
    case VERI_DEC_OP :
    case VERI_PLUS_ASSIGN :
    case VERI_MIN_ASSIGN :
    case VERI_DIV_ASSIGN :
    case VERI_MOD_ASSIGN :
    case VERI_AND_ASSIGN :
    case VERI_OR_ASSIGN :
    case VERI_XOR_ASSIGN :
    case VERI_EDGE_AMPERSAND :
    case VERI_MUL_ASSIGN :
        break ;

    case VERI_LOGAND :
    case VERI_LOGOR :
    case VERI_RIGHTARROW : // SV-2009 "->"
    case VERI_LOG_EQUIVALENCE : // SV-2009 "<->"
        // VIPER 1586 fix : Logical &&, ||, and ! operands are self-determined
        lsize = rsize = 0 ; // These are self-determined
        lsign = rsign = 0 ;
        break ;

    case VERI_LOGEQ :
    case VERI_LOGNEQ :
    case VERI_CASEEQ :
    case VERI_CASENEQ :
    case VERI_LT :
    case VERI_GT :
    case VERI_GEQ :
    case VERI_LEQ :
    case VERI_WILDEQUALITY:
    case VERI_WILDINEQUALITY:
    {
        // Verilog 2000 changed semantics for all these operators.
        // Used to be self-detemined, but that changed to MAX(lsize,rsize).  Return size
        // of these operators is fixed 1 bit, we should always determine left and right size.
        int lss = _left ? (int)_left->StaticSizeSignInternal(n, 0 /* target_constraint? */) : 0 ;
        int rss = _right ? (int)_right->StaticSizeSignInternal(n, 0 /* target_constraint? */) : 0 ;
        lsize = GET_CONTEXT_SIZE(lss) ;
        rsize = GET_CONTEXT_SIZE(rss) ;

        // VIPER #5048: Handling real operands: they do not contribute to evaluation size:
        if (real_lhs) lsize = 0 ;
        if (real_rhs) rsize = 0 ;

        lsize = rsize = MAX(lsize, rsize) ;
        lsign = (_left) ? GET_CONTEXT_SIGN(lss) : 0 ;
        rsign = (_right) ? GET_CONTEXT_SIGN(rss) : 0 ;
        unsigned sign = (lsign && rsign) ? 1 : 0 ;
        lsign = rsign = sign ;
        break ;
    }
    case VERI_POWER :
    case VERI_LSHIFT :
    case VERI_RSHIFT :
    case VERI_ARITLSHIFT :
    case VERI_ARITRSHIFT :
    case VERI_LSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
    {
        rsize = 0 ; // self-determined
        rsign = 0 ;
        break ;
    }
    case VERI_LEADTO:
    case VERI_COLON_EQUAL:
    case VERI_COLON_NEQUAL:
    case VERI_DISABLE:
    case VERI_OVERLAPPED_IMPLICATION:
    case VERI_NON_OVERLAPPED_IMPLICATION:
    case VERI_OVERLAPPED_FOLLOWED_BY:
    case VERI_NON_OVERLAPPED_FOLLOWED_BY:
    case VERI_CONSECUTIVE_REPEAT:
    case VERI_NON_CONSECUTIVE_REPEAT:
    case VERI_GOTO_REPEAT:
    case VERI_OR:
    case VERI_INTERSECT:
    case VERI_AND:
    case VERI_THROUGHOUT:
    case VERI_WITHIN: // fall through to the warning message
    default:
        if (!expect_nonconst) Warning("%s is not supported for static elaboration", PrintToken(_oper_type)) ;
        return 0 ;
    }

    // Evaluate left hand side and right hand side with the calculated context size.
    // Keep in mind that one of the operands may be 0 for increment/decrement operators!
    int lsize_sign = MAKE_CONTEXT_SIZE_SIGN(lsize,lsign) ;
    VeriBaseValue *l_val = (_left) ? _left->StaticEvaluateInternal(lsize_sign, n, 0, expect_nonconst) : 0 ;
    if (expect_nonconst && _left && !l_val) return 0 ; // Cannot evaluate left operand

    // IEEE 1800 (2009) LRM section 11.4.7:
    //   The && and || operators shall use short circuit evaluation as follows:
    //     — The first operand expression shall always be evaluated.
    //     — For &&, if the first operand value is logically false then the second operand shall not be evaluated.
    //     — For ||, if the first operand value is logically true then the second operand shall not be evaluated.
    if (l_val /* value is always constant */) {
        VeriInteger zero(0) ;
        if (l_val->IsEqual(&zero, 1 /* do not check type */)) {
            if (_oper_type==VERI_LOGAND) {
                // VIPER #7061: Result is single bit constant 0:
                delete l_val ;
                return new VeriBasedNumber("1'b0") ;
            }
        } else if (!l_val->HasXZ()) {
            if (_oper_type==VERI_LOGOR) {
                // VIPER #7061: Result is single bit constant 1:
                delete l_val ;
                return new VeriBasedNumber("1'b1") ;
            }
        }
    }

    int rsize_sign = MAKE_CONTEXT_SIZE_SIGN(rsize,rsign) ;
    VeriBaseValue *r_val = (_right) ? _right->StaticEvaluateInternal(rsize_sign, n, 0, expect_nonconst) : 0 ;
    if (_right && !r_val) { // Cannot evaluate right operand
        delete l_val ; return 0 ;
    }

    // If any of operand is enum type value, operation is to be performed on the
    // value this enum literal. So convert enum value to appropriate type
    if (l_val && l_val->Type() == ENUM) {
        VeriBaseValue *init_val = l_val->GetValue() ;
        delete l_val ;
        l_val = init_val ;
    }
    if (r_val && r_val->Type() == ENUM) {
        VeriBaseValue *init_val = r_val->GetValue() ;
        delete r_val ;
        r_val = init_val ;
    }
    // FIXME: Currently we don't correctly evaluate expressions like i = (i++ + i++)

    // If it is increment or decrement operator, it may be pre or post type. By default,
    // we assume it to be a post increment operator.
    unsigned post = 1 ;

    // Now check if the operand is ++ or --. Then we have to evaluate it return the
    // current value or the evaluated value depending on whether it is post or pre operation!
    if ((_oper_type == VERI_INC_OP) || (_oper_type == VERI_DEC_OP)) {
        // If it is a pre increment or decrement operator then _left is '0'. If it
        // is post increment or decrement operator then _right is '0'. If it is the
        // right hand side that is '0' we swap that with the left hand side, so that
        // we can use the normal addition and subtraction operators for this pre or post
        // increment or decrement operator.  We create a VeriBasedNumber with value 1'b1 for the right hand side.
        if (_right) {
            // So, it is a pre increment or decrement operator.  Swap it now, otherwise '--b' could not be evaluated this way!
            VERIFIC_ASSERT(!l_val) ; // Just to be sure
            l_val = r_val ;
            lsign = rsign ;
            post = 0 ;
        }
        r_val = new VeriBasedNumber("1'b1") ;
        rsign = 0 ;
        // So, now we have both l_val and r_val. r_val is 1.
    }

    // VIPER #3004: Adjust the size of the two values to the context size. Only do this
    // for based number and integers. Do not try to adjust size of real values. Also
    // Do not adjust size of integers into 32 bit (they are itself of 32 bit size):
    // VIPER #3176: Adjust to the context size with sign of the corresponding operands.
    // Don't use the sign of the operands to extend. Adjusted the Adjust routine so that
    // it adjusts only when required. But do not adjust the string values, since strings
    // are dynamically sized adjusting them may lead to incorrect results:
    if (lsize && l_val && (l_val->Type() != ASCIISTR)) l_val = l_val->Adjust(lsize, lsign, 0, 0) ;
    if (rsize && r_val && (r_val->Type() != ASCIISTR)) r_val = r_val->Adjust(rsize, rsign, 0, 0) ;

    // Further processing is done to evaluate the binary operator.

    VeriBaseValue *val = 0 ;
    // VIPER #2561: Allow 'VERI_EQUAL_ASSIGN' operator to have LHS value 0 for un-initialized targets:
    if ((_oper_type == VERI_EQUAL) || (_oper_type == VERI_EQUAL_ASSIGN)) {
        // But it must have valid RHS value, check it:
        if (!r_val) {
            delete l_val ;
            return val ;
        }
        val = r_val ;
    } else if ((!l_val) || (!r_val)) { // All other operator must have valid LHS and RHS value
        delete l_val ;
        delete r_val ;
        return val ;
    } else {
        val = VeriBinaryOperator::StaticEvaluateBinary(l_val, r_val, context_size, _oper_type) ;
        delete r_val ;
    }
    delete l_val ;

    // Here check for the assignment, ++ and -- operators. They also changes the value
    // itself. So we have to overwrite its current value in the value table.
    switch (_oper_type) {
    case VERI_INC_OP:
    case VERI_DEC_OP: // This is an intentional fall through!
        {
            VeriIdDef *id = 0 ;
            if (post) {
                // Post increment or decrement operator, get the id of the left hand side
                id = (_left) ? _left->GetId() : 0 ;
                // The old value should be returned, get it from the table.
                VeriBaseValue *old_val = (n) ? n->FetchValue(id): 0 ;
                VeriBaseValue *ret_val = (old_val) ? old_val->Copy() : 0 ;
                // The new value should be inserted into the value table.
                if (n && !n->Insert(id, val)) {
                    delete val ;
                    return 0 ;
                }
                // Free it up to avoid memory leak
                delete val ;
                val = ret_val ; // This previous value will be returned
            } else {
                // Pre increment or decrement operator, get the id of the right hand side
                id = (_right) ? _right->GetId() : 0 ;
                // The new value should be inserted into the value table.
                if (n && !n->Insert(id, val)) {
                    delete val ;
                    return 0 ;
                }
            }
        }
        break ;
    case VERI_EQUAL: // though VERI_EQUAL and VERI_EQUAL_ASSIGN are not SV specific operator
    case VERI_EQUAL_ASSIGN: // they can only be used here in expression only in SV.
    case VERI_PLUS_ASSIGN:
    case VERI_MIN_ASSIGN:
    case VERI_MUL_ASSIGN:
    case VERI_DIV_ASSIGN:
    case VERI_MOD_ASSIGN:
    case VERI_AND_ASSIGN:
    case VERI_OR_ASSIGN:
    case VERI_XOR_ASSIGN:
    case VERI_LSHIFT_ASSIGN:
    case VERI_ALSHIFT_ASSIGN:
    case VERI_RSHIFT_ASSIGN:
    case VERI_ARSHIFT_ASSIGN: // This is an intentional fall through!
        {
            // We must insert the new value into the value table, since it is an
            // assignment operatoion also. Get the id of the left hand operand
            VeriIdDef *id = (_left) ? _left->GetId() : 0 ;
            // Insert the new value into the table
            if (n && !n->Insert(id, val)) {
                delete val ;
                return 0 ;
            }
        }
        break ;
    default:
        // Nothing to do!
        break ;
    }

    return val ;
}

/* static */
VeriBaseValue* VeriBinaryOperator::StaticEvaluateBinary(VeriBaseValue *l_val, VeriBaseValue *r_val, int context_size, unsigned oper_type)
{
    if (!l_val || !r_val) return 0 ;

    VeriBaseValue *val = 0 ;
    VeriBaseValue *new_val2 = 0 ;
    veri_value_type ret_type = INTEGER ;

    // Get expression size/sign from context_size
    unsigned ret_size = GET_CONTEXT_SIZE(context_size) ;
    unsigned ret_sign = GET_CONTEXT_SIGN(context_size) ;

    // If any one of the argument value is a VeriMinTypMax value it is ensured both
    // of them are VeriMinTypMax values. This is done to facilitate the computations.
    if ((l_val->Type() == MINTYPMAX) && (r_val->Type() != MINTYPMAX)) {
        new_val2 = new VeriMinTypMaxValue(r_val) ;
    } else if ((r_val->Type() == MINTYPMAX) && (l_val->Type() != MINTYPMAX)) {
        new_val2 = new VeriMinTypMaxValue(l_val) ;
    }

    // If any of the argument values are VeriMinTypMax, this function is called
    // recursively, with corresponding values of both the VeriMinTypMax values.
    if ((l_val->Type() == MINTYPMAX) || (r_val->Type() == MINTYPMAX)) {
        VeriBaseValue *min1 = 0 ;
        VeriBaseValue *typ1 = 0 ;
        VeriBaseValue *max1 = 0 ;

        if (l_val->Type() == MINTYPMAX) {
            min1 = l_val->GetMinimum() ;
            typ1 = l_val->GetTypical() ;
            max1 = l_val->GetMaximum() ;
        } else if (new_val2) {
            min1 = new_val2->GetMinimum() ;
            typ1 = new_val2->GetTypical() ;
            max1 = new_val2->GetMaximum() ;
        }

        VeriBaseValue *min2 = 0 ;
        VeriBaseValue *typ2 = 0 ;
        VeriBaseValue *max2 = 0 ;

        if (r_val->Type() == MINTYPMAX) {
            min2 = r_val->GetMinimum() ;
            typ2 = r_val->GetTypical() ;
            max2 = r_val->GetMaximum() ;
        } else if (new_val2) {
            min2 = new_val2->GetMinimum() ;
            typ2 = new_val2->GetTypical() ;
            max2 = new_val2->GetMaximum() ;
        }

        VeriBaseValue *new_min = StaticEvaluateBinary(min1, min2, context_size, oper_type) ;
        VeriBaseValue *new_typ = StaticEvaluateBinary(typ1, typ2, context_size, oper_type) ;
        VeriBaseValue *new_max = StaticEvaluateBinary(max1, max2, context_size, oper_type) ;
        delete new_val2 ;

        if (new_min && new_typ && new_max) val = new VeriMinTypMaxValue(new_min, new_typ, new_max) ;

        delete new_min ;
        delete new_typ ;
        delete new_max ;

        return val ;
    }

    delete new_val2 ; // fix mem leak - we don't use this from here on out
    new_val2 = 0 ; // tie dangling pointer
    double ld_val = 0.0 ;
    double rd_val = 0.0 ;
    double ret_dval = 0.0 ;
    char *ret_str = 0 ;
    unsigned b_real = 0 ;

    // Evaluating the return type of the binary operator.
    if ((l_val->Type() == INTEGER) && (r_val->Type() == INTEGER)) {
        ret_type = INTEGER ;
    } else if ((l_val->Type() == REAL) || (r_val->Type() == REAL)) {
        // If any of the operands is real, computations cannot be done in string domain,
        // it has to be done in floating domain.
        ret_type = REAL ;
        ld_val = l_val->GetRealValue() ;
        rd_val = r_val->GetRealValue() ;
        b_real = 1 ;
    } else {
        ret_type = BASEDNUM ;
    }
    // VIPER #5908 : When size of binary expression is 1, use BASEDNUM as return type
    // instead of INTEGER
    switch(oper_type) {
    case VERI_LOGEQ:
    case VERI_LOGNEQ:
    case VERI_CASEEQ:
    case VERI_CASENEQ:
    case VERI_LT:
    case VERI_GT:
    case VERI_GEQ:
    case VERI_LEQ:
        ret_type = BASEDNUM ; break ;
    default : break ;
    }

    // Fetching the binary values of the lhs and rhs taking care of the signedness of the lhs and rhs.
    unsigned l_sign = 0 ;
    char *l_str = l_val->GetBinaryValue(&l_sign) ;
    unsigned r_sign = 0 ;
    char *r_str = r_val->GetBinaryValue(&r_sign) ;

    if (((!l_str) || (!r_str)) && (!b_real)) {
        Strings::free(l_str) ;
        Strings::free(r_str) ;
        //Error("Non-constant binary operator\n") ;
        return 0 ;
    }

    unsigned b_sign = ((l_sign == 1) && (r_sign == 1)) ;
    // Adjust size of arguments
    switch(oper_type) {
    case VERI_LOGAND :
    case VERI_LOGOR :
    case VERI_RIGHTARROW : // SV-2009 "->"
    case VERI_LOG_EQUIVALENCE : // SV-2009 "<->"
        break ;
    case VERI_LSHIFT :
    case VERI_RSHIFT :
    case VERI_ARITLSHIFT :
    case VERI_ARITRSHIFT :
    case VERI_LSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
        b_sign = l_sign ;
        break ; // Evaluate in self size
    default :
        if (!b_real) {
            unsigned l_len = (unsigned)Strings::len(l_str) ;
            unsigned r_len = (unsigned)Strings::len(r_str) ;
            if (l_len > r_len) {
                char *tmp = r_str ;
                r_str = ConstStrEvaluation::GetPaddedString(r_str, l_len, r_sign) ;
                Strings::free(tmp) ;
            }
            if (r_len > l_len) {
                char *tmp = l_str ;
                l_str = ConstStrEvaluation::GetPaddedString(l_str, r_len, l_sign) ;
                Strings::free(tmp) ;
            }
            // Do the necessary padding.
            if (l_str && (ret_size > (unsigned)Strings::len(l_str))) {
                char *tmp = l_str ;
                l_str = ConstStrEvaluation::GetPaddedString(l_str, ret_size, l_sign) ;
                Strings::free(tmp) ;
            }
            if (r_str && (ret_size > (unsigned)Strings::len(r_str))) {
                char *tmp = r_str ;
                r_str = ConstStrEvaluation::GetPaddedString(r_str, ret_size, r_sign) ;
                Strings::free(tmp) ;
            }
        }
        break ;
    }

    switch (oper_type) {
    case VERI_PLUS:
    case VERI_PLUS_ASSIGN:
    case VERI_INC_OP: // This is an intentional fall through!
        // if it is an increment operator, we have already set the right hand side
        // to 1, so just perform an addition here!

        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = ld_val + rd_val ;
            break ;
        }

        // If any of the operands has 'x' or 'z', return value should also be a string of 'x' of required size.
        if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // Perform the adding operation in String domain.
            ret_str = ConstStrEvaluation::StrAdd(l_str, r_str, '0') ;
        }
        break ;
    case VERI_MIN:
    case VERI_MIN_ASSIGN:
    case VERI_DEC_OP: // This is an intentional fall through!
        // if it is a decrement operator, we have already set the right hand side to 1,
        // so just perform an subtraction here!

        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = ld_val - rd_val ;
            break ;
        }
        // If any of the operands has 'x' or 'z', return value should also be a string of 'x' of required size.
        if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // Perform the subtracting operation in String domain.
            ret_str = ConstStrEvaluation::StrSub(l_str, r_str, '0') ;
        }
        break ;
    case VERI_MUL:
    case VERI_MUL_ASSIGN:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = ld_val * rd_val ;
            break ;
        }

        // If any of the operands has 'x' or 'z', return value should also be a string of 'x' of required size.
        if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // Perform the multiplication in String domain.
            if (ret_sign) {
                ret_str = ConstStrEvaluation::StrSignedMultiply(l_str, r_str) ;
            } else {
                ret_str = ConstStrEvaluation::StrMultiply(l_str, r_str) ;
            }
        }
        break ;
    case VERI_DIV:
    case VERI_DIV_ASSIGN:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            // First check for division by zero in real domain:
            // VIPER #5248 (test63): No need to check for division by zero, just do it.
            // Real domain supports division by zero and it stores/interprets as 'inf'.
            //if (rd_val==0.0) { // FIXME: (rd_val==0.0) is not portable. Should do: fabs(rd_val) < 0.0001
            //    // Cannot calculate in real domain. Standard simulators return 'mfinf'. But we can't.
            //    // Cannot even return 'x', since real does not support 'x' (Verilog 95 LRM section 3.9.2).
            //    // Error("division by zero") ;
            //    Strings::free(l_str) ;
            //    Strings::free(r_str) ;
            //    return 0 ;
            //} else {
                ret_dval = ld_val / rd_val ;
            //}
            break ;
        }

        // If any of the operands has 'x' or 'z', return value should also be a string of 'x' of required size.
        if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // Perform the division in String domain.
            ret_str = ConstStrEvaluation::StrDivision(l_str, r_str, ret_sign) ;
        }
        break ;
    case VERI_MODULUS:
    case VERI_MOD_ASSIGN:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '%%' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        // If any of the operands has 'x' or 'z', return value should also be a string of 'x' of required size.
        if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // Perform the modulous operation in String domain.
            ret_str = ConstStrEvaluation::StrMod(l_str, r_str, ret_sign) ;
        }
        break ;
    case VERI_LOGEQ:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            // FIXME: Should do: fabs(ld_val-rd_val) < 0.0001
            ret_dval = (ld_val == rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Checking the logical equality
            ret_str = ConstStrEvaluation::StrEQ(l_str, r_str, VERI_LOGEQ) ;
        }
        break ;
    case VERI_LOGNEQ:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            // FIXME: Should do: fabs(ld_val-rd_val) < 0.0001
            ret_dval = (ld_val != rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Checking the logical inequality
            ret_str = ConstStrEvaluation::StrNEQ(l_str, r_str, VERI_LOGNEQ) ;
        }
        break ;
    case VERI_CASEEQ:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1){
            // FIXME: Should do: fabs(ld_val-rd_val) < 0.0001
            ret_dval = (ld_val == rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Checking the case equality
            ret_str = ConstStrEvaluation::StrEQ(l_str, r_str, VERI_CASEEQ) ;
        }
        break ;
    case VERI_CASENEQ:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1){
            // FIXME: Should do: fabs(ld_val-rd_val) < 0.0001
            ret_dval = (ld_val != rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Checking the case inequality
            ret_str = ConstStrEvaluation::StrNEQ(l_str, r_str, VERI_CASENEQ) ;
        }
        break ;
    case VERI_LOGAND:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (ld_val && rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Performing logical AND in String domain.
            ret_str = ConstStrEvaluation::StrLogicalAND(l_str, r_str) ;
        }
        break ;
    case VERI_LOGOR:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (ld_val || rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Performing logical OR in String domain.
            ret_str = ConstStrEvaluation::StrLogicalOR(l_str, r_str) ;
        }
        break ;
    case VERI_RIGHTARROW : // SV-2009 "->" == !lop || rop
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (!ld_val || rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            char *tmp = ConstStrEvaluation::StrLogicalNOT(l_str) ;
            ret_str = ConstStrEvaluation::StrLogicalOR(tmp, r_str) ;
            Strings::free(tmp) ;
        }
        break ;
    case VERI_LOG_EQUIVALENCE : // SV-2009 "<->" == (!e1 || e2) && (!e2 || e1)
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = ((!ld_val || rd_val) && (!rd_val || ld_val)) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            char *ll = ConstStrEvaluation::StrLogicalNOT(l_str) ;
            char *l = ConstStrEvaluation::StrLogicalOR(ll, r_str) ;
            Strings::free(ll) ;

            char *rr = ConstStrEvaluation::StrLogicalNOT(r_str) ;
            char *r = ConstStrEvaluation::StrLogicalOR(rr, l_str) ;
            Strings::free(rr) ;
            ret_str = ConstStrEvaluation::StrLogicalAND(l, r) ;
            Strings::free(l) ;
            Strings::free(r) ;
        }
        break ;
    case VERI_LT:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (ld_val < rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Performing '<' in String domain.
            // If any of the operands has a bit 'x' or 'z', return value should be a string of 'x' of size 1.
            if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
                ret_str = ConstStrEvaluation::VeriGetXOfWidth(1) ;
            } else {
                ret_str = ConstStrEvaluation::StrLT(l_str, r_str, b_sign) ;
            }
        }
        break ;
    case VERI_GT:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (ld_val > rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Performing '>' in String domain.
            // If any of the operands has a bit 'x' or 'z', return value should be a string of 'x' of size 1.
            if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
                ret_str = ConstStrEvaluation::VeriGetXOfWidth(1) ;
            } else {
                ret_str = ConstStrEvaluation::StrGT(l_str, r_str, b_sign) ;
            }
        }
        break ;
    case VERI_GEQ:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (ld_val >= rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Performing '>=' in String domain.
            // If any of the operands has a bit 'x' or 'z', return value should be a string of 'x' of size 1.
            if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
                ret_str = ConstStrEvaluation::VeriGetXOfWidth(1) ;
            } else {
                ret_str = ConstStrEvaluation::StrGTE(l_str, r_str, b_sign) ;
            }
        }
        break ;
    case VERI_LEQ:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            ret_dval = (ld_val <= rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Performing '<=' in String domain.
            // If any of the operands has a bit 'x' or 'z', return value should be a string of 'x' of size 1.
            if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
                ret_str = ConstStrEvaluation::VeriGetXOfWidth(1) ;
            } else {
                ret_str = ConstStrEvaluation::StrLTE(l_str, r_str, b_sign) ;
            }
        }
        break ;
    case VERI_REDAND:
    case VERI_AND_ASSIGN:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '&' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        // Performing reduction AND in String domain.
        ret_str = ConstStrEvaluation::StrBitAnd(l_str, r_str) ;
        break ;
    case VERI_REDOR:
    case VERI_OR_ASSIGN:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '|' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        // Performing reduction OR in String domain.
        ret_str = ConstStrEvaluation::StrBitOr(l_str, r_str) ;
        break ;
    case VERI_REDXOR:
    case VERI_XOR_ASSIGN:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '^' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        // Performing reduction XOR in String domain.
        ret_str = ConstStrEvaluation::StrBitXor(l_str, r_str) ;
        break ;
    case VERI_REDXNOR:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '~^' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        // Performing reduction XNOR in String domain.
        ret_str = ConstStrEvaluation::StrBitXnor(l_str, r_str) ;
        break ;
    case VERI_LSHIFT:
    case VERI_LSHIFT_ASSIGN:
    {
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '<<' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        if (ConstStrEvaluation::StrHasXZ(r_str)) { // Right operand contains x, return x
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // VIPER #7977 : IEEE 1800-2012 section 11.4.10 : The right operand
            // should be always treated as unsigned number
            int shift_width = ConstStrEvaluation::BinToInt(r_str, 0 /* unsigned*/) ;
            // Check if we are to shift by a positive width
            // VIPER #3804: Shift amount is always interpreted as an unsigned value, no need to check:
            //if (shift_width < 0) {
            //    Strings::free(l_str) ;
            //    Strings::free(r_str) ;
            //    return 0 ;
            //}
            // Performing left shift in String domain.
            ret_str = ConstStrEvaluation::StrShift(l_str, 1, (unsigned)shift_width, '0') ;
        }
        break ;
    }
    case VERI_RSHIFT:
    case VERI_RSHIFT_ASSIGN:
    {
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '>>' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        if (ConstStrEvaluation::StrHasXZ(r_str)) { // Right operand contains x, return x
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // VIPER #7977 : IEEE 1800-2012 section 11.4.10 : The right operand
            // shoule be always treated as unsigned number
            int shift_width = ConstStrEvaluation::BinToInt(r_str, 0 /* unsigned*/) ;
            // Check if we are to shift by a positive width
            // VIPER #3804: Shift amount is always interpreted as an unsigned value, no need to check:
            //if (shift_width < 0) {
            //    Strings::free(l_str) ;
            //    Strings::free(r_str) ;
            //    return 0 ;
            //}
            // Performing right shift in String domain.
            ret_str = ConstStrEvaluation::StrShift(l_str, 0, (unsigned)shift_width, '0') ;
        }
        break ;
    }
    case VERI_POWER:
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            // Directly use the 'pow()' function:
            ret_dval = pow(ld_val, rd_val) ;
            break ;
        }
        // If any of the operands has a bit 'x' or 'z', return value should be a string of 'x' of required size.
        if (ConstStrEvaluation::StrHasXZ(l_str) || ConstStrEvaluation::StrHasXZ(r_str)) {
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // Performing power operation.
            ret_str = ConstStrEvaluation::StrExp(l_str, r_str, l_sign, r_sign) ;
        }
        break ;
    case VERI_ARITLSHIFT:
    case VERI_ALSHIFT_ASSIGN:
    {
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            //Error("Operator '<<<' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        if (ConstStrEvaluation::StrHasXZ(r_str)) { // Right operand contains x, return x
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // VIPER #7977 : IEEE 1800-2012 section 11.4.10 : The right operand
            // should be always treated as unsigned number
            int shift_width = ConstStrEvaluation::BinToInt(r_str, 0 /* unsigned*/) ;
            // Check if we are to shift by a positive width
            // VIPER #3804: Shift amount is always interpreted as an unsigned value, no need to check:
            //if (shift_width < 0) {
            //    Strings::free(l_str) ;
            //    Strings::free(r_str) ;
            //    return 0 ;
            //}
            // Performing arithmatic left shift.
            ret_str = ConstStrEvaluation::StrShift(l_str, 1, (unsigned)shift_width, '0') ;
        }
        break ;
    }
    case VERI_ARITRSHIFT:
    case VERI_ARSHIFT_ASSIGN:
    {
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real) {
            //Error("Operator '>>>' does not operate on real numbers.\n") ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ;
        }
        if (ConstStrEvaluation::StrHasXZ(r_str)) { // Right operand contains x, return x
            ret_str = ConstStrEvaluation::VeriGetXOfWidth(ret_size) ;
        } else {
            // VIPER #7977 : IEEE 1800-2012 section 11.4.10 : The right operand
            // should be always treated as unsigned number
            int shift_by = ConstStrEvaluation::BinToInt(r_str, 0 /* unsigned*/) ;
            // Check if we are to shift by a positive width
            // VIPER #3804: Shift amount is always interpreted as an unsigned value, no need to check:
            //if (shift_by < 0) {
            //    Strings::free(l_str) ;
            //    Strings::free(r_str) ;
            //    return 0 ;
            //}

            // Performing arithmatic right shift, considering the sign of left hand side.
            if (b_sign) {
                ret_str = ConstStrEvaluation::StrShift(l_str, 0, (unsigned)shift_by, (l_str) ? l_str[0]: '0') ;
            } else {
                ret_str = ConstStrEvaluation::StrShift(l_str, 0, (unsigned)shift_by, '0') ;
            }
        }
        break ;
    }
    case VERI_WILDEQUALITY: // _left =?= _right. X or Z matches to anything (act as wild cards!)
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            // FIXME: Should do: fabs(ld_val-rd_val) < 0.0001
            ret_dval = (ld_val == rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Checking the wild equality
            ret_str = ConstStrEvaluation::StrEQ(l_str, r_str, VERI_WILDEQUALITY) ;
        }
        break ;
    case VERI_WILDINEQUALITY: // _left !?= _right. X or Z matches to anything (act as wild cards!)
        // At least one of the operand is real, so performing the required operation in real domain.
        if (b_real == 1) {
            // FIXME: Should do: fabs(ld_val-rd_val) < 0.0001
            ret_dval = (ld_val != rd_val) ;
            char *suffix = Strings::itoa((int)ret_dval) ;
            char *tmp_str = Strings::save("1'b", suffix) ;
            val = new VeriBasedNumber(tmp_str) ;
            Strings::free(suffix) ;
            Strings::free(tmp_str) ;
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return val ;
        } else {
            // Checking the wild inequality
            ret_str = ConstStrEvaluation::StrNEQ(l_str, r_str, VERI_WILDINEQUALITY) ;
        }
        break ;

/*    // SV assert
    case VERI_LEADTO:
    case VERI_COLON_EQUAL:
    case VERI_COLON_NEQUAL:
    case VERI_DISABLE:
    case VERI_OVERLAPPED_IMPLICATION:
    case VERI_NON_OVERLAPPED_IMPLICATION:
    case VERI_OVERLAPPED_FOLLOWED_BY:
    case VERI_NON_OVERLAPPED_FOLLOWED_BY:
    case VERI_CONSECUTIVE_REPEAT:
    case VERI_NON_CONSECUTIVE_REPEAT:
    case VERI_GOTO_REPEAT:
    case VERI_OR:
    case VERI_THROUGHOUT:
    case VERI_WITHIN:

    // event control or SV assert
    case VERI_OR:

    // constraint
    case VERI_INSIDE:

    // PSL
    case VERI_PSL_PREV: */

    default:
        break ;
    }

    if (b_real == 0) {
        // VIPER #3770: Bail out early if we can't evaluate the operator above
        if (!ret_str) {
            Strings::free(l_str) ;
            Strings::free(r_str) ;
            return 0 ; // Something is wrong
        }

        unsigned len = (unsigned)Strings::len(ret_str) ;

        // Do necessary padding.
        if (len != ret_size) {
            char *tmp = ret_str ;
            ret_str = ConstStrEvaluation::GetPaddedString(tmp, ret_size, ret_sign) ;
            Strings::free(tmp) ;
        }
    }

    // According to the return type, create return values.
    switch(ret_type) {
    case REAL:
        val = new VeriReal(ret_dval) ;
        break ;
    case INTEGER:
        if (!ConstStrEvaluation::StrHasXZ(ret_str)) {
            val = new VeriInteger(ConstStrEvaluation::BinToInt(ret_str, ret_sign)) ;
            break ;
        } // Else when the value contains x/z, fall-through to based number :-)

    case BASEDNUM:
        {
            unsigned len = (unsigned)Strings::len(ret_str) ;
            char *prefix = Strings::itoa((int)len) ;
            char *val_str = Strings::save(prefix, (ret_sign ? "'sb" : "'b"), ret_str) ;
            val = new VeriBasedNumber(val_str) ;
            Strings::free(val_str) ;
            Strings::free(prefix) ;
            break ;
        }
    default:
        break ;
    }
    Strings::free(l_str) ;
    Strings::free(r_str) ;
    Strings::free(ret_str) ;

    return val ;
}

VeriBaseValue* VeriQuestionColon::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_if_expr) return 0 ;
    VeriBaseValue *val = 0 ;
    // Evaluate the condition in self context (VIPER #4960)
    VeriBaseValue *cond_val = _if_expr ? _if_expr->StaticEvaluateInternal(0 /* self context */, val_table, 0, expect_nonconst) : 0 ;
    // If the condition cannot be evaluated then return 0
    if (!cond_val) return 0 ;

    // VIPER #5839 : Determine the context size and sign for the arguments :
    int arg_size_sign = (context_size) ? (context_size) : (int)StaticSizeSignInternal(val_table, target_constraint) ;
    // VIPER #2887: Check for ambiguous value in the condition:
    if (cond_val->HasXZ()) {
        delete cond_val ;
        // Here, we need to evaluate both 'then' and 'else' part of the operator and need to return
        // the combined values according to IEEE 1364 LRM, section 4.1.13 on "Conditional operator":
        VeriBaseValue *then_val = (_then_expr) ? _then_expr->StaticEvaluateInternal(arg_size_sign, val_table, target_constraint, expect_nonconst) : 0 ;
        VeriBaseValue *else_val = (_else_expr) ? _else_expr->StaticEvaluateInternal(arg_size_sign, val_table, target_constraint, expect_nonconst) : 0 ;

        // LRM says: if one of the operands is real, then the result will be 0:
        if ((then_val && (REAL == then_val->Type())) || (else_val && (REAL == else_val->Type()))) {
            delete then_val ;
            delete else_val ;
            // Returning 'real' 0 from here:
            return (new VeriReal(0.0)) ;
        }

        // Get the string representation of both operands:
        unsigned then_sign = 0 ;
        unsigned else_sign = 0 ;
        char *then_str = (then_val) ? then_val->GetBinaryValue(&then_sign) : 0 ;
        char *else_str = (else_val) ? else_val->GetBinaryValue(&else_sign) : 0 ;
        delete then_val ;
        if (!then_str || !else_str) {
            // Static evaluation failed on the operands, return the else value
            Strings::free(then_str) ;
            Strings::free(else_str) ;
            return else_val ;
        }
        delete else_val ;

        // Need to extend the smaller size operand to match the size of the bigger operand:
        unsigned then_size = Strings::len(then_str) ;
        unsigned else_size = Strings::len(else_str) ;
        unsigned size = MAX(then_size, else_size) ;
        char *tmp = 0 ;
        // IEEE 1364 LRM section 4.1.13 on "Conditional operator" says that the values
        // should be 0 extended, but standard simulators seem to sign extend, so do we:
        if (size != then_size) {
            tmp = then_str ;
            then_str = ConstStrEvaluation::GetPaddedString(then_str, size, then_sign) ;
            Strings::free(tmp) ;
        }
        if (size != else_size) {
            tmp = else_str ;
            else_str = ConstStrEvaluation::GetPaddedString(else_str, size, else_sign) ;
            Strings::free(tmp) ;
        }

        // Now bit by bit combine both the operands to get the result string:
        char *combined_str = ConstStrEvaluation::StrConditionalCombine(then_str, else_str) ;
        Strings::free(then_str) ;
        Strings::free(else_str) ;

        // Honour the context size/sign by returning a value that matches the context:
        // LRM is not clear about the sign part. Standard simulators seem to make the result
        // signed if both the operands are signed:
        unsigned sign = (then_sign && else_sign) ? 1 : 0 ;
        if (context_size != 0) {
            // Take the context sign if it is specified:
            sign = GET_CONTEXT_SIGN(context_size) ;
            if (size != GET_CONTEXT_SIZE(context_size)) {
                // Make the result to be of context size:
                size = GET_CONTEXT_SIZE(context_size) ;
                tmp = combined_str ;
                combined_str = ConstStrEvaluation::GetPaddedString(combined_str, size, sign) ;
                Strings::free(tmp) ;
            }
        }

        // Create the value with the value string and with the sign and return:
        char *size_str = Strings::itoa((int)size) ;
        char *val_str = Strings::save(size_str, ((sign) ? "'sb" : "'b"), combined_str) ;
        val = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        Strings::free(size_str) ;
        Strings::free(combined_str) ;
        return val ;
    }

    // VIPER #4960: Apply reduction-OR operator on the evaluated value:
    val = VeriUnaryOperator::StaticEvaluateUnary(cond_val, 1, VERI_REDOR) ;
    delete cond_val ;
    if (!val) return 0 ;
    int c = val->GetIntegerValue();
    delete val ;
    // If the condition is 1, evaluate the 1 expression, else evaluate the 0 expression.
    if (!c) {
        val = (_else_expr) ? _else_expr->StaticEvaluateInternal(arg_size_sign, val_table, target_constraint, expect_nonconst) : 0 ;
    } else {
        val = (_then_expr) ? _then_expr->StaticEvaluateInternal(arg_size_sign, val_table, target_constraint, expect_nonconst) : 0 ;
    }
    return val ;
}

// Nothing to do for VeriEventExpression.
VeriBaseValue* VeriEventExpression::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{
    return 0 ;
}

VeriBaseValue* VeriPortConnect::StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    // SV modyfication : need evaluate the connection. This is required for
    // .named() association of function ports
    return (_connection) ? _connection->StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst) : 0 ;
}

// Nothing to do for VeriPortOpen.
VeriBaseValue* VeriPortOpen::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{ return 0 ; }

// Nothing to do for VeriAnsiPortDecl.
VeriBaseValue* VeriAnsiPortDecl::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{ return 0 ; }

// Nothing to do for VeriTimingCheckEvent.
VeriBaseValue* VeriTimingCheckEvent::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{ return 0 ; }

VeriBaseValue* VeriRange::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{ return 0 ; }

VeriBaseValue* VeriDataType::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{ return 0 ; }

VeriBaseValue* VeriTypeRef::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{ return 0 ; }
VeriBaseValue *VeriIndexedExpr::StaticEvaluateInternal(int /*context_size*/, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_prefix) return 0 ;

    VeriBaseValue *prefix_val = _prefix->StaticEvaluateInternal(0, val_table, target_constraint, expect_nonconst) ;
    if (!prefix_val) return 0 ;

    if (!_idx) return prefix_val ;

    // Size, index of prefix:
    int prefix_size_sign = (int)prefix_val->StaticSizeSign() ;
    unsigned prefix_size = (unsigned)ABS(prefix_size_sign) ;
    int left_bound = (int)prefix_size-1 ;
    int right_bound = 0 ;

    int left_idx = 0 ;
    int right_idx = 0 ;
    // Find the indexed bits:
    if (_idx->IsRange()) {
        // Range is easy, evaluate both the sides:
        unsigned width = _idx->GetWidth(&left_idx, &right_idx, val_table) ;
        if (!width) { delete prefix_val ; return 0 ; }

        // Check if direction is the same as inferred prefix range
        if ((left_bound>right_bound && left_idx<right_idx) ||
            (left_bound<right_bound && left_idx>right_idx)) {
            // Only an error for normal [l:r] ranges. part-select +:/-: etc do not require this test.
            if (!_idx->GetPartSelectToken()) {
                _idx->Error("part-select direction is opposite from prefix index direction") ;
            }
            // Swap the bounds.
            // This can happen legally for +:/-: part-selects, or if somebody suppressed the error above.
            int tmp = left_idx ;
            left_idx = right_idx ;
            right_idx = tmp ;
        }
    } else {
        // Single index, evaluate and take integer value:
        VeriBaseValue *idx_val = _idx->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
        if (!idx_val) return 0 ;
        left_idx = (right_idx = idx_val->GetIntegerValue()) ;
        delete idx_val ;
    }

    // Check for valid indexing:
    if (CHECK_INDEX(left_idx, left_bound, right_bound)) {
        _idx->Warning("index %d is out of range [%d:%d] for %s", left_idx, left_bound, right_bound, "select on concatenation") ;
    } else if (CHECK_INDEX(right_idx, left_bound, right_bound)) {
        _idx->Warning("index %d is out of range [%d:%d] for %s", right_idx, left_bound, right_bound, "select on concatenation") ;
    }

    // Now, index the value
    unsigned value_size = (unsigned)((left_idx>right_idx) ? left_idx-right_idx : right_idx-left_idx) + 1 ;
    unsigned lsb_bit_offset = 0 ;
    // VIPER #6426 : Rectified the offset calculation
    if (left_bound > right_bound) {
        if (right_idx >= right_bound) {
            lsb_bit_offset = (unsigned)(right_idx - right_bound) ;
        } else {
            int x = right_bound - right_idx ;
            x = (int)value_size - x ;
            // VIPER #4543: Check that we are not casting a -ve number to unsigned:
            if (x >= 0) value_size = (unsigned)x ;
        }
    } else {
        if (right_bound >= right_idx) {
            lsb_bit_offset = (unsigned)(right_bound - right_idx) ;
        } else {
            int x = right_idx - right_bound ;
            // VIPER #4543: Check that we are not casting a -ve number to unsigned:
            x = (int)value_size - x ;
            if (x >= 0) value_size = (unsigned)x ;
        }
    }
    VeriBaseValue *result = prefix_val->Evaluate(value_size, lsb_bit_offset) ;
    delete prefix_val ;
    return result ;
}
VeriBaseValue* VeriCast::StaticEvaluateInternal(int /*context_size*/, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    // Evaluate the expression in self determined context size
    // VIPER #8247 : Pass target size/sign to evaluate expression
    // IEEE 1800-2012 section 6.24.1 : If the expression is assignment compatible
    // with the casting type, then the cast shall return the value that a variable
    // of casting type would hold after being assigned the expression.
    int target_size_sign = (int)StaticSizeSignInternal(n, target_constraint) ;
    int expr_size_sign = _expr ? (int)_expr->StaticSizeSignInternal(n, target_constraint) : 0 ;
    int context_size = MAX(ABS(target_size_sign), ABS(expr_size_sign)) ;
    int context_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
    int context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size, context_sign) ;
    VeriBaseValue *val = (_expr) ?  _expr->StaticEvaluateInternal(context_size_sign, n, 0, expect_nonconst) : 0 ;
    if (!val) return 0 ;

    if (!_target_type) return val ;

    if (_target_type->IsConst()) {
        // size-cast. Adjust the size of the returned value :
        int size = _target_type->Integer() ;
        if (!size || size < 0) {
            Error("zero or negative value for size") ; // VCS compliant
            return val ;
        }
        // VIPER #5248 (test_41): Allowed real to be size casted, adjust here:
        if (val->Type() == REAL) {
            verific_int64 ival = (verific_int64)RoundToNearestInteger(val->GetRealValue()) ;
            char *bin_str = ConstStrEvaluation::Int64ToBin(ival) ;
            char *val_str = Strings::save("64'b", bin_str) ; // Size casting will always make it unsigned
            Strings::free(bin_str) ;
            val = new VeriBasedNumber(val_str) ;
            Strings::free(val_str) ;
        }
        return val->Adjust((unsigned)size, 0, 1, 0) ;
    } else if (_target_type->Type()==VERI_SIGNED) {
        // Make value signed using size of _expr
        int size_sign = (_expr) ? (int)_expr->StaticSizeSignInternal(n, target_constraint) : 0 ;
        return val->Adjust(ABS(size_sign), 1, 0, 0) ;
    } else if (_target_type->Type()==VERI_UNSIGNED) {
        int size_sign = (_expr) ? (int)_expr->StaticSizeSignInternal(n, target_constraint) : 0 ;
        // Make value unsigned using size of _expr
        return val->Adjust(ABS(size_sign), 0, 0, 0) ;
    } else if (_target_type->Type()==VERI_STRINGTYPE) {
        // VIPER #4045 : If target type is string convert value to string
        return val->ToAsciiString() ;
    } else if (_target_type->Type()==VERI_CONST) {
        return val ; // Return expression value as it is
    } else if (_target_type->IsConstExpr()) {
        // VIPER #8287: Error out if size of traget type is zero/negative.
        VeriBaseValue *target_val = _target_type->StaticEvaluateInternal(0, 0, 0, 0) ;
        int size = target_val ? target_val->GetIntegerValue() : 0 ;
        delete target_val ;
        if (!size || size < 0) {
            Error("zero or negative value for size") ; // VCS compliant
            return val ;
        }
        return val->Adjust((unsigned)size, 0, 1, 0) ;
    } else {
        // VIPER #8293 : If target type is enum, convert argument specified value
        // to enum literal
        VeriTypeInfo *target_type = _target_type->CheckType(0, NO_ENV) ;
        if (target_type && target_type->IsEnumeratedType()) {
            VeriIdDef *first_id = target_type ? target_type->GetEnumAt(0): 0 ;
            VeriDataType *enum_type = first_id ? first_id->GetDataType(): 0 ;
            verific_int64 enum_context_size = enum_type ? enum_type->StaticSizeSignInternal(0, 0): 0 ;
            verific_uint64 enum_width = GET_CONTEXT_SIZE(enum_context_size) ;
            unsigned is_signed = target_type->IsSigned() ? 1 : 0 ;
            unsigned two_state = target_type->Is2State() ;
            val = val->Adjust((unsigned)enum_width, is_signed /*signed*/, 0, two_state) ;
            char *val_str = val ? val->GetBinaryValue(0): 0 ;

            unsigned enum_count = target_type->NumOfEnums() ;
            unsigned i ;
            VeriIdDef *enum_id, *target_enum_id = 0 ;
            for(i=0; i < enum_count; i++) {
                enum_id = target_type->GetEnumAt(i) ;
                if (!enum_id) continue ;
                VeriExpression *init_val = enum_id->GetInitialValue() ;
                VeriBaseValue *ele_val = init_val ? init_val->StaticEvaluateInternal((int)enum_width, 0, 0, 0): 0 ;
                ele_val = ele_val ? ele_val->Adjust((unsigned)enum_width, is_signed /*signed*/, 0, two_state): 0 ;
                char *ele_val_str = ele_val ? ele_val->GetBinaryValue(0): 0 ;
                if (ele_val_str && val_str && Strings::compare(ele_val_str, val_str)) {
                    target_enum_id = enum_id ;
                    break ;
                }
                Strings::free(ele_val_str) ;
                delete ele_val ;
            }
            Strings::free(val_str) ;
            if (target_enum_id) {
                delete target_type ;
                delete val ;
                return new VeriEnumVal(target_enum_id) ;
            }
        }
        delete target_type ;
        // Otherwise, adjust to the size of the target type
        int size_sign = (int)_target_type->StaticSizeSignInternal(n, target_constraint) ;
        // If target type is of two state, value is to be converted to 2-state
        unsigned is_two_state = 0 ;
        switch(_target_type->Type()) {
        case VERI_INT:
        case VERI_INTEGER: // VIPER #7424
        {
            // Convert value to int
            int int_val = val ? val->GetIntegerValue(): 0 ;
            delete val ;
            return new VeriInteger(int_val) ;
        }
        case VERI_REALTIME:
        case VERI_SHORTREAL:
        case VERI_REAL:
        {
            // Convert value to real
            double real_val = val ? val->GetRealValue(): 0 ;
            delete val ;
            return new VeriReal(real_val) ;
        }
        // VIPER #4754: 'time' is 64 bit (4-state) unsigned, 'shortint' is 16 bit integer
        // and 'longtint' is 64 bit integer. So, do not create real/integer value for them:
        case VERI_TIME: break ;  // 4-state
        case VERI_SHORTINT: // 16 bit 'int'. Converted to based number.
        case VERI_LONGINT:  // 64 bit 'int'. Converted to based number.
        case VERI_BYTE:
        case VERI_BIT: is_two_state = 1 ;  // 2-state

        default : break ;
        }
        // VIPER #5215 : Adjust the value with target size and then apply the
        // target sign on it.
        val = val ? val->Adjust(ABS(size_sign), 0, 1, is_two_state): 0 ;
        if (val) val->ImposeSignState((size_sign < 0) ? 1: 0, is_two_state) ;
        return val ;
    }
    return 0 ;
}
VeriBaseValue* VeriTimeLiteral::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{
    if (!_literal) return 0 ; // No literal, nothing to do
    // For time literal values, multiply time unit by prefix value and create VeriBasedNumber
    double num = 0 ;
    char *unit = 0 ;
    char *str = Strings::save(_literal) ; // Copy literal
    char *prefix = str ;
    // Follow characters until we get [a-zA-Z]
    while(*str) {
        if (isalpha(*str)) {    // Get character : create unit
            unit = str ;        // Get the unit pointer
            char save = *str ;  // Save this character
            *str = '\0' ;       // Terminate the string
            num = Strings::atof(prefix) ; // Calculate the value
            *str = save ;       // Restore the unit character
            break ;
        }
        str++ ;
    }

    // Determine the `timescale (default is nanosecond 'ns'):
    // NOTE: Need to set global timescale using 'veri_file::SetTimeScale(const char *)'
    // to the timescale of the module before elaborating a module and reset it when done:
    char *tick_timescale = veri_file::GetUserTimeScale() ;
    char *str_to_free = tick_timescale ;
    unsigned n = (unsigned)Strings::atoi(tick_timescale) ; // timescale it always unsigned
    double timescale = (n) ? n : 1 ;
    // Divide 1 by it to get the timescale relative to ns.
    // We will make it relative to the timescale specified later:
    timescale = 1.0 / timescale ;

    // Now, determine the timescale according to the unit here:
    while (tick_timescale && isdigit(*tick_timescale)) tick_timescale++ ; // Skip the numbers
    while (tick_timescale && isspace(*tick_timescale)) tick_timescale++ ; // Skip the white spaces

    // Now we are at the time unit of the `timescale:
    char timeunit = (tick_timescale) ? (*tick_timescale) : 'n' ;
    switch (timeunit) {
    case 's': timescale = timescale * 1e-9 ; break ;
    case 'm': timescale = timescale * 1e-6 ; break ;
    case 'u': timescale = timescale * 1e-3 ; break ;
    case 'n': break ; // This is the default
    case 'p': timescale = timescale * 1e+3 ; break ;
    case 'f': timescale = timescale * 1e+6 ; break ;
    default: break ;
    }

    if (unit) {
        switch (*unit) { // Check first character of unit and multiply value with it
        case 's' :
        {
            // CHECKME: Could not understand the 'step'. Seems like a single time unit:
            if (!Strings::compare(unit, "step")) {
                // Only scale for the second, do not do anything for 'step':
                num = num * 1e+9 * timescale ;
            }
            break ;
        }
        case 'm' : num = num * 1e+6 * timescale ; break ;
        case 'u' : num = num * 1e+3 * timescale ; break ;
        case 'n' : num = num        * timescale ; break ;
        case 'p' : num = num * 1e-3 * timescale ; break ;
        case 'f' : num = num * 1e-6 * timescale ; break ;
        default : break ;
        }
    }
    Strings::free(prefix) ;
    Strings::free(str_to_free) ;
    // Cast it to 64 bit value, we used double for the multiplications:
    // CHECKME: Need to use ceil/floor to convert the double to integer.
    verific_uint64 value = (verific_uint64)num ;
    return new VeriBasedNumber(value) ;
}
VeriBaseValue* VeriDollar::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{
    return 0 ; // FIXME: We may need another value class for VeriDollar to check its proper usage
}
VeriBaseValue *VeriExpression::StaticEvaluateForAssignmentPattern(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst, unsigned /*check_validity*/) const
{
    return StaticEvaluateInternal(context_size, val_table, target_constraint, expect_nonconst) ;
}
VeriBaseValue* VeriAssignmentPattern::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    // If assignment pattern contains type information, create constraint from that
    // otherwise use target constraint
    VeriConstraint *type_constraint = (_type) ? _type->EvaluateConstraintInternal(val_table, 0): 0 ;
    VeriConstraint *assignment_constraint = (type_constraint) ? type_constraint: target_constraint ;

    // VIPER #2992 : Assignment pattern applies to unpacked structures for which
    // StaticEvaluateInternal routine should not return a constant value. So, return and
    // array of evaluated expressions instead.
    //if (!assignment_constraint || !_exprs) return VeriConcat::EvaluateParamArrayExpr(context_size, val_table, 0) ;
    if (!assignment_constraint || !_exprs) return VeriConcat::StaticEvaluateUnpackedArrayConcat(0, val_table, expect_nonconst) ;

    delete type_constraint ;
    return StaticEvaluateForAssignmentPattern(context_size, val_table, target_constraint, expect_nonconst, 0) ;
}
VeriBaseValue *VeriAssignmentPattern::StaticEvaluateForAssignmentPattern(int /*context_size*/, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst, unsigned check_validity) const
{
    if (!_exprs || !_exprs->Size()) return 0 ;
    // If assignment pattern contains type information, create constraint from that
    // otherwise use target constraint
    VeriConstraint *type_constraint = (_type) ? _type->EvaluateConstraintInternal(val_table, 0): 0 ;
    VeriConstraint *assignment_constraint = (type_constraint) ? type_constraint: target_constraint ;
    // Get the last element of expression list to determine whether it is named
    // or positional association :
    VeriExpression *expr = (VeriExpression*)_exprs->GetLast() ;

    // VIPER #6764 : First arrange the elements of assignment pattern to process 'member : value'
    // first when target is structure
    Array *rearranged_exprs = new Array(_exprs->Size()) ;
    if (assignment_constraint->IsRecordConstraint() || assignment_constraint->IsArrayConstraint()) {
        Set names(STRING_HASH) ;
        Array mem_index(_exprs->Size()) ;
        Array full_mem_type_match(_exprs->Size()) ;
        unsigned i ;
        VeriExpression *expr2 ;
        FOREACH_ARRAY_ITEM(_exprs, i, expr2) {
            if (expr2 && expr2->IsConcatItem()) {
                VeriExpression *label = expr2->GetMemberLabel() ;
                VeriIdDef *type_id = label ? label->GetId(): 0 ;
                if (!label || (label->IsDataType() || (type_id && type_id->IsType()))) {
                   rearranged_exprs->InsertLast(expr2) ;
                } else { // index : value and member_name : value
                   VeriBaseValue *idx = label->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
                   char *image = 0 ;
                   if (idx) {
                       image = idx->Image() ;
                   } else {
                       image = Strings::save(label->GetName()) ;
                   }
                   if (!names.Insert(image)) {
                       expr2->Warning("assignment pattern key %s is already covered", image) ;
                       Strings::free(image) ;
                   }
                   delete idx ;
                   mem_index.InsertLast(expr2) ;
                   continue ;
                }
            } else {
                if (rearranged_exprs->Size()) rearranged_exprs->Reset() ;
                rearranged_exprs->Append(_exprs) ; // No need to rearrange for positional association
                break ;
            }
        }
        FOREACH_ARRAY_ITEM(&mem_index, i, expr2) {
            if (expr2) rearranged_exprs->InsertLast(expr2) ;
        }
        SetIter si ;
        char *image ;
        FOREACH_SET_ITEM(&names, si, &image) Strings::free(image) ;
    } else {
        rearranged_exprs->Append(_exprs) ;
    }
    VeriBaseValue *result = 0 ;
    Array *values = 0 ;
    if (expr && expr->IsConcatItem()) { // named association
        // If target is unpacked array/structure, create value upto element label,
        // but if target is scalar type (int/shortint), create value in bit label.
        // int x[1:0] = '{0:1, 1:9} ; // Need element label value
        // byte x = '{7:1, default: 0} ; // Need bit-label value
        VeriConstraint *arr_element_constraint = assignment_constraint->ElementConstraint() ;
        //result = assignment_constraint->CreateEmptyValue((assignment_constraint->IsScalarTypeConstraint() || (assignment_constraint->IsArrayConstraint() && assignment_constraint->IsPacked() && (assignment_constraint->PackedDimCount()==1))) ? 1 : 0) ;
        // VIPER #8050 : Create bit-level value when target is packed array having scalar elements
        result = assignment_constraint->CreateEmptyValue((assignment_constraint->IsScalarTypeConstraint() || (assignment_constraint->IsArrayConstraint() && assignment_constraint->IsPacked() && (arr_element_constraint && arr_element_constraint->IsScalarTypeConstraint()))) ? 1 : 0) ;

    } else { // Positional
        // Create an array of null values to store evaluated elements
        unsigned n_elements = _exprs->Size() ;
        values = new Array(n_elements) ;
        unsigned i ;
        for(i = 0 ; i < n_elements; i++) values->InsertLast(0) ;
        result = new VeriArrayValue(values, 0) ;
        if (assignment_constraint->IsRecordConstraint()) result->SetIsStructure() ;
    }

    VeriExpression *default_expr = 0 ; // Store default : value to process last
    unsigned named = 0, positional = 0 ;
    VeriBaseValue *value ;
    VeriConstraint *bit_ele_constraint = 0, *element_constraint = 0 ;
    unsigned i ;
    unsigned cannot_evaluate = 0 ; // VIPER #7424 : Handling of non constant expression
    Set done(POINTER_HASH) ; // Keep track of already full match record/array constraints
    FOREACH_ARRAY_ITEM(rearranged_exprs, i, expr) { // Iterate over elements
        if (!expr) continue ;
        if (expr->IsConcatItem()) { // label : value (named association)
            named = 1 ;
            if (positional) { // pattern contains both positional and named association
                Error("elements of %s cannot be mixed ordered and named", "assignment pattern") ;
                delete result ;
                delete type_constraint ;
                delete rearranged_exprs ; // JJ 130110 need to free reaaranged_exprs before return
                return 0 ; // Stop processing
            }
            if (!expr->GetMemberLabel()) { // (default : value) type element
                if (default_expr) { // Already contains a default
                    expr->Warning("more than one default value found in literal") ;
                    continue ;
                }
                default_expr = expr ;
                continue ;
            }
            expr->StaticEvaluateByChoice(result, val_table, assignment_constraint, expect_nonconst, cannot_evaluate, check_validity, &done) ;
        } else {
            positional = 1 ;
            if (named) { // positional element in named
                Error("elements of %s cannot be mixed ordered and named", "assignment pattern") ;
                delete result ;
                delete type_constraint ;
                delete rearranged_exprs ; // JJ 130110 need to free reaaranged_exprs before return
                return 0 ; // Stop processing
            }
            if (!values) continue ;
            // Extract constraint of the element :
            element_constraint = assignment_constraint->ElementConstraintAt(i) ;
            if (!element_constraint && assignment_constraint->IsScalarTypeConstraint() && !bit_ele_constraint) {
                bit_ele_constraint = assignment_constraint->BitElementConstraint() ;
            }
            if (bit_ele_constraint) element_constraint = bit_ele_constraint ;
            // VIPER #8482: Evaluate expression with proper size and sign
            unsigned label_size = element_constraint ? element_constraint->NumOfBits(): 0 ;
            verific_int64 expr_size_sign = expr->StaticSizeSignInternal(val_table, element_constraint) ;
            unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
            int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
            expr_size = MAX(expr_size, label_size) ;
            expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
            value = expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, val_table, element_constraint, expect_nonconst, check_validity) ;
            if (!value && check_validity) value = new VeriInteger(0) ;
            if (check_validity) expr->CheckConstraint(element_constraint, READ) ;
            if (!value) {
                cannot_evaluate = 1 ; // VIPER #7424
                continue ; // Error some where
            }
            if (element_constraint && _type && !check_validity) value = value->Adjust(element_constraint->NumOfBits(), 0, 1, 0) ;
            values->Insert(i, value) ;
        }
    }
    delete rearranged_exprs ;
    delete bit_ele_constraint ;
    // Lastly process default element
    if (default_expr) default_expr->StaticEvaluateByChoice(result, val_table, assignment_constraint, expect_nonconst, cannot_evaluate, check_validity, 0) ;
    if (named) { // Named association
        if (result && result->IsAnyFieldEmpty()) {
            cannot_evaluate = 1 ;
            // VIPER #7424: Produce error only when we are processing constant expression
            if (!expect_nonconst || check_validity) Error("some element(s) in %s is missing", "assignment pattern") ;
        }
    } else { // Positional : Check element count
        // VCS Compatibility issue (Params_module_port): If target is dynamic array, do not check length
        // mismatch, as target will get value length
        if (!assignment_constraint->IsDynamicArrayConstraint() && !assignment_constraint->IsEnumConstraint() && assignment_constraint->Length() && (assignment_constraint->Length() != _exprs->Size())) {
            Error("expression has %d elements; expected %d", _exprs->Size(), assignment_constraint->Length()) ;
        }
        if (!assignment_constraint->IsEnumConstraint() && assignment_constraint->IsScalarTypeConstraint() && (assignment_constraint->NumOfBits() != _exprs->Size())) {
            Error("expression has %d elements; expected %d", _exprs->Size(), assignment_constraint->NumOfBits()) ;
        }
    }
    // VIPER #7424 : If this expression can be non constant and we cannot evaluate
    // some expressions, return 0
    // VIPER #8207 : Return 0 if we cannot evaluate any expression
    if (cannot_evaluate) {
        delete result ; result = 0 ;
    }
    delete type_constraint ;
    return result ;
}
void VeriExpression::StaticEvaluateByChoice(VeriBaseValue * /*value*/, ValueTable * /*val_table*/, VeriConstraint * /*constraint*/, unsigned /*expect_nonconst*/, unsigned &cannot_evaluate, unsigned check_validity, Set *done)
{ (void) cannot_evaluate ; (void) check_validity ; (void) done ; }
void VeriConcatItem::StaticEvaluateByChoice(VeriBaseValue *value, ValueTable *val_table, VeriConstraint *constraint, unsigned expect_nonconst, unsigned &cannot_evaluate, unsigned check_validity, Set *done)
{
    VeriBaseValue *elem_value = 0 ;
    VeriIdDef *type_id = (_member_label) ? _member_label->GetId(): 0 ;
    // Check whether it is 'default : value' or 'simple_type : value'
    if (!_member_label || (_member_label->IsDataType() || (type_id && type_id->IsType()))) {
        // Evaluate the constraint of label
        VeriConstraint *label_constraint = (_member_label) ? _member_label->EvaluateConstraintInternal(val_table, 0): 0 ;
        // _expr is to be evaluated with this label type, do that
        if (label_constraint) {
            // Moreover popular simulators allow '{enum_type : 1'b1} in assignment pattern.
            // So if target type is enum type, convert value to enum literal
            VeriTypeInfo *target_type = _member_label ? _member_label->CheckType(0, NO_ENV): 0 ;
            if (target_type && target_type->IsEnumeratedType()) {
                VeriMapForCopy old2new ;
                VeriCast cast_expr(_member_label ? _member_label->CopyExpression(old2new):0, _expr ? _expr->CopyExpression(old2new): 0) ;
                elem_value = cast_expr.StaticEvaluateForAssignmentPattern(0,  val_table, label_constraint, expect_nonconst, check_validity) ;
            } else {
                unsigned label_size = label_constraint->NumOfBits() ;
                verific_int64 expr_size_sign = _expr ? _expr->StaticSizeSignInternal(val_table, label_constraint): 0 ;
                unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
                int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
                expr_size = MAX(expr_size, label_size) ;
                expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
                elem_value = (_expr) ? _expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, val_table, label_constraint, expect_nonconst, check_validity): 0 ;
            }
            if (!elem_value && check_validity) elem_value = new VeriInteger(0) ;
            if (check_validity && _expr) _expr->CheckConstraint(label_constraint, READ) ;
            if (!elem_value) {
                cannot_evaluate = 1 ; // VIPER #8207: Propagate information that we cannot evaluate
            }
        }
        // Try to fill all empty spots for default value and particular spots for
        // typed label :
        unsigned ele_pos = 0 ;
        if (!constraint->StaticFillSpecifiedElements(value, _expr, label_constraint, elem_value, ele_pos, val_table, constraint, cannot_evaluate, expect_nonconst, check_validity, done)) {
            // _member_label does not match with any element, error :
            if ((!expect_nonconst || check_validity) && _member_label) Error("illegal concat label for %s", "array/struct literals") ;
        }
        delete label_constraint ;
        delete elem_value ;
        return ;
    }
    // Try to evaluate the member label:
    VeriBaseValue *idx = _member_label->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
    unsigned pos = 0 ;
    if (idx) { // It is index of array
        // Check if this element is in the target constraint range :
        if (!constraint->StaticContains(idx)) {
            if (!expect_nonconst || check_validity) Error("out of range index label for array literal") ;
            delete idx ;
            return ;
        }
        // Find the position where to insert the value
        pos = constraint->PosOf(idx->GetIntegerValue()) ;
        // Check if there is an existing one here
        // Override with new value
        //VeriBaseValue *exist = value->GetValueAt(pos) ;
        //if (exist && !exist->IsEmpty()) {
            //char *image = idx->Image() ;
            //Warning("assignment pattern key %s is already covered", image) ;
            //Strings::free(image) ;
            //delete idx ;
            //return ;
        //}
        delete idx ;
    } else if (_member_label->GetName()) { // It is element of structure
        // Find the position where to insert the value
        pos = constraint->PosOfMember(_member_label->GetName()) ;
        // Check if there is an existing one here
        // Override with new value
        //VeriBaseValue *exist = value->GetValueAt(pos) ;
        //if (exist && !exist->IsEmpty()) {
            //Warning("assignment pattern key %s is already covered", _member_label->GetName()) ;
            //return ;
        //}
    }
    // Get constraint of appropriate position and evaluate the expression:
    VeriConstraint *ele_constraint = constraint->ElementConstraintAt(pos) ;
    VeriConstraint *bit_ele_constraint = 0 ;
    // If target is scalar type, element constraint is bit-element contraint,
    // it returns an allocated one :
    if (constraint->IsScalarTypeConstraint() && !ele_constraint) {
        bit_ele_constraint = constraint->BitElementConstraint() ;
        ele_constraint = bit_ele_constraint ;
    }
    unsigned label_size = ele_constraint ? ele_constraint->NumOfBits(): 0 ;
    verific_int64 expr_size_sign = _expr ? _expr->StaticSizeSignInternal(val_table, ele_constraint): 0 ;
    unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
    int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
    expr_size = MAX(expr_size, label_size) ;
    expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
    elem_value = (_expr) ? _expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, val_table, ele_constraint, expect_nonconst, check_validity): 0 ;
    if (!elem_value && check_validity) elem_value = new VeriInteger(0) ;
    if (check_validity && _expr) _expr->CheckConstraint(ele_constraint, READ) ;

    if (elem_value) {
        value->SetValueAt(elem_value, pos) ;
    } else {
        cannot_evaluate = 1 ; // VIPER #8207: Propagate information that we cannot evaluate
    }
    delete bit_ele_constraint ;
}
VeriBaseValue* VeriMultiAssignmentPattern::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    if (!_repeat || !_exprs) return 0 ;
    // If assignment pattern contains type information, create constraint from that
    // otherwise use target constraint
    VeriConstraint *type_constraint = (_type) ? _type->EvaluateConstraintInternal(val_table, 0): 0 ;
    VeriConstraint *assignment_constraint = (type_constraint) ? type_constraint: target_constraint ;

    // VIPER #2992 : Assignment pattern applies to unpacked structures for which
    // StaticEvaluate routine should not return a constant value. So, return and
    // array of evaluated expressions instead.
    //if (!assignment_constraint) return VeriMultiConcat::EvaluateParamArrayExpr(context_size, val_table, 0) ;
    if (!assignment_constraint) return VeriMultiConcat::StaticEvaluateUnpackedArrayConcat(0, val_table, expect_nonconst) ;
    delete type_constraint ;
    return StaticEvaluateForAssignmentPattern(context_size, val_table, target_constraint, expect_nonconst, 0) ;
}

VeriBaseValue *VeriMultiAssignmentPattern::StaticEvaluateForAssignmentPattern(int /*context_size*/, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst, unsigned check_validity) const
{
    if (!_repeat || !_exprs) return 0 ;
    // If assignment pattern contains type information, create constraint from that
    // otherwise use target constraint
    VeriConstraint *type_constraint = (_type) ? _type->EvaluateConstraintInternal(val_table, 0): 0 ;
    VeriConstraint *assignment_constraint = (type_constraint) ? type_constraint: target_constraint ;

    VeriBaseValue *rep = _repeat->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
    if (!rep) {
        delete type_constraint ;
        return 0 ;
    }
    int loop_count = rep->GetIntegerValue() ;
    delete rep ;
    if (loop_count <= 0) {
        delete type_constraint ;
        return 0 ;
    }
    // Get the last element of expression list to determine whether it is named
    // or positional association :
    VeriExpression *expr = (VeriExpression*)_exprs->GetLast() ;

    VeriBaseValue *result = 0 ;
    Array *values = 0 ;
    if (expr && expr->IsConcatItem()) { // named association
        // If target is unpacked array/structure, create value upto element label,
        // but if target is scalar type (int/shortint), create value in bit label.
        // int x[1:0] = '{0:1, 1:9} ; // Need element label value
        // byte x = '{7:1, default: 0} ; // Need bit-label value
        //result = assignment_constraint->CreateEmptyValue((assignment_constraint->IsScalarTypeConstraint() || assignment_constraint->IsPacked()) ? 1: 0) ;
        // VIPER #8050 : Create bit-level value when target is packed array having scalar elements
        VeriConstraint *arr_element_constraint = assignment_constraint->ElementConstraint() ;
        result = assignment_constraint->CreateEmptyValue((assignment_constraint->IsScalarTypeConstraint() || (assignment_constraint->IsArrayConstraint() && assignment_constraint->IsPacked() && (arr_element_constraint && arr_element_constraint->IsScalarTypeConstraint()))) ? 1 : 0) ;
    } else { // Positional
        // Create an array of null values to store evaluated elements
        unsigned n_elements = (unsigned)loop_count * _exprs->Size() ;
        values = new Array(n_elements) ;
        unsigned i ;
        for(i = 0 ; i < n_elements; i++) values->InsertLast(0) ;
        result = new VeriArrayValue(values, 0) ;
        if (assignment_constraint->IsRecordConstraint()) result->SetIsStructure() ;
    }

    VeriExpression *default_expr = 0 ; // Store default : value to process last
    unsigned named = 0, positional = 0 ;
    VeriBaseValue *value ;
    VeriConstraint *bit_ele_constraint = 0, *element_constraint = 0 ;
    unsigned i, j ;
    int pos = -1 ;
    unsigned cannot_evaluate = 0 ; // VIPER #7424
    Set done(POINTER_HASH) ; // To keep track of already matched items by data-type : value
    for (j = 0 ; j < (unsigned)loop_count; j++) { // Iterate repeat times
        FOREACH_ARRAY_ITEM(_exprs, i, expr) { // Iterate over elements
            if (!expr) continue ;
            pos++ ;
            if (expr->IsConcatItem()) { // label : value (named association)
                named = 1 ;
                if (positional) { // pattern contains both positional and named association
                    Error("elements of %s cannot be mixed ordered and named", "assignment pattern") ;
                    delete result ;
                    delete type_constraint ;
                    return 0 ; // Stop processing
                }
                if (!expr->GetMemberLabel()) { // (default : value) type element
                    if (default_expr) { // Already contains a default
                        expr->Warning("more than one default value found in literal") ;
                        continue ;
                    }
                    default_expr = expr ;
                    continue ;
                }
                expr->StaticEvaluateByChoice(result, val_table, assignment_constraint, expect_nonconst, cannot_evaluate, check_validity, &done) ;
            } else {
                if (!values) continue ;
                positional = 1 ;
                if (named) { // positional element in named
                    Error("elements of %s cannot be mixed ordered and named", "assignment pattern") ;
                    delete result ;
                    delete type_constraint ;
                    return 0 ; // Stop processing
                }
                // Extract constraint of the element :
                element_constraint = assignment_constraint->ElementConstraintAt((unsigned)pos) ;
                if (!element_constraint && assignment_constraint->IsScalarTypeConstraint() && !bit_ele_constraint) {
                    bit_ele_constraint = assignment_constraint->BitElementConstraint() ;
                }
                if (bit_ele_constraint) element_constraint = bit_ele_constraint ;
                // VIPER #8482: Evaluate expression with proper size and sign
                unsigned label_size = element_constraint ? element_constraint->NumOfBits(): 0 ;
                verific_int64 expr_size_sign = expr->StaticSizeSignInternal(val_table, element_constraint) ;
                unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
                int expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
                expr_size = MAX(expr_size, label_size) ;
                expr_size_sign = MAKE_CONTEXT_SIZE_SIGN(expr_size, expr_sign) ;
                value = expr->StaticEvaluateForAssignmentPattern((int)expr_size_sign, val_table, element_constraint, expect_nonconst, check_validity) ;
                if (!value && check_validity) value = new VeriInteger(0) ;
                if (check_validity) expr->CheckConstraint(element_constraint, READ) ;
                if (!value) {
                    cannot_evaluate = 1 ;
                    continue ; // Error some where
                }
                values->Insert((unsigned)pos, value) ;
            }
        }
    }
    delete bit_ele_constraint ;
    // Lastly process default element
    if (default_expr) default_expr->StaticEvaluateByChoice(result, val_table, assignment_constraint, expect_nonconst, cannot_evaluate, check_validity, 0) ;
    if (named) { // Named association
        if (result->IsAnyFieldEmpty()) {
            cannot_evaluate = 1 ;
            if (!expect_nonconst || check_validity) Error("some element(s) in %s is missing", "assignment pattern") ;
        }
    } else { // Positional : Check element count
        if (assignment_constraint->Length() && _exprs && (assignment_constraint->Length() != ((unsigned)loop_count * _exprs->Size()))) {
            Error("expression has %d elements; expected %d", ((unsigned)loop_count * _exprs->Size()), assignment_constraint->Length()) ;
        }
        if (assignment_constraint->IsScalarTypeConstraint() && _exprs && (assignment_constraint->NumOfBits() != ((unsigned)loop_count * _exprs->Size()))) {
            Error("expression has %d elements; expected %d", ((unsigned)loop_count *_exprs->Size()), assignment_constraint->NumOfBits()) ;
        }
    }
    // VIPER #7424 : If this expression can be non constant and we cannot evaluate
    // some expressions, return 0
    // VIPER #8207 : Return 0 if we cannot evaluate some expression
    if (cannot_evaluate) {
        delete result ; result = 0 ;
    }
    delete type_constraint ;
    return result ;
}
VeriBaseValue* VeriStreamingConcat::StaticEvaluateInternal(int context_size, ValueTable *val_table, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    int size_sign = (int)StaticSizeSignInternal(val_table, target_constraint) ;
    unsigned size = GET_CONTEXT_SIZE(size_sign) ;
    unsigned context_len = GET_CONTEXT_SIZE(context_size) ;

    // If stream is larger than target expression, it is an error
    if (context_len && (size > context_len)) {
        Error ("width does not match, stream larger than %s", "target") ;
        return 0 ;
    }
    // Evaluate the stream concatenation
    VeriBaseValue *stream = VeriConcat::StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
    if (!stream) return 0 ;

    unsigned i, pad_amout ;
    unsigned sign = 0 ;
    char *result = 0 ;
    if (_stream_operator == VERI_RSHIFT) {
        if (context_len && (size < context_len)) {
            pad_amout = context_len - size ;
            result = stream->GetBinaryValue(&sign) ;
            delete stream ;
            if (!result) return 0 ; // stream concatenation cannot be evaluated
            char *padd_str = Strings::allocate(pad_amout) ;
            padd_str[0] = 0 ;
            for (i = 0; i < pad_amout; i++) {
                padd_str[i] = '0' ;
            }
            // VIPER #4628 : Terminate the string 'padd_str':
            padd_str[i] = '\0' ;
            char *prefix = Strings::itoa((int)(pad_amout+size)) ;
            char *x = Strings::save(prefix, "'b", result, padd_str) ;
            Strings::free(result) ;
            Strings::free(prefix) ;
            Strings::free(padd_str) ;
            stream = new VeriBasedNumber(x) ;
            Strings::free(x) ;
        }
        return stream ;
    }
    // Get the binary bits from evaluated stream concatenation
    result = stream->GetBinaryValue(&sign) ;
    delete stream ;
    if (!result) return 0 ; // stream concatenation cannot be evaluated

    char *arranged_str = StaticStreamingRight2Left(result, val_table, expect_nonconst) ;
    if (!arranged_str) return 0 ;

    if (context_len) {
        pad_amout = context_len - size ;
        char *x = Strings::allocate(pad_amout) ;
        i = pad_amout ;
        unsigned idx = 0 ;
        while (i != 0) {
            x[idx] = '0' ;
            idx++ ;
            i-- ;
        }
        x[idx] = 0 ;
        char *tmp = arranged_str ;
        arranged_str = Strings::save(arranged_str, x) ;
        Strings::free(tmp) ;
        Strings::free(x) ;
    }
    unsigned str_len = Strings::len(arranged_str) ;
    char *prefix = Strings::itoa((int)(str_len)) ;
    char *x = Strings::save(prefix, "'b", arranged_str) ;
    Strings::free(arranged_str) ;
    Strings::free(prefix) ;
    stream = new VeriBasedNumber(x) ;
    Strings::free(x) ;

    return stream ;
}

// VIPER #4770: Evaluate cond-predicate expression similar to logical-AND binary operator.
// But, do not evaluate the RHS if LHS has failed - this is according to LRM to avoid side effects.
VeriBaseValue *
VeriCondPredicate::StaticEvaluateInternal(int /*context_size*/, ValueTable *n, VeriConstraint * /*target_constraint*/, unsigned expect_nonconst) const
{
    // Both LHS and RHS must be valid:
    if (!_left || !_right) return 0 ;

    // Evaluate left hand side with self-determined size-sign:
    VeriBaseValue *l_val = _left->StaticEvaluateInternal(0 /* size-sign is self-determined */, n, 0, expect_nonconst) ;
    if (expect_nonconst && !l_val) return 0 ;

    // If the operand is enum type value, operation is to be performed on the
    // value of this enum literal. So convert enum value to appropriate type:
    if (l_val->Type() == ENUM) {
        VeriBaseValue *init_val = l_val->GetValue() ;
        delete l_val ;
        l_val = init_val ;
    }

    // Check whether the LHS failed, in that case do not evaluate RHS:
    if (!l_val || !l_val->GetIntegerValue()) {
        delete l_val ;
        return new VeriBasedNumber("1'b0") ; // Evaluated as 'false'
    }

    // Now, evaluate right hand side with self-determined size-sign:
    VeriBaseValue *r_val = _right->StaticEvaluateInternal(0 /* size-sign is self-determined */, n, 0, expect_nonconst) ;
    if (expect_nonconst && !r_val) { delete l_val ; return 0 ; }

    // If the operand is enum type value, operation is to be performed on the
    // value of this enum literal. So convert enum value to appropriate type:
    if (r_val && r_val->Type() == ENUM) {
        VeriBaseValue *init_val = r_val->GetValue() ;
        delete r_val ;
        r_val = init_val ;
    }

    // Check whether the RHS failed:
    if (!r_val || !r_val->GetIntegerValue()) {
        delete l_val ;
        delete r_val ;
        return new VeriBasedNumber("1'b0") ; // Evaluated as 'false'
    }

    // Clean-up:
    delete r_val ;
    delete l_val ;

    return (new VeriBasedNumber("1'b1")) ; // Evaluated as 'true'
}

VeriBaseValue* VeriRealVal::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{
    // For real constant values, nothing special to do, so just creating
    // a new VeriReal, with the existing double value.
    return new VeriReal(_num) ;
}

VeriBaseValue* VeriIntVal::StaticEvaluateInternal(int /*context_size*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, unsigned /*expect_nonconst*/) const
{
    // For real constant values, nothing special to do, so just creating
    // a new VeriIntVal, with the existing integer value.
    return new VeriInteger(_num) ;
}

VeriBaseValue* VeriConstVal::StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned /*expect_nonconst*/) const
{
    unsigned ret_size = 0 ;
    unsigned ret_sign = 0 ;
    if (!context_size) {
        int size_sign = (int)StaticSizeSignInternal(n, target_constraint) ;
        ret_sign = (size_sign < 0) ;
        ret_size = (unsigned)ABS(size_sign) ;
    } else {
        ret_size = (unsigned)ABS(context_size) ;
        // Only 'sign' evaluate if the value itself is signed:
        ret_sign = (_is_signed) ? (context_size < 0) : 0 ;
    }
    // VIPER #7397: Donot use Image() as that will extend the string if there
    // is non printable or escape characters
    // Try to create Image type string without converting escape/non-printable characters
    char *img = 0 ;
    unsigned string_len = 0 ;
#ifdef VERILOG_PRESERVE_LITERALS
    // If we have the original value preserved, return it as the image:
    // Viper #4994 : added _is_string check
    if (_orig_value && !_is_string) img = Strings::save(_orig_value) ;
    // VIPER #3687: Under this compile switch we keep both '_' and '?' in the image.
    // Here we need to get rid of those so that our normal processing can be done:
    if (!_is_string && img) { // JJ: 130521 check img for null prior to deref, coverity fix
        // VIPER #3766: Do this only if it is not a string type constant value.
        // We should not change the string for that, otherwise we do as before.
        char *idx = img ;
        char *src = img ;
        for (; *src != '\0'; src++) {
            if (*src == '_') continue ; // ignore this '_' character
            if (*src == '?') *src = 'z' ; // '?' is a 'z-digit' : LRM 2.5 table.
            *idx++ = *src ;
        }
        *idx = '\0' ;
    }
#endif

    // Similar to NonconstVal::String :
    // Create a char* representation for this value.
    unsigned char r ;
    unsigned i, j ;
    if (!img && _is_string && (_size % 8 == 0) && !_zvalue && !_xvalue) {
        // Value should and can be printed as a Verilog string.
        // The characters are saved in opposite order (MSB-LSB). So swap them.
        // Also, start and end with a ". (Return as a string)
        unsigned len = _size / 8 ;
        if (!_value) return 0 ;
        // VIPER #7397 : Use the string as it is here, do not change the content
        string_len = (len==0) ? 1 : len ;
        // VIPER #4483: Do not use Strings::save(_value), _value can have \0 in it:
        img = Strings::allocate((len==0) ? len+2: len+1) ; // Allocate more spaces for enclosing quotes
        unsigned length = len ;
        i = 0 ;
        while(length-- != 0) {
            r = _value[length] ;
            img[i++] = (char)r ;
        }
        // VIPER #4670 & #4678: Empty string ("") is equivalent to "\0".
        if (len == 0) img[i++] = '\0' ;
        img[i++] = '\0' ;
    } else if (!img) {
        // Otherwise, build a bit-string, one character per bit
        char *buffer = Strings::allocate(_size + 33) ; // The 33 is for taking into consideration the header and a possibly large _size
        if (_is_unsized_bit) {
            // Don't print size and base for SystemVerilog unsized bit literal (#1740)
            ::sprintf(buffer,"'") ;
        } else if (_is_unsized_number) {
            // Don't print size for SystemVerilog unsized bit literal (#1740)
            ::sprintf(buffer,"'%sb", (_is_signed) ? "s" : "") ;
        } else {
            // Print bit-literal header : size (issue 1313) and optional sign
            ::sprintf(buffer,"%d'%sb", _size, (_is_signed) ? "s" : "") ;
        }
        img = Strings::save(buffer) ;

        // Go MSB to LSB. MSB is highest index. 0 the lowest.
        unsigned print_the_rest = 0 ; // Flag used to control printing of remaining bits
        char last_value = 0 ;         // Last bit encountered.  Used to determine when to start printing.
        char current_value ;
        i = _size ;
        j = 0 ;
        while (i-- != 0) {
            // Determine the value of this bit
            if (GET_BIT(_xvalue,i))         { current_value = 'x' ; }
            else if (GET_BIT(_zvalue,i))    { current_value = 'z' ; }
            else if (GET_BIT(_value,i))     { current_value = '1' ; }
            else                            { current_value = '0' ; }
            // Determine if you need to print this bit or trim it.
            if (print_the_rest || (current_value == '1' && !_is_unsized_bit) || (last_value && (last_value != current_value))) { // Don't print anything if it's redundant
                if (!print_the_rest) {
                    // Print last_value if last_value is defined and it's not the same as the current_value
                    if (last_value && (last_value != current_value)) buffer[j++] = last_value ;
                    // Force printing of remainder bits
                    print_the_rest = 1 ;
                }
                buffer[j++] = current_value ;
            }
            last_value = current_value ;
        }
        // If nothing was printed, then print the last_value.  This is the case
        // for all 0's, x's, z's (and 1's only _is_unsized_bit).
        if (!print_the_rest) buffer[j++] = last_value ;

        // Terminate the string properly :
        buffer[j] = '\0' ;
        char *tmp = img ;
        img = Strings::save(img,buffer) ;
        Strings::free(tmp) ;
        Strings::free(buffer) ;
    }
    VeriBaseValue *val = 0 ;
    char *str = 0 ;
    unsigned sign = 0 ;
    // If 'this' is a string, just create a new string with the image.
    if (IsString()) {
        // Image() returns strings encapsulated with "'s if it is a string
        // So, first remove them, if they are there
        //str = (img[0] == '"') ? img+1 : img ;
        //unsigned len = (unsigned)Strings::len(str) ;
        //if (str[len-1] == '"') str[len-1] = '\0' ;
        //if (str[0] == '\0') { // It was an empty string ("")
            //// VIPER #4670 & #4678: Empty string ("") is equivalnet to "\0".
            //// Since it can be indexed later, return "\0" in place of empty string:
            //Strings::free(img) ;
            //img = Strings::save("\0") ;
            //str = img ;
        //}
        // Create the string value with this string
        val = new VeriAsciiString(img, string_len) ;
    } else {
        // If 'this' is a based number, convert from the image to the equivalent binary
        // number, do the nessary padding, and create a new VeriBasedNumber.
        if (_is_unsized_bit) {
            // Call this function with the context size, so that '1 type values get
            // extended properly to proper size.
            str = ConstStrEvaluation::VeriStringToBinary(img, &sign, (int)ret_size) ;
        } else {
            // VIPER #7892 : Do not try to convert const-val with size 0 to binary
            if (_size == 0) {
                Strings::free(img) ; return 0 ;
            }
            // Normal 2'b10 and 'b10 type values are extended properly with
            // 0s or x or z to the proper default proper size
            // VIPER #3064 : Extend 'bx type value to 32 bits for 95 mode only,
            // for 2001 and system verilog mode, extend 'bx type value to context size
            if (VeriNode::IsVeri95()) {
                str = ConstStrEvaluation::VeriStringToBinary(img, &sign) ;
            } else {
                str = ConstStrEvaluation::VeriStringToBinary(img, &sign, (int)ret_size) ;
            }
        }
        if (str == 0) {
            Strings::free(img) ;
            return 0 ;
        }
        unsigned len = (unsigned)Strings::len(str) ;
        if (len != ret_size) {
            char *tmp = str ;
            str = ConstStrEvaluation::GetPaddedString(tmp, ret_size, ret_sign) ;
            Strings::free(tmp) ;
        }
        char *prefix = Strings::itoa((int)ret_size) ;
        // 'ret_sign' determines the signing of the returned value:
        char *bin_str = Strings::save(prefix, ((ret_sign) ? "'sb" : "'b"), str) ;
        val = new VeriBasedNumber(bin_str) ;
        Strings::free(str) ;
        Strings::free(bin_str) ;
        Strings::free(prefix) ;
    }
    Strings::free(img) ;
    return val ;
}

////////////////////////////////////////////////////////////////////
////////////////// Constant function evaluation ////////////////////
////////////////////////////////////////////////////////////////////

VeriBaseValue* VeriFunctionCall::StaticEvaluateInternal(int /*context_size*/, ValueTable *val_table, VeriConstraint * /*target_constraint*/, unsigned expect_nonconst) const
{
    VeriIdDef *id = (_func_name) ? _func_name->GetId() : 0 ;
    VeriBaseValue *ret_val = 0 ;
    if (!id) {
        // It can be a built-in method call, like len() on an string type object!
        ret_val = StaticEvaluateBuiltInMethods(val_table, expect_nonconst) ;
        return ret_val ;
    }

    VeriModuleItem *mod_item = id->GetModuleItem() ;
    ret_val = mod_item ? mod_item->StaticElaborateFunction(_args, val_table, expect_nonconst): 0 ;

    if (val_table) val_table->SetJumpToken(0) ; // Not dead code any more!

    return ret_val ;
}

VeriBaseValue *VeriFunctionCall::StaticEvaluateBuiltInMethods(ValueTable *val_table, unsigned expect_nonconst) const
{
    if (!_func_name) return 0 ;

    // This is supposed to be a built-in method call like len() on string in the format s.len()
    // So, get the object on which the built-in method is called. The object must be resolved
    VeriName *object = _func_name->GetPrefix() ;
    VeriIdDef *object_id = (object) ? object->GetId() : 0 ;
    if (!object_id) return 0 ; // Can't do anything without the actual object!

    // Create an id-ref to reference to this object id, we have to evaluate that
    VeriIdRef object_ref(object_id) ;
    object_ref.SetLinefile(_func_name->Linefile()) ;
    // Now (static) evaluate the value of the object on which the built-in method is called
    VeriBaseValue *object_val = object_ref.StaticEvaluateInternal(0 /* size-sign is self-determined */, val_table, 0, expect_nonconst) ;
    //if (!object_val) return 0 ; // Can't do anything without a valid value of the object!

    // FIXME: The outcome of this routine is undefined in the error condition:
    // SV 3.1 final LRM, page 12, section 3.7.9 says:
    // function integer atoi() - The string is converted until the first non-digit is encountered.
    // We currently don't do this type of checking. So output may not conform to LRM in error condition.

    VeriBaseValue *ret_val = 0 ;
    switch(_function_type) {
    case VERI_METHOD_ATOBIN:
        // atobin         : integer string::atobin() - interprets the string as binary.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            char *str = Strings::strtolower(Strings::save(object_val->GetString())) ;
            char *p = str ;
            char *p_str = str ;
            unsigned is_negative = 0 ;
            // Check whether string is prefixed with '-/+'
            if (p && (*p == '-')) {
                is_negative = 1 ;
                p_str++ ;
                p++ ;
            } else if (p && (*p == '+')) {
                p_str++ ;
                p++ ;
            }
            while (p && (*p)) {
                if (!((*p == '0') || (*p == '1') ||
                      (*p == '_') || (*p == '?') ||
                      (*p == 'x') || (*p == 'z'))) {
                    *p = '\0' ; // Terminate at first non-binary character
                    break ;
                }
                p++ ;
            }
            // If string contains x/z, returns x/z. Standard simulators behave
            // differently. Some returns 0
            if (ConstStrEvaluation::StrHasXZ(p_str)) {
                unsigned l = (unsigned)Strings::len(p_str) ;
                char *prefix = Strings::itoa((int)l) ;
                char *v = 0 ;
                if (!is_negative && ConstStrEvaluation::StrHasOnlyZ(p_str)) {
                    v = Strings::save(prefix, "'bz") ; // contains only z not x
                } else {
                    v = Strings::save(prefix, "'bx") ;
                }
                ret_val = new VeriBasedNumber(v) ;
                Strings::free(prefix) ;
                Strings::free(v) ;
            } else {
                // Treat it as binary and get the integer decimal value
                int val = ConstStrEvaluation::BinToInt(p_str, object_id->IsSigned()) ;
                // Create a new VeriInteger value, this will be returned!
                ret_val = new VeriInteger(is_negative ? (-val): val) ;
            }
            Strings::free(str) ;
            break ;
        }
    case VERI_METHOD_ATOHEX:
        // atohex         : integer string::atohex() - interprets the string as hexadecimal.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            char *str = Strings::strtolower(Strings::save(object_val->GetString())) ;
            char *p = str ;
            char *p_str = str ;
            unsigned is_negative = 0 ;
            // Check whether string is prefixed with '-/+'
            if (p && (*p == '-')) {
                is_negative = 1 ;
                p_str++ ;
                p++ ;
            } else if (p && (*p == '+')) {
                p_str++ ;
                p++ ;
            }
            while (p && (*p)) {
                if (!(isdigit(*p) ||
                      (*p == '_') || (*p == '?') ||
                      (*p == 'x') || (*p == 'z') ||
                      ((*p >= 'a') && (*p <= 'f')))) {
                    *p = '\0' ; // Terminate at first non-hex character
                    break ;
                }
                p++ ;
            }
            // Treat it as hexadecimal and get the binary value
            char *bin = ConstStrEvaluation::HexToBin(p_str) ;
            // If string contains x/z, returns x/z. Standard simulators behave
            // differently. Some returns 0
            if (ConstStrEvaluation::StrHasXZ(bin)) {
                unsigned l = (unsigned)Strings::len(bin) ;
                char *prefix = Strings::itoa((int)l) ;
                char *v = 0 ;
                if (!is_negative && ConstStrEvaluation::StrHasOnlyZ(p_str)) {
                    v = Strings::save(prefix, "'bz") ; // contains only z not x
                } else {
                    v = Strings::save(prefix, "'bx") ;
                }
                ret_val = new VeriBasedNumber(v) ;
                Strings::free(prefix) ;
                Strings::free(v) ;
            } else {
                // Now convert the binary value into integer decimal value
                int val = ConstStrEvaluation::BinToInt(bin, object_id->IsSigned()) ;
                // Create a new VeriInteger value, this will be returned!
                ret_val = new VeriInteger((is_negative) ? (-val): val) ;
            }
            Strings::free(bin) ;
            Strings::free(str) ;
            break ;
        }
    case VERI_METHOD_ATOI:
        // atoi           : integer string::atoi() - interprets the string as decimal.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            const char *str = object_val->GetString() ;
            // Get the integer value from this string
            int val = Strings::atoi(str) ;
            // Create a new VeriInteger value, this will be returned!
            ret_val = new VeriInteger(val) ;
            break ;
        }
    case VERI_METHOD_ATOOCT:
        // atooct         : integer string::atooct() - interprets the string as octal.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            char *str = Strings::strtolower(Strings::save(object_val->GetString())) ;
            char *p = str ;
            char *p_str = str ;
            unsigned is_negative = 0 ;
            // Check whether string is prefixed with '-/+'
            if (p && (*p == '-')) {
                is_negative = 1 ;
                p_str++ ;
                p++ ;
            } else if (p && (*p == '+')) {
                p_str++ ;
                p++ ;
            }
            while (p && (*p)) {
                if (!(((*p >= '0') && (*p <= '7')) ||
                      (*p == '_') || (*p == '?') ||
                      (*p == 'x') || (*p == 'z'))) {
                    *p = '\0' ; // Terminate at first non-octal character
                    break ;
                }
                p++ ;
            }
            // Treat it as octal and get the binary value
            char *bin = ConstStrEvaluation::OctToBin(p_str) ;
            // If string contains x/z, returns x/z. Standard simulators behave
            // differently. Some returns 0
            if (ConstStrEvaluation::StrHasXZ(bin)) {
                unsigned l = (unsigned)Strings::len(bin) ;
                char *prefix = Strings::itoa((int)l) ;
                char *v = 0 ;
                if (!is_negative && ConstStrEvaluation::StrHasOnlyZ(p_str)) {
                    v = Strings::save(prefix, "'bz") ; // contains only z not x
                } else {
                    v = Strings::save(prefix, "'bx") ;
                }
                ret_val = new VeriBasedNumber(v) ;
                Strings::free(prefix) ;
                Strings::free(v) ;
            } else {
                // Now convert the binary value into integer decimal value
                int val = ConstStrEvaluation::BinToInt(bin, object_id->IsSigned()) ;
                // Create a new VeriInteger value, this will be returned!
                ret_val = new VeriInteger(is_negative ? (-val): val) ;
            }
            Strings::free(bin) ;
            Strings::free(str) ;
            break ;
        }
    case VERI_METHOD_ATOREAL:
        // atoreal        : real string::atoreal() - interprets the string as decimal real.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            const char *str = object_val->GetString() ;
            // Get the integer value from this string
            double val = Strings::atof(str) ;
            // Create a new VeriReal value, this will be returned!
            ret_val = new VeriReal(val) ;
            break ;
        }
    case VERI_METHOD_BACK:
        // back           : T list::back() - returns the data stored in the last element of
        //                                   the list (valid only if the list is not empty).
        break ;
    case VERI_METHOD_BINTOA:
        // bintoa         : string::bintoa(N) - stores the ASCII binary representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the binary string value corresponding to this value
            unsigned sign = 0 ;
            char *bin_val = arg_val->GetBinaryValue(&sign) ;
            delete arg_val ;
            // Get past the ' character
            char *trim_bin_val = bin_val ? strchr(bin_val, '\''): 0 ;
            trim_bin_val = (trim_bin_val) ? trim_bin_val+1 : bin_val ;
            // Remove the leading 0 s
            while (trim_bin_val && *trim_bin_val) {
                if (*trim_bin_val == '0') {
                    trim_bin_val++ ;
                } else {
                    break ;
                }
            }
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(trim_bin_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(bin_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_CLEAR:
        // clear          : list::clear() - removes all the elements from a list, but not the list
        //                                  itself (i.e., the list header itself).
        break ;
    case VERI_METHOD_COMPARE:
        // compare        : int string::icompare(s) - compares string and s, like the ANSI C strcmp()
        //                                            function, with a compatible return value.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            if (!_args || !_args->Size()) break ; // Nothing to compare with this string!
            // Get the string representation of this value
            const char *str = object_val->GetString() ;
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't compare!
            // Get the string representation of this argument
            const char *s = arg_val->GetString() ;
            // Now compare the two strings using ANSI strcmp():
            int val = strcmp(str, s) ; // val can be less than, equal to, or greater than zero
            // Create a new VeriInteger value, this will be returned!
            ret_val = new VeriInteger(val) ;
            delete arg_val ;
            break ;
        }
    case VERI_METHOD_CONSTRAINT_MODE:
        // constraint_mode: int constraint_id::constraint_mode() - returns the current active state of the
        //                  specified constraint block. It returns 1 if the constraint is active (ON),
        //                  and 0 if the constraint is inactive (OFF).
        //                : constraint_id::constraint_mode(bit) - used to enable or disable any named
        //                                                        constraint block in an object.
        break ;
    case VERI_METHOD_DATA:
        // data           : T list::data() - returns the data stored in the element at the given
        //                                   iterator location of the list.
        break ;
    case VERI_METHOD_DELETE:
        // delete         : dynamic_array::delete([N]) - delete Nth element/empties the array,
        //                                               resulting in a zero-sized array.
        break ;
    case VERI_METHOD_EMPTY:
        // empty          : int list::empty() - returns 1 if the number elements stored in the list
        //                                      is zero, 0 otherwise.
        break ;
    case VERI_METHOD_EQ:
        // eq             : int List_Iterator::eq(List_Iterator#(T) iter) - compares two iterators, returns
        //                  1 if both iterators refer to the same list element. Otherwise, it returns 0.
        break ;
    case VERI_METHOD_ERASE:
        // erase          : list::erase(List_Iterator#(T) position) - erase removes form the list the element at
        //                  the specified position. After erase() returns, the position iterator becomes invalid.
        break ;
    case VERI_METHOD_ERASE_RANGE:
        // erase_range    : list::erase_range(List_Iterator#(T) first, last) - erase_range removes from a list
        //                  the range of elements specified by the first and last iterators. This operation
        //                  removes elements from the first iterator's position up to, but not including, the last
        //                  iterator's position. If the last iterator refers to an element before the first iterator,
        //                  the range wraps around the end of the list.
        break ;
    case VERI_METHOD_EXISTS:
        // exists         : int array::exists() - checks if an element exists at the specified index within the given
        //                  array. It returns 1 if the element exists, otherwise it returns 0.
        break ;
    case VERI_METHOD_FIRST:
        // first          : int array::first() - assigns to the given index variable the value of the first (smallest)
        //                  index in the associative array. It returns 0 if the array is empty, and 1 otherwise.
        //                : enum enum::first() - returns the value of the first member of the enumeration enum
        {
        // Check whether it is enum or array:
        // Get the data type of the id, it should be enum or typeref to enum:
        VeriDataType *id_type = object_id->GetDataType() ;
        // If it is enum, GetEnums() should return the enum ids:
        Array *enum_ids = (id_type) ? id_type->GetEnums() : 0 ;
        if (enum_ids && enum_ids->Size()) {
            // It's an enum, get the first enum literal:
            VeriIdDef *first_id = (VeriIdDef *)enum_ids->GetFirst() ;

            // Create value for this literal
            ret_val = (first_id) ? new VeriEnumVal(first_id) : 0  ;
            break ;
        }
        // FIXME: Need to implement array::first() here:
        break ;
        }
    case VERI_METHOD_FRONT:
        // front          : T list::front() - returns the data stored in the first element of the list (valid only
        //                                    if the list is not empty).
        break ;
    case VERI_METHOD_GETC:
        // getc           : int string::getc(i) - returns the ASCII code of the ith character in string.
        //                  If i < 0 or i >= string.len(), then string.getc(i) returns 0. Note: x = string.getc(j)
        //                  is identical to x = string[j].
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            if (!_args || !_args->Size()) break ; // Nothing to get from the string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ;
            // Get the integer value of this argument - it is the index to get
            int index = arg_val->GetIntegerValue() ;
            delete arg_val ;
            const char *str = object_val->GetString() ; // Should be an ASCII string, no need to get past the ' character
            // Return 0 for out-of-range index
            if (index < 0 || index >= (int)Strings::len(str)) break ;
            // Create an integer value with the ASCII value of the character in the index position
            ret_val = new VeriInteger(str[index]) ;
            break ;
        }
    case VERI_METHOD_HEXTOA:
        // hextoa         : string::hextoa(N) - stores the ASCII hexadecimal representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the integer value corresponding to this value
            int int_val = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Allocate string for the hex repersentation, int can have atmost 8 hex digits!
            char *hex_val = Strings::allocate(8) ;
            // Get the hex representation of this integer
            sprintf(hex_val, "%x", int_val) ; // Also handles -ve values correctly!!
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(hex_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(hex_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_ICOMPARE:
        // icompare       : int string::icompare(s) - compares string and s, like the ANSI C strcmp()
        //                  function, with a compatible return value, but the comparison is case insensitive.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            if (!_args || !_args->Size()) break ; // Nothing to compare with this string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't compare!
            // Get the string representation of this argument and make it lower-case:
            char *s_lower = Strings::save(arg_val->GetString()) ;
            delete arg_val ;
            s_lower = Strings::strtolower(s_lower) ;
            // Get the string representation of this value and make it lower-case:
            char *str_lower = Strings::save(object_val->GetString()) ;
            str_lower = Strings::strtolower(str_lower) ;
            // Now compare the two strings using ANSI strcmp() - effectively compare ignoring case:
            int val = strcmp(str_lower, s_lower) ; // val can be less than, equal to, or greater than zero
            Strings::free(str_lower) ;
            Strings::free(s_lower) ;
            // Create a new VeriInteger value, this will be returned!
            ret_val = new VeriInteger(val) ;
            break ;
        }
    case VERI_METHOD_INSERT:
        // insert         : list::insert(List_Iterator#(T) position, T value) - insert inserts the given
        //                  data (value) into the list at the position specified by the iterator (before
        //                  the ele- ment, if any, that was previously at the iterator's position). If the
        //                  iterator is not a valid position within the list, then this operation is illegal
        //                  and can generate an error.
        break ;
    case VERI_METHOD_INSERT_RANGE:
        // insert_range   : list::insert_range(List_Iterator#(T) position, first, last) - insert_range inserts
        //                  the elements contained in the list range specified by the iterators first and last
        //                  at the specified list position (before the element, if any, that was previously
        //                  at the position iterator). All the elements from first up to, but not including,
        //                  last are inserted into the list. If the last iterator refers to an element before
        //                  the first iterator, the range wraps around the end of the list. The range iterators
        //                  can specify a range either in another list or in the same list as being inserted.
        //                  If the position iterator is not a valid position within the list, or if the range
        //                  iterators are invalid (i.e., they refer to different lists or to invalid positions),
        //                  then this operation is illegal and can generate an error.
        break ;
    case VERI_METHOD_ITOA:
        // itoa           : string::itoa(N) - stores the ASCII decimal representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the integer value corresponding to this value
            int int_val = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Get the decimal string representation of this integer
            char *dec_val = Strings::itoa(int_val) ;
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(dec_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(dec_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_LAST:
        // last           : int array::last() - assigns to the given index variable the value of the
        //                  last (largest) index in the associative array. It returns 0 if the array
        //                  is empty, and 1 otherwise.
        //                : int enum::last() - returns the value of the last member of the enumeration enum
        {
        // Check whether it is enum or array:
        // Get the data type of the id, it should be enum or typeref to enum:
        VeriDataType *id_type = object_id->GetDataType() ;
        // If it is enum, GetEnums() should return the enum ids:
        Array *enum_ids = (id_type) ? id_type->GetEnums() : 0 ;
        if (enum_ids && enum_ids->Size()) {
            // It's an enum, get the last enum literal:
            VeriIdDef *first_id = (VeriIdDef *)enum_ids->GetLast() ;
            // Create value with this literal
            ret_val = (first_id) ? new VeriEnumVal(first_id) : 0 ;
            break ;
        }
        // FIXME: Need to implement array::last() here:
        break ;
        }
    case VERI_METHOD_LEN:
        // len            : int string::len() - returns the length of the string, i.e., the number of characters in
        //                  the string (excluding any terminating character). If str is "", then string.len() returns 0.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            const char *str = object_val->GetString() ;
            // Get the length of the string
            int val = (int)Strings::len(str) ;
            // Create a VeriInteger with the length, this value will be returned!
            ret_val = new VeriInteger(val) ;
            break ;
        }
    case VERI_METHOD_NAME:
        // name           : string enum::name() - returns the string representation of the given
        //                  enumeration value. If the given value is not a member of the enumeration,
        //                  the name() method returns the empty string.
        {
            // VIPER #5406: Implement enum.name() built-in method:
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Check whether it is enum or not:
            // Get the data type of the id, it should be enum or typeref to enum:
            VeriDataType *id_type = object_id->GetDataType() ;
            // If it is enum, GetEnums() should return the enum ids:
            Array *enum_ids = (id_type) ? id_type->GetEnums() : 0 ;
            // Iterate over the enum literals to check whose value is 'object_val'
            VeriIdDef *matched_enum = 0 ; // Store the enum whose value is 'object_val'
            unsigned i ;
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(enum_ids, i, id) {
                // First try to match with object_val:
                if (id == object_val->GetEnumId()) {
                    matched_enum = id ;
                    break ;
                }
            }
            if (!matched_enum) break ; // Could not find any matched enum
            ret_val = new VeriAsciiString(matched_enum->Name()) ;
            break ;
        }
    case VERI_METHOD_NEQ:
        // neq            : int List_Iterator::neq(List_Iterator#(T) iter) - neq is the negation of eq();
        //                  It compares two iterators, and returns 0 if both iterators refer to the same
        //                  list element. Otherwise, it returns 1.
        break ;
    case VERI_METHOD_NEXT:
        // next           : List_Iterator::next([N]) - changes the iterator so that it refers to the next
        //                  element in the list. [index for array.] - finds the entry whose index is greater
        //                  than the given index. If there is a next entry, the index variable is assigned
        //                  the index of the next entry, and the function returns 1. Otherwise, index is unchanged,
        //                  and the function returns 0.
        //                  enum enum::next([N]) - returns the next enum literal of the enum that is currently being pointed.
        //                  assoc_array::next() - returns the next entry value in the associative array.
        {
        if (!object_val) break ; // Can't do anything without a valid value of the object
        // Check whether it is enum or array:
        // Get the data type of the id, it should be enum or typeref to enum:
        VeriDataType *id_type = object_id->GetDataType() ;
        // If it is enum, GetEnums() should return the enum ids:
        Array *enum_ids = (id_type) ? id_type->GetEnums() : 0 ;
        if (!enum_ids || !enum_ids->Size()) break ; // Can't continue
        // Iterate over the enum literals to check whose value is 'object_val'
        VeriIdDef *matched_enum = 0 ; // Store the enum whose value is 'object_val'
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(enum_ids, i, id) {
            // First try to match with object_val:
            if (id == object_val->GetEnumId()) {
                matched_enum = id ;
                break ;
            }
        }
        if (!matched_enum) break ; // Could not find any matched enum
        // If there is any argument, evaluate that one
        VeriExpression *arg = (_args && _args->Size()) ? (VeriExpression*)_args->GetFirst() : 0 ;
        VeriBaseValue *arg_value = arg ? arg->StaticEvaluateInternal(0, 0, 0, expect_nonconst) : 0 ;
        unsigned arg_val = arg_value ? (unsigned)arg_value->GetIntegerValue() : 0 ;
        delete arg_value ;
        // It is enum.next(N) : We should return next N th enum from current_enum
        // If it is enum.next() : We should return next enum.
        // Calculate the position of returned enum
        unsigned next_pos = arg_val ? (i + arg_val) : (i+1) ;
        // If this position is outside of the enum ids, it wraps:
        if (next_pos >= enum_ids->Size()) next_pos = next_pos % (enum_ids->Size()) ;
        // Get next enum
        VeriIdDef *next_enum = (VeriIdDef*)enum_ids->At(next_pos) ;
        if (next_enum) {
            // Create value for next enum literal
            ret_val = new VeriEnumVal(next_enum) ;
        }
        break ;
        }
    case VERI_METHOD_NUM:
        // num            : int enum::num() - returns the number of elements in the given enumeration.
        //                  int assoc_array::num() - returns the number of elements in the given associative array.
        {
        // Check whether it is enum or assoc_array:
        // Get the data type of the id, it should be enum or typeref to enum:
        VeriDataType *id_type = object_id->GetDataType() ;
        // If it is enum, GetEnums() should return the enum ids:
        Array *enum_ids = (id_type) ? id_type->GetEnums() : 0 ;
        if (enum_ids) {
            // It's an enum, get the number of ids:
            unsigned val = enum_ids->Size() ;
            // Create a new VeriInteger value, this will be returned!
            ret_val = new VeriInteger((int)val) ;
            break ;
        }
        // FIXME: Need to implement assoc_array::num() here:
        break ;
        }
    case VERI_METHOD_OCTTOA:
        // octtoa         : string::octtoa(N) - stores the ASCII octal representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the integer value corresponding to this value
            int int_val = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Allocate string for the octal repersentation, int can have atmost 8 hex digits!
            char *oct_val = Strings::allocate(8) ;
            // Get the octal representation of this integer
            sprintf(oct_val, "%o", int_val) ; // Also handles -ve values correctly!!
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(oct_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(oct_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_POP_BACK:
        // pop_back       : list::pop_back() - removes the last element of the list. If the list is empty,
        //                                     this method is illegal and can generate an error.
        break ;
    case VERI_METHOD_POP_FRONT:
        // pop_front      : list::pop_front() - removes the first element of the list. If the list is empty,
        //                                      this method is illegal and can generate an error.
        break ;
    case VERI_METHOD_POST_RANDOMIZE:
        // post_randomize : class::post_randomize() - Every class contains built-in pre_randomize() and
        //                  post_randomize() functions, that are automatically called by randomize()
        //                  before and after computing new random values.
        break ;
    case VERI_METHOD_PRE_RANDOMIZE:
        // pre_randomize  : class::pre_randomize() - Same as post_randomize, but it is called before computing new random values.
        break ;
    case VERI_METHOD_PREV:
        // prev           : List_Iterator::prev([N]) - changes the iterator so that it refers to the
        //                  previous element in the list. [index for array] - finds the entry whose
        //                  index is smaller than the given index. If there is a previous entry, the
        //                  index variable is assigned the index of the previous entry, and the function
        //                  returns 1. Otherwise, the index is unchanged, and the function returns 0.
        //                  enum enum::prev([N]) - returns the previous enum literal of the enum that is currently being pointed.
        {
        if (!object_val) break ; // Can't do anything without a valid value of the object
        // Check whether it is enum or array:
        // Get the data type of the id, it should be enum or typeref to enum:
        VeriDataType *id_type = object_id->GetDataType() ;
        // If it is enum, GetEnums() should return the enum ids:
        Array *enum_ids = (id_type) ? id_type->GetEnums() : 0 ;
        if (!enum_ids || !enum_ids->Size()) break ; // Can't continue
        // Iterate over the enum literals to check whose value is 'object_val'
        VeriIdDef *matched_enum = 0 ; // Store the enum whose value is 'object_val'
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(enum_ids, i, id) {
            // Check whether id matches
            if (id == object_val->GetEnumId()) {
                matched_enum = id ;
                break ;
            }
        }
        if (!matched_enum) break ; // Could not find any matched enum
        // If there is any argument, evaluate that one
        VeriExpression *arg = (_args && _args->Size()) ? (VeriExpression*)_args->GetFirst() : 0 ;
        VeriBaseValue *arg_value = arg ? arg->StaticEvaluateInternal(0, 0, 0, expect_nonconst) : 0 ;
        unsigned arg_val = arg_value ? (unsigned)arg_value->GetIntegerValue() : 0 ;
        delete arg_value ;
        // It is enum.prev(N) : We should return previous N th enum from current_enum
        // If it is enum.prev() : We should return previous enum.
        // Calculate the position of returned enum
        int prev_pos = (arg_val) ? ((int)i-(int)arg_val) : ((int)i-1) ;
        // If this position is outside of the enum ids, it wraps:
        while (prev_pos < 0) prev_pos = ((int)enum_ids->Size() + prev_pos) ;
        // Get previous enum
        VeriIdDef *prev_enum = (VeriIdDef*)enum_ids->At((unsigned)prev_pos) ;
        if (prev_enum) {
            // Create value for previous enum literal
            ret_val = new VeriEnumVal(prev_enum) ;
        }
        break ;
        }
    case VERI_METHOD_PURGE:
        // purge          : list::purge() - removes all the list elements (as in clear) and the list itself.
        //                  This accomplishes the same effect as assign- ing null to the list. A purged list
        //                  must be re-created using new before it can be used again.
        break ;
    case VERI_METHOD_PUSH_BACK:
        // push_back      : list::push_back(T value) - inserts the specified value at the end of the list.
        break ;
    case VERI_METHOD_PUSH_FRONT:
        // push_front     : list::push_front(T value) - inserts the specified value at the front of the list.
        break ;
    case VERI_METHOD_PUTC:
        // putc           : string::putc(i, s) - s is either 'byte' or 'string'. Replaces the ith character
        //                  in string with the given integral value. putc does not change the size of string:
        //                  If i < 0 or i >= string.len(), then string is unchanged. Note: string.putc(j, x)
        //                  is identical to string[j] = x.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            if (!_args || (_args->Size() < 2)) break ; // Nothing to put into the string!
            // Get the first argument, which is the index position to replace.
            VeriExpression *arg = (VeriExpression *)_args->At(0) ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ;
            // Get the integer value of this argument.
            int index = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Get the second argument, which is the replace string/byte
            arg = (VeriExpression *)_args->At(1) ;
            // Evaluate this argument to get the value
            arg_val = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ;
            // Get the string value of this argument.
            const char *repl_str = arg_val->GetString() ;
            char replace = (repl_str) ? repl_str[0] : 0 ;
            delete arg_val ;
            // Get the char * of the string to be replaced
            const char *str = object_val->GetString() ;
            // Sanity checking, whether we can actually replace
            if (!replace || !str || (index < 0) || (index >= (int)Strings::len(str))) break ;
            char *new_str = Strings::save(str) ;
            // Now replace the character in the 'index' position with the first character of the given string
            new_str[index] = replace ; // Replace the character
            // Create and insert a string value into the value table against this string
            VeriAsciiString val(new_str) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(new_str) ; // The string is copied, so free it now
            if (val_table && !val_table->Insert(object_id, &val)) {  // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_RAND_MODE:
        // rand_mode      : class::rand_mode() - set/get the current randomization mode
        break ;
    case VERI_METHOD_RANDOMIZE:
        // randomize      : class::randomize() - a virtual function that generates random values for all
        //                  the active random variables in the object, subject to the active constraints.
        break ;
    case VERI_METHOD_REALTOA:
        // realtoa        : string::realtoa(N) - stores the ASCII real representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the real value corresponding to this value
            double d_val = arg_val->GetRealValue() ;
            delete arg_val ;
            // Allocate string for the decimal real repersentation.
            char *real_val = Strings::allocate(64) ;
            // Get the string representation of this double
            sprintf(real_val, "%f", d_val) ; // Also handles -ve values correctly!!
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(real_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(real_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table  && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_SET:
        // set            : list::set(List_Iterator#(T) first, last) - set assigns to the list
        //                  object the elements that lie in the range specified by the first and
        //                  last iterators. After this method returns, the modified list shall have
        //                  a size equal to the range specified by first and last. This method copies
        //                  the data from the first iterator's position up to, but not including, the
        //                  last iterator's position. If the last iterator refers to an element before
        //                  the first iterator, the range wraps around the end of the list.
        break ;
    //case VERI_METHOD_SIZE:
    case VERI_SYS_CALL_SIZE:
        // size           : int list::size()/dynamic_array::size() - returns the number of elements
        //                  stored in the list/dynamic_array.
        break ;
    case VERI_METHOD_START:
        // start          : List_Iterator#(T) list::start() - start returns an iterator to the position of
        //                  the first element in the list.
        break ;
    case VERI_METHOD_SUBSTR:
        // substr         : string string::substr(i, j) - returns a new string that is a sub-string formed
        //                  by characters in position i through j of the string.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            if (!_args) break ;
            // VIPER #5248 (vcs_compatibility_issue-9000387999): According to LRM P1800(2009) this function expect 2 arguments,
            // thus we are giving correct error but vcs does not error out. So we put this check into relaxed check in mode.
            // In relaxed check in mode we allow the function with one argument and in this case find the substring from
            // the position specified by the first argument to the end of the string.
            if (InRelaxedCheckingMode()) {
                if (_args->Size() < 1) break ; // Nothing to put into the string!
            } else {
                if (_args->Size() < 2) break ; // Nothing to put into the string!
            }
            // Get the first argument, which is the starting index position.
            VeriExpression *arg = (VeriExpression *)_args->At(0) ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ;
            // Get the integer value of this argument.
            int start = arg_val->GetIntegerValue() ;
            delete arg_val ;
            int finish = 0 ;
            if (_args->Size() > 1) {
                // Get the second argument, which is the ending index
                arg = (VeriExpression *)_args->At(1) ;
                // Evaluate this argument to get the value
                arg_val = (arg) ? arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
                if (!arg_val) break ;
                // Get the integer value of this argument.
                finish = arg_val->GetIntegerValue() ;
                delete arg_val ;
            } else {
                // If the function has one argument, we find the substring from the position
                // specified by the first argument to the end of the string.
                const char *str = object_val->GetString() ;
                unsigned len = Strings::len(str) ;
                finish = (int)((len > 1) ? (len-1) : 0) ;
            }
            // Get the char * of the string to return substring from
            const char *str = object_val->GetString() ;
            char *new_str = Strings::save(str) ;
            // Sanity checking, whether we can actually return the substring
            if (start < 0 || start >= (int)Strings::len(str) ||
                finish < 0 || finish >= (int)Strings::len(str) ||
                start > finish) {
                Strings::free(new_str) ;
                break ;
            }
            // Now return the substring from 'start' to 'finish'
            char *ret_str = new_str+start ;
            new_str[finish+1] = '\0' ;
            ret_val = new VeriAsciiString(ret_str) ;
            Strings::free(new_str) ;
            break ;
        }
    case VERI_METHOD_SWAP:
        // swap           : list::swap(List#(T) lst) - swap exchanges the contents of two equal-size lists.
        break ;
    case VERI_METHOD_TOLOWER:
        // tolower        : string string::tolower() - returns a string with characters in str converted to lowercase.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            const char *str = object_val->GetString() ;
            // Make it lower-case
            char *str_lower = Strings::save(str) ;
            str_lower = Strings::strtolower(str_lower) ;
            ret_val = new VeriAsciiString(str_lower) ;
            Strings::free(str_lower) ;
            break ;
        }
    case VERI_METHOD_TOUPPER:
        // toupper        : string string::toupper() - returns a string with characters in str converted to uppercase.
        {
            if (!object_val) break ; // Can't do anything without a valid value of the object
            // Get the string representation of this value
            const char *str = object_val->GetString() ;
            // Make it upper-case
            char *str_upper = Strings::save(str) ;
            str_upper = Strings::strtoupper(str_upper) ;
            ret_val = new VeriAsciiString(str_upper) ;
            Strings::free(str_upper) ;
            break ;
        }
    default:
        break ;
    }

    // Delete the value of the object we have evaluated before
    delete object_val ;

    // Return the value of the built-in method call
    return ret_val ;
}

unsigned VeriModuleItem::InitializeDecls(const Array *decls, ValueTable *val_table, unsigned expect_nonconst)const
{
    if (!val_table) return 0 ;
    unsigned i, j ;
    VeriModuleItem *decl = 0 ;

    unsigned status = 1 ;
    VeriTypeInfo *type = 0 ;
    FOREACH_ARRAY_ITEM(decls, i, decl) {
        if (!decl) continue ;
        Array *id_list = decl->GetIds() ;
        VeriIdDef *decl_id = 0 ;
        FOREACH_ARRAY_ITEM(id_list, j, decl_id) {
            if (!decl_id || decl_id->IsPort()) continue ;

            type = decl_id->CreateType(0) ;
            if (type && type->IsDynamicArrayType()) {
                if (!expect_nonconst) decl->Warning("%s is not supported for static elaboration", "dynamic array in constant function") ;
                status = 0 ;
            } else if (type && type->IsAssociativeArrayType()) {
                if (!expect_nonconst) decl->Warning("%s is not supported for static elaboration", "associative array in constant function") ;
                status = 0 ;
            } else if (type && type->IsQueue()) {
                if (!expect_nonconst) decl->Warning("%s is not supported for static elaboration", "queue in constant function") ;
                status = 0 ;
            }
            delete type ;
            if (status == 0) return 0 ;
            VeriExpression *rhs = decl_id->GetInitialValue() ;
            VeriBaseValue *val = 0 ;
            if (rhs) {
                // VIPER #3303 and similar issue #3320: Evaluate the initial value
                // in the size of LHS and sign of RHS (like blocking assign):
                VeriConstraint *id_constraint = decl_id->EvaluateConstraintInternal(val_table, 0, 0) ;
                int val_size_sign = (int)rhs->StaticSizeSignInternal(val_table, id_constraint) ;
                int id_size_sign = (int)decl_id->StaticSizeSignInternal(val_table, 0) ;
                unsigned val_size = GET_CONTEXT_SIZE(val_size_sign) ;
                unsigned id_size = GET_CONTEXT_SIZE(id_size_sign) ;
                unsigned max_size = MAX(id_size, val_size) ;
                unsigned val_sign = (val_size_sign < 0) ? 1 : 0 ;
                int size_sign = MAKE_CONTEXT_SIZE_SIGN(max_size, val_sign) ;
                val = rhs->StaticEvaluateInternal(size_sign, val_table, id_constraint, expect_nonconst) ;
                delete id_constraint ;
            }
            if (!val_table->Insert(decl_id, val)) status = 0 ;
            delete val ;
        }
    }
    return status ;
}

unsigned VeriModuleItem::ElaborateStatements(const Array *statements, ValueTable *table, unsigned expect_nonconst) const
{
    unsigned ret_status = 1 ;

    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(statements, i, stmt) {
        if (stmt && !stmt->StaticElaborateStmt(table, expect_nonconst)) ret_status = 0 ;
        // Check if we have reached the return statement:
        if (table && table->GetJumpToken() == VERI_RETURN) break ;
        if (table && table->IsDisabled()) break ;
    }

    return ret_status ;
}

unsigned VeriFunctionDecl::InitializePorts(const Array *arg_list, ValueTable *val_table, ValueTable *curr_table, unsigned expect_nonconst) const
{
    if (!curr_table) return 0 ;
    if (!_ports) return 1 ;

    unsigned i ;
    VeriBaseValue *val = 0 ;
    VeriExpression *expr = 0 ;
    // LRM 3.1a (draft5), section 10.4.4, page 111: If both positional and named arguments
    // are specified in a single subroutine call, then all the positional arguments must
    // come before the named arguments. It keep tracks of positional/named arguments.
    // Resolve the actuals, and check formals against actuals :
    VeriIdDef *formal ;
    Set connected_formals(POINTER_HASH, (_ports) ? _ports->Size() : 1) ;
    FOREACH_ARRAY_ITEM(arg_list, i, expr) {
        if (!expr) continue ; // unconnected actual

        // Find the formal
        formal = 0 ;
        if (expr->NamedFormal()) {
            // This is a (SV-style) named formal
            formal = (_scope) ? _scope->FindLocal(expr->NamedFormal()): 0 ;
            expr = expr->GetConnection() ;
        } else {
            // This is a ordered formal
            if (i>= ((_ports) ? _ports->Size() : 0)) {
                break ; // no use checking more
            }
            formal = (_ports && _ports->Size() > i) ? (VeriIdDef*)_ports->At(i) : 0 ;
        }
        if (!formal) continue ;
        // Concentrate on inputs only (and inouts). Basically ignore outputs.
        if (formal->IsOutput()) continue ;

        // Flag this formal as connected.
        (void) connected_formals.Insert(formal) ;

        // Evaluate the actual.
        // Since this is an 'assignment' (actual to formal) in the Verilog sense,
        // we need to determine LHS and RHS size, to comply with Verilog sizing/sign rules
        int lsize = (int)formal->StaticSizeSignInternal(curr_table, 0) ;
        unsigned left_size = (unsigned)ABS(lsize) ;
        VeriConstraint *formal_constraint = formal->EvaluateConstraintInternal(curr_table, 0, 0) ;
        int rsize = (int)expr->StaticSizeSignInternal(curr_table, formal_constraint) ;
        unsigned right_size = (unsigned)ABS(rsize) ;
        unsigned rsign = (rsize < 0) ? 1 : 0 ;

        // LRM 4.4.1 : size must always be the MAX of both LHS and RHS.
        unsigned size = (left_size > right_size) ? left_size : right_size ;
        int eval_context = rsign ? -((int)size) : (int)size ;
        val = expr->StaticEvaluateInternal(eval_context, val_table, formal_constraint, expect_nonconst) ;
        delete formal_constraint ;
        if (!val) return 0 ;

        // Insert the value against the port identifier
        if (!curr_table->Insert(formal, val)) {
            delete val ;
            return 0 ;
        }
        delete val ;
    }

    // Now check if all formals have an actual or initial value
    if (((_ports) ? _ports->Size() : 0) != connected_formals.Size()) {
        FOREACH_ARRAY_ITEM(_ports, i, formal) {
            if (!formal) continue ;
            if (connected_formals.GetItem(formal)) continue ;
            if (formal->IsOutput()) {
                // Insert x against it
                if (!curr_table->Insert(formal, 0)) return 0 ;
            } else {
                // Pick up the initial value
                expr = formal->GetInitialValue() ;
                if (!expr) continue ;

                // Now evaluate the initial value :
                int lsize = (int)formal->StaticSizeSignInternal(curr_table, 0) ;
                unsigned left_size = (unsigned)ABS(lsize) ;
                VeriConstraint *formal_constraint = formal->EvaluateConstraintInternal(curr_table, 0, 0) ;
                int rsize = (int)expr->StaticSizeSignInternal(curr_table, formal_constraint) ;
                unsigned right_size = (unsigned)ABS(rsize) ;
                unsigned rsign = (rsize < 0) ? 1 : 0 ;

                // LRM 4.4.1 : size must always be the MAX of both LHS and RHS.
                unsigned size = (left_size > right_size) ? left_size : right_size ;
                int eval_context = rsign ? -((int)size) : (int)size ;
                val = expr->StaticEvaluateInternal(eval_context, curr_table, formal_constraint, expect_nonconst) ;
                delete formal_constraint ;
                if (!val) return 0 ;

                // Insert the value against the port identifier
                if (!curr_table->Insert(formal, val)) {
                    delete val ;
                    return 0 ;
                }
                delete val ;
            }
        }
    }

    return 1 ;
}

// The StaticElaborateStmt routines are virtual routines on VeriStatement and its
// derived classes. The main objective of this method is to incrementally evaluate
// each statement, update the entries of the identifiers used in that statement in
// the ValueTable present in the local class supplied, so that the updated values
// could be used for executing subsequent statements.

unsigned VeriStatement::StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst)
{
    if (eval && eval->IsDeadCode()) return 1 ;  // We are in dead code

    if (!expect_nonconst) Error("%s is not supported for static elaboration", "this statement");
    return 0 ;
}

unsigned VeriSystemTaskEnable::StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst)
{
    if (eval && eval->IsDeadCode()) return 1 ;  // We are in dead code

    (void) expect_nonconst ; //JJ: needed to circomvent unused error

    // Get the first argument:
    VeriExpression *arg = (_args && _args->Size()) ? (VeriExpression *)_args->At(0) : 0 ;
    switch (_function_type) {
    case VERI_SYS_CALL_CAST:
        {
            // The is the system task $cast. It has two arguments. The first one is the
            // target and the second one is the value to be assigned on the target.
            VeriExpression *expr = (_args && (_args->Size() > 1)) ? (VeriExpression*)_args->At(1) : 0 ;

            // Evaluate the expression:
            VeriBaseValue *value = (expr) ? expr->StaticEvaluateInternal(0, eval, 0, expect_nonconst) : 0 ;
            if (!value) return 0 ;

            // Assign the value on the target (assume success if target is not there):
            unsigned ret_val = (arg) ? arg->StaticAssign(eval, value, expect_nonconst) : 1 ;
            delete value ;

            return ret_val ;
        }
    default : break ;
    }

    // VIPER #2704: We want to ignore this system task invocation and continue the
    // elaboration process as if this statement was commented in the Verilog source.
    // So, return 1 from here to indicate that we have successfully processed this
    // but do not do anything for it. Without this method the virtual catcher was
    // called and that returns 0 to indicate a failure and stops the elaboration.
    //Warning("%s is not supported for static elaboration", "system task invocation");
    return 1 ;
}
unsigned VeriTaskEnable::StaticElaborateStmt(ValueTable *val_table, unsigned expect_nonconst)
{
    if (!_method_type || !_task_name) return VeriStatement::StaticElaborateStmt(val_table, expect_nonconst) ;

    VeriName *object = (_task_name) ? _task_name->GetPrefix(): 0 ;
    VeriIdDef *object_id = (object) ? object->GetId() : 0 ;

    // VCS Compatibility issue (test_62) : Allow built-in method calls
    // itoa, hextoa, octtoa and bintoa.
    // Create an id-ref to reference to this object id, we have to evaluate that
    VeriIdRef object_ref(object_id) ;
    object_ref.SetLinefile(_task_name->Linefile()) ;
    // Now (static) evaluate the value of the object on which the built-in method is called
    VeriBaseValue *object_val = object_ref.StaticEvaluateInternal(0 /* size-sign is self-determined */, val_table, 0, expect_nonconst) ;

    switch (_method_type) {
    case VERI_METHOD_ITOA:
        // itoa           : string::itoa(N) - stores the ASCII decimal representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the integer value corresponding to this value
            int int_val = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Get the decimal string representation of this integer
            char *dec_val = Strings::itoa(int_val) ;
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(dec_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(dec_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_HEXTOA:
        // hextoa         : string::hextoa(N) - stores the ASCII hexadecimal representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the integer value corresponding to this value
            int int_val = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Allocate string for the hex repersentation, int can have atmost 8 hex digits!
            char *hex_val = Strings::allocate(8) ;
            // Get the hex representation of this integer
            sprintf(hex_val, "%x", int_val) ; // Also handles -ve values correctly!!
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(hex_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(hex_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_OCTTOA:
        // octtoa         : string::octtoa(N) - stores the ASCII octal representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the integer value corresponding to this value
            int int_val = arg_val->GetIntegerValue() ;
            delete arg_val ;
            // Allocate string for the octal repersentation, int can have atmost 8 hex digits!
            char *oct_val = Strings::allocate(8) ;
            // Get the octal representation of this integer
            sprintf(oct_val, "%o", int_val) ; // Also handles -ve values correctly!!
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(oct_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(oct_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_BINTOA:
        // bintoa         : string::bintoa(N) - stores the ASCII binary representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the binary string value corresponding to this value
            unsigned sign = 0 ;
            char *bin_val = arg_val->GetBinaryValue(&sign) ;
            delete arg_val ;
            // Get past the ' character
            char *trim_bin_val = strchr(bin_val, '\'') ;
            trim_bin_val = (trim_bin_val) ? trim_bin_val+1 : bin_val ;
            // Remove the leading 0 s
            while (trim_bin_val && *trim_bin_val) {
                if (*trim_bin_val == '0') {
                    trim_bin_val++ ;
                } else {
                    break ;
                }
            }
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(trim_bin_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(bin_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    case VERI_METHOD_REALTOA:
        // realtoa        : string::realtoa(N) - stores the ASCII real representation of N into string.
        {
            if (!_args || !_args->Size()) break ; // Nothing to convert to string!
            // Get the first argument.
            VeriExpression *first_arg = (VeriExpression *)_args->GetFirst() ;
            // Evaluate this argument to get the value
            VeriBaseValue *arg_val = (first_arg) ? first_arg->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) : 0 ;
            if (!arg_val) break ; // Can't convert it to string, no value can be evaluated!
            // Get the real value corresponding to this value
            double d_val = arg_val->GetRealValue() ;
            delete arg_val ;
            // Allocate string for the decimal real repersentation.
            char *real_val = Strings::allocate(64) ;
            // Get the string representation of this double
            sprintf(real_val, "%f", d_val) ; // Also handles -ve values correctly!!
            // Create a VeriAsciiString value with this string
            VeriAsciiString val(real_val) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(real_val) ; // Free the string, it is already copied in the value
            // Now we have to insert this value into the value table against this string
            if (val_table  && !val_table->Insert(object_id, &val)) { // Value is copied by 'Insert'
                delete object_val ;
                return 0 ;
            }
            break ;
        }
    default :
        delete object_val ;
        return VeriStatement::StaticElaborateStmt(val_table, expect_nonconst) ;
    }
    delete object_val ;
    return 1 ;
}
unsigned VeriSeqBlock::StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst)
{
    if (eval && eval->IsDeadCode()) return 1 ;  // We are in dead code

    // Initialize the declarations in the block:
    if (!InitializeDecls(_decl_items, eval, expect_nonconst)) return 0 ;

    // VIPER #4431: Push the current block id into stack:
    if (eval) eval->PushBlockId(_label) ;

    // Calling 'StaticElaborateStmt' for all statements
    unsigned result = ElaborateStatements(_stmts, eval, expect_nonconst) ;

    // VIPER #4431: Pop the block id from stack and clear it if disabled
    if (eval) eval->PopBlockId(_label) ;

    return result ;
}

unsigned VeriConditionalStatement::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (table && table->IsDeadCode()) return 1 ;  // We are in dead code

    // Ignoring unique/priority qualifiers, if any, in static elaborator

    unsigned ret_status = 1 ;
    VeriBaseValue *val = _if_expr ? _if_expr->StaticEvaluateInternal(0, table, 0, expect_nonconst): 0 ;

    // The condition is evaluated, if non-constant, error is produced.
    if (!val) return 0 ;  //Error("Non-constant function, evaluation failed\n") ;
    // VIPER #4960: Apply reduction-OR operator on the evaluated value:
    VeriBaseValue *res = VeriUnaryOperator::StaticEvaluateUnary(val, 1, VERI_REDOR) ;
    delete val ;
    if (!res) return 0 ;
    int b_cond = res->GetIntegerValue();
    delete res ;

    if (b_cond) {
        // The condition is logically 1, execute the true statement.
        ret_status = _then_stmt ? _then_stmt->StaticElaborateStmt(table, expect_nonconst) : 1 ;
    } else {
        //The condition is logically 0, execute the false statement.
        ret_status = _else_stmt ? _else_stmt->StaticElaborateStmt(table, expect_nonconst): 1 ;
    }
    return ret_status ;
}

unsigned VeriCaseStatement::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (table && table->IsDeadCode()) return 1 ;  // We are in dead code

    // Ignoring unique/priority qualifiers, if any, in static elaborator

    if (_type==VERI_INSIDE) {
        if (!expect_nonconst) Error("%s is not supported for static elaboration", "case inside statement");
        return 0 ;
    }
    if (_type==VERI_MATCHES) {
        if (!expect_nonconst) Error("%s is not supported for static elaboration", "case matches statement") ;
        return 0 ;
    }

    unsigned ret_status = 1 ;
    unsigned i, j ;
    VeriCaseItem *ci ;

    // VIPER #5819: Support for type (type operator and $typeof system function) case condition:
    if (_cond && (_cond->IsTypeOperator() || (_cond->GetFunctionType() == VERI_SYS_CALL_TYPEOF))) {
        unsigned done = 0 ;
        VeriBaseValue *val ;
        VeriExpression *expr ;
        VeriModuleItem *matched_stmt = 0 ;
        // Determine which case item matches the case condition:
        FOREACH_ARRAY_ITEM(_case_items, i, ci) {
            if (!ci) continue ;
            if (ci->IsDefault()) {
                // Store the default statement, if none matches it will be executed:
                matched_stmt = ci->GetStmt() ;
                continue ;
            }

            Array *cond_list = ci->GetConditions() ;
            // Traverse the case conditions of case item
            FOREACH_ARRAY_ITEM(cond_list, j, expr) {
                if (!expr) continue ;
                // Create a binary operator (automatic object, will get deleted automatically):
                VeriBinaryOperator cond_equl_expr(VERI_LOGEQ, _cond, expr) ;
                cond_equl_expr.SetLinefile(_cond->Linefile()) ;
                val = cond_equl_expr.StaticEvaluateInternal(0, table, 0, expect_nonconst) ;
                cond_equl_expr.SetLeft(0) ; cond_equl_expr.SetRight(0) ; // So that they don't get deleted
                if (!val) return 0 ;
                if (val->GetIntegerValue()) {
                    // This item matches the case condition, it should be executed:
                    matched_stmt = ci->GetStmt() ;
                    delete val ;
                    done = 1 ;
                    break ;
                }
                delete val ;
            }
            if (done) break ;
        }

        // Now, elaborate matched statement (it may be the default statement if none matches):
        if (matched_stmt) {
            ret_status = matched_stmt->StaticElaborateStmt(table, expect_nonconst) ;
        }

        return ret_status ; // Done
    }

    // Calculate the (Verilog) constraint size of the expressions
    int cond_size_sign = _cond ? (int)_cond->StaticSizeSignInternal(table, 0): 0 ;
    unsigned size = GET_CONTEXT_SIZE(cond_size_sign) ;
    unsigned sign = GET_CONTEXT_SIGN(cond_size_sign) ;
    VeriConstraint *cond_consraint = _cond ? _cond->EvaluateConstraintInternal(table, 0) : 0 ;

    // LRM 9.5 : expression size is MAX of all conditions, case-expression
    // If all conditions are signed, then all conditions should be evaluated with signed context
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        if (!ci) continue ;
        Array *conditions = ci->GetConditions() ;
        VeriExpression *ci_cond ;
        FOREACH_ARRAY_ITEM(conditions, j, ci_cond) {
            if (!ci_cond) continue ;
            int ci_cond_size_sign = (int)ci_cond->StaticSizeSignInternal(table, cond_consraint) ;
            unsigned ci_size = GET_CONTEXT_SIZE(ci_cond_size_sign) ;
            unsigned ci_sign = GET_CONTEXT_SIGN(ci_cond_size_sign) ;
            size = MAX(size,ci_size) ;
            if (!ci_sign) sign = 0 ;
        }
    }
    // So, evaluate condition with that size/sign info :
    int size_sign = MAKE_CONTEXT_SIZE_SIGN(size,sign) ;
    VeriBaseValue *c_val = _cond ? _cond->StaticEvaluateInternal(size_sign, table, 0, expect_nonconst): 0 ;
    // If the case condition is non-constant, produce an error.
    unsigned b_sign = 0 ;
    char *c_str = c_val ? c_val->GetBinaryValue(&b_sign): 0 ;
    delete c_val ;
    if (!c_str) return 0 ;  //Error("Non-constant function, evaluation failed\n") ;
    // Padd c_str to length 'size'
    if (Strings::len(c_str) != size) {
        char *tmp = c_str ;
        c_str = ConstStrEvaluation::GetPaddedString(c_str, size, sign) ;
        Strings::free(tmp) ;
    }

    VeriStatement *default_stmt = 0 ;
    Array expr_array(3) ;
    Array stmt_array(3) ;
    char *e_str = 0 ;
    VeriBaseValue *e_val = 0 ;

    // For each entry in the case_item_list, evaluate each case conditions and append
    // in an array. Parallelly another array of statements is also maintained
    // corresponding to an entry in the condition array.
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        if (!ci) {
            ret_status = 0 ;
            break ;
        }
        VeriStatement *stmt = ci->GetStmt() ;
        Array *expr_list = ci->GetConditions() ;

        VeriExpression *expr = 0 ;
        if (expr_list) {
            FOREACH_ARRAY_ITEM(expr_list, j, expr) {
                if (!expr) continue ;

                e_val = expr->StaticEvaluateInternal(size_sign, table, cond_consraint, expect_nonconst) ;
                e_str = e_val ? e_val->GetBinaryValue(&b_sign) : 0;
                delete e_val ;

                if (!e_str) {
                    //Error("Non-constant function, evaluation failed\n") ;
                    ret_status = 0 ;
                    break ;
                }
                // Padd evaluated value to 'size'
                if (Strings::len(e_str) != size) {
                    char *tmp = e_str ;
                    e_str = ConstStrEvaluation::GetPaddedString(tmp, size, sign) ;
                    Strings::free(tmp) ;
                }
                // Parallel entries are maintained in the two array to keep one to one correspondance.
                expr_array.InsertLast(e_str) ;
                stmt_array.InsertLast(stmt) ;
            }
        } else {
            // Default statement should execute.
            default_stmt = stmt ;
        }
    }

    delete cond_consraint ;

    if (ret_status) {
        unsigned b_matched = 0 ;
        FOREACH_ARRAY_ITEM(&expr_array, i, e_str) {
            if (!e_str) continue ;
            // If the case expression and the case item expression match, statement
            // corresponding to the case item expression in the condition array should get executed.
            // VIPER #6548: Do not use plain Strings::compare(c_str, e_str), check for casex/casez:
            if (ConstStrEvaluation::StrCaseEQ(c_str, e_str, _case_style)) {
                // pick up the corresponding statement, and execute it
                VeriStatement *stmt = (VeriStatement*)stmt_array.At(i) ;
                // VIPER #8423: Add NULL check to avoid segfault:
                if (stmt) ret_status = stmt->StaticElaborateStmt(table, expect_nonconst) ;
                b_matched = 1 ;
                break ; // we are done
            }
        }

        if ((!b_matched) && default_stmt) {
            ret_status = default_stmt->StaticElaborateStmt(table, expect_nonconst) ;
        }
    }

    FOREACH_ARRAY_ITEM(&expr_array, i, e_str) {
        Strings::free(e_str) ;
        e_str = 0 ;
    }
    Strings::free(c_str) ;

    return ret_status ;
}

unsigned VeriDisable::StaticElaborateStmt(ValueTable *eval, unsigned /*expect_nonconst*/)
{
    if (eval) {
        // VIPER #4431: Disable the block specified by the id id:
        VeriIdDef *block_id = (_task_block_name) ? _task_block_name->GetId() : 0 ;
        eval->DisableBlock(block_id) ; // Will do nothing if we are not inside the block of this id
    }

    return 1 ; // Always evaluated successfully
}

// We will loop upto this much times
// variable for it
#define LOOPLIMIT 5000

unsigned VeriFor::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 0 ; // nothing can be done without value table here

    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    unsigned ret_status = 1 ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_initials, i, item) {
        ret_status = item ? item->StaticElaborateStmt(table, expect_nonconst): 1 ;
        if (!ret_status) return 0 ;
    }

    // VIPER : 2947/4152 - honor user loop limit, if set by user
    unsigned user_loop_limit = veri_file::GetLoopLimit() ;
    unsigned loop_limit = user_loop_limit ? user_loop_limit : LOOPLIMIT ;

    unsigned loopcount = 0 ;
    // Gimple complains if we use 'while(1)', use 'for( ; ; )' instead : both are infinite loop!
    for( ; ; ) {
        if (loopcount++ >= loop_limit) {
            if (_cond && !expect_nonconst) {
                _cond->Error("loop count limit of %d exceeded; condition is never false", loop_limit) ;
            }
            break ;
        }

        // Assume final_cond cannot be null evaluate the terminating condition.
        VeriBaseValue * final_val = 0 ;
        final_val = _cond ? _cond->StaticEvaluateInternal(0, table, 0, expect_nonconst): 0 ;

        // If 'final_val' is zero, 'final_cond' could not be evaluated.
        if (!final_val) return 0 ; // final_cond->Warning("condition in for loop could not be evaluated") ;

        // Check whether the terminating condition is 1. One infers 1.
        int b_true = final_val->GetIntegerValue() ;
        delete final_val ;

        if (!b_true) break ;
        // If b_true is 1, terminating condition is not reached yet, so the statement is executed.
        ret_status = _stmt ? _stmt->StaticElaborateStmt(table, expect_nonconst): ret_status ;
        if (!ret_status) break ;
        // Get the token of the jump, if any
        unsigned jump_token = table->GetJumpToken() ;
        if (jump_token) {
            /*
            // The code inside this 'if' stament would be like this:
            if (jump_token == VERI_RETURN) {
                // It's a 'return', don't process anything after this point.
                break ;
            } else if (jump_token == VERI_CONTINUE) {
                // It's a 'continue', so reset the jump token only. We have already
                // executed the statements and 'continue' requires to evaluate the
                // repeat statement, so we have to evaluate that. If we don't reset
                // the jump token we would be skipping all the statements after the
                // loop also, which we should not!
                table->SetJumpToken(0) ;
            } else if (jump_token == VERI_BREAK) {
                // It's a 'break' statement, so reset the jump token for the same reason
                // as that of the 'continue'. Also break out of the loop, we don't need
                // to execute anything within the loop.
                table->SetJumpToken(0) ;
                break ;
            }
            // We cannot use a 'switch' statement instead of the if-else-if statement here,
            // becasue we, then, cannot break from the outer loop, 'break' will break out
            // of the 'switch' statement, we need to break out from the loop and we have to
            // do another 'if' check! To optimize the above code we used the two lines below!
            */
            // If it is not a 'return', then it must be 'break' or 'continue' for which
            // we must execute statements after the loop, so clear the jump token.
            if (jump_token != VERI_RETURN) table->SetJumpToken(0) ;
            // If it's not a 'continue', it must be 'break' or 'return': break out of the loop.
            if (jump_token != VERI_CONTINUE) break ;
        }
        if (table->IsDisabled()) break ;

        FOREACH_ARRAY_ITEM(_reps, i, item) {
            ret_status = item ? item->StaticElaborateStmt(table, expect_nonconst): 1 ;
            if (!ret_status) return 0 ;
        }
    }

    return ret_status ;
}

unsigned VeriWhile::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 0 ;

    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    unsigned ret_status = 1 ;
    unsigned loopcount = 0 ;
    unsigned b_true = 0 ;
    unsigned jump_token = 0 ;

    // VIPER : 2947/4152 - honor user loop limit, if set by user
    unsigned user_loop_limit = veri_file::GetLoopLimit() ;
    unsigned loop_limit = user_loop_limit ? user_loop_limit : LOOPLIMIT ;

    while (loopcount++ < loop_limit) {
        // Evaluate the loop condition.
        VeriBaseValue *cond_val = _cond ? _cond->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;

        // If 'cond_val' is zero, the loop condition could not be evaluated.
        if (!cond_val) return 0 ;

        unsigned term_sign = 0 ;
        char *terminating_cond = cond_val->GetBinaryValue(&term_sign) ;

        // Check whether the terminating condition is 1. One infers 1.
        b_true = !ConstStrEvaluation::StrIsZero(terminating_cond) ;
        Strings::free(terminating_cond) ;
        delete cond_val ;

        if (!b_true) break ;

        // If b_true is 1, terminating condition is not reached yet, so the statement is executed.
        ret_status = _stmt ? _stmt->StaticElaborateStmt(table, expect_nonconst) : ret_status ;
        if (!ret_status) break ;

        // Get the token of the jump, if any
        jump_token = table->GetJumpToken() ;
        if (jump_token) {
            // If it is not a 'return', then it must be 'break' or 'continue' for which
            // we must execute statements after the loop, so clear the jump token.
            if (jump_token != VERI_RETURN) table->SetJumpToken(0) ;

            // If it's not a 'continue', it must be 'break' or 'return': break out of the loop.
            if (jump_token != VERI_CONTINUE) break ;
        }
        if (table->IsDisabled()) break ;
    }

    // VIPER #5270: Do not produce the error message when we are in disabled state:
    if (!expect_nonconst && b_true && !jump_token && _cond && !table->IsDisabled()) {
        _cond->Error("loop count limit of %d exceeded; condition is never false", loop_limit) ;
    }

    return ret_status ;
}

unsigned VeriRepeat::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 0 ;

    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    // Evaluate the repeat count
    VeriBaseValue *repeat_count_val = _cond ? _cond->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;
    if (!repeat_count_val) return 0 ;

    // Get the integer value: do not store it in usigned, it may be negative!
    int repeat_count = repeat_count_val->GetIntegerValue() ;
    delete repeat_count_val ;
    // VIPER #4231: Produce a warning message if the repeat count is negative:
    if (!expect_nonconst && (repeat_count < 0)) {
        if (_cond) _cond->Warning("repeat expression should evaluate to non-negative value") ;
    }

    // Repeat as many times as the designer wants, don't check the limit!
    //if (repeat_count > LOOPLIMIT) {
    //    if (_cond) _cond->Error("loop count limit exceeded; cannot loop any more") ;
    //    repeat_count = LOOPLIMIT ;
    //}

    int i ;
    unsigned ret_status = 1 ;
    for(i=0; i<repeat_count; i++) {
        // Execute the statement repeat_count times!
        ret_status = _stmt ? _stmt->StaticElaborateStmt(table, expect_nonconst) : ret_status ;

        // Do check for return status and dead code
        if (!ret_status) break ;
        // Get the token of the jump, if any
        unsigned jump_token = table->GetJumpToken() ;
        if (jump_token) {
            // If it is not a 'return', then it must be 'break' or 'continue' for which
            // we must execute statements after the loop, so clear the jump token.
            if (jump_token != VERI_RETURN) table->SetJumpToken(0) ;
            // If it's not a 'continue', it must be 'break' or 'return': break out of the loop.
            if (jump_token != VERI_CONTINUE) break ;
        }
        if (table->IsDisabled()) break ;
    }

    return ret_status ;
}

unsigned VeriForever::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 0 ;

    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    // VIPER : 2947/4152 - honor user loop limit, if set by user
    unsigned user_loop_limit = veri_file::GetLoopLimit() ;
    unsigned loop_limit = user_loop_limit ? user_loop_limit : LOOPLIMIT ;

    unsigned ret_status = 1 ;
    unsigned loopcount = 0 ;
    unsigned jump_token = 0 ;
    while (loopcount++ < loop_limit) {
        // Execute the statement forever
        ret_status = _stmt ? _stmt->StaticElaborateStmt(table, expect_nonconst) : ret_status ;

        // Do check for return status and dead code
        if (!ret_status) break ;
        jump_token = table->GetJumpToken() ;
        // Get the token of the jump, if any
        if (jump_token) {
            // If it is not a 'return', then it must be 'break' or 'continue' for which
            // we must execute statements after the loop, so clear the jump token.
            if (jump_token != VERI_RETURN) table->SetJumpToken(0) ;
            // If it's not a 'continue', it must be 'break' or 'return': break out of the loop.
            if (jump_token != VERI_CONTINUE) break ;
        }
        if (table->IsDisabled()) break ;
    }

    // VIPER #5270: Do not produce the error message when we are in disabled state:
    if (!expect_nonconst && !jump_token && ret_status && !table->IsDisabled()) {
        Error("loop count limit of %d exceeded; forever never breaks", loop_limit) ;
    }

    return ret_status ;
}

unsigned VeriBlockingAssign::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (table && table->IsDeadCode()) return 1 ;  // We are in dead code
    if (table) table->SetInsideBlockingAssign(1) ; // VIPER #2482, #3511: We are now inside blocking assigment

    int lsize = 0 ;
    int rsize = 0 ;
    unsigned left = 0 ;
    unsigned right = 0 ;
    VeriConstraint *target_constraint = 0 ;

    switch(_oper_type) {
    case VERI_EQUAL:
    case VERI_EQUAL_ASSIGN:
    {
        // VIPER #2626: For these two operators, the sign depends only on RHS
        lsize = _lval ? (int)_lval->StaticSizeSignInternal(table, 0) : 0 ;
        target_constraint = _lval ? _lval->EvaluateConstraintInternal(table, 0): 0 ;
        rsize = _val ? (int)_val->StaticSizeSignInternal(table, target_constraint) : 0 ;
        left = GET_CONTEXT_SIZE(lsize) ;
        right = GET_CONTEXT_SIZE(rsize) ;
        int sign = (rsize < 0) ? 1 : 0 ; // Sign doesn't depend on LHS
        int size = MAX(ABS(lsize), ABS(rsize)) ;
        lsize = rsize = (sign ? -size : size) ;
        break ;
    }
    case VERI_INC_OP:
    case VERI_DEC_OP:
    case VERI_PLUS_ASSIGN:
    case VERI_MIN_ASSIGN:
    case VERI_MUL_ASSIGN:
    case VERI_DIV_ASSIGN:
    case VERI_MOD_ASSIGN:
    case VERI_AND_ASSIGN:
    case VERI_OR_ASSIGN:
    case VERI_XOR_ASSIGN:
    {
        lsize = _lval ? (int)_lval->StaticSizeSignInternal(table, 0) : 0 ;
        rsize = _val ? (int)_val->StaticSizeSignInternal(table, 0) : 0 ;
        int sign = (lsize < 0) && (rsize < 0) ;
        int size = MAX(ABS(lsize), ABS(rsize)) ;
        lsize = rsize = (sign ? -size : size) ;
        break ;
    }
    case VERI_LSHIFT_ASSIGN:
    case VERI_ALSHIFT_ASSIGN:
    case VERI_RSHIFT_ASSIGN:
    case VERI_ARSHIFT_ASSIGN:
    case VERI_WITH:
        lsize = _lval ? (int)_lval->StaticSizeSignInternal(table, 0) : 0 ;
        // rsize is self determined
        break ;
    default:
        if (!expect_nonconst) Error("%s is not supported for static elaboration", PrintToken(_oper_type));
    }

    // If LHS is streaming concat, size of RHS cannot be less than LHS:
    if (_lval && _lval->IsStreamingConcat() && (left > right)) {
        if (!expect_nonconst) _lval->Error("width does not match, stream larger than %s", "value") ;
        if (table) table->SetInsideBlockingAssign(0) ; // VIPER #2482, #3511: We are leaving blocking assigment
        return 0 ;
    }

    if (_val && _val->IsStreamingConcat()) rsize = (left) ? MAKE_CONTEXT_SIZE_SIGN(left, 0) : rsize ;

    // We have to evaluate the LHS for a += b type blocking assignment, check it:
    VeriBaseValue *l_val = 0 ;
    switch (_oper_type) {
    case VERI_EQUAL:
    case VERI_EQUAL_ASSIGN:
        break ; // No need to evaluate the LHS for these operators
    case VERI_INC_OP:
    case VERI_DEC_OP:
        // Increment and declrement operators can be pre or post. For post increment
        // and decrement operators, we have no RHS and need to evaluate LHS:
        if (!_lval && _val) break ; // It's a pre increment/decrement operator, break
        // Otherwise, it's a post increment/decrement operator, evaluate LHS below

    default:
        // For all other operators we need to evaluate LHS also:
        l_val = (_lval) ? _lval->StaticEvaluateInternal(lsize, table, 0, expect_nonconst) : 0 ;
        break ;
    }

    // Evaluate the RHS of the assignment:
    VeriBaseValue *val = (_val) ? _val->StaticEvaluateInternal(rsize, table, target_constraint, expect_nonconst) : 0 ;
    delete target_constraint ; // Memory leak fix: delete it before the return statement
    if (_val && !val) return 0 ;

    unsigned ret_status = 1 ;

    // Both of the values cannot be zero!
    if (!l_val && !val) {
        if (table) table->SetInsideBlockingAssign(0) ; // VIPER #2482, #3511: We are leaving blocking assigment
        //Error("Non-constant function, evaluation failed\n") ;
        return 0 ;
    }

    // Now check if the operand is ++ or --. Then we have to evaluate it, return the
    // current value or the evaluated value depending on post or pre operation!
    if ((_oper_type == VERI_INC_OP) || (_oper_type == VERI_DEC_OP)) {
        // If it is a pre increment or decrement operator then _left is '0'. If it is
        // post increment or decrement operator then _right is '0'.
        // If right hand side is '0' we swap that with the left hand side, so that we
        // can use the normal addition and subtraction operators for this pre or post
        // increment or decrement operator. We create a VeriBasedNumber with value 1'b1 for the right hand side.
        if (_val) {
            // So, it is a pre increment or decrement operator.
            // Swap it now, otherwise '--b' could not be evaluated this way!
            VERIFIC_ASSERT(!l_val) ; // Just to be sure
            l_val = val ;
            val = 0 ;
        }
        VERIFIC_ASSERT(!val) ; // Just to make sure that we are not leaking
        val = new VeriBasedNumber("1'b1") ;
        // So, now we have both l_val and val. val is 1.
    }

    // Now evaluate the operator on both the values if it is not a simple '=' assign:
    if (!lsize) lsize = (_lval) ? (int)_lval->StaticSizeSignInternal(table, 0) : 0 ;
    if (!((_oper_type == VERI_EQUAL_ASSIGN) || (_oper_type == VERI_EQUAL))) {
        VeriBaseValue *r_val = val ;
        val = VeriBinaryOperator::StaticEvaluateBinary(l_val, r_val, lsize, _oper_type) ;
        delete r_val ;
    }
    delete l_val ;

    if (_lval) {
        // Assign the value into LHS for a=b, a+=b or a++ type blocking assign:
        ret_status = _lval->StaticAssign(table, val, expect_nonconst) ;
    } else if (_val && ((_oper_type == VERI_INC_OP) || (_oper_type == VERI_DEC_OP))) {
        // It is a pre-increment or pre-decrement operator, assign on the _val itself
        ret_status = _val->StaticAssign(table, val, expect_nonconst) ;
    }
    delete val ; // Clean up

    if (table) {
        // VIPER #2482, #3511: Reset blocking assign info and clear all the evaluated indexes:
        table->SetInsideBlockingAssign(0) ;
        table->ClearAllEvaluatedIndexes() ;
    }

    return ret_status ;
}
unsigned VeriWaitOrder::StaticElaborateStmt(ValueTable * /*eval*/, unsigned expect_nonconst)
{
    if (!expect_nonconst) Error("%s is not supported for static elaboration", "wait_order statement" ) ;
    return 0 ;
}
unsigned VeriForeach::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 0 ; // Nothing can be done without value table here
    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    // VIPER #3481: Support for foreach statement:

    // Quick check to see if at least one valid loop variable is specified:
    unsigned found_valid_index = 0 ;
    unsigned i ;
    VeriIdDef *loop_index ;
    FOREACH_ARRAY_ITEM(_loop_indexes, i, loop_index) {
        if (loop_index) {
            found_valid_index = 1 ;
            break ;
        }
    }

    if (!found_valid_index) {
        // No vaid loop index found, return from here:
        if (!expect_nonconst) Warning("no loop index variable found in foreach statement") ;
        return 0 ; // Failed
    }

    // Iterate over the dimensions with the id of the referenced array:
    return IterateDimension(table, ((_array) ? _array->GetId() : 0), 0 /* start from the 0th dimension */, expect_nonconst) ;
}
unsigned VeriForeach::IterateDimension(ValueTable *table, VeriIdDef *array_id, unsigned dimension, unsigned expect_nonconst)
{
    if (!table) return 0 ; // Nothing can be done without value table here
    if (table->IsDeadCode()) return 1 ;  // We are in dead code
    if (!array_id) return 0 ; // Can't do anything without this id

    // Check whether we reached the terminating condition of the recursion:
    if (!_loop_indexes || (dimension >= _loop_indexes->Size())) {
        // So, every loop index is properly set, just elaborate the statement:
        return (_stmt) ? _stmt->StaticElaborateStmt(table, expect_nonconst) : 1 ;
    }

    // Get the next available loop index after this position:
    VeriIdDef *loop_index = 0 ;
    unsigned iter_dim = dimension ;
    while (!loop_index && (_loop_indexes->Size() > iter_dim)) {
        loop_index = (VeriIdDef *)_loop_indexes->At(iter_dim) ;
        iter_dim++ ;
    }

    // Get the dimension of the array we want to traverse with this loop index:
    // Note 'iter_dim' is the next dimension that loop index represents.
    VeriRange *dim = array_id->GetDimensionAt(iter_dim-1) ;
    // Some quick checks to see if we can elaborate this foreach:
    VeriExpression *left = (dim) ? dim->GetLeft() : 0 ;
    VeriExpression *right = (dim) ? dim->GetRight() : 0 ;
    VeriIdDef *left_id = (left) ? left->GetId() : 0 ;
    VeriIdDef *right_id = (right) ? right->GetId() : 0 ;
    if (!dim || (!left && !right) ||
        (left && left->IsDollar()) || (right && right->IsDollar()) ||
        (left && left->IsDataType()) || (left_id && left_id->IsType()) ||
        (right && right->IsDataType()) || (right_id && right_id->IsType())) {
        // Associative array, range is data type, not supported:
        // CHECKME: Don't know how to traverse associative arrays:
        if (!expect_nonconst) array_id->Error("foreach statement on associative/dynamic array is not supported") ;
        loop_index = 0 ; // So that we skip this dimension
    }

    // NOTE: This is how simulators behave: If a loop index is missing,
    // it does not iterate other its range but skips it altogether:
    if (!loop_index) {
        // Do the same here, but recursively call myself for the next dimension:
        return IterateDimension(table, array_id, iter_dim, expect_nonconst) ;
    }

    // Evaluate its left/rigth bounds to get the index range domain:
    VERIFIC_ASSERT(dim) ; // Since we must have returned if it is not there
    int lbound = 0 ;
    int rbound = 0 ;
    unsigned size = dim->GetWidth(&lbound, &rbound, table) ;
    if (!size && expect_nonconst) return 0 ; // VIPER #7424

    // Now iterate over the current dimension range to set the value of the loop index:
    unsigned status = 1 ;
    for (int idx=lbound; ((lbound>rbound) ? idx>=rbound : idx<=rbound); ((lbound>rbound) ? idx-- : idx++)) {
        // Set the value of the index to the current index value:
        if (loop_index) {
            VeriBaseValue *val = new VeriInteger(idx) ;
            if (!table->Insert(loop_index, val)) status = 0 ;
            delete val ;
        }
        // Recursively call myself for the next dimension:
        status &= IterateDimension(table, array_id, iter_dim, expect_nonconst) ;
    }

    return status ;
}

unsigned VeriExpression::StaticAssign(ValueTable * /*table*/, VeriBaseValue * /*value*/, unsigned /*expect_nonconst*/)
{
    //Error("illegal expression in target") ;
    return 0 ;
}

unsigned VeriIdRef::StaticAssign(ValueTable *table, VeriBaseValue *val, unsigned /*expect_nonconst*/)
{
    if (!_id || !val || !table) return 0 ; // IdRef not resolved.

    VeriBaseValue *value = table->FetchValue(_id) ;
    if (!table->Insert(_id, val)) {
        return 0 ;
    }

    return (!value ? 0 : 1) ;
}

unsigned VeriIndexedId::StaticAssign(ValueTable *table, VeriBaseValue *val, unsigned expect_nonconst)
{
    if (!_prefix || !_idx || !_id || !val || !table) return 0 ;

    VeriBaseValue *prefix_value = _prefix->StaticEvaluateInternal(0, table, 0, expect_nonconst) ;
    if (!prefix_value) return 0 ;

    // VIPER #4872: Support indexed assign on string types:
    VeriDataType *id_data_type = _id->GetDataType() ;
    if (id_data_type && (id_data_type->GetType() == VERI_STRINGTYPE)) {
        VeriBaseValue *idx_val = _idx->StaticEvaluateInternal(0, table, 0, expect_nonconst) ;
        int index = (idx_val) ? idx_val->GetIntegerValue() : -1 ; // Default to -1
        delete idx_val ;
        const char *repl_str = val->GetString() ;
        const char *str = prefix_value->GetString() ;
        if (str && repl_str && (index>=0) && (index<(int)Strings::len(str))) {
            char *new_str = Strings::save(str) ;
            new_str[index] = repl_str[0] ; // Assign the character
            VeriAsciiString new_val(new_str) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(new_str) ;
            (void) table->Insert(_id, &new_val) ; // Value is copied by 'Insert'
        }
        delete prefix_value ;
        return 1 ;
    }

    Array indexes(1) ;
    indexes.InsertLast(_idx) ;
    VeriConstraint *constraint = _prefix->EvaluateConstraintInternal(table, 0) ;
    prefix_value = prefix_value->ToBasedNum(1) ;
    if (!StaticAssignIndexed(&indexes, prefix_value, table, val, constraint, expect_nonconst)) {
        delete constraint ;
        delete prefix_value ;
        return 0 ;
    }
    delete constraint ;
    unsigned s = _prefix->StaticAssign(table, prefix_value, expect_nonconst) ;
    delete prefix_value ;
    return s ;
}

unsigned VeriIndexedMemoryId::StaticAssign(ValueTable *table, VeriBaseValue *val, unsigned expect_nonconst)
{
    if (!_prefix || !_indexes || !val) return 0 ;

    // Evaluate the prefix expression:
    VeriBaseValue *prefix_value = _prefix->StaticEvaluateInternal(0, table, 0, expect_nonconst) ;
    if (!prefix_value) return 0 ;

    // VIPER #4872: Support indexed assign on string types:
    VeriDataType *id_data_type = _id ? _id->GetDataType(): 0 ;
    if (id_data_type && id_data_type->GetType() == VERI_STRINGTYPE) {
        if (_indexes->Size() > 2) { delete prefix_value ; return 1 ; } // Cannot assign with more than 2 dimensions
        // Process the first index to index the string:
        VeriExpression *idx = (_indexes->Size()) ? (VeriExpression *)_indexes->GetFirst() : 0 ;
        VeriBaseValue *idx_val = (idx) ? idx->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;
        int index = (idx_val) ? idx_val->GetIntegerValue() : -1 ; // Default to -1
        delete idx_val ;
        // Now, decide on the bit of the RHS value that will be used to replace:
        char repl_char = 0 ;
        if (val->Type() == ASCIISTR) {
            const char *str_val = val->GetString() ;
            repl_char = (str_val) ? str_val[0] : '0' ; // First character
        } else {
            unsigned sign = 0 ;
            char *bin_val = val->GetBinaryValue(&sign) ;
            repl_char = (bin_val) ? bin_val[Strings::len(bin_val)-1] : '0' ; // LSB
            Strings::free(bin_val) ;
        }
        unsigned repl_bit = (repl_char == '1') ? 1 : 0 ; // This is the final bit
        // Now process the second index to index the character resulting from previous index:
        idx = (_indexes->Size() > 1) ? (VeriExpression *)_indexes->At(1) : 0 ;
        idx_val = (idx) ? idx->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;
        int index2 = (idx_val) ? idx_val->GetIntegerValue() : -1 ; // Default to -1
        delete idx_val ;
        const char *str = prefix_value->GetString() ;
        // index2 must be within [0:7] range as it is indexing on char/byte:
        if (str && (index>=0) && (index<(int)Strings::len(str)) && (index2>=0) && (index2<8)) {
            char *new_str = Strings::save(str) ;
            char replace = new_str[index] ;
            // Now, we have to overwrite bit at position 'index2' with 'repl_bit':
            unsigned weight = 1<<index2 ;
            if (replace & (char)weight) {
                // This bit is already set.
                if (!repl_bit) {
                    // Need to reset it:
                    replace = replace - (char)weight ;
                }
            } else {
                // This bit is not set.
                if (repl_bit) {
                    // Need to set it:
                    replace = replace + (char)weight ;
                }
            }
            new_str[index] = replace ; // Overwrite the character
            VeriAsciiString tmp_val(new_str) ; // Automatic variable, will be deleted out-of-scope
            Strings::free(new_str) ;
            (void) table->Insert(_id, &tmp_val) ; // Value is copied by 'Insert'
        }
        delete prefix_value ;
        return 1 ;
    }

    VeriConstraint *constraint = _prefix->EvaluateConstraintInternal(table, 0) ;
    if (!constraint) constraint =  _id ? _id->EvaluateConstraintInternal(table, 0, 0):0 ;

    Array indexes(_indexes->Size()) ;
    unsigned i ;
    VeriExpression *idx = 0 ;
    FOREACH_ARRAY_ITEM_BACK(_indexes, i, idx) indexes.InsertLast(idx) ;

    prefix_value = prefix_value->ToBasedNum(1) ;

    if (!StaticAssignIndexed(&indexes, prefix_value, table, val, constraint, expect_nonconst)) {
        delete constraint ;
        delete prefix_value ;
        return 0 ;
    }
    delete constraint ;
    unsigned s = _prefix->StaticAssign(table, prefix_value, expect_nonconst) ;
    delete prefix_value ;
    return s ;
}
// VIPER #4310 : Rewrite index/part select expression evaluation/assign to support multi-dimensional array :
unsigned VeriName::StaticAssignIndexed(Array *indexes, VeriBaseValue *prefix_value, ValueTable *table, VeriBaseValue *val, VeriConstraint *prefix_constraint, unsigned expect_nonconst) const
{
    // Find the size of one element at this dimension
    // Start at the prefix, and walk MSB->LSB over the dimensions.
    VeriConstraint *range ;
    int left_bound = 0, right_bound = 0 ;
    unsigned value_size = 0, lsb_bit_offset = 0 ;
    unsigned elem_size = prefix_constraint ? prefix_constraint->NumOfBits(): 0 ;
    VeriConstraint *runner = prefix_constraint ;
    VeriExpression *index = 0 ;
    while (indexes->Size()) {
        index = (VeriExpression*)indexes->RemoveLast() ;
        if (!index) return 0 ;
        // Pick up the dimension into which we are indexing :
        range = runner ? runner->IndexConstraint(): 0 ; // dimension of the element
        // Get the range bounds. Inherit from the range, of size-1:0 if this is a scalar :
        left_bound = (range) ? range->Left() : (int)elem_size-1 ;
        right_bound = (range) ? range->Right() : 0 ;

        // Check how large the size of one element is. If this is a scalar, element size will always be 1 (or else we did calculations incorrectly). CHECK HERE
        elem_size = elem_size / (unsigned)((left_bound>right_bound) ? (left_bound-right_bound)+1 : (right_bound-left_bound)+1) ;
        runner = (runner) ? runner->ElementConstraint(): 0 ;

        // If no index constraint exists and bounds are 0, produce warning
        if (!left_bound && !right_bound && !range) { // ie. if we have a declaration like bit x ; and we try to access x[1]
            if (!expect_nonconst) Warning("cannot index into non-array %s", GetName()) ;
            return 0 ;
        }
        int left_idx = 0, right_idx = 0 ;
        unsigned elements_in_index = 0 ;
        unsigned always_out_of_bounds = 0 ;
        if (index->IsRange()) {
            // VIPER #2482, #3511: Single evaluation is handled within GetWidth:
            elements_in_index = index->GetWidth(&left_idx, &right_idx, table) ;
            if (!elements_in_index) return 0 ;
        } else {
            // VIPER #2482, #3511: Get the index value from the table if it is already evaluated:
            VeriBaseValue *v = (table) ? table->GetEvaluatedIndexValue(index) : 0 ;
            if (!v) v = index->StaticEvaluateInternal(0, table, 0, expect_nonconst) ;
            if (!v) return 0 ; // VIPER #4478 : Return null when index cannot be evaluated
            left_idx = v->GetIntegerValue() ;
            right_idx = left_idx ;
            delete v ;
        }
        // Check bounds :
        if ((left_bound>right_bound && left_idx<right_idx) ||
            (left_bound<right_bound && left_idx>right_idx)) {
            if (!index->GetPartSelectToken()) {
                if (!expect_nonconst) Error("index is always out of bounds for array %s", GetName()) ;
                always_out_of_bounds = 1 ; // VIPER #4543 set always out of bounds
            }
            // Swap the bounds.
            // This can happen legally for +:/-: part-selects, or if somebody suppressed the error above.
            int tmp = left_idx ;
            left_idx = right_idx ;
            right_idx = tmp ;
        }
        // Return from function if both the indexes are out of range
        if (CHECK_INDEX(left_idx, left_bound, right_bound) &&
                CHECK_INDEX(right_idx, left_bound, right_bound)) {
            if (!expect_nonconst) Warning("index is always out of bounds for array %s", GetName()) ;
            always_out_of_bounds = 1 ; // VIPER #4543 set always out of bounds
            //return 0 ;
        }
        // Check if the accessed element are out of range or not. If so send a warning.
        if (CHECK_INDEX(left_idx, left_bound, right_bound)) {
            if (!expect_nonconst) Warning("index %d is out of range [%d:%d] for %s", left_idx, left_bound, right_bound, GetName()) ;
        }
        // Check if the accessed element are out of range or not. If so send a warning.
        if (CHECK_INDEX(right_idx, left_bound, right_bound)) {
            if (!expect_nonconst) Warning("index %d is out of range [%d:%d] for %s", right_idx, left_bound,  right_bound, GetName()) ;
        }
        if (indexes->Size() == 0) { // Last index
            unsigned offset = 0 ;
            if (index->IsRange()) {
                if (left_bound > right_bound) {
                    if (right_idx >= right_bound) {
                        offset = (unsigned)(right_idx - right_bound) ;
                    } else {
                        int x = right_bound - right_idx ;
                        x = (int)elements_in_index - x ;
                        // VIPER #4543: Check that we are not casting a -ve number to unsigned:
                        if (x >= 0) elements_in_index = (unsigned)x ;
                    }
                } else {
                    if (right_bound >= right_idx) {
                        offset = (unsigned)(right_bound - right_idx) ;
                    } else {
                        int x = right_idx - right_bound ;
                        // VIPER #4543: Check that we are not casting a -ve number to unsigned:
                        x = (int)elements_in_index - x ;
                        if (x >= 0) elements_in_index = (unsigned)x ;
                    }
                }

                // VIPER #4543: If it is always out-of-bounds, set offset to size so that all x's are returned for out-of-bounds index:
                if (always_out_of_bounds) offset = (unsigned)((left_bound>right_bound) ? (left_bound-right_bound)+1 : (right_bound-left_bound)+1) ;

                // offset in bits is multplied by element size, and added to existing lsb_offset :
                lsb_bit_offset = lsb_bit_offset + offset*elem_size ;
                value_size = elem_size*elements_in_index ;
            } else {
                offset = (unsigned)((left_bound>right_bound) ? left_idx - right_bound: right_bound - left_idx) ;
                lsb_bit_offset = lsb_bit_offset + elem_size*offset ;
                value_size = elem_size ;
            }
            if (prefix_value->Type() == ARRAY) {
                if (right_idx == left_idx) {
                    int pos = (left_bound > right_bound) ? (left_bound-left_idx) : (left_idx -left_bound) ;
                    VeriBaseValue *adjusted_value = val->Copy() ;
                    adjusted_value = adjusted_value->Adjust(value_size, 0, 1, 0) ;
                    if (pos >= 0) prefix_value->SetValueAt(adjusted_value, (unsigned)pos) ;
                } else {
                    unsigned i ;
                    int index1 = 0 ;
                    VeriBaseValue *vv = val->ToArray(elements_in_index, value_size) ;
                    for (i=0; i < elements_in_index; i++) {
                        index1 = (left_bound > right_bound) ? ((left_bound-left_idx)+(int)i) : ((left_idx -left_bound)+(int)i) ;
                        if (index1 < 0) continue ; // out of range selection, cannot assign anything
                        VeriBaseValue *v = vv->TakeValueAt(i) ;
                        prefix_value->SetValueAt(v, (unsigned)index1) ;
                    }
                    delete vv ;
                }
            } else {
                if (!prefix_value->Assign(val, value_size, lsb_bit_offset)) return 0 ;
            }
        } else {
            if (index->IsRange()) {
                if (!expect_nonconst) index->Warning("range expressions not allowed in hierarchical pathnames") ;
                right_idx = left_idx ; // Only take the left range
            }
            if (prefix_value->Type() == ARRAY) {
                int pos = (left_bound > right_bound) ? (left_bound-left_idx) : (left_idx -left_bound) ;
                if (pos >= 0) prefix_value = prefix_value->GetValueAt((unsigned)pos) ;
            } else {
                unsigned offset = (unsigned)((left_bound>right_bound) ? left_idx - right_bound: right_bound - left_idx) ;
                lsb_bit_offset = lsb_bit_offset + elem_size*offset ;
                value_size = elem_size ;
            }
        }
    }
    return 1 ;
}

unsigned VeriConcat::StaticAssign(ValueTable *eval, VeriBaseValue *val, unsigned expect_nonconst)
{
    if (!val || !_exprs) return 0 ; // Nothing to assign
    unsigned status = 0 ;

    unsigned b_sign = 0 ;
    char *r_str = val->GetBinaryValue(&b_sign) ;
    if (!r_str) return 0 ; //Error("Non-constant function, evaluation failed\n") ;
    unsigned len = (unsigned)Strings::len(r_str) ;
    int index = (int)len - 1 ;
    unsigned i, j ;

    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM_BACK(_exprs, i, expr) {
        if (!expr) continue ;
        int size_sign = (int)expr->StaticSizeSignInternal(eval, 0) ;
        unsigned size = (unsigned)ABS(size_sign) ;
        b_sign = (size_sign < 0) ? 1 : 0 ;
        char *str = Strings::allocate(size + 1) ;
        int count = (int)size - 1 ;
        VeriBaseValue *r_val = 0 ;

        for(j = 0; j < size; j++) {
            str[count] = (index >= 0) ? r_str[index] : '0' ;
            index-- ;
            count-- ;
        }
        str[size] = '\0' ;
        len = (unsigned)Strings::len(str) ;
        char *prefix = Strings::itoa((int)len) ;
        char *val_str = Strings::save(prefix, "'b", str) ;
        VeriBaseValue *value = new VeriBasedNumber(val_str) ;
        Strings::free(val_str) ;
        Strings::free(prefix) ;
        r_val = value ;
        // Value corresponding to the lhs of this expression is ready, now insert the
        // value corresponding to the identifier in the value table.
        status = expr->StaticAssign(eval, r_val, expect_nonconst) ;
        delete r_val ;
        Strings::free(str) ;
    }
    Strings::free(r_str) ;

    return status ;
}

unsigned
VeriSelectedName::StaticAssign(ValueTable *table, VeriBaseValue *val, unsigned expect_nonconst)
{
    if (!val || !table) return 0 ;

    if (!_prefix_id || !_suffix_id) {
        if (_prefix_id) {
            // Suffix was not resolved. Error out now.
            if (!expect_nonconst) Error("external reference %s not supported for static elaboration", _prefix_id->Name()) ;
        }
        return 0 ;
    }

    // Get the old value if any!
    VeriBaseValue *old_val = table->FetchValue(_prefix_id) ;

    // Check if this is struct/union member selection. Check the data type of prefix
    VeriDataType *data_type = _prefix_id->GetDataType() ;
    if (data_type && ((data_type->Type() == VERI_STRUCT) || (data_type->Type() == VERI_UNION))) {
        // So, it is now part of a variable. If the prefix is an union, it gets the whole value
        //if (_prefix_id->Type() == VERI_UNION) {
        if (data_type->IsUnionType()) {
            return _prefix ? _prefix->StaticAssign(table, val, expect_nonconst) : 0 ;
        }

        // Here, its a structure. We have to partially assign to the _prexid_id for only the
        // _suffix_id. Calculate where (in bits from LSB) this member starts. This is currently
        // a bit tricky. Might need to build an API function, or annotate offset on the member id.

        // Get the scope of the data type
        VeriScope *member_scope = data_type->GetMemberScope() ;
        Map *members = (member_scope) ? member_scope->DeclArea() : 0 ;

        // Run FRONT-to-BACK (MSB->LSB) to find offset from MSB for static elab : Since we are
        // using string representation we need to know the starting point of the suffix id from
        // the MSB side in whole variable.
        unsigned offset = 0 ;
        MapIter mi ;
        VeriIdDef *id ;
        int pos = -1 ;
        FOREACH_MAP_ITEM(members, mi, 0, &id) {
            if (!id) continue ;
            pos++ ;
            if (id==_suffix_id) break ;
            int id_ss = (int)id->StaticSizeSignInternal(table, 0) ;
            offset += (unsigned)ABS(id_ss) ;
        }

        // Ok, suffix starts at position 'offset' and has size 'member_size'
        int suffix_id_ss = (int)_suffix_id->StaticSizeSignInternal(table, 0) ;
        unsigned member_size = (unsigned)ABS(suffix_id_ss) ;

        // Prefix must be equal or larger than sum of its member upto Suffix
        // VIPER #7936: Do not calculate size-sign of prefix-id, prefix may be
        // and indexed-id too. So, calculate size-sign of the prefix itself:
        //int prefix_ss = (int)_prefix_id->StaticSizeSignInternal(table, 0) ;
        int prefix_ss = (_prefix) ? (int)_prefix->StaticSizeSignInternal(table, 0) : 0 ;
        unsigned prefix_size = (unsigned)ABS(prefix_ss) ;
        VERIFIC_ASSERT(prefix_size >= (offset+member_size)) ;

        // Now evaluate the value of the prefix. We are going to modify it with
        // the value by replacing the to be assigned value in the right place.
        VeriBaseValue *prefix_val = _prefix ? _prefix->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;

        // Nothing that we can do if the prefix value is not constant.
        if (!prefix_val) return 0 ;

        // Now we have to replace the value corresponding to this member in the whole
        // value with the new value specified. So, get the string representation of both the values:
        VeriBaseValue *new_val = 0 ;
        if (prefix_val->Type() == ARRAY) {
            new_val = prefix_val->Copy() ;
            new_val->SetValueAt(val->Copy(), (unsigned)pos) ;
        } else {
            unsigned b_sign ;
            char *org_val_str = prefix_val->GetBinaryValue(&b_sign) ;
            char *new_val_str = val->GetBinaryValue(&b_sign) ;

            // Now create the value replacing the old value of this memeber with the new one
            new_val = table->CreateValue(org_val_str, new_val_str, offset,
                                  offset+member_size-1, 0, prefix_size-1, 0) ;
            // Free up the binary value strings for both the values
            Strings::free(org_val_str) ;
            Strings::free(new_val_str) ;
        }

        // Assign this value to the prefix
        unsigned status = _prefix ? _prefix->StaticAssign(table, new_val, expect_nonconst) : 0 ;

        // Delete these values, we don't need them any more!
        delete new_val ;
        delete prefix_val ;

        return status ;
    }

    // Just insert the value against the suffix id
    if (!table->Insert(_suffix_id, val)) return 0 ;

    return (old_val ? 1 : 0) ;
}
unsigned
VeriFunctionCall::StaticAssign(ValueTable * /*table*/, VeriBaseValue * /*val*/, unsigned /*expect_nonconst*/)
{
    return 0 ; // Function call cannot be lvalue of any assignment
}
unsigned
VeriStreamingConcat::StaticAssign(ValueTable *table, VeriBaseValue *val, unsigned expect_nonconst)
{
    if (!val) return 0 ; // Nothing to assign
    // For streaming operator if coming value contains less number of bits
    // than required by this expression, it is an error.
    // If streaming operator is '>>', we have to adjust the size of coming value and
    // assign that to concat elements just like concat
    if (_stream_operator == VERI_RSHIFT) return VeriConcat::StaticAssign(table, val, expect_nonconst) ;

    // If streaming operator is '<<' and size of value to be assigned is larger
    // than this streaming concatenation size, value is to be resized by truncating
    // from right hand side. After truncation we can simply apply the streaming operator
    // on that value and assign process value on streaming concat. i.e.
    // {<< 4 {a, b, c, d}} = 12'b0000_0001_0010_0100 ;
    // we will do
    // {a, b, c, d} = {<< 4 {12'b0000_0001_0010_0100}} ;

    // Get the size of this expression
    int size = (int)StaticSizeSignInternal(table, 0) ;
    unsigned target_size = GET_CONTEXT_SIZE(size) ;

    unsigned sign = 0 ;
    // Get bit-stream from value
    char *stream = val->GetBinaryValue(&sign) ;
    if (!stream) {
        delete val ;
        return 0 ;
    }
    unsigned val_size = Strings::len(stream) ;
    // Break the stream into slices with specified number of bits and streamed slices
    // in specified order :
    stream = StaticStreamingRight2Left(stream, table, expect_nonconst) ;
    if (!stream) return 0 ;
    // If incoming bit-stream size is larger than target size, truncate extra bits :
    char *passed_str = stream ;
    if (val_size > target_size) {
        stream = stream + (val_size - target_size) ; // Terminate string at target size
    }

    // Create based number with rearranged stream :
    char *s = Strings::itoa((int)target_size) ;
    char *x = Strings::save(s, "'b", stream) ;
    Strings::free(s) ;
    Strings::free(passed_str) ;
    VeriBaseValue *value = new VeriBasedNumber(x) ;
    Strings::free(x) ;
    // Assign modified value to elements of concat :
    unsigned ret_val = VeriConcat::StaticAssign(table, value, expect_nonconst) ;
    delete value ;
    return ret_val ;
}
char *VeriStreamingConcat::StaticStreamingRight2Left(char *str, ValueTable *val_table, unsigned expect_nonconst) const
{
    if (!str) return 0 ; // stream concatenation cannot be evaluated

    // First determine the size into which data is to be sliced
    unsigned slice_size = 1 ; // Default slice size

    if (_slice_size) { // 'slice_size' will be size of this data type/expression
        // If _slice_size is data type, take size of data type
        VeriIdDef *id = _slice_size->GetId() ;
        if (_slice_size->IsDataType() || (id && id->IsType())) {
            int size = (int)_slice_size->StaticSizeSignInternal(val_table, 0) ;
            slice_size = (unsigned)ABS(size) ;
        } else {
            VeriBaseValue *val = _slice_size->StaticEvaluateInternal(0, val_table, 0, expect_nonconst) ;
            if (!val) { // Produce error if slice-size is not constant
                if (!expect_nonconst) _slice_size->Error("%s is not a constant", "slice_size") ;
                return 0 ;
            }
            int size = val->GetIntegerValue() ;
            delete val ;
            if (size <= 0) {
                if (!expect_nonconst) _slice_size->Error("zero or negative value for size") ;
                return 0 ;
            }
            slice_size = (unsigned)ABS(size) ;
        }
    }

    unsigned str_len = Strings::len(str) ;
    char *arranged_str = Strings::allocate(str_len) ;
    arranged_str[0] = 0 ;

    unsigned i, j, idx = 0 ;
    unsigned start_pos = 0 ;
    // Break the bit-stream into slices with specified number of bits and arrange slices
    for (i = (str_len - 1); i >= slice_size; i = i - slice_size) {
        start_pos = i - (slice_size - 1) ;
        for(j = start_pos; j <= i; j++) {
            arranged_str[idx] = str[j] ;
            idx++ ;
        }
    }
    if (i <= slice_size) {
        start_pos = 0 ;
        while (start_pos <= i) {
            arranged_str[idx] = str[start_pos] ;
            start_pos++ ;
            idx++ ;
        }
    }
    arranged_str[idx] = 0 ;
    Strings::free(str) ;
    return arranged_str ;
}
// VIPER #5710 : Replace formals with actual and evaluate the expression associated with let construct
VeriBaseValue *VeriLetDecl::StaticElaborateFunction(Array *arg_list, ValueTable *table, unsigned expect_nonconst)
{
    if (!_id) return 0 ;
    if (_id->IsProcessing()) return 0 ; // recursion

    _id->SetIsProcessing() ;

    unsigned i ;
    VeriExpression *expr = 0 ;
    // LRM 3.1a (draft5), section 10.4.4, page 111: If both positional and named arguments
    // are specified in a single subroutine call, then all the positional arguments must
    // come before the named arguments. It keep tracks of positional/named arguments.
    // Resolve the actuals, and check formals against actuals :
    VeriIdDef *formal ;
    Set connected_formals(POINTER_HASH, (_ports) ? _ports->Size() : 1) ;
    FOREACH_ARRAY_ITEM(arg_list, i, expr) {
        if (!expr) continue ; // unconnected actual

        // Find the formal
        formal = 0 ;
        if (expr->NamedFormal()) {
            // This is a (SV-style) named formal
            formal = (_scope) ? _scope->FindLocal(expr->NamedFormal()): 0 ;
            expr = expr->GetConnection() ;
        } else {
            // This is a ordered formal
            if (i>= ((_ports) ? _ports->Size() : 0)) {
                break ; // no use checking more
            }
            formal = (_ports && _ports->Size() > i) ? (VeriIdDef*)_ports->At(i) : 0 ;
        }
        if (!formal) continue ;
        // Concentrate on inputs only (and inouts). Basically ignore outputs.
        if (formal->IsOutput()) continue ;

        // Flag this formal as connected.
        (void) connected_formals.Insert(formal) ;
        formal->SetActual(expr) ;
    }

    // Now check if all formals have an actual or initial value
    if (((_ports) ? _ports->Size() : 0) != connected_formals.Size()) {
        FOREACH_ARRAY_ITEM(_ports, i, formal) {
            if (!formal || formal->IsOutput()) continue ;
            if (connected_formals.GetItem(formal)) continue ;
            // Pick up the initial value
            expr = formal->GetInitialValue() ;
            if (!expr) continue ;

            formal->SetActual(expr) ;
        }
    }
    VeriBaseValue *val = (_expr) ? _expr->StaticEvaluateInternal(0, table, 0, expect_nonconst): 0 ;
    FOREACH_ARRAY_ITEM(_ports, i, formal) {
        if (!formal || formal->IsOutput()) continue ;
        formal->SetActual(0) ;
    }
    _id->ClearIsProcessing() ;
    return val ;
}

VeriBaseValue *VeriFunctionDecl::StaticElaborateFunction(Array *arg_list, ValueTable *table, unsigned expect_nonconst)
{
    // VIPER #3299: If this function cannot be a constant function, don't try to evaluate.
    // We may go into an infinite recursion leading to an unwanted crash which is not good:
    if (!_can_be_const_func) {
        //Error("function %s is not a valid constant function", (_func) ? _func->Name() : "") ;
        return 0 ; // Return 0 to indicate non-constant function
    }

    // VIPER #3893: Do not evaluate a virtul function, we might evaluate the wrong one:
    if (GetQualifier(VERI_VIRTUAL)) return 0 ;

    VeriBaseValue *val = 0 ;
    static unsigned counter = 0 ;

    ValueTable current_table ; // Always create a value table to handle recursion

    // Initializes ports with the supplied values from the function call arguments.
    // VIPER #3614: Do not try to evaluate the body if we cannot initialize the ports with constants:
    if (!InitializePorts(arg_list, table, &current_table, expect_nonconst)) return 0 ;
    // Initializes local declarations with the values, if initialized or with pointer NULL.
    if (!InitializeDecls(_decls, &current_table, expect_nonconst)) return 0 ;
    
    // Entering function identifier.
    unsigned status = 1 ;
    VeriBaseValue *value = table ? table->FetchValue(_func): 0 ;
    if (!value) status = 0 ;
    if (!current_table.Insert(_func, 0)) return 0 ;

    // if status equals 1, recursion exists.
    if (status) counter++ ;

    // If counter exceeds the defined RECURSION_LIMIT error is produced.
    // VIPER #7125: Honour user stack limit, if set by user:
    // Default value is set while adding the runtime variable. If value 0
    // is set by user, no recursion will be supported.
    unsigned user_stack_limit = veri_file::GetMaxStackDepth() ;
    //unsigned stack_limit = (user_stack_limit) ? user_stack_limit : RECURSION_LIMIT ;
    if (counter > user_stack_limit) {
        //Error("stack overflow on recursion via %s", (_func)?_func->Name():"<un-known>") ;
        return 0 ;
    }

    // VIPER #8011: Extract scope and push into stack
    VeriScope *save_scope = VeriNode::_present_scope ;
    if(GetScope()) VeriNode::_present_scope = GetScope() ;

    // VeriStatement::StaticElaborateStmt is called to execute the function statements.
    unsigned ret_status = ElaborateStatements(_stmts, &current_table, expect_nonconst) ;

    // VIPER #8011: Retrive the scope back
    VeriNode::_present_scope = save_scope ;

    // If 'ret_status' is 1, that is, execution of the function statement is performed
    // successfully, the entry against the function identifier in the ValueTable is
    // the value of the constant function.
    if (ret_status) val = current_table.FetchValue(_func) ;

    if (!val) return 0 ;

    VeriBaseValue *ret_val = 0 ;
    // According to the return type of the function, VeriBaseValue is created and returned.
    unsigned func_type = _data_type ? _data_type->Type() : 0 ;
    switch(func_type) {
    case VERI_REAL :
    case VERI_SHORTREAL : // VIPER #8421: Also check for shortreal type here
        {
        double real_val = val->GetRealValue() ;
        ret_val = new VeriReal(real_val) ;
        break ;
        }
    case VERI_INT :
        {
        int int_val = val->GetIntegerValue() ;
        ret_val = new VeriInteger(int_val) ;
        break ;
        }
        // Check if the data type is 'integer' or not. If so, then return the
        // value obtained from GetIntegerValue() only if 'val' doesn't contain
        // any x or z values. This is so as 'integer' is a 4-state variable
        // while GetIntegerValue() only returns a 2-state value.
    case VERI_INTEGER :
        {
        if (!val->HasXZ()) { // Check if value contains 'x'/'z'
            int int_val = val->GetIntegerValue() ;
            ret_val = new VeriInteger(int_val) ;
        } else {
            // Copy the value
            ret_val = val->Copy() ;
        }
        break ;
        }
    default :
        // Copy the value if none of the above conditions hold true
        ret_val = val->Copy() ;
    }

    if (counter) counter-- ;

    return ret_val ;
}

unsigned VeriJumpStatement::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (table && table->IsDeadCode()) return 1 ;  // We are in dead code

    if (!table) {
        // Value table does not exist, may be this jump statement has been used out of context
        // Warning("%s statement ignored in this situation", PrintToken(_jump_token)) ;
        return 0 ;
    }

    unsigned ret_status = 1 ;
    if (_expr) {
        // We have something to return
        if (_jump_id) {
            // VIPER #6693: Determine the context size/sign:
            // IEEE 1800 (2009) section 10.8: return statement is an assignment like
            // context. So, size is Max(LHS, RHS) and sign is that of RHS only:
            int context_size_sign = (int)_jump_id->StaticSizeSignInternal(table, 0) ;
            unsigned context_size = GET_CONTEXT_SIZE(context_size_sign) ;
            int expr_size_sign = (int)_expr->StaticSizeSignInternal(table, 0) ;
            unsigned expr_size = GET_CONTEXT_SIZE(expr_size_sign) ;
            unsigned expr_sign = GET_CONTEXT_SIGN(expr_size_sign) ;
            context_size = MAX(context_size, expr_size) ; // Take max of the two
            context_size_sign = MAKE_CONTEXT_SIZE_SIGN(context_size, expr_sign) ;
            // Evaluate the return value using context size/sign calculated above (VIPER #6693):
            VeriBaseValue *val = _expr->StaticEvaluateInternal(context_size_sign, table, 0, expect_nonconst) ;
            // Insert it against the function id, which is the _jump_id
            if (!table->Insert(_jump_id, val)) ret_status = 0 ;
            if (!val) ret_status = 0 ; // Set the return status to 0 if we are unable to evaluate the return value
            delete val ;
        } else {
            // Function id is still not resolved! Cannot continue
            //Warning("%s statement ignored in this situation", "value of the return") ;
        }
    }

    // Set dead code with the jump token, so that we don't execute any other statement in the context of the jump
    table->SetJumpToken(_jump_token) ;

    return ret_status ;
}

VeriBaseValue *
VeriConcatItem::StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const
{
    // Without knowing the target, we cannot do anything. All 'formal' handling should be done at the caller level

    if (!_member_label) return 0 ; // VIPER #2992. If the member is specified as default then return 0.

    // VIPER #2615. Try to evaluate the expression. Ignore the label.
    VeriBaseValue *val = (_expr) ? _expr->StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst) : 0 ;

    // If the expression is constant then issue warning.
    if (val && !expect_nonconst) Warning("concatenation member label not yet supported; label ignored") ; // Report that we don't do that yet.

    return val ;
}

unsigned
VeriDoWhile::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 0 ;

    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    // VIPER : 2947/4152 - honor user loop limit, if set by user
    // VIPER #7568 : Depend on user specific or default loop limit from runtime flag
    unsigned user_loop_limit = veri_file::GetLoopLimit() ;
    unsigned loop_limit = user_loop_limit ? user_loop_limit : LOOPLIMIT ;

    unsigned ret_status = 1 ;
    unsigned b_true = 1 ;
    unsigned loopcount = 0 ;
    do {
        if (loopcount++ >= loop_limit) {
            if (!expect_nonconst && _cond) {
                _cond->Error("loop count limit of %d exceeded; condition is never false", loop_limit) ;
            }
            break ;
        }

        // First execute the statement: statement is always executed once even if the condition is false
        ret_status = _stmt ? _stmt->StaticElaborateStmt(table, expect_nonconst) : ret_status ;
        if (!ret_status) break ;

        unsigned jump_token = table->GetJumpToken() ;
        // Get the token of the jump, if any
        if (jump_token) {
            // If it is not a 'return', then it must be 'break' or 'continue' for which
            // we must execute statements after the loop, so clear the jump token.
            if (jump_token != VERI_RETURN) table->SetJumpToken(0) ;
            // If it's not a 'continue', it must be 'break' or 'return': break out of the loop.
            if (jump_token != VERI_CONTINUE) break ;
            // Condition is evaluated even if it is a 'continue' statement, So. don't use a C
            // continue statement here for VERI_CONTINUE. Process normaly for that
        }
        if (table->IsDisabled()) break ;

        // Evaluate the loop condition.
        VeriBaseValue *cond_val = _cond ? _cond->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;
        if (!cond_val) return 0 ;

        unsigned term_sign = 0 ;
        char *terminating_cond = cond_val->GetBinaryValue(&term_sign) ;

        // Check whether the terminating condition is 1. One infers 1.
        b_true = !ConstStrEvaluation::StrIsZero(terminating_cond) ;
        Strings::free(terminating_cond) ;
        delete cond_val ;
    } while (b_true) ; // Use b_true as the terminating condition!

    return ret_status ;
}

unsigned VeriDataDecl::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!table) return 1 ;
    if (table->IsDeadCode()) return 1 ;  // We are in dead code

    unsigned i ;
    VeriIdDef *id ;
    unsigned status = 1 ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id || id->IsPort()) continue ;
        VeriTypeInfo *type = id->CreateType(0) ;
        if (type && type->IsDynamicArrayType()) {
            if (!expect_nonconst) Warning("%s is not supported for static elaboration", "dynamic array in constant function") ;
            status = 0 ;
        } else if (type && type->IsAssociativeArrayType()) {
            if (!expect_nonconst) Warning("%s is not supported for static elaboration", "associative array in constant function") ;
            status = 0 ;
        } else if (type && type->IsQueue()) {
            if (!expect_nonconst) Warning("%s is not supported for static elaboration", "queue in constant function") ;
            status = 0 ;
        }
        unsigned is_4state_var = type ? type->Is4State(): 0 ;
        delete type ;
        if (status == 0) return 0 ;
        VeriBaseValue *val = 0 ;

        VeriExpression *rhs = id->GetInitialValue() ;
        VeriConstraint *id_constraint = id->EvaluateConstraintInternal(table, 0, 0) ;
        val = rhs ? rhs->StaticEvaluateInternal(0, table, id_constraint, expect_nonconst): 0 ;
        delete id_constraint ;
        if (rhs && !val) return 0 ;  // VIPER #7424
        // VIPER #5717 (VCS Compatibility issue): If initial value is not specified for
        // loop index variable, consider it 0 for 2 state variable and x for 4 state variable
        if (!val) {
            if (is_4state_var) {
                val = new VeriBasedNumber("1'bx") ;
            } else {
                val = new VeriInteger(0) ;
            }
        }

        if (!table->Insert(id, val)) {
            delete val ;
            return 0 ;
        }
        delete val ;
    }

    return 1 ;
}

unsigned VeriGenVarAssign::StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst)
{
    if (!_id) return 0 ; // genvar id not resolved!

    // Evaluate the right hand side of the assignment.
    VeriBaseValue *r_val = _value ? _value->StaticEvaluateInternal(0, table, 0, expect_nonconst) : 0 ;
    if (!r_val) {
        if (!expect_nonconst) Error("constant expression required") ;
        return 0 ;
    }

    VeriBaseValue *l_val = (table) ? table->FetchValue(_id): 0 ;
    if (!l_val && (!((_oper_type == VERI_EQUAL) || (_oper_type == VERI_EQUAL_ASSIGN)))) {
        // Previous value must exists if it is not a simple '=' assign statement
        if (!expect_nonconst) Error("cannot operate on uninitialized genvar") ;
        delete r_val ;
        return 0 ;
    }

    VeriBaseValue *val = 0 ;

    switch(_oper_type) {
    case VERI_EQUAL:
    case VERI_EQUAL_ASSIGN:
        val = r_val ;
        break ;
    case VERI_PLUS_ASSIGN:
    case VERI_MIN_ASSIGN:
    case VERI_MUL_ASSIGN:
    case VERI_DIV_ASSIGN:
    case VERI_MOD_ASSIGN:
    case VERI_AND_ASSIGN:
    case VERI_OR_ASSIGN:
    case VERI_XOR_ASSIGN:
    case VERI_LSHIFT_ASSIGN:
    case VERI_ALSHIFT_ASSIGN:
    case VERI_RSHIFT_ASSIGN:
    case VERI_ARSHIFT_ASSIGN:
    {
        int context_size = -32 ; // Genvar ids are 32 bit signed value
        int rsize = (_value) ? (int)_value->StaticSizeSignInternal(table, 0) : 0 ;
        if (ABS(rsize) > ABS(context_size)) context_size = rsize ;
        val = VeriBinaryOperator::StaticEvaluateBinary(l_val, r_val, context_size, _oper_type) ;
        delete r_val ;
        break ;
    }
    default:
        VERIFIC_ASSERT(0) ;
    }

    if (table && !table->Insert(_id, val)) {
        delete val ;
        return 0 ;
    }
    delete val ;

    return 1 ;
}

VeriBaseValue *
VeriSelectedName::StaticEvaluateInternal(int context_size, ValueTable *n, VeriConstraint * target_constraint, unsigned expect_nonconst) const
{
    if (!_prefix || !_prefix_id) return 0 ;

    if (!_suffix_id) {
        // This may be a built-in method call like str.len() where str is a string type object!
        if (GetFunctionType()) {
            // So, this is a method call without the parenthesis, try to evaluate the function.
            // For this create a temporary VeriFunctionCall object...
            VeriMapForCopy copy_tab ; // Map for copying 'this'
            VeriName *func_name = CopyName(copy_tab) ; // Copy myself, destructor of VeriFunctionCall will delete it.
            VeriFunctionCall built_in_method(func_name, 0 /* no argument */) ;
            // and call StaticEvaluateInternal() on this object and return the value, that will do the trick!
            // The automatic varibale built_in_method will be autmatically deleted and will delete the copied name.
            return (built_in_method.StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst)) ;
        }
        // Cannot do anything if the _suffix_id is not resolved!
        return 0 ;
    }

    // Check if this is a selected name into some variable :

    // For that, check if the prefix has a data type :
    VeriDataType *data_type = _prefix_id->GetDataType() ;
    if (data_type && _suffix_id->IsMember()) {
        // The prefix is a variable, so first evaluate the prefix
        VeriBaseValue *prefix_value = _prefix->StaticEvaluateInternal(0, n, 0, expect_nonconst) ;
        if (!prefix_value) return 0 ;

        // If the prefix is an union, it gets the whole value
        //if (_prefix_id->Type() == VERI_UNION) return prefix_value ;
        if (data_type->IsUnionType()) return prefix_value ;

        // Its a structure. Calculate where (in bits from LSB) this member starts.
        // This is currently a bit tricky. Might need to build a API function, or annotate offset on the member id.

        // Get the scope of the data type
        VeriScope *member_scope = data_type->GetMemberScope() ;
        Map *members = (member_scope) ? member_scope->DeclArea() : 0 ;

        // Run FRONT-to-BACK (MSB->LSB) to find offset from MSB for static elab :
        // Since we are using string representation we need to know the starting
        // point of the suffix id from the MSB side in whole variable.
        unsigned offset = 0 ;
        MapIter mi ;
        VeriIdDef *id ;
        int pos = -1 ;
        FOREACH_MAP_ITEM(members, mi, 0, &id) {
            if (!id) continue ;
            pos++ ;
            if (id==_suffix_id) break ;
            int id_ss = (int)id->StaticSizeSignInternal(n, 0) ;
            offset += (unsigned)ABS(id_ss) ;
        }

        VeriBaseValue *ret_val = 0 ;
        if (prefix_value->Type() == ARRAY) {
            ret_val = prefix_value->GetValueAt((unsigned)pos) ;
            ret_val = (ret_val) ? ret_val->Copy(): 0 ;
        } else {
            // Ok, suffix starts at position 'offset' and has size 'member_size'
            int suffix_id_ss = (int)_suffix_id->StaticSizeSignInternal(n, target_constraint) ;
            unsigned member_size = (unsigned)ABS(suffix_id_ss) ;

            // Prefix must be equal or larger than sum of its member upto Suffix
            int prefix_id_ss = (int)_prefix_id->StaticSizeSignInternal(n, 0) ;
            unsigned prefix_size = (unsigned)ABS(prefix_id_ss) ;
            (void) prefix_size ; // To prevent "unused variable" warning
            VERIFIC_ASSERT(prefix_size >= (offset+member_size)) ;

            unsigned b_sign = 0 ;
            // Get the string value of the prefix, we want to cut the suffix from it!
            char *str = prefix_value->GetBinaryValue(&b_sign) ;
            if (!str) {
                delete prefix_value ;
                return 0 ;
            }

            // This is the starting point of the suffix
            char *member_str = str+offset ;
            // And this is the ending point
            *(member_str+member_size) = '\0' ;
            unsigned sign = _suffix_id->IsSigned() ;
            char *prefix = 0 ;
            char *padded_str = 0 ;
            if (context_size) {
                padded_str = ConstStrEvaluation::GetPaddedString(member_str, (unsigned)ABS(context_size), sign) ;
                prefix = Strings::itoa(ABS(context_size)) ;
            } else {
                prefix = Strings::itoa((int)member_size) ;
            }
            // Create the string representation of the value
            char *val_str = Strings::save(prefix, (sign ? "'sb" : "'b"), (padded_str) ? padded_str : member_str) ;
            Strings::free(prefix) ;
            Strings::free(str) ;
            Strings::free(padded_str) ;

            // And return the value
            ret_val = new VeriBasedNumber(val_str) ;
            Strings::free(val_str) ;
        }
        delete prefix_value ;

        return ret_val ;
    }

    if (expect_nonconst && _suffix_id->IsParam()) {
        // VIPER #8418: If we are evaluating expression which can be non-constant
        // and suffix is enum literal, it is hierarchical reference of enum literal.
        // Return 0 for such case
        VeriDataType *dtype = _suffix_id->GetDataType() ;
        if (dtype && dtype->GetEnums()) { // This is enum type variable/enum literal
            Map *ids = dtype->GetEnumLiterals() ;
            VeriIdDef *enum_id = (ids) ? (VeriIdDef*)ids->GetValue(_suffix_id->Name()): 0 ;
            if (enum_id == _suffix_id) return 0 ; // Enum literal, return 0
        }
    }
    // Create an id-ref referring to this suffix_id, and evaluate that
    VeriIdRef suffix_id_ref(_suffix_id) ;
    suffix_id_ref.SetLinefile(Linefile()) ;
    return suffix_id_ref.StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst) ;
}

#if 0
VeriBaseValue *VeriExpression::EvaluateParamArrayExpr(int contextSize, ValueTable *n, VeriConstraint *target_constraint) const {
    return StaticEvaluateInternal(contextSize, n, target_constraint) ;
}
VeriBaseValue *VeriAssignmentPattern::EvaluateParamArrayExpr(int contextSize, ValueTable *n, VeriConstraint *target_constraint) const
{
    return StaticEvaluateInternal(contextSize, n, target_constraint) ;
}
VeriBaseValue *VeriMultiAssignmentPattern::EvaluateParamArrayExpr(int contextSize, ValueTable *n, VeriConstraint *target_constraint) const
{
    return StaticEvaluateInternal(contextSize, n, target_constraint) ;
}
// Create array value after evaluating each element of concat. Array property remains
VeriBaseValue* VeriConcat::EvaluateParamArrayExpr(int /*contextSize*/, ValueTable *n, VeriConstraint *target_constraint) const
{
    return StaticEvaluateInternal(0, n, target_constraint) ;
    if (!_exprs) return 0 ;
    Array *values = new Array(_exprs->Size()) ;
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        VeriBaseValue *evaluated_val = expr->EvaluateParamArrayExpr(0, n, 0) ;
        if (!evaluated_val) { // If any of the concat elements couldn't be evaluated return 0
            delete values ; // Free the dynamically allocated storage
            return 0 ;
        }
        values->InsertLast(evaluated_val) ;
    }
    // Pass a flag to VeriArrayValue class depeding on whether the expression is
    // concat or assignment pattern
    unsigned is_concat_arr_val = 1 ; //!IsAssignPattern() ;
    return new VeriArrayValue(values, is_concat_arr_val) ;
}
VeriBaseValue* VeriMultiConcat::EvaluateParamArrayExpr(int /*contextSize*/, ValueTable *n, VeriConstraint *target_constraint) const
{
    return StaticEvaluateUnpackedArrayConcat(target_constraint, n) ;
    if (!_repeat || !_exprs) return 0 ;

    VeriBaseValue *rep = _repeat->StaticEvaluateInternal(0, n, 0) ;
    if (!rep) return 0 ;
    int loop_count = rep->GetIntegerValue() ;
    delete rep ;
    if (loop_count <= 0) return 0 ;

    int i ;
    unsigned j ;
    VeriExpression *expr ;
    VeriBaseValue *evaluated_val = 0 ;
    Array *values = new Array((unsigned)loop_count * _exprs->Size()) ;
    for(i = 0; i < loop_count; i++) {
        FOREACH_ARRAY_ITEM(_exprs, j, expr) {
            if (!expr) continue ;
            evaluated_val = expr->EvaluateParamArrayExpr(0, n, 0) ;
            if (!evaluated_val) { // If any of the concat elements couldn't be evaluated return 0
                delete values ; // Free the dynamically allocated storage
                return 0 ;
            }
            values->InsertLast(evaluated_val) ;
        }
    }
    // Pass a flag to VeriArrayValue class depeding on whether the expression is
    // multi concat or multi assignment pattern
    unsigned is_mult_concat_arr_val = 1 ; //!IsMultiAssignPattern() ;
    return new VeriArrayValue(values, is_mult_concat_arr_val) ;
}
#endif

// Another version to evaluate a constant expression and constant parts of data type.
// This is required as type parameter value can be a data type. We cannot evaluate
// those, but we should evaluate constant parts of every data type
VeriExpression *VeriExpression::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    VeriBaseValue *val = 0 ;
    // VIPER #6041 : Do not try to evaluate this expression when formal is type
    // parameter. This expression can be an enum object reference for a
    // type parameter, but that reference can also be evaluated.
    if (!formal || !formal->IsType()) {
        val = StaticEvaluateInternal(contextSize, n, target_constraint, expect_nonconst) ;
        // VIPER #7892 : Apply context of formal on 'val'
        if (formal && !formal->IsType()) {
            // VIPER #8017 : Use overridden value to calculate formal size and sign
            // Otherwise original initial-value will be considered.
            //verific_int64 formal_size_sign = formal->TargetSizeSign(n, this) ;
            //val = ApplyContextOnValue(val, formal, formal_size_sign, n) ;
            // VIPER #8202/8203 : First apply the context size-sign to the value and then
            // apply formal size-sign
            val = ApplyContextOnValue(val, formal, (verific_int64)contextSize, n) ;
            verific_int64 formal_size_sign = formal->TargetSizeSign(n, this) ;
            val = ApplyContextOnValue(val, formal, formal_size_sign, n) ;
        } else if ((IsAssignPattern() || IsMultiAssignPattern()) && val && !val->IsConcatValue() && target_constraint) {
            // VIPER #8070 : Apply context from constraint to the evaluated values
            // of assignment pattern
            VeriBaseValue *new_result = val->AdjustAndCheck(target_constraint, 0, this) ;
            if (new_result) val = new_result ;
        }
    }
    VeriExpression *const_expr = val ? val->ToExpression(this) : 0 ;
    delete val ;
    if (!const_expr) {
        // If type identifier is used as parameter value like
        //   mod_name #(my_type) I();
        // then VeriIdRef is created instead of VeriTypeRef. So when we cannot evaluate
        // the expression we should check whether it is type reference. If yes, create
        // a typeref with the full_id.
        VeriIdDef *full_id = FullId() ; // Returns only for VeriIdRef
        if (full_id && !full_id->IsParam() && (full_id->IsType() || full_id->IsClass())) { // It is type reference
            // VIPER #6041 : Create the type reference using the full expression
            // not only id, as it may be a hierarchical/scope name
            VeriName *type_name = NameCast() ;
            VeriMapForCopy old2new ;
            const_expr = type_name ? new VeriTypeRef(type_name->CopyName(old2new), 0, 0) : new VeriTypeRef(full_id, 0, 0) ;
            const_expr->SetLinefile(Linefile()) ;
        }
        if (!const_expr && full_id && (full_id->IsParam() && full_id->IsType())) { // It is type parameter
            // VIPER #6565 : Mark and check type parameter for processing
            if (full_id->IsProcessing()) {
                if (!expect_nonconst) Error("value of %s depends on itself", full_id->Name()) ;
            } else {
                full_id->SetIsProcessing() ;
                VeriExpression *initi_val = full_id->GetInitialValue() ;
                // VIPER #7601: If initial value of type parameter is struct/union/enum
                // type, create type reference and let that be the expression
                VeriDataType *init_data_type = initi_val ? initi_val->DataTypeCast(): 0 ;
                if (init_data_type && init_data_type->GetIds() && (init_data_type->IsUnionType() || init_data_type->IsStructType() || init_data_type->IsEnumType())) {
                    VeriName *type_name = NameCast() ;
                    VeriMapForCopy old2new ;
                    const_expr = type_name ? new VeriTypeRef(type_name->CopyName(old2new), 0, 0) : new VeriTypeRef(full_id, 0, 0) ;
                    const_expr->SetLinefile(Linefile()) ;
                } else {
                    const_expr = initi_val ? initi_val->StaticEvaluateToExprInternal(contextSize, n, 0, full_id, expect_nonconst) : 0 ;
                }
                full_id->ClearIsProcessing() ;
            }
        }
        // VIPER 5513. For $ initial value return a VeriDollar expression
        if (!const_expr && full_id && full_id->IsParam()) { // It is type parameter
            VeriExpression *initi_val = full_id->GetInitialValue() ;
            if (initi_val && initi_val->IsDollar()) {
                VeriMapForCopy old2new ;
                const_expr = initi_val->CopyExpression(old2new) ;
            }
        }
    }
    return const_expr ;
}
// VIPER #6617 : Handle port connect
VeriExpression *VeriPortConnect::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    return _connection ? _connection->StaticEvaluateToExprInternal(contextSize, n, target_constraint, formal, expect_nonconst): 0 ;
}
// VIPER #2709: Implement this routine on VeriIndexedId and VeriIndexedMemoryId so that we can catch this:
// typedef bit node ;
// bot #(.DTYPE(node[3:0])) b1 (...) ;            // VeriIndexedId
// bot #(.DTYPE(node[3:0]i[1:0])) b1 (...) ;      // VeriIndexedMemoryId
// Here the dimensions, semantically, are not indexing. It is more like a type declaration
// of type 'node' with a dimensions. So, we must treat it differently.
// No need to implement it for VeriIdRef, becasue, it is perfectly handled by the virtual catcher.
VeriExpression *VeriIndexedId::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    VeriBaseValue *val = 0 ;
    // VIPER #6041 : Do not try to evaluate this expression when formal is type
    // parameter. This expression can be an enum object reference for a
    // type parameter, but that reference can also be evaluated.
    if (!formal || !formal->IsType()) {
        val = StaticEvaluateInternal(contextSize, n, target_constraint, expect_nonconst) ;
        // VIPER #7892 : Apply context of formal on 'val'
        if (formal && !formal->IsType()) {
            // VIPER #8017 : Use overridden value to calculate formal size and sign
            // Otherwise original initial-value will be considered.
            // VIPER #8202/8203 : First apply the context size-sign to the value and then
            // apply formal size-sign
            val = ApplyContextOnValue(val, formal, (verific_int64)contextSize, n) ;
            verific_int64 formal_size_sign = formal->TargetSizeSign(n, this) ;
            val = ApplyContextOnValue(val, formal, formal_size_sign, n) ;
        }
    }
    VeriExpression *const_expr = val ? val->ToExpression(this) : 0 ;
    delete val ;
    if (!const_expr) {
        if (_idx && _idx->IsRange() && !_idx->GetPartSelectToken()) {
            // If type identifier is used as parameter value like:
            //   mod_name #(my_type[3:0]) I();
            // then VeriIndexedId is created instead of VeriTypeRef. So when we cannot evaluate
            // the expression we should check whether it is type reference. If yes, create
            // a typeref with the id.

            // Here, we need to create a new dimension same as the range of 'this':
            VeriExpression *index = _idx->GetLeft() ; // Get left of range
            VeriExpression *left = (index) ? index->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst) : 0 ; // Evaluate it
            index = _idx->GetRight() ; // Get right index
            VeriExpression *right = (index) ? index->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst) : 0 ; // Evaluate it
            // Create the (constant) dimension, we will use it to create the type:
            VeriRange *dimensions = 0 ;
            if (!left || !right) {
                if (!expect_nonconst) _idx->Error("range index value is not constant") ;
                delete left ;
                delete right ;
            } else {
                dimensions = new VeriRange(left, right, 0 /* no part select token */, 0 /* no next range */) ;
                dimensions->SetLinefile(_idx->Linefile()) ;
            }

            if (_id && !_id->IsParam() && (_id->IsType() || _id->IsClass())) { // It is type reference
                // Create the type-ref with this dimension:
                // VIPER #6041 : Create the type reference using the full expression
                // not only id, as it may be a hierarchical/scope name
                VeriName *type_name = _prefix ? _prefix->NameCast(): 0 ;
                VeriMapForCopy old2new ;
                const_expr = type_name ? new VeriTypeRef(type_name->CopyName(old2new), 0, dimensions): new VeriTypeRef(_id, 0, dimensions) ;
                const_expr->SetLinefile(Linefile()) ;
                dimensions = 0 ; // Reset it, we have used it up
            }
            if (!const_expr && _id && (_id->IsParam() && _id->IsType())) { // It is type parameter
                // We need to get the initial value of this type parameter:
                VeriExpression *init_val = _id->GetInitialValue() ;
                const_expr = (init_val) ? init_val->StaticEvaluateToExprInternal(contextSize, n, 0, _id, expect_nonconst) : 0 ;
                // We have to insert the range into the initial value, so cast it to data type:
                // It must be a data type, otherwise something went wrong already:
                VeriDataType *init_type = (const_expr) ? const_expr->DataTypeCast() : 0 ;
                // And insert the range into the data type:
                if (init_type && dimensions) {
                    // Take dimensions from the initial value data type:
                    VeriRange *existing_dims = init_type->TakeDimensions() ;
                    // VIPER #7919: This existing dimensions are from data types.
                    // So, they are packed dimensions. New dimension becomes first
                    // packed dimension of the data type now:
                    dimensions->SetNext(existing_dims) ;
                    init_type->SetDimensions(dimensions) ;
                    dimensions = 0 ; // Reset it, we have used it up
                }
                // Set this to be the new actual value of the type parameter:
                const_expr = init_type ;
            }
            // Delete it to avoid memory leak (if it is still there):
            delete dimensions ;
        } else {
            if (_id && ((_id->IsParam() && _id->IsType()) || _id->IsType() || _id->IsClass())) {
                // Issue error if _id is type id/type parameter/class
                if (!expect_nonconst) Error("only [expr1:expr2] syntax is allowed for packed ranges of types") ;
            }
        }
    }
    return const_expr ;
}
VeriExpression *VeriIndexedMemoryId::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    VeriBaseValue *val = 0 ;
    // VIPER #6041 : Do not try to evaluate this expression when formal is type
    // parameter. This expression can be an enum object reference for a
    // type parameter, but that reference can also be evaluated.
    if (!formal || !formal->IsType()) {
        val = StaticEvaluateInternal(contextSize, n, target_constraint, expect_nonconst) ;
        // VIPER #7892 : Apply context of formal on 'val'
        if (formal && !formal->IsType()) {
            // VIPER #8017 : Use overridden value to calculate formal size and sign
            // Otherwise original initial-value will be considered.
            // VIPER #8202/8203 : First apply the context size-sign to the value and then
            // apply formal size-sign
            val = ApplyContextOnValue(val, formal, (verific_int64)contextSize, n) ;
            verific_int64 formal_size_sign = formal->TargetSizeSign(n, this) ;
            val = ApplyContextOnValue(val, formal, formal_size_sign, n) ;
        }
    }
    VeriExpression *const_expr = val ? val->ToExpression(this) : 0 ;
    delete val ;
    if (!const_expr && _id && ((_id->IsParam() && _id->IsType()) || _id->IsType() || _id->IsClass())) {
        // If type identifier is used as parameter value like:
        //   mod_name #(my_type[1:0][3:0]) I();
        // then VeriIndexedMemoryId is created instead of VeriTypeRef. So when we cannot evaluate
        // the expression we should check whether it is type reference. If yes, create
        // a typeref with the id.

        unsigned i ;
        VeriExpression *idx ;
        // Create the (constant) dimension, we will use it to create the type:
        VeriRange *dimensions = 0 ;
        FOREACH_ARRAY_ITEM(_indexes, i, idx) {
            if (!idx) continue ;
            if (!idx->IsRange() || idx->GetPartSelectToken()) {
                if (!expect_nonconst) Error("only [expr1:expr2] syntax is allowed for packed ranges of types") ;
                continue ;
            }
            VeriExpression *index = idx->GetLeft() ; // Get left of range
            VeriExpression *left = (index) ? index->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst) : 0 ; // Evaluate it
            index = idx->GetRight() ; // Get right index
            VeriExpression *right = (index) ? index->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst) : 0 ; // Evaluate it
            if (!left || !right) {
                if (!expect_nonconst) idx->Error("range index value is not constant") ;
                delete left ;
                delete right ;
            } else {
                VeriRange *dim = new VeriRange(left, right, 0 /* no part select token */, 0 /* no next range */) ;
                dim->SetLinefile(idx->Linefile()) ;
                if (!dimensions) {
                    dimensions = dim ;
                } else {
                    dimensions->SetNext(dim) ;
                }
            }
        }

        if (!_id->IsParam() && (_id->IsType() || _id->IsClass())) { // It is type reference
            // Create the type-ref with this dimension:
            // VIPER #6041 : Create the type reference using the full expression
            // not only id, as it may be a hierarchical/scope name
            VeriName *type_name = _prefix ? _prefix->NameCast(): 0 ;
            VeriMapForCopy old2new ;
            const_expr = type_name ? new VeriTypeRef(type_name->CopyName(old2new), 0, dimensions): new VeriTypeRef(_id, 0, dimensions) ;
            const_expr->SetLinefile(Linefile()) ;
            dimensions = 0 ; // Reset it, we have used it up
        }
        if (!const_expr && (_id->IsParam() && _id->IsType())) { // It is type parameter
            // We need to get the initial value of this type parameter:
            VeriExpression *init_val = _id->GetInitialValue() ;
            const_expr = (init_val) ? init_val->StaticEvaluateToExprInternal(contextSize, n, 0, _id, expect_nonconst) : 0 ;
            // We have to insert the range into the initial value, so cast it to data type:
            // It must be a data type, otherwise something went wrong already:
            VeriDataType *init_type = (const_expr) ? const_expr->DataTypeCast() : 0 ;
            // And insert the range into the data type:
            if (init_type) {
                // First check whether it has any dimension:
                VeriRange *existing_dims = init_type->GetDimensions() ;
                if (existing_dims) {
                    // Then insert it into the end of the existing dimensions:
                    existing_dims->SetNext(dimensions) ;
                } else {
                    // Otherwise, set it to be the new dimension:
                    init_type->SetDimensions(dimensions) ;
                }
                dimensions = 0 ; // Reset it, we have used it up
            }
            // Set this to be the new actual value of the type parameter:
            const_expr = init_type ;
        }
        // Delete it to avoid memory leak (if it is still there):
        delete dimensions ;
    }
    return const_expr ;
}
VeriExpression *VeriPathPulseVal::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    // Return the evaluated reject limit only in case there is no error limit specified
    if (!_error_limit) return  (_reject_limit) ? _reject_limit->StaticEvaluateToExprInternal(0,n,target_constraint, formal, expect_nonconst) : 0 ;

    // Return a VeriPathPulseVal in case both error_limit and reject_limits are specified
    VeriExpression *new_reject_limit = (_reject_limit) ? _reject_limit->StaticEvaluateToExprInternal(0,n,target_constraint, formal, expect_nonconst) : 0 ;
    if (_reject_limit && !new_reject_limit) return 0 ;

    VeriExpression *new_error_limit = _error_limit->StaticEvaluateToExprInternal(0,n,target_constraint, formal, expect_nonconst) ;
    if (!new_error_limit) { delete new_reject_limit ; return 0 ; }

    return new VeriPathPulseVal(new_reject_limit, new_error_limit) ;
}
VeriExpression *VeriDataType::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable *n, VeriConstraint * /*target_constraint*/, const VeriIdDef * /*formal*/, unsigned expect_nonconst) const
{
    // In data type only msb/lsb of range can be constant, so try to evaluate those.
    // If possible, replace the expression by its constant node form
    VeriMapForCopy old2new ;
    VeriDataType *ret_expr = CopyDataType(old2new) ;
    VeriRange *dimensions = ret_expr ? ret_expr->GetDimensions() : 0 ;
    VeriExpression *index ;
    VeriExpression *new_index ;
    while (dimensions) {
        index = dimensions->GetLeft() ; // Get left of range
        new_index = index ? index->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst) : 0 ; // Evaluate it
        if (new_index) (void) dimensions->ReplaceChildExprInternal(index, new_index, 1) ; // Replace by constant expression
        index = dimensions->GetRight() ; // Get right index
        new_index = index ? index->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst) : 0 ; // Evaluate it
        if (new_index) (void) dimensions->ReplaceChildExprInternal(index, new_index, 1) ; // Replace by constant expression
        dimensions = dimensions->GetNext() ;
    }
    return ret_expr ;
}
VeriExpression *VeriNetDataType::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef * /*formal*/, unsigned /*expect_nonconst*/) const
{
    VeriMapForCopy old2new ;
    VeriDataType *ret_type = CopyDataType(old2new) ;
    if (ret_type) (void) ret_type->StaticProcessInitialValue(0, 0, 0, 0) ;
    return ret_type ;
}
VeriExpression *VeriIndexedExpr::StaticEvaluateToExprInternal(int context_size, ValueTable *value_table, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    if (!_prefix) return 0 ;
    // VIPER #6426 : First try to evaluate the value of this expression
    VeriBaseValue *val = StaticEvaluateInternal(context_size, value_table, target_constraint, expect_nonconst) ;
    // VIPER #7892 : Apply context of formal on 'val'
    if (val && formal && !formal->IsType()) {
        // VIPER #8017 : Use overridden value to calculate formal size and sign
        // Otherwise original initial-value will be considered.
        // VIPER #8202/8203 : First apply the context size-sign to the value and then
        // apply formal size-sign
        val = ApplyContextOnValue(val, formal, (verific_int64)context_size, value_table) ;
        verific_int64 formal_size_sign = formal->TargetSizeSign(value_table, this) ;
        val = ApplyContextOnValue(val, formal, formal_size_sign, value_table) ;
    }
    if (val) return val->ToExpression(this) ;

    VeriExpression *prfx = _prefix->StaticEvaluateToExprInternal(context_size, value_table, target_constraint, formal, expect_nonconst) ;
    if (!prfx) return 0 ;

    return (new VeriIndexedExpr(prfx, ((_idx)?_idx->StaticEvaluateToExprInternal(context_size, value_table, target_constraint, formal, expect_nonconst):0))) ;
}
VeriExpression *VeriEnum::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef * /*formal*/, unsigned /*expect_nonconst*/) const
{
    // No part constant evaluation is done now, only copy the datatype and return
    // Here we are assuming that practical design will not be as follows
    //   module_name #(enum { AND= p, OR, MOD}) I(....)
    VeriMapForCopy old2new ;
    VeriDataType *ret_type = CopyDataType(old2new) ;
    if (ret_type) (void) ret_type->StaticProcessInitialValue(0, 0, 0, 0) ;
    return ret_type ;
}

VeriExpression *VeriStructUnion::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef * /*formal*/, unsigned /*expect_nonconst*/) const
{
    // No part constant evaluation is done now, only copy the data and return
    // Here we are assuming that practical design will not be as follows
    //   module_name #(struct { int a; real b;}) I(....)
    VeriMapForCopy old2new ;
    VeriDataType *ret_expr = CopyDataType(old2new) ;
    // Now try to evaluate the ranges and default value of structure/union defined
    // variables
    if (ret_expr) (void) ret_expr->StaticProcessInitialValue(0, 0, 0, 0) ;
    return ret_expr ;
}
VeriExpression *VeriTypeOperator::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable * /*n*/, VeriConstraint * /*target_constraint*/, const VeriIdDef * /*formal*/, unsigned /*expect_nonconst*/) const
{
    VeriMapForCopy old2new ;
    VeriDataType *ret_expr = CopyDataType(old2new) ;
    // Now try to evaluate the ranges and default value of structure/union defined
    // variables
    if (ret_expr) {
        VeriExpression *arg = ret_expr->GetArg() ;
        if (arg) (void) arg->StaticProcessInitialValue(0, 0, 0, 0) ;
    }
    return ret_expr ;
}
VeriExpression *VeriCast::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint * target_constraint, const VeriIdDef * /*formal*/, unsigned expect_nonconst) const
{
    // Get the target type :
    VeriTypeInfo *target = (_target_type) ? _target_type->CheckType(0, NO_ENV): 0 ;
    if (target && !target->IsTypeParameterType() && !target->IsUnresolvedNameType() && target->IsUnpacked() && !target->IsRealType() && !target->IsString()) {
        // Target is unpacked, cannot represent it as a value :
        VeriConstraint *t_constraint = (_target_type) ? _target_type->EvaluateConstraintInternal(n, 0): 0 ;
        VeriExpression *new_expr = (_expr) ? _expr->StaticEvaluateToExprInternal(0, 0, t_constraint, 0, expect_nonconst) : 0 ; // FIXME : need constraint of target
        if (_expr && !new_expr) return 0 ;
        delete t_constraint ;
        VeriMapForCopy old2new ;
        VeriCast *copied_cast = new VeriCast(*this, old2new) ;
        if (new_expr) {
            (void) copied_cast->ReplaceChildExprInternal(copied_cast->GetExpr(), new_expr, 1) ;
        }
        delete target ;
        return copied_cast ;
    }
    delete target ;
    return VeriExpression::StaticEvaluateToExprInternal(contextSize, n, target_constraint, 0, expect_nonconst) ;
}
VeriBaseValue *VeriSeqPropertyFormal::StaticEvaluateInternal(ValueTable *n, unsigned expect_nonconst)
{
    VeriConstraint *formal_constraint = EvaluateConstraintInternal(n, 0, 0) ;
    int lsize = (int)StaticSizeSignInternal(n, 0) ;
    unsigned left_size = (unsigned)ABS(lsize) ;
    VeriBaseValue *val = 0 ;
    if (_actual) {
        // This is an actual/formal association. Section 11.13 says actual argument
        // shall be cast to the type of formal argument before being substituted
        // for a reference to the formal argument.
        int rsize = (int)_actual->StaticSizeSignInternal(n, formal_constraint) ;
        unsigned right_size = (unsigned)ABS(rsize) ;
        unsigned rsign = (rsize < 0) ? 1 : 0 ;

        unsigned size = (left_size > right_size) ? left_size : right_size ;
        int eval_context = rsign ? -((int)size) : (int)size ;
        val = _actual->StaticEvaluateInternal(eval_context, n, formal_constraint, expect_nonconst) ;
    }
    VeriExpression *init_val = GetInitialValue() ;
    if (!val && init_val) {
        int rsize = (int)init_val->StaticSizeSignInternal(n, formal_constraint) ;
        unsigned right_size = (unsigned)ABS(rsize) ;
        unsigned rsign = (rsize < 0) ? 1 : 0 ;
        unsigned size = (left_size > right_size) ? left_size : right_size ;
        int eval_context = rsign ? -((int)size) : (int)size ;
        val = init_val->StaticEvaluateInternal(eval_context, n, formal_constraint, expect_nonconst) ;
    }
    delete formal_constraint ;
    return val ;
}
VeriExpression *VeriAssignmentPattern::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint * target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    // Just call the base method to evaluate the concat expressions, if it does not have any type:
    if (!_type) return VeriConcat::StaticEvaluateToExprInternal(contextSize, n, target_constraint, formal, expect_nonconst) ;

    // VIPER #5248 (test_70): Here, it has a type, need some special processing:

    // Copy the assignment pattern first:
    VeriMapForCopy id_map_table ;
    VeriAssignmentPattern *copied = new VeriAssignmentPattern(*this, id_map_table) ;

    // Static evaluate the copied assignment pattern to expression, generates another assignment pattern:
    // Cannot call copied->StaticEvaluateToExprInternal(...) directly, that will create inifinite loop!
    VeriBaseValue *eval_val = copied->StaticEvaluateInternal(contextSize, n, target_constraint, expect_nonconst) ;
    if (!eval_val) {
        delete copied ;
        return 0 ;
    }
    VeriConstraint *assignment_pattern_constraint = _type->EvaluateConstraintInternal(n, 0) ;
    if (!eval_val->IsConcatValue() && assignment_pattern_constraint) {
        // VIPER #8070 : Apply context from constraint to the evaluated values
        VeriBaseValue *new_result = eval_val->AdjustAndCheck(assignment_pattern_constraint, 0, this) ;
        if (new_result) eval_val = new_result ;
    }
    delete assignment_pattern_constraint ;
    VeriExpression *eval = eval_val->ToExpression(copied) ;
    delete eval_val ; // Not needed anymore

    // Get the evaluated and copied assignment pattern items:
    Array *eval_exprs = (eval) ? eval->GetExpressions() : 0 ; // Evaluated
    Array *copied_exprs = copied->GetExpressions() ; // Copied

    if (!(eval_exprs && copied_exprs && (eval_exprs->Size() == copied_exprs->Size()))) {
        // Something bad happened: the evaluation went wrong!
        delete copied ;
        return eval ; // Anyway, better than returning NULL!
    }

    // Now, replace the copied assignment pattern items with the evaluated ones:
    unsigned i ;
    VeriExpression *eval_expr ;
    VeriExpression *copied_expr ;
    FOREACH_ARRAY_ITEM(eval_exprs, i, eval_expr) {
        copied_expr = (VeriExpression *)copied_exprs->At(i) ;
        (void) copied->ReplaceChildExprInternal(copied_expr, eval_expr, 1) ;
    }

    // Finally, delete the evaluated expression, but before that reset the array since we have used them:
    eval_exprs->Reset() ;
    delete eval ;

    // All of the above was done to have the proper 'typename' set in the evaluated expression.
    return copied ;
}
VeriExpression *VeriSystemFunctionCall::StaticEvaluateToExprInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, const VeriIdDef *formal, unsigned expect_nonconst) const
{
    VeriExpression *arg = (_args && _args->Size()) ? (VeriExpression*)_args->At(0): 0 ;
    if (arg && _function_type == VERI_SYS_CALL_TYPEOF) {
        // Convert this typeof system function to type operator and return
        VeriMapForCopy old2new ;
        VeriExpression *new_arg = arg->CopyExpression(old2new) ;
        VeriExpression *ret_expr = new VeriTypeOperator(new_arg) ;
        ret_expr->SetLinefile(Linefile()) ;
        (void) ret_expr->StaticProcessInitialValue(0, 0, 0, 0) ;
        return ret_expr ;
    }
    return VeriExpression::StaticEvaluateToExprInternal(contextSize, n, target_constraint, formal, expect_nonconst) ;
}

VeriExpression *VeriTypeRef::StaticEvaluateToExprInternal(int /*contextSize*/, ValueTable *n, VeriConstraint * /*target_constraint*/, const VeriIdDef * formal, unsigned expect_nonconst) const
{
    // In data type only msb/lsb of range can be constant, so try to evaluate those.
    // If possible, replace the expression by its constant node form
    VeriMapForCopy old2new ;
    VeriExpression *ret_expr = 0 ; //CopyDataType(old2new) ;

    (void) expect_nonconst ; //JJ: needed for unused warning

    if (_id && _id->IsParam() && _id->IsType() && _type_name && !_type_name->GetParamValues()) {
        // Type reference is a type parameter reference, evaluate its value
        VeriExpression *new_expr = _type_name->StaticEvaluateToExprInternal(0, n, 0, formal, expect_nonconst) ;
        ret_expr = new_expr ;
        if (!ret_expr) return 0 ;
    }
    if (!ret_expr) {
        VeriDataType *copied_data_type = CopyDataType(old2new) ;
        ret_expr = copied_data_type ;
        VeriName *copied_type_name = (copied_data_type) ? copied_data_type->GetTypeName(): 0 ;
        Array *param_values = (copied_type_name) ? copied_type_name->GetParamValues(): 0 ;

        // Iterate param values and evaluate actuals
        VeriExpression *expr, *actual, *new_actual ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(param_values, i, expr) {
            if (!expr) continue ;
            actual = expr->GetConnection() ;
            new_actual = (actual) ? actual->StaticEvaluateToExprInternal(0, n, 0, 0, expect_nonconst): 0 ;
            //if (!new_actual) continue ;
            if (expr->NamedFormal()) {
                if (!expr->ReplaceChildExprInternal(actual, new_actual, 1)) delete new_actual ;
            } else {
                if (copied_data_type) {
                    if (!copied_data_type->ReplaceChildExprInternal(actual, new_actual, 1)) delete new_actual ;
                } else {
                    delete new_actual ;
                }
            }
        }
    }
    if (ret_expr) (void) ret_expr->StaticProcessInitialValue(0, 0, 0, 0) ;
    return ret_expr ;
}

VeriBaseValue *
VeriExpression::StaticEvaluate(int context_size, ValueTable *n, VeriConstraint *target_constraint /*= 0*/, unsigned expect_nonconst /*=0*/) const
{
    // Check and set elaboration style to static (VIPER #7120):
    unsigned static_elab = VeriNode::IsStaticElab() ;
    if (!static_elab) VeriNode::SetStaticElab() ;

    // Call internal routine:
    VeriBaseValue *result = StaticEvaluateInternal(context_size, n, target_constraint, expect_nonconst) ;

    // Restore elaboration style:
    if (!static_elab) VeriNode::ResetStaticElab() ;

    return result ;
}

VeriExpression *
VeriExpression::StaticEvaluateToExpr(int context_size, ValueTable *n, unsigned expect_nonconst /*=0*/) const
{
    // Check and set elaboration style to static (VIPER #7120):
    unsigned static_elab = VeriNode::IsStaticElab() ;
    if (!static_elab) VeriNode::SetStaticElab() ;

    // Evaluate constant expression and part of data type, returns expression
    VeriExpression *result = StaticEvaluateToExprInternal(context_size, n, 0, 0, expect_nonconst) ;

    // Restore elaboration style:
    if (!static_elab) VeriNode::ResetStaticElab() ;

    return result ;
}

