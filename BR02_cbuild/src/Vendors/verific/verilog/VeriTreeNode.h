/*
 *
 * [ File Version : 1.305 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_TREE_NODE_H_
#define _VERIFIC_VERI_TREE_NODE_H_

#include <iosfwd> // iostream inclusion needed for PrettyPrint
using std::ostream ;

#include "VerificSystem.h"
#include "VeriCompileFlags.h" // Verilog-specific compile flags

#include "LineFile.h"         // For linefile_type
#include "Message.h"          // For msg_type
#include "VeriClassIds.h"     // Get class ids (VERI_CLASS_ID)

#include "VeriVisitor.h"      // Abstract base Visitor class

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class SaveRestore ;

class DesignObj ;
class Netlist ;
class Net ;
class Instance ;
class Library ;

class Map ;
class BitArray ;
class Array ;
class Set ;

class VeriIdDef ;
class VeriModule ;
class VeriScope ;
class veri_file ;
class ValueTable ;
class VeriConfigurationInfo ;
class VeriConstraint ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
class VhdlPrimaryUnit ;
#endif

class VeriMapForCopy ;
class VeriLibrary ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Conversion of verilog package to vhdl package
class VhdlScope ;
#endif
// Following compile flags are introduced for efficient memory usage in the
// Verific parse tree
//
// This compile flag, when defined, free the _name string from the IdRef class
// after the class is resolved. Since the class already has a valid IdDef pointer
// _name field is redundant. IdRef::GetName already picks up the name from IdDef
// when _name is null
#define VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
//
// Following compile flag shares the char * strings members of of different class
// objects. The string members are made const char * so that no one can change the
// strin in-place as the same string is shared among the objects. This prevents
// re-allocation of the same string and saves memory. To share the string we added
// API Strings::CreateConstantString which caches the string.
//
// Following points are worth mentioning in this context:
// (1) With this compile flag turned on, _name field of IdRef and IdDef are shared.
//     Hence this flag is a superset of the VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
//     compile flag defined above.
// (2) Additionally this compile flag shares the strings in VeriPortConnec (for the
//     string port_ref_name) and VeriSelectedName (for suffix name) class.
// (3) Under this compile flag, the string passed to the ctor or SetName APIs as
//     argument is free'd, if the string exists in the cache. So care must be
//     taken to not access the same pointer after calling SetName or the ctor.
//     The string must be obtained again using GetName/Name APIs.
//
// #define VERILOG_SHARE_STRINGS

// Following compile flag gets rid of classes that encapsulates <char *, VeriExpression*>
// e.g. VeriPortConnect class by deriving another class from the base expression class
// to hold the extra char *. For big structural designs (with lots of port connects) this
// saves significant memory (4 bytes for each port connect object).
// #define VERILOG_USE_NAME_EXTENDED_EXPRESSION

// Following compile flag adds virtual catcher for all 'ReplaceChildXXX' APIs in
// VeriTreeNode, so that only 'VeriTreeNode*' can be used as parent/container pointer
// for all types of parse tree nodes (VeriModuleItem, VeriStatement, VeriExpression or
// any class derived from VeriTreeNode)
//
// #define VERILOG_USE_TREENODE_REPLACECHILD
//

/* -------------------------------------------------------------- */

/*
   VeriNode is basic to ALL nodes (parse-tree or value).
   It contains only static variables and routines for fast access
   throughout the Verilog reader code.
*/
class VFC_DLL_PORT VeriNode
{
public:
    // Analysis mode backward compatibility routines (analysis_mode is now in veri_file)
    static unsigned         IsVeri95() ;
    static unsigned         IsVeri2001() ;
    static unsigned         IsSystemVeri2005() ;
    static unsigned         IsSystemVeri2009() ;
    static unsigned         IsSystemVeri2012() ;
    static unsigned         IsSystemVeri2009OrLater() ;
    static unsigned         IsSystemVeri() ;
    static unsigned         IsPSL() ;
    static unsigned         IsAms() ;
    static void             SetVeri95() ;
    static void             SetVeri2001() ;
    static void             SetSystemVeri2005() ;
    static void             SetSystemVeri2009() ;
    static void             SetSystemVeri() ;
    static void             SetPSL() ;
    static void             SetAms() ;

    // Test if we are presently elaborating in 'static' mode:
    static unsigned         IsStaticElab()          { return _is_static_elab ; }
    static void             SetStaticElab() ;
    static void             ResetStaticElab()       { _is_static_elab = 0 ; }

    // Support for pretty-printing
    static const char *     PrintToken(unsigned veri_token) ; // Get a char* for a Verilog token, as it appears in the language (opposite of flex)

    static unsigned         IsEscapedIdentifier(const char *str) ;
    static unsigned         IsVerilogKeyword(const char *str, unsigned in_verilog_mode) ; // Returns 1 if the given string is a keyword in the given mode (mode is veri_file::VERILOG_95, VERILOG_2K, SYSTEM_VERILOG etc)
    static void             ResetVerilogKeywordMap() ;
    static char             GetUdpEdgeChar(const char *udp_char) ; // example "(0x)" returns 'C'. Return 0 if not a double-char edge token.
    static unsigned         IsUdpXEdge(char udp_char) ; // Check if this is a 'x' edge (to or from 'x').
    static unsigned         AreUdpCharsOverlapping(char c1, char c2) ; // Viper #3812/3444: Check if these two UDP input characters are overlapping
    static char             GetCleanUdpEdge(char udp_char) ; // Extract clean (r or f) edge, if the edge contains a 0->1 or 1->0 edge. So "b1" becomes 'r', "0?" also becomes 'r'. All unclean edges (not containing a clean r or f), multi-edges ("bb" and "??" or '*') and all values return 0.
    static void             ResetUdpEdgeCharMap() ;

    static unsigned         IsSystemTask(const char *name) ; // VIPER #2641: Returns 1 if the name refers to a system task
    static void             ResetSystemTasksTable() ;

    // VIPER #6594 (issue 131): Retired with addition of veri_file::AnalyzeExpr() API.
    // Equivalent call is: veri_file::AnalyzeExpr(value, veri_file::GetAnalysisMode()) ;
    //static VeriExpression * StringToExpression(const char *value) ;

    // Comment preservation (only active if compile flag is set)
    // Node specific routines for comment handling on 'this' node
    void                    AddComments(Array *arr, unsigned pre = 0) const ; // Add comments (VeriComment)s to 'this' node. Note: 'arr' is absorbed in this routine and even it may get deleted too
    Array *                 GetComments() const ; // Get the comments (VeriComment) from 'this' node
    Array *                 TakeComments() const ; // Remove 'this' node's comments from the static table, and return the pointer. Opposite of AddComments.
    static void             DeleteComments(Array *comments) ; // free comments in 'comments'

    // Routines for attribute handling
    // Node specific routines for attributes on 'this' node :
    void                    AddAttributes(Map *attr) const ; // Absorb the Map (char*->VeriConst) of attribute tree nodes to the (attributed) expression.
    void                    SetAttributes(Map *attr) const ; // Same as AddAttributes, but historically disallows existing attributes on 'this' node.
    Map                    *TakeAttributes() const ; // Remove attributes from 'this' node's and return the pointer. Opposite of AddAttributes/SetAttributes.
    Map *                   GetAttributes() const ; // Get the Map of attribute_name->VeriExpression for this node
    VeriExpression         *GetAttribute(const char *attr_name) const ; // Get attribute expression for 'attr_name' attribute on 'this' node
    void                    DeleteAttributes() const ; // Delete attributes for 'this' node.
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNode() ;
    virtual ~VeriNode() ;

private:
    // Prevent compiler from defining the following
    VeriNode(const VeriNode &) ;            // Purposely leave unimplemented
    VeriNode& operator=(const VeriNode &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    enum { VERI_EDGE_NOEDGE=0, VERI_EDGE_POSEDGE=1, VERI_EDGE_NEGEDGE=2, VERI_EDGE_EDGE=(VERI_EDGE_POSEDGE|VERI_EDGE_NEGEDGE) } ; // VIPER #6766

    // Support for pretty-printing
    static const char *     PrintLevel(unsigned level) ;
    static void             PrintIdentifier(ostream &f, const char *str) ;
    static void             PrettyPrintCommaList(const Array *list, ostream &f, unsigned level) ;
    static void             PrettyPrintExprList(const Array *list, ostream &f, unsigned level) ;

    // UDP table entry encoding. All double-char "(..)" edge tokens are internally encoded with a (capital) character. All single-char edges and values are encoded as they come in ('f', '*' etc)
    static const char *     PrintUdpEdgeChar(char udp_char) ; // example character 'C' returns edge "(0x)". return NULL if not a double-char edge value.
    // Support for hierarchical/escaped identifier name outputting
    static char *           MakeVerilogName(const char *str) ;

    // Pragma settings (used by analyzer while parsing), and moving pragmas to identifier attributes. Done during analysis.
    static Map *            PragmasToAttributes(Map *attributes, Map *pragmas) ; // Add 'pragmas' table, obtained with TakePragmas(), (char*->char*) and translate to (Verilog 2001) 'attributes' table (char*->VeriExpression*). Absorb both tables, return a new one.

    static unsigned         IsTickProtectedRtlRegion(unsigned update_status) ; // This is internal use only API called inside ModuleItem constructor to set the qualifier in module item. When update_status is true, this will reset the tick_protected_region flag, if the entire protected region is already parsed
    // VIPER 4800. 5411 Produce error if streaming concatenation is
    // larger than other side of assignment.
    static void             CheckStreamingConcat(const VeriExpression *lval, const VeriExpression* val) ;
    // VIPER #4977 : Routines for reference count handling
    static void             IncreaseReferenceCount(const VeriIdDef *id, unsigned incr_if_gt_1 = 0) ;
    static unsigned         GetReferenceCount(const VeriIdDef *id) ;
    static void             ResetReferenceCountMap() ;

    // Internal routine for Verific's use only. Make is static in VeriNode for VIPER #5107.
    // Returns the modified 'str'. 'from_lex' is used to modify linefile values in lex.
    // Note that returned pointer may not be the same as 'str', it may be str+1.
    static char *           MakeVerilogPrintable(char *str) ;

    // Internal routine for Verific's use only. Rounds (not truncates) the double to nearest integer:
    static double           RoundToNearestInteger(double val) ;

    // Message handling support : Message id table, output formatted, varargs message functions
    static const Map *      GetMessageMap()             { (void) GetMessageId("") ; return _msgs ; } // Return the entire map : (char*)formatted_string -> (char*)message_id
    static const char *     GetMessageId(const char *msg) ;     // Aid to get message ID from formatted string. Accessed by Error/Warning/Error
    static void             ResetMessageMap() ;

    // Relaxed Language Checking Mode :
    // This mode is to match simulators that allow contructs beyond the LRM.
    static void             SetRelaxedChecking(unsigned on) ; // flip 'relaxed' language checking mode on or off
    static const Map *      GetRelaxedMessageMap()      { return _relaxed_msgs ; }
    static unsigned         InRelaxedCheckingMode()     { return (_relaxed_msgs) ?  1 : 0 ; }
    static msg_type_t       GetRelaxedMessageType(const char *msg_id, msg_type_t orig_msg_type) ; // will change the msg_type of this message id, if it occurs in relaxed_msgs table.
    static void             ResetRelaxedMessageMap() ;

protected:
    static void             InitializeVerilogKeywords() ; // VIPER #2917
    static void             PopulateTransitionSetForChar(char c, Set &c_set) ; // For use in AreUdpCharsOverlapping() routine

public:
    // Static maps of attributes and comments linked to the parse trees.
    static Map *            GetMapOfCommentsArray() ; // static map of nodes->comments of all Verilog parse tree nodes. Table can be NULL.
    static void             FreeMapOfCommentsArray() ; // cleans up all the elements, frees the map, sets pointer to null

    static Map *            GetAttributeMap() ; // static map of of nodes->attributes for all Verilog parse tree nodes. Table can be NULL.
    static void             FreeAttributeMap() ; // cleans up all the elements, frees the map, sets pointer to null

    // VIPER #6640: Move all built-in type size/sign info into one place:
    static unsigned         GetSizeOfBuiltInType(unsigned type, unsigned *sign=0) ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
public:
    // Added the last argument for Viper# 3262
    static VhdlPrimaryUnit *GetVhdlUnitForVerilogInstance(const char *entity_name, const char *lib_name, unsigned search_from_loptions, char ** architecture = 0) ; // Pick up the vhdl entity instantiated inside a verilog module
    static unsigned         IsUniqueNocaseName(const Array *list1, const Array *list2, const char *name) ;
    static char *           CreateEscapedNameFromVerilog(const char *str, unsigned is_generic, const Array* ports, const Array* parameters) ;
#endif
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
public:
    // VIPER #6895: Convert vhdl package to verilog package:
    static VeriPackage*     ConvertVhdlToVerilogPackage(const char *pkg_name, const char *pkg_lib) ;
    // VIPER #6896: Conversion of verilog package to vhdl package
    static char*            GetTokenName(unsigned type, unsigned dim) ; // return vhdl name corresponding to the verilog token type
    static char*            CreateUniqueIdName(const VhdlScope *vhdl_scope, VeriScope *veri_scope, const char *name) ;
#endif

public:
    static VeriScope  *_present_scope ; // Should point to the scope we are currently in (maintained only in VarUsage and Elaborate flow)
    static VeriExpression  *_sva_clock_expr ; // Only used for System Verilog assertions. This is used to determine if the property is under clock. Viper 2627
    static VeriExpression  *_default_disable_iff_cond ; // Only used for System Verilog assertions. This is used to determine if the property is under reset condition
    static unsigned  _contains_disable_iff_cond ; // Only used for System Verilog assertions. This is used to determine if the property is under local disable iff: Viper 7078
    // These flags are used to implement presence of timing, event control, or usage of id in event expression
    // in the procedural block. LRM 2009 Section : 16.15.6
    static unsigned _contains_event_control ;
    static unsigned _contains_delay_control ;
    static unsigned _contains_even_id_in_body ;
    static unsigned _multiple_inferred_clock ;

    static unsigned  _in_property_expr ; // Only used for System Verilog assertions/assumptions. This is used to determine if we are inside property

private:
    static Map      *_msgs ;             // Static table to store formatted message->messageID
    static Map      *_relaxed_msgs ;     // Static table of msg_id->msg_type, with messages that need to be "relaxed" (downgraded from error->warning or so), to match simulators that allow contructs beyond the LRM.

    static Map      *_udp_edge_chars ;   // Static table to encode double-edge UDP tokens into a single char.
    static Map      *_verilog_keywords ; // Static table of keywords vs there dialect: VIPER #2917
    static Set      *_system_tasks ;     // VIPER #2641: Set of Verilog system tasks names without $ like display
    static Map      *_ref_count_map ;    // Static map for VeriIdDef * vs number of its references: VIPER #4977

    static Map      *_attribute_map ;    // Map of VeriExpression/VeriModuleItem * --> _attributes (Map *)

    static Map      *_map_of_comment_arr ; // Map of VeriTreeNode -> Array of VeriComment nodes

protected:
    static unsigned   _is_static_elab ;     // Flag static elaboration.

public:
    static unsigned   _under_default_clock ; //Set in Initialize of clocking block and used in evaluate of $past from initialize of interface. Viper 5948
    static unsigned  _has_delay_or_event_control ; // flag to store if the preceding stmt in a seq block is delay or event control Viper 3956, 4420

    // VIPER #6878: Flag to control pretty print of attributes and pragmas (default is print as what was specified in RTL):
    static unsigned _attr_print_mode ; // Cannot make it veri_file::attr_pretty_print_mode, that will need veri_file to be included before VeriTreeNode.h
    static unsigned  _potential_always_loop ; // VIPER #7506: flag to store if a sequential block contains a wait statement

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNode

/* -------------------------------------------------------------- */

/*
   VeriTreeNode is the base class for all Verilog parse-tree nodes.
   It contains linefile info, and the routines that
   operate on all parse-tree nodes.
*/
class VFC_DLL_PORT VeriTreeNode : public VeriNode
{
public:
    explicit VeriTreeNode(linefile_type linefile = 0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const                      { return ID_VERITREENODE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // LineFile info from this tree node
    inline linefile_type        Linefile() const                        { return _linefile ; } // Get linefile info of this node

    // Next two ones are here for back-ward compatibility.
    // Since 9/2004, we create start or ending linefile info based on the location info from bison rule-location-range processing.
    // This is much simpler than overloading all classes with a special routine.
    // Thus, every node now has fixed linefile info, which is either start or end linefile (determined by compile switch VERILOG_USE_STARTING_LINEFILE_INFO),
    // or it is full location info (start/end column/line all available), (determined by compile switch VERIFIC_LINEFILE_INCLUDES_COLUMNS)
    inline virtual linefile_type StartingLinefile() const               { return _linefile ; }  // Get starting linefile info (this will be overridden by various classes)
    inline linefile_type         EndingLinefile() const                 { return _linefile ; }  // Get ending linefile info (purposely not virtual)
    inline void                  SetLinefile(linefile_type linefile)    { _linefile = linefile ; }

    // Pretty Print (should be implemented by every node)
    // Cannot make it a pure function, since it's not abstract : we instantiate it in lex/yacc to get line-numbering right
    virtual void                PrettyPrint(ostream& /*f*/, unsigned /*level*/) const { }

    // Virtual catcher for operator nodes
    virtual unsigned            OperType() const { return 0 ; }

    virtual unsigned            GetQualifier(unsigned /*token*/) const { return 0 ; } // Viper #6948

    virtual unsigned            IsTickProtectedNode() const ; // Viper #8208

    // Output formatted, varargs message functions
    void                        Comment(const char *format, ...) const ;
    void                        Error(const char *format, ...) const ;
//CARBON_BEGIN
    void                        Alert(const char *format, ...) const ;
//CARBON_END
    void                        Warning(const char *format, ...) const ;
    void                        Info(const char *format, ...) const ;

    // VIPER #4335 : Pass value table to calculate size of any parse tree
    // Size and sign of parse tree nodes (mainly expressions and identifiers).
    // Negative return value indicates signed and absolute value is size.
    // Viper #7711: return in 64 bits
    // VIPER #8070: Added constraint/context info which is used for assignment pattern.
    verific_int64               StaticSizeSign(ValueTable *tab = 0, const VeriConstraint *target_context = 0) const ;

#ifdef VERILOG_USE_TREENODE_REPLACECHILD
    virtual VeriScope *         GetScope() const                        { return 0 ; } // The local scope (if any) of this module item
    // All Parse tree manipulation routines :
    // VIPER #6432 : Add default argument to these ReplaceChildXXX Apis, so that caller can determine
    // whether they want to delete old parse tree or not. By default, old parse tree is delete, but
    // if ReplaceChildXXX is called passing 0 as third argument, old parse tree will not be deleted.
    unsigned            ReplaceChildName(VeriName *old_parent, VeriName *new_parent, unsigned delete_old_node=1) { return ReplaceChildNameInternal(old_parent, new_parent, delete_old_node) ; } // Delete old name, and put new name
    unsigned            ReplaceChildId(VeriIdDef *old_id, VeriIdDef *new_id, unsigned delete_old_node=1) { return ReplaceChildIdInternal(old_id, new_id, delete_old_node) ; } // Delete old id, and put new id
    unsigned            ReplaceChildExpr(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node=1) { return ReplaceChildExprInternal(old_expr, new_expr, delete_old_node) ; } // Delete 'old_expr', and puts new expression in its place
    unsigned            ReplaceChildStmt(VeriStatement *old_statement, VeriStatement *new_statement, unsigned delete_old_node=1) { return ReplaceChildStmtInternal(old_statement, new_statement, delete_old_node) ; } // Delete old statement, and put new statement
    unsigned            ReplaceChildModuleItem(VeriModuleItem *old_decl, VeriModuleItem *new_decl, unsigned delete_old_node=1) { return ReplaceChildModuleItemInternal(old_decl, new_decl, delete_old_node) ; } // Delete old declaration item, and put new declaration item
    unsigned            ReplaceChildDataType(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node=1) { return ReplaceChildDataTypeInternal(old_data, new_data, delete_old_node) ; } // Delete 'old_data' specific data type, and puts new data type in its place
    unsigned            ReplaceChildStrength(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node=1) { return ReplaceChildStrengthInternal(old_stre, new_stre, delete_old_node) ; } // Delete 'old_stre', and puts new strength in its place.
    unsigned            ReplaceChildAnsiPortDecl(VeriAnsiPortDecl *old_port, VeriAnsiPortDecl *new_port, unsigned delete_old_node=1) { return ReplaceChildAnsiPortDeclInternal(old_port, new_port, delete_old_node) ; }  // Delete 'old_port', and puts new ansi port declaration in its place
    unsigned            ReplaceChildDefParamAssign(VeriDefParamAssign *old_assign, VeriDefParamAssign *new_assign, unsigned delete_old_node=1) { return ReplaceChildDefParamAssignInternal(old_assign, new_assign, delete_old_node) ; } // Delete 'old-assign' specific defparam assignment from assign list, and puts new assignment in its place
    unsigned            ReplaceChildNetRegAssign(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node=1) { return ReplaceChildNetRegAssignInternal(old_assign, new_assign, delete_old_node) ; } // Delete 'old_assign' specific net assignment, and puts new assignment in its place
    unsigned            ReplaceChildInstId(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node=1) { return ReplaceChildInstIdInternal(old_inst, new_inst, delete_old_node) ; } // Delete 'old_inst' from instance list, and puts new instance in its place
    unsigned            ReplaceChildPath(VeriPath *old_path, VeriPath *new_path, unsigned delete_old_node=1) { return ReplaceChildPathInternal(old_path, new_path, delete_old_node) ; } // Delete 'old_path', and puts new path in its place
    unsigned            ReplaceChildGenerateCaseItem(VeriGenerateCaseItem *old_item, VeriGenerateCaseItem *new_item, unsigned delete_old_node=1) { return ReplaceChildGenerateCaseItemInternal(old_item, new_item, delete_old_node) ; } // Delete 'old_item' from case-generate items, and puts new case-item in its place
    unsigned            ReplaceChildEventControl(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node=1) { return ReplaceChildEventControlInternal(old_control, new_control, delete_old_node) ; } // Delete 'old_control', and puts new control in its place
    unsigned            ReplaceChildCaseItem(VeriCaseItem *old_case, VeriCaseItem *new_case, unsigned delete_old_node=1) { return ReplaceChildCaseItemInternal(old_case, new_case, delete_old_node) ; } // Delete 'old_case' from case item list, and puts new case item in its place.
    unsigned            ReplaceChildRange(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node=1) { return ReplaceChildRangeInternal(old_range, new_range, delete_old_node) ; } // Delete 'old_range' if it is next range, and puts new range as new next range

    unsigned            ReplaceChildModportDecl(VeriModportDecl *old_modport_decl, VeriModportDecl *new_modport_decl, unsigned delete_old_node=1) { return ReplaceChildModportDeclInternal(old_modport_decl, new_modport_decl, delete_old_node) ; } // Delete 'old_modport_decl' from modport declaration list, and puts new modport declaration in its place.
    unsigned            ReplaceChildClockingDir(VeriClockingDirection *old_dir, VeriClockingDirection *new_dir, unsigned delete_old_node=1) { return ReplaceChildClockingDirInternal(old_dir, new_dir, delete_old_node) ; } // Delete 'old_dir', and puts new clocking direction in its place.
    unsigned            ReplaceChildProduction(VeriProduction *old_prod, VeriProduction *new_prod, unsigned delete_old_node=1) { return ReplaceChildProductionInternal(old_prod, new_prod, delete_old_node) ; } // Delete 'old_prod' and puts 'new_prod' in its place
    unsigned            ReplaceChildProductionItem(VeriProduction *old_prod_item, VeriProduction *new_prod_item, unsigned delete_old_node=1) { return ReplaceChildProductionItemInternal(old_prod_item, new_prod_item, delete_old_node) ; } // Delete 'old_prod_item' and put new production item in its place
    unsigned            ReplaceChildCaseOperatorItem(VeriCaseOperatorItem *old_case, VeriCaseOperatorItem *new_case, unsigned delete_old_node=1) { return ReplaceChildCaseOperatorItemInternal(old_case, new_case, delete_old_node) ; } // Delete 'old_case' from case item list, and puts new case item in its place.
#endif // VERILOG_USE_TREENODE_REPLACECHILD

    virtual unsigned            IsNameExtendedClass() const { return 0 ; }

    // environment check (context in which an expression occurs) for Resolve()
    enum veri_environment {       // enum for expression environment checks
        VERI_EXPRESSION,          // normal RHS expression
        VERI_EXPRESSION_UNDECL_USE_GOOD,          // normal RHS expression
        VERI_LET_EXPRESSION,      // normal RHS expression for let construct of sv 2009
        VERI_ABORT_EXPRESSION,    // conditional expression for abort functions
        VERI_STRING_TARGET,       // left-hand side is a string type variable in an assignment : VIPER# 4259
        VERI_LVALUE,              // LHS assignment to a reg or net
        VERI_PORT_EXPRESSION,     // module port expression list
        VERI_PATH_EXPRESSION,     // path expression for specify block. vcs-verific incompability
        VERI_PORT_CONNECTION,     // port connection in instantiation
        VERI_CONSTANT_EXPRESSION, // any constant expression
        VERI_PARAM_INITIAL_EXP,   // any constant expression that can be used to initialize a parameter
        VERI_INTEGRAL_CONSTANT,   // VIPER 2539 : Only integral constant expression
        VERI_DELAY_CONTROL,       // delay (control) expression
        VERI_EVENT_CONTROL,       // @(..) event (control) expression
        VERI_EVENT_CONTROL_UNDECL_USE_GOOD,       // @(..) event (control) expression
        VERI_NET_LVALUE,          // LHS assignment to a net
        VERI_NET_LVALUE_UNDECL_USE_GOOD,          // LHS assignment to a net
        VERI_REG_LVALUE,          // LHS assignment to a reg
        VERI_FORCE_LVALUE,        // LHS assignment to a net/reg in a 'force' statement
        VERI_GENVAR_LVALUE,       // LHS assignment to a genvar in a generate-for-loop
        VERI_UPWARD_SCOPE_NAME,   // function, task, and named block upward name referencing
        VERI_SYSTEM_TASK_ARG,     // system task argument handling
        VERI_PARAM_VALUE,         // value of parameters
        VERI_BINS_INIT_VALUE,     // iinit value of bins id
        VERI_TYPE_ENV,            // data type expression
        VERI_GATE_INOUT_PORT_CONNECTION, // actual of primitive gate's inout port handling (VIPER 2543)
        VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR, // VIPER# 2288, 3289 : Only reg/net/port allowed (for VeriTimingCheckEvent)
        VERI_TIMING_CHECK_REGISTER_IDENTIFIER, // VIPER# 2288, 3289 : Only reg identifier allowed (for VeriTimingCheckEvent)
        VERI_FORK_JOIN_NONE_IN_FUNC, // VIPER #4292 : For fork join_none construct inside function
        VERI_FORK_JOIN,           // VIPER #4323 : For fork-join block
        VERI_WAIT_EXP,            // VIPER #4839 : For wait call
        VERI_ALWAYS_BLOCK,        // VIPER #3955 : For contextually inferred _sva_clock_expr
        VERI_INITIAL_BLOCK,        // VIPER #3955 : For contextually inferred _sva_clock_expr
        VERI_RAND_VAR,            // only rand variables
        VERI_PROPERTY_ENV,        // property expression for SVA
        VERI_SEQUENCE_ENV,        // sequence expression for SVA
        VERI_CONSTRAINT_EXPR,     // constraint expression
        VERI_INLINE_CONSTRAINT_EXPR, // inline constraint expression (#6818)
        VERI_CLASS_NAME,          // only class name
        VERI_BIN_VAL_ENV,         // for bin values in covergroup
        VERI_PRODUCTION_ENV,      // for productions in randsequence
        VERI_SPECIAL_ALWAYS,      // for always_comb and always_latch (VIPER 2447)
        VERI_ALWAYS_FF_BLOCK,     // for always_ff (VIPER #4241)
        VERI_ACTION_BLOCK,        // for action block of assertion (VIPER #3953)
        VERI_VALUE_RANGE,         // for value range of distribution (VIPER #4124)
        VERI_FINAL_BLOCK,         // for final block (VIPER #4204)
        VERI_DEFERRED_ASSERT,     // for deferred assertion (SV-2009)
        VERI_CLOCKING_BLOCK,      // for clocking block declaration
        VERI_PROPERTY_ARG,        // for actuals of property instance (VIPER #4229)
        VERI_SEQUENCE_ARG,        // for actuals of sequence instance (sequence expr and event expr)
        VERI_PSL_PROPERTY_ENV,    // Property expression for PSL
        VERI_PSL_SEQUENCE_ENV,    // Sequence expression for PSL
        VERI_CONSTRUCTOR_LVALUE,  // lhs of assignment inside constructor (VIPER #4039)
        VERI_BRANCH_TERM,         // Terminal of branch, allow part-select on memory
        VERI_STATIC_ENV,          // used to mark decls of static task/func as static
        VERI_SEQ_PROP_INIT,       // default actual argument of sequence/property
        VERI_PROCE_CONT_ASSIGN_VAL, // VIPER #6110: used for RHS expression of procedural continuous assignments
        VERI_COVERPOINT_ENV,      // for expression of coverpoint
        VERI_UNDEF_ENV
    } ;
    static const char *         PrintEnvironment(veri_environment environment) ; // Get a char * for an environment

    // environment check (context in which an expression occurs) for CheckType
    enum veri_type_env {
        READ,             // RHS expression
        READ_INSTANCE,    // Actual for interface type formal (#3397)
        WRITE,            // LHS expression
        PARAM_INIT,       // Default value of parameter
        RANGE_BOUND,      // Bounds of range
        INIT_VAL,         // Initial value of variable
        ASSOC,            // Formal/actual association
        CASE_CONDITION,   // case condition with case item condition
        RETURN_EXPR,           // return value of function
        VALUE_RANGE_LIST, // value range list of set membership
        TASK,             // association for task call
        FUNC,             // association for function call
        PROP_SEQ,         // association for property or sequence call
        INSTANCE,         // association for instance
        CASTING,          // casting
        INTERFACE_TYPE,   // virtual interface context
        PROP_SEQ_INIT_VAL,// initial value for property/sequence formal
        INST_READ,        // Reading for instantiation (VIPER #7092)
        INST_WRITE,       // Writing for instantiation (VIPER #7092)
        NO_ENV
    } ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTreeNode(const VeriTreeNode &tree_node, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTreeNode(SaveRestore &save_restore) ;

    virtual ~VeriTreeNode() ;

protected:
    VeriTreeNode(VeriTreeNode &base, unsigned) ;
private:
    // Prevent compiler from defining the following
    VeriTreeNode(const VeriTreeNode &) ;            // Purposely leave unimplemented
    VeriTreeNode& operator=(const VeriTreeNode &) ; // Purposely leave unimplemented

public:
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const { PrettyPrint(f,level) ; } // Default catch for Print statement without semi colon at end. Differs from default for only a few classes.
    virtual void                PrettyPrintPreComments(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintPostComments(ostream &f, unsigned level, unsigned print_linefeed) const ;

    void                        ResolveAttributes(VeriScope *scope) const ; // VIPER 2536 : attribute expression is to be resolved with constant expression environment
    void                        PrettyPrintAttributes(ostream &f, unsigned level) const ;
    void                        PrettyPrintPragmas(ostream &f, unsigned level, unsigned print_attrs_too=0) const ; // VIPER #6878

    // Parse tree copy routines :
    static Map *                CopyAttributes(const Map *this_attr, VeriMapForCopy &id_map_table) ; // Static function to copy the attributes
    static Array *              CopyComments(const Array *this_comments, VeriMapForCopy &id_map_table) ; // Static function to copy the given comments
    static Array *              CopyExpressionArray(const Array *exprs, VeriMapForCopy &id_map_table) ; // Static function to copy an array of expressions
    static Array *              CopyStatementArray(const Array *stmts, VeriMapForCopy &id_map_table) ; // Static function to copy an array of statements
    static Array *              CopyModuleItemArray(const Array *items, VeriMapForCopy &id_map_table) ; // Static function to copy an array of module items
    static Array *              CopyIdArray(const Array *ids, VeriMapForCopy &id_map_table) ; // Static function to copy an array of identifiers (iddefs)

    static VeriIdDef *          CreateInstantiatedUnitName(VeriIdDef *module_id, const char *name, VeriLibrary *lib, VeriMapForCopy &id_map_table) ; // Static routine to create an unique name

    // VIPER #4241 : APIs to check procedural construct or special always constructs
    virtual unsigned            IsProceduralArea() const  { return 0 ; }
    virtual unsigned            GetSpecialAlwaysToken() const   { return 0 ; }

    virtual verific_int64       StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const { return 0 ; } // Virtual routine to get size/sign of any parse tree node, Viper #7711:return  in 64 bits
    // VIPER #7938 : Create and set new name to unnamed instance
    static const char *         CreateAndSetUniqueInstanceName(const char *mod_name, VeriIdDef *instance_id, VeriScope *present_scope) ;

    VeriDataType *              CreateAndAddTypeDef(VeriScope *container_scope, Array *container_items, VeriDataType *data_type, VeriRange *dims, const VeriModuleItem *from_data_decl) const ; // VIPER #7456 : Create and type definition

#ifdef VERILOG_USE_TREENODE_REPLACECHILD
    // Internal routines
    virtual unsigned            ReplaceChildNameInternal(VeriName * /*old_parent*/, VeriName * /*new_parent*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old name, and put new name
    virtual unsigned            ReplaceChildIdInternal(VeriIdDef * /*old_id*/, VeriIdDef * /*new_id*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old id, and put new id
    virtual unsigned            ReplaceChildExprInternal(VeriExpression * /*old_expr*/, VeriExpression * /*new_expr*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_expr', and puts new expression in its place
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement * /*old_statement*/, VeriStatement * /*new_statement*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old statement, and put new statement
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem * /*old_decl*/, VeriModuleItem * /*new_decl*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete old declaration item, and put new declaration item
    virtual unsigned            ReplaceChildDataTypeInternal(VeriDataType * /*old_data*/, VeriDataType * /*new_data*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_data' specific data type, and puts new data type in its place
    virtual unsigned            ReplaceChildStrengthInternal(VeriStrength * /*old_stre*/, VeriStrength * /*new_stre*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_stre', and puts new strength in its place.
    virtual unsigned            ReplaceChildAnsiPortDeclInternal(VeriAnsiPortDecl * /*old_port*/, VeriAnsiPortDecl * /*new_port*/, unsigned /*delete_old_node*/) { return 0 ; }  // Delete 'old_port', and puts new ansi port declaration in its place
    virtual unsigned            ReplaceChildDefParamAssignInternal(VeriDefParamAssign * /*old_assign*/, VeriDefParamAssign * /*new_assign*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old-assign' specific defparam assignment from assign list, and puts new assignment in its place
    virtual unsigned            ReplaceChildNetRegAssignInternal(VeriNetRegAssign * /*old_assign*/, VeriNetRegAssign * /*new_assign*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_assign' specific net assignment, and puts new assignment in its place
    virtual unsigned            ReplaceChildInstIdInternal(VeriInstId * /*old_inst*/, VeriInstId * /*new_inst*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_inst' from instance list, and puts new instance in its place
    virtual unsigned            ReplaceChildPathInternal(VeriPath * /*old_path*/, VeriPath * /*new_path*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_path', and puts new path in its place
    virtual unsigned            ReplaceChildGenerateCaseItemInternal(VeriGenerateCaseItem * /*old_item*/, VeriGenerateCaseItem * /*new_item*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_item' from case-generate items, and puts new case-item in its place
    virtual unsigned            ReplaceChildEventControlInternal(VeriDelayOrEventControl * /*old_control*/, VeriDelayOrEventControl * /*new_control*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_control', and puts new control in its place
    virtual unsigned            ReplaceChildCaseItemInternal(VeriCaseItem * /*old_case*/, VeriCaseItem * /*new_case*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_case' from case item list, and puts new case item in its place.
    virtual unsigned            ReplaceChildRangeInternal(VeriRange * /*old_range*/, VeriRange * /*new_range*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_range' if it is next range, and puts new range as new next range

    virtual unsigned            ReplaceChildModportDeclInternal(VeriModportDecl * /*old_modport_decl*/, VeriModportDecl * /*new_modport_decl*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_modport_decl' from modport declaration list, and puts new modport declaration in its place.
    virtual unsigned            ReplaceChildClockingDirInternal(VeriClockingDirection * /*old_dir*/, VeriClockingDirection * /*new_dir*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_dir', and puts new clocking direction in its place.
    virtual unsigned            ReplaceChildProductionInternal(VeriProduction * /*old_prod*/, VeriProduction * /*new_prod*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_prod' and puts 'new_prod' in its place
    virtual unsigned            ReplaceChildProductionItemInternal(VeriProduction * /*old_prod_item*/, VeriProduction * /*new_prod_item*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_prod_item' and put new production item in its place
    virtual unsigned            ReplaceChildCaseOperatorItemInternal(VeriCaseOperatorItem * /*old_case*/, VeriCaseOperatorItem * /*new_case*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_case' from case item list, and puts new case item in its place.
#endif // VERILOG_USE_TREENODE_REPLACECHILD

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;
    // Save Restore functions
    static void                 SaveArray(SaveRestore &save_restore, const Array *array) ;
    static Array *              RestoreArray(SaveRestore &save_restore) ;
    static void                 SaveStringArray(SaveRestore &save_restore, const Array *array) ;
    static Array *              RestoreStringArray(SaveRestore &save_restore) ;
    static void                 SaveIdDefMap(SaveRestore &save_restore, const Map *map) ;
    static Map *                RestoreIdDefMap(SaveRestore &save_restore) ;
    static void                 SaveNodePtr(SaveRestore &save_restore, const VeriTreeNode *node) ;
    static VeriTreeNode *       RestoreNodePtr(SaveRestore &save_restore) ;
    static void                 SaveScopePtr(SaveRestore &save_restore, const VeriScope *scope) ;
    static VeriScope *          RestoreScopePtr(SaveRestore &save_restore) ;
    static VeriTreeNode *       CreateObject(SaveRestore &save_restore, unsigned class_id) ;

private:
    // Source line/file info stored here. Use LineFile Utility to get line number or file name back
    linefile_type _linefile ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTreeNode

/* -------------------------------------------------------------- */

/*
   VeriCommentNode is a class which is used only when VERILOG_PRESERVE_COMMENTS switch is set
   In that case, comments will be annotated on VeriModule, VeriModuleItem, VeriStatement and VeriExpression.
*/
class VFC_DLL_PORT VeriCommentNode : public VeriTreeNode
{
public:
    explicit VeriCommentNode(linefile_type linefile = 0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERICOMMENTNODE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    void                        AppendComment(const char *str) ;
    const char *                GetCommentString() const ;
    unsigned                    GetStartingLineNumber() const ;
    unsigned                    IsBlockComment() const ;
    void                        AdjustClosingLinefile(unsigned right_line, unsigned right_col) ; // Set the terminating linefile info
    unsigned                    IsCommentClosed() const ; // Returns 1 if the comment is closed, 0 otherwise!

    VeriCommentNode *           CopyComment() const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCommentNode(const VeriCommentNode &tree_node, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriCommentNode(SaveRestore &save_restore) ;

    ~VeriCommentNode() ;

private:
    // Prevent compiler from defining the following
    VeriCommentNode(const VeriCommentNode &) ;            // Purposely leave unimplemented
    VeriCommentNode& operator=(const VeriCommentNode &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    //virtual void                PrettyPrint(ostream &f, unsigned level) const { VeriTreeNode::PrettyPrint(f, level) ; }

private:
    char *_str ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCommentNode

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_TREE_NODE_H_
