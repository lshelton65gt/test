/*
 *
 * [ File Version : 1.11 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_COPY_H_
#define _VERIFIC_VERI_COPY_H_

#include "VerificSystem.h"
#include "VeriCompileFlags.h"
#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// The folloing class and its static methods are a general utility
// methods, mostly used in VeriExprEvaluate.cpp. While constant
// expression evaluation, all the calculations are done in string
// domain. So these methods proved very handfull there.

//Forward Declarations
class Array ;
class VeriIdDef ;
class VeriScope ;
class VeriModport ;
class VeriDataDecl ;

/* -------------------------------------------------------------- */

/*
   It is used in virtual VeriTreeNode::Copy() routine. Here the Map contains the
   information of VeriIdDef vs. copied VeriIdDef, and VeriScope vs. copied VeriScope.
   For details please refer VeriCopy_Stat.cpp.
*/

class VFC_DLL_PORT VeriMapForCopy : public Map
{
public:
    explicit VeriMapForCopy(hash_type type = POINTER_HASH, unsigned init_bucket_size = 2) ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
    // Note: Destructor of base class Map is not virtual, do not delete a VeriMapForCopy object by Map *
    ~VeriMapForCopy() ;

    void                AddModport(const VeriModport *modport) ;
    Array *             GetModports() { return _modports ; }
    // Process the modports to set the proper data type of the modport port ids:
    //void                ProcessModports(const VeriScope *active_scope, const VeriTreeNode *from = 0) ;
    void                SetCopiedClassId(const VeriIdDef *old_id, const VeriIdDef *new_id) ;
    VeriIdDef *         GetCopiedClassId(const VeriIdDef *id) const ;
    Array *             GetDeclarations() const { return _decls ; }
    void                AddDecl(const VeriDataDecl* new_decl) ;
    void                SetAsInternal(unsigned s) { _is_internal = s ; }
    unsigned            IsInternal() const { return _is_internal ; }
    // VIPER #7508 : Store package scope map (old scope vs copied scope)
    void                SetPkgScopeMap(Map *m) { _pkg_scope_map = m ; }
    Map *               GetPkgScopeMap() const { return _pkg_scope_map ; }
    // VIPER #7913 (side-effect): Store the array interface instance (and created virtual array interface) ids:
    void                SetInterfaceInstIds(Array *ids) { _intf_inst_ids = ids ; }
    Array *             GetInterfaceInstIds() const     { return _intf_inst_ids ; }
    void                MakeAssociation(const VeriScope *old_scope, const VeriScope *new_scope) ;
    void                MakeAssociationForPkg(const VeriScope *old_scope, const VeriScope *new_scope, VeriIdDef *ignore_id, VeriScope *ignore_scope) ;

private:
    // Prevent compiler from defining the following
    VeriMapForCopy(const VeriMapForCopy &) ;            // Purposely leave unimplemented
    VeriMapForCopy& operator=(const VeriMapForCopy &) ; // Purposely leave unimplemented

    Array *_modports ; // Store modports
    Map   *_class_id_map ; // Map to store class id while copying class
    Map   *_pkg_scope_map ; // Map to store old pkg scope vs copied pkg scope (VIPER #7508)
    Array *_intf_inst_ids ; // VIPER #7913 (side-effect): Store the array interface instance (and created virtual array interface) ids
    // VIPER #7456 : Store the copied declarations to process type operator
    Array *_decls ; // array of copied declarations
    unsigned _is_internal ; // Processing seq/par block
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriMapForCopy

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_COPY_H_

