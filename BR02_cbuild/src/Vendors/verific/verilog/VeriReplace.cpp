/*
 *
 * [ File Version : 1.11 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "VeriExpression.h"
#include "VeriId.h"
#include "VeriStatement.h"
#include "VeriTreeNode.h"
#include "Array.h"
#include "VeriScope.h" // For Add/Remove routines
#include "Strings.h" // For Add/Remove routines
#include "veri_tokens.h" // For Add/Remove routines

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Replacement routines are very essential while doing the elaboration. Many
// a times we need to detach an element from the parse tree and then delete it,
// or we do need to replace it. Again many a times we need only to detach an element
// so that it can be added in another place.
// For example,
// * In generate elaboration after unrolling the generate statement, the actual
// generate statement needs to be detached from the tree i.e. the module item
// list, and then should be deleted.
//
// * Another example could be defparam statements. Here after overriding the respective
// parameter values, we used to detach the defparam statement from module item list and then delete it.
//
// * Another example could be for-generate statements. Here while unrolling, the
// genvar node should be replaced with the appropriate value node.
//
// * In generate elaboration if a function is declared between the keyword generate
// and endgenerate, we have to detach this function declaration from generate item
// list and to add this in module item list. So we can avoid copying the declaration
// if it is not within for-generate statement.
//
// So replacement routines do all these things. If one calls ReplaceChild(child, 0),
// then the routine will replace 'child' node with '0', i.e. it will detach 'child'
// from ('this')its parent in the parse tree, and will put a '0' in its placeholder
// in the parent node and delete child.

#define REPLACE_IN_ARRAY(A, O, N, D) \
{ \
    unsigned I ; \
    VeriTreeNode *node ; \
    FOREACH_ARRAY_ITEM(A, I, node) { \
        if (node == (O)) { \
            if (D) delete O ;  \
            (A)->Insert(I, N) ; \
            return 1 ; \
        } \
    } \
}

#define REPLACE_IN_NODE(C, O, N, D) \
    if ((C) == (O)) { \
        if (D) delete O ; \
        C = N ; \
        return 1 ; \
    }

////////////////////////////////////////////////////////////////////////////
////////////////Replace routines without using Parent pointers /////////////
////////////////////////////////////////////////////////////////////////////

unsigned VeriDataDecl::ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_data, new_data, delete_old_node) ;
    return 0 ;
}

unsigned VeriNetDecl::ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_strength, old_stre, new_stre, delete_old_node) ;
    return 0 ;
}

unsigned VeriNetDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_delay, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriFunctionDecl::ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_data, new_data, delete_old_node)  ;
    return 0 ;
}

unsigned VeriFunctionDecl::ReplaceChildAnsiPortDeclInternal(VeriAnsiPortDecl *old_port, VeriAnsiPortDecl *new_port, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_ansi_io_list, old_port, new_port, delete_old_node) ;
    return 0 ;
}

unsigned VeriFunctionDecl::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriFunctionDecl::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_stmts, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriTaskDecl::ReplaceChildAnsiPortDeclInternal(VeriAnsiPortDecl *old_port, VeriAnsiPortDecl *new_port, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_ansi_io_list, old_port, new_port, delete_old_node) ;
    return 0 ;
}

unsigned VeriTaskDecl::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriTaskDecl::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_stmts, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}
unsigned VeriDefParam::ReplaceChildDefParamAssignInternal(VeriDefParamAssign *old_def_assign, VeriDefParamAssign *new_def_assign, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_defparam_assigns, old_def_assign, new_def_assign, delete_old_node) ;
    return 0 ;
}
unsigned VeriContinuousAssign::ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_strength, old_stre, new_stre, delete_old_node) ;
    return 0 ;
}

unsigned VeriContinuousAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_delay, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriContinuousAssign::ReplaceChildNetRegAssignInternal(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_net_assigns, old_assign, new_assign, delete_old_node) ;
    return 0 ;
}

unsigned VeriGateInstantiation::ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_strength, old_stre, new_stre, delete_old_node) ;
    return 0 ;
}

unsigned VeriGateInstantiation::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_delay, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriGateInstantiation::ReplaceChildInstIdInternal(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_insts, old_inst, new_inst, delete_old_node) ;
    return 0 ;
}

unsigned VeriModuleInstantiation::ReplaceChildStrengthInternal(VeriStrength *old_stre, VeriStrength *new_stre, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_strength, old_stre, new_stre, delete_old_node) ;
    return 0 ;
}

unsigned VeriModuleInstantiation::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_param_values, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriModuleInstantiation::ReplaceChildInstIdInternal(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(&_insts, old_inst, new_inst, delete_old_node) ;
    return 0 ;
}

unsigned VeriSpecifyBlock::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_specify_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriPathDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_delay, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriPathDecl::ReplaceChildPathInternal(VeriPath *old_path, VeriPath *new_path, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_path, old_path, new_path, delete_old_node) ;
    return 0 ;
}

unsigned VeriSystemTimingCheck::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_args, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriInitialConstruct::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriAlwaysConstruct::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateConstruct::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateConditional::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_then_item, old_item, new_item, delete_old_node) ;
    REPLACE_IN_NODE(_else_item, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateConditional::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_if_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateCase::ReplaceChildGenerateCaseItemInternal(VeriGenerateCaseItem *old_item, VeriGenerateCaseItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_case_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateCase::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateFor::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_initial, old_item, new_item, delete_old_node) ;
    REPLACE_IN_NODE(_rep, old_item, new_item, delete_old_node) ;
    REPLACE_IN_ARRAY(_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriGenerateFor::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateBlock::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriNetDataType::ReplaceChildDataTypeInternal(VeriDataType *old_type, VeriDataType *new_type, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_type, new_type, delete_old_node) ;
    return 0 ;
}
unsigned VeriClass::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_base_class_name, old_name, new_name, delete_old_node) ;
    REPLACE_IN_ARRAY(_interface_classes, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriClass::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_parameter_connects, old_item, new_item, delete_old_node) ;
    REPLACE_IN_ARRAY(_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriPropertyDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_spec, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriPropertyDecl::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriSequenceDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_spec, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriSequenceDecl::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriModport::ReplaceChildModportDeclInternal(VeriModportDecl *old_modport_decl, VeriModportDecl *new_modport_decl, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_modport_decls, old_modport_decl, new_modport_decl, delete_old_node) ;
    return 0 ;
}

unsigned VeriModportDecl::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_modport_port_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriClockingDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_clocking_event, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriClockingDecl::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_clocking_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriConstraintDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_constraint_blocks, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriBindDirective::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_target_inst_list, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriBindDirective::ReplaceChildModuleItemInternal(VeriModuleItem *old_instantiation, VeriModuleItem *new_instantiation, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_instantiation, old_instantiation, new_instantiation, delete_old_node) ;
    return 0 ;
}
unsigned VeriOperatorBinding::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_func_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriOperatorBinding::ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_data, new_data, delete_old_node) ;

    REPLACE_IN_ARRAY(_formals, old_data, new_data, delete_old_node) ;
    return 0 ;
}
unsigned VeriNetAlias::ReplaceChildExpressionInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    return ReplaceChildExprInternal(old_expr, new_expr, delete_old_node) ;
}
unsigned VeriNetAlias::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_members, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriTimeUnit::ReplaceChildExpressionInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    return ReplaceChildExprInternal(old_expr, new_expr, delete_old_node) ;
}
unsigned VeriTimeUnit::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_literal, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriClockingSigDecl::ReplaceChildClockingDirInternal(VeriClockingDirection *old_dir, VeriClockingDirection *new_dir, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_clk_dir, old_dir, new_dir, delete_old_node) ;
    REPLACE_IN_NODE(_o_clk_dir, old_dir, new_dir, delete_old_node) ;
    return 0 ;
}
unsigned VeriCovergroup::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression* new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_port_connects, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_event, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriCovergroup::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriCoverageOption::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression* new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_value, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriCoverageOption::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_option, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriCoverageSpec::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression* new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_point_list, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_iff_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriCoverageSpec::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_bins, old_item, new_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriStructUnion::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriSequenceConcat::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    if (VeriBinaryOperator::ReplaceChildExprInternal(old_expr, new_expr, delete_old_node)) return 1 ;
    REPLACE_IN_NODE(_cycle_delay_range, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriClockedSequence::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    if (VeriUnaryOperator::ReplaceChildExprInternal(old_expr, new_expr, delete_old_node)) return 1 ;
    REPLACE_IN_NODE(_event_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriAssignInSequence::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_stmts, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriDistOperator::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    if (VeriUnaryOperator::ReplaceChildExprInternal(old_expr, new_expr, delete_old_node)) return 1 ;
    REPLACE_IN_ARRAY(_dist_list, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriSolveBefore::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_solve_list, old_name, new_name, delete_old_node) ;
    REPLACE_IN_ARRAY(_before_list, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriCast::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_target_type, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriNew::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_args, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_size_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriConcatItem::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_member_label, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriAssignmentPattern::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    // Replace the type expression
    REPLACE_IN_NODE(_type, old_expr, new_expr, delete_old_node) ;
    // Replace the expressions :
    if (VeriConcat::ReplaceChildExprInternal(old_expr, new_expr, delete_old_node)) return 1 ;
    return 0 ;
}

unsigned VeriMultiAssignmentPattern::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    // Replace the type expression
    REPLACE_IN_NODE(_type, old_expr, new_expr, delete_old_node) ;
    // Replace the repeat expression or concat element :
    if (VeriMultiConcat::ReplaceChildExprInternal(old_expr, new_expr, delete_old_node)) return 1 ;
    return 0 ;
}

unsigned VeriStreamingConcat::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    // Replace slice size
    REPLACE_IN_NODE(_slice_size, old_expr, new_expr, delete_old_node) ;
    // Replace stream expressions
    if (VeriConcat::ReplaceChildExprInternal(old_expr, new_expr, delete_old_node)) return 1 ;
    return 0 ;
}
unsigned VeriCondPredicate::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_right, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriDotName::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriTaggedUnion::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_member, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriTaggedUnion::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriWithExpr::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_right, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriInlineConstraint::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_constraint_block, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriOpenRangeBinValue::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_iff_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_open_range_list, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriTransBinValue::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_iff_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_trans_list, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriDefaultBinValue::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_iff_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriSelectBinValue::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_iff_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_select_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriTransSet::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_trans_range_list, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriTransRangeList::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_trans_item, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_repetition, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriSelectCondition::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_range_list, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_bins_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriTypeOperator::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr_or_data_type, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriPatternMatch::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_right, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriConstraintSet::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_exprs, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

// VIPER #5710(vcs_compatibility: always_checker(for checker instantiation inside procedural code)):
unsigned VeriSequentialInstantiation::ReplaceChildInstIdInternal(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(&_insts, old_inst, new_inst, delete_old_node) ;
    return 0 ;
}

unsigned VeriAssertion::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_property_spec, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriAssertion::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_then_stmt, old_stmt, new_stmt, delete_old_node) ;
    REPLACE_IN_NODE(_else_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriJumpStatement::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriDoWhile::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriEnum::ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_base_type, old_data, new_data, delete_old_node) ;
    return 0 ;
}
unsigned VeriForeach::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_array, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriWaitOrder::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_events, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriWaitOrder::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_then_stmt, old_stmt, new_stmt, delete_old_node) ;
    REPLACE_IN_NODE(_else_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}
unsigned VeriRandsequence::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_start_prod, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriRandsequence::ReplaceChildProductionInternal(VeriProduction *old_prod, VeriProduction *new_prod, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_productions, old_prod, new_prod, delete_old_node) ;
    return 0 ;
}
unsigned VeriCodeBlock::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decls, old_item, new_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriCodeBlock::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_stmts, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}
unsigned VeriProduction::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_formals, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriProduction::ReplaceChildDataTypeInternal(VeriDataType *old_type, VeriDataType *new_type, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_type, new_type, delete_old_node) ;
    return 0 ;
}
unsigned VeriProduction::ReplaceChildProductionItemInternal(VeriProduction *old_prod_item, VeriProduction *new_prod_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_prod_item, new_prod_item, delete_old_node) ;
    return 0 ;
}
unsigned VeriProductionItem::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_w_spec, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriProductionItem::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_stmt, new_stmt, delete_old_node) ;
    REPLACE_IN_NODE(_code_blk, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}
unsigned VeriArrayMethodCall::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_right, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriArrayMethodCall::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}
unsigned VeriInlineConstraintStmt::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_constraint_block, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriInlineConstraintStmt::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}
unsigned VeriImportDecl::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriLetDecl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriDefaultDisableIff::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr_or_dist, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriExportDecl::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_items, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriIndexedExpr::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_prefix, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_idx, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriScopeName::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_param_values, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriNamedPort::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_port_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriCovgOptionId::ReplaceChildDataTypeInternal(VeriDataType*old_type, VeriDataType *new_type, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_type, new_type, delete_old_node) ;
    return 0 ;
}
unsigned VeriCaseOperator::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriCaseOperator::ReplaceChildCaseOperatorItemInternal(VeriCaseOperatorItem *old_case, VeriCaseOperatorItem *new_case, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_case_items, old_case, new_case, delete_old_node) ;
    return 0 ;
}
unsigned VeriCaseOperatorItem::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_conditions, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_property_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriTypeRef::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_type_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}
unsigned VeriTypeRef::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    return (_type_name) ? _type_name->ReplaceChildExprInternal(old_expr, new_expr, delete_old_node): 0 ;
}

unsigned VeriIndexedId::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_idx, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriIndexedId::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_prefix, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriSelectedName::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_prefix, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriIndexedMemoryId::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_indexes, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriIndexedMemoryId::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_prefix, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriConcat::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_exprs, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriMultiConcat::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_repeat, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_exprs, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriFunctionCall::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_args, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriFunctionCall::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_func_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriSystemFunctionCall::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_args, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriMinTypMaxExpr::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_min_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_typ_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_max_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriUnaryOperator::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_arg, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriBinaryOperator::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_right, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriQuestionColon::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_if_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_then_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_else_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriEventExpression::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_expr, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_iff_condition, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriPortConnect::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_connection, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriAnsiPortDecl::ReplaceChildDataTypeInternal(VeriDataType *old_data, VeriDataType *new_data, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_data_type, old_data, new_data, delete_old_node) ;
    return 0 ;
}

unsigned VeriTimingCheckEvent::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_terminal_desc, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_condition, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriRange::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_left, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_right, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriRange::ReplaceChildRangeInternal(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_next, old_range, new_range, delete_old_node) ;
    return 0 ;
}

unsigned VeriDataType::ReplaceChildRangeInternal(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_dimensions, old_range, new_range, delete_old_node) ;
    return 0 ;
}

// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
unsigned VeriPathPulseVal::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_reject_limit, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_error_limit, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriBlockingAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_lval, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_val, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriBlockingAssign::ReplaceChildEventControlInternal(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_control, old_control, new_control, delete_old_node) ;
    return 0 ;
}

unsigned VeriNonBlockingAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_lval, old_expr, new_expr, delete_old_node) ;
    // VIPER #3917: Find and replace the old expression with the new expression in _val also:
    REPLACE_IN_NODE(_val, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriNonBlockingAssign::ReplaceChildEventControlInternal(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_control, old_control, new_control, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenVarAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_value, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriAssign::ReplaceChildNetRegAssignInternal(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_assign, old_assign, new_assign, delete_old_node) ;
    return 0 ;
}

unsigned VeriDeAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_lval, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriForce::ReplaceChildNetRegAssignInternal(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_assign, old_assign, new_assign, delete_old_node) ;
    return 0 ;
}

unsigned VeriRelease::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_lval, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriTaskEnable::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_task_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriTaskEnable::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_args, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriSystemTaskEnable::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_args, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriDelayControlStatement::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_delay, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriDelayControlStatement::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriEventControlStatement::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_at, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriEventControlStatement::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriConditionalStatement::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_if_expr, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriConditionalStatement::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_then_stmt, old_stmt, new_stmt, delete_old_node) ;
    REPLACE_IN_NODE(_else_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriCaseStatement::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriCaseStatement::ReplaceChildCaseItemInternal(VeriCaseItem *old_case, VeriCaseItem *new_case, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_case_items, old_case, new_case, delete_old_node) ;
    return 0 ;
}

unsigned VeriLoop::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriRepeat::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriWhile::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriFor::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_initials, old_item, new_item, delete_old_node) ;
    REPLACE_IN_ARRAY(_reps, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriFor::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriWait::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_cond, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriWait::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriDisable::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_task_block_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriEventTrigger::ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_event_name, old_name, new_name, delete_old_node) ;
    return 0 ;
}

unsigned VeriSeqBlock::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decl_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriSeqBlock::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_stmts, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriParBlock::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_decl_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriParBlock::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_stmts, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriModule::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_package_import_decls, old_item, new_item, delete_old_node) ;
    REPLACE_IN_ARRAY(_parameter_connects, old_item, new_item, delete_old_node) ;
    REPLACE_IN_ARRAY(_module_items, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriModule::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_port_connects, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriNetRegAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_lval, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_val, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
unsigned VeriDefParamAssign::ReplaceChildNameInternal(VeriName *old_def_ref, VeriName *new_def_ref, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_lval, old_def_ref, new_def_ref, delete_old_node) ;
    return 0 ;
}
unsigned VeriDefParamAssign::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_val, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriInstId::ReplaceChildRangeInternal(VeriRange *old_range, VeriRange *new_range, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_dimensions, old_range, new_range, delete_old_node) ;
    return 0 ;
}

unsigned VeriInstId::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(&_port_connects, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriCaseItem::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_conditions, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriCaseItem::ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_stmt, old_stmt, new_stmt, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateCaseItem::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_conditions, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriGenerateCaseItem::ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_item, old_item, new_item, delete_old_node) ;
    return 0 ;
}

unsigned VeriPath::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_ARRAY(_in_terminals, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_out_terminals, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_data_source, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

unsigned VeriDelayOrEventControl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_delay_control, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_NODE(_repeat_event, old_expr, new_expr, delete_old_node) ;
    REPLACE_IN_ARRAY(_event_control, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}
#ifdef VERILOG_PATHPULSE_PORTS
unsigned VeriPathPulseValPorts::ReplaceChildNameInternal(VeriName *old_port_ref, VeriName *new_port_ref, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_input_port, old_port_ref, new_port_ref, delete_old_node) ;
    REPLACE_IN_NODE(_output_port, old_port_ref, new_port_ref, delete_old_node) ;
    return 0 ;
}
#endif
unsigned VeriClockingDirection::ReplaceChildEventControlInternal(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node)
{
    REPLACE_IN_NODE(_delay_control, old_control, new_control, delete_old_node) ;
    return 0 ;
}
// Replace the argument specific module item by a list of module items. First argument
// specific module item is deleted. If list of new module items is not given, first
// argument specific module item is deleted from parse tree.
unsigned VeriModuleItem::ReplaceChildByInternal(Array *module_items, VeriModuleItem *old_item, const Array *new_items)
{
    if (!old_item) return 0 ;
    // No new item list, remove 'old_item' from module items and delete that
    if (!new_items) return ReplaceChildModuleItemInternal(old_item, 0, 1 /*delete_old_node*/) ;
    unsigned old_item_pos = 0 ;
    VeriModuleItem *mod_item ;
    unsigned bfound = 0 ;
    // Find the position of the module item to be deleted
    FOREACH_ARRAY_ITEM(module_items, old_item_pos, mod_item) {
        if (mod_item == old_item) {
            bfound = 1 ;
            module_items->Insert(old_item_pos, 0) ;
            break ;
        }
    }
    if (!bfound || !module_items) {
        return 0 ; // desired module item not found, return 0
    }
    unsigned j ;
    // Store the module items below the 'old_item' of the module item list in the following array
    Array remaining_elements(module_items->Size() - old_item_pos) ;
    for(j = old_item_pos + 1; j < module_items->Size(); j++) remaining_elements.Insert(module_items->At(j)) ;

    // Increment the position of the item to be deleted, so that others items are
    // inserted after this position, the position of the item to be deleted is filled with null
    old_item_pos++ ;

    // Add the new module items in the module item list starting from the position of the 'old_item'
    FOREACH_ARRAY_ITEM(new_items, j, mod_item) {
        if (!mod_item) continue ;
        module_items->Insert(old_item_pos, mod_item) ;
        old_item_pos++ ;
    }
    // Add the module items below the 'old_item' of the module item list again in the extended module item list
    FOREACH_ARRAY_ITEM(&remaining_elements, j, mod_item) {
        module_items->Insert(old_item_pos, mod_item) ;
        old_item_pos++ ;
    }

    // Delete the old_item
    if (old_item->IsBindDirective()) {
        old_item->RemoveFromHierModule() ;
    }
    delete old_item ;
    return 1 ;
}
unsigned VeriModule::ReplaceChildBy(VeriModuleItem *old_item, const Array *new_items)
{
    return ReplaceChildByInternal(_module_items, old_item, new_items) ;
}
unsigned VeriGenerateBlock::ReplaceChildBy(VeriModuleItem *old_item, const Array *new_items)
{
    return ReplaceChildByInternal(_items, old_item, new_items) ;
}
unsigned VeriPulseControl::ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) // Deletes 'old_expr', and puts new expr in its place
{
    REPLACE_IN_ARRAY(_path_list, old_expr, new_expr, delete_old_node) ;
    return 0 ;
}

///////////////////////////////////////////////////////////////////////////////////////////
//              Parse-tree Port Creation/Deletion Routines                               //
///////////////////////////////////////////////////////////////////////////////////////////

VeriIdDef *
VeriModule::AddPort(const char *port_name, unsigned port_direction, VeriDataType *subtype_indication)
{
    if (!port_name || !port_direction || !_scope) return 0 ; // Impossible to create port

    // Check if there is an existing identifier by that name
    VeriIdDef *new_port = _scope->FindLocal(port_name) ;
    if (new_port) {
        // Legal to re-declare a variable (or net) in some cases (port declarations).
        // So let this pass through, and error out only once we know if there is a real violation.
        if (!new_port->IsVar()) {
            // If its not a variable, error out
            new_port->Error("%s is already declared", new_port->Name()) ;
            // VIPER #7773: No need to print info message, this is the location of the actual declaration.
        }
        // VIPER 1578 - Clear implicit net setting (if set)
        new_port->ClearImplicitNet() ;
    } else {
        // Create iddef using 'port_name'
        new_port = new VeriVariable(Strings::save(port_name)) ;

        // Add created identifier to module scope
        if (!_scope->Declare(new_port)) { // Identifier 'port_name' already exists in module scope
            delete new_port ; // Delete created port identifier
            return 0 ; // Failed to add port
        }
    }

    // Determine the style of port declaration in module (ansi or 95)

    // Get the first port expression from port list of module
    VeriExpression *first_port = (_port_connects && _port_connects->Size()) ? (VeriExpression*)_port_connects->At(0) : 0 ;

    // Check whether first port expression is port declaration/expression
    if (first_port && first_port->IsAnsiPortDecl()) { // Ansi style port declaration
        // Create a new VeriAnsiPortDecl
        VeriAnsiPortDecl *decl = new VeriAnsiPortDecl(port_direction, subtype_indication, new_port) ;

        // Add created port declaration to port list
        VERIFIC_ASSERT(_port_connects) ; // must be there, we got the first_port from it
        _port_connects->InsertLast(decl) ;

        // Type inference created ansi port declaration
        decl->Resolve(_scope, VERI_PORT_EXPRESSION) ;
    } else { // 95 style port declaration
        // Create and add port expression to port list
        VeriIdRef *new_port_expr = new VeriIdRef(new_port) ; // Create port expression
        if (!_port_connects) _port_connects = new Array(1) ; // Create port list if not exists
        _port_connects->InsertLast(new_port_expr) ;          // Insert create port expression as last element

        // Create I/O declaration
        Array *id_list = new Array(1) ; // Create an array to insert created port id
        id_list->InsertLast(new_port) ; // Add created port id to id list
        VeriDataDecl *decl = new VeriDataDecl(port_direction, subtype_indication, id_list) ; // Create decl

        // Add created I/O decl after last port decl found in module item list
        unsigned last_decl_pos = 0 ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module item list
            if (!item || !item->IsIODecl()) continue ;
            last_decl_pos = i + 1 ; // Update 'last_decl_pos' with I/O decl position
        }
        if (!_module_items) _module_items = new Array(1) ; // Create module item list if not exists
        if (!last_decl_pos) { // Module item list has no IO decl
            // VIPER #3466: No IO declaration found, insert at the beginning:
            _module_items->InsertFirst(decl) ; // Insert created declaration as first item
        } else { // Module item list has IO decls
            _module_items->InsertAfter(last_decl_pos - 1, decl) ; // Insert created declaration after last existed IO decl
        }

        // Type inference created port expression
        new_port_expr->Resolve(_scope, VERI_PORT_EXPRESSION) ;

        // Type inference created port declaration
        decl->Resolve(_scope, VERI_UNDEF_ENV) ;
    }
    // Update ordered list of port identifiers
    if (!_ports) _ports = new Array(1) ; // Create port identifier list if not exists
    _ports->InsertLast(new_port) ; // Add created port identifier to the list

    return new_port ; // Addition completed, return created port identifier
}

unsigned
VeriModule::RemovePort(const char *port_name)
{
    if (!port_name) return 0 ; // Nothing to remove

    // Get the declared port 'port_name' from module
    VeriIdDef *port_id = FindDeclared(port_name) ;

    if (!port_id || !port_id->IsPort()) return 0 ; // No port of name 'port_name' exists, return 0

    unsigned i, j = 0 ;
    // Module maintained an ordered list of port identifiers. Remove 'port_name' from it.
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ports, i, id) { // Iterate over port identifier list
        if (id == port_id) { // Found desired port id
            _ports->Remove(i) ; // Remove desired port id from list
            break ;
        }
    }

    // Determine the style of port declaration in module (ansi or 95)

    // Get the first port expression from port list of module
    VeriExpression *first_port = (_port_connects) ? (VeriExpression*)_port_connects->At(0) : 0 ;

    // Check whether first port expression is port declaration/expression
    if (first_port && first_port->IsAnsiPortDecl()) { // Ansi style port declaration
        // Find VeriAnsiPortDecl containing 'port_name'
        VeriExpression *port_expr ;

        FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) { // Iterate over port list
            Array *ids = port_expr ? port_expr->GetIds() : 0 ; // Get identifier list
            FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over port ids
                if (!id || (id != port_id)) continue ;

                if (ids->Size() == 1) { // Id list contains only one id
                    _port_connects->Remove(i) ; // Remove VeriAnsiPortDecl from port list
                    delete port_expr ; // Delete entire ansi declaration
                    if (!_port_connects->Size()) { // '_port_connects' becomes empty array
                        delete _port_connects ;
                        _port_connects = 0 ;
                    }
                } else { // VeriAnsiPortDecl has multiple ports
                    ids->Remove(j) ; // Remove 'port_id' from id list
                }

                // Remove port_id from scope
                if (_scope) (void) _scope->Undeclare(port_id) ;
                delete port_id ; // Delete port_id
                return 1 ; // Removing completed, return 1
            }
        }
    } else { // 95 style port declaration
        // Find 'port_name' defined port expression in port list
        VeriExpression *port_expr ;
        FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
            if (port_expr && (port_expr->GetId() == port_id)) { // Found port expression
                _port_connects->Remove(i) ; // Remove port expression from port list
                delete port_expr ; // Delete port expression
                break ; // No need to iterate further
            }
        }

        // Find I/O declaration of 'port_name' in module item list
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module items
            Array *id_list = (item && item->IsDataDecl()) ? item->GetIds() : 0 ;
            // Multiple data decl can have 'port_id'
            FOREACH_ARRAY_ITEM(id_list, j, id) { // Iterate over port ids
                if (id != port_id) continue ;
                // Found id
                if (id_list->Size() == 1) { // Declaration contains one id
                    _module_items->Remove(i) ; // Remove data declaration from module item list
                    delete item ; // Delete data declaration
                } else { // Declaration contains multiple ids
                    id_list->Remove(j) ; // Remove port id from id list
                }
                break ;
            }
        }
        // Remove port_id from scope
        if (_scope) (void) _scope->Undeclare(port_id) ;
        delete port_id ; // Delete port_id
        return 1 ; // Removing completed, return 1
    }
    return 0 ; // Failed to remove
}

///////////////////////////////////////////////////////////////////////////////////////////
//              Parse-tree Parameter Creation/Deletion Routines                          //
///////////////////////////////////////////////////////////////////////////////////////////

VeriIdDef *
VeriModule::AddParameter(const char *parameter_name, VeriDataType *subtype_indication, VeriExpression *init_assign)
{
     if (!parameter_name || !init_assign || !_scope) return 0 ; // Impossible to add parameter

    // Create iddef using 'parameter_name'
    VeriIdDef *new_parameter = new VeriParamId(Strings::save(parameter_name)) ;

    // Set new_parameter to the desired value
    new_parameter->SetInitialValue(init_assign) ;

    // Add created identifier to module scope
    if (!_scope->Declare(new_parameter)) { // Identifier 'parameter_name' already exists in module scope
        delete new_parameter ; // Delete created parameter identifier
        return 0 ; // Failed to add parameter
    }

    // Create VeriDataDecl
    VeriDataDecl *data_decl = new VeriDataDecl(VERI_PARAMETER, subtype_indication, new_parameter) ;

    // If parameter_connects exists insert data_decl at last
    if (_parameter_connects) {
        _parameter_connects->InsertLast(data_decl) ;
    } else {
        VeriModuleItem *item ;
        unsigned i, last_param_pos = 0, first_port_pos = 0 ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module item list
            if (!item) continue ;

            // Store the position of last occurrence of parameter decl
            if (item->IsParamDecl()) last_param_pos = i + 1 ;

            // Store the position of first occurrence of port decl
            if (item->IsIODecl() && !first_port_pos) first_port_pos = i + 1 ;
        }

        // Create new storage if _module_items do not exist
        if (!_module_items) _module_items = new Array(1) ;

        if (last_param_pos) {
            // If at least one parameter exists
            _module_items->InsertAfter(last_param_pos - 1, data_decl) ;
        } else if (first_port_pos) {
            // If no parameter exist but at least one port declaration exists
            _module_items->InsertBefore(first_port_pos - 1, data_decl) ;
        } else {
            // VIPER #3466: If both parameter and port lists are absent, insert at the beginning:
            _module_items->InsertFirst(data_decl) ;
        }
    }

    // Type inference created port declaration
    data_decl->Resolve(_scope, VERI_UNDEF_ENV) ;

    // If _prameters do not exist then create one
    if (!_parameters) _parameters = new Array(1) ;
    _parameters->InsertLast(new_parameter) ;

    return new_parameter ;
}

unsigned
VeriModule::RemoveParameter(const char *parameter_name)
{
    if (!parameter_name) return 0 ; // Nothing to remove

    // Get the declared parameter 'parameter_name' from module
    VeriIdDef *parameter_id = FindDeclared(parameter_name) ;

    if (!parameter_id || !parameter_id->IsParam()) return 0 ; // No parameter by name 'parameter_name' exists, return 0

    unsigned i, j = 0 ;
    // Find VeriIdDefs in VeriModule by iterating over _parameters
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_parameters, i, id) { // Iterate over _parameters items
        if (id == parameter_id) {
            _parameters->Remove(i) ;
            break ;
        }
    }

    // Module maintained an ordered list of module_items identifiers. Remove 'parameter_name' from it.
    VeriModuleItem *item ;
    if (_parameter_connects){
        // For Ansi declaration _parameter_connects maintains an ordered list of
        // parameter declarations. Remove declaration for 'parameter_name' from it.
        FOREACH_ARRAY_ITEM(_parameter_connects, i, item) { // Iterate over parameter_connects connects items
            Array *ids = (item) ? item->GetIds() : 0 ;
            FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over parameter identifiers in parameter decl
                if (id != parameter_id) continue ; // Not matched
                // Found desired id
                if (ids->Size() == 1) { // Parameter declaration contains one parameter id
                    _parameter_connects->Remove(i) ; // Remove parameter declaration from parameter_connects list
                    delete item ;       // Delete parameter declaration
                    if (!_parameter_connects->Size()) { // Parameter port list has no element
                        delete _parameter_connects ; // Delete parameter port list
                        _parameter_connects = 0 ; // Set null to _parameter_connects
                    }
                } else {                // Found parameter declaration contains mulltiple parameter ids
                    ids->Remove(j) ;    // Remove 'parameter_id' from id list of parameter declaration
                }
                // Remove parameter identifier from scope
                if (_scope) (void) _scope->Undeclare(parameter_id) ;
                delete parameter_id ; // Delete parameter identifier
                return 1 ;            // Success. Desired parameter removed
            }
        }
    } else { // Desired parameter declaration is in module item list. Remove from it.
        FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module_items
            Array *ids = (item && item->IsParamDecl()) ? item->GetIds() : 0 ;
            FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over parameter identifiers
                if (id != parameter_id) continue ; // Not matched
                // Found desired id
                if (ids->Size() == 1) { // Parameter declaration contains one parameter id
                    _module_items->Remove(i) ;  // Remove parameter declaration from module item list
                    delete item ;       // Delete parameter declaration
                } else {                // Found parameter declaration contains mulltiple parameter ids
                    ids->Remove(j) ;    // Remove 'parameter_id' from id list of parameter declaration
                }
                // Remove parameter identifier from scope
                if (_scope) (void) _scope->Undeclare(parameter_id) ;
                delete parameter_id ;   // Delete parameter identifier
                return 1 ;              // Success. Desired parameter removed
            }
        }
    }

    return 0 ; // Failure. Not able to remove the desired parameter
}

///////////////////////////////////////////////////////////////////////////////////////////
//          Parse-tree Signal (Net) Creation/Deletion Routines                           //
///////////////////////////////////////////////////////////////////////////////////////////

VeriIdDef *
VeriModule::AddSignal(const char *signal_name, VeriDataType *subtype_indication, VeriExpression *init_assign)
{
    if (!signal_name || !subtype_indication || !_scope) return 0 ; // Impossible to add signal

    // Check if there is an existing identifier by that name
    VeriIdDef *new_net = _scope->FindLocal(signal_name) ;
    if (new_net) {
        // Legal ito re-declare a variable (or net) in some cases (port declarations).
        // So let this pass through, and error out only once we know if there is a real violation.
        if (!new_net->IsVar()) {
            // If its not a variable, error out
            new_net->Error("%s is already declared", new_net->Name()) ;
            // VIPER #7773: No need to print info message, this is the location of the actual declaration.
        }
        // VIPER 1578 - Clear implicit net setting (if set)
        new_net->ClearImplicitNet() ;
    } else {
        // Create net identifier using 'signal_name'
        new_net = new VeriVariable(Strings::save(signal_name)) ;

        // Add created net identifier to module scope
        if (!_scope->Declare(new_net)) { // 'signal_name' already in _scope
            delete new_net ; // Delete created net identifier
            return 0 ;       // Failed to add, return 0
        }
    }

    // Add initial value if any to created net identifier
    if (init_assign) new_net->SetInitialValue(init_assign) ;

    // Create an array to insert created net identifier
    Array *net_ids = new Array(1) ;
    net_ids->InsertLast(new_net) ;

    // Create net declaration
    VeriNetDecl *net_decl = new VeriNetDecl(subtype_indication, 0, 0, net_ids) ;

    // Created net declaration is to be added to the module item list after last data decl.
    unsigned i ;
    VeriModuleItem *item ;
    unsigned last_net_decl_pos = 0 ; // Store the position of last declared variable in item list
    FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module item list
        if (!item || !item->IsDataDecl()) continue ; // Continue for items other than variable decl
        last_net_decl_pos = i + 1 ; // Update the position of declared variable,
                                    // Increment to distinguish from data decl at '0' or no decl
    }
    if (!_module_items) _module_items = new Array(1) ; // Create module item list if not exists
    if (!last_net_decl_pos) { // Module item list has no net declaration
        // VIPER #3466: No signal declaration found, insert at the beginning:
        _module_items->InsertFirst(net_decl) ; // Insert as first item
    } else { // Module item list has net declaration
        _module_items->InsertAfter(last_net_decl_pos - 1, net_decl) ; // Add after last declared net position
    }

    // Type inference created net declaration
    net_decl->Resolve(_scope, VERI_UNDEF_ENV) ;

    return new_net ; // Addition completed, return created net identifier
}

unsigned
VeriModule::RemoveSignal(const char *signal_name)
{
    if (!signal_name) return 0 ; // Nothing to remove

    VeriIdDef *net_id = FindDeclared(signal_name) ;

    if (!net_id || !net_id->IsNet()) return 0 ; // No net named 'signal_name' exists

    // Find the declaration of 'signal_name' in module item list
    unsigned i, j = 0 ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module items
        Array *ids = (item && item->IsNetDecl()) ? item->GetIds() : 0 ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over net identifiers in net decl
            if (id != net_id) continue ; // Not matched
            // Found desired id
            if (ids->Size() == 1) { // Found net declaration contains one net id
                _module_items->Remove(i) ; // Remove net declaration from module item list
                delete item ; // Delete net declaration
            } else { // Found net declaration contains mulltiple net ids
                ids->Remove(j) ; // Remove ''net_id' from id list of net declaration
            }

            // Remove net identifier from scope
            if (_scope) (void) _scope->Undeclare(net_id) ;
            delete net_id ; // Delete net identifier
            return 1 ; // Removing completed, return 1
        }
    }
    return 0 ; // Failed to remove
}

///////////////////////////////////////////////////////////////////////////////////////////
//            Parse-tree Instance Creation/Deletion Routines                             //
///////////////////////////////////////////////////////////////////////////////////////////

VeriModuleInstantiation *
VeriModule::AddInstance(const char *instance_name, const char *instantiated_component_name)
{
    if (!instance_name || !instantiated_component_name || !_scope) return 0 ; // Impossible to add
    // Create instance identifier using 'instance_name'
    VeriInstId *instance_id = new VeriInstId(Strings::save(instance_name), 0, 0/* no connection list*/) ;

    // Add created instance identifier to module scope
    if (!_scope->Declare(instance_id)) { // Identifier already exists
        delete instance_id ; // Delete created instance id
        return 0 ; // Failed to add
    }

    // Convert 'instantiated_component_name' into a VeriName
    VeriIdRef *instantiated_module = new VeriIdRef(Strings::save(instantiated_component_name)) ;

    // Create module instantiation
    VeriModuleInstantiation *new_instance = new VeriModuleInstantiation(instantiated_module, 0, 0, instance_id, 0) ;

    // Insert created instantiation into module item list
    if (!_module_items) _module_items = new Array(1) ; // Create module item list if not exists
    _module_items->InsertLast(new_instance) ; // Add created instance as last item

    // Type inference created module instantiation
    new_instance->Resolve(_scope, VERI_UNDEF_ENV) ;

    return new_instance ; // Return created instantiation
}

unsigned
VeriModule::RemoveInstance(const char *instance_name)
{
    if (!instance_name) return 0 ; // Nothing to remove

    // Get the instance identifier
    VeriIdDef *instance_id = FindDeclared(instance_name) ;

    if (!instance_id || !instance_id->IsInst()) return 0 ; // No instance named 'instance_name' exists
    // Find module instantiation having instance name 'instance_name' from module item list
    unsigned i, j = 0 ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module items
        Array *ids = (item && (item->IsInstantiation() || item->GetInstType())) ? item->GetIds() : 0 ; // Get id list of each instantiation
        VeriInstId *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over instance identifiers
            if (id != instance_id) continue ; // Not matched
            // Found desired id
            if (ids->Size() == 1) { // Instantiation contains one instance
                _module_items->Remove(i) ; // Remove instantiation from module item list
                delete item ; // Delete module instantiation
            } else { // Instantiation contains multiple instances
                ids->Remove(j) ; // Remove instance id from instance id list of instantiation
            }

            // Remove 'instance_id' from module scope
            if (_scope) (void) _scope->Undeclare(instance_id) ;

            // Delete 'instance_id'
            delete instance_id ;
            return 1 ; // Removing completed, return 1
        }
    }
    return 0 ; // Failed to remove
}

///////////////////////////////////////////////////////////////////////////////////////////
//                   Parse-tree Instance connection Routines                             //
///////////////////////////////////////////////////////////////////////////////////////////

unsigned
VeriInstId::AddPortRef(const char *formal_port_name, VeriExpression *actual, VeriScope *module_scope)
{
    if (!module_scope) return 0 ;  // Cannot resolve to be crated parse-tree, return 0

    unsigned named_assoc = 0 ; // VIPER #6872: Start with positional port connection
    if (formal_port_name) { // Connection for 'formal_port_name' is to be created
        // Check if existing port connection list already contains actual for 'formal_port_name'
        unsigned i ;
        VeriExpression *assoc ;
        FOREACH_ARRAY_ITEM(&_port_connects, i, assoc) { // Iterate over connection list
            const char *formal = assoc ? assoc->NamedFormal(): 0 ; // Get formal name
            if (!formal) continue ; // Not named association
            named_assoc = 1 ; // VIPER #6872: Named port connection
            if (Strings::compare(formal, formal_port_name)) { // Actual exists for 'formal_port_name'
                if (assoc) assoc->Error("port %s is already connected", formal_port_name) ; // Issue error
                return 0 ; // Failed to add port connection
            }
        }
    }
    // Create new port-ref
    // VIPER #6872: Create named port connection if existing connection are also named style:
    VeriExpression *new_port_ref = (formal_port_name && (named_assoc || !_port_connects.Size())) ? new VeriPortConnect(Strings::save(formal_port_name), actual) : actual ;

    // Insert new port-ref into port connection list
    // if (!_port_connects) _port_connects = new Array(1) ; // Create port connects if not exists
    _port_connects.InsertLast(new_port_ref) ;

    // Type inference created port_ref
    new_port_ref->Resolve(module_scope, VERI_PORT_CONNECTION) ;

    return 1 ; // Addition completed
}

unsigned
VeriModule::AddPortRef(const char *instance_name, const char *formal_port_name, VeriExpression *actual)
{
    if (!instance_name || !_scope) return 0 ; // Impossible to add port-ref

    // Find instance id from module scope
    VeriIdDef *instance_id = FindDeclared(instance_name) ;

    if (!instance_id || !instance_id->IsInst()) return 0 ; // No instance named 'instance_name' exists
    // Add actual to port connection list of instance identifier
    return instance_id->AddPortRef(formal_port_name, actual, _scope) ;
}
// VIPER #6129 : Implement AddParamRef
unsigned
VeriModule::AddParamRef(const char *instance_name, const char *formal_param_name, VeriExpression *actual)
{
    if (!instance_name || !_scope) return 0 ; // Impossible to add port-ref

    // Find instance id from module scope
    VeriIdDef *instance_id = FindDeclared(instance_name) ;

    if (!instance_id || !instance_id->IsInst()) return 0 ; // No instance named 'instance_name' exists

    VeriModuleInstantiation *instance = instance_id->GetModuleInstance() ;
    // Add actual to param connection list of instantiation
    return instance ? instance->AddParamRef(formal_param_name, actual, _scope): 0 ;
}
unsigned
VeriModuleInstantiation::AddParamRef(const char *formal_param_name, VeriExpression *actual, VeriScope *module_scope)
{
    if (!module_scope) return 0 ;  // Cannot resolve to be crated parse-tree, return 0

    unsigned named_assoc = 0 ; // VIPER #6872: Start with positional param connection
    if (formal_param_name) { // Connection for 'formal_param_name' is to be created
        // Check if existing parameter connection list already contains actual for 'formal_param_name'
        unsigned i ;
        VeriExpression *assoc ;
        FOREACH_ARRAY_ITEM(_param_values, i, assoc) { // Iterate over connection list
            const char *formal = assoc ? assoc->NamedFormal(): 0 ; // Get formal name
            if (!formal) continue ; // Not named association
            named_assoc = 1 ; // VIPER #6872: Named param connection
            if (Strings::compare(formal, formal_param_name)) { // Actual exists for 'formal_param_name'
                if (assoc) assoc->Error("parameter %s is already connected", formal_param_name) ; // Issue error
                return 0 ; // Failed to add parameter connection
            }
        }
    }
    // Create new port-ref
    // VIPER #6872: Create named param connection if existing connections are also named style:
    VeriExpression *new_param_ref = (formal_param_name && (named_assoc || !_param_values || !_param_values->Size())) ? new VeriPortConnect(Strings::save(formal_param_name), actual) : actual ;

    // Insert new param-ref into parameter connection list
    if (!_param_values) _param_values = new Array(1) ; // Create param connects if not exists
    _param_values->InsertLast(new_param_ref) ;

    // Type inference created param_ref
    new_param_ref->Resolve(module_scope, VERI_PARAM_VALUE) ;

    return 1 ; // Addition completed
}

unsigned
VeriModule::RemovePortRef(const char *instance_name, const char *formal_port_name)
{
    if (!instance_name) return 0 ; // Impossible to remove, no instance name given

    // Find instance id named 'instance_name'
    VeriIdDef *instance_id = FindDeclared(instance_name) ;
    if (!instance_id) return 0 ;

    // Get module instantiation statement from instance identifier
    VeriModuleInstantiation *instantiation = instance_id->GetModuleInstance() ;

    if (!instantiation) return 0 ; // No instance named 'instance_name' exists

    // Get connection list of instantiation
    Array *port_connects = instance_id->GetPortConnects() ;

    if (!port_connects) return 0 ; // No port connection list, failed to remove

    if (formal_port_name) { // Formal port name given
        unsigned named_port_connection = 0 ;
        // Find actual for 'formal_port_name' in port connection list
        unsigned i ;
        VeriExpression *port_connect ;
        FOREACH_ARRAY_ITEM(port_connects, i, port_connect) { // Iterate over port connection list
            // Get formal name from named association
            const char *formal = port_connect ? port_connect->NamedFormal(): 0 ;
            if (!formal) continue ; // Not named association, continue
            named_port_connection = 1 ;
            if (Strings::compare(formal, formal_port_name)) { // Found connection for 'formal_port_name'
                port_connects->Remove(i) ; // Remove port connect from port connection list
                delete port_connect ; // Delete port connect
                return 1 ; // Finished, return 1
            }
        }
        // VIPER #6170: If the port connection is named, do not try to remove positional port
        // connection since the two cannot be mixed, just return 0 to indicate failure to remove.
        if (named_port_connection) return 0 ;
        // Formal by 'formal_port_name' is not in named association
        // Find the position of port 'formal_port_name' from instantiated module's port list

        // Get instantiated module
        VeriModule *instantiated_module = instantiation->GetInstantiatedModule() ;
        // Get port identifiers list from instantiated module
        Array *port_ids = instantiated_module ? instantiated_module->GetPorts(): 0 ;

        VeriIdDef *port_id ;
        FOREACH_ARRAY_ITEM(port_ids, i, port_id) { // Iterate over port identifier list
            if (!port_id) continue ;
            if (Strings::compare(port_id->Name(), formal_port_name)) { // Found desired formal
                // 'i' is the position of formal 'formal_port_name' in port list
                // Remove 'i' th element form positional port connection list
                if (port_connects->Size() > i) { // Check if actual exists for 'i' th pos
                    port_connect = (VeriExpression*)port_connects->At(i) ; // Get 'i' th actual
                    port_connects->Remove(i) ; // Remove port connect from port connection list
                    delete port_connect ; // Delete port connect
                    return 1 ; // Finished, return 1
                }
                break ;
            }
        }
    } else { // Formal name not given, remove last port connect
        // Remove last port connect from port connection list
        VeriExpression *last_assoc = (VeriExpression*)port_connects->RemoveLast() ;
        delete last_assoc ; // Delete last port connect
        return 1 ; // Finished, return 1
    }
    return 0 ; // Failed to remove
}

unsigned
VeriModule::RemoveParamRef(const char *instance_name, const char *formal_param_name)
{
    if (!instance_name) return 0 ; // Impossible to remove, no instance name given

    // Find instance id named 'instance_name'
    VeriIdDef *instance_id = FindDeclared(instance_name) ;
    if (!instance_id) return 0 ;

    // Get module instantiation statement from instance identifier
    VeriModuleInstantiation *instantiation = instance_id->GetModuleInstance() ;

    if (!instantiation) return 0 ; // No instance named 'instance_name' exists

    // Get connection list of instantiation
    Array *param_connects = instantiation->GetParamValues() ;

    if (!param_connects) return 0 ; // No param connection list, failed to remove

    if (formal_param_name) { // Formal param name given
        // Find actual for 'formal_param_name' in param connection list
        unsigned i ;
        VeriExpression *param_connect ;
        FOREACH_ARRAY_ITEM(param_connects, i, param_connect) { // Iterate over param connection list
            // Get formal name from named association
            const char *formal = param_connect ? param_connect->NamedFormal(): 0 ;
            if (!formal) continue ; // Not named association, continue
            if (Strings::compare(formal, formal_param_name)) { // Found connection for 'formal_param_name'
                param_connects->Remove(i) ; // Remove param connect from param connection list
                delete param_connect ; // Delete param connect
                return 1 ; // Finished, return 1
            }
        }
        // Formal by 'formal_param_name' is not in named association
        // Find the position of param 'formal_param_name' from instantiated module's param list

        // Get instantiated module
        VeriModule *instantiated_module = instantiation->GetInstantiatedModule() ;
        // Get param identifiers list from instantiated module
        Array *param_ids = instantiated_module ? instantiated_module->GetParameters(): 0 ;

        VeriIdDef *param_id ;
        FOREACH_ARRAY_ITEM(param_ids, i, param_id) { // Iterate over param identifier list
            if (!param_id) continue ;
            if (Strings::compare(param_id->Name(), formal_param_name)) { // Found desired formal
                // 'i' is the position of formal 'formal_param_name' in param list
                // Remove 'i' th element form positional param connection list
                if (param_connects->Size() > i) { // Check if actual exists for 'i' th pos
                    param_connect = (VeriExpression*)param_connects->At(i) ; // Get 'i' th actual
                    param_connects->Remove(i) ; // Remove param connect from param connection list
                    delete param_connect ; // Delete param connect
                    return 1 ; // Finished, return 1
                }
                break ;
            }
        }
    } else { // Formal name not given, remove last param connect
        // Remove last param connect from param connection list
        VeriExpression *last_assoc = (VeriExpression*)param_connects->RemoveLast() ;
        delete last_assoc ; // Delete last port connect
        return 1 ; // Finished, return 1
    }
    return 0 ; // Failed to remove
}

