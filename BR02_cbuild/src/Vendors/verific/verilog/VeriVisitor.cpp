/*
 *
 * [ File Version : 1.111 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriVisitor.h"

#include "VeriTreeNode.h"
#include "VeriModuleItem.h"
#include "VeriModule.h"
#include "VeriMisc.h"
#include "VeriStatement.h"
#include "VeriExpression.h"
#include "VeriConstVal.h"
#include "VeriId.h"
#include "VeriLibrary.h"

#include "Map.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

///////////////////////////////////////////////////////////

void VeriVisitor::TraverseNode(VeriTreeNode *node)
{
    if (node) node->Accept(*this) ;
}

void VeriVisitor::TraverseAttributes(const Map *attr)
{
    if (!attr) return ;

    MapIter mi ;
    VeriExpression *expr = 0 ;
    FOREACH_MAP_ITEM(attr, mi, 0, &expr) {
        if (expr) expr->Accept(*this) ;
    }
}

void VeriVisitor::TraverseArray(const Array *array)
{
    if (!array) return ;

    VeriTreeNode * node_item = 0 ;
    unsigned int i = 0 ;

    FOREACH_ARRAY_ITEM(array, i, node_item) {
        if (node_item) node_item->Accept(*this) ;
    }
}

///////////////////////////////////////////////////////////

// Traverse the module
void VeriVisitor::VERI_VISIT(VeriModule, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse module identifier
    TraverseNode(node.Id()) ;
    // Traverse package import decls
    TraverseArray(node.GetPackageImportDecls()) ;
    // Traverse parameter port list
    TraverseArray(node.GetParameterConnects()) ;

    // Traverse ports
    TraverseArray(node.GetPortConnects()) ;

    // Traverse module items
    TraverseArray(node.GetModuleItems()) ;
}

// Traverse configuration declaration
void VeriVisitor::VERI_VISIT(VeriConfiguration, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModule, node) ;

    // Traverse 'design' array (array of top modules VeriName *)
    TraverseArray(node.GetTopModuleNames()) ;

    // Traverse config rules (array of VeriConfigRule *)
    TraverseArray(node.GetConfigRules()) ;
}

// Traverse primitive
void VeriVisitor::VERI_VISIT(VeriPrimitive, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModule, node) ;
}

//////////////////// Expressions ///////////////////////////

// Traverse name reference
void VeriVisitor::VERI_VISIT(VeriIdRef, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriName, node) ;
}

// Traverse bit select expression for vector
void VeriVisitor::VERI_VISIT(VeriIndexedId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriName, node) ;

    // Traverse the prefix :
    TraverseNode(node.GetPrefix()) ;

    // Traverse select expression
    TraverseNode(node.GetIndexExpr()) ;
}

// Traverse selected name
void VeriVisitor::VERI_VISIT(VeriSelectedName, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriName, node) ;

    // Traverse the prefix :
    TraverseNode(node.GetPrefix()) ;

    // suffix is a char*. No traversal needed.
}

// Traverse bit-select expression
void VeriVisitor::VERI_VISIT(VeriIndexedMemoryId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriName, node) ;

    // Traverse the prefix :
    TraverseNode(node.GetPrefix()) ;

    // Traverse indexes
    TraverseArray(node.GetIndexes()) ;
}

// Traverse concatenation
void VeriVisitor::VERI_VISIT(VeriConcat, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse each element of concatenation
    TraverseArray(node.GetExpressions()) ;
}

// Traverse multi concat expression
void VeriVisitor::VERI_VISIT(VeriMultiConcat, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the repetation expression
    TraverseNode(node.GetRepeat()) ;

    // Traverse the elements of concatenation
    TraverseArray(node.GetExpressions()) ;
}

// Traverse function call
void VeriVisitor::VERI_VISIT(VeriFunctionCall, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriName, node) ;

    // Traverse function name
    TraverseNode(node.GetFunctionName()) ;

    // Traverse argument list
    TraverseArray(node.GetArgs()) ;
}

// Traverse system function call
void VeriVisitor::VERI_VISIT(VeriSystemFunctionCall, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse argument list
    TraverseArray(node.GetArgs()) ;
}

// Traverse min-typ-max expression
void VeriVisitor::VERI_VISIT(VeriMinTypMaxExpr, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the minimum expression
    TraverseNode(node.GetMinExpr()) ;

    // Traverse the typical expression
    TraverseNode(node.GetTypExpr()) ;

    // Traverse the maximum expression
    TraverseNode(node.GetMaxExpr()) ;
}

// Traverse unary operator
void VeriVisitor::VERI_VISIT(VeriUnaryOperator, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the operand
    TraverseNode(node.GetArg()) ;
}

// Traverse binary operator
void VeriVisitor::VERI_VISIT(VeriBinaryOperator, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse left operand
    TraverseNode(node.GetLeft()) ;

    // Traverse right operand
    TraverseNode(node.GetRight()) ;
}

// Traverse question colon expression
void VeriVisitor::VERI_VISIT(VeriQuestionColon, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the condition
    TraverseNode(node.GetIfExpr()) ;

    // Traverse the 1 expression
    TraverseNode(node.GetThenExpr()) ;

    // Traverse the false expression
    TraverseNode(node.GetElseExpr()) ;
}

// Traverse the event expression
void VeriVisitor::VERI_VISIT(VeriEventExpression, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the expression
    TraverseNode(node.GetExpr()) ;

    TraverseNode(node.GetIffCondition()) ;
}

// Traverse the named association of a (named) port association
void VeriVisitor::VERI_VISIT(VeriPortConnect, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the connection
    TraverseNode(node.GetConnection()) ;
}

// Traverse a open port expression
void VeriVisitor::VERI_VISIT(VeriPortOpen, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;
}

// Traverse ansi port declaration
void VeriVisitor::VERI_VISIT(VeriAnsiPortDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the data type
    TraverseNode(node.GetDataType()) ;

    // Traverse port identifiers
    TraverseArray(node.GetIds()) ;
}

// Traverse timing check event
void VeriVisitor::VERI_VISIT(VeriTimingCheckEvent, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse terminal description
    TraverseNode(node.GetTerminalDesc()) ;

    // Traverse condition
    TraverseNode(node.GetCondition()) ;
}

// Traverse the range
void VeriVisitor::VERI_VISIT(VeriRange, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the msb expression
    TraverseNode(node.GetLeft()) ;

    // Traverse the lsb expression
    TraverseNode(node.GetRight()) ;

    // Traverse the next range (dimension)
    TraverseNode(node.GetNext()) ;
}

// Traverse a data type :
void VeriVisitor::VERI_VISIT(VeriDataType, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the dimensions (range(s)).
    TraverseNode(node.GetDimensions()) ;
}
// VIPER #4896 : Traverse net data type
void VeriVisitor::VERI_VISIT(VeriNetDataType, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriDataType, node) ;

    // Traverse the data type associated with the node
    TraverseNode(node.GetDataType()) ;
}

// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
// Traverse a Pathpulse value:
void VeriVisitor::VERI_VISIT(VeriPathPulseVal, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the reject limit expression
    TraverseNode(node.GetRejectLimit()) ;
    // Traverse the error limit expression
    TraverseNode(node.GetErrorLimit()) ;
}
//////////////////////     Identifiers          /////////////////////////////

// Traverse variable identifier
void VeriVisitor::VERI_VISIT(VeriVariable, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Traverse the initial value
    TraverseNode(node.GetInitialValue()) ;

    // Traverse the dimensions
    TraverseNode(node.GetDimensions()) ;
}

// Traverse instance identifier
void VeriVisitor::VERI_VISIT(VeriInstId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Traverse the dimensions
    TraverseNode(node.GetDimensions()) ;

    // Traverse port connection list :
    TraverseArray(node.GetPortConnects()) ;
}

// Traverse module identifier
void VeriVisitor::VERI_VISIT(VeriModuleId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;
}

// Traverse user defined primitive identifier
void VeriVisitor::VERI_VISIT(VeriUdpId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleId, node) ;
}

// Traverse configuration identifier
void VeriVisitor::VERI_VISIT(VeriConfigurationId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleId, node) ;
}

// Traverse task identifier
void VeriVisitor::VERI_VISIT(VeriTaskId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;
}

// Traverse function identifier
void VeriVisitor::VERI_VISIT(VeriFunctionId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriVariable, node) ;
}

// Traverse genvar identifier
void VeriVisitor::VERI_VISIT(VeriGenVarId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;
}

// Traverse parameter identifier
void VeriVisitor::VERI_VISIT(VeriParamId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Traverse the initial value
    TraverseNode(node.GetInitialValue()) ;
}

// Traverse the block identifier
void VeriVisitor::VERI_VISIT(VeriBlockId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;
}

// Traverse the strength
void VeriVisitor::VERI_VISIT(VeriStrength, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;
}

// Traverse net reg assigment
void VeriVisitor::VERI_VISIT(VeriNetRegAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the left hand side expression
    TraverseNode(node.GetLValExpr()) ;

    // Trverse the right hand size expression
    TraverseNode(node.GetRValExpr()) ;
}

// Traverse net reg assigment
void VeriVisitor::VERI_VISIT(VeriDefParamAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the left hand side expression
    TraverseNode(node.GetLVal()) ;

    // Trverse the right hand size expression
    TraverseNode(node.GetRVal()) ;
}

// Traverse the case item
void VeriVisitor::VERI_VISIT(VeriCaseItem, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the conditions
    TraverseArray(node.GetConditions()) ;

    // Traverse the statement for case item
    TraverseNode(node.GetStmt()) ;
}

// Traverse the case item
void VeriVisitor::VERI_VISIT(VeriCaseOperatorItem, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the conditions
    TraverseArray(node.GetConditions()) ;

    // Traverse the statement for case item
    TraverseNode(node.GetPropertyExpr()) ;
}

// Traverse the generate case item
void VeriVisitor::VERI_VISIT(VeriGenerateCaseItem, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the conditions
    TraverseArray(node.GetConditions()) ;

    // Traverse the module item for the case item
    TraverseNode(node.GetItem()) ;
}

// Traverse the path
void VeriVisitor::VERI_VISIT(VeriPath, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse input terminals
    TraverseArray(node.GetInTerminals()) ;

    // Traverse output terminals
    TraverseArray(node.GetOutTerminals()) ;

    // Traverse data source
    TraverseNode(node.GetDataSource()) ;
}

// Traverse delay of event control
void VeriVisitor::VERI_VISIT(VeriDelayOrEventControl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the delay control
    TraverseNode(node.GetDelayControl()) ;

    // Traverse repeat expression
    TraverseNode(node.GetRepeatEvent()) ;

    // Traverse event control
    TraverseArray(node.GetEventControl()) ;
}

// Traverse config rule abstract class
void VeriVisitor::VERI_VISIT(VeriConfigRule, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;
}

// Traverse the instance configuration
void VeriVisitor::VERI_VISIT(VeriInstanceConfig, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConfigRule, node) ;

    // Traverse the instance name
    TraverseNode(node.GetInstanceName()) ;

    // Traverse the use clause
    TraverseNode(node.GetUseClause()) ;
}

// Traverse the cell configuration
void VeriVisitor::VERI_VISIT(VeriCellConfig, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConfigRule, node) ;

    // Traverse the cell name
    TraverseNode(node.GetCellName()) ;

    // Traverse the use clause
    TraverseNode(node.GetUseClause()) ;
}

// Traverse the default library list
void VeriVisitor::VERI_VISIT(VeriDefaultConfig, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConfigRule, node) ;
}

// Traverse the use clause
void VeriVisitor::VERI_VISIT(VeriUseClause, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the cell ref name
    TraverseNode(node.GetCellName()) ;

    // VIPER #7416: Traverse parameter value assignment list
    TraverseArray(node.GetParamValues()) ;
}
#ifdef VERILOG_PATHPULSE_PORTS
// Traverse the path pulse ports
void VeriVisitor::VERI_VISIT(VeriPathPulseValPorts, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the terminals
    TraverseNode(node.GetInputPort()) ;
    TraverseNode(node.GetOutputPort()) ;
}
#endif
//////////////////         Module items       ////////////////////////

// Traverse any data declaration
void VeriVisitor::VERI_VISIT(VeriDataDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the data type
    TraverseNode(node.GetDataType()) ;

    // Traverse identifier list
    TraverseArray(node.GetIds()) ;
}

// Traverse net declaration
void VeriVisitor::VERI_VISIT(VeriNetDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriDataDecl, node) ;

    // Traverse the strength
    TraverseNode(node.GetStrength()) ;

    // Traverse the delay
    TraverseArray(node.GetDelay()) ;
}

// Traverse function declaration
void VeriVisitor::VERI_VISIT(VeriFunctionDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse function identifier
    TraverseNode(node.GetFunctionId()) ;

    // Traverse return range
    TraverseNode(node.GetDataType()) ;

    // Traverse port declaration
    TraverseArray(node.GetAnsiIOList()) ;

    // Traverse function items declaration
    TraverseArray(node.GetDeclarations()) ;

    // Traverse function statement
    TraverseArray(node.GetStatements()) ;
}

// Traverse task declaration
void VeriVisitor::VERI_VISIT(VeriTaskDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse task identifier
    TraverseNode(node.GetTaskId()) ;

    // Traverse port list
    TraverseArray(node.GetAnsiIOList()) ;

    // Traverse task item declarations
    TraverseArray(node.GetDeclarations()) ;

    // Traverse task statement
    TraverseArray(node.GetStatements()) ;
}

// Traverse defparam
void VeriVisitor::VERI_VISIT(VeriDefParam, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse defparam assignments :
    TraverseArray(node.GetDefParamAssigns()) ;
}

// Traverse continuous assignment
void VeriVisitor::VERI_VISIT(VeriContinuousAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the strength
    TraverseNode(node.GetStrength()) ;

    // Traverse the delay
    TraverseArray(node.GetDelay()) ;

    // Traverse net assignment list
    TraverseArray(node.GetNetAssigns()) ;
}

// Traverse gate instantiation
void VeriVisitor::VERI_VISIT(VeriGateInstantiation, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the strength
    TraverseNode(node.GetStrength()) ;

    // Traverse the delay
    TraverseArray(node.GetDelay()) ;

    // Traverse the instance list
    TraverseArray(node.GetInstances()) ;
}

// Traverse module instantiation
void VeriVisitor::VERI_VISIT(VeriModuleInstantiation, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the strength
    TraverseNode(node.GetStrength()) ;

    // Traverse parameter value assignment list
    TraverseArray(node.GetParamValues()) ;

    // Traverse instance list
    TraverseArray(node.GetInstances()) ;
}

// Traverse specify block
void VeriVisitor::VERI_VISIT(VeriSpecifyBlock, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse specify items
    TraverseArray(node.GetSpecifyItems()) ;
}

// Traverse path declaration
void VeriVisitor::VERI_VISIT(VeriPathDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the condition
    TraverseNode(node.GetCondition()) ;

    // Traverse the path
    TraverseNode(node.GetPath()) ;

    // Traverse the delay
    TraverseArray(node.GetDelay()) ;
}

// Traverse system timing check
void VeriVisitor::VERI_VISIT(VeriSystemTimingCheck, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse argument list
    TraverseArray(node.GetArgs()) ;
}

// Traverse initial construct
void VeriVisitor::VERI_VISIT(VeriInitialConstruct, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the statement
    TraverseNode(node.GetStmt()) ;
}

// Traverse always construct
void VeriVisitor::VERI_VISIT(VeriAlwaysConstruct, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the statement
    TraverseNode(node.GetStmt()) ;
}

// Traverse generate construct
void VeriVisitor::VERI_VISIT(VeriGenerateConstruct, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse generate items
    TraverseArray(node.GetItems()) ;
}

// Traverse generate conditional statement
void VeriVisitor::VERI_VISIT(VeriGenerateConditional, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the condition
    TraverseNode(node.GetIfExpr()) ;

    // Traverse the module item that will be executed when the condition is true
    TraverseNode(node.GetThenItem()) ;

    // Traverse the module item that will be executed when the condition is false
    TraverseNode(node.GetElseItem()) ;
}

// Traverse generate case statement
void VeriVisitor::VERI_VISIT(VeriGenerateCase, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the case condition
    TraverseNode(node.GetCondition()) ;

    // Traverse case items
    TraverseArray(node.GetCaseItems()) ;
}

// Traverse generate for statement
void VeriVisitor::VERI_VISIT(VeriGenerateFor, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the initial assignment
    TraverseNode(node.GetInitial()) ;

    // Traverse the final condition
    TraverseNode(node.GetCondition()) ;

    // Traverse step assignment
    TraverseNode(node.GetRepetition()) ;

    // Traverse generate block identifier
    TraverseNode(node.GetBlockId()) ;

    // Traverse block items
    TraverseArray(node.GetItems()) ;
}

// Traverse generate block
void VeriVisitor::VERI_VISIT(VeriGenerateBlock, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse block identifier
    TraverseNode(node.GetBlockId()) ;

    // Traverse block items
    TraverseArray(node.GetItems()) ;
}

// Traverse table
void VeriVisitor::VERI_VISIT(VeriTable, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;
}
// Traverse the expressions
void VeriVisitor::VERI_VISIT(VeriPulseControl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the expressions
    TraverseArray(node.GetPathList()) ;
}

//////////////////////////    Statements    ///////////////////////////////////

// Traverse null statement
void VeriVisitor::VERI_VISIT(VeriNullStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;
}

// Traverse blocking assignment
void VeriVisitor::VERI_VISIT(VeriBlockingAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse left hand side of assignment
    TraverseNode(node.GetLVal()) ;

    // Traverse delay or event control
    TraverseNode(node.GetControl()) ;

    // Traverse the value of assignment
    TraverseNode(node.GetValue()) ;
}

// Traverse non blocking assignment
void VeriVisitor::VERI_VISIT(VeriNonBlockingAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse left hand side of assignment
    TraverseNode(node.GetLVal()) ;

    // Traverse delay or event control
    TraverseNode(node.GetControl()) ;

    // Traverse the value
    TraverseNode(node.GetValue()) ;
}

// Traverse genvar assignment
void VeriVisitor::VERI_VISIT(VeriGenVarAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse genvar identifier
    TraverseNode(node.GetId()) ;

    // Traverse right hand side of assignment
    TraverseNode(node.GetValue()) ;
}

// Traverse assignment
void VeriVisitor::VERI_VISIT(VeriAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse net or register assignment
    TraverseNode(node.GetAssign()) ;
}

// Traverse deassign
void VeriVisitor::VERI_VISIT(VeriDeAssign, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse the lvalue
    TraverseNode(node.GetLVal()) ;
}

// Traverse force statement
void VeriVisitor::VERI_VISIT(VeriForce, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse net or register assignment
    TraverseNode(node.GetAssign()) ;
}

// Traverse release statement
void VeriVisitor::VERI_VISIT(VeriRelease, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse lvalue
    TraverseNode(node.GetLVal()) ;
}

// Traverse task call
void VeriVisitor::VERI_VISIT(VeriTaskEnable, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse task identifier
    TraverseNode(node.GetTaskName()) ;

    // Traverse argument list
    TraverseArray(node.GetArgs()) ;
}

// Traverse system task call
void VeriVisitor::VERI_VISIT(VeriSystemTaskEnable, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse argument list
    TraverseArray(node.GetArgs()) ;
}

// Traverse delay control statement
void VeriVisitor::VERI_VISIT(VeriDelayControlStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse the delay expression
    TraverseNode(node.GetDelay()) ;

    // Traverse the statement
    TraverseNode(node.GetStmt()) ;
}

// Traverse event control statement
void VeriVisitor::VERI_VISIT(VeriEventControlStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse event expressions
    TraverseArray(node.GetAt()) ;

    // Traverse statement
    TraverseNode(node.GetStmt()) ;
}

// Traverse conditional statement
void VeriVisitor::VERI_VISIT(VeriConditionalStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse the condition
    TraverseNode(node.GetIfExpr()) ;

    // Traverse then statement
    TraverseNode(node.GetThenStmt()) ;

    // Traverse else statement
    TraverseNode(node.GetElseStmt()) ;
}

// Traverse case statement
void VeriVisitor::VERI_VISIT(VeriCaseStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse case condition
    TraverseNode(node.GetCondition()) ;

    // Traverse case items
    TraverseArray(node.GetCaseItems()) ;
}

// Traverse forever statement
void VeriVisitor::VERI_VISIT(VeriForever, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriLoop, node) ;
}

// Traverse repeat statement
void VeriVisitor::VERI_VISIT(VeriRepeat, node)
{
    // Traverse repeat condition
    TraverseNode(node.GetCondition()) ;

    // Call of Base class Visit
    // The ordering is not manitained due to syntax
    VERI_VISIT_NODE(VeriLoop, node) ;
}

// Traverse while statement
void VeriVisitor::VERI_VISIT(VeriWhile, node)
{
    // Traverse the condition
    TraverseNode(node.GetCondition()) ;

    // Call of Base class Visit
    // The ordering is not manitained due to syntax
    VERI_VISIT_NODE(VeriLoop, node) ;
}

// Traverse for statement
void VeriVisitor::VERI_VISIT(VeriFor, node)
{
    // Traverse initial assignment
    TraverseArray(node.GetInitials()) ;

    // Traverse final condition
    TraverseNode(node.GetCondition()) ;

    // Traverse step assignment
    TraverseArray(node.GetRepetitions()) ;

    // Call of Base class Visit
    // The ordering is not manitained due to syntax
    VERI_VISIT_NODE(VeriLoop, node) ;
}

// Traverse the wait statement
void VeriVisitor::VERI_VISIT(VeriWait, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse the condition
    TraverseNode(node.GetCondition()) ;

    // Traverse the statement
    TraverseNode(node.GetStmt()) ;
}

// Traverse disable statement
void VeriVisitor::VERI_VISIT(VeriDisable, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse task/block name
    TraverseNode(node.GetTaskBlockName()) ;
}

// Traverse event trigger statement
void VeriVisitor::VERI_VISIT(VeriEventTrigger, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse event name
    TraverseNode(node.GetEventName()) ;
}

// Traverse sequential block
void VeriVisitor::VERI_VISIT(VeriSeqBlock, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse block id and block item declarations
    TraverseNode(node.GetLabel()) ;

    // Traverse block declarations
    TraverseArray(node.GetDeclItems()) ;

    // Traverse block statements
    TraverseArray(node.GetStatements()) ;
}

// Traverse parallel block
void VeriVisitor::VERI_VISIT(VeriParBlock, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse block identifier and block item declarations
    TraverseNode(node.GetLabel()) ;

    // Traverse block declarations
    TraverseArray(node.GetDeclItems()) ;

    // Traverse block statements
    TraverseArray(node.GetStatements()) ;
}

/////////////////////  Constant values /////////////////////////////

// Traverse string and based number
void VeriVisitor::VERI_VISIT(VeriConstVal, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConst, node) ;
}

// Traverse integer value
void VeriVisitor::VERI_VISIT(VeriIntVal, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConst, node) ;
}

// Traverse real value
void VeriVisitor::VERI_VISIT(VeriRealVal, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConst, node) ;
}

// Traverse library declaration (library mapping)
void VeriVisitor::VERI_VISIT(VeriLibraryDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;
}

// Traverse time literal
void VeriVisitor::VERI_VISIT(VeriTimeLiteral, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConst, node) ;
}
////////////////////////////// Others ////////////////////////////////

void VeriVisitor::VERI_VISIT(VeriConst, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;
}

void VeriVisitor::VERI_VISIT(VeriName, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;
}

void VeriVisitor::VERI_VISIT(VeriExpression, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse attribute instances
    TraverseAttributes(node.GetAttributes()) ;
}

void VeriVisitor::VERI_VISIT(VeriModuleItem, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse attribute instances
    TraverseAttributes(node.GetAttributes()) ;
}

void VeriVisitor::VERI_VISIT(VeriTreeNode, node)
{
    // No Item to traverse
    (void) node ;
}

void VeriVisitor::VERI_VISIT(VeriCommentNode, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // No item to traverse
}

void VeriVisitor::VERI_VISIT(VeriIdDef, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;
}

void VeriVisitor::VERI_VISIT(VeriStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;
}

void VeriVisitor::VERI_VISIT(VeriLoop, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse the statement
    TraverseNode(node.GetStmt()) ;
}

    //////////////////////////System Verilog Class/////////////////

// Traverse the Interface
void VeriVisitor::VERI_VISIT(VeriInterface, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModule, node) ;
    // No local fields to visit
}

// Traverse the Program
void VeriVisitor::VERI_VISIT(VeriProgram, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModule, node) ;
    // No local fields to visit
}

// Traverse the Checker
void VeriVisitor::VERI_VISIT(VeriChecker, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModule, node) ;
    // No local fields to visit
}

// Tranverse the VeriPackage
void VeriVisitor::VERI_VISIT(VeriPackage, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModule, node) ;
    // No local fields to visit
}

// Traverse the class
void VeriVisitor::VERI_VISIT(VeriClass, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse Program identifier
    TraverseNode(node.GetId()) ;

    // Traverse parameter port list
    TraverseArray(node.GetParameterConnects()) ;

    // Traverse interface class types
    TraverseArray(node.GetInterfaceClasses()) ;

    // Traverse Class items
    TraverseArray(node.GetItems()) ;
}

// Traverse the VeriPropertyDecl
void VeriVisitor::VERI_VISIT(VeriPropertyDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse Identifier
    TraverseNode(node.GetId()) ;

    // Traverse port list
    TraverseArray(node.GetAnsiIOList()) ;

    // Traverse Expression items
    TraverseArray(node.GetDeclarations()) ;

    // Traverse Expression item
    TraverseNode(node.GetSpec()) ;
}

// Traverse the VeriSequenceDecl
void VeriVisitor::VERI_VISIT(VeriSequenceDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse Identifier
    TraverseNode(node.GetId()) ;

    // Traverse port list
    TraverseArray(node.GetAnsiIOList()) ;

    // Traverse Expression items
    TraverseArray(node.GetDeclarations()) ;

    // Traverse Expression item
    TraverseNode(node.GetSpec()) ;
}

// Tranverse the VeriModport
void VeriVisitor::VERI_VISIT(VeriModport, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse VeriModportDecl
    TraverseArray(node.GetModportDecls()) ;
}

// Tranverse the VeriModportDecl
void VeriVisitor::VERI_VISIT(VeriModportDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse Identifier
    TraverseNode(node.GetId()) ;

    // Traverse VeriModportDecl
    TraverseArray(node.GetModportPortDecls()) ;
}

// Tranverse the VeriClockingDecl
void VeriVisitor::VERI_VISIT(VeriClockingDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse Identifier
    TraverseNode(node.GetId()) ;

    // Traverse Expression
    TraverseNode(node.GetClockingEvent()) ;

    // Traverse clocking data declarations
    TraverseArray(node.GetClockingItems()) ;
}

// Tranverse the VeriConstraintDecl
void VeriVisitor::VERI_VISIT(VeriConstraintDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse Identifier
    TraverseNode(node.GetId()) ;

    // Traverse name
    TraverseNode(node.GetConstraintName()) ;

    // Traverse constraint blocks
    TraverseArray(node.GetConstraintBlocks()) ;
}

// Tranverse the VeriBindDirective
void VeriVisitor::VERI_VISIT(VeriBindDirective, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the target instantiation list
    TraverseArray(node.GetTargetInstanceList()) ;

    // Traverse the instantiation
    TraverseNode(node.GetInstantiation()) ;
}

// Tranverse the VeriOperatorBinding
void VeriVisitor::VERI_VISIT(VeriOperatorBinding, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse operator identifier
    TraverseNode(node.GetId()) ;

    // Traverse associated function's return type
    TraverseNode(node.GetDataType()) ;

    // Traverse associated function's name
    TraverseNode(node.GetFunctionName()) ;

    // Traverse associated function prototype's formals
    TraverseArray(node.GetFormals()) ;
}
// Traverse foreach operator
void VeriVisitor::VERI_VISIT(VeriForeachOperator, node)
{
    // Call of Base calss Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse array name
    TraverseNode(node.GetArrayName()) ;

    // Traverse loop index variables
    TraverseArray(node.GetLoopIndexes()) ;

    // Traverse constraint set
    TraverseNode(node.GetConstraintSet()) ;
}
// Traverse signal aliasing statement
void VeriVisitor::VERI_VISIT(VeriNetAlias, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse left member
    TraverseNode(node.GetLeftMember()) ;

    // Traverse other memebers
    TraverseArray(node.GetMembers()) ;
}

// Traverse timeunit declaration
void VeriVisitor::VERI_VISIT(VeriTimeUnit, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse literal
    TraverseNode(node.GetTimeLiteral()) ;

    // Traverse time precision
    TraverseNode(node.GetTimePrecision()) ;
}

// Traverse clocking signal declaration
void VeriVisitor::VERI_VISIT(VeriClockingSigDecl, node)
{
    // Traverse clocking directions
    TraverseNode(node.GetClockingDirection()) ;
    TraverseNode(node.GetOutputClockingDirection()) ;

    // Call of Base class Visit
    VERI_VISIT_NODE(VeriDataDecl, node) ;
}

// Traverse covergroup declaration
void VeriVisitor::VERI_VISIT(VeriCovergroup, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse identifier
    TraverseNode(node.GetId()) ;

    // Traverse port list
    TraverseArray(node.GetPortConnects()) ;

    // Traverse coverage event
    TraverseNode(node.GetCoverageEvent()) ;

    // Traverse sample function
    TraverseNode(node.GetSampleFunction()) ;

    // Traverse items
    TraverseArray(node.GetItems()) ;
}

// Traverse coverage option
void VeriVisitor::VERI_VISIT(VeriCoverageOption, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse option
    TraverseNode(node.GetOption()) ;

    // Traverse value
    TraverseNode(node.GetValue()) ;
}
// Traverse coverage specification
void VeriVisitor::VERI_VISIT(VeriCoverageSpec, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse coverage spec label
    TraverseNode(node.GetId()) ;

    // Traverse coverpoint list
    TraverseArray(node.GetExprList()) ;

    // Traverse iff expression
    TraverseNode(node.GetIffExpression()) ;

    // Traverse bins
    TraverseArray(node.GetBins()) ;
}
// Traverse bin declaration
void VeriVisitor::VERI_VISIT(VeriBinDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    TraverseNode(node.GetId()) ; // Traverse bin identifier
}
// Traverse package import declaration
void VeriVisitor::VERI_VISIT(VeriImportDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    TraverseArray(node.GetItems()) ; // Traverse import items
}
void VeriVisitor::VERI_VISIT(VeriLetDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse the identifier
    TraverseNode(node.GetId()) ;

    // Traverse port list
    TraverseArray(node.GetAnsiIOList()) ;

    // Traverse expression
    TraverseNode(node.GetExpression()) ;
}
void  VeriVisitor::VERI_VISIT(VeriDefaultDisableIff, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    // Traverse expression
    TraverseNode(node.GetExpression()) ;
}
// Traverse package export declaration
void VeriVisitor::VERI_VISIT(VeriExportDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleItem, node) ;

    TraverseArray(node.GetItems()) ; // Traverse export items
}
// Tranverse the VeriTypeRef
void VeriVisitor::VERI_VISIT(VeriTypeRef, node)
{
    // Traverse the type reference name :
    TraverseNode(node.GetTypeName()) ;

    // Call of Base class Visit
    // The ordering is not manitained due to syntax
    VERI_VISIT_NODE(VeriDataType, node) ;
}

// Tranverse the VeriStructUnion
void VeriVisitor::VERI_VISIT(VeriStructUnion, node)
{
    // Tranverse the VeriDataDecl
    TraverseArray(node.GetDecls()) ;

    // Call of Base class Visit
    // The ordering is not manitained due to syntax
    VERI_VISIT_NODE(VeriDataType, node) ;
}

// Tranverse the VeriEnum
void VeriVisitor::VERI_VISIT(VeriEnum, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriDataType, node) ;

    // Traverse base type
    TraverseNode(node.GetBaseType()) ;

    //Traverse the Array of VeriIdDef
    TraverseArray(node.GetIds()) ;
}

// Tranverse the VeriDotStar
void VeriVisitor::VERI_VISIT(VeriDotStar, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;
}

// Tranverse the VeriIfOperator
void VeriVisitor::VERI_VISIT(VeriIfOperator, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriQuestionColon, node) ;
}

// Tranverse the VeriIfOperator
void VeriVisitor::VERI_VISIT(VeriCaseOperator, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Tranverse the Condition
    TraverseNode(node.GetCondition()) ;

    // Tranverse the cycle delay range
    TraverseArray(node.GetCaseItems()) ;
}

// Tranverse the VeriSequenceConcat
void VeriVisitor::VERI_VISIT(VeriSequenceConcat, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriBinaryOperator, node) ;

     // Tranverse the cycle delay range
     TraverseNode(node.GetCycleDelayRange()) ;
}

// Tranverse the VeriClockedSequence
void VeriVisitor::VERI_VISIT(VeriClockedSequence, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriUnaryOperator, node) ;

     // Tranverse the clocking event
     TraverseNode(node.GetEventExpression()) ;
}

// Tranverse the VeriAssignInSequence
void VeriVisitor::VERI_VISIT(VeriAssignInSequence, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriUnaryOperator, node) ;

     // Tranverse the Array of statement
     TraverseArray(node.GetStatements()) ;
}

// Tranverse the VeriDistOperator
void VeriVisitor::VERI_VISIT(VeriDistOperator, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriUnaryOperator, node) ;

     // Tranverse the Array of dist items
     TraverseArray(node.GetDistList()) ;
}

// Tranverse the VeriSolveBefore
void VeriVisitor::VERI_VISIT(VeriSolveBefore, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

     // Tranverse the Array of variables which need to be solved
    TraverseArray(node.GetSolveList()) ;

     // Tranverse the Array of variables which need to be solved before
     // the solved list
     TraverseArray(node.GetBeforeList()) ;
}

// Tranverse the VeriCast
void VeriVisitor::VERI_VISIT(VeriCast, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

     // Tranverse the target type, The type to which we will cast
    TraverseNode(node.GetTargetType()) ;

     // Tranverse the expression which is casted
     TraverseNode(node.GetExpr()) ;
}

// Tranverse the VeriNew
void VeriVisitor::VERI_VISIT(VeriNew, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

     // Tranverse the  number of elements to create
    TraverseNode(node.GetSizeExpr()) ;

     // Tranverse the arguments to the new operators
     TraverseArray(node.GetArgs()) ;
}

// Tranverse the VeriConcatItem
void VeriVisitor::VERI_VISIT(VeriConcatItem, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

     // Traverse the expression which identifies the member label
    TraverseNode(node.GetMemberLabel()) ;

     // Traverse the expression which is the concat element
    TraverseNode(node.GetExpr()) ;
}

// Traverse the VeriIndexedExpr
void VeriVisitor::VERI_VISIT(VeriIndexedExpr, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse the prefix:
    TraverseNode(node.GetPrefixExpr()) ;

    // Traverse select expression
    TraverseNode(node.GetIndexExpr()) ;
}

// Traverse the VeriDPIFunctionDecl
void VeriVisitor::VERI_VISIT(VeriDPIFunctionDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriFunctionDecl, node) ;
}

// Traverse the VeriDPITaskDecl
void VeriVisitor::VERI_VISIT(VeriDPITaskDecl, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTaskDecl, node) ;
}

// Traverse the VeriScopeName
void VeriVisitor::VERI_VISIT(VeriScopeName, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriSelectedName, node) ;

    // Traverse the param value assignment list
    TraverseArray(node.GetParamValues()) ;

    // Traverse modport name
    TraverseNode(node.GetModportName()) ;
}

// VIPER #5710(vcs_compatibility: always_checker):
// Traverse sequential instantiation
void VeriVisitor::VERI_VISIT(VeriSequentialInstantiation, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse instance list
    TraverseArray(node.GetInstances()) ;
}

// Tranverse the VeriAssertion
void VeriVisitor::VERI_VISIT(VeriAssertion, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

     // Tranverse the property
    TraverseNode(node.GetPropertySpec()) ;

     // Tranverse the then stmt
    TraverseNode(node.GetThenStmt()) ;

     // Tranverse the else stmt
    TraverseNode(node.GetElseStmt()) ;
}

// Tranverse the VeriJumpStatement
void VeriVisitor::VERI_VISIT(VeriJumpStatement, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

     // Tranverse the Expression
    TraverseNode(node.GetExpr()) ;
}

// Tranverse the VeriDoWhile
void VeriVisitor::VERI_VISIT(VeriDoWhile, node)
{
     // Tranverse the Condition
    TraverseNode(node.GetCondition()) ;

    // Call of Base class Visit
    // The ordering is not manitained due to syntax
    VERI_VISIT_NODE(VeriLoop, node) ;
}

// Traverse the interface identifier
void VeriVisitor::VERI_VISIT(VeriInterfaceId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleId, node) ;

    // Don't traverse local scope

    // Don't traverse back pointer to interface body
}

// Traverse the program identifier
void VeriVisitor::VERI_VISIT(VeriProgramId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleId, node) ;

    // Don't traverse local scope

    // Don't traverse back pointer to program body
}

// Traverse the checker identifier
void VeriVisitor::VERI_VISIT(VeriCheckerId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriModuleId, node) ;

    // Don't traverse local scope

    // Don't traverse back pointer to checker body
}

// Traverse foreach loop statement
void VeriVisitor::VERI_VISIT(VeriForeach, node)
{
    // Traverse array name
    TraverseNode(node.GetArrayName()) ;

    // Traverse loop indexes
    TraverseArray(node.GetLoopIndexes()) ;

    // Call of Base class Visit
    VERI_VISIT_NODE(VeriLoop, node) ;
}

// Traverse wait_order statement
void VeriVisitor::VERI_VISIT(VeriWaitOrder, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse event list
    TraverseArray(node.GetEvents()) ;

    // Traverse pass statement
    TraverseNode(node.GetThenStmt()) ;

    // Traverse fail statement
    TraverseNode(node.GetElseStmt()) ;
}

// Traverse the class identifier
void VeriVisitor::VERI_VISIT(VeriTypeId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriVariable, node) ;

    // Don't traverse local scope

    // Don't traverse back pointer to interface body
}

// Traverse base class of with construct
void VeriVisitor::VERI_VISIT(VeriWithStmt, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse left hand side of 'with' construct
    TraverseNode(node.GetLeft()) ;
}

// Traverse 'with' construct whose right hand side is expression
void VeriVisitor::VERI_VISIT(VeriArrayMethodCall, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriWithStmt, node) ;

    // Traverse right hand side of 'with' keyword
    TraverseNode(node.GetRight()) ;
}
// Traverse 'with' construct whose right hand side is constraint block
void VeriVisitor::VERI_VISIT(VeriInlineConstraintStmt, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriWithStmt, node) ;

    // Traverse constraint block
    TraverseArray(node.GetConstraintBlock()) ;
}
// Traverse the modport identifier
void VeriVisitor::VERI_VISIT(VeriModportId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Don't traverse local scope

    // Don't traverse back pointer to modport body
}

// Traverse the null :
void VeriVisitor::VERI_VISIT(VeriNull, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // No field to traverse
}

// Traverse the dollar :
void VeriVisitor::VERI_VISIT(VeriDollar, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // No field to traverse
}
// Traverse assignment pattern
void VeriVisitor::VERI_VISIT(VeriAssignmentPattern, node)
{
    // Traverse the type of the prefix
    TraverseNode(node.GetTargetType()) ;

    // Call of Base class (concat elements) Visit
    VERI_VISIT_NODE(VeriConcat, node) ;
}

// Traverse multi assignment pattern
void VeriVisitor::VERI_VISIT(VeriMultiAssignmentPattern, node)
{
    // Traverse the type of the prefix
    TraverseNode(node.GetTargetType()) ;

    // Call of Base class (concat elements) Visit
    VERI_VISIT_NODE(VeriMultiConcat, node) ;
}

// Traverse the operator identifier
void VeriVisitor::VERI_VISIT(VeriOperatorId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Don't traverse back pointer to operator binding
}

// Traverse the clocking identifier
void VeriVisitor::VERI_VISIT(VeriClockingId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriVariable, node) ;

    // Don't traverse back pointer to clocking declarationoperator binding
}
// Traverse the clocking direction
void VeriVisitor::VERI_VISIT(VeriClockingDirection, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse the delay control
    TraverseNode(node.GetDelayControl()) ;
}
// Traverse streaming concatenation
void VeriVisitor::VERI_VISIT(VeriStreamingConcat, node)
{
    // Traverse the slice size
    TraverseNode(node.GetSliceSize()) ;

    // Call of Base class Visit
    VERI_VISIT_NODE(VeriConcat, node) ;
}
// Traverse cond predicate expression
void VeriVisitor::VERI_VISIT(VeriCondPredicate, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse left operand
    TraverseNode(node.GetLeft()) ;
    // Traverse right operand
    TraverseNode(node.GetRight()) ;
}
// Traverse dot name pattern
void VeriVisitor::VERI_VISIT(VeriDotName, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse name
    TraverseNode(node.GetVarName()) ;
}
// Traverse tagged union expression
void VeriVisitor::VERI_VISIT(VeriTaggedUnion, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse member name
    TraverseNode(node.GetMember()) ;

    // Traverse member value
    TraverseNode(node.GetExpr()) ;
}
// Traverse with construct whose right hand side is expression
void VeriVisitor::VERI_VISIT(VeriWithExpr, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriWith, node) ;

    // Traverse right hand side of 'with'
    TraverseNode(node.GetRight()) ;
}
// Traverse with construct whose right hand side is constraint block
void VeriVisitor::VERI_VISIT(VeriInlineConstraint, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriWith, node) ;

    // Traverse right hand side of 'with'
    TraverseArray(node.GetConstraintBlock()) ;
}
void VeriVisitor::VERI_VISIT(VeriBinValue, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse member expression
    TraverseNode(node.GetIffExpression()) ;
}
void VeriVisitor::VERI_VISIT(VeriOpenRangeBinValue, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriBinValue, node) ;

    // Traverse member array
    TraverseArray(node.GetOpenRangeList()) ;
}
void VeriVisitor::VERI_VISIT(VeriTransBinValue, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriBinValue, node) ;

    // Traverse member array
    TraverseArray(node.GetTransList()) ;
}
void VeriVisitor::VERI_VISIT(VeriDefaultBinValue, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriBinValue, node) ;
}
void VeriVisitor::VERI_VISIT(VeriSelectBinValue, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriBinValue, node) ;

    // Traverse member expression
    TraverseNode(node.GetSelectExpr()) ;
}
void VeriVisitor::VERI_VISIT(VeriTransSet, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse member list
    TraverseArray(node.GetTransRangeList()) ;
}
void VeriVisitor::VERI_VISIT(VeriTransRangeList, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse member array
    TraverseArray(node.GetTransItem()) ;

    // Traverse member expression
    TraverseNode(node.GetRepetition()) ;
}
void VeriVisitor::VERI_VISIT(VeriSelectCondition, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse member expression
    TraverseNode(node.GetBinsExpr()) ;

    // Traverse member array
    TraverseArray(node.GetOpenRangeList()) ;
}
// Traverse randsequence statement
void VeriVisitor::VERI_VISIT(VeriRandsequence, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse production
    TraverseNode(node.GetStartingProduction()) ;

    // Traverse array of statements
    TraverseArray(node.GetProductions()) ;

    // Don't traverse scope
}
// Traverse code block statement
void VeriVisitor::VERI_VISIT(VeriCodeBlock, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriStatement, node) ;

    // Traverse array of declarations
    TraverseArray(node.GetDecls()) ;

    // Traverse array of statements
    TraverseArray(node.GetStmts()) ;

    // Don't traverse scope
}
// Traverse production
void VeriVisitor::VERI_VISIT(VeriProduction, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse data type
    TraverseNode(node.GetDataType()) ;

    // Traverse production id
    TraverseNode(node.GetId()) ;

    // Traverse array of formals
    TraverseArray(node.GetFormals()) ;

    // Traverse array of items
    TraverseArray(node.GetItems()) ;
}
// Traverse production
void VeriVisitor::VERI_VISIT(VeriProductionItem, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTreeNode, node) ;

    // Traverse rand join expression
    TraverseNode(node.GetExpression()) ;

    // Traverse array of items
    TraverseArray(node.GetItems()) ;

    // Traverse weight specification
    TraverseNode(node.GetWeightSpec()) ;

    // Traverse code block
    TraverseNode(node.GetCodeBlock()) ;
}
// Traverse production identifier
void VeriVisitor::VERI_VISIT(VeriProductionId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;
}
// Traverse base class of 'with' construct
void VeriVisitor::VERI_VISIT(VeriWith, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse left hand side of 'with'
    TraverseNode(node.GetLeft()) ;
}
// Traverse the VeriTypeOperator
void VeriVisitor::VERI_VISIT(VeriTypeOperator, node)
{
    // Call of Base class Visit
    //VERI_VISIT_NODE(VeriDataType, node) ;

    // Traverse the type reference name :
    TraverseNode(node.GetArg()) ;
}
// Traverse pattern match
void VeriVisitor::VERI_VISIT(VeriPatternMatch, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse left
    TraverseNode(node.GetLeft()) ;
    // Traverse right
    TraverseNode(node.GetRight()) ;
}
// Traverse pattern match
void VeriVisitor::VERI_VISIT(VeriConstraintSet, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriExpression, node) ;

    // Traverse expressions
    TraverseArray(node.GetExpressions()) ;
}
// Traverse named port
void VeriVisitor::VERI_VISIT(VeriNamedPort, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriVariable, node) ;

    // Traverse optional port expression
    TraverseNode(node.GetPortExpression()) ;
}
// Traverse binsid
void VeriVisitor::VERI_VISIT(VeriBinsId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriVariable, node) ;
}

// Traverse option/type_option id
void VeriVisitor::VERI_VISIT(VeriCovgOptionId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Traverse optional port expression
    TraverseNode(node.GetDataType()) ;
}
void VeriVisitor::VERI_VISIT(VeriSeqPropertyFormal, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriVariable, node) ;
}
void VeriVisitor::VERI_VISIT(VeriLetId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;
}
void VeriVisitor::VERI_VISIT(VeriExternForkjoinTaskId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriTaskId, node) ;
}
void VeriVisitor::VERI_VISIT(VeriLabelId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriBlockId, node) ;
}
void VeriVisitor::VERI_VISIT(VeriPrototypeId, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdDef, node) ;

    // Need to visit actual identifier ?
}
// Traverse the VeriKeyword
void VeriVisitor::VERI_VISIT(VeriKeyword, node)
{
    // Call of Base class Visit
    VERI_VISIT_NODE(VeriIdRef, node) ;
}

/////////////////////  Accept Routines  /////////////////////////////

// The following class definitions can be found in VeriTreeNode.h
void VeriTreeNode::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriTreeNode, v) ; }
void VeriCommentNode::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriCommentNode, v) ; }

// The following class definitions can be found in VeriModule.h
void VeriModule::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriModule, v) ; }
void VeriPrimitive::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriPrimitive, v) ; }
void VeriConfiguration::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriConfiguration, v) ; }

// The following class definitions can be found in VeriExpression.h
void VeriExpression::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriExpression, v) ; }
void VeriName::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriName, v) ; }
void VeriIdRef::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriIdRef, v) ; }
void VeriIndexedId::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriIndexedId, v) ; }
void VeriSelectedName::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriSelectedName, v) ; }
void VeriIndexedMemoryId::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriIndexedMemoryId, v) ; }
void VeriConcat::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriConcat, v) ; }
void VeriMultiConcat::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriMultiConcat, v) ; }
void VeriFunctionCall::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriFunctionCall, v) ; }
void VeriSystemFunctionCall::Accept(VeriVisitor &v)          { VERI_ACCEPT(VeriSystemFunctionCall, v) ; }
void VeriMinTypMaxExpr::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriMinTypMaxExpr, v) ; }
void VeriUnaryOperator::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriUnaryOperator, v) ; }
void VeriBinaryOperator::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriBinaryOperator, v) ; }
void VeriQuestionColon::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriQuestionColon, v) ; }
void VeriEventExpression::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriEventExpression, v) ; }
void VeriPortConnect::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriPortConnect, v) ; }
void VeriPortOpen::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriPortOpen, v) ; }
void VeriAnsiPortDecl::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriAnsiPortDecl, v) ; }
void VeriTimingCheckEvent::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriTimingCheckEvent, v) ; }
void VeriDataType::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriDataType, v) ; }
void VeriNetDataType::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriNetDataType, v) ; }
void VeriPathPulseVal::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriPathPulseVal, v) ; }

// The following class definitions can be found in VeriId.h
void VeriIdDef::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriIdDef, v) ; }
void VeriVariable::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriVariable, v) ; }
void VeriInstId::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriInstId, v) ; }
void VeriModuleId::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriModuleId, v) ; }
void VeriUdpId::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriUdpId, v) ; }
void VeriConfigurationId::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriConfigurationId, v) ; }
void VeriTaskId::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriTaskId, v) ; }
void VeriFunctionId::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriFunctionId, v) ; }
void VeriGenVarId::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriGenVarId, v) ; }
void VeriParamId::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriParamId, v) ; }
void VeriBlockId::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriBlockId, v) ; }

// The following class definitions can be found in VeriMisc.h
void VeriRange::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriRange, v) ; }
void VeriStrength::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriStrength, v) ; }
void VeriNetRegAssign::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriNetRegAssign, v) ; }
void VeriDefParamAssign::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriDefParamAssign, v) ; }
void VeriCaseItem::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriCaseItem, v) ; }
void VeriGenerateCaseItem::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriGenerateCaseItem, v) ; }
void VeriPath::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriPath, v) ; }
void VeriDelayOrEventControl::Accept(VeriVisitor &v)         { VERI_ACCEPT(VeriDelayOrEventControl, v) ; }
void VeriConfigRule::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriConfigRule, v) ; }
void VeriInstanceConfig::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriInstanceConfig, v) ; }
void VeriCellConfig::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriCellConfig, v) ; }
void VeriDefaultConfig::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriDefaultConfig, v) ; }
void VeriUseClause::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriUseClause, v) ; }
#ifdef VERILOG_PATHPULSE_PORTS
void VeriPathPulseValPorts::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriPathPulseValPorts, v) ; }
#endif

// The following class definitions can be found in VeriModuleItem.h
void VeriModuleItem::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriModuleItem, v) ; }
void VeriDataDecl::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriDataDecl, v) ; }
void VeriNetDecl::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriNetDecl, v) ; }
void VeriFunctionDecl::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriFunctionDecl, v) ; }
void VeriTaskDecl::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriTaskDecl, v) ; }
void VeriDefParam::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriDefParam, v) ; }
void VeriContinuousAssign::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriContinuousAssign, v) ; }
void VeriGateInstantiation::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriGateInstantiation, v) ; }
void VeriModuleInstantiation::Accept(VeriVisitor &v)         { VERI_ACCEPT(VeriModuleInstantiation, v) ; }
void VeriSpecifyBlock::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriSpecifyBlock, v) ; }
void VeriPathDecl::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriPathDecl, v) ; }
void VeriSystemTimingCheck::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriSystemTimingCheck, v) ; }
void VeriInitialConstruct::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriInitialConstruct, v) ; }
void VeriAlwaysConstruct::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriAlwaysConstruct, v) ; }
void VeriGenerateConstruct::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriGenerateConstruct, v) ; }
void VeriGenerateConditional::Accept(VeriVisitor &v)         { VERI_ACCEPT(VeriGenerateConditional, v) ; }
void VeriGenerateCase::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriGenerateCase, v) ; }
void VeriGenerateFor::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriGenerateFor, v) ; }
void VeriGenerateBlock::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriGenerateBlock, v) ; }
void VeriTable::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriTable, v) ; }
void VeriPulseControl::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriPulseControl, v) ; }

// The following class definitions can be found in VeriStatement.h
void VeriStatement::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriStatement, v) ; }
void VeriNullStatement::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriNullStatement, v) ; }
void VeriBlockingAssign::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriBlockingAssign, v) ; }
void VeriNonBlockingAssign::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriNonBlockingAssign, v) ; }
void VeriGenVarAssign::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriGenVarAssign, v) ; }
void VeriAssign::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriAssign, v) ; }
void VeriDeAssign::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriDeAssign, v) ; }
void VeriForce::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriForce, v) ; }
void VeriRelease::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriRelease, v) ; }
void VeriTaskEnable::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriTaskEnable, v) ; }
void VeriSystemTaskEnable::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriSystemTaskEnable, v) ; }
void VeriDelayControlStatement::Accept(VeriVisitor &v)       { VERI_ACCEPT(VeriDelayControlStatement, v) ; }
void VeriEventControlStatement::Accept(VeriVisitor &v)       { VERI_ACCEPT(VeriEventControlStatement, v) ; }
void VeriConditionalStatement::Accept(VeriVisitor &v)        { VERI_ACCEPT(VeriConditionalStatement, v) ; }
void VeriCaseStatement::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriCaseStatement, v) ; }
void VeriLoop::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriLoop, v) ; }
void VeriForever::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriForever, v) ; }
void VeriRepeat::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriRepeat, v) ; }
void VeriWhile::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriWhile, v) ; }
void VeriFor::Accept(VeriVisitor &v)                         { VERI_ACCEPT(VeriFor, v) ; }
void VeriWait::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriWait, v) ; }
void VeriDisable::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriDisable, v) ; }
void VeriEventTrigger::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriEventTrigger, v) ; }
void VeriSeqBlock::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriSeqBlock, v) ; }
void VeriParBlock::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriParBlock, v) ; }

// The following class definitions can be found in VeriConstVal.h
void VeriConst::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriConst, v) ; }
void VeriConstVal::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriConstVal, v) ; }
void VeriIntVal::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriIntVal, v) ; }
void VeriRealVal::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriRealVal, v) ; }

// The following class definitions can be found in VeriLibrary.h
void VeriLibraryDecl::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriLibraryDecl, v) ; }

// The System Verilog Class
void VeriInterface::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriInterface, v) ; }
void VeriProgram::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriProgram, v) ; }
void VeriClass::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriClass, v) ; }
void VeriPropertyDecl::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriPropertyDecl, v) ; }
void VeriSequenceDecl::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriSequenceDecl, v) ; }
void VeriModport::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriModport, v) ; }
void VeriModportDecl::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriModportDecl, v) ; }
void VeriClockingDecl::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriClockingDecl, v) ; }
void VeriConstraintDecl::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriConstraintDecl, v) ; }
void VeriBindDirective::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriBindDirective, v) ; }
void VeriPackage::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriPackage, v) ; }
void VeriChecker::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriChecker, v) ; }
void VeriSequentialInstantiation::Accept(VeriVisitor &v)     { VERI_ACCEPT(VeriSequentialInstantiation, v) ; }
void VeriTypeRef::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriTypeRef, v) ; }
void VeriKeyword::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriKeyword, v) ; }
void VeriStructUnion::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriStructUnion, v) ; }
void VeriEnum::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriEnum, v) ; }
void VeriDotStar::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriDotStar, v) ; }
void VeriIfOperator::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriIfOperator, v) ; }
void VeriSequenceConcat::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriSequenceConcat, v) ; }
void VeriClockedSequence::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriClockedSequence, v) ; }
void VeriAssignInSequence::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriAssignInSequence, v) ; }
void VeriDistOperator::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriDistOperator, v) ; }
void VeriSolveBefore::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriSolveBefore, v) ; }
void VeriCast::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriCast, v) ; }
void VeriNew::Accept(VeriVisitor &v)                         { VERI_ACCEPT(VeriNew, v) ; }
void VeriConcatItem::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriConcatItem, v) ; }
void VeriAssertion::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriAssertion, v) ; }
void VeriJumpStatement::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriJumpStatement, v) ; }
void VeriDoWhile::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriDoWhile, v) ; }
void VeriInterfaceId::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriInterfaceId, v) ; }
void VeriProgramId::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriProgramId, v) ; }
void VeriCheckerId::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriCheckerId, v) ; }
void VeriTypeId::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriTypeId, v) ; }
void VeriModportId::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriModportId, v) ; }
void VeriNull::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriNull, v) ; }
void VeriDollar::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriDollar, v) ; }
void VeriAssignmentPattern::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriAssignmentPattern, v) ; }
void VeriMultiAssignmentPattern::Accept(VeriVisitor &v)      { VERI_ACCEPT(VeriMultiAssignmentPattern, v) ; }
void VeriTimeLiteral::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriTimeLiteral, v) ; }
void VeriOperatorId::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriOperatorId, v) ; }
void VeriOperatorBinding::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriOperatorBinding, v) ; }
void VeriForeachOperator::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriForeachOperator, v) ; }
void VeriStreamingConcat::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriStreamingConcat, v) ; }
void VeriNetAlias::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriNetAlias, v) ; }
void VeriTimeUnit::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriTimeUnit, v) ; }
void VeriForeach::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriForeach, v) ; }
void VeriWaitOrder::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriWaitOrder, v) ; }
void VeriCondPredicate::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriCondPredicate, v) ; }
void VeriDotName::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriDotName, v) ; }
void VeriTaggedUnion::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriTaggedUnion, v) ; }
void VeriClockingId::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriClockingId, v) ; }
void VeriClockingDirection::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriClockingDirection, v) ; }
void VeriClockingSigDecl::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriClockingSigDecl, v) ; }
void VeriRandsequence::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriRandsequence, v) ; }
void VeriCodeBlock::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriCodeBlock, v) ; }
void VeriProduction::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriProduction, v) ; }
void VeriProductionItem::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriProductionItem, v) ; }
void VeriProductionId::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriProductionId, v) ; }
void VeriWith::Accept(VeriVisitor &v)                        { VERI_ACCEPT(VeriWith, v) ; }
void VeriWithExpr::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriWithExpr, v) ; }
void VeriInlineConstraint::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriInlineConstraint, v) ; }
void VeriWithStmt::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriWithStmt, v) ; }
void VeriArrayMethodCall::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriArrayMethodCall, v) ; }
void VeriInlineConstraintStmt::Accept(VeriVisitor &v)        { VERI_ACCEPT(VeriInlineConstraintStmt, v) ; }
void VeriCovergroup::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriCovergroup, v) ; }
void VeriCoverageOption::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriCoverageOption, v) ; }
void VeriCoverageSpec::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriCoverageSpec, v) ; }
void VeriBinDecl::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriBinDecl, v) ; }
void VeriBinValue::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriBinValue, v) ; }
void VeriOpenRangeBinValue::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriOpenRangeBinValue, v) ; }
void VeriTransBinValue::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriTransBinValue, v) ; }
void VeriDefaultBinValue::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriDefaultBinValue, v) ; }
void VeriSelectBinValue::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriSelectBinValue, v) ; }
void VeriTransSet::Accept(VeriVisitor &v)                    { VERI_ACCEPT(VeriTransSet, v) ; }
void VeriTransRangeList::Accept(VeriVisitor &v)              { VERI_ACCEPT(VeriTransRangeList, v) ; }
void VeriSelectCondition::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriSelectCondition, v) ; }
void VeriTypeOperator::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriTypeOperator, v) ; }
void VeriImportDecl::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriImportDecl, v) ; }
void VeriLetDecl::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriLetDecl, v) ; }
void VeriDefaultDisableIff::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriDefaultDisableIff, v) ; }
void VeriExportDecl::Accept(VeriVisitor &v)                  { VERI_ACCEPT(VeriExportDecl, v) ; }
void VeriScopeName::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriScopeName, v) ; }
void VeriNamedPort::Accept(VeriVisitor &v)                   { VERI_ACCEPT(VeriNamedPort, v) ; }
void VeriPatternMatch::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriPatternMatch, v) ; }
void VeriConstraintSet::Accept(VeriVisitor &v)               { VERI_ACCEPT(VeriConstraintSet, v) ; }
void VeriCovgOptionId::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriCovgOptionId, v) ; }
void VeriBinsId::Accept(VeriVisitor &v)                      { VERI_ACCEPT(VeriBinsId, v) ; }
void VeriSeqPropertyFormal::Accept(VeriVisitor &v)           { VERI_ACCEPT(VeriSeqPropertyFormal, v) ; }
void VeriLetId::Accept(VeriVisitor &v)                       { VERI_ACCEPT(VeriLetId, v) ; }
void VeriExternForkjoinTaskId::Accept(VeriVisitor &v)        { VERI_ACCEPT(VeriExternForkjoinTaskId, v) ; }
void VeriCaseOperator::Accept(VeriVisitor &v)                { VERI_ACCEPT(VeriCaseOperator, v) ; }
void VeriCaseOperatorItem::Accept(VeriVisitor &v)            { VERI_ACCEPT(VeriCaseOperatorItem, v) ; }
void VeriIndexedExpr::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriIndexedExpr, v) ; }
void VeriDPIFunctionDecl::Accept(VeriVisitor &v)             { VERI_ACCEPT(VeriDPIFunctionDecl, v) ; }
void VeriDPITaskDecl::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriDPITaskDecl, v) ; }
void VeriLabelId::Accept(VeriVisitor &v)                     { VERI_ACCEPT(VeriLabelId, v) ; }
void VeriPrototypeId::Accept(VeriVisitor &v)                 { VERI_ACCEPT(VeriPrototypeId, v) ; }

