/*
 *
 * [ File Version : 1.40 - 2014/02/05 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_RUNTIME_FLAGS_H_
#define _VERIFIC_VERI_RUNTIME_FLAGS_H_

#include "VerificSystem.h"
#include "VeriCompileFlags.h"
#include "RuntimeFlags.h" // global run-time flags utility

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

// Central Runtime flags mechanism implemented with utility "RuntimeFlags", under VIPER 7208.
//
// Many aspects of Verific's  can be customized by compile-time and run-time switches.
//
// The runtime switches relevant to the Verilog parser (analyzer) and elaborator (static and RTL)
// are defined here in the class VeriRuntimeFlags, which is derived from the Verific
// global run-time variable utility 'RuntimeFlags' (defined in the file util/RuntimeFlags.h).
//
// Note that some of these global variables may be shared with other components.
// For example, the "preserve_user_nets" switch is common to Verilog RTL elaboration,
// VHDL RTL elaboration and certain netlist (Database) optimization routines.
//
// Creation (and initialization) of each run-time variable is done in the routine 'Initialize',
// which is automatically called once, during program-initialization time.
// During run-time, individual variables can be set to a different value using
// the (static) RuntimeFlags::Set() API, and their value obtained with the RuntimeFlags::Get() API.
//
// Advanced : If you choose to implement Set/Get on this class VeriRuntimeFlags, then you can
// provide an alternative implementation (than the default RuntimeFlags::Set/Get) for
// callers of VeriRuntimeFlags::Set/Get.
//
// If all Verilog related variables should be re-set to original (initial) setting, simply call
// VeriRuntimeFlags::Initialize() again.
//
// For compile time flags (from the VeriCompileFlags.h header) that have been converted to
// runtime flags, Verific uses the compile-time flags initialize the run-time variable
// as shown in the Initialize() routine below.
//
// As a Verific source-code customer, you are free to change the initial values of the
// existing variables, and are free to add run-time variables yourself (on which you then
// can rely in the source code that you want to customize).
// Either way, if you make changes to source code, please share these with Verific for
// merge into your companies' code branch, so that we can make sure that we are running
// the software in the same way as you do. This (share you source code modifications with us)
// would make it much easier for you to integrate new monthly releases from Verific into your system.
//
// Some of our existing customers have shared their own run-time flag mechanism with us,
// before Verific implemented the 'RuntimeFlags' utility. For these customers, we left your
// existing run-time flags in place (and removed Verific 'RuntimeFlags' calls).
// We would be happy to assist you merging your custom run-time flag mechanism
// with Verific's 'RuntimeFlags' utility, so that you obtain a consistent and extendable
// unified run-time variable management system.
//
class VFC_DLL_PORT VeriRuntimeFlags : public RuntimeFlags
{
private :
    // Class should never be instantiated
    friend class RuntimeFlags ;
    VeriRuntimeFlags() ;  // Purposely leave unimplemented
    ~VeriRuntimeFlags() ; // Purposely not virtual, and leave unimplemented

    // Initialization of all known global variables. This routine is called automatically, the first time any of the (RuntimeFlags::) API routines is called
    static void Initialize() {
        // Set run-time flags to compile time settings
        // Use AddVar() so that variable will get created if it was so far non-existing.
        // So at this point, all local runtime flags are registered (and available) system-wide, via the global 'RuntimeFlags' utility.

        // HDL analysis flags :
#ifdef VERILOG_PRESERVE_COMMENTS
        (void) AddVar("veri_preserve_comments", 1) ; // Preserve comments in parse trees during analysis of Verilog designs.
#else
        (void) AddVar("veri_preserve_comments", 0) ;
#endif
        // VIPER #7314: Support for nested translate off with same trigger under runtime flag:
        (void) AddVar("veri_allow_nesting_of_translate_off_pragma_with_same_trigger", 0) ;

        // VIPER #7855: Ignore nested translate/synthesis off/on pragmas.
        // We support nested translate/synthesis off/on pragmas with same trigger user the above flag
        // 'veri_allow_nesting_of_translate_off_pragma_with_same_trigger' for VIPER #7314.
        // This flag, when set will not allow any nesting (with or without same pragma trigger).
        // Default is previous behaviour of supporting nested translate/synthesis off/on pragmas.
        (void) AddVar("veri_ignore_pragma_nesting", 0) ;

#ifdef VERILOG_MOVE_YV_MODULES_INTO_WORK_LIBRARY
        (void) AddVar("veri_move_yv_modules_into_work_library", 1) ;
#else
        (void) AddVar("veri_move_yv_modules_into_work_library", 0) ;
#endif
#ifdef VERILOG_PROCESS_PRAGMAS_AS_COMMENTS
        (void) AddVar("veri_process_pragmas_as_comments", 1) ;
#else
        (void) AddVar("veri_process_pragmas_as_comments", 0) ;
#endif

        // RTL elaboration flags
#ifdef VERILOG_PRESERVE_USER_NETS
        (void) AddVar("veri_preserve_user_nets", 1) ;   // Preserve all user-nets, during RTL elaboration and netlist optimizations
#else
        (void) AddVar("veri_preserve_user_nets", 0) ;
#endif
#ifdef VERILOG_PRESERVE_ASSIGNMENTS
        (void) AddVar("veri_preserve_assignments", 1) ; // Preserve assignments during Verilog RTL elaboration.
#else
        (void) AddVar("veri_preserve_assignments", 0) ;
#endif
#ifdef VERILOG_PRESERVE_DRIVERS
        (void) AddVar("veri_preserve_drivers", 1) ;     // Preserve 'drivers' during Verilog RTL elaboration.
#else
        (void) AddVar("veri_preserve_drivers", 0) ;
#endif
#ifdef VERILOG_PRESERVE_X
        (void) AddVar("veri_preserve_x", 1) ;           // Preserve 'x' during Verilog RTL elaboration (instead of optimizing as don't-care state).
#else
        (void) AddVar("veri_preserve_x", 0) ;
#endif
#ifdef VERILOG_EXTRACT_MULTI_PORT_RAMS
        (void) AddVar("veri_extract_multiport_rams", 1) ; // Extract multi-port RAMs during Verilog RTL elaboration.
#else
        (void) AddVar("veri_extract_multiport_rams", 0) ;
#endif
#ifdef VERILOG_EXTRACT_RAMS
        (void) AddVar("veri_extract_dualport_rams", 1) ; // Extract dual-port RAMs during Verilog RTL elaboration.
#else
        (void) AddVar("veri_extract_dualport_rams", 0) ;
#endif
#ifdef VERILOG_PRESERVE_REGISTER_NAMES
        // Viper 8147: After removing VeriNode::ReNameInstance to Instance::ReNameInstance this flag is obselete
        // use corresponding database flag db_preserve_register_names instead.
        (void) AddVar("veri_preserve_register_names", 1) ; // During RTL elaboration, use the name of the target net to be the register instance name.
#else
        (void) AddVar("veri_preserve_register_names", 0) ;
#endif
#ifdef VERILOG_SYNOPSYS_REGISTER_NAMES
        // Viper 8147: After removing VeriNode::ReNameInstance to Instance::ReNameInstance this flag is obselete
        // use corresponding database flag db_synopsys_register_names instead.
        (void) AddVar("veri_synopsys_register_names", 1) ; // During RTL elaboration, use the name of the target net with a _reg suffix to be the register instance name.
#else
        (void) AddVar("veri_synopsys_register_names", 0) ; // During RTL elaboration, use the name of the target net with a _reg suffix to be the register instance name.
#endif
#ifdef VERILOG_IGNORE_ASSERTION_STATEMENTS
        (void) AddVar("veri_ignore_assertion_statements", 1) ; // Ignore assertion statements (including SVA) during RTL and static elaboration.
#else
        (void) AddVar("veri_ignore_assertion_statements", 0) ;
#endif
#ifdef VERILOG_ELABORATE_TOP_LEVEL_MODULES_HAVING_INTERFACE_PORTS
        (void) AddVar("veri_elaborate_top_level_modules_having_interface_ports", 1) ; // Elaborate top level modules having interface ports.
#else
        (void) AddVar("veri_elaborate_top_level_modules_having_interface_ports", 0) ;
#endif
#ifdef VERILOG_ONE_LEVEL_ELABORATION
        (void) AddVar("veri_one_level_elaboration", 1) ;
#else
        (void) AddVar("veri_one_level_elaboration", 0) ;
#endif

        // Static elaboration run-time flags
#ifdef VERILOG_REPLACE_CONST_EXPRS
        (void) AddVar("veri_replace_const_exprs", 1) ; // Replace expressions which must be constant according to LRM after successful static elaboration
#else
        (void) AddVar("veri_replace_const_exprs", 0) ;
#endif
#ifdef VERILOG_CLEANUP_BASE_MODULES
        (void) AddVar("veri_cleanup_base_modules", 1) ; // Cleanup the base modules created after static elaboration
#else
        (void) AddVar("veri_cleanup_base_modules", 0) ;
#endif
#ifdef VERILOG_UNIQUIFY_ALL_INSTANCES
        (void) AddVar("veri_uniquify_all_instances", 1) ; // Uniquify all instances
#else
        (void) AddVar("veri_uniquify_all_instances", 0) ;
#endif
#ifdef VERILOG_COPY_TOP_BEFORE_STATIC_ELAB
        (void) AddVar("veri_copy_top_before_static_elab", 1) ;
#else
        (void) AddVar("veri_copy_top_before_static_elab", 0) ;
#endif

	// CARBON_BEGIN
        (void) AddVar("veri_carbon_ignore_concat_items_with_0_repeat_count", 0) ; // set this to 1 to match the LRM for Verilog2005, and SystemVerilog variants, if this is 0 then a concat item
										  // with a 0 repeat count will be replaced with 1'b0.
	// CARBON_END

        // Verilog pre-processor :
#ifdef VERILOG_ANSI_INCLUDE_FILE_SEARCH
        (void) AddVar("veri_include_ansi_file_search", 1) ; // Use ANSI C/C++ #include search algorithm (relative to CWD) during Verilog analysis.
#else
        (void) AddVar("veri_include_ansi_file_search", 0) ;
#endif

        // RTL elaboration run-time flags
        (void) AddVar("veri_minimum_ram_size", 0) ;        // Minimum size (in bits) a RAM needs to be before RTL elaboration extract it. Value 0 means no lower limit.
        (void) AddVar("veri_loop_limit", 0) ;              // Maximum number of iterations through a non-static loop before RTL elaboration issues an error (maximum loop iterations exceeded). Value 0 means no limit.
        (void) AddVar("veri_nonconst_loop_limit", 200) ;   // Maximum number of iterations through a loop with non-constant condition before RTL elaboration issues an error (maximum loop iterations exceeded).
        (void) AddVar("veri_max_array_size", 23) ;         // Maximum size (in log2(bits) (number of address bits)) before RTL elaboration issues an error (excessive size) message. Value 0 means no limit.
        (void) AddVar("veri_max_stack_depth", 900) ;       // Maximum depth of subprogram recursion before RTL elaboration issues an error (stack depth exceeded) message.
        (void) AddVar("veri_elaborate_assertion_action_block", 0) ;         // Apply RTL elaboration on SVA 'action block' statements (statements executed under the condition that the assertion holds or fails.)  VIPER 7290 for details.
        (void) AddVar("veri_extract_multiport_rams_for_structures", 0) ;    // Viper #7277: Extract multi-port RAMs during Verilog RTL elaboration for SV structures.
        (void) AddVar("veri_elaborate_all_function_task_as_automatic", 0) ; // VIPER #7369: Elaborate all function/task as automatic even if they are not marked as automatic

        // Verilog static elaboration :
        (void) AddVar("veri_uniquify_hier_name_path", 0) ; // Static elaboration will copy modules whose elements are referred by any hierarchical path

        (void) AddVar("veri_max_hierarchy_depth", 1000) ; // Maximum depth of hierarchy before elaboration issues an error (stack depth exceeded) message. (VIPER #7568)
        // Assertion synthesis :
        (void) AddVar("veri_consider_vacuous_matches_for_implication", 0) ; // Consider vacuous matches while creating match of implication in assertion synthesis (VIPER #7573)
        (void) AddVar("veri_convert_assume_and_cover_statement", 1) ; // Flag to convert assume and cover statements in the same way as assertion (VIPER #7573). Decided to keep the default value of this flag as on
        (void) AddVar("veri_preserve_const_shifter", 0) ; // Flag to enable creation for creating shifter for constants. This address initial sim-mismatch issue: Viper 8244
        (void) AddVar("veri_reduced_flop_checker", 1) ; // Flag to enable creation for creating shifter for constants. This address initial sim-mismatch issue: Viper 8244
#ifdef VERILOG_DO_NOT_TOUCH_PARSE_TREE
        (void) AddVar("veri_do_not_touch_parse_tree", 1) ; // Flag to keep original version of every module in parse tree in static elaboration
#else
        (void) AddVar("veri_do_not_touch_parse_tree", 0) ; // Flag to keep original version of every module in parse tree in static elaboration
#endif // VERILOG_DO_NOT_TOUCH_PARSE_TREE
        // VIPER #5357 : Flag to control declaration scope of enum
        // By default enums declared within struct/union will be declared in
        // non-struct/union scope traversing upwards from struct/union scope
        // Set this flag to declare enums within struct/union scope
        (void) AddVar("veri_declare_enums_in_struct_union_scope", 0) ;

#ifdef VERILOG_DO_NOT_CREATE_LONG_MODULE_NAMES
        (void) AddVar("veri_do_not_create_long_module_names", 1) ; // Create short name for statically elaborated long module names (for names > 200 characters long)
#else
        (void) AddVar("veri_do_not_create_long_module_names", 0) ; // Create short name for statically elaborated long module names (for names > 200 characters long)
#endif
        (void) AddVar("veri_static_elaborate_unnamed_module_instances", 0) ; // VIPER #7938 : Create instance name for unnamed module instances and static elaborate them
        (void) AddVar("veri_semantic_checking_after_static_elab", 0) ; // VIPER #7953 : Introduce a pass on statically elaborated modules for semantic checking
        (void) AddVar("veri_conv_entity_port_param_name_based_on_scope", 0) ; // VIPER #8005 : Create port names of converted entity based on names of other ports and generic in the module

#ifdef VERILOG_ALTERNATE_GENERATE_ELAB
        // VIPER #8114: Add runtime flag for the compile flag 'VERILOG_ALTERNATE_GENERATE_ELAB'.
        // If this compile flag is on the runtime flag is also on by default: can be switch to off from command line.
        (void) AddVar("veri_alternative_generate_elab", 1) ;
#else
        // VIPER #8114: Add runtime flag for the compile flag 'VERILOG_ALTERNATE_GENERATE_ELAB'.
        // If this compile flag is off the runtime flag is also off by default: can be switch to on from command line.
        (void) AddVar("veri_alternative_generate_elab", 0) ;
#endif

#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        (void) AddVar("veri_create_name_for_unnamed_gen_block", 1) ;
#else
        (void) AddVar("veri_create_name_for_unnamed_gen_block", 0) ;
#endif

        // VIPER #8360 : Runtime flag to add an attribute to array instance elaboration
        // created single instance identifiers
        (void) AddVar("veri_add_attribute_to_array_instance_created_instance_id", 1) ;

#ifndef VERILOG_QUICK_PARSE_V_FILES
        // VIPER #8405: Ignore unnecessary modules in -v files.
        // Do this only if VERILOG_QUICK_PARSE_V_FILES is OFF because
        // both want to solve the same issue or long runtime, but in a different way.
        (void) AddVar("verilog_ignore_unnecessary_modules_in_v_files", 0) ;
#endif
    }
} ; // class VeriRuntimeFlags

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_RUNTIME_FLAGS_H_

