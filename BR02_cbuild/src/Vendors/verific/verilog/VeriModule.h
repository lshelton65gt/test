/*
 *
 * [ File Version : 1.247 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_MODULE_H_
#define _VERIFIC_VERI_MODULE_H_

#include "VeriModuleItem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class TextBasedDesignMod ; // for TBDM manipulation
class Map ;
class Array ;

class VeriIdDef ;
class Library ;
class Netlist ;
class VeriExpression ;
class VeriScope ;
class VeriLibrary;
class VeriTable ;
class ModuleVisit ;

class VhdlEntityDecl ;
class VhdlPrimaryUnit ;
class VhdlScope ;

class SynthesisInfo ;

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */

/*
   VeriModule is at the top of any Verilog parse tree.
   It represents one entire Verilog module, in parse-tree form.
   VeriModule's are stored in the static veri_file::_all_modules hash table,
   which represents all Verilog modules that have been analyzed.

   Main RTL elaboration routine on a VeriModule is 'Elaborate()', which
   decends into the hierarchy, elaborates lower level (instantiated) modules
   and creates a Netlist datastructure from the VeriModule.

   Since 8/2004, VeriModule is derived from VeriModuleItem.
   This is to accommodate System Verilog, where a module can also be a module item itself,
   and all modules module items of the 'root' module.
*/
class VFC_DLL_PORT VeriModule  : public VeriModuleItem
{
public:
    VeriModule(VeriIdDef *id, Array *parameter_connects, Array *port_connects, Array *module_items, VeriScope *scope);

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const                  { return ID_VERIMODULE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Resolve typically runs already in the VeriModule constructor, but separate call might be needed :
    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    void                        AnalyzeFull() ; // Analyze this module in full. Only do this when all modules are read in.

    // Tests
    virtual unsigned            IsModule() const                    { return 1 ; }
    virtual unsigned            IsPrimitive() const                 { return 0 ; } // Default catcher for primitive modules
    virtual unsigned            IsPslUnit() const                   { return 0 ; } // Default catcher for PSL modules
    virtual unsigned            IsInterface() const                 { return 0 ; } // Default catcher for VeriInterface modules
    virtual unsigned            IsProgram() const                   { return 0 ; } // Default catcher for VeriProgram modules
    virtual unsigned            IsPackage() const                   { return 0 ; } // Default catcher for VeriPackage (package is derived from a module in Verific)
    virtual unsigned            IsChecker() const                   { return 0 ; } // Default catcher for VeriChecker

    virtual unsigned            HasInterfacePorts() const ; // Check if this module has 'interface' ports. SV only. If so, we should not elaborate it as top-level.
    virtual unsigned            HasGenericInterfacePorts() const ; // Check if this module has generic 'interface' ports. SV only. If so, we should not elaborate it as top-level.
    virtual unsigned            HasTypeParameters() const ; // Check if this module has 'type' parameters. SV only.

    // Late parse-tree additions :
    unsigned                    AddModuleItem(VeriModuleItem *module_item) ; // Return 1 if item is added to this module

    // The name of module (Name() of id)
    const char *                Name() const ;
    const char *                GetName() const ; // Same as Name().  To be consistent with new accessor naming

    // Member accessor functions
    unsigned long               TimeStamp() const                   { return _timestamp ; }  // The time that this module was created.

    // Get the mode (dialect) under which this module was analyzed (needed for access to mode post-analysis) :
    virtual unsigned            GetAnalysisDialect() const          { return _analysis_dialect ; } // mode (dialect) how this module was parsed. Encoding of dialects is in veri_file

    // The Module Identifier
    virtual VeriIdDef *         GetId() const                       { return _id ; } // The standard way to obtain the declared identifier of a ModuleItem.
    VeriIdDef *                 Id() const                          { return _id ; } // For backward compatibility
    virtual VeriIdDef *         GetSingleDesignModuleId(unsigned /* recursion_depth */) const { return _id ; }  // This makes more sense only for configuration

    // (Pointer to) Array of PortIds (ports) in order of declaration (or 0)
    virtual Array *             GetPorts() const                    { return _ports ; }
    virtual VeriIdDef *         GetPort(const char *name) const ; // Get port of the module
    virtual VeriIdDef *         GetPortFromVhdlName(const char *name) const ; // Get port of the module

    // (Pointer to) Array of ParamIds (parameters) in order of declaration (or 0)
    virtual VeriIdDef *         GetParamAt(unsigned pos) const ;
    virtual VeriIdDef *         GetParam(const char *name) const ;
    virtual VeriIdDef *         GetParamFromVhdlName(const char *name) const ;
    virtual Array *             GetParameters() const               { return _parameters ; }

    // Pointer to its scope
    virtual VeriScope *         GetScope() const                    { return _scope ; }

    // Parse-tree Find routine : find an identifier by name in this module scope.
    VeriIdDef *                 FindDeclared(const char *name) const ;

    // Some other accessor methods
    Array *                     GetParameterConnects() const        { return _parameter_connects ; }
    Array *                     GetPortConnects() const             { return _port_connects ; }
    Array *                     GetModuleItems() const              { return _module_items ; }
    virtual Array *             GetItems() const                    { return _module_items ; } // For module/interface/program/class/generates. Return array of VeriModuleItem*, item list.
    Array *                     GetPackageImportDecls() const       { return _package_import_decls ; }

    // flags from Verilog directives set at construction time of module
    unsigned                    IsCellDefine() const                { return _is_celldefine ; }
    unsigned                    GetDefaultNetType() const ;
    unsigned                    GetUnconnectedDrive() const         { return _unconnected_drive ; }
    const char *                GetTimeScale() const                { return _timescale ; }
    void                        SetTimeScale(const char *s) ; // VIPER #5119

    // VIPER #7379: Added support for following optional compiler directives (IEEE-1800-2009):
    unsigned                    GetDelayModeDistributed() const     { return _delay_mode_distributed ; }
    unsigned                    GetDelayModePath() const            { return _delay_mode_path ; }
    unsigned                    GetDelayModeUnit() const            { return _delay_mode_unit ; }
    unsigned                    GetDelayModeZero() const            { return _delay_mode_zero ; }
    const char *                GetDefaultDecayTime() const         { return _default_decay_time ; }
    const char *                GetDefaultTriregStrength() const    { return _default_trireg_strength ; }

    // Get (Set) the library in which this module is compiled
    virtual VeriLibrary *       GetLibrary() const                  { return _parent_library ; }
    virtual void                SetLibrary(VeriLibrary *lib)        { _parent_library = lib ; }

    // Default catcher for configurations:
    virtual Array *             GetTopModuleNames() const           { return 0 ; }
    virtual Array *             GetConfigRules() const              { return 0 ; }

    // VIPER #6725: Returns the linefile spanning module item space of an *empty* module.
    // Note that it returns an *allocated* linefile, so avoid unnecessary calling.
    // At the same time, you do not need to delete the linefile either!
    // It only works under linefile-column mode, if it is not enabled, returns 0.
    linefile_type               GetEmptyModuleItemLinefile() const ;
    // VIPER #7453: Returns the linefile spanning module *empty* port declaration space.
    // Note that it returns an *allocated* linefile, so avoid unnecessary calling.
    // At the same time, you do not need to delete the linefile either!
    // It only works under linefile-column mode, if it is not enabled, returns 0.
    linefile_type               GetEmptyModulePortDeclLinefile() const ;

    // Used for SV only. Essentially an Array of VeriModuleInstantiation* back-pointers. Set up during AnalyzeFull(). Contains instantiations forced on this module by external bind directives.
    Array *                     GetBindInstances() const            { return _bind_instances ; }
    void                        AddBindInstance(const VeriModuleItem *bind_instance) ; // add a (external) instantiation to this module.

    void                        SetPackageImportDecls(Array *dls) ;
    void                        ClearPackageImportList() ;
    void                        ClearParameterConnects() ;
    void                        ClearPortConnects() ;

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const { return CopyModule(id_map_table) ; } // Copy module as  a ModuleItem :
    virtual VeriModuleItem *    CopyWithName(const char *name, VeriMapForCopy &id_map_table, unsigned add_copied_obj) ; // Copy module with the first argument specific name
    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const ; // Copy module as module

    virtual unsigned            ReplaceChildBy(VeriModuleItem *old_item, const Array *new_items) ; // Virtual routine to replace module/interface instantiation array  by bunch of single instantiations

    // Parse-tree creation/deletion routines
    VeriIdDef *                 AddPort(const char *port_name, unsigned port_direction, VeriDataType *subtype_indication) ; // Create and add argument specific port in module
    VeriIdDef *                 AddParameter(const char *parameter_name, VeriDataType *subtype_indication, VeriExpression *init_assign) ; // Create and add argument specific parameter to module
    VeriIdDef *                 AddSignal(const char *signal_name, VeriDataType *subtype_indication, VeriExpression *init_assign = 0) ; // Create and add signal to module
    VeriModuleInstantiation *   AddInstance(const char *instance_name, const char *instantiated_component_name) ; // Create and add instance of module 'instantiated_component_name'.
    unsigned                    AddPortRef(const char *instance_name, const char *formal_port_name, VeriExpression *actual) ; // Add 'actual' to the instance 'instance_name' for formal 'formal_port_name'
    // VIPER #6129 : API to add actual to the instance 'instance_name' for parameter 'formal_param_name'
    unsigned                    AddParamRef(const char *instance_name, const char *formal_param_name, VeriExpression *actual) ;
    unsigned                    RemovePort(const char *port_name) ; // Remove argument specific port
    unsigned                    RemoveParameter(const char *parameter_name) ; // Remove argument specific parameter
    unsigned                    RemoveSignal(const char *signal_name) ; // Remove argument specific signal
    unsigned                    RemoveInstance(const char *instance_name) ; // Remove argument specific instance
    unsigned                    RemovePortRef(const char *instance_name, const char *formal_port_name) ; // Remove actual of formal 'formal_port_name' from instance 'instance_name'
    // VIPER #6129 : API to remove actual of formal parameter 'formal_param_name' from instance 'instance_name
    unsigned                    RemoveParamRef(const char *instance_name, const char *formal_param_name) ;

    // Alternative (simplified) parse-tree manipulation routines
    void                        AddPort(const char *dir, const char *port_name) ; // add single-bit port
    void                        AddPort(const char *dir, const char *port_name, int left, int right) ; // add array port
    void                        AddSignal(const char *type, const char *name) ; // add single bit signal
    void                        AddSignal(const char *type, const char *name, int left, int right) ; // add array signal
    void                        AddPortRef(const char *inst_name, const char *formal, const char *actual) ; // add a port-reference to instance "inst_name" for formal "formal" and actual expression "actual".
    void                        AddAssignment(const char *lhs, const char *rhs) ; // add an assignment (rhs expression to lhs expression)

    // Text Based Design Manipulation API routines.
    // These routines manipulate the source file text via the TBDM object (and methods therein).

    // Add a port, net or instance to the module :
    unsigned                    TbdmAddPort(TextBasedDesignMod &tbdm, const char *name, const char *direction, const char *data_type=0, const char *packed_range=0, const char *unpacked_range=0) const ; // add port with name "name", direction ("input, output, inout"), type ("reg", "wire" etc), packed_range ("[6:0]" or "[size-1:0]") (printed before identifier), unpacked_range ("[6:0]" or "[size-1:0]") (printed after identifier). Return 0 for failure, non-0 for success.
    unsigned                    TbdmAddNet(TextBasedDesignMod &tbdm, const char *name, const char *data_type, const char *packed_range=0, const char *unpacked_range=0) const ; // add port with name "name", direction ("input, output, inout"), type ("reg", "wire" etc), packed_range ("[6:0]" or "[size-1:0]") (printed before identifier), unpacked_range ("[6:0]" or "[size-1:0]") (printed after identifier). Return 0 for failure, non-0 for success.
    unsigned                    TbdmAddInstance(TextBasedDesignMod &tbdm, const char *module_name, const char *instance_name, const char *param_list=0, const char *port_list=0) const ; // add instance with name "name" and parameter list (comma-separated list with ()'s), port list (comma-separated list with ()s). Return 0 for failure, non-0 for success.
    // Remove a port, net, instance from this module :
    unsigned                    TbdmRemovePort(TextBasedDesignMod &tbdm, const char *name) const ; // remove text for an existing port (note: can only remove ports in original text). Return 0 for failure, non-zero for success.
    unsigned                    TbdmRemoveNet(TextBasedDesignMod &tbdm, const char *name) const ; // remove text for an existing port (note: can only remove ports in original text). Return 0 for failure, non-zero for success.
    unsigned                    TbdmRemoveInstance(TextBasedDesignMod &tbdm, const char *name) const ; // remove text for an existing instance (note: can only remove instances in original text). Return 0 for failure, non-zero for success.

    // Add a portref to an instance, or change a portref to an instance
    // Instances under a generate statement need a hierarchical name ( "label[index].inst_name" ) to be accessed.
    // Note that TBDM for instances under generate loops is still under development
    unsigned                    TbdmAddPortRef(TextBasedDesignMod &tbdm, const char *instance_name, const char *actual_expr, const char *port_name=0) const ; // add portref at end of instance port list, named (name port name) or ordered (without port name). Return 0 for failure, non-0 for success.
    unsigned                    TbdmChangePortRef(TextBasedDesignMod &tbdm, const char *instance_name, const char *actual_expr, const char *port_name=0, unsigned order_nr=0) const ; // change existing portref with named (port_name) or ordered (order_nr). Return 0 for failure, non-0 for success.
    unsigned                    TbdmRemovePortRef(TextBasedDesignMod &tbdm, const char *instance_name, const char *port_name=0, unsigned order_nr=0) const ; // remove portref for an existing instance (note: can only remove instances in original text). Return 0 for failure, non-zero for success.

    // Static Elaboration
    unsigned                    StaticElaborate(Map *actual_params, Array *created_top_modules = 0) ; // The main routine to static elaborate a module :
    // Replace the verilog language defined constant expressions like range bounds, default value,
    // delay value and index expression.
    // Argument 'is_const_index_context' specifies whether context demands that index
    // expression of bit-select should be constant (target of continuous assignment/actual for
    // output port etc).
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ;

    unsigned                    IsCopiedModule() const              { return _original_module_name ? 1: 0 ; } // Was this module copied from another module
    unsigned                    IsBaseModule() const                { return _original_module_name ? 0: 1 ; } // Was this module originally instantiated before static elaboration occu.
    virtual VeriModule *        GetOriginalModule() const ; // Get the module from which this module is copied
    virtual const char *        GetOriginalModuleName() const ;  // Get the module name from which this module is copied

    // Create 'actual_params' Map from a char*generic_name->char*generic_value map from an external application */
    virtual Map *               CreateActualParameters(const Map *external_params) const ; // virtual for configuration
    VeriIdDef*                  GetInterfaceFromVhdlName(const char* name, unsigned is_param = 1) const ;

    // Access to flags used during elaboration (if included)
    // Control compilation from users : option to elaborate as a black-box module (netlist without contents)
    void                        SetCompileAsBlackbox()              { _compile_as_blackbox = 1 ; }
    void                        SetCompileNormal()                  { _compile_as_blackbox = 0 ; /* the default */ }
    unsigned                    GetCompileAsBlackbox() const        { return _compile_as_blackbox ; }

    // Flag module as a 'root' module (SystemVerilog root compilation scope) :
    virtual unsigned            IsRootModule() const                { return _is_root_module ; }

    // Flags set by AnalyzeFull() (post-parsing analysis).
    unsigned                    IsAnalyzed() const                  { return _is_analyzed ; } // Flag that module is fully analyzed (can only be done after all modules are read in, in AnalyzeFull())
    unsigned                    HasAnalyzeError() const             { return _has_analyze_error ; } // Flag that module showed an error in full analysis (check done after all modules are read in, in AnalyzeFull())
    void                        SetHasAnalyzeError(SynthesisInfo *info) ;

    // Flag set by elaboration (if included). Indicate that the module has been elaborated.
    unsigned                    IsCompiled() const                  { return _is_compiled ; }
    unsigned                    IsStaticElaborated() const          { return _is_static_elaborated ; } // Has the module been visited by static elaboration ?
    void                        SetStaticElaborated()               { _is_static_elaborated = 1 ; }    // Tag this module as having been visited by static elaboration

    void                        ClearId()                           { _id = 0 ; } // VIPER 3369
    virtual Set *               GetDesignModules() const            { return 0 ; } //Defined for VeriConfiguration only
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual Set *               GetDesignUnits() const              { return 0 ; } //Defined for VeriConfiguration only
#endif

    virtual VhdlPrimaryUnit*    ConvertModule(const char *lib_name) const ; // Converts a Verilog module to an entity and returns the converted entity

    // VIPER #5805 : Support for path_name attribute in mixed language
    unsigned                    IsAffectedByPathName() const     { return _affected_by_path_name ; }
    void                        SetAffectedByPathName()          { _affected_by_path_name = 1 ; }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl package to verilog one:
    virtual unsigned            IsVhdlPackage() const               { return _is_vhdl_package ; } // Flag indicating that the processing package is a converted verilog package from a vhdl package
    virtual unsigned            IsConvertedToVhdlPackage() const ;  // Flag to indicate that this verilog package is converted to vhdl
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION

//#*# VERIFIC_START DOCUMENT_INTERNAL

//***************** INTERNAL USE ONLY **************************
public :
    VeriModule(const VeriModule &mod, VeriMapForCopy &id_map_table); // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriModule(SaveRestore &save_restore) ;

    virtual ~VeriModule();

private:
    // Prevent compiler from defining the following
    VeriModule() ;                              // Purposely leave unimplemented
    VeriModule(const VeriModule &) ;            // Purposely leave unimplemented
    VeriModule& operator=(const VeriModule &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ProcessRootModule() ; // VIPER #6526: Special routine to do some 'resolve' thing for the root module when it is closed/done
    // User will call 'AnalyzeFull' not VarUsage
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;
    virtual void                ResolveBody() ; // top-level Resolve call, for Resolving entire Module.
    virtual void                InlineDeclDataTypes() ; // Copy data declaration, with uniquified/inlined data_type for each identifier. Needed for VIPER #7277
    void                        SetAnalysisDialect(unsigned d)      { _analysis_dialect = d ; } // mode (dialect) setting  (5686)
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' specific module item, and puts new module item in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it matches with any port expression, and puts new port expression in its place

    unsigned                    StaticElaborateInternal(Map *actual_params, Array *created_top_modules = 0, unsigned from_vhdl = 0, unsigned *has_path_name = 0) ; // static elaborate a module without copying itself:

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            StaticElaborateInternalToGetUnits(Map * /*actual_params*/, Array * /*created_units*/, unsigned /*from_vhdl*/, unsigned /*has_path_name*/) { return 0 ; } // static elaborate a module without copying itself: Defined for configuration

#endif
    virtual VeriModule *        CreateUniqueUnit(const Map *actual_params, const VhdlScope *vhdl_scope, VhdlScope *this_scope) ; // Create unique module inspecting argument specific parameter values. For viper 2056

    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams) ;
    void                        SetAsBaseModule() ;

    virtual unsigned            PortWidthDir(const char *port_name, unsigned port_pos, unsigned *dir, VeriIdDef **master_id, const char **formal_name) ; // virtual routine to get module/interface port dir and size

    virtual VeriPseudoTreeNode *CreateTreeRoot(Map *roots, Set &done) ; // Create root node(s) for this module and insert into the given Array
    virtual void                ExpandTree(ModuleVisit &visit, Map *roots, Set *done) ;    // Expand the tree by creating the full hierarchy

    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ; // VIPER #5919: For instantiations in nested modules
    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Internal routine to find instantiated module names iterating over module items
    // Flag module as a 'root' module (SystemVerilog root compilation scope) :
    void                        SetIsRootModule()                   { _is_root_module = 1 ; }

    virtual VeriIdDef *         GetMatchingTypeId(const char* /*type_name*/) const { return 0 ;} // Viper 6896 : Defined for VeriPackage
    // VIPER #5954 : Create and declare an identifier for non-ansi complex port
    virtual VeriIdDef *         DeclareNonAnsiPort(const char *name, unsigned dir, VeriExpression *port_expr, unsigned port_idx) ; // Declare non-ansi port

    // VIPER #6550: Generate explict state machine:
    unsigned                    GenerateExplicitStateMachine(unsigned reset) ;

    // VIPER #6550: Contains the number of replaced module items during explict state machine generation:
    unsigned                    GetNumOfReplacedItem() const                        { return _module_item_replaced ; }
    void                        SetModuleItemReplaced(unsigned num_of_items)        { _module_item_replaced = num_of_items ; }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    void                        SetVhdlPackage(unsigned value)      { _is_vhdl_package = value ; } // Set the flag on a converted verilog package from a vhdl package
    unsigned                    IsAlreadyProcessed() const          { return _is_already_processed ; } // Indicates that we have already processed the package: we may come to initialize the package if the package is imported to more than one designs
    void                        SetAlreadyProcessed(unsigned value) { _is_already_processed = value ; } // Set the flag when we are initializing the package for the first time
#endif
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual void                FillRangesAndInitialExpr()          { return ; } // Fill up the verilog ranges and initial value of constants during elaboration
#endif

    char *                      GetInterfacePortsString() const ; // VIPER #7324: Check and return a comma separated string of interface port names (caller is responsible to free the returned string)
    char *                      GetGenericInterfacePortsString() const ; // VIPER #7324: Check and return a comma separated string of generic interface port names (caller is responsible to free the returned string)

    virtual VeriScope *         GetPortScope() const  { return _port_scope ; } // Returns scope generated for to store named port like (.a(expr)) (exists in relaxed checking mode)
    virtual char *              GetTimeUnitDefinedTimeScale() const ; // VIPER #8304: Returns allocated timescale creating from timeunit and timeprecision declaration

protected:
// Tree nodes :
    VeriIdDef       *_id ;                      // Module identifier.
    Array           *_package_import_decls ;    // Array of imported packages (VeriImportDecl*). New in SV-2009
    Array           *_parameter_connects ;      // Array of parameter VeriDataDecl(VeriModuleItem)*. New in Verilog 2000. Declared parameters :  #(<paramdecl1> <paramdecl2> ..)
    Array           *_port_connects ;           // Array of VeriExpression*. Declared or Defined ports :  (<portexpr1> <portexpr2> ..)
    Array           *_module_items ;            // Array of VeriModuleItem*. The module's module items.
    VeriScope       *_scope ;                   // Module-scope symbol table.

// Additional nodes :
    Array           *_parameters ;              // Array of VeriIdDef*. Set up in constructor. Lists all parameters of this module in ORDER.
    Array           *_ports ;                   // Array of VeriIdDef*. Set up in constructor. Lists all ports of this module in ORDER.
    Array           *_bind_instances ;         // Used for SV only. Array of VeriModuleInstantiation*. Set up during AnalyzeFull(). Contains instantiations forced on this module by external bind directives.

    unsigned long    _timestamp ;               // Time that this module was created (set with ::time() in regular constructor).

    // language dialect at time of parsing :
    unsigned         _analysis_dialect:4 ;      // Language dialect used to parse this module (VERILOG_95, VERILOG_2K, SYSTEM_VERILOG, VERILOG_PSL, VERILOG_AMS or UNDEFINED)

    // Preprocessor macro settings at time of parsing :
    unsigned         _is_celldefine:1 ;         // Flag set in constructor (if `celldefine was set)
    unsigned         _default_nettype:11 ;      // VERI token set in constructor (setting of `default_nettype)
    unsigned         _unconnected_drive:11 ;    // VERI token set in constructor (setting of `unconnected_drive)

    // flags used during elaboration (if included).
    unsigned         _is_analyzed:1 ;           // Flag stating that module is fully analyzed (set in AnalyzeFull()).
    unsigned         _has_analyze_error:1 ;     // Flag that module showed an error in full analysis (set in AnalyzeFull())
    unsigned         _is_compiled:1 ;           // Flag set during (at) elaboration
    unsigned         _is_static_elaborated:1 ;  // Flag to test if this module is already statically elaborated.
    unsigned         _compile_as_blackbox:1 ;   // Flag set by user SetCompileAsBlackbox(), will cause elaboration to create a black-box netlist (only interfaces) for this module.

    // More flags (32 filled above) :
    unsigned         _is_root_module:1 ;        // Flag that this is a 'root' module (SystemVerilog root compilation scope).
    unsigned         _affected_by_path_name:1 ;  // Flag to indicate whether this module is to be make unique for the presence of path_name/instance_name attribute in the hierarchy

    char            *_original_module_name ;     // Name of design file specific module from which this module is copied (during static elaboration only).

    char            *_timescale ;               // char* set in constructor (setting of `timescale at module parsing time)

    VeriLibrary     *_parent_library ;          // The library that owns this module

    VeriScope       *_port_scope ;              // Scope for non-ansi ports (VIPER #5954

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    Array           *_linefile_structs ; // VIPER #5932: The Array of linefile structures created for this module
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    // VIPER #6550: Contains the number of replaced module items during explict state machine generation:
    unsigned        _module_item_replaced:10 ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    unsigned        _is_vhdl_package: 1 ;      // VIPER #6895: flag for converted verilog package from a vhld one
    unsigned        _is_already_processed: 1 ; // VIPER #6895: flag on package that is already elaborated
#endif
    // VIPER #7379: Added support for following optional compiler directives (IEEE-1800-2009):
    unsigned        _delay_mode_distributed: 1 ;
    unsigned        _delay_mode_path: 1 ;
    unsigned        _delay_mode_unit: 1 ;
    unsigned        _delay_mode_zero: 1 ;
    char *          _default_decay_time ;
    char *          _default_trireg_strength ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriModule

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriPrimitive : public VeriModule
{
public:
    VeriPrimitive(VeriIdDef *id, Array *ports, Array *module_items, VeriTable *table, VeriScope *scope);

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPRIMITIVE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsPrimitive() const         { return 1 ; }
    virtual VeriTable *         GetPrimitiveTable() const ;

    // Parse tree copy routine :
    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const { return Copy(id_map_table) ; } // Copy primitive as module
    VeriPrimitive *             Copy(VeriMapForCopy &id_map_table) const ; // Copy primitive

// RD: Treat pr primitive as a regular module
//    virtual VhdlPrimaryUnit*    ConvertModule(const char* /*lib_name*/) const { return 0 ;}
//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPrimitive(const VeriPrimitive &prim, VeriMapForCopy & id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriPrimitive(SaveRestore &save_restore) ;

    virtual ~VeriPrimitive();

private:
    // Prevent compiler from defining the following
    VeriPrimitive() ;                                 // Purposely leave unimplemented
    VeriPrimitive(const VeriPrimitive &) ;            // Purposely leave unimplemented
    VeriPrimitive& operator=(const VeriPrimitive &) ; // Purposely leave unimplemented
public :
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

//#*# VERIFIC_END DOCUMENT_INTERNAL

} ; // class VeriPrimitive

/* -------------------------------------------------------------- */

/*
   VIPER #2739: This class represents Verilog 2000 configuration declaration. Configurations
   belong to the same namespace/scope as modules and also treated similar to modules. They
   are stored in library. If a configuration and a modules have same name, the later will
   overwrite the previous.
*/
class VFC_DLL_PORT VeriConfiguration : public VeriModule
{
public:
    VeriConfiguration(VeriIdDef *id, Array *top_modules, Array *config_rules) ;
    VeriConfiguration(VeriIdDef *id, Array *local_params, Array *top_modules, Array *config_rules, VeriScope *scope) ;

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERICONFIGURATION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Print Parse Tree
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsConfiguration() const         { return 1 ; }

    virtual Array *             GetTopModuleNames() const       { return _top_modules ; }
    virtual Array *             GetConfigRules() const          { return _config_rules ; }

    // (Pointer to) Array of PortIds (ports) in order of declaration (or 0)
    // Return the ports of underlying top design. Returns null for multiple top.
    virtual Array *             GetPorts() const ;
    virtual VeriIdDef *         GetPort(const char *name) const ; // Get port of the design module

    // (Pointer to) Array of ParamIds (parameters) in order of declaration (or 0)
    // Return the parameters of underlying top design. Returns null for multiple top.
    // Viper #8022 shows the need
    virtual VeriIdDef *         GetParamAt(unsigned pos) const ;
    virtual VeriIdDef *         GetParam(const char *name) const ;
    virtual Array *             GetParameters() const ;

    // Returns the Set of VeriModule * of the 'design' modules. Since, it returns a Set,
    // it will return only one occurrence for multiple same 'designs'.
    // Caller of this routine is responsible to delete the returned Set.
    virtual Set *               GetDesignModules() const ;
    virtual VeriIdDef *         GetSingleDesignModuleId(unsigned recursion_depth) const ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual Set *               GetDesignUnits() const ;
#endif

    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const ; // Copy configuration as module

    virtual Map *               CreateActualParameters(const Map *external_params) const ;
    virtual VhdlPrimaryUnit*    ConvertModule(const char* /*lib_name*/) const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConfiguration(const VeriConfiguration &mod, VeriMapForCopy &id_map_table); // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriConfiguration(SaveRestore &save_restore) ;

    virtual ~VeriConfiguration() ;

private:
    // Prevent compiler from defining the following
    VeriConfiguration() ;                                     // Purposely leave unimplemented
    VeriConfiguration(const VeriConfiguration &) ;            // Purposely leave unimplemented
    VeriConfiguration& operator=(const VeriConfiguration &) ; // Purposely leave unimplemented
public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ;
    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Internal routine to find module names which can be treated as instantiated

    virtual VeriModule *        CreateUniqueUnit(const Map *actual_params, const VhdlScope * /*vhdl_scope*/, VhdlScope * /*this_scope*/) ;

    virtual VeriPseudoTreeNode *CreateTreeRoot(Map *roots, Set &done) ; // Create root node(s) for this configuration and insert into the given Array return one of the nodes created
    virtual void                ExpandTree(ModuleVisit &visit, Map *roots, Set *done) ;    // Expand the tree by creating the partial tree first and then creating the full hierarchy on it

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            StaticElaborateInternalToGetUnits(Map *actual_params, Array *created_units, unsigned from_vhdl, unsigned has_path_name) ; // static elaborate a module without copying itself:
#endif

protected:
    Array           *_top_modules ;   // Array of VeriName * (top level module names)
    Array           *_config_rules ;  // Array of VeriConfigRule *
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConfiguration

/* -------------------------------------------------------------- */

/*
   Module for System Verilog:
   VIPER #5710: (vcs-compatibility: always_checker) A new buliding block that specially
   encapsulate assertion items. Checker is (since SV P1800(2009) LRM) stored in the
   Module design space and should thus be derived from VeriModule, otherwise they will
   be rejected by the VeriLibrary::AddModule() routine.
*/
class VFC_DLL_PORT VeriChecker  : public VeriModule
{
public:
    VeriChecker(VeriIdDef *id, Array *port_connects, Array *items, VeriScope *scope) ;
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERICHECKER ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsChecker() const             { return 1 ; }  // Affirms that this module item is a VeriChecker

    // Parse Tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy interface as module item
    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const ; // Copy interface as module

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriChecker(const VeriChecker &checker, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriChecker(SaveRestore &save_restore) ;

    virtual ~VeriChecker() ;

private:
    // Prevent compiler from defining the following
    VeriChecker() ;                               // Purposely leave unimplemented
    VeriChecker(const VeriChecker &) ;            // Purposely leave unimplemented
    VeriChecker& operator=(const VeriChecker &) ; // Purposely leave unimplemented
public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Internal routine for Static Elaboration :
    // VIPER #7641 : Can be generate item
    //virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item
protected:
    // All members of VeriModule are used.

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriChecker

/* -------------------------------------------------------------- */

/*
   Modules for System Verilog
   Interface and Program are (since SV 3.1a) stored in the Module design space and should thus be derived from VeriModule,
   otherwise they will be rejected by the VeriLibrary::AddModule() routine.
*/
class VFC_DLL_PORT VeriInterface  : public VeriModule
{
public:
    VeriInterface(VeriIdDef *id, Array *parameter_connects, Array *port_connects, Array *items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIINTERFACE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

//    virtual void              Resolve(VeriScope *scope, veri_environment environment) ;

    // Local/Virtual Accessor Methods to parse tree sub-nodes
// All these taken care of in VeriModule :
//    virtual VeriIdDef *         GetId() const                   { return _id ; }
//    Array *                     GetParameterConnects() const    { return _parameter_connects ; }
//    Array *                     GetPortConnects() const         { return _port_connects ; }
//    virtual Array *             GetItems() const                { return _items ; }
//    virtual VeriScope *         GetScope() const                { return _scope ; }
//    // Ordered list of locally declared IdDef's :
//    virtual Array *             GetPorts() const                { return _ports ; }
//    virtual Array *             GetParameters() const           { return _parameters ; }
    virtual unsigned            IsInterface() const             { return 1 ; }  // Affirms that this module item is a VeriInterface

    // Parse Tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy interface as module item
    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const ; // Copy interface as module

    virtual VhdlPrimaryUnit*    ConvertModule(const char* /*lib_name*/) const { return 0 ;}

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriInterface(const VeriInterface &intrfc, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriInterface(SaveRestore &save_restore) ;

    virtual ~VeriInterface() ;

private:
    // Prevent compiler from defining the following
    VeriInterface() ;                                 // Purposely leave unimplemented
    VeriInterface(const VeriInterface &) ;            // Purposely leave unimplemented
    VeriInterface& operator=(const VeriInterface &) ; // Purposely leave unimplemented
public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Internal routines for Static Elaboration :
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item
    virtual void                CheckModportExportImport() const ; // VIPER #7915

protected:
    // All members of VeriModule are used.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriInterface

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriProgram  : public VeriModule
{
public:
    VeriProgram(VeriIdDef *id, Array *parameter_connects, Array *port_connects, Array *items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIPROGRAM ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

//    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; //

    // Local/Virtual Accessor Methods to parse tree sub-nodes :
// All these taken care of in VeriModule :
//    virtual VeriIdDef *         GetId() const                   { return _id ; } // The identifier of this construct (the name).
//    Array *                     GetParameterConnects() const    { return _parameter_connects ; } // Array of VeriParamDecl (a VeriModuleItem). The parameters.
//    Array *                     GetPortConnects() const         { return _port_connects ; } // Array of VeriAnsiPortDecl (a VeriExpression). The ports.
//    virtual Array *             GetItems() const                { return _items ; } // Array of VeriModuleItem. The contents of this construct.
//    virtual VeriScope *         GetScope() const                { return _scope ; } // The scope (with locally declared identifiers) of this construct
//    // Ordered list of locally declared IdDef's :
//    virtual Array *             GetPorts() const                { return _ports ; }
//    virtual Array *             GetParameters() const           { return _parameters ; }
    virtual unsigned            IsProgram() const               { return 1 ; }  // Affirms that this module item is a VeriProgram
    virtual unsigned            IsAnonymousProgram() const ; // VIPER #4701 : Return 1 for anonymous program

    // Parse tree copy routines :
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy program as module item
    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const ; // Copy program as module

    // Specific parse-tree manipulation routines // Use default from VeriModule :
//    virtual unsigned            ReplaceChildModuleItem(VeriModuleItem *old_item, VeriModuleItem *new_item) ; // Delete 'old_item' from parameter port list/Package items, and puts new item in its place

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriProgram(const VeriProgram &program, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriProgram(SaveRestore &save_restore) ;

    virtual ~VeriProgram() ;

private:
    // Prevent compiler from defining the following
    VeriProgram() ;                               // Purposely leave unimplemented
    VeriProgram(const VeriProgram &) ;            // Purposely leave unimplemented
    VeriProgram& operator=(const VeriProgram &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Internal routines for Static Elaboration
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item

    virtual VhdlPrimaryUnit*    ConvertModule(const char* /*lib_name*/) const { return 0 ;}

protected:
    // All members of VeriModule are used.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriProgram

/* -------------------------------------------------------------- */

/*
   Std 1800 addition to represent System Verilog Package declaration
     package foo .... endpackage
   Packages in SV 1800 should actually be stored in their own 'package' namespace.
   However, both VCS and ModelSim store them next to modules in the module-definitions name space.
   That means they end up in the (work) library, and there cannot be name collisions between packages and modules.
   That is no problem (as long as the simulators keep on behaving like this),
   and it does make modeling a bit easier for us. No new API routines for package name space.
   We do need to be carefull not to designate packages as top-level modules and other
   checks where there is a semantic difference between modules and packages.
*/
class VFC_DLL_PORT VeriPackage  : public VeriModule
{
public:
    VeriPackage(VeriIdDef *id, Array *package_items, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIPACKAGE ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

//    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; // run default VeriModule::Resolve

    // Local/Virtual Accessor Methods to parse tree sub-nodes :
    // derived from VeriModule
//    virtual VeriIdDef *         GetId() const                   { return _id ; } // The identifier of this construct (the name).
//    virtual Array *             GetItems() const                { return _items ; } // Array of VeriModuleItem. The contents of this construct.
//    virtual VeriScope *         GetScope() const                { return _scope ; } // The scope (with locally declared identifiers) of this construct
    virtual unsigned            IsPackage() const               { return 1 ; }  // Affirms that this module item is a VeriPackage

    // Parse tree copy routines
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const ; // Copy Package as module item
//    virtual VeriModuleItem *    CopyWithName(const char *name, VeriMapForCopy &id_map_table, unsigned add_copied_obj) ; // Copy module item with the first argument specific name. Use VeriModule::CopyWithName
    virtual VeriModule *        CopyModule(VeriMapForCopy &id_map_table) const ; // Copy a package as module

    // Specific parse-tree manipulation routines // Use default from VeriModule :
//    virtual unsigned            ReplaceChildModuleItem(VeriModuleItem *old_item, VeriModuleItem *new_item) ; // Delete 'old_item' from parameter port list/Package items, and puts new item in its place

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlPrimaryUnit*    ConvertModule(const char *lib_name) const ;  // VIPER #6896: Convert verilog package to vhdl package
#endif

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriPackage(const VeriPackage &Package, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriPackage(SaveRestore &save_restore) ;

    virtual ~VeriPackage() ;

private:
    // Prevent compiler from defining the following
    VeriPackage() ;                               // Purposely leave unimplemented
    VeriPackage(const VeriPackage &) ;            // Purposely leave unimplemented
    VeriPackage& operator=(const VeriPackage &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Internal routines for Static Elaboration
    virtual void                StaticElaborateGenerate(VeriElabGenerate * /*gen_obj*/, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/) { } // Cannot be generate item

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VeriIdDef *         GetMatchingTypeId(const char* type_name) const ; // Viper 6896 : Collect the matching type id by matching id_name
#endif

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl package to verilog one: Fill up the verilog ranges
    // and initial value of constants during elaboration:
    virtual void                FillRangesAndInitialExpr() ;
#endif

protected:
// Use fields in VeriModule
//    VeriIdDef   *_id ;                 // The identifier of this construct (the name).
//    Array       *_items ;              // Array of VeriModuleItem. The contents of this construct.
//    VeriScope   *_scope ;              // The scope (with locally declared identifiers) of this construct
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriPackage

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_MODULE_H_
