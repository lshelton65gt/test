/*
 *
 * [ File Version : 1.405 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriVarUsage.h"

#include "RuntimeFlags.h"
#include "Strings.h"
#include "Message.h"
#include "Array.h"
#include "BitArray.h"
#include "Map.h"
#include "Set.h"

#include "veri_file.h"
#include "VeriLibrary.h"

#include "VeriId.h"
#include "VeriModule.h"
#include "VeriExpression.h"
#include "VeriMisc.h"
#include "VeriStatement.h"
#include "VeriScope.h"

#include "veri_tokens.h"
#include "VeriConstVal.h"
#include "VeriBaseValue_Stat.h"

// VIPER #6701 : Compile flag to print debug information about the
// variables which are discarded as dual-port/multi-port ram from
// 'ResetRamVariablesInsideGenerates' API.
// These variables are declared inside some generate construct and
// referrenced using unresolved hierarchical names.
// #define VERI_PRINT_DISCARDED_RAM_VARIABLES

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
#include "../vhdl/VhdlIdDef.h"
#endif
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/vhdl_file.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// This enumerated type represents the state of the control flow as it is being traversed.
// Values passed as 'action' into VarUsage routines for VeriStatements/ModuleItems.
// VALUE_IGNORED is the return type from top-level concurrent statements which is always ignored
// Don't use a true enumeration type, since then we need to include its definition in all Verilog header files.
#define NONE 0
#define DEAD 1
#define ALIVE 2
#define VALUE_IGNORED 3

// This enumerated type represents the current state of the expression tree as it is being traversed.
// Values passed as 'action' into VarUsage routines for VeriExpression.
// Don't use a true enumeration type, since then we need to include its definition in all Verilog header files.
// FIX ME, FIX ME : Should really use 'env_type' for this, or somewhat refined form of it.
#define USING 4
#define PARTIAL_USING 5
#define BLOCKING_ASSIGNING 6
#define PARTIAL_BLOCKING_ASSIGNING 7
#define NONBLOCKING_ASSIGNING 8
#define PARTIAL_NONBLOCKING_ASSIGNING 9
#define PORT_CONNECTION 10
#define TYPE_USAGE 11   // added for resolving type expressions.
#define SUBPROG_NAME 12 // added for resolving subprogram names.
#define EVENT_USAGE 13 // added for resolving event expressions.
#define CONSTANT 14 // added to check for constant check
#define CLOCKING_DRIVE 15 // added to check assignment to clocking variable
#define CYCLE_DELAY 16 // added to check cycle delay bound
#define CYCLE_DELAY_RBOUND 17 // added to check use of '$'
#define INSTANCE_ARG 18  // added to check actual of seq/prop instance
#define DISABLE_RESET 19 // added to check reset expression of disable
#define SEQ_ENDED 20 // added to check sequence instance on which ended is applied
#define CONSTRAINT_DECL 21 // added to check sequence instance on which ended is applied
#define CONTRIBUTION_LVALUE 22 // added to check lhs of ams contribution statement
#define SYSTASKCALL_ARG 23 // added ignore all ram inference decision
#define PROCEDURAL_CONTINOUS_ASSIGN 24 // added to report error for illegal func args
#define GENVAR_ASSIGN 25 // added to find illegal use for genvar
#define FORGEN_INIT 26 // to identify for-generate initialization

////////////////////////////////////////////////////////////////////
//       Global handling to detect whether we are inside loop or not  (3736)
////////////////////////////////////////////////////////////////////
// This static variable will keep track whether we are inside loop or not. If we
// are inside loop statement and indexed identifier is used within loop, that
// identifier cannot be considered as dual port RAM. We cannot propagate this
// information via VarUsageInfo class, as for generate-for no VarUsageInfo class
// is created. For this instead of adding an argument to VarUsage routine, we are
// using this global.
/* static */ unsigned VeriVarUsageInfo::_is_inside_loop = 0 ;
/* static */ void     VeriVarUsageInfo::InitializeWithinLoop() { _is_inside_loop = 0 ; }
/* static */ void     VeriVarUsageInfo::PushWithinLoop()       { _is_inside_loop++ ; }
/* static */ void     VeriVarUsageInfo::PopWithinLoop()        { _is_inside_loop-- ; }
/* static */ unsigned VeriVarUsageInfo::IsWithinLoop()         { return _is_inside_loop ; }

/////////////////////////////////////////////////////////////////////
//       Global handling to ignore already processed prefix (VIPER #6701)
////////////////////////////////////////////////////////////////////
/* static */ Set *VeriVarUsageInfo::_visited_selected_names = 0 ;
/* static */ unsigned VeriVarUsageInfo::InitializeVisitedSelectedNames()                { if (_visited_selected_names) return 0 ; _visited_selected_names = new Set(POINTER_HASH) ; return 1 ; }
/* static */ void     VeriVarUsageInfo::DeleteVisitedSelectedNames()                    { delete _visited_selected_names ; _visited_selected_names = 0 ; }
/* static */ void     VeriVarUsageInfo::AddVisitedSelectedName(const VeriName *name)   { if (_visited_selected_names && name) (void) _visited_selected_names->Insert(name) ; }
/* static */ unsigned VeriVarUsageInfo::IsVisitedSelectedName(const VeriName *name)    { return (_visited_selected_names && _visited_selected_names->GetItem(name)) ? 1 : 0 ; }

/////////////////////////////////////////////////////////////////////
// Global handling to produce error for assignment from different
// process (VIPER #4241)
////////////////////////////////////////////////////////////////////
// Modes used for assigned ids
#define FULL_ASSIGNED 30
#define PARTIAL_ASSIGNED 31
Map *VeriVarUsageInfo::_assigned_ids_with_mode = 0 ;

// VIPER #7825: Following macros are no more required:
//#define SPECIAL_ALWAYS 32
//#define PROC_AREA 33
//#define CONCUR_AREA 34
Map *VeriVarUsageInfo::_assigned_ids_with_context = 0 ;

// We will store the assigned identifiers along with its context and other info.
// We will handle only full assignment here. No error will be produced for partially
// assigned variables (with or without overlap in multiple assignments)
void VeriVarUsageInfo::SetAssignedInfoForVars(VeriIdDef *id, unsigned assign_mode, const VeriTreeNode *context)
{
    if (!VeriNode::IsStaticElab()) return ; // Do the check only for static elaboration
    if (!id || !context) return ;
    if (!_assigned_ids_with_mode) _assigned_ids_with_mode = new Map(POINTER_HASH) ;
    if (!_assigned_ids_with_context) _assigned_ids_with_context = new Map(POINTER_HASH) ;

    // Check what type of assignment is current one
    unsigned present_concurrent = (!context->GetSpecialAlwaysToken() && !context->IsProceduralArea()) ? 1 : 0 ;
    unsigned present_special_always = (context->GetSpecialAlwaysToken()) ? 1 : 0 ;

    MapItem *prev_item = _assigned_ids_with_context->GetItem(id) ;
    if (prev_item) {
        const VeriTreeNode *prev_context = (const VeriTreeNode *)prev_item->Value() ;
        VERIFIC_ASSERT(prev_context) ; // VIPER #7825: Must hold since we inserted it after NULL check
        // One assignment is from special always and there is another assignment too
        // Cases that can have :
        // 1. One assignment partial, other full
        // 2. Both are partial
        // 3. Both are full
        // We cannot produce error if both are partial, as we do not know
        // the range. So we can produce error for case (1) and (3)
        unsigned prev_mode = (unsigned)(unsigned long)_assigned_ids_with_mode->GetValue(id) ;
        if ((prev_mode == PARTIAL_ASSIGNED) && (assign_mode == PARTIAL_ASSIGNED)) {
            // Not error
        } else {
            // Check what type of assignment was previous one
            unsigned prev_concurrent = (!prev_context->GetSpecialAlwaysToken() && !prev_context->IsProceduralArea()) ? 1 : 0 ;
            unsigned prev_special_always = (prev_context->GetSpecialAlwaysToken()) ? 1 : 0 ;

            // If any context is concurrent area, produce warning
            if (prev_concurrent || present_concurrent) {
                if (VeriNode::IsSystemVeri() && !id->IsLocalVar() && !id->IsNet() && !id->IsMember() && !id->IsModportPort() && !id->IsInterfacePort()) {
                    // Do not produce warning for class object
                    VeriTypeInfo *data_type = id->CreateType(0) ;
                    if (!data_type || (!data_type->IsTypeParameterType() && !data_type->IsUnresolvedNameType() && !data_type->IsClassType())) {
                        context->Warning("variable %s might have multiple concurrent drivers",id->Name()) ;
                        prev_context->Info("another %s driver of %s is from here", "concurrent", id->Name()) ; // VIPER #7825
                    }
                    delete data_type ;
                }
            } else if (prev_special_always || present_special_always) {
                // If any assignment is from special always, produce warning
                if (VeriNode::IsSystemVeri() && !id->IsLocalVar() && !id->IsMember() && !id->IsModportPort() && !id->IsInterfacePort()) {
                    // Do not produce warning for class object
                    VeriTypeInfo *data_type = id->CreateType(0) ;
                    if (!data_type || (!data_type->IsTypeParameterType() && !data_type->IsUnresolvedNameType() && !data_type->IsClassType())) {
                        const VeriTreeNode *special_always_node = context ;
                        const VeriTreeNode *other_node = prev_context ;
                        if (prev_special_always) {
                            special_always_node = prev_context ;
                            other_node = context ;
                        }
                        // VIPER #7825: Produce more specific/elaborate warning/info about this multiple drivers:
                        id->Warning("variable %s is driven by invalid combination of procedural drivers", id->Name()) ;
                        special_always_node->Warning("%s driven by this %s block should not be driven by any other process",
                                                     id->Name(), VeriNode::PrintToken(special_always_node->GetSpecialAlwaysToken())) ;
                        other_node->Info("another %s driver of %s is from here", "procedural", id->Name()) ;
                    }
                    delete data_type ;
                }
            }
        }
        // Overwrite entries if current assignment is full assignment and from always_comb/ff/latch
        if (assign_mode == FULL_ASSIGNED) (void) _assigned_ids_with_mode->Insert(id, (void*)(unsigned long)assign_mode, 1 /* overwrite*/) ;
        if (context->GetSpecialAlwaysToken()) (void) _assigned_ids_with_context->Insert(id, context, 1 /* overwrite*/) ; // VIPER #7825: Insert the node
    } else {
        (void) _assigned_ids_with_mode->Insert(id, (void*)(unsigned long)assign_mode) ;
        (void) _assigned_ids_with_context->Insert(id, context) ; // VIPER #7825: Insert the node
    }
}
void VeriVarUsageInfo::ResetAssignedInfo()
{
    if (!VeriNode::IsStaticElab()) return ; // Do the check only for static elaboration
    delete _assigned_ids_with_mode ;
    _assigned_ids_with_mode = 0 ;
    delete _assigned_ids_with_context ;
    _assigned_ids_with_context = 0 ;
}
////////////////////////////////////////////////////////////////////
//       Global handling to detect whether we are in argument of sampled
//       value functions or not (7834 and 7082)
////////////////////////////////////////////////////////////////////
// This static variable will keep track whether we are in argument of sampled
// value functions or not. If we are in argument of sampled value functions,
// we will produce error if local variables or sequence method triggered/matched
// are used there. We cannot propagate this information via VarUsageInfo class, as for
// continuous assignment no VarUsageInfo class is created.
/* static */ unsigned VeriVarUsageInfo::_in_sampled_value_fn = 0 ;
/* static */ void     VeriVarUsageInfo::InitializeInSampledValueFunc() { _in_sampled_value_fn = 0 ; }
/* static */ void     VeriVarUsageInfo::PushInSampledValueFunc()       { _in_sampled_value_fn++ ; }
/* static */ void     VeriVarUsageInfo::PopInSampledValueFunc()        { _in_sampled_value_fn-- ; }
/* static */ unsigned VeriVarUsageInfo::IsInSampledValueFunction()     { return _in_sampled_value_fn ; }
////////////////////////////////////////////////////////////////////
//        Methods for VeriVarUsage
////////////////////////////////////////////////////////////////////

// AnalyzeFull does analysis of a module after all modules are read in.
// This is the first time that we can fully analyze module instantiations,
// hierarchical names, and (SV) type expressions.
//
// Historically this routine only did analyze variable/net usage and assignments.
// It traverses the parse tree from 'Starting point' of a VeriModule,
// because VeriModule is the one and only independent parse tree structure in the parser:
// VeriModule is stored in a Verilog library rather than inside another parse tree.
//
// Variable usage analyis runs over the module, and analyzes usage and assignments of variables
// as they occur in sequential dataflows, and in the rest of Verilog module items (concurrent statements).
//
// This traversal keeps track of mutually-exclusivly executed flows, so that
// certain properties (of identifiers) and flow-related semantic checks can be done :
//   - Dual Port RAM recognition : only 1 simultanious read and 1 simultanious write allowed
//   - SV concurrent assignments to 'variables' (only one concurrent assignment allowed)
//   - Set IsConcurrentAssigned() flag for vars/nets to indicate that id is changed after initialization.
//   - Set IsConcurrentUsed() flag for vars/nets to indicate that id is used after initialization.
//
// AnalyzeFull() is NOT re-entrant, since it depends on flag settings made in ids (to count #of assignments/usages).
// AnalyzeFull() should therefore only be called once on a parse tree node
// (typically after Resolve() ran to resolve the id references).
// A flag on the VeriModule assures that this routine does not accidentally get executed multiple times.
//

void
VeriModule::AnalyzeFull()
{
    // If extern module remains in parse tree (for which no actual def exists), do not process that
    if (GetQualifier(VERI_EXTERN)) return ;

    // First, block re-entrance :
    SynthesisInfo *this_info = 0 ;
    if (!this_info && _is_analyzed) return ;

    if (RuntimeFlags::GetVar("veri_extract_multiport_rams_for_structures")) InlineDeclDataTypes() ; // VIPER #7277

    // VIPER #6580: Set analysis mode only when we are going to "VarUsage" this module.
    // VeriModule::Elaborate properly set/reset the analysis mode from within itself.
    // VIPER 3583 : Set analysis mode even when this module is already processed
    // here. This is needed as elaboration uses 'Resolve' routine which needs this
    // mode setting to resolve properly.
    // VIPER issue 2802 and 2787 :
    // Full analysis requires that we know the dialect under which this module was analyzed. (we call IsSystemVeri() in this routine)
    // Set dialect here, based on the field in the module :
    unsigned prev_analysis_dialect = veri_file::GetAnalysisMode() ; // VIPER #6580: Also push/pop the mode here
    veri_file::SetAnalysisMode(_analysis_dialect) ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    if (!this_info) _is_analyzed = 1 ;

    unsigned old_loop_depth = VeriVarUsageInfo::IsWithinLoop() ;
    VeriVarUsageInfo::InitializeWithinLoop() ; // Reset global

    unsigned old_sampled_value_fn_depth = VeriVarUsageInfo::IsInSampledValueFunction() ;
    VeriVarUsageInfo::InitializeInSampledValueFunc() ; // Reset global (VIPER #7834/7082)

    // VIPER #6701 : Set to store processed selected_names
    unsigned is_set_created = VeriVarUsageInfo::InitializeVisitedSelectedNames() ;

    // VIPER #4241 : Reset stored assignment information
    VeriVarUsageInfo::ResetAssignedInfo() ;

    // Keep track of the error count.
    // If it increases during this run, then flag this module as a black-box.
    // Otherwise, the module will go through full elaboration with semantic errors in there.
    unsigned error_count = Message::ErrorCount() ;

    // Now begin traversal :
    // VIPER #6236: Resolve the ANSI parameter declarations:
    unsigned i;
    VeriDataDecl *param_decl ;
    FOREACH_ARRAY_ITEM(_parameter_connects, i, param_decl) {
        if (!param_decl) continue ;
        (void) param_decl->VarUsage(0, NONE) ;
    }

    unsigned is_chcker = IsChecker() ; // In checker input/inout port association is like blocking assignment
    // Traverse the ports :
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ports, i, id) {
        if (!id) continue ;

        // Discard module ports as a dual-port/multi-port memory.
        id->SetCannotBeDualPortRam(this_info) ;

        if (id->IsInput() || id->IsInout()) {
            // If 'id' is for non-ansi port, continue
            VeriExpression *actual = id->GetActual() ;
            actual = actual ? actual->GetConnection(): 0 ;
            if (actual) continue ;
            // inputs are (non-blocking) assigned (from outside) :
            id->VarUsage(0, is_chcker ? BLOCKING_ASSIGNING : NONBLOCKING_ASSIGNING, 0) ;
        }

        if (id->IsOutput() || id->IsInout()) {
            // outputs are used (outside) :
            id->VarUsage(0, USING, 0) ;
        }
    }

    // Resolving work :
    // Resolve the ANSI ports (interface ports data types might not have resolved types yet)
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (!port_expr) continue ;
        // Viper #4966 : Do VarUsage with  USING even for PortConnects
        // VarUsage with PORT_CONNECTION might generate spurious multi
        // driver warning from VarUsage
        // if (!port_expr->IsAnsiPortDecl()) continue ; // only do something for ansi port decls.
        port_expr->VarUsage(0, USING) ;
    }

    // Traverse module items
    // Viper 4840 : store value of flag to be restored later
    unsigned saved_under_default_clock = _under_default_clock ;
#if 0
    verific_int64 time_unit_lit = 0 ;
    verific_int64 time_precision_lit = 0 ;
    // number of timeunits and timeprecisions present
    unsigned timeunits_present = 0 ;
    unsigned timeprecisions_present = 0 ;
#endif
    // VIPER #5248 (test_25): Find the 'table' if this module is a primitive:
    VeriTable *table = (IsPrimitive()) ? GetPrimitiveTable() : 0 ;
    VeriModuleItem *mi;
    FOREACH_ARRAY_ITEM(_module_items, i, mi) {
        // Donot process sequence/property declaration here. Will process from
        // sequence/property instance :
        // VIPER #6408: Ignore Let declarations also, we will call them with usage:
        if (!mi || (mi->IsSequenceDecl() || mi->IsPropertyDecl() || mi->IsLetDecl())) continue ;
        // VIPER #5248 (test_25): Do not run var-usage on 'initial' block in combinational udps:
        // Note: Bison only allows data-decls, (single) table and (single) initial block in udps.
        if (table && table->IsCombinational() && !mi->IsDataDecl() && (mi!=table)) continue ;
        // Viper 4666. Module instantiations are not allowed inside interface declaration.
        VeriModule *mod =  mi->GetInstantiatedModule() ;
        if (IsInterface() && mod && mod->IsModule() && !mod->IsInterface() && !mod->IsProgram())  {
            mi->Error("module instantiation of %s is not allowed in interface %s", mod->GetName(), GetName()) ;
        }
#if 0
        if (mi->IsTimeUnitDecl()) {
            // Viper 4807:
            // 1. timeunit declaration after a statement declaration is error // THis is checked in VeriModule::AddModuleItem
            // 2. setting time precision larger than the time unit is error
            // 3. more then one time unit declaration is error
            // 4. more then one time precession declaration is error
            // 5. time unit /time precession not in multiples of 10 is error

            VeriExpression *literal = mi->GetTimeLiteral() ;
            if (literal) {
                verific_int64 time_value = 0 ;
                if (IsStaticElab()) {
                    VeriBaseValue *bval = literal->StaticEvaluateInternal(0,0,0, 0) ;
                    time_value = bval ? bval->Get64bitInteger() : 0 ;
                    delete bval ;
                }
                if (mi->GetTimeUnitType()==VERI_TIMEUNIT) {
                    if (timeunits_present && (time_unit_lit != time_value)) {
                        mi->Error("new timescale value (%s) conflicts with an existing value", PrintToken(VERI_TIMEUNIT)) ;
                    } else {
                        timeunits_present++ ;
                        time_unit_lit = (time_value) ? time_value : 0 ;
                    }
                    // VIPER #8304 : Consider time precision from time-unit declaration
                    VeriExpression *time_precision = mi->GetTimePrecision() ;
                    if (time_precision) {
                        verific_int64 time_precision_value = 0 ;
                        if (IsStaticElab()) {
                            VeriBaseValue *bval = time_precision->StaticEvaluateInternal(0,0,0, 0) ;
                            time_precision_value = bval ? bval->Get64bitInteger() : 0 ;
                            delete bval ;
                        }
                        if (timeprecisions_present && (time_precision_lit != time_precision_value)) {
                            mi->Error("new timescale value (%s) conflicts with an existing value", PrintToken(VERI_TIMEPRECISION)) ;
                        } else {
                            timeprecisions_present++ ;
                            time_precision_lit = time_precision_value ? time_precision_value: 0 ;
                        }
                    }
                }
                if (mi->GetTimeUnitType()==VERI_TIMEPRECISION) {
                    if (timeprecisions_present && (time_precision_lit != time_value)) {
                        mi->Error("new timescale value (%s) conflicts with an existing value", PrintToken(VERI_TIMEPRECISION)) ;
                    } else {
                        timeprecisions_present++ ;
                        time_precision_lit = (time_value) ? time_value : 0 ;
                    }
                }
                if (time_unit_lit && time_precision_lit && (time_unit_lit < time_precision_lit)) {
                    mi->Error("error in timescale or timeprecision statement. <timeprecision> must be at least as precise as <timeunit>") ;
                    //continue ; // VIPER #6986: This 'continue' is not required any more since we moved the following to checks elsewhere
                }
                // VIPER #6986: Moved the following to checks in constructor of VeriTimeUnit (in analyzer).
                // Viper 5412. Issue error even if only timeunit or timeprecision is present
                //if (time_unit_lit) {
                //    verific_int64 prev_time_unit_lit = time_unit_lit ;
                //    while (time_unit_lit % 10 == 0) time_unit_lit /= 10 ;
                //    if (time_unit_lit != 1) mi->Error("%s literal not a power of 10", "timeunit") ;
                //    time_unit_lit = prev_time_unit_lit ;
                //}
                //if (time_precision_lit) {
                //    verific_int64 prev_time_precision_lit = time_precision_lit ;
                //    while (time_precision_lit % 10 == 0) time_precision_lit /= 10 ;
                //    if (time_precision_lit != 1) mi->Error("%s literal not a power of 10", "timeprecision") ;
                //    time_precision_lit = prev_time_precision_lit ;
                //}
            }
        }
#endif

#ifdef VERILOG_DYNAMIC_VARUSAGE_ANALYSIS
        if (mi->IsTaskDecl()) continue ; // Viper #4060: varusage on func/task decl done only when it is called
#endif // VERILOG_DYNAMIC_VARUSAGE_ANALYSIS
        (void) mi->VarUsage(0, NONE) ;
    }

    // Viper 4840
    _under_default_clock = saved_under_default_clock ;

    // If an error showed up, then we will flag that in the module.
    // This will allow elaboration (or other routines) to skip this module.
    // VIPER issue 2599.
    if (error_count < Message::ErrorCount()) {
        SetHasAnalyzeError(this_info) ;
    }
    VeriVarUsageInfo::InitializeWithinLoop() ; // Reset global
    VeriVarUsageInfo::InitializeInSampledValueFunc() ; // Reset global (VIPER #7834/7082)

    _present_scope = save_scope ;

    // Reset global (VIPER #6701)
    if (is_set_created) VeriVarUsageInfo::DeleteVisitedSelectedNames() ;

    // VIPER #6580: Reset to the previous analysis mode here:
    veri_file::SetAnalysisMode(prev_analysis_dialect) ;

    for (; old_loop_depth > 0 ; --old_loop_depth) VeriVarUsageInfo::PushWithinLoop() ;

    for (; old_sampled_value_fn_depth > 0 ; --old_sampled_value_fn_depth) VeriVarUsageInfo::PushInSampledValueFunc() ;

    // VIPER #4241 : Reset stored assignment information
    VeriVarUsageInfo::ResetAssignedInfo() ;
}

unsigned
VeriFunctionDecl::VarUsage(VeriVarUsageInfo * /*info*/, unsigned /*action*/)
{
    // If this is definition of exported function, its body is not resolved in 'Resolve'
    // If it is still unelaborated exported function, resolve it now
    if (_name && _name->IsHierName()) {
        // If prefix is interface port/instance, it is exported function
        VeriName *prefix = _name->GetPrefix() ;
        VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
        if (prefix_id && (prefix_id->IsInterfacePort() || prefix_id->IsInst())) {
            char *tmp_name = Strings::save(prefix_id->Name(),  " ", _name->GetSuffix()) ;
            if (_func && Strings::compare(tmp_name, _func->Name())) {
                Resolve(0, VERI_UNDEF_ENV) ; // exported function, resolve its body
            }
            Strings::free(tmp_name) ;
        }
    }

    // Get the class if exist
    VeriIdDef *class_id = (_scope) ? _scope->GetClass(): 0 ;
    VeriModuleItem *class_body = (class_id) ? class_id->GetModuleItem() : 0 ;
    unsigned is_virtual_class =  class_body && class_body->GetQualifier(VERI_VIRTUAL) ? 1 : 0 ;

    // Viper 4869 report error in extern virtual function does not have body
    VeriModuleItem *body = _func ? _func->GetModuleItem() : 0 ;
    if (_func && body && body->GetQualifier(VERI_EXTERN) && body->GetQualifier(VERI_VIRTUAL) && !body->GetQualifier(VERI_FORKJOIN) && !is_virtual_class) {
        // Check whether it is inside package 'std'
        unsigned inside_std_pkg = 0 ;
        VeriScope *method_scope = body->GetScope() ;
        VeriIdDef *container_id = (method_scope) ? method_scope->GetContainingModule(): 0 ;
        if (container_id && Strings::compare(container_id->Name(), "std")) {
            VeriModule *container = container_id->GetModule() ;
            VeriLibrary *lib = (container) ? container->GetLibrary(): 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) {
                inside_std_pkg = 1 ;
            }
        }
        if (!inside_std_pkg && _verilog_method) Warning("method never defined for %s %s", "function", _func->Name()) ;
    }
    if (_name && _name->IsScopeName()) {
        VeriName *prefix = _name->GetPrefix() ;
        VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
        if (prefix_id && prefix_id->IsClass()) {
            // This is a out-of-block function declaration. No need to call VarUsage
            // as it is not called for functions declared within class
            return VALUE_IGNORED ;
        }
    }

    // FunctionDecl : process as totally separate dataflow, as if it executes only once in concurrent area.

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // Create new info table
    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo info(&id_table) ;
    info.SetInFunction() ;

    // VIPER #7475: VarUsage the data type to check undefined type-refs etc:
    if (_data_type) _data_type->VarUsage(&info, TYPE_USAGE) ;

    // Mark all inputs as having been assigned
    Array *pc_array = (_func) ? _func->GetPorts() : 0 ;
    unsigned i ;
    VeriIdDef *id;
    FOREACH_ARRAY_ITEM(pc_array, i, id) {
        if (!id) continue ;
        if (id->IsInput() || id->IsInout()) id->VarUsage(&info, BLOCKING_ASSIGNING, 0) ;
    }

    // Set action to 'active', sequential statement processing :
    unsigned action = USING ;

    // Traverse declarations
    // Could contain local variable declarations with initial value assignments :
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        // VIPER #6408: Ignore Let declarations, we will call them with usage:
        if (!decl_item || decl_item->IsLetDecl()) continue ;
        action = decl_item->VarUsage(&info, action) ;
    }

    // Traverse statements
    VeriStatement *stmt;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) action = stmt->VarUsage(&info, action) ;
    }

    // mark all outputs as having been read
    FOREACH_ARRAY_ITEM(pc_array, i, id) {
        if (!id) continue;
        if (id->IsOutput() || id->IsInout()) id->VarUsage(&info, USING, 0) ;
    }

    // RD: 7/2005: Verilog functions get assignments to function name,
    // and that means the function name behaves as a normal (local) variable.
    // The only thing special about the return value is that the identifier gets 'used' here,
    // just like an output formal. So flag that here.
    if (action!=DEAD && _func) _func->VarUsage(&info, USING, 0) ;

    // Now run typical 'update' cycle, as if this function is called from a cuncurrent area.
    // This is to indicate that the sequential data flow ended.
    info.UpdateConcurrentVarStatus() ;

    _present_scope = save_scope ;

    return VALUE_IGNORED ; // return value is immaterial
}

/*--------------------------------------------------------------------------*/

unsigned
VeriTaskDecl::VarUsage(VeriVarUsageInfo *parent_info, unsigned /*action*/)
{
    // If this is definition of exported task, its body is not resolved during 'Resolve'
    // If it is  still unelaborated task decl, 'Resolve' it now
    if (_name && _name->IsHierName()) {
        // If prefix is interface port/instance, it is exported function
        VeriName *prefix = _name->GetPrefix() ;
        VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
        if (prefix_id && (prefix_id->IsInterfacePort() || prefix_id->IsInst())) {
            char *tmp_name = Strings::save(prefix_id->Name(),  " ", _name->GetSuffix()) ;
            if (_task && Strings::compare(tmp_name, _task->Name())) {
                //Resolve(0, VERI_UNDEF_ENV) ; // exported task, resolve its body
                Strings::free(tmp_name) ;
                return 1 ;
            }
            Strings::free(tmp_name) ;
        }
    }
    // Get the class if exist
    VeriIdDef *class_id = (_scope) ? _scope->GetClass(): 0 ;
    VeriModuleItem *class_body = (class_id) ? class_id->GetModuleItem() : 0 ;
    unsigned is_virtual_class =  class_body && class_body->GetQualifier(VERI_VIRTUAL) ? 1 : 0 ;

    // Viper 4869 report error in extern virtual function does not have body in a non virtual class
    VeriModuleItem *body = _task ? _task->GetModuleItem() : 0 ;
    if (_task && body && body->GetQualifier(VERI_EXTERN) && body->GetQualifier(VERI_VIRTUAL) && !body->GetQualifier(VERI_FORKJOIN) && !is_virtual_class) {
        // Check whether it is inside package 'std'
        unsigned inside_std_pkg = 0 ;
        VeriScope *method_scope = body->GetScope() ;
        VeriIdDef *container_id = (method_scope) ? method_scope->GetContainingModule(): 0 ;
        if (container_id && Strings::compare(container_id->Name(), "std")) {
            VeriModule *container = container_id->GetModule() ;
            VeriLibrary *lib = (container) ? container->GetLibrary(): 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) {
                inside_std_pkg = 1 ;
            }
        }
        if (!inside_std_pkg && body->IsVerilogMethod()) Warning("method never defined for %s %s", (_task->IsFunction()) ? "function":"task", _task->Name()) ;
    }
    if (IsStaticElab()) {
        // If this is external declaration of class method and the class is
        // not instantiated, do not process its body
        if (class_body && !class_body->IsClassInstantiated()) return VALUE_IGNORED ;
    }

    SynthesisInfo *this_info = 0 ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // Task Declaration is processed as a separate data flow segment, as if it executes once in concurrent area.
    // to calculate used/assigned info for every local variable.
    VeriVarUsageInfo *info = 0 ;
    Map id_table(POINTER_HASH) ;
    if (parent_info) {
        info = parent_info->Copy() ;
    } else {
        info = new VeriVarUsageInfo(&id_table) ;
    }

    info->ResetInConditionalGenerate() ;

    // Mark all inputs as having been assigned
    Array *pc_array = (_task) ? _task->GetPorts() : 0 ;
    unsigned i ;
    VeriIdDef *id;
    FOREACH_ARRAY_ITEM(pc_array, i, id) {
        if (!id) continue;

        id->ResetVarUsageInfo() ; // FIXME: Need to do it in SynthesisInfo too?
        switch (id->Dir()) {
        case VERI_INPUT  : id->SetAssigned(this_info) ; break ;
        case VERI_OUTPUT : id->SetUsed(this_info) ; break ;
        case VERI_INOUT  : id->SetUsed(this_info) ; id->SetAssigned(this_info) ; break ;
        default : break ;
        }

        if (id->IsInput() || id->IsInout()) id->VarUsage(info, BLOCKING_ASSIGNING, 0) ;
    }

    // Set action to 'active', sequential statement processing :
    unsigned action = USING ;

    // Traverse declarations
    // Could contain local variable declarations with initial value assignments :
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (decl_item) action = decl_item->VarUsage(info, action) ;
    }

    // Traverse task body
    VeriStatement *stmt;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) action = stmt->VarUsage(info, action) ;
    }

    // mark all outputs as having been read
    FOREACH_ARRAY_ITEM(pc_array, i, id) {
        if (!id) continue;
        if (id->IsOutput() || id->IsInout()) id->VarUsage(info, USING, 0) ;
    }

    // Now run typical 'update' cycle, as if this function is called from a cuncurrent area.
    // This is to indicate that the sequential data flow ended.
    info->UpdateConcurrentVarStatus() ;

    delete info ;
    _present_scope = save_scope ;

    return VALUE_IGNORED ; // return value immaterial
}

/*--------------------------------------------------------------------------*/

/* Begin VeriModuleItem derived objects:
class VeriModuleInstantiation
class VeriContinuousAssign
class VeriGateInstantiation
class VeriAlwaysConstruct
class VeriInitialConstruct  (need to test initial assignments)

class VeriFunctionDecl
class VeriTaskDecl
class VeriDataDecl
class VeriNetDecl
class VeriGenerateConstruct
class VeriGenerateConditional
class VeriGenerateCase
class VeriGenerateFor
class VeriGenerateBlock
class VeriStatement  (this has its own section in this file.)

NOT implemented:     -------------------------

    STD Verilog:
class VeriDefParam
class VeriTable
class VeriSpecifyBlock
class VeriSystemTimingCheck
class VeriPathDecl

    PSL:
class VeriSequenceDecl

    System Verilog:
class VeriClockingDecl
class VeriConstraintDecl
*/

unsigned
VeriModuleItem::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    // Do Nothing here.  All derived classes will define their own
    // specific version of VarUsage
    return action ;
}

unsigned
VeriClockingDecl::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    //Viper 4080: Mark for default clocking
    if (GetQualifier(VERI_DEFAULT) || (_id && _id->IsDefaultClocking())) _under_default_clock = 1 ;

    return action ;
}
unsigned
VeriClass::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    // VIPER #4404 : Do not process body of unelaborated class in static
    // elaboration. We can produce incorrect error.
    if (IsStaticElab() && !IsClassInstantiated()) return action ;

    // VIPER #5248 (test case: Amd_cnb_031808_block_echo_RH30_64).
    // If the base class is a type parameter or a typedef (which is declared but not
    // defined before the derived class) we did not resolve the class items in Resolve,
    // this has been done to suppress unwanted errors when base class members are used
    // in derived class. For all other type of base classes we already resolved
    // the class items. So, here, we should resolve items of those classes that are not
    // resolved in Resolve flow.
    //VeriIdDef *base_id = _base_class_name ? _base_class_name->GetId() : 0 ;
    //VeriModuleItem *base_class_item = base_id ? base_id->GetModuleItem() : 0 ;
    //if (!(!_base_class_name || (base_class_item && base_class_item->IsClass())))
    //if (_base_class_name && (!base_class_item || !base_class_item->IsClass())) {
    VeriTypeInfo *base_type = _base_class_name ? _base_class_name->CheckType(0, NO_ENV): 0 ;
    if (base_type && (base_type->IsTypeParameterType() || base_type->IsUnresolvedNameType())) {
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_items, i, item) {
            if (item) item->Resolve(_scope, VERI_UNDEF_ENV) ;
        }
    }
    delete base_type ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // Do Nothing here.  All derived classes will define their own
    // specific version of VarUsage

    Map rand_randc(POINTER_HASH) ;
    VeriModuleItem *curr_class = this ;
    curr_class->CollectRandRandCVars(&rand_randc) ;
    //while (curr_class) {
        // Viper 5134 Collect rand/randC variables from base class as well
        //Array *items = curr_class->GetItems() ;
        //curr_class->CollectRandRandCVars(items, &rand_randc) ;
        //VeriName *base_class_name = curr_class->GetBaseClassName() ;
        //VeriIdDef *base_class_id = base_class_name ? base_class_name->GetId() : 0 ;
        //VeriModuleItem *base_class = base_class_id ? base_class_id->GetModuleItem() : 0 ;
        //curr_class = base_class ;
    //}
    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo info(&id_table) ;
    info.SetRandRandCVars(&rand_randc) ;

    // IEEE 1800-2012 section : 8.26.2 : For non-abstract/non-interface class,
    // it is required to fully define the methods from their implemented classes
    // 8.26.7 : Virtual class should implement or redeclare methods from their
    // implemented classes.
    Map methods(STRING_HASH) ;
    PopulateToBeImplementedMethods(&methods, 0) ;
    unsigned is_virtual_class = GetQualifier(VERI_VIRTUAL) ? 1 : 0 ;
    unsigned is_interface_class = GetQualifier(VERI_INTERFACE) ? 1 : 0 ;
    Array *items = GetItems() ;
    VeriModuleItem *mi;
    unsigned i ;
    FOREACH_ARRAY_ITEM(items, i, mi) {
        // Donot process sequence/property declaration here. Will process from
        // sequence/property instance :
        // VIPER #6408: Ignore Let declarations also, we will call them with usage:
        if (!mi || (mi->IsSequenceDecl() || mi->IsPropertyDecl() || mi->IsLetDecl())) continue ;
        if (mi->IsConstraintDecl()) {
            (void) mi->VarUsage(&info, CONSTRAINT_DECL) ;
        } else {
            (void) mi->VarUsage(0, NONE) ;
        }
        // Viper #5232 : Produce error if interface type objects are declared inside class :
        if (mi->IsDataDecl() && !mi->IsTypeAlias() && !mi->GetQualifier(VERI_VIRTUAL)) {
            VeriDataType *data_type = mi->GetDataType() ;
            if (data_type && data_type->IsVirtualInterface()) continue ;
            VeriTypeInfo *tt = (data_type) ? data_type->CreateType(0):0 ;
            VeriIdDef *type_id = (tt) ? tt->GetNameId(): 0 ;
            if (type_id && (type_id->IsInterface() || type_id->IsModport()) && (tt && !tt->IsVirtualInterface())) {
                mi->Error("interface type objects cannot be declared within class") ;
            }
            delete tt ;
        }
        // Check whether proper method is implemented/redeclared in non-interface class
        if (!is_interface_class && (mi->IsFunctionDecl() || mi->IsTaskDecl())) {
            ValidateImplementedMethod(mi, &methods, is_virtual_class, is_virtual_class ? 0: 1) ;
        }
    }
    if (!is_interface_class && methods.Size()) {
        // Has unimplemented pure virtual methods, find in base class
        VeriName *base_class_name = _base_class_name ;
        while (base_class_name) {
            VeriTypeInfo *base_class_type = base_class_name->CheckType(0, NO_ENV) ;
            VeriIdDef *base_class_id = (base_class_type) ? base_class_type->GetNameId(): 0 ;
            delete base_class_type ;
            VeriModuleItem *base_class_item = base_class_id ? base_class_id->GetModuleItem() : 0 ;
            Array *class_items = base_class_item ? base_class_item->GetItems(): 0 ;
            unsigned is_base_class_virtual = base_class_item ? base_class_item->GetQualifier(VERI_VIRTUAL): 0 ;
            FOREACH_ARRAY_ITEM(class_items, i, mi) {
                if (!mi) continue ;
                if (mi->IsFunctionDecl() || mi->IsTaskDecl()) {
                    // Pick pure virtual method only for virtual class
                    if (!is_virtual_class && mi->GetQualifier(VERI_PURE)) continue ;
                    if (base_class_item) base_class_item->ValidateImplementedMethod(mi, &methods, is_base_class_virtual, is_virtual_class ? 0: 1) ;
                }
            }
            base_class_name = base_class_item ? base_class_item->GetBaseClassName(): 0 ;
        }
        if (methods.Size()) {
            // All pure virtual methods are not implemented, error
            char *method_name ;
            MapIter mii ;
            FOREACH_MAP_ITEM(&methods, mii, &method_name, 0) {
                Error("class %s must implement/redeclare %s from interface/base classes", _id ? _id->Name(): "", method_name) ;
            }
        }
    }

    _present_scope = save_scope ;

    return action ;
}

void
VeriClass::CollectRandRandCVars(Map *rand_randc)
{
    // First collect the rand and randc variables
    // Viper 4715. store the qualifier of functions in info->_rand_randc_vars
    // this is used in varusage of the argument of the dist operator
    VeriModuleItem *mi;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_items, i, mi) {
        if (mi && mi->IsDataDecl()) {
            Array *ids = mi->GetIds() ;
            unsigned j ;
            VeriIdDef *id ;
            unsigned isrand = mi->GetQualifier(VERI_RAND) ? 1 : 0 ;
            unsigned israndc = mi->GetQualifier(VERI_RANDC) ? 1 : 0 ;
            if (isrand || israndc) {
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    if (!id) continue ;
                    unsigned qualifier = isrand ? VERI_RAND : (israndc ? VERI_RANDC : 0) ;
                    if (!rand_randc->Insert(id, (void*)(unsigned long)qualifier)) return ;
                }
            }
        }
    }
    // Viper #5134 Collect rand/randC variables from base class as well
    VeriIdDef *base_class_id = _base_class_name ? _base_class_name->GetId() : 0 ;
    VeriModuleItem *base_class = base_class_id ? base_class_id->GetModuleItem() : 0 ;
    if (base_class) base_class->CollectRandRandCVars(rand_randc) ;
}

unsigned
VeriConstraintDecl::VarUsage(VeriVarUsageInfo * info, unsigned action)
{
    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    if (info) info->SetInConstraintDecl() ; // Mark info that constraint decl will be processed
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_constraint_blocks, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }

    if (info) info->SetInConstraintDecl(0) ; // Reset in constraint decl
    _present_scope = save_scope ;

    return action ;
}
void
VeriDistOperator::VarUsage(VeriVarUsageInfo * info, unsigned action)
{
    // only call VarUsage for arg if the dist operator is of a constraint
    // Viper 4715
    if (info && info->IsInConstraintDecl()) {
        // VIPER #5799 : Reset the rand/randc flags to check whether distribution
        // expression contains rand/randc variables :
        info->ContainsRand(0) ; // Reset the flags
        info->ContainsRandC(0) ; // Reset the flags
        if (_arg) _arg->VarUsage(info, action) ;
        if (!info->DoesContainRand()) Error("a distribution expression in constraint must contain at least one random variable") ;
        if (info->DoesContainRandC()) Error("randc variables cannot be used in distribution and solve-before") ;
        info->ContainsRand(0) ; // Reset the flags
        info->ContainsRandC(0) ; // Reset the flags
    }
}

void
VeriSolveBefore::VarUsage(VeriVarUsageInfo * info, unsigned action)
{
    // only call VarUsage for names in _solve_list and _before_list
    // Viper 4836
    if (action == CONSTRAINT_DECL && info) {
        // VIPER #5799 : Reset the randc flag before traversing solve/before list
        info->ContainsRandC(0) ;
        VeriName *name ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_solve_list, i, name) {
            // Viper 4271. Do not error out for name not a idref or indexedid
            if (!name || !name->GetId()) continue ;
            info->ContainsRand(0) ;
            name->VarUsage(info, action) ;
            // Viper 4997 error out if the variable in solve before is not rand
            if (!info->DoesContainRand()) Error("non rand variable %s cannot be used in solve before", name->GetName()) ;
        }
        FOREACH_ARRAY_ITEM(_before_list, i, name) {
            // Viper 4271. Do not error out for name not a idref or indexedid
            if (!name || !name->GetId()) continue ;
            info->ContainsRand(0) ;
            name->VarUsage(info, action) ;
            // Viper 4997 error out if the variable in solve before is not rand
            if (!info->DoesContainRand()) Error("non rand variable %s cannot be used in solve before", name->GetName()) ;
        }
        if (info->DoesContainRandC()) Error("randc variables cannot be used in distribution and solve-before") ;
        info->ContainsRand(0) ; // Reset the flags
        info->ContainsRandC(0) ; // Reset the flags
    }
}

/*--------------------------------------------------------------------------*/

unsigned
VeriModule::VarUsage(VeriVarUsageInfo * /*info*/, unsigned /*action*/)
{
    // Module coming as nasted module

    AnalyzeFull() ;

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

// VIPER #6508: Moved a VarUsage code for single port-connect to this routine.
// This helps call it on implicit port connections through dor-star.
unsigned
VeriModuleInstantiation::VarUsagePortConnect(VeriVarUsageInfo *info, unsigned pos, VeriExpression *actual, const VeriModule *module, void *void_unit, unsigned array_inst_dims, Set &connected_formals, VeriExpression **found_dot_star, const VeriIdDef *inst_id) const
{
    if (!actual) return 1 ;

    VeriIdDef *module_id = (module) ? module->GetId() : 0 ;
    Array *formals = (module) ? module->GetPorts() : 0 ;
    unsigned is_checker = (module && module->IsChecker()) ? 1 : 0 ;
    //VeriScope *formal_scope = (module) ? module->GetScope() : 0 ;
    (void) void_unit ; // Prevent compiler warning: unused parameter.
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    VhdlPrimaryUnit *vhdl_unit = (VhdlPrimaryUnit *)void_unit ;
#endif

    // Skip DotStar. We will re-visit it later after we are done with all normal port connections.
    if (actual->IsDotStar()) {
        if (found_dot_star) {
            //if (*found_dot_star) {
            //    actual->Error("multiple .* port connections found") ;
            //    (*found_dot_star)->Info("previous .* connection is here") ;
            //}
            *found_dot_star = actual ;
        }
        return 1 ;
    }

    // Find the port here :
    VeriIdDef *formal = 0 ;
    const char *formal_name = actual->NamedFormal() ;
    if (formal_name) {
        // This is a named formal.
        //formal = (formal_scope) ? formal_scope->FindLocal(formal_name) : 0 ;
        formal = (module) ? module->GetPort(formal_name) : 0 ;
        if (module && !formal) {
            // Cannot immediately error out, since module could have 'named' port expressions itself,
            // which do not create identifiers in the scope. Happens for ivltests/contrib8.2.v.
            //
            // In that case, scan the port-connects of the module.
            unsigned found_it = 0 ;
            Array *port_connects = module->GetPortConnects() ;
            unsigned k ;
            VeriExpression *port_connect ;
            FOREACH_ARRAY_ITEM(port_connects, k, port_connect) {
                if (!port_connect) continue ;
                if (!port_connect->NamedFormal()) continue ;
                if (Strings::compare(port_connect->NamedFormal(),formal_name)) {
                    found_it = 1 ;
                    // We do not have an identifier here..(Maybe we should have created one?).
                    break ;
                }
            }
            // Pff. All this just for an error message...
            // VIPER #7102: Do not produce this error if we are inside conditional generate branch.
            // If the branch is active, we will anyway produce the error from RTL elaborator.
            // For static elaboration, there should not be generate at all.
            if (!found_it && (!info || !info->GetInConditionalGenerate())) {
                actual->Error("%s has no port called %s",module->Name(),formal_name) ;
                if (module_id) module_id->Info("%s is declared here",module_id->Name()) ;
            }
        }
        // VIPER #6024: Do not produce the error in case the found net is implicitly declared:
        if (formal && !formal->IsPort() && !formal->IsImplicitNet()) {
            // VIPER #7102: Do not produce this error if we are inside conditional generate branch.
            // If the branch is active, we will anyway produce the error from RTL elaborator.
            // For static elaboration, there should not be generate at all.
            if (!info || !info->GetInConditionalGenerate()) {
                actual->Error("no definition for port %s", formal->Name()) ; // VIPER #8172
            }
            formal = 0 ;
        }
    } else {
        // This is a ordered formal
        if (pos>=((formals) ? formals->Size() : 0)) {
            if (module_id) {
                // VIPER #7102: Do not produce this error if we are inside conditional generate branch.
                // If the branch is active, we will anyway produce the error from RTL elaborator.
                // For static elaboration, there should not be generate at all.
                if (!info || !info->GetInConditionalGenerate()) {
                    Error("%s expects %d arguments", module_id->Name(), ((formals) ? formals->Size() : 0)) ;
                }
                // no use checking more. No use resolving either, since these actuals are obsolete.
                return 0 ; // Don't bail out if module was not found. We still want to resolve a bit.
            }
        } else {
            formal = (formals && (formals->Size() > pos)) ? (VeriIdDef*)formals->At(pos) : 0 ;
        }
    }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    VhdlIdDef *vhd_port = 0 ;

    VhdlIdDef *vhdl_unit_id = vhdl_unit ? vhdl_unit->Id() : 0 ;
    if (formal_name) {
        char *formal_name_cp = Strings::save(formal_name) ;
        formal_name_cp = Strings::strtolower(formal_name_cp) ;

        vhd_port = (vhdl_unit_id) ? vhdl_unit_id->GetPort(formal_name_cp): 0 ;

        if (!vhd_port && vhdl_unit_id) {
            // port name may need escaping in vhdl
            char *formal_name_cp1 = Strings::save("\\", formal_name_cp, "\\") ;
            vhd_port = vhdl_unit_id->GetPort(formal_name_cp1) ;
            Strings::free(formal_name_cp1) ;
        }
        Strings::free(formal_name_cp) ;
    } else if (vhdl_unit_id) {
        vhd_port = (vhdl_unit_id->NumOfPorts() > pos) ? vhdl_unit_id->GetPortAt(pos): 0 ;
    }
#endif
    // add this as a connected formal :
    if (formal && !connected_formals.Insert(formal)) {
        // Formal is already connected. Complain :
        // Do not complain if this was a 'ordered' actual :
        // There could be multiple ports with same name in the module, "module foo(a,a);" which is allowed in Verilog (LRM 12.3.3).
        // We are just not allowed to have two named ports in instantiation : "mod inst (.b(),.b());" is NOT allowed (LRM 12.3.6)
        // FIX ME : We can check this in Resolve(), based on just the formal names.
        // That can be done before the actual module is found, since Verilog does not allow mixed named and positional associations.
        if (formal_name) {
            actual->Error("port %s is already connected", formal->Name()) ;
        }
    }
    // FIX ME : can use 'connected_formals' to resolve .* associations...

    // Resolve the actual : Non-blocking assignment to outputs, 'using' for inputs,
    // use PORT_CONNECTION for everything else. (That triggers both concurrent read and write).
    //
    // RD: Issue 2656 : If the module not resolved, then we cannot make an assumption
    // about the direction of the formals at all. If we give PORT_CONNECTION, which
    // will essentially be an 'inout' behavior, then we could get incorrect errors
    //
    // If the module IS resolved, but there is no 'formal', then we must be dealing
    // with a real 95 port expression as port (these do not create a port).
    //
    // Either way, we don't know the direction.
    // However, if we pass in environment PORT_EXPRESSION, then we will register both
    // a cuncurrent read and a concurrent write. This can lead to incorrect errors like in VIPER 2656 if there is another driver.
    // If we pass in USING, then these error go away, but then we will not have registered a
    // concurrent write on the actuals. And that (no write) in turn can lead to incorrectly using the
    // initial value for the identifier (during elaboration) if there is no other write.
    //
    // That is a catch-22 situation, which points at a conceptual problem with the used/assigned flags
    // and with the initialization code, which we need to solve at some point. FIX ME !!
    //
    // Meanwhile, to fix 2656, pass in environment USING for undefined modules
    // while using PORT_EXPRESSION for actuals of 95 port expressions to stay backward compatible.
    // RD: 9/26/2006: Back off on this (2656) fix. If we pass 'USING', VarUsage does not register
    // any writes on actuals of undefined modules. As a result, many unintended net-initializations (initialize with constant) occur in elaboration.
    // FIX ME : have to find another way to avoid the error messages.

    // FIX ME for .* and .name associations...
    if ((formal && !formal->IsInterfacePort())
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        || vhd_port
#endif
       ) {
        unsigned direction = formal ? formal->Dir() : 0 ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        // Viper 6397 for vhdl unit get the direction from vhdl port
        if (!formal && vhd_port) {
            if (vhd_port->IsInput()) direction = VERI_INPUT ;
            if (vhd_port->IsOutput()) direction = VERI_OUTPUT ;
            if (vhd_port->IsInout()) direction = VERI_INOUT ;
        }
#endif
        switch(direction) {
        case VERI_OUTPUT :
            actual->VarUsage(info, NONBLOCKING_ASSIGNING) ;
            break ;
        case VERI_INPUT :
            actual->VarUsage(info, USING) ;
            break ;
        case VERI_INOUT :
            // Issue 2808 : explicit INOUT port is BOTH a nonblock assign AND a using.
            actual->VarUsage(info, NONBLOCKING_ASSIGNING) ;
            actual->VarUsage(info, USING) ;
            break ;
        default :
            break ;
        }
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        // Viper 6397 check for illegal connection
        if (vhd_port) actual->CheckAssociationVhdl(vhd_port, is_checker ? PROP_SEQ : INSTANCE) ;
#endif
    } else {
        actual->VarUsage(info, PORT_CONNECTION) ;
    }

    if (!formal) return 1 ;
    // Assignment pattern cannnot be used as actual to inout/ref port of module/program/interface
    if (((formal->Dir() == VERI_REF) || (formal->IsInout() && !formal->IsInterfacePort())) && actual->IsAssignPattern()) {
        actual->Error("assignment pattern is illegal for use with %s port %s", PrintToken(formal->Dir()), formal->Name()) ;
    }

    // VIPER #4580: Error out for open ref type ports:
    if ((formal->Dir() == VERI_REF) && actual->GetConnection()->IsOpen()) {
        actual->Error("ref type port %s cannot be left unconnected", formal->Name()) ;
    }

    // VIPER #2852: Adjustment for the Array instances in type checking:
    // We need to adjust the unpacked dimensions of the formal <-> actual pair for the
    // array instances. Since we don't exactly know about the situation unless we evaluate
    // the ranges we will only adjust the dimensions without depending on its size. We can't
    // evaluate a range properly, because it can contain parameters and those parameters
    // might be overridden from elesewhere. Thus, our moto is to adjust the unpacked dimensions
    // so that we don't produce error in legal designs. We may be silent on illegal designs:
    if (array_inst_dims) { // array instance
        // So actual type can be same as formal or array of formal type so that
        // each array element can be used for every single instance of the array instance
        // Create formal type
        VeriTypeInfo *formal_type = formal->CreateType(0) ;

        if (!formal_type) return 1 ; // Nothing can be checked without formal type

        unsigned is_assignment_pattrn = (actual->IsConcat() || actual->IsMultiConcat() ||  actual->IsAssignPattern() || actual->IsMultiAssignPattern()) ? 1 : 0 ;
        VeriTypeInfo *actual_type = actual->CheckType(is_assignment_pattrn ? formal_type : 0 /*formal_type*/, (formal->IsOutput()) ? WRITE : (formal->IsInterfacePort() ? READ_INSTANCE: READ)) ;
        if (actual_type && (actual_type->IsTypeParameterType() || actual_type->IsUnresolvedNameType())) {
            delete actual_type ; actual_type = 0 ;
        }

        if (actual_type && (actual_type->UnpackedDimension() > formal_type->UnpackedDimension())) { // Actual type cannot be determined
            // Try again with array of formal type
            Array idx_arr(array_inst_dims) ;
            for (unsigned idx = 0; idx < array_inst_dims; idx++) idx_arr.InsertLast(0) ;
            actual_type = actual_type->ReduceDimBy(&idx_arr, 0) ;
            //formal_type = new VeriFixedSizeArray(formal_type, array_inst_dims) ;
            //actual_type = actual->CheckType(0 /*formal_type*/, (formal->IsOutput()) ? WRITE : (formal->IsInterfacePort() ? READ_INSTANCE: READ)) ;
        }
        if (actual_type) (void) actual_type->CheckAgainst(formal_type, actual, (formal->IsOutput()) ? WRITE : READ) ;
        delete actual_type ;
        delete formal_type ;
    } else {
        // VIPER #5710: (vcs_compatibility: block_after_checker/endchecker_with_name)
        // Checker ports are like sequence/property formals, so for checker block
        // we pass 'PROP_SEQ' else pass 'INSTANCE' as 'veri_type_env':
        // VIPER #8255: Keep track of the error count.
        // If it increases during this run, then produce warning for incompatible
        // formal actual association
        unsigned error_count = Message::ErrorCount() ;
        unsigned error_given = actual->CheckAssociation(formal, is_checker ? PROP_SEQ : INSTANCE) ;
        if (!error_given && (error_count < Message::ErrorCount())) {
            actual->Info("connection type is incompatible with formal type for port %s in instance %s", formal->Name(), inst_id ? inst_id->Name(): "of module") ;
        }
    }

    return 1 ;
}

/*--------------------------------------------------------------------------*/
unsigned
VeriModuleInstantiation::VarUsage(VeriVarUsageInfo *info, unsigned /*action*/)
{
    // Change since 1800 System Verilog :
    // VarUsage is now called only after all modules are analyzed.
    // Use that to fully resolve all module instantiations.

    // Lookup the module.
    // This will find the module in the work library, or if not there,
    // it will find it in the 'uselib' library, or if not there
    // it will return 0 (unknown module).
    VeriModule *module = GetInstantiatedModule() ;

    if (module && module->IsPackage()) {
        // VIPER #7210: Packages cannot be instantiated in this way:
        Error("illegal instantiation: %s %s cannot be instantiated in this way", "package", module->GetName()) ;
        return VALUE_IGNORED ;
    }

    void *void_unit = 0 ; // For simplified use of VhdlPrimaryUnit *vhdl_unit:
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    VhdlPrimaryUnit *vhdl_unit = 0 ;
    if (!module && _module_name) {
        // If its really not defined in Verilog :
        // Determine the library of this module :
        VeriLibrary *module_lib = GetLibrary() ;

        // Otherwise, use the work library (no uselib setting).
        if (!module_lib) module_lib = veri_file::GetWorkLib() ;
        VERIFIC_ASSERT(module_lib) ;
        vhdl_unit = VeriNode::GetVhdlUnitForVerilogInstance(_module_name->GetName(), module_lib->GetName(), 1) ;
        void_unit = vhdl_unit ;
    }
#endif

    // If extern module remains in parse tree (for which no actual def exists), do not process that
    if (module && module->GetQualifier(VERI_EXTERN)) module = 0 ;
    if (module && _module_name) {
        // Set the id as the instantiated module ?
        // Only do this for 'interface' modules.
        // Regular modules remain theit simple char* name reference, so that we avoid getting
        // massive stale pointers.
        // Interfaces really need the IdDef*, because we will definitely access fields in the interface..
        if (module->IsInterface()) {
            _module_name->SetId(module->GetId()) ;
            // Flag that this module depends on the used module, to avoid stale pointers later.
            // FIX ME : Don't have a scope here.
            // if (scope) scope->Using(module->GetScope()) ;
        }
    } else if (GetModuleName()) {
        // Potentially error out for unknown module instantiations :
        // Error("instantiating unknown module %s", GetModuleName()) ;
    }

    // Get the instantiated module ports (so that we can determine direction of assignments to actuals
    //VeriIdDef *module_id = (module) ? module->GetId() : 0 ;
    Array *formals = (module) ? module->GetPorts() : 0 ;
    //VeriScope *formal_scope = (module) ? module->GetScope() : 0 ;
    // VIPER #5710: (vcs_compatibility: block_after_checker/endchecker_with_name)
    // If we are in checker set the the flag on.
    unsigned is_checker = 0 ;
    if (module && module->IsChecker()) is_checker = 1 ;
    // Keep track of the connected ports :
    Set connected_formals(POINTER_HASH, (formals) ? formals->Size() : 1) ;

    // Process the instances:
    unsigned i, j;
    VeriInstId *inst ;
    VeriExpression *expr ;
    VeriIdDef *formal ;
    FOREACH_ARRAY_ITEM(&_insts, j, inst) {
        if (!inst) continue ;

        // Verilog module instantiation.
        connected_formals.Reset() ;

        // VIPER #2852: Adjustment for the Array instances in type checking:
        // Get the number of dimensions of this instance (array_inst_dims > 0 for array instance):
        unsigned array_inst_dims = inst->Dimension() ;

        // Process the port_connections on these instances.
        // Do not call VeriInstId::VarUsage(), because that would only mean a 'usage' of the InstId.
        // Instead, we want to process the port_connect actuals here :
        //unsigned found_dot_star = 0 ;
        VeriExpression *found_dot_star = 0 ;
        FOREACH_ARRAY_ITEM(inst->GetPortConnects(), i, expr) {
            if (!VarUsagePortConnect(info, i, expr, module, void_unit, array_inst_dims, connected_formals, &found_dot_star, inst)) {
                break ;
            }
        }

        // VIPER #3230: Now check if all formal ports have an actual connection:
        if (formals && (formals->Size() != connected_formals.Size())) {
            // There is a mismatch need to produce error messages:
            FOREACH_ARRAY_ITEM(formals, i, formal) {
                if (!formal || connected_formals.GetItem(formal)) continue ;
                // Here, this formal does not have an actual connected:
                if (found_dot_star) {
                    // VIPER #6508: Call VarUsage on this implicitly connected port through dot-star.
                    VeriIdDef *actual_id = (_present_scope) ? _present_scope->Find(formal->Name()) : 0 ;
                    if (actual_id) {
                        VeriIdRef *tmp_conn = new VeriIdRef(actual_id) ; tmp_conn->SetLinefile(found_dot_star->Linefile()) ;
                        VeriPortConnect tmp_actual(Strings::save(formal->Name()), tmp_conn) ; tmp_actual.SetLinefile(found_dot_star->Linefile()) ;
                        (void) VarUsagePortConnect(info, i, &tmp_actual, module, void_unit, array_inst_dims, connected_formals, 0, inst) ;
                        // Automatic parse tree node 'tmp_actual' will get deleted past this block.
                    }
                    continue ; // CHECKME: Continue or fall-through if not connected even here?
                }
                if (formal->Dir() == VERI_REF) {
                    // VIPER #4580: Error out for open ref type ports:
                    Error("ref type port %s cannot be left unconnected", formal->Name()) ;
                } else {
                    VeriExpression *formal_port_expr = formal->GetActual() ;
                    if (formal_port_expr && formal_port_expr->IsOpen()) continue ;

                    // VIPER #7102: Do not produce following error if we are inside conditional generate branch.
                    // If the branch is active, we will anyway produce the error from RTL elaborator.
                    // For static elaboration, there should not be generate at all.
                    if (info && info->GetInConditionalGenerate()) continue ;

                    if (is_checker && !formal->IsOutput() && !formal->GetInitialValue()) {
                        // VIPER #6512: Produce error for checker input/inout ports.
                        // Checkers are not 'instantiated' in that way in the netlist,
                        // so the error will not be produced later. Also checker ports
                        // are VeriSeqPropertyFormal. If they are not connected and have
                        // no initial value then the usage of the ports are not going to
                        // create any logic since Evaluate() on them returns NULL then.
                        // We already produce error for un-connected sequence/property
                        // ports for VIPER #4081. Produce error here for checkers also:
                        Error("port %s is not connected to this instance", formal->Name()) ;
                    } else {
                        // VIPER #7466: Use a different message id/text for warning than error message:
                        Warning("port %s remains unconnected for this instance", formal->Name()) ;
                    }
                }
            }
        }
    }
    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriGateInstantiation::VarUsage(VeriVarUsageInfo *info, unsigned /*action*/)
{
    // Process the instances:
    unsigned i, j;
    VeriInstId *inst ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_insts, i, inst) {
        if (!inst) continue ;

        // Verilog gate instantiation.
        // Process the port_connections on these instances.
        // Do not call VeriInstId::VarUsage(), because that would only mean a 'usage' of the InstId.
        // Instead, we want to process the port_connect actuals here :
        Array *port_connects = inst->GetPortConnects() ;

        FOREACH_ARRAY_ITEM(port_connects, j, expr) {
            if (!expr) continue ;

            switch (_inst_type) {
            case VERI_BUF :
            case VERI_NOT :
                // one or more outputs, followed by one input:
                if (j==port_connects->Size()-1) {
                    expr->VarUsage(info, USING) ; // input
                } else {
                    expr->VarUsage(info, NONBLOCKING_ASSIGNING) ; // output
                }
                break ;

            case VERI_NMOS :
            case VERI_PMOS :
            case VERI_RNMOS :
            case VERI_RPMOS :

            case VERI_CMOS :
            case VERI_RCMOS :

            case VERI_TRAN :
            case VERI_RTRAN :

            case VERI_TRANIF1 :
            case VERI_TRANIF0 :
            case VERI_RTRANIF1 :
            case VERI_RTRANIF0 :
                // Quick check for all transistors : resolve with PORT_CONNECTION
                // (assuming we both assign and use the actual)
                // finetune later..
                expr->VarUsage(info, PORT_CONNECTION) ;
                break ;

            default:
                // Regular Verilog gate: The first one is always the output. Rest is input.
                if (j==0) {
                    expr->VarUsage(info, NONBLOCKING_ASSIGNING) ; // output
                } else {
                    expr->VarUsage(info, USING) ; // input
                }
                break ;
            }
        }
    }
    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriContinuousAssign::VarUsage(VeriVarUsageInfo *info, unsigned /*action*/)
{
    VeriNetRegAssign *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_net_assigns, i, item) {
        if (item) item->VarUsage(info, USING) ;
    }
    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriAlwaysConstruct::VarUsage(VeriVarUsageInfo * this_info, unsigned /*action*/)
{
    // Always construct is a separate 'sequential' flow.
    // Process here :
    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo info(&id_table) ;

    // VIPER #8278: Do not produce this error if we are inside conditional generate branch.
    // If the branch is active, we will anyway produce the error from RTL elaborator.
    // For static elaboration, there should not be generate at all. Check VIPER #7102 also.
    // So if we are in generate conditional set this information to info of always construct.
    if (this_info && this_info->GetInConditionalGenerate()) {
        info.SetInConditionalGenerate() ;
    }

    if (GetSpecialAlwaysToken()) {
        info.SetInSpecialAlways() ;
    }

    info.SetInAlwaysInitial() ;
    // traverse the sequential statements
    if (_stmt) (void) _stmt->VarUsage(&info, USING) ;
    info.SetInAlwaysInitial(0) ;

    // All variables used or assigned in this block are marked
    // as concurrent_used or concurrent_assigned
    info.UpdateConcurrentVarStatus() ;
    // VIPER #4241 : Set assignment information for variables assigned inside this construct
    info.SetGlobalAssignmentInfo(this) ;

    return VALUE_IGNORED ;
}

unsigned
VeriInitialConstruct::VarUsage(VeriVarUsageInfo * this_info, unsigned /*action*/)
{
    // Always construct is a separate 'sequential' flow.
    // Process here, so all semantic checks (assignments to nets/regs) is done here.
    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo info(&id_table, 1 /*flag that we are in an initial construct*/ ) ;

    // VIPER #8278: Do not produce this error if we are inside conditional generate branch.
    // If the branch is active, we will anyway produce the error from RTL elaborator.
    // For static elaboration, there should not be generate at all. Check VIPER #7102 also.
    // So if we are in generate conditional set this information to info of initial construct.
    if (this_info && this_info->GetInConditionalGenerate()) {
        info.SetInConditionalGenerate() ;
    }

    info.SetInAlwaysInitial() ;
    // traverse the sequential statements
    if (_stmt) (void) _stmt->VarUsage(&info, USING) ;
    info.SetInAlwaysInitial(0) ;

    // Initial construct is NOT assigning identifiers 'concurrently', since it is only executed once.
    // So, do NOT update ConcurrentVarStatus.
    // VIPER #4241 : Set assignment information for variables assigned inside this construct
    info.SetGlobalAssignmentInfo(this) ;

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/
// For generate statements we don't know if the body is going to be executed even once.
// For the purposes of determining variable usage, the most conservative approach is
// to assume that the body IS going to be executed and therefor we traverse all the
// concurrent statements in the body.
/*
class VeriGenerateConstruct
class VeriGenerateConditional
class VeriGenerateCase
class VeriGenerateFor
class VeriGenerateBlock
class VeriGenerateCaseItem
*/

unsigned
VeriGenerateConstruct::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    if (action == DEAD) return DEAD ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) (void) item->VarUsage(0, action) ;
    }
    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriGenerateConditional::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    if (action == DEAD) return DEAD ;

    // Traverse the conditional expression
    if (_if_expr) _if_expr->VarUsage(0, USING) ;

    // Viper #3908 : create a local VarUsageInfo
    // This will help us skip certain semantic checks as the condition
    // may evaluate to always true/false
    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo local_info(&id_table) ;
    local_info.SetInConditionalGenerate() ;

    // Issue 2450 and 2344 : If we run VarUsage on both branches,
    // then we will get a multiple-assignment error for regs assigned within each branch (SV semantics).

    // Short-term solution : VarUsage check on only ONE branch.
    // That at least tests VarUsage for ONE setting of the parameters :

    // Traverse the module item that will be executed when the condition is true
    if (_then_item) (void) _then_item->VarUsage(&local_info, USING) ; // local_info for 3908

    // Traverse the module item that will be executed when the condition is false
    // the !_then_item test fixes issues 2450 and 2344.
//    if (!_then_item && _else_item) (void) _else_item->VarUsage(0, USING) ;
    //
    // RD: VIPER 3725 : Have to scan both branches, or else we incorrectly flag nets as 'not-assigned'
    //
    // This breaks 2450/2344, but that is not too bad, because the error (VERI-1318 : multiple assignments)
    // that we produce there had to be turned into a warning already due to VIPER 2656 (blackbox actuals).
    //
    // A better solution might be to create a 'VeriVarUsageInfo' for concurrent areas also :
    // just flag it with '_is_concurrent' and do a 'mutual-exclusive dataflow' analysis here, similar to what we do in VeriConditionalStatement ('if').
    //
    // The test (multiple assignments) is of limited use any way, because we cannot check 'partial' assignment collisions (VIPER 2254 and 2232).
    // Need to reconsider this test, and where we do it. Probably better in (RTL or static) elaboration.
    // RTL elaboration DOES this as electrical check, but static elab still relies on the test here.
    // Static elab removes VeriGenerateConditional, so we are free to revert this change.
    //
    // Modified the message (VERI-1318) using 'might be multiple assigned'.
    // Because of limited usability, users are encouraged to suppress the message altogether (with Message::SetMessageType()).
    if (_else_item) (void) _else_item->VarUsage(&local_info, USING) ; // local_info for 3908

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriGenerateCase::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    if (action == DEAD) return DEAD ;

    // Traverse the case condition
    if (_cond) _cond->VarUsage(0, USING) ;

    // Viper #3908 : create a local VarUsageInfo
    // This will help us skip certain semantic checks as the condition
    // may evaluate to always true/false
    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo local_info(&id_table) ;
    local_info.SetInConditionalGenerate() ;

    // Traverse case items
    unsigned i;
    VeriGenerateCaseItem *item ;
    FOREACH_ARRAY_ITEM(_case_items, i, item) {
        if (item) item->VarUsage(&local_info, USING) ;
    }

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriGenerateFor::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    Map id_table(POINTER_HASH) ;
    VeriVarUsageInfo *local_info = info ? info->Copy() : new VeriVarUsageInfo(&id_table) ;

    // To avoid many semantic checks, use an existing flag
    local_info->SetInConditionalGenerate() ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    // Process initial statement :
    if (_initial) (void) _initial->VarUsage(local_info, FORGEN_INIT) ;

    // Process the condition :
    if (_cond) _cond->VarUsage(0,USING) ;

    // Traverse block items
    unsigned i;
    VeriModuleItem *item;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) (void) item->VarUsage(local_info, action) ;
    }

    // Process the 'repetition' statement :
    if (_rep) (void) _rep->VarUsage(0,GENVAR_ASSIGN) ;

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    _present_scope = save_scope ;
    delete local_info ;

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriGenerateBlock::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) (void) item->VarUsage(info, action) ;
    }

    _present_scope = save_scope ;

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

void
VeriGenerateCaseItem::VarUsage(VeriVarUsageInfo *info, unsigned action) const
{
    if (action == DEAD) return ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // Traverse choices
    VeriExpression *dr;
    unsigned i;
    FOREACH_ARRAY_ITEM(_conditions, i, dr) {
        if (dr) dr->VarUsage(info, action) ;
    }

    // Traverse statement
    if (_item) (void) _item->VarUsage(info, action) ;

    _present_scope = save_scope ;
}

unsigned
VeriBindDirective::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    // Bind Directive forces a instantiation to occur inside another module (in hier_name).

    if (!_instantiation) return VALUE_IGNORED; // nothing to do
    if (!_target_inst_list || !_target_inst_list->Size()) return VALUE_IGNORED; // no module to bind this thing to.

    // VIPER #2705 : Target module of the bind directive can be in work library
    // and in any other user library. Bind instantiation is to be inserted in all
    // those modules. But with our current approach, we cannot insert bind
    // instantiation in multiple modules as we do not copy parse tree. So, for
    // the time being we will insert bind instance in only one target module
    // searching from all libraries.
    //
    // Bind directive can specify one target module or one/multiple instance
    // paths. For now (3/2006) we only support plain module name.
    //
    // Do not iterate over target instance list, as it will issue error if
    // module is not present in work library. Actually module can present
    // in any user library too.
#if 0
    unsigned i ;
    VeriName *hier_name = 0 ;
    // Iterate over the target instance list
    FOREACH_ARRAY_ITEM(_target_inst_list, i, hier_name) {
        // The hier name can be a plain module name, or an instance path.
        // For now (3/2006) we only support plain module name.
        if (!hier_name)  return VALUE_IGNORED ; // should issue a warning instead

        // VarUsage on the hier name : should result in a module :
        hier_name->VarUsage(info, USING/*something better?*/) ;
    }
#endif

    // Pick up the module identifier :
    VeriModule *hier_module = 0 ;
    const char *hier_module_name = 0 ;
    if (_target_mod_name) {
        hier_module_name = _target_mod_name ;
    } else { // When the target module name is null the size of _target_inst_list must be 1
        VeriName *target_mod = _target_inst_list ? (VeriName*)_target_inst_list->GetFirst() : 0 ;
        hier_module_name = (target_mod && !target_mod->IsHierName()) ? target_mod->GetName() : 0 ; // Only consider simple module name
    }

    // First search 'hier_module_name' in work library
    // As we are not suporting bind instance addition in multiple modules, so
    // give priority to work library over user defined libraries.
    VeriLibrary *work_lib = veri_file::GetWorkLib() ;
    hier_module = work_lib ? work_lib->GetModule(hier_module_name, 1): 0 ;

    if (!hier_module) {
        // Module not in work library, search in the user library. That one should be in the _instantiation.
        // Can't get a hold of GetLibrary, since _instantiation is a VeriModuleItem*. Change argument of BindDirective ?
#if 0
        work_lib = _instantiation->GetLibrary() ;
        hier_module = work_lib ? work_lib->GetModule(hier_module_name, 1): 0 ;
#endif
        // Instead, iterate over all user libraries to search for module 'hier_module_name'
        VeriLibrary *lib ;
        MapIter mi ;
        FOREACH_VERILOG_LIBRARY(mi, lib) {
            if (!lib || !lib->IsUserLibrary()) continue ;
            hier_module = lib->GetModule(hier_module_name, 1) ;
            if (hier_module) break ; // Take the first module, ignore all others for time being
        }
    }
    if (!hier_module) { //Search in VhdlLib
        // Viper 3431: Support for bind construct to Vhdl Entity
        VhdlLibrary *vhdl_lib ;
        MapIter mi ;
        FOREACH_VHDL_LIBRARY(mi, vhdl_lib) {
            if (!vhdl_lib) continue ;
            VhdlPrimaryUnit *hier_unit = vhdl_lib->GetPrimUnit(hier_module_name, 0) ;
            if (hier_unit) {
                unsigned num_sec_units = hier_unit->NumOfSecondaryUnits() ;
                VhdlSecondaryUnit *sec_unit = num_sec_units ? hier_unit->SecondaryUnitByIndex(num_sec_units-1) : 0 ;
                // Viper 7225: Only add the Verilog instance but not process it. Process it
                // at the time of elaboration as the instance may contain hierarchical name
                // reference.
                if (sec_unit) sec_unit->AddVerilogBindInstance(_instantiation->CastModuleInstance()) ;
                //hier_unit->ProcessBindInstance(_instantiation->CastModuleInstance()) ;
                return VALUE_IGNORED ;
            }
        }
    }

    if (!hier_module) { // Not simple module, may be instances
        unsigned i ;
        VeriName *hier_name = 0 ;
        // Iterate over the target instance list
        FOREACH_ARRAY_ITEM(_target_inst_list, i, hier_name) {
            // VeriName        *hier_name ;       // The reference to a module or instance path
            if (!hier_name)  return VALUE_IGNORED ; // should issue a warning instead
            // VarUsage on the hier name :
            hier_name->VarUsage(info, USING/*something better?*/) ;
        }
    }

    // FIX ME : Warn (error) on hierarchical (instance path) hier names.. Not supported yet..

    // FIXME : Bind directive with instance list can only add the instantiation to all the instances
    // of the module and not to particular instance(s).

    if (!hier_module) return VALUE_IGNORED; // error out on 'is not a module' ?

    VeriScope *hier_module_scope = hier_module->GetScope() ; // the target module's scope.

    VeriScope *save_scope = _present_scope ;
    _present_scope = hier_module_scope ; // Should set to scope of the target module

    // VeriModuleItem  *_instantiation ;   // The instantiation of a module, program or interface
    // The instantiatiation first needs to be resolved IN THE SCOPE OF THE 'hier_name' module.
    // This has not yet happened, since we did not have modules parsed up till now.
    // So, we need to first call 'Resolve' on the instantiation (pass in scope of hier_name module)
    // and then run full VarUsage() on the instantiation.

    _instantiation->Resolve(hier_module_scope, VERI_UNDEF_ENV) ;
    (void) _instantiation->VarUsage(info, NONE) ;

    //
    // On top of that, we need to put a back-pointer to this instantiation into
    // the hier_name module, so that upon elaboration that module knows that the
    // instantiation needs to occur.
    hier_module->AddBindInstance(_instantiation) ;

    _present_scope = save_scope ;

    return VALUE_IGNORED ; // this return value for module items is silly.
}

/*--------------------------------------------------------------------------*/
/*Begin VeriStatement derived objects

   VeriAssign
   VeriBlockingAssign
   VeriNonBlockingAssign
   VeriTaskEnable
   VeriDelayControlStatement
   VeriConditionalStatement
   VeriCaseStatement
   VeriForever
   VeriRepeat
   VeriWhile
   VeriFor
   VeriSeqBlock
   VeriDisable
   VeriDoWhile
   VeriJumpStatement
   VeriForeach
   VeriArrayMethodCall
   VeriInlineConstraintStmt
   VeriSystemTaskEnable
   VeriEventControlStatement

   NOT implemented ----------------------------------

   VeriForce
   VeriRelease
   VeriAssertion
   VeriParBlock
   VeriDeAssign
   VeriWait
   VeriEventTrigger
   VeriWaitOrder
*/
/*--------------------------------------------------------------------------*/

unsigned
VeriStatement::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    // Do Nothing here.  All derived classes will define their own
    // specific version of VarUsage
    return action ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriAssign::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    if (_assign) _assign->VarUsage(info, action) ;

    _present_scope = save_scope ;
    return ALIVE ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriGenVarAssign::VarUsage(VeriVarUsageInfo *info, unsigned action) // Viper #6081
{
   if (action == DEAD) return DEAD ;

   if (!info || !_id) return ALIVE ;

   // VIPER #8109: Call VarUsage on value to catch illegal initialization of for-generate index
   if (_value) _value->VarUsage(info, action) ;

   if (info->HasIdInStack(_id)) {
       Error("genvar %s is already used in this scope", _id->Name()) ;
   } else {
       info->AddIdInStack(_id) ;
   }

   return ALIVE ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriNonBlockingAssign::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // VIPER #5832: According to LRM (p1800 2009 Section 14.6) regular intra-assignment delay is not
    // not allowed in synchronous drives. So error out if intra-assignment delay control is used in
    // an assignment to a clocking.
    if (_control) {
        VeriDelayOrEventControl *control = _control ;
        unsigned is_cycle = control->IsCycleDelay() ;
        if (_lval) {
            VeriName *prefix = _lval->GetPrefix() ;
            VeriIdDef *prefix_id = prefix ? prefix->GetId() : 0 ;
            if (!is_cycle && prefix_id && prefix_id->IsClocking()) {
                Error("intra-assignment delay control is not allowed in an assignment to a clocking") ;
            }
        }
    }

    // test only the RHS for use of variables
    if (_val) _val->VarUsage(info, USING) ;

    // The LHS needs to be processed to determine the RAM status
    if (_lval) _lval->VarUsage(info, NONBLOCKING_ASSIGNING) ;
    if (_lval && _val) _lval->CheckAssignment(_val, VERI_LEQ) ;
#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    // Viper 4495: Reference: IEEE Std 1800-2005, the last paragraph of 16.2
    // However both the popular simulators do not catch this so decided to
    // throw a warning instead of an Error. The requrement is program
    // variables cannot have a non-blocking assignment
    VeriIdDef *context = _present_scope ? _present_scope->GetOwner() : 0 ;
    VeriIdDef *lval_id = _lval ? _lval->GetId() : 0 ;
    VeriScope *lval_decl_scope = lval_id ? lval_id->GetOwningScope() : 0 ;
    VeriIdDef *lval_decl_scope_id = lval_decl_scope ? lval_decl_scope->GetOwner() : 0 ;
    if (lval_id && context && context->IsProgram() && lval_decl_scope_id && lval_decl_scope_id->IsProgram()) Warning("illegal non-blocking assignment of program variable %s", lval_id->GetName()) ;
#endif

    // Viper 4840 : This is in accordance with LRM section 15.10. in P1800
    // Used the flag _under_default_clock to see if the assignment is not
    // under any default clock. If the assignment is not under default clock
    // and the assignment is not to a clocking block id then we error out on
    // the cycle delay statement.
    if (_control && _control->IsCycleDelay() && !_under_default_clock) {
         // Now check if the lval is a clocking block id
         unsigned is_clocking_block_id = 0 ;
         VeriIdDef *curr_id = _lval ? _lval->GetId() : 0 ;
         VeriExpression *curr_val = _lval ;
         while (curr_id) {
             if (curr_id->IsClocking()) {
                 is_clocking_block_id = 1 ;
                 break ;
             }
             curr_val = curr_val ? curr_val->GetPrefix() : 0 ;
             curr_id = curr_val ? curr_val->GetId() : 0 ;
         }
         if (!is_clocking_block_id) Error("cycle delay in assignment only defined under default clock") ;
    }

    _present_scope = save_scope ;
    return ALIVE ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriBlockingAssign::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // test the RHS first in case the same variable is
    // on the RHS and the LHS
    if (_val) _val->VarUsage(info, USING) ;

    // It can be
    // VIPER #8109: Genvar assignment is legal in for-generate initialization, repeat
    // and in AMS for-loop initialization, repeat
    //if (_lval) _lval->VarUsage(info, (action==GENVAR_ASSIGN) ? 0: BLOCKING_ASSIGNING) ;
    // VIPER #8241: Pass GENVAR_ASSIGN when this blocking assign is from analog-for
    // initialization/repeat to set is_assign/is_used flag properly
    if (_lval) _lval->VarUsage(info, (action==GENVAR_ASSIGN) ? GENVAR_ASSIGN: BLOCKING_ASSIGNING) ;

#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    // Viper 4495: Reference: IEEE Std 1800-2005, the last paragraph of 16.2
    // However both the popular simulators do not catch this so decided to
    // throw a warning instead of an Error. The requrement is non-program
    // variables cannot have a blocking assignment
    VeriIdDef *context = _present_scope ? _present_scope->GetOwner() : 0 ;
    VeriIdDef *lval_id = _lval ? _lval->GetId() : 0 ;
    VeriScope *lval_decl_scope = lval_id ? lval_id->GetOwningScope() : 0 ;
    VeriIdDef *lval_decl_scope_id = lval_decl_scope ? lval_decl_scope->GetOwner() : 0 ;
    if (lval_id && context && context->IsProgram() && lval_decl_scope_id && !lval_decl_scope_id->IsProgram()) Warning("illegal blocking assignment of non-program variable %s", lval_id->GetName()) ;
#endif
    if (_lval && _val) { // normal assignment <target> = <value>
        _lval->CheckAssignment(_val, _oper_type) ;
        if (IsStaticElab()) {
            _lval->CheckArraySize(_val) ;
            // Viper 5411, 4800 Produce error if streaming concatenation is
            // larger than other side of assignment.
            VeriNode::CheckStreamingConcat(_lval, _val) ;
        }
        // VIPER #4806 : Produce warning for assignment to input
        if (_lval->IsHierName()) {
            VeriIdDef *id = _lval->GetId() ;
            if (id && id->IsInput() && !id->IsLocalVar()) {
                _lval->Warning("assignment to input %s",id->Name()) ;
            }
        }
    } else { // SV specific increment/decrement
        if (_lval) _lval->CheckAssignment(0, _oper_type) ;
        if (_val) _val->CheckAssignment(0, _oper_type) ;
    }

    // Viper 4840 : This is in accordance with LRM section 15.10. in P1800
    // Used the flag _under_default_clock to see if the assignment is not
    // under any default clock. If the assignment is not under default clock
    // and the assignment is not to a clocking block id then we error out on
    // the cycle delay statement.
    if (_control && _control->IsCycleDelay() && !_under_default_clock) {
         // Now check if the lval is a clocking block id
         unsigned is_clocking_block_id = 0 ;
         VeriIdDef *curr_id = _lval ? _lval->GetId() : 0 ;
         VeriExpression *curr_val = _lval ;
         while (curr_id) {
             if (curr_id->IsClocking()) {
                 is_clocking_block_id = 1 ;
                 break ;
             }
             curr_val = curr_val ? curr_val->GetPrefix() : 0 ;
             curr_id = curr_val ? curr_val->GetId() : 0 ;
         }
         if (!is_clocking_block_id) Error("cycle delay in assignment only defined under default clock") ;
    }

    _present_scope = save_scope ;
    return ALIVE ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriTaskEnable::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    if (!_task_name) return ALIVE ; // cannot do anything without a task name

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    VeriIdDef *task_id = _task_name->GetId() ;
    if (!task_id) {
        // Here, post-analysis, we should be able to resolve the task name :
        _task_name->VarUsage(info, SUBPROG_NAME) ;

        // Try it again :
        task_id = _task_name->GetId() ;
    }

    // If task id is from export created id without body, ignore that
    if (task_id && task_id->IsUnresolvedSubprog()) task_id = 0 ;
    // Check prefix for randomize, constraint_mode and random_mode functions. Viper 5161, Viper 5174
    // VCS Compatibility issue (test69): Check methods when task name is not resolved
    if (!task_id) CheckFunctionPrefix(0) ;

    if (!task_id) {
        _method_type = _task_name->GetFunctionType() ;
        if (!_method_type && !_task_name->IsHierName()) {
            _method_type = VeriExpression::GetFuncTokenOfClass(_task_name->GetName()) ;
        }
    }
    // VIPER #3030 : Call method resolver on task name. Normal VarUsage on _task_name
    // does not call 'VarUsageMethod', because the action becomes 'SUBPROG_NAME' and
    // VeriSelectedName::VarUsage does not call method resolver if action is 'SUBPROG_NAME'.
    // If the prefix pertains to a 'method', then call the method resolver :
    // Note that we only call this if the normal Resolve on the prefix did not find a known identifier.
    // Methods can be overridden by user-defined function (e.g. in classes).
    if (!task_id && _method_type) {
        // Call resolving the prefix as a 'method' task call, with the argument list in _args.
        _task_name->VarUsageMethod(info,action,_args) ;
        _present_scope = save_scope ;
        return ALIVE ; // done. All checked.
    }

    // Produce error if task is called inside function :
    if (info && info->GetInFunction() && ((task_id && task_id->IsTask()) || (!task_id && !_method_type))) {
        Error("functions cannot enable tasks") ;
    }
    if (!task_id) {
        _present_scope = save_scope ;
        return ALIVE ; // cannot do anything without a task name.
    }

    // Viper 7627: Issue warning for task call having delay/event control in always_comb/always_latch/always_ff block
    if (info && info->GetInSpecialAlways()) {
        VeriModuleItem *task_decl = task_id->GetModuleItem() ;
        if (task_decl && task_decl->HasDelayOrEventControl()) Warning("%s in always_comb/always_latch/always_ff is not allowed", "delay or event control") ;
    }

#ifdef VERILOG_DYNAMIC_VARUSAGE_ANALYSIS
    VeriModuleItem *task_decl = task_id->GetModuleItem() ;
    if (task_decl && task_id->IsTask()) { // Viper #4060
        if (!info || !info->HasTaskIdInStack(task_id)) {
            if (info) info->AddTaskIdInStack(task_id) ;
            task_decl->VarUsage(info, 0) ;
            if (info) info->RemoveTaskIdFromStack(task_id) ;
        }
    }
#endif // VERILOG_DYNAMIC_VARUSAGE_ANALYSIS

    // VIPER #3211 : Check if this is appropriate for a task/function call. This check
    // is also done in Resolve. But if task name is defined hierarchically upwards,
    // it is resolved in elaboration and so we can error out here.
    // VIPER #7368: Use of non-task used as a task enable is ERROR in V2K in all the simulators
    // (we have a special checks for non-void functions for SV product below):
    if (!task_id->IsTask() && !task_id->IsProduction() && (!IsSystemVeri() || !task_id->IsFunction())) {
        // In SV, there can be a 'function' or a 'production' name too.
        Error("%s is not a task", task_id->Name()) ;
    }
    VeriDataType *id_type = task_id->GetDataType() ;
    if (id_type) id_type = id_type->GetBaseDataType() ;
    if (IsSystemVeri() && task_id->IsFunction() && id_type && id_type->Type() && (id_type->Type()!=VERI_VOID) && !GetQualifier(VERI_VOID) ) {
        // VIPER #7368: This is a function call which is calling a non-void function without void casting:
        // Warning is produced in most of the simulators. So, produce warning here:
        Warning("non-void function %s called as a task without void casting", task_id->Name()) ;
    }

    // VIPER #4797 : Produce warning if 'extern' qualifier exist in method's body.
    // That means that no actual definition is associated with method id.
    if (task_id->IsExternForkjoinTask()) {
        // Get the definitions of extern/extern-fork-join tasks
        Array *bodies = task_id->GetExternForkjoinDefs() ;
        // Viper 4869. Added the body->GetQualifier(VERI_VIRTUAL) check
        //if (body && body->GetQualifier(VERI_EXTERN) && !body->GetQualifier(VERI_FORKJOIN) && !body->GetQualifier(VERI_VIRTUAL)) {
        if (!bodies || !bodies->Size()) {  // No actual definition
            VeriModuleItem *body = task_id->GetModuleItem() ; // get associated body
            if (body && body->GetQualifier(VERI_EXTERN) && !body->GetQualifier(VERI_FORKJOIN) && !body->GetQualifier(VERI_VIRTUAL)) {
                // Check whether it is inside package 'std'
                unsigned inside_std_pkg = 0 ;
                VeriScope *method_scope = body->GetScope() ;
                VeriIdDef *container_id = (method_scope) ? method_scope->GetContainingModule(): 0 ;
                if (container_id && Strings::compare(container_id->Name(), "std")) {
                    VeriModule *container = container_id->GetModule() ;
                    VeriLibrary *lib = (container) ? container->GetLibrary(): 0 ;
                    if (lib && Strings::compare(lib->GetName(), "std")) {
                        inside_std_pkg = 1 ;
                    }
                }
                if (!inside_std_pkg && body->IsVerilogMethod()) Warning("method never defined for %s %s", (task_id->IsFunction()) ? "function":"task", task_id->Name()) ;
            }
        }
    }

    // Pick up the formals :
    Array *formals = task_id->GetPorts() ;
    VeriScope *formal_scope = task_id->LocalScope() ;

    // VIPER #7688: Check whether it is 'randomize' function call. Then we will
    // not check port connections
    unsigned do_not_check = Strings::compare(_task_name->GetSuffix(),"randomize") ;
    VeriModuleItem *body = task_id->GetModuleItem() ;
    // Do not check for  VCS specific extern function
    if (body && !body->IsVerilogMethod()) do_not_check = 1 ;
    if (task_id->GetFunctionType() == VERI_METHOD_RANDOMIZE) do_not_check = 1 ;

    // traverse the actuals
    unsigned i ;
    VeriExpression *actual ;
    VeriIdDef *formal ;
    unsigned all_ports_connected = 1 ;
    unsigned named_formal = 0 ;
    Map connected_formals(POINTER_HASH, (formals) ? formals->Size() : 1) ;
    FOREACH_ARRAY_ITEM(_args, i, actual) {
        if (!actual) continue ;

        // Find the formal
        formal = 0 ;
        if (actual->NamedFormal()) {
            named_formal = 1 ;
            // This is a (SV-style) named formal.
            formal = (formal_scope) ? formal_scope->FindLocal(actual->NamedFormal()) : 0 ;
            // VIPER #7688: Check actual against formal
            if (task_id && !formal) {
                actual->Error("%s has no port called %s",task_id->Name(),actual->NamedFormal()) ;
                task_id->Info("%s is declared here",task_id->Name()) ;
            }
            if (formal && !formal->IsPort()) {
                actual->Error("no definition for port %s", formal->Name()) ; // VIPER #8172
                formal = 0 ;
            }
        } else {
            if (named_formal) {
                actual->Error("positional arguments must occur before named arguments") ;
            }
            // This is a ordered formal
            // VIPER #7688: Check actual against formal
            if (i>=((formals) ? formals->Size() : 0)) {
                if (!do_not_check) Error("%s expects %d arguments", task_id->Name(), ((formals) ? formals->Size() : 0)) ;
            } else {
                formal = (formals && (formals->Size() > i)) ? (VeriIdDef*)formals->At(i) : 0 ;
            }
        }

        if (!formal) continue ; // the formal cannot be found

        // infer actual direction based on formal direction
        if (formal->IsInput() || formal->IsInout())  actual->VarUsage(info, USING) ;
        if (formal->IsOutput() || formal->IsInout()) actual->VarUsage(info, BLOCKING_ASSIGNING) ;

        // VIPER #2046 :Check the type of actual against formal
        (void) actual->CheckAssociation(formal, TASK) ;

        VeriExpression *conn_expr = actual->GetConnection() ;
        unsigned long connected = (!conn_expr || conn_expr->IsOpen()) ? 0 : 1 ;
        all_ports_connected = all_ports_connected & connected ; // Update this flag with the status of this actual
        // VIPER #7688: Check actual against formal
        if (!connected_formals.Insert(formal, (void *)connected)) {
            // Formal is already connected. Complain :
            actual->Error("port %s is already connected", formal->Name()) ;
        }
    }

    // VIPER #7688: Now check if all formals have an actual or initial value
    if (!do_not_check && ((((formals) ? formals->Size() : 0) != connected_formals.Size()) || !all_ports_connected)) {
        // This can be legal for System Verilog, but not for anything else :
        if (IsSystemVeri() && task_id) {
            // There is a mismatch. Some must have an initial value
            FOREACH_ARRAY_ITEM(formals, i, formal) {
                if (!formal) continue ;
                if (connected_formals.GetValue(formal)) continue ;
                // Here, this formal is not connected.
                // Only allowed if it is an output, or if it has an initial value :
                VeriExpression *initial_expr = formal->GetInitialValue() ;
                if (initial_expr) continue ;
                Error("port %s is not connected on %s call %s", formal->Name(), "task", task_id->Name()) ;
            }
        } else {
            // Any other dialect should report this error ? :
            if (task_id) Error("%s expects %d arguments", task_id->Name(), ((formals) ? formals->Size() : 0)) ;
        }
    }
    // Now mark IsConcurrentAssigned in the class prefix
    // Pass this through to the prefix
    // For task calls we mark the class object as
    // partial concurrent assigned
    VeriName *prefix = _task_name->GetPrefix() ;
    VeriIdDef *prefix_id = prefix ? prefix->GetId() : 0 ;
    VeriTypeInfo *prefix_type = prefix_id ? prefix_id->CreateType(0): 0 ;
    //VeriDataType *d_type = prefix_id ? prefix_id->GetDataType() : 0 ;
    //VeriIdDef *type_id = d_type ? d_type->GetId() : 0 ;
    //if (prefix && type_id && type_id->IsClass()) prefix->VarUsage(info, PARTIAL_BLOCKING_ASSIGNING) ;
    if (prefix && prefix_type && prefix_type->IsClassType()) prefix->VarUsage(info, PARTIAL_BLOCKING_ASSIGNING) ;
    delete prefix_type ;
    _present_scope = save_scope ;
    return ALIVE ;
}

/*--------------------------------------------------------------------------*/

// Traverse delay control statement
unsigned
VeriDelayControlStatement::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    if (_stmt) action = _stmt->VarUsage(info, action) ;

    _present_scope = save_scope ;
    return action ;
}

/*--------------------------------------------------------------------------*/

// Traverse conditional statement
unsigned
VeriConditionalStatement::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // process the condition in the enclosing CurrStatus scope
    if (_if_expr) _if_expr->VarUsage(info, USING) ;

    // check type of condition
    VeriTypeInfo *cond_type = (_if_expr) ? _if_expr->CheckType(0, READ) : 0 ; ; // condition is context independent
    delete cond_type ;

    // VIPER #8278: Check for generate conditional along with in always/initial flag
    if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial())) {
        _present_scope = save_scope ;
        return action ; // Don't traverse if outside a data flow
    }

    // An array to store the assignment info for the THEN, and ELSE regions
    Array array_of_info(2);

    // setup the assignment info for the THEN region
    VeriVarUsageInfo *local_info = info->Copy() ;

    // If there were multiple edges in the upper data flow, then under a condition, these disappear (because the 'then' area is asynchronous):
    if (local_info->GetNumEdges()>1) local_info->SetNumEdges(0) ;

    if (_then_stmt) action = _then_stmt->VarUsage(local_info, USING) ;

    int all_dead = 1;
    if (action == DEAD) {
        // We used to ignore (delete) a dataflow branch if it ended in 'dead' code.
        // However, that is incorrect. See VIPER 3982. 'ConcurrentUsed' data would not be set.
        // So reconsider : A 'dead' branch will conservatively not return.
        // So, we could consider it to be a 'concurrent' action branch. Treat the same way as the end of a subprogram.
        // That should be correct for 'ever-used', 'ever-assigned' and 'fully-assigned' data.
        // So mimic 'end-of-dataflow' for this branch :
        local_info->UpdateConcurrentVarStatus() ;
        delete local_info ; // ignore this sub flow
        action = ALIVE ; // bring back alive for 'else' part
    } else {
        all_dead = 0;
        array_of_info.Insert(local_info) ;
    }

    // there always is an ELSE statement -- explicit or implicit.
    // setup the assignment info for the ELSE region
    local_info = info->Copy() ;

    // If there were multiple edges in the upper data flow, then under the 'else' condition these decrement.
    // Once there is a single edge, we no longer decrement (because that is the clocked area).
    if (local_info->GetNumEdges()>1) local_info->SetNumEdges(local_info->GetNumEdges()-1) ;

    if (_else_stmt) action = _else_stmt->VarUsage(local_info, USING) ;

    if (action == DEAD) {
        // VIPER 3982 : mimic 'end-of-dataflow' for this branch :
        local_info->UpdateConcurrentVarStatus() ;
        delete local_info ; // ignore this sub flow
        action = ALIVE ;
    } else {
        array_of_info.Insert(local_info) ;
        all_dead = 0 ;
    }

    // Combine the VarStatus's from the arms of the IF into
    // an overall VarStatus (i.e. into InitialVarStatus).
    // The array can be empty if all the arms of the IF are DEAD.
    // In this case, we want the VarStatus (i.e. initialVarStatus)
    // at entry into the IF to be the final VarStatus
    if (array_of_info.Size() > 0) {
        info->Combine(array_of_info) ;

        // cleanup local CurrStatusArray
        VeriVarUsageInfo *v ;
        unsigned i;
        FOREACH_ARRAY_ITEM(&array_of_info, i, v) delete v ;
    }
    _present_scope = save_scope ;
    return (all_dead) ? DEAD : ALIVE ;
}

/*--------------------------------------------------------------------------*/

// Traverse case statement
unsigned
VeriCaseStatement::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // Process the case expression :
    if (_cond) _cond->VarUsage(info, USING) ;

    // VIPER #8278: Check for generate conditional along with in always/initial flag
    if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial())) {
        _present_scope = save_scope ;
        return action ; // Don't traverse if outside a data flow
    }

    // An array to store the assignment info for the THEN, and ELSE regions
    Array array_of_info((_case_items) ? _case_items->Size() + 1 : 1);

    // traverse the case item expressions and bodies.  Also calculate
    // the whether all paths through this case statement end in a return,
    // next, or exit. In VERI the case statement is defined to always be
    // FULL -- thus we don't have to do a "fullness" check here.
    int all_caseitems_are_dead = 1 ;
    unsigned default_found = 0 ;
    unsigned i ;
    VeriCaseItem *item ;
    FOREACH_ARRAY_ITEM(_case_items, i, item) {
        if (!item) continue;

        // make sure the case choices are evaluated in the "initialVarStatus" context
        // and that the body of the caseAlternative is evaluated in a
        // separate copy of "initialVarStatus"
        // Since there are no assignments allowed in the choices, we can
        // safely use the same CurrStatus for both.
        VeriVarUsageInfo *local_info = info->Copy() ;

        action = item->VarUsage(local_info, USING) ;
        if (action == DEAD) {
            // VIPER 3982 : mimic 'end-of-dataflow' for this branch :
            local_info->UpdateConcurrentVarStatus() ;
            delete local_info ;
        } else {
            all_caseitems_are_dead = 0;
            array_of_info.Insert(local_info) ;
        }
        default_found |= item->IsDefault() ;
    }

    // Add a dummy case item to account for a non-full
    // case or a case without a default statement
    if ( !IsFullCase() && !default_found ) {
        VeriVarUsageInfo *local_info = info->Copy() ;
        all_caseitems_are_dead = 0;
        array_of_info.Insert(local_info) ;
    }

    // Combine the VarStatus's from the arms of the CASE into
    // an overall VarStatus (i.e. into InitialVarStatus).
    // The array can be empty if all the arms of the CASE are DEAD.
    // In this case, we want the VarStatus (i.e. initialVarStatus)
    // at entry into the IF to be the final VarStatus
    if (array_of_info.Size() > 0) {
        info->Combine(array_of_info) ;

        // cleanup local CurrStatusArray
        VeriVarUsageInfo *v ;
        FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
            if (v) delete v ;
        }
    }

    _present_scope = save_scope ;
    // if all the case items ended in DEAD control flow, then the entire
    // case statement is DEAD
    return (all_caseitems_are_dead) ? DEAD : ALIVE;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriForever::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    // We always go though the statement at least once. So no need to create separate data flow.
    if (_stmt) (void) _stmt->VarUsage(info, USING) ;

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    _present_scope = save_scope ;

    return action ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriRepeat::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    if (_cond) _cond->VarUsage(info, USING) ;

    // Then the statement itself. Note that the loop might not execute at all,
    // so we should not apply assignments inside the loop to the current 'info' structure.
    // So duplicate the 'info' structure and throw it away when we are done with the statement/repeat :
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    VeriVarUsageInfo *loop_info = (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) ? info->Copy() : 0 ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    if (_stmt) (void) _stmt->VarUsage(loop_info, USING) ;

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    // Now merge the dataflow back into the main flow,
    // In essence the two flows are mutually exclusive (like an if):
    // Either loop executes at least once, or not at all. So treat it as an if :
    Array array_of_info(2) ;
    array_of_info.Insert(info) ;
    array_of_info.Insert(loop_info) ;
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) info->Combine(array_of_info) ;

    delete loop_info ;

    _present_scope = save_scope ;

    return action ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriWhile::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    if (_cond) _cond->VarUsage(info, USING) ;

    // Then the statement itself. Note that the loop might not execute at all,
    // so we should not apply assignments inside the loop to the current 'info' structure.
    // So duplicate the 'info' structure and throw it away when we are done with the statement/repeat :
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    VeriVarUsageInfo *loop_info = (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) ? info->Copy() : 0 ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    if (_stmt) (void) _stmt->VarUsage(loop_info, USING) ;

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    // Now merge the dataflow back into the main flow,
    // In essence the two flows are mutually exclusive (like an if):
    // Either loop executes at least once, or not at all. So treat it as an if :
    Array array_of_info(2) ;
    array_of_info.Insert(info) ;
    array_of_info.Insert(loop_info) ;
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) info->Combine(array_of_info) ;

    delete loop_info ;

    _present_scope = save_scope ;

    return action ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriFor::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    // First scan the 'initials' : they are always executed first :
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_initials, i, item) {
        if (item) (void) item->VarUsage(info, IsAms() ? GENVAR_ASSIGN: action) ;
    }

    // Now scan the condition (also always executed) :
    if (_cond) _cond->VarUsage(info, USING) ;

    // Then the statement itself. Note that the loop might not execute at all,
    // so we should not apply assignments inside the loop to the current 'info' structure.
    // So duplicate the 'info' structure and throw it away when we are done with the statement/repeat :
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    VeriVarUsageInfo *loop_info = (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) ? info->Copy() : 0 ;

    if (_stmt) (void) _stmt->VarUsage(loop_info, action) ;

    // After the statement is done, scan the 'repeat' statements.
    // There happen only conditional (if the loop executes at least once)
    // so use the loop_info table.
    FOREACH_ARRAY_ITEM(_reps, i, item) {
        if (item) (void) item->VarUsage(loop_info, IsAms() ? GENVAR_ASSIGN: action) ;
    }

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    // Now merge the dataflow back into the main flow,
    // In essence the two flows are mutually exclusive (like an if):
    // Either loop executes at least once, or not at all. So treat it as an if :
    Array array_of_info(2) ;
    array_of_info.Insert(info) ;
    array_of_info.Insert(loop_info) ;
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) info->Combine(array_of_info) ;

    // Delete the var-usage structure which contains assignments which might never be executed :
    delete loop_info ;

    _present_scope = save_scope ;

    return action ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriDoWhile::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    // Statement gets executed at least once. So no need to create separate 'info' flow.
    if (_stmt) (void) _stmt->VarUsage(info, USING) ;

    // Condition evaluation happens after the loop :
    if (_cond) _cond->VarUsage(info, USING) ;

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    _present_scope = save_scope ;

    return action ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriForeach::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    if (IsStaticElab()) {
        Resolve(save_scope, VERI_UNDEF_ENV) ;
    }
    // Array name and loop index variables are always executed
    if (_array) _array->VarUsage(info, USING) ;

    // Then the statement itself. Note that the loop might not execute at all,
    // so we should not apply assignments inside the loop to the current 'info' structure.
    // So duplicate the 'info' structure and throw it away when we are done with the statement/repeat :
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    VeriVarUsageInfo *loop_info = (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) ? info->Copy() : 0 ;

    VeriVarUsageInfo::PushWithinLoop() ; // Increase within_loop global

    if (_stmt) (void) _stmt->VarUsage(loop_info, action) ;

    VeriVarUsageInfo::PopWithinLoop() ; // Decrease within_loop global

    // Now merge the dataflow back into the main flow,
    // In essence the two flows are mutually exclusive (like an if):
    // Either loop executes at least once, or not at all. So treat it as an if :
    Array array_of_info(2) ;
    array_of_info.Insert(info) ;
    array_of_info.Insert(loop_info) ;
    // VIPER #8278: Check for generate conditional along with in always/initial flag
    if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) info->Combine(array_of_info) ;

    // Delete the var-usage structure which contains assignments which might never be executed :
    delete loop_info ;

    _present_scope = save_scope ;

    return action ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriArrayMethodCall::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // Traverse the method call
    if (_left) (void) _left->VarUsage(info, action) ;

    if (IsStaticElab() && _left && _right && !_right->IsRange() && _scope) {
        // VIPER #6243 : Get the array iterator variable and set data type
        // to that. Data type may change due to elaboration
        VeriIdDef *iterator = _scope ? _scope->FindLocal("item"): 0 ;
        //  Extract the array id whose data type is to be set to iterator
        VeriExpression *array_ref = _left->GetTaskName() ;
        array_ref = (array_ref) ? array_ref->GetPrefix(): 0 ;
        VeriIdDef *array_id = array_ref ? array_ref->GetId() : 0 ;
        if (iterator && array_id && array_id->GetDataType()) {
            iterator->ClearDataType() ;
            iterator->SetDataType(array_id->GetDataType()) ; // Set array id's data type
        }
        // VIPER #6243 : Array method call can have arguments and those arguments
        // are explicitly declared iterators. So set data types to those also
        Array *args = _left->GetArgs() ;
        unsigned i ;
        VeriExpression *arg ;
        FOREACH_ARRAY_ITEM(args, i, arg) {
            iterator = arg ? arg->GetId(): 0 ;
            if (iterator && array_id && array_id->GetDataType()) {
                iterator->ClearDataType() ;
                iterator->SetDataType(array_id->GetDataType()) ; // Set array id's data type
            }
        }
    }
    // Traverse with expression
    if (_right) _right->VarUsage(info, action) ;
    _present_scope = save_scope ;
    return action ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriInlineConstraintStmt::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ; // Save current scope
    if (_scope) _present_scope = _scope ; // Push current scope

    // Traverse the method call
    if (_left) (void) _left->VarUsage(info, action) ;

    // Traverse the constraint block
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_constraint_block, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
        // VIPER #5154 : Check type validation for operands of constraint expression
        VeriTypeInfo *t = expr ? expr->CheckType(0, READ): 0 ;
        delete t ;
    }
    _present_scope = save_scope ; // Pop current scope
    return action ;
}

/*--------------------------------------------------------------------------*/

// Traverse case operator
void
VeriCaseOperator::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Process the case expression :
    if (_cond) _cond->VarUsage(info, USING) ;

    // An array to store the assignment info for the THEN, and ELSE regions
    Array array_of_info((_case_items) ? _case_items->Size() + 1 : 1);

    // traverse the case item expressions and bodies.  Also calculate
    // the whether all paths through this case statement end in a return,
    // next, or exit. In VERI the case statement is defined to always be
    // FULL -- thus we don't have to do a "fullness" check here.
    //int all_caseitems_are_dead = 1 ;
    //unsigned default_found = 0 ;
    unsigned i ;
    VeriCaseOperatorItem *item ;
    FOREACH_ARRAY_ITEM(_case_items, i, item) {
        if (!item) continue;

        // make sure the case choices are evaluated in the "initialVarStatus" context
        // and that the body of the caseAlternative is evaluated in a
        // separate copy of "initialVarStatus"
        // Since there are no assignments allowed in the choices, we can
        // safely use the same CurrStatus for both.
        VeriVarUsageInfo *local_info = info->Copy() ;

        action = item->VarUsage(local_info, USING) ;
        if (action == DEAD) {
            // VIPER 3982 : mimic 'end-of-dataflow' for this branch :
            local_info->UpdateConcurrentVarStatus() ;
            delete local_info ;
        } else {
            //all_caseitems_are_dead = 0;
            array_of_info.Insert(local_info) ;
        }
        //default_found |= item->IsDefault() ;
    }

    // Add a dummy case item to account for a non-full
    // case or a case without a default statement
    //if (!default_found ) {
    //    VeriVarUsageInfo *local_info = info->Copy() ;
    //    all_caseitems_are_dead = 0;
    //    array_of_info.Insert(local_info) ;
    //}

    // Combine the VarStatus's from the arms of the CASE into
    // an overall VarStatus (i.e. into InitialVarStatus).
    // The array can be empty if all the arms of the CASE are DEAD.
    // In this case, we want the VarStatus (i.e. initialVarStatus)
    // at entry into the IF to be the final VarStatus
    if (array_of_info.Size() > 0) {
        info->Combine(array_of_info) ;

        // cleanup local CurrStatusArray
        VeriVarUsageInfo *v ;
        FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
            if (v) delete v ;
        }
    }
}

/*--------------------------------------------------------------------------*/

unsigned
VeriCaseOperatorItem::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    // Traverse choices
    VeriExpression *dr;
    unsigned i;
    FOREACH_ARRAY_ITEM(_conditions, i, dr) {
        if (dr) dr->VarUsage(info, action) ;
    }

    // Traverse statement
    if (_property_expr) _property_expr->VarUsage(info, action) ;

    return action ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriSeqBlock::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // Traverse the declarations (could contain local decls with initial assignments)
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decl_items, i, item) {
        // VIPER #6408: Ignore Let declarations, we will call them with usage:
        if (!item || item->IsLetDecl()) continue ;
        action = item->VarUsage(info, action) ;
    }

    // Traverse the statements :
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) action = stmt->VarUsage(info, action) ;
    }

    _present_scope = save_scope ;

    return action ;
}
// VIPER #7856 : Check disable statement specified task/block
unsigned
VeriDisable::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    if (action == DEAD) return DEAD ;
    if (!IsStaticElab()) return action ; // Check only in static elab

    VeriIdDef *task_block_id = _task_block_name ? _task_block_name->GetId(): 0 ;
    if (!task_block_id) return action; // still unresolved

    unsigned is_block = task_block_id->IsBlock() ;
    if (is_block) {
        // Discard generate blocks
        VeriScope *scope = task_block_id->LocalScope() ;
        if (!scope && !task_block_id->GetModuleItem()) is_block = 0 ; // No scope, no module item (illegal)
        if (scope && scope->IsGenerateScope()) is_block = 0 ;
    }
    // Check if this is appropriate for a disable statement : LRM A.6.5 :
    if (!task_block_id->IsTask() && !is_block) {
        Error("%s is not a task or block", task_block_id->Name()) ;
    }
    return action ;
}
/*--------------------------------------------------------------------------*/
// VIPER #5710(vcs_compatibility: always_checker):
// VarUsage is called only after all modules are analyzed.
// Use that to fully resolve all module instantiations.
// Lookup the module.
// This will find the module in the work library, or if not there,
// it will find it in the 'uselib' library, or if not there
// it will return 0 (unknown module).
unsigned
VeriSequentialInstantiation::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    // Check for var usage only if we are instantiating a checker module:
    VeriModule *module = GetInstantiatedModule() ;
    if (module && !module->IsChecker())
    {
        Error("instantiation is not allowed in sequential area except checker instantiation") ;
        return VALUE_IGNORED ;
    }
    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // VIPER #6873: Produce error if it is not checker instance
    VeriIdDef *resolved_id = _module_name ? _module_name->GetId(): 0 ;
    if ((!module && !resolved_id) || (resolved_id && !resolved_id->IsChecker())) {
        Error("instantiation is not allowed in sequential area except checker instantiation") ;
    }
    // Get the instantiated module ports (so that we can determine direction of assignments to actuals)
    VeriIdDef *module_id = (module) ? module->GetId() : 0 ;
    Array *formals = (module) ? module->GetPorts() : 0 ;
    VeriScope *formal_scope = (module) ? module->GetScope() : 0 ;
    // Keep track of the connected ports :
    Set connected_formals(POINTER_HASH, (formals) ? formals->Size() : 1) ;

    // Process the instances:
    unsigned i, j;
    VeriInstId *inst ;
    VeriExpression *expr ;
    VeriIdDef *formal ;
    FOREACH_ARRAY_ITEM(&_insts, j, inst) {
        if (!inst) continue ;

        // Verilog module instantiation.
        connected_formals.Reset() ;

        // Adjustment for the Array instances in type checking:
        // Get the number of dimensions of this instance (array_inst_dims > 0 for array instance):
        unsigned array_inst_dims = inst->Dimension() ;

        // Process the port_connections on these instances.
        // Do not call VeriInstId::VarUsage(), because that would only mean a 'usage' of the InstId.
        // Instead, we want to process the port_connect actuals here :
        unsigned found_dot_star = 0 ;
        FOREACH_ARRAY_ITEM(inst->GetPortConnects(), i, expr) {
            if (!expr) continue ;
            // Skip DotStar.. FIX ME : could do something with that, since we have all formals.
            if (expr->IsDotStar()) {
                found_dot_star = 1 ;
                continue ;
            }
            // Find the port here :
            formal = 0 ;
            if (expr->NamedFormal()) {
                // This is a named formal.
                formal = (formal_scope) ? formal_scope->FindLocal(expr->NamedFormal()) : 0 ;
                if (module && !formal) {
                    // Cannot immediately error out, since module could have 'named' port expressions itself,
                    // which do not create identifiers in the scope. In that case, scan the port-connects of the module.
                    unsigned found_it = 0 ;
                    Array *port_connects = module->GetPortConnects() ;
                    unsigned k ;
                    VeriExpression *port_connect ;
                    FOREACH_ARRAY_ITEM(port_connects, k, port_connect) {
                        if (!port_connect) continue ;
                        if (!port_connect->NamedFormal()) continue ;
                        if (Strings::compare(port_connect->NamedFormal(),expr->NamedFormal())) {
                            found_it = 1 ;
                            // We do not have an identifier here..(May be we should have created one?).
                            break ;
                        }
                    }
                    // Pff. All this just for an error message...
                    if (!found_it) {
                        expr->Error("%s has no port called %s",module->Name(),expr->NamedFormal()) ;
                        if (module_id) module_id->Info("%s is declared here",module_id->Name()) ;
                    }
                }
                if (formal && !formal->IsPort()) {
                    expr->Error("no definition for port %s", formal->Name()) ; // VIPER #8172
                    formal = 0 ;
                }
            } else {
                // This is a ordered formal
                formal = (formals && (formals->Size() > i)) ? (VeriIdDef*)formals->At(i) : 0 ;
            }

            // add this as a connected formal :
            if (formal && !connected_formals.Insert(formal)) {
                // Formal is already connected. Complain :
                // Do not complain if this was a 'ordered' actual :
                // There could be multiple ports with same name in the module, "module foo(a,a);" which is allowed in
                // Verilog (LRM 12.3.3). We are just not allowed to have two named ports in instantiation :
                // "mod inst (.b(),.b());" is NOT allowed (LRM 12.3.6).
                // FIX ME : We can check this in Resolve(), based on just the formal names.
                // That can be done before the actual module is found, since Verilog does not allow mixed named
                // and positional associations.
                if (expr->NamedFormal()) {
                    expr->Error("port %s is already connected", formal->Name()) ;
                }
            }

            if (!formal) continue ;

            // Adjustment for the Array instances in type checking:
            // We need to adjust the unpacked dimensions of the formal <-> actual pair for the
            // array instances. Since we don't exactly know about the situation unless we evaluate
            // the ranges we will only adjust the dimensions without depending on its size. We can't
            // evaluate a range properly, because it can contain parameters and those parameters
            // might be overridden from elesewhere. Thus, our moto is to adjust the unpacked dimensions
            // so that we don't produce error in legal designs. We may be silent on illegal designs:
            if (array_inst_dims) { // array instance
                // So actual type can be same as formal or array of formal type so that
                // each array element can be used for every single instance of the array instance
                // Create formal type
                VeriTypeInfo *formal_type = formal->CreateType(0) ;

                if (!formal_type) continue ; // Nothing can be checked without formal type
                unsigned is_assignment_pattrn = (expr->IsConcat() || expr->IsMultiConcat() ||  expr->IsAssignPattern() || expr->IsMultiAssignPattern()) ? 1 : 0 ;
                VeriTypeInfo *actual_type = expr->CheckType(is_assignment_pattrn ? formal_type : 0 /*formal_type*/,
                                            (formal->IsOutput()) ? WRITE : (formal->IsInterfacePort() ? READ_INSTANCE: READ)) ;
                if (actual_type && (actual_type->IsTypeParameterType() || actual_type->IsUnresolvedNameType())) {
                    delete actual_type ; actual_type = 0 ;
                }

                if (actual_type && (actual_type->UnpackedDimension() > formal_type->UnpackedDimension())) { // Actual type cannot be determined
                    // Try again with array of formal type
                    Array idx_arr(array_inst_dims) ;
                    for (unsigned idx = 0; idx < array_inst_dims; idx++) idx_arr.InsertLast(0) ;
                    actual_type = actual_type->ReduceDimBy(&idx_arr, 0) ;
                }
                if (actual_type) (void) actual_type->CheckAgainst(formal_type, expr, (formal->IsOutput()) ? WRITE : READ) ;
                delete actual_type ;
                delete formal_type ;
            } else {
                // Checker ports are like sequence/property formals, so for checker block
                // we pass 'PROP_SEQ' as 'veri_type_env':
                (void) expr->CheckAssociation(formal, PROP_SEQ) ;
            }
            formal->SetActual(expr->GetConnection()) ;
        }

        // Now check if all formal ports have an actual connection:
        if (!found_dot_star && formals && (formals->Size() != connected_formals.Size())) {
            // There is a mismatch need to produce error messages:
            FOREACH_ARRAY_ITEM(formals, i, formal) {
                if (!formal || connected_formals.GetItem(formal)) continue ;
                // VIPER #7466: This is error in all the standard simulators those support checkers:
                Error("port %s is not connected to this instance", formal->Name()) ;
            }
        }

        // This is a checker instance, process its body
        VeriModuleItem *body = module_id ? module_id->GetModuleItem() : 0 ;
        if (body) (void) body->VarUsage(info, action) ;
        FOREACH_ARRAY_ITEM(formals, i, formal) {
            if (formal) formal->SetActual(0) ; // Reset actual setting :
        }
    }
    _present_scope = save_scope ;
    return VALUE_IGNORED ;
}

unsigned
VeriAssertion::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;
    (void) info ;

    // VIPER #5940 : if IGNORE_ASSERTION_STATEMENTS is on, do not process assertion
    if (RuntimeFlags::GetVar("veri_ignore_assertion_statements")) return action ;

    // Commented out for Viper #4191
    // if (!IsConcurrentAssertion()) return action ; // No need to process immediate assertion

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // Create a VarUsageInfo class to process property spec. Mark in the class
    // that we are going to process temporal expression :
    Map id_table(POINTER_HASH) ;

    VeriVarUsageInfo *local_info = (info) ? info->Copy() : new VeriVarUsageInfo(&id_table) ;
    if (IsConcurrentAssertion()) local_info->SetInTemporalExpr() ; // Mark to process temporal expr :
    if (_property_spec) _property_spec->VarUsage(local_info, USING) ;

    if (info) {
        // If 'info' is present, merge the created 'info' with old one
        Array info_arr(1) ;
        info_arr.InsertLast(local_info) ;
        info->Combine(info_arr) ;
    }
    delete local_info ;

    // VIPER #5633: Create a VarUsageInfo class to process then statement.
    // Clocking block signals can only be driven by non-blocking assignments.
    // If coming 'info' is null and '_then_stmt' contains non-blocking assignments,the
    // VarUsage of its lvalue is called with 'info' null and action 'NONBLOCKING_ASSIGNING'.
    // This condition becomes similar to lvalue of continuous assignment. So to
    // differentiate non-blocking assignment within '_then_stmt' and continuous
    // assignment as module item, a 'local_info' is created here.
    if (_then_stmt) {
        id_table.Reset() ;
        local_info = (info) ? info->Copy() : new VeriVarUsageInfo(&id_table) ;
        (void) _then_stmt->VarUsage(local_info, USING) ; // Viper #4191
        delete local_info ;
    }

    if (_else_stmt) {
        // VIPER #7089: Also call VarUsage() on the else statement part:
        id_table.Reset() ;
        local_info = (info) ? info->Copy() : new VeriVarUsageInfo(&id_table) ;
        (void) _else_stmt->VarUsage(local_info, USING) ;
        delete local_info ;
    }

    _present_scope = save_scope ;
    return action ;
}

/*--------------------------------------------------------------------------*/
// This statement represents a RETURN, BREAK, or CONTINUE

unsigned
VeriJumpStatement::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // Process the expression (if there).
    if (_expr) _expr->VarUsage(info, USING) ;

    // A 'return' statement will trigger an 'assignment' to the function variable (if there is an expression)
    if (_jump_id && _jump_token==VERI_RETURN && _expr) {
        // assignment to _jump_id
        _jump_id->VarUsage(info, BLOCKING_ASSIGNING,/*from*/this) ;
        // Check type of return statement in the context of function type
        VeriIdRef id_ref(_jump_id) ;
        id_ref.SetLinefile(Linefile()) ;
        id_ref.CheckAssignmentContext(_expr, RETURN_EXPR) ;
    }

    _present_scope = save_scope ;
    // everything after the jump (in the same DF region) is ignored
    // by changing the Action to DEAD
    return DEAD ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriEventControlStatement::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    // Event expressions are not really 'used', so use special environment for the sensitivity list :
    unsigned i ;
    VeriExpression *event_expr ;
    unsigned edges = 0 ;
    FOREACH_ARRAY_ITEM(_at, i, event_expr) {
        if (!event_expr) continue ;
        event_expr->VarUsage(info, EVENT_USAGE) ;
        if (event_expr->IsEdge(0)) edges++ ;
    }

    // If there are edges on the sensitivity list, then the statement is under 'edgecontrol' :
    if (info) info->SetNumEdges(edges) ;

    // Scan only the statement
    if (_stmt) action = _stmt->VarUsage(info, USING) ;

    _present_scope = save_scope ;
    return action ;
}

/*--------------------------------------------------------------------------*/
/*Begin VeriExpression derived objects

VeriAnsiPortDecl
VeriUnaryOperator
VeriBinaryOperator
VeriCast
VeriConcatItem
VeriAssignmentPattern
VeriMultiAssignmentPattern
VeriStreamingConcat
VeriDotName
VeriTaggedUnion
VeriWithExpr
VeriInlineConstraint
VeriConcat
VeriEventExpression
VeriFunctionCall
VeriSystemFunctionCall
VeriMultiConcat
VeriName
VeriIdRef
VeriPortConnect
VeriQuestionColon

NOT implemented -----------------------

VeriRange // contains only constant exprs
VeriConst
VeriDotStar
VeriMinTypMaxExpr
VeriNew
VeriPortOpen
VeriSolveBefore
VeriTimingCheckEvent

*/
/*--------------------------------------------------------------------------*/

void
VeriExpression::VarUsage(VeriVarUsageInfo * /*info*/, unsigned /*action*/)
{
    // Do Nothing here.  All derived classes will define their own
    // specific version of VarUsage
}

/*--------------------------------------------------------------------------*/

// Data types : not really part of VarUsage, but some resolving might be needed for System Verilog 'interface' data types.
// FIX ME : This is really a 'Resolve' issue, and should run that after all modules are analyzed.
void
VeriDataType::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    // Do Nothing here.  All derived classes will define their own
    // specific version of VarUsage
    // VIPER #5238: Check for valid type of prefix of scope name.
    if(_dimensions) _dimensions->VarUsage(info, action) ;
    (void) info ;
    (void) action ;
}

/*--------------------------------------------------------------------------*/
void
VeriTypeRef::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (_id) return ; // already resolved

    // We only get here if we need to have an interface port type resolved.
    // Check if the target is resolved. If not, error out.
    // Resolve the type-reference (if not yet done)
    if (_type_name) _type_name->VarUsage(info, action) ; // expect a 'type' identifier in here..

    // Pick up/and set the resolve identifier (if there) :
    _id = (_type_name) ? _type_name->GetId() : 0 ;
    if (!_id) return ;

    // VIPER #6404: Check proper usage of this id as type:
    _id->CheckEnvironment(VERI_TYPE_ENV, this, 0) ;

    if (_dimensions) {
        // VIPER #7430: Check for packed dimensions with interface used as type.
        switch (_id->Type()) {
        case VERI_ENUM :
            {
            // VIPER 5880. Issue cannot have packed dimensions of type enum error for enum types
            VeriDataType *id_type = _id->GetDataType() ;
            // VIPER #6215: Only error out in case the base size of enum is > 1:
            if (!id_type || (id_type->BaseTypeSize() <= 1)) break ;
            }
        case VERI_CLASS :
        case VERI_INTERFACE :
            // VCS and Modelsim now both issue an error on this.
            Error("cannot have packed dimensions of type %s", _id->Name()) ;
            delete _dimensions ; // Don't let it enter the parse tree, to avoid problems elsewhere.
            _dimensions = 0 ;
            break ;
        default :
            break ;
        }
    }
}

/*--------------------------------------------------------------------------*/
// VIPER #4017 : Check all members of struct-union to produce error for undeclared type of all members :
void
VeriStructUnion::VarUsage(VeriVarUsageInfo *info, unsigned /*action*/)
{
    unsigned i ;
    VeriDataDecl *decl ;
    VeriDataType *data_type ;

    verific_uint64 prev_size = 0  ;
    // VIPER #5593: Flag for packed union data type:
    unsigned packed_union = 0 ;
    if (IsStaticElab() && _is_packed && IsUnionType()) packed_union = 1 ;

    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        data_type = (decl) ? decl->GetDataType() : 0 ;
        // VIPER 5593: A packed union shall contain members that must be packed
        // structures or packed arrays or integer data types all of the same size.
        // Error out if the members are of different size.
        // VIPER #7086: Tagged union can have members of different sizes.
        if (packed_union && !_is_tagged) {
            verific_int64 static_size_sign = data_type ? data_type->StaticSizeSignInternal(0, 0) : 0 ;
            verific_uint64 size = (verific_uint64) ABS(static_size_sign) ;
            if (prev_size && size && prev_size != size) {
                Error("packed union contains members of different size") ;
            }
            prev_size = size ;
        }

        if (data_type) data_type->VarUsage(info, TYPE_USAGE) ;
    }
}

void
VeriEnum::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    // VIPER #7914: Check data type of enum:
    if (_base_type) _base_type->VarUsage(info, action) ;
    if (_dimensions) _dimensions->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

void
VeriAnsiPortDecl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Like a DataDecl, but we need to still resolve System Verilog
    // 'interface' (or modport) types, since these could be in the data type of a ANSI port.
    // The other spot where interfaces need to be resolved is in module instantiation.
    if (_data_type) {
        _data_type->VarUsage(info, TYPE_USAGE) ;
    }

    // Traverse the ansi port decls.
    int i;
    VeriIdDef *id;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;

        VeriExpression *rhs = id->GetInitialValue() ;
        if (rhs) {
            // process the initial value expression :
            rhs->VarUsage(info, USING) ;

            // Issue 2833 : Initial value on a (module) port reg is (just like in VeriDataDecl) actually only an 'initial' assignment.
            // Also, any initial value on (module) port nets is also 'initial' (not a concurrent assignment).
            // Finally, a function or task port WILL be a (blocking) assignment (if input without actual) but
            // since we are only processing the body, that assignment is already taken care of in the function/task decl of VarUsage.
            // So we should not ever see this initial value as an assignment.
            // id->VarUsage(info, BLOCKING_ASSIGNING, 0) ;
        }
    }
}

/*--------------------------------------------------------------------------*/

void
VeriUnaryOperator::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    if (info && info->GetInTemporalExpr()) {
        // If we are inside temporal expression, mark the info before processing
        // the operands to produce error if method ended is used in operands and
        // local variable is passed to the sequence on which ended is applied.
        VeriVarUsageInfo *local_info = info->Copy() ;
        if (local_info) local_info->SetInOperand() ;
        // Mark the operand of property operator 'not' (VIPER #3949)
        if ((_oper_type == VERI_NOT) && local_info) local_info->SetInNotOperand() ;
        if (_arg) _arg->VarUsage(local_info, action) ;
        if (local_info) {
            Array info_arr(1) ;
            info_arr.InsertLast(local_info) ;
            info->Combine(info_arr) ;
        }
        delete local_info ;
        return ;
    }
    if (_arg) _arg->VarUsage(info, action) ;
}

/*--------------------------------------------------------------------------*/

void
VeriBinaryOperator::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Catch some oddbals first :
    // For SystemVerilog, assignment operators require that
    // the result be (blocking) assigned back to the LHS.
    // So we should also Resolve LHS with the blocking-assignment environment :
    switch (_oper_type) {
    case VERI_EQUAL_ASSIGN :
        // LRM 7.3 : these guys not allowed outside a sequential data flow :
        // These guys not allowed in temporal expressions :
        // VIPER #8278: Check for generate conditional along with in always/initial flag
        if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()) || info->GetInTemporalExpr()) {
            Error("assignment operator %s not allowed outside procedural statements", PrintToken(_oper_type)) ;
        }
        // Evaluate right and assign to left :
        if (_right) _right->VarUsage(info, action) ; // standard environment
        if (_left) _left->VarUsage(info, BLOCKING_ASSIGNING) ; // blocking assignment
        return ;

    case VERI_PLUS_ASSIGN :
    case VERI_MIN_ASSIGN :
    case VERI_MUL_ASSIGN :
    case VERI_DIV_ASSIGN :
    case VERI_MOD_ASSIGN :
    case VERI_AND_ASSIGN :
    case VERI_OR_ASSIGN :
    case VERI_XOR_ASSIGN :
    case VERI_LSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
        // LRM 7.3 : these guys not allowed outside a sequential data flow :
        // These guys are not allowed in temporal expressions :
        // VIPER #8278: Check for generate conditional along with in always/initial flag
        if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()) || info->GetInTemporalExpr()) {
            Error("assignment operator %s not allowed outside procedural statements", PrintToken(_oper_type)) ;
        }
        // Evaluate left and right and assign to left :
        if (_left) _left->VarUsage(info, action) ; // standard environment
        if (_right) _right->VarUsage(info, action) ; // standard environment
        if (_left) _left->VarUsage(info, BLOCKING_ASSIGNING) ; // blocking assignment
        return ;

        // same thing for the in-expression ++/-- operations :
    case VERI_INC_OP :
    case VERI_DEC_OP :
        // LRM 7.3 : these guys not allowed outside a sequential data flow :
        // These guys are not allowed in temporal expressions :
        // Viper #5324: event_usage check added
        // VIPER #8278: Check for generate conditional along with in always/initial flag
        if ((action == EVENT_USAGE) || !info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()) || info->GetInTemporalExpr()) {
            Error("assignment operator %s not allowed outside procedural statements", PrintToken(_oper_type)) ;
        }
        // Only one arm is set. Evaluate that one and then assign.
        if (_left) _left->VarUsage(info, action) ; // standard environment
        if (_right) _right->VarUsage(info, action) ; // standard environment
        if (_left) _left->VarUsage(info, BLOCKING_ASSIGNING) ; // blocking assignment
        if (_right) _right->VarUsage(info, BLOCKING_ASSIGNING) ; // blocking assignment
        return ;
    // VIPER #3958, #3960 : check usage of local variables of sequence/property
    case VERI_PROPERTY_AND :
    case VERI_SEQ_AND :
    case VERI_PROPERTY_OR :
    case VERI_SEQ_OR :
    case VERI_INTERSECT :
    {
        Array info_arr(2) ;
        VeriVarUsageInfo *left_info = info ? info->Copy() : 0 ;
        if (left_info) left_info->SetInOperand() ;
        info_arr.InsertLast(left_info) ;
        VeriVarUsageInfo *right_info = info ? info->Copy() : 0 ;
        if (right_info) right_info->SetInOperand() ;
        info_arr.InsertLast(right_info) ;

        if (_left) _left->VarUsage(left_info, action) ;
        if (_right) _right->VarUsage(right_info, action) ;

        if (info) info->CombineForAssertionOp(_oper_type, info_arr) ;
        delete left_info ; delete right_info ; // Leak fix for VIPER #2275 design
        return ;
    }
    case VERI_DISABLE :
        // VIPER #3948 : Disable iff can only be used as top-level of property
        // specification. If this disable is in an operand, produce error
        if (info && info->GetInOperand()) {
            Error("illegal context for disable iff") ;
        }
        // VIPER #3961 : In reset expression of disable statement, matched or
        // ended cannot be used
        if (_left) _left->VarUsage(info, DISABLE_RESET) ; // Pass different action for #3961
        if (_right) _right->VarUsage(info, action) ;
        return ;
    case VERI_CONSECUTIVE_REPEAT :
    case VERI_NON_CONSECUTIVE_REPEAT :
    case VERI_GOTO_REPEAT :
        // In these cases _right can be a constant expression or cycle delay range
        if (_left) _left->VarUsage(info, action) ;
        if (_right) _right->VarUsage(info, CYCLE_DELAY) ;
        return ;
    case VERI_NEXTTIME :
    case VERI_S_NEXTTIME :
    case VERI_EVENTUALLY :
    case VERI_S_EVENTUALLY :
    case VERI_ALWAYS :
    case VERI_S_ALWAYS :
        // In these cases _left can be a constant expression, range or $
        if (_left) _left->VarUsage(info, CYCLE_DELAY) ;
        if (_right) _right->VarUsage(info, action) ;
        return ;
    default :
        break ;
    }
    if (info && info->GetInTemporalExpr()) {
        // If we are inside temporal expression, mark the info before processing
        // the operands to produce error if method ended is used in operands and
        // local variable is passed to the sequence on which ended is applied.
        VeriVarUsageInfo *local_info = info->Copy() ;
        if (local_info) local_info->SetInOperand() ;
        if (_left) _left->VarUsage(local_info, action) ;
        if (_right) _right->VarUsage(local_info, action) ;
        if (local_info) {
            Array info_arr(1) ;
            info_arr.InsertLast(local_info) ;
            info->Combine(info_arr) ;
        }
        delete local_info ;
        return ;
    }

    // Default operator resolving : both sides are called with 'action' environment
    if (_left)  _left->VarUsage(info, action) ;

    if (_right) _right->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

void
VeriConcat::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    int i;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
}

/*--------------------------------------------------------------------------*/

// Traverse multi concat expression
void
VeriMultiConcat::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;
    // the repetition expression is constant, don't traverse

    // Traverse the elements of concatenation
    int i;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
}

/*--------------------------------------------------------------------------*/

void
VeriCast::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    if (_expr) _expr->VarUsage(info, action) ;
}

void
VeriConcatItem::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Label contains only constants ? Not needed.

    // Process the expression :
    if (_expr) _expr->VarUsage(info, action) ;
}

/*--------------------------------------------------------------------------*/
// Traverse the assignment pattern expression
void
VeriAssignmentPattern::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the (data type) prefix ? No : it contains no variables.
    // if (_type) _type->VarUsage(info, action) ;

    // Simple traverse the base type (concat) :
    VeriConcat::VarUsage(info, action) ;
}

/*--------------------------------------------------------------------------*/

// Traverse the multi assignment pattern expression
void
VeriMultiAssignmentPattern::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the (data type) prefix ? No : it contains no variables.
    // if (_type) _type->VarUsage(info, action) ;

    // Simple traverse the base type (concat) :
    VeriMultiConcat::VarUsage(info, action) ;
}

/*--------------------------------------------------------------------------*/

// Traverse the streaming concatenation expression
void
VeriStreamingConcat::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Simple traverse the base type (concat) :
    VeriConcat::VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

// Traverse the cond predicate expression
void
VeriCondPredicate::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the operands
    if (_left) _left->VarUsage(info, action) ;
    if (_right) _right->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

// Traverse the ".<variable_identifier>" pattern
void
VeriDotName::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Simple traverse the name
    if (_name) _name->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/
// Traverse the tagged union expression
void
VeriTaggedUnion::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Need to traverse member name ?
    // Traverse the expression
    if (_expr) _expr->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

// Traverse the 'with' construct whose right hand side is expression
void
VeriWithExpr::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    if (_left) _left->VarUsage(info, action) ; // Traverse left hand side
    if (IsStaticElab() && _right && !_right->IsRange() && _scope) {
        // VIPER #5294 : Get the array iterator variable and set data type
        // to that. Data type may change due to elaboration
        VeriIdDef *iterator = _scope->FindLocal("item") ;
        Array *args = 0 ;
        //  Extract the array id whose data type is to be set to iterator
        VeriExpression *array_ref = _left ;
        while (array_ref) {
            if (array_ref->IsFunctionCall()) args = array_ref->GetArgs() ;
            if (array_ref->GetFunctionType() && !array_ref->IsFunctionCall()) { // 'array_ref' has function name as suffix
                // Its prefix is array reference
                array_ref = array_ref->GetPrefix() ;
                break ;
            }
            array_ref = array_ref->GetPrefix() ;
        }
        VeriIdDef *array_id = array_ref ? array_ref->GetId() : 0 ;
        if (iterator && array_id && array_id->GetDataType()) {
            iterator->ClearDataType() ;
            iterator->SetDataType(array_id->GetDataType()) ; // Set array id's data type
        }
        // VIPER #5771 : Array method call can have arguments and those arguments
        // are explicitly declared iterators. So set data types to those also
        unsigned i ;
        VeriExpression *arg ;
        FOREACH_ARRAY_ITEM(args, i, arg) {
            iterator = arg ? arg->GetId(): 0 ;
            if (iterator && array_id && array_id->GetDataType()) {
                iterator->ClearDataType() ;
                iterator->SetDataType(array_id->GetDataType()) ; // Set array id's data type
            }
        }
    }
    if (_right) _right->VarUsage(info, action) ; // Traverse right hand side
}
/*--------------------------------------------------------------------------*/

// Traverse the pattern expressions connected with 'matches' token
void
VeriPatternMatch::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    if (_left) _left->VarUsage(info, action) ; // Traverse left hand side
    if (_right) _right->VarUsage(info, action) ; // Traverse right hand side
}
/*--------------------------------------------------------------------------*/

// Traverse the constraint set expressions
void VeriConstraintSet::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) if (expr) expr->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/
// Traverse the 'with' construct whose right hand side is constraint block
void
VeriInlineConstraint::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    VeriScope *save_scope = _present_scope ; // Save current scope
    if (_scope) _present_scope = _scope ; // Push current scope

    if (_left) _left->VarUsage(info, action) ; // Traverse left hand side

    // Traverse constraint block
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_constraint_block, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
    _present_scope = save_scope ; // Pop current scope
}
/*--------------------------------------------------------------------------*/

//  Traverse the open range bin value
void
VeriOpenRangeBinValue::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the expression
    if (_iff_expr) _iff_expr->VarUsage(info, action) ;

    // Traverse the open range list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_open_range_list, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
}
/*--------------------------------------------------------------------------*/

// Traverse the transition list
void
VeriTransBinValue::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the expression
    if (_iff_expr) _iff_expr->VarUsage(info, action) ;

    // Traverse the trans list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_trans_list, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
}
/*--------------------------------------------------------------------------*/

// Traverse the default bin values ie. the bin values with 'default' or default sequence
void
VeriDefaultBinValue::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the expression
    if (_iff_expr) _iff_expr->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

// Traverse the select expressions specified in selection bin
void
VeriSelectBinValue::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the expression
    if (_iff_expr) _iff_expr->VarUsage(info, action) ;

    // Traverse the select expression
    if (_select_expr) _select_expr->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

// Traverse the list of trans range lists
void
VeriTransSet::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the range lists
    unsigned i ;
    VeriTransRangeList *range_list ;
    FOREACH_ARRAY_ITEM(_trans_range_list, i, range_list) {
        if (range_list) range_list->VarUsage(info, action) ;
    }
}
/*--------------------------------------------------------------------------*/

// Traverse a trans range list
void
VeriTransRangeList::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the range lists
    unsigned i ;
    VeriTransRangeList *expr ;
    FOREACH_ARRAY_ITEM(_trans_item, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
    // Traverse the repetition expression
    if (_repetition) _repetition->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

// Traverse the select condition
void
VeriSelectCondition::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the open range list
    unsigned i ;
    VeriExpression * expr;
    FOREACH_ARRAY_ITEM(_range_list, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
}
/*--------------------------------------------------------------------------*/

// Traverse the select condition
void
VeriTypeOperator::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the expression field
    if (_expr_or_data_type) {
        _expr_or_data_type->VarUsage(info, action) ;
        // VIPER #4578: Check for hier name with either data type or element of dynamic type
        // in type operator expression. LRM says it cannot contain hier-name or elements of
        // dynamic type. But other tools seem to allow hier-name to element of non-dynamic
        // type as well non-hier-name to dynamic type itself. So, we should behave similarly:
        unsigned dynamic_type = 0 ;
        VeriExpression *runner = _expr_or_data_type ;
        while ((runner = runner->GetPrefix()) && !dynamic_type) {
            VeriTypeInfo *type = runner->CheckType(0, NO_ENV) ;
            if (type && type->IsDynamicType()) dynamic_type = 1 ;
            delete type ;
        }
        unsigned type_id = 0 ;
        if (!dynamic_type) {
            // Other tools do not allow (variable declared with) named type, like "typedef" type:
            VeriIdDef *id = _expr_or_data_type->GetId() ;
            if (id && id->IsType()) type_id = 1 ; // A named type itself
            if (!type_id && id) {
                // Not a named type, but check if it is a variable of a named type:
                VeriDataType *id_type = id->GetDataType() ;
                if (id_type && id_type->IsTypeRef()) type_id = 1 ;
            }
        }
        if (_expr_or_data_type->IsHierName() && (dynamic_type || type_id || _expr_or_data_type->DataTypeCast())) {
            // Hier name with dynamic type container or reference to a named type or a data type itself:
            _expr_or_data_type->Error("type reference expression may not be hierarchical name or elements of dynamic type") ;
        }
    }
}

/*--------------------------------------------------------------------------*/

void
VeriEventExpression::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Discovered as part of VIPER 3457 :
    // If 'posedge clk' (or some other event) is used on the sensitivity list, then it DOES constitute a 'read' of the identifier.
    // So set action to 'using' for event expressions with an 'edge' :
    if (_expr) _expr->VarUsage(info, (_edge) ? USING : action) ;

    // Similar for 'iff' condition (or is this always a 'using' ?) :
    if (_iff_condition) _iff_condition->VarUsage(info, (_edge) ? USING : action) ;

    // Chandles cannot be used in event expression :
    VeriTypeInfo *expr_type = (_expr) ? _expr->CheckType(0, READ): 0 ;
    if (expr_type && expr_type->IsChandleType()) {
        Error("illegal reference to chandle type") ;
    }
    delete expr_type ;
}

/*--------------------------------------------------------------------------*/

void
VeriFunctionCall::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Cannot do anything without a function name.
    if (!_func_name) return ;

    // SV functions can also have outputs. So use same processing as a taske enable.
    VeriIdDef *function_id = _func_name->GetId() ;

    // If function id is from export created id without body, ignore that
    if (function_id && function_id->IsUnresolvedSubprog()) function_id = 0 ;

    if (function_id && (action == PROCEDURAL_CONTINOUS_ASSIGN || action == EVENT_USAGE)) {
        // P1800 lrm section 12.3 : VIPER 4606
        // It shall be illegal to call a function with output,
        // inout, or ref arguments in an event expression, in an expression within a
        // procedural continuous assignment, or in an expression that is not within a
        // procedural statement. However, a const ref function argument shall be legal in
        // this context
        VeriModuleItem *function_decl = function_id->GetModuleItem() ;
        unsigned i ;
        VeriAnsiPortDecl *port_decl ;
        Array *ansi_io_list = function_decl ? function_decl->GetIOList() : 0 ;
        FOREACH_ARRAY_ITEM(ansi_io_list, i, port_decl) {
            if (!port_decl) continue ;
            unsigned dir = port_decl->GetDir() ;
            unsigned is_ref_dir  = 0 ;
            is_ref_dir  = dir == VERI_REF ;
            if (dir == VERI_OUTPUT || dir == VERI_INOUT || is_ref_dir) {
                if (action == PROCEDURAL_CONTINOUS_ASSIGN) {
                    Warning("illegal %s port in function %s used in procedural continuous assignment statement", PrintToken(dir), function_id->Name()) ;
                } else {
                    Warning("illegal %s port in function %s used in event expression", PrintToken(dir), function_id->Name()) ;
                }
            }
        }
    }
    // VIPER #3211 : Check if this is appropriate for a function call. This check
    // is also done in Resolve. But if function name is defined hierarchically upwards,
    // it is resolved in elaboration and so we can error out here.
    if (function_id && !function_id->IsFunction()) {
        if (!function_id->IsSequence() && !function_id->IsProperty() && !function_id->IsLetId())
            Error("%s is not a function", function_id->Name()) ;
    }
//    if (!function_id) { // VIPER #6441: Always call the following VarUsage()
        // Here, post-analysis, we should be able to resolve the function name :
            _func_name->VarUsage(info, SUBPROG_NAME) ;

        // Try it again :
        // VIPER #6441: Set the function-id if not already set
        if (!function_id) function_id = _func_name->GetId() ;
//    }

    if (!function_id) {
        _function_type = _func_name->GetFunctionType() ;
        if (!_function_type && !_func_name->IsHierName()) {
            _function_type = GetFuncTokenOfClass(_func_name->GetName()) ;
        }
    }
    // If the prefix pertains to a 'method', then call the method resolver :
    // Note that we only call this if the normal Resolve on the prefix did not gain a known identifier.
    // Methods can be overridden by user-defined function (e.g. in classes).
    if (!function_id && _function_type) {
        // Call resolving the prefix as a 'method' function call, with the argument list in _args.
        _func_name->VarUsageMethod(info,action,_args) ;
        return ; // done. All checked.
    }

    if (!function_id) return ; // cannot do anything without a function name.

    unsigned check_association = 1 ;
    if (function_id->GetFunctionType() == VERI_METHOD_RANDOMIZE) {
        // It is calling of function 'randomize' which is defined in std package.
        // This function can have any number of inputs. So do not check port
        // connection for this function.
        check_association = 0 ;
    }
    // VIPER #4797 : Produce warning if 'extern' qualifier exist in method's body.
    // That means that no actual definition is associated with method id.
    if (function_id->IsExternForkjoinTask()) {
        // Get the definitions of extern/extern-fork-join tasks
        Array *bodies = function_id->GetExternForkjoinDefs() ;
        // Viper 4869. Added the body->GetQualifier(VERI_VIRTUAL) check
        //if (body && body->GetQualifier(VERI_EXTERN) && !body->GetQualifier(VERI_FORKJOIN) && !body->GetQualifier(VERI_VIRTUAL)) {
        if (!bodies || !bodies->Size()) {  // No actual definition
            VeriModuleItem *body = function_id->GetModuleItem() ; // get associated body
            if (body && body->GetQualifier(VERI_EXTERN) && !body->GetQualifier(VERI_FORKJOIN) && !body->GetQualifier(VERI_VIRTUAL)) {
                // Check whether it is inside package 'std'
                unsigned inside_std_pkg = 0 ;
                VeriScope *method_scope = body->GetScope() ;
                VeriIdDef *container_id = (method_scope) ? method_scope->GetContainingModule(): 0 ;
                if (container_id && Strings::compare(container_id->Name(), "std")) {
                    VeriModule *container = container_id->GetModule() ;
                    VeriLibrary *lib = (container) ? container->GetLibrary(): 0 ;
                    if (lib && Strings::compare(lib->GetName(), "std")) {
                        inside_std_pkg = 1 ;
                    }
                }
                if (!inside_std_pkg && body->IsVerilogMethod()) Warning("method never defined for %s %s", "function", function_id->Name()) ;
            }
        }
    }
    // Pick up the formals :
    Array *formals = function_id->GetPorts() ;
    VeriScope *formal_scope = function_id->LocalScope() ;
    Set associated_formals(POINTER_HASH) ;

    SynthesisInfo *this_info = 0 ;

    // traverse the actuals
    unsigned i ;
    VeriExpression *actual ;
    VeriIdDef *formal ;
    FOREACH_ARRAY_ITEM(_args, i, actual) {
        if (!actual) continue ;

        // Viper #7500: Disqualify RAM-ness for full access as function actual
        VeriIdDef *actual_full_id = actual->FullId() ;

        // Viper #7854: Even if the id is not yet marked as _can_be_ram (dp or mp),
        // it is important to discard them when improperly used. For external hier
        // names, they may later be marked as good for ram (from data-decl::var-usage).
        // The stikey flag on the id will ensure that an id discarded for ram can never
        // be marked as ram ever.
        if (actual_full_id) {
            SynthesisInfo *actual_info = 0 ;
            actual_full_id->SetCannotBeMultiPortRam(actual_info ? actual_info: this_info) ;
        }

        // Find the formal
        formal = 0 ;
        if (actual->NamedFormal()) {
            // This is a (SV-style) named formal.
            formal = (formal_scope) ? formal_scope->FindLocal(actual->NamedFormal()) : 0 ;
        } else {
            // This is a ordered formal
            if (i<((formals) ? formals->Size() : 0)) {
                formal = (formals) ? (VeriIdDef*)formals->At(i) : 0 ;
            }
        }

        if (!formal) continue ; // the formal cannot be found

        (void) associated_formals.Insert(formal) ;

        // Process actuals only for function call, not for sequence/property call
        if (function_id->IsFunction() && (formal->IsInput() || formal->IsInout()))  actual->VarUsage(info, USING) ;
        if (function_id->IsFunction() && (formal->IsOutput() || formal->IsInout())) actual->VarUsage(info, BLOCKING_ASSIGNING) ;

        // VIPER #2046 :Check the type of actual against formal
        if (check_association) (void) actual->CheckAssociation(formal, ((function_id->IsProperty() || function_id->IsSequence()) ? PROP_SEQ : FUNC)) ;
        if (!info || !info->GetInTemporalExpr()) continue ; // Continue, if we are not part of temporal expression
        formal->SetActual(actual) ;
        if (function_id->IsFunction()) {
            // This is a function call inside temporal expression.
            // VIPER #3962 : Produce error if this function contains output, inout
            // or ref ports.
            switch(formal->Dir()) {
            case VERI_OUTPUT :
            case VERI_INOUT :
            case VERI_REF :
                Error("illegal function call with %s argument in assertion expression", PrintToken(formal->Dir())) ;
                break ;
            default :
                break ;
            }
        } else if (function_id->IsSequence() && (action == SEQ_ENDED)) {
            // This is sequence instance on which method ended is applied.
            // Local variable can only be passed here as a whole. Check that
            actual = actual->GetConnection() ;
            VeriIdDef *arg_id = (actual) ? actual->FullId(): 0 ;
            // Check whether argument is local variable :
            unsigned local_var_arg = (arg_id && arg_id->IsLocalVar()) ? 1 : 0 ;
            if (!local_var_arg) {
                // Actual is not local variable, check actual whether local variable
                // is used as part of actual. If so produce error (VIPER #3946)
                if (actual) actual->VarUsage(info, INSTANCE_ARG) ;
            } else if (info->GetInOperand()) {
                // Viper 7776: Error should only be given for non full association of local variable.
                // This was done for Viper 3946: No error should be given in this else part. Not
                // sure why we implemented this code at that time.
                // Local variable is passed as actual to sequence instance on which
                // ended is applied. According to LRM the application of ended cannot
                // have negation or any other expression operator applied to it.
                // Error("the application of ended or triggered to a sequence %s to which local variable is being passed should be maximal boolean expression", function_id->Name()) ;
            }
        }
    }

    // Viper 7148: argument mismatch with formal not caught for external functions
    // randomize function can take any number of arguments
    if (function_id->GetFunctionType() != VERI_METHOD_RANDOMIZE) {
        FOREACH_ARRAY_ITEM(formals, i, formal) {
            if (!formal) continue ;
            if (!associated_formals.GetItem(formal) && !formal->GetInitialValue()) {
                formal->Error("no associated actual for formal %s", formal->GetName()) ;
            }
        }
    }

    if (function_id->IsSequence() || function_id->IsProperty() || function_id->IsLetId()) { // VIPER #6408: Include let-decls also
        // This is a sequence/property instance, process its body
        VeriModuleItem *body = function_id->GetModuleItem() ;
        if (body) (void) body->VarUsage(info, action) ;
        FOREACH_ARRAY_ITEM(formals, i, formal) {
            if (formal) formal->SetActual(0) ; // Reset actual setting :
        }
    }
    // Now mark IsConcurrentUsed in the class prefix
    // Pass this through to the prefix .
    // For function calls we mark the class object as
    // concurrent used
    VeriName *prefix = _func_name->GetPrefix() ;
    VeriIdDef *prefix_id = prefix ? prefix->GetId() : 0 ;
    VeriTypeInfo *prefix_type = prefix_id ? prefix_id->CreateType(0): 0 ;
    //VeriDataType *d_type = prefix_id ? prefix_id->GetDataType() : 0 ;
    //VeriIdDef *type_id = d_type ? d_type->GetId() : 0 ;
    //if (prefix && type_id && type_id->IsClass()) prefix->VarUsage(info, PARTIAL_USING) ;
    if (prefix && prefix_type && prefix_type->IsClassType()) prefix->VarUsage(info, PARTIAL_USING) ;
    delete prefix_type ;
}

/*--------------------------------------------------------------------------*/

void
VeriSystemFunctionCall::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DISABLE_RESET) {
        // If sampled value functions are used in reset expression of disable iff,
        // the sampling clock must be explicitly specified in actual argument list
        switch (_function_type) {
        // SVA system functions :
        case VERI_SYS_CALL_ROSE :
        case VERI_SYS_CALL_FELL :
        case VERI_SYS_CALL_STABLE :
        case VERI_SYS_CALL_SAMPLED :
            // Expect one argument (expression).
            // Second argument (clock expr) optional.
            if (!_args || (_args->Size()!=2)) {
                if (_name) Error("explicit clocking event must be specified for %s in disable iff condition", _name) ;
            }
            break ;
        case VERI_SYS_CALL_PAST :
            if (!_args || (_args->Size()!=4)) {
                if (_name) Error("explicit clocking event must be specified for %s in disable iff condition", _name) ;
            }
            break ;
        default :
            break ;
        }
    }
    // Viper 4586. Error out for less than 1 value of ticks in $past
    VeriExpression *ticks = (_args && _args->Size()>1) ? (VeriExpression*)_args->At(1) : 0 ;
    if (ticks && (action == USING || action == PROCEDURAL_CONTINOUS_ASSIGN) && _function_type == VERI_SYS_CALL_PAST) {
        int val = 1 ;
        if (IsStaticElab()) {
            VeriBaseValue *value = ticks->StaticEvaluateInternal(0, 0, 0, 0) ;
            VeriConst *const_val = value ? value->ToConstVal(ticks) : 0 ;
            delete value ;
            if (const_val && !const_val->HasXZ()) {
                val = const_val->Integer() ;
            }
            delete const_val ;
        }
        if (val < 1) ticks->Error("number of ticks for $past must be 1 or greater") ;
    }

    // VIPER #7834/7082 : Set global that we are processing sampled value functions
    // to produce error if local variables, method triggered or method matched
    // is used there.
    unsigned is_sampled_value_fn = 0 ;
    switch (_function_type) {
    // SVA system functions :
    case VERI_SYS_CALL_ROSE :
    case VERI_SYS_CALL_FELL :
    case VERI_SYS_CALL_STABLE :
    case VERI_SYS_CALL_SAMPLED :
    case VERI_SYS_CALL_PAST :
    case VERI_SYS_CALL_CHANGED :
        is_sampled_value_fn = 1 ;
    default :
        break ;
    }
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) {
        if ((i==0) && is_sampled_value_fn) VeriVarUsageInfo::PushInSampledValueFunc() ;
        if (arg) arg->VarUsage(info, USING) ; // VIPER 2490 : Mark each argument as used
        if ((i==0) && is_sampled_value_fn) VeriVarUsageInfo::PopInSampledValueFunc() ;
    }
}

/*--------------------------------------------------------------------------*/

void
VeriQuestionColon::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // process the condition in the enclosing CurrStatus scope
    if (_if_expr) _if_expr->VarUsage(info, USING) ;

    // save this initial assignment info
    VeriVarUsageInfo *local_info = 0 ;
    Map *id_table = 0 ;
    if (info) {
        local_info = info->Copy() ;
    } else {
        id_table = new Map(POINTER_HASH) ;
        local_info = new VeriVarUsageInfo(id_table) ;
    }

    // An array to store the assignment info for the THEN, and ELSE regions
    Array array_of_info(2);
    array_of_info.Insert(local_info) ;

    // VIPER #4446 : Make the info used to process then_expr as operand processing.
    // This will help us to error out if any construct (disable iff) which can
    // only appear as top level remains in _then_expr
    if (IsIfOperator()) local_info->SetInOperand() ;

    if (_then_expr) _then_expr->VarUsage(local_info, USING) ;

    if (info) {
        local_info = info->Copy() ;
    } else {
        local_info = new VeriVarUsageInfo(id_table) ;
    }
    array_of_info.Insert(local_info) ;

    // VIPER #4446 : Make the info used to process _else_expr as operand processing.
    // This will help us to error out if any construct (disable iff) which can
    // only appear as top level remains in _else_expr
    if (IsIfOperator()) local_info->SetInOperand() ;

    if (_else_expr) _else_expr->VarUsage(local_info, USING) ;

    // Combine the VarUsageInfo's from the arms of the IF into
    // an overall VarUsageInfo in order to update the status of
    // concurrent variable use/assignment
    if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
        info->Combine(array_of_info) ;
    } else {
        // make a temporary info holder here just long enough that the concurrent
        // status of the variables can be updated
        VeriVarUsageInfo tmp_info(id_table) ;
        tmp_info.Combine(array_of_info) ;
        tmp_info.UpdateConcurrentVarStatus() ;
    }

    // cleanup local CurrStatusArray
    VeriVarUsageInfo *v ;
    unsigned i;
    FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
        delete v ;
    }

    delete id_table;
}

/*--------------------------------------------------------------------------*/

void
VeriIdRef::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Post-analysis, the identifier HAS TO BE there.
    if (!_id) {
        // Try to find this identifier as a module in the 'compilation' scope (the current working 'library') :
        VeriModule *module = veri_file::GetModule(_name, 1/*case_sensitive*/, veri_file::GetWorkLib()->GetName()) ;
        if (module) {
            // Set the back-pointer.
            _id = module->GetId() ;
            // Flag that this module depends on the used module.
            // FIX ME : I have no scope... if (scope) scope->Using(module->GetScope()) ;
        } else if (action==SUBPROG_NAME) {
            // cannot error out. Subprogram names can be found in 'upward name scope' (up an instance path),
            // which is only known at elaboration time.
            // VIPER #6485: Try to resolve the function/task name using the _present_scope
            if (_present_scope) _id = _present_scope->Find(_name, this /*from*/) ;
            if (_id && !_id->IsFunction() && !_id->IsTask()) _id = 0 ; // Reset
        } else if (info && (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            ; // Do nothing Viper #3908. This may be in false branch
        // Do not produce error if there is a module anywhere with the same name.
        // The module may be bound using configuration. See P1800/Positive/21/test31.
        } else if (!veri_file::GetWorkLib()->GetModuleAnywhere(_name, 1 /* case sensitive */)) {
            // VIPER #7941: Do not produce this error for dual language if an unit with this name exits:
            VhdlLibrary *work = vhdl_file::GetLibrary(veri_file::GetWorkLib()->GetName()) ;
            VhdlPrimaryUnit *unit = (work) ? work->GetPrimUnit(_name) : 0 ; // Find in current (Verilog side) work library
            if (!unit) unit = vhdl_file::GetUnitFromLOptions(_name) ; // Find in -L option too
            if (!unit) // Only produce the following error if an unit with that name is not found
            // Here, since all is resolved, we can finally error out :
            Error("%s is not declared",_name) ;
        }
    }

    SynthesisInfo *this_info = 0 ;

    // discard as multi-port RAM, if it is directly accessed.
    // Viper #7854: Even if the id is not yet marked as _can_be_ram (dp or mp),
    // it is important to discard them when improperly used. For external hier
    // names, they may later be marked as good for ram (from data-decl::var-usage).
    // The stikey flag on the id will ensure that an id discarded for ram can never
    // be marked as ram ever.
    if (_id && !(info && info->GetInInitialClause()) &&
        (action!=PARTIAL_USING && action!=PARTIAL_BLOCKING_ASSIGNING && action!=PARTIAL_NONBLOCKING_ASSIGNING && action!=EVENT_USAGE && action != SYSTASKCALL_ARG)) {
        // Non-partial read or write of an identifier outside an 'initial' statement, and outside event expression.
        // This cannot be a RAM.
        _id->SetCannotBeDualPortRam(this_info) ;
        _id->SetCannotBeMultiPortRam(this_info) ;
    }

    // Viper #4265 : Skip RAM inference for variables declared inside a function
    if (_id && info && info->GetInFunction() && (action != SYSTASKCALL_ARG)) {
        _id->SetCannotBeDualPortRam(this_info) ;
        _id->SetCannotBeMultiPortRam(this_info) ;
    }

    if (_id && _id->IsSequenceFormal()) {
        // This is reference to formal identifier of sequence/property, process
        // the actual expression
        if (_id->IsProcessing()) return ; // recursive call
        _id->SetIsProcessing() ;
        VeriExpression *actual = _id->GetActual() ;
        if (actual) actual->VarUsage(info, action) ;
        _id->ClearIsProcessing() ;
        return ;
    }
    if (_id && ((_id->IsSequence() || _id->IsProperty() || _id->IsLetId())
        // VIPER #8011: Here we are referencing function call without argument(Function call without arg (no port, not within function itself)).
        // Check that the function call without argument is called within a function: if not we will call varusage on its body, otherwise not.
           || (_id->IsFunction() && _id->CanCallWithoutArg() && _present_scope && _id != _present_scope->GetSubprogram()))) {
        // This is sequence/property instance, process its body
        VeriModuleItem *body = _id->GetModuleItem() ;
        if (body) (void) body->VarUsage(info, action) ;
        return ;
    }
    // VIPER #7834/7082: IEEE 1800-2009 section 16.9.3 : It is illegal to use local variable
    // in the argument of sampled value function
    if (_id && (_id->IsLocalVar() || _id->IsAutomatic()) && VeriVarUsageInfo::IsInSampledValueFunction()) {
        Error("use of local variable %s in sampled value function is illegal", _id->Name()) ;
    }
    // VIPER #5637: Do not check the validity of temporal expression for
    // subprogram name when _id is not function/sequence/property
    if (_id && info && info->GetInTemporalExpr() &&
        (((action == SUBPROG_NAME) && (_id->IsFunction() || _id->IsSequence() || _id->IsProperty())) ||
             (action != SUBPROG_NAME))) {
        // VIPER #3939, 3940, 3941, 3942, 3943, 3957
        // Check validity of the data type of variables used in temporal expression
        VeriTypeInfo *id_type = _id->CreateType(0) ;
        if (id_type && !id_type->IsTypeParameterType() && !id_type->IsUnresolvedNameType()) id_type->CheckTemporalExprType(_id, this, info->GetInAlwaysInitial()) ;
        delete id_type ;
    }
    if (_id && _id->IsLocalVar() && (action == INSTANCE_ARG)) {
        // VIPER #3946 : Local variable is used as actual argument, but not as a entire actual
        Error("local variable %s can be passed only as entire actual argument to sequence on which ended or triggered is applied", _id->Name()) ;
    }

    if (!IsAms() && _id && _id->IsGenVar() && !VeriVarUsageInfo::IsWithinLoop() && !(info && (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()))) { // VIPER #8278: Check for generate conditional along with in always/initial flag
        // VIPER #8109 : Use of genvar, but it is not within for-generate loop
        Error("invalid context for genvar %s", _id->Name()) ;
    }
    if (_id && _id->IsGenVar() && (action == FORGEN_INIT) && info && !info->HasIdInStack(_id)) {
        // VIPER #8109 : Genvar which is not previously used as genvar index
        // is used to initialize genvar in for-generate. Produce error
        Error("invalid rhs in generate loop initialization assignment") ;
    }

    // reference to _id :
    if (_id) _id->VarUsage(info, action, /*from*/ this) ;
}

/*--------------------------------------------------------------------------*/

void
VeriIndexedId::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Make sure that any assignment to a indexed name is marked as a partial assignment.
    unsigned prefix_action = action ;
    // Adjust the action for the prefix name because there is partial access of the prefix :
    switch (action) {
    case BLOCKING_ASSIGNING : prefix_action = PARTIAL_BLOCKING_ASSIGNING ; break ;
    case NONBLOCKING_ASSIGNING : prefix_action = PARTIAL_NONBLOCKING_ASSIGNING ; break ;
    case PROCEDURAL_CONTINOUS_ASSIGN :
    case USING : prefix_action = PARTIAL_USING ; break ;
    default : break ; // prefix action same as this full name action
    }

    SynthesisInfo *prefix_info = 0 ;
    VeriName *my_prefix = _prefix ;
    VeriExpression *delete_prefix = 0 ;
    VeriIdDef *my_id = _id ;
    SynthesisInfo *this_info = 0 ;

    // Pass that through to the prefix :
    if (my_prefix) my_prefix->VarUsage(info, prefix_action) ;

    // Post-analysis, the identifier HAS TO BE there :
    if (!_id && _prefix) {
        // Find and set the prefix identifier :
        _id = _prefix->GetId() ;
        if (!this_info) my_id = _id ;
        // If it is still not there, an error will have occurred in prefix->VarUsage().
    }

    if (my_id && my_id->IsType() && (prefix_action != TYPE_USAGE)) Warning("type identifier %s used in illegal context", my_id->GetName()) ; // VIPER #5806

    // VIPER #3736 : Move this check from 'Resolve' to here. Discard id as a i
    // dual-port memory if this indexed name is used inside a loop.
    // Viper #7854: Even if the id is not yet marked as _can_be_ram (dp or mp),
    // it is important to discard them when improperly used. For external hier
    // names, they may later be marked as good for ram (from data-decl::var-usage).
    // The stikey flag on the id will ensure that an id discarded for ram can never
    // be marked as ram ever.
    if (my_id && _idx && VeriVarUsageInfo::IsWithinLoop() && prefix_action != SYSTASKCALL_ARG) {
        my_id->SetCannotBeDualPortRam(prefix_info) ;
    }

    // Discard id as RAM if first index is constant or a range
    // (VIPER 3618) and we are NOT in an initial clause :
    // Viper #7854: Even if the id is not yet marked as _can_be_ram (dp or mp),
    // it is important to discard them when improperly used. For external hier
    // names, they may later be marked as good for ram (from data-decl::var-usage).
    // The stikey flag on the id will ensure that an id discarded for ram can never
    // be marked as ram ever.
    if (my_id && _idx && !(info && info->GetInInitialClause()) && prefix_action!=SYSTASKCALL_ARG) {
        // Discard id as a memory if we part-select it with the first index..
        // VIPER 3549 : A directly sliced identfier cannot be modeled as a multi-port RAM.
        // That is for two reasons :
        //   (1) direct slicing of a multi-port RAM identifier is not yet supported in multi-port RAM elaboration (reader side)
        //   (2) if the slice constitutes a full range of the identifier (we don't know that here), then read/write ports would
        //       be created with 0 address bits and full-range data bits. That is certainly not intended to be a RAM of any sort.
        if (_idx->IsRange()) {
            my_id->SetCannotBeDualPortRam(prefix_info) ;
            my_id->SetCannotBeMultiPortRam(prefix_info) ;
        }

        // Check if the expression if first index is a 'constant' expression.
        // Also discard if the id in the index is a constant :
        // FIX ME : should have an API for that.
        VeriIdDef *idx_id = _idx->GetId() ;
        if (_idx->IsConst() || // constant literal index
            // VIPER #8278: Check for generate conditional along with in always/initial flag
            (action==NONBLOCKING_ASSIGNING && (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()))) || // Verilog requires a constant index expression in continuous assignments
            (idx_id && (idx_id->IsParam() || idx_id->IsGenVar()))
            ) {
            // index with a constant value :
            my_id->SetCannotBeDualPortRam(prefix_info) ;
            my_id->SetCannotBeMultiPortRam(prefix_info) ;
        }

        // VIPER 3603 : Assignments in a non-clocked area (number of control edges != 1) discard a RAM :
        if ((info  && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial()) && info->GetNumEdges()!=1) &&
            (prefix_action == PARTIAL_NONBLOCKING_ASSIGNING || prefix_action == PARTIAL_BLOCKING_ASSIGNING)) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            // my_id->SetCannotBeDualPortRam(prefix_info) ;
            my_id->SetCannotBeMultiPortRam(prefix_info) ; // discard multiport RAM if asynchronously assigned.
        }
    }

    if (_idx) {
        if (action==NONBLOCKING_ASSIGNING && (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()))) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            _idx->VarUsage(info, CONSTANT) ; // VIPER #2943 : index must be constant
        } else {
            _idx->VarUsage(info, USING) ; // index expression is always used.
        }
    }

    delete delete_prefix ;
}

/*--------------------------------------------------------------------------*/

void
VeriIndexedMemoryId::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Make sure that any assignment to a indexed name is marked as a partial assignment.
    unsigned prefix_action = action ;
    // Adjust the action for the prefix name because there is partial access of the prefix :
    switch (action) {
    case BLOCKING_ASSIGNING : prefix_action = PARTIAL_BLOCKING_ASSIGNING ; break ;
    case NONBLOCKING_ASSIGNING : prefix_action = PARTIAL_NONBLOCKING_ASSIGNING ; break ;
    case PROCEDURAL_CONTINOUS_ASSIGN :
    case USING : prefix_action = PARTIAL_USING ; break ;
    default : break ; // prefix action same as this full name action
    }

    SynthesisInfo *prefix_info = 0 ;
    VeriName *my_prefix = _prefix ;
    VeriExpression *delete_prefix = 0 ;
    VeriIdDef *my_id = _id ;
    SynthesisInfo *this_info = 0 ;

    // Pass that through to the prefix :
    if (my_prefix) my_prefix->VarUsage(info, prefix_action) ;

    // Post-analysis, the identifier HAS TO BE there :
    if (!_id && _prefix) {
        // Find and set the prefix identifier :
        _id = _prefix->GetId() ;
        if (!this_info) my_id = _id ;
        // If it is still not there, an error will have occurred in prefix->VarUsage().
    }

    // Discard RAMs (if not in 'initial' area) in some cases :
    // Viper #7854: Even if the id is not yet marked as _can_be_ram (dp or mp),
    // it is important to discard them when improperly used. For external hier
    // names, they may later be marked as good for ram (from data-decl::var-usage).
    // The stikey flag on the id will ensure that an id discarded for ram can never
    // be marked as ram ever.
    if (my_id && !(info && info->GetInInitialClause())) {
        // Discard id with multiple indexes as a dual-port memory (Once elaboration supports RAM access with multiple indexes, we can remove this)
        // Only VeriIndexedMemoryId::Elaborate/Assign would need to be adjusted. Rest of the code will work.
        // See issue 2220.
        if (prefix_action != SYSTASKCALL_ARG) my_id->SetCannotBeDualPortRam(prefix_info) ;

        // VIPER 3593 :
        // In an BLOCKING assignment, if ALL indexes of the identifier are constant, then a RAM is not smart : Constant propagation would break.
        // FIX ME : Maybe we should do the same test for normal VeriIndexedId (discard only for blocking assignments with constant index)
        if (prefix_action==PARTIAL_BLOCKING_ASSIGNING ||
            prefix_action==PARTIAL_NONBLOCKING_ASSIGNING) { // Viper #5254
            unsigned i ;
            VeriExpression *idx ;
            unsigned constant_index = 1 ;
            FOREACH_ARRAY_ITEM(_indexes, i, idx) {
                if (!idx) continue ;
                if (idx->IsRange()) continue ; // part-select does not matter..

                // Check if index is constant
                if (idx->IsConst()) continue ;

                // Also discard if the id in the index is a constant :
                // FIX ME : should have an API for that.
                VeriIdDef *idx_id = idx->GetId() ;
                if ((idx_id && (idx_id->IsParam() || idx_id->IsGenVar()))) {
                    // constant index
                    continue ;
                }
                // Here, assume non-constant index
                constant_index = 0 ;
            }

            if (constant_index) {
                my_id->SetCannotBeDualPortRam(prefix_info) ;
                my_id->SetCannotBeMultiPortRam(prefix_info) ;
            }
        }

        // VIPER 3603 : Assignments in a non-clocked area (number of control edges != 1) discard a RAM :
        if ((info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial()) && info->GetNumEdges()!=1) &&
            (prefix_action == PARTIAL_NONBLOCKING_ASSIGNING || prefix_action == PARTIAL_BLOCKING_ASSIGNING)) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            // my_id->SetCannotBeDualPortRam(prefix_info) ;
            my_id->SetCannotBeMultiPortRam(prefix_info) ; // discard multiport RAM if asynchronously assigned.
        }
    }

    // Run over all indexes :
    unsigned i ;
    VeriExpression *idx ;
    FOREACH_ARRAY_ITEM(_indexes, i, idx) {
        if (idx) {
            if (action==NONBLOCKING_ASSIGNING && (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()))) { // VIPER #8278: Check for generate conditional along with in always/initial flag
                idx->VarUsage(info, CONSTANT) ; // VIPER #2943 : index must be constant
            } else {
                idx->VarUsage(info, USING) ; // index expression is always used.
            }
        }
    }

    delete delete_prefix ;
}

/*--------------------------------------------------------------------------*/
void
VeriRange::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    if (_left) _left->VarUsage(info, action) ;
    if (_right) _right->VarUsage(info, (action == CYCLE_DELAY) ? CYCLE_DELAY_RBOUND : action) ;
}

/*--------------------------------------------------------------------------*/

void
VeriIndexedExpr::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Make sure that any assignment to a indexed name is marked as a partial assignment.
    unsigned prefix_action = action ;
    // Adjust the action for the prefix name because there is partial access of the prefix:
    switch (action) {
    case BLOCKING_ASSIGNING : prefix_action = PARTIAL_BLOCKING_ASSIGNING ; break ;
    case NONBLOCKING_ASSIGNING : prefix_action = PARTIAL_NONBLOCKING_ASSIGNING ; break ;
    case PROCEDURAL_CONTINOUS_ASSIGN :
    case USING : prefix_action = PARTIAL_USING ; break ;
    default : break ; // prefix action same as this full name action
    }

    // Pass that through to the prefix:
    if (_prefix) _prefix->VarUsage(info, prefix_action) ;

    if (_idx) {
        if (action==NONBLOCKING_ASSIGNING && (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()))) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            _idx->VarUsage(info, CONSTANT) ; // VIPER #2943 : index must be constant
        } else {
            _idx->VarUsage(info, USING) ; // index expression is always used.
        }
    }
}

/*--------------------------------------------------------------------------*/

// VIPER #5238: Check the prefix of a scope name whether it is package or class name
// or covergroup/coverpoint as only these are allowed in prefix.
void VeriScopeName::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    VeriSelectedName::VarUsage(info, action) ;

    // Can't do anything without a prefix, suffix null represents 'class_id #(...)',
    // and need not to check the scope name if suffix is null.
    if (!_prefix_id || !_suffix) return ;

    // Prefix of a scope name can only have package/class name or covergroup/coverpoint
    // If prefix_id is parameter then create its type and then get its id.
    VeriIdDef *prefix_id = _prefix_id ;
    if (prefix_id->IsParam()) { // If prefix_id is parameter then create its type and then get its id
        VeriTypeInfo *prefix_type = prefix_id->CreateType(0) ;
        prefix_id = (prefix_type) ? prefix_type->GetNameId() : 0 ;
        delete prefix_type ;
    }
    if (!prefix_id) return ;
    VeriModuleItem *module_item = prefix_id->GetModuleItem() ;
    if (!prefix_id->IsClass() && !prefix_id->IsPackage() && !prefix_id->IsCovergroup()
     && !(module_item && (module_item->GetType() == VERI_COVERPOINT)) && (_prefix->GetToken() != VERI_LOCAL)) { // VIPER #6391
        Error("%s is not a valid package or class type prefix for '::'", _prefix_id->Name()) ; //VIPER #5238
    }
}
/*--------------------------------------------------------------------------*/

void
VeriSelectedName::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;
    if (_cross_lang_name) return ; // No need to check usage for cross language hier names (VIPER #5624)

    VeriExpression *my_prefix = _prefix ;
    VeriExpression *delete_prefix = 0 ;
    VeriIdDef *my_prefix_id = (_prefix_id) ? _prefix_id : ((_prefix) ? _prefix->GetId() : 0) ;
    VeriIdDef *my_suffix_id = _suffix_id ;
    SynthesisInfo *this_info = 0 ;
    SynthesisInfo *suffix_info = 0 ;

    // Make sure that any assignment to a indexed name is marked as a partial assignment.
    unsigned prefix_action = action ;
    // Adjust the action for the prefix name because there is partial access of the prefix :
    switch (action) {
    case BLOCKING_ASSIGNING : prefix_action = PARTIAL_BLOCKING_ASSIGNING ; break ;
    case NONBLOCKING_ASSIGNING : prefix_action = PARTIAL_NONBLOCKING_ASSIGNING ; break ;
    case PROCEDURAL_CONTINOUS_ASSIGN :
    case USING : prefix_action = PARTIAL_USING ; break ;
    default : break ; // prefix action same as this full name action
    }
    // Viper 7001: Issue error for non illegal usage of local variable in argument expression
    if (info && info->GetInTemporalExpr() && (_function_type == VERI_METHOD_ENDED || _function_type == VERI_METHOD_TRIGGERED)) {
        // If this is sequence method ended, pass that information to process the
        // prefix which is sequence instance :
        prefix_action = SEQ_ENDED ;
    }

    // Pass this through to the prefix
    if (my_prefix) my_prefix->VarUsage(info, prefix_action) ;

    // Try to find out if the prefix-id has a scope (local or from its data type):
    VeriScope *prefix_scope = 0 ;
    if (/*(!this_info || to_be_ignored) &&*/ _prefix_id) {
        prefix_scope = _prefix_id->LocalScope() ;
        if (IsSystemVeri2009() && !prefix_scope && _prefix_id->IsLabel()) {
            VeriModuleItem *body = _prefix_id->GetModuleItem() ;
            // VIPER #4881: This may be a statement label, get the scope via its module item (statement):
            if (body) prefix_scope = body->GetScope() ;
        }
        if (!prefix_scope) {
            VeriDataType *type = _prefix_id->GetDataType() ;
            VeriIdDef *type_id = (type) ? type->GetId() : 0 ;
            if (type_id) prefix_scope = type_id->LocalScope() ;
        }
    }

    if (/*(!this_info || to_be_ignored) &&*/ (action==TYPE_USAGE) && !_suffix_id && _prefix && _prefix_id && !prefix_scope) {
        // VIPER #6278: Here, 'this' is a type but suffix is not resolve and prefix does not
        // have a scope, so, we have no chance to be able to resolve suffix. Try differently:
        // Clear the id of the prefix resolved after taking the currently resolved id.
        VeriIdDef *old_id = _prefix->GetId() ;
        _prefix->SetId(0) ;
        // Now, run VarUsage again, it may resolve the prefix-id differently:
        // Use SUBPROG_NAME as action for VeriIdRef so that we do not produce error:
        unsigned local_action = SUBPROG_NAME ;
        if (_prefix->IsHierName()) local_action = prefix_action ;
        _prefix->VarUsage(info, local_action) ;
        if (!_prefix->GetId()) {
            // Cannot resolve prefix differently, set the old-id back
            _prefix->SetId(old_id) ;
        } else {
            // Reset it so that we set it below to the updated prefix->Id
            _prefix_id = 0 ;
            if (!this_info) my_prefix_id = _prefix_id ; // Update
        }
    }

    // Post-analysis, the identifier HAS TO BE there :
    if (!_prefix_id && _prefix) {
        // Find and set the prefix identifier :
        _prefix_id = _prefix->GetId() ;
        if (!this_info) my_prefix_id = _prefix_id ; // Update
        // If it is still not there, an error will have occurred in prefix->VarUsage().
    }
    if (!_prefix_id && (!this_info || !my_prefix_id)) return ;

    // VIPER #5911 : If there is any hierarchical access to a class member from
    // another class, add the rand/randc variables of that class also in the map
    // kept in VarUsageInfo
    if (my_prefix_id && my_prefix_id->IsClass() && info && info->GetRandRandCVars()) {
        VeriModuleItem *class_body = my_prefix_id->GetModuleItem() ;
        if (class_body) class_body->CollectRandRandCVars(info->GetRandRandCVars()) ;
    }

    // Selected name can be converted to id-ref making suffix id and name null
    // in static elaboration if it refers object declared within generate construct.
    // If so no need to proceed further
    if (!_suffix_id && !_suffix && (!this_info || !my_suffix_id)) return ;

    // VIPER #2954: Do not resolve a suffix defined under an (packed/unpacked) array
    // for which the prefix is not an indexed name. This should not be allowed:
    // VIPER #2980: Match the number of packed/unpacked dimension in declaration and
    // dimension in the prefix. We will not resolve the suffix if they don't match:
    // VIPER #5014: All all prefix dimensions are not selected in this hierarchical name,
    // do not select the suffix id. Do this check also when _function_type is present
    unsigned can_select_ele = 1 ;
    if (my_prefix && my_prefix_id && (my_prefix_id->IsVar() || my_prefix_id->IsInst())) { // && !_function_type) {
        // First get the number of dimensions in the prefix of this hier identifier:
        unsigned select_dims = 0 ;
        if (my_prefix->GetIndexExpr()) {
            select_dims = 1 ; // VeriIndexedId with only one dimension
        } else {
            Array *indices = my_prefix->GetIndexes() ;
            if (indices) select_dims = indices->Size() ; // VeriIndexedMemoryId with multiple dimensions
        }

        // Now get the number of dimensions that must be indexed to refer elements declared
        // inside the prefix. This is unpacked dimensions and packed/unpacked dimenisons for
        // structure and union type variables:
        // Make it same as 'select_dims' so that we do not error out in case we could not create id-type below:
        unsigned declare_dims = select_dims ;

        // VIPER #4096 : Use created type instead of data type back pointer to
        // check if selected name is legal or not.
        VeriTypeInfo *data_type = my_prefix_id->CreateType(0) ;
        if (data_type) {
            declare_dims = data_type->UnpackedDimension() ;
            VeriTypeInfo *datatype = data_type->BaseType() ;
            if (datatype && (datatype->IsStructure() || datatype->IsUnion())) {
                // Add the packed dimensions only for the structure/unions:
                declare_dims = declare_dims + data_type->PackedDimension() ;
            } else if (data_type->IsTypeParameterType() || data_type->IsUnresolvedNameType()) {
                // This data type is a type parameter, set 'declare_dims' such that we don't error out.
                // We cannot fully check for dimension in case of type parameters, so better be silent:
                declare_dims = select_dims ;
            }
        }
        delete data_type ;

        // Check that the two types of dimensions matches for this hier-id:
        if (select_dims != declare_dims) {
            if (!_function_type) {
                Warning("cannot select %s inside %s due to dimension mismatch", _suffix, my_prefix->GetName()) ;
                return ; // Don't resolve the suffix here
            } else { // Dimension mismatch occurs, do not select suffix id
                can_select_ele = 0 ;
            }
        }
    }
    if (/*(!this_info || to_be_ignored) &&*/ _prefix_id && !_suffix_id && can_select_ele) {
        // Find the suffix id (now that we found the prefix id) :
        // This section is same as VeriSelectedName::Resolve(), with some modification
        // since now (post-analysis) all modules should be in place.

        // Check if we are about to point into a generate-for loop.
        VeriModuleItem *prefix_module_item = _prefix_id->GetModuleItem() ;
        if ((!this_info || !my_suffix_id) && prefix_module_item && (prefix_module_item->IsGenerateFor() || prefix_module_item->IsGenerateBlock())) {
            // Do NOT try to resolve an identifier declared inside a generate-for loop or generate block.
            // It would not identify a unique identifier, since inside generate-for loops we
            // will 'generate' new identifiers.
            // This is similar to trying to resolve an identifier via a instance, or into a separate module.
            delete delete_prefix ;
            return ;
        }

        // First, check if this is a selected name of Verilog 95/2001 style.
        // In that case, we need to look in the prefix scope :
        // VIPER #4881: Use the prefix_scope found above, if not set then use it:
        if (!prefix_scope) prefix_scope = _prefix_id->LocalScope() ;

        // Find the suffix local in that scope :
        if (prefix_scope) {
            _suffix_id = prefix_scope->FindLocal(_suffix) ;
            if (!this_info) my_suffix_id = _suffix_id ; // Update
        }
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
            if ((!this_info || !my_suffix_id) && _suffix_id && _suffix_id->IsUnnamedBlock()) {
                _suffix_id = 0 ;
                delete delete_prefix ;
                return ; // Don't resolve the suffix with unnamed block name
            }
        }
//#endif

        // If that did not work, check if we are dealing with an instantiation :
        if (!_suffix_id && _prefix_id->IsInst()) {
            // Look through the instantiation !
            // Needed for direct selected name into an instance (of a interface or class or so)
            VeriModuleInstantiation *instance = _prefix_id->GetModuleInstance() ;
            VeriName *unit_name = (instance) ? instance->GetModuleNameRef() : 0 ;
            VeriIdDef *unit_id = (unit_name) ? unit_name->GetId() : 0 ;

            // We should not actually do anything here, since instance-path specific identifiers are not unique.
            // But for now, we have to do this for Interface and for Class instances,
            // to make later (elaboration) work. FIX ME later.
            if (unit_id && !unit_id->IsInterface() && !unit_id->IsClass()) {
                // Do NOT resolve a hierarchical name into an instance path,
                // since RTL elaboration would not recognize it as an external reference.
                unit_id = 0 ;
            }

            // Get the scope of this unit :
            prefix_scope = (unit_id) ? unit_id->LocalScope() : 0 ;
            // Find the suffix local in that scope :
            if (prefix_scope) {
                _suffix_id = prefix_scope->FindLocal(_suffix) ;
                if (!this_info) my_suffix_id = _suffix_id ; // Update
            }
        }

        // If the prefix  (such as a function id) has a scope, then we still might need to look in the datatype.
        // see example 10.3/strReturn1 of an unnamed struct data type on a function.
        // So just check if we resolved the suffix id. If not, then we will check the data type.
        if ((!this_info || !my_suffix_id) && !_suffix_id) {
            // If the identifier itself is a variable,
            // we could find the suffix in the data type of the identifier.
            // Works for variables where the data type is a class or struct/union.
            // Also works for ports where the data type denotes a interface, or modport.
            VeriDataType *data_type = _prefix_id->GetDataType() ;

            // Check for member in the data type :
            // This includes any reference to class/type/ or interface.
            if (data_type) _suffix_id = data_type->GetMember(_suffix) ;

            // VIPER #6504: Try to resolve suffix by creating VeriTypeInfo and
            // discard processing for type parameter or unresolved type
            VeriTypeInfo *type = _prefix ? _prefix->CheckType(0, NO_ENV): 0 ;
            if (type && !_suffix_id) _suffix_id = type->GetMember(_suffix) ;
            if (type && type->IsTypeParameterType()) {
                _suffix_id = 0 ;
                delete type ;
                delete delete_prefix ;
                return ;
            }
            if (!_suffix_id && type && type->IsUnresolvedNameType()) {
                delete type ;
                delete delete_prefix ;
                return ;
            }
            delete type ;

            my_suffix_id = _suffix_id ; // Update
        }

#if 0 // Cannot check environment, because we do not have it (not as accurate as in Resolve(), that is).
        // Check if the suffix is a struct/union member.
        // If so, apply environment checks to the prefix (if it is not a struct member itself)
        // This way, only real variables get their environment checked.
        if (_suffix_id && _suffix_id->IsMember() && !_prefix_id->IsMember()) {
            _prefix_id->CheckEnvironment(environment, /*from*/this) ;
        }
#endif

        // VIPER #3073 : Reset function type if suffix id has been resolved.
        if (my_suffix_id) _function_type = 0 ;

        if ((!this_info || !my_suffix_id) && !_suffix_id && !_function_type) {
            // Cannot find a suffix identifier.
            // If prefix is an instance, then we cannot error out yet.
            // Some instances are not yet resolved (e.g. if instance is not yet full-analyzed) and
            // also we do not resolve instance paths at analysis time, since they are not 'unique' identifier references.
            if (_prefix_id->IsInst()) { delete delete_prefix ; return ; }
            // SV : Also be silent if prefix_id is a 'interface' port, since interfaces can be used before defined.
            // Interface ports should be resolved by now. Let go through...
            if (_prefix_id->IsInterfacePort()) { delete delete_prefix ; return ; }
            // SV : Also be silent for typeparameter::class_item
            if (_prefix_id->IsParam()) { delete delete_prefix ; return ; }
            VeriDataType *prefix_data_type = _prefix_id->GetDataType() ;
            VeriIdDef *prefix_data_type_id = prefix_data_type ? prefix_data_type->GetId() : 0 ;
            if (prefix_data_type_id && prefix_data_type_id->IsParam()) { delete delete_prefix ; return ; }

            // Otherwise, error out :
            // VIPER #8002: Do not produce this error if we are inside conditional generate branch.
            // If the branch is active, we will anyway produce the error from RTL elaborator.
            // For static elaboration, there should not be generate at all. Check VIPER #7102 also.
            if (!info || !info->GetInConditionalGenerate()) Error("%s is not declared under prefix %s", _suffix, _prefix_id->Name()) ;
        }
    }
    // If there is no suffix id, even after this full resolving, and there is a _function_type,
    // then this is a 'method', called without arguments.
    // If we are looking for a subprogram name, then do NOT yet resolve this as a 'method'.
    // Note that we should always first try to find the suffix_id in normal ways, since
    // methods can be overridden by user-defined identifiers. (no clear LRM statement about this, but makes sense)
    if (!my_suffix_id && _function_type && action!=SUBPROG_NAME) {
        // resolve this as a method function call without arguments
        VarUsageMethod(info,action,0) ; // resolve this (method) selected name as a function call with (VeriExpression) arguments.
    }

    // VIPER #3191 : Signals declared within clocking block can only be driven with
    // a non-blocking assignment. Moreover both nettype and reg type are allowed as lvalue
    if (my_suffix_id && my_prefix_id && my_prefix_id->IsClocking() && ((action == BLOCKING_ASSIGNING) || (action == NONBLOCKING_ASSIGNING))) {
        // 'my_suffix_id' is declared within clocking block, issue error if it is not
        // non-blocking assignment
        // VIPER #8278: Check for generate conditional along with in always/initial flag
        if ((action == BLOCKING_ASSIGNING) || ((action == NONBLOCKING_ASSIGNING) && (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()))/*concurrent assign*/)) {
            Error("clocking block signal %s can be driven only with a non-blocking assignment", my_suffix_id->Name()) ;
        }
        // Change action to process my_suffix_id to avoid error issuing for net assignment
        // in procedural area
        action = 0; // FIXME : Set action 0. Should be CLOCKING_DRIVE ?
    }
    // VIPER #3959 : Hierarchical access to local variable is illegal :
    if (my_suffix_id && my_suffix_id->IsLocalVar()) {
        Error("hierarchical access to local variable %s is illegal", my_suffix_id->Name()) ;
    }

    // Now run the suffix through the analyzer (use 'action', since this id is referenced in full) :
    if (my_suffix_id) {
        // Issue 3102 : The suffix of a selected name can be a member, a var inside an interface (via instance or interface port), or a var in a module.
        // It is risky to actually run a full test on this thing (at least during assignment) because it will trigger
        // a multiple-assign error incorrectly (if a second selected name has a different prefix, but same suffix id).
        // So skip assignment altogether for suffixes. Hope that does not cause more trouble down the line...
        switch (action) {
        case BLOCKING_ASSIGNING :
        case NONBLOCKING_ASSIGNING :
            // VIPER #5288 (baseline part): Discard as RAM, if it is fully/directly accessed.
            my_suffix_id->SetCannotBeDualPortRam(suffix_info) ;
            my_suffix_id->SetCannotBeMultiPortRam(suffix_info) ;
        case PARTIAL_BLOCKING_ASSIGNING :
        case PARTIAL_NONBLOCKING_ASSIGNING :
            // two non-mutually exclusive assignments in sequential area -- cannot be a dual-port RAM
            if (my_suffix_id->IsConcurrentAssigned(suffix_info)) my_suffix_id->SetCannotBeDualPortRam(suffix_info) ;

            if (my_prefix_id) {
                VeriDataType *prefix_type = my_prefix_id->GetDataType() ;
                VeriIdDef *prefix_type_id = prefix_type ? prefix_type->GetId() : 0 ;
                // Viper 6870 : Call varusage for system verilog package also.
                unsigned is_class = ((prefix_type_id && prefix_type_id->IsClass()) || my_prefix_id->IsClass() || my_prefix_id->IsPackage()) ;
                if ((is_class && !my_suffix_id->IsVirtualInterface()) || my_suffix_id->IsMember()) {
                    my_suffix_id->VarUsage(info, action, /*from*/this) ;
                }
            }
            break ;
        default :
            // other environment for an instance field. Pass through test :
            my_suffix_id->VarUsage(info, action, /*from*/this) ;
            break ;
        }
    }

    // Viper 6984 : Issue Warning for acces of interface members using interface name.
    // VIPER #8351 : Produced warning if any object defined within interface is accessed.
    // FIXME : Allowing modport, task, function ?
    if (my_suffix_id && my_prefix_id && my_prefix_id->IsInterface() && !my_suffix_id->IsModport() && !my_suffix_id->IsInst() && !my_suffix_id->IsModule() &&
                  /*my_suffix_id->IsVar() &&*/ !my_suffix_id->IsFunction() && !my_suffix_id->IsTask()) {
        Warning("illegal reference of %s inside interface %s", my_suffix_id->GetName(), my_prefix_id->GetName()) ;
    }

    delete delete_prefix ;
}

void VeriIdRef::VarUsageMethod(VeriVarUsageInfo *info, unsigned action, Array *arguments)
{
    unsigned function_type = GetFuncTokenOfClass(_name) ;
    if (!function_type) return ;

    VarUsageMethodInternal(info, action, 0, function_type, arguments) ;
}
void VeriSelectedName::VarUsageMethod(VeriVarUsageInfo *info, unsigned action, Array *arguments)
{
    // Resolve this selected name (which must be a built-in method) as a method-function call with this argument list
    // Cant do anything without a prefix or suffix :
    if (!_prefix_id || _suffix_id || !_function_type) return ; // if there is no prefix id, or already a resolved suffix id, then it is not a method.

    VarUsageMethodInternal(info, action, _prefix, _function_type, arguments) ;
}
void VeriName::VarUsageMethodInternal(VeriVarUsageInfo *info, unsigned action, VeriName *prefix, unsigned function_type, Array *arguments)
{
    if (!function_type) return ; // cannot do anything without function type token
    // Environment checks on methods.
    switch (action) {
    case BLOCKING_ASSIGNING:
    case PARTIAL_BLOCKING_ASSIGNING:
    case NONBLOCKING_ASSIGNING:
    case PARTIAL_NONBLOCKING_ASSIGNING:
        // Assignment to a method ? method in LHS is never allowed..
        Error("function call %s is not allowed here", PrintToken(function_type)) ; // print the name of the method.
        break ;
    default :
        break ;
    }

    // Resolve the arguments (all inputs), if any :
    unsigned i ;
    VeriExpression *item ;
    FOREACH_ARRAY_ITEM(arguments, i, item) {
        if (!item) continue ;
        // All inputs, resolve with 'expression' environment.
        // CHECK ME: Might need special environment per method.
        item->VarUsage(info,USING) ;
    }

    // VIPER #3784 && #3845: Use the type checking utility to check the prefix type:
    VeriTypeInfo *prefix_type = (prefix) ? prefix->CheckType(0 /* no expected type */, NO_ENV) : 0 ;
    VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;

    // Check arguments versus the applicable method :
    switch (function_type) {
    // 3.1 methods :
    // string conversion functions :
    case VERI_METHOD_ATOBIN : // str.atobin()
    case VERI_METHOD_ATOHEX : // str.atohex()
    case VERI_METHOD_ATOI :   // str.atoi()
    case VERI_METHOD_ATOOCT : // str.atooct()
    case VERI_METHOD_ATOREAL : // str.atoreal()
    case VERI_METHOD_TOLOWER : // str.tolower()
    case VERI_METHOD_TOUPPER : // str.tolower()
    case VERI_METHOD_LEN : // str.len()
        // Check number of arguments (0) :
        // If the function is called with '()', we create a single argument with value 0.
        // If the function is called without '()', we don't create any argument.
        // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Also check prefix is a string (the pre-defined type, so no dimensions will be given) :
        if (!prefix_type || (!prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && !prefix_type->IsString())) {
            // VIPER #3784 && #3845: Prefix is not a string type, error:
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        break ;
    // string tasks (these evaluate the argument and assign (override) the string prefix) !
    case VERI_METHOD_BINTOA : // task (assign to prefix) : str.bintoa(i)
    case VERI_METHOD_ITOA : // task (assign to prefix) : str.itoa(i)
    case VERI_METHOD_HEXTOA : // task (assign to prefix) : str.hextoa(i)
    case VERI_METHOD_OCTTOA : // task (assign to prefix) : str.octtoa(i)
    case VERI_METHOD_REALTOA : // task (assign to prefix) : str.realtoa(r)
        // Check number of arguments (1) :
        if (!arguments || arguments->Size()!=1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
        // Also check prefix is a string (the pre-defined type, so no dimensions will be given) :
        if (!prefix_type || (!prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && !prefix_type->IsString())) {
            // VIPER #3784 && #3845: Prefix is not a string type, error:
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        // CHECK ME : Test that return value 'void' is OK here ? environment==VERI_UNDEF_ENV ?
        break ;
    // string manipulation/compare :
    case VERI_METHOD_COMPARE : // str.compare(str)
    case VERI_METHOD_ICOMPARE : // str.icompare(str)
    case VERI_METHOD_GETC : // str.getc(i)
        // Check number of arguments (1) :
        if (!arguments || arguments->Size()!=1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
        // Also check prefix is a string (the pre-defined type, so no dimensions will be given) :
        if (!prefix_type || (!prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && !prefix_type->IsString())) {
            // VIPER #3784 && #3845: Prefix is not a string type, error:
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        break ;
    case VERI_METHOD_PUTC : // task (assign to prefix) : str.putc(i,s)
    case VERI_METHOD_SUBSTR : // task (assign to prefix) : str.substr(i,j)
        // Check number of arguments (2) :
        // VIPER #5248 (vcs_compatibility_issue-9000387999): According to LRM P1800(2009) this function expect 2 arguments,
        // thus we are giving correct error but vcs does not error out. So we put this check into relaxed check in mode.
        if (InRelaxedCheckingMode()) {
            if (!arguments || (arguments->Size() != 1 && arguments->Size() !=2)) Error("%s needs at least %d arguments", PrintToken(function_type), 1) ;
        } else { // expect at least one argument
            if (!arguments || arguments->Size() != 2) Error("%s expects %d arguments", PrintToken(function_type), 2) ;
        }
        // Also check prefix is a string (the pre-defined type, so no dimensions will be given) :
        if (!prefix_type || (!prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && !prefix_type->IsString())) {
            // VIPER #3784 && #3845: Prefix is not a string type, error:
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        // CHECK ME : Test that return value 'void' is OK here ? environment==VERI_UNDEF_ENV ?
        break ;

    // enum (and associative/dynamic arrays) :
    case VERI_METHOD_NUM :   // enum.num() or array.num()
        {
            // Check number of arguments (0) :
            // If the function is called with '()', we create a single argument with value 0.
            // If the function is called without '()', we don't create any argument.
            // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
            if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;

            // VIPER #5642 : Use VeriTypeInfo instead of VeriDataType to
            // verify prefix type
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()
                 && !prefix_type->IsEnumeratedType() && !prefix_type->IsAssociativeArrayType()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
            }
            break ;
        }
    case VERI_METHOD_NAME :  // enum.name()
        {
            // Check number of arguments (0) :
            // If the function is called with '()', we create a single argument with value 0.
            // If the function is called without '()', we don't create any argument.
            // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
            if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
            // Get the prefix's MSB dimension :
            VeriRange *msb_dim = (prefix_id) ? prefix_id->GetDimensionAt(0): 0 ;
            // If there are no dimensions, the prefix should be of an enum datatype.
            //if (!prefix || (!msb_dim && (prefix_id && (!prefix_id->GetDataType() || !prefix_id->GetDataType()->GetEnums())))) {
            // VIPER #5642 : Use VeriTypeInfo instead of VeriDataType to
            // verify prefix type
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()
                 && !prefix_type->IsEnumeratedType()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
            }
            // VIPER 2452 : Enum can have a dimension as data type. Don't check dimension if prefix is enum
            if (prefix_type && prefix_type->IsEnumeratedType()) break ;
            //if (prefix_id && prefix_id->GetDataType() && prefix_id->GetDataType()->GetEnums()) break ;
            // For associative arrays : Check dimension (FIX ME : check that dimension is associative):
            VeriExpression *left = msb_dim ? msb_dim->GetLeft(): 0 ; // Msb of range
            VeriExpression *right = msb_dim ? msb_dim->GetRight(): 0 ; // Lsb of range
            // For associative array dimension can have '*', data type or type id.
            // If data type it is stored in 'right' and if type id it is stored in
            // 'left' of dimension.
            if (msb_dim && (msb_dim->GetPartSelectToken()!=VERI_MUL) &&
                !(right && right->IsDataType() && !left) &&
                !(left && left->GetId() && left->GetId()->IsType() && !right)) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"an associative array") ;
            }
        }
        break ;
    // enum (and associative/dynamic arrays). first(), last() takes 0 or 1 argument - SV LRM 3.1, section 4.10.4 :
    case VERI_METHOD_FIRST : // enum.first(N) or array.first(N)
    case VERI_METHOD_LAST :  // enum.last(N) or array.last(N)
    case VERI_METHOD_NEXT :  // enum.next(N=1)  or array.next(N=1)
    case VERI_METHOD_PREV :  // enum.prev(N=1)  or array.next(N=1)
        {
            // Check number of arguments (0 or 1) :
            if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),0) ;
            // Prefix should be enum/associative array
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()
                  && !prefix_type->IsEnumeratedType() && !prefix_type->IsAssociativeArrayType()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
            }
            // Get the prefix's MSB dimension :
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
            // If there are no dimensions, the prefix should be of an enum datatype.
            //if (!prefix || (!msb_dim && (prefix_id && (!prefix_id->GetDataType() || !prefix_id->GetDataType()->GetEnums())))) {
                //Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
            //}
            // VIPER 2452 : Enum can have a dimension as data type. Don't check dimension if prefix is enum
            //if (prefix_id && prefix_id->GetDataType() && prefix_id->GetDataType()->GetEnums()) break ;
            // For associative arrays : Check dimension (FIX ME : check that dimension is associative):
            //VeriExpression *left = msb_dim ? msb_dim->GetLeft(): 0 ; // Msb of range
            //VeriExpression *right = msb_dim ? msb_dim->GetRight(): 0 ; // Lsb of range
            // For associative array dimension can have '*', data type or type id.
            // If data type it is stored in 'right' and if type id it is stored in
            // 'left' of dimension.
            //if (msb_dim && (msb_dim->GetPartSelectToken()!=VERI_MUL) &&
                //!(right && right->IsDataType() && !left) &&
                //!(left && left->GetId() && left->GetId()->IsType() && !right)) {
                //Error("prefix of method %s should be %s",PrintToken(function_type),"associative array") ;
            //}
        }
        break ;

    // dynamic/associative arrays and queues:
    case VERI_METHOD_EXISTS : // array.exists(index)
    case VERI_METHOD_DELETE : // array.delete() or array.delete(index)
        {
            // Check number of arguments (0 or 1) :
            if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
            // Get the prefix's MSB dimension :
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
            // Get left bound of MSB dimension
            //VeriExpression *left = msb_dim ? msb_dim->GetLeft() : 0 ;
            // Check dimension, and what type of array it is (dynamic/associative or a queue)
            //if (!prefix || !msb_dim || (msb_dim->GetPartSelectToken()!=VERI_MUL && msb_dim->GetPartSelectToken()!=VERI_NONE && left && !left->IsDollar())) {
            // VIPER #5642 : Use VeriTypeInfo instead of VeriDataType to
            // verify prefix type
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() &&
                  !prefix_type->IsAssociativeArrayType() && !prefix_type->IsDynamicArrayType() &&
                  !prefix_type->IsQueue()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a dynamic array or associative array or queue") ;
            }
        }
        break ;
    //case VERI_METHOD_SIZE : // array.size() (associative or dynamic arrays only ?)
    case VERI_SYS_CALL_SIZE : // the actual token returned from string 'size'.
        {
            // Check number of arguments (0) :
            // If the function is called with '()', we create a single argument with value 0.
            // If the function is called without '()', we don't create any argument.
            // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
            if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() &&
                   !prefix_type->IsAssociativeArrayType() && !prefix_type->IsDynamicArrayType() &&
                   !prefix_type->IsQueue()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a dynamic array or associative array or queue") ;
            }
            // Get the prefix's MSB dimension :
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
            // Get left bound of MSB dimension
            //VeriExpression *left = msb_dim ? msb_dim->GetLeft() : 0 ;
            // Check dimension, and what type of array it is (dynamic/associative or a queue)
            //if (!prefix || !msb_dim || (msb_dim->GetPartSelectToken()!=VERI_MUL && msb_dim->GetPartSelectToken()!=VERI_NONE && left && !left->IsDollar())) {
                //Error("prefix of method %s should be %s",PrintToken(function_type),"associative or dynamic array or queue") ;
            //}
        }
        break ;
    // queues :
    case VERI_METHOD_INSERT : // queue.insert(index,item)
        {
            // Check number of arguments (2) :
            if (!arguments || arguments->Size()!=2) Error("%s expects %d arguments",PrintToken(function_type),2) ;
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() &&
                    !prefix_type->IsQueue()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a queue") ;
            }
            // Get the prefix's MSB dimension :
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0) : 0 ;
            // Get left bound of MSB dimension
            //VeriExpression *left = msb_dim ? msb_dim->GetLeft() : 0 ;
            // Check dimension, and what type of array it is (must be a queue)
            //if (!prefix || !msb_dim || (left && !left->IsDollar())) {
                //Error("prefix of method %s should be %s",PrintToken(function_type),"queue") ;
            //}
        }
        break ;

    case VERI_METHOD_POP_BACK : // queue.pop_back()
    case VERI_METHOD_POP_FRONT : // queue.pop_front()
        {
            // Check number of arguments (0) :
            // If the function is called with '()', we create a single argument with value 0.
            // If the function is called without '()', we don't create any argument.
            // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
            if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
            // Get the prefix's MSB dimension :
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
            // Get left bound of MSB dimension
            //VeriExpression *left = msb_dim ? msb_dim->GetLeft() : 0 ;
            // Check dimension, and what type of array it is (must be a queue)
            //if (!prefix || !msb_dim || (left && !left->IsDollar())) {
            // VIPER #5642 : Use VeriTypeInfo instead of VeriDataType to
            // verify prefix type
            if (prefix_type && !prefix_type->IsTypeParameterType() &&
                    !prefix_type->IsUnresolvedNameType() && !prefix_type->IsQueue()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a queue") ;
            }
        }
        break ;

    case VERI_METHOD_PUSH_BACK : // queue.push_back(item)
    case VERI_METHOD_PUSH_FRONT : // queue.push_front(item)
        {
            // Check number of arguments (1) :
            if (!arguments || arguments->Size()!=1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
            // Get the prefix's MSB dimension :
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
            // Get left bound of MSB dimension
            //VeriExpression *left = msb_dim ? msb_dim->GetLeft() : 0 ;
            // Check dimension, and what type of array it is (must be a queue)
            //if (!prefix || !msb_dim || (left && !left->IsDollar())) {
            if (prefix_type && !prefix_type->IsTypeParameterType() &&
                    !prefix_type->IsUnresolvedNameType() && !prefix_type->IsQueue()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a queue") ;
            }
        }
        break ;

    // Randomization of class variables..
    case VERI_METHOD_CONSTRAINT_MODE : // task constraint_mode(bit) or function object.constraint_mode()
    case VERI_METHOD_RAND_MODE :
         // IEEE 1800-2012 (8.26.9) : Call to rand_mode/constraint_mode on interface class handle is illegal
         if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()) {
             VeriIdDef *prefix_type_id = prefix_type->GetNameId() ;
             VeriModuleItem *type_body = (prefix_type_id && prefix_type_id->IsClass()) ? prefix_type_id->GetModuleItem(): 0 ;
             if (type_body && type_body->GetQualifier(VERI_INTERFACE)) {
                 Error("call to rand_mode/constraint_mode on interface class handle is illegal") ;
             }
         }
    case VERI_METHOD_POST_RANDOMIZE :
    case VERI_METHOD_PRE_RANDOMIZE :
        // Only moderate checks done here.
        // Check number of arguments (0 or 1) :
        if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Work on any 'object' prefix in which the (a?) constraint is defined...CHECK SEMANTICS
        // CHECK ME : if 1 argument, Test that return value 'void' is OK here. environment==VERI_UNDEF_ENV ?
        break ;

    // 3.1a array reduction operations ('with' operation not tested here) :
    case VERI_METHOD_AND :  // array.and(prefix=item) with item
    case VERI_METHOD_OR :   // array.or
    case VERI_METHOD_XOR :  // array.xor
    case VERI_METHOD_SUM :  // array.sum // Viper 5016
    case VERI_METHOD_PRODUCT : // array.product // Viper 5016
        // Check number of arguments (0 or 1) :
        if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
        if (!prefix || (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()
                    && !prefix_type->IsUnpackedArrayType())) {
        // Check prefix (must be an unpacked array) :
        //if (!prefix || (prefix_id && prefix_id->Dimension()==0)) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
        }
        break ;

    // P1800 array index query method : VIPER 2546
    case VERI_METHOD_INDEX : // array.index(int)
        // Check number of arguments (0 or 1) :
        if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Check prefix must be an array :
        //if (!_prefix_id->PackedDimension() && !_prefix_id->UnPackedDimension()) {
            //Error("prefix of method %s should be %s",PrintToken(function_type),"unpacked/packed array") ;
        //}
        // VIPER #3029. The prefix can only be an array iterator for array manipulation methods.
        if (!prefix_id || !prefix_id->IsIterator()) Error("prefix of method %s should be %s",PrintToken(function_type),"an array iterator") ;
        break ;
    // 3.1a array locator methods : operate on all unpacked arrays ('with' operation not tested here) :
    case VERI_METHOD_FIND : // array.find(item) with (item-expr)
    case VERI_METHOD_FIND_FIRST :
    case VERI_METHOD_FIND_FIRST_INDEX :
    case VERI_METHOD_FIND_INDEX :
    case VERI_METHOD_FIND_LAST :
    case VERI_METHOD_FIND_LAST_INDEX :
    case VERI_METHOD_MAX :
    case VERI_METHOD_MIN :
    case VERI_METHOD_UNIQUE :
    case VERI_METHOD_UNIQUE_INDEX :
        {
            // Check number of arguments (0 or 1) :
            if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
            // Check prefix (must be an unpacked array) :
            if (!prefix || (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()
                    && !prefix_type->IsUnpackedArrayType())) {
            //if (!prefix_id || prefix_id->Dimension()==0) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
            }
            // Viper 5314. index type methods cannot have associative wildcard array as prefix
            //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
            //if (msb_dim && msb_dim->GetPartSelectToken() == VERI_MUL && ((function_type == VERI_METHOD_FIND_FIRST_INDEX)||
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()
                 && prefix_type->IsAssociativeArrayType() && !prefix_type->IndexType()
                 && ((function_type == VERI_METHOD_FIND_FIRST_INDEX)||
                                                                         (function_type == VERI_METHOD_FIND_INDEX)      ||
                                                                         (function_type == VERI_METHOD_FIND_LAST_INDEX) ||
                                                                         (function_type == VERI_METHOD_UNIQUE_INDEX))) {
                Error("prefix of array method %s should not be %s", PrintToken(function_type), "a wildcard associative array") ;
            }
        }
        break ;

    // 3.1a : array ordering methods : operate on all one-dim unpacked arrays ('with' operation not tested here) (page 50) :
    // VIPER #3093: Allow array ordering method 'reverse' for packed arrays as well.
    case VERI_METHOD_REVERSE : // array.reverse(arg=item) with (item-expr)
        // Check number of arguments (0 or 1) :
        if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
        // Check prefix. Error out if the array has more than one unpacked dimensions or has no dimensions(either packed or unpacked) at all.
        // Viper #5248 (test_84) : Allow built-in data type having size > 1
        // VIPER #8354: According to IEEE 1800-2009 LRM section 7.12.2, array
        // ordering methods work on unpacked array. But VCS supports this for
        // packed array too
        // So allowed packed array only in relaxed checking mode
        if (InRelaxedCheckingMode()) {
            if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() &&
                 ((!prefix_type->UnpackedDimension() && !prefix_type->PackedDimension()) &&
                 //((prefix_type->UnpackedDimension()> 1) &&
                 (!(prefix_type->IsBuiltInType() && prefix_type->BaseTypeSize()> 1)))) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a packed or unpacked array") ;
            }
        } else if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && !prefix_type->UnpackedDimension()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
        }
        // According to IEEE 1800-2009 LRM section 7.12.2, array ordering methods
        // are not allowed for associative array
        if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && prefix_type->IsAssociativeArrayType() && !InRelaxedCheckingMode()) {
            Error("prefix of array method %s should not be %s", PrintToken(function_type),"an associative array") ;
        }
        //if (!prefix_id || ((!prefix_id->Dimension() && !prefix_id->PackedDimension()) || prefix_id->Dimension()>1 && !(prefix_type && prefix_type->IsBuiltInType() && prefix_type->BaseTypeSize()> 1))) {
            //if (InRelaxedCheckingMode()) {
                //Warning("prefix of method %s should be %s",PrintToken(function_type),"one-dimensional array") ;
            //} else {
                //Error("prefix of method %s should be %s",PrintToken(function_type),"one-dimensional array") ;
            //}
        //}
        break ;
    case VERI_METHOD_RSORT :
    case VERI_METHOD_SORT :
    case VERI_METHOD_SHUFFLE :
        // Check number of arguments (0 or 1) :
        if (arguments && arguments->Size()>1) Error("%s expects %d arguments",PrintToken(function_type),1) ;
        // Check prefix (must be an unpacked array) :
        // VIPER #8354: According to IEEE 1800-2009 LRM section 7.12.2, array
        // ordering methods work on unpacked array.
        if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() &&
                  !prefix_type->UnpackedDimension()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
        }
        // According to IEEE 1800-2009 LRM section 7.12.2, array ordering methods
        // are not allowed for associative array
        if (prefix_type && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType() && prefix_type->IsAssociativeArrayType() && !InRelaxedCheckingMode()) {
            Error("prefix of array method %s should not be %s", PrintToken(function_type),"an associative array") ;
        }
        break ;

    // 3.1a pre-defined tasks on a class 'process' (page 100) :
    case VERI_METHOD_AWAIT :
    case VERI_METHOD_KILL :
    case VERI_METHOD_SUSPEND :
    case VERI_METHOD_RESUME :
    case VERI_METHOD_STATUS :
    case VERI_METHOD_SELF :
    case VERI_METHOD_GET_RANDSTATE : // page 154. apply to a class 'process'.
        // Check number of arguments (0) :
        // If the function is called with '()', we create a single argument with value 0.
        // If the function is called without '()', we don't create any argument.
        // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Check that prefix is a (class) object ?
        break ;
    case VERI_METHOD_SET_RANDSTATE :
        if (!arguments || (arguments->Size() != 1)) Error("%s expects %d arguments",PrintToken(function_type),1) ;
        // Check that prefix is a (class) object ?
        break ;

    // LRM 17.7.10 / 17.12.5 : methods on sequences :
    case VERI_METHOD_ENDED :
    case VERI_METHOD_MATCHED :
        // Check number of arguments (0) :
        // If the function is called with '()', we create a single argument with value 0.
        // If the function is called without '()', we don't create any argument.
        // So, check that, number of arguments should be 0, if there is an argument, it should also be 0.
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Check that prefix is a sequence object ?
        if (prefix_id && !prefix_id->IsSequence()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a sequence") ;
        }
        // VIPER #3961 : Produce error if method ended/matched is used in the rest
        // expression of disable statement
        if (action == DISABLE_RESET) {
            Error("method %s is not allowed in disable iff condition", PrintToken(function_type)) ;
        }
        break ;

    // ?? only for linked lists in class objects ? Which classes ? Not well defined..
    case VERI_METHOD_BACK :
    case VERI_METHOD_CLEAR :
    case VERI_METHOD_DATA :
    case VERI_METHOD_EMPTY :
    case VERI_METHOD_EQ :
    case VERI_METHOD_ERASE :
    case VERI_METHOD_ERASE_RANGE :
    case VERI_METHOD_FRONT :
    case VERI_METHOD_INSERT_RANGE :
    case VERI_METHOD_NEQ :
    case VERI_METHOD_PURGE :
    case VERI_METHOD_SET :
    case VERI_METHOD_START :
    case VERI_METHOD_SWAP :
    //case VERI_METHOD_FINISH :
    case VERI_SYS_CALL_FINISH :
    case VERI_METHOD_RANDOMIZE :  // for VIPER# 4332
        break ;

    case VERI_METHOD_NEW : // ?? Not sure how this is used as a 'method'. "some_type.new() ?"
    case VERI_METHOD_PROCESS : // ?? Don't know what this is. A class by itself ?
    case VERI_METHOD_SIE : // ?? typo in LRM ?
        break ;

    // Covergroup methods
    //case VERI_METHOD_GET_COVERAGE :
    case VERI_SYS_CALL_GET_COVERAGE :
    case VERI_METHOD_GET_INST_COVERAGE :
    case VERI_METHOD_SET_INST_NAME :
    //case VERI_METHOD_STOP :
    case VERI_SYS_CALL_STOP :
    case VERI_METHOD_SAMPLE :
    case VERI_METHOD_TRIGGERED :
        break ;
    default :
        // This is not a (known) method. Discard as method :
        function_type = 0 ;
        delete prefix_type ;
        return ; // do not do any more tests..
    }

    if (VeriVarUsageInfo::IsInSampledValueFunction()) {
        // VIPER #7834/7082: Produce error if matched/triggered is used in the argument of
        // sampled value functions (1800-2009 section 16.9.3)
        switch(function_type) {
        case VERI_METHOD_MATCHED :
        case VERI_METHOD_TRIGGERED :
            Error("method triggered/matched is not allowed in argument of sampled value function") ;
        default :
            break ;
        }
    }
    delete prefix_type ;
}
/*--------------------------------------------------------------------------*/

void
VeriAssignInSequence::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    // Do not call VeriUnaryOperator::VarUsage. It will treat _arg as operand
    if (_arg) _arg->VarUsage(info, action) ;

    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM (_stmts, i, stmt) {
        if (stmt) (void) stmt->VarUsage(info, action) ;
    }
}
/*--------------------------------------------------------------------------*/

void
VeriDollar::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (info && info->GetInTemporalExpr() && (action != CYCLE_DELAY_RBOUND) && (action != INSTANCE_ARG)) {
        // This '$' is used inside temporal expression, but it is not used
        // as the right bound of cycle delay or argument of
        Error("illegal context for $") ;
    }
}

/*--------------------------------------------------------------------------*/
void
VeriSequenceConcat::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (_cycle_delay_range) _cycle_delay_range->VarUsage(info, CYCLE_DELAY) ;
    // Do not call VeriBinaryOperator::VarUsage. It will treat _left and _right as operand
    if (_left) _left->VarUsage(info, action) ;
    if (_right) _right->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/
void
VeriClockedSequence::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    // VIPER #4307 : Call var usage on event expression to resolve any hierarchical id
    // VIPER #5436: If we are within temporal expression, don't consider the
    // event expression as temporal expression
    unsigned in_temporal = (info) ? info->GetInTemporalExpr(): 0 ;
    if (in_temporal && info) info->SetInTemporalExpr(0) ;
    if (_event_expr) _event_expr->VarUsage(info, action) ;
    if (in_temporal && info) info->SetInTemporalExpr(1) ;
    // Do not call VeriUnaryOperator::VarUsage. It will treat _arg as operand
    if (_arg) _arg->VarUsage(info, action) ;
}
/*--------------------------------------------------------------------------*/

void VeriKeyword::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Post-analysis, the identifier HAS TO BE there if it is not a generic interface.
    if (!_id) {
        switch (_token) {
        case VERI_INTERFACE:
        case VERI_DOLLAR_ROOT:
        case VERI_NEW:
        case VERI_LOCAL: // VIPER #6391
            // Cannot error out. It is a generic interface or $root
            break ;
        default:
            // Here, we error out :
            Error("%s is not declared",_name) ;
            break ;
        }
    }

    // reference to _id :
    if (_id) _id->VarUsage(info, action, /*from*/ this) ;
}
/*--------------------------------------------------------------------------*/

void
VeriPortConnect::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    if (_connection) _connection->VarUsage(info, action) ;
}

/*--------------------------------------------------------------------------*/
/*Begin VeriIdDef derived objects:

VeriVariable
VeriInstId
VeriPrototypeId

  NOT implemented ------------------------

VeriBlockId
VeriGenVarId
VeriModportId
VeriModuleId
VeriParamId
VeriPslId
VeriTaskId

*/
/*--------------------------------------------------------------------------*/

void
VeriIdDef::VarUsage(VeriVarUsageInfo *info, unsigned action, const VeriTreeNode *from)
{
    // Do semantic checks here.
    if (!from) from = this ; // just to get line/file info set

    // Tests on identifier usage.

    // This is similar to tests done in VeriIdDef::CheckEnvironment, although now we
    // are 'complete' w.r.t. component instantiation actuals, and thus can do accurate
    // concurrent/reg assignment tests. Also we can do other tests that require (or often have)
    // hierarchical names  (and thus were not tested in Resolve()).

    // We cannot really test identifiers that are not a IsVar() (variable or net),
    // because this routine is also called for the prefix of selected names.
    // The only objects that cannot be a pure 'container' of other objects are IsVar().
    // 'parameter' or 'genvar' could also be tested, but we use 'blocking_assign' as a environment
    // for legal parameter/genvar assignments too. So ONLY test identifiers that are IsVar() here. FIX ME.
    if (!IsVar() && !IsGenVar() && !IsParam()) return ;

    if (info && info->IsInConstraintDecl()) {
        Map* rand_randc = info->GetRandRandCVars() ;
        unsigned  qualifier = rand_randc ? (unsigned)(unsigned long)rand_randc->GetValue(this) : 0 ;
        if (qualifier == VERI_RAND) info->ContainsRand() ;
        if (qualifier == VERI_RANDC) info->ContainsRandC() ;
    }
    if (action==CONSTANT) {
        if (!IsParam() && !IsGenVar() && !IsFunction()) { // to be extra safe
            from->Error("%s is not a constant", Name()) ;
        }
    }

    SynthesisInfo *this_info = 0 ;

    // Test concurrent assignment to a 'reg' type for System Verilog.
    // This code moved here from 'Resolve()', since we need to be
    // precise for the assignments done. For SV, at most 1 concurrent assignment is allowed.

    // Need to test non-variables too. Especially the external (hierarchical) ones that were just resolved.
    // Carefull not to include testing for modules and such, since this routine might be called
    // on prefixes too, which are by definition not assignable.
    switch (action) {
    case NONBLOCKING_ASSIGNING :
    case PARTIAL_NONBLOCKING_ASSIGNING :
        // VIPER #3272 : Automatic variables shall not be written with non-blocking,
        // continuous or procedural continuous assignments.
        if (IsAutomatic()) { // Variable is automatic
            from->Error("automatic variable %s cannot be written in this context", Name()) ;
        }

    case BLOCKING_ASSIGNING :
    case PARTIAL_BLOCKING_ASSIGNING :
        // Any assignment (full or partial).
        if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            // in some concurrent assignment..
            // Verilog defines this is only allowed on a net :
            // Also rule out any non-variables (struct members or modport-ports), because they are containers of fields,
            // and we would error out if they are assigned multiple times but to different fields ?
            // VIPER #3266 : Also rule out interface type ports here :
            if (!IsNet() && !IsMember() && !IsModportPort() && !IsInterfacePort()) {
                if ((IsVar() && !IsSystemVeri()) || (IsGenVar() || IsParam())) {
                    // In non-System-Verilog, any assignment to a variable is an error.
                        from->Error("concurrent assignment to a non-net %s is not permitted", Name()) ;
                } else if (IsConcurrentAssigned(this_info) && (action==NONBLOCKING_ASSIGNING || action==BLOCKING_ASSIGNING) && !IsLocalVar()) {
                    // For SV, allow one single assignment to a non-net, but not more. LRM 5.6.
                    // Ignore local variables of sequence/property :
                    // IMPORTANT NOTE : the 'IsConcurrentAssigned()' call creates 'memory' in this (VarUsage) routine,
                    // which makes VarUsage() non-re-entrant. VarUsage() should only be called once on a parse tree.
                    // Issue 2254 and 2232 : Do not error out on partial assignments, so rule out partial assignments.
                    // VIPER #2656: Do not error out, make it a warning. It may not be a multiple driver situation
                    // if it is connected as actual to a black box instantiation. If it is really a multiple driver,
                    // we will any way error out in electrical check later in the RTL elaboration.
                    // VIPER 3725 : had to change message to 'might' since we cannot do good analysis from generate (if/case).
                    from->Warning("variable %s might have multiple concurrent drivers",Name()) ;
                }
            }
        } else {
            // in a procedural statement
            // Any assignment to a net is not allowed :
            // Covergroups can be in the LHS of an assignment with 'new' in the RHS.
            // Local variables of sequence/property can be used as lhs of assignment
            if (!IsReg() && !IsCovergroup() && !IsLocalVar()) {
                from->Error("procedural assignment to a non-register %s is not permitted", Name()) ;
            }
        }
        break ;
    case SUBPROG_NAME :
        // Need to do this test, Resolve() did not do that yet..
        // Now, prefix of a function name CAN be a variable that is of 'type' of a 'interface'.
        // In that case, the suffix will be the function name. Prefix will be a varibale.
        // FIX ME : This means that we cannot do this test here.
        // Maybe we need to distinguish between testing context 'full' or 'selected' or 'indexed',
        // totally separate from the 'environment'.
        // So : FIX ME. Need to re-think id testing in Resolve() and here.
//        if (!IsFunction() && !IsTask() && !IsSequence() && !IsProperty()) {
//            from->Error("%s is not a subprogram",Name()) ;
//        }
        break ;
    default :
        break ;
    }
}

/*--------------------------------------------------------------------------*/

// Check the use/assign status of the variable based on
// the current action and the previously calculated
// assignment status.  This calculation is done in the
// context of the VarUsageInfo.  Here, if the "info" field
// is NULL, then we assume we are inside a concurrent statement.

void
VeriVariable::VarUsage(VeriVarUsageInfo *info, unsigned action, const VeriTreeNode *from)
{
    if (action == DEAD) return ;
    // VCS Compatibility issue (foreach) : Do not check void function, void
    // function is like task and task identifier is not checked by VarUsage
    if (IsFunction() && (Type() == VERI_VOID)) return ;

    if (action == CONSTRAINT_DECL && info) {
        Map* rand_randc = info->GetRandRandCVars()  ;
        unsigned  qualifier = rand_randc ? (unsigned)(unsigned long)rand_randc->GetValue(this) : 0 ;
        if (qualifier == VERI_RAND) info->ContainsRand() ;
        if (qualifier == VERI_RANDC) info->ContainsRandC() ;
    }

    // First, do general semantic checks on this usage of this variable :
    VeriIdDef::VarUsage(info, action, from) ;

    // Get the index into the 'info' set :
    unsigned indx = (info) ? info->AddVariable(this) : 0 ;

    SynthesisInfo *this_info = 0 ;

    // calculate changes in variable status based on the
    // previous status and the current action
    switch(action) {
    case USING:
    case PROCEDURAL_CONTINOUS_ASSIGN:
    case PARTIAL_USING:
        // In a concurrent area, using the ID causes it to be UBA
        // In a sequential area, if it has been previously not
        //   fully assigned, using the ID causes it to be UBA
        if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()) || !info->GetFullyAssigned(indx)) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            SetUsedBeforeAssigned(this_info) ;
            UnSetAssignedBeforeUsed(this_info) ;
        }

        // two non-mutually exclusive uses in sequential area -- cannot be a dual-port RAM
        if (IsConcurrentUsed(this_info) || (info && info->GetEverUsed(indx))) SetCannotBeDualPortRam(this_info) ;

        // update usage info depending on context
        if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            info->SetEverUsed(indx) ;
        } else {
            SetConcurrentUsed(this_info) ;
        }
        break;
    case NONBLOCKING_ASSIGNING:
        {
        const VeriTreeNode *msg_node = (from) ? from : this ;
        // VIPER #7831: Produce the warning on 'from' rather than on 'this' for better linefile:
        // VIPER #8322: Do not produce warning if event is used in port declaration(According to IEEE LRM 1800-2009, section 23.2.2)
        if (_data_type && (_data_type->Type() == VERI_EVENT) && !IsPort()) msg_node->Warning("illegal reference to event type") ; // Viper #5326 : VCS gives error, ModelSim passes.
        }

    case CLOCKING_DRIVE:
        SetUsedBeforeAssigned(this_info) ;
        UnSetAssignedBeforeUsed(this_info) ;

    case GENVAR_ASSIGN : // VIPER #8285 : Set flags similar to BLOCKING_ASSIGNING
    case BLOCKING_ASSIGNING:
    case CONTRIBUTION_LVALUE:

        // any variable which is directly assigned cannot be a DPRam.
        // All accesses to a DP RAM must be indexed accesses
        SetCannotBeDualPortRam(this_info) ;

        // update assignment info depending on context
        if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            info->SetFullyAssigned(indx) ;
            info->SetEverAssigned(indx) ;
        } else {
            SetConcurrentAssigned(this_info) ;
            // VIPER #4241 : Set assignment information
            VeriVarUsageInfo::SetAssignedInfoForVars(this, FULL_ASSIGNED, from) ;
        }
        break ;

    case PARTIAL_NONBLOCKING_ASSIGNING:
        SetUsedBeforeAssigned(this_info) ;
        UnSetAssignedBeforeUsed(this_info) ;

    case PARTIAL_BLOCKING_ASSIGNING:

        // two non-mutually exclusive assignments in sequential area -- cannot be a dual-port RAM
        if (IsConcurrentAssigned(this_info) || (info && (!info->GetInConditionalGenerate() || (info->GetInConditionalGenerate() && info->GetInAlwaysInitial())) && info->GetEverAssigned(indx))) SetCannotBeDualPortRam(this_info) ;

        // update assignment info depending on context
        if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            info->SetEverAssigned(indx) ;
        } else {
            SetConcurrentAssigned(this_info) ;
            // VIPER #4241 : Set assignment information
            VeriVarUsageInfo::SetAssignedInfoForVars(this, PARTIAL_ASSIGNED, from) ;
        }
        // If local variable of sequence/property is of type structure and its
        // elements are assigned, it will be considered as fully assigned. Standard simulators behave that way
        if (info && info->GetInTemporalExpr()) {
            info->SetFullyAssigned(indx) ;
        }
        break;

    case PORT_CONNECTION:
        // This is a (concurrent) module instantiation actual expression.

        // We don't know if this is a use or assign. Assume both, both no semantic checks..
        // Either way, discard as Ram..
        SetCannotBeDualPortRam(this_info) ;

        // Be conservative on the use-before-assign here :
        SetUsedBeforeAssigned(this_info) ;
        UnSetAssignedBeforeUsed(this_info) ;

        // And set as concurrently assigned (to be in line with the IsUsed/IsAssigned flags
        SetConcurrentAssigned(this_info) ;
        SetConcurrentUsed(this_info) ;
        break ;

    case EVENT_USAGE:
        // expect a event expression here. This is NOT a 'read'.
        break ;
    case TYPE_USAGE:
        // expect a type here. Test already done in Resolve. This is not a 'read'.
        break ;
    case SUBPROG_NAME:
        // expect a subprogram here. This is NOT a 'read'.
        break ;

    case SEQ_ENDED :
    case DISABLE_RESET :
    case INSTANCE_ARG :
    case CYCLE_DELAY :
        // identifier is used in these environments :
        if (info && !info->GetFullyAssigned(indx)) {
            SetUsedBeforeAssigned(this_info) ;
            UnSetAssignedBeforeUsed(this_info) ;
        }
        // update usage info depending on context
        if (info && (!info->GetInConditionalGenerate() || info->GetInAlwaysInitial())) { // VIPER #8278: Check for generate conditional along with in always/initial flag
            info->SetEverUsed(indx) ;
        } else {
            SetConcurrentUsed(this_info) ;
        }
        break ;
    case SYSTASKCALL_ARG:
        break ;
    //case GENVAR_ASSIGN :
        //break ; // Nothing to set VIPER #8109
    default:
        // Unexpected environment : be conservative:
        SetCannotBeDualPortRam(this_info) ;
        SetUsedBeforeAssigned(this_info) ;
        UnSetAssignedBeforeUsed(this_info) ;
    }

    // VIPER #4987: Always set as concurrently used for variables declared
    // inside generate block. We, actually, do not know whether it is used
    // or not. It may be hierarchically used and the hier-ref inside the
    // generate block is not resolved, so this used status will not be
    // properly set and that may create some issue later in the flow.
    VeriScope *runner = _present_scope ;
    while (runner && !runner->IsGenerateScope()) runner = runner->Upper() ;
    if (runner) SetConcurrentUsed(this_info) ;
}

/*--------------------------------------------------------------------------*/
void
VeriPrototypeId::VarUsage(VeriVarUsageInfo* info, unsigned action, const VeriTreeNode *from)
{
    if (action == DEAD) return ;
    if (_actual_id) _actual_id->VarUsage(info, action, from) ;
}
/*--------------------------------------------------------------------------*/
/*Begin Misc Objects

VeriNetRegAssign
VeriCaseItem
VeriDelayOrEventControl
*/
/*--------------------------------------------------------------------------*/

void
VeriNetRegAssign::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // test the RHS first in case the same variable is
    // on the RHS and the LHS

    if (_val) _val->VarUsage(info, PROCEDURAL_CONTINOUS_ASSIGN) ;

    if (_lval) _lval->VarUsage(info, NONBLOCKING_ASSIGNING) ;
    // Presence of 'info' means procedural area i.e. this is procedural continuous
    // assignment. No 'info' specifies concurrent continuous assignment
    if (_lval && _val) _lval->CheckAssignment(_val, (info) ? VERI_EQUAL: VERI_ASSIGN) ; // Type check
    // Viper 5411, 4800 Produce error if streaming concatenation is
    // larger than other side of assignment.
    if (IsStaticElab()) VeriNode::CheckStreamingConcat(_lval, _val) ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriCaseItem::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    if (_scope) _present_scope = _scope ;

    // Traverse choices
    VeriExpression *dr;
    unsigned i;
    FOREACH_ARRAY_ITEM(_conditions, i, dr) {
        if (dr) dr->VarUsage(info, action) ;
    }

    // Traverse statement
    if (_stmt) action = _stmt->VarUsage(info, action) ;

    _present_scope = save_scope ;

    return action ;
}

/*--------------------------------------------------------------------------*/

void
VeriDelayOrEventControl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;
    if (_delay_control) _delay_control->VarUsage(info, action) ;
    if (_repeat_event) _repeat_event->VarUsage(info, action) ;

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_event_control, i, expr) {
        if (expr) expr->VarUsage(info, action) ;
    }
}

/*--------------------------------------------------------------------------*/

unsigned
VeriDataDecl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    // Resolve work :
    // Also for a DataDecl, we need to still resolve System Verilog
    // data types 'interface' types, or at least error out on undefined types.
    if (_data_type) {
        _data_type->VarUsage(info, TYPE_USAGE) ;
    }

    // DataDecl can be a declaration of a local variable in a sequential area.
    // That would be a 'reg' decl.
    // For that, we need to check the initial value on the id's.
    // If there, we need to send a (blocking) assignment in :
    int i;
    VeriIdDef *id;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;
        VeriTypeInfo *port_type = id->CreateType(0) ;
        // Chandles cannot be used as ports
        if (id->IsPort()) { // Port identifier
            //VeriTypeInfo *port_type = id->CreateType(0) ;
            if (port_type && port_type->IsChandleType()) {
                id->Error("illegal reference to chandle type") ;
            }
            //delete port_type ;
        } else {
            // VIPER #7392 side effect: Non-virtual, non-port, non-type(def) ids cannot
            // use interface/modport as data type, check and error out:
            VeriDataType *id_type = id->GetDataType() ;
            VeriIdDef *id_type_id = (id_type) ? id_type->GetId() : 0 ;
            if (id_type_id && id_type && !id_type->IsVirtualInterface() &&
                (id_type_id->IsInterface() || id_type_id->IsModport()) &&
                !id->IsType() && !IsTypeAlias()) {
                id->Error("%s is not a type", id_type_id->Name()) ;
            }
        }
        delete port_type ;
        VeriExpression *rhs = id->GetInitialValue() ;
        if (rhs) {
            // Type checking :
            VeriIdRef id_ref(id) ;
            id_ref.SetLinefile(Linefile()) ;
            id_ref.CheckAssignmentContext(rhs, id->IsParam() ? PARAM_INIT: INIT_VAL) ;
            if (IsStaticElab()) {
                // Viper 5411, 4800 Produce error if streaming concatenation is
                // larger than other side of assignment.
                VeriNode::CheckStreamingConcat(&id_ref, rhs) ;
            }
        }
        if (IsStaticElab() && (id->GetDataType() != _data_type) && !id->IsParam()) {
            // VIPER #6292 : Shift this check from static elaboration to here
            // This is a Verilog 2000 check (so we look at packed dimensions only).
            VeriRange *range = (_data_type) ? _data_type->GetDimensions() : 0 ;
            // Pick up the existing MSB range on the data type of the id :
            VeriDataType *id_data_type = id->GetDataType() ;
            VeriRange *exist = (id_data_type) ? id_data_type->GetDimensionAt(0): 0 ;
            if (range && exist) {
                // Both have a data type with dimensions.
                // VIPER #5190 : Produce error if ranges are different in two declarations of 'id'
                int curr_msb = 0, curr_lsb = 0 ;
                (void) range->GetWidth(&curr_msb, &curr_lsb, 0) ;
                int prev_msb = 0, prev_lsb = 0 ;
                (void) exist->GetWidth(&prev_msb, &prev_lsb, 0) ;
                if ((prev_msb != curr_msb) || (prev_lsb != curr_lsb)) {
                    if (id_data_type) id_data_type->Warning("%s was previously declared with a different range", id->Name()) ;
                }
            }
        }

        SynthesisInfo *this_info = 0 ;

        // RAM inference code. Moved here (from VeriVariable::Resolve()) after VIPER 3736 made it necessary to keep bit set/reset code in one routine (for re-entry).
        // Label id as a potential RAM if this is :
        //  (1) it is a REG variable (cannot be assigned in concurrent area), and
        //  (2) it has packed range (array) (this check to prevent massive extraction of bit-size rams), and
        //  (3) it has unpacked (memory) dimension 1 (don't extract RAM for multi-dimensional memories yet).
        //  (4) it is NOT a port (ports communicate with external vars, so we need predicatable data.
        // 11/2004 : relieve on (2) : no need for a packed range. Any packed data type will do.
        // 11/2004 : relieve on (3) : need at least 1 unpacked dimension. More is OK.
        if (id->IsReg() && id->UnPackedDimension()>=1 && /*id->PackedDimension()==1 && */ !id->IsPort()) {
//        if (id->IsReg() && (id->UnPackedDimension()>=1 || id->PackedDimension()>=1) && !id->IsPort()) { // If RAMs need to be inferred for plain unpacked arrays (VIPER 3797)
            VeriTypeInfo *id_type = id->CreateType(0) ;
            VeriTypeInfo *base_type = (id_type && !id_type->IsTypeParameterType() && !id_type->IsUnresolvedNameType()) ? id_type->BaseType() : 0 ;
            // VIPER #7197: RAM not yet supported for struct-union types
            if (!base_type || (!base_type->IsClassType() && !base_type->IsUnion() && !base_type->IsStructure())) {
                // set as potential dual port RAM
                id->SetCanBeDualPortRam(this_info) ;
                // Set as potential multi-port RAM
                id->SetCanBeMultiPortRam(this_info) ;
            }
            delete id_type ;
        }
        // VIPER #8109 : Call VarUsage on initial value of genvar decl
        // used to initialize for-generate-loop
        if (id->IsGenVar() && rhs && (action == FORGEN_INIT)) {
            rhs->VarUsage(info, FORGEN_INIT) ;
        }

        // There is an assignment only if there is an initial value.
        // For Nets (NetDecl()) this would be a concurrent assignment.
        // For Regs (here in DataDecl) an initial value is considered a 'procedural' assignment (LRM 5.6).
        // However, as such, outside a procedural area (!info) that assignment would only be
        // executed once, at startup time, and this mimic a 'initialization' value.
        // The only case where the reg initial assignment is always happening (as far as we know) is
        // for 'variables' (IsReg()) in a data flow.
        // There, it would be a blocking assignment that always happens when the dataflow executes.
        // All other identifiers receive initial value at power-up (initialization) time.
        // Test this (process only reg in dataflow) :
        if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial()) || !id->IsReg()) continue ; // VIPER #8278: Check for generate conditional along with in always/initial flag

        // In a procedural statement (info is set) this is a normal initialization.
        if (rhs) {
            rhs->VarUsage(info, USING) ;
            id->VarUsage(info, BLOCKING_ASSIGNING, 0) ;
        }

        // Viper #7724
        // Viper #7854: Even if the id is not yet marked as _can_be_ram (dp or mp),
        // it is important to discard them when improperly used. For external hier
        // names, they may later be marked as good for ram (from data-decl::var-usage).
        // The stikey flag on the id will ensure that an id discarded for ram can never
        // be marked as ram ever.
        if (info->GetInFunction()) {
            id->SetCannotBeDualPortRam(this_info) ;
            id->SetCannotBeMultiPortRam(this_info) ;
        }
    }

    return action ;
}

/*--------------------------------------------------------------------------*/

unsigned
VeriNetDecl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    int i;
    VeriIdDef *id;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

        // There is an assignment to the net only if it has an initial value :
        VeriExpression *rhs = id->GetInitialValue() ;
        if (rhs) {
            rhs->VarUsage(info, USING) ;
            id->VarUsage(info, NONBLOCKING_ASSIGNING, 0) ;
        }
    }
    return VALUE_IGNORED ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriSequenceDecl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    if (_is_processing) return action ; // Recursion

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    _is_processing = 1 ;

    VeriVarUsageInfo *local_info = (info) ? info->Copy() : 0 ;

    unsigned i, j ;
    VeriIdDef *id ;
    // If local variable is passed to an instance of a sequence on which ended
    // is applied, the formal argument to which local variable is bound must not
    // be referenced before it is assigned.
    if (action == SEQ_ENDED && local_info) {
        // This is the body of sequence on which ended is applied, mark the formals
        // with whom local variable is bound as not assigned :
        FOREACH_ARRAY_ITEM(_ports, i, id) {
            if (!id || !id->IsLocalVar()) continue ; // Local variable is not bound
            // Get the local variable from actual
            VeriExpression *actual = id->GetActual() ;
            VeriIdDef *local_var = (actual) ? actual->FullId() : 0 ;
            unsigned indx = local_info->AddVariable(local_var) ;
            if (local_info->GetFullyAssigned(indx)) { // Assigned before passing to this sequence
                local_info->ResetFullyAssigned(indx) ; // reset fully assigned flag
            }
        }
    }
    // VIPER #6450: If any local variable (formal local variable or declared in decl part)
    // contains initialization, consider that variable as assigned
    // First look at the formal local variables
    FOREACH_ARRAY_ITEM(_ports, i, id) {
        if (id && id->IsLocalVar() && id->GetInitialValue()) {
            id->VarUsage(local_info, BLOCKING_ASSIGNING, this) ;
        }
    }
    VeriModuleItem *decl ;
    Array *ids ;
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        ids = decl ? decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            if (id->GetInitialValue()) id->VarUsage(local_info, BLOCKING_ASSIGNING, decl) ;
        }
    }
    if (_spec) _spec->VarUsage(local_info, action) ;
    // Iterate over all local declarations and check whether they have proper flow
    // VIPER #6450 : Check flow for local formal arguments also
    FOREACH_ARRAY_ITEM(_ports, i, id) {
        if (!id || !id->IsLocalVar()) continue ;
        unsigned indx = (local_info) ? local_info->AddVariable(id): 0 ;
        // VIPER #3958 & 3960 : Produce error if local variables does not
        // flow properly i.e. it does not follow its assignment rule, but used some where.
        if (local_info && !local_info->GetFullyAssigned(indx) && local_info->GetEverUsed(indx)) id->Error("local variable %s referenced in expression where it does not flow", id->Name()) ;
    }
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        ids = decl ? decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            unsigned indx = (local_info) ? local_info->AddVariable(id): 0 ;
            // VIPER #3958 & 3960 : Produce error if local variables does not
            // flow properly i.e. it does not follow its assignment rule, but used some where.
            if (local_info && !local_info->GetFullyAssigned(indx) && local_info->GetEverUsed(indx)) id->Error("local variable %s referenced in expression where it does not flow", id->Name()) ;
        }
    }
    if (action == SEQ_ENDED && local_info) {
        // This is the body of sequence on which ended is applied, check whether
        // local variables bound with formals are assigned before use
        FOREACH_ARRAY_ITEM(_ports, i, id) {
            if (!id || !id->IsLocalVar()) continue ; // Local variable is not bound
            // Get the local variable from actual
            VeriExpression *actual = id->GetActual() ;
            VeriIdDef *local_var = (actual) ? actual->FullId() : 0 ;
            unsigned indx = local_info->AddVariable(local_var) ;
            if (!local_info->GetFullyAssigned(indx) && local_info->GetEverUsed(indx)) {
                id->Error("local variable passed to formal %s of sequence %s to which ended or triggered is applied. The local variable does not flow out of the sequence", id->Name(), (_id) ? _id->Name(): "") ;
            }
        }
    }

    if (local_info && info) {
        // Merge this local info to coming one :
        Array info_arr(1) ;
        info_arr.InsertLast(local_info) ;
        info->Combine(info_arr) ;
    }
    delete local_info ;
    _is_processing = 0 ;

    _present_scope = save_scope ;

    return action ;
}

/*--------------------------------------------------------------------------*/
unsigned
VeriModport::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    unsigned i ;
    VeriModuleItem *modport_decl ;
    FOREACH_ARRAY_ITEM(_modport_decls, i, modport_decl) {
        if (modport_decl) (void) modport_decl->VarUsage(info, action) ;
    }
    return action ;
}
/*--------------------------------------------------------------------------*/
unsigned
VeriModportDecl::VarUsage(VeriVarUsageInfo * /*info*/, unsigned action)
{
    unsigned i, j ;
    VeriModuleItem *modport_port_decl ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_modport_port_decls, i, modport_port_decl) {
        Array *ids = modport_port_decl ? modport_port_decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id || !id->IsNamedPort()) continue ;
            // VIPER #5906 : Call VarUsage on the port expressions :
            VeriExpression *port_expr = id->GetPortExpression() ;
            if (!port_expr) continue ;
            // VIPER #6740: Proper VarUsage for named port expressions:
            if (id->IsInput()) port_expr->VarUsage(0, USING) ;
            if (id->IsOutput()) port_expr->VarUsage(0, BLOCKING_ASSIGNING) ;
        }
    }
    return action ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriPropertyDecl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    if (_is_processing) {
        // VIPER #3950 :
        // This is a recursive property, check whether it contains 'disable iff'
        // If yes it is an error
        VeriExpression *spec = _spec ;
        // If _spec is clocked sequence, take its sequence expression
        if (spec && spec->GetArg()) spec = spec->GetArg() ;

        if (spec && (spec->OperType() == VERI_DISABLE)) {
            spec->Error("illegal context for disable iff") ;
        }
        if (info && info->GetInNotOperand()) {
            // VIPER #3949 : Operator 'not' cannot be applied to recursive property.
            // This is a recursive property and it is a part of operand of 'not'
            Error("not operator cannot be applied to recursive property") ;
        }
        return 1 ; // Recursion
    }

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    _is_processing = 1 ;
    VeriVarUsageInfo *local_info = (info) ? info->Copy() : 0 ;
    unsigned i, j ;
    VeriModuleItem *decl ;
    Array *ids ;
    VeriIdDef *id ;
    // VIPER #6450: If any local variable (formal local variable or declared in decl part)
    // contains initialization, consider that variable as assigned
    // First look at the formal local variables
    FOREACH_ARRAY_ITEM(_ports, i, id) {
        if (id && id->IsLocalVar() && id->GetInitialValue()) {
            id->VarUsage(local_info, BLOCKING_ASSIGNING, this) ;
        }
    }
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        ids = decl ? decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            if (id->GetInitialValue()) id->VarUsage(local_info, BLOCKING_ASSIGNING, decl) ;
        }
    }

    if (_spec) _spec->VarUsage(local_info, action) ;
    // Iterate over all local declarations and check whether they have proper flow
    // VIPER #6450 : Check flow for local formal arguments also
    FOREACH_ARRAY_ITEM(_ports, i, id) {
        if (!id || !id->IsLocalVar()) continue ;
        unsigned indx = (local_info) ? local_info->AddVariable(id): 0 ;
        // VIPER #3958 & 3960 : Produce error if local variables does not
        // flow properly i.e. it does not follow its assignment rule, but used some where.
        if (local_info && !local_info->GetFullyAssigned(indx) && local_info->GetEverUsed(indx)) id->Error("local variable %s referenced in expression where it does not flow", id->Name()) ;
    }
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        ids = decl ? decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            unsigned indx = (local_info) ? local_info->AddVariable(id): 0 ;
            // VIPER #3958 & 3960 : Produce error if local variables does not
            // flow properly i.e. it does not follow its assignment rule, but used some where.
            if (local_info && !local_info->GetFullyAssigned(indx) && local_info->GetEverUsed(indx)) id->Error("local variable %s referenced in expression where it does not flow", id->Name()) ;
        }
    }
    if (local_info && info) {
        // Merge this local info to coming one :
        Array info_arr(1) ;
        info_arr.InsertLast(local_info) ;
        info->Combine(info_arr) ;
    }
    delete local_info ;
    _is_processing = 0 ;

    _present_scope = save_scope ;

    return action ;
}
/*--------------------------------------------------------------------------*/

unsigned
VeriClockingSigDecl::VarUsage(VeriVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    int i;
    VeriIdDef *id;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

        // There is an assignment to the net only if it has an initial value :
        VeriExpression *rhs = id->GetInitialValue() ;
        if (rhs) {
            rhs->VarUsage(info, USING) ;
            //id->VarUsage(info, NONBLOCKING_ASSIGNING, 0) ;
 // It is not an assignment ?
        }
    }
    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned VeriCovergroup::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD ;

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    unsigned i ;
    VeriExpression *port_expr ; // Traverse the port list
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (port_expr) port_expr->VarUsage(info, PORT_CONNECTION) ;
    }
    if (_event) _event->VarUsage(info, USING) ; // Traverse coverage event

    VeriModuleItem *item ; // Traverse coverage spec or options
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) (void) item->VarUsage(info, NONE) ;
    }

    _present_scope = save_scope ;

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned VeriCoverageOption::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (_option) _option->VarUsage(info, action) ; // Traverse option

    if (_value) _value->VarUsage(info, USING) ; // Traverse the value
    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned VeriCoverageSpec::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // Traverse coverpoint list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_point_list, i, expr) {
        if (expr) expr->VarUsage(info, USING) ;
    }

    if (_iff_expr) _iff_expr->VarUsage(info, action) ; // Traverse iff expression

    // Traverse bin declarations
    VeriModuleItem *bin ;
    FOREACH_ARRAY_ITEM(_bins, i, bin) {
        if (bin) (void) bin->VarUsage(info, action) ;
    }

    _present_scope = save_scope ;

    return VALUE_IGNORED ;
}

/*--------------------------------------------------------------------------*/

unsigned VeriBinDecl::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    if (_id) _id->VarUsage(info, action, this) ; // Traverse bin identifier
    return VALUE_IGNORED ;
}

unsigned VeriSystemTaskEnable::VarUsage(VeriVarUsageInfo* info, unsigned action)
{
    //(void) info ;
    VeriScope *label_scope = GetScope() ;
    VeriScope *save_scope = _present_scope ;
    if (label_scope) _present_scope = label_scope ;

    switch (_function_type) {
    case VERI_SYS_CALL_ASSERTKILL :
    case VERI_SYS_CALL_ASSERTOFF :
    case VERI_SYS_CALL_ASSERTON :
    case VERI_SYS_CALL_DUMPVARS :
        {
        // According to secton 22.8 of P1800 LRM, when working with assert system calls,
        // the first argument  must be of  constant positive integer expression. Viper 5136
        VeriExpression *level = (_args && _args->Size() > 0) ? (VeriExpression*)_args->At(0) : 0 ;
        if (level) {
        int level_value = -1 ;
            if (IsStaticElab()) {
                VeriBaseValue *val = level->StaticEvaluateInternal(0,0,0, 0) ;
                VeriConst *const_val = val ? val->ToConstVal(level) : 0 ;
                delete val ;
                if (const_val && !const_val->HasXZ()) {
                    level_value = const_val->Integer() ;
                }
                if (level_value < 0) Error("first argument level of system task %s must be a positive integer", PrintToken(_function_type)) ;
                delete const_val ;
            }
        }
        // Viper 5171. Check whether the arguments(>0) are module or variable identifier
        if (_function_type == VERI_SYS_CALL_DUMPVARS)
        {
            unsigned i ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(_args, i, expr) {
                if (i == 0 || !expr) continue ;
                if (expr->IsConst()) {
                    expr->Error("illegal argument to dumpvars") ;
                }
            }
        }
        break ;
        }
    case VERI_SYS_CALL_FATAL :
    case VERI_SYS_CALL_STOP :
    case VERI_SYS_CALL_FINISH :
        {
            // VIPER #6428: Check for the first arguemnt of these tasks.
            // According to IEEE 1800-2009 LRM section 20.2/20.9 the first
            // argument should be either of 0, 1 or 2:
            // Only check this in static elab mode, RTL elab ignores these tasks:
            if (!IsStaticElab()) break ;
            VeriExpression *finish_expr = (_args && _args->Size()) ? (VeriExpression *)_args->GetFirst() : 0 ;
            VeriBaseValue *finish_val = (finish_expr) ? finish_expr->StaticEvaluateInternal(0, 0, 0, 0) : 0 ;
            // Cannot check if we cannot evaluate the argument, it may not have been specified:
            if (!finish_val) break ; // Or it may be a non-constant value too, bail out
            // VIPER #7089: Properly check for 0, 1, 2; simple GetIntegerValue() will return 0 for strings and real 0.2:
            // Try to convert to VeriInteger type, will return itself if it cannot be converted
            // or is already a VeriInteger, otherwise it returns a VeriInteger of same value:
            finish_val = finish_val->TryConvertToInteger() ; // 'finish_val' will be absorbed
            // Check the converted value type, only call GetIntegerValue() if it is a VeriInteger:
            int finish_number = (finish_val && (finish_val->Type()==INTEGER)) ? finish_val->GetIntegerValue() : -1 ;
            delete finish_val ;
            if ((0 > finish_number) || (finish_number > 2)) {
                Error("first argument of $%s is invalid, expecting 0, 1 or 2", PrintToken(_function_type)) ;
            }
        break ;
        }
    default :
        break ;
    }
    //case VERI_SYS_CALL_DISPLAY : //Viper 5165. Illegal selected name in argument of $display
        //{
        //unsigned i ;
        //VeriExpression *arg ;
        //FOREACH_ARRAY_ITEM(_args, i, arg) {
            //// added a new environment to ignore all ram
            //// inference decisions. Viper 5165, 4060
            //if (arg) arg->VarUsage(info, SYSTASKCALL_ARG) ;
        //}
        //break ;
        //}
    //default:
        //{
    //}
    // VIPER #5935 : Call VarUsage on arguments of all system task calls
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) {
        // added a new environment to ignore all ram
        // inference decisions. Viper 5165, 4060
        if (arg) arg->VarUsage(info, SYSTASKCALL_ARG) ;
    }
    _present_scope = save_scope ;
    return action ;
}
////////////////////////////////////////////////////////////////////
//      Utility Methods for VeriVarUsageInfo
////////////////////////////////////////////////////////////////////

VeriVarUsageInfo::~VeriVarUsageInfo() {
    // Do not delete "_indices" it is not owned by VeriVarUsageInfo!
    _indices = 0 ;
    _rand_randc_vars = 0 ;
    delete _recursion_checker ;
    delete _is_fully_assigned ;
    delete _is_ever_assigned ;
    delete _is_ever_used ;
}

/*---------------------------------------------*/

VeriVarUsageInfo *
VeriVarUsageInfo::Copy() {
    VeriVarUsageInfo *new_vui = new VeriVarUsageInfo(_in_initial_clause) ;
    new_vui->_indices = _indices ;
    if (_is_fully_assigned) new_vui->_is_fully_assigned = new BitArray(*_is_fully_assigned) ;
    if (_is_ever_assigned)  new_vui->_is_ever_assigned  = new BitArray(*_is_ever_assigned) ;
    if (_is_ever_used)      new_vui->_is_ever_used      = new BitArray(*_is_ever_used) ;
    new_vui->_num_edges = _num_edges ;    // copy the number of control edges :
    new_vui->_in_functon = _in_functon ;
    new_vui->_in_conditional_generate = _in_conditional_generate ;
    new_vui->_in_always_initial = _in_always_initial ;
    new_vui->_in_temporal_expr = _in_temporal_expr ;
    new_vui->_in_operand = _in_operand ;
    new_vui->_in_not_operand = _in_not_operand ;
    new_vui->_recursion_checker = 0 ;
    if (_recursion_checker) { // Viper #4060
        SetIter si ;
        VeriIdDef *task_id ;
        FOREACH_SET_ITEM(_recursion_checker, si, &task_id) new_vui->AddTaskIdInStack(task_id) ;
    }
    return new_vui ;
}

/*---------------------------------------------*/

unsigned
VeriVarUsageInfo::AddVariable(VeriIdDef *id)
{
    if (!_indices || !id) return 0 ;

    SynthesisInfo *this_info = 0 ;
    void *pair = id ;

    // Make sure the ID is in the ID table and it is initialized correctly
    unsigned indx = (unsigned)(unsigned long)_indices->GetValue(pair) ;
    if (!indx) {
        indx = _indices->Size() + 1 ;
        (void) _indices->Insert(pair, (void*)(unsigned long)indx) ;
        if (id->IsUsedBeforeAssigned(this_info) == 0) {
            // if the variable has not been previously set as UBA,
            // begin following the assignment state of this variable
            // by setting it to ABU.
            id->SetAssignedBeforeUsed(this_info) ;
        }
    }
    return indx;
}

/*---------------------------------------------*/
// Check the use/assign status of the variable based on
// the current action and the previously calculated
// assignment status. This calculation combines
// the concurrent use/assignement info stored
// on the variable/signal with the isEverAssigned and
// isEverUsed info derived from traversing a
// concurrent statement

void
VeriVarUsageInfo::UpdateConcurrentVarStatus() const
{
    // Only update the 'concurrent' used/assigned status if this was actually a 'concurrent' dataflow.
    // If it was an 'initial' dataflow, then the usage/assignments done here were not done 'concurrent',
    // and should thus be ignored.
    if (GetInInitialClause()) return ;

    MapIter i ;
    VeriIdDef *id ;
    void *pair ;
    unsigned long indx;
    FOREACH_MAP_ITEM(_indices, i, &pair, &indx) {
        if (!pair) continue ;

        SynthesisInfo *this_info = 0 ;
        id = (VeriIdDef *) pair ;
        if (!id) continue ;

        if (GetEverAssigned((unsigned)indx)) {
            if (id->IsConcurrentAssigned(this_info)) {
                // id was already concurrently assigned, so this is the second concurrent assignment.
                // That is two writes simulaniously, which cannot be done with a dual-port ram
                id->SetCannotBeDualPortRam(this_info) ;
            } else {
                id->SetConcurrentAssigned(this_info) ;
            }
        }
        if (GetEverUsed((unsigned)indx)) {
            if (id->IsConcurrentUsed(this_info)) {
                // id was already concurrently used, so this is the second concurrent usage.
                // That is two reads simulaniously, which cannot be done with a dual-port ram
                id->SetCannotBeDualPortRam(this_info) ;
            } else {
                id->SetConcurrentUsed(this_info) ;
            }
        }
    }
}

/*---------------------------------------------*/

void VeriVarUsageInfo::SetGlobalAssignmentInfo(const VeriModuleItem *context) const
{
    if (!VeriNode::IsStaticElab()) return ; // Only for static elaboration
    MapIter i ;
    VeriIdDef *id ;
    void *pair ;
    unsigned long indx;
    FOREACH_MAP_ITEM(_indices, i, &pair, &indx) {
        if (!pair) continue ;

        id = (VeriIdDef *) pair ;
        if (!id) continue;

        if (GetEverAssigned((unsigned)indx)) {
            if (GetFullyAssigned((unsigned)indx)) { // Full assignment
                SetAssignedInfoForVars(id, FULL_ASSIGNED, context) ; // VIPER #4241 : Set assignment information
            } else { // Partial assignment
                SetAssignedInfoForVars(id, PARTIAL_ASSIGNED, context) ; // VIPER #4241 : Set assignment information
            }
        }
    }
}
/*---------------------------------------------*/
void
VeriVarUsageInfo::Combine(Array& array_of_info, unsigned and_op) {
    Array fully_assigned(array_of_info.Size()) ;
    Array ever_assigned( array_of_info.Size()) ;
    Array ever_used(     array_of_info.Size()) ;

    unsigned i ;
    VeriVarUsageInfo *info;
    FOREACH_ARRAY_ITEM(&array_of_info, i, info) {
        if (!info || (info->GetInConditionalGenerate() && !info->GetInAlwaysInitial())) continue ; // VIPER #8278: Check for generate conditional along with in always/initial flag
        fully_assigned.Insert(info->_is_fully_assigned) ;
        ever_assigned.Insert( info->_is_ever_assigned) ;
        ever_used.Insert(     info->_is_ever_used) ;
    }

    if (!_is_fully_assigned) _is_fully_assigned = new BitArray() ;
    if (!_is_ever_assigned)  _is_ever_assigned  = new BitArray() ;
    if (!_is_ever_used)      _is_ever_used      = new BitArray() ;

    // Combined flow is fully assign only if all flows are fully assigned.
    if (and_op) {
        // VIPER #4903 : For and operator in assertion, if a local variable is
        // assigned in any branch, it will be considered assigned in upper branch
        BitArray *ba ;
        FOREACH_ARRAY_ITEM(&fully_assigned, i, ba) {
            if (ba) _is_fully_assigned->Or(*ba) ;
        }
    } else {
        _is_fully_assigned->ReduceAND(fully_assigned) ;
    }
    // Combined flow is ever assigned if at least one flow is ever assigned
    _is_ever_assigned->ReduceOR(  ever_assigned) ;
    // Combined flow is ever used if at least one flow is ever used
    _is_ever_used->ReduceOR(      ever_used) ;
}
/*---------------------------------------------*/
void
VeriVarUsageInfo::CombineForAssertionOp(unsigned oper_type, Array &info_arr)
{
    VeriVarUsageInfo *left_info = (info_arr.Size()) ? (VeriVarUsageInfo*)(info_arr.At(0)): 0 ;
    VeriVarUsageInfo *right_info = (info_arr.Size() > 1) ? (VeriVarUsageInfo*)(info_arr.At(1)): 0 ;

    Map *left_indices_map = (left_info) ? left_info->_indices : 0 ;
    Map *right_indices_map = (right_info) ? right_info->_indices : 0 ;

    // Check the rules of local variable assignment
    // If variable is assigned in one operand and used in other, it will be error
    // if variable is not assigned in other branch or outside this operator
    unsigned long index ;
    MapIter mi ;
    VeriIdDef *id ;
    void *pair ;
    FOREACH_MAP_ITEM(left_indices_map, mi, &pair, &index) {
        id = (VeriIdDef *) pair ;
        if (!id || !id->IsLocalVar() || !left_info) continue ;
        if (!left_info->GetFullyAssigned(index)) {
            // VIPER #5651 : Reset ever-used flag, as it has no contribution to
            // local variable flow for operators 'or' and 'and'
            if (right_info && right_info->GetEverUsed(index)) right_info->ResetEverUsed(index) ;
            continue ;
        }

        // 'id' is assigned in left operand
        // Now check whether it is assigned in right operand
        if (right_info && right_info->GetEverUsed(index) && !right_info->GetFullyAssigned(index) && !GetFullyAssigned(index)) {
            id->Error("local variable %s referenced in expression where it does not flow", id->Name()) ;
        }
        if (right_info && right_info->GetEverUsed(index)) right_info->ResetEverUsed(index) ;
    }
    FOREACH_MAP_ITEM(right_indices_map, mi, &pair, &index) {
        id = (VeriIdDef *) pair ;
        if (!id || !id->IsLocalVar() || !right_info) continue ;
        if (!right_info->GetFullyAssigned(index)) {
            // VIPER #5651 : Reset ever-used flag, as it has no contribution to
            // local variable flow for operators 'or' and 'and'
            if (left_info && left_info->GetEverUsed(index)) left_info->ResetEverUsed(index) ;
            continue ;
        }

        // 'id' is assigned in left operand
        // Now check whether it is assigned in right operand
        if (left_info && left_info->GetEverUsed(index) && !left_info->GetFullyAssigned(index) && !GetFullyAssigned(index)) {
            id->Error("local variable %s referenced in expression where it does not flow", id->Name()) ;
        }
        if (left_info && left_info->GetEverUsed(index)) left_info->ResetEverUsed(index) ;
    }

    // In case of 'and' and 'intersect', a local variable that flows out of at
    // least one operand shall flow out of composite sequence.
    // If case of 'or', a local variable that flows out of both operands shall
    // flow out of composite sequence.
    if ((oper_type == VERI_SEQ_OR) || (oper_type == VERI_PROPERTY_OR)) {
        Combine(info_arr) ;
    } else {
        Combine(info_arr, 1 /* and operator */) ;
    }
}

unsigned
VeriLetDecl::VarUsage(VeriVarUsageInfo *info, unsigned /*action*/)
{
    //if (!info) return VALUE_IGNORED ;

    if (_id) {
        if (_id->IsProcessing()) return VALUE_IGNORED ;
        _id->SetIsProcessing() ;
    }

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    // VIPER #6408: VarUsage using this 'USING' action to set the proper flags:
    if (_expr) _expr->VarUsage(info, USING) ;

    _present_scope = save_scope ;

    if (_id) _id->ClearIsProcessing() ;

    return VALUE_IGNORED ;
}

/*---------------------------------------------*/
void
VeriVarUsageInfo::SetFullyAssigned(unsigned indx) {
    if (!_is_fully_assigned) _is_fully_assigned = new BitArray() ;
    _is_fully_assigned->SetBit(indx) ;
}
void
VeriVarUsageInfo::SetEverAssigned (unsigned indx) {
    if (!_is_ever_assigned) _is_ever_assigned = new BitArray() ;
    _is_ever_assigned->SetBit(indx) ;
}
void
VeriVarUsageInfo::SetEverUsed     (unsigned indx) {
    if (!_is_ever_used) _is_ever_used = new BitArray() ;
    _is_ever_used->SetBit(indx) ;
}
void
VeriVarUsageInfo::ResetFullyAssigned(unsigned indx) {
    if (_is_fully_assigned) _is_fully_assigned->ClearBit(indx) ;
}
void
VeriVarUsageInfo::ResetEverUsed(unsigned indx) {
    if (_is_ever_used) _is_ever_used->ClearBit(indx) ;
}
/*---------------------------------------------*/

unsigned
VeriVarUsageInfo::GetFullyAssigned(unsigned indx) const {
    return (_is_fully_assigned) ? _is_fully_assigned->GetBit(indx) : 0 ;
}
unsigned
VeriVarUsageInfo::GetEverAssigned (unsigned indx) const {
    return (_is_ever_assigned) ? _is_ever_assigned->GetBit(indx) : 0 ;
}
unsigned
VeriVarUsageInfo::GetEverUsed     (unsigned indx) const {
    return (_is_ever_used) ? _is_ever_used->GetBit(indx) : 0 ;
}

unsigned
VeriVarUsageInfo::HasTaskIdInStack(VeriIdDef const *task_id) const { // Viper #4060
    if (!task_id || !_recursion_checker) return 0 ;
    return (_recursion_checker->GetItem(task_id)) ? 1 : 0 ;
}

void
VeriVarUsageInfo::AddTaskIdInStack(VeriIdDef const *task_id) { // Viper #4060
    if (!task_id) return ;
    if (!_recursion_checker) _recursion_checker = new Set(POINTER_HASH) ;
    (void) _recursion_checker->Insert(task_id) ;
}

void
VeriVarUsageInfo::RemoveTaskIdFromStack(VeriIdDef const *task_id) {  // Viper #4060
    if (!task_id) return ;
    if (!_recursion_checker) return ;
    (void) _recursion_checker->Remove(task_id) ;
}

unsigned
VeriVarUsageInfo::HasIdInStack(VeriIdDef const *id) const { // Viper #6081
    if (!id || !_recursion_checker) return 0 ;
    return (_recursion_checker->GetItem(id)) ? 1 : 0 ;
}

void
VeriVarUsageInfo::AddIdInStack(VeriIdDef const *id) { // Viper #6081
    if (!id) return ;
    if (!_recursion_checker) _recursion_checker = new Set(POINTER_HASH) ;
    (void) _recursion_checker->Insert(id) ;
}

void
VeriVarUsageInfo::RemoveIdFromStack(VeriIdDef const *id) {  // Viper #6081
    if (!id) return ;
    if (!_recursion_checker) return ;
    (void) _recursion_checker->Remove(id) ;
}

void VeriClass::PopulateToBeImplementedMethods(Map *methods, unsigned ignore_interface_classes) const
{
    if (!methods) return ;
    unsigned interface_class = GetQualifier(VERI_INTERFACE) ? 1 : 0 ;

    unsigned i ;
    VeriModuleItem *item ;
    if ((interface_class && !ignore_interface_classes) || (ignore_interface_classes)) {
        // 1. Need to consider pure virtual methods declared here
        // 2. Need to consider methods from extended interface classes
        FOREACH_ARRAY_ITEM(_items, i, item) {
            if (!item) continue ;
            if ((item->IsFunctionDecl() || item->IsTaskDecl()) && (item->GetQualifier(VERI_PURE) && item->GetQualifier(VERI_VIRTUAL))) {
                VeriIdDef *subprog_id = item->GetId() ;
                if (subprog_id) (void) methods->Insert(subprog_id->Name(), item, 0, 1 /* allow multiple definitions*/) ;
            }
        }
    }
    Array *interface_classes = _interface_classes ;
    if (interface_classes && interface_classes->Size() && !ignore_interface_classes) {
        // Consider methods from implements/extends(for interface class) clause specified classes
        VeriName *interface_class_ref ;
        FOREACH_ARRAY_ITEM(_interface_classes, i, interface_class_ref) {
            VeriIdDef *interface_class_id = interface_class_ref ? interface_class_ref->GetId(): 0 ;
            item = interface_class_id ? interface_class_id->GetModuleItem(): 0 ;
            if (item) item->PopulateToBeImplementedMethods(methods, ignore_interface_classes) ;
        }
    }
    if (_base_class_name) {
        VeriTypeInfo *base_type = _base_class_name->CheckType(0, NO_ENV) ;
        VeriIdDef *base_class = (base_type) ? base_type->GetNameId(): 0 ;
        item = (base_class && base_class->IsClass()) ? base_class->GetModuleItem(): 0 ;
        delete base_type ;
        // Base class is always non-interface class. So if base class has implements
        // clause, that are to be reeclared/implemented in base class. We need
        // to pick only pure virtual functions from base class.
        if (item) item->PopulateToBeImplementedMethods(methods, 1) ;
    }
}

void VeriClass::ValidateImplementedMethod(VeriModuleItem *subprog_decl, Map *methods, unsigned virtual_class, unsigned ignore_pure_virtual)
{
    if (!subprog_decl || !methods || !subprog_decl->GetQualifier(VERI_VIRTUAL)) return ; // consider virtual methods only
    VeriIdDef *subprog_id = subprog_decl->GetId() ;

    if (!subprog_id) return ;
    if (subprog_decl->GetQualifier(VERI_PURE)) {
        if (!virtual_class) subprog_decl->Error("pure virtual method %s can exist only in abstract class", subprog_id->Name()) ;
        if (ignore_pure_virtual) return ;
    }

    VeriModuleItem *prototype = 0 ;
    VeriIdDef *prototype_id = 0 ;
    MapItem *map_item = methods->GetItem(subprog_id->Name()) ;
    while (map_item) {
        prototype = (VeriModuleItem*)map_item->Value() ;
        prototype_id = prototype ? prototype->GetId(): 0 ;
        if (prototype_id && (prototype_id->IsFunction() != subprog_id->IsFunction())) {
            subprog_decl->Error("%s %s in subclass %s cannot override %s in superclass", subprog_id->IsFunction() ? "function": "task", subprog_id->Name(), _id ? _id->Name(): " ", prototype_id->IsFunction() ? "function": "task") ;
        }
        // Check the return type between prototype and full definition :
        //VeriDataType *proto_ret_type = prototype_id->GetDataType() ;
        VeriTypeInfo *p_type = prototype_id ? prototype_id->CreateType(0): 0 ;
        //VeriDataType *actual_ret_type = subprog_id->GetDataType() ;
        VeriTypeInfo *a_type = subprog_id->CreateType(0) ;
        if (p_type && !p_type->IsTypeParameterType() && !p_type->IsUnresolvedNameType() &&
            a_type && !a_type->IsTypeParameterType() && !a_type->IsUnresolvedNameType() &&
            !a_type->IsEquivalent(p_type, 0, 0, 0)) {
            subprog_id->Error("return type for virtual method %s in class %s is not type equivalent to the superclass method", prototype_id ? prototype_id->Name(): "", _id ? _id->Name(): "") ;
        }
        delete p_type ; delete a_type ;

        // Check the formals of prototype with body
        Array *proto_ports = prototype_id ? prototype_id->GetPorts(): 0 ;
        unsigned proto_port_count = proto_ports ? proto_ports->Size(): 0 ;
        Array *actual_ports = subprog_id->GetPorts() ;
        unsigned actual_port_count = actual_ports ? actual_ports->Size(): 0 ;

        if (actual_port_count != proto_port_count) {
            subprog_decl->Error("formal count of virtual method %s does not match prototype", subprog_id->Name()) ;
        } else {
            VeriIdDef *proto_port, *actual_port ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(actual_ports, i, actual_port) {
                proto_port = (proto_ports) ? (VeriIdDef*)proto_ports->At(i): 0 ;
                if (!actual_port || !proto_port) continue ;

                // Compare names : Produce warning here. Some simulators produce error and some warning
                if (!Strings::compare(actual_port->Name(), proto_port->Name())) {
                    actual_port->Warning("argument name %s for virtual method %s in subclass %s does not match the argument name %s in superclass", actual_port->Name(), subprog_id->Name(), _id ? _id->Name(): "", proto_port->Name()) ;
                }
                // Compare types :
                VeriTypeInfo *actual_port_type = actual_port->CreateType(0) ;
                VeriTypeInfo *proto_port_type = proto_port->CreateType(0) ;

                if (actual_port_type && !actual_port_type->IsUnresolvedNameType() && !actual_port_type->IsTypeParameterType() &&
                    proto_port_type && !proto_port_type->IsUnresolvedNameType() && !proto_port_type->IsTypeParameterType() &&
                    !proto_port_type->IsEquivalent(actual_port_type, 0, 0, 0)) {
                    actual_port->Error("type of argument %s for virtual method %s in subclass %s does not match the type of argument in superclass", actual_port->Name(), subprog_id->Name(), _id ? _id->Name(): "") ;
                }
                delete actual_port_type ; delete proto_port_type ;
            }
        }
        MapItem *prev_item = map_item ;
        map_item = methods->GetNextSameKeyItem(map_item) ; // Multiple prototypes from different interface class can exist
        (void) methods->Remove(prev_item) ;
    }
}

