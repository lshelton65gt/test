/*
 *
 * [ File Version : 1.83 - 2014/03/01 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriUtil_Stat.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "Strings.h"
#include "VeriExpression.h"
#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "VeriStatement.h"
#include "VeriId.h"
#include "VeriMisc.h"
#include "Array.h"
#include "veri_tokens.h"
#include "VeriScope.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Allow size of the result of the pow upto 16K
//#define MAX_ALLOWED_POW_SIZE (1<<14)

// this calculates 2**n
int ConstStrEvaluation::Pow2(int n)
{
    // If n is equal/greater than 32 then we will overflow,
    // also we can't continue for negative values, check it
    if (n >= 32 || n < 0) return 0 ;
    // Just left-shift 1 to n number of bits:
    return (1 << n) ;

    // Don't use this loop!
    /*
    int result = 1 ;
    for (int i = 0; i < x; i++) result = result*2 ;
    return result ;
    */
}

// Converts string to binary. It extracts the value from the string and returns a binary string
// of accurate size.  Third parameter represents context size.  The default value of this parameter is
// -1: context free. Otherwise it is context dependent. This is required for '1, 'b1 type values.
char *ConstStrEvaluation::VeriStringToBinary(char *instr, unsigned *p_signed, int context_size)
{
    int val_len = Strings::atoi(instr) ;
    if (val_len < 0) return 0 ; // Cannot proceed if the size value of the string is negative like -10 for "-10'b10101..."

    unsigned retlen = (unsigned)val_len ;
    if (!retlen) {
        // Default value of context_size is -1
        retlen = (context_size > 0) ? (unsigned)context_size : 32 ;
    }

    // Decide on signedness:
    char *firstcp = strpbrk(instr, "sS") ;
    unsigned val_sign = (firstcp) ? 1 : 0 ;

    // Convert to binary stream depending on base:
    char *binstr = 0 ;
    if (strpbrk(instr, "hH")) { // hex
        firstcp = strpbrk(instr, "hH") ;
        if (firstcp) firstcp++ ;
        binstr = HexToBin(firstcp) ;
    } else if (strpbrk(instr, "bB")) { // binary
        firstcp = strpbrk(instr, "bB") ;
        if (firstcp) firstcp++ ;
        binstr = BinToBin(firstcp) ;
    } else if (strpbrk(instr, "dD")) { // decimal
        firstcp = strpbrk(instr, "dD") ;
        if (firstcp) firstcp++ ;
        binstr = DecToBin(firstcp) ;
    } else if (strpbrk(instr, "oO")) { // octal
        firstcp = strpbrk(instr, "oO") ;
        if (firstcp) firstcp++ ;
        binstr = OctToBin(firstcp) ;
    } else { // It is probably of '1 format. 'b1 format is already handled.
        if (instr[0] == '\'') {
            switch(instr[1])
            {
            case '0':
            case '1':
            case 'x':
            case 'X':
            case 'z':
            case 'Z':
                if (instr[2] == '\0') {
                    binstr = instr + 1 ; // Skip the ' char
                    // Return padded string to the specified size. Treat as signed to extend with the same bit!
                    return GetPaddedString(binstr, retlen, 1 /* signed */) ;
                }
                break ;
            default:
                //printf("Cannot recognize data format.\n") ;
                break ;
            }
        }
        return 0 ;
    }

    VERIFIC_ASSERT(binstr) ;
    unsigned binlen = (unsigned)Strings::len(binstr) ;
    VERIFIC_ASSERT(binlen >= 1) ;

    // VIPER #7700: Looks like simulators truncate unsized literals to 32 bit only for decimal values
    // ie, 'd.... type numbers. For others, they keep the bit pattern as is. Since we already truncated
    // the decimal values to 32 bit in constructor of VeriConstVal, no need to do it here. Also, we
    // do not know whether the literal came from decimal or other base in this case...
    //// VIPER #4616: Check if this is an unsized literal of size more than 32 bits.
    //if (!val_len && (binlen >= 32)) {
    //    // Truncate the string to size 32 bit, extension will happen below, if required:
    //    if (binlen > 32) {
    //        char *tmp = binstr ;
    //        binstr = Strings::save(binstr+(binlen-32)) ;
    //        Strings::free(tmp) ;
    //    }
    //    // Make it behave like 32 bit value
    //    val_len = 32 ;
    //    binlen = 32 ;
    //}

    char *retstr = 0 ;
    switch (binstr[0]) {
    case 'x':
    case 'X':
    case 'z':
    case 'Z':
        if (retlen == binlen) {
            retstr = Strings::save(binstr) ;
        } else if (retlen < binlen) {
            retstr = GetPaddedString(binstr, retlen, 0) ;
        } else {
            retstr = Strings::allocate(retlen) ;
            unsigned i ;
            for (i = 0; i < retlen; i++) {
                if (i < (retlen - binlen)) {
                    retstr[i] = binstr[0] ;
                } else {
                    retstr[i] = binstr[i - (retlen - binlen)] ;
                }
            }
            retstr[retlen] = '\0' ;
        }
        break ;
    default:
        // VIPER #7910: Unsized non x/z numbers are at-least 32 bit in size and are unsigned extended:
        if (!val_len) val_len = 32 ;
        if (/*val_len &&*/ ((unsigned)val_len > binlen)) {
            // VIPER #4857: First, always unsigned extend upto the given size:
            retstr = GetPaddedString(binstr, (unsigned)val_len, 0 /* unsigned extend */) ;
            Strings::free(binstr) ;
            binstr = retstr ;
        }

        // VIPER #7700: If the sign was specified, always sign extend irrespective of unsized or sized literal:
        //// VIPER #4616: Then, signed/unsigned extend eccording to the value only if the size
        //// was explicitly specified. Always unsigned extend unsized literals (VIPER #4857):
        //retstr = GetPaddedString(binstr, retlen, ((val_len) ? val_sign : 0)) ;
        retstr = GetPaddedString(binstr, retlen, val_sign) ;
        break ;
    }

    Strings::free(binstr) ;

    if (p_signed) *p_signed = val_sign ;

    return retstr ;
}

// Converts from a string representing a binary number to another string. Output
// string is free from '_' if present, and any '?' is converted to 'z'.
char *ConstStrEvaluation::BinToBin(const char *instr)
{
    if (!instr) return 0 ;

    unsigned s_len = (unsigned)Strings::len(instr) ;
    unsigned i = 0 , j = 0 ;

    char *outstr = Strings::allocate(s_len) ;
    char c ;

    for (i = 0; i < s_len; i++) {
        c = instr[i] ;
        if (c == '?') {
            outstr[j++] = 'z' ;
            continue ;
        }
        if (c != '_') outstr[j++] = c ;
    }
    outstr[j] = '\0' ;
    return outstr ;
}

char *ConstStrEvaluation::DecToBin(const char *s)
{
    if (!s) return 0 ;

    unsigned i ;
    char x_z = 0 ;
    unsigned b_xz = 0 ;
    unsigned len = Strings::len(s) ;
    for (i = 0; i < len; i++) {
        if ((s[i] == 'x') || (s[i] == 'X') ||
            (s[i] == 'z') || (s[i] == 'Z')) {
            x_z = s[i] ;
            b_xz = 1 ;
            break ;
        }
    }

    char *ret_str = 0 ;
    if (b_xz) {
        ret_str = Strings::allocate(32) ;
        for (i = 0; i < 32; i++) ret_str[i] = x_z ;
        // VIPER #4726: Terminate the string here:
        ret_str[i] = '\0' ;
        return ret_str ;
    }

    // VIPER #4726: Calculate binary from arbitrary size decimal number:
    ret_str = Strings::allocate((len*4)+1) ; // Allocate enough for integer -> binary
    unsigned bit = 0 ;
    char sum ;
    char borrow ;
    char *digit ;
    char *number = Strings::save(s) ;
    char *tmp = number ;
    while (*number!='\0') {
        // Divide by 2:
        borrow = 0 ;
        digit = number ;
        while (*digit!='\0') {
             sum = (*digit - '0') ;     // Calculate value of digit
             if (borrow) sum += 10 ;    // Add the borrow (base 10)
             borrow = sum & 1 ;         // Update borrow : take the LSB bit
             *digit = (sum>>1) + '0' ;  // Update digit : divide by two.
             digit++ ;                  // Shift to next digit
        }
        // Prune off leading '0's:
        if (*number=='0') number++ ;

        // Set this bit if division created a borrow.
        ret_str[bit++] = (borrow) ? '1' : '0' ;
    }
    // If it is less than 32 bit, add a 0, so that it does not get 1 extension while sign extended
    if ((bit < 32) && (ret_str[bit-1] != '0')) ret_str[bit++] = '0' ;
    Strings::free(tmp) ;

    // Now, reverse the bits:
    tmp = ret_str ;
    ret_str = Strings::allocate(bit+1) ;
    for (i=0; i<bit; i++) ret_str[i] = tmp[(bit-1)-i] ;
    ret_str[bit] = '\0' ;
    Strings::free(tmp) ;

    return ret_str ;
}

char *ConstStrEvaluation::OctToBin(const char *s)
{
    if (!s) return 0 ;

    unsigned len = (unsigned)Strings::len(s) ;
    char *ret_ptr = Strings::allocate((len*3) + 1) ;
    ret_ptr[len*3] = '\0' ;

    unsigned i = 0, idx, j ;
    for (idx = 0; idx < len; idx++) {
        switch(s[idx]) {
        case '0' :
            for(j=0;j<3;j++) ret_ptr[3*i + j] = '0' ;
            i++ ;
            break ;
        case '1' :
            ret_ptr[3*i + 0] = '0' ;
            ret_ptr[3*i + 1] = '0' ;
            ret_ptr[3*i + 2] = '1' ;
            i++ ;
            break ;
        case '2' :
            ret_ptr[3*i + 0] = '0' ;
            ret_ptr[3*i + 1] = '1' ;
            ret_ptr[3*i + 2] = '0' ;
            i++ ;
            break ;
        case '3' :
            ret_ptr[3*i + 0] = '0' ;
            ret_ptr[3*i + 1] = '1' ;
            ret_ptr[3*i + 2] = '1' ;
            i++ ;
            break ;
        case '4' :
            ret_ptr[3*i + 0] = '1' ;
            ret_ptr[3*i + 1] = '0' ;
            ret_ptr[3*i + 2] = '0' ;
            i++ ;
            break ;
        case '5' :
            ret_ptr[3*i + 0] = '1' ;
            ret_ptr[3*i + 1] = '0' ;
            ret_ptr[3*i + 2] = '1' ;
            i++ ;
            break ;
        case '6' :
            ret_ptr[3*i + 0] = '1' ;
            ret_ptr[3*i + 1] = '1' ;
            ret_ptr[3*i + 2] = '0' ;
            i++ ;
            break ;
        case '7' :
            ret_ptr[3*i + 0] = '1' ;
            ret_ptr[3*i + 1] = '1' ;
            ret_ptr[3*i + 2] = '1' ;
            i++ ;
            break ;
        case 'Z' :
        case 'z' :
            ret_ptr[3*i + 0] = 'z' ;
            ret_ptr[3*i + 1] = 'z' ;
            ret_ptr[3*i + 2] = 'z' ;
            i++ ;
            break ;
        case '?' :
            ret_ptr[3*i + 0] = 'z' ;
            ret_ptr[3*i + 1] = 'z' ;
            ret_ptr[3*i + 2] = 'z' ;
            i++ ;
            break ;
        case '_' : /* ignore it */ break ;
        default  :
            ret_ptr[3*i + 0] = 'x' ;
            ret_ptr[3*i + 1] = 'x' ;
            ret_ptr[3*i + 2] = 'x' ;
            i++ ;
            break ;
        }
    }
    ret_ptr[3*i] = '\0' ; // Terminate the string
    return ret_ptr ;
}

char *ConstStrEvaluation::HexToBin(const char *s)
{
    if (!s) return 0 ;

    unsigned len = (unsigned)Strings::len(s) ;
    char *ret_ptr = Strings::allocate(((len*4)+1)) ;
    ret_ptr[len*4] = '\0' ;

    unsigned idx, i = 0, j ;
    for (idx = 0; idx < len; idx++) {
        switch(s[idx]) {
        case '0' :
            for(j=0;j<4;j++) ret_ptr[4*i + j] = '0' ;
            i++ ;
            break ;
        case '1' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case '2' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case '3' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case '4' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case '5' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case '6' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case '7' :
            ret_ptr[4*i + 0] = '0' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case '8' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case '9' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case 'A' :
        case 'a' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case 'B' :
        case 'b' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '0' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case 'C' :
        case 'c' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case 'D' :
        case 'd' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '0' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case 'E' :
        case 'e' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '0' ;
            i++ ;
            break ;
        case 'F' :
        case 'f' :
            ret_ptr[4*i + 0] = '1' ;
            ret_ptr[4*i + 1] = '1' ;
            ret_ptr[4*i + 2] = '1' ;
            ret_ptr[4*i + 3] = '1' ;
            i++ ;
            break ;
        case 'Z' :
        case 'z' :
            ret_ptr[4*i + 0] = 'z' ;
            ret_ptr[4*i + 1] = 'z' ;
            ret_ptr[4*i + 2] = 'z' ;
            ret_ptr[4*i + 3] = 'z' ;
            i++ ;
            break ;
        case '?' :
            ret_ptr[4*i + 0] = 'z' ;
            ret_ptr[4*i + 1] = 'z' ;
            ret_ptr[4*i + 2] = 'z' ;
            ret_ptr[4*i + 3] = 'z' ;
            i++ ;
            break ;
        case '_' :
            break ; // nothing to do
        default  :
            ret_ptr[4*i + 0] = 'x' ;
            ret_ptr[4*i + 1] = 'x' ;
            ret_ptr[4*i + 2] = 'x' ;
            ret_ptr[4*i + 3] = 'x' ;
            i++ ;
            break ;
        }
    }
    ret_ptr[4*i] = '\0' ; // Terminate the string
    return ret_ptr ;
}
// VIPER #7397 : Pass length of the string along with string as string can have
// '\0' inside
char *ConstStrEvaluation::AsciiToBin(const char *asciistr, unsigned length)
{
    if (!asciistr) return 0 ;

    unsigned len = length ? length : (unsigned)Strings::len(asciistr) ;
    unsigned i, j ;
    char *str = Strings::allocate(len*8) ;
    unsigned idx = 0 ;
    for (i = 0; i < len; i++) {
        char c = asciistr[i] ;
        if (c == '\\') {
            switch (asciistr[i+1]) {
            case '0':
                // VIPER #4546: Adjustments for \0 in strings - treat it as integer 0:
                c = 0 ;
                i++ ;
                break ;
            case 'n':
                // VIPER #5762: Adjustments for \n in strings - treat it as newline:
                c = '\n' ;
                i++ ;
                break ;
            default: break ;
            }
        }
        int x = (int)c ;
        char *binstr = IntToBin(x) ;
        if (!binstr) continue ;
        for (j = 24; j < 32; j++) str[idx++] = binstr[j] ;
        Strings::free(binstr) ;
    }
    str[idx] = '\0' ;
    return str ;
}

char *ConstStrEvaluation::GetPaddedString(const char *str, unsigned size, unsigned b_sign)
{
    if (!str) return 0 ;

    char *ret_ptr = Strings::allocate(size+1) ;
    unsigned s_len = (unsigned)Strings::len(str) ;
    int i ;

    ret_ptr[size] = 0 ;
    if (s_len >= size) {
        //index 0 is MSB. We must start from LSB
        for(i = 0; i < (int)size; i++) ret_ptr[(size-1)-(unsigned)i] = str[(s_len-1)-(unsigned)i] ;
    } else {
        unsigned n_prev_size = (unsigned)Strings::len(str) ;
         //scalar. zero padding.
        if (n_prev_size == 1) {
            ret_ptr[size-1] = str[0] ;
            for(i = (int)size - 2; i >= 0; i--) {
                //need to honour the sign here too
                ret_ptr[i] = (b_sign) ? str[0] : '0' ;
            }
        } else {
            for(i = (int)size - 1; i >= 0; i--) {
                if (n_prev_size == 0) {
                    ret_ptr[i] = b_sign ? str[0]: '0' ;
                } else {
                    ret_ptr[i] = str[n_prev_size - 1] ;
                    n_prev_size-- ;
                }
            }
        }
    }
    return ret_ptr ;
}

char *ConstStrEvaluation::StrAdd(const char *left, const char *right, char cin)
{
    if (!left || !right) return 0 ;

    unsigned n_left = (unsigned)Strings::len(left) ;
    unsigned n_right = (unsigned)Strings::len(right) ;
    if (n_left != n_right) return 0 ;

    int i ;
    unsigned s_len = n_right ;
    char *ret_ptr = Strings::allocate(s_len+2) ;
    ret_ptr[s_len+1] = '\0' ;
    char sum ;
    char carry = cin - '0' ;
    for(i = (int)s_len - 1; i >= 0; i--) {
        sum = left[i] + right[i] + carry - 2*(int('0')) ;
        ret_ptr[i+1] = '0' + sum%2 ;
        carry = sum/2 ;
    }
    ret_ptr[0] = '0' + carry ;
    char *str = GetPaddedString(ret_ptr, n_left, 1) ;
    Strings::free(ret_ptr) ;
    return str ;
}

char *ConstStrEvaluation::StrInvert(const char *str)
{
    unsigned nStr = (unsigned)Strings::len(str) ;
    if (!nStr) return 0 ;

    char *ret_ptr = Strings::save(str) ;

    for(unsigned i=0; i<nStr; i++) ret_ptr[i] = CharInvert(ret_ptr[i]) ;
    return ret_ptr ;
}

char *ConstStrEvaluation::StrIncr(const char *str)
{
    if (!str) return 0 ;

    unsigned s_len = (unsigned)Strings::len(str) ;
    if (!s_len) return 0 ;

    int i ;
    char *ret_ptr = Strings::allocate(s_len+2) ;
    ret_ptr[s_len+1] = '\0' ;
    for(i = (int)s_len - 1; i >= 0; i--) ret_ptr[i+1] = str[i] ;

    int break_flag = 0 ;
    for(i = (int)s_len; i > 0; i--) {
        if (ret_ptr[i]=='0') {
            ret_ptr[i] = '1' ;
            break_flag = 1 ;
            break ;
        } else {
            ret_ptr[i] = '0' ;
        }
    }

    ret_ptr[0] = break_flag ? ret_ptr[1]: '1' ;

    return ret_ptr ;
}

char *ConstStrEvaluation::Str2sComp(const char *str)
{
    char *t1 = ConstStrEvaluation::StrInvert(str) ;
    char *t2 = ConstStrEvaluation::StrIncr(t1) ;
    Strings::free(t1) ;
    // Incrementing the inverted string may have incremented the size also, we should not
    // return a 2's complemented string whose size is different from the given string:
    unsigned org_len = (unsigned)Strings::len(str) ;
    unsigned new_len = (unsigned)Strings::len(t2) ;
    if (org_len != new_len) {
        t1 = GetPaddedString(t2, org_len, 0) ;
        Strings::free(t2) ;
        t2 = t1 ;
    }
    return t2 ;
}

//left and right are signed bit strings. caution : passing unsigned nos may result wrong result.
char *ConstStrEvaluation::StrSub(const char *left, const char *right, char cin)
{
    if (!left || !right) return 0 ;

    unsigned n_left = (unsigned)Strings::len(left) ;
    unsigned n_right = (unsigned)Strings::len(right) ;
    if (n_left != n_right) return 0 ;

    char *intermediate = ConstStrEvaluation::StrInvert(right) ;
    char *to_be_added = ConstStrEvaluation::StrIncr(intermediate) ;
    char *new_right = GetPaddedString(to_be_added, n_left, 1) ;
    char *ret_ptr = ConstStrEvaluation::StrAdd(left, new_right, cin) ;

    Strings::free(new_right) ;
    Strings::free(to_be_added) ;
    Strings::free(intermediate) ;

    return ret_ptr ;
}

// Shifts a string. Unit left shift if left = 1, unit right shift if left = 0
// 'padd' gives the padding bit.
char *ConstStrEvaluation::StrShift(const char *str, int left, char padd)
{
    char *ret_str = 0 ;
    unsigned n_len = (unsigned)Strings::len(str) ;
    unsigned i ;

    if (left) { //left shift.
        ret_str = (padd == '0') ? Strings::save(str, "0"): Strings::save(str, "1") ;
    } else { //right shift.
        ret_str = Strings::save(str) ;
        for (i = 0; i < n_len-1; i++) ret_str[i+1] = str[i] ;
        ret_str[0] = padd ;
        ret_str[n_len] = '\0' ;
    }

    return ret_str ;
}

char *ConstStrEvaluation::StrMultiply(const char *left, const char *right)
{
    if (!left || !right) return 0 ;
    unsigned n_len_left = (unsigned)Strings::len(left) ;
    unsigned n_len_right = (unsigned)Strings::len(right) ;
    char *ret_ptr = 0 ;

    if (StrIsZero(left) || StrIsZero(right)) {
        ret_ptr = Strings::save("0") ;
        return ret_ptr ;
    }

    unsigned ret_len = n_len_left + n_len_right ;
    ret_ptr = Strings::allocate(ret_len+1) ;
    ret_ptr[ret_len] = '\0' ;

    int i ;
    for(i = 0; i < (int)ret_len; i++) ret_ptr[i] = '0' ;

    char *multp = 0 ;
    char *multiplier = 0 ;

    if (n_len_left >= n_len_right) {
        multp = Strings::save(left) ;
        multiplier = Strings::save(right) ;
    } else {
        multp = Strings::save(right) ;
        multiplier = Strings::save(left) ;
    }

    unsigned cnt = (unsigned)Strings::len(multiplier) ;

    for(i = (int)(cnt - 1); i >= 0; i--) {
        if (i < (int)(cnt - 1)) {
            char *temp = multp ;
            multp = ConstStrEvaluation::StrShift(multp, 1, '0') ;
            Strings::free(temp) ;
        }
        if (multiplier[i] == '1') {
            char *temp1 = multp ;
            multp = GetPaddedString(multp, ret_len, 0) ;
            Strings::free(temp1) ;
            char *temp2 = ret_ptr ;
            ret_ptr = ConstStrEvaluation::StrAdd(ret_ptr, multp, '0') ;
            Strings::free(temp2) ;
        }
    }
    Strings::free(multp) ;
    Strings::free(multiplier) ;

    return ret_ptr ;
}

char* ConstStrEvaluation::StrSignedMultiply(const char* left, const char* right)
{
    if (!left || !right) return 0 ;
    char *ret_ptr = 0 ;

    if (StrIsZero(left) || StrIsZero(right)) { // Return 0 if any operand is 0
        ret_ptr = Strings::save("0") ;
        return ret_ptr ;
    }
    char* lstr = 0;
    char* rstr = 0;
    int signCount = 0;

    if(left[0] == '1') { // If left operand is negative, 2s complement it
        lstr = Str2sComp(left);
        signCount++;
    } else {
        lstr = Strings::save(left);
    }
    if(right[0] == '1') { // If right operand is negative, 2s complement it
        rstr = Str2sComp(right);
        signCount++;
    } else {
        rstr = Strings::save(right);
    }
    ret_ptr = StrMultiply(lstr, rstr) ; // Multiply
    if(signCount == 1) { // Result should be negative, 2s complement it
        char* temp = ret_ptr;
        ret_ptr = Str2sComp(temp);
        Strings::free(temp);
    }
    Strings::free(lstr);
    Strings::free(rstr);
    return ret_ptr;
}
char *ConstStrEvaluation::StrExp(const char *l_str, const char *r_str, unsigned l_sign, unsigned r_sign)
{
    if (!l_str || !r_str) return 0 ; // Can't do anything without these

    // Check whether the operands are zero, we can return early if they are zero:
    unsigned left_zero = (StrIsZero(l_str)) ? 1 : 0 ;
    unsigned right_zero = (StrIsZero(r_str)) ? 1 : 0 ;

    // Here are some initial simple/quick checks:
    if (left_zero && right_zero) {                    // 0 ** 0          == inf
        // FIXME: This is 0**0 and is 'Inf'. But standard simulators return 1 for this
        // situation (probably they don't check left 0), So, we also return "1" here:
        // The same thing is below also, but it is here to document this behaviour.
        //return 0 ; // Return 0 to indicate that we failed to evaluate it
        return Strings::save("01") ; // Return "01" to indicate that it is not -1
    }
    if (left_zero) {
        // Check whether right operand is negative ( < 0):
        if (r_sign && (r_str[0] == '1')) {            // 0 ** -n (n > 0) == inf
            // FIXME: This is 0**-n and is 'Inf'. But standard simulators return 0 for this
            // situation (probably they don't check left 0), So, we also return "0" here:
            // The same thing is below also, but it is here to document this behaviour.
            //return 0 ; // Return 0 to indicate that we failed to evaluate it
            return Strings::save("0") ;
        }
        return Strings::save("0") ;                   // 0 ** n (n > 0)  == 0
    }
    // Return "01" to indicate that it is not -1
    if (right_zero) return Strings::save("01") ;      // n ** 0 (n != 0) == 1

    // Check whether the left operand is negative:
    char *left = 0 ;
    unsigned left_neg = 0 ;
    if (l_sign && (l_str[0] == '1')) {
        // Make it positive and set the flag:
        left = Str2sComp(l_str) ;
        left_neg = 1 ;
    } else {
        left = Strings::save(l_str) ;
    }

    // Check whether the right operand is negative:
    char *right = 0 ;
    unsigned right_neg = 0 ;
    if (r_sign && (r_str[0] == '1')) {
        // Make it positive and set the flag:
        right = Str2sComp(r_str) ;
        right_neg = 1 ;
    } else {
        right = Strings::save(r_str) ;
    }

    // Save the allocated strings for freeing:
    char *left_save = left ;
    char *right_save = right ;

    // Ignore leading zeros from the operands:
    while (left && (*left)) {
        if (*left == '0') left++ ;
        if (*left == '1') break ;
    }
    while (right && (*right)) {
        if (*right == '0') right++ ;
        if (*right == '1') break ;
    }

    // 'left' and 'right' right must be valid here otherwise we can't do anything:
    if (!(left && right && (*left) && (*right))) {
        Strings::free(left_save) ;
        Strings::free(right_save) ;
        return 0 ;
    }

    // Find the active size of the operands:
    unsigned left_len = (unsigned) Strings::len(left) ;
    unsigned right_len = (unsigned)Strings::len(right) ;

    // Another quick check: whether we can return "0" from here:
    if ((left_len > 1) && right_neg) {
        // We have already converted negative values into unsigned value and trancated
        // the leading zeros. So, length of left operand is > 1 means its absolute value
        // itself is > 1 and right operand is negative which is at-least -1. In this
        // situation the result will be "0" becasue: a**(-b) where |a|>1 and b>1 = 1/(a**b)
        // and absolute value of (a**b) is > 1 so finally it will be 0, don't compute it:
        Strings::free(left_save) ;
        Strings::free(right_save) ;
        return Strings::save("0") ;
    }

    // Calculate the size of the return value string:
    // Take one extra bit for sign otherwise we will treat 10 ** 1 = 10 as signed
    // if the context was signed which will create illegal value later:
    unsigned ret_len = left_len * ((unsigned)Pow2((int)right_len) - 1) + 1 ;

    // VIPER #3770 side effect: No need to enforce this check here:
#if 0
    // Allow size only upto MAX_ALLOWED_POW_SIZE:
    if (ret_len > MAX_ALLOWED_POW_SIZE) {
        // Return from here, so that we don't try to allocate a huge string:
        //Error("this operation requires too many bits") ;
        Strings::free(left_save) ;
        Strings::free(right_save) ;
        return 0 ;
    }
#endif

    // Allocate string of the required size:
    char *result = Strings::allocate(ret_len+1) ;
    // VIPER #3770 side effect: Instead check that we could allocate the memory
    if (!result) return 0 ; // Can't allocate memory!
    result[ret_len] = '\0' ;

    int i ;
    // Set the initial value to "1":
    for(i = 0; i < (int)(ret_len - 1); i++) result[i] = '0' ;
    result[ret_len - 1] = '1' ;

    char *cur_val = 0 ;
    char *temp = 0 ;
    // Here calculate the (left ** right) in string:
    for(i = (int)(right_len - 1); i >= 0; i--) {
        // Calculate the weight at this position:
        if (!cur_val) {
            cur_val = Strings::save(left) ;
        } else {
            temp = cur_val ;
            cur_val = StrMultiply(cur_val, cur_val) ;
            Strings::free(temp) ;
        }
        // If this position in the right operand is '1', multiply weight with result:
        if (right[i] == '1') {
            temp = result ;
            /* coverity[copy_paste_error] */
            result = StrMultiply(result, cur_val) ;
            Strings::free(temp) ;
        }
    }
    Strings::free(cur_val) ;

    // Need to adjust size and sign and other things of this result below:
    char *ret_ptr = result ;

    // Check whether the right operand is negative:
    if (right_neg) {
        // The result is: 1/result
        temp = ret_ptr ;
        ret_ptr = StrDivision("1", ret_ptr, 0 /* always unsigned */) ;
        Strings::free(temp) ;
    }

    // Adjust the size of the result here:
    temp = ret_ptr ;
    ret_ptr = GetPaddedString(ret_ptr, ret_len, 0 /* always unsigned */) ;
    Strings::free(temp) ;

    // Here we need to decide on the sign of the result:
    if (left_neg && (right[right_len-1]=='1')) { // left is negative and right operand is an odd number
        // We are doing like this (-a) ** (2n+1), a and n > 0, result will be negative:
        // Make the result 2's complement to interprete it as negative:
        temp = ret_ptr ;
        ret_ptr = Str2sComp(ret_ptr) ;
        Strings::free(temp) ;
    }

    // Clean up:
    Strings::free(left_save) ;
    Strings::free(right_save) ;

    return ret_ptr ; // Done
}

//Returns 1 if the string(binary) is all 0.
unsigned ConstStrEvaluation::StrIsZero(const char *str)
{
    if (!str) return 1 ;

    unsigned ret = 1 ;
    unsigned n_len = (unsigned)Strings::len(str) ;
    for(unsigned cnt = 0; cnt < n_len; cnt++) {
        // old if (str[cnt] == '1')
        if (str[cnt]!='0') {
            ret = 0 ;
            break ;
        }
    }
    return ret ;
}

char *ConstStrEvaluation::StrDivision(const char *lstr, const char *rstr, unsigned sign)
{
    if (!lstr || !rstr) return 0 ;
    char *ret_ptr = 0 ;
    unsigned llen = (unsigned)Strings::len(lstr) ;
    unsigned rlen = (unsigned)Strings::len(rstr) ;
    if (!llen || !rlen) return 0 ;
    unsigned retlen = (llen > rlen) ? llen : rlen ;

    if (StrIsZero(rstr)) {
        // IEEE 1364-2001 4.1.5 Arithmetic operators: If 'right' is zero, result is all x.
        // Return 'retlen' number/size of the x from here. Otherwise if the context
        // is unsigned we will extend it with 0s which will be incorrect.
        //ret_ptr = VeriGetXOfWidth(1) ;
        ret_ptr = VeriGetXOfWidth(retlen) ;
        return ret_ptr ;
    }
    if (StrIsZero(lstr)) {
        ret_ptr = GetPaddedString("0", retlen, 0) ;
        return ret_ptr ;
    }
    unsigned sign_count = 0 ; // Keeps track of how many of the operands are negative
    char *l_tmp = 0, *l_tmp_save = 0 ;
    char *r_tmp = 0, *r_tmp_save = 0 ;
    if (sign && lstr[0] == '1') { // Operand is negative, 2s complement it
        l_tmp = Str2sComp(lstr) ;
        sign_count++ ; // This was a negative operand
    } else {
        l_tmp = Strings::save(lstr) ;
    }
    if (sign && rstr[0] == '1') { // Operand is negative, 2s complement it
        r_tmp = Str2sComp(rstr) ;
        sign_count++ ; // This was a negative operand
    } else {
        r_tmp = Strings::save(rstr) ;
    }
    // Store the strings, we need to free them:
    l_tmp_save = l_tmp ;
    r_tmp_save = r_tmp ;

    // So, here, both the operands are unsigned, negative operands are 2's complemented.
    // Remove the leading 0s from both the operands, they do not effect the result:
    char *left = 0, *right = 0 ;
    if (l_tmp) {
        while(*l_tmp) {
            if (*l_tmp == '0') l_tmp++ ;
            if (*l_tmp == '1') break ;
        }
    }
    left = Strings::save(l_tmp) ;
    if (r_tmp) {
        while(*r_tmp) {
            if (*r_tmp == '0') r_tmp++ ;
            if (*r_tmp == '1') break ;
        }
    }
    right = Strings::save(r_tmp) ;
    // Clean up these strings:
    Strings::free(l_tmp_save) ;
    Strings::free(r_tmp_save) ;

    // Now, check whether the division will result in > 1 value (its an unsigned division here):
    unsigned n_len_left = (unsigned)Strings::len(left) ;
    unsigned n_len_right = (unsigned)Strings::len(right) ;
    unsigned valid = 1 ;
    char *temp = 0 ;
    if (n_len_left < n_len_right) {
        valid = 0 ; // The result will be less than 1
    } else {
        temp = GetPaddedString(right, n_len_left, 0) ;
        char *lt_str = StrLT(left, temp, 0) ;
        if (lt_str && (lt_str[0] == '1')) valid = 0 ; // Here also, it will be less than 1
        Strings::free(lt_str) ;
        Strings::free(temp) ;
    }
    if (!valid) {
        // Since it will result in < 1 value, we will return 0 from here:
        ret_ptr = GetPaddedString("0", retlen, 0) ;
        Strings::free(left) ;
        Strings::free(right) ;
        return ret_ptr ;
    }

    // Here we have to divide the 'left' by 'right'. We will do this:
    // 1. Find N for which (left > 2*N*right) and (left < 2*(N+1)*right), Essentially,
    //    it is the diff of left length and right length.
    // 2. Find right = 2*N*right for the above N, the result will be of size (N+1)
    // 3. Now we compare left with right, if left is >= right, the result at this bit pos is 1, 0 otherwise.
    //    After this left becomes (left-right) and right is right shifted on bit and continue to check
    //    untill left and right becomes empty.
    //
    // This works as follows:
    // Let left = 1110 (14 in decimal) and right = 11 (3 in decimal) so, we need to find 14/3
    // 1. Here, N = 2, becasue (14 > 2*2*3) and (14 < 2*(2+1)*3)
    // 2. Then right = 2*2*3 = 12 = 1100 (in binary)
    // 3. In the beginning the result is: 000
    //    since left (1110) >= (right) 1100, the result is now 100, and left now: 0010 and right = 110
    //    since left ( 010) <  (right) 110, the result is now 100, and left now: 010 and right = 11
    //    since left (  10) <  (right) 11, the result is now 100, and left now: 00 and right = 1
    //    since left (   0) <  (right) 1, the result is now 100, and left and right now empty and we stop
    // So, the final result it: 100 (4 in decimal)
    unsigned ret_len = ((n_len_left + 1) - n_len_right) ; // ret_len > 0 since valid == 1, otherwise we have returned!
    unsigned i ;
    for(i = 0; i < (ret_len - 1); i++) {
        char *temp1 = right ;
        right = ConstStrEvaluation::StrShift(temp1, 1, '0') ;
        Strings::free(temp1) ;
    }
    ret_ptr = Strings::allocate(ret_len+1) ;
    ret_ptr[ret_len] = '\0' ;
    for(i = 0; i < ret_len; i++) ret_ptr[i] = '0' ;

    for(i = 0; i < ret_len; i++) {
        char *compres = StrGTE(left, right, 0) ;
        if (compres && compres[0] == '1') {
            temp = left ;
            left = StrSub(left, right, '0') ;
            Strings::free(temp) ;
            ret_ptr[i] = '1' ;
        }
        temp = right ;
        right = ConstStrEvaluation::StrShift(right, 0, '0') ;
        Strings::free(temp) ;
        temp = left ;
        left = GetPaddedString(left, (unsigned)Strings::len(right), 0) ;
        Strings::free(temp) ;
        Strings::free(compres) ;
    }
    Strings::free(left) ;
    Strings::free(right) ;

    // VIPER #2626: Always extend the result as unsigned before 2's complementing it,
    // if required. Since for signed division also we have 2's complemented the negative
    // operand(s) and thus the result is now unsigned:
    temp = GetPaddedString(ret_ptr, retlen, 0 /* always unsigned padding */) ;
    Strings::free(ret_ptr) ;
    ret_ptr = temp ;

    if (sign_count == 1) {
        // This must be a negative result, so 2's complement it, because it is now
        // in unsigned domain (as we have 2's complemented the negative operands):
        temp = Str2sComp(ret_ptr) ;
        Strings::free(ret_ptr) ;
        ret_ptr = temp ;
    }

    return ret_ptr ;
}

char *ConstStrEvaluation::StrMod(const char *left, const char *right, unsigned sign)
{
    if (!left || !right) return 0 ;
    char *ret_ptr = 0 ;
    unsigned n_len_left = (unsigned)Strings::len(left) ;
    if (StrIsZero(right)) {
        // IEEE 1364-2001 4.1.5 Arithmetic operators: If 'right' is zero, result is all x:
        unsigned n_len_right = (unsigned)Strings::len(right) ;
        // Get the maximum length of the two operands, we need to return that many x:
        unsigned retlen = (n_len_left > n_len_right) ? n_len_left : n_len_right ;
        ret_ptr = VeriGetXOfWidth(retlen) ;
        return ret_ptr ;
    }
    char *divres = StrDivision(left, right, sign) ;
    char *mulres = sign ? StrSignedMultiply(right, divres) : StrMultiply(right, divres) ;
    Strings::free(divres) ;
    unsigned n_len_mult = (unsigned)Strings::len(mulres) ;

    if (n_len_left > n_len_mult) {
        char *temp = mulres ;
        mulres = GetPaddedString(mulres, n_len_left, sign) ;
        Strings::free(temp) ;
        ret_ptr = StrSub(left, mulres, '0') ;
    } else {
        char *temp1 = GetPaddedString(left, n_len_mult, sign) ;
        ret_ptr = StrSub(temp1, mulres, '0') ;
        Strings::free(temp1) ;
    }
    Strings::free(mulres) ;
    return ret_ptr ;
}

unsigned ConstStrEvaluation::StrIsReductOR(const char *str)
{
    if (!str) return 0 ;
    unsigned is_true = 0 ;
    for(int i = 0; i < (int)Strings::len(str); i++) {
        if (str[i] == '1') {
            is_true = 1 ;
            break ;
        }
    }
    return is_true ;
}

char *ConstStrEvaluation::StrReductOR(const char *str)
{
    if (!str) return 0 ;
    unsigned ret_x = 0 ;
    char *ret_str ;
    for(int i = 0; i < (int)Strings::len(str); i++) {
        if (str[i] == '0') continue ;
        if (str[i] == '1') {
            ret_str = Strings::save("1") ;
            return ret_str ;
        } else {
            ret_x = 1 ;
        }
    }
    ret_str = (ret_x) ? VeriGetXOfWidth(1) : Strings::save("0") ;
    return ret_str ;
}

char *ConstStrEvaluation::StrReductAND(const char *str)
{
    if (!str) return 0 ;
    unsigned i ;
    char *ret_str = 0 ;
    unsigned ret_x = 0 ;
    for (i = 0; i < (unsigned)Strings::len(str); i++) {
        if (str[i] == '1') continue ;
        if (str[i] == '0') {
            ret_str = Strings::save("0") ;
            return ret_str ;
        } else {
            ret_x = 1 ;
        }
    }
    ret_str = (ret_x) ? VeriGetXOfWidth(1) : Strings::save("1") ;
    return ret_str ;
}

char *ConstStrEvaluation::StrReductXOR(const char *str)
{
    if (!str) return 0 ;
    unsigned s_len = (unsigned)Strings::len(str) ;
    if (s_len == 1) return Strings::save(str) ;

    unsigned i ;
    char *ret_str = 0 ;
    for (i = 0; i < s_len; i++) {
        switch (str[i]) {
        case 'x':
        case 'X':
        case 'z':
        case 'Z':
            ret_str = VeriGetXOfWidth(1) ;
            return ret_str ;
        default:
            break ;
        }
    }

    char prev ;
    if (str[0] == str[1]) {
        prev = '0' ;
    } else {
        prev = '1' ;
    }
    for (i = 2; i < s_len; i++) {
        if (str[i] == prev) {
            prev = '0' ;
        } else {
            prev = '1' ;
        }
    }
    ret_str = Strings::allocate(2) ;
    ret_str[0] = prev ;
    ret_str[1] = '\0' ;
    return ret_str ;
}

char *ConstStrEvaluation::StrLogicalNOT(const char *str)
{
    // VIPER #2606: (!a) is (a==0) is actually (~(|a))
    char *reduced_str = StrReductOR(str) ; // Reduce using reduction OR
    char *ret_str = 0 ;
    // Must be of size exactly 1
    if (reduced_str && (Strings::len(reduced_str) == 1)) {
        ret_str = Strings::allocate(2) ;
        // Just invert the single character
        ret_str[0] = CharInvert(reduced_str[0]) ;
        ret_str[1] = '\0' ;
    }
    Strings::free(reduced_str) ;
    return ret_str ;
}

char *ConstStrEvaluation::StrLogicalOR(const char *left, const char *right)
{
    // (a || b) is actually ((|a) | (|b))
    char *reduced_left = StrReductOR(left) ;    // Reduce-OR left
    char *reduced_right = StrReductOR(right) ;  // Reduce-OR right
    char *ret_str = 0 ;
    // Both must be of size 1
    if (reduced_left && (Strings::len(reduced_left) == 1) &&
        reduced_right && (Strings::len(reduced_right) == 1)) {
        ret_str = Strings::allocate(2) ;
        // Bitwise OR the reduced-OR results
        ret_str[0] = CharOr(reduced_left[0], reduced_right[0]) ;
        ret_str[1] = '\0' ;
    }
    Strings::free(reduced_right) ;
    Strings::free(reduced_left) ;
    return ret_str ;
}

char *ConstStrEvaluation::StrLogicalAND(const char *left, const char *right)
{
    // (a && b) is actually ((|a) & (|b))
    char *reduced_left = StrReductOR(left) ;    // Reduce-OR left
    char *reduced_right = StrReductOR(right) ;  // Reduce-OR right
    char *ret_str = 0 ;
    // Both must be of size 1
    if (reduced_left && (Strings::len(reduced_left) == 1) &&
        reduced_right && (Strings::len(reduced_right) == 1)) {
        ret_str = Strings::allocate(2) ;
        // Bitwise AND the reduced-OR results
        ret_str[0] = CharAnd(reduced_left[0], reduced_right[0]) ;
        ret_str[1] = '\0' ;
    }
    Strings::free(reduced_right) ;
    Strings::free(reduced_left) ;
    return ret_str ;
}

char *ConstStrEvaluation::StrLT(const char *first_str, const char *second_str, unsigned sign)
{
    if (!first_str || !second_str) return 0 ;
    unsigned first_len = (unsigned)Strings::len(first_str) ;
    unsigned second_len = (unsigned)Strings::len(second_str) ;

    if (first_len != second_len) return 0 ;

    char *first_str_copy = Strings::save(first_str) ;
    char *second_str_copy = Strings::save(second_str) ;

    //reverse the sign bit.
    if (sign) {
        if (first_str_copy[0] == '1') {
            first_str_copy[0] = '0' ;
        } else {
            first_str_copy[0] = '1' ;
        }

        if (second_str_copy[0] == '1') {
            second_str_copy[0] = '0' ;
        } else {
            second_str_copy[0] = '1' ;
        }
    }

    char *ret_ptr = 0 ;
    unsigned result_flag = 0 ;
    unsigned s_len = first_len ;

    for (unsigned i = 0 ; i < s_len ; i++) {
        if (first_str_copy[i] > second_str_copy[i]) break ;
        if (first_str_copy[i] < second_str_copy[i]) {
            result_flag = 1 ;
            break ;
        }
    }

    ret_ptr = Strings::save((result_flag) ? "1" : "0") ;

    Strings::free(first_str_copy) ;
    Strings::free(second_str_copy) ;

    return ret_ptr ;
}

char *ConstStrEvaluation::StrGT(const char *first_str, const char *second_str, unsigned sign)
{
    return StrLT(second_str,first_str,sign) ;
}

char *ConstStrEvaluation::StrGTE(const char *first_str, const char *second_str, unsigned sign)
{
    char *ret_ptr = StrLT(first_str,second_str,sign) ;

    if (!ret_ptr) return 0 ; // FIXME: return Strings::save("0") ; ?

    ret_ptr[0] = CharInvert(ret_ptr[0]) ;

    return ret_ptr ;
}

char *ConstStrEvaluation::StrLTE(const char *first_str, const char *second_str, unsigned sign)
{
    char *ret_ptr = StrGT(first_str,second_str,sign) ;

    if (!ret_ptr) return 0 ; // FIXME: return Strings::save("0") ; ?

    ret_ptr[0] = CharInvert(ret_ptr[0]) ;

    return ret_ptr ;
}

char *ConstStrEvaluation::StrEQ(const char *first_str, const char *second_str, unsigned comp_type)
{
    if (!first_str || !second_str) return 0 ;
    unsigned first_len = (unsigned)Strings::len(first_str) ;
    unsigned second_len = (unsigned)Strings::len(second_str) ;

    if (first_len != second_len) return 0 ;

    char *ret_ptr = 0 ;
    unsigned i ;

    switch(comp_type) {
    case VERI_CASEEQ:
    {
        if (Strings::compare_nocase(first_str,second_str)) {
            ret_ptr = Strings::save("1") ;
        } else {
            ret_ptr = Strings::save("0") ;
        }
        break ;
    }
    case VERI_LOGEQ:
    {
        // IEEE 1364.1, section 4.1.8: For the logical equality and logical inequality operators
        // (== and !=), if, due to unknown or high-impedance bits in the operands, the relation is
        // ambiguous, then the result shall be a one bit unknown value (x).
        for (i = 0; i < first_len; i++) {
            // First try to match unambiguously (both equality and inequality):
            if (((first_str[i] == '0') && (second_str[i] == '1')) ||
                ((first_str[i] == '1') && (second_str[i] == '0'))) {
                // Here it is unambiguously different, return 0:
                return Strings::save("0") ;
            }
            if (((first_str[i] == '0') && (second_str[i] == '0')) ||
                ((first_str[i] == '1') && (second_str[i] == '1'))) {
                // Here it is unambiguously equal till this bit pos, check next bits:
                continue ;
            }

            // Here it is ambiguous, the result is a single bit 'x':
            return VeriGetXOfWidth(1) ;
        }
        // So, both of the operands are equal and does not have anything other than 0 or 1:
        ret_ptr = Strings::save("1") ;
        break ;
    }
    case VERI_WILDEQUALITY:
    {
        // IEEE P1800, section 8.5: The wild equality (==?) and inequality (!=?) operators treat
        // X and Z values in a given bit position of their right operand as a wildcard. X and Z
        // values in the left operand are not treated as wildcard. A wildcard bit matches any bit
        // value (0, 1, X, Z) in the corresponding bit of the left operand being compared against
        // it. Any other bits are compared as for the logical equality and logical inequality
        // operators.
        for (i = 0; i < first_len; i++) {
            // Check right side of this position for x/z, it will be don't care then:
            switch (second_str[i]) {
            case 'x':
            case 'X':
            case 'z':
            case 'Z': // This is an intentional fall through: X or Z matches to anything, so skip this bit!
                continue ;
            default:
                switch (first_str[i]) {
                case 'x':
                case 'X':
                case 'z':
                case 'Z': // This is an intentional fall through: X or Z in left side, result is 1 bit 'x'
                    return VeriGetXOfWidth(1) ;
                default:
                    // So, this bit is not X or Z in both the strings. Check this bit whether they are equal or in-equal.
                    if (first_str[i] != second_str[i]) {
                        // This bit does not match, we need not check it further. Set the return value to '0' for not-equal.
                        return Strings::save("0") ;
                    }
                }
            }
        }
        // The two strings are wild-equal, return 1
        ret_ptr = Strings::save("1") ;
        break ;
    }
    default:
        break ;
    }

    return ret_ptr ;
}

char *ConstStrEvaluation::StrNEQ(const char *first_str, const char *second_str, unsigned comp_type)
{
    if (!first_str || !second_str) return 0 ;
    //Strings may be of different size at this point. e.g. for a case if (J != 15) where J = 32 bit.
    unsigned size1 = (unsigned)Strings::len(first_str) ;
    unsigned size2 = (unsigned)Strings::len(second_str) ;
    unsigned i = 0 ;
    char *new_str = 0 ;
    char *tmp_first = 0 ;
    char *tmp_second = 0 ;
    if (size1 > size2) {
        new_str = Strings::allocate(size1) ;
        for(i = 0; i < size1 - size2; i ++) new_str[i] = second_str[0] ;
        new_str[size1 - size2] = '\0' ;
        //char *tmp = Strings::save(new_str, second_str) ;
        tmp_second = Strings::save(new_str, second_str) ;
        //second_str = tmp ;
    } else if (size2 > size1) {
        new_str = Strings::allocate(size2) ;
        for(i = 0; i < size2 - size1; i ++) new_str[i] = first_str[0] ;
        new_str[size2 - size1] = '\0' ;
        //char *tmp = Strings::save(new_str, first_str) ;
        tmp_first = Strings::save(new_str, first_str) ;
        //first_str = tmp ;
    }

    unsigned equal_comp_type = 0 ;
    // The given comp_type is of NOT-EQUAL type. We are going to call the StrEQ() function, so find the equivalent EQUAL token.
    switch(comp_type) {
    case VERI_LOGNEQ:         equal_comp_type = VERI_LOGEQ ;        break ;
    case VERI_CASENEQ:        equal_comp_type = VERI_CASEEQ ;       break ;
    case VERI_WILDINEQUALITY: equal_comp_type = VERI_WILDEQUALITY ; break ;
    default:
        break ;
    }

    char *temp = 0 ;
    //char *temp = StrEQ(first_str, second_str, equal_comp_type) ;
    if (tmp_first) temp = StrEQ(tmp_first, second_str, equal_comp_type) ;
    else if (tmp_second) temp = StrEQ(first_str, tmp_second, equal_comp_type) ;
    else temp = StrEQ(first_str, second_str, equal_comp_type) ;

    Strings::free(tmp_first) ;
    Strings::free(tmp_second) ;
    Strings::free(new_str) ;

    if (!temp) return 0 ;  // FIXME: return Strings::save("0") ; ?

    char *ret_ptr = 0 ;
    switch (temp[0]) {
    case '1':
        ret_ptr = Strings::save("0") ;
        break ;
    case '0':
        ret_ptr = Strings::save("1") ;
        break ;
    case 'x':
    case 'X':
        ret_ptr = VeriGetXOfWidth(1) ;
        break ;
    default:
        break ;
    }

    Strings::free(temp) ;
    return ret_ptr ;
}

char *ConstStrEvaluation::IntToBin(int num)
{
    char *str = Strings::allocate(8) ;

    sprintf(str, "%s", "00000000") ;
    sprintf(str, "%x", num) ;
    char *temp = HexToBin(str) ;
    Strings::free(str) ;
    char *ret_str = GetPaddedString(temp, 32, 0) ;
    Strings::free(temp) ;
    VERIFIC_ASSERT(Strings::len(ret_str) == 32) ;
    return ret_str ;
}

char *ConstStrEvaluation::BinToAscii(const char *str)
{
    if (!str || StrHasXZ(str)) return 0 ;

    // Binary number is being converted to ASCII string. Standard simulators convert
    // them right-aligned. That mens, the binary digits in block of size 8 from the
    // right side is picked up and converted to character. So, we should do the same.
    // Allocate enough space (two for every block of 8 for we store \0 as '\' and '0').
    unsigned len = Strings::len(str) ;
    char *ascii = Strings::allocate(((len*2)/8)+1) ;

    // The number of remaining characters in case the binary string is not multiple of 8:
    unsigned size = len % 8 ;
    if (!size) size = 8 ; // If it is multiple, set size to be 8

    int n ;
    char s[9] ;
    unsigned i = 0 ;
    unsigned s_idx = 0 ;
    while (len > s_idx) {
        // Copy the first block (remaining of multiple of 8) from the beginning:
        (void) strncpy(s, str+s_idx, size) ; s[size] = '\0' ;
        n = BinToInt(s) ; // Convert it to integer (character)
        // Store it in the result by checking valid character range:
        if ((n < 0) || (n > 255)) n = ' ' ;
        // For '0', store "\0" (two characters '\' and '0'):
        if (!n) { ascii[i++] = '\\' ; n = '0' ; }
        ascii[i++] = (char)n ;
        s_idx = s_idx + size ; // Update the position for the next position
        size = 8 ; // Size should be 8 for subsequent iterations
    }
    ascii[i] = '\0' ;
    return ascii ;
}

int ConstStrEvaluation::BinToInt(const char *str, unsigned sign)
{
    if (!str) return 0 ;
    char *c_str = (sign && str[0] == '1') ? Str2sComp(str) : Strings::save(str) ;
    if (!c_str) return 0 ;
    unsigned length = (unsigned)Strings::len(c_str) ;
    int i = 0 ;
    int val = -1 ;
    int index = 0 ;
    int ret_val = 0 ;

    for(i = ((int)length - 1); i >= 0; i--) {
        val = (c_str[i] == '0') ? 0 : 1 ;
        ret_val += val*Pow2(index) ;
        index++ ;
    }
    Strings::free(c_str) ;
    return (sign && str[0] == '1') ? -ret_val: ret_val ;
}

char ConstStrEvaluation::CharAnd(char a, char b)
{
    // VIPER #2578: Follow this table to in evaluating bitwise binary AND operator
    //  &  |  0  1  x  z
    // ----|-------------
    //  0  |  0  0  0  0
    //  1  |  0  1  x  x
    //  x  |  0  x  x  x
    //  z  |  0  x  x  x

    if (('0'==a) || ('0'==b)) return '0' ;
    if (('1'==a) && ('1'==b)) return '1' ;
    return 'x' ;
}

char ConstStrEvaluation::CharOr(char a, char b)
{
    // VIPER #2578: Follow this table to in evaluating bitwise binary OR operator
    //  |  |  0  1  x  z
    // ----|-------------
    //  0  |  0  1  x  x
    //  1  |  1  1  1  1
    //  x  |  x  1  x  x
    //  z  |  x  1  x  x

    if (('0'==a) && ('0'==b)) return '0' ;
    if (('1'==a) || ('1'==b)) return '1' ;
    return 'x' ;
}

char ConstStrEvaluation::CharInvert(char a)
{
    switch (a) {
    case '1': return '0' ;
    case '0': return '1' ;
    default : return 'x' ;
    }
}

char ConstStrEvaluation::CharXor(char a, char b)
{
    // VIPER #2578: Follow this table to in evaluating bitwise binary XOR operator
    //  ^  |  0  1  x  z
    // ----|-------------
    //  0  |  0  1  x  x
    //  1  |  1  0  x  x
    //  x  |  x  x  x  x
    //  z  |  x  x  x  x

    switch (a)
    {
    case '0':
        if ('0'==b) return '0' ;
        if ('1'==b) return '1' ;
        break ;
    case '1':
        if ('0'==b) return '1' ;
        if ('1'==b) return '0' ;
        break ;
    default: break ;
    }

    return 'x' ;
}

char *ConstStrEvaluation::StrBitAnd(const char *str1, const char *str2)
{
    if (!str1 || !str2) return 0 ;

    unsigned len1 = (unsigned)Strings::len(str1) ;
    unsigned len2 = (unsigned)Strings::len(str2) ;
    VERIFIC_ASSERT(len1 == len2) ;
    (void) len2 ; // To prevent "unused variable" warning

    unsigned i ;
    char *ret_str = Strings::allocate(len1+1) ;
    ret_str[len1] = '\0' ;

    for(i = 0; i < len1; i++) ret_str[i] = CharAnd(str1[i], str2[i]) ;
    return ret_str ;
}

char *ConstStrEvaluation::StrBitOr(const char *str1, const char *str2)
{
    if (!str1 || !str2) return 0 ;

    unsigned len1 = (unsigned)Strings::len(str1) ;
    unsigned len2 = (unsigned)Strings::len(str2) ;
    VERIFIC_ASSERT(len1 == len2) ;
    (void) len2 ; // To prevent "unused variable" warning

    unsigned i ;
    char *ret_str = Strings::allocate(len1+1) ;
    ret_str[len1] = '\0' ;

    for(i = 0; i < len1; i++) ret_str[i] = CharOr(str1[i], str2[i]) ;
    return ret_str ;
}

char *ConstStrEvaluation::StrBitXor(const char *str1, const char *str2)
{
    if (!str1 || !str2) return 0 ;

    unsigned len1 = (unsigned)Strings::len(str1) ;
    unsigned len2 = (unsigned)Strings::len(str2) ;
    VERIFIC_ASSERT(len1 == len2) ;
    (void) len2 ; // To prevent "unused variable" warning

    unsigned i ;
    char *ret_str = Strings::allocate(len1+1) ;
    ret_str[len1] = '\0' ;

    for(i = 0; i < len1; i++) ret_str[i] = CharXor(str1[i], str2[i]) ;
    return ret_str ;
}

char *ConstStrEvaluation::StrBitXnor(const char *str1, const char *str2)
{
    if (!str1 || !str2) return 0 ;

    unsigned len1 = (unsigned)Strings::len(str1) ;
    unsigned len2 = (unsigned)Strings::len(str2) ;
    VERIFIC_ASSERT(len1 == len2) ;
    (void) len2 ; // To prevent "unused variable" warning

    unsigned i ;
    char *ret_str = Strings::allocate(len1+1) ;
    ret_str[len1] = '\0' ;
    char temp ;

    for(i = 0; i < len1; i++) {
        temp = CharXor(str1[i], str2[i]) ;
        ret_str[i] = CharInvert(temp) ;
    }
    return ret_str ;
}

// Shifts a string. If the field 'left' is 1, 'str1' is shifted left by 'shiftby'  unit,
// otherwise it is shifted right by one unit. 'padd' is the character by which it should be padded.
char *ConstStrEvaluation::StrShift(const char *str1, int left, unsigned shiftby, char padd)
{
    if (!str1) return 0 ;

    unsigned len = (unsigned)Strings::len(str1) ;
    char *ret_str = 0 ;
    unsigned i ;

    ret_str = Strings::allocate(len) ;
    if (left) {
        for(i = 0; i < len; i++) {
            if ((i < (len - shiftby)) && (len >= shiftby)) {
                ret_str[i] = str1[i + shiftby] ;
            } else {
                ret_str[i] = '0' ;
            }
        }
        ret_str[len] = '\0' ;
    } else {
        for(i = 0; i < len; i++) {
            if (i < shiftby) {
                ret_str[i] = padd ;
            } else {
                ret_str[i] = str1[i - shiftby] ;
            }
        }
        ret_str[len] = '\0' ;
    }
    return ret_str ;
}

unsigned ConstStrEvaluation::StrHasXZ(const char *str)
{
    if (!str) return 0 ;

    unsigned len = (unsigned)Strings::len(str) ;
    unsigned i ;
    for (i = 0; i < len; i++) {
        switch(str[i]) {
        case '1':
        case '0': break ;
        default: return 1 ;
        }
    }
    return 0 ;
}
unsigned ConstStrEvaluation::StrHasOnlyZ(const char *str)
{
    if (!str) return 0 ;

    unsigned len = (unsigned)Strings::len(str) ;
    unsigned i ;
    unsigned has_z = 0 ;
    for (i = 0; i < len; i++) {
        switch(str[i]) {
        case 'z':
        case 'Z': has_z = 1 ; break ;
        case 'x' :
        case 'X' : return 0 ;
        default: break ;
        }
    }
    return has_z ;
}

char *ConstStrEvaluation::VeriGetXOfWidth(unsigned size)
{
    char *ret_str = Strings::allocate(size + 1) ;
    unsigned i ;
    for(i = 0; i < size; i++) ret_str[i] = 'x' ;
    ret_str[i] = '\0' ;
    return ret_str ;
}

/* static */ char *
ConstStrEvaluation::StrConditionalCombine(const char *str1, const char *str2)
{
    if (!str1 || !str2) return 0 ;

    unsigned len1 = (unsigned)Strings::len(str1) ;
    unsigned len2 = (unsigned)Strings::len(str2) ;
    VERIFIC_ASSERT(len1 == len2) ;
    (void) len2 ; // To prevent "unused variable" warning

    // Ambiguous condition results for conditional operator:
    //  ?: |  0  1  x  z
    // ----|------------
    //  0  |  0  x  x  x
    //  1  |  x  1  x  x
    //  x  |  x  x  x  x
    //  z  |  x  x  x  x

    char *ret_str = Strings::allocate(len1) ;
    unsigned i ;
    for(i=0; i<len1; i++) {
        ret_str[i] = 'x' ;
        if ((str1[i] == '0') && (str2[i] == '0')) {
            ret_str[i] = '0' ;
        } else if ((str1[i] == '1') && (str2[i] == '1')) {
            ret_str[i] = '1' ;
        }
    }
    ret_str[i] = '\0' ;
    return ret_str ;
}

/* static */ verific_uint64
ConstStrEvaluation::BinToUnsigned64(const char *bin_str)
{
    verific_uint64 value = 0 ; // Start with value 0

    // Calculate the length of the binary string:
    unsigned length = (unsigned)Strings::len(bin_str) ;

    verific_uint64 this_bit ;
    for (int i = (int)length-1; i>=0; i--) {
        // Check if this bit contributes to the value:
        if (bin_str[i] != '1') continue ;
        // Calculate the weighted value of this bit:
        this_bit = ((verific_uint64)1) << (((int)length-i)-1) ;
        // Add the result to the value:
        value = value | this_bit ;
    }

    return value ;
}

/* static */ double
ConstStrEvaluation::BinToUnsignedDouble(const char *str, unsigned sign)
{
    double value = 0.0 ; // Start with value 0
    if (!str) return value ;
    char *bin_str = (sign && str[0] == '1') ? Str2sComp(str) : Strings::save(str) ;
    if (!bin_str) return value ;

    // Calculate the length of the binary string:
    unsigned length = (unsigned)Strings::len(bin_str) ;

    double this_bit = 1.0 ;
    for (int i = (int)length-1; i>=0; i--) {
        // Previously for binary to double conversion we used
        // (double)BinToUnsigned64()
        // But VC++ 6.0 cannot convert unsigned __int64(verific_uint64) to double
        // So this routine is added. But compiler does not support following
        // commented line also.
        //this_bit = ((double)1) << (((int)length-i)-1) ;
        // So weighted value of this bit is calculated here in 'this_bit'
        // and that is added to result only when value of this bit is '1'
        // Check if this bit contributes to the value:
        if (bin_str[i] == '1') {
            // Add the result to the value:
            value = value + this_bit ;
        }
        // Calculate the weighted value of this bit:
        this_bit = this_bit * 2.0 ;
    }
    Strings::free(bin_str) ;
    return (sign && str[0] == '1') ? -value : value ;
}

/* static */ char *
ConstStrEvaluation::Unsigned64ToBin(verific_uint64 value)
{
    // Allocate string for 64 bit unsigned number:
    char *bin_str = Strings::allocate(64) ;
    bin_str[64] = '\0' ;

    verific_uint64 this_bit ;
    for (unsigned i=0; i<64; i++) {
        // Check whether this bit is ON in the value or not:
        this_bit = ((verific_uint64)1) << i ;
        // If it is ON, put 1 in the string, otherwise put 0:
        bin_str[(64-i)-1] = (value & this_bit) ? '1' : '0' ;
    }

    return bin_str ;
}

/* static */ verific_int64
ConstStrEvaluation::BinToInt64(const char *bin_str, unsigned sign /* = 0 */)
{
    verific_int64 result = 0 ; // Start with value 0
    if (!bin_str) return result ;

    char *c_str = (sign && (bin_str[0] == '1')) ? Str2sComp(bin_str) : Strings::save(bin_str) ;
    if (!c_str) return result ;

    unsigned length = (unsigned)Strings::len(c_str) ;
    if (length >= 64) {
        length = 64 ; // Do not try to handle more than 64 bit binary
        // VIPER #7837: Reduce the size of coming string to 64 bit, take last 64 bits
        char *tmp = c_str ;
        c_str = GetPaddedString(tmp, length, sign) ;
        Strings::free(tmp) ;
    }
    verific_int64 this_bit ;
    for (int i = (int)(length - 1); i >= 0; i--) {
        // Check if this bit contributes to the result:
        if (c_str[i] != '1') continue ;
        // Calculate the weighted value of this bit:
        this_bit = ((verific_int64)1) << (((int)length-i)-1) ;
        // Add (bit-wise OR) the value to the result:
        result = result | this_bit ;
    }
    Strings::free(c_str) ;
    return (sign && (bin_str[0] == '1')) ? -result : result ;
}

/* static */ char *
ConstStrEvaluation::Int64ToBin(verific_int64 value)
{
    // Allocate string for 64 bit unsigned number:
    char *bin_str = Strings::allocate(64) ;
    bin_str[64] = '\0' ;

    unsigned negative_val = 0 ;
    if (value < 0) {
        // Make negative value positive after setting a flag:
        negative_val = 1 ;
        value = -value ;
    }

    verific_uint64 this_bit ;
    for (unsigned i=0; i<64; i++) {
        // Check whether this bit is ON in the value or not:
        this_bit = ((verific_uint64)1) << i ;
        // If it is ON, put 1 in the string, otherwise put 0:
        bin_str[(64-i)-1] = ((verific_uint64)value & this_bit) ? '1' : '0' ;
    }

    if (negative_val) {
        // Adjustment for negative values:
        char *neg = Str2sComp(bin_str) ;
        Strings::free(bin_str) ;
        bin_str = neg ;
    }
    return bin_str ;
}

// VIPER #3076: Short Real -> Bits and Bits -> Short Real conversion:
typedef union {
    float    float_value ;
    unsigned bit_pattern ;
} verific_float ;

/* static */ char *
ConstStrEvaluation::ShortRealToBits(float value)
{
    verific_float f ;
    f.float_value = value ;

    return IntToBin((int)f.bit_pattern) ;
}

/* static */ float
ConstStrEvaluation::BitsToShortReal(const char *value)
{
    unsigned len = Strings::len(value) ;
    if (!value || !len) return 0.0 ;

    verific_float f ;
    f.bit_pattern = (unsigned)BinToInt(value) ;

    return f.float_value ;
}

// VIPER #3076: Real -> Bits and Bits -> Real conversion:
typedef union {
    double         double_value ;
    verific_uint64 bit_pattern ;
} verific_double ;

/* static */ char *
ConstStrEvaluation::RealToBits(double value)
{
    verific_double d ;
    d.double_value = value ;

    return Unsigned64ToBin(d.bit_pattern) ;
}

/* static */ double
ConstStrEvaluation::BitsToReal(const char *value)
{
    unsigned len = Strings::len(value) ;
    if (!value || !len) return 0.0 ;

    verific_double f ;
    f.bit_pattern = BinToUnsigned64(value) ;

    return f.double_value ;
}

// VIPER #6548: Support for case/casex/casez evaluation (comparison):
/* static */ unsigned
ConstStrEvaluation::StrCaseEQ(const char *case_cond_str, const char *item_cond_str, unsigned case_style)
{
    if (!case_cond_str || !item_cond_str) return 0 ;
    if (case_style == VERI_RANDCASE) return 0 ; // Donot support 'randcase'

    unsigned case_cond_len = (unsigned)Strings::len(case_cond_str) ;
    unsigned item_cond_len = (unsigned)Strings::len(item_cond_str) ;
    if (case_cond_len != item_cond_len) return 0 ; // Does not match

    char c_char ;
    char i_char ;
    for (unsigned i=0; i<case_cond_len; i++) {
        c_char = (char)::tolower(case_cond_str[i]) ;
        i_char = (char)::tolower(item_cond_str[i]) ;
        switch (case_style) {
        case VERI_CASEZ:
           // For casez, 'x' and 'z' are don't care, '?' is equivalent to 'z':
           if ((c_char == '?') ||
               (i_char == '?') ||
               (c_char == 'z') ||
               (i_char == 'z')) continue ;

        case VERI_CASEX:
           // For casex, 'x' is don't care:
           if ((c_char == 'x') ||
               (i_char == 'x')) continue ;

        case VERI_CASE:
        default:
           // Here, for normal case, this two must match:
           if (c_char != i_char) return 0 ; // No match
           break ;
        }
    }

    return 1 ; // Match found
}

