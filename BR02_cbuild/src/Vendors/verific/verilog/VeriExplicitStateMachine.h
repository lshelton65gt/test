/*
 *
 * [ File Version : 1.25 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_EXPLICITSTATEMACHINE_H_
#define _VERIFIC_VERI_EXPLICITSTATEMACHINE_H_

#include "Array.h"
#include "Set.h"
#include "Map.h"

#include "VeriVisitor.h"    // Visitor base class definition
#include "VeriCopy.h"
#include "ControlFlow.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VeriScope ;
class VeriExpression ;

// VIPER #7031, VIPER #7034: Optimization of explicit state machine is add under this complile flag
// We need to optimize the parse tree for the following issues:
// 1) Megre duplicate state nodes:
//    If a particular node is child of two state node, we can merge that two state nodes.
//    Eg.
//        @(posedge clk)
//        count = 0 ;
//        if (cond > 2) begin
//            count++ ;
//            @(posedge clk) ;
//        end
//        else (cond > 2) begin
//            count++ ;
//            @(posedge clk) ;
//        end
// Here the last statement of both if and else branch is a clocking statement ie. state node.
// and both the state node is parent of statement node 'count=0', so we merge the two state
// node into one state node.
// During this process we mark the traversed state node by storing the state number(increased by 1,
// as we can have state '0', and as '0' cannot be inserted to a set, we store state number by increasing
// its value by 1  and will be retrived by decreasing its value by 1 when required).
// Whenever a state number of a state is already stored in state_set, we do not
// store the state in the set state_count. At the same time, we store that duplicate state node
// in a multi-map 'duplicate_state_map' against the existing state node.

// 2) Renumber the state nodes:
//    Renumber ll the state nodes of the CFG beginnig from 0 as 0,1,2.....
//    We can have some situations where we have state nodes numbered as 0,2,1.
//    During optimization, these states will be renumbered as 0,1,2. We also can have
//    some situations where we have states 0,1,3 (after merging duplicate state nodes:VIPER#7031),
//    in these cases we will renumber the state nodes as 0,1,2.
//    When renumbering the state nodes of the array 'state_count' we also renumber all
//    the duplicate state nodes stored in the duplicate_state_map.
#define OPTIMIZE_EXPLICIT_STATE_MACHINE        // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*
   Class to visit module and the hierarchy under it. This class is used to create the
   control flow graph under checking some conditions and limitations and finally create
   a new module item replacing the older one:
*/
class VFC_DLL_PORT VeriFsmAlwaysVisitor : public VeriVisitor
{
public:
    VeriFsmAlwaysVisitor();
    virtual ~VeriFsmAlwaysVisitor();

private:
    // Prevent the compiler from implementing the following
    VeriFsmAlwaysVisitor(const VeriFsmAlwaysVisitor &node) ;           // Purposely leave unimplemented
    VeriFsmAlwaysVisitor& operator=(const VeriFsmAlwaysVisitor &rhs) ; // Purposely leave unimplemented

public:
    // The following methods need to be redefined for our purpose :
    virtual void      VERI_VISIT(VeriModule, module) ;
    virtual void      VERI_VISIT(VeriGenerateBlock, gen_block) ;
    virtual void      VERI_VISIT(VeriGenerateConstruct, gen_construct) ;
    virtual void      VERI_VISIT(VeriGenerateConditional, gen_cond) ;
    virtual void      VERI_VISIT(VeriGenerateCaseItem, gen_case_item) ;
    virtual void      VERI_VISIT(VeriGenerateFor, gen_for) ;
    virtual void      VERI_VISIT(VeriAlwaysConstruct, always) ;
    virtual void      VERI_VISIT(VeriInitialConstruct, initial) ;
    virtual void      VERI_VISIT(VeriEventControlStatement, event) ;
    virtual void      VERI_VISIT(VeriEventTrigger, event) ;
    virtual void      VERI_VISIT(VeriForever, forever) ;
    virtual void      VERI_VISIT(VeriSeqBlock, seq_blk) ;

    // Called from VeriAlwaysConstruct and VeriInitialConstruct, which check some conditions
    // and limitations and then create the cfg:
    unsigned            ConvertToExplicitStateMachine(const VeriModuleItem *item, VeriStatement *stmt) ; // Check for some conditions and create the cfg
    VerificCFGRootNode* ProcessStatement(VeriStatement *stmt) ; // Create the cfg and return new module item

    Map*                TakeAlwaysMap() ;
    Map*                TakeEventVarMap() ;
    Map*                TakeDeclNodeMap() ;
    Map*                TakeFirstBlockIdMap() ;
    Map*                TakeEventNodeMap() ;
    Array*              TakeNewlyCreatedArray() ;
    Array*              TakeTempArray() ;
    Map*                TakeRootVrsEventVarMap() ;

    unsigned              IsTransperentNode(const VerificCFGRootNode *node) const ; // statement node corresponding to sequential block/scope end node
    unsigned              IsClockedEventNode(const VerificCFGRootNode *node) const ; // node corresponding to clocking event not event wait
    static char*          CreateVariableName(const char *prefix, const Array *label_arr, VeriScope *scope) ; // Create unique name for variable: checking name conflicts in that scope and the upper scopes
    static int            GetExprSize(const VeriExpression *expr) ; // VIPER #7338
    unsigned              ReArrangeCfgForEventWait(VerificCFGRootNode *node, Set *visited) ; // Rearrange cfg: check for clock after event wait, go upward of the cfg and push copy of state node
    unsigned              ClockFollowed(VerificCFGRootNode *node, Set *visited) ; // Check for a clock after event wait
    unsigned              PushNode(VerificCFGRootNode *parent, VerificCFGRootNode *child, VerificCFGRootNode *state, Set *visited) ; // push copy of state node
    unsigned              ReplaceEventNode(VerificCFGRootNode *node, Set *visited) ; // Replace event wait with a subgraph
    VerificCFGRootNode*   CreateEventSubGraph(VerificCFGRootNode *node) ; // Create a subgraph
    unsigned              ReArrangeCfgForEventTrigger(VerificCFGRootNode *node, Set *visited) ; // Replace event triggering with a conditional statement
    unsigned              InsertNodeAfterNearestStateNode(VerificCFGRootNode *node, VerificCFGRootNode *statement_node, Map &mutually_exclusive_event_map, Set *visited) ; // Insert a statement node restoring the value of event after the nearest clock which is followed by a event trigger
    void                  Reset() ; // Reset all the fields

protected:
    virtual void        TraverseArray(const Array *array) ; // Overwrite the VeriVisitor::TraverseArray()

private:
    Set                       _event_expr_set ;           // store the string made from event expression array of the first event control statement traversed
    Set                       _event_control_set ;        // store the total number of events within a always/initial construct: do not convert to explicit
    VeriScope                 *_scope ;                   // store the parent scope
    unsigned                  _has_always: 1 ;            // flag indicating module containing always block
    unsigned                  _has_initial: 1 ;           // flag indicating module containing initial block
    unsigned                  _has_event_control: 1 ;     // flag indicating module containing event trigger
    unsigned                  _has_forever: 1 ;           // flag indicating module containing forever statement
    unsigned                  _edged_expr_inserted: 1 ;   // flag indicating edge expression is already inserted in _event_expr_set
    unsigned                  _mult_clk: 1 ;              // this feature is not yet supported: flag indicating module containing multiple clocking event
    unsigned                  _convert : 1 ;              // VIPER #6843: flag for initial construct with more than one event controls
    VeriIdDef                 *_first_blk_id ;            // VIPER #6922: use old block id for named block
    unsigned                  _label_already_set: 1 ;     // VIPER #6922: need for keep track that the label id setting
    unsigned                  _is_converted: 1 ;          // VIPER #6922: call resolve on module if any always/initial block is converted
    Map                       *_always_map ;              // VIPER #6667: map containing old module item vrs cfg root node
    Map                       *_event_variable_map ;      // VIPER #6667: map containing original event id vrs new event id created during cfg generation
    VerificCFGDeclarationNode *_decl_node ;               // VIPER #6667: node contain all the local declarations and repeat variable declarations
    Map                       *_decl_node_map ;           // VIPER #6667: map containing module item(always/initial construct) vrs _decl_node(containing new declarations)
    Map                       *_first_blk_id_map ;        // VIPER #6667: map containing module item(always/initial construct) vrs first block id of always/initial construct
    Map                       *_internal_event_node_map ; // Map containing event node map vrs always construct
    Map                       *_event_nodes ;             // Map containing conditional nodes: conditional_node1 vrs conditional_node2 and vice versa. conditional nodes corresponding to (start != start_old) and (start ==start_old) and vice versa
    Set                       *_event_set ;
    Map                       *_root_vrs_event_var_map ;  // VIPER #7422
    Array                     *_newly_created_arr ;       // VIPER #6667: array of statements that are created during CFG formation
    Array                     *_temp_array ;              // VIPER #6667: array of declaration nodes created for new declarations
    unsigned                  _is_not_event_or_edged: 1 ; // VIPER #6667
    unsigned                  _edged_named_event: 1 ;     // VIPER #6667: edged named event is not supported
    unsigned                  _mixed_event: 1 ;           // VIPER #6667: mixture of wait and clocking event
    unsigned                  _has_event_trigger: 1 ;     // flag for event trigger
    unsigned                  _has_nonedged_event_control: 1 ; // flag for event wait
    unsigned                  _rearrange: 1 ;             // flag for rearranging cfg, this flag is set by the fault, if it is not set, error out and do not continue with the other nodes.
    unsigned                  _clock_followed: 1 ;        // flag for clocking statement following wait statement
    unsigned                  _state_num: 10 ;            // require for renumbering state nodes during rearranging the cfg
    unsigned                  _counter: 10 ;              // for creating unique event names
} ; // class VeriFsmAlwaysVisitor

/* -------------------------------------------------------------------------- */

/*
   Visitor class for renaming selected_name of the new generated parse tree:
*/
class VFC_DLL_PORT VeriFsmSelectedNameVisitor : public VeriVisitor
{
public:
    VeriFsmSelectedNameVisitor();
    virtual ~VeriFsmSelectedNameVisitor();

private:
    // Prevent the compiler from implementing the following
    VeriFsmSelectedNameVisitor(const VeriFsmSelectedNameVisitor &node) ;           // Purposely leave unimplemented
    VeriFsmSelectedNameVisitor& operator=(const VeriFsmSelectedNameVisitor &rhs) ; // Purposely leave unimplemented

public:
    // The following methods need to be redefined for our purpose :
    virtual void    VERI_VISIT(VeriSelectedName, selected_name) ;

    // methods:
    void            SetOld2NewIdsMap(Map *old2new)          { _old2new_ids_map = old2new ; }
    void            SetOldIdsSeqBlkMap(Map *old2new)        { _old_ids_seqblk_map = old2new ; }

private:
    Map             *_old_ids_seqblk_map ;     // map containing old ids vrs depth of sequential block
    Map             *_old2new_ids_map ;        // ld vrs new ids map
} ; // class VeriFsmSelectedNameVisitor

/* -------------------------------------------------------------------------- */

/*
   Class to visit event control statement and event triger:
   During creating cfg this is used to check the presence of an event control/trigger
   within a block of statements:
*/
class VFC_DLL_PORT VeriFsmEventVisitor : public VeriVisitor
{
public:
    VeriFsmEventVisitor();
    virtual ~VeriFsmEventVisitor();

private:
    // Prevent the compiler from implementing the following
    VeriFsmEventVisitor(const VeriFsmEventVisitor &node);
    VeriFsmEventVisitor& operator=(const VeriFsmEventVisitor &rhs);

public:
    // The following methods need to be redefined for our purpose :
    virtual void    VERI_VISIT(VeriEventControlStatement, node);
    virtual void    VERI_VISIT(VeriEventTrigger, event) ;
    virtual void    VERI_VISIT(VeriDisable, disable) ;
    virtual void    VERI_VISIT(VeriSeqBlock, seq_blk) ;
    virtual void    VERI_VISIT(VeriJumpStatement, jump) ;

    unsigned        HasEvent() const            { return _has_event ; }
    unsigned        HasLocalBlock() const       { return _has_local_block ; }
    unsigned        HasJumpOrDisable() const    { return _has_jump_or_disable ; }

private:
    unsigned       _has_event: 1 ;           // Flag for events
    unsigned       _has_local_block: 1 ;     // Flag for local block
    unsigned       _has_jump_or_disable: 1 ; // Flag for jump/disable statements
} ; // class VeriFsmEventVisitor

/* -------------------------------------------------------------------------- */

/*
   VIPER #7021: Class to visit sequential statements.
   This class is used to create and declare new label id
*/
class VFC_DLL_PORT VeriFsmBlkLabelVisitor: public VeriVisitor
{
public:
    VeriFsmBlkLabelVisitor(VeriScope *scope, VeriMapForCopy &old2new);
    virtual ~VeriFsmBlkLabelVisitor();

private:
    // Prevent the compiler from implementing the following
    VeriFsmBlkLabelVisitor();
    VeriFsmBlkLabelVisitor(const VeriFsmBlkLabelVisitor &node);
    VeriFsmBlkLabelVisitor& operator=(const VeriFsmBlkLabelVisitor &rhs);

public:
    // The following method needs to be redefined for our purpose :
    virtual void    VERI_VISIT(VeriStatement, node);
    virtual void    VERI_VISIT(VeriSeqBlock, node);
    virtual void    VERI_VISIT(VeriParBlock, node);

private:
    VeriMapForCopy  *_old2new ;
    VeriScope       *_scope ;
} ; // class VeriFsmBlkLabelVisitor

/* -------------------------------------------------------------------------- */

/*
   Class to visit plain/event control/conditional/loop statements under always/initial block.
   This class is used to create the control flow graph and a new module item:
*/
class VFC_DLL_PORT VeriFsmProcessVisitor : public VeriVisitor
{
public:
    VeriFsmProcessVisitor();
    virtual ~VeriFsmProcessVisitor();

private:
    // Prevent the compiler from implementing the following
    VeriFsmProcessVisitor(const VeriFsmProcessVisitor &node);
    VeriFsmProcessVisitor& operator=(const VeriFsmProcessVisitor &rhs);

public:
    // The following methods need to be redefined for our purpose :
    virtual void     VERI_VISIT(VeriEventControlStatement, node) ;
    virtual void     VERI_VISIT(VeriEventTrigger, node) ; // VIPER #6667
    virtual void     VERI_VISIT(VeriSeqBlock, node);
    virtual void     VERI_VISIT(VeriStatement, node) ;
    virtual void     VERI_VISIT(VeriConditionalStatement, node) ;
    virtual void     VERI_VISIT(VeriCaseItem, node) ;
    virtual void     VERI_VISIT(VeriCaseStatement, node) ;
    virtual void     VERI_VISIT(VeriWhile, node) ;
    virtual void     VERI_VISIT(VeriFor, node) ;
    virtual void     VERI_VISIT(VeriForever, node) ;
    virtual void     VERI_VISIT(VeriRepeat, node) ;
    virtual void     VERI_VISIT(VeriDisable, node) ;
    virtual void     VERI_VISIT(VeriDoWhile, node) ;
    virtual void     VERI_VISIT(VeriJumpStatement, node) ;
    virtual void     VERI_VISIT(VeriAssertion, node) ;

    void                      SetCurrentNode(VerificCFGRootNode *current_node)   { _current_node = current_node ; }
    void                      SetParentNode(VerificCFGRootNode *parent_node)     { _parent_node = parent_node ; }
    VerificCFGRootNode*       GetCurrentNode() const              { return _current_node ; } // Get the current processing node
    VerificCFGRootNode*       GetParentNode() const               { return _parent_node ; }  // Get the parent node
    unsigned                  GetState() const                    { return _state_num ; }    // Get the state
    void                      SetScope(VeriScope *scope)          { _scope = scope ; }
    VerificCFGDeclarationNode *GetDeclNode()                      { return _decl_node ; }
    void                      SetNewlyCreatedArray(Array *arr)    { _newly_created_arr = arr ; }
    void                      SetTempArray(Array *arr)            { _temp_array = arr ; }
    void                      AddMyParent(VerificCFGRootNode *parent, VerificCFGRootNode *child) const ;

private:
    VerificCFGRootNode        *_parent_node ;           // pointer to the parent node
    VerificCFGRootNode        *_current_node ;          // pointer to currently processing node
    VerificCFGRootNode        *_loop_node ;             // keep the parent loop node for treating break/continue statements
    VerificCFGRootNode        *_seq_blk_end ;           // keep the parent sequential block
    VerificCFGDeclarationNode *_decl_node ;             // node contain all the local declarations and repeat variable declarations
    Map                       _seq_blk_map ;            // store label id vrs the end node of a sequential block
    unsigned                  _state_num: 10 ;          // state
    VeriScope                 *_scope ;                 // contains the present scope
    Array                     *_continue_nodes ;        // contains the nodes for continue statements in loops
    Array                     *_newly_created_arr ;     // array of statements that are created during CFG formation
    Array                     *_temp_array ;            // array of declaration nodes created for repeat variable declaration
    Map                       _decl_map ;               // map containing data_decl against scope: not to create duplicate declarations
                                                        // for a particular local declaration inside 'for' loop during 'for' loop unrolling.
} ; // class VeriFsmProcessVisitor

/* -------------------------------------------------------------------------- */

/*
   VIPER #6667: Class to visit module and the hierarchy under it. This class is used to generate
   the explicit state m/c by creating a new module item replacing the older one:
*/
class VFC_DLL_PORT VeriFsmGenerateVisitor : public VeriVisitor
{
public:
    VeriFsmGenerateVisitor();
    virtual ~VeriFsmGenerateVisitor();

private:
    // Prevent the compiler from implementing the following
    VeriFsmGenerateVisitor(const VeriFsmGenerateVisitor &node) ;           // Purposely leave unimplemented
    VeriFsmGenerateVisitor& operator=(const VeriFsmGenerateVisitor &rhs) ; // Purposely leave unimplemented

public:
    // The following methods need to be redefined for our purpose :
    virtual void      VERI_VISIT(VeriModule, module) ;
    virtual void      VERI_VISIT(VeriGenerateBlock, gen_block) ;
    virtual void      VERI_VISIT(VeriGenerateConstruct, gen_construct) ;
    virtual void      VERI_VISIT(VeriGenerateConditional, gen_cond) ;
    virtual void      VERI_VISIT(VeriGenerateCaseItem, gen_case_item) ;
    virtual void      VERI_VISIT(VeriGenerateFor, gen_for) ;
    virtual void      VERI_VISIT(VeriAlwaysConstruct, always) ;
    virtual void      VERI_VISIT(VeriInitialConstruct, initial) ;
    virtual void      VERI_VISIT(VeriSeqBlock, seq_blk) ;

    VeriModuleItem*   CreateExplicitStateMachine(const VeriModuleItem *item, VerificCFGRootNode *root, const VerificCFGDeclarationNode *decl_node, VeriIdDef *first_block_id, Map *internal_event_map, unsigned always_construct) ; // VIPER 6667: Generate the explicit state m/c
    void              CountStateNode(VerificCFGRootNode *root, Array &state_node, Set *visited_set, Set &state_set, Map &state_map, Map &duplicate_state_map) ; // Count the total number of event controls in an always/initial construct
    void              MergeDuplicateStateNodes(VerificCFGRootNode *start, VerificCFGRootNode *node, Map &state_map, Map &duplicate_state_map)  ; // VIPER #7031: Merge duplicate state nodes
    void              ReNumberStateNode(Array &state_count, Map &duplicate_state_map) ; // VIPER #7034: Renumber the state nodes as 0,1,2....

    void              ExplicitStateMachine(VerificCFGRootNode *child, const VerificCFGRootNode *node, Map &state_map, Map *internal_event_map, Map &mutually_exclusive_event_map, Array *expr_arr, Array *stmts, VeriScope *scope, VeriMapForCopy &old2new) ; // Generate ExplicitStateMachine
    virtual void      GenerateCaseState(VerificCFGRootNode *node, Array *stmts) ; // Generate statement for state node(event control)

    void              SetAlwaysMap(Map *always_map)                  { _always_map = always_map ; }
    void              SetResetVariable(unsigned reset)               { _reset = reset ; }
    void              SetDeclNodeMap(Map *decl_node_map)             { _decl_node_map = decl_node_map ; }
    void              SetFirstBlockIdMap(Map *first_blk_id_map)      { _first_blk_id_map = first_blk_id_map; }
    void              SetEventNodeMap(Map *event_node_map)           { _internal_event_node_map = event_node_map ; }
    unsigned          GetNumOfItemReplaced() const                   { return _items_replaced ; }
    void              Reset() ; // Reset all the fields

protected:
    virtual void      TraverseArray(const Array *array) ; // Overwrite the VeriVisitor::TraverseArray()

private:
    VeriScope             *_module_scope ;            // store the module scope
    VeriScope             *_scope ;                   // store the parent scope
    VeriModuleItem        *_always ;                  // store the newly created module item
    unsigned              _always_construct ;         // used for keep track of the mumber of always construct: used for naming the sequential block of newly created always construct
    VeriIdDef             *_var_id ;                  // store the id of the state variable required for explicit state m/c generation
    unsigned              _infinite_loop: 1 ;         // flag indicating presence of cycles
    Map                   *_old_ids_seqblk_map ;      // map containing old ids vrs depth of sequential block
    Map                   *_old2new_ids_map ;         // ld vrs new ids map
    unsigned              _reset: 1 ;                 // flag for reset variable, required to execute reset block
    VerificCFGRootNode    *_init_state_node ;         // store the initial state node
    Map                   *_always_map ;              // map containing root node of cfg vrs the always construct
    Map                   *_decl_node_map ;           // map containing the event variable declarations vrs always construct
    Map                   *_first_blk_id_map ;        // map containing the blocking id vrs always construct
    Map                   *_internal_event_node_map ;
    unsigned              _items_replaced ;           // number of always/initial construct replaced
} ; // class VeriFsmGenerateVisitor

/* -------------------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_EXPLICITSTATEMACHINE_H_

