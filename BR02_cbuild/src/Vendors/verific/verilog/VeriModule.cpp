/*
 *
 * [ File Version : 1.349 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>
#include <stdio.h>   // for sprintf
#include <time.h>    // for timestamp
#include <ctype.h>   // for isdigit
#include <stdlib.h>  // for strtod, strtoul

#include "Map.h"
#include "Set.h"
#include "Strings.h"
#include "Array.h"

#include "TextBasedDesignMod.h"  // to support Module text-based-design-modification API

#include "VeriLibrary.h"
#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "VeriMisc.h"
#include "VeriId.h"
#include "VeriExpression.h"
#include "VeriScope.h"

#include "veri_file.h"
#include "veri_tokens.h"
#include "Message.h"
#include "VeriConstVal.h"
#include "veri_tokens.h"
#include "VeriCopy.h"

#include "../vhdl/vhdl_file.h"
#include "../vhdl/VhdlTreeNode.h"
#include "../vhdl/VhdlStatement.h"
#include "../vhdl/VhdlName.h"
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/VhdlIdDef.h"
#include "../vhdl/VhdlDeclaration.h"
#include "../vhdl/VhdlSpecification.h"
#include "../vhdl/VhdlScope.h"

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
//VIPER #6895: Need for vhdl to verilog package conversion
#include "../vhdl/VhdlValue_Elab.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// VIPER #5932: Compile flag to disable the deletion only:
#define DELETE_LINEFILES_OF_UNUSED_Y_V_MODULES

/* -------------------------------------------------------------- */
// class to visit default clocking decl to set the default sva clock
// Viper 6820. The default clocking declaration may occur after the
// assertion and hence we should capture it before hand to eliminate false error
class ClockingDeclVisit : public VeriVisitor
{
public:
    ClockingDeclVisit() : VeriVisitor() { }
    ~ClockingDeclVisit() { }

private:
    // Prevent compiler from defining the following
    ClockingDeclVisit(const ClockingDeclVisit &) ;            // Purposely leave unimplemented
    ClockingDeclVisit& operator=(const ClockingDeclVisit &) ; // Purposely leave unimplemented

public:
    virtual void VERI_VISIT(VeriClockingDecl, node) ;
    virtual void VERI_VISIT(VeriAlwaysConstruct, node) ;
    virtual void VERI_VISIT(VeriInitialConstruct, node) ;
    virtual void VERI_VISIT(VeriModuleInstantiation, node) ;
    virtual void VERI_VISIT(VeriDataDecl, node) ;
private:
} ; // class ClockingDeclVisist

/* -------------------------------------------------------------- */

void ClockingDeclVisit::VERI_VISIT(VeriClockingDecl, node)
{
     // VIPER #3663 : Mark clocking identifier as default clocking
    if (node.GetQualifier(VERI_DEFAULT)) {
        VeriNode::_sva_clock_expr = node.GetClockingEvent() ;
        VeriIdDef *clocking_id = node.GetId() ;
        // Viper 6237: Get the clocking decl from the declaration
        if (!VeriNode::_sva_clock_expr && clocking_id && clocking_id->IsDefaultClocking()) {
            VeriModuleItem *clk_decl = clocking_id->GetModuleItem() ;
            VeriNode::_sva_clock_expr = clk_decl ? clk_decl->GetClockingEvent() : 0 ;
        }
    }
}
// Make the following routines return because the clocking decl
// can only be a module or generate item. LRM STD 2009 Sec 14.12
void ClockingDeclVisit::VERI_VISIT(VeriAlwaysConstruct, node) { (void) node ; }
void ClockingDeclVisit::VERI_VISIT(VeriInitialConstruct, node) { (void) node ; }
void ClockingDeclVisit::VERI_VISIT(VeriModuleInstantiation, node) { (void) node ; }
void ClockingDeclVisit::VERI_VISIT(VeriDataDecl, node) { (void) node ; }

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VeriModule::VeriModule(VeriIdDef *id, Array *parameter_connects, Array *port_connects, Array *module_items, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _package_import_decls(0),
    _parameter_connects(parameter_connects),
    _port_connects(port_connects),
    _module_items(module_items),
    _scope(scope),
    _parameters(0),
    _ports(0),
    _bind_instances(0),
    _timestamp(0),
    _analysis_dialect(0),
    _is_celldefine(0),
    _default_nettype(0),
    _unconnected_drive(0),
    _is_analyzed(0),
    _has_analyze_error(0),
    _is_compiled(0),
    _is_static_elaborated(0),
    _compile_as_blackbox(0),
    _is_root_module(0),
    _affected_by_path_name(0),
    _original_module_name(0),
    _timescale(0),
    _parent_library(0)
    , _port_scope(0)
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    , _linefile_structs(0) // VIPER #5932
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    , _module_item_replaced(0) // VIPER #6550
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    , _is_vhdl_package(0)      // VIPER #6895: flag for converted verilog package from a vhld one
    , _is_already_processed(0) // VIPER #6895: flag on package that is already elaborated
#endif
    , _delay_mode_distributed(0),
    _delay_mode_path(0),
    _delay_mode_unit(0),
    _delay_mode_zero(0),
    _default_decay_time(0),
    _default_trireg_strength(0)
{
    // Set back-pointer to tree onto identifier :
    if (_id) _id->SetModule(this) ;

    // Set the tiemstamp to now :
    _timestamp = (unsigned long) ::time(0) ;

    // Set the dialect under which we are currently parsing this module :
    _analysis_dialect = veri_file::GetAnalysisMode() ;

    // Set predefined macro flags from lexer onto this module :
    _default_nettype = veri_file::GetUserDefaultNettype() ; // VIPER #4581, #7128: may be NULL
    _is_celldefine = veri_file::GetCelldefine() ;
    _unconnected_drive = veri_file::GetUnconnectedDrive() ;
    // VIPER #7703: Time-scale is now properly set from bison, keep it here for other cases (not created from bison):
    _timescale = Strings::save(veri_file::GetTimeScale()) ; // saved form of present timescale setting (can be NULL if not set).

    // VIPER #7379: Add compiler directives from Annex-E of IEEE 1800 (2009) LRM:
    _delay_mode_distributed  = veri_file::GetDelayModeDistributed() ;
    _delay_mode_path         = veri_file::GetDelayModePath() ;
    _delay_mode_unit         = veri_file::GetDelayModeUnit() ;
    _delay_mode_zero         = veri_file::GetDelayModeZero() ;
    _default_decay_time      = Strings::save(veri_file::GetDefaultDecayTime()) ;
    _default_trireg_strength = Strings::save(veri_file::GetDefaultTriregStrength()) ;

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    // VIPER #5932: Take the Array of all linefiles created for this particular module:
    // Only available for non-nested modules created from user library files.
    _linefile_structs = veri_file::TakeLinefileStructures() ;
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)

// RD: VIPER 5182: do not resolve modules until they are added to a permanent library.
// So moved 'ResolveBody' call out of the constructor, and is now called separate in the bison file (after module parsing).
//    if (!is_nested_module) ResolveBody() ;
}

void
VeriModule::ResolveBody()
{
    // VIPER #7718: For extern modules, return after removing the open port connection:
    // Do not resolve body of extern module, header may not be complete:
    //if (_id && _id->IsExtern()) return ;

    // Just like instantiations :
    // LRM is confusing on this : module i ()
    // could mean :
    //    - module has one port which is not connected
    //    - module has no ports (and none are connected).
    // We will assume that this means a module without ports :
    if (_port_connects && _port_connects->Size()==1) {
        VeriExpression *assoc = (VeriExpression*)_port_connects->GetLast() ;
        if (assoc->IsOpen()) {
            _port_connects->Remove(0) ;
            delete assoc ;
        }
    }

    // VIPER #7718: For extern modules, return after removing the open port connection:
    // Do not resolve body of extern module, header may not be complete:
    if (_id && _id->IsExtern()) return ;

    // Resolve all the elements in the tree (resolve all referenced identifiers in there) :
    //   parameter_connects
    //   port_connects
    //   module items

    // Also, set-up port/parameter lists :
    // done by order from ansi port/parameters list (in port_connects) and/or the ModuleItems

    VeriScope *save_scope = _present_scope ;
    _present_scope = _scope ;

    VeriExpression *save_sva_clock_expr = _sva_clock_expr ;
    _sva_clock_expr = 0 ;

    unsigned save_contains_event_control = _contains_event_control ;
    _contains_event_control = 0 ;
    unsigned save_contains_delay_control = _contains_delay_control ;
    _contains_delay_control = 0 ;
    unsigned save_contains_even_id_in_body = _contains_even_id_in_body ;
    _contains_even_id_in_body = 0 ;
    unsigned save_multiple_inferred_clock = _multiple_inferred_clock ;
    _multiple_inferred_clock = 0 ;

    // Memory leak fix: Resolve(Body) can be called second time on nested
    // module form RTL elab, so do not create ports/parameters then:
    if (!_ports) _ports = new Array() ;
    _ports->Reset() ; // But do reset the Array to avoid multiple entry
    if (!_parameters) _parameters = new Array() ;
    _parameters->Reset() ; // But do reset the Array to avoid multiple entry

    VeriModuleItem *item ;
    VeriExpression *port_expr ;

    unsigned has_ansi_port_list = 0 ;
    unsigned has_ansi_parameter_list = 0 ;
    unsigned i ;

    //unsigned dot_star_in_port_list = 0 ;
    FOREACH_ARRAY_ITEM(_package_import_decls, i, item) {
        if (item) item->Resolve(_scope, VERI_UNDEF_ENV) ;
    }
    // parameter list (of VeriModuleItems)
    FOREACH_ARRAY_ITEM(_parameter_connects, i, item) {
        if (!item) continue ;
        // Resolve
        item->Resolve(_scope, VERI_UNDEF_ENV) ;
        // Collect the parameters (VeriIdDef's) in here, by order.
        item->AccumulateParameters(*_parameters, 0 /* do not check for ANSI param: this is the ANSI param */, _id) ;
        // Only ANSI style VeriDataDecls are allowed here. So all will be 'ansi' style.
        if (item->IsParamDecl()) has_ansi_parameter_list = 1 ;
    }

    // port list (of VeriExpressions)
    // SV LRM 19.8 : ANSI ports inherit direction and type from previous ports.
    // We implement this 'inheritance' by setting back-pointer, type and direction into the ANSI id's.
    // Default direction is INOUT, default type is 'wire' (or actually DefaultNetType())
    // Default direction is 'INOUT', default kind is 'wire' (or actually DefaultNetType()) and default type is 'logic'
    // VIPER #5949: Set kind to 'wire' (or actually DefaultNetType())
    unsigned nettype = veri_file::GetUserDefaultNettype() ; // VIPER #4581, #7128: may be NULL
    unsigned previous_direction = IsChecker() ? VERI_INPUT: VERI_INOUT ;
    unsigned default_kind = GetDefaultNetType() ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = default_kind ;
    // For system verilog default_type is VERI_LOGIC
    // VIPER #6527 : Move the fix for VIPER #6438 inside 'ResolveAnsiPortDecl'
    // VIPER #6438: For default nettype (actually DefaultNetType()) of type none,
    // the default type will be of type none:
    if (IsSystemVeri()) { // && nettype != VERI_NONE) {
        default_type = VERI_LOGIC ;
    }
    unsigned previous_type = default_type ;
    veri_file::SetDefaultNettype(default_kind) ;
    //unsigned default_type = GetDefaultNetType() ;
    //unsigned previous_type = default_type ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (!port_expr) continue ;

        // Set flag :
        if (port_expr->IsAnsiPortDecl()) has_ansi_port_list = 1 ;

        if (has_ansi_port_list) {
            // ANSI port lists, need 'previous' type/direction info.
            port_expr->ResolveAnsiPortDecl(_scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
        } else {
            // VIPER #3672 : If interface type port is declared in non-ansi style, associate
            // inout direction with those, so that they canbe considered later as ports
            port_expr->MarkInterfaceTypePort(_scope) ;
            // Resolve, knowing that this is a port expression
            port_expr->Resolve(_scope, VERI_PORT_EXPRESSION) ;
        }

        // Collect the ports (VeriIdDef's) in here, by order.
        // This will accumulate ANSI ports, but also full-id '95 port expression ports.
        // Any indexed/sliced/concat and named port expressions will insert 0 at that position (no port IdDef available)
        port_expr->AccumulatePorts(*_ports) ;
    }

    // module items list (of VeriModuleItems)
    // VIPER #5248 (test_25): Find the (single) 'initial' block in UDP:
    VeriModuleItem *initial = 0 ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    unsigned gen_blk_num = 1 ;
//#endif
    // Viper 5316. number of timeunits and timeprecisions present
    unsigned timeunits_present = 0 ;
    unsigned timeprecisions_present = 0 ;
    // VIPER #8304: Store previous value of timeunit and timeprecision to allow
    // multiple declarations with same value
    char *prev_timeunit = 0 ;
    char *prev_timeprecision = 0 ;
    if (IsSystemVeri()) {
        // Viper 6820. The default clocking declaration may occur after the
        // assertion and hence we should capture it before hand to eliminate false error
        ClockingDeclVisit clk_visit ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) {
            if (!item) continue ;
            item->Accept(clk_visit) ;
            if (_sva_clock_expr) break ;
        }
    }
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (!item) continue ;

        // VIPER #3370 : Do not resolve property/sequence body here. Will be resolved from call
        // VIPER #7720 : Do not resolve body of let here. Will be resolved from call
        if (item->IsPropertyDecl() || item->IsSequenceDecl() || item->IsLetDecl()) continue ;

        // VIPER #5248 (test_25): Find the 'initial' block in the UDP and do not resolve it now:
        if (IsPrimitive() && !item->IsDataDecl() && !item->IsTable()) {
            // In primitives, only data decls, table(1) and initial block(1) are allowed in bison.
            VERIFIC_ASSERT(!initial) ; // It must not be set here
            initial = item ; // So, this must be an 'initial' block
            continue ; // Ignore it for now
        }

        // Reset _has_delay_or_event_control here set from previous item
        VeriNode::_has_delay_or_event_control = 0 ;

        // VIPER #7506: Reset the flag here set from previous item
        VeriNode::_potential_always_loop = 0 ;

        // Resolve this item
        item->Resolve(_scope, VERI_UNDEF_ENV) ;

        // Viper 5316. Error out if timeunit/timeprecision is not the first mi in the module
        if (item->IsTimeUnitDecl()) {
            VeriExpression *curr_literal_expr = item->GetTimeLiteral() ;
            char *curr_literal = curr_literal_expr ? curr_literal_expr->Image(): 0 ;
            if (item->GetTimeUnitType()==VERI_TIMEPRECISION) {
                if (timeprecisions_present)  {
                    // VIPER #8304: Produce error if value is different from previous declaration
                    if (Strings::compare(prev_timeprecision, curr_literal)==0) {
                        item->Error("multiple definitions of %s", "timeprecision") ;
                    }
                } else if (i != timeunits_present) {
                    item->Error("unexpected %s","timeprecision") ;
                }
                timeprecisions_present++ ;
                if (!prev_timeprecision) {
                    prev_timeprecision = curr_literal ;
                } else {
                    Strings::free(curr_literal) ;
                }
            } else if (item->GetTimeUnitType()==VERI_TIMEUNIT) {
                if (timeunits_present)  {
                    // VIPER #8304: Produce error if value is different from previous declaration
                    if (Strings::compare(prev_timeunit, curr_literal) == 0) {
                        item->Error("multiple definitions of %s", "timeunit") ;
                    }
                } else if (i != timeprecisions_present) {
                    item->Error("unexpected %s","timeunit") ;
                }
                timeunits_present++ ;
                if (!prev_timeunit) {
                    prev_timeunit = curr_literal ;
                } else {
                    Strings::free(curr_literal) ;
                }
                // VIPER #8304 : Consider time precision from timeunit decl
                VeriExpression *time_precision = item->GetTimePrecision() ;
                char *curr_timeprecision = time_precision ? time_precision->Image(): 0 ;
                if (time_precision && timeprecisions_present && (Strings::compare(prev_timeprecision, curr_timeprecision)==0)) {
                    item->Error("multiple definitions of %s", "timeprecision") ;
                }
                if (!prev_timeprecision) {
                   prev_timeprecision = curr_timeprecision ;
                } else {
                   Strings::free(curr_timeprecision) ;
                }
                if (time_precision) timeprecisions_present++ ;
            }
            if (prev_timeunit && prev_timeprecision && IsFirstLessThanSecondLiteral(prev_timeunit, prev_timeprecision)) {
                item->Error("error in timescale or timeprecision statement. <timeprecision> must be at least as precise as <timeunit>") ;
            }
        }

        // Accumulate '95 style parameters from this declaration
        // VIPER #7603: ANSI parameter check and making this as localparam is now moved into this routine:
        item->AccumulateParameters(*_parameters, has_ansi_parameter_list, _id) ;

        // If port size is different, then there was AN IO decl with ANSI decl
        if (item->IsIODecl()) {
            // A port declaration.
            if (has_ansi_port_list) {
                if (_id) item->Error("port declaration not allowed in %s with formal port declaration list", _id->Name()) ;
                // CHECK ME : Should we turn them into nets ?
                // Also throw these ports out of the '_ports' list,
                // so they can never be accessed by 'order'. ReSize to the previous size :
            } else {
                // Accumulate '95 style ports from this declaration
                // Don't do this any more : We now accumulate all ports from the port_expression list.
                // That is the only list which is correct by order.
                // item->AccumulatePorts(*_ports) ;
            }
        }
        // VIPER #3056 : Issue error for nested module in verilog95 and 2001 mode
        if (item->IsModule() && !VeriNode::IsSystemVeri()) {
            item->Error("%s not allowed in this dialect. Use system verilog mode", "nested module") ;
        }
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
            // VIPER #5727 : Unnamed generate block handling :
            if (item->IsGenerateConstruct()) {
                unsigned k ;
                VeriModuleItem *gen_item ;
                Array *gen_items = item->GetItems() ;
                FOREACH_ARRAY_ITEM(gen_items, k, gen_item) {
                    if (!gen_item) continue ;
                    // Consider only generate block/for/if/case :
                    if (!gen_item->IsGenerateFor() && !gen_item->IsGenerateBlock() &&
                            !gen_item->IsGenerateConditional()) continue ;

                    gen_item->ProcessUnnamedGenBlk(_scope, gen_blk_num, -1 /* not a branch*/, 0, 0) ;
                    gen_blk_num++ ; // increase generate block number
                }
            }
            if (item->IsGenerateFor() || item->IsGenerateBlock() || item->IsGenerateConditional()) {
                item->ProcessUnnamedGenBlk(_scope, gen_blk_num, -1 /* not a branch*/, 0, 0) ;
                gen_blk_num++ ; // increase generate block number
            }
        }
//#endif
    }
    Strings::free(prev_timeunit) ;
    Strings::free(prev_timeprecision) ;
    if (initial) {
        // VIPER #5248 (test_25): Do not resolve the 'initial' block if the primitive is combinational:
        VeriTable *table = GetPrimitiveTable() ; // Find the UDP table entry
        if (table && table->IsCombinational()) {
            // Combination UDPs cannot have 'initial' statement:
            initial->Error("combinational udp cannot have initial statement") ;
            // Do not resolve this initial block, that may lead to further unintended errors.
        } else {
            // Here resolve this 'initial' block, now:
            initial->Resolve(_scope, VERI_UNDEF_ENV) ;
        }
    }

    if (_ports->Size()==0) {
        /* Set to 0 for faster checks later on */
        delete _ports ;
        _ports = 0 ;
    }

    if (_parameters->Size()==0) {
        /* Set to 0 for faster checks later on */
        delete _parameters ;
        _parameters = 0 ;
    }

    // Run variable-usage analysis, now that resolving is done on the module.
    // AnalyzeFull(); // can only call this after all modules are read in. (so at elaboration time).

    // Find extern module for this, check port/parameter validity and remove
    // the extern one
    VeriModule *extern_mod = _id ? _id->GetExternModule(): 0 ;
    if (extern_mod) {
        // Do not check types for parameters, standard simulators do not:
        Array *port_connects = extern_mod->GetPortConnects() ;

        if (port_connects) {
            // Port list exists means, both extern and actual module definition contains
            // port list. These two lists should specify ports in same order and with same type :
            unsigned j ;
            VeriIdDef *port_id, *actual_port_id ;
            unsigned port_pos = 0 ; // To maintain port order
            FOREACH_ARRAY_ITEM(port_connects, i, port_expr) {
                if (!port_expr) continue ;
                if (port_expr->IsAnsiPortDecl()) { // ansi port
                    Array *ids = port_expr->GetIds() ;
                    FOREACH_ARRAY_ITEM(ids, j, port_id) {
                        if (!port_id) continue ;
                        // Find corresponding actual port identifier
                        actual_port_id = (_ports && _ports->Size() > port_pos) ? (VeriIdDef*)_ports->At(port_pos) : 0 ;
                        port_pos++ ;
                        if (!actual_port_id) { // Actual port identifier does not exists, error
                            Error("actual module definition does not have port %s", port_id->Name()) ;
                        } else { // Check name and data type
                            actual_port_id->CheckWithExtern(port_id) ;
                        }
                    }
                } else {
                    port_id = port_expr->FullId() ; // Get port identifier from port expression
                    // Find corresponding actual port identifier
                    actual_port_id = (_ports && _ports->Size() > port_pos) ? (VeriIdDef*)_ports->At(port_pos) : 0 ;
                    if (port_id) {
                        if (!actual_port_id) { // Actual port identifier does not exists, error
                            Error("actual module definition does not have port %s", port_id->Name()) ;
                        } else { // Check name and data type
                            actual_port_id->CheckWithExtern(port_id) ;
                        }
                    } else {
                        // Viper 5337. Port expr w/o declaration do not have _id
                        if (!actual_port_id) { // Actual port identifier does not exists, error
                            Error("actual module definition does not have port %s", port_expr->GetName()) ;
                        } else { // Check name
                            if (!Strings::compare(Name(), port_expr->GetName())) {
                                // Produce warning if name does not match
                                actual_port_id->Warning("name of port declaration in module definition does not match that in extern module") ;
                                actual_port_id->Warning("port %s should exactly match its corresponding port in extern module", port_expr->GetName()) ;
                            }
                        }
                    }
                    port_pos++ ;
                }
            }
        }

        // Now remove extern module definition
        VeriLibrary *lib = extern_mod->GetLibrary() ;
        if (lib) { // Extern module is in library
            delete extern_mod ; // Remove that
        } else { // Extern module is nested one, remove from its container
            VeriScope *mod_scope = extern_mod->GetScope() ;
            VeriScope *upper = (mod_scope) ? mod_scope->Upper(): 0 ;
            VeriIdDef *container_id = (upper) ? upper->GetOwner(): 0 ;
            VeriModuleItem *container = (container_id) ? container_id->GetModuleItem(): 0 ;
            Array *items = (container) ? container->GetItems(): 0 ;
            FOREACH_ARRAY_ITEM(items, i, item) {
                if (item == extern_mod) {
                    delete extern_mod ;
                    items->Insert(i, 0) ;
                    break ;
                }
            }
        }
    }

    // In SystemVerilog, the 'upper' scope of modules (that do into a library) is the 'root' scope.
    // All identifiers in the root scope are visible directly from within the module.
    // So, we have to set a dependency of this module to the root scope.
    // NOTE: Do this only if there is something declared in the root scope.
    // Otherwise, we would create a lot of unnecessary dependencies on the root scope.
    // In VeriModule::AddModuleItem() we store the root module in the library the moment something is in it.
    // So, here, check if the root module is already in the library.
    // If not, detach the root scope from this module.
    // If so, then register a dependency and leave the 'upper' scope of this module pointing to the root scope.
    VeriScope *root_scope = (_scope) ? _scope->Upper() : 0 ;
    VeriIdDef *root_id = (root_scope) ? root_scope->GetOwner() : 0 ;
    VeriModule *root_tree = (root_id) ? root_id->GetModule() : 0 ;
    if (root_tree && root_tree->IsRootModule()) {
        if (root_tree->GetLibrary()) {
            // root unit is added to library. So there is something in it.
            // Flag dependency :
            if (_scope) _scope->Using(root_scope) ;
            // FIX ME : Ideally, we would like to flag this dependency only if there is something inside this module
            // that actually REFERS to something in the root scope. But that would require a bit more work...
            // (Maybe flag a scope if someone finds (scope::Find()) an identifier in a root scope).
            // For now, it should be enough just to flag a dependency if there is something in the root scope.
        } else if (root_scope && !Message::ErrorCount() && root_scope->IsStdPackageExplicitlyImported()) {
            // VIPER #3231, 3232, 3233, 3234 :
            // 'Std' package is explicitly imported in root-scope i.e. identifier
            // declared in 'std' package is used inside this module. So root module
            // should be in the using list of this module :
            // Add root module to work library :
            if (veri_file::AddRootModuleToWorkLib(root_tree)) {
                if (_scope) _scope->Using(root_scope) ;
            }
        } else {
            // There is nothing in the root unit yet. So detach it :
            // VIPER #5963: Do not reset _scope->_upper pointer, the root
            // scope may have something after this declaration. If it does
            // not, we will reset the pointer later; only under compatibility mode:
            if (root_scope && !VeriNode::InRelaxedCheckingMode()) {
                // So, we do not remove the child scope in compatibility mode.
                // Only reset the upper pointer if we are in normal mode:
                root_scope->RemoveChildScope(_scope) ;
            }
        }
    }
    // VIPER #6982: Attribute instance can be specified for modules/programs/interfaces,
    // those attributes have to be resolved after resolving the module/program/interface.
    ResolveAttributes(_scope) ;

    _sva_clock_expr = save_sva_clock_expr ;
    _contains_event_control = save_contains_event_control ;
    _contains_delay_control = save_contains_delay_control ;
    _contains_even_id_in_body = save_contains_even_id_in_body ;
    _multiple_inferred_clock = save_multiple_inferred_clock ;

    veri_file::SetDefaultNettype(nettype) ;
    _present_scope = save_scope ;

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    // VIPER #5932: This module is going to be used in the design.
    // So, delete the Array of linefiles to save memory:
    // Just delete the container (not the content) and set it to 0:
    delete _linefile_structs ;
    _linefile_structs = 0 ;
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
}

void
VeriModule::ProcessRootModule()
{
    if (!_is_root_module) return ; // Not a root module

    if (!_parameters) _parameters = new Array() ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (!item) continue ;

        if (item->IsParamDecl()) {
            // Accumulate '95 style parameters from this declaration
            item->AccumulateParameters(*_parameters, 0 /* root module cannot have ANSI parameter */, _id) ;
        }
    }

    if (_parameters->Size()==0) {
        /* Set to 0 for faster checks later on */
        delete _parameters ;
        _parameters = 0 ;
    }

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    // VIPER #5932: This module is going to be used in the design.
    // So, delete the Array of linefiles to save memory:
    // Just delete the container (not the content) and set it to 0:
    delete _linefile_structs ;
    _linefile_structs = 0 ;
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
}

unsigned
VeriModule::GetDefaultNetType() const
{
    // VIPER #4581, #7128: For NULL _default_nettype return VERI_WIDE (default):
    return (_default_nettype) ? _default_nettype : VERI_WIRE ;
}

void VeriModule::SetTimeScale(const char *s)
{
    if (!s) return ;
    Strings::free(_timescale) ;
    _timescale = Strings::save(s) ;
}

// Late parse-tree additions :
// Used during additions to a 'root' module..
unsigned
VeriModule::AddModuleItem(VeriModuleItem *module_item)
{
    if (!module_item) return 1 ;

    // Since this is used during addition to root module,
    // we should check if there was a error during parsing,
    // and ignore the item if there was.
    if (Message::ErrorCount()) {
        // Somewhat convoluted message..
        // RD: 4/07: decided to remove this message. Should be clear from the 'previous' errors that we ignore the construct.
        // module_item->Error("module %s ignored due to previous errors", "item") ;

        // Identifiers declared in a module item are normally owned by the scope (not by the module item).
        // We have made attempts here to remove the identifier from the scope, and delete it.
        // However, because they are really owned by the scope, and multiple module items can point at them,
        // such attempts were always followed by a core dump later in some test case.
        // So, do NOT attempt to delete the identifiers declared in this module item !
        // Simply leave them in the scope, and let scope removal take care of it.
        // This is also what we do for normal module items...
        //
        // Delete the item
        delete module_item ;

        // Clear error count (so that next module item will not be discarded) and return.
        // Do NOT clear error count. Errors in root items would otherwise go through analysis unnoticed.
        // Message::ClearErrorCount() ;
        return 0 ;
    }

    // Now, if this (root) module is not yet added to the library, do it now.
    // Do this when we know the error count is 0, otherwise Library::AddModule() will refuse to add the
    // root module and even worse : it will be delete the root module (and its scope with it).
    // And that will cause stale pointers, because the root scope is still the root scope in the parser.
    // So add the root module to the library right here, when we know that the error count is 0.
    if (IsRootModule() && !_parent_library) {
        (void) veri_file::AddRootModuleToWorkLib(this) ;
        if (_id && !LineFile::GetLineNo(_id->Linefile()) && LineFile::GetLineNo(module_item->Linefile())) _id->SetLinefile(module_item->Linefile()) ;
    }

    // Viper 5316. Error out if timeunit/timeprecision is not the first mi in the module
    if (module_item->IsTimeUnitDecl() && _module_items) {
        // Check whether we have timeunit/timeprecision already specified:
        unsigned timeunits_present = 0 ;
        unsigned timeprecisions_present = 0 ;
        // VIPER #8304: Store previous value of timeunit and timeprecision to allow
        // multiple declarations with same value
        char *prev_timeunit = 0 ;
        char *prev_timeprecision = 0 ;
        unsigned non_timeunit_decl = 0 ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_module_items, i, item) {
            if (!item) continue ;
            if (!item->IsTimeUnitDecl()) {
                non_timeunit_decl = 1 ;
            } else {
                VeriExpression *literal_expr = item->GetTimeLiteral() ;
                if (item->GetTimeUnitType() == VERI_TIMEUNIT) {
                    if (!prev_timeunit) prev_timeunit = literal_expr ? literal_expr->Image(): 0 ;
                    // VIPER #8304 : Consider time precision from timeunit decl
                    VeriExpression *time_precision = item->GetTimePrecision() ;
                    if (!prev_timeprecision && time_precision) prev_timeprecision = time_precision->Image() ;
                    if (time_precision) timeprecisions_present++ ;
                    timeunits_present++ ;
                } else /* if (item->GetTimeUnitType() == VERI_TIMEPRECISION) */ {
                    timeprecisions_present++ ;
                    if (!prev_timeprecision) prev_timeprecision = literal_expr ? literal_expr->Image(): 0 ;
                }
            }
        }
        VeriExpression *curr_literal_expr = module_item->GetTimeLiteral() ;
        char *curr_literal = curr_literal_expr ? curr_literal_expr->Image(): 0 ;
        if (module_item->GetTimeUnitType() == VERI_TIMEUNIT) {
            if (timeunits_present) {
                if (Strings::compare(prev_timeunit, curr_literal) == 0)  {
                    module_item->Error("multiple definitions of %s", "timeunit") ;
                }
            } else if (non_timeunit_decl) {
                // This is certainly not the first module item, error out:
                module_item->Error("unexpected %s","timeunit") ;
            }
            if (!prev_timeunit) prev_timeunit = Strings::save(curr_literal) ;
            VeriExpression *curr_time_precision = module_item->GetTimePrecision() ;
            if (curr_time_precision) {
                char *curr_time_precision_val = curr_time_precision->Image() ;
                if (timeprecisions_present) {
                    if (Strings::compare(prev_timeprecision, curr_time_precision_val) == 0) {
                        module_item->Error("multiple definitions of %s", "timeprecision") ;
                    }
                }
                if (!prev_timeprecision) {
                    prev_timeprecision = Strings::save(curr_time_precision_val) ;
                } else {
                    Strings::free(curr_time_precision_val) ;
                }
            }
        } else /* if (item->GetTimeUnitType() == VERI_TIMEPRECISION) */ {
            if (timeprecisions_present) {
                if (Strings::compare(prev_timeprecision, curr_literal) == 0)  {
                    module_item->Error("multiple definitions of %s", "timeprecision") ;
                }
            } else if (non_timeunit_decl) {
                // This is certainly not the first module item, error out:
                module_item->Error("unexpected %s","timeprecision") ;
            }
            if (!prev_timeprecision) prev_timeprecision = Strings::save(curr_literal) ;
        }
        if (prev_timeunit && prev_timeprecision && IsFirstLessThanSecondLiteral(prev_timeunit, prev_timeprecision)) {
            module_item->Error("error in timescale or timeprecision statement. <timeprecision> must be at least as precise as <timeunit>") ;
        }
        Strings::free(curr_literal) ;
        Strings::free(prev_timeunit) ;
        Strings::free(prev_timeprecision) ;
    }
    // Add the module item to this module's module items.
    if (!_module_items) _module_items = new Array() ;

    _module_items->InsertLast(module_item) ;

    // Now, call Resolve() on it, since that has not happened yet.
    // VIPER #3666 (left from #3370 fix): Do not resolve property/sequence declarations here.
    if (!module_item->IsPropertyDecl() && !module_item->IsSequenceDecl() && !module_item->IsLetDecl()) {
        // We will resolve property and sequence from its references with the actuals:
        module_item->Resolve(_scope, VERI_UNDEF_ENV) ; // could pass in a 'root_module_item' environment..
    }

    if (_parent_library && !_parent_library->IsTmpLibrary() && veri_file::GetCurrentUserLibrary()) {
        // VIPER #6560: Process module instantiations including bind directives similar to Resolve():
        module_item->ProcessModuleInstInLibrary((_parent_library->IsUserLibrary()) ? _parent_library : 0, _scope) ;
    }

    return 1 ; // Addition completed
}

// Destructor
VeriModule::~VeriModule()
{
    // First, detach myself from the library I am in (if any) :
    if (_parent_library) {
        // module is stored in a mibrary.
        (void) _parent_library->DetachModule(this) ;
        _parent_library = 0 ; // now we are no longer part of a library.
    } else if (_scope && _scope->Upper()) {
        // Typically, module is stored in a library.
        // But nested module are stored in a parse tree (of another module).
        // For these, _id is stored in a scope.
        // If so, undeclare them from there first :
        (void) _scope->Upper()->Undeclare(_id) ; // remove from upper scope (if there).
    }

    // If 'this' is the current root module, then clear it from veri_file to avoid corruption:
    if (this == veri_file::GetRootModule(0)) veri_file::ResetRootModule() ;

    unsigned i ;

    VeriExpression *pc ;
    FOREACH_ARRAY_ITEM(_port_connects,i,pc) delete pc ;
    delete _port_connects ;

    VeriModuleItem *mi ;
    FOREACH_ARRAY_ITEM(_package_import_decls,i,mi) delete mi ;
    delete _package_import_decls ;
    FOREACH_ARRAY_ITEM(_parameter_connects,i,mi) delete mi ;
    delete _parameter_connects ;

    FOREACH_ARRAY_ITEM(_module_items,i,mi) delete mi ;
    delete _module_items ;

    // Delete the _scope (containing all identifiers owned by this module).
    delete _scope ;
    _scope = 0 ;

    // VIPER #5954: Delete the scope for non-ansi ports
    delete _port_scope ;
    _port_scope = 0 ;

    // id was detached from a scope (if it was in one), or else removed from a library.
    // Either way, it is free now (and still allocated). So, delete it now :
    delete _id ;

    // Delete the on-the-fly-generated lists of Ports/Parameters/defparams
    delete _ports ;
    delete _parameters ;

    delete _bind_instances ;

    Strings::free(_original_module_name) ; // free the allocated string
    _original_module_name = 0 ;

    // Delete the directive settings
    Strings::free(_timescale) ; // free the allocated string
    _timescale = 0 ;

    _delay_mode_distributed = 0 ;
    _delay_mode_path = 0 ;
    _delay_mode_unit = 0 ;
    _delay_mode_zero = 0 ;
    Strings::free(_default_decay_time) ; _default_decay_time = 0 ;
    Strings::free(_default_trireg_strength) ; _default_trireg_strength = 0 ;

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
#ifdef DELETE_LINEFILES_OF_UNUSED_Y_V_MODULES
    // VIPER #5932: Delete all the linefiles which were created for this module:
    linefile_type lf ;
    FOREACH_ARRAY_ITEM(_linefile_structs, i, lf) {
        delete lf ;
    }
#endif
    delete _linefile_structs ;
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
}

const char *VeriModule::GetOriginalModuleName() const
{
    return _original_module_name ;
}
// Get the module from which this module is copied. If this is design file specific
// module, returns 0.
VeriModule *VeriModule::GetOriginalModule() const
{
    if (!_original_module_name) return 0 ; // This module is not copied module, return 0
    // This is copied module. '_original_module_name' represents the module name
    // from which this module is copied. Assuming copied module is added
    // in the same library as original module, return module from this module's
    // parent library
    if (_parent_library) {
        return _parent_library->GetModule(_original_module_name, 1) ;
    } else { // It is nested module
        // Get parent scope of this copied module
        VeriScope *upper_scope = (_scope) ? _scope->Upper() : 0 ;
        // Find original one in parent scope
        VeriIdDef *orig_mod_id = (upper_scope) ? upper_scope->FindLocal(_original_module_name) : 0 ;
        return (orig_mod_id) ? orig_mod_id->GetModule() : 0 ; // Return the module
    }
}

void VeriModule::SetPackageImportDecls(Array *decls)
{
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_package_import_decls, i, item) delete item ;
    delete _package_import_decls ;
    _package_import_decls = decls ;
}
//////////////////////////////////////////////////////////////////////
// Derived class Construction/Destruction
//////////////////////////////////////////////////////////////////////

VeriPrimitive::VeriPrimitive(VeriIdDef *id, Array *ports, Array *module_items, VeriTable *table, VeriScope *scope) :
    VeriModule(id, 0, ports, module_items, scope)
{
    // VIPER #5088 : Do not perform semnatic checks for extern primitive
    if (_id && _id->IsExtern()) return ;
    // Set the table's module back-pointer here :
    if (table) table->SetTheModule(this) ;

    // Issue 2347 : If there are ansi ports, then there might not be ANY module items.
    // So, create them any way, since we really need to add the 'table' :
    if (!_module_items) _module_items = new Array() ;

    if (!table) {
        // VIPER 3028. Users can downgrade or upgrade this message as they please.
        Warning("UDP primitive %s does not have a table",(id) ? id->Name() : "") ;
    } else {
        // For now, add the table to the module items.
        // That makes all existing routines on VeriModule work on any VeriPrimitive
        _module_items->InsertLast(table) ;
// RD: since VIPER 5182, we call Resolve when we add module to library, not already in the VeriModule constructor.
// At that point, Resolve will also run on the table (which is now added as a module item).
// So no need to call Resolve() separately here
        // table->Resolve(_scope, VERI_UNDEF_ENV) ; // Run resolve on the table
    }
}

VeriPrimitive::~VeriPrimitive()
{
    // VeriModule destructor does the work for all normal module items
}

VeriTable *
VeriPrimitive::GetPrimitiveTable() const
{
    Array *module_items = GetModuleItems() ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM_BACK(module_items, i, item) {
        if (item && item->IsTable()) return item->GetPrimitiveTable() ;
    }

    return 0 ;
}

VeriConfiguration::VeriConfiguration(VeriIdDef *id, Array *top_modules, Array *config_rules) :
    VeriModule(id, 0, 0, 0, 0),
    _top_modules(top_modules),
    _config_rules(config_rules)
{
    // It is an error to specify more than one default library list, check it:
    unsigned i ;
    VeriConfigRule *rule ;
    unsigned default_found = 0 ;
    FOREACH_ARRAY_ITEM(_config_rules, i, rule) {
        if (rule && rule->IsDefaultConfig()) {
            if (default_found) {
                Error("multiple configuration default clauses are not allowed") ;
                break ;
            }
            default_found = 1 ;
        }
    }
}
// VIPER #7416 : Configuration with local parameter declaration list
VeriConfiguration::VeriConfiguration(VeriIdDef *id, Array *local_params, Array *top_modules, Array *config_rules, VeriScope *scope) :
    VeriModule(id, 0, 0, local_params, scope),
    _top_modules(top_modules),
    _config_rules(config_rules)
{
    // It is an error to specify more than one default library list, check it:
    unsigned i ;
    VeriConfigRule *rule ;
    unsigned default_found = 0 ;
    FOREACH_ARRAY_ITEM(_config_rules, i, rule) {
        if (rule && rule->IsDefaultConfig()) {
            if (default_found) {
                Error("multiple configuration default clauses are not allowed") ;
                break ;
            }
            default_found = 1 ;
        }
    }
    VeriModuleItem *decl ;
    FOREACH_ARRAY_ITEM(local_params, i, decl) {
        if (!decl) continue ;
        if (!decl->IsLocalParamDecl()) {
            Error("only local parameter declarations are allowed in configuration") ;
        }
    }
}

VeriConfiguration::~VeriConfiguration()
{
    unsigned i ;
    VeriName *module_name ;
    FOREACH_ARRAY_ITEM(_top_modules, i, module_name) delete module_name ;
    delete _top_modules ;

    VeriConfigRule *rule ;
    FOREACH_ARRAY_ITEM(_config_rules, i, rule) delete rule ;
    delete _config_rules ;
}

// Traverse configuration items to find module names which can be treated as instantiated
void
VeriConfiguration::AccumulateInstantiatedModuleNames(Set &mod_names, Set & /*done_libs*/, Set & /*def_mods*/)
{
    unsigned i ;
    VeriName *top_mod_name ;
    const char *parent_lib_name = (_parent_library) ? _parent_library->GetName() : 0 ;
    // First insert the module names specified in the 'design' construct:
    FOREACH_ARRAY_ITEM(_top_modules, i, top_mod_name) {
        if (!top_mod_name) continue ;
        // Check whether this top module is a selected name and is from a different library:
        if (top_mod_name->IsHierName()) {
            VeriName *prefix = top_mod_name->GetPrefix() ;
            const char *lib_name = (prefix) ? prefix->GetName() : 0 ;
            // In that case we should not insert the 'design' name info the Set:
            if (!Strings::compare("work", lib_name) && !Strings::compare(parent_lib_name, lib_name)) continue ;
        }
        const char *name = top_mod_name->GetName() ;
        if (name) (void) mod_names.Insert(name) ;
    }

    VeriConfigRule *config_item ;
    // Then accumulate the instantiated module names from the configuration rules:
    FOREACH_ARRAY_ITEM(_config_rules, i, config_item) {
        if (config_item) config_item->AccumulateInstantiatedModuleNames(mod_names, parent_lib_name) ;
    }
}

Array *
VeriConfiguration::GetPorts() const
{
    VeriIdDef *module_id = GetSingleDesignModuleId(0) ;
    VeriModule *module = module_id ? module_id->GetModule() : 0 ;
    return module ? module->GetPorts() : 0 ;
}

VeriIdDef *
VeriConfiguration::GetPort(const char *name) const
{
    VeriIdDef *module_id = GetSingleDesignModuleId(0) ;
    VeriModule *module = module_id ? module_id->GetModule() : 0 ;
    return module ? module->GetPort(name) : 0 ;
}

Array *
VeriConfiguration::GetParameters() const
{
    // Viper #8022
    VeriIdDef *module_id = GetSingleDesignModuleId(0) ;
    VeriModule *module = module_id ? module_id->GetModule() : 0 ;
    return module ? module->GetParameters() : 0 ;
}

VeriIdDef *
VeriConfiguration::GetParam(const char *name) const
{
    // Viper #8022
    VeriIdDef *module_id = GetSingleDesignModuleId(0) ;
    VeriModule *module = module_id ? module_id->GetModule() : 0 ;
    return module ? module->GetParam(name) : 0 ;
}

VeriIdDef *
VeriConfiguration::GetParamAt(unsigned pos) const
{
    // Viper #8022
    VeriIdDef *module_id = GetSingleDesignModuleId(0) ;
    VeriModule *module = module_id ? module_id->GetModule() : 0 ;
    return module ? module->GetParamAt(pos) : 0 ;
}

VeriIdDef *
VeriConfiguration::GetSingleDesignModuleId(unsigned recursion_depth) const
{
    if (!_top_modules || _top_modules->Size() != 1 || recursion_depth > 10) return 0 ;

    Set *module_set = GetDesignModules() ;
    if (!module_set || !module_set->Size()) return 0 ;

    VeriModule *design_mod =  (VeriModule *)module_set->GetLast() ;
    delete module_set ;

    return design_mod ? design_mod->GetSingleDesignModuleId(recursion_depth+1) : 0 ;
}

// Returns the Set of VeriModule * of the 'design' modules
Set *
VeriConfiguration::GetDesignModules() const
{
    if (!_top_modules || !_top_modules->Size()) return 0 ;

    Set *top_mods = new Set(POINTER_HASH) ;
    VeriModule *design_mod ;
    VeriLibrary *lib ;
    VeriName *design ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_top_modules, i, design) {
        if (!design) continue ;
        // Default library is the library in which this configuration is stored
        lib = _parent_library ;
        if (design->IsHierName()) {
            // It is library_name.module_name, need to find it in the specified library:
            VeriName *prefix = design->GetPrefix() ;
            const char *lib_name = (prefix) ? prefix->GetName() : 0 ;
            // "work" means current library (_parent_library), so skip it:
            // Viper 6102: create a library if not found
            if (!Strings::compare("work", lib_name)) lib = veri_file::GetLibrary(lib_name, 1 /*create if needed*/) ;
        }
        // Find the design in the selected library:
        design_mod = (lib) ? lib->GetModule(design->GetName(), 1) : 0 ;
        if (design_mod) (void) top_mods->Insert(design_mod) ;
    }

    // Note: It cannot contain the same module multiple times if defined so.
    return top_mods ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
Set *
VeriConfiguration::GetDesignUnits() const
{
    if (!_top_modules || !_top_modules->Size()) return 0 ;

    Set *top_units = new Set(POINTER_HASH) ;
    VeriLibrary *lib ;
    VeriName *design ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_top_modules, i, design) {
        if (!design) continue ;
        // Default library is the library in which this configuration is stored
        lib = _parent_library ;
        if (design->IsHierName()) {
            // It is library_name.module_name, need to find it in the specified library:
            VeriName *prefix = design->GetPrefix() ;
            const char *lib_name = (prefix) ? prefix->GetName() : 0 ;
            // "work" means current library (_parent_library), so skip it:
            // Viper 6102: create a library if not found
            if (!Strings::compare("work", lib_name)) lib = veri_file::GetLibrary(lib_name, 1 /*create if needed*/) ;
        }
        VhdlLibrary *vhdl_lib = lib ? vhdl_file::GetLibrary(lib->GetName()) : 0 ;
        // Find the unit in the selected library:
        VhdlPrimaryUnit *vhdl_unit = vhdl_lib ? vhdl_lib->GetPrimUnit(design->GetName(), 1) : 0 ;
        if (vhdl_unit) (void) top_units->Insert(vhdl_unit) ;
    }
    // Note: It cannot contain the same module multiple times if defined so.
    return top_units ;
}
#endif

// ModuleItems for System Verilog
VeriInterface::VeriInterface(VeriIdDef *id, Array *parameter_connects, Array *port_connects, Array *items, VeriScope *scope) :
    VeriModule(id,parameter_connects,port_connects,items, scope)
{
    // All initialization already done in VeriModule constructor.
}

VeriInterface::~VeriInterface()
{
    // All members taken care of in VeriModule destructor
}

VeriProgram::VeriProgram(VeriIdDef *id, Array *parameter_connects, Array *port_connects, Array *items, VeriScope *scope) :
    VeriModule(id,parameter_connects,port_connects,items, scope)
{
    // All initialization already done in VeriModule constructor.
}

VeriProgram::~VeriProgram()
{
    // All members taken care of in VeriModule destructor
}
unsigned VeriProgram::IsAnonymousProgram() const
{
    if (!_id && !_scope) return 1 ; // No identifier, no scope, it is anonymous

    return 0 ; // normal program
}

VeriPackage::VeriPackage(VeriIdDef *id, Array *package_items, VeriScope *scope) :
    VeriModule(id,0/*params*/,0/*ports*/,package_items, scope)
{
    // All initialization already done in VeriModule constructor.

    // VIPER #4523: Do not use the root (upper) scope. Standard simulators seems
    // to do the same thing. Could not find anything in the LRM. But as depending
    // on the upper scope introduces recussion we do not depend on it as others do:
    if (_scope) {
        VeriScope *root_scope = _scope->Upper() ; // Packages are always defined in $unit
        _scope->NoLongerUsing(root_scope) ; // But they do not depend on $unit!!
        // VIPER #5426: Also remove the scope from its upper scope (and set upper scope to NULL):
        // This prohibits visibility of 'std' package here, so, implicitly import it:
        if (root_scope) root_scope->RemoveChildScope(_scope) ;

        VeriLibrary *lib = veri_file::GetWorkLib() ;
        const char *lib_name = (lib) ? lib->GetName() : 0 ;
        // Do not try to import if this is the 'std' package of library 'std' itself:
        if (!Strings::compare(lib_name, "std") || !id || !Strings::compare(id->Name(), "std")) {
            // VIPER #3231, 3232, 3233, 3234 : Get 'std' package and set that package as
            // an wildcard imported package of root module. This will make std package
            // visible to all root items and modules :

            // Get current message status
            msg_type_t VERI_1500 = Message::GetMessageType("VERI-1500") ;
            msg_type_t VERI_1501 = Message::GetMessageType("VERI-1501") ;
            msg_type_t VERI_1502 = Message::GetMessageType("VERI-1502") ;
            msg_type_t VERI_1503 = Message::GetMessageType("VERI-1503") ;
            msg_type_t VERI_1508 = Message::GetMessageType("VERI-1508") ;
            msg_type_t VERI_1509 = Message::GetMessageType("VERI-1509") ;
            // Suppress the messages produced during restoration of std package
            (void) Message::SetMessageType("VERI-1500", VERIFIC_IGNORE) ;
            (void) Message::SetMessageType("VERI-1501", VERIFIC_IGNORE) ;
            (void) Message::SetMessageType("VERI-1502", VERIFIC_IGNORE) ;
            (void) Message::SetMessageType("VERI-1503", VERIFIC_IGNORE) ;
            (void) Message::SetMessageType("VERI-1508", VERIFIC_IGNORE) ;
            (void) Message::SetMessageType("VERI-1509", VERIFIC_IGNORE) ;

            unsigned analysis_mode = veri_file::GetAnalysisMode() ;
            VeriModule *std_package = veri_file::GetModule("std", 1, "std", 1 /* restore */) ;
            veri_file::SetAnalysisMode(analysis_mode) ;

            // Restore message status back :
            if (VERI_1500==VERIFIC_NONE) Message::ClearMessageType("VERI-1500") ; else (void) Message::SetMessageType("VERI-1500", VERI_1500) ;
            if (VERI_1501==VERIFIC_NONE) Message::ClearMessageType("VERI-1501") ; else (void) Message::SetMessageType("VERI-1501", VERI_1501) ;
            if (VERI_1502==VERIFIC_NONE) Message::ClearMessageType("VERI-1502") ; else (void) Message::SetMessageType("VERI-1502", VERI_1502) ;
            if (VERI_1503==VERIFIC_NONE) Message::ClearMessageType("VERI-1503") ; else (void) Message::SetMessageType("VERI-1503", VERI_1503) ;
            if (VERI_1508==VERIFIC_NONE) Message::ClearMessageType("VERI-1508") ; else (void) Message::SetMessageType("VERI-1508", VERI_1508) ;
            if (VERI_1509==VERIFIC_NONE) Message::ClearMessageType("VERI-1509") ; else (void) Message::SetMessageType("VERI-1509", VERI_1509) ;

            if (std_package) {
                // Make 'std' package implicitly imported in root module by adding package's
                // scope in the imported list of root scope
                _scope->AddImportItem(std_package->GetScope(), 0, this) ;
            }
        }
    }
}

VeriPackage::~VeriPackage()
{
    // All members taken care of in VeriModule destructor

    // VIPER #4198 & #4523: Cleanup when the root module is being deleted:
    VeriModule *root_module = veri_file::GetRootModule(0 /* do not create */) ;
    VeriScope *root_scope = (root_module) ? root_module->GetScope() : 0 ;
    if (_scope && (root_scope == _scope)) {
        // The current root module ($unit) is being deleted, clean up:
        veri_file::ResetRootModule() ; // First reset the existing root module
        // Then start a new compilation unit that creates the root module.
        // But only do it if yacc is active, ie, we are parsing something
        // otherwise we do not need the root module ($unit) any more:
        // (veri_file::GetPresentScope() returns 0 if yacc is not active)
        //if (veri_file::GetPresentScope()) veri_file::StartCompilationUnit() ;
        // VIPER #7657: Use the following specific routine to check if the parser is active:
        if (veri_file::IsParserActive()) veri_file::StartCompilationUnit() ;
    }
}

// VIPER #5710: Constructor
VeriChecker::VeriChecker(VeriIdDef *id, Array *port_connects, Array *items, VeriScope *scope) :
    VeriModule(id,0,port_connects,items, scope)
{
    // All initialization already done in VeriModule constructor.
}
// Destructor:
VeriChecker::~VeriChecker()
{
    // All members taken care of in VeriModule destructor
}

// Add a (external) instantiation to this module. One from a bind directive :
void
VeriModule::AddBindInstance(const VeriModuleItem *bind_instance)
{
    if (!bind_instance) return ; // nothing to do

    if (!_bind_instances) _bind_instances = new Array() ;

    // VIPER 2678 : check if the instance names are free (in the scope) :
    Array *instances = bind_instance->GetIds() ;
    VeriScope *this_scope = GetScope() ;
    VeriIdDef *inst ;
    VeriIdDef *exist ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(instances, i, inst) {
        if (!inst) continue ;
        exist = (this_scope) ? this_scope->FindLocal(inst->Name()) : 0 ;
        if (exist) {
            bind_instance->Error("instance '%s' already declared in the bind target scope", inst->Name()) ;
            exist->Info("%s is declared here",exist->Name()) ;
        }
    }

#if 0  // Modev the whole checking in FindLocal VIPER 4360
    // Also VIPER 2678 : if there is a an instance in the bind_instance that is already in the bind list,
    // then  we need to error, and replace old with new one.
    VeriModuleItem *exist_bind_instance ;
    Array *exist_instances ;
    unsigned j ;
    FOREACH_ARRAY_ITEM(instances, i, inst) {
        // Check each existing binding instance to see if this one is already there (by name) :
        FOREACH_ARRAY_ITEM(_bind_instances, j, exist_bind_instance) {
            if (!exist_bind_instance) continue ;
            if (exist_bind_instance == bind_instance) return ; // this bind instance is already (by pointer) in the target module No need to do it again.
            // Get this instance identifiers (instances) :
            exist_instances = exist_bind_instance->GetIds() ;
            if (!exist_instances || (exist_instances->Size() > 1)) continue ; // cannot replace one instance in a set of them in the bind instance.
            // Get the single instance and compare to new instance :
            exist = (VeriIdDef*)exist_instances->GetFirst() ;
            if (!exist) continue ; // rule out unnamed instances.

            if (Strings::compare(exist->Name(),inst->Name())) {
                // Yeah. match by name.
                // Rule out 'unnamed' instances (they have no name or name "") :
                if (!exist->Name() || Strings::compare(exist->Name(),"")) continue ;
                // Here, definitely a name collision. Previous
                bind_instance->Error("instance '%s' already declared in the bind target scope", inst->Name()) ;
                exist->Info("%s is declared here",exist->Name()) ;
                // Remove this instance from the _bind_instances, and override with this one :
                _bind_instances->Insert(j,bind_instance) ;
                return ; // new one inserted in place of old one
            }
        }
    }

#endif
    // Insert as last back pointer into the array. Use at elaboration time.
    _bind_instances->InsertLast(bind_instance) ;
}

// Separate Resolve routines for any style module (module, interface or program),
// This routine would be called if module is called as a module item...
// That typically would only happen for System Verilog nested modules, interfaces and programs.
// Actually not sure if we should do anything, since resolving already ran during the constructor.
void
VeriModule::Resolve(VeriScope * /*scope*/, veri_environment /*environment*/)
{
    // This is 'top' of the Resolve tree. Call ResolveBody(), which will call Resolve on all constructs in the module.
    ResolveBody() ;
}

void
VeriModule::InlineDeclDataTypes()
{
    // Copy data declaration, with uniquified/inlined data_type for each identifier
    // Needed for VIPER #7277
    if (!_module_items || !_scope) return ;

    Set local_ids ;
    _scope->GetDeclaredIds(local_ids) ;

    // we intend to reuse all these ids in the inlined decls
    VeriMapForCopy id_map_table ;
    SetIter si ;
    VeriIdDef *id ;
    FOREACH_SET_ITEM(&local_ids, si, &id) if (id) (void) id_map_table.Insert(id, id) ;

    VeriModuleItem *item ;
    unsigned i ;

    Array delete_items ;
    Array modified_module_items ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (!item) continue ;

        Array *inlined_decls = item->InlineDataType(id_map_table) ;
        if (inlined_decls) {
            modified_module_items.Append(inlined_decls) ;
            delete_items.Insert(item) ;
            delete inlined_decls ;
        } else {
            modified_module_items.Insert(item) ;
        }
    }

    FOREACH_ARRAY_ITEM(&delete_items, i, item) {
        delete item ;
    }

    _module_items->Reset() ;
    _module_items->Append(&modified_module_items) ;

    ResolveBody() ;

    // PrettyPrint(std::cout, 0) ;
}

//////////////////////////////////////////////////////////////////////
// Name of the module
//////////////////////////////////////////////////////////////////////

const char *
VeriModule::Name() const
{
    return (_id) ? _id->Name() : 0 ;
}

const char *
VeriModule::GetName() const
{
    return (_id) ? _id->Name() : 0 ;
}

//////////////////////////////////////////////////////////////////////
// Find an identifier by name in this module scope
//////////////////////////////////////////////////////////////////////

VeriIdDef *
VeriModule::FindDeclared(const char *name) const
{
    return (_scope) ? _scope->FindLocal(name) : 0 ; // Do named look-up in module scope
}

//////////////////////////////////////////////////////////////////////
// Parameter support
//////////////////////////////////////////////////////////////////////

VeriIdDef *
VeriModule::GetParamAt(unsigned pos) const
{
    if (!_parameters || _parameters->Size()<=pos) return 0 ;
    return (VeriIdDef*)_parameters->At(pos) ;
}

VeriIdDef *
VeriModule::GetParam(const char *name) const
{
    if ((!_parameters && (_analysis_dialect != veri_file::VERILOG_AMS)) || !_scope) return 0 ;

    // Find the id in the scope, since that is fast :
    VeriIdDef *id = _scope->FindLocal(name) ;

    // but don't return non-parameters :
    if (id && !id->IsParam()) return 0 ;
    return id ;
}

VeriIdDef*
VeriModule::GetInterfaceFromVhdlName(const char* name, unsigned is_param /*= 1*/) const
{
    VeriIdDef* formal_id = 0 ;

    formal_id = is_param ? GetParam(name) : GetPort(name) ;

    Array* interface_arr = is_param ? _parameters : _ports ;
    if (!formal_id) {
        // Because of dual-language support, and top-level parameter support, we need
        // to make the following GetParam(...) do a case-insensitive lookup. (VIPER #1721)
        // The following is may be very slow!
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(interface_arr, i, id) {
            if (Strings::compare_nocase(id->Name(), name)) {
                formal_id = id ;
                break ;
            }
        }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        if (!formal_id) {
            // It is possible that the generic name is escaped.. Unescape it
            // and then look for it.
            char *unescaped_name = VhdlNode::UnescapeVhdlName(name) ;
            if (unescaped_name) {
                // Get rid of the front and the trailing escape characters
                // Now look for it.
                formal_id = is_param ? GetParam(unescaped_name) : GetPort(unescaped_name) ;
                if (!formal_id) {
                    // Ok.. Now case insensitive search
                    FOREACH_ARRAY_ITEM(interface_arr, i, id) {
                        if (Strings::compare_nocase(id->Name(), unescaped_name)) {
                            formal_id = id ;
                            break ;
                        }
                    }
                }
            }

            if (unescaped_name && !formal_id) {
                // Still not found.. Last try.. Escape the name
                // in verilog and then look for it...
                char *interface_name = Strings::save("\\", unescaped_name, " ") ;
                // param name is verilog escaped name..
                formal_id = is_param ? GetParam(interface_name) : GetPort(interface_name) ;
                if (!formal_id) {
                    FOREACH_ARRAY_ITEM(interface_arr, i, id) {
                        if (Strings::compare_nocase(id->Name(), interface_name)) {
                            formal_id = id ;
                            break ;
                        }
                    }
                }
                Strings::free(interface_name) ;
            }

            if (unescaped_name) Strings::free(unescaped_name) ;
        }
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
    }
    return formal_id ;
}

Map *
VeriModule::CreateActualParameters(const Map *params) const
{
    if (!params) return 0 ;

    if (!GetScope()) return 0 ; // VeriModule is corrupt

    // 'param' is a char*name->VeriConst*value Map association of parameter
    // settings, as provided by the user in another application.
    // Could be name associated, or order associated (if 'name'==0).
    //
    // Here, we check the parameters, and create an internal representation
    // that will work as 'actual_params' into the elaboration of a top module.
    // Result :   char*name->VeriConst*value.
    // The value part is allocated and needs to be cleaned up afterwards,
    // the name part is NOT allocated (points to the parameter name itself) !
    //

    Map *result = new Map(STRING_HASH) ;

    MapIter mi ;
    const char *name ;
    char *value ;

    VeriIdDef *formal_id ;
    VeriExpression *actual ;

    unsigned pos = 0 ;
    FOREACH_MAP_ITEM(params, mi, &name, &value) {
        pos++ ; // pos is one unit larger than the position of the actual

        if (!value) continue ; // OPEN ?

        // Find the parameter in this module :
        if (name) {
            formal_id = GetInterfaceFromVhdlName(name) ;
            if (!formal_id && _id) {
                // VIPER #7466: Produce warning instead of error since other tools warn out, use a different message:
                //_id->Warning("module %s does not have a parameter named %s", Name(), name) ;
                _id->Warning("module %s does not have a parameter named %s to override", Name(), name) ;
            }
        } else { // find by order
            formal_id = GetParamAt(pos-1) ;
            if (!formal_id && _id) {
                // VIPER #3406: Specify exactly the same number of arguments as the format requires:
                _id->Error("module %s does not have %d parameters", Name(), pos) ;
            }
        }
        // VIPER #7746: In case formal identifier is not found, do not ignore others, ignore only this one:
        //if (!formal_id) break ; // Error occurred
        if (!formal_id) continue ; // Error occurred

        // Figure out what kind of value this is and create proper ConstVal tree node.
        // VIPER #6594 (issue 131): Use better string to expression parsing routine:
        //actual = StringToExpression(value) ;
        actual = veri_file::AnalyzeExpr(value, _analysis_dialect) ;
        // Note: once we need IdRef's here (System Verilog), we need to set scope and run Resolve() on actual.

        if (!actual) {
            formal_id->Error("unknown literal value %s for parameter %s ignored", value, formal_id->Name()) ;
            continue ;
        }

        // Set line/file info on the actual to the formal id
        actual->SetLinefile(formal_id->Linefile()) ;

        // Insert the formal name -> actual association :
        // Since 11/2004, the key is an allocated char* :
        if (formal_id->IsType()) {
            // VIPER #8008: Convert the actual to data type when formal is type parameter
            actual->Resolve(_scope, VERI_UNDEF_ENV) ;
            VeriDataType *actual_type = actual->CreateDataType(formal_id, 0) ;
            (void) result->Insert(Strings::save(formal_id->Name()), actual_type) ;
            delete actual ;
            continue ;
        }
        (void) result->Insert(Strings::save(formal_id->Name()), actual) ;
    }

    return result ;
}

Map *
VeriConfiguration::CreateActualParameters(const Map *params) const
{
    if (!params) return 0 ;

    Map *result = 0 ; // Will return this Map
    // Flag that we have found all the parameters on all the modules
    unsigned all_params_found = 1 ;

    // Get the top level 'design' modules of this configuration:
    Set *designs = GetDesignModules() ;
    // Get the last item of the Set, we need to handle that specially:
    VeriModule *last_item = (designs && designs->Size()) ? (VeriModule *)designs->GetLast() : 0 ;
    SetIter si ;
    VeriModule *top ;
    // Create the actual parameter Maps for each of its modules:
    FOREACH_SET_ITEM(designs, si, &top) {
        if (!top) continue ;

        // Create the actual parameters for this module:
        Map *actual_params = top->CreateActualParameters(params) ;

        // Check whether we have found all the parameters in this module:
        if (actual_params && (actual_params->Size() != params->Size())) all_params_found = 0 ;

        // If this is the last of the top level 'design' modules and we have found all
        // the parameters in all the top level modules, then use/return this Map:
        if ((top == last_item) && all_params_found) {
            result = actual_params ; // Use this Map, will return it
            break ; // Do not delete, instead break out from here
        }

        // Cleanup the actual parameters Map
        // The following block of code is the inline implementation of VeriNode::DeleteDefParams.
        // This is needed since VeriNode::DeleteDefParams is currently only supported under rtl elaboration.
        MapIter mi ;
        //VeriConst *actual ;
        VeriExpression *actual ;
        char *name ;
        FOREACH_MAP_ITEM(actual_params, mi, &name, &actual) {
            Strings::free(name) ;
            delete actual ;
        }
        delete actual_params ;
    }
    delete designs ; // Delete the returned Set of top level modules

    return result ;
}

// Traverse module items recursing through generate to find instantiated module names
void VeriModule::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    unsigned i ;
    VeriModuleItem *item ;
    // Call AccumulateInstantiatedModuleNames for every module item
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (item) item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    }
}

// VIPER #5919: For instantiations in nested modules
void
VeriModule::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope * /*module_scope*/)
{
    unsigned i ;
    VeriModuleItem *item ;
    // Call ProcessModuleInstInLibrary for every module item
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (item) item->ProcessModuleInstInLibrary(lib, _scope /* pass my scope */) ;
    }
}

unsigned
VeriModule::HasInterfacePorts() const
{
    // Check if this module has 'interface' ports.
    // Essentially, that is the case if any of the ports has a data type which resolves
    // to an 'interface' or 'modport'. And we have a routine that tests that.
    unsigned i ;
    VeriIdDef *port ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        if (!port) continue ;
        if (port->IsInterfacePort()) return 1 ;
    }
    return 0 ;
}

unsigned
VeriModule::HasGenericInterfacePorts() const
{
    // Check if this module has generic 'interface' ports.
    // Essentially, that is the case if any of the ports has VERI_INTERFACE
    // as direction or type of its data type is VERI_INTERFACE it is, then,
    // a generic interface type ports. And we have a routine that tests that.
    unsigned i ;
    VeriIdDef *port ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        if (!port) continue ;
        if (port->IsGenericInterfacePort()) return 1 ;
    }
    return 0 ;
}

char *
VeriModule::GetInterfacePortsString() const
{
    // VIPER #7324: Check and return a comma separated string of interface port names
    char *module_interface_ports = 0 ;
    unsigned i ;
    VeriIdDef *port ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        if (!port) continue ;
        if (port->IsInterfacePort()) {
            if (!module_interface_ports) {
                module_interface_ports = Strings::save(port->Name()) ;
            } else {
                char *tmp = module_interface_ports ;
                module_interface_ports = Strings::save(module_interface_ports, ", ", port->Name()) ;
                Strings::free(tmp) ;
            }
        }
    }
    return module_interface_ports ; // Caller is responsible to free the returned string
}

char *
VeriModule::GetGenericInterfacePortsString() const
{
    // VIPER #7324: Check and return a comma separated string of generic interface port names
    char *module_generic_interface_ports = 0 ;
    unsigned i ;
    VeriIdDef *port ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        if (!port) continue ;
        if (port->IsGenericInterfacePort()) {
            if (!module_generic_interface_ports) {
                module_generic_interface_ports = Strings::save(port->Name()) ;
            } else {
                char *tmp = module_generic_interface_ports ;
                module_generic_interface_ports = Strings::save(module_generic_interface_ports, ", ", port->Name()) ;
                Strings::free(tmp) ;
            }
        }
    }
    return module_generic_interface_ports ; // Caller is responsible to free the returned string
}

unsigned
VeriModule::HasTypeParameters() const
{
    // Check if this module has type-parameter ports.
    unsigned i ;
    VeriIdDef *param ;
    FOREACH_ARRAY_ITEM(_parameters, i, param) {
        if (!param) continue ;
        if (param->IsType()) return 1 ;
    }
    return 0 ;
}

// VIPER #6725: Returns the linefile spanning module item space of an *empty* module.
// Note that it returns an *allocated* linefile, so avoid unnecessary calling.
// At the same time, you do not need to delete the linefile either!
// It only works under linefile-column mode, if it is not enabled, returns 0.
linefile_type
VeriModule::GetEmptyModuleItemLinefile() const
{
    linefile_type result = 0 ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (_module_items && _module_items->Size()) return 0 ; // Not an empty module

    linefile_type my_lf = Linefile() ;
    linefile_type id_lf = (_id) ? _id->Linefile() : 0 ; // Linefile of the module-id
    if (!my_lf || !id_lf) return 0 ;

    // Copy the linefile of the module-id and set as result:
    result = id_lf->Copy() ;
    result->SetLeftLine(id_lf->GetRightLine()) ;  // Set the left line at the end of module-id
    result->SetLeftCol(id_lf->GetRightCol()) ;    // Set the left col after the end of module-id
    result->SetRightLine(my_lf->GetRightLine()) ; // Same end line as "endmodule"
    result->SetRightCol(my_lf->GetRightCol()) ;   // End col at the "endmodule" keyword or after the closing label

    TextBasedDesignMod tbdm ;
    // Get the text from module-id upto endmodule:
    char *str = tbdm.GetText(result, 0 /* do not include any modification */) ;
    if (!str) return 0 ;

    // Start with the left side of the id linefile:
    unsigned line = id_lf->GetRightLine() ;
    unsigned col = id_lf->GetRightCol() ;

    // And find the position of the ; after the module id
    unsigned within_long_comment = 0 ;
    unsigned within_short_comment = 0 ;
    char *s = str ;
    while (*s) {
        if ((*s=='\n') || (*s=='\r')) { line++ ; col = 1 ; }
        else /* non new-line chars */ { col++ ; }

        if (within_long_comment) {
            // Skip long comments:
            if ((*s=='*') && (*(s+1)=='/')) { within_long_comment = 0 ; s++ ; col++ ; }
            s++ ;
            continue ;
        } else if (within_short_comment) {
            // Skip short comments:
            if ((*s=='\n') || (*s=='\r')) { within_short_comment = 0 ; }
            s++ ;
            continue ;
        }

        // CHECKME: Are we within translate/synthesis off?
        if (*s==';') {
            // Found the required start position
            result->SetLeftLine(line) ; // Set the left line at the ;
            result->SetLeftCol(col) ;   // Set the left col after the ;
            s++ ;
            continue ;
        } else if (((*s)=='e') && ((*(s+1))=='n') && ((*(s+2))=='d') &&
                   ((*(s+3))=='m') && ((*(s+4))=='o') && ((*(s+5))=='d') &&
                   ((*(s+6))=='u') && ((*(s+7))=='l') && ((*(s+8))=='e')) {
            // Found the required end position
            if (col>1) col-- ; // It should be before "endmodule", we are already at 'e', so decrease one
            result->SetRightLine(line) ; // Set the right line at the "endmodule"
            result->SetRightCol(col) ;   // Set the right col before the "endmodule"
            break ; // Done
        }

        if ((*s=='/') && (*(s+1)=='*'))      { within_long_comment = 1 ; s++ ; col++ ; }  // Go into long comment
        else if ((*s=='/') && (*(s+1)=='/')) { within_short_comment = 1 ; s++ ; col++ ; } // Go into short comment
        s++ ;
    }

    Strings::free(str) ; // Cleanup now
#endif
    return result ;
}

// VIPER #7453: Returns the linefile spanning module *empty* port declaration space.
// Note that it returns an *allocated* linefile, so avoid unnecessary calling.
// At the same time, you do not need to delete the linefile either!
// It only works under linefile-column mode, if it is not enabled, returns 0.
linefile_type
VeriModule::GetEmptyModulePortDeclLinefile() const
{
    linefile_type result = 0 ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (_port_connects && _port_connects->Size()) return 0 ; // Not an empty port module

    linefile_type my_lf = Linefile() ;
    unsigned i ;
    VeriModuleItem *item = 0 ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) if (item) break ;
    linefile_type first_item_lf = (item) ? item->Linefile() : 0 ; // Linefile of the first module-item
    linefile_type id_lf = (_id) ? _id->Linefile() : 0 ; // Linefile of the module-id
    if ((!my_lf && !first_item_lf) || !id_lf) return 0 ;

    // Copy the linefile of the module-id and set as result:
    result = id_lf->Copy() ;
    result->SetLeftLine(id_lf->GetRightLine()) ;  // Set the left line at the end of module-id
    result->SetLeftCol(id_lf->GetRightCol()) ;    // Set the left col after the end of module-id
    if (first_item_lf) {
        // Same as start of the first module item
        result->SetRightLine(first_item_lf->GetLeftLine()) ;
        result->SetRightCol(first_item_lf->GetLeftCol()) ;
    } else {
        VERIFIC_ASSERT(my_lf) ; // Must hold since we checked it above
        result->SetRightLine(my_lf->GetRightLine()) ; // Same end line as "endmodule"
        result->SetRightCol(my_lf->GetRightCol()) ;   // End col at the "endmodule" keyword or after the closing label
    }

    TextBasedDesignMod tbdm ;
    // Get the text from module-id upto endmodule:
    char *str = tbdm.GetText(result, 0 /* do not include any modification */) ;
    if (!str) return 0 ;

    // Start with the left side of the id linefile:
    unsigned line = id_lf->GetRightLine() ;
    unsigned col = id_lf->GetRightCol() ;

    // And find the position of the ; after the module id
    unsigned within_long_comment = 0 ;
    unsigned within_short_comment = 0 ;
    unsigned within_ansi_param_decl = 0 ;
    unsigned within_import_decl = 0 ;
    char *s = str ;
    while (*s) {
        if ((*s=='\n') || (*s=='\r')) { line++ ; col = 1 ; }
        else /* non new-line chars */ { col++ ; }

        if (within_long_comment) {
            // Skip long comments:
            if ((*s=='*') && (*(s+1)=='/')) { within_long_comment = 0 ; s++ ; col++ ; }
            s++ ;
            continue ;
        } else if (within_short_comment) {
            // Skip short comments:
            if ((*s=='\n') || (*s=='\r')) { within_short_comment = 0 ; }
            s++ ;
            continue ;
        } else if (within_ansi_param_decl) {
            if (*s==')') within_ansi_param_decl = 0 ;
            s++ ;
            continue ;
        } else if (within_import_decl) {
            if (*s==';') within_import_decl = 0 ;
            s++ ;
            continue ;
        }

        // CHECKME: Are we within translate/synthesis off?
        if (*s=='(') {
            result->SetLeftLine(line) ;  // Set the left line at the (
            result->SetLeftCol(col) ;    // Set the left col after the (
            if (*(s+1)=='\n' || *(s+1)=='\r') { line++ ; col = 1 ; } // Go to next line
            result->SetRightLine(line) ; // Set the right line at the )
            result->SetRightCol(col) ; // Set the right col after the )
            break ;
        } else if (*s==';') {
            // Found the required position (start and end)
            if (col>1) col-- ;           // We already increased by one, adjust
            result->SetLeftLine(line) ;  // Set the left line at the ;
            result->SetLeftCol(col) ;    // Set the left col at the ;
            result->SetRightLine(line) ; // Set the right line at the ;
            result->SetRightCol(col) ;   // Set the right col at the ;
            break ;
        }

        if ((*s=='/') && (*(s+1)=='*'))      { within_long_comment = 1 ; s++ ; col++ ; }  // Go into long comment
        else if ((*s=='/') && (*(s+1)=='/')) { within_short_comment = 1 ; s++ ; col++ ; } // Go into short comment
        else if (*s=='#')                    { within_ansi_param_decl = 1 ; s++ ; col++ ; } // Ignore ANSI parameters
        else if (IsSystemVeri() && (0==strncmp(s, "import", 6))) { within_import_decl = 1 ; s++ ; col++ ; } // Ignore SV 'import' declarations
        s++ ;
    }

    Strings::free(str) ; // Cleanup now
#endif
    return result ;
}

//////////////////////////////////////////////////////////////////////
// Text Based Design Manipulation API routines.
//////////////////////////////////////////////////////////////////////

// These routines manipulate the source file text via the TBDM object (and methods therein).
unsigned
VeriModule::TbdmAddPort(TextBasedDesignMod &tbdm, const char *name, const char *direction, const char *data_type, const char *packed_range, const char *unpacked_range) const
{
    // add port with name "name", direction ("input, output, inout"), type ("reg", "wire" etc, packed_range ("[6:0]" or "[size-1:0]") (printed before identifier), unpacked_range ("[6:0]" or "[size-1:0]") (printed after identifier). Return 0 for failure, non-0 for success.
    if (!name) return 0 ; // No name for the port. Can't create.

    // TBDM sanity check : check if identifier is already declared ?

    // Determine the style of port declaration in module (ansi or 95)
    // Get the last port expression from port list of module
    VeriExpression *last_port = (_port_connects && _port_connects->Size()) ? (VeriExpression*)_port_connects->GetLast() : 0 ;

    if (!last_port) {
        Error ("TBDM: cannot insert %s, since module has an empty port list",name) ;
        return 0 ;
    }

    // Check whether first port expression is port declaration/expression
    if (last_port->IsAnsiPortDecl()) { // Ansi style port declaration
        // Create a ansi port decl text :
        char *image = Strings::save(", ",direction," ",(data_type)?data_type:"",(packed_range)?packed_range:" ",name/*,unpacked_range*/) ;

        // And insert after the last port :
        (void) tbdm.InsertAfter(last_port->Linefile(), image) ;
        Strings::free(image) ;
    } else { // 95 style port declaration

        // Add the port expression to the port list :
        char *image = Strings::save(", ",name) ;
        (void) tbdm.InsertAfter(last_port->Linefile(),image) ;
        Strings::free(image) ;

        // Now find the proper spot to insert the port declaration.
        // Choose to insert after the last declaration item (skip all declarations) :
        VeriModuleItem *last_decl = 0 ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_module_items,i,item) {
            if (!item) continue ;
            // break if this is not a declaration :
            if (last_decl && !item->IsDataDecl()) break ;
            // Set last decl to this item :
            last_decl = item ;
        }

        // Now, 'last_decl' points to the last declaration item. Insert after this :
        if (!last_decl) {
            Error ("TBDM: cannot insert %s, since module has no module items",name) ;
            return 0 ;
        }

        // VIPER #6394: Add the net decl before the port decl. TBDM::InsertAfter
        // work in such a way that last inserted item is put first.
        // and create the net decl for this port :
        if (data_type) {
            // Only do this if datatype has be specified, otherwise we output a wrong design:
            image = Strings::save("\n",data_type,(packed_range)?packed_range:" ",name,(unpacked_range)?unpacked_range:""," ;") ;
            (void) tbdm.InsertAfter(last_decl->Linefile(), image) ;
            Strings::free(image) ;
        }

        // Create a port decl text :
        if (!data_type && (packed_range || unpacked_range)) {
            // Use the dimensions in the io-decl in case data type has not be specified:
            image = Strings::save("\n",direction," ",(packed_range)?packed_range:" ",name,(unpacked_range)?unpacked_range:""," ; ",veri_file::GetTbdmCommentString()) ;
        } else {
            image = Strings::save("\n",direction," ",name," ; ",veri_file::GetTbdmCommentString()) ;
        }

        // And insert after the last declaration :
        (void) tbdm.InsertAfter(last_decl->Linefile(), image) ;
        Strings::free(image) ;
    }
    return 1 ;
}

unsigned
VeriModule::TbdmAddNet(TextBasedDesignMod &tbdm, const char *name, const char *data_type, const char *packed_range, const char *unpacked_range) const
{
    // add net with name "name", type ("reg", "wire" etc), packed_range ("[6:0]" or "[size-1:0]") (printed before identifier), unpacked_range ("[6:0]" or "[size-1:0]") (printed after identifier). Return 0 for failure, non-0 for success.

    // find the proper spot to insert the port declaration.
    // Choose to insert after the last declaration item (skip all declarations) :
    VeriModuleItem *last_decl = 0 ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items,i,item) {
        if (!item) continue ;
        // break if this is not a declaration :
        if (last_decl && !item->IsDataDecl()) break ;
        // Set last decl to this item :
        last_decl = item ;
    }

    // Now, 'last_decl' points to the last declaration item. Insert after this :
    if (!last_decl) {
        Error ("TBDM: cannot insert %s, since module has no module items",name) ;
        return 0 ;
    }

    // Create a net decl text :
    char *image = Strings::save("\n",data_type,(packed_range)?packed_range:" ",name,unpacked_range," ; ",veri_file::GetTbdmCommentString()) ;
    // and insert after the last declaration :
    (void) tbdm.InsertAfter(last_decl->Linefile(), image) ;
    Strings::free(image) ;

    return 1 ;
}

unsigned
VeriModule::TbdmAddInstance(TextBasedDesignMod &tbdm, const char *module_name, const char *instance_name, const char * /*param_list*/, const char *port_list) const
{
    if (!module_name || !instance_name) return 0 ;
    // add instance with name "name" and parameter list (comma-separated list with ()'s), port list (comma-separated list with ()s). Return 0 for failure, non-0 for success.

    // find the proper spot to insert the port declaration.
    // Do not insert after last decl item, instead choose to insert after the last module item.
    // The reason is if we add some decl and then add an instance, the instance is written
    // first and then the decl since TBDM operations are LIFO. Iterate module items from the back side:
    unsigned i ;
    VeriModuleItem *last_item = 0 ;
    FOREACH_ARRAY_ITEM_BACK(_module_items,i,last_item) {
        if (last_item) break ;
    }

    // Now, 'last_item' points to the last declaration item. Insert after this :
    if (!last_item) {
        Error ("TBDM: cannot insert %s, since module has no module items",instance_name) ;
        return 0 ;
    }

    // Create a inst decl text :
    char *image = Strings::save("\n",module_name," ",instance_name," ",(port_list)?port_list:"(); ",veri_file::GetTbdmCommentString()) ; // FIX ME param_list.
    // and insert after the last declaration :
    (void) tbdm.InsertAfter(last_item->Linefile(), image) ;
    Strings::free(image) ;

    return 1 ;
}

unsigned
VeriModule::TbdmAddPortRef(TextBasedDesignMod &tbdm, const char *instance_name, const char *actual_expr, const char *port_name) const
{
    if (!instance_name) return 0 ;
    // add portref with name "name" and port list (comma-separated list with ()s). Return 0 for failure, non-0 for success.

    VeriIdDef *inst_id = FindDeclared(instance_name) ;
    if (!inst_id) {
        Error ("TBDM: cannot find %s in module %s",instance_name,Name()) ;
        return 0 ;
    }

    // Add port reference. Always add at the end of the port connection list of this instance
    Array *portrefs = inst_id->GetPortConnects() ;
    if (!portrefs || !portrefs->Size()) {
        Error ("TBDM: %s has no port connections",instance_name) ;
        return 0 ;
    }

    VeriExpression *last_port_connect = (VeriExpression*)portrefs->GetLast() ;
    if (!last_port_connect) {
        Error ("TBDM: %s has no port connections",instance_name) ;
        return 0 ;
    }

    // Now create the image for the new port connect :
    char *image = 0 ;
    if (port_name) {
        // Create 'named' port reference
        image = Strings::save(",.",port_name,veri_file::GetTbdmCommentString()," (",actual_expr,") ") ;
    } else {
        // Add as 'ordered' port reference at the end
        image = Strings::save(", ",veri_file::GetTbdmCommentString()," ",actual_expr) ;
    }
    // and insert after the last port connection :
    (void) tbdm.InsertAfter(last_port_connect->Linefile(), image) ;

    Strings::free(image) ;
    return 1 ;
}

unsigned
VeriModule::TbdmChangePortRef(TextBasedDesignMod &tbdm, const char *instance_name, const char *actual_expr, const char *port_name, unsigned order_nr) const
{
    // Change portref in instance "instance_name", with portname "port_name" or the port_connect at order index order_nr.
    VeriIdDef *inst_id = FindDeclared(instance_name) ;
    if (!inst_id) {
        Warning ("TBDM: cannot find %s in module %s",instance_name,Name()) ;
        return 0 ;
    }

    // Add port reference. Always add at the end of the port connection list of this instance
    Array *portrefs = inst_id->GetPortConnects() ;
    if (!portrefs || !portrefs->Size()) {
        Error ("TBDM: %s has no port connections",instance_name) ;
        return 0 ;
    }

    // Now find the port reference :
    VeriExpression *port_connect = 0 ;
    if (port_name) {
        // Find port connect by formal port name :
        int i ;
        FOREACH_ARRAY_ITEM(portrefs,i,port_connect) {
            if (!port_connect) continue ;
            if (Strings::compare(port_connect->NamedFormal(),port_name)) {
                break ; // port_connect found
            }
        }
    } else {
        // Find port connect by order :
        port_connect = (VeriExpression*)portrefs->At(order_nr) ;
    }

    if (!port_connect) {
        Error ("TBDM: %s has no port connect %s",instance_name,(port_name)?port_name:"at this order nr") ;
        return 0 ;
    }

    // Now create the image for the new port connect :
    char *image = 0 ;
    if (port_name) {
        // Create 'named' port reference
        image = Strings::save(".",port_name,veri_file::GetTbdmCommentString()," (",actual_expr,")") ;
    } else {
        // Add as 'ordered' port reference at the end
        image = Strings::save(veri_file::GetTbdmCommentString()," ",actual_expr) ;
    }
    // and change the port connection :
    (void) tbdm.Replace(port_connect->Linefile(), image) ;

    Strings::free(image) ;
    return 1 ;
}

// Remove routines to be added later.
unsigned
VeriModule::TbdmRemovePort(TextBasedDesignMod &tbdm, const char *name) const
{
    // VIPER #7036: remove text for an existing port (note: can only remove ports in original text). Return 0 for failure, non-zero for success.

    // FIXEME: There can be multiple declarations of a port like: output o1, o2 ; reg o1 ;
    // Cannot remove both of o1, since we have a single id for a port id and
    // hence a single linefile for it. Even if we can find both the declarations.
    // Note that we can remove: output o1 ; reg o1 ; ie, all the declarations
    // have a single id and we remove the full declarations in that case.

    if (!name) return 0 ; // Nothing to remove

    // Get the declared port from module
    VeriIdDef *port_id = FindDeclared(name) ;
    if (!port_id || !port_id->IsPort()) return 0 ; // No port of name 'name' exists, return 0

    // Get the first port expression from port list of module
    unsigned i ;
    VeriExpression *first_port = 0 ;
    FOREACH_ARRAY_ITEM(_port_connects, i, first_port) if (first_port) break ; // Found the first non-NULL port

    if (!first_port) return 0 ; // No port in this module?

    // Determine the style of port declaration in module (ansi or 95)
    VeriExpression *port_expr ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    VeriExpression *prev_port_expr = 0 ;
#endif
    linefile_type remove_lf = 0 ;
    if (first_port->IsAnsiPortDecl()) { // Ansi style port declaration
        // Find VeriAnsiPortDecl containing 'name'
        FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) { // Iterate over port list
            if (!port_expr) continue ;
            Array *ids = port_expr->GetIds() ; // Get identifier list
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            VeriIdDef *prev_id = 0 ;
#endif
            unsigned j ;
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over port ids
                if (!id) continue ;
                if (id == port_id) {
                    // Found the port to be removed:
                    if (ids->Size() == 1) { // Id list contains only one id
                        // So here, we need to remove the full AnsiPortDecl object
                        remove_lf = port_expr->Linefile() ;
                        if (!remove_lf) return 0 ; // No linefile info, cannot remove!
                        // If the module has a single ANSI port, we are done, otherwise:
                        if (_port_connects->Size() != 1) {
                            // There are multiple AnsiPortDecls, need to remove previous/next comma too
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                            remove_lf = remove_lf->Copy() ; // Copy the linefile, we need to change it
                            if (prev_port_expr) {
                                linefile_type lf = prev_port_expr->Linefile() ;
                                if (lf) {
                                    // Remove from end of previous ANSI port object:
                                    remove_lf->SetLeftLine(lf->GetRightLine()) ;
                                    remove_lf->SetLeftCol(lf->GetRightCol()) ;
                                }
                            } else {
                                VeriExpression *next_port_expr = (_port_connects->Size() > (i+1)) ? (VeriExpression *)_port_connects->At(i+1) : 0 ;
                                linefile_type lf = (next_port_expr) ? next_port_expr->Linefile() : 0 ;
                                if (lf) {
                                    // Remove upto next ANSI port object:
                                    remove_lf->SetRightLine(lf->GetLeftLine()) ;
                                    remove_lf->SetRightCol(lf->GetLeftCol()) ;
                                }
                            }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
                        }
                    } else { // VeriAnsiPortDecl has multiple ports
                        remove_lf = id->Linefile() ;
                        if (!remove_lf) return 0 ; // No linefile info, cannot remove!
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                        remove_lf = remove_lf->Copy() ; // Copy the linefile, we need to change it
                        if (prev_id) {
                            linefile_type lf = prev_id->Linefile() ;
                            if (lf) {
                                // Remove from end of previous port id:
                                remove_lf->SetLeftLine(lf->GetRightLine()) ;
                                remove_lf->SetLeftCol(lf->GetRightCol()) ;
                            }
                        } else {
                            VeriIdDef *next_id = (ids->Size() > (j+1)) ? (VeriIdDef *)ids->At(j+1) : 0 ;
                            linefile_type lf = (next_id) ? next_id->Linefile() : 0 ;
                            if (lf) {
                                // Remove upto next port id:
                                remove_lf->SetRightLine(lf->GetLeftLine()) ;
                                remove_lf->SetRightCol(lf->GetLeftCol()) ;
                            }
                        }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
                    }
                    return tbdm.Remove(remove_lf) ; // Remove the ANSI port and we are done!
                }
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                prev_id = id ;
#endif
            }
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            prev_port_expr = port_expr ;
#endif
        }

        return 0 ; // If we are here, then failed to find/remove ANSI port declaration
    }

    // Here, it is 95 style port declaration
    // Find 'name' defined port expression in port list
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (!port_expr) continue ;
        if (port_expr->GetId() == port_id) { // Found port expression
            remove_lf = port_expr->Linefile() ;
            if (!remove_lf) return 0 ; // No linefile info, cannot remove!
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            remove_lf = remove_lf->Copy() ; // Copy the linefile, we need to change it
            if (prev_port_expr) {
                linefile_type lf = prev_port_expr->Linefile() ;
                if (lf) {
                    // Remove from end of previous ANSI port object:
                    remove_lf->SetLeftLine(lf->GetRightLine()) ;
                    remove_lf->SetLeftCol(lf->GetRightCol()) ;
                }
            } else {
                VeriExpression *next_port_expr = (_port_connects->Size() > (i+1)) ? (VeriExpression *)_port_connects->At(i+1) : 0 ;
                linefile_type lf = (next_port_expr) ? next_port_expr->Linefile() : 0 ;
                if (lf) {
                    // Remove upto next ANSI port object:
                    remove_lf->SetRightLine(lf->GetLeftLine()) ;
                    remove_lf->SetRightCol(lf->GetLeftCol()) ;
                }
            }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
            // Found the linefile location to remove, will actually remove it later below:
            break ; // No need to iterate further
        }
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        prev_port_expr = port_expr ;
#endif
    }

    if (!remove_lf) return 0 ; // Cannot find the port in the port-list!

    // Now first remove the wire/reg, input/output declaration. We will remove the port from
    // the port list only if we can remove the data/io decl. The reason is there can be multiple
    // delclarations with multiple ids of the port and we do not remove any in that case:
    if (TbdmRemoveNet(tbdm, name)) return tbdm.Remove(remove_lf) ; // Remove the port expression

    return 0 ;
}

unsigned
VeriModule::TbdmRemoveNet(TextBasedDesignMod &tbdm, const char *name) const
{
    // VIPER #7036: remove text for an existing port (note: can only remove ports in original text). Return 0 for failure, non-zero for success.
    if (!name) return 0 ; // Nothing to remove

    VeriIdDef *net_id = FindDeclared(name) ;
    if (!net_id || !net_id->IsVar()) return 0 ; // No net with the given name

    // Find I/O declaration of 'name' in module item list
    Array remove_lfs(2) ; // Store the locations to be removed
    // Mark if we remove only identifiers (for decl with multiple ids), not the whole decl:
    unsigned remove_only_id = 0 ;
    linefile_type remove_lf ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module items
        if (!item) continue ;
        Array *ids = (item->IsDataDecl()) ? item->GetIds() : 0 ;
        // Multiple data decl can have 'net_id'
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        VeriIdDef *prev_id = 0 ;
#endif
        unsigned j ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over port ids
            if (!id) continue ;
            if (id == net_id) { // Found id
                if (ids->Size() > 1) remove_only_id = 1 ; // Multiple ids declared, need to remove only this id

                if (remove_lfs.Size() && remove_only_id) {
                    // Here, multiple declarations of the id found and at-least one of the declarations
                    // have multiple ids in it, cannot remove the identifier in that case.
                    item->Error("TBDM: cannot remove %s having multiple declarations with multiple ids", name) ;
                    VeriTreeNode tmp((linefile_type)remove_lfs.GetFirst()) ; tmp.Info("%s is declared here", name) ;
                    return 0 ; // This is another declaration and the id is already declared with others, cannot remove
                }

                if (ids->Size() == 1) {
                    // Declaration contains one id: remove the whole item:
                    // FIXEME: There can be multiple declarations like: output o1, o2 ; reg o1 ;
                    // Cannot remove both declarations of o1, since we have a single id/linefile.
                    remove_lf = item->Linefile() ;
                    if (!remove_lf) return 0 ; // No linefile info, cannot remove!
                    remove_lfs.InsertLast((void *)remove_lf) ;
                    break ; // Will check multiple declarations later when found
                }

                // Declaration contains multiple ids
                remove_lf = id->Linefile() ;
                if (!remove_lf) return 0 ; // No linefile info, cannot remove!
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                remove_lf = remove_lf->Copy() ; // Copy the linefile, we need to change it
                if (prev_id) {
                    linefile_type lf = prev_id->Linefile() ;
                    if (lf) {
                        // Remove from end of previous port id:
                        remove_lf->SetLeftLine(lf->GetRightLine()) ;
                        remove_lf->SetLeftCol(lf->GetRightCol()) ;
                    }
                } else {
                    VeriIdDef *next_id = (ids->Size() > (j+1)) ? (VeriIdDef *)ids->At(j+1) : 0 ;
                    linefile_type lf = (next_id) ? next_id->Linefile() : 0 ;
                    if (lf) {
                        // Remove upto next port id:
                        remove_lf->SetRightLine(lf->GetLeftLine()) ;
                        remove_lf->SetRightCol(lf->GetLeftCol()) ;
                    }
                }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
                remove_lfs.InsertLast((void *)remove_lf) ;
                // FIXEME: There can be multiple declarations like: output o1, o2 ; reg o1 ;
                // Cannot remove both of declarations o1, since we have a single id/linefile.
                break ; // Will check multiple declarations later when found
            }
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            prev_id = id ;
#endif
        }
    }

    // Now, actually remove the locations (note that there can be multiple locations):
    unsigned result = 0 ;
    FOREACH_ARRAY_ITEM(&remove_lfs, i, remove_lf) result |= tbdm.Remove(remove_lf) ;

    return result ;
}

unsigned
VeriModule::TbdmRemoveInstance(TextBasedDesignMod &tbdm, const char *name) const
{
    // VIPER #7036: remove text for an existing instance (note: can only remove instances in original text). Return 0 for failure, non-zero for success.
    if (!name) return 0 ; // Nothing to remove

    // Get the instance identifier
    VeriIdDef *instance_id = FindDeclared(name) ;
    if (!instance_id || !instance_id->IsInst()) return 0 ; // No instance exists with the given name

    // Find module instantiation having instance from module item list
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) { // Iterate over module items
        if (!item) continue ;
        Array *ids = (item->IsInstantiation() || item->GetInstType()) ? item->GetIds() : 0 ; // Get id list of each instantiation
        unsigned j ;
        VeriInstId *id ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
        VeriInstId *prev_id = 0 ;
#endif
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over instance identifiers
            if (id == instance_id) {
                // Found desired id
                if (ids->Size() == 1) { // Instantiation contains one instance
                    // Single instance id: remove the whole instantiation:
                    return tbdm.Remove(item->Linefile()) ;
                }

                // Instantiation contains multiple ids
                linefile_type remove_lf = id->Linefile() ;
                if (!remove_lf) return 0 ; // No linefile info, cannot remove!
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                remove_lf = remove_lf->Copy() ; // Copy the linefile, we need to change it
                if (prev_id) {
                    linefile_type lf = prev_id->Linefile() ;
                    if (lf) {
                        // Remove from end of previous instance id:
                        remove_lf->SetLeftLine(lf->GetRightLine()) ;
                        remove_lf->SetLeftCol(lf->GetRightCol()) ;
                    }
                } else {
                    VeriIdDef *next_id = (ids->Size() > (j+1)) ? (VeriIdDef *)ids->At(j+1) : 0 ;
                    linefile_type lf = (next_id) ? next_id->Linefile() : 0 ;
                    if (lf) {
                        // Remove upto next instance id:
                        remove_lf->SetRightLine(lf->GetLeftLine()) ;
                        remove_lf->SetRightCol(lf->GetLeftCol()) ;
                    }
                }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS
                return tbdm.Remove(remove_lf) ;
            }
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            prev_id = id ;
#endif
        }
    }

    return 0 ;
}

unsigned
VeriModule::TbdmRemovePortRef(TextBasedDesignMod &tbdm, const char *instance_name, const char *port_name, unsigned order_nr) const
{
    // VIPER #7036: remove portref for an existing instance (note: can only remove instances in original text). Return 0 for failure, non-zero for success.
    if (!instance_name) return 0 ; // Impossible to remove, no instance name given

    // Find instance id named 'instance_name'
    VeriIdDef *instance_id = FindDeclared(instance_name) ;
    if (!instance_id || !instance_id->IsInst()) return 0 ; // No instance exists with the given name

    // Get connection list of instantiation
    Array *port_connects = instance_id->GetPortConnects() ;
    if (!port_connects || !port_connects->Size()) return 0 ; // No port connection list, failed to remove

    VeriModuleItem *the_instance = instance_id->GetModuleInstance() ;
    if (port_name) {
        if (!the_instance) return 0 ; // This must be a gate instantiation: cannot remove by port name

        if (instance_id->HasOrderedPortAssoc()) {
            // Need to find the position in the port connects
            VeriModule *inst_mod = the_instance->GetInstantiatedModule() ;
            Array *ports = (inst_mod) ? inst_mod->GetPorts() : 0 ;
            unsigned i ;
            VeriIdDef *port ;
            FOREACH_ARRAY_ITEM(ports, i, port) {
                if (!port) continue ;
                if (Strings::compare(port->Name(), port_name)) { // Found desired formal
                    order_nr = i ;
                    break ;
                }
            }
        } else if (instance_id->HasNamedPortAssoc()) {
            unsigned i ;
            VeriExpression *conn ;
            FOREACH_ARRAY_ITEM(port_connects, i, conn) {
                if (!conn) continue ;
                if (Strings::compare(conn->NamedFormal(), port_name)) {
                    order_nr = i ;
                    break ;
                }
            }
        }
    }

    if (order_nr >= port_connects->Size()) return 0 ; // Cannot remove, no connection at this position

    VeriExpression *conn = (VeriExpression *)port_connects->At(order_nr) ;
    linefile_type remove_lf = (conn) ? conn->Linefile() : 0 ;
    if (!remove_lf) return 0 ; // Cannot remove, no linefile info

    if (!order_nr && (port_connects->Size()==1)) return tbdm.Remove(remove_lf) ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    remove_lf = remove_lf->Copy() ;
    if (!order_nr) {
        VERIFIC_ASSERT(port_connects->Size() > 1) ;
        VeriExpression *next = (VeriExpression *)port_connects->At(1 /* order_nr+1 */) ;
        linefile_type lf = (next) ? next->Linefile() : 0 ;
        if (lf) {
            // Remove upto next next port-ref
            remove_lf->SetRightLine(lf->GetLeftLine()) ;
            remove_lf->SetRightCol(lf->GetLeftCol()) ;
        }
    } else {
        VeriExpression *prev = (VeriExpression *)port_connects->At(order_nr-1) ;
        linefile_type lf = (prev) ? prev->Linefile() : 0 ;
        if (lf) {
            // Remove from end of previous instance id:
            remove_lf->SetLeftLine(lf->GetRightLine()) ;
            remove_lf->SetLeftCol(lf->GetRightCol()) ;
        }
    }
#endif // VERIFIC_LINEFILE_INCLUDES_COLUMNS

    return tbdm.Remove(remove_lf) ;
}

//////////////////////////////////////////////////////////////////////
// Parse tree Manipulation API routines.
//////////////////////////////////////////////////////////////////////

void
VeriModule::AddPort(const char *dir, const char *port_name)
{
    if (!port_name) return ;

    unsigned dir_token = 0 ;
    if (Strings::compare(dir,"input")) dir_token = VERI_INPUT ;
    else if (Strings::compare(dir,"output")) dir_token = VERI_OUTPUT ;
    else if (Strings::compare(dir,"inout")) dir_token = VERI_INOUT ;

    if (!dir_token) {
        Warning("unknown port direction %s found for port %s. Assume input",dir,port_name) ;
        dir_token = VERI_INPUT ;
    }

    (void) AddPort(port_name, dir_token, new VeriDataType(0, 0, 0)) ;
}

void
VeriModule::AddPort(const char *dir, const char *port_name, int left, int right)
{
    if (!port_name) return ;

    unsigned dir_token = 0 ;
    if (Strings::compare(dir,"input"))       dir_token = VERI_INPUT ;
    else if (Strings::compare(dir,"output")) dir_token = VERI_OUTPUT ;
    else if (Strings::compare(dir,"inout"))  dir_token = VERI_INOUT ;

    if (!dir_token) {
        Warning("unknown port direction %s found for port %s. Assume input",dir,port_name) ;
        dir_token = VERI_INPUT ;
    }

    (void) AddPort(port_name, dir_token, new VeriDataType(0, 0, new VeriRange(new VeriIntVal(left), new VeriIntVal(right)))) ;
}

void
VeriModule::AddSignal(const char *type, const char *name)
{
    if (!name) return ;

    unsigned type_token = 0 ;
    // net-types :
    if (Strings::compare(type,"wire"))         type_token = VERI_WIRE ;
    else if (Strings::compare(type,"supply0")) type_token = VERI_SUPPLY0 ;
    else if (Strings::compare(type,"supply1")) type_token = VERI_SUPPLY1 ;
    else if (Strings::compare(type,"wand"))    type_token = VERI_WAND ;
    else if (Strings::compare(type,"wor"))     type_token = VERI_WOR ;
    else if (Strings::compare(type,"tri"))     type_token = VERI_TRI ;
    else if (Strings::compare(type,"tri1"))    type_token = VERI_TRI1 ;
    else if (Strings::compare(type,"trior"))   type_token = VERI_TRIOR ;
    else if (Strings::compare(type,"triand"))  type_token = VERI_TRIAND ;
    else if (Strings::compare(type,"trireg"))  type_token = VERI_TRIREG ;
    // variable types
    else if (Strings::compare(type,"reg"))     type_token = VERI_REG ;
    else if (Strings::compare(type,"integer")) type_token = VERI_INTEGER ;
    else if (Strings::compare(type,"time"))    type_token = VERI_TIME ;
    else if (Strings::compare(type,"real"))    type_token = VERI_REAL ;

    if (!type_token) {
        Warning("unknown signal type %s found for added signal %s. Assume wire",type,name) ;
        type_token = VERI_WIRE ;
    }

    (void) AddSignal(name, new VeriDataType(type_token, 0, 0)) ;
}

void
VeriModule::AddSignal(const char *type, const char *name, int left, int right)
{
    if (!name) return ;

    unsigned type_token = 0 ;
    // net-types :
    if (Strings::compare(type,"wire"))         type_token = VERI_WIRE ;
    else if (Strings::compare(type,"supply0")) type_token = VERI_SUPPLY0 ;
    else if (Strings::compare(type,"supply1")) type_token = VERI_SUPPLY1 ;
    else if (Strings::compare(type,"wand"))    type_token = VERI_WAND ;
    else if (Strings::compare(type,"wor"))     type_token = VERI_WOR ;
    else if (Strings::compare(type,"tri"))     type_token = VERI_TRI ;
    else if (Strings::compare(type,"tri1"))    type_token = VERI_TRI1 ;
    else if (Strings::compare(type,"trior"))   type_token = VERI_TRIOR ;
    else if (Strings::compare(type,"triand"))  type_token = VERI_TRIAND ;
    else if (Strings::compare(type,"trireg"))  type_token = VERI_TRIREG ;
    // variable types
    else if (Strings::compare(type,"reg"))     type_token = VERI_REG ;
    else if (Strings::compare(type,"integer")) type_token = VERI_INTEGER ;
    else if (Strings::compare(type,"time"))    type_token = VERI_TIME ;
    else if (Strings::compare(type,"real"))    type_token = VERI_REAL ;

    if (!type_token) {
        Warning("unknown signal type %s found for added signal %s. Assume wire",type,name) ;
        type_token = VERI_WIRE ;
    }

    (void) AddSignal(name, new VeriDataType(type_token, 0, new VeriRange(new VeriIntVal(left), new VeriIntVal(right)))) ;
}

void
VeriModule::AddPortRef(const char *inst_name, const char *formal, const char *actual)
{
    if (!inst_name || !formal || !actual) return ;

    VeriIdDef *instance_id = FindDeclared(inst_name) ;
    if (!instance_id || !instance_id->IsInst()) return ;

    VeriExpression *actual_expr = veri_file::AnalyzeExpr(actual) ;
    if (!actual_expr) return ;

    // get rid of existing connection, if any
    (void) RemovePortRef(inst_name, formal) ;

    // add the new connextion
    (void) AddPortRef(inst_name, formal, actual_expr) ;
}

void
VeriModule::AddAssignment(const char *lhs, const char *rhs)
{
    if (!lhs || !rhs) return ;

    VeriExpression *lhs_expr = veri_file::AnalyzeExpr(lhs) ;
    VeriExpression *rhs_expr = veri_file::AnalyzeExpr(rhs) ;
    if (!lhs_expr || !rhs_expr) return ;

    Array *assign_array = new Array(1) ;
    assign_array->Insert(new VeriNetRegAssign(lhs_expr, rhs_expr)) ;

    (void) AddModuleItem(new VeriContinuousAssign(0, 0, assign_array)) ;
}

//////////////////////////////////////////////////////////////////////
// Generate a VHDL 'entity' from a Verilog module.
//////////////////////////////////////////////////////////////////////

// Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
// This routine creates a VHDL entity(without any architecture) for the VeriModule*
// passed to it. On successful creation of the entity it sets the VhdlDesignUnit::_is_verilog_module
// flag to 1 and also stores the original module name in VhdlDesignUnit::_orig_design_unit variable.
// This routine currently works for Verilog 95 only.
VhdlPrimaryUnit*
VeriModule::ConvertModule(const char *lib_name) const
{
    // Viper 4611: take the lib_name from verilog module
    const char *library_name = _parent_library ? _parent_library->GetName() : lib_name ;
    if (!library_name) return 0 ; // If no such module or library exist then return 0
    VhdlEntityDecl *unit = 0 ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION

    /* First, reset the scope to 0, then create a new scope for the library unit */
    VhdlScope *save_scope = VhdlTreeNode::_present_scope ;
    VhdlTreeNode::_present_scope = 0 ;
    VhdlScope::Push() ;

    /* Now create a 'context_clause' tree node (an Array of context items). */
    Array *context_clause = new Array() ;

    /* Define library names 'STD' and 'WORK' */
    Array *libs = new Array() ;

    /* Declare library identifier 'std' */
    VhdlIdDef *id = new VhdlLibraryId(Strings::save("std")) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    /* Declare library identifier for work library */
    id = new VhdlLibraryId(Strings::save(library_name)) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    /* Declare library identifier 'vl' */
    // This library is the one used by Modelsim to declare variables 'vl_logic', 'vl_logic_vector', etc
    id = new VhdlLibraryId(Strings::save("vl")) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    /* Set them in the parse tree */
    context_clause->InsertLast(new VhdlLibraryClause(libs, 0 /*implicitly declared*/)) ;

    // Create id for entity
    VhdlEntityId *entity_id = new VhdlEntityId(vhdl_file::CreateVhdlEntityName(Name())) ;
    entity_id->SetLinefile(Linefile()) ;

    VhdlTreeNode::_present_scope->SetOwner(entity_id) ;

    /* Include std.standard.all ; with a use-clause */
    VhdlName *n = new VhdlSelectedName(
                new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                new VhdlIdRef(Strings::save("standard"))),
                new VhdlAll()) ;
    Array *use_clauses = new Array() ;
    use_clauses->InsertLast(n) ;
    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;

    /* Include vl.vltypes.all ; with a use-clause */
    VhdlName *n1 = new VhdlSelectedName(
                new VhdlSelectedName(new VhdlIdRef(Strings::save("vl")),
                new VhdlIdRef(Strings::save("vl_types"))),
                new VhdlAll()) ;
    use_clauses = new Array() ;
    use_clauses->InsertLast(n1) ;
    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;

    (void) VhdlTreeNode::_present_scope->Declare(entity_id) ;

    unsigned i ;
    VeriIdDef *veri_id ;
    Array *generic_clause = 0 ;
    Array *port_clause = 0 ;
    // Iterate through the array of parameters
    FOREACH_ARRAY_ITEM(_parameters, i, veri_id) {
        if (!veri_id) continue ; // Ignore if id is NULL
        if (!generic_clause) generic_clause = new Array ; // Create generic clause

        VhdlName *sub_type_indication = 0 ; // Indicates the subtype indication
        VhdlName *vhdl_init_val = 0 ; // Stores the initial value of the parameter
        VeriExpression *expr = veri_id->GetInitialValue() ; // Get the initial value of the parameter
        // Use the VeriExpression::ConstCast function to cast the expression into a VeriConst*
        VeriConst *const_expr = (expr) ? expr->ConstCast() : 0 ;
        // Viper 5001: Even if we cannot evaluate the initial expression
        // we create uninitialiazed parameters. These will be constrained by actuals
        // We check the size in CheckPortBinding which takes the port size from Verilog Module
        VeriDataType *data_type = veri_id->GetDataType() ; // Get the data type of the parameter
        unsigned type = data_type ? data_type->GetType() : 0 ; // Get the type like VERI_INTEGER, VERI_STRING, VERI_REAL, etc

        // Viper 6895: If the type is from imported Vhdl package take the Vhdl Package itself
        VeriIdDef *type_id = data_type ? data_type->GetId() : 0 ;
        VhdlIdDef *vhdl_type_id = 0 ;
        (void) type_id ;
        if (type_id && type_id->IsConvertedVhdlType()) {
            vhdl_type_id = type_id->GetVhdlTypeFromPackage() ;
        }

        if (vhdl_type_id) {
            // Viper 6895: Here we can handle unpacked dimension as we import the Vhdl Type.
            VeriRange *dims = data_type ? data_type->GetDimensions() : 0 ; // Get the dimension
            Array * assoc_list = 0 ;
            verific_int64 left_bound = 0 ;
            verific_int64 right_bound = 0 ;
            while (dims) {
                VeriExpression *left_expr = dims ? dims->GetLeft() : 0 ; // Get the left bound
                left_bound = left_expr ? left_expr->Integer() : 0 ;

                VeriExpression *right_expr = dims ? dims->GetRight() : 0 ; // Get the right bound
                right_bound = right_expr ? right_expr->Integer() : 0 ;

                VhdlRange *range = 0 ;
                // Note if left_expr and right_expr are not literals we presently
                // will create range of type [0:0] (FIXME).
                if (left_expr && left_expr->IsConst() && right_expr && right_expr->IsConst()) { //Viper 4647, 5019
                    range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;// create the range
                    VhdlExpression* left = range->GetLeftExpression() ;
                    VhdlExpression* right = range->GetRightExpression() ;
                    if (left) left->SetLinefile(left_expr->Linefile()) ;
                    if (right) right->SetLinefile(right_expr->Linefile()) ;
                    if (dims) range->SetLinefile(dims->Linefile()) ;
                }
                if (range) {
                    // Constrained vl_logic_vector
                    if (!assoc_list) assoc_list = new Array;
                    assoc_list->Insert(range) ;
                }
                dims = dims ? dims->GetNext() : 0 ;
            }
            if (assoc_list) {
                sub_type_indication = new VhdlIndexedName(new VhdlIdRef(vhdl_type_id), assoc_list ) ;
            } else {
                sub_type_indication = new VhdlIdRef(vhdl_type_id) ; //Unconstrained
            }
        }

        // For VERI_SIGNED type is zero and hence we determine type by the initial value VIPER 4357
        if (data_type && type && !vhdl_type_id && !sub_type_indication) {
            switch (type) {
            case (VERI_INTEGER) :
                {
                sub_type_indication = new VhdlIdRef(Strings::save("integer")) ;
                // Now a parameter can be of type
                //    parameter integer O = 40'b1101;
                // So, for such parameter the corresponding generic should be
                //    O : integer := 13
                // So, find out the length of the 'b value and accordingly consider it or discard
                // it if it is greater than 32 bits.
                if (const_expr && !((const_expr->IsUnsizedConst() == 0) && (const_expr->Size(0) > 32))) {
                    verific_int64 init_val = const_expr->Integer() ; // Get the integer value
                    vhdl_init_val = new VhdlInteger(init_val) ; // Convert to VhdlExpression*
                }
                break ;
                }
            case (VERI_REAL) :
                {
                sub_type_indication = new VhdlIdRef(Strings::save("real")) ;
                if (const_expr) {
                    char *image = const_expr->Image() ; // Find the string equivalent
                    double init_val = Strings::atof(image) ; // Convert the string to real
                    vhdl_init_val = new VhdlReal(init_val) ; // Convert to VhdlExpression*
                }
                break ;
                }
            case (VERI_STRINGTYPE) :
                {
                sub_type_indication = new VhdlIdRef(Strings::save("string")) ;
                if (const_expr) {
                    char *image = const_expr->Image() ;
                    vhdl_init_val = new VhdlStringLiteral(image) ; // Convert to VhdlExpression*
                }
                break ;
                }
            case (VERI_TIME) : // Viper 6495: add support for time type in verilog
                {
                sub_type_indication = new VhdlIdRef(Strings::save("time")) ;
                if (const_expr) {
                    verific_int64 init_val = const_expr->Integer() ; // Get the integer value
                    vhdl_init_val = new VhdlInteger(init_val) ; // Convert to VhdlExpression*
                }
                break ;
                }
            default : break ;
            }
        }

        if (!sub_type_indication && !vhdl_init_val && !vhdl_type_id) {
            // If the data type has not been specified explicitly then try to determine it from the initial value of the parameter
            // if the constant expression is an integer
            // Ignore parameters having values as sized constants where the size of the
            // constant is greater than 32 bits.
            // Viper 5857 For parameters with data types having dimensions modelsim creates a vl_logic_vector and
            // matches it with integer, bit_vector and std_logic_vector. For these cases we now create a vl_logic_vector
            // type of generic. only if the const_expr id not string or real
            VeriRange *dims = data_type ? data_type->GetDimensions() : 0 ; // Get the dimension
            if (dims) {
                char *data_type_name = Strings::save("vl_logic_vector") ;

                VeriExpression *left_expr = dims->GetLeft() ; // Get the left bound
                verific_int64 left_bound = left_expr ? left_expr->Integer() : 0 ;

                VeriExpression *right_expr = dims->GetRight() ; // Get the right bound
                verific_int64 right_bound = right_expr ? right_expr->Integer() : 0 ;

                VhdlRange *range = 0 ;
                unsigned size = 0 ;
                if (left_expr && left_expr->IsConst() && right_expr && right_expr->IsConst()) {
                    range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;// create the range
                    size = (left_bound > right_bound) ? ((unsigned)(left_bound - right_bound) + 1) : ((unsigned)(right_bound - left_bound) + 1) ;
                    VhdlExpression* left = range->GetLeftExpression() ;
                    VhdlExpression* right = range->GetRightExpression() ;
                    if (left) left->SetLinefile(left_expr->Linefile()) ;
                    if (right) right->SetLinefile(right_expr->Linefile()) ;
                    range->SetLinefile(dims->Linefile()) ;
                }
                // Viper 6076: Crash for empty range. For null range we
                // will create a unconstrained vl_logic_vector. It will
                // be constrained later on by actual
                if (range) {
                    if (const_expr && !const_expr->HasXZ()) {
                        char *image = const_expr ? const_expr->ImageVhdl(0, size) : 0 ;
                        vhdl_init_val = image ? new VhdlBitStringLiteral(image) : 0 ; // Convert to VhdlExpression*
                    }
                    Array * assoc_list = new Array ;
                    assoc_list->Insert(range) ;
                    sub_type_indication = new VhdlIndexedName(new VhdlIdRef(data_type_name), assoc_list) ;
                } else {
                    // unconstrained vl_logic_vector
                    sub_type_indication =  new VhdlIdRef(data_type_name) ;
                }
            } else if (const_expr && const_expr->IsString()) { // If the constant expression is a string
                char *image = const_expr->Image() ;
                vhdl_init_val = new VhdlStringLiteral(image) ; // Convert to VhdlExpression*
                sub_type_indication = new VhdlIdRef(Strings::save("string")) ;
            } else if (const_expr && const_expr->IsReal()) { // If the constant expression is of 'real' type
                char *image = const_expr->Image() ; // Find the string equivalent
                double init_val = Strings::atof(image) ; // Convert the string to real
                vhdl_init_val = new VhdlReal(init_val) ; // Convert to VhdlExpression*
                sub_type_indication = new VhdlIdRef(Strings::save("real")) ;
            } else if (const_expr && const_expr->IsInt()) {
                if (!const_expr->HasXZ()) {
                    verific_int64 init_val = const_expr->Integer() ;
                    vhdl_init_val = new VhdlInteger(init_val) ; // Convert to VhdlExpression*
                }
                sub_type_indication = new VhdlIdRef(Strings::save("integer")) ;
            } else {
                // Now check for size of expression
                if (const_expr) {
                    char *data_type_name = Strings::save("vl_logic_vector") ;
                    verific_int64 left_bound = 0 ;
                    verific_int64 right_bound = const_expr->Size(0) ? const_expr->Size(0) - 1 : 0 ;

                    unsigned size = (left_bound > right_bound) ? ((unsigned)(left_bound - right_bound) + 1) : ((unsigned)(right_bound - left_bound) + 1);
                    if (!const_expr->HasXZ()) {
                        char *image = const_expr->ImageVhdl(0, size) ;
                        vhdl_init_val = image ? new VhdlBitStringLiteral(image) : 0 ; // Convert to VhdlExpression*
                    }

                    // Since there is no data type we keep the vector unconstrained so that it gets constrained
                    // by the actual
                    sub_type_indication =  new VhdlIdRef(data_type_name) ;
                } else {
                    // create an integer type parameter for non constant initial value.
                    sub_type_indication = new VhdlIdRef(Strings::save("integer")) ;
                }
            }
        }
        // Set line file of vhdl_init_val. Issue seen in archpro test extra_test/101014
        if (vhdl_init_val && const_expr) vhdl_init_val->SetLinefile(const_expr->Linefile()) ;
        // Create VhdlInterfaceId for 'generic_name'
        char *escaped_generic_name = VeriNode::CreateEscapedNameFromVerilog(veri_id->GetName(), 1, _ports, _parameters) ;

        VhdlInterfaceId* new_generic = VhdlTreeNode::CreateVhdlGeneric(escaped_generic_name) ;
        if (new_generic) new_generic->SetLinefile(veri_id->Linefile()) ;
        Strings::free(escaped_generic_name) ;
        // Create array to hold created generic id
        Array *id_list = new Array(1) ;
        id_list->InsertLast(new_generic) ;

        // Create declaration
        VhdlInterfaceDecl *generic_decl = new VhdlInterfaceDecl(0, id_list, 0, sub_type_indication, 0, vhdl_init_val) ;
        generic_decl->SetLinefile(veri_id->Linefile()) ;
        // Viper 5112. This caused restoration failure because the
        // _has_init_assign flag was different while saving and restoring
        // This is because during elaboration the initial expression for
        // a expression got evaluated and the expression became constant
        // In saving phase the expression was not evaluated and the _has_init_assign
        // flag was not set on the VhdlInterfaceId.
        if (new_generic) new_generic->SetHasInitAssign() ; // Viper 5112

        // Insert generic declaration into generic clause
        generic_clause->InsertLast(generic_decl) ;
    }

    unsigned mode = 0 ;
    // Viper 3395: If any unnamed port is present in the Verilog module then use madeup names of the port.
    // hence named connection is not possible but positional association should work correctly.

    Array *ports = GetPorts() ;
    Array *connections = GetPortConnects() ;
    unsigned j ;
    FOREACH_ARRAY_ITEM(ports, j, veri_id) {
        if (!veri_id) continue ;
        VeriIdDef *formal_id = 0 ;
        // Get the formal data type:
        // Check whether it is named connection or not
        VeriExpression *conn = (connections && (j < connections->Size()))? (VeriExpression*)connections->At(j) : 0 ;
        // Viper 8201: Do not create converted entity ports for un-named ports. This is also the
        // behavior of common simulator.
        if (conn && conn->IsOpen()) continue ;
        // For ansi port declarations do not extract datatype or size information from actual.
        if (conn && conn->IsAnsiPortDecl()) {
            conn = 0 ;
        }
        const char *formal =  conn ? conn->NamedFormal() : 0 ;
        unsigned is_scalar_port =  0 ;
        if (formal) {
            VeriModule *module = (_id) ? _id->GetModule(): 0 ;
            formal_id = (module) ? module->GetPort(formal) : 0 ;
            if (formal_id && formal_id->IsImplicitNonAnsiPort()) formal_id = 0 ;
            // For implicit net take veri_id as the actual id the formal_id
            // will not contain size and type information.
            if (!formal_id && conn) {
                VeriExpression *actual = conn->GetConnection() ;
                if (actual->FindSize() == 1) is_scalar_port = 1 ;
                // For VeriIndexedId like port[0:0] size is 1 but still we will
                // treat this as vector unlike port[0] which is treated as scalar
                VeriExpression *idx_expr = actual->GetIndexExpr() ;
                if (idx_expr && !idx_expr->IsSingleValueRange()) is_scalar_port = 0 ; // Reset Scalar ports for ports like p[3:3]
                // Now set veri_id to the actual identofier.
                if (actual->IsConcat()) {
                    Array *exprs = actual->GetExpressions() ;
                    if (exprs && exprs->Size()>=1) actual = (VeriExpression*)(exprs->At(0)) ;
                }
                VeriIdDef* actual_id = actual->FullId() ;
                if (!actual_id) actual_id = actual->GetId() ;
                veri_id = actual_id ? actual_id : veri_id ;
            }
        } else {
            formal_id = veri_id ;
        }

        if (!port_clause) port_clause = new Array ; // Create port clause

        // Viper 6895: If the type is from imported Vhdl package take the Vhdl Package itself
        VeriDataType *data_type = veri_id->GetDataType() ; // Get the data type
        VeriIdDef *type_id = data_type ? data_type->GetId() : 0 ;
        VhdlIdDef *vhdl_type_id = 0 ;
        (void) type_id ;
        if (type_id && type_id->IsConvertedVhdlType()) {
            vhdl_type_id = type_id->GetVhdlTypeFromPackage() ;
        }

        // Viper 7066: create the port with the first dimension. Might produce scalar ports for ports having only unpacked dimension
        // If the id has got unpacked dimensions then ignore it
        //if (veri_id->GetDimensions() && !vhdl_type_id) continue ;

        // Set the modes i.e the directions depending on the Verilog port direction
        if (veri_id->IsInput()) {
            mode = VhdlTreeNode::GetInputMode() ; /* VHDL_in */
        } else if (veri_id->IsOutput()) {
            mode = VhdlTreeNode::GetOutputMode() ; /* VHDL_out */
        } if (veri_id->IsInout()) {
            mode = VhdlTreeNode::GetInoutMode() ; /* VHDL_inout */
        }

        unsigned packed_dim = (data_type) ? data_type->PackedDimension() : 0 ; // Get the number of packed dimensions
        VeriRange *unpacked_dimension = veri_id->GetDimensions() ;

        // Viper 7066: create the port with the first dimension.
        // Ignore multiple packed dimensions
        //if (packed_dim>1) continue ;

        Array *assoc_list = 0 ;
        char *data_type_name = 0 ;
        VhdlName *sub_type_indication = 0 ;
        unsigned has_unconstrained_range = 0 ;
        if (!vhdl_type_id) {
            // First insert the unpacked dimensions if any:
            if (unpacked_dimension) {
                VeriRange* dims = unpacked_dimension ;
                while (dims) {
                    VhdlDiscreteRange *range = 0 ;
                    VeriExpression *left_expr = dims ? dims->GetLeft() : 0 ; // Get the left bound
                    verific_int64 left_bound = left_expr ? left_expr->Integer() : 0 ;

                    VeriExpression *right_expr = dims ? dims->GetRight() : 0 ; // Get the right bound
                    verific_int64 right_bound = right_expr ? right_expr->Integer() : 0 ;

                    if (left_expr && left_expr->IsConst() && right_expr && right_expr->IsConst()) { //Viper 4647, 5019
                        range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;// create the range
                        VhdlExpression* left = range->GetLeftExpression() ;
                        VhdlExpression* right = range->GetRightExpression() ;
                        if (left) left->SetLinefile(left_expr->Linefile()) ;
                        if (right) right->SetLinefile(right_expr->Linefile()) ;
                        if (dims) range->SetLinefile(dims->Linefile()) ;
                    } else {
                        range = new VhdlExplicitSubtypeIndication(0, new VhdlIdRef(Strings::save("natural")), new VhdlBox()) ;
                        if (dims) range->SetLinefile(dims->Linefile()) ;
                        has_unconstrained_range = 1 ;
                    }
                    dims = dims ? dims->GetNext() : 0 ;
                    if (!assoc_list) assoc_list = new Array ;
                    assoc_list->InsertFirst(range) ;
                }
            }
            if (!packed_dim || is_scalar_port) { // If this is a scalar
                unsigned type = data_type? data_type->GetType() : 0 ;
                // If the data type of the port is an integer/real/string/byte then the corresponding
                // VHDL port should be of 'integer/real/string' type as well. Otherwise, it will be of 'logic' type.
                // This is issue was filed as Viper 5813.
                if (type && data_type) {
                    switch (type) {
                    case (VERI_INTEGER) :
                    case (VERI_INT) : //VIPER 5486. create vhdl integer for system verilog int
                        {
                        data_type_name = Strings::save("integer") ;
                        break ;
                        }
                    case (VERI_REAL) :
                        {
                        data_type_name = Strings::save("real") ;
                        }
                        break ;
                    case (VERI_STRINGTYPE) :
                        {
                        data_type_name = Strings::save("string") ;
                        }
                        break ;
                    /*
                    // Viper 6495: Modelsim seem to match time type port with vector port in Vhdl
                    // hence preferred to keep it as a vl_logic_vector. Vcs do not support
                    // time type of ports
                    case (VERI_TIME) :
                        {
                        data_type_name = Strings::save("time") ;
                        }
                        break ;
                    */
                    default :
                        if (data_type->BaseTypeSize() > 1 || unpacked_dimension) {
                            data_type_name = Strings::save("vl_logic_vector") ;
                        } else {
                            data_type_name = Strings::save("vl_logic") ;
                        }
                        break ;
                    }
                }
                if (!data_type_name) {
                    if (unpacked_dimension) {
                        data_type_name = Strings::save("vl_logic_vector") ;
                    } else {
                        data_type_name = Strings::save("vl_logic") ;
                    }
                }
                sub_type_indication = new VhdlIdRef(data_type_name) ;
            } else { // If the port is a vector
                // : For port declarations like
                //     output[1+3'b111:0] out1, clk1;
                // the dimensions are currently getting ignored.
                // We create unconstrained vl_logic_vector. This is
                // constrained by the actual. The type checking (size mismatch errors)
                // are done in VhdlEntityDecl::CheckPortBinding by taking the actual verilog module VIPER 4647, 5019
                verific_int64 left_bound = 0 ;
                verific_int64 right_bound = 0 ;
                if (formal_id) {
                    VeriRange *dims = data_type ? data_type->GetDimensions() : 0 ; // Get the dimension
                    while (dims) {
                        VhdlDiscreteRange *range = 0 ;
                        VeriExpression *left_expr = dims ? dims->GetLeft() : 0 ; // Get the left bound
                        left_bound = left_expr ? left_expr->Integer() : 0 ;

                        VeriExpression *right_expr = dims ? dims->GetRight() : 0 ; // Get the right bound
                        right_bound = right_expr ? right_expr->Integer() : 0 ;

                        if (left_expr && left_expr->IsConst() && right_expr && right_expr->IsConst()) { //Viper 4647, 5019
                            range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;// create the range
                            VhdlExpression* left = range->GetLeftExpression() ;
                            VhdlExpression* right = range->GetRightExpression() ;
                            if (left) left->SetLinefile(left_expr->Linefile()) ;
                            if (right) right->SetLinefile(right_expr->Linefile()) ;
                            if (dims) range->SetLinefile(dims->Linefile()) ;
                        } else {
                            range = new VhdlExplicitSubtypeIndication(0, new VhdlIdRef(Strings::save("natural")), new VhdlBox()) ;
                            if (dims) range->SetLinefile(dims->Linefile()) ;
                            has_unconstrained_range = 1 ;
                        }
                        dims = dims ? dims->GetNext() : 0 ;
                        if (!assoc_list) assoc_list = new Array ;
                        assoc_list->InsertFirst(range) ;
                    }
                } else {
                    VhdlDiscreteRange *range = 0 ;
                    VeriExpression *actual = conn ? conn->GetConnection() : 0 ;
                    if (actual && actual->IsConcat()) {
                        left_bound = actual->FindSize() - 1 ;
                        range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;// create the range
                    }
                    if (!assoc_list) assoc_list = new Array ;
                    assoc_list->Insert(range) ;
                }

                data_type_name = Strings::save("vl_logic_vector") ;

                VhdlDiscreteRange *range = assoc_list && assoc_list->Size()==1 ? (VhdlDiscreteRange*)assoc_list->At(0) : 0 ;
                if (range)  {
                    if (has_unconstrained_range) {
                        sub_type_indication =  new VhdlIdRef(data_type_name) ;
                        delete range ;
                        range = 0 ;
                        delete assoc_list ;
                    } else {
                        sub_type_indication = new VhdlIndexedName(new VhdlIdRef(data_type_name), assoc_list) ;
                    }
                    assoc_list = 0 ;
                } else {
                    // unconstrained vl_logic_vector
                    sub_type_indication =  new VhdlIdRef(data_type_name) ;
                }
            }
        } else {
            // Viper 6895: Here we can handle unpacked dimension as we import the Vhdl Type.
            VeriRange *dims = veri_id->GetDimensions() ; // Get the dimension
            Array * vhdl_assoc_list = 0 ;
            verific_int64 left_bound = 0 ;
            verific_int64 right_bound = 0 ;
            while (dims) {
                VhdlDiscreteRange *range = 0 ;
                VeriExpression *left_expr = dims ? dims->GetLeft() : 0 ; // Get the left bound
                left_bound = left_expr ? left_expr->Integer() : 0 ;

                VeriExpression *right_expr = dims ? dims->GetRight() : 0 ; // Get the right bound
                right_bound = right_expr ? right_expr->Integer() : 0 ;

                // Note if left_expr and right_expr are not literals we presently
                // will create range of type [0:0] (FIXME).
                if (left_expr && left_expr->IsConst() && right_expr && right_expr->IsConst()) { //Viper 4647, 5019
                    range =  VhdlTreeNode::CreateVhdlRange(left_bound, right_bound) ;// create the range
                    VhdlExpression* left = range->GetLeftExpression() ;
                    VhdlExpression* right = range->GetRightExpression() ;
                    if (left) left->SetLinefile(left_expr->Linefile()) ;
                    if (right) right->SetLinefile(right_expr->Linefile()) ;
                    if (dims) range->SetLinefile(dims->Linefile()) ;
                }
                if (range) {
                    // Constrained vl_logic_vector
                    if (!vhdl_assoc_list) vhdl_assoc_list = new Array;
                    vhdl_assoc_list->Insert(range) ;
                }
                dims = dims ? dims->GetNext() : 0 ;
            }
            if (vhdl_assoc_list) {
                sub_type_indication = new VhdlIndexedName(new VhdlIdRef(vhdl_type_id), vhdl_assoc_list ) ;
            } else {
                sub_type_indication = new VhdlIdRef(vhdl_type_id) ; //Unconstrained
            }
        }
        // Create VhdlInterfaceId for 'port_name'

        char *escaped_port_name = VeriNode::CreateEscapedNameFromVerilog(formal_id ? formal_id->GetName() : formal, 0, _ports, _parameters) ;
        VhdlInterfaceId *new_port = VhdlTreeNode::CreateVhdlPort(escaped_port_name) ;
        if (new_port) {
            new_port->SetLinefile(veri_id->Linefile()) ;
            if (assoc_list) new_port->SetVerilogAssocList(assoc_list) ;
        }
        Strings::free(escaped_port_name) ;

        // Create array to hold created port id
        Array *id_list = new Array(1) ;
        id_list->InsertLast(new_port) ;

        // Create declaration
        VhdlInterfaceDecl *port_decl = new VhdlInterfaceDecl(0, id_list, mode, sub_type_indication, 0, 0) ;
        port_decl->SetLinefile(veri_id->Linefile()) ;

        // Insert port declaration into port clause
        port_clause->InsertLast(port_decl) ;
    }
    // Create new entity
    unit = new VhdlEntityDecl(context_clause, entity_id, generic_clause, port_clause, 0,0, VhdlScope::Pop()) ;
    unit->SetLinefile(Linefile()) ;
    VhdlTreeNode::_present_scope = save_scope ;
#endif
    unit->SetVerilogModule(1) ; // Set the flag to denote that this entity has been obtained from a Verilog module
    // 6918 : If this module is marked as black-box by user, mark the created
    // unit as black-box
    if (GetCompileAsBlackbox() || veri_file::CheckCompileAsBlackbox(Name())) {
        unit->SetCompileAsBlackbox() ;
    }
    // Viper 6122: Set _original_unit_name only for copied entites
    //unit->SetOriginalUnitName(GetName()) ;  // Set the verilog module name which retains the original module name
    return unit ; // Return the entity unit
}
VhdlPrimaryUnit*
VeriConfiguration::ConvertModule(const char *lib_name) const
{
    // Archpro/extra_tests/090409 and Viper 4972 Support Vhdl Configuration
    // Viper 4611: take the lib_name from verilog module
    const char *library_name = _parent_library ? _parent_library->GetName() : lib_name ;
    if (!library_name) return 0 ; // If no such module or library exist then return 0
    VhdlConfigurationDecl *unit = 0 ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION

    /* First, reset the scope to 0, then create a new scope for the library unit */
    VhdlScope *save_scope = VhdlTreeNode::_present_scope ;
    VhdlTreeNode::_present_scope = 0 ;
    VhdlScope::Push() ;

    /* Now create a 'context_clause' tree node (an Array of context items). */
    Array *context_clause = new Array(3) ;

    /* Define library names 'STD' and 'WORK' */
    Array *libs = new Array(3) ;

    /* Declare library identifier 'std' */
    VhdlIdDef *id = new VhdlLibraryId(Strings::save("std")) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    /* Declare library identifier for work library */
    id = new VhdlLibraryId(Strings::save(library_name)) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    /* Declare library identifier 'vl' */
    // This library is the one used by Modelsim to declare variables 'vl_logic', 'vl_logic_vector', etc
    id = new VhdlLibraryId(Strings::save("vl")) ;
    (void) VhdlTreeNode::_present_scope->Declare(id) ;
    libs->InsertLast(id) ;

    /* Set them in the parse tree */
    context_clause->InsertLast(new VhdlLibraryClause(libs, 0 /*implicitly declared*/)) ;

    // Create id for entity
    VhdlConfigurationId *configuration_id = new VhdlConfigurationId(vhdl_file::CreateVhdlEntityName(Name())) ;
    if (_id) configuration_id->SetLinefile(_id->Linefile()) ;

    VhdlTreeNode::_present_scope->SetOwner(configuration_id) ;

    /* Include std.standard.all ; with a use-clause */
    VhdlName *n = new VhdlSelectedName(
                new VhdlSelectedName(new VhdlIdRef(Strings::save("std")),
                new VhdlIdRef(Strings::save("standard"))),
                new VhdlAll()) ;
    Array *use_clauses = new Array(1) ;
    use_clauses->InsertLast(n) ;
    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;

    /* Include vl.vltypes.all ; with a use-clause */
    VhdlName *n1 = new VhdlSelectedName(
                new VhdlSelectedName(new VhdlIdRef(Strings::save("vl")),
                new VhdlIdRef(Strings::save("vl_types"))),
                new VhdlAll()) ;
    use_clauses = new Array(1) ;
    use_clauses->InsertLast(n1) ;
    context_clause->InsertLast(new VhdlUseClause(use_clauses, 0 /*implicitly declared*/)) ;

    (void) VhdlTreeNode::_present_scope->Declare(configuration_id) ;

    // Get the top level 'design' modules of this configuration:
    Set *designs = GetDesignModules() ;
    VeriModule *verilog_module = 0 ;
    char *entity_name = 0 ;
    linefile_type line_file = 0 ;
    VhdlPrimaryUnit *entity_unit = 0 ;
    if (designs && designs->Size() >= 1) {
        if (designs->Size() > 1) {
             // It should not have more than one top modules (even for the configuration):
             Error("configuration with multiple designs cannot be instantiated") ;
        }
        verilog_module = (VeriModule *)designs->GetLast() ; // Get the last 'design' top module anyway
        entity_unit = verilog_module ? verilog_module->ConvertModule(lib_name): 0 ;
        //entity_name = vhdl_file::CreateVhdlEntityName(verilog_module ? verilog_module->GetName() : 0 ) ;
        line_file = verilog_module ? verilog_module->Linefile() : 0 ;
    } else {
        Set *design_units = GetDesignUnits() ;
        if (design_units && design_units->Size()>=1) {
            if (design_units->Size() > 1) {
                 // It should not have more than one top modules (even for the configuration):
                 Error("configuration with multiple designs cannot be instantiated") ;
            }
            entity_unit = (VhdlPrimaryUnit*)design_units->GetLast() ;
            //entity_name = entity_unit ? Strings::save(entity_unit->Name()) : 0 ;
            line_file = entity_unit ? entity_unit->Linefile() : 0 ;
        }
        delete design_units ;
    }
    entity_name = entity_unit ? Strings::save(entity_unit->Name()) : 0 ;
    if (entity_unit) configuration_id->DeclareConfigurationId(entity_unit->Id()) ;
    delete designs ; designs = 0 ;
    // Create new configuration
    if (!entity_name) {
        // Cleanup
        unsigned i ;
        VhdlDeclaration *decl ;
        FOREACH_ARRAY_ITEM(context_clause, i, decl) delete decl ;
        delete context_clause ;
        // Viper 5622. _present scope is popped to prevent crash later on
        VhdlTreeNode::_present_scope = save_scope ;
        return 0 ;
    }
    VhdlIdRef *entity_ref = new VhdlIdRef(entity_name) ;
    if (line_file) entity_ref->SetLinefile(line_file) ;

    unit = new VhdlConfigurationDecl(context_clause, configuration_id, entity_ref, 0, 0, VhdlScope::Pop()) ;
    unit->SetLinefile(Linefile()) ;
    VhdlTreeNode::_present_scope = save_scope ;
#endif
    unit->SetVerilogModule(1) ; // Set the flag to denote that this entity has been obtained from a Verilog module
    // 6918 : If this module is marked as black-box by user, mark the created
    // unit as black-box
    if (GetCompileAsBlackbox() || veri_file::CheckCompileAsBlackbox(Name())) {
        unit->SetCompileAsBlackbox() ;
    }
    // Viper 6122: Set _original_unit_name only for copied entites
    //unit->SetOriginalUnitName(GetName()) ;  // Set the verilog module name which retains the original module name
    return unit ; // Return the configuration unit
}
// Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
// Clear parameter and port connects :
void VeriModule::ClearParameterConnects()
{
    _parameter_connects = 0 ;
}
void VeriModule::ClearPortConnects()
{
    _port_connects = 0 ;
}
void VeriModule::ClearPackageImportList()
{
    _package_import_decls = 0 ;
}

VeriIdDef *VeriModule::DeclareNonAnsiPort(const char *name, unsigned dir, VeriExpression *port_expr, unsigned port_idx)
{
    // If relaxed checking mode is on, declare the identifier in a specific
    // scope created only for non-ansi ports. Otherwise declare in module scope
    VeriScope *decl_scope = _scope ;
    if (VeriNode::InRelaxedCheckingMode()) {
        if (!_port_scope) _port_scope = new VeriScope() ;
        decl_scope = _port_scope ;
    }

    // Create an identifier for unnamed/named non-ansi style port
    // RD: 2/2013: Make sure to give unnamed ports a name that includes the ordering index, since messages rely on a valid name.
    char *port_name ;
    if (!name) {
        char buffer[16] ;
        sprintf(buffer, "number %d",port_idx+1) ;
        port_name = Strings::save(buffer) ;
    } else {
        port_name = Strings::save(name) ;
    }

    // Choose VeriSeqPropertyFormal since that one has a 'actual' expression back-pointer.
    VeriIdDef *port_id = new VeriSeqPropertyFormal(port_name, 0) ;

    // Set the info we know about :
    port_id->SetDir(dir) ;
    port_id->SetActual(port_expr) ;
    port_id->SetIsImplicitNonAnsiPort() ;

    if (decl_scope) {
        if (name) {
            // if name exists, there should not be any name conflict
            VeriIdDef *exist = 0 ;
            if (!decl_scope->Declare(port_id, 0 /* no force insert */, &exist)) {
                VeriTreeNode *from = port_expr ;
                if (!from) from = this ;
                from->Error("%s is already declared", name) ;
                if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
                delete port_id ;
                port_id = 0 ;
            }
        } else {
            // unnamed port, force insert port_id
            (void) decl_scope->Declare(port_id, 1/*force_insert*/) ;
        }
    }

    return port_id ;
}
// Get port identifier of a module by name :
VeriIdDef *VeriModule::GetPort(const char *name) const
{
    if (!name) return 0 ; // nothing to find

    VeriIdDef *id = 0 ;
    // Find the port first in _port_scope if exists
    if (_port_scope) id = _port_scope->FindLocal(name) ;

    if (id) return id ;
    id =_scope ? _scope->FindLocal(name): 0 ; // otherwise find in scope
    if (id && !id->IsPort()) id = 0 ;
    return id ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Convert vhdl package to verilog one: Fill up the verilog ranges
// and initial value of constants with verilog expression during elaboration:
void VeriPackage::FillRangesAndInitialExpr()
{
    // Do nothing if the package is not a converted verilog package or already elaborated
    if (!IsVhdlPackage() || IsAlreadyProcessed()) return ;

    // Set already processed flag when initializing the packege first time.
    // We may come to initialize the package again if the package is imported
    // to more than one design:
    SetAlreadyProcessed(1) ; // set already processed flag when initializing the packege first time

    const char *pkg_name = GetName() ;
    VeriLibrary *lib = GetLibrary() ; // Get the verilog library
    const char *lib_name = lib ? lib->GetName() : "work" ;
    // Get the correspondig vhdl package from the library
    VhdlPrimaryUnit *vhdl_package = VeriNode::GetVhdlUnitForVerilogInstance(pkg_name, lib_name, 1, 0) ;
    if (!vhdl_package) return ;

    if (IsStaticElab()) (void) vhdl_package->StaticElaborate(0, 0, 0, 0, 0) ;

    // Get the vhdl package scope
    VhdlScope *vhdl_scope = vhdl_package->LocalScope() ;
    // Get the declared verilog ids from verilog scope
    Map *veri_ids = _scope ? _scope->GetThisScope() : 0 ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(veri_ids, mi, 0, &id) {
        if (!id) continue ;
        const char *name = id->GetName() ;
        VhdlIdDef *vhdl_id = vhdl_scope ? vhdl_scope->FindSingleObject(name) : 0 ; // find the correspong vhdl id
        if (!vhdl_id || vhdl_id->IsEnumerationLiteral()) continue ;
        VhdlConstraint *constraint = vhdl_id->Constraint() ;
        linefile_type line_file = vhdl_id->Linefile() ;
        if (constraint) constraint->FillUpRanges(_scope, id, 0, line_file) ; // fill up verilog unconstriant ranges

        // Add initial value to the constants
        if (id->IsParam() && !id->GetInitialValue()) {
            VhdlValue *init_val = vhdl_id->Value() ;
            VeriExpression *init_expr = init_val ? init_val->ConvertToVerilogExpression(_scope, init_val->GetType(), line_file) : 0 ;
            if (init_expr) id->SetInitialValue(init_expr) ;
        }
    }
}
#endif

void
VeriModule::SetHasAnalyzeError(SynthesisInfo *info)
{
    (void) info ;

    _has_analyze_error = 1 ;
}

// Returns allocated timescale timeunit/timeprecision created from timeunit and timeprecision declaration
char * VeriModule::GetTimeUnitDefinedTimeScale() const
{
    // Timeunit and timeprecision should be declared before declarating any other module item
    char *time_unit = 0 ;
    char *time_precision = 0 ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (!item) continue ;
        if (item->IsTimeUnitDecl()) {
            VeriExpression *literal_expr = item->GetTimeLiteral() ;
            char *literal = literal_expr ? literal_expr->Image(): 0 ;
            if (item->GetTimeUnitType()==VERI_TIMEPRECISION) {
                if (!time_precision) {
                    time_precision = literal ;
                } else {
                    Strings::free(literal) ;
                }
            } else if (item->GetTimeUnitType()==VERI_TIMEUNIT) {
                if (!time_unit) {
                    time_unit = literal ;
                } else {
                    Strings::free(literal) ;
                }
                VeriExpression *time_precision_expr = item->GetTimePrecision() ;
                char *timeprecision = time_precision_expr ? time_precision_expr->Image(): 0 ;
                if (!time_precision) {
                   time_precision = timeprecision ;
                } else {
                   Strings::free(timeprecision) ;
                }
            }
        } else {
            // Not timeunit decl
            break ;
        }
    }
    if (!time_unit && !time_precision) return 0 ;

    if (!time_unit || !time_precision) { // either one is present
        // Get missing one from `timescale
        char *timescale = Strings::save(_timescale) ;
        char *stored_timescale = timescale ;
        char *timeunit_fromtimeScale = 0 ;
        char *timeprec_fromtimeScale = 0 ;
        if (timescale) {
            char *tmp = strchr(timescale, '/') ;
            if (tmp) {
                *tmp = '\0' ;
                tmp++ ;
                timeprec_fromtimeScale = tmp ;
            }
            timeunit_fromtimeScale = timescale ;
        }
        if (!time_unit && timeunit_fromtimeScale) time_unit = Strings::save(timeunit_fromtimeScale) ;
        if (!time_precision && timeprec_fromtimeScale) time_precision = Strings::save(timeprec_fromtimeScale) ;
        Strings::free(stored_timescale) ;
    }
    char *ret_timescale = Strings::save(time_unit ? time_unit: "1ns", "/", time_precision ? time_precision: "1ns") ;
    Strings::free(time_unit) ;
    Strings::free(time_precision) ;
    return ret_timescale ;
}

