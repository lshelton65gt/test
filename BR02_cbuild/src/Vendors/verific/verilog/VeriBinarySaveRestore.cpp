/*
 *
 * [ File Version : 1.297 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>     // strcmp
#include <stdio.h>      // sprintf

#include "veri_file.h"
#include "SaveRestore.h"
#include "FileSystem.h"
#include "Message.h"
#include "Strings.h"
#include "Map.h"
#include "Set.h"
#include "Array.h"
#include "RuntimeFlags.h" // for run-time flags

#include "VeriTreeNode.h"
#include "VeriModuleItem.h"
#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "VeriModule.h"
#include "VeriExpression.h"
#include "VeriId.h"
#include "VeriStatement.h"
#include "VeriScope.h"
#include "veri_tokens.h"
#include "VeriLibrary.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/* ----------------------------------------- */
// Version History : Introduced on 1/2/07
/* ----------------------------------------- */

// Version 0x0ab00032 (03/2014) :
//      Added a new member in VeriClass to store extends/implements specified interface classes
//
// Version 0x0ab00031 (03/2014) :
//      #VIPER #8303: Added a new class VeriLabelId for label with scope.
//
// Version 0x0ab00030 (02/2014) :
//      #VIPER #8419: Added a new class VeriNullStatement for statement with only ';'.
//
// Version 0x0ab0002f (01/2014) :
//      #VIPER #8304: Added field in VeriTimeUnit to store optional time precision specified with time unit declaration.
//
// Version 0x0ab0002e (12/2013) :
//      #VIPER #8339: Issue warning for variable initialization inside task/function/procedural block
//      without specifying then as static or automatic.
//          unsigned _loop_scope: 1 filed is added to VeriScope
//          unsigned __seq_block_scope: 1 filed is added to VeriScope
//
// Version 0x0ab0002d (09/2013) :
//      Converted VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK compile flag to runtime flag
//      for pseudo tree node based elaboration flow. This exposes the following two members
//      of VeriBlockId out of the compile flag (always available, but not set):
//          unsigned _is_unnamed_blk: 1 (single bit, bit flag) and char *_orig_name
//
//      10/2013: VIPER #8155: Had to include this version in the check while restoring
//      VeriParamId since we added it on September 2013.
//
// Version 0x0ab0002c (07/2013) :
//      Key and Value of _fsm_constants are allocated char* now
//
// Version 0x0ab0002b (06/2013) :
//      Added _forward_type_defined field on VeriTypeId class.
//
// BACKWARD COMPATIBILITY ACTIVATED (01/2013) :
//      RD: Initiation of backward compatibility.
//      Any modifications of the binary save/restore stream after this note must install a new version number
//      as well as restored version checks to handle versions from 0x0ab00029 on-ward.
//
// Version 0x0ab0002a (03/2013) :
//      SV 1800-2012 Section 6.6.7: Added field _resolution_function in VeriDataDecl
//
// Version 0x0ab00029 (09/2012) :
//      VIPER #7416: Added field _param_values in VeriUseClause
//      VIPER #5239: Added field _is_used_in_for_loop_index in VeriVariable.
//      VIPER #6907: Added field _is_static_class_variable in VeriVariable.
//
// Version 0x0ab00028 (09/2012) :
//      VIPER #7379: Added support for IEEE 1800 (2009) Annex E optional compiler directives.
//
// Version 0x0ab00027 (06/2012) :
//      Added _exported_scopes field in VeriScope
//      Also added saving/restoring two numbers as part of dependency signature (will be useful for fixing VIPER #7192).
//
// Version 0x0ab00026 (03/2012) :
//      Added VeriExternForkjoinTaskId class to represent extern fork-join task id
//
// Version 0x0ab00025 (01/2012) :
//     Save/Restore of _implicit_decl_list field of VeriForeach and VeriForeachOperator
//
// Version 0x0ab00024 (12/2011) :
//     Save _is_vhdl_package field of VeriModule
//     Save _unconstraint_dim_count fiel of VeriTypeId
//     Save _vhdl_package_name
//     Save _vhdl_lib_name
//
// Version 0x0ab00023 (10/2011) :
//     Save _is_clocking_decl_id field of VeriVariable
//
// Version 0x0ab00022 (09/2011) :
//     Save _sva_clock_expr_index field of VeriEventControlStatement
//
// Version 0x0ab00021 (09/2011) :
//     Save _id_refs Array field of VeriInlineConstraint
//
// Version 0x0ab00020 (08/2011) :
//     Save _actual field of VeriSeqPropertyFormal
//
// Version 0x0ab0001F (06/2011) :
//     Added _port_scope in VeriModule class
//
// Version 0x0ab0001F (06/2011) :
//      Added new field _is_resolved in VeriClass
//
// Version 0x0ab0001E (02/2011) :
//      Changed Version number for CS changes
// Version 0x0ab0001D (02/2011) :
//      Added two new classes VeriDPIFunctionDecl and VeriDPITaskDecl for DPI import/export
//      function/task declarations (see VIPER #6387 for more details).
//
// Version 0x0ab0001C (11/2010) :
//      Added new class VeriIndexedExpr to represent indexing on concats/multi-concats.
//
// Version 0x0ab0001B (10/2010) :
//      Added unsigned _kind and _unused fields to class VeriVariable
//
// Version 0x0ab0001A (10/2010) :
//      Adjusted VeriScope::CalculateSaveRestoreSignature to take type of id's data type.
//
// Version 0x0ab00019 (09/2010) :
//      Save/restore new class VeriPathPulseValPorts
//
// Version 0x0ab00018 (07/2010) :
//      Adjusted VeriScope::CalculateSaveRestoreSignature to ignore type for type-refs.
//
// Version 0x0ab00017 (06/2010) :
//      Save/restore new classes VeriCaseOperator and VeriCaseOperatorItem
//
// Version 0x0ab00016 (06/2010) :
//      Save/restore new field of VeriEventExpression class
//
// Version 0x0ab00015 (05/2010) :
//      Save/restore two new fields of VeriAssertion class
//
// Version 0x0ab00014 (05/2010) :
//      Save/restore _is_cycle_delay field for VeriDelayOrEventControl and VeriDelayControlStatement
//
// Version 0x0ab00013 (04/2010) :
//      Added Array *_package_import_decls field to class VeriModule
//
// Version 0x0ab00012 (04/2010) :
//      Added VeriExportDecl class to represent export declaration
//
// Version 0x0ab00011 (04/2010) :
//      Added VeriSequentialInstantiation class to represent checker instance of P1800-2009
//
// Version 0x0ab00010 (04/2010) :
//      Added VeriDefaultDisableIff class to represent default disable iff of P1800-2009
//
// Version 0x0ab0000f (04/2010) :
//      Added VeriChecker and VeriCheckerId classes to represent checker block of P1800-2009
//
// Version 0x0ab0000e (03/2010) :
//      Added VeriLetDecl and VeriLetId class to represent let construct of P1800-2009
//
// Version 0x0ab0000d (03/2010) :
//      Added _sample_func field in VeriCovergroup to store sample function associated with covergroup (P1800-2009 addition)
//
// Version 0x0ab0000c (03/2010) :
//      Added _is_unnamed_blk field in VeriBlockId to mark identifier as unnamed block identifier
//
// Version 0x0ab0000b (02/2010) :
//      Save/restore owner ids of extended and base scope instead of scope pointers.
//
// Version 0x0ab0000a (02/2010) :
//      Added _out_polarity_operator field in VeriPath class
//
// Version 0x0ab00009 (01/2010) :
//      Added _virtual_token field in VeriTypeRef class to store virtual keyword
//
// Version 0x0ab00007 (11/2009) :
// Version 0x0ab00008 (12/2009) :
//      Added VeriDisciplinedId class in ams to store data type along with discipline name
//
// Version 0x0ab00007 (11/2009) :
//      Added _verilog_method flag in VeriFunctionDecl class to handle extern "C" functions
//
// Version 0x0ab00006 (11/2009) :
//      Moved _is_elaborated flag from rtl elab specific to analysis specific
//
// Version 0x0ab00005 (10/2009) :
//      Introduced VeriName * in VeriPrototypeId class
//
// Version 0x0ab00004 (10/2009) :
//      Removed VeriCovergroupId class, using VeriTypeId instead
//
// Version 0x0ab00003 (04/2009) :
//      Introduced VeriDelayOrEventControl in VeriEventTrigger class
//
// Version 0x0ab00002 (04/2009) :
//      Introduced base class scope pointer in scope for classes.
//
// Version 0x0ab00001 (08/07) :
//      We do not support backward compatibility yet, so pump up version number each month.
//      At restore time we then error out if version does not match exactly.
//
// Version 0x0ab00000 (1/2/07) :
//      Introduction of Verilog save/restore support.
//
/* ----------------------------------------- */

#define VERI_PARSE_TREE             0xbaccbef0
#define VERI_PERSISTENCE_VER_NUM    0x0ab00032

/* ----------------------------------------- */

// Macro for storing bit flags as unsigned integers and restoring them
// a => collection of bit flags, b => bit flag value,  n => size of bit flag
#define SAVE_BIT_FLAG(a,b,n)       ((a) = ((a) << (n)) | (b))
#define RESTORE_BIT_FLAG(a,b,n)    {(b) = (a) & ((1<<(n))-1) ; (a)>>=(n) ;}

/* ----------------------------------------- */

// Static member initialization:

// Verilog library path
/*static*/ char *veri_file::_default_library_path = 0 ;
/*static*/ Map  *veri_file::_map_library_paths = 0 ;
#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
/*static*/ Map  *veri_file::_lib_unit_map = 0 ; // Map of lib_name->(Map of unitnames) to quickly check if a unit is in a lib-wide map file.
#endif

static const unsigned int PSL_ENABLED               = 1 ;
static const unsigned int AMS_ENABLED               = 2 ;
static const unsigned int SYS_VERILOG_ENABLED       = 4 ;
static const unsigned int PRESERVE_COMMENTS_ENABLED = 8 ;

// Product configuration flags :
static const unsigned int _current_product_flags = 0
        | SYS_VERILOG_ENABLED
//        | PRESERVE_COMMENTS_ENABLED // Run-time switch since 6/2012. Set to value when we save current_product_flags
        ;

//--------------------------------------------------------------
// #included from veri_file.h
//--------------------------------------------------------------

unsigned
veri_file::Save(const char *lib_name, const char *module_name)
{
    // Verify arguments.
    // if module_name is NULL then we will save the entire library.
    if (!lib_name) return 0;

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    if (!module_name) {
        // attempt to save entire library.
        // Under this compile flag, we will save it in a sdbl file :
        return SaveLib(lib_name) ;
    }
#endif

    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Get the library :
    VeriLibrary *work_lib = GetLibrary(lib_name, 0) ;
    if (!work_lib) {
        tmp_print_node.Error("The library %s was not found", lib_name) ; // VIPER #3142
        return 0;
    }

    // First check to see if a path was specified for this lib
    char *file_path = 0 ;
    if (GetLibraryPath(lib_name)) {
        // SDB explicit path
        file_path = Strings::save(GetLibraryPath(lib_name), "/") ;
    } else if (GetDefaultLibraryPath()) {
        // SDB default path
        file_path = Strings::save(GetDefaultLibraryPath(), "/", lib_name, "/") ;
    } else {
        tmp_print_node.Error("No library search path could be found. Please set a search path to where module can be saved") ;
        return 0 ;
    }

    // Now create the directory path
    if (!FileSystem::Mkdir(file_path)) {
        tmp_print_node.Error("Save failed due to mkdir failure") ;
        Strings::free(file_path) ;
        return 0 ;
    }

    // Determine sdb destination path based on library name and module name

    // Compile/run-time flag settings :
    unsigned current_product_flags = _current_product_flags ; // compile flag setting
    // Add run-time flags that affect the stream :
    if (RuntimeFlags::GetVar("veri_preserve_comments")) current_product_flags |= PRESERVE_COMMENTS_ENABLED ;

    // Flag needed when module_name is valid.
    unsigned specific_module = (module_name) ? 1 : 0 ;

    MapIter mi ;
    VeriModule *module ;
    FOREACH_VERILOG_MODULE_IN_LIBRARY(work_lib, mi, module) {
        if (!module) continue ;
        // If module_name is valid, then only save that module
        if (specific_module) {
            if (!module_name || !Strings::compare(module_name, module->Name())) continue ;
            module_name = 0 ;
        }

        // Do NOT save the root module if nobody depends on it :
        if (module->IsRootModule()) {
            VeriScope *root_scope = module->GetScope() ;
            // VIPER #3729: Don't check just the declared items, check the used by
            // set. If no one is using this root module, then don't save it.
            Set *users = (root_scope) ? root_scope->GetUsedBy() : 0 ;
            if (!users || !users->Size()) continue ;
        }

        if (module->IsStaticElaborated() || module->IsCompiled() || module->IsAnalyzed()) {
            // For elaborated modules (static or RTL elaborated), ignore the module
            // as if a module contains hierarchical ids then such ids will get resolved
            // for any kind of elaboration or with AnalyzeFull. While restoring,
            // the save restore utility would not be able to locate the cross-module hierarchical ids.
            tmp_print_node.Comment("Ignoring elaborated Verilog module which cannot be saved %s.%s", work_lib->GetName(), module->GetName()) ;
            // Message::Msg(VERIFIC_COMMENT, 0, 0, "Ignoring elaborated Verilog module which cannot be saved %s.%s", work_lib->GetName(), module->GetName()) ;
            continue ;
        }

        // Append file name to end of path
        //char *full_file_path = Strings::save(file_path, module->Name(), ".sdb") ;

        // Create SaveRestore object
        // VIPER #3465: Create different sdb files (not just differing by case)
        // i.e. for modules 'm' and 'M' create sdb files m.sdb and @M.sdb respectively
        char *sdb_name = SaveRestore::GetDumpFileName(module->Name(), ".sdb" /* extension */) ; // VIPER 3465
        char *full_file_path = Strings::save(file_path, sdb_name) ;

        SaveRestore save_restore(full_file_path, 0 /* Save */, 1, veri_file::GetProtectionObject()) ;
        Strings::free(full_file_path) ;
        Strings::free(sdb_name) ; // Free the allocated string returned by SaveRestore::GetDumpFileName function
        if (!save_restore.IsOK()) {
            // Error Message is already pumped out
            Strings::free(file_path) ;
            return 0 ;
        }

        // Save the SaveRestore versions. Needed for restore checks.

        // Dump veri parse-tree identification
        save_restore.SaveInteger((int)VERI_PARSE_TREE) ;
        // Dump verilog persistence version number
        save_restore.SaveInteger(VERI_PERSISTENCE_VER_NUM) ;

        // Dump an int (32 bits) for potential product flag information (used for backward compatibility)
        save_restore.SaveInteger(current_product_flags) ;

        // Dump lib and module name, so on the restore side of things it knows where to load module.
        save_restore.SaveString(lib_name) ;
        save_restore.SaveString(module->Name()) ;

        MapIter iter ;
        VeriLibrary *lib ;
        // Save the number of libraries in the map
        save_restore.SaveInteger(_all_libraries ? _all_libraries->Size() : 0) ;
        // Iterate through all the libraries and save them
        FOREACH_VERILOG_LIBRARY(iter, lib) {
            if (lib) VeriLibrary::SaveLibPtr(save_restore, lib) ;
        }

        tmp_print_node.Comment("Saving Verilog parse-tree %s.%s into %s", work_lib->GetName(), module->GetName(), save_restore.FileName()) ;
        // Message::Msg(VERIFIC_COMMENT, 0, 0, "Saving Verilog parse-tree %s.%s into %s", work_lib->GetName(), module->GetName(), save_restore.FileName()) ;

        // Before starting with the saving of modules check if there has been already some error
        if (!save_restore.IsOK()) {
            Strings::free(file_path) ;
            return 0;
        }

        // Save the design module
        VeriTreeNode::SaveNodePtr(save_restore, module) ;

        // If an error occurred during saving, delete appropriately and return zero
        if (!save_restore.IsOK()) {
            Strings::free(file_path) ;
            return 0;
        }
        // If the name of a module has been specified then no need to iterate through
        // the rest of the modules in the library.
        if (specific_module) break ;
    }

    Strings::free(file_path) ;

    // Make sure if module_name was provided, then it actually got saved.  (if module_name is still
    // valid here, then it was not saved)
    if (specific_module && module_name) {
        tmp_print_node.Error("The module %s was not found in library %s", module_name, lib_name) ;
        return 0 ;
    }

    return 1 ;
}

/* ----------------------------------------- */

unsigned
veri_file::Restore(const char *lib_name, const char *module_name, unsigned silent /*=0*/)
{
    // Error count is not relied upon during the saving/restoring of a SDB, therefore clearing the error
    // count is not needed.  The SaveRestore::IsOK() is to be used to determine if an 'error' has occurred.

    if (!lib_name || !module_name) return 0;

    VeriTreeNode tmp_print_node ;   // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Determine full path to sdb
    char *sdb_name = SaveRestore::GetDumpFileName(module_name, ".sdb" /* extension */) ; // VIPER 3465
    // First check to see if a path was specified for this lib
    char *file_path = 0 ;
    if (GetLibraryPath(lib_name)) {
        file_path = Strings::save(GetLibraryPath(lib_name), "/", sdb_name) ;
    } else {
        const char *default_lib_path = GetDefaultLibraryPath() ;
        if (default_lib_path) {
            // Use default search path
            file_path = Strings::save(default_lib_path, "/", lib_name, "/", sdb_name) ;
        }
    }
    Strings::free(sdb_name) ; // Free the allocated string returned by SaveRestore::GetDumpFileName function

    if (!file_path) {
        if (!silent) tmp_print_node.Error("Verilog library search path not set; cannot restore") ;
        return 0 ;
    }

    // Don't need to do a SaveRestore::FileExists(file_path) check here, since error
    // will be pushed out by SaveRestore::SaveRestore if file doesn't exist.

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    long startpos = 0 ;
    if (!SaveRestore::FileExists(file_path)) {
        // Try the library-wide dump file
        startpos = PosInLibFile(lib_name, module_name) ;

        // If startpos is 0, then unit is not in a lib side file.
        // But if it IS, then adjust the restore file to the restore file name.
        // The fact that 'startpos' is set also means that we are dealing with a lib file, and not a unit-only file.
        if (startpos) {
            // Otherwise, create the lib file name that this module is in :
            // First check to see if a path was specified for this lib
            Strings::free(file_path) ;
            file_path = 0 ;
            if (veri_file::GetLibraryPath(lib_name)) {
                file_path = Strings::save(veri_file::GetLibraryPath(lib_name), "/", lib_name, ".sdbl") ;
            } else if (veri_file::GetDefaultLibraryPath()) {
                // Use default search path
                file_path = Strings::save(veri_file::GetDefaultLibraryPath(), "/", lib_name, "/", lib_name, ".sdbl") ;
            }
        }
    }
#endif

    // Return silently if file doesn't exist and 'silent' was specified, else SaveRestore::SaveRestore will generate error.
    if (silent && !SaveRestore::FileExists(file_path)) {
        Strings::free(file_path) ; // use save_restore.FileName() from now on
        return 0 ;
    }

    // Open the file stream
    SaveRestore save_restore(file_path, 1 /* Restore */, 1, veri_file::GetProtectionObject()) ;
    Strings::free(file_path) ; // use save_restore.FileName() from now on
    if (!save_restore.IsOK()) {
        return 0 ; // Error Message is already pumped out
    }

    // Verify that file contains a binary VERI parse-tree
    if ((unsigned)save_restore.RestoreInteger() != VERI_PARSE_TREE) {
        tmp_print_node.Error("%s does not contain a known Verilog parse-tree format", save_restore.FileName()) ;
        return 0 ;
    }

    // get version number :
    int ver_num = save_restore.RestoreInteger() ;

    // RD: Binary restore back-ward compatibility activated since version 0x0ab00029.
    // Error out on versions before that or after the current persistence number :
    if ((ver_num < 0x0ab00029) || (ver_num > VERI_PERSISTENCE_VER_NUM)) {
        char tmp1[12];
        char tmp2[12];
        sprintf(tmp1, "0x%x", ver_num) ;
        sprintf(tmp2, "0x%x", VERI_PERSISTENCE_VER_NUM) ;
        tmp_print_node.Error("%s has persistence version number %s, which is outside the range 0x0ab00029 to %s that this executable can restore", save_restore.FileName(), tmp1, tmp2) ;
        return 0 ;
    }

    // Override SaveRestore version with VERI binary save/restore version.
    // This is needed (later) for backward compatibility.
    save_restore.SetVersion(ver_num) ;

    //Restore flags (used for backward compatibility)
    unsigned flags = save_restore.RestoreInteger() ;
    save_restore.SetUserFlags(flags) ;

    // Warn on product mismatches where the program that created the stream may have saved formats that not supported in the current program.
    if (_current_product_flags != flags) {
        if (~_current_product_flags & flags & PSL_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a PSL-enabled Verilog analyzer, since the current Verilog analyzer is not PSL-enabled, there is a high probability that a restore error will occur due to unknown PSL constructs", save_restore.FileName()) ;
        }
        if (~_current_product_flags & flags & AMS_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a AMS-enabled Verilog analyzer, since the current Verilog analyzer is not AMS-enabled, there is a high probability that a restore error will occur due to unknown AMS constructs", save_restore.FileName()) ;
        }
        if (~_current_product_flags & flags & SYS_VERILOG_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a System Verilog-enabled Verilog analyzer, since the current Verilog analyzer is not System Verilog-enabled, there is a high probability that a restore error will occur due to unknown System Verilog constructs", save_restore.FileName()) ;
        }
    }

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // if startpos is set, then we need to go to that position (in the lib file) for the unit :
    if (startpos) {
        save_restore.SetPos(startpos) ;
    }
#endif

    // Restore library name
    char *lib_name2 = save_restore.RestoreString() ;
    Strings::free(lib_name2) ; // Original (logical) library name is not important. We restore requested logical library name

    // Restore design module name
    char *module_name2 = save_restore.RestoreString() ;
    Strings::free(module_name2) ; // Original (logical) module name is not important any more.

    // Restore the number of libraries
    unsigned lib_map_size = save_restore.RestoreInteger() ;
    if (!_all_libraries) _all_libraries = new Map(STRING_HASH, lib_map_size) ; // Create new map
    unsigned i ;
    for (i=0; i<lib_map_size; i++) {
        (void) VeriLibrary::RestoreLibPtr(save_restore) ; // Restore the library pointer
    }

    tmp_print_node.Comment("Restoring Verilog parse-tree %s.%s from %s", lib_name, module_name, save_restore.FileName()) ;
    // Message::Msg(VERIFIC_COMMENT, 0, 0, "Restoring Verilog parse-tree %s.%s from %s", lib_name, module_name, save_restore.FileName()) ;

    // Get handle to the library which this module will be added to.
    VeriLibrary *library = GetLibrary(lib_name, 1 /* Create if necessary */, 1 /* Case sensitive */) ;

    // Finally, really restore the module/package :
    VeriModule *module = (VeriModule *)VeriTreeNode::RestoreNodePtr(save_restore) ;

    // If an error occurred, delete appropriately and return zero
    if (!module || !save_restore.IsOK()) {
        tmp_print_node.Error("%s.%s failed to restore", lib_name, module_name) ;
        delete module ;
        return 0 ;
    }

    // NOTE : We could check/warn here if the module name is the same as the expected module name

    VERIFIC_ASSERT(library) ; // it must be created if not existing

    // Add the module to the requested library.
    (void) library->AddModule(module, 0 /*refuse_on_error*/) ;

    return 1 ;
}

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE

/*************************************************************************/
/*             VIPER 2944 : library-wide save/restore file               */
/*************************************************************************/
long
veri_file::PosInLibFile(const char *lib_name, const char *unit_name)
{
    if (!lib_name) return 0 ;

    // First check if we have visited this library before, and have a unit_name->pos_in_file Map for it.
    if (!_lib_unit_map) _lib_unit_map = new Map(STRING_HASH) ; // Map of lib_name->(Map of unitnames) to quickly check if a unit is in a lib-wide map file.

    // Check if this library is done before :
    Map *unit_map = (Map*)(_lib_unit_map->GetValue(lib_name));

    // get from the unit map.
    if (unit_map) {
        long pos = 0 ;
        if (unit_name) {
            // Return the position of the unit in the file, or 0 if not found.
            pos = (long)(unit_map->GetValue(unit_name));
        } else {
            // If there is NO unit name, then assume user is asking for the position of the FIRST unit in the lib file.
            pos = (long)(unit_map->GetValueAt(0)) ;
        }
        return pos ;
    }

    // Now that we have a library unit map, call myself again.
    // It will then pick up unit position from the map :
    return RestoreLibUnitMap(lib_name) ? PosInLibFile(lib_name, unit_name) : 0 ; // 0 means : unit not found.
}

const char *
veri_file::FindCaseInsensitiveInLibFile(const char *lib_name, const char *unit_name)
{
    if (!lib_name || !unit_name) return 0 ;

    // Try normal finding first, it will restore the map if not already restored:
    long pos = PosInLibFile(lib_name, unit_name) ;
    if (pos) return unit_name ;

    // Now check case insensitive way:
    // Must be valid here since we called PosInLibFile above
    VERIFIC_ASSERT(_lib_unit_map) ;

    // Check if this library is done before:
    Map *unit_map = (Map*)(_lib_unit_map->GetValue(lib_name));
    if (!unit_map) return 0 ; // Not found

    // Now find the name in case insensitive way:
    MapIter mi ;
    const char *name ;
    FOREACH_MAP_ITEM(unit_map, mi, &name, 0) {
        if (Strings::compare_nocase(name, unit_name)) return name ; // Found
    }

    return 0 ; // Now found
}

unsigned
veri_file::RestoreLibUnitMap(const char *lib_name)
{
    // First check if we have visited this library before, and have a unit_name->pos_in_file Map for it.
    if (!_lib_unit_map) _lib_unit_map = new Map(STRING_HASH) ; // Map of lib_name->(Map of unitnames) to quickly check if a unit is in a lib-wide map file.

    // Check if this library is done before :
    Map *unit_map = (Map*)(_lib_unit_map->GetValue(lib_name));
    if (unit_map) return 1 ;

    // If there is no unit_map yet, create one from the side-file for this library (if there).
    // First create an empty unit_map and insert under this library name.
    unit_map = new Map(STRING_HASH);
    (void) _lib_unit_map->Insert(Strings::save(lib_name), unit_map); // save library name, since argument pointer is out of our control.

    // Check to see if there is a sidefile for this lib. Build its name :
    char *side_file_path = 0 ;
    if (veri_file::GetLibraryPath(lib_name)) {
        side_file_path = Strings::save(veri_file::GetLibraryPath(lib_name), "/", lib_name, ".sdbx") ;
    } else if (veri_file::GetDefaultLibraryPath()) {
        // Use default search path
        side_file_path = Strings::save(veri_file::GetDefaultLibraryPath(), "/", lib_name, "/", lib_name, ".sdbx") ;
    }

    // Check if this file exists :
    if (!SaveRestore::FileExists(side_file_path)) {
        Strings::free(side_file_path) ;
        return 0 ;
    }

    // Open side file and check if we have 'unitname' in there.
    SaveRestore side_file(side_file_path, 1 /* Restore */) ;
    Strings::free(side_file_path) ; // use side_file_path.FileName() from now on

    if (!side_file.IsOK()) {
        return 0 ; // Error Message is already pumped out
    }

    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Verify that side file is correct
    if ((unsigned)side_file.RestoreInteger() != VERI_PARSE_TREE) {
        tmp_print_node.Error("%s does not contain a known Verilog parse-tree format", side_file.FileName()) ;
        return 0 ;
    }

    // get version number :
    int ver_num = side_file.RestoreInteger() ;

    // RD: Binary restore back-ward compatibility activated since version 0x0ab00029.
    // Error out on versions before that or after the current persistence number :
    if ((ver_num < 0x0ab00029) || (ver_num > VERI_PERSISTENCE_VER_NUM)) {
        char tmp1[12];
        char tmp2[12];
        sprintf(tmp1, "0x%x", ver_num) ;
        sprintf(tmp2, "0x%x", VERI_PERSISTENCE_VER_NUM) ;
        tmp_print_node.Error("%s has persistence version number %s, which is outside the range 0x0ab00029 to %s that this executable can restore", side_file.FileName(), tmp1, tmp2) ;
        return 0 ;
    }

    // Finally : Fill the 'unit_map' with all available unit->pos associations from the side file
    char *on_file_unit_name ;
    long pos ;
    while (1) {
        // Get next unit name :
        on_file_unit_name = side_file.RestoreString() ; // downcased name of the unit..
        if (!on_file_unit_name) break ; // there are no more units in this file.

        // Get its file position :
        pos = side_file.RestoreLong() ; // unit's position in the file.

        // Insert in the unit map, so we can use it next time.
        if (!unit_map->Insert(on_file_unit_name,(void*)pos)) { // give (the memory of) the string to the map.
            // Insert failed.. already existing in the map
            Strings::free(on_file_unit_name) ;
        }
    }

    return 1 ;
}

/* static */
void
veri_file::ClearLibUnitMap()
{
    // clear internal tables used to speedup lookups in library-wide save/restore files
    if (!_lib_unit_map || _lib_unit_map->Size()==0) return ;

    // Clear the lib_unit_map. Free all its elements memory :
    // Map of lib_name->(Map of unitnames->pos_in_libfile)
    MapIter mi, mii ;
    Map *unit_map ;
    char *lib_name, *unit_name ;
    FOREACH_MAP_ITEM(_lib_unit_map, mi, &lib_name, &unit_map) {
        // unit_map is a Map of unitnames->pos_in_libfile.
        FOREACH_MAP_ITEM(unit_map, mii, &unit_name, 0/* pos is 'long' (not allocated) */) {
            Strings::free(unit_name) ;
        }
        delete unit_map ;
        Strings::free(lib_name) ;
    }

    // delete the table, and re-set the _lib_unit_map pointer.
    // NOTE : Instead of this, we could just do a _lib_unit_map->Reset().
    delete _lib_unit_map ;
    _lib_unit_map = 0 ;
}

/* static */
unsigned
veri_file::SaveLib(const char *lib_name)
{
    // Verify arguments
    if (!lib_name) return 0;

    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Get the library :
    VeriLibrary *work_lib = GetLibrary(lib_name, 0) ;
    if (!work_lib) {
        tmp_print_node.Error("The library %s was not found", lib_name) ; // VIPER #3142
        return 0;
    }

    // First check to see if a path was specified for this lib
    char *file_path ;
    if (GetLibraryPath(lib_name)) {
        file_path = Strings::save(GetLibraryPath(lib_name), "/") ;
    } else if (GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(GetDefaultLibraryPath(), "/", lib_name, "/") ;
    } else {
        tmp_print_node.Error("No library search path could be found. Please set a search path to where module can be saved") ;
        return 0 ;
    }

    // Now create the directory path
    if (!FileSystem::Mkdir(file_path)) {
        tmp_print_node.Error("Save failed due to mkdir failure") ;
        Strings::free(file_path) ;
        return 0 ;
    }

    // Determine sdb destination path based on library name and module name

    // We are now about to create a sdbl file with its sdbx sidefile for this library libname.
    // First remove existing library entry in the _lib_unit_map for this library (if there from a previous saved library/sidefile).
    // Lets just clear the whole map (easier) :
    ClearLibUnitMap() ;

    // Create sdbl(library)/sdbx(index) file for this library:
    // Append file name to end of path. Target name of file is "<libname>.sdbl".
    char *full_file_path = Strings::save(file_path, lib_name, ".sdbl") ;
    char *side_file_path = Strings::save(file_path, lib_name, ".sdbx") ; // index file (or unit names->file position)
    Strings::free(file_path) ;

    // Create SaveRestore object (open the file) :
    SaveRestore save_restore(full_file_path, 0 /* Save */, 1, veri_file::GetProtectionObject()) ;
    Strings::free(full_file_path) ;

    if (!save_restore.IsOK()) {
        // Error Message is already pumped out
        Strings::free(side_file_path) ;
        return 0 ;
    }

    // Open file 'side_file_path' as a file stream :
    SaveRestore side_file(side_file_path, 0 /* save */) ;
    Strings::free(side_file_path) ;

    if (!side_file.IsOK()) {
        // Error Message is already pumped out
        return 0 ;
    }

    // Dump Verilog parse-tree identification
    save_restore.SaveInteger(VERI_PARSE_TREE) ;

    // Dump Verilog persistence version number
    save_restore.SaveInteger(VERI_PERSISTENCE_VER_NUM) ;

    // Same for the side file :
    side_file.SaveInteger(VERI_PARSE_TREE) ; // should probably be different id number for lib idx files.

    // Push version number out :
    side_file.SaveInteger(VERI_PERSISTENCE_VER_NUM) ; // could be lib idx file specific number

    // Compile/run-time flag settings :
    unsigned current_product_flags = _current_product_flags ; // compile flag setting
    // Add run-time flags that affect the stream :
    if (RuntimeFlags::GetVar("veri_preserve_comments")) current_product_flags |= PRESERVE_COMMENTS_ENABLED ;

    // Dump an int (32 bits) for product configuration information of the current program.
    save_restore.SaveInteger(current_product_flags) ;

    MapIter mi ;
    VeriModule *module ;
    FOREACH_VERILOG_MODULE_IN_LIBRARY(work_lib, mi, module) {
        if (!module) continue ;

        // Do NOT save the root module if nobody depends on it :
        if (module->IsRootModule()) {
            VeriScope *root_scope = module->GetScope() ;
            // VIPER #6238: Do not depend on decl-area size, check used-by set explicitly:
            //Map *decl_area = (root_scope) ? root_scope->DeclArea() : 0 ;
            //if (!decl_area || decl_area->Size()==0) continue ;
            Set *used_by = (root_scope) ? root_scope->GetUsedBy() : 0 ;
            if (!used_by || !used_by->Size()) continue ; // No one is using this
        }

        if (module->IsStaticElaborated() || module->IsCompiled() || module->IsAnalyzed()) {
            // For elaborated modules (static or RTL elaborated), ignore the module
            // as if a module contains hierarchical ids then such ids will get resolved
            // for any kind of elaboration or with AnalyzeFull. And while restoring,
            // the save restore utility will not be able to locate the cross-module
            // hierarchical ids.
            tmp_print_node.Comment("Ignoring elaborated Verilog module which cannot be saved %s.%s", work_lib->GetName(), module->GetName()) ;
            // Message::Msg(VERIFIC_COMMENT, 0, 0, "Ignoring elaborated Verilog module which cannot be saved %s.%s", work_lib->GetName(), module->GetName()) ;
            continue ;
        }

        // reset the pointer registrations :
        save_restore.Reset() ;

        // Save the name of the unit and the position where we are now into the side file :
        side_file.SaveString(module->Name()) ; // downcased name..
        long pos = save_restore.GetPos() ; // current file position in the main vdb file
        side_file.SaveLong(pos) ;

        // Dump lib and module name, so on the restore side of things it knows where to load module.
        save_restore.SaveString(lib_name) ;
        save_restore.SaveString(module->Name()) ;

        MapIter iter ;
        VeriLibrary *lib ;
        // Save the number of libraries in the map
        save_restore.SaveInteger(_all_libraries ? _all_libraries->Size() : 0) ;
        // Iterate through all the libraries and save them
        FOREACH_VERILOG_LIBRARY(iter, lib) {
            if (lib) VeriLibrary::SaveLibPtr(save_restore, lib) ;
        }

        tmp_print_node.Comment("Saving Verilog parse-tree %s.%s into %s", work_lib->GetName(), module->GetName(), save_restore.FileName()) ;
        // Message::Msg(VERIFIC_COMMENT, 0, 0, "Saving Verilog parse-tree %s.%s into %s", work_lib->GetName(), module->GetName(), save_restore.FileName()) ;

        // Before starting with the saving of modules check if there has been already some error
        if (!save_restore.IsOK()) {
            return 0;
        }

        // Save the design module
        VeriTreeNode::SaveNodePtr(save_restore, module) ;

        // If an error occurred during saving, delete appropriately and return zero
        if (!save_restore.IsOK()) {
            return 0;
        }
    }

    // Dump a NULL pointer string so that we know there are no more units in this file :
    save_restore.SaveString(0) ;

    // and into the side file too :
    side_file.SaveString(0) ;

    // Successfully dumped the whole library.
    return 1 ;
}

/* static */
unsigned
veri_file::RestoreLib(const char *lib_name)
{
    if (!lib_name) return 0;

    // Determine full path to vdb
    char *file_path = 0 ;
    // First check to see if a path was specified for this lib
    if (veri_file::GetLibraryPath(lib_name)) {
        file_path = Strings::save(veri_file::GetLibraryPath(lib_name), "/", lib_name, ".sdbl") ;
    } else if (veri_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(veri_file::GetDefaultLibraryPath(), "/", lib_name, "/", lib_name, ".sdbl") ;
    }

    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    if (!file_path) {
        tmp_print_node.Error("Verilog library search path not set; cannot restore") ;
        return 0;
    }

    // Open the file stream
    SaveRestore save_restore(file_path, 1 /* Restore */, 1, veri_file::GetProtectionObject()) ;
    Strings::free(file_path) ; // use save_restore.FileName() from now on

    if (!save_restore.IsOK()) {
        return 0 ; // Error Message is already pumped out
    }

    // Verify that file contains a binary VERI parse-tree
    if ((unsigned)save_restore.RestoreInteger() != VERI_PARSE_TREE) {
        tmp_print_node.Error("%s does not contain a known VERI parse-tree format", save_restore.FileName()) ;
        save_restore.FileNotOk() ; // Disable SaveRestore object
        return 0 ;
    }

    // Restore veri persistence version number
    unsigned ver_num = save_restore.RestoreInteger() ;

    // RD: Binary restore back-ward compatibility activated since version 0x0ab00029.
    // Error out on versions before that or after the current persistence number :
    if ((ver_num < 0x0ab00029) || (ver_num > VERI_PERSISTENCE_VER_NUM)) {
        char tmp1[12];
        char tmp2[12];
        sprintf(tmp1, "0x%x", ver_num) ;
        sprintf(tmp2, "0x%x", VERI_PERSISTENCE_VER_NUM) ;
        tmp_print_node.Error("%s has persistence version number %s, which is outside the range 0x0ab00029 to %s that this executable can restore", save_restore.FileName(), tmp1, tmp2) ;
        return 0 ;
    }

    // Override SaveRestore version with VERI binary save/restore version.
    // This is needed for backward compatibility.
    save_restore.SetVersion(ver_num) ;

    // Restore flags (used for backward compatibility)
    unsigned flags = save_restore.RestoreInteger() ;
    save_restore.SetUserFlags(flags) ;

    if (_current_product_flags != flags) {
        // Warn on product mismatches where the program that created the stream may have saved formats that not supported in the current program.
        if (~_current_product_flags & flags & PSL_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a PSL-enabled Verilog analyzer, since the current Verilog analyzer is not PSL-enabled, there is a high probability that a restore error will occur due to unknown PSL constructs", save_restore.FileName()) ;
        }
        if (~_current_product_flags & flags & AMS_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a AMS-enabled Verilog analyzer, since the current Verilog analyzer is not AMS-enabled, there is a high probability that a restore error will occur due to unknown AMS constructs", save_restore.FileName()) ;
        }
        if (~_current_product_flags & flags & SYS_VERILOG_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a System Verilog-enabled Verilog analyzer, since the current Verilog analyzer is not System Verilog-enabled, there is a high probability that a restore error will occur due to unknown System Verilog constructs", save_restore.FileName()) ;
        }
    }

    (void) veri_file::RestoreLibUnitMap(lib_name) ;
    if (!_lib_unit_map) return 0 ;

    Map *unit_map = (Map*)(_lib_unit_map->GetValue(lib_name)) ;

    MapIter mii ;
    long pos ;
    char *unit_name ;
    FOREACH_MAP_ITEM(unit_map, mii, &unit_name, &pos) {
        if (!unit_name) continue ;

        // reset the pointer registrations :
        save_restore.Reset() ;

        save_restore.SetPos(pos) ;

        // Restore library name first.
        // If it is there, then there is a unit coming up. Otherwise, we are at the end of the units.
        char *lib_name2 = save_restore.RestoreString() ;
        if (!lib_name2) break ; // done. No more units in this file.
        Strings::free(lib_name2) ; // lib_name2 not important : we will store unit in library 'lib_name'.

        // Restore primary unit name
        char *module_name = save_restore.RestoreString() ;

        tmp_print_node.Comment("Restoring Verilog parse-tree %s.%s from %s", lib_name, module_name, save_restore.FileName()) ;
        // Message::Msg(VERIFIC_COMMENT, 0, 0, "Restoring Verilog parse-tree %s.%s from %s", lib_name, module_name, save_restore.FileName()) ;

        // Restore the number of libraries
        unsigned lib_map_size = save_restore.RestoreInteger() ;
        if (!_all_libraries) _all_libraries = new Map(STRING_HASH, lib_map_size) ; // Create new map
        unsigned i ;
        for (i=0; i<lib_map_size; i++) {
            (void) VeriLibrary::RestoreLibPtr(save_restore) ; // Restore the library pointer
        }

        // Get handle to the library which this module will be added to.
        VeriLibrary *library = GetLibrary(lib_name, 1 /* Create if necessary */, 1 /* Case sensitive */) ;

        // Finally, really restore the module/package :
        VeriModule *module = (VeriModule *)VeriTreeNode::RestoreNodePtr(save_restore) ;

        // If an error occurred, delete appropriately and return zero
        if (!module || !save_restore.IsOK()) {
            tmp_print_node.Error("%s.%s failed to restore", lib_name, module_name) ;
            delete module ;
            Strings::free(module_name);
            return 0 ;
        }

        // NOTE : We could check/warn here if the module name is the same as the expected module name

        VERIFIC_ASSERT(library) ; // it must be created if not existing

        // Add the module to the requested library.
        (void) library->AddModule(module, 0 /*refuse_on_error*/) ;

        Strings::free(module_name) ;
    }

    return 1 ;
}

#endif // VERILOG_USE_LIB_WIDE_SAVERESTORE

/*************************************************************************/
/*                  LIBRARY SEARCH PATH METHODS                          */
/*************************************************************************/

unsigned
veri_file::VerifyAndModifyPath(char **path)
{
    // Corner case
    if (!(*path) || !strcmp(*path, "")) {
        Strings::free(*path) ;
        // For an empty path set the current directory as the path
        *path = Strings::save(".") ;
        return 1 ;
    }

    // Make sure that path does NOT end with a "/"
    char *p ;
    for(p = *path + Strings::len(*path) - 1; *p == '/'; p--) { *p = 0 ; }

    return FileSystem::IsDir(*path) ;
}

#if 0
unsigned
veri_file::IsSDBValid(const char *lib_name, const char *module_name)
{
    if (!lib_name || !module_name) return 0 ;

    // Determine full path to sdb
    char *file_path = 0 ;
    char *sdb_name = SaveRestore::GetDumpFileName(module_name, ".sdb" /* extension */) ; // VIPER 3465: Special handling for uppercase chars and escape chars
    // First check to see if a path was specified for this lib
    if (veri_file::GetLibraryPath(lib_name)) {
        file_path = Strings::save(veri_file::GetLibraryPath(lib_name), "/", sdb_name) ;
    } else if (veri_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(veri_file::GetDefaultLibraryPath(), "/", lib_name, "/", sdb_name) ;
    }
    Strings::free(sdb_name) ; // Free the allocated string
    if (!file_path) return 0 ; // no library path set

    // Now check to see if this file exists
    if (SaveRestore::FileExists(file_path)) {
        Strings::free(file_path) ;
        return 1 ;
    }
    // Here, file does not exist.
    Strings::free(file_path) ;

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // Check if the unit is in a library-wide side file :
    long pos = PosInLibFile(lib_name,module_name) ;
    if (pos) {
        return 1 ; // yes. It is in a library side-file.
    }
#endif // VERILOG_USE_LIB_WIDE_SAVERESTORE

    return 0 ; // cannot find this module on disk
}
#endif

/* ----------------------------------------- */

// DEFAULT SEARCH PATH :

// static
char *
veri_file::GetDefaultLibraryPath()
{
    return _default_library_path ;
}

// static
unsigned
veri_file::SetDefaultLibraryPath(const char *path)
{
    // For an empty path, nothing to set and so return.
    if (!path) return 0 ;

    // Check if there is any default path. If exists then clear it.
    if (_default_library_path) {
        ClearDefaultLibraryPath() ;
    }

    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Set the current path to be the default lib path.
    _default_library_path = FileSystem::Convert2AbsolutePath(path, 1) ;
    if (!_default_library_path || !VerifyAndModifyPath(&_default_library_path)) {
        Strings::free(_default_library_path) ;
        _default_library_path = 0 ;
        tmp_print_node.Error("The path \"%s\" does not exist", path) ;
        return 0 ;   // path is not a valid path
    }

    tmp_print_node.Info("The default veri library search path is now \"%s\"", _default_library_path) ;
    // Message::Info(0, "The default veri library search path is now \"", _default_library_path, "\"") ;

    return 1 ;
}

// static
void
veri_file::ClearDefaultLibraryPath()
{
    Strings::free(_default_library_path) ;
    _default_library_path = 0 ;
#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // also clear static internal table (if there) that deals with library-wide save/restore files
    ClearLibUnitMap() ;
#endif
}

/* ----------------------------------------- */

// LIBRARY -> SEARCH PATH MAPPING:

// static
char *
veri_file::GetLibraryPath(const char *library)
{
    if (!_map_library_paths) return 0 ;

    return (char*)(_map_library_paths->GetValue(library)) ;
}

// static
unsigned
veri_file::AddLibraryPath(const char *library, const char *dir_name)
{
    // All allocation and deallocation of map strings must be handled from within this function
    char *lib_key = Strings::save(library) ;
    char *dir_val = FileSystem::Convert2AbsolutePath(dir_name, 1) ;

    // Force lib_key to lowercase, unless it is escaped
    // VIPER #3246 : Comment out the following line as Verilog is a case sensitive language and so the lib_key
    // should not be converted to lower case.
    //if (*lib_key != '\\') { for(char *str=lib_key ; *str ; str++) { *str = (char)::tolower(*str) ; } }

    (void) VerifyAndModifyPath(&dir_val) ;  // Don't need to check if it is a valid path.  This will be done at the Save/Restore time.

    // Remove old entry if it was there
    (void) RemoveLibraryPath(library) ;

    // Insert the new entry
    if (!_map_library_paths) _map_library_paths = new Map(STRING_HASH) ;
    (void) _map_library_paths->Insert(lib_key, dir_val) ;

    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    tmp_print_node.Info("The veri library search path for library \"%s\" is now \"%s\"", lib_key, dir_val) ;
    // Message::Info(0, "The veri library search path for library \"", lib_key, "\" is now \"", dir_val, "\"") ;

    return 1 ;
}

// static
unsigned
veri_file::RemoveLibraryPath(const char *library)
{
    if (!library || !_map_library_paths) return 0 ;

    MapItem *map_item = _map_library_paths->GetItem(library) ;
    if (!map_item) return 0 ;

    char *orig_key = (char*)map_item->Key() ;
    char *orig_val = (char*)map_item->Value() ;
    (void) _map_library_paths->Remove(map_item) ;
    Strings::free(orig_key) ;
    Strings::free(orig_val) ;
#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // also clear static internal table (if there) that deals with library-wide save/restore files
    ClearLibUnitMap() ;
#endif
    return 1 ;
}

// static
void
veri_file::ResetAllLibraryPaths()
{
    MapIter mi ;
    char *key, *val ;
    FOREACH_MAP_ITEM(_map_library_paths, mi, &key, &val) {
        Strings::free(key) ;
        Strings::free(val) ;
    }
    //_map_library_paths->Reset() ; // Clear all items in the map (does not deallocate memory)
    delete _map_library_paths ;
    _map_library_paths = 0 ;
#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // also clear static internal table (if there) that deals with library-wide save/restore files
    ClearLibUnitMap() ;
#endif
}

//--------------------------------------------------------------
// #included from VeriTreeNode.h
//--------------------------------------------------------------

// The following DATA_TO_FOLLOW constant is only used by the VeriTreeNode utility methods.
static const unsigned DATA_TO_FOLLOW = 0xb0000001 ;   // Specifies that the next data to parse is class data

/****************************************************************/
/***************** Utility Persistence Methods ******************/
/****************************************************************/

// static
void
VeriTreeNode::SaveArray(SaveRestore &save_restore, const Array *array)
{
    // Dump flag specifying whether Array actually exists.
    if (!array) {
        // The array pointer is NULL
        save_restore.SaveChar(0) ;
        return ;
    } else {
       // There is an array, it might have zero elements
       save_restore.SaveChar(1) ;
    }

    // Dump array size
    save_restore.SaveInteger(array->Size()) ; // Save array size

    // Dump elements appropriately
    VeriTreeNode *node ;
    int i ;
    FOREACH_ARRAY_ITEM(array, i, node) {
        // NOTE: a NULL array element is allowed!!!
        SaveNodePtr(save_restore, node) ;
    }
}

// static
Array *
VeriTreeNode::RestoreArray(SaveRestore &save_restore)
{
    // Restore flag specifying whether Array originally existed.
    char is_array = save_restore.RestoreChar() ;
    if (!is_array) return 0 ;

    // Restore array size
    unsigned array_size = save_restore.RestoreInteger() ;
    Array *array = new Array(array_size) ;

    // Restore elements (VeriTreeNode* in this case)
    VeriTreeNode *node ;
    for(unsigned i = 0 ; i < array_size ; i++) {
        // NOTE: a NULL array element is allowed!!!
        node = RestoreNodePtr(save_restore) ;
        array->Insert(node) ;
    }

    return array ;
}

/* ----------------------------------------- */

void
VeriTreeNode::SaveStringArray(SaveRestore &save_restore, const Array *array)
{
    // Array size is 0
    if (!array || !array->Size()) {
        save_restore.SaveInteger(0) ;
        return ;
    }

    // Dump array size
    save_restore.SaveInteger(array->Size()) ; // Save array size

    // Dump elements appropriately
    char *str ;
    int i ;
    FOREACH_ARRAY_ITEM(array, i, str) {
        // NOTE: a NULL array element is allowed!!!
        save_restore.SaveString(str) ;
    }
}

// static
Array *
VeriTreeNode::RestoreStringArray(SaveRestore &save_restore)
{
    // Restore array size
    unsigned array_size = save_restore.RestoreInteger() ;
    if (!array_size) return 0 ;

    Array *array = new Array(array_size) ;

    // Restore elements (char* in this case)
    char *str ;
    for(unsigned i = 0 ; i < array_size ; i++) {
        // NOTE: a NULL array element is allowed!!!
        str = save_restore.RestoreString() ;
        array->Insert(str) ;
    }
    return array ;
}

/* ----------------------------------------- */

// This function is only for self-referencing STRING_HASHed maps!
// static
void
VeriTreeNode::SaveIdDefMap(SaveRestore &save_restore, const Map *map)
{
    // Dump map size which may even be '0'
    save_restore.SaveInteger(map ? map->Size(): 0) ;

    // Map is assumed to be STRING_HASH or STRING_HASH_CASE_INSENSITIVE
    VeriTreeNode *node ;
    MapIter mi ;
    FOREACH_MAP_ITEM(map, mi, 0, &node) {
        SaveNodePtr(save_restore, node) ;
        // No need to dump the char* key, since its self-referencing
    }
}

// static
Map *
VeriTreeNode::RestoreIdDefMap(SaveRestore &save_restore)
{
    // Restore map size
    unsigned map_size = save_restore.RestoreInteger() ;

    Map *map = new Map(STRING_HASH, map_size) ; // STRING_HASH might cause a problem if STRING_HASH_CASE_INSENSITIVE is expected
    // Restore elements (VeriIdDef* in this case)
    // Map is assumed to be STRING_HASH or STRING_HASH_CASE_INSENSITIVE
    VeriIdDef *id_def ;
    for(unsigned i = 0 ; i < map_size ; i++) {
        id_def = (VeriIdDef*)RestoreNodePtr(save_restore) ;
        if (id_def) (void) map->Insert(id_def->Name(), id_def, 0 /* overwrite */, 1 /* forced-insert */) ;
    }
    return map ;
}

/* ----------------------------------------- */

// static
void
VeriTreeNode::SaveNodePtr(SaveRestore &save_restore, const VeriTreeNode *node)
{
    if (node) {
        // If this instance is not registered, then register it and dump it
        if (!save_restore.IsRegistered(node)) {
            // Register pointer
            save_restore.RegisterPointer(node) ;
            // Dump object
            if (node->IsNameExtendedClass()) {
                save_restore.SaveInteger(ID_VERI_NAME_EXTENDED_EXPRESSION) ;
            } else {
                save_restore.SaveInteger(node->GetClassId()) ;
            }
            node->Save(save_restore) ;
        } else { // if the node is registered then save its reference
            unsigned long id = save_restore.Pointer2Id((const void*)node) ;
            if (id >= (unsigned)ID_VERI_BEGIN_TABLE) {
                VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
                tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

                tmp_print_node.Error("This parse-tree is too large to be saved") ;
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return ;
            }
            save_restore.SaveInteger((unsigned)id) ;
        }
    } else {
        save_restore.SaveInteger(0) ;
    }
}

// static
VeriTreeNode *
VeriTreeNode::RestoreNodePtr(SaveRestore &save_restore)
{
    unsigned val = save_restore.RestoreInteger() ;
    if (!val) return 0 ; // NULL pointer

    // Check if this pointer 'id' is registered (against a pointer) already. Pick up that pointer :
    VeriTreeNode *node = (VeriTreeNode*)save_restore.Id2Pointer(val) ;

    if (!node) {
        // not registered means that there is a new object coming up. Restore that.
        // in this case 'val' will be a ClassId for the type of object that comes up.
        node = VeriTreeNode::CreateObject(save_restore, val) ;
    }
    return node ;
}

/* ----------------------------------------- */

// static
void
VeriTreeNode::SaveScopePtr(SaveRestore &save_restore, const VeriScope *scope)
{
    if (scope) {
        // If this instance is not registered, then register it and dump it
        if (!save_restore.IsRegistered(scope)) {
            save_restore.RegisterPointer(scope) ;
            save_restore.SaveInteger(DATA_TO_FOLLOW) ;
            scope->Save(save_restore) ;
        } else {
            unsigned long id = save_restore.Pointer2Id((const void*)scope) ;
            // If this doesn't hold, our ClassId approach breaks!
            if (id >= (unsigned)ID_VERI_BEGIN_TABLE) {
                VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
                tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

                tmp_print_node.Error("This parse-tree is too large to be saved") ;
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return ;
            }
            save_restore.SaveInteger((unsigned)id) ;
        }
    } else {
        save_restore.SaveInteger(0) ;
    }
}

// static
VeriScope *
VeriTreeNode::RestoreScopePtr(SaveRestore &save_restore)
{
    unsigned val = save_restore.RestoreInteger() ;
    if (!val) return 0 ; // NULL pointer

    VeriScope *scope = 0 ;
    if (val==DATA_TO_FOLLOW) {
        // VeriScope data to follow. Restore the scope :
        scope = new VeriScope(save_restore) ;
        if (!save_restore.IsOK()) {
            delete scope ;
            scope = 0 ;
        }
    } else {
        // 'val' is an id of a previously encountered pointer. Get the registered pointer for that :
        scope = (VeriScope*)save_restore.Id2Pointer(val) ;
        if (!scope) save_restore.FileNotOk() ; // Disable SaveRestore object
    }

    return scope ;
}

/* ----------------------------------------- */

// static
VeriTreeNode *
VeriTreeNode::CreateObject(SaveRestore &save_restore, unsigned class_id)
{
    // You can only create an object from a save_restore that is in restore mode!
    if (save_restore.IsSaving()) {
        VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Error("A Verilog parse-tree node is trying to be created from a SaveRestore object that is in save mode!") ;
        save_restore.FileNotOk() ; // Disable SaveRestore object
        return 0 ;
    }

    VeriTreeNode *node = 0 ;

    switch(class_id)
    {
        // VeriConstraint & VeriValue & VeriBaseValue pertains to elaboration-specific code, so
        // these objects will not be saved/restored.

        // The following two constructors will never get executed
        // =================================================
        case ID_VERIIDDEF :
            node = (new VeriIdDef(save_restore)) ; break ;
        case ID_VERITREENODE :
            node = (new VeriTreeNode(save_restore)) ; break ;
        // =================================================

        case ID_VERICONSTVAL :
            node = (new VeriConstVal(save_restore)) ; break ;
        case ID_VERIINTVAL :
            node = (new VeriIntVal(save_restore)) ; break ;
        case ID_VERIREALVAL :
            node = (new VeriRealVal(save_restore)) ; break ;
        case ID_VERITIMELITERAL :
            node = (new VeriTimeLiteral(save_restore)) ; break ;
        case ID_VERIIDREF :
            node = (new VeriIdRef(save_restore)) ; break ;
        case ID_VERIINDEXEDID :
            node = (new VeriIndexedId(save_restore)) ; break ;
        case ID_VERISELECTEDNAME :
            node = (new VeriSelectedName(save_restore)) ; break ;
        case ID_VERIINDEXEDMEMORYID :
            node = (new VeriIndexedMemoryId(save_restore)) ; break ;
        case ID_VERISCOPENAME :
            node = (new VeriScopeName(save_restore)) ; break ;
        case ID_VERIINDEXEDEXPR :
            node = (new VeriIndexedExpr(save_restore)) ; break ;
        case ID_VERIKEYWORD :
            node = (new VeriKeyword(save_restore)) ; break ;
        case ID_VERICONCAT :
            node = (new VeriConcat(save_restore)) ; break ;
        case ID_VERIMULTICONCAT :
            node = (new VeriMultiConcat(save_restore)) ; break ;
        case ID_VERIFUNCTIONCALL :
            node = (new VeriFunctionCall(save_restore)) ; break ;
        case ID_VERISYSTEMFUNCTIONCALL :
            node = (new VeriSystemFunctionCall(save_restore)) ; break ;
        case ID_VERIMINTYPMAXEXPR :
            node = (new VeriMinTypMaxExpr(save_restore)) ; break ;
        case ID_VERIUNARYOPERATOR :
            node = (new VeriUnaryOperator(save_restore)) ; break ;
        case ID_VERIBINARYOPERATOR :
            node = (new VeriBinaryOperator(save_restore)) ; break ;
        case ID_VERIQUESTIONCOLON :
            node = (new VeriQuestionColon(save_restore)) ; break ;
        case ID_VERIEVENTEXPRESSION :
            node = (new VeriEventExpression(save_restore)) ; break ;
        case ID_VERIPORTCONNECT :
            node = (new VeriPortConnect(save_restore)) ; break ;
        case ID_VERIPORTOPEN :
            node = (new VeriPortOpen(save_restore)) ; break ;
        case ID_VERIANSIPORTDECL :
            node = (new VeriAnsiPortDecl(save_restore)) ; break ;
        case ID_VERITIMINGCHECKEVENT :
            node = (new VeriTimingCheckEvent(save_restore)) ; break ;
        case ID_VERIRANGE :
            node = (new VeriRange(save_restore)) ; break ;
        case ID_VERIDATATYPE :
            node = (new VeriDataType(save_restore)) ; break ;
        case ID_VERINETDATATYPE :
            node = (new VeriNetDataType(save_restore)) ; break ;
        case ID_VERIPATHPULSEVAL :
            node = (new VeriPathPulseVal(save_restore)) ; break ;
        case ID_VERITYPEREF :
            node = (new VeriTypeRef(save_restore)) ; break ;
        case ID_VERISTRUCTUNION :
            node = (new VeriStructUnion(save_restore)) ; break ;
        case ID_VERIENUM :
            node = (new VeriEnum(save_restore)) ; break ;
        case ID_VERIDOTSTAR :
            node = (new VeriDotStar(save_restore)) ; break ;
        case ID_VERIIFOPERATOR :
            node = (new VeriIfOperator(save_restore)) ; break ;
        case ID_VERISEQUENCECONCAT :
            node = (new VeriSequenceConcat(save_restore)) ; break ;
        case ID_VERICLOCKEDSEQUENCE :
            node = (new VeriClockedSequence(save_restore)) ; break ;
        case ID_VERIASSIGNINSEQUENCE :
            node = (new VeriAssignInSequence(save_restore)) ; break ;
        case ID_VERIDISTOPERATOR :
            node = (new VeriDistOperator(save_restore)) ; break ;
        case ID_VERISOLVEBEFORE :
            node = (new VeriSolveBefore(save_restore)) ; break ;
        case ID_VERICAST :
            node = (new VeriCast(save_restore)) ; break ;
        case ID_VERINEW :
            node = (new VeriNew(save_restore)) ; break ;
        case ID_VERICONCATITEM :
            node = (new VeriConcatItem(save_restore)) ; break ;
        case ID_VERINULL :
            node = (new VeriNull(save_restore)) ; break ;
        case ID_VERIDOLLAR :
            node = (new VeriDollar(save_restore)) ; break ;
        case ID_VERIASSIGNMENTPATTERN :
            node = (new VeriAssignmentPattern(save_restore)) ; break ;
        case ID_VERIMULTIASSIGNMENTPATTERN :
            node = (new VeriMultiAssignmentPattern(save_restore)) ; break ;
        case ID_VERIFOREACHOPERATOR :
            node = (new VeriForeachOperator(save_restore)) ; break ;
        case ID_VERISTREAMINGCONCAT :
            node = (new VeriStreamingConcat(save_restore)) ; break ;
        case ID_VERICONDPREDICATE :
            node = (new VeriCondPredicate(save_restore)) ; break ;
        case ID_VERIDOTNAME :
            node = (new VeriDotName(save_restore)) ; break ;
        case ID_VERITAGGEDUNION :
            node = (new VeriTaggedUnion(save_restore)) ; break ;
        case ID_VERIWITHEXPR :
            node = (new VeriWithExpr(save_restore)) ; break ;
        case ID_VERIINLINECONSTRAINT :
            node = (new VeriInlineConstraint(save_restore)) ; break ;
        case ID_VERIOPENRANGEBINVALUE :
            node = (new VeriOpenRangeBinValue(save_restore)) ; break ;
        case ID_VERITRANSBINVALUE :
            node = (new VeriTransBinValue(save_restore)) ; break ;
        case ID_VERIDEFAULTBINVALUE :
            node = (new VeriDefaultBinValue(save_restore)) ; break ;
        case ID_VERISELECTBINVALUE :
            node = (new VeriSelectBinValue(save_restore)) ; break ;
        case ID_VERITRANSSET :
            node = (new VeriTransSet(save_restore)) ; break ;
        case ID_VERITRANSRANGELIST :
            node = (new VeriTransRangeList(save_restore)) ; break ;
        case ID_VERISELECTCONDITION :
            node = (new VeriSelectCondition(save_restore)) ; break ;
        case ID_VERITYPEOPERATOR :
            node = (new VeriTypeOperator(save_restore)) ; break ;
        case ID_VERIPATTERNMATCH :
            node = (new VeriPatternMatch(save_restore)) ; break ;
        case ID_VERICONSTRAINTSET :
            node = (new VeriConstraintSet(save_restore)) ; break ;
        case ID_VERIVARIABLE :
            node = (new VeriVariable(save_restore)) ; break ;
        case ID_VERIINSTID :
            node = (new VeriInstId(save_restore)) ; break ;
        case ID_VERIMODULEID :
            node = (new VeriModuleId(save_restore)) ; break ;
        case ID_VERICONFIGURATIONID:
            node = (new VeriConfigurationId(save_restore)) ; break ;
        case ID_VERIUDPID :
            node = (new VeriUdpId(save_restore)) ; break ;
        case ID_VERITASKID :
            node = (new VeriTaskId(save_restore)) ; break ;
        case ID_VERIFUNCTIONID :
            node = (new VeriFunctionId(save_restore)) ; break ;
        case ID_VERIGENVARID :
            node = (new VeriGenVarId(save_restore)) ; break ;
        case ID_VERIPARAMID :
            node = (new VeriParamId(save_restore)) ; break ;
        case ID_VERIBLOCKID :
            node = (new VeriBlockId(save_restore)) ; break ;
        case ID_VERIINTERFACEID :
            node = (new VeriInterfaceId(save_restore)) ; break ;
        case ID_VERIPROGRAMID :
            node = (new VeriProgramId(save_restore)) ; break ;
        case ID_VERICHECKERID :
            node = (new VeriCheckerId(save_restore)) ; break ;
        case ID_VERITYPEID :
            node = (new VeriTypeId(save_restore)) ; break ;
        case ID_VERIMODPORTID :
            node = (new VeriModportId(save_restore)) ; break ;
        case ID_VERIOPERATORID :
            node = (new VeriOperatorId(save_restore)) ; break ;
        case ID_VERICLOCKINGID :
            node = (new VeriClockingId(save_restore)) ; break ;
        case ID_VERIPRODUCTIONID :
            node = (new VeriProductionId(save_restore)) ; break ;
        case ID_VERINAMEDPORT :
            node = (new VeriNamedPort(save_restore)) ; break ;
        case ID_VERICOVGOPTIONID :
            node = (new VeriCovgOptionId(save_restore)) ; break ;
        // VIPER #3737 : Create object for sequence/property formal class
        case ID_VERISEQPROPERTYFORMAL :
            node = (new VeriSeqPropertyFormal(save_restore)) ; break ;
        case ID_VERIBINSID :
            node = (new VeriBinsId(save_restore)) ; break ;
        case ID_VERILETID :
            node = (new VeriLetId(save_restore)) ; break ;
        case ID_VERIEXTERNFORKJOINTASKID :
            node = (new VeriExternForkjoinTaskId(save_restore)) ; break ;
        case ID_VERILABELID :
            node = (new VeriLabelId(save_restore)) ; break ;
        case ID_VERIPROTOTYPEID :
            node = (new VeriPrototypeId(save_restore)) ; break ;
        case ID_VERISTRENGTH :
            node = (new VeriStrength(save_restore)) ; break ;
        case ID_VERINETREGASSIGN :
            node = (new VeriNetRegAssign(save_restore)) ; break ;
        case ID_VERIDEFPARAMASSIGN :
            node = (new VeriDefParamAssign(save_restore)) ; break ;
        case ID_VERICASEITEM :
            node = (new VeriCaseItem(save_restore)) ; break ;
        case ID_VERIGENERATECASEITEM :
            node = (new VeriGenerateCaseItem(save_restore)) ; break ;
        case ID_VERIPATH :
            node = (new VeriPath(save_restore)) ; break ;
        case ID_VERIDELAYOREVENTCONTROL :
            node = (new VeriDelayOrEventControl(save_restore)) ; break ;
        case ID_VERIINSTANCECONFIG :
            node = (new VeriInstanceConfig(save_restore)) ; break ;
        case ID_VERICELLCONFIG :
            node = (new VeriCellConfig(save_restore)) ; break ;
        case ID_VERIDEFAULTCONFIG:
            node = (new VeriDefaultConfig(save_restore)) ; break ;
        case ID_VERIUSECLAUSE:
            node = (new VeriUseClause(save_restore)) ; break ;
        case ID_VERICLOCKINGDIRECTION :
            node = (new VeriClockingDirection(save_restore)) ; break ;
        case ID_VERIPRODUCTION :
            node = (new VeriProduction(save_restore)) ; break ;
        case ID_VERIPRODUCTIONITEM :
            node = (new VeriProductionItem(save_restore)) ; break ;
        case ID_VERIPATHPULSEVALPORTS :
#ifdef VERILOG_PATHPULSE_PORTS
            node = (new VeriPathPulseValPorts(save_restore)) ;
#else
            save_restore.FileNotOk() ; // Disable SaveRestore object
#endif // VERILOG_PATHPULSE_PORTS
            break ;
        case ID_VERIMODULE :
            node = (new VeriModule(save_restore)) ; break ;
        case ID_VERICONFIGURATION :
            node = (new VeriConfiguration(save_restore)) ; break ;
        case ID_VERIPRIMITIVE :
            node = (new VeriPrimitive(save_restore)) ; break ;
        case ID_VERIINTERFACE :
            node = (new VeriInterface(save_restore)) ; break ;
        case ID_VERIPROGRAM :
            node = (new VeriProgram(save_restore)) ; break ;
        case ID_VERICHECKER :
            node = (new VeriChecker(save_restore)) ; break ;
        case ID_VERIPACKAGE :
            node = (new VeriPackage(save_restore)) ; break ;
        case ID_VERIDATADECL :
            node = (new VeriDataDecl(save_restore)) ; break ;
        case ID_VERINETDECL :
            node = (new VeriNetDecl(save_restore)) ; break ;
        case ID_VERIFUNCTIONDECL :
            node = (new VeriFunctionDecl(save_restore)) ; break ;
        case ID_VERITASKDECL :
            node = (new VeriTaskDecl(save_restore)) ; break ;
        case ID_VERIDEFPARAM :
            node = (new VeriDefParam(save_restore)) ; break ;
        case ID_VERICONTINUOUSASSIGN :
            node = (new VeriContinuousAssign(save_restore)) ; break ;
        case ID_VERIGATEINSTANTIATION :
            node = (new VeriGateInstantiation(save_restore)) ; break ;
        case ID_VERIMODULEINSTANTIATION :
            node = (new VeriModuleInstantiation(save_restore)) ; break ;
        case ID_VERISEQUENTIALINSTANTIATION :
            node = (new VeriSequentialInstantiation(save_restore)) ; break ;
        case ID_VERISPECIFYBLOCK :
            node = (new VeriSpecifyBlock(save_restore)) ; break ;
        case ID_VERIPATHDECL :
            node = (new VeriPathDecl(save_restore)) ; break ;
        case ID_VERISYSTEMTIMINGCHECK :
            node = (new VeriSystemTimingCheck(save_restore)) ; break ;
        case ID_VERIINITIALCONSTRUCT :
            node = (new VeriInitialConstruct(save_restore)) ; break ;
        case ID_VERIALWAYSCONSTRUCT :
            node = (new VeriAlwaysConstruct(save_restore)) ; break ;
        case ID_VERIGENERATECONSTRUCT :
            node = (new VeriGenerateConstruct(save_restore)) ; break ;
        case ID_VERIGENERATECONDITIONAL :
            node = (new VeriGenerateConditional(save_restore)) ; break ;
        case ID_VERIGENERATECASE :
            node = (new VeriGenerateCase(save_restore)) ; break ;
        case ID_VERIGENERATEFOR :
            node = (new VeriGenerateFor(save_restore)) ; break ;
        case ID_VERIGENERATEBLOCK :
            node = (new VeriGenerateBlock(save_restore)) ; break ;
        case ID_VERITABLE :
            node = (new VeriTable(save_restore)) ; break ;
        case ID_VERIPULSECONTROL :
            node = (new VeriPulseControl(save_restore)) ; break ;
        case ID_VERICLASS :
            node = (new VeriClass(save_restore)) ; break ;
        case ID_VERIPROPERTYDECL :
            node = (new VeriPropertyDecl(save_restore)) ; break ;
        case ID_VERISEQUENCEDECL :
            node = (new VeriSequenceDecl(save_restore)) ; break ;
        case ID_VERIMODPORT :
            node = (new VeriModport(save_restore)) ; break ;
        case ID_VERIMODPORTDECL :
            node = (new VeriModportDecl(save_restore)) ; break ;
        case ID_VERICLOCKINGDECL :
            node = (new VeriClockingDecl(save_restore)) ; break ;
        case ID_VERICONSTRAINTDECL :
            node = (new VeriConstraintDecl(save_restore)) ; break ;
        case ID_VERIBINDDIRECTIVE :
            node = (new VeriBindDirective(save_restore)) ; break ;
        case ID_VERIOPERATORBINDING :
            node = (new VeriOperatorBinding(save_restore)) ; break ;
        case ID_VERINETALIAS :
            node = (new VeriNetAlias(save_restore)) ; break ;
        case ID_VERITIMEUNIT :
            node = (new VeriTimeUnit(save_restore)) ; break ;
        case ID_VERICLOCKINGSIGDECL :
            node = (new VeriClockingSigDecl(save_restore)) ; break ;
        case ID_VERICOVERGROUP :
            node = (new VeriCovergroup(save_restore)) ; break ;
        case ID_VERICOVERAGEOPTION :
            node = (new VeriCoverageOption(save_restore)) ; break ;
        case ID_VERICOVERAGESPEC :
            node = (new VeriCoverageSpec(save_restore)) ; break ;
        case ID_VERIBINDECL :
            node = (new VeriBinDecl(save_restore)) ; break ;
        case ID_VERIIMPORTDECL :
            node = (new VeriImportDecl(save_restore)) ; break ;
        case ID_VERILETDECL :
            node = (new VeriLetDecl(save_restore)) ; break ;
        case ID_VERIDEFAULTDISABLEIFF :
            node = (new VeriDefaultDisableIff(save_restore)) ; break ;
        case ID_VERIEXPORTDECL :
            node = (new VeriExportDecl(save_restore)) ; break ;
        case ID_VERIDPIFUNCTIONDECL :
            node = (new VeriDPIFunctionDecl(save_restore)) ; break ;
        case ID_VERIDPITASKDECL :
            node = (new VeriDPITaskDecl(save_restore)) ; break ;
        case ID_VERINULLSTATEMENT :
            node = (new VeriNullStatement(save_restore)) ; break ;
        case ID_VERIBLOCKINGASSIGN :
            node = (new VeriBlockingAssign(save_restore)) ; break ;
        case ID_VERINONBLOCKINGASSIGN :
            node = (new VeriNonBlockingAssign(save_restore)) ; break ;
        case ID_VERIGENVARASSIGN :
            node = (new VeriGenVarAssign(save_restore)) ; break ;
        case ID_VERIASSIGN :
            node = (new VeriAssign(save_restore)) ; break ;
        case ID_VERIDEASSIGN :
            node = (new VeriDeAssign(save_restore)) ; break ;
        case ID_VERIFORCE :
            node = (new VeriForce(save_restore)) ; break ;
        case ID_VERIRELEASE :
            node = (new VeriRelease(save_restore)) ; break ;
        case ID_VERITASKENABLE :
            node = (new VeriTaskEnable(save_restore)) ; break ;
        case ID_VERISYSTEMTASKENABLE :
            node = (new VeriSystemTaskEnable(save_restore)) ; break ;
        case ID_VERIDELAYCONTROLSTATEMENT :
            node = (new VeriDelayControlStatement(save_restore)) ; break ;
        case ID_VERIEVENTCONTROLSTATEMENT :
            node = (new VeriEventControlStatement(save_restore)) ; break ;
        case ID_VERICONDITIONALSTATEMENT :
            node = (new VeriConditionalStatement(save_restore)) ; break ;
        case ID_VERICASESTATEMENT :
            node = (new VeriCaseStatement(save_restore)) ; break ;
        case ID_VERIFOREVER :
            node = (new VeriForever(save_restore)) ; break ;
        case ID_VERIREPEAT :
            node = (new VeriRepeat(save_restore)) ; break ;
        case ID_VERIWHILE :
            node = (new VeriWhile(save_restore)) ; break ;
        case ID_VERIFOR :
            node = (new VeriFor(save_restore)) ; break ;
        case ID_VERIWAIT :
            node = (new VeriWait(save_restore)) ; break ;
        case ID_VERIDISABLE :
            node = (new VeriDisable(save_restore)) ; break ;
        case ID_VERIEVENTTRIGGER :
            node = (new VeriEventTrigger(save_restore)) ; break ;
        case ID_VERISEQBLOCK :
            node = (new VeriSeqBlock(save_restore)) ; break ;
        case ID_VERIPARBLOCK :
            node = (new VeriParBlock(save_restore)) ; break ;
        case ID_VERIASSERTION :
            node = (new VeriAssertion(save_restore)) ; break ;
        case ID_VERIJUMPSTATEMENT :
            node = (new VeriJumpStatement(save_restore)) ; break ;
        case ID_VERIDOWHILE :
            node = (new VeriDoWhile(save_restore)) ; break ;
        case ID_VERIFOREACH :
            node = (new VeriForeach(save_restore)) ; break ;
        case ID_VERIWAITORDER :
            node = (new VeriWaitOrder(save_restore)) ; break ;
        case ID_VERIARRAYMETHODCALL :
            node = (new VeriArrayMethodCall(save_restore)) ; break ;
        case ID_VERIINLINECONSTRAINTSTMT :
            node = (new VeriInlineConstraintStmt(save_restore)) ; break ;
        case ID_VERIRANDSEQUENCE :
            node = (new VeriRandsequence(save_restore)) ; break ;
        case ID_VERICODEBLOCK :
            node = (new VeriCodeBlock(save_restore)) ; break ;
        case ID_VERICASEOPERATOR :
            node = (new VeriCaseOperator(save_restore)) ; break ;
        case ID_VERICASEOPERATORITEM :
            node = (new VeriCaseOperatorItem(save_restore)) ; break ;
        case ID_VERICOMMENTNODE :
            node = (new VeriCommentNode(save_restore)) ; break ;
        case ID_VERI_NAME_EXTENDED_EXPRESSION :
        {
            VeriExpression *expr = (VeriExpression *)(VeriTreeNode::RestoreNodePtr(save_restore)) ;
            char *name = save_restore.RestoreString() ;
            save_restore.UnRegister(expr) ;
            if (expr) node = expr->GetNameExtendedExpression(name) ;
            else {
                node = 0 ;
                Strings::free(name) ;
            }
            break ;
        }
        default:
            {
            //char szBuf[12] ;
            //sprintf(szBuf, "0x%x", class_id) ;
            save_restore.FileNotOk() ; // Disable SaveRestore object
            return 0 ;
            }
    }

    // If an error occurred (i.e. strea is not ok), delete appropriately
    if (!save_restore.IsOK()) {
        delete node ;
        node = 0 ;
    }

    return node ;
}

//--------------------------------------------------------------
// #included from VeriConstVal.h
//--------------------------------------------------------------

void
VeriConst::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
}

VeriConst::VeriConst(SaveRestore &save_restore)
  : VeriExpression(save_restore)
{
}

/* ---------------------------- */

void
VeriConstVal::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VeriConst::Save(save_restore) ;

    // Save the size
    save_restore.SaveInteger(_size) ;

    // Collect the bit flags in bit_flags and save
    unsigned bit_flags = 0 ;
    bit_flags = _is_signed ;
    SAVE_BIT_FLAG(bit_flags, _is_string, 1) ; // Flag constant as a 'string' type constant
    SAVE_BIT_FLAG(bit_flags, _is_unsized_bit, 1) ; // Flag constant as unsized bit
    SAVE_BIT_FLAG(bit_flags, _is_unsized_number, 1) ; // Flag constant as unsized number
    SAVE_BIT_FLAG(bit_flags, _value ? 1 : 0, 1) ; // Store 1 if _value is present else save 0
    SAVE_BIT_FLAG(bit_flags, _zvalue ? 1 : 0, 1) ;
    SAVE_BIT_FLAG(bit_flags, _xvalue ? 1 : 0, 1) ;
    save_restore.SaveInteger(bit_flags) ;

    // Save _value as a collection of characters instead of saving as a string.
    unsigned i=0 ;
    unsigned size = GetMemSize() ;
    for (i=0; i<size; i++) {
        // Values are saved as array of chars. So save individual characters.
        if (_value) save_restore.SaveChar(_value[i]) ;
        if (_zvalue) save_restore.SaveChar(_zvalue[i]) ;
        if (_xvalue) save_restore.SaveChar(_xvalue[i]) ;
    }

#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & #2770: Preserve the original value string:
    save_restore.SaveString(_orig_value) ;
#else
    // Save a null string:
    save_restore.SaveString(0) ;
#endif
}

VeriConstVal::VeriConstVal(SaveRestore &save_restore)
  : VeriConst(save_restore),
    _size(0),
    _is_signed(0),
    _is_string(0),
    _is_unsized_bit(0),
    _is_unsized_number(0),
    _value(0),
    _zvalue(0),
    _xvalue(0)
#ifdef VERILOG_PRESERVE_LITERALS
    , _orig_value(0) // VIPER #2587 & #2770: Preserve the original value string
#endif
{
    char v=0, v_z=0, v_x=0 ;
    unsigned bit_flags ;
    // Restore the size
    _size = save_restore.RestoreInteger() ;

    bit_flags = save_restore.RestoreInteger() ;
    // Restore the bit flags in reverse order of saving

    RESTORE_BIT_FLAG(bit_flags, v_x, 1) ;
    RESTORE_BIT_FLAG(bit_flags, v_z, 1) ;
    RESTORE_BIT_FLAG(bit_flags, v, 1) ;

    RESTORE_BIT_FLAG(bit_flags, _is_unsized_number,1) ; // Flag the constant as unsized number
    RESTORE_BIT_FLAG(bit_flags, _is_unsized_bit,1) ; // Flag the constant as unsized bit
    RESTORE_BIT_FLAG(bit_flags, _is_string,1) ; // Flag the constant as a 'string' type constant
    RESTORE_BIT_FLAG(bit_flags, _is_signed,1) ; // Flag the constant as 'signed'

    // Restore _value as a collection of characters instead of saving as a string.
    unsigned i=0 ;
    unsigned mem_size = GetMemSize() ;
    if (v)   _value =  VFC_ARRAY_NEW(unsigned char, mem_size) ;
    if (v_z) _zvalue = VFC_ARRAY_NEW(unsigned char, mem_size) ;
    if (v_x) _xvalue = VFC_ARRAY_NEW(unsigned char, mem_size) ;

    for (i=0; i<mem_size; i++) {
        // Restore the values as character arrays
        if (_value)   _value[i] = save_restore.RestoreChar() ;
        if (_zvalue) _zvalue[i] = save_restore.RestoreChar() ;
        if (_xvalue) _xvalue[i] = save_restore.RestoreChar() ;
    }

#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & #2770: Preserve the original value string:
    _orig_value = save_restore.RestoreString() ;
#else
    // Restore the string and ignore:
    Strings::free(save_restore.RestoreString()) ; // JJ 130322 Free ignored string
#endif
}

/* ---------------------------- */

void
VeriIntVal::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VeriConst::Save(save_restore) ;

    // Save integer
    save_restore.SaveInteger(_num) ;
}

VeriIntVal::VeriIntVal(SaveRestore &save_restore)
  : VeriConst(save_restore),
    _num(0)
{
    // Restore integer
    _num = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriRealVal::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VeriConst::Save(save_restore) ;

    // Save integer
    save_restore.SaveDouble(_num) ;
}

VeriRealVal::VeriRealVal(SaveRestore &save_restore)
  : VeriConst(save_restore),
    _num(0)
{
    // Restore double
    _num = save_restore.RestoreDouble() ;
}

/* ---------------------------- */

void
VeriTimeLiteral::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VeriConst::Save(save_restore) ;

    // Save time literal like 1.0ps in string format
    save_restore.SaveString(_literal) ;
}

VeriTimeLiteral::VeriTimeLiteral(SaveRestore &save_restore)
  : VeriConst(save_restore),
    _literal(0)
{
    // Restore string
    _literal = save_restore.RestoreString() ;
}

//--------------------------------------------------------------
// #included from VeriExpression.h
//--------------------------------------------------------------

void
VeriExpression::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;

    // Save attributes map. char* -> VeriExpression* mapping
    // Save map size
    Map *attributes = GetAttributes() ;
    save_restore.SaveInteger(attributes ? attributes->Size() : 0) ;

    char *str ;
    VeriExpression *expr ;
    MapIter mi ;
    FOREACH_MAP_ITEM(attributes, mi, &str, &expr) {
        save_restore.SaveString(str) ;
        VeriTreeNode::SaveNodePtr(save_restore, expr) ; // 'expr' may be NULL.
    }

    // Save comments array
    if (RuntimeFlags::GetVar("veri_preserve_comments")) {
        SaveArray(save_restore, GetComments()) ;
    }
}

VeriExpression::VeriExpression(SaveRestore &save_restore)
  : VeriTreeNode(save_restore)
{
    // Restore char* -> VeriExpression* map
    unsigned size = save_restore.RestoreInteger() ;
    if (size) {
        Map *attributes = new Map(STRING_HASH, size) ;
        unsigned i ;
        for(i=0; i<size; i++) {
            char *str = save_restore.RestoreString() ;
            VeriExpression *expr = (VeriExpression *)(VeriTreeNode::RestoreNodePtr(save_restore)) ;
            (void)attributes->Insert(str, expr) ; // 'expr' may be NULL.
        }
        SetAttributes(attributes) ;
    }
    // Restore parse-tree comments if stream contains them.
    // Determine if the stream was saved with PRESERVE_COMMENTS enabled.  Bit #3 of user
    // flags contains this information.
    if (save_restore.GetUserFlags() & PRESERVE_COMMENTS_ENABLED) {
        if (RuntimeFlags::GetVar("veri_preserve_comments")) {
            // Restore comment array
            Array *comment_arr = RestoreArray(save_restore) ;
            if (comment_arr) AddComments(comment_arr, 0) ;
        } else {
            // If current setting of preserve_comments is not enabled, deallocate what was restored.
            VeriCommentNode *node ;
            unsigned i ;
            Array *array = RestoreArray(save_restore) ;
            FOREACH_ARRAY_ITEM(array, i, node) {
                // We need to unregister pointer before deleting
                save_restore.UnRegister(node) ;
                delete node ;
            }
            delete array ;
        }
    }
}

/* ---------------------------- */

void
VeriName::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
}

VeriName::VeriName(SaveRestore &save_restore)
  : VeriExpression(save_restore)
{
}

/* ---------------------------- */

void
VeriIdRef::Save(SaveRestore &save_restore) const
{
    VeriName::Save(save_restore) ;

    // Save the unresolved prefix name
    save_restore.SaveString(_name) ;
    // Save the resolved identifier definition
    SaveNodePtr(save_restore, _id) ;
}

VeriIdRef::VeriIdRef(SaveRestore &save_restore)
  : VeriName(save_restore),
    _name(0),
    _id(0)
{
    // Restore the prefix name
    char *name = save_restore.RestoreString() ;
#ifdef VERILOG_SHARE_STRINGS
    _name = Strings::CreateConstantString(name) ; // used shared string
    Strings::free(name) ;
#else
    _name = name ;
#endif

    // Restore the id
    _id = (VeriIdDef*)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriIndexedId::Save(SaveRestore &save_restore) const
{
    VeriName::Save(save_restore) ;

    // Save the prefix name
    SaveNodePtr(save_restore, _prefix) ;
    // Save the index expression (VeriRange for part-select)
    SaveNodePtr(save_restore, _idx) ;
    // Save the resolved prefix identifier
    SaveNodePtr(save_restore, _id) ;
}

VeriIndexedId::VeriIndexedId(SaveRestore &save_restore)
  : VeriName(save_restore),
    _prefix(0),
    _idx(0),
    _id(0)
{
    // Restore the prefix name
    _prefix = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore the index expression
    _idx = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the resolved prefix identifier
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriSelectedName::Save(SaveRestore &save_restore) const
{
    VeriName::Save(save_restore) ;

    // Save the prefix name
    SaveNodePtr(save_restore, _prefix) ;
    // Save the suffix name string
    save_restore.SaveString(_suffix) ;
    // Save the prefix_id
    SaveNodePtr(save_restore, _prefix_id) ;
    // Save the suffix id
    SaveNodePtr(save_restore, _suffix_id) ;
    // Save the bit flags
    unsigned bit_flags = _function_type ;
    SAVE_BIT_FLAG(bit_flags, _suffix_is_token, 1) ;
    save_restore.SaveInteger(bit_flags) ;
}

VeriSelectedName::VeriSelectedName(SaveRestore &save_restore)
  : VeriName(save_restore),
    _prefix(0),
    _suffix(0),
    _prefix_id(0),
    _suffix_id(0),
    _function_type(0),
    _suffix_is_token(0),
    _cross_lang_name(0) // VIPER #5624 : Set in static elaboration, no need to save/restore
{
    // Restore the prefix name
    _prefix = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore the suffix name
    char *name = save_restore.RestoreString() ;
#ifdef VERILOG_SHARE_STRINGS
    _suffix = Strings::CreateConstantString(name) ; // used shared string
    Strings::free(name) ;
#else
    _suffix = name ;
#endif

    // Restore the prefix_id
    _prefix_id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore the suffix id
    _suffix_id = (VeriIdDef *)RestoreNodePtr(save_restore) ;

    // Find the token for the suffix (in case this is a 'method' on the prefix).
    // Methods are only allowed in System Verilog mode :
    //if (IsSystemVeri()) {
    //    _function_type = GetFuncToken(_suffix) ;
    //    // Note: This can return a non-method token (such as a system call name).
    //    // If so, discard this as a valid method in 'Resolve'.
    //}
    // Restore the bit flags:
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _suffix_is_token, 1) ;
    if (IsSystemVeri()) _function_type = bit_flags ; // Direct assign for last member of bit_flags
}

/* ---------------------------- */

void
VeriIndexedMemoryId::Save(SaveRestore &save_restore) const
{
    VeriName::Save(save_restore) ;

    // Save prefix
    SaveNodePtr(save_restore, _prefix) ;
    // Save indexes
    SaveArray(save_restore, _indexes) ;
    // Save id
    SaveNodePtr(save_restore, _id) ;
}

VeriIndexedMemoryId::VeriIndexedMemoryId(SaveRestore &save_restore)
  : VeriName(save_restore),
    _prefix(0),
    _indexes(0),
    _id(0)
{
    // Restore prefix
    _prefix = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore indexes
    _indexes = RestoreArray(save_restore) ;
    // Restore id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriIndexedExpr::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;

    // Save the prefix expression
    SaveNodePtr(save_restore, _prefix) ;
    // Save the index expression (VeriRange for part-select)
    SaveNodePtr(save_restore, _idx) ;
}

VeriIndexedExpr::VeriIndexedExpr(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _prefix(0),
    _idx(0)
{
    // Restore the prefix expression
    _prefix = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the index expression
    _idx = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

void
VeriScopeName::Save(SaveRestore &save_restore) const
{
    VeriSelectedName::Save(save_restore) ;

    // Save the parameter value assignment list
    SaveArray(save_restore, _param_values) ;

    // Save modport name
    SaveNodePtr(save_restore, _modport_name) ;
}

VeriScopeName::VeriScopeName(SaveRestore &save_restore)
  : VeriSelectedName(save_restore),
    _param_values(0),
    _modport_name(0)
{
    // Restore the parameter value assignment list
    _param_values = RestoreArray(save_restore) ;
    // Restore modport name
    _modport_name = (VeriName *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriKeyword::Save(SaveRestore &save_restore) const
{
    VeriIdRef::Save(save_restore) ;

    // Save unsigned integer token
    save_restore.SaveInteger(_token) ;
}

VeriKeyword::VeriKeyword(SaveRestore &save_restore)
  : VeriIdRef(save_restore),
    _token(0)
{
    // Restore token
    _token = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriConcat::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;

    // Save array of expressions
    SaveArray(save_restore, _exprs) ;
}

VeriConcat::VeriConcat(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _exprs(0)
{
    // Restore array of expressions
    _exprs = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriMultiConcat::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;

    // Save repeat expression
    SaveNodePtr(save_restore, _repeat) ;
    // Save array of concat expressions
    SaveArray(save_restore, _exprs) ;
}

VeriMultiConcat::VeriMultiConcat(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _repeat(0),
    _exprs(0)
{
    // Restore repeat expression
    _repeat = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore expression array
    _exprs = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriFunctionCall::Save(SaveRestore &save_restore) const
{
    VeriName::Save(save_restore) ;
    // Save function name
    SaveNodePtr(save_restore, _func_name) ;
    // Save argument array
    SaveArray(save_restore, _args) ;
}

VeriFunctionCall::VeriFunctionCall(SaveRestore &save_restore)
  : VeriName(save_restore),
    _func_name(0),
    _args(0),
    _function_type(0)
{
    // Restore function name
    _func_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore argument array
    _args = RestoreArray(save_restore) ;
    // Find the token for the suffix (in case this is a 'method' on the prefix).
    // Methods are only allowed in System Verilog mode :
    if (IsSystemVeri() && _func_name) {
        _function_type = _func_name->GetFunctionType() ;
        //if (!_function_type) _function_type = GetFuncToken(_func_name->GetName()) ;
        // Note: This can return a non-method token (such as a system call name).
        // If so, discard this as a valid method in 'Resolve'.
    }
}

/* ---------------------------- */

void
VeriSystemFunctionCall::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save system function name
    save_restore.SaveString(_name) ;
    // Save argument array
    SaveArray(save_restore, _args) ;
    // Save clock expression
    SaveNodePtr(save_restore, _sva_clock_expression) ;
    // Save unsigned integer function type which is a VERI token of the function
    // of the system call and is  0 if not known.
    save_restore.SaveInteger(_function_type) ;
}

VeriSystemFunctionCall::VeriSystemFunctionCall(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _name(0),
    _args(0),
    _sva_clock_expression(0),
    _function_type(0)
{
    // Restore function name
    _name = save_restore.RestoreString() ;
    // Restore argument array
    _args = RestoreArray(save_restore) ;
    // Restore clock expression
    _sva_clock_expression = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore function type
    _function_type = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriMinTypMaxExpr::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save min expression
    SaveNodePtr(save_restore, _min_expr) ;
    // Save typ expression
    SaveNodePtr(save_restore, _typ_expr) ;
    // Save max expression
    SaveNodePtr(save_restore, _max_expr) ;
}

VeriMinTypMaxExpr::VeriMinTypMaxExpr(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _min_expr(0),
    _typ_expr(0),
    _max_expr(0)
{
    // Restore min expression
    _min_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore typ expression
    _typ_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore max expression
    _max_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriUnaryOperator::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;

    // Save the oper type and bit flags
    unsigned bit_flags = 0 ; // Initialize it to '0'
    SAVE_BIT_FLAG(bit_flags, _parenthesized, 1) ;
    SAVE_BIT_FLAG(bit_flags, _oper_type, 12) ;
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags

    // Save operand expression
    SaveNodePtr(save_restore, _arg) ;
}

VeriUnaryOperator::VeriUnaryOperator(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _oper_type(0),
    _parenthesized(0),
    _arg(0)
{
    // Restore the unsigned oper type integer and bit flags
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _oper_type, 12) ;
    RESTORE_BIT_FLAG(bit_flags, _parenthesized, 1) ;

    // Restore the operand expression
    _arg = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriBinaryOperator::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;

    // save oper_type and bit flags
    unsigned bit_flags = 0 ; // Initialize it to '0'
    SAVE_BIT_FLAG(bit_flags, _parenthesized, 1) ;
    SAVE_BIT_FLAG(bit_flags, _oper_type, 12) ;
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags

    // Save left expression
    SaveNodePtr(save_restore, _left) ;
    // Save right expression
    SaveNodePtr(save_restore, _right) ;
}

VeriBinaryOperator::VeriBinaryOperator(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _oper_type(0),
    _parenthesized(0),
    _left(0),
    _right(0)
{
    // Restore the unsigned oper type integer and bit flags
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _oper_type, 12) ;
    RESTORE_BIT_FLAG(bit_flags, _parenthesized, 1) ;

    // Restore left expression
    _left = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore right expression
    _right = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriQuestionColon::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save if conditional expression
    SaveNodePtr(save_restore, _if_expr) ;
    // Save then expression
    SaveNodePtr(save_restore, _then_expr) ;
    // Save else expression
    SaveNodePtr(save_restore, _else_expr) ;
}

VeriQuestionColon::VeriQuestionColon(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _if_expr(0),
    _then_expr(0),
    _else_expr(0)
{
    // Restore if expression
    _if_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore then expression
    _then_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore else expression
    _else_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriEventExpression::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save unsigned integer edge token
    save_restore.SaveInteger(_edge) ;
    // Save edge expression
    SaveNodePtr(save_restore, _expr) ;
    // Save iff conditional  expression
    SaveNodePtr(save_restore, _iff_condition) ;
    // VIPER #5958: Save/restore _event_with_at bit directly for now:
    save_restore.SaveInteger(_event_with_at) ;
}

VeriEventExpression::VeriEventExpression(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _edge(0),
    _expr(0),
    _iff_condition(0),
    _event_with_at(0)
{
    // Restore the unsigned integer edge
    _edge = save_restore.RestoreInteger() ;
    // Restore edge expression
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore iff conditional expression
    _iff_condition = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // VIPER #5958: Save/restore _event_with_at bit directly for now:
    _event_with_at = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriPortConnect::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save port ref name string
    save_restore.SaveString(_port_ref_name) ;
    // Save connection expression
    SaveNodePtr(save_restore, _connection) ;
}

VeriPortConnect::VeriPortConnect(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _port_ref_name(0),
    _connection(0)
{
    // Restore port ref name string
    char *name = save_restore.RestoreString() ;
#ifdef VERILOG_SHARE_STRINGS
    _port_ref_name = Strings::CreateConstantString(name) ; // used shared string
    Strings::free(name) ;
#else
    _port_ref_name = name ;
#endif

    // Restore connection expression
    _connection = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriPortOpen::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
}

VeriPortOpen::VeriPortOpen(SaveRestore &save_restore)
  : VeriExpression(save_restore)
{
}

/* ---------------------------- */

void
VeriAnsiPortDecl::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the port direction (unsigned integer)
    //save_restore.SaveInteger(_dir) ;
    unsigned bit_flags = _dir ;
    // Save _data_type expression
    SaveNodePtr(save_restore, _data_type) ;
    // Save ids
    SaveArray(save_restore, _ids) ;
    // Save the unsigned integer qualifier for tokens VERI_VAR and VERI_VIRTUAL
    save_restore.SaveInteger(_qualifier) ;
    SAVE_BIT_FLAG(bit_flags, _is_local, 1) ;
    save_restore.SaveInteger(bit_flags) ;
}

VeriAnsiPortDecl::VeriAnsiPortDecl(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _dir(0),
   _is_local(0),
    _data_type(0),
    _ids(0)
   ,_qualifier(0)
{
    // Restore the unsigned port direction integer
    //_dir = save_restore.RestoreInteger() ;
    // Restore _data_type expression
    _data_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
    // Restore ids
    _ids = RestoreArray(save_restore) ;
    // Set the back pointer '_data_type' to each of the individual ids of _ids.
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (id && _data_type) id->SetDataTypeForSave(_data_type) ;
    }
    // Restore the unsigned integer qualifier for tokens VERI_VAR and VERI_VIRTUAL
    _qualifier = save_restore.RestoreInteger() ;
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_local, 1) ;
    _dir = bit_flags ;
}

/* ---------------------------- */

void
VeriTimingCheckEvent::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the unsigned edge
    save_restore.SaveInteger(_edge) ;
    // Save edge descriptor string
    save_restore.SaveString(_edge_desc_str) ;
    // Save terminal descriptor expression
    SaveNodePtr(save_restore, _terminal_desc) ;
    // Save timing check condition expression
    SaveNodePtr(save_restore, _condition) ;
}

VeriTimingCheckEvent::VeriTimingCheckEvent(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _edge(0),
    _edge_desc_str(0),
    _terminal_desc(0),
    _condition(0)
{
    // Restore the unsigned edge integer
    _edge = save_restore.RestoreInteger() ;
    // Restore edge description string
    _edge_desc_str = save_restore.RestoreString() ;
    // Restore terminal descriptor expression
    _terminal_desc = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore timing check condition expression
    _condition = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriRange::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save left expression
    SaveNodePtr(save_restore, _left) ;
    // Save right expression
    SaveNodePtr(save_restore, _right) ;
    // Save the part select token, unsigned integer
    save_restore.SaveInteger(_part_select_token) ;
    // Save next expression
    SaveNodePtr(save_restore, _next) ;
}

VeriRange::VeriRange(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _left(0),
    _right(0),
    _part_select_token(0),
    _next(0)
{
    // Restore left expression
    _left = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore right expression
    _right = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the part select token, unsigned integer.
    // 0 for plain range [left:right]. For Verilog 2000 : VERI_PARTSELECT_UP / DOWN.
    // More choices for SV
    _part_select_token = save_restore.RestoreInteger() ;
    // Restore next expression
    _next = (VeriRange *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDataType::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the unsigned VERI token (VERI_REG, VERI_INTEGER, etc)
    save_restore.SaveInteger(_type) ;
    // Save the unsigned VERI token for signing  (VERI_SIGNED, VERI_UNSIGNED)
    save_restore.SaveInteger(_signing) ;
    save_restore.SaveInteger(_is_virtual_interface) ;
    // Save linked list of range expression to indicat packed dimensions
    SaveNodePtr(save_restore, _dimensions) ;
}

VeriDataType::VeriDataType(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _type(0),
    _signing(0),
    _is_virtual_interface(0),
    _is_processing(0), // Save-restore is not required
    _dimensions(0)
{
    // Restore the unsigned  VERI token (VERI_REG, VERI_INTEGER, etc)
    _type = save_restore.RestoreInteger() ;
    // Restore the unsigned VERI token  (VERI_SIGNED, VERI_UNSIGNED)
    _signing = save_restore.RestoreInteger() ;
    _is_virtual_interface = save_restore.RestoreInteger() ;
    // Restore linked list of range expression to indicate packed dimensions
    _dimensions = (VeriRange *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriNetDataType::Save(SaveRestore &save_restore) const
{
    VeriDataType::Save(save_restore) ;
    // Save data type
    SaveNodePtr(save_restore, _data_type) ;
}
VeriNetDataType::VeriNetDataType(SaveRestore &save_restore)
    : VeriDataType(save_restore),
     _data_type(0)
{
    // Restore data type
    _data_type = (VeriDataType*)RestoreNodePtr(save_restore) ;
}

// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
void
VeriPathPulseVal::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the reject and error limits
    SaveNodePtr(save_restore, _reject_limit) ;
    SaveNodePtr(save_restore, _error_limit) ;
}

VeriPathPulseVal::VeriPathPulseVal(SaveRestore &save_restore)
  : VeriExpression(save_restore),
   _reject_limit(0),
   _error_limit(0)
{
    // Restore the reject and error limits
    _reject_limit = (VeriExpression *)RestoreNodePtr(save_restore) ;
    _error_limit = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

/* ---------------------------- */

void
VeriTypeRef::Save(SaveRestore &save_restore) const
{
    VeriDataType::Save(save_restore) ;
    // Save type name
    SaveNodePtr(save_restore, _type_name) ;
    // Save virtual token
    save_restore.SaveInteger(_virtual_token) ;
    // Save resolved id
    SaveNodePtr(save_restore, _id) ;
}

VeriTypeRef::VeriTypeRef(SaveRestore &save_restore)
  : VeriDataType(save_restore),
    _type_name(0),
    _virtual_token(0),
    _id(0)
{
    // Restore type name
    _type_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore virtual token
    _virtual_token = save_restore.RestoreInteger() ;
    // Restore resolved id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriStructUnion::Save(SaveRestore &save_restore) const
{
    VeriDataType::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save member declarations
    SaveArray(save_restore, _decls) ;
    // Save member 'VeriIdDef's
    SaveArray(save_restore, _ids) ;

    char bit_flags = 0 ;
    // Collect the bit flags in bit_flags and save
    bit_flags = _is_packed ; // direct assign for first member. Macro not required.
    SAVE_BIT_FLAG(bit_flags, _is_tagged, 1) ;
    save_restore.SaveChar(bit_flags) ;
}

VeriStructUnion::VeriStructUnion(SaveRestore &save_restore)
  : VeriDataType(save_restore),
    _decls(0),
    _scope(0),
    _ids(0),
    _is_packed(0),
    _is_tagged(0),
    _contain_typeparam_type_ele(0), // No need to save/restore
    _ele_types(0), // No need to save/restore
    _ele_ids(0)    // No need to save/restore
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore member declarations
    _decls = RestoreArray(save_restore) ;
    // Restore member 'VeriIdDef's
    _ids = RestoreArray(save_restore) ;

    // Restore the bit flags in reverse order of saving.
    char bit_flags = save_restore.RestoreChar() ;
    RESTORE_BIT_FLAG(bit_flags, _is_tagged, 1) ;
    _is_packed = bit_flags ; // Direct assign for last member of bit_flags
}

/* ---------------------------- */

void
VeriEnum::Save(SaveRestore &save_restore) const
{
    VeriDataType::Save(save_restore) ;
    // Save base data type of enumerated types
    SaveNodePtr(save_restore, _base_type) ;
    // Save array of VeriIdDefs, which are the declared enum constants
    SaveArray(save_restore, _ids) ;
}

VeriEnum::VeriEnum(SaveRestore &save_restore)
  : VeriDataType(save_restore),
    _base_type(0),
    _ids(0),
    _enum_ids(0)
{
    // Restore the base data type of enumerated types
    _base_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
    // Restore array of VeriIdDefs
    _ids = RestoreArray(save_restore) ;

    _enum_ids = new Map(STRING_HASH, _ids ? _ids->Size(): 1) ;
    // 'copy' the DataType back pointers on id to 'this' (but only if not yet set)^M
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        // Set data type back-pointer, but only if not yet set.
        id->SetDataTypeForSave(this) ;
        (void) _enum_ids->Insert(id->Name(), id) ;
    }
}

/* ---------------------------- */

void
VeriDotStar::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the back-pointer to the scope in which this .* construct appears.
    SaveScopePtr(save_restore, _dot_star_scope) ;
}

VeriDotStar::VeriDotStar(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _dot_star_scope(0)
{
    // Restore the back-pointer to the scope in which this .* construct appears.
    _dot_star_scope = (VeriScope *)RestoreScopePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriIfOperator::Save(SaveRestore &save_restore) const
{
    VeriQuestionColon::Save(save_restore) ;
}

VeriIfOperator::VeriIfOperator(SaveRestore &save_restore)
  : VeriQuestionColon(save_restore)
{
}

/* ---------------------------- */

void
VeriSequenceConcat::Save(SaveRestore &save_restore) const
{
    VeriBinaryOperator::Save(save_restore) ;
    // Save cycle expression
    SaveNodePtr(save_restore, _cycle_delay_range) ;
}

VeriSequenceConcat::VeriSequenceConcat(SaveRestore &save_restore)
  : VeriBinaryOperator(save_restore),
    _cycle_delay_range(0)
{
    // Restore int expr or range defining number of cycles between left-end and right-start.
    _cycle_delay_range = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriClockedSequence::Save(SaveRestore &save_restore) const
{
    VeriUnaryOperator::Save(save_restore) ;
    // Save clocking event for the sequence
    SaveNodePtr(save_restore, _event_expr) ;
}

VeriClockedSequence::VeriClockedSequence(SaveRestore &save_restore)
  : VeriUnaryOperator(save_restore),
    _event_expr(0)
{
    // Restore clocking event for the sequence
    _event_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriAssignInSequence::Save(SaveRestore &save_restore) const
{
    VeriUnaryOperator::Save(save_restore) ;
    // Save the array of assignment statements to be executed when 'expr' holds
    SaveArray(save_restore, _stmts) ;
}

VeriAssignInSequence::VeriAssignInSequence(SaveRestore &save_restore)
  : VeriUnaryOperator(save_restore),
    _stmts(0)
{
    // Restore the array of assignment statements to be executed when 'expr' holds
    _stmts = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriDistOperator::Save(SaveRestore &save_restore) const
{
    VeriUnaryOperator::Save(save_restore) ;
    // Save the array of dist items on RHS of the dist token
    SaveArray(save_restore, _dist_list) ;
}

VeriDistOperator::VeriDistOperator(SaveRestore &save_restore)
  : VeriUnaryOperator(save_restore),
    _dist_list(0)
{
    // Restore the array of dist items on RHS of the dist token
    _dist_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriSolveBefore::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save array of VeriName's, indicating the variables which need to be solved
    SaveArray(save_restore, _solve_list) ;
    // Save array of VeriName's, indicating the variables which need to be
    // solved before the solved list
    SaveArray(save_restore, _before_list) ;
}

VeriSolveBefore::VeriSolveBefore(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _solve_list(0),
    _before_list(0)
{
    // Restore array of VeriName's, indicating the variables which need to be solved
    _solve_list = RestoreArray(save_restore) ;
    // Restore array of VeriName's, indicating the variables which need to be
    // solved before the solved list
    _before_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriCast::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the type to which casting will be done
    SaveNodePtr(save_restore, _target_type) ;
    // Save the expression which is being cast-ed
    SaveNodePtr(save_restore, _expr) ;
}

VeriCast::VeriCast(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _target_type(0),
    _expr(0)
{
    // Restore the type to which casting will be done
    _target_type = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the expression which is being cast-ed
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriNew::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the size expression
    SaveNodePtr(save_restore, _size_expr) ;
    // Save the argument array
    SaveArray(save_restore, _args) ;
    // Save the bit flags, currently only a single one
    save_restore.SaveInteger(_copy_constructor) ;
}

VeriNew::VeriNew(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _size_expr(0),
    _args(0),
    _copy_constructor(0)
{
    // Restore the size expression
    _size_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the argument array
    _args = RestoreArray(save_restore) ;
    // Restore the bit flags, currently only a single one
    _copy_constructor = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriConcatItem::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save member label expression
    SaveNodePtr(save_restore, _member_label) ;
    // Save the concat item expression
    SaveNodePtr(save_restore, _expr) ;
}

VeriConcatItem::VeriConcatItem(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _member_label(0),
    _expr(0)
{
    // Restore member label expression. If 0 it is the 'default' label.
    _member_label = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the concat item expression
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriNull::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
}

VeriNull::VeriNull(SaveRestore &save_restore)
  : VeriExpression(save_restore)
{
}

/* ---------------------------- */

void
VeriDollar::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
}

VeriDollar::VeriDollar(SaveRestore &save_restore)
  : VeriExpression(save_restore)
{
}

/* ---------------------------- */
void
VeriAssignmentPattern::Save(SaveRestore &save_restore) const
{
    VeriConcat::Save(save_restore) ;
    // Save the data type expression
    SaveNodePtr(save_restore, _type) ;
}

VeriAssignmentPattern::VeriAssignmentPattern(SaveRestore &save_restore)
  : VeriConcat(save_restore),
    _type(0)
{
    // Restore the data type expression
    _type = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriMultiAssignmentPattern::Save(SaveRestore &save_restore) const
{
    VeriMultiConcat::Save(save_restore) ;
    // Save the data type expression
    SaveNodePtr(save_restore, _type) ;
}

VeriMultiAssignmentPattern::VeriMultiAssignmentPattern(SaveRestore &save_restore)
  : VeriMultiConcat(save_restore),
    _type(0)
{
    // Restore the data type expression
    _type = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriForeachOperator::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array name of type VeriName
    SaveNodePtr(save_restore, _array_name) ;
    // Save the array of loop indexes
    SaveArray(save_restore, _loop_indexes) ;
    // Save the constraint set expression
    SaveNodePtr(save_restore, _constraint_set) ;
    // Save array of implicit loop variable declarations
    SaveArray(save_restore, _implicit_decl_list) ;
}

VeriForeachOperator::VeriForeachOperator(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _array_name(0),
    _loop_indexes(0),
    _constraint_set(0),
    _scope(0),
    _implicit_decl_list(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array name
    _array_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore loop indexes
    _loop_indexes = RestoreArray(save_restore) ;
    // Restore constraint set expression
    _constraint_set = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore implicit loop variable declarations
    _implicit_decl_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriStreamingConcat::Save(SaveRestore &save_restore) const
{
    VeriConcat::Save(save_restore) ;
    // Save integer oper type (left shift or right shift)
    save_restore.SaveInteger(_stream_operator) ;
    // Save expression specifying number of bits in each slice of data to be streamed
    SaveNodePtr(save_restore, _slice_size) ;
}

VeriStreamingConcat::VeriStreamingConcat(SaveRestore &save_restore)
  : VeriConcat(save_restore),
    _stream_operator(0),
    _slice_size(0)
{
    // Restore the oper type (left shift or right shift)
    _stream_operator = save_restore.RestoreInteger() ;
    // Restore expression specifying number of bits in each slice of data to be streamed
    _slice_size = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriCondPredicate::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save left operand expression
    SaveNodePtr(save_restore, _left) ;
    // Save right operand expression
    SaveNodePtr(save_restore, _right) ;
}

VeriCondPredicate::VeriCondPredicate(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _left(0),
    _right(0)
{
    // Restore left operand expression
    _left = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore right operand expression
    _right = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */
void
VeriDotName::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save VeriName variable name
    SaveNodePtr(save_restore, _name) ;
}

VeriDotName::VeriDotName(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _name(0)
{
    // Restore VeriName name
    _name = (VeriName *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriTaggedUnion::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save member name
    SaveNodePtr(save_restore, _member) ;
    // Save expression
    SaveNodePtr(save_restore, _expr) ;
}

VeriTaggedUnion::VeriTaggedUnion(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _member(0),
    _expr(0)
{
    // Restore member name
    _member = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore expression
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriWith::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save left expression
    SaveNodePtr(save_restore, _left) ;
}

VeriWith::VeriWith(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _left(0),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore left expression
    _left = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriWithExpr::Save(SaveRestore &save_restore) const
{
    VeriWith::Save(save_restore) ;
    // Save right expression
    SaveNodePtr(save_restore, _right) ;
}

VeriWithExpr::VeriWithExpr(SaveRestore &save_restore)
  : VeriWith(save_restore),
    _right(0)
{
    // Restore right expression
    _right = (VeriExpression *)RestoreNodePtr(save_restore) ;

    if (_right && _right->IsRange()) return ;
    // VCS Compatibility issue (test52): Set data type to iterator variables
    if (_left && _left->IsFunctionCall()) {
        VeriName *func_name = _left->GetPrefix() ;
        VeriName *prefix = func_name ? func_name->GetPrefix(): 0 ;
        VeriIdDef *array_id = prefix ? prefix->GetId() : 0 ;

        Array *args = _left->GetArgs() ;
        unsigned i ;
        VeriExpression *arg ;
        // Set data type to argument specified iterators :
        FOREACH_ARRAY_ITEM(args, i, arg) {
            if (!arg) continue ;
            const char *arg_name = arg->GetName() ;
            if (_scope && arg_name && !arg->GetPrefix()) { // It is simple name refererence
                VeriIdDef *id = _scope->FindLocal(arg_name) ;
                if (array_id && array_id->GetDataType() && id) {
                    id->SetDataType(array_id->GetDataType()) ;
                }
            }
        }
    }
    // Now set data type to default iterator
    VeriIdDef *id = _scope ? _scope->FindLocal("item"): 0 ;
    // Extract the array id whose data type is to be set to 'id'
    VeriExpression *array_ref = _left ;
    while (array_ref) {
        if (array_ref->GetFunctionType() && !array_ref->IsFunctionCall()) { // 'array_ref' has function name as suffix
            array_ref = array_ref->GetPrefix() ; // Its prefix is array reference
            break ;
        }
        array_ref = array_ref->GetPrefix() ;
    }
    VeriIdDef *array_id = array_ref ? array_ref->GetId() : 0 ;
    if (array_id && array_id->GetDataType() && id) {
        id->SetDataType(array_id->GetDataType()) ; // Set array id's data type
    }
}

/* ---------------------------- */

void
VeriInlineConstraint::Save(SaveRestore &save_restore) const
{
    VeriWith::Save(save_restore) ;
    // Save identifier referenced array:
    SaveArray(save_restore, _id_refs) ; // VIPER #6756
    // Save constraint block array
    SaveArray(save_restore, _constraint_block) ;
}

VeriInlineConstraint::VeriInlineConstraint(SaveRestore &save_restore)
  : VeriWith(save_restore),
    _id_refs(0),
    _constraint_block(0)
{
    // Restore identifier referenced array:
    _id_refs = RestoreArray(save_restore) ; // VIPER #6756
    // Restore constraint block array
    _constraint_block = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriBinValue::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save iff expression
    SaveNodePtr(save_restore, _iff_expr) ;
}

VeriBinValue::VeriBinValue(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _iff_expr(0)
{
    // Restore iff expression
    _iff_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriOpenRangeBinValue::Save(SaveRestore &save_restore) const
{
    VeriBinValue::Save(save_restore) ;
    // Save open range list array
    SaveArray(save_restore, _open_range_list) ;
}

VeriOpenRangeBinValue::VeriOpenRangeBinValue(SaveRestore &save_restore)
  : VeriBinValue(save_restore),
   _open_range_list(0)
{
    // Restore open range list array
    _open_range_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriTransBinValue::Save(SaveRestore &save_restore) const
{
    VeriBinValue::Save(save_restore) ;
    // Save trans list array
    SaveArray(save_restore, _trans_list) ;
}

VeriTransBinValue::VeriTransBinValue(SaveRestore &save_restore)
  : VeriBinValue(save_restore),
    _trans_list(0)
{
    // Save trans list array
    _trans_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriDefaultBinValue::Save(SaveRestore &save_restore) const
{
    VeriBinValue::Save(save_restore) ;
    // Save integer token for keyword VERI_SEQUENCE
    save_restore.SaveInteger(_sequence) ;
}

VeriDefaultBinValue::VeriDefaultBinValue(SaveRestore &save_restore)
  : VeriBinValue(save_restore),
    _sequence(0)
{
    // Restore integer token for keyword VERI_SEQUENCE
    _sequence = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriSelectBinValue::Save(SaveRestore &save_restore) const
{
    VeriBinValue::Save(save_restore) ;
    // Save select expression specified in selection bin
    SaveNodePtr(save_restore, _select_expr) ;
}

VeriSelectBinValue::VeriSelectBinValue(SaveRestore &save_restore)
  : VeriBinValue(save_restore),
    _select_expr(0)
{
    // Save select expression specified in selection bin
    _select_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriTransSet::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save array of trans range list
    SaveArray(save_restore, _trans_range_list) ;
}

VeriTransSet::VeriTransSet(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _trans_range_list(0)
{
    // Restore array of trans range list
    _trans_range_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriTransRangeList::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save array of trans item i.e range list
    SaveArray(save_restore, _trans_item) ;
    // Save repetition of transition item expression
    SaveNodePtr(save_restore, _repetition) ;
}

VeriTransRangeList::VeriTransRangeList(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _trans_item(0),
    _repetition(0)
{
    // Restore array of trans item i.e range list
    _trans_item = RestoreArray(save_restore) ;
    // Restore repetition of transition item expression
    _repetition = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriSelectCondition::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save bins expression
    SaveNodePtr(save_restore, _bins_expr) ;
    // Save range list
    SaveArray(save_restore, _range_list) ;
}

VeriSelectCondition::VeriSelectCondition(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _bins_expr(0),
    _range_list(0)
{
    // Restore bins expression
    _bins_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore range list
    _range_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriTypeOperator::Save(SaveRestore &save_restore) const
{
    VeriDataType::Save(save_restore) ;
    // Save expression
    SaveNodePtr(save_restore, _expr_or_data_type) ;
}

VeriTypeOperator::VeriTypeOperator(SaveRestore &save_restore)
  : VeriDataType(save_restore),
    _expr_or_data_type(0)
{
    // Restore expression
    _expr_or_data_type = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriPatternMatch::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save left expression
    SaveNodePtr(save_restore, _left) ;
    // Save scope which contains the right expression
    SaveScopePtr(save_restore, _scope) ;
    // Save right expression
    SaveNodePtr(save_restore, _right) ;
}

VeriPatternMatch::VeriPatternMatch(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _left(0),
    _right(0),
    _scope(0)
{
    // Restore left expression
    _left = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore scope which contains the right expression
    _scope = RestoreScopePtr(save_restore) ;
    // Restore right expression
    _right = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriConstraintSet::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save the expression array
    SaveArray(save_restore, _exprs) ;
}

VeriConstraintSet::VeriConstraintSet(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _exprs(0)
{
    // Restore the expression array
    _exprs = RestoreArray(save_restore) ;
}

//--------------------------------------------------------------
// #included from VeriId.h
//--------------------------------------------------------------

void
VeriIdDef::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save name string
    save_restore.SaveString(_name) ;

#ifdef VERILOG_STORE_ID_PRAGMAS
    // save the identifier pragmas (if there)
    save_restore.SaveInteger(_id_pragmas ? _id_pragmas->Size() : 0) ;
    save_restore.SaveInteger(_line_infos ? _line_infos->Size() : 0) ; // CARBON ADDITION
    // The id_pragmas table is a char*->char* table.
    MapIter mi ;
    const char *name ;
    const char *value ;
    FOREACH_MAP_ITEM(_id_pragmas, mi, &name, &value) {
        save_restore.SaveString(name) ;
        save_restore.SaveString(value) ;
    }
//CARBON_BEGIN
    // The line_infos table is a char*->char* table.
    MapIter mi2 ;
    const char *pname ;
    const char *pline ;
    FOREACH_MAP_ITEM(_line_infos, mi2, &pname, &pline) {
        save_restore.SaveString(pname) ;
        save_restore.SaveString(pline) ;
    }
//CARBON_END
#endif
    // Save-restore not required for bit flag _is_dummy_id
}

VeriIdDef::VeriIdDef(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _name(0)
#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    // This is set in the scope constructor.
    , _owning_scope(0) // set owning scope when identifier is added to a scope.
#endif
#ifdef VERILOG_STORE_ID_PRAGMAS
    , _id_pragmas(0) // identifier pragmas (char*->char* table)
//CARBON_BEGIN
    , _line_infos(0) // line info for pragmas (char*->char* table)
//CARBON_END
#endif
{
    // Restore the name of this identifier
    char *name = save_restore.RestoreString() ;
#ifdef VERILOG_SHARE_STRINGS
    _name = Strings::CreateConstantString(name) ; // used shared string
    Strings::free(name) ;
#else
    _name = name ;
#endif

#ifdef VERILOG_STORE_ID_PRAGMAS
    // restore the identifier pragmas (if there)
    int size = save_restore.RestoreInteger() ;
//CARBON_BEGIN
    int size2 = save_restore.RestoreInteger() ;
//CARBON_END
    if (size) {
        // The id_pragmas table is a char*->char* table.
        char *name, *value ;
        for (int i=0;i<size; i++) {
            name = save_restore.RestoreString() ;
            value = save_restore.RestoreString() ;
            // add to this identifier
            AddIdPragma(name,value) ;
            // free the restored strings.
            Strings::free(name) ;
            Strings::free(value) ;
        }
    }
    // CARBON_BEGIN
    if (size2) {
        // The id_pragmas table is a char*->char* table.
        char *name2, *value2 ;
        for (int i=0;i<size2; i++) {
            name2 = save_restore.RestoreString() ;
            value2 = save_restore.RestoreString() ;
            // add to this identifier
            AddPragmaLineInfo(name2,value2) ; // add to current _line_infos table.
            // free the restored strings.
            Strings::free(name2) ;
            Strings::free(value2) ;
        }
    }
    // CARBON_END
#endif
}

/* ---------------------------- */

void
VeriVariable::Save(SaveRestore &save_restore) const
{
     // Dimensions not saved. Restored in VeriScope::VeriScope(SaveRestore&)
    VeriIdDef::Save(save_restore) ;
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    // Save  VeriVariable
    SaveNodePtr(save_restore, _link_var) ;

    // Save _fsm_constants map size
    save_restore.SaveInteger(_fsm_constants ? _fsm_constants->Size() : 0) ;
    // Save table to keep track of constants that can be used for state machines
    // The fsm constants is a "long int" -> VeriExpression* table.
    //long int li ;
    char *int_img ;
    char *expr ; // VIPER #7995 : Key and Value are now char*
    MapIter mi ;
    FOREACH_MAP_ITEM(_fsm_constants, mi, &int_img, &expr) {
        //save_restore.SaveLong(li) ;
        save_restore.SaveString(int_img) ;
        //VeriTreeNode::SaveNodePtr(save_restore, expr) ;
        save_restore.SaveString(expr) ;
    }
#endif
    // Don't save the '_data_type' backpointer. Rather save it by calling the SetDataTypeForSave routine.

    // Save the bit flags
    unsigned bit_flags = 0 ; // Initialize it to '0'
    // Save the type information i.e. VERI_WIRE / VERI_REG / VERI_INTEGER etc (net/reg type)
    bit_flags = _type ;
    // Save the direction flag (3 bits encoded form of tokens VERI_INPUT / VERI_INOUT / VERI_OUTPUT). Used by ports only.
    SAVE_BIT_FLAG(bit_flags, _dir, 3) ;
    SAVE_BIT_FLAG(bit_flags, _implicit_net,1) ; // Flag that variable is implicitly declared
    // Flags that determine if an id is assigned or used. Set during analysis/resolve
    SAVE_BIT_FLAG(bit_flags, _is_used, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_assigned, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_concurrent_assigned, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_concurrent_used, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_assigned_before_used, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_used_before_assigned, 1) ;
    // Flag to see if var can be (onehot) encoded
    SAVE_BIT_FLAG(bit_flags, _can_be_onehot, 1) ;
    // Flag to see if var can be used as a dual-port RAM
    SAVE_BIT_FLAG(bit_flags, _can_be_dual_port_ram, 1) ;
    SAVE_BIT_FLAG(bit_flags, _can_be_multi_port_ram, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_member, 1) ; // Flag to check if this variable is actually a struct/union member. (SV only)
    SAVE_BIT_FLAG(bit_flags, _is_modport_port, 1) ; // Flag to check if this variable is a modport port (member of a modport) (SV only)
    SAVE_BIT_FLAG(bit_flags, _is_ansi_port, 1) ; // Flag to check if this variable is an ansi port
    SAVE_BIT_FLAG(bit_flags, _is_constant, 1) ; // Flag to check if this variable is a constant
    SAVE_BIT_FLAG(bit_flags, _is_automatic, 1) ; // Flag to check if this variable is automatic (non-static)
    SAVE_BIT_FLAG(bit_flags, _is_iterator, 1) ; // Flag to check if this variable is iterator
    SAVE_BIT_FLAG(bit_flags, _is_localvar, 1) ; // Flag to check if this variable is local variable of sequence/property
    SAVE_BIT_FLAG(bit_flags, _is_private, 1) ; // Flag to check if this variable is private member of class
    SAVE_BIT_FLAG(bit_flags, _is_protected, 1) ; // Flag to check if this variable is protected member of class
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags
    // VIPER #5949: Save the kind information i.e. VERI_WIRE / VERI_REG / VERI_INTEGER etc (net/reg type)
    bit_flags = 0 ;
    bit_flags = _kind ;
    SAVE_BIT_FLAG(bit_flags, _is_implicit_non_ansi_port, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_ram_overridden, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_clocking_decl_id, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_used_in_for_loop_index, 1) ; // VIPER #5239
    SAVE_BIT_FLAG(bit_flags, _is_static_class_variable, 1) ; // VIPER #6907
    SAVE_BIT_FLAG(bit_flags, _is_import, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_export, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_onehot_overridden, 1) ; // VIPER #8045
    SAVE_BIT_FLAG(bit_flags, _unused, 9) ;
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags
}

VeriVariable::VeriVariable(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    _link_var(0),
    _fsm_constants(0),
#endif
    _initial_value(0),
    _type(0),
    _dir(0),
    _implicit_net(0),
    _is_used(0),
    _is_assigned(0),
    _is_concurrent_assigned(0),
    _is_concurrent_used(0),
    _is_assigned_before_used(0),
    _is_used_before_assigned(0),
    _can_be_onehot(0),
    _can_be_dual_port_ram(0),
    _can_be_multi_port_ram(0),
    _is_member(0),
    _is_modport_port(0),
    _is_ansi_port(0),
    _is_constant(0),
    _is_automatic(0),
    _is_iterator(0),
    _is_localvar(0),
    _is_private(0),
    _is_protected(0),
    _kind(0), // VIPER #5949
    _is_implicit_non_ansi_port(0), // VIPER #6506
    _is_ram_overridden(0),
    _is_clocking_decl_id(0),
    _is_se_created_vi(0), // VIPER #6992 : No need to save/restore
    _is_used_in_for_loop_index(0), // VIPER #5239
    _is_static_class_variable(0),
    _is_import(0),
    _is_export(0),
    _is_onehot_overridden(0),
    _unused(0),
    _data_type(0)
    , _dimensions(0)
{
     // Dimensions not saved. Restored in VeriScope::VeriScope(SaveRestore&)
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    // Restore  VeriVariable
    _link_var = (VeriVariable *)RestoreNodePtr(save_restore) ;

    // Restore map size
    unsigned size = save_restore.RestoreInteger() ;
    if (size) {
        // The fsm constants is a "long int" -> VeriExpression* table
        // Restore _fsm_constants map
        unsigned i ;
        long int li ;
        char *int_img = 0 ;
        char *param_name = 0 ;
        _fsm_constants = new Map(STRING_HASH,size) ;
        for(i=0; i<size; i++) {
            //li = save_restore.RestoreLong() ;
            // VIPER #7995 : Key and Value of _fsm_constants are char* now
            if (save_restore.GetVersion() >= 0x0ab0002c) {
                int_img = save_restore.RestoreString() ;
                param_name = save_restore.RestoreString() ;
            } else {
                li = save_restore.RestoreLong() ;
                int_img = Strings::itoa(li) ;
                VeriExpression *expr = (VeriExpression*)VeriTreeNode::RestoreNodePtr(save_restore) ;
                VeriIdDef *id = expr ? expr->FullId(): 0 ;
                param_name = id ? Strings::save(id->Name()): 0 ;
                // VIPER #8044 : 'expr' is not owned by '_fsm_constants', it is
                // set from other expression (like state==3'b001). So we cannot
                // delete 'expr' here.
                //delete expr ; // Not stored anywhere
            }
            (void) _fsm_constants->Insert(int_img, param_name) ;
        }
    }
#endif
    // Set the '_data_type' backpointer by calling the SetDataTypeForSave routine in the restore
    // routine for VeriDataDecl.
    // Restore the bit flags in an order reverse to saving.
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_protected, 1) ; // Flag to check if this variable is protected
    RESTORE_BIT_FLAG(bit_flags, _is_private, 1) ; // Flag to check if this variable is private
    RESTORE_BIT_FLAG(bit_flags, _is_localvar, 1) ; // Flag to check if this variable is local var of seq/prop
    RESTORE_BIT_FLAG(bit_flags, _is_iterator, 1) ; // Flag to check if this variable is cwiterator
    RESTORE_BIT_FLAG(bit_flags, _is_automatic, 1) ; // Flag to check if this variable is automatic (non-static)
    RESTORE_BIT_FLAG(bit_flags, _is_constant, 1) ; // Flag to check if this variable is a constant
    RESTORE_BIT_FLAG(bit_flags, _is_ansi_port, 1) ; // Flag to check if this variable is an ansi port
    RESTORE_BIT_FLAG(bit_flags, _is_modport_port, 1) ; // Flag to check if this variable is a modport port (member of a modport) (SV only)
    RESTORE_BIT_FLAG(bit_flags, _is_member, 1) ; // Flag to check if this variable is actually a struct/union member. (SV only)
    // Flag to see if varable can be used as a dual-port or multi-port RAM
    RESTORE_BIT_FLAG(bit_flags, _can_be_multi_port_ram,1) ;
    RESTORE_BIT_FLAG(bit_flags, _can_be_dual_port_ram,1) ;
    // Flag to see if varable can be (onehot) encoded
    RESTORE_BIT_FLAG(bit_flags, _can_be_onehot, 1) ;

    // Flags that determine if an id is assigned or used. Set during analysis/resolve
    RESTORE_BIT_FLAG(bit_flags, _is_used_before_assigned, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_assigned_before_used, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_concurrent_used, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_concurrent_assigned, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_assigned, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_used, 1) ;
    // Flag that variable is implicitly declared
    RESTORE_BIT_FLAG(bit_flags,_implicit_net, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _dir, 3) ;
    _type = bit_flags ; // No need for using macro as last element in the integer
    // Restore the bit flags in an order reverse to saving.
    // VIPER #5949: Restore the kind information i.e. VERI_WIRE / VERI_REG / VERI_INTEGER etc (net/reg type)
    bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _unused, 9) ;
    RESTORE_BIT_FLAG(bit_flags, _is_onehot_overridden, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_export, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_import, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_static_class_variable, 1) ; // VIPER #6907
    RESTORE_BIT_FLAG(bit_flags, _is_used_in_for_loop_index, 1) ; // VIPER #5239
    RESTORE_BIT_FLAG(bit_flags, _is_clocking_decl_id, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_ram_overridden, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_implicit_non_ansi_port, 1) ;
    _kind = bit_flags ;
}

void
VeriVariable::SetDataTypeForSave(VeriDataType *data_type)
{
    // The implementation is similar to the function SetDataTypeForCopy
    if (data_type && (!_data_type || !_data_type->Type())) {
        _data_type = data_type ;
    }
}

/* ---------------------------- */

void
VeriInstId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // Dimensions not saved. Restored in VeriScope::VeriScope(SaveRestore&)
    // _port_connects array not saved. Restored in VeriScope::VeriScope(SaveRestore&)

    // Don't save/restore back-pointer '_the_instance'. Rather set it from VeriModuleInstantiation.
}

VeriInstId::VeriInstId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _dimensions(0),
    _port_connects(1),
// back-pointer :
    _the_instance(0)
{
    // Dimensions not saved. Restored in VeriScope::VeriScope(SaveRestore&)
    // _port_connects array not saved. Restored in VeriScope::VeriScope(SaveRestore&)

    // Don't save back-pointer '_the_instance'. Rather set it from VeriModuleInstantiation.

    // Create an array which is to be set later from VeriScope::VeriScope(SaveRestore &)
    // _port_connects = new Array() ;
}

/* ---------------------------- */

void
VeriModuleId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
}

VeriModuleId::VeriModuleId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _local_scope(0),
    _tree(0)
    , _is_extern(0)
    , _is_package(0)
    , _is_export_subprog(0)
{
}

void
VeriConfigurationId::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriModuleId::Save(save_restore) ;

    // No element exists in this class
}

VeriConfigurationId::VeriConfigurationId(SaveRestore &save_restore)
  : VeriModuleId(save_restore)
{
    // No element exists in this class
}

/* ---------------------------- */

void
VeriUdpId::Save(SaveRestore &save_restore) const
{
    VeriModuleId::Save(save_restore) ;
}

VeriUdpId::VeriUdpId(SaveRestore &save_restore)
  : VeriModuleId(save_restore)
{
}

/* ---------------------------- */

void
VeriTaskId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;

    // Since 5/2007, taskid has bit flags :
    unsigned bit_flags = 0 ; // Initialize it to '0'
    SAVE_BIT_FLAG(bit_flags, _is_automatic,1) ; // Flag that task is automatic
    SAVE_BIT_FLAG(bit_flags, _is_forkjoin,1) ; // Flag that it is extern forkjoin task
    SAVE_BIT_FLAG(bit_flags, _is_import, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_export, 1) ;
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags
}

VeriTaskId::VeriTaskId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _local_scope(0),
    _tree(0),
    _is_automatic(0),
    _is_forkjoin(0),
    _is_import(0),
    _is_export(0)
{
    // Since 5/2007, taskid has bit flags :
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_export, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_import, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_forkjoin, 1) ; // Flag to check if this task is declared as extern forkjoin
    RESTORE_BIT_FLAG(bit_flags, _is_automatic, 1) ; // Flag to check if this variable is automatic (non-static)
}

/* ---------------------------- */

void
VeriFunctionId::Save(SaveRestore &save_restore) const
{
    VeriVariable::Save(save_restore) ;
    save_restore.SaveInteger(_function_type) ;
}

VeriFunctionId::VeriFunctionId(SaveRestore &save_restore)
  : VeriVariable(save_restore),
    _local_scope(0),
    _tree(0)
    , _function_type(0)
{
    _function_type = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriGenVarId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // No need to save/restore initial value. It is set from VeriScope::VeriScope(SaveRestore &)
}

VeriGenVarId::VeriGenVarId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _initial_value(0)
{
    // No need to save/restore initial value. It is set from VeriScope::VeriScope(SaveRestore &)
}

/* ---------------------------- */

void
VeriParamId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // Don't save/resore 'data_type' back-pointer. Rather set it with SetDataTypeForSave routine.

    // Dimensions not saved. Restored in VeriScope::VeriScope(SaveRestore&)

    // No need to save/restore initial value. It is set from VeriScope::VeriScope(SaveRestore &)
    // save the bit flags
    unsigned bit_flags = 0 ;
    bit_flags = _param_type ; // 12  bits. Indicates VERI_PARAMETER, VERI_LOCALPARAM, VERI_DEFPARAM, VERI_SPECPARAM
    SAVE_BIT_FLAG(bit_flags, _type, 12) ; // 12  bits. Indicates VERI_WIRE / VERI_REG / VERI_INTEGER etc (net/reg type)
    // Two flags that determine if an id is assigned or used
    SAVE_BIT_FLAG(bit_flags, _is_used, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_assigned, 1) ;
    // Flag to check if this variable is declared as a type. (SV only)
    SAVE_BIT_FLAG(bit_flags, _is_type, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_elaborated, 1) ;
    SAVE_BIT_FLAG(bit_flags, _id_implicit, 1) ; // VIPER #8109
    save_restore.SaveInteger(bit_flags) ; // Save the unsigned integer to file
}

VeriParamId::VeriParamId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _data_type(0),
    _dimensions(0),
    _initial_value(0),
    _param_type(0),
    _type(0),
    _is_used(0),
    _is_assigned(0),
    _is_type(0)
    ,_is_elaborated(0)
    , _is_processing(0) // No need to save/restore this flag
    , _id_implicit(0) // VIPER #8109
#ifdef VERILOG_PATHPULSE_PORTS
    ,_path_pulse_ports(0)
#endif
{
    // Don't save/resore 'data_type' back-pointer. Rather set it with SetDataTypeForSave routine.

    // Dimensions not saved. Restored in VeriScope::VeriScope(SaveRestore&)

    // No need to save/restore initial value. It is set from VeriScope::VeriScope(SaveRestore &)

    // Restore the bit flags in an order opposite to the order they got saved
    unsigned bit_flags = save_restore.RestoreInteger() ;

    // VIPER #8155: Need to check for version number for bit flags too:
    if (save_restore.GetVersion() >= 0x0ab0002d) RESTORE_BIT_FLAG(bit_flags, _id_implicit, 1) ; // VIPER #8109
    RESTORE_BIT_FLAG(bit_flags, _is_elaborated, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_type, 1) ;
    // Two flags that determine if an id is assigned or used
    RESTORE_BIT_FLAG(bit_flags, _is_assigned, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_used, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _type, 12) ; // 12  bits. Indicates VERI_PARAMETER, VERI_LOCALPARAM, VERI_DEFPARAM, VERI_SPECPARAM
    _param_type = bit_flags ; // Macro not called as last item in int. Indicates VERI_WIRE / VERI_REG / VERI_INTEGER etc (net/reg type)
}

void
VeriParamId::SetDataTypeForSave(VeriDataType *data_type)
{
    // The implementation is similar to the function SetDataTypeForCopy
    if (data_type && !_data_type) {
        _data_type = data_type ;
    }
}

/* ---------------------------- */

void
VeriBlockId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    save_restore.SaveInteger(_is_unnamed_blk) ; // Save the unsigned integer to file
    save_restore.SaveString(_orig_name) ;
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
}

VeriBlockId::VeriBlockId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _local_scope(0),
    _tree(0)
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    , _is_unnamed_blk(0)
    , _orig_name(0)
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
{
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if ((save_restore.GetVersion() >= 0x0ab0002d) || RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        _is_unnamed_blk = save_restore.RestoreInteger() ;
        _orig_name = save_restore.RestoreString() ;
    }
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
}

/* ---------------------------- */

void
VeriInterfaceId::Save(SaveRestore &save_restore) const
{
    VeriModuleId::Save(save_restore) ;
}

VeriInterfaceId::VeriInterfaceId(SaveRestore &save_restore)
  : VeriModuleId(save_restore),
    _ele_ids(0) // No need to save/restore
{
}

/* ---------------------------- */

void
VeriProgramId::Save(SaveRestore &save_restore) const
{
    VeriModuleId::Save(save_restore) ;
}

VeriProgramId::VeriProgramId(SaveRestore &save_restore)
  : VeriModuleId(save_restore)
{
}

/* ---------------------------- */

// VIPER #5710: vcs-compatibility: always_checker)
// Save-restore routines for class VeriCheckerId:
void
VeriCheckerId::Save(SaveRestore &save_restore) const
{
    VeriModuleId::Save(save_restore) ;
}

VeriCheckerId::VeriCheckerId(SaveRestore &save_restore)
  : VeriModuleId(save_restore)
{
}

/* ---------------------------- */

void
VeriTypeId::Save(SaveRestore &save_restore) const
{
    VeriVariable::Save(save_restore) ;

    // VIPER #7951 (duplicate_classes): Flag to identify if this type-id has been forward defined:
    save_restore.SaveInteger(_forward_type_defined) ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895:
    // Save bitflags
    unsigned bit_flags = 0 ;
    bit_flags = _unconstraint_unpacked_dim_count ; // Flag set in constructor
    SAVE_BIT_FLAG(bit_flags, _unconstraint_packed_dim_count, 15) ; //
    save_restore.SaveInteger(bit_flags) ; // Save the unconstaint dim count

    // Save package name
    save_restore.SaveString(_vhdl_package_name) ;
    // Save library name
    save_restore.SaveString(_vhdl_lib_name) ;
#endif
}

VeriTypeId::VeriTypeId(SaveRestore &save_restore)
  : VeriVariable(save_restore),
    _local_scope(0),
    _tree(0),
    _is_processing(0), // No need to save/restore
    _forward_type_defined(0)
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895:
    , _unconstraint_unpacked_dim_count(0)
    , _unconstraint_packed_dim_count(0)
    , _vhdl_package_name(0)
    , _vhdl_lib_name(0)
#endif
    , _elab_created(0)
{
    // VIPER #7951 (duplicate_classes): Flag to identify if this type-id has been forward defined:
    if (save_restore.GetVersion() >= 0x0ab0002b) _forward_type_defined = save_restore.RestoreInteger() ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895:
    // Restore bit flags
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _unconstraint_packed_dim_count, 15) ; // Flag stating that module is fully analyzed
    _unconstraint_unpacked_dim_count = bit_flags ; // restore unconstaint dimension count

    // Restore package name
    _vhdl_package_name = save_restore.RestoreString() ;
    // Restore library name
    _vhdl_lib_name = save_restore.RestoreString() ;
#endif
}

/* ---------------------------- */

void
VeriModportId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
}

VeriModportId::VeriModportId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _local_scope(0),
    _tree(0),
    _ele_ids(0) // No need to save/restore
{
}

/* ---------------------------- */

void
VeriOperatorId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // Save the oper type
    save_restore.SaveInteger(_oper_type) ;
}

VeriOperatorId::VeriOperatorId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _oper_type(0),
    _tree(0)
{
    // Restore the unsigned oper type integer
    _oper_type = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriClockingId::Save(SaveRestore &save_restore) const
{
    VeriVariable::Save(save_restore) ;
    // VIPER #3663 : Save the default qualifier
    save_restore.SaveInteger(_is_default) ;
}

VeriClockingId::VeriClockingId(SaveRestore &save_restore)
  : VeriVariable(save_restore),
    _local_scope(0),
    _tree(0),
    _is_default(0)
{
    // VIPER #3663 : Restore the unsigned _is_default integer
    _is_default = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriProductionId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // Don't save/restore backpointer '_prod', rather set it from VeriProductionId::VeriProductionId(SaveRestore &)
}

VeriProductionId::VeriProductionId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _prod(0)
{
    // Don't save/restore backpointer '_prod', rather set it from VeriProductionId::VeriProductionId(SaveRestore &)
}

/* ---------------------------- */

void
VeriNamedPort::Save(SaveRestore &save_restore) const
{
    VeriVariable::Save(save_restore) ;
    // Save port expression
    SaveNodePtr(save_restore, _port_expr) ;
}

VeriNamedPort::VeriNamedPort(SaveRestore &save_restore)
  : VeriVariable(save_restore),
    _port_expr(0),
    _is_processing(0) // no need to save/restore
{
    // Restore port expression
    _port_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriCovgOptionId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // Save port expression
    SaveNodePtr(save_restore, _data_type) ;
}

VeriCovgOptionId::VeriCovgOptionId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _data_type(0)
{
    // Restore port expression
    _data_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */
void
VeriSeqPropertyFormal::Save(SaveRestore &save_restore) const
{
    VeriVariable::Save(save_restore) ;

    // Save the _actual as this id is now created for non-ansi ports
    SaveNodePtr(save_restore, _actual) ;
}

VeriSeqPropertyFormal::VeriSeqPropertyFormal(SaveRestore &save_restore)
  : VeriVariable(save_restore),
    _actual(0),
    _is_processing(0)
{
    // Restore actual
    _actual = (VeriExpression*)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */
void
VeriBinsId::Save(SaveRestore &save_restore) const
{
    VeriVariable::Save(save_restore) ;
}

VeriBinsId::VeriBinsId(SaveRestore &save_restore)
  : VeriVariable(save_restore)
{ }

/* ---------------------------- */

void
VeriLetId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
}

VeriLetId::VeriLetId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _local_scope(0),
    _tree(0),
    _is_processing(0)
{ }

/* ---------------------------- */

void
VeriExternForkjoinTaskId::Save(SaveRestore &save_restore) const
{
    VeriTaskId::Save(save_restore) ;
}
VeriExternForkjoinTaskId::VeriExternForkjoinTaskId(SaveRestore &save_restore)
    : VeriTaskId(save_restore),
    _trees(0)
{}

/* ---------------------------- */

void
VeriLabelId::Save(SaveRestore &save_restore) const
{
    VeriBlockId::Save(save_restore) ;

    // Save scope
    SaveScopePtr(save_restore, _scope) ;
}
VeriLabelId::VeriLabelId(SaveRestore &save_restore)
    : VeriBlockId(save_restore),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
}

/* ---------------------------- */
void
VeriPrototypeId::Save(SaveRestore &save_restore) const
{
    VeriIdDef::Save(save_restore) ;
    // Save the bit flags
    unsigned bit_flags = 0 ; // Initialize it to '0'
    // Save the direction flag (3 bits encoded form of tokens VERI_INPUT / VERI_INOUT / VERI_OUTPUT). Used by ports only.
    SAVE_BIT_FLAG(bit_flags, _dir, 3) ;
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags
    // Save the actual name
    SaveNodePtr(save_restore, _actual_name) ;
    // Save actual identifier
    SaveNodePtr(save_restore, _actual_id) ;
}
VeriPrototypeId::VeriPrototypeId(SaveRestore &save_restore)
  : VeriIdDef(save_restore),
    _dir(0),
    _actual_name(0),
    _actual_id(0)
{
    // Restore the bit flags in an order reverse to saving.
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _dir, 3) ;
    // Restore actual name:
    _actual_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore identifier :
    _actual_id = (VeriIdDef*)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

//--------------------------------------------------------------
// #included from VeriMisc.h
//--------------------------------------------------------------

void
VeriStrength::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save the VERI drive/charge tokens integer
    save_restore.SaveInteger(_lval) ;
    // Save the VERI drive token integer.
    save_restore.SaveInteger(_rval) ;
}

VeriStrength::VeriStrength(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _lval(0),
    _rval(0)
{
    // Restore the VERI drive/charge tokens integer
    _lval = save_restore.RestoreInteger() ;
    // Restore the VERI drive token integer
    _rval = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriNetRegAssign::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save the LHS expression : VeriIdRef, VeriIndexedId, or VeriConcat
    SaveNodePtr(save_restore, _lval) ;
    // Save the RHS expression
    SaveNodePtr(save_restore, _val) ;
}

VeriNetRegAssign::VeriNetRegAssign(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _lval(0),
    _val(0)
{
    // Restore the LHS expression : VeriIdRef, VeriIndexedId, or VeriConcat
    _lval = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the RHS expression
    _val = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDefParamAssign::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save the LHS expression : VeriIdRef, VeriSelectedName
    SaveNodePtr(save_restore, _lval) ;
    // Save the RHS expression
    SaveNodePtr(save_restore, _val) ;
}

VeriDefParamAssign::VeriDefParamAssign(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _lval(0),
    _val(0)
{
    // Restore the LHS expression : VeriIdRef, VeriSelectedName
    _lval = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore the RHS expression
    _val = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriCaseItem::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save conditions array
    SaveArray(save_restore, _conditions) ;
    // Save statement
    SaveNodePtr(save_restore, _stmt) ;
}

VeriCaseItem::VeriCaseItem(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _conditions(0),
    _stmt(0)
    , _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore conditions
    _conditions = RestoreArray(save_restore) ;
    // Restore statement
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriGenerateCaseItem::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save conditions array
    SaveArray(save_restore, _conditions) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save the case item expression
    SaveNodePtr(save_restore, _item) ;
}

VeriGenerateCaseItem::VeriGenerateCaseItem(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _conditions(0),
    _item(0),
    _scope(0)
{
    // Restore conditions
    _conditions = RestoreArray(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    if (_scope) _scope->SetGenerateScope() ;
    // Restore min expression
    _item = (VeriModuleItem *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriPath::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save the polarity_operator integer
    save_restore.SaveInteger(_polarity_operator) ;
    // Save the path token integer
    save_restore.SaveInteger(_path_token) ;
    // Save the edge integer (VERI_POSEDGE VERI_NEGEDGE)
    save_restore.SaveInteger(_edge) ;
    // Save array of input expressions to the path
    SaveArray(save_restore, _in_terminals) ;
    // Save the output terminals to the path
    SaveArray(save_restore, _out_terminals) ;
    // Save output polarity_operator integer
    save_restore.SaveInteger(_out_polarity_operator) ;
    // Save the data source expression
    SaveNodePtr(save_restore, _data_source) ;
}

VeriPath::VeriPath(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _polarity_operator(0),
    _path_token(0),
    _edge(0),
    _in_terminals(0),
    _out_terminals(0),
    _out_polarity_operator(0),
    _data_source(0)
{
    // Restore the polarity_operator integer
    _polarity_operator = save_restore.RestoreInteger() ;
    // Restore the path token integer
    _path_token = save_restore.RestoreInteger() ;
    // Restore the edge integer (VERI_POSEDGE VERI_NEGEDGE)
    _edge = save_restore.RestoreInteger() ;
    // Restore array of input expressions to the path
    _in_terminals = RestoreArray(save_restore) ;
    // Restore the output terminals to the path
    _out_terminals = RestoreArray(save_restore) ;
    // Restore the output polarity_operator integer
    _out_polarity_operator = save_restore.RestoreInteger() ;
    // Restore the data source expression
    _data_source = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDelayOrEventControl::Save(SaveRestore &save_restore) const
{
    VeriExpression::Save(save_restore) ;
    // Save delay expression
    SaveNodePtr(save_restore, _delay_control) ;
    // Save repeat expression
    SaveNodePtr(save_restore, _repeat_event) ;
    // Save array of event expressions
    SaveArray(save_restore, _event_control) ;
    // VIPER #5922: Directly save/restore this bit field (only one for now):
    save_restore.SaveInteger(_is_cycle_delay) ;
}

VeriDelayOrEventControl::VeriDelayOrEventControl(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _delay_control(0),
    _repeat_event(0),
    _event_control(0)
    ,_is_cycle_delay(0)
{
    // Restore delay expression
    _delay_control = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore repeat expression
    _repeat_event = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of event expressions
    _event_control = RestoreArray(save_restore) ;
    // VIPER #5922: Directly save/restore this bit field (only one for now):
    _is_cycle_delay = save_restore.RestoreInteger() ;
}

void
VeriConfigRule::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriTreeNode::Save(save_restore) ;

    // Save the Array of char * library names:
    SaveStringArray(save_restore, _liblist) ;
}

VeriConfigRule::VeriConfigRule(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _liblist(0)
{
    // Restore the Array of char * library names:
    _liblist = RestoreStringArray(save_restore) ;
}

void
VeriInstanceConfig::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriConfigRule::Save(save_restore) ;

    // Save the VeriName * instance reference:
    SaveNodePtr(save_restore, _inst_ref) ;

    // Save the VeriUseClause * use clause:
    SaveNodePtr(save_restore, _use_clause) ;
}

VeriInstanceConfig::VeriInstanceConfig(SaveRestore &save_restore)
  : VeriConfigRule(save_restore),
    _inst_ref(0),
    _use_clause(0)
{
    // Restore the VeriName * instance reference:
    _inst_ref = (VeriName *)RestoreNodePtr(save_restore) ;

    // Restore the VeriUseClause * use clause:
    _use_clause = (VeriUseClause *)RestoreNodePtr(save_restore) ;
}

void
VeriCellConfig::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriConfigRule::Save(save_restore) ;

    // Save the VeriName * cell reference:
    SaveNodePtr(save_restore, _cell_ref) ;

    // Save the VeriUseClause * use clause:
    SaveNodePtr(save_restore, _use_clause) ;
}

VeriCellConfig::VeriCellConfig(SaveRestore &save_restore)
  : VeriConfigRule(save_restore),
    _cell_ref(0),
    _use_clause(0)
{
    // Restore the VeriName * cell reference:
    _cell_ref = (VeriName *)RestoreNodePtr(save_restore) ;

    // Restore the VeriUseClause * use clause:
    _use_clause = (VeriUseClause *)RestoreNodePtr(save_restore) ;
}

void
VeriDefaultConfig::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriConfigRule::Save(save_restore) ;

    // No element exists in this class
}

VeriDefaultConfig::VeriDefaultConfig(SaveRestore &save_restore)
  : VeriConfigRule(save_restore)
{
    // No element exists in this class
}

void
VeriUseClause::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriTreeNode::Save(save_restore) ;

    // Save the VeriName * cell reference:
    SaveNodePtr(save_restore, _cell_ref) ;

    // Save the bit flag that indicates whether the referred cell is a configuration:
    save_restore.SaveChar(_is_config) ;

    // Save parameter values
    SaveArray(save_restore, _param_values) ;
}

VeriUseClause::VeriUseClause(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _cell_ref(0),
    _is_config(0),
    _param_values(0) // VIPER #7416 : parameter value assignment
{
    // Restore the VeriName * cell reference:
    _cell_ref = (VeriName *)RestoreNodePtr(save_restore) ;

    // Restore the bit flag that indicates whether the referred cell is a configuration:
    _is_config = save_restore.RestoreChar() ;

    // Restore parameter value assignment
    _param_values = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriClockingDirection::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save the direction VERI_INPUT, VERI_OUTPUT or VERI_INOUT
    save_restore.SaveInteger(_dir) ;
    // Save the edge specification VERI_POSEDGE or VERI_NEGEDGE or 0
    save_restore.SaveInteger(_edge) ;
    // Save delay control expression
    SaveNodePtr(save_restore, _delay_control) ;
}

VeriClockingDirection::VeriClockingDirection(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _dir(0),
    _edge(0),
    _delay_control(0)
{
    // Restore integer direction VERI_INPUT, VERI_OUTPUT or VERI_INOUT
    _dir = save_restore.RestoreInteger() ;
    // Restore integer edge specification VERI_POSEDGE or VERI_NEGEDGE or 0
    _edge = save_restore.RestoreInteger() ;
    // Restore delay control expression
    _delay_control = (VeriDelayOrEventControl *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriProduction::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save data type expression
    SaveNodePtr(save_restore, _data_type) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save id
    SaveNodePtr(save_restore, _id) ;
    // Save formals array (array of VeriAnsiPortDecl*)
    SaveArray(save_restore, _formals) ;
    // Save array production items (VeriProductionItem*)
    SaveArray(save_restore, _items) ;
    // Save array of production arguments in ORDER
    SaveArray(save_restore, _ports) ;
}

VeriProduction::VeriProduction(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _data_type(0),
    _id(0),
    _formals(0),
    _items(0),
    _ports(0),
    _scope(0)
{
    // Restore data type expression
    _data_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Set the back-pointer '_prod' of _id
    if (_id) _id->SetProduction(this) ;
    // Restore formals array (array of VeriAnsiPortDecl*)
    _formals = RestoreArray(save_restore) ;
    // Restore array production items (VeriProductionItem*)
    _items = RestoreArray(save_restore) ;
    // Restore array of production arguments in ORDER
    _ports = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriProductionItem::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save the integer flag to indicate VERI_RAND and VERI_JOIN token exists
    save_restore.SaveInteger(_is_rand_join) ;
    // Save rand join expression
    SaveNodePtr(save_restore, _expr) ;
    // Save array of production items
    SaveArray(save_restore, _items) ;
    // Save probability of selecting terminal/non-terminal
    SaveNodePtr(save_restore, _w_spec) ;
    // Save code block of this item
    SaveNodePtr(save_restore, _code_blk) ;
}

VeriProductionItem::VeriProductionItem(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _is_rand_join(0),
    _expr(0),
    _items(0),
    _w_spec(0),
    _code_blk(0)
{
    // Restore the integer flag to indicate VERI_RAND and VERI_JOIN token exists
    _is_rand_join = save_restore.RestoreInteger() ;
    // Restore rand join expression
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of production items (array of VeriStatement *)
    _items = RestoreArray(save_restore) ;
    // Restore probability of selecting terminal/non-terminal
    _w_spec = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore code block of this item
    _code_blk = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

#ifdef VERILOG_PATHPULSE_PORTS
/* ---------------------------- */
void VeriPathPulseValPorts::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
    // Save input terminal descriptor
    SaveNodePtr(save_restore, _input_port) ;
    // Save output terminal descriptor
    SaveNodePtr(save_restore, _output_port) ;
}
VeriPathPulseValPorts::VeriPathPulseValPorts(SaveRestore &save_restore)
    : VeriTreeNode(save_restore),
    _input_port(0),
    _output_port(0)
{
    // Restore input terminal descriptor
    _input_port = (VeriName*)RestoreNodePtr(save_restore) ;
    // Restore output terminal descriptor
    _output_port = (VeriName*)RestoreNodePtr(save_restore) ;
}
/* ---------------------------- */
#endif
//--------------------------------------------------------------
// #included from VeriModule.h
//--------------------------------------------------------------

void
VeriModule::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // VIPER 3362: Save/Restore bit flags at the very beginning.
    // Save bitflags
    unsigned bit_flags = 0 ;
    bit_flags = _analysis_dialect ; // Flag set in constructor (if `celldefine was set)
    SAVE_BIT_FLAG(bit_flags, _is_celldefine, 1) ; // Save the language mode this module was parsed in
    SAVE_BIT_FLAG(bit_flags, _default_nettype, 11) ; // 11 bit flag. VERI token set in constructor
    SAVE_BIT_FLAG(bit_flags, _unconnected_drive, 11) ; // 11 bit flag. VERI token set in constructor

    // Save bit flags used during elaboration (if included)
    SAVE_BIT_FLAG(bit_flags,  _is_analyzed, 1) ;  // Flag stating that module is fully analyzed (set in AnalyzeFull())
    SAVE_BIT_FLAG(bit_flags, _has_analyze_error, 1) ; // Flag that module showed an error in full analysis (set in AnalyzeFull())
    SAVE_BIT_FLAG(bit_flags, _is_compiled, 1) ;  // Flag set during (at) elaboration
    SAVE_BIT_FLAG(bit_flags, _is_static_elaborated, 1) ;  // Flag to test if this module is already statically elaborated
    SAVE_BIT_FLAG(bit_flags, _compile_as_blackbox, 1) ;  // Flag set by user SetCompileAsBlackbox()
    // save the bit flags to file (32 done)
    save_restore.SaveInteger(bit_flags) ;

    // More flags :
    bit_flags = 0 ;
    SAVE_BIT_FLAG(bit_flags, _is_root_module, 1) ; // Save flag that module is a 'root' module
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895:
    SAVE_BIT_FLAG(bit_flags, _is_vhdl_package, 1) ; // Save flag that the package is converted from a vhdl one
#endif

    // save these bit flags to file too
    save_restore.SaveInteger(bit_flags) ;

    // VIPER #7379: Save/Restore bit flags and other info for optional compiler directives:
    // Restore bitflags in reverse order of saving
    bit_flags = 0 ;
    SAVE_BIT_FLAG(bit_flags, _delay_mode_distributed, 1) ;
    SAVE_BIT_FLAG(bit_flags, _delay_mode_path, 1) ;
    SAVE_BIT_FLAG(bit_flags, _delay_mode_unit, 1) ;
    SAVE_BIT_FLAG(bit_flags, _delay_mode_zero, 1) ;
    save_restore.SaveInteger(bit_flags) ;
    save_restore.SaveString(_default_decay_time) ;
    save_restore.SaveString(_default_trireg_strength) ;

    // Save module id
    SaveNodePtr(save_restore, _id) ;
    // Save port scope (VIPER #5954)
    SaveScopePtr(save_restore, _port_scope) ;

    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of package import decls (VeriImportDecl*)
    SaveArray(save_restore, _package_import_decls) ;
    // Save array of parameter VeriDataDecl(VeriModuleItem)*
    SaveArray(save_restore, _parameter_connects) ;
    // Save array of VeriExpression*
    SaveArray(save_restore, _port_connects) ;
    // Save module's module items array
    SaveArray(save_restore, _module_items) ;
    // Save array of all parameters of this module in ORDER
    SaveArray(save_restore, _parameters) ;
    // Save array of all ports of this module in ORDER
    SaveArray(save_restore, _ports) ;
    // Save array of instantiations forced on this module by external bind directives
    SaveArray(save_restore, _bind_instances) ;
    // Save integer for time that this module was created
    save_restore.SaveLong(_timestamp) ;
   // Save string for setting of `timescale at module parsing time
    save_restore.SaveString(_timescale) ;
    // Save library
    VeriLibrary::SaveLibPtr(save_restore, _parent_library) ;
}

VeriModule::VeriModule(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _package_import_decls(0),
    _parameter_connects(0),
    _port_connects(0),
    _module_items(0),
    _scope(0),
    _parameters(0),
    _ports(0),
    _bind_instances(0),
    _timestamp(0),
    _analysis_dialect(0),
    _is_celldefine(0),
    _default_nettype(0),
    _unconnected_drive(0),
    _is_analyzed(0),
    _has_analyze_error(0),
    _is_compiled(0),
    _is_static_elaborated(0),
    _compile_as_blackbox(0),
    _is_root_module(0),
    _affected_by_path_name(0), // no need to save/restore, elaboration specific
    _original_module_name(0),
    _timescale(0),
    _parent_library(0)
    , _port_scope(0)
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    , _linefile_structs(0) // VIPER #5932: No need to save/restore it
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    , _module_item_replaced(0) // VIPER #6550
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    , _is_vhdl_package(0)      // VIPER #6895: flag for converted verilog package from a vhld one
    , _is_already_processed(0) // VIPER #6895: flag on package that is already elaborated
#endif
    , _delay_mode_distributed(0),
    _delay_mode_path(0),
    _delay_mode_unit(0),
    _delay_mode_zero(0),
    _default_decay_time(0),
    _default_trireg_strength(0)
{
    // VIPER 3362: Save/Restore bit flags at the very beginning.
    // Restore bitflags in reverse order of saving
    unsigned bit_flags = save_restore.RestoreInteger() ;
    // Restore bit flags used during elaboration in reverse order of saving
    RESTORE_BIT_FLAG(bit_flags, _compile_as_blackbox, 1) ; // Flag stating that module is fully analyzed
    RESTORE_BIT_FLAG(bit_flags, _is_static_elaborated, 1) ; // Flag to test if this module is already statically elaborated
    RESTORE_BIT_FLAG(bit_flags, _is_compiled, 1) ; //  Flag set during (at) elaboration
    RESTORE_BIT_FLAG(bit_flags, _has_analyze_error, 1) ; // Flag that module showed an error in full analysis
    RESTORE_BIT_FLAG(bit_flags, _is_analyzed, 1) ; // Restore flag set in AnalyzeFull
    RESTORE_BIT_FLAG(bit_flags, _unconnected_drive, 11) ; // VERI token set in constructor
    RESTORE_BIT_FLAG(bit_flags, _default_nettype, 11) ; // VERI token set in constructor
    RESTORE_BIT_FLAG(bit_flags, _is_celldefine,1) ;
    _analysis_dialect = bit_flags ; // Restore integer for analysis mode of this module :

    // Restore 'more' flags :
    bit_flags = save_restore.RestoreInteger() ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895:
    RESTORE_BIT_FLAG(bit_flags, _is_vhdl_package, 1) ;
#endif
    RESTORE_BIT_FLAG(bit_flags, _is_root_module, 1) ;

    // VIPER #7379: Save/Restore bit flags and other info for optional compiler directives:
    // Restore bitflags in reverse order of saving
    bit_flags = save_restore.RestoreInteger() ;
    // Restore bit flags used during elaboration in reverse order of saving
    RESTORE_BIT_FLAG(bit_flags, _delay_mode_zero, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _delay_mode_unit, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _delay_mode_path, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _delay_mode_distributed, 1) ;
    _default_decay_time = save_restore.RestoreString() ;
    _default_trireg_strength = save_restore.RestoreString() ;

    // VIPER 3362: After restoring of bit flags set the global bit flag i.e. veri_file::_analysis_mode
    veri_file::SetAnalysisMode(_analysis_dialect) ;

    // Restore module id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    if (_id) _id->SetModule(this) ; // Set backpointer over here
    // Restore port scope (VIPER #5954)
    _port_scope = RestoreScopePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of package import (VeriImportDecl*)
    _package_import_decls = RestoreArray(save_restore) ;
    // Restore array of parameter VeriDataDecl(VeriModuleItem)*
    _parameter_connects = RestoreArray(save_restore) ;
    // Restore array of VeriExpression*
    _port_connects = RestoreArray(save_restore) ;
    // Restore module's module items array
    _module_items = RestoreArray(save_restore) ;
    // Restore array of all parameters of this module in ORDER
    _parameters = RestoreArray(save_restore) ;
    // Restore array of all ports of this module in ORDER
    _ports = RestoreArray(save_restore) ;
    // Restore array of instantiations forced on this module by external bind directives
    _bind_instances = RestoreArray(save_restore) ;
    // Restore integer for time that this module was created
    _timestamp = save_restore.RestoreLong() ;
    // Restore string for setting of `timescale at module parsing time
    _timescale = save_restore.RestoreString() ;
    // Restore library
    _parent_library = VeriLibrary::RestoreLibPtr(save_restore) ;
}

/* ---------------------------- */

void
VeriPrimitive::Save(SaveRestore &save_restore) const
{
    VeriModule::Save(save_restore) ;
}

VeriPrimitive::VeriPrimitive(SaveRestore &save_restore)
  : VeriModule(save_restore)
{
    unsigned i ;
    VeriModuleItem *mod_item ;
    FOREACH_ARRAY_ITEM(_module_items, i, mod_item) {
        if (mod_item && mod_item->IsTable()) mod_item->SetTheModule(this) ; // Set module backpointer for VeriTable
    }
}

void
VeriConfiguration::Save(SaveRestore &save_restore) const
{
    // Save the base class elements first:
    VeriModule::Save(save_restore) ;

    // Save the Array of VeriName * top 'design' modules:
    SaveArray(save_restore, _top_modules) ;

    // Save the Array of VeriConfigRule * rules of this configuration:
    SaveArray(save_restore, _config_rules) ;
}

VeriConfiguration::VeriConfiguration(SaveRestore &save_restore)
  : VeriModule(save_restore),
    _top_modules(0),
    _config_rules(0)
{
    // Restore the Array of VeriName * top 'design' modules:
    _top_modules = RestoreArray(save_restore) ;

    // Resore the Array of VeriConfigRule * rules of this configuration:
    _config_rules = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriInterface::Save(SaveRestore &save_restore) const
{
    VeriModule::Save(save_restore) ;
}

VeriInterface::VeriInterface(SaveRestore &save_restore)
  : VeriModule(save_restore)
{
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        // VIPER #4286: Update the modport ports module item back pointers.
        // Do this only after full interface is restored, otherwise if modport
        // is defined before the actual item definition we will not be able to
        // update the modport ports since the actuals are not restored then.
        if (item) item->UpdateModportPorts() ;
    }
}

/* ---------------------------- */

void
VeriProgram::Save(SaveRestore &save_restore) const
{
    VeriModule::Save(save_restore) ;
}

VeriProgram::VeriProgram(SaveRestore &save_restore)
  : VeriModule(save_restore)
{
}

/* ---------------------------- */

// VIPER #5710: vcs-compatibility: always_checker)
// Save-restore routines for class VeriChecker:
void
VeriChecker::Save(SaveRestore &save_restore) const
{
    VeriModule::Save(save_restore) ;
}

VeriChecker::VeriChecker(SaveRestore &save_restore)
  : VeriModule(save_restore)
{
}

/* ---------------------------- */

void
VeriPackage::Save(SaveRestore &save_restore) const
{
    VeriModule::Save(save_restore) ;
}

VeriPackage::VeriPackage(SaveRestore &save_restore)
  : VeriModule(save_restore)
{
}

//--------------------------------------------------------------
// #included from VeriModuleItem.h
//--------------------------------------------------------------

void
VeriModuleItem::Save(SaveRestore &save_restore) const
{
    if (IsTickProtectedNode() && !save_restore.HasProtectionObject()) Warning("binary dump database must be encrypted to secure `protected RTL") ;

    VeriTreeNode::Save(save_restore) ;
    // Save attribute map :
    Map *attributes = GetAttributes() ;
    if (!attributes || !attributes->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0) ;
    } else {
        // Dump map size
        save_restore.SaveInteger(attributes->Size()) ;

        // Map is char* -> VeriExpression* mapping the value will be dumped as NULL
        VeriExpression *expr ;
        MapIter mi ;
        char *str ;
        FOREACH_MAP_ITEM(attributes, mi, &str, &expr) {
            save_restore.SaveString(str) ;
            SaveNodePtr(save_restore, expr) ;
        }
    }

    // Save comment array
    if (RuntimeFlags::GetVar("veri_preserve_comments")) {
        SaveArray(save_restore, GetComments()) ; // always enabled for saving (since 6/2012). Cost one byte per expression.
    }

    // Save unsigned 32 bit flags, used to flag info on this ModuleItem
    save_restore.SaveInteger(_qualifiers) ;
}

VeriModuleItem::VeriModuleItem(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _qualifiers(0)
{
    // Restore attribute map :
    unsigned map_size = save_restore.RestoreInteger() ;
    if (map_size) {
        Map *attributes = new Map(STRING_HASH, map_size) ;
        // Map is char* -> VeriExpression* mapping but the value portion will be forced to NULL.
        VeriExpression *expr ;
        char *str ;
        for(unsigned i = 0; i < map_size; i++) {
            str = save_restore.RestoreString() ;
            expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
            (void) attributes->Insert(str, expr) ;
        }
        SetAttributes(attributes) ;
    }
    // Restore parse-tree comments if stream contains them.
    // Determine if the stream was saved with VERILOG_PRESERVE_COMMENTS enabled.  Bit #3 of user
    // flags contains this information.
    if (save_restore.GetUserFlags() & PRESERVE_COMMENTS_ENABLED) {
        if (RuntimeFlags::GetVar("veri_preserve_comments")) {
            // Restore comment array
            Array *comment_arr = RestoreArray(save_restore) ;
            if (comment_arr) AddComments(comment_arr, 0) ;
        } else {
            // If PRESERVE_COMMENTS is not enabled, deallocate what was restored.
            VeriCommentNode *node ;
            unsigned i ;
            Array *array = RestoreArray(save_restore) ;
            FOREACH_ARRAY_ITEM(array, i, node) {
                // We need to unregister pointer before deleting
                save_restore.UnRegister(node) ;
                delete node ;
            }
            delete array ;
        }
    }
    // Restore unsigned 32 bit flags, used to flag info on this ModuleItem
    _qualifiers = save_restore.RestoreInteger() ;

    if (VeriModuleItem::GetQualifier(VERI_TICK_PROTECTED)) {
        // This is protected RTL. Modify the _linefile to carry only filename
        // and 0 as the line number. This is a special linefile to denote the
        // protected RTL region
        const char *file_name = LineFile::GetFileName(Linefile()) ;
        if (file_name) SetLinefile(LineFile::EncodeLineFile(file_name, 0)) ;
    }
}

/* ---------------------------- */

void
VeriDataDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save data type
    SaveNodePtr(save_restore, _data_type) ;
    // Save ids
    SaveArray(save_restore, &_ids) ;
    // Save the unsigned decl_type stores 'direction' token for IO decls
    save_restore.SaveInteger(_decl_type) ;

    SaveNodePtr(save_restore, _resolution_function) ;
}

VeriDataDecl::VeriDataDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _data_type(0),
    _ids(2),
    _decl_type(0)
    , _resolution_function(0)
{
    // Restore data type (VERI_REG/WIRE and some more complex data types in SV)
    // plus 'packed' dimensions, and 'signing
    _data_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
    // Restore ids
    Array *ids = RestoreArray(save_restore) ;
    _ids.Append(ids) ;
    delete ids ;
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;
        id->SetModuleItem(this) ; // Set backpointer
        // Set the data type backpointer for the id
        id->SetDataTypeForSave(_data_type) ;
    }

    // Restore the unsigned decl_type stores 'direction' token for IO decls
    _decl_type = save_restore.RestoreInteger() ;

    if (save_restore.GetVersion() >= 0x0ab0002a) _resolution_function = (VeriName *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriNetDecl::Save(SaveRestore &save_restore) const
{
    VeriDataDecl::Save(save_restore) ;
    // Save strength
    SaveNodePtr(save_restore, _strength) ;
    // Save array of delays
    SaveArray(save_restore, _delay) ;
}

VeriNetDecl::VeriNetDecl(SaveRestore &save_restore)
  : VeriDataDecl(save_restore),
    _strength(0),
    _delay(0)
{
    // Restore strength
    _strength = (VeriStrength *)RestoreNodePtr(save_restore) ;
    // Restore delay array
    _delay = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriFunctionDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save the bit flags
    unsigned bit_flags = 0 ; // Initialize it to '0'
    // Accumulate the values of the bit flags:
    SAVE_BIT_FLAG(bit_flags, _automatic_type, 10) ; // Save automatic type bit flag
    SAVE_BIT_FLAG(bit_flags, _can_be_const_func, 1) ; // Save the constant function property
    SAVE_BIT_FLAG(bit_flags, _is_const_called, 1) ; // Save the constant called property
    SAVE_BIT_FLAG(bit_flags, _verilog_method, 1) ; // Save the constant called property
    // Save the bit flags as a single integer:
    save_restore.SaveInteger(bit_flags) ;
    // Save data type
    SaveNodePtr(save_restore, _data_type) ;
    // Save function id
    SaveNodePtr(save_restore, _func) ;
    // Save function name
    SaveNodePtr(save_restore, _name) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of arguments declared
    SaveArray(save_restore, _ansi_io_list) ;
    // Save array of declarations
    SaveArray(save_restore, _decls) ;
    // Save array of stmts
    SaveArray(save_restore, _stmts) ;
    // Save array of ports
    SaveArray(save_restore, _ports) ;
}

VeriFunctionDecl::VeriFunctionDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _func(0),
    _name(0),
    _automatic_type(0),
    _can_be_const_func(1),
    _is_const_called(0),
    _verilog_method(1),
    _data_type(0),
    _ansi_io_list(0),
    _decls(0),
    _stmts(0),
    _scope(0),
    _ports(0)
{
    // Restore the bit flags:
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _verilog_method, 1) ; // Restore the constant called property
    RESTORE_BIT_FLAG(bit_flags, _is_const_called, 1) ; // Restore the constant called property
    RESTORE_BIT_FLAG(bit_flags, _can_be_const_func, 1) ; // Restore the constant function property
    RESTORE_BIT_FLAG(bit_flags, _automatic_type, 10) ; // Restore the automatic type bit flag
    // Restore data type
    _data_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
    // Restore function id
    _func = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // VIPER #6061: Clear the data type, it is going to be set from this decl itself:
    // Note that the previous data type may have come from an 'extern' decl.
    if (_func && _func->GetModuleItem() && _data_type) _func->ClearDataType() ;
    // Restore function name
    _name = (VeriName*)RestoreNodePtr(save_restore) ;
    if (_func) _func->SetModuleItem(this) ; // Set backpointer over here
    // Set the data type (if there) back pointer :
    if (_func && _data_type) _func->SetDataType(_data_type) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of arguments declared
    _ansi_io_list = RestoreArray(save_restore) ;
    // Restore array of declarations
    _decls = RestoreArray(save_restore) ;
    // Restore array of stmts
    _stmts = RestoreArray(save_restore) ;
    // Restore array of ports
    _ports = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriTaskDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save task id
    SaveNodePtr(save_restore, _task) ;
    // Save task name
    SaveNodePtr(save_restore, _name) ;
    // Save the unsigned int flag automatic type
    save_restore.SaveInteger(_automatic_type) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of task arguments
    SaveArray(save_restore, _ansi_io_list) ;
    // Save array of decls
    SaveArray(save_restore, _decls) ;
    // Save array of stmts
    SaveArray(save_restore, _stmts) ;
    // Save array of ports
    SaveArray(save_restore, _ports) ;
}

VeriTaskDecl::VeriTaskDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _task(0),
    _name(0),
    _automatic_type(0),
    _contains_delay_or_event_control(0),
    _ansi_io_list(0),
    _decls(0),
    _stmts(0),
    _scope(0),
    _ports(0)
{
    // Restore task id
    _task = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    _name = (VeriName*)RestoreNodePtr(save_restore) ;
    if (_task) _task->SetModuleItem(this) ; // Set backpointer over here
    // Restore the unsigned int flag automatic type
    _automatic_type = save_restore.RestoreInteger() ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of task arguments
    _ansi_io_list = RestoreArray(save_restore) ;
    // Restore array of decls
    _decls = RestoreArray(save_restore) ;
    // Restore array of stmts
    _stmts = RestoreArray(save_restore) ;
    // Restore array of ports
    _ports = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriDefParam::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save array of assignments to an (external) parameter, from a local expression
    SaveArray(save_restore, _defparam_assigns) ;
}

VeriDefParam::VeriDefParam(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _defparam_assigns(0)
{
    // Restore array of assignments to an (external) parameter, from a local expression
    _defparam_assigns = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriContinuousAssign::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save strength
    SaveNodePtr(save_restore, _strength) ;
    // Save array of delay expressions
    SaveArray(save_restore, _delay) ;
    // Save array of net assigns
    SaveArray(save_restore, _net_assigns) ;
}

VeriContinuousAssign::VeriContinuousAssign(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _strength(0),
    _delay(0),
    _net_assigns(0)
{
    // Restore strength
    _strength = (VeriStrength *)RestoreNodePtr(save_restore) ;
    // Restore array of delay expressions
    _delay = RestoreArray(save_restore) ;
    // Restore array of net assigns
    _net_assigns = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriGateInstantiation::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save the unsigned gate-type. As VERI token. VERI_AND, VERI_PULLUP etc
    save_restore.SaveInteger(_inst_type) ;
    // Save strength
    SaveNodePtr(save_restore, _strength) ;
    // Save array of delay expressions
    SaveArray(save_restore, _delay) ;
    // Save array of VeriInstId*.
    SaveArray(save_restore, _insts) ;
}

VeriGateInstantiation::VeriGateInstantiation(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _inst_type(0),
    _strength(0),
    _delay(0),
    _insts(0)
{
    // Restore the unsigned gate-type. As VERI token. VERI_AND, VERI_PULLUP etc
    _inst_type = save_restore.RestoreInteger() ;
    // Restore strength
    _strength = (VeriStrength *)RestoreNodePtr(save_restore) ;
    // Restore array of delay expressions
    _delay = RestoreArray(save_restore) ;
    // Restore array of VeriInstId*.
    _insts = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriModuleInstantiation::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save module name
    SaveNodePtr(save_restore, _module_name) ;
    // Save strength
    SaveNodePtr(save_restore, _strength) ;
    // Save parameter values
    SaveArray(save_restore, _param_values) ;
    // Save insts
    SaveArray(save_restore, &_insts) ;
    // Save library or library pointer
    VeriLibrary::SaveLibPtr(save_restore, _library) ;
}

VeriModuleInstantiation::VeriModuleInstantiation(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _module_name(0),
    _strength(0),
    _param_values(0),
    _insts(1),
    _library(0)
{
    // Restore module name
    _module_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore strength
    _strength = (VeriStrength *)RestoreNodePtr(save_restore) ;
    // Restore parameter values
    _param_values = RestoreArray(save_restore) ;
    // Restore insts
    Array *insts = RestoreArray(save_restore) ;
    _insts.Append(insts) ;
    delete insts ;

    unsigned i ;
    VeriInstId *id ;
    // Set the back pointers for each of the inst_ids to the current module instantiation.
    FOREACH_ARRAY_ITEM(&_insts, i, id) {
        if (!id) continue ;
        id->SetModuleInstance(this) ;
    }
    // Restore library
    _library = VeriLibrary::RestoreLibPtr(save_restore) ;
}

/* ---------------------------- */

void
VeriSpecifyBlock::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of specify items
    SaveArray(save_restore, _specify_items) ;
}

VeriSpecifyBlock::VeriSpecifyBlock(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _specify_items(0),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Save array of specify items
    _specify_items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriPathDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save the unsigned flag which store 1 if IFNONE path or 0 otherwise.
    // Save this 1 bit flag as a char to save space.
    save_restore.SaveChar(_ifnone) ;
    // Save condition expression
    SaveNodePtr(save_restore, _cond) ;
    // Save the path
    SaveNodePtr(save_restore, _path) ;
    // Save array of delay expressions
    SaveArray(save_restore, _delay) ;
}

VeriPathDecl::VeriPathDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _ifnone(0),
    _cond(0),
    _path(0),
    _delay(0)
{
    // Restore the unsigned flag which store 1 if IFNONE path or 0 otherwise.
    _ifnone = save_restore.RestoreChar() ;
    // Restore condition expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the path
    _path = (VeriPath *)RestoreNodePtr(save_restore) ;
    // Restore array of delay expressions
    _delay = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriSystemTimingCheck::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save task name string
    save_restore.SaveString(_task_name) ;
    // Save array of system timing arguments
    SaveArray(save_restore, _args) ;
    // Save the unsigned function type
    save_restore.SaveInteger(_function_type) ;
}

VeriSystemTimingCheck::VeriSystemTimingCheck(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _task_name(0),
    _args(0),
    _function_type(0)
{
    // Restore task name string
    _task_name = save_restore.RestoreString() ;
    // Restore array of system timing arguments
    _args = RestoreArray(save_restore) ;
    // Restore the unsigned function type
    _function_type = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriInitialConstruct::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save statement
    SaveNodePtr(save_restore, _stmt) ;
}

VeriInitialConstruct::VeriInitialConstruct(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _stmt(0)
{
    // Restore statement
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriAlwaysConstruct::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save statement
    SaveNodePtr(save_restore, _stmt) ;
}

VeriAlwaysConstruct::VeriAlwaysConstruct(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _stmt(0)
{
    // Restore statement
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriGenerateConstruct::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save array of VeriModuleItems
    SaveArray(save_restore, _items) ;
}

VeriGenerateConstruct::VeriGenerateConstruct(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _items(0)
{
    // Restore array of VeriModuleItems
    _items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriGenerateConditional::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save if scope
    SaveScopePtr(save_restore, _then_scope) ;
    // Save if expression
    SaveNodePtr(save_restore, _if_expr) ;
    // Save then item
    SaveNodePtr(save_restore, _then_item) ;
    // Save else scope
    SaveScopePtr(save_restore, _else_scope) ;
    // Save else item
    SaveNodePtr(save_restore, _else_item) ;
}

VeriGenerateConditional::VeriGenerateConditional(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _if_expr(0),
    _then_item(0),
    _else_item(0),
    _then_scope(0),
    _else_scope(0)
{
    // Restore if scope
    _then_scope = RestoreScopePtr(save_restore) ;
    if (_then_scope) _then_scope->SetGenerateScope() ;
    // Restore if expression
    _if_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore then item
    _then_item = (VeriModuleItem *)RestoreNodePtr(save_restore) ;
    // Restore else scope
    _else_scope = RestoreScopePtr(save_restore) ;
    if (_else_scope) _else_scope->SetGenerateScope() ;
    // Restore else item
    _else_item = (VeriModuleItem *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriGenerateCase::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save the unsigned case style
    save_restore.SaveInteger(_case_style) ;
    // Save cond expression
    SaveNodePtr(save_restore, _cond) ;
    // Save array of case items
    SaveArray(save_restore, _case_items) ;
}

VeriGenerateCase::VeriGenerateCase(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _case_style(0),
    _cond(0),
    _case_items(0)
{
    // Restore the unsigned case style
    _case_style = save_restore.RestoreInteger() ;
    // Restore cond expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of case items
    _case_items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriGenerateFor::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save scope created for in-line genvar declaration
    SaveScopePtr(save_restore, _loop_scope) ;
    // Save initial item
    SaveNodePtr(save_restore, _initial) ;
    // Save cond expression
    SaveNodePtr(save_restore, _cond) ;
    // Save genvar assignment
    SaveNodePtr(save_restore, _rep) ;
    // Save block id
    SaveNodePtr(save_restore, _block_id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save generate items
    SaveArray(save_restore, _items) ;
}

VeriGenerateFor::VeriGenerateFor(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _initial(0),
    _cond(0),
    _rep(0),
    _block_id(0),
    _items(0),
    _scope(0),
    _loop_scope(0)
{
    // Restore scope created for in-line genvar declaration
    _loop_scope = RestoreScopePtr(save_restore) ;
    if (_loop_scope) _loop_scope->SetGenerateScope() ;
    // Restore initial item
    _initial = (VeriModuleItem *)RestoreNodePtr(save_restore) ;
    // Restore cond expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore genvar assignment
    _rep = (VeriModuleItem *)RestoreNodePtr(save_restore) ;
    // Restore block id
    _block_id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    if (_scope) {
        _scope->SetGenerateScope() ;
        VeriIdDef *loop_label = _scope->FindLocal(" gen_loop_label") ;
        if (loop_label) loop_label->SetModuleItem(this) ;
    }
    // Set the module item back-pointer of the label to this.
    if (_block_id) _block_id->SetModuleItem(this) ;

    // Restore generate items
    _items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriGenerateBlock::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save block id
    SaveNodePtr(save_restore, _block_id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of generate items
    SaveArray(save_restore, _items) ;
}

VeriGenerateBlock::VeriGenerateBlock(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _block_id(0),
    _items(0),
    _scope(0)
    ,_is_elaborated(0) // No need to save/restore this elaboration specific flag
{
    // Restore id
    _block_id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Set the module item back-pointer of the label to this. Can we do this before scope copy ?
    if (_block_id) _block_id->SetModuleItem(this) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    if (_scope) _scope->SetGenerateScope() ;
    // Restore array of generate items
    _items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriTable::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save array of table entries
    SaveStringArray(save_restore, _table_entries) ;

    // Save the bit flags
    unsigned bit_flags = 0 ; // Initialize it to '0'
    // Accumulate the values of the bit flags:
    SAVE_BIT_FLAG(bit_flags, _is_comb, 1) ; // Save the combinational flag
    SAVE_BIT_FLAG(bit_flags, _is_seq, 1) ; // Save the sequential flag
    // Save the bit flags as a single integer:
    save_restore.SaveInteger(bit_flags) ;
}
VeriTable::VeriTable(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _table_entries(0),
    _the_module(0),
    _is_comb(0),
    _is_seq(0)
{
    // Restore array of table entries
    _table_entries = RestoreStringArray(save_restore) ;

    // Restore the bit flags:
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_seq, 1) ; // Restore the sequential flag
    RESTORE_BIT_FLAG(bit_flags, _is_comb, 1) ; // Restore the combinational flag
}
/* ---------------------------- */

void
VeriPulseControl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save pulse control style
    save_restore.SaveInteger(_pulse_control_style) ;
    // Save array of expressions
    SaveArray(save_restore, _path_list) ;
}
VeriPulseControl::VeriPulseControl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _pulse_control_style(0),
    _path_list(0)
{
    // Restore the pulse control style
    _pulse_control_style = save_restore.RestoreInteger() ;

    // Restore array of expressions
    _path_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriClass::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save identifier of this class (the simple name)
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of parameters. Array of VeriParamDecl (a VeriModuleItem)
    SaveArray(save_restore, _parameter_connects) ;
    // Save base class name
    SaveNodePtr(save_restore, _base_class_name) ;
    // Save interface class refs
    SaveArray(save_restore, _interface_classes) ;
    // Save array of class items
    SaveArray(save_restore, _items) ;
    // Save Array of VeriIdDef*. List of function arguments in ORDER
    SaveArray(save_restore, _parameters) ;
    // Save _has_static_members flag
    save_restore.SaveInteger(_has_static_members) ;
    save_restore.SaveInteger(_is_resolved) ;
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    SaveScopePtr(save_restore, _owning_scope) ;
#endif
}

VeriClass::VeriClass(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _new_id(0),
    _parameter_connects(0),
    _base_class_name(0),
    _interface_classes(0),
    _items(0),
    _scope(0),
    _parameters(0),
    _has_static_members(0),
    _is_processing(0)   // No need to save/restore
    , _is_instantiated(0) // No need to save/restore
    , _contain_copied_version(0)
    , _is_resolved(0)
    , _orig_class_name(0)
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    , _owning_scope(0)
#endif
{
    // Restore identifier of this class (the simple name)
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Set module item back-pointer
    if (_id) _id->SetModuleItem(this) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of parameters. Array of VeriParamDecl (a VeriModuleItem)
    _parameter_connects = RestoreArray(save_restore) ;
    // Restore base class name
    _base_class_name = (VeriName *)RestoreNodePtr(save_restore) ;
    if (save_restore.GetVersion() >= 0x0ab00032) {
        // Restore interface class refs
        _interface_classes = RestoreArray(save_restore) ;
    }
    // Restore array of class items
    _items = RestoreArray(save_restore) ;
    // Restore Array of VeriIdDef*. List of function arguments in ORDER
    _parameters = RestoreArray(save_restore) ;
    // Restore _has_static_members
    _has_static_members = save_restore.RestoreInteger() ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (!item || !(item->IsFunctionDecl() || item->IsTaskDecl())) continue ;
        // VIPER #4030 : Functions declared inside a class are automatic if not
        // explicitly specified as static
        // VIPER #4853 : Tasks declared inside a class are automatic if not
        // explicitly specified as static
        VeriIdDef *func_task_id = item->GetId() ;
        if (func_task_id && !func_task_id->IsAutomatic() && !item->GetQualifier(VERI_STATIC)) {
            func_task_id->SetIsAutomatic() ;
        }
        if (func_task_id && func_task_id->GetFunctionType()==VERI_METHOD_NEW && !_new_id) {
            // VCS Compatibility issue (test51) : Ignore new function of inner class
            VeriName *method_name = item->GetSubprogramName() ;
            if (method_name && method_name->IsScopeName()) { // It is like c::new
                VeriName *prefix = method_name->GetPrefix() ;
                VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
                if (prefix_id && prefix_id->IsClass() && (prefix_id != _id)) {
                    continue ; // This is out_of_block declaration of new
                }
            }
            _new_id = func_task_id ;
        }
    }
    _is_resolved = save_restore.RestoreInteger() ;
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    _owning_scope = RestoreScopePtr(save_restore) ;
#endif
}

/* ---------------------------- */

void
VeriPropertyDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save property decl id
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of arguments declared.  Array of VeriAnsiPortDecl*.
    SaveArray(save_restore, _ansi_io_list) ;
    // Save array of decls.
    SaveArray(save_restore, _decls) ;
    // Save property spec.
    SaveNodePtr(save_restore, _spec) ;
    // Save array of ports. Array of VeriIdDef in ORDER.
    SaveArray(save_restore, _ports) ;
}

VeriPropertyDecl::VeriPropertyDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _ansi_io_list(0),
    _decls(0),
    _spec(0),
    _scope(0),
    _ports(0),
    _is_processing(0),
    _feedback_net(0),
    _formal_to_actual(0)
{
    // Restore property decl id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    if (_id) _id->SetModuleItem(this) ; // Set backpointer over here
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of arguments declared.  Array of VeriAnsiPortDecl*.
    _ansi_io_list = RestoreArray(save_restore) ;
    // Restore array of decls.
    _decls = RestoreArray(save_restore) ;
    // Restore property spec.
    _spec = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of ports. Array of VeriIdDef in ORDER.
    _ports = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriSequenceDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save sequence_decl_id
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of VeriAnsiPortDecl*. Function arguments declared.
    SaveArray(save_restore, _ansi_io_list) ;
    // Save array of decls
    SaveArray(save_restore, _decls) ;
    // Save sequence spec expression
    SaveNodePtr(save_restore, _spec) ;
    // Save  array of VeriIdDef. The formals of this construct in ORDER.
    SaveArray(save_restore, _ports) ;
}

VeriSequenceDecl::VeriSequenceDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _ansi_io_list(0),
    _decls(0),
    _spec(0),
    _scope(0),
    _ports(0),
    _is_processing(0)
{
    // Restore sequence_decl_id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    if (_id) _id->SetModuleItem(this) ; // Set backpointer over here
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of VeriAnsiPortDecl*. Function arguments declared.
    _ansi_io_list = RestoreArray(save_restore) ;
    // Restore array of decls
    _decls = RestoreArray(save_restore) ;
    // Restore sequence spec expression
    _spec = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore  array of VeriIdDef. The formals of this construct in ORDER.
    _ports = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriModport::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save array of VeriModportDecl.
    SaveArray(save_restore, _modport_decls) ;
}

VeriModport::VeriModport(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _modport_decls(0)
{
    // Restore array of VeriModportDecl.
    _modport_decls = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriModportDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save modport id
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of VeriModuleItem.
    SaveArray(save_restore, _modport_port_decls) ;
    // Save array of VeriIdDef*. List of modport_ports ids in ORDER.
    SaveArray(save_restore, _ports) ;
}

VeriModportDecl::VeriModportDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _modport_port_decls(0),
    _scope(0),
    _ports(0)
{
    // Restore modport id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of VeriModuleItem.
    _modport_port_decls = RestoreArray(save_restore) ;
    // Restore array of VeriIdDef*. List of modport_ports ids in ORDER.
    _ports = RestoreArray(save_restore) ;

    // VIPER #6313: Set the module item pointer on the 'id' back to this:
    if (_id) _id->SetModuleItem(this) ;
}

/* ---------------------------- */

void
VeriClockingDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save clocking id
    SaveNodePtr(save_restore, _id) ;
    // Save clocking event
    SaveNodePtr(save_restore, _clocking_event) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save clocking items array
    SaveArray(save_restore, _clocking_items) ;
}

VeriClockingDecl::VeriClockingDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _clocking_event(0),
    _clocking_items(0),
    _scope(0)
{
    // Restore clocking id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore clocking event
    _clocking_event = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore clocking items array
    _clocking_items = RestoreArray(save_restore) ;

    if (_id) _id->SetModuleItem(this) ;

    // VIPER #4264: Set data type back pointer in signal declarated within clocking
    // declaration from its definition in upper scope. See related VIPER #4286 for
    // modport and setting its data type and module item back pointers properly.
    // We had to do that after parsing the full interface decl. But similar strategy
    // is not needed here because clocking items must be declared before use.
    VeriScope *upper_scope = (_scope) ? _scope->Upper() : 0 ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_clocking_items, i, item) {
        if (!item) continue ;
        Array *ids = item->GetIds() ;
        unsigned j ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id || id->GetDataType()) continue ;
            VeriIdDef *clocking_sig_id = (upper_scope) ? upper_scope->Find(id->Name()) : 0 ;
            VeriExpression *init_val = id->GetInitialValue() ;
            if (clocking_sig_id && !init_val) id->SetDataType(clocking_sig_id->GetDataType()) ;
            if (init_val) {
                // Set data type from initial value specified identifier/expression
                // FIXME : This will not work if init_val is an expression other than name reference
                VeriIdDef *r_id = init_val->GetId() ;
                if (r_id) id->SetDataType(r_id->GetDataType()) ;
            }
        }
    }
}

/* ---------------------------- */

void
VeriConstraintDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save constraint decl id
    SaveNodePtr(save_restore, _id) ;
    // Save array of constraint blocks
    SaveArray(save_restore, _constraint_blocks) ;
    // Save constraint name
    SaveNodePtr(save_restore,_name) ;
    // Save scope
    SaveScopePtr(save_restore,_scope) ;
}

VeriConstraintDecl::VeriConstraintDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _constraint_blocks(0),
    _scope(0),
    _name(0)
{
    // Restore constraint decl id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore array of constraint blocks
    _constraint_blocks = RestoreArray(save_restore) ;
    // Restore constraint name
    _name = (VeriName*)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriBindDirective::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save string specifying target_scope
    save_restore.SaveString(_target_mod_name) ;
    // Save array of target instances
    SaveArray(save_restore, _target_inst_list) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save the instantiation of a module, program or interface
    SaveNodePtr(save_restore, _instantiation) ;
}

VeriBindDirective::VeriBindDirective(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _target_mod_name(0),
    _target_inst_list(0),
    _instantiation(0),
    _scope(0)
    ,_is_static_elaborated(0)  // VIPER #7968:
{
    // Restore string specifying target_scope
    _target_mod_name = save_restore.RestoreString() ;
    // Restore array of target instances
    _target_inst_list = RestoreArray(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore the instantiation of a module, program or interface
    _instantiation = (VeriModuleItem *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriOperatorBinding::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save id
    SaveNodePtr(save_restore, _oper_id) ;
    // Save data type
    SaveNodePtr(save_restore, _data_type) ;
    // Save function name
    SaveNodePtr(save_restore, _func_name) ;
    // Save array of formal data types
    SaveArray(save_restore, _formals) ;
}

VeriOperatorBinding::VeriOperatorBinding(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _oper_id(0),
    _data_type(0),
    _func_name(0),
    _formals(0)
{
    // Restore id
    _oper_id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    if (_oper_id) _oper_id->SetModuleItem(this) ; // Set module item back-pointer
    // Restore data type
    _data_type = (VeriDataType *)RestoreNodePtr(save_restore) ;
    // Restore function name
    _func_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore array of formal data types
    _formals = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriNetAlias::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save left expression
    SaveNodePtr(save_restore, _left) ;
    // Save array of members
    SaveArray(save_restore, _members) ;
}

VeriNetAlias::VeriNetAlias(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _left(0),
    _members(0)
{
    // Restore left expression
    _left = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of members
    _members = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriTimeUnit::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save the unsigned VERI_TIMEUNIT or VERI_TIMEPRECISION token
    save_restore.SaveInteger(_type) ;
    // Save time literal (VeriTimeLiteral*)
    SaveNodePtr(save_restore, _literal) ;

    // VIPER #8304 : Save time precision
    SaveNodePtr(save_restore, _time_precision) ;
}

VeriTimeUnit::VeriTimeUnit(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _type(0),
    _literal(0),
    _time_precision(0)
{
    // Restore the unsigned VERI_TIMEUNIT or VERI_TIMEPRECISION token
    _type = save_restore.RestoreInteger() ;
    // RestoreSave time literal (VeriTimeLiteral*)
    _literal = (VeriTimeLiteral *)RestoreNodePtr(save_restore) ;

    // VIPER #8304 : Restore time precision
    if (save_restore.GetVersion() >= 0x0ab0002f) {
        _time_precision = (VeriTimeLiteral *)RestoreNodePtr(save_restore) ;
    }
}

/* ---------------------------- */

void
VeriClockingSigDecl::Save(SaveRestore &save_restore) const
{
    VeriDataDecl::Save(save_restore) ;
    // Save clocking direction (direction and skew)
    SaveNodePtr(save_restore, _clk_dir) ;
    // Save clocking direction of output
    SaveNodePtr(save_restore, _o_clk_dir) ;
}

VeriClockingSigDecl::VeriClockingSigDecl(SaveRestore &save_restore)
  : VeriDataDecl(save_restore),
    _clk_dir(0),
    _o_clk_dir(0)
{
    // Restore clocking direction (direction and skew)
    _clk_dir = (VeriClockingDirection *)RestoreNodePtr(save_restore) ;
    // Restore clocking direction of output
    _o_clk_dir = (VeriClockingDirection *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriCovergroup::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save id
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save port list (array of VeriExpression*)
    SaveArray(save_restore, _port_connects) ;
    // Save coverage event
    SaveNodePtr(save_restore, _event) ;
    // Save sample function
    SaveNodePtr(save_restore, _sample_func) ;
    // Save list of coverage spec or options (array of VeriModuleItem*)
    SaveArray(save_restore, _items) ;
    // Save array of VeriIdDef*. List of function arguments in ORDER.
    SaveArray(save_restore, _ports) ;
}

VeriCovergroup::VeriCovergroup(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _port_connects(0),
    _event(0),
    _sample_func(0),
    _items(0),
    _scope(0),
    _ports(0)
{
    // Restore id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Set back-pointer from id to this covergroup :
    if (_id) _id->SetModuleItem(this) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore port list (array of VeriExpression*)
    _port_connects = RestoreArray(save_restore) ;
    // Restore coverage event
    _event = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore sample function
    _sample_func = (VeriModuleItem*)RestoreNodePtr(save_restore) ;
    // Restore list of coverage spec or options (array of VeriModuleItem*)
    _items = RestoreArray(save_restore) ;
    // Restore array of VeriIdDef*. List of function arguments in ORDER.
    _ports = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriCoverageOption::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save option name
    SaveNodePtr(save_restore, _option) ;
    // Save option value
    SaveNodePtr(save_restore, _value) ;
}

VeriCoverageOption::VeriCoverageOption(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _option(0),
    _value(0)
{
    // Restore option name
    _option = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore option value
    _value = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriCoverageSpec::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save coverpoint id
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save the unsigned type of coverage VERI_COVERPOINT or VERI_CROSS
    save_restore.SaveInteger(_type) ;
    // Save cover point expression list (array of VeriExpression*)
    SaveArray(save_restore, _point_list) ;
    // Save iff expression
    SaveNodePtr(save_restore, _iff_expr) ;
    // Save array of bins or options (array of VeriModuleItem*)
    SaveArray(save_restore, _bins) ;
}

VeriCoverageSpec::VeriCoverageSpec(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _type(0),
    _id (0),
    _point_list(0),
    _iff_expr(0),
    _bins(0),
    _scope(0)
{
    // Restore coverpoint id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore the unsigned type of coverage VERI_COVERPOINT or VERI_CROSS
    _type = save_restore.RestoreInteger() ;
    // Restore cover point expression list (array of VeriExpression*)
    _point_list = RestoreArray(save_restore) ;
    // Restore iff expression
    _iff_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of bins or options (array of VeriModuleItem*)
    _bins = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriBinDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save the unsigned wildcard
    save_restore.SaveInteger(_wildcard) ;
    // Save bins keyword VERI_BINS, VERI_ILLEGAL_BINS or VERI_IGNOREBINS
    save_restore.SaveInteger(_bins_keyword) ;
    // Save bins id
    SaveNodePtr(save_restore, _id) ;
}

VeriBinDecl::VeriBinDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _wildcard(0),
    _bins_keyword(0),
    _id(0)
{
    // Restore the unsigned wildcard
    _wildcard = save_restore.RestoreInteger() ;
    // Restore bins keyword VERI_BINS, VERI_ILLEGAL_BINS or VERI_IGNOREBINS
    _bins_keyword = save_restore.RestoreInteger() ;
    // Restore bins id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriImportDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save array of package import items (array of VeriScopeName*)
    SaveArray(save_restore, _items) ;
}

VeriImportDecl::VeriImportDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _items(0)
{
    // Restore array of package import items (array of VeriScopeName*)
    _items = RestoreArray(save_restore) ;
}
void
VeriLetDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save let_decl_id
    SaveNodePtr(save_restore, _id) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of VeriAnsiPortDecl*. Let arguments declared.
    SaveArray(save_restore, _ansi_io_list) ;
    // Save expression
    SaveNodePtr(save_restore, _expr) ;
    // Save  array of VeriIdDef. The formals of this construct in ORDER.
    SaveArray(save_restore, _ports) ;
}
VeriLetDecl::VeriLetDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _id(0),
    _ansi_io_list(0),
    _expr(0),
    _scope(0),
    _ports(0)
{
    // Restore let_decl_id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    if (_id) _id->SetModuleItem(this) ; // Set backpointer over here
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of VeriAnsiPortDecl*. Let arguments declared.
    _ansi_io_list = RestoreArray(save_restore) ;
    // Restore expression
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore  array of VeriIdDef. The formals of this construct in ORDER.
    _ports = RestoreArray(save_restore) ;
}
void VeriDefaultDisableIff::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save expression
    SaveNodePtr(save_restore, _expr_or_dist) ;
}
VeriDefaultDisableIff::VeriDefaultDisableIff(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _expr_or_dist(0)
{
    // Restore expression
    _expr_or_dist = (VeriExpression *)RestoreNodePtr(save_restore) ;
}
void VeriExportDecl::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save array of package import items (array of VeriScopeName*)
    SaveArray(save_restore, _items) ;
}

VeriExportDecl::VeriExportDecl(SaveRestore &save_restore)
  : VeriModuleItem(save_restore),
    _items(0)
{
    // Restore array of package import items (array of VeriScopeName*)
    _items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriCaseOperator::Save(SaveRestore &save_restore) const
{
    // Save base items
    VeriExpression::Save(save_restore) ;

    // Save condition expression
    SaveNodePtr(save_restore, _cond) ;
    // Save array of case items
    SaveArray(save_restore, _case_items) ;
}

VeriCaseOperator::VeriCaseOperator(SaveRestore &save_restore)
  : VeriExpression(save_restore),
    _cond(0),
    _case_items(0)
{
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of case items
    _case_items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriCaseOperatorItem::Save(SaveRestore &save_restore) const
{
    // Save base items
    VeriTreeNode::Save(save_restore) ;

    // Save conditions array
    SaveArray(save_restore, _conditions) ;
    // Save property expression
    SaveNodePtr(save_restore, _property_expr) ;
}

VeriCaseOperatorItem::VeriCaseOperatorItem(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _conditions(0),
    _property_expr(0)
{
    // Restore conditions
    _conditions = RestoreArray(save_restore) ;
    // Restore property expression
    _property_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDPIFunctionDecl::Save(SaveRestore &save_restore) const
{
    // Save base items
    VeriFunctionDecl::Save(save_restore) ;

    // Save DPI import/export type
    save_restore.SaveInteger(_type) ;
    // Save DPI spec string
    save_restore.SaveString(_spec_string) ;
    // Save DPI function property context/pure
    save_restore.SaveInteger(_property) ;
    // Save DPI C indentifier name
    save_restore.SaveString(_c_identifier) ;
}

VeriDPIFunctionDecl::VeriDPIFunctionDecl(SaveRestore &save_restore)
  : VeriFunctionDecl(save_restore),
    _type(0),
    _spec_string(0),
    _property(0),
    _c_identifier(0)
{
    // Restore DPI import/export type
    _type = save_restore.RestoreInteger() ;
    // Restore DPI spec string
    _spec_string = save_restore.RestoreString() ;
    // Restore DPI function property context/pure
    _property = save_restore.RestoreInteger() ;
    // Restore DPI C indentifier name
    _c_identifier = save_restore.RestoreString() ;
}

/* ---------------------------- */

void
VeriDPITaskDecl::Save(SaveRestore &save_restore) const
{
    // Save base items
    VeriTaskDecl::Save(save_restore) ;

    // Save DPI import/export type
    save_restore.SaveInteger(_type) ;
    // Save DPI spec string
    save_restore.SaveString(_spec_string) ;
    // Save DPI function property context/pure
    save_restore.SaveInteger(_property) ;
    // Save DPI C indentifier name
    save_restore.SaveString(_c_identifier) ;
}

VeriDPITaskDecl::VeriDPITaskDecl(SaveRestore &save_restore)
  : VeriTaskDecl(save_restore),
    _type(0),
    _spec_string(0),
    _property(0),
    _c_identifier(0)
{
    // Restore DPI import/export type
    _type = save_restore.RestoreInteger() ;
    // Restore DPI spec string
    _spec_string = save_restore.RestoreString() ;
    // Restore DPI function property context/pure
    _property = save_restore.RestoreInteger() ;
    // Restore DPI C indentifier name
    _c_identifier = save_restore.RestoreString() ;
}

//--------------------------------------------------------------
// #included from VeriStatement.h
//--------------------------------------------------------------

void
VeriStatement::Save(SaveRestore &save_restore) const
{
    VeriModuleItem::Save(save_restore) ;
    // Save opening label
    SaveNodePtr(save_restore, _opening_label) ;
}

VeriStatement::VeriStatement(SaveRestore &save_restore)
  : VeriModuleItem(save_restore)
    , _opening_label(0)
{
    // Restore label
    _opening_label = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Set moduleitem label back-pointer
    if (_opening_label) _opening_label->SetModuleItem(this) ;
}

/* ---------------------------- */

void
VeriNullStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VeriStatement::Save(save_restore)  ;

    // Dump data members appropriately

    // No data members to dump
}

VeriNullStatement::VeriNullStatement(SaveRestore &save_restore)
  : VeriStatement(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VeriBlockingAssign::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore)  ;
    // Save LHS expression
    SaveNodePtr(save_restore, _lval) ;
    // Save unsigned int oper type
    save_restore.SaveInteger(_oper_type) ;
    // Save delay or event control
    SaveNodePtr(save_restore, _control) ;
    // Save the value expression
    SaveNodePtr(save_restore, _val) ;
}

VeriBlockingAssign::VeriBlockingAssign(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _lval(0),
    _oper_type(0),
    _control(0),
    _val(0)
{
    // Restore LHS expression
    _lval = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore unsigned int oper type
    _oper_type = save_restore.RestoreInteger() ;
    // Restore delay or event control
    _control = (VeriDelayOrEventControl *)RestoreNodePtr(save_restore) ;
    // Restore the value expression
    _val = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriNonBlockingAssign::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save LHS expression
    SaveNodePtr(save_restore, _lval) ;
    // Save delay or event control
    SaveNodePtr(save_restore, _control) ;
    // Save the value expression
    SaveNodePtr(save_restore, _val) ;
}

VeriNonBlockingAssign::VeriNonBlockingAssign(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _lval(0),
    _control(0),
    _val(0)
{
    // Restore LHS expression
    _lval = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore delay or event control
    _control = (VeriDelayOrEventControl *)RestoreNodePtr(save_restore) ;
    // Restore the value expression
    _val = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriGenVarAssign::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save name
    save_restore.SaveString(_name) ;
    // Save genvar id
    SaveNodePtr(save_restore, _id) ;
    // Save the value expression
    SaveNodePtr(save_restore, _value) ;
    // Save unsigned int oper type
    save_restore.SaveInteger(_oper_type) ;
}

VeriGenVarAssign::VeriGenVarAssign(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _name(0),
    _id(0),
    _value(0),
    _oper_type(0)
{
    // Restore name string
    _name = save_restore.RestoreString() ;
    // Restore genvar id
    _id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore the value expression
    _value = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore unsigned int oper type
    _oper_type = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriAssign::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save assign
    SaveNodePtr(save_restore, _assign) ;
}

VeriAssign::VeriAssign(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _assign(0)
{
    // Restore assign
    _assign = (VeriNetRegAssign *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDeAssign::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save LHS expression
    SaveNodePtr(save_restore, _lval) ;
}

VeriDeAssign::VeriDeAssign(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _lval(0)
{
    // Restore LHS expression
    _lval = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriForce::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save assignment
    SaveNodePtr(save_restore, _assign) ;
}

VeriForce::VeriForce(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _assign(0)
{
    // Restore assign
    _assign = (VeriNetRegAssign *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriRelease::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save LHS expression
    SaveNodePtr(save_restore, _lval) ;
}

VeriRelease::VeriRelease(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _lval(0)
{
    // Restore LHS expression
    _lval = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriTaskEnable::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save task name
    SaveNodePtr(save_restore, _task_name) ;
    // Save array of arguments
    SaveArray(save_restore, _args) ;
}

VeriTaskEnable::VeriTaskEnable(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _task_name(0),
    _args(0),
    _method_type(0)
{
    // Restore task name
    _task_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore array of arguments
    _args = RestoreArray(save_restore) ;
    // Find the token for the suffix (in case this is a 'method' on the prefix).
    // Methods are only allowed in System Verilog mode :
    if (IsSystemVeri() && _task_name) {
        _method_type = _task_name->GetFunctionType() ;
        // VCS Compatibility issue (test69) : Do not set _method_type from GetFuncToken
        // Rely on _task_name->GetFunctionType(). GetFuncToken method type may return
        // token for already resolved task call. For example std package contains
        // a task 'randomize', but we have tokens for 'randomize' too.
        //if (!_method_type) _method_type = VeriExpression::GetFuncToken(_task_name->GetName()) ;
    }
}

/* ---------------------------- */

void
VeriSystemTaskEnable::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save task name string
    save_restore.SaveString(_name) ;
    // Save array of arguments
    SaveArray(save_restore, _args) ;
    // Save the unsigned function type
    save_restore.SaveInteger(_function_type) ;
}

VeriSystemTaskEnable::VeriSystemTaskEnable(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _name(0),
    _args(0),
    _function_type(0)
{
    // Restore task name string
    _name = save_restore.RestoreString() ;
    // Restore array of arguments
    _args = RestoreArray(save_restore) ;
    // Restore the unsigned function type
    _function_type = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriDelayControlStatement::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save delay expr
    SaveNodePtr(save_restore, _delay) ;
    // Save stament
    SaveNodePtr(save_restore, _stmt) ;
    // VIPER #5922: Directly save/restore this bit field (only one for now):
    save_restore.SaveInteger(_is_cycle_delay) ;
}

VeriDelayControlStatement::VeriDelayControlStatement(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _delay(0),
    _stmt(0)
    ,_is_cycle_delay(0)
{
    // Restore delay expr
    _delay = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore stament
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
    // VIPER #5922: Directly save/restore this bit field (only one for now):
    _is_cycle_delay = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriEventControlStatement::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save array of event control expressions
    SaveArray(save_restore, _at) ;
    // Save statement to execute under event control
    SaveNodePtr(save_restore, _stmt) ;
    // save the inferred clock
    // VIPER #6753: Directly save/restore this integer
    save_restore.SaveInteger(_sva_clock_expr_index) ;
}

VeriEventControlStatement::VeriEventControlStatement(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _at(0),
    _stmt(0)
    ,_sva_clock_expr_index(0)
{
    // Restore array of event control expressions
    _at = RestoreArray(save_restore) ;
    // Restore statement to execute under event control
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
    // VIPER #6753: Directly save/restore this integer
    _sva_clock_expr_index = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VeriConditionalStatement::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save if expression
    SaveNodePtr(save_restore, _if_expr) ;
    // Save then statement
    SaveNodePtr(save_restore, _then_stmt) ;
    // Save else statement
    SaveNodePtr(save_restore, _else_stmt) ;
}

VeriConditionalStatement::VeriConditionalStatement(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _if_expr(0),
    _then_stmt(0),
    _else_stmt(0)
{
    // Restore if expression
    _if_expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore then statement
    _then_stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
    // Restore else statement
    _else_stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriCaseStatement::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save the unsigned token to determine VERI_CASEX, VERI_CASEZ, VERI_CASE
    save_restore.SaveInteger(_case_style) ;
    // Save conditional expression
    SaveNodePtr(save_restore, _cond) ;
    // Save array of case items
    SaveArray(save_restore, _case_items) ;
    unsigned bit_flags = 0 ;
    // Save the unsigned decl_type to represent case-inside and pattern matching case statement
    bit_flags = _type ;
    SAVE_BIT_FLAG(bit_flags, _is_full_case, 1) ;
    SAVE_BIT_FLAG(bit_flags, _is_parallel_case, 1) ;
    save_restore.SaveInteger(bit_flags) ;
}

VeriCaseStatement::VeriCaseStatement(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _case_style(0),
    _cond(0),
    _case_items(0)
    ,_type(0)
    , _is_full_case(0)
    , _is_parallel_case(0)
{
    // Restore the unsigned token to determine VERI_CASEX, VERI_CASEZ, VERI_CASE
    _case_style = save_restore.RestoreInteger() ;
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of case items
    _case_items = RestoreArray(save_restore) ;
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_parallel_case, 1) ;
    RESTORE_BIT_FLAG(bit_flags, _is_full_case, 1) ;
    // Restore the unsigned decl_type to represent case-inside and pattern matching case statement
    _type = bit_flags ;
}

/* ---------------------------- */

void
VeriLoop::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save stmt
    SaveNodePtr(save_restore, _stmt) ;
}

VeriLoop::VeriLoop(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _stmt(0),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore stmt
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
    if (_scope) {
        // Update the loop(break) and continue labels :
        VeriIdDef *loop_label = _scope->FindLocal(" loop_label") ;
        if (loop_label) loop_label->SetModuleItem(this) ;
        loop_label = _scope->FindLocal(" continue_label") ;
        if (loop_label) loop_label->SetModuleItem(this) ;
    }
}

/* ---------------------------- */

void
VeriForever::Save(SaveRestore &save_restore) const
{
    VeriLoop::Save(save_restore) ;
}

VeriForever::VeriForever(SaveRestore &save_restore)
  : VeriLoop(save_restore)
{
}

/* ---------------------------- */

void
VeriRepeat::Save(SaveRestore &save_restore) const
{
    VeriLoop::Save(save_restore) ;
    // Save conditional expression
    SaveNodePtr(save_restore, _cond) ;
}

VeriRepeat::VeriRepeat(SaveRestore &save_restore)
  : VeriLoop(save_restore),
    _cond(0)
{
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriWhile::Save(SaveRestore &save_restore) const
{
    VeriLoop::Save(save_restore) ;
    // Save conditional expression
    SaveNodePtr(save_restore, _cond) ;
}

VeriWhile::VeriWhile(SaveRestore &save_restore)
  : VeriLoop(save_restore),
    _cond(0)
{
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriFor::Save(SaveRestore &save_restore) const
{
    VeriLoop::Save(save_restore) ;
    // Save array of module items
    SaveArray(save_restore, _initials) ;
    // Save conditional expression
    SaveNodePtr(save_restore, _cond) ;
    // Save array of VeriStatement
    SaveArray(save_restore, _reps) ;
}

VeriFor::VeriFor(SaveRestore &save_restore)
  : VeriLoop(save_restore),
    _initials(0),
    _cond(0),
    _reps(0)
{
    // Restore array of module items
    _initials = RestoreArray(save_restore) ;
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore array of VeriStatement
    _reps = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriWait::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save conditional expression
    SaveNodePtr(save_restore, _cond) ;
    // Save statement
    SaveNodePtr(save_restore, _stmt) ;
}

VeriWait::VeriWait(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _cond(0),
    _stmt(0)
{
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore statement
    _stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDisable::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save reference to a task or block identifier
    SaveNodePtr(save_restore, _task_block_name) ;
}

VeriDisable::VeriDisable(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _task_block_name(0)
{
    // Save reference to a task or block identifier
    _task_block_name = (VeriName *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriEventTrigger::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save event trigger operator
    save_restore.SaveInteger(_trigger_oper) ;
    // Save the delay or event control information
    SaveNodePtr(save_restore, _control) ;
    // Save reference to a event identifier
    SaveNodePtr(save_restore, _event_name) ;
}

VeriEventTrigger::VeriEventTrigger(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _trigger_oper(0),
    _control(0),
    _event_name(0)
{
    // Restore event trigger operator
    _trigger_oper = save_restore.RestoreInteger() ;
    // Restore the delay or event control information
    _control = (VeriDelayOrEventControl *)RestoreNodePtr(save_restore) ;
    // Restore reference to a event identifier
    _event_name = (VeriName *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriSeqBlock::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save label
    SaveNodePtr(save_restore, _label) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save stmts
    SaveArray(save_restore, _stmts) ;
    // Save decl items
    SaveArray(save_restore, _decl_items) ;
}

VeriSeqBlock::VeriSeqBlock(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _label(0),
    _decl_items(0),
    _stmts(0),
    _scope(0)
{
    // Restore label
    _label = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Set the module item back-pointer of the label to this.
    if (_label) _label->SetModuleItem(this) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore stmts
    _stmts = RestoreArray(save_restore) ;
    // Restore decl items
    _decl_items = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriParBlock::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save label
    SaveNodePtr(save_restore, _label) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save stmts
    SaveArray(save_restore, _stmts) ;
    // Save decl items
    SaveArray(save_restore, _decl_items) ;
    // Save the unsigned closing token
    save_restore.SaveInteger(_closing_token) ;
}

VeriParBlock::VeriParBlock(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _label(0),
    _decl_items(0),
    _stmts(0),
    _scope(0),
    _closing_token(0)
{
    // Restore label
    _label = (VeriIdDef *)RestoreNodePtr(save_restore) ;
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    if (_scope) _scope->SetParBlockScope() ; // VIPER #4830
    // Set the module item back-pointer of the label to this.
    if (_label) _label->SetModuleItem(this) ;
    // Restore stmts
    _stmts = RestoreArray(save_restore) ;
    // Restore decl items
    _decl_items = RestoreArray(save_restore) ;
    // Restore the unsigned closing token
    _closing_token = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

// VIPER #5710(vcs_compatibility: always_checker(for checker instantiation inside procedural code)):
// Saving members of class VeriSequentialInstantiation:
void
VeriSequentialInstantiation::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save module name
    SaveNodePtr(save_restore, _module_name) ;
    // Save insts
    SaveArray(save_restore, &_insts) ;
    // Save library or library pointer
    VeriLibrary::SaveLibPtr(save_restore, _library) ;
}

// Restoring members of class VeriSequentialInstantiation:
VeriSequentialInstantiation::VeriSequentialInstantiation(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _module_name(0),
    _insts(1),
    _library(0)
{
    // Restore module name
    _module_name = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore insts
    Array *insts = RestoreArray(save_restore) ;
    _insts.Append(insts) ;
    delete insts ;

    // Restore library
    _library = VeriLibrary::RestoreLibPtr(save_restore) ;
}

/* ---------------------------- */
void
VeriAssertion::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save the unsigned assert cover property
    save_restore.SaveInteger(_assert_cover_property) ;
    // Save the unsigned final or property
    save_restore.SaveInteger(_final_property) ;
    // Save property (expression) which is tested
    SaveNodePtr(save_restore, _property_spec) ;
    // Save the 'then' statement
    SaveNodePtr(save_restore, _then_stmt) ;
    // Save the 'else' statement
    SaveNodePtr(save_restore, _else_stmt) ;
    // Save clock expression
    SaveNodePtr(save_restore, _sva_clock_expression) ;

    unsigned bit_flags = _is_immediate ;
    SAVE_BIT_FLAG(bit_flags, _is_deferred, 1) ;
    save_restore.SaveInteger(bit_flags) ; // Save the bit flags
}

VeriAssertion::VeriAssertion(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _assert_cover_property(0),
    _is_immediate(0),
    _final_property(0),
    _is_deferred(0),
    _property_spec(0),
    _then_stmt(0),
    _else_stmt(0),
    _sva_clock_expression(0)
{
    // Restore the unsigned assert cover property
    _assert_cover_property = save_restore.RestoreInteger() ;
    // Restore the unsigned final or property
    _final_property = save_restore.RestoreInteger() ;
    // Restore property (expression) which is tested
    _property_spec = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore the 'then' statement
    _then_stmt= (VeriStatement *)RestoreNodePtr(save_restore) ;
    // Restore the 'else' statement
    _else_stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
    // Restore clock expression
    _sva_clock_expression = (VeriExpression *)RestoreNodePtr(save_restore) ;

    // Restore the bit flags in an order reverse to saving.
    unsigned bit_flags = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_deferred, 1) ;
    _is_immediate = bit_flags ;
}

/* ---------------------------- */

void
VeriJumpStatement::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save the unsigned jump token
    save_restore.SaveInteger(_jump_token) ;
    // Save for 'return' statement : the returned expression
    SaveNodePtr(save_restore, _expr) ;
    // Save backpointer to id (loop label or subprogran) to which this statement jumps
    SaveNodePtr(save_restore, _jump_id) ;
}

VeriJumpStatement::VeriJumpStatement(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _jump_token(0),
    _expr(0),
    _jump_id(0)
{
    // Restore the unsigned jump token
    _jump_token = save_restore.RestoreInteger() ;
    // Restore for 'return' statement : the returned expression
    _expr = (VeriExpression *)RestoreNodePtr(save_restore) ;
    // Restore backpointer to id (loop label or subprogran) to which this statement jumps
    _jump_id = (VeriIdDef *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriDoWhile::Save(SaveRestore &save_restore) const
{
    VeriLoop::Save(save_restore) ;
    // Save conditional expression
    SaveNodePtr(save_restore, _cond) ;
}

VeriDoWhile::VeriDoWhile(SaveRestore &save_restore)
  : VeriLoop(save_restore),
    _cond(0)
{
    // Restore conditional expression
    _cond = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriForeach::Save(SaveRestore &save_restore) const
{
    VeriLoop::Save(save_restore) ;
    // Save array name
    SaveNodePtr(save_restore, _array) ;
    // Save array of loop indices
    SaveArray(save_restore, _loop_indexes) ;
    // Save array of implicit loop variable declarations
    SaveArray(save_restore, _implicit_decl_list) ;
}

VeriForeach::VeriForeach(SaveRestore &save_restore)
  : VeriLoop(save_restore),
    _array(0),
    _loop_indexes(0),
    _implicit_decl_list(0) // VIPER #4649/4703
{
    // Restore array name
    _array = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore array of loop indices
    _loop_indexes = RestoreArray(save_restore) ;
    // Restore array of implicit loop variable declarations
    _implicit_decl_list = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriWaitOrder::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save list of events to be triggered in order
    SaveArray(save_restore, _events) ;
    // Save then statement
    SaveNodePtr(save_restore, _then_stmt) ;
    // Save else statement
    SaveNodePtr(save_restore, _else_stmt) ;
}

VeriWaitOrder::VeriWaitOrder(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _events(0),
    _then_stmt(0),
    _else_stmt(0)
{
    // Restore list of events to be triggered in order
    _events = RestoreArray(save_restore) ;
    // Restore then statement
    _then_stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
    // Restore else statement
    _else_stmt = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriWithStmt::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save left statement
    SaveNodePtr(save_restore, _left) ;
}

VeriWithStmt::VeriWithStmt(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _left(0),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore left statement
    _left = (VeriStatement *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriArrayMethodCall::Save(SaveRestore &save_restore) const
{
    VeriWithStmt::Save(save_restore) ;
    // Save right expression
    SaveNodePtr(save_restore, _right) ;
}

VeriArrayMethodCall::VeriArrayMethodCall(SaveRestore &save_restore)
  : VeriWithStmt(save_restore),
    _right(0)
{
    // Restore right expression
    _right = (VeriExpression *)RestoreNodePtr(save_restore) ;
}

/* ---------------------------- */

void
VeriInlineConstraintStmt::Save(SaveRestore &save_restore) const
{
    VeriWithStmt::Save(save_restore) ;
    // Save array of constraint blocks
    SaveArray(save_restore, _constraint_block) ;
}

VeriInlineConstraintStmt::VeriInlineConstraintStmt(SaveRestore &save_restore)
  : VeriWithStmt(save_restore),
    _constraint_block(0)
{
    // Restore array of constraint blocks
    _constraint_block = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriRandsequence::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save start production
    SaveNodePtr(save_restore, _start_prod) ;
    // Save array of productions
    SaveArray(save_restore, _productions) ;
}

VeriRandsequence::VeriRandsequence(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _start_prod(0),
    _productions(0),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore start production
    _start_prod = (VeriName *)RestoreNodePtr(save_restore) ;
    // Restore array of productions
    _productions = RestoreArray(save_restore) ;
}

/* ---------------------------- */

void
VeriCodeBlock::Save(SaveRestore &save_restore) const
{
    VeriStatement::Save(save_restore) ;
    // Save scope
    SaveScopePtr(save_restore, _scope) ;
    // Save array of declratations
    SaveArray(save_restore, _decls) ;
    // Save array of stmts
    SaveArray(save_restore, _stmts) ;
}

VeriCodeBlock::VeriCodeBlock(SaveRestore &save_restore)
  : VeriStatement(save_restore),
    _decls(0),
    _stmts(0),
    _scope(0)
{
    // Restore scope
    _scope = RestoreScopePtr(save_restore) ;
    // Restore array of declratations
    _decls = RestoreArray(save_restore) ;
    // Restore array of stmts
    _stmts = RestoreArray(save_restore) ;
}

//--------------------------------------------------------------
// #included from VeriTreeNode.h
//--------------------------------------------------------------

void
VeriTreeNode::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    // None - VeriNode has no data to be saved.

    // Dump data members appropriately.

    // The _linefile member, regardless of the VERILOG_USE_STARTING_LINEFILE_INFO
    // switch setting, should be directly dumped.  Don't call Linefile() here.
    save_restore.SaveLinefile(_linefile);
}

VeriTreeNode::VeriTreeNode(SaveRestore &save_restore)
  : // Use the default VeriNode() constructor here, because there is nothing to restore.
    VeriNode(),
    _linefile(0)
{
    // Registering of all TreeNode objects occur here, AND ONLY here!!!
    save_restore.RegisterPointer(this);

    // Restore (and register) the linefile after registering myself, because when saving,
    // we registered myself (from SaveNodePtr) before saving (registering) the linefile:
    _linefile = save_restore.RestoreLinefile();
}

/* ---------------------------- */

void
VeriCommentNode::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VeriTreeNode::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveString(_str) ;
}

VeriCommentNode::VeriCommentNode(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _str(0)
{
    // Restore data members appropriately
    _str = save_restore.RestoreString() ;
}

//--------------------------------------------------------------
// #included from VeriScope.h
//--------------------------------------------------------------

// The following NULL_PTR constant is only used by VeriScope
static const unsigned NULL_PTR = 0xdeadbeef;  // NULL pointer value.

void
VeriScope::SaveUsingSet(SaveRestore &save_restore) const
{
    Set *set = _using ;

    if (!set || !set->Size()) {
        // Dump that set does not exist
        save_restore.SaveInteger(0) ;
    } else {
        save_restore.SaveInteger(set->Size()) ;

        SetIter si ;
        VeriScope *scope ;
        FOREACH_SET_ITEM(set, si, &scope) {
            VeriTreeNode::SaveScopePtr(save_restore, scope) ;
        }
    }
}

void
VeriScope::RestoreUsingSet(SaveRestore &save_restore)
{
    // Restore _using set size
    unsigned set_size = save_restore.RestoreInteger() ;

    if (set_size) {
        // Restore elements (VeriScope* in this case)
        VeriScope *scope ;
        for(unsigned i = 0 ; i < set_size ; i++) {
            scope = VeriTreeNode::RestoreScopePtr(save_restore) ;
            Using(scope) ; // Sets the _using and _used_by fields
        }
    }
}

/* ---------------------------- */

static const unsigned int FLAG_INIT_VALUE    = 1 ;
static const unsigned int FLAG_UNPACKED_DIMS = 2 ;
static const unsigned int FLAG_PORT_CONNECTS = 4 ;

void
VeriScope::Save(SaveRestore &save_restore) const
{
    // This object is registered in VeriTreeNode::SaveScopePtr, right before this
    // function gets called.

    // Register external dependencies
    if (!RegisterDependencies(save_restore)) {
        VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Error("Error from RegisterDependencies") ;
        return ; // An error occurred
    }

    // No base class's member data to save

    // Save the upper scope
    VeriTreeNode::SaveScopePtr(save_restore, _upper) ;
    // Save owner of scope
    VeriTreeNode::SaveNodePtr(save_restore, _owner) ;
    // Save the _using set
    SaveUsingSet(save_restore) ;
    // No need to save _used_by set. Restore it by calling Using()
    // in VeriScope::RestoreUsingSet
    // Save the present-level scope table

    // No need to save the _children field. The VeriScope::AddChildScope
    // Just save the number of children, so that at restore time we can create a Set of exact size:
    save_restore.SaveInteger((_children) ? _children->Size() : 0) ;

    // routine populates that field.
    VeriTreeNode::SaveIdDefMap(save_restore, _this_scope) ;

    // Re-Initialize the values of VeriIdDefs and restore port connects of VeriInstId now.
    // It may so happen that we get a initialization "a = b ;" at a point where "a" has
    // been inserted in id_map_table but "b" is yet to be inserted. In that case "a" is
    // initialized with the VeriIdDef pointing to the old location.
    MapIter iter ;
    char *name ;
    VeriIdDef *val ;
    FOREACH_MAP_ITEM(_this_scope, iter, &name, &val) {
        // VIPER #5261 : prototype identifiers do  not have initial value or dimension
        // So ignore those identifiers
        if (!val || val->IsPrototypeId()) continue ;
        // This flag will have its 1st, 2nd, 3rd and 4th bits set if it val contains initial
        // value, unpacked dimensions, port connects and for AMS value_range respectively
        char bit_flags = 0 ;
        if (val->GetInitialValue()) bit_flags |= FLAG_INIT_VALUE ;
        if (val->GetDimensions())   bit_flags |= FLAG_UNPACKED_DIMS ;
        if (val->GetPortConnects()) bit_flags |= FLAG_PORT_CONNECTS ;

        // Save the character flag
        save_restore.SaveChar(bit_flags) ;

        // The initial value, unpacked dimensions, port connects and for AMS value_range
        // were not saved before. Save them now.
        if (bit_flags & FLAG_INIT_VALUE) VeriTreeNode::SaveNodePtr(save_restore, val->GetInitialValue()) ;
        if (bit_flags & FLAG_UNPACKED_DIMS) VeriTreeNode::SaveNodePtr(save_restore, val->GetDimensions()) ;
        if (bit_flags & FLAG_PORT_CONNECTS) VeriTreeNode::SaveArray(save_restore, val->GetPortConnects()) ;
    }

    // Save the _imported_scopes map
    if (!_imported_scopes || !_imported_scopes->Size()) {
        save_restore.SaveInteger(0) ; // Save 0 as size
    } else {
        save_restore.SaveInteger(_imported_scopes->Size()) ;

        char *str ;
        VeriScope *imp_scope ;
        MapIter mi ;
        Set *names ;
        FOREACH_MAP_ITEM(_imported_scopes, mi, &imp_scope, &names) {
            VeriTreeNode::SaveScopePtr(save_restore, imp_scope) ;
            if (!names || !names->Size()) {
                save_restore.SaveInteger(0) ; // Save 0 as size
            } else {
                save_restore.SaveInteger(names->Size()) ;
                SetIter si ;
                FOREACH_SET_ITEM(names, si, &str) {
                    save_restore.SaveString(str) ;
                }
            }
        }
    }
    // Save the flag to indicate transparent scope
    unsigned bit_flags = _empty_scope ;
    SAVE_BIT_FLAG(bit_flags, _is_std_pkg_used, 1) ;
    //save_restore.SaveInteger(_empty_scope) ;
    save_restore.SaveInteger(bit_flags) ;
    // VIPER #5740: Do not save/restore extended and base class scopes,
    // save/restore their owner ids instead:
    // Save the extended scope
    //VeriTreeNode::SaveScopePtr(save_restore, _extended_scope) ;
    VeriIdDef *extended_id = (_extended_scope) ? _extended_scope->GetOwner() : 0 ;
    VeriTreeNode::SaveNodePtr(save_restore, extended_id) ;
    // Save the base class scope
    //VeriTreeNode::SaveScopePtr(save_restore, _base_class_scope) ;
    VeriIdDef *base_class = (_base_class_scope) ? _base_class_scope->GetOwner() : 0 ;
    VeriTreeNode::SaveNodePtr(save_restore, base_class) ;
    // VIPER #7232 : Save the _exported_scopes map
    if (!_exported_scopes || !_exported_scopes->Size()) {
        save_restore.SaveInteger(0) ; // Save 0 as size
    } else {
        save_restore.SaveInteger(_exported_scopes->Size()) ;

        char *str ;
        VeriScope *exp_scope ;
        MapIter mi ;
        Map *names ;
        linefile_type lf ;
        FOREACH_MAP_ITEM(_exported_scopes, mi, &exp_scope, &names) {
            VeriTreeNode::SaveScopePtr(save_restore, exp_scope) ;
            if (!names || !names->Size()) {
                save_restore.SaveInteger(0) ; // Save 0 as size
            } else {
                save_restore.SaveInteger(names->Size()) ;
                MapIter mii ;
                FOREACH_MAP_ITEM(names, mii, &str, &lf) {
                    save_restore.SaveString(str) ;
                    save_restore.SaveLinefile(lf);
                }
            }
        }
    }
}

VeriScope::VeriScope(SaveRestore &save_restore)
  : _upper(0),
    _owner(0),
    _this_scope(0),
    _using(0),
    _used_by(0),
    _children(0),
    _imported_scopes(0),
    _empty_scope(0),
    _generate_scope(0),
    _struct_union_scope(0)
    , _seq_block_scope(0)
    , _loop_scope(0)
    , _is_std_pkg_used(0)
    , _par_block_scope(0)
    , _exported_subprog_scope(0)
    , _extended_scope(0)
    , _base_class_scope(0)
    , _struct_constraints(0) // No need to save/restore (VIPER #6434)
    , _exported_scopes(0)

{
    save_restore.RegisterPointer(this) ;

    // Register external dependencies
    if (!RegisterDependencies(save_restore)) {
        save_restore.FileNotOk() ; // Disable SaveRestore object
        return ; // If an restore error has occurred, exit early
    }

    // Create a container for this scope which contains all the ids declared
    // under the current scope
    // _this_scope = new Map(STRING_HASH) ;

    // Restore the upper scope
    _upper = VeriTreeNode::RestoreScopePtr(save_restore) ;

    // Restore id which owns the scope
    _owner = (VeriIdDef *)VeriTreeNode::RestoreNodePtr(save_restore) ;
    if(_owner) _owner->SetLocalScope(this) ; // Set backpointer
    // Restore _using and _used_by sets
    RestoreUsingSet(save_restore) ;
    // Restore number of children and create a Set of exact size:
    unsigned num_children = save_restore.RestoreInteger() ;
    if (num_children) _children = new Set(POINTER_HASH, num_children) ;
    // Restore the present-level scope map table
    _this_scope = VeriTreeNode::RestoreIdDefMap(save_restore) ;
    // Re-Initialize the values of VeriIdDefs and Copy port connects of VeriInstId now.
    // It may so happen that we get a initialization "a = b ;" at a point where "a" has
    // been inserted in id_map_table but "b" is yet to be inserted. In that case "a" is
    // initialized with the VeriIdDef pointing to the old location.
    MapIter iter ;
    char *name ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(_this_scope, iter, &name, &id) {
        // VIPER #5261 : prototype identifiers do  not have initial value or dimension
        // So ignore those identifiers
        //if (!id || id->IsPrototypeId()) continue ;
        if (!id) continue ;

        // VIPER #8020 : Set owning scope back pointer for prototype ids
#ifdef VERILOG_ID_SCOPE_BACKPOINTER
        // Set the scope backpointer of the identifier to this :
        id->SetOwningScope(this) ;
#endif
        if (id->IsPrototypeId()) continue ;

        // Get character flag
        char bit_flags = save_restore.RestoreChar() ;

        // Set the initial value of 'id'
        if (bit_flags & FLAG_INIT_VALUE) {
            VeriExpression *init_val = (VeriExpression *)VeriTreeNode::RestoreNodePtr(save_restore) ;
            id->SetInitialValue(init_val) ; // Set the initial value
        }
        // Set the dimensions of 'id'
        if (bit_flags & FLAG_UNPACKED_DIMS) {
            VeriRange *dim_range = (VeriRange *)VeriTreeNode::RestoreNodePtr(save_restore) ;
            id->SetMemoryRange(dim_range) ;
        }
        // Set the port connects array for VeriInstId
        if (bit_flags & FLAG_PORT_CONNECTS) {
            Array *restored_arr = VeriTreeNode::RestoreArray(save_restore) ;
            // Get the port connects array
            Array *port_conn_arr = id->GetPortConnects() ;
            // Assign the restored array to the port connects array
            if (port_conn_arr) port_conn_arr->Append(restored_arr) ;
            delete restored_arr ;
        }
    }
    // Add this scope into the upper scope as child:
    // This sets the _children field
    if (_upper) _upper->AddChildScope(this) ;
    // Restore the _imported_scopes map
    unsigned size = save_restore.RestoreInteger() ;
    if (size) {
        _imported_scopes = new Map(POINTER_HASH) ;
        unsigned i, j ;
        for(i=0; i<size; i++) {
            VeriScope *package_scope = VeriTreeNode::RestoreScopePtr(save_restore) ;
            unsigned set_size = save_restore.RestoreInteger() ;
            Set *names = 0 ;
            if (set_size) {
                names = new Set(STRING_HASH, set_size) ;
                for(j = 0; j < set_size; j++) {
                    char *str = save_restore.RestoreString() ;
                    id = (package_scope) ? package_scope->FindLocal(str) : 0 ;
                    if (!id) id = (package_scope) ? package_scope->FindExported(str) : 0 ;
                    if (id) {
                        (void) names->Insert(id->Name()) ;
                    } else {
                        (void) names->Insert("*") ;
                    }
                    Strings::free(str) ;
                }
            }
            if (_imported_scopes) (void)_imported_scopes->Insert(package_scope, names) ;
        }
    }
    // Restore the flag to indicate transparent scope
    //_empty_scope = save_restore.RestoreInteger() ;
    unsigned bit_flags  = save_restore.RestoreInteger() ;
    RESTORE_BIT_FLAG(bit_flags, _is_std_pkg_used, 1) ;
    _empty_scope = bit_flags ;

    // VIPER #5740: Do not save/restore extended and base class scopes,
    // save/restore their owner ids instead:
    // Restore the extended scope
    //_extended_scope = VeriTreeNode::RestoreScopePtr(save_restore) ;
    VeriIdDef *extended_id = (VeriIdDef *)VeriTreeNode::RestoreNodePtr(save_restore) ;
    _extended_scope = (extended_id) ? extended_id->LocalScope() : 0 ;
    // Restore the base class scope
    //_base_class_scope = VeriTreeNode::RestoreScopePtr(save_restore) ;
    VeriIdDef *base_class = (VeriIdDef *)VeriTreeNode::RestoreNodePtr(save_restore) ;
    _base_class_scope = (base_class) ? base_class->LocalScope() : 0 ;

    // VIPER #7232 : Restore _exported_scopes
    size = save_restore.RestoreInteger() ;
    if (size) {
        _exported_scopes = new Map(POINTER_HASH) ;
        unsigned i, j ;
        for(i=0; i<size; i++) {
            VeriScope *package_scope = VeriTreeNode::RestoreScopePtr(save_restore) ;
            unsigned map_size = save_restore.RestoreInteger() ;
            Map *names = 0 ;
            if (map_size) {
                names = new Map(STRING_HASH, map_size) ;
                for (j=0; j<map_size; j++) {
                    char *str = save_restore.RestoreString() ;
                    linefile_type lf = save_restore.RestoreLinefile();
                    id = (package_scope) ? package_scope->FindLocal(str) : 0 ;
                    if (!id) id = (package_scope) ? package_scope->FindExported(str) : 0 ;
                    if (id) {
                        (void) names->Insert(id->Name(), (void*)lf) ;
                    } else {
                        (void) names->Insert("*", (void*)lf) ;
                    }
                    Strings::free(str) ; // JJ: need to free allocated str before looping...
                }
            }
            if (_exported_scopes) (void)_exported_scopes->Insert(package_scope, names) ;
        }
    }
}

unsigned
VeriScope::RegisterDependencies(SaveRestore &save_restore) const
{
    VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    if (save_restore.IsSaving()) { // --- SAVING ---
        // Dump num of dependencies
        unsigned num_dependencies = (_using) ? _using->Size() : 0 ;
        save_restore.SaveInteger(num_dependencies) ;

        // Dump dependencies
        SetIter si ;
        const VeriScope *scope ;
        // For every external scope on which the current scope depends
        // call RegisterDependencies for each of them
        FOREACH_SET_ITEM(_using, si, &scope) {
            if (!scope->RegisterDependencies(save_restore)) {
                return 0 ;   // A save error occurred!
            }
        }

        // Set scope to current scope
        scope = this ;

        // If the scope is not registered yet then register it
        if (!save_restore.IsRegistered(scope)) {
            save_restore.RegisterPointer(scope) ;

            VeriIdDef *module_id = scope->GetContainingModule() ; // Get the module/interface/package/program identifier that encompasses the current scope.
            VeriModule *module = module_id ? module_id->GetModule() : 0 ;

            if (!module_id || !module) { // If there is no module to be saved then some error occurred and so return
                // ERROR: ...
                tmp_print_node.Error("Registering Dependencies Error: %s", "no module found") ;
                return 0 ;
            }
            if (module_id->IsModule()) { // Returns true for package/module/interface/program
                // Save class id for package :
                if (module_id->IsPackage()) {
                    save_restore.SaveInteger((int)ID_VERIPACKAGE) ;
                } else {
                    // Save class id for module :
                    save_restore.SaveInteger((int)ID_VERIMODULE) ;
                }

                VeriLibrary *mod_lib = module->GetLibrary() ;
                // Save library name
                save_restore.SaveString(mod_lib ? mod_lib->GetName() : 0) ;

                const char *module_id_name = module_id->GetName() ; // Get the name of the design module
                // Save module/package name
                save_restore.SaveString(module_id_name) ;
            } else { // The design module has to be a package or a module
                tmp_print_node.Error("Registering Dependencies Error: %s", "unknown design unit type") ;
                save_restore.FileNotOk() ;
                return 0 ;
            }
            // Get module/package scope
            VeriScope *module_scope = module->GetScope() ;

            // Now dump the design module's signature
            unsigned long signature = (module_scope) ? module_scope->CalculateSaveRestoreSignature(): 0 ;
            save_restore.SaveLong(signature) ;

            // Save two more numbers for signature matching. We may need them when we support
            // partial dependency with root modules (VIPER #7192). Adding now to ease backward compatibility later.
            save_restore.SaveInteger(0) ; // This may be the current number of module items
            save_restore.SaveLong(0) ;    // This may be the partial signature with current number of module items

            // Register pointer but don't dump info.
            if (!save_restore.IsRegistered(module)) save_restore.RegisterPointer(module) ;

            // Register module id but don't dump info.
            if (!save_restore.IsRegistered(module_id)) save_restore.RegisterPointer(module_id) ;

            // Now register each module in scope's use clauses
            if (module_scope && !(module_scope->RegisterIdDefs(save_restore))) {
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return 0 ; // Error occurred during save.
            }
        } else {
            save_restore.SaveInteger(NULL_PTR) ;   // This means the scope is already registered
        }
    } else {   // --- RESTORING ---
        VeriLibrary *library ;
        // VeriIdDef *module_id ;
        VeriModule *module ;

        // Determine number of modules this scope depends on
        unsigned num_dependencies = save_restore.RestoreInteger() ;

        // Register modules this scope depends on
        for(unsigned i = 0 ; i < num_dependencies ; i++) {
            // Need to first recurse through dependencies first
            if (!RegisterDependencies(save_restore)){
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return 0 ;
            }
        }

        // Determine whether it's a module/package
        unsigned ptr_type = save_restore.RestoreInteger() ;
        switch(ptr_type)
        {
            case NULL_PTR:
                // This means that this scope is already registered.
                // module_id = 0 ;
                break ;

            case (unsigned)ID_VERIMODULE:
            case (unsigned)ID_VERIPACKAGE:
                {
                    // Retrieve library name and module name
                    char *lib_name = save_restore.RestoreString() ;
                    char *module_name = save_restore.RestoreString() ;

                    // Get Library.
                    library = veri_file::GetLibrary(lib_name) ;

                    // RD: this code seems very complex, and should be much simpler. Maybe because some libraries are 'special' user-libraries ? Review later.
                    if (!library) {
                        // Attempt first to do a restore of library/module
                        if (!veri_file::Restore(lib_name, module_name)) {
                            // Wasn't able to do a restore
                            tmp_print_node.Error("Registering Dependencies Error: The %s '%s' could not be found during restore", "library", lib_name) ;
                            Strings::free(lib_name) ;
                            Strings::free(module_name) ;
                            save_restore.FileNotOk() ; // Disable SaveRestore object
                            return 0;
                        }

                        // Now get the library
                        library = veri_file::GetLibrary(lib_name);
                        if (!library){
                            tmp_print_node.Error("Registering Dependencies Error: %s", "library not defined") ;
                            Strings::free(lib_name) ;
                            Strings::free(module_name) ;
                            save_restore.FileNotOk() ; // Disable SaveRestore object
                            return 0 ;
                        }
                    }

                    // library must be there now.
                    VERIFIC_ASSERT(library) ;

                    // Get the module/package. Do case-sensitive, since we look for a Verilog module.
                    module = library->GetModule(module_name, 1) ;

                    if (!module) {
                        // Attempt first to do a restore of library/module
                        if (!veri_file::Restore(lib_name, module_name)) {
                            // Wasn't able to do a restore
                            tmp_print_node.Error("Registering Dependencies Error: The %s '%s' could not be found during restore", "library", lib_name) ;
                            Strings::free(lib_name) ;
                            Strings::free(module_name) ;
                            save_restore.FileNotOk() ; // Disable SaveRestore object
                            return 0 ;
                        }
                        module = library->GetModule(module_name, 1) ;
                        if (!module) {
                            // Wasn't able to do a restore
                            tmp_print_node.Error("Registering Dependencies Error: The %s '%s' could not be found during restore", "module", module_name) ;
                            Strings::free(lib_name) ;
                            Strings::free(module_name) ;
                            save_restore.FileNotOk() ; // Disable SaveRestore object
                            return 0 ;
                        }
                    }

                    // Retrieve signature
                    unsigned long orig_sig = save_restore.RestoreLong() ; // Restore signature
                    unsigned long new_sig = (module->GetScope()) ? module->GetScope()->CalculateSaveRestoreSignature() : 0 ;

                    // Restore and ignore two more numbers for signature matching. We may need them when we support
                    // partial dependency with root modules (VIPER #7192). Adding now to ease backward compatibility later.
                    (void) save_restore.RestoreInteger() ; // This may be the current number of module items
                    (void) save_restore.RestoreLong() ;    // This may be the partial signature with current number of module items

                    // Compare restored signature with restore module's signature
                    if (orig_sig != new_sig) {
                        tmp_print_node.Error("%s needs to be re-saved since %s.%s changed", save_restore.FileName(), lib_name, module_name) ;
                        delete module ;
                        Strings::free(lib_name) ;
                        Strings::free(module_name) ;
                        save_restore.FileNotOk() ; // Disable SaveRestore object
                        return 0 ;
                    }

                    // Now register the module scope (it was registered at saving time just before we saved the module) :
                    if (!save_restore.IsRegistered(module->GetScope())) save_restore.RegisterPointer(module->GetScope()) ;

                    // Register module, and its IdDef.
                    if (!save_restore.IsRegistered(module)) save_restore.RegisterPointer(module) ;
                    if (!save_restore.IsRegistered(module->GetId())) save_restore.RegisterPointer(module->GetId()) ;

                    VeriScope *local_scope = module->GetScope() ;
                    if (local_scope && !local_scope->RegisterIdDefs(save_restore)) {
                        Strings::free(lib_name) ;
                        Strings::free(module_name) ;
                        save_restore.FileNotOk() ; // Disable SaveRestore object
                        return 0 ;   // Error occurred during restore.
                    }
                    // Free dynamically allocated memory
                    Strings::free(lib_name) ;
                    Strings::free(module_name) ;
                }
                break ;

            default:
                //VERIFIC_ASSERT(0) ;  // This should never get executed!
                tmp_print_node.Error("Registering Dependencies Error: %s", "unknown pointer type") ;
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return 0 ;
        }
    }
    return 1 ;    // No error occurred
}

unsigned
VeriScope::RegisterIdDefs(SaveRestore &save_restore) const
{
    // Register all IdDefs
    char *name ;
    VeriIdDef *id_def ;
    MapIter mi ;
    FOREACH_MAP_ITEM(_this_scope, mi, &name, &id_def) {
        if (!id_def) continue ;
        // VIPER #4951 : If restored unit is elaborated, it can have elaborated
        // copied class identifiers. We should not consider those here, as
        // original sdb was always created from analyzed parse tree before elaboration
        VeriModuleItem *tree = (id_def->IsClass()) ? id_def->GetModuleItem(): 0 ;
        if (tree && tree->GetOriginalClassName()) continue ;
        if (!save_restore.IsRegistered(id_def)) {
            save_restore.RegisterPointer(id_def) ;
            // If an IdDef has a scope (entity decl, config decl, record type decl, process statement, etc ...),
            // then register IdDefs in its local scope.
            VeriScope *local_scope = id_def->LocalScope() ;
            if (!local_scope) {
                // For struct and unions get the local scope by calling the
                // local scope of the data type
                VeriDataType *data_type = id_def->GetDataType() ;
                // VIPER #5091 : Recurse into data type scope only for structure/union
                // do not check data type scope for interface data type
                local_scope = (data_type && data_type->GetIds()) ? data_type->GetMemberScope() : 0 ;
            }
            // Register the ids under the local scope by recursively calling
            // the RegisterIdDefs function
            if (local_scope && !local_scope->RegisterIdDefs(save_restore)) return 0 ;
        }
    }
    return 1 ;    // No error occurred
}

//--------------------------------------------------------------
// #included from VeriLibrary.h
//--------------------------------------------------------------

// The values for the following constants should be greater than ID_VERI_BEGIN_TABLE (default: 0xc0000000)
// as they mimic class ids. However they are not actually class ids.
static const unsigned FLAG_LIB      = (unsigned)ID_VERI_BEGIN_TABLE + 1 ; // default: 0xc0000001
static const unsigned FLAG_USER_LIB = (unsigned)ID_VERI_BEGIN_TABLE + 2 ; // default: 0xc0000002

// static
// This routine does not save VeriTmpLibrary
void
VeriLibrary::SaveLibPtr(SaveRestore &save_restore, const VeriLibrary *lib)
{
    if (!lib || lib->IsTmpLibrary()) {
        save_restore.SaveInteger(0) ;
        return ;
    }
    // If this instance is not registered, then register it and dump it
    if (!save_restore.IsRegistered(lib)) { // If the library has not yet been registered
        // Register the library
        save_restore.RegisterPointer(lib) ;
        // lib can either be VeriLibrary*, VeriUserLibrary* or VeriTmpLibrary*
        if (lib->IsUserLibrary()) {
            save_restore.SaveInteger(FLAG_USER_LIB) ;
        } else { // If lib is not user lib or tmp lib then it has to be VeriLibrary*
            save_restore.SaveInteger(FLAG_LIB) ;
        }
        // Call the save routine for each of the individual libraries
        lib->Save(save_restore) ;
    } else { // If the library has already been registered then save its reference
        unsigned long id = save_restore.Pointer2Id((const void*)lib) ;
        if (id >= (unsigned)ID_VERI_BEGIN_TABLE) {
            VeriTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
            tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

            tmp_print_node.Error("This parse-tree is too large to be saved") ;
            save_restore.FileNotOk() ; // Disable SaveRestore object
        }
        save_restore.SaveInteger((unsigned)id) ;
    }
}

// static
VeriLibrary *
VeriLibrary::RestoreLibPtr(SaveRestore &save_restore)
{
    VeriLibrary *lib = 0 ;
    // Restore flag to determine which type of lib to restore
    unsigned flag = save_restore.RestoreInteger() ;
    if (!flag) return 0 ; // This is a NULL or tmp library.  Return 0.

    lib = (VeriLibrary *)save_restore.Id2Pointer(flag) ;
    if (!lib) { // If the library has not yet been registered
        if (flag == FLAG_LIB) {
            lib = new VeriLibrary(save_restore) ;
        } else if (flag == FLAG_USER_LIB ) {
            lib = new VeriUserLibrary(save_restore) ;
        }
        VERIFIC_ASSERT(lib) ;
        const char *name = lib->GetName() ;
        // Look for any exsiting library with the same name
        VeriLibrary *root_lib = name ? veri_file::GetLibrary(name) : 0 ;

        // If the library already exists then delete the restored library
        if (root_lib){
            delete lib ;
            lib = root_lib ;
        } else { // Insert the created lib
            veri_file::AddLibrary(lib) ;
        }
        // Register the library (this may be an existing library or a newly
        // created one)
        save_restore.RegisterPointer(lib) ;
    }

    return lib ;
}

/* ---------------------------- */

// Save and restore routines for classes in VeriLibrary.h

void
VeriLibrary::Save(SaveRestore &save_restore) const
{
    // Save the library name
    save_restore.SaveString(_name) ;
}

VeriLibrary::VeriLibrary(SaveRestore &save_restore)
  : _name(0),
    _module_table(0),
    _bb_list(0)
{
    // Restore library name
    _name = save_restore.RestoreString() ;
    _module_table = new Map(STRING_HASH) ; // Create the module table
}

/* ---------------------------- */

void
VeriUserLibrary::Save(SaveRestore &save_restore) const
{
    // Call base class's save restore function
    VeriLibrary::Save(save_restore) ;
    // Save array size
    save_restore.SaveInteger(_all_files_dirs ? _all_files_dirs->Size() : 0) ;
    // Save array of classes (VeriFileDirLibOption) to store -v/-y options
    unsigned i ;
    VeriFileDirLibOption *file_dir_opt ;
    FOREACH_ARRAY_ITEM(_all_files_dirs, i, file_dir_opt) {
        if (!file_dir_opt) continue ;
        // Save a flag to indicate as to whether this is a file or dir option
        save_restore.SaveInteger(file_dir_opt->IsLibOption() ? 2 : ((file_dir_opt->IsDirOption()) ? 1 : 0)) ;
        // Save the file dir option
        file_dir_opt->Save(save_restore) ;
    }
    // Save the size of the array used to store extensions
    save_restore.SaveInteger(_libext ? _libext->Size() : 0 ) ;
    char *ext ;
    FOREACH_ARRAY_ITEM(_libext, i, ext) {
        // Save the string
        save_restore.SaveString(ext) ;
    }

    // Save the set _module_names_to_lookup
    // char *instantiated_module_name->integer flag to indicate
    // resolved (0)/unresolved (1) module association. Helps to
    // avoid holes and maintain order
    save_restore.SaveInteger(_module_names_to_lookup ? _module_names_to_lookup->Size() : 0 ) ; // Save the map size
    MapIter mapIter ;
    char *mod_name ;
    long flag ;
    FOREACH_MAP_ITEM(_module_names_to_lookup, mapIter, &mod_name, &flag) {
        save_restore.SaveString(mod_name) ; // Save the key
        save_restore.SaveInteger(flag) ; // Save the value
    }

    // Save the set  _uselib_lib_mod_lib_map
    // char *instantiated_module_name->char *lib_name
    save_restore.SaveInteger(_uselib_lib_mod_lib_map ? _uselib_lib_mod_lib_map->Size() : 0 ) ; // Save the map size
    MapIter mapIter1 ;
    char *lib_name ;
    FOREACH_MAP_ITEM(_uselib_lib_mod_lib_map, mapIter1, &mod_name, &lib_name) {
        save_restore.SaveString(mod_name) ; // Save the key
        save_restore.SaveString(lib_name) ; // Save the library
    }

    // No need to save/restore the following item
    //      Set      *_module_names_added

    // Collect the bit flags in bit_flags and save
    // VIPER #5424: No need to save/restore '_is_processing' flag.
    // Save _lib_rescan_active flag to indicate if +librescan is active
    char bit_flags = _lib_rescan_active ; // Direct assign for first member. Macro not required.
    // Save the flag that indicates whether this is a `uselib specific library
    SAVE_BIT_FLAG(bit_flags, _tick_uselib, 1) ;
    SAVE_BIT_FLAG(bit_flags, _tick_uselib_lib, 1) ;
    save_restore.SaveChar(bit_flags) ;
    // Do not save-restore _dir_file_status
#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    // Do not save-restore _modules_in_file
#endif
}

VeriUserLibrary::VeriUserLibrary(SaveRestore &save_restore)
  : VeriLibrary(save_restore),
    _all_files_dirs(0),
    _libext(0),
    _module_names_to_lookup(0),
    _num_modules_to_lookup(0), // VIPER #7740
    _module_names_added(0),
    _lib_rescan_active(0),
    _tick_uselib(0),
    _tick_uselib_lib(0),
    _is_processing(0),
    _uselib_lib_mod_lib_map(0),
    _dir_file_status(0)
#if defined(VERILOG_QUICK_PARSE_V_FILES) && defined(VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS)
    , _modules_in_file(0) // Do not save-restore _modules_in_file
#endif
{
    // Restore array size for storing -v/-y options
    unsigned arr_size = save_restore.RestoreInteger() ;
    if (arr_size) {
        _all_files_dirs = new Array(arr_size) ;
        unsigned i ;
        VeriFileDirLibOption *file_dir_opt = 0 ;
        for (i=0; i<arr_size; i++) {
            // Restore flag to indicate file option or dir option
            unsigned flag = save_restore.RestoreInteger() ;
            // Create file or dir option structure
            if (flag == 2) {
                file_dir_opt = new VeriLibOption(save_restore) ;
            } else if (flag == 1) {
                file_dir_opt = new VeriDirOption(save_restore) ;
            } else {
                file_dir_opt = new VeriFileOption(save_restore) ;
            }
            // install
            _all_files_dirs->Insert(file_dir_opt) ;
        }
    }
    unsigned lib_ext_size = save_restore.RestoreInteger() ; // Restore array size
    if (lib_ext_size) {
        unsigned i ;
        _libext = new Array(lib_ext_size) ;
        for (i=0; i<lib_ext_size; i++) {
            char *ext = save_restore.RestoreString() ;
            _libext->Insert(ext) ;
        }
    }

    // Restore the set _module_names_to_lookup
    // char *instantiated_module_name->integer flag to indicate
    // resolved (0)/unresolved (1) module association. Helps to
    // avoid holes and maintain order
    unsigned map_size = save_restore.RestoreInteger() ; // Restore the map size
    if (map_size) {
        _module_names_to_lookup = new Map(STRING_HASH, map_size) ;
        unsigned i ;
        for (i=0; i<map_size; i++) {
            char *mod_name = save_restore.RestoreString() ;
            int flag = save_restore.RestoreInteger() ;
            (void)_module_names_to_lookup->Insert(mod_name, (void *)(long)flag) ; // Insert the key and the value in the map
        }
    }

    // Restore the set  _uselib_lib_mod_lib_map
    // char *instantiated_module_name->char *lib_name
    map_size = save_restore.RestoreInteger() ; // Restore the map size
    if (map_size) {
        _uselib_lib_mod_lib_map = new Map(STRING_HASH, map_size) ;
        unsigned i ;
        for (i=0; i<map_size; i++) {
            char *mod_name = save_restore.RestoreString() ;
            char *lib_name = save_restore.RestoreString() ;
            (void) _uselib_lib_mod_lib_map->Insert(mod_name, lib_name) ; // Insert the key and the value in the map
        }
    }

    // No need to save/restore the following item
    //      Set      *_module_names_added

    // Restore the bit flags in reverse order of saving.
    // VIPER #5424: No need to save/restore '_is_processing' flag.
    char bit_flags = save_restore.RestoreChar() ;
    // Restore the flag that indicates whether this is a `uselib lib
    RESTORE_BIT_FLAG(bit_flags, _tick_uselib_lib, 1) ;
    // Restore the flag that indicates whether this is a `uselib specific library
    RESTORE_BIT_FLAG(bit_flags, _tick_uselib, 1) ;
    // Restore integer flag to indicate if +librescan is active
    _lib_rescan_active = bit_flags ; // Direct assign for last member of bit_flags
    // Do not save-restore _dir_file_status
#ifdef VERILOG_QUICK_PARSE_V_FILES
    // Do not save-restore _modules_in_file
#endif
}

/* ---------------------------- */

void
VeriFileDirLibOption::Save(SaveRestore &save_restore) const
{
    // Save the name of file or directory
    save_restore.SaveString(_file_dir_name) ;
}

VeriFileDirLibOption::VeriFileDirLibOption(SaveRestore &save_restore)
  : _file_dir_name(0)
{
    // Restore name of file or directory
    _file_dir_name = save_restore.RestoreString() ;
}

/* ---------------------------- */

void
VeriFileOption::Save(SaveRestore &save_restore) const
{
    VeriFileDirLibOption::Save(save_restore) ;
}

VeriFileOption::VeriFileOption(SaveRestore &save_restore)
  : VeriFileDirLibOption(save_restore)
{
}

/* ---------------------------- */

void
VeriDirOption::Save(SaveRestore &save_restore) const
{
    VeriFileDirLibOption::Save(save_restore) ;
}

VeriDirOption::VeriDirOption(SaveRestore &save_restore)
  : VeriFileDirLibOption(save_restore)
{
}

/* ---------------------------- */

void
VeriLibOption::Save(SaveRestore &save_restore) const
{
    VeriFileDirLibOption::Save(save_restore) ;
}

VeriLibOption::VeriLibOption(SaveRestore &save_restore)
  : VeriFileDirLibOption(save_restore)
{
}

/* ---------------------------- */

void
VeriLibraryDecl::Save(SaveRestore &save_restore) const
{
    VeriTreeNode::Save(save_restore) ;
}

VeriLibraryDecl::VeriLibraryDecl(SaveRestore &save_restore)
  : VeriTreeNode(save_restore),
    _name(0),
    _patterns(0)
{
}

/*****************************************************************/
// Calculate Signature
/*****************************************************************/
#define HASH_NAME(SIG, NAME)  (((SIG) << 5) + (SIG) + (Hash::StringCompare((NAME), 0)))
#define HASH_NUM(SIG, NUM)    (((SIG) << 5) + (SIG) + (Hash::NumCompare((const void *)(long)(NUM), 0)))

unsigned long VeriScope::CalculateSaveRestoreSignature() const
{
    unsigned long sig = 0 ;

    // Iterate over its identifiers to create signature
    MapIter mi ;
    VeriIdDef *id ;
    VeriScope *child_scope = 0 ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (!id) continue ;
        // VIPER #4951 : If restored unit is elaborated, it can have elaborated
        // copied class identifiers. We should not consider those here, as
        // original sdb was always created from analyzed parse tree before elaboration
        VeriModuleItem *tree = (id->IsClass()) ? id->GetModuleItem(): 0 ;
        if (tree && tree->GetOriginalClassName()) continue ;
        // Consider properties of identifier in signature
        unsigned long id_sig = HASH_NUM(0, id->GetClassId()) ; // Consider class id
        if (id->Name()) id_sig = HASH_NAME(id_sig, id->Name()) ; // Consider its name
        VeriDataType *id_type = id->GetDataType() ;
        id_sig = HASH_NUM(id_sig, (id_type) ? 1 : 0) ; // data type
        id_sig = HASH_NUM(id_sig, id->PackedDimension()) ; // packed dimension count
        // VIPER #6062: Do not count id->Type() in case of type-refs, since it may get
        // resolved/set to actual/different type after static elaboration, which may
        // create issues later with mismatch in signature of perfectly valid nodes:
        // VIPER #6171: Take the type of the id-data type not that of the id and
        // only if the data type is not a type ref (since that may get changed):
        //if (!id_type || !id_type->IsTypeRef()) id_sig = HASH_NUM(id_sig, id->Type()) ; // type
        if (id_type && !id_type->IsTypeRef()) id_sig = HASH_NUM(id_sig, id_type->Type()) ; // type
        id_sig = HASH_NUM(id_sig, id->Dir()) ; // Direction
        id_sig = HASH_NUM(id_sig, id->IsImplicitNet()) ; // Implicit/explicit
        id_sig = HASH_NUM(id_sig, id->IsType()) ; // Type id
        id_sig = HASH_NUM(id_sig, id->IsModportPort()) ; // Modport port
        id_sig = HASH_NUM(id_sig, id->IsMember()) ; // Is member of struct/union
        id_sig = HASH_NUM(id_sig, id->UnPackedDimension()) ; // unpacked dimenison count
        Array *port_connects = id->GetPortConnects() ; // Port connects for instance id
        if (port_connects) id_sig = HASH_NUM(id_sig, port_connects->Size()) ;
        id_sig = HASH_NUM(id_sig, id->ParamType()) ; // Parameter type

        sig = HASH_NUM(sig, id_sig) ;
        // Incorporate signature of child scope
        child_scope = id->LocalScope() ;
        if (child_scope) sig = HASH_NUM(sig, child_scope->CalculateSaveRestoreSignature()) ;
    }

    // Consider number of using units in signature creation
    // VIPER #5115 : Consider size of _using in signature only when size is non-zero
    if (_using && _using->Size()) sig = HASH_NUM(sig, _using->Size()) ;

    return sig ;
}

void
VeriModport::UpdateModportPorts()
{
    unsigned i ;
    VeriModportDecl *decl ;
    FOREACH_ARRAY_ITEM(_modport_decls, i, decl) {
        if (decl) decl->UpdateModportPorts() ;
    }
}

void
VeriModportDecl::UpdateModportPorts()
{
    VeriScope *upper_scope = (_scope) ? _scope->Upper() : 0 ;
    if (!upper_scope) return ; // Can't do anything without this scope

    unsigned i ;
    VeriIdDef *modport_port ; // the modportport prototype
    VeriIdDef *modport_var ; // the original variable
    FOREACH_ARRAY_ITEM(_ports, i, modport_port) {
        if (!modport_port || modport_port->IsNamedPort()) continue ; // Named ports are not declared in interface scope
        modport_var = upper_scope->Find(modport_port->Name()) ;
        // VIPER #4264: Set the data type back-pointer in the modport_port
        // to the data type back-pointer of the defining variable:
        if (modport_var && modport_port->IsPort() && !modport_port->GetDataType()) {
            modport_port->SetDataType(modport_var->GetDataType()) ;
        }
        // VIPER #2620 && #4286: Set the modport_var module item back pointer
        // to modport_port. Also set the scope of the decl as the local scope:
        if (!modport_port->IsPort() && modport_var) {
            modport_port->SetModuleItem(modport_var->GetModuleItem()) ;
            modport_port->SetLocalScope(modport_var->LocalScope()) ;
        }
    }
}

