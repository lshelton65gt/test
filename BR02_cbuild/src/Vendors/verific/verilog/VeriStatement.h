/*
 *
 * [ File Version : 1.180 - 2014/03/13 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_STATEMENT_H_
#define _VERIFIC_VERI_STATEMENT_H_

#include "VeriModuleItem.h"

// As of 8/2004, VeriStatement (sequential stmts) is derived from VeriModuleItem (concurrent statements)
// These two classes have a lot in common, so it is natural to derive them.
// Makes parser cleaner also.

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Map ;
class BitArray ;
class VeriDelay ;
class VeriIdDef ;
class VeriExpression ;
class VeriName ;
class VeriEventControl ;
class VeriNetRegAssign ;
class VeriDelayOrEventControl ;
class VeriDataFlow ;
class VeriNonConstVal ;
class VeriCaseItem ;
class VeriIdRef ;
class VeriScope ;
class ValueTable;
class VeriBaseValue;

class VeriVarUsageInfo ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriStatement  : public VeriModuleItem
{
public :
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const { PrettyPrint(f,level) ; } // Default catch for Print statement without semi colon at end. Differs from default for only a few classes.

    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsStatement() const             { return 1 ; } // This test checks if this module item is a (sequential) statement :
    virtual unsigned            IsNullStatement() const         { return 0 ; } // This test checks if this module item is a null statement :
    virtual unsigned            IsDelayControlStmt() const      { return 0 ; } // Return 1 for delay control statement
    virtual unsigned            IsConcurrentAssertion() const   { return 0 ; } // Return 1 for concurrent assertion statement
    virtual unsigned            IsDeferredAssertion() const     { return 0 ; } // Return 1 for deferred assertion statement

    // Virtual accessor methods.
    virtual VeriScope *         GetScope() const ; //              { return 0 ; } // For statements with a scope
    virtual VeriName *          GetTaskName() const             { return 0 ; }
    virtual Array *             GetArgs() const                 { return 0 ; }
    virtual unsigned            GetMethodType() const           { return 0 ; }
    virtual void                SetMethodType(unsigned /*t*/)       { }
    virtual unsigned            GetCaseStyle() const            { return 0 ; }
    virtual Array *             GetAt() const                   { return 0 ; } // Defined for VeriEventControlStatement

    virtual VeriIdDef *         GetOpeningLabel() const         { return _opening_label ; } // Return label of this statement (SV only)
    virtual VeriExpression *    GetSvaClkExpr() const           { return 0 ; } // The resolved clock expression Viper 6456

    // Parse tree copy routines :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const = 0 ; // Pure virtual function to copy statement as statement
    virtual VeriModuleItem *    CopyModuleItem(VeriMapForCopy &id_map_table) const { return CopyStatement(id_map_table) ; } // Copy statement as a module item

    // Specific parse-tree manipulation routines.
    // VIPER #6432 : Add default argument to these ReplaceChildXXX Apis, so that caller can determine
    // whether they want to delete old parse tree or not. By default, old parse tree is delete, but
    // if ReplaceChildXXX is called passing 0 as third argument, old parse tree will not be deleted.
    unsigned            ReplaceChildEventControl(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node=1) { return ReplaceChildEventControlInternal(old_control, new_control, delete_old_node) ; } // Delete 'old_control', and puts new control in its place
    unsigned            ReplaceChildNetRegAssign(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node=1) { return ReplaceChildNetRegAssignInternal(old_assign, new_assign, delete_old_node) ; } // Delete 'old_assign', and puts new assign in its place
    unsigned            ReplaceChildCaseItem(VeriCaseItem *old_case, VeriCaseItem *new_case, unsigned delete_old_node=1) { return ReplaceChildCaseItemInternal(old_case, new_case, delete_old_node) ; } // Delete 'old_case' from case item list, and puts new case item in its place.
    unsigned            ReplaceChildProduction(VeriProduction *old_prod, VeriProduction *new_prod, unsigned delete_old_node=1) { return ReplaceChildProductionInternal(old_prod, new_prod, delete_old_node) ; } // Delete 'old_prod' and puts 'new_prod' in its place

    // Class identifier method
    virtual unsigned            IsEventControl() const         { return 0 ; }
    virtual unsigned            IsEventTrigger() const         { return 0 ; }

    virtual VeriExpression *    GetIfExpr() const           { return 0 ; } // only for conditional statements
    virtual VeriStatement *     GetThenStmt() const         { return 0 ; } // only for conditional statements
    virtual VeriStatement *     GetElseStmt() const         { return 0 ; } // only for conditional statements

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    VeriStatement();

    VeriStatement(const VeriStatement &stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriStatement(SaveRestore &save_restore) ;

public:
    virtual ~VeriStatement();

private:
    // Prevent compiler from defining the following
    VeriStatement(const VeriStatement &) ;            // Purposely leave unimplemented
    VeriStatement& operator=(const VeriStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                PrettyPrintPreStatement(ostream &f, unsigned level) const ; // Pretty-print general stuff (comments, attributes etc) before the statement itself gets printed.
    virtual void                ResolveArrayMethod(VeriScope *scope, veri_environment environment) ; // Resolve array methods
    virtual void                SetIsImmediateAssertion()       { }
    // System Verilog allows a label on every statement
    virtual void                SetOpeningLabel(VeriIdDef *opening_label) ; // Set the label of this statement (SV only). OK since

    // Internal routines.
    virtual unsigned            ReplaceChildEventControlInternal(VeriDelayOrEventControl * /*old_control*/, VeriDelayOrEventControl * /*new_control*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_control', and puts new control in its place
    virtual unsigned            ReplaceChildNetRegAssignInternal(VeriNetRegAssign * /*old_assign*/, VeriNetRegAssign * /*new_assign*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_assign', and puts new assign in its place
    virtual unsigned            ReplaceChildCaseItemInternal(VeriCaseItem * /*old_case*/, VeriCaseItem * /*new_case*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_case' from case item list, and puts new case item in its place.
    virtual unsigned            ReplaceChildProductionInternal(VeriProduction * /*old_prod*/, VeriProduction * /*new_prod*/, unsigned /*delete_old_node*/) { return 0 ; } // Delete 'old_prod' and puts 'new_prod' in its place

    // Static Elaboration
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst) ;
    virtual void                StaticReplaceConstantExpr(unsigned /*is_const_index_context*/) {} // Replace range bounds, default value, bit/part/slice expressions

    // Validate the statements of deferred assertion. Only subprogram call is allowed
    void                        ValidateDeferredAssertStmt(veri_environment environment) const ;

protected:
    VeriIdDef *_opening_label ; // System Verilog allows a label on every statement
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriBlockingAssign : public VeriStatement
{
public:
    VeriBlockingAssign(VeriExpression *lval, VeriDelayOrEventControl *control, VeriExpression *val, unsigned oper_type = 0) ; // Allow different assignment operator tokens. Default (0) will be interpreted as '='.

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERIBLOCKINGASSIGN; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ; // Should only be called from VeriFor::PrettyPrint

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetLVal() const             { return _lval ; }
    VeriDelayOrEventControl *   GetControl() const          { return _control ; }
    VeriExpression *            GetValue() const            { return _val ; }
    virtual unsigned            OperType() const            { return _oper_type ; } // VERI_EQUAL_ASSIGN for Verilog2000. More variety for SV.

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy blocking assignment as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriBlockingAssign(const VeriBlockingAssign &block_assign, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriBlockingAssign(SaveRestore &save_restore) ;

    virtual ~VeriBlockingAssign() ;

private:
    // Prevent compiler from defining the following
    VeriBlockingAssign() ;                                      // Purposely leave unimplemented
    VeriBlockingAssign(const VeriBlockingAssign &) ;            // Purposely leave unimplemented
    VeriBlockingAssign& operator=(const VeriBlockingAssign &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it matches with lhs/rhs, and puts new expression in its place
    virtual unsigned            ReplaceChildEventControlInternal(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node) ; // Delete 'old_control', and puts new control in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression          *_lval ;
    unsigned                 _oper_type ; // VERI_EQUAL_ASSIGN for Verilog2000. More variety for SV.
    VeriDelayOrEventControl *_control ;
    VeriExpression          *_val ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriBlockingAssign

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriNonBlockingAssign : public VeriStatement
{
public:
    VeriNonBlockingAssign(VeriExpression *lval, VeriDelayOrEventControl *control, VeriExpression *val) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERINONBLOCKINGASSIGN; } // Defined in VeriClassIds.h
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetLVal() const             { return _lval ; }
    VeriDelayOrEventControl *   GetControl() const          { return _control ; }
    VeriExpression *            GetValue() const            { return _val ; }

    // Parse tree copy routines :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy non blocking assignment as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriNonBlockingAssign(const VeriNonBlockingAssign &non_block_assign, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriNonBlockingAssign(SaveRestore &save_restore) ;

    virtual ~VeriNonBlockingAssign() ;

private:
    // Prevent compiler from defining the following
    VeriNonBlockingAssign() ;                                         // Purposely leave unimplemented
    VeriNonBlockingAssign(const VeriNonBlockingAssign &) ;            // Purposely leave unimplemented
    VeriNonBlockingAssign& operator=(const VeriNonBlockingAssign &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from lhs/rhs, and puts in expression in its place.
    virtual unsigned            ReplaceChildEventControlInternal(VeriDelayOrEventControl *old_control, VeriDelayOrEventControl *new_control, unsigned delete_old_node) ; // Delete 'old_control', and puts new control in its place.

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression          *_lval ;
    VeriDelayOrEventControl *_control ;
    VeriExpression          *_val ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNonBlockingAssign

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriGenVarAssign : public VeriStatement
{
public:
    VeriGenVarAssign(char *name, VeriExpression *value, unsigned oper_type = 0) ;    // unresolved reference
    VeriGenVarAssign(VeriIdDef *id, VeriExpression *value, unsigned oper_type = 0) ; // resolved reference

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIGENVARASSIGN; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ; // Should only be called from VeriFor::PrettyPrint

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    // Viper #6081: Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual const char *        GetName() const             { return _name ; }
    VeriIdDef *                 GetId() const               { return _id; }
    VeriExpression *            GetValue() const            { return _value ; }
    virtual unsigned            OperType() const            { return _oper_type ; } // VERI_EQUAL_ASSIGN for Verilog2000.

    // Parse tree copy routines :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy genvar assignment as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriGenVarAssign(const VeriGenVarAssign &gen_var_assign, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriGenVarAssign(SaveRestore &save_restore) ;

    virtual ~VeriGenVarAssign() ;

private:
    // Prevent compiler from defining the following
    VeriGenVarAssign() ;                                    // Purposely leave unimplemented
    VeriGenVarAssign(const VeriGenVarAssign &) ;            // Purposely leave unimplemented
    VeriGenVarAssign& operator=(const VeriGenVarAssign &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new value for the genvar

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst) ;

protected:
    char           *_name ; // unresolved reference
    VeriIdDef      *_id ;   // resolved reference
    VeriExpression *_value ;
    unsigned        _oper_type ; // VERI_EQUAL for Verilog 2000. Different operators for System Verilog
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriGenVarAssign

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriAssign : public VeriStatement
{
public:
    explicit VeriAssign(VeriNetRegAssign *assign) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIASSIGN; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action);

    // Accessor methods
    VeriNetRegAssign *          GetAssign() const           { return _assign ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy assignment as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriAssign(const VeriAssign &assign, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriAssign(SaveRestore &save_restore) ;

    virtual ~VeriAssign() ;
private:
    // Prevent compiler from defining the following
    VeriAssign() ;                              // Purposely leave unimplemented
    VeriAssign(const VeriAssign &) ;            // Purposely leave unimplemented
    VeriAssign& operator=(const VeriAssign &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildNetRegAssignInternal(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node) ; // Delete 'old_assign', and puts new assign in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriNetRegAssign *_assign ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAssign

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriDeAssign : public VeriStatement
{
public:
    explicit VeriDeAssign(VeriExpression *lval) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDEASSIGN; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    VeriExpression *            GetLVal() const             { return _lval ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy deassign construct as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDeAssign(const VeriDeAssign &de_assign, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDeAssign(SaveRestore &save_restore) ;

    virtual ~VeriDeAssign() ;

private:
    // Prevent compiler from defining the following
    VeriDeAssign() ;                                // Purposely leave unimplemented
    VeriDeAssign(const VeriDeAssign &) ;            // Purposely leave unimplemented
    VeriDeAssign& operator=(const VeriDeAssign &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine .
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expression as new lvalue to be deassigned

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_lval ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDeAssign

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriForce : public VeriStatement
{
public:
    explicit VeriForce(VeriNetRegAssign *assign) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIFORCE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    VeriNetRegAssign *          GetAssign() const           { return _assign ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy force statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriForce(const VeriForce &force, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriForce(SaveRestore &save_restore) ;

    virtual ~VeriForce() ;

private:
    // Prevent compiler from defining the following
    VeriForce() ;                             // Purposely leave unimplemented
    VeriForce(const VeriForce &) ;            // Purposely leave unimplemented
    VeriForce& operator=(const VeriForce &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNetRegAssignInternal(VeriNetRegAssign *old_assign, VeriNetRegAssign *new_assign, unsigned delete_old_node) ; // Delete 'old_assign', and puts new force assignment

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriNetRegAssign *_assign ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriForce

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriRelease : public VeriStatement
{
public:
    explicit VeriRelease(VeriExpression *lval) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIRELEASE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    VeriExpression *            GetLVal() const             { return _lval ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy release statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriRelease(const VeriRelease &rel, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriRelease(SaveRestore &save_restore) ;

    virtual ~VeriRelease() ;

private:
    // Prevent compiler from defining the following
    VeriRelease() ;                               // Purposely leave unimplemented
    VeriRelease(const VeriRelease &) ;            // Purposely leave unimplemented
    VeriRelease& operator=(const VeriRelease &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new released expression in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_lval ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriRelease

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriTaskEnable : public VeriStatement
{
public:
// 6/2004 : Change in class : name reference changed from char* to VeriName field.
//    VeriTaskEnable(char *name, Array *args) ; // unresolved call
//    VeriTaskEnable(VeriIdDef *task, Array *args) ; // resolved call
    VeriTaskEnable(VeriName *task_name, Array *args) ; // resolved call

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERITASKENABLE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
// Following two replaced by GetTaskName()
//    const char *              GetName() const             { return _name ; }
//    VeriIdDef *               GetId() const               { return _id ; }
    virtual VeriName *          GetTaskName() const         { return _task_name ; }
    virtual Array *             GetArgs() const             { return _args ; }
    virtual unsigned            GetMethodType() const       { return _method_type ; }
    virtual void                SetMethodType(unsigned t)   { _method_type = t ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy task enable as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTaskEnable(const VeriTaskEnable &task_enable, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTaskEnable(SaveRestore &save_restore) ;

    virtual ~VeriTaskEnable() ;

private:
    // Prevent compiler from defining the following
    VeriTaskEnable() ;                                  // Purposely leave unimplemented
    VeriTaskEnable(const VeriTaskEnable &) ;            // Purposely leave unimplemented
    VeriTaskEnable& operator=(const VeriTaskEnable &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ResolveArrayMethod(VeriScope *scope, veri_environment environment) ; // Resolve array methods
    void                        CheckFunctionPrefix(const VeriScope *scope) const ; // Check prefix for randomize, constraint_mode and random_mode functions. Viper 5161, Viper 5174

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', and puts new task name in its place
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from argument list, and puts new argument in its place

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst) ;

protected:
    VeriName        *_task_name ;
//    char          *_name ; // unresolved task name.
//    VeriIdDef     *_id ;   // resolved task id.
    Array           *_args ; // Array of VeriExpression*. Can contain VeriPortConnect. The task-enable arguments.
    unsigned         _method_type ; // Token indicating a possible (built-in) method.
//    unsigned         _void_cast:1 ; // void casting of the task enable VIPER 5129. RD: replaced by Qualifier VERI_VOID on statement.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTaskEnable

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriSystemTaskEnable : public VeriStatement
{
public:
    VeriSystemTaskEnable(char *name, Array *args) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISYSTEMTASKENABLE; } // Defined in VeriClassIds.h
    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ; // VIPER 2539 : used without semi as sequence_match_item/loop variable assignment

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    const char *                GetName() const             { return _name ; }
    Array *                     GetArgs() const             { return _args ; }
    unsigned                    GetFunctionType() const     { return _function_type ; } // Get (function) token for this system task. 0 if not known.

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy system task enable as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSystemTaskEnable(const VeriSystemTaskEnable &sys_task_enable, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSystemTaskEnable(SaveRestore &save_restore) ;

    virtual ~VeriSystemTaskEnable() ;

private:
    // Prevent compiler from defining the following
    VeriSystemTaskEnable() ;                                        // Purposely leave unimplemented
    VeriSystemTaskEnable(const VeriSystemTaskEnable &) ;            // Purposely leave unimplemented
    VeriSystemTaskEnable& operator=(const VeriSystemTaskEnable &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from argument list, and puts new argument in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst) ; // VIPER #2704: Implement this method so that it does not go to the virtual catcher
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    char        *_name ; // unresolved system task name.
    Array       *_args ; // Array of VeriExpression*. Should not contain VeriPortConnect. The system task-enable arguments.
// Set during construction :
    unsigned     _function_type ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSystemTaskEnable

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriDelayControlStatement : public VeriStatement
{
public:
    VeriDelayControlStatement(VeriExpression *delay, VeriStatement *stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDELAYCONTROLSTATEMENT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetDelay() const            { return _delay ; }
    VeriStatement *             GetStmt() const             { return _stmt ; }
    virtual unsigned            IsDelayControlStmt() const  { return 1 ; } // Return 1 for delay control statement
    unsigned                    IsCycleDelay()       const  { return _is_cycle_delay ; }
    void                        SetIsCycleDelay()           { _is_cycle_delay = 1 ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy delay control statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDelayControlStatement(const VeriDelayControlStatement &delay_control_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDelayControlStatement(SaveRestore &save_restore) ;

    virtual ~VeriDelayControlStatement() ;

private:
    // Prevent compiler from defining the following
    VeriDelayControlStatement() ;                                             // Purposely leave unimplemented
    VeriDelayControlStatement(const VeriDelayControlStatement &) ;            // Purposely leave unimplemented
    VeriDelayControlStatement& operator=(const VeriDelayControlStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt', and puts new statement in its place.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' specific delay expression, and puts new expression in its place.

    // Static Elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_delay ;
    VeriStatement  *_stmt ;
    unsigned _is_cycle_delay:1 ; // Viper 4504 Flag to store this is cycle delay
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDelayControlStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriEventControlStatement : public VeriStatement
{
public:
    VeriEventControlStatement(Array *at, VeriStatement *stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIEVENTCONTROLSTATEMENT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Class identifier method
    virtual unsigned            IsEventControl() const      { return 1 ; }

    // Accessor methods
    virtual Array *             GetAt() const               { return _at ; } // If _at is NULL, then this means all read signals are included (ie. @*)
    VeriStatement *             GetStmt() const             { return _stmt ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy event control statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriEventControlStatement(const VeriEventControlStatement &event_control_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriEventControlStatement(SaveRestore &save_restore) ;

    virtual ~VeriEventControlStatement() ;

private:
    // Prevent compiler from defining the following
    VeriEventControlStatement() ;                                             // Purposely leave unimplemented
    VeriEventControlStatement(const VeriEventControlStatement &) ;            // Purposely leave unimplemented
    VeriEventControlStatement& operator=(const VeriEventControlStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' from event expressions, and puts new event expression in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ;   // Delete 'old_stmt' specific statement, and puts new statement in its place.

    // Static Elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array           *_at ;   // Array of VeriExpression*. The event control expressions : @(<expr1>,<expr2>,<expr3>..).
    VeriStatement   *_stmt ; // The statement to execute under event control.
    unsigned         _sva_clock_expr_index ; // This is set during Resolve. It is a pointer to _at list for the sva clock expression Viper 6753
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriEventControlStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriConditionalStatement : public VeriStatement
{
public:
    VeriConditionalStatement(VeriExpression *if_expr, VeriStatement *then_stmt, VeriStatement *else_stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICONDITIONALSTATEMENT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriExpression *    GetIfExpr() const           { return _if_expr ; }
    virtual VeriStatement *     GetThenStmt() const         { return _then_stmt ; }
    virtual VeriStatement *     GetElseStmt() const         { return _else_stmt ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy conditional statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConditionalStatement(const VeriConditionalStatement &cond_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriConditionalStatement(SaveRestore &save_restore) ;

    virtual ~VeriConditionalStatement() ;

private:
    // Prevent compiler from defining the following
    VeriConditionalStatement() ;                                            // Purposely leave unimplemented
    VeriConditionalStatement(const VeriConditionalStatement &) ;            // Purposely leave unimplemented
    VeriConditionalStatement& operator=(const VeriConditionalStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is if-condition, and puts new condition in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' from then/else statement, and puts new statement in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_if_expr ;
    VeriStatement  *_then_stmt ;
    VeriStatement  *_else_stmt ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConditionalStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriCaseStatement : public VeriStatement
{
public:
    VeriCaseStatement(unsigned case_style, VeriExpression *cond, Array *case_items) ;

     // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICASESTATEMENT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual unsigned            GetCaseStyle() const        { return _case_style ; }
    VeriExpression *            GetCondition() const        { return _cond ; }
    Array *                     GetCaseItems() const        { return _case_items ; }
    unsigned                    IsFullCase() const ;       // via GetAttributes() and bit flag _is_full_case
    unsigned                    IsParallelCase() const ;   // via GetAttributes() and bit flag _is_parallel_case
    unsigned                    GetCaseType() const         { return _type ; }  // Get case type VERI_MATCHES/VERI_INSIDE for inside case or pattern matching case statement

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy case statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCaseStatement(const VeriCaseStatement &case_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCaseStatement(SaveRestore &save_restore) ;

    virtual ~VeriCaseStatement() ;

private:
    // Prevent compiler from defining the following
    VeriCaseStatement() ;                                     // Purposely leave unimplemented
    VeriCaseStatement(const VeriCaseStatement &) ;            // Purposely leave unimplemented
    VeriCaseStatement& operator=(const VeriCaseStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    void                        ProcessCaseStmtPragmas(Map *pragmas) ;

    void                        SetCaseType(unsigned t)     { _type = t ; }     // Set case type VERI_MATCHES/VERI_INSIDE
    void                        SetFullCase()               { _is_full_case = 1 ; }     // Set only the bit flag
    void                        SetParallelCase()           { _is_parallel_case = 1 ; } // Set only the bit flag
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if its is case condition, and puts new condition in its place.
    virtual unsigned            ReplaceChildCaseItemInternal(VeriCaseItem *old_case, VeriCaseItem *new_case, unsigned delete_old_node) ; // Delete 'old_case' from case item list, and puts new case item in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned        _case_style ;
    VeriExpression *_cond ;
    Array          *_case_items ;   // Array of VeriCaseItem
    unsigned        _type:30 ;      // Token VERI_MATCHES or VERI_INSIDE to represent case-inside and pattern matching case statement
    unsigned        _is_full_case:1 ;     // Set if pragma full_case was specified and recognized
    unsigned        _is_parallel_case:1 ; // Set if pragma parallel_case was specified and recognized
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCaseStatement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriLoop : public VeriStatement
{
    // This is a pure-virtual collection class for all 'loop' statements
protected : // Abstract class
    VeriLoop(VeriStatement *stmt, VeriScope *scope) ;

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0; // Ids defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;

    // Accessor methods
    VeriStatement *             GetStmt() const             { return _stmt ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    VeriLoop(const VeriLoop &loop, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriLoop(SaveRestore &save_restore) ;

public:
    virtual ~VeriLoop() ;

private:
    // Prevent compiler from defining the following
    VeriLoop() ;                            // Purposely leave unimplemented
    VeriLoop(const VeriLoop &) ;            // Purposely leave unimplemented
    VeriLoop& operator=(const VeriLoop &) ; // Purposely leave unimplemented

protected:
public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' if it is loop statement, and puts new statement in its place.

    // Static Elaboration
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriStatement  *_stmt ;
    VeriScope      *_scope ; // scope created for break/continue labels
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriLoop

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriForever : public VeriLoop
{
public:
    VeriForever(VeriStatement *stmt, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIFOREVER; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy forever statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriForever(const VeriForever &for_ever, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriForever(SaveRestore &save_restore) ;

    virtual ~VeriForever() ;

private:
    // Prevent compiler from defining the following
    VeriForever() ;                               // Purposely leave unimplemented
    VeriForever(const VeriForever &) ;            // Purposely leave unimplemented
    VeriForever& operator=(const VeriForever &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriForever

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriRepeat : public VeriLoop
{
public:
    VeriRepeat(VeriExpression *cond, VeriStatement *stmt, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIREPEAT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetCondition() const        { return _cond ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy repeat statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriRepeat(const VeriRepeat &repeat, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriRepeat(SaveRestore &save_restore) ;

    virtual ~VeriRepeat() ;

private:
    // Prevent compiler from defining the following
    VeriRepeat() ;                              // Purposely leave unimplemented
    VeriRepeat(const VeriRepeat &) ;            // Purposely leave unimplemented
    VeriRepeat& operator=(const VeriRepeat &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is repeat expression, and puts new expression in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_cond ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriRepeat

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriWhile : public VeriLoop
{
public:
    VeriWhile(VeriExpression *cond, VeriStatement *stmt, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIWHILE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetCondition() const        { return _cond ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy while loop as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriWhile(const VeriWhile &veri_while, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriWhile(SaveRestore &save_restore) ;

    virtual ~VeriWhile() ;

private:
    // Prevent compiler from defining the following
    VeriWhile() ;                             // Purposely leave unimplemented
    VeriWhile(const VeriWhile &) ;            // Purposely leave unimplemented
    VeriWhile& operator=(const VeriWhile &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is while condition, and puts new condition in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_cond ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriWhile

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriFor : public VeriLoop
{
public:
// Since 9/2004, for System Verilog support,
//    field VeriStatement '_initial' is replaced by Array of VeriModuleItem (VeriStatement for pure Verilog 2000 designs).
//    field VeriStatement '_rep' is replaced by Array of VeriStatement.
// All API routines here are adjusted for this modification.
//    The back-ward compatibility accessor functions also still operate, but are only valid for pure Verilog 2000 designs (will be incomplete for SV).
//
    VeriFor(Array *initials, VeriExpression *cond, Array *reps, VeriStatement *stmt, VeriScope *scope) ;
// The old constructor is maintained here just for backward-compatibility :
    VeriFor(const VeriStatement *initial, VeriExpression *cond, const VeriStatement *rep, VeriStatement *stmt, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIFOR; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods :
    Array *                     GetInitials() const         { return _initials; } // Array of VeriModuleItem :the comma separated list of assignments and declarations.
    VeriExpression *            GetCondition() const        { return _cond ; }    // Return the test-condition (to stay or leave the loop).
    Array *                     GetRepetitions() const      { return _reps ; }    // Array of VeriStatement : the comma separated list of assignment statements..

    // Backward compatibility Accessor methods (still work, but only for pure Verilog2000 designs (returns first statement)
    VeriStatement *             GetInitial() const ;
    VeriStatement *             GetRepetition() const ;

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy for loop as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriFor(const VeriFor &veri_for, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriFor(SaveRestore &save_restore) ;

    virtual ~VeriFor() ;

private:
    // Prevent compiler from defining the following
    VeriFor() ;                           // Purposely leave unimplemented
    VeriFor(const VeriFor &) ;            // Purposely leave unimplemented
    VeriFor& operator=(const VeriFor &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it  is final condition, and puts new condition in its place.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from initial statement(declaration)/incremental statements, and puts new item in its place.

    // Static Elaboration
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array           *_initials ;
    VeriExpression  *_cond ;
    Array           *_reps ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriFor

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriWait : public VeriStatement
{
public:
    VeriWait(VeriExpression *cond, VeriStatement *stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIWAIT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    VeriExpression *            GetCondition() const        { return _cond ; }
    VeriStatement *             GetStmt() const             { return _stmt ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy wait statement as statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriWait(const VeriWait &wait, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriWait(SaveRestore &save_restore) ;

    virtual ~VeriWait() ;

private:
    // Prevent compiler from defining the following
    VeriWait() ;                            // Purposely leave unimplemented
    VeriWait(const VeriWait &) ;            // Purposely leave unimplemented
    VeriWait& operator=(const VeriWait &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is wait condition, and puts new condition in its place
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' if it is wait statement, and puts new statement in its place.

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_cond ; // Wait condition.
    VeriStatement  *_stmt ; // Wait statement.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriWait

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriDisable : public VeriStatement
{
public:
// 6/2004 : Change in class : name reference changed from char* to VeriName field.
//    VeriDisable(char *name) ;    // unresolved reference
//    VeriDisable(VeriIdDef *id) ; // resolved reference
    explicit VeriDisable(VeriName *task_block_name) ; // unresolved reference

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDISABLE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
//    const char *              GetName() const             { return _name ; }
//    VeriIdDef *               GetId() const               { return _id ; }
    VeriName *                  GetTaskBlockName()          { return _task_block_name ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy disable statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDisable(const VeriDisable &disable, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDisable(SaveRestore &save_restore) ;

    virtual ~VeriDisable() ;

private:
    // Prevent compiler from defining the following
    VeriDisable() ;                               // Purposely leave unimplemented
    VeriDisable(const VeriDisable &) ;            // Purposely leave unimplemented
    VeriDisable& operator=(const VeriDisable &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is task/block name, and puts new name in its place.

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ; // VIPER #7856
    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);

protected:
//    char        *_name ;
//    VeriIdDef   *_id ;
    VeriName      *_task_block_name ; // reference to a task or block identifier
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDisable

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriEventTrigger : public VeriStatement
{
public:
// 6/2004 : Change in class : name reference changed from char* to VeriName field.
//    VeriEventTrigger(char *name) ;    // unresolved reference
//    VeriEventTrigger(VeriIdDef *id) ; // resolved reference
    explicit VeriEventTrigger(VeriName *event_name) ; // reference to an event identifier
    VeriEventTrigger(unsigned trigger_op, VeriName *event_name, VeriDelayOrEventControl *control = 0) ; // reference to an event identifier

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIEVENTTRIGGER; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    virtual unsigned            IsEventTrigger() const      { return 1 ; }

    // Accessor methods
//    const char *              GetName() const             { return _name ; }
//    VeriIdDef *               GetId() const               { return _id ; }
    unsigned                    GetTriggerOper() const      { return _trigger_oper; } // VIPER #4668
    VeriDelayOrEventControl *   GetControl() const          { return _control ; } // VIPER #5198
    VeriName *                  GetEventName() const        { return _event_name ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy event trigger

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriEventTrigger(const VeriEventTrigger &event_trig, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriEventTrigger(SaveRestore &save_restore) ;

    virtual ~VeriEventTrigger() ;

private:
    // Prevent compiler from defining the following
    VeriEventTrigger() ;                                    // Purposely leave unimplemented
    VeriEventTrigger(const VeriEventTrigger &) ;            // Purposely leave unimplemented
    VeriEventTrigger& operator=(const VeriEventTrigger &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name' if it is event name, and puts new name in its place

protected:
    unsigned                 _trigger_oper ; // event trigger operator VERI_RIGHTARRAW_ARROW or VERI_RIGHTARROW (VIPER #4031)
    VeriDelayOrEventControl *_control ;      // VIPER #4673: Store the delay or event control
    VeriName                *_event_name ;   // reference to a event identifier
//    char                    *_name ;
//    VeriIdDef               *_id ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriEventTrigger

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriSeqBlock : public VeriStatement
{
public:

    // VeriBlockDecl is removed since 8/2004. It's decl_items and statements are now
    // directly inserted in the VeriSeqBlock and VeriParBlock classes which used them before.
    // Accessor routines for VeriBlockDecl are now directly available off VeriSeqBlock and VeriParBlock.

    //VeriSeqBlock(VeriBlockDecl *block, Array *stmts, VeriScope *scope) ;
    VeriSeqBlock(VeriIdDef *label, Array *decl_items, Array *stmts, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERISEQBLOCK; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // VIPER #7249: Set label, we are about to invalidate a _label pointer
    virtual void                SetId(VeriIdDef *label) ;

    // Accessor methods
    virtual VeriIdDef *         GetId() const               { return _label ; }   // VIPER 2607. Get the block label.
    VeriIdDef *                 GetLabel() const            { return _label ; }
    Array *                     GetDeclItems() const        { return _decl_items ; }
    Array *                     GetStatements() const       { return _stmts ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }

    // VIPER #6922: Marking a statement as sequential block:
    virtual unsigned            IsSeqBlock() const          { return 1 ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy sequential block.

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSeqBlock(const VeriSeqBlock &seq_block, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSeqBlock(SaveRestore &save_restore) ;

    virtual ~VeriSeqBlock() ;

private:
    // Prevent compiler from defining the following
    VeriSeqBlock() ;                                // Purposely leave unimplemented
    VeriSeqBlock(const VeriSeqBlock &) ;            // Purposely leave unimplemented
    VeriSeqBlock& operator=(const VeriSeqBlock &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual unsigned            AttachCommentToLastChild(const VeriCommentNode *comment) { return AttachCommentToLastChildInArray(_stmts, comment) ; } // VIPER #7560

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from declaration list, and puts new declaration in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' from statement list, and puts new statement in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst);
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef       *_label ;       // The block label
    Array           *_decl_items ;  // Array of VeriModuleItem.  The declaration items
    Array           *_stmts ;       // Array of VeriStatement. The block statements
    VeriScope       *_scope ;       // The block-scope. A symbol table.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSeqBlock

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VeriParBlock : public VeriStatement
{
public: // Fork-Join block.

    // VeriBlockDecl is removed since 8/2004. It's decl_items and statements are now
    // directly inserted in the VeriSeqBlock and VeriParBlock classes which used them before.
    // Accessor routines for VeriBlockDecl are now directly available off VeriSeqBlock and VeriParBlock.

    //VeriParBlock(VeriBlockDecl *block, Array *stmts, VeriScope *scope) ;
    VeriParBlock(VeriIdDef *label, Array *decl_items, Array *stmts, VeriScope *scope, unsigned closing_token = 0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIPARBLOCK; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    virtual unsigned            IsParBlock() const          { return 1 ; }

    // VIPER #7249: Set label, we are about to invalidate a _label pointer
    virtual void                SetId(VeriIdDef *label) ;

    // Accessor methods
    virtual VeriIdDef *         GetId() const               { return _label ; }         // VIPER 2607. Get the block label.
    VeriIdDef *                 GetLabel() const            { return _label ; }         // The block label
    Array *                     GetDeclItems() const        { return _decl_items ; }    // Array of VeriModuleItem.  The declaration items
    Array *                     GetStatements() const       { return _stmts ; }         // Array of VeriStatement. The block statements
    virtual VeriScope *         GetScope() const            { return _scope ; }         // The block-scope. A symbol table.
    unsigned                    GetClosingToken() const     { return _closing_token ; } // VERI_JOIN in Verilog2000. More freedom in SV.

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy parallel block

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriParBlock(const VeriParBlock &par_block, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriParBlock(SaveRestore &save_restore) ;

    virtual ~VeriParBlock() ;

private:
    // Prevent compiler from defining the following
    VeriParBlock() ;                                // Purposely leave unimplemented
    VeriParBlock(const VeriParBlock &) ;            // Purposely leave unimplemented
    VeriParBlock& operator=(const VeriParBlock &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item' from declaration list, and puts new declaration in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' from statement list and puts new statement in its place.

    // Static Elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriIdDef       *_label ;           // The block label
    Array           *_decl_items ;      // Array of VeriModuleItem.  The declaration items
    Array           *_stmts ;           // Array of VeriStatement. The block statements
    VeriScope       *_scope ;           // The block-scope. A symbol table.
    unsigned         _closing_token ;   // VERI_JOIN in Verilog2000. More freedom in SV.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriParBlock

/* -------------------------------------------------------------- */

/*
   Statements for System Verilog

   VIPER #5710(vcs_compatibility: always_checker(for checker instantiation inside procedural code)):
   We declare the class VeriSequentialInstantiation for instantiating sequential instances (eg. checker) inside procedural code:
*/
class VFC_DLL_PORT VeriSequentialInstantiation : public VeriStatement
{
public:
    VeriSequentialInstantiation(VeriName *module_name, const VeriInstId *single_inst, Array *additional_insts) ; // single instance, with possible additional insts.

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERISEQUENTIALINSTANTIATION ; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Cast to VeriModuleItemInstance
    virtual VeriSequentialInstantiation* CastSequentialInstance() { return (static_cast<class VeriSequentialInstantiation*>(this)) ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Tests
    virtual unsigned            IsInstantiation() const         { return 1 ; } // Test if this is a (module) instantiation.

    // Find the module that is being instantiated here.
    virtual VeriModule *        GetInstantiatedModule() const ; // Find the instantiated VeriModule, and return it.

    // Accessor methods
    VeriName *                  GetModuleNameRef() const        { return _module_name ; }
    Array *                     GetInstances() const            { return (Array *)&_insts ; }
    const char *                GetModuleName() const ;  // backward compatible accessor
    // Virtual accessors :
    virtual Array *             GetIds() const                  { return (Array *)&_insts ; }  // VeriSequentialInstantiation now contains an array of VeriInstId (VeriIdDef's).

    // Verific routines for user-library (-v/-y/uselib) setting
    VeriLibrary *               GetLibrary() const              { return _library ; } // The library in which the module should be found (uselib/-v/-y). If 0, we look in the work library.
    // Parse tree copy routines :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // copy module instantiation

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriSequentialInstantiation(const VeriSequentialInstantiation &mod_inst, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriSequentialInstantiation(SaveRestore &save_restore) ;

    virtual ~VeriSequentialInstantiation() ;

private:
    // Prevent compiler from defining the following
    VeriSequentialInstantiation() ;                                               // Purposely leave unimplemented
    VeriSequentialInstantiation(const VeriSequentialInstantiation &) ;            // Purposely leave unimplemented
    VeriSequentialInstantiation& operator=(const VeriSequentialInstantiation &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;
    virtual void                SetLibrary(VeriLibrary *lib) ;
    virtual void                ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope) ;

    virtual void                AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods) ; // Insert instantiated module name in the argument specific set

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildInstIdInternal(VeriInstId *old_inst, VeriInstId *new_inst, unsigned delete_old_node) ; // Delete 'old_inst' from instance list, and puts new instance in its place

    // Internal routines for Static Elaboration :
    virtual unsigned            StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /*top_mod_nodes*/, Set * /*defparams*/) { return 0 ; }
    void                        BreakInstantiationAndPopulateArray(Array * /*arr*/) const { }
    virtual void                ProcessModuleItem(VeriScope * /*container_scope*/, unsigned & /*is_prameterized_construct*/) { }
    virtual void                ElaboratePortConnections(VeriScope * /*scope_of_instantiation*/) { }
    void                        SetModuleName(const char * /*name*/) const { } // Set (change) the name of the instantiated module :
    virtual void                StaticReplaceConstantExpr(unsigned /*is_const_index_context*/) { } // Replace range bounds, default value, bit/part/slice expressions
    virtual void                ConvertToMultipleSingleItems(Array * /*list*/, VeriModuleItem * /*container*/) { } // Convert comma separated module items to multiple single items

    VhdlComponentInstantiationStatement*   ConvertInstance(VhdlInstantiatedUnit * /*instantiated_unit*/, const VeriInstId * /*module_instId*/) const { return 0 ; } //Convert to Vhdl Instance

protected:
    VeriName        *_module_name ;     // unresolved instantiated module name (resolving to VeriIdDef* happens during elaboration).
    Array            _insts ;           // Array of VeriInstId*. The instantiation(s) itself.
// Verific internal use:
    VeriLibrary     *_library ;         // The library in which the module should be found (uselib/-v/-y). If 0, we look in the work library.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriSequentialInstantiation

/* -------------------------------------------------------------- */

/*
   VeriAssertion implements the (immediate) 'assert' statement in System Verilog,
   as well as the concurrent 'assert property' and 'cover property' statements.
   It contains a 'property_spec' (an expression) which is tested,
   and a 'action_block' : a 'if' statement and a 'then' statement.
   The two statements are available directly in this class
*/
class VFC_DLL_PORT VeriAssertion : public VeriStatement
{
public:
    // 'assert_cover_property' is set to 0 for immediate assertion statement,
    // and set to 'VERI_PROPERTY' of 'VERI_COVER' or VERI_ASSUME for concurrent 'assert property' and 'cover property' statements
    // and set to VERI_EXPECT for an 'expect_property_statement'.
    VeriAssertion(unsigned assert_cover_property, unsigned final_property, VeriExpression *property_spec, VeriStatement *if_stmt, VeriStatement *else_stmt, unsigned deferred=0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const              { return ID_VERIASSERTION; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;
    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsDeferredAssertion() const     { return _is_deferred ; }
    virtual unsigned            IsConcurrentAssertion() const ; // Return 1 for concurrent assertion statement
    // Accessor methods
    unsigned                    GetAssertCoverProperty() const  { return _assert_cover_property ; } // Returns the assertion token: VERI_ASSERT, VERI_COVER, VERI_ASSUME, VERI_RESTRICT or VERI_EXPECT
    unsigned                    GetFinalSequenceProperty() const{ return _final_property ; } // VIPER #6372: Returns the second token of the assert statement: VERI_PROPERTY, VERI_FINAL, VERI_SEQUENCE or 0 for immediate assertion
    VeriExpression *            GetPropertySpec() const         { return _property_spec ; } // The property (expression) which is tested.
    VeriStatement *             GetThenStmt() const             { return _then_stmt ; }     // The statement executed if the property holds
    VeriStatement *             GetElseStmt() const             { return _else_stmt ; }     // The statement executed if the property does not hold
    virtual VeriExpression *    GetSvaClkExpr() const           { return _sva_clock_expression ; } // The resolved clock expression
    void                        SetSvaClkExpr(VeriExpression *s) { _sva_clock_expression = s ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ;  // Copy assertion statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriAssertion(const VeriAssertion &assert, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriAssertion(SaveRestore &save_restore) ;

    virtual ~VeriAssertion() ;

private:
    // Prevent compiler from defining the following
    VeriAssertion() ;                                 // Purposely leave unimplemented
    VeriAssertion(const VeriAssertion &) ;            // Purposely leave unimplemented
    VeriAssertion& operator=(const VeriAssertion &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                SetIsImmediateAssertion()       { _is_immediate = 1 ; }
    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is property specification, and puts new expression to be tested in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' from true/false statement, and puts new statement in its place.

    // Static Elaboration :
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned        _assert_cover_property:31 ; // Assertion token: VERI_ASSERT, VERI_COVER, VERI_ASSUME, VERI_RESTRICT or VERI_EXPECT
    unsigned        _is_immediate:1 ; // Flag to mark immediate assertion
    unsigned        _final_property:31 ; // Assertion property token: VERI_PROPERTY, VERI_FINAL, VERI_SEQUENCE or 0 for immediate assertion
    unsigned        _is_deferred:1 ;  // Flag to mark deferred assertion
    VeriExpression *_property_spec ; // The property (expression) which is tested.
    VeriStatement  *_then_stmt ;     // The statement executed if the property holds
    VeriStatement  *_else_stmt ;     // The statement executed if the property does not hold
    VeriExpression *_sva_clock_expression ; // Viper 6456: Store the sva clock expression
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriAssertion

/* -------------------------------------------------------------- */

/*
   VeriJumpStatement implements the Verilog statements 'return', 'continue' and 'break'.
   They are distinguished via a token.
   Later, might create derived classes for each (to save memory and be more C++ compliant).
*/
class VFC_DLL_PORT VeriJumpStatement : public VeriStatement
{
public:
    VeriJumpStatement(unsigned jump_token, VeriExpression *expr) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIJUMPSTATEMENT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    unsigned                    GetJumpToken() const        { return _jump_token ; } // VERI_RETURN, VERI_BREAK or VERI_CONTINUE
    VeriExpression *            GetExpr() const             { return _expr ; }       // For 'return' statement : the returned expression
    VeriIdDef *                 GetJumpId() const           { return _jump_id ; }    // Backpointer to id (loop label or subprogram) to which this statement jumps

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy jump statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriJumpStatement(const VeriJumpStatement &jump_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriJumpStatement(SaveRestore &save_restore) ;

    virtual ~VeriJumpStatement() ;

private:
    // Prevent compiler from defining the following
    VeriJumpStatement() ;                                     // Purposely leave unimplemented
    VeriJumpStatement(const VeriJumpStatement &) ;            // Purposely leave unimplemented
    VeriJumpStatement& operator=(const VeriJumpStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr' if it is returned expression, and puts new expression in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *table, unsigned expect_nonconst) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    unsigned         _jump_token ;  // VERI_RETURN, VERI_BREAK or VERI_CONTINUE
    VeriExpression  *_expr ;        // For 'return' statement : the returned expression
// Set during Resolve :
    VeriIdDef       *_jump_id ;     // Backpointer to id (loop label or subprogran) to which this statement jumps.
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriJumpStatement

/* -------------------------------------------------------------- */

/*
   VeriDoWhile implement loop statement : "do <stmt> while <expr> ;"
*/
class VFC_DLL_PORT VeriDoWhile : public VeriLoop
{
public:
    VeriDoWhile(VeriStatement *stmt, VeriExpression *cond, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIDOWHILE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriExpression *            GetCondition() const        { return _cond ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy do while statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriDoWhile(const VeriDoWhile &do_while, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriDoWhile(SaveRestore &save_restore) ;

    virtual ~VeriDoWhile() ;

private:
    // Prevent compiler from defining the following
    VeriDoWhile() ;                               // Purposely leave unimplemented
    VeriDoWhile(const VeriDoWhile &) ;            // Purposely leave unimplemented
    VeriDoWhile& operator=(const VeriDoWhile &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', if it is condition, and puts new condition in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst) ;
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_cond ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriDoWhile

/* -------------------------------------------------------------- */

/*
   VeriForeach implement loop statement :
   "foreach (<array_identifier>[<loop_variables>]) <stmt> "
*/
class VFC_DLL_PORT VeriForeach : public VeriLoop
{
public:
    VeriForeach(VeriName *array_name, Array *loop_indexes, VeriStatement *stmt, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIFOREACH; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    VeriName *                  GetArrayName() const        { return _array ; }
    Array *                     GetLoopIndexes() const      { return _loop_indexes ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy foreach statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriForeach(const VeriForeach &foreach, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriForeach(SaveRestore &save_restore) ;

    virtual ~VeriForeach() ;

private:
    // Prevent compiler from defining the following
    VeriForeach() ;                               // Purposely leave unimplemented
    VeriForeach(const VeriForeach &) ;            // Purposely leave unimplemented
    VeriForeach& operator=(const VeriForeach &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', if it is array name, and puts new name in its place.

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst) ;

protected:
    // Internal routine to iterate over dimensions recursively:
    unsigned                    IterateDimension(ValueTable *table, VeriIdDef *array_id, unsigned dimension, unsigned expect_nonconst) ;

protected:
    VeriName *_array ; // Reference to array identifier to be traverse
    Array    *_loop_indexes ; // Array of loop index variables (array of VeriIdDefs)
    Array    *_implicit_decl_list ; // VIPER #4649 / 4703 : list of implicit declarations for array index (list of VeriDataDecl)
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriForeach

/* -------------------------------------------------------------- */

/*
   VeriWaitOrder implement wait statement :
   "wait_order ( <hierarchical_identifier {, hierarchical_identifier}>) <action_block> "
   action_block :
       <statement_or_null>
     | [ statement] else <statement>
*/
class VFC_DLL_PORT VeriWaitOrder : public VeriStatement
{
public:
    VeriWaitOrder(Array *hier_refs, VeriStatement *pass_stmt, VeriStatement *fail_stmt) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIWAITORDER; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    Array *                     GetEvents() const           { return _events ; }
    VeriStatement *             GetThenStmt() const         { return _then_stmt ; }
    VeriStatement *             GetElseStmt() const         { return _else_stmt ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy foreach statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriWaitOrder(const VeriWaitOrder &wait_order, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriWaitOrder(SaveRestore &save_restore) ;

    virtual ~VeriWaitOrder() ;

private:
    // Prevent compiler from defining the following
    VeriWaitOrder() ;                                 // Purposely leave unimplemented
    VeriWaitOrder(const VeriWaitOrder &) ;            // Purposely leave unimplemented
    VeriWaitOrder& operator=(const VeriWaitOrder &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', if it is event name, and puts new name in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' if it is true/false stmt and puts 'new_stmt' in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable *eval, unsigned expect_nonconst) ;

protected:
    Array         *_events ; // List of events to be triggered in order (array of VeriName*)
    VeriStatement *_then_stmt ; // Statement to be executed if events triggered in prescribed order
    VeriStatement *_else_stmt ; // Statement to be executed if events not triggered in prescribed order
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriWaitOrder

/* -------------------------------------------------------------- */

/*
   Std 1800 addition for modeling 'with' construct
   The following flavors of 'with' constructs call occur in design
    "<tf_call> with ( <expression>)"
    "<tf_call> with <constraint_block>"
   Left hand side of 'with' keyword in the above two flavors can be represented
   by a statement, but right hand side can be expression or array of expressions (constraint block).
   To represent this by a single class we need three fields
   VeriStatement *_left, VeriExpression *_right, Array *_constraint_exprs
   but one field will be null always (either _right or _constraint_exprs)
   Proper utilization of memory can be done if we define a abstract class 'VeriWithStmt'
   and derive two classes from it (one with expression and other with array as field)

   Abstract class VeriWithStmt to represent 'with' construct
*/
class VFC_DLL_PORT VeriWithStmt : public VeriStatement
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ;

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) = 0 ;

    // Accessor methods
    VeriStatement *             GetLeft() const             { return _left ; } // Left hand side of 'with'
    virtual Array *             GetConstraintBlock() const  { return 0 ; } // Return constraint block if right hand side is constraint block (inline constraint)
    virtual VeriExpression *    GetRight() const            { return 0 ; } // Return expression if right hand side of 'with' is expression
    virtual VeriScope *         GetScope() const            { return _scope ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const = 0 ; // Copy foreach statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    explicit VeriWithStmt(VeriStatement *left) ;

    VeriWithStmt(const VeriWithStmt &with_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriWithStmt(SaveRestore &save_restore) ;

public:
    virtual ~VeriWithStmt() ;

private:
    // Prevent compiler from defining the following
    VeriWithStmt() ;                                // Purposely leave unimplemented
    VeriWithStmt(const VeriWithStmt &) ;            // Purposely leave unimplemented
    VeriWithStmt& operator=(const VeriWithStmt &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriStatement *_left ; // Statement used as left hand side of 'with' keyword
    VeriScope      *_scope ; // Scope local to this array method call
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriWithStmt
/* -------------------------------------------------------------- */

/*
   VeriArrayMethodCall implements array method call with'with' expression
    "<tf_call> with ( <expression>)"
*/
class VFC_DLL_PORT VeriArrayMethodCall : public VeriWithStmt
{
public:
    VeriArrayMethodCall(VeriStatement *left, VeriExpression *right) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIARRAYMETHODCALL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual VeriExpression *    GetRight() const            { return _right ; } // Return expression if right hand side of 'with' is expression

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy foreach statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriArrayMethodCall(const VeriArrayMethodCall &method_call, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriArrayMethodCall(SaveRestore &save_restore) ;

    virtual ~VeriArrayMethodCall() ;

private:
    // Prevent compiler from defining the following
    VeriArrayMethodCall() ;                                       // Purposely leave unimplemented
    VeriArrayMethodCall(const VeriArrayMethodCall &) ;            // Purposely leave unimplemented
    VeriArrayMethodCall& operator=(const VeriArrayMethodCall &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' and puts 'new_stmt' in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable * /*eval*/, unsigned /*expect_nonconst*/) { return 0 ; }

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriExpression *_right ; // Right hand side of 'with' keyword
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriArrayMethodCall

/* -------------------------------------------------------------- */

/*
   VeriInlineConstraintStmt implements the inline constraints
    "<tf_call> with <constraint_block>"
*/
class VFC_DLL_PORT VeriInlineConstraintStmt : public VeriWithStmt
{
public:
    VeriInlineConstraintStmt(VeriStatement *left, Array *constraint_block) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERIINLINECONSTRAINTSTMT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VeriVarUsageInfo* info, unsigned action) ;

    // Accessor methods
    virtual Array *             GetConstraintBlock() const  { return _constraint_block ; } // Return constraint block (right hand side of 'with' keyword)

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy foreach statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriInlineConstraintStmt(const VeriInlineConstraintStmt &constraint_stmt, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriInlineConstraintStmt(SaveRestore &save_restore) ;

    virtual ~VeriInlineConstraintStmt() ;

private:
    // Prevent compiler from defining the following
    VeriInlineConstraintStmt() ;                                             // Purposely leave unimplemented
    VeriInlineConstraintStmt(const VeriInlineConstraintStmt &) ;            // Purposely leave unimplemented
    VeriInlineConstraintStmt& operator=(const VeriInlineConstraintStmt &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExprInternal(VeriExpression *old_expr, VeriExpression *new_expr, unsigned delete_old_node) ; // Delete 'old_expr', and puts new expr in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' and puts 'new_stmt' in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable * /*eval*/, unsigned /*expect_nonconst*/) { return 0 ; }

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array *_constraint_block ; // Right hand side of 'with' keyword : constraint block : array of VeriExpression*
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriInlineConstraintStmt

/* -------------------------------------------------------------- */

/*
   Std 1800 modeling for randsequence statement
   VeriRandsequence implements randsequence statement :
   "randsequence ([ <production_identifier>])
       <production> { <production>}
    endsequence"
*/
class VFC_DLL_PORT VeriRandsequence : public VeriStatement
{
public:
    VeriRandsequence(VeriName *start_prod, Array *productions, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const            { return ID_VERIRANDSEQUENCE; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    VeriName *                  GetStartingProduction() const { return _start_prod ; }
    Array *                     GetProductions() const        { return _productions ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy randsequence statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriRandsequence(const VeriRandsequence &randsequence, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriRandsequence(SaveRestore &save_restore) ;

    virtual ~VeriRandsequence() ;

private:
    // Prevent compiler from defining the following
    VeriRandsequence() ;                                    // Purposely leave unimplemented
    VeriRandsequence(const VeriRandsequence &) ;            // Purposely leave unimplemented
    VeriRandsequence& operator=(const VeriRandsequence &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildNameInternal(VeriName *old_name, VeriName *new_name, unsigned delete_old_node) ; // Delete 'old_name', if it is event name, and puts new name in its place.
    virtual unsigned            ReplaceChildProductionInternal(VeriProduction *old_prod, VeriProduction *new_prod, unsigned delete_old_node) ; // Delete 'old_prod' and puts 'new_prod' in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable * /*eval*/, unsigned /*expect_nonconst*/) {return 0 ;}

    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    VeriName  *_start_prod ;  // Name of the production from which sequence starts
    Array     *_productions ; // List of productions (Array of VeriProduction*)
    VeriScope *_scope ;       // Scope created by randsequence statement
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriRandsequence

/* -------------------------------------------------------------- */

/*
   Std 1800 modeling for randsequence code block
   VeriCodeBlock implements code block of production  :
    { { <data_declarations>} { statement_or_null}}
*/
class VFC_DLL_PORT VeriCodeBlock : public VeriStatement
{
public:
    VeriCodeBlock(Array *decls, Array *stmts, VeriScope *scope) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const          { return ID_VERICODEBLOCK; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ;

    // Accessor methods
    Array *                     GetDecls() const            { return _decls ; }
    Array *                     GetStmts() const            { return _stmts ; }
    virtual VeriScope *         GetScope() const            { return _scope ; }

    // Parse tree copy routine :
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &id_map_table) const ; // Copy code block statement

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriCodeBlock(const VeriCodeBlock &code_block, VeriMapForCopy &id_map_table) ; // Parse tree copy specific constructor

// Persistence (Binary restore via SaveRestore class)
    explicit VeriCodeBlock(SaveRestore &save_restore) ;

    virtual ~VeriCodeBlock() ;

private:
    // Prevent compiler from defining the following
    VeriCodeBlock() ;                                 // Purposely leave unimplemented
    VeriCodeBlock(const VeriCodeBlock &) ;            // Purposely leave unimplemented
    VeriCodeBlock& operator=(const VeriCodeBlock &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildModuleItemInternal(VeriModuleItem *old_item, VeriModuleItem *new_item, unsigned delete_old_node) ; // Delete 'old_item', and puts new item in its place.
    virtual unsigned            ReplaceChildStmtInternal(VeriStatement *old_stmt, VeriStatement *new_stmt, unsigned delete_old_node) ; // Delete 'old_stmt' and puts 'new_stmt' in its place

    // Static Elaboration :
    virtual unsigned            StaticElaborateStmt(ValueTable * /*eval*/, unsigned /*expect_nonconst*/) {return 0 ;}
    virtual void                StaticReplaceConstantExpr(unsigned is_const_index_context) ; // Replace range bounds, default value, bit/part/slice expressions

protected:
    Array     *_decls ; // List of declarations (array of VeriModuleItem*)
    Array     *_stmts ; // List of statements (array of VeriStatement*)
    VeriScope *_scope ; // Scope created by randsequence statement
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriCodeBlock

/* -------------------------------------------------------------- */

// VIPER #8419: Added the following class for null statements (has only ";")
class VFC_DLL_PORT VeriNullStatement : public VeriStatement
{
public:
    VeriNullStatement() ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const { return ID_VERINULLSTATEMENT; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual void                PrettyPrintWOSemi(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope* /*scope*/, veri_environment /*environment*/) ;
    virtual unsigned            IsNullStatement() const    { return 1 ; }

    // Copy null statement
    virtual VeriStatement *     CopyStatement(VeriMapForCopy &old2new) const ;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    // Copy tree node
    VeriNullStatement(const VeriNullStatement &node, VeriMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VeriNullStatement(SaveRestore &save_restore);

    virtual ~VeriNullStatement() ;

private:
    // Prevent compiler from defining the following
    VeriNullStatement(const VeriNullStatement &) ;            // Purposely leave unimplemented
    VeriNullStatement& operator=(const VeriNullStatement &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Static Elaboration
    virtual unsigned            StaticElaborateStmt(ValueTable* /*eval*/, unsigned /*expect_nonconst*/) { return 0 ; }

protected:
// Leaf node
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriNullStatement

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_STATEMENT_H_
