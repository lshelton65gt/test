/*
 *
 * [ File Version : 1.157 - 2014/02/27 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VeriTreeNode.h"

#include "veri_file.h"
#include "VeriUtil_Stat.h"
#include "veri_tokens.h"
#include "VeriBaseValue_Stat.h"
#include "VeriMisc.h"
#include "VeriExpression.h"
#include "VeriModuleItem.h"
#include "VeriModule.h"
#include "VeriConstVal.h"
#include "VeriStatement.h"
#include "VeriId.h"
#include "Strings.h"
#include "VeriScope.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

unsigned
VeriParamId::GetPackedWidth(int *msb, int *lsb)
{
    verific_int64 msb64 = 0 ;
    verific_int64 lsb64 = 0 ;
    unsigned width = (unsigned) GetPackedWidth(&msb64, &lsb64) ;

    *msb = (int) msb64 ;
    *lsb = (int) lsb64 ;
    return width ;
}

verific_uint64
VeriParamId::GetPackedWidth(verific_int64 *msb, verific_int64 *lsb)
{
    // VIPER #6011 : Check for recursion
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    verific_uint64 size = 1 ; // By default, size is 1
    VeriRange *range = _data_type ? _data_type->GetDimensions() : 0 ;
    if (!range) {
        *msb = 0 ;
        *lsb = 0 ;
    } else {
        unsigned is_multi_dim = 0 ;
        while (range) {
            // Calculate the size here
            size = size * range->GetWidth(msb, lsb, 0) ;
            range = range->GetNext() ;
            if (range) is_multi_dim = 1 ;
        }
        if (is_multi_dim) {
            // Only set msb and lsb if it has multiple dimensions, otherwise
            // they are set from the range->GetWidth(msb, lsb, 0) call.
            *msb = (verific_int64)size - 1 ;
            *lsb = 0 ;
        }
    }
    ClearIsProcessing() ;
    return size ;
}

verific_int64 VeriConstVal::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{
    // VIPER #4670 & #4678: Empty string ("") should have size 8.
    verific_int64 ret_size = (!_size && _is_string) ? 8 : (verific_int64)_size ;
    return (_is_signed) ? -ret_size : ret_size ;
}

verific_int64 VeriIntVal::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{
    verific_int64 ret_size = sizeof(int)*8 ;
    return -ret_size ;
}

verific_int64 VeriRealVal::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{
    verific_int64 ret_size = sizeof(double)*8 ;
    return -ret_size ;
}
verific_int64 VeriTimeLiteral::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{
    return sizeof(double)*8 ;
}
verific_int64 VeriOperatorId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (!_tree) return 0 ; // No back pointer to operator binding
    // Operator id is associated with a function prototype, return type of that
    // function is the type of this operator and size of return type of that
    // function is the size of this particular operator

    // Get Return type of associated function prototype
    VeriDataType *ret_type = _tree->GetDataType() ;

    verific_int64 ret_size = 0 ;
    // Get size of return type
    if (ret_type) ret_size = ret_type->StaticSizeSignInternal(tab, target_context) ;
    return ret_size ;
}
verific_int64 VeriIndexedExpr::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // Size of this index-expr is the size of the actual indexing, sign is always 0:
    return (_idx) ? _idx->StaticSizeSignInternal(tab, target_context) : 0 ;
}

verific_int64 VeriIdRef::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (!_id) return 0 ;
    return _id->StaticSizeSignInternal(tab, target_context) ;
}

verific_int64 VeriIndexedId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    if (!_prefix || !_id || !_idx) return 0 ;

    // Get the size of the prefix
    verific_int64 size = _prefix->StaticSizeSignInternal(tab, 0) ;
    size = ABS(size) ;

    // Get the most varying dimension - this one is selected
    VeriRange *range = _id->GetDimensionAt(0) ;

    verific_int64 msb = 0, lsb = 0 ;
    // There is a valid range
    if (range) {
        // Calculate the width of the range
        verific_int64 elems_in_range = (verific_int64)range->GetWidth(&msb, &lsb, tab) ;
        // The size is whole size divided by number of elements in this range
        if (elems_in_range) size = size / elems_in_range ;
    } else {
        // It is a scalar, has no dimension (packed or unpacked). So size is 1
        size = 1 ;
    }

    // It can be a part select, so check that
    if (_idx->IsRange()) {
        // Size is multiplied by the size of the part select
        verific_int64 idx_ss = _idx->StaticSizeSignInternal(tab, 0) ;
        size = size * ABS(idx_ss) ;
    }

    // Get the absolute value of the size, select is, normally, unsigned
    size = ABS(size) ;

    // If the id has unpacked dimensions then we must check the sign of the data type.
    // If it is signed, the selected id should also be signed.
    // For example: int i [3:0]; i[3] should be signed. but i[2:1] should not be signed.
    if ((!_idx->IsRange()) && _id->UnPackedDimension()) {
        // So, it has unpacked dimentions, get the data type
        VeriDataType *id_dt = _id->GetDataType() ;
        unsigned sign = 0 ;
        // If the data type is signed, the select should also be so
        if (id_dt && id_dt->StaticSizeSignInternal(tab, 0) < 0) sign = 1 ;
        if (sign) size = -size ;
    }

    return size ;
}

verific_int64 VeriIndexedMemoryId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    if (!_id || !_prefix || !_indexes) return 0 ;

    // Get the size of its prefix - can be another select or ide-ref etc.
    verific_int64 size = _prefix->StaticSizeSignInternal(tab, 0) ;
    size = ABS(size) ;

    // Get the number dimensions we have to check to calculate its size
    unsigned remaining_dims = _indexes->Size() ;
    unsigned dim ;
    verific_uint64 elems_in_range ;
    verific_int64 msb = 0, lsb = 0 ;
    VeriRange *range ;
    for (dim=0; dim<remaining_dims; dim++) {
        range = _id->GetDimensionAt(dim) ;
        if (!range) break ; // finished checking all dimensions for this id
        // Get the number of elements in this dimension :
        elems_in_range = range->GetWidth(&msb, &lsb, tab) ;
        // The size is whole size divided by number of elements in this range
        if (elems_in_range) size = size / (verific_int64)elems_in_range ;
    }

    if (remaining_dims != dim) {
        // There are remaining dimensions, but no more range in prefix.
        // This must thus be indexing into a scalar. Size of that is always 1 bit
        size = 1 ;
    }

    // Now, finally, if the last index is actually a range, then this is a part-select.
    // Similar to IndexedId, part-select returns as many elements as there are elements in the range:
    VeriExpression *last_index = (VeriExpression*)_indexes->GetLast() ;
    if (last_index && last_index->IsRange()) {
        // Its a part-select. Elements selected is size of the part-select range.
        // We need to compute the size of the last range for this is a part-select
        verific_int64 last_index_ss = last_index->StaticSizeSignInternal(tab, 0) ;
        size = size * ABS(last_index_ss) ;
    }

    // Get the absolute value of the size, select is, normally, unsigned
    size = ABS(size) ;

    // If the id has unpacked dimensions and all the selections are
    // made in those unpacked dimentions then we must check the sign
    // of the data type. If it is signed, the selected id should also
    // be signed. For example: int i [3:0][2:0]; i[3][0] should be signed.
    // but i[0][2:1] should not be signed!
    // UnPackedDimension() must equal number of indices for the expression to
    // inherit datatype sign : Also check VeriIndexedMemoryId::Sign() (Rtl Elab)
    // if (last_index && (!last_index->IsRange()) && (_id->UnPackedDimension() >= _indexes->Size())) {
    if (last_index && (!last_index->IsRange()) && (_id->UnPackedDimension() == _indexes->Size())) {
        // Get the data type
        VeriDataType *id_dt = _id->GetDataType() ;
        unsigned sign = 0 ;
        // If the data type is signed, the select should also be so
        if (id_dt && id_dt->StaticSizeSignInternal(tab, 0) < 0) sign = 1 ;
        if (sign) size = -size ;
    }

    return size ;
}

verific_int64 VeriSelectedName::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (_suffix_id) return _suffix_id->StaticSizeSignInternal(tab, target_context) ;
    // It might be a SV built-in method call: return the size-sign of that:
    // VIPER #7906: Do not treat this as built-in method call if the prefix is
    // an instance even if the suffix is not resolved and _function_type is set.
    // VIPER #8227: Find size-sign for built-in functions.
    if (_prefix_id && _function_type && !_prefix_id->IsInst()) {
        switch (_function_type) {
        case VERI_METHOD_GETC              : return -8 ; // byte

        case VERI_METHOD_ATOI              : // fall-through
        case VERI_METHOD_ATOBIN            : // fall-through
        case VERI_METHOD_ATOOCT            : // fall-through
        case VERI_METHOD_ATOHEX            : // fall-through

        case VERI_METHOD_COMPARE           : // fall-through
        case VERI_METHOD_ICOMPARE          : // fall-through
        case VERI_METHOD_LEN               : // fall-through

        case VERI_METHOD_EXISTS            : // fall-through

        case VERI_METHOD_NUM               : // fall-through
        //case VERI_METHOD_SIZE              : // fall-through
        case VERI_SYS_CALL_SIZE            : // fall-through

        case VERI_METHOD_RANDOMIZE         : // fall-through
        case VERI_METHOD_RAND_MODE         : // fall-through
        case VERI_METHOD_CONSTRAINT_MODE   : return -32 ; // integer

        case VERI_METHOD_ATOREAL           : // fall-through
        //case VERI_METHOD_GET_COVERAGE      : // fall-through
        case VERI_SYS_CALL_GET_COVERAGE    : // fall-through
        case VERI_METHOD_GET_INST_COVERAGE : return -64 ; // real

        case VERI_METHOD_FIND              : // fall-through
        case VERI_METHOD_FIND_FIRST        : // fall-through
        case VERI_METHOD_FIND_LAST         : // fall-through
        case VERI_METHOD_MAX               : // fall-through
        case VERI_METHOD_MIN               : // fall-through
        case VERI_METHOD_UNIQUE            : // fall-through
        case VERI_METHOD_FIND_INDEX        : // fall-through
        case VERI_METHOD_FIND_FIRST_INDEX  : // fall-through
        case VERI_METHOD_FIND_LAST_INDEX   : // fall-through
        case VERI_METHOD_UNIQUE_INDEX      : // Returns queue: so size will be 0
                                             return 0 ;

        case VERI_METHOD_INDEX             : { // Size will be size of base type(if base type exists) or 32 for associative array and 32 for other arrays:
                                               verific_int64 element_size = 0 ;
                                               VeriTypeInfo *prefix_type = _prefix ? _prefix->CreateTypeWithoutError() : 0 ;
                                               if (prefix_type && prefix_type->IsAssociativeArrayType()) {
                                                   VeriTypeInfo *base_type = prefix_type ? prefix_type->BaseType() : 0 ;
                                                   unsigned sign = 0 ;
                                                   element_size = VeriNode::GetSizeOfBuiltInType(base_type ? base_type->Type() : VERI_INT, &sign) ;
                                                   if (sign) element_size = (-element_size) ;
                                               } else {
                                                   VeriTypeInfo *base_type = prefix_type ? prefix_type->BaseType() : 0 ;
                                                   unsigned sign = 0 ;
                                                   element_size = VeriNode::GetSizeOfBuiltInType(base_type ? base_type->Type() : VERI_INT, &sign) ;
                                                   if (sign) element_size = (-element_size) ;
                                               }
                                               delete prefix_type ;
                                               return element_size ;
                                             }

        case VERI_METHOD_SUM               : // fall-through
        case VERI_METHOD_PRODUCT           : // fall-through
        case VERI_METHOD_AND               : // fall-through
        case VERI_METHOD_OR                : // fall-through
        case VERI_METHOD_XOR               : { // Size will be size of the array element for array prefix.
                                               VeriConstraint *constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(0, 0): 0 ;
                                               verific_int64 element_size = 0 ;
                                               if (constraint && constraint->IsArrayConstraint()) {
                                                   VeriConstraint *element_constraint = constraint->ElementConstraint() ;
                                                   if (element_constraint) {
                                                       element_size = element_constraint->NumOfBits() ;
                                                       unsigned sign = element_constraint->IsSigned() ;
                                                       if (sign) element_size = (-element_size) ;
                                                   }
                                               } else {
                                                   element_size = constraint ? constraint->NumOfBits(): 0 ;
                                                   unsigned sign = constraint ? constraint->IsSigned() : 0 ;
                                                   if (sign) element_size = (-element_size) ;
                                               }
                                               delete constraint ;
                                               return element_size ;
                                             }

        case VERI_METHOD_FIRST             : // depends on enum type/associative array type
        case VERI_METHOD_LAST              : // depends on enum type/associative array type
        case VERI_METHOD_NEXT              : // depends on enum type/associative array type
        case VERI_METHOD_PREV              : { // Size will be the size of element if prefix is enumerated type and 32 for other types
                                               verific_int64 element_size = 0 ;
                                               VeriTypeInfo *prefix_type = _prefix ? _prefix->CreateTypeWithoutError() : 0 ;
                                               if (prefix_type && prefix_type->IsEnumeratedType()) {
                                                   element_size = _prefix ? _prefix->StaticSizeSignInternal(tab, 0) : 0 ;
                                               } else if (prefix_type && prefix_type->IsAssociativeArrayType()) {
                                                   VeriTypeInfo *base_type = prefix_type ? prefix_type->BaseType() : 0 ;
                                                   unsigned sign = 0 ;
                                                   element_size = VeriNode::GetSizeOfBuiltInType(base_type ? base_type->Type() : VERI_INT, &sign) ;
                                                   if (sign) element_size = (-element_size) ;
                                               }
                                               delete prefix_type ;
                                               return element_size ;
                                             }

        case VERI_METHOD_POP_BACK          : // fall-through
        case VERI_METHOD_POP_FRONT         : { // Size will be the size of the element of queue
                                               verific_int64 element_size = 0 ;
                                               VeriConstraint *constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(0, 0) : 0 ;
                                               VeriConstraint *element_constraint = constraint ? constraint->ElementConstraint() : 0 ;
                                               if (element_constraint) {
                                                   element_size = element_constraint ? element_constraint->NumOfBits() : 0 ;
                                                   unsigned sign = element_constraint->IsSigned() ;
                                                   if (sign) element_size = (-element_size) ;
                                               }
                                               delete constraint ;
                                               return element_size ;
                                              }

        case VERI_METHOD_NEW               :  // Size will be the size of prefix
                                              return _prefix ? _prefix->StaticSizeSignInternal(tab, 0) : 0 ;

        case VERI_METHOD_TOLOWER           : // fall-through
        case VERI_METHOD_TOUPPER           : // fall-through
        case VERI_METHOD_SUBSTR            : // fall-through
        case VERI_METHOD_NAME              : // fall-through
        case VERI_METHOD_GET_RANDSTATE     : // string type: return 1
                                               return 1 ;

        case VERI_METHOD_BINTOA            : // task: fall-through
        case VERI_METHOD_OCTTOA            : // task: fall-through
        case VERI_METHOD_HEXTOA            : // task: fall-through
        case VERI_METHOD_ITOA              : // task: fall-through
        case VERI_METHOD_REALTOA           : // task: fall-through
        case VERI_METHOD_PUTC              : // task: fall-through
        case VERI_METHOD_AWAIT             : // task: fall-through
        case VERI_METHOD_KILL              : // task: fall-through
        case VERI_METHOD_RESUME            : // task: fall-through
        case VERI_METHOD_SUSPEND           : // task: return 0
                                             return 0 ;

        case VERI_METHOD_CLEAR             : // void: fall-through
        case VERI_METHOD_DELETE            : // void: fall-through
        case VERI_METHOD_ERASE             : // void: fall-through
        case VERI_METHOD_ERASE_RANGE       : // void: fall-through

        case VERI_METHOD_POST_RANDOMIZE    : // void: fall-through
        case VERI_METHOD_PRE_RANDOMIZE     : // void: fall-through
        case VERI_METHOD_PURGE             : // void: fall-through
        case VERI_METHOD_PUSH_BACK         : // void: fall-through
        case VERI_METHOD_PUSH_FRONT        : // void: fall-through

        case VERI_METHOD_SET               : // void: fall-through
        case VERI_METHOD_SET_INST_NAME     : // void: fall-through
        case VERI_METHOD_SET_RANDSTATE     : // void: fall-through

        case VERI_METHOD_START             : // void: fall-through
        //case VERI_METHOD_STOP              : // void: fall-through
        case VERI_SYS_CALL_STOP            : // void: fall-through
        case VERI_METHOD_SAMPLE            : // void: fall-through

        case VERI_METHOD_INSERT            : // void fall-through
        case VERI_METHOD_INSERT_RANGE      : // void fall-through

        case VERI_METHOD_SWAP              : // void: fall-through

        case VERI_METHOD_REVERSE           : // void: fall-through
        case VERI_METHOD_SORT              : // void: fall-through
        case VERI_METHOD_RSORT             : // void: fall-through
        case VERI_METHOD_SHUFFLE           : // void: fall-through
        case VERI_METHOD_STATUS            : // state type enum (integer?)
                                             return 0 ;

        case VERI_METHOD_BACK              : // templated
        case VERI_METHOD_DATA              : // templated
        case VERI_METHOD_FRONT             : // templated
        //case VERI_METHOD_FINISH            : // templated
        case VERI_SYS_CALL_FINISH          : // templated

        case VERI_METHOD_EMPTY             : // fall-through
        case VERI_METHOD_EQ                : // fall-through
        case VERI_METHOD_NEQ               : // fall-through

        case VERI_METHOD_PROCESS           : // chandle?
        case VERI_METHOD_SELF              : // chandle?
        case VERI_METHOD_SIE               : // typo?
        case VERI_METHOD_ENDED             : // ?
        case VERI_METHOD_MATCHED           : // ?
        case VERI_METHOD_TRIGGERED         : // ?
                                             return 0 ;

        default: break ;
        }
        return _prefix_id->StaticSizeSignInternal(tab, 0) ;
    }
    // When both _suffix_id && _suffix is null, it means this hierarchical name
    // is converted to simple name as a result of static elaboration of generate
    if (!_suffix && _prefix) return _prefix->StaticSizeSignInternal(tab, 0) ;
    return 0 ;
}

verific_int64 VeriConcat::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64 size = 0 ;
    verific_int64 expr_size = 0 ;
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        expr_size = expr->StaticSizeSignInternal(tab, 0) ;
        size += ABS(expr_size) ;
    }
    return size ;
}

verific_int64 VeriMultiConcat::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64 size = 0 ;

    // VIPER #4872 (issue 2): Pass in the value table while evaluating repeat exprression:
    VeriBaseValue *val = _repeat->StaticEvaluateInternal(0,tab,0,0) ;
    if(!val) {
        _repeat->Error("constant expression required") ;
        return 0 ;
    }
    verific_int64 rep_cnt = val->Get64bitInteger() ;
    delete val ;
    if (rep_cnt < 0) return 0 ;

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        verific_int64 expr_ss = expr->StaticSizeSignInternal(tab, 0) ;
        size += ABS(expr_ss) ;
    }
    size = size * rep_cnt ;
    return size ;
}

// VIPER #5772: Return the size of conctenated stream:
verific_int64 VeriStreamingConcat::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64 size = 0 ;
    VeriExpression *expr ;
    // Return array of VeriExpressions for concat expressions:
    Array *exprs = GetExpressions() ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(exprs, i, expr) {
        if (!expr) continue ;
        // Create the type of the expression:
        VeriTypeInfo *type_info = expr->CheckType(0, VeriTreeNode::NO_ENV) ;
        // If class type variable is used in concatenated stream, then calculate its
        // size by calculating the sizes of its members:
        VeriTypeInfo *base_type = type_info ? type_info->BaseType() : 0 ;
        VeriIdDef *id = (base_type && base_type->IsNamedType()) ? base_type->GetNameId() : 0 ; // Get the type identifier
        if (id && id->IsClass()) {
            int ele_size = id->StaticSizeOfClassMembers(tab) ;
            // If expr is an array of class objects then find the width of the array
            // and calculate the size of the expr by multipling the total size of all
            // the class members with its width.
            verific_int64 expr_size = expr->StaticSizeSignInternal(tab, 0) ;
            // VIPER #7774 : Use correct value of pointer as the size of class object
            verific_int64 width = expr_size / GetSizeOfBuiltInType(VERI_CHANDLE) ;
            verific_int64 dim_width = ABS(width) ;
            verific_int64 actual_size = dim_width*ABS(ele_size) ;
            size = size + actual_size ;
        } else {
            // calculate size for non class type variables:
            verific_int64 expr_size = expr->StaticSizeSignInternal(tab, 0) ;
            size = size + ABS(expr_size) ;
        }
        delete type_info ;
    }
    return size ;
}

verific_int64 VeriFunctionCall::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    VeriIdDef *id = (_func_name) ? _func_name->GetId() : 0 ;
    // VIPER #5710 : Size of let expression :
    Array *ports = id ? id->GetPorts(): 0 ;
    unsigned i ;
    VeriIdDef *formal ;
    if (id && id->IsLetId()) { // This is let expression
        VeriModuleItem *let_decl = id->GetModuleItem() ;
        VeriScope *let_scope = let_decl ? let_decl->GetScope(): 0 ;
        // Associate actuals to formals to get proper size/sign
        VeriExpression *expr = 0 ;
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (!expr) continue ; // unconnected actual
            if (expr->NamedFormal()) {
                formal = (let_scope) ? let_scope->FindLocal(expr->NamedFormal()): 0 ;
                expr = expr->GetConnection() ;
            } else {
                if (i>= ((ports) ? ports->Size() : 0)) break ;
                formal = (ports && ports->Size() > i) ? (VeriIdDef*)ports->At(i) : 0 ;
            }
            if (!formal) continue ;
            formal->SetActual(expr) ;
        }
    }
    verific_int64 size_sign = 1 ;
    if (id) size_sign = id->StaticSizeSignInternal(tab, target_context) ; // return size of the function identifier
    // Try to return the size of the unresolved function name:
    if (!id && _func_name) size_sign = _func_name->StaticSizeSignInternal(tab, target_context) ;

    if (id && id->IsLetId()) { // This is let expression
        FOREACH_ARRAY_ITEM(ports, i, formal) {
            if (formal) formal->SetActual(0) ;
        }
    }
    return size_sign ;
}

verific_int64 VeriSystemFunctionCall::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64 size = 0 ;
    unsigned sign = 0 ;
    VeriExpression *arg = (_args) ? (VeriExpression*)_args->At(0): 0 ;

    switch (_function_type) {
    case VERI_SYS_CALL_SIGNED :
        size = (arg) ? arg->StaticSizeSignInternal(tab, 0): 0 ;
        size = ABS(size) ;
        sign = 1 ;
        break ;
    case VERI_SYS_CALL_UNSIGNED :
        size = (arg) ? arg->StaticSizeSignInternal(tab, 0): 0 ;
        size = ABS(size) ;
        sign = 0 ;
        break ;
    case VERI_SYS_CALL_TIME :
        size = 64 ;
        sign = 0 ;
        break ;
    case VERI_SYS_CALL_BITSTOREAL :
    case VERI_SYS_CALL_ITOR :
    case VERI_SYS_CALL_REALTIME :
        size = 64 ;
        sign = 1 ;
        break ;
    case VERI_SYS_CALL_STIME :
    case VERI_SYS_CALL_URANDOM :
    case VERI_SYS_CALL_URANDOM_RANGE :
        size = 32 ;
        sign = 0 ;
        break ;
    case VERI_SYS_CALL_RTOI :
    case VERI_SYS_CALL_RANDOM :
    // File I/O operations
    case VERI_SYS_CALL_FGETC: // returns a byte, but should hold -1 when error. Simulators return 32 bit
    case VERI_SYS_CALL_FGETS:
    case VERI_SYS_CALL_UNGETC:
    case VERI_SYS_CALL_FSCANF:
    case VERI_SYS_CALL_SSCANF:
    case VERI_SYS_CALL_FREAD:
    case VERI_SYS_CALL_FOPEN:
    case VERI_SYS_CALL_REWIND:
    case VERI_SYS_CALL_FTELL:
    case VERI_SYS_CALL_FSEEK:
    case VERI_SYS_CALL_FERROR:
    // Added other distribution functions for VIPER #3756
    // std P1364-Y2K section 17.9.3
    case VERI_SYS_CALL_DIST_CHI_SQUARE :
    case VERI_SYS_CALL_DIST_EXPONENTIAL :
    case VERI_SYS_CALL_DIST_NORMAL :
    case VERI_SYS_CALL_DIST_T :
    case VERI_SYS_CALL_DIST_UNIFORM :
    case VERI_SYS_CALL_DIST_ERLANG :
    case VERI_SYS_CALL_DIST_POISSON :
    case VERI_SYS_CALL_BITS :
    case VERI_SYS_CALL_DIMENSIONS :
    case VERI_SYS_CALL_LEFT :
    case VERI_SYS_CALL_RIGHT :
    case VERI_SYS_CALL_LOW :
    case VERI_SYS_CALL_HIGH :
    case VERI_SYS_CALL_INCREMENT :
    case VERI_SYS_CALL_LENGTH :
    case VERI_SYS_CALL_SIZE :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS : // SV Std 1800 addition
    case VERI_SYS_CALL_BITSTOSHORTREAL :
    case VERI_SYS_CALL_FEOF : // P1800-2009 addition : return non-zero or 0 value : assume signed (not sure)
        size = 32 ;
        sign = 1 ;
        break ;
    case VERI_SYS_CALL_SHORTREALTOBITS : // Returns [31:0]
        size = 32 ;
        sign = 0 ;
        break ;
    case VERI_SYS_CALL_REALTOBITS : // Returns [63:0]
        size = 64 ;
        sign = 0 ;
        break ;
    case VERI_SYS_CALL_ONEHOT :
    case VERI_SYS_CALL_ONEHOT0 :
    case VERI_SYS_CALL_ISUNKNOWN :
    case VERI_SYS_CALL_ROSE :
    case VERI_SYS_CALL_FELL :
    case VERI_SYS_CALL_STABLE :
    case VERI_SYS_CALL_FELL_GCLK            :
    case VERI_SYS_CALL_CHANGED_GCLK         :
    case VERI_SYS_CALL_CHANGED              :
    case VERI_SYS_CALL_ROSE_GCLK            :
    case VERI_SYS_CALL_STABLE_GCLK          :
    case VERI_SYS_CALL_RISING_GCLK          :
    case VERI_SYS_CALL_STEADY_GCLK          :
    case VERI_SYS_CALL_FALLING_GCLK         :
    case VERI_SYS_CALL_CHANGING_GCLK        :
        size = 1 ;
        sign = 0 ;
        break ;
    case VERI_SYS_CALL_SAMPLED :
    case VERI_SYS_CALL_PAST :
    case VERI_SYS_CALL_PAST_GCLK            :
    case VERI_SYS_CALL_COUNTONES :
    case VERI_SYS_CALL_FUTURE_GCLK          :
        size = 32 ;
        sign = 0 ;
        break ;
    // Moved these function to VERILOG_2000 mode
    // LRM section 17.11.1/17.11.2 Viper 6807
    case VERI_SYS_CALL_CLOG2 : // integer function
        size = 32 ;
        sign = 1 ;
        break ;
    case VERI_SYS_CALL_SIN :
    case VERI_SYS_CALL_COS :
    case VERI_SYS_CALL_TAN :
    case VERI_SYS_CALL_ASIN :
    case VERI_SYS_CALL_ACOS :
    case VERI_SYS_CALL_ATAN :
    case VERI_SYS_CALL_ATAN2 :
    case VERI_SYS_CALL_HYPOT :
    case VERI_SYS_CALL_SINH :
    case VERI_SYS_CALL_COSH :
    case VERI_SYS_CALL_TANH :
    case VERI_SYS_CALL_ASINH :
    case VERI_SYS_CALL_ACOSH :
    case VERI_SYS_CALL_ATANH :
    case VERI_SYS_CALL_LN : // real functions
    case VERI_SYS_CALL_LOG10 :
    case VERI_SYS_CALL_EXP :
    case VERI_SYS_CALL_SQRT :
    case VERI_SYS_CALL_POW :
    case VERI_SYS_CALL_FLOOR :
    case VERI_SYS_CALL_CEIL :
        size = 64 ; // result type read
        sign = 1 ;
        break ;
    case VERI_SYS_CALL_TYPENAME:
    {
        // VIPER #5248 (test60): Support $typename system function call:
        VeriBaseValue *val = StaticEvaluateInternal(0, tab, 0, 0) ;
        // This should be a string value, get the string:
        const char *str = (val) ? val->GetString() : 0 ;
        size = (verific_int64)Strings::len(str) * 8 ; // Multiply by 8 for 'char'
        delete val ;
        break ;
    }
    default:
        break ;
    }
    return (sign) ? (-size) : (size) ;
}

verific_int64 VeriMinTypMaxExpr::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    verific_int64 size = (_typ_expr) ? _typ_expr->StaticSizeSignInternal(tab, target_context) : 0 ;
    return size ;
}

verific_int64 VeriUnaryOperator::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    switch(_oper_type) {
    case VERI_PLUS:
    case VERI_MIN:
    case VERI_REDNOT:
        break ;
    default:
        // VIPER #5030 : Return 1 as size without calculating the operand size
        return 1 ;
    }
    verific_int64 size = 0 ;
    unsigned sign = 0 ;
    size = _arg ? _arg->StaticSizeSignInternal(tab, target_context) : 0 ;
    if (size == 0) return size ;
    if (size < 0) sign = 1 ;
    size = ABS(size) ;

    return (sign) ? (-size) : size ;
}

verific_int64 VeriBinaryOperator::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // VIPER #5030 : Do not calculate size of operands when size of operator
    // is known. Return size of all following operators is 1 irrespective of
    // size of operands.
    switch(_oper_type) {
    case VERI_LOGEQ:
    case VERI_LOGNEQ:
    case VERI_CASEEQ:
    case VERI_CASENEQ:
    case VERI_LOGAND:
    case VERI_LOGOR:
    case VERI_LT:
    case VERI_GT:
    case VERI_GEQ:
    case VERI_LEQ:
    case VERI_WILDEQUALITY:
    case VERI_WILDINEQUALITY:
    case VERI_LEADTO:
    case VERI_INSIDE:
    case VERI_COLON_EQUAL:
    case VERI_AT:
    case VERI_CYCLE_DELAY_OP:
    case VERI_DISABLE:
    case VERI_OVERLAPPED_IMPLICATION:
    case VERI_NON_OVERLAPPED_IMPLICATION:
    case VERI_OVERLAPPED_FOLLOWED_BY:
    case VERI_NON_OVERLAPPED_FOLLOWED_BY:
    case VERI_CONSECUTIVE_REPEAT:
    case VERI_NON_CONSECUTIVE_REPEAT:
    case VERI_GOTO_REPEAT:
    case VERI_OR:
    case VERI_AND:
    case VERI_INTERSECT:
    case VERI_THROUGHOUT:
    case VERI_WITHIN: // All SVA operators have size 1 ?
    // VIPER #5710 : VCS Compatibility issue (if_in_property), abort properties
    case VERI_ACCEPT_ON :
    case VERI_REJECT_ON :
    case VERI_SYNC_ACCEPT_ON :
    case VERI_SYNC_REJECT_ON :
    // VIPER #5710 : VCS Compatibility issue (iff_in_property)
    case VERI_IFF :
    case VERI_IMPLIES :
    case VERI_UNTIL :
    case VERI_S_UNTIL :
    case VERI_UNTIL_WITH :
    case VERI_S_UNTIL_WITH :
    case VERI_NEXTTIME :
    case VERI_S_NEXTTIME :
    case VERI_EVENTUALLY :
    case VERI_S_EVENTUALLY :
    case VERI_ALWAYS :
    case VERI_S_ALWAYS :
    case VERI_RIGHTARROW : // SV-2009 "->"
    case VERI_LOG_EQUIVALENCE : // SV-2009 "<->"
        return 1 ;
    default :
        break ;
    }
    verific_int64 size  = 0 ;
    verific_int64 lsize = 0 ;
    verific_int64 rsize = 0 ;
    unsigned sign = 0 ;

    lsize = (_left) ? _left->StaticSizeSignInternal(tab, target_context) : 0 ;
    unsigned lsign = (lsize < 0) ? 1 : 0 ;
    rsize = (_right) ? _right->StaticSizeSignInternal(tab, target_context) : 0 ;
    unsigned rsign = (rsize < 0) ? 1 : 0 ;
    lsize = ABS(lsize) ;
    rsize = ABS(rsize) ;

    switch(_oper_type) {
    case VERI_PLUS:
    case VERI_MIN:
    case VERI_MUL:
    case VERI_DIV:
    case VERI_MODULUS:
    case VERI_EDGE_AMPERSAND:
    case VERI_REDAND:
    case VERI_REDOR:
    case VERI_REDXOR:
    case VERI_REDNAND:
    case VERI_REDNOR:
    case VERI_REDXNOR:
        sign = (lsign && rsign) ? 1: 0 ;
        size = (lsize > rsize) ? lsize : rsize ;
        break ;
    case VERI_INC_OP:
    case VERI_DEC_OP:
        sign = (_left) ? lsign : ((_right) ? rsign : 0) ;
        size = (lsize > rsize) ? lsize : rsize ;
        break ;
    case VERI_POWER:
        // LRM has conflicts. If either operand is integer/real/signed,
        // expression should be real. It also says that size should be
        // size of left operand. Verilog2k 4.1.5 and table 4.21
    case VERI_LSHIFT:
    case VERI_RSHIFT:
    case VERI_ARITLSHIFT:
    case VERI_ARITRSHIFT:
        size = lsize ;
        sign = lsign ? 1: 0 ;
        break ;
    case VERI_PLUS_ASSIGN:
    case VERI_MIN_ASSIGN:
    case VERI_MUL_ASSIGN:
    case VERI_DIV_ASSIGN:
    case VERI_MOD_ASSIGN:
    case VERI_AND_ASSIGN:
    case VERI_OR_ASSIGN:
    case VERI_XOR_ASSIGN:
        sign = (lsign && rsign) ? 1 : 0 ;
        size = (lsize > rsize) ? lsize : rsize ;
        break ;
    case VERI_EQUAL_ASSIGN:
    case VERI_LSHIFT_ASSIGN:
    case VERI_ALSHIFT_ASSIGN:
    case VERI_RSHIFT_ASSIGN:
    case VERI_ARSHIFT_ASSIGN:
        size = lsize ;
        sign = lsign ? 1: 0 ;
        break ;
    default: // Don't know this operator: return 0?
        break ;
    }
    return (sign) ? (-size) : size ;
}

verific_int64 VeriQuestionColon::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    verific_int64 size = 0 ;
    verific_int64 lsize = 0 ;
    verific_int64 rsize = 0 ;
    unsigned lsign = 0 ;
    unsigned rsign = 0 ;

    lsize = (_then_expr) ? _then_expr->StaticSizeSignInternal(tab, target_context) : 0 ;
    if (lsize < 0) lsign = 1 ;
    rsize = (_else_expr) ? _else_expr->StaticSizeSignInternal(tab, target_context) : 0 ;
    if (rsize < 0) rsign = 1 ;
    lsize = ABS(lsize) ;
    rsize = ABS(rsize) ;

    // VIPER #6704: size of ?: operator is max size of its operands:
    //size = (lsize > rsize) ? lsize : rsize ;
    size = MAX(lsize, rsize) ;
    unsigned sign = (lsign & rsign) ? 1 : 0 ;
    return (sign) ? (-size) : (size) ;
}

verific_int64 VeriEventExpression::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    verific_int64 size = _expr ? _expr->StaticSizeSignInternal(tab, target_context) : 0 ;
    return size ;
}

verific_int64 VeriPortConnect::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    verific_int64 size = _connection ? _connection->StaticSizeSignInternal(tab, target_context): 0 ;
    return size ;
}

verific_int64 VeriPortOpen::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }

verific_int64 VeriAnsiPortDecl::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }

verific_int64 VeriTimingCheckEvent::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }

verific_int64 VeriDollar::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{
    // FIXME : Depends on context
    return 0 ;
}

verific_int64 VeriVariable::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    verific_int64 result = 1 ;
    verific_int64 size = 0 ;
    unsigned sign = 0 ;

    // If it has a data type, return the size/sign of the data type
    if (_data_type) {
        if (_data_type->Type() == VERI_STRINGTYPE) {
            // VIPER #3359 & #4755: It is a dynamic string type variable; check for current value:
            VeriBaseValue *val = (tab) ? tab->FetchValue(this) : 0 ;
            if (val) size = val->StaticSizeSign() ;
        } else {
            size = _data_type->StaticSizeSignInternal(tab, ((_dimensions)?0:target_context)) ;
        }
    } else {
        // It does not have a data type, check its type
        switch(_type) {
        case VERI_INTEGER  : result = result * 32 ; sign = 1 ; break ;
        case VERI_TIME     : result = result * 64 ; sign = 0 ; break ;
        case VERI_REAL     : result = result * 64 ; sign = 1 ; break ;
        case VERI_REALTIME : result = result * 64 ; sign = 1 ; break ;
        case VERI_INT      : result = result * 32 ; sign = 1 ; break ;
                            // It is probably a single bit variable
        default            : result = result * 1 ; sign = IsSigned() ; break ;
        }

        size = (sign) ? (-result) : (result) ;
    }
    if (!_dimensions) return size ;
    // Calculate the total number of elements of the unpacked dimensions
    // Just multiply the size of the dimensions
    verific_uint64 dim_size = 1 ;
    VeriRange *range = _dimensions ;
    verific_int64 msb = 0, lsb = 0 ;
    while (range) {
        dim_size = dim_size * range->GetWidth(&msb, &lsb, tab) ;
        range = range->GetNext() ;
    }

    // Multiply element size and dimensions size
    return size * (verific_int64)dim_size ;
}

verific_int64 VeriInstId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    // Now calculate width of it
    verific_uint64 dim_size = 1 ;
    VeriRange *range = _dimensions ;
    verific_int64 msb = 0, lsb = 0 ;
    while (range) {
        dim_size = dim_size * range->GetWidth(&msb, &lsb, tab) ;
        range = range->GetNext() ;
    }
    return (int)dim_size ;
}

verific_int64 VeriModuleId::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }

verific_int64 VeriUdpId::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }

verific_int64 VeriTaskId::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }

verific_int64 VeriFunctionId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    return VeriVariable::StaticSizeSignInternal(tab, target_context) ;
}

verific_int64 VeriGenVarId::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return -32 ; }

verific_int64 VeriParamId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // VIPER #6011 : Check for recursion
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriDataType *data_type = GetDataType() ; // Returns initial value of type parameter
    // If the parameter has a data type, size of the parameter will be size of that data type, otherwise
    // it will be the size of the initial value. Keep in mind that the data type can be only 'signed'.
    // In that case we use the size-sign of the initial value:
    verific_int64 size = -32 ;
    if (data_type && (data_type->Type() || data_type->GetDimensions()) &&
        (data_type->Type() != VERI_STRINGTYPE)) { // VIPER #4989: If data type is string look into initial value
        // VIPER #8436: Static size signed for enum literals will be the size sign of enum's base type:
        Map *enum_lits = data_type->IsEnumType() ? data_type->GetEnumLiterals(): 0 ;
        VeriIdDef *enum_id = enum_lits ? (VeriIdDef*)enum_lits->GetValue(Name()): 0 ;
        if (IsEqualObject(static_cast<VeriParamId *>(enum_id))) {
            VeriDataType *base_type = data_type->GetBaseType() ;
            size = base_type ? base_type->StaticSizeSignInternal(tab, target_context) : -32 ;
        } else {
            size = data_type->StaticSizeSignInternal(tab, ((_dimensions)?0:target_context)) ;
            // VIPER #4749: Count the unpacked dimensions too:
            verific_uint64 up_size = 1 ;
            VeriRange *range = _dimensions ;
            verific_int64 msb = 0, lsb = 0 ;
            while (range) {
                up_size = up_size * range->GetWidth(&msb, &lsb, tab) ;
                range = range->GetNext() ;
            }
            if (up_size) size = size * (verific_int64)up_size ;
        }
    } else if (!data_type && _dimensions && InRelaxedCheckingMode()) {
        // VIPER #8037 : No data type, only unpacked dimension, packed size 1
        verific_uint64 up_size = 1 ;
        VeriRange *range = _dimensions ;
        verific_int64 msb = 0, lsb = 0 ;
        while (range) {
            up_size = up_size * range->GetWidth(&msb, &lsb, tab) ;
            range = range->GetNext() ;
        }
        size = (verific_int64)up_size ;
    } else if (_initial_value) {
        size = _initial_value->StaticSizeSignInternal(tab, target_context) ;
    }

    // The initial value might not be negative but the id can be declared as 'signed'.
    // So, incorporate the sign information in the size here:
    if ((size > 0) && (data_type && (data_type->Sign()))) size = -size ;

    ClearIsProcessing() ;
    return size ;
}

verific_int64 VeriBlockId::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{ return 0 ; }
verific_int64 VeriSeqPropertyFormal::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    verific_int64 size_sign = 0 ;
    // If data type exists, return size from data type :
    if (_data_type) {
        size_sign = VeriVariable::StaticSizeSignInternal(tab, target_context) ;
    } else if (_actual) { // Otherwise size is determined from actual
        size_sign = _actual->StaticSizeSignInternal(tab, target_context) ;
    } else if (_initial_value) { // Otherwise, the size is the size of the initial value expression :
        size_sign = _initial_value->StaticSizeSignInternal(tab, target_context) ;
    }
    ClearIsProcessing() ;
    return size_sign ;
}
verific_int64 VeriNamedPort::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    return (_port_expr) ? _port_expr->StaticSizeSignInternal(tab, target_context): 0 ;
}
verific_int64 VeriLetId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (IsProcessing()) return 0 ; // recursion
    // If this is let identifier, size/sign is determined by its expression
    // If let declaration contains formal arguments without type specification
    // size/sign can be determined only if actuals are associated with formals
    SetIsProcessing() ;
    VeriExpression *expr = _tree ? _tree->GetExpression(): 0 ;
    verific_int64 size_sign = (expr) ? expr->StaticSizeSignInternal(tab, target_context) : 0 ;
    ClearIsProcessing() ;
    return size_sign ;
}
verific_int64 VeriPrototypeId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    return (_actual_id) ? _actual_id->StaticSizeSignInternal(tab, target_context): 0 ;
}
verific_int64 VeriRange::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64  size = 0 ;

    // Compute the size of the range, addcolon range and subcolon range
    // according to the node type, and insert the size in the Map.
    switch(_part_select_token) {
    case VERI_PARTSELECT_UP :
    case VERI_PARTSELECT_DOWN :
        {
            VeriBaseValue *rval = (_right) ? _right->StaticEvaluateInternal(0,tab,0,0) : 0 ;
            if (!rval) {
                if (_right) _right->Error("constant expression required") ;
                size = 0 ;
            } else {
                size = rval->GetIntegerValue() ;
                if (size <= 0) {
                    Error("part-select has a negative or zero size") ;
                }
                delete rval ;
            }
            break ;
        }
    default :
        {
            // VIPER #7509: _left can be non-constant in case it is +: or -: type range:
            VeriBaseValue *lval = (_left) ? _left->StaticEvaluateInternal(0,tab,0,((_part_select_token)?1:0)) : 0 ;
            VeriBaseValue *rval = (_right) ? _right->StaticEvaluateInternal(0,tab,0,0) : 0 ;
            if (lval && !_right) {
                // Its a SV [n] style range: size is the lvalue itself!
                // FIXME: what happens if someone uses [-3]?
                size = lval->GetIntegerValue() ;
                delete lval ;
                // if (_part_select_token==VERI_NONE && size < 0)  Error("part-select has a negative or zero size") ;
                break ;
            }
            if (!lval || !rval) {
                if (_right) _right->Error("constant expression required") ;
                size = 0 ;
            } else {
                verific_int64 lsize = lval->Get64bitInteger() ;
                verific_int64 rsize = rval->Get64bitInteger() ;
                if (InRelaxedCheckingMode()) {
                    // VIPER #7811: In relaxed checking mode, truncate/cast individual range bounds to 'int':
                    // Looks like other tools do the same, LRM does not mention anything, so we do it under compatibility mode:
                    lsize = (int)lsize ;
                    rsize = (int)rsize ;
                }
                size = (lsize > rsize) ? (lsize - rsize) + 1 : (rsize - lsize) + 1 ;
            }
            delete lval ;
            delete rval ;
            break ;
        }
    }
    return size ;
}

unsigned
VeriRange::GetWidth(int *msb, int *lsb, ValueTable *table) const
{
    verific_int64 msb64 = 0 ;
    verific_int64 lsb64 = 0 ;
    unsigned width = (unsigned) GetWidth(&msb64, &lsb64, table) ;

    *msb = (int) msb64 ;
    *lsb = (int) lsb64 ;

    // VIPER #7811: Do not depend on the returned width, it may be incorrect after casting to 'int' above, recalculate:
    if (!width && !msb64 && !lsb64) return 0 ; // Something went wrong!
    width = (*msb > *lsb) ? (unsigned)((*msb - *lsb) + 1) : (unsigned)((*lsb - *msb) + 1) ;

    return width ;
}

verific_uint64
VeriRange::GetWidth(verific_int64 *msb, verific_int64 *lsb, ValueTable *table) const
{
    verific_uint64 size = 0 ;
    // VIPER #2482, #3511: Get the index value from the table if it is already evaluated:
    VeriBaseValue *lval = (table) ? table->GetEvaluatedIndexValue(_left) : 0 ;
    // VIPER #7509: _left can be non-constant in case it is +: or -: type range:
    if (!lval && _left) lval = _left->StaticEvaluateInternal(0, table, 0, ((_part_select_token)?1:0)) ;
    // VIPER #2482, #3511: Insert the evaluated index value into the table:
    if (table) table->InsertEvaluatedIndexValue(_left, lval) ;
    // VIPER #3224 : Do not check for constant from evaluated value, already checked
    // in analysis
    if (lval && !CheckRangeBound(lval)) { // Check for constant / non-real bounds : (VIPER #2879)
        delete lval ;
        return 0 ;
    }
    // VIPER #2482, #3511: Get the index value from the table if it is already evaluated:
    VeriBaseValue *rval = (table) ? table->GetEvaluatedIndexValue(_right) : 0 ;
    if (!rval && _right) rval = _right->StaticEvaluateInternal(0, table, 0, 0) ;
    // VIPER #2482, #3511: Insert the evaluated index value into the table:
    if (table) table->InsertEvaluatedIndexValue(_right, rval) ;
    // VIPER #3224 : Do not check for constant from evaluated value, already checked
    // in analysis
    if (rval && !CheckRangeBound(rval)) { // Check for constant / non-real bounds : (VIPER #2879)
        delete lval ;
        delete rval ;
        return 0 ;
    }
    if (lval && !_right) {
        // Its a SV [n] style range: size is the lvalue itself!
        verific_int64 tmp_size = lval->Get64bitInteger() ;
        // As per SV LRM Std 1800 sec 5.2, only a single positive number can be
        // specified as the size of an unpacked array.
        if (_left && (!tmp_size || (tmp_size < 0))) _left->Warning("zero or negative value for size") ; // VIPER 2600
        // Assign the absolute value of 'tmp_size' to 'size' if 'tmp_size' is positive
        // or else, if 'tmp_size' is negative, then the range is [0:tmp_size-1] i.e.
        // 'size' becomes -tmp_size + 2.
        // If someone uses [-3] then store '5' as 'size' '0' as 'msb' and '-4' as 'lsb'.
        // VIPER #5396: Treat zero range [0] having size 2 since when it is elaborated
        // it becomes [0:0-1], ie, [0:-1] which has size 2. Other tools do the same:
        size = (verific_uint64) ((tmp_size <= 0) ? (-tmp_size + 2) : tmp_size) ;
        delete lval ;
        *msb =  0 ;
        *lsb = tmp_size - 1 ; // VIPER 2601 : As per SV LRM Std 1800 sec 5.2,
        // the size of an unpacked array like [size] is same as [0:size-1], i.e.
        // msb is '0' and lsb is 'size - 1'.
        return size ;
    }
    if(!lval || !rval) { // FIXME : Should not execute this code
        delete lval ;
        delete rval ;
        return 0 ;
    }
    verific_int64 lIVal = lval->Get64bitInteger() ;
    verific_int64 rIVal = rval->Get64bitInteger() ;
    if (InRelaxedCheckingMode()) {
        // VIPER #7811: In relaxed checking mode, truncate/cast individual range bounds to 'int':
        // Looks like other tools do the same, LRM does not mention anything, so we do it under compatibility mode:
        lIVal = (int)lIVal ;
        rIVal = (int)rIVal ;
    }
    delete lval ;
    delete rval ;
    switch(_part_select_token) {
    case 0:
        *msb = lIVal ;
        *lsb = rIVal ;
        break ;
    case VERI_PARTSELECT_UP:
        *msb = lIVal ;
        *lsb = lIVal + rIVal - 1 ;
        break ;
    case VERI_PARTSELECT_DOWN:
        *msb = lIVal ;
        *lsb = (lIVal - rIVal) + 1 ;
        break ;
    default:
        break ;
    }
    size = (verific_uint64)((*msb > *lsb) ? (*msb - *lsb) + 1 : (*lsb - *msb) + 1) ;
    return size ;
}

unsigned
VeriVariable::GetPackedWidth(int *msb, int *lsb)
{
    verific_int64 msb64 = 0 ;
    verific_int64 lsb64 = 0 ;
    unsigned width = (unsigned) GetPackedWidth(&msb64, &lsb64) ;

    *msb = (int) msb64 ;
    *lsb = (int) lsb64 ;
    return width ;
}

verific_uint64
VeriVariable::GetPackedWidth(verific_int64 *msb, verific_int64 *lsb)
{
    verific_uint64 size = 1 ;
    VeriRange *range = _data_type ? _data_type->GetDimensions() : 0 ;
    if (!range) {
        *msb = 0 ;
        *lsb = 0 ;
    } else {
        unsigned is_multi_dim = 0 ;
        while (range) {
            size = size * range->GetWidth(msb, lsb, 0) ;
            range = range->GetNext() ;
            if (range) is_multi_dim = 1 ;
        }
        if (is_multi_dim) {
            *msb = (int)size - 1 ;
            *lsb = 0 ;
        }
    }
    return size ;
}

verific_int64
VeriDataType::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64 result = 1 ;
    unsigned sign = 0 ;
    if (_type) {
        // VIPER #6640: Call the merged routine to get the size of the built-in type:
        result = (int)VeriNode::GetSizeOfBuiltInType(_type, &sign) ;
    }

    // Consider signedness for all cases
    if (_signing == VERI_SIGNED) sign = 1 ;
    if (_signing == VERI_UNSIGNED) sign = 0 ;
    if (result == 1) {
        // Single bit elements may have dimensions:
        VeriRange *range = _dimensions ;
        verific_int64 msb = 0, lsb = 0 ;
        while (range) {
            result = result * (verific_int64)range->GetWidth(&msb, &lsb, tab) ;
            range = range->GetNext() ;
        }
    }

    return (sign) ? (-result) : (result) ;
}
verific_int64
VeriNetDataType::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (_data_type) return _data_type->StaticSizeSignInternal(tab, target_context) ;

    return VeriDataType::StaticSizeSignInternal(tab, target_context) ;
}
verific_int64
VeriTypeRef::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // Start with the size of the referred type identifier
    verific_int64 result = (_id) ? _id->StaticSizeSignInternal(tab, target_context) : 1 ;

    // It may have some packed dimensions, check them
    VeriRange *range = _dimensions ;
    verific_int64 msb = 0, lsb = 0 ;
    while (range) {
        result = result * (verific_int64)range->GetWidth(&msb, &lsb, tab) ;
        range = range->GetNext() ;
    }

    // Return the final result
    return result ;
}

verific_int64
VeriStructUnion::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    verific_int64 result = 0 ; // start from size zero, no member? no size!
    verific_int64 elem_size ;
    VeriIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        elem_size = id->StaticSizeSignInternal(tab, 0) ;
        elem_size = ABS(elem_size) ;
        if (!elem_size) continue ;
        if (_type == VERI_STRUCT) {
            result = result + elem_size ;
        } else { // It must be VERI_UNION
            if (elem_size > result) result = elem_size ;
        }
    }

    // VIPER #6158: Take dimensions into consideration:
    if (_dimensions) {
        verific_uint64 dim_size = 1 ;
        VeriRange *range = _dimensions ;
        verific_int64 msb = 0, lsb = 0 ;
        while (range) {
            dim_size = dim_size * range->GetWidth(&msb, &lsb, tab) ;
            range = range->GetNext() ;
        }
        if (dim_size) result = result * (verific_int64)dim_size ;
    }

    // VIPER #4004 : Sign can be specified for packed structure
    return (Sign()) ? (-result) : result ; // For packed struct, union sign matters
}

verific_int64
VeriEnum::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // If it has dimensions or type (other than enum), we must call the StaticSizeSign of _base_type
    if (!_dimensions) return _base_type ? _base_type->StaticSizeSignInternal(tab, target_context) : -32 ;

    // VIPER #8436: If enum type has packed dimension take this dimension into consideration.
    verific_int64 result = 0 ; // start from size zero, no member? no size!
    verific_uint64 dim_size = 1 ;
    VeriRange *range = _dimensions ;
    verific_int64 msb = 0, lsb = 0 ;
    while (range) {
        dim_size = dim_size * range->GetWidth(&msb, &lsb, tab) ;
        range = range->GetNext() ;
    }
    if (dim_size) result = (verific_int64)dim_size ;

    // call this routine for its base type:
    if (_base_type) {
        return (result * (_base_type->StaticSizeSignInternal(tab, target_context))) ;
    }

    // Otherwise it is integer, return size/sign of that
    return -32 ;
}
verific_int64
VeriTypeOperator::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    return (_expr_or_data_type) ? _expr_or_data_type->StaticSizeSignInternal(tab, target_context) : 0 ;
}
verific_int64
VeriCast::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    if (!_target_type || !_expr) return 0 ; // don't know..

    // Get the size and sign of _expr
    verific_int64 expr_size = _expr->StaticSizeSignInternal(tab, target_context) ;
    verific_int64 expr_sign = (expr_size < 0) ? 1: 0 ;

    // The target_type can be a VeriDataType (for type-referencing),
    // or a data type with VERI_SIGNED/VERI_UNSIGNED as the type,
    // or it can be a VeriConstVal (for 'sized' cast).
    // These are separate styles of casts, so test them differently :
    if (_target_type->IsConst()) {
        // size-cast. Adjust the size of the returned value :
        // if size is negative, we already give error in VeriCast::StaticEvaluate
        int size = _target_type->Integer() ;
        return (expr_sign) ? -ABS(size) : ABS(size) ;
    } else if (_target_type->Type()==VERI_SIGNED) {
        // Make the value 'signed' :
        // size is determined by the expression :
        return (-1 * ABS(expr_size)) ;
    } else if (_target_type->Type()==VERI_UNSIGNED) {
        // Make the value 'unsigned' :
        // size is determined by the expression :
        return ABS(expr_size) ;
    } else if (_target_type->Type()==VERI_CONST) {
        // size/sign is determined by the expression :
        return expr_size ;
    } else {
        return _target_type->StaticSizeSignInternal(tab, target_context) ;
    }
    return 0 ;
}

verific_int64
VeriInterfaceId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    // Size of an interface if used as a type. Size is accumulation of sizes of the
    // locally declared variables. Interfaces are always unsigned as a whole.
    if (!_local_scope) return 0 ;

    verific_int64 result = 0 ;
    VeriIdDef *id ;
    Map *all_ids = _local_scope->DeclArea() ;
    MapIter mi ;
    FOREACH_MAP_ITEM(all_ids, mi, 0, &id) {
        if (!id) continue ;

        // Accept only variables :
        if (!id->IsNet() && !id->IsVar()) continue ;

        // Discard types and members and functions and such (return 1 for IsVar() ?)
        if (id->IsMember() || id->IsType() || id->IsFunction() || id->IsTask()) continue ;

        // Take this variable, and add its size to the result :
        verific_int64 id_ss = id->StaticSizeSignInternal(tab, 0) ;
        result += ABS(id_ss) ;
    }
    return result ;
}

verific_int64
VeriTypeId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // For non-class types, return the base class' size-sign:
    if (_type != VERI_CLASS) return VeriVariable::StaticSizeSignInternal(tab, target_context) ;

    // VIPER #5850: Calculate the size of class type variable with unpacked dimension:
    // Classes are always treated as pointers and to use a class variable, we need to use
    // 'new' first. So we calculate the size of class variable as 32.
    // For array class type variable we first find the dimension size and then multiply
    // it with 32. For all other class type variables we simply return 32.

    if (_dimensions) {
        VeriRange *range = _dimensions ;

        // Calculate the total number of elements of the unpacked dimensions
        // Just multiply the size of the dimensions
        verific_uint64 dim_size = 1 ;
        verific_int64 msb = 0, lsb = 0 ;
        while (range) {
            dim_size = dim_size * range->GetWidth(&msb, &lsb, tab) ;
            range = range->GetNext() ;
        }
        verific_int64 size = (int)(dim_size*(8*sizeof(void*))) ;
        return size ;
    }

    // Calculate the size for class type variables with no dimension as follows:
    // Return 32 for classes. Classes are always treated as pointers.
    // To use a class variable, we need to use 'new' first.
    return (8*sizeof(void*)) ;
}

// VIPER #5772: Return the size of the type identifier:
// For class type identifier this function returns the size of the identifier by calculating
// the total size of all its members except its member functions and tasks, and for other type
// identifier returns simply their satic size.
int VeriTypeId::StaticSizeOfClassMembers(ValueTable *tab) const
{
    // Calculate the size for class type identifier:
    if (IsClass()) {
        // Get a const char *name->VeriIdDef*  map of the locally declared identifiers :
        Map *all_ids = _local_scope ? _local_scope->DeclArea() : 0 ;
        MapIter mi ;
        VeriIdDef *id ;
        verific_int64 size = 0 ;
        FOREACH_MAP_ITEM(all_ids, mi, 0, &id) {
            if (!id) continue ;
            if (id->IsFunction() || id->IsTask()) continue ; // Do not consider the member functions and tasks
            verific_int64 ele_size = id->StaticSizeOfClassMembers(tab) ;
            size = size + ABS(ele_size) ;
        }
        return (int)(size) ; // return the size
    }
    // Calculate the size for non class type identifier:
    return VeriIdDef::StaticSizeOfClassMembers(tab) ;
}

// VIPER #5772: Return the size of the identifier:
int VeriIdDef::StaticSizeOfClassMembers(ValueTable *tab) const
{
    verific_int64 size = StaticSizeSignInternal(tab, 0) ;
    return (int)size ;
}

verific_int64
VeriModportId::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint * /*target_context*/) const
{
    // Size of an modport if used as a type. Size is accumulation of sizes of the
    // locally declared variables.  Modports are always unsigned as a whole.
    if (!_local_scope) return 0 ;

    verific_int64 result = 0 ;
    VeriIdDef *id ;
    Map *all_ids = _local_scope->DeclArea() ;
    MapIter mi ;
    FOREACH_MAP_ITEM(all_ids, mi, 0, &id) {
        if (!id) continue ;

        // Accept only variables :
        if (!id->IsNet() && !id->IsVar()) continue ;

        // Discard types and members and functions and such (return 1 for IsVar() ?)
        if (id->IsMember() || id->IsType() || id->IsFunction() || id->IsTask()) continue ;

        // Take this variable, and add its size to the result :
        verific_int64 id_ss = id->StaticSizeSignInternal(tab, 0) ;
        result += ABS(id_ss) ;
    }
    return result ;
}

verific_int64
VeriConcatItem::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // StaticSizeSign of a concat item would be StaticSizeSign of the expression ?
    if (_expr) return _expr->StaticSizeSignInternal(tab, target_context) ;

    return 0 ;
}

verific_int64
VeriNull::StaticSizeSignInternal(ValueTable * /*tab*/, const VeriConstraint * /*target_context*/) const
{
    return 0 ;
}

verific_int64
VeriAssignmentPattern::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // If target type is present, size/sign will be that of target type
    // The target type can be a VeriDataType (for type-referencing),
    if (_type) return _type->StaticSizeSignInternal(tab, target_context) ;

    // If no type is present, calculate the size of assignment pattern by adding size of each element
    // FIXME : May give wrong result if proper sized expression is not used in array elements
    // byte a[2][3] = '{{1'b1, 0, 2}, {1'b0, 3, 4}} ; // This will give wrong
    // result because 1'b1, 1'b0 will give size 1; while 0, 2, etc will give size 32
    //
    // RD: This routine should never have to be called (without context info).
    // RD: For now, use only the concat info :
    // VIPER #8070: Do not call the following routine, use size of target_context instead:
    //return VeriConcat::StaticSizeSignInternal(tab, target_context) ;
    if (!target_context) return 0 ;

    unsigned target_size = target_context->NumOfBits() ;
    unsigned target_sign = target_context->IsSigned() ;
    return MAKE_CONTEXT_SIZE_SIGN(target_size, target_sign) ;
}

verific_int64 VeriMultiAssignmentPattern::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // If target type is present, size/sign will be that of target type
    // The target type can be a VeriDataType (for type-referencing),
    if (_type) return _type->StaticSizeSignInternal(tab, target_context) ;

    // If no type is present, calculate the size of assignment pattern by
    // adding size of each element and mutipling that with repeat count
    // FIXME : May give wrong result if proper sized expression is not used in array elements
    // byte a[2][3] = '{2{1'b1, 0, 2}} ; // This will give wrong
    // result because 1'b1 will give size 1, 0, 2, etc will give size 32

    // RD: This routine should never have to be called (without context info).
    // RD: For now, use only the concat info :
    // VIPER #8070: Do not call the following routine, use size of target_context instead:
    //return VeriMultiConcat::StaticSizeSignInternal(tab, target_context) ;
    if (!target_context) return 0 ;

    unsigned target_size = target_context->NumOfBits() ;
    unsigned target_sign = target_context->IsSigned() ;
    return MAKE_CONTEXT_SIZE_SIGN(target_size, target_sign) ;
}

verific_int64 VeriTaggedUnion::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    // Size of tagged union expression is the size of the union whose member is _member
    // We have no way to know from this expression what the whole union definition is.
    // Actually tagged union without context is meaningless. As context is not
    // available here, we are now returning size of member itself.
    return (_member) ? _member->StaticSizeSignInternal(tab, target_context) : 0 ;
}

// VIPER #8227: Calculate static size sign of with expression:
verific_int64 VeriWith::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    return (_left) ? _left->StaticSizeSignInternal(tab, target_context) : 0 ;
}

verific_int64 VeriWithExpr::StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const
{
    verific_int64 result = 0 ;

    // It can be array method call or stream expression.
    // For stream expression _right can only be VeriRange
    if (_right && _right->IsRange()) { // It is stream expression:
        return 0 ; // FIX ME: its size-sign
    }
    // It is array method call :
    if (_right) result = _right->StaticSizeSignInternal(tab, target_context) ;
    return result ;
}

void VeriIndexedId::CheckForZeroOrNegArrayWidth() const
{
     if (!_prefix || !_id || !_idx) return ;
     if (_idx->IsRange()) _idx->CheckForZeroOrNegArrayWidth() ;
}
void VeriIndexedMemoryId::CheckForZeroOrNegArrayWidth() const
{
     if (!_id || !_prefix || !_indexes) return ;
     // Now, finally, if the last index is actually a range, then this is a part-select.
     // Similar to IndexedId, part-select returns as many elements as there are elements in the range:
     VeriExpression *last_index = (VeriExpression*)_indexes->GetLast() ;
     if (last_index && last_index->IsRange()) {
         // Its a part-select. Elements selected is size of the part-select range.
         // We need to compute the size of the last range for this is a part-select
         last_index->CheckForZeroOrNegArrayWidth() ;
     }
}
void VeriSelectedName::CheckForZeroOrNegArrayWidth() const
{
    if (_suffix_id) _suffix_id->CheckForZeroOrNegArrayWidth() ;
}

void VeriConcat::CheckForZeroOrNegArrayWidth() const
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->CheckForZeroOrNegArrayWidth() ;
    }
}

void VeriMultiConcat::CheckForZeroOrNegArrayWidth() const
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->CheckForZeroOrNegArrayWidth() ;
    }
}

void VeriMinTypMaxExpr::CheckForZeroOrNegArrayWidth() const
{
    if (_typ_expr) _typ_expr->CheckForZeroOrNegArrayWidth() ;
}

void VeriUnaryOperator::CheckForZeroOrNegArrayWidth() const
{
    if (_arg) _arg->CheckForZeroOrNegArrayWidth() ;
}

void VeriBinaryOperator::CheckForZeroOrNegArrayWidth() const
{
    if (_left) _left->CheckForZeroOrNegArrayWidth() ;
    if (_right) _right->CheckForZeroOrNegArrayWidth() ;
}

void VeriQuestionColon::CheckForZeroOrNegArrayWidth() const
{
    if (_then_expr) _then_expr->CheckForZeroOrNegArrayWidth() ;
    if (_else_expr) _else_expr->CheckForZeroOrNegArrayWidth() ;
}

void VeriPortConnect::CheckForZeroOrNegArrayWidth() const
{
    if (_connection) _connection->CheckForZeroOrNegArrayWidth() ;
}
void VeriRange::CheckForZeroOrNegArrayWidth() const
{
    int  size = 0 ;

    // Compute the size of the range, addcolon range and subcolon range
    // according to the node type, and insert the size in the Map.
    if ((_part_select_token == VERI_PARTSELECT_UP) || (_part_select_token == VERI_PARTSELECT_DOWN)) {
        VeriBaseValue *rval = (_right) ? _right->StaticEvaluateInternal(0,0,0,0) : 0 ;
        if (rval) {
            size = rval->GetIntegerValue() ;
            if (size <= 0) {
                Error("part-select has a negative or zero size") ;
            }
            delete rval ;
        }
    }
}
void VeriIndexedExpr::CheckForZeroOrNegArrayWidth() const
{
     if (_prefix) _prefix->CheckForZeroOrNegArrayWidth() ;
     if (_idx && _idx->IsRange()) _idx->CheckForZeroOrNegArrayWidth() ;
}

verific_int64
VeriTreeNode::StaticSizeSign(ValueTable *tab /*= 0*/, const VeriConstraint *target_context /* = 0 */) const
{
    // Check and set elaboration style to static:
    unsigned static_elab = VeriNode::IsStaticElab() ;
    if (!static_elab) VeriNode::SetStaticElab() ;

    // Evaluate size-sign, returns int
    verific_int64 result = StaticSizeSignInternal(tab, target_context) ;

    // Restore elaboration style:
    if (!static_elab) VeriNode::ResetStaticElab() ;

    return result ;
}

