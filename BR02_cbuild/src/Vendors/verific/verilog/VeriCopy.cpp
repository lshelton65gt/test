/*
 *
 * [ File Version : 1.73 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>
#include <stdio.h>

#include "Array.h"
#include "Set.h"
#include "Strings.h"

#include "veri_tokens.h"
#include "veri_file.h"
#include "VeriCopy.h"

#include "VeriModuleItem.h"
#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "VeriModule.h"
#include "VeriExpression.h"
#include "VeriId.h"
#include "VeriStatement.h"
#include "VeriTreeNode.h"
#include "VeriScope.h"
#include "VeriLibrary.h"

#include "VeriUtil_Stat.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VeriMapForCopy::VeriMapForCopy(hash_type type , unsigned init_bucket_size) :
    Map(type, init_bucket_size)
    , _modports(0)
    , _class_id_map(0)
    , _pkg_scope_map(0)
    , _intf_inst_ids(0) // VIPER #7913 (side-effect)
    , _decls(0)
    , _is_internal(0)
{}
VeriMapForCopy::~VeriMapForCopy()
{
    delete _modports ;
    delete _class_id_map ;
    delete _decls ;
    _pkg_scope_map = 0 ;
    _intf_inst_ids = 0 ; // VIPER #7913 (side-effect)
}

void VeriMapForCopy::AddModport(const VeriModport *modport)
{
    if (!modport) return ; // Nothing to add
    if (!_modports) _modports = new Array() ;
    _modports->InsertLast(modport) ;
}

// Set the data type of the modport port ids that are copied and stored in this map-for-copy object.
// Find the original copied id of the modport ports using the specified scope and set its data type.
// Set th local scope back pointer also for these modport ports.
//void VeriMapForCopy::ProcessModports(const VeriScope *active_scope, const VeriTreeNode *from /* = 0 */)
/*{
    if (!active_scope || !_modports) return ;

    unsigned i ;
    VeriModport *item ;
    FOREACH_ARRAY_ITEM(_modports, i, item) {
        if (!item) continue ;
        Array *modport_decls = item->GetModportDecls() ;
        unsigned j ;
        VeriModportDecl *modport_decl ;
        FOREACH_ARRAY_ITEM(modport_decls, j, modport_decl) {
            if(!modport_decl) continue ;
            Array *ports = modport_decl->GetPorts() ; // Get the array of id defs
            unsigned k ;
            VeriIdDef *modport_port ; // the modportport prototype.
            VeriIdDef *modport_var ;  // the original variable
            FOREACH_ARRAY_ITEM(ports, k, modport_port) {
                if (!modport_port) continue ;
                // Look for the modport_port definition in the interface scope
                modport_var = active_scope->Find(modport_port->Name(), from) ;
                if (!modport_var) continue ;
                // If the modport_port doesn't have a data type then set its
                // data type to the data type of modport_var
                if (modport_port->IsPort() && !modport_port->GetDataType()) {
                    modport_port->SetDataType(modport_var->GetDataType()) ;
                }
                if (!modport_port->IsPort()) {
                    // Set the local scope as well as the module item for clocking decls:
                    modport_port->SetModuleItem(modport_var->GetModuleItem()) ;
                    modport_port->SetLocalScope(modport_var->LocalScope()) ;
                }
            }
        }
    }
    // Reset the Array so that we don't traverse them over again:
    _modports->Reset() ;
}*/
void VeriMapForCopy::AddDecl(const VeriDataDecl* new_decl)
{
    if (!new_decl || _is_internal) return ; // Do not add declaration from seq/par blocks
    if (!_decls) _decls = new Array(2) ;
    _decls->Insert(new_decl) ;
}

// This is a function to copy the attributes. This is needed for the nodes having attributes.
Map *VeriTreeNode::CopyAttributes(const Map *this_attr, VeriMapForCopy &id_map_table)
{
    if (!this_attr) return 0 ; // no attributes to copy

    MapIter iter ;
    char *name ;
    VeriExpression *val ;
    Map *attr = new Map(STRING_HASH) ;

    FOREACH_MAP_ITEM(this_attr, iter, &name, &val) {
        VeriExpression *node = val ? val->CopyExpression(id_map_table) : 0 ;
        (void) attr->Insert(Strings::save(name), node) ;
    }
    return attr ;
}

/* static */ Array *
VeriTreeNode::CopyComments(const Array *this_comments, VeriMapForCopy &id_map_table)
{
    if (!this_comments) return 0 ; // No comments to copy

    Array *new_comment_arr = 0 ;

    new_comment_arr = new Array(this_comments->Size()) ;
    unsigned i ;
    VeriCommentNode *comment ;
    FOREACH_ARRAY_ITEM(this_comments, i, comment) {
        // Use the copy constructor to save an unnecessary object creation of VeriMapForCopy:
        VeriCommentNode *node = (comment) ? (new VeriCommentNode(*comment, id_map_table)) : 0 ;
        new_comment_arr->Insert(node) ;
    }

    return new_comment_arr ;
}

Array *VeriTreeNode::CopyExpressionArray(const Array *exprs, VeriMapForCopy &id_map_table)
{
    if (!exprs) return 0 ;
    Array *new_exprs = new Array(exprs->Size()) ;
    unsigned i = 0 ;
    VeriExpression *node = 0 ;
    VeriExpression *new_node = 0 ;
    FOREACH_ARRAY_ITEM(exprs, i, node) {
        new_node = node ? node->CopyExpression(id_map_table) : 0 ;
        new_exprs->InsertLast(new_node) ;
    }
    return new_exprs ;
}

Array *VeriTreeNode::CopyStatementArray(const Array *stmts, VeriMapForCopy &id_map_table)
{
    if (!stmts) return 0 ;
    Array *new_stmts = new Array(stmts->Size()) ;
    unsigned i = 0 ;
    VeriStatement *node = 0 ;
    VeriStatement *new_node = 0 ;
    FOREACH_ARRAY_ITEM(stmts, i, node) {
        new_node = node ? node->CopyStatement(id_map_table) : 0 ;
        new_stmts->InsertLast(new_node) ;
    }
    return new_stmts ;
}

Array *VeriTreeNode::CopyModuleItemArray(const Array *items, VeriMapForCopy &id_map_table)
{
    if (!items) return 0 ;
    Array *new_items = new Array(items->Size()) ;
    unsigned i = 0 ;
    VeriModuleItem *node = 0 ;
    VeriModuleItem *new_node = 0 ;
    FOREACH_ARRAY_ITEM(items, i, node) {
        new_node = node ? node->CopyModuleItem(id_map_table) : 0 ;
        new_items->InsertLast(new_node) ;
    }
    return new_items ;
}

Array *VeriTreeNode::CopyIdArray(const Array *ids, VeriMapForCopy &id_map_table)
{
    if (!ids) return 0 ;
    unsigned i ;
    Array *new_ids = new Array(ids->Size()) ;
    VeriIdDef *node = 0 ;
    FOREACH_ARRAY_ITEM(ids, i, node) {
        const MapItem *ele = id_map_table.GetItem(node) ;
        VeriIdDef *new_id = 0 ;
        new_id = ele ? (VeriIdDef *)ele->Value(): node ;
        new_ids->InsertLast(new_id) ;
    }
    return new_ids ;
}
void VeriMapForCopy::SetCopiedClassId(const VeriIdDef *old, const VeriIdDef *new_id)
{
    if (!_class_id_map) _class_id_map = new Map(POINTER_HASH) ;
    (void) _class_id_map->Insert(old, new_id) ;
}
VeriIdDef *VeriMapForCopy::GetCopiedClassId(const VeriIdDef *id) const
{
    if (!_class_id_map) return 0 ;
    return (VeriIdDef*)_class_id_map->GetValue(id) ;
}
void VeriMapForCopy::MakeAssociation(const VeriScope *old_scope, const VeriScope *new_scope)
{
    if (!old_scope || !new_scope || (old_scope == new_scope)) return ;

    (void) Insert(old_scope, new_scope) ;
    Map *old_this_scope = old_scope->GetThisScope() ;
    Map *new_this_scope = new_scope->GetThisScope() ;

    if (!old_this_scope || !new_this_scope) return ;

    MapIter mi ;
    char *id_name ;
    VeriIdDef *old_id, *new_id ;
    FOREACH_MAP_ITEM(old_this_scope, mi, &id_name, &old_id) {
        new_id = (VeriIdDef*)new_this_scope->GetValue(id_name) ;
        if (new_id) (void) Insert(old_id, new_id) ;
    }
}
void VeriMapForCopy::MakeAssociationForPkg(const VeriScope *old_scope, const VeriScope *new_scope, VeriIdDef *ignore_id, VeriScope *ignore_scope)
{
    if (!old_scope || !new_scope || (old_scope == new_scope) || (old_scope == ignore_scope)) return ;

    (void) Insert(old_scope, new_scope) ;
    Map *old_this_scope = old_scope->GetThisScope() ;
    Map *new_this_scope = new_scope->GetThisScope() ;

    if (!old_this_scope || !new_this_scope) return ;

    MapIter mi ;
    char *id_name ;
    VeriIdDef *old_id, *new_id ;
    FOREACH_MAP_ITEM(old_this_scope, mi, &id_name, &old_id) {
        if (old_id == ignore_id) continue ;
        new_id = (VeriIdDef*)new_this_scope->GetValue(id_name) ;
        if (new_id) (void) Insert(old_id, new_id) ;
    }
    Set *children = old_scope->GetChildrenScopes() ;
    Set *new_children = new_scope->GetChildrenScopes() ;
    if(!children || !new_children || (children->Size() != new_children->Size())) return ; // something wrong
    SetIter si ;
    unsigned idx = 0 ;
    VeriScope *old_child, *new_child ;
    FOREACH_SET_ITEM(children, si, &old_child) {
        new_child = (VeriScope*)new_children->GetAt(idx) ;
        idx++ ;
        MakeAssociationForPkg(old_child, new_child, ignore_id, ignore_scope) ;
    }
}
///////////////////////////////////////////////////////////////////
/////////////////////////  Copy Routines //////////////////////////
///////////////////////////////////////////////////////////////////

// All the following methods take an argument, which is an object of 'VeriMapForCopy',
// derived from Map. Default value is zero. If argument is having the default value,
// i.e. no argument is passed from the upper level, the method creates a new
// 'VeriMapForCopy' and remembers that the object is created here. Before returning
// the copied node, it deletes 'id_map_table', if created in this particular method.
VeriTreeNode::VeriTreeNode(const VeriTreeNode &tree_node, VeriMapForCopy &id_map_table) :
    VeriNode(),
    _linefile(0)
{
    // Set the linefile info
    _linefile = tree_node._linefile ;

    // VIPER #7201: Copy attributes and comments from the node:
    // Attributes and Comments are now on VeriTreeNode class.
    // So, this is the best place to copy them.
    SetAttributes(CopyAttributes(tree_node.GetAttributes(), id_map_table)) ;
    AddComments(CopyComments(tree_node.GetComments(), id_map_table)) ;
}

VeriRange *VeriRange::CopyRange(VeriMapForCopy &id_map_table) const
{
    return new VeriRange(*this, id_map_table) ;
}

VeriRange::VeriRange(const VeriRange &range, VeriMapForCopy &id_map_table) :
    VeriExpression(range, id_map_table),
    _left(0),
    _right(0),
    _part_select_token(0),
    _next(0)
{
    // Get the left, right, and the next and copy.
    _left =  (range._left) ? range._left->CopyExpression(id_map_table) : 0 ;
    _right = (range._right) ? range._right->CopyExpression(id_map_table) : 0 ;
    _next = (range._next) ? range._next->CopyRange(id_map_table) : 0 ;
    _part_select_token = range._part_select_token ;
}

VeriDataType *VeriDataType::CopyDataType(VeriMapForCopy &id_map_table) const
{
    return new VeriDataType(*this, id_map_table) ;
}
// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
VeriExpression *VeriPathPulseVal::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriPathPulseVal(*this, id_map_table) ;
}

VeriStrength *VeriStrength::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriStrength(*this, id_map_table) ;
}

VeriStrength::VeriStrength(const VeriStrength &strength, VeriMapForCopy &id_map_table) :
    VeriTreeNode(strength, id_map_table),
    _lval(0),
    _rval(0)
{
    _lval = strength._lval ;
    _rval = strength._rval ;
}

VeriNetRegAssign* VeriNetRegAssign::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriNetRegAssign(*this, id_map_table) ;
}

VeriNetRegAssign::VeriNetRegAssign(const VeriNetRegAssign &net_reg_assign, VeriMapForCopy &id_map_table) :
    VeriTreeNode(net_reg_assign, id_map_table),
    _lval(0),
    _val(0)
{
    _lval = net_reg_assign._lval ? net_reg_assign._lval->CopyExpression(id_map_table) : 0 ;
    _val = net_reg_assign._val ? net_reg_assign._val->CopyExpression(id_map_table) : 0 ;
}

VeriDefParamAssign* VeriDefParamAssign::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriDefParamAssign(*this, id_map_table) ;
}

VeriDefParamAssign::VeriDefParamAssign(const VeriDefParamAssign &defparam_assign, VeriMapForCopy &id_map_table) :
    VeriTreeNode(defparam_assign, id_map_table),
    _lval(0),
    _val(0)
{
    _lval = defparam_assign._lval ? defparam_assign._lval->CopyName(id_map_table) : 0 ;
    _val = defparam_assign._val ? defparam_assign._val->CopyExpression(id_map_table) : 0 ;
}

VeriCaseItem* VeriCaseItem::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriCaseItem(*this, id_map_table) ;
}

VeriCaseItem::VeriCaseItem(const VeriCaseItem &case_item, VeriMapForCopy &id_map_table) :
    VeriTreeNode(case_item, id_map_table),
    _conditions(0),
    _stmt(0)
    , _scope(0)
{
    _scope = (case_item._scope) ? case_item._scope->Copy(id_map_table) : 0 ;
    // Copying Array of Expressions
    _conditions = CopyExpressionArray(case_item._conditions, id_map_table) ;

    // Copying the statement
    _stmt = case_item._stmt ? case_item._stmt->CopyStatement(id_map_table) : 0 ;

    if (_scope) _scope->UpdateScope(case_item._scope, id_map_table) ;
}

VeriGenerateCaseItem* VeriGenerateCaseItem::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriGenerateCaseItem(*this, id_map_table) ;
}

VeriGenerateCaseItem::VeriGenerateCaseItem(const VeriGenerateCaseItem &gen_case_item, VeriMapForCopy &id_map_table) :
    VeriTreeNode(gen_case_item, id_map_table),
    _conditions(0),
    _item(0),
    _scope(0)
{
    // Copying Array of Expressions, values under which this case item hits
    _conditions = CopyExpressionArray(gen_case_item._conditions, id_map_table) ;

    // Copy the scope of this branch
    _scope = gen_case_item._scope ? gen_case_item._scope->Copy(id_map_table) : 0  ;
    if (_scope) _scope->SetGenerateScope() ;

    // Copying the case item itself
    _item = gen_case_item._item ? gen_case_item._item->CopyModuleItem(id_map_table) : 0 ;
    if (_scope) _scope->UpdateScope(gen_case_item._scope, id_map_table) ;
}

VeriPath* VeriPath::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriPath(*this, id_map_table) ;
}

VeriPath::VeriPath(const VeriPath &path, VeriMapForCopy &id_map_table) :
    VeriTreeNode(path, id_map_table),
    _polarity_operator(0),
    _path_token(0),
    _edge(0),
    _in_terminals(0),
    _out_terminals(0),
    _out_polarity_operator(0),
    _data_source(0)
{
    // Copying the polarity operator
    _polarity_operator = path._polarity_operator ;
    // Copying the token
    _path_token = path._path_token ;
    // Copying the edge
    _edge = path._edge ;

    // Copying the inputs to the path
    _in_terminals = CopyExpressionArray(path._in_terminals, id_map_table) ;

    // Copying the outputs to the path
    _out_terminals = CopyExpressionArray(path._out_terminals, id_map_table) ;

    // Copying output polarity operator
    _out_polarity_operator = path._out_polarity_operator ;

    // Copying the path data source
    _data_source = path._data_source ? path._data_source->CopyExpression(id_map_table) : 0 ;
}

VeriDelayOrEventControl* VeriDelayOrEventControl::Copy(VeriMapForCopy &id_map_table) const
{
    return  new VeriDelayOrEventControl(*this, id_map_table) ;
}

VeriDelayOrEventControl::VeriDelayOrEventControl(const VeriDelayOrEventControl &delay_event_control, VeriMapForCopy &id_map_table) :
    VeriExpression(delay_event_control, id_map_table),
    _delay_control(0),
    _repeat_event(0),
    _event_control(0)
    ,_is_cycle_delay(0)
{
    // Copying Delay expression
    _delay_control = delay_event_control._delay_control ? delay_event_control._delay_control->CopyExpression(id_map_table): 0 ;

    // Copying Repeat expression
    _repeat_event = delay_event_control._repeat_event ? delay_event_control._repeat_event->CopyExpression(id_map_table) : 0 ;

    // Copying the event expression
    _event_control = CopyExpressionArray(delay_event_control._event_control, id_map_table) ;
}

VeriConfigRule::VeriConfigRule(const VeriConfigRule &rule, VeriMapForCopy &id_map_table) :
    VeriTreeNode(rule, id_map_table),
    _liblist(0)
{
    _liblist = (rule._liblist) ? new Array(rule._liblist->Size()) : 0 ;
    if (_liblist) {
        unsigned i ;
        const char *lib_name ;
        FOREACH_ARRAY_ITEM(rule._liblist, i, lib_name) {
            _liblist->InsertLast(Strings::save(lib_name)) ;
        }
    }
}

VeriConfigRule *VeriInstanceConfig::CopyRule(VeriMapForCopy &id_map_table) const
{
    return new VeriInstanceConfig(*this, id_map_table) ;
}

VeriInstanceConfig::VeriInstanceConfig(const VeriInstanceConfig &inst_cfg, VeriMapForCopy &id_map_table) :
    VeriConfigRule(inst_cfg, id_map_table),
    _inst_ref(0),
    _use_clause(0)
{
    _inst_ref = (inst_cfg._inst_ref) ? inst_cfg._inst_ref->CopyName(id_map_table) : 0 ;
    _use_clause = (inst_cfg._use_clause) ? inst_cfg._use_clause->Copy(id_map_table) : 0 ;
}

VeriConfigRule *VeriCellConfig::CopyRule(VeriMapForCopy &id_map_table) const
{
    return new VeriCellConfig(*this, id_map_table) ;
}

VeriCellConfig::VeriCellConfig(const VeriCellConfig &cell_cfg, VeriMapForCopy &id_map_table) :
    VeriConfigRule(cell_cfg, id_map_table),
    _cell_ref(0),
    _use_clause(0)
{
    _cell_ref = (cell_cfg._cell_ref) ? cell_cfg._cell_ref->CopyName(id_map_table) : 0 ;
    _use_clause = (cell_cfg._use_clause) ? cell_cfg._use_clause->Copy(id_map_table) : 0 ;
}

VeriConfigRule *VeriDefaultConfig::CopyRule(VeriMapForCopy &id_map_table) const
{
    return new VeriDefaultConfig(*this, id_map_table) ;
}

VeriDefaultConfig::VeriDefaultConfig(const VeriDefaultConfig &def_cfg, VeriMapForCopy &id_map_table) :
    VeriConfigRule(def_cfg, id_map_table)
{
    // Nothing more to copy
}

VeriUseClause *VeriUseClause::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriUseClause(*this, id_map_table) ;
}

VeriUseClause::VeriUseClause(const VeriUseClause &clause, VeriMapForCopy &id_map_table) :
    VeriTreeNode(clause, id_map_table),
    _cell_ref(0),
    _is_config(clause._is_config),
    _param_values(0) // VIPER #7416
{
    _cell_ref = (clause._cell_ref) ? clause._cell_ref->CopyName(id_map_table) : 0 ;
    _param_values = CopyExpressionArray(clause._param_values, id_map_table) ;
}
#ifdef VERILOG_PATHPULSE_PORTS
VeriPathPulseValPorts *VeriPathPulseValPorts::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriPathPulseValPorts(*this, id_map_table) ;
}
VeriPathPulseValPorts::VeriPathPulseValPorts(const VeriPathPulseValPorts &ports, VeriMapForCopy &id_map_table) :
    VeriTreeNode(ports, id_map_table),
    _input_port(0),
    _output_port(0)
{
    _input_port = (ports._input_port) ? ports._input_port->CopyName(id_map_table) : 0 ;
    _output_port = (ports._output_port) ? ports._output_port->CopyName(id_map_table) : 0 ;
}
#endif

VeriClockingDirection * VeriClockingDirection::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriClockingDirection(*this, id_map_table) ;
}
VeriClockingDirection::VeriClockingDirection(const VeriClockingDirection &clk_dir, VeriMapForCopy &id_map_table) :
    VeriTreeNode(clk_dir, id_map_table),
    _dir(clk_dir._dir),
    _edge(clk_dir._edge),
    _delay_control(0)
{
    _delay_control = clk_dir._delay_control ? clk_dir._delay_control->Copy(id_map_table) : 0 ;
}
VeriProduction *VeriProduction::Copy(VeriMapForCopy &id_map_table)const
{
    return new VeriProduction(*this, id_map_table) ;
}
VeriProduction::VeriProduction(const VeriProduction &production, VeriMapForCopy &id_map_table) :
    VeriTreeNode(production, id_map_table),
    _data_type(0),
    _id(0),
    _formals(0),
    _items(0),
    _ports(0),
    _scope(0)
{
    // Copy data type
    _data_type = production._data_type ? production._data_type->CopyDataType(id_map_table) : 0 ;

    // Copy identifier
    if (production._id) {
        const MapItem *ele = id_map_table.GetItem(production._id) ;
        if (ele) {
            _id = (VeriIdDef *)ele->Value() ;
        } else {
            _id = production._id->CopyId(id_map_table, 1) ;
            (void) id_map_table.Insert(production._id, _id) ;
        }
    }

    // Copy the local scope
    _scope = production._scope ? production._scope->Copy(id_map_table) : 0 ;

    // Copy formals
    _formals = CopyExpressionArray(production._formals, id_map_table) ;

    // Copy production items
    _items = production._items ? new Array(production._items->Size()) : 0 ;
    unsigned i ;
    VeriProductionItem *prod_item ;
    FOREACH_ARRAY_ITEM(production._items, i, prod_item) {
        if (_items) _items->InsertLast(prod_item ? prod_item->Copy(id_map_table): 0) ;
    }

    // Copy ordered list of production arguments
    _ports = CopyExpressionArray(production._ports, id_map_table) ;

    if (_scope) _scope->UpdateScope(production._scope, id_map_table) ;
    // Set the module item back-pointer of the production id
    if (_id) _id->SetProduction(this) ;
}
VeriProductionItem *VeriProductionItem::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriProductionItem(*this, id_map_table) ;
}
VeriProductionItem::VeriProductionItem(const VeriProductionItem &prod_item, VeriMapForCopy &id_map_table) :
    VeriTreeNode(prod_item, id_map_table),
    _is_rand_join(0),
    _expr(0),
    _items(0),
    _w_spec(0),
    _code_blk(0)
{
    // Copy the is_rand_join flag
    _is_rand_join = prod_item._is_rand_join ;

    // Copy rand join expression
    _expr = prod_item._expr ? prod_item._expr->CopyExpression(id_map_table) : 0 ;

    // Copy statements
    _items = CopyStatementArray(prod_item._items, id_map_table) ;

    // Copy weight specification
    _w_spec = prod_item._w_spec ? prod_item._w_spec->CopyExpression(id_map_table) : 0 ;

    // Copy the code block
    _code_blk = prod_item._code_blk ? prod_item._code_blk->CopyStatement(id_map_table) : 0 ;
}

VeriConst::VeriConst(const VeriConst &veri_const, VeriMapForCopy &id_map_table) :
    VeriExpression(veri_const, id_map_table)
{}

VeriExpression* VeriConstVal::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriConstVal(*this, id_map_table) ;
}

VeriConstVal::VeriConstVal(const VeriConstVal &const_val, VeriMapForCopy &id_map_table) :
    VeriConst(const_val, id_map_table),
    _size(0),
    _is_signed(0),
    _is_string(0),
    _is_unsized_bit(0),
    _is_unsized_number(0),
    _value(0),
    _zvalue(0),
    _xvalue(0)
#ifdef VERILOG_PRESERVE_LITERALS
    , _orig_value(0)  // VIPER #2587 & #2770: Preserve the original value string
#endif
{
    _size = const_val._size ;
    _is_signed = const_val._is_signed ;
    _is_string = const_val._is_string ;
    _is_unsized_bit = const_val._is_unsized_bit ;
    _is_unsized_number = const_val._is_unsized_number ;

    unsigned mem_size = const_val.GetMemSize() ;
    _value = VFC_ARRAY_NEW(unsigned char, mem_size) ;
    memset(_value,0,mem_size) ;
    if (const_val._zvalue) _zvalue = VFC_ARRAY_NEW(unsigned char, mem_size) ;
    if (const_val._xvalue) _xvalue = VFC_ARRAY_NEW(unsigned char, mem_size) ;

    for (unsigned i = 0; i < mem_size; i++) {
        _value[i] = const_val._value[i] ;
        if (_zvalue) _zvalue[i] = const_val._zvalue[i] ;
        if (_xvalue) _xvalue[i] = const_val._xvalue[i] ;
    }

#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & #2770: Preserve the original value string:
    if (const_val._orig_value) _orig_value = Strings::save(const_val._orig_value) ;
#endif
}

VeriExpression* VeriIntVal::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriIntVal(*this, id_map_table) ;
}

VeriIntVal::VeriIntVal(const VeriIntVal &int_val, VeriMapForCopy &id_map_table) :
    VeriConst(int_val, id_map_table),
    _num(0)
{
    _num = int_val._num ;
}

VeriExpression* VeriRealVal::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriRealVal(*this, id_map_table) ;
}

VeriRealVal::VeriRealVal(const VeriRealVal &real_val, VeriMapForCopy &id_map_table) :
    VeriConst(real_val, id_map_table),
    _num(0)
{
    _num = real_val._num ;
}
VeriExpression* VeriTimeLiteral::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriTimeLiteral(*this, id_map_table) ;
}

VeriTimeLiteral::VeriTimeLiteral(const VeriTimeLiteral &time_literal, VeriMapForCopy &id_map_table) :
    VeriConst(time_literal, id_map_table),
    _literal(0)
{
    _literal = Strings::save(time_literal._literal) ;
}

VeriExpression* VeriIndexedExpr::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriIndexedExpr(*this, id_map_table) ;
}
VeriIndexedExpr::VeriIndexedExpr(const VeriIndexedExpr &indexed_expr, VeriMapForCopy &id_map_table) :
    VeriExpression(indexed_expr, id_map_table),
    _prefix(0),
    _idx(0)
{
    // Copying prefix expression
    _prefix = (indexed_expr._prefix) ? indexed_expr._prefix->CopyExpression(id_map_table) : 0 ;

    // Copy the index expression, if not null.
    _idx = (indexed_expr._idx) ? indexed_expr._idx->CopyExpression(id_map_table): 0 ;
}

VeriName* VeriScopeName::CopyName(VeriMapForCopy &id_map_table) const
{
    return new VeriScopeName(*this, id_map_table) ;
}
VeriScopeName::VeriScopeName(const VeriScopeName &scope_name, VeriMapForCopy &id_map_table) :
    VeriSelectedName(scope_name, id_map_table),
    _param_values(0),
    _modport_name(0)
{
    _param_values = CopyExpressionArray(scope_name._param_values, id_map_table) ;
    _modport_name = scope_name._modport_name ? scope_name._modport_name->CopyName(id_map_table): 0 ;
}
VeriScope* VeriScope::Copy(VeriMapForCopy &id_map_table) const
{
    // VIPER #7508 : Check whether it is already copied or not
    VeriScope *copied_scope = (VeriScope*)id_map_table.GetValue(this) ;
    if (!copied_scope) {
        // Search in copied package scopes
        Map *pkg_scope_map = id_map_table.GetPkgScopeMap() ;
        MapItem *ele = pkg_scope_map ? pkg_scope_map->GetItem(this): 0 ;
        copied_scope = ele ? (VeriScope*)ele->Value(): 0 ;
    }
    if (copied_scope) return copied_scope ; // Return already copied
    return new VeriScope(*this, id_map_table) ;
}
VeriScope::VeriScope(const VeriScope &scope, VeriMapForCopy &id_map_table) :
    _upper(0),
    _owner(0),
    _this_scope(0),
    _using(0),
    _used_by(0),
    _children(0),
    _imported_scopes(0),
    _empty_scope(scope._empty_scope),
   _generate_scope(scope._generate_scope),
   _struct_union_scope(scope._struct_union_scope)
   , _seq_block_scope(scope._seq_block_scope)
   , _loop_scope(scope._loop_scope)
   , _is_std_pkg_used(scope._is_std_pkg_used)
   , _par_block_scope(scope._par_block_scope)
   , _exported_subprog_scope(scope._exported_subprog_scope)
   , _extended_scope(0)
   , _base_class_scope(0)
   , _struct_constraints(0)
   , _exported_scopes(0)

{
    // Get the parent scope.
    _upper = scope._upper ;
    if (_upper) {
        // If parent scope is not null, and we could find an entry of parent scope the
        // 'id_map_table', we should set the copied parent as the parent of 'this' scope.
        const MapItem *ele = id_map_table.GetItem(_upper) ;

        if (ele) _upper = (VeriScope*)ele->Value() ;
    }
    // Just add this scope into the upper scope as child. No need to process the child
    // of the scope being copied, when they will be copied, they will also add themselves
    // into this scope as child.
    if (_upper && (_upper != scope._upper)) _upper->AddChildScope(this) ;

    // Get the owner identifier of 'this' scope.
    VeriIdDef *owner = scope._owner ;
    if (owner) {
        // Ideally owner should be copied before copying the scope and we should get an entry
        // against 'owner' in the 'id_map_table'. If not found, we did some mistake in coding.
        const MapItem *ele = id_map_table.GetItem(owner) ;
        if (ele) {
            _owner = (VeriIdDef *)ele->Value() ;
            _owner->SetLocalScope(this) ;
        } else {
            VERIFIC_ASSERT(0) ;
        }
    }
    // Get the table containing the identifiers inside the scope. Copy each of the identifiers,
    // and insert the same in the table of the new scope.
    MapIter iter ;
    const char *name ;
    VeriIdDef *val ;

    _this_scope = new Map(STRING_HASH, scope._this_scope->Size()) ;
    FOREACH_MAP_ITEM(scope._this_scope, iter, &name, &val) {
        // Copy the id without the initial value, port connects we will copy them later
        VeriIdDef *new_id = val->CopyId(id_map_table, 0 /* don't copy initial values */) ;
        if (!new_id) continue ; // just in case

#ifdef VERILOG_ID_SCOPE_BACKPOINTER
        // Set the scope backpointer of the identifier to this :
        new_id->SetOwningScope(this) ;
#endif
        // Insert into this scope :
        (void) _this_scope->Insert(new_id->Name(), new_id, 0, 1) ;
    }

    if (scope._using) {
        _using = new Set(POINTER_HASH) ;
        SetIter si ;
        VeriScope *used_scope ;
        FOREACH_SET_ITEM(scope._using, si, &used_scope) {
            if (!used_scope) continue ;
            VeriScope *new_used_scope = 0 ;
            const MapItem *ele = id_map_table.GetItem(used_scope) ;
            new_used_scope = ele ? (VeriScope*)ele->Value(): used_scope ;
            // VIPER #7508 : Consider copied package scopes while creating new using list
            // We should use copied package scope if that package is copied
            if ((new_used_scope == used_scope)) {
                Map *pkg_scope_map = id_map_table.GetPkgScopeMap() ;
                ele = pkg_scope_map ? pkg_scope_map->GetItem(used_scope): 0 ;
                new_used_scope = ele ? (VeriScope*)ele->Value(): used_scope ;
                if (new_used_scope && (new_used_scope != used_scope)) {
                    (void) id_map_table.Insert(used_scope->GetOwner(), new_used_scope->GetOwner()) ;
                    // Elements defined inside package can be used in the scope, so put
                    // those in 'id_map_table'
                    id_map_table.MakeAssociationForPkg(used_scope, new_used_scope, 0, 0) ;
                }
                if ((new_used_scope == used_scope) && pkg_scope_map) {
                    // Check whether 'used_scope' is root scope, some packages
                    // may be imported in root and elements from those package
                    // can be used in this scope. So make copied version of those
                    // elements visible through 'id_map_table'
                    VeriIdDef *used_owner_id = used_scope->GetContainingModule() ;
                    VeriModule *used_design_unit = used_owner_id ? used_owner_id->GetModule(): 0 ;
                    if (used_design_unit && used_design_unit->IsRootModule()) {
                        // Traverse each copied package scope and make association
                        MapIter mi ;
                        VeriScope *old_scope, *new_scope ;
                        FOREACH_MAP_ITEM(pkg_scope_map, mi, &old_scope, &new_scope) {
                            if (scope._using->GetItem(old_scope)) continue ; // Already in using list
                            if (!old_scope || !new_scope) continue ;
                            (void) id_map_table.Insert(old_scope->GetOwner(), new_scope->GetOwner()) ;
                            id_map_table.MakeAssociationForPkg(old_scope, new_scope, 0, 0) ;
                        }
                    }
                }
            }
            (void) _using->Insert(new_used_scope) ;
            if (new_used_scope && (!new_used_scope->_used_by)) new_used_scope->_used_by = new Set(POINTER_HASH) ;
            if (new_used_scope) (void) new_used_scope->_used_by->Insert(this) ;
        }
    }
#if 0
    // Re-Initialize the values of VeriIdDefs and Copy port connects of VeriInstId now.
    // It may so happen that we get a initialization "a = b ;" at a point where "a" has
    // been inserted in id_map_table but "b" is yet to be inserted. In that case "a" is
    // initialized with the VeriIdDef pointing to the old location.
    FOREACH_MAP_ITEM(scope._this_scope, iter, &name, &val) {
        if (!val) continue ;
        // Get the initial value of the old id from scope._this_scope. We didn't copy the
        // initial value then, copy it now
        VeriIdDef *new_val = (VeriIdDef *)id_map_table.GetValue(val) ;
        if (!new_val) continue ;
        VeriExpression *init_val = val->GetInitialValue() ;
        // Copy the unpacked dimensions here, since it may reference ids that were not copied before:
        VeriRange *val_dim = val->GetDimensions() ;
        if (val_dim && !new_val->IsPrototypeId()) new_val->SetMemoryRange(val_dim->CopyRange(id_map_table)) ;
        // Copy and set the initial value.
        if (init_val && !new_val->IsPrototypeId()) {
            init_val = init_val->CopyExpression(id_map_table) ;
            new_val->SetInitialValue(init_val) ;
        }
        // VIPER #2676 : Copy and set port expression of named port id. We didn't copy those
        // during identifier copy as port expression may contain references to identifiers
        // declared after named port. So copy port expression now, all declared
        // identifiers are copied already.
        if (new_val->IsNamedPort()) {
            new_val->CopyPortExprFrom(val, id_map_table) ;
            continue ;
        }
        // Copy and set port connects and range of instance id. We didn't copy those above, copy now.
        if (!val->IsInst()) continue ;
        new_val->CopyPortConnectsFrom(val, id_map_table) ;
    }
#endif
    if (scope._imported_scopes) {
        _imported_scopes = new Map(POINTER_HASH) ;
        VeriScope *package_scope ;
        Set *names ;
        FOREACH_MAP_ITEM(scope._imported_scopes, iter, &package_scope, &names) {
            VeriScope *new_package_scope = 0 ;
            const MapItem *ele = id_map_table.GetItem(package_scope) ;
            new_package_scope = ele ? (VeriScope*)ele->Value(): package_scope ;
            // VIPER #7508 : Consider copied package scopes
            if ((new_package_scope == package_scope)) {
                Map *pkg_scope_map = id_map_table.GetPkgScopeMap() ;
                ele = pkg_scope_map ? pkg_scope_map->GetItem(package_scope): 0 ;
                new_package_scope = ele ? (VeriScope*)ele->Value(): package_scope ;
                // Insert old package id with new id in id_map_table if there is
                // no _using set
                if ((new_package_scope != package_scope) && !scope._using) {
                    // Elements defined inside package can be used in the scope, so put
                    // those in 'id_map_table'
                    (void) id_map_table.Insert(package_scope->GetOwner(), new_package_scope->GetOwner()) ;
                    id_map_table.MakeAssociationForPkg(package_scope, new_package_scope, 0, 0) ;
                }
            }
            Set *new_names = new Set(STRING_HASH, names->Size()) ;
            SetIter si ;
            FOREACH_SET_ITEM(names, si, &name) {
                VeriIdDef *id = new_package_scope ? new_package_scope->FindLocal(name) : 0 ;
                if (!id) id = new_package_scope ? new_package_scope->FindExported(name) : 0 ;
                // VIPER #7508 : Name of 'id' may be changed due to copy
                if (!id) {
                    VeriIdDef *old_id = package_scope ? package_scope->FindLocal(name): 0 ;
                    if (!old_id) old_id = package_scope ? package_scope->FindExported(name) : 0 ;
                    if (old_id) id = (VeriIdDef*)id_map_table.GetValue(old_id) ;
                }
                if (id) { // Explicitly imported identifier
                    (void) new_names->Insert(id->Name()) ;
                } else { // Wildcard imported
                    (void) new_names->Insert(name) ;
                }
            }
            if (new_package_scope) {
                (void) _imported_scopes->Insert(new_package_scope, new_names) ;
            } else {
                delete new_names ;
            }
        }
    }
    // Get the parent scope.
    _extended_scope = scope._extended_scope ;
    if (_extended_scope) {
        // If extended scope is not null, and we could find an entry of extended scope the
        // 'id_map_table', we should set the copied extended as the extended of 'this' scope.
        const MapItem *ele = id_map_table.GetItem(_extended_scope) ;

        if (ele) _extended_scope = (VeriScope*)ele->Value() ;
    }
    // Get the base class scope.
    _base_class_scope = scope._base_class_scope ;
    if (_base_class_scope) {
        // If base class scope is not null, and we could find an entry of base class scope the
        // 'id_map_table', we should set the copied scope as the base class of 'this' scope.
        const MapItem *ele = id_map_table.GetItem(_base_class_scope) ;
        if (ele) _base_class_scope = (VeriScope*)ele->Value() ;
    }
    if (scope._exported_scopes) {
        _exported_scopes = new Map(POINTER_HASH) ;
        VeriScope *package_scope ;
        Map *names ;
        FOREACH_MAP_ITEM(scope._exported_scopes, iter, &package_scope, &names) {
            VeriScope *new_package_scope = 0 ;
            const MapItem *ele = id_map_table.GetItem(package_scope) ;
            new_package_scope = ele ? (VeriScope*)ele->Value(): package_scope ;
            if (new_package_scope == package_scope) { // VIPER #7508
                Map *pkg_scope_map = id_map_table.GetPkgScopeMap() ;
                ele = pkg_scope_map ? pkg_scope_map->GetItem(package_scope): 0 ;
                new_package_scope = ele ? (VeriScope*)ele->Value(): package_scope ;
            }
            Map *new_names = new Map(STRING_HASH, names->Size()) ;
            MapIter mii ;
            linefile_type lf ;
            FOREACH_MAP_ITEM(names, mii, &name, &lf) {
                VeriIdDef *id = new_package_scope ? new_package_scope->FindLocal(name) : 0 ;
                if (!id) id = new_package_scope ? new_package_scope->FindExported(name) : 0 ;
                if (id) {
                    (void) new_names->Insert(id->Name(), (void*)lf) ;
                } else {
                    (void) new_names->Insert(name, (void*)lf) ;
                }
            }
            if (new_package_scope) {
                (void) _exported_scopes->Insert(new_package_scope, new_names) ;
            } else {
                delete new_names ;
            }
        }
    }
#if 0
    Array *intf_inst_ids = id_map_table.GetInterfaceInstIds() ;
    if (intf_inst_ids) {
        // VIPER #7913 (side-effect): This module/scope is being half in the process of elaboration.
        // It has some interface array instance which we elaborated but created virtual
        // interface variable is yet to be added into the scope.
        // So, generate the same structure for the copied module too, otherwise we corrupt:
        Array *new_intf_inst_ids = new Array(intf_inst_ids->Size()) ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(intf_inst_ids, i, val) {
            if (!val) continue ;
            VeriIdDef *new_id = 0 ;
            if (val->IsInst()) {
                // Instances must already get copied with the scope:
                new_id = (VeriIdDef *)id_map_table.GetValue(val) ;
                VERIFIC_ASSERT(new_id) ;
            } else {
                // Here need to copy this virtual interface array id:
                new_id = val->CopyId(id_map_table, 1 /* copy initial value too */) ;
                (void) id_map_table.Insert(val, new_id) ; // Insert into old -> new mapping
            }
            // Keep this identifier handy, we need to attach them to _intf_inst_id_map later
            new_intf_inst_ids->Insert(new_id) ;
        }
        // Since all such hanging ids now processed, set the new array to be processed by caller:
        id_map_table.SetInterfaceInstIds(new_intf_inst_ids) ;
    }
#endif
    // Insert this scope aganist old scope "scope" so that while copying a child scope of argument
    // specific scope, proper parent could be found.
    (void) id_map_table.Insert(&scope, this) ;
}
void VeriScope::UpdateScope(const VeriScope *scope, VeriMapForCopy &id_map_table) const
{
    // Re-Initialize the values of VeriIdDefs and Copy port connects of VeriInstId now.
    // It may so happen that we get a initialization "a = b ;" at a point where "a" has
    // been inserted in id_map_table but "b" is yet to be inserted. In that case "a" is
    // initialized with the VeriIdDef pointing to the old location.
    MapIter iter ;
    const char *name ;
    VeriIdDef *val ;
    FOREACH_MAP_ITEM(scope->_this_scope, iter, &name, &val) {
        if (!val) continue ;
        // Get the initial value of the old id from scope._this_scope. We didn't copy the
        // initial value then, copy it now
        VeriIdDef *new_val = (VeriIdDef *)id_map_table.GetValue(val) ;
        if (!new_val) continue ;
        VeriExpression *init_val = val->GetInitialValue() ;
        // Copy the unpacked dimensions here, since it may reference ids that were not copied before:
        VeriRange *val_dim = val->GetDimensions() ;
        if (val_dim && !new_val->IsPrototypeId()) new_val->SetMemoryRange(val_dim->CopyRange(id_map_table)) ;
        // Copy and set the initial value.
        if (init_val && !new_val->IsPrototypeId()) {
            init_val = init_val->CopyExpression(id_map_table) ;
            new_val->SetInitialValue(init_val) ;
        }
        // VIPER #2676 : Copy and set port expression of named port id. We didn't copy those
        // during identifier copy as port expression may contain references to identifiers
        // declared after named port. So copy port expression now, all declared
        // identifiers are copied already.
        if (new_val->IsNamedPort()) {
            new_val->CopyPortExprFrom(val, id_map_table) ;
            continue ;
        }
        // Copy and set port connects and range of instance id. We didn't copy those above, copy now.
        if (!val->IsInst()) continue ;
        new_val->CopyPortConnectsFrom(val, id_map_table) ;
    }
    Array *intf_inst_ids = id_map_table.GetInterfaceInstIds() ;
    if (intf_inst_ids) {
        // VIPER #7913 (side-effect): This module/scope is being half in the process of elaboration.
        // It has some interface array instance which we elaborated but created virtual
        // interface variable is yet to be added into the scope.
        // So, generate the same structure for the copied module too, otherwise we corrupt:
        Array *new_intf_inst_ids = new Array(intf_inst_ids->Size()) ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(intf_inst_ids, i, val) {
            if (!val) continue ;
            VeriIdDef *new_id = 0 ;
            if (val->IsInst()) {
                // Instances must already get copied with the scope:
                new_id = (VeriIdDef *)id_map_table.GetValue(val) ;
                VERIFIC_ASSERT(new_id) ;
            } else {
                // Here need to copy this virtual interface array id:
                new_id = val->CopyId(id_map_table, 1 /* copy initial value too */) ;
                (void) id_map_table.Insert(val, new_id) ; // Insert into old -> new mapping
            }
            // Keep this identifier handy, we need to attach them to _intf_inst_id_map later
            new_intf_inst_ids->Insert(new_id) ;
        }
        // Since all such hanging ids now processed, set the new array to be processed by caller:
        id_map_table.SetInterfaceInstIds(new_intf_inst_ids) ;
    }
}
void VeriIdDef::CopyPortConnectsFrom(VeriIdDef * /*old_id*/, VeriMapForCopy & /*id_map_table*/)
{
}

void VeriInstId::CopyPortConnectsFrom(VeriIdDef *old_id, VeriMapForCopy &id_map_table)
{
    VeriRange *dimensions = old_id->GetDimensions() ;
    if (dimensions) {
        VeriRange *new_dimensions = dimensions->CopyRange(id_map_table) ;
        SetMemoryRange(new_dimensions) ;
    }
    Array *port_connects = old_id->GetPortConnects() ;
    Array *new_port_connects = port_connects ? CopyExpressionArray(port_connects, id_map_table) : 0 ;
    if (new_port_connects) {
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(&_port_connects, i, expr) delete expr ;
        _port_connects.Reset() ;
        _port_connects.Append(new_port_connects) ;
        delete new_port_connects ;
    }
}
// VIPER #2676 : Copy port expression of named port from old named port to copied one
void VeriIdDef::CopyPortExprFrom(VeriIdDef * /*old_id*/, VeriMapForCopy & /*id_map_table*/)
{
}
void VeriNamedPort::CopyPortExprFrom(VeriIdDef *old_id, VeriMapForCopy &id_map_table)
{
    if (!old_id) return ;
    VeriExpression *port_expr = old_id->GetPortExpression() ; // Get port expression of old named port
    if (!port_expr) return ;

    delete _port_expr ;
    _port_expr = port_expr->CopyExpression(id_map_table) ;
}
VeriModule *VeriModule::CopyModule(VeriMapForCopy &id_map_table) const
{
    return new VeriModule(*this, id_map_table) ;
}

VeriModule::VeriModule(const VeriModule &mod, VeriMapForCopy &id_map_table) :
    VeriModuleItem(mod, id_map_table),
    _id(0),
    _package_import_decls(0),
    _parameter_connects(0),
    _port_connects(0),
    _module_items(0),
    _scope(0),
    _parameters(0),
    _ports(0),
    _bind_instances(0),
    _timestamp(0),
    _analysis_dialect(0),
    _is_celldefine(0),
    _default_nettype(0),
    _unconnected_drive(0),
    _is_analyzed(0),
    _has_analyze_error(0),
    _is_compiled(0),
    _is_static_elaborated(0),
    _compile_as_blackbox(0),
    _is_root_module(0),
    _affected_by_path_name(mod._affected_by_path_name),
    _original_module_name(0),
    _timescale(0),
    _parent_library(0)
    ,_port_scope(0)
#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    , _linefile_structs(0) // VIPER #5932: No need to copy it
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    , _module_item_replaced(0) // VIPER #6550
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    , _is_vhdl_package(0)      // VIPER #6895: flag for converted verilog package from a vhld one
    , _is_already_processed(0) // VIPER #6895: flag on package that is already elaborated
#endif
    , _delay_mode_distributed(0),
    _delay_mode_path(0),
    _delay_mode_unit(0),
    _delay_mode_zero(0),
    _default_decay_time(0),
    _default_trireg_strength(0)
{
    VeriIdDef *id = mod._id ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    const MapItem *ele = id ? id_map_table.GetItem(id) : 0 ;
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else if (id) {
        _id = id->CopyId(id_map_table, 1) ;
    }
    // Set back-pointer to tree onto identifier :
    if (_id) _id->SetModule(this) ;

    // VIPER #5954: Copy the port-scope if exists
    if (mod._port_scope) _port_scope = mod._port_scope->Copy(id_map_table) ;

    // Owner is already copied, now we can copy the scope, before going further.
    if (mod._scope) _scope = mod._scope->Copy(id_map_table) ;

    // Set the timestamp from the module. Since it is copied, timestamp should not change.
    _timestamp = mod._timestamp ;

    // Set the dialect under which this module was parsed :
    _analysis_dialect = mod._analysis_dialect ;

    // Set predefined macro flags from argument specific module
    _default_nettype = mod._default_nettype ;
    _is_celldefine = mod._is_celldefine ;
    _unconnected_drive = mod._unconnected_drive ;

    // Copy analysis/elab related flags.
    // VIPER #7508 : Do not copy _is_compiled flag, copied module should be elaborated
    _is_compiled = 0 ; //mod._is_compiled ;
    _is_analyzed = mod._is_analyzed ;
    _has_analyze_error = mod._has_analyze_error ;
    _is_root_module = mod._is_root_module ;

    // Set the design file specific module name from which this module is copied
    // If the module from which it is copying is itself copied module, set the
    // module name from which that module (here 'mod') is copied, otherwise set
    // the module name from which this is copying (here 'mod')
    _original_module_name = mod._original_module_name ? Strings::save(mod._original_module_name): Strings::save(mod.Name()) ;
    _timescale = Strings::save(mod._timescale) ;

    // VIPER #7379: Copy these optional compiler directives:
    _delay_mode_distributed  = mod._delay_mode_distributed ;
    _delay_mode_path         = mod._delay_mode_path ;
    _delay_mode_unit         = mod._delay_mode_unit ;
    _delay_mode_zero         = mod._delay_mode_zero ;
    _default_decay_time      = Strings::save(mod._default_decay_time) ;
    _default_trireg_strength = Strings::save(mod._default_trireg_strength) ;

    // Parse tree nodes owned by 'this' module :
    _parameter_connects = CopyModuleItemArray(mod._parameter_connects, id_map_table) ;
    //_port_connects = CopyExpressionArray(mod._port_connects, id_map_table) ;

    // VIPER #6797 : While copying the mod._port_connects, collect old port expression
    // with its copied version to update _actual field of port ids created for
    // compelex non-ansi ports and named ports
    Array *old_port_connects = mod._port_connects ;
    _port_connects = old_port_connects ? new Array(old_port_connects->Size()): 0 ;
    unsigned i ;
    Map expr_map(POINTER_HASH, old_port_connects ? old_port_connects->Size(): 3) ;
    VeriExpression *port_expr = 0 ;
    VeriExpression *new_port_expr = 0 ;
    FOREACH_ARRAY_ITEM(old_port_connects, i, port_expr) {
        new_port_expr = port_expr ? port_expr->CopyExpression(id_map_table) : 0 ;
        if (_port_connects) _port_connects->InsertLast(new_port_expr) ;
        if (port_expr && new_port_expr) (void) expr_map.Insert(port_expr, new_port_expr) ;
    }
    _module_items = CopyModuleItemArray(mod._module_items, id_map_table) ;

    // Arrays of IdDef backpointers
    //_ports = CopyIdArray(mod._ports, id_map_table) ;
    VeriIdDef *port_id = 0, *new_id = 0 ;
    _ports = mod._ports ? new Array(mod._ports->Size()): 0 ;
    FOREACH_ARRAY_ITEM(mod._ports, i, port_id) {
        ele = id_map_table.GetItem(port_id) ;
        new_id = ele ? (VeriIdDef *)ele->Value(): port_id ;
        if (_ports) _ports->InsertLast(new_id) ;
        VeriExpression *old_actual = port_id ? port_id->GetActual(): 0 ;
        if (old_actual) {
            VeriExpression *new_actual = (VeriExpression*)expr_map.GetValue(old_actual) ;
            if (new_actual && new_id) new_id->SetActual(new_actual) ;
        }
    }

    _parameters = CopyIdArray(mod._parameters, id_map_table) ;

    // SV bind instances : essentially back-pointers to instantiations that should occur in this module
    // RD: not sure if we should copy this array. It is module-oriented, which is only allowed for top-level modules
    // (or modules instantiated once). So copying should not happen (often) for these guys.
    // Also, the instatiations (actuals) are resolved already using 'this' module scope.
    // So, we might get bad pointers if we used these guys in a new, copied module.
    // Check with static elab developers if this is needed. Until then, bind instances are not copied.
    // _bind_instances = (mod._bind_instances) ? new Array(*(mod._bind_instances)) : 0 ;
    _package_import_decls = CopyModuleItemArray(mod._package_import_decls, id_map_table) ;
    if (_scope) _scope->UpdateScope(mod._scope, id_map_table) ;
}

VeriModule *VeriConfiguration::CopyModule(VeriMapForCopy &id_map_table) const
{
    return new VeriConfiguration(*this, id_map_table) ;
}

VeriConfiguration::VeriConfiguration(const VeriConfiguration &config, VeriMapForCopy &id_map_table) :
    VeriModule(config, id_map_table),
    _top_modules(0),
    _config_rules(0)
{
    // Copy the VeriName * top 'design' module names:
    _top_modules = CopyExpressionArray(config._top_modules, id_map_table) ;

    _config_rules = (config._config_rules) ? new Array(config._config_rules->Size()) : 0 ;
    if (_config_rules) {
        unsigned i ;
        VeriConfigRule *rule ;
        FOREACH_ARRAY_ITEM(config._config_rules, i, rule) {
            // Copy this VeriConfigRule:
            _config_rules->InsertLast((rule) ? rule->CopyRule(id_map_table) : 0) ;
        }
    }
}

VeriModuleItem* VeriDataDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriDataDecl(*this, id_map_table) ;
}

VeriDataDecl::VeriDataDecl(const VeriDataDecl &data_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(data_decl, id_map_table),
    _data_type(0),
    _ids(data_decl._ids.Size()),
    _decl_type(0)
    , _resolution_function(0)
{
    // Copy the data type
    _data_type = data_decl._data_type ? data_decl._data_type->CopyDataType(id_map_table) : 0 ;
    // Get the array of identifiers, and the range, if any.
    Array *ids = CopyIdArray(&data_decl._ids, id_map_table) ;
    _ids.Append(ids) ;
    delete ids ;
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (id) id->SetModuleItem(this) ;
        if (id && _data_type) id->SetDataTypeForCopy(_data_type) ;
        // Declaration will be set as back pointer only for typedef.(needed
        // for static elaboration of class type)
        if (id) id->SetModuleItem(this) ;
    }
    // Create a new data declaration.
    _decl_type = data_decl._decl_type ;
    id_map_table.AddDecl(this) ;

    _resolution_function = data_decl._resolution_function ? data_decl._resolution_function->CopyName(id_map_table) : 0 ;
}

VeriModuleItem* VeriNetDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriNetDecl(*this, id_map_table) ;
}

VeriNetDecl::VeriNetDecl(const VeriNetDecl &net_decl, VeriMapForCopy &id_map_table) :
    VeriDataDecl(net_decl, id_map_table),
    _strength(0),
    _delay(0)
{
    _strength = net_decl._strength ? net_decl._strength->Copy(id_map_table) : 0 ;

    _delay = CopyExpressionArray(net_decl._delay, id_map_table) ;
}

VeriModuleItem* VeriFunctionDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriFunctionDecl(*this, id_map_table) ;
}

VeriFunctionDecl::VeriFunctionDecl(const VeriFunctionDecl &func_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(func_decl, id_map_table),
    _func(0),
    _name(0),
    _automatic_type(0),
    _can_be_const_func(1),
    _is_const_called(0),
    _verilog_method(1),
    _data_type(0),
    _ansi_io_list(0),
    _decls(0),
    _stmts(0),
    _scope(0),
    _ports(0)
{
    const MapItem *ele = id_map_table.GetItem(func_decl._func) ;
    // VeriFunctionDecl should have a function identifier, so null checking is not done.
    // This identifier is the owner of the function scope, so it needs to be copied or the
    // copied identifier is to be extracted from the 'id_map_table' before copying the scope.
    if (ele) {
        _func = (VeriIdDef *)ele->Value() ;
    } else {
        _func = func_decl._func ? func_decl._func->CopyId(id_map_table, 1) : 0 ;
    }
    // Copy the name reference :
    _name = func_decl._name ? func_decl._name->CopyName(id_map_table) : 0 ;

    // Owner is copied, now copy the scope.
     _scope = func_decl._scope ? func_decl._scope->Copy(id_map_table) : 0 ;

    _automatic_type = func_decl._automatic_type ;
    _can_be_const_func = func_decl._can_be_const_func ;
    _is_const_called = func_decl._is_const_called ;
    _verilog_method = func_decl._verilog_method ;

    // Copy the data type if any.
    _data_type = (func_decl._data_type) ? func_decl._data_type->CopyDataType(id_map_table) : 0 ;

    // Copy (ansi) IO list :
    _ansi_io_list = CopyExpressionArray(func_decl._ansi_io_list, id_map_table) ;

    // Copy local declarations :
    _decls = CopyModuleItemArray(func_decl._decls, id_map_table) ;

    // Copy statements
    _stmts = CopyStatementArray(func_decl._stmts,id_map_table) ;

    _ports = CopyIdArray(func_decl._ports, id_map_table) ;

    if (_func) _func->SetModuleItem(this) ;
    // Set the data type (if there) back pointer
    if (_func && _data_type) {
        _func->ClearDataType() ;
        _func->SetDataType(_data_type) ;
    }

    if (_scope) _scope->UpdateScope(func_decl._scope, id_map_table) ;
}

VeriModuleItem* VeriTaskDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriTaskDecl(*this, id_map_table) ;
}

VeriTaskDecl::VeriTaskDecl(const VeriTaskDecl &task_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(task_decl, id_map_table),
    _task(0),
    _name(0),
    _automatic_type(0),
    _contains_delay_or_event_control(0),
    _ansi_io_list(0),
    _decls(0),
    _stmts(0),
    _scope(0),
    _ports(0)
{
    const MapItem *ele = id_map_table.GetItem(task_decl._task) ;
    // VeriTaskDecl should have an task identifier, so null checking is not done.
    // This identifier is the owner of the task scope, so it needs to be copied or the copied
    // identifier is to be extracted from the 'id_map_table' before copying the scope.
    if (ele) {
        _task = (VeriIdDef *)ele->Value() ;
    } else {
        _task = task_decl._task ? task_decl._task->CopyId(id_map_table, 1) : 0 ;
    }

    // Copy name reference.
    _name = task_decl._name ? task_decl._name->CopyName(id_map_table) : 0 ;

    // Owner is copied, now copy the scope.
     _scope = task_decl._scope ? task_decl._scope->Copy(id_map_table) : 0 ;

    _automatic_type = task_decl._automatic_type ;
    _contains_delay_or_event_control = task_decl._contains_delay_or_event_control ;

    // Copy (ansi) IO declarations
    _ansi_io_list = CopyExpressionArray(task_decl._ansi_io_list, id_map_table) ;

    // Copy local declarations :
    _decls = CopyModuleItemArray(task_decl._decls, id_map_table) ;

    // Copy statements
    _stmts = CopyStatementArray(task_decl._stmts,id_map_table) ;

    _ports = CopyIdArray(task_decl._ports, id_map_table) ;

    if (_scope) _scope->UpdateScope(task_decl._scope, id_map_table) ;
    if (_task) _task->SetModuleItem(this) ;
}

VeriModuleItem* VeriDefParam::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriDefParam(*this, id_map_table) ;
}

VeriDefParam::VeriDefParam(const VeriDefParam &def_param, VeriMapForCopy &id_map_table) :
    VeriModuleItem(def_param, id_map_table),
    _defparam_assigns(0)
{
    // Copying Array of VeriDefParamAssign*
    VeriDefParamAssign *node, *new_node ;
    unsigned i ;
    if (def_param._defparam_assigns) {
        _defparam_assigns = new Array(def_param._defparam_assigns->Size()) ;
        FOREACH_ARRAY_ITEM(def_param._defparam_assigns, i, node) {
            new_node = node ? node->Copy(id_map_table) : 0 ;
            _defparam_assigns->InsertLast(new_node) ;
        }
    }
    (void) id_map_table.Insert(&def_param, this) ; // Store defparam in map with copied parse tree
}

VeriModuleItem* VeriContinuousAssign::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriContinuousAssign(*this, id_map_table) ;
}

VeriContinuousAssign::VeriContinuousAssign(const VeriContinuousAssign &cont_assign, VeriMapForCopy &id_map_table) :
    VeriModuleItem(cont_assign, id_map_table),
    _strength(0),
    _delay(0),
    _net_assigns(0)
{
    // Copy the strength
    _strength = cont_assign._strength ? cont_assign._strength->Copy(id_map_table) : 0 ;

    // Copying delay
    _delay = CopyExpressionArray(cont_assign._delay, id_map_table) ;

    // Copying Array of VeriNetRegAssign*
    VeriNetRegAssign *node ;
    VeriNetRegAssign *new_node ;
    if (cont_assign._net_assigns) {
        _net_assigns = new Array(cont_assign._net_assigns->Size()) ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(cont_assign._net_assigns, i, node) {
            new_node = node ? node->Copy(id_map_table) : 0 ;
            _net_assigns->InsertLast(new_node) ;
        }
    }
}

VeriModuleItem* VeriGateInstantiation::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriGateInstantiation(*this, id_map_table) ;
}

VeriGateInstantiation::VeriGateInstantiation(const VeriGateInstantiation &gate_inst, VeriMapForCopy &id_map_table) :
    VeriModuleItem(gate_inst, id_map_table),
    _inst_type(0),
    _strength(0),
    _delay(0),
    _insts(0)
{
    _inst_type = gate_inst._inst_type ;

    // Copy the strength, if any.
    _strength = gate_inst._strength ?gate_inst._strength->Copy(id_map_table) : 0 ;

    _delay = CopyExpressionArray(gate_inst._delay, id_map_table) ;

    VeriInstId *inst = 0 ;
    VeriInstId *new_gate_inst = 0 ;

    // Copy the instances.
    if (gate_inst._insts) {
        _insts = new Array(gate_inst._insts->Size()) ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(gate_inst._insts, i, inst) {
            const MapItem *ele = inst ? id_map_table.GetItem(inst) : 0 ;
            if (ele) {
                new_gate_inst = (VeriInstId *)ele->Value() ;
            } else {
                new_gate_inst = inst ;
            }
            //new_gate_inst = inst ? new VeriInstId(*inst, id_map_table)  : 0 ; // can't use CopyId, since we need a InstId.
            _insts->InsertLast(new_gate_inst) ;
        }
    }
}

VeriModuleItem* VeriModuleInstantiation::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return Copy(id_map_table) ;
}

VeriModuleInstantiation* VeriModuleInstantiation::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriModuleInstantiation(*this, id_map_table) ;
}

VeriModuleInstantiation::VeriModuleInstantiation(const VeriModuleInstantiation &mod_inst, VeriMapForCopy &id_map_table) :
    VeriModuleItem(mod_inst, id_map_table),
    _module_name(0),
    _strength(0),
    _param_values(0),
    _insts(mod_inst._insts.Size()),
    _library(0)
{
    const char *instantiated_module_name = mod_inst._module_name ? mod_inst._module_name->GetName(): 0 ;
    _module_name = mod_inst._module_name ? mod_inst._module_name->CopyName(id_map_table) : 0 ;

    // VIPER #7641 : Donot reset name and id when this is checker instance, as checker
    // instance cannot be recursive
    VeriIdDef *module_id = (_module_name) ? _module_name->GetId() : 0 ;
    if (!module_id || !module_id->IsChecker()) {
        if (_module_name && instantiated_module_name) _module_name->SetName(Strings::save(instantiated_module_name)) ;
        // For recursive module instantiations the instantiated module id becomes the copied
        // module id. For this we cannot handle recursive module instantiations in Static
        // Elaboration. For this, set the module id of _module_name to zero.
        // FIXME : This is a hack. We need to find an alternate way to handle this problem
        // in a more elegant manner. A copy of a recursive module should create a module which
        // is also recursive.
        if (_module_name && (module_id && Strings::compare(instantiated_module_name, module_id->GetName()) == 0)) _module_name->SetId(0) ;
    }

    // Copy the strength, if any.
    _strength = mod_inst._strength ? mod_inst._strength->Copy(id_map_table) : 0 ;

    _param_values = CopyExpressionArray(mod_inst._param_values, id_map_table) ;

    VeriInstId *inst = 0 ;
    VeriInstId *new_mod_inst = 0 ;

    _library = mod_inst._library ;
    // Copy the instances.
    if (mod_inst._insts.Size()) {
        unsigned i ;
        FOREACH_ARRAY_ITEM(&mod_inst._insts, i, inst) {
            const MapItem *ele = inst ? id_map_table.GetItem(inst) : 0 ;
            new_mod_inst = (ele) ? (VeriInstId *)ele->Value() : inst ;
            //new_mod_inst = inst ? new VeriInstId(*inst, id_map_table)  : 0 ; // can't use CopyId, since we need a InstId.
            _insts.InsertLast(new_mod_inst) ;
            // Also update back-pointer.
            if (new_mod_inst && (new_mod_inst != inst)) new_mod_inst->SetModuleInstance(this) ;
        }
    }
}

VeriModuleItem* VeriSpecifyBlock::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriSpecifyBlock(*this, id_map_table) ;
}

VeriSpecifyBlock::VeriSpecifyBlock(const VeriSpecifyBlock &spec_block, VeriMapForCopy &id_map_table) :
    VeriModuleItem(spec_block, id_map_table),
    _specify_items(0),
    _scope(0)
{
    // Copy the scope. Note, specify block does not have any owner for its scope.
    // RD : scope might be absent (XL/vcs don't have a specify scope). 0-pointer check needed.
    _scope = spec_block._scope ? spec_block._scope->Copy(id_map_table) : 0 ;

    _specify_items = CopyModuleItemArray(spec_block._specify_items, id_map_table) ;
    if (_scope) _scope->UpdateScope(spec_block._scope, id_map_table) ;
}

VeriModuleItem* VeriPathDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriPathDecl(*this, id_map_table) ;
}

VeriPathDecl::VeriPathDecl(const VeriPathDecl &path_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(path_decl, id_map_table),
    _ifnone(0),
    _cond(0),
    _path(0),
    _delay(0)
{
    _ifnone = path_decl._ifnone ;

    // Get the condition, and copy the same, if any.
    _cond = path_decl._cond ? path_decl._cond->CopyExpression(id_map_table): 0 ;

    // Get the path, and copy, if not null.
    _path = path_decl._path ? path_decl._path->Copy(id_map_table): 0 ;

    _delay = CopyExpressionArray(path_decl._delay, id_map_table) ;
}

VeriModuleItem* VeriSystemTimingCheck::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriSystemTimingCheck(*this, id_map_table) ;
}

VeriSystemTimingCheck::VeriSystemTimingCheck(const VeriSystemTimingCheck &sys_time_check, VeriMapForCopy &id_map_table) :
    VeriModuleItem(sys_time_check, id_map_table),
    _task_name(0),
    _args(0),
    _function_type(0)
{
    _task_name = Strings::save(sys_time_check._task_name) ;
    _function_type = sys_time_check._function_type ;

    // Copying the Array of expression
    _args = CopyExpressionArray(sys_time_check._args, id_map_table) ;
}

VeriModuleItem* VeriInitialConstruct::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriInitialConstruct(*this, id_map_table) ;
}

VeriInitialConstruct::VeriInitialConstruct(const VeriInitialConstruct &initial_con, VeriMapForCopy &id_map_table) :
    VeriModuleItem(initial_con, id_map_table),
    _stmt(0)
{
    _stmt = initial_con._stmt ? initial_con._stmt->CopyStatement(id_map_table) : 0 ;
}

VeriModuleItem* VeriAlwaysConstruct::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriAlwaysConstruct(*this, id_map_table) ;
}

VeriAlwaysConstruct::VeriAlwaysConstruct(const VeriAlwaysConstruct &always_con, VeriMapForCopy &id_map_table) :
    VeriModuleItem(always_con, id_map_table),
    _stmt(0)
{
    _stmt = always_con._stmt ? always_con._stmt->CopyStatement(id_map_table) : 0 ;
}

VeriModuleItem* VeriGenerateConstruct::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriGenerateConstruct(*this, id_map_table) ;
}

VeriGenerateConstruct::VeriGenerateConstruct(const VeriGenerateConstruct &gen_con, VeriMapForCopy &id_map_table) :
    VeriModuleItem(gen_con, id_map_table),
    _items(0)
{
    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
    _items = CopyModuleItemArray(gen_con._items, id_map_table) ;
    id_map_table.SetAsInternal(is_internal) ;
}

VeriModuleItem* VeriGenerateConditional::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return Copy(id_map_table) ;
}

VeriGenerateConditional* VeriGenerateConditional::Copy(VeriMapForCopy &id_map_table) const
{
    return new  VeriGenerateConditional(*this, id_map_table) ;
}

VeriGenerateConditional::VeriGenerateConditional(const VeriGenerateConditional &gen_cond, VeriMapForCopy &id_map_table) :
    VeriModuleItem(gen_cond, id_map_table),
    _if_expr(0),
    _then_item(0),
    _else_item(0),
    _then_scope(0),
    _else_scope(0)
{
    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
     // if item
    _if_expr = gen_cond._if_expr ? gen_cond._if_expr->CopyExpression(id_map_table) : 0 ;

    // copy the then branch scope
    _then_scope = gen_cond._then_scope ? gen_cond._then_scope->Copy(id_map_table): 0 ;
    if (_then_scope) _then_scope->SetGenerateScope() ;
    // then item
    _then_item = gen_cond._then_item ? gen_cond._then_item->CopyModuleItem(id_map_table): 0 ;
    if (_then_scope) _then_scope->UpdateScope(gen_cond._then_scope, id_map_table) ;

    // copy the else branch scope
    _else_scope = gen_cond._else_scope ? gen_cond._else_scope->Copy(id_map_table): 0 ;
    if (_else_scope) _else_scope->SetGenerateScope() ;
    // else item
    _else_item = gen_cond._else_item ? gen_cond._else_item->CopyModuleItem(id_map_table): 0 ;
    if (_else_scope) _else_scope->UpdateScope(gen_cond._else_scope, id_map_table) ;
    id_map_table.SetAsInternal(is_internal) ;
}

VeriModuleItem* VeriGenerateCase::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return Copy(id_map_table) ;
}

VeriGenerateCase* VeriGenerateCase::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriGenerateCase(*this, id_map_table) ;
}

VeriGenerateCase::VeriGenerateCase(const VeriGenerateCase &gen_case, VeriMapForCopy &id_map_table) :
    VeriModuleItem(gen_case, id_map_table),
    _case_style(0),
    _cond(0),
    _case_items(0)
{
    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
    _case_style = gen_case._case_style ;

    // Get the condition and copy.
    _cond = gen_case._cond ? gen_case._cond->CopyExpression(id_map_table): 0 ;

    VeriGenerateCaseItem *node ;

    // Get the case item array and copy.
    if (gen_case._case_items) {
        _case_items = new Array(gen_case._case_items->Size()) ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(gen_case._case_items, i, node) {
            VeriGenerateCaseItem *new_node = node ? node->Copy(id_map_table) : 0 ;
            _case_items->InsertLast(new_node) ;
        }
    }
    id_map_table.SetAsInternal(is_internal) ;
}

VeriModuleItem* VeriGenerateFor::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriGenerateFor(*this, id_map_table) ;
}

VeriGenerateFor::VeriGenerateFor(const VeriGenerateFor &gen_for, VeriMapForCopy &id_map_table) :
    VeriModuleItem(gen_for, id_map_table),
    _initial(0),
    _cond(0),
    _rep(0),
    _block_id(0),
    _items(0),
    _scope(0),
    _loop_scope(0)
{
    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
    // Copy the scope which contains loop control variable, if declared in-line
    _loop_scope = gen_for._loop_scope ? gen_for._loop_scope->Copy(id_map_table) : 0 ;
    if (_loop_scope) _loop_scope->SetGenerateScope() ;
    // Copy the initial assignment, terminating condition, and the incrementing statement.
    _initial = gen_for._initial ? gen_for._initial->CopyModuleItem(id_map_table) : 0 ;
    _cond = gen_for._cond ? gen_for._cond->CopyExpression(id_map_table) : 0 ;
    _rep = gen_for._rep ? gen_for._rep->CopyModuleItem(id_map_table) : 0 ;

    const MapItem *ele = id_map_table.GetItem(gen_for._block_id) ;
    // Copy the identifier which owns the forgenerate scope.
    if (ele) {
        _block_id = (VeriIdDef *)ele->Value() ;
    } else {
        _block_id = gen_for._block_id ? gen_for._block_id->CopyId(id_map_table, 1) : 0 ;
    }

    // Copy the scope as the owner is already copied.
    _scope = gen_for._scope ? gen_for._scope->Copy(id_map_table) : 0 ;
    if (_scope) {
        _scope->SetGenerateScope() ;
        VeriIdDef *loop_label = _scope->FindLocal(" gen_loop_label") ;
        if (loop_label) loop_label->SetModuleItem(this) ;
    }
    // Set the module item back-pointer of the label to this. Can we do this before scope copy ?
    if (_block_id) _block_id->SetModuleItem(this) ;

    // Get the for-generate items and copy.
    _items = CopyModuleItemArray(gen_for._items, id_map_table) ;

    if (_scope) _scope->UpdateScope(gen_for._scope, id_map_table) ;
    if (_loop_scope) _loop_scope->UpdateScope(gen_for._loop_scope, id_map_table) ;

    id_map_table.SetAsInternal(is_internal) ;
}

VeriModuleItem* VeriGenerateBlock::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriGenerateBlock(*this, id_map_table) ;
}

VeriGenerateBlock::VeriGenerateBlock(const VeriGenerateBlock &gen_block, VeriMapForCopy &id_map_table) :
    VeriModuleItem(gen_block, id_map_table),
    _block_id(0),
    _items(0),
    _scope(0)
    ,_is_elaborated(gen_block._is_elaborated)
{
    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
    // Get the generate block scope identifier. Look for the copied identifier in the 'id_map_table', if not found copy it.
    const MapItem *ele = gen_block._block_id ? id_map_table.GetItem(gen_block._block_id) : 0 ;

    if (ele) {
        _block_id = (VeriIdDef *)ele->Value() ;
    } else {
        _block_id = gen_block._block_id ? gen_block._block_id->CopyId(id_map_table, 1) : 0 ;
    }

    // Owner is copied, now scope can be copied.
    _scope = gen_block._scope ? gen_block._scope->Copy(id_map_table): 0 ;
    if (_scope) _scope->SetGenerateScope() ;
    // Set the module item back-pointer of the label to this. Can we do this before scope copy ?
    if (_block_id) _block_id->SetModuleItem(this) ;

    // Copy the generate block items, copy each and append in a new array.
    _items = CopyModuleItemArray(gen_block._items, id_map_table) ;
    if (_scope) _scope->UpdateScope(gen_block._scope, id_map_table) ;
    id_map_table.SetAsInternal(is_internal) ;
}

VeriModuleItem* VeriTable::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriTable(*this, id_map_table) ;
}

VeriTable::VeriTable(const VeriTable &table, VeriMapForCopy &id_map_table) :
    VeriModuleItem(table, id_map_table),
    _table_entries(0),
    _the_module(0),
    _is_comb(0),
    _is_seq(0)
{
    unsigned i ;
    char *item ;
    // Copy the table entries.
    if (table._table_entries) {
        _table_entries = new Array(table._table_entries->Size()) ;
        FOREACH_ARRAY_ITEM(table._table_entries, i, item) {
            _table_entries->InsertLast(Strings::save(item)) ;
        }
    }

    _is_comb = table._is_comb ;
    _is_seq = table._is_seq ;
}
VeriModuleItem* VeriPulseControl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriPulseControl(*this, id_map_table) ;
}
VeriPulseControl::VeriPulseControl(const VeriPulseControl &pulse_ctrl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(pulse_ctrl, id_map_table),
    _pulse_control_style(0),
    _path_list(0)
{
    // Copy the pulse style
    _pulse_control_style = pulse_ctrl.GetPulseControlStyle() ;
    // Copy the output paths
    _path_list = CopyExpressionArray(pulse_ctrl._path_list, id_map_table) ;
}
VeriName::VeriName(const VeriName &name, VeriMapForCopy &id_map_table) :
    VeriExpression(name, id_map_table)
{
    // VIPER #6490 : Insert overwrite name references in map as variable's
    // initial value and unpacked ranges are copied twice.
    (void) id_map_table.Insert(&name, this, 1) ;
}

VeriIdRef* VeriIdRef::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriIdRef(*this, id_map_table) ;
}

VeriName* VeriIdRef::CopyName(VeriMapForCopy &id_map_table) const
{
    return Copy(id_map_table) ;
}

VeriIdRef::VeriIdRef(const VeriIdRef &id_ref, VeriMapForCopy &id_map_table) :
    VeriName(id_ref, id_map_table),
    _name(0),
    _id(0)
{
    _id = id_ref._id ;
    const MapItem *ele = id_ref._id ? id_map_table.GetItem(id_ref._id) : 0 ;
    if (ele) _id = (VeriIdDef *)ele->Value() ;
#ifndef VERILOG_SHARE_STRINGS
    _name = Strings::save(_id ? _id->Name() : id_ref._name) ;
#else
    _name = Strings::CreateConstantString(_id ? _id->Name() : id_ref._name) ;
#endif
}

VeriName* VeriIndexedId::CopyName(VeriMapForCopy &id_map_table) const
{
    return new VeriIndexedId(*this, id_map_table) ;
}

VeriIndexedId::VeriIndexedId(const VeriIndexedId &index_id, VeriMapForCopy &id_map_table) :
    VeriName(index_id, id_map_table),
    _prefix(0),
    _idx(0),
    _id(0)
{
    // Copying prefix name
    _prefix = index_id._prefix ? index_id._prefix->CopyName(id_map_table) : 0 ;

    // Copy the index expression, if not null.
    _idx = index_id._idx ? index_id._idx->CopyExpression(id_map_table): 0 ;

    // Copying resolved prefix identifier
    _id = index_id._id ;
    const MapItem *ele = index_id._id ? id_map_table.GetItem(index_id._id) : 0 ;
    if (ele) _id = (VeriIdDef *)ele->Value() ;
}

VeriName* VeriSelectedName::CopyName(VeriMapForCopy &id_map_table) const
{
    return new VeriSelectedName(*this, id_map_table) ;
}

VeriSelectedName::VeriSelectedName(const VeriSelectedName &selected_name, VeriMapForCopy &id_map_table) :
    VeriName(selected_name, id_map_table),
    _prefix(0),
    _suffix(0),
    _prefix_id(0),
    _suffix_id(0),
    _function_type(0),
    _suffix_is_token(0),
    _cross_lang_name(selected_name._cross_lang_name)
{
    // Copying prefix name
    _prefix = selected_name._prefix ? selected_name._prefix->CopyName(id_map_table) : 0 ;

    // Copy the suffix :
#ifndef VERILOG_SHARE_STRINGS
    _suffix = Strings::save(selected_name._suffix) ;
#else
    _suffix = Strings::CreateConstantString(selected_name._suffix) ;
#endif

    // Copy the (method) function token
    _function_type = selected_name._function_type ;
    _suffix_is_token = selected_name._suffix_is_token ; // VIPER #2971 & #4961

    // Copying resolved prefix identifier pointer
    _prefix_id = selected_name._prefix_id ;
    const MapItem *ele = selected_name._prefix_id ? id_map_table.GetItem(selected_name._prefix_id) : 0 ;
    if (ele) _prefix_id = (VeriIdDef *)ele->Value() ;

    // Copying resolved suffix identifier pointer
    _suffix_id = selected_name._suffix_id ;
    ele = selected_name._suffix_id ? id_map_table.GetItem(selected_name._suffix_id) : 0 ;
    if (ele) _suffix_id = (VeriIdDef *)ele->Value() ;

    // VIPER #5248 (test_12): Copy the suffix now after we copied suffix id, consider modified suffix:
    // Copying leads to incorrect result for ids created out of generate elaboration. Cannot do this
    // here, instead modified pretty print of VeriScopeName to print correct suffix from suffix-id:
    //_suffix = Strings::save((_suffix_id)? _suffix_id->Name() : selected_name._suffix) ;
}

VeriName* VeriIndexedMemoryId::CopyName(VeriMapForCopy &id_map_table) const
{
    return new VeriIndexedMemoryId(*this, id_map_table) ;
}

VeriIndexedMemoryId::VeriIndexedMemoryId(const VeriIndexedMemoryId &index_mem_id, VeriMapForCopy &id_map_table) :
    VeriName(index_mem_id, id_map_table),
    _prefix(0),
    _indexes(0),
    _id(0)
{
    // Copying prefix name
    _prefix = index_mem_id._prefix ? index_mem_id._prefix->CopyName(id_map_table) : 0 ;

    // Copying resolved prefix identifier
    _id = index_mem_id._id ;
    const MapItem *ele = index_mem_id._id ? id_map_table.GetItem(index_mem_id._id) : 0 ;
    if (ele) _id = (VeriIdDef *)ele->Value() ;

    // Copying The index expressions
    _indexes = CopyExpressionArray(index_mem_id._indexes, id_map_table) ;
}

VeriName* VeriKeyword::CopyName(VeriMapForCopy &id_map_table) const
{
    return new VeriKeyword(*this, id_map_table) ;
}

VeriKeyword::VeriKeyword(const VeriKeyword &key_word, VeriMapForCopy &id_map_table) :
    VeriIdRef(key_word, id_map_table),
    _token(key_word._token)
{
    VeriIdDef *id = id_map_table.GetCopiedClassId(key_word.GetId()) ;
    if (id) {
        _id = id ;
#ifndef VERILOG_SHARE_STRINGS
        Strings::free(_name) ;
        _name = Strings::save(id->Name()) ;
#else
        _name = Strings::CreateConstantString(id->Name()) ;
#endif
    }
}

VeriExpression* VeriConcat::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriConcat(*this, id_map_table) ;
}

VeriConcat::VeriConcat(const VeriConcat &concat, VeriMapForCopy &id_map_table) :
    VeriExpression(concat, id_map_table),
    _exprs(0)
{
    _exprs = CopyExpressionArray(concat._exprs, id_map_table) ;
}

VeriExpression* VeriMultiConcat::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriMultiConcat(*this, id_map_table) ;
}

VeriMultiConcat::VeriMultiConcat(const VeriMultiConcat &multi_concat, VeriMapForCopy &id_map_table) :
    VeriExpression(multi_concat, id_map_table),
    _repeat(0),
    _exprs(0)
{
    _repeat = multi_concat._repeat ? multi_concat._repeat->CopyExpression(id_map_table): 0 ;
    _exprs = CopyExpressionArray(multi_concat._exprs, id_map_table) ;
}

VeriName* VeriFunctionCall::CopyName(VeriMapForCopy &id_map_table) const
{
    return new VeriFunctionCall(*this, id_map_table) ;
}

VeriFunctionCall::VeriFunctionCall(const VeriFunctionCall &func_call, VeriMapForCopy &id_map_table) :
    VeriName(func_call, id_map_table),
    _func_name(0),
    _args(0),
    _function_type(func_call._function_type)
{
    // Get function name and copy :
    _func_name = func_call._func_name ? func_call._func_name->CopyName(id_map_table) : 0 ;

    // Get the arguments and copy.
    _args = CopyExpressionArray(func_call._args, id_map_table) ;
}

VeriExpression* VeriSystemFunctionCall::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriSystemFunctionCall(*this, id_map_table) ;
}

VeriSystemFunctionCall::VeriSystemFunctionCall(const VeriSystemFunctionCall &sys_func_call, VeriMapForCopy &id_map_table) :
    VeriExpression(sys_func_call, id_map_table),
    _name(0),
    _args(0),
    _sva_clock_expression(0),
    _function_type(0)
{
    _name = Strings::save(sys_func_call._name) ;
    _args = CopyExpressionArray(sys_func_call._args, id_map_table) ;
    // Copying the clock expression
    // Memory leak fix: clock expression is not owned, no need to copy, just set the pointer:
    _sva_clock_expression = sys_func_call._sva_clock_expression ;
    _function_type = sys_func_call._function_type ;
}

VeriExpression* VeriMinTypMaxExpr::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriMinTypMaxExpr(*this, id_map_table) ;
}

VeriMinTypMaxExpr::VeriMinTypMaxExpr(const VeriMinTypMaxExpr &min_typ_max, VeriMapForCopy &id_map_table) :
    VeriExpression(min_typ_max, id_map_table),
    _min_expr(0),
    _typ_expr(0),
    _max_expr(0)
{
    // Get the minimum, typical and maximum expression, copy each, if present, create VeriMinTypMaxExpr,
    // set the line file info of the copied node, and return.
    _min_expr = min_typ_max._min_expr ? min_typ_max._min_expr->CopyExpression(id_map_table): 0 ;
    _typ_expr = min_typ_max._typ_expr ? min_typ_max._typ_expr->CopyExpression(id_map_table) : 0 ;
    _max_expr = min_typ_max._max_expr ? min_typ_max._max_expr->CopyExpression(id_map_table): 0 ;
}

VeriExpression* VeriUnaryOperator::CopyExpression(VeriMapForCopy &id_map_table) const
{
    // Create VeriUnaryOperator.
    return new VeriUnaryOperator(*this, id_map_table) ;
}

VeriExpression* VeriBinaryOperator::CopyExpression(VeriMapForCopy &id_map_table) const
{
    // Create VeriBinaryOperator with copied entities.
    return new VeriBinaryOperator(*this, id_map_table) ;
}

VeriExpression* VeriQuestionColon::CopyExpression(VeriMapForCopy &id_map_table) const
{
    // Create VeriQuestionColon with copied entities.
    return new VeriQuestionColon(*this, id_map_table) ;
}

VeriExpression* VeriEventExpression::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriEventExpression(*this, id_map_table) ;
}

VeriEventExpression::VeriEventExpression(const VeriEventExpression &event_expr, VeriMapForCopy &id_map_table) :
    VeriExpression(event_expr, id_map_table),
    _edge(0),
    _expr(0),
    _iff_condition(0),
    _event_with_at(0)
{
    _edge = event_expr._edge ;
    _expr = event_expr._expr ? event_expr._expr->CopyExpression(id_map_table): 0 ;
    _iff_condition = event_expr._iff_condition ? event_expr._iff_condition->CopyExpression(id_map_table): 0 ;
}

VeriExpression* VeriPortConnect::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriPortConnect(*this, id_map_table) ;
}

VeriPortConnect::VeriPortConnect(const VeriPortConnect &port_connect, VeriMapForCopy &id_map_table) :
    VeriExpression(port_connect, id_map_table),
    _port_ref_name(0),
    _connection(0)
{
    _port_ref_name = Strings::save(port_connect._port_ref_name) ;

    // Get the expression, and copy.
    _connection = port_connect._connection ? port_connect._connection->CopyExpression(id_map_table): 0 ;
}

VeriExpression* VeriPortOpen::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriPortOpen(*this, id_map_table) ;
}

VeriPortOpen::VeriPortOpen(const VeriPortOpen &port_open, VeriMapForCopy &id_map_table) :
    VeriExpression(port_open, id_map_table)
{}

VeriExpression* VeriAnsiPortDecl::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriAnsiPortDecl(*this, id_map_table) ;
}

VeriAnsiPortDecl::VeriAnsiPortDecl(const VeriAnsiPortDecl &ansi_port_decl, VeriMapForCopy &id_map_table) :
    VeriExpression(ansi_port_decl, id_map_table),
    _dir(0),
   _is_local(ansi_port_decl._is_local),
    _data_type(0),
    _ids(0)
   ,_qualifier(ansi_port_decl._qualifier)
{
    _dir = ansi_port_decl._dir ;

    // Copy the data type
    _data_type = ansi_port_decl._data_type ? ansi_port_decl._data_type->CopyDataType(id_map_table) : 0 ;

    unsigned i ;
    VeriIdDef *id ;
    _ids = CopyIdArray(ansi_port_decl._ids, id_map_table) ;
    FOREACH_ARRAY_ITEM(_ids, i, id) if (id && _data_type) id->SetDataTypeForCopy(_data_type) ;
}

VeriExpression* VeriTimingCheckEvent::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriTimingCheckEvent(*this, id_map_table) ;
}

VeriTimingCheckEvent::VeriTimingCheckEvent(const VeriTimingCheckEvent &time_check_event, VeriMapForCopy &id_map_table) :
    VeriExpression(time_check_event, id_map_table),
    _edge(0),
    _edge_desc_str(0),
    _terminal_desc(0),
    _condition(0)
{
    _edge = time_check_event._edge ;
    _edge_desc_str = Strings::save(time_check_event._edge_desc_str) ;

    // Get the expressions and copy.
    _terminal_desc = time_check_event._terminal_desc ? time_check_event._terminal_desc->CopyExpression(id_map_table): 0 ;
    _condition = time_check_event._condition ? time_check_event._condition->CopyExpression(id_map_table): 0 ;
}

// Change name of an identifier definition
unsigned VeriIdDef::ChangeName(char* name)
{
    if (LocalScope()) return 0 ;
#ifdef VERILOG_SHARE_STRINGS
    _name = Strings::CreateConstantString(name) ; // used shared string
    Strings::free(name) ;
#else
    Strings::free(_name) ;
    _name = name ;
#endif
    return 1 ;
}

// Copy identifiers
VeriIdDef *VeriIdDef::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriIdDef(*this, id_map_table) ;
}

VeriIdDef::VeriIdDef(const VeriIdDef &id_def, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriTreeNode(id_def, id_map_table),
    _name(0)
#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    , _owning_scope(0) // set owning scope when identifier is added to a scope
#endif
#ifdef VERILOG_STORE_ID_PRAGMAS
    , _id_pragmas(0) // identifier pragmas (char*->char* table)
    //CARBON_BEGIN
    , _line_infos(0) // line information for pragmas (char*->char* table)
    //CARBON_END
#endif
{
    const char *copy_name = (new_name) ? new_name : id_def._name ;
#ifdef VERILOG_SHARE_STRINGS
    _name = Strings::CreateConstantString(copy_name) ;
#else
    _name = Strings::save(copy_name) ;
#endif
    (void) id_map_table.Insert(&id_def, this) ;

#ifdef VERILOG_STORE_ID_PRAGMAS
    // copy identifier pragmas (char*->char* table).
    MapIter mi ;
    char *name ;
    char *value ;
    FOREACH_MAP_ITEM(id_def._id_pragmas, mi, &name, &value) {
        AddIdPragma(name,value) ; // add to current _id_pragma table.
    }

    //CARBON_BEGIN
    // copy line infos for pragmas (char*->char* table).
    MapIter mi2 ;
    char *pragma_name ;
    char *line_info ;
    FOREACH_MAP_ITEM(id_def._line_infos, mi2, &pragma_name, &line_info) {
        AddPragmaLineInfo(pragma_name,line_info) ; // add to current _line_infos table.
    }
    //CARBON_END
#endif
}

VeriIdDef* VeriVariable::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    return new VeriVariable(*this, id_map_table, copy_initial_value) ;
}

VeriVariable::VeriVariable(const VeriVariable &var, VeriMapForCopy &id_map_table, unsigned copy_initial_value, const char *new_name /*=0*/)
  : VeriIdDef(var, id_map_table, new_name),
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    _link_var(0),
    _fsm_constants(0),
#endif
    _initial_value(0),
    _type(0),
    _dir(0),
    _implicit_net(0),
    _is_used(0),
    _is_assigned(0),
    _is_concurrent_assigned(0),
    _is_concurrent_used(0),
    _is_assigned_before_used(0),
    _is_used_before_assigned(0),
    _can_be_onehot(0),
    _can_be_dual_port_ram(0),
    _can_be_multi_port_ram(0),
    _is_member(0),
    _is_modport_port(0),
    _is_ansi_port(0),
    _is_constant(0),
    _is_automatic(0),
    _is_iterator(0),
    _is_localvar(0),
    _is_private(0),
    _is_protected(0),
    _kind(0), // VIPER #5949
    _is_implicit_non_ansi_port(0), // VIPER #6506
    _is_ram_overridden(0),
    _is_clocking_decl_id(0),
    _is_se_created_vi(0), // VIPER #6992
    _is_used_in_for_loop_index(0), // VIPER #5239
    _is_static_class_variable(0),
    _is_import(0),
    _is_export(0),
    _is_onehot_overridden(0),
    _unused(0),
    _data_type(0)
    ,_dimensions(0)
{
    // Copy and set the initial value.
    if (copy_initial_value) {
        _initial_value = var._initial_value ? var._initial_value->CopyExpression(id_map_table) : 0 ;
    }

    _type = var._type ;
    _kind = var._kind ; // VIPER #5949
    _dir = var._dir ;
    _implicit_net = var._implicit_net ;
    _is_used = var._is_used ;
    _is_assigned = var._is_assigned ;
    _is_concurrent_assigned  = var._is_concurrent_assigned ;
    _is_concurrent_used      = var._is_concurrent_used ;
    _is_assigned_before_used = var._is_assigned_before_used ;
    _is_used_before_assigned = var._is_used_before_assigned ;
    _can_be_onehot = var._can_be_onehot ;
    _can_be_dual_port_ram = var._can_be_dual_port_ram ;
    _can_be_multi_port_ram = var._can_be_multi_port_ram ;
    _is_member = var._is_member ;
    _is_modport_port = var._is_modport_port ;
    _is_ansi_port = var._is_ansi_port ;
    _is_constant = var._is_constant ;
    _is_automatic = var._is_automatic ;
    _is_iterator = var._is_iterator ;
    _is_localvar = var._is_localvar ;
    _is_private = var._is_private ;
    _is_protected = var._is_protected ;
    _is_implicit_non_ansi_port = var._is_implicit_non_ansi_port ;
    _is_ram_overridden = var._is_ram_overridden ;
    _is_clocking_decl_id = var._is_clocking_decl_id ;
    _is_se_created_vi = var._is_se_created_vi ;
    _is_used_in_for_loop_index = var._is_used_in_for_loop_index ; // VIPER #5239
    _is_static_class_variable = var._is_static_class_variable ; // VIPER #6907
    _is_import = var._is_import ;
    _is_export = var._is_export ;
    _is_onehot_overridden = var._is_onehot_overridden ;

    // Copy and insert the dimensions.^M
    if (copy_initial_value) {
        _dimensions = var._dimensions ? var._dimensions->CopyRange(id_map_table) : 0 ;
    }

#ifdef VERILOG_CREATE_ONEHOT_FSMS
    _link_var = var._link_var ;
    const MapItem *ele = var._link_var ? id_map_table.GetItem(var._link_var) : 0 ;
    if (ele) _link_var = (VeriVariable *)ele->Value() ;

    if (var._fsm_constants) {
        _fsm_constants = new Map(STRING_HASH) ;
        MapIter iter ;
        char* num ;
        char *expr ;
        // VIPER #7995: Key and Value of '_fsm_constants' are allocated char* now
        FOREACH_MAP_ITEM(var._fsm_constants, iter, &num, &expr) {
            //(void) _fsm_constants->Insert((void*)(long)num, expr ? expr->CopyExpressionArray(id_map_table): 0) ;
            (void) _fsm_constants->Insert(Strings::save(num), Strings::save(expr)) ;
        }
    }
#endif
}
void VeriVariable::SetDataTypeForCopy(VeriDataType *data_type)
{
    if (data_type && (!_data_type || !_data_type->Type())) {
        _data_type = data_type ;
    }
}
void VeriParamId::SetDataTypeForCopy(VeriDataType *data_type)
{
    // Only set datatype back pointer if not yet set. Change made after VeriEnum back pointers (MB)
    if (data_type && !_data_type) {
        _data_type = data_type ;
    }
}

VeriIdDef* VeriInstId::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    return new VeriInstId(*this, id_map_table, copy_initial_value) ;
}

VeriInstId::VeriInstId(const VeriInstId &inst_id, VeriMapForCopy &id_map_table, unsigned copy_port_connects, const char *new_name /*=0*/) :
    VeriIdDef(inst_id, id_map_table, new_name),
    _dimensions(0),
    _port_connects((copy_port_connects) ? inst_id._port_connects.Size() : 1),
// back-pointer :
    _the_instance(0)
{
    // Copying Instantiation range
    if (copy_port_connects) {
        _dimensions = inst_id._dimensions ? inst_id._dimensions->CopyRange(id_map_table) : 0 ;

        // Copying The port connections
        Array *pc_array = CopyExpressionArray(&inst_id._port_connects, id_map_table) ;
        _port_connects.Append(pc_array) ;
        delete pc_array ;
    }

    // Set back-pointer ?
    _the_instance = inst_id._the_instance ;
}

VeriIdDef* VeriModuleId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriModuleId(*this, id_map_table) ;
}

VeriModuleId::VeriModuleId(const VeriModuleId &mod_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(mod_id, id_map_table, new_name),
    _local_scope(0),
    _tree(0)
    , _is_extern(mod_id._is_extern)
    , _is_package(mod_id._is_package)
    , _is_export_subprog(mod_id._is_export_subprog)

{ }

VeriIdDef *VeriConfigurationId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriConfigurationId(*this, id_map_table) ;
}

VeriConfigurationId::VeriConfigurationId(const VeriConfigurationId &mod_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriModuleId(mod_id, id_map_table, new_name)
{ }

VeriIdDef* VeriUdpId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriUdpId.
    return new VeriUdpId(*this, id_map_table) ;
}

VeriUdpId::VeriUdpId(const VeriUdpId &udp_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriModuleId(udp_id, id_map_table, new_name)
{}

VeriIdDef* VeriTaskId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriTaskId.
    return new VeriTaskId(*this, id_map_table) ;
}

VeriTaskId::VeriTaskId(const VeriTaskId &task_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(task_id, id_map_table, new_name),
    _local_scope(0),
    _tree(0),
    _is_automatic(0),
    _is_forkjoin(0)
    , _is_import(0)
    , _is_export(0)
{
    // copy bit flags
    _is_automatic = task_id._is_automatic ;
    _is_forkjoin = task_id._is_forkjoin ;
    _is_import = task_id._is_import ;
    _is_export = task_id._is_export ;
}
VeriIdDef* VeriFunctionId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriFunctionId.
    return new VeriFunctionId(*this, id_map_table) ;
}

VeriFunctionId::VeriFunctionId(const VeriFunctionId &func_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriVariable(func_id, id_map_table, 1 /* copy initial value */, new_name), // Note: Copy initial value for historical reason, it should have an initial value
    _local_scope(0),
    _tree(0)
    , _function_type(func_id._function_type)
{ }

VeriIdDef* VeriGenVarId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriGenVarId.
    return new VeriGenVarId(*this, id_map_table) ;
}

VeriGenVarId::VeriGenVarId(const VeriGenVarId &gen_var_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(gen_var_id, id_map_table, new_name),
    _initial_value(0)
{
    // Copy the initial value expression :
    _initial_value = gen_var_id._initial_value ? gen_var_id._initial_value->CopyExpression(id_map_table) : 0 ;
}

VeriIdDef* VeriParamId::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    // Create VeriParamId.
    return new VeriParamId(*this, id_map_table, copy_initial_value) ;
}

VeriParamId::VeriParamId(const VeriParamId &param_id, VeriMapForCopy &id_map_table, unsigned copy_initial_value, const char *new_name /*=0*/) :
    VeriIdDef(param_id, id_map_table, new_name),
    _data_type(0),
    _dimensions(0),
    _initial_value(0),
    _param_type(param_id._param_type),
    _type(param_id._type),
    _is_used(param_id._is_used),
    _is_assigned(param_id._is_assigned),
    _is_type(param_id._is_type)
    ,_is_elaborated(param_id._is_elaborated)
    , _is_processing(param_id._is_processing)
    , _id_implicit(param_id._id_implicit)
#ifdef VERILOG_PATHPULSE_PORTS
    ,_path_pulse_ports(0)
#endif
{
    // Copy the parameter dimensions
    if (param_id._dimensions) _dimensions = param_id._dimensions->CopyRange(id_map_table) ;

    // Copy the initial value if requested
    if (copy_initial_value) {
        _initial_value = param_id._initial_value ? param_id._initial_value->CopyExpression(id_map_table) : 0 ;
    }

#ifdef VERILOG_PATHPULSE_PORTS
    _path_pulse_ports = param_id._path_pulse_ports ? param_id._path_pulse_ports->Copy(id_map_table) : 0 ;
#endif
}

VeriIdDef* VeriBlockId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriBlockId.
    return new VeriBlockId(*this, id_map_table) ;
}

VeriBlockId::VeriBlockId(const VeriBlockId &block_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(block_id, id_map_table, new_name),
    _local_scope(0),
    _tree(0)
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    , _is_unnamed_blk(block_id._is_unnamed_blk)
    , _orig_name(0)
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
{
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    _orig_name = (block_id._orig_name) ? Strings::save(block_id._orig_name): 0 ;
//#endif
}

VeriIdDef* VeriInterfaceId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriInterfaceId.
    return new VeriInterfaceId(*this, id_map_table) ;
}

VeriInterfaceId::VeriInterfaceId(const VeriInterfaceId &interface_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriModuleId(interface_id, id_map_table, new_name),
    _ele_ids(0)
{ }

VeriIdDef* VeriProgramId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriProgramId.
    return new VeriProgramId(*this, id_map_table) ;
}

VeriProgramId::VeriProgramId(const VeriProgramId &program_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriModuleId(program_id, id_map_table, new_name)
{ }

// VIPER #5710:
VeriIdDef* VeriCheckerId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriCheckerId.
    return new VeriCheckerId(*this, id_map_table) ;
}

VeriCheckerId::VeriCheckerId(const VeriCheckerId &checker_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriModuleId(checker_id, id_map_table, new_name)
{ }

VeriIdDef* VeriTypeId::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    // Create VeriTypeId.
    return new VeriTypeId(*this, id_map_table, copy_initial_value) ;
}

VeriTypeId::VeriTypeId(const VeriTypeId &type_id, VeriMapForCopy &id_map_table, unsigned copy_initial_value /* = 1 */, const char *new_name /*=0*/) :
    VeriVariable(type_id, id_map_table, copy_initial_value, new_name),
    _local_scope(0),
    _tree(0),
    _is_processing(0), // Needed for class type elaboration
    _forward_type_defined(type_id._forward_type_defined) // VIPER #7951 (duplicate_classes)
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895:
    , _unconstraint_unpacked_dim_count(0)
    , _unconstraint_packed_dim_count(0)
    , _vhdl_package_name(0)
    , _vhdl_lib_name(0)
#endif
    , _elab_created(type_id._elab_created)
{ }

VeriIdDef* VeriModportId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriModportId.
    return new VeriModportId(*this, id_map_table) ;
}

VeriModportId::VeriModportId(const VeriModportId &modport_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(modport_id, id_map_table, new_name),
    _local_scope(0),
    _tree(0),
    _ele_ids(0)
{ }
VeriIdDef* VeriOperatorId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create operator id
    return new VeriOperatorId(*this, id_map_table) ;
}

VeriOperatorId::VeriOperatorId(const VeriOperatorId &operator_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(operator_id, id_map_table, new_name),
    _oper_type(operator_id._oper_type),
    _tree(0)
{}
VeriIdDef* VeriClockingId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriClockingId(*this, id_map_table) ;
}
VeriClockingId::VeriClockingId(const VeriClockingId &clocking_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriVariable(clocking_id, id_map_table, 1 /* copy initial value */, new_name), // Note: Copy initial value for historical reason, it should have an initial value
    _local_scope(0),
    _tree(0),
    _is_default(clocking_id._is_default)
{}
VeriIdDef * VeriProductionId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    // Create VeriProductionId.
    return new VeriProductionId(*this, id_map_table) ;
}
VeriProductionId::VeriProductionId(const VeriProductionId &production_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(production_id, id_map_table, new_name),
    _prod(production_id.GetProduction())
{}
VeriIdDef * VeriNamedPort::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    return new VeriNamedPort(*this, id_map_table, copy_initial_value) ; // Create VeriNamedPort
}
VeriNamedPort::VeriNamedPort(const VeriNamedPort &named_port, VeriMapForCopy &id_map_table, unsigned copy_port_expr, const char *new_name /*=0*/) :
    VeriVariable(named_port, id_map_table, 1 /* copy initial value */, new_name), // Note: Copy initial value for historical reason, it should have an initial value
    _port_expr(0),
    _is_processing(0)
{
    // VIPER #2676 : Copy port expression if specified
    if (copy_port_expr) {
        _port_expr = (named_port._port_expr) ? named_port._port_expr->CopyExpression(id_map_table) : 0 ;
    }
}
// VIPER #3384
VeriIdDef * VeriCovgOptionId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriCovgOptionId(*this, id_map_table) ; // Create VeriCovgOptionId
}
VeriCovgOptionId::VeriCovgOptionId(const VeriCovgOptionId &covg_opt_id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(covg_opt_id, id_map_table, new_name),
    _data_type(0)
{
    _data_type = covg_opt_id._data_type ? covg_opt_id._data_type->CopyDataType(id_map_table) : 0 ;
}

VeriIdDef *VeriSeqPropertyFormal::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriSeqPropertyFormal(*this, id_map_table) ;
}
VeriSeqPropertyFormal::VeriSeqPropertyFormal(const VeriSeqPropertyFormal &formal, VeriMapForCopy &id_map_table, unsigned copy_initial_value, const char *new_name /*=0*/) :
    VeriVariable(formal, id_map_table, copy_initial_value, new_name),
    _actual(0),
    _is_processing(0)
{}
VeriIdDef* VeriBinsId::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    return new VeriBinsId(*this, id_map_table, copy_initial_value) ;
}
VeriBinsId::VeriBinsId(const VeriBinsId &formal, VeriMapForCopy &id_map_table, unsigned copy_initial_value, const char *new_name /*=0*/) :
    VeriVariable(formal, id_map_table, copy_initial_value, new_name)
{}

VeriIdDef *VeriLetId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriLetId(*this, id_map_table) ;
}
VeriLetId::VeriLetId(const VeriLetId &id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriIdDef(id, id_map_table, new_name),
    _local_scope(0),
    _tree(0),
    _is_processing(0)
{}
VeriIdDef *VeriExternForkjoinTaskId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriExternForkjoinTaskId(*this, id_map_table) ;
}
VeriExternForkjoinTaskId::VeriExternForkjoinTaskId(const VeriExternForkjoinTaskId &id, VeriMapForCopy &id_map_table, const char *new_name /*=0*/) :
    VeriTaskId(id, id_map_table, new_name),
    _trees(0)
{}
VeriIdDef *VeriLabelId::CopyId(VeriMapForCopy &id_map_table, unsigned copy_initial_value) const
{
    return new VeriLabelId(*this, id_map_table, copy_initial_value) ;
}
VeriLabelId::VeriLabelId(const VeriLabelId &id, VeriMapForCopy &id_map_table, unsigned copy_initial_value, const char *new_name /*=0*/) :
    VeriBlockId(id, id_map_table, new_name),
    _scope(0)
{
    if (copy_initial_value) _scope = id._scope ? id._scope->Copy(id_map_table) : 0 ;
}
VeriIdDef *VeriPrototypeId::CopyId(VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/) const
{
    return new VeriPrototypeId(*this, id_map_table) ;
}
VeriPrototypeId::VeriPrototypeId(const VeriPrototypeId &id, VeriMapForCopy &id_map_table, unsigned /*copy_initial_value*/, const char *new_name /*=0*/) :
    VeriIdDef(id, id_map_table, new_name),
    _dir(id._dir),
    _actual_name(0),
    _actual_id(0)
{
    if (id._actual_id) {
        const MapItem *ele = id_map_table.GetItem(id._actual_id) ;
        _actual_id = ele ? (VeriIdDef *)ele->Value() : id._actual_id ;
    }
    if (id._actual_name) _actual_name = id._actual_name->CopyName(id_map_table) ;
}

VeriStatement* VeriNullStatement::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriNullStatement(*this, id_map_table) ;
}

VeriNullStatement::VeriNullStatement(const VeriNullStatement &null_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(null_stmt, id_map_table)
{ }

VeriStatement* VeriBlockingAssign::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriBlockingAssign(*this, id_map_table) ;
}

VeriBlockingAssign::VeriBlockingAssign(const VeriBlockingAssign &block_assign, VeriMapForCopy &id_map_table) :
    VeriStatement(block_assign, id_map_table),
    _lval(0),
    _oper_type(0),
    _control(0),
    _val(0)
{
    // Get the left hand side and the right hand side, and copy them.
    _lval = block_assign._lval ? block_assign._lval->CopyExpression(id_map_table) : 0 ;
    _val = block_assign._val ? block_assign._val->CopyExpression(id_map_table) : 0 ;

    // Copy the VeriDelayOrEventControl if present.
    _control = block_assign._control ? block_assign._control->Copy(id_map_table): 0 ;
    _oper_type = block_assign._oper_type ;
}

VeriStatement* VeriNonBlockingAssign::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriNonBlockingAssign(*this, id_map_table) ;
}

VeriNonBlockingAssign::VeriNonBlockingAssign(const VeriNonBlockingAssign &non_blocking_assign, VeriMapForCopy &id_map_table) :
    VeriStatement(non_blocking_assign, id_map_table),
    _lval(0),
    _control(0),
    _val(0)
{
    // Get the left hand side and the right hand side, and copy them.
    _lval = non_blocking_assign._lval->CopyExpression(id_map_table) ;
    _val = non_blocking_assign._val->CopyExpression(id_map_table) ;

    // Copy the VeriDelayOrEventControl if present.
    _control = non_blocking_assign._control ? non_blocking_assign._control->Copy(id_map_table): 0 ;
}

VeriStatement* VeriGenVarAssign::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriGenVarAssign(*this, id_map_table) ;
}

VeriGenVarAssign::VeriGenVarAssign(const VeriGenVarAssign &gen_var_assign, VeriMapForCopy &id_map_table) :
    VeriStatement(gen_var_assign, id_map_table),
    _name(0),
    _id(0),
    _value(0),
    _oper_type(0)
{
    _name = Strings::save(gen_var_assign._name) ;
    // Get the rhs expression and copy it after null checking.
    _value = gen_var_assign._value? gen_var_assign._value->CopyExpression(id_map_table): 0 ;

    // Get the copied identifier from the 'id_map_table' or use the original one, if identifier is present
    // and create VeriGenVarAssign with that identifier or create VeriGenVarAssign with the name.
    if (gen_var_assign._id) {
        const MapItem *ele = id_map_table.GetItem(gen_var_assign._id) ;
        _id = ele ? (VeriIdDef *)ele->Value() : gen_var_assign._id ;
    }
    _oper_type = gen_var_assign._oper_type ;
}

VeriStatement* VeriAssign::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriAssign(*this, id_map_table) ;
}

VeriAssign::VeriAssign(const VeriAssign &assign, VeriMapForCopy &id_map_table) :
    VeriStatement(assign, id_map_table),
    _assign(0)
{
    // Get the assignment and copy it, after null checking.
    _assign = assign._assign? assign._assign->Copy(id_map_table): 0 ;
}

VeriStatement* VeriDeAssign::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriDeAssign(*this, id_map_table) ;
}

VeriDeAssign::VeriDeAssign(const VeriDeAssign &de_assign, VeriMapForCopy &id_map_table) :
    VeriStatement(de_assign, id_map_table),
    _lval(0)
{
    // Get the expression, copy it, and create a new VeriDeAssign with the copied expression.
    _lval = de_assign._lval ? de_assign._lval->CopyExpression(id_map_table) : 0 ;
}

VeriStatement* VeriForce::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriForce(*this, id_map_table) ;
}

VeriForce::VeriForce(const VeriForce &force, VeriMapForCopy &id_map_table) :
    VeriStatement(force, id_map_table),
    _assign(0)
{
    // Get the assign, copy it, and create a new VeriForce, with the copied node.
    // Next set its line file info, and return the copied node.
    _assign = force._assign? force._assign->Copy(id_map_table): 0 ;
}

VeriStatement* VeriRelease::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriRelease(*this, id_map_table) ;
}

VeriRelease::VeriRelease(const VeriRelease &rel, VeriMapForCopy &id_map_table) :
    VeriStatement(rel, id_map_table),
    _lval(0)
{
    _lval = rel._lval ? rel._lval->CopyExpression(id_map_table) : 0 ;
}

VeriStatement* VeriTaskEnable::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriTaskEnable(*this, id_map_table) ;
}

VeriTaskEnable::VeriTaskEnable(const VeriTaskEnable &task_enable, VeriMapForCopy &id_map_table) :
    VeriStatement(task_enable,id_map_table),
    _task_name(0),
    _args(0),
    _method_type(task_enable._method_type)
{
    // Get the task name and copy
    _task_name = task_enable._task_name ? task_enable._task_name->CopyName(id_map_table) : 0 ;

    // Copy the task enable arguments.
    _args = CopyExpressionArray(task_enable._args, id_map_table) ;
}

VeriStatement* VeriSystemTaskEnable::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriSystemTaskEnable(*this, id_map_table) ;
}

VeriSystemTaskEnable::VeriSystemTaskEnable(const VeriSystemTaskEnable &sys_task_enable, VeriMapForCopy &id_map_table) :
    VeriStatement(sys_task_enable, id_map_table),
    _name(0),
    _args(0),
    _function_type(0)
{
    // Copy the system task name
    _name = Strings::save(sys_task_enable._name) ;

    // Copy the system task token :
    _function_type = sys_task_enable._function_type ;

    // Copy and insert the arguments in a new array.
    _args = CopyExpressionArray(sys_task_enable._args, id_map_table) ;
}

VeriStatement* VeriDelayControlStatement::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriDelayControlStatement(*this, id_map_table) ;
}

VeriDelayControlStatement::VeriDelayControlStatement(const VeriDelayControlStatement &delay_control_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(delay_control_stmt, id_map_table),
    _delay(0),
    _stmt(0)
    ,_is_cycle_delay(0)
{
    // Get the delay and the statement, and copy them after null checking.
    _delay = delay_control_stmt._delay ? delay_control_stmt._delay->CopyExpression(id_map_table): 0 ;

    _stmt = delay_control_stmt._stmt ? delay_control_stmt._stmt->CopyStatement(id_map_table): 0 ;
}

VeriStatement* VeriEventControlStatement::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriEventControlStatement(*this, id_map_table) ;
}

VeriEventControlStatement::VeriEventControlStatement(const VeriEventControlStatement &event_control_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(event_control_stmt, id_map_table),
    _at(0),
    _stmt(0)
    ,_sva_clock_expr_index(0)
{
    // Get the event control, and copy it.
    _at = CopyExpressionArray(event_control_stmt._at, id_map_table) ;
    // Get the statement, and copy.
    _stmt = event_control_stmt._stmt ? event_control_stmt._stmt->CopyStatement(id_map_table) : 0 ;
    // Get the _sva_clock_expr_index, and copy.
    _sva_clock_expr_index = event_control_stmt._sva_clock_expr_index ;
}

VeriStatement* VeriConditionalStatement::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriConditionalStatement(*this, id_map_table) ;
}

VeriConditionalStatement::VeriConditionalStatement(const VeriConditionalStatement &cond_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(cond_stmt, id_map_table),
    _if_expr(0),
    _then_stmt(0),
    _else_stmt(0)
{
    // Get the condition, 1 statement and the 0 statement and copy them after null checking.
    _if_expr = cond_stmt._if_expr ? cond_stmt._if_expr->CopyExpression(id_map_table): 0 ;
    _then_stmt = cond_stmt._then_stmt ? cond_stmt._then_stmt->CopyStatement(id_map_table): 0 ;
    _else_stmt = cond_stmt._else_stmt ? cond_stmt._else_stmt->CopyStatement(id_map_table): 0 ;
}

VeriStatement* VeriCaseStatement::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriCaseStatement(*this, id_map_table) ;
}

VeriCaseStatement::VeriCaseStatement(const VeriCaseStatement &case_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(case_stmt, id_map_table),
    _case_style(0),
    _cond(0),
    _case_items(0)
    ,_type(case_stmt._type)
    , _is_full_case(case_stmt._is_full_case)
    , _is_parallel_case(case_stmt._is_parallel_case)
{
    _case_style = case_stmt._case_style ;

    // Get the case condition and copy it.
    _cond = case_stmt._cond ? case_stmt._cond->CopyExpression(id_map_table): 0 ;

    VeriCaseItem *node ;
    unsigned i ;
    // Get the case item conditions and copy them.
    if (case_stmt._case_items) {
        _case_items = new Array(case_stmt._case_items->Size()) ;
        FOREACH_ARRAY_ITEM(case_stmt._case_items, i, node) {
            VeriCaseItem *new_node = node ? node->Copy(id_map_table) : 0 ;
            _case_items->InsertLast(new_node) ;
        }
    }
}

VeriStatement* VeriForever::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriForever(*this, id_map_table) ;
}

VeriForever::VeriForever(const VeriForever &for_ever, VeriMapForCopy &id_map_table) :
    VeriLoop(for_ever, id_map_table)
{}

VeriStatement* VeriRepeat::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriRepeat(*this, id_map_table) ;
}

VeriRepeat::VeriRepeat(const VeriRepeat &repeat, VeriMapForCopy &id_map_table) :
    VeriLoop(repeat, id_map_table),
    _cond(0)
{
    // Get the repeat condition, and copy, if it is not null.
    _cond = repeat._cond ? repeat._cond->CopyExpression(id_map_table): 0 ;
}

VeriStatement* VeriWhile::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriWhile(*this, id_map_table) ;
}

VeriWhile::VeriWhile(const VeriWhile &veri_while, VeriMapForCopy &id_map_table) :
    VeriLoop(veri_while, id_map_table),
    _cond(0)
{
    // Get the while loop condition and copy it.
    _cond = veri_while._cond ? veri_while._cond->CopyExpression(id_map_table): 0 ;
}

VeriStatement* VeriFor::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriFor(*this, id_map_table) ;
}

VeriFor::VeriFor(const VeriFor &veri_for, VeriMapForCopy &id_map_table) :
    VeriLoop(veri_for, id_map_table),
    _initials(0),
    _cond(0),
    _reps(0)
{
    // Get the initial statements, and copy.
    _initials = CopyModuleItemArray(veri_for._initials,id_map_table) ;

    // Get the terminating condition, and copy.
    _cond = veri_for._cond ? veri_for._cond->CopyExpression(id_map_table) : 0;

    // Get the repeatition statements, and copy.
    _reps = CopyStatementArray(veri_for._reps,id_map_table) ;
}

VeriStatement* VeriWait::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriWait(*this, id_map_table) ;
}

VeriWait::VeriWait(const VeriWait &wait, VeriMapForCopy &id_map_table) :
    VeriStatement(wait, id_map_table),
    _cond(0),
    _stmt(0)
{
    // Get the wait condition, and copy.
    _cond = (wait._cond) ? wait._cond->CopyExpression(id_map_table) : 0 ;

    // Get the statement, and copy.
    _stmt = (wait._stmt) ? wait._stmt->CopyStatement(id_map_table) : 0 ;
}

VeriStatement* VeriDisable::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriDisable(*this, id_map_table) ;
}

VeriDisable::VeriDisable(const VeriDisable &disable, VeriMapForCopy &id_map_table) :
    VeriStatement(disable, id_map_table),
    _task_block_name(0)
{
    // Copy the task/block reference :
    _task_block_name = disable._task_block_name ? disable._task_block_name->CopyName(id_map_table) : 0 ;
}

VeriStatement* VeriEventTrigger::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return  new VeriEventTrigger(*this, id_map_table) ;
}

VeriEventTrigger::VeriEventTrigger(const VeriEventTrigger &event_trig, VeriMapForCopy &id_map_table) :
    VeriStatement(event_trig, id_map_table),
    _trigger_oper(0),
    _control(0),
    _event_name(0)
{
    _trigger_oper = event_trig._trigger_oper ;
    // Copy the VeriDelayOrEventControl if present.
    _control = event_trig._control ? event_trig._control->Copy(id_map_table) : 0 ;
    // Copy the even reference :
    _event_name = event_trig._event_name ? event_trig._event_name->CopyName(id_map_table) : 0 ;
}

VeriStatement* VeriSeqBlock::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriSeqBlock(*this, id_map_table) ;
}

VeriSeqBlock::VeriSeqBlock(const VeriSeqBlock &seq_block, VeriMapForCopy &id_map_table) :
    VeriStatement(seq_block, id_map_table),
    _label(0),
    _decl_items(0),
    _stmts(0),
    _scope(0)
{
    // VeriSeqBlock can have a scope, and its owner is the label. So copy of the identifier
    // has to be done first, then the scope and then the declarations and statements.
    if (seq_block._label) {
        const MapItem *ele = id_map_table.GetItem(seq_block._label) ;
        if (ele) {
            _label = (VeriIdDef *)ele->Value() ;
        } else {
            _label = seq_block._label->CopyId(id_map_table, 1) ;
        }
    }

    // label is copied, now copy the scope.
    _scope = seq_block._scope ? seq_block._scope->Copy(id_map_table) : 0 ;
    // Set the module item back-pointer of the label to this. Can we do this before scope copy ?
    if (_label) _label->SetModuleItem(this) ;

    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
    // Declarations / Statements
    _decl_items = CopyModuleItemArray(seq_block._decl_items, id_map_table) ;

    _stmts = CopyStatementArray(seq_block._stmts, id_map_table) ;
    if (_scope) _scope->UpdateScope(seq_block._scope, id_map_table) ;
    id_map_table.SetAsInternal(is_internal) ;
}

VeriStatement* VeriParBlock::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriParBlock(*this, id_map_table) ;
}

VeriParBlock::VeriParBlock(const VeriParBlock &par_block, VeriMapForCopy &id_map_table) :
    VeriStatement(par_block, id_map_table),
    _label(0),
    _decl_items(0),
    _stmts(0),
    _scope(0),
    _closing_token(0)
{
    // VeriParBlock can have a scope, and its owner is the label. So copy of the identifier
    // has to be done first, then the scope and then the declarations and statements.
    if (par_block._label) {
        const MapItem *ele = id_map_table.GetItem(par_block._label) ;
        if (ele) {
            _label = (VeriIdDef *)ele->Value() ;
        } else {
            _label = par_block._label->CopyId(id_map_table, 1) ;
        }
    }

    // label is copied, now copy the scope.
    _scope = par_block._scope ? par_block._scope->Copy(id_map_table) : 0 ;
    // Set the module item back-pointer of the label to this. Can we do this before scope copy ?
    if (_label) _label->SetModuleItem(this) ;

    unsigned is_internal = id_map_table.IsInternal() ;
    id_map_table.SetAsInternal(1) ;
    // Declarations / Statements
    _decl_items = CopyModuleItemArray(par_block._decl_items, id_map_table) ;

    _stmts = CopyStatementArray(par_block._stmts, id_map_table) ;

    _closing_token = par_block._closing_token ;
    if (_scope) _scope->UpdateScope(par_block._scope, id_map_table) ;
    id_map_table.SetAsInternal(is_internal) ;
}

VeriPrimitive* VeriPrimitive::Copy(VeriMapForCopy &id_map_table) const
{
    return  new VeriPrimitive(*this, id_map_table) ;
}

VeriPrimitive::VeriPrimitive(const VeriPrimitive &prim, VeriMapForCopy &id_map_table) :
    VeriModule(prim, id_map_table)
{
    unsigned i ;
    VeriModuleItem *mod_item ;
    FOREACH_ARRAY_ITEM(_module_items, i, mod_item) {
        if (mod_item && mod_item->IsTable()) mod_item->SetTheModule(this) ; // Set module backpointer for VeriTable
    }
}

VeriIdDef* VeriTreeNode::CreateInstantiatedUnitName(VeriIdDef *module_id, const char *name, VeriLibrary *lib, VeriMapForCopy &id_map_table)
{
    if (!module_id) return 0 ;
    char *unique_name = 0 ;
    VeriScope *mod_scope = module_id->LocalScope() ;
    VeriScope *scope = (mod_scope && !lib) ? mod_scope->Upper() : 0 ;

    if (!name) {
        const char *id_name = module_id->GetName() ;
        unsigned i = 0 ;
        char int_name[32] ;
        // Loop untill we get a name that is not in the library as module name and not in the local scope
        do {
            Strings::free(unique_name) ;
            sprintf(int_name, "%d", ++i) ;
            unique_name = Strings::save(id_name, "_copy", int_name) ;
        } while ((lib && lib->GetModule(unique_name, 1)) || (scope && scope->FindLocal(unique_name))) ;
    } else {
        unique_name = Strings::save(name) ;
    }
    VeriIdDef *ret_id = module_id->CopyWithName(unique_name, id_map_table) ;
    Strings::free(unique_name) ;
    if (ret_id) ret_id->SetLinefile(module_id->Linefile()) ;
    return ret_id ;
}

// The following routines copy some specific identifiers with a new name
VeriIdDef *VeriIdDef::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriIdDef(*this, id_map_table, name) ;
}

VeriIdDef *VeriVariable::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    // Note: Initial value is not copied for historical reason; we never copied initial value before.
    return new VeriVariable(*this, id_map_table, 0 /* do not copy initial value */, name) ;
}

VeriIdDef *VeriParamId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    // Note: Initial value is not copied for historical reason; we never copied initial value before.
    return new VeriParamId(*this, id_map_table, 0 /* do not copy initial value */, name) ;
}
VeriIdDef *VeriBlockId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriBlockId(*this, id_map_table, name) ;
}
VeriIdDef *VeriLabelId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriLabelId(*this, id_map_table, 0 /* do not copy initial value */, name) ;
}
VeriIdDef *VeriLetId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriLetId(*this, id_map_table, name) ;
}

VeriIdDef *VeriModuleId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    VeriIdDef *module_id = new VeriModuleId(*this, id_map_table, name) ;

// No need to copy it now as we are calling copy constructor, it gets copied with VeriIdDef itself.
//#ifdef VERILOG_STORE_ID_PRAGMAS
//    // Viper #3728 : Do NOT lose the pragma during static elab
//    // copy identifier pragmas (char*->char* table).
//    MapIter mi ;
//    char *name1 ;
//    char *value ;
//    FOREACH_MAP_ITEM(_id_pragmas, mi, &name1, &value) {
//        module_id->AddIdPragma(name1,value) ; // add to module_id._id_pragma table.
//    }
//#endif

    return module_id ;
}
VeriIdDef *VeriConfigurationId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriConfigurationId(*this, id_map_table, name) ;
}
VeriIdDef *VeriUdpId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriUdpId(*this, id_map_table, name) ;
}
VeriIdDef *VeriInterfaceId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriInterfaceId(*this, id_map_table, name) ;
}
VeriIdDef *VeriProgramId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriProgramId(*this, id_map_table, name) ;
}
VeriIdDef *VeriCheckerId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    return new VeriCheckerId(*this, id_map_table, name) ;
}
VeriIdDef *VeriTypeId::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    // Note: Initial value is not copied for historical reason; we never copied initial value before.
    return new VeriTypeId(*this, id_map_table, 0 /* do not copy initial value */, name) ;
}
VeriIdDef *VeriSeqPropertyFormal::CopyWithName(const char *name, VeriMapForCopy &id_map_table)
{
    // Note: Initial value is not copied for historical reason; we never copied initial value before.
    return new VeriSeqPropertyFormal(*this, id_map_table, 0 /* do not copy initial value */, name) ;
}
// The following routine copies a module with a given name.
VeriModuleItem *VeriModule::CopyWithName(const char *name, VeriMapForCopy &id_map_table, unsigned add_copied_obj)
{
    VeriModule *ret_ptr = 0 ;
    // Create an copy of this module, but give it a new name. The new name created should
    // not create name conflict with the existing module names in the same library.

    // Modules declared in $root are kept in library. Nested modules reside in $root as another root item
    VeriLibrary * lib = GetLibrary() ;
    // Only for nested module _id->LocalScope() is not null
    VeriScope *id_scope = (_id) ? _id->LocalScope() : 0 ;
    VeriScope *declared_scope = (id_scope) ? id_scope->Upper() : 0 ;
    VeriModule *if_exist = lib ? lib->GetModule(name, 1): 0 ;

    VeriIdDef *exist_id = 0 ;
    // Check in declared scope if name conflict exist
    if (!if_exist) exist_id = declared_scope ? declared_scope->FindLocal(name) : 0 ;

    if (if_exist || exist_id) name = 0 ;
    VeriIdDef *new_id = CreateInstantiatedUnitName(_id, name, lib, id_map_table) ;

    // Copy the module as a whole.
    ret_ptr = CopyModule(id_map_table) ;

    // VIPER #7508 : Update base_class_scope and extended scope, as there can be
    // forward typedef of class.
    VeriScope *copied_scope = ret_ptr->GetScope() ;
    if (copied_scope) copied_scope->UpdateScopeBackPointers(id_scope, id_map_table) ;

    if (add_copied_obj) {
        if (lib) { // Normal module
            if (!lib->AddModule(ret_ptr)) ret_ptr = 0 ;
            // ret_ptr->SetLibrary(lib) ; // RD: Already done by AddModule.
        } else if (declared_scope) { // Nested module
            (void) declared_scope->Declare(new_id) ; // declare copied id
            VeriIdDef *owner = declared_scope->GetOwner() ;
            // Nested modules are only defined within module
            if (owner && owner->IsModule()) {
                VeriModule *parent_mod = owner->GetModule() ;
                Array *module_items = parent_mod ? parent_mod->GetModuleItems() : 0 ;
                if (module_items) module_items->InsertLast(ret_ptr) ;
            }
        }
        // VIPER #7633 : Check whether copied scope is attached as child of
        // 'declared_scope'. If not do that
        Set *children = declared_scope ? declared_scope->GetChildrenScopes(): 0 ;
        if (ret_ptr && children && declared_scope && !children->Get(ret_ptr->GetScope())) {
            declared_scope->AddChildScope(ret_ptr->GetScope()) ;
        }
    }
    // Return the copied module with new name.
    return ret_ptr ;
}

VeriModuleItem::VeriModuleItem(const VeriModuleItem &mod_item, VeriMapForCopy &id_map_table) :
    VeriTreeNode(mod_item, id_map_table),
    _qualifiers(0)
{
    Map *attributes = mod_item.GetAttributes() ;
    if (attributes) {
        Map *new_attributes = new Map(STRING_HASH) ;
        MapIter iter ;
        char *name ;
        VeriExpression *val ;
        FOREACH_MAP_ITEM(attributes, iter, &name, &val) {
            VeriExpression *node = val ? val->CopyExpression(id_map_table) : 0 ;
            (void) new_attributes->Insert(Strings::save(name), node) ;
        }

        SetAttributes(new_attributes) ;
    }
    _qualifiers = mod_item._qualifiers ;
}

VeriCommentNode *VeriCommentNode::CopyComment() const
{
    // Create a temporary VeriMapForCopy object:
    VeriMapForCopy id_map_table ;
    // Call the copy constructor:
    return (new VeriCommentNode(*this, id_map_table)) ;
}

VeriCommentNode::VeriCommentNode(const VeriCommentNode &tree_node, VeriMapForCopy &id_map_table) :
    VeriTreeNode(tree_node, id_map_table),
    _str(0)
{
    // Just copy the comment string:
    _str = Strings::save(tree_node._str) ;
}

VeriDataType::VeriDataType(const VeriDataType &data_type, VeriMapForCopy &id_map_table) :
    VeriExpression(data_type, id_map_table),
    _type(0),
    _signing(0),
    _is_virtual_interface(0),
    _is_processing(0),
    _dimensions(0)
{
    _type = data_type._type ;
    _signing = data_type._signing ;
    _is_virtual_interface = data_type._is_virtual_interface ;
    _is_processing = data_type._is_processing ;
    _dimensions = data_type._dimensions ? data_type._dimensions->CopyRange(id_map_table) : 0 ;
    (void) id_map_table.Insert(&data_type, this) ;
}

// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
VeriPathPulseVal::VeriPathPulseVal(const VeriPathPulseVal &path_pulse, VeriMapForCopy &id_map_table) :
    VeriExpression(path_pulse, id_map_table),
    _reject_limit(0),
    _error_limit(0)
{
    // Copy the reject and error limits
    _reject_limit = path_pulse._reject_limit ? path_pulse._reject_limit->CopyExpression(id_map_table) : 0 ;
    _error_limit = path_pulse._error_limit ? path_pulse._error_limit->CopyExpression(id_map_table) : 0 ;
}

VeriBinaryOperator::VeriBinaryOperator(const VeriBinaryOperator &bin_op, VeriMapForCopy &id_map_table) :
    VeriExpression(bin_op, id_map_table),
    _oper_type(0),
    _parenthesized(0),
    _left(0),
    _right(0)
{
    _oper_type = bin_op._oper_type ;
    _parenthesized = bin_op._parenthesized ;
    _left = bin_op._left ? bin_op._left->CopyExpression(id_map_table) : 0 ;
    _right = bin_op._right ? bin_op._right->CopyExpression(id_map_table) : 0 ;
}

VeriExpression::VeriExpression(const VeriExpression &veri_expr, VeriMapForCopy &id_map_table) :
   VeriTreeNode(veri_expr, id_map_table)
{
    Map *attributes = veri_expr.GetAttributes() ;
    if (attributes) {
        Map *new_attributes = new Map(STRING_HASH) ;
        MapIter iter ;
        char *name ;
        VeriExpression *val ;
        FOREACH_MAP_ITEM(attributes, iter, &name, &val) {
            VeriExpression *node = val ? val->CopyExpression(id_map_table) : 0 ;
            (void) new_attributes->Insert(Strings::save(name), node) ;
        }
        SetAttributes(new_attributes) ;
    }
}

VeriQuestionColon::VeriQuestionColon(const VeriQuestionColon &ques_colon, VeriMapForCopy &id_map_table) :
    VeriExpression(ques_colon, id_map_table),
    _if_expr(0),
    _then_expr(0),
    _else_expr(0)
{
    _if_expr = ques_colon._if_expr ? ques_colon._if_expr->CopyExpression(id_map_table) : 0 ;
    _then_expr = ques_colon._then_expr ? ques_colon._then_expr->CopyExpression(id_map_table) : 0 ;
    _else_expr = ques_colon._else_expr ? ques_colon._else_expr->CopyExpression(id_map_table) : 0 ;
}

VeriUnaryOperator::VeriUnaryOperator(const VeriUnaryOperator &unary_op, VeriMapForCopy &id_map_table) :
    VeriExpression(unary_op, id_map_table),
    _oper_type(0),
    _parenthesized(0),
    _arg(0)
{
    _oper_type = unary_op._oper_type ;
    _parenthesized = unary_op._parenthesized ;
    _arg = unary_op._arg ? unary_op._arg->CopyExpression(id_map_table) : 0 ;
}

VeriStatement::VeriStatement(const VeriStatement &stmt, VeriMapForCopy &id_map_table) :
    VeriModuleItem(stmt, id_map_table)
    , _opening_label(0)
{
    // VeriStatement can have a opening label. Declared in upper scope. If not yet copied, we need to do so now...
    if (stmt._opening_label) {
        const MapItem *ele = id_map_table.GetItem(stmt._opening_label) ;
        if (ele) {
            _opening_label = (VeriIdDef *)ele->Value() ;
            if (_opening_label && !_opening_label->GetScope() && stmt._opening_label->GetScope()) {
                // VIPER #8303 : Copy the scope of label if exist
                // Scope of label is not copied while copying the identifier
                VeriScope *label_scope = stmt._opening_label->GetScope() ;
                VeriScope *new_scope = label_scope ? label_scope->Copy(id_map_table) : 0 ;
                _opening_label->SetScope(new_scope) ;
            }
        } else {
            _opening_label = stmt._opening_label->CopyId(id_map_table, 1) ;
        }

        // Set moduleitem label back-pointer
        if (_opening_label) _opening_label->SetModuleItem(this) ;
    }
}

VeriLoop::VeriLoop(const VeriLoop &loop, VeriMapForCopy &id_map_table) :
    VeriStatement(loop, id_map_table),
    _stmt(0),
    _scope(0)
{
    // Owner is already copied, now we can copy the scope.
    _scope = loop._scope ? loop._scope->Copy(id_map_table) : 0 ;
    if (_scope) {
        // Update the loop(break) and continue labels :
        VeriIdDef *loop_label = _scope->FindLocal(" loop_label") ;
        if (loop_label) loop_label->SetModuleItem(this) ;

        loop_label = _scope->FindLocal(" continue_label") ;
        if (loop_label) loop_label->SetModuleItem(this) ;
    }
    _stmt = loop._stmt ? loop._stmt->CopyStatement(id_map_table) : 0 ;
    if (_scope) _scope->UpdateScope(loop._scope, id_map_table) ;
}

// Copy interface as a module :
VeriModule *VeriInterface::CopyModule(VeriMapForCopy &id_map_table) const
{
    return new VeriInterface(*this, id_map_table) ;
}
// Copy interface as a module item :
VeriModuleItem *VeriInterface::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriInterface(*this, id_map_table) ;
}

VeriInterface::VeriInterface(const VeriInterface &interf, VeriMapForCopy &id_map_table) :
    VeriModule(interf, id_map_table)
{
    // Set the data type of the modport ports copied for the interface:
    //id_map_table.ProcessModports(_scope, this) ;
}

// Copy program as a module :
VeriModule *VeriProgram::CopyModule(VeriMapForCopy &id_map_table) const
{
    return new VeriProgram(*this, id_map_table) ;
}
// Copy program as a module item :
VeriModuleItem *VeriProgram::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriProgram(*this, id_map_table) ;
}

VeriProgram::VeriProgram(const VeriProgram &program, VeriMapForCopy &id_map_table) :
    VeriModule( program, id_map_table)
{ }

// VIPER #5710:
// Copy checker as a module :
VeriModule *VeriChecker::CopyModule(VeriMapForCopy &id_map_table) const
{
    return new VeriChecker(*this, id_map_table) ;
}

// Copy checker as a module item :
VeriModuleItem *VeriChecker::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriChecker(*this, id_map_table) ;
}

VeriChecker::VeriChecker(const VeriChecker &checker, VeriMapForCopy &id_map_table) :
    VeriModule(checker, id_map_table)
{ }

// Copy package as a module :
VeriModule *VeriPackage::CopyModule(VeriMapForCopy &id_map_table) const
{
    return new VeriPackage(*this, id_map_table) ;
}
// Copy package as a module item :
VeriModuleItem *VeriPackage::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriPackage(*this, id_map_table) ;
}

VeriPackage::VeriPackage(const VeriPackage &package, VeriMapForCopy &id_map_table) :
    VeriModule( package, id_map_table)
{
    // All fields are standard VeriModule fields.
}

// VeriClass copy :
VeriModuleItem *VeriClass::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriClass(*this, id_map_table) ;
}

VeriClass::VeriClass(const VeriClass &veri_class, VeriMapForCopy &id_map_table) :
    VeriModuleItem(veri_class, id_map_table),
    _id(0),
    _new_id(0),
    _parameter_connects(0),
    _base_class_name(0),
    _interface_classes(0),
    _items(0),
    _scope(0),
    _parameters(0),
    _has_static_members(0),
    _is_processing(0)
    , _is_instantiated(veri_class._is_instantiated)
    , _contain_copied_version(0)
    , _is_resolved(veri_class._is_resolved)
    , _orig_class_name(0)
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    , _owning_scope(0)
#endif
{
    VeriIdDef *id = veri_class._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // VIPER #4542: If only this class is going to be copied, do not change the reference
    // of this class within class body. That will stop recursive class processing.
    // But if this class is going to be copied along with its container, change its reference
    // inside its body with copied class reference. Otherwise while copying a class along with
    // its container module, we will lose the information that this class contains recursive
    // instance.
    unsigned is_container_copied = (id_map_table.Size() > 1) ? 1 : 0 ;
    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }
    // Set module item back-pointer
    if (_id) _id->SetModuleItem(this) ;

    // Owner is already copied, now we can copy the scope, before going further.
    _scope = veri_class._scope ? veri_class._scope->Copy(id_map_table) : 0 ;

    // Set the design file specific class name from which this class is copied
    // If the class from which it is copying is itself copied class, set the
    // class name from which that class is copied, otherwise set the class name
    // from which this is copying
    _orig_class_name = veri_class._orig_class_name ? Strings::save(veri_class._orig_class_name): (id ? Strings::save(id->Name()): 0) ;

    // Within the class body, reference of itself should not point to copied class id
    // So remove mapping for class id from id_map_table
    // VIPER #4542 : Remove its reference only when this class is going to be copied alone.
    if (!is_container_copied) (void) id_map_table.Remove(id) ;
    // But if there is 'this' within class body, that should point to copied class id
    id_map_table.SetCopiedClassId(id, _id) ;
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    if (veri_class._owning_scope) {
        ele = id_map_table.GetItem(veri_class._owning_scope) ;
        _owning_scope = (ele) ? (VeriScope*)ele->Value(): veri_class._owning_scope ;
    }
#endif
    // Copying parameter list
    _parameter_connects = CopyModuleItemArray(veri_class._parameter_connects, id_map_table) ;

    // Copying module item list
    _items = CopyModuleItemArray(veri_class._items, id_map_table) ;

    // Copying Array of VeriIdDef*. Created in constructor from 'parameter_connects'
    _parameters = CopyIdArray(veri_class._parameters, id_map_table) ;

    // Copy base class ref
    _base_class_name = veri_class._base_class_name ? veri_class._base_class_name->CopyName(id_map_table) : 0 ;

    // Copy interface class refs
    _interface_classes = CopyExpressionArray(veri_class._interface_classes, id_map_table) ;
    // Now re-insert class id mapping in id_map_table
    (void) id_map_table.Insert(id, _id) ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (!item || !(item->IsFunctionDecl() || item->IsTaskDecl())) continue ;
        // VIPER #4030 : Functions declared inside a class are automatic if not
        // explicitly specified as static
        // VIPER #4853 : Tasks declared inside a class are automatic if not
        // explicitly specified as static
        VeriIdDef *func_task_id = item->GetId() ;
        if (func_task_id && !func_task_id->IsAutomatic() && !item->GetQualifier(VERI_STATIC)) {
            func_task_id->SetIsAutomatic() ;
        }
        if (func_task_id && func_task_id->GetFunctionType()==VERI_METHOD_NEW && !_new_id) _new_id = func_task_id ;
    }
    if (_scope) _scope->UpdateScope(veri_class._scope, id_map_table) ;
    _has_static_members = veri_class._has_static_members ;
}

VeriModuleItem *VeriClass::CopyWithName(const char *name, VeriMapForCopy &id_map_table, unsigned add_copied_obj)
{
    VeriModuleItem *ret_ptr = 0 ;

    // Find the scope where 'this' class resides
    VeriScope *declared_scope = (_id) ? _id->GetOwningScope(): 0 ;
    // Search in copied package scopes
    Map *pkg_scope_map = id_map_table.GetPkgScopeMap() ;
    MapItem *ele = pkg_scope_map ? pkg_scope_map->GetItem(declared_scope): 0 ;
    if (ele) declared_scope  = (VeriScope*)ele->Value() ;
    VeriIdDef *if_exist = declared_scope ? declared_scope->FindLocal(name) : 0 ;

    if (if_exist) name = 0 ;
    VeriIdDef *new_id = CreateInstantiatedUnitName(_id, name, 0, id_map_table) ;

    // Copy class as a whole
    ret_ptr = CopyModuleItem(id_map_table) ;

    VeriModuleItem *item ;
    unsigned i ;
    // Add the copied interface in proper place. Interface can resides within $root/within interface
    if (add_copied_obj) {
        if (declared_scope) (void) declared_scope->Declare(new_id) ;
        VeriIdDef *owner = declared_scope ? declared_scope->GetOwner() : 0 ;
        VeriModuleItem *parent_item = owner ? owner->GetModuleItem() : 0 ;
        Array *items = parent_item ? parent_item->GetItems() : 0 ;
        if (items) {
            // Need to add copied class just before current class
            // Find current class position
            unsigned class_pos = 0 ;
            unsigned type_def_pos = 0 ;
            unsigned num_pos_found = 0 ;
            unsigned forward_type_def = (_id) ? _id->IsForwardTypeDefined() : 0 ; // VIPER #7951 (duplicate_classes)
            const Array *ids ;
            FOREACH_ARRAY_ITEM(items, i, item) {
                if (!item) continue ;
                if (item == this) {
                    class_pos = i ;
                    if (!forward_type_def) break ; // Done
                    num_pos_found++ ; // Found class_pos
                }

                if (!forward_type_def) continue ; // No forward typedef, do not proceed further

                // VIPER #7951 (duplicate_classes): Find the position of the forward typedef:
                ids = item->GetIds() ;
                if (ids && (ids->Size() == 1) && (_id == (VeriIdDef *)ids->GetFirst())) {
                    type_def_pos = i ;
                    num_pos_found++ ; // Found type_def_pos
                }
                if (num_pos_found == 2) break ; // Done: Both of class_pos and type_def_pos are found
            }
            items->InsertBefore(class_pos, ret_ptr) ;

            if (forward_type_def && new_id) {
                // VIPER #7951 (duplicate_classes): Create a forward typedef of the new class type too:
                VeriDataDecl *new_forward_type_def = new VeriDataDecl(VERI_TYPEDEF, 0 /*no data type*/, new_id) ;
                items->InsertBefore(type_def_pos, new_forward_type_def) ;
            }
        }
        // VIPER #7633 : Check whether copied scope is attached as child of
        // 'declared_scope'. If not do that
        Set *children = declared_scope ? declared_scope->GetChildrenScopes(): 0 ;
        if (ret_ptr && children && declared_scope && !children->Get(ret_ptr->GetScope())) {
            declared_scope->AddChildScope(ret_ptr->GetScope()) ;
        }
    }
    // Class can have out-of-block method declarations. Copy those also
    // The prototype of out-of-block methods are within the class and their
    // method id contains a back-pointer to out-of-block method body.

    // Iterate over class items
    FOREACH_ARRAY_ITEM(_items, i, item) {
        VeriIdDef *id = item ? item->GetId() : 0 ;
        if (!id || !(id->IsFunction() || id->IsTask()) || (id->GetModuleItem() == item)) continue ;

        // This is a method having a out-of-block decl
        VeriModuleItem *body = id->GetModuleItem() ;
        VeriIdDef *new_method_id = (VeriIdDef*)id_map_table.GetValue(id) ;
        if (new_method_id) new_method_id->ClearDataType() ;

        // Copy this body
        VeriModuleItem *new_body = (body) ? body->CopyModuleItem(id_map_table) : 0 ;
        if (new_body && add_copied_obj) {
            // Copied module item is to be added :
            // VIPER #5248 (test_12): Add the sub-program in the owner of the module item
            // referenced first by its VeriName *name. For example, add a function with name
            // C1::C2::foo in the owner of C1. This is because from that place C1 is visible:
            VeriName *prefix_name = new_body->GetSubprogramName() ;
            while (prefix_name && prefix_name->GetPrefix()) {
                // Go to the first name referenced:
                prefix_name = prefix_name->GetPrefix() ;
            }
            VeriIdDef *prefix_id = (prefix_name) ? prefix_name->GetId() : 0 ;
            VeriModuleItem *owner_item = (prefix_id) ? prefix_id->GetModuleItem() : 0 ; // Find its module item
            VeriScope *owner_scope = (owner_item) ? owner_item->GetScope() : 0 ; // Scope of the module item
            if (owner_scope) owner_scope = owner_scope->Upper() ; // Scope of the owner of the module item
            // If the above owner_scope is not available add as usual declard scope:
            VeriIdDef *owner = (owner_scope) ? owner_scope->GetOwner() : (declared_scope ? declared_scope->GetOwner() : 0) ;
            VeriModuleItem *parent_item = owner ? owner->GetModuleItem() : 0 ;
            Array *items = parent_item ? parent_item->GetItems() : 0 ;
            if (items) items->InsertLast(new_body) ;
        }
    }
    // Return the copied interface with new name.
    return ret_ptr ;
}

VeriModuleItem *VeriPropertyDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriPropertyDecl(*this, id_map_table) ;
}

VeriPropertyDecl::VeriPropertyDecl(const VeriPropertyDecl &property_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(property_decl, id_map_table),
    _id(0),
    _ansi_io_list(0),
    _decls(0),
    _spec(0),
    _scope(0),
    _ports(0),
    _is_processing(0),
    _feedback_net(0),
    _formal_to_actual(0)
{
    VeriIdDef *id = property_decl._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }

    // Set the id's module item backpointer to this
    if (_id) _id->SetModuleItem(this) ;

    // Owner is already copied, now we can copy the scope, before going further.
    _scope = property_decl._scope ? property_decl._scope->Copy(id_map_table) : 0 ;

    // Copy (ansi) IO list :
    _ansi_io_list = CopyExpressionArray(property_decl._ansi_io_list, id_map_table) ;

    // Copying module item
    _decls = CopyModuleItemArray(property_decl._decls, id_map_table) ;

    // Copying property spec
    _spec = property_decl._spec ? property_decl._spec->CopyExpression(id_map_table) : 0 ;

    // Copying Array of VeriIdDef
    _ports = CopyIdArray(property_decl._ports, id_map_table) ;
    if (_scope) _scope->UpdateScope(property_decl._scope, id_map_table) ;
}

VeriModuleItem *VeriSequenceDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriSequenceDecl(*this, id_map_table) ;
}

VeriSequenceDecl::VeriSequenceDecl(const VeriSequenceDecl &sequence_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(sequence_decl, id_map_table),
    _id(0),
    _ansi_io_list(0),
    _decls(0),
    _spec(0),
    _scope(0),
    _ports(0),
    _is_processing(0)
{
    VeriIdDef *id = sequence_decl._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }

    // Set the id's module item backpointer to this
    if (_id) _id->SetModuleItem(this) ;

    // Owner is already copied, now we can copy the scope, before going further.
    _scope = sequence_decl._scope ? sequence_decl._scope->Copy(id_map_table) : 0 ;

    // Copy (ansi) IO list :
    _ansi_io_list = CopyExpressionArray(sequence_decl._ansi_io_list, id_map_table) ;

    // Copying module item list
    _decls = CopyModuleItemArray(sequence_decl._decls, id_map_table) ;

    // Copying sequence spec
    _spec = sequence_decl._spec ? sequence_decl._spec->CopyExpression(id_map_table) : 0 ;

    // Copying Array of VeriIdDef
    _ports = CopyIdArray(sequence_decl._ports, id_map_table) ;
    if (_scope) _scope->UpdateScope(sequence_decl._scope, id_map_table) ;
}

VeriModuleItem *VeriModport::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriModport(*this, id_map_table) ;
}

VeriModport::VeriModport(const VeriModport &modport, VeriMapForCopy &id_map_table) :
    VeriModuleItem(modport, id_map_table),
    _modport_decls(0)
{
    // Copying Module item list
    _modport_decls = CopyModuleItemArray(modport._modport_decls, id_map_table) ;
    id_map_table.AddModport(this) ;
}

VeriModuleItem *VeriModportDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriModportDecl(*this, id_map_table) ;
}

VeriModportDecl::VeriModportDecl(const VeriModportDecl &modport_port_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(modport_port_decl, id_map_table),
    _id(0),
    _modport_port_decls(0),
    _scope(0),
    _ports(0)
{
    VeriIdDef *id = modport_port_decl._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }

    // Owner is already copied, now we can copy the scope, before going further.
    _scope = modport_port_decl._scope ? modport_port_decl._scope->Copy(id_map_table) : 0 ;

    // Copying module item list
    _modport_port_decls = CopyModuleItemArray(modport_port_decl._modport_port_decls, id_map_table) ;

    // Copying Array of VeriIdDef
    _ports = CopyIdArray(modport_port_decl._ports, id_map_table) ;

    if (_scope) _scope->UpdateScope(modport_port_decl._scope, id_map_table) ;
    // VIPER #2975 : Set declaration back pointer to modport decl identifier
    if (_id) _id->SetModuleItem(this) ;
}

VeriModuleItem *VeriClockingDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriClockingDecl(*this, id_map_table) ;
}

VeriClockingDecl::VeriClockingDecl(const VeriClockingDecl &clocking_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(clocking_decl, id_map_table),
    _id(0),
    _clocking_event(0),
    _clocking_items(0),
    _scope(0)
{
    VeriIdDef *id = clocking_decl._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        // VIPER #4935: Add null check
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }

    // Owner is already copied, now we can copy the scope, before going further.
    _scope = clocking_decl._scope ? clocking_decl._scope->Copy(id_map_table) : 0 ;

    // Copying Module item list
    _clocking_items = CopyModuleItemArray(clocking_decl._clocking_items, id_map_table) ;

    // Copying clock expression
    _clocking_event = clocking_decl._clocking_event ? clocking_decl._clocking_event->CopyExpression(id_map_table) : 0 ;

    // Set clocking declaration back pointer to _id
    if (_id) _id->SetModuleItem(this) ;

    if (_scope) _scope->UpdateScope(clocking_decl._scope, id_map_table) ;
    // VIPER #3191 : Signals used within a clocking declaration must be defined
    // in some upper scope. Set data type to those signals (used within clocking decl)
    // from their actual definition (in upper scope)
    unsigned i, j ;
    VeriModuleItem *item ;
    VeriScope *upper = (_scope) ? _scope->Upper() : 0 ;
    FOREACH_ARRAY_ITEM(_clocking_items, i, item) {
        if (!item) continue ;
        Array *ids = item->GetIds() ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            VeriIdDef *clocking_sig_id  =  (upper) ? upper->Find(id->Name()) : 0 ;
            VeriExpression *init_val = id->GetInitialValue() ;
            if (clocking_sig_id && !init_val) { // Signal exists in upper scope, set data type from that
                id->SetDataType(clocking_sig_id->GetDataType()) ;
            }
            if (init_val) { // Initial value exists, set data type from that
                VeriIdDef *r_id = init_val->GetId() ;
                if (r_id) id->SetDataType(r_id->GetDataType()) ;
            }
        }
    }
}

VeriModuleItem *VeriConstraintDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriConstraintDecl(*this, id_map_table) ;
}

VeriConstraintDecl::VeriConstraintDecl(const VeriConstraintDecl &constraint_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(constraint_decl, id_map_table),
    _id(0),
    _constraint_blocks(0),
    _scope(constraint_decl._scope),
    _name(0)
{
    VeriIdDef *id = constraint_decl._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }

    // Copying constraint blocks
    _constraint_blocks = CopyExpressionArray(constraint_decl._constraint_blocks, id_map_table) ;

    // Copy the name reference
    _name = constraint_decl._name ? constraint_decl._name->CopyName(id_map_table) : 0 ;

    // Set the scope from map table ConstraintDecl does not own any scope
    ele = id_map_table.GetItem(_scope) ;
    if (ele) _scope = (VeriScope*) ele->Value() ;
}

VeriModuleItem *VeriBindDirective::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriBindDirective(*this, id_map_table) ;
}

VeriBindDirective::VeriBindDirective(const VeriBindDirective &bind_directive, VeriMapForCopy &id_map_table) :
    VeriModuleItem(bind_directive, id_map_table),
    _target_mod_name(0),
    _target_inst_list(0),
    _instantiation(0),
    _scope(0)
    ,_is_static_elaborated(bind_directive._is_static_elaborated)  // VIPER #7968:
{
    // Copy the target module name
    _target_mod_name = Strings::save(bind_directive._target_mod_name) ;

    // Copy the target instantiation list
    _target_inst_list = CopyExpressionArray(bind_directive._target_inst_list, id_map_table) ;

    // Copy the scope
    _scope = bind_directive._scope ? bind_directive._scope->Copy(id_map_table) : 0 ; // VIPER 2476

    // Copy the instantiation
    _instantiation = bind_directive._instantiation ? bind_directive._instantiation->CopyModuleItem(id_map_table) : 0 ;

    (void) id_map_table.Insert(&bind_directive, this) ; // Store bind directive in map
    if (_scope) _scope->UpdateScope(bind_directive._scope, id_map_table) ;
}

VeriModuleItem *VeriOperatorBinding::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriOperatorBinding(*this, id_map_table) ;
}

VeriOperatorBinding::VeriOperatorBinding(const VeriOperatorBinding &binding, VeriMapForCopy &id_map_table) :
    VeriModuleItem( binding, id_map_table),
    _oper_id(0),
    _data_type(0),
    _func_name(0),
    _formals(0)
{
    _oper_id = binding._oper_id ;
    const MapItem *ele = id_map_table.GetItem(_oper_id) ;

    // Operator identifier is the owner of the parent scope, so get it from map
    if (ele) {
        _oper_id = (VeriIdDef *)ele->Value() ;
        _oper_id->SetModuleItem(this) ; // Set module item back-pointer
    }

    // Copy return type of associated function prototype
    if (binding._data_type) _data_type = binding._data_type->CopyDataType(id_map_table) ;

    // Copy function name
    if (binding._func_name) _func_name = binding._func_name->CopyName(id_map_table) ;

    // Copy formals
    if (binding._formals) _formals = new Array(binding._formals->Size()) ;
    unsigned i ;
    VeriDataType *formal ;
    FOREACH_ARRAY_ITEM(binding._formals, i, formal) {
        VeriDataType *new_formal = formal ? formal->CopyDataType(id_map_table) : 0 ;
        if (_formals) _formals->InsertLast(new_formal) ;
    }
}
VeriModuleItem *VeriNetAlias::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriNetAlias(*this, id_map_table) ;
}
VeriNetAlias::VeriNetAlias(const VeriNetAlias &net_alias, VeriMapForCopy &id_map_table) :
    VeriModuleItem(net_alias, id_map_table),
    _left(0),
    _members(0)
{
    _left = (net_alias._left) ? net_alias._left->CopyExpression(id_map_table) : 0 ; // Copy left member

    _members = CopyExpressionArray(net_alias._members, id_map_table) ; // Copy other members
}
VeriModuleItem *VeriTimeUnit::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriTimeUnit(*this, id_map_table) ;
}
VeriTimeUnit::VeriTimeUnit(const VeriTimeUnit &time_unit, VeriMapForCopy &id_map_table) :
    VeriModuleItem(time_unit, id_map_table),
    _type(time_unit._type),
    _literal(0),
    _time_precision(0)
{
    _literal = (time_unit._literal) ? time_unit._literal->CopyExpression(id_map_table) : 0 ; // Copy time literal
    _time_precision = (time_unit._time_precision) ? time_unit._time_precision->CopyExpression(id_map_table) : 0 ; // Copy time literal
}
VeriModuleItem *VeriClockingSigDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriClockingSigDecl(*this, id_map_table) ;
}
VeriClockingSigDecl::VeriClockingSigDecl(const VeriClockingSigDecl &sig_decl, VeriMapForCopy &id_map_table) :
    VeriDataDecl(sig_decl, id_map_table),
    _clk_dir(0),
    _o_clk_dir(0)
{
    _clk_dir = sig_decl._clk_dir ? sig_decl._clk_dir->Copy(id_map_table) : 0 ; // Copy clocking direction
    _o_clk_dir = sig_decl._o_clk_dir ? sig_decl._o_clk_dir->Copy(id_map_table) : 0 ; // Copy clocking direction
}
VeriModuleItem *VeriCovergroup::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriCovergroup(*this, id_map_table) ;
}
VeriCovergroup::VeriCovergroup(const VeriCovergroup &covergroup_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(covergroup_decl, id_map_table),
    _id(0),
    _port_connects(0),
    _event(0),
    _sample_func(0),
    _items(0),
    _scope(0),
    _ports(0)
{
    // VeriCoverGroup can have a scope, and its owner is the id. So copy of the identifier
    // has to be done first, then the scope and then the declarations and statements.
    if (covergroup_decl._id) {
        const MapItem *ele = id_map_table.GetItem(covergroup_decl._id) ;
        if (ele) {
            _id = (VeriIdDef *)ele->Value() ;
        } else {
            _id = covergroup_decl._id->CopyId(id_map_table, 1) ;
        }
    }
    if (_id) {
        // Set back-pointer from id to this covergroup :
        _id->SetModuleItem(this) ;
        _id->SetType(VERI_COVERGROUP) ; // Set the type to covergroup
    }
    // Copy the scope
    _scope = (covergroup_decl._scope) ? covergroup_decl._scope->Copy(id_map_table) : 0 ;
    // Copy the port connects
    _port_connects = CopyExpressionArray(covergroup_decl._port_connects, id_map_table) ;
    // Copy the event
    _event = (covergroup_decl._event) ? covergroup_decl._event->CopyExpression(id_map_table) : 0 ;
    // Copy sample function :
    _sample_func = (covergroup_decl._sample_func) ? covergroup_decl._sample_func->CopyModuleItem(id_map_table): 0 ;
    // Copy the items
    _items = CopyModuleItemArray(covergroup_decl._items, id_map_table) ;
    // Copy the ports
    _ports = CopyIdArray(covergroup_decl._ports, id_map_table) ;
    if (_scope) _scope->UpdateScope(covergroup_decl._scope, id_map_table) ;
}
VeriModuleItem *VeriCoverageOption::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriCoverageOption(*this, id_map_table) ;
}
VeriCoverageOption::VeriCoverageOption(const VeriCoverageOption &coverage_option, VeriMapForCopy &id_map_table) :
    VeriModuleItem(coverage_option, id_map_table),
    _option(0),
    _value(0)
{
    // Copy the name
    _option = (coverage_option._option) ?  coverage_option._option->CopyName(id_map_table) : 0 ;
    // Copy the value expression
    _value = (coverage_option._value) ?  coverage_option._value->CopyExpression(id_map_table) : 0 ;
}
VeriModuleItem *VeriCoverageSpec::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriCoverageSpec(*this, id_map_table) ;
}
VeriCoverageSpec::VeriCoverageSpec(const VeriCoverageSpec &coverage_spec, VeriMapForCopy &id_map_table) :
    VeriModuleItem(coverage_spec, id_map_table),
    _type(0),
    _id (0),
    _point_list(0),
    _iff_expr(0),
    _bins(0),
    _scope(0)
{
    // VeriCoverageSpec can have a scope, and its owner is the id. So copy the identifier
    // before copying other items.
    if (coverage_spec._id) {
        const MapItem *ele = id_map_table.GetItem(coverage_spec._id) ;
        if (ele) {
            _id = (VeriIdDef *)ele->Value() ;
        } else {
            _id = coverage_spec._id->CopyId(id_map_table, 1) ;
        }
    }
    // Set the module item back-pointer of the id to this.
    if (_id) _id->SetModuleItem(this) ;
    // Copy the type
    _type = coverage_spec._type ;
    // Copy the scope
    _scope = (coverage_spec._scope) ? coverage_spec._scope->Copy(id_map_table) : 0 ;
    // Copy the list of coverpoints
    _point_list = CopyExpressionArray(coverage_spec._point_list, id_map_table) ;
    // Copy the list of bins
    _bins = CopyModuleItemArray(coverage_spec._bins, id_map_table) ;
    if (_scope) _scope->UpdateScope(coverage_spec._scope, id_map_table) ;
}
VeriModuleItem *VeriBinDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriBinDecl(*this, id_map_table) ;
}
VeriBinDecl::VeriBinDecl(const VeriBinDecl &bin_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(bin_decl, id_map_table),
    _wildcard(0),
    _bins_keyword(0),
    _id(0)
{
    // Copy the wildcard token
    _wildcard = bin_decl._wildcard ;
    // Copy the bins keyword token
    _bins_keyword = bin_decl._bins_keyword ;
    // Copy the id
    if (bin_decl._id) {
        const MapItem *ele = id_map_table.GetItem(bin_decl._id) ;
        if (ele) {
            _id = (VeriIdDef *)ele->Value() ;
        } else {
            _id = bin_decl._id ;
        }
    }
}
VeriModuleItem *VeriImportDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriImportDecl(*this, id_map_table) ;
}
VeriImportDecl::VeriImportDecl(const VeriImportDecl &import_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(import_decl, id_map_table),
    _items(0)
{
    _items = CopyExpressionArray(import_decl._items, id_map_table) ;
}
VeriModuleItem *VeriLetDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriLetDecl(*this, id_map_table) ;
}
VeriLetDecl::VeriLetDecl(const VeriLetDecl &decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(decl, id_map_table),
    _id(0),
    _ansi_io_list(0),
    _expr(0),
    _scope(0),
    _ports(0)
{
    VeriIdDef *id = decl._id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Let identifier is the owner of the let scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    if (ele) {
        _id = (VeriIdDef *)ele->Value() ;
    } else {
        _id = id ? id->CopyId(id_map_table, 1): 0 ;
    }

    // Set the id's module item backpointer to this
    if (_id) _id->SetModuleItem(this) ;

    // Owner is already copied, now we can copy the scope, before going further.
    _scope = decl._scope ? decl._scope->Copy(id_map_table) : 0 ;

    // Copy (ansi) IO list :
    _ansi_io_list = CopyExpressionArray(decl._ansi_io_list, id_map_table) ;

    // Copying expression
    _expr = decl._expr ? decl._expr->CopyExpression(id_map_table) : 0 ;

    if (_scope) _scope->UpdateScope(decl._scope, id_map_table) ;
    // Copying Array of VeriIdDef
    _ports = CopyIdArray(decl._ports, id_map_table) ;
}
VeriModuleItem *VeriDefaultDisableIff::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriDefaultDisableIff(*this, id_map_table) ;
}
VeriDefaultDisableIff::VeriDefaultDisableIff(const VeriDefaultDisableIff &disable_iff, VeriMapForCopy &id_map_table) :
    VeriModuleItem(disable_iff, id_map_table) ,
    _expr_or_dist(0)
{
    _expr_or_dist = (disable_iff._expr_or_dist) ? disable_iff._expr_or_dist->CopyExpression(id_map_table) : 0 ;
}
VeriModuleItem *VeriExportDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriExportDecl(*this, id_map_table) ;
}
VeriExportDecl::VeriExportDecl(const VeriExportDecl &export_decl, VeriMapForCopy &id_map_table) :
    VeriModuleItem(export_decl, id_map_table),
    _items(0)
{
    _items = CopyExpressionArray(export_decl._items, id_map_table) ;
}
VeriExpression *VeriCaseOperator::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriCaseOperator(*this, id_map_table) ;
}
VeriCaseOperator::VeriCaseOperator(const VeriCaseOperator &case_oper, VeriMapForCopy &id_map_table) :
    VeriExpression(case_oper, id_map_table),
    _cond(0),
    _case_items(0)
{
    _cond = (case_oper._cond) ? case_oper._cond->CopyExpression(id_map_table) : 0 ;

    if (case_oper._case_items) {
        _case_items = new Array(case_oper._case_items->Size()) ;
        unsigned i ;
        VeriCaseOperatorItem *node ;
        FOREACH_ARRAY_ITEM(case_oper._case_items, i, node) {
            _case_items->InsertLast((node)?node->Copy(id_map_table):0) ;
        }
    }
}
VeriCaseOperatorItem* VeriCaseOperatorItem::Copy(VeriMapForCopy &id_map_table) const
{
    return new VeriCaseOperatorItem(*this, id_map_table) ;
}
VeriCaseOperatorItem::VeriCaseOperatorItem(const VeriCaseOperatorItem &case_item, VeriMapForCopy &id_map_table) :
    VeriTreeNode(case_item, id_map_table),
    _conditions(0),
    _property_expr(0)
{
    // Copying Array of Expressions
    _conditions = CopyExpressionArray(case_item._conditions, id_map_table) ;

    // Copying the statement
    _property_expr = (case_item._property_expr) ? case_item._property_expr->CopyExpression(id_map_table) : 0 ;
}
VeriModuleItem* VeriDPIFunctionDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriDPIFunctionDecl(*this, id_map_table) ;
}
VeriDPIFunctionDecl::VeriDPIFunctionDecl(const VeriDPIFunctionDecl &decl, VeriMapForCopy &id_map_table) :
    VeriFunctionDecl(decl, id_map_table),
    _type(decl._type),
    _spec_string(Strings::save(decl._spec_string)),
    _property(decl._property),
    _c_identifier(Strings::save(decl._c_identifier))
{
}
VeriModuleItem* VeriDPITaskDecl::CopyModuleItem(VeriMapForCopy &id_map_table) const
{
    return new VeriDPITaskDecl(*this, id_map_table) ;
}
VeriDPITaskDecl::VeriDPITaskDecl(const VeriDPITaskDecl &decl, VeriMapForCopy &id_map_table) :
    VeriTaskDecl(decl, id_map_table),
    _type(decl._type),
    _spec_string(Strings::save(decl._spec_string)),
    _property(decl._property),
    _c_identifier(Strings::save(decl._c_identifier))
{
}

VeriDataType *VeriDataType::InlineDataType(VeriMapForCopy &id_map_table) const
{
    return CopyDataType(id_map_table) ;
}

VeriDataType *VeriTypeRef::InlineDataType(VeriMapForCopy &id_map_table) const
{
    VeriDataType *type_dt = _type_name ? _type_name->GetDataType() : 0 ;
    if (type_dt) {
        VeriDataType *type_dt_copy = type_dt->InlineDataType(id_map_table) ;
        VERIFIC_ASSERT(type_dt_copy) ;

        VeriRange *this_dimensions = _dimensions ? _dimensions->CopyRange(id_map_table) : 0 ;
        if (this_dimensions) {
            VeriRange *dimension_copy = type_dt_copy->TakeDimensions() ;
            this_dimensions->SetNext(dimension_copy) ;
            type_dt_copy->SetDimensions(this_dimensions) ;
        }
        return type_dt_copy ;
    }

    return CopyDataType(id_map_table) ;
}

VeriDataType *VeriStructUnion::InlineDataType(VeriMapForCopy &id_map_table) const
{
    VeriDataType *this_copy = CopyDataType(id_map_table) ;

    Array *new_decls = new Array() ;
    unsigned idx = 0 ;
    VeriDataDecl *data_decl ;
    FOREACH_ARRAY_ITEM(_decls, idx, data_decl) {
        if (!data_decl) continue ;

        Array *uniq_decls = data_decl->InlineDataType(id_map_table) ;
        VERIFIC_ASSERT(uniq_decls) ;

        new_decls->Append(uniq_decls) ;
        delete uniq_decls ;
    }

    Array *old_decls = this_copy->TakeDecls() ;
    this_copy->SetDecls(new_decls) ;

    FOREACH_ARRAY_ITEM(old_decls, idx, data_decl) delete data_decl ;
    delete old_decls ;

    return this_copy ;
}

Array *VeriDataDecl::InlineDataType(VeriMapForCopy &id_map_table) const
{
    Array *return_array = new Array() ;
    if (!_data_type) {
        VeriModuleItem *new_decl = CopyModuleItem(id_map_table) ;
        return_array->Insert(new_decl) ;
        return return_array ;
    }

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

        VeriMapForCopy local_id_map_table ;
        VeriDataType *new_data_type = _data_type->InlineDataType(local_id_map_table) ;

        const MapItem *ele = id_map_table.GetItem(id) ;
        VERIFIC_ASSERT(ele) ;

        VeriIdDef *new_id = (VeriIdDef *)ele->Value() ;
        VERIFIC_ASSERT (new_id) ;

        new_id->ClearTreeNodeBckPtr() ;
        new_id->ClearDataType() ;

        VeriDataDecl *new_decl = new VeriDataDecl(_decl_type, new_data_type, new_id) ;
        return_array->Insert(new_decl) ;
    }

    return return_array ;
}

Array *VeriNetDecl::InlineDataType(VeriMapForCopy &id_map_table) const
{
    Array *return_array = new Array() ;
    if (!_data_type) {
        VeriModuleItem *new_decl = CopyModuleItem(id_map_table) ;
        return_array->Insert(new_decl) ;
        return return_array ;
    }

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

        VeriMapForCopy local_id_map_table ;
        VeriDataType *new_data_type = _data_type->InlineDataType(local_id_map_table) ;

        const MapItem *ele = id_map_table.GetItem(id) ;
        VERIFIC_ASSERT(ele) ;

        VeriIdDef *new_id = (VeriIdDef *)ele->Value() ;
        VERIFIC_ASSERT (new_id) ;

        Array *new_ids = new Array(1) ;
        new_ids->Insert(new_id) ;

        new_id->ClearTreeNodeBckPtr() ;
        new_id->ClearDataType() ;

        VeriStrength *new_strength = _strength ? _strength->Copy(id_map_table) : 0 ;
        Array *new_delay = CopyExpressionArray(_delay, id_map_table) ;

        VeriNetDecl *new_decl = new VeriNetDecl(new_data_type, new_strength, new_delay, new_ids) ;
        return_array->Insert(new_decl) ;
    }

    return return_array ;
}

VeriDataType *VeriNetDataType::CopyDataType(VeriMapForCopy &id_map_table) const
{
    return new VeriNetDataType(*this, id_map_table) ;
}
VeriNetDataType::VeriNetDataType(const VeriNetDataType &data_type, VeriMapForCopy &id_map_table) :
    VeriDataType(data_type, id_map_table),
    _data_type(0)
{
    _data_type = (data_type._data_type) ? data_type._data_type->CopyDataType(id_map_table) : 0 ;
}
VeriDataType *VeriTypeRef::CopyDataType(VeriMapForCopy &id_map_table) const
{
    return new VeriTypeRef(*this, id_map_table) ;
}

VeriTypeRef::VeriTypeRef(const VeriTypeRef &data_type, VeriMapForCopy &id_map_table) :
    VeriDataType(data_type, id_map_table),
    _type_name(0),
    _virtual_token(data_type._virtual_token),
    _id(0)
{
    // Copying the name
    _type_name = data_type._type_name ? data_type._type_name->CopyName(id_map_table) : 0 ;

    // Copying VeriIdDef, the resolved id back-pointer :
    if (data_type._id) {
        const MapItem *ele = id_map_table.GetItem(data_type._id) ;
        _id = ele ? (VeriIdDef *)ele->Value(): data_type._id ;
    }
}

VeriDataType *VeriStructUnion::CopyDataType(VeriMapForCopy &id_map_table) const
{
    return new VeriStructUnion(*this, id_map_table) ;
}

VeriStructUnion::VeriStructUnion(const VeriStructUnion &struct_union, VeriMapForCopy &id_map_table) :
    VeriDataType(struct_union, id_map_table),
    _decls(0),
    _scope(0),
    _ids(0),
    _is_packed(struct_union._is_packed),
    _is_tagged(struct_union._is_tagged),
    _contain_typeparam_type_ele(0),
    _ele_types(0),
    _ele_ids(0)
{
    // Owner is already copied, now we can copy the scope, before going further.
    _scope = struct_union._scope ? struct_union._scope->Copy(id_map_table) : 0 ;

    // Copying module item list
    _decls = CopyModuleItemArray(struct_union._decls, id_map_table) ;

    // Copying Array of VeriIdDef
    _ids = CopyIdArray(struct_union._ids, id_map_table) ;
    if (_scope) _scope->UpdateScope(struct_union._scope, id_map_table) ;
}

VeriDataType *VeriEnum::CopyDataType(VeriMapForCopy &id_map_table) const
{
    return new VeriEnum(*this, id_map_table) ;
}

VeriEnum::VeriEnum(const VeriEnum &veri_enum, VeriMapForCopy &id_map_table) :
    VeriDataType(veri_enum, id_map_table),
    _base_type(0),
    _ids(0),
    _enum_ids(0)
{
    // Copy base type
    _base_type = (veri_enum._base_type) ? veri_enum._base_type->CopyDataType(id_map_table) : 0 ;
    // Copying Array of VeriIdDef
    _ids = CopyIdArray(veri_enum._ids, id_map_table) ;
    // Enum declaration can appear as an expression. Take ownership if it is not already
    // owned by other. If we copy only the expression, then we will use the previously
    // declared identifier.

    _enum_ids = new Map(STRING_HASH, _ids ? _ids->Size(): 1) ;
    // 'copy' the DataType back pointers on id to 'this' (but only if not yet set)
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        // Set data type back-pointer, but only if not yet set.
        id->SetDataTypeForCopy(this) ;
        (void) _enum_ids->Insert(id->Name(), id) ;
    }
}

VeriExpression *VeriDotStar::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriDotStar(*this, id_map_table) ;
}

VeriDotStar::VeriDotStar(const VeriDotStar &dot_star, VeriMapForCopy &id_map_table) :
    VeriExpression(dot_star, id_map_table),
    _dot_star_scope(dot_star._dot_star_scope)
{
    // No member to copy
    // We need to update the _dot_star_scope back-pointer (pointing to the scope of
    // the missing actuals) by finding its entry in 'id_map_table'. We should set
    // copied scope as new _dot_star_scope.
    const MapItem *ele = id_map_table.GetItem(_dot_star_scope) ;
    if (ele) _dot_star_scope = (VeriScope*)ele->Value() ; // Set copied scope if exists
}

VeriExpression *VeriIfOperator::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriIfOperator(*this, id_map_table) ;
}

VeriIfOperator::VeriIfOperator(const VeriIfOperator &if_op, VeriMapForCopy &id_map_table) :
    VeriQuestionColon(if_op, id_map_table)
{
    // No member to copy
}

VeriExpression *VeriSequenceConcat::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriSequenceConcat(*this, id_map_table) ;
}

VeriSequenceConcat::VeriSequenceConcat(const VeriSequenceConcat &seq_concat, VeriMapForCopy &id_map_table) :
    VeriBinaryOperator(seq_concat, id_map_table),
    _cycle_delay_range(0)
{
    // Copying the expr or range defining number of cycles between left-end and right-start
    _cycle_delay_range = seq_concat._cycle_delay_range ? seq_concat._cycle_delay_range->CopyExpression(id_map_table) : 0 ;
}

VeriExpression *VeriClockedSequence::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriClockedSequence(*this, id_map_table) ;
}

VeriClockedSequence::VeriClockedSequence(const VeriClockedSequence &clock_seq, VeriMapForCopy &id_map_table) :
    VeriUnaryOperator(clock_seq, id_map_table),
    _event_expr(0)
{
    // Copying the clocking event for the sequence
    _event_expr = clock_seq._event_expr ? clock_seq._event_expr->CopyExpression(id_map_table) : 0 ;
}

VeriExpression *VeriAssignInSequence::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriAssignInSequence(*this, id_map_table) ;
}

VeriAssignInSequence::VeriAssignInSequence(const VeriAssignInSequence &assign_seq, VeriMapForCopy &id_map_table) :
    VeriUnaryOperator(assign_seq, id_map_table),
    _stmts(0)
{
    // Copying the assignment statements
    _stmts = CopyStatementArray(assign_seq._stmts, id_map_table) ;
}

VeriExpression *VeriDistOperator::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriDistOperator(*this, id_map_table) ;
}

VeriDistOperator::VeriDistOperator(const VeriDistOperator &dist_op, VeriMapForCopy &id_map_table) :
    VeriUnaryOperator(dist_op, id_map_table),
    _dist_list(0)
{
    // Copying the dist items
    _dist_list = CopyExpressionArray(dist_op._dist_list, id_map_table) ;
}

VeriExpression *VeriSolveBefore::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriSolveBefore(*this, id_map_table) ;
}

VeriSolveBefore::VeriSolveBefore(const VeriSolveBefore &solve_before, VeriMapForCopy &id_map_table) :
    VeriExpression(solve_before, id_map_table),
    _solve_list(0),
    _before_list(0)
{
    // Copying the variables which need to be solved
    _solve_list = CopyExpressionArray(solve_before._solve_list, id_map_table) ;

    // Copying the variables which need to be solved before the solved list
    _before_list = CopyExpressionArray(solve_before._before_list, id_map_table) ;
}

VeriExpression *VeriCast::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriCast(*this, id_map_table) ;
}

VeriCast::VeriCast(const VeriCast &cast, VeriMapForCopy &id_map_table) :
    VeriExpression(cast, id_map_table),
    _target_type(0),
    _expr(0)
{
    // Copying the target type, The type to which we will cast
    _target_type = cast._target_type ? cast._target_type->CopyExpression(id_map_table) : 0 ;

    // Copying the expression which is casted
    _expr = cast._expr ? cast._expr->CopyExpression(id_map_table) : 0 ;
}

VeriExpression *VeriNew::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriNew(*this, id_map_table) ;
}

VeriNew::VeriNew(const VeriNew &veri_new, VeriMapForCopy &id_map_table) :
    VeriExpression(veri_new, id_map_table),
    _size_expr(0),
    _args(0),
    _copy_constructor(0)
{
    // Copying the  number of elements to create
    _size_expr = veri_new._size_expr ? veri_new._size_expr->CopyExpression(id_map_table) : 0 ;

    // Copying the arguments to the new operator
    _args = CopyExpressionArray(veri_new._args, id_map_table) ;

    _copy_constructor = veri_new._copy_constructor ;
}

VeriExpression *VeriConcatItem::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriConcatItem(*this, id_map_table) ;
}

VeriConcatItem::VeriConcatItem(const VeriConcatItem &concat_item, VeriMapForCopy &id_map_table) :
    VeriExpression(concat_item, id_map_table),
    _member_label(0),
    _expr(0)
{
    // Copying the expression which identifies the member label
    _member_label = concat_item._member_label ? concat_item._member_label->CopyExpression(id_map_table) : 0 ;

    // Copying the expression which is the concat element
    _expr = concat_item._expr ? concat_item._expr->CopyExpression(id_map_table) : 0 ;
}

VeriExpression *VeriNull::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriNull(*this, id_map_table) ;
}

VeriNull::VeriNull(const VeriNull &null_literal, VeriMapForCopy &id_map_table) :
    VeriExpression(null_literal, id_map_table)
{
}

VeriExpression *VeriDollar::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriDollar(*this, id_map_table) ;
}

VeriDollar::VeriDollar(const VeriDollar &dollar, VeriMapForCopy &id_map_table) :
    VeriExpression(dollar, id_map_table)
{
}
VeriExpression* VeriAssignmentPattern::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriAssignmentPattern(*this, id_map_table) ;
}

VeriAssignmentPattern::VeriAssignmentPattern(const VeriAssignmentPattern &assignment_pattern, VeriMapForCopy &id_map_table) :
    VeriConcat(assignment_pattern, id_map_table),
    _type(0)
{
    // Copying the target type expression
    _type = assignment_pattern._type ? assignment_pattern._type->CopyExpression(id_map_table) : 0 ;

    // RD: _exprs already copied in the VeriConcat copy constructor.
    // Copying the array of expressions
    // _exprs = CopyExpressionArray(assignment_pattern._exprs, id_map_table) ;
}

VeriExpression* VeriMultiAssignmentPattern::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriMultiAssignmentPattern(*this, id_map_table) ;
}

VeriMultiAssignmentPattern::VeriMultiAssignmentPattern(const VeriMultiAssignmentPattern &multi_assignment_pattern, VeriMapForCopy &id_map_table) :
    VeriMultiConcat(multi_assignment_pattern, id_map_table),
    _type(0)
{
    // Copying the target type expression
    _type = multi_assignment_pattern._type ? multi_assignment_pattern._type->CopyExpression(id_map_table) : 0 ;

    // RD: _repeat and _exprs already copied in the VeriConcat copy constructor.
    // Copying the repeat expression
    // _repeat = multi_assignment_pattern._repeat ? multi_assignment_pattern._repeat->CopyExpression(id_map_table) : 0 ;
    // Copying the array of expressions
    // _exprs = CopyExpressionArray(multi_assignment_pattern._exprs, id_map_table) ;
}
VeriExpression *VeriForeachOperator::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriForeachOperator(*this, id_map_table) ;
}
VeriForeachOperator::VeriForeachOperator(const VeriForeachOperator &foreach_op, VeriMapForCopy &id_map_table) :
    VeriExpression(foreach_op, id_map_table),
    _array_name(0),
    _loop_indexes(0),
    _constraint_set(0),
    _scope(0),
    _implicit_decl_list(0)
{
    // Copy the scope
    _scope = foreach_op._scope ? foreach_op._scope->Copy(id_map_table) : 0 ;

    // Copy array name
    _array_name = foreach_op._array_name ? foreach_op._array_name->CopyName(id_map_table) : 0 ;

    // Copy identifier array
    _loop_indexes = CopyIdArray(foreach_op._loop_indexes, id_map_table) ;

    // Copy constraint set
    _constraint_set = foreach_op._constraint_set ? foreach_op._constraint_set->CopyExpression(id_map_table) : 0 ;
    _implicit_decl_list =  CopyModuleItemArray(foreach_op._implicit_decl_list, id_map_table) ;
    if (_scope) _scope->UpdateScope(foreach_op._scope, id_map_table) ;
}
VeriExpression *VeriStreamingConcat::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriStreamingConcat(*this, id_map_table) ;
}
VeriStreamingConcat::VeriStreamingConcat(const VeriStreamingConcat &stream_concat, VeriMapForCopy &id_map_table) :
    VeriConcat(stream_concat, id_map_table),
    _stream_operator(stream_concat._stream_operator),
    _slice_size(0)
{
    // Copy slice size
    _slice_size = stream_concat._slice_size ? stream_concat._slice_size->CopyExpression(id_map_table) : 0 ;
}
VeriExpression *VeriCondPredicate::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriCondPredicate(*this, id_map_table) ;
}
VeriCondPredicate::VeriCondPredicate(const VeriCondPredicate &cond_predicate, VeriMapForCopy &id_map_table) :
    VeriExpression(cond_predicate, id_map_table),
    _left(0),
    _right(0)
{
    // Copy left and right operand
    _left = (cond_predicate._left) ? cond_predicate._left->CopyExpression(id_map_table) : 0 ;
    _right = (cond_predicate._right) ? cond_predicate._right->CopyExpression(id_map_table) : 0 ;
}
VeriExpression *VeriDotName::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriDotName(*this, id_map_table) ;
}
VeriDotName::VeriDotName(const VeriDotName &dot_name, VeriMapForCopy &id_map_table) :
    VeriExpression(dot_name, id_map_table),
    _name(0)
{
    _name = dot_name._name ? dot_name._name->CopyName(id_map_table) : 0 ;
    // Get copied data type of variable from map and set that to copied identifier
    VeriIdDef *orig_id = dot_name._name ? dot_name._name->GetId(): 0 ;
    VeriIdDef *new_id = _name ? _name->GetId() : 0 ;
    VeriDataType *orig_data_type = orig_id ? orig_id->GetDataType(): 0 ;
    if (orig_data_type && new_id) {
        VeriDataType *new_type = (VeriDataType*)id_map_table.GetValue(orig_data_type) ;
        if (!new_type) new_type = orig_data_type ; // Data type is not copied
        new_id->SetDataType(new_type) ;
    }
}
VeriExpression *VeriTaggedUnion::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriTaggedUnion(*this, id_map_table) ;
}
VeriTaggedUnion::VeriTaggedUnion(const VeriTaggedUnion &tagged_union, VeriMapForCopy &id_map_table) :
    VeriExpression(tagged_union, id_map_table),
    _member(0),
    _expr(0)
{
    _member = tagged_union._member ? tagged_union._member->CopyName(id_map_table) : 0 ;
    _expr = tagged_union._expr ? tagged_union._expr->CopyExpression(id_map_table) : 0 ;
}
VeriExpression *VeriWithExpr::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriWithExpr(*this, id_map_table) ;
}
VeriWith::VeriWith(const VeriWith &with_base, VeriMapForCopy &id_map_table) :
    VeriExpression(with_base, id_map_table),
    _left(0),
    _scope(0)
{
    _scope = (with_base._scope) ? with_base._scope->Copy(id_map_table) : 0 ;
    _left = (with_base._left) ? with_base._left->CopyExpression(id_map_table) : 0 ;
    if (_scope) _scope->UpdateScope(with_base._scope, id_map_table) ;
}
VeriWithExpr::VeriWithExpr(const VeriWithExpr &with_expr, VeriMapForCopy &id_map_table) :
    VeriWith(with_expr, id_map_table),
    _right(0)
{
    _right = (with_expr._right) ? with_expr._right->CopyExpression(id_map_table) : 0 ;
    // Some identifiers are declared in the scope local to this array method call
    // Set their data type from the array whose method is called here
    Map *this_scope = _scope ? _scope->GetThisScope(): 0 ;

    // Extract the array id whose data type is to be set to 'id'
    VeriExpression *array_ref = _left ;

    while (array_ref) {
        if (array_ref->GetFunctionType()) { // 'array_ref' has function name as suffix
            // Its prefix is array reference
            array_ref = array_ref->GetPrefix() ;
            break ;
        }
        array_ref = array_ref->GetPrefix() ;
    }
    VeriIdDef *array_id = array_ref ? array_ref->GetId() : 0 ;

    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(this_scope, mi, 0, &id) {
        if (id && !id->GetDataType() && array_id && array_id->GetDataType()) {
            id->SetDataType(array_id->GetDataType()) ;
        }
    }
}
VeriExpression *VeriInlineConstraint::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriInlineConstraint(*this, id_map_table) ;
}
VeriInlineConstraint::VeriInlineConstraint(const VeriInlineConstraint &constraint, VeriMapForCopy &id_map_table) :
    VeriWith(constraint, id_map_table),
    _id_refs(0),
    _constraint_block(0)
{
    _id_refs = CopyExpressionArray(constraint._id_refs, id_map_table) ; // VIPER #6756
    _constraint_block = CopyExpressionArray(constraint._constraint_block, id_map_table) ;
}
VeriBinValue::VeriBinValue(const VeriBinValue &bin_value, VeriMapForCopy &id_map_table) :
    VeriExpression(bin_value, id_map_table),
    _iff_expr(0)
{
    // Copy the 'iff' expression
    _iff_expr = (bin_value._iff_expr) ? bin_value._iff_expr->CopyExpression(id_map_table) : 0 ;
}
VeriExpression *VeriOpenRangeBinValue::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriOpenRangeBinValue(*this, id_map_table) ;
}
VeriOpenRangeBinValue::VeriOpenRangeBinValue(const VeriOpenRangeBinValue &open_range_bin, VeriMapForCopy &id_map_table) :
   VeriBinValue(open_range_bin, id_map_table),
   _open_range_list(0)
{
    // Copy open range list
    _open_range_list = CopyExpressionArray(open_range_bin._open_range_list, id_map_table) ;
}
VeriExpression *VeriTransBinValue::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriTransBinValue(*this, id_map_table) ;
}
VeriTransBinValue::VeriTransBinValue(const VeriTransBinValue &trans_bin, VeriMapForCopy &id_map_table) :
    VeriBinValue(trans_bin, id_map_table),
    _trans_list(0)
{
    // Copy trans sets
    _trans_list = CopyExpressionArray(trans_bin._trans_list, id_map_table) ;
}
VeriExpression * VeriDefaultBinValue::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriDefaultBinValue(*this, id_map_table) ;
}
VeriDefaultBinValue::VeriDefaultBinValue(const VeriDefaultBinValue &default_bin, VeriMapForCopy &id_map_table) :
    VeriBinValue(default_bin, id_map_table),
    _sequence(0)
{
    _sequence = default_bin._sequence ;
}
VeriExpression * VeriSelectBinValue::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriSelectBinValue(*this, id_map_table) ;
}
VeriSelectBinValue::VeriSelectBinValue(const VeriSelectBinValue &select_bin, VeriMapForCopy &id_map_table) :
    VeriBinValue(select_bin, id_map_table),
    _select_expr(0)
{
    // Copy select expression
    _select_expr = (select_bin._select_expr) ? select_bin._select_expr->CopyExpression(id_map_table) : 0 ;
}
VeriExpression *VeriTransSet::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriTransSet(*this, id_map_table) ;
}
VeriTransSet::VeriTransSet(const VeriTransSet &trans_set, VeriMapForCopy &id_map_table) :
    VeriExpression(trans_set, id_map_table),
    _trans_range_list(0)
{
    // Copy the trans range list
    _trans_range_list = CopyExpressionArray(trans_set._trans_range_list, id_map_table) ;
}
VeriExpression *VeriSelectCondition::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriSelectCondition(*this, id_map_table) ;
}
VeriSelectCondition::VeriSelectCondition(const VeriSelectCondition &select_cond, VeriMapForCopy &id_map_table) :
    VeriExpression(select_cond, id_map_table),
    _bins_expr(0),
    _range_list(0)
{
    // Copy the bins expression
    _bins_expr = (select_cond._bins_expr) ? select_cond._bins_expr->CopyExpression(id_map_table) : 0 ;
    // Copy the range list
    _range_list = CopyExpressionArray(select_cond._range_list, id_map_table) ;
}
VeriExpression *VeriTransRangeList::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriTransRangeList(*this, id_map_table) ;
}
VeriTransRangeList::VeriTransRangeList (const VeriTransRangeList &trans_range_list, VeriMapForCopy &id_map_table)  :
    VeriExpression(trans_range_list, id_map_table),
    _trans_item(0),
    _repetition(0)
{
    // Copy trans items
    _trans_item = CopyExpressionArray(trans_range_list._trans_item, id_map_table) ;

    _repetition = (trans_range_list._repetition) ? trans_range_list._repetition->CopyExpression(id_map_table) : 0 ;
}

// VIPER #5710(vcs_compatibility: always_checker(for checker instantiation inside procedural code)):
// CopyStatement routine for VeriSequentialInstantiation:
VeriStatement* VeriSequentialInstantiation::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriSequentialInstantiation(*this, id_map_table) ;
}

// Copyconstructor  for VeriSequentialInstantiation:
VeriSequentialInstantiation::VeriSequentialInstantiation(const VeriSequentialInstantiation &mod_inst, VeriMapForCopy &id_map_table) :
    VeriStatement(mod_inst, id_map_table),
    _module_name(0),
    _insts(mod_inst._insts.Size()),
    _library(0)
{
    const char *instantiated_module_name = mod_inst._module_name ? mod_inst._module_name->GetName(): 0 ;
    _module_name = mod_inst._module_name ? mod_inst._module_name->CopyName(id_map_table) : 0 ;
    if (_module_name && instantiated_module_name) _module_name->SetName(Strings::save(instantiated_module_name)) ;
    // For recursive sequential instantiations the instantiated module id becomes the copied
    // module id. For this we cannot handle recursive sequential instantiations in Static
    // Elaboration. For this, set the module id of _module_name to zero.
    // FIXME : This is a hack. We need to find an alternate way to handle this problem
    // in a more elegant manner. A copy of a recursive module should create a module which
    // is also recursive.
    VeriIdDef *module_id = (_module_name) ? _module_name->GetId() : 0 ;
    if (_module_name && (module_id && Strings::compare(instantiated_module_name, module_id->GetName()) == 0)) _module_name->SetId(0) ;

    VeriInstId *inst = 0 ;
    VeriInstId *new_mod_inst = 0 ;

    _library = mod_inst._library ;
    // Copy the instances.
    if (mod_inst._insts.Size()) {
        unsigned i ;
        FOREACH_ARRAY_ITEM(&mod_inst._insts, i, inst) {
            const MapItem *ele = inst ? id_map_table.GetItem(inst) : 0 ;
            new_mod_inst = (ele) ? (VeriInstId *)ele->Value() : inst ;
            _insts.InsertLast(new_mod_inst) ;
        }
    }
}

VeriStatement *VeriAssertion::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriAssertion(*this, id_map_table) ;
}

VeriAssertion::VeriAssertion(const VeriAssertion &assert, VeriMapForCopy &id_map_table) :
    VeriStatement(assert, id_map_table),
    _assert_cover_property(0),
    _is_immediate(assert._is_immediate),
    _final_property(0),
    _is_deferred(assert._is_deferred),
    _property_spec(0),
    _then_stmt(0),
    _else_stmt(0),
    _sva_clock_expression(0)
{
    // Copying the flag which decides whether it is a immediate or concurrent assertion
    _assert_cover_property = assert._assert_cover_property ;
    _final_property = assert._final_property ;

    // Copying the property (expression)
    _property_spec = assert._property_spec ? assert._property_spec->CopyExpression(id_map_table) : 0 ;

    // Copying then stmt
    _then_stmt = assert._then_stmt ? assert._then_stmt->CopyStatement(id_map_table) : 0 ;

    // Copying else stmt
    _else_stmt = assert._else_stmt ? assert._else_stmt->CopyStatement(id_map_table) : 0 ;

    // Copying the clock expression
    // Memory leak fix: clock expression is not owned, no need to copy, just set the pointer:
    _sva_clock_expression = assert._sva_clock_expression ;
}

VeriStatement *VeriJumpStatement::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriJumpStatement(*this, id_map_table) ;
}

VeriJumpStatement::VeriJumpStatement(const VeriJumpStatement &jump_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(jump_stmt, id_map_table),
    _jump_token(0),
    _expr(0),
    _jump_id(0)
{
    // Copying the Token
    _jump_token = jump_stmt._jump_token ;

    // Copying return stmt
    _expr = jump_stmt._expr ? jump_stmt._expr->CopyExpression(id_map_table) : 0 ;

    VeriIdDef *id = jump_stmt._jump_id ;
    const MapItem *ele = id_map_table.GetItem(id) ;

    // Module identifier is the owner of the module scope, so at first we will try to find
    // an entry against it in the table, if not found, we will copy the identifier.
    _jump_id = (ele) ? (VeriIdDef *)ele->Value() : id ;
}

VeriStatement *VeriDoWhile::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriDoWhile(*this, id_map_table) ;
}

VeriDoWhile::VeriDoWhile(const VeriDoWhile &do_while, VeriMapForCopy &id_map_table) :
    VeriLoop(do_while, id_map_table),
    _cond(0)
{
    // Copy the Condition
    _cond = do_while._cond ? do_while._cond->CopyExpression(id_map_table) : 0 ;
}
VeriStatement *VeriForeach::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriForeach(*this, id_map_table) ;
}
VeriForeach::VeriForeach(const VeriForeach &foreach, VeriMapForCopy &id_map_table) :
    VeriLoop(foreach, id_map_table),
    _array(0),
    _loop_indexes(0),
    _implicit_decl_list(0) // VIPER #4649/4703
{
    _array = foreach._array ? foreach._array->CopyName(id_map_table) : 0 ; // Copy array name
    _loop_indexes = CopyIdArray(foreach._loop_indexes, id_map_table) ; // Copy loop indexes
    // Copy implicit declarations :
    _implicit_decl_list = CopyModuleItemArray(foreach._implicit_decl_list, id_map_table) ;
}
VeriStatement *VeriWaitOrder::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriWaitOrder(*this, id_map_table) ;
}
VeriWaitOrder::VeriWaitOrder(const VeriWaitOrder &wait_order, VeriMapForCopy &id_map_table) :
    VeriStatement(wait_order, id_map_table),
    _events(0),
    _then_stmt(0),
    _else_stmt(0)
{
    // Copy events
    _events = wait_order._events ? new Array(wait_order._events->Size()) : 0 ;
    unsigned i ;
    VeriName *name ;
    FOREACH_ARRAY_ITEM(wait_order._events, i, name) {
        if (_events) _events->InsertLast(name ? name->CopyName(id_map_table): 0) ;
    }

    _then_stmt = wait_order._then_stmt ? wait_order._then_stmt->CopyStatement(id_map_table) : 0 ; // Copy then statement
    _else_stmt = wait_order._else_stmt ?  wait_order._else_stmt->CopyStatement(id_map_table) : 0 ; // Copy else statement
}
VeriWithStmt::VeriWithStmt(const VeriWithStmt &with_stmt, VeriMapForCopy &id_map_table) :
    VeriStatement(with_stmt, id_map_table),
    _left(0),
    _scope(0)
{
   _scope = (with_stmt._scope) ? with_stmt._scope->Copy(id_map_table) : 0 ;
   _left = (with_stmt._left) ? with_stmt._left->CopyStatement(id_map_table) : 0 ;
    if (_scope) _scope->UpdateScope(with_stmt._scope, id_map_table) ;
}
VeriStatement *VeriArrayMethodCall::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriArrayMethodCall(*this, id_map_table) ;
}
VeriArrayMethodCall::VeriArrayMethodCall(const VeriArrayMethodCall &method_call, VeriMapForCopy &id_map_table) :
    VeriWithStmt(method_call, id_map_table),
    _right(0)
{
    _right = (method_call._right) ? method_call._right->CopyExpression(id_map_table) : 0 ;
    // Some identifiers are declared in the scope local to this array method call
    // Set their data type from the array whose method is called here
    Map *this_scope = _scope ? _scope->GetThisScope(): 0 ;

    // Extract the array identifier from _left
    VeriName *task_name = _left ? _left->GetTaskName() : 0 ;
    // 'task_name' is a selected name whose prefix is array id and suffix function name
    VeriName *prefix = task_name ? task_name->GetPrefix() : 0 ;
    VeriIdDef *array_id = prefix ? prefix->GetId() : 0 ;

    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(this_scope, mi, 0, &id) {
        if (id && !id->GetDataType() && array_id && array_id->GetDataType()) {
            id->SetDataType(array_id->GetDataType()) ;
        }
    }
}
VeriStatement *VeriInlineConstraintStmt::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriInlineConstraintStmt(*this, id_map_table) ;
}
VeriInlineConstraintStmt::VeriInlineConstraintStmt(const VeriInlineConstraintStmt &constraint, VeriMapForCopy &id_map_table) :
    VeriWithStmt(constraint, id_map_table),
    _constraint_block(0)
{
    _constraint_block = CopyExpressionArray(constraint._constraint_block, id_map_table) ;
}
VeriStatement *VeriRandsequence::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriRandsequence(*this, id_map_table) ;
}
VeriRandsequence::VeriRandsequence(const VeriRandsequence &randsequence, VeriMapForCopy &id_map_table) :
    VeriStatement(randsequence, id_map_table),
    _start_prod(0),
    _productions(0),
    _scope(0)
{
    // Copy scope
    _scope = randsequence._scope ? randsequence._scope->Copy(id_map_table) : 0  ;

    // Copy starting production which is of type VeriName
    _start_prod = randsequence._start_prod ? randsequence._start_prod->CopyName(id_map_table) : 0 ;

    // Copy list of productions
    _productions = randsequence._productions ? new Array(randsequence._productions->Size()) : 0 ;
    unsigned i ;
    VeriProduction *prod ;
    FOREACH_ARRAY_ITEM(randsequence._productions, i, prod) {
        if (_productions) _productions->InsertLast(prod ? prod->Copy(id_map_table): 0) ;
    }
    if (_scope) _scope->UpdateScope(randsequence._scope, id_map_table) ;
}
VeriStatement *VeriCodeBlock::CopyStatement(VeriMapForCopy &id_map_table) const
{
    return new VeriCodeBlock(*this, id_map_table) ;
}
VeriCodeBlock::VeriCodeBlock(const VeriCodeBlock &code_block, VeriMapForCopy &id_map_table) :
    VeriStatement(code_block, id_map_table),
    _decls(0),
    _stmts(0),
    _scope(0)
{
    // Copy scope
    _scope = code_block._scope ? code_block._scope->Copy(id_map_table) : 0  ;

    // Copy list of productions
    _decls = CopyModuleItemArray(code_block._decls, id_map_table) ;

    // Copy list of statements
    _stmts = CopyStatementArray(code_block._stmts, id_map_table) ;
    if (_scope) _scope->UpdateScope(code_block._scope, id_map_table) ;
}
VeriDataType *VeriTypeOperator::CopyDataType(VeriMapForCopy &id_map_table) const
{
    return new VeriTypeOperator(*this, id_map_table) ;
}
VeriTypeOperator::VeriTypeOperator(const VeriTypeOperator &type_op, VeriMapForCopy &id_map_table) :
    VeriDataType(type_op, id_map_table),
    _expr_or_data_type(0)
{
    // Copying the expression
    _expr_or_data_type = (type_op._expr_or_data_type) ? type_op._expr_or_data_type->CopyExpression(id_map_table) : 0 ;
}
VeriExpression *VeriPatternMatch::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriPatternMatch(*this, id_map_table) ;
}
VeriPatternMatch::VeriPatternMatch(const VeriPatternMatch &pattern_match, VeriMapForCopy &id_map_table) :
    VeriExpression(pattern_match, id_map_table),
    _left(0),
    _right(0),
    _scope(0)
{
    _left = (pattern_match._left) ? pattern_match._left->CopyExpression(id_map_table) : 0 ;
    _scope = (pattern_match._scope) ? pattern_match._scope->Copy(id_map_table) : 0 ;
    _right = (pattern_match._right) ? pattern_match._right->CopyExpression(id_map_table) : 0 ;
    if (_scope) _scope->UpdateScope(pattern_match._scope, id_map_table) ;
}
VeriExpression *VeriConstraintSet::CopyExpression(VeriMapForCopy &id_map_table) const
{
    return new VeriConstraintSet(*this, id_map_table) ;
}
VeriConstraintSet::VeriConstraintSet(const VeriConstraintSet &contraint_set, VeriMapForCopy &id_map_table) :
    VeriExpression(contraint_set, id_map_table),
    _exprs(0)
{
    // Copying expression array
    _exprs = CopyExpressionArray(contraint_set._exprs, id_map_table) ;
}

VeriLibraryDecl::VeriLibraryDecl(const VeriLibraryDecl &tree_node, VeriMapForCopy &id_map_table) :
    VeriTreeNode(tree_node, id_map_table),
    _name(0),
    _patterns(0)
{
    // Copy the char *name of the library:
    _name = Strings::save(tree_node._name) ;
    // Copy the Array of char * patters:
    if (tree_node._patterns) {
        _patterns = new Array(tree_node._patterns->Size()) ;
        unsigned i ;
        char *pattern ;
        FOREACH_ARRAY_ITEM(tree_node._patterns, i, pattern) {
            // Copy each patterns:
            _patterns->InsertLast(Strings::save(pattern)) ;
        }
    }
}
ReResolveIds::ReResolveIds(VeriMapForCopy *copy_obj)
    : VeriVisitor(),
    _copy_obj(copy_obj)
{}
ReResolveIds::~ReResolveIds() { _copy_obj = 0 ;}
void ReResolveIds::VERI_VISIT(VeriIdRef, id_ref)
{
    VeriIdDef *id = id_ref.GetId() ;
    const MapItem *ele = (id && _copy_obj) ? _copy_obj->GetItem(id) : 0 ;
    if (ele) {
        VeriIdDef *new_id = (VeriIdDef *)ele->Value() ;
        if (id != new_id) id_ref.SetId(new_id) ;
    }
}

// VIPER #7508 : Update _extended_scope and base_class_scope after copying module
void VeriScope::UpdateScopeBackPointers(const VeriScope *scope, VeriMapForCopy &id_map_table)
{
    if (!scope) return ;
    // Get the parent scope.
    if (_extended_scope) {
        // If extended scope is not null, and we could find an entry of extended scope the
        // 'id_map_table', we should set the copied extended as the extended of 'this' scope.
        const MapItem *ele = id_map_table.GetItem(scope->GetExtendedScope()) ;

        if (ele) _extended_scope = (VeriScope*)ele->Value() ;
    }
    // Get the base class scope.
    if (_base_class_scope) {
        // If base class scope is not null, and we could find an entry of base class scope the
        // 'id_map_table', we should set the copied scope as the base class of 'this' scope.
        const MapItem *ele = id_map_table.GetItem(scope->GetBaseClassScope()) ;
        if (ele) _base_class_scope = (VeriScope*)ele->Value() ;
    }

    Set *children_scopes = scope->GetChildrenScopes() ;
    SetIter si ;
    VeriScope *old_child, *new_child ;
    FOREACH_SET_ITEM(children_scopes, si, &old_child) {
        if (!old_child) continue ;
        new_child = (VeriScope*)id_map_table.GetValue(old_child) ;
        if (new_child) {
            new_child->UpdateScopeBackPointers(old_child, id_map_table) ;
        } else if (this == scope) {
            old_child->UpdateScopeBackPointers(old_child, id_map_table) ;
        }
    }
}
