/*
 *
 * [ File Version : 1.469 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * val Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/
#include <string.h> // strncmp
#include "Array.h"
#include "Map.h"
#include "Strings.h"

#include "veri_file.h"
#include "VeriId.h"
#include "VeriMisc.h"
#include "VeriScope.h"
#include "VeriExpression.h"
#include "VeriConstVal.h"
#include "VeriModule.h"
#include "VeriModuleItem.h"
#include "VeriLibrary.h"

#include "veri_tokens.h"

#include "VeriBaseValue_Stat.h" // for constraint of parameter

#include "Message.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/* All constructors, take subnodes as arguments. These subnodes are absorbed and
 * then owned by the constructed node. This makes every node a parse-tree node.
 * When a node is destroyed, it destroys its own subnodes.
 *
 * One exception is IdDefs (VeriIdDef is any named object in Verilog). IdDefs are stored
 * and owned by a Scope.
*/

/* ALERT : To reduce copies/freeing of already allocated space, we ABSORB
 * (take ownership of) the incoming name when constructing an identifier !
 * We don't make an internal copy.
*/
VeriIdDef::VeriIdDef(char *n) : VeriTreeNode(),
    _name(n)  /* Absorb the string ! */
#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    , _owning_scope(0) // set owning scope when identifier is added to a scope
#endif
#ifdef VERILOG_STORE_ID_PRAGMAS
    , _id_pragmas(0) // identifier pragmas (char*->char* table)
    //CARBON_BEGIN
    , _line_infos(0) // identifier lineinfos (char*->char* table)
    //CARBON_END
#endif
{
#ifdef VERILOG_SHARE_STRINGS
    _name = Strings::CreateConstantString(n) ; // used shared string
    Strings::free(n) ;
#endif
}

VeriIdDef::~VeriIdDef()
{
    // Free the name.
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ;
#endif
    _name = 0 ;

#ifdef VERILOG_STORE_ID_PRAGMAS
    // _id_pragmas is a char*->char* table
    if (_id_pragmas) {
        char *name ;
        char *value ;
        MapIter mi ;
        FOREACH_MAP_ITEM(_id_pragmas, mi, &name, &value) {
            Strings::free(name) ;
            Strings::free(value) ;
        }
        delete _id_pragmas ;
    }
    //CARBON_BEGIN
    // _line_infos is a char*->char* table (pragmaname->lineinfo)
    if (_line_infos) {
        char *name ;
        char *value ;
        MapIter mi ;
        FOREACH_MAP_ITEM(_line_infos, mi, &name, &value) {
            Strings::free(name) ;
            Strings::free(value) ;
        }
        delete _line_infos;
    }
    //CARBON_END
#endif
}

VeriVariable::VeriVariable(char *n, VeriRange *dimensions)
  : VeriIdDef(n)
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    // Avoid these fields if we don't do FSM extraction
    , _link_var(0)
    , _fsm_constants(0)
#endif
    , _initial_value(0)
    , _type(0)
    , _dir(0)
    , _implicit_net(0)
    , _is_used(0)
    , _is_assigned(0)
    , _is_concurrent_assigned(0)
    , _is_concurrent_used(0)
    , _is_assigned_before_used(0)
    , _is_used_before_assigned(0)
    , _can_be_onehot(0)
    , _can_be_dual_port_ram(0)
    , _can_be_multi_port_ram(0)
    , _is_member(0)
    , _is_modport_port(0)
    , _is_ansi_port(0)
    , _is_constant(0)
    , _is_automatic(0)
    , _is_iterator(0)
    , _is_localvar(0)
    , _is_private(0)
    , _is_protected(0)
    , _kind(0) // VIPER #5949
    , _is_implicit_non_ansi_port(0) // VIPER #6506
    , _is_ram_overridden(0)
    , _is_clocking_decl_id(0)
    , _is_se_created_vi(0) // VIPER #6992
    , _is_used_in_for_loop_index(0) // VIPER #5239
    , _is_static_class_variable(0) //Viper 6907
    , _is_import(0)
    , _is_export(0)
    , _is_onehot_overridden(0) // VIPER #8045
    , _unused(0)
    , _data_type(0)
    , _dimensions(dimensions)
{
}

void VeriVariable::ResetVarUsageInfo() // Viper #4060
{
    _is_used = 0 ; _is_assigned = 0 ;
    _is_concurrent_assigned = 0 ;
    _is_concurrent_used = 0 ;
    _is_assigned_before_used = 0 ;
    _is_used_before_assigned = 0 ;
    _can_be_dual_port_ram = 0 ;
    _can_be_multi_port_ram = 0 ;
    _is_ram_overridden = 0 ;
    // _is_automatic = 0 ; // This does not change on the usage
}

VeriVariable::~VeriVariable()
{
    // Initial value is a 'late' parse-tree addition, owned by the identifier. Delete it.
    delete _initial_value ;
    delete _dimensions ; // delete the list of ranges

#ifdef VERILOG_CREATE_ONEHOT_FSMS
    _link_var = 0 ;
    // The fsm constants is a "long int" -> VeriExpression* table.
    // No memory allocated for the elements.
    // VIPER #7995 : Allocated string is stored as key and value now, free those
    MapIter mi ;
    //VeriExpression *expr ;
    char *key, *val ;
    FOREACH_MAP_ITEM(_fsm_constants, mi, &key, &val) {
        Strings::free(key) ;
        Strings::free(val) ;
    }
    delete _fsm_constants ;
#endif

    // _data_type is a back-pointer. Don't delete.
    _data_type = 0 ;
}

void VeriVariable::SetCanBeDualPortRam()
{
    if (veri_file::ExtractRams() && !_is_ram_overridden) {
        _can_be_dual_port_ram = 1 ;
        _is_ram_overridden = 1 ;
    }
}

void VeriVariable::SetCanBeMultiPortRam()
{
    if (veri_file::ExtractMPRams() && !_is_ram_overridden) {
        _can_be_multi_port_ram = 1 ;
        _is_ram_overridden = 1 ;
    }
}

void VeriVariable::SetCannotBeDualPortRam()
{
    if (veri_file::ExtractRams()) {
        _can_be_dual_port_ram = 0 ;
        _is_ram_overridden = 1 ;
    }
}

void VeriVariable::SetCannotBeMultiPortRam()
{
    if (veri_file::ExtractMPRams()) {
        _can_be_multi_port_ram = 0 ;
        _is_ram_overridden = 1 ;
    }
}

VeriModuleId::VeriModuleId(char *n) : VeriIdDef(n), _local_scope(0), _tree(0), _is_extern(0)
, _is_package(0)
, _is_export_subprog(0)
{
}
VeriModuleId::~VeriModuleId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
}

VeriConfigurationId::VeriConfigurationId(char *name) : VeriModuleId(name) { }
VeriConfigurationId::~VeriConfigurationId() { }

VeriUdpId::VeriUdpId(char *n) : VeriModuleId(n) {}
VeriUdpId::~VeriUdpId() {}

VeriInstId::VeriInstId(char *n, VeriRange *dimensions, Array *port_connects) : VeriIdDef(n),
    _dimensions(dimensions),
    _port_connects((port_connects) ? port_connects->Size() : 1),
    _the_instance(0)
{
    if (port_connects) _port_connects.Append(port_connects) ;
    delete port_connects ;

    // Check port association list :
    // LRM is confusing on this syntax : "inst i ()"
    // could mean :
    //    - module instantiated has one port which is not connected
    //    - module instantiated has no ports (and none are connected).
    // Need to process both differently, (especially for unknown module instantiation)
    // so if the list is length 1, and that association is open, reduce it to a list length 0.
    // Related issues : 1064 and 1303.
    if (_port_connects.Size()==1) {
        VeriExpression *assoc = (VeriExpression*)_port_connects.GetLast() ;
        if (!assoc || (!assoc->IsNameExtendedClass() && assoc->IsOpen())) {
            _port_connects.Remove(0) ;
            delete assoc ;
        }
    }

    // Issue 2358 : 2001 LRM 12.3.6 : error on mixed used of ordered/named port assocs :
    // Oddly enough, this seems to be true only for module instantiation port assocs.
    // No such rule for function/task assocs or module port specification assocs.
    VeriExpression *pc ;
    unsigned i ;
    unsigned is_named = 0 ;
    unsigned is_ordered = 0 ;
    unsigned dot_star_count = 0 ; // VIPER #4257: Count the number of .* in port association
    FOREACH_ARRAY_ITEM(&_port_connects, i, pc) {
        if (!pc) continue ; // empty ordered assoc. Ordered ? Nah. Seems allowed in named lists too.
        if (pc->NamedFormal()) {
            is_named = 1 ;
        } else if (pc->IsDotStar()) {
            is_named = 1 ;
            dot_star_count++ ;
        } else {
            is_ordered = 1 ;
        }
        if (is_ordered && is_named) {
            // VCS and ModelSim give error. Follow that. Customers can always downgrade this message.
            Error("port connections cannot be mixed ordered and named") ;
            break ;
        }
        if (dot_star_count > 1) {
            // VIPER #4257 : Produce error if .* appears multiple times:
            Warning(".* token can only appear at most once in port list") ;
        }
    }
}
VeriInstId::~VeriInstId()
{
    // Delete the range and the port-association list nodes :
    delete _dimensions ;

    unsigned i ;
    VeriExpression *pc ;
    FOREACH_ARRAY_ITEM(&_port_connects, i, pc) delete pc ;
    _the_instance = 0 ;
}

unsigned VeriInstId::IsInterfaceInst() const
{
    VeriName *unit_name = (_the_instance) ? _the_instance->GetModuleNameRef(): 0 ;
    VeriIdDef *unit_id = (unit_name) ? unit_name->GetId() : 0 ;
    //VeriModule *inst_mod = GetInstantiatedModule() ;
    //return (inst_mod && inst_mod->IsInterface()) ? 1 : 0 ;
    return (unit_id && unit_id->IsInterface()) ? 1 : 0 ;
}

// VIPER #5349: Check if an identifier of type VeriPrototypeId is an interface instance:
unsigned VeriPrototypeId::IsInterfaceInst() const { return (_actual_id) ? _actual_id->IsInterfaceInst() : 0 ; }

VeriModule *VeriInstId::GetInstantiatedModule() const
{
    // Obtain pointer to the instantiated module.
    return (_the_instance) ? _the_instance->GetInstantiatedModule(): 0 ;
}

// VIPER #5349: Return pointer to the instantiated module:
VeriModule *VeriPrototypeId::GetInstantiatedModule() const { return (_actual_id) ? _actual_id->GetInstantiatedModule() : 0 ; }

const char *VeriInstId::GetModuleReference() const
{
    // Used to need separate field. Now use the VeriModuleInstantiation back-pointer :
    return (_the_instance) ? _the_instance->GetModuleName() : 0 ;
}

// VIPER #5349: Get the name of the instantiated module (fast):
const char *VeriPrototypeId::GetModuleReference() const { return (_actual_id) ? _actual_id->GetModuleReference() : 0 ; }

VeriLibrary *VeriModuleId::GetModuleLibrary() const
{
    return (_tree) ? _tree->GetLibrary() : 0 ;
}

VeriLibrary *VeriInstId::GetModuleLibrary() const
{
    // Used to need separate field. Now use the VeriModuleInstantiation back-pointer :
    return (_the_instance) ? _the_instance->GetLibrary() : 0 ;
}

// VIPER #5349:  Get the library in which this instance' module should be searched if not in work library.
VeriLibrary *VeriPrototypeId::GetModuleLibrary() const { return (_actual_id) ? _actual_id->GetModuleLibrary() : 0 ; }

// VIPER #5349: Back-pointer to the 'module_instantiation' (tree definition) for a instance identifier only
VeriModuleInstantiation *VeriPrototypeId::GetModuleInstance() const { return (_actual_id) ? _actual_id->GetModuleInstance() : 0 ; }

// VIPER #5349: Return the range (dimensions) of the identifier:
VeriRange *VeriPrototypeId::GetRange() const { return (_actual_id) ? _actual_id->GetRange() : 0 ; }

// VIPER #5349: Array of VeriExpressions (VeriPortConnect/VeriIdRef/VeriExpressions), the port connections. This list of parse tree nodes is also owned by the VeriPrototypeId.
Array *VeriPrototypeId::GetPortConnects() const { return (_actual_id) ? _actual_id->GetPortConnects() : 0 ; }

// VIPER #5349: Return the instance name:
const char *VeriPrototypeId::InstName() const { return (_actual_id ? _actual_id->InstName() : 0 ) ; }

// VIPER #5349: Rerurn the instance id. Backward compatible for old VeriInst. Unnamed instance has no name:
VeriIdDef *VeriPrototypeId::GetInstId() const { return (_actual_id) ? _actual_id->GetInstId() : 0 ; }

VeriTaskId::VeriTaskId(char *n) : VeriIdDef(n),
    _local_scope(0),
    _tree(0),
    _is_automatic(0),
    _is_forkjoin(0),
    _is_import(0),
    _is_export(0)
{
}

VeriTaskId::~VeriTaskId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
}

VeriFunctionId::VeriFunctionId(char *n) : VeriVariable(n), _local_scope(0), _tree(0)
   , _function_type(0)
{
    if (IsSystemVeri()) {
        _function_type = VeriExpression::GetFuncToken(GetName()) ;
    }
}
VeriFunctionId::~VeriFunctionId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
}

VeriGenVarId::VeriGenVarId(char *n) : VeriIdDef(n),
    _initial_value(0)
{}

VeriGenVarId::~VeriGenVarId()
{
    delete _initial_value ;
}

VeriParamId::VeriParamId(char *n) : VeriIdDef(n)
    , _data_type(0)
    , _dimensions(0)
    , _initial_value(0)
    , _param_type(0)
    , _type(0)
    , _is_used(0)
    , _is_assigned(0)
    , _is_type(0)
    , _is_elaborated(0)
    , _is_processing(0)
    , _id_implicit(0)
#ifdef VERILOG_PATHPULSE_PORTS
    ,_path_pulse_ports(0)
#endif
{}
VeriParamId::~VeriParamId()
{
    // Since we don't have a different id for enum literals, we use param id for
    // them. So, this param id could be an enum literal as well, check it here:
    // VIPER 5470 : make sure to use IsEnumType here (if any test) since datatype->Type() can recur into already freed tree nodes.
    if (_data_type && _data_type->IsEnumType()) {
        // It is an enum literal. Since, this id is going to be deleted, remove
        // reference of this from the id array of that enum to avoid corruption:
        Array *ids = _data_type->GetIds() ;
        unsigned i ;
        VeriIdDef *me ;
        FOREACH_ARRAY_ITEM(ids, i, me) {
            if (me == this) {
                // Insert 0 in this position;
                ids->Insert(i, 0) ;
                break ;
            }
        }
    }
    _data_type = 0 ;
    delete _dimensions ;
    delete _initial_value ;
    // _actual is just borrowed from the caller. So don't free it.
#ifdef VERILOG_PATHPULSE_PORTS
    delete _path_pulse_ports ;
#endif
}

void VeriParamId::ResetVarUsageInfo() { _is_used = 0 ; _is_assigned = 0 ; } // Viper #4060

VeriBlockId::VeriBlockId(char *n) : VeriIdDef(n)
    , _local_scope(0)
    , _tree(0)
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    , _is_unnamed_blk(0)
    , _orig_name(0)
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
{ }
VeriBlockId::~VeriBlockId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    Strings::free(_orig_name) ; _orig_name = 0 ;
//#endif
}
unsigned
VeriBlockId::IsLabel() const
{
    // Check if this block id has been used as System Verilog statement label.
    // Label ids does not have scope and has module item back-pointer set.
    if (_local_scope || !_tree) return 0 ; // Cannot be a label
    // Had to cast the VeriIdDef * returned from _tree->GetOpeningLabel() to
    // const VeriIdDef * to fix MS VC++ .Net on Windows compilation error:
    if (this == (const VeriIdDef *)_tree->GetOpeningLabel()) return 1 ;
    return 0 ;
}

VeriInterfaceId::VeriInterfaceId(char *n) : VeriModuleId(n), _ele_ids(0) { }
VeriInterfaceId::~VeriInterfaceId()
{
// local_scope decoupling already done in VeriModuleId destructor
//    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    delete _ele_ids ;
}

VeriProgramId::VeriProgramId(char *n) : VeriModuleId(n) { }
VeriProgramId::~VeriProgramId()
{
// local_scope decoupling already done in VeriModuleId destructor
//    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
}
// VIPER #5710:
VeriCheckerId::VeriCheckerId(char *n) : VeriModuleId(n) { }
VeriCheckerId::~VeriCheckerId()
{
}

VeriTypeId::VeriTypeId(char *n, VeriRange *dimensions /* = 0 */)
    : VeriVariable(n, dimensions),
    _local_scope(0),
    _tree(0),
    _is_processing(0),
    _forward_type_defined(0) // VIPER #7951 (duplicate_classes)
// VIPER #6895:
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    , _unconstraint_unpacked_dim_count(0)
    , _unconstraint_packed_dim_count(0)
    , _vhdl_package_name(0)
    , _vhdl_lib_name(0)
#endif
    , _elab_created(0)
{
}

VeriTypeId::~VeriTypeId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER# 6895:
    Strings::free(_vhdl_package_name) ;
    Strings::free(_vhdl_lib_name) ;
    _vhdl_package_name = 0 ;
    _vhdl_lib_name = 0 ;
#endif
}
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
VeriScope *VeriTypeId::GetOwningScope() const
{
    return (_tree) ? _tree->GetOwningScope() : 0 ;
}
#endif

VeriModportId::VeriModportId(char *n) : VeriIdDef(n), _local_scope(0), _tree(0), _ele_ids(0) { }
VeriModportId::~VeriModportId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
    delete _ele_ids ;
}

VeriOperatorId::VeriOperatorId(unsigned oper_type) : VeriIdDef(Strings::save(PrintToken(oper_type))), _oper_type(oper_type), _tree(0) { }
VeriOperatorId::~VeriOperatorId() { _tree = 0 ; }

VeriClockingId::VeriClockingId(char *n) : VeriVariable(n), _local_scope(0), _tree(0), _is_default(0)  {}
VeriClockingId::~VeriClockingId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
}
VeriProductionId::VeriProductionId(char *n) : VeriIdDef(n), _prod(0) {}
VeriProductionId::~VeriProductionId()
{
    // VIPER #3369: Clear module item's back pointer to this id
    if (_prod) _prod->ClearId() ;
    _prod = 0 ; // Clear the back pointer to production item
}
unsigned VeriProductionId::Type() const
{
    if (!_prod) return 0 ;
    VeriDataType *production_type = _prod->GetDataType() ;
    return production_type ? production_type->Type(): 0 ;
}
VeriNamedPort::VeriNamedPort(char *name, VeriExpression *port_expr) :
    VeriVariable(name),
    _port_expr(port_expr),
    _is_processing(0)
{}
VeriNamedPort::~VeriNamedPort()
{
    delete _port_expr ;
}
void VeriNamedPort::SetIsProcessing() const
{
    VeriNamedPort *this_ref = const_cast<VeriNamedPort*>(this) ;
    if (this_ref) this_ref->_is_processing = 1 ;
}
void VeriNamedPort::ClearIsProcessing() const
{
    VeriNamedPort *this_ref = const_cast<VeriNamedPort*>(this) ;
    if (this_ref) this_ref->_is_processing = 0 ;
}
unsigned
VeriNamedPort::IsRenamedNamedPort() const
{
    VeriIdDef *actual_id = (_port_expr) ? _port_expr->FullId() : 0 ;
    if (!actual_id) return 1 ; // Must be complex port expression: renamed
    // If the formal and actual name natch, return 0; 1 otherwise:
    return (Strings::compare(Name(), actual_id->Name()) ? 0 : 1) ;
}
// VIPER #3384
VeriCovgOptionId::VeriCovgOptionId(char *name) : VeriIdDef(name)
    , _data_type(0)
{}
VeriCovgOptionId::~VeriCovgOptionId()
{
    delete _data_type ;
}

// VIPER #5871
VeriBinsId::VeriBinsId(char *name) : VeriVariable(name, 0)
{}
VeriBinsId::~VeriBinsId()
{
}
// VIPER #3370 :
VeriSeqPropertyFormal::VeriSeqPropertyFormal(char *name, VeriRange *dimensions) :
    VeriVariable(name, dimensions),
    _actual(0),
    _is_processing(0)
{}
VeriSeqPropertyFormal::~VeriSeqPropertyFormal() { _actual = 0 ; }

void VeriSeqPropertyFormal::SetIsProcessing() const
{
    VeriSeqPropertyFormal *this_ref = const_cast<VeriSeqPropertyFormal*>(this) ;
    if (this_ref) this_ref->_is_processing = 1 ;
}
void VeriSeqPropertyFormal::ClearIsProcessing() const
{
    VeriSeqPropertyFormal *this_ref = const_cast<VeriSeqPropertyFormal*>(this) ;
    if (this_ref) this_ref->_is_processing = 0 ;
}
VeriPrototypeId::VeriPrototypeId(char *name):
    VeriIdDef(name),
    _dir(0),
    _actual_name(0),
    _actual_id(0)
{}
VeriPrototypeId::VeriPrototypeId(VeriName *name):
    VeriIdDef(Strings::save((name)?name->GetName():0)),
    _dir(0),
    _actual_name(name),
    _actual_id(0)
{}
VeriPrototypeId::~VeriPrototypeId() { delete _actual_name ; _actual_id = 0 ; }
// VIPER #5349: Check if an identifier of type VeriPrototypeId is an instance:
unsigned VeriPrototypeId::IsInst() const                { return (_actual_id) ? _actual_id->IsInst() : 0 ; }
void VeriPrototypeId::ResetVarUsageInfo()               { if (_actual_id) _actual_id->ResetVarUsageInfo() ; }
unsigned VeriPrototypeId::IsUsed() const                { return (_actual_id) ? _actual_id->IsUsed(): 0 ; }
unsigned VeriPrototypeId::IsAssigned() const            { return (_actual_id) ? _actual_id->IsAssigned(): 0 ; }
void VeriPrototypeId::SetUsed()                         { if(_actual_id) _actual_id->SetUsed() ; }
void VeriPrototypeId::SetAssigned()                     { if(_actual_id) _actual_id->SetAssigned() ; }
void VeriPrototypeId::SetConcurrentUsed()               { if(_actual_id) _actual_id->SetConcurrentUsed() ; }
void VeriPrototypeId::SetConcurrentAssigned()           { if(_actual_id) _actual_id->SetConcurrentAssigned() ; }
unsigned VeriPrototypeId::IsConcurrentUsed() const      { return (_actual_id) ? _actual_id->IsConcurrentUsed(): 0 ; }
unsigned VeriPrototypeId::IsConcurrentAssigned() const  { return (_actual_id) ? _actual_id->IsConcurrentAssigned(): 0 ; }
void VeriPrototypeId::SetAssignedBeforeUsed()           { if(_actual_id) _actual_id->SetAssignedBeforeUsed() ; }
void VeriPrototypeId::SetUsedBeforeAssigned()           { if(_actual_id) _actual_id->SetUsedBeforeAssigned() ; }
unsigned VeriPrototypeId::IsAssignedBeforeUsed() const  { return (_actual_id) ? _actual_id->IsAssignedBeforeUsed(): 0 ; }
void VeriPrototypeId::UnSetAssignedBeforeUsed()         { if(_actual_id) _actual_id->UnSetAssignedBeforeUsed() ; }
void VeriPrototypeId::UnSetUsedBeforeAssigned()         { if(_actual_id) _actual_id->UnSetUsedBeforeAssigned() ; }
unsigned VeriPrototypeId::IsUsedBeforeAssigned() const  { return (_actual_id) ? _actual_id->IsUsedBeforeAssigned(): 0 ; }

unsigned VeriPrototypeId::CanBeOnehot() const           { return (_actual_id) ? _actual_id->CanBeOnehot(): 0 ; }
void VeriPrototypeId::SetCanBeOnehot()                  { if (_actual_id) _actual_id->SetCanBeOnehot() ; }
void VeriPrototypeId::SetCannotBeOnehot()               { if (_actual_id) _actual_id->SetCannotBeOnehot() ; }
void VeriPrototypeId::LinkOnehotVar(VeriIdDef *var)     { if (_actual_id) _actual_id->LinkOnehotVar(var) ; }
void VeriPrototypeId::AddFsmStateConstant(VeriExpression *state_expression) { if (_actual_id) _actual_id->AddFsmStateConstant(state_expression) ; }
Map *VeriPrototypeId::FsmStateConstants() const         { return (_actual_id) ? _actual_id->FsmStateConstants(): 0 ; }

unsigned VeriPrototypeId::CanBeDualPortRam() const      { return (_actual_id) ? _actual_id->CanBeDualPortRam(): 0 ; }
void VeriPrototypeId::SetCanBeDualPortRam()             { if (_actual_id) _actual_id->SetCanBeDualPortRam() ;}
void VeriPrototypeId::SetCannotBeDualPortRam()          { if (_actual_id) _actual_id->SetCannotBeDualPortRam() ;}
unsigned VeriPrototypeId::CanBeMultiPortRam() const     { return (_actual_id) ? _actual_id->CanBeMultiPortRam(): 0 ; }
void VeriPrototypeId::SetCanBeMultiPortRam()            { if (_actual_id) _actual_id->SetCanBeMultiPortRam() ;}
void VeriPrototypeId::SetCannotBeMultiPortRam()         { if (_actual_id) _actual_id->SetCannotBeMultiPortRam() ;}

unsigned VeriPrototypeId::IsMember() const              {  return (_actual_id) ? _actual_id->IsMember(): 0 ; }
void VeriPrototypeId::SetIsMember()                     { } //if (_actual_id) _actual_id->SetIsMember() ; }
unsigned VeriPrototypeId::IsModportPort() const         {  return (_actual_id) ? _actual_id->IsModportPort(): 0 ; }
void VeriPrototypeId::SetIsModportPort()                { } //if (_actual_id) _actual_id->SetIsModportPort() ; }
unsigned VeriPrototypeId::IsAnsiPort() const            { return (_actual_id) ? _actual_id->IsAnsiPort(): 0 ; }
void VeriPrototypeId::SetIsAnsiPort()                   { } //if (_actual_id) _actual_id->SetIsAnsiPort() ; }
unsigned VeriPrototypeId::IsConstant() const            { return (_actual_id) ? _actual_id->IsConstant(): 0 ; }
void VeriPrototypeId::SetIsConstant()                   { } //if (_actual_id) _actual_id->SetIsConstant() ; }
unsigned VeriPrototypeId::IsAutomatic() const           { return (_actual_id) ? _actual_id->IsAutomatic() : 0 ; }
void VeriPrototypeId::SetIsAutomatic()                  { } //if (_actual_id) _actual_id->SetIsAutomatic() ; }
unsigned VeriPrototypeId::IsVirtualInterface() const    { return (_actual_id) ? _actual_id->IsVirtualInterface(): 0 ; }
unsigned VeriPrototypeId::IsMemory() const              { return (_actual_id) ? _actual_id->IsMemory(): 0 ; }
unsigned VeriPrototypeId::IsClocking() const            { return (_actual_id) ? _actual_id->IsClocking(): 0 ; }
unsigned VeriPrototypeId::IsDefaultClocking() const     { return (_actual_id) ? _actual_id->IsDefaultClocking(): 0 ; }
void     VeriPrototypeId::SetAsDefault()                { } //if (_actual_id) _actual_id->SetAsDefault() ; }
unsigned VeriPrototypeId::GetFunctionType() const       { return (_actual_id) ? _actual_id->GetFunctionType(): 0 ; }
VeriRange *  VeriPrototypeId::GetDimensions() const     { return (_actual_id) ? _actual_id->GetDimensions(): 0 ; }

VeriModuleItem *VeriPrototypeId::GetModuleItem() const  { return (_actual_id) ? _actual_id->GetModuleItem(): 0 ; }
// VIPER #5333 : Do not set module item to actual identifier of prototype id :
void VeriPrototypeId::SetModuleItem(VeriModuleItem * /*t*/) { } //if (_actual_id) _actual_id->SetModuleItem(t) ; }

unsigned VeriPrototypeId::IsTask() const                { return (_actual_id) ? _actual_id->IsTask(): 0 ; }
unsigned VeriPrototypeId::IsFunction() const            { return (_actual_id) ? _actual_id->IsFunction(): 0 ; }
void VeriPrototypeId::SetLocalScope(VeriScope * /*local_scope*/) { } //if (_actual_id) _actual_id->SetLocalScope(local_scope) ; }
VeriScope *VeriPrototypeId::LocalScope() const          { return (_actual_id) ? _actual_id->LocalScope(): 0 ; }

Array *VeriPrototypeId::GetPorts()                      { return (_actual_id) ? _actual_id->GetPorts(): 0 ; }
VeriLetId::VeriLetId(char *name)
    : VeriIdDef(name),
    _local_scope(0),
    _tree(0),
    _is_processing(0)
{}
VeriLetId::~VeriLetId()
{
    if (_local_scope && (_local_scope->GetOwner() == this))  _local_scope->SetOwner(0) ; // Remove link to 'this'
    _local_scope = 0 ;
    // VIPER #3369: Clear module item's back pointer to this id
    if (_tree) _tree->ClearId() ;
    _tree = 0 ;
}
void VeriLetId::SetIsProcessing() const
{
    VeriLetId *this_ref = const_cast<VeriLetId*>(this) ;
    if (this_ref) this_ref->_is_processing = 1 ;
}
void VeriLetId::ClearIsProcessing() const
{
    VeriLetId *this_ref = const_cast<VeriLetId*>(this) ;
    if (this_ref) this_ref->_is_processing = 0 ;
}
VeriExternForkjoinTaskId::VeriExternForkjoinTaskId(char *name)
    : VeriTaskId(name),
    _trees(0)
{
}
VeriExternForkjoinTaskId::~VeriExternForkjoinTaskId()
{
    unsigned i ;
    VeriModuleItem *tree ;
    FOREACH_ARRAY_ITEM(_trees, i, tree) {
        if (tree) tree->ClearId() ;
        // Do not miss back pointer in other body's scope
        VeriScope *body_scope = (tree) ? tree->GetScope(): 0 ;
        if (body_scope) body_scope->SetOwner(0) ;
    }
    delete _trees ;
    _trees = 0 ;
}
void VeriExternForkjoinTaskId::AddExternForkjoinDef(VeriModuleItem *tree)
{
    if (!tree) return ;
    if (!_trees) _trees = new Array(1) ;
    _trees->InsertLast(tree) ;
}
VeriLabelId::VeriLabelId(char *name)
    : VeriBlockId(name),
      _scope(0)
{}
VeriLabelId::~VeriLabelId()
{
    delete _scope ; _scope = 0 ;
}
void VeriLabelId::SetScope(VeriScope *s)
{
    delete _scope ;
    _scope = s ;
    if (s) _local_scope = s ;
}

// 'type' of various SV objects that can be used as 'variables'
unsigned VeriTypeId::IsClass() const   { return ((_type == VERI_CLASS) || (_type == VERI_INTERFACE)) ? 1 : 0 ; }
unsigned VeriTypeId::IsCovergroup() const   { return (_type == VERI_COVERGROUP) ? 1 : 0 ; }
unsigned VeriInterfaceId::Type() const { return VERI_INTERFACE ; }
unsigned VeriModportId::Type() const   { return VERI_MODPORT ; }

void VeriTypeId::SetDataType(VeriDataType *data_type)
{
    if (data_type==_data_type) return ; // nothing to do (same thing just happened twice).

    // Check for type mismatch in forward declaration and actual declaration:
    unsigned new_type = (data_type) ? data_type->Type() : 0 ;
    // VIPER #3449: Special handling for enum data type. If the enum has a base type
    // we return that type as the Type for enum not VERI_ENUM, so adjust here for that:
    if (data_type && data_type->GetEnums()) new_type = VERI_ENUM ;
    if (!_data_type && _type && new_type && (_type != new_type)) {
        // Got a mismatch in actual declaration:
        Error("%s was forward declared as %s type not as %s", Name(), PrintToken(_type), PrintToken(new_type)) ;
    }

    // Call the base class' routine:
    VeriVariable::SetDataType(data_type) ;
}

void VeriTypeId::SetModuleItem(VeriModuleItem *t)
{
    // Currently, we only set the module item for classes, since only class ids
    // are treated as type and thus is represented as VeriTypeId:
    if (!t) return ;

    // Set module item back pointer to user defined types also, if it is not set already.
    if (!_tree && !t->IsClass() && !t->IsCovergroup()) {
        _tree = t ;
        return ;
    }
    // Discard other module item expect class, it may reset valid _tree back pointer:
    if (!(t->IsClass() || t->IsCovergroup())) return ;

    unsigned is_interface_class = 0 ;
    unsigned is_normal_class = 0 ;
    if (t->IsClass()) {
        if (t->GetQualifier(VERI_INTERFACE)) {
            is_interface_class = 1 ;
        } else {
            is_normal_class = 1 ;
        }
    }
    // Check for type mismatch in forward declaration and actual declaration:
    if (_type && (((_type != VERI_CLASS) && is_normal_class) || ((_type != VERI_COVERGROUP) && t->IsCovergroup()) || ((_type != VERI_INTERFACE) && is_interface_class))) {
        // This was forward declared as something other than class, but now declared as class:
        t->Error("%s was forward declared as %s type not as %s", Name(), PrintToken(_type), t->IsClass() ? (is_interface_class ? "interface class" : PrintToken(VERI_CLASS)): PrintToken(VERI_COVERGROUP)) ;
    }

    if (_tree && (_tree->IsClass() || _tree->IsCovergroup())) {
        // This is probably a class declaration which is already declared:
        t->Error("%s is already declared", Name()) ;
        //_tree->Info("%s is declared here", Name()) ;
        _tree->Info("previous declaration of %s is from here", Name()) ; // VIPER #7773
    }

    // Set the module item back pointer as well as the type:
    _type = t->IsClass() ? (is_interface_class ? VERI_INTERFACE: VERI_CLASS): VERI_COVERGROUP ;
    _tree = t ;
}
void VeriTypeId::SetIsProcessing() const
{
    VeriTypeId *this_ref = const_cast<VeriTypeId*>(this) ;
    if (this_ref) this_ref->_is_processing = 1 ;
}
void VeriTypeId::ClearIsProcessing() const
{
    VeriTypeId *this_ref = const_cast<VeriTypeId*>(this) ;
    if (this_ref) this_ref->_is_processing = 0 ;
}

void VeriCovgOptionId::SetDataType(VeriDataType *data_type)
{
    if (data_type==_data_type) return ; // nothing to do (same thing just happened twice).

    delete _data_type ;
    _data_type = data_type ;
}

/*********************************************************************************/
/* Late tree node additions */
/*********************************************************************************/

// Set the memory dimensions :
// Might be needed to override unpacked range after construction..
void VeriIdDef::SetMemoryRange(VeriRange * /*dimensions*/) {
    Error("%s does not accept dimensions",Name()) ;
}

// VIPER #6895: Add a new dimension to the end of existing dimension
void VeriIdDef::AddMemoryRange(VeriRange* /*dimensions*/)
{
    Error("%s does not accept dimensions",Name()) ;
}

void VeriVariable::SetMemoryRange(VeriRange *dimensions)
{
    if (!dimensions) return ;

    //if (_dimensions == dimensions) return ; // Nothing to do...

    // delete previous dimensions. If any
    delete _dimensions ;
    _dimensions = dimensions ;
}

// VIPER #6895: Add a new dimension to the end of existing dimension
void VeriVariable::AddMemoryRange(VeriRange *dimensions)
{
    if (!dimensions) return ;
    if (_dimensions) {
        _dimensions->SetNext(dimensions) ;
    } else {
        _dimensions = dimensions ;
    }
}

void VeriInstId::SetMemoryRange(VeriRange *dimensions)
{
    if (!dimensions) return ;

    // delete previous dimensions. If any
    delete _dimensions ;
    _dimensions = dimensions ;
}

// VIPER #6895: Add a new dimension to the end of existing dimension
void VeriInstId::AddMemoryRange(VeriRange *dimensions)
{
    if (!dimensions) return ;
    if (_dimensions) {
        _dimensions->SetNext(dimensions) ;
    } else {
        _dimensions = dimensions ;
    }
}

// Set the memory dimensions :
void VeriParamId::SetMemoryRange(VeriRange *dimensions)
{
    if (!dimensions) return ;

    // delete previous dimensions. If any
    delete _dimensions ;
    _dimensions = dimensions ;
}

// VIPER #6895: Add a new dimension to the end of existing dimension
void VeriParamId::AddMemoryRange(VeriRange *dimensions)
{
    if (!dimensions) return ;
    if (_dimensions) {
        _dimensions->SetNext(dimensions) ;
    } else {
        _dimensions = dimensions ;
    }
}

void VeriParamId::SetIsProcessing() const
{
    VeriParamId *this_ref = const_cast<VeriParamId*>(this) ;
    if (this_ref) this_ref->_is_processing = 1 ;
}
void VeriParamId::ClearIsProcessing() const
{
    VeriParamId *this_ref = const_cast<VeriParamId*>(this) ;
    if (this_ref) this_ref->_is_processing = 0 ;
}
void VeriPrototypeId::SetMemoryRange(VeriRange * /*dimensions*/) { } //if (_actual_id) _actual_id->SetMemoryRange(dimensions) ; }
// VIPER #6895: Add a new dimension to the end of existing dimension
void VeriPrototypeId::AddMemoryRange(VeriRange* /*dimensions*/) { }

// Initial value for parameters and variables
void VeriIdDef::SetInitialValue(VeriExpression *expr) {
    if (expr) expr->Error("%s does not accept an initial value",Name()) ;
    delete expr ;
}
void VeriVariable::SetInitialValue(VeriExpression *expr) {
    if (!expr) return ;

    //if (_initial_value == expr) return ; // Nothing to do...

    delete _initial_value ;
    _initial_value = expr ;

    // SV 2012: 6.7.1: Check for generic interconnect
    if (IsInterconnectType()) Error("generic interconnect must not specify initial assignment expression") ;
}
void VeriParamId::SetInitialValue(VeriExpression *expr) {
    if (!expr) return ;

    // VIPER #7472: If the initial value is the data type itself (it may be
    // a type parameter), clear the data type to avoid memory corruption,
    // we are going to delete the initial value and hence the data type too:
    //VERIFIC_ASSERT(!_data_type || (_data_type!=_initial_value)) ;
    if (_data_type && (_data_type==_initial_value)) ClearDataType() ;

    delete _initial_value ;
    _initial_value = expr ;
}
void VeriGenVarId::SetInitialValue(VeriExpression *expr) {
    if (!expr) return ;

    delete _initial_value ;
    _initial_value = expr ;
}
void VeriPrototypeId::SetInitialValue(VeriExpression * /*expr*/) {
    //if (_actual_id) _actual_id->SetInitialValue(expr) ;
}
VeriExpression *VeriPrototypeId::GetInitialValue() const { return (_actual_id) ? _actual_id->GetInitialValue() : 0 ; }
VeriExpression *VeriPrototypeId::TakeInitialValue()  { return (_actual_id) ? _actual_id->TakeInitialValue(): 0 ; }
VeriExpression *VeriVariable::TakeInitialValue()
{
    VeriExpression *result = _initial_value ; // Store initial value
    _initial_value = 0 ; // Make this field null
    return result ; // Return value
}
VeriExpression * VeriParamId::TakeInitialValue()
{
    VeriExpression *result = _initial_value ; // Store initial value
    _initial_value = 0 ; // Make this field null
    return result ; // Return value
}

/*********************************************************************************/
/* Change name of an identifier    */
/* CAUTION : Do NOT do this for identifiers that are stored in a scope ! The scope will not change, so stale pointer will remain. */
/*********************************************************************************/

// JJ 130108: change SetName to accept a const char * and make a copy, rely on caller to handle the memory

void
VeriIdDef::SetName(const char *name)
{
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ;
    _name = Strings::save(name) ; // JJ: 130108 no longer absorb name, rely on caller to handle memory
#else
    _name = Strings::CreateConstantString(name) ;
#endif
}

// VIPER #5949: Set the _kind field to the variables if the data_type
// does not exist depending upon the direction and also set the _type
// if the _type is still not set.
void VeriVariable::SetKindFromDataType()
{
    // Find the default_kind:
    // VIPER #7204: Set default_kind to the default_nettype:
    unsigned default_kind = veri_file::GetDefaultNettype() ;

    if (!_kind) { // VIPER #7885: Set the _kind field only if _kind is not set
        switch (_dir) {
        case 1 : _kind = _type ? _type : default_kind ; break ;
                 // VIPER #6406 : Consider ams mode as ams+verilog-2001
        case 2 : if (IsVeri95() || IsVeri2001() || IsAms()) {
                     _kind = _type ? _type : default_kind ;
                 } else {
                     _kind = _type ? VERI_REG : default_kind ;
                 }
                 break ;
        case 3 : _kind = default_kind ; break ;
        case 4 : _kind = VERI_VAR ; break ;
        default : break ; // do not know this direction
        }
    }

    // Set _type if not set:
    if (IsVeri95() || IsVeri2001() || IsAms()) {
        if (!_type) _type = default_kind ;
    } else {
        // VIPER #6527: If default_nettype is none for a port having no data type,
        // type will be none also.
        if (!_type) _type = (default_kind == VERI_NONE) ? VERI_NONE: VERI_LOGIC ;
    }
}

/*********************************************************************************/
/* Set data type on a identifier    */
/*********************************************************************************/

// this sets back_pointer from id to data type, and sets default type field in this id.
void VeriVariable::SetDataType(VeriDataType *data_type)
{
    if (data_type==_data_type) return ; // nothing to do (same thing just happened twice).

    // This routine is run construction (before resolve) of VeriDataDecl and VeriAnsiPortDecl.

    // First, do checks for re-declarations.
    // ports can be re-declared to nets/variables legally.
    // Just report re-declarations in range-definition inconsistency :
    if (data_type && _data_type) {
        // Previously declared identifier.
        //

        // Either way, check range consistency on the ranges (ans signed info?) :
        // LRM 12.3.3 rule  is confusing between 2001 and 1995 definitions for if only one of them has a range.
        // Issue 1416 : accept mismatch in declaration, under a warning.
        if (!_data_type->GetDimensions() && data_type->GetDimensions()) {
            data_type->Warning("%s was previously declared without a range", Name()) ;
        } else if (_data_type->GetDimensions() && !data_type->GetDimensions()) {
            data_type->Warning("%s was previously declared with a range", Name()) ;
        }

        // Issue 2015 : If the existing data type  back-pointer points to a valid type,
        // and the new one does not (because it only specifies a range or so),
        // then we should not override the existing data type.
        //
        // The only legal overrides (with two data types) is if this is a port (re)declaration :
        //    "input [3:0] a ;"  <-- data type contains only "[3:0]". No base type info.
        // followed by
        //    "wire [3:0] a ;"   <-- data type contains range and base type VERI_WIRE.
        //
        // Check here if it is a legal override :
        // 'Type()' works great for testing standard 'base' types (like 'reg' and 'wire' and such),
        // but for named types we should check if there is a 'named' (typeref). That is also valid as a type'.
        if (IsPort() && !(_data_type->Type() || _data_type->GetTypeName())) {
            // Previous declaration was a port decl without a type ; like "input [3:0] a ;"
            // Now the real declaration comes up. So that is a legal re-declaration.
            // Swap the data type to the new one
        } else {
            // Here, we will ignore the second declaration.
            // Do not issue a message if this is a legal override.
            //   "reg signed a ;"
            // followed by :
            //   "input signed a ;"
            if (IsType() || IsMember()) {
                // A type (historically a 'variable' that has its datatype set to something (not a prototype identifier)
                // is an illegal re-declaration.
                data_type->Error("%s is already declared", Name()) ;
                //_data_type->Info("%s is declared here", Name()) ;
                _data_type->Info("previous declaration of %s is from here", Name()) ; // VIPER #7773
            } else if (data_type->Type() || data_type->GetTypeName()) {
                // There is a standard base type or a named type in both data types,
                // so this is a real re-declaration. This is an error in ModelSim, but a warning in VCS.
                // Stick to warning. Customers can upgrade message to an error.
// RD: test moved to yacc
//                data_type->Warning("%s is already declared", Name()) ;
//                data_type->Warning("second declaration of %s ignored", Name()) ;
//                //_data_type->Info("%s is declared here",Name()) ;
//                _data_type->Info("previous declaration of %s is from here",Name()) ; // VIPER #7773
            } else {
                // legal re-daclaration (like "input [3:0] a;" after the net declaration for a,
                // New data type one does not have a base type. So ignore the new one.
            }
            return ; // do NOT override the declaration.
        }
    }

    // set back-pointer :
    _data_type = data_type ;

    // VIPER #5949: Set flag for class/udp/checker:
    VeriIdDef *id = _data_type ? _data_type->GetId() : 0 ;
    unsigned is_class = id ? id->IsClass() : 0 ;
    unsigned is_udp = id ? id->IsUdp() : 0 ;
    unsigned is_checker = id ? id->IsChecker() : 0 ;

    // We have to set base 'type' info now (if available).
    // Cannot wait until 'Resolve()' time, because of (SV) forward typedefs.
    // In forward typedef's, the actual type definition typically occurs AFTER the declaration of id's of that type.
    // so forward type-declarations will not obtain 'type' info in time, and Resolve()
    // would assign default type to identifiers of a forward typedef type..
    // Only set the type if the data type is resolved. Otherwise, leave _type unchanged (might contain default type).
    // Only set the kind for net data_type if the data type is resolved. Otherwise, leave _kind unchanged (might contain default kind).
    // unsigned type = (_data_type) ? _data_type->Type() : 0 ;
    // if (type) _type = type ;
    unsigned type = 0 ;
    unsigned kind = 0 ;
    unsigned default_type = 0 ;
    // VIPER #6406 : Consider ams mode as ams+verilog-2001
    if (IsVeri95() || IsVeri2001() || IsAms()) {
        default_type = VERI_REG ;
    } else {
        default_type = VERI_LOGIC ;
    }

    if (_data_type) {
        // Set _type field for port variables:
        if (IsPort()) {
            // Set _type of the data_type to the
            // _kind field of interface ports:
            if (IsInterfacePort()) {
                kind = _data_type->Type() ;
                type = _data_type->Type() ;
            } else {
                // If the data_type is net data_type then
                // set _kind with the net type and _type
                // with the type of the base data_type:
                if (_data_type->IsNetDataType()) {
                    kind = _data_type->NetType() ;
                    VeriDataType *d_type = _data_type->GetDataType() ;
                    type = d_type ? d_type->Type() : 0 ;
                } else {
                    // For class, udp and check set _type
                    // to the type of data_type:
                    if (is_class || is_udp || is_checker) {
                        type = _data_type->Type() ;
                    //  If the data_type is non-net type ie reg/logic
                    } else if (!_data_type->IsNetType()) {
                        type = _data_type->Type() ;
                    } else { // If the data_type is net type ie wire/wand
                        if (IsVeri95() || IsVeri2001() || IsAms()) {
                            type = _data_type->Type() ;
                        } else {
                            type = default_type ;
                        }
                    }
                }
            }
        } else { // For non-port variables:
            // If the data_type is net data_type then
            // set _kind with the net type and _type
            // with the type of the base data_type:
            if (_data_type->IsNetDataType()) {
                kind = _data_type->NetType() ;
                VeriDataType *d_type = _data_type->GetDataType() ;
                type = d_type ? d_type->Type() : 0 ;
            } else {
                // For class and udp set _type
                // to the type of data_type:
                if (is_class || is_udp) {
                    type = _data_type->Type() ;
                // If the data_type is non-net type ie reg/logic
                } else if (!_data_type->IsNetType()) {
                    // VIPER #7250: In system verilog mode populate _kind
                    // field with VERI_VAR, in other mode populate it with
                    // the type of its _data_type
                    if (IsVeri95() || IsVeri2001() || IsAms()) {
                        kind = _data_type->Type() ;
                    } else {
                        kind = VERI_VAR ;
                    }
                    type = _data_type->Type() ;
                } else { // If the data_type is net type ie wire/wand
                    kind = _data_type->Type() ;
                    if (IsVeri95() || IsVeri2001() || IsAms()) {
                        type = _data_type->Type() ;
                    } else {
                        type = default_type ;
                    }
                }
            }
        }

        if (kind) _kind = kind ; // Set _kind
        if (type) _type = type ; // Set _type

        // SV 2012: 6.7.1: Check for generic interconnect
        if (_initial_value && IsInterconnectType()) Error("generic interconnect must not specify initial assignment expression") ;
    }
}

void
VeriVariable::ClearDataType()
{
    _data_type = 0 ;
}

void
VeriVariable::ClearMatchedDataType(const VeriDataType *data_type)
{
    if (_data_type == data_type) _data_type = 0 ;
}

void VeriParamId::SetDataType(VeriDataType *data_type)
{
    if (data_type==_data_type) return ; // nothing to do (same thing just happened twice).

    if (_data_type && _data_type->IsEnumType()) {
        // VIPER #7472: Refuse to set data type if I am an enum-id to avoid corruption:
        // Do not return from here, _data_type may have been deleted after this
        // SetDataType() call and will leave this node corrupted. Proceed with care:
        //return ;

        // VIPER #7472: If it was a enum literal and now to be treated as something else,
        // remove reference of this from the id array of that enum to avoid corruption:
        Array *ids = _data_type->GetIds() ;
        unsigned i ;
        VeriIdDef *me ;
        FOREACH_ARRAY_ITEM(ids, i, me) {
            if (me == this) {
                // Insert 0 in this position;
                ids->Insert(i, 0) ;
                break ;
            }
        }
    }

    // set back-pointer :
    _data_type = data_type ;

    // We have to set base 'type' info now (if available).
    // Cannot wait until 'Resolve()' time, because of (SV) forward typedefs.
    // In forward typedef's, the actual type definition typically occurs AFTER the declaration of id's of that type.
    // so forward type-declarations will not obtain 'type' info in time, and Resolve()
    // would assign default type to identifiers of a forward typedef type..
    // Only set the type if the data type is resolved. Otherwise, leave _type unchanged (might contain default type).
    unsigned type = (_data_type) ? _data_type->Type() : 0 ;
    if (type) _type = type ;
}

void
VeriParamId::ClearDataType()
{
    _data_type = 0 ;
}

void
VeriParamId::ClearMatchedDataType(const VeriDataType *data_type)
{
    if (_data_type == data_type) _data_type = 0 ;
}

VeriDataType *
VeriParamId::GetDataType() const
{
    if (!_data_type) return 0 ;
    // Special case for type-parameters.
    // Type-parameters have a data_type back-pointer, but it is of type VERI_TYPE.
    // We should return the type-expression of the initial value :
    //if (_data_type->GetType()==VERI_TYPE) {
    if (IsType()) {
        // Clean-Dynamically cast to a data type class. Bison rules guarantees this is a VeriDataType class.
        if (_initial_value) return _initial_value->DataTypeCast() ;
    }
    return _data_type ;
}
VeriDataType *VeriSeqPropertyFormal::GetDataType() const
{
    // If data type exists, return this
    // VIPER #3730 : Return data type when either data type or unpacked dimension exists
    if (_data_type || _dimensions) return _data_type ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing
    // No data type, extract type from actual
    VeriDataType *ret_type =  (_actual) ? _actual->GetBaseDataType(): 0 ;
    ClearIsProcessing() ; // finished processing
    // FIXME : Do not use GetBaseDataType, extract type by CheckType and return
    return ret_type ;
}
void VeriPrototypeId::SetDataType(VeriDataType * /*data_type*/) { }//if (_actual_id) _actual_id->SetDataType(data_type) ; }
VeriDataType *VeriPrototypeId::GetDataType() const { return (_actual_id) ? _actual_id->GetDataType() : 0 ; }
void VeriPrototypeId::ClearDataType() { } // if (_actual_id) _actual_id->ClearDataType() ; }
void VeriPrototypeId::ClearMatchedDataType(const VeriDataType *data_type) { if (_actual_id) _actual_id->ClearMatchedDataType(data_type) ; }

// Backward compatibility routine :
// Return 1 if this identifier is declared with a (packed) vector range.
unsigned VeriVariable::IsArray() const { return (_data_type && _data_type->GetDimensions()) ? 1 : 0 ; }
unsigned VeriParamId::IsArray() const
{
    unsigned is_array = (_data_type && _data_type->GetDimensions()) ? 1 : 0 ; ;
    if (!is_array) {
        VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
        // Check type from initial value
        VeriTypeInfo *param_type = param_id->CreateType(0) ;
        if (param_type && (param_type->IsPackedArrayType() || param_type->IsUnpackedArrayType())) {
            is_array = 1 ;
        }
        delete param_type ;
    }
    return is_array ;
}
unsigned VeriPrototypeId::IsArray() const { return (_actual_id) ? _actual_id->IsArray(): 0 ; }

// return 1 for a signed data type, 0 for unsigned.
unsigned VeriVariable::IsSigned() const { return (_data_type && _data_type->Sign()) ? 1 : 0 ; }
unsigned VeriParamId::IsSigned() const {
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    // Parameter is signed if it has a data type which is signed,
    VeriDataType *data_type = GetDataType() ;
    if (data_type) {
        unsigned sign = data_type->Sign() ;
        ClearIsProcessing() ;
        return sign ;
    }
    ClearIsProcessing() ;
    VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
    VeriTypeInfo *param_type = param_id->CreateType(0) ;
    unsigned is_signed = 0 ;
    if (param_type && !param_type->IsTypeParameterType() && !param_type->IsUnresolvedNameType() && param_type->IsSigned()) {
        is_signed = 1 ;
    }
    delete param_type ;
    return is_signed ;
/*

    // or, if it does not have a data type, check the initial expression :
    // FIX ME : to get a Sign() from an expression is currently only
    // supported under elaboration products.

    ClearIsProcessing() ;
    // Don't know the sign. Assume unsigned ?
    return 0 ;
*/
}
unsigned VeriPrototypeId::IsSigned() const { return (_actual_id) ? _actual_id->IsSigned(): 0 ; }
unsigned VeriLetId::IsSigned() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    unsigned sign = 0 ;
    ClearIsProcessing() ;
    return sign ;
}
unsigned VeriNamedPort::IsSigned() const
{
    if (!_port_expr) return 0 ;

    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = _port_expr->CheckType(0, NO_ENV) ;
    unsigned ret_sign = (type) ? type->IsSigned(): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return ret_sign ;
}

// VIPER #3369
// Clear routines for clearing the tree node backpointers.
void VeriModuleId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriTaskId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriFunctionId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriBlockId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriTypeId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriModportId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriOperatorId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriClockingId::ClearTreeNodeBckPtr() { _tree = 0 ; }
void VeriProductionId::ClearTreeNodeBckPtr() { _prod = 0 ; }
void VeriLetId::ClearTreeNodeBckPtr() {  _tree = 0 ; }
void VeriExternForkjoinTaskId::ClearTreeNodeBckPtr()
{
    unsigned i ;
    VeriModuleItem *tree ;
    FOREACH_ARRAY_ITEM(_trees, i, tree) {
        if (tree) tree->ClearId() ;
        // Do not miss back pointer in other body's scope
        VeriScope *body_scope = (tree) ? tree->GetScope(): 0 ;
        if (body_scope) body_scope->SetOwner(0) ;
    }
    delete _trees ; _trees = 0 ;
    VeriTaskId::ClearTreeNodeBckPtr() ;
}
void VeriSeqPropertyFormal::ClearTreeNodeBckPtr() { _actual = 0 ; }
void VeriPrototypeId::ClearTreeNodeBckPtr() { _actual_id = 0 ; }
void
VeriIdDef::ClearAllBckPtrs()
{
    ClearDataType() ;
    ClearTreeNodeBckPtr() ;
}
/*********************************************************************************/
/* Collecting fsm constants used in this module */
/*********************************************************************************/

void
VeriVariable::AddFsmStateConstant(VeriExpression *state_expression)
{
    if (!state_expression) return ;

#ifdef VERILOG_CREATE_ONEHOT_FSMS
    // Accept or deny a (constant) state expression into this variable.

    // Add the state constant expression to the state table of this variable
    // (if it is a valid state constant (without x'es).

    // Create the state value image (the value of the state constant)
    long int integer_image = 0 ;

#if 0
    // Since we store the state value as a 'long int' into the hash table,
    // we cannot handle constants that are larger than an integer :
    // This check moved to elaboration, since 'Size()' is not defined in analysis.
    if (state_expression->Size(0) > ((unsigned)sizeof(long int)*8)) {
        SetCannotBeOnehot() ; // Make sure we don't create a FSM for the variable.
        return ;
    }
#endif
    // FIX ME : We will store incorrect integer values/constants for >32 bit FSMs.
    // Elaboration has to discard fsms beyond 32 bits.

    // Get the other side identifier (if there)
    VeriIdDef *param = state_expression->FullId() ;
    VeriExpression *param_init_val = (param && param->IsParam()) ? param->GetInitialValue() : 0 ;
    if (param_init_val && param_init_val->IsConst()) {
        // A parameter constant.
        // Use the initial value as the image of the state constant.
        if (param_init_val->HasXZ()) return ; // accept variable as state variable, but don't enter constant in table.
        // If it is a parameter, use the initial value of the parameter as image.
        integer_image = param_init_val->Integer() ;
    } else if (state_expression->IsConst()) {
        if (state_expression->HasXZ()) return ; // accept variable as state variable, but don't enter constant in table.
        // for normal constant : use the image of the constant :
        integer_image = state_expression->Integer() ;
    } else {
        // something bad is wrong. Discard this variable as onehot :
        SetCannotBeOnehot() ;
        return ;
    }

    // find the table of fsm constants.
    // It could be anywhere on the link_var loop.
    Map *constants = FsmStateConstants() ;
    if (!constants) {
        // It is nowhere on the loop. First time that we add a state constant.
        // So create it now, here.
        // The fsm constants is a "long int" -> VeriExpression* table.
        _fsm_constants = new Map(STRING_HASH,16) ;
        constants = _fsm_constants ;
    }

    // Insert the constant value integer-image
    // and the tree-node that it came from :
    // VIPER #7995: Instead of storing 'state_expression' store name of
    // parameter if state_expression is identifier reference
    // Store key in char* form
    char *int_str_image = Strings::itoa(integer_image) ;
    if (!constants->GetItem(int_str_image)) {
        (void) constants->Insert(int_str_image, (void*)(param ? Strings::save(param->Name()): 0)) ;
    } else {
        Strings::free(int_str_image) ;
    }
#endif // #ifdef VERILOG_CREATE_ONEHOT_FSMS
}

Map *
VeriVariable::FsmStateConstants() const
{
    // Find the variable which has the state constants.
    // This could be anywhere in the link loop.
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    if (_fsm_constants) return _fsm_constants ;
    VeriVariable *runner = _link_var ;
    while (runner && runner!=this) {
        if (runner->_fsm_constants) return runner->_fsm_constants ;
        runner = runner->_link_var ;
    }
    // fsm constants not found.
#endif
    return 0 ;
}

/*********************************************************************************/
/* Resolve */
/*********************************************************************************/

void VeriIdDef::Resolve(VeriScope * /*scope*/, veri_environment /*environment*/)
{
    // Resolve a declared identifier.
    // If it has no initial value assignment, there is nothing to resolve
}
void VeriVariable::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (IsMemory()) {
        // Parameter declaration is illegal in UDP
        VeriIdDef *owner = (scope) ? scope->GetOwner() : 0 ;
        if (owner && owner->IsUdp()) Error("%s cannot be declared within UDP %s", "memory", owner->Name()) ;

        // VIPER #4656: Check for memry type port in non-SV mode. No need to check
        // the data type for unpacked dimensions, typedef is only allowed in SV mode:
        // Viper 5563. Memory type port is allowed in AMS LRM 2.3 Section 4.7 syntax 4-4
        if (!IsSystemVeri() && !IsAms() && IsPort()) Warning("port %s must not be declared to be an array", Name()) ;
    }
    // Resolve a declared variable.

    // Before anything, do a quick self-loop check. "foo foo;" would create a self loop :
    if (_data_type && _data_type->GetId()==this) {
        Error("%s is not a type", Name()) ; // or something like that..
        _data_type = 0 ; // break the loop.
    }

    // First of all, make sure we have a 'type' field set for this guy.
    // If not, assign the default type which is appropriate for this style of variable.
    // For that, we need to know where this identifier is declared (who owns it) :
    // FIX ME: Alternatively, we could flag Variable with a bit-flag if they are in a module or a subprogram.
    VeriIdDef *owner = (scope) ? scope->GetOwner() : 0 ;
    // VIPER #5949: set flag for /class/udp/checker/function/task/property/sequence/letconstruct
    unsigned func_task_seq = owner ? owner->IsFunction() || owner->IsTask() || owner->IsProperty() || owner->IsSequence() || owner->IsLetId() : 0 ;
    unsigned is_class = owner ? owner->IsClass() : 0 ;
    unsigned is_udp = owner ? owner->IsUdp() : 0 ;
    unsigned is_checker = owner ? owner->IsChecker() : 0 ;

    // VIPER #5949: Set the kind fild of the id.
    // Setting of _type fields will not always be done at construction time,
    // (e.g. for named types) since data type is not yet resolved there.
    // So do it (setting _type) here, now that data_type should be fully resolved.
    // Only set the type if the data type is resolved. Otherwise, leave _type unchanged (might contain default type).
    // unsigned type = (_data_type) ? _data_type->Type() : 0 ;
    // if (type) _type = type ;

    // We set the _type field at the construction time. But do not set the _kind field there
    // unless the data_type is net_data_type. So we set the _kind field here checking the
    // port_type for port variable and also for non_port variable.
    if (_data_type) {
        unsigned type = 0 ;
        unsigned default_type = 0 ;
        if (IsVeri95() || IsVeri2001()) {
            default_type = VERI_REG ;
        } else {
            default_type = VERI_LOGIC ;
        }

        unsigned kind = 0 ;
        // Find the default_kind:
        // VIPER #7204: Set default_kind to the default_nettype:
        unsigned default_kind = veri_file::GetDefaultNettype() ;
        unsigned direction = _dir ;

        // Set _kind for port variable:
        if (IsPort()) {
            // Set _type of the data_type to the
            // _kind field of interface ports:
            if (IsInterfacePort()) {
                kind = _data_type->Type() ;
            // For fun/task/seq/propery/let set _type to the
            // type of the data_type and _kind to VERI_REG:
            } else if (func_task_seq) {
                type = _data_type->Type() ;
                kind = VERI_REG ;
            } else {
                // For class, udp and check set bot _type and _kind
                // to the type of data_type:
                if (is_class || is_udp || is_checker) {
                    kind = _data_type->Type() ;
                    type = _data_type->Type() ;
                } else {
                    // VIPER #6549: If _type is not set already, set that now. _type field may not
                    // be set from VeriDataDecl constructor if data type is type reference
                    if (!_type) type = _data_type->Type() ;
                }
                // If the data_type is net data_type then
                // set _kind with the net type:
                if (_data_type->IsNetDataType()) {
                    kind = _data_type->Type() ;
                } else { // For non-net (ie. wire logic)data_type
                    // If the data_type is non-net type ie reg/logic
                    if (!_data_type->IsNetType()) {
                        if (IsVeri95() || IsVeri2001() || IsAms()) { // VIPER #7309: Also check for ams: if in ams mode
                                                                     // populate kind field with the type of the date type
                            if (_data_type->Type()) { // type exists
                                kind = _data_type->Type() ; // set _kind with type of data_type
                            } else { // type does not exists
                                kind = default_kind ; // set _kind with default_kind
                            }
                        } else { // For systemverilog and forth
                            if (!_kind) {
                                switch (direction) {
                                case 1: { // According to LRM-2009 default kind of input port
                                        // should be default_net_type but for backward compatibility
                                        // and as ModelSim does we are treating input ports as register
                                        // type or logic type not as net type.
                                        // kind = VERI_REG ; break ;
                                        // VIPER #8195: Set kind checking data type: If it is 4-state the kind will be
                                        // set to default kind otherwise it will be set to variable. It is not acrroding
                                        // to lrm, but for compatibility with ModelSim we set kind filed checking data type.
                                        // kind = default_kind ;
                                        VeriTypeInfo *data_type_info = _data_type->CreateType(0) ;
                                        if (data_type_info && !data_type_info->IsTypeParameterType() && !data_type_info->IsUnresolvedNameType()) {
                                            if (data_type_info->Is4State() && data_type_info->Type() != VERI_REG) {
                                                kind = default_kind ;
                                            } else {
                                                kind = VERI_VAR ;
                                            }
                                        }
                                        delete data_type_info ;
                                        break ;
                                        }
                                case 2: if (IsVeri95() || IsVeri2001()) {
                                            kind = VERI_REG ;
                                        } else {
                                            kind = VERI_VAR ;
                                        }
                                        break ;
                                case 3: kind = default_kind ; break ;
                                case 4: kind = VERI_VAR ; break ;
                                default: break ;
                                }
                            }
                        }
                    } else { // For net data_type(ie. wire/wand etc)
                        kind = _data_type->Type() ;
                    }
                }
            }
        } else { // For non-port variables:
            // If the data_type is net data_type then
            // set _kind with the net type and _type
            // with the type of the base data_type:
            if (_data_type->IsNetDataType()) {
                kind = _data_type->NetType() ;
                VeriDataType *d_type = _data_type->GetDataType() ;
                type = d_type ? d_type->Type() : 0 ;
            } else {
                // For class and udp set _type
                // to the type of data_type:
                if (is_class || is_udp) {
                    type = _data_type->Type() ;
                // If the data_type is non-net type ie reg/logic
                } else if (!_data_type->IsNetType()) {
                    // VIPER #7250: In system verilog mode populate _kind
                    // field with VERI_VAR, in other mode populate it with
                    // the type of its _data_type
                    if (IsVeri95() || IsVeri2001() || IsAms()) { // VIPER #7309: Also check for ams: if in ams mode
                                                                 // populate kind field with the type of the date type
                        kind = _data_type->Type() ;
                    } else {
                        kind = VERI_VAR ; // set _kind with default_kind
                    }
                    type = _data_type->Type() ;
                } else { // If the data_type is net type ie wire/wand
                    kind = _data_type->Type() ;
                    if (IsVeri95() || IsVeri2001()) {
                        type = _data_type->Type() ;
                    } else {
                        type = default_type ;
                    }
                }
            }
        }

        if (kind) _kind = kind ; // Set _kind
        if (type) _type = type ; // Set _type
    }

    // For named_port variables set the _type field from the type of its expression:
    if (!_data_type && IsNamedPort()) {
        VeriTypeInfo *port_type = CreateType(0) ;
        unsigned type = (port_type) ? port_type->Type() : 0 ;
        if (type) _type = type ;
        delete port_type ;
    }

    // Default type setting for implicit (port) nets :
    // If no type is found, check if this is a port, and give that the LRM specified default type :
    // NOTE: SV interface ports should NOT get the default type (wire). Because they are not nets.
    if (IsPort() && !_type && !IsInterfacePort()) {
        // Declared port, which was not declared later by a net or reg.
        // Any way, it does not have a default data type.
        //
        // Default I/O type LRM rules are not clear, but I think
        // that all module ports are by default 'wire' and
        // all task ports are by default 'reg'.

        // RD: 4/2006: All ansi port decl ids already get default initialized (with SetType())
        // in the ResolveAnsiPortDecl() routine. Specific rules apply there.
        // Same thing for function id's.
        // If we extend SetType() to VeriDataDecl(), then we would not need to do this work here.
        // However, in VeriDataDecl we also do not know if we are in a module or in a subprogram.
        // So, keep this code here (using the 'hack' of looking at the owner of the scope) until we
        // have context info properly taken care of.
        if (owner && (owner->IsModule())) { // IsModule returns 'true' for all VeriModule derivatives (interfaces, programs, packages, udps etc).
            _kind = VERI_WIRE ; // VIPER #5949
            _type = VERI_WIRE ; // Actually, we should use DefaultNetType ??
        } else {
            _kind = VERI_REG ; // VIPER #5949
            _type = VERI_REG ;
        }
    }

    if (owner && IsAnsiPort()) {
        // VIPER #2949: Semantic check on initial value of ANSI ports for module/task/functions:
        // For modules: only reg type output port are allowed to have initial value
        // For task/functions: Output ports are not allowed to have initial value
        // VIPER #7103: Looks like VCS changed the behaviour, they now always keep the initial
        // value of module ANSI ports irrespective of input/output or reg/wire. So, change ours:
        // VIPER #7688: In SV-2009 mode, initial value is allowed for output port
        if (/*(owner->IsModule() && _initial_value && (!IsOutput() || !IsReg())) ||*/
            (!IsSystemVeri2009OrLater() && (owner->IsTask() || owner->IsFunction()) && _initial_value && IsOutput())) {
            Warning("illegal initial value of %s port %s for %s %s ignored", PrintToken(Dir()), Name(),
                               (owner->IsModule() ? "module" : "task/function"), owner->Name()) ;
            // Clear out the initial value so that we don't output a wrong design:
            delete _initial_value ;
            _initial_value = 0 ;
        }
    }
    // VIPER #4814 : Produce warning if initial value is specified in non-ansi
    // ports of function/task
    if (owner && (owner->IsFunction() || owner->IsTask()) && IsPort() && !IsAnsiPort() && _initial_value) {
        Warning("illegal initial value of %s port %s for %s %s ignored", PrintToken(Dir()), Name(), "task/function", owner->Name()) ;
        // Clear out the initial value so that we don't output a wrong design:
        delete _initial_value ;
        _initial_value = 0 ;
    }

    // AMS: If type is not set for a variable of a descipline, set it to wire type here.
    VeriIdDef *data_type_id = _data_type ? _data_type->GetId() : 0 ;
    // VIPER #4871: If virtual interface type object is declared via type reference
    // then if type is of virtual interface, mark this variable as virtual interface type
    if (_data_type && data_type_id && data_type_id->IsVirtualInterface()) _data_type->SetIsVirtualInterface() ;

    if (IsPort() && owner && owner->IsModule()) {
        // Check if this (module) port is defined in the module port expression list (issue 1223).
        // Using circumstantial evidence here based on how we flag defined port expressions (in Resolve()) :
        // We already ran Resolve on the module port expressions, where 'assign' and 'use' flags were set on ids in the expression.
        // So, IF the port is not assigned (or used) then it did not occur on the module port expression list.
        // FIX ME : We should actually label 'defined' ports with a new bit-flag in VeriVariable.
        if ((IsInput() && !IsAssigned()) ||
            (IsOutput() && !IsUsed()) ||
            (IsInout() && (!IsAssigned() || !IsUsed()))) {
            Error("port %s is not defined",Name()) ; // VIPER 2612 : upgraded to error
        }

        if (_type == VERI_NONE) Error("net type must be explicitly specified for '%s' when default_nettype is none", Name()) ; // VIPER Issue #7126

        // Somewhat strange error that both VCS and ModelSim issue on module ports :
        // In Verilog 2000 and earlier, 'reg' or other variable type cannot be inout or input
        // In System Verilog and earlier, 'reg' or other variable cannot be inout.
        // Cannot find this in LRM, but simulators rule...
        // CHECK ME : This message is not issued for dynamic array's. Not clear why, ...
        // VIPER #5710: (vcs_compatibility: block_after_checker/endchecker_with_name): Do not error out for checker
        is_checker = 0 ;
        if (owner->IsChecker()) is_checker = 1 ;
        if (!IsNet() && !IsOutput() && !IsInterfacePort() && !is_checker) {
            if (IsInout() || (!IsSystemVeri() && IsInput())) {
                if (!InRelaxedCheckingMode() || (Kind() != VERI_NONE)) {
                    // VIPER #5895: Do produce the error if the type is set (not from "`default_nettype none"):
                    // VIPER #5827: Only produce the error if we are not in relaxed checking mode:
                    Error("non-net port %s cannot be of mode %s",Name(),PrintToken(Dir()) ) ;
                }
            }
        }
        // Port of a module cannot be of virtual interface type
        if (IsVirtualInterface()) {
            Error("virtual interface %s cannot be used as port", Name()) ;
        }
    }

    // Now, since (95-style) function/task ports, as well as ANSI ports don't have a port expression,
    // set used/assigned flags here, depending on the port direction.
    switch (Dir()) {
    case VERI_INPUT  : SetAssigned() ; break ;
    case VERI_OUTPUT : SetUsed() ; break ;
    case VERI_INOUT  : SetUsed() ; SetAssigned() ; break ;
    default : break ;
    }
    VeriTypeInfo *port_type = 0 ;
    // Chandles cannot be used as ports
    // VIPER #4095 : Ports of function/task can be of chandle type
    if (IsPort() && (owner && !owner->IsFunction() && !owner->IsTask())) { // Port identifier
        port_type = CreateType(0) ;
        if (port_type && port_type->IsChandleType()) {
            Error("illegal reference to chandle type") ;
        }
    }
    if (owner && owner->IsUdp()) {
        // VIPER #3290 : In Udp output port can be of type reg/default_net_type.
        // Input ports can be only of nettype. Beside ports only reg can be declared
        if (!port_type) port_type = CreateType(0) ;
        if (port_type && (port_type->IsTypeParameterType() || port_type->IsUnresolvedNameType())) {
            // Do not check anything
        } else if (port_type && (Dir() == VERI_OUTPUT) && ((!port_type->IsRegType() && !port_type->IsNetType()) || !port_type->IsBuiltInType())) {
            //Error("port %s must be a single bit %s", Name(), "reg/net") ;
            Error("illegal data type for udp port %s", Name()) ; // VIPER #4137 : Change message produced for illegal data type of udp port
        } else if (port_type && (Dir() == VERI_INPUT) && (!port_type->IsNetType() || !port_type->IsBuiltInType())) {
            Error("illegal data type for udp port %s", Name()) ;
        } else if (port_type && !IsPort() && (!port_type->IsRegType() || !port_type->IsBuiltInType())) {
            //Error("variable %s must be a single bit reg", Name()) ;
            Error("illegal data type for udp register %s", Name()) ;
        }
    }

    // If there is an initial assignment, this identifier is certainly assigned :
    if (_initial_value) SetAssigned() ;

    if (_initial_value) {
        data_type_id = 0 ;
        if (_initial_value->IsNewExpr()) {
            // Viper 6994 : Check for virtual class constructor call
            data_type_id = _data_type ? _data_type->GetId() : 0 ;
            VeriModuleItem *class_decl = data_type_id ? data_type_id->GetModuleItem() : 0 ;
            while (class_decl && class_decl->IsTypeAlias()) {
                VeriDataType *data_type = class_decl->GetDataType() ;
                data_type_id = data_type ? data_type->GetId() : 0 ;
                class_decl = data_type_id ? data_type_id->GetModuleItem() : 0 ;
                if (class_decl && class_decl->IsClass()) break ;
            }
            // Expression size in abstract class constructor is meant to initialize a dynamic array.
            // Do not issue error for copy constructor Viper 5642 has the example.
            if (class_decl && data_type_id && class_decl->GetQualifier(VERI_VIRTUAL) && !_initial_value->GetSizeExpr()
                                           && !_initial_value->IsCopyConstructor()) {
                Error("cannot instantiate object of virtual class %s", data_type_id->GetName()) ;
            }
        }
        veri_environment init_env = VERI_EXPRESSION ;
        // Initial value of task/function port must be a constant expression
        // VIPER #3226 : Initial value of task/function port can be any expression
        // FIXME : Initial value for task/func port is not allowed in 95/2001 mode. Issue error for that
        //if (IsPort() && owner && (owner->IsFunction() || owner->IsTask())) init_env = VERI_CONSTANT_EXPRESSION ;
        // Initial value of a sequence/property port can be an event expression
        if (IsPort() && owner && (owner->IsSequence() || owner->IsProperty())) init_env = VERI_SEQ_PROP_INIT ; // VIPER 2180 : Initial value of formal id of sequence is event expression

        // VIPER #3786 : Initial value of virtual interface can be instance identifier : So resolve
        // initial value with the same environment used to resolve actual of instantiation.
        if (IsVirtualInterface()) init_env = VERI_PORT_CONNECTION ;

        // VIPER #4981 : In clocking declaration, initial expression follows port
        // connection rules. So initial expression for inout/output port should be lvalue
        if (owner && owner->IsClocking() && (IsInout() || IsOutput())) {
            init_env = VERI_NET_LVALUE ;
        }
        // VIPER #2839 : Initial value of register should be constant in verilog95 and 2001 mode
        // VIPER #5830 : Initial value of register should be constant in ams mode too.
        if (IsReg() && (VeriNode::IsVeri95() || VeriNode::IsVeri2001() || VeriNode::IsAms())) init_env = VERI_CONSTANT_EXPRESSION ;
        // VIPER 4490 : the actual passed to inout port of a func must be lvalue
        // Viper 5122 : Commented out this line. Call this from func/task call's resolve rather
        // than from func/task decl's resolve.
        //if (owner && ((owner->IsTask() || owner->IsFunction()) && IsInout()))  init_env = VERI_REG_LVALUE ;
        if (!port_type) port_type = CreateType(0) ;
        // VIPER #4561 : If instance of a class is declared as constant and its initial value is new
        // expr, arguments of new expression will be constant.
        if (port_type && port_type->IsClassType() && IsConstant() && _initial_value->IsNewExpr()) init_env = VERI_CONSTANT_EXPRESSION ;

        // VIPER #4624 : If this is string type variable pass VERI_STRING_TARGET
        // as environment to resolve initial expression. Initial expression can
        // have multi-concat operator with non-constant replicator operator.
        if (port_type && port_type->IsString()) init_env = VERI_STRING_TARGET ;

        // Viper 6980 : LRM 1800-2009 Section 7.2.2 Assigning to structures
        // Members of a structure data type can be assigned individual default member
        // values by using an initial assignment with the declaration of each member. The
        // assigned expression shall be a constant expression.
        if (scope && scope->IsStructUnionScope())  init_env = VERI_CONSTANT_EXPRESSION ;

        unsigned error_count1 = Message::ErrorCount() ;
        _initial_value->Resolve(scope, init_env) ;
        unsigned error_count2 = Message::ErrorCount() ;

        // Check the type of initial value against this identifier
        VeriIdRef id_ref(this) ;
        id_ref.SetLinefile(Linefile()) ;
        id_ref.CheckAssignmentContext(_initial_value, (IsSequenceFormal() | IsPropertyFormal()) ? PROP_SEQ_INIT_VAL: INIT_VAL) ;
        // Viper 5101: For new function call we never used to call VeriFunctionCall::Resolve rather called the VeriNew::Resolve
        // Called a VeriFunctionCall::Resolve separately to catch normal function call errors.
        if (_initial_value->IsNewExpr() && error_count2 == error_count1) {
            VeriModuleItem *class_item = 0 ;
            if (data_type_id && data_type_id->IsClass()) class_item = data_type_id->GetModuleItem() ;
            VeriIdDef *new_func_id = class_item ? class_item->GetNewFunctionId() : 0 ;
            _initial_value->ResolveNewCall(new_func_id) ;
        }
    }
    delete port_type ;

    // Check if this variable can be one-hot encoded, for FSM extraction :

    // declared variables range could be 'onehot' encoded (FSM), if its an array,
    // and it is a 'variable' (assignments will be (conditional) from with a always block).
    if (IsReg() && PackedDimension()==1 && UnPackedDimension()==0 && !IsPort()) {
        // A reg which is is a 1? dimensional array of 1-bit values.
        // Could be a FSM variable :
        SetCanBeOnehot() ;
    }
    // System Verilog : variable of an enum type can be encoded onehot..
    if (IsReg() && (_data_type && _data_type->IsEnumType()) && PackedDimension()==0 && UnPackedDimension()==0 && !IsPort()) {
        // A variable of some enum type.
        // Assume that this can always be a fsm variable
        SetCanBeOnehot() ;
    }
    if (IsMemory()) {
        // Resolve the unpacked dimensions. All dimensions must be constant ranges :
        // Pass environment VERI_BINS_INIT_VALUE for BinsId to resolve them differently. Viper 5871
        // Viper #8268, test2.sv: Resolve with VERI_TYPE_ENV
        if (_dimensions) _dimensions->Resolve(scope, IsBinsId() ? VERI_BINS_INIT_VALUE : VERI_TYPE_ENV) ;

        if (_dimensions && !IsSystemVeri()) {
            // VIPER #3403: Check whether it contains SV specific [n] type single value range in non-SV mode:
            if (_dimensions->IsSingleValueRange()) {
                // Can't check it in the constructor for late parse tree bindings:
                _dimensions->Error("single value range is not allowed in this mode of verilog") ;
            } else if (_dimensions->IsDynamicRange()) {
                // VIPER #6153: Check whether it contains SV specific [] type dynamic range in non-SV mode:
                // FIXME: Check next dimensions as well
                _dimensions->Error("dynamic range is not allowed in this mode of verilog") ;
            }
        }
    }

    ResolveAttributes(scope) ;
}
void VeriParamId::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Parameter declaration is illegal in UDP
    VeriIdDef *owner = (scope) ? scope->GetOwner() : 0 ;
    if (owner && owner->IsUdp()) Error("%s cannot be declared within UDP %s", "parameter/localparam", owner->Name()) ;

    // Before anything, do a quick self-loop check. "parameter foo foo;" would create a self loop :
    if (_data_type && _data_type->GetId()==this) {
        Error("%s is not a type", Name()) ; // or something like that..
        _data_type = 0 ; // break the loop.
    }

    // Setting of _type fields will not always be done at construction time,
    // (e.g. named types) since data type is not yet resolved there.
    // So do it (setting _type) here, now that data_type should be fully resolved.
    // Only set the type if the data type is resolved. Otherwise, leave _type unchanged (might contain default type).
    unsigned type = (_data_type) ? _data_type->Type() : 0 ;
    if (type) _type = type ;

    // Resolve the unpacked dimensions. All dimensions must be constant ranges :
    // Viper #8268, test2.sv: Resolve with VERI_TYPE_ENV
    if (_dimensions) _dimensions->Resolve(scope, VERI_TYPE_ENV) ;

    if (_dimensions && !IsSystemVeri()) {
        if (_dimensions->IsSingleValueRange()) {
            // VIPER #3403: Check whether it contains SV specific [n] type single value range in non-SV mode:
            // Can't check it in the constructor for late parse tree bindings:
            _dimensions->Error("single value range is not allowed in this mode of verilog") ;
        } else if (_dimensions->IsDynamicRange()) {
            // VIPER #6153: Check whether it contains SV specific [] type dynamic range in non-SV mode:
            // FIXME: Check next dimensions as well
            _dimensions->Error("dynamic range is not allowed in this mode of verilog") ;
        }
    }

    // Resolve a declared parameter : receives a constant expression :
    if (_initial_value) {
        // Expect a type-expression if this is a (System Verilog) Type parameter :
        if (IsType()) {
            _initial_value->Resolve(scope, VERI_TYPE_ENV) ;
        } else {
            _initial_value->Resolve(scope, ParamType() == VERI_PARAMETER ? VERI_PARAM_INITIAL_EXP : VERI_CONSTANT_EXPRESSION) ;
            // Check the type of initial value against this identifier
            if (_data_type && ParamType()) {
                // VIPER #8293 : According to P1800-2012 section 10.8, initial value
                // of enum literal in enum declaration is not an assignment like context
                // So do not check that.
                VeriIdRef id_ref(this) ;
                id_ref.SetLinefile(Linefile()) ;
                id_ref.CheckAssignmentContext(_initial_value, PARAM_INIT) ;
            } else { // Type is derived from initial value
                VeriTypeInfo *init_type = CreateType(0) ;
                delete init_type ;
            }
        }
    }

    // Don't register the parameter up-front as a FSM state constant.
    // We don't know if it will be used at all in a FSM (happens in denc example),
    // or for which state variable it will be used.
    // The Resolve routine (LinkOnehot()) is a better place to add the parameters,

    // If there is an initial assignment, this identifier is certainly assigned :
    if (_initial_value) SetAssigned() ;

#ifdef VERILOG_PATHPULSE_PORTS
    if (_path_pulse_ports) _path_pulse_ports->Resolve(scope, VERI_PORT_EXPRESSION) ;
#endif
    ResolveAttributes(scope) ;
}
void VeriGenVarId::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Parameter declaration is illegal in UDP
    VeriIdDef *owner = (scope) ? scope->GetOwner() : 0 ;
    if (owner && owner->IsUdp()) Error("%s cannot be declared within UDP %s", "genver", owner->Name()) ;

    // Resolve a declared parameter : receives a constant expression :
    if (_initial_value) _initial_value->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    ResolveAttributes(scope) ;
}

void VeriInstId::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the unpacked dimensions. All dimensions must be constant ranges :
    // Viper #8268, test2.sv: Resolve with VERI_TYPE_ENV
    if (_dimensions) _dimensions->Resolve(scope, VERI_TYPE_ENV) ;

    // Check the port connections..
    unsigned i ;
    VeriExpression *pc ;
    FOREACH_ARRAY_ITEM(&_port_connects, i, pc) {
        if (!pc) continue ;

        // Check if this is a module instantiation :
        if (_the_instance) {
            // Verilog module instantiation.
            // We don't know the instantiated module yet, so resolve with VERI_PORT_CONNECTION :
            pc->Resolve(scope, VERI_PORT_CONNECTION) ;
        } else {
            // Verilog gate instantiation : The first one is always the output..
            // We should resolve that with 'VERI_NET_LVALUE' (issue 2037) :
            // Others are always inputs ? (resolve with 'VERI_EXPRESSION') :
            if (i==0) {
                pc->Resolve(scope, VERI_NET_LVALUE) ;
            } else {
                pc->Resolve(scope, VERI_EXPRESSION) ;
            }
        }
    }

    if (_dimensions && !IsSystemVeri()) {
        if (_dimensions->IsSingleValueRange()) {
            // VIPER #3403: Check whether it contains SV specific [n] type single value range in non-SV mode:
            // Can't check it in the constructor for late parse tree bindings:
            _dimensions->Error("single value range is not allowed in this mode of verilog") ;
        } else if (_dimensions->IsDynamicRange()) {
            // VIPER #6153: Check whether it contains SV specific [] type dynamic range in non-SV mode:
            // FIXME: Check next dimensions as well
            _dimensions->Error("dynamic range is not allowed in this mode of verilog") ;
        } else if (_dimensions->GetNext()) {
            // VIPER #7679: Error out in non-SV mode only since SV now supports multiple dimensions here:
            _dimensions->Error("multiple dimensions in %s instantiation is not allowed in this mode of Verilog", "module") ;
        }
    }
    ResolveAttributes(scope) ;
}

void VeriNamedPort::Resolve(VeriScope *scope, veri_environment environment)
{
    // Call base class's resolve
    //VeriVariable::Resolve(scope, environment) ;

    // Resolve port expression:
    if (_port_expr) {
        // VIPER #4246: Resolve the port expression by passing appropriate environment for its direction:
        switch(Dir()) {
        case VERI_OUTPUT:
        case VERI_INOUT:
            // Output/Inout port expression must be a valid lvalue:
            _port_expr->Resolve(scope, VERI_NET_LVALUE_UNDECL_USE_GOOD) ;
            break ;
        default:
            _port_expr->Resolve(scope, VERI_EXPRESSION_UNDECL_USE_GOOD) ;
            break ;
        }
    }

    // VIPER #7607: Create and set the memory range here:
    // VIPER #7802: Create only unpacked dimension, since this is an identifier:
    if (!_dimensions) _dimensions = CreateDimensions(1 /* only unpacked */) ;

    // VIPER #5949: Call base class's resolve
    VeriVariable::Resolve(scope, environment) ;
}
//VIPER #3384
void VeriCovgOptionId::Resolve(VeriScope *scope, veri_environment environment)
{
    // Call base class's resolve
    VeriIdDef::Resolve(scope, environment) ;
    ResolveAttributes(scope) ;
}
void VeriPrototypeId::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_actual_name) _actual_name->Resolve(scope, environment) ;
}
/*********************************************************************************/
/* Check if an instance has ordered and/or named associations */
/*********************************************************************************/

unsigned VeriInstId::HasOrderedPortAssoc() const
{
    VeriExpression *pc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(&_port_connects, i, pc) {
        if (!pc) return 1 ; // empty ordered assoc
        if (!pc->NamedFormal()) return 1 ;
    }
    return 0 ;
}

// VIPER #5349: Check if this instance has ordered and/or named port associations.
unsigned VeriPrototypeId::HasOrderedPortAssoc() const { return (_actual_id) ? _actual_id->HasOrderedPortAssoc() : 0 ; }

unsigned VeriInstId::HasNamedPortAssoc() const
{
    VeriExpression *pc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(&_port_connects, i, pc) {
        if (!pc) continue ; // ordered assoc
        if (pc->NamedFormal()) return 1 ;
    }
    return 0 ;
}

// VIPER #5349: Check if this instance has ordered and/or named port associations.
unsigned VeriPrototypeId::HasNamedPortAssoc() const { return (_actual_id) ? _actual_id->HasNamedPortAssoc() : 0 ; }

/*********************************************************************************/
/* During resolving, check the environment in which this identifier is used */
/*********************************************************************************/

void
VeriIdDef::CheckEnvironment(veri_environment environment, const VeriTreeNode *from, unsigned is_defined_in_interface)
{
    if (!from) from = this ; // just to make sure we have a proper line/file info to report to
    if (IsUnresolvedSubprog()) return ; // unresolved id

    if (environment == VERI_WAIT_EXP && IsEvent()) from->Error("event type variable %s not allowed here", Name()) ;
    // Now do environment checks :
    switch (environment) {
    case VERI_PROCE_CONT_ASSIGN_VAL : // VIPER #6110: value for RHS of procedural continuous assignments
        // Error out if automatic variable is used in RHS of any procedural continuous assignments:
        if (IsAutomatic()) {
            from->Error("automatic variable %s is not allowed in procedural continuous assignments", Name()) ;
        }
    case VERI_EXPRESSION :
    case VERI_EXPRESSION_UNDECL_USE_GOOD :
    case VERI_ABORT_EXPRESSION :
    case VERI_LET_EXPRESSION :
    case VERI_WAIT_EXP :
    case VERI_STRING_TARGET : // Added for VIPER #4259
    case VERI_CONSTRAINT_EXPR : // Constraint expression behaves as expression
    case VERI_INLINE_CONSTRAINT_EXPR :
        // usage of id in a regular (RHS) expression
        // Viper 5498 added the IsCovergroupOption
        // VIPER #5591: Added IsType() check. Type ids cannot be used as expression:
        if (IsType() || (!IsVar() && !IsParam() && !IsEvent() && !IsGenVar() &&
                         !IsCovergroup() && !IsProduction() && !IsCovergroupOption() && !IsLetId())) { // VIPER 2621.
            // Allow implicit variables in production as per SV LRM IEEE Std 1800 sec 13.16.7.
                from->Error("%s is not valid in an expression", Name()) ;
        }

        // Here, id is used.
        if (!is_defined_in_interface) SetUsed() ;
        break ;
    case VERI_LVALUE :
    case VERI_REG_LVALUE :
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
        // Assignment of id in a regular (LHS) expression
        if (!IsVar()) {
            // Covergroup can be used as lvalue because of its implicit variable declaration (like function)
            // Temporary : genvar allowed in a blocking assignment (needed for generate-loop assignments in new structure).
            // FIX ME : cannot differentiate between blocking assignment in generate loop and blocking assignment elsewhere.
            // Checks for coverpoint and coveroption are added for VIPER# 4244
            VeriModuleItem *module_item = GetModuleItem() ;
            if (!IsGenVar() && !IsImplicitLocalParam() && !IsCovergroup() && !IsCovergroupOption()
            && !(module_item && (module_item->GetType() == VERI_COVERPOINT))
            )
             from->Error("cannot assign to non-variable %s", Name()) ;
        }

        if ((environment==VERI_REG_LVALUE) || (environment == VERI_CONSTRUCTOR_LVALUE)) {
            if (!IsReg()) {
                // Temporary : genvar allowed in a blocking assignment for new structure
                // FIX ME : cannot differentiate between blocking assignment in generate loop and blocking assignment in sequential area.
                // Covergroup can be used as lvalue because of its implicit variable declaration (like function)
                // In ams genvar is allowed as lvalue
                // Virtual interface object can be used as lvalue
                // VIPER# 4244: If the "type_option" and "option" are used outside "covergroup" is valid.
                // The check for CovergroupOption is added for it.
                // P1800 System Verilog LRM, Ref. Sec-18.6
                VeriModuleItem *module_item = GetModuleItem() ;
                if (!IsCovergroup() && !(IsAms() && IsGenVar()) && !IsLocalVar() && !IsCovergroupOption() && !IsVirtualInterface()
                && !(module_item && (module_item->GetType() == VERI_COVERPOINT))
                 ) from->Error("procedural assignment to a non-register %s is not permitted", Name()) ;
            }

        // Issue 2232 and 2254 make the checking of procedural/concurrent assignments to vars very difficult.
        // For example, module instances are not yet resolved (here in Resolve(), so we would not know if an
        // actual is being assigned to or only used. That is true for all dialects of Verilog.
        // With System Verilog it gets even worse : concurrent assignment IS allowed, as long as there is only one.
        // That requires counting the number of assignments (which hints at non-re-entry), and special
        // and treatment of partial assignments (which requires a partial 'environment' info).
        //
        // So, decided to move these tests to the AnalyseFull() routine, which is run AFTER all modules are read in,
        // and thus all exact assignment info (including from module instantiations) is available.
        // ModelSim seems to be doing the same tests (this message shows up only in 'vsim').
        //
        // Also the 'dual-port-RAM' checks are moved over there (also require exact info and counting).

        // VIPER #2768: Enable this check here in analyzer/resolve but without the assignment part:
        } else if (environment==VERI_NET_LVALUE || environment==VERI_NET_LVALUE_UNDECL_USE_GOOD) {
            // Check for (multiple) assignments to a variable (non-net) from within a concurrent area.
            // Exclude System Verilog struct/union members and modport-ports, since these designate field names, not unique variables.
            if (!IsNet() && !IsMember() && !IsModportPort()) {
                //if (IsSystemVeri() && !IsAssigned()) {
                if (IsSystemVeri()) {
                    // In System Verilog, one net-assignment is allowed for regs.
                } else {
                    // In non-System-Verilog, any assignment to a variable is an error.
                    from->Error("concurrent assignment to a non-net %s is not permitted", Name()) ;
                }
            }
        } else if (environment == VERI_GENVAR_LVALUE) {
            // Only genvar is allowed here
            // VIPER #8109: Implicit localparam is allowed where genvar can be used
            if (!IsGenVar() && !IsImplicitLocalParam()) from->Error("only genvar can be assigned here") ;
        }
        // Assignment to input port of sequence/property is legal if actual for
        // that port is a local variable from another sequence/property :
        if (IsInput() && !IsLocalVar()) {
            from->Warning("assignment to input %s",Name()) ;
        }
        // VIPER #5949: Check for kind:
        //if (Type()==VERI_SUPPLY0 || Type()==VERI_SUPPLY1) {
        if (Kind()==VERI_SUPPLY0 || Kind()==VERI_SUPPLY1) {
            from->Warning("assignment to constant %s",Name()) ;
        }

        // VIPER 2959 : check assignment to constant (happens for SV only)
        if (IsConstant()) {
            if (!GetInitialValue() && (environment == VERI_CONSTRUCTOR_LVALUE)) {
                // VIPER #4039 : If we are processing assignment inside constructor
                // and constant does not have any initial value, this constant is
                // instance constant and can be assigned inside constructor :
            } else {
                from->Error("assignment to const variable %s",Name()) ;
            }
        }

        // Here, id is assigned.
        if (!is_defined_in_interface) SetAssigned() ;
        break ;
    case VERI_PATH_EXPRESSION :
    case VERI_PORT_EXPRESSION :
        // usage of id in a port expression (module level port)
        if (!IsPort()) {
            if (environment == VERI_PORT_EXPRESSION) {
                from->Error("no definition for port %s", Name()) ; // VIPER #8172
            } else {
                // Verific-vcs incompatibility VIPER 5248/5301
                from->Warning("no definition for port %s", Name()) ; // VIPER #8172
            }
        }

        // Set used/assigned based on port direction.
        switch (Dir()) {
        case VERI_INPUT  : if (!is_defined_in_interface) SetAssigned() ; break ;
        case VERI_OUTPUT : if (!is_defined_in_interface) SetUsed() ; break ;
        case VERI_INOUT  : if (!is_defined_in_interface) { SetUsed() ; SetAssigned() ; } break ;
        default : break ;
        }

        break ;
    case VERI_PORT_CONNECTION :
        // usage of id in a port connection (actual in module instantiation)

        // Cannot determine if it was used or assigned, since we
        // do not know the port direction of the formal.
        // VIPER issue 2424 and 2467 : should not assume ANY usage or assignment :
        // SetUsed() ;
        // SetAssigned() ;
        // VIPER #6904: Donot allow type identifier in port connection
        if (IsType()) from->Error("type identifier %s used in illegal context", Name()) ;

        break ;
    case VERI_GATE_INOUT_PORT_CONNECTION :
        // VIPER 2543 : usage of id in a port connection to trans gate
        if (!IsNet()) { // Only scalar net/select of vector net is allowed
            from->Error("illegal expression in target") ;
        }
        if (!is_defined_in_interface) {
            SetUsed() ; // Set id as used
            SetAssigned() ; // Set id assigned
        }
        break ;
    case VERI_PARAM_VALUE : // value of parameter
        // usage of id as parmeter value
        // In System verilog parameter value can be type identifier including
        // class name or constant, so check for IsClass() (true for class id)
        // Const functions may be called without parentheses. We should not error out.
        // VIPER 5347 : allow struct/union members, since they can be member of a constant.
        if (!IsParam() && !IsGenVar() && !IsType() && !IsClass() && !IsFunction() && !IsMember() && !IsLetId() &&
            // VIPER #3723 (#2940, #3370): Allow constant sequence/property formals
            !(IsSequenceFormal() && IsConstant())) {
            from->Error("%s is not a constant", Name()) ;
        }

        // id is used
        if (!is_defined_in_interface) SetUsed() ;
        break ;
    case VERI_CONSTANT_EXPRESSION :
    case VERI_PARAM_INITIAL_EXP :
        // usage of id in a constant expression
        // Const functions may be called without parentheses. We should not error out.
        if (!IsParam() && !IsGenVar() && !IsFunction() && !IsLetId() &&
            // VIPER #3723 (#2940, #3370): Allow constant sequence/property formals
            !(IsSequenceFormal() && IsConstant())) {
            from->Error("%s is not a constant", Name()) ;
        }
        // Viper 5573 specparam not allowed in parameter initialization expression
        // VIPER #6581: Produce the warning message on the 'from' node for proper linefile-info:
        if (ParamType() == VERI_SPECPARAM && environment == VERI_PARAM_INITIAL_EXP) from->Warning("specparam %s not allowed here", Name()) ;

        // id is used
        if (!is_defined_in_interface) SetUsed() ;
        break ;
    case VERI_INTEGRAL_CONSTANT :
        {
        // usage of id in an integral constant expression
        // Const functions may be called without parentheses. We should not error out.
        // Viper 4841. Check IsActualConstant instead of IsConstant
        unsigned is_const = (IsSequenceFormal() || IsPropertyFormal()) ? IsActualConstant() : 0 ;
        // VIPER #5862 : If actual of sequence/property formal is of type parameter
        // type/unresolved name type, we should not produce error
        unsigned do_not_produce_error = 0 ;
        if (IsSequenceFormal() || IsPropertyFormal()) {
            VeriExpression *actual = GetActual() ;
            VeriTypeInfo *t = (actual) ? actual->CheckType(0, NO_ENV): 0 ;
            if (!t || (t->IsTypeParameterType() || t->IsUnresolvedNameType())) {
                do_not_produce_error = 1 ;
            }
            delete t ;
        }
        if (!IsParam() && !IsGenVar() && !IsFunction() && !is_const && !do_not_produce_error) {
            from->Error("%s is not a constant", Name()) ;
        }
        // Real is not allowed in integral constant :
        if (Type() == VERI_REAL) {
            from->Error("illegal context for real expression") ;
        }

        // id is used
        if (!is_defined_in_interface) SetUsed() ;
        break ;
        }
    case VERI_DELAY_CONTROL :
        // usage of id in a delay (control) expression.
        // CHECK ME : Maybe only specparam is allowed here ?
        // CHECK ME : We consider this 'used'..
        if (!is_defined_in_interface) SetUsed() ;
        break ;

    case VERI_EVENT_CONTROL :
    case VERI_EVENT_CONTROL_UNDECL_USE_GOOD :
    case VERI_SEQ_PROP_INIT :
        // usage of id in a @(..) list expression
        // CHECK ME : id in a event control expression is not considered 'used' ?
        // VIPER #3063 : Only variable, parameter, genvar, sequence and event are allowed in event control
        if (!IsVar() && !IsParam() && !IsEvent() && !IsGenVar() && !IsSequence() && !IsLetId()) {
                from->Error("%s is not valid in an expression", Name()) ;
        }
        break ;
    case VERI_PROPERTY_ARG :
        // VIPER #4229 : Only variable, parameter, genvar, sequence, property and event are allowed as actual in property instance
        if (!IsVar() && !IsParam() && !IsEvent() && !IsGenVar() && !IsSequence() && !IsProperty() && !IsLetId()) {
                from->Error("%s is not valid in an expression", Name()) ;
        }
        break ;
    case VERI_SEQUENCE_ARG :
        // Only variable, parameter, genvar, sequence and event are allowed as actual in property instance
        if (!IsVar() && !IsParam() && !IsEvent() && !IsGenVar() && !IsSequence() && !IsLetId()) {
                from->Error("%s is not valid in an expression", Name()) ;
        }
        break ;
    case VERI_UPWARD_SCOPE_NAME :
        // No checks can be done for upwards Name() referencing during analysis.
        break ;

    case VERI_TYPE_ENV:
        // Expect a type-expression :
        // Check if the identifier is a type or an interface or a modport or a class
        // Covergroup can be used as a type.
        if (!IsType() && !IsInterface() && !IsModport() && !IsClass() && !IsCovergroup()) {
                from->Error("%s is not a type", Name()) ;
                // VIPER #2936: Clear the data type of this id, otherwise we may go
                // into an infinite loop if this id is specified as its own data type:
                ClearDataType() ;
        }

        // VIPER #8142: Here, id is used: set _is_used flag here for type parameters.
        if (!is_defined_in_interface && IsParam()) SetUsed() ;

        // Don't think we need to set anything here. Just type-checking.
        break ;
    case VERI_SEQUENCE_ENV:
        // In a sequence expression, only a property is not allowed.
        if (IsProperty()) {
            from->Error("property instance %s is not allowed in sequence expression", Name()) ;
        }
        // VIPER #5643 : Produce error if block identifier is used in sequence environment
        if (IsBlock()) {
            from->Error("%s is not valid in an expression", Name()) ;
        }
        break ;
    case VERI_PROPERTY_ENV:
        // In a property expression, pretty much all is allowed (sequence, normal expression, constants etc).
        // VIPER #5643 : Produce error if block identifier is used in property environment
        if (IsBlock()) {
            from->Error("%s is not valid in an expression", Name()) ;
        }
        break ;
    case VERI_BIN_VAL_ENV :
        // In a bin expression, ranges with non-consts are allowed
        // VIPER #5196: Only constant expressions are allowed in a bin expression used in covergroup:
        if (!IsParam() && !IsGenVar() && !IsFunction() &&
            !(IsSequenceFormal() && IsConstant()) && !IsConstant() && !IsPort()) {
            // VCS Compatibility issue (var_in_covgrp): In relaxed checking mode
            // produce warning instead of error
            if (InRelaxedCheckingMode()) {
                from->Warning("%s is not a constant", Name()) ;
            } else {
                from->Error("%s is not a constant", Name()) ;
            }
        }
        break ;
    default : break ; // no checks.
    }
}

/*********************************************************************************/
/* Onehot variable handling. Control bit flags for individual ids and control linked variables */
/*********************************************************************************/

void VeriVariable::SetCanBeOnehot()
{
    // Disable setting this bit if we wont extract FSMs. Will avoid confusion.
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    if (!_is_onehot_overridden) { // VIPER #8045: Set flag if it is not overridden already
        _can_be_onehot = 1 ;
        _is_onehot_overridden = 1 ;
    }
#endif
}
void VeriVariable::SetCannotBeOnehot()
{
    // Disable this routine to avoid the _link_var field if we don't extract fsms.
#ifdef VERILOG_CREATE_ONEHOT_FSMS
    // set the 'cannot_be_onehot' flag for all linked vars.
    if (!_can_be_onehot) return ; // already done.

    // Loop over all variables in the loop and disable their use as onehot reg.
    // Meanwhile, we can also break up the loop completely, since
    // we already know that these variable cannot be used as fsm.
    // Breaking up the loop will increase compilation speed.
    _can_be_onehot = 0 ;
    VeriVariable *runner = _link_var ;
    VeriVariable *tmp ;
    while (runner && runner!=this) {
        runner->_can_be_onehot = 0 ;
        tmp = runner ;
        runner = runner->_link_var ;
        tmp->_link_var = 0 ; // break up the loop completely.
    }
    _link_var = 0 ; // break up the loop completely
    _is_onehot_overridden = 1 ; // VIPER #8045 : It is overridden
#endif
}

void VeriVariable::LinkOnehotVar(VeriIdDef *var)
{
    if (!var || var==this) return ;
    // Disable this routine to avoid the _link_var field if we don't extract fsms.
#ifdef VERILOG_CREATE_ONEHOT_FSMS

    // only link variables :
    if (!var->IsVar()) return ;

    // Check if they both can be onehot encoded.
    if (!CanBeOnehot()) { var->SetCannotBeOnehot() ; return ; }
    if (!var->CanBeOnehot()) { SetCannotBeOnehot() ; return ; }

    // insert this variable in the link loop. Similar to Net wire linking.
    // FIX ME : up-cast..
    VeriVariable *newvar = (VeriVariable*)var ;

    if (_link_var && newvar->_link_var) {
        // Only if we BOTH have a _link_var loop, we
        // need to check if newvar is in my loop (or I am in newvar's loop).
        // If one of us has no _link_var loop, its was certainly never linked yet
        // so we can safely insert.
        VeriVariable *runner = _link_var ;
        while (runner != this) {
            if (runner == newvar) return ; // We are already connected
            runner = runner->_link_var ;
        }
    }

    // Before we merge the two variable link loops,
    // we should merge the state constants tables.
    Map *this_table = FsmStateConstants() ;
    Map *other_table = newvar->FsmStateConstants() ;
    if (this_table && other_table) {
        VERIFIC_ASSERT(this_table!=other_table) ; // Unlinked variables should never have the same table.
        // Only if both variable loops have a table, we need to consolidate them into one.
        // We will maintain 'this_table' and move 'other_table' into it.
        // First, remove 'other_table' from the 'newvar' loop. Find it in the loop :
        VeriVariable *runner = newvar ;
        while (runner) {
            if (runner->_fsm_constants) {
                VERIFIC_ASSERT(runner->_fsm_constants == other_table) ; // there should never be multiple tables in one variable loop..
                runner->_fsm_constants = 0 ; // remove the table from its container.
                break ;
            }
            runner = runner->_link_var ;
            VERIFIC_ASSERT(runner != newvar) ; // should have found 'other_table' in the loop.
        }

        // Move the items from 'other_table' into 'this_table' before we delete other_table.
        // The hash table will prevent multiple insertions of the same constant
        // FIX ME: integer image could overflow for >32 bit Verilog values. We might need a state_expression compare and force-insert..
        MapIter mi ;
        char *integer_image ;
        // VIPER #7995 : Key and Value of _fsm_constants is now char*
        char *state_expression ;
        FOREACH_MAP_ITEM(other_table, mi, &integer_image, &state_expression) {
            if (!this_table->Insert(integer_image, state_expression)) {
                Strings::free(integer_image) ;
                Strings::free(state_expression) ;
            }
        }
        // finally delete the 'other_table'.
        delete other_table ;
    }

    // Now, hook both loops into one _link_var loop
    VeriVariable *my_old_link_var = _link_var ;
    _link_var = (newvar->_link_var) ? newvar->_link_var : newvar ;
    newvar->_link_var = (my_old_link_var) ? my_old_link_var : this ;
    // Done.
#endif // #ifdef VERILOG_CREATE_ONEHOT_FSMS
}

/******************************/
/* Direction handling */
/******************************/
// Direction of a port is stored in 4 bits on the variable, to save memory space for bit-flags on VeriVariable.
// Since we currently use only values 0->6, we could actually get away with only 3 bits (if needed)..
// Verilog applications work with API routines below to convert to the normal VERI tokens.
unsigned VeriVariable::Dir() const
{
    switch (_dir) {
    case 0 : return 0 ; // not a port
    case 1 : return VERI_INPUT ;
    case 2 : return VERI_OUTPUT ;
    case 3 : return VERI_INOUT ;
    case 4 : return VERI_REF ;
    case 5 : return VERI_INTERFACE ;
    case 6 : return VERI_CONST ;
    default : return 0 ; // do not know this direction
    }
}

void VeriVariable::SetDir(unsigned direction)
{
    switch (direction) {
    case 0 : _dir = 0 ; break ; // not a port
    case VERI_INPUT : _dir = 1 ; break ;
    case VERI_OUTPUT : _dir = 2 ; break ;
    case VERI_INOUT : _dir = 3 ; break ;
    case VERI_REF : _dir = 4 ; break ;
    case VERI_INTERFACE : _dir = 5 ; break ; // generic 'interface'
    case VERI_CONST : _dir = 6 ; break ; // VERI_CONST means 'const ref'.
    default : _dir = 0 ; VERIFIC_ASSERT(0) ; break ; // do not know this token
    }
}
unsigned VeriPrototypeId::Dir() const
{
    switch (_dir) {
    case 0 : return 0 ; // not a port
    case 1 : return VERI_INPUT ;
    case 2 : return VERI_OUTPUT ;
    case 3 : return VERI_INOUT ;
    case 4 : return VERI_REF ;
    case 5 : return VERI_INTERFACE ;
    case 6 : return VERI_CONST ;
    default : return 0 ; // do not know this direction
    }
}
void VeriPrototypeId::SetDir(unsigned direction)
{
    switch (direction) {
    case 0 : _dir = 0 ; break ; // not a port
    case VERI_INPUT : _dir = 1 ; break ;
    case VERI_OUTPUT : _dir = 2 ; break ;
    case VERI_INOUT : _dir = 3 ; break ;
    case VERI_REF : _dir = 4 ; break ;
    case VERI_INTERFACE : _dir = 5 ; break ; // generic 'interface'
    case VERI_CONST : _dir = 6 ; break ; // VERI_CONST means 'const ref'.
    default : _dir = 0 ; VERIFIC_ASSERT(0) ; break ; // do not know this token
    }
}

/******************************/
/* Tests */
/******************************/

unsigned VeriVariable::IsInput() const      { return (Dir() == VERI_INPUT) ; }
unsigned VeriVariable::IsOutput() const     { return (Dir() == VERI_OUTPUT) ; }
unsigned VeriVariable::IsInout() const      { return (Dir() == VERI_INOUT) ; }
unsigned VeriIdDef::IsLocalParam() const    { return (ParamType()==VERI_LOCALPARAM) ? 1 : 0 ; }
unsigned VeriPrototypeId::IsInput() const      { return (Dir() == VERI_INPUT) ; }
unsigned VeriPrototypeId::IsOutput() const     { return (Dir() == VERI_OUTPUT) ; }
unsigned VeriPrototypeId::IsInout() const      { return (Dir() == VERI_INOUT) ; }
unsigned VeriPrototypeId::IsInterfacePort() const { return (_actual_id) ? _actual_id->IsInterfacePort(): 0 ; }
unsigned VeriPrototypeId::IsGenericInterfacePort() const { return (_actual_id) ? _actual_id->IsGenericInterfacePort(): 0 ; }
unsigned VeriPrototypeId::IsParam() const { return (_actual_id) ? _actual_id->IsParam(): 0 ; }
unsigned VeriPrototypeId::ParamType() const { return (_actual_id) ? _actual_id->ParamType(): 0 ; }
unsigned VeriSeqPropertyFormal::IsInterfacePort() const
{
    if (_is_processing) return 0 ;
    SetIsProcessing() ;

    // VIPER #6760: Define IsInterfacePort on VeriSeqPropertyFormal:
    // Actual is a VeriPortConnect like .a(b), take the actual connection from it:
    VeriExpression *actual_conn = (_actual) ? _actual->GetConnection() : 0 ;
    unsigned result = 0 ;
    if (actual_conn) {
        // Now get the id from the actual connection and check if it is an interface port:
        VeriIdDef *actual_id = actual_conn->GetId() ;
        result = (actual_id) ? actual_id->IsInterfacePort() : 0 ;
    } else {
        // 5611/SV_2009/Negative/17.2/test6: If no actual connection is present
        // fall back to the default data type check:
        result = VeriVariable::IsInterfacePort() ;
    }

    ClearIsProcessing() ;
    return result ;
}
// VIPER #5949: Check for reg type:
unsigned VeriVariable::IsReg() const
{
    // A Variable is considered a 'reg' (Verilog 2000 semantics) if it has a 'reg' type :
    //switch (_type) {
    switch (_kind) {
    case VERI_REG :
    case VERI_TIME :
    case VERI_INTEGER :
    case VERI_REAL :
    case VERI_REALTIME :
    // VIPER #6842 : string and event are ams data types too
    case VERI_STRINGTYPE :
    // event is a variable in System Verilog: See issue 2557
    case VERI_EVENT :
    // Other 'variable' styles :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_BYTE :
    case VERI_LOGIC :
    case VERI_BIT :
    case VERI_SHORTREAL :
    case VERI_CHANDLE :
    case VERI_CHAR : // 3.0 only
    // complex types
    case VERI_STRUCT :
    case VERI_ENUM :
    case VERI_UNION :
    // more stuff that can be called up :
    case VERI_CLASS : // a class (as type of a variable)
    case VERI_COVERGROUP :
    case VERI_INTERFACE : // an interface (as type of a variable)
    case VERI_MODPORT : // a modport (as type of a variable)
    // type via a type-parameter : type is not yet know, but accept for now as Reg(), since its certainly a variable.. FIX ME later.
    case VERI_TYPE :
    case VERI_VAR :
        return 1 ;
    default : break ;
    }

    // No type set means its not yet resolved user-declared type.
    // That must be an interface object or so. Assume it is a 'reg'...?
    // Better would be to explicitly ignore testing IsReg for ids with unresolved types..
    if (!_kind) return 1 ;

    return 0 ;
}
unsigned VeriSeqPropertyFormal::IsReg() const
{
    // If data type exists, get type from data type
    if (_data_type || _dimensions) return VeriVariable::IsReg() ;

    // No data type, extract type from actual
    if (!_actual) return VeriVariable::IsReg() ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV) ;
    unsigned is_reg = (actual_type) ? actual_type->IsRegType() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return is_reg ;
}
unsigned VeriPrototypeId::IsReg() const { return (_actual_id) ? _actual_id->IsReg(): 0 ; }

// VIPER #5949: Check for net type:
unsigned VeriVariable::IsNet() const
{
    // Check if it has a 'net' type :
    unsigned kind = _kind ? _kind : _type ;
    //switch (_type) {
    switch (kind) {
    case VERI_WIRE :
    case VERI_TRI  :
    case VERI_TRI1 :
    case VERI_SUPPLY0 :
    case VERI_WAND :
    case VERI_TRIAND :
    case VERI_TRIOR :
    case VERI_TRI0 :
    case VERI_SUPPLY1 :
    case VERI_WOR :
    case VERI_TRIREG :
    case VERI_UWIRE : /* Verilog 2005 addition*/
    case VERI_INTERCONNECT:
        return 1 ;
    default : break ;
    }
    return 0 ;
}
unsigned VeriSeqPropertyFormal::IsNet() const
{
    // If data type exists, get type from data type
    if (_data_type || _dimensions) return VeriVariable::IsNet() ;

    // No data type, extract type from actual
    if (!_actual) return VeriVariable::IsNet() ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV) ;
    unsigned is_net = (actual_type) ? actual_type->IsNetType() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return is_net ;
}
unsigned VeriPrototypeId::IsNet() const { return (_actual_id) ? _actual_id->IsNet() : 0 ; }

unsigned VeriVariable::IsEvent() const
{
    return (_type==VERI_EVENT) ? 1 : 0 ;
}
unsigned VeriSeqPropertyFormal::IsEvent() const
{
    // If data type exists, get type from data type
    if (_data_type || _dimensions) return VeriVariable::IsEvent() ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned is_event = (actual_type) ? actual_type->IsEventType() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return is_event ;
}
unsigned VeriPrototypeId::IsEvent() const { return (_actual_id) ? _actual_id->IsEvent() : 0 ; }

// Tests which use the 'moduleitem' backpointer to find out what they are.
unsigned VeriFunctionId::IsFunction() const
{
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item) ? mod_item->IsFunctionDecl() : 0 ;
}
unsigned VeriFunctionId::IsTask() const
{
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item) ? mod_item->IsTaskDecl() : 0 ;
}
unsigned VeriFunctionId::IsVar() const
{
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item && mod_item->IsFunctionDecl()) ? 1 : 0 ;
}
unsigned VeriPrototypeId::IsVar() const { return (_actual_id) ? _actual_id->IsVar() : 0 ; }
unsigned VeriPrototypeId::IsPort() const { return (_dir) ? 1: 0 ; }

unsigned  VeriIdDef::IsPackage() const
{
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item) ? mod_item->IsPackage() : 0 ;
}
unsigned  VeriModuleId::IsPackage() const
{
    if (_is_package) return 1 ; //Viper 5256. Get the is_package info from flag as in the constructor it is still a module and not package
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item) ? mod_item->IsPackage() : 0 ;
}

unsigned VeriIdDef::IsInterconnectType() const
{
    VeriDataType *data_type = GetDataType() ;
    return data_type && data_type->IsInterconnectType() ;
}

unsigned  VeriIdDef::IsProperty() const
{
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item) ? mod_item->IsPropertyDecl() : 0 ;
}
unsigned  VeriIdDef::IsSequence()  const
{
    VeriModuleItem *mod_item = GetModuleItem() ;
    return (mod_item) ? mod_item->IsSequenceDecl() : 0 ;
}
unsigned  VeriModuleId::IsRootModule() const
{
    // Check if this module is the root module
    return (_tree) ? _tree->IsRootModule() : 0 ;
}

unsigned  VeriVariable::IsInterfacePort()  const
{
    // Check if this is an interface port.
    // This routine can be run after Resolve() (direct visible scope resolving).
    // So, interface back-pointer can be set or not
    if (!IsPort()) return 0 ; // this is not a port..
    if (!_data_type) return 0 ; // without a 'datatype' structure this is a default port type. Not a interface port

    // Check if it is a 'generic' interface port. Use what bison is creating now :
    if (Dir()==VERI_INTERFACE) return 1 ; // generic interface port...
    if (_data_type->GetType()==VERI_INTERFACE) {
        // Type() of data type can be VERI_INTERFACE for interface class reference
        // Check that
        VeriIdDef *class_id = _data_type->GetId() ;
        if (class_id && class_id->IsClass()) return 0 ;
        return 1 ; // generic interface port ?
    }

    // Check if there is a type name (typeref) rather than a normal datatype :
    if (!_data_type->GetTypeName()) return 0 ; // no 'name', so no interface port.

    // Check the type identifier of this port :
    VeriIdDef *port_type = _data_type->GetId() ;
    if (!port_type) {
        // Not resolved yet. This means type is not visible in scope. So must be a interface type :
        return 1 ;
    } else {
        // Is resolved. Check if it is pointing to a interface or modport or (interface) inst:
        if (port_type->IsInterface() || port_type->IsModport() || port_type->IsInterfaceInst() || port_type->IsInterfacePort()) return 1 ;
    }
    // Not an interface port.
    return 0 ;
}

unsigned  VeriVariable::IsGenericInterfacePort()  const
{
    // Check if it is a 'generic' interface port. Use what bison is creating now :
    if (Dir()==VERI_INTERFACE) return 1 ; // generic interface port...
    if (!_data_type) return 0 ; // without a 'datatype' structure this is a default port type. Not a generic interface port

    VeriName *type_name = _data_type->GetTypeName() ;
    if (!type_name) {
        if (_data_type->GetType()==VERI_INTERFACE) return 1 ; // generic interface port
        return 0 ; // without a type-name this cannot be a generic interface port
    }

    VeriName *prefix = type_name->GetPrefix() ;
    while (prefix) {
        if (prefix->GetToken()==VERI_INTERFACE) return 1 ; // generic interface port
        prefix = prefix->GetPrefix() ;
    }

    // Not a generic interface port.
    return 0 ;
}

// Test if identifier contains unpacked dimension/unpacked data type i.e real
// or unpacked structure/union
unsigned VeriIdDef::IsUnpacked() const
{
    return 0 ;
}
unsigned VeriParamId::IsUnpacked() const
{
    if (_dimensions) return 1 ; // Has unpacked dimension, return 1
    //if (!_data_type) return 0 ; // No data type : scalar.
    if (IsProcessing()) return 0 ;

    VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
    VeriTypeInfo *param_type = param_id->CreateType(0) ;
    unsigned is_unpacked = 0 ;

    if (param_type && param_type->IsUnpacked()) {
        is_unpacked = 1 ;
    }
    delete param_type ;
    return is_unpacked ;
    /*SetIsProcessing() ;

    unsigned is_unpacked = _data_type->IsUnpacked() ; // Check data type
    ClearIsProcessing() ;
    return is_unpacked ;*/
}
unsigned VeriVariable::IsUnpacked() const
{
    if (_dimensions) return 1 ; // Has unpacked dimension, return 1
    if (!_data_type) return 0 ; // No data type : scalar
    return _data_type->IsUnpacked() ; // Check data type
}
unsigned VeriSeqPropertyFormal::IsUnpacked() const
{
    if (_dimensions) return 1 ; // Has unpacked dimension, return 1
    // If data type exists, get type from data type
    if (_data_type) return VeriVariable::IsUnpacked() ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned is_unpacked = (actual_type) ? actual_type->IsUnpacked() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return is_unpacked ;
}
unsigned VeriNamedPort::IsUnpacked() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV): 0 ;
    unsigned is_unpacked = (type) ? type->IsUnpacked(): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return is_unpacked ;
}
unsigned VeriPrototypeId::IsUnpacked() const { return (_actual_id) ? _actual_id->IsUnpacked(): 0 ; }
// Test if identifier contains packed dimension/packed data type i.e
// packed structure/union
unsigned VeriIdDef::IsPacked() const { return 0 ; }
unsigned VeriParamId::IsPacked() const
{
    if (_dimensions) return 0 ; // Has unpacked dimension, return 0
    //if (!_data_type) return 1 ; // No data type : scalar.
    if (IsProcessing()) return 0 ;
    VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
    VeriTypeInfo *param_type = param_id->CreateType(0) ;
    unsigned is_packed = 0 ;
    if (param_type && param_type->IsPacked()) {
        is_packed = 1 ;
    }
    delete param_type ;
    return is_packed ;
   /*
    SetIsProcessing() ;
    unsigned is_packed = _data_type->IsPacked() ; // Check data type
    ClearIsProcessing() ;
    return is_packed ;*/
}
unsigned VeriVariable::IsPacked() const
{
    if (_dimensions) return 0 ; // Has unpacked dimension, return 0
    if (!_data_type) return 1 ; // No data type : scalar
    return _data_type->IsPacked() ; // Check data type
}
unsigned VeriSeqPropertyFormal::IsPacked() const
{
    if (_dimensions) return 0 ; // Has unpacked dimension, return 0
    // If data type exists, get type from data type
    if (_data_type) return VeriVariable::IsPacked() ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned is_packed = (actual_type) ? actual_type->IsPacked() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return is_packed ;
}
unsigned VeriNamedPort::IsPacked() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV): 0 ;
    unsigned is_packed = (type) ? type->IsPacked(): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return is_packed ;
}
unsigned VeriPrototypeId::IsPacked() const { return (_actual_id) ? _actual_id->IsPacked(): 0 ; }

unsigned VeriVariable::IsVirtualInterface() const
{
    return (_data_type) ? _data_type->IsVirtualInterface() : 0 ;
}
unsigned VeriParamId::IsVirtualInterface() const
{
    return (_data_type) ? _data_type->IsVirtualInterface() : 0 ;
}

//////////////////////////////////////////////////////////////////////
//                        Net type of a net :
//////////////////////////////////////////////////////////////////////
unsigned VeriVariable::NetType() const
{
    // VIPER #5949: If data type exists, return net type from data type. Otherwise for nets return _kind.
    //return (_data_type && _data_type->NetType()) ? _data_type->NetType() : (IsNet() ? _type: 0) ;
    return (_data_type && _data_type->NetType()) ? _data_type->NetType() : (IsNet() ? _kind: 0) ;
}
unsigned VeriPrototypeId::NetType() const { return (_actual_id) ? _actual_id->NetType(): 0 ; }
//////////////////////////////////////////////////////////////////////
// Obtain pointers to a member (struct/union) of this data type :
//////////////////////////////////////////////////////////////////////

// return struct-member by name (if data type is a structure/union)
VeriIdDef *VeriVariable::GetMember(const char *member_name) const
{
    // Find member in the data type of this variable
    VeriDataType *data_type = GetDataType() ; // via API for type-parameters (could use _data_type here since its a variable).
    return (data_type) ? data_type->GetMember(member_name) : 0 ;
}

VeriIdDef *VeriParamId::GetMember(const char *member_name) const
{
    if (IsProcessing()) return 0 ;

    VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
    VeriTypeInfo *param_type = param_id->CreateType(0) ;
    VeriIdDef *member = 0 ;
    if (param_type && !param_type->IsTypeParameterType() && !param_type->IsUnresolvedNameType()) {
        member = param_type->GetMember(member_name) ;
    }
    delete param_type ;
    return member ;
    /*
    SetIsProcessing() ;
    // Find member in the data type of this parameter
    VeriDataType *data_type = GetDataType() ; // via API for type-parameters.
    VeriIdDef *member = (data_type) ? data_type->GetMember(member_name) : 0 ;
    ClearIsProcessing() ;
    return member ;*/
}

VeriIdDef *VeriInterfaceId::GetMember(const char *member_name) const
{
    // Find the member in the local scope:
    return (_local_scope) ? _local_scope->FindLocal(member_name) : 0 ;
}
VeriIdDef *VeriPrototypeId::GetMember(const char *member_name) const { return (_actual_id) ? _actual_id->GetMember(member_name) : 0 ; }

//////////////////////////////////////////////////////////////////////
// Obtain dimensions of an identifier :
//////////////////////////////////////////////////////////////////////

// Useful Tests, which work after Resolve (analysis) has been run.
// For Verilog 2001, these routines are trivial, but for SV, they look through named types.

// return the number of packed (memory) dimensions until we hit a scalar (or struct in SV)
unsigned VeriVariable::PackedDimension() const
{
    VeriDataType *data_type = GetDataType() ; // via API for type-parameters (could use _data_type here since its a variable).
    return (data_type) ? data_type->PackedDimension() : 0 ;
}
unsigned VeriParamId::PackedDimension() const
{
    if (IsProcessing()) return 0 ;

    VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
    VeriTypeInfo *param_type = param_id->CreateType(0) ;
    unsigned packed_dims = 0 ;
    if (param_type && !param_type->IsTypeParameterType() && !param_type->IsUnresolvedNameType()) {
        packed_dims = param_type->PackedDimension() ;
    }
    delete param_type ;
    return packed_dims ;
/*
    SetIsProcessing() ;
    VeriDataType *data_type = GetDataType() ; // via API for type-parameters.
    unsigned packed_dim = (data_type) ? data_type->PackedDimension() : 0 ;
    ClearIsProcessing() ;
    return packed_dim ;*/
}
unsigned VeriSeqPropertyFormal::PackedDimension() const
{
    // If data type exists, get dimension from data type
    if (_data_type || _dimensions) return VeriVariable::PackedDimension() ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned packed_dims = (actual_type) ? actual_type->PackedDimension() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return packed_dims ;
}
unsigned VeriNamedPort::PackedDimension() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV) : 0 ;
    unsigned packed_dim_count = (type) ? type->PackedDimension(): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return packed_dim_count ;
}
unsigned VeriPrototypeId::PackedDimension() const { return (_actual_id) ? _actual_id->PackedDimension() : 0 ; }

// return the number of unpacked (array) dimensions until we hit a scalar (or struct in SV)
unsigned VeriVariable::UnPackedDimension() const    {
    // Count the number of local unpacked dimensions :
    unsigned dim = 0 ;
    VeriRange *runner = _dimensions ;
    while (runner) {
        dim++ ;
        runner = runner->GetNext() ;
    }

    VeriDataType *data_type = GetDataType() ; // via API for type-parameters (could use _data_type here since its a variable).
    if (data_type) dim += data_type->UnPackedDimension() ;

    return dim ;
}
unsigned VeriParamId::UnPackedDimension() const
{
    if (IsProcessing()) return 0 ;
    VeriParamId *param_id = const_cast<VeriParamId*>(this) ;
    VeriTypeInfo *param_type = param_id->CreateType(0) ;
    unsigned unpacked_dims = 0 ;
    if (param_type && !param_type->IsTypeParameterType() && !param_type->IsUnresolvedNameType()) {
        unpacked_dims = param_type->UnpackedDimension() ;
    }
    delete param_type ;
    return unpacked_dims ;
/*
    SetIsProcessing() ;
    // Count the number of local unpacked dimensions :
    unsigned dim = 0 ;
    VeriRange *runner = _dimensions ;
    while (runner) {
        dim++ ;
        runner = runner->GetNext() ;
    }
    // And add the number of unpacked dimensions from the data type (if there) :

    VeriDataType *data_type = GetDataType() ; // via API for type-parameters.
    if (data_type) dim += data_type->UnPackedDimension() ;

    ClearIsProcessing() ;
    return dim ;
*/
}
unsigned VeriInstId::UnPackedDimension() const
{
    // Count the number of local unpacked dimensions :
    unsigned dim = 0 ;
    VeriRange *runner = _dimensions ;
    while (runner) {
        dim++ ;
        runner = runner->GetNext() ;
    }

    return dim ;
}
unsigned VeriSeqPropertyFormal::UnPackedDimension() const
{
    // If data type exists, get dimension from data type
    if (_data_type || _dimensions) return VeriVariable::UnPackedDimension() ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned dims = (actual_type) ? actual_type->UnpackedDimension() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return dims ;
}
unsigned VeriNamedPort::UnPackedDimension() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV): 0 ;
    unsigned unpacked_dim_count = (type) ? type->UnpackedDimension(): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return unpacked_dim_count ;
}
unsigned VeriPrototypeId::UnPackedDimension() const { return (_actual_id) ? _actual_id->UnPackedDimension(): 0 ; }

//////////////////////////////////////////////////////////////////////
// Obtain pointers to the nearest packed/unpacked dimension(s) :
//////////////////////////////////////////////////////////////////////

// Return packed dimensions from the data type :
VeriRange *VeriVariable::GetDimensionAt(unsigned dimension) const {
    // First decent into the local (unpacked) dimensions :
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) return runner ; // requested dimension is right here

    // Otherwise, decend into the data type dimensions :
    VeriDataType *data_type = GetDataType() ; // via API for type-parameters.
    return (data_type) ? data_type->GetDimensionAt(dimension) : 0 ;
}

VeriRange *VeriParamId::GetDimensionAt(unsigned dimension) const {
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    // First decent into the local (unpacked) dimensions :
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) {
        ClearIsProcessing() ;
        return runner ; // requested dimension is right here
    }

    // Otherwise, decend into the data type dimensions :
    VeriDataType *data_type = GetDataType() ; // via API for type-parameters.
    VeriRange *dim = (data_type) ? data_type->GetDimensionAt(dimension) : 0 ;
    ClearIsProcessing() ;
    return dim ;
}
VeriRange *VeriInstId::GetDimensionAt(unsigned dimension) const {
    // Decent into the local (unpacked) dimensions :
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) return runner ; // requested dimension is right here

    // Out of bound accessing, return 0
    return 0 ;
}
VeriRange *VeriPrototypeId::GetDimensionAt(unsigned dimension) const { return (_actual_id) ? _actual_id->GetDimensionAt(dimension) : 0 ; }

//VeriScope   *VeriInstId::LocalScope() const
//{
    // A bit more difficult :
    // Find the instantiated module, and return its scope :
    //
    // 5/2003 : We really should not do this at all (in analysis) :
    // We risk putting cross-module boundary pointers in place,
    // which can cause dangling pointers if elaboration uses the references to look at the nets
    // under an identifier in another module.
    // On top of that, there is no guarantee that the module(s) are here at all (during analysis).
    // Need to wait until elaboration before we can do anything with certainty.
    //return 0 ;
//}

// For veriety of identifiers :
// return ports in array of VeriIdDef* form
Array *VeriIdDef::GetPorts()        { return 0 ; }
Array *VeriTaskId::GetPorts()       { return (_tree) ? _tree->GetPorts() : 0 ; }
Array *VeriFunctionId::GetPorts()   { return (_tree) ? _tree->GetPorts() : 0 ; }
Array *VeriModuleId::GetPorts()     { return (_tree) ? _tree->GetPorts() : 0 ; }
Array *VeriProductionId::GetPorts() { return (_prod) ? _prod->GetPorts() : 0 ; }
Array *VeriLetId::GetPorts() { return (_tree) ? _tree->GetPorts() : 0 ; }

//////////////////////////////////////////////////////////////////////////////////////
/// Obtain pointers to base data type (built-in type/struct/union) of an identifier :
//////////////////////////////////////////////////////////////////////////////////////

VeriDataType *VeriVariable::GetBaseDataType() const
{
    return (_data_type) ? _data_type->GetBaseDataType() : 0 ; // Get base type
}
VeriDataType *VeriParamId::GetBaseDataType() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    // FIXME : data type of a type parameter is the initial value of it. GetDataType
    // routine returns the initial value for type parameter and data type for value
    // parameters. Default value of type parameter will be fixed in elaboration. So,
    // before that we cannot use initial value of type parameter for type checking.
    // Currently we will use 'GetBaseDataType()' routine in analyzer where initial
    // value of type parameter is not appropriate. So, we are using _data_type instead
    // of GetDataType. This way this routine will return 0 always for type parameter.

    VeriDataType *data_type = (_data_type && (_data_type->GetType()!=VERI_TYPE)) ? _data_type->GetBaseDataType() : 0 ;
    ClearIsProcessing() ;
    return data_type ;
    // RD: NOTE: We should not really do anything special here.
    // The choice that a type parameter gets initialized (data_type pointer set) to its initial type is a choice made in Resolve().
    // We can un-do it there at any time. So here we SHOULD return this :
    // return (_data_type) ? _data_type->GetBaseDataType() : 0 ;
    // Not enabling this code yet, because it will cause regressions.
}
VeriDataType *VeriSeqPropertyFormal::GetBaseDataType() const
{
    // If data type exists, get base type from data type
    if (_data_type || _dimensions) return VeriVariable::GetBaseDataType() ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    // No data type, extract type from actual
    VeriDataType *ret_type = (_actual) ? _actual->GetBaseDataType(): 0 ;
    ClearIsProcessing() ; // finished processing
    return ret_type ;
}
VeriDataType *VeriNamedPort::GetBaseDataType() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriDataType *ret_type = (_port_expr) ? _port_expr->GetBaseDataType(): 0 ;
    ClearIsProcessing() ;
    return ret_type ;
}
unsigned VeriSeqPropertyFormal::Type() const
{
    // If data type exists, get type from data type
    if (_data_type || _dimensions) return VeriVariable::Type() ;

    // No data type exists, extract type from _actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned type = (actual_type) ? actual_type->Type() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return type ;
}
unsigned VeriNamedPort::Type() const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV): 0 ;
    unsigned ret_type = (type) ? type->Type(): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return ret_type ;
}
unsigned VeriSeqPropertyFormal::IsSigned() const
{
    // If data type exists, get sign from that
    if (_data_type || _dimensions) return VeriVariable::IsSigned() ;

    // No data type, extract sign from actual
    if (!_actual) return 0 ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned sign = (actual_type) ? actual_type->IsSigned() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return sign ;
}
unsigned VeriSeqPropertyFormal::IsArray() const
{
    // If data type exists, get type info from that
    if (_data_type || _dimensions) return VeriVariable::IsArray() ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;
    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    unsigned is_arr = (actual_type) ? actual_type->IsPackedArrayType() : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return is_arr ;
}
unsigned VeriSeqPropertyFormal::IsConstant() const
{
    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    // Viper 4841. Check IsConstant from id
    VeriIdDef *const_id = (_actual) ? _actual->GetId() : 0 ;
    unsigned is_const = (const_id) ? const_id->IsConstant() : (_actual ? _actual->IsConstExpr() : 0) ;

    ClearIsProcessing() ; // finished processing
    return is_const ;
}
// Viper 4841. Check whether the actual is constant expr
unsigned VeriSeqPropertyFormal::IsActualConstant() const
{
    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    unsigned is_const = (_actual) ? _actual->IsConstExpr() : 0 ;
    // Viper 5533 IsActualConstant for nested actuals have to do a
    // recursive call is actual_id is of type VeriSeqPropertyFormal
    if (!is_const) {
        VeriIdDef *id = (_actual) ? _actual->GetId() : 0 ;
        if (id && (id->IsSequenceFormal() || id->IsPropertyFormal())) {
            is_const = id->IsActualConstant() ;
        }
    }
    ClearIsProcessing() ; // finished processing
    return is_const ;
}
unsigned VeriSeqPropertyFormal::IsParam() const
{
    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriIdDef *id = (_actual) ? _actual->GetId() : 0 ;
    unsigned is_param = (id) ? id->IsParam() : 0 ;
    ClearIsProcessing() ; // finished processing
    return is_param ;
}
unsigned VeriSeqPropertyFormal::IsGenVar() const
{
    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriIdDef *id = (_actual) ? _actual->GetId() : 0 ;
    unsigned is_genvar = (id) ? id->IsGenVar() : 0 ;
    ClearIsProcessing() ; // finished processing
    return is_genvar ;
}
// VIPER #7073: VeriSeqPropertyFormal does not represent a function, do not override:
//unsigned VeriSeqPropertyFormal::IsFunction() const
//{
//    if (IsProcessing()) return 0 ; // Recursive call
//    SetIsProcessing() ; // Mark starting of processing
//
//    VeriIdDef *id = (_actual) ? _actual->GetId() : 0 ;
//    unsigned is_function = (id) ? id->IsFunction() : 0 ;
//    ClearIsProcessing() ; // finished processing
//    return is_function ;
//}
unsigned VeriSeqPropertyFormal::IsLocalVar() const
{
    if (IsProcessing()) return 0 ; // Recursive call

    // VIPER #6335 : sequence/property formal can be declared as local
    // and that qualifier is stored in VeriVariable. So try to get information
    // from there first
    if (VeriVariable::IsLocalVar()) return 1 ;

    SetIsProcessing() ; // Mark starting of processing
    VeriIdDef *id = (_actual) ? _actual->GetId() : 0 ;
    unsigned is_localvar = (id) ? id->IsLocalVar() : 0 ;
    ClearIsProcessing() ; // finished processing
    return is_localvar ;
}

VeriIdDef *VeriSeqPropertyFormal::GetMember(const char *member_name) const
{
    // If data type exists, get type info from that
    if (_data_type || _dimensions) return VeriVariable::GetMember(member_name) ;

    // No data type, extract type from actual
    if (!_actual) return 0 ;
    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    VeriTypeInfo *actual_type = _actual->CheckType(0, NO_ENV);
    VeriIdDef *member = (actual_type) ? actual_type->GetMember(member_name) : 0 ;
    delete actual_type ;
    ClearIsProcessing() ; // finished processing
    return member ;
}
VeriIdDef *VeriNamedPort::GetMember(const char *member_name) const
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV): 0 ;
    VeriIdDef *member = (type) ? type->GetMember(member_name): 0 ;
    delete type ;
    ClearIsProcessing() ;
    return member ;
}
VeriRange *VeriSeqPropertyFormal::GetDimensionAt(unsigned dimension) const
{
    // If data type exists, get dimension from that
    if (_data_type || _dimensions) return VeriVariable::GetDimensionAt(dimension) ;

    // No data type, extract dimension from actual
    if (!_actual) return 0 ;
    // FIXME : try to do it with our Type structure (which does not include dimension now)
    if (IsProcessing()) return 0 ; // Recursive call

    VeriDataType *data_type = GetDataType() ;
    SetIsProcessing() ; // Mark starting of processing
    VeriRange *range = (data_type) ? data_type->GetDimensionAt(dimension) : 0 ;
    ClearIsProcessing() ; // finished processing
    return range ;
}
VeriRange *VeriNamedPort::GetDimensionAt(unsigned dimension) const
{
#if 0
    (void) dimension ;
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriConstraint *constraint = (_port_expr) ? _port_expr->EvaluateConstraintInternal(0, 0): 0 ;
    ClearIsProcessing() ;
    VeriRange *dimensions = constraint ? constraint->CreateDimensions(0 /* packed and unpacked*/, this) : 0 ;
    delete constraint ;
    VeriNamedPort *this_id = const_cast<VeriNamedPort*>(this) ;
    if (this_id) this_id->SetMemoryRange(dimensions) ;
    VeriRange *runner = dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) return runner ;
    return 0 ;
#else
    // VIPER #7607: Dimension is now always set properly, return from base class:
    return VeriVariable::GetDimensionAt(dimension) ;
#endif
}
VeriRange *VeriNamedPort::GetDimensions() const
{
#if 0
    if (IsProcessing()) return 0 ;
    VeriRange *dimensions = 0 ;
    SetIsProcessing() ;
    VeriConstraint *constraint = (_port_expr) ? _port_expr->EvaluateConstraintInternal(0, 0): 0 ;
    dimensions = constraint ? constraint->CreateDimensions(0 /* packed and unpacked*/, this) : 0 ;
    VeriNamedPort *this_id = const_cast<VeriNamedPort*>(this) ;
    if (this_id) this_id->SetMemoryRange(dimensions) ;
    delete constraint ;
    ClearIsProcessing() ;
    return dimensions ;
#else
    // VIPER #7607: Dimension is now always set properly, return from base class:
    return VeriVariable::GetDimensions() ;
#endif
}
VeriDataType *VeriPrototypeId::GetBaseDataType() const { return (_actual_id) ? _actual_id->GetBaseDataType() : 0 ; }
unsigned VeriPrototypeId::Type() const { return (_actual_id) ? _actual_id->Type() : 0 ; }
unsigned VeriPrototypeId::Sign() const { return (_actual_id) ? _actual_id->Sign(): 0 ; }
void VeriPrototypeId::SetType(unsigned /*type*/) { }//if (_actual_id) _actual_id->SetType(type) ; }
/******************************************************************************
*                     Create type from identifier                             *
*******************************************************************************/
VeriTypeInfo *VeriIdDef::CreateType(VeriIdDef * /*prefix_id*/)
{
    return 0 ; // Not a type, donot do anything
}
VeriTypeInfo *VeriVariable::CreateType(VeriIdDef *prefix_id)
{
    VeriTypeInfo *this_type = 0 ;
    if (_data_type) { // Data type exists
        // Create type from data type
        this_type = _data_type->CreateType(this) ;
    } else if (prefix_id && prefix_id->IsClocking() && _initial_value) {
        // When prefix is clocking id and variable has initial value,
        // its type should be extracted from initial value
        this_type = _initial_value->CheckType(0, NO_ENV) ;
    } else if (IsPort() // No data type for port. It is scalar type
            || _kind == VERI_VAR // VIPER #7108: Also do the same thing for 'var' type variable with no data type
        ) {
        this_type = new VeriBuiltInType((_type) ? _type: VERI_REG) ;
        this_type->SetNetType(_kind) ; // VIPER #5949 Set net type to the type_info
    } else if (IsFunction()) {
        unsigned ret_type = 0 ;
        // 2001 LRM section 10.3.1
        if (IsVeri95() || IsVeri2001()) ret_type = VERI_REG ;
        // 1800-2009 LRM section 13.4 :
        if (!ret_type) ret_type = VERI_LOGIC ;
        this_type = new VeriBuiltInType(ret_type) ;
    } else if (IsImplicitNet() && _type) {
        this_type = new VeriBuiltInType(_type) ;
        this_type->SetNetType(_kind) ; // VIPER #5949 Set net type to the type_info
    }
    if (!_dimensions) return this_type ;
    VeriTypeInfo *unpacked_type = CreateUnpackedArray(this_type, _dimensions) ;
    return unpacked_type ;
}
VeriTypeInfo *VeriInstId::CreateType(VeriIdDef * /*prefix_id*/)
{
    VeriName *unit_name = (_the_instance) ? _the_instance->GetModuleNameRef(): 0 ;
    VeriIdDef *unit_id = (unit_name) ? unit_name->GetId() : 0 ;
    VeriTypeInfo *type = (unit_id && unit_id->IsInterface()) ? new VeriNamedType(unit_id) : ((unit_id) ? 0 : new VeriNamedType(unit_name)) ;

    if (!_dimensions) return type ;
    VeriTypeInfo *unpacked_type = CreateUnpackedArray(type,  _dimensions) ;
    return unpacked_type ;
}
VeriTypeInfo *VeriParamId::CreateType(VeriIdDef * /*prefix_id*/)
{
    // VIPER #6011 : Check for recursion
    if (IsProcessing()) {
        Error("value of %s depends on itself", Name()) ;
        return 0 ;
    }
    SetIsProcessing() ;
    VeriTypeInfo *ret_type = 0 ;
    if (_data_type && (_data_type->Type() || _data_type->GetDimensions() || _data_type->IsTypeRef())) {
        // VIPER #5654: Not just 'signed' data type, create type from this data type:
        ret_type = _data_type->CreateType(this) ;
    } else if (!IsType() && _initial_value && !_dimensions) {
        if (_type) { // self-referencing assignment
            Error("value of %s depends on itself", Name()) ;
            ClearIsProcessing() ;
            return 0 ;
        }
        _type = VERI_INTEGER ; // Set type to indicate we are going to extract type from initial expression: prevent self-referencing
        // It is value parameter having no data type. Type of parameter will be
        // determined from its initial value. P1800 LRM 6.3.2 says, the type of
        //  right hand expression will be real or integral. If expression is real,
        //  parameter is real. If expression is integral, parameter is a logic vector
        VeriExpression *expr = _initial_value ;
        VeriTypeInfo *type = expr->CheckType(0, READ) ;
        _type = 0 ;
        if (!type) {
            ClearIsProcessing() ;
            return 0 ; // cannot extract type
        }
        if (type->IsRealType()) { // value real
            ret_type = new VeriBuiltInType(VERI_REAL) ; // parameter type real
        } else if (!type->IsTypeParameterType() && !type->IsUnresolvedNameType() && !type->IsEnumeratedType() && type->IsIntegralType()) { // value integral
            ret_type = new VeriArrayType(new VeriBuiltInType(VERI_REG), 1) ; // logic vector
            if (type->IsStringLiteral()) ret_type->SetAsStringLiteral() ;
            if (type->IsSigned()) ret_type->SetAsSigned() ;
        } else if (!type->IsTypeParameterType() && !type->IsUnresolvedNameType()) { // Error : illegal initial value
            //Warning("real/integral value required for parameter %s", Name()) ;
            // VIPER #5248 (test_70): Non type parameters without data type gets type from initial
            // value only for real/integral types. Allow all types under compatibility mode:
            ret_type = type ;
            type = 0 ; // Used up, retset it now
        }
        delete type ;
        // VIPER #5654: Only 'signed' data type should create type from initial value but imposes sign on it:
        if (ret_type && _data_type && _data_type->GetSigning()) ret_type->SetAsSigned() ;
    }

    if (!_dimensions) {
        ClearIsProcessing() ;
        return ret_type ; // Packed parameter
    }

    // VIPER #4749: If there is no data type but unpacked dimension is specified, create single bit type:
    if (!ret_type) ret_type = new VeriBuiltInType(VERI_REG) ;

    VeriTypeInfo *unpacked_type = CreateUnpackedArray(ret_type, _dimensions) ;
    ClearIsProcessing() ;
    return unpacked_type ;
}
VeriTypeInfo *VeriTypeId::CreateType(VeriIdDef *prefix_id)
{
    if (GetDataType()) return VeriVariable::CreateType(prefix_id) ; // Named type

    // Class/Interface/Covergroup
    VeriTypeInfo *named_type = new VeriNamedType(this) ;

    if (!_dimensions) return named_type ;
    VeriTypeInfo *unpacked_type = CreateUnpackedArray(named_type, _dimensions) ;
    return unpacked_type ;
}
VeriTypeInfo *VeriCovgOptionId::CreateType(VeriIdDef * /*prefix_id*/)
{
    VeriTypeInfo *this_type = 0 ;
    if (_data_type) { // Data type exists
        // Create type from data type
        this_type = _data_type->CreateType(this) ;
    }
    return this_type ;
}
VeriTypeInfo *VeriModportId::CreateType(VeriIdDef * /*prefix_id*/)
{
    // VIPER #7692: Create type for modports in the same way as interface instance:
    return new VeriNamedType(this) ;
}
VeriTypeInfo *VeriSeqPropertyFormal::CreateType(VeriIdDef *prefix_id)
{
    // If data type exists, create type from data type
    if (_data_type || _dimensions) return VeriVariable::CreateType(prefix_id) ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    // No data type, create type from actual
    VeriTypeInfo *type = (_actual) ? _actual->CheckType(0, NO_ENV): 0 ;

    ClearIsProcessing() ; // finished processing
    return type ;
}
VeriTypeInfo *VeriNamedPort::CreateType(VeriIdDef * /*prefix_id*/)
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriTypeInfo *ret_type = (_port_expr) ? _port_expr->CheckType(0, NO_ENV): 0 ;
    ClearIsProcessing() ;
    return ret_type ;
}
VeriTypeInfo *VeriPrototypeId::CreateType(VeriIdDef * /*prefix_id*/)
{
    return (_actual_id) ? _actual_id->CreateType(0): 0 ;
}

VeriTypeInfo *VeriGenVarId::CreateType(VeriIdDef * /*prefix_id*/)
{
    return new VeriBuiltInType(VERI_GENVAR) ;
}
VeriTypeInfo *VeriIdDef::CreateUnpackedArray(VeriTypeInfo *element, VeriRange *dimensions) const
{
    // Now support multiple dimensions of queue/associative array/dynamic array and
    // also mixed array like
    //  int x[$][*] ;
    // This is a queue declaration whose elements are associative array

    // This can be fixed-size unpacked array/associative-array/dynamic-array/queue
    // Type of unpacked array can be determined from dimension
    VeriTypeInfo *ret_type = 0 ;
    unsigned dim = 0 ; // keep track of dimension
    VeriRange *range = dimensions ;
    // We need to traverse dimensions from last i.e. for array like
    // int x[$][]
    // we need to consider [] first and then [$], as 'x' will be queue whose elements
    // are dynamic array
    Array ranges(3) ;
    while (range) {
        ranges.InsertLast(range) ;
        range = range->GetNext() ;
    }
    unsigned is_fixed_size = 0 ;
    unsigned i ;
    //while (range) {
    FOREACH_ARRAY_ITEM_BACK(&ranges, i, range) {
        //dim++ ; // Increase dimension
        // Test for associative array, dimension can have '*'/data type
        VeriExpression *left = range->GetLeft() ; // Msb of range
        VeriExpression *right = range->GetRight() ; // Lsb of range
        if ((range->GetPartSelectToken() == VERI_MUL) ||  // Dimension has '*'
            (right && right->IsDataType() && !left) ||    // Dimension data type
            (left && left->GetId() && left->GetId()->IsType() && !right)) {
            if (is_fixed_size) {
                ret_type = new VeriFixedSizeArray(ret_type ? ret_type:element, dim) ;
                dim = 0 ;
                is_fixed_size = 0 ; // One dimension of fixed size is illegal
            }
            //if (dim == 1) { // First unpacked dimension
            VeriTypeInfo *index_type = (right) ? right->CheckType(0, RANGE_BOUND) : (left) ? left->CheckType(0, RANGE_BOUND): 0 ;
                // Create associative array type
                //if (ret_type) delete ret_type ; // assign in a loop, free prior to overwrite else mem leak
            if (index_type && index_type->IsRealType()) {
                // IEEE 1800-2012 section 7.8.5 : real/shortreal is illegal index type
                // Some simulators allow real as index type, so produce warning for this
                Warning("real data type is not a legal type of an associative array index") ;
            }
            ret_type = new VeriAssociativeArray(ret_type ? ret_type:element, index_type) ;
            //} else {
                // Dimension greater than 1, but contains data type : Error
                //Warning("dimension %d of %s cannot have data type", dim, Name()) ;
                //break ;
            //}
        } else if ((range->GetPartSelectToken() == VERI_NONE) && !left && !right) { // Test for dynamic array, one empty dimension is allowed '[]'
            if (is_fixed_size) {
                ret_type = new VeriFixedSizeArray(ret_type ? ret_type:element, dim) ;
                dim = 0 ;
                is_fixed_size = 0 ; // One dimension of fixed size is illegal
            }
            //if (dim == 1) {
                //if (ret_type) delete ret_type ; // assign in a loop, free prior to overwrite else mem leak
            ret_type = new VeriDynamicArray(ret_type ? ret_type: element) ;
            //} else {
                // Dimension greater than 1, but contains nothing : Error
                //Warning("illegal empty dimension %d of %s", dim, Name()) ;
                //break ;
            //}
        } else if (left && left->IsDollar()) { // Test for queue, one dimension allowed and that dimension should have '$' '[$]'
            if (is_fixed_size) {
                ret_type = new VeriFixedSizeArray(ret_type ? ret_type:element, dim) ;
                dim = 0 ;
                is_fixed_size = 0 ; // One dimension of fixed size is illegal
            }
            //if (dim == 1) {
                //if (ret_type) delete ret_type ; // assign in a loop, free prior to overwrite else mem leak
            ret_type = new VeriQueue(ret_type ? ret_type: element) ;
            //} else {
                // Dimension other than 0 contains '$' error
                //Warning("$ in dimension %d for %s", dim, Name()) ;
                //break ;
            //}
        } else { // Fixed array
            is_fixed_size = 1 ; // mark as fixed size array
            dim++ ; // Increase dimension only for fixed size array
        }
        //range = range->GetNext() ; // Get next range
    }
    if (is_fixed_size) { // Create fixed size array
        ret_type = new VeriFixedSizeArray(ret_type ? ret_type : element, dim) ;
    }
    if (!ret_type) delete element ; // Delete coming type, not absorbed by any
    return ret_type ;
}

//////////////////////////////////////////////////////////////////////////////////////
// VIPER 3239 : some pragma's are non-positional, and applied to an identifier directly as attributes.
// Under compile flag VERILOG_STORE_ID_PRAGMAS is set, we store these non-positional attributes as char*->char* in a table on the identifier :
//////////////////////////////////////////////////////////////////////////////////////
void
VeriIdDef::AddIdPragma(const char *pragma_name, const char *pragma_value)
{
    if (!pragma_name && !pragma_value) return ;

#ifdef VERILOG_STORE_ID_PRAGMAS
    // Create the table if not yet there
    if (!_id_pragmas) _id_pragmas = new Map(STRING_HASH) ; // case-sensitive..?

    // Remove existing pragma if there :
    MapItem *exist = _id_pragmas->GetItem(pragma_name) ;
    if (exist) {
        char *name = (char*)exist->Key() ;
        char *value = (char*)exist->Value() ;
        _id_pragmas->Remove(pragma_name) ;
        Strings::free(name) ;
        Strings::free(value) ;
    }

    // Insert name->value pair in the table
    _id_pragmas->Insert(Strings::save(pragma_name), Strings::save(pragma_value)) ;
#endif
}

//CARBON_BEGIN
void
VeriIdDef::AddPragmaLineInfo(const char *pragma_name, const char *lineinfo)
{
    if (!pragma_name && !lineinfo) return ;

#ifdef VERILOG_STORE_ID_PRAGMAS
    // Create the table if not yet there
    if (!_line_infos) _line_infos = new Map(STRING_HASH) ; // case-sensitive..?

    // Remove existing pragma if there :
    MapItem *exist = _line_infos->GetItem(pragma_name) ;
    if (exist) {
        char *name = (char*)exist->Key() ;
        char *value = (char*)exist->Value() ;
        _line_infos->Remove(pragma_name) ;
        Strings::free(name) ;
        Strings::free(value) ;
    }

    // Insert pragmaName->lineinfo pair in the table
    _line_infos->Insert(Strings::save(pragma_name), Strings::save(lineinfo)) ;
#endif
}
//CARBON_END

const char *
VeriIdDef::GetIdPragma(const char *pragma_name)  const
{
    if (!pragma_name) return 0 ;
    // pick up from table :
#ifdef VERILOG_STORE_ID_PRAGMAS
    if (!_id_pragmas) return 0 ;
    return (const char*)_id_pragmas->GetValue(pragma_name) ;
#else
    return 0 ;
#endif
}

Map *
VeriIdDef::GetIdPragmas() const
{
#ifdef VERILOG_STORE_ID_PRAGMAS
    return _id_pragmas ;
#else
    return 0 ;
#endif
}

//CARBON_BEGIN
Map *
VeriIdDef::GetPragmaLineInfos() const
{
#ifdef VERILOG_STORE_ID_PRAGMAS
    return _line_infos;
#else
    return 0 ;
#endif
}
//CARBON_END

/******************************************************************************/
// Constraint from identifier
/******************************************************************************/
VeriConstraint *VeriIdDef::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/, unsigned /*ignore_dims */)
{ return 0 ; }
VeriConstraint *VeriVariable::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned ignore_dims)
{
    VeriConstraint *constraint = 0 ;
    if (_data_type) {
        constraint = _data_type->EvaluateConstraintInternal(tab, df) ;
    } else if (IsPort()) { // No data type for port. It is scalar type
        constraint = new VeriScalarTypeConstraint((_type) ? _type: VERI_REG) ;
    } else { //if (IsImplicitNet() && _type) {
        constraint = new VeriScalarTypeConstraint(_type) ;
    }
    if (!_dimensions || ignore_dims) return constraint ;
    // Incorporate unpacked dimensions :
    VeriRange *dim = _dimensions ;
    VeriConstraint *range_constraint ;
    Array range_arr(4) ;
    while (dim) {
        range_arr.InsertLast(dim) ;
        dim = dim->GetNext() ;
    }
    unsigned i ;
    FOREACH_ARRAY_ITEM_BACK(&range_arr, i, dim) {
        range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
        if (!range_constraint) {
            delete constraint ;
            constraint = 0 ;
            break ;
        }
        range_constraint->SetAsUnPacked() ;
        constraint = new VeriArrayConstraint(range_constraint, constraint) ;
    }
    return constraint ;
}
VeriConstraint *VeriGenVarId::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/, unsigned /*ignore_dims*/)
{
    // Genvar is an integer [31:0]
    //VeriConstraint *range = new VeriRangeConstraint(31, 0) ;
    //range->SetAsPacked() ; // Always packed
    //return (new VeriArrayConstraint(range, new VeriScalarTypeConstraint(VERI_REG))) ;
    return new VeriScalarTypeConstraint(VERI_INTEGER) ;
}
VeriConstraint *VeriParamId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned ignore_dims)
{
    if (IsElaborated()) return 0 ; // Recursive call
    SetIsElaborated() ; // To prevent self-referencing parameters

    // Build the constraint from the constraint of the identifier's data type
    VeriDataType *data_type = GetDataType() ;
    VeriConstraint *constraint = 0 ;
    if (data_type && (data_type->Type() || data_type->GetDimensions() || data_type->IsTypeRef())) {
        // VIPER #6516: Only evaluate constraint on data type if it is a real one:
        constraint = data_type->EvaluateConstraintInternal(tab, df) ;
        // VIPER #8388: If data_type has packed dimensions, but we are evaluating
        // constraint of enum literal, we should ignore packed dimension from constraint
        if (data_type->GetDimensions()) {
            Map *enum_lits = data_type->IsEnumType() ? data_type->GetEnumLiterals(): 0 ;
            VeriIdDef *enum_id = enum_lits ? (VeriIdDef*)enum_lits->GetValue(Name()): 0 ;
            if (IsEqualObject(static_cast<VeriParamId*>(enum_id))) {
                // We are evaluating enum literal, so ignore packed dimension
                VeriConstraint *ele_constraint = (constraint && constraint->IsArrayConstraint() && constraint->IsPacked()) ? constraint->ElementConstraint(): 0 ;
                if (ele_constraint && ele_constraint->IsEnumConstraint()) {
                    ele_constraint = ele_constraint->Copy() ;
                    delete constraint ;
                    constraint = ele_constraint ;
                }
            }
        }
    }
    // VIPER #8037 : If parameter has no data type and has only unpacked dimension,
    // consider 1 as packed size in relaxed checking mode
    if (!constraint && InRelaxedCheckingMode() && !data_type && _dimensions) {
        constraint = new VeriScalarTypeConstraint(VERI_REG) ;
    }

    if (!ignore_dims) {
        // Incorporate unpacked dimensions :
        VeriRange *dim = _dimensions ;
        VeriConstraint *range_constraint ;
        Array range_arr(4) ;
        while (dim) {
            range_arr.InsertLast(dim) ;
            dim = dim->GetNext() ;
        }
        unsigned i ;
        FOREACH_ARRAY_ITEM_BACK(&range_arr, i, dim) {
            range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
            if (!range_constraint) {
                delete constraint ;
                constraint = 0 ;
                break ;
            }
            range_constraint->SetAsUnPacked() ;
            constraint = new VeriArrayConstraint(range_constraint, constraint) ;
        }
    }

    // If we do not have a constraint for this parameter, we need to look into the initial value:
    VeriExpression *expr = _initial_value ;
    if (!constraint && expr) {
        // Evaluate the constraint of this expression:
        // VIPER #5697: Evaluate the constraint only if it is a type parameter:
        if (IsType()) constraint = expr->EvaluateConstraintInternal(tab, df) ;
        if (!constraint && IsStaticElab()) {
            // If we still do not have the constraint, get it the hard way:
            // Evaluate the expression:
            // VIPER #5207: No need to evaluate the expression, just call CheckType on it.
            // Otherwise we may go into a deep recursion because it will evaluate initial value.
            //VeriBaseValue *val = expr->StaticEvaluateInternal(0, 0, 0) ;
            VeriTypeInfo *id_type = expr->CheckType(0, NO_ENV) ;
            verific_int64 size_sign = expr->StaticSizeSignInternal(tab, 0) ;
            verific_uint64 size = GET_CONTEXT_SIZE(size_sign) ;
            // VIPER #5207: Check real type from the type returned by CheckType instead of value.
            //if (val && val->Type() == REAL)
            if (id_type && id_type->IsRealType()) {
                constraint = new VeriScalarTypeConstraint(VERI_REAL) ;
            } else if (id_type && !id_type->IsEnumeratedType() && id_type->IsIntegralType()) {
                // VIPER #8287: Create constraint if size is greater than 0.
                if (size > 0) {
                    //if (size == 1) {
                        //constraint = new VeriScalarTypeConstraint(VERI_REG) ;
                    //} else if (size > 1) {
                    // VIPER #4453 : Always create array constraint even of size 1 for
                    // parameter, as bit-select on object of size 1 is allowed.
                    VeriConstraint *range = new VeriRangeConstraint((int)size-1, 0) ;
                    range->SetAsPacked() ; // Always packed
                    constraint = new VeriArrayConstraint(range, constraint) ;
                    //}
                }
            } else {
                constraint = expr->EvaluateConstraintInternal(tab, df) ;
            }
            delete id_type ;
        }
    }
    ClearIsElaborated() ; // To prevent self-referencing parameters
    return constraint ;
}
VeriConstraint *VeriTypeId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned ignore_dims)
{
    if (GetDataType()) return VeriVariable::EvaluateConstraintInternal(tab, df, ignore_dims) ; // Named type
    // If this is a class, it is really a 'pointer'. So it is like chandle
    if (IsClass()) {
        VeriConstraint *constraint = new VeriScalarTypeConstraint(VERI_CHANDLE) ;
        if (!_dimensions) return constraint ;
        // Incorporate unpacked dimensions :
        VeriRange *dim = _dimensions ;
        VeriConstraint *range_constraint ;
        Array range_arr(4) ;
        while (dim) {
            range_arr.InsertLast(dim) ;
            dim = dim->GetNext() ;
        }
        unsigned i ;
        FOREACH_ARRAY_ITEM_BACK(&range_arr, i, dim) {
            range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
            if (!range_constraint) {
                delete constraint ;
                constraint = 0 ;
                break ;
            }
            range_constraint->SetAsUnPacked() ;
            constraint = new VeriArrayConstraint(range_constraint, constraint) ;
        }
        return constraint ;
    }
    return 0 ; // not Class type
}
VeriConstraint *VeriSeqPropertyFormal::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned ignore_dims)
{
    // If data type exists, create constraint from data type
    if (_data_type || _dimensions) return VeriVariable::EvaluateConstraintInternal(tab, df, ignore_dims) ;

    if (IsProcessing()) return 0 ; // Recursive call
    SetIsProcessing() ; // Mark starting of processing

    // No data type, create type from actual
    VeriConstraint *constraint = (_actual) ? _actual->EvaluateConstraintInternal(tab, df): 0 ;

    ClearIsProcessing() ; // finished processing
    return constraint ;
}
VeriConstraint *VeriNamedPort::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned /*ignore_dims*/)
{
    if (IsProcessing()) return 0 ;
    SetIsProcessing() ;
    VeriConstraint *constraint = (_port_expr) ? _port_expr->EvaluateConstraintInternal(tab, df): 0 ;
    ClearIsProcessing() ;
    return constraint ;
}
VeriConstraint *VeriModportId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned /*ignore_dims*/)
{
    if (!_local_scope) return 0 ;
    Array *element_constraints = (_present_scope) ? _present_scope->GetElementConstraints(this): 0 ;
    if (!element_constraints || !_ele_ids) {
        // Modports get their variables from their scope.
        VeriIdDef *id ;
        Map *all_ids = _local_scope->DeclArea() ;
        MapIter mi ;
        unsigned evaluate_ele_constraint = 0 ;
        if (!element_constraints) {
            element_constraints = new Array(all_ids ? all_ids->Size(): 2) ;
            evaluate_ele_constraint = 1 ;
        }
        unsigned add_ele_ids = 0 ;
        if (!_ele_ids) {
            _ele_ids = new Map(STRING_HASH, all_ids ? all_ids->Size(): 2) ;
            add_ele_ids = 1 ;
        }

        VeriConstraint *element_constraint ;
        FOREACH_MAP_ITEM(all_ids, mi, 0, &id) {
            if (!id) continue ;

            // Accept only variables :
            if (!id->IsNet() && !id->IsVar()) continue ;

            // Discard types and members and functions and such (return 1 for IsVar() ?)
            if (id->IsMember() || id->IsType() || id->IsFunction() || id->IsTask()) continue ;
            if (evaluate_ele_constraint) {
                element_constraint = id->EvaluateConstraintInternal(tab, df, 0) ;
                element_constraints->InsertLast(element_constraint) ;
            }
            if (add_ele_ids) (void) _ele_ids->Insert(id->Name(), id) ;
        }
        if (evaluate_ele_constraint && _present_scope) _present_scope->AddElementConstraints(this, element_constraints) ;
    }
    return  new VeriRecordConstraint(element_constraints, _ele_ids, VERI_MODPORT, 0) ;
}
VeriConstraint *VeriInterfaceId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned /*ignore_dims*/)
{
    if (!_local_scope) return 0 ;

    Array *element_constraints = (_present_scope) ? _present_scope->GetElementConstraints(this): 0 ;
    if (!element_constraints || !_ele_ids) {
        // Interfaces get their variables from their scope.
        VeriIdDef *id ;
        Map *all_ids = _local_scope->DeclArea() ;
        MapIter mi ;
        unsigned evaluate_ele_constraint = 0 ;
        if (!element_constraints) {
            element_constraints = new Array(all_ids ? all_ids->Size(): 2) ;
            evaluate_ele_constraint = 1 ;
        }
        unsigned add_ele_ids = 0 ;
        if (!_ele_ids) {
            _ele_ids = new Map(STRING_HASH, all_ids ? all_ids->Size(): 2) ;
            add_ele_ids = 1 ;
        }

        VeriConstraint *element_constraint ;
        FOREACH_MAP_ITEM(all_ids, mi, 0, &id) {
            if (!id) continue ;

            // Accept only variables :
            if (!id->IsNet() && !id->IsVar()) continue ;

            // Discard types and members and functions and such (return 1 for IsVar() ?)
            if (id->IsMember() || id->IsType() || id->IsFunction() || id->IsTask()) continue ;
            if (evaluate_ele_constraint) {
                element_constraint = id->EvaluateConstraintInternal(tab, df, 0) ;
                element_constraints->InsertLast(element_constraint) ;
            }
            if (add_ele_ids) (void) _ele_ids->Insert(id->Name(), id) ;
        }
        if (evaluate_ele_constraint && _present_scope) _present_scope->AddElementConstraints(this, element_constraints) ;
    }
    return  new VeriRecordConstraint(element_constraints, _ele_ids, VERI_INTERFACE, 0) ;
}
VeriConstraint *VeriPrototypeId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df, unsigned ignore_dims)
{
    return (_actual_id) ? _actual_id->EvaluateConstraintInternal(tab, df, ignore_dims): 0 ;
}
unsigned VeriPrototypeId::IsUnresolvedSubprog() const
{
    return (_actual_id) ? _actual_id->IsUnresolvedSubprog(): 0 ;
}
void VeriIdDef::CheckUsageOfClassMember(const VeriScope *scope, const VeriName *from) const
{
    if (!scope) return ; // Cannot do anything without scope
    if (!IsPrivate() && !IsProtected() && !IsAutomatic()) return ; // Not a private/protected/automatic member, can be used anywhere

    // VIPER #4043 : Private(local) and protected members of a class cannot be used outside class
    VeriIdDef *class_id = scope->GetClass() ;
    if (!class_id) { // Member used outside class, error
        if (from && (IsPrivate() || IsProtected())) from->Error("access to %s member %s from outside a class context is illegal", (IsPrivate()) ? "local" : "protected", Name()) ;
        return ;
    }
    // Viper 4596 Protected member cannot be used in another class's scope which is
    // not derived from class_id
    VeriIdDef *orig_class_id = class_id ;
    unsigned is_correct_class_scope = 0 ;
    VeriName *prefix = (from) ? from->GetPrefix() : 0 ;
    VeriIdDef *prefix_class_id = 0 ;
    if (prefix) {
        VeriIdDef *prefix_id = prefix->GetId() ;
        VeriDataType *prefix_data_type = prefix_id ? prefix_id->GetDataType() : 0 ;
        VeriIdDef *prefix_data_type_id = prefix_data_type ? prefix_data_type->GetId() : 0 ;
        prefix_class_id = ((prefix_data_type_id && prefix_data_type_id->IsClass()) ? prefix_data_type_id : 0) ;

        VeriDataType *curr_data_type = prefix_class_id ? prefix_class_id->GetDataType() : 0 ;
        while (curr_data_type) {
            // The loop is required for class types defined using typedefs
            // If typedefs are used we need to get the original class type.
            prefix_class_id = curr_data_type->GetId() ;
            curr_data_type = prefix_class_id ? prefix_class_id->GetDataType() : 0 ;
        }

        // VIPER #5773: Find the actual class under which 'this' id is defined:
        VeriScope *prefix_class_scope = (prefix_class_id) ? prefix_class_id->LocalScope() : 0 ;
        while (prefix_class_scope && !prefix_class_scope->FindLocal(Name())) {
            // Go to base class of this class:
            prefix_class_scope = prefix_class_scope->GetBaseClassScope() ;
        }
        if (prefix_class_scope) prefix_class_id = prefix_class_scope->GetOwner() ;
    }

    if (class_id == prefix_class_id) is_correct_class_scope = 1 ;
    // Member used inside class
    // Local members cannot be used in subclass
    // Get the scope of class :
    VeriScope *class_scope = class_id->LocalScope() ;
    // Check if this identifier is local to this class :
    if (class_scope && (class_scope->FindLocal(Name()) != this)) {
        // This identifier is not local to this class.
        // Check whether it is declared in base classes
        class_scope = class_scope->GetBaseClassScope() ;
        while(class_scope) {
            class_id = class_scope->GetOwner() ;
            if (!class_id || !class_id->IsClass()) break ; // Upper scope is not a class
            VeriDataType *curr_data_type = class_id->GetDataType() ;
            while (curr_data_type) {
                class_id = curr_data_type->GetId() ;
                curr_data_type = class_id ? class_id->GetDataType() : 0 ;
            }
            if (class_id == prefix_class_id) is_correct_class_scope = 1 ;
            if (IsPrivate() && class_scope->FindLocal(Name()) == this) {
                // Identifier is declared in base class. That is private member of
                // one class is used in derived class. Produce error
                if (from) from->Error("access to %s member %s from %s class context is illegal", "local", Name(), "derived") ;
                break ;
            }
            class_scope = class_scope->GetBaseClassScope() ;
        }
    }

    // VIPER #6241: Check if this identifier is public/local/static to container class
    // in which this class is nested (according to IEEE 1800-2009 LRM section 8.22):
    if (!class_scope) class_scope = orig_class_id->LocalScope() ;
    if (class_scope && (class_scope->FindLocal(Name()) != this)) {
        // This identifier is not local to this class.
        // Check whether it is declared in upper classes
        class_scope = class_scope->Upper() ;
        while(class_scope) {
            class_id = class_scope->GetOwner() ;
            if (!class_id || !class_id->IsClass()) break ; // Upper scope is not a class
            VeriDataType *curr_data_type = class_id->GetDataType() ;
            while (curr_data_type) {
                class_id = curr_data_type->GetId() ;
                curr_data_type = class_id ? class_id->GetDataType() : 0 ;
            }
            // Viper 8009: Types declared in the outer class can be used in the inner class. The LRM does not have
            // any statement clarifying it but all popular simulators accept this.
            if (!IsType() && IsAutomatic() && !IsPrivate() && !IsProtected() && (class_scope->FindLocal(Name()) == this)) {
                // VIPER #6241: Identifier is declared in outer class. That is non-static, non-local,
                // non-protected member of a class is used in inner class. Produce error:
                if (from) from->Error("access to %s member %s from %s class context is illegal", "non-local, non-static", Name(), "nested") ;
                break ;
            }
            if (class_id == prefix_class_id) is_correct_class_scope = 1 ;
            class_scope = class_scope->Upper() ;
        }
    }

    if (prefix_class_id && !is_correct_class_scope && from)  from->Error("cannot access %s member %s of class %s in scope of %s", (IsPrivate()) ? "local" : "protected", Name(), prefix_class_id->Name(), orig_class_id->Name()) ;
}
void VeriIdDef::ValidateVirtualMethod(VeriIdDef *base_subprog)
{
    if (!base_subprog || (!IsFunction() && !IsTask())) return ; // Without base class method, cannot check anything
    if (IsFunction() && !base_subprog->IsFunction()) {
        Error("%s %s in subclass cannot override %s in superclass", "function", Name(), base_subprog->Name()) ;
        return ;
    }
    if (IsTask() && !base_subprog->IsTask()) {
        Error("%s %s in subclass cannot override %s in superclass", "task", Name(), base_subprog->Name()) ;
        return ;
    }
    // Check return type for function
    if (IsFunction()) {
        VeriTypeInfo *this_ret_type = CreateType(0) ;
        VeriTypeInfo *base_ret_type = base_subprog->CreateType(0) ;
        if (this_ret_type && !this_ret_type->IsTypeParameterType() && !this_ret_type->IsUnresolvedNameType() &&
            base_ret_type && !base_ret_type->IsTypeParameterType() && !base_ret_type->IsUnresolvedNameType() &&
            !this_ret_type->IsEquivalent(base_ret_type, 0, 0, 0)) {
            Error("return type of %s in subclass does not match with return type in superclass", Name()) ;
        }
        delete this_ret_type ;
        delete base_ret_type ;
    }

    // Now Check the port types :
    Array *this_ports = GetPorts() ;
    Array *base_ports = base_subprog->GetPorts() ;
    unsigned this_port_count = (this_ports) ? this_ports->Size(): 0 ;
    unsigned base_port_count = (base_ports) ? base_ports->Size(): 0 ;

    if (this_port_count != base_port_count) {
        Error("port count %d of %s in subclass does not match with port count %d in superclass", this_port_count, Name(), base_port_count) ;
        return ;
    }
    unsigned i ;
    VeriIdDef *this_port ;
    VeriIdDef *base_port ;
    FOREACH_ARRAY_ITEM(this_ports, i, this_port) {
        base_port = (base_ports) ? (VeriIdDef*)base_ports->At(i): 0 ;
        if (!base_port || !this_port) continue ;
        VeriTypeInfo *this_port_type = this_port->CreateType(0) ;
        VeriTypeInfo *base_port_type = base_port->CreateType(0) ;
        if (this_port_type && !this_port_type->IsTypeParameterType() && !this_port_type->IsUnresolvedNameType() &&
            base_port_type && !base_port_type->IsTypeParameterType() && !base_port_type->IsUnresolvedNameType() &&
            !this_port_type->IsEquivalent(base_port_type, 0, 0, 0)) {
            Error("type of argument %s of virtual method %s does not match with type of argument in superclass", this_port->Name(), Name()) ;
        }
        delete this_port_type ;
        delete base_port_type ;
    }
}

// Get extern module definition for this module identifier : Extern module can
// be a nested module or can be a module from library :
VeriModule *VeriIdDef::GetExternModule() const
{
    // Get scope of this module
    VeriScope *mod_scope = LocalScope() ;
    if (!mod_scope || !IsModule()) return 0 ; // Not module identifier

    // Check whether extern module exists for this module declaration
    // First check in upper scope of module scope. It will work for extern
    // and actual nested module declaration.
    VeriScope *upper = mod_scope->Upper() ;

    // 'upper' scope can contain both extern module identifier and actual
    // module identifier, get extern one
    Map *this_scope = (upper) ? upper->GetThisScope(): 0 ;
    MapItem *item = (this_scope) ? this_scope->GetItem(Name()): 0 ;
    VeriIdDef *extern_mod_id = 0 ;
    VeriModule *extern_mod = 0 ;
    while (item) {
        extern_mod_id = (VeriIdDef*)item->Value() ;
        extern_mod = (extern_mod_id) ? extern_mod_id->GetModule(): 0 ;
        if (extern_mod && extern_mod->GetQualifier(VERI_EXTERN)) {
            break ;
        }
        item = (this_scope) ? this_scope->GetNextSameKeyItem(item): 0 ;
        extern_mod_id = 0 ;
        extern_mod = 0 ;
    }

    if (!extern_mod || !extern_mod->GetQualifier(VERI_EXTERN)) { // This is not extern module
        extern_mod = 0 ;
        extern_mod_id = 0 ;
    }
    if (!extern_mod) {
        // Extern module declaration can be in library, check
        // Get working library :
        VeriLibrary *work_lib = veri_file::GetWorkLib() ;
        // Get extern module from same library :
        // VIPER #6706 (issue #3): Do not call VeriLibrary::GetModule() with restore == 0
        // This may insert the module name into the BB-list of the library and we may have
        // lots of issues with it later. Note that we do not need to restore the module
        // since the extern module declartion and definition should be in same file
        // (simulators behave this way for extern module linking).
        //extern_mod = (work_lib) ? work_lib->GetModule(Name(), 1, 0) : 0 ;
        // Instead directly check for the module in the module table of the library.
        // Note that we do not need to check into WorkingLibrary for TmpLibraries
        // in this particular case since the extern module declartion and definition
        // should be in same file as specified above.
        Map *modules = (work_lib) ? work_lib->GetModuleTable() : 0 ;
        extern_mod = (modules) ? (VeriModule *)modules->GetValue(Name()) : 0 ;
        if (!extern_mod || !extern_mod->GetQualifier(VERI_EXTERN)) { // This is not extern module
            extern_mod = 0 ;
            extern_mod_id = 0 ;
        }
    }
    return extern_mod ;
}

// Check port of actual module definition with corresponding port in extern module
void VeriIdDef::CheckWithExtern(VeriIdDef *port_id)
{
    if (!port_id) return ;
    // Need to check name and type
    if (!Strings::compare(Name(), port_id->Name())) {
        // Produce warning if name does not match
        Warning("name of port declaration in module definition does not match that in extern module") ;
        Warning("port %s should exactly match its corresponding port in extern module", Name()) ;
    }
    // VIPER #4808 : Check direction mismatch
    if (Dir() != port_id->Dir()) {
        Warning("direction of port %s does not match that in extern module declaration", Name()) ;
    }
    // Data type of both ports should be equivalent. If not produce warning :
    VeriTypeInfo *this_type = CreateType(0) ;
    VeriTypeInfo *other_type = port_id->CreateType(0) ;

    if (this_type && !this_type->IsUnresolvedNameType() && !this_type->IsTypeParameterType()
          && other_type && !other_type->IsUnresolvedNameType() && !other_type->IsTypeParameterType()
          && !this_type->IsEquivalent(other_type, 0, 0, 0)) {
        Warning("port %s should exactly match its corresponding port in extern module", Name()) ;
    }
    delete this_type ;
    delete other_type ;
}
unsigned VeriIdDef::CanCallWithoutArg()
{
    if (!IsFunction() && !IsTask() && !IsSequence() && !IsProperty() && !IsLetId()) return 1  ; // not function/task/sequence/property, can use without arg

    Array *ports = GetPorts() ;
    if (!ports || (ports->Size() == 0)) return 1 ; // no port

    // VIPER #5861: It is calling of function 'randomize' which is defined in std package.
    // This function can have any number of inputs. So do not check default value of port.
    if (GetFunctionType() == VERI_METHOD_RANDOMIZE) return 1 ;

    // Check whether ports contain default value
    VeriIdDef *port_id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(ports, i, port_id) {
        if (!port_id || port_id->GetInitialValue()) continue ; // have default value
        // Port does not have default value, cannot call without argument
        return 0 ;
    }
    return 1 ;
}
#ifdef VERILOG_PATHPULSE_PORTS
void VeriIdDef::CreatePathPulseValPorts(VeriScope *scope)
{
    if (!scope) return ;
    // First check whether it is pulse control specparam or not
    if (strncmp(_name, "PATHPULSE$", 10) != 0) return ;

    // It is pulse control specparam, we need to create VeriPathPulseValPorts
    // Extract input port
    char *copied_str = Strings::save(_name) ;
    char *tmp = copied_str + 10 ;

    char *str = tmp ;
    char *input_port = 0, *output_port = 0 ;
    VeriIdDef *id = 0 ;
    unsigned produce_error = 0 ;
    while (*tmp != '\0') {
        if (*tmp == '$') {
            *tmp = 0 ;
            // Find in scope whether any port of this name exists or not
            id = scope->Find(str) ;
            if (id) {
                if (!input_port) {
                    input_port = Strings::save(str) ;
                    str = tmp+1 ;
                    // Find output port
                    id = scope->Find(str) ;
                    if (id) {
                        output_port = Strings::save(str) ;
                        break ;
                    } else {
                        produce_error = 1 ;
                        break ;
                    }
                }
            } else {
                *tmp = '$' ;
            }
        }
        tmp++ ;
    }
    if (!input_port && !output_port && (*str == '\0')) {
        // Legal PATHPULSE$ without port
    } else if (input_port && output_port) {
        // Legal PATHPULSE$ with ports
    } else {
        produce_error = 1 ;
    }
    if (produce_error) {
        Error("illegal specify input/output terminal descriptor in PATHPULSE") ;
    }
    Strings::free(copied_str) ;
    VeriName *i_p = (input_port) ? new VeriIdRef(input_port) : 0 ;
    VeriName *o_p = (output_port) ? new VeriIdRef(output_port): 0 ;

    VeriPathPulseValPorts *pathpulse_ports = new VeriPathPulseValPorts(i_p, o_p) ;
    SetPathPulseValPorts(pathpulse_ports) ;
}
#endif

unsigned
VeriModportId::ContainsInterfaceMethod() const
{
    return (_tree) ? _tree->ContainsInterfaceMethod() : 0 ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
void VeriTypeId::SetVhdlPackageName(char *name)
{
    _vhdl_package_name = Strings::save(name) ;
}

void VeriTypeId::SetVhdlLibName(char *name)
{
    _vhdl_lib_name = Strings::save(name) ;
}

// VIPER #6895: True if vhdl package name and vhdl library name is set
unsigned VeriTypeId::IsConvertedVhdlType() const
{
    if (_vhdl_package_name && _vhdl_lib_name) return 1 ;
    return 0 ;
}
#endif

VeriRange *
VeriIdDef::CreateDimensions(unsigned only_unpacked) const
{
    if (only_unpacked) return 0 ; // No unpacked dimension here

    VeriDataType *data_type = GetDataType() ;
    return (data_type) ? data_type->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriVariable::CreateDimensions(unsigned only_unpacked) const
{
    VeriDataType *data_type = GetDataType() ;
    // Data type (type-ref) can have unpacked dimensions:
    VeriRange *packed_dims = (data_type /*&& !only_unpacked*/) ? data_type->CreateDimensions(only_unpacked) : 0 ;
    VeriRange *unpacked_dims = (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;

    VeriRange *result = 0 ;
    if (!packed_dims) result = unpacked_dims ;
    if (!unpacked_dims) result = packed_dims ;

    if (!result && packed_dims && unpacked_dims) {
        result = unpacked_dims ;
        while (unpacked_dims->GetNext()) unpacked_dims = unpacked_dims->GetNext() ;
        unpacked_dims->SetNext(packed_dims) ;
    }

    return result ;
}

VeriRange *
VeriInstId::CreateDimensions(unsigned only_unpacked) const
{
    return (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriParamId::CreateDimensions(unsigned only_unpacked) const
{
    if (IsProcessing()) return 0 ; // Recursive call

    VeriDataType *data_type = GetDataType() ;

    SetIsProcessing() ; // Mark starting of processing
    // VIPER #7777: Do not create dimensions from _initial_value, since actual value can change in elaboration:
    // CHECKME: For type parameters (test2.v) we still create dimensions from initial value as the same is set as data type.
    // Data type (type-ref) can have unpacked dimensions:
    VeriRange *packed_dims = (data_type /*&& !only_unpacked*/) ? data_type->CreateDimensions(only_unpacked) : 0 ; //((_initial_value) ? _initial_value->CreateDimensions(only_unpacked) : 0) ;
    VeriRange *unpacked_dims = (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;

    VeriRange *result = 0 ;
    if (!packed_dims) result = unpacked_dims ;
    if (!unpacked_dims) result = packed_dims ;

    if (!result && packed_dims && unpacked_dims) {
        result = unpacked_dims ;
        while (unpacked_dims->GetNext()) unpacked_dims = unpacked_dims->GetNext() ;
        unpacked_dims->SetNext(packed_dims) ;
    }
    ClearIsProcessing() ; // Finished processing

    return result ;
}

VeriRange *
VeriSeqPropertyFormal::CreateDimensions(unsigned only_unpacked) const
{
    if (IsProcessing()) return 0 ; // Recursive call

    VeriDataType *data_type = GetDataType() ;

    SetIsProcessing() ; // Mark starting of processing

    // Data type (type-ref) can have unpacked dimensions:
    VeriRange *packed_dims = (data_type /*&& !only_unpacked*/) ? data_type->CreateDimensions(only_unpacked) : 0 ;
    VeriRange *unpacked_dims = (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;

    VeriRange *result = 0 ;
    if (!packed_dims) result = unpacked_dims ;
    if (!unpacked_dims) result = packed_dims ;

    if (!result && packed_dims && unpacked_dims) {
        result = unpacked_dims ;
        while (unpacked_dims->GetNext()) unpacked_dims = unpacked_dims->GetNext() ;
        unpacked_dims->SetNext(packed_dims) ;
    }

    ClearIsProcessing() ; // Finished processing

    return result ;
}

VeriRange *
VeriNamedPort::CreateDimensions(unsigned only_unpacked) const
{
    if (IsProcessing()) return 0 ; // Recursive call

    SetIsProcessing() ; // Mark starting of processing
    VeriRange *result = (_port_expr) ? _port_expr->CreateDimensions(only_unpacked) : 0 ;
    ClearIsProcessing() ; // Finished processing

    return result ;
}

// VIPER #8093 : Check if id (defined in interface) can be accessed always
// using hierarchical name from outside the interface, independent of their
// presence in modport
// IEEE 1800-2012 LRM section 25.10 says objects that are not permissible to
// be listed in a modport shall remain accessible
unsigned VeriIdDef::CanBeAccessedAlways() const
{
    return 1 ; // Default behaviour, can be accessed always
}

unsigned VeriVariable::CanBeAccessedAlways() const
{
    // In relaxed checking mode var/net id is always accessible
    if (InRelaxedCheckingMode()) return 1 ;
    return 0 ; // Var/nets can be defined in modport, so cannot be accessed always
}
unsigned VeriFunctionId::CanBeAccessedAlways() const
{
    // In relaxed checking mode task/function id is always accessible
    if (InRelaxedCheckingMode()) return 1 ;
    if (IsSequence() || IsProperty()) return 1 ; // Property/sequence cannot be defined in modport
    return 0 ; // function can be defined in modport, so cannot be accessed always
}
unsigned VeriTaskId::CanBeAccessedAlways() const
{
    // In relaxed checking mode task/function id is always accessible
    if (InRelaxedCheckingMode()) return 1 ;
    return 0 ; // Task can be defined in modport, so cannot be accessed always
}

unsigned VeriTypeId::CanBeAccessedAlways() const
{
    return 1 ; // Cannot be defined in modport, so can be accessed always
}
unsigned VeriClockingId::CanBeAccessedAlways() const
{
    // In relaxed checking mode clocking id is always accessible
    if (InRelaxedCheckingMode()) return 1 ;
    return 0 ; // can be defined in modport, so cannot be accessed always
}
unsigned VeriBinsId::CanBeAccessedAlways() const
{
    return 1 ; // Cannot be defined in modport, so can be accessed always
}

void
VeriBlockId::SetIsUnnamedBlock()
{
    if (!RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) return ;

    _is_unnamed_blk = 1 ;
}

void
VeriBlockId::SetOrigName(const char *s)
{
    if (!RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) return ;

    Strings::free(_orig_name) ;
    _orig_name = Strings::save(s) ;
}
// Internal routine to return GetOrigName if exists else GetName()
const char * VeriIdDef::IdNameInternal() const
{
    const char *name = GetOrigName() ;
    if (!name) name = GetName() ;
    return name ;
}

unsigned VeriParamId::IsUsed(const SynthesisInfo * /* info */) const                          { /* VERIFIC_ASSERT(!info) ; */ return IsUsed() ; }
unsigned VeriParamId::IsAssigned(const SynthesisInfo * /* info */) const                      { /* VERIFIC_ASSERT(!info) ; */ return IsAssigned() ; }
void     VeriParamId::SetUsed(SynthesisInfo * /* info */)                                     { /* VERIFIC_ASSERT(!info) ; */ SetUsed() ; }
void     VeriParamId::SetAssigned(SynthesisInfo * /* info */)                                 { /* VERIFIC_ASSERT(!info) ; */ SetAssigned() ; }
unsigned VeriVariable::IsUsed(const SynthesisInfo * /* info */) const                         { /* VERIFIC_ASSERT(!info) ; */ return IsUsed() ; }
unsigned VeriVariable::IsAssigned(const SynthesisInfo * /* info */) const                     { /* VERIFIC_ASSERT(!info) ; */ return IsAssigned() ; }
unsigned VeriVariable::IsConcurrentUsed(const SynthesisInfo * /* info */) const               { /* VERIFIC_ASSERT(!info) ; */ return IsConcurrentUsed() ; }
unsigned VeriVariable::IsConcurrentAssigned(const SynthesisInfo * /* info */) const           { /* VERIFIC_ASSERT(!info) ; */ return IsConcurrentAssigned() ; }
unsigned VeriVariable::IsAssignedBeforeUsed(const SynthesisInfo * /* info */) const           { /* VERIFIC_ASSERT(!info) ; */ return IsAssignedBeforeUsed() ; }
unsigned VeriVariable::IsUsedBeforeAssigned(const SynthesisInfo * /* info */) const           { /* VERIFIC_ASSERT(!info) ; */ return IsUsedBeforeAssigned() ; }
unsigned VeriVariable::CanBeDualPortRam(const SynthesisInfo * /* info */) const               { /* VERIFIC_ASSERT(!info) ; */ return CanBeDualPortRam() ; }
unsigned VeriVariable::CanBeMultiPortRam(const SynthesisInfo * /* info */) const              { /* VERIFIC_ASSERT(!info) ; */ return CanBeMultiPortRam() ; }
void     VeriVariable::SetUsed(SynthesisInfo * /* info */)                                    { /* VERIFIC_ASSERT(!info) ; */ SetUsed() ; }
void     VeriVariable::SetAssigned(SynthesisInfo * /* info */)                                { /* VERIFIC_ASSERT(!info) ; */ SetAssigned() ; }
void     VeriVariable::SetConcurrentUsed(SynthesisInfo * /* info */)                          { /* VERIFIC_ASSERT(!info) ; */ SetConcurrentUsed() ; }
void     VeriVariable::SetConcurrentAssigned(SynthesisInfo * /* info */)                      { /* VERIFIC_ASSERT(!info) ; */ SetConcurrentAssigned() ; }
void     VeriVariable::SetAssignedBeforeUsed(SynthesisInfo * /* info */)                      { /* VERIFIC_ASSERT(!info) ; */ SetAssignedBeforeUsed() ; }
void     VeriVariable::UnSetAssignedBeforeUsed(SynthesisInfo * /* info */)                    { /* VERIFIC_ASSERT(!info) ; */ UnSetAssignedBeforeUsed() ; }
void     VeriVariable::SetUsedBeforeAssigned(SynthesisInfo * /* info */)                      { /* VERIFIC_ASSERT(!info) ; */ SetUsedBeforeAssigned() ; }
void     VeriVariable::UnSetUsedBeforeAssigned(SynthesisInfo * /* info */)                    { /* VERIFIC_ASSERT(!info) ; */ UnSetUsedBeforeAssigned() ; }
void     VeriVariable::SetCanBeDualPortRam(SynthesisInfo * /* info */)                        { /* VERIFIC_ASSERT(!info) ; */ SetCanBeDualPortRam() ; }
void     VeriVariable::SetCannotBeDualPortRam(SynthesisInfo * /* info */)                     { /* VERIFIC_ASSERT(!info) ; */ SetCannotBeDualPortRam() ; }
void     VeriVariable::SetCanBeMultiPortRam(SynthesisInfo * /* info */)                       { /* VERIFIC_ASSERT(!info) ; */ SetCanBeMultiPortRam() ; }
void     VeriVariable::SetCannotBeMultiPortRam(SynthesisInfo * /* info */)                    { /* VERIFIC_ASSERT(!info) ; */ SetCannotBeMultiPortRam() ; }

