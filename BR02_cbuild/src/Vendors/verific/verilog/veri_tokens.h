/*
 *
 * [ File Version : 1.71 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_TOKENS_H_
#define _VERIFIC_VERI_TOKENS_H_

// This file declares tokens defined by bison (from veri_yacc.h)
// as well as a number of tokens that are added for use in
// the Verific reader. It is included by flex, and also by most
// reader files that use the tokens.

// The 'union' between bison/flex also
// comes in the veri_yacc.h header, so we need to do some work
// here to make that valid C++.

// Don't declare the union itself in the Verific namespace, since
// lex and yacc cannot find each other's lval any more.
// But sine we are using Verific fields from Verific namespace,
// we DO need to 'use' Verific namespace so that the fields in
// the union refer to Verific classes. That's why we include VerificSystem.h here.
#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// Forward declaration of Verific tree node classes (in verific name space)
// These classes are used in the union between bison/flex (defined in veri_yacc.h)
class VeriTreeNode ;
class VeriModule ;
class VeriExpression ;
class VeriName ;
class VeriDataType ;
class VeriStatement;
class VeriModuleItem ;
class VeriTable ;
class VeriIdDef;
class VeriInstId ;
class Array;
class Map;
class VeriRange;
class VeriNetRegAssign ;
class VeriDefParamAssign ;
class VeriStrength ;
class VeriPath;
class VeriStatement ;
class VeriDelayOrEventControl ;
class VeriConfigRule ;
class VeriUseClause ;
class VeriGenerateBlock ;
class VeriPathDecl ;
class VeriAnsiPortDecl ;

class VeriFileDirLibOption ;
class VeriUserLibrary ;
class VeriScope ;
class VeriClockingDirection ;
class VeriProductionItem ;

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#if !defined YYLTYPE && !defined YYLTYPE_IS_DECLARED
// Define the custom location info type before including veri_yacc.h file
typedef struct yyltype
{
    int first_line ;
    int first_column ;

    int last_line ;
    int last_column ;
    int file_id ;
} yyltype ;

#define YYLTYPE yyltype
#define YYLTYPE_IS_DECLARED 1
#endif

// Include the tokens created by yacc/bison. Also the union is in there.
// Bison tokens run between 256 and 999.
#include "veri_yacc.h"

// Tokens added for System Calls
// This should probably be changed to an enumeration type.

// LRM Verilog 2000. Section 17.  System Tasks and Functions
//
// Display Tasks
#define VERI_SYS_CALL_DISPLAY       1000
#define VERI_SYS_CALL_DISPLAYB      1001
#define VERI_SYS_CALL_DISPLAYH      1002
#define VERI_SYS_CALL_DISPLAYO      1003
#define VERI_SYS_CALL_MONITOR       1004
#define VERI_SYS_CALL_MONITORB      1005
#define VERI_SYS_CALL_MONITORH      1006
#define VERI_SYS_CALL_MONITORO      1007
#define VERI_SYS_CALL_MONITOROFF    1008
#define VERI_SYS_CALL_STROBE        1009
#define VERI_SYS_CALL_STROBEB       1010
#define VERI_SYS_CALL_STROBEH       1011
#define VERI_SYS_CALL_STROBEO       1012
#define VERI_SYS_CALL_WRITE         1013
#define VERI_SYS_CALL_WRITEB        1014
#define VERI_SYS_CALL_WRITEH        1015
#define VERI_SYS_CALL_WRITEO        1016
#define VERI_SYS_CALL_MONITORON     1017
// File I/O
#define VERI_SYS_CALL_FCLOSE        1018
#define VERI_SYS_CALL_FDISPLAY      1019
#define VERI_SYS_CALL_FDISPLAYB     1020
#define VERI_SYS_CALL_FDISPLAYH     1021
#define VERI_SYS_CALL_FDISPLAYO     1022
#define VERI_SYS_CALL_FGETC         1023
#define VERI_SYS_CALL_FMONITOR      1024
#define VERI_SYS_CALL_FMONITORB     1025
#define VERI_SYS_CALL_FMONITORH     1026
#define VERI_SYS_CALL_FMONITORO     1027
#define VERI_SYS_CALL_READMEMB      1028
#define VERI_SYS_CALL_SWRITE        1029
#define VERI_SYS_CALL_SWRITEO       1030
#define VERI_SYS_CALL_SFORMAT       1031
#define VERI_SYS_CALL_FSCANF        1032
#define VERI_SYS_CALL_FREAD         1033
#define VERI_SYS_CALL_FSEEK         1034
#define VERI_SYS_CALL_FOPEN         1035
#define VERI_SYS_CALL_FSTROBE       1036
#define VERI_SYS_CALL_FSTROBEB      1037
#define VERI_SYS_CALL_FSTROBEH      1038
#define VERI_SYS_CALL_FSTROBEO      1039
#define VERI_SYS_CALL_UNGETC        1040
#define VERI_SYS_CALL_FERROR        1041
#define VERI_SYS_CALL_REWIND        1042
#define VERI_SYS_CALL_FWRITE        1043
#define VERI_SYS_CALL_FWRITEB       1044
#define VERI_SYS_CALL_FWRITEH       1045
#define VERI_SYS_CALL_FWRITEO       1046
#define VERI_SYS_CALL_READMEMH      1047
#define VERI_SYS_CALL_SWRITEB       1048
#define VERI_SYS_CALL_SWRITEH       1049
#define VERI_SYS_CALL_SDF_ANNOTATE  1050
#define VERI_SYS_CALL_SSCANF        1051
#define VERI_SYS_CALL_FTELL         1052
#define VERI_SYS_CALL_FGETS         1053
// Timescale tasks
#define VERI_SYS_CALL_PRINTTIMESCALE     1054
#define VERI_SYS_CALL_TIMEFORMAT         1055
// Simulation control tasks
#define VERI_SYS_CALL_FINISH             1057
#define VERI_SYS_CALL_STOP               1058
// PLA modeling tasks
#define VERI_SYS_CALL_ASYNC__AND__ARRAY  1060
#define VERI_SYS_CALL_ASYNC__NAND__ARRAY 1061
#define VERI_SYS_CALL_ASYNC__OR__ARRAY   1062
#define VERI_SYS_CALL_ASYNC__NOR__ARRAY  1063
#define VERI_SYS_CALL_SYNC__AND__ARRAY   1064
#define VERI_SYS_CALL_SYNC__NAND__ARRAY  1065
#define VERI_SYS_CALL_SYNC__OR__ARRAY    1066
#define VERI_SYS_CALL_SYNC__NOR__ARRAY   1067
#define VERI_SYS_CALL_ASYNC__AND__PLANE  1068
#define VERI_SYS_CALL_ASYNC__NAND__PLANE 1069
#define VERI_SYS_CALL_ASYNC__OR__PLANE   1070
#define VERI_SYS_CALL_ASYNC__NOR__PLANE  1071
#define VERI_SYS_CALL_SYNC__AND__PLANE   1072
#define VERI_SYS_CALL_SYNC__NAND__PLANE  1073
#define VERI_SYS_CALL_SYNC__OR__PLANE    1074
#define VERI_SYS_CALL_SYNC__NOR__PLANE   1075
// Stochastic Analysis tasks
#define VERI_SYS_CALL_Q_INITIALIZE  1077
#define VERI_SYS_CALL_Q_REMOVE      1078
#define VERI_SYS_CALL_Q_EXAM        1079
#define VERI_SYS_CALL_Q_ADD         1080
#define VERI_SYS_CALL_Q_FULL        1081
// Simulation time functions
#define VERI_SYS_CALL_REALTIME      1083
#define VERI_SYS_CALL_TIME          1084
#define VERI_SYS_CALL_STIME         1085
// Conversion functions
#define VERI_SYS_CALL_BITSTOREAL    1087
#define VERI_SYS_CALL_ITOR          1088
#define VERI_SYS_CALL_SIGNED        1089
#define VERI_SYS_CALL_REALTOBITS    1090
#define VERI_SYS_CALL_RTOI          1091
#define VERI_SYS_CALL_UNSIGNED      1092
// Probabilistic distribution functions
#define VERI_SYS_CALL_DIST_CHI_SQUARE    1094
#define VERI_SYS_CALL_DIST_EXPONENTIAL   1095
#define VERI_SYS_CALL_DIST_POISSON       1096
#define VERI_SYS_CALL_DIST_UNIFORM       1097
#define VERI_SYS_CALL_DIST_ERLANG        1098
#define VERI_SYS_CALL_DIST_NORMAL        1099
#define VERI_SYS_CALL_DIST_T             1100
#define VERI_SYS_CALL_RANDOM             1101
// Command line input
#define VERI_SYS_CALL_TEST__PLUSARGS     1103
#define VERI_SYS_CALL_VALUE__PLUSARGS    1104
// LRM Verilog 2000. Section 15 Timing Checks
#define VERI_SYS_CALL_SETUP         1107
#define VERI_SYS_CALL_HOLD          1108
#define VERI_SYS_CALL_SETUPHOLD     1109
#define VERI_SYS_CALL_RECOVERY      1110
#define VERI_SYS_CALL_REMOVAL       1111
#define VERI_SYS_CALL_RECREM        1112
#define VERI_SYS_CALL_SKEW          1113
#define VERI_SYS_CALL_TIMESKEW      1114
#define VERI_SYS_CALL_FULLSKEW      1115
#define VERI_SYS_CALL_PERIOD        1116
#define VERI_SYS_CALL_WIDTH         1117
#define VERI_SYS_CALL_NOCHANGE      1118

//
// System Verilog (3.1) system calls
//
#define VERI_SYS_CALL_ASSERTKILL    1200
#define VERI_SYS_CALL_ASSERTOFF     1201
#define VERI_SYS_CALL_ASSERTON      1202
#define VERI_SYS_CALL_BITS          1203
#define VERI_SYS_CALL_BITSTOSHORTREAL 1204
#define VERI_SYS_CALL_CAST          1205
#define VERI_SYS_CALL_COUNTONES     1206
#define VERI_SYS_CALL_DIMENSIONS    1207
#define VERI_SYS_CALL_ERROR         1208
#define VERI_SYS_CALL_EXIT          1209
#define VERI_SYS_CALL_FATAL         1210
#define VERI_SYS_CALL_FELL          1211
#define VERI_SYS_CALL_HIGH          1212
#define VERI_SYS_CALL_INCREMENT     1213
#define VERI_SYS_CALL_INFO          1214
#define VERI_SYS_CALL_INSET         1215 // 3.1 only ? Does not show up in 3.1a.
#define VERI_SYS_CALL_INSETZ        1216 // 3.1 only ? Does not show up in 3.1a.
#define VERI_SYS_CALL_ISUNKNOWN     1217
#define VERI_SYS_CALL_LEFT          1218
#define VERI_SYS_CALL_LENGTH        1219 // 3.1 only ? Does not show up in 3.1a.
#define VERI_SYS_CALL_LOW           1220
#define VERI_SYS_CALL_ONEHOT        1221
#define VERI_SYS_CALL_PAST          1222
#define VERI_SYS_CALL_RIGHT         1223
#define VERI_SYS_CALL_ROOT          1224
#define VERI_SYS_CALL_ROSE          1225
#define VERI_SYS_CALL_SHORTREALTOBITS 1226
#define VERI_SYS_CALL_SRANDOM       1227
#define VERI_SYS_CALL_STABLE        1228
#define VERI_SYS_CALL_URANDOM       1229
#define VERI_SYS_CALL_URANDOM_RANGE 1230
#define VERI_SYS_CALL_WARNING       1231

// System Verilog 3.1a system call additions :
#define VERI_SYS_CALL_ONEHOT0          1232
#define VERI_SYS_CALL_COVERAGE_CONTROL 1233
#define VERI_SYS_CALL_COVERAGE_MERGE   1234
#define VERI_SYS_CALL_COVERAGE_SAVE    1235
#define VERI_SYS_CALL_GET_COVERAGE     1236
#define VERI_SYS_CALL_ISUNBOUNDED      1237
#define VERI_SYS_CALL_LOAD_COVERAGE_DB 1238
// #define VERI_SYS_CALL_READMEMB         1239 // already defined in 95
// #define VERI_SYS_CALL_READMEMH         1240 // already defined in 95
#define VERI_SYS_CALL_SAMPLED          1241
#define VERI_SYS_CALL_SET_COVERAGE_DB_NAME 1242
#define VERI_SYS_CALL_SIZE             1243
#define VERI_SYS_CALL_TYPENAME         1245
#define VERI_SYS_CALL_TYPEOF           1246
#define VERI_SYS_CALL_UNIT             1247
#define VERI_SYS_CALL_WRITEMEMB        1248
#define VERI_SYS_CALL_WRITEMEMH        1249

// System Verilog (3.1) methods
#define VERI_METHOD_ATOBIN          1251
#define VERI_METHOD_ATOHEX          1252
#define VERI_METHOD_ATOI            1253
#define VERI_METHOD_ATOOCT          1254
#define VERI_METHOD_ATOREAL         1255
#define VERI_METHOD_BACK            1256
#define VERI_METHOD_BINTOA          1257
#define VERI_METHOD_CLEAR           1258
#define VERI_METHOD_COMPARE         1259
#define VERI_METHOD_CONSTRAINT_MODE 1260
#define VERI_METHOD_DATA            1261
#define VERI_METHOD_DELETE          1262
#define VERI_METHOD_EMPTY           1263
#define VERI_METHOD_EQ              1264
#define VERI_METHOD_ERASE           1265
#define VERI_METHOD_ERASE_RANGE     1266
#define VERI_METHOD_EXISTS          1267
#define VERI_METHOD_FIRST           1268
#define VERI_METHOD_FRONT           1269
#define VERI_METHOD_GETC            1270
#define VERI_METHOD_HEXTOA          1271
#define VERI_METHOD_ICOMPARE        1272
#define VERI_METHOD_INSERT          1273
#define VERI_METHOD_INSERT_RANGE    1274
#define VERI_METHOD_ITOA            1275
#define VERI_METHOD_LAST            1276
#define VERI_METHOD_LEN             1277
#define VERI_METHOD_NAME            1278
#define VERI_METHOD_NEQ             1279
#define VERI_METHOD_NEXT            1280
#define VERI_METHOD_NUM             1281
#define VERI_METHOD_OCTTOA          1282
#define VERI_METHOD_POP_BACK        1283
#define VERI_METHOD_POP_FRONT       1284
#define VERI_METHOD_POST_RANDOMIZE  1285
#define VERI_METHOD_PRE_RANDOMIZE   1286
#define VERI_METHOD_PREV            1287
#define VERI_METHOD_PURGE           1288
#define VERI_METHOD_PUSH_BACK       1289
#define VERI_METHOD_PUSH_FRONT      1290
#define VERI_METHOD_PUTC            1291
#define VERI_METHOD_RAND_MODE       1292
#define VERI_METHOD_RANDOMIZE       1293
#define VERI_METHOD_REALTOA         1294
#define VERI_METHOD_SET             1295
#define VERI_METHOD_SIZE            1296
#define VERI_METHOD_START           1297
#define VERI_METHOD_SUBSTR          1298
#define VERI_METHOD_SWAP            1299
#define VERI_METHOD_TOLOWER         1300
#define VERI_METHOD_TOUPPER         1301

// System Verilog 3.1a method additions :
#define VERI_METHOD_AND               1302
#define VERI_METHOD_AWAIT             1303
#define VERI_METHOD_FIND_FIRST        1304
#define VERI_METHOD_FIND_FIRST_INDEX  1305
#define VERI_METHOD_FIND_INDEX        1306
#define VERI_METHOD_FIND_LAST         1307
#define VERI_METHOD_FIND_LAST_INDEX   1308
#define VERI_METHOD_FINISH            1309
#define VERI_METHOD_FIND              1310
#define VERI_METHOD_GET_RANDSTATE     1311
#define VERI_METHOD_KILL              1312
#define VERI_METHOD_MAX               1313
#define VERI_METHOD_MIN               1314
#define VERI_METHOD_NEW               1315
#define VERI_METHOD_OR                1316
#define VERI_METHOD_PROCESS           1317
#define VERI_METHOD_PRODUCT           1318
#define VERI_METHOD_RESUME            1319
#define VERI_METHOD_REVERSE           1320
#define VERI_METHOD_RSORT             1321
#define VERI_METHOD_SELF              1322
#define VERI_METHOD_SET_RANDSTATE     1323
#define VERI_METHOD_SHUFFLE           1324
#define VERI_METHOD_SIE               1325
#define VERI_METHOD_SORT              1326
#define VERI_METHOD_STATUS            1327
#define VERI_METHOD_SUM               1328
#define VERI_METHOD_SUSPEND           1329
#define VERI_METHOD_UNIQUE            1330
#define VERI_METHOD_UNIQUE_INDEX      1331
#define VERI_METHOD_XOR               1332

// System Verilog methods, found Ad-Hoc in LRM :
#define VERI_METHOD_ENDED             1333 // LRM 17.7.10 / 17.12.5
#define VERI_METHOD_MATCHED           1334 // LRM 17.7.10 / 17.12.5

// System Verilog Std 1800 additions
#define VERI_SYS_CALL_UNPACKED_DIMENSIONS 1340
#define VERI_SYS_CALL_COVERAGE_GET_MAX    1341
#define VERI_SYS_CALL_COVERAGE_GET        1342
#define VERI_SYS_CALL_DUMPVARS            1343 // Belongs to verilog. Has been put here to avoid renumbering.
// Added here because 'next' is a op as well as func
// System Verilog Std 1800 addition
// VIPER 2546 : Built-in method index
#define VERI_METHOD_INDEX                 1345
// Viper 2625
// The different AND/OR op for sequence and property
#define VERI_SEQ_AND                      1346
#define VERI_PROPERTY_AND                 1347
#define VERI_SEQ_OR                       1348
#define VERI_PROPERTY_OR                  1349
#define VERI_METHOD_GET_COVERAGE          1350
#define VERI_METHOD_GET_INST_COVERAGE     1351
#define VERI_METHOD_SET_INST_NAME         1352
#define VERI_METHOD_STOP                  1353
#define VERI_METHOD_SAMPLE                1354
#define VERI_METHOD_TRIGGERED             1355
// VIPER #3460 : dumpvars is already present.. add rest of vcd system functions
// all these belong to Verilog.. Added at the end to avoid re-numbering
#define VERI_SYS_CALL_DUMPALL             1356
#define VERI_SYS_CALL_DUMPON              1357
#define VERI_SYS_CALL_DUMPOFF             1358
#define VERI_SYS_CALL_DUMPFILE            1359
#define VERI_SYS_CALL_DUMPLIMIT           1360
#define VERI_SYS_CALL_DUMPFLUSH           1361
#define VERI_SYS_CALL_DUMPPORTS           1362
#define VERI_SYS_CALL_DUMPPORTSON         1363
#define VERI_SYS_CALL_DUMPPORTSOFF        1364
#define VERI_SYS_CALL_DUMPPORTSALL        1365
#define VERI_SYS_CALL_DUMPPORTSLIMIT      1366
#define VERI_SYS_CALL_DUMPPORTSFLUSH      1367

// System task/function aded in P1800-2009 LRM
// I/O tasks and functions (21.3)
#define VERI_SYS_CALL_FFLUSH              1368
#define VERI_SYS_CALL_FEOF                1369
// Moved these function to VERILOG_2000 mode
// LRM section 17.11.1/17.11.2 Viper 6807
// Math functions (20.8)
#define VERI_SYS_CALL_CLOG2               1370
#define VERI_SYS_CALL_LN                  1371
#define VERI_SYS_CALL_LOG10               1372
#define VERI_SYS_CALL_EXP                 1373
#define VERI_SYS_CALL_SQRT                1374
#define VERI_SYS_CALL_POW                 1375
#define VERI_SYS_CALL_FLOOR               1376
#define VERI_SYS_CALL_CEIL                1377
#define VERI_SYS_CALL_SIN                 1378
#define VERI_SYS_CALL_COS                 1379
#define VERI_SYS_CALL_TAN                 1380
#define VERI_SYS_CALL_ASIN                1381
#define VERI_SYS_CALL_ACOS                1382
#define VERI_SYS_CALL_ATAN                1383
#define VERI_SYS_CALL_ATAN2               1384
#define VERI_SYS_CALL_HYPOT               1385
#define VERI_SYS_CALL_SINH                1386
#define VERI_SYS_CALL_COSH                1387
#define VERI_SYS_CALL_TANH                1388
#define VERI_SYS_CALL_ASINH               1389
#define VERI_SYS_CALL_ACOSH               1390
#define VERI_SYS_CALL_ATANH               1391
// Assertion action control tasks (20.12)
#define VERI_SYS_CALL_ASSERTPASSON        1392
#define VERI_SYS_CALL_ASSERTPASSOFF       1393
#define VERI_SYS_CALL_ASSERTFAILON        1394
#define VERI_SYS_CALL_ASSERTFAILOFF       1395
#define VERI_SYS_CALL_ASSERTNONVACUOUSON  1396
#define VERI_SYS_CALL_ASSERTVACUOUSOFF    1397
// Assertion functions (20.13)
#define VERI_SYS_CALL_PAST_GCLK           1398
#define VERI_SYS_CALL_FELL_GCLK           1399
#define VERI_SYS_CALL_CHANGED_GCLK        1400
#define VERI_SYS_CALL_RISING_GCLK         1401
#define VERI_SYS_CALL_STEADY_GCLK         1402
#define VERI_SYS_CALL_CHANGED             1403
#define VERI_SYS_CALL_ROSE_GCLK           1404
#define VERI_SYS_CALL_STABLE_GCLK         1405
#define VERI_SYS_CALL_FUTURE_GCLK         1406
#define VERI_SYS_CALL_FALLING_GCLK        1407
#define VERI_SYS_CALL_CHANGING_GCLK       1408

// System function aded in P1800-2009 LRM
// VIPER #5710:
#define VERI_SYS_CALL_GLOBAL_CLOCK          1442
#define VERI_SYS_CALL_PAST_GLOBAL_CLOCK     1443
#define VERI_SYS_CALL_ROSE_GLOBAL_CLOCK     1444
#define VERI_SYS_CALL_FELL_GLOBAL_CLOCK     1445
#define VERI_SYS_CALL_STABLE_GLOBAL_CLOCK   1446
#define VERI_SYS_CALL_CHANGED_GLOBAL_CLOCK  1447
#define VERI_SYS_CALL_FUTURE_GLOBAL_CLOCK   1448
#define VERI_SYS_CALL_RISING_GLOBAL_CLOCK   1449
#define VERI_SYS_CALL_FALLING_GLOBAL_CLOCK  1450
#define VERI_SYS_CALL_STEADY_GLOBAL_CLOCK   1451
#define VERI_SYS_CALL_CHANGING_GLOBAL_CLOCK 1452

// Internal operator for assertion synthesis
#define VERI_SEQ_SP_AND                     1453
#define VERI_SEQ_FM_AND                     1454
#define VERI_SP_NOT                         1455
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #6895:
#define VERI_UNCONSTRAINT                   1456
#endif

#endif // #ifndef _VERIFIC_VERI_TOKENS_H_
