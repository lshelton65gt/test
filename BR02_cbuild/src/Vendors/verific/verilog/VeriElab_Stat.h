/*
 *
 * [ File Version : 1.172 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_STATIC_ELAB_H_
#define _VERIFIC_VERI_STATIC_ELAB_H_

#include "VeriCompileFlags.h"
#include "Array.h"
#include "VeriId.h" // for VeriModuleId ; needed below for VeriAliasModuleId

// Compile flag to print debug information during static elaboration
// 1. print the total amount of memory used at different stages
// of static elaboration.
// 2. for every instantiation, prints its overwritten parameters and other
//    information to understand why this instantiated module needs to be uniquified.
// 3. prints the name of the modules which are instantiated multiple times and
//    hierarchy under multiple instantiations are different. These modules need
//    to be uniquified
// 4. prints the name of the modules copied in elaboration.
//#define VERI_STATIC_ELAB_DEBUG

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VeriScope ;
class VeriIdDef ;
class VeriExpression ;
class VeriName ;

// Forward declaration of pseudo hierarchical tree node class
class VeriPseudoTreeNode;
class VeriConfigInfo ;
class VeriCellInfo ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
class VhdlIdDef ;
class VhdlPrimaryUnit ;
class VhdlScope ;
#endif

/* -------------------------------------------------------------- */

/*
   This class stores the information of a hierarchical identifier.  The name of
   the identifier along with its declaration scope is stored in this class.
   After resolving hierarchical identifier we can extract resolved id from this
   class along with the pseudo full grown tree node where the first element of
   hierarchical id is defined.
*/
class VFC_DLL_PORT HierIdWithDeclScope
{
public:
    // The name of the first argument for following constructors is the hierarchical name.
    // Second argument is the declaration scope of the identifier.
    HierIdWithDeclScope(VeriExpression *id, VeriScope *decl_scope) ;

    HierIdWithDeclScope(const HierIdWithDeclScope &id) ;

    virtual ~HierIdWithDeclScope() ;

private:
    // Prevent compiler from defining the following
    HierIdWithDeclScope() ;                                       // Purposely leave unimplemented
    HierIdWithDeclScope& operator=(const HierIdWithDeclScope &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    VeriExpression *        GetNameRef() const                              { return _name_node ; }  // Returns the VeriName parse tree node representing this hierarchical id
    VeriScope *             GetDeclarationScope()                           { return _decl_scope ;} // Returns the declaration scope of this identifier

    void                    SetDeclarationScope(VeriScope *s)               { _decl_scope = s; }    // Set the identifier declaration scope
    void                    SetNameRef(VeriExpression *new_name) ; // Set the VeriExpression

    virtual VeriExpression *GetValue() const                                { return 0 ; }
    virtual void            SetValue(VeriExpression * /*v*/)                { }

    virtual unsigned        IsDefparam() const                              { return 0 ; } // VIPER 2128, Check if this hier id is for defparam

    virtual unsigned        IsBindTarget() const                            { return 0 ; }
    void                    SetFirstMatchModule(unsigned set = 1)           { _is_first_id_module = set ; }     // Set first element referred by this id as module
    unsigned                IsFirstMatchModule() const                      { return _is_first_id_module ; }    // Returns 1 if first element of hier id is module
    void                    SetFirstMatchInstance(unsigned set = 1)         { _is_first_id_inst = set ; }       // Set first element referred by this id is the instance
    unsigned                IsFirstMatchInstance() const                    { return _is_first_id_inst ; }      // Returns 1 if first element of hier id is the instance
    unsigned                IsFirstMatchOtherModule() const                 { return _first_id_other_module ; } // Returns 1 if first element of hier id is module, but not '_first_ele_node' specified module
    void                    SetFirstMatchOtherModule()                      { _first_id_other_module = 1 ; }

    void                    SetFirstReferencedNode(VeriPseudoTreeNode *n)   { _first_ele_node = n ; }    // Set first element referred pseudo node
    VeriPseudoTreeNode *    GetFirstReferencedNode() const                  { return _first_ele_node ; } // Get first element referred pseudo node

    void                    SetLastReferencedNode(VeriPseudoTreeNode *n) ; //   { _last_ele_node = n ; }    // Set last element referred pseudo node
    VeriPseudoTreeNode *    GetLastReferencedNode() const                   { return _last_ele_node ; } // Get last element referred pseudo node

    void                    SetResolvedIddef(VeriIdDef *id)                 { _resolved_id = id ; }     // Set resolved iddef
    VeriIdDef *             GetResolvedIddef() const                        { return _resolved_id ; }   // Get resolved iddef

    void                    SetPrefixScope(VeriScope *scope)                { _prefix_scope = scope ; } // Set prefix scope
    VeriScope *             GetPrefixScope() const                          { return _prefix_scope ; }  // Get prefix code

    void                    SetUnresolvedName(char *name) ; // absorb the name
    const char *            GetUnresolvedName() const                       { return _unresolved_name ; }
    void                    IncreaseUnresolveCount()                        { _unresolve_count++ ; }
    unsigned                GetUnresolvedCount() const                      { return _unresolve_count ; }
    void                    ResetUnresolveCount()                           { _unresolve_count = 0 ; }
    virtual void            ResetFlags() ;

    // VIPER 2128 : Set this identifier as to be ignored
    void                    SetAsIgnored()                                  { _to_be_ignore = 1 ; }
    // VIPER 2128 : Return 1 if this identifier is to be ignored
    unsigned                IsTobeIgnored() const                           { return _to_be_ignore ; }
    // VIPER #2128 : Return 1 if this defparam contains array instance reference
    unsigned                IsArrayInstanceRef() const                      { return _is_array_instance ; }
    void                    SetArrayInstanceRef()                           { _is_array_instance = 1 ; }
    virtual void            Process(unsigned /*is_sup_error*/, VeriPseudoTreeNode * /*expr_node*/, Map * /* mod_map_tab*/) { }
    virtual HierIdWithDeclScope *Copy()                                     { return new HierIdWithDeclScope(*this) ; } // Copy normal hierarchical identifier

    virtual void            SetInsideGenerate()                             { }
    virtual unsigned        IsInsideGenerate() const                        { return 0 ; }
    virtual VeriDefParam *  GetDefparam() const                             { return 0 ; }
    virtual void            SetDefparam(VeriDefParam * /*def*/)             { }
    virtual VeriBindDirective *   GetBindDirective() const                  { return 0 ; }
    virtual void                  SetBindDirective(VeriBindDirective * /*b*/)    { }
    void                    SetResolvedInfo(VeriIdDef *first_id, VeriPseudoTreeNode *first_matched_node, VeriScope *prefix_scope, unsigned depth) ;
    void                    SetNotProduceError()                            { _not_produce_error = 1 ; }
    unsigned                GetNotProduceError() const                      { return _not_produce_error ; }
    void                    ResetNotProduceError()                          { _not_produce_error = 0 ; }
    virtual void                MarkAsSpecial()                  {}
    virtual unsigned            IsSpecial() const                { return 0 ; }
    virtual void                ResetMarkAsSpecial()             {}
    virtual void                SetDepth(unsigned )              {}
    virtual unsigned            GetDepth() const                 { return 0 ; }
    virtual void                AddResolvedId(VeriIdDef * /*id*/)     {}
    virtual Array *             TakeResolvedIds()                { return 0 ; }
    virtual unsigned            ResolvedIdCount() const          { return 0 ; }
    virtual void                ChangeDefparam(VeriDefParam * /*n*/)  {}
    void                        SetFirstMatchInstanceWithoutNode() { _first_id_inst_without_node = 1 ; }
    unsigned                    IsFirstMatchInstanceWithoutNode() const { return _first_id_inst_without_node ; }
    void                        SetFromContAssignLhs() { _cont_assign_lhs = 1 ; }
    unsigned                    IsFromContAssignLhs() const { return _cont_assign_lhs ; }

    void                        SetCrossLangId()       { _cross_lang_id = 1 ; }
    unsigned                    IsCrossLangId() const  { return _cross_lang_id ; }
    void                        SetCoverpointRhsId()   { _coverpoint_rhs = 1 ; }
    unsigned                    IsCoverpointRhsId() const    { return _coverpoint_rhs ; }
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    void                        SetVhdlPrefixScope(VhdlScope *s) { _prefix_vhdl_scope = s ; }
    VhdlScope *                 GetVhdlPrefixScope() const       { return _prefix_vhdl_scope ; }
    void                        SetVhdlResolvedId(VhdlIdDef *id) { _resolved_vhdl_id = id ; }
    VhdlIdDef *                 GetVhdlResolvedId() const        { return _resolved_vhdl_id ; }
    unsigned                    FindSuffixScope(VhdlIdDef *id, unsigned depth) ;
    VhdlIdDef *                 FindSuffixId(const VhdlScope *prefix_scope, const char *name) ;
    VhdlIdDef *                 FindSuffixIdInternal(const VhdlScope *prefix_scope, const char *name) const ;
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual unsigned            IsCrossLangNotSupported() const   { return 0 ; }
    virtual void                SetCrossLangNotSupported()        { }
protected:
    VeriScope          *_decl_scope ;           // declaration scope
    VeriPseudoTreeNode *_first_ele_node ;       // hierarchical tree node
                                                // representing first href element
    VeriPseudoTreeNode *_last_ele_node ;        // hierarchical tree node
                                                // representing last referred module.
    VeriIdDef          *_resolved_id ;          // resolved identifier
    VeriExpression     *_name_node ;            // parse tree node representing this href
    VeriScope          *_prefix_scope ;         // scope of the prefix id
    char               *_unresolved_name ;      // unresolved portion of the hier id
    unsigned            _unresolve_count:22 ;   // number of unresolved element in this hier id
    unsigned            _to_be_ignore:1 ;       // flag to indicate if this id is to be ignored
    unsigned            _is_array_instance:1 ;  // VIPER 2128 : flag to indicate this id is defparam having reference to array instance
    unsigned            _first_id_other_module:1 ; // VIPER #3318 : Indicates that first element is module from lib, not '_first_ele_node' specified module
    unsigned            _not_produce_error:1 ;  // flag to indicate that we should not produce error if this is unresolved
    unsigned            _first_id_inst_without_node:1 ; // Flag to indicate that first identifier is instance, but no node is created for that instance
    unsigned            _cont_assign_lhs:1 ;    // flag to mark that this hier id is from lvalue oc continuous assign
    unsigned            _is_first_id_module:1 ; // flag to indicate that the first object
                                                // referenced by this hier id is module
    unsigned            _is_first_id_inst:1 ;   // VIPER #3726: flag to indicate that the first object
                                                // referenced by this hier id is an instance
    unsigned            _cross_lang_id:1 ;      // Flag to mark that this hier id contains elements of vhdl also (VIPER #5624)
    unsigned            _coverpoint_rhs:1 ;     // Hier id is at the rhs of coverpoint
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    VhdlScope          *_prefix_vhdl_scope ;    // Scope of prefix vhdl object
    VhdlIdDef          *_resolved_vhdl_id ;     // resolved id of vhdl object
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
} ; // class HierIdWithDeclScope

/* -------------------------------------------------------------- */

/*
   Class to represent defparam (hierarchical identifier with expression)
*/
class VFC_DLL_PORT DefparamId : public HierIdWithDeclScope
{
public:
    DefparamId(VeriName *id, VeriScope *decl_scope, VeriExpression *value, VeriDefParam *defparam) ;

    DefparamId(const DefparamId &id) ;

    virtual ~DefparamId() ;

private:
    // Prevent compiler from defining the following
    DefparamId() ;                              // Purposely leave unimplemented
    DefparamId& operator=(const DefparamId &) ; // Purposely leave unimplemented

public:
    virtual VeriExpression *    GetValue() const                 { return _value ; }
    virtual void                SetValue(VeriExpression *v) ;

    virtual unsigned            IsDefparam() const               { return 1 ; } // VIPER 2128, Check if this hier id is for defparam
    virtual void                Process(unsigned is_sup_error, VeriPseudoTreeNode *expr_node, Map * /*mod_map_tab*/) ;
    virtual HierIdWithDeclScope * Copy()                         { return new DefparamId(*this) ; } // Copy defparam id
    virtual void                SetInsideGenerate()              { _inside_generate = 1 ; }
    virtual unsigned            IsInsideGenerate() const         { return _inside_generate ; }

    // Virtual routines to get/set defparam. Required to update this id when containing module is copied
    virtual VeriDefParam *      GetDefparam() const              { return _defparam ; }
    virtual void                SetDefparam(VeriDefParam *def)   { _defparam = def ; }
    virtual void                MarkAsSpecial()                  { _special_defparam = 1 ;}
    virtual void                ResetMarkAsSpecial()             { _special_defparam = 0 ; }
    virtual unsigned            IsSpecial() const                { return _special_defparam ; }
    virtual void                SetDepth(unsigned d)             { _depth = d ; }
    virtual unsigned            GetDepth() const                 { return _depth ; }
    virtual void                ResetFlags() ;
    virtual unsigned            ResolvedIdCount() const ;
    virtual void                AddResolvedId(VeriIdDef *id) ;
    virtual Array *             TakeResolvedIds() ;
    virtual void                ChangeDefparam(VeriDefParam *n) ;
    virtual unsigned            IsCrossLangNotSupported() const { return _cross_lang_not_supported ; }
    virtual void                SetCrossLangNotSupported()      { _cross_lang_not_supported = 1 ; }
private :
    VeriExpression *    _value ;  // Defparam specific value
    VeriDefParam   *    _defparam ; // Store the defparam here so that we can delete this without traversing module items
    unsigned            _inside_generate:1 ;  // Flag to indicate this defparam is inside generate/within bind directive specific instantiation's hierarchy
    unsigned            _special_defparam:1 ; // Specially created defparam to check whether it can change value outside of hierarchy
    unsigned            _depth:29 ; // Depth of defparam name upto which it is resolved
    // VIPER #6909 :  Flag to mark that this defparam contains cross language references,
    // but cannot be supported yet. So these should be kept in parse tree with resolved ids
    unsigned            _cross_lang_not_supported:1 ;
    Array *             _ids ; // array of resolved VeriIdDefs for each element of defparam name
} ; // class DefparamId

/* -------------------------------------------------------------- */
/*
   Class to represent bind directive having target scope as hier id
*/
class VFC_DLL_PORT HierIdForBind : public HierIdWithDeclScope
{
public:
    HierIdForBind(VeriName *target_scope, VeriScope *decl_scope, VeriBindDirective *bind) ;

    HierIdForBind(const HierIdForBind &id) ;

    virtual ~HierIdForBind() ;

private:
    // Prevent compiler from defining the following
    HierIdForBind() ;                                 // Purposely leave unimplemented
    HierIdForBind& operator=(const HierIdForBind &) ; // Purposely leave unimplemented
public:
    virtual unsigned            IsBindTarget() const                        { return 1 ; }
    virtual void                Process(unsigned is_sup_error, VeriPseudoTreeNode * /*expr_node*/, Map *mod_map_tab) ;
    virtual HierIdWithDeclScope * Copy()                                    { return new HierIdForBind(*this) ; } // Copy hier id

    // Virtual routines to get/set bind directive. Required to update this id when containing module is copied
    virtual VeriBindDirective *   GetBindDirective() const                  { return _bind_directive ; }
    virtual void                  SetBindDirective(VeriBindDirective *b)    { _bind_directive = b ; }
private :
    VeriBindDirective *_bind_directive ; // Bind Directive
    Set *_processed_libraries ; // Processed libraries Viper 5039
} ; // class HierIdForBind

/* -------------------------------------------------------------- */

// VIPER 2128 : Add this class in header file
/*
   This class represents the node of the full grown hierarchical tree created during static elaboration.
   Each node represents a module. The root nodes of the full grown hierarchical tree represent the top
   level modules and the nodes under the root represent the instantiated modules.
*/
class VFC_DLL_PORT VeriPseudoTreeNode
{
public:
    explicit VeriPseudoTreeNode(VeriModuleInstantiation *inst = 0, VeriModuleItem *design = 0, VeriConfigInfo *cfg_info = 0) ;
    VeriPseudoTreeNode(VeriPseudoTreeNode &node, Map *mod_map_tab, unsigned is_top) ;
    virtual ~VeriPseudoTreeNode() ;

private:
    // Prevent compiler from defining the following
    VeriPseudoTreeNode(const VeriPseudoTreeNode &) ;            // Purposely leave unimplemented
    VeriPseudoTreeNode& operator=(const VeriPseudoTreeNode &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Get the parent tree node
    VeriPseudoTreeNode *          GetParent() const                     { return _upper ; } // return the parent pointer

    // Set the parent tree node
    void                          SetParent(VeriPseudoTreeNode *p) ;

    // Add the argument specific node as the child of this node
    void                          AddChild(const VeriPseudoTreeNode *p) ;
    // Remove the argument specific node from the children of this node
    unsigned                      RemoveChild(const VeriPseudoTreeNode *p) ;

    // Set the module that is represented by this node
    void                          SetInstantiatedModule(VeriModuleItem *design) { _design = design ; }
    VeriModuleItem *              GetDesign() const { return _design ; }
    virtual VeriPseudoTreeNode *  Copy(Map *node_map) ;

    // Get the module represented by this node
    //VeriModule *                  GetInstantiatedModule() const ;
    VeriLibrary *                 GetLibrary() const ;
    VeriModule *                  GetContainingModule() const ; // return _design if this is node for module, else look in parent
    virtual unsigned              IsBlackBox() const ;

    // Returns the children. Map of instance name vs VeriPseudoTreeNode*
    Map *                         GetChildren() const                   { return _children ; }
    void                          SetChildren(Map *children)            ; // Viper 6087

    virtual void                  SetNext(VeriPseudoTreeNode * /*n*/)   { } // Set next pointer
    virtual VeriPseudoTreeNode *  GetNext() const                       { return 0 ; } // Get next pointer
    virtual void                  SetAsHead()                           { } // Virtual catcher
    virtual unsigned              IsSplitDone()                         { return 0 ; }
    virtual void                  SetSplitDone()                        { }
    virtual unsigned              IsArrayInstance() const               { return 0 ; }
    virtual unsigned              IsUnresolvedNode() const              { return 0 ; }

    // Get the name of instantiated unit
    virtual const char *          GetInstantiatedUnitName() const ;
    const char *                  GetBlackBoxModuleName() const ;

    // Returns true if the addition of this node(instantiation of the representing module)
    // creates recursive instantiation.
    unsigned                      IsCreateLoop() const ;

    // Get the instance name
    virtual const char *          GetInstanceName() const ;
#ifdef VERI_STATIC_ELAB_DEBUG
    void                          PrintInstancePath() const ;
    void                          PrintHierarchy(char *instance_path, unsigned depth) ;
    void                          PrintParameterAndHierIds() ;
#endif
    virtual void                  SetAsElaborated()  { }

    // Get the library where this node specified module resides
    VeriLibrary *                 GetParentLibrary() const ;

    // Add the information of a parameter. First argument is the parameter identifier.
    // Second argument indicates if this parameter is declared in module scope (those are overwriting from instantiation) or
    // in other inner scope. Third argument indicates if parameter/enum has initial value and fourth argument indicates if
    // first argument specific id is enum literal
    void                          AddParamInfo(const VeriIdDef *id, unsigned is_inner, unsigned has_init_val = 1, unsigned is_enum = 0, VeriScope *scope = 0) ; // VIPER 2662 : Pass declaration scope to create full path name of parameter

    // Returns the default/overridden value of the argument specific parameter.
    VeriExpression *              GetParamValue(const VeriIdDef* id) const ;

    // Return 1 if argument specific parameter is overwritten
    unsigned                      IsParamOverwritten(const VeriIdDef* id) const ;

    // Returns the pseudo tree node where the parameter is declared.
    VeriPseudoTreeNode *          GetParamScope(const VeriIdDef* id) const ;

    // If the argument specific instance is instantiated in the module represented by this node, true is returned.
    unsigned                      Exist(const char *name) const ;

    // Hierarchical identifier storage routines.
    void                          AddHierId(VeriName *id, VeriScope *scope, unsigned from_cont_assign_lhs, unsigned from_coverpoint=0) ;
    void                          Add(HierIdWithDeclScope *id) ;
    void                          AddDefparam(VeriDefParam *defparam, VeriScope *scope, unsigned inside_generate) ;

    // Change the value of the first argument specific parameter. Second argument is the new value of the parameter.
    // The third argument represents the module from where the value of the parameter is changed by defparam or instantiation.
    void                          OverwriteParameter(VeriIdDef *pId, VeriExpression *val, VeriPseudoTreeNode *expr_parent, unsigned is_user_overwrite=0) ;
    // Internal routine to change parameter value
    void                          ChangeParamInfo(const VeriIdDef *pId, VeriExpression *val) const ;

    // Return the signature of this node.
    char *                        GetSignature() ;

    // Set the signature
    void                          SetSignature(char *sig, unsigned reset_param_change_flag = 0) ;

    // Get the instantiation. This node represents this instantiated module.
    VeriModuleInstantiation *     GetInstantiation() const              { return _inst ; }

    // Set the instantiation
    void                          SetInstantiation(VeriModuleInstantiation *inst) { _inst = inst;}

    // Return the instance identifier
    VeriIdDef*                    GetInstanceId() const ;

    // Process hierarchical identifiers for all hiarchical tree nodes.
    void                          ProcessHierIds(Map *top_mod_array, Map *mod_map_tab, unsigned b_error_sup, unsigned ignore_ids_from_cont_assign_lhs=0) ;

    // If the argument specific expression contains parameter(s)/type id(s), this method
    // returns 1, 0 otherwise.
    static unsigned               IsContainParamId(VeriTreeNode *param_expr) ;
    static unsigned               IsContainNonLocalParam(VeriTreeNode *param_expr) ; // VIPER #6050

    // Process the defparams for all hiarchical tree nodes.
    void                          ProcessDefparams(Map *top_nodes, Map *mod_map_tab) ;

    // Create the signature for all hiarchical tree nodes.
    void                          CreateFirstSignature(const Map *tab, const Map *top_mod_array, unsigned has_path_name_att, unsigned consider_hier_ids) ;

    void                          CleanUp() ; // VIPER #7508
    char *                        CreateInterfaceSignature(const char *org_sig, Map *mod_map_tab, Map *top_mod_array) ;
    VeriIdDef *                   ProcessActual(VeriExpression *actual_conn, Map *top_mod_array, Map *mod_map_tab, VeriIdDef **interface_id, VeriIdDef *formal_id, VeriPseudoTreeNode **interface_node, Array *gen_blk_ids) ; // VIPER #6695 : Add new argument to get elaboration generate generate blocks
    const char *                  ProcessFormal(const VeriIdDef *formal_id, VeriLibrary *actual_interface_lib, const VeriPseudoTreeNode *actual_interface_node, VeriIdDef **formal_interface, unsigned &is_generic, Array *gen_blk_ids) const ; // VIPER #6695 : Add new argument to get elaboration generate generate blocks
    unsigned                      IsEqual(VeriPseudoTreeNode *node1, const Map *tab) ;

    // Returns the depth of the pseudo node.
    unsigned                      GetDepth() const;

    // Sets the bits of the _unique_int to a unique number.
    void                          SetUniqueInt(unsigned i)              { _unique_int = i; }
    // Returns the unique number.
    unsigned                      GetUniqueInt() const                  { return _unique_int; }

    // Set the argument specific module as the module represented by this node
    void                          SetModule(VeriModuleItem *new_mod, Map *mod_map_tab, VeriMapForCopy *id_map_tab, Map *copied_mod_vs_id_map_tab) ;

    // Set the actual values of the parameters for the module represented by this node.
    // Also flattens the interface type ansiportdecls having multiple ids
    void                          ModifyParamAndAnsiPortDecl(Map *mod_map_tab, Array *mod_array, Map *top_mod_nodes, const Map *mod_vs_id_map_tab) const ;

    // Returns the table containing VeriIdDef* as key and ParamValueInfo* as value.
    Map *                         GetParamInfoTab() const               { return _param_info_tab ; }

    // Returns the table containing hierarchical identifiers
    Set *                         GetHierIdTab() const                  { return _hier_id_tab ; }

    // Modify the name of the hierarchical identifiers (if required) for all pseudo tree nodes.
    unsigned                      ModifyHierIdsNCollectSpuriousNestedMods(Set *unique_designs, Map *mod_map_tab, const Map *top_nodes, Set *spurious_mods, Set *instantiated_mods) ;

    // Change instantiations so that they become the instantiations of unique modules
    virtual void                  ModifyInstantiations(Map *mod_map_tab, Map *top_mod_pseudo_nodes, Set *processed_inst) ;

    // This routine carries out some semantic checks on the instantiation represented
    // by this node. Most of them are related to formal-actual pair.
    // Made this routine static so that we can call it for any instance in any situation:
    virtual void                  CheckInstantiation(const VeriModuleInstantiation *inst) const ;
    // Viper 4861. Made this check through a recursive function over expressions of a VeriConcat
    static void                   CheckOutputPortConnection(const VeriIdDef *formal_id, const VeriExpression *actual_expr) ;
    void                          CheckSemantics(VeriModule *design, const VeriModuleInstantiation *instance) const ;

    void                          ArrangeBackPointers(const VeriPseudoTreeNode *orig_node, const Map *node_map) const ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    static void                   CheckVhdlSemantics(const VhdlPrimaryUnit *unit, const VeriModuleInstantiation *instance) ;
    static void                   CheckVhdlOutputPortConnection(const VhdlIdDef *formal_id, const VeriExpression *actual_expr) ;
#endif

    static void                   CheckContAssignLvalue(VeriNetRegAssign *assign, const Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes) ; // VIPER #5615
    static void                   AddContAssign(const VeriContinuousAssign *cont_assign, const Array *nodes) ; // VIPER #5625
    static void                   AddNetAlias(const VeriNetAlias *net_alias, const Array *nodes) ; // VIPER #7962

    virtual void                  SplitArrayInstanceTerminals(Map * /*top_mod_array*/, Map * /*mod_map_tab*/, Set * /*insts*/, Array * /*widths*/, Array * /*dirs*/) ; // VIPER 2128 : split terminals of array instance, VIPER 3247 : split terminals with mixed language
    static VeriDataDecl *         CreateVirtualInterface(const VeriIdDef *inst, const Array *inst_ids, const VeriModule *instantiated_mod, const VeriModuleItem *mod) ;

    // Returns true if the any node of the hierarchical tree is modified.
    unsigned                      IsTreeChanged() const ;

    // Returns true if the node is modified.
    unsigned                      IsAnyFlagChange() const               { return _change_param || _is_changed_hier_id || (_is_hierarchy_changed && !IsBlackBox()) ; }

    // Returns 1 if any new hierarchical Identifier is added.
    unsigned                      IsHierIdAdded() const                 { return _is_changed_hier_id ; }
    void                          SetHierIdAdded()                      { _is_changed_hier_id = 1 ; }
    unsigned                      IsHierarchyChanged() const            { return _is_hierarchy_changed ; }
    void                          SetHierarchyChanged()                 { _is_hierarchy_changed = 1 ; }
    unsigned                      IsHierIdsTobeConsidered() ; // Returns 1 if hier identifiers of this module are to be considered in signature creation
    void                          PostElabProcess(Set *designs) const ;
    // Routine to replace particular constant expressions if compile switch VERILOG_REPLACE_CONST_EXPRS is on
    void                          ReplaceConstantExpr(Set *unique_mods) const ;
    // Replace the node representing array instance to nodes representing instances made by array instance elaboration
    void                          CreateArrayInstanceNodes(const Array *instances, const VeriIdDef *orig_inst_id, Map *mod_map_tab, Map *top_mod_nodes) ;

    void                          UpdateTree(const Array *gen_items, Map *mod_map_tab, Map *top_mod_nodes) const ; // Update this node from first argument specific module items

    // Check whether the second argument specific name matches with the name of any uniquely instantiated module
    VeriPseudoTreeNode *          CheckMatchWithUniquelyInstantiatedDesign(const char *str, const Map *mod_map_tab, VeriIdDef **first_id) const ;

    // Check whether the second argument specific name matches with the name of any top level modules.
    VeriPseudoTreeNode *          CheckMatchWithAnyTopLevel(const char* str, const Map *top_mod_array, VeriIdDef **first_id) const ;

    // Check whether the second argument specific name matches with any module name, function/task name or instance name of any node
    // traversing hierarchically upwards.
    VeriPseudoTreeNode *          CheckMatchInHierarchicalParent(const char *str, Map *top_mod_array, Map *mod_map_tab, VeriIdDef **first_id) const ;

    // Search the first argument specific name traversing hierarchically upwards.
    virtual VeriPseudoTreeNode *  SearchNode(const char *name, Map *top_mod_array, Map *mod_map_tab, VeriIdDef **first_id) ;

    // Check whether the second argument specific name matches with any instances instantiated in this module
    VeriPseudoTreeNode *          CheckMatchWithInstanceName(const char *str, VeriIdDef **inst_id) const ;
    VeriPseudoTreeNode *          CheckElementMatchWithInstanceName(const char *str, VeriIdDef **inst_id) const ;
    // This checks the instance name hierarchically downwards
    VeriPseudoTreeNode *          CheckMatchHierarchyWithInstanceName(const char *str, VeriIdDef **inst_id) const ;

    // Static routines for signature creation
    //static void                   CreateActualSignature(const Map *tab) ;
    static void                   ProcessChildFirst(const VeriPseudoTreeNode *node, const Map *tab) ;
    static void                   CheckEquality(const Set *node_array, const Map *tab, const VeriModuleItem *mod) ;

    // Static routines for array instance elaboration
    static unsigned               GetGateTermDirection(unsigned pos, unsigned term_size, const VeriGateInstantiation *inst) ;
    static unsigned               CreateDecl(VeriScope *scope, unsigned term_width, unsigned term_sign, VeriExpression *rhs_expr, VeriIdDef **id, Array &decls) ;
    static char *                 GetUniqueName(const VeriScope *scope, const char *name) ;
    static VeriInstId *           CreateInstanceId(const VeriIdDef *inst_id, int instance_id_idx, Array *new_term_list, VeriScope *mod_scope) ;

    // Chnages interface type ports (formal and/or actual)
    void                          ChangeInterfacePortType(Set *done_designs, Map *top_mod_array, Map *mod_map_tab) ;

    // Routines to support bind directive
    void                          AddBindInstance(const VeriModuleItem *instance, unsigned through_inst = 0) ; // Add bind instance to this node
    void                          AddBindDirective(VeriBindDirective *bind, VeriScope *scope) ; // Add bind directive by creating hier id node
    void                          DefineBindInstance(const VeriIdDef *inst_id, Map *mod_map_tab, Map *top_mod_nodes) const ; // Add 'inst_id' to 'this' node specific design unit
    // VIPER #6123 : Pass the scope where type is used to start finding type from there :
    VeriScope *                   ResolveType(VeriName *type_name, VeriIdDef **id, unsigned produce_error=0, VeriScope *scope =0) const ;
    void                          CollectSpuriousNestedModules(Set *orig_mods, Set *instantiated_mods) ;
    unsigned                      IsUnderHierarchy(const VeriPseudoTreeNode *root) ; // Check if 'this' is under the hierarchy created by 'root'
    void                          ResetProcessedInterface() { _processed_interface = 0 ; }
    unsigned                      IsProcessedInterfaceSet() const { return _processed_interface ; } // VIPER #7913
    // VIPER #7913
    void                          SetCannotCreateSignature() { _cannot_create_sig = 1 ; }
    unsigned                      IsCannotCreateSignature() const { return _cannot_create_sig ; }

    // Addition for configuration processing:
    void                          SetConfigurationInfo(VeriConfigInfo *cfg_info)   { _config_info = cfg_info ; }
    VeriConfigInfo *              GetConfigurationInfo() const                     { return _config_info ; }
    // Virtual catcher for VeriUnresolvedNode:
    virtual void                  SetInstanceName(const char *name) ; // Set the name of the instance for VeriUnresolvedNode
    virtual void                  SetUseClause(VeriUseClause * /* use_clause */)   {} // Set the use clasue for this node specified in an instance clause
    virtual void                  SetLiblist(Array * /* liblist */)                {} // Set the liblist for this node specified in an instance clause
    virtual Array *               GetLiblist() const ;
    virtual VeriUseClause *       GetUseClause() const                             { return 0 ; }
    virtual unsigned              IsBlackBoxArrayInstance() const       { return 0 ; }
    virtual void                  SetBlackboxArrayInstance()                 { } // Set black-box
    virtual void                  SetIsArrayInstance()                  { }
    void                          RemoveReferenceFrom(Map *mod_map_tab) const ;
    void                          ProcessClass(VeriModuleItem *class_decl, Map *mod_map_tab, unsigned prev_unresolved_class) const ;
    static void                   ElaborateClassDataType(VeriTreeNode *tree, VeriModuleItem *container, Map *mod_map_tab) ;

    // The following 2 routines help to process static members of uninstantiated classes:
    static void                   ProcessUnInstantiatedClassesInt(const VeriScope *scope, VeriModuleItem *container, Map *mod_map_tab) ;
    void                          ProcessUninstantiatedClasses(Set *done, Map *mod_map_tab) ;
    static VeriExpression *       GetEvaluatedValue(VeriIdDef *formal, const VeriExpression *actual_or_default, VeriScope *p, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab) ;
public :
    VeriExpression *              FindIdAndEvaluate(VeriIdDef *p_id, VeriExpression *expr, Set *duplet_tab, VeriPseudoTreeNode *expr_node, Map *top_nodes, Map *mod_map_tab) const ;
protected:
    // Process defparam to change the corresponding parameter value.
    void                          ProcessDefparamId(HierIdWithDeclScope *id, unsigned suppress_error) ;

    // The evaluation of any parameter can be dependent on itself. This circular dependency is checked by this routine.
    void                          CheckCircularDependency(VeriIdDef *pid, VeriExpression *expr, Set *duplet_tab, VeriPseudoTreeNode *expr_node, Map *top_nodes, Map *mod_map_tab) ;

    // The following two routines evaluate the value of a parameter
    //VeriExpression *              FindIdAndEvaluate(VeriIdDef *p_id, VeriExpression *expr, Set *duplet_tab, VeriPseudoTreeNode *expr_node, const Map *top_nodes, const Map *mod_map_tab) const ;
    VeriExpression *              EvaluateParamValue(VeriIdDef *p_id, const VeriExpression *expr, const VeriPseudoTreeNode *expr_node, const Map *dependent_param_ids) const ;

    // If any parameter of this node is overridden, this routine returns true.
    unsigned                      IsParamChanged() const                { return _change_param ;}

    // Recreate the Map containing parameter information. When the module specified by this node
    // is changed by SetModule routine, the parameter identifiers (VeriIdDef) stored in this
    // node are changed. So the Map containing parameter information is recreated to change
    // the parameter identifiers (VeriIdDef)
    void                          RecreateParamInfoTab(VeriMapForCopy *id_map_tab, const Map *copied_mod_vs_id_map_tab) ;

    // Recreate the Map containing information about hierarchical identifiers. When the module
    // specified by this node is changed by SetModule routine, the declaration scope of the
    // hierarchical identifiers are to be changed. So the Map containing information about
    // hierarchical identifier is recreated to change the declaration scope
    void                          RecreateHierIdTab(VeriMapForCopy *id_map_tab) ;
    unsigned                      IsChildChangedByConfig() const ; // Routine to check whether hierarchy under this node is modified by config

    // Static function used as the compare function for Map. In static elaboration each
    // hierarchical identifier is stored creating an object of the class HierIdWithDeclScope
    // where the name of the identifier along with its declaration scope is stored. These
    // class objects are stored in a Map. So to find a particular hierarchical identifier
    // from the Map both the name and the declaration scope are to be compared. The following
    // function compare both the name and the scope of the identifier.
    static unsigned long          IdWithDeclScopeCmp(const void* v0, const void* v1) ;

    // Static function used as the compare function for Set. This function compares
    // the parameter identifier along with the pseudo tree node where this parameter is declared.
    // To evaluate the value of each parameter, it helps to check circular dependency.
    static unsigned long          IdWithPseudoNodeComp(const void* v0, const void* v1) ;

public :
    unsigned                      IsBindInstanceAdded() const  { return _bind_instance_added ; }
    unsigned                      HasCrossModuleRef() const    { return _has_cross_mod_ref ; }
    void                          SetHasCrossModuleRef(unsigned s)       { _has_cross_mod_ref = s ; }
    // Methods for vhdl node (vhdl instance in verilog)
    virtual unsigned              IsVhdlNode() const            { return 0 ; }
    virtual unsigned              IsBlockNode() const ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    const char *                  GetLocalPath() const          { return _local_inst_path ; } // Return local instance path
    VhdlIdDef *                   PushVerilogPath(VhdlIdDef *vhdl_unit_id, const char *arch_name) ;
    virtual VhdlPrimaryUnit *     GetVhdlUnit() const           { return 0 ; }
    virtual VhdlScope *           GetNodeScope() const          { return 0 ; }
    virtual unsigned              IsAffectedByPathName() const  { return 0 ; }
    virtual void                  SetVhdlUnit(VhdlPrimaryUnit * /*unit*/, Map * /*unit_map_tab*/) {}
    virtual unsigned              AddParameter(VhdlIdDef * /*generic_id*/, VeriExpression * /*value*/) { return 0 ; }
#endif
    static void                   AddPathName(const VeriIdDef *id, char *path_name) ;
    void                          SetLocalPath(const VeriIdDef *inst_ele) ;
    static void                   PropagateAssociatedPathName(const VeriIdDef *orig_id, const VeriIdDef *new_id, int instance_id_idx) ;
    virtual unsigned              ElaborateVhdl(Map *tab, Map *top_nodes) ; // do nothing for verilog nodes
    virtual void                  SetArchName(const char*  /*ar*/) {}

    // Uniquification of hier name path specific
public :
    void                          AddHierName(const VeriExpression *name) ;
    Set *                         GetHierNameList() const  { return _hier_name_list ; }
    unsigned                      HasSameHierIds(const VeriPseudoTreeNode *other) const ;
    void                          ResetHierNameList() ;
public:
    static unsigned           _depth ;                    // Store depth of the tree, needed to catch recursion
    static unsigned           _max_depth ;                // Maximum depth allowed for recursion

protected:
    VeriPseudoTreeNode *      _upper ;                    // parent pointer
    Map *                     _children ;                   // (char*->VeriPseudoTreeNode*) Map for instance identifier vs pseudo node for that instance
    VeriModuleItem *          _design ;                   // the module represented by this node
    VeriModuleInstantiation * _inst ;                     // instantiatiation represented by this node
    Map *                     _param_info_tab ;           // (VeriIdDef*->ParamValueInfo*) Map for parameter information storage
    Set *                     _hier_id_tab ;              // Set of HierIdWithDeclScope* (hierarchical identifier)
    VeriConfigInfo *          _config_info ;              // The configuration information that we need to expand the tree
    char *                    _sig ;                      // signature of this node
    unsigned                  _change_param:1 ;           // flag, indicates whether the parameter values of this node is changed
    unsigned                  _is_changed_hier_id:1 ;     // flag, indicates whether any hierarchical identifier is added.
    unsigned                  _is_hierarchy_changed:1 ;   // flag, indicates whether any children/enum declaration/configuration is added/processed.
    unsigned                  _processed_interface:1 ;    // flag, indicates whether the interface type ports need to be considered while creating signature
    unsigned                  _bind_instance_added:1 ;    // flag, indicates whether bind instance is added to this node
    unsigned                  _bind_instance_through_inst:1 ; // flag, indicates bind instance is added using instance id as target in bind construct
    unsigned                  _has_cross_mod_ref :1 ;        // flag, indicates module instance has reference out side the module scope
    // These 25 bits are used to create the unique signature of a pesudo node, where the node has to be uniquified
    // because of its child signature contribution.
    unsigned                  _unique_int:24 ;
    unsigned                  _cannot_create_sig:1 ; // flag to indicate that signature of this node cannot be created now (VIPER #7913)
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    char *                    _local_inst_path ; // VIPER #5805: Local path of this instance: I or gen_blk:I:
    char *                    _inst_name ; // Name of instance for verilog module instantiated in vhdl
#endif
    Set *                     _hier_name_list ; // List of VeriName* touching this node
} ; // class VeriPseudoTreeNode

/* -------------------------------------------------------------- */

/*
   This class represent a node of full grown hierarchical tree for an unrolled array instance
*/
class VFC_DLL_PORT VeriArrayInstanceNode : public VeriPseudoTreeNode
{
public:
    explicit VeriArrayInstanceNode(VeriModuleInstantiation *inst = 0, VeriModuleItem *design = 0, VeriConfigInfo *cfg_info = 0) ;
    VeriArrayInstanceNode(VeriArrayInstanceNode &node, Map *mod_map_tab, unsigned is_top) ;
    virtual ~VeriArrayInstanceNode() ;

private:
    // Prevent compiler from defining the following
    VeriArrayInstanceNode(const VeriArrayInstanceNode &) ;            // Purposely leave unimplemented
    VeriArrayInstanceNode& operator=(const VeriArrayInstanceNode &) ; // Purposely leave unimplemented

public:
    virtual unsigned              IsArrayInstance() const               { return _is_array_inst ; }
    virtual VeriPseudoTreeNode *  Copy(Map *node_map) ;
    virtual void                  SetNext(VeriPseudoTreeNode *n)        { _next = n ; }     // Set next pointer
    virtual VeriPseudoTreeNode *  GetNext() const                       { return _next ; }  // Get next pointer
    virtual void                  SplitArrayInstanceTerminals(Map *top_mod_array, Map *mod_map_tab, Set *insts, Array *widths, Array *dirs) ;                 // Split terminals of array instance, VIPER 3247 : split terminals with mixed language
    virtual void                  SetAsHead()                           { _is_head = 1 ; }  // Set this node as head of link list
    virtual unsigned              IsSplitDone()                         { return _is_split_done ; }
    virtual void                  SetSplitDone()                        { _is_split_done = 1 ; }
    virtual unsigned              IsBlackBoxArrayInstance() const       { return _is_blackbox ; }
    virtual void                  SetBlackboxArrayInstance()            { _is_blackbox = 1 ; } // Set black-box
    virtual void                  SetIsArrayInstance()                  { _is_array_inst = 1 ; }
private:
    VeriPseudoTreeNode *_next ;       // LinkList of nodes those are created for each instance of an array-instance
    unsigned            _is_head:1 ;     // Indicate head of linked list
    unsigned            _is_split_done:1 ; // Flag to check whether terminals are already split or not
    unsigned            _is_blackbox:1 ; // Flag to check it is black-box instance
    // This flag is needed as VeriVhdlNode is derived from VeriArrayInstanceNode
    // and VeriVhdlNode can behave as normal or array instance node
    unsigned            _is_array_inst:1 ; // Flag to check whether it is array instance node
} ; // class VeriArrayInstanceNode

/* -------------------------------------------------------------- */

/*
   Class to represent the nodes created in the first step for the partial tree.
   This node does not have any instance pointer set. At least the name of the instance
   is known and at most the name of the instance and instantiated module and the
   VeriModule * instantiated module is known to this node.
*/
class VFC_DLL_PORT VeriUnresolvedNode : public VeriPseudoTreeNode
{
public:
    explicit VeriUnresolvedNode(const char *inst_name, VeriConfigInfo *cfg_info = 0, VeriCellInfo *cell_info = 0) ;
    VeriUnresolvedNode(VeriUnresolvedNode &node, Map *mod_map_tab, unsigned is_top) ;
    virtual ~VeriUnresolvedNode() ;

private:
    // Prevent compiler from defining the following
    VeriUnresolvedNode() ;                                      // Purposely leave unimplemented
    VeriUnresolvedNode(const VeriUnresolvedNode &) ;            // Purposely leave unimplemented
    VeriUnresolvedNode &operator=(const VeriUnresolvedNode &) ; // Purposely leave unimplemented

public:
    virtual VeriPseudoTreeNode *  Copy(Map *node_map) ;
    virtual void                  SetInstanceName(const char *name) ;
    virtual void                  SetUseClause(VeriUseClause *use_clause) ;
    virtual void                  SetLiblist(Array *liblist) ;

    virtual const char *          GetInstanceName() const ;
    VeriCellInfo *                GetCellInfo() const                   { return _cell_info ; }
    virtual VeriUseClause *       GetUseClause() const ;
    virtual Array *               GetLiblist() const ;
    virtual unsigned              IsUnresolvedNode() const              { return 1 ; }

protected:
    char *         _instance_name ;  // Unresolved name of the instance
    VeriCellInfo * _cell_info ;  // Stores the use clasue/library list applicable to this node
} ; // class VeriUnresolvedNode

/* -------------------------------------------------------------- */

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// VIPER #5805: Class to represent the vhdl instance instantiated in verilog.
// It will have pointer to instantiated vhdl_unit.
class VFC_DLL_PORT VeriVhdlNode : public VeriArrayInstanceNode
{
public:
    explicit VeriVhdlNode(VeriModuleInstantiation *inst = 0, const VhdlPrimaryUnit *design = 0, VeriConfigInfo *cfg_info = 0) ;
    explicit VeriVhdlNode(VhdlScope *block_scope) ;
    VeriVhdlNode(VeriVhdlNode &node, Map *mod_map_tab, unsigned is_top) ;
    virtual ~VeriVhdlNode() ;

private:
    // Prevent compiler from defining the following
    //VeriVhdlNode() ;                                // Purposely leave unimplemented
    VeriVhdlNode(const VeriVhdlNode &) ;            // Purposely leave unimplemented
    VeriVhdlNode &operator=(const VeriVhdlNode &) ; // Purposely leave unimplemented

public:
    virtual unsigned           IsVhdlNode() const   { return 1 ; }
    virtual unsigned           IsBlockNode() const  { return _is_unit_node ? 0 : 1 ; }
    virtual VeriPseudoTreeNode *  Copy(Map *node_map) ;
    virtual VhdlPrimaryUnit *  GetVhdlUnit() const ;
    virtual VhdlScope *        GetNodeScope() const { return _block_scope ; }
    virtual unsigned           IsAffectedByPathName() const ;
    virtual unsigned           ElaborateVhdl(Map *tab, Map *top_nodes) ; // Elaborate vhdl unit
    unsigned                   ElaborateVhdlInternal(Map *tab, Map *top_nodes) ; // Elaborate vhdl unit
    // Change instantiations so that they become the instantiations of unique modules
    virtual void               ModifyInstantiations(Map *mod_map_tab, Map *top_mod_pseudo_nodes, Set *processed_inst) ;
    virtual void               SetArchName(const char* ar) ;
    virtual unsigned           IsBlackBox() const ;
    virtual void               SetVhdlUnit(VhdlPrimaryUnit *unit, Map *unit_map_tab) ;
    virtual void               CheckInstantiation(const VeriModuleInstantiation *inst) const ;
    // Get the name of instantiated unit
    virtual const char *       GetInstantiatedUnitName() const ;
    virtual void               SetAsElaborated()  { _is_elaborated = 1 ; }

    virtual unsigned           AddParameter(VhdlIdDef *generic_id, VeriExpression *value) ;
    char *                     ActualImage(const char *formal_name, unsigned pos, VeriExpression *actual, Map *mod_map_tab, Map *top_nodes) const ;
    virtual VeriPseudoTreeNode *  SearchNode(const char *name, Map *top_mod_array, Map *mod_map_tab, VeriIdDef **first_id) ;
protected:
    VhdlScope       *_block_scope ; // Scope for primary unit or block
    char            *_arch_name ; // instantiated architecture name if any
    unsigned         _is_elaborated:1 ; //
    unsigned         _is_unit_node:1 ; // node for instance/block
    Map             *_generic_map ; // Map of generic_id vs VeriExpression (VIPER #6909)
} ; // class VeriVhdlNode

/* -------------------------------------------------------------- */
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION

/*
   Class to store the cell info - the 'use' clasue/library list from which
   we need to pick up a module. Please, see the notes below:
*/
class VFC_DLL_PORT VeriCellInfo
{
public:
    VeriCellInfo(VeriUseClause *use_clause, Array *liblist) ;
    VeriCellInfo(const VeriCellInfo &) ;
    virtual ~VeriCellInfo() ;

private:
    // Prevent compiler from defining the following
    VeriCellInfo() ;                                // Purposely leave unimplemented
    VeriCellInfo &operator=(const VeriCellInfo &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    void                          SetUseClause(VeriUseClause *use_clause) ;
    void                          SetLiblist(Array *liblist) ;
    VeriUseClause *               GetUseClause() const                  { return _use_clause ; }
    Array *                       GetLiblist() const                    { return _liblist ; }

    VeriCellInfo *                Copy() const                          { return new VeriCellInfo(*this) ; }

protected:
    VeriUseClause *_use_clause ; // Use clause specified for the cell, does not own
    Array *        _liblist ;    // Library list specified for the cell, does not own
    // NOTE: Currently only one of these two are set (last one survives)
    // We give more priority to the last binding clasue. For example:
    // config cfg ;
    //    design test ;
    //    instance test.I use bot1 ;
    //    instance test.I liblist lib1 ;
    // endconfig
    // Here we will use the library list to resolve test.I. Other tools also seem to do
    // the same. IEEE 1364 LRM section 13.3.1.6 on "The use clause" seems to give more
    // preference on the use clasue over the library list, but it is not very clear.
} ; // class VeriCellInfo

/* -------------------------------------------------------------- */

/*
   Stores the char * original module name Vs. VeriCellInfo * mapping.
   VeriCellInfo stores the 'use' clause/library list to resolve the instantiated
   module. The new module should be used in place of the original module.
   Also stores the configuration back pointer from which this is created.
*/
class VFC_DLL_PORT VeriConfigInfo
{
public:
    explicit VeriConfigInfo(VeriConfiguration *config) ;
    virtual ~VeriConfigInfo() ;

private:
    // Prevent compiler from defining the following
    VeriConfigInfo() ;                                  // Purposely leave unimplemented
    VeriConfigInfo(const VeriConfigInfo &) ;            // Purposely leave unimplemented
    VeriConfigInfo &operator=(const VeriConfigInfo &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    Array *                       GetDefaultLiblist() const                  { return _default_liblist ; }
    void                          SetDefaultLiblist(Array *default_liblist)  { _default_liblist = default_liblist ; }
    VeriConfiguration *           GetConfiguration() const                   { return _originating_config ; }
    const char *                  GetConfigurationName() const ;

    VeriUseClause *               GetUseClause(const char *cell_name) const ;
    Array *                       GetLiblist(const char *cell_name) const ;
    unsigned                      InsertCellMapping(const char *cell_name, VeriUseClause *use_clause, Array *liblist) ;

protected:
    VeriConfiguration *_originating_config ; // The configuration from which it is extracted, does not own
    Map *              _cell_mapping ;       // The cell mappings: char * name vs. VeriCellInfo *, owns
    Array *            _default_liblist ;    // The current default liblist for the whole configuration, does not own
} ; // class VeriConfigInfo

/* -------------------------------------------------------------- */
////////////////////////////////////////////////////////////////////////////
////                       Internal class definitions                ///////
////////////////////////////////////////////////////////////////////////////

/*
   VeriStaticElaborator is a placeholder to statically elaborate the verilog design.
   Typically calling sequence is as follows :

       VeriStaticElaborator elabObj ;
       elabObj.Elaborate() ;

   But user can use module_ptr->StaticElaborate() to elaborate a module or
   library_ptr->StaticElaborate() to elaborate a library.

   In the absence of any argument in Elaborate method, top level modules are extracted from
   veri_file's modules and those top level modules are elaborated. If the argument is a pointer
   to a VeriModule, that module is considered as top level and that is elaborated.

   The Elaborate method will create the full grown hierarchical tree of the top level modules.
   After creating the tree it will resolve all the hierarchical identifiers and function/task
   calls. For each pseudo tree node a signature is created considering its parameter values and
   hierarchical identifiers. The instantiated and parameterized modules are copied for different
   signatures and their parameter values are modified depending on parameter overwriting.
   Those copied modules become the new masters of the instantiations. The generate statements are
   elaborated for each module as the parameter values are now fixed. The generate elaboration can
   extend the hierarchical tree or can add some new defparams or hierarchical identifiers. So the
   hierarchical identifier processing, signature creating and module copying are repeated till
   all generate constructs are elaborated and the hierarchical tree becomes full grown.
*/
class VFC_DLL_PORT VeriStaticElaborator
{
public:
    // Elaborate the argument specific module
    VeriStaticElaborator(const VeriModule *module, Map *parameters);

    // VIPER #3099: Elaborate multiple top modules
    // VIPER #6545 : Allow parameter association as argument
    VeriStaticElaborator(const Array * top_mods, Map *parameters);

    // Elaborate all modules of veri_file.
    explicit VeriStaticElaborator(VeriLibrary *library);

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // Elaborate vhdl unit
    VeriStaticElaborator(VhdlPrimaryUnit *unit, const char *arch_name) ;
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION

    ~VeriStaticElaborator();

private:
    // Prevent compiler from defining the following
    VeriStaticElaborator() ;                                        // Purposely leave unimplemented
    VeriStaticElaborator(const VeriStaticElaborator &) ;            // Purposely leave unimplemented
    VeriStaticElaborator& operator=(const VeriStaticElaborator &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Elaborate top level vhdl unit
    void         ElaborateVhdlTop() ;
    // Elaborte the top level module/modules
    void         ElaborateVerilogTopLevelModules() ;

    Array *      CheckTopLevelModules(const Array *top_modules) const ; // VIPER #8357
    // Check whether the argument specific module is a top level. While copying the modules for
    // different signature, this check is performed. If the module is top level, it is not copied.
    unsigned     IsTopLevel(const VeriModuleItem *org_mod) const ;

    // Signature creation routines :
    void         CreateInitialSignature(unsigned consider_hier_ids, Map *tmp_mod_map_tab) const ;
    void         CreateSignature() ;
    void         CreateNonUniqueSignature(unsigned donot_optimize_hier_ids, Map *tmp_mod_map_tab) ;
    void         CreateUniqueSignature(Map *tmp_mod_map_tab) const ;
    void         CreateSignatureConsideringHierIds(Map *interface_node_map) ;
    void         CreateActualSignature(Map *tmp_mod_map_tab=0) const ;
    unsigned     CheckHierIdEquality(VeriPseudoTreeNode *node1, VeriPseudoTreeNode *node2, const Map *node_map_tab, const Map *tab, const Map *sig_num_map) const ;
    void         PopulateEquivalentNodesMap(const VeriPseudoTreeNode* node1, const VeriPseudoTreeNode* node2, Map* node_map_tab) ;
    unsigned     TraverseEquivalentHierarchy(const Map *node_map_tab, const Map *tab, Map *sig_num_map, const Map *sorted_node_map) const ;
    void         ProcessNodesHavingSameSig(const Array *module_array, const Map *sig_map_tab, Map *sig_num_map) ;
    void         ProcessNodesFromBottom(VeriPseudoTreeNode *node, const Map *sig_map_tab, Map *parent_map, Map *sig_num_map) const ;

    // Add a new configuration info into the Array (called from constructor of VeriConfigInfo)
    static void  AddConfigurationInfo(const VeriConfigInfo *info) ;
    static void  ResetConfigInfoMap() ;

    // Insert the top level modules into the given Array that became top level after elaboration.
    void         GetTopLevelModules(Array *top_mods) const ;

    void         GetTopLevelUnits(Array *top_units) const ; // VIPER #7727 : Populate top vhdl units

    // Routines that are required to check whether we need to copy a module irrespective of parameter overwrites etc:
    static void      SetAlwaysCopyModule(const VeriModuleItem *module) ;
    static unsigned  IsAlwaysCopyModule(const VeriModuleItem *module) ;
    static void      ResetAlwaysCopyModuleMap() ;
    unsigned         Process(Array *mod_array, Map *top_mod_pseudo_nodes, Map *mod_map_tab, VeriLibrary *library, Set *done_designs) const ;

    // Reset/deallocate all static members
    static void      ResetStatics() ;

    // Set/get APIs for current node setting
    void             SetCurrentNode(VeriPseudoTreeNode *n) { _current_node = n ; }
    VeriPseudoTreeNode *GetCurrentNode() const { return _current_node ; }
    void             CleanUp() ;
    void             CleanUpContentOfNodes(const Set *nodes) const ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    void AddVerilogNode(VeriPseudoTreeNode *current_node, VeriModule *module, const Map *actual_params, Array *created_units, const char *inst_name) ;
    void AddVhdlNode(VeriPseudoTreeNode *current_node, VhdlPrimaryUnit *unit, const char *arch_name, const char *inst_name) ;
    void AddVhdlBlockNode(VeriPseudoTreeNode *current_node, VhdlScope *block_scope) ;
    void             CopyCurrentNode(const VhdlPrimaryUnit *unit) ;
    void             SetVhdlInstanceName(const char *i) { _vhdl_inst_name = i ; }
    const char *     GetVhdlInstanceName() const        { return _vhdl_inst_name ; }
    Map *            GetUnitMapTab() const              { return _unit_map_tab ; }
    Map *            GetBlockMapTab() const             { return _block_map_tab ; }
    VhdlPrimaryUnit *GetVhdlTop() const                 { return _vhdl_top ; }
    Map *            GetTmpModMapTab() const            { return _tmp_mod_map_tab ; }
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
    Map *            GetModMapTab() const               { return _mod_map_tab ; }
    Map *            GetTopModPseudoNodes() const       { return _top_mod_pseudo_nodes ; }
    // Check whether any verilog node is created in static elaboration
    static void      SetContainsVeriNode()              { _contains_veri_node = 1 ; }
    static unsigned  IsContainsVeriNode()               { return _contains_veri_node ; }

    // VIPER #7727 : Processing of parameter/generic value
    static void CreateVerilogAndVhdlParamsGenerics(const Map *user_parameters, Map *verilog_parameters, Map *vhdl_parameters) ;
    static void CreateVhdlVerilogValue(const char *value, char **verilog_value, char **vhdl_value) ;
private:
    Array * _top_modules ;                  // array of top level modules
    Map   * _top_mod_pseudo_nodes ;         // map of pseudo tree node for top level modules module name vs pseudo node
    Map   * _mod_map_tab ;                  // VeriModuleItem*->Set of  pseudo tree node associated Map
    Map   * _parameters ;                   // It is a char*name->VeriConst* value Map association
                                            // of parameter settings, as provided by user to
                                            // elaborate a single module
    VeriLibrary *_library ;                 // the library to elaborate
    unsigned     _one_module_elab ;         // Flag to indicate if we are elaborating only one module
    static Array *_all_configuration_info ; // Stores the configuration info created for static elaborating configurations
    static Set  *_always_copy_modules ;     // Stores the VeriModuleItem * of the modules that has generate block or array instance
                                            // with parameters dependency. We need to always copy those modules.
    static unsigned  _contains_veri_node  ; // Flag to indicate if any verilog node exists (VIPER #6047)
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    VhdlPrimaryUnit *_vhdl_top ;            // Top level vhdl unit
    Map             *_unit_map_tab ;        // VhdlPrimaryUnit* vs pseudo nodes
    Map             *_block_map_tab ;       // VhdlScope* vs pseudo nodes (VIPER #6047)
    const char      *_arch_name ;
    const char      *_vhdl_inst_name ;      // label of vhdl instance in vhdl
    Map             *_tmp_mod_map_tab ;
    VeriPseudoTreeNode *_veri_in_vhdl ;     // verilog instantiated from vhdl (VIPER #7026)
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION
    VeriPseudoTreeNode *_current_node ;     // Pointer to currently processing node

public :
    // Static member to store the modules having no parameter/instances/interface ports/hiearchical names
    // but instantiated in design hierarchy. Since these modules contribute almost nothing in elaboration,
    // we will not create pseudo tree nodes for these module instantiations. This will help us to reduce
    // memory requirement of static elaboration for structural designs :
    static Set *_modules_with_no_side_effect ; // Modules having no parameter/instances/interface ports/hierarchical names
    static void SetModuleWithNoSideEffect(const VeriModuleItem *module) ;
    static unsigned IsModuleWithNoSideEffect(const VeriModuleItem *module) ;
    static Set *_blackboxs ;
    static void SetBlackBoxModule(const char *name) ;
    static unsigned IsBlackBoxModule(const char *bb) ;

    static Map *_port_map_tab ;
    static Map *GetPortInfo(const VeriModuleItem *mod) ;

    // Following map stores VeriModule * vs Set of array instance ids and instance ids for
    // which no node is created. After copying the modules and propagating parameters to
    // copied module, this map will be used to elaborate array instances present in copied
    // module.
    static Map *_instid_map ;
    static void SetInstanceId(const VeriModuleItem *mod, const VeriIdDef *inst_id) ; // Set module along with its instance identifier
    static Set *GetInstanceIds(const VeriModuleItem *mod) ; // Get list of instance ids for module 'mod'
    static void ResetInstanceIds(const VeriModuleItem *mod) ; // Reset instance id list
    // When 'old_mod' is copied and 'new_mod' is created, we need to create entries for the instance ids
    // present inside 'new_mod' in the map. Otherwise we will not get array instance ids for copied modules
    static void ReInsertInstanceIds(const VeriModuleItem *old_mod, const VeriModuleItem *new_mod, const VeriMapForCopy *old2new) ;

    static Map *_intf_inst_id_map ; // Store interface array instance ids
    static void SetInterfaceArrInstId(const VeriModuleItem *mod, const VeriIdDef *id) ;
    static void ProcessInterfaceArrInstIds() ;
    // VIPER #5725: Following map stores VeriContinuousAssign * vs Array of pseudo nodes where this
    // assignment exist. In our normal flow reference of instance array instance is replaced
    // by a virtual interface type variable (created for each interface array instance)
    // reference. But according to LRM virtual interface cannot be used in the lvalue of
    // continuous assignment. So we will store the continuous-assignment statements here
    // while processing and after elaborating all generate blocks we will try to resolve
    // the interface array instance references. This will be done before actually declaring
    // the virtual interface identifiers created for interface array instance. So if there is
    // any reference of interface array instance in lvalue, that will be resolved with actual
    // array instance references.
    static Map *_cont_assign_map ;
    static void AddContinuousAssign(const VeriModuleItem *cont_assign, const Array *nodes) ;
    void ProcessContinuousAssigns() ;

    // VIPER #7962 : _net_alias_map stores VeriNetAlias * vs Array of pseudo nodes
    // where this declaration exist. In net-alias hierarchical names having interface
    // array instance reference will not be replaced by virtual interface.
    static Map *_net_alias_map ;
    static void AddNetAlias(const VeriModuleItem *net_alias, const Array *nodes) ;
    void ProcessNetAliases() ;

    // VIPER #5805: For Path_name support in dual language elaboration:
    static Map *_inst_path_map ; // Map of VeriInstId vs char *path_name
    static void AddPathName(const VeriIdDef *inst_id, char *path_name) ;
    static char *GetPathName(const VeriIdDef *inst_id) ;
    static void ResetPathNameMap() ;

    void MakeVhdlInstancePathUnique() ;
    unsigned HasPathNameAttribute() const { return _has_path_name ; }
    void SetHasPathNameAttribute(unsigned s)  { _has_path_name = s ; }
    unsigned _has_path_name ;

    static unsigned     IsFromVhdl()  { return _from_vhdl ; } // VIPER #3318 : Module from vhdl
    static void         SetFromVhdl(unsigned s) { _from_vhdl = s ; } // VIPER #3318 : Module from vhdl
    static unsigned  _from_vhdl ;           // VIPER #3318 : Flag to indicate we are processing module instantited from vhdl
    static void AddToDeleteBlkId(const VeriIdDef *id) ;
    static Set *_to_delete_blk_ids ; // Store the original for-generate ids declared in module scope (VIPER #6080)
public :
    // VIPER #7456 : Store the set of object identifiers used in type operator
    // along with the container module. We need to process data types of those
    // identifiers to replace type-operator with proper type.
    static void       AddIdUsedInTypeOperator(const VeriIdDef *id, const VeriModuleItem *container) ;
    static Set*       GetIdsUsedinTypeOperatorFor(const VeriModuleItem *container) ;
    static void       UpdateIdsUsedInTypeOp(const VeriModuleItem *old_mod, const VeriModuleItem *new_mod, const VeriMapForCopy *old2new) ;
    static void       ResetIdsUsedInTypeOp() ;
    // VIPER #7807: Handling of enum identifier along with its type
    static void       AddEnumIdWithNewType(const VeriIdDef *enum_id, const VeriIdDef *type_id) ;
    static VeriIdDef *GetTypeIdOfEnum(const VeriIdDef *enum_id) ;
    static void       ResetEnumIdVsNewType() ;
private :
    static Map *_container_vs_ids ;
    static Map *_enum_id_vs_newtype ; // VIPER #7807: Store enum literal and its new type
public:
    // VIPER #7508 : Stored original package scope with copied scope
    static Map * GetPackageScopeMap() { return _pkg_scope_map ; }
    static Map *_pkg_scope_map ;

// VIPER #7853 : Create short name when length of made up name is greater than 200
    static char *   CreateShortName(const char *unit_name, char *long_name) ;
    static Map      *_long2short ; // long_unit_name vs short name
    static Map      *_module_num_map ;
    static void     CleanLong2ShortMap() ; // API to clean above static map
private:
    // Elaborate the top level modules
    void CreateHierTreeForTopModules() ;

    // Instantiate VeriModule*->Array of  pseudo tree node associated Map (_modMapTab)
    void SetModMapTab();

    // Instantiate array of pseudo tree node for top level modules (_topModPseudoNodes)
    void SetTopModPseudoNodeArray();

    // Create the hierarchical tree under argument specific module.
    Array *CreateHierTree(VeriModule *mod) ; // Returns Array of VeriPseudoTreeNode *

    // Resolve the hierarchical identifiers, function/task calls and defparams for the hierarchical tree.
    void ResolveHierarchicalIds(unsigned b_error_suppress);

    // Process the defparam statements to find the overridden value of the parameters.
    void ProcessDefparams(Map *top_nodes) const ;

    // Internal method for elaboration
    void ElaborateInternal(unsigned b_connect_insert) ;

    // All the iterative steps of the elaboration (copying of modules, elaboartion of generate,
    // hierarchical id resolution, signature creation) are performed.
    void ElaborateIteratively(unsigned b_connect_insert) ;

    // Populate the argument specific signature table. The key of this Map is VeriModule* and
    // the data is Map* (signature in char*->array of pseudo tree node containing the key specific signature)
    void PopulateSignatureTab(Map *sig_map_tab) const ;
    void PopulateSigTabInternal(Map *sig_map_tab, const VeriModuleItem *mod, const Set *array, Set *done) const ; // VIPER #7508

    // Delete the parameter value assignment list from VeriModuleInstantiation. This deletion
    // is not performed for black box instantiation.
    void PostElabProcess() const ;
    void DeleteSpuriousNestedModules(Set *orig_mods, const Set *instantiated_mods) ;
    void ProcessDeletedModuleList(const VeriModuleItem *design, Set *list) const ;
    void ProcessNestedModuleList(const VeriScope *scope, Set *list) const ;
#ifdef VERI_STATIC_ELAB_DEBUG
    void PrintParameterAndHierIds() ;
    void RestoreAllInstantiatedModules() ;
#endif
    // Related to uniquify modules depending hier name path
    void CreateSignatureForHierNamePath() const ;
} ; // class VeriStaticElaborator

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_STATIC_ELAB_H_
