/*
 *
 * [ File Version : 1.546 - 2014/03/28 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>   // For UDP table entry manipulation
#include <math.h>     // For double log10(double) and double ceil(double)

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"

#include "VeriModuleItem.h"
#include "VeriId.h"
#include "VeriMisc.h"
#include "VeriStatement.h"
#include "VeriExpression.h"
#include "VeriScope.h"
#include "VeriLibrary.h"
#include "VeriConstVal.h"
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/vhdl_file.h"
#include "../vhdl/VhdlIdDef.h"
#include "VeriModule.h"
#include "VeriBaseValue_Stat.h"

#include "veri_file.h" // For veri_file::GetLastAddedLibrary()

#include "veri_tokens.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/* -------------------------------------------------------------- */
// class to visit block statement to find out if it contains delay or event control
// in an expression. This class is used in parameter value evaluation
class StmtVisit : public VeriVisitor
{
public:
    StmtVisit() : VeriVisitor(), _event_control_present(0) { }
    ~StmtVisit() { }
private:
    // Prevent compiler from defining the following
    StmtVisit(const StmtVisit &) ;            // Purposely leave unimplemented
    StmtVisit& operator=(const StmtVisit &) ; // Purposely leave unimplemented

public:
    virtual void VERI_VISIT(VeriEventControlStatement, node) ;
    unsigned ContainsEventControl() const { return _event_control_present ; }

private:
    unsigned _event_control_present ; // Flag to indicate if event control stmt exists
} ; // class StmtIdVisit

/* -------------------------------------------------------------- */

void StmtVisit::VERI_VISIT(VeriEventControlStatement, node)
{
    _event_control_present++ ;
    VeriVisitor::VERI_VISIT_NODE(VeriEventControlStatement, node) ;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VeriModuleItem::VeriModuleItem()
  : VeriTreeNode()
  , _qualifiers(0)
{
    // At construction time of a VeriModuleItem (AT the semicolon),
    // pick up the pragma's that were set during parsing the moduleitem,
    // and set them as attributes on the ModuleItem
    // VIPER #6840: Only attach pragma if the last one is defined within 'this' item:
    unsigned last_line = LineFile::GetLineNo(Linefile()) ;
    if (last_line >= veri_file::GetLastPragmaLine()) {
        Map *pragmas = veri_file::TakePragmas() ;
        if (pragmas) {
            // Translate them to attributes, and set on the ModuleItem
            AddAttributes(PragmasToAttributes(0,pragmas)) ;
        }
    }
}

VeriModuleItem::~VeriModuleItem()
{
    // VIPER #6906: Attributes and comments are now deleted from VeriTreeNode destructor only:

    // Delete attributes if present
    //DeleteAttributes() ;

    // Delete comments if there
    //VeriNode::DeleteComments(TakeComments()) ;
}

//////////////////////////////////////////////////////////////////////
// Derived class Construction/Destruction
//////////////////////////////////////////////////////////////////////

VeriDataDecl::VeriDataDecl(unsigned decl_type, VeriDataType *data_type, Array *ids, VeriName *resolution_function) :
    VeriModuleItem(),
    _data_type(data_type),
    _ids((ids) ? ids->Size() : 1),
    _decl_type(decl_type)
    , _resolution_function(resolution_function)
{
    if (ids) _ids.Append(ids) ;
    delete ids ;

    (void) resolution_function ; // bypass unused warning
    // internal consistency : resolution fn only allowed for user nettype
    if (_resolution_function) VERIFIC_ASSERT(_decl_type == VERI_NETTYPE) ;

    unsigned has_protected_objects = 0 ;

    // Set the id's direction, type and signing.
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

        if (id->IsTickProtectedNode()) has_protected_objects = 1 ;

        // For ports : pass direction to all ids :
        if (GetDir()) id->SetDir(GetDir()) ;

        // For parameters (or identifiers that need the declaration type):
        id->SetParamType(_decl_type) ; // set what kind of declaration this is.

        // For type-parameters :
        // Check whether it is actually type parameter/its type is specified by
        // type parameter
        // parameter type p = bit ;
        // parameter p p2 = 1 ; // 'p2' is not type
        if (_data_type && _data_type->Type()==VERI_TYPE && !_data_type->GetTypeName() && !_data_type->IsTypeOperator() && id->IsParam()) id->SetIsType() ;
        // It should not be a typedef here, so, no need to check the data types

        // VIPER #7951 (duplicate_classes): Forward typedef cannot land here since it defines multiple ids which is not possible by yeacc rule for typedefs!

        // For all : set the data type (if there). If not, DON'T override with 0. There might be already a data type in there.
        if (_data_type) {
            if (_data_type == id->GetDataType()) {
                // VIPER #4594: Same data type set here, which means it is a redeclaration like: reg a, a ;
                Warning("%s is already declared", id->Name()) ;
                Warning("second declaration of %s ignored", id->Name()) ;
                //id->Info("%s is declared here", id->Name()) ;
                id->Info("previous declaration of %s is from here", id->Name()) ; // VIPER #7773
            } else {
                id->SetDataType(_data_type) ;
            }
        }

        // If this id is an ansi port then this DataDecl attempts to re-declare an ansi port.
        if (id->IsAnsiPort()) {
            // VIPER 2949 : If on top of that it has an initial value, and is an output/inout port
            // then we should discard that initial value (because it is not legal to have an output with initial value).
            if (id->GetInitialValue() && (id->IsOutput() || id->IsInout())) {
                id->SetInitialValue(0) ; // FIX ME : SetInitialValue does not allow override wit 0.
            }
        }
        // For identifiers with a module item back-pointer.
        id->SetModuleItem(this) ;
    }

    linefile_type linefile = Linefile() ;
    if (linefile && has_protected_objects) {
        // This is protected RTL. Modify the _linefile to carry only filename
        // and 0 as the line number. This is a special linefile to denote the
        // protected RTL region
        const char *file_name = LineFile::GetFileName(linefile) ;
        if (file_name) SetLinefile(LineFile::EncodeLineFile(file_name, 0)) ;
    }
}

VeriDataDecl::VeriDataDecl(unsigned decl_type, VeriDataType *data_type, VeriIdDef *id, VeriName *resolution_function) :
    VeriModuleItem(),
    _data_type(data_type),
    _ids(1),
    _decl_type(decl_type)
    , _resolution_function(resolution_function)
{
    if (!id) return ; // something bad happened.
    _ids.InsertLast(id) ;

    // For ports : pass direction to all ids :
    if (GetDir()) id->SetDir(GetDir()) ;

    (void) resolution_function ; // bypass unused warning
    // internal consistency : resolution fn only allowed for user nettype
    if (_resolution_function) VERIFIC_ASSERT(_decl_type == VERI_NETTYPE) ;

    // For parameters :
    id->SetParamType(_decl_type) ; // set what kind of declaration this is.

    // For type-parameters :
    // Check whether it is actually type parameter/its type is specified by
    // type parameter
    // parameter type p = bit ;
    // parameter p p2 = 1 ; // 'p2' is not type
    if (_data_type && _data_type->Type()==VERI_TYPE && !_data_type->GetTypeName() && id->IsParam()) id->SetIsType() ;

    // VIPER #7951 (duplicate_classes): Set the forward typedef bit flag:
    if ((_decl_type==VERI_TYPEDEF) && !_data_type) id->SetForwardTypeDefined() ;

    // For all : set the data type (if there). If not, DON'T override with 0. There might be already a data type in there (from a reg/wire decl before a io-decl).
    if (_data_type) id->SetDataType(_data_type) ;

    // For identifiers with a module item back-pointer.
    id->SetModuleItem(this) ;
}

void
VeriDataDecl::AddDataDecl(VeriIdDef *id)
{
    // Add another id to the list
    if (!id) return ; // something bad happened.
    _ids.InsertLast(id) ;

    // For ports : pass direction to all ids :
    if (GetDir()) id->SetDir(GetDir()) ;

    // For parameters :
    id->SetParamType(_decl_type) ; // set what kind of declaration this is.

    // For type-parameters :
    // Check whether it is actually type parameter/its type is specified by
    // type parameter
    // parameter type p = bit ;
    // parameter p p2 = 1 ; // 'p2' is not type
    if (_data_type && _data_type->Type()==VERI_TYPE && !_data_type->GetTypeName() && id->IsParam()) id->SetIsType() ;
    // It should not be a typedef here, so, no need to check the data types

    // For all : set the data type (if there). If not, DON'T override with 0. There might be already a data type in there.
    if (_data_type) id->SetDataType(_data_type) ;

    // For identifiers with a module item back-pointer.
    id->SetModuleItem(this) ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Now, adjust the line-column info of this data declaration to include this id:
    linefile_type decl_lf = Linefile() ;
    // VIPER #6943: Update the ending linefile to include the initial value, if any:
    //linefile_type id_lf = id->Linefile() ;
    VeriExpression *init_val = id->GetInitialValue() ;
    linefile_type id_lf = (init_val) ? init_val->Linefile() : id->Linefile() ;
    if (decl_lf && id_lf) {
        // Set the ending line-column same as that of the id:
        decl_lf->SetRightLine(id_lf->GetRightLine()) ;
        decl_lf->SetRightCol(id_lf->GetRightCol()) ;
    }
#endif
}
VeriDataDecl::~VeriDataDecl()
{
    // Reset the back pointers of the ids which points to the data type of
    // the data type of this declaration:
    // Note that the identifiers MUST still exist. This is a general rule for moduleitem destructors :
    // The identifiers can be deleted only AFTER the module item is destroyed.
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;
        // Clear the data type of the id only if we are going to delete it:
        // Since for type parameter, GetDataType() returns the initial value, we need
        // to explicitly check for type parameter and clear the data type for that:
        if (id->IsPrototypeId() || (id->GetDataType() == _data_type) ||
            (id->IsParam() && id->IsType())) id->ClearDataType() ;
        id->ClearTreeNodeBckPtr() ;
    }

    delete _data_type ;

    delete _resolution_function ;

    // Don't delete ids themselves (they belong to the scope). Only delete the array.
}

void VeriDataDecl::AccumulatePorts(Array &ports) const
{
    if (!GetDir()) return ; // only focus on IO declarations
            //&& !GetQualifier(VERI_EXPORT) && !GetQualifier(VERI_IMPORT)
        //) return ; // only focus on IO declarations
    ports.Append(&_ids) ;
}

void VeriDataDecl::AccumulateParameters(Array &parameters, unsigned has_ansi_parameter_list, const VeriIdDef *mod_id) const
{
    if (_decl_type != VERI_PARAMETER) return ; // only focus on pure parameter declarations

    if (has_ansi_parameter_list) {
        // Issue 1538 : This is allowed in Verilog LRM 3.1.1.
        // The added parameters into 'localparam',
        // so that elaboration will error out if they get overwritten :
        if (mod_id) Warning("parameter declaration becomes local in %s with formal parameter declaration list", mod_id->Name()) ;
        // Change the identifiers' ParamType :
        unsigned j ;
        VeriIdDef *param_id ;
        FOREACH_ARRAY_ITEM(&_ids, j, param_id) {
            if (param_id) param_id->SetParamType(VERI_LOCALPARAM) ;
        }
        return ; // Do not append these ids into parameter list
    }

    parameters.Append(&_ids) ;
}

void
VeriDataDecl::SetDir(unsigned dir)
{
    // The incoming 'dir' argument should be either VERI_INPUT, VERI_OUTPUT, or VERI_INOUT.
    // We will not do a check in here to assure that 'dir' is a valid value.  These values
    // are defined in veri_tokens.h.

    // Change the direction (declaration type) of this declaration and all of its ids to 'dir'.
    _decl_type = dir ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (GetDir()) id->SetDir(GetDir()) ;
    }
}

unsigned
VeriDataDecl::GetDir() const
{
    // We store the direction token in the _decl_type token field.
    // Or, if you want to think of it otherwise, we store the 'declaration type' (Parameter, specparam, genvar etc)
    // in the 'direction' token field (called _decl_type).

    // This routine GetDir() makes the distiction between a data type token and a direction token.
    switch (_decl_type) {
    case VERI_INPUT :
    case VERI_OUTPUT :
    case VERI_INOUT :
    case VERI_REF :
    case VERI_CONST : // means "CONST REF"
        return _decl_type ;
    default : break ;
    }
    return 0 ; // not a IO declaration, so there is no direction.
}

unsigned
VeriDataDecl::IsTypeAlias() const
{
    return (VERI_TYPEDEF == _decl_type) || (VERI_NETTYPE == _decl_type) ;
}

VeriNetDecl::VeriNetDecl(unsigned net_type, VeriStrength *strength, VeriRange *range, Array *delay, Array *ids, unsigned signed_type) :
    VeriDataDecl(VERI_WIRE,new VeriNetDataType(net_type, new VeriDataType(0,signed_type,range)),ids), // Linefile info may be wrong for the data type
    _strength(strength),
    _delay(delay)
{
    Validate() ;
}

// New constructor (using a data type).
VeriNetDecl::VeriNetDecl(VeriDataType *data_type, VeriStrength *strength, Array *delay, Array *ids) :
    VeriDataDecl(VERI_WIRE, data_type, ids),
    _strength(strength),
    _delay(delay)
{
    Validate() ;
}

VeriNetDecl::~VeriNetDecl() {
    delete _strength ;
    unsigned i ;
    VeriExpression *delay_elem ;
    FOREACH_ARRAY_ITEM(_delay, i, delay_elem) delete delay_elem ;
    delete _delay ;
}
void VeriNetDecl::Validate()
{
    // VIPER #4262 : Either all ids need to have initial value or none can have
    // initial value :
    VeriIdDef *id ;
    unsigned i ;
    unsigned has_init_assign = 0 ;
    VeriExpression *init = 0 ;
    VeriRange *unpacked_dim ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;
        init = id->GetInitialValue() ;
        // Viper 5514. Do not check for initial value if the existing datatype of NetDecl is
        // different from id->GetDataType(). This is because if this id represents an already
        // declared id with an initial value then checking whether this NetDecl has all its ids with
        // initial value or non with any initial value would be wrong.
        if (id->GetDataType()!=_data_type) init = 0 ;
        if (i == 0) {
            if (init) has_init_assign = 1 ;
            continue ; // Do not check anything for first identifier
        }
        if ((init && !has_init_assign) || (!init && has_init_assign)) {
            id->Error("all identifiers need to have initial value") ;
        }
        // VIPER #6996 : Dynamic/associative range cannot be used in net declaration
        unpacked_dim = id->GetDimensions() ;
        while (unpacked_dim) {
            if (unpacked_dim->IsDynamicRange()) {
                unpacked_dim->Error("dynamic range is not allowed in net declaration") ;
            }
            unpacked_dim = unpacked_dim->GetNext() ;
        }
    }
    // VIPER #6996 : Dynamic/associative range cannot be used in net declaration
    VeriRange *packed_dim = _data_type ? _data_type->GetDimensions(): 0 ;
    while (packed_dim) {
        if (packed_dim->IsDynamicRange()) {
            packed_dim->Error("dynamic range is not allowed in net declaration") ;
        }
        packed_dim = packed_dim->GetNext() ;
    }

    // SV 2012: 6.7.1: Check for generic interconnect
    if (IsInterconnectType()) {
        // Check (1): Can have at most one delay value
        if (_delay && _delay->Size() > 1) Error("generic interconnect may specify at most one delay value") ;

        // Check (2): Must not specify drive strength
        if (_strength) Error("generic interconnect must not specify any drive strength") ;
    }
}
VeriFunctionDecl::VeriFunctionDecl(unsigned automatic, VeriDataType *data_type, VeriIdDef *func, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) :
    VeriModuleItem(),
    _func(func),
    _name(new VeriIdRef(func)),
    _automatic_type(automatic),
    _can_be_const_func(1),
    _is_const_called(0),
    _verilog_method(1),
    _data_type(data_type),
    _ansi_io_list(ansi_io_list),
    _decls(decls),
    _stmts(stmts),
    _scope(scope),
    _ports(0)
{
    if (_func) _name->SetLinefile(_func->Linefile()) ;
    Init() ;
}
void VeriFunctionDecl::Init()
{
    unsigned i ;
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (_ansi_io_list && decl_item && decl_item->IsIODecl()) {
            // VIPER #7363 (re-opened): If ANSI ports (even the empty parenthesis) are specified,
            // there cannot be any IO declaration in the declaration part. It is an error:
            decl_item->Error("syntax error near %s", PrintToken(decl_item->GetDeclType())) ;
        }
    }

    if (_stmts && _stmts->Size() > 1 && (IsVeri95() || IsVeri2001())) Error("multiple statement function/task without begin/end not supported in this mode of Verilog") ; // VIPER #4762

    // Set the pointer back to me
    if (_func) {
        // VIPER #4915 : Propagate virtual qualifier from prototype to actual definition
        VeriModuleItem *prev_body = _func->GetModuleItem() ;
        if (prev_body) {
            // Verific-VCS incompatibility VCS issues error.
            if (prev_body->GetAutomaticType() != GetAutomaticType()) _func->Warning("life times of extern declaration and prototypes do not match") ;
            if (prev_body->GetQualifier(VERI_VIRTUAL)) SetQualifier(VERI_VIRTUAL) ;
            if (prev_body->GetQualifier(VERI_LOCAL)) SetQualifier(VERI_LOCAL) ;
            if (prev_body->GetQualifier(VERI_PROTECTED)) SetQualifier(VERI_PROTECTED) ;
            if (prev_body->GetQualifier(VERI_STATIC)) SetQualifier(VERI_STATIC) ;
            if (prev_body->GetQualifier(VERI_AUTOMATIC)) SetQualifier(VERI_AUTOMATIC) ;

            if (!prev_body->GetQualifier(VERI_EXTERN)) {
                // VIPER #5248 (test_12): Clear the back pointer to its id:
                prev_body->ClearId() ;
                // Do not miss back pointer in other body's scope
                VeriScope *body_scope = prev_body->GetScope() ;
                if (body_scope) body_scope->SetOwner(0) ;
            }
        }
        _func->SetModuleItem(this) ;
    }

    // Set lifetime as qualifier onto this item :
    // Don't set as qualifier from the _automatic type token.
    // The _automatic_type token represents the lifetime, as set AFTER the function keyword.
    // If qualifier is set, then it is a class qualifier, set BEFORE the function keyword.
    // if (_automatic_type) SetQualifier(_automatic_type) ;

    // If automatic (either as lifetime or as class qualifier), set as flag onto the identifier :
    if ((GetQualifier(VERI_AUTOMATIC) || _automatic_type==VERI_AUTOMATIC) && _func) _func->SetIsAutomatic() ;

    if (_data_type && _data_type->GetDimensions() && _data_type->GetType()) {
        // error only valid for Verilog 2000.
        if (IsVeri2001()) Error("cannot set both range and type on function declaration") ;
    }
    // VIPER #4897: Produce error if signing is specified for 'void' type
    if (_data_type && _data_type->GetSigning() && (_data_type->GetType() == VERI_VOID)) {
        Error("cannot set both signing and void type on function decl") ;
    }
    // VIPER #4798 : Produce error if return type is specified for constructor
    if (_data_type && _func && (_func->GetFunctionType() == VERI_METHOD_NEW)) {
        Error("method new cannot have return type") ;
    }
    if (!_data_type) {
        unsigned default_type = 0 ;
        if (default_type) _data_type = new VeriDataType(default_type, 0, 0) ;
        if (_func && _data_type) _data_type->SetLinefile(_func->Linefile()) ;
    }

    // Set the data type (if there) back pointer :
    if (_func && _data_type) _func->SetDataType(_data_type) ;

    //unsigned i ;

    // Set-up port list : done by order
    _ports = new Array() ;

    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->AccumulatePorts(*_ports) ;
    }

    unsigned size = _ports->Size(); // Needed for port decl check

    //VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (!decl_item) continue ;
        decl_item->AccumulatePorts(*_ports) ;
        // If size is different, then there was an IO decl with ANSI decl
        if (size && size != _ports->Size()) {
            if (_func) decl_item->Error("port declaration not allowed in %s with formal port declaration list", _func->Name()) ;
            size = _ports->Size() ;
        }
    }

    // Verify that there is at least one input, and no outputs or inouts (LRM 10.3.4)
    VeriIdDef *portid ;
    unsigned input_exists = 0;
    // Default direction setting : SV 1800 LRM 12.2: default direction is 'input'.
    // Need to do this NOW, since in Resolve(), we check against formals of function calls,
    // and functions can be used before declared.
    // NOTE : we can only set the default 'direction' here, not the default 'type'.
    // Default type is only known at 'Resolve' time (due to forward type decls).
    unsigned previous_dir = VERI_INPUT ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_ports, i, portid) {
        if (!portid) continue ;
        // get direction of this port :
        dir = portid->Dir() ;
        // Inherit previous port direction :
        if (!dir) {
            dir = previous_dir ;
            portid->SetDir(dir) ;
        }
        // Check if there are any inputs.
        switch (dir) {
        case VERI_INPUT: input_exists = 1 ; break ;
        case VERI_OUTPUT:
        case VERI_INOUT:
            // Following is only true for non-SV parsing
            // In analog functions inout/output ports are allowed AMS 2.3(4.7.1)
            if (!IsSystemVeri() && !IsAms()) portid->Error("output or inout not allowed in functions") ; break ;
        case VERI_REF: // ref
        case VERI_CONST: // means "const ref"
            // VIPER #4426: IEEE 1800 LRM section 12.3.3: constant functions cannot have 'ref' type port
            _can_be_const_func = 0 ; break ;
        default:
            break ;
        }
        // update previous port direction
        previous_dir = dir ;
    }
    // Following is only true for non-SV parsing :
    if (!input_exists && _func && !IsSystemVeri()) _func->Error("a function must have at least one input") ;
}

VeriFunctionDecl::VeriFunctionDecl(unsigned automatic, VeriDataType *data_type, VeriName *func_name, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) :
    VeriModuleItem(),
    _func(0),
    _name(func_name),
    _automatic_type(automatic),
    _can_be_const_func(1),
    _is_const_called(0),
    _verilog_method(1),
    _data_type(data_type),
    _ansi_io_list(ansi_io_list),
    _decls(decls),
    _stmts(stmts),
    _scope(scope),
    _ports(0)
{
    _func = (_name) ? _name->GetId() : 0 ;

    Init() ;
}

VeriFunctionDecl::~VeriFunctionDecl()
{
#if 0
    delete _func ; // Owned by scope
#else
    // VIPER #3369: Clear the data type pointer
    if (_func) {
        // Check whether this identifier is shared by multiple function bodies, if
        // so set _id back pointer in that tree as null.
        if ((_scope && _scope->GetClass()) && (_func->GetModuleItem() != this)) {
            VeriModuleItem *body = _func->GetModuleItem() ;
            if (body) body->ClearId() ;
            // Do not miss back pointer in other body's scope
            VeriScope *body_scope = (body) ? body->GetScope(): 0 ;
            if (body_scope) {
                VeriIdDef *owner = body_scope->GetOwner() ;
                if (owner) owner->SetLocalScope(0) ;
                body_scope->SetOwner(0) ;
            }
        }
        _func->ClearAllBckPtrs() ;
    }
    _func = 0 ;
#endif
    delete _name ;

    delete _data_type ;

    unsigned i ;
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) delete port_decl ;
    delete _ansi_io_list ;

    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) delete decl_item ;
    delete _decls ;

    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) delete stmt ;
    delete _stmts ;

    // Delete the created list of ports
    delete _ports ;

    // Delete the identifiers in the scope and scope itself
    delete _scope ;
}

VeriTaskDecl::VeriTaskDecl(unsigned automatic_type, VeriIdDef *task, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) :
    VeriModuleItem(),
    _task(task),
    _name(new VeriIdRef(task)),
    _automatic_type(automatic_type),
    _contains_delay_or_event_control(0),
    _ansi_io_list(ansi_io_list),
    _decls(decls),
    _stmts(stmts),
    _scope(scope),
    _ports(0)
{
    if (_task) _name->SetLinefile(_task->Linefile()) ;
    Init() ;
}
VeriTaskDecl::VeriTaskDecl(unsigned automatic_type, VeriName *task, Array *ansi_io_list, Array *decls, Array *stmts, VeriScope *scope) :
    VeriModuleItem(),
    _task(0),
    _name(task),
    _automatic_type(automatic_type),
    _contains_delay_or_event_control(0),
    _ansi_io_list(ansi_io_list),
    _decls(decls),
    _stmts(stmts),
    _scope(scope),
    _ports(0)
{
    _task = (_name) ? _name->GetId() : 0 ;
    Init() ;
}
void VeriTaskDecl::Init()
{
    unsigned i ;
    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (_ansi_io_list && decl_item && decl_item->IsIODecl()) {
            // VIPER #7363 (re-opened): If ANSI ports (even the empty parenthesis) are specified,
            // there cannot be any IO declaration in the declaration part. It is an error:
            decl_item->Error("syntax error near %s", PrintToken(decl_item->GetDeclType())) ;
        }
    }

    if (_stmts && _stmts->Size() > 1 && (IsVeri95() || IsVeri2001())) Error("multiple statement function/task without begin/end not supported in this mode of Verilog") ; // VIPER #4762

    // Set the pointer back to me
    if (_task) {
        // VIPER #4915 : Propagate virtual qualifier from prototype to actual definition
        VeriModuleItem *prev_body = _task->GetModuleItem() ;
        if (prev_body) {
            if (prev_body->GetQualifier(VERI_VIRTUAL)) SetQualifier(VERI_VIRTUAL) ;
            if (prev_body->GetQualifier(VERI_LOCAL)) SetQualifier(VERI_LOCAL) ;
            if (prev_body->GetQualifier(VERI_PROTECTED)) SetQualifier(VERI_PROTECTED) ;
            if (prev_body->GetQualifier(VERI_STATIC)) SetQualifier(VERI_STATIC) ;
            if (prev_body->GetQualifier(VERI_AUTOMATIC)) SetQualifier(VERI_AUTOMATIC) ;
        }
        _task->SetModuleItem(this) ;
    }

    // Set lifetime as qualifier onto this item :
    // Don't set as qualifier from the _automatic type token.
    // The _automatic_type token represents the lifetime, as set AFTER the task keyword.
    // If qualifier (automatic or static) is set, then it is a class qualifier, set BEFORE the task keyword.
    // if (_automatic_type) SetQualifier(_automatic_type) ;

    // If automatic (either as lifetime or as class qualifier), set as flag onto the identifier :
    if ((GetQualifier(VERI_AUTOMATIC) || _automatic_type==VERI_AUTOMATIC) && _task) _task->SetIsAutomatic() ;

    // Set-up port list : done by order
    _ports = new Array() ;

    //unsigned i ;
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->AccumulatePorts(*_ports) ;
    }

    unsigned size = _ports->Size(); // Needed for port decl check

    //VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (!decl_item) continue ;
        decl_item->AccumulatePorts(*_ports) ;

        // If size is different, then there was an IO decl with ANSI decl
        if (size && size != _ports->Size()) {
            if (_task) decl_item->Error("port declaration not allowed in %s with formal port declaration list", _task->Name()) ;
            size = _ports->Size() ;
        }
    }

    // Default direction setting : SV 1800 LRM 12.2: default direction is 'input'.
    // Need to do this NOW, since in Resolve(), we check against formals of function calls,
    // and functions can be used before declared.
    // NOTE : we can only set the default 'direction' here, not the default 'type'.
    // Default type is only known at 'Resolve' time (due to forward type decls).
    VeriIdDef *portid ;
    unsigned previous_dir = VERI_INPUT ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_ports, i, portid) {
        if (!portid) continue ;
        // get direction of this port :
        dir = portid->Dir() ;
        // Inherit previous port direction :
        if (!dir) {
            dir = previous_dir ;
            portid->SetDir(dir) ;
        }
        // update previous port direction
        previous_dir = dir ;
    }
}
VeriTaskDecl::~VeriTaskDecl()
{
#if 0
    delete _task ; // Owned by scope
#else
    // VIPER #3369: Clear the data type pointer
    if (_task) {
        // Check whether this identifier is shared by multiple task bodies, if
        // so set _id back pointer in that tree as null.
        if ((_scope && _scope->GetClass()) && (_task->GetModuleItem() != this)) {
            VeriModuleItem *body = _task->GetModuleItem() ;
            if (body) body->ClearId() ;
            // Do not miss back pointer in other body's scope
            VeriScope *body_scope = (body) ? body->GetScope(): 0 ;
            if (body_scope) body_scope->SetOwner(0) ;
        }
        _task->ClearAllBckPtrs() ;
    }
    _task = 0 ;
#endif
    delete _name ;
    unsigned i ;

    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) delete port_decl ;
    delete _ansi_io_list ;

    VeriModuleItem *decl_item ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) delete decl_item ;
    delete _decls ;

    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) delete stmt ;
    delete _stmts ;

    // Delete the created list of ports
    delete _ports ;

    // Delete the identifiers in the scope and scope itself
    delete _scope ;
}

VeriDefParam::VeriDefParam(Array *defparam_assigns) :
    VeriModuleItem(),
    _defparam_assigns(defparam_assigns)
{
}
VeriDefParam::~VeriDefParam()
{
    unsigned i ;
    VeriDefParamAssign *defparam_assign ;
    FOREACH_ARRAY_ITEM(_defparam_assigns, i, defparam_assign) delete defparam_assign ;
    delete _defparam_assigns ;
}

VeriContinuousAssign::VeriContinuousAssign(VeriStrength *strength, Array *delay, Array *net_assigns) :
    VeriModuleItem(),
    _strength(strength),
    _delay(delay),
    _net_assigns(net_assigns)
{
}
VeriContinuousAssign::~VeriContinuousAssign()
{
    delete _strength ;

    unsigned i ;
    VeriExpression *delay_elem ;
    FOREACH_ARRAY_ITEM(_delay, i, delay_elem) delete delay_elem ;
    delete _delay ;

    VeriNetRegAssign *item ;
    FOREACH_ARRAY_ITEM(_net_assigns, i, item) delete item ;
    delete _net_assigns ;
}

// default constructor
VeriGateInstantiation::VeriGateInstantiation(unsigned inst_type, VeriStrength *strength, Array *delay, Array *insts) :
    VeriModuleItem(),
    _inst_type(inst_type),
    _strength(strength),
    _delay(delay),
    _insts(insts)
{
}

// constructor used from bison
VeriGateInstantiation::VeriGateInstantiation(unsigned inst_type, VeriStrength *strength, Array *delay, const VeriInstId *single_inst, Array *additional_insts) :
    VeriModuleItem(),
    _inst_type(inst_type),
    _strength(strength),
    _delay(delay),
    _insts(additional_insts)
{
    // Possibly we have only one instance, or one plus additional ones.
    // Later, we might save memory by having a single instance constructor with only one field and no array.
    // Now, we need to merge them into an array any way,
    if (single_inst) {
        if (!_insts) _insts = new Array(1) ;
        _insts->InsertFirst(single_inst) ;
    }
}
VeriGateInstantiation::~VeriGateInstantiation() {
    unsigned i ;
    // Don't delete InstId identifiers. They are owned by the scope
    //VeriInstId *inst ;
    //FOREACH_ARRAY_ITEM(_insts, i, inst) delete inst ;
    delete _insts ;
    _insts = 0;

    VeriExpression *delay_elem ;
    FOREACH_ARRAY_ITEM(_delay, i, delay_elem) delete delay_elem ;
    delete _delay ;

    delete _strength ;
}

// default constructor :
VeriModuleInstantiation::VeriModuleInstantiation(char *module_name, VeriStrength *strength, Array *param_values, Array *insts) :
    VeriModuleItem(),
    _module_name(0),
    _strength(strength),
    _param_values(param_values),
    _insts((insts) ? insts->Size() : 1),
    _library(0)
{
    if (insts) _insts.Append(insts) ;
    delete insts ;

    // Old way of constructing a module instantiation : initialize with a char*...
    _module_name = new VeriIdRef(module_name) ; // absorbs 'module_name'

    VeriInstId *inst ;
    unsigned i ;
    // Set the module instantiation back pointer into the instance iddefs :
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (!inst) continue ;
        // Set back-pointers :
        inst->SetModuleInstance(this) ;
    }

    // Set the current user-library (from -v/-y/uselib)
    _library = veri_file::GetCurrentUserLibrary();
}

// constructor used from bison.
VeriModuleInstantiation::VeriModuleInstantiation(char *module_name, VeriStrength *strength, Array *param_values, const VeriInstId *single_inst, Array *additional_insts) :
    VeriModuleItem(),
    _module_name(0),
    _strength(strength),
    _param_values(param_values),
    _insts((additional_insts) ? additional_insts->Size() : 1),
    _library(0)
{
    if (additional_insts) _insts.Append(additional_insts) ;
    delete additional_insts ;

    // Old way of constructing a module instantiation : initialize with a char*...
    _module_name = new VeriIdRef(module_name) ; // absorbs 'module_name'

    // Possibly we have only one instance, or one plus additional ones.
    // Later, we might save memory by having a single instance constructor with only one field and no array.
    // Now, we need to merge them into an array any way,
    if (single_inst) {
        _insts.InsertFirst(single_inst) ;
    }

    VeriInstId *inst ;
    unsigned i ;
    // Set the module instantiation back pointer into the instance iddefs :
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (!inst) continue ;
        // Set back-pointers :
        inst->SetModuleInstance(this) ;
    }

    // Set the current user-library (from -v/-y/uselib)
    _library = veri_file::GetCurrentUserLibrary();
}

VeriModuleInstantiation::VeriModuleInstantiation(VeriName *module_name, VeriStrength *strength, Array *param_values, const VeriInstId *single_inst, Array *additional_insts) :
    VeriModuleItem(),
    _module_name(module_name),
    _strength(strength),
    _param_values(param_values),
    _insts((additional_insts) ? additional_insts->Size() : 1),
    _library(0)
{
    if (additional_insts) _insts.Append(additional_insts) ;
    delete additional_insts ;

    // Possibly we have only one instance, or one plus additional ones.
    // Later, we might save memory by having a single instance constructor with only one field and no array.
    // Now, we need to merge them into an array any way,
    if (single_inst) {
        _insts.InsertFirst(single_inst) ;
    }

    VeriInstId *inst ;
    unsigned i ;
    // Set the module instantiation back pointer into the instance iddefs :
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (!inst) continue ;
        // Set back-pointers :
        inst->SetModuleInstance(this) ;
    }

    // Set the current user-library (from -v/-y/uselib)
    _library = veri_file::GetCurrentUserLibrary();
}

VeriModuleInstantiation::~VeriModuleInstantiation()
{
    unsigned i ;
    // Don't delete InstId identifiers. They are owned by the scope
    // But remove the back-pointers in the id's
    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (!inst) continue ;
        inst->SetModuleInstance(0) ;
    }

    VeriExpression *param_value ;
    FOREACH_ARRAY_ITEM(_param_values, i, param_value) delete param_value ;
    delete _param_values ;

    delete _strength ;
    delete _module_name ;
    _library = 0 ;
}

VeriSpecifyBlock::VeriSpecifyBlock(Array *specify_items, VeriScope *scope) :
    VeriModuleItem(),
    _specify_items(specify_items),
    _scope(scope)
{
}
VeriSpecifyBlock::~VeriSpecifyBlock() {
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_specify_items, i, item) delete item ;
    delete _specify_items ;

    // Delete the identifiers in the scope and scope itself
    delete _scope ;
}

VeriPathDecl::VeriPathDecl(unsigned ifnone, VeriExpression *cond, VeriPath *path, Array *delay) :
    VeriModuleItem(),
    _ifnone(ifnone),
    _cond(cond),
    _path(path),
    _delay(delay)
{
}
VeriPathDecl::~VeriPathDecl() {
    delete _cond ;
    delete _path ;
    unsigned i ;
    VeriExpression *item ;
    FOREACH_ARRAY_ITEM(_delay, i, item) delete item ;
    delete _delay ;
}

VeriSystemTimingCheck::VeriSystemTimingCheck(char *task_name, Array *args) :
    VeriModuleItem(),
    _task_name(task_name),
    _args(args),
    _function_type(0)
{
    // Find this system task in the known system task table,
    // and set local token for it. 0 if not known.
    _function_type = VeriExpression::GetFuncToken(task_name) ;

    // VIPER #6674: Check for predefined system timing check functions:
    switch (_function_type) {
    case VERI_SYS_CALL_SETUP:
    case VERI_SYS_CALL_HOLD:
    case VERI_SYS_CALL_SETUPHOLD:
    case VERI_SYS_CALL_RECOVERY:
    case VERI_SYS_CALL_REMOVAL:
    case VERI_SYS_CALL_RECREM:
    case VERI_SYS_CALL_SKEW:
    case VERI_SYS_CALL_TIMESKEW:
    case VERI_SYS_CALL_FULLSKEW:
    case VERI_SYS_CALL_PERIOD:
    case VERI_SYS_CALL_WIDTH:
    case VERI_SYS_CALL_NOCHANGE: break ;
    default:
        // Here, this is not a predefined system timing check function, warn out:
        Warning("%s is not a predefined system timing check", task_name) ;
        break ;
    }

    VeriScope *present_scope = veri_file::GetPresentScope() ;

    // Superset parsing. Implicitly define the eventype arguments (argument 1 and 2) for
    // SystemTimingCheck functions
    if (_args && (veri_file::GetDefaultNettype() != VERI_NONE) && present_scope) {
        unsigned i ;
        VeriExpression *arg ;
        FOREACH_ARRAY_ITEM(_args, i, arg) {
            if (!arg) continue ;
            if (i > 1) break ;
            // Implicitly declare the net of the argument if required:
            if (arg->DeclareImplicitNet(present_scope, veri_file::GetDefaultNettype())) {
                arg->Warning("argument %d of task %s is implicitly defined, violates IEEE 1800 syntax", i+1, _task_name) ;
            }
        }
    }

    // VIPER #3724: Implicitly declare arguments 8 and 9 of $setuphold if they are not defined:
    // VIPER #5571: Do the same thing for $recrem as well:
    if (((_function_type == VERI_SYS_CALL_SETUPHOLD) || (_function_type == VERI_SYS_CALL_RECREM)) &&
        _args && (_args->Size() > 7) && (veri_file::GetDefaultNettype() != VERI_NONE) && present_scope) {
        unsigned i ;
        VeriExpression *arg ;
        FOREACH_ARRAY_ITEM(_args, i, arg) {
            if (!arg || (i < 7)) continue ;
            // Implicitly declare the net of the argument if required:
            (void) arg->DeclareImplicitNet(present_scope, veri_file::GetDefaultNettype()) ;
        }
    }
}

VeriSystemTimingCheck::~VeriSystemTimingCheck() {
    Strings::free(_task_name) ;
    _task_name = 0 ;
    unsigned i ;
    VeriExpression *item ;
    FOREACH_ARRAY_ITEM(_args, i, item) delete item ;
    delete _args ;
}

VeriInitialConstruct::VeriInitialConstruct(VeriStatement *stmt) :
    VeriModuleItem(),
    _stmt(stmt)
{
}
VeriInitialConstruct::~VeriInitialConstruct() {
    delete _stmt ;
}

VeriAlwaysConstruct::VeriAlwaysConstruct(VeriStatement *stmt) :
    VeriModuleItem(),
    _stmt(stmt)
{
}
VeriAlwaysConstruct::~VeriAlwaysConstruct() {
    delete _stmt ;
}

VeriGenerateConstruct::VeriGenerateConstruct(Array *items) :
    VeriModuleItem(),
    _items(items)
{
}
VeriGenerateConstruct::~VeriGenerateConstruct()
{
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) delete item ;
    delete _items ;
}

VeriGenerateConditional::VeriGenerateConditional(VeriExpression *if_expr, VeriModuleItem *then_item, VeriModuleItem *else_item, VeriScope *then_scope, VeriScope *else_scope) :
    VeriModuleItem(),
    _if_expr(if_expr),
    _then_item(then_item),
    _else_item(else_item),
    _then_scope(then_scope),
    _else_scope(else_scope)
{
    if (_then_scope) _then_scope->SetGenerateScope() ;
    if (_else_scope) _else_scope->SetGenerateScope() ;
}
VeriGenerateConditional::~VeriGenerateConditional() {
    delete _if_expr ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        VeriIdDef *block_id = _then_scope ? _then_scope->GetOwner(): 0 ;
        if (block_id) block_id->SetModuleItem(0) ;
        block_id = _else_scope ? _else_scope->GetOwner(): 0 ;
        if (block_id) block_id->SetModuleItem(0) ;
    }
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    delete _then_item ;
    delete _else_item ;

    delete _then_scope ;
    delete _else_scope ;
}

VeriGenerateCase::VeriGenerateCase(unsigned case_style, VeriExpression *cond, Array *case_items) :
    VeriModuleItem(),
    _case_style(case_style),
    _cond(cond),
    _case_items(case_items)
{
}
VeriGenerateCase::~VeriGenerateCase() {
    delete _cond ;
    unsigned i ;
    VeriCaseItem *ci ;
    FOREACH_ARRAY_ITEM(_case_items, i, ci) delete ci ;
    delete _case_items ;
}

VeriGenerateFor::VeriGenerateFor(VeriModuleItem *initial, VeriExpression *cond, VeriModuleItem *rep, VeriIdDef *block_id, Array *items, VeriScope *scope, VeriScope *loop_scope) :
    VeriModuleItem(),
    _initial(initial),
    _cond(cond),
    _rep(rep),
    _block_id(block_id),
    _items(items),
    _scope(scope),
    _loop_scope(loop_scope)
{
    VeriModuleItem *body = (_items && (_items->Size()==1)) ? (VeriModuleItem*)_items->At(0) : 0 ;

    // VIPER #4781: Rule for generate_for is changed in yacc. It will now parse a
    // generate item as its body and add that to its '_items'. So now if design is
    //   for (i = 0; i < 2; i++)
    //   begin : b
    //      ..........
    //   end
    // '_items' contains one element and that is generate block and here _block_id
    // is null. '_scope' is a scope which is upper scope of generate block and child
    // scope of initialization part of generate ('for (i = 0; i < 2; i++)').
    // To populate our this data structure and keep elaboration of for-generate as
    // it is, we will modify this data structure. If body of for-generate is generate
    // block, we will extract its identifier and set that as '_block_id'. We will
    // extract items of generate block and set those as '_items'. We will also set
    // the block scope as '_scope' (i.e. scope of for-generate body) and delete
    // upper scope of block as that is unnecessary at this point.
    if (body && body->IsGenerateBlock() && !_block_id) { // Body if generate block
        _block_id = body->GetId() ; // Set block id as block id of for-generate
        VeriScope *block_scope = body->GetScope() ;
        // Remove the scope '_scope' from '_loop_scope' and add block scope
        // as child scope of loop scope
        // VIPER #8109 : No need to remove _scope, _scope is same as block_scope
        //if (loop_scope) loop_scope->RemoveChildScope(_scope) ;
        // Move the identifiers declared in _scope to _loop_scope, as we will delete
        // the '_scope'
        // VIPER #8109: No need to move identifiers of _scope. If for-generate-item
        // is generate-block, _scope is used as generate-block-scope
#if 0
        MapIter mi ;
        Map *id_map = (_scope) ? _scope->GetThisScope(): 0 ;
        VeriIdDef *id ;
        // VIPER #8109 : Implicit localparam is declared in _scope. Undeclare that
        // from _scope and declare that in block_scope
        VeriIdDef *localparam_id = 0 ;
        VeriIdDef *genvar_id = FindSingleGenVarId() ;
        FOREACH_MAP_ITEM(id_map, mi, 0, &id) {
            if (!id) continue ;
            if (id->Name()[0] == ' ') continue ;  // Do not move internally declared identifiers to upper scope
            if (_scope) (void) _scope->Undeclare(id) ; // Undeclare identifier from its container
            // VIPER #8109 : Implicit localparam is declared only in SystemVerilog mode
            if (IsSystemVeri() && genvar_id && Strings::compare(genvar_id->Name(), id->Name())) {
                localparam_id = id ;
                continue ; // Do not declare it in loop scope
            }
            if (!loop_scope) continue ;
            // Declare that in loop scope. Use different routine to declare block id
            // and other identifiers. 'loop_scope' is an empty scope when no genvar
            // is declared in initialization part of for-loop. So for loop like
            // for (i = 0 ; i <....)
            // begin : block
            // identifier 'block' should be declared in the container scope of for-loop
            // for (genvar i = 0 ...) begin : block

            // 'block' should be declared in the '_loop_scope'.
            VeriIdDef *exist = 0 ;
            if (id->IsBlock()) {
               if (!loop_scope->DeclareBlockId(id, 0 /* no force insert */, &exist)) {
                   id->Error("%s is already declared", id->Name()) ;
                   if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
                   // We are going to leak 'id' here, but cannot delete it since it may already have some references.
               }
            } else {
                if (!loop_scope->Declare(id, 0 /* no force insert */, &exist)) {
                   id->Error("%s is already declared", id->Name()) ;
                   if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
                   // We are going to leak 'id' here, but cannot delete it since it may already have some references.
                }
            }
        }
#endif
        // Undeclare gen_block_label id from this block scope. It is created
        // inside the constructor of VeriGenerateBlock and it helps the elaboration.
        // But now we will elaborate elements of generate block as part of for-loop.
        // So this internally declared variable is not needed any more.
        if (block_scope) {
            VeriIdDef *l_id = block_scope->FindLocal(" gen_block_label") ;
            if (l_id) (void) block_scope->Undeclare(l_id) ;
            delete l_id ; // Delete this orphan identifier
            // Set block scope as child scope of loop scope.
            //block_scope->SetUpper(loop_scope) ;
        }
        //delete _scope ; // Delete current body scope
        //_scope = block_scope ; // Set block scope as body scope of for-loop
        delete _items ; // Delete current items
        _items = body->GetItems() ; // Add items of generate block as items of for-loop
        Array *comments = body->TakeComments() ;
        if (comments) {
            // If comments are attached with generate block, move those to first
            // item of generate block (comments were attached there before fixing #4781)
            VeriModuleItem *first_item = (_items && _items->Size()) ? (VeriModuleItem*)_items->At(0): 0 ;
            if (first_item) first_item->AddComments(comments, 1 /*prepend*/) ;
        }
        body->DeleteOnlyBlock() ; // Delete the created generate block excluding its elements (we have added those in for-loop)
        body = 0 ; // It is deleted from DeleteOnlyBlock, reset it to fix corruption
    }
    // Since this is a loop, create a loop label, so we can find out (in Resolve) if we are inside a loop.
    // This constructor is also called while copying for-generate statements and there loop_label iddef is already
    // copied in _scope.
    if (_scope && !_scope->FindLocal(" gen_loop_label")) {
        VeriIdDef *loop_label = new VeriBlockId(Strings::save(" gen_loop_label")) ;
        // Set 'this' as the module item to this label.
        // So we can get back to the module item if needed.
        loop_label->SetModuleItem(this) ;
        // Declare in this local scope, so it gets deleted at some point :
        (void) _scope->Declare(loop_label) ;
    }

    // LRM 12.1.3 :
    // Check whether the body of this for-loop contains named generate block,
    // if not produce warning :

    if (!_block_id) {
//#ifndef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        if (!RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
            // Should actually be an error
            if (body) { // Use generate-for item to produce warning with proper linefile
                body->Warning("block identifier is required on this block") ;
            } else {
                Warning("block identifier is required on this block") ;
            }
        }
//#endif
    } else {
        // Set 'this' as the module item of this block label.
        _block_id->SetModuleItem(this) ;
    }
    if (_scope) _scope->SetGenerateScope() ;
    if (_loop_scope) _loop_scope->SetGenerateScope() ;
}
VeriGenerateFor::~VeriGenerateFor() {
    delete _initial ;
    delete _cond ;
    delete _rep ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        delete item ;
    }
    delete _items ;
    // block_id is (as all VeriIdDefs) owned by the (upper) scope..
    if (_block_id) _block_id->ClearAllBckPtrs() ; // Set back pointer null
    _block_id = 0 ;
    delete _scope ;
    delete _loop_scope ;
}

VeriGenerateBlock::VeriGenerateBlock(VeriIdDef *block_id, Array *items, VeriScope *scope) :
    VeriModuleItem(),
    _block_id(block_id),
    _items(items),
    _scope(scope)
    ,_is_elaborated(0)
{
    // Set 'this' as the module item of this block label.
    if (_block_id) _block_id->SetModuleItem(this) ;
    // VIPER #6673 : Treat label id of generate block as block label of generate block
    // Set scope back-pointer
    if (_block_id && _scope && !_scope->GetOwner()) {
        _block_id->SetLocalScope(_scope) ;
        _scope->SetOwner(_block_id) ;
    }
    if (_scope) _scope->SetGenerateScope() ;
}
VeriGenerateBlock::~VeriGenerateBlock() {
    // _block_id is (as all VeriIdDef's) owned by the (upper) scope. So don't delete it.
    if (_block_id) _block_id->SetModuleItem(0) ; // Set back pointer null
    _block_id = 0 ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        delete item ;
    }
    delete _items ;
    delete _scope ;
}
void VeriGenerateBlock::DeleteOnlyBlock()
{
    if (_block_id) _block_id->SetModuleItem(0) ; // Set back pointer null
    _block_id = 0 ;
    _items = 0 ;
    VeriIdDef *label = (_scope) ? _scope->FindLocal(" gen_block_label"): 0 ;
    if (label) label->ClearAllBckPtrs() ;
    _scope = 0 ;
    delete this ;
}

VeriTable::VeriTable(Array *table_entries) :
    VeriModuleItem(),
    _table_entries(table_entries),
    _the_module(0),
    _is_comb(0),
    _is_seq(0)
{}

VeriTable::~VeriTable()
{
    unsigned i ;
    char *item ;
    FOREACH_ARRAY_ITEM(_table_entries, i, item)  Strings::free(item) ;
    delete _table_entries ;
    // _the_module is a back-pointer
    _the_module = 0 ;
}

// Additional System Verilog module items :
VeriClass::VeriClass(VeriIdDef *id, Array *parameter_connects, VeriName *base_class_name, Array *items, VeriScope *scope, Array *interface_classes, unsigned interface_class) :
    VeriModuleItem(),
    _id(id),
    _new_id(0),
    _parameter_connects(parameter_connects),
    _base_class_name(base_class_name),
    _interface_classes(interface_classes),
    _items(items),
    _scope(scope),
    _parameters(0),
    _has_static_members(0),
    _is_processing(0)
    , _is_instantiated(0)
    , _contain_copied_version(0)
    , _is_resolved(0)
    , _orig_class_name(0)
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    , _owning_scope(scope ? scope->Upper(): 0)
#endif
{
    if (interface_class) SetQualifier(VERI_INTERFACE) ;
    // Set backpointer on id back to this
    if (_id) _id->SetModuleItem(this) ;

    // Create _parameters arrays, from the parameter_connects declarations :
    // parameter list (of VeriModuleItems)
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_parameter_connects, i, item) {
        if (!item) continue ;
        if (!_parameters) _parameters = new Array(_parameter_connects->Size()) ;
        item->AccumulateParameters(*_parameters, 0 /* do not check for ANSI param: this is the ANSI param */, _id) ;
    }
    // Iterate over class items and mark the functions automatic if 'static'
    // qualifier is not specified :
    FOREACH_ARRAY_ITEM(_items, i, item) {
        // VCS Compatibility issue (class_localparam): P1800-LRM(2009) Section: 8.3
        // In a parameter_declaration that is a class_item, the parameter
        // keyword shall be a synonym for the localparam keyword.
        // Therefore, mark parameters as 'localparam',
        // so that elaboration will error out if they get overwritten:
        if (item && item->IsParamDecl()) {
            if (_id) item->Warning("parameter declaration becomes local in %s with formal parameter declaration list", _id->Name()) ;
            // Change the identifiers' ParamType :
            Array *all_ids = item->GetIds() ;
            unsigned j ;
            VeriIdDef *param_id ;
            FOREACH_ARRAY_ITEM(all_ids, j, param_id) {
                if (param_id) param_id->SetParamType(VERI_LOCALPARAM) ;
            }
            continue ;
        }
        if (!item || !(item->IsFunctionDecl() || item->IsTaskDecl())) continue ;
        // VIPER #4030 : Functions declared inside a class are automatic if not
        // explicitly specified as static
        // VIPER #4853 : Tasks declared inside a class are automatic if not
        // explicitly specified as static
        VeriIdDef *func_task_id = item->GetId() ;
        if (func_task_id && !func_task_id->IsAutomatic() && !item->GetQualifier(VERI_STATIC)) {
            func_task_id->SetIsAutomatic() ;
        }
        if (func_task_id && func_task_id->GetFunctionType()==VERI_METHOD_NEW) {
            // VIPER #5522 : Check whether it is out-of-block declaration of 'new'
            // method for another class.
            VeriName *method_name = item->GetSubprogramName() ;
            if (method_name && method_name->IsScopeName()) { // It is like c::new
                VeriName *prefix = method_name->GetPrefix() ;
                VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
                if (prefix_id && prefix_id->IsClass() && (prefix_id != _id)) {
                    continue ; // This is out_of_block declaration of new
                }
            }
            if (!_new_id) {
                _new_id = func_task_id ;
            } else {
                Warning("multiple definitions of new in class %s, taking the first one", id->Name()) ;
            }
        }
    }

    // VIPER #5717 (test6.sv): Set resolved base class scope in constructor:
    // Note: The following code is duplicated from VeriClass::Resolve() routine.
    // Now pick-up the already resolved base class identifier (if resolved):
    VeriIdDef *base_class = (_base_class_name) ? _base_class_name->GetId() : 0 ;
    // If the base class is a type parameter, take its initial value:
    if (base_class && base_class->IsParam() && base_class->IsType()) {
        VeriExpression *initial = base_class->GetInitialValue() ;
        base_class = (initial) ? initial->GetId() : 0 ;
    }
    // If this identifier is not there, then it was not declared and error is already given.
    // Here, we just need to pick up its scope, and set our own 'upper' pointer to that :
    if (_scope && base_class && base_class->LocalScope()) {
        // NOTE : This (SetUpper()) is risky, and not normally done.
        // Check if this causes problems elsewhere (in tree copying etc)..
        // Check if this (scope points to base class scope only) causes other Verilog scope semantic problems..
        // Otherwise, we might need an 'extended' scope pointer in VeriScope for base-class scope visibility..
        // VIPER #4954: Do not use SetUpper(), added a base class pointer in VeriScope, set it from here:
        _scope->SetBaseClassScope(base_class->LocalScope()) ;
    }
}

VeriClass::~VeriClass()
{
    // Delete the arrays (created at construction time) of IdDefs :
    delete _parameters ;

    // Delete the owned parse tree sub-nodes :
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_parameter_connects, i, item) delete item ;
    delete _parameter_connects ;

    delete _base_class_name ;

    VeriName *interface_class ;
    FOREACH_ARRAY_ITEM(_interface_classes, i, interface_class) delete interface_class ;
    delete _interface_classes ;

    FOREACH_ARRAY_ITEM(_items, i, item) delete item ;
    delete _items ;

    delete _scope ; // Delete the identifiers in the scope and scope itself

    _new_id = 0 ;
    // don't delete _id itself : its owned by the scope above.
    if (_id) _id->ClearAllBckPtrs() ; // VIPER #3369
    _id = 0 ;
    Strings::free(_orig_class_name) ; // free the allocated string
    _orig_class_name = 0 ;
#ifndef VERILOG_ID_SCOPE_BACKPOINTER
    _owning_scope = 0 ;
#endif
}

VeriIdDef *VeriClass::GetBaseClassId(unsigned &is_base_typeparam)
{
    VeriIdDef *class_id = _id ;
    while (class_id && class_id->IsClass()) {
        VeriModuleItem *class_decl = class_id->GetModuleItem() ;
        VeriName *base_class = (class_decl) ? class_decl->GetBaseClassName() : 0 ;
        // No base class reference, it is base class
        if (!base_class) return class_id ;

        VeriIdDef *super_id = base_class->GetId() ;
        if (super_id && super_id->IsClass()) {
            class_id = super_id ;
            continue ;
        }
        if (super_id && super_id->IsParam() && super_id->IsType()) {
            is_base_typeparam = 1 ;
            return 0 ;
        }
        break ;
    }
    return 0 ;
}

// Viper 7398: Added param_list in the argument list to create name with
// parameter values.
char *VeriClass::CreateNameWithDefaultparam(Array *param_list /*= 0*/) const
{
    if (!_id) return 0 ;
    unsigned i ;
    VeriIdDef *param_id ;
    VeriExpression *expr ;
    char *class_name = 0, *image = 0 ;
    VeriExpression *actual ;
    const char *formal_name ;
    VeriIdDef *formal ;

    Map param_map(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(param_list, i, actual) {
        if (!actual) continue ;
        formal_name = actual->NamedFormal() ;
        if (formal_name) {
            // Actual is a 'PortConnect'.
            actual = actual->GetConnection() ;
        }
        formal = 0 ;
        // Find the formal itself if we can :
        if (!_parameters || !_parameters->Size()) {
            break ;
        }
        if (formal_name) {
            formal = (_scope) ? _scope->FindLocal(formal_name) : 0 ;
            if (!formal) {
                actual->Error("class %s does not have a parameter named %s", _id->Name(), formal_name) ;
                continue ;
            }
        } else {
            if (_parameters->Size() <= i) {
                break ;
            }
            formal = (VeriIdDef*)_parameters->At(i) ;
            formal_name = (formal) ? formal->Name() : 0 ;
        }
        if (!formal_name || !formal) continue ;
        (void) param_map.Insert(formal->Name(), actual) ;
    }

    FOREACH_ARRAY_ITEM(_parameters, i, param_id) { // Iterate over parameters to consider value
        image = 0 ;
        expr = param_id ? (VeriExpression*)param_map.GetValue(param_id->GetName()) : 0 ;
        if (!expr) expr = param_id ? param_id->GetInitialValue() : 0 ;

        if (!expr || !param_id) continue ;
        VeriConstraint *param_constraint = param_id->EvaluateConstraintInternal(0, 0, 0) ;
        if (param_id->IsType()) {
            if (IsStaticElab()) {
                VeriScope *parent = _id->GetOwningScope() ;
                VeriExpression *new_expr = expr->StaticEvaluateToExprInternal(0,0, param_constraint, param_id, 0) ;
                image = new_expr ? new_expr->StaticImage(parent, 0): expr->StaticImage(parent, 0) ;
                delete new_expr ;
            }
            if (!image) image = expr->Image() ;
        } else {
            if (IsStaticElab()) {
                VeriBaseValue *val = expr->StaticEvaluateInternal(0, 0, param_constraint, 0) ;
                image = val ? val->Image(): 0 ;
                delete val ;
            }
        }
        delete param_constraint ;
        // Char* equivalent of parameter value can have space, replace that
        // space with '_'. FIXME : This replacement can produce name collision
        if (image) {
            char *str = image ;
            while(*str) {
                if (*str == ' ') *str = '_' ; // Replace space with '_'
                str++ ;
            }
        }
        if (!class_name) {
            class_name = Strings::save("(", param_id->Name(), "=", image) ;
        } else {
            char *tmp = class_name ;
            class_name = Strings::save(tmp, ",", param_id->Name(), "=", image) ;
            Strings::free(tmp) ;
        }
        Strings::free(image) ;
    }
    char *tmp = class_name ;
    if (tmp) { // Parameterized class
        class_name = Strings::save(_id->Name(), tmp, ")") ;
    } else { // Not Parameterized, append _default
        class_name = Strings::save(_id->Name()) ;
    }
    Strings::free(tmp) ;
    return class_name ;
}

VeriPropertyDecl::VeriPropertyDecl(VeriIdDef *id, Array *ansi_io_list, Array *decls, VeriExpression *spec, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _ansi_io_list(ansi_io_list),
    _decls(decls),
    _spec(spec),
    _scope(scope),
    _ports(0),
    _is_processing(0),
    _feedback_net(0),
    _formal_to_actual(0)
{
    // Set the module item pointer on the 'id' back to this :
    if (_id) _id->SetModuleItem(this) ;

    // Issue 2540 : Sequence now has a ansi port list.
    unsigned i ;

    // Set-up port list : done by order
    _ports = new Array() ;

    // Accumulate ports from the ansi port list
    VeriAnsiPortDecl *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->AccumulatePorts(*_ports) ;
        // VIPER #6333 : If port is declared as 'local', direction and type
        // should be specified. Alternatively if direction is specified, local
        // keyword should also be specified
        if (port_decl->IsLocal() && !port_decl->GetDataType()) {
            port_decl->Error("type must be specified for local variable formal argument") ;
        }
        if (port_decl->GetDir() && !port_decl->IsLocal()) {
            port_decl->Error("keyword local is missing for port item with direction") ;
        }
        // VIPER #6335 : Ports with 'local' keyword are local variables, mark those
        if (port_decl->IsLocal()) {
            unsigned j ;
            VeriIdDef *id2 ;
            Array *ids = port_decl->GetIds() ;
            FOREACH_ARRAY_ITEM(ids, j, id2) {
                if (id2) id2->SetIsLocalVar() ;
            }
            // VIPER #6336 : The type of local variables has some restrictions according
            // to SV-2009 LRM section 16.6.1. Check here
            VeriDataType *data_type = port_decl->GetDataType() ;
            VeriTypeInfo *ptype = data_type ? data_type->CreateType(0): 0 ;
            if (ptype && (ptype->IsRealType() || ptype->IsString() || ptype->IsEventType()
                      || ptype->IsChandleType() || ptype->IsClassType()
                      || ptype->IsAssociativeArrayType() || ptype->IsDynamicArrayType())) {
                port_decl->Error("illegal type for local variable formal argument") ;
            }
            delete ptype ;
        }
    }

    // _decls cannot (syntax) contain port declarations, so no need to check.

    // Verify sequence ports. Do same way (but simplified) as we do for functions.
    VeriIdDef *portid ;
    // Default direction setting : SV 1800 LRM 17.6.1: LRM says nothing about direction for sequence ports,
    // but 'input' should be the only one allowed. Actually, ModelSim errors out (syntax) on any port with direction.

    // NOTE : we can only set the default 'direction' here, not the default 'type'.
    // Default type is only known at 'Resolve' time (due to forward type decls).
    unsigned previous_dir = VERI_INPUT ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_ports, i, portid) {
        if (!portid) continue ;
        // get direction of this port :
        dir = portid->Dir() ;
        // Inherit previous port direction :
        if (!dir) {
            dir = previous_dir ;
            portid->SetDir(dir) ;
        }
        // Check if there are any inputs.
        switch (dir) {
        case VERI_INPUT: break ;
        case VERI_OUTPUT:
        case VERI_INOUT:
            // FIX ME: should give 'sequence' specific message ?
            portid->Error("output or inout not allowed in functions") ; break ;
        default:
            break ;
        }
        // update previous port direction
        previous_dir = dir ;
    }
}

VeriPropertyDecl::~VeriPropertyDecl()
{
    // Delete the owned parse tree sub-nodes :
    unsigned i ;
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) delete port_decl ;
    delete _ansi_io_list ;

    // Delete local declarations :
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decls, i, item) delete item ;
    delete _decls ;

    delete _spec ;  // Delete the (property) spec

    delete _scope ; // Delete the identifiers in the scope and scope itself

    // Delete the created list of ports
    delete _ports ;

    // don't delete _id itself : its owned by the scope above.
    if (_id) _id->ClearAllBckPtrs() ; // VIPER #3369
    _id = 0 ;
    delete _formal_to_actual ;
   _feedback_net = 0 ;
}

VeriSequenceDecl::VeriSequenceDecl(VeriIdDef *id, Array *ansi_io_list, Array *decls, VeriExpression *spec, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _ansi_io_list(ansi_io_list),
    _decls(decls),
    _spec(spec),
    _scope(scope),
    _ports(0),
    _is_processing(0)
{
    // Set the module item pointer on the 'id' back to this :
    if (_id) _id->SetModuleItem(this) ;

    // Issue 2540 : Sequence now has a ansi port list.
    unsigned i ;

    // Set-up port list : done by order
    _ports = new Array() ;

    // Accumulate ports from the ansi port list
    VeriAnsiPortDecl *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->AccumulatePorts(*_ports) ;
        // VIPER #6333 : If port is declared as 'local', direction and type
        // should be specified. Alternatively if direction is specified, local
        // keyword should also be specified
        if (port_decl->IsLocal() && !port_decl->GetDataType()) {
            port_decl->Error("type must be specified for local variable formal argument") ;
        }
        if (port_decl->GetDir() && !port_decl->IsLocal()) {
            port_decl->Error("keyword local is missing for port item with direction") ;
        }
        // VIPER #6335 : Ports with 'local' keyword are local variables, mark those
        if (port_decl->IsLocal()) {
            unsigned j ;
            VeriIdDef *id2 ;
            Array *ids = port_decl->GetIds() ;
            FOREACH_ARRAY_ITEM(ids, j, id2) {
                if (id2) id2->SetIsLocalVar() ;
            }
            // VIPER #6336 : The type of local variables has some restrictions according
            // to SV-2009 LRM section 16.6.1. Check here
            VeriDataType *data_type = port_decl->GetDataType() ;
            VeriTypeInfo *ptype = data_type ? data_type->CreateType(0): 0 ;
            if (ptype && (ptype->IsRealType() || ptype->IsString() || ptype->IsEventType()
                      || ptype->IsChandleType() || ptype->IsClassType()
                      || ptype->IsAssociativeArrayType() || ptype->IsDynamicArrayType())) {
                port_decl->Error("illegal type for local variable formal argument") ;
            }
            delete ptype ;
        }
    }

    // _decls cannot (syntax) contain port declarations, so no need to check.

    // Verify sequence ports. Do same way (but simplified) as we do for functions.
    VeriIdDef *portid ;
    // Default direction setting : SV 1800 LRM 17.6.1: LRM says nothing about direction for sequence ports,
    // but 'input' should be the only one allowed. Actually, ModelSim errors out (syntax) on any port with direction.

    // NOTE : we can only set the default 'direction' here, not the default 'type'.
    // Default type is only known at 'Resolve' time (due to forward type decls).
    unsigned previous_dir = VERI_INPUT ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_ports, i, portid) {
        if (!portid) continue ;
        // get direction of this port :
        dir = portid->Dir() ;
        // Inherit previous port direction :
        if (!dir) {
            dir = previous_dir ;
            portid->SetDir(dir) ;
        }
        // VIPER #6332 : Output/inout ports are allowed in sequence decl
        // according to SV-P1800-2009 LRM section 16.8
        // Check if there are any inputs.
        // VIPER #6334 : Default actual argument is not allowed for inout/output port
        switch (dir) {
        case VERI_OUTPUT:
        case VERI_INOUT:
            if (portid->GetInitialValue()) {
                portid->Error("default actual argument for inout/output port %s is not allowed", portid->Name()) ;
            }
            break ;
        default:
            break ;
        }
        // update previous port direction
        previous_dir = dir ;
    }
}

VeriSequenceDecl::~VeriSequenceDecl()
{
    // Delete the owned parse tree sub-nodes :
    unsigned i ;
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) delete port_decl ;
    delete _ansi_io_list ;

    // Delete local declarations :
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decls, i, item) delete item ;
    delete _decls ;

    delete _spec ;  // Delete the (property) spec

    delete _scope ; // Delete the identifiers in the scope and scope itself

    // Delete the created list of ports
    delete _ports ;

    // don't delete _id itself : its owned by the scope above.
    if (_id) _id->ClearAllBckPtrs() ; // VIPER #3369
    _id = 0 ;
}

VeriModport::VeriModport(Array *modport_decls) :
    VeriModuleItem(),
    _modport_decls(modport_decls)
{
}

VeriModport::~VeriModport()
{
    unsigned i ;
    VeriModuleItem *modport_decl ;
    FOREACH_ARRAY_ITEM(_modport_decls, i, modport_decl) delete modport_decl ;
    delete _modport_decls ;
}

VeriModportDecl::VeriModportDecl(VeriIdDef *id, Array *modport_port_decls, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _modport_port_decls(modport_port_decls),
    _scope(scope),
    _ports(0)
{
    // Set the module item pointer on the 'id' back to this :
    if (_id) _id->SetModuleItem(this) ;

    // ModportDecl is a bit like a 'function' decl :
    // it has a name, and a list of port declarations.
    // The port declarations are special, since they are ModportPortId's which are 'prototype'
    // identifiers. (need to be resolved to find their true definition with data type etc).
    // The 'direction' of a modportport however is only available from the ModportPortId.

    // Accumulate all modport_port's (as VeriIdDef's) and collect them in 'ports' array.
    // Not sure if this is needed for modportports, since I'm not sure if System Verilog
    // requires modport_ports in a modport to be accessible by order..
    unsigned i ;
    VeriModuleItem *modport_decl ;
    FOREACH_ARRAY_ITEM(_modport_port_decls, i, modport_decl) {
        if (!modport_decl) continue ;
        if (!_ports) _ports = new Array(_modport_port_decls->Size()) ;
        if (modport_decl->GetId()) {
            // VIPER 2620. This modport decl item is a clocking declaration.
            // Insert clocking decl id in modport's portlist.
            _ports->InsertLast(modport_decl->GetId()) ;
        } else {
            modport_decl->AccumulatePorts(*_ports) ;
        }
    }

    // Also, set each port as being a modport-port.
    // Each of these is like a 'member' field in a modport.
    VeriIdDef *port ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        port->SetIsModportPort() ;
    }
}

VeriModportDecl::~VeriModportDecl()
{
    // don't delete _id. Is owned by upper scope.
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_modport_port_decls, i, item) delete item ;
    delete _modport_port_decls ;

    // delete the local scope and everything in it
    delete _scope ;

    // delete the ordered array of id's.
    delete _ports ;
    if (_id) _id->ClearAllBckPtrs() ; // VIPER #3369
    _id = 0 ;
}

VeriClockingDecl::VeriClockingDecl(VeriIdDef *id, VeriExpression *clocking_event, Array *clocking_items, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _clocking_event(clocking_event),
    _clocking_items(clocking_items),
    _scope(scope)
{
    // Set back-pointer from id to this clocking decl :
    // Do not set parse tree like 'default clocking id;' as back pointer
    if (_id && _scope) _id->SetModuleItem(this) ;
}

VeriClockingDecl::~VeriClockingDecl()
{
    delete _clocking_event ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_clocking_items, i, item) delete item ;
    delete _clocking_items ;

    delete _scope ; // Delete the identifiers in the scope and scope itself
    // don't delete _id itself : its owned by the scope above.

    if (_id) _id->ClearAllBckPtrs() ; // VIPER #3369
    _id = 0 ;
}

VeriConstraintDecl::VeriConstraintDecl(VeriIdDef *id, Array *constraint_blocks) :
    VeriModuleItem(),
    _id(id),
    _constraint_blocks(constraint_blocks),
    _scope(0),
    _name(new VeriIdRef(id))
{
    // Set the module item pointer on the 'id' back to this :
    if (_id) _name->SetLinefile(_id->Linefile()) ;
    if (_id) _id->SetModuleItem(this) ;
}
VeriConstraintDecl::VeriConstraintDecl(VeriName *name, Array *constraint_blocks, VeriScope *scope) :
    VeriModuleItem(),
    _id(0),
    _constraint_blocks(constraint_blocks),
    _scope(scope),
    _name(name)
{
    // Set the module item pointer on the 'id' back to this :
    _id = (_name)? _name->GetId() : 0 ;
    if (_id) _id->SetModuleItem(this) ;
}

VeriConstraintDecl::~VeriConstraintDecl()
{
    // don't delete _id. Is owned by upper scope.
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_constraint_blocks, i, expr) delete expr ;
    delete _constraint_blocks ;
    // if (_id) _id->ClearAllBckPtrs() ; // Previously added for VIPER #3369, commented for VIPER# 4179
    _id = 0 ;
    _scope = 0 ;
    delete _name ;
}

VeriBindDirective::VeriBindDirective(const VeriName *target_scope, VeriModuleItem *instantiation, VeriScope *bind_scope) :
    VeriModuleItem(),
    _target_mod_name(0),
    _target_inst_list(0),
    _instantiation(instantiation),
    _scope(bind_scope)
    ,_is_static_elaborated(0)
{
    _target_inst_list = new Array() ;
    if (target_scope) _target_inst_list->InsertLast(target_scope) ;
}

VeriBindDirective::VeriBindDirective(char *target_mod_name, Array *target_inst_list, VeriModuleItem *instantiation, VeriScope *bind_scope) :
    VeriModuleItem(),
    _target_mod_name(target_mod_name),
    _target_inst_list(target_inst_list),
    _instantiation(instantiation),
    _scope(bind_scope)
    ,_is_static_elaborated(0)
{
}

VeriBindDirective::~VeriBindDirective()
{
    // Delete the target module name
    Strings::free(_target_mod_name) ;

    // Delete the target instantiation list
    unsigned i ;
    VeriName *hier_name ;
    FOREACH_ARRAY_ITEM(_target_inst_list, i, hier_name) delete hier_name ;
    delete _target_inst_list ;

    // Delete the instantiation
    delete _instantiation ;

    // Delete the local scope
    delete _scope ;
}

VeriOperatorBinding::VeriOperatorBinding(VeriIdDef *overload_operator, VeriDataType *return_type, VeriName *func_name, Array *formals) :
    VeriModuleItem(),
    _oper_id(overload_operator),
    _data_type(return_type),
    _func_name(func_name),
    _formals(formals)
{
    // Set backpointer on id back to this
    if (_oper_id) _oper_id->SetModuleItem(this) ;
}

VeriOperatorBinding::~VeriOperatorBinding()
{
    // Delete the owned parse tree sub-nodes :
    if (_oper_id) _oper_id->ClearAllBckPtrs() ; // VIPER #3369
    _oper_id = 0 ; // don't delete _oper_id itself : its owned by the scope above.
    delete _data_type ;
    delete _func_name ;
    unsigned i ;
    VeriDataType *formal ;
    FOREACH_ARRAY_ITEM(_formals, i, formal) delete formal ;
    delete _formals ;
}
VeriNetAlias::VeriNetAlias(VeriExpression *net_lvalue, Array *net_lvalues) :
    VeriModuleItem(),
    _left(net_lvalue),
    _members(net_lvalues)
{ }
VeriNetAlias::~VeriNetAlias()
{
    // Delete the owned parse tree sub-nodes :
    delete _left ;
    VeriName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_members, i, name) delete name ;
    delete _members ;
}
VeriTimeUnit::VeriTimeUnit(unsigned type, VeriExpression *time_literal, VeriExpression *time_precision) :
    VeriModuleItem(),
    _type(type),
    _literal(time_literal),
    _time_precision(time_precision)
{
    CheckTimeLiteral(_literal, 0) ;

    // VIPER #8304 : Check for incorrect time precision value
    CheckTimeLiteral(_time_precision, 1 /* time precision*/) ;
}
void VeriTimeUnit::CheckTimeLiteral(const VeriExpression *literal, unsigned time_precision)
{
    // VIPER #6986: Check for incorrect time literal value.
    // LRM says it must be order of magnitude of 1, 10 or 100.
    // Just check the value to be power of 10 here:
    char *time_image = (literal) ? literal->Image() : 0 ;
    if (!time_image) return ;
    // Note that we are going to check it in 'integer' domain by checking
    // integral and fraction part separately one by one.
    // Also note that it came from flex rule which parses only
    // 'unsigned number' or 'unsigned number.unsigned number' format.
    // Checking in double domain does not seem to be reliable.
    //double power_of_10 = (time_value>0) ? log10(time_value) : 0 ;
    //if ((time_value<=0) || (power_of_10 != ceil(power_of_10))) {
    //    // Found an incorrect timeunit/timeprecision literal value, error out:
    //    Error("%s literal not a power of 10", PrintToken(_type)) ;
    //}

    // Find the fraction:
    char *fraction = strchr(time_image, '.') ;
    if (fraction) { *fraction = '\0' ; fraction++ ; }

    // It should not be -ve but for a very large +ve number we may get a -ve:
    unsigned incorrect_value = (*time_image == '-') ? 1 : 0 ;
    unsigned time_value = 0 ;
    // First check integer part:
    if (!incorrect_value) {
        unsigned overflow = 0 ;
        time_value = Strings::atoi(time_image, &overflow) ;
        if (overflow) Error("decimal constant %s is too large, using %d instead", time_image, time_value) ;
        unsigned runner = time_value ;
        while (runner && !(runner % 10)) runner = runner / 10 ;
        // Incorrect if result of the above division is not 1.
        // It may not be 1 but still be a correct value, in case of
        // (1) the value was 0 AND (2) there is a fraction part.
        if ((runner != 1) && (time_value || !fraction)) incorrect_value = 1 ;
    }

    // Then check fraction part:
    if (!incorrect_value && fraction) {
        unsigned overflow = 0 ;
        unsigned time_frac = Strings::atoi(fraction, &overflow) ;
        if (overflow) Error("decimal constant %s is too large, using %d instead", fraction, time_frac) ;
        unsigned runner = time_frac ;
        while (runner && !(runner % 10)) runner = runner / 10 ;
        // Incorrect if result of the above division is not 1.
        // It may not be 1 but still be a correct value, in case of
        // (1) the fraction was 0 AND (2) the integral part is not 0 (and correct).
        if ((runner != 1) && (time_frac || !time_value)) incorrect_value = 1 ;
        if ((time_frac > 0) && (time_value != 0)) {
            Error("%s literal not a power of 10", time_precision ? "timeprecision" : PrintToken(_type)) ;
            incorrect_value = 1 ; // 10.01 not allowed, 0.001 allowed
        }
    }

    // VIPER #6986: Issue error if it is an incorrect value:
    // FIXME: Change error message, the value may be -ve or 0 too.
    if (incorrect_value) Error("%s literal not a power of 10", time_precision ? "timeprecision" : PrintToken(_type)) ;

    Strings::free(time_image) ; // Clean-up
}
VeriTimeUnit::~VeriTimeUnit()
{
    delete _literal ; // Delete time literal
    delete _time_precision ;
}
VeriClockingSigDecl::VeriClockingSigDecl(VeriClockingDirection *clocking_dir, VeriClockingDirection *o_clocking_dir, Array *ids) :
    VeriDataDecl(0, 0, ids),
    _clk_dir(clocking_dir),
    _o_clk_dir(o_clocking_dir)
{
    // VIPER #4981 : Set direction to clocking signals
    if (ids && clocking_dir) {
        unsigned dir = clocking_dir->GetDirection() ;
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(&_ids, i, id) {
            if (id && dir) id->SetDir(dir) ;
        }
    }
}
VeriClockingSigDecl::~VeriClockingSigDecl()
{
    delete _clk_dir ; // Delete clocking directions
    delete _o_clk_dir ;
}
VeriCovergroup::VeriCovergroup(VeriIdDef *id, Array *port_list, VeriExpression *event, Array *items, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _port_connects(port_list),
    _event(event),
    _sample_func(0),
    _items(items),
    _scope(scope),
    _ports(0)
{
    if (_id) {
        // Set back-pointer from id to this covergroup :
        _id->SetModuleItem(this) ;
        _id->SetType(VERI_COVERGROUP) ; // Set the type to covergroup
    }

    // Create _ports arrays, from the port_connects declarations :
    // port list (of VeriExpressions)
    unsigned i ;
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (!port_expr) continue ;
        if (!_ports) _ports = new Array(_port_connects->Size()) ;
        port_expr->AccumulatePorts(*_ports) ;
    }
}
VeriCovergroup::VeriCovergroup(VeriIdDef *id, Array *port_list, VeriExpression *event, VeriModuleItem *sample_func, Array *items, VeriScope *scope) :
    VeriModuleItem(),
    _id(id),
    _port_connects(port_list),
    _event(event),
    _sample_func(sample_func),
    _items(items),
    _scope(scope),
    _ports(0)
{
    if (_id) {
        // Set back-pointer from id to this covergroup :
        _id->SetModuleItem(this) ;
        _id->SetType(VERI_COVERGROUP) ; // Set the type to covergroup
    }

    // Create _ports arrays, from the port_connects declarations :
    // port list (of VeriExpressions)
    unsigned i ;
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (!port_expr) continue ;
        if (!_ports) _ports = new Array(_port_connects->Size()) ;
        port_expr->AccumulatePorts(*_ports) ;
    }
}
VeriCovergroup::~VeriCovergroup()
{
    unsigned i ;
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) delete port_expr ;
    delete _port_connects ;

    delete _event ;
    delete _sample_func ;
    delete _ports ;

    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) delete item ;
    delete _items ;

    delete _scope ; // Delete the identifiers in the scope and scope itself

    if (_id) _id->ClearAllBckPtrs() ;
    // don't delete _id itself : its owned by the scope above.
    _id = 0 ;
}
VeriCoverageOption::VeriCoverageOption(VeriName *option, VeriExpression *value) :
    VeriModuleItem(),
    _option(option),
    _value(value)
{}
VeriCoverageOption::~VeriCoverageOption()
{
    delete _option ;
    delete _value ;
}
VeriCoverageSpec::VeriCoverageSpec(unsigned type, VeriIdDef *id, VeriExpression *expr, VeriExpression *iff_expr, Array *bins, VeriScope *scope) :
    VeriModuleItem(),
    _type(type),
    _id(id),
    _point_list(0),
    _iff_expr(iff_expr),
    _bins(bins),
    _scope(scope)
{
    // Set back-pointer from id to this coverage spec :
    if (_id) _id->SetModuleItem(this) ;

    if (expr) {
        // VIPER #2881: As per LRM IEEE Std 1800 sec 18.4, a coverpoint  does
        // not have a label specified but has got a single variable associated
        // with it then it is to be referred by the variable.
        // Now, as 'option' and 'type_option' are implicitly declared structures
        // within a covergroup, declaring a coverpoint with name as 'option'/'type_option'
        // will cause an error.
        // VIPER #4704 : Do not try to declare an identifier with this name. It
        // is a name reference. Produce error if name specified here is 'option'
        // or 'type_option'.
        VeriIdDef *var_id = expr->GetId() ;
        const char *name = var_id ? var_id->GetName() : 0 ;
        if (var_id) { // If the label does not exist but a single variable is associated
            //VeriIdDef *temp_label = new VeriBlockId(Strings::save(name)) ;
            //VeriScope *parent_scope = (_scope) ? _scope->Upper() : 0 ;
            // Try to declare in the scope :
            //if (parent_scope && !parent_scope->DeclareBlockId(temp_label)) {
            if (Strings::compare(name, "option") || Strings::compare(name, "type_option")) {
                expr->Error("an object or type with name %s already exists in Covergroup scope", name) ;
                //delete temp_label;
            }
        }
        _point_list = new Array(1) ;
        _point_list->InsertLast(expr) ;
    }
}

VeriCoverageSpec::VeriCoverageSpec(unsigned type, VeriIdDef *id, Array *coverpoints, VeriExpression *iff_expr, Array *bins, VeriScope *scope) :
    VeriModuleItem(),
    _type(type),
    _id(id),
    _point_list(coverpoints),
    _iff_expr(iff_expr),
    _bins(bins),
    _scope(scope)
{
    // Set back-pointer from id to this coverage spec :
    if (_id) _id->SetModuleItem(this) ;
}
VeriCoverageSpec::~VeriCoverageSpec()
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_point_list, i, expr) delete expr ;
    delete _point_list ;

    delete _iff_expr ;

    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_bins, i, item) delete item ;
    delete _bins ;

    delete _scope ;
    if (_id) _id->SetModuleItem(0) ;
    _id = 0 ; // Don't delete _id, it is owned by scope
}
VeriBinDecl::VeriBinDecl(unsigned wildcard, unsigned bins_keyword, VeriIdDef *bin_id) :
    VeriModuleItem(),
    _wildcard(wildcard),
    _bins_keyword(bins_keyword),
    _id(bin_id)
{}
VeriBinDecl::~VeriBinDecl()
{
    if (_id) _id->ClearAllBckPtrs() ; // VIPER #3369
    _id = 0 ; // Don't delete _id, it is owned by scope
}
VeriImportDecl::VeriImportDecl(Array *import_items) :
    VeriModuleItem(),
    _items(import_items)
{}
VeriImportDecl::~VeriImportDecl()
{
    unsigned i ;
    VeriScopeName *import_item ;
    FOREACH_ARRAY_ITEM(_items, i, import_item) delete import_item ;
    delete _items ;
}
VeriLetDecl::VeriLetDecl(VeriIdDef *let_id, Array *let_port_list, VeriExpression *expr, VeriScope *scope)
    : VeriModuleItem(),
    _id(let_id),
    _ansi_io_list(let_port_list),
    _expr(expr),
    _scope(scope),
    _ports(0)
{
    // Set the module item pointer on the 'id' back to this :
    if (_id) _id->SetModuleItem(this) ;

    unsigned i ;

    // Set-up port list : done by order
    _ports = new Array() ;

    // Accumulate ports from the ansi port list
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->AccumulatePorts(*_ports) ;
    }

    // Verify sequence ports. Do same way (but simplified) as we do for functions.
    VeriIdDef *portid ;
    // Default direction setting : SV 1800 LRM 11.13: LRM says direction is not specified
    // for its formal arguments. So we set 'input'

    // NOTE : we can only set the default 'direction' here, not the default 'type'.
    // Default type is only known at 'Resolve' time (due to forward type decls).
    unsigned previous_dir = VERI_INPUT ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_ports, i, portid) {
        if (!portid) continue ;
        // get direction of this port :
        dir = portid->Dir() ;
        // Check if there are any direction set.
        switch (dir) {
        case VERI_INPUT:
        case VERI_OUTPUT:
        case VERI_INOUT:
            portid->Error("direction is not allowed in formal argument of let") ; break ;
        default:
            break ;
        }
        // Inherit previous port direction :
        if (!dir) {
            dir = previous_dir ;
            portid->SetDir(dir) ;
        }
        // update previous port direction
        previous_dir = dir ;
    }
}
VeriLetDecl::~VeriLetDecl()
{
    // Delete the owned parse tree sub-nodes :
    unsigned i ;
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) delete port_decl ;
    delete _ansi_io_list ;

    delete _expr ;  // Delete the expression

    delete _scope ; // Delete the identifiers in the scope and scope itself

    // Delete the created list of ports
    delete _ports ;

    // don't delete _id itself : its owned by the scope above.
    if (_id) _id->ClearAllBckPtrs() ;
    _id = 0 ;
}
VeriDefaultDisableIff::VeriDefaultDisableIff(VeriExpression *expr_or_dist) :
    VeriModuleItem(),
    _expr_or_dist(expr_or_dist)
{}
VeriDefaultDisableIff::~VeriDefaultDisableIff()
{
    delete _expr_or_dist ;
}
VeriExportDecl::VeriExportDecl(Array *import_items) :
    VeriModuleItem(),
    _items(import_items)
{}
VeriExportDecl::~VeriExportDecl()
{
    unsigned i ;
    VeriScopeName *export_item ;
    FOREACH_ARRAY_ITEM(_items, i, export_item) delete export_item ;
    delete _items ;
}
VeriPulseControl::VeriPulseControl(unsigned pulse_control_style, Array *path_list) :
    VeriModuleItem(),
    _pulse_control_style(pulse_control_style),
    _path_list(path_list)
{}
VeriPulseControl::~VeriPulseControl()
{
    unsigned i ;
    VeriExpression *expr ;
    // Delete the output paths i.e. the array elements
    FOREACH_ARRAY_ITEM(_path_list, i, expr) delete expr ;
    // Delete the array
    delete _path_list ;
}

//////////////////////////////////////////////////////////////////////
// (virtual) tests, mainly for backward compatibility after all data decl classes were merged into VeriDataDecl..
//////////////////////////////////////////////////////////////////////

unsigned VeriDataDecl::IsParamDecl() const          { return (_decl_type==VERI_PARAMETER) ? 1 : 0 ; }  // Item was previously a VeriParamDecl.. now a VeriDataDecl.
unsigned VeriDataDecl::IsLocalParamDecl() const     { return (_decl_type==VERI_LOCALPARAM) ? 1 : 0 ; } // Item was previously a VeriLocalParamDecl.. now a VeriDataDecl.
unsigned VeriDataDecl::IsSpecParamDecl() const      { return (_decl_type==VERI_SPECPARAM) ? 1 : 0 ; } // Item was previously a VeriSpecParamDecl.. now a VeriDataDecl.
unsigned VeriDataDecl::IsRegDecl() const            { return (_decl_type==VERI_REG) ? 1 : 0 ; }  // Item was previously a VeriRegDecl.. now a VeriDataDecl.
unsigned VeriDataDecl::IsGenVarDecl() const         { return (_decl_type==VERI_GENVAR) ? 1 : 0 ; } // Item was previously a VeriGenVarDecl.. now a VeriDataDecl.
unsigned VeriDataDecl::IsIODecl() const {
    // IODecl if this data declaration has a 'direction' :
    return (GetDir()) ? 1 : 0 ;
}
unsigned VeriDataDecl::IsEventDecl() const {
    // EventDecl is (in Verilog) a RegDecl, with data type 'VERI_EVENT'
    if (!IsRegDecl()) return 0 ;
    if (_data_type && _data_type->GetType()==VERI_EVENT) return 1 ;
    return 0 ;
}

unsigned VeriDataDecl::IsInterconnectType() const
{
    return (_data_type) ? _data_type->IsInterconnectType() : 0 ;
}

//////////////////////////////////////////////////////////////////////
// Accessor methods, mainly for backward compatibility after all data decl classes were merged into VeriDataDecl..
//////////////////////////////////////////////////////////////////////

unsigned VeriDataDecl::GetSignedType() const        { return (_data_type) ? _data_type->GetSigning() : 0 ; }
VeriRange *VeriDataDecl::GetRange() const           { return (_data_type) ? _data_type->GetDimensions() : 0 ; }
unsigned VeriDataDecl::GetParamType() const         { return (_data_type) ? _data_type->GetType() : 0 ; }
unsigned VeriDataDecl::GetRegType() const           { return (_data_type) ? _data_type->GetType() : 0 ; }
unsigned VeriDataDecl::GetIOType() const            { return (_data_type) ? _data_type->GetType() : 0 ; }
unsigned VeriDataDecl::GetNetType() const           { return (_data_type) ? _data_type->GetType() : 0 ; }

// VeriFunctionDecl now stores net_type, signing and range in the data_type field :
unsigned VeriFunctionDecl::GetType() const          { return (_data_type) ? _data_type->GetType() : 0 ; }
unsigned VeriFunctionDecl::GetSignedType() const    { return (_data_type) ? _data_type->GetSigning() : 0 ; }
VeriRange *VeriFunctionDecl::GetRange() const       { return (_data_type) ? _data_type->GetDimensions() : 0 ; }

//////////////////////////////////////////////////////////////////////
// Set items
//////////////////////////////////////////////////////////////////////
// VIPER #6550: Replace the old generate conditional if and else item with the respective newer one:
void
VeriGenerateConditional::SetThenItem(VeriModuleItem *new_item)
{
    // The application api should catch the old item and delete it
    // otherwise there will be memory leak:
    _then_item = new_item ;
}
void
VeriGenerateConditional::SetElseItem(VeriModuleItem *new_item)
{
    // The application api should catch the old item and delete it
    // otherwise there will be memory leak:
    _else_item = new_item ;
}

//////////////////////////////////////////////////////////////////////
// Resolve
//////////////////////////////////////////////////////////////////////

void VeriDataDecl::Resolve(VeriScope *scope, veri_environment environment)
{
    (void) environment ; // To avoid unused variable warning
    // Check in which context we are declaring :
    VeriIdDef *owner = (scope) ? scope->GetOwner() : 0 ;

    // Check for automatic variable declaration as module item
    if (GetQualifier(VERI_AUTOMATIC) && owner && owner->IsModule()) {
        // Automatic variable declaration directly within module, issue error
        Error("automatic variable not allowed in module") ;
    }

    // VIPER #7603: Parameter declaration within generates are only supported in SV mode:
    if ((_decl_type == VERI_PARAMETER) && scope && scope->IsGenerateScope()) {
        if (!IsSystemVeri()) {
            // Parameter declaration within generate is only allowed in SV mode:
            Warning("parameter declaration is not allowed here in this mode of verilog") ;
        } else if (veri_file::GetAnalysisMode() > veri_file::SYSTEM_VERILOG_2009) {
            // LRM 1800-2012 section 27.2 : Parameters declared inside generate
            // block shall be treated as localparams
            Warning("parameter declared inside generate block shall be treated as localparam") ;
            // Change the identifiers' ParamType :
            unsigned j ;
            VeriIdDef *param_id ;
            FOREACH_ARRAY_ITEM(&_ids, j, param_id) {
                if (param_id) param_id->SetParamType(VERI_LOCALPARAM) ;
            }
        }
    }

    // Resolve the data type
    if (_data_type) {
        _data_type->Resolve(scope, VERI_TYPE_ENV) ;
        // Following may not be correct for 'typedef interface_name new_type_name ;'.  So, for now,
        // rely on the elaborator to error out, if required, we can produce specific error later:
        // VIPER #4847: Error out if the referenced type is not resolved in typedef:
        //if ((_decl_type==VERI_TYPEDEF) && _data_type->IsTypeRef() && !_data_type->IsVirtualInterface() && !_data_type->GetId()) {
        //    VeriName *type_name = _data_type->GetTypeName() ;
        //    if (!type_name->IsHierName()) _data_type->Error("%s is an unknown type", (type_name)?type_name->GetName():"<typedef datatype>") ;
        //}
    }

    // VIPER #8195: Check for data type netdata declaration:
    // LRM IEEE-1800-2012: Sec-6.7 : Certain restrictions apply to the data type of a net declaration.
    // i) a 4 state intergral type, including a packed array or packed struct
    // ii) an unpacked array or unpacked struct, where each element has a valid data type for a net
    // But it does not state this for port declaration. But for compatibility with ModelSim we error out here:
    // for non-ansi port declarations.
    if (_data_type && _data_type->IsNetDataType()) {
        // If any lexical element exists between nettype and the data type
        // for net, 'reg' is allowed as data type.
        unsigned allow_reg = 0 ;
        if (GetQualifier(VERI_SCALARED) || GetQualifier(VERI_VECTORED)) allow_reg = 1 ;

        unsigned is_analog_net_decl = 0 ;

        VeriTypeInfo *type_info = _data_type->CreateType(0) ;
        if (type_info && !is_analog_net_decl) type_info->CheckNetDataType(_data_type, allow_reg) ;
        delete type_info ;
    }

    // VIPER #6177: Check for real type ports in Verilog95/2000 mode:
    if ((IsVeri95() || IsVeri2001()) && GetDir() && _data_type && (_data_type->Type()==VERI_REAL)) {
        VeriIdDef *scope_owner = (scope) ? scope->GetOwner() : 0 ;
        // VIPER #6205: Error out only for module ports, task/function ports can be of type real:
        if (scope_owner && scope_owner->IsModule()) Error("real type port is not allowed in this mode of verilog") ;
    }

    // VIPER #5204: Error out on hierarchical (not scoped name) data type through
    // instance used as data type in (non-typedef) data declaration statements:
    if ((_decl_type!=VERI_TYPEDEF) && _data_type && _data_type->IsTypeRef()) {
        VeriName *type_name = _data_type->GetTypeName() ;
        if (type_name && type_name->IsHierName() && !type_name->IsScopeName() && type_name->GetPrefix()) {
            VeriIdDef *prefix_id = type_name->GetPrefix()->GetId() ;
            if (prefix_id && prefix_id->IsInst()) {
                // True hier name used as data type: this is only allowed in 'typedef'.
                // This is not allowed in BNF syntax itself, so produce syntax error here:
                _data_type->Error("reference to type %s through instance %s is not allowed here",
                                  type_name->GetSuffix(), prefix_id->Name()) ;
            }
        }
    }

    // VIPER #4838: Calculate base type of rand variables and produce error
    // if base type is non-integral type
    VeriTypeInfo *ele_type = ((GetQualifier(VERI_RAND) || GetQualifier(VERI_RANDC)) && _data_type) ? _data_type->CreateType(0): 0 ;
    VeriTypeInfo *base_type = (ele_type) ? ele_type->BaseType() : 0 ;

    // VIPER #3236 : Check whether we are processing ports of automatic task/function
    // If so then marked these ports as automatic.
    VeriIdDef *subprogram = (scope) ? scope->GetSubprogram(): 0 ;
    unsigned is_automatic = (subprogram) ? subprogram->IsAutomatic() : 0 ;

    // VIPER #4030 : Variables declared inside a class are automatic if not
    // explicitly specified as static
    if (!is_automatic && owner && owner->IsClass() && !GetQualifier(VERI_STATIC)) {
        is_automatic = 1 ;
    }

    // VIPER #3786 : Mark data type virtual for virtual interface objects
    if (GetQualifier(VERI_VIRTUAL) && _data_type) {
        _data_type->SetIsVirtualInterface() ; // virtual interface object
        // VIPER #4640 : Virtual interface type objects cannot be automatic
        is_automatic = 0 ;
    } else if (_data_type && _data_type->IsVirtualInterface()) {
        is_automatic = 0 ;
    }

    unsigned is_class_static = 0 ;
    if (GetQualifier(VERI_STATIC) && owner && owner->IsClass()) {
        is_class_static = 1 ;
    }

    // Determine if this is function/task port
    unsigned is_function_task = owner ? owner->IsFunction() || owner->IsTask() : 0 ;

#ifdef VERILOG_PATHPULSE_PORTS
    // VIPER #6119 : Create a class VeriPathPulseValPorts and add that with pulse control specparam id
    unsigned is_specparam_decl = IsSpecParamDecl() ; // Check whether it is specparam decl
#endif

    // Pick up the data type and direction (if any) of this port decl :
    VeriDataType *data_type = _data_type ;

    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids, i, id) {
        if (!id) continue ;

    // VIPER #5949: Set _kind field of port variables depending upon whether the
    // data_type and the direction are mentioned at the time of declaration:
        unsigned kind = 0 ;
        unsigned type = 0 ;
        unsigned default_type = 0 ;
        default_type = VERI_LOGIC ;
        unsigned direction = GetDir() ;

        if (is_class_static) id->SetStaticClassVariable() ;

        if (id->IsPort() && !id->IsNamedPort()) { // Consider only port variables
            if (is_function_task) {
                // Nothing to do..
            } else if (!data_type && direction) {
                // data_type is not mentioned but and direction is mentioned at the
                // time of declaration ie. it is declared as 'input sig', then
                // set the _kind field by the following method:
                id->SetKindFromDataType() ;
            } else if (data_type && !direction) {
                // data_type is present but no direction is mentioned at the
                // time of declaration ie. it is declared as 'reg sig', then
                // set the _kind field by the type of the data_type:
                // VIPER #6254: Also reset the type field here:
                if (data_type->IsNetDataType()) { // For net data type
                    kind = data_type->NetType() ;
                    VeriDataType *d_type = data_type->GetDataType() ;
                    type = d_type ? d_type->Type() : 0 ;
                } else {
                    if (!data_type->IsNetType()) { // data_type is non-net type ie. 'reg sig'
                        kind = data_type->Type() ;
                        type = data_type->Type() ;
                    } else { // for net-type port ie, 'wire sig'
                        if (IsVeri95() || IsVeri2001()) {
                            type = data_type->Type() ;
                        } else {
                            type = default_type ;
                        }
                        kind = data_type->Type() ;
                    }
                }
            }
            if (kind) id->SetKind(kind) ; // Set the kind field
            if (type) id->SetType(type) ; // Set the type field
        }

        // extract the 'type' of declaration (if any).
        // Call resolve only if data type here matches backpointer in the id.
        // Otherwise, we would resolve id twice for re-declarations (and
        // possibly use a data type before it was resolved itself).

        // Always resolve a parameter, since these cannot be re-declared (and type-parameters change their data type pointer on the fly).
        if ((id->GetDataType() != _data_type) && !id->IsParam()) {
            continue ;
        }

        // Viper 5667: Issue error for initialization of not reg ports in declaration
        // 1364 LRM A.2.1.2 and P1800 lrm A2.6/2.7 and A2.1.2
        // FIXME : We do not error out for multiple declarations in ports
        // Forexample Verific do not error out for output o = 1'b1; wire o ;
        // this is because it cannot distinguish between the above style of
        // declaration and declaration like output o ; wire o =1'b1 or like
        // wire o =1'b1 ; output o as we only maintain a single definition of
        // id o (the first one) and the initial value is stored on the id.

        VeriDataType *id_data_type = id->GetDataType() ;
        unsigned is_null_type = !id_data_type || !id_data_type->Type() ;

        if ((id->IsNet() || is_null_type) && id->GetInitialValue() && id->IsPort()) {
            if (IsSystemVeri() && GetDir()) {
                if (!is_function_task) id->Error("invalid initialization in declaration") ;
            } else {
                if (GetDir() == VERI_OUTPUT) id->Error("invalid initialization in declaration") ;
            }
        }

        // VIPER #8339: Issue warning for variables declared with initialization within procedural block/
        // task/function declaration without specifying then as static/automatic
        unsigned is_automatic_static = 0 ;
        if ((id ->GetInitialValue() && _decl_type != VERI_PARAMETER) && !GetQualifier(VERI_AUTOMATIC)
            && !GetQualifier(VERI_STATIC)
                ) {
            VeriIdDef *sub_program = (scope) ? scope->GetSubprogram(): 0 ;
            if (sub_program && (sub_program->IsTask() || sub_program->IsFunction())) {
                VeriModuleItem *item = sub_program->GetModuleItem() ;
                if (item && ((!item->GetQualifier(VERI_AUTOMATIC) && item->GetAutomaticType() != VERI_AUTOMATIC)
                    && (!item->GetQualifier(VERI_STATIC) && item->GetAutomaticType() != VERI_STATIC)
                )) {
                    is_automatic_static = 1 ;
                if (scope && scope->GetClass()) is_automatic_static = 0 ;
                }
            }

            // Issue error for blocking items:
            if (scope && scope->IsSeqBlockScope()) {
                // Do not error for task/functions here: it will be checked in the above:
                if (!sub_program || (!sub_program->IsTask() && !sub_program->IsFunction())) is_automatic_static = 1 ;
            }

            // do not warn for loop variables and generate constructs
            if (scope && (scope->IsLoopScope() || scope->IsGenerateScope())) is_automatic_static = 0 ;

            // do not warn for net/port variables
            if (id->IsNet() || id->IsPort()) is_automatic_static = 0 ;

            // Flag the warning:
            if (is_automatic_static) {
                id->Warning("variable %s must explicitly be declared as automatic or static", id->Name()) ;
            }
        }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        // Viper 6895 : Check for correct dimensions in instantiations
        // It may be noted that vhdl types of the form
        // "subtype bit_arr is std_ulogic_vector" can be mapped to
        // "bit_arr b[1:0]" or "bit_arr b" or "bit_arr [1:0] b"

        VeriIdDef *type_id = id_data_type ? id_data_type->GetId() : 0 ;
        if (type_id && type_id->IsConvertedVhdlType() && id_data_type) {
            unsigned datatype_dim_cnt = id_data_type->GetDimensions() ? id_data_type->PackedDimension() : 0 ;
            unsigned id_dim_cnt = id->GetDimensions() ? id->UnPackedDimension() : 0 ;
            unsigned num_unconstraint = type_id ? type_id->GetUnpackedUnconstraintDimCount() + type_id->GetPackedUnconstraintDimCount() : 0 ;
            // Check for total number of dimensions is at-least not greater than the number of unconstrained dimensions
            // types with unconstrained dimensions are still allowed. Example above.
            if ((id_dim_cnt + datatype_dim_cnt) > num_unconstraint) {
                id->Error("incorrect number of dimensions in instantiation") ;
            }
            // If the vhdl type has unpacked unconstrained dimension then there should be at-least that many dimensions in instantiation
            if (id_dim_cnt < type_id->GetUnpackedUnconstraintDimCount()) {
                id->Error("number of unpacked dimensions %d does not match the number of dimensions %d of VHDL unconstrained array",id->Dimension(),  type_id->GetUnpackedUnconstraintDimCount()) ;
                if (id_data_type->PackedDimension()) id->Error("illegal to define packed dimensions for vhdl unconstrained arrays") ;
            }
        }
#endif

        // Resolve() on data_type could have created circular type loops (for SV forward type declarations)
        // Check for circular type declarations :
        // RD: Only do this when a 'circular loop' can be closed. That is if this is a type-def.
        if (IsTypeAlias()) {
            Set type_ids(POINTER_HASH) ;
            (void) type_ids.Insert(id) ;
            VeriDataType *dt = id->GetDataType() ;
            if (dt && dt->IsCircular(type_ids)) {
                id->Error("illegal circular dependency found through %s", id->Name()) ;
                // Break the loop, so that other routines do not get stuck in it.
                id->ClearDataType() ;
            }
        }

        // Set qualifiers (individual ones). Cannot do this in constructor, since qualifiers get set after construction.
        if (GetQualifier(VERI_CONST)) id->SetIsConstant() ;
        // VIPER #4425 : Direction VERI_CONST refers to const ref port
        if (id->Dir() == VERI_CONST) id->SetIsConstant() ;
        if (GetQualifier(VERI_AUTOMATIC)) id->SetIsAutomatic() ;
        // VIPER #4043 : Mark the identifiers looking at qualifier 'local' and 'protected'
        if (GetQualifier(VERI_LOCAL)) id->SetIsPrivate() ;
        if (GetQualifier(VERI_PROTECTED)) id->SetIsProtected() ;
        // VIPER #3547: Set the type to be logic (default type for 'var') when no type has been inferred for 'var'iables:
        // VIPER #5949: set the kind to be var (default kind for 'var') when no kind has been inferred for 'var'iables:
        if (!id->Type() && GetQualifier(VERI_VAR)) {
            id->SetType(VERI_LOGIC) ;
            id->SetKind(VERI_VAR) ;
        }
        // Viper 5778: env equal to VERI_STATIC_ENV implies task/function is static
        if (GetQualifier(VERI_STATIC) || environment == VERI_STATIC_ENV) is_automatic = 0 ; // Explicitly declared as static
        // Check for randc variables, it cannot have data types other than bit/enum
        // VIPER #4744 : Allow unpacked array as randc variable :
        //if (GetQualifier(VERI_RANDC) && !id->IsMember() && (!_data_type || ((_data_type->Type() != VERI_BIT) && !_data_type->GetEnums()))) {
        // VIPER #5655 : Use VeriTypeInfo to check type of randc variables
        //if (GetQualifier(VERI_RANDC) && !id->IsMember() && (!_data_type || (base_type && !base_type->IsTypeParameterType() && !base_type->IsUnresolvedNameType() && !(base_type->Type() == VERI_BIT) && !base_type->IsEnumeratedType()))) {
        // P1800-2009 removes the restriction of randc variables data type. It will be of
        // integral type (test_70)
        if (GetQualifier(VERI_RANDC) && base_type && !base_type->IsTypeParameterType() && !base_type->IsUnresolvedNameType() && !base_type->IsIntegralType()) {
            id->Error("invalid data type for random variable %s", id->Name()) ;
        }
        //  VIPER #4838 : Produce error if random variables are not integral type
        if (GetQualifier(VERI_RAND) && base_type && !base_type->IsTypeParameterType() && !base_type->IsUnresolvedNameType() && (base_type->IsRealType() || base_type->IsEventType())) {
            id->Error("invalid data type for random variable %s", id->Name()) ;
        }
        // Mark local variables of sequence and property :
        if (owner && (owner->IsSequence() || owner->IsProperty())) id->SetIsLocalVar() ;

        if (owner && owner->IsClass()) {
            // VIPER #4039 : If constant is declared inside class without specifying
            // initial value, that is instance constant and instance constant
            // cannot be declared as static
            if (!id->GetInitialValue() && GetQualifier(VERI_CONST) && GetQualifier(VERI_STATIC)) {
                Error("instance constant %s cannot be declared static", id->Name()) ;
            }
        }
        if (is_automatic && !id->IsMember()) id->SetIsAutomatic() ; // Mark as automatic: Not marked struct/union members
#ifdef VERILOG_PATHPULSE_PORTS
        if (is_specparam_decl) {
            // VIPER #6119 : Create and add VeriPathPulseValPorts with specparam for pulse control specparam
            id->CreatePathPulseValPorts(scope) ;
        }
#endif

        // Resolve this identifier
        id->Resolve(scope, VERI_UNDEF_ENV) ;

        // VIPER #4943: Variable declaration with initial values are only legal at module level (for non-SV mode):
        if (!IsSystemVeri() && id->IsReg() && id->GetInitialValue() && owner && !owner->IsModule()) {
            // Produce a warning in V95/V2K mode (BNF does not allow initial value!):
            VeriModuleItem *owner_mod_item = owner->GetModuleItem() ;
            if (!owner_mod_item || (!owner_mod_item->IsGenerateBlock() && !owner_mod_item->IsGenerateFor())) {
                // Produce the warning message only if it is not inside immediate generate blocks,
                // since generate block items will be moved into module scope at elaboration phase:
                Warning("non-module variable %s cannot be initialized at declaration", id->Name()) ;
            }
        }

        if (owner && owner->IsChecker() && id->IsInterfacePort()) id->Error("checker formal argument '%s' may not be of interface type", id->Name()) ; // // LRM 2009, 17.2

        // VIPER #3499: Check for $ in dimension. $ cannot be in the RHS of a range:
        // Also, $ cannot involve binary operator in it (operator + and -).
        // dimension should not have +/- here and can have in indexing with $.
        unsigned num_dimensions = id->PackedDimension() + id->UnPackedDimension() ;
        unsigned j ;
        VeriRange *dim ;
        for (j=0; j<num_dimensions; j++) {
            dim = id->GetDimensionAt(j) ;
            VeriExpression *left = (dim) ? dim->GetLeft() : 0 ;
            // $ is allowed as the only value as MSB (LHS) side of the range only:
            if (left && left->IsDollar()) continue ; // [$:$] is not in grammar itself, no need to check
            // $ other than the above places is not allowed, other tools produce syntax error:
            if (dim && dim->HasDollar()) dim->Error("syntax error near %s", "$") ;
        }

        // Viper #5274: multiple packed dimensions only allowed in SV mode
        if (!IsSystemVeri() && id->PackedDimension() > 1) Warning("multiple packed dimensions are not allowed in this mode of verilog") ;
    }

    delete ele_type ;

    if (_resolution_function) {
        _resolution_function->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ;

        // perform sanity checks with resolution function

        // Check (1): return type must exactly match
        VeriDataType *func_type = _resolution_function->GetDataType() ;
        VeriTypeInfo *func_type_info = func_type ? func_type->CreateType(0) : 0 ;
        VeriTypeInfo *data_type_info = _data_type ? _data_type->CreateType(0) : 0 ;
        if (!data_type_info || !data_type_info->IsEqual(func_type_info, 0)) {
            VeriName *type_name = _data_type ? _data_type->GetTypeName() : 0 ;
            const char *type_name_str = type_name ? type_name->GetName() : "" ;
            _resolution_function->Error("%s is not a valid resolution function for type %s : %s", _resolution_function->GetName(), type_name_str, "return type mismatch") ;
        }

        // Check (2): resolution function must be automatic
        VeriIdDef *func_id = _resolution_function->GetId() ;
        if (func_id && !func_id->IsAutomatic()) _resolution_function->Error("resolution function %s must be automatic", _resolution_function->GetName()) ;

        // Check (3): Must have a single argument
        Array *func_args = func_id ? func_id->GetPorts() : 0 ;
        VeriIdDef *arg_id = (func_args && func_args->Size()) ? (VeriIdDef *)func_args->At(0) : 0 ;
        if (!func_args || func_args->Size() != 1 || !arg_id) _resolution_function->Error("resolution function %s must have single input argument", _resolution_function->GetName()) ;

        // Check (4): Argument must be dynamic array
        VeriRange *unpacked_dim = arg_id ? arg_id->GetDimensions() : 0 ;
        if (arg_id && (!unpacked_dim || !unpacked_dim->IsDynamicRange())) _resolution_function->Error("argument %s of resolution function %s must be dynamic array", arg_id ? arg_id->Name() : "", _resolution_function->GetName()) ;

        // Check (5): Argument must be input type
        unsigned is_input_arg = arg_id ? arg_id->IsInput() : 0 ;
        if (arg_id && !is_input_arg) _resolution_function->Error("argument %s of resolution function %s must be an input", arg_id ? arg_id->Name() : "", _resolution_function->GetName()) ;

        // Check (6): Argument element type is same as data type
        VeriTypeInfo *arg_type_info = arg_id ? arg_id->CreateType(0) : 0 ;
        VeriTypeInfo *arg_element_type_info = arg_type_info ? arg_type_info->ElementType() : 0 ;
        if (arg_id && (!arg_element_type_info || !arg_element_type_info->IsEqual(data_type_info, 0))) {
            VeriName *type_name = _data_type ? _data_type->GetTypeName() : 0 ;
            const char *type_name_str = type_name ? type_name->GetName() : "" ;
            _resolution_function->Error("%s is not a valid resolution function for type %s : %s", _resolution_function->GetName(), type_name_str, "argument type mismatch") ;
        }

        delete arg_type_info ;
        delete func_type_info ;
        delete data_type_info ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriNetDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve using the base type resolver :
    VeriDataDecl::Resolve(scope, VERI_UNDEF_ENV) ;

    // Resolve strength and delay
    if (_strength) {
        _strength->Resolve(scope, VERI_EXPRESSION) ;
        // VIPER #5648 : Produce error if strength is specified in net declaration,
        // but identifier does not have any initial value
        // '_strength' here can be charge strength also. Ignore those
        unsigned token = _strength->GetLVal() ;
        if ((token != VERI_SMALL) && (token != VERI_MEDIUM) && (token != VERI_LARGE)) {
            // VIPER #5677 : For net declaration both the strengths are required to specify.
            // Produce error if both strengths are not specified.
            if (!_strength->GetRVal()) {
                _strength->Error("both strengths required for %s", "net declaration") ;
            }
            unsigned i ;
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(&_ids, i, id) {
                if (id && !id->GetInitialValue()) {
                    id->Error("strength without assignment is not allowed in net declaration") ;
                }
            }
        } else /* if (VERI_SMALL or VERI_MEDIUM or VERI_LARGE) */ {
            if (GetNetType() != VERI_TRIREG) {
                // VIPER #6600: This is a charge sepcification on a net which is not a trireg - error:
                _strength->Error("only trireg nets can have a charge strength specification") ;
            }
        }
    }

    // LRM 6.5 : Certain restrictions apply to the data type of a net
    // i) a 4 state intergral type, including a packed array or packed struct
    // ii) an unpacked array or unpacked struct, where each element has a valid
    //     data type for a net
    VeriDataType *data_type = (_data_type && _data_type->IsNetDataType()) ? _data_type->GetDataType() : 0 ;
    VeriTypeInfo *type = (data_type) ? data_type->CreateType(0) : 0 ;

    // VIPER #4732: If any lexical element exists between nettype and the data type
    // for net, 'reg' is allowed as data type.
    unsigned allow_reg = 0 ;
    if (_strength || GetQualifier(VERI_SCALARED) || GetQualifier(VERI_VECTORED)) allow_reg = 1 ;

    unsigned is_analog_net_decl = 0 ;

    if (type && !is_analog_net_decl) type->CheckNetDataType(_data_type, allow_reg) ;

    // VIPER #4828: Produce warning if 'scalared'/'vectored' keyword is used, but data type
    // does not have any packed dimension:
    if ((GetQualifier(VERI_SCALARED) || GetQualifier(VERI_VECTORED)) && type && !type->IsTypeParameterType() && !type->IsUnresolvedNameType()
            && !type->PackedDimension() && (type->BaseTypeSize() == 1)) {
        if (data_type) data_type->Warning("scalared or vectored keyword can be used with vectored data types") ;
    }
    delete type ;

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_delay, i, expr) {
        if (expr) expr->Resolve(scope, VERI_DELAY_CONTROL) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

// Visitor class to visit function and check all the id-refs inside the function.
// It takes a Set of already declared ids. It checked the resolved id of the refs
// it visits. If the resolved id is in the Set it treats it as locally declared.
// Otherwise, it treats it as global. If the global ids are not constant or are not
// parameters it sets a flag to indicate that the function is not constant.
class FuncVisitor : public VeriVisitor
{
public:
    FuncVisitor(unsigned is_static, const Set *declared_ids) : VeriVisitor(), _declared_ids(declared_ids), _using_global_non_const(0), _is_static(is_static), _using_sys_call(0), _inside_inline_constraint(0) { }
    virtual ~FuncVisitor() { _declared_ids = 0 ; } // We don't own it, just clear the pointer}

private:
    // Prevent compiler from defining the following
    FuncVisitor() ;                               // Purposely leave unimplemented
    FuncVisitor(const FuncVisitor &) ;            // Purposely leave unimplemented
    FuncVisitor& operator=(const FuncVisitor &) ; // Purposely leave unimplemented

public:
    virtual void VERI_VISIT(VeriIdRef, ref) ; // We are only interested in the references
    // VIPER #4450 : Overwrite Visit of keyword to catch 'this'
    virtual void VERI_VISIT(VeriKeyword, node) ;
    virtual void VERI_VISIT(VeriInlineConstraintStmt, node) ;
    virtual void VERI_VISIT(VeriInlineConstraint, node) ;
    virtual void VERI_VISIT(VeriSystemFunctionCall, call) ; // VIPER #4380 : To determine which system calls are not allowed in constant function
    virtual void VERI_VISIT(VeriSystemTaskEnable, r) ; // VIPER #4380 : Do not check system task enable, ignore those, VIPER #5574: Ignore but do trap it

    // Returns the flag whether the function is using non-constant global or not:
    unsigned     UsingGlobalNonConstant() const  { return _using_global_non_const ; }
    unsigned     UsingSystemFuncTask() const     { return _using_sys_call ; }

private:
    const Set *_declared_ids ;             // Pointer hashed Set of VeriIdDef * of the defined ids. Does not own the Set.
    unsigned   _using_global_non_const:1 ; // Flag to indicate whether it found any non-constant gloab reference while traversing.
    unsigned   _is_static:1 ;
    unsigned   _using_sys_call:1 ; // VIPER #4380 : Constant function cannot have system func/task call
    unsigned   _inside_inline_constraint:1 ; // VIPER #5248 (test19): Flag to indicate that we are inside inline constraint
} ; // class FuncVisitor

/* -------------------------------------------------------------- */

void FuncVisitor::VERI_VISIT(VeriIdRef, ref)
{
    // Get the resolved identifier from this reference:
    VeriIdDef *resolved_id = ref.GetId() ;

    if (!resolved_id) {
        // This Id-ref is not resolved, treat it as global.
        _using_global_non_const = 1 ;
        return ;
    }
    // VIPER #4030 : Produce error if non-static objects are referenced inside
    // static function
    // VIPER #4403 : Do not produce error for accessing objects other than variables
    // or function/task. Type identifiers can be accessed here.
    // VIPER 5042 automatic variables declared inside the static function should
    // not be considered as global automatic.
    unsigned locally_declared = _declared_ids && _declared_ids->Get(resolved_id) ? 1 : 0 ;
    // VIPER #5248 (test19): Do not produce the error if we are inside inline constraint.
    // That seem to be allowed, for example, C.randomize() with { a == 0 ; } ;
    if (_is_static && !_inside_inline_constraint && (resolved_id->IsVar() || resolved_id->IsTask()) &&
        !locally_declared && resolved_id->IsAutomatic()) {
        ref.Error("illegal reference to non-static %s from static function", resolved_id->Name()) ;
    }
    if (!_declared_ids) return ; // Can't continue without this Set

    // Check whether this id is in the Set. If it is not, it is a global reference.
    // Check in that case, whether the global reference is constant or not:
    if (!_declared_ids->GetItem(resolved_id) &&
        // Skip types and functions, they can be declared outside:
        !resolved_id->IsType() && !resolved_id->IsFunction() &&
        !resolved_id->IsParam() && !resolved_id->IsConstant()) {
        // This global ref is neither a parameter nor a (SV) constant:
        _using_global_non_const = 1 ;
    }
}
void FuncVisitor::VERI_VISIT(VeriKeyword, node)
{
    if (_is_static && ((node.GetToken() == VERI_THIS) || (node.GetToken() == VERI_SUPER))) {
        node.Error("illegal reference to %s from static function/task", (node.GetToken() == VERI_THIS) ? "this" : "super") ;
    }
}
void FuncVisitor::VERI_VISIT(VeriInlineConstraintStmt, node)
{
    unsigned prev = _inside_inline_constraint ;
    _inside_inline_constraint = 1 ;
    VeriVisitor::VERI_VISIT_NODE(VeriInlineConstraintStmt, node) ;
    _inside_inline_constraint = prev ;
}
void FuncVisitor::VERI_VISIT(VeriInlineConstraint, node)
{
    unsigned prev = _inside_inline_constraint ;
    _inside_inline_constraint = 1 ;
    VeriVisitor::VERI_VISIT_NODE(VeriInlineConstraint, node) ;
    _inside_inline_constraint = prev ;
}
void FuncVisitor::VERI_VISIT(VeriSystemFunctionCall, call)
{
    switch(call.GetFunctionType()) {
    case VERI_SYS_CALL_BITS:
    case VERI_SYS_CALL_DIMENSIONS:
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS:
    case VERI_SYS_CALL_LEFT:
    case VERI_SYS_CALL_RIGHT:
    case VERI_SYS_CALL_LOW:
    case VERI_SYS_CALL_HIGH:
    case VERI_SYS_CALL_INCREMENT:
    case VERI_SYS_CALL_LENGTH:
    case VERI_SYS_CALL_SIZE:
    case VERI_SYS_CALL_CAST:
    case VERI_SYS_CALL_ISUNKNOWN:
    // VIPER #5248 (test_24): $rtoi and $itor are constant system functions:
    case VERI_SYS_CALL_RTOI:
    case VERI_SYS_CALL_ITOR:
    case VERI_SYS_CALL_ISUNBOUNDED : // VIPER #5652
    case VERI_SYS_CALL_SIGNED:
    case VERI_SYS_CALL_UNSIGNED:
        break ;
    default: // Not supported in constant function
        _using_sys_call = 1 ;
        break ;
    }
}

// All system tasks are to be ignored inside constant function as per LRM 1364-2001 section 10.3.5
void FuncVisitor::VERI_VISIT(VeriSystemTaskEnable, call)
{
    // VIPER #5574: Trap this visit routine but do not do anything.
    // We need to trap it because, if we don't it will go to the base class visitor
    // and will traverse the arguments. Arguments can have system function call that
    // may not be allowed on its own inside a constant function. Since this system
    // task invocation is ignored altogether, we need to ignore such function calls too.
    // Viper 5574 need to stack _using_global_non_const _using_sys_call before
    // calling base function as they might be overwritten
    unsigned using_global_non_const = _using_global_non_const ;
    unsigned using_sys_call = _using_sys_call ;
    VeriVisitor::VERI_VISIT_NODE(VeriSystemTaskEnable, call) ;
    _using_global_non_const = using_global_non_const ;
    _using_sys_call = using_sys_call ;
/*
     switch(call.GetFunctionType()) {
     case VERI_SYS_CALL_CAST:
     case VERI_SYS_CALL_DISPLAY:
     case VERI_SYS_CALL_DISPLAYB:
     case VERI_SYS_CALL_DISPLAYH:
     case VERI_SYS_CALL_DISPLAYO:
         break ;
     default : // Not supported in constant function
         _using_sys_call = 1 ;
         break ;
     }
*/
}

/* -------------------------------------------------------------- */

void VeriFunctionDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // If this is definition of exported function, do not resolve its body
    if (_name && _name->IsHierName()) {
        // If prefix is interface port/instance, it is exported function
        VeriName *prefix = _name->GetPrefix() ;
        VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
        if (prefix_id && (prefix_id->IsInterfacePort() || prefix_id->IsInst())) {
            char *tmp_name = Strings::save(prefix_id->Name(),  " ", _name->GetSuffix()) ;
            if (_func && Strings::compare(tmp_name, _func->Name())) {
                Strings::free(tmp_name) ;
                return ; // exported function, do not resolve its body
            }
            Strings::free(tmp_name) ;
        }
    }
    // Resolve the data type
    if (_data_type) _data_type->Resolve(scope, VERI_TYPE_ENV) ;

    // Switch to local scope if it is set :
    if (_scope) scope = _scope ;

    // LRM 12.1.3 - Task/Function declarations are not allowed in a generate loop
    if (scope && scope->Find(" gen_loop_label")) {
        Error("function/task declaration not allowed inside generate loop") ;
    }
    // VIPER #6983 : Produce error if function/task with ref arguments is not
    // declared automatic (1800-2009 LRM section - 13.5.2)
    if (_func && !_func->IsAutomatic()) { // task not automatic
        unsigned i ;
        VeriIdDef *port_id ;
        FOREACH_ARRAY_ITEM(_ports, i, port_id) { // Check for ref argument
            if (port_id && ((port_id->Dir() == VERI_REF) || (port_id->Dir() == VERI_CONST))) {
                Warning("task or function %s with ref arguments must be automatic", _func->Name()) ;
                break ;
            }
        }
    }

    // Set base 'type' field on the function id itself (now that data type is resolved).
    unsigned function_type = (_data_type) ? _data_type->Type() : 0 ;
    // Use default 'reg' if data type does not provide it. Can happen for "function [3:0] foo () ;"
    if (!function_type) function_type = VERI_REG ;
    // VIPER #5949: Set kind to the function id with the function_type:
    //if (_func) _func->SetType(function_type) ;
    if (_func) {
        _func->SetType(function_type) ;
        _func->SetKind(function_type) ;
    }

    unsigned exported_func = (_scope) ? _scope->IsExportedSubprogScope(): 0 ;
    // Reset is_exported flag in scope before checking whether any variable of same
    // name exists inside the function. If this flag is set 'FindLocal' will always
    // return function id  :
    if (exported_func && _scope) _scope->SetExportedSubprogScope(0) ;
    // Check if a variable with same name as this function is declared within this scope
    VeriIdDef *same_name_var = (_scope && _func) ? _scope->FindLocal(_func->Name()) : 0 ;
    unsigned is_void = 0 ;

    // Set is_exported flag again :
    if (exported_func && _scope) _scope->SetExportedSubprogScope(1) ;

    if (function_type == VERI_VOID) is_void = 1 ;
    // VIPER #4899 : For local qualifier before function decl, mark the identifier private
    if (GetQualifier(VERI_LOCAL) && _func) _func->SetIsPrivate() ;
    if (GetQualifier(VERI_PROTECTED) && _func) _func->SetIsProtected() ;
    // VIPER #4833 : Inside class randomize/rand_mode/constraint_mode
    // built-in methods cannot be overwridden
    unsigned func_token = (_name) ? VeriExpression::GetFuncToken(_name->GetName()): 0 ;
    switch(func_token) {
    case VERI_METHOD_CONSTRAINT_MODE :
    case VERI_METHOD_RAND_MODE :
    case VERI_METHOD_RANDOMIZE :
        // Check whether we are inside clas
        if (_name && scope && scope->GetClass()) {
            _name->Error("invalid declaration of built-in method %s", _name->GetName()) ;
        }
        break ;
    default :
        break ;
    }

    // Variable with function name is declared in function scope, error out
    // VIPER #4502 : Produce error only for non-void function. Void function can declare object
    // with same name as function
    if (same_name_var && !is_void) {
        if (InRelaxedCheckingMode()) {
            // VIPER #5248 (test_99): Produce warning in compatibility mode:
            same_name_var->Warning("%s is already declared", same_name_var->Name()) ;
            //same_name_var->Warning("second declaration of %s ignored", same_name_var->Name()) ;
        } else {
            same_name_var->Error("%s is already declared", same_name_var->Name()) ;
        }
        if (_func) _func->Info("previous declaration of %s is from here", _func->Name()) ; // VIPER #7773
        // VIPER #5248 (test_99): Anyway reset the data type of the id, so that we continue
        // with this id itself. We do this always so that downgrading the above error works:
        same_name_var->ClearDataType() ;
        same_name_var->SetDataType(_data_type) ;
    }

    // Set base 'type' field on ansi ports :
    // LRM (SV 1800) 12.2 :
    // Ports inherit direction and type from previous ports.
    // default port direction is 'input'.
    // default type is 'logic'.
    unsigned i ;
    VeriExpression *port_decl ;
    unsigned previous_direction = VERI_INPUT ;
    // VIPER #5949: Set previous_kind and default_kind to VERI_REG:
    unsigned default_kind = VERI_REG ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = VERI_REG ;
    // VIPER #5949: Set default_type to VERI_LOGIC:
    if (IsSystemVeri()) {
        default_type = VERI_LOGIC ;
    }
    unsigned previous_type = default_type ;
    // Now do the ansi port list itself :
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->ResolveAnsiPortDecl(_scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
    }

    // Local decls :
    VeriModuleItem *decl_item ;
    // Viper 5778: Need to mark local decls static/automatic if the task/function is static/automatic
    veri_environment env = VERI_UNDEF_ENV ;
    if (_automatic_type == VERI_STATIC) env = VERI_STATIC_ENV ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (decl_item) decl_item->Resolve(scope, env) ;
    }

    // Statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (_func && _func->GetFunctionType()==VERI_METHOD_NEW) {
            VeriName *task_name = stmt->GetTaskName() ;
            VeriName *prefix = (task_name) ? task_name->GetPrefix() : 0 ;
            const char *suffix = (task_name) ? task_name->GetSuffix() : 0 ;
            if ((prefix && prefix->GetToken() == VERI_SUPER) && (suffix && Strings::compare(suffix,"new")) && i!=0 )  {
                stmt->Error("the call to super.new must be the first statement in the constructor") ;
            }
        }
        if (stmt) stmt->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;

    // VIPER #4728: Propagate virtual qualifier. If this function is defined within
    // a class, test whether that function is defined as virtual in base class. If
    // so set virtual qualifier to this function too.
    // Viper 5130. For virtual functions we need to validate the virtual method with that of super class
    // Thus activated the below code irrespective of GetQualifier(VERI_VIRTUAL)
    // Viper #5435 : Check base classes, as function can be declared virtual in
    // any base class:
    VeriIdDef *enclosing_class = (_scope) ? _scope->GetClass(): 0 ;
    VeriScope *enclosing_class_scope = (enclosing_class) ? enclosing_class->LocalScope(): 0 ;
    VeriScope *base_class_scope = enclosing_class_scope ? enclosing_class_scope->GetBaseClassScope(): 0 ;
    while (base_class_scope) {
        VeriIdDef *base_func_id = base_class_scope->FindLocal(_func ? _func->Name(): 0) ;
        VeriModuleItem *base_func_body = (base_func_id) ? base_func_id->GetModuleItem(): 0 ;
        if (base_func_body && base_func_body->GetQualifier(VERI_VIRTUAL)) {
            // Check data type of virtual function in base class with this
            if (_func) _func->ValidateVirtualMethod(base_func_id) ;
            SetQualifier(VERI_VIRTUAL) ;
            break ;
        }
        base_class_scope = base_class_scope->GetBaseClassScope() ;
    }
    // VIPER #4030 : Produce error if static methods are defined as virtual
    // VIPER #7980: Moved this check into bison grammar.
    //if (GetQualifier(VERI_STATIC) && GetQualifier(VERI_VIRTUAL)) {
    //    Error("static methods cannot be virtual") ;
    //}
    switch(func_token) {
    case VERI_METHOD_PRE_RANDOMIZE :
    case VERI_METHOD_POST_RANDOMIZE :
        // Check whether we are inside clas
        // VIPER #4805 : virtual keyword cannot be used for overwrite built-in method
        // pre_randomize/post_randomize
        if (_name && GetQualifier(VERI_VIRTUAL) && scope && scope->GetClass()) {
            _name->Error("invalid use of virtual keyword for built-in method %s", _name->GetName()) ;
        }
        break ;
    default :
        break ;
    }

    // VIPER #3299: Check whether this function uses any non-constant global:
    // First accumulate all the ids defined in the function scope and sub-scopes:
    unsigned is_static = 0 ;
    if (GetQualifier(VERI_STATIC)) is_static = 1 ;
    Set ids(POINTER_HASH) ;
    if (_scope) _scope->GetDeclaredIds(ids) ;
    // Create an object of a visitor that visits only IdRefs:
    FuncVisitor fv(is_static, &ids) ;
    Accept(fv) ; // Visit this function to check all IdRefs' resolved ids
    // Reset the non-constant function flag if it uses global non-constant:
    if (fv.UsingGlobalNonConstant()) _can_be_const_func = 0 ;
    // Reset the non-constant function flag if it uses system function/task
    // which are not allowed in cosntant function
    if (fv.UsingSystemFuncTask()) _can_be_const_func = 0 ;

    if (_is_const_called && fv.UsingGlobalNonConstant()) {
        // VIPER #3276: Check whether this function is called from a constant environment
        // and whether it can be a potential constant function. If not, produce error:
        Error("global or hierarchical references may not be used in a constant function") ;
    }
    if (_is_const_called && fv.UsingSystemFuncTask()) {
        // VIPER #4380: If this function is called from a constant environment
        // and this function is using system function/task those are not allowed
        //produce error
        Error("system functions/tasks may not be used in a constant function") ;
    }
}

void VeriTaskDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // VCS Compatibility issue (test45) : If task prototype is declared only
    // extern, then also consider this as extern forkjoin. Standard simulators
    // behave like that. But LRM says for multiple export of task, it should be
    // declared as extern forkjoin.
    if (_task && (GetQualifier(VERI_FORKJOIN) || GetQualifier(VERI_EXTERN))) _task->SetIsExternForkjoinTask() ;
    // If this is definition of exported task, do not resolve its body
    if (_name && _name->IsHierName()) {
        // If prefix is interface port/instance, it is exported function
        VeriName *prefix = _name->GetPrefix() ;
        VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
        if (prefix_id && (prefix_id->IsInterfacePort() || prefix_id->IsInst())) {
            char *tmp_name = Strings::save(prefix_id->Name(),  " ", _name->GetSuffix()) ;
            if (_task && Strings::compare(tmp_name, _task->Name())) {
                Strings::free(tmp_name) ;
                return ; // exported task, do not resolve its body
            }
            Strings::free(tmp_name) ;
        }
    }
    // Switch to local scope if it is set :
    if (_scope) scope = _scope ;

    // LRM 12.1.3 - Task/Function declarations are not allowed in a generate loop
    if (scope && scope->Find(" gen_loop_label")) {
        Error("function/task declaration not allowed inside generate loop") ;
    }

    unsigned i ;
    // VIPER #6983 : Produce error if function/task with ref arguments is not
    // declared automatic (1800-2009 LRM section - 13.5.2)
    if (_task && !_task->IsAutomatic()) { // task not automatic
        VeriIdDef *port_id ;
        FOREACH_ARRAY_ITEM(_ports, i, port_id) { // Check for ref argument
            if (port_id && ((port_id->Dir() == VERI_REF) || (port_id->Dir() == VERI_CONST))) {
                Warning("task or function %s with ref arguments must be automatic", _task->Name()) ;
                break ;
            }
        }
    }

    // ansi port list :
    // LRM (SV 1800) 12.2 :
    // Ports inherit direction and type from previous ports.
    // default port direction is 'input'.
    // default type is 'logic'.
    VeriExpression *port_decl ;
    unsigned previous_direction = VERI_INPUT ;
    // VIPER #5949: Set previous_kind and default_kind to VERI_REG:
    unsigned default_kind = VERI_REG ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = VERI_REG ;
    // VIPER #5949: Set default_type to VERI_LOGIC:
    if (IsSystemVeri()) {
        default_type = VERI_LOGIC ;
    }
    unsigned previous_type = default_type ;
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->ResolveAnsiPortDecl(_scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
    }

    // Local decls :
    VeriModuleItem *decl_item ;
    // Viper 5778: Need to mark local decls static/automatic if the task/function is static/automatic
    veri_environment env = VERI_UNDEF_ENV ;
    if (_automatic_type == VERI_STATIC) env = VERI_STATIC_ENV ;
    FOREACH_ARRAY_ITEM(_decls, i, decl_item) {
        if (decl_item) decl_item->Resolve(scope, env) ;
    }

    VeriNode::_has_delay_or_event_control = 0 ;
    // Statements
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        if (stmt) stmt->Resolve(scope, VERI_UNDEF_ENV) ;
    }
    if (VeriNode::_has_delay_or_event_control)  _contains_delay_or_event_control = 1 ;
    VeriNode::_has_delay_or_event_control = 0 ;

    // VIPER #4833 : Inside class randomize/rand_mode/constraint_mode
    // built-in methods cannit be overwridden
    unsigned func_token = (_name) ? VeriExpression::GetFuncToken(_name->GetName()): 0 ;
    switch(func_token) {
    case VERI_METHOD_CONSTRAINT_MODE :
    case VERI_METHOD_RAND_MODE :
    case VERI_METHOD_RANDOMIZE :
        // Check whether we are inside clas
        if (_name && scope && scope->GetClass()) {
            _name->Error("invalid declaration of built-in method %s", _name->GetName()) ;
        }
        break ;
    default :
        break ;
    }
    // VIPER #4728: Propagate virtual qualifier. If this function is defined within
    // a class, test whether that function is defined as virtual in base class. If
    // so set virtual qualifier to this function too.
    // Viper 5130. For virtual functions we need to validate the virtual method with that of super class
    // Thus activated the below code irrespective of GetQualifier(VERI_VIRTUAL)
    // Viper #5435 : Check base classes, as function can be declared virtual in
    // any base class:
    VeriIdDef *enclosing_class = (_scope) ? _scope->GetClass(): 0 ;
    VeriScope *enclosing_class_scope = (enclosing_class) ? enclosing_class->LocalScope(): 0 ;
    VeriScope *base_class_scope = enclosing_class_scope ? enclosing_class_scope->GetBaseClassScope(): 0 ;
    while (base_class_scope) {
        VeriIdDef *base_func_id = base_class_scope->FindLocal(_task ? _task->Name(): 0) ;
        VeriModuleItem *base_func_body = (base_func_id) ? base_func_id->GetModuleItem(): 0 ;
        if (base_func_body && base_func_body->GetQualifier(VERI_VIRTUAL)) {
            // Check data type of virtual function in base class with this
            if (_task) _task->ValidateVirtualMethod(base_func_id) ;
            SetQualifier(VERI_VIRTUAL) ;
            break ;
        }
        base_class_scope = base_class_scope->GetBaseClassScope() ;
    }
    // VIPER #4450 : Check body of static task to produce error if non-static
    // members are accessed within this.
    unsigned is_static = 0 ;
    if (GetQualifier(VERI_STATIC)) is_static = 1 ;

    // VIPER #4899 : For local qualifier before task decl, mark the identifier private
    if (GetQualifier(VERI_LOCAL) && _task) _task->SetIsPrivate() ;
    if (GetQualifier(VERI_PROTECTED) && _task) _task->SetIsProtected() ;

    if (is_static) {
        Set ids(POINTER_HASH) ;
        if (_scope) _scope->GetDeclaredIds(ids) ;
        // Create an object of a visitor that visits only IdRefs:
        FuncVisitor fv(is_static, &ids) ;
        Accept(fv) ; // Visit this function to check all IdRefs' resolved ids
    }
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriDefParam::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    unsigned i ;
    VeriDefParamAssign *defparam_assign ;
    FOREACH_ARRAY_ITEM(_defparam_assigns, i, defparam_assign) {
        if (defparam_assign) defparam_assign->Resolve(scope, VERI_LVALUE) ; // defparam assignments.
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriContinuousAssign::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_strength) {
        _strength->Resolve(scope, VERI_EXPRESSION) ;
        // VIPER #5625 : Produce error if both strengths are not specified
        if (!_strength->GetLVal() || !_strength->GetRVal()) {
            _strength->Error("both strengths required for %s", "continuous assignment") ;
        }
    }

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_delay, i, expr) {
        if (expr) expr->Resolve(scope, VERI_DELAY_CONTROL) ;
    }

    // Only net-assignments are allowed here (LRM A.6.2)
    VeriNetRegAssign *assign ;
    FOREACH_ARRAY_ITEM(_net_assigns, i, assign) {
        if (assign) assign->Resolve(scope, VERI_NET_LVALUE) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriGateInstantiation::Resolve(VeriScope *scope, veri_environment /* environment */)
{
    if (_strength) _strength->Resolve(scope, VERI_EXPRESSION) ;
    // VIPER #5677 : For gate instantiation both strengths are required for
    // strength specification. So produce error if both strengths are not specified.
    // '_strength' here can be charge strength also, so ignore those:
    // For pullup and pulldown gate single drive strength is allowed,
    // so ignore them also:
    if (_strength) {
        unsigned token =_strength->GetLVal() ;
        if ((token != VERI_SMALL) && (token != VERI_MEDIUM) && (token != VERI_LARGE) && (_inst_type != VERI_PULLUP) && (_inst_type != VERI_PULLDOWN)) {
            if (!_strength->GetRVal()) {
                _strength->Error("both strengths required for %s", "gate instantiation") ;
            }
        }
        // VIPER #5945 : Strength cannot be specified in cmos, mos, pass-enable
        // and pass switches.
        switch (_inst_type) {
        case VERI_CMOS :
        case VERI_RCMOS :
        case VERI_NMOS :
        case VERI_PMOS :
        case VERI_RNMOS :
        case VERI_RPMOS :
        case VERI_TRANIF1 :
        case VERI_TRANIF0 :
        case VERI_RTRANIF1 :
        case VERI_RTRANIF0 :
        case VERI_TRAN :
        case VERI_RTRAN :
            Error("strength is not allowed in %s switch", PrintToken(_inst_type)) ;
            break ;
        default:
            break ;
        }
    }

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_delay, i, expr) {
        if (expr) expr->Resolve(scope, VERI_DELAY_CONTROL) ;
    }

    if (_delay) {
        // VIPER 2541 : Validate delay count
        switch(_inst_type) {
        case VERI_NMOS :
        case VERI_PMOS :
        case VERI_RNMOS :
        case VERI_RPMOS :
        case VERI_CMOS :
        case VERI_RCMOS :
        case VERI_BUFIF0 :
        case VERI_BUFIF1 :
        case VERI_NOTIF0 :
        case VERI_NOTIF1 :
            if (_delay->Size() > 3) {
                Error("too many delays in gate instantiation") ;
            }
            break ;
        case VERI_TRAN :
        case VERI_RTRAN :
        case VERI_PULLUP :
        case VERI_PULLDOWN :
            Error("too many delays in gate instantiation") ;
            break ;
        case VERI_TRANIF1 :
        case VERI_TRANIF0 :
        case VERI_RTRANIF1 :
        case VERI_RTRANIF0 :
        case VERI_BUF :
        case VERI_NOT :
        default:
            if (_delay->Size() > 2) {
                if (_delay->Size() == 3) {
                    // VIPER #5248 (test_315): Allow three delay form with a warning message:
                    Warning("%s delay form with this gate violates IEEE 1364 LRM", "three") ;
                } else {
                    Error("too many delays in gate instantiation") ;
                }
            }
            break ;
        }
    }
    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(_insts, i, inst) {
        if (!inst) continue ;
        VeriRange *dims = inst->GetDimensions() ;
        if (dims) {
            // Viper 6598: Issue warning as we ignore multidimension gate instantiation array.
            // Popular simulators choose to error out.
            //if (dims->GetNext()) dims->Error("multiple dimensions in gate instantiation ignored") ;
            if (!IsSystemVeri() && dims->GetNext()) {
                // VIPER #7679: Error out in non-SV mode only since SV now supports multiple dimensions here:
                dims->Error("multiple dimensions in %s instantiation is not allowed in this mode of Verilog", "gate") ;
            }
            dims->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // All dimensions must be constant ranges :
        }
        Array *port_connects = inst->GetPortConnects() ;
        if (!port_connects) continue ;

        // Validate the port connection count. It can be done during
        // resolving arguments, but then same check will be performed for each
        // port connection. So, make it separate. Note : This check is also done
        // in RTL elaborator, which can be removed after this modification.
        switch (_inst_type) {
        case VERI_NMOS :
        case VERI_PMOS :
        case VERI_RNMOS :
        case VERI_RPMOS :
        case VERI_BUFIF0 :
        case VERI_BUFIF1 :
        case VERI_NOTIF0 :
        case VERI_NOTIF1 :
            // One output, one input, one enable
        case VERI_TRANIF1 :
        case VERI_TRANIF0 :
        case VERI_RTRANIF1 :
        case VERI_RTRANIF0 :
            // inout, inout, enable
            if (port_connects->Size() != 3) {
                Error("need %d terminals to this gate", 3) ;
            }
            break ;
        case VERI_CMOS :
        case VERI_RCMOS :
            // Output, input, ncontrol, pcontrol
            if (port_connects->Size() != 4) {
                Error("need %d terminals to this gate",4) ;
            }
            break ;

        case VERI_TRAN :
        case VERI_RTRAN :
            // Inout, inout
            if (port_connects->Size() != 2) {
                Error("need %d terminals to this gate",2) ;
            }
            break ;

        case VERI_PULLUP :
        case VERI_PULLDOWN :
            // Only one output
            if (port_connects->Size() != 1) {
                Error("too many actuals in instantiation") ;
            }
            break ;
        default:
            // Regular Verilog gate/buf/not: n-input/n-output
            if (port_connects->Size() < 2) {
                Error("need at least one output and one input to this gate") ;
            }
            break ;
        }

        // VIPER 2543 : Resolve connections properly, so that we can issue
        // error if non-lvalue is connected to inout/output port.
        // Do not call VeriInstId::Resolve.
        // Resolve dimensions of instance if any
        unsigned j ;
        FOREACH_ARRAY_ITEM(port_connects, j, expr) {
            if (!expr || expr->IsOpen()) {
                if (expr) expr->Error("unconnected terminal to this gate") ; // VIPER #2884 : Issue error for unconnected gate terminal
                continue ;
            }
            if (expr->NamedFormal()) { // VIPER #2884 : named connection not allowed in gate instance : error
                expr->Error("name-based port connection not allowed in gate-instantiation") ;
                continue ;
            }

            switch (_inst_type) {
            case VERI_BUF :
            case VERI_NOT :
                // one or more outputs, followed by one input:
                if (j==port_connects->Size()-1) {
                    expr->Resolve(scope, VERI_EXPRESSION) ; // input
                } else {
                    expr->Resolve(scope, VERI_NET_LVALUE) ; // output
                }
                break ;

            case VERI_NMOS :
            case VERI_PMOS :
            case VERI_RNMOS :
            case VERI_RPMOS :
            case VERI_BUFIF0 :
            case VERI_BUFIF1 :
            case VERI_NOTIF0 :
            case VERI_NOTIF1 :
                // One output, one input, one enable
                if (j == 0) { // Output
                    expr->Resolve(scope, VERI_NET_LVALUE) ;
                } else {
                    expr->Resolve(scope, VERI_EXPRESSION) ; // input
                }
                break ;
            case VERI_CMOS :
            case VERI_RCMOS :
                // Output, input, ncontrol, pcontrol
                if (j == 0 ) {
                    expr->Resolve(scope, VERI_NET_LVALUE) ; // output
                } else {
                    expr->Resolve(scope, VERI_EXPRESSION) ; // input
                }
                break ;

            case VERI_TRAN :
            case VERI_RTRAN :
                // Inout, inout
                expr->Resolve(scope, VERI_GATE_INOUT_PORT_CONNECTION) ; // inout
                break ;

            case VERI_TRANIF1 :
            case VERI_TRANIF0 :
            case VERI_RTRANIF1 :
            case VERI_RTRANIF0 :
                // inout, inout, enable
                if (j < 2) {
                    expr->Resolve(scope, VERI_GATE_INOUT_PORT_CONNECTION) ; // inout
                } else {
                    expr->Resolve(scope, VERI_EXPRESSION) ; // input
                }
                break ;

            case VERI_PULLUP :
            case VERI_PULLDOWN :
                // Only one output
                expr->Resolve(scope, VERI_NET_LVALUE) ; // output
                break ;
            default:
                // Regular Verilog gate: The first one is always the output. Rest is input.
                if (j==0) {
                    expr->Resolve(scope, VERI_NET_LVALUE) ; // output
                } else {
                    expr->Resolve(scope, VERI_EXPRESSION) ; // input
                }
                break ;
            }
        }
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriModuleInstantiation::Resolve(VeriScope *scope, veri_environment environment)
{
    // _module_name cannot be hierarchical name, but according to yacc rules, instantiations
    // can be created with hierchical name as module_name. Error out here
    if (_module_name && _module_name->IsHierName()) {
        _module_name->Error("syntax error near %s", _module_name->GetName() ? _module_name->GetName() : "") ;
    }
    // Resolve the module name first.
    // In Verilog 2000, this resolving is not actually needed (in analysis), since we
    // should always search for a module (don't look in the scope). And modules are likely not yet defined.
    // But in System Verilog, this could be a nested module name, or a program name,
    // or a class name, or an interface name, many of which could be visible from the scope.
    // So call 'Resolve' on it any way, see what is directly visible.
    if (_module_name) {
        // VIPER #6288: This may be the second time Resolve is called. It may be the case that
        // previously we have resolved the instantiated module correctly, so, save it now:
        VeriIdDef *prev_module = _module_name->GetId() ;
        // Do check for valid module id (class/interface/program etc) and reset it anyway if invalid:
        // VIPER #7210: Class instantiation in module instantiation style is not allowed.
        // Only module, interface, checker and program can be instantiated in this way.
        // All of them are derived from VeriModule class so IsModule() will return true on them.
        // Specifically, check for package which cannot be instantiated in this way.
        if (prev_module && (!prev_module->IsModule() || prev_module->IsPackage())) prev_module = 0 ;

        // Try to find the name here in the normal scope
        _module_name->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ; // don't complain if id not found.

        // Directly visible identifiers here can be :
        // a module (IsModule()), a class type (IsClass())(IsType()), a program (IsProgram()/IsModule()), or a interface (IsInterface()/IsModule())
        VeriIdDef *the_module = _module_name->GetId() ;

        // VIPER #7210: Check for proper constructs only which can be instantiated in this way:
        if (the_module && (!the_module->IsModule() || the_module->IsPackage())) {
            VeriIdDef *container = (scope) ? scope->GetContainingModule() : 0 ;
            VeriModule *mod = (container) ? container->GetModule() : 0 ;
            VeriLibrary *lib = (mod) ? mod->GetLibrary() : 0 ;
            if (!lib) lib = veri_file::GetWorkLib() ;
            // Prevent error when both a module and a class with same name exist in the visible scope/work library:
            if (the_module->IsPackage() || (the_module->IsClass() && (!lib || !lib->GetModule(the_module->Name(), 1 /* case sensitive */, 0 /* do not restore */)))) {
                // VIPER #7210: Error out here, simulators seem to produce error for class and package instantiations:
                Error("illegal instantiation: %s %s cannot be instantiated in this way", ((the_module->IsPackage())?"package":"class"), the_module->Name()) ;
            }
            // This must be a local reference to a variable or so
            // Since these are in a different name space, rule them out right here (Verilog intent is to look for a module).
            // Set back the id pointer, to indicate that its a real module instantiation.
            // Module must be resolved at elaboration time.
            // Alternatively, Resolve() with UPWARD_SCOPE_NAME could ignore anything other than functions/tasks etc in pure scope.
            // VIPER #6288: Reset back to the previous module id which will be valid in re-resolve flow:
            _module_name->SetId(prev_module) ;
        }

        // VIPER #5199: Do not resolve to an extern module/interface/class/program etc. The extern declaration
        // will get deleted with its actual/full declaration and we will have a corrupted pointer here:
        if (the_module && the_module->GetModuleItem() && the_module->GetModuleItem()->GetQualifier(VERI_EXTERN)) _module_name->SetId(0) ;
    }

    if (_strength) _strength->Resolve(scope, VERI_EXPRESSION) ;
    // VIPER #5677 : For udp instantiation both strengths are required for
    // strength specification. So produce error if both strengths are not specified.
    // '_strength' here can be charge strength also. Ignore those:
    if (_strength) {
        unsigned token =_strength->GetLVal() ;
        if ((token != VERI_SMALL) && (token != VERI_MEDIUM) && (token != VERI_LARGE)) {
            if (!_strength->GetRVal()) {
                _strength->Error("both strengths required for %s", "udp instantiation") ;
            }
        }
    }

    // VIPER #3293 : Issue error on mixed used of ordered/named parameter assocs :
    unsigned i = 0 ;
    VeriExpression *expr ;
    VeriTreeNode *error_node = (_module_name) ? (VeriTreeNode *)_module_name : (VeriTreeNode *)this ;
    unsigned is_empty = 0 ;
    unsigned is_named = 0 ;
    unsigned is_ordered = 0 ;
    Set formal_name_set(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(_param_values, i, expr) {
        if (!expr) { is_empty = 1 ; continue ; }
        if (!is_empty) error_node = expr ;
        const char *formal_name = expr->NamedFormal() ;
        if (formal_name) {
            is_named = 1 ;
        } else {
            is_ordered = 1 ;
        }
        if (is_ordered && is_named) {
            // VCS and ModelSim give error. Follow that. Customers can always downgrade this message.
            Error("parameter connections cannot be mixed ordered and named") ;
        }

        if (formal_name) {
            // Viper# 3330 : Check consistency for param overrides
            if (formal_name_set.GetItem(formal_name)) {
                // Modelsim gives error, VCS gives warning. Lets be conservative
                Warning("multiple overrides for parameter %s", formal_name) ;
            }

            (void) formal_name_set.Insert(formal_name) ;
        }

        expr->Resolve(scope, VERI_PARAM_VALUE) ; // Can be constant expression/data type (for type parameter)
    }

    // VIPER #6591: Check for empty parameter assignments:
    if (is_empty && // Got at-lest one empty parameter assignment in the list
         // Empty list #() allowed only in IEEE 1800 mode or in relaxed checking mode:
         // VIPER #6591: Looks like simulators do not produce any error/warning for empty #()
         // parameter override even in Verilog 95/200 mode, so, disable it here:
        (/*((i==1) && !IsSystemVeri() && !InRelaxedCheckingMode()) ||*/
         // Empty item in list allowed only in positional connection or in relaxed checking mode:
         ((i>1) && (is_named || !InRelaxedCheckingMode())))) {
        error_node->Error("empty parameter assignment not allowed") ;
    }

    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (inst) inst->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSpecifyBlock::Resolve(VeriScope *scope, veri_environment environment)
{
    // Switch to local scope if it is set :
    if (_scope) scope = _scope ;

    VeriModuleItem *mi ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_specify_items, i, mi) {
        if (mi) mi->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriPathDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // LRM A.7.4.
    if (_cond) _cond->Resolve(scope, VERI_EXPRESSION) ; // FIX ME : Should be 'VERI_MODULE_PATH'
    if (_path) _path->Resolve(scope, VERI_UNDEF_ENV) ; // for a 'VeriPath' : environment not important.

    unsigned i ;
    VeriExpression *path_delay ;
    FOREACH_ARRAY_ITEM(_delay, i, path_delay) {
        if (path_delay) path_delay->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // A.7.4 : constant mintypmax expression.
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSystemTimingCheck::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;

    VeriExpression *arg_expr = 0 ;
    unsigned i ;

    // VIPER# 3288, 3289 : Support System Timing Check Symantics
    // Intercept system functions which we know something about
    switch (_function_type) {
    case VERI_SYS_CALL_REMOVAL :
    case VERI_SYS_CALL_SETUP :
    case VERI_SYS_CALL_HOLD :
    case VERI_SYS_CALL_RECOVERY :
    case VERI_SYS_CALL_SKEW :
        if (!_args || (_args->Size() < 3)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 3) ;
        }

        if (_args && (_args->Size() > 4)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 4) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 3) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i == 2) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // should be non_negative_const
            } else if (i < 2) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            }
        }
        return ;
    case VERI_SYS_CALL_RECREM :
    case VERI_SYS_CALL_SETUPHOLD :
        if (!_args || (_args->Size() < 4)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 4) ;
        }

        if (_args && (_args->Size() > 9)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 9) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 4) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i == 2 || i == 3) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // should be non_negative_const
            } else if (i < 2) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else if (i == 4) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            } else {
                arg_expr->Resolve(scope, VERI_EXPRESSION) ;
            }
        }
        return ;
    case VERI_SYS_CALL_TIMESKEW :
        if (!_args || (_args->Size() < 3)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 3) ;
        }

        if (_args && (_args->Size() > 6)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 6) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 3) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i == 2 || i == 4 || i == 5) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // should be non_negative_const
            } else if (i < 2) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else { // i == 3
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            }
        }
        return ;
    case VERI_SYS_CALL_FULLSKEW :
        if (!_args || (_args->Size() < 4)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 4) ;
        }

        if (_args && (_args->Size() > 7)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 7) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 4) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i == 2 || i == 3 || i == 5 || i == 6) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // should be non_negative_const
            } else if (i < 2) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else { // i == 4
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            }
        }
        return ;
    case VERI_SYS_CALL_WIDTH :
        if (!_args || (_args->Size() < 2)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 2) ;
        }

        if (_args && (_args->Size() > 4)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 4) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 2) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i == 2 || i == 1) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // should be non_negative_const
            } else if (i == 0) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else { // i == 3
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            }
        }
        return ;
    case VERI_SYS_CALL_PERIOD :
        if (!_args || (_args->Size() < 2)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 2) ;
        }

        if (_args && (_args->Size() > 3)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 3) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 2) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i == 1) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // should be non_negative_const
            } else if (i == 0) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else { // i == 2
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            }
        }
        return ;
    case VERI_SYS_CALL_NOCHANGE :
        if (!_args || (_args->Size() < 4)) { // fourth argument optional
            if (_task_name) Warning("%s expects at least %d arguments", _task_name, 4) ;
        }

        if (_args && (_args->Size() > 5)) { // fourth argument optional
            if (_task_name) Warning("%s expects at most %d arguments", _task_name, 5) ;
        }

        FOREACH_ARRAY_ITEM(_args, i, arg_expr) {
            if (!arg_expr) {
                if (i < 4) Warning("missing or empty argument against format specification for %s", _task_name) ;
                continue ;
            }

            if (i < 2) {
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) ;
            } else if (i < 4) {
                arg_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ; // min_typ_max
            } else { // i == 4
                arg_expr->Resolve(scope, VERI_TIMING_CHECK_REGISTER_IDENTIFIER) ;
            }
        }
        return ;
    default :
        break ;
    }

    // Check the arguments (is this a good idea ?)
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_args, i, expr) {
        if (expr) expr->Resolve(scope, VERI_UNDEF_ENV) ; // Don't pass any environment (tests) in here. Specific arguments are allowed...
    }
}

void VeriInitialConstruct::Resolve(VeriScope *scope, veri_environment environment)
{
    (void) environment ;
    VeriNode::_has_delay_or_event_control = 0 ;
    if (_stmt) _stmt->Resolve(scope, VERI_INITIAL_BLOCK) ;
    VeriNode::_has_delay_or_event_control = 0 ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriAlwaysConstruct::Resolve(VeriScope *scope, veri_environment environment)
{
    veri_environment org_environment = environment ;

    if (GetQualifier(VERI_ALWAYS_COMB) || GetQualifier(VERI_ALWAYS_LATCH)) {
        // VIPER 2447 :It is always_comb/always_latch. In its statements blocking timing or
        // event controls are not allowed. Pass VERI_SPECIAL_ALWAYS environment
        // to check it
        environment = VERI_SPECIAL_ALWAYS ;
    }
    if (GetQualifier(VERI_ALWAYS_FF)) {
        environment = VERI_ALWAYS_FF_BLOCK ; // VIPER# 4241 :To check if it's a always_ff

        // Error accroding to IEEE 1800 LRM section 11.4 of "Sequential logic":
        if (_stmt) {
            StmtVisit st_visit ;
            _stmt->Accept(st_visit) ;
            if (st_visit.ContainsEventControl() != 1) {
                Error("an always_ff block must have one and only one event control") ; //Viper 4890, 7323
            }
        }
    }
    // VIPER #4204 : It is final block. In its statements event control/delay
    // control/task call are not allowed. Pass different environment to check those
    if (GetQualifier(VERI_FINAL)) environment = VERI_FINAL_BLOCK ;

    if (environment == org_environment && _stmt && _stmt->IsEventControl()) {
        Array *at = _stmt->GetAt() ;
        if (at) environment = VERI_ALWAYS_BLOCK ;
    }

    // VIPER #7506: Warn for a potential always loop: an unused hanging process.
    // It will be a infinite loop if an always construct does not contain any
    // event control statement/delay statement/wait statement.
    // Set the flag
    VeriNode::_potential_always_loop = 1 ;

    VeriNode::_has_delay_or_event_control = 0 ;

    // Resolve _stmt:
    if (_stmt) _stmt->Resolve(scope, environment) ;

    // VIPER #7506: Do not warn for a potential always loop: an unsued hanging process.
    // If we are in always_comb/always_latch/always_ff/final block
    if (GetQualifier(VERI_ALWAYS_COMB) || GetQualifier(VERI_ALWAYS_LATCH)
        || GetQualifier(VERI_ALWAYS_FF) || GetQualifier(VERI_FINAL)) {
            _potential_always_loop = 0 ;
    }

    VeriNode::_has_delay_or_event_control = 0 ;

    // Warn here:
    if (_potential_always_loop) {
        Warning("potential always loop found") ;
    }
    // Reset the flag
    VeriNode::_potential_always_loop = 0 ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriGenerateConstruct::Resolve(VeriScope *scope, veri_environment environment)
{
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriGenerateConditional::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_if_expr) _if_expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    if (_then_item) _then_item->Resolve(_then_scope, environment) ;
    if (_else_item) _else_item->Resolve(_else_scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriGenerateCase::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_cond) _cond->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;

    // Pass resolved case condition into the case items resolving
    unsigned i ;
    VeriGenerateCaseItem *ci ;
    unsigned default_item_count = 0 ; // Contains the number of 'default' case
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        if (!ci) continue ;
        ci->Resolve(scope, _cond) ;
        if (ci->IsDefault()) default_item_count++ ; // Increase default count for default case item
        // VIPER 2884: Error out if the 'default' case item appears more than once
        if (default_item_count > 1) ci->Error("default case should appear only once") ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriGenerateFor::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_loop_scope) scope = _loop_scope ; // Switch to local scope created for initialization
    if (scope) scope->SetLoopScope(1) ;

    if (_initial) _initial->Resolve(scope, environment) ;
    if (_cond) _cond->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    // VIPER #3237 : increment/decrement should be done only on genvar
    if (_rep) _rep->Resolve(scope, VERI_GENVAR_LVALUE) ;

    if (scope) scope->SetLoopScope(0) ;
    if (_scope) scope = _scope ; // adjust to local scope if there..

    // Resolve the generate items :
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriGenerateBlock::Resolve(VeriScope *scope, veri_environment environment)
{
    // Switch to local scope if it is set :
    if (_scope) scope = _scope ;

    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTable::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // VIPER #4506 & #4532: Semantic check for UDP table entries:

    // Set up array of input nets, and the output net
    // LRM 8.1.4: The order of the input state fields of each row of the state
    // table is taken directly from the port list in the UDP definition header.
    // Luckily, only full-id ports are allowed, so we can use the module's GetPorts() array:

    // The following checks have been moved to the constructor of
    // VeriPrimitive class (VIPER 2884):
    //  * if the first port is an output port or not
    //  * if any port is vector or not
    //  * if any of the ports is inout or not
    //  * if none of the ports is of type 'output'

    if (!_the_module) return ; // cannot check anything in the table if we do not have the module it is contained in.

    // Get the module ports :
    Array *module_ports = _the_module->GetPorts() ;

    // Check table style consistency and extract it's style (set _is_seq/_is_comb)
    // First get the number of inputs :
    unsigned in_size = (module_ports) ? module_ports->Size() : 0 ;
    if (in_size) in_size-- ;

    // Then check that against the table entries :
    unsigned in_length = 0 ;
    const char *edge = 0 ;
    const char *IOsep = 0 ;
    // VIPER 5383: compat issue with VC8.0
    // const char *table_entry ;
    char *table_entry ;
    unsigned i, j ;
    Map table_entry_map(STRING_HASH) ; // VIPER #5812
    FOREACH_ARRAY_ITEM(_table_entries, i, table_entry) {
        if (!table_entry) continue ;
        IOsep = strchr(table_entry,':') ;
        if (!IOsep) {
            Error("table entry without ':' IO separator : %s",table_entry) ;
            continue ;
        }
        // Check input length
        in_length = (unsigned)(IOsep - table_entry) ;
        if (in_length != in_size) {
            Error("incorrect number of inputs for table entry : %s",table_entry) ;
            continue ;
        }
        if (Strings::len(table_entry) == in_length+4) {
            if (_is_comb) {
                Error("sequential table entry found in combinational table : %s",table_entry) ;
                continue ;
            }
            _is_seq = 1 ;
        } else if (Strings::len(table_entry) == in_length + 2) {
            if (_is_seq) {
                Error("missing 'present state' field in sequential table : %s",table_entry) ;
                continue ;
            }
            _is_comb = 1 ;
        } else {
            Error("incorrect output field length found in table : %s",table_entry) ;
            continue ;
        }
        // Find second separator
        if (_is_seq && *(IOsep+2)!=':') {
            Error("present state ':' output field separator misplaced : %s",table_entry) ;
            continue ;
        }

        edge = strpbrk(table_entry, "ABCDEFGHIJKLMNOPQRSTUVWXYrfpn*") ;
        if (edge && _is_comb) {
            // VIPER #4506: Error out for edge in combinational UDP:
            const char *print_edge = PrintUdpEdgeChar(*edge) ;
            if (!print_edge) print_edge = table_entry ; // Use the full table entry for non-encypted edges
            Error("edge illegal in combinational table : %s", print_edge) ;
            continue ;
        }

        // Add more semantic checks
        if (edge && (*edge == 'A' || *edge == 'G' || *edge == 'M')) {
            const char *print_edge = PrintUdpEdgeChar(*edge) ;
            if (!print_edge) print_edge = table_entry ; // Use the full table entry for non-encypted edges
            Warning("invalid edge specification %s in udp", print_edge) ;
        }

        // VIPER #5812/3444/6348: Check for conflicting entry
        char *inp_str = Strings::save(table_entry) ;
        char *inp_end = strchr(inp_str,':') ;
        if (inp_end) *inp_end = '\0' ;
        char *out_str = inp_end ? inp_end+1 : 0 ;

        MapIter smi ;
        char *inp, *outp ;
        FOREACH_MAP_ITEM(&table_entry_map, smi, &inp, &outp) {
            if (!out_str) break ;
            if (!inp || !outp) continue ;
            if (in_length != Strings::len(inp)) continue ;

            unsigned overlap = 1 ;
            for (j = 0 ; j < in_length ; ++j) {
                if (!AreUdpCharsOverlapping(inp[j], inp_str[j])) overlap = 0 ;
                if (!overlap) break ;
            }
            if (!overlap) continue ;

            // input values are overlapping.

            if (_is_seq) {
                if (out_str[0] != outp[0] && out_str[0] != '?' && outp[0] != '?') continue ; // different transition

                // 'q' is identical or '?'
                unsigned output_same = 1 ;
                char c1 = out_str[2] ;
                char c2 = outp[2] ;
                if (c1 != '-' && c2 != '-') {
                    output_same = (c1 == c2) ;
                } else if (c1 != '-' && c2 == '-') {
                    output_same = (c1 == outp[0]) || (c1 == out_str[0] && outp[0] == '?') ;
                } else if (c1 == '-' && c2 != '-') {
                    output_same = (c2 == out_str[0]) || (c2 == outp[0] && out_str[0] == '?') ;
                }

                if (!output_same) {
                    char *print_str = Strings::allocate(Strings::len(table_entry) * 4) ;
                    char *r = table_entry ;
                    char *r1 = print_str ;
                    while (r && *r!='\0') {
                        const char *c = PrintUdpEdgeChar(*r) ;
                        if (!c) {
                            *r1 = *r ; ++r1 ;
                        } else {
                            for (j = 0 ; j < Strings::len(c) ; ++j) {
                                *r1 = c[j] ; ++r1 ;
                            }
                        }
                        ++r ;
                        *r1 = '\0' ;
                    }
                    Warning("udp table entry %s conflicts with earlier entry", print_str) ;
                    Strings::free(print_str) ;
                }
            } else {
                // VIPER #6348: Conflict check for combinational UDPs:
                if (out_str[0] != outp[0]) {
                    // Here the outputs are conflicting
                    Warning("udp table entry %s conflicts with earlier entry", table_entry) ;
                //} else {
                //    // Here the outputs are identical
                //    Warning("udp table entry %s is redundant", table_entry) ;
                }
            }
        }
        if (!table_entry_map.Insert(inp_str, (void *)out_str)) Strings::free(inp_str) ;
        // VIPER #5812/3444/6348: Done
    }

    MapIter smi ;
    char *inp ;
    FOREACH_MAP_ITEM(&table_entry_map, smi, &inp, 0) Strings::free(inp) ; // VIPER #5812: cleanup

    // These checks used to be in the VeriPrimitive constructor.
    // Now moved here, so that they are consistent and independent of other routines.
    // Only requirement is that the module header is Resolved before we resolve this table.
    unsigned count_output = 0 ;
    VeriIdDef *port_id ;
    // VIPER 2884: Check if an UDP port follow correct semantics or not.
    FOREACH_ARRAY_ITEM(module_ports, i, port_id) {
        if (!port_id) continue ;
        // Issue error if the base type of an UDP port is a vector type or if
        // it contains packed dimensions
        VeriDataType *data_type = port_id->GetDataType() ;
        if (port_id->PackedDimension() || (data_type && data_type->BaseTypeSize()>1)){
            port_id->Error("every UDP port must be a scalar") ;
        }
        // Issue error if the first port of an UDP is not of type 'output'
        if (port_id->IsOutput()) {
            if (i!=0) port_id->Error("UDP output must be the first port") ;
            // If the port is an output port then increment count_output
            count_output++ ;
            // VIPER #5017: Initial value of output port should be a constant:
            VeriExpression *init_val = port_id->GetInitialValue() ;
            if (init_val) init_val->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
        }
        // Issue error if an UDP port cannot be declared inout.
        if (port_id->IsInout()) port_id->Error("inout ports not allowed on a UDP") ;

        if (IsCombinational() && port_id->IsReg()) port_id->Error("combinational udp port %s cannot be reg", port_id->Name()) ; // VIPER #4707

        if (IsSequential() && port_id->IsOutput() && !port_id->IsReg()) port_id->Error("sequential udp output port %s must be reg", port_id->Name()) ; // VIPER #4930
    }
    // Issue error if none of the UDP ports is of type 'output'
    if (!count_output) _the_module->Error("UDP has no output") ;

    // VIPER #5017: Check for non-port identifiers in the scope (of the module)
    MapIter mi ;
    FOREACH_MAP_ITEM((scope) ? scope->DeclArea() : 0, mi, 0, &port_id) {
        if (port_id && !port_id->IsPort()) port_id->Error("identifier %s is not in udp port list", port_id->Name()) ;
    }

    /*
    // VIPER #5248 (test_25): The following check is moved to VeriModule::ResolveBody under IsPrimitive():
    // VIPER #5017: Check for initial statement. Need to scan the module's module items :
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM_BACK(_the_module->GetModuleItems(), i, item) {
        if (!item || item->IsDataDecl() || item->IsTable()) continue ; // It is not an initial statement
        // Here, got an initial statement, nothing else is supported by bison now:
        if (IsCombinational()) item->Error("combinational udp cannot have initial statement") ;
        break ; // Only a single initial statement is supported in bison
    }
    */

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

// Resolve for System Verilog classes :
void VeriClass::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // If there is a 'base_class_name' given, then we need to let the local scope
    // here be derived from the base class scope. Otherwise base class members will
    // be invisible. Do that here (Before we start to resolve the body).
    VeriTypeInfo *base_type = 0 ; // Type of base class reference
    if (_base_class_name) {
        // Resolve in the scope from above..
        _base_class_name->Resolve(scope, VERI_CLASS_NAME) ; // expect to find class name here.

        // Now pick-up the class identifier :
        //VeriIdDef *base_class = _base_class_name->GetId() ;
        // If the base class if a type parameter, take its initial value:
        //if (base_class && base_class->IsParam() && base_class->IsType()) {
            //VeriExpression *initial = base_class->GetInitialValue() ;
            //base_class = (initial) ? initial->GetId() : 0 ;
        //}
        // VIPER #6590 : Extract the base class identifier when base class reference is
        // 1. class name, 2. type parameter name, 3. type identifier name
        base_type = _base_class_name->CheckType(0, NO_ENV) ;
        VeriIdDef *base_class = (base_type) ? base_type->GetNameId(): 0 ;
        //if (base_type && !base_type->IsTypeParameterType() && !base_type->IsUnresolvedNameType()) {
            //base_class = base_type->GetNameId() ;
            if (base_class && !base_class->IsClass()) base_class = 0 ;
        //}

        VeriModuleItem *base_class_item = base_class ? base_class->GetModuleItem() : 0 ;
        // IEEE 2012 Section 8.26 : interface class cannot be extended
        if (base_class_item && base_class_item->GetQualifier(VERI_INTERFACE)) {
            _base_class_name->Error("illegal extends, class %s may not extend an interface class", _id ? _id->Name(): "") ;
        }
        if (base_class_item && base_class_item->HasStaticMembers()) _has_static_members = 1 ;
        // If this identifier is not there, then it was not declared and error is already given.
        // Here, we just need to pick up its scope, and set our own 'upper' pointer to that :
        if (_scope && base_class && base_class->LocalScope()) {
            // NOTE : This (SetUpper()) is risky, and not normally done.
            // Check if this causes problems elsewhere (in tree copying etc)..
            // Check if this (scope points to base class scope only) causes other Verilog scope semantic problems..
            // Otherwise, we might need an 'extended' scope pointer in VeriScope for base-class scope visibility..
            // VIPER #4954: Do not use SetUpper(), added a base class pointer in VeriScope, set it from here:
            _scope->SetBaseClassScope(base_class->LocalScope()) ;
        }
    }

    // Checking for interface class
    unsigned interface_class = GetQualifier(VERI_INTERFACE) ? 1 : 0 ;
    // Validate extends specific classes for interface class and implements specified
    // classes for other class
    unsigned i ;
    VeriName *interface_class_ref ;
    FOREACH_ARRAY_ITEM(_interface_classes, i, interface_class_ref) {
        if (!interface_class_ref) continue ;
        // Resolve in the scope from above..
        interface_class_ref->Resolve(scope, VERI_CLASS_NAME) ; // expect to find class name here.

        // Now pick-up the class identifier :
        VeriIdDef *interface_class_id = interface_class_ref->GetId() ;

        VeriModuleItem *interface_class_item = interface_class_id ? interface_class_id->GetModuleItem() : 0 ;
        // IEEE 2012 Section 8.26 : Only interface class can be used here
        // type parameter referring to an interface class cannot be used here
        // Section 8.26.4 : An interface class shall be declared before it is
        // implemented/extended
        if ((interface_class_id && !interface_class_id->IsClass()) || !interface_class_item || !interface_class_item->GetQualifier(VERI_INTERFACE)) {
            interface_class_ref->Error("only interface class can be referenced in %s clause", interface_class ? "extends" : "implements") ;
        }
    }

    // Now flip to local scope :
    if (_scope) scope = _scope ;

    // Resolve all sub-nodes :
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_parameter_connects, i, item) {
        if (item) item->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // VIPER #5248 (test case: Amd_cnb_031808_block_echo_RH30_64).
    // If the base class is a type parameter or a typedef (which is declared but not
    // defined before the derived class) we do not resolve the class items,
    // so that no error is produced when base class members are used in derived class
    // For all other type of base classes we resolve the class items.
    //VeriIdDef *base_id = _base_class_name ? _base_class_name->GetId() : 0 ;
    //VeriModuleItem *base_class_item = base_id ? base_id->GetModuleItem() : 0 ;
    //if (!_base_class_name || (base_class_item && base_class_item->IsClass())) {
    //VeriTypeInfo *base_type = _base_class_name ? _base_class_name->CheckType(0, NO_ENV): 0 ;
    if (!base_type || (!base_type->IsTypeParameterType() && !base_type->IsUnresolvedNameType())) {
        _is_resolved = 1 ; // Mark as resolved
        FOREACH_ARRAY_ITEM(_items, i, item) {
            if (!item) continue ;
            item->Resolve(scope, VERI_UNDEF_ENV) ;
            if (item->IsDataDecl() && item->GetQualifier(VERI_STATIC)) _has_static_members = 1 ;
        }
    }
    delete base_type ;
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriPropertyDecl::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_is_processing) { // Cyclic reference, return :
        return ;
    }
    _is_processing = 1 ; // Set start processing
    // Flip to local scope :
    if (_scope) scope = _scope ;

    // Resolve all sub-nodes :

    // Set base 'type' field on ansi ports :
    // LRM (SV 1800) 12.2 :
    // Ports inherit direction and type from previous ports.
    // default port direction is 'input'.
    // default type is 'logic'.
    unsigned i ;
    VeriAnsiPortDecl *port_decl ;
    // For property, all ports are input (checked in constructor already)
    unsigned previous_direction = VERI_INPUT ;
    // For property, default type should be 'untyped' (LRM . I don't have that, so give type 'logic' ?
    // VIPER #5949: Set previous_kind and default_kind to VERI_LOGIC:
    unsigned default_kind = VERI_LOGIC ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = VERI_UNTYPED ;
    unsigned previous_type = default_type ;
    // Now do the ansi port list itself :
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->ResolveAnsiPortDecl(_scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
    }

    // Resolve the local declarations :
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decls, i, item) {
        if (item) item->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // resolve the (implementation) spec :
    if (_spec) _spec->Resolve(scope, environment==VERI_CLOCKING_BLOCK ? VERI_CLOCKING_BLOCK : VERI_PROPERTY_ENV) ; // Give 'property' environment, so we know what the expressions mean..

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
    _is_processing = 0 ; // Set finish processing
}

void VeriSequenceDecl::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_is_processing) { // Cyclic reference, return
        Error("cyclic reference to SVA %s %s", "sequence", (_id) ? _id->Name(): "") ;
        return ;
    }
    _is_processing = 1 ; // Set start processing
    // Flip to local scope :
    if (_scope) scope = _scope ;

    // Resolve all sub-nodes :

    // Set base 'type' field on ansi ports :
    // LRM (SV 1800) 12.2 :
    // Ports inherit direction and type from previous ports.
    // default port direction is 'input'.
    // default type is 'logic'.
    unsigned i ;
    VeriAnsiPortDecl *port_decl ;
    // For sequence, all ports are input (checked in constructor already)
    unsigned previous_direction = VERI_INPUT ;
    // For sequence, default type should be 'untyped' (LRM . I don't have that, so give type 'logic' ?
    // VIPER #5949: Set previous_kind and default_kind to VERI_LOGIC:
    unsigned default_kind = VERI_LOGIC ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = VERI_UNTYPED ;
    unsigned previous_type = default_type ;
    // Now do the ansi port list itself :
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->ResolveAnsiPortDecl(_scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
    }

    // Resolve the local declarations :
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_decls, i, item) {
        if (item) item->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // resolve the (implementation) spec :
    if (_spec) _spec->Resolve(scope, environment==VERI_CLOCKING_BLOCK ? VERI_CLOCKING_BLOCK : VERI_SEQUENCE_ENV) ; // Give 'property' environment, so we know what the expressions mean..

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
    _is_processing = 0 ; // Set finish processing
}

void VeriModport::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Give error if this modport declaration is used outside interface
    VeriIdDef *unit_id = scope ? scope->GetContainingModule(): 0 ;
    if (unit_id && !unit_id->IsInterface()) { // Enclosing unit not interface
        Error("syntax error near %s", "modport") ;
    }
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_modport_decls, i, item) {
        if (item) item->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriModportDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    unsigned i ;
    VeriIdDef *modport_port ; // the modportport prototype..
    VeriIdDef *modport_var ; // the original variable
    VeriScope *container_scope = scope ;
    VeriModuleItem *modport_port_decl ;
    VeriScope *interface_scope = 0 ;
    VeriScope *s = container_scope ;
    while(s) {
        VeriIdDef *owner = s->GetOwner() ;
        if (owner && owner->IsInterface()) {
            interface_scope = s ; break ;
        }
        s = s->Upper() ;
    }

    // If function/task is import/export inside modport, 2 situation can arise
    // 1. Function/task definition is declared within interface
    // 2. Function/task definition is within a module who uses this modport
    // For this (2) situation we need to define an identifier in interface scope.
    // If prototypes are defined within modport, we know whether it is task or
    // function. But if only names are defined within modport, we do not know
    // whether to define task or function within interface. In such situation
    // we will declare function identifier.
    FOREACH_ARRAY_ITEM(_modport_port_decls, i, modport_port_decl) {
        if (!modport_port_decl || (!modport_port_decl->GetQualifier(VERI_EXPORT) && !modport_port_decl->GetQualifier(VERI_IMPORT))) continue ;
        unsigned j ;
        Array *ids = modport_port_decl->GetIds() ;
        Array *tmp_ids = 0 ;
        if (ids) { // list of task/function
            tmp_ids = new Array(*ids) ;
        } else { // function/task proptotype
            tmp_ids = new Array(1) ;
            tmp_ids->InsertLast(modport_port_decl->GetId()) ;
        }
        FOREACH_ARRAY_ITEM(tmp_ids, j, modport_port) {
            if (!modport_port || modport_port->IsNamedPort()) continue ;// Named ports are not declared in interface scope
            modport_var = interface_scope ? interface_scope->Find(modport_port->Name(), /*from*/this) : 0 ;
            if (!modport_var && interface_scope) {
                VeriIdDef *func_id = 0 ;
                if (modport_port_decl->IsFunctionDecl()) {
                    func_id = new VeriFunctionId(Strings::save(modport_port->Name())) ;
                } else if (modport_port_decl->IsTaskDecl()) {
                    func_id = new VeriTaskId(Strings::save(modport_port->Name())) ;
                } else {
                    func_id = new VeriFunctionId(Strings::save(modport_port->Name())) ;
                }
                if (!interface_scope->Declare(func_id)) {
                    // FIXME: Following code should never be executed, since modport_var==0 (VIPER #7773)
                    Error("%s is already declared", modport_port->Name()) ;
                    delete func_id ;
                    func_id = 0 ;
                }
            }
        }
        delete tmp_ids ;
    }
    // We need to check if the modport-ports that were declared here
    // are already declared earlier (as variables).
    // Do that by resolving the names of the accumulated ports in the
    // current scope (which is exactly one level above the modportport decls)
    // This code might be obsolete once we resolve proto id's.
    VeriIdDef *interface_id = (interface_scope) ? interface_scope->GetOwner() : 0 ;
    FOREACH_ARRAY_ITEM(_ports, i, modport_port) {
        if (!modport_port || modport_port->IsNamedPort()) continue ;// Named ports are not declared in interface scope
        // VIPER #5248 (respecify_sig_direction): If actual-id is already set, no need to find it again:
        //if (modport_port->GetActualId()) continue ; // Do not continue, we want to do other checks
        modport_var = modport_port->GetActualId() ;
        // error out if modport_port was not declared :
        if (!modport_var) modport_var = (scope) ? scope->Find(modport_port->Name(), /*from*/this) : 0 ;
        // VIPER #6344: Check that modport ports are declared within the owner interface:
        VeriName *actual_name = modport_port->GetActualName() ;
        if (modport_var && !modport_var->IsFunction() && !modport_var->IsTask() && (!actual_name || !actual_name->IsHierName())) {
            s = scope ;
            unsigned found = 0 ;
            while (s && interface_scope) {
                // VIPER #8344 : Check only identifiers declared in scope 's', avoid explicitly/implicitly imported ids
                Map *scope_map =  s->GetThisScope() ;
                //if (modport_var == s->FindLocal(modport_port->Name())) { found = 1 ; break ; }
                if (scope_map && (modport_var == scope_map->GetValue(modport_port->Name()))) { found = 1 ; break ; }
                if (s == interface_scope) break ;
                s = s->Upper() ;
            }
            if (!found) modport_port->Error("modport item %s must refer to an item in interface %s",
                                                modport_port->Name(), (interface_id) ? interface_id->Name() : "") ;
        }
        // Viper 4851. If the moport_var is input the modport_port cannot be output or inout
        if (modport_var && modport_var->Dir() == VERI_INPUT && (modport_var->Dir() != modport_port->Dir())) {
            char *port_dir = 0 ;
            switch (modport_port->Dir()) {
            case VERI_OUTPUT : port_dir = Strings::save("output") ;
                               break ;
            case VERI_REF    : port_dir = Strings::save("ref") ;
                               break ;
            case VERI_CONST  : port_dir = Strings::save("const") ;
                               break ;
            default          : port_dir = Strings::save("inout") ;
                               break ;
            }
            Error("cannot re-specify direction for signal %s to %s because it was specified as input", modport_var->Name(), port_dir) ;
            Strings::free(port_dir) ;
        }
        // VIPER 2620. Functions/tasks can be exported and hence may not have a definition in the upper scope of the modport
        if (!modport_var && !modport_port->IsFunction() && !modport_port->IsTask()) {
            Error("%s is not declared", modport_port->Name()) ;
        }
        // VIPER #2994: modport id should not be declared as type in the upper scope:
        if (modport_var && (modport_var->IsType() || modport_var->IsClass())) {
            // Actual identifier was a typedef or a class, this is an illegal use:
            Error("%s was previously declared as type", modport_port->Name()) ;
        }
        // VIPER #7889: Task/function ids cannot be used as simple modport port with direction:
        if (modport_port->Dir() && (modport_var && (modport_var->IsFunction() || modport_var->IsTask()))) {
            Error("modport port %s does not support declared direction %s", modport_port->Name(), PrintToken(modport_port->Dir())) ;
        }
        // FIX ME : Set the modport_var to be the modport_port definition...
        // For now, just set the data_decl back-pointer in the modport_port
        // to the data_decl back-pointer of the defining variable :
        modport_port->SetActualId(modport_var) ;
        /*
        if (modport_var && modport_port->IsPort() && !modport_port->GetDataType()) {
            modport_port->SetDataType(modport_var->GetDataType()) ;
        }
        */
        // VIPER 2620. Set the modport_var which is a clocking id to be the clocking
        // decl definition.  For this set clocking decl definition as a back pointer
        // to modport_port. Also set the scope of the clocking decl as the local
        // scope of the modport_port.
        //if (!modport_port->IsPort() && modport_var) {
            //modport_port->SetModuleItem(modport_var->GetModuleItem()) ;
            //modport_port->SetLocalScope(modport_var->LocalScope()) ;
        //}
    }

    // Switch to local scope if it is set :
    if (_scope) scope = _scope ;

    // Resolve the modport_port decls (mostly VeriDataDecl) :
    //VeriModuleItem *modport_port_decl ;
    FOREACH_ARRAY_ITEM(_modport_port_decls, i, modport_port_decl) {
        if (modport_port_decl && (modport_port_decl->GetQualifier(VERI_IMPORT) || (modport_port_decl->GetQualifier(VERI_EXPORT)))) {
            // For modport item like 'export task_name' we create VeriDataDecl
            // and 'export'/'import' is set as qualifier to VeriDataDecl', but this
            // identifier 'task_name' is not included in '_ports' and this VeriDataDecl
            // does not have any direction. So check those here and bind actual definition
            // if any with this
            unsigned j ;
            Array *ids = modport_port_decl->GetIds() ;
            FOREACH_ARRAY_ITEM(ids, j, modport_port) {
                if (!modport_port || modport_port->IsNamedPort()) continue ;// Named ports are not declared in interface scope
                // error out if modport_port was not declared :
                modport_var = (container_scope) ? container_scope->Find(modport_port->Name(), /*from*/this) : 0 ;
                if (!modport_var) continue ;
                modport_port->SetActualId(modport_var) ;

                // VIPER #7891: Produce error if task/function id is used in exported task/function but declared as
                // imported task/function in the modport of the interface. If the task/function id is declared both
                // as export and import task/function we do not produce error.
                // interface intr;
                //     modport n(input a, import tsk1); // user wrote import instead of export by mistake
                // endinterface
                // module test(intr intr1);
                //     task intr1.tsk1; // this should be an error as tsk1 is not exported
                //         input logic a;
                //         a = 1;
                //     endtask
                // endmodule
                // eg. modport m(export/import tsk) ; // body of tsk is not declared in this interface, it does not
                // have any module item. Here we set the flag on the modport port declared in the interface.
                // and using them produce error in elaboration phase.
                if (!modport_port_decl->IsTaskDecl() && !modport_port_decl->IsFunctionDecl() && !modport_var->GetModuleItem()) {
                    if (modport_port_decl->GetQualifier(VERI_EXPORT)) {
                        modport_var->SetIsExportTaskFunc(1) ;
                    } else {
                        modport_var->SetIsImportTaskFunc(1) ;
                    }
                }
            }

            // VIPER #7891: Produce error if task/function id is used in exported task/function but declared as
            // imported task/function in the modport of the interface. If the task/function id is declared both
            // as export and import task/function we do not produce error.
            // We only produce error in the followin case:
            // interface intr;
            //     modport n1(input a, import task tsk1(input logic a)); // user wrote import instead of export by mistake
            // endinterface
            // module test(intr intr1);
            //     task intr1.tsk1; // this should be an error as tsk1 is not exported
            //         input logic a;
            //         a = 1;
            //     endtask
            // endmodule
            // eg. modport m(export/import tsk(input logic a) ; // body of tsk is not declared in this interface, it does not
            // have any module item, but is a task/fun decl. Here we set flag export/import on the modport port id and using
            // them produce error in elaboration phase.
            if (modport_port_decl->IsTaskDecl() || modport_port_decl->IsFunctionDecl()) { // eg. modport m(export/import task tsk(input logic a)) ;
                VeriIdDef *task_func_prot_id = modport_port_decl->GetId() ;
                VeriIdDef *task_func_decl_id = (task_func_prot_id && container_scope) ? container_scope->Find(task_func_prot_id->Name(), /*from*/this) : 0 ;
                VeriModuleItem *task_func_decl = task_func_decl_id ? task_func_decl_id->GetModuleItem() : 0 ;
                if (!task_func_decl && task_func_decl_id) {
                    if (modport_port_decl->GetQualifier(VERI_EXPORT)) {
                        task_func_decl_id->SetIsExportTaskFunc(1) ;
                    } else {
                        task_func_decl_id->SetIsImportTaskFunc(1) ;
                    }
                }
            }

            // Handling import/export task/function declarations:
            modport_port = modport_port_decl->GetId() ;
            modport_var = (container_scope && modport_port) ? container_scope->Find(modport_port->Name(), /*from*/this) : 0 ;
            if (modport_var && modport_port) modport_port->SetActualId(modport_var) ;

            if (container_scope && modport_port_decl->GetQualifier(VERI_IMPORT) &&
                (modport_port_decl->IsTaskDecl() || modport_port_decl->IsFunctionDecl())) {
                // Viper 4817. Check for prototype vs actual declaration of imported task
                // It is an error if the imported task does not contain the full declaration.
                VeriIdDef *task_func_prot_id = modport_port_decl->GetId() ;
                VeriIdDef *task_func_decl_id = task_func_prot_id ? container_scope->Find(task_func_prot_id->Name(), /*from*/this) : 0 ;
                VeriModuleItem *task_func_decl = task_func_decl_id ? task_func_decl_id->GetModuleItem() : 0 ;
                if (task_func_decl && (task_func_decl->IsTaskDecl() || (task_func_decl->IsFunctionDecl()))) {
                    task_func_decl->CheckDecl(modport_port_decl) ;
                }
            }
        }
        if (modport_port_decl) modport_port_decl->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriModuleItem::CheckDecl(VeriModuleItem *f_decl) const
{
    if (!f_decl || !(f_decl->IsFunctionDecl() || f_decl->IsTaskDecl())) return ;
    VeriIdDef *func_id = f_decl->GetId() ;
    VeriIdDef *decl_id = GetId() ;

    if (!func_id || !decl_id) return ;

    // Now check the formals of prototype with body :
    // Get ports of from prototype
    Array *proto_ports = f_decl->GetPorts() ;
    Array *port_list = GetPorts() ;

    // Calculate port count of prototype
    unsigned proto_port_count = (proto_ports) ? proto_ports->Size() : 0 ;
    unsigned this_port_count = (port_list) ? port_list->Size() : 0 ;

    // Produce error, if prototype and body have formal count mismatch :
    if (proto_port_count != this_port_count) {
        f_decl->Error("formal count of extern method %s does not match prototype", decl_id->Name()) ;
        return ;
    }

    unsigned i ;
    VeriIdDef *port ;
    VeriIdDef *proto_port ;
    unsigned body_port_count = 0 ;
    // Iterate over the port list of the body :
    FOREACH_ARRAY_ITEM (port_list, i, port) {
        if (!port) continue ;
        body_port_count++ ;
        // Try to get corresponding port from prototype
        proto_port = (proto_ports && proto_ports->Size() >= body_port_count) ? (VeriIdDef*)proto_ports->At(body_port_count - 1) : 0 ;
        if (!proto_port) continue ;

        // Compare their names :
        if (!Strings::compare(port->Name(), proto_port->Name())) {
            // VIPER #7350 : Produce warning for name mismatch in relaxed checking mode
            if (InRelaxedCheckingMode()) {
                proto_port->Warning("formal name of extern method %s does not match prototype", decl_id->Name()) ;
            } else {
                proto_port->Error("formal name of extern method %s does not match prototype", decl_id->Name()) ;
            }
        }
        // Compare their types
        VeriTypeInfo *port_type = port->CreateType(0) ;
        VeriTypeInfo *proto_port_type = proto_port->CreateType(0) ;
        if (port_type && !port_type->IsUnresolvedNameType() && !port_type->IsTypeParameterType() &&
                proto_port_type && !proto_port_type->IsUnresolvedNameType() &&
                !proto_port_type->IsTypeParameterType() && !proto_port_type->IsEquivalent(port_type, 0, 0, 0)) {
            proto_port->Error("formal type of extern method %s does not match prototype", decl_id->Name()) ;
        }
        delete port_type ;
        delete proto_port_type ;
    }
}

void VeriClockingDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // VIPER #3663 : Mark clocking identifier as default clocking
    if (GetQualifier(VERI_DEFAULT) && _id) _id->SetAsDefault() ;

    VeriExpression *save_sva_clock_expr = _sva_clock_expr ;
    _sva_clock_expr = _clocking_event ;

    // Viper 6237: Get the clocking decl from the declaration
    if (!_clocking_event && _id && _id->IsDefaultClocking()) {
        VeriModuleItem *clk_decl = _id->GetModuleItem() ;
        VeriExpression *clk_expr = clk_decl ? clk_decl->GetClockingEvent() : 0 ;
        _sva_clock_expr = clk_expr ? clk_expr : _sva_clock_expr ;
    }

    // Flip to local scope (if there)
    VeriScope *parent_scope = scope ; // Save for later processing
    if (_scope) scope = _scope ;

    // Resolve the clock expression :
    if (_clocking_event) _clocking_event->Resolve(scope,VERI_EVENT_CONTROL) ; // allow event expression

    // Resolve the clocking items.
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_clocking_items, i, item) {
        if (!item) continue ;
        // Viper 4841. Removed calling resolve on prop/seq decl from
        // clocking block. It is called from VeriAssert statement
        // Viper 6369: Commented out the next line: so we allow resolve to be called
        // for clock items of a clocking block. Passed a new environment
        // VERI_CLOCKING_BLOCK for it.
        //if (item->IsPropertyDecl() || item->IsSequenceDecl()) continue ;
        item->Resolve(scope, VERI_CLOCKING_BLOCK) ;
    }

    // VIPER 2620. Variables used within a clocking declaration must be defined
    // in some upper scope.  Check the clocking items here by resolving their
    // names in the upper scope (which is one level above the clocking decl).
    unsigned j ;
    FOREACH_ARRAY_ITEM(_clocking_items, i, item) {
        if (!item) continue ;
        Array *ids = item->GetIds() ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            VeriIdDef *clocking_sig_id = (parent_scope) ? parent_scope->Find(id->Name()) : 0 ;

            id->SetIsClockingDeclId() ; // Viper 6781

            VeriExpression *init_val = id->GetInitialValue() ;
            // Don't error out if the id has an initial value
            if (!clocking_sig_id && !init_val) id->Error("%s is not declared", id->Name()) ;
            // VIPER #3191 : Set data type back pointer in signal declarated within clocking
            // decl from its definition in upper scope.
            if (id->GetDataType()) continue ;
            if (clocking_sig_id && !init_val) {
                id->SetDataType(clocking_sig_id->GetDataType()) ;
            }
            if (init_val) {
                // Set data type from initial value specified identifier/expression
                // FIXME : This will not work if init_val is an expression other than name reference
                VeriIdDef *r_id = init_val->GetId() ;
                if (r_id) id->SetDataType(r_id->GetDataType()) ;
            }
        }
    }

    if (!(GetQualifier(VERI_DEFAULT) || (_id && _id->IsDefaultClocking()))) {
        _sva_clock_expr = save_sva_clock_expr ;
    }
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriConstraintDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the constraint blocks
    unsigned i ;
    VeriExpression *expr ;

    if (_scope) scope = _scope ;  // Added for VIPER: 4179

    FOREACH_ARRAY_ITEM(_constraint_blocks, i, expr) {
        if (expr) expr->Resolve(scope, VERI_CONSTRAINT_EXPR) ; // expecting a constraint expression..
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriBindDirective::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Check whether this bind is inside root scope
    VeriIdDef *container_id = scope ? scope->GetContainingModule(): 0 ;
    VeriModule *container = container_id ? container_id->GetModule(): 0 ;
    unsigned is_container_root = container ? container->IsRootModule(): 0 ;

    // Resolve the hier names (should pertain to a module name or instance path).
    // So id's in there should not have to be defined already
    // Call with environment UPPER_NAME_SCOPE
    unsigned i ;
    VeriName *hier_name, *prefix ;
    FOREACH_ARRAY_ITEM(_target_inst_list, i, hier_name) {
        if (hier_name) hier_name->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ;
        if (!is_container_root || !container) continue ;
        // VIPER #8352 : Bind directive is in root scope. So 'hier_name'
        // should start with top level module name. Extract the top level
        // module and add root scope to the using list of that module.
        prefix = hier_name ;
        while(prefix && prefix->GetPrefix()) {
            prefix = prefix->GetPrefix() ;
        }
        const char *prefix_name = prefix ? prefix->GetName(): 0 ;
        VeriModule *module = veri_file::GetModule(prefix_name, 1/*case_sensitive*/, veri_file::GetWorkLib()->GetName()) ;
        if (module) {
            VeriScope *module_scope = module->GetScope() ;
            if (module_scope) module_scope->Using(container->GetScope()) ;
        }
    }

    // Resolve the instantiation :
    // RD: cannot resolve the instantiation, since the actuals refer to
    // the scope of the hierarchical name...
    // Even if that is resolved, there are too many complications
    // if we resolve the instantiation right now in that scope.
    // if (_instantiation) _instantiation->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriOperatorBinding::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the return type
    if (_data_type) _data_type->Resolve(scope, VERI_TYPE_ENV) ;

    // Resolve function name
    if (_func_name) _func_name->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ;

    // Resolve formals of function prototype
    unsigned i ;
    VeriDataType *formal ;
    FOREACH_ARRAY_ITEM(_formals, i, formal) {
        if (formal) formal->Resolve(scope, VERI_TYPE_ENV) ;
    }

    // Do some checks regarding number of operands depending on operator type
    unsigned oper_type = (_oper_id) ? _oper_id->GetOperatorType() : 0 ; // Get operator type from operator identifier

    switch (oper_type) {
    case VERI_MUL:
    case VERI_DIV:
    case VERI_GT:
    case VERI_LT:
    case VERI_GEQ:
    case VERI_LEQ:
    case VERI_LOGEQ:
    case VERI_LOGNEQ:
    case VERI_POWER:
        if (_formals && (_formals->Size() != 2)) { // Function prototype for these operators should contain two arguments
            Error("%s expects %d arguments", (_oper_id) ? _oper_id->Name() : "operator", 2) ;
        }
        break ;
    case VERI_INC_OP:
    case VERI_DEC_OP:
        if (_formals && (_formals->Size() != 1)) { // Function prototype for these operators should contain one arguments
            // FIXME : in c++ postfix and prefix is resolved by adding an argument
            // to associated function, but nothing similar is said in 1800 LRM
            // So, suppress the following error checking
            //Error("%s expects %d arguments", _oper_id->Name(), 1) ;
        }
        break ;
    default : break ;
    }

    // VIPER #2733 : Produce warning about this non-supported feature
    Warning("operator overloading is not supported yet") ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriNetAlias::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Use 'VERI_PORT_CONNECTION' environment to resolve members of alias statement
    // It will mark used nets as used and assigned and will not check anything else

    // FIXME : A separate environment can allow us to do some specific checks (like only net allowed and such)
    if (_left) _left->Resolve(scope, VERI_PORT_CONNECTION) ; // Resolve left member
    // Viper 4829. LRM P1800 6.8 alias of signal to itself is illegal
    Set *leftids = 0 ;
    Set *rightids = 0 ;
    if (_left) {
        // Viper 4829. collect all the ids in left
        leftids = new Set() ;
        _left->CollectAllIds(leftids) ;
    }
    if (_members) rightids = new Set() ;
    // VIPER #3555 : LRM 6.8. do lhs<->rhs type check (must be same base net type, and same dimensions)
    VeriTypeInfo *left_type = (_left) ? _left->CheckType(0, NO_ENV) : 0 ;
    if (left_type && (left_type->IsTypeParameterType() || left_type->IsUnresolvedNameType())) {
        delete left_type ;
        left_type = 0 ;
    }
    // Get base type of left operand
    VeriTypeInfo *base_type = (left_type) ? left_type->BaseType() : 0 ;
    if (base_type && !base_type->IsNetType()) {
        // Left operand is not net type : error
        Error("aliasing can be done only on net types") ;
    }

    // All other members should be references of net
    unsigned i ;
    VeriName *member ;
    VeriTypeInfo *mem_type ;
    FOREACH_ARRAY_ITEM(_members, i, member) {
        if (!member) continue ;
        member->Resolve(scope, VERI_PORT_CONNECTION) ;
        // Viper 4829. collect all the ids in right
        member->CollectAllIds(rightids) ;
        // Determine type of each member
        mem_type = member->CheckType(0, NO_ENV) ;
        if (mem_type && (mem_type->IsTypeParameterType() || mem_type->IsUnresolvedNameType())) {
            // Type is not yet resovled, continue :
            delete mem_type ;
            continue ;
        }
        // Base type of member should be net :
        base_type = (mem_type) ? mem_type->BaseType() : 0 ;
        if (base_type && !base_type->IsNetType()) { // Not net type : error
            Error("aliasing can be done only on net types") ;
        }
        // type of rhs should be same as left
        // Viper 5496. Alias should not match number of packed dimentions
        // To avoid this in the IsEqual argument is_in_alias is set
        if (mem_type && left_type && !left_type->IsEqual(mem_type, 1/*is_in_alias*/)) {
            Error("nets connected in alias statement must be type compatible") ;
        }
        delete mem_type ;
    }
    delete left_type ;
    if (rightids && leftids) {
        SetIter si ;
        VeriIdDef *id ;
        FOREACH_SET_ITEM(leftids, si, &id) {
            if (id && rightids->Get(id)) Error("aliasing an individual signal %s to itself is not allowed", id->GetName()) ;
        }
    }
    delete rightids ; //JJ: move delete out of conditional
    delete leftids ; //JJ: move delete out of conditional

    // FIX ME : LRM 6.8. Gotta do lhs<->rhs type check (must be same base net type, and same dimensions)

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTimeUnit::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_literal) _literal->Resolve(scope, VERI_EXPRESSION) ; // Resolve time literal
    // VIPER #4489 : Produce error if timeunit/timeprecision contains 'step'
    char *image = (_literal) ? _literal->Image(): 0 ;
    if (image && strstr(image, "step")) {
        Error("step cannot be used in %s", PrintToken(_type)) ;
    }
    Strings::free(image) ;
    // VIPER #8304 : Resolve time precision also
    if (_time_precision) _time_precision->Resolve(scope, VERI_EXPRESSION) ; // Resolve time literal
    image = _time_precision ? _time_precision->Image(): 0 ;
    if (image && strstr(image, "step")) {
        Error("step cannot be used in %s", "timeprecision") ;
    }
    Strings::free(image) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriClockingSigDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve using the base type resolver :
    VeriDataDecl::Resolve(scope, VERI_UNDEF_ENV) ;
    if (_clk_dir) _clk_dir->Resolve(scope, VERI_DELAY_CONTROL) ; // Resolve clocking skew
    if (_o_clk_dir) _o_clk_dir->Resolve(scope, VERI_DELAY_CONTROL) ; // Resolve output clocking skew

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriCovergroup::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_scope) scope = _scope ;
    unsigned i ;
    VeriExpression *port_expr ; // Resolve port list

    unsigned previous_direction = VERI_INPUT ;
    // VIPER #5949: Set previous_kind and default_kind to VERI_LOGIC:
    unsigned default_kind = VERI_LOGIC ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = VERI_LOGIC ;
    unsigned previous_type = default_type ;
    unsigned has_ansi_port_list = 0 ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (!port_expr) continue ;
        if (port_expr->IsAnsiPortDecl()) has_ansi_port_list = 1 ;

        if (has_ansi_port_list) {
            // VIPER #5180: Propagate previous' type/direction info
            port_expr->ResolveAnsiPortDecl(scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
        } else {
            port_expr->Resolve(scope, VERI_PORT_EXPRESSION) ;
        }
    }
    if (_event) _event->Resolve(scope, VERI_EVENT_CONTROL) ; // Resolve coverage event

    if (_sample_func) _sample_func->Resolve(scope, VERI_UNDEF_ENV) ; // Resolve sample function

    VeriModuleItem *item ; // Resolve coverage spec or options
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->Resolve(scope, VERI_UNDEF_ENV) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriCoverageOption::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    veri_environment value_env = VERI_EXPRESSION ;
    if (_option) {
        _option->Resolve(scope, VERI_UNDEF_ENV) ; // _option is option.option_name where 'option' is not a declared identifier

        // VIPER #6915: IEEE 1800 (2009), section 19.3: Value should be constant if _option is 'type_option':
        VeriName *prefix = _option ;
        while (prefix && prefix->GetPrefix()) prefix = prefix->GetPrefix() ;
        VeriIdDef *option_id = (prefix) ? prefix->GetId() : 0 ;
        if (option_id && option_id->IsCovergroupOption() && Strings::compare(option_id->Name(), "type_option")) {
            // This is a 'type_option' covergroup option id, its RHS should be a constant expression:
            value_env = VERI_CONSTANT_EXPRESSION ;
        }
    }
    if (_value) _value->Resolve(scope, value_env) ; // Resolve option value

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriCoverageSpec::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve coverpoint list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_point_list, i, expr) {
        if (!expr) continue ;
        expr->Resolve(scope, (_type == VERI_COVERPOINT) ? VERI_COVERPOINT_ENV: VERI_UNDEF_ENV) ;
        if (_type == VERI_COVERPOINT) {
            // VIPER #4844 : Produce error if real expression is used as coverpoint
            // expression, integral expression is needed here.
            VeriTypeInfo *expr_type = expr->CheckType(0, NO_ENV) ;
            if (expr_type && expr_type->IsRealType()) {
                expr->Error("illegal real type expression for coverpoint expression") ;
            }
            delete expr_type ;
        }
    }

    if (_iff_expr) _iff_expr->Resolve(scope, (_type == VERI_COVERPOINT) ? VERI_COVERPOINT_ENV: VERI_EXPRESSION) ; // Resolve iff expression

    if (_scope) scope = _scope ;

    // Resolve bin declarations
    VeriModuleItem *bin ;
    FOREACH_ARRAY_ITEM(_bins, i, bin) {
        if (bin) bin->Resolve(scope, (_type == VERI_COVERPOINT) ? VERI_COVERPOINT_ENV: VERI_UNDEF_ENV) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriBinDecl::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_id) _id->Resolve(scope, environment) ; // Resolve bin identifier

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriImportDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // All scope nodes are already resolved and scopes are linked in parser.

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriLetDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // VIPER #7720 : Handle recursive let decl using 'IsProcessing'
    if (_id) {
        if (_id->IsProcessing()) return ;
        _id->SetIsProcessing() ;
    }
    // Flip to local scope :
    if (_scope) scope = _scope ;

    // Resolve all sub-nodes :

    // Set base 'type' field on ansi ports :
    // LRM (SV 1800-2009) 11.13 :
    // Ports inherit direction and type from previous ports.
    // default port direction is 'input'.
    // default type is 'logic'.
    unsigned i ;
    VeriExpression *port_decl ;
    // For let construct, all ports are input (checked in constructor already)
    unsigned previous_direction = VERI_INPUT ;
    // For checker, default type should be 'untyped' (LRM . I don't have that, so give type 'logic' ?
    // VIPER #5949: Set previous_kind and default_kind to VERI_LOGIC:
    unsigned default_kind = VERI_LOGIC ;
    unsigned previous_kind = default_kind ;
    unsigned default_type = VERI_LOGIC ;
    unsigned previous_type = default_type ;
    // Now do the ansi port list itself :
    FOREACH_ARRAY_ITEM(_ansi_io_list, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->ResolveAnsiPortDecl(_scope, &previous_direction, &previous_kind, default_kind, &previous_type, default_type) ;
    }
    if (_expr) _expr->Resolve(scope, VERI_LET_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
    if (_id) _id->ClearIsProcessing() ;
}
void VeriDefaultDisableIff::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the disable condition
    if (_expr_or_dist) _expr_or_dist->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriExportDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // All scope nodes are already resolved and scopes are linked in parser.

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void
VeriPulseControl::Resolve(VeriScope *scope, veri_environment environment)
{
    unsigned i ;
    VeriExpression *expr ;
    // Resolve the output paths
    FOREACH_ARRAY_ITEM(_path_list, i, expr) {
        if (expr) expr->Resolve(scope, environment) ;
    }
}

//////////////////////////////////////////////////////////////////////
// Instantiation management
//////////////////////////////////////////////////////////////////////

const char *VeriModuleInstantiation::GetModuleName() const
{
    return (_module_name) ? _module_name->GetName() : 0 ;
}

// Get the module that is being instantiated here
VeriModule *VeriModuleInstantiation::GetInstantiatedModule() const
{
    VeriModule *module = 0 ;

    // VIPER #5080: Look into user library even before looking into anything:
    // VIPER #3048: Look into user library before work library if it was from `uselib:
    if (_library && _library->IsTickUseLib()) module = _library->GetModule(GetModuleName(), 1) ;
    if (module) return module ;

    // VIPER #5080: With this VIPER looking for direct instantaiation becomes second:
    // Look for a direct (nested) instantiation first :
    VeriIdDef *instantiated_mod = _module_name ? _module_name->GetId() : 0 ;
    module = instantiated_mod ? instantiated_mod->GetModule() : 0 ;
    if (module) return module ;

    // VIPER #5080: Code moved above.
    // VIPER #3048: Look into user library before work library if it was from `uselib:
    //if (_library && _library->IsTickUseLib()) module = _library->GetModule(GetModuleName(), 1) ;
    //if (module) return module ;

    // VIPER #2762 : Try to find instantiated module in ordered libraries specified
    // by -L options
    module = veri_file::GetModuleFromLOptions(GetModuleName(), 1) ;
    if (module) return module ;

#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    // Look into the library of the parent module in which
    // scope this instantiation statement is present
    VeriInstId *inst_id = 0 ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(&_insts, i, inst_id) {
        if (!inst_id) continue ;
        break ;
    }

    VeriScope *parent_scope = inst_id ? inst_id->GetOwningScope() : 0 ;
    VeriIdDef *parent_owner = parent_scope ? parent_scope->GetOwner() : 0 ;
    VeriModule *parent_module = parent_owner ? parent_owner->GetModule() : 0 ;

    if (parent_module) {
        VeriLibrary *lib = parent_module->GetLibrary() ;
        if (lib) module = lib->GetModule(GetModuleName(), 1);
    }

    if (module) return module ;
#endif

    // Then look in the work library
    // As a side effect of VIPER #2726, in multiple library environment, the next line
    // will work properly if the veri_file::_work_library is set to the library of the
    // module in which this module instantiation resides. This can be set by calling
    // the routine veri_file::SetWorkLib(module_lib) before calling this routine.
    VeriLibrary *lib = veri_file::GetWorkLib();
    if (lib) module = lib->GetModule(GetModuleName(), 1);
    if (module) {
        // FIX ME : Should not set any internal pointers in a Get() routine.
        // _library = lib ;
        return module ;
    }

    // If not in the work library, look in the uselib library :
    if (_library && !_library->IsTickUseLib()) module = _library->GetModule(GetModuleName(), 1);

    return module ;
}

// These should be used post elaboration. If these 2 APIs return 0
// then it means no module is bounded.
// Viper 6770 : Add APIs to get bounded units/modules.
VeriModule *VeriModuleInstantiation::GetBoundedModule() const
{
    // This is a post elaboration routine which returns the
    // bounded Verilog module if at all it is present.
    VeriIdDef * inst_mod = _module_name ? _module_name->GetId() : 0 ;
    VeriModule* module = inst_mod ? inst_mod->GetModule() : 0 ;
    return module ;
}

VhdlPrimaryUnit *VeriModuleInstantiation::GetBoundedUnit(char **arch_name /*=0*/) const
{
    // This is a post elaboration routine which returns the
    // bounded Vhdl unit if at all it is present.

    // The arch_name is populated iff it is present as (arch_name) in the _module_name
    VeriExpression *lib_name = GetAttribute(" vhdl_unit_lib_name") ;
    if (!lib_name) return 0 ; // VIPER #7055 : Null check

    char *lib_name_str = Strings::save(lib_name->Image()) ;
    char *unquote_str = lib_name_str ;
    if (lib_name_str && *lib_name_str == '"') {
        unquote_str++ ;
        *(lib_name_str + Strings::len(lib_name_str)-1) = 0 ;
    }

    VhdlLibrary* vhdl_lib = unquote_str ? vhdl_file::GetLibrary(unquote_str) : 0 ;
    const char *unit_name = _module_name ? _module_name->GetName() : 0 ;
    VhdlPrimaryUnit *unit = vhdl_lib ? VeriNode::GetVhdlUnitForVerilogInstance(unit_name, unquote_str, 1, arch_name) : 0 ;
    Strings::free(lib_name_str) ;
    return unit ;
}

// Set the name of the library in which the module can be found
void VeriModuleInstantiation::SetLibrary(VeriLibrary * lib)
{
    _library = lib ;
}

// Flag the library with the name of a module that is instantiated.
void VeriModuleInstantiation::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    if (_library && (_library->IsTmpLibrary() || _library->IsEmptyUseLib())) _library = lib ;

    // VIPER #5919: Do not add module names of nested modules:
    const char *instantiated_module_name = (_module_name) ? _module_name->GetName() : 0 ;
    while (module_scope) {
        VeriIdDef *mod = module_scope->FindLocal(instantiated_module_name) ;
        if (mod && mod->IsModule()) return ;   // Found a nested local module
        module_scope = module_scope->Upper() ; // Find in upper module scope
    }

    if (_library) {
        _library->AddModuleName(instantiated_module_name) ;
    } else if (lib) {
        lib->AddModuleName(instantiated_module_name) ;
    }

    if (RuntimeFlags::GetVar("veri_move_yv_modules_into_work_library")) {
        // VIPER #6706: Reset this _library under this compile flag
        // if it is not a `tickuselib since we will be moving all
        // the modules into work library later. This _library should
        // not be required anymore and may create problem if left here:
        if (_library && !_library->IsTickUseLib()) _library = 0 ;
    }

    return ;
}

// Traverse the module items inside generate to find module instantiation
void VeriGenerateConstruct::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    unsigned i ;
    VeriModuleItem *mod_item ;
    FOREACH_ARRAY_ITEM(_items, i, mod_item) {
        if (mod_item) mod_item->ProcessModuleInstInLibrary(lib, module_scope) ;
    }
}

void VeriGenerateConditional::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    if (_then_item) _then_item->ProcessModuleInstInLibrary(lib, module_scope) ;
    if (_else_item) _else_item->ProcessModuleInstInLibrary(lib, module_scope) ;
}

void VeriGenerateCase::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    unsigned i ;
    VeriGenerateCaseItem *case_item ;
    FOREACH_ARRAY_ITEM(_case_items, i, case_item) {
        if (case_item && case_item->GetItem()) case_item->GetItem()->ProcessModuleInstInLibrary(lib, module_scope) ;
    }
}

void VeriGenerateFor::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    unsigned i ;
    VeriModuleItem *mod_item ;
    FOREACH_ARRAY_ITEM(_items, i, mod_item) {
        if (mod_item) mod_item->ProcessModuleInstInLibrary(lib, module_scope) ;
    }
}

void VeriGenerateBlock::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    unsigned i ;
    VeriModuleItem *mod_item ;
    FOREACH_ARRAY_ITEM(_items, i, mod_item) {
        if (mod_item) mod_item->ProcessModuleInstInLibrary(lib, module_scope) ;
    }
}

// VIPER #6560: Process module instantiations in bind directives:
void VeriBindDirective::ProcessModuleInstInLibrary(VeriLibrary *lib, const VeriScope *module_scope)
{
    if (_instantiation) _instantiation->ProcessModuleInstInLibrary(lib, module_scope) ;
}

//////////////////////////////////////////////////////////////////////
// Comment preservation
//////////////////////////////////////////////////////////////////////

Array *
VeriModuleItem::ProcessPostComments(Array *comments)
{
    if (!comments) return 0 ;

    unsigned i ;
    VeriCommentNode *node ;
    FOREACH_ARRAY_ITEM(comments, i, node) {
        if (!node) continue ;

        // Take out ONLY the comments which have the same  or less starting line number (and same fileid)
        // as this statement's ending line number. We assume these belong to 'this' statement.
        // Leave other comments in the array (and return that)
        linefile_type comment_line_file = node->StartingLinefile() ; // comment line/file info
        unsigned comment_line_no = LineFile::GetLineNo(comment_line_file) ; // comment line number
        unsigned comment_file_id = LineFile::GetFileId(comment_line_file) ; // comment file id

        linefile_type stmt_line_file = EndingLinefile() ; // this (moduleitem) line/file
        unsigned stmt_line_no = LineFile::GetLineNo(stmt_line_file) ; // this (moduleitem) line number
        unsigned stmt_file_id = LineFile::GetFileId(stmt_line_file) ; // comment file id

        // Check if comment appears at most at the last line of the statement
        // Check if the file names are same or not,
        if ((comment_line_no <= stmt_line_no) && (comment_file_id == stmt_file_id)) {
            // VIPER #7560: If the comment is within my line number, try to attach it
            // to the last statement/module item inside me (line begin ... stmts ... end).
            // Note that we could not attach comments that are on the next line to my line
            // since that should get attached the next item. But for the last such item
            // we are trying to attach the comment to it here.
            if ((comment_line_no < stmt_line_no) && AttachCommentToLastChild(node)) {
                comments->Insert(i, 0) ; // set null entry in the array
                continue ; // Done
            }

            // Consider this a comment of the statement.
            Array *comment_arr = new Array(1) ;
            comment_arr->InsertLast(node) ;
            AddComments(comment_arr) ;
            comments->Insert(i, 0) ; // set null entry in the array
        }
    }
    return comments ;
}

//////////////////////////////////////////////////////////////////////
// Qualifiers
//////////////////////////////////////////////////////////////////////

void
VeriModuleItem::SetQualifier(unsigned token)
{
    if (!token) return ; // Don't set anything if token is not set.
    _qualifiers = _qualifiers | (1 << IndexOfQualifier(token)) ;
}

unsigned
VeriModuleItem::GetQualifier(unsigned token) const
{
    if (!token) return 0 ;
    return (_qualifiers >> IndexOfQualifier(token)) & 1 ;
}

/*static*/ unsigned
VeriModuleItem::IndexOfQualifier(unsigned token)
{
    switch (token) {
    // lifetime on functions etc
    case VERI_AUTOMATIC : return 1 ;
    // on a net declaration :
    case VERI_VECTORED : return 2 ;
    case VERI_SCALARED : return 3 ;
    // SV Qualifiers :
    // static lifetime :
    case VERI_STATIC : return 4 ;
    // on prototype decls
    case VERI_EXTERN : return 5 ;
    case VERI_FORKJOIN : return 6 ;
    // class qualifiers :
    case VERI_VIRTUAL : return 7 ;
    case VERI_PROTECTED : return 8 ;
    case VERI_LOCAL : return 9 ;
    case VERI_RAND : return 10 ;
    case VERI_RANDC : return 11 ;
    // on a data decl :
    case VERI_CONST : return 12 ;
    // on modport_tf_port :
    case VERI_IMPORT : return 13 ;
    case VERI_EXPORT : return 14 ;
    // on clocking decl :
    case VERI_DEFAULT : return 15 ;
    // special always constructs :
    case VERI_ALWAYS_COMB : return 16 ;
    case VERI_ALWAYS_LATCH : return 17 ;
    case VERI_ALWAYS_FF : return 18 ;
    case VERI_FINAL : return 19 ;
    // case / if statements :
    case VERI_UNIQUE : return 20 ;
    case VERI_PRIORITY : return 21 ;
    // DPI import/export :
    case VERI_CONTEXT : return 22 ;
    case VERI_PURE : return 23 ;
    // on a data decl/ansi port decl
    case VERI_VAR : return 24 ;
    // void cast (on task enable/system task enable).
    case VERI_VOID : return 26 ;
    case VERI_TICK_PROTECTED : return 25 ;
    default : break ;
    }
    return 0 ; // qualifier not known.
}

//////////////////////////////////////////////////////////////////////
// Label Handling
//////////////////////////////////////////////////////////////////////

void
VeriModuleItem::ClosingLabel(char *label_name, VeriIdDef *last_label) const
{
    if (!label_name) return ;

    VeriIdDef *id = GetId() ;
    // VIPER #6966 : For sequence-block, parallel-block and generate block
    // statement label is equivalent to block name (1800-2009: 9.3.5)
    if (!id && (IsSeqBlock() || IsParBlock() || IsGenerateBlock())) id = last_label ;

    // VIPER #4211 & #4212: Produce an error if there is no starting label or the two do not match:
    if (!id || !Strings::compare(id->Name(),label_name)) {
        Error("mismatch in closing label %s; expected %s", label_name, (id) ? id->Name() : "<unnamed>") ;
    }

    Strings::free(label_name) ; // Clean up
}

//////////////////////////////////////////////////////////////////////
// Collect instantiated module names
//////////////////////////////////////////////////////////////////////
// In verilog 95/2001 all uninstantiated modules are top levels. The following virtual routine
// is used to collect all instantiated module names, so that we can find all uninstantiated modules
// from the library. Module instantiations can reside inside generate, case-generate, if-generate,
// generate-block, so we have to recurse through  those module items to find instantiated modules.
// Routine VeriModuleItem::GetInstantiatedModule returns VeriModule * processing -y / -v options but it
// does not work for instantiations inside generate. Moreover only the names of instantiated
// modules are sufficient to find top level modules and hence we can skip the redundant -y/-v processing.
void
VeriModuleInstantiation::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    // If there are instances of the module then insert the name in the Set
    const char *name = (_insts.Size()) ? GetModuleName() : 0 ;
    if (name) (void) mod_names.Insert(name) ;

    // VIPER #4933: Traverse this user library for all its modules for instantiated modules:
    if (_library) _library->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
}
void
VeriGenerateCase::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    VeriGenerateCaseItem *ci ;
    unsigned i ;
    // Checks the different case generate items
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        // Recurse through this case generate items' statement
        VeriModuleItem *mod_item = (ci) ? ci->GetItem() : 0 ;
        if (mod_item) mod_item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    }
}
void
VeriGenerateFor::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    unsigned i ;
    VeriModuleItem *item ;
    // Call AccumulateInstantiatedModuleNames for every module item
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    }
}
void
VeriGenerateConditional::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    // For the module items within the if-generate block call the AccumulateInstantiatedModuleNames routine
    if (_then_item) _then_item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    if (_else_item) _else_item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
}
void
VeriGenerateBlock::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    unsigned i ;
    VeriModuleItem *item ;
    // Call AccumulateInstantiatedModuleNames for every module item
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    }
}
void
VeriGenerateConstruct::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
    }
}
void
VeriGenerateConstruct::AccumulateParameters(Array &parameters, unsigned has_ansi_parameter_list, const VeriIdDef *mod_id) const
{
    // VIPER #7603: Simulators allow overwriting parameters declared
    // within generate ... endgenerate from instances, so, accumulate them:
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->AccumulateParameters(parameters, has_ansi_parameter_list, mod_id) ;
    }
}

void
VeriBindDirective::AccumulateInstantiatedModuleNames(Set &mod_names, Set &done_libs, Set &def_mods)
{
    if (_instantiation) _instantiation->AccumulateInstantiatedModuleNames(mod_names, done_libs, def_mods) ;
}

void
VeriBindDirective::RemoveFromHierModule()
{
    // Pick up the module identifier :
    VeriModule *hier_module = 0 ;
    const char *hier_module_name = 0 ;
    if (_target_mod_name) {
        hier_module_name = _target_mod_name ;
    } else { // When the target module name is null the size of _target_inst_list must be 1
        VeriName *target_mod = _target_inst_list ? (VeriName*)_target_inst_list->GetFirst() : 0 ;
        hier_module_name = (target_mod && !target_mod->IsHierName()) ? target_mod->GetName() : 0 ; // Only consider simple module name
    }
    // First search 'hier_module_name' in work library
    // As we are not suporting bind instance addition in multiple modules, so
    // give priority to work library over user defined libraries.
    VeriLibrary *work_lib = veri_file::GetWorkLib() ;
    hier_module = work_lib ? work_lib->GetModule(hier_module_name, 1): 0 ;

    if (!hier_module) {
        // Module not in work library, search in the user library. That one should be in the _instantiation.
        // Can't get a hold of GetLibrary, since _instantiation is a VeriModuleItem*. Change argument of BindDirective ?
        // Instead, iterate over all user libraries to search for module 'hier_module_name'
        VeriLibrary *lib ;
        MapIter mi ;
        FOREACH_VERILOG_LIBRARY(mi, lib) {
            if (!lib || !lib->IsUserLibrary()) continue ;
            hier_module = lib->GetModule(hier_module_name, 1) ;
            if (hier_module) break ; // Take the first module, ignore all others for time being
        }
    }
    Array *bind_inst_array = 0 ;
    if (hier_module) bind_inst_array = hier_module->GetBindInstances() ;
    if (!hier_module) { //Search in VhdlLib
        VhdlLibrary *vhdl_lib ;
        MapIter mi ;
        FOREACH_VHDL_LIBRARY(mi, vhdl_lib) {
            if (!vhdl_lib) continue ;
            VhdlPrimaryUnit *hier_unit = vhdl_lib->GetPrimUnit(hier_module_name, 0) ;
            if (hier_unit) {
                bind_inst_array = hier_unit->GetBindInstances() ;
            }
        }
    }
    if (bind_inst_array) {
         VeriModuleItem *exist_bind_instance ;
         unsigned j ;
         FOREACH_ARRAY_ITEM(bind_inst_array, j, exist_bind_instance) {
             if (exist_bind_instance == _instantiation) bind_inst_array->Insert(j,0) ;
         }
    }
}
VeriExpression *
VeriPropertyDecl::GetActual(VeriIdDef *formal)
{
    return (_formal_to_actual? (VeriExpression*)(_formal_to_actual->GetValue(formal)): 0) ;
}

VeriModuleItem *
VeriClass::GetDataDecl(VeriIdDef *vid, unsigned qualifier) const
{
    if (!vid) return 0 ;
    VeriModuleItem *item ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (!item) continue ;
        switch (qualifier) {
        case VERI_CONSTRAINT : if (!item->IsConstraintDecl()) continue ; break ;
        case VERI_RANDC :
        case VERI_RAND : if (!item->GetQualifier(VERI_RAND) && !item->GetQualifier(VERI_RANDC)) continue ; break ; // Viper 4834
        default : break ;
        }
        VeriIdDef *item_id = item->GetId() ;

        if (item_id && item_id == vid) return item ;

        if (!item_id && item->IsDataDecl()){
            Array *ids = item->GetIds() ;
            unsigned j ;
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (id == vid) return item ;
            }
        }
    }
    if (_base_class_name) {
        VeriIdDef *base_class = _base_class_name->GetId() ;
        VeriModuleItem *base_item = base_class ? base_class->GetModuleItem() : 0 ;
        if (base_item) return base_item->GetDataDecl(vid, qualifier) ;
    }
    return 0 ;
}

//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
// VIPER #5727 : Unnamed generate block handling :
void VeriModuleItem::ProcessUnnamedGenBlk(VeriScope * /*scope*/, unsigned /*gen_blk_num*/, int /*branch_num*/, Set * /*block_names*/, Set * /*ignore_scopes*/)
{
    // Do nothing
}
void VeriGenerateConditional::ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_num, Set *block_names, Set * /*ignore_s*/)
{
    // If scope contains no id or only block id, it should be empty scope and
    // if block id exists, that should be moved in enclosing non-empty scope
    if (_then_scope && _then_scope->CanBeConsideredEmpty()) {
        _then_scope->SetAsEmptyScope() ;
    }
    Set *created_set = 0 ;
    if (!block_names && _else_item) {
        block_names = new Set(STRING_HASH) ;
        created_set = block_names ;
    }
    Set ignore_scopes(POINTER_HASH) ;
    (void) ignore_scopes.Insert(_then_scope) ;
    if (_else_scope) (void) ignore_scopes.Insert(_else_scope) ;

    // If _then_item of if-generate is not a generate block/conditional generate,
    // we should create a named generate block for then branch.
    // If _then_item is generate block/conditional generate, we should not
    // create named block. We need to pass the coming numbers to process that item
    if (_then_item && (_then_item->IsGenerateBlock() || _then_item->IsGenerateConditional())) {
        _then_item->ProcessUnnamedGenBlk(_then_scope, gen_blk_num, branch_num, block_names, &ignore_scopes) ;
    } else {
        VeriIdDef *block_id = CreateUnnamedBlockId(scope, gen_blk_num, branch_num, 0, _then_item, &ignore_scopes) ;
        if (block_id && _then_scope) {
            _then_scope->SetOwner(block_id) ;
            block_id->SetLocalScope(_then_scope) ;
            _then_scope->ResetEmptyScope() ;
            block_id->SetModuleItem(_then_item) ;
            //if (block_names && !block_names->Size()) (void) block_names->Insert(block_id->Name()) ;
            if (block_names) (void) block_names->Insert(block_id->Name()) ;
        }
        // Then branch creates a new scope, so its item should be processed with a
        // fresh block number 1 and no branch number :
        if (_then_item) _then_item->ProcessUnnamedGenBlk(_then_scope, 1, -1, 0, 0) ;
    }
    if (!_else_item) return ; // No else part

    // If scope contains no id or only block id, it should be empty scope and
    // if block id exists, that should be moved in enclosing non-empty scope
    if (_else_scope && _else_scope->CanBeConsideredEmpty()) {
        _else_scope->SetAsEmptyScope() ;
    }
    // If _else_item is not a generate block/conditional generate, we should create
    // a named generate block for else branch.
    // If _else_item is generate block/conditional generate, we should not create
    // named block. We need to pass the coming gen_blk_num and branch_num should be
    // increased by 1
    if (_else_item->IsGenerateBlock() || _else_item->IsGenerateConditional()) {
        _else_item->ProcessUnnamedGenBlk(_else_scope, gen_blk_num, branch_num + 1, block_names, &ignore_scopes) ;
    } else {
        VeriIdDef *block_id = CreateUnnamedBlockId(scope, gen_blk_num, branch_num+ 1, 0, _else_item, &ignore_scopes) ;
        if (block_id && _else_scope) {
            _else_scope->SetOwner(block_id) ;
            block_id->SetLocalScope(_else_scope) ;
            _else_scope->ResetEmptyScope() ;
            block_id->SetModuleItem(_else_item) ;
            if (block_names && !block_names->Size()) (void) block_names->Insert(block_id->Name()) ;
        }
        // Else branch creates a new scope, so its item should be processed with a
        // fresh block number 1 and no branch number :
        _else_item->ProcessUnnamedGenBlk(_else_scope,  1, -1, 0, 0) ;
    }
    delete created_set ;
}
void VeriGenerateCase::ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_num, Set *block_names, Set * /*ignore_s*/)
{
    Set *created_set = 0 ;
    if (!block_names) {
        block_names = new Set(STRING_HASH) ;
        created_set = block_names ;
    }
    Set ignore_scopes(POINTER_HASH) ;
    // Case-generate is like if-generate, but it can have multiple branches.
    unsigned i ;
    VeriGenerateCaseItem *case_item ;
    VeriScope *item_scope ;
    int case_branch_num = 0 ;
    FOREACH_ARRAY_ITEM(_case_items, i, case_item) {
        if (!case_item) continue ;
        item_scope = case_item->GetScope() ;
        (void) ignore_scopes.Insert(item_scope) ;
    }
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_case_items, i, case_item) {
        if (!case_item) continue ;
        item = case_item->GetItem() ;
        item_scope = case_item->GetScope() ;
        //if (!item) continue ;
        // If scope contains no id or only block id, it should be empty scope and
        // if block id exists, that should be moved in enclosing non-empty scope
        if (item_scope && item_scope->CanBeConsideredEmpty()) {
            item_scope->SetAsEmptyScope() ;
        }
        // If case_item is not a generate block/conditional generate, we should create
        // a named generate block for case item.
        // If case_item is generate block/conditional generate, we should not create
        // named block. We need to pass the coming gen_blk_num and branch_num for the
        // first case item. For remaining items branch_num should be increased by 1
        if (item && (item->IsGenerateBlock() || item->IsGenerateConditional())) {
            item->ProcessUnnamedGenBlk(item_scope, gen_blk_num, branch_num + case_branch_num, block_names, &ignore_scopes) ;
        } else {
            VeriIdDef *block_id = CreateUnnamedBlockId(scope, gen_blk_num, branch_num + case_branch_num, 0, item, &ignore_scopes) ;
            if (block_id && item_scope) {
                item_scope->SetOwner(block_id) ;
                block_id->SetLocalScope(item_scope) ;
                item_scope->ResetEmptyScope() ;
                block_id->SetModuleItem(item) ;
                if (block_names && !block_names->Size()) (void) block_names->Insert(block_id->Name()) ;
            }
            // This case item creates a new scope, so its item should be processed with a
            // fresh block number 1 and no branch number :
            if (item) item->ProcessUnnamedGenBlk(item_scope, 1, -1, 0, 0) ;
        }
        case_branch_num++ ;
    }
    delete created_set ;
}
void VeriGenerateFor::ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_num, Set * /*block_names*/, Set *  /*ignore_scopes*/)
{
    if (!_block_id) { // It is unnamed genearte block
        // A block identifier will be created and declared
        _block_id = CreateUnnamedBlockId(scope, gen_blk_num, branch_num, 0, this, 0) ;
        if (_block_id && _scope) {
            _scope->SetOwner(_block_id) ;
            _block_id->SetLocalScope(_scope) ;
            _block_id->SetModuleItem(this) ;
        }
    }

    // It is a scoped object, so every item of it will get fesh number starting from 1
    unsigned i ;
    VeriModuleItem *item ;
    unsigned gen_blk_num1 = 1 ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (!item) continue ;
        if (item->IsGenerateBlock() || item->IsGenerateFor() || item->IsGenerateConditional()) {
            item->ProcessUnnamedGenBlk(_scope, gen_blk_num1, -1 /* no branch here*/, 0, 0) ;
            gen_blk_num1++ ;
        }
    }
}
void VeriGenerateBlock::ProcessUnnamedGenBlk(VeriScope *scope, unsigned gen_blk_num, int branch_num, Set *block_names, Set *ignore_scopes)
{
    if (_block_id) {
        ProcessDesignSpecifiedBlockId(scope, branch_num, block_names) ;
        //if (_block_id && block_names && !block_names->Size()) (void) block_names->Insert(_block_id->Name()) ;
        // VIPER #6163 : If block_names present, insert block name, so that it can be checked
        // with other branch ids
        if (_block_id && block_names) (void) block_names->Insert(_block_id->Name()) ;
    } else { // It is unnamed genearte block
        // A block identifier will be created and declared
        _block_id = CreateUnnamedBlockId(scope, gen_blk_num, branch_num, 0, this, ignore_scopes) ;
        if (_block_id && _scope) {
            _scope->SetOwner(_block_id) ;
            _block_id->SetLocalScope(_scope) ;
            _block_id->SetModuleItem(this) ;
        }
        if (_block_id && block_names) (void) block_names->Insert(_block_id->Name()) ;
    }
    // It is a scoped object, so every item of it will get fesh number starting from 1
    unsigned i ;
    VeriModuleItem *item ;
    unsigned gen_blk_num1 = 1 ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (!item) continue ;
        if (item->IsGenerateBlock() || item->IsGenerateFor() || item->IsGenerateConditional()) {
            item->ProcessUnnamedGenBlk(_scope, gen_blk_num1, -1 /* no branch here*/, 0, 0) ;
            gen_blk_num1++ ;
        }
    }
}
// Create block identifier for unnamed generate block using argument specified generate block number
// and branch number. Also declare that identifier in argument specified enclosing_scope.
VeriIdDef *VeriModuleItem::CreateUnnamedBlockId(VeriScope *enclosing_scope, unsigned gen_blk_num, int branch_num, const VeriIdDef *block_id, const VeriTreeNode *from, Set *ignore_scopes) const
{
    if (!enclosing_scope) return 0 ; // Cannot do anything without enclosing scope
    // First find the nearest non-empty enclosing scope to check name conflict
    VeriScope *non_empty_scope = enclosing_scope ;
    while (non_empty_scope) {
        if (non_empty_scope->IsEmptyScope()) {
            non_empty_scope = non_empty_scope->Upper() ;
        } else {
            break ;
        }
    }
    if (!non_empty_scope) return 0 ; // Something bad, cannot find non-empty enclosing scope

    // Create the block name using block number and branch number
    char block_num[20] ;
    sprintf(block_num, "%d", gen_blk_num) ;
    char *orig_name = 0 ;
    char *block_name = (block_id) ? Strings::save(block_id->Name()): Strings::save("genblk", block_num) ;

    // Check for name conflict :
    // If identifier of same name exists in nearest non-empty enclosing scope,
    // Leading zeros are added in front of the number to resolve conflict
    // VIPER #6186 : Before Applying branch number, check for name conflict so that id like genblk01_1 can be produced
    VeriIdDef *exist = non_empty_scope->FindLocal(block_name) ;
    if (exist && exist->IsUnnamedBlock() && (branch_num >= 0)) exist = 0 ; // Ignore unnamed block ids for else branch only
    // VIPER #6903 : Check in other child scopes of non_empty_scope to find
    // if id of same name is already declared. But in this process ignore the
    // construct you are processing, i.e. if you are now processing then-branch
    // of an if-else, ignore that if-else branches.
    if (!exist) { // && (branch_num < 0)) {
        Set *child_scopes = non_empty_scope->GetChildrenScopes() ;
        SetIter si ;
        VeriScope *child_scope ;
        VeriScope *c ;
        VeriIdDef *owner ; const char *owner_name ;
        FOREACH_SET_ITEM(child_scopes, si, &child_scope) {
            if (!child_scope) continue ;
            SetIter sii ;
            VeriScope *u_scope ;
            unsigned ignore_this_child = 0 ;
            FOREACH_SET_ITEM(ignore_scopes, sii, &u_scope) {
                c = u_scope ;
                while (c) {
                    if (c == child_scope) {
                        ignore_this_child = 1 ;
                        break ;
                    }
                    c = c->Upper() ;
                }
                if (ignore_this_child) break ;
            }
            if (ignore_this_child) continue ;
            owner = child_scope->GetOwner() ;
            owner_name = (owner) ? owner->GetName() : 0 ;
            if (owner && owner_name && (*owner_name != ' ')) continue ;
            exist = child_scope->FindLocal(block_name) ;
            if (!exist) exist = child_scope->FindInUnnamedChildScopes(block_name) ;
            if (exist) break ;
        }
    }
    unsigned leading_zero_count = 0 ;
    while (exist && !block_id) {
        char *tmp = block_name ;
        // Create a new name :
        leading_zero_count++ ;
        char zero[20] ;
        unsigned i ;
        for (i = 0; i < leading_zero_count; i++) zero[i] = '0' ;
        zero[i] = '\0' ;
        block_name = Strings::save("genblk", zero, block_num) ;
        exist = non_empty_scope->FindLocal(block_name) ;
        if (exist && exist->IsUnnamedBlock()  && (branch_num >= 0)) exist = 0 ; // Ignore unnamed block ids
        Strings::free(tmp) ;
    }
    if (branch_num < 0) { // branch_num negative means this block is not within a else branch
        block_name = block_name ; // Only for documentation purpose
    } else { // this block is within a else branch,name will be like genblk1_0
        char branch_num_str[20] ;
        sprintf(branch_num_str, "_%d", branch_num) ;
        char *tmp1 = block_name ;
        if (block_id) {
            block_name = Strings::save(block_id->Name(), branch_num_str) ;
            orig_name = Strings::save(block_id->Name()) ;
        } else {
            block_name = Strings::save(tmp1, branch_num_str) ;
            orig_name = Strings::save(tmp1) ;
        }
        // Check for name conflict applying branch number :
        // If identifier of same name exists in nearest non-empty enclosing scope,
        // If branch number exist, branch number should be increased
        exist = non_empty_scope->FindLocal(block_name) ;
        // VIPER #6687 : Do not ignore unnamed block ids
        //if (exist && exist->IsUnnamedBlock()) exist = 0 ;
        while (exist) {
            char *tmp = block_name ;
            // Create a new name :
            branch_num++ ;
            char branch_num_str1[20] ;
            sprintf(branch_num_str1, "_%d", branch_num) ;
            if (block_id) {
                block_name = Strings::save(block_id->Name(), branch_num_str1) ;
            } else {
                block_name = Strings::save(orig_name, branch_num_str1) ;
            }
            exist = non_empty_scope->FindLocal(block_name) ;
            // VIPER #6687 : Do not ignore unnamed block ids
            //if (exist && exist->IsUnnamedBlock()) exist = 0 ;
            Strings::free(tmp) ;
        }
        Strings::free(tmp1) ;
    }

    // Now create a block identifier with the name :
    VeriIdDef *ret_block_id = new VeriBlockId(block_name) ;
    if (from) ret_block_id->SetLinefile(from->Linefile()) ;
    ret_block_id->SetIsUnnamedBlock() ; // Mark as unnamed block
    if (orig_name) ret_block_id->SetOrigName(orig_name) ;
    Strings::free(orig_name) ;
    if (!non_empty_scope->DeclareBlockId(ret_block_id)) {
        // FIXME: Following code should never be executed, since we already checked and renamed (VIPER #7773)
        if (from) from->Error("%s is already declared", block_name) ;
    }
    return ret_block_id ;
}

void VeriGenerateBlock::ProcessDesignSpecifiedBlockId(VeriScope *enclosing_scope, int branch_num, const Set *block_names)
{
    if (!_block_id || !enclosing_scope) return ;

    if (!block_names || !block_names->Size() || !block_names->Get(_block_id->Name())) {
    /*
    // First find the nearest non-empty enclosing scope to check name conflict
    VeriScope *non_empty_scope = enclosing_scope ;
    while (non_empty_scope) {
        if (non_empty_scope->IsEmptyScope()) {
            non_empty_scope = non_empty_scope->Upper() ;
        } else {
            break ;
        }
    }
    if (!non_empty_scope) return ; // Something bad, cannot find non-empty enclosing scope
    VeriIdDef *exist = non_empty_scope->FindLocal(_block_id->Name()) ;
    if (!exist || (!exist->IsUnnamedBlock())) {
    */
        // This means either this block is not used as branch statement of conditional
        // generate or it is the first branch of conditional generate or it is in
        // any branch, but previous branches does not have same design declared block name.
        // If enclosing_scope is a empty scope, it is a branch of conditional generate
        // and _block_id is declared in this enclosing_scope, but should be moved
        // to nearest non-empty scope.
        if (!enclosing_scope->IsEmptyScope()) return ; // nothing to do

        // Need to move block identifier from empty scope to non-empty scope
        if (enclosing_scope->Undeclare(_block_id)) { // Undeclare from branch scope
            _block_id->SetIsUnnamedBlock() ;
            VeriIdDef *exist = 0 ;
            if (!enclosing_scope->DeclareBlockId(_block_id, 0 /* no force insert */, &exist)) {
                // Error : name conflict
                _block_id->Error("%s is already declared", _block_id->Name()) ;
                if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
                _block_id->SetModuleItem(0) ;
                _block_id->SetLocalScope(0) ;
                _block_id = 0 ;
                delete _block_id ;
            }
        }
        return ;
    }
    // Name of this block is already used in other branch of conditional generate.
    // This is allowed as per LRM. So we will modified the name to create an unique
    // name.
    VERIFIC_ASSERT(branch_num >= 0) ;
    if (enclosing_scope->Undeclare(_block_id)) {
        VeriIdDef *block_id = CreateUnnamedBlockId(enclosing_scope, 0, branch_num, _block_id, this, 0) ;
        if (block_id) {
            _block_id->SetModuleItem(0) ;
            _block_id->SetLocalScope(0) ;
            delete _block_id ;
            _block_id = block_id ;
            _block_id->SetModuleItem(this) ;
            _block_id->SetLocalScope(_scope) ;
            if (_scope) _scope->SetOwner(_block_id) ;
        }
    }
}
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK

VeriFunctionDecl::VeriFunctionDecl(VeriFunctionDecl *decl) :
    VeriModuleItem(),
    _func(0),
    _name(0),
    _automatic_type(0),
    _can_be_const_func(0),
    _is_const_called(0),
    _verilog_method(0),
    _data_type(0),
    _ansi_io_list(0),
    _decls(0),
    _stmts(0),
    _scope(0),
    _ports(0)
{
    if (!decl) return ;

    _func               = decl->_func ;              decl->_func = 0 ;
    _name               = decl->_name ;              decl->_name = 0 ;
    _automatic_type     = decl->_automatic_type ;
    _can_be_const_func  = decl->_can_be_const_func ;
    _is_const_called    = decl->_is_const_called ;
    _verilog_method     = decl->_verilog_method ;
    _data_type          = decl->_data_type ;         decl->_data_type = 0 ;
    _ansi_io_list       = decl->_ansi_io_list ;      decl->_ansi_io_list = 0 ;
    _decls              = decl->_decls ;             decl->_decls = 0 ;
    _stmts              = decl->_stmts ;             decl->_stmts = 0 ;
    _scope              = decl->_scope ;             decl->_scope = 0 ;
    _ports              = decl->_ports ;             decl->_ports = 0 ;

    delete decl ;
    if (_func) _func->SetModuleItem(this) ;
}

VeriTaskDecl::VeriTaskDecl(VeriTaskDecl *decl) :
    VeriModuleItem(),
    _task(0),
    _name(0),
    _automatic_type(0),
    _contains_delay_or_event_control(0),
    _ansi_io_list(0),
    _decls(0),
    _stmts(0),
    _scope(0),
    _ports(0)
{
    if (!decl) return ;

    _task                            = decl->_task ;         decl->_task = 0 ;
    _name                            = decl->_name ;         decl->_name = 0 ;
    _automatic_type                  = decl->_automatic_type ;
    _contains_delay_or_event_control = decl->_contains_delay_or_event_control ;
    _ansi_io_list                    = decl->_ansi_io_list ; decl->_ansi_io_list = 0 ;
    _decls                           = decl->_decls ;        decl->_decls = 0 ;
    _stmts                           = decl->_stmts ;        decl->_stmts = 0 ;
    _scope                           = decl->_scope ;        decl->_scope = 0 ;
    _ports                           = decl->_ports ;        decl->_ports = 0 ;

    delete decl ;
    if (_task) _task->SetModuleItem(this) ;
}

VeriDPIFunctionDecl::VeriDPIFunctionDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriFunctionDecl *func_proto) :
    VeriFunctionDecl(func_proto), // absorbes 'func_proto'
    _type(type),
    _spec_string(spec),
    _property(property),
    _c_identifier(c_id)
{
}

VeriDPIFunctionDecl::VeriDPIFunctionDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriName *func, VeriScope *scope) :
    VeriFunctionDecl(0), // Use special constructor to stop VeriFunctionDecl::Init call
    _type(type),
    _spec_string(spec),
    _property(property),
    _c_identifier(c_id)
{
    delete _name ;
    _name = func ;
    _func = (func) ? func->GetId(): 0 ;
    _scope = scope ;
}

VeriDPIFunctionDecl::~VeriDPIFunctionDecl()
{
    Strings::free(_spec_string) ; _spec_string = 0 ;
    Strings::free(_c_identifier) ; _c_identifier = 0 ;
}

void
VeriDPIFunctionDecl::Resolve(VeriScope *scope, veri_environment environment)
{
    // Call the parent class resolve:
    VeriFunctionDecl::Resolve(scope, environment) ;
}

VeriDPITaskDecl::VeriDPITaskDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriTaskDecl *task_proto) :
    VeriTaskDecl(task_proto), // absorbes 'task_proto'
    _type(type),
    _spec_string(spec),
    _property(property),
    _c_identifier(c_id)
{
}

VeriDPITaskDecl::VeriDPITaskDecl(unsigned type, char *spec, unsigned property, char *c_id, VeriName *task, VeriScope *scope) :
    VeriTaskDecl(0, task, 0, 0, 0, scope),
    _type(type),
    _spec_string(spec),
    _property(property),
    _c_identifier(c_id)
{
}

VeriDPITaskDecl::~VeriDPITaskDecl()
{
    Strings::free(_spec_string) ; _spec_string = 0 ;
    Strings::free(_c_identifier) ; _c_identifier = 0 ;
}

void
VeriDPITaskDecl::Resolve(VeriScope *scope, veri_environment environment)
{
    // Call the parent class resolve:
    VeriTaskDecl::Resolve(scope, environment) ;
}

// VIPER #6461: Check if the modport has any task/functions in it which is
// defined in the interface. That must be an 'import' and can be called from
// anywhere the modport is available. Ideally, it should check whether the
// task/function is using anything from the interface which is not in the
// modport, but that optimization is not done in this case.
unsigned
VeriModportDecl::ContainsInterfaceMethod() const
{
    // Get the scope of the mother interface:
    VeriScope *this_interface_scope = (_scope) ? _scope->ModuleScope() : 0 ;
    if (!this_interface_scope) return 0 ; // Cannot check

    // Cannot iterate over '_ports' it does not contains items without direction!

    unsigned i ;
    VeriModuleItem *item ;
    VeriIdDef *id ;
    Array *ids ;
    FOREACH_ARRAY_ITEM(_modport_port_decls, i, item) {
        if (!item) continue ;
        ids = item->GetIds() ; // For VeriDataDecls of task/functions
        Array *tmp = 0 ;
        id = item->GetId() ; // For task/function prototypes
        if (!ids && id) {
            ids = new Array(1) ;
            ids->Insert(id) ;
            tmp = ids ;
        }
        unsigned j ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            if (!id->IsFunction() && !id->IsTask()) continue ;
            VeriModuleItem *body = id->GetModuleItem() ;
            VeriScope *scope = (body) ? body->GetScope() : 0 ;
            // This task/function is defined in this interface and is being imported
            if (scope && (scope->ModuleScope()==this_interface_scope)) {
                delete tmp ;
                return 1 ;
            }
        }
        delete tmp ;
    }

    return 0 ;
}

VeriIdDef *
VeriGenerateFor::FindSingleGenVarId() const
{
    // VIPER #6688: Find the single genvar-id for this for-generate:
    VeriIdDef *genvar_id = (_initial) ? _initial->GetId() : 0 ;
    if (!genvar_id && _initial) {
        Array *ids = _initial->GetIds() ;
        if (ids && (ids->Size()==1)) {
            // Initial has a single id, it is the genvar id:
            genvar_id = (VeriIdDef *)ids->GetFirst() ;
        }
    }

    // Leave the following for now, will re-visit if needed later.
    //if (!genvar_id && _rep) {
    //    // Repeatition statement can be either VeriGenVarAssign or VeriBlockingAssign.
    //    // We may have to find the id from these classes, but that requires class-id
    //    // check as well as casting to respective class type with our current set of
    //    // API or we have to add some vistual method to avoid that.
    //}
    //if (!genvar_id && _cond) {
    //    // The same comments as above hold here too.
    //}

    return genvar_id ;
}
unsigned VeriAlwaysConstruct::GetSpecialAlwaysToken() const
{
    if (GetQualifier(VERI_ALWAYS_COMB)) return VERI_ALWAYS_COMB ;
    if (GetQualifier(VERI_ALWAYS_LATCH)) return VERI_ALWAYS_LATCH ;
    if (GetQualifier(VERI_ALWAYS_FF)) return VERI_ALWAYS_FF ;
    return 0 ;
}

/* static */ unsigned
VeriModuleItem::AttachCommentToLastChildInArray(const Array *children, const VeriCommentNode *comment)
{
    if (!children || !comment) return 0 ;

    // VIPER #7560: Attach the comment to the last child node within me:
    unsigned i ;
    VeriTreeNode *child = 0 ;
    FOREACH_ARRAY_ITEM_BACK(children, i, child) if (child) break ;
    if (!child) return 0 ; // No child found

    Array *arr = new Array(1) ;
    arr->InsertLast(comment) ;
    child->AddComments(arr) ;

    return 1 ; // Attached
}
/* static */ unsigned
VeriModuleItem::IsFirstLessThanSecondLiteral(const char *timeunit_lit, const char *timeprecision_lit)
{
    if (!timeunit_lit || !timeprecision_lit) return 0 ;

    double timeunit_num = 0 ;
    char *timeunit_unit = 0 ;
    char *str = Strings::save(timeunit_lit) ;
    char *prefix = str ;

    while(*str) {
        if (isalpha(*str)) {    // Get character : create unit
            timeunit_unit = Strings::save(str) ; // Get the unit pointer
            char save = *str ; // Save this character
            *str = '\0' ;       // Terminate the string
            timeunit_num = Strings::atof(prefix) ; // Calculate the value
            *str = save ;       // Restore the unit character
            break ;
        }
        str++ ;
    }
    if (timeunit_num < 0) {
        Strings::free(prefix) ;
        Strings::free(timeunit_unit) ;
        return 0 ; // Negative number
    }
    int timeunit_val = 0 ;
    if (timeunit_unit) {
        switch (*timeunit_unit) {
        case 'f' : timeunit_val = 15 ; break ;
        case 'p' : timeunit_val = 12 ; break ;
        case 'n' : timeunit_val = 9 ; break ;
        case 'u' : timeunit_val = 6 ; break ;
        case 'm' : timeunit_val = 3 ; break ;
        case 's' : timeunit_val = 1 ; break ;
        default  : break ;
        }
    }
    // Remove the fractional part of timeunit value
    // Adjust timeunit value accordingly
    while (timeunit_num < 1) {
        timeunit_num = timeunit_num * 10 ;
        timeunit_val++ ;
    }
    // Make the timeunit_num 1 and adjust timeunit value accordingly
    while (timeunit_num > 1) {
        timeunit_num = timeunit_num / 10 ;
        timeunit_val-- ;
    }
    Strings::free(prefix) ;

    double timeprec_num = 0 ;
    char *timeprec_unit = 0 ;
    str = Strings::save(timeprecision_lit) ;
    prefix = str ;
    while(*str) {
        if (isalpha(*str)) {    // Get character : create unit
            timeprec_unit = Strings::save(str) ; // Get the unit pointer
            char save = *str ; // Save this character
            *str = '\0' ;       // Terminate the string
            timeprec_num = Strings::atof(prefix) ; // Calculate the value
            *str = save ;       // Restore the unit character
            break ;
        }
        str++ ;
    }
    if (timeprec_num < 0) {
        Strings::free(prefix) ;
        Strings::free(timeprec_unit) ;
        Strings::free(timeunit_unit) ;
        return 0 ; // Negative number
    }
    int timeprec_val = 0 ;
    if (timeprec_unit) {
        switch (*timeprec_unit) {
        case 'f' : timeprec_val = 15 ; break ;
        case 'p' : timeprec_val = 12 ; break ;
        case 'n' : timeprec_val = 9 ; break ;
        case 'u' : timeprec_val = 6 ; break ;
        case 'm' : timeprec_val = 3 ; break ;
        case 's' : timeprec_val = 1 ; break ;
        default  : break ;
        }
    }
    // Remove the fractional part of timeprecision value
    if (timeprec_num < 1) {
        while (timeprec_num < 1) {
            timeprec_num = timeprec_num * 10 ;
            timeprec_val++ ;
        }
    }
    // Make the timeunit_num 1 and adjust timeunit value accordingly
    while (timeprec_num > 1) {
        timeprec_num = timeprec_num / 10 ;
        timeprec_val-- ;
    }
    Strings::free(prefix) ;
    if (timeunit_unit && timeprec_unit) {
        if (timeunit_val > timeprec_val) {
            // Timeunit is less than time precision
            Strings::free(timeunit_unit) ;
            Strings::free(timeprec_unit) ;
            return 1 ;
        } else if (timeunit_val < timeprec_val) {
            Strings::free(timeunit_unit) ;
            Strings::free(timeprec_unit) ;
            return 0 ;
        }
    }
    Strings::free(timeunit_unit) ;
    Strings::free(timeprec_unit) ;

    // Units are same compare the numbers
    if (timeunit_num < timeprec_num) return 1 ; // Timeunit is less than time precision
    return 0 ;
}

