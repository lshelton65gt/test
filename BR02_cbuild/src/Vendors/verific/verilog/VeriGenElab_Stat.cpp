/*
 *
 * [ File Version : 1.360 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h>
#include <string.h> // strchr
#include <math.h>   // fabs

#include "veri_file.h"
#include "Message.h"
#include "VeriElab_Stat.h"
#include "VeriModule.h"
#include "VeriExpression.h"
#include "VeriModuleItem.h"
#include "VeriStatement.h"
#include "VeriMisc.h"
#include "VeriConstVal.h"
#include "Set.h"
#include "Map.h"
#include "Array.h"
#include "VeriUtil_Stat.h"
#include "VeriCopy.h"
#include "VeriScope.h"
#include "veri_tokens.h"
#include "Strings.h"
#include "VeriBaseValue_Stat.h"
#include "VeriId.h"
#include "VeriLibrary.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif
//////////////////////////////////////////////////////////////////////////////
//////////////////     Generate elaboration                 /////////////////
/////////////////////////////////////////////////////////////////////////////

// limit on the number of iterations that for-generate loop statements can go through.
#define LOOPLIMIT 5000

//////////////////////////////////////////////////////////////////////////////
//////////////////     Internal class definitions            /////////////////
/////////////////////////////////////////////////////////////////////////////
#ifdef VERIFIC_NAMESPACE
namespace Verific {
#endif
// Class to elaborate a generate construct
// This class stores the elaborated module items in an array. The function 'GetNewModuleItems'
// of this class will return those objects. In main static elaboration these items are added
// in the module item list and the generate construct is removed from the parse tree.
class VeriElabGenerate
{
public:
    VeriElabGenerate(VeriModuleItem *gen, const VeriModuleItem *module, VeriLibrary *lib) ;
    ~VeriElabGenerate() ;

private:
    // Prevent compiler from defining the following
    VeriElabGenerate() ;                                    // Purposely leave unimplemented
    VeriElabGenerate(const VeriElabGenerate &) ;            // Purposely leave unimplemented
    VeriElabGenerate& operator=(const VeriElabGenerate &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    void             AddNewModuleItem(const VeriModuleItem *new_item, unsigned long copied_item) ; // Add elaborated module item

    Map *            GetNewModuleItems() { return &_new_mod_items ; } // Return elaborated module items

    Map *            GetIdTab() { return _id_tab; } //  Return the Map of VeriIdDef*->VeriExpression*, maintained to associate objects created inside generate scope to new objects created due to generate elaboration.

    VeriScope *      GetModScope() {
        if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
            return _current_blk_scope ; // VIPER #6080 : Return current block scope to copy generate items properly
        } else {
            return _mod_scope ;
        }
    } // Return module scope

    void             Elaborate(const VeriIdDef *blk_id, const Array *blk_items, const VeriScope *blk_scope, unsigned is_cp_needed, VeriScope *active_scope, const VeriIdDef *localparam_id=0) ; // Elaborate generate block
    // VIPER #6080 : Main routine to create generate block for each iteration of for-generate/generateblock/if-generate/case-generate
    void             ElaborateCreatingBlock(const VeriIdDef *blk_id, const Array *blk_items, const VeriScope *blk_scope, unsigned is_cp_needed, VeriScope *active_scope, const VeriIdDef *gen_id, const VeriBaseValue *loop_val, const VeriTreeNode *from, const VeriIdDef *localparam_id=0) ;
#ifdef VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
    // Create localparameter in the generate block created for each iteration of for-gen-loop
    void             CreateLocalParam(const VeriIdDef *gen_id, const VeriBaseValue *loop_val, const VeriTreeNode *from, const VeriIdDef *localparam_id) ;
#endif

    void             BindGenStmt(VeriTreeNode *node) ; // Replace genvar usage by constant value

    unsigned         PopulateIdTable(const VeriScope *blk_scope, VeriScope *new_scope =0) ; // Create identifiers for each iteration of for-generate
    unsigned         UpdateIds(const VeriScope *blk_scope, VeriScope *new_scope =0) ; // VIPER #7986 : Copy initial-value/port-connects/dimensions of identifiers

    void             Detach(VeriModuleItem *node) ; // Detach the argument specific generate item from the generate

    void             PushNamedPath(const char* name) ; // push a name prefix
    void             PopNamedPath() ; // Pop name prefix
    char *           CreateFullName(const char *short_name, const char *sep = 0) const ; // Create a full (allocated) hierarchical name from the named path stack
    unsigned         IsHierPath() const { return _named_path ? 1 : 0 ; } // Returns 1 if named path stack exists
    ValueTable *     GetValueTable() const { return _val_table ; }
    VeriMapForCopy * GetCopyObj() const { return _copy_obj ; }

    // VIPER #4382 :
    void             AddDefparam(const Array *id_list, const VeriModuleItem *df) ;
    Map *            GetDefparams() const  { return _defparams ; }
    void             CheckDefparamValidity(VeriPseudoTreeNode *mod_node, Map *mod_map_tab, Map *top_mod_nodes, Set *rem_defparams) ;
    Map *            GetNewIds() const     { return _new_ids ;}
    void             AddNewId(const VeriIdDef *id) ;
    void             ResetNewIds() ;
    unsigned         IsCreatedPreviously(const char *name) const ;
    char *           CreateBlockName(const VeriIdDef *block_id, const VeriBaseValue *idx_val) const ;

private:
    VeriModuleItem *_gen ; // pointer to generate construct
    VeriScope *_mod_scope ; // module scope, identifiers declared within generate scope are moved to module scope.
    Map _new_mod_items ; // Array of elaboarted module items
    Array *_tmp_new_mod_items ; // Array to store elaborated module items for each iteration
    Map *_id_tab ; // Map of VeriIdDef*->VeriExpression*
    VeriLibrary *_library ;
    Array  *_named_path ; // Stack for name prefix
    ValueTable *_val_table ; // value table for expression evaluation
    VeriMapForCopy *_copy_obj ; // associates original iddefs to copied iddefs

    // VIPER #4382 :
    Map *_defparams ; // Store the defparams present inside generate
    Map *_new_ids ; // Set of identifiers created by generate elaboration

public :
    Map *GetElaboratedItems() { return _elaborated_items ; }
    // VIPER #6080 : Map to store elaborated items to be add in a elaborated generate block
    Map *_elaborated_items ;
    VeriScope *_current_blk_scope ; // Current elaborated generate block
    unsigned IsIdOfThisGenerate(const VeriIdDef *id) const { return (_gen_ids && _gen_ids->Get(id)) ? 1 : 0 ; }
    void AddThisGenerateId(const VeriScope *s) ;
    Set *_gen_ids ; // Store the block ids present in this generate
} ; // class VeriElabGenerate

/* -------------------------------------------------------------- */

// Class to replace genvar with its appropriate constant value. When each item of
// generate construct is elaborated, all the genvar identifiers used within the
// elaborated item are replaced with its value. This class also store the
// hierarchical identifiers used in the item in the 'VeriElabGenerate' class.
class ReplaceGenVarFromStmt : public VeriVisitor
{
public :
    explicit ReplaceGenVarFromStmt(VeriElabGenerate *gen_obj) ;
    ~ReplaceGenVarFromStmt() { _gen_obj = 0 ;}

private:
    // Prevent compiler from defining the following
    ReplaceGenVarFromStmt() ;                                         // Purposely leave unimplemented
    ReplaceGenVarFromStmt(const ReplaceGenVarFromStmt &) ;            // Purposely leave unimplemented
    ReplaceGenVarFromStmt& operator=(const ReplaceGenVarFromStmt &) ; // Purposely leave unimplemented

public:
    virtual void VERI_VISIT(VeriIndexedId, id_ref) ;
    virtual void VERI_VISIT(VeriIndexedMemoryId, id_ref) ;
    virtual void VERI_VISIT(VeriConcat, concat) ;
    virtual void VERI_VISIT(VeriMultiConcat, concat) ;
    virtual void VERI_VISIT(VeriFunctionCall, func_call) ;
    virtual void VERI_VISIT(VeriSystemFunctionCall, func_call) ;
    virtual void VERI_VISIT(VeriMinTypMaxExpr, mtp) ;
    virtual void VERI_VISIT(VeriUnaryOperator, u_op) ;
    virtual void VERI_VISIT(VeriBinaryOperator, b_op) ;
    virtual void VERI_VISIT(VeriQuestionColon, qc) ;
    virtual void VERI_VISIT(VeriEventExpression, event_expr) ;
    virtual void VERI_VISIT(VeriPortConnect, port_conn) ;
    virtual void VERI_VISIT(VeriRange, range) ;
    virtual void VERI_VISIT(VeriScopeName, name) ;
    virtual void VERI_VISIT(VeriDefParam, defparam) ;
    virtual void VERI_VISIT(VeriContinuousAssign, cont_assign) ;
    virtual void VERI_VISIT(VeriGateInstantiation, inst) ;
    virtual void VERI_VISIT(VeriModuleInstantiation, inst) ;
    virtual void VERI_VISIT(VeriNetRegAssign, net_assign) ;
    virtual void VERI_VISIT(VeriInstId, inst) ;
    virtual void VERI_VISIT(VeriCaseItem, case_item) ;
    virtual void VERI_VISIT(VeriDelayOrEventControl, case_item) ;
    virtual void VERI_VISIT(VeriParamId, id_def) ;
    virtual void VERI_VISIT(VeriVariable, id_def) ;
    virtual void VERI_VISIT(VeriBlockingAssign, assign) ;
    virtual void VERI_VISIT(VeriNonBlockingAssign, assign) ;
    virtual void VERI_VISIT(VeriTaskEnable, task_call) ;
    virtual void VERI_VISIT(VeriSystemTaskEnable, task_call) ;
    virtual void VERI_VISIT(VeriDelayControlStatement, delay_stmt) ;
    virtual void VERI_VISIT(VeriConditionalStatement, if_stmt) ;
    virtual void VERI_VISIT(VeriCaseStatement, case_stmt) ;
    virtual void VERI_VISIT(VeriRepeat, loop) ;
    virtual void VERI_VISIT(VeriWhile, loop) ;
    virtual void VERI_VISIT(VeriFor, loop) ;
    virtual void VERI_VISIT(VeriWait, wait) ;
    virtual void VERI_VISIT(VeriSequenceConcat, concat) ;
    virtual void VERI_VISIT(VeriClockedSequence, clk_seq) ;
    virtual void VERI_VISIT(VeriDistOperator, dist_op) ;
    virtual void VERI_VISIT(VeriCast, op) ;
    virtual void VERI_VISIT(VeriNew, op) ;
    virtual void VERI_VISIT(VeriConcatItem, concat_item) ;
    virtual void VERI_VISIT(VeriNamedPort, item) ;
    VeriExpression *ToCurrGenvarValue(VeriExpression *expr) ;

private :
    VeriElabGenerate  *_gen_obj ;
} ; // class ReplaceGenVarFromStmt

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
}
#endif

///////////////////////////////////////////////////////////////////////////
///////////////    Internal class definitions end        //////////////////
///////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//    Virtual routine 'StaticElaborate' on module item to unroll generate,  //
//    array instances and delete genvars outside generate                   //
//////////////////////////////////////////////////////////////////////////////

// Call 'StaticElaborate' on every module item to trigger generate elaboration,
// array instance elaboration. genvars outside generate are deleted
unsigned VeriModule::StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set * /*defparams*/)
{
    // VIPER #3785: Set the global timescale to be that of the module:
    char *timescale = Strings::save(veri_file::GetTimeScale()) ;
    char *timescale_fromtimeunit = GetTimeUnitDefinedTimeScale() ;
    veri_file::SetTimeScale(timescale_fromtimeunit ? timescale_fromtimeunit: _timescale) ;

    unsigned analysis_mode = veri_file::GetAnalysisMode() ;
    // Set dialect here, based on the field in the module :
    veri_file::SetAnalysisMode(_analysis_dialect) ;

    unsigned succeeded = 1 ;
    unsigned i ;
    VeriExpression *port ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port) {
        if (!port) continue ;
        VeriPseudoTreeNode::ElaborateClassDataType(port, this, mod_map_tab) ;
    }
    VeriModuleItem *item ;
    Set local_defparams(POINTER_HASH) ;
    Set processed(POINTER_HASH) ;
    // VIPER #7508 : Traverse import declarations
    FOREACH_ARRAY_ITEM(_package_import_decls, i, item) {
        if (!item) continue ;
        if (!item->StaticElaborateModuleItem(this, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, &local_defparams)) {
            succeeded = 0 ;
            break ;
        }
    }
    // Traverse the module items to find generate and instantiations
    FOREACH_ARRAY_ITEM(_module_items, i, item) {
        if (!item || item->IsModule()) continue ; // Do not recur into nested module
        if (!processed.Insert(item)) continue ; // VIPER #6072 : Item is already processed, don't process that again
        if (!item->StaticElaborateModuleItem(this, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, &local_defparams)) {
            succeeded = 0 ;
            break ;
        }
    }
    // VIPER #4382 : Iterate over all defparams whose no elements are defined in the
    // scope where defparam is defined.So all these are outside of generate scope,
    // we will produce error only for resolved defparams.
    VeriPseudoTreeNode *mod_node = 0 ;
    FOREACH_ARRAY_ITEM(parent_pseudo_nodes, i, mod_node) {
        if (mod_node) break ;
    }
    HierIdWithDeclScope *id ;
    SetIter si ;
    FOREACH_SET_ITEM(&local_defparams, si, &id) {
        if (!id) continue ;
        VeriExpression *name_ref = id->GetNameRef() ;
        if (!name_ref) continue ;
        id->ResetMarkAsSpecial() ; // Mark this defparam as special
        id->SetDeclarationScope(_scope) ;

        // Try to resolve the defparam name
        name_ref->ResolveName(id, mod_node, top_mod_nodes, mod_map_tab, 0) ;
        if (id->GetResolvedIddef()) {  // Get last resolved id
            name_ref->Error("defparam under generate scope cannot change parameter value outside of hierarchy") ;
        }
        delete id ;
    }

    // VIPER #3785: Restore the original global timescale:
    veri_file::SetTimeScale(timescale) ;
    Strings::free(timescale) ;
    Strings::free(timescale_fromtimeunit) ;

    veri_file::SetAnalysisMode(analysis_mode) ;
    return succeeded ;
}
// Process uninstantiated classes defined inside scope 'scope'
void VeriPseudoTreeNode::ProcessUnInstantiatedClassesInt(const VeriScope *scope, VeriModuleItem *container, Map *mod_map_tab)
{
    if (!scope) return ;

    Map *this_scope = scope->GetThisScope() ;
    MapIter mi ;
    VeriIdDef *v ;
    Set done(POINTER_HASH) ;
    FOREACH_MAP_ITEM(this_scope, mi, 0, &v) {
        if (!v || !v->IsClass()) continue ;
        // Get class body :
        VeriModuleItem *body = v->GetModuleItem() ;
        if (!body || done.Get(body)) continue ; // 'body' is already processed, ignore this
        if (body->IsClassInstantiated()) continue ; // This class is instantiated, ignore this
        if (body->IsContainCopiedVersion()) continue ; // This is created as we have copied this class for a parameter setting

        // VIPER #5248 (test case: Amd_cnb_031808_block_echo_RH30_64).
        // If the base class is a type parameter or a typedef (which is declared but not
        // defined before the derived class) we did not resolve the class items in Resolve,
        // this has been done to suppress unwanted errors when base class members are used
        // in derived class. For all other type of base classes we already resolved
        // the class items. So, here, we should resolve items of those classes that are not
        // resolved in Resolve flow.
        VeriName *base_class_name = body->GetBaseClassName() ;
        VeriTypeInfo *base_type = base_class_name ? base_class_name->CheckType(0, VeriTreeNode::NO_ENV): 0 ;
        //VeriIdDef *base_id = base_class_name ? base_class_name->GetId() : 0 ;
        //VeriModuleItem *base_class_item = base_id ? base_id->GetModuleItem() : 0 ;
        // Do not try to process uninstantiated classes
        // with 1. type parameters or 2. type parameter or unresolved type as base class
        // FIXME : Not sure whether should be done only for relaxed checking mode
        //if (VeriNode::InRelaxedCheckingMode()) {
        unsigned contains_type_params = 0 ;
        Array *params = body->GetParameters() ;
        unsigned idx ;
        VeriIdDef *param_id ;
        FOREACH_ARRAY_ITEM(params, idx, param_id) {
            if (param_id && param_id->IsType()) {
                contains_type_params = 1 ; // contains type parameter
                break ;
            }
        }
        if (contains_type_params || (base_type && (base_type->IsTypeParameterType() || base_type->IsUnresolvedNameType()))) {
            // Base type not resolved yet
            delete base_type ;
            continue ;
        }
        // If base class scope is not already set, set that
        VeriIdDef *base_class_id = base_type ? base_type->GetNameId(): 0 ;
        VeriScope *class_scope = body->GetScope() ;
        if (base_class_name && class_scope && !class_scope->GetBaseClassScope()) {
            if (base_class_id) class_scope->SetBaseClassScope(base_class_id->LocalScope()) ;
        }
        unsigned not_resolved_prev = 0 ;
        if (!body->IsResolved()) {
            not_resolved_prev = 1 ; // VIPER #7328 :
            body->Resolve(0, VeriTreeNode::VERI_UNDEF_ENV) ;
        }
        //}
        //Array *items = body->GetItems() ;
        //if (base_class_name && (!base_class_item || !base_class_item->IsClass())) {
        //if (base_type && (base_type->IsTypeParameterType() || base_type->IsUnresolvedNameType())) {
            //unsigned i ;
            //VeriModuleItem *item ;
            //FOREACH_ARRAY_ITEM(items, i, item) {
                //if (item) item->Resolve(body->GetScope(), VeriTreeNode::VERI_UNDEF_ENV) ;
            //}
        //}
        delete base_type ;
        // Get the pseudo tree nodes for 'container'
        Set *nodes = (Set*)mod_map_tab->GetValue(container) ;
        // Copy the class with its default parameter setting. If class does not
        // have any parameter, append _default with class name and copy class
        // with that name. This will stop us to modify original class declaration.
        // Processing static members of class may change the body of the class. This
        // may lead us to create illegal processing if this class is later used as
        // type with different parameter setting from any other location:
        VeriModuleItem *copied_body = body->CopyWithDefaultParameter() ;
        if (!copied_body) continue ;
        (void) done.Insert(copied_body) ;

        // Traverse the body of this class to add its hierarchical identifiers in
        // container module and elaborate other class instances inside it.
        SetIter si ;
        VeriPseudoTreeNode *node ;
        FOREACH_SET_ITEM (nodes, si, &node) {
            if (!node || (node->GetDesign() != container)) continue ;
            node->ProcessClass(copied_body, mod_map_tab, not_resolved_prev) ;
        }
    }
    Set *children = scope->GetChildrenScopes() ;
    SetIter si ;
    VeriScope *s ;
    FOREACH_SET_ITEM(children, si, &s) {
        if (!s) continue ;
        VeriIdDef *owner = s->GetOwner() ;
        // VIPER #4859 : Do not recurse into nested module. We will process that
        // separately from its pseudo node
        if (owner && (owner->IsClass() || owner->IsModule())) continue ;
        VeriPseudoTreeNode::ProcessUnInstantiatedClassesInt(s, container, mod_map_tab) ;
    }
}

// Copy class with default parameter setting. Append _default with class name
// if no parameter exists :
VeriModuleItem *VeriClass::CopyWithDefaultParameter()
{
    if (!_id) return 0 ;
    char *class_name = CreateNameWithDefaultparam(0) ;
    VeriScope *parent = _id->GetOwningScope() ;

    // Check whether this class with same parameter setting is already copied
    VeriIdDef *exist = (parent) ? parent->Find(class_name) : 0 ;
    if (exist) {
        // If it is already existed, that means it is already processed too
        // So return null to stop procesing its body once more
        Strings::free(class_name) ;
        return exist->GetModuleItem() ;
    }
    VeriMapForCopy old2new ;
    _contain_copied_version = 1 ; // Mark this class, this class will be copied and copied one will be elaborated
    VeriModuleItem *copied_class = CopyWithName(class_name, old2new, 1 /* add copied class*/) ;
    // Mark this class, so that we do not consider it again as unprocessed class.
    // We may call 'ProcessUninstantitedClasses' multiple times due to presence
    // of generate/hier name in class body  in design.
    if (copied_class) copied_class->SetContainCopiedVersion() ;
    Strings::free(class_name) ;

    /* Following section is not needed, as this is already done in copy routine of class
    Array *items = copied_class ? copied_class->GetItems() : 0 ;
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(items, i, item) {
        if (!item || !(item->IsFunctionDecl() || item->IsTaskDecl())) continue ;
        // VIPER #4030 : Functions declared inside a class are automatic if not
        // explicitly specified as static
        // VIPER #4853 : Tasks declared inside a class are automatic if not
        // explicitly specified as static
        VeriIdDef *func_task_id = item->GetId() ;
        if (func_task_id && !func_task_id->IsAutomatic() && !item->GetQualifier(VERI_STATIC)) {
            func_task_id->SetIsAutomatic() ;
        }
        if (func_task_id && func_task_id->GetFunctionType()==VERI_METHOD_NEW && copied_class && !copied_class->GetNewFunctionId()) copied_class->SetNewFunctionId(func_task_id) ;
    }
    */

    return copied_class ;
}
unsigned VeriImportDecl::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set * /*defparams*/)
{
    // VIPER #7508 : Iterate over all imported items and changed the package and
    // imported item name if needed
    VeriScope *container_scope = mod ? mod->GetScope(): 0 ;
    VeriScopeName *item ;
    VeriName *prefix = 0 ;
    const char *suffix = 0 ;
    unsigned i ;
    Map *pkg_scope_map = VeriStaticElaborator::GetPackageScopeMap() ;
    VeriIdDef *prefix_id, *suffix_id ;
    VeriScope *package_scope = 0, *copied_package_scope = 0 ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        prefix = item ? item->GetPrefix(): 0 ;
        if (!prefix || !item) continue ;
        prefix_id = prefix->GetId() ;
        package_scope = prefix_id ? prefix_id->LocalScope(): 0 ;
        // Check whether 'package_scope' is already copied. If so use copied scope
        copied_package_scope = pkg_scope_map ? (VeriScope*)pkg_scope_map->GetValue(package_scope) : 0 ;
        if (copied_package_scope) {
            // Used copied prefix identifier
            prefix_id = copied_package_scope->GetOwner() ;
            prefix->SetId(prefix_id) ;
            item->SetPrefixId(prefix_id) ;
            prefix->SetName(Strings::save(prefix_id->Name())) ;
        }
        suffix = item->GetSuffix() ;
        suffix_id = item->GetId() ;
        if (copied_package_scope && suffix_id) {
            // Use copied suffix identifier
            suffix_id = copied_package_scope->FindLocal(suffix) ;
            if (container_scope) container_scope->Using(copied_package_scope) ;
            if (suffix_id) item->SetId(suffix_id) ;
        }
        if (suffix && suffix_id && (Strings::compare(suffix_id->Name(), suffix)==0)) {
            item->SetName(Strings::save(suffix_id->Name())) ;
        }
    }
    return 1 ;
}
// Insert genvar declaration (if it is) in argument specific array
unsigned VeriDataDecl::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array *parent_pseudo_nodes, Map *mod_map_tab, Map * /* top_mod_nodes */, Set * /*defparams*/)
{
    (void) mod_map_tab ; // to prevent compiler warnings
    (void) parent_pseudo_nodes ; // to prevent compiler warnings

    // FIX_ME have to use IsAms flag in elaboration. In verilog 2k static elaboration
    // we remove genvars while unrolling generate. But in ams genvar can be used in
    // analog block. So we cannot remove genvars. If we use product marker to not
    // remove genvars in ams, non pre-processed code does not behave same as pre-processed code.
    if (!IsAms()) {
        if (IsGenVarDecl()) {
            (void) mod->ReplaceChildBy(this, 0) ;
            return 1 ;
        }
    }
    /* VIPER #6292 : Moved to VarUsage as range can contain hierarchical ids
       referring to objects declared within generate block
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(&_ids,i,id) {
        if (!id) continue ;
        if ((id->GetDataType() != _data_type) && !id->IsParam()) {
            // This is a Verilog 2000 check (so we look at packed dimensions only).
            VeriRange *range = (_data_type) ? _data_type->GetDimensions() : 0 ;
            // Pick up the existing MSB range on the data type of the id :
            VeriDataType *id_data_type = id->GetDataType() ;
            VeriRange *exist = (id_data_type) ? id_data_type->GetDimensionAt(0) : 0 ;
            if (range && exist) {
                // Both have a data type with dimensions.
                // VIPER #5190 : Produce error if ranges are different in two declarations of 'id'
                int curr_msb = 0, curr_lsb = 0 ;
                (void) range->GetWidth(&curr_msb, &curr_lsb, 0) ;
                int prev_msb = 0, prev_lsb = 0 ;
                (void) exist->GetWidth(&prev_msb, &prev_lsb, 0) ;
                if ((prev_msb != curr_msb) || (prev_lsb != curr_lsb)) {
                    if (id_data_type) id_data_type->Warning("%s was previously declared with a different range", id->Name()) ;
                }
            }
        }
    }
    */

    // VIPER #7353 : If design contains interface array instance, a virtual interface
    // declaration is created for that to handle the whole identifier use of interface
    // array instance. But instantiated interface may be copied in elaboration. If
    // so change the data type of declared virtual interface also.
    unsigned i, j ;
    VeriIdDef *id ;
    VeriModule *instantiated_mod = 0 ;

    FOREACH_ARRAY_ITEM(&_ids,i,id) {
        if (!id) continue ;
        if (!id->IsStaticElabCreatedVirtualInterface()) break ; // Not virtual interface
        // Extract instantiated interface from initial value
        VeriExpression *init_val = id->GetInitialValue() ;
        Array *exprs = init_val ? init_val->GetExpressions(): 0 ;
        VeriExpression *first_expr = exprs ? (VeriExpression*)exprs->At(0): 0 ;
        VeriIdDef *inst_id = first_expr ? first_expr->GetId(): 0 ;
        if (!inst_id) continue ;
        VeriPseudoTreeNode *parent ;
        FOREACH_ARRAY_ITEM(parent_pseudo_nodes, j, parent) {
            VeriPseudoTreeNode *n = parent ?  parent->CheckMatchWithInstanceName(inst_id->Name(), 0): 0 ;
            instantiated_mod = n ? n->GetContainingModule(): 0 ;
            break ;
        }
        if (!instantiated_mod) continue ;
        // Extract specified interface in data type
        VeriIdDef *type_id = _data_type ? _data_type->GetId(): 0 ;
        VeriModule *d_instantiated_mod = type_id ? type_id->GetModule(): 0 ;
        // If data type and initial value are pointing to different interface,
        // change data type with the interface specified in initial value.
        if (instantiated_mod != d_instantiated_mod) {
            if (_data_type) _data_type->SetId(instantiated_mod->GetId()) ;
        }
    }
    // VIPER #3828 : Elaboration of class data type:
    VeriPseudoTreeNode::ElaborateClassDataType(this, mod, mod_map_tab) ;
    return 1 ;
}
unsigned VeriAlwaysConstruct::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map *mod_map_tab, Map * /*top_mod_nodes*/, Set * /*defparams*/)
{
    VeriPseudoTreeNode::ElaborateClassDataType(this, mod, mod_map_tab) ;
    return 1 ;
}
unsigned VeriContinuousAssign::StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array *parent_pseudo_nodes, Map * /*mod_map_tab*/, Map * /*top_mod_nodes*/, Set * /*defparams*/)
{
    // VIPER #5615 : Change the reference of array instances in lvalue with
    // array instance elaboration created instances.
    VeriPseudoTreeNode::AddContAssign(this, parent_pseudo_nodes) ;

    // Moved to VarUsage :
    // Viper 5411, 4800 Produce error if streaming concatenation is
    // larger than other side of assignment.
    // Only net-assignments are allowed here (LRM A.6.2)
    //VeriNetRegAssign *assign ;
    //unsigned i ;
    //FOREACH_ARRAY_ITEM(_net_assigns, i, assign) {
        //if (!assign) continue ;
        //VeriExpression *lval = assign->GetLValExpr() ;
        //VeriExpression *val = assign->GetRValExpr() ;
        //VeriNode::CheckStreamingConcat(lval, val) ;
    //}
    return 1 ;
}
unsigned VeriInitialConstruct::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map *mod_map_tab, Map * /*top_mod_nodes*/, Set * /*defparams*/)
{
    VeriPseudoTreeNode::ElaborateClassDataType(this, mod, mod_map_tab) ;
    return 1 ;
}
// Vipers #4777, 4786, #4698, #4819 : export/import function/task support
VeriIdDef *VeriModuleItem::BindExportedSubprogram(VeriName *name, const Array *parent_pseudo_nodes)
{
    VeriIdDef *subprog_id = GetId() ;
    if (!name || !name->IsHierName() || !subprog_id) return 0 ; // cannot be exported subprog

    // If prefix is interface port/instance, it is exported function
    VeriName *prefix = name->GetPrefix() ;
    VeriIdDef *prefix_id = (prefix) ? prefix->GetId(): 0 ;
    unsigned exported_subprog = 0 ;
    if (prefix_id) {
        char *tmp_name = Strings::save(prefix_id->Name(), " ", name->GetSuffix()) ;
        if (Strings::compare(subprog_id->Name(), tmp_name)) exported_subprog = 1 ;
        Strings::free(tmp_name) ;
    }
    if (!prefix_id || !prefix_id->IsInterfacePort() || !exported_subprog) return 0 ; // already resolved or not exported subprog

    // Extract interface identifier from data type :
    VeriDataType *data_type = prefix_id->GetDataType() ;
    VeriName *formal_type_name = (data_type) ? data_type->GetTypeName() : 0 ;

    if (!formal_type_name)  return 0 ; // data type is not interface name
    VeriIdDef *interface_id = 0 ;
    if (formal_type_name->IsHierName()) { // interface_name.modport
        prefix = formal_type_name->GetPrefix() ;
        interface_id = prefix ? prefix->GetId(): 0 ;
    } else { // interface_name
        interface_id = formal_type_name->GetId() ;
    }
    if (!interface_id) {
        VeriPseudoTreeNode *parent_node = (parent_pseudo_nodes && parent_pseudo_nodes->Size()) ? (VeriPseudoTreeNode*)parent_pseudo_nodes->At(0): 0 ;
        unsigned is_generic = 0 ;
        if (parent_node) (void) parent_node->ProcessFormal(prefix_id, 0, 0, &interface_id, is_generic, 0) ;
    }
    VeriScope *interface_scope = interface_id ? interface_id->LocalScope(): 0 ;
    VeriIdDef *int_subprog_id = interface_scope ? interface_scope->FindLocal(name->GetSuffix()): 0 ;
    if (!int_subprog_id) {
        // VIPER #8375: Task or function is not exported. For every exported function we have
        // declared a task/function id in interface (VeriModportDecl::Resolve)
        if (interface_id) subprog_id->Error("task/function %s is not exported/externed in interface %s", name->GetSuffix(), interface_id ? interface_id->Name(): "") ;
        return 0 ; // something wrong, no id in interface
    }

    // Set this body to the identifier declared in interface
    // VIPER #5507: Do not error out if the subprogram was declared as extern, in that case it was not defined/exported:
    VeriModuleItem *body = int_subprog_id->GetModuleItem() ;
    if (((body && !body->GetQualifier(VERI_EXTERN)) || (parent_pseudo_nodes && (parent_pseudo_nodes->Size() > 1))) && !int_subprog_id->IsExternForkjoinTask()) {
        Error("multiple exports of %s", int_subprog_id->Name()) ;
    }

    // VIPER #7891: Produce error if subprogram id used as exported task/function but declared as imported task/func
    // in the modport of the interface. If the subprogram id is declared both as export and import we do not produce error.
    // We only produce error in the followin case:
    // interface intr;
    //     modport n1(input a, import tsk1); // user wrote import instead of export by mistake
    //     modport n2(input a, import task tsk1(input logic a)); // user wrote import instead of export by mistake
    //     modport n3(input a, import tsk2);
    //     modport n4(input a, expor task tsk2(input logic a));
    // endinterface
    // module test(intr intr1);
    //     task intr1.tsk1; // this should be an error as tsk1 is not exported
    //         input logic a;
    //         a = 1;
    //     endtask
    //     task intr1.tsk2; // this is an error as tsk1 is exported
    //         input logic a;
    //         a = 1;
    //     endtask
    // endmodule
    if (interface_id && !body && !int_subprog_id->IsExportTaskFunc() && int_subprog_id->IsImportTaskFunc()) {
        int_subprog_id->Error("task/function %s not exported/externed in interface %s", int_subprog_id->Name(), interface_id->Name()) ;
    }

    // FIXME : Check signature with prototype
    if (int_subprog_id->IsExternForkjoinTask()) {
        // VIPER #7069 : Extern fork-join task can have multiple bodies, add those
        int_subprog_id->AddExternForkjoinDef(this) ;
    } else {
        int_subprog_id->SetModuleItem(this) ;
    }
    name->SetId(int_subprog_id) ;
    VeriDataType *func_type = subprog_id->GetDataType() ;
    VeriMapForCopy old2new ;
    //if (func_type) int_subprog_id->SetDataType(func_type->CopyDataType(old2new)) ;
    if (func_type) {
        int_subprog_id->SetDataType(GetDataType()) ; // Set data type from this function body
        subprog_id->ClearDataType() ;
    }

    // Undeclare the subprogram identifier
    VeriScope *local_scope = GetScope() ;
    VeriScope *upper = (local_scope) ? local_scope->Upper() : 0 ;

    if (upper && upper->Undeclare(subprog_id)) {
        ClearId() ;
        delete subprog_id ;
    }
    if (local_scope) local_scope->SetOwner(int_subprog_id) ;
    // VIPER #5457: Set the local scope too, SetOwner does not set local scope:
    if (!int_subprog_id->IsExternForkjoinTask()) {
        // VIPER #7069 : Extern fork-join task can have multiple bodies, cannot add one scope
        int_subprog_id->SetLocalScope(local_scope) ;
    }
    return int_subprog_id ;
}
unsigned VeriFunctionDecl::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array *parent_pseudo_nodes, Map *mod_map_tab, Map * /*top_mod_nodes*/, Set * /*defparams*/)
{
    VeriIdDef *new_id = BindExportedSubprogram(_name, parent_pseudo_nodes) ;
    if (new_id) {
        _func = new_id ;
        // Mark the scope of this exported function. The identifier of this function is
        // actually  defined in interface and this body is inside a module. So now if
        // the body of this  function contains a reference to the function name, we cannot
        // resolve the reference as function identifier is not declared in the upper scope
        // of function. This is solved by this marking. In Find routine, if we encounter such
        // marked scope, then we will check the owner of the scope to find required id.
        if (_scope) _scope->SetExportedSubprogScope(1) ;
        Resolve(0, VERI_UNDEF_ENV) ;
    }

    // VIPER #5461 :Check for out-of-block method, do not try to process those. They will be
    // processed with class body.
    if (_name && _name->IsScopeName()) return 1 ;
    VeriPseudoTreeNode::ElaborateClassDataType(this, mod, mod_map_tab) ;
    return 1 ;
}
unsigned VeriTaskDecl::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array *parent_pseudo_nodes, Map *mod_map_tab, Map * /*top_mod_nodes*/, Set * /*defparams*/)
{
    VeriIdDef *new_id = BindExportedSubprogram(_name, parent_pseudo_nodes) ;
    if (new_id) {
        _task = new_id ;
        Resolve(0, VERI_UNDEF_ENV) ;
    }
    // VIPER #5461 :Check for out-of-block method, do not try to process those. They will be
    // processed with class body.
    if (_name && _name->IsScopeName()) return 1 ;
    VeriPseudoTreeNode::ElaborateClassDataType(this, mod, mod_map_tab) ;
    return 1 ;
}
unsigned VeriNetAlias::StaticElaborateModuleItem(VeriModuleItem * /*mod*/, VeriLibrary * /*lib*/, Array *parent_pseudo_nodes, Map * /*mod_map_tab*/, Map * /* top_mod_nodes */, Set * /*defparams*/)
{
    if (!_left || !_members) return 1 ; // Nothing to check
    // VIPER #2884 : Issue error for size mismatch
    verific_int64 lsize = _left->StaticSizeSignInternal(0, 0) ; // Get size of left expression
    verific_uint64 left_size = GET_CONTEXT_SIZE(lsize) ;

    // Iterate over members are issue error
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_members, i, expr) {
        if (!expr) continue ;
        verific_int64 rsize = expr->StaticSizeSignInternal(0, 0) ; // Get member size
        verific_uint64 right_size = GET_CONTEXT_SIZE(rsize) ;
        if (left_size && right_size && (left_size != right_size)) { // Size mismatch : issue error
            expr->Error("alias bit length %d differs from actual bit length %d", (unsigned)left_size, (unsigned)right_size) ;
            return 0 ;
        }
    }
    // VIPER #7962 : Change the reference of array instances in lvalue with
    // array instance elaboration created instances.
    VeriPseudoTreeNode::AddNetAlias(this, parent_pseudo_nodes) ;
    return 1 ;
}

// Elaborate generate. Add elaboration created module items in module item list
// of module/interface 'mod' and remove itself from parse tree
/* static */ unsigned VeriModuleItem::StaticElaborateModuleItemInternal(const Array *module_items, VeriModuleItem *generate, VeriModuleItem *mod, VeriLibrary *lib, const Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams)
{
    if (!generate) return 1 ;
    unsigned i ;
    VeriModuleItem *item ;
    // Create a local class object to store necessary information
    VeriElabGenerate gen_obj(generate, mod, lib) ;
    VeriScope *mod_scope = mod->GetScope() ;
    // VIPER #4382 :
    // Iterate over each generate items to elaborate each
    FOREACH_ARRAY_ITEM(module_items, i, item) {
        if (!item) continue ;
        item->StaticElaborateGenerate(&gen_obj, 0, mod->GetScope()) ;
    }

    // VIPER #2996: We need to set the data type of the modport ids that might be copied:
    //VeriMapForCopy *copy_obj = gen_obj.GetCopyObj() ;
    //if (copy_obj) copy_obj->ProcessModports(mod->GetScope(), mod) ;
    //ReResolveIds resolve_obj(copy_obj) ; FIXME : Is it needed here

    if (Message::ErrorCount()) return 0 ;

    // VIPER #4382 :
    VeriPseudoTreeNode *mod_node = 0 ;
    FOREACH_ARRAY_ITEM(parent_pseudo_nodes, i, mod_node) {
        if (mod_node) break ;
    }
    gen_obj.CheckDefparamValidity(mod_node, mod_map_tab, top_mod_nodes, defparams) ;

    Map *new_ids = gen_obj.GetNewIds() ;
    MapIter mi ;
    VeriIdDef *new_id ;
    unsigned status = 1 ;
    FOREACH_MAP_ITEM(new_ids, mi, 0, &new_id) {
        if (!mod_scope || !new_id) continue ;

        // VIPER #8114: Replace compile flag with runtime flag
        if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) {
            VeriIdDef *old_id = mod_scope->FindLocal(new_id->Name()) ;
            if (old_id) {
                if (gen_obj.IsIdOfThisGenerate(old_id)) {
                    (void) mod_scope->Undeclare(old_id) ;
                    VeriStaticElaborator::AddToDeleteBlkId(old_id) ;
                } else {
                    generate->Error("name conflict during elaboration for %s", old_id->Name()) ;
                    status = 0 ;
                }
            }
        }

        if (new_id->Name()) {
            if (!mod_scope->Declare(new_id)) {
                if (new_id->Name()) {
                    generate->Error("name conflict during elaboration for %s", new_id->Name()) ;
                    status = 0 ;
                }
            }
        } else {
            // unnamed instance id, force insert in scope
            if (!mod_scope->Declare(new_id, 1)) {
                // Collision allowed
            }
        }
    }
    gen_obj.ResetNewIds() ;
    // Add the elaborated items in module item list
    VeriModuleItem *new_item = 0 ;
    unsigned long copied_item ;
    Map *gen_obj_mod_items = 0 ;
    // VIPER #8114: Replace compile flag with runtime flag
    if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) {
        gen_obj_mod_items = gen_obj.GetElaboratedItems() ;
    } else {
        gen_obj_mod_items = gen_obj.GetNewModuleItems() ;
    }
    Array new_mod_items((gen_obj_mod_items) ? gen_obj_mod_items->Size() : 0) ;
    // If some generate items resides in module scope and we have to add that in
    // module item list after generate elaboration, those are not copied. Those
    // are first detached from existing parse tree and then those are added to module item list.
    FOREACH_MAP_ITEM(gen_obj_mod_items, mi, &new_item, &copied_item) {
        if (!new_item) continue ;
        if (!copied_item) gen_obj.Detach(new_item) ; // Detach from parse tree, it is not copied
        if (copied_item && (new_item->IsImportDecl() || new_item->IsExportDecl())) {
            // Import/export decl can be found inside generate. Need to add those
            // in module scope
            unsigned is_import_decl = new_item->IsImportDecl() ? 1 : 0 ;
            Array *import_items = new_item->GetItems() ;
            VeriName *import_item ; unsigned j ;
            FOREACH_ARRAY_ITEM(import_items, j, import_item) {
                VeriName *pkg_node = import_item ? import_item->GetPrefix(): 0 ;
                VeriIdDef *pkg_id = pkg_node ? pkg_node->GetId(): 0 ;
                const char *suffix_name = import_item ? import_item->GetSuffix(): 0 ;
                VeriScope *pkg_scope = pkg_id ? pkg_id->LocalScope(): 0 ;
                if (mod_scope) {
                    if (is_import_decl) {
                        mod_scope->AddImportItem(pkg_scope, suffix_name, import_item) ;
                    } else {
                        mod_scope->AddExportItem(pkg_scope, suffix_name, import_item) ;
                    }
                }
            }
        }
        // No need to set parent scope of this copied generate item to module scope, we already did that
        //VIPER 7482: Convert defparam with multiple assignments with multiple defparams with single assignment
        //            before replacing the generate statement with generated items
        Array *defparam_assigns = new_item->GetDefParamAssigns() ;
        if (defparam_assigns && (defparam_assigns->Size() > 1)) {
            new_item->ConvertToMultipleSingleItems(&new_mod_items,0) ;
            delete new_item ;
        } else {
            new_mod_items.Insert(new_item) ;
        }
        // VIPER #4128 : Re-resolve the id-refs to point iddefs to proper ids.
        // It is required for forward referencing of objects.
        //if (!copied_item) new_item->Accept(resolve_obj) ;
    }
    // Remove the generate construct from parse-tree and add elaborated generate items in its place
    (void) mod->ReplaceChildBy(generate, &new_mod_items) ;

    // Update or expand pseudo hierarchical tree by iterating over generate elaboration
    // created module items
    unsigned local_depth = VeriPseudoTreeNode::_depth ; // Store local depth
    VeriPseudoTreeNode *parent ;
    FOREACH_ARRAY_ITEM(parent_pseudo_nodes, i, parent) {
        if (!parent) continue ;
        if (!i)  { // All 'parent' nodes have same signature
            // Check recursion for one node. Now we are considering instantiations
            // inside generate in another phase, so set global depth to parent's depth
            // to reproduce its real depth in pseudo tree creation
            VeriPseudoTreeNode::_depth = parent->GetDepth() ;
        } else { // No need to reproduce exact picture for remaining parent's
            VeriPseudoTreeNode::_depth = local_depth ;
        }
        parent->UpdateTree(&new_mod_items, mod_map_tab, top_mod_nodes) ;
        if (!i) { // Before Updating tree we have increase the depth, reset that
            VeriPseudoTreeNode::_depth = local_depth ;
        }
        // Error in updating pseudo tree, elaborated items are already attached to
        // module, so reset 'gen_obj_mod_items' so that destructor of VeriElabGenerate
        // cannot delete those items
        if (Message::ErrorCount() && gen_obj_mod_items) gen_obj_mod_items->Reset() ;
    }
    return status ;
}

unsigned VeriGenerateConstruct::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams)
{
    return StaticElaborateModuleItemInternal(GetItems(), this, mod, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, defparams) ;
}

// VIPER #3855: Generate for, if and case in module as module item
unsigned VeriGenerateFor::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams)
{
    // Create a dummy generate-construct and elaborate that:
    Array gen_items(1) ;
    gen_items.InsertLast(this) ;
    return StaticElaborateModuleItemInternal(&gen_items, this, mod, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, defparams) ;
}

unsigned VeriGenerateConditional::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams)
{
    // Create a dummy generate-construct and elaborate that:
    Array gen_items(1) ;
    gen_items.InsertLast(this) ;
    return StaticElaborateModuleItemInternal(&gen_items, this, mod, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, defparams) ;
}

unsigned VeriGenerateCase::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams)
{
    // Create a dummy generate-construct and elaborate that:
    Array gen_items(1) ;
    gen_items.InsertLast(this) ;
    return StaticElaborateModuleItemInternal(&gen_items, this, mod, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, defparams) ;
}

unsigned VeriGenerateBlock::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary *lib, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set *defparams)
{
    // VIPER #8114: Replace compile flag with runtime flag
    if (_is_elaborated && RuntimeFlags::GetVar("veri_alternative_generate_elab")) {
        // Generate block can contain parameters and array instance identifiers.
        // Set evaluated value to parameters and split array instances to multiple single instances
        Set *nodes = mod_map_tab ? (Set*)mod_map_tab->GetValue(this): 0 ;
        SetIter si ;
        Array node_arr(nodes ? nodes->Size(): 1) ;
        VeriPseudoTreeNode *n ;
        VeriPseudoTreeNode *node = 0 ;
        FOREACH_SET_ITEM(nodes, si, &n) {
            if (n) node_arr.InsertLast(n) ;
            if (!node && n) node = n ;
        }
        if (node) node->ModifyParamAndAnsiPortDecl(mod_map_tab, &node_arr, top_mod_nodes, 0) ;

        // VIPER #6080 : If generate block is already elaborated, call 'StaticElaborateModuleItem'
        // on every item of generate block
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(_items, i, item) {
            if (item && !item->StaticElaborateModuleItem(mod ? mod : this, lib, &node_arr, mod_map_tab, top_mod_nodes, defparams)) {
                return 0 ;
            }
        }
        // Check whether defparams present inside generate block change parameter value outside of hierarchy
        // generated by this generate block
        HierIdWithDeclScope *id ;
        FOREACH_SET_ITEM(defparams, si, &id) {
            if (!id) continue ;
            VeriExpression *name_ref = id->GetNameRef() ;
            if (!name_ref) continue ;
            id->ResetMarkAsSpecial() ; // Mark this defparam as special
            id->SetDeclarationScope(_scope) ;

            // Try to resolve the defparam name
            name_ref->ResolveName(id, node, top_mod_nodes, mod_map_tab, 0) ;
            if (id->GetResolvedIddef()) {  // Get last resolved id
                name_ref->Error("defparam under generate scope cannot change parameter value outside of hierarchy") ;
            }
            (void) defparams->Remove(id) ;
            delete id ;
        }
        return 1 ;
    }

    // VIPER #5612: Create a dummy generate-construct and elaborate that:
    Array gen_items(1) ;
    gen_items.InsertLast(this) ;
    return StaticElaborateModuleItemInternal(&gen_items, this, mod, lib, parent_pseudo_nodes, mod_map_tab, top_mod_nodes, defparams) ;
}

// Elaborate if-generate. Evaluate condition and elaborate then/else path depending on
// condition value. Generate items inside if-generate are always copied as those are inside if-scope or else-scope
void  VeriGenerateConditional::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned /*is_cp_needed*/, VeriScope * /* active_scope*/)
{
    VeriBaseValue *cond_val = _if_expr ? _if_expr->StaticEvaluateInternal(0, gen_obj->GetValueTable(), 0, 0) : 0 ;

    unsigned b_cond = 1 ;
    // Evaluate the condition
    if (cond_val) {
        unsigned b_sign = 0 ;
        char *str = cond_val->GetBinaryValue(&b_sign) ;
        // If the value of the condition is zero, make the flag b_cond 0
        // VIPER #4364: If the value of the condition contains x or z, make it 0 too:
        if (!str || ConstStrEvaluation::StrIsZero(str) || ConstStrEvaluation::StrHasXZ(str)) b_cond = 0 ;
        // VIPER #4441: Adjustment for real values - set the flag if it is real and is non-zero:
        if (!b_cond && (cond_val->Type() == REAL) && (cond_val->GetRealValue())) b_cond = 1 ;
        Strings::free(str) ;
        delete cond_val ;
    } else {
        Error("constant expression required") ;
        b_cond = 0 ;
    }
    VeriModuleItem *stmt = 0 ;
    VeriScope *scope = 0 ;
    // If the condition of the if-generate is true, the true item will be elaborated
    // and false item will be ignored. Otherwise true statement will be ignored and
    // false statement will be elaborated
    if (b_cond) {
        // Condition is true, so true statement will be elaborated
        stmt = _then_item ;
        scope = _then_scope ;
    } else {
        stmt = _else_item ;
        scope = _else_scope ;
    }

    // Elaborate the proper item
    Array items(1) ;
    items.Insert(stmt) ;
    // VIPER #6186 : Store all branch owners to check name conflict for
    // elaboration created block ids with implicit block ids created in analyzer
    // VIPER #8114: Replace compile flag with runtime flag
    if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) {
        VeriScope *other_scope = (scope == _then_scope) ? _else_scope : _then_scope ;
        gen_obj->AddThisGenerateId(other_scope) ;
        // VIPER #8377 : If 'stmt' is not present, pass 'this' to set proper linefile
        // info to created generate block
        gen_obj->ElaborateCreatingBlock(scope ? scope->GetOwner(): 0, &items, scope, 1, scope, 0, 0, stmt ? stmt: this) ;
    } else {
        gen_obj->Elaborate(scope ? scope->GetOwner(): 0, &items, scope, 1, scope) ;
    }
}

// Evaluate case cond and case items condition. Depending on those condition particular
// branch is elaborate. Every branch resides in different scope, so generate items of
// branch to be executed are copied.
void VeriGenerateCase::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned /*is_cp_needed*/, VeriScope * /*active_scope*/)
{
    if (!gen_obj) return ;

    unsigned i, j ;
    VeriGenerateCaseItem *ci ;

    // VIPER #5819: Support for type (type operator and $typeof system function) case condition:
    if (_cond && (_cond->IsTypeOperator() || (_cond->GetFunctionType() == VERI_SYS_CALL_TYPEOF))) {
        unsigned done = 0 ;
        VeriBaseValue *val ;
        VeriExpression *expr ;
        VeriModuleItem *matched_stmt = 0 ;
        VeriScope *matched_stmt_scope = 0 ;
        Array branch_scopes(_case_items ? _case_items->Size(): 1) ;
        // Determine which case item matches the case condition:
        FOREACH_ARRAY_ITEM(_case_items, i, ci) {
            if (!ci) continue ;
            if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
                branch_scopes.InsertLast(ci->GetScope()) ;
                if (done) continue ;
            } else {
                if (done) break ;
            }
            if (ci->IsDefault()) {
                // Store the default statement, if none matches it will be executed:
                matched_stmt = ci->GetItem() ;
                matched_stmt_scope = ci->GetScope() ;
                continue ;
            }

            Array *cond_list = ci->GetConditions() ;
            // Traverse the case conditions of case item
            FOREACH_ARRAY_ITEM(cond_list, j, expr) {
                if (!expr) continue ;
                // Create a binary operator (automatic object, will get deleted automatically):
                VeriBinaryOperator cond_equl_expr(VERI_LOGEQ, _cond, expr) ;
                cond_equl_expr.SetLinefile(_cond->Linefile()) ;
                val = cond_equl_expr.StaticEvaluateInternal(0, 0, 0, 0) ;
                cond_equl_expr.SetLeft(0) ; cond_equl_expr.SetRight(0) ; // So that they don't get deleted
                if (val && val->GetIntegerValue()) {
                    // This item matches the case condition, it should be executed:
                    matched_stmt = ci->GetItem() ;
                    matched_stmt_scope = ci->GetScope() ;
                    delete val ;
                    done = 1 ;
                    break ;
                }
                delete val ;
            }
            //if (done) break ;
        }

        // Now, elaborate matched statement (it may be the default statement if none matches):
        if (matched_stmt) {
            Array items(1) ;
            items.Insert(matched_stmt) ;
            VeriIdDef *owner = (matched_stmt_scope) ? matched_stmt_scope->GetOwner() : 0 ;
            if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
                // VIPER #6186 : Store all branch owners to check name conflict for
                // elaboration created block ids with implicit block ids created in analyzer
                VeriScope *s ;
                FOREACH_ARRAY_ITEM(&branch_scopes, i, s) gen_obj->AddThisGenerateId(s) ;
                gen_obj->ElaborateCreatingBlock(owner, &items, matched_stmt_scope, 1, matched_stmt_scope, 0, 0, matched_stmt ? matched_stmt: this) ;
            } else {
                gen_obj->Elaborate(owner, &items, matched_stmt_scope, 1, matched_stmt_scope) ;
            }
        }

        return ; // Done
    }

    // Calculate the (Verilog) constraint size/sign of the expressions
    verific_int64 cond_size_sign = _cond ? _cond->StaticSizeSignInternal(gen_obj->GetValueTable(), 0): 0 ;
    verific_uint64 size = GET_CONTEXT_SIZE(cond_size_sign) ;
    unsigned sign = GET_CONTEXT_SIGN(cond_size_sign) ;
    VeriConstraint *cond_constraint = _cond ? _cond->EvaluateConstraintInternal(gen_obj->GetValueTable(), 0): 0 ;

    // LRM 9.5 : expression size is MAX of all conditions, case-expression
    // If all conditions are signed, then all conditions should be evaluated in sign context
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        if (!ci) continue ;
        Array *conditions = ci->GetConditions() ;
        VeriExpression *ci_cond ;
        FOREACH_ARRAY_ITEM(conditions, j, ci_cond) {
            if (!ci_cond) continue ;
            verific_int64 ci_cond_size_sign = ci_cond->StaticSizeSignInternal(gen_obj->GetValueTable(), cond_constraint) ;
            verific_uint64 ci_size = GET_CONTEXT_SIZE(ci_cond_size_sign) ;
            unsigned ci_sign = GET_CONTEXT_SIGN(ci_cond_size_sign) ;
            size = MAX(size,ci_size) ;
            if (!ci_sign) sign = 0 ;
        }
    }
    // So, evaluate condition with that size/sign info :
    int size_sign = MAKE_CONTEXT_SIZE_SIGN(size,sign) ;
    // Evaluate the case-condition
    VeriBaseValue *c_val = _cond ? _cond->StaticEvaluateInternal(size_sign, gen_obj->GetValueTable(), 0, 0): 0 ;
    if (!c_val) {
        Error("generate case expression is not constant") ;
        delete cond_constraint ;
        return ;
    }

    // Adjust this value to the max size of case condition and case item conditions:
    c_val = c_val->Adjust((unsigned)size, 0 /* sign does not matter -> */, 1 /* use self sign */, 0 /* not two state */) ;

    VeriBaseValue *val ;
    VeriModuleItem *default_stmt = 0 ;
    VeriScope *default_stmt_scope = 0 ;
    // Array containing the case condition in string form
    Array expr_array(2) ;
    // Array of case statements that will be elaborated when the corresponding expression
    // matches with case condition.
    Array stmt_array(2) ;
    // Array containing the scopes of each case item
    Array scope_array(2) ;

    // Traverse case items
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        if (!ci) continue ;
        VeriModuleItem *stmt = ci->GetItem() ;
        if (ci->IsDefault()) {
            // Store the default statement
            default_stmt = stmt ;
            default_stmt_scope = ci->GetScope() ;
            continue ;
        }
        Array *new_cond_list = ci->GetConditions() ;
        VeriExpression *expr ;
        // Traverse the case conditions of case item
        FOREACH_ARRAY_ITEM(new_cond_list, j, expr) {
            if (!expr) continue ;
            // Evaluate the condition
            val = expr->StaticEvaluateInternal(size_sign, gen_obj->GetValueTable(), cond_constraint, 0) ;
            if (!val) {
                expr->Error("generate case item condition is not constant") ;
                continue ;
            }

            // Adjust the value to the max size of case condition and case item conditions:
            val = val->Adjust((unsigned)size, 0 /* sign does not matter -> */, 1 /* use self sign */, 0 /* not two state */) ;

            // Populate arrays with appropriate values
            expr_array.InsertLast(val) ;
            stmt_array.InsertLast(stmt) ;
            scope_array.Insert(ci->GetScope()) ;
        }
    }
    delete cond_constraint ;

    unsigned b_matched = 0 ;
    // Traverse the conditions of case items
    FOREACH_ARRAY_ITEM(&expr_array, i, val) {
        if (!val) continue ;

        // VIPER #4441: Compare case condition with condition of case item in real domain for
        // real values. For other values compare using binary string - IsEqual checks type too.
        // So, it cannot match 32'b0 (based number) with 0 (integer). For real it works fine.
        unsigned equal = 0 ;
        if ((c_val->Type() == REAL) || (val->Type() == REAL)) {
            // CHECKME: Cannot do real comparison, use fabs and compare with a tolerance:
            //equal = (c_val->GetRealValue() == val->GetRealValue()) ? 1 : 0 ;
            equal = (fabs(c_val->GetRealValue() - val->GetRealValue()) < 0.0001) ? 1 : 0 ;
        } else {
            unsigned c_sign ;
            char *c_str = c_val->GetBinaryValue(&c_sign) ;
            char *ci_str = val->GetBinaryValue(&c_sign) ;
            equal = Strings::compare(c_str, ci_str) ;
            Strings::free(c_str) ;
            Strings::free(ci_str) ;
        }
        if (equal) {
            // Condition matches, corresponding statement is elaborated
            VeriModuleItem *stmt = (VeriModuleItem*)stmt_array.At(i) ;
            VeriScope *branch_scope = (VeriScope*)scope_array.At(i) ;
            Array items(1) ;
            items.Insert(stmt) ;
            if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
                // VIPER #6186 : Store all branch owners to check name conflict for
                // elaboration created block ids with implicit block ids created in analyzer
                VeriScope *s ;
                FOREACH_ARRAY_ITEM(&scope_array, j, s) gen_obj->AddThisGenerateId(s) ;
                gen_obj->AddThisGenerateId(default_stmt_scope) ;
                gen_obj->ElaborateCreatingBlock(branch_scope ? branch_scope->GetOwner(): 0, &items, branch_scope, 1, branch_scope, 0, 0, stmt ? stmt: this) ;
            } else {
                gen_obj->Elaborate(branch_scope ? branch_scope->GetOwner(): 0, &items, branch_scope, 1, branch_scope) ;
            }
            b_matched = 1 ;
            break ;
        }
    }
    // No conditions of case items matches with case condition. Default statement will be elaborated
    if ((!b_matched) && (default_stmt)) {
        Array items(1) ;
        items.Insert(default_stmt) ;
        if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
            // VIPER #6186 : Store all branch owners to check name conflict for
            // elaboration created block ids with implicit block ids created in analyzer
            VeriScope *s ;
            FOREACH_ARRAY_ITEM(&scope_array, j, s) gen_obj->AddThisGenerateId(s) ;
            gen_obj->ElaborateCreatingBlock(default_stmt_scope ? default_stmt_scope->GetOwner(): 0, &items, default_stmt_scope, 1, default_stmt_scope, 0, 0, default_stmt ? default_stmt : this) ;
        } else {
            gen_obj->Elaborate(default_stmt_scope ? default_stmt_scope->GetOwner(): 0, &items, default_stmt_scope, 1, default_stmt_scope) ;
        }
    }

    // Delete created array elements
    FOREACH_ARRAY_ITEM(&expr_array, i, val) delete val ;
    delete c_val ;
}

//
// To elaborate the for-generate statement, the initial, final and increment value of
// loop index variable are evaluated. Depending on those values for-loop items are
// elaborated multiple times each time with new prefix name.
void VeriGenerateFor::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned /*is_cp_needed*/, VeriScope * /*active_scope*/)
{
    if (!gen_obj) return ;

    ValueTable *value_table = gen_obj->GetValueTable() ;
    // Get loop index variable for simple assignment
    VeriIdDef *gen_id = (_initial) ? _initial->GetId() : 0 ;
    // VIPER #3523: Check whether this id has already initialized:
    if (gen_id && value_table && value_table->FetchValue(gen_id)) {
        // So, we are now inside a nested for generate loop with the same index, error out:
        Error("same genvar %s cannot control this nested for generate loop", gen_id->Name()) ;
        return ; // Do not process this nested loop
    }

    // Evaluate initial expression
    if (_initial) (void) _initial->StaticElaborateStmt(value_table, 0) ;

    // Initial is genvar declaration
    if (!gen_id && _initial && _initial->IsGenVarDecl()) {
        Array *ids = _initial->GetIds() ;
        gen_id = (ids && ids->Size()) ? (VeriIdDef*)ids->GetFirst() : 0 ;
    }

    // VIPER #8109 : Find the impicitly defined localparam and set value
    // of genvar to that
    VeriIdDef *local_param_id = (_scope && gen_id) ? _scope->FindLocal(gen_id->Name()): 0 ;

    // VIPER : 2947/4152 - honor user loop count, if set by user
    unsigned user_loop_limit = veri_file::GetLoopLimit() ;
    unsigned loop_limit = user_loop_limit ? user_loop_limit : LOOPLIMIT ;

    int b_true = 0 ;
    int runner = 0 ;
    unsigned loopcount = 0 ;

    for(;;) {
        if (loopcount++ == loop_limit) {
            if (_cond) _cond->Error("loop count limit of %d exceeded; condition is never false", loop_limit) ;
            break ;
        }

        b_true = 0 ;
        // Examine final condition to determine if the loop statements will be elaborated for current loop index value

        // Evaluate the final condition
        VeriBaseValue * final_value = _cond ? _cond->StaticEvaluateInternal(0, gen_obj->GetValueTable(), 0, 0) : 0 ;
        if (!final_value) {
            // Final expression cannot be evaluated.  Elaboration failed.
            Error("non-static generate-for loop. Unroll failed") ;
            break ;
        }
        // FIXME: If 'final_value' is real we should round it instead of truncating it:
        b_true = final_value->GetIntegerValue() ;
        delete final_value ;
        final_value = 0 ;

        if (b_true) {
            // Loop statements will be elaborated
            MapItem *ele = gen_obj->GetIdTab()->GetItem(gen_id) ;
            VeriMapForCopy *gen_copy_obj = gen_obj->GetCopyObj() ;
            VeriIdDef *copied_gen_var = gen_copy_obj ? (VeriIdDef*)gen_copy_obj->GetValue(gen_id) : 0 ;
            if (ele) {
                // Delete the previous value of loop index variable
                VeriExpression *pre_value = (VeriExpression*)ele->Value() ;
                delete pre_value ;
            }
            // Get the value of loop index variable
            VeriBaseValue *gen_var_val = value_table ? value_table->FetchValue(gen_id): 0 ;
            if (!gen_var_val) {
                Error("non-static generate-for loop. Unroll failed") ;
                break ;
            }

            runner = gen_var_val->GetIntegerValue() ; // get loop index var value
            // Insert new value of loop index variable
            VeriIntVal *runner_val = new VeriIntVal(runner) ;
            runner_val->SetLinefile((gen_id) ? gen_id->Linefile() : 0) ;
            (void) gen_obj->GetIdTab()->Insert(gen_id, runner_val, 1) ;
            if (copied_gen_var) {
                VeriIntVal *copied_runner_val = new VeriIntVal(runner) ;
                copied_runner_val->SetLinefile(copied_gen_var->Linefile()) ;
                (void) gen_obj->GetIdTab()->Insert(copied_gen_var, copied_runner_val, 1) ;
            }
            // VIPER #8109 : If implicit localparam is created, set current value of genvar to that id
            if (local_param_id) {
                MapItem *lele = gen_obj->GetIdTab()->GetItem(local_param_id) ;
                VeriIdDef *copied_local_param = gen_copy_obj ? (VeriIdDef*)gen_copy_obj->GetValue(local_param_id) : 0 ;
                if (lele) {
                    VeriExpression *pre_value = (VeriExpression*)lele->Value() ; delete pre_value ;
                }
                VeriIntVal *copied_val = new VeriIntVal(runner) ;
                (void) gen_obj->GetIdTab()->Insert(local_param_id, copied_val, 1) ;
                if (copied_local_param) {
                    VeriIntVal *copied_val2 = new VeriIntVal(runner) ;
                    (void) gen_obj->GetIdTab()->Insert(copied_local_param, copied_val2, 1) ;
                }
            }
            //char num[10] ;
            //num[0] = '\0' ;
            //sprintf(num, "%d", runner) ;
            //char *new_loop_idx_val ;
            // Create new loop index with the loop index value for this iteration. This loop index
            // will be used to create the new name of the objects declared within the block within for-generate
            //new_loop_idx_val = Strings::save(_block_id ? _block_id->Name(): "", "[", num, "]") ;
            if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
                gen_obj->ElaborateCreatingBlock(_block_id, _items, _scope, 1, _scope, copied_gen_var ? copied_gen_var: gen_id, gen_var_val, this, local_param_id) ;
            } else {
                char *new_loop_idx_val ;
                // VIPER #4437 : Use API to create name from block identifier and loop index value
                new_loop_idx_val = gen_obj->CreateBlockName(_block_id, gen_var_val) ;
                gen_obj->PushNamedPath(new_loop_idx_val) ;
                gen_obj->Elaborate(0, _items, _scope, 1, _scope, local_param_id) ;
                gen_obj->PopNamedPath() ;
                Strings::free(new_loop_idx_val) ;
            }
        } else {
            break ;
        }

        if (_rep) (void) _rep->StaticElaborateStmt(gen_obj->GetValueTable(), 0) ;
    }

    // Remove the value of current genvar from the table, it should not be available later:
    if (value_table) (void) value_table->RemoveValue(gen_id) ; // Remove and delete the value
    // VIPER #8321 : Remove the genvar id from id table, it should not be available
    // later :
    MapItem *ele = gen_obj->GetIdTab()->GetItem(gen_id) ;
    if (ele) {
        // Delete the previous value of loop index variable
        VeriExpression *pre_value = (VeriExpression*)ele->Value() ;
        delete pre_value ;
        (void) gen_obj->GetIdTab()->Remove(gen_id) ;
    }
}

// Elaborate generate block
void VeriGenerateBlock::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope)
{
    if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
        gen_obj->ElaborateCreatingBlock(_block_id, _items, _scope, _scope ? 1 : is_cp_needed, _scope ? _scope : active_scope, 0, 0, this) ;
    } else {
        gen_obj->Elaborate(_block_id, _items, _scope, _scope ? 1 : is_cp_needed, _scope ? _scope : active_scope) ;
    }
}

// This is a generate item. Depending on 'is_cp_needed' flag it is either copied or taken as it is. Convert the use of genvars with their proper constant value.
void VeriDataDecl::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope)
{
    // We will ignore genvar declaration, as after generate elaboration
    // no genvar declaration should exist in parse tree
    if (_decl_type == VERI_GENVAR) return ;

    // Call base's routine to copy this data declaration
    VeriModuleItem::StaticElaborateGenerate(gen_obj, is_cp_needed, active_scope) ;
}

// Convert comma separated instantiations to multiple single instantiations. Single instantiations are inserted in argument specific array
void VeriModuleInstantiation::BreakInstantiationAndPopulateArray(Array *list)
{
    unsigned i;
    if(_insts.Size() < 2) {
        list->InsertLast(this);
        return;
    }
    VeriModuleInstantiation *new_inst;
    VeriInstId *inst ;
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (!inst) continue;

        // Second or later instance : Create separate module instantiation for this
        // Detach this instance id from 'this' to connect at new instance
        _insts.Insert(i, 0); // Remove from original list.

        // Copy parameter assoc map
        VeriMapForCopy copy_map(POINTER_HASH);
        Array *param_array = CopyExpressionArray(_param_values, copy_map) ;

        // Create new separate module instantiation
        new_inst = new VeriModuleInstantiation(
            (_module_name ? _module_name->CopyName(copy_map) : 0),
            (_strength ? _strength->Copy(copy_map) : 0),
            param_array, inst, 0) ;
        // Duplicate the the library back-pointer and the linefile info.
        new_inst->SetLinefile(Linefile());
        new_inst->SetLibrary(_library) ;
        // Set the new instantiation as a backpointer on the instance id :
        inst->SetModuleInstance(new_inst) ;

        // Insert this in the list :
        list->InsertLast(new_inst);
    }
}

// Convert comma separated instantiations to multiple single instantiations. Single instantiations are inserted in argument specific array
void VeriGateInstantiation::BreakInstantiationAndPopulateArray(Array *list)
{
    unsigned i;
    if((!_insts) || (_insts->Size() < 2)) {
        list->InsertLast(this);
        return;
    }
    VeriGateInstantiation *new_inst;
    Array* new_inst_list;
    VeriInstId *inst ;
    VeriMapForCopy copy_map ;
    FOREACH_ARRAY_ITEM(_insts, i, inst) {
        if (!inst) continue;

        // Second or later instance : Create separate module instantiation for this
        // Start by moveing the instance to a new array :
        new_inst_list = new Array(1);
        new_inst_list->Insert(inst);
        _insts->Insert(i, 0); // Remove from original list.

        // Create new separate module instantiation
        new_inst = new VeriGateInstantiation(_inst_type,
            (_strength ? _strength->Copy(copy_map) : 0),
            (_delay ? CopyExpressionArray(_delay, copy_map) : 0) ,
            new_inst_list);
        new_inst->SetLinefile(Linefile());

        // Insert this in the list :
        list->InsertLast(new_inst);
    }
}
///////////////////////////////////////////////////////////////////////////////
// Convert comma separated module items to multiple single module items. Single
// items will be inserted in argument specified array and also in parent module
///////////////////////////////////////////////////////////////////////////////
void VeriModuleInstantiation::ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container)
{
    if (!list) return ; // No place to insert broken items, do nothing
    // Convert comma separated instantiation to multiple single instantiation
    BreakInstantiationAndPopulateArray(list) ;

    if (!container) return ; // No container to add multiple single instances
    if (list->Size() > 1) (void) container->ReplaceChildBy(this, list) ;
}

void VeriGateInstantiation::ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container)
{
    if (!list) return ; // No place to insert broken items, do nothing
    // Convert comma separated instantiation to multiple single instantiation
    BreakInstantiationAndPopulateArray(list) ;

    if (!container) return ; // No container to add multiple single instances
    if (list->Size() > 1) (void) container->ReplaceChildBy(this, list) ;
}
void VeriDefParam::ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container)
{
    if (!list) return ; // No place to insert broken items, do nothing

    if ((!_defparam_assigns) || (_defparam_assigns->Size() < 2)) {
        list->InsertLast(this) ; // Single defparam assignment exists, nothing to convert
        return ;
    }
    // Multiple defparam assignments exists
    VeriDefParam *new_defparam ;
    unsigned i ;
    VeriDefParamAssign *assign ;
    FOREACH_ARRAY_ITEM(_defparam_assigns, i, assign) {
        if (!assign) continue ;

        // Detach this assignment for this list
        _defparam_assigns->Insert(i, 0) ;
        // Create new array and add assignment in that
        Array *new_list = new Array(1) ;
        new_list->InsertLast(assign) ;
        new_defparam = new VeriDefParam(new_list) ; // Create new defparam
        new_defparam->SetLinefile(Linefile()) ;

        // Insert this in the list
        list->InsertLast(new_defparam) ;
    }
    if (!container) return ; // No container to add multiple single instances
    if (list->Size() > 1) (void) container->ReplaceChildBy(this, list) ;
}
void VeriBindDirective::ConvertToMultipleSingleItems(Array *list, VeriModuleItem *container)
{
    if (!list) return ; // No place to insert broken items, do nothing

    if ((!_target_inst_list) || (_target_inst_list->Size() < 2)) {
        list->InsertLast(this) ; // Single instance, no need to do anything
        return ;
    }

    // Contains multiple Target instances. Break this construct so that every single
    // item contains only one target instance
    unsigned i ;
    VeriBindDirective *new_directive ;
    VeriExpression *target_instance ;
    FOREACH_ARRAY_ITEM(_target_inst_list, i, target_instance) {
        if (!target_instance) continue ;
        // Detach target instance from list
        _target_inst_list->Insert(i, 0) ;
        // Create new array and add detached target instance
        Array *new_target_list = new Array(1) ;
        new_target_list->InsertLast(target_instance) ;

        // Create new scope from old one
        VeriMapForCopy old2new ;
        VeriScope *new_scope = _scope ? _scope->Copy(old2new) : 0 ;

        // Copy instantiation
        VeriModuleItem *new_instance = _instantiation ? _instantiation->CopyModuleItem(old2new) : 0 ;

        // Create new bind directive
        new_directive = new VeriBindDirective(_target_mod_name ? Strings::save(_target_mod_name) : 0, new_target_list, new_instance, new_scope) ;
        new_directive->SetLinefile(Linefile()) ;

        // VIPER #7986 : Update identifier's initial-value/port-connection list etc
        if (new_scope) new_scope->UpdateScope(_scope, old2new) ;

        // Add created directive in list
        list->InsertLast(new_directive) ;
    }
    if (!container) return ; // No container to add multiple single instances
    if (list->Size() > 1) (void) container->ReplaceChildBy(this, list) ;
}

// Depending on 'is_cp_needed' flag this generate item is either copied or taken as it is. Convert the use of genvars with their proper constant value.
void VeriModuleItem::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope)
{
    if (!gen_obj) return ;
    (void) StaticElaborateGenerateInternal(gen_obj, is_cp_needed, active_scope) ;
}
void VeriModuleInstantiation::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope)
{
    // VIPER #5984 : Expand .* port connections for instances those will not be copied
    if (!is_cp_needed) ElaboratePortConnections(active_scope) ;
    VeriModuleItem::StaticElaborateGenerate(gen_obj, is_cp_needed, active_scope) ;
}
VeriModuleItem* VeriModuleItem::StaticElaborateGenerateInternal(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope)
{
    VeriModuleItem *new_node = 0 ;
    VeriMapForCopy *gen_copy_obj = gen_obj->GetCopyObj() ;
    if (is_cp_needed && gen_copy_obj) {
        // Item is within if/for generate. So copy it
        // First insert the 'module scope' against 'active_scope' in the Map,
        // so that they are automatically switched in the copied module item:
        (void) gen_copy_obj->Insert(active_scope, gen_obj->GetModScope()) ;
        new_node = CopyModuleItem(*(gen_copy_obj)) ;
    } else {
        // Item is not within if/for generate. Do not copy it
        new_node = this ;
        // Switch the upper scope of all the children of the active scope to the module scope:
        VeriScope *mod_scope = gen_obj->GetModScope() ; // Get the module scope
        // Get the children of the active scope:
        Set *children = (active_scope) ? active_scope->GetChildrenScopes() : 0 ;
        SetIter si ;
        VeriScope *child ;
        FOREACH_SET_ITEM(children, si, &child) {
            // Set the upper scope to module scope:
            if (child) child->SetUpper(mod_scope) ;
        }
        // Replace the genvar with its proper value and hierarchical identifiers are added in an array maintained by this class
        // VIPER #7986 : Replace genvar only for non-copied object. Change in genvar
        // for copied objects will be performed after coping initial-value/port-connection/dims etc
        gen_obj->BindGenStmt(new_node) ;
    }

    // Insert the copied item in the array.
    gen_obj->AddNewModuleItem(new_node, (is_cp_needed ? 1L : 0L)) ;

    // Replace the genvar with its proper value and hierarchical identifiers are added in an array maintained by this class
    //gen_obj->BindGenStmt(new_node) ;

    return new_node ;
}
void VeriDefParam::StaticElaborateGenerate(VeriElabGenerate *gen_obj, unsigned is_cp_needed, VeriScope *active_scope)
{
    VeriModuleItem *copied_defparam = StaticElaborateGenerateInternal(gen_obj, is_cp_needed, active_scope) ;

    // VIPER #4382 :
    unsigned i ;
    VeriDefParamAssign *assign ;
    VeriName *lval ;
    VeriExpression *rval ;
    Array id_list(_defparam_assigns ? _defparam_assigns->Size(): 1) ;
    FOREACH_ARRAY_ITEM(_defparam_assigns, i, assign) {
        lval = assign ? assign->GetLVal(): 0 ;
        rval = assign ? assign->GetRVal(): 0 ;

        VeriMapForCopy old2new ;
        // Create identifier for defparams and store those in argument specified set
        DefparamId *id = new DefparamId(lval, active_scope, rval? rval->CopyExpression(old2new):0, this) ;
        id->SetInsideGenerate() ;
        id_list.InsertLast(id) ;
    }
    if (id_list.Size()) gen_obj->AddDefparam(&id_list, copied_defparam) ;
}
///////////////////////////////////////////////////////////////////////////////////
//                   Detach module item from parse tree                          //
///////////////////////////////////////////////////////////////////////////////////
// Detach the argument specific module item from its container.  This method is
// implemented for generate construct. If any module item (except if/case/for
// generate) resides directly in generate construct, in static elaboration
// that module item is not copied. It is detached
// from its parent and added in the module item list.
void VeriElabGenerate::Detach(VeriModuleItem *node)
{
    if (!_gen || !node) return ;
    if (!_gen->Detach(node)) VERIFIC_ASSERT(0) ;
}
unsigned VeriGenerateConstruct::Detach(VeriModuleItem *node)
{
    unsigned i ;
    VeriModuleItem *gen_item ;
    // Traverse gen item list
    FOREACH_ARRAY_ITEM(_items, i, gen_item) {
        if (!gen_item) continue ;
        if (gen_item == node) {
            // item found, insert null in its position
            _items->Insert(i, 0) ;
            return 1 ;
        }
    }
    return 0 ;
}

VeriElabGenerate::VeriElabGenerate(VeriModuleItem *gen, const VeriModuleItem *module, VeriLibrary *lib) :
    _gen(gen),
    _mod_scope(0),
    _new_mod_items(POINTER_HASH),
    _tmp_new_mod_items(0),
    _id_tab(0),
    _library(lib),
    _named_path(0),
    _val_table(0),
    _copy_obj(0),
    _defparams(0),
    _new_ids(0)
    // VIPER #6080 :
    ,_elaborated_items(0)
    ,_current_blk_scope(module->GetScope())
    ,_gen_ids(0)
{
    _mod_scope = module->GetScope() ;
    _id_tab = new Map(POINTER_HASH) ;
    _val_table = new ValueTable() ;
    _copy_obj = new VeriMapForCopy(POINTER_HASH) ;
}

VeriElabGenerate::~VeriElabGenerate()
{
    MapIter mi ;
    VeriExpression *data ;
    FOREACH_MAP_ITEM(_id_tab, mi, 0, &data) delete data ;
    delete _id_tab ;
    if (Message::ErrorCount()) { // Copied module items are not attached, delete those
        VeriModuleItem *mod_item ;
        unsigned long copied_item ;
        FOREACH_MAP_ITEM(&_new_mod_items, mi, &mod_item, &copied_item) {
            if (!copied_item) continue ;
            delete mod_item ;
        }
    }
    delete _tmp_new_mod_items ;
    _gen = 0 ;
    _mod_scope = 0 ;
    _library = 0 ;
    delete _named_path ;
    delete _val_table ;
    delete _copy_obj ;
    DefparamId *id ;
    FOREACH_MAP_ITEM(_defparams, mi, 0, &id) delete id ;
    delete _defparams ;
    VeriIdDef *copied_id ;
    FOREACH_MAP_ITEM(_new_ids, mi, 0, &copied_id) delete copied_id ;
    delete _new_ids ;

    // VIPER #6080 :
    delete _elaborated_items ;
    _current_blk_scope = 0 ;
    delete _gen_ids ;
    _gen_ids = 0 ;
}

void VeriElabGenerate::AddNewModuleItem(const VeriModuleItem *new_item, unsigned long copied_item)
{
    if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
        if (!_elaborated_items) _elaborated_items = new Map(POINTER_HASH) ;
        (void) _elaborated_items->Insert(new_item, (void*)copied_item) ;
    } else {
        (void) _new_mod_items.Insert(new_item, (void*)copied_item) ;
    }
    if (_tmp_new_mod_items) _tmp_new_mod_items->InsertLast(new_item) ;
}

void VeriElabGenerate::PushNamedPath(const char *name)
{
    if (!_named_path) _named_path = new Array() ;
    _named_path->Insert((void*)name) ; // store just the const pointer..
}
void VeriElabGenerate::PopNamedPath()
{
    VERIFIC_ASSERT(_named_path && _named_path->Size()) ;
    (void) _named_path->RemoveLast() ;
    if (!_named_path->Size()) {
        delete _named_path ;
        _named_path = 0 ;
    }
}
char *VeriElabGenerate::CreateFullName(const char *short_name, const char *sep) const
{
    if (!short_name) return 0 ;

    if (!sep) sep = "." ;
    // If there is a named path, create an allocated string with the path suffixed to the 'short_name'.
    // If there is no named path, return 0 (instead of an allocated string), so that the user
    // can use the short name. (saves unnecessary string duplications).

    // Used to create unique names for objects inside blocks in Verilog.  We use '.' as separator
    // for block-hierarchy naming. Should be OK.
    unsigned i ;
    char *full_name = 0 ;
    const char *fragment ;
    FOREACH_ARRAY_ITEM_BACK(_named_path,i,fragment) {
        if (full_name) {
            char *tmp = full_name ;
            full_name = Strings::save(fragment,sep,full_name) ;
            Strings::free(tmp) ;
        } else {
            full_name = Strings::save(fragment) ;
        }
    }
    if (full_name) {
        char *tmp = full_name ;
        full_name = Strings::save(full_name, sep, short_name) ;
        Strings::free(tmp) ;
    } else {
        full_name = Strings::save(short_name) ;
    }
    return full_name ;
}
void VeriElabGenerate::AddThisGenerateId(const VeriScope *s)
{
    if (!s) return ;
    if (!_gen_ids) _gen_ids = new Set(POINTER_HASH) ;
    VeriIdDef *id = s->GetOwner() ;
    if (id) (void) _gen_ids->Insert(id) ;

    // Consider its children also, as for branch like
    // if (1)
    // begin
    //     ......
    // block id will be created when CREATE_UNNAMED_BLOCK flag is on and will be
    // declared in module scope
    Set *children = s->GetChildrenScopes() ;
    SetIter si ;
    VeriScope *scope ;
    FOREACH_SET_ITEM(children, si, &scope) {
         AddThisGenerateId(scope) ;
    }
}
void VeriElabGenerate::ElaborateCreatingBlock(const VeriIdDef *blk_id, const Array *blk_items, const VeriScope *blk_scope, unsigned is_cp_needed, VeriScope *active_scope, const VeriIdDef *gen_id, const VeriBaseValue *loop_val, const VeriTreeNode *from, const VeriIdDef *localparam_id)
{
    if (blk_id) {
        // 'blk_id' is present in generate constructs, store them so that we can
        // check later the name conflicts due to new block id creation
        AddThisGenerateId(blk_scope) ;
    }
    char *loop_index = 0 ;
    if (loop_val) {
        int v = loop_val->GetIntegerValue() ;
        loop_index = Strings::itoa(v) ;
    }
    // If blk_id exists, we need to create a new generate block :
    // 1) Store already elaborated items in a local variable
    // 2) Create new block id and declare that in upper scope (FIXME )of blk_scope
    // 3) Create new _copy_obj (storing old one in a local var) and add
    //    old_block_id vs new_blk_id and old_scope vs new_scope
    // 4) Copy the identifiers declared within 'blk_scope'
    VeriMapForCopy *old_copy_obj = _copy_obj ; // store old iddefs association
    Map *old_elaborated_items = _elaborated_items ; // store old elaborated items
    VeriScope *old_current_block_scope = _current_blk_scope ;
    VeriScope *new_blk_scope = active_scope ;
    VeriIdDef *block_id = 0 ;

    Array *save_tmp_new_mod_items = _tmp_new_mod_items ;
    _tmp_new_mod_items = 0 ;
    if (blk_id || loop_index || blk_scope) {
        // Need to create a new generate block
        const char *block_name = 0 ;
        _copy_obj = new VeriMapForCopy(POINTER_HASH) ;
        // VIPER #4128 : Create an array to store copied module items of this iteration :
        _tmp_new_mod_items = new Array(2) ;
        //_tmp_new_mod_items->Reset() ;
        if (blk_id) {
            block_name = blk_id->Name() ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
            if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
                // If name is like genblk1_0 and name is for unnamed generate block,
                // remove the suffix like _0 from the name before pushing in named path
                const char *modified_name = blk_id->GetOrigName() ;
                if (modified_name) block_name = modified_name ;
            }
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        }
        // If 'loop_index' exists, we should always create a new block id.
        // If 'loop_index' does not exists, new block id for this block may
        // be already created by upper block
        // begin : b1   // while elaborating blk 'b1', 'nested' is copied
        //    begin : nested
        if (!loop_index && block_name) {
            block_id = _current_blk_scope ? _current_blk_scope->FindLocal(block_name):0 ;
            // If blk_id is already copied by upper block, it should be present
            // in _copy_obj, find that
            if (!old_copy_obj || (old_copy_obj->GetValue(blk_id) != block_id)) block_id = 0 ;
        }
        // If this block identifier is not already copied, copy that
        if (!block_id && (block_name || loop_index)) {
            char *new_name = 0 ;
            if (blk_id && loop_index) {
                new_name = Strings::save(block_name, "[", loop_index, "]") ;
            } else if (loop_index) {
                new_name = Strings::save("[", loop_index, "]") ;
            } else if (blk_id) {
                new_name = Strings::save(block_name) ;
            }
            block_id = new VeriBlockId(new_name) ;
            block_id->SetLinefile(blk_id ? blk_id->Linefile(): (from ? from->Linefile():0)) ; // VIPER #6185 : Set linefile
            // VIPER #8337 : Add attribute to mark generate elaboration created ids.
            // FIXME: Can use bit-flag to mark such identifiers
            Map* attr_decl_map = new Map(STRING_HASH) ;
            (void) attr_decl_map->Insert(Strings::save(" gen_elab_created"), new VeriIntVal(1)) ;
            block_id->AddAttributes(attr_decl_map) ;

            // Block id with same name is already present in container scope
            VeriIdDef *curr_block_owner = (_current_blk_scope) ? _current_blk_scope->GetOwner(): 0 ;
            if (curr_block_owner && curr_block_owner->IsModule()) {
                // If created block ids are to be declared in module scope, check whether
                // id with same name is already created by elaboration. If so produce error :
                if (IsCreatedPreviously(new_name)) {
                    if (from) from->Error("name conflict during elaboration for %s", block_id->Name()) ;
                } else {
                    AddNewId(block_id) ; // Store in a list to declare later
                }
            } else if (_current_blk_scope && !_current_blk_scope->DeclareBlockId(block_id)) {
                if (from) from->Error("name conflict during elaboration for %s", block_id->Name()) ;
            }
            // VIPER #6710 : Do not insert block ids created for for-loop into '_copy_obj', so
            // that any reference of for-loop block id inside generate cannot be replaced by new id
            if (!loop_index) (void) _copy_obj->Insert(blk_id, block_id, 1) ;
            //(void) _copy_obj->Insert(blk_id, block_id, 1) ;
        }

        if (old_copy_obj) {
            // This generate block is within another block and the identifiers declared
            // within that block can be used within this block. So in the new class object of
            // VeriMapForCopy, the identifiers of upper block are also
            MapIter mi ;
            VeriIdDef *key ; VeriIdDef *value ;
            FOREACH_MAP_ITEM(old_copy_obj, mi, &key, &value) (void) _copy_obj->Insert(key, value, 1) ;
        }
        // Create a new scope for this elaborated block
        if (blk_id || loop_index) {
            new_blk_scope = new VeriScope(_current_blk_scope, block_id) ;
            _current_blk_scope = new_blk_scope ;
            _elaborated_items = 0 ;
#ifdef VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
            // Create localparam for this block
            CreateLocalParam(gen_id, loop_val, from, localparam_id) ;
#else
            (void) gen_id ;
#endif
        } else {
            new_blk_scope = _current_blk_scope ;
        }
        // Copy the objects declared :
        if (!PopulateIdTable(blk_scope, new_blk_scope)) {
            delete _copy_obj ;
            _copy_obj = old_copy_obj ;
            Strings::free(loop_index) ;
            return ;
        }
        // VIPER #8109 : Find the copied localparam id and set value to that id
        if (localparam_id) {
            VeriIdDef *copied_id = (VeriIdDef*)_copy_obj->GetValue(localparam_id) ;
            VeriExpression *param_val = (VeriExpression*)GetIdTab()->GetValue(localparam_id) ;
            if (param_val && copied_id) (void) GetIdTab()->Insert(copied_id, param_val->CopyExpression(*_copy_obj), 1) ;
        }
    }
    // Now elaborate items :
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(blk_items, i, item) {
        if (!item) continue ;
        item->StaticElaborateGenerate(this, blk_scope ? 1: is_cp_needed, active_scope) ;
    }
    if (blk_id || loop_index || blk_scope) {
        (void) UpdateIds(blk_scope, new_blk_scope) ;
    }
    if (_copy_obj != old_copy_obj) { // TO DO : Make a function to avoid code repetation
        // VIPER #2996: Copy the array of copied modport into the old copy object.
        // Some of the items may be delcared later and only after processing that
        // we can set the data type of the modport port ids of these modports:
        if (old_copy_obj && _copy_obj) {
            Array *modports = _copy_obj->GetModports() ;
            VeriModport *modport ;
            FOREACH_ARRAY_ITEM(modports, i, modport) old_copy_obj->AddModport(modport) ;
        }
        // VIPER #4128 : Some identifiers of inner scope may be copied here and
        // those identifiers can be referred before declaration in assignment
        // pattern and tagged union using forward type ref.
        // typedef my_type ;
        // my_type s = '{ a: 4} ; // 'a' object of inner scope used before decl
        // typedef struct {int a;} my_type ;

        // So after copying the module items try to catch all idrefs and change
        // its iddef using map-for-copy.
        ReResolveIds obj(_copy_obj) ;
        // '_tmp_new_mod_items' holds the items copied in this iteration :
        FOREACH_ARRAY_ITEM(_tmp_new_mod_items, i, item) {
            if (item) BindGenStmt(item) ; // VIPER #7986
            if (item) item->Accept(obj) ;
        }
        delete _copy_obj ;
        _copy_obj = old_copy_obj ;
        if (blk_id || loop_index) { // It is from named block
            Array *new_items = new Array(_elaborated_items ? _elaborated_items->Size(): 2) ;
            MapIter mi ;
            FOREACH_MAP_ITEM(_elaborated_items, mi, &item, 0) {
                if (item) new_items->InsertLast(item) ;
            }
            // Create a generate block with elaborated items :
            VeriGenerateBlock *b = new VeriGenerateBlock(block_id, new_items, new_blk_scope) ;
            b->SetLinefile(from ? from->Linefile():0) ; // VIPER #6185 : Set linefile
            b->SetIsElaborated() ;
            // Set _elaborated_items to previous value, as those are already taken
            delete _elaborated_items ;
            _elaborated_items = old_elaborated_items ;
            // Add the new generate block in old _elaborated_items
            if (!_elaborated_items) _elaborated_items = new Map(POINTER_HASH) ;
            (void) _elaborated_items->Insert(b, (void*)1L) ;
            _current_blk_scope = old_current_block_scope ;
        }
    }
    delete _tmp_new_mod_items ;
    _tmp_new_mod_items = save_tmp_new_mod_items ; // Pop
    Strings::free(loop_index) ;
}
#ifdef VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
void VeriElabGenerate::CreateLocalParam(const VeriIdDef *gen_id, const VeriBaseValue *loop_val, const VeriTreeNode *from, const VeriIdDef *local_param_id)
{
    if (!gen_id || !loop_val) return ;

    // Create a localparam with the same name as genvar
    VeriIdDef *local_param = new VeriParamId(Strings::save(gen_id->Name())) ;
    local_param->SetLinefile(gen_id->Linefile()) ; // VIPER #6185 : Set linefile
    if (_copy_obj) (void) _copy_obj->Insert(gen_id, local_param, 1) ;
    if (_copy_obj && local_param_id) (void) _copy_obj->Insert(local_param_id, local_param, 1) ;

    // Create initial value of this localparameter from 'loop_val'
    //VeriExpression *init_val = loop_val->ToExpression(gen_id) ;
    VeriExpression *init_val = new VeriIntVal(loop_val->GetIntegerValue()) ;
    init_val->SetLinefile(from ? from->Linefile(): 0) ; // VIPER #6185 : Set linefile
    local_param->SetInitialValue(init_val) ;

    // Declare this localparameter in current generate block
    if (_current_blk_scope) (void) _current_blk_scope->Declare(local_param) ;

    // Create declaration :
    Array *id_list = new Array(1) ;
    id_list->InsertLast(local_param) ;

    VeriDataDecl *decl = new VeriDataDecl(VERI_LOCALPARAM, 0, id_list) ;
    decl->SetLinefile(from ? from->Linefile(): 0) ; // VIPER #6185 : Set linefile

    // Add this in elaborated item list
    AddNewModuleItem(decl, 1L) ;
}
#endif

// Routine to elaborate generate block items. Create new name prefix for this block.
void  VeriElabGenerate::Elaborate(const VeriIdDef *blk_id, const Array *blk_items, const VeriScope *blk_scope, unsigned is_cp_needed, VeriScope *active_scope, const VeriIdDef *localparam_id)
{
    VeriMapForCopy *old_copy_obj = _copy_obj ; // store old iddefs association

    if (blk_id) {
        const char *block_name = blk_id->Name() ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
            // If name is like genblk1_0 and name is for unnamed generate block,
            // remove the suffix like _0 from the name before pushing in named path
            const char *modified_name = blk_id->GetOrigName() ;
            if (modified_name) block_name = modified_name ;
        }
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
        // The name of the block identifier will contribute to the new name of the objects declared within the block
        PushNamedPath(block_name) ;
    }
    unsigned i ;
    VeriModuleItem *item ;
    // VIPER #5984 : Before creating identifiers for this generate block and
    // elaborating its item, expand .* presents in port connection list of
    // instantiations. It should be done before block elaboration
    // Elaborate block items
    FOREACH_ARRAY_ITEM(blk_items, i, item) {
        if (!item) continue ;
        item->ElaboratePortConnections(active_scope) ;
    }
    Array *save_tmp_new_mod_items = _tmp_new_mod_items ;
    _tmp_new_mod_items = 0 ;
    if (blk_scope) {
        // Object to store old iddefs vs new identifiers created for this generate block
        // elaboration. Old iddefs get new name <blk_name>.<id_name>. It is passed to copy
        // block items so that copied block items obtain new identifiers
        _copy_obj = new VeriMapForCopy(POINTER_HASH) ;
        // VIPER #4128 : Create an array to store copied module items of this iteration :
        _tmp_new_mod_items = new Array(2) ;
        //_tmp_new_mod_items->Reset() ;
        if (old_copy_obj) {
            // This generate block is within another block and the identifiers declared
            // within that block can be used within this block. So in the new class object of
            // VeriMapForCopy, the identifiers of upper block are also inserted.
            MapIter mi ;
            VeriIdDef *key ; VeriIdDef *value ;
            FOREACH_MAP_ITEM(old_copy_obj, mi, &key, &value) (void) _copy_obj->Insert(key, value, 1) ;
        }
        // Copy the declared objects of this block with new name and the copied objects are added in module scope.
        if (!PopulateIdTable(blk_scope)) {
            delete _copy_obj ;
            _copy_obj = old_copy_obj ;
            delete _tmp_new_mod_items ; // Memory leak fix
            _tmp_new_mod_items = save_tmp_new_mod_items ; // Pop saved array
            return ;
        }
        // VIPER #8109 : Find the copied localparam id and set value to that id
        if (localparam_id) {
            VeriIdDef *copied_id = (VeriIdDef*)_copy_obj->GetValue(localparam_id) ;
            VeriExpression *param_val = (VeriExpression*)GetIdTab()->GetValue(localparam_id) ;
            if (param_val && copied_id) (void) GetIdTab()->Insert(copied_id, param_val->CopyExpression(*_copy_obj), 1) ;
        }
    }
    //unsigned i ;
    //VeriModuleItem *item ;
    // Elaborate block items
    FOREACH_ARRAY_ITEM(blk_items, i, item) {
        if (!item) continue ;
        item->StaticElaborateGenerate(this, blk_scope ? 1: is_cp_needed, active_scope) ;
    }
    if (blk_scope) (void) UpdateIds(blk_scope) ; // VIPER #7986
    if (blk_id) PopNamedPath() ;
    if (_copy_obj != old_copy_obj) {
        // VIPER #2996: Copy the array of copied modport into the old copy object.
        // Some of the items may be delcared later and only after processing that
        // we can set the data type of the modport port ids of these modports:
        if (old_copy_obj && _copy_obj) {
            Array *modports = _copy_obj->GetModports() ;
            VeriModport *modport ;
            FOREACH_ARRAY_ITEM(modports, i, modport) old_copy_obj->AddModport(modport) ;
        }
        // VIPER #4128 : Some identifiers of inner scope may be copied here and
        // those identifiers can be referred before declaration in assignment
        // pattern and tagged union using forward type ref.
        // typedef my_type ;
        // my_type s = '{ a: 4} ; // 'a' object of inner scope used before decl
        // typedef struct {int a;} my_type ;

        // So after copying the module items try to catch all idrefs and change
        // its iddef using map-for-copy.
        ReResolveIds obj(_copy_obj) ;
        // '_tmp_new_mod_items' holds the items copied in this iteration :
        FOREACH_ARRAY_ITEM(_tmp_new_mod_items, i, item) {
            if (item) BindGenStmt(item) ; // VIPER #7986
            if (item) item->Accept(obj) ;
        }
        //if (_tmp_new_mod_items) _tmp_new_mod_items->Reset() ; // Make it ready for next iteration
        delete _copy_obj ;
        _copy_obj = old_copy_obj ;
    }
    delete _tmp_new_mod_items ;
    _tmp_new_mod_items = save_tmp_new_mod_items ; // Pop saved array
}

// Each object declared within the second argument specific scope is copied and its name is changed.
// Changed name is obtained from block identifier name and loop index value of for-generate statement.
unsigned VeriElabGenerate::PopulateIdTable(const VeriScope *blk_scope, VeriScope *new_scope)
{
    if (!blk_scope) return 1 ;
    (void) new_scope ;
    Map *blk_scope_id_tab = blk_scope->GetThisScope() ;
    MapIter mi ;
    VeriIdDef *id ;
    // Traverse each identifiers
    if (!_copy_obj) return 0 ;

    if (RuntimeFlags::GetVar("veri_alternative_generate_elab")) { // VIPER #8114: Replace compile flag with runtime flag
        if (!new_scope) return 0 ;
        FOREACH_MAP_ITEM(blk_scope_id_tab, mi, 0, &id) {
            // VIPER #6186 : Do not create new iddef for unnamed generate block ids
            if (!id || id->IsGenVar() || id->IsUnnamedBlock()) continue ; // VIPER #2074: Do not create new iddef for genvar
#ifdef VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
            // VIPER #8109 : Ignore implicitly declared localparam. We have already created one for this block
            if (id->IsImplicitLocalParam()) continue ;
#endif
            const char *id_name = id->Name() ;
            if (id_name) {
                // Hier name and VeriLoop constructor created iddef has ' '. Here we ignore the VeriLoop
                // created block id. If the first occurred space has no prefix, we can consider it as
                // constructor created block id
                const char *first_space = strchr(id_name, ' ') ;
                if (first_space && Strings::compare(first_space, id_name)) continue ;
            }
            // Copy the identifier. Use the VeriMapForCopy which is used to copy generate items
            // VIPER #8109: Copy implicit localparam with its initial value, as reference of those
            // identifiers can be replaced (for nested blocks) before calling 'UpdateIds' for this block
            VeriIdDef *new_id = id->CopyId(*_copy_obj, (id->IsImplicitLocalParam()?1:0)) ;
            if (id_name) {
                if (!new_scope->Declare(new_id)) {
                    id->Error("illegal duplicate name created during elaboration, multiple declarations created for '%s'", id_name) ;
                    delete new_id ;
                    return 0 ;
                }
            } else {
                (void) new_scope->Declare(new_id, 1 /* force insert*/) ; // ids for unnamed instances
            }

            // Insert the copied identifier in the object required for generate item copying.
            (void) _copy_obj->Insert(id, new_id, 1) ;
        }
    } else {
        FOREACH_MAP_ITEM(blk_scope_id_tab, mi, 0, &id) {
            if (!id || id->IsGenVar()) continue ; // VIPER #2074: Do not create new iddef for genvar
            char *name = 0 ;
            const char *id_name = id->Name() ;
            if (id_name) { // Create new name for named id, just copy unnamed
                // Hier name and VeriLoop constructor created iddef has ' '. Here we ignore the VeriLoop
                // created block id. If the first occurred space has no prefix, we can consider it as
                // constructor created block id
                const char *first_space = strchr(id_name, ' ') ;
                if (first_space && Strings::compare(first_space, id_name)) continue ;
                // Create new name of the identifier
                name = CreateFullName(id_name) ;

                if (_mod_scope && (_mod_scope->FindLocal(name) || (IsCreatedPreviously(name)))) {
                    // If object of new name is already in module scope, then we error out
                    id->Error("illegal duplicate name created during elaboration, multiple declarations created for '%s'", name) ;
                    Strings::free(name) ;
                    return 0;
                }
            }

            // Copy the identifier. Use the VeriMapForCopy which is used to copy generate items
            // VIPER #6782 : Donot copy initial value while copying iddef. Initial
            // value will be copied later.
            // VIPER #8109: Copy implicit localparam with its initial value, as reference of those
            // identifiers can be replaced (for nested blocks) before calling 'UpdateIds' for this block
            VeriIdDef *new_id = id->CopyId(*_copy_obj, id->IsImplicitLocalParam()?1:0) ;

            if (id->IsInst() && id_name) {
                char *path_name = CreateFullName(id_name, ":" /* separator */) ;
                VeriPseudoTreeNode::AddPathName(new_id, path_name) ; // Absorbs path_name
            }
            // Change the name of the identifier
            if (name) (void) new_id->ChangeName(name) ;

            // Add copied identifier to module scope
            //if (_mod_scope) (void) _mod_scope->Declare(new_id, 1) ;
            AddNewId(new_id) ; // VIPER #4382

            if (id_name) {
                // VIPER #8337 : Add attribute to mark generate elaboration created ids.
                // FIXME: Can use bit-flag to mark such identifiers
                Map* attr_decl_map = new Map(STRING_HASH) ;
                (void) attr_decl_map->Insert(Strings::save(" gen_elab_created"), new VeriIntVal(1)) ;
                new_id->AddAttributes(attr_decl_map) ;
            }
            // VIPER #2074: Do not allow genvars to be used other than loop index.
            /*if (id->IsGenVar()) {
                // Genvar can be used as other than loop index. So set old value of genvar as value of copied genvar
                VeriExpression *old_node = (VeriExpression*)GetIdTab()->GetValue(id) ;

                // Insert the copied identifier in the Map VeriIdDef*->VeriExpression
                (void) GetIdTab()->Insert(new_id, (old_node ? old_node->CopyExpression(*_copy_obj) : 0), 1) ;
            }*/

            // Insert the copied identifier in the object required for generate item copying.
            (void) _copy_obj->Insert(id, new_id, 1) ;
        }
    }
    return 1 ;
}
unsigned VeriElabGenerate::UpdateIds(const VeriScope *blk_scope, VeriScope *new_scope)
{
    if (!blk_scope) return 1 ;
    (void) new_scope ;
    Map *blk_scope_id_tab = blk_scope->GetThisScope() ;
    MapIter mi ;
    VeriIdDef *id ;
    // Traverse each identifiers
    if (!_copy_obj) return 0 ;
    // Initial value, unpacked dimension and port connects of identifiers can use locally declared identifiers
    // which remains after the identifier to copy in the scope. In that case initial value
    // or port connects can be copied with old identifiers. To switch those to new identifiers
    // traverse all copied identifiers after coping all identifiers and copy initial value and port connects.
    FOREACH_MAP_ITEM(blk_scope_id_tab, mi, 0, &id) {
        if (!id) continue ;
#ifdef VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
        if (id->IsImplicitLocalParam()) continue ; // Ignore implicit local param for this compile flag
#endif
        VeriIdDef *new_id = (VeriIdDef*)_copy_obj->GetValue(id) ;
        if (!new_id) continue ;
        VeriExpression *init_val = id->GetInitialValue() ;
        if (init_val) {
            init_val = init_val->CopyExpression(*_copy_obj) ;
            new_id->SetInitialValue(init_val) ;
        }
        VeriRange *unpacked_dim = id->GetDimensions() ;
        if (unpacked_dim) {
            unpacked_dim = unpacked_dim->CopyRange(*_copy_obj) ;
            new_id->SetMemoryRange(unpacked_dim) ;
        }
        if (id->IsNamedPort()) {
            new_id->CopyPortExprFrom(id, *_copy_obj) ;
        }
        if (!new_id->IsInst()) continue ;
        new_id->CopyPortConnectsFrom(id, *_copy_obj) ;
    }
    return  1 ;
}
void VeriElabGenerate::AddDefparam(const Array *id_list, const VeriModuleItem *df)
{
    if (!id_list) return ;
    if (!_defparams) _defparams = new Map(POINTER_HASH) ;
    DefparamId *old_one = (DefparamId*)_defparams->GetValue(df) ;
    unsigned i ;
    DefparamId *id ;
    if (old_one) {
        // Delete new one
        FOREACH_ARRAY_ITEM(id_list, i, id) delete id ;
        return ;
    }
    FOREACH_ARRAY_ITEM(id_list, i, id) {
        if (id) (void) _defparams->Insert(df, id, 0, 1) ;
    }
}
/////////////////////////////////////////////////////////////////////////////////////
//////           Replace use of genvar with its constant value                 //////
/////////////////////////////////////////////////////////////////////////////////////

// After elaborating each items of generate construct this routine is called to replace
// genvar identifiers with their appropriate value and to add hierarchical identifiers
// in an array maintained by this class.
void VeriElabGenerate::BindGenStmt(VeriTreeNode *node)
{
    ReplaceGenVarFromStmt obj(this) ;
    if (node) node->Accept(obj) ;
}

ReplaceGenVarFromStmt::ReplaceGenVarFromStmt(VeriElabGenerate *gen_obj) :
    VeriVisitor(),
    _gen_obj(gen_obj)
{ }

VeriExpression *ReplaceGenVarFromStmt::ToCurrGenvarValue(VeriExpression *expr)
{
    if (!expr) return 0 ;
    VeriExpression *ret_expr = 0 ;
    VeriIdDef *genvar_id = 0 ;
    // VIPER #2974 : Indexing can be performed on genvar and we should replace that
    // with proper value. So here we will consider VeriIndexedId and VeriIdRef
    if (expr->GetIndexExpr()) { // Bit/part select
        // Check if select is on genvar
        VeriName *prefix = expr->GetPrefix() ;
        genvar_id = (prefix) ? prefix->FullId() : 0 ; // Get prefix id
    } else {
        genvar_id = expr->FullId() ;
    }
    if (genvar_id && (genvar_id->IsGenVar() || genvar_id->IsImplicitLocalParam())) {
        // Get the current value of the genvar identifier
        ret_expr = (VeriExpression*)_gen_obj->GetIdTab()->GetValue(genvar_id) ;
        if (ret_expr) {
#ifndef VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
            if (expr->FullId()) {
                // For simple identifier reference, argument specified 'expr' should
                // be replaced by current value of genvar.
                VeriMapForCopy copy_map ;
                ret_expr = ret_expr->CopyExpression(copy_map) ;
            } else { // Select expression. Need to evaluate it
                // VIPER #6095 : if prefix of index expression is genvar, try to replace whole
                // indexed expression with its evaluated value
                if (expr->GetIndexExpr()) {
                    // Try to replace index expression first, may contain genvar
                    VeriExpression *idx = expr->GetIndexExpr() ;
                    VeriExpression *new_idx_expr = ToCurrGenvarValue(idx) ;
                    if (new_idx_expr) {
                        (void) expr->ReplaceChildExprInternal(expr->GetIndexExpr(), new_idx_expr, 1) ;
                    } else {
                        // It may be range or complex index expression
                        if (idx) idx->Accept(*this) ;
                    }
                }
                VeriExpression *store_ret_expr = ret_expr ;
                //VeriBaseValue *genval = ret_expr->StaticEvaluateInternal(0, 0, 0, 0) ; // Evaluate genvar value to get its value form
                //ValueTable val_tab ; // Create value table
                //if (!val_tab.Insert(genvar_id, genval)) { // Insert current value in valuetable
                    //delete genval ;
                    //return 0 ;
                //}
                //delete genval ;
                //genval = expr->StaticEvaluateInternal(0, &val_tab, 0, 0) ;
                VeriBaseValue *genval = expr->StaticEvaluateInternal(0, _gen_obj->GetValueTable(), 0, 0) ;
                //int int_val = (genval) ? genval->GetIntegerValue() : 0 ; // Get integer value
                //delete genval ;
                //ret_expr = new VeriIntVal(int_val) ;
                // VIPER #5021 : Create expression looking at the value
                ret_expr = (genval) ? genval->ToExpression(expr): 0 ;
                delete genval ;
                if (expr->GetIndexExpr() && !ret_expr) {
                    // VIPER #6095 :It is expression like 'genvar[non_const_index]', produce error
                    expr->Error("cannot replace genvar expression like genvar_ref[non_const_index]") ;
                    VeriMapForCopy old2new ;
                    ret_expr = store_ret_expr->CopyExpression(old2new) ;
                }
            }
#else
            VeriMapForCopy copy_map ;
            ret_expr = ret_expr->CopyExpression(copy_map) ;

#endif
        } else {
            // VIPER #2074: If uninitialized genvar then assign 1 to it following standard simulator
            expr->Warning("invalid context for genvar %s", genvar_id->Name());
            ret_expr = new VeriIntVal(1) ;
        }
        if (ret_expr) ret_expr->SetLinefile(expr->Linefile()) ;
    }

    if (ret_expr && expr->NamedFormal()) ret_expr = ret_expr->GetNameExtendedExpression(Strings::save(expr->NamedFormal())) ;

    return ret_expr ;
}

void ReplaceGenVarFromStmt::VERI_VISIT(VeriIndexedId, id_ref)
{
    VeriExpression *new_expr = ToCurrGenvarValue(id_ref.GetIndexExpr()) ;
    if (new_expr) {
        (void) id_ref.ReplaceChildExprInternal(id_ref.GetIndexExpr(), new_expr, 1) ;
    } else {
        if (id_ref.GetIndexExpr()) id_ref.GetIndexExpr()->Accept(*this) ;
    }
    VeriName *id_prefix = id_ref.GetPrefix() ;
    if (id_prefix) id_prefix->Accept(*this) ;
}

void ReplaceGenVarFromStmt::VERI_VISIT(VeriIndexedMemoryId, id_ref)
{
    unsigned i ;
    VeriExpression *idx_expr, *new_expr ;
    Array *id_ref_indexes = id_ref.GetIndexes() ;
    FOREACH_ARRAY_ITEM(id_ref_indexes, i, idx_expr) {
        new_expr = ToCurrGenvarValue(idx_expr) ;
        if (new_expr) {
            (void) id_ref.ReplaceChildExprInternal(idx_expr, new_expr, 1) ;
        } else {
            if (idx_expr) idx_expr->Accept(*this) ;
        }
    }
    VeriName *id_prefix = id_ref.GetPrefix() ;
    if (id_prefix) id_prefix->Accept(*this) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriConcat, concat)
{
    unsigned i ;
    VeriExpression *expr, *new_expr ;
    Array *concat_exprs = concat.GetExpressions() ;
    FOREACH_ARRAY_ITEM(concat_exprs, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) concat.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriConcat, concat) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriMultiConcat, mult_concat)
{
    VeriExpression *new_expr = ToCurrGenvarValue(mult_concat.GetRepeat()) ;
    if (new_expr) (void) mult_concat.ReplaceChildExprInternal(mult_concat.GetRepeat(), new_expr, 1) ;
    unsigned i ;
    VeriExpression *expr ;
    Array *mult_concat_exprs = mult_concat.GetExpressions() ;
    FOREACH_ARRAY_ITEM(mult_concat_exprs, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) mult_concat.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriMultiConcat, mult_concat) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriFunctionCall, func_call)
{
    unsigned i ;
    VeriExpression *expr, *new_expr ;
    Array *func_call_args = func_call.GetArgs() ;
    FOREACH_ARRAY_ITEM(func_call_args, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) func_call.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriFunctionCall, func_call) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriSystemFunctionCall, func_call)
{
    unsigned i ;
    VeriExpression *expr, *new_expr ;
    Array *func_call_args = func_call.GetArgs() ;
    FOREACH_ARRAY_ITEM(func_call_args, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) func_call.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriSystemFunctionCall, func_call) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriMinTypMaxExpr, mtm)
{
    VeriExpression *new_expr ;
    new_expr = ToCurrGenvarValue(mtm.GetMinExpr()) ;
    if (new_expr) (void) mtm.ReplaceChildExprInternal(mtm.GetMinExpr(), new_expr, 1) ;

    new_expr = ToCurrGenvarValue(mtm.GetTypExpr()) ;
    if (new_expr) (void) mtm.ReplaceChildExprInternal(mtm.GetTypExpr(), new_expr, 1) ;

    new_expr = ToCurrGenvarValue(mtm.GetMaxExpr()) ;
    if (new_expr) (void) mtm.ReplaceChildExprInternal(mtm.GetMaxExpr(), new_expr, 1) ;

    VeriVisitor::VERI_VISIT_NODE(VeriMinTypMaxExpr, mtm) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriUnaryOperator, u_op)
{
    VeriExpression *new_expr = ToCurrGenvarValue(u_op.GetArg()) ;
    if (new_expr) (void) u_op.ReplaceChildExprInternal(u_op.GetArg(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriUnaryOperator, u_op) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriBinaryOperator, b_op)
{
    VeriExpression *new_expr = ToCurrGenvarValue(b_op.GetLeft()) ;
    if (new_expr) (void) b_op.ReplaceChildExprInternal(b_op.GetLeft(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(b_op.GetRight()) ;
    if (new_expr) (void) b_op.ReplaceChildExprInternal(b_op.GetRight(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriBinaryOperator, b_op) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriQuestionColon, qc)
{
    VeriExpression *new_expr = ToCurrGenvarValue(qc.GetIfExpr()) ;
    if (new_expr) (void) qc.ReplaceChildExprInternal(qc.GetIfExpr(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(qc.GetThenExpr()) ;
    if (new_expr) (void) qc.ReplaceChildExprInternal(qc.GetThenExpr(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(qc.GetElseExpr()) ;
    if (new_expr) (void) qc.ReplaceChildExprInternal(qc.GetElseExpr(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriQuestionColon, qc) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriEventExpression, event_expr)
{
    VeriExpression *new_expr = ToCurrGenvarValue(event_expr.GetExpr()) ;
    if (new_expr) (void) event_expr.ReplaceChildExprInternal(event_expr.GetExpr(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(event_expr.GetIffCondition()) ;
    if (new_expr) (void) event_expr.ReplaceChildExprInternal(event_expr.GetIffCondition(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriEventExpression, event_expr) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriPortConnect, port_conn)
{
    VeriExpression *new_expr = ToCurrGenvarValue(port_conn.GetConnection()) ;
    if (new_expr) (void) port_conn.ReplaceChildExprInternal(port_conn.GetConnection(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriPortConnect, port_conn) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriRange, range)
{
    VeriExpression *new_expr = ToCurrGenvarValue(range.GetLeft()) ;
    if (new_expr) (void) range.ReplaceChildExprInternal(range.GetLeft(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(range.GetRight()) ;
    if (new_expr) (void) range.ReplaceChildExprInternal(range.GetRight(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(range.GetNext()) ;
    if (new_expr) (void) range.ReplaceChildExprInternal(range.GetNext(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriRange, range) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriScopeName, name)
{
    Array *param_values = name.GetParamValues() ;
    unsigned i ;
    VeriExpression *expr, *new_expr ;
    FOREACH_ARRAY_ITEM(param_values, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) name.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriScopeName, name) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriDefParam, defparam)
{
    unsigned i ;
    VeriDefParamAssign *defparam_assign ;
    Array *defparam_assigns = defparam.GetDefParamAssigns() ;
    FOREACH_ARRAY_ITEM(defparam_assigns, i, defparam_assign) {
        if (!defparam_assign) continue ;
        VeriExpression *init_val = defparam_assign->GetRVal() ;
        VeriExpression *new_expr = ToCurrGenvarValue(init_val) ;
        if (new_expr) (void) defparam_assign->ReplaceChildExprInternal(init_val, new_expr, 1) ;
        defparam_assign->GetRVal()->Accept(*this) ;
        VeriExpression *lhs = defparam_assign->GetLVal() ;
        if (lhs) lhs->Accept(*this) ;
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriContinuousAssign, cont_assign)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *cont_assign_delays = cont_assign.GetDelay() ;
    FOREACH_ARRAY_ITEM(cont_assign_delays, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) cont_assign.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriContinuousAssign, cont_assign) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriGateInstantiation, inst)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *inst_delay = inst.GetDelay() ;
    FOREACH_ARRAY_ITEM(inst_delay, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) inst.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriGateInstantiation, inst) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriModuleInstantiation, inst)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *inst_params = inst.GetParamValues() ;
    FOREACH_ARRAY_ITEM(inst_params, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) inst.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriModuleInstantiation, inst) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriNetRegAssign, net_assign)
{
    VeriExpression *new_expr = ToCurrGenvarValue(net_assign.GetRValExpr()) ;
    if (new_expr) (void) net_assign.ReplaceChildExprInternal(net_assign.GetRValExpr(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriNetRegAssign, net_assign) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriInstId, inst)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *inst_port_connects = inst.GetPortConnects() ;
    FOREACH_ARRAY_ITEM(inst_port_connects, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) inst.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriInstId, inst) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriCaseItem, case_item)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *case_item_conds = case_item.GetConditions() ;
    FOREACH_ARRAY_ITEM(case_item_conds, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) case_item.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriCaseItem, case_item) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriDelayOrEventControl, control)
{
    VeriExpression *new_expr = ToCurrGenvarValue(control.GetDelayControl()) ;
    if (new_expr) (void) control.ReplaceChildExprInternal(control.GetDelayControl(), new_expr, 1) ;
    new_expr = ToCurrGenvarValue(control.GetRepeatEvent()) ;
    if (new_expr) (void) control.ReplaceChildExprInternal(control.GetRepeatEvent(), new_expr, 1) ;
    unsigned i ;
    VeriExpression *expr ;
    Array *event_controls = control.GetEventControl() ;
    FOREACH_ARRAY_ITEM(event_controls, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) control.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriDelayOrEventControl, control) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriParamId, id_def)
{
    VeriExpression *new_expr = ToCurrGenvarValue(id_def.GetInitialValue()) ;
    if (new_expr) {
        id_def.SetInitialValue(new_expr) ;
    } else if (id_def.GetInitialValue()) {
        id_def.GetInitialValue()->Accept(*this) ;
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriVariable, id_def)
{
    VeriExpression *new_expr = ToCurrGenvarValue(id_def.GetInitialValue()) ;
    if (new_expr) {
        id_def.SetInitialValue(new_expr) ;
    } else if (id_def.GetInitialValue()) {
        id_def.GetInitialValue()->Accept(*this) ;
    }
    VeriRange *dimensions = id_def.GetDimensions() ;
    if (dimensions) {
        dimensions->Accept(*this) ;
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriBlockingAssign, assign)
{
    VeriExpression *new_expr = ToCurrGenvarValue(assign.GetValue()) ;
    if (new_expr) (void) assign.ReplaceChildExprInternal(assign.GetValue(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriBlockingAssign, assign) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriNonBlockingAssign, assign)
{
    VeriExpression *new_expr = ToCurrGenvarValue(assign.GetValue()) ;
    if (new_expr) (void) assign.ReplaceChildExprInternal(assign.GetValue(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriNonBlockingAssign, assign) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriTaskEnable, task_call)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *task_call_args = task_call.GetArgs() ;
    FOREACH_ARRAY_ITEM(task_call_args, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) task_call.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriTaskEnable, task_call) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriSystemTaskEnable, task_call)
{
    unsigned i ;
    VeriExpression *new_expr, *expr ;
    Array *task_call_args = task_call.GetArgs() ;
    FOREACH_ARRAY_ITEM(task_call_args, i, expr) {
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) (void) task_call.ReplaceChildExprInternal(expr, new_expr, 1) ;
    }
    VeriVisitor::VERI_VISIT_NODE(VeriSystemTaskEnable, task_call) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriDelayControlStatement, delay_stmt)
{
    VeriExpression *new_expr = ToCurrGenvarValue(delay_stmt.GetDelay()) ;
    if (new_expr) (void) delay_stmt.ReplaceChildExprInternal(delay_stmt.GetDelay(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriDelayControlStatement, delay_stmt) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriConditionalStatement, if_stmt)
{
    VeriExpression *new_expr = ToCurrGenvarValue(if_stmt.GetIfExpr()) ;
    if (new_expr) (void) if_stmt.ReplaceChildExprInternal(if_stmt.GetIfExpr(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriConditionalStatement, if_stmt) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriCaseStatement, case_stmt)
{
    VeriExpression *new_expr = ToCurrGenvarValue(case_stmt.GetCondition()) ;
    if (new_expr) (void) case_stmt.ReplaceChildExprInternal(case_stmt.GetCondition(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriCaseStatement, case_stmt) ;
}

void ReplaceGenVarFromStmt::VERI_VISIT(VeriRepeat, loop)
{
    VeriExpression *new_expr = ToCurrGenvarValue(loop.GetCondition()) ;
    if (new_expr) (void) loop.ReplaceChildExprInternal(loop.GetCondition(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriRepeat, loop) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriWhile, loop)
{
    VeriExpression *new_expr = ToCurrGenvarValue(loop.GetCondition()) ;
    if (new_expr) (void) loop.ReplaceChildExprInternal(loop.GetCondition(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriWhile, loop) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriFor, loop)
{
    VeriExpression *new_expr = ToCurrGenvarValue(loop.GetCondition()) ;
    if (new_expr) (void) loop.ReplaceChildExprInternal(loop.GetCondition(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriFor, loop) ;
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriWait, wait)
{
    VeriExpression *new_expr = ToCurrGenvarValue(wait.GetCondition()) ;
    if (new_expr) (void) wait.ReplaceChildExprInternal(wait.GetCondition(), new_expr, 1) ;
    VeriVisitor::VERI_VISIT_NODE(VeriWait, wait) ;
}

void ReplaceGenVarFromStmt::VERI_VISIT(VeriSequenceConcat, concat)
{
    VERI_VISIT_NODE(VeriBinaryOperator, concat) ; // Call base class's visit
    // If cycle delay range is genvar ref, replace that by value
    VeriExpression *new_expr = ToCurrGenvarValue(concat.GetCycleDelayRange()) ;
    if (new_expr) {
        (void) concat.ReplaceChildExprInternal(concat.GetCycleDelayRange(), new_expr, 1) ;
    } else {
        if (concat.GetCycleDelayRange()) concat.GetCycleDelayRange()->Accept(*this) ;
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriClockedSequence, clk_seq)
{
    VERI_VISIT_NODE(VeriUnaryOperator, clk_seq) ; // Call base class's visit
    //VeriExpression *new_expr = ToCurrGenvarValue(clk_seq.GetEventExpression()) ;
    //if (new_expr) { // Replace event expression if it is genvar
        //(void) clk_seq.ReplaceChildExpr(clk_seq.GetEventExpression(), new_expr) ;
    //} else { // Visit event expression recursively
        if (clk_seq.GetEventExpression()) clk_seq.GetEventExpression()->Accept(*this) ;
    //}
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriDistOperator, dist_op)
{
    VERI_VISIT_NODE(VeriUnaryOperator, dist_op) ; // Call base class's visit

    unsigned i ;
    VeriExpression *expr, *new_expr ;
    Array *dist_list = dist_op.GetDistList() ;
    FOREACH_ARRAY_ITEM(dist_list, i, expr) { // Iterate over dist expressions
        if (!expr) continue ;
        new_expr = ToCurrGenvarValue(expr) ;
        if (new_expr) { // If expr is genvar, replace id-ref by value
            (void) dist_op.ReplaceChildExprInternal(expr, new_expr, 1) ;
        } else { // visit expression
            expr->Accept(*this) ;
        }
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriCast, op)
{
    VeriExpression *new_expr = ToCurrGenvarValue(op.GetTargetType()) ;
    if (new_expr) { // Replace target if it is genvar
        (void) op.ReplaceChildExprInternal(op.GetTargetType(), new_expr, 1) ;
    } else { // visit target recursively
        if (op.GetTargetType()) op.GetTargetType()->Accept(*this) ;
    }
    new_expr = ToCurrGenvarValue(op.GetExpr()) ;
    if (new_expr) { // Replace expr if it is genvar ref
        (void) op.ReplaceChildExprInternal(op.GetExpr(), new_expr, 1) ;
    } else { // Visit expr recursively
        if (op.GetExpr()) op.GetExpr()->Accept(*this) ;
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriNew, op)
{
    VeriExpression *new_expr = ToCurrGenvarValue(op.GetSizeExpr()) ;
    if (new_expr) { // Replace size expr if it is genvar ref
        (void) op.ReplaceChildExprInternal(op.GetSizeExpr(), new_expr, 1) ;
    } else { // Visit size expr recursively
        if (op.GetSizeExpr()) op.GetSizeExpr()->Accept(*this) ;
    }
    unsigned i ;
    VeriExpression *arg ;
    Array *args = op.GetArgs() ;
    FOREACH_ARRAY_ITEM(args, i, arg) { // Iterate over arguments
        if (!arg) continue ;
        new_expr = ToCurrGenvarValue(arg) ;
        if (new_expr) { // Replace argument if it is genvar ref
            (void) op.ReplaceChildExprInternal(arg, new_expr, 1) ;
        } else { // Visit argument recursively
            arg->Accept(*this) ;
        }
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriConcatItem, concat_item)
{
    VeriExpression *new_expr = ToCurrGenvarValue(concat_item.GetMemberLabel()) ;
    if (new_expr) { // Replace label if it is genvar
        (void) concat_item.ReplaceChildExprInternal(concat_item.GetMemberLabel(), new_expr, 1) ;
    } else { // visit label recursively
        if (concat_item.GetMemberLabel()) concat_item.GetMemberLabel()->Accept(*this) ;
    }
    new_expr = ToCurrGenvarValue(concat_item.GetExpr()) ;
    if (new_expr) { // Replace expr if it is genvar ref
        (void) concat_item.ReplaceChildExprInternal(concat_item.GetExpr(), new_expr, 1) ;
    } else { // Visit expr recursively
        if (concat_item.GetExpr()) concat_item.GetExpr()->Accept(*this) ;
    }
}
void ReplaceGenVarFromStmt::VERI_VISIT(VeriNamedPort, port)
{
    VeriExpression *new_expr = ToCurrGenvarValue(port.GetPortExpression()) ;
    if (new_expr) {
        (void) port.ReplaceChildExprInternal(port.GetPortExpression(), new_expr, 1) ;
    } else {
        if (port.GetPortExpression()) port.GetPortExpression()->Accept(*this) ;
    }
}

////////////////////////////////////////////////////////////////////////////////////
/////////////                 Array instance elaboration              //////////////
////////////////////////////////////////////////////////////////////////////////////

// Return the direction of the first argument specific terminal.  Second argument is the size of the terminal list for a particular gate.
unsigned VeriPseudoTreeNode::GetGateTermDirection(unsigned pos, unsigned term_size, const VeriGateInstantiation *inst)
{
    unsigned d_type = 0 ;
    // Switch over gate type
    switch(inst->GetInstType()) {
    case VERI_AND:
    case VERI_NAND:
    case VERI_OR:
    case VERI_NOR:
    case VERI_XOR:
    case VERI_XNOR:
    case VERI_BUFIF0:
    case VERI_BUFIF1:
    case VERI_NOTIF0:
    case VERI_NOTIF1:
    case VERI_NMOS:
    case VERI_PMOS:
    case VERI_RNMOS:
    case VERI_RPMOS:
    case VERI_CMOS:
    case VERI_RCMOS:
        // First terminal is output, others are input
        d_type = (!pos) ? VERI_OUTPUT : VERI_INPUT ;
        break ;
    case VERI_BUF:
    case VERI_NOT:
        // All terminals excluding last one is output
        d_type = (pos < (term_size - 1)) ? VERI_OUTPUT: VERI_INPUT ;
        break ;
    case VERI_TRAN:
    case VERI_RTRAN:
        // All terminals are inout
        d_type = VERI_INOUT ;
        break ;
    case VERI_TRANIF0:
    case VERI_TRANIF1:
    case VERI_RTRANIF0:
    case VERI_RTRANIF1:
        // All terminals excluding last one is inout, last one
        // is input
        d_type = (pos == (term_size - 1)) ? VERI_INPUT: VERI_INOUT ;
        break ;
    case VERI_PULLUP:
    case VERI_PULLDOWN:
        // All terminals are output
        d_type = VERI_OUTPUT ;
        break ;
    default:
        VERIFIC_ASSERT(0) ;
    }
    return d_type ;
}

// Visitor to explicitly declare implicitly declared ids:
class ImplicitVisitor : public VeriVisitor
{
public:
    explicit ImplicitVisitor(Array *decls) : VeriVisitor(), _decls(decls) { VERIFIC_ASSERT(_decls) ; /* must be given */ }
    virtual ~ImplicitVisitor() { _decls = 0 ; /* Just reset it: we do not own this Array */ }

private:
    // Prevent compiler from defining the following
    ImplicitVisitor() ;                                   // Purposely leave unimplemented
    ImplicitVisitor(const ImplicitVisitor &) ;            // Purposely leave unimplemented
    ImplicitVisitor& operator=(const ImplicitVisitor &) ; // Purposely leave unimplemented

public:
    // Only need to visit the VeriIdRefs:
    virtual void VERI_VISIT(VeriIdRef, node)
    {
        VeriIdDef *id = node.GetId() ;
        if (!id || !id->IsImplicitNet()) return ;

        // Here, we have an implicit net reference, need to explicitly declare it:
        // First copy the data type:
        VeriDataType *id_data_type = id->GetDataType() ;
        if (id_data_type) {
            VeriMapForCopy id_map_table ;
            id_data_type = id_data_type->CopyDataType(id_map_table) ;
            if (id_data_type) id_data_type->SetLinefile(node.Linefile()) ;
        }
        // Create an Array with the id being declared:
        Array *ids = new Array(1) ;
        ids->Insert(id) ;
        VeriModuleItem *decl = 0 ;
        // Now create a net-decl (implicit nets cannot be of type reg):
        if (id_data_type) {
            decl = new VeriNetDecl(id_data_type, 0, 0, ids) ;
        } else {
            // Data type not available, create with type token:
            decl = new VeriNetDecl(id->Type(), 0, 0, 0, ids, id->Sign()) ;
        }
        decl->SetLinefile(node.Linefile()) ;
        id->ClearImplicitNet() ; // We have it declared now
        _decls->Insert(decl) ;
    }

protected:
    Array *_decls ; // Array into which we insert the explicit declarations
} ; // class ImplicitVisitor

/* -------------------------------------------------------------- */

// Create net object declaration. These declaration are created while processing the instance terminal list.
// First argument is the word range of the object to be created.  Second argument is the sign of the object to be created.
// Third argument is the initial value of the object to be created. Initial value will be assigned for input ports
// Fourth argument will be populated with the identifier to be created.
unsigned VeriPseudoTreeNode::CreateDecl(VeriScope *scope, unsigned term_width, unsigned term_sign, VeriExpression *rhs_expr, VeriIdDef **id, Array &decls)
{
    if (!rhs_expr || !scope) return 0 ;

    VeriModuleItem* new_decl = 0 ;
    VeriRange *range = 0 ;
    if (term_width != 1) {
        // Vector object
        // Word range is created. Msb will be 'term_width - 1' and lsb will be '0'
        VeriIntVal *msb = new VeriIntVal((int)term_width - 1) ;
        VeriIntVal *lsb = new VeriIntVal(0) ;
        msb->SetLinefile(rhs_expr->Linefile()) ;
        lsb->SetLinefile(rhs_expr->Linefile()) ;
        range = new VeriRange(msb , lsb) ;
        range->SetLinefile(rhs_expr->Linefile()) ;
    }
    Array *id_list = new Array(1) ;
    // Generate an unique name of the object
    char* var_name = GetUniqueName(scope, 0) ;

    // Create new variable
    VeriVariable *var = new VeriVariable(var_name) ;
    var->SetLinefile(rhs_expr->Linefile()) ;

    // Add the variable to the scope
    (void) scope->Declare(var) ;
    id_list->InsertLast(var) ;
    *id = var ;
    // Set initial value if required
    var->SetInitialValue(rhs_expr) ;
    new_decl = new VeriNetDecl(VERI_WIRE, 0, range, 0, id_list, term_sign ? VERI_SIGNED : 0) ;
    new_decl->SetLinefile(rhs_expr->Linefile()) ;
    // VIPER #2918: Explicitly declare implicitly declared nets in 'rhs_expr':
    ImplicitVisitor iv(&decls) ;
    rhs_expr->Accept(iv) ;
    decls.Insert(new_decl) ;
    return 1 ;
}

// Create single instance identifier with appropraite index. These identifiers are used to
// create new elaborated instantiation. Since VeriInstId now also takes the port-association list, pass that in as argument.
VeriInstId *VeriPseudoTreeNode::CreateInstanceId(const VeriIdDef *inst_id, int instance_id_idx, Array *new_term_list, VeriScope *mod_scope)
{
    if (!inst_id || !mod_scope) return 0 ; // Cant create a new one without an old one.

    // Create the new name of the instance.
    char str[40] ;
    sprintf(str, "%d", instance_id_idx) ;
    // Unnamed instance names create unnamed generated instance names.
    char *new_name = (inst_id->Name()) ? Strings::save(inst_id->Name(), "[", str, "]") : 0 ;

    // Create the instance : identifier with the copied actuals list :
    VeriInstId *new_inst = new VeriInstId(new_name, 0, new_term_list) ; // all arguments absorbed.
    // VIPER #8337 : Add attribute to mark generate elaboration created ids.
    // FIXME: Can use bit-flag to mark such identifiers
    Map* attr_decl_map = new Map(STRING_HASH) ;
    (void) attr_decl_map->Insert(Strings::save(" gen_elab_created"), new VeriIntVal(1)) ;

    // VIPER #8360: Add attribute to array instance created instance identifier
    if (RuntimeFlags::GetVar("veri_add_attribute_to_array_instance_created_instance_id")) {
        (void) attr_decl_map->Insert(Strings::save(" from_array_inst"), new VeriIntVal(1)) ;
    }
    new_inst->AddAttributes(attr_decl_map) ;

    // If path name is associated with 'inst_id', propagate that to new instance id :
    PropagateAssociatedPathName(inst_id, new_inst, instance_id_idx) ;

    // VIPER #8366 : Allow name conflict only for unnamed instances
    // Store this identifier in the same scope as the original :
    if (inst_id->Name()) {
        if (!mod_scope->Declare(new_inst)) {
            inst_id->Error("illegal duplicate name created during elaboration, multiple declarations created for '%s'", new_inst->Name()) ;
            delete new_inst ;
            return 0 ;
        }
    } else {
        (void) mod_scope->Declare(new_inst, 1 /* force insert */) ;
    }

    // Set line/file info from original identifier :
    new_inst->SetLinefile(inst_id->Linefile()) ;

    return new_inst ;
}

// Extract direction, size, iddef of module/interface port using first argument specific port name/second argument specific position.
unsigned VeriModule::PortWidthDir(const char *port_name, unsigned port_pos, unsigned *dir, VeriIdDef **master_id, const char **formal_name)
{
    if (!_ports || !_port_connects) return 0 ; // no port
    *master_id = 0 ; // Clear it
    if (formal_name) *formal_name = port_name ; // Reset it to the name specified, if any
    unsigned master_width = 0 ;

    // Check if module/interface has ansi port list
    unsigned is_ansi_port = 0 ;
    unsigned i ;
    VeriExpression *port_expr ;
    FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
        if (port_expr && port_expr->IsAnsiPortDecl()) {
            is_ansi_port = 1 ; // ansi port
            break ;
        }
    }
    if (!port_name) { // Use 'port_pos' to obtain port information
        if (is_ansi_port) { // use port def list to obtain port
            VeriIdDef *port = (_ports->Size() > port_pos) ? (VeriIdDef*)_ports->At(port_pos): 0 ;
            // Get direction
            *dir = port ? port->Dir() : 0 ;
            *master_id = port ;
            verific_int64 port_ss = (port) ? port->StaticSizeSignInternal(0, 0) : 0 ;
            master_width = (unsigned)ABS(port_ss) ;
        } else { // Non-ansi port, get port expression from port list
            VeriExpression *port = (_port_connects->Size() > port_pos) ? (VeriExpression*)_port_connects->At(port_pos) : 0 ;
            // Get the formal name of this non-ANSI port if it is a named formal:
            if (formal_name && port) *formal_name = port->NamedFormal() ;
            port = port ? port->GetConnection() : 0 ;

            *dir = port ? port->NonAnsiPortDir() : 0 ;
            *master_id = port ? port->FullId() : 0 ;
            verific_int64 port_ss = (port) ? port->StaticSizeSignInternal(0, 0) : 0 ;
            master_width = (unsigned)ABS(port_ss) ;
        }
        // Get the name of this port if it is not already set (not a named formal) from its port id:
        if (formal_name && !(*formal_name) && (*master_id)) *formal_name = (*master_id)->Name() ;
    } else { // Terminal is named port connection, we have to extract port information comparing name
        if (is_ansi_port) {
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(_ports, i, id) {
                if (!id) continue ;
                if (Strings::compare(id->GetName(), port_name)) { // Find right port
                    *dir = id->Dir() ;
                    *master_id = id ;
                    verific_int64 id_ss = (id) ? id->StaticSizeSignInternal(0, 0) : 0 ;
                    master_width = (unsigned)ABS(id_ss) ;
                    break ;
                }
            }
        } else {
            FOREACH_ARRAY_ITEM(_port_connects, i, port_expr) {
                if (!port_expr) continue ;
                const char* temp_formal_name = port_expr->NamedFormal() ; // local formal_name hides parameter of same name. Refactored to temp_formal_name to aviod compiler warning
                if (temp_formal_name) { // named port
                    if (Strings::compare(port_name, temp_formal_name)) {
                        port_expr = port_expr->GetConnection() ;

                        *dir = port_expr->NonAnsiPortDir() ;
                        *master_id = port_expr->FullId() ;
                        verific_int64 port_expr_ss = port_expr->StaticSizeSignInternal(0, 0) ;
                        master_width = (unsigned)ABS(port_expr_ss) ;
                        break ;
                    }
                } else {
                    VeriIdDef *id = port_expr->GetId() ;
                    if (id && (Strings::compare(id->GetName(), port_name))) {
                        *dir = id->Dir() ;
                        *master_id = id ;
                        verific_int64 id_ss = id->StaticSizeSignInternal(0, 0) ;
                        master_width = (unsigned)ABS(id_ss) ;
                        break ;
                    }
                }
            }
        }
    }
    VeriIdDef *formal_id = *master_id ;
    if (!formal_id) return master_width ;

    // Check if this is a modport-interface association.
    // We need to treat these special to calculate formal width
    VeriDataType *formal_data_type = formal_id->GetDataType() ;
    VeriName *type_name = (formal_data_type) ? formal_data_type->GetTypeName(): 0 ;
    VeriIdDef *formal_type = 0 ;
    if (type_name) { // Named data type
        formal_type = type_name->GetId() ;
        if (formal_type) { // Data type is resolved
            if (formal_type->IsModport()) {
                // Go to the prefix to get interface name
                VeriName *prefix = type_name->GetPrefix() ;
                formal_type = (prefix) ? prefix->GetId() : 0 ;
            }
        } else {
            // 'type_name' can be interface_name or interface_name.modport
            // Try to find interface from this name
            VeriName *prefix = type_name ;
            VeriName *prev_prefix = prefix ;
            while(prefix && prev_prefix) { // Get prefix until we get first element
                prefix = prev_prefix->GetPrefix() ;
                if (prefix) prev_prefix = prefix ;
            }
            VeriLibrary *work_lib = _parent_library ;
            if (!work_lib) work_lib = veri_file::GetWorkLib() ;
            VeriModule *interface = veri_file::GetModule(prev_prefix ? prev_prefix->GetName(): type_name->GetName(), 1, (work_lib) ? work_lib->GetName(): 0) ;
            formal_type = interface ? interface->GetId(): 0 ; // Get interface id
        }
    }
    if ((formal_type && formal_type->IsInterface()) || (formal_id->Dir() == VERI_INTERFACE)) {
        // Formal is interface, get formal size. Consider only dimensions, not size of
        // interface, as formal can have generic interface
        VeriRange *formal_range = formal_id->GetDimensions() ;
        unsigned formal_size = 1 ;
        VeriRange *range = formal_range ;
        int msb = 0, lsb = 0 ;
        while (range) { // Calculate formal size
            formal_size = formal_size * range->GetWidth(&msb, &lsb, 0) ;
            range = range->GetNext() ;
        }
        master_width = formal_size ; // Set formal_size as master_width
    }
    return master_width ;
}

// Elaborate .* and .name style port connections to .name(name) style port connection
// of the module instantiations inside these generate items.
// is_prameterized_construct holds 2 information
// 1) 1 st bit from lsb indicates whether generate items are parameterized
// 2) 2 nd bit from lsb indicates whether generate contains instantiation
void VeriGenerateConditional::ProcessModuleItem(VeriScope * /* container_scope */, unsigned &is_prameterized_construct)
{
    if (!(is_prameterized_construct & 1) && VeriPseudoTreeNode::IsContainNonLocalParam(_if_expr)) is_prameterized_construct = is_prameterized_construct | 1 ;

    // Elaborate instances both inside the 'then' and 'else' part:
    if (_then_item) _then_item->ProcessModuleItem(_then_scope, is_prameterized_construct) ;
    if (_else_item) _else_item->ProcessModuleItem(_else_scope, is_prameterized_construct) ;
}

void VeriGenerateCase::ProcessModuleItem(VeriScope * /* container_scope */, unsigned &is_prameterized_construct)
{
    if (!(is_prameterized_construct & 1) && VeriPseudoTreeNode::IsContainNonLocalParam(_cond)) is_prameterized_construct = is_prameterized_construct | 1 ;

    // Elaborate instances inside all the case items:
    unsigned i ;
    VeriGenerateCaseItem *gen_item ;
    FOREACH_ARRAY_ITEM(_case_items, i, gen_item) {
        if (!gen_item) continue ;
        VeriModuleItem *item = gen_item->GetItem() ;
        VeriScope *scope = gen_item->GetScope() ;
        if (item) item->ProcessModuleItem(scope, is_prameterized_construct) ;
        // If we know that it is already parameterized, we don't need to check it again:
        if (is_prameterized_construct & 1) continue ;
        Array *item_conds = gen_item->GetConditions() ;
        unsigned j ;
        VeriExpression *cond ;
        FOREACH_ARRAY_ITEM(item_conds, j, cond) {
            if (VeriPseudoTreeNode::IsContainNonLocalParam(cond)) {
                is_prameterized_construct = is_prameterized_construct | 1 ;
                break ;
            }
        }
    }
}

void VeriGenerateFor::ProcessModuleItem(VeriScope * /* container_scope */, unsigned &is_prameterized_construct)
{
    if (!(is_prameterized_construct & 1) && (VeriPseudoTreeNode::IsContainNonLocalParam(_cond) ||
                                      VeriPseudoTreeNode::IsContainNonLocalParam(_initial) ||
                                      VeriPseudoTreeNode::IsContainNonLocalParam(_rep))) is_prameterized_construct = is_prameterized_construct | 1 ;

    // Elaborate instances inside all the for items:
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->ProcessModuleItem(_scope, is_prameterized_construct) ;
    }
}

void VeriGenerateBlock::ProcessModuleItem(VeriScope * /* container_scope */, unsigned &is_prameterized_construct)
{
    // Elaborate instances inside all the block items:
    unsigned i ;
    VeriModuleItem *item ;
    FOREACH_ARRAY_ITEM(_items, i, item) {
        if (item) item->ProcessModuleItem(_scope, is_prameterized_construct) ;
    }
}

void VeriModuleInstantiation::ProcessModuleItem(VeriScope *container_scope, unsigned &is_prameterized_construct)
{
    // Contains module instantiation inside generate, propagate information to upper
    // Set 2 nd bit of argument 2
    is_prameterized_construct = is_prameterized_construct | (1 << 1) ;
    // VIPER #5984 : Do not expand .* here. It will be done outside
    //ElaboratePortConnections(container_scope) ;

    // If we know that it is already parameterized, we don't need to check it again:
    if (is_prameterized_construct & 1) return ;

    unsigned i ;
    VeriInstId *inst_id ;
    FOREACH_ARRAY_ITEM(&_insts, i, inst_id) {
        VeriRange *dim = (inst_id) ? inst_id->GetRange() : 0 ;
        //if (VeriPseudoTreeNode::IsContainNonLocalParam(dim)) {
        if (dim) {
            // VIPER #6050 : If array instance is present, we need to copy
            // container module always. So mark this parameterized without
            // checking presence of parameter in instance range :
            is_prameterized_construct = is_prameterized_construct | 1 ;
            break ;
        }
        if (inst_id && !inst_id->Name()) {
            // VIPER #7938: Module instance with no instance name, create
            // instance name
            VeriModule *instantiated_mod = GetInstantiatedModule() ;
            if (RuntimeFlags::GetVar("veri_static_elaborate_unnamed_module_instances") && (!instantiated_mod || (!instantiated_mod->IsPrimitive()))) {
                inst_id->Warning("module instantiation should have an instance name") ;
                (void) CreateAndSetUniqueInstanceName(_module_name ? _module_name->GetName(): 0, inst_id, container_scope) ;
            }
        }
    }
}
// Elaborate .* and .name style port connections to .name(name) style port connection
void VeriModuleInstantiation::ElaboratePortConnections(VeriScope *scope)
{
    if (!scope) return ;
    // Get the module that is instantiated
    VeriModule *master = GetInstantiatedModule() ;
    if (!master || master->GetQualifier(VERI_EXTERN)) return ; // Cannot continue

    // Get the formal ports of the module
    Array *formals = master->GetPorts() ;
    //if (!formals || !formals->Size()) return ; // No need to proceed further
    // Get the port connects of the module
    Array *exact_formals = master->GetPortConnects() ;
    if (!exact_formals || !exact_formals->Size()) return ; // No need to proceed further

    // Check if all declared formal ports can be processed for .name and .* style
    // port connection in instantiation. Some of the Verilog95 style port declarations cannot be processed!
    unsigned i ;
    VeriExpression *port ;
    unsigned is_ansi_port = 0 ;
    FOREACH_ARRAY_ITEM(exact_formals, i, port) {
        if (!port) continue ;
        if (port->IsAnsiPortDecl()) is_ansi_port = 1 ;
        break ;
        // To be a valid port for .name/.* elaborate it must be either an ansi port
        // or non-ansi port that have a proper id and does not have an index expression
        // which indicates that it is not a select expression
        // We will do nothing if formal is like '.a(b)' (VeriPortConnect), '{a, b}' VeriConcat,
        // 'a[3:0] or b[3:0][3:0]' (VeriIndexedId), 'a, , b' (VeriPortOpen), '.*' (VeriDotStar)
        //if (!(port->FullId() || port->IsAnsiPortDecl()) || port->GetIndexExpr()) {
            // Can't process this type of port declaration for .name or .* style connection
            //return ;
        //}
    }
    // VIPER #5185 : If formal is named port like .a(p), expand the .* present in instantiation
    Set formal_names(STRING_HASH, exact_formals->Size()) ;
    VeriIdDef *formal_id ;
    const char *formal_name ;
    if (is_ansi_port) {
        FOREACH_ARRAY_ITEM(formals, i, formal_id) {
            if (formal_id) (void) formal_names.Insert(formal_id->Name()) ;
        }
    } else {
        FOREACH_ARRAY_ITEM(exact_formals, i, port) {
            if (!port) continue ;
            formal_name = port->NamedFormal() ;
            if (formal_name) port = port->GetConnection() ;
            formal_id = (port) ? port->FullId(): 0 ;
            if (formal_name || formal_id) { // named port or identifier reference
                (void) formal_names.Insert(formal_name ? formal_name : (formal_id) ? formal_id->Name(): "") ;
            } else { // concat '{a, b}/indexed a[3:0] /open/.* in port list
                return ; // Can't process this type of port declaration  for .name or .* style connection
            }
        }
    }

    VeriInstId *inst ;
    // Traverse all the instances of the module instantiation
    FOREACH_ARRAY_ITEM(&_insts, i, inst) {
        if (!inst) continue ;
        // Get the Array of port connection for the current instance
        Array *port_connections = inst->GetPortConnects() ;
        if (!port_connections) continue ; // Nothing to do!

        // Create a Set to store the formal names of the ports
        Set *formal_port_names = new Set(STRING_HASH, formal_names.Size()) ;

        // Insert the port names into the set
        SetIter si ;
        FOREACH_SET_ITEM(&formal_names, si, &formal_name) {
            if (!formal_name) continue ;
            (void) formal_port_names->Insert(formal_name) ;
        }

        VeriExpression *connection ;
        int has_dot_star_at = -1 ; // Assume there is no .* style port connection by default
        unsigned j ;
        // Now, elaborate all the .name style port connection to .name(name) style
        // and delete the port named from the Set that should not be processed for .*
        FOREACH_ARRAY_ITEM_BACK(port_connections, j, connection) {
            if (!connection) continue ;

            if (connection->IsDotStar()) {
                if (has_dot_star_at >= 0) {
                    // Delete the previous .* port connection, if any
                    VeriExpression *dot_star = (VeriExpression *)port_connections->At((unsigned)has_dot_star_at) ;
                    port_connections->Remove((unsigned)has_dot_star_at) ;
                    delete dot_star ; // Delete the port connection
                }
                // Store the position of the .* style port connection
                has_dot_star_at = (int)j ; // So, there is only one .* connection
            } else if ((!connection->IsNameExtendedClass() && connection->IsOpen()) || // Open port connection : assuming positional
                       (!connection->NamedFormal())) { // Normal expression connected, assuming positional
                // We should not process an open or positional port connection for .*. So, try to remove it
                if (formal_port_names->Size() > j) {
                    const char *port_name = (const char *)formal_port_names->GetAt(j) ;
                    (void) formal_port_names->Remove(port_name) ;
                    // Do not warn, since there can be same port defined more than
                    // once and other tools do not warn. So should not we.
                //} else {
                    //Warning("too many actuals in instantiation") ;
                }
            } else if (connection->NamedFormal()) {
                // Assume it to be a .name style port connection
                const char *port_name = connection->NamedFormal() ;
                if (formal_port_names->GetItem(port_name)) {
                    // We should not process this .name() style port connection for .*
                    // Remove this also
                    (void) formal_port_names->Remove(port_name) ;
                } else if (formal_names.GetItem(port_name)) {
                    // VIPER #5342: Not in the current Set but the port was there. So,
                    // it is already connected, produce a warning message about this:
                    Warning("port %s is already connected", port_name) ;
                } else {
                    Warning("cannot find port %s on this module", port_name) ;
                }
            }
        }

        if (has_dot_star_at >= 0) {
            // We have got a dot-star port connection
            unsigned replaced = 0 ;
            // Get the .* style port connection
            VeriExpression *dot_star = (VeriExpression *)port_connections->At((unsigned)has_dot_star_at) ;
            // Get the linefile of the .* port conection
            linefile_type dot_star_line_file = (dot_star) ? dot_star->Linefile() : 0 ;
            char *name ;
            // Create the port connection for non-existing one. Remove the .* port connection from the Array.
            if (!formal_port_names->Size()) {
                // VIPER #4702: .* exists in port connection, but all formal identifiers
                // are already covered, so need to remove .* connection from
                // association list:
                port_connections->Remove((unsigned)has_dot_star_at) ;
                delete dot_star ; dot_star = 0 ; // Memory leak fix
            }
            FOREACH_SET_ITEM(formal_port_names, si, &name) {
                if (!name) continue ;

                VeriIdDef *id = scope->Find(name) ;
                VeriPortConnect *pc = 0 ;
                VeriExpression *connection_expr = 0 ;
                char *port_ref_name = Strings::save(name) ;
                // Create the port-connection with this id, if it is there.
                // If no identifier found, create an open port connection
                if (id) {
                    connection_expr = new VeriIdRef(id) ;
                } else {
                    // VIPER #5430: Produce error here. Other tools error out in this case:
                    Error("%s is not declared", name) ;
                    connection_expr = new VeriPortOpen() ;
                }
                connection_expr->SetLinefile(dot_star_line_file) ;
                pc = new VeriPortConnect(port_ref_name, connection_expr) ;
                pc->SetLinefile(dot_star_line_file) ;
                if (!replaced) {
                    // Over-write existing .* port connection
                    port_connections->Insert((unsigned)has_dot_star_at, pc) ;
                    delete dot_star ; // Delete the port connection
                    dot_star = 0 ;
                    replaced = 1 ;
                } else {
                    // Position does not matter
                    port_connections->InsertLast(pc) ;
                }
            }
        }
        delete formal_port_names ;
    }
}

// Main routine to elaborate module instantiation.
// This routine break the array instantiation like (mod I[3:0] (...))
// to multiple single instantiations (mod I[3](); mod I[2](); .....).
// Elaboration steps
//    * The instance range is evaluated to get the number of instantiations to be created
//      Ex-  mod I [3:0]  (...)
//      Four instantiations will be created after elaboration
//    * For each actual the following steps are performed to create this actual for each single instantiation.
//       > If the actual is open connection, that actual for each single instantiation will be open. So this
//         actual is copied instance_range_width times.
//       > If the width of the actual is equal to the width of the master, this actual should go as it is
//         to each single instantiation. So actual is copied instance_range_width times.
//       > Width of the actual is equal to width_of_formal times instance_range_width.  If the
//         actual is simple identifier reference, bit or part select or concatenation of id_ref,
//         select and concat, then actual is split, so that width of each split becomes width of formal.
//         If the actual is complex expression (except id-ref, select, concat), a wire type object
//         declaration is created. The word range of the created object will be width of actual
//         and the initial value of that object will be actual expression.  The corresponding
//         actual for each single instantiation becomes bit/part select of created wire.
//    * single instantiation creation
//        * New terminal list of each instantiation is created from previous step specific actuals
//        * New instance identifier will be 'old_instance_id[index_to_indicate_particular_instance]'
//    * Each single instantiations and created net object declartions are inserted in the module
//      item list and original array-instance is removed from the parse-tree
//
//  Ex - module child (input [1:0] in1, in2, in3, in4, output out);
//     endmodule
//
//       module top ;
//          reg [1:0] A, B;  reg [7:0] C, D;
//          wire [3:0] O1 ;
//
//          child I[3:0] (A | B, C + D, B, , O1) ;
//       endmodule
//     .................
//
//    After elaboration the module top will look like
//
//    module top ;
//         reg [1:0] A, B ;
//         reg [7:0] C, D ;
//         wire [3:0] O1 ;
//         wire [7:0] tmp = (C + D) ;
//        child \I[3]  ((A | B), tmp[7:6], B, , O1[3]) ;
//        child \I[2]  ((A | B), tmp[5:4], B, , O1[2]) ;
//        child \I[1]  ((A | B), tmp[3:2], B, , O1[1]) ;
//        child \I[0]  ((A | B), tmp[1:0], B, , O1[0]) ;
//     endmodule
unsigned VeriModuleInstantiation::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array *parent_pseudo_nodes, Map *mod_map_tab, Map *top_mod_nodes, Set * /*defparams*/)
{
    // VIPER 2128 : Break array instance to create multiple single instance, but
    // do not split the terminals, terminals will be as it was. We will split the
    // terminals at the last step of elaboration
    Array *instances = GetInstances() ; // Get instance list
    VeriInstId *inst = (instances && instances->Size()) ? (VeriInstId*)instances->At(0) : 0 ; // get single instance
    if (!inst) return 0 ; // Error in analyze
    VeriRange *dims = inst->GetDimensions() ;
    // VIPER #4697: Need to split the actual ports for non-array instances, so remove this return statement
    //if (!dims) return 1 ; // Not array instance, return

    // VIPER #3374 : Do not split array instance if it is black-box instance. For
    // array instance of black-boxes we have already created array instance node
    // and those nodes are marked as black-box. So here only check if any node
    // for this instance is created, if so and that node is marked as black-box
    // array instance, function will stop processing and return.
    VeriPseudoTreeNode *parent ;
    unsigned idx = 0 ;
    FOREACH_ARRAY_ITEM (parent_pseudo_nodes, idx, parent) {
        Map *children = (parent) ? parent->GetChildren() : 0 ;
        VeriPseudoTreeNode *child = (children) ? (VeriPseudoTreeNode*)children->GetValue(inst->Name()) : 0 ;
        if (!child) continue ;
        //  VIPER #4697: split instance type actuals for black-boxes. For module
        // instantiations split will be done inside "VeriStaticElaboration::Process"
        // VIPER #7308: Do not split actuals fo black-boxes here. This instantiation
        // can come here multiple times because containing module can have generate
        // That will cause corruption as we can add 'actual' in hier-id-tab
        //if (!child->GetDesign() && !child->IsArrayInstance() && !child->IsBlackBoxArrayInstance()) {
            //inst->SplitInterfaceArrayPortActuals(child, mod_map_tab, top_mod_nodes) ;
            //continue ;
        //}
        if (child->IsBlackBoxArrayInstance()) return 1 ; // It is black-box
    }

    if (!dims) return 1 ; // Not array instance, return

    int msb = 0, lsb = 0 ; // Msb lsb of range
    unsigned instance_count = dims->GetWidth(&msb, &lsb, 0) ;
    if (instance_count == 0) return 0 ; // May be error
    // VIPER #3252 : Do not delete the range from instance identifier, it will
    // help us to resolve hierarchical identifier correctly.
    //if ((instance_count == 1) && inst) (void) inst->ReplaceChildRange(dims, 0) ; // Delete range if instance count is one

    Array *terminal_list = inst->GetPortConnects() ; // get terminal list

    // Create a container to store created instances
    Array instance_arr(instance_count) ;
    VeriScope *scope = mod ? mod->GetScope(): 0 ; // Get module scope
    // Create instance range width numbers instantiations
    // Do not split terminals, only break instances with original terminal copied in all
    idx = 0 ;
    Array instance_ids(instance_count) ;
    while(idx < instance_count) {
        VeriMapForCopy old2new ; // Map to copy instance elements
        // New terminal list of instantiation
        Array *new_term_list = CopyExpressionArray(terminal_list, old2new) ;

        int instance_id_idx = 0 ;
        // The index of the instance identifier will depend on msb and lsb of the range.
        // Index will be created staring from msb of the range expression
        instance_id_idx = (msb > lsb) ? (msb - (int)idx): (msb + (int)idx) ;

        // Create the instance : identifier with the copied actuals list :
        VeriInstId *new_inst = VeriPseudoTreeNode::CreateInstanceId(inst, instance_id_idx, new_term_list, scope) ;
        if (new_inst) {
            // Copy the strength
            VeriStrength *new_strength = _strength ? _strength->Copy(old2new) : 0 ;
            // Copy the parameter overridden list
            Array *new_params = CopyExpressionArray(_param_values, old2new) ;

            // Copy the attributes
            Map *new_attr = CopyAttributes(GetAttributes(), old2new) ;

            // Copy instantiated module name
            VeriName *new_module_name = _module_name ? _module_name->CopyName(old2new) : 0 ;
            instance_ids.InsertLast(new_inst) ;

            // Create instantiation
            VeriModuleInstantiation *new_mod_inst = new VeriModuleInstantiation(new_module_name, new_strength, new_params, new_inst, 0) ;
            new_mod_inst->SetAttributes(new_attr) ;
            new_mod_inst->SetLinefile(Linefile()) ;
            new_mod_inst->SetLibrary(_library) ;
            // Set the new instantiation as a backpointer on the instance id :
            new_inst->SetModuleInstance(new_mod_inst) ;
            // Add the created instantiation in 'instance_arr' array
            instance_arr.InsertLast(new_mod_inst) ;
        }
        idx++ ; // increment the index
    }

    // Create pseudo nodes for multiple instances
    FOREACH_ARRAY_ITEM (parent_pseudo_nodes, idx, parent) {
        if (parent) parent->CreateArrayInstanceNodes(&instance_arr, inst /* original name */, mod_map_tab, top_mod_nodes) ;
    }

    if (instance_arr.Size()) {
        // VIPER #5278 : After spliting the array instance of interface, create a
        // virtual interface declaration with the same name as array instance and
        // initial value of that identifier will be assignment pattern whose elements
        // are references of instance identifiers created from array instance split.
        //  intf I[1:0]() ; // array instance
        //  Above code will break this like :
        // intf \I[1] () ; intf \I[0] () ;

        // So created virtual interface will  be
        // virtual intf I[1:0] = '{\I[1] , \I[0] } ;

        // If in the design there is reference to whole array instance identifier, that
        // reference will point to new virtual interface. This technique will help us to
        // make static elaborated output legal. Otherwise we need to replace all whole
        // array instance reference with assignment pattern.
        VeriModuleInstantiation *instance ;
        VeriModule *instantiated_mod = 0 ;
        unsigned is_needed = 1 ;
        unsigned i ;
        // Check whether it is array instance of interface. Take help from created
        // pseudo tree nodes as from there only we can get actual instantiated module
        FOREACH_ARRAY_ITEM(&instance_arr, idx, instance) {
            if (!inst) continue ;
            VeriPseudoTreeNode *n ;
            Array *ids = instance->GetIds() ;
            VeriIdDef *inst_id = (ids) ? (VeriIdDef*)ids->At(0): 0 ;
            FOREACH_ARRAY_ITEM(parent_pseudo_nodes, i, parent) {
                n = parent ? parent->CheckMatchWithInstanceName(inst_id ? inst_id->Name(): 0, 0): 0 ;
                VeriModule *m = n ? n->GetContainingModule(): 0 ;
                if (!m) continue ;
                if (!instantiated_mod) instantiated_mod = m ;
                if ((instantiated_mod != m) || !m->IsInterface()) {
                    is_needed = 0 ; break ; // not interface instance, no need of virtual interface
                }
            }
        }
        // Undeclare the current array instance identifier and create new virtual interface
        if (is_needed && scope && instantiated_mod) {
            VeriDataDecl *decl = VeriPseudoTreeNode::CreateVirtualInterface(inst, &instance_ids, instantiated_mod, mod) ;
            instance_arr.InsertLast(decl) ; // Add it after created instantiations in container
        }

        // Add the created declarations, instances in module item list. Declarations are to
        // inserted before the new created instantiations. Also delete the current instantiation
        if (mod) (void) mod->ReplaceChildBy(this, &instance_arr) ;
    }
    return 1 ;
}

// Main routine to elaborate gate instantiation.
// This routine break the array instantiation like (and I[3:0] (...))
// to multiple single instantiations (and I[3](); and I[2](); .....).
// Elaboration steps
//    * The instance range is evaluated to get the number of instantiations to be created
//      Ex-  and I [3:0]  (...)
//      Four instantiations will be created after elaboration
//    * For each actual the following steps are performed to create this actual for each
//      single instantiation.
//       > If the actual is open connection, that actual for each single instantiation
//         will be open. So this actual is copied instance_range_width times.
//       > If the width of the actual is equal to the width of the master, this actual
//         should go as it is to each single instantiation. So actual is copied instance_range_width times.
//       > Width of the actual is equal to width_of_formal times instance_range_width.
//         If the actual is simple identifier reference, bit or part select or
//         concatenation of id_ref, select and concat, then actual is split, so that width
//         of each split becomes width of formal.
//         If the actual is complex expression (except id-ref, select, concat), a wire type
//         object declaration is created. The word range of the created object will be width
//         of actual and the initial value of that object will be actual expression.
//         The corresponding actual for each single instantiation becomes bit/part select of created wire
//    * single instantiation creation
//        * New terminal list of each instantiation is created from previous step specific actuals
//        * New instance identifier will be 'old_instance_id[index_to_indicate_particular_instance]'
//    * Each single instantiations and created net object declartions are inserted in the module item
//      list and original array-instance is removed from the parse-tree
unsigned VeriGateInstantiation::StaticElaborateModuleItem(VeriModuleItem *mod, VeriLibrary * /*lib*/, Array * /*parent_pseudo_nodes*/, Map *mod_map_tab, Map *top_mod_nodes, Set * /*defparams*/)
{
    Array arr(2) ;
    ConvertToMultipleSingleItems(&arr, mod) ;
    // Make single instantiation from multiple instantiations
    //BreakInstantiationAndPopulateArray(&arr) ;
    //if (arr.Size() > 1) (void) mod->ReplaceChildBy(this, &arr) ;

    Set *nodes = (mod_map_tab) ? (Set*)mod_map_tab->GetValue(mod): 0 ;

    SetIter si ;
    VeriPseudoTreeNode *node = 0 ;
    FOREACH_SET_ITEM(nodes, si, &node) { // Iterate over all instance created pseudo node
        if (!node || (node->GetDesign() != mod)) continue ;
        break ;
    }

    // VIPER #4860 : Pass pseudo tree nodes for the containing module and other related information
    // to 'ElaborateArrayInst' routine to porvide proper scope to process any hierarchical name
    // present in actual of gate instance.
    unsigned i = 0 ;
    unsigned array_inst = 0 ;
    VeriGateInstantiation *inst = 0 ;
    FOREACH_ARRAY_ITEM(&arr, i, inst) {
        Array *inst_list = inst ? inst->GetInstances(): 0 ;
        VeriInstId *inst_id = 0  ;
        unsigned k = 0 ;
        FOREACH_ARRAY_ITEM(inst_list, k, inst_id) {
            if (inst_id && inst_id->GetDimensions()) {
                array_inst = 1 ;
                break ;
            }
        }

        if (inst && !inst->ElaborateArrayInst(mod, node, top_mod_nodes, mod_map_tab)) return 0 ;
    }

    if (array_inst) return 1 ;
    FOREACH_ARRAY_ITEM(&arr, i, inst) {
        if (!inst) continue ;
        // VIPER #5247: The output/inout terminals of all primitive instances
        // shall be connected only to scalar nets or bit-selects of vector nets.
        Array *inst_list = inst->GetInstances() ;
        VeriInstId *inst_id = 0  ;
        unsigned k = 0 ;

        FOREACH_ARRAY_ITEM(inst_list, k, inst_id) {
            if (!inst_id) continue ;
            // VIPER #5247: The output/inout terminals of all primitive instances
            // with multidimension can be connected to vector net/var.
            if (inst_id->GetDimensions()) continue ;
            Array *port_connects = inst_id->GetPortConnects() ;
            unsigned j = 0 ;
            VeriExpression *expr ;

            FOREACH_ARRAY_ITEM(port_connects, j, expr) {
                if (!expr) continue ;
                verific_int64 expr_ss = expr->StaticSizeSignInternal(0, 0) ; // Get size of the expression node
                unsigned expression_ss = (unsigned)ABS(expr_ss) ;
                // VIPER #5362 : Do not use fields of 'this', get type from array
                unsigned inst_type = inst->GetInstType() ;
                switch (inst_type) {
                case VERI_BUF :
                case VERI_NOT :
                    // one or more outputs, followed by one input:
                    if (j==port_connects->Size()-1) { // input
                        if (expression_ss > 1)  inst->Warning("actual bit length %d differs from formal bit length %d", expression_ss, 1) ;
                    } else { // output
                        if (expression_ss > 1)  inst->Error("primitive %s connection must be a scalar var or net", "output") ;
                    }
                    break ;
                case VERI_NMOS :
                case VERI_PMOS :
                case VERI_RNMOS :
                case VERI_RPMOS :
                case VERI_CMOS :
                case VERI_RCMOS :
                    // One output, one input, one enable
                    if (j == 0) { // output
                        if (expression_ss > 1)  inst->Error("primitive %s connection must be a scalar var or net", "output") ;
                    } else { // input
                        if (expression_ss > 1)  inst->Warning("actual bit length %d differs from formal bit length %d", expression_ss, 1) ;
                    }
                    break ;
                case VERI_TRAN :
                case VERI_RTRAN :
                case VERI_TRANIF1 :
                case VERI_TRANIF0 :
                case VERI_RTRANIF1 :
                case VERI_RTRANIF0 :
                    // inout, inout, enable
                    if (j < 2) { // inout
                        if (expression_ss > 1)  inst->Error("primitive %s connection must be a scalar var or net", "inout") ;
                    } else { // input
                        if (expression_ss > 1)  inst->Warning("actual bit length %d differs from formal bit length %d", expression_ss, 1) ;
                    }
                    break ;
                default:
                    // Regular Verilog gate: The first one is always the output. Rest is input.
                    if (j==0) { // output
                        if (expression_ss > 1)  inst->Error("primitive %s connection must be a scalar var or net", "output") ;
                    } else { // input
                        if (expression_ss > 1)  inst->Warning("actual bit length %d differs from formal bit length %d", expression_ss, 1) ;
                    }
                    break ;
                }
            }
        }
    }
    return 1 ;
}

// Elaborate gate array like (and I[3:0] (....))
unsigned VeriGateInstantiation::ElaborateArrayInst(VeriModuleItem *mod, VeriPseudoTreeNode *node, Map *top_nodes, Map *mod_map_tab)
{
    if (!mod) return 1 ;
    VeriInstId *inst = 0 ;
    if (_insts && (_insts->Size() > 0)) inst = (VeriInstId*)_insts->At(0) ;
    if (inst && inst->HasNamedPortAssoc()) return 0 ;
    VeriRange *range = (inst) ? inst->GetRange(): 0 ; // Get instance range
    if (!range) return 1 ; // Not array instance, return

    int msb = 0, lsb = 0 ;
    unsigned instance_count =  range->GetWidth(&msb, &lsb, 0) ; // Get number of instances
    VeriScope *mod_scope = mod->GetScope() ; // Scope of instantiating module

    if ((instance_count == 1) && inst) (void) inst->ReplaceChildRangeInternal(range, 0, 1) ; // Delete range if instance count is one
    // Some net objects, instances are created for elaboration. The declarations
    // of these objects along with the created instantiations are stored in the
    // following array to be added to the module where 'this' is defined.
    Array items_to_add(instance_count) ;

    Array *stored_splits = 0 ;
    Array *terminal_list = inst ? inst->GetPortConnects(): 0 ;
    // Array of arrays to store split actuals. For each actual an array is created
    // to store split elements. Actual is split if its size is multiple of
    // formal, otherwise actual is copied instance range width times and inserted in the array.
    Array *array_of_splits = new Array(terminal_list ? terminal_list->Size(): 1) ;

    unsigned i ;
    VeriPortConnect *org_term ;
    FOREACH_ARRAY_ITEM(terminal_list, i, org_term) {
        if (!org_term) continue ; // analyze error

        // Get each terminal
        VeriExpression *term = org_term ;
        if (org_term->NamedFormal()) term = org_term->GetConnection() ;
        if (!term) continue ; // analyze error

        VeriMapForCopy old2new ;
        if (term->IsOpen()) {
            // open connection, copy the actual instance range width times
            stored_splits = new Array(instance_count) ;
            VeriExpression *copy_term ;
            unsigned k ;
            for (k = 0; k < instance_count; k++) {
                 copy_term = org_term->CopyExpression(old2new) ;
                 stored_splits->InsertLast(copy_term) ;
            }
            array_of_splits->Insert(stored_splits) ;
            continue ;
        }
        unsigned master_width = 1 ;
        // Get the direction of the formal
        unsigned dir = VeriPseudoTreeNode::GetGateTermDirection(i, terminal_list->Size(), this) ;

        unsigned elab_is_possible = 1 ;
        // Spliting the terminal and storing in an array
        stored_splits = term->SplitTerm(master_width, 0, instance_count, node, top_nodes, mod_map_tab, 0) ;
        if (!stored_splits && !Message::ErrorCount()) { // complex expression, can only be input
            verific_int64 temp_ss = term->StaticSizeSignInternal(0, 0) ;
            unsigned term_width = (unsigned)ABS(temp_ss) ;
            unsigned term_sign = (temp_ss < 0) ? 1 : 0 ;
            if ((term_width != (instance_count * master_width)) && (term_width != master_width)) {
                term->Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
                elab_is_possible = 0 ;
            } else {
                if (dir == VERI_INPUT) { // input
                    VeriIdDef *temp_id = 0 ;
                    // Declare vector/scalar net and the terminal expression is set assigned to it.
                    if (term_width == (instance_count * master_width)) {
                        (void) VeriPseudoTreeNode::CreateDecl(mod_scope, term_width, term_sign, term->CopyExpression(old2new), &temp_id, items_to_add) ;
                    }
                    // create actuals for each single instantiation from the created net object
                    if (temp_id) {
                        VeriIdRef id_ref(temp_id) ;
                        id_ref.SetLinefile(temp_id->Linefile()) ;
                        stored_splits = id_ref.SplitTerm(master_width, 0, instance_count, node, top_nodes, mod_map_tab, 0) ;
                    } else if (term_width == master_width) {
                        stored_splits = new Array(instance_count) ;
                        VeriExpression *copy_term ;
                        unsigned k ;
                        for (k = 0; k < instance_count; k++) {
                             copy_term = org_term->CopyExpression(old2new) ;
                             stored_splits->InsertLast(copy_term) ;
                        }
                    }
                } else { // formal inout/output, but expression other than id-ref, selct, concat
                    elab_is_possible = 0 ;
                    Warning("the actual is illegally connected to inout/output port of gate '%s'", PrintToken(_inst_type)) ;
                }
            }
        }
        if (!elab_is_possible || Message::ErrorCount()) { // Elaboration not possible, return
            // Delete created stuffs and produce warning
            Array *ele_array ;
            unsigned j ;
            FOREACH_ARRAY_ITEM(array_of_splits, j, ele_array) {
                unsigned k ;
                VeriExpression *created_expr ;
                FOREACH_ARRAY_ITEM(ele_array, k, created_expr) delete created_expr ;
                delete ele_array ;
            }
            delete array_of_splits ;
            return 1 ;
        }
        array_of_splits->InsertLast(stored_splits) ;
    }

    // Drive strength, delay and attributes should be inherited by all created instances
    unsigned idx = 0 ;

    // 'instance_count' gives the number of instantiations to be created. The
    // following loop will create those instantiations.
    while(idx < instance_count) {
        // create new terminal list
        Array *new_term_list = 0 ;
        if (terminal_list) {
            new_term_list = new Array(terminal_list->Size()) ;
            Array *i_th_array ;
            // fetch proper actuals from previously stored arrays
            FOREACH_ARRAY_ITEM(array_of_splits, i, i_th_array) {
                VeriExpression *new_term = i_th_array ? (VeriExpression *)i_th_array->At(idx) : 0 ;
                /* Gate instantiation cannot have named association. if exists, we return from
                   this function without processing anything :
                VeriExpression *new_org_term = (VeriExpression *)terminal_list->At(i) ;
                if (new_org_term && new_org_term->NamedFormal()) {
                    linefile_type lf = (new_term) ? new_term->Linefile() : 0 ;
                    new_term = new VeriPortConnect(Strings::save(new_org_term->NamedFormal()), new_term) ;
                    new_term->SetLinefile(lf) ;
                }*/
                new_term_list->InsertLast(new_term) ;
            }
        }
        VeriMapForCopy old2new ;
        // Copy the strength
        VeriStrength *new_strength = _strength ? _strength->Copy(old2new) : 0 ;
        // Copy the parameter overridden list
        Array *new_delay = CopyExpressionArray(_delay, old2new) ;
        // Copy the attributes
        Map *new_attr = CopyAttributes(GetAttributes(), old2new) ;
        int instance_id_idx = 0 ;
        // The index of the instance identifier will depend on msb and lsb of the range.
        // Index will be created staring from msb of the range expression
        instance_id_idx = (msb > lsb) ? (msb - (int)idx): (msb + (int)idx) ;

        // Create new instance, with indexed name :
        VeriInstId *new_inst = VeriPseudoTreeNode::CreateInstanceId(inst, instance_id_idx, new_term_list, mod_scope) ;

        // Create instantiation
        VeriGateInstantiation *new_gate_inst = new VeriGateInstantiation(_inst_type, new_strength, new_delay, new_inst, 0) ;
        new_gate_inst->SetAttributes(new_attr) ;
        new_gate_inst->SetLinefile(Linefile()) ;

        // Add the created instance to 'items_to_add', so that it is inserted in module item list
        items_to_add.Insert(new_gate_inst) ;
        idx++ ;
    }
    // Delete the created arrays
    Array *created_terms ;
    FOREACH_ARRAY_ITEM(array_of_splits, i, created_terms) delete created_terms ;
    delete array_of_splits ;
    // Insert created declaration(s), instantiations in module and delete array-instance
    (void) mod->ReplaceChildBy(this, &items_to_add) ;
    return 1 ;
}

// VIPER #3062: Internal class to break the array instance actuals by indexing in multiple dimensions.
// This is a linked list of all the indexable dimensions including the base (width-1 to 0).
// For example:
//     int a [1:0][3:0]
// We will create 3 classes like this:
//     [1:0] -> [3:0] -> [31:0]
//     dim 0 -> dim 1 -> base width is 32
// The main utility routine is GetIndices. It takes the formal size, full actual size
// and the size of the array instance. It then creates the indices to be selected from
// the actual. Based on that we create new broken actuals to be connected to the
// flattened instances. Previously we only indexed in a single dimension.

// This is the abstract base class to represent the linked list of ranges.
// we have two derived classes that stores constant and specific non-constant ranges:
class VeriArrayRange
{
protected: // Abstract class
    VeriArrayRange(int current_index, unsigned is_base_type_unpacked) ;

public:
    virtual ~VeriArrayRange() ;

private:
    // Prevent compiler from defining the following
    VeriArrayRange() ;                                   // Purposely leave unimplemented
    VeriArrayRange(const VeriArrayRange &) ;             // Purposely leave unimplemented
    VeriArrayRange & operator=(const VeriArrayRange &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Add next dimension at the end of the linked list
    void              AddDimension(VeriArrayRange *range) ;

    // Inserts VeriExpression * in the given Array 'indices', 'from' is used for line-file info and error reporting:
    virtual void      GetIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, Array &indices, VeriName *from) = 0 ;

    // Inserts char *in the given Array 'indices'. This is used for the actuals those are in turn instances (like interface instance)
    // char *prefix contains the index upto this dimension (the first invocation should pass the name of the instance identifier):
    virtual void      GetInstanceIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, const char *prefix, Array &indices) = 0 ;

    // Returns the multiplied value of size of each dimensions:
    virtual unsigned  Size() const = 0 ;

    // Set/reset access routines for _fully_indexed member:
    unsigned          IsFullyIndexed() const     { return _fully_indexed ; }
    void              SetFullyIndexed()          { _fully_indexed = 1 ; }
    void              ResetFullyIndexed()        { _fully_indexed = 0 ; }

protected:
    // Increase (or decrease) the value of the _current_index by width depending on whether the range is 'to' or 'down to':
    virtual void      NextIndex(unsigned width) = 0 ;

protected:
    VeriArrayRange *  _next ;                    // The next range of this linked list
    int               _current_index ;           // The current index selected for GetIndices/GetInstanceIndices
    unsigned          _is_base_type_unpacked:1 ; // Flag to store whether this is an unpacked dimension (only set for base type and that too if it is unpacked)
    unsigned          _fully_indexed:1 ;         // Flag to indicate that the full range is selected. Set when '_current_index' reaches '_right' and back reset to '_left'
    /* unsigned       _un_used_bits:30 ;  */     // Number of unused bits that can be used if needed
} ; // class VeriArrayRange

/* -------------------------------------------------------------- */

// Class for range for which both left and right bounds are constants:
class VeriConstArrayRange : public VeriArrayRange
{
public:
    VeriConstArrayRange(int left, int right, unsigned is_base_type_unpacked = 0) ; // Normal constructor
    explicit VeriConstArrayRange(const VeriRange *range) ; // Constructor with VeriRange : takes the info from the constant range
    virtual ~VeriConstArrayRange() { /* nothing to do */ ; }

private:
    // Prevent compiler from defining the following
    VeriConstArrayRange() ;                                        // Purposely leave unimplemented
    VeriConstArrayRange(const VeriConstArrayRange &) ;             // Purposely leave unimplemented
    VeriConstArrayRange & operator=(const VeriConstArrayRange &) ; // Purposely leave unimplemented

public:
    // Inserts VeriExpression * in the given Array 'indices', 'from' is used for line-file info and error reporting:
    virtual void      GetIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, Array &indices, VeriName *from) ;

    // Inserts char *in the given Array 'indices'. This is used for the actuals those are in turn instances (like interface instance)
    // char *prefix contains the index upto this dimension (the first invocation should pass the name of the instance identifier):
    virtual void      GetInstanceIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, const char *prefix, Array &indices) ;

    // Returns the multiplied value of size of each dimensions:
    virtual unsigned  Size() const ;

protected:
    // Increase (or decrease) the value of the _current_index by width depending on whether the range is 'to' or 'down to':
    virtual void      NextIndex(unsigned width) ;

protected:
    int               _left ;   // The constant left (MSB) index of this range
    int               _right ;  // The right (LSB) index of this range
} ; // class VeriConstArrayRange

/* -------------------------------------------------------------- */

// Class for +:, -: type range with non-constant left and constant width:
class VeriNonConstArrayRange : public VeriArrayRange
{
public:
    VeriNonConstArrayRange(VeriExpression *left, unsigned width, unsigned part_select_token) ;
    virtual ~VeriNonConstArrayRange() ;

private:
    // Prevent compiler from defining the following
    VeriNonConstArrayRange() ;                                           // Purposely leave unimplemented
    VeriNonConstArrayRange(const VeriNonConstArrayRange &) ;             // Purposely leave unimplemented
    VeriNonConstArrayRange & operator=(const VeriNonConstArrayRange &) ; // Purposely leave unimplemented

public:
    // Inserts VeriExpression * in the given Array 'indices', 'from' is used for line-file info and error reporting:
    virtual void      GetIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, Array &indices, VeriName *from) ;

    // Inserts char *in the given Array 'indices'. This is used for the actuals those are in turn instances (like interface instance)
    // char *prefix contains the index upto this dimension (the first invocation should pass the name of the instance identifier):
    virtual void      GetInstanceIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, const char *prefix, Array &indices) ;

    // Returns the multiplied value of size of each dimensions:
    virtual unsigned  Size() const ;

protected:
    // Increase (or decrease) the value of the _current_index by width depending on whether the range is 'to' or 'down to':
    virtual void      NextIndex(unsigned width) ;

protected:
    VeriExpression *  _left ;              // The non-constant left (MSB) index of this range for [i +: 2] type range
    unsigned          _width ;             // The width of this non constant range
    unsigned          _part_select_token ; // The part select token of this non-constant range
} ; // class VeriNonConstArrayRange

/* -------------------------------------------------------------- */

VeriArrayRange::VeriArrayRange(int current_index, unsigned is_base_type_unpacked)
  : _next(0),
    _current_index(current_index),
    _is_base_type_unpacked(is_base_type_unpacked),
    _fully_indexed(0)
{
}

VeriArrayRange::~VeriArrayRange()
{
    delete _next ;
}

void VeriArrayRange::AddDimension(VeriArrayRange *range)
{
    // Add the range at the end of the linked list:
    if (_next) {
        _next->AddDimension(range) ;
    } else {
        _next = range ;
    }
}

VeriConstArrayRange::VeriConstArrayRange(int left, int right, unsigned is_base_type_unpacked /* = 0 */)
  : VeriArrayRange(left /* current index */, is_base_type_unpacked),
    _left(left),
    _right(right)
{
}

VeriConstArrayRange::VeriConstArrayRange(const VeriRange *range)
  : VeriArrayRange(0 /* will be set later */, 0 /* it can't be range of the base type, so assume packed */),
    _left(0),
    _right(0)
{
    // Get the left, right bounds from the given VeriRange:
    // It may be a non-constance range, in that case we get it as [0:0]
    if (range) (void) range->GetWidth(&_left, &_right, 0 /* value table */) ;
    _current_index = _left ; // Initialize this to the MSB, we start the split from MSB
}

void VeriConstArrayRange::GetIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, Array &indices, VeriName *from)
{
    // Check whether we need to index any more or we are done:
    if (formal_size >= actual_size) {
        // Set this flag so that next time the next index is selected:
        _fully_indexed = 1 ;
        return ; // Already done in the previous range
    }

    // Here index into this dimension depending on various constraints.
    // First check if we are doing something illegal by indexing into an unpacked base type:
    if (_is_base_type_unpacked) {
        if (from) from->Error("cannot index into unpacked base of %s", from->GetName()) ;
        // Clean-up the created indices and return:
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(&indices, i, expr) delete expr ;
        indices.Reset() ;
        return ;
    }

    // Find out the size of this dimension, we need to later:
    unsigned my_size = (unsigned)(ABS((int)_right - (int)_left) + 1) ;

    // Check whether we need to finally select in this dimension:
    if (!_next || (my_size > instance_count)) {
        // This is the last dimension or it has more elements that the number of instances,
        // in such case we must index such a way that it has same size as 'formal_size':
        unsigned unit_size = actual_size / my_size ; // Find the unit size of each element in this range
        // Find the width we need to select so that it becomes equal to formal size:
        unsigned width = (unit_size) ? formal_size / unit_size : formal_size ;
        // Create the index (MSB) starting from the current index:
        // May be a single bit or multi-bit indexing create the left side first:
        VeriExpression *idx = new VeriIntVal(_current_index) ;
        // Find the position where the index (LSB) should end to be of size formal size:
        int lsb = (_right > _left) ? ((_current_index + (int)width) - 1) : ((_current_index - (int)width) + 1) ;
        if (lsb != _current_index) {
            // Multi-bit part select, create a VeriRange:
            VeriIntVal *right = new VeriIntVal(lsb) ;
            if (from) {
                idx->SetLinefile(from->Linefile()) ;
                right->SetLinefile(from->Linefile()) ;
            }
            idx = new VeriRange(idx, right) ;
        }
        if (from) idx->SetLinefile(from->Linefile()) ;
        indices.InsertLast(idx) ;
        // Go to the next index for the next iteration:
        NextIndex(width) ;
        return ;
    }

    // We should select further, just select a single bit from this dimension:
    VeriIntVal *idx = new VeriIntVal(_current_index) ;
    if (from) idx->SetLinefile(from->Linefile()) ;
    indices.InsertLast(idx) ;

    // Go indexing into the next dimension with the adjusted actual size after indexing in this dimension:
    _next->GetIndices(formal_size, (actual_size / my_size), instance_count, indices, from) ;

    // Check whether the next dimension have selected a full range:
    if (_next->IsFullyIndexed()) {
        // Reset the flag of the next dimension now
        _next->ResetFullyIndexed() ;
        NextIndex(1) ; // Go to the next index in my context
    }
}

void VeriConstArrayRange::GetInstanceIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, const char *prefix, Array &indices)
{
    if (!prefix) return ;

    // Check whether we need to index any more or we are done:
    if (formal_size >= actual_size) {
        // Insert a copy of the prefix into the indices Array:
        indices.InsertLast(Strings::save(prefix)) ;
        // Set this flag so that next time the next index is selected:
        _fully_indexed = 1 ;
        return ; // Done
    }

    // Find out the size of this dimension, we need to later:
    unsigned my_size = (unsigned)(ABS((int)_right - (int)_left) + 1) ;

    // Check whether we need to finally selected in this dimension:
    char idx[32] ;
    if (!_next || (my_size > instance_count)) {
        // This is the last dimension or it has more elements that the number of instances,
        // in such case we must index such a way that it has same size as 'formal_size':
        unsigned unit_size = actual_size / my_size ; // Find the unit size of each element in this range
        // Find the width we need to select so that it becomes equal to formal size:
        unsigned width = (unit_size) ? formal_size/unit_size : formal_size ;
        int finish = (_right > _left) ? (_current_index + (int)width) : (_current_index - (int)width) ;
        // Create the indices by taking care of the 'to' or 'down to' range:
        for (int i=_current_index; (_right>_left) ? (i<finish) : (i>finish); (_right>_left) ? i++ : i--) {
            (void) sprintf(idx, "[%d]", i) ;
            indices.InsertLast(Strings::save(prefix, idx)) ;
        }
        // Go to the next index for the next iteration:
        NextIndex(width) ;
        return ;
    }

    // We should select further, just select a single bit from this dimension:
    (void) sprintf(idx, "[%d]", _current_index) ;
    char *new_prefix = Strings::save(prefix, idx) ;

    // Go indexing into the next dimension with the adjusted actual size after indexing in this dimension:
    _next->GetInstanceIndices(formal_size, (actual_size / my_size), instance_count, new_prefix, indices) ;

    Strings::free(new_prefix) ; // Free the created new indexed prefix

    // Check whether the next dimension have selected a full range:
    if (_next->IsFullyIndexed()) {
        // Reset the flag of the next dimension now
        _next->ResetFullyIndexed() ;
        NextIndex(1) ; // Go to the next index in my context
    }
}

unsigned VeriConstArrayRange::Size() const
{
    // Return the size of all the ranges:
    unsigned size = (unsigned)(ABS((int)_right - (int)_left) + 1) ; // this dimension size
    return size * ((_next) ? _next->Size() : 1) ; // multiply with next dimension size
}

void VeriConstArrayRange::NextIndex(unsigned width)
{
    // Set the _current_index from _current_index to next width position.
    // Reset back to left index if it goes out-of-bounds and set the flag properly:
    if (_right >= _left) {
        _current_index = _current_index + (int)width ;
        if (_current_index > _right) {
            _current_index = _left ;
            _fully_indexed = 1 ;
        }
    } else {
        _current_index = _current_index - (int)width ;
        if (_current_index < _right) {
            _current_index = _left ;
            _fully_indexed = 1 ;
        }
    }
}

VeriNonConstArrayRange::VeriNonConstArrayRange(VeriExpression *left, unsigned width, unsigned part_select_token)
  : VeriArrayRange(0 /* current index */, 0 /* it is not range of the base type, so assume packed */),
    _left(left),
    _width(width),
    _part_select_token(part_select_token)
{
}

VeriNonConstArrayRange::~VeriNonConstArrayRange()
{
    // Do not delete, just set it to 0, we don't own this pointer:
    _left = 0 ;
}

void VeriNonConstArrayRange::GetIndices(unsigned formal_size, unsigned actual_size, unsigned instance_count, Array &indices, VeriName *from)
{
    // Check whether we need to index any more or we are done:
    if (formal_size >= actual_size) {
        // Set this flag so that next time the next index is selected:
        _fully_indexed = 1 ;
        return ; // Already done in the previous range
    }

    // Here index into this dimension depending on various constraints.
    // First check if we are doing something illegal by indexing into an unpacked base type:
    if (_is_base_type_unpacked) {
        if (from) from->Error("cannot index into unpacked base of %s", from->GetName()) ;
        // Clean-up the created indices and return:
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(&indices, i, expr) delete expr ;
        indices.Reset() ;
        return ;
    }

    // Find out the size of this dimension, we need to later:
    unsigned my_size = (_width) ? _width : 1 ;

    // Check whether we need to finally select in this dimension:
    if (!_next || (my_size > instance_count)) {
        // This is the last dimension or it has more elements that the number of instances,
        // in such case we must index such a way that it has same size as 'formal_size':
        unsigned unit_size = actual_size / my_size ; // Find the unit size of each element in this range
        // Find the width we need to select so that it becomes equal to formal size:
        unsigned width = (unit_size) ? formal_size / unit_size : formal_size ;
        // Create the index (MSB) starting from the current index:
        VeriMapForCopy id_map_table ;
        VeriExpression *idx = _left->CopyExpression(id_map_table) ;
        if (_current_index) {
            VeriIntVal *left_start = new VeriIntVal(_current_index) ;
            if (from) left_start->SetLinefile(from->Linefile()) ;
            if (_part_select_token == VERI_PARTSELECT_UP) {
                idx = new VeriBinaryOperator(VERI_PLUS, idx, left_start) ;
            } else {
                idx = new VeriBinaryOperator(VERI_MIN, idx, left_start) ;
            }
        }
        VeriIntVal *range_width = new VeriIntVal((int)width) ;
        if (from) range_width->SetLinefile(from->Linefile()) ;
        idx = new VeriRange(idx, range_width, _part_select_token) ;
        if (from) idx->SetLinefile(from->Linefile()) ;
        indices.InsertLast(idx) ;
        // Go to the next index for the next iteration:
        NextIndex(width) ;
        return ;
    }

    // We should select further, just select a single bit from this dimension:
    VeriExpression *idx = 0 ;
    if (_current_index) {
        idx = new VeriIntVal(_current_index) ;
        if (from) idx->SetLinefile(from->Linefile()) ;
    }
    VeriMapForCopy id_map_table ;
    if (!idx) {
        idx = _left->CopyExpression(id_map_table) ;
    } else if (_part_select_token == VERI_PARTSELECT_UP) {
        idx = new VeriBinaryOperator(VERI_PLUS, _left->CopyExpression(id_map_table), idx) ;
    } else {
        idx = new VeriBinaryOperator(VERI_MIN, _left->CopyExpression(id_map_table), idx) ;
    }
    if (from) idx->SetLinefile(from->Linefile()) ;
    indices.InsertLast(idx) ;

    // Go indexing into the next dimension with the adjusted actual size after indexing in this dimension:
    _next->GetIndices(formal_size, (actual_size / my_size), instance_count, indices, from) ;

    // Check whether the next dimension have selected a full range:
    if (_next->IsFullyIndexed()) {
        // Reset the flag of the next dimension now
        _next->ResetFullyIndexed() ;
        NextIndex(1) ; // Go to the next index in my context
    }
}

void VeriNonConstArrayRange::GetInstanceIndices(unsigned /* formal_size */, unsigned /* actual_size */,
                             unsigned /* instance_count */, const char * /* prefix */, Array & /* indices */)
{
    // This should not be called for non-constant range, do nothing. Assert?
}

unsigned VeriNonConstArrayRange::Size() const
{
    // Return the size of all the ranges:
    unsigned size = (_width) ? _width : 1 ; // this dimension size, width can't be 0!
    return size * ((_next) ? _next->Size() : 1) ; // multiply with next dimension size
}

void VeriNonConstArrayRange::NextIndex(unsigned width)
{
    // Set the _current_index from _current_index to next width position.
    // Reset back to left index if it goes out-of-bounds and set the flag properly:
    _current_index = _current_index + (int)width ;
    if (_current_index >= (int)_width) {
        _current_index = 0 ;
        _fully_indexed = 1 ;
    }
}

/*
* Split actuals for array instance. Actual can have packed and unpacked dimensions for SV.

* reg [0:1][2:3] foo [4:5][6:7] ; [2:3] is most varying, [0:1] comes next, [6:7] comes next
  and [4:5] at last. foo[0] or for[3] or foo[6] is illegal, foo[4] and foo[5] is legal.
  foo[4][6] is also legal.

* If the formal has unpacked dimensions, the effective actual must have at least that number
  of unpacked dimensions with the same corresponding dimension size. For example:

    module top ;
      byte C [3:2][1:0][5:4] ;

      bot b[1:0] (C[3]) ;
    endmodule

    module bot (input [7:0] C [2:3]) ;

    endmodule

  is correct, because C[3] has two more unpacked dimension which is one more that the formal.
  Formal has one unpacked dimensions.

* If the actual or the formal has unpacked dimension then the packed size (sum of number of
  dimension * number of elements in that dimension for each packed dimension) of both formal
  and actual must eaxctly match. For example:

    module top ;
      byte b [1:0] ;

      bot b[1:0] (b) ;
    endmodule

    module bot(input [7:0] c) ;

    endmodule

  is correct, because both formal and actual have packed size 8. We 'select' in the least varying
  unpacked dimension if the actual has one more unpacked dimension than the formal, or connect as
  a whole if it does not have any unpacked dimension or have same number of unpacked dimensions.
  It is exactly like formal_width == actual_width.
*/
// Split 'this', size of each split will be 'master_width'. If size of 'this' is 'master_width',
// copy the expression 'instance_count' times, insert in an array and return
Array *VeriIdRef::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map * mod_map_tab, unsigned *select_in_up_dim)
{
    if (!master_width) return 0 ;
    (void) top_mods ;    // Needed to prevent "unused" build warnings
    (void) mod_map_tab ; // Needed to prevent "unused" build warnings
    if (!instance_count) {
        verific_int64 s = StaticSizeSignInternal(0, 0) ;
        instance_count = (unsigned)ABS(s) ;
    }

    VeriModuleItem *mod = node ? node->GetDesign() : 0 ;
    VeriScope *scope = mod ? mod->GetScope(): 0 ;
    unsigned term_width = 0 ;
    if (_id && (_id->IsInst() || _id->IsModport() || _id->IsInterfacePort())) { // Actual is interface instance
        // Calculate actual size. Consider only the dimensions of actual.
        // Do not consider interface size, as formal can have generic interface
        VeriRange *range = _id->GetDimensions() ;
        unsigned actual_size = 1 ;
        int msb = 0, lsb = 0 ;
        while(range) { // Calculate actual size
            actual_size = actual_size * range->GetWidth(&msb, &lsb, 0) ;
            range = range->GetNext() ;
        }
        term_width = actual_size ; // Assign actual size as term width
        if ((term_width == master_width) && (term_width != 1)) {
            // Actual is interface reference, terminal width is same as formal width,
            // but formal width is not 1. In this case break the terminal into
            // elements of size 1.
            Array *terms = SplitTerm(1, 0, instance_count, node, top_mods, mod_map_tab, 0) ;
            // Create a concatenation from broken elements
            VeriAssignmentPattern *concat = new VeriAssignmentPattern(0, terms) ;
            concat->SetLinefile(Linefile()) ;
            VeriMapForCopy id_map_table ;
            Array *storing_split_term = new Array(instance_count) ;
            storing_split_term->Insert(concat) ;
            for (unsigned i = 1; i < instance_count; i++) {
                VeriExpression *ele = concat->CopyExpression(id_map_table) ;
                // Only copy 'this' and return
                storing_split_term->Insert(ele) ;
            }
            return storing_split_term ;
        } else if ((term_width == 1) && scope) {
            // VIPER #7299: If actual is interface array instance reference, create expression
            // using elaborated (newly created) array instance identifier:
            VERIFIC_ASSERT(msb == lsb) ;
            int int_val = msb ; // or lsb, both are same as we asserted above
            char idx_str[10] ;
            sprintf(idx_str, "%d", int_val) ;
            char *name = Strings::save(_id->Name(), "[", idx_str, "]") ;
            VeriIdDef *new_def = scope->FindLocal(name) ;
            // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
            if (!new_def) new_def = (scope->IsGenerateScope()) ? scope->Find(name, 0): 0 ;
            if (new_def && new_def->IsInst()) {
                VeriMapForCopy id_map_table ;
                VeriName *new_ref = new VeriIdRef(new_def) ;
                new_ref->SetLinefile(Linefile()) ;
                Strings::free(name) ;
                Array *storing_split_term = new Array(instance_count) ;
                storing_split_term->Insert(new_ref) ;
                for (unsigned i = 1; i < instance_count; i++) {
                    VeriExpression *ele = new_ref->CopyExpression(id_map_table) ;
                    storing_split_term->Insert(ele) ;
                }
                return storing_split_term ;
            }
            Strings::free(name) ;
        }
    } else { // Actual is normal variable
        if (formal_id && formal_id->IsInterfacePort()) {
            Warning("formal port %s expects a modport or interface instance actual", formal_id ? formal_id->Name() : "") ;
            return 0 ;
        }
        // Calculate the width of this terminal
        verific_int64 this_ss = StaticSizeSignInternal(0, 0) ;
        term_width = (unsigned)ABS(this_ss) ;
    }

    // Check validity of this terminal and issue warning message
    if ((term_width != master_width) && ((master_width * instance_count) != term_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        return 0 ;
    }
    if (term_width == master_width) { // formal_width == actual_width
        // Copy 'this' instance_count' times and return
        Array *storing_split_term = new Array(instance_count) ;
        VeriMapForCopy id_map_table ;
        for(unsigned i = 0 ; i < instance_count; i++) {
            // Only copy 'this'
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }

    if (formal_id && _id) {
        // Some checks for arrays and its packed/unpacked dimensions
        unsigned formal_unpacked_dim = formal_id->UnPackedDimension() ;
        unsigned actual_unpacked_dim = _id->UnPackedDimension() ;
        if (actual_unpacked_dim) {
            VeriRange *actual_up_rng = _id->GetDimensionAt(0) ;
            verific_int64 actual_up_rng_ss = (actual_up_rng) ? actual_up_rng->StaticSizeSignInternal(0, 0) : 0 ;
            unsigned actual_up_width = (unsigned)ABS(actual_up_rng_ss) ;
            VeriRange *formal_up_rng = formal_unpacked_dim ? formal_id->GetDimensionAt(0) : 0 ;
            verific_int64 formal_up_rng_ss = (formal_up_rng) ? formal_up_rng->StaticSizeSignInternal(0, 0) : (int)actual_up_width ;
            unsigned formal_up_width = (unsigned)ABS(formal_up_rng_ss) ;
            // Get the size of the next unpacked dimension, if it is there:
            // Since 'this' is an id-ref, dimension 0 will be selected, next dimension is 1.
            VeriRange *next_dim = (actual_unpacked_dim > 1) ? _id->GetDimensionAt(1) : 0 ;
            verific_int64 next_dim_ss = (next_dim) ? next_dim->StaticSizeSignInternal(0, 0) : 0 ;
            unsigned next_dim_width = (unsigned)ABS(next_dim_ss) ;
            // If the actual has one more number of unpacked dimension then it must have
            // same number of element as that of the formal. We will select in the next dimension.
            // Or if there is not same number of element, the next dimension should have same
            // number of element and we will select in the first dimension, i.e., in 0th dimension.
            // If the actual has same number of unpacked dimension as that of the formal then, it
            // must have instance_count times element in it, so that we can select into it.
            if (!(((formal_unpacked_dim == actual_unpacked_dim-1) && ((actual_up_width == formal_up_width) ||
                  /* or if we are selecting in 0th dimension then: */ (next_dim_width == formal_up_width))) ||
                  ((formal_unpacked_dim == actual_unpacked_dim  ) && (actual_up_width == instance_count*formal_up_width)))) {
                Error("incompatible number of unpacked dimensions in instantiation") ;
                return 0 ;
            }

            // If number of unpacked dimensions match, their corresponding size should also match,
            // except the least varying one, on which we will select and split the actual
            int msb, lsb ;
            for(unsigned i=1; i<actual_unpacked_dim; i++) {
                // Each corresponding dimension must have same size
                VeriRange *formal_this_dim = formal_id->GetDimensionAt(i-1) ;
                VeriRange *id_this_dim = _id->GetDimensionAt(i) ;
                if (formal_this_dim && id_this_dim && (formal_this_dim->GetWidth(&msb, &lsb, 0) != id_this_dim->GetWidth(&msb, &lsb, 0))) {
                    Error("size of the corresponding unpacked dimensions do not match") ;
                    return 0 ;
                }
            }

            // VIPER #3265: We don't need this check. We already checked the total size as well
            // as the number of unpacked dimensions and their corresponding sizes. So the packed
            // size is bound to be equal. Also, the check became wrong due to SV typedef. With
            // typedef, data type can include unpacked dimensions. Enhancing/adjusting the check
            // does not help either. Because it is possible to select into packed dimensions as
            // well as unpacked dimensions in the same instance. So, if we really need to check
            // then it should be the size of formal vs. the actual after splitting and that is
            // already done all the way above. So, just comment this code part out.
            /* if (!_id->IsInst()) { // Check packed size only for normal variables
                // Packed size of both the formal and actual should exactly match
                // If it has data type, then packed size is the size of the data type (including the
                // packed dimensions), otherwise, it is a single bit element
                VeriDataType *id_data_type = _id->GetDataType() ;
                int id_dt_ss = (id_data_type) ? id_data_type->StaticSizeSignInternal(0, 0) : 1 ;
                unsigned my_packed_size = (unsigned)ABS(id_dt_ss) ;
                VeriDataType *formal_data_type = formal_id->GetDataType() ;
                int formal_dt_ss = (formal_data_type) ? formal_data_type->StaticSizeSignInternal(0, 0) : 1 ;
                unsigned formal_packed_size = (unsigned)ABS(formal_dt_ss) ;

                if (formal_unpacked_dim && (my_packed_size != formal_packed_size)) {
                    // Packed size of both the formal and the actual does not match!
                    error("packed size mismatch between formal and actual") ;
                    return 0 ;
                }
            } */
        } else if (formal_unpacked_dim) {
            // Actual does not have any unpacked dimension, but formal has
            // and the actual is directly connected - it is an error
            Error("unpacked dimension mismatch between formal and actual") ;
            return 0 ;
        }
    }

    // We will return this array with the broken actuals:
    Array *storing_split_term = new Array(instance_count) ;

    // Get the first (un)packed dimension, if any - it is the least varying dimension
    VeriRange *least_varying_dim = (_id) ? _id->GetDimensionAt(0) : 0 ;
    VeriArrayRange *dummy_dim = 0 ;
    if (least_varying_dim) {
        if (select_in_up_dim) *select_in_up_dim = 1 ;
        VERIFIC_ASSERT(_id) ; // It must be there, since we found a dimension of it from the identifier
        dummy_dim = new VeriConstArrayRange(least_varying_dim) ;
        // Get the number of total (packed and unpacked) dimensions we can select:
        unsigned dims = _id->PackedDimension() + _id->UnPackedDimension() ;
        for (unsigned i=1; i<dims; i++) {
            VeriRange *rng = _id->GetDimensionAt(i) ;
            // Add the next dimensions at the end of this linked list:
            dummy_dim->AddDimension(new VeriConstArrayRange(rng)) ;
        }
        // Get the base data type where we are stopped with the dimensions:
        VeriDataType *base_type = _id->GetBaseDataType() ;
        // Get the size of all the dimensions we can index into:
        unsigned dim_size = dummy_dim->Size() ;
        VERIFIC_ASSERT(dim_size) ; // Size should never be 0
        // Get the base size of the actual (size of the base data type):
        unsigned base_size = term_width / dim_size ;
        // Add the last dimension with the base size (if it is packed we can index into it, otherwise not):
        dummy_dim->AddDimension(new VeriConstArrayRange((int)base_size-1, 0, ((base_type) ? base_type->IsUnpacked() : 0))) ;
    } else {
        // It does not have any dimension at all (packed or unpacked)
        VeriDataType *id_data_type = (_id) ? _id->GetDataType(): 0 ;
        // VIPER #6160: Packed struct/union types can be broken bit by bit, so check for not-packed flag:
        if (id_data_type && (id_data_type->IsEnumType() || ((id_data_type->IsUnionType() || id_data_type->IsStructType()) && !id_data_type->IsPacked()))) {
            // Can't do anything other than copying and returning 'this'. Becasue,
            // they can't be treated as arrays, so we can't really select on these
            // type of elements, produce error and copy the term itself:
            //Error("cannot split unpacked item without dimension") ;
            Error("cannot index into unpacked base of %s", ((_id)?_id->Name():((_name)?_name:"<unknown>"))) ;
            for (unsigned i = 0; i < instance_count; i++) {
                VeriMapForCopy id_map_table ;
                // Only copy 'this'
                storing_split_term->Insert(CopyExpression(id_map_table)) ;
            }
            return storing_split_term ;
        }
        // It is like int/integer/byte etc, so the size of the dimension is in term_width:
        dummy_dim = new VeriConstArrayRange((int)term_width-1, 0) ;
    }

    VERIFIC_ASSERT(dummy_dim) ; // It must be there, we have already created it

    VeriScope *defined_scope = 0 ;
    if (_id && _id->IsInst() && _id->GetDimensions()) {
        // If _id is array instance identifier, we have to pick proper instance
        // identifier created due to array instance elaboration. To find those use
        // the scope where _id is defined
        defined_scope = scope ; // Get the scope where _id is defined
    }
    // Now split the actual in the number of instances:
    for (unsigned i = 0; i < instance_count; i++) {
        unsigned j ;
        VeriExpression *ret_term = 0 ;
        if (defined_scope && _id) { // Formal is interface instance id
            Array indices ;
            // Get the indices that should be slected for this array instance flattening:
            dummy_dim->GetInstanceIndices(master_width, term_width, instance_count, _id->Name() /* prefix */, indices) ;
            char *name = 0 ;
            if (indices.Size() > 1) {
                // Find and insert all the instance ids into this array with the name:
                Array *exprs = new Array(indices.Size()) ;
                FOREACH_ARRAY_ITEM(&indices, j, name) {
                    VeriIdDef *new_suffix = defined_scope->FindLocal(name) ;
                    // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
                    if (!new_suffix) new_suffix = (defined_scope->IsGenerateScope()) ? defined_scope->Find(name, 0): 0 ;
                    VeriIdRef *new_suffix_ref = new VeriIdRef(new_suffix) ;
                    new_suffix_ref->SetLinefile(Linefile()) ;
                    exprs->InsertLast(new_suffix_ref) ;
                    Strings::free(name) ;
                }
                // Create a concat with all the instance identifiers:
                if (IsSystemVeri()) {
                    ret_term =  new VeriAssignmentPattern(0, exprs) ;
                } else {
                    ret_term =  new VeriConcat(exprs) ;
                }
            } else if (indices.Size()) {
                // Find the instance id of the indexed name:
                name = (char *)indices.GetFirst() ;
                VeriIdDef *new_suffix = defined_scope->FindLocal(name) ;
                // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
                if (!new_suffix) new_suffix = (defined_scope->IsGenerateScope()) ? defined_scope->Find(name, 0): 0 ;
                Strings::free(name) ;
                // VIPER #5523: Do not create an id-ref with NULL id and no name (VIPER #7308)
                ret_term = (new_suffix) ? new VeriIdRef(new_suffix) : 0 ;
            }
        } else { // formal is normal variable
            Array *indices = new Array() ;
            dummy_dim->GetIndices(master_width, term_width, instance_count, *indices, this) ;
            VeriMapForCopy old2new ;
            if (indices->Size() > 1) {
                // Multiple indices : create indexed memory id for this:
                ret_term = new VeriIndexedMemoryId(CopyName(old2new), indices) ;
            } else if (indices->Size()) {
                // Single index : create indexed id only:
                ret_term = new VeriIndexedId(CopyName(old2new), (VeriExpression *)indices->GetFirst()) ;
                delete indices ;
            }
            if (ret_term) ret_term->SetId(_id) ; // Set the id of the created indexed/indexed memory id
        }
        if (!ret_term) {
            // Something bad happened, clean-up and return 0 from here:
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(storing_split_term, j, expr) delete expr ;
            delete storing_split_term ;
            delete dummy_dim ; // Memory leak fix (VIPER #5523)
            return 0 ;
        }
        // Set the proper line-file information of the new parse tree node created:
        ret_term->SetLinefile(Linefile()) ;
        storing_split_term->Insert(ret_term) ;
    }

    // Delete the created dummy dimension for splitting
    delete dummy_dim ;

    return storing_split_term ;
}

// Split 'this', size of each split will be 'master_width'. If size of 'this' is
// 'master_width', copy the expression 'instance_count' times, insert in an array and return
Array *VeriIndexedId::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned *select_in_up_dim)
{
    if (!_prefix || !master_width) return 0 ;

    VeriModuleItem *mod = node ? node->GetDesign(): 0 ;
    VeriScope *scope = mod ? mod->GetScope(): 0 ;
    // If name is not resolved, try to resolve it
    if (!_id) {
        HierIdWithDeclScope tmp_id(_prefix, scope) ;
        _prefix->ResolveName(&tmp_id, node, top_mods, mod_map_tab, 0) ;

        VeriIdDef *resolved_id = tmp_id.GetResolvedIddef() ;
        if (resolved_id) {
            (void) _prefix->Bind(&tmp_id, node, 0, top_mods, mod_map_tab, 0) ;
            _id = resolved_id ;
            if (_prefix->IsHierName()) {
                // If prefix is hierarchical identifier, resolved identifier is not
                // defined in 'scope'. So update 'scope' and 'node' from hierid
                VeriPseudoTreeNode *last_node = tmp_id.GetLastReferencedNode() ;
                VeriModuleItem *design = (last_node) ? last_node->GetDesign(): 0 ;
                if (last_node) node = last_node ;
                if (design) scope = design->GetScope() ;
            }
        }
    }
    if (!instance_count) {
        verific_int64 s = StaticSizeSignInternal(0, 0) ;
        instance_count = (unsigned)ABS(s) ;
    }
    // Calculate the width of this terminal
    unsigned term_width = 0 ;
    if (_id && (_id->IsInst() || _id->IsModport() || _id->IsInterfacePort())) { // Actual is interface instance
        // Calculate actual size. Consider only the index expression
        // Do not consider interface size, as formal can have generic interface
        if (_idx && _idx->IsRange()) { // Calculate actual size
            verific_int64 idx_ss = _idx->StaticSizeSignInternal(0, 0) ;
            term_width = (unsigned)ABS(idx_ss) ;
        } else {
            term_width = 1 ;
        }
        if ((term_width == master_width) && (term_width != 1)) {
            // Actual is interface reference, terminal width is same as formal width,
            // but formal width is not 1. In this case break the terminal into
            // elements of size 1. Pass term_width as instance count so that the split is of size 1:
            Array *terms = SplitTerm(1, 0, term_width, node, top_mods, mod_map_tab, 0) ;
            // Create a concatenation from broken elements
            VeriAssignmentPattern *concat = new VeriAssignmentPattern(0, terms) ;
            concat->SetLinefile(Linefile()) ;
            VeriMapForCopy id_map_table ;
            Array *storing_split_term = new Array(instance_count) ;
            storing_split_term->Insert(concat) ;
            for (unsigned i = 1; i < instance_count; i++) {
                VeriExpression *ele = concat->CopyExpression(id_map_table) ;
                // Only copy 'this' and return
                storing_split_term->Insert(ele) ;
            }
            return storing_split_term ;
        } else if ((term_width == 1) && scope) {
            // If actual is interface array instance reference, create expression
            // using array instance elaboration created new instance identifiers :
            VeriBaseValue *idx_val =  _idx ? _idx->StaticEvaluateInternal(0, 0, 0, 0): 0 ;
            if (idx_val) {
                int int_val = idx_val->GetIntegerValue() ;
                delete idx_val ;
                char idx_str[10] ;
                sprintf(idx_str, "%d", int_val) ;
                char *name = Strings::save(_id->Name(), "[", idx_str, "]") ;
                VeriIdDef *new_def = scope->FindLocal(name) ;
                // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
                if (!new_def) new_def = (scope->IsGenerateScope()) ? scope->Find(name, 0): 0 ;
                if ( new_def && new_def->IsInst()) {
                    VeriName *new_suffix_ref = 0 ;
                    VeriMapForCopy id_map_table ;
                    if (_prefix->IsHierName() && _prefix->GetPrefix()) {
                        VeriName *prev_prefix = _prefix->GetPrefix() ;
                        new_suffix_ref = new VeriSelectedName(prev_prefix ? prev_prefix->CopyName(id_map_table): 0, Strings::save(name)) ;
                        new_suffix_ref->SetId(new_def) ;
                    } else {
                        new_suffix_ref = new VeriIdRef(new_def) ;
                    }
                    new_suffix_ref->SetLinefile(Linefile()) ;
                    Strings::free(name) ;
                    Array *storing_split_term = new Array(instance_count) ;
                    storing_split_term->Insert(new_suffix_ref) ;
                    for (unsigned i = 1; i < instance_count; i++) {
                        VeriExpression *ele = new_suffix_ref->CopyExpression(id_map_table) ;
                        storing_split_term->Insert(ele) ;
                    }
                    return storing_split_term ;
                }
                Strings::free(name) ;
            }
        }
    } else { // Actual is normal variable
        if (formal_id && formal_id->IsInterfacePort()) {
            Warning("formal port %s expects a modport or interface instance actual", formal_id ? formal_id->Name() : "") ;
            return 0 ;
        }
        // Calculate the width of this terminal
        verific_int64 this_ss = StaticSizeSignInternal(0, 0) ;
        term_width = (unsigned)ABS(this_ss) ;
    }
    if ((term_width != (master_width * instance_count)) && (term_width != master_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        return 0 ;
    }
    if (term_width == master_width) { // formal_width == actual_width
        VeriMapForCopy id_map_table ;
        Array *storing_split_term = new Array(instance_count) ;
        for (unsigned i = 0; i < instance_count; i++) {
            // Only copy 'this' and return
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }

    if (!_id || !_idx) return 0 ;

    if (formal_id) {
        // Flag to check whether we are selecting in the same dimension as that of the division
        unsigned select_in_this_dimension = 0 ;
        // Some checks for arrays and its packed/unpacked dimensions
        unsigned formal_unpacked_dim = formal_id->UnPackedDimension() ;
        unsigned actual_unpacked_dim = _id->UnPackedDimension() ;
        if (actual_unpacked_dim) {
            // Actual has at least one unpacked dimension, so formal must have same or one or two
            // less number of unpacked dimensions, since one dimension is occupied for my selection.
            // For part select, we will select in this dimension, actual can have same or only one
            // more dimension than the formal.
            if (_idx->IsRange()) {
                // _idx is a range, so get the selected dimension
                verific_int64 actual_up_ss = _idx->StaticSizeSignInternal(0, 0) ;
                unsigned actual_up_width = (unsigned)ABS(actual_up_ss) ;
                VeriRange *formal_up_rng = formal_unpacked_dim ? formal_id->GetDimensionAt(0) : 0 ;
                verific_int64 formal_up_ss = formal_up_rng ? formal_up_rng->StaticSizeSignInternal(0, 0) : (int)actual_up_width ;
                unsigned formal_up_width = (unsigned)ABS(formal_up_ss) ;
                // If the size of the part-selected dimension of the actual and the size of
                // least varying dimension of the formal do not match, then we are selecting in the
                // part-selected dimension of the actual. Other cases will not proceed from the next if check.
                if (actual_up_width != formal_up_width) select_in_this_dimension = 1 ;
                if (select_in_this_dimension && select_in_up_dim) *select_in_up_dim = 1 ;
                // Get the size of the next unpacked dimension, if it is there:
                // Since 'this' is an indexed id, dimension 0 is selected, next dimension is 1.
                VeriRange *next_dim = (actual_unpacked_dim > 1) ? _id->GetDimensionAt(1) : 0 ;
                verific_int64 next_dim_ss = (next_dim) ? next_dim->StaticSizeSignInternal(0, 0) : 0 ;
                unsigned next_dim_width = (unsigned)ABS(next_dim_ss) ;
                // If the actual has one more number of unpacked dimension then it must have
                // same number of element as that of the formal. We will select in the next dimension.
                // Or if there is not same number of element, the next dimension should have same
                // number of element and we will select in this dimension, i.e., in _idx range.
                // If the actual has same number of unpacked dimension as that of the formal then, it
                // must be have instance_count times element in it, so that we can select into it.
                if (!(((formal_unpacked_dim == actual_unpacked_dim-1) && ((actual_up_width == formal_up_width) ||
                      /* or if we are selecting in this dimension then: */ (next_dim_width == formal_up_width))) ||
                      ((formal_unpacked_dim == actual_unpacked_dim  ) && (actual_up_width == instance_count*formal_up_width)))) {
                    Error("incompatible number of unpacked dimensions in instantiation") ;
                    return 0 ;
                }
            }
            if (!_idx->IsRange() && formal_unpacked_dim && (formal_unpacked_dim != actual_unpacked_dim-2)) {
                // For bit select actual must have two more number of dimension for, one dimension
                // is already selected by this indexed id. We will select into the next one.
                Error("incompatible number of unpacked dimensions in instantiation") ;
                return 0 ;
            }

            // If number of unpacked dimensions match, their corresponding size should also match,
            // except the least varying one, on which we will select and split the actual.
            // If it is a range then check from the second(1) dimension, otherwise
            // check from the third(2) dimension
            int msb, lsb ;
            for(unsigned i=(_idx->IsRange() ? 1 : 2); i<actual_unpacked_dim; i++) {
                // Get the corresponding dimension to match. If we are selecting in the
                // part selected dimension, then don't check that dimension, we have already
                // checked that one previously.
                unsigned dim_no = i - (_idx->IsRange() ? ((select_in_this_dimension) ? 0 : 1) : 2) ;
                // Each corresponding dimension must have same size
                VeriRange *formal_this_dim = formal_id->GetDimensionAt(dim_no) ;
                VeriRange *id_this_dim = _id->GetDimensionAt(i) ;
                if (formal_this_dim && id_this_dim && (formal_this_dim->GetWidth(&msb, &lsb, 0) != id_this_dim->GetWidth(&msb, &lsb, 0))) {
                    Error("size of the corresponding unpacked dimensions do not match") ;
                    return 0 ;
                }
            }

            // VIPER #3265: We don't need this check. We already checked the total size as well
            // as the number of unpacked dimensions and their corresponding sizes. So the packed
            // size is bound to be equal. Also, the check became wrong due to SV typedef. With
            // typedef, data type can include unpacked dimensions. Enhancing/adjusting the check
            // does not help either. Because it is possible to select into packed dimensions as
            // well as unpacked dimensions in the same instance. So, if we really need to check
            // then it should be the size of formal vs. the actual after splitting and that is
            // already done all the way above. So, just comment this code part out.
            /* // Packed size of both the formal and actual should exactly match
            // If it has data type, then packed size is the size of the data type (including the
            // packed dimensions), otherwise, it is a single bit element
            VeriDataType *id_data_type = _id->GetDataType() ;
            int my_packed_ss = (id_data_type) ? id_data_type->StaticSizeSignInternal(0, 0) : 1 ;
            unsigned my_packed_size = (unsigned)ABS(my_packed_ss) ;
            VeriDataType *formal_id_data_type = formal_id->GetDataType() ;
            int formal_id_ss = (formal_id_data_type) ? formal_id_data_type->StaticSizeSignInternal(0, 0) : 1 ;
            unsigned formal_packed_size = (unsigned)ABS(formal_id_ss) ;

            if (formal_unpacked_dim && (my_packed_size != formal_packed_size)) {
                // Packed size of both the formal and the actual does not match!
                error("packed size mismatch between formal and actual") ;
                return 0 ;
            } */
        } else if (formal_unpacked_dim) {
            // Actual does not have any unpacked dimension, but formal has
            // and the actual is directly connected - it is an error
            Error("unpacked dimension mismatch between formal and actual") ;
            return 0 ;
        }
    }

    VeriScope *defined_scope = 0 ;
    if (_id->IsInst() && _id->GetDimensions()) {
        // If _id is array instance identifier, we have to pick proper instance
        // identifier created due to array instance elaboration. To find those use
        // the scope where _id is defined
        defined_scope = scope ; // Get the scope where _id is defined
    }
    unsigned i ;
    if (_idx->IsRange()) { // part select
        // Fetch the slice.
        VeriExpression *l_expr = _idx->GetLeft() ;
        VeriExpression *r_expr = _idx->GetRight() ;
        if (!l_expr || !r_expr) return 0 ;

        // Evaluate the msb and lsb.
        VeriBaseValue *l_val = l_expr->StaticEvaluateInternal(0,0,0,0) ;
        VeriBaseValue *r_val = r_expr->StaticEvaluateInternal(0,0,0,0) ;
        // Both the indices are non-constant, can't continue
        if (!l_val && !r_val) return 0 ;

        // Get the first (un)packed dimension, if any - it is the least varying dimension.
        VeriRange *least_varying_dim = _id->GetDimensionAt(0) ;
        // VIPER #2562: Do not assert, we may select on 'int' for which 'least_varying_dim' is 0!
        //VERIFIC_ASSERT(least_varying_dim) ; // Must have this range, we are inside part select!

        int msb = 0 ;
        int lsb = 0 ;
        unsigned size = 0 ;
        if (least_varying_dim) {
            size = least_varying_dim->GetWidth(&msb, &lsb, 0) ;
        } else {
            verific_int64 id_ss = _id->StaticSizeSignInternal(0, 0) ;
            size = (unsigned)ABS(id_ss) ;
            msb = (int)size-1 ;
        }
        if (!size) size = 1 ;

        unsigned this_range_size = 0 ;
        VeriArrayRange *dummy_dim = 0 ;
        unsigned part_sel_token = _idx->GetPartSelectToken() ;
        int l_idx = 0 ;
        int r_idx = 0 ;
        if (l_val && r_val) { // Both indices are constant
            l_idx = l_val->GetIntegerValue() ;
            r_idx = r_val->GetIntegerValue() ;
            delete l_val ;
            delete r_val ;

            // Convert +:, -: to normal :
            switch (part_sel_token) {
            case VERI_PARTSELECT_DOWN:
                r_idx = (l_idx - r_idx) + 1;
                break ;
            case VERI_PARTSELECT_UP:
                r_idx = (l_idx + r_idx) - 1;
                break ;
            default:
                break;
            }

            // Range should always be in the order of decalration:
            if (((msb > lsb) && (r_idx > l_idx)) ||
                ((msb < lsb) && (r_idx < l_idx))) {
                if ((part_sel_token != VERI_PARTSELECT_DOWN) &&
                    (part_sel_token != VERI_PARTSELECT_UP)) {
                    Error("part-select direction is opposite from prefix index direction") ;
                    return 0 ;
                }
                int tmp = l_idx ;
                l_idx = r_idx ;
                r_idx = tmp ;
            }

            this_range_size = (unsigned)ABS(l_idx-r_idx)+1 ;
            dummy_dim = new VeriConstArrayRange(l_idx, r_idx) ;
        } else if (!l_val && r_val && part_sel_token) {
            // +:, -: expression
            // Starting index is not constant, width is constant
            r_idx = r_val->GetIntegerValue() ;
            delete r_val ;

            this_range_size = (unsigned)ABS(r_idx) ;
            dummy_dim = new VeriNonConstArrayRange(l_expr, this_range_size, part_sel_token) ;
        }

        if (!dummy_dim || !this_range_size) {
            delete dummy_dim ; // In case it is valid and this_range_size == 0
            return 0 ; // failed
        }

        if (least_varying_dim) {
            // Get the number of total (packed and unpacked) dimensions we can select:
            unsigned dims = _id->PackedDimension() + _id->UnPackedDimension() ;
            for (i=1; i<dims; i++) {
                VeriRange *rng = _id->GetDimensionAt(i) ;
                // Add the next dimensions at the end of this linked list:
                dummy_dim->AddDimension(new VeriConstArrayRange(rng)) ;
            }
            // Get the base data type where we are stopped with the dimensions:
            VeriDataType *base_type = _id->GetBaseDataType() ;
            // Get the size of all the dimensions we can index into:
            unsigned dim_size = dummy_dim->Size() ;
            VERIFIC_ASSERT(dim_size) ; // Size should never be 0
            // If the dimension is not fully selected, we can't get the proper base size
            // So, discard the value of the range selected dimension from 'dim_size'
            dim_size = dim_size / this_range_size ;
            // And consider the full size of that dimension:
            dim_size = dim_size * size ;
            // Get the base size of the actual (size of the base data type):
            verific_int64 id_ss = _id->StaticSizeSignInternal(0, 0) ;
            unsigned base_size = (unsigned)ABS(id_ss) / dim_size ;
            // Add the last dimension with the base size (if it is packed we can index into it, otherwise not):
            dummy_dim->AddDimension(new VeriConstArrayRange((int)base_size-1, 0, ((base_type) ? base_type->IsUnpacked() : 0))) ;
        }

        // Create proper terminals from 'this'
        Array *storing_split_term = new Array(instance_count) ;
        for (i = 0; i < instance_count; i++) {
            Array *indices = new Array() ;
            dummy_dim->GetIndices(master_width, term_width, instance_count, *indices, this) ;
            if (!indices->Size()) {
                // Something bad happened, clean-up and return 0 from here:
                VeriExpression *expr ;
                FOREACH_ARRAY_ITEM(storing_split_term, i, expr) delete expr ;
                delete storing_split_term ;
                return 0 ;
            }
            VeriName *ret_term = 0 ;
            VeriMapForCopy old2new ;
            if (indices->Size() > 1) {
                // Multiple indices : create indexed memory id for this:
                ret_term = new VeriIndexedMemoryId(_prefix->CopyName(old2new), indices) ;
                ret_term->SetId(_id) ;
            } else {
                // If actual is interface array instance reference, create expression
                // using array instance elaboration created new instance identifiers :
                VeriExpression *index = (VeriExpression *)indices->GetFirst() ;
                if (defined_scope) {
                    char *index_image = (index)  ? index->Image(): 0 ;
                    char *expr_image = Strings::save(_id->Name(), "[", index_image, "]") ;
                    VeriIdDef *new_suffix = defined_scope->FindLocal(expr_image) ;
                    // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
                    if (!new_suffix) new_suffix = (defined_scope->IsGenerateScope()) ? defined_scope->Find(expr_image, 0): 0 ;
                    if (new_suffix) {
                        if (_prefix->IsHierName() && _prefix->GetPrefix()) {
                            VeriName *prev_prefix = _prefix->GetPrefix() ;
                            ret_term = new VeriSelectedName(prev_prefix ? prev_prefix->CopyName(old2new): 0, Strings::save(new_suffix->Name())) ;
                            ret_term->SetId(new_suffix) ;
                        } else {
                            ret_term = new VeriIdRef(new_suffix) ;
                        }
                    }
                    Strings::free(index_image) ;
                    Strings::free(expr_image) ;
                }
                // Single index : create indexed id only:
                if (!ret_term) {
                    ret_term = new VeriIndexedId(_prefix->CopyName(old2new), index) ;
                    ret_term->SetId(_id) ;
                } else {
                    delete index ;
                }
                delete indices ;
            }
            ret_term->SetLinefile(Linefile()) ;
            storing_split_term->Insert(ret_term) ;
        }

        delete dummy_dim ; // Clean-up

        return storing_split_term ;
    } else {
        // 'this' is memory word select.  i.e if the declaration is reg [7:0] mem[1:0]
        // then 'this' is mem[1] i.e. size of 'this' index expression is not 1 but
        // vector width of this identifier.  For 'this' case bits are selected from the
        // word range and actuals are created.  If formal width 2, then
        // mem[1][7:6], mem[1][5:4], mem[1][3:2] and mem[1][1:0] are created
        Array *storing_split_term = new Array(instance_count) ;

        // Get the second (un)packed dimension, if any - it is the second least varying dimension.
        // The first one being the dimension that is already 'selected'
        VeriRange *second_least_varying_dim = _id->GetDimensionAt(1) ;
        VeriArrayRange *dummy_dim = 0 ;
        if (second_least_varying_dim) {
            VERIFIC_ASSERT(_id) ; // It must be there, since we found a dimension of it from the identifier
            dummy_dim = new VeriConstArrayRange(second_least_varying_dim) ;
            // Get the number of total (packed and unpacked) dimensions we can select:
            unsigned dims = _id->PackedDimension() + _id->UnPackedDimension() ;
            for (i=2; i<dims; i++) {
                VeriRange *rng = _id->GetDimensionAt(i) ;
                // Add the next dimensions at the end of this linked list:
                dummy_dim->AddDimension(new VeriConstArrayRange(rng)) ;
            }
            // Get the base data type where we are stopped with the dimensions:
            VeriDataType *base_type = _id->GetBaseDataType() ;
            // Get the size of all the dimensions we can index into:
            unsigned dim_size = dummy_dim->Size() ;
            VERIFIC_ASSERT(dim_size) ; // Size should never be 0
            // Get the base size of the actual (size of the base data type):
            unsigned base_size = term_width / dim_size ;
            // Add the last dimension with the base size (if it is packed we can index into it, otherwise not):
            dummy_dim->AddDimension(new VeriConstArrayRange((int)base_size-1, 0, ((base_type) ? base_type->IsUnpacked() : 0))) ;
        } else {
            // It does not have any dimension at all (packed or unpacked)
            VeriDataType *id_data_type = (_id) ? _id->GetDataType(): 0 ;
            //if (_id && (_id->Type() == VERI_ENUM ||
                //_id->Type() == VERI_UNION ||
                //_id->Type() == VERI_STRUCT)) {
            // VIPER #6160: Packed struct/union types can be broken bit by bit, so check for not-packed flag:
            if (id_data_type && (id_data_type->IsEnumType() || ((id_data_type->IsUnionType() || id_data_type->IsStructType()) && !id_data_type->IsPacked()))) {
                // Can't do anything other than copying and returning 'this'. Becasue,
                // they can't be treated as arrays, so we can't really select on these
                // type of elements, produce error and copy the term itself:
                //Error("cannot split unpacked item without dimension") ;
                Error("cannot index into unpacked base of %s", ((_id)?_id->Name():((_prefix)?_prefix->GetName():"<unknown>"))) ;
                for (i = 0; i < instance_count; i++) {
                    VeriMapForCopy id_map_table ;
                    // Only copy 'this'
                    storing_split_term->Insert(CopyExpression(id_map_table)) ;
                }
                return storing_split_term ;
            }
            // It is like int/integer/byte etc, so the size of the dimension is in term_width:
            dummy_dim = new VeriConstArrayRange((int)term_width-1, 0) ;
        }

        // Now split the actual in the number of instances:
        for (i = 0; i < instance_count; i++) {
            Array *indices = new Array() ;
            VeriMapForCopy id_map_table ;
            indices->Insert(_idx->CopyExpression(id_map_table)) ;
            dummy_dim->GetIndices(master_width, term_width, instance_count, *indices, this) ;
            if (!indices->Size()) {
                // Something bad happened, clean-up and return 0 from here:
                VeriExpression *expr ;
                FOREACH_ARRAY_ITEM(storing_split_term, i, expr) delete expr ;
                delete storing_split_term ;
                return 0 ;
            }
            VeriMapForCopy old2new ;
            VeriIndexedMemoryId *ret_term = new VeriIndexedMemoryId(_prefix->CopyName(old2new), indices) ;
            ret_term->SetId(_id) ;
            ret_term->SetLinefile(Linefile()) ;
            storing_split_term->Insert(ret_term) ;
        }

        delete dummy_dim ; // Clean-up

        return storing_split_term ;
    }

    return 0 ;
}

// Split 'this', size of each split will be 'master_width'. If size of 'this' is 'master_width',
// copy the expression 'instance_count' times, insert in an array and return
Array *VeriIndexedMemoryId::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned *select_in_up_dim)
{
    if (!_prefix || !master_width) return 0 ;
    VeriModuleItem *mod = node ? node->GetDesign(): 0 ;
    VeriScope *scope = mod ? mod->GetScope(): 0 ;
    // If name is not resolved, try to resolve it
    if (!_id) {
        HierIdWithDeclScope tmp_id(_prefix, scope) ;
        _prefix->ResolveName(&tmp_id, node, top_mods, mod_map_tab, 0) ;

        VeriIdDef *resolved_id = tmp_id.GetResolvedIddef() ;
        if (resolved_id) {
            (void) _prefix->Bind(&tmp_id, node, 0, top_mods, mod_map_tab, 0) ;
            _id = resolved_id ;
        }
    }
    if (!instance_count) {
        verific_int64 s = StaticSizeSignInternal(0, 0) ;
        instance_count = (unsigned)ABS(s) ;
    }
    // Calculate the width of this terminal
    unsigned term_width = 0 ;
    if (_id && (_id->IsInst() || _id->IsModport() || _id->IsInterfacePort())) { // Actual is interface instance
        // Calculate actual size. Consider only the index expressions
        // Do not consider interface size, as formal can have generic interface
        unsigned i ;
        unsigned actual_size = 1 ;
        VeriExpression *idx ;
        FOREACH_ARRAY_ITEM(_indexes, i, idx) { // Iterate over indexes
            if (!idx) continue ;
            verific_int64 idx_ss = idx->StaticSizeSignInternal(0, 0) ; // Calculate size of each index
            actual_size *= (unsigned)ABS(idx_ss) ;
        }
        term_width = actual_size ;
        if ((term_width == master_width) && (term_width != 1)) {
            // Actual is interface reference, terminal width is same as formal width,
            // but formal width is not 1. In this case break the terminal into
            // elements of size 1.
            Array *terms = SplitTerm(1, 0, instance_count, node, top_mods, mod_map_tab, 0) ;
            // Create a concatenation from broken elements
            VeriAssignmentPattern *concat = new VeriAssignmentPattern(0, terms) ;
            concat->SetLinefile(Linefile()) ;
            VeriMapForCopy id_map_table ;
            Array *storing_split_term = new Array(instance_count) ;
            storing_split_term->Insert(concat) ;
            for (i = 1; i < instance_count; i++) {
                VeriExpression *ele = concat->CopyExpression(id_map_table) ;
                // Only copy 'this' and return
                storing_split_term->Insert(ele) ;
            }
            return storing_split_term ;
        }
    } else { // Actual is normal variable
        if (formal_id && formal_id->IsInterfacePort()) {
            Warning("formal port %s expects a modport or interface instance actual", formal_id ? formal_id->Name() : "") ;
            return 0 ;
        }
        // Calculate the width of this terminal
        verific_int64 this_ss = StaticSizeSignInternal(0, 0) ;
        term_width = (unsigned)ABS(this_ss) ;
    }

    VeriExpression *last_node = _indexes ? (VeriExpression*) _indexes->GetLast(): 0 ;
    if ((term_width != (master_width * instance_count)) && (term_width != master_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        return 0 ;
    }

    if (term_width == master_width) {
        // formal width equal to actual width, copy the expression
        Array *storing_split_term = new Array(instance_count) ;
        VeriMapForCopy id_map_table ;
        for (unsigned i = 0; i < instance_count; i++) {
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }

    if (!_id || !_indexes || !last_node) return 0 ;

    if (formal_id) {
        // Flag to check whether we are selecting in the same dimension as that of the division
        unsigned select_in_this_dimension = 0 ;
        // Some checks for arrays and its packed/unpacked dimensions
        unsigned formal_unpacked_dim = formal_id->UnPackedDimension() ;
        // Find out the actual remaining dimensions (subtract my selection dimensions)
        unsigned actual_unpacked_dim = (_id->UnPackedDimension() > _indexes->Size()) ? (_id->UnPackedDimension() - _indexes->Size()) : 0 ;
        if (actual_unpacked_dim) {
            // Actual has at least one unpacked dimension, so formal must have same one or two
            // less number of unpacked dimensions, since one dimension is occupied for my selection
            // For part select, we will select in this dimension, actual should have same number of
            // unpacked dimension remaining, for bit select it should have one more dimension than the formal.
            if (last_node->IsRange()) {
                // Last node is a range, so get the selected dimension
                verific_int64 actual_up_ss = last_node->StaticSizeSignInternal(0, 0) ;
                unsigned actual_up_width = (unsigned)ABS(actual_up_ss) ;
                VeriRange *formal_up_rng = formal_unpacked_dim ? formal_id->GetDimensionAt(0) : 0 ;
                verific_int64 formal_up_ss = formal_up_rng ? formal_up_rng->StaticSizeSignInternal(0, 0) : (int)actual_up_width ;
                unsigned formal_up_width = (unsigned)ABS(formal_up_ss) ;
                // If the size of the part-selected dimension of the actual and the size of
                // least varying dimension of the formal do not match, then we are selecting in the
                // part-selected dimension of the actual. Other cases will not proceed from the next if check.
                if (actual_up_width != formal_up_width) select_in_this_dimension = 1 ;
                if (select_in_this_dimension && select_in_up_dim) *select_in_up_dim = 1 ;
                // Get the size of the next unpacked dimension, if it is there:
                // Since 'this' is an indexed memory id, _indexes->Size() number of dimensions
                // are already selected in 'this', next dimension is _indexes->Size() th dimension.
                // Since 'actual_unpacked_dim' is already counts dimensions above the selected dimension,
                // we don't need to check it again - we are here because it is greater than zero.
                VeriRange *next_dim = _id->GetDimensionAt(_indexes->Size()) ;
                verific_int64 next_dim_ss = (next_dim) ? next_dim->StaticSizeSignInternal(0, 0) : 0 ;
                unsigned next_dim_width = (unsigned)ABS(next_dim_ss) ;
                // If the actual has one more number of unpacked dimension then it must have
                // same number of element as that of the formal. We will select in the next dimension.
                // Or if there is not same number of element, the next dimension should have same
                // number of element and we will select in last of the selected dimensions.
                // If the actual has same number of unpacked dimension as that of the formal then, it
                // must be have instance_count times element in it, so that we can select into it.
                if (!(((formal_unpacked_dim == actual_unpacked_dim) && ((actual_up_width == formal_up_width) ||
                      /* or if we are selecting in the last of the selected dimension then: */ (next_dim_width == formal_up_width))) ||
                      ((formal_unpacked_dim == actual_unpacked_dim+1) && (actual_up_width == instance_count*formal_up_width)))) {
                    Error("incompatible number of unpacked dimensions in instantiation") ;
                    return 0 ;
                }
            }
            if (!last_node->IsRange() && formal_unpacked_dim && (formal_unpacked_dim != actual_unpacked_dim-1)) {
                Error("incompatible number of unpacked dimensions in instantiation") ;
                return 0 ;
            }

            // If number of unpacked dimensions match, their corresponding size should also match,
            // except the least varying one, on which we will select and split the actual.
            // If it is a range then check from the second(1) dimension, otherwise
            // check from the third(2) dimension
            int msb, lsb ;
            for(unsigned i=(last_node->IsRange() ? 0 : 1); i<actual_unpacked_dim; i++) {
                // Get the corresponding dimension to match. If we are selecting in the
                // part selected dimension, then don't check that dimension, we have already
                // checked that one previously.
                unsigned dim_no = (unsigned)((int)i - (last_node->IsRange() ? ((select_in_this_dimension) ? -1 : 0) : 1)) ;
                // Each corresponding dimension must have same size, start checking from my remaining dimensions only
                VeriRange *formal_this_dim = formal_id->GetDimensionAt(dim_no) ;
                VeriRange *id_this_dim = _id->GetDimensionAt(i+_indexes->Size()) ;
                if (formal_this_dim && id_this_dim && (formal_this_dim->GetWidth(&msb, &lsb, 0) != id_this_dim->GetWidth(&msb, &lsb, 0))) {
                    Error("size of the corresponding unpacked dimensions do not match") ;
                    return 0 ;
                }
            }

            // VIPER #3265: We don't need this check. We already checked the total size as well
            // as the number of unpacked dimensions and their corresponding sizes. So the packed
            // size is bound to be equal. Also, the check became wrong due to SV typedef. With
            // typedef, data type can include unpacked dimensions. Enhancing/adjusting the check
            // does not help either. Because it is possible to select into packed dimensions as
            // well as unpacked dimensions in the same instance. So, if we really need to check
            // then it should be the size of formal vs. the actual after splitting and that is
            // already done all the way above. So, just comment this code part out.
            /* // Packed size of both the formal and actual should exactly match
            // If it has data type, then packed size is the size of the data type (including the
            // packed dimensions), otherwise, it is a single bit element
            VeriDataType *id_data_type = _id->GetDataType() ;
            int my_packed_ss = id_data_type ? id_data_type->StaticSizeSignInternal(0, 0) : 1 ;
            unsigned my_packed_size = (unsigned)ABS(my_packed_ss) ;
            VeriDataType *formal_data_type = formal_id->GetDataType() ;
            int formal_packed_ss = formal_data_type ? formal_data_type->StaticSizeSignInternal(0, 0) : 1 ;
            unsigned formal_packed_size = (unsigned)ABS(formal_packed_ss) ;

            if (formal_unpacked_dim && (my_packed_size != formal_packed_size)) {
                // Packed size of both the formal and the actual does not match!
                error("packed size mismatch between formal and actual") ;
                return 0 ;
            } */
        } else if (formal_unpacked_dim) {
            // Actual does not have any unpacked dimension, but formal has
            // and the actual is directly connected - it is an error
            Error("unpacked dimension mismatch between formal and actual") ;
            return 0 ;
        }
    }

    unsigned i ;
    if (last_node->IsRange()) { // part select expression
        // Fetch the slice.
        VeriExpression *l_expr = last_node->GetLeft() ;
        VeriExpression *r_expr = last_node->GetRight() ;
        if (!l_expr || !r_expr) return 0 ;

        // Evaluate the msb and lsb.
        VeriBaseValue *l_val = l_expr->StaticEvaluateInternal(0,0,0,0) ;
        VeriBaseValue *r_val = r_expr->StaticEvaluateInternal(0,0,0,0) ;
        // Both the indices are non-constant, can't continue
        if (!l_val && !r_val) return 0 ;

        // Get the dimension in which we will 'select', if any - it is the least varying dimension available to us
        VeriRange *least_varying_dim = _id->GetDimensionAt(_indexes->Size()-1) ;
        //VERIFIC_ASSERT(least_varying_dim) ; // Must have this range, we are inside part select!

        int msb = 0 ;
        int lsb = 0 ;
        unsigned size = 0 ;
        if (least_varying_dim) {
            size = least_varying_dim->GetWidth(&msb, &lsb, 0) ;
        } else {
            // It can be built-in vector type
            VeriDataType *base_type = _id->GetDataType() ;
            size = base_type ? base_type->BaseTypeSize(): 0 ;
            if (size) msb = (int)(size -1) ;
        }
        if (!size) size = 1 ;

        unsigned this_range_size = 0 ;
        VeriArrayRange *dummy_dim = 0 ;
        unsigned part_sel_token = last_node->GetPartSelectToken() ;
        int l_idx = 0 ;
        int r_idx = 0 ;
        if (l_val && r_val) { // Both indices are constant
            l_idx = l_val->GetIntegerValue() ;
            r_idx = r_val->GetIntegerValue() ;
            delete l_val ;
            delete r_val ;

            // Convert +:, -: to normal :
            switch (part_sel_token) {
            case VERI_PARTSELECT_DOWN:
                r_idx = (l_idx - r_idx) + 1;
                break ;
            case VERI_PARTSELECT_UP:
                r_idx = (l_idx + r_idx) - 1;
                break ;
            default:
                break;
            }

            // Range should always be in the order of decalration:
            if (((msb > lsb) && (r_idx > l_idx)) ||
                ((msb < lsb) && (r_idx < l_idx))) {
                if ((part_sel_token != VERI_PARTSELECT_DOWN) &&
                    (part_sel_token != VERI_PARTSELECT_UP)) {
                    Error("part-select direction is opposite from prefix index direction") ;
                    return 0 ;
                }
                int tmp = l_idx ;
                l_idx = r_idx ;
                r_idx = tmp ;
            }

            this_range_size = (unsigned)ABS(l_idx-r_idx)+1 ;
            dummy_dim = new VeriConstArrayRange(l_idx, r_idx) ;
        } else if (!l_val && r_val && part_sel_token) {
            // +:, -: with start index non constant. Expression var[2][i+:4] is first split
            // in single bit expressions, i.e var[2][i], var[2][i+1], var[2][i+2],
            // var[2][i+3] are created.
            // If formal is single bit, created expressions are returned.  If formal is
            // multi-bit, 1 bit expressions are grouped to create multi-bit concat expressions
            // If formal is 2 bit width, the actuals will be
            // {var[2][i], var[2][i+1]}, {var[2][i+2], var[2][i+3]}
            r_idx = r_val->GetIntegerValue() ;
            delete r_val ;

            this_range_size = (unsigned)ABS(r_idx) ;
            dummy_dim = new VeriNonConstArrayRange(l_expr, this_range_size, part_sel_token) ;
        }

        if (!dummy_dim || !this_range_size) {
            delete dummy_dim ; // In case it is valid and this_range_size == 0
            return 0 ; // failed
        }

        // Get the number of total (packed and unpacked) dimensions we can select:
        unsigned dims = _id->PackedDimension() + _id->UnPackedDimension() ;
        for (i=_indexes->Size(); i<dims; i++) {
            VeriRange *rng = _id->GetDimensionAt(i) ;
            // Add the next dimensions at the end of this linked list:
            dummy_dim->AddDimension(new VeriConstArrayRange(rng)) ;
        }
        // Get the base data type where we are stopped with the dimensions:
        VeriDataType *base_type = _id->GetBaseDataType() ;
        // Get the size of all the dimensions we can index into:
        unsigned dim_size = dummy_dim->Size() ;
        VERIFIC_ASSERT(dim_size) ; // Size should never be 0
        // If the dimension is not fully selected, we can't get the proper base size
        // So, discard the value of the range selected dimension from 'dim_size'
        dim_size = dim_size / this_range_size ;
        // And consider the full size of that dimension:
        dim_size = dim_size * size ;
        // Get the base size of the actual (size of the base data type):
        unsigned base_size = term_width / dim_size ;
        // Add the last dimension with the base size (if it is packed we can index into it, otherwise not):
        dummy_dim->AddDimension(new VeriConstArrayRange((int)base_size-1, 0, ((base_type) ? base_type->IsUnpacked() : 0))) ;

        // Create proper terminals from 'this'
        Array *storing_split_term = new Array(instance_count) ;
        for (i = 0; i < instance_count; i++) {
            Array *indices = new Array() ;
            unsigned j ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(_indexes, j, expr) {
                if (j+1 == _indexes->Size()) break ; // Take all but the last (range) index
                VeriMapForCopy id_map_table ;
                indices->InsertLast(expr->CopyExpression(id_map_table)) ;
            }
            dummy_dim->GetIndices(master_width, term_width, instance_count, *indices, this) ;
            if (!indices->Size()) {
                // Something bad happened, clean-up and return 0 from here:
                FOREACH_ARRAY_ITEM(storing_split_term, i, expr) delete expr ;
                delete storing_split_term ;
                return 0 ;
            }
            VERIFIC_ASSERT(indices->Size() > 1) ;
            VeriName *ret_term = 0 ;
            VeriMapForCopy old2new ;
            ret_term = new VeriIndexedMemoryId(_prefix->CopyName(old2new), indices) ;
            ret_term->SetId(_id) ;
            ret_term->SetLinefile(Linefile()) ;
            storing_split_term->Insert(ret_term) ;
        }

        delete dummy_dim ; // Clean-up

        return storing_split_term ;
    } else {
        // Expression like mem[2][4]. mem is declared as reg [7:0] mem[3:0][5:0].
        // Width of mem[2][4] is 8. Proper bits are selected from its ('mem') word range
        // For 2 bit formal, mem[2][4][7:6], mem[2][4][5:4], mem[2][4][3:2], mem[2][4][1:0] are created
        Array *storing_split_term = new Array(instance_count) ;

        // Get the dimension in which we will 'select', if any - it is the least varying dimension
        // of the remaining dimensions.
        VeriRange *second_least_varying_dim = _id->GetDimensionAt(_indexes->Size()) ;
        VeriArrayRange *dummy_dim = 0 ;
        if (second_least_varying_dim) {
            dummy_dim = new VeriConstArrayRange(second_least_varying_dim) ;
            // Get the number of total (packed and unpacked) dimensions we can select:
            unsigned dims = _id->PackedDimension() + _id->UnPackedDimension() ;
            for (i=2; i<dims; i++) {
                VeriRange *rng = _id->GetDimensionAt(i) ;
                // Add the next dimensions at the end of this linked list:
                dummy_dim->AddDimension(new VeriConstArrayRange(rng)) ;
            }
            // Get the base data type where we are stopped with the dimensions:
            VeriDataType *base_type = _id->GetBaseDataType() ;
            // Get the size of all the dimensions we can index into:
            unsigned dim_size = dummy_dim->Size() ;
            VERIFIC_ASSERT(dim_size) ; // Size should never be 0
            // Get the base size of the actual (size of the base data type):
            unsigned base_size = term_width / dim_size ;
            // Add the last dimension with the base size (if it is packed we can index into it, otherwise not):
            dummy_dim->AddDimension(new VeriConstArrayRange((int)base_size-1, 0, ((base_type) ? base_type->IsUnpacked() : 0))) ;
        } else {
            // It does not have any dimension at all (packed or unpacked)
            VeriDataType *id_data_type = (_id) ? _id->GetDataType(): 0 ;
            //if (_id && (_id->Type() == VERI_ENUM ||
                //_id->Type() == VERI_UNION ||
                //_id->Type() == VERI_STRUCT)) {
            // VIPER #6160: Packed struct/union types can be broken bit by bit, so check for not-packed flag:
            if (id_data_type && (id_data_type->IsEnumType() || ((id_data_type->IsUnionType() || id_data_type->IsStructType()) && !id_data_type->IsPacked()))) {
                // Can't do anything other than copying and returning 'this'. Becasue
                // they can't be treated as arrays, so we can't really select on these
                // type of elements, produce error and copy the term itself:
                //Error("cannot split unpacked item without dimension") ;
                Error("cannot index into unpacked base of %s", ((_id)?_id->Name():((_prefix)?_prefix->GetName():"<unknown>"))) ;
                for (i = 0; i < instance_count; i++) {
                    VeriMapForCopy id_map_table ;
                    // Only copy 'this'
                    storing_split_term->Insert(CopyExpression(id_map_table)) ;
                }
                return storing_split_term ;
            }
            // It is like int/integer/byte etc, so the size is in term_width. Set the msb and lsb
            dummy_dim = new VeriConstArrayRange((int)term_width-1, 0) ;
        }

        // Now split the actual in the number of instances:
        for (i = 0; i < instance_count; i++) {
            Array *indices = new Array() ;
            unsigned j ;
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(_indexes, j, expr) {
                VeriMapForCopy id_map_table ;
                indices->InsertLast(expr->CopyExpression(id_map_table)) ;
            }
            dummy_dim->GetIndices(master_width, term_width, instance_count, *indices, this) ;
            if (!indices->Size()) {
                // Something bad happened, clean-up and return 0 from here:
                FOREACH_ARRAY_ITEM(storing_split_term, i, expr) delete expr ;
                delete storing_split_term ;
                return 0 ;
            }
            VeriMapForCopy old2new ;
            VeriIndexedMemoryId *ret_term = new VeriIndexedMemoryId(_prefix->CopyName(old2new), indices) ;
            ret_term->SetId(_id) ;
            ret_term->SetLinefile(Linefile()) ;
            storing_split_term->Insert(ret_term) ;
        }

        delete dummy_dim ; // Clean-up

        return storing_split_term ;
    }
    return 0 ;
}

// Split 'this', size of each split will be 'master_width'. If size of 'this' is
// 'master_width', copy the expression 'instance_count' times, insert in an array and return
Array *VeriConcat::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned * /*select_in_up_dim*/)
{
    if (!master_width) return 0 ;

    // Split the concat expression to array of 1 bit expressions
    Array *individual_bit = new Array(2) ;
    unsigned i ;
    unsigned j ;
    VeriExpression *current_child ;
    FOREACH_ARRAY_ITEM(_exprs, j, current_child) {
        if (!current_child) continue ;
        //int ele_ss = current_child->StaticSizeSignInternal(0, 0) ;
        //unsigned ele_width = (unsigned)ABS(ele_ss) ;
        Array *temp_array1 = current_child->SplitTerm(1, 0, 0, node, top_mods, mod_map_tab, 0) ;
        if (!temp_array1) {
            FOREACH_ARRAY_ITEM(individual_bit, i, current_child) delete current_child ;
            delete individual_bit ;
            return 0 ;
        }
        individual_bit->Append(temp_array1) ;
        delete temp_array1 ;
    }
    // VIPER #4505 : Calculate size after callling SplitTerm on individual element
    // If any element is unresolved, SplitTerm will resolve that
    verific_int64 term_ss = StaticSizeSignInternal(0, 0) ;
    unsigned term_width = (unsigned)ABS(term_ss) ;
    if (!instance_count) {
        instance_count = (unsigned)ABS(term_ss) ;
    }

    // Check validity of the terminal, issue warning and return
    if ((term_width != (master_width * instance_count)) && (term_width != master_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        FOREACH_ARRAY_ITEM(individual_bit, i, current_child) delete current_child ;
        delete individual_bit ; // cleanup before return
        return 0 ;
    }
    Array *storing_split_term = new Array(instance_count) ;
    if (term_width == master_width) {
        // Delete created array
        FOREACH_ARRAY_ITEM(individual_bit, i, current_child) delete current_child ;
        delete individual_bit ;
        // formal and actual are of same size, copy the expression 'instance_count' times and return
        VeriMapForCopy id_map_table ;
        for(i = 0; i < instance_count; i++) {
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }
    if (master_width == 1) { // formal is 1 bit
        delete storing_split_term ;
        return individual_bit ;
    }
    // formal multi-bit, group 1 bit expressions to create multi-bit concat
    for (i = 0, j = 0; j < instance_count; j++) {
        Array *single_bit = new Array(master_width) ;
        for(; i < ((j + 1) * master_width); i++) {
            single_bit->Insert(individual_bit->At(i)) ;
        }
        VeriConcat *concated_bit = new VeriConcat(single_bit) ;
        concated_bit->SetLinefile(Linefile()) ;
        storing_split_term->Insert(concated_bit) ;
    }
    delete individual_bit ;

    return storing_split_term ;
}

// Split 'this', size of each split will be 'master_width'. If size of 'this' is
// 'master_width', copy the expression 'instance_count' times, insert in an array and return
Array *VeriMultiConcat::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned * /*select_in_up_dim */)
{
    if (!master_width) return 0 ;

    VeriBaseValue *repeat_value = _repeat->StaticEvaluateInternal(0,0,0,0) ;
    if (!repeat_value) return 0 ; // repeatation operator non-constant

    int temp = repeat_value->GetIntegerValue() ;
    delete repeat_value ;
    if (temp < 0) return 0 ; // multiplier in multi-concat cannot be negative!
    unsigned multiplier = (unsigned)ABS(temp) ;
    Array *storing_split_term = new Array(instance_count ? instance_count: 2) ;

    unsigned loop ;
    // Split the concat expression to create array of single bit expressions
    Array *individual_bit = new Array(2) ;
    for (loop = 0; loop < _exprs->Size(); loop++) {
        VeriExpression *current_child = (VeriExpression*)_exprs->At(loop) ;
        if (!current_child) continue ;
        //int ele_ss = current_child->StaticSizeSignInternal(0, 0) ;
        //unsigned ele_width = (unsigned)ABS(ele_ss) ;
        Array *temp_array1 = current_child->SplitTerm(1, 0, 0, node, top_mods, mod_map_tab, 0) ;
        if (!temp_array1) {
            unsigned idx ; VeriExpression *e ;
            FOREACH_ARRAY_ITEM(individual_bit, idx, e) delete e ;
            delete individual_bit ;
            delete storing_split_term ;
            return 0 ;
        }
        individual_bit->Append(temp_array1) ;
        delete temp_array1 ;
    }
    verific_int64 term_ss = StaticSizeSignInternal(0, 0) ;
    unsigned term_width = (unsigned)ABS(term_ss) ; // Calculate actual size
    if (!instance_count) instance_count = term_width ;

    // Check validity of the terminal. Issue warning for incorrect width and return
    if ((term_width != (master_width * instance_count)) && (term_width != master_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        unsigned idx ; VeriExpression *e ;
        FOREACH_ARRAY_ITEM(individual_bit, idx, e) delete e ;
        delete individual_bit ;
        delete storing_split_term ; // cleanup before return
        return 0 ;
    }
    if (term_width == master_width) {
        // formal and actual are of same width, only copy 'this'
        VeriMapForCopy id_map_table ;
        for (unsigned i = 0; i < instance_count; i++) {
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        unsigned idx ; VeriExpression *e ;
        FOREACH_ARRAY_ITEM(individual_bit, idx, e) delete e ;
        delete individual_bit ;
        return storing_split_term ;
    }

    unsigned i ;
    unsigned j ;
    unsigned len = individual_bit->Size() ;
    // Array of single bit expressions is expanded repetation value times to
    // create array of expressions for 'this' multi concat
    for (i = 0; i < multiplier-1; i++) {
         for (j = 0; j < len; j++) {
             VeriMapForCopy id_map_table ;
             individual_bit->Insert(((VeriExpression*)individual_bit->At(j))->CopyExpression(id_map_table)) ;
         }
    }
    if (master_width == 1) {
        // Formal 1 bit, return single bit expressions
        delete storing_split_term ;
        return individual_bit ;
    }
    // Formal multiple bits, group single bit expressions to create multi-bit concat
    for (i = 0, j = 0; j < instance_count; j++) {
        Array *single_bit = new Array(master_width) ;
        for(; i < ((j + 1) * master_width); i++) {
            single_bit->Insert(individual_bit->At(i)) ;
        }
        VeriConcat *concated_bit = new VeriConcat(single_bit) ;
        concated_bit->SetLinefile(Linefile()) ;
        storing_split_term->Insert(concated_bit) ;
    }
    delete individual_bit ;

    return storing_split_term ;
}

Array *VeriSelectedName::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned *select_in_up_dim)
{
    // If width of formal is 0, can't do anything!
    if (!master_width) return 0 ;
    VeriModuleItem *mod = node ? node->GetDesign(): 0 ;
    VeriScope *scope = mod ? mod->GetScope(): 0 ;
    VeriScope *prefix_scope = 0 ;
    // If suffix id is not resolved, try to resolve it
    if (!_suffix_id || ((_suffix_id->IsInst() && _suffix_id->GetDimensions()) || _suffix_id->IsInterfacePort())) {
        HierIdWithDeclScope tmp_id(this, scope) ;
        ResolveName(&tmp_id, node, top_mods, mod_map_tab, 0) ;

        VeriIdDef *resolved_id = tmp_id.GetResolvedIddef() ;
        if (resolved_id) (void) Bind(&tmp_id, node, 0, top_mods, mod_map_tab, 0) ;
        if (resolved_id) {
            // If prefix is hierarchical identifier, resolved identifier is not
            // defined in 'scope'. So update 'scope' and 'node' from hierid
            VeriPseudoTreeNode *last_node = tmp_id.GetLastReferencedNode() ;
            VeriModuleItem *design = (last_node) ? last_node->GetDesign(): 0 ;
            if (last_node) node = last_node ;
            if (design) scope = design->GetScope() ;
            if (design) prefix_scope = scope ;
        }
    }
    if (!_suffix_id) return 0 ; // Can't resolve it, return
    if (!instance_count) {
        verific_int64 s = StaticSizeSignInternal(0, 0) ;
        instance_count = (unsigned)ABS(s) ;
    }

    unsigned term_width = 0 ;
    if (_suffix_id->IsInst() || _suffix_id->IsModport() || _suffix_id->IsInterfacePort()) { // Actual is interface reference
        // Get formal and actual size. Consider only size of dimensions. Do not
        // consider interface size, as formal can be generic interface
        VeriRange *actual_range = _suffix_id->GetDimensions() ;
        unsigned actual_size = 1 ;
        int msb = 0, lsb = 0 ;
        VeriRange *range = actual_range ;
        while(range) { // Calculate actual size
            actual_size = actual_size * range->GetWidth(&msb, &lsb, 0) ;
            range = range->GetNext() ;
        }
        term_width = actual_size ; // Set term size
        if ((term_width == master_width) && (term_width != 1)) {
            // Actual is interface reference, terminal width is same as formal width,
            // but formal width is not 1. In this case break the terminal into
            // elements of size 1.
            Array *terms = SplitTerm(1, 0, instance_count, node, top_mods, mod_map_tab, 0) ;
            // Create a concatenation from broken elements
            VeriAssignmentPattern *concat = new VeriAssignmentPattern(0, terms) ;
            concat->SetLinefile(Linefile()) ;
            VeriMapForCopy id_map_table ;
            Array *storing_split_term = new Array(instance_count) ;
            storing_split_term->Insert(concat) ;
            for (unsigned i = 1; i < instance_count; i++) {
                VeriExpression *ele = concat->CopyExpression(id_map_table) ;
                // Only copy 'this' and return
                storing_split_term->Insert(ele) ;
            }
            return storing_split_term ;
        }
    } else { // Actual is normal variable
        if (formal_id && formal_id->IsInterfacePort()) {
            Warning("formal port %s expects a modport or interface instance actual", formal_id ? formal_id->Name() : "") ;
            return 0 ;
        }
        // Calculate the width of this terminal
        verific_int64 this_ss = StaticSizeSignInternal(0, 0) ;
        term_width = (unsigned)ABS(this_ss) ;
    }

    // Check validity of this terminal and issue warning message
    if ((term_width != master_width) && ((master_width * instance_count) != term_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        return 0 ;
    }
    if (term_width == master_width) { // formal_width == actual_width
        Array *storing_split_term = new Array(instance_count) ;
        VeriMapForCopy id_map_table ;
        for(unsigned i = 0 ; i < instance_count; i++) {
            // Only copy 'this' 'instance_count' times
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }

    if (formal_id) {
        // Flag to check whether we are selecting in the same dimension as that of the division
        unsigned select_in_this_dimension = 0 ;
        // Some checks for arrays and its packed/unpacked dimensions
        unsigned formal_unpacked_dim = formal_id->UnPackedDimension() ;
        unsigned actual_unpacked_dim = (_suffix_id) ? _suffix_id->UnPackedDimension() : 0 ; //JJ: added null check for Coverity
        if (actual_unpacked_dim) {
            VeriRange *actual_up_rng = _suffix_id->GetDimensionAt(0) ;
            verific_int64 actual_up_ss = actual_up_rng ? actual_up_rng->StaticSizeSignInternal(0, 0) : 0 ;
            unsigned actual_up_width = (unsigned)ABS(actual_up_ss) ;
            VeriRange *formal_up_rng = formal_unpacked_dim ? formal_id->GetDimensionAt(0) : 0 ;
            verific_int64 formal_up_ss = formal_up_rng ? formal_up_rng->StaticSizeSignInternal(0, 0) : (int)actual_up_width ;
            unsigned formal_up_width = (unsigned)ABS(formal_up_ss) ;
            // If the size of the least-varying dimension of the actual and the size of
            // least-varying dimension of the formal do not match, then we are selecting in the
            // least-varying dimension of the actual. Other cases will not proceed from the next if check.
            if (actual_up_width != formal_up_width) select_in_this_dimension = 1 ;
            if (select_in_this_dimension && select_in_up_dim) *select_in_up_dim = 1 ;
            // Get the size of the next unpacked dimension, if it is there:
            // Since 'this' is an selected name, dimension 0 will be selected, next dimension is 1.
            VeriRange *next_dim = (actual_unpacked_dim > 1) ? _suffix_id->GetDimensionAt(1) : 0 ;
            verific_int64 next_dim_ss = (next_dim) ? next_dim->StaticSizeSignInternal(0, 0) : 0 ;
            unsigned next_dim_width = (unsigned)ABS(next_dim_ss) ;
            // If the actual has one more number of unpacked dimension then it must have
            // same number of element as that of the formal. We will select in the next dimension.
            // Or if there is not same number of element, the next dimension should have same
            // number of element and we will select in first dimension, i.e., in 0th dimension.
            // If the actual has same number of unpacked dimension as that of the formal then, it
            // must be have instance_count times element in it, so that we can select into it.
            if (!(((formal_unpacked_dim == actual_unpacked_dim-1) && ((actual_up_width == formal_up_width) ||
                  /* or if we are selecting in 0th dimension then: */ (next_dim_width == formal_up_width))) ||
                  ((formal_unpacked_dim == actual_unpacked_dim  ) && (actual_up_width == instance_count*formal_up_width)))) {
                Error("incompatible number of unpacked dimensions in instantiation") ;
                return 0 ;
            }

            // If number of unpacked dimensions match, their corresponding size should also match,
            // except the least varying one, on which we will select and split the actual
            int msb, lsb ;
            for(unsigned i=1; i<actual_unpacked_dim; i++) {
                // Get the corresponding dimension to match. If we are selecting in the
                // least-varying dimension, then don't check that dimension, we have already
                // checked that one previously.
                unsigned dim_no = i - ((select_in_this_dimension) ? 0 : 1) ;
                // Each corresponding dimension must have same size
                VeriRange *formal_this_dim = formal_id->GetDimensionAt(dim_no) ;
                VeriRange *id_this_dim = _suffix_id->GetDimensionAt(i) ;
                if (formal_this_dim && id_this_dim && (formal_this_dim->GetWidth(&msb, &lsb, 0) != id_this_dim->GetWidth(&msb, &lsb, 0))) {
                    Error("size of the corresponding unpacked dimensions do not match") ;
                    return 0 ;
                }
            }
            // VIPER #3265: We don't need this check. We already checked the total size as well
            // as the number of unpacked dimensions and their corresponding sizes. So the packed
            // size is bound to be equal. Also, the check became wrong due to SV typedef. With
            // typedef, data type can include unpacked dimensions. Enhancing/adjusting the check
            // does not help either. Because it is possible to select into packed dimensions as
            // well as unpacked dimensions in the same instance. So, if we really need to check
            // then it should be the size of formal vs. the actual after splitting and that is
            // already done all the way above. So, just comment this code part out.
            /* if (!_suffix_id->IsInst()) { // Check packed size only for normal variables
                // Packed size of both the formal and actual should exactly match
                // If it has data type, then packed size is the size of the data type (including the
                // packed dimensions), otherwise, it is a single bit element
                VeriDataType *suffix_id_data_type = _suffix_id->GetDataType() ;
                int suffix_id_ss = suffix_id_data_type ? suffix_id_data_type->StaticSizeSignInternal(0, 0) : 1 ;
                unsigned my_packed_size = (unsigned)ABS(suffix_id_ss) ;
                VeriDataType *formal_data_type = formal_id->GetDataType() ;
                int formal_packed_ss = formal_data_type ? formal_data_type->StaticSizeSignInternal(0, 0) : 1 ;
                unsigned formal_packed_size = (unsigned)ABS(formal_packed_ss) ;

                if (my_packed_size != formal_packed_size) {
                    // Packed size of both the formal and the actual does not match!
                    error("packed size mismatch between formal and actual") ;
                    return 0 ;
                }
            } */
        } else if (formal_unpacked_dim) {
            // Actual does not have any unpacked dimension, but formal has
            // and the actual is directly connected - it is an error
            Error("unpacked dimension mismatch between formal and actual") ;
            return 0 ;
        }
    }

    Array *storing_split_term = new Array(instance_count) ;

    // Get the first (un)packed dimension, if any - it is the least varying dimension
    VeriRange *least_varying_dim = (_suffix_id) ? _suffix_id->GetDimensionAt(0) : 0 ; //JJ: Add null check for Coverity
    VeriArrayRange *dummy_dim = 0 ;
    if (least_varying_dim) {
        dummy_dim = new VeriConstArrayRange(least_varying_dim) ;
        // Get the number of total (packed and unpacked) dimensions we can select:
        unsigned dims = _suffix_id->PackedDimension() + _suffix_id->UnPackedDimension() ;
        for (unsigned i=1; i<dims; i++) {
            VeriRange *rng = _suffix_id->GetDimensionAt(i) ;
            // Add the next dimensions at the end of this linked list:
            dummy_dim->AddDimension(new VeriConstArrayRange(rng)) ;
        }
        // Get the base data type where we are stopped with the dimensions:
        VeriDataType *base_type = _suffix_id->GetBaseDataType() ;
        // Get the size of all the dimensions we can index into:
        unsigned dim_size = dummy_dim->Size() ;
        VERIFIC_ASSERT(dim_size) ; // Size should never be 0
        // Get the base size of the actual (size of the base data type):
        unsigned base_size = term_width / dim_size ;
        // Add the last dimension with the base size (if it is packed we can index into it, otherwise not):
        dummy_dim->AddDimension(new VeriConstArrayRange((int)base_size-1, 0, ((base_type) ? base_type->IsUnpacked() : 0))) ;
    } else {
        // It does not have any dimension at all (packed or unpacked)
        VeriDataType *id_data_type = (_suffix_id) ? _suffix_id->GetDataType() : 0 ; //JJ: null check of _suffix_id for Coverity
        if (id_data_type && (id_data_type->IsEnumType() || id_data_type->IsUnionType() || id_data_type->IsStructType())) {
            // Can't do anything other than copying and returning 'this'. Becasue,
            // they can't be treated as arrays, so we can't really select on these
            // type of elements, produce error and copy the term itself:
            //Error("cannot split unpacked item without dimension") ;
            Error("cannot index into unpacked base of %s", ((_suffix_id)?_suffix_id->Name():((_suffix)?_suffix:"<unknown>"))) ;
            for (unsigned i = 0; i < instance_count; i++) {
                VeriMapForCopy id_map_table ;
                // Only copy 'this'
                storing_split_term->Insert(CopyExpression(id_map_table)) ;
            }
            return storing_split_term ;
        }
        // It is like int/integer/byte etc, so the size of the dimension is in term_width:
        dummy_dim = new VeriConstArrayRange((int)term_width-1, 0) ;
    }

    VERIFIC_ASSERT(dummy_dim) ; // It must be there, we have just created it
    unsigned actual_is_instance = 0 ;

    if (_suffix_id->IsInst() && _suffix_id->GetDimensions()) {
        // If suffix id is array instance identifier, we have to pick proper instance
        // identifier created due to array instance elaboration. To find those extract
        // prefix scope.
        actual_is_instance = 1 ;
        if (!prefix_scope) {
            prefix_scope = (_prefix_id) ? _prefix_id->LocalScope(): scope ; // Get local scope of prefix id
            if (!prefix_scope && _prefix_id && _prefix_id->IsInst()) {  // Prefix id is instance
                VeriModule *instantiated_module = _prefix_id->GetInstantiatedModule() ;
                prefix_scope = instantiated_module ? instantiated_module->GetScope(): 0 ; // Get instantiated module's sc
            }
        }
    }
    // Now split the actual in the number of instances:
    for (unsigned i = 0; i < instance_count; i++) {
        unsigned j ;
        VeriExpression *ret_term = 0 ;
        if (actual_is_instance && prefix_scope) { // Formal is interface instance id
            Array indices ;
            // Get the indices that should be slected for this array instance flattening:
            dummy_dim->GetInstanceIndices(master_width, term_width, instance_count, _suffix_id->Name() /* prefix */, indices) ;
            char *name = 0 ;
            if (indices.Size() > 1) {
                // Find and insert all the instance ids into this array with the name:
                Array *exprs = new Array(indices.Size()) ;
                FOREACH_ARRAY_ITEM(&indices, j, name) {
                    VeriIdDef *new_suffix = prefix_scope->FindLocal(name) ;
                    // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
                    if (!new_suffix) new_suffix = (prefix_scope->IsGenerateScope()) ? prefix_scope->Find(name, 0): 0 ;
                    if (exprs && new_suffix) {
                        VeriName *new_suffix_ref ;
                        if (_prefix) {
                            VeriMapForCopy old2new ;
                            new_suffix_ref = new VeriSelectedName(_prefix->CopyName(old2new), name) ;
                        } else {
                            new_suffix_ref = new VeriIdRef(name) ;
                        }
                        new_suffix_ref->SetLinefile(Linefile()) ;
                        new_suffix_ref->SetId(new_suffix) ; // Set new suffix id
                        exprs->InsertLast(new_suffix_ref) ;
                    } else { // VIPER #7308 : Not possible to create
                        delete exprs ;
                        exprs = 0 ;
                        Strings::free(name) ;
                        // Do not call break to delete all elements of 'indices'
                    }
                }
                if (VeriNode::IsSystemVeri()) {
                    if (exprs) ret_term =  new VeriAssignmentPattern(0, exprs) ;
                } else {
                    if (exprs) ret_term =  new VeriConcat(exprs) ;
                }
            } else if (indices.Size()) {
                // Find the instance id of the indexed name:
                name = (char *)indices.GetFirst() ;
                VeriIdDef *new_suffix = prefix_scope->FindLocal(name) ;
                // VIPER #7493 : If scope is generate scope, find 'name' in upper scopes too
                if (!new_suffix) new_suffix = (prefix_scope->IsGenerateScope()) ? prefix_scope->Find(name, 0): 0 ;
                if (new_suffix) {
                    if (_prefix) {
                        VeriMapForCopy old2new ;
                        ret_term = new VeriSelectedName(_prefix->CopyName(old2new), name) ;
                    } else {
                        ret_term = new VeriIdRef(name) ;
                    }
                    ret_term->SetId(new_suffix) ; // Set new suffix id
                } else {
                    Strings::free(name) ;
                }
            }
        } else { // formal is normal variable
            Array *indices = new Array() ;
            dummy_dim->GetIndices(master_width, term_width, instance_count, *indices, this) ;
            VeriMapForCopy old2new ;
            if (indices->Size() > 1) {
                // Multiple indices : create indexed memory id for this:
                ret_term = new VeriIndexedMemoryId(CopyName(old2new), indices) ;
            } else if (indices->Size()) {
                // Single index : create indexed id only:
                ret_term = new VeriIndexedId(CopyName(old2new), (VeriExpression *)indices->GetFirst()) ;
                delete indices ;
            }
            if (ret_term) ret_term->SetId(_suffix_id) ; // Set the id of the created indexed/indexed memory id
        }
        if (!ret_term) {
            // Something bad happened, clean-up and return 0 from here:
            VeriExpression *expr ;
            FOREACH_ARRAY_ITEM(storing_split_term, j, expr) delete expr ;
            delete storing_split_term ;
            // Delete the created dummy dimension for splitting
            delete dummy_dim ;
            return 0 ;
        }
        // Set the proper line-file information of the new parse tree node created:
        ret_term->SetLinefile(Linefile()) ;
        storing_split_term->Insert(ret_term) ;
    }

    // Delete the created dummy dimension for splitting
    delete dummy_dim ;

    return storing_split_term ;
}

Array *VeriConst::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode * /*node*/, Map * /*top_mods*/, Map * /*mod_map_tab*/, unsigned * /*select_in_up_dim*/)
{
    if (!master_width) return 0 ;

    // Calculate the width of this terminal
    verific_int64 this_ss = StaticSizeSignInternal(0, 0) ;
    if (!instance_count) instance_count = (unsigned)ABS(this_ss) ;

    unsigned term_width = (unsigned)ABS(this_ss) ;
    // Check validity of this terminal and issue warning message
    if ((term_width != master_width) && ((master_width * instance_count) != term_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        return 0 ;
    }
    if (term_width == master_width) { // formal_width == actual_width
        // Copy 'this' instance_count' times and return
        Array *storing_split_term = new Array(instance_count) ;
        VeriMapForCopy id_map_table ;
        for(unsigned i = 0 ; i < instance_count; i++) {
            // Only copy 'this'
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }
    // term_width == (master_width * instance_count)
    // We have to split the value here
    // First evaluate this
    VeriBaseValue *val = StaticEvaluateInternal(0, 0, 0, 0) ;
    if (!val) return 0 ;

    // Get binary equivalent of this value
    unsigned sign = 0 ;
    char *str_val = val->GetBinaryValue(&sign) ;

    delete val ;
    if (!str_val || (Strings::len(str_val) != term_width)) {
        Strings::free(str_val) ;
        return 0 ; // May be real value
    }

    char *stored_str = str_val ;
    Array *storing_split_term = new Array(instance_count) ; // Array to store created values
    while(*str_val) {
        // Create VeriConstVal with master_width size
        char *str = Strings::allocate(master_width) ;
        str[0] = 0 ;
        unsigned iter ;
        for (iter = 0; iter < master_width; iter++) {
            str[iter] = *str_val ;
            str_val++ ;
        }
        str[iter] = 0 ;
        char *x = Strings::save((sign) ? "'sb": "'b", str) ;
        VeriExpression *ele = new VeriConstVal(x, VERI_BASED_NUM, master_width) ;
        ele->SetLinefile(Linefile()) ;
        storing_split_term->InsertLast(ele) ;
        Strings::free(str) ;
        Strings::free(x) ;
    }
    Strings::free(stored_str) ;
    return storing_split_term ;
}

Array *VeriIndexedExpr::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned * /*select_in_up_dim*/)
{
    if (!master_width || !_prefix) return 0 ;
    if (!_idx) return _prefix->SplitTerm(master_width, formal_id, instance_count, node, top_mods, mod_map_tab, 0) ;

    Array *storing_split_term = new Array(instance_count) ;

    // First break the prefix in 1 bit size, we want the individual bits:
    Array *individual_bits = _prefix->SplitTerm(1, 0, 0, node, top_mods, mod_map_tab, 0) ;
    if (!individual_bits) {
        delete storing_split_term ; // cleanup before return
        return 0 ;
    }

    // VIPER #4505 : Calculate size after callling SplitTerm on individual element
    // If any element is unresolved, SplitTerm will resolve that
    verific_int64 term_ss = StaticSizeSignInternal(0, 0) ;
    unsigned term_width = (unsigned)ABS(term_ss) ;
    if (!instance_count) {
        instance_count = (unsigned)ABS(term_ss) ;
    }

    // Check validity of the terminal, issue warning and return
    if ((term_width != (master_width * instance_count)) && (term_width != master_width)) {
        if (formal_id && formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        delete individual_bits ; // cleanup before return
        delete storing_split_term ; // cleanup before return
        return 0 ;
    }
    if (term_width == master_width) {
        // Delete created array
        delete individual_bits ;
        // formal and actual are of same size, copy the expression 'instance_count' times and return
        VeriMapForCopy id_map_table ;
        for(unsigned i = 0; i < instance_count; i++) {
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }
    if (master_width == 1) { // formal is 1 bit
        delete storing_split_term ;
        return individual_bits ;
    }
    // formal multi-bit, group 1 bit expressions to create multi-bit concat
    for (unsigned i = 0, j = 0; j < instance_count; j++) {
        Array *single_bit = new Array(master_width) ;
        for(; i < ((j + 1) * master_width); i++) {
            single_bit->Insert(individual_bits->At(i)) ;
        }
        VeriConcat *concated_bit = new VeriConcat(single_bit) ;
        concated_bit->SetLinefile(Linefile()) ;
        storing_split_term->Insert(concated_bit) ;
    }
    delete individual_bits ;

    return storing_split_term ;
}
Array *VeriAssignmentPattern::SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned * /*select_in_up_dim*/)
{
    // VIPER #8034 : Support assignment pattern as actual of interface type port
    if (!formal_id || !formal_id->IsInterfacePort()) {
        Warning("%s is not supported for static elaboration", "assignment pattern as actual") ;
        return 0 ;
    }
    if (!master_width) return 0 ;

    // Split the concat expression to array of 1 bit expressions
    Array *individual_bit = new Array(2) ;
    unsigned i ;
    unsigned j ;
    VeriExpression *current_child ;
    FOREACH_ARRAY_ITEM(_exprs, j, current_child) {
        if (!current_child) continue ;
        Array *temp_array1 = current_child->SplitTerm(1, 0, 1, node, top_mods, mod_map_tab, 0) ;
        if (!temp_array1) {
            FOREACH_ARRAY_ITEM(individual_bit, i, current_child) delete current_child ;
            delete individual_bit ;
            return 0 ;
        }
        individual_bit->Append(temp_array1) ;
        delete temp_array1 ;
    }
    unsigned term_width = individual_bit->Size() ;
    if (!instance_count) {
        instance_count = term_width ;
    }

    // Check validity of the terminal, issue warning and return
    if ((term_width != (master_width * instance_count)) && (term_width != master_width)) {
        if (formal_id->Name()) {
            //Warning("actual bit length %d differs from formal bit length %d for port %s", term_width, master_width, formal_id->Name()) ;
            Error("width of actual %d differs from width of formal %d for port %s", term_width, master_width, formal_id->Name()) ; // VIPER #6991: Different message for Error
        } else {
            //Warning("actual bit length %d differs from formal bit length %d", term_width, master_width) ;
            Error("width of actual %d differs from width of formal %d", term_width, master_width) ; // VIPER #6991: Different message for Error
        }
        FOREACH_ARRAY_ITEM(individual_bit, i, current_child) delete current_child ;
        delete individual_bit ; // cleanup before return
        return 0 ;
    }
    Array *storing_split_term = new Array(instance_count) ;
    if (term_width == master_width) {
        // Delete created array
        FOREACH_ARRAY_ITEM(individual_bit, i, current_child) delete current_child ;
        delete individual_bit ;
        // formal and actual are of same size, copy the expression 'instance_count' times and return
        VeriMapForCopy id_map_table ;
        for(i = 0; i < instance_count; i++) {
            storing_split_term->Insert(CopyExpression(id_map_table)) ;
        }
        return storing_split_term ;
    }
    if (master_width == 1) { // formal is 1 bit
        delete storing_split_term ;
        return individual_bit ;
    }
    // formal multi-bit, group 1 bit expressions to create multi-bit concat
    for (i = 0, j = 0; j < instance_count; j++) {
        Array *single_bit = new Array(master_width) ;
        for(; i < ((j + 1) * master_width); i++) {
            single_bit->Insert(individual_bit->At(i)) ;
        }
        VeriAssignmentPattern *concated_bit = new VeriAssignmentPattern(0, single_bit) ;
        concated_bit->SetLinefile(Linefile()) ;
        storing_split_term->Insert(concated_bit) ;
    }
    delete individual_bit ;

    return storing_split_term ;
    return 0 ;
}
Array *VeriMultiAssignmentPattern::SplitTerm(unsigned /*master_width*/, const VeriIdDef * /*formal_id*/, unsigned /*instance_count*/, VeriPseudoTreeNode * /*node*/, Map * /*top_mods*/, Map * /*mod_map_tab*/, unsigned * /*select_in_up_dim*/)
{
    Warning("%s is not supported for static elaboration", "assignment pattern as actual") ;
    return 0 ;
}
// Algorithim followed by this routine is as follows
//   * If the second argument is NULL assume a default name
//   * Check if any object of this name is existed in the scope
//   * If yes, initialize an index.
//   * Create name with the index 'name<index_value>'
//   * Check if any object of the new name is existed in the scope
//   * Increment the index
//   * Above three steps are repeated untill an unique name is found.
char *VeriPseudoTreeNode::GetUniqueName(const VeriScope *scope, const char *name)
{
    if (!scope) return 0 ; // Can't create an unique name if scope is not there

    char *ret_name = 0 ;
    // If second argument is null then default name is 'tmp'
    if (!name) name = "tmp" ;
    ret_name = Strings::save(name) ;
    // Check if any object with 'name' is existed in the scope
    VeriIdDef *id = scope->FindLocal(ret_name) ;

    // Index to create new name
    unsigned idx = 0 ;
    while (id) {
        char *tmp = ret_name ;
        char *suffix = Strings::itoa((int)idx) ;
        idx++ ; // increment index
        // create new name with the updated index
        ret_name = Strings::save(name, suffix) ;
        Strings::free(tmp) ;
        Strings::free(suffix) ;

        // Check if any object with this name is already existed in the scope
        id = scope->FindLocal(ret_name) ;
    }
    return ret_name ;
}
//////////////////////////////////////////////////////////////////////
// NonAnsiPortDir
//////////////////////////////////////////////////////////////////////
// Derive the direction of a port from a (Verilog 95) port expression.
// Also check for a valid port expression

// VIPER #3066 : Routine to extract direction of port expression. These routines
// are same as PortExpressionDir. But we need to implement this so that we will
// not issue error for mixed direction ports in concat port expression.
unsigned VeriExpression::NonAnsiPortDir() const
{
    return PortExpressionDir() ;
}
unsigned VeriConcat::NonAnsiPortDir() const
{
    unsigned i ;
    VeriExpression *expr ;
    unsigned result = 0 ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        dir = expr->NonAnsiPortDir() ;
        if (result!=dir) {
            if (result && dir && (((dir == VERI_INPUT) && ((result == VERI_OUTPUT) || (result == VERI_INOUT))) ||
                     ((result == VERI_INPUT) && ((dir == VERI_OUTPUT) || (dir == VERI_INOUT))))) {
                // Mix of input and inout/output : result inout direction
                return VERI_INOUT ;
            }
            result = dir ;
        }
    }
    return result ;
}

///////////////////////////////////////////////////////////////////////////
///////////////      Class elaboration                  ///////////////////
///////////////////////////////////////////////////////////////////////////

// class to visit identifier references. For each identifier reference it checks
// whether this identifier exists inside old-VeriIdDef*->new-VeriIdDef* map, if
// exists, this identifier reference is changed to point new identifier
class ChangeIdRef : public VeriVisitor
{
public:
    explicit ChangeIdRef(VeriMapForCopy *old2new) : VeriVisitor(), _id_map(old2new), _base_scope(0), _class_scope(0), _interface_class(0) { }
    virtual ~ChangeIdRef() { _id_map = 0 ; _base_scope = 0 ; _class_scope = 0 ; }

private:
    // Prevent compiler from defining the following
    ChangeIdRef(const ChangeIdRef &) ;            // Purposely leave unimplemented
    ChangeIdRef& operator=(const ChangeIdRef &) ; // Purposely leave unimplemented

public:
    virtual void VERI_VISIT(VeriClass, node) ;
    virtual void VERI_VISIT(VeriIdRef, node) ;
    virtual void VERI_VISIT(VeriKeyword, node) ;
    virtual void VERI_VISIT(VeriIndexedId, node) ;
    virtual void VERI_VISIT(VeriIndexedMemoryId, node) ;
    virtual void VERI_VISIT(VeriSelectedName, node) ;
    virtual void VERI_VISIT(VeriTypeOperator, node) ;
    virtual void VERI_VISIT(VeriTypeRef, node) ;
    virtual void VERI_VISIT(VeriFunctionDecl, node) ;
    virtual void VERI_VISIT(VeriTaskDecl, node) ;
    VeriIdDef *  GetCopiedId(const VeriIdDef *id) const ;
private:
    VeriMapForCopy *_id_map ; // Map of old-VeriIdDef*->new-VeriIdDef*
    VeriScope      *_base_scope ; // Scope of base class (VIPER #4728)
    VeriScope      *_class_scope ;  // Scope of the class
    unsigned        _interface_class ; // Processing interface class
} ; // class ChangeIdRef

/* -------------------------------------------------------------- */
VeriIdDef *ChangeIdRef::GetCopiedId(const VeriIdDef *id) const
{
    if (!id) return 0 ;
    VeriIdDef *new_id = (_id_map) ? (VeriIdDef*)_id_map->GetValue(id): 0 ;
    if (new_id && _base_scope && _base_scope->GetOwner() == new_id) return 0 ;
    return new_id ;
}

void ChangeIdRef::VERI_VISIT(VeriClass, node)
{
    // Get the scope of the class
    VeriScope *old_class_scope = _class_scope ;
    _class_scope = node.GetScope() ;

    unsigned old_interface_class = _interface_class ;
    _interface_class = node.GetQualifier(VERI_INTERFACE) ? 1 : 0 ;

    VeriScope *old_base_scope = _base_scope ;
    _base_scope = _class_scope ? _class_scope->GetBaseClassScope(): 0 ;
    // Traverse parameter port list
    TraverseArray(node.GetParameterConnects()) ;

    // Traverse Class items
    TraverseArray(node.GetItems()) ;
    _base_scope = old_base_scope ;
    _class_scope = old_class_scope ;
    _interface_class = old_interface_class ;
}
void ChangeIdRef::VERI_VISIT(VeriIdRef, node)
{
    VeriIdDef *id = node.GetId() ;
    // For interface class always find referenced id from scope to produce error
    // for conflicting object access
    if (_interface_class && _class_scope) {
        VeriIdDef *new_id = _class_scope->Find(node.GetName(), &node) ;
        if (new_id && (new_id != id)) node.SetId(new_id) ;
        return ;
    }
    // If the identifier is not resolved, find it from the class scope and set it.
    if (!id && _class_scope) {
        id = _class_scope->Find(node.GetName(), &node) ;
        if (id) node.SetId(id) ;
    } else {
        VeriIdDef *new_id = GetCopiedId(id) ;
        if (new_id && (new_id != id)) node.SetId(new_id) ;
    }
}
// Check whether the keyword is this/super, if so set the class id/base id
// and also set class name/base class name.
void ChangeIdRef::VERI_VISIT(VeriKeyword, node)
{
    unsigned token = node.GetToken() ;
    if (token == VERI_THIS) {
        VeriIdDef *class_id = _class_scope ? _class_scope->GetClass() : 0 ;
        if (class_id) {
            node.SetId(class_id) ;
            node.SetName(Strings::save(class_id->GetName())) ;
        }
    }
    if (token == VERI_SUPER) {
        VeriIdDef *base_id = _base_scope ? _base_scope->GetClass() : 0 ;
        if (base_id) {
            node.SetId(base_id) ;
            node.SetName(Strings::save(base_id->GetName())) ;
        }
    }
}
void ChangeIdRef::VERI_VISIT(VeriIndexedId, node)
{
    VeriName *prefix = node.GetPrefix() ;
    if (prefix) prefix->Accept(*this) ;
    VeriIdDef *id = (prefix) ? prefix->GetId(): 0 ;
    VeriIdDef *new_id = GetCopiedId(id) ;
    if (new_id && (new_id != id)) node.SetId(new_id) ;

    VeriExpression *idx = node.GetIndexExpr() ;
    if (idx) idx->Accept(*this) ;
}
void ChangeIdRef::VERI_VISIT(VeriIndexedMemoryId, node)
{
    VeriName *prefix = node.GetPrefix() ;
    if (prefix) prefix->Accept(*this) ;
    VeriIdDef *id = (prefix) ? prefix->GetId(): 0 ;
    VeriIdDef *new_id = GetCopiedId(id) ;
    if (new_id && (new_id != id)) node.SetId(new_id) ;

    Array *indexes = node.GetIndexes() ;
    unsigned i ;
    VeriExpression *idx ;
    FOREACH_ARRAY_ITEM(indexes, i, idx) {
        if (idx) idx->Accept(*this) ;
    }
}
void ChangeIdRef::VERI_VISIT(VeriSelectedName, node)
{
    VeriName *prefix = node.GetPrefix() ;
    if (prefix) prefix->Accept(*this) ;
    VeriIdDef *id = (prefix) ? prefix->GetId(): 0 ;
    if (id && _base_scope && id != _base_scope->GetOwner()) node.SetPrefixId(id) ;
    //if (id) node.SetPrefixId(id) ;
    VeriIdDef *new_id = GetCopiedId(id) ;
    if (new_id && (new_id != id)) node.SetPrefixId(new_id) ;

    VeriIdDef *new_suffix_id = GetCopiedId(node.GetId()) ;
    if (!new_suffix_id) {
        id = (prefix) ? prefix->GetId(): 0 ;
        VeriScope *prefix_scope = id ? id->LocalScope(): 0 ;
        new_suffix_id = prefix_scope ? prefix_scope->FindLocal(node.GetSuffix()): 0 ;
    }
    if (new_suffix_id && (new_suffix_id != node.GetId())) node.SetId(new_suffix_id) ;
}
void ChangeIdRef::VERI_VISIT(VeriTypeOperator, node)
{
    VeriIdDef *id = node.GetId() ;
    VeriIdDef *new_id = GetCopiedId(id) ;
    if (new_id && (new_id != id)) node.SetId(new_id) ;
    VeriVisitor::VERI_VISIT_NODE(VeriTypeOperator, node) ;
}
void ChangeIdRef::VERI_VISIT(VeriTypeRef, node)
{
    VeriIdDef *id = node.GetId() ;
    VeriIdDef *new_id = GetCopiedId(id) ;
    if (new_id && (new_id != id)) node.SetId(new_id) ;
    // VIPER #7896 : Try to resolve type ref first and then set copied id
    VeriVisitor::VERI_VISIT_NODE(VeriTypeRef, node) ;
    if (!id) {
        VeriName *type_name = node.GetTypeName() ;
        id = type_name ? type_name->GetId(): 0 ;
        if (id) node.SetId(id) ;
    }
}
// VIPER #4728: Check task/function declaration. If this is not virtual, check if
// it is defined in base class with virtual keyword. If so, set that qualifier to
// this subprogram.
void ChangeIdRef::VERI_VISIT(VeriFunctionDecl, node)
{
    VeriVisitor::VERI_VISIT_NODE(VeriFunctionDecl, node) ;
    if (!node.GetQualifier(VERI_VIRTUAL)) { // Virtual is not set
        VeriIdDef *func_id = node.GetId() ;
        // Find this in base scope
        VeriScope *base_class_scope = _base_scope ;
        // VIPER #5435 : Search hierarchically upward to check whether function
        // was declared as virtual
        while (base_class_scope) {
            VeriIdDef *base_func = base_class_scope->FindLocal(func_id ? func_id->GetName(): 0) ;
            VeriModuleItem *base_func_body = (base_func) ? base_func->GetModuleItem(): 0 ;
            if (base_func && base_func_body && (base_func_body->GetQualifier(VERI_VIRTUAL))) {
                // Check data type of virtual function in base class with this
                if (func_id) func_id->ValidateVirtualMethod(base_func) ;
                node.SetQualifier(VERI_VIRTUAL) ;
                break ;
            }
            base_class_scope = base_class_scope->GetBaseClassScope() ;
        }
    }
}
void ChangeIdRef::VERI_VISIT(VeriTaskDecl, node)
{
    VeriVisitor::VERI_VISIT_NODE(VeriTaskDecl, node) ;
    if (!node.GetQualifier(VERI_VIRTUAL)) { // Virtual is not set
        VeriIdDef *task_id = node.GetId() ;
        // Find this in base scope
        // VIPER #5435 : Search hierarchically upward to check whether function
        // was declared as virtual
        VeriScope *base_class_scope = _base_scope ;
        while (base_class_scope) {
            VeriIdDef *base_func = base_class_scope->Find(task_id ? task_id->GetName(): 0) ;
            VeriModuleItem *base_func_body = (base_func) ? base_func->GetModuleItem(): 0 ;
            if (base_func && base_func_body && base_func_body->GetQualifier(VERI_VIRTUAL)) {
                // Check data type of virtual function in base class with this
                if (task_id) task_id->ValidateVirtualMethod(base_func) ;
                node.SetQualifier(VERI_VIRTUAL) ;
                break ;
            }
            base_class_scope = base_class_scope->GetBaseClassScope() ;
        }
    }
}

VeriDataType* VeriDataType::StaticElaborateClassType(VeriScope * /*instance_scope*/, Array * /*container_items*/, VeriModuleItem * /*from_decl*/, Map * /*mod_map_tab*/) { return 0 ; }
VeriDataType* VeriNetDataType::StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab)
{
     VeriDataType *new_data_type = (_data_type) ? _data_type->StaticElaborateClassType(instance_scope, container_items, from_decl, mod_map_tab): 0 ;
     if (new_data_type) (void) ReplaceChildDataTypeInternal(_data_type, new_data_type, 1) ;
     return 0 ;
}
VeriDataType* VeriEnum::StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab)
{
    VeriDataType *new_base = (_base_type) ? _base_type->StaticElaborateClassType(instance_scope, container_items, from_decl, mod_map_tab) : 0 ;
    if (new_base) {
        (void) ReplaceChildDataTypeInternal(_base_type, new_base, 1) ;
    }
    return 0 ;
}
VeriDataType* VeriTypeOperator::StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab)
{
    return (_expr_or_data_type) ? _expr_or_data_type->StaticElaborateClassType(instance_scope, container_items, from_decl, mod_map_tab): 0 ;
}
VeriDataType *VeriTypeRef::StaticElaborateClassType(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab)
{
    if (!_id) return 0 ;

    VeriModuleItem *def = _id->GetModuleItem() ;
    if (def && def->IsDataDecl() && _id->IsProcessing()) return 0 ; // Contain self reference of type _id
    _id->SetIsProcessing() ; // Mark as processing

    // If it is type parameter reference or class reference, elaborate its data type and return new
    // data type to replace its usage :
    if ((_id->IsType() && (_id->IsParam() || (_id->IsClass() && def && def->IsClass()))) || (_type_name && _type_name->IsHierName())) {
        VeriName *new_name = (_type_name) ? _type_name->StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, 0, 0): 0 ;
        _id->ClearIsProcessing() ; // Reset processing
        if (new_name) {
            VeriMapForCopy old2new ;
            // VIPER #6092 : Consider existing dimensions to create new data type
            VeriDataType *new_type = new VeriTypeRef(new_name, 0, _dimensions ? _dimensions->CopyRange(old2new):0) ; // Create type with new class
            new_type->SetLinefile(Linefile()) ;
            return new_type ;
        }
        return 0 ;
    }
    // _id is user defined type reference, so we have to elaborate only the data
    // type of this identifier and replace that in its declaration. We cannot just
    // return the elaborated data type (as we do for type parameter), as type id
    // may have unpacked dimension.
    // So get the definition of this type identifier, elaborate its data type and
    // replace data type reference both in definition and back pointer in this id
    if (!def || !def->IsDataDecl()) return 0 ; // No back pointer/back pointer is not declaration
    VeriDataType *data_type = def->GetDataType() ;
    VeriDataType *new_data_type = (data_type) ? data_type->StaticElaborateClassType(instance_scope, container_items, from_decl, mod_map_tab): 0 ;
    _id->ClearIsProcessing() ;
    if (new_data_type) { // Elaboration creates new class
        VeriMapForCopy old2new ;
        Array *ids = def->GetIds() ;
        unsigned i ;
        VeriIdDef *ele_id ;
        FOREACH_ARRAY_ITEM(ids, i, ele_id) {
            if (!ele_id) continue ;
            ele_id->ClearDataType() ;
            ele_id->SetDataTypeForCopy(new_data_type) ;
        }
        (void) def->ReplaceChildDataTypeInternal(data_type, new_data_type, 1) ;
        if (!_id->GetDimensions()) return new_data_type->CopyDataType(old2new) ;
    }
    return 0 ;
}
VeriName *VeriName::StaticElaborateClassName(VeriScope * /*instance_scope*/, Array * /*container_items*/, VeriModuleItem * /*from_decl*/, Map * /*mod_map_tab*/, VeriMapForCopy * /*old2new*/, unsigned /*replace_typeid*/) { return 0 ; }
VeriName *VeriIdRef::StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned replace_typeid)
{
    if (!_id) return 0 ;
    VeriModuleItem *def = _id->GetModuleItem() ;
    if (def && def->IsDataDecl() && _id->IsProcessing()) return 0 ; // Self reference, return
    _id->SetIsProcessing() ;  // Mark as processing is started
    VeriName *ret_name = 0 ;
    VeriModuleItem *class_body = _id->GetModuleItem() ; // Get body of class
    if (_id->IsClass() && class_body && class_body->IsClass()) {
        // Elaborate class body to get new class :
        VeriIdDef *new_id = class_body->StaticElaborateClass(0, instance_scope, container_items, from_decl, mod_map_tab, old2new) ;
        if (new_id) {
            ret_name = new VeriIdRef(new_id) ;
            ret_name->SetLinefile(Linefile()) ;
        }
    } else if (_id->IsType()) { // Type reference, elaborate its data type
        VeriDataType *data_type = _id->GetDataType() ;
        VeriName *class_name = (data_type) ? data_type->GetTypeName() : 0 ;
        ret_name = (class_name) ? class_name->StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, old2new, replace_typeid): 0 ;
        // Viper #5219 : Replace type reference with its data type while processing
        // base class name of any class, for other references do not replace type
        // ids, as that can make the scope name (module.type_id...)illegal  :
        if (!_id->IsParam() && !replace_typeid) {
            if (ret_name) {
                delete ret_name ;
                ret_name = new VeriIdRef(_id) ;
                ret_name->SetLinefile(Linefile()) ;
            }
        } else if (replace_typeid && !ret_name) {
            // VIPER #6123 : ret_name null means data type of this type id is already
            // elaborated, so just return copy of class_name as replace_typeid is 1.
            VeriMapForCopy tmp_old2new ;
            ret_name = class_name ? class_name->CopyName(tmp_old2new): 0 ;
        }
    }
    _id->ClearIsProcessing() ; // Reset is_processing flag
    return ret_name ;
}
VeriName *VeriScopeName::StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned replace_typeid)
{
    // VIPER #4451 : Process the prefix of scope name. Prefix can be class name :
    VeriName *new_prefix = 0 ;
    VeriIdDef *id = GetId() ;
    VeriIdDef *new_suffix_id = 0 ;
    VeriModuleItem *class_body = (_prefix_id) ? _prefix_id->GetModuleItem(): 0 ; // Get body of class
    if (_prefix && _prefix_id && _prefix_id->IsClass()) {
        if (class_body && !class_body->IsClass()) {
            new_prefix = (_prefix) ? _prefix->StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, old2new, replace_typeid) : 0 ;
        } else {
            // VIPER #6201 : Pass parameter overwrite list of _prefix, when _suffix exists
            VeriIdDef *new_id = (class_body) ? class_body->StaticElaborateClass((_suffix) ? _prefix->GetParamValues(): _param_values, instance_scope, container_items, from_decl, mod_map_tab, old2new): 0 ;
            if (new_id && !(new_id == _prefix_id)) { // If elaborated class is different
                new_prefix = new VeriIdRef(new_id) ;
                new_prefix->SetLinefile(_prefix->Linefile()) ;
            }
        }
        // Prefix is changed, point suffix identifier to id defined within new prefix
        VeriIdDef *new_id = new_prefix ? new_prefix->GetId() : 0 ;
        if (new_id) {
            VeriScope *prefix_scope = new_id->LocalScope() ;
            VeriIdDef *suffix_id = (prefix_scope) ? prefix_scope->Find(_suffix) : 0 ;
            if (suffix_id) {
                id = suffix_id ;
                new_suffix_id = id ;
            }
        }
    }
    if (_suffix && id  && id->IsClass()) { // Suffix is also a class, elaborate that too :
        VeriModuleItem *class_ptr = id->GetModuleItem() ; // Get body of class
        VeriIdDef *new_id = 0 ;
        if (class_ptr && class_ptr->IsClass()) {
            new_id = class_ptr->StaticElaborateClass(_param_values, instance_scope, container_items, from_decl, mod_map_tab, old2new) ;
        } else {
            VeriIdRef id_ref(id) ;
            VeriName *ret_ref = id_ref.StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, old2new, replace_typeid) ;
            new_id = (ret_ref && ret_ref->GetId()) ? ret_ref->GetId() : id ;
            delete ret_ref ;
        }

        if (new_id && !(new_id == id)) { // If elaborated class is different
            new_suffix_id = new_id ;
        }
    }
    // Scope name is changed, create a new one :
    if (new_prefix || new_suffix_id) {
        VeriMapForCopy tmp_old2new ;
        VeriName *new_name = 0 ;
        if (!new_prefix && _prefix) new_prefix = _prefix->CopyName(tmp_old2new) ;
        if (_suffix) {
            new_name = new VeriScopeName(new_prefix, Strings::save(new_suffix_id ? new_suffix_id->Name(): _suffix), 0) ;
            if (new_suffix_id) new_name->SetId(new_suffix_id) ;
            if (new_prefix) new_name->SetPrefixId(new_prefix->GetId()) ;
        } else {
            new_name = new_prefix ;
        }
        return new_name ;
    }
    return 0 ;
}
VeriName *VeriFunctionCall::StaticElaborateClassName(VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new, unsigned replace_typeid)
{
    VeriName *new_name = (_func_name) ? _func_name->StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, old2new, replace_typeid) : 0 ;

    if (new_name) {
        VeriMapForCopy localold2new ;
        new_name = new VeriFunctionCall(new_name, (_args) ? CopyExpressionArray(_args, localold2new) : 0) ;
        new_name->SetLinefile(Linefile()) ;
    }
    return new_name ; // Return new class ref or 0
}

VeriIdDef *VeriModuleItem::StaticElaborateClass(Array * /* param_list*/, VeriScope * /*instance_scope*/, Array * /*container_items*/, VeriModuleItem * /*from_decl*/, Map * /*mod_map_tab*/, VeriMapForCopy * /*old2new*/)
{ return 0 ; }
VeriIdDef *VeriClass::StaticElaborateClass(Array *param_list, VeriScope *instance_scope, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab, VeriMapForCopy *old2new)
{
    if (!_scope || !_id) return 0 ;
    VeriScope *parent = (_id) ? _id->GetOwningScope() : 0 ;
    // Now get the container module and its pseudo tree nodes :
    // Get the module item of 'parent' scope
    VeriScope *mod_scope = (parent) ? parent->ModuleScope(): 0 ;
    VeriIdDef *parent_owner = (mod_scope) ? mod_scope->GetOwner(): 0 ;
    VeriModuleItem *container = (parent_owner) ? parent_owner->GetModuleItem() : 0 ;
    Array *this_container_items = container ? container->GetItems(): 0 ;
    // Iterate over the parameter overwrite list and create a name for this class
    unsigned i ;
    VeriExpression *actual ;
    const char *formal_name ;
    VeriIdDef *formal ;
    char *class_name = 0 ;
    char *image = 0 ;
    Map param_map(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(param_list, i, actual) {
        if (!actual) continue ;
        formal_name = actual->NamedFormal() ;
        if (formal_name) {
            // Actual is a 'PortConnect'.
            actual = actual->GetConnection() ;
        }
        formal = 0 ;
        // Find the formal itself if we can :
        if (!_parameters || !_parameters->Size()) {
            actual->Error("class does not expect any parameter") ;
            break ;
        }
        if (formal_name) {
            formal = (_scope) ? _scope->FindLocal(formal_name) : 0 ;
            if (!formal) {
                actual->Error("class %s does not have a parameter named %s", _id->Name(), formal_name) ;
                continue ;
            }
        } else {
            if (_parameters->Size() <= i) {
                actual->Error("too many parameters for class instance") ;
                break ;
            }
            formal = (VeriIdDef*)_parameters->At(i) ;
            formal_name = (formal) ? formal->Name() : 0 ;
        }
        if (!formal_name || !formal) continue ;
        (void) param_map.Insert(formal->Name(), actual) ;
    }
    Map param_values(STRING_HASH) ;
    if (param_list && param_list->Size()) {
        // VIPER #5191 : If parameter overwrite occurs, create signature using overwritten
        // values only when it is different from default value :
        Array tmp_param_val_arr(_parameters ? _parameters->Size(): 1) ;
        // Set the overwritten values to parameters to evaluate default values properly, as
        // default value can refer overwritten parameters :
        char *default_value = 0, *overwritten_value = 0 ;
        VeriExpression *default_expr = 0 ;
        FOREACH_ARRAY_ITEM(_parameters, i, formal) {
            if (!formal) continue ;
            actual = (VeriExpression*)param_map.GetValue(formal->Name()) ; // Get overwritten value
            default_value = 0 ; overwritten_value = 0 ;
            VeriExpression *new_actual = 0 ;
            if (actual) {
                // Get overwritten value :
                new_actual = VeriPseudoTreeNode::GetEvaluatedValue(formal, actual, instance_scope, container_items, from_decl, mod_map_tab) ;
                if (new_actual) {
                    if (new_actual->IsConst()) {
                        overwritten_value = new_actual->Image() ;
                    } else {
                        overwritten_value = new_actual->StaticImage(parent, 0) ;
                    }
                } else {
                    overwritten_value = actual->StaticImage(parent, 0) ;
                }
            }
            // Get default value :
            default_expr =  VeriPseudoTreeNode::GetEvaluatedValue(formal, formal->GetInitialValue(), parent, this_container_items, this, mod_map_tab) ;
            if (default_expr) {
                default_value = (default_expr->IsConst()) ? default_expr->Image(): default_expr->StaticImage(parent, 0) ;
            }

            VeriExpression *pexpr = formal->TakeInitialValue() ;
            if (!pexpr) {
                Strings::free(overwritten_value) ;
                continue ;
            }
            tmp_param_val_arr.InsertLast(pexpr) ;

            if (param_map.GetValue(formal->Name())) {
                (void) param_values.Insert(formal->Name(), new_actual) ; // Store evaluated overwritten value to set later to parameters
                VeriMapForCopy copy_map ;
                new_actual = (new_actual) ? new_actual->CopyExpression(copy_map): 0 ;
                delete default_expr ;
            } else {
                new_actual = default_expr ;
            }
            if (new_actual) formal->SetInitialValue(new_actual) ;

            // If default value is same as overwritten value, do not consider this parameter for
            // signature creation :
            if (default_value && overwritten_value && Strings::compare(default_value, overwritten_value)) {
                Strings::free(default_value) ;
                Strings::free(overwritten_value) ;
                continue ;
            }
            if (overwritten_value) {
                image = overwritten_value ;
                Strings::free(default_value) ;
            } else {
                Strings::free(default_value) ;
                continue ;
            }
            // Char* equivalent of parameter value can have space, replace that
            // space with '_'. FIXME : This replacement can produce name
            // collision.
            char *str = image ;
            while(*str) {
                if (*str == ' ') *str = '_' ; // Replace space with '_'
                str++ ;
            }
            if (!class_name) {
                class_name = Strings::save("(", formal->Name(), "=", image) ;
            } else {
                char *tmp = class_name ;
                class_name = Strings::save(tmp, ",", formal->Name(), "=", image) ;
                Strings::free(tmp) ;
            }
            Strings::free(image) ;
        }
        // Revert back the default values to parameters as we will create copy of this class
        // keeping the original unchanged to process other instantiations of this class using
        // different overwritten values :
        FOREACH_ARRAY_ITEM(_parameters, i, formal) {
            if (!formal) continue ;
            actual = (tmp_param_val_arr.Size()>i) ? (VeriExpression*)tmp_param_val_arr.At(i) : 0 ;
            if (actual) formal->SetInitialValue(actual) ;
        }
    }
    char *tmp = class_name ;
    if (tmp) { // Parameterized class
        class_name = Strings::save(_id->Name(), tmp, ")") ;
    } else {
        // VIPER #4042 : If base class of this is specified by type parameter,
        // we will change the base class link to point to actual class. So
        // if this class is instantiated later with different parameter overwrite
        // value, we cannot change the base class as it is pointing to a class
        // instead of a parameter.
        // class C #(type T=c2) extends T
        // . ..C x ;
        // C #(c3) y ;
        // Elaborated class for first object declaration ( C x;)is
        // class C #(type T=c2) extends c2
        // Copy the class before elaboration if base class is specified by
        // type parameter. This will help us to leave original class def as it was
        // so that it can be processed later
        VeriIdDef *base_id = (_base_class_name) ? _base_class_name->GetId(): 0 ;

        //if (base_id && base_id->IsParam() && base_id->IsType()) {
        if (base_id && base_id->IsType() && !base_id->IsClass()) {
            class_name = Strings::save(_id->Name(), "_", base_id->Name()) ;
        } else {
            class_name = Strings::save(_id->Name()) ;
        }
    }
    if (IsClassInstantiated()) {
        // VIPER #6072 : If class is already instantiated do not consider anything for name creation
        Strings::free(class_name) ;
        class_name = Strings::save(_id->Name()) ;
    } else if (Strings::compare(class_name, _id->Name())) {
        Strings::free(class_name) ;
        class_name = CreateNameWithDefaultparam(0) ;
    }
    Strings::free(tmp) ;

    // Check whether this class with same parameter setting is already copied

    VeriIdDef *exist = (parent) ? parent->Find(class_name) : 0 ;
    VeriModuleItem *to_elaborate_class = this ;
    // VIPER #7508 : If class is instantiated and has no parameter to overwrite, we
    // do not copy that class. But if class contains import declaration, we need to
    // copy that as imported package may be copied
    if (exist && (exist == _id) && !to_elaborate_class->IsClassInstantiated() && VeriStaticElaborator::GetPackageScopeMap()) {
        // Check whether class contains import declaration
        unsigned need_to_copy = 0 ;
        VeriModuleItem *ele ;
        FOREACH_ARRAY_ITEM(_items, i, ele) {
            if (ele && ele->IsImportDecl()) {
                Strings::free(class_name) ;
                class_name = Strings::save(_id->Name(), "_default") ;
                need_to_copy = 1 ;
                break ;
            }
        }
        if (!need_to_copy) {
            // No import declaration, but check whether class is in root scope.
            // If yes, it can use identifiers defined in package. So need to copy
            // the class
            VeriIdDef *parent_id = parent ? parent->GetContainingModule(): 0 ;
            VeriModule *parent_mod = parent_id ? parent_id->GetModule(): 0 ;
            if (parent_mod && parent_mod->IsRootModule()) {
                Strings::free(class_name) ;
                class_name = Strings::save(_id->Name(), "_default") ;
                need_to_copy = 1 ;
            }
        }
        // Check whether class is already copied with '_default'
        if (need_to_copy) {
            VeriIdDef *default_exist = (parent) ? parent->Find(class_name) : 0 ;
            exist = default_exist ;
        }
    }
    if (exist) { // Class exists
        to_elaborate_class = exist->GetModuleItem() ;
        if (to_elaborate_class && to_elaborate_class->IsClassInstantiated()) {
            // Delete param values
            MapIter mi ;
            FOREACH_MAP_ITEM(&param_values, mi, 0, &actual) delete actual ;
            Strings::free(class_name) ;
            // VCS Compatibility issue (test70): When class to be elaborated is already copied and
            // elaborated, populate the coming old2new map (if exists), so that we can change
            // the reference of base class's object in derived class.
            if (old2new) {
                // VIPER #7508 : Associate class id and scope too
                VeriScope *to_elaborate_scope = to_elaborate_class->GetScope() ;
                VeriIdDef *to_elaborate_owner = to_elaborate_scope ? to_elaborate_scope->GetOwner(): 0 ;
                (void) old2new->Insert(_scope ? _scope->GetOwner(): 0, to_elaborate_owner) ;
                old2new->MakeAssociation(_scope, to_elaborate_class->GetScope()) ;
            }
            return exist ;
        }
    } else { // We need to copy this class and elaborate its base
        //VeriMapForCopy old2new ;
        VeriMapForCopy *copy_map = 0 ;
        if (!old2new) {
            copy_map = new VeriMapForCopy() ;
            old2new = copy_map ;
        }
        _contain_copied_version = 1 ; // Mark this class, this class will be copied and copied one will be elaborated

        // VIPER #7508 : Set package scope vs copied package scope map before copying class
        Map *copied_pkg_map = VeriStaticElaborator::GetPackageScopeMap() ;
        if (copied_pkg_map) {
            old2new->SetPkgScopeMap(VeriStaticElaborator::GetPackageScopeMap()) ;
            // Traverse each copied package scope and make association
            // id vs id and scope vs scope
            MapIter mi ;
            VeriScope *old_scope, *new_scope ;
            FOREACH_MAP_ITEM(copied_pkg_map, mi, &old_scope, &new_scope) {
                if (!old_scope || !new_scope) continue ;
                (void) old2new->Insert(old_scope->GetOwner(), new_scope->GetOwner()) ;
                old2new->MakeAssociationForPkg(old_scope, new_scope, _id, _scope) ;
            }
        }
        to_elaborate_class = CopyWithName(class_name, *old2new, 1 /* add copied class*/) ;
        delete copy_map ;
        if (!to_elaborate_class) return 0 ; // Something wrong some where
    }

    // Set overwritten parameter values to copied class :
    VeriIdDef *param ;
    VeriExpression *param_expr, *copied_expr ;
    Array *params = (to_elaborate_class) ? to_elaborate_class->GetParameters(): 0 ;

    // VIPER #4633: Nonparameterized mailbox class is type less. So if no parameter override
    // exists, do not set default value of type parameter as its data type. Not setting that
    // will stop type checking of any methods of nonparameterized mailbox.
    VeriIdDef *class_id = (to_elaborate_class) ? to_elaborate_class->GetId(): 0 ;
    if (!param_list && class_id && to_elaborate_class && Strings::compare(_id->GetName(), "mailbox")) {
        // Check whether it is inside package 'std'
        VeriScope *class_scope = to_elaborate_class->GetScope() ;
        VeriIdDef *container_id = (class_scope) ? class_scope->GetContainingModule(): 0 ;
        VeriModule *container_ptr = container_id ? container_id->GetModule(): 0 ;
        if (container_id && (Strings::compare(container_id->Name(), "std") ||
                 (container_ptr && container_ptr->GetOriginalModuleName() && Strings::compare(container_ptr->GetOriginalModuleName(), "std")))) {
            VeriLibrary *lib = (container_ptr) ? container_ptr->GetLibrary(): 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) {
                params = 0 ;
            }
        }
    }
    FOREACH_ARRAY_ITEM(params, i, param) {
        if (!param) continue ;
        param_expr = (VeriExpression*)param_values.GetValue(param->Name()) ;
        if (!param_expr) {
            param_expr = param->GetInitialValue() ;

            // Try to full evaluate the value for normal parameter and evaluate
            // parameterized part of type expression specific data type
            copied_expr = VeriPseudoTreeNode::GetEvaluatedValue(param, param_expr, parent, this_container_items, this, mod_map_tab) ;
        } else {
            copied_expr = param_expr ;
        }
        // According to SV-2009 LRM, class can have parameter port declaration without
        // default value, but in that case instantiation should provide the param value
        // Otherwise it is an error :
        if (!param_expr) {
            param->Error("parameter %s has no actual or default value", param->Name()) ;
        }
        if (copied_expr) {
            param->SetInitialValue(copied_expr) ;
            if (param->IsType()) {
                VeriDataType *new_type = copied_expr->DataTypeCast() ;
                if (new_type) {
                    // VIPER #4991 : Set data type back pointer to every enum identifier
                    VeriIdDef *id ;
                    unsigned idx ;
                    Array *ids = (new_type->IsEnumType()) ? new_type->GetEnums(): 0 ;
                    FOREACH_ARRAY_ITEM(ids, idx, id) {
                        if (id) id->SetDataType(new_type) ;
                    }
                    param->SetDataType(new_type) ;
                }
            }
        }
    }
    Strings::free(class_name) ;
    if (!to_elaborate_class) return 0 ; // Something wrong some where
    to_elaborate_class->SetClassInstantiated() ; // Mark class as instantiated

    // Now get the container module and its pseudo tree nodes :
    // Get the module item of 'parent' scope
    //VeriScope *mod_scope = (parent) ? parent->ModuleScope(): 0 ;
    //VeriIdDef *parent_owner = (mod_scope) ? mod_scope->GetOwner(): 0 ;
    //VeriModuleItem *container = (parent_owner) ? parent_owner->GetModuleItem() : 0 ;

    // Elaborate the base class :
    VeriName *base_class_name = to_elaborate_class->GetBaseClassName() ;
    VeriMapForCopy id_map ;
    // VIPER #5219 : Base class name can have type reference. While processing that
    // type name should be replaced by its real data type :
    VeriName *new_base_class_name = (base_class_name) ? base_class_name->StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, &id_map, 1 /* replace type*/) : 0 ;
    // If elaborated base class is different, replace old one with new :
    if (new_base_class_name) {
        (void) to_elaborate_class->ReplaceChildNameInternal(base_class_name, new_base_class_name, 1) ;

        // Now pick up base class identifier :
        VeriIdDef *base_class = new_base_class_name->GetId() ;

        VeriScope *derived_class_scope = to_elaborate_class->GetScope() ;
        if (derived_class_scope && base_class && base_class->LocalScope()) {
            // VIPER #4954: Set base class scope of derived class to base class:
            derived_class_scope->SetBaseClassScope(base_class->LocalScope()) ;
        }
        // VIPER #4405 : Body of class can use identifier from base directly. Since
        // we have changed our class, we need to change base class object's reference
        // in 'to_elaborate_class'.
        //ChangeIdRef obj(&id_map) ;
        //to_elaborate_class->Accept(obj) ;
    }

    // Elaborate interface classes :
    Array *interface_classes = to_elaborate_class->GetInterfaceClasses() ;
    VeriName *interface_class_ref, *interface_class_ref_tmp = 0 ;
    FOREACH_ARRAY_ITEM(interface_classes, i, interface_class_ref) {
        VeriName *new_interface_class_name = interface_class_ref ? interface_class_ref->StaticElaborateClassName(instance_scope, container_items, from_decl, mod_map_tab, &id_map, 1 /* replace type*/) : 0 ;
        if (new_interface_class_name) {
            delete interface_class_ref ;
            interface_classes->Insert(i, new_interface_class_name) ;
            interface_class_ref_tmp = new_interface_class_name ;
        }
    }
    // Interface class can be inherited from other interface classes and derived
    // class can use objects defined in base classes. So need to change references
    if (new_base_class_name || (interface_class_ref_tmp && (to_elaborate_class->GetQualifier(VERI_INTERFACE)))) {
        // VIPER #4405 : Body of class can use identifier from base directly. Since
        // we have changed our class, we need to change base class object's reference
        // in 'to_elaborate_class'.
        ChangeIdRef obj(&id_map) ;
        to_elaborate_class->Accept(obj) ;
    }
    unsigned not_resolved_prev = 0 ;
    if (new_base_class_name || !to_elaborate_class->IsResolved()) {
        // VIPER #5248 (test case: Amd_cnb_031808_block_echo_RH30_64).
        // If the base class is a type parameter or a typedef (which is declared but not
        // defined before the derived class) we do not resolve the class items in VeriClass::Resolve()
        // rather we resolve the class items here
        not_resolved_prev = 1 ; // VIPER #7328 : Remember that this class is to be re-resolved
        to_elaborate_class->Resolve(0, VERI_UNDEF_ENV) ;
        // Resolve the method body having a out-of_block decl
        Array *items = to_elaborate_class->GetItems() ;
        unsigned idx ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(items, idx, item) {
            if (!item || item->IsDataDecl()) continue ;
            VeriIdDef *id = item->GetId() ;
            if (!id || !(id->IsFunction() || id->IsTask()) || (id->GetModuleItem() == item)) continue ;
            VeriModuleItem *body = id->GetModuleItem() ;
            if (body) body->Resolve(0, VERI_UNDEF_ENV) ;
        }
    }
    // VIPER #6123 : Also re-resolve all nested class, it may be inside a class with type parameter as base
    if (parent && parent->GetClass()) to_elaborate_class->Resolve(0, VERI_UNDEF_ENV) ;

    // Get the pseudo tree nodes for 'container'
    Set *nodes = (Set*)mod_map_tab->GetValue(container) ;

    // Traverse the body of this class to add its hierarchical identifiers in
    // container module and elaborate other class instances inside it.
    SetIter si ;
    VeriPseudoTreeNode *node ;
    FOREACH_SET_ITEM (nodes, si, &node) {
        if (!node || (node->GetDesign() != container)) continue ;
        node->ProcessClass(to_elaborate_class, mod_map_tab, not_resolved_prev) ;
    }
    return to_elaborate_class->GetId() ;
}
VeriExpression *VeriPseudoTreeNode::GetEvaluatedValue(VeriIdDef *formal, const VeriExpression *actual_or_default, VeriScope *parent, Array *container_items, VeriModuleItem *from_decl, Map *mod_map_tab)
{
    if (!formal || !actual_or_default) return 0 ;
    VeriMapForCopy old2new ;
    VeriExpression *actual = actual_or_default->CopyExpression(old2new) ;
    VeriConstraint *param_constraint = formal->EvaluateConstraintInternal(0, 0, 0) ;
    VeriExpression *new_actual = 0 ;
    if (formal->IsType()) { // formal type parameter
        VeriExpression *tmp = actual->StaticEvaluateToExprInternal(0,0, param_constraint, formal, 0) ;
        VeriDataType *tmp_data_type = (tmp) ? tmp->DataTypeCast(): 0 ;
        VeriDataType *new_data_type = 0 ;
        if (tmp_data_type && tmp_data_type->IsTypeOperator()) {
            VeriExpression *type_op_expr = tmp_data_type->GetArg() ;
            VeriDataType *arg_type = type_op_expr ? type_op_expr->DataTypeCast(): 0 ;
            if (arg_type) {
                new_data_type = arg_type->StaticElaborateClassType(parent, container_items, from_decl, mod_map_tab) ;
            } else {
                new_data_type = type_op_expr ? type_op_expr->ExtractDataType(parent, container_items, from_decl) : 0 ;
            }
        } else if (tmp_data_type) {
            new_data_type = tmp_data_type->StaticElaborateClassType(parent, container_items, from_decl, mod_map_tab) ;
        }
        if (new_data_type) {
            delete tmp ;
            new_actual = new_data_type ;
        } else {
            new_actual = tmp ;
        }
    } else {
        // VIPER #7892 : Consider assignment context rule to evaluate actual
        // and adjust return value according to the type of formal
        // VIPER #8017 : Use overridden value to calculate formal size and sign
        verific_int64 formal_size_sign = formal->TargetSizeSign(0, actual) ;

        //int formal_size_sign = (int)formal->StaticSizeSignInternal(0, 0) ;
        verific_uint64 id_size = GET_CONTEXT_SIZE(formal_size_sign) ; // param-id size
        verific_int64 val_size_sign = actual->StaticSizeSignInternal(0, 0) ;
        verific_uint64 val_size = GET_CONTEXT_SIZE(val_size_sign) ; // init-val size
        unsigned val_sign = GET_CONTEXT_SIGN(val_size_sign) ; // init-val sign
        val_size = MAX(val_size, id_size) ; // Max size of id and value
        val_size_sign = MAKE_CONTEXT_SIZE_SIGN(val_size, val_sign) ; // Evaluation context*/
        // If parameter has no data type initial value is self determined anyway.
        // This is to prevent issues with parameter override having different initial and actul type:
        //VeriDataType *id_type = formal->GetDataType() ;
        //if (!id_type) val_size_sign = 0 ;
        VeriBaseValue *val =  actual->StaticEvaluateInternal((int)val_size_sign, 0, param_constraint, 0) ;
        val = actual->ApplyContextOnValue(val, formal, formal_size_sign, 0) ;
        new_actual = (val) ? val->ToExpression(actual): 0 ;
        delete val ;
    }
    delete param_constraint ;
    delete actual ;
    return new_actual ;
}
VeriIdDef *VeriScope::FindDownwards(const char *name, VeriScope **s, unsigned find_all_children)
{
    // First search in local scope
    VeriIdDef *ret_id = FindLocal(name) ;
    if (ret_id) {
        if (s) *s = (ret_id->LocalScope()) ? ret_id->LocalScope(): this ;
        return ret_id ;
    }
    // Now find in child scopes :
    SetIter si ;
    VeriScope *scope ;
    VeriIdDef *owner_id = 0 ;
    const char *owner_name = 0 ;
    unsigned owner_present = 0 ;
    // First try to find in all child scopes. Search only one level
    FOREACH_SET_ITEM(_children, si, &scope) {
        if (!scope) continue ;
        owner_id = scope->GetOwner() ;
        owner_name = owner_id ? owner_id->GetName(): 0 ;
        owner_present = (owner_id) ? 1: 0 ;
        // Hier name and VeriLoop constructor created iddef has ' '. Here we ignore the VeriLoop
        // created block id. If the first occurred space has no prefix, we can consider it as
        // constructor created block id
        if (owner_name) {
            const char *first_space = strchr(owner_name, ' ') ;
            if (first_space && Strings::compare(first_space, owner_name)) {
                owner_present = 0 ;
            }
        }
        if (owner_present && !find_all_children) continue ;
        ret_id = scope->FindLocal(name) ;
        if (ret_id) {
            if (s) *s = (ret_id->LocalScope()) ? ret_id->LocalScope(): scope ;
            return ret_id ;
        }
    }
    // Now try to find in every child of child:
    FOREACH_SET_ITEM(_children, si, &scope) {
        if (!scope || (scope->GetOwner() && !find_all_children)) continue ;
        ret_id = scope->FindDownwards(name, s, find_all_children) ;
        if (ret_id) return ret_id ;
    }
    return 0 ; // Not found
}
void VeriElabGenerate::CheckDefparamValidity(VeriPseudoTreeNode *mod_node, Map *mod_map_tab, Map *top_mod_nodes, Set *rem_defparams)
{
    MapIter mi ;
    HierIdWithDeclScope *id ;
    VeriScope *decl_scope = 0 ;
    VeriDefParam *copied_version ;
    FOREACH_MAP_ITEM(_defparams, mi, &copied_version, &id) {
        if (!id) continue ;
        VeriExpression *name_ref = id->GetNameRef() ;
        if (!name_ref) continue ;
        id->MarkAsSpecial() ; // Mark this defparam as special
        decl_scope = id->GetDeclarationScope() ; // Get its declaration scope

        // Try to resolve the defparam name
        name_ref->ResolveName(id, mod_node, top_mod_nodes, mod_map_tab, 0) ;
        VeriPseudoTreeNode *last_ele_node = id->GetLastReferencedNode() ;
        if (!last_ele_node) last_ele_node = mod_node ;
        unsigned depth = id->GetDepth() + 1 ;
        Array *resolved_ids = id->TakeResolvedIds() ;
        if (!resolved_ids) resolved_ids = new Array(1) ;
        VeriIdDef *ele_id = 0 ;
        if (depth != resolved_ids->Size()) {
            unsigned lsb_offset_of_1st_not_found_ele = depth - resolved_ids->Size() ;
            unsigned depth_count = 0 ;
            VeriModuleItem *this_mod = (last_ele_node) ? last_ele_node->GetDesign(): (mod_node ? mod_node->GetDesign(): 0) ;
            VeriScope *present_scope = id->GetPrefixScope() ? id->GetPrefixScope() : ((this_mod) ? this_mod->GetScope() : decl_scope) ;
            VeriExpression *prefix = name_ref ;
            Array name_arr(lsb_offset_of_1st_not_found_ele) ;
            while(prefix) {
                depth_count++ ;
                name_arr.InsertLast(prefix) ;
                if (lsb_offset_of_1st_not_found_ele == depth_count) {
                    unsigned i , first = 1, find_all_children = 0 ;
                    FOREACH_ARRAY_ITEM_BACK(&name_arr, i, prefix) {
                        // We have to start finding from this element
                        const char *name = prefix->GetName() ;
                        ele_id = 0 ;
                        VeriScope *match_scope = 0 ;
                        if (first && (lsb_offset_of_1st_not_found_ele == depth)) {
                            ele_id = decl_scope->Find(name) ;
                            find_all_children = 1 ;
                        }
                        if (!ele_id) ele_id = (present_scope) ? present_scope->FindDownwards(name, &match_scope, find_all_children) : 0 ;
                        find_all_children = 0 ;
                        first = 0 ;
                        if (!ele_id) break ;
                        if (match_scope) present_scope = match_scope ;
                        resolved_ids->InsertLast(ele_id) ;
                    }
                    break ;
                }
                prefix = prefix->GetPrefix() ;
            }
        }
        unsigned i ;
        unsigned match = 0 ;
        FOREACH_ARRAY_ITEM_BACK(resolved_ids, i, ele_id) {
            if (ele_id && ((ele_id == decl_scope->FindLocal(ele_id->Name()) ||
                           (ele_id == decl_scope->GetOwner())))) {
                match = 1 ;
                break ;
            }
        }
        //if (!match) name_ref->Error("defparam under generate scope cannot change parameter value outside of hierarchy") ;
        delete resolved_ids ;
        if (match) {
            delete id ;
        } else {
            id->ChangeDefparam(copied_version) ;
            if (rem_defparams) (void) rem_defparams->Insert(id) ;
        }
    }
    if (_defparams) _defparams->Reset() ;
}
void VeriElabGenerate::AddNewId(const VeriIdDef *id)
{
    if (!id) return ;
    if (!_new_ids) _new_ids = new Map(STRING_HASH) ;
    if (id->Name()) {
        (void) _new_ids->Insert(id->Name(), id) ;
    } else {
        // Force insert unnamed instance ids
        (void) _new_ids->Insert(id->Name(), id, 0, 1/* force-insert*/) ;
    }
}
void VeriElabGenerate::ResetNewIds()
{
    if (_new_ids) _new_ids->Reset() ;
}
unsigned VeriElabGenerate::IsCreatedPreviously(const char *name) const
{
    if (!_new_ids) return 0 ;

    // If this name is in new_ids list, it is created already, return 1
    if (_new_ids->GetValue(name)) return 1 ;
    return 0 ;
}

// VIPER #4437 : Use API to create name from block identifier and loop index value
char *VeriElabGenerate::CreateBlockName(const VeriIdDef *block_id, const VeriBaseValue *idx_val) const
{
    char *ret_name = 0 ;
    if (idx_val) {
        char num[10] ;
        num[0] = '\0' ;
        int runner = idx_val->GetIntegerValue() ;
        sprintf(num, "%d", runner) ;
        // Create new loop index with the loop index value for this iteration. This loop index
        // will be used to create the new name of the objects declared within the block within for-generate
        ret_name = Strings::save(block_id ? block_id->Name(): "", "[", num, "]") ;
    } else {
        ret_name = Strings::save(block_id ? block_id->Name(): "") ;
    }
    return ret_name ;
}
// VIPER #7508 : Processing of interface based typedef
VeriDataType *VeriName::StaticElaborateInterfaceBasedType(VeriScope *instance_scope, Map * /*mod_map_tab*/, unsigned is_typedef)
{
    if (!is_typedef) return 0 ;
    // VIPER #7650: Produce error if any identifier other than data type is used
    // in typedef
    VeriIdDef *id = GetId() ;
    if (id && !id->IsType()) {
        Error("%s is an unknown type", id->Name()) ;
        return 0 ;
    }
    // If 'id' is not present, check whether it is interface or module name
    VeriIdDef *container_id = instance_scope ? instance_scope->GetContainingModule(): 0 ;
    VeriModule *container = container_id ? container_id->GetModule(): 0 ;
    VeriLibrary *container_lib = container ? container->GetLibrary(): 0 ;
    VeriModule *mod = veri_file::GetModule(GetName(), 1, container_lib ? container_lib->GetName(): "work") ;
    if (mod) Error("%s is an unknown type", mod->Name()) ;
    return 0 ;
}
VeriDataType *VeriSelectedName::StaticElaborateInterfaceBasedType(VeriScope *instance_scope, Map *mod_map_tab, unsigned is_typedef)
{
    if (!_prefix) return 0 ;
    if (!is_typedef) {
        // Hierarchical name in data declaration as type is not allowed. It is allowed
        // only in type def
        _suffix_id = 0 ;
        Error("hierarchical name in type reference is not allowed") ;
        return 0 ;
    }
    // VIPER #7508 : It must be
    // 1. interface_port.type_defined_inside_interface
    // 2. interface_inst.type_defined_inside_interface

    VeriIdDef *prefix_id = _prefix_id ;
    if (!_prefix_id) prefix_id = instance_scope ? instance_scope->Find(_prefix->GetName()) : 0 ;
    VeriScope *prefix_scope = 0 ;
    if (!prefix_id) { //return 0 ;
        // VIPER #7909 : Produce error if prefix is not declared
        Error("illegal interface based typedef") ;
        return 0 ;
    }
    if (prefix_id->IsInst()) {
        // It can be instance of nested interface
        // Get the pseudo tree node of container to get proper instantiated unit
        // for this instance id
        VeriIdDef *container_id = instance_scope ? instance_scope->GetContainingModule(): 0 ;
        VeriModuleItem *container = container_id ? container_id->GetModuleItem(): 0 ;
        Set *mod_arry = mod_map_tab ? (Set*)mod_map_tab->GetValue(container): 0 ;
        SetIter si ;
        VeriPseudoTreeNode *node, *inst_node = 0 ;
        FOREACH_SET_ITEM(mod_arry, si, &node) {
            if (!node || (node->GetDesign() != container)) continue ;
            VeriIdDef *inst_id = 0 ;
            inst_node = node->CheckMatchWithInstanceName(prefix_id->Name(), &inst_id) ;
            if (inst_id != prefix_id) continue ;
            VeriModuleItem *instantiated_mod = inst_node ? inst_node->GetDesign(): 0 ;
            prefix_scope = instantiated_mod ? instantiated_mod->GetScope(): 0 ;
            if (prefix_scope) break ;
        }
        if (!prefix_scope) {
            VeriModuleItem *instantiated_mod = prefix_id->GetInstantiatedModule() ;
            prefix_scope = instantiated_mod ? instantiated_mod->GetScope(): 0 ;
        }
    } else if (prefix_id->IsInterfacePort()) {
        // Get its data type
        VeriDataType *data_type = prefix_id->GetDataType() ;
        VeriName *interface_name = data_type ? data_type->GetTypeName(): 0 ;
        VeriIdDef *type_id = interface_name ? interface_name->GetId(): 0 ;
        if (!type_id && interface_name) {
            VeriIdDef *container_id = instance_scope ? instance_scope->GetContainingModule(): 0 ;
            VeriModuleItem *container = container_id ? container_id->GetModuleItem(): 0 ;
            Set *mod_arry = mod_map_tab ? (Set*)mod_map_tab->GetValue(container): 0 ;
            SetIter si ;
            VeriPseudoTreeNode *node = 0 ;
            FOREACH_SET_ITEM(mod_arry, si, &node) {
                if (!node || (node->GetDesign() != container)) continue ;
                break ;
            }
            if (node) (void) node->ResolveType(interface_name, &type_id, 1, instance_scope) ;
        }
        prefix_scope = type_id ? type_id->LocalScope(): 0 ;
    } else {
        if (!prefix_id->IsInterface()) Error("illegal interface based typedef") ;
        return 0 ;
    }
    VeriIdDef *suffix_id = prefix_scope ? prefix_scope->FindLocal(_suffix) : 0 ;
    if (!suffix_id) {
        // VIPER #8168: Supported accessing of interface object that is not in
        // modport following IEEE 1800-2012 Section 25.10.
        VeriIdDef *prefix_owner = prefix_scope ? prefix_scope->GetOwner(): 0 ;
        if (prefix_owner && prefix_owner->IsModport()) {
            VeriScope *interface_scope = prefix_scope ? prefix_scope->ModuleScope(): 0 ;
            if (interface_scope) suffix_id = interface_scope->FindLocal(_suffix) ;
            // Interface item that cannot be listed within modport, can
            // be accessed always (IEEE 1800-2012 Section 25.10)
            if (suffix_id && !suffix_id->CanBeAccessedAlways()) suffix_id = 0 ;
        }
    }
    if (!suffix_id) {
        Error("%s is not declared under prefix %s", _suffix, prefix_id->Name()) ;
        return 0 ;
    }
    if (!suffix_id->IsType()) {
        Error("illegal interface based typedef") ;
        return 0 ;
    }
    // Create new type ref
    VeriName *type_name = new VeriIdRef(prefix_id) ;
    type_name->SetLinefile(Linefile()) ;
    type_name = new VeriSelectedName(type_name, Strings::save(suffix_id->Name())) ;
    type_name->SetLinefile(Linefile()) ;
    type_name->SetId(suffix_id) ;
    VeriDataType *ret_type = new VeriTypeRef(type_name, 0, 0) ;
    ret_type->SetLinefile(Linefile()) ;
    return ret_type ;
}

// VIPER #8360 : Returns 1 if this is instantiation created from static elaboration of
// array instance and runtime flag 'veri_add_attribute_to_array_instance_created_instance_id'
// is set
unsigned VeriModuleItem::IsArrayInstanceElaborationCreated() const
{
    if (!IsInstantiation()) return 0 ;
    // Check whether this instance is created from static elaboration of array instance
    // This will work only when runtimeflag veri_add_attribute_to_array_instance_created_instance_id is set
    Array *inst_ids = GetIds() ;

    VeriIdDef *inst_id = (inst_ids && inst_ids->Size()) ? (VeriIdDef*)inst_ids->At(0): 0 ;
    return inst_id ? inst_id->IsArrayInstanceElaborationCreated(): 0 ;
}

// VIPER #8360 : Returns 1 if this is instance identifier created from static elaboration of
// array instance and runtime flag 'veri_add_attribute_to_array_instance_created_instance_id'
// is set
unsigned VeriIdDef::IsArrayInstanceElaborationCreated() const
{
    return GetAttribute(" from_array_inst") ? 1: 0 ;
}

