/*
 *
 * [ File Version : 1.55 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <iostream>
#include <sstream>                     // for ostringstream

#include "VeriExplicitStateMachine.h"  // Make visitor pattern available

#include "Message.h"                   // Make message handlers available
#include "Strings.h"                   // Definitions of all verilog module item tree nodes

#include "veri_file.h"                 // Make verilog reader available
#include "veri_tokens.h"

#include "VeriScope.h"
#include "VeriModule.h"                // Definition of a VeriModule and VeriPrimitive
#include "VeriExpression.h"            // Definitions of all verilog expression tree nodes
#include "VeriId.h"                    // Definitions of all verilog identifier tree nodes
#include "VeriStatement.h"             // Definitions of all verilog statement tree nodes
#include "VeriMisc.h"                  // Definitions of all extraneous verilog tree nodes (ie. range, path, strength, etc...)
#include "VeriConstVal.h"
#include "VeriTreeNode.h"              // VIPER #6922

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//#define EXPLICIT_STATE_MACHINE_DEBUG        // <--- Comment this line out to control flag from build environment, or uncomment to force on.
#define THRESHOLD_FOR_UNROLLING 10            // Threshold for 'for' loop unrolling

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
static
void print_veri_tree_node(void* obj)
{
    VeriTreeNode *node = (VeriTreeNode *)obj ;
    if (node) node->PrettyPrint(cout, 0) ;
}
#endif

/*-----------------------------------------------------------------*/
//      VeriFsmSelectedNameVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VeriFsmSelectedNameVisitor::VeriFsmSelectedNameVisitor():
    VeriVisitor(),
    _old_ids_seqblk_map(0),
    _old2new_ids_map(0)
{
}

VeriFsmSelectedNameVisitor::~VeriFsmSelectedNameVisitor()
{
    _old_ids_seqblk_map = 0 ;
    _old2new_ids_map = 0 ;
}

/*-----------------------------------------------------------------*/
//    Visit routines of VeriFsmSelectedNameVisitor
/*-----------------------------------------------------------------*/

void VeriFsmSelectedNameVisitor::VERI_VISIT(VeriSelectedName, selected_name)
{
    VeriIdDef *selected_id = selected_name.GetId() ;
    MapItem *item = _old_ids_seqblk_map ? _old_ids_seqblk_map->GetItem(selected_id) : 0 ;
    if (item) {
        VeriIdDef *new_id = _old2new_ids_map ? (VeriIdDef*) _old2new_ids_map->GetValue(selected_id) : 0 ;
        unsigned long seq_blk_count = _old_ids_seqblk_map ? (unsigned long) _old_ids_seqblk_map->GetValue(selected_id) : 0 ;
        // We need to delete some of the prefix but not all
        // For example:
        // I1.I2.b2.x
        // where b2 is the name of sequential block and x is a local variable declared within that.
        // We move the local declaration at the new generated always block as \b2.x . So we have to delete the
        // id for b2. So prefix of this node should be selected name I1.I2 and suffix will be \b2.x
        VeriName *new_prefix = selected_name.GetMergedPrefix((unsigned) seq_blk_count, new_id) ;
        selected_name.ModifyPrefix(new_prefix) ;
    }
}

/*-----------------------------------------------------------------*/
//    Routines of VeriFsmSelectedNameVisitor
/*-----------------------------------------------------------------*/

void VeriSelectedName::ModifyPrefix(VeriName *new_prefix)
{
    delete _prefix ;
    _prefix = new_prefix ;
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_suffix) ; // We own this string.
#endif
    _suffix = 0 ;
    _suffix_id = 0 ;
}

VeriName *VeriIdRef::GetMergedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id)
{
    VERIFIC_ASSERT(merge_cnt == 0) ;
    (void) merge_cnt ; // Prevent "unused" compile warning
    VeriName *returned_prefix = new VeriIdRef(prefix_id) ;
    returned_prefix->SetName(Strings::save(prefix_id->Name())) ;
    return returned_prefix ;
}

VeriName *VeriIndexedId::GetMergedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id)
{
    if (!_prefix) return 0 ;
    // Call this routine for its prefix
    return _prefix->GetMergedPrefix(merge_cnt - 1, prefix_id) ;
}

VeriName *VeriIndexedMemoryId::GetMergedPrefix(unsigned merge_cnt, VeriIdDef *prefix_id)
{
    if (!_prefix) return 0 ;
    // Call this routine for its prefix
    return _prefix->GetMergedPrefix(merge_cnt - 1, prefix_id) ;
}

VeriName *VeriSelectedName::GetMergedPrefix(unsigned seq_blk_count, VeriIdDef *prefix_id)
{
    if (!_prefix) return 0 ;
    if (seq_blk_count) return _prefix->GetMergedPrefix(seq_blk_count - 1, prefix_id) ;

    VeriMapForCopy old2new ;
    VeriSelectedName *returned_prefix = new VeriSelectedName(_prefix ? _prefix->CopyName(old2new) : 0, Strings::save(prefix_id ? prefix_id->Name() : 0)) ;
    returned_prefix->SetId(prefix_id) ;
    return returned_prefix ;
}

/*-----------------------------------------------------------------*/
//      VeriFsmAlwaysVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VeriFsmAlwaysVisitor::VeriFsmAlwaysVisitor():
    VeriVisitor(),
    _event_expr_set(STRING_HASH),
    _event_control_set(POINTER_HASH),
    _scope(0),
    _has_always(0),
    _has_initial(0),
    _has_event_control(0),
    _has_forever(0),
    _edged_expr_inserted(0),
    _mult_clk(0),
    _convert(0),
    _first_blk_id(0), // VIPER #6922
    _label_already_set(0), // VIPER #6922
    _is_converted(1), // VIPER #6922
    _always_map(new Map(POINTER_HASH)), // VIPER #6667
    _event_variable_map(new Map(POINTER_HASH)), // VIPER #6667
    _decl_node(0),
    _decl_node_map(new Map(POINTER_HASH)),
    _first_blk_id_map(new Map(POINTER_HASH)),
    _internal_event_node_map(new Map(POINTER_HASH)),
    _event_nodes(new Map(POINTER_HASH)),
    _event_set(new Set(POINTER_HASH)),
    _root_vrs_event_var_map(new Map(POINTER_HASH)),
    _newly_created_arr(new Array()),
    _temp_array(new Array()),
    _is_not_event_or_edged(0),
    _edged_named_event(0),
    _mixed_event(0),
    _has_event_trigger(0),
    _has_nonedged_event_control(0),
    _rearrange(1),
    _clock_followed(1),
    _state_num(0),
    _counter(0)
{
}

VeriFsmAlwaysVisitor::~VeriFsmAlwaysVisitor()
{
    // Delete the string stored in the set
    SetIter si ;
    char *s ;
    FOREACH_SET_ITEM(&_event_expr_set, si, &s) {
        Strings::free(s) ;
    }

    unsigned ai ;
    // delete: new declarations created during cfg generation
    VeriDataDecl *decl ;
    FOREACH_ARRAY_ITEM(_temp_array, ai, decl) {
        if (!decl) continue ;
        Array *decl_ids = decl->GetIds() ;
        if (!decl_ids) continue ;
        Array temp_decl_ids(*decl_ids) ;
        delete decl ;
        unsigned aj ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(&temp_decl_ids, aj, id) delete id ;
    }
    delete _temp_array ;

    // delete newly created statements for repeat/event statement processing:
    VeriTreeNode *tree ;
    FOREACH_ARRAY_ITEM(_newly_created_arr, ai, tree) delete tree ;
    delete _newly_created_arr ;

    if (_event_nodes) _event_nodes->Reset() ;
    delete _event_nodes ;

    if (_internal_event_node_map) _internal_event_node_map->Reset() ;
    delete _internal_event_node_map ;

    if (_first_blk_id_map) _first_blk_id_map->Reset() ;
    delete _first_blk_id_map ;

    // delete event_set
    if (_event_set) _event_set->Reset() ;
    delete _event_set ;

    MapIter mi ;
    // delete root_vrs_event_var_map:
    Set *event_set ;
    FOREACH_MAP_ITEM(_root_vrs_event_var_map, mi, 0, &event_set) {
        if (event_set) event_set->Reset() ;
        delete event_set ;
    }
    delete _root_vrs_event_var_map ;

    // delete decl_node_map
    VerificCFGRootNode *decl_node ;
    FOREACH_MAP_ITEM(_decl_node_map, mi, 0, &decl_node) delete decl_node ;
    if (_decl_node_map) _decl_node_map->Reset() ;
    delete _decl_node_map ;

    // delete newly created event ids and event map
    Array *arr ;
    FOREACH_MAP_ITEM(_event_variable_map, mi, 0, &arr) {
        VeriIdDef *event_id ;
        FOREACH_ARRAY_ITEM(arr, ai, event_id) delete event_id ;
        delete arr ;
    }
    if (_event_variable_map) _event_variable_map->Reset() ;
    delete _event_variable_map ;

    // delete always map
    VerificCFGRootNode *node ;
    FOREACH_MAP_ITEM(_always_map, mi, 0, &node) delete node ;
    if (_always_map) _always_map->Reset() ;
    delete _always_map ;

    _counter = 0 ;
    _rearrange = 1 ;
    _clock_followed = 1 ;
    _has_nonedged_event_control = 0 ;
    _has_event_trigger = 0 ;
    _is_not_event_or_edged = 0 ;
    _edged_named_event = 0 ;
    _mixed_event = 0 ;
    _decl_node = 0 ;
    _is_converted = 1 ; // VIPER #6667
    _label_already_set = 0 ; // VIPER #6922
    _first_blk_id = 0 ; // VIPER #6922
    _scope = 0 ;
}

/*-----------------------------------------------------------------*/
//    TraverseArray routine
/*-----------------------------------------------------------------*/

// Overwrite the VeriVisitor::TraverseArray()
void VeriFsmAlwaysVisitor::TraverseArray(const Array *array)
{
    if (!array) return ;

    // For each module item visit the corresponding visit routine
    // and for any always/initial construct if we able to construct
    // the new module item replace the old one with the new one.
    VeriTreeNode *node_item = 0 ;
    unsigned int i = 0 ;
    FOREACH_ARRAY_ITEM(array, i, node_item) {
        if (node_item) node_item->Accept(*this) ;
    }
}

/*-----------------------------------------------------------------*/
//    Visit routines of VeriFsmAlwaysVisitor
/*-----------------------------------------------------------------*/

// Traverse VeriModule:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriModule, module)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = module.GetScope() ;

    // Traverse module items
    TraverseArray(module.GetModuleItems()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse Generate block:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriGenerateBlock, gen_block)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_block.GetScope() ;
    _counter++ ;

    // Traverse block items
    TraverseArray(gen_block.GetItems()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse Generate constructs:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriGenerateConstruct, gen_construct)
{
    // Traverse generate items
    TraverseArray(gen_construct.GetItems()) ;
}

// Traverse generate conditional statement:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriGenerateConditional, gen_cond)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_cond.GetThenScope() ;
    _counter++ ;

    // Traverse the module item that will be executed when the condition is true
    TraverseNode(gen_cond.GetThenItem()) ;

    _scope = gen_cond.GetElseScope() ;
    _counter++ ;

    // Traverse the module item that will be executed when the condition is fals
    TraverseNode(gen_cond.GetElseItem()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse the generate case item:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriGenerateCaseItem, gen_case_item)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_case_item.GetScope() ;
    _counter++ ;

    // Traverse the module item for the case item
    TraverseNode(gen_case_item.GetItem()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse generate for statement:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriGenerateFor, gen_for)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_for.GetLoopScope() ;
    _counter++ ;

    // Traverse block items
    TraverseArray(gen_for.GetItems()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse always construct
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriAlwaysConstruct, always)
{
    if (always.GetQualifier(VERI_ALWAYS_COMB) || always.GetQualifier(VERI_ALWAYS_LATCH)
        || always.GetQualifier(VERI_ALWAYS_FF) || always.GetQualifier(VERI_FINAL)) {
        return ;
    }

    _has_always = 1 ;

    // Traverse the statement
    TraverseNode(always.GetStmt()) ;

    // VIPER #7881: No need to convert if always construct has
    // single event control and that is at the very beginning.
    if (_event_control_set.Size() <= 1) {
        VeriStatement *stmt = always.GetStmt() ;
        if (stmt && stmt->IsEventControl()) _is_converted = 0 ;
    }

    // Check conditions/limitations, generate the CFG and create a new module item:
    unsigned can_be_converted = ConvertToExplicitStateMachine(&always, always.GetStmt()) ;
    if (!can_be_converted) {
        Reset() ;
        return ;
    }

    // Reset all flags:
    Reset() ;
}

// Traverse initial construct
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriInitialConstruct, initial)
{
    _has_initial = 1 ;

    // Traverse the statement
    TraverseNode(initial.GetStmt()) ;

    // Check conditions/limitations, generate the CFG and create a new module item:
    unsigned can_be_converted = ConvertToExplicitStateMachine(&initial, initial.GetStmt()) ;
    if (!can_be_converted) {
        Reset() ;
        return ;
    }

    // Reset all flags:
    Reset() ;
}

// Traverse event control statement:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriEventControlStatement, event)
{
    _has_event_control = 1 ;

    // Check for use of multiple cloking:
    // For the time being we do not support use of multiple clocking/not edged or event/
    // edged named event/mixture of clcoking and wait event.
    // eg.
    //   multiple clcoking:
    //     (posedge clk, posedge clk1)
    //     (negedge clk, negedge clk1)
    //   not edged or event:
    //     @c // c is not event
    //   edged named event:
    //     (posedge restart) // restart is an event
    //   mixture of clcoking and wait event:
    //     (posedge clk, restart) // restart is event and clcok is not
    if (_mult_clk || _is_not_event_or_edged || _edged_named_event || _mixed_event) return ;

    // Insert all the cloking event nodes in the set: need to count the
    // total number of event statements within a always/initial construct.
    if (_has_always || _has_initial) (void) _event_control_set.Insert(&event) ;

    // Check for multiple cloking:
    // Store the first clocking event in the set
    // if any other clocking event is present then
    // set the flag _mult_clk.
    // The clocking event is stored as a string:
    unsigned is_edged = 1 ;
    unsigned is_event = 0 ;
    Array *expr_arr = event.GetAt() ;
    // Check for mixture of wait and clcoking event
    VeriExpression *event_expr = 0 ;
    if (expr_arr && expr_arr->Size()) {
        event_expr = (VeriExpression *)expr_arr->GetLast() ;
        VeriIdDef *orig_event_id = event_expr ? event_expr->FullId() : 0 ;
        if (orig_event_id && orig_event_id->IsEvent()) is_event = 1 ;
    }

    // Collect original event ids:
    unsigned ai ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(expr_arr, ai, expr) {
        if (!expr) continue ;
        VeriIdDef *orig_event_id = expr->FullId() ;
        if (orig_event_id && orig_event_id->IsEvent()) (void) _event_set->Insert(orig_event_id) ;
    }

    FOREACH_ARRAY_ITEM(expr_arr, ai, expr) {
        if (!expr) continue ;
        if (is_event) { // last expression is a event so all the expressions should be event
            VeriIdDef *orig_event_id = expr->FullId() ;
            if (orig_event_id && !orig_event_id->IsEvent()) {
                _mixed_event = 1 ;
                return ;
            }
        } else { // last expression is a clocking so all the expression should be clocking
            VeriIdDef *orig_event_id = expr->FullId() ;
            if (orig_event_id && orig_event_id->IsEvent()) {
                _mixed_event = 1 ;
                return ;
            }
        }
    }

    // VIPER #7803: Do not support multiple clocking event:
    // As in elaboration we cannot handle multiple clocking issue,
    // we stop creating cfg and explicit m/c and issue an warning.
    // Eg. (posedge clk, posedge clk1)
    Set event_set(POINTER_HASH) ;
    char *str = 0 ;
    FOREACH_ARRAY_ITEM(expr_arr, ai, expr) {
        if (!expr) continue ;
        ostringstream os ; // Dynamically allocated string buffer
        expr->PrettyPrint(os, 0) ;
        string pp_str = os.str() ;
        const char *s = pp_str.c_str() ;
        str = Strings::save(s) ;
        if (!event_set.Size()) {
            (void) event_set.Insert(str) ;
            continue ;
        }
        if (!event_set.GetItem(s)) {
            _mult_clk = 1 ;
            Strings::free(str) ;
            SetIter si ;
            char *ss ;
            FOREACH_SET_ITEM(&event_set, si, &ss) Strings::free(ss) ;
            event_set.Reset() ;
            return ;
        }
    }

    SetIter si ;
    char *s ;
    FOREACH_SET_ITEM(&event_set, si, &s) Strings::free(s) ;
    event_set.Reset() ;

    str = 0 ;
    FOREACH_ARRAY_ITEM(expr_arr, ai, expr) {
        if (!expr) continue ;
        if (!expr->GetEdgeToken()) {
            is_edged = 0 ;
            _has_nonedged_event_control = 1 ;
            VeriIdDef *orig_event_id = expr->FullId() ;
            if (orig_event_id && !orig_event_id->IsEvent()) {
                _is_not_event_or_edged = 1 ;
                Strings::free(str) ;
                return ;
            }
        } else {
            VeriIdDef *orig_event_id = expr->FullId() ;
            if (orig_event_id && orig_event_id->IsEvent()) {
                _edged_named_event = 1 ;
                Strings::free(str) ;
                return ;
            }
        }

        ostringstream os ; // Dynamically allocated string buffer
        expr->PrettyPrint(os, 0) ;
        if (str) {
            char *temp = str ;
            str = Strings::save(str, ",") ;
            Strings::free(temp) ;
        }
        string pp_str = os.str() ;
        const char *ss = pp_str.c_str() ;
        char *temp = str ;
        str = str ? Strings::save(str, ss) : Strings::save(ss) ;
        Strings::free(temp) ;
    }

    if (expr_arr && expr_arr->Size()) { // Collect those expression string that have posedge/negedge clock
        if (!is_edged) {
            if (!_event_expr_set.GetItem(str)) {
                (void) _event_expr_set.Insert(str) ;
            } else {
                Strings::free(str) ;
            }
        } else {
            if (!_edged_expr_inserted) {
                (void) _event_expr_set.Insert(str) ;
                _edged_expr_inserted = 1 ;
            } else {
                if (!_event_expr_set.GetItem(str)) {
                    _mult_clk = 1 ;
                    Strings::free(str) ;
                    return ;
                }
                Strings::free(str) ;
            }
        }
    } else {
        Strings::free(str) ;
    }

    // Get event statement:
    TraverseNode(event.GetStmt()) ;
}

// Traverse event trigger:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriEventTrigger, event)
{
    _has_event_trigger = 1 ;
    TraverseNode(event.GetEventName()) ;

    VeriName *event_name = event.GetEventName() ;
    VeriIdDef *orig_event_id = event_name ? event_name->FullId() : 0 ;
    (void) _event_set->Insert(orig_event_id) ;
}

// Traverse forever statement:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriForever, forever)
{
    _has_forever = 1 ;

    // Traverse the statement
    TraverseNode(forever.GetStmt()) ;
}

// Traverse sequential block:
void VeriFsmAlwaysVisitor::VERI_VISIT(VeriSeqBlock, seq_blk)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = seq_blk.GetScope() ;

    // VIPER #6922: Keep the upper most label id
    if ((_has_always || _has_initial) && !_label_already_set) {
        _first_blk_id = seq_blk.GetLabel() ; // keep the label id
        Array *stmts = seq_blk.GetStatements() ;
        // if the upper seq_blk is unnamed and has more than one sequential block then no need to keep the label id
        if (stmts && (stmts->Size()) > 1) {
            _label_already_set = 1 ;
        } else { // if only one containing statement is not a sequential block then also we do not need to keep tha label id
            VeriStatement *stmt = stmts ? (VeriStatement*)stmts->GetLast() : 0 ;
            if (stmt && !stmt->IsSeqBlock()) _label_already_set = 1 ;
        }
    }

    TraverseArray(seq_blk.GetDeclItems()) ;

    // Traverse the block statements
    TraverseArray(seq_blk.GetStatements()) ;

    // Restore the scope:
    _scope = old_scope ;
}

/*-----------------------------------------------------------------*/
//    Member function definitions
/*-----------------------------------------------------------------*/

Map* VeriFsmAlwaysVisitor::TakeAlwaysMap()
{
    Map *always_map = _always_map ;
    _always_map = 0 ;
    return always_map ;
}

Map* VeriFsmAlwaysVisitor::TakeEventVarMap()
{
    Map *event_variable_map = _event_variable_map ;
    _event_variable_map = 0 ;
    return event_variable_map ;
}

Map* VeriFsmAlwaysVisitor::TakeDeclNodeMap()
{
    Map *decl_node_map = _decl_node_map ;
    _decl_node_map = 0 ;
    return decl_node_map ;
}

Map* VeriFsmAlwaysVisitor::TakeFirstBlockIdMap()
{
    Map *first_blk_id_map = _first_blk_id_map ;
    _first_blk_id_map = 0 ;
    return first_blk_id_map ;
}

Map* VeriFsmAlwaysVisitor::TakeEventNodeMap()
{
    Map *internal_event_node_map = _internal_event_node_map ;
    _internal_event_node_map = 0 ;
    return internal_event_node_map ;
}

Map* VeriFsmAlwaysVisitor::TakeRootVrsEventVarMap()
{
    Map *root_vrs_event_var_map = _root_vrs_event_var_map ;
    _root_vrs_event_var_map = 0 ;
    return root_vrs_event_var_map ;
}

// Check for transperant node: statement node corresponding to sequential block/scope end node
unsigned VeriFsmAlwaysVisitor::IsTransperentNode(const VerificCFGRootNode *node) const
{
    if (!node) return 0 ;

    if (node->IsScopeEndNode()) return 1 ; // scope end node is a transperant node

    // statement node with zero statement list corresponding to sequential block is a transperant node
    if (node->IsSimpleStatementNode() && !node->IsStateNode()) { // state node is also a simple statement node
        Map *stmt_list = node->GetStatementList() ;
        // if the statement node is dummy statement node sequential block,
        // then it would not contain any statement, otherwise it will contain statemnts
        if (!stmt_list || !stmt_list->Size()) return 1 ;
    }

    return 0 ;
}

// node corresponding to clocking event not event wait
unsigned VeriFsmAlwaysVisitor::IsClockedEventNode(const VerificCFGRootNode *node) const
{
    if (!node) return 0 ;

    Array *event_arr = node->GetEventExprList() ;
    if (event_arr) {
        VeriExpression *event_expr = (VeriExpression*)event_arr->GetLast() ;
        VeriIdDef *event_id = event_expr ? event_expr->FullId() : 0 ;
        if (event_id && event_id->IsEvent()) return 0 ; // event wat
        return 1 ;
    }

    return 0 ;
}

// Create unique name for variables:
// If find the variable declaration in the present scope or in the  upper scopes,
// create a new name and return the new name:
char*
VeriFsmAlwaysVisitor::CreateVariableName(const char *prefix, const Array *label_arr, VeriScope *present_scope)
{
    char *name = Strings::save(prefix) ;
    unsigned ai ;
    VeriIdDef *label_id ;
    FOREACH_ARRAY_ITEM(label_arr, ai, label_id) {
        const char *label = label_id ? label_id->GetName() : 0 ;
        if (!label) continue ;
        char *temp_name = name ;
        name = Strings::save(label, ".", name) ;
        Strings::free(temp_name) ;
    }

    char *full_name = Strings::save(name) ;
    VeriScope *scope = present_scope ;
    unsigned number = 0 ;
    while(1)
    {
        if (scope && !scope->IsEmptyScope()) {
            if (scope->FindLocal(name)) { // find in local scope
                char *num = Strings::itoa((int)number) ;
                Strings::free(name) ;
                name = Strings::save(full_name, "_", num) ;
                number++ ;
                Strings::free(num) ;
            } else {
                Strings::free(full_name) ;
                return name ;
            }
        } else { // find in upper scope
            scope = scope ? scope->Upper() : 0 ;
        }
    }

    return 0 ;
}

// VIPER #7338: Get the size-sign of expression:
int
VeriFsmAlwaysVisitor::GetExprSize(const VeriExpression *expr)
{
    if (!expr) return 0 ;

    int size = 0 ;
    if (expr->IsInt()) {
        int num = expr->Integer() ;
        size = (num >= 0) ? 32 : -32 ;
        return size ;
    }
    size = (int) expr->StaticSizeSignInternal(0, 0) ;
    if (size) return size ;
    return -32 ;
}

// The cfg created from the rtl at first does not create graph for events(wait/trigger).
// In the first pass the cfg contain state node for event wait and statement node for event trigger.
// In the following routine we take the cfg created in the first pass and do the following.
// 1) Traverse the cfg from root.
// 2) For all its children check the following:
//        1. When ever we get a state node corresponding to event wait we find for a clock.
//               - If we do not find any clock, error out here.
//               - If a clock is found, then for all the parents of that state node corresponding
//                 to the clock push the copy of that state node.
//        2. If we error out for any state node(for event wait) we do not continue traversing the cfg.

// eg. the cfg craeted in the first pass is:
//     conditional_node    (/*if (cond)*/
//     state_node1         (/*@start*/)
//     scope_end_node
//     conditional_node    (/*else*/)
//     statement_node          (/*done = 1*/)
//     scope_end_node
//     state_node2         (/* @(posedge clk)*/)

// After rearrange, the cfg will be:
//     conditiona_ node    (/*if (cond)*/
//     state_node1         (/*@start*/)
//     state_node3             (/* @(posedge clk)*/) // copy of state node2 of the first cfg
//     scope_end_node
//     conditional_node    (/*else*/)
//     statement_node          (/*done = 1*/)
//     state_node4             (/* @(posedge clk)*/) // copy of state node2 of the first cfg
//     scope_end_node
unsigned
VeriFsmAlwaysVisitor::ReArrangeCfgForEventWait(VerificCFGRootNode *node, Set *visited)
{
    if (!node) return 0 ;

    if (!_rearrange) return 0 ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && !visited->Insert(node)) return 0 ;

    if (node->IsStateNode()) { // state node
        Array *event_arr = node->GetEventExprList() ;
        if (event_arr) {
            VeriExpression *event_expr = (VeriExpression*)event_arr->GetLast() ;
            VeriIdDef *event_id = event_expr ? event_expr->FullId() : 0 ;
            if (event_id && event_id->IsEvent()) { // state node corresponding to event wait: check for a clocking state node
                Set *children = node->GetChildren() ;
                SetIter si ;
                VerificCFGRootNode *child ;
                FOREACH_SET_ITEM(children, si, &child) {
                    if (!child) continue ;
                    Set new_visited(POINTER_HASH) ;
                    _rearrange = ClockFollowed(child, &new_visited) ; // If contain any error for any child, do not continue traversing with the other children
                    if (!_rearrange) return 0 ;
                }
            }
        }
    }

    // Traverse with all the children
    Array *children = new Array(2) ;
    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
        if (!child) continue ;
        children->InsertLast(child) ;
    }

    unsigned ai ;
    FOREACH_ARRAY_ITEM(children, ai, child) {
        if (!child) continue ;
        (void) ReArrangeCfgForEventWait(child, visited) ;
    }
    delete children ;

    return (!_rearrange) ? 0 : 1 ;
}

// The following routine check for a clocking node after a state node(for event wait).
//    1. If we get a node other than a scope end node or a clocking state node we error out.
//    2. If the node is a scope end node we continue traversing for its children for a
//       clocking state node.
//    3. If we get a clocking state node we go upwards of the cfg from this state node.
//       For all its parents we push copy of this state node.
unsigned
VeriFsmAlwaysVisitor::ClockFollowed(VerificCFGRootNode *node, Set *visited)
{
    if (!node) return 0 ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && !visited->Insert(node)) return 0 ;

    if (!_clock_followed) return 0 ; // do not contain clocking node after enevt wait

    if (node->IsStateNode()) { // Contains state node
        if (!IsClockedEventNode(node)) {
            _clock_followed = 0 ;
            return 0 ;
        }
        // contains a clocking state node
        Set *parents = node->GetParents() ;
        SetIter si ;
        VerificCFGRootNode *parent ;
        FOREACH_SET_ITEM(parents, si, &parent) {
            if (!parent) continue ;
            Set new_visited(POINTER_HASH) ;
            (void) PushNode(parent, node, node, &new_visited) ; // push copy of state node
        }
        (void) node->DeleteNode() ; // After pushing the copy of state node, delete the node
        return 1 ;
    }

    if (IsTransperentNode(node)) {
        // Continue for clock checking with the children of the node
        Array *children = new Array(2) ;
        SetIter si ;
        VerificCFGRootNode *child ;
        FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
            if (!child) continue ;
            children->InsertLast(child) ;
        }

        unsigned ai ;
        FOREACH_ARRAY_ITEM(children, ai, child) {
            if (!child) continue ;
            (void) ClockFollowed(child, visited) ;
        }
        delete children ;
    } else {
        // Error out for all other nodes
        _clock_followed = 0 ;
        return 0 ;
    }

    return (!_clock_followed) ? 0 : 1 ;
}

// This routine traverse the cgf upwards from a given state node, and push copy of this state node before all the
// parents of this state node. We traverse upwars till we get scope end nodes.
unsigned
VeriFsmAlwaysVisitor::PushNode(VerificCFGRootNode *p_node, VerificCFGRootNode *c_node, VerificCFGRootNode *state_node, Set *visited)
{
    if (!p_node || !c_node || !state_node) return 0 ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && !visited->Insert(p_node)) return 0 ;

    unsigned top_node = 0 ;
    if (p_node->IsSimpleStatementNode() && !p_node->IsStateNode()) { // simple statement node: check for transferent node
        if (!p_node->GetParents()) { // We always create a dummy statement node with no stmt_list as the top node of the cfg,
            top_node = 1 ;           // do not push copy of state node after the top node.
        }
    }

    if (IsTransperentNode(p_node) && !top_node) {
        Set *parents = p_node->GetParents() ;
        SetIter si ;
        VerificCFGRootNode *parent ;
        FOREACH_SET_ITEM(parents, si, &parent) {
            if (!parent) continue ;
            (void) PushNode(parent, p_node, state_node, visited) ; // traverse upwards: push node
        }
    } else {
        // Insert node
        VerificCFGStateNode *new_node = new VerificCFGStateNode() ;
        new_node->SetLinefile(state_node->GetLinefile()) ;
        new_node->SetState(_state_num++) ;
        Array *event_arr = state_node->GetEventExprList() ;
        new_node->AddEventExprList(event_arr) ;
        (void) p_node->InsertNodeBetween(c_node, new_node) ;
    }
    return 1 ;
}

// Again trverse the cfg created after rearranging it and replace the state node corresponding
// to event wait with a subgraph created for it.
unsigned VeriFsmAlwaysVisitor::ReplaceEventNode(VerificCFGRootNode *node, Set *visited)
{
    if (!node) return 0 ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && !visited->Insert(node)) return 0 ;

    VerificCFGRootNode *new_node = 0 ;
    if (node->IsStateNode()) { // state node
        // state node corresponding to event wait: create subgraph
        if (!IsClockedEventNode(node)) new_node = CreateEventSubGraph(node) ;
    }
    if (new_node) node = new_node ;

    // Traverse for its children:
    Array *children = new Array(2) ;
    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
        if (!child) continue ;
        children->InsertLast(child) ;
    }

    unsigned ai ;
    FOREACH_ARRAY_ITEM(children, ai, child) {
        if (!child) continue ;
        (void) ReplaceEventNode(child, visited) ;
    }
    delete children ;

    return 1 ;
}

// Create subgraph for state node corresponding to event wait and replace it.
VerificCFGRootNode*
VeriFsmAlwaysVisitor::CreateEventSubGraph(VerificCFGRootNode *node)
{
    if (!node) return 0 ;

    Array *state_expr_list = 0 ;
    Array state_nodes(2) ;
    Set *children = node->GetChildren() ;
    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(children, si, &child) {
        if (!child) continue ;
        if (child->IsStateNode()) {
            state_expr_list = child->GetEventExprList() ;
            state_nodes.InsertLast(child) ;
        }
    }
    // Delete the state nodes followed by event wait.
    unsigned ai ;
    FOREACH_ARRAY_ITEM(&state_nodes, ai, child) (void) child->DeleteNode() ;

    // For wait statement followed by a clocking statement, create a conditional statemnet
    // followed by a while loop:
    // eg. @restart
    //     @(posedge clk)
    // we replace it by:
    //
    // if ((restart != restart_old)) begin
    //     state_var <=  1 ;
    // end
    // else begin
    //     if ((restart == restart_0)) begin
    //         state_var <=  2 ;
    //     end
    //     else begin
    //         restart_0 <=  restart ;
    //         state_var <=  0 ;
    //     end
    // end
    // Create a state node:
    VerificCFGRootNode *current_node = 0 ;
    VerificCFGRootNode *parent_node = 0 ;

    // Create a conditional node
    VerificCFGConditionalStatementNode *cond1 = new VerificCFGConditionalStatementNode() ;

    // Set the parent and current node:
    parent_node = cond1 ;
    current_node = cond1 ;

    VERIFIC_ASSERT(cond1 == current_node) ;

    // scope end node:
    VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond1) ;
    (void) cond1->SetScopeEnd(cond_end) ;

    Array temp_event_arr ;
    Array *event_expr_list = node->GetEventExprList() ;
    VeriIdDef *orig_event_id = 0 ;
    VeriExpression *event_expr ;
    const char *event_name = 0 ;
    FOREACH_ARRAY_ITEM(event_expr_list, ai, event_expr) {
        if (!event_expr) continue ;
        event_name = event_expr->GetName() ;
        orig_event_id = event_expr->FullId() ;

        // Event variable declaration:
        MapItem *map_item = _event_variable_map ? _event_variable_map->GetItem(orig_event_id) : 0 ;
        if (map_item) { // event variable already inserted in _event_variable_map,
            // so get the proper event id from the map.
            Array *arr = _event_variable_map ? (Array*)_event_variable_map->GetValue(orig_event_id) : 0 ;
            unsigned i ;
            VeriIdDef *event_id ;
            FOREACH_ARRAY_ITEM(arr, i, event_id) {
                if (!event_id) continue ;
                VeriIdRef *event_var = new VeriIdRef(event_id) ;
                event_var->SetLinefile(event_id->Linefile()) ;
                temp_event_arr.InsertLast(event_var) ;
            }
        } else { // create new event id
            Array *event_arr = new Array(2) ;
            char *counter_num = Strings::itoa(_counter) ;
            char *temp = counter_num ;
            char *temp_name = Strings::save("_", counter_num) ;
            Strings::free(temp) ;
            temp = temp_name ;
            char *name = Strings::save(event_name, temp_name) ;
            Strings::free(temp) ;
            temp = name ;
            char *prefix = VeriFsmAlwaysVisitor::CreateVariableName(name, 0, _scope) ;
            Strings::free(temp) ;
            VeriIdDef *event_id = new VeriVariable(prefix) ;
            event_id->SetLinefile(orig_event_id ? orig_event_id->Linefile() : event_expr->Linefile()) ;
            event_arr->InsertLast(event_id) ;

            VeriIdRef *event_var1 = new VeriIdRef(event_id) ;
            event_var1->SetLinefile(event_id->Linefile()) ;
            temp_event_arr.InsertLast(event_var1) ;

            name = Strings::save(prefix, "_old") ;
            temp = name ;
            prefix = VeriFsmAlwaysVisitor::CreateVariableName(name, 0, _scope) ;
            Strings::free(temp) ;
            event_id = new VeriVariable(prefix) ;
            event_id->SetLinefile(orig_event_id ? orig_event_id->Linefile() : event_expr->Linefile()) ;
            event_arr->InsertLast(event_id) ;

            VeriIdRef *event_var2 = new VeriIdRef(event_id) ;
            event_var2->SetLinefile(event_id->Linefile()) ;
            temp_event_arr.InsertLast(event_var2) ;
            if (_event_variable_map) (void) _event_variable_map->Insert(orig_event_id, event_arr) ;
        }
    }

    // Create and store condition:
    VeriMapForCopy old_2_new(POINTER_HASH) ;
    Array cond_expr_arr ;
    for(ai=0 ; ai < temp_event_arr.Size(); ai++) {
        VeriExpression *event_var1 = (VeriIdRef *)temp_event_arr.At(ai++) ;
        VeriExpression *event_var2 = (VeriIdRef *)temp_event_arr.At(ai) ;

        // Create conditional expression
        VeriExpression *cond_expr = new VeriBinaryOperator(VERI_LOGNEQ, event_var1, event_var2) ;
        cond_expr->SetLinefile(event_var1 ? event_var1->Linefile() : 0) ;
        cond_expr_arr.InsertLast(cond_expr) ;
    }

    while(cond_expr_arr.Size() > 1) {
        VeriExpression *left = (VeriExpression *) cond_expr_arr.RemoveLast() ;
        VeriExpression *right = (VeriExpression *) cond_expr_arr.RemoveLast() ;
        VeriExpression *new_expr = new VeriBinaryOperator(VERI_LOGOR, left, right) ;
        new_expr->SetLinefile(left ? left->Linefile() : 0) ;
        cond_expr_arr.InsertLast(new_expr) ;
    }

    VeriExpression *cond_expr1 = cond_expr_arr.Size() ? (VeriExpression *) cond_expr_arr.RemoveLast() : 0 ;
    cond1->SetConditionExpression((void*)cond_expr1) ;
    if (_newly_created_arr) _newly_created_arr->InsertLast(cond_expr1) ;

    // Create a internal state node:
    VerificCFGStateNode *internal_state_node = new VerificCFGStateNode() ;
    internal_state_node->SetLinefile(node->GetLinefile()) ;
    internal_state_node->SetState(_state_num++) ;
    internal_state_node->AddEventExprList(state_expr_list) ;

    // Keep the parent node:
    VerificCFGRootNode *orig_parent_node = parent_node ;

    // For each branch of conditional node create a fresh parse tree
    // keeping the _parent_node and _current_node in stack and make
    // each of them null before going to process then and else part:
    current_node = 0 ;
    parent_node = 0 ;

    // Traverse then statement
    parent_node = internal_state_node ;
    current_node = internal_state_node ;

    (void) cond1->AddBranch(0, parent_node) ;
    (void) cond_end->AddParent(current_node) ;

    // no else part:
    (void) cond_end->AddParent(cond1) ;
    current_node = cond_end ;

    // Create another conditional node
    VerificCFGConditionalStatementNode *cond2 = new VerificCFGConditionalStatementNode() ;

    // Add this conditional node in event map: required for remove redundant codes
    // The condition of this conditional node and its next conditional node are mutually
    // exclusive: so if we go to the true branch of the first conditional node then we must
    // go to the false path of the 2nd conditional node, and vise versa.
    (void)_event_nodes->Insert(cond1, cond2) ;
    (void)_event_nodes->Insert(cond2, cond1) ;

    (void) cond2->AddParent(current_node) ;
    current_node = cond2 ;

    // Set the parent node if there is no parent node:
    parent_node = current_node ;

    VERIFIC_ASSERT(cond2 == current_node) ;

    // scope end node:
    cond_end = new VerificCFGScopeEndNode(cond2) ;
    (void) cond2->SetScopeEnd(cond_end) ;

    for(ai=0 ; ai < temp_event_arr.Size(); ai++) {
        VeriExpression *event_var1 = (VeriIdRef *)temp_event_arr.At(ai++) ;
        VeriExpression *event_var2 = (VeriIdRef *)temp_event_arr.At(ai) ;
        if (event_var1 && event_var2) { // JJ: added 'if' to satisty Coverty 130118
            // Create conditional expression
            VeriExpression *cond_expr = new VeriBinaryOperator(VERI_LOGEQ, event_var1->CopyExpression(old_2_new), event_var2->CopyExpression(old_2_new)) ;
            cond_expr->SetLinefile(event_var1->Linefile()) ;
            cond_expr_arr.InsertLast(cond_expr) ;
        }
    }

    while(cond_expr_arr.Size() > 1) {
        VeriExpression *left = (VeriExpression *) cond_expr_arr.RemoveLast() ;
        VeriExpression *right = (VeriExpression *) cond_expr_arr.RemoveLast() ;
        VeriExpression *new_expr = new VeriBinaryOperator(VERI_LOGOR, left, right) ;
        new_expr->SetLinefile(left ? left->Linefile() : 0) ;
        cond_expr_arr.InsertLast(new_expr) ;
    }

    VeriExpression *cond_expr2 = cond_expr_arr.Size() ? (VeriExpression *) cond_expr_arr.RemoveLast() : 0 ;
    cond2->SetConditionExpression((void*)cond_expr2) ;
    if (_newly_created_arr) _newly_created_arr->InsertLast(cond_expr2) ;

    // if part:
    current_node = 0 ;
    parent_node = 0 ;
    // Create a state node:
    VerificCFGStateNode *state = new VerificCFGStateNode() ;
    state->SetLinefile(node->GetLinefile()) ;
    current_node = state ;

    // Set the parent node if there is no parent node:
    parent_node = current_node ;

    // Get the state, increase that by one and
    // set the new state to the _current_node:
    state->SetState(_state_num++) ;
    state->AddEventExprList(state_expr_list) ;
    (void) cond2->AddBranch(0, parent_node) ;
    (void) cond2->AddParent(current_node) ;

    // else part
    current_node = 0 ;
    parent_node = 0 ;
    (void) cond2->AddBranch(0, cond_end) ;
    current_node = cond_end ;

    // Restore the original parent node:
    parent_node = orig_parent_node ;

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
    // Print the control flow diagram:
    printf ("----------subgraph rearrange----------------------------\n") ;
    parent_node->PrintNode(&print_veri_tree_node, 0) ;
    printf ("--------------------------------------\n") ;
#endif

    // Replace the wait node by the newly created subgraph:
    (void) node->ReplaceNode(parent_node, current_node) ; // replace the node with the subgraph

    return parent_node ;
}

// Again traverse the cfg and replace the event trigger statement with a conditional statement.
// Also create a statement node for restoring the event value and push that node after the
// nearest clocking state node.
unsigned
VeriFsmAlwaysVisitor::ReArrangeCfgForEventTrigger(VerificCFGRootNode *node, Set *visited)
{
    if (!node) return 0 ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && !visited->Insert(node)) return 0 ;

    if (node->IsSimpleStatementNode() && !node->IsStateNode()) {
        Map *stmt_list = node->GetStatementList() ;
        if (stmt_list) {
            unsigned has_event_trigger = 0 ;
            unsigned pos = 0 ;
            MapIter mi ;
            VeriStatement *stmt ;
            FOREACH_MAP_ITEM(stmt_list, mi, &stmt, 0) {
                pos++ ;
                if (!stmt || !stmt->IsEventTrigger()) continue ;
                has_event_trigger = 1 ;
                break ;
            }
            if (pos > 0) pos = pos - 1 ;

            if (has_event_trigger) {
                // For event triggering we craete a conditional statement
                // eg. ->restar
                // we replae it by:
                // if ((restart == restart_0))
                //    restart <=  (~restart) ;
                // Event variable declaration:
                MapItem *item = stmt_list->GetItemAt(pos) ;
                VeriEventTrigger *event_trigger = item ? (VeriEventTrigger*)item->Key() : 0 ;
                VeriScope *stmt_scope = item ? (VeriScope *) item->Value() : 0 ;

                VeriName *event_name = 0 ;
                if (event_trigger) event_name = event_trigger->GetEventName() ;
                VeriIdDef *orig_event_id = event_name ? event_name->FullId() : 0 ;

                Array temp_event_arr ;
                // Event variable declaration:
                MapItem *map_item = _event_variable_map ? _event_variable_map->GetItem(orig_event_id) : 0 ;
                if (map_item) { // event variable already inserted in _event_variable_map,
                    // so get the proper event id from the map.
                    Array *arr = _event_variable_map ? (Array*)_event_variable_map->GetValue(orig_event_id) : 0 ;
                    unsigned ai ;
                    VeriIdDef *event_id ;
                    FOREACH_ARRAY_ITEM(arr, ai, event_id) {
                        if (!event_id) continue ;
                        VeriIdRef *event_var = new VeriIdRef(event_id) ;
                        event_var->SetLinefile(event_id->Linefile()) ;
                        temp_event_arr.InsertLast(event_var) ;
                    }
                } else { // create new event id
                    Array *event_arr = new Array(2) ;
                    char *counter_num = Strings::itoa(_counter) ;
                    char *temp = counter_num ;
                    char *temp_name = Strings::save("_", counter_num) ;
                    Strings::free(temp) ;
                    temp = temp_name ;
                    char *name = event_name ? Strings::save(event_name->GetName(), temp_name) : 0 ;
                    Strings::free(temp) ;
                    temp = name ;
                    char *prefix = VeriFsmAlwaysVisitor::CreateVariableName(name, 0, _scope) ;
                    Strings::free(temp) ;
                    VeriIdDef *event_id = new VeriVariable(prefix) ;
                    event_id->SetLinefile(orig_event_id ? orig_event_id->Linefile() : 0) ;
                    event_arr->InsertLast(event_id) ;

                    VeriIdRef *event_var1 = new VeriIdRef(event_id) ;
                    event_var1->SetLinefile(event_id->Linefile()) ;
                    temp_event_arr.InsertLast(event_var1) ;

                    name = Strings::save(prefix, "_old") ;
                    temp = name ;
                    prefix = VeriFsmAlwaysVisitor::CreateVariableName(name, 0, _scope) ;
                    Strings::free(temp) ;
                    event_id = new VeriVariable(prefix) ;
                    event_id->SetLinefile(orig_event_id ? orig_event_id->Linefile() : 0) ;
                    event_arr->InsertLast(event_id) ;

                    VeriIdRef *event_var2 = new VeriIdRef(event_id) ;
                    event_var2->SetLinefile(event_id->Linefile()) ;
                    temp_event_arr.InsertLast(event_var2) ;
                    if (_event_variable_map) (void) _event_variable_map->Insert(orig_event_id, event_arr) ;
                }

                // Create and store condition:
                VeriMapForCopy old_2_new(POINTER_HASH) ;
                // Get expressions from last
                VeriIdRef *event_var1 = temp_event_arr.Size() ? (VeriIdRef *)temp_event_arr.RemoveLast() : 0 ;
                VeriIdRef *event_var2 = temp_event_arr.Size() ? (VeriIdRef *)temp_event_arr.RemoveLast() : 0 ;

                // Create and store condition:
                VeriExpression *if_expr = new VeriBinaryOperator(VERI_LOGEQ, event_var2, event_var1) ;
                if_expr->SetLinefile(event_var1 ? event_var1->Linefile() : 0) ;

                // Create new statement and store to statement
                VeriExpression *right_event_expr = new VeriUnaryOperator(VERI_REDNOT, (event_var2 ? event_var2->CopyExpression(old_2_new) : 0)) ;
                VeriStatement *if_stmt = new VeriNonBlockingAssign((event_var2 ? event_var2->CopyExpression(old_2_new) : 0), 0, right_event_expr) ;
                if_stmt->SetLinefile(event_var1 ? event_var1->Linefile() : 0) ;

                VeriConditionalStatement *conditional_stmt = new VeriConditionalStatement(if_expr, if_stmt, 0) ;
                conditional_stmt->SetLinefile(if_expr->Linefile()) ;
                if (_newly_created_arr) _newly_created_arr->InsertLast(conditional_stmt) ;

                (void) stmt_list->Remove(item) ;
                (void) stmt_list->Insert(conditional_stmt, stmt_scope) ;

                stmt = new VeriNonBlockingAssign((event_var1 ? event_var1->CopyExpression(old_2_new) : 0), 0, (event_var2 ? event_var2->CopyExpression(old_2_new) : 0)) ;
                stmt->SetLinefile(event_var1 ? event_var1->Linefile() : 0) ;
                VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;

                (void) statement->AddStatement(stmt, stmt_scope) ;
                if (_newly_created_arr) _newly_created_arr->InsertLast(stmt) ;

                Map mutually_exclusive_event_map(POINTER_HASH) ;
                Set new_visited(POINTER_HASH) ;
                (void) InsertNodeAfterNearestStateNode(node, statement, mutually_exclusive_event_map, &new_visited) ;
                mutually_exclusive_event_map.Reset() ;
                delete statement ;
            }
        }
    }

    // Traverse all its children
    Array *children = new Array(2) ;
    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
        if (!child) continue ;
        children->InsertLast(child) ;
    }

    unsigned ai ;
    FOREACH_ARRAY_ITEM(children, ai, child) {
        if (!child) continue ;
        (void) ReArrangeCfgForEventTrigger(child, visited) ;
    }
    delete children ;

    return 1 ;
}

// Insert the statement node after the nearest clocking state node which is
// followed by the event triggering.
// eg. ->start
//     @(posedgr clk)
// It will be replcaed with
//     conditional_node    /*if (start_old == start) start = ~start*/
//     state_node
//     statement_node      /* start_old <= start */
unsigned
VeriFsmAlwaysVisitor::InsertNodeAfterNearestStateNode(VerificCFGRootNode *node, VerificCFGRootNode *statement_node, Map &mutually_exclusive_event_map, Set *visited)
{
    if (!node || !statement_node) return 0 ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && !visited->Insert(node)) return 0 ;

    if (node->IsStateNode()) { // state node
        VerificCFGStatementNode *new_statement_node = new VerificCFGStatementNode() ;
        Map *stmt_list = statement_node->GetStatementList() ;
        MapIter mi ;
        VeriStatement *stmt ;
        VeriScope *scope ;
        FOREACH_MAP_ITEM(stmt_list, mi, &stmt, &scope) {
            if (!stmt) continue ;
            (void) new_statement_node->AddStatement(stmt, scope) ;
        }

        (void) node->InsertNode(new_statement_node) ; // Insert node
        return 1 ;
    } else {
        if (node->IsConditionalNode()) {
            VeriExpression *cond_expr = (VeriExpression *)node->GetConditionExpression() ;

            unsigned mutually_exclusive = 0 ;
            unsigned i ;
            unsigned branch_cond_size = node->NumBranch() ;
            // For case statement the branch size is one or more
            // for loop/if-else statement the branch size is zero
            if (node->NumBranch()) {
                // For default case the branch enry is zero, so the branch
                // statement size is one more than branch condition size
                for (i=0; i< branch_cond_size+1; i++) {
                    VerificCFGRootNode *statement = (VerificCFGRootNode *)node->GetBranchStatement(i) ;
                    if(statement) {
                        (void) InsertNodeAfterNearestStateNode(statement, statement_node, mutually_exclusive_event_map, visited) ;
                    }
                }
            } else {
                // For if-else/loop:
                VerificCFGRootNode *statement = (VerificCFGRootNode *)node->GetBranchStatement(0) ; // if branch
                unsigned branch = 0 ;
                // If we have 2 conditional node with mutually exclusive conditions, then if the true branch of
                // of first condition is executed then the false branch of the other condition will be executed
                // and vice versa. For this we maintain a map that will contain the other mutually conditional
                // node and its branch through which we will not traverse.
                Map new_mutually_exclusive_event_map(POINTER_HASH) ;
                if (!mutually_exclusive_event_map.Size()) {
                    if (_event_nodes && _event_nodes->GetItem(node)) {
                        VerificCFGRootNode *other_node = (VerificCFGRootNode*)_event_nodes->GetValue(node) ;
                        (void) new_mutually_exclusive_event_map.Insert(other_node, (void*) (unsigned long)1) ;
                    }
                } else {
                    if (mutually_exclusive_event_map.GetItem(node)) {
                        branch = (unsigned) ((unsigned long)mutually_exclusive_event_map.GetValue(node)) ;
                    } else {
                        MapIter mi ;
                        VerificCFGRootNode *node1, *node2 ;
                        FOREACH_MAP_ITEM(&mutually_exclusive_event_map, mi, &node1, &node2) {
                            if (!node1 || !node2) continue ;
                            (void) new_mutually_exclusive_event_map.Insert(node1, node2) ;
                        }
                    }
                }

                if (branch == 1) {
                    statement = 0 ;
                    mutually_exclusive = 1 ;
                }
                if (statement) (void) InsertNodeAfterNearestStateNode(statement, statement_node, new_mutually_exclusive_event_map, visited) ;

                if (cond_expr) {
                    statement = (VerificCFGRootNode *)node->GetBranchStatement(1) ; // else branch
                    branch = 0 ;
                    new_mutually_exclusive_event_map.Reset() ;
                    if (!mutually_exclusive_event_map.Size()) {
                        if (_event_nodes && _event_nodes->GetItem(node)) {
                            VerificCFGRootNode *other_node = (VerificCFGRootNode*)_event_nodes->GetValue(node) ;
                            (void) new_mutually_exclusive_event_map.Insert(other_node, (void*) (unsigned long)2) ;
                        }
                    } else {
                        if (mutually_exclusive_event_map.GetItem(node)) {
                            branch = (unsigned) ((unsigned long)mutually_exclusive_event_map.GetValue(node)) ;
                        } else {
                            MapIter mi ;
                            VerificCFGRootNode *node1, *node2 ;
                            FOREACH_MAP_ITEM(&mutually_exclusive_event_map, mi, &node1, &node2) {
                                if (!node1 || !node2) continue ;
                                (void) new_mutually_exclusive_event_map.Insert(node1, node2) ;
                            }
                        }
                    }

                    if (branch == 2) {
                        statement = 0 ;
                        mutually_exclusive = 1 ;
                    }

                    if (statement) {
                        (void) InsertNodeAfterNearestStateNode(statement, statement_node, new_mutually_exclusive_event_map, visited) ;
                    } else {
                        if (!mutually_exclusive) {
                            // If statement without else part
                            VerificCFGRootNode *scope_end = node->GetScopeEnd() ;
                            (void) InsertNodeAfterNearestStateNode(scope_end, statement_node, mutually_exclusive_event_map, visited) ;
                        }
                    }
                }
            }
        } else {
            Array *children = new Array(2) ;
            SetIter si ;
            VerificCFGRootNode *child ;
            FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
                if (!child) continue ;
                children->InsertLast(child) ;
            }

            unsigned ai ;
            FOREACH_ARRAY_ITEM(children, ai, child) {
                if (!child) continue ;
                (void) InsertNodeAfterNearestStateNode(child, statement_node, mutually_exclusive_event_map, visited) ;
            }
            delete children ;
        }
    }

    return 1 ;
}

// Reset all the flages and expression set after each always block traversal:
void
VeriFsmAlwaysVisitor::Reset()
{
    // Reset all flags:
    _has_always = 0 ;
    _has_initial = 0 ;
    _has_event_control = 0 ;
    _has_forever = 0 ;
    _edged_expr_inserted = 0 ;
    _mult_clk = 0 ;
    _convert = 0 ;
    _first_blk_id = 0 ;
    _label_already_set = 0 ;
    _decl_node = 0 ;
    _is_converted = 1 ;
    _is_not_event_or_edged = 0 ;
    _edged_named_event = 0 ;
    _mixed_event = 0 ;
    _has_event_trigger = 0 ;
    _rearrange = 1 ;
    _clock_followed = 1 ;
    _has_nonedged_event_control = 0 ;

    // Reset the sets:
    SetIter si ;
    char *s ;
    FOREACH_SET_ITEM(&_event_expr_set, si, &s) {
        Strings::free(s) ;
    }
    _event_expr_set.Reset() ;
    _event_control_set.Reset() ;
    _event_set->Reset() ;
}

// Check some conditions/limitations, create the new module item:
unsigned VeriFsmAlwaysVisitor::ConvertToExplicitStateMachine(const VeriModuleItem *item, VeriStatement *stmt)
{
    if (!stmt) return 0 ;

    // Check for forever block within initial block:
    if (_has_initial && !_has_forever) {
        // VIPER #6843: Need to convert if the initial construct
        // contains at least one event control statements although
        // it does not contain forever statement
        if (_event_control_set.Size() < 1) { // VIPER #7881
            Reset() ;
            return 0 ; // no need to convert
        } else {
            _convert = 1 ; // should convert
        }
    }

    // Check for presence of clock/envent control:
    if (!_has_event_control) return 0 ;

    // Check for multiple clocking:
    if (_mult_clk) {
        stmt->Info("multiple clocking is not yet supported") ;
        _is_converted = 0 ;
    }

    // Check for wait statement that is neither event or edged:
    // eg. input in
    //     always@(in)
    if (_is_not_event_or_edged) {
        Reset() ;
        return 0 ;
    }

    // Edged named event
    if (_edged_named_event) {
        stmt->Info("not converting statement with edged named event") ;
        _is_converted = 0 ;
    }

    // Mixture of clocking and wait event
    if (_mixed_event) {
        stmt->Info("not converting statement with mixture of clocking and wait event") ;
        _is_converted = 0 ;
    }

    // VIPER #7881: No need to convert to the explicitStateMachine if there is only one event control statement
    // at the very beginning of the always contruct. But do the conversion if the always construct contains
    // single event control statement not at the very beginning.
    if (_event_control_set.Size() <= 1 && !_is_converted) {
        Reset() ;
        return 0 ;
    }

    // Traverse and generate the control flow graph, print the graph, modify the parse tree
    // and generate the corresponding explicit state machine:
    VerificCFGRootNode *root = ProcessStatement(stmt) ;

    VERIFIC_ASSERT(root) ;
    root->SetConversionStatus(_is_converted) ;

    if (_always_map) (void) _always_map->Insert(item, root) ; // store the cfg vrs module item
    if (_decl_node_map) (void) _decl_node_map->Insert(item, _decl_node) ; // store declation node vrs module item
    if (_first_blk_id_map) (void) _first_blk_id_map->Insert(item, _first_blk_id) ; // store the label id of the first block within always construct vrs module item
    if (_internal_event_node_map) (void) _internal_event_node_map->Insert(item, _event_nodes) ;

    Set *event_set = new Set(POINTER_HASH) ;
    SetIter si ;
    VeriIdDef *event_id ;
    FOREACH_SET_ITEM(_event_set, si, &event_id) (void) event_set->Insert(event_id) ;
    if (_root_vrs_event_var_map) (void) _root_vrs_event_var_map->Insert(root, event_set) ;
    else delete event_set ; //JJ: need to clean this up if we don't insert it into _root_vrs_event_var_map

    Reset() ;
    return 1 ;
}

// Generate the CFG :
VerificCFGRootNode*
VeriFsmAlwaysVisitor::ProcessStatement(VeriStatement *stmt)
{
    if (!stmt) return 0 ;

    // Generate the control flow diagram:
    VeriFsmProcessVisitor process_visitor ;
    process_visitor.SetScope(_scope) ;
    process_visitor.SetTempArray(_temp_array) ;
    process_visitor.SetNewlyCreatedArray(_newly_created_arr) ;

    // Always create a dummy statement node as the top node of the cfg
    VerificCFGRootNode *parent_node = new VerificCFGStatementNode() ;
    process_visitor.SetCurrentNode(parent_node) ;
    process_visitor.SetParentNode(parent_node) ;

    if (!_is_converted) return parent_node ;

    stmt->Accept(process_visitor) ;

    // _decl_node contains the internal declations created during cfg generation
    _decl_node = process_visitor.GetDeclNode() ;

    VerificCFGRootNode *root = process_visitor.GetParentNode() ;
    VerificCFGRootNode *child = process_visitor.GetCurrentNode() ;
    _state_num = process_visitor.GetState() ;

    VERIFIC_ASSERT(root) ;

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
    // Print the control flow diagram:
    printf ("----------before rearrange----------------------------\n") ;
    root->PrintNode(&print_veri_tree_node, 0) ;
    printf ("--------------------------------------\n") ;
#endif

    // Set the last node as the parent of the child of first node:
    // eg: 1. statement node
    //     2. statement node
    //     3. state node
    //     4. condition node
    //     5. end node
    //     2. statement node
    // Now here _parent_node is no1 statement node and _current_node is no5 end_node,
    // so we set no5 node as the parent node of no2 node and no2 as the child node of no5 node.
    if (!_convert) {
        VerificCFGRootNode *next_node = root->GetChild() ;
        if (!next_node) {
            _is_converted = 0 ;
            return root ;
        }
        (void) next_node->AddParent(child) ; // Create a edge between the child node and the child of top node.
        //(void) root->AddParent(child) ;
    } else {
        // For initial construct with more than one event control
        // convert to explicit state m/c where the m/c will stuck
        // at a default state with empty body.
        // Create a state node:
        VerificCFGStateNode *state = new VerificCFGStateNode() ;
        state->SetLinefile(stmt->Linefile()) ;
        state->SetState(process_visitor.GetState()) ;
        (void) state->AddParent(child) ;
    }

    // Designs with event trigger/non-edged event control statement we left processing the wait statement
    // and triggering statement during cfg creation. Now we traverse the cfg and rearrange the cfg
    // replacing the wait and trigger statement.
    if (_has_event_trigger || _has_nonedged_event_control) {
        Set visited(POINTER_HASH) ;
        // Traverse cfg and push state node after all the state nodes corresponding to the wait statements
        unsigned rearrange = ReArrangeCfgForEventWait(root, &visited) ;
        visited.Reset() ;

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
        // Print the control flow diagram:
        printf ("------------during rearrange--------------------------\n") ;
        root->PrintNode(&print_veri_tree_node, 0) ;
        printf ("--------------------------------------\n") ;
#endif

        if (!rearrange) { // A wait statement be must followed by a clocking statement: for simulation matching
            stmt->Error("wait statement must be followed by event control statement") ;
            _is_converted = 0 ;
            return root ;
        } else {
            // Traverse the cfg replacing the state nodes corresponding to the wait statements with a subgraph
            // eg. @start is replaced with the following subgraph
            //     if (start_old != start) /*conditional node*/
            //         state1
            //     if (start_old == start) /*conditional node*/
            //         state2
            (void) ReplaceEventNode(root, &visited) ;
            visited.Reset() ;

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
            // Print the control flow diagram:
            printf ("------------after rearrange--------------------------\n") ;
            root->PrintNode(&print_veri_tree_node, 0) ;
            printf ("--------------------------------------\n") ;
#endif

            // Traverse the cfg pushing a statement node(restoring the value of event) after the nearest clock after event trigger
            // and also replace the triggering statement with a conditional statement.
            // eg. ->start
            //     @posedge clk
            // it will be converted to
            //     if (start == start_old)
            //         start = ~start ;
            //     start_old = start ;
            (void) ReArrangeCfgForEventTrigger(root, &visited) ;
            visited.Reset() ;
        }
    }

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
    // Print the control flow diagram:
    printf ("------------after trigger rearrange--------------------------\n") ;
    root->PrintNode(&print_veri_tree_node, 0) ;
    printf ("--------------------------------------\n") ;
#endif

    return root ;
}

// Create explicit state machine:
VeriModuleItem*
VeriFsmGenerateVisitor::CreateExplicitStateMachine(const VeriModuleItem *mod_item, VerificCFGRootNode *root, const VerificCFGDeclarationNode *decl_node, VeriIdDef *first_blk_id, Map *internal_event_map, unsigned always_construct)
{
    if (!root || !mod_item) return 0 ;
    VeriStatement *stmt = mod_item->GetStmt() ;
    if (!stmt) return 0 ;

    // Count the state nodes
    Array state_count ;
    Set state_node_set(POINTER_HASH) ;      // VIPER #7031: set to store the state number(increased by 1) of state node
    Map state_node_map(POINTER_HASH) ;      // VIPER #7031: map to store node vrs state node
    Map duplicate_state_map(POINTER_HASH) ; // VIPER #7034: multi-map containing all the duplicate state nodes against a particular state node
    CountStateNode(root, state_count, 0, state_node_set, state_node_map, duplicate_state_map) ;
    ReNumberStateNode(state_count, duplicate_state_map) ; // VIPER #7034: Renumber state nodes as 0, 1, 2...

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
    printf("state_count = %d", state_count.Size()) ;
    printf("\n") ;
    printf ("--------------------------------------\n") ;
#endif

    // Copy parse tree and generate the explicit state machine:
    // map for copy ids:
    VeriMapForCopy old2new(POINTER_HASH) ;

    // Insert all the label ids into the map old2new
    Map *mod_decl_ids = _module_scope->DeclArea() ;
    MapIter mi ;
    VeriIdDef *mod_id ;
    FOREACH_MAP_ITEM(mod_decl_ids, mi, 0, &mod_id) {
        if (mod_id && mod_id->IsBlock()) (void) old2new.Insert(mod_id, mod_id) ;
    }

    char *prefix = 0 ;
    VeriIdDef *label_id = 0 ;
    if (first_blk_id) { // VIPER #6922: If the always/initial block is labeled, then use that label id for the new always block
        label_id = first_blk_id ;
        // VIPER #7249: If the label of always/initial block is used for new always block
        // then detach the labe from its previous scope and also invalidate label pointer
        VeriModuleItem *item = first_blk_id->GetModuleItem() ;
        if (item) item->SetId(0) ;
    } else { // For unlabeled block create a new block id
        // Label id declaration:
        char *temp_always_construct = Strings::itoa((int)always_construct) ;
        char *tmp1 = temp_always_construct ;
        prefix = VeriFsmAlwaysVisitor::CreateVariableName("verific_implicit_to_explicit_conv_scope", 0, _scope) ;
        char *tmp2 = prefix ;
        label_id = new VeriBlockId(Strings::save(prefix, "_", temp_always_construct)) ;
        label_id->SetLinefile(stmt->Linefile()) ;
        Strings::free(tmp1) ;
        Strings::free(tmp2) ;
        if (_scope) (void) _scope->Declare(label_id) ; // Declare the labeled id in the present scope
    }

    VeriScope *old_scope = stmt->GetScope() ;

    // Create new always scope from the parent scope:
    VeriScope *always_scope = new VeriScope(_scope, label_id) ;

    // Array containing all the declared items
    Array *decl_items = new Array() ;

    VeriIdDef *reset_id = 0 ;
    //if (_reset && !root->IsStateNode()) {
    if (_reset) { // We always create a dummy statement node as the root node, no scope to have state node as root
        // Reset variable declaration:
        prefix = VeriFsmAlwaysVisitor::CreateVariableName("reset_var", 0, _scope) ;
        reset_id = new VeriVariable(prefix) ;
        reset_id->SetLinefile(stmt->Linefile()) ;
        (void) always_scope->Declare(reset_id) ;

        VeriDataType *reset_data_type = new VeriDataType(VERI_REG, 0, 0) ;
        VeriDataDecl *reset_data_decl = new VeriDataDecl(VERI_REG, reset_data_type, reset_id) ;
        reset_data_decl->SetLinefile(stmt->Linefile()) ;
        decl_items->InsertLast(reset_data_decl) ;
    }

    // State var declaration:
    // VIPER #7881: No need of state varibale if we have only one state.
    // otherwise create state variable.
    if (state_count.Size() > 1) {
        prefix = VeriFsmAlwaysVisitor::CreateVariableName("state_var", 0, _scope) ;
        VERIFIC_ASSERT(!_var_id && "old state id not freed") ;
        _var_id = new VeriVariable(prefix) ;
        _var_id->SetLinefile(stmt->Linefile()) ;
        (void) always_scope->Declare(_var_id) ;

        VeriExpression *right = new VeriIntVal(0) ;
        unsigned state_num = state_count.Size() ;
        int digit = 0 ;
        while(state_num) {
            digit++ ;
            state_num = state_num >> 1 ;
        }
        if (digit) digit = digit-1 ;
        VeriExpression *left = new VeriIntVal(digit) ;
        VeriRange *range = new VeriRange(left, right) ;

        VeriDataType *data_type = new VeriDataType(VERI_REG, 0, range) ;
        VeriDataDecl *data_decl = new VeriDataDecl(VERI_REG, data_type, _var_id) ;
        data_decl->SetLinefile(stmt->Linefile()) ;
        decl_items->InsertLast(data_decl) ;
    }

    // Repeat var/local declaration:
    // Declare all the ids in the copied/new always scope
    unsigned num_decls = decl_node ? decl_node->NumDeclaration() : 0 ;
    unsigned i ;
    for (i=0; i<num_decls; i++) {
        VeriDataDecl *d_decl = decl_node ? (VeriDataDecl *) decl_node->GetDeclaration(i) : 0 ;
        if (!d_decl) continue ;
        VeriScope *decl_scope = decl_node ? (VeriScope *) decl_node->GetScope(i) : 0 ;
        VeriScope *scope = decl_scope ;
        // Array for keeping the list of label ids:
        Array *label_arr = new Array() ;
        // count the label of depth of named sequential block:
        unsigned seq_blk_count = 0 ;

        while(scope && (scope != old_scope)) {
            VeriIdDef *label = scope->GetOwner() ;
            if (label && (label != first_blk_id)) {
                (void) old2new.Insert(label, label) ;
                label_arr->InsertLast(label) ;
                seq_blk_count++ ;
            }
            scope = scope->Upper() ;
        }

        Array *decl_ids = d_decl->GetIds() ;
        unsigned ai ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(decl_ids, ai, id) {
            if (!id) continue ;
            VeriIdDef *new_id = 0 ;
            // If name conflicts, create new name and id othewise copy the existing id
            prefix = VeriFsmAlwaysVisitor::CreateVariableName(id->GetName(), label_arr, always_scope) ;
            if (!Strings::compare(prefix, id->GetName())) {
                char *tmp = prefix ;
                new_id = id->CopyWithName(prefix, old2new) ;
                (void) _old2new_ids_map->Insert(id, new_id) ; // map containing old vrs new ids required for renaming selected name
                (void) _old_ids_seqblk_map->Insert(id, (void*) (unsigned long) seq_blk_count) ; // map containing old ids vrs the depth of sequential block
                Strings::free(tmp) ;
            } else {
                // VIPER #7474: Set initial value for local parameters:
                // put 1 in the second argument of CopyId() routine
                // it will copy the initial values if exists
                new_id = id->CopyId(old2new, 1) ;
                Strings::free(prefix) ;
            }
            (void) always_scope->Declare(new_id) ; // declare all ids in the always scope

            // VIPER #6922: Decalre enum ids
            VeriDataType *d_type =id->GetDataType() ;
            if (d_type && d_type->IsEnumType()) {
                Array *enum_ids = d_type->GetIds() ;
                unsigned aj ;
                VeriIdDef *enum_id ;
                FOREACH_ARRAY_ITEM(enum_ids, aj, enum_id) {
                    if (!enum_id) continue ;
                    VeriIdDef *new_enum_id = 0 ;
                    prefix = VeriFsmAlwaysVisitor::CreateVariableName(enum_id->GetName(), label_arr, always_scope) ;
                    if (!Strings::compare(prefix, enum_id->GetName())) {
                        char *tmp = prefix ;
                        new_enum_id = enum_id->CopyWithName(prefix, old2new) ;
                        (void) _old2new_ids_map->Insert(enum_id, new_enum_id) ; // map containing old vrs new ids required for renaming selected name
                        (void) _old_ids_seqblk_map->Insert(enum_id, (void*) (unsigned long) seq_blk_count) ; // map containing old ids vrs the depth of sequential block
                        Strings::free(tmp) ;
                    } else {
                        // VIPER #7474: put 1 in the second argument of CopyId() routine
                        new_enum_id = enum_id->CopyId(old2new, 1) ;
                        Strings::free(prefix) ;
                    }
                    (void) always_scope->Declare(new_enum_id) ; // declare all ids in the always scope
                }
            }
        }
        VeriModuleItem *new_data_decl = d_decl->CopyModuleItem(old2new) ;
        decl_items->InsertLast(new_data_decl) ; // store in the array of declaring items
        delete label_arr ;
    }

    Map state_map(POINTER_HASH) ; // contains the starting and ending node of a cfc path
    Map mutually_exclusive_event_map(POINTER_HASH) ;

    // Reset block:
    VeriSeqBlock *reset_blk = 0 ;
    //if (_reset && !root->IsStateNode()) {
    if (_reset) { // We always create a dummy statement node as the root node, no scope to have state node as root
        Array *stmt_arr = new Array() ;
        VeriScope *reset_scope = new VeriScope(1/*empty scope*/, always_scope) ;
        // until get a state node
        ExplicitStateMachine(root, 0, state_map, internal_event_map, mutually_exclusive_event_map, 0, stmt_arr, reset_scope, old2new) ;

        VeriIdRef *l_val = new VeriIdRef(reset_id) ;
        VeriExpression *r_val = new VeriIntVal((int)0) ;
        VeriNonBlockingAssign *reset_stmt = new VeriNonBlockingAssign(l_val, 0, r_val) ;
        reset_stmt->SetLinefile(stmt->Linefile()) ;
        stmt_arr->InsertLast(reset_stmt) ;

        state_map.Reset() ;
        mutually_exclusive_event_map.Reset() ;

        reset_blk = new VeriSeqBlock(0, 0, stmt_arr, reset_scope) ;
        reset_blk->SetLinefile(stmt->Linefile()) ;
    }

    // Array containing the initial construct for state/reset
    // variable initialization and new always construt
    Array *new_module_items = new Array(2) ;

    // Set flag for generate items checking generate scope:
    VeriScope *new_seq_blk_scope = 0 ; // child scope of the original scope
    unsigned is_generate = 0 ;
    if (_scope && _scope->IsGenerateScope()) {
        is_generate = 1 ;
        new_seq_blk_scope = new VeriScope(1, _scope) ; // child scope of the original scope
    }

    // Initialize state variable:
    // VIPER #7881: No need of state varibale if we have only one state.
    // otherwise we have state variables, and here we initialize them.
    Array *initial_arr = new Array(1) ;
    if (state_count.Size() > 1) {
        VeriIdRef *selected_prefix = new VeriIdRef(label_id) ;
        const char *var_id_name = _var_id->Name() ;
        char *suffix = Strings::save(var_id_name) ;
        VeriSelectedName *init_statement = new VeriSelectedName(selected_prefix, suffix) ;
        init_statement->SetId(_var_id) ;
        init_statement->SetLinefile(stmt->Linefile()) ;
        unsigned init_state =  _init_state_node ? _init_state_node->GetState() : 0 ;
        VeriExpression *r_val = new VeriIntVal((int)init_state) ;
        VeriNonBlockingAssign *initial_state_stmt = new VeriNonBlockingAssign(init_statement, 0, r_val) ;
        initial_state_stmt->SetLinefile(stmt->Linefile()) ;
        if (is_generate) {
            initial_arr->InsertLast(initial_state_stmt) ;
        } else {
            VeriInitialConstruct *initial_state_construct = new VeriInitialConstruct(initial_state_stmt) ;
            initial_state_construct->SetLinefile(stmt->Linefile()) ;
            VeriIdDef *owner = _scope ? _scope->GetOwner() : 0 ;
            if (!owner) owner = _module_scope ? _module_scope->GetOwner() : 0 ;
            VeriModuleItem *module_item = owner ? owner->GetModuleItem() : 0 ;
            Array *items = module_item ? module_item->GetItems() : 0 ;
            // Add the variable declaration to the module scope:
            if (items) items->InsertFirst(initial_state_construct) ;
            else delete initial_state_construct ; //JJ 130918 possible memleak
        }
    }

    // Initialize reset variable:
    //if (_reset && !root->IsStateNode()) {
    if (_reset) { // We always create a dummy statement node as the root node, no scope to have state node as root
        const char *reset_id_name = reset_id ? reset_id->Name() : 0 ;
        char *suffix = Strings::save(reset_id_name) ;
        VeriExpression *r_val = new VeriIntVal((int)1) ;
        VeriIdRef *selected_prefix = new VeriIdRef(label_id) ;
        VeriSelectedName *init_statement = new VeriSelectedName(selected_prefix, suffix) ;
        init_statement->SetId(reset_id) ;
        init_statement->SetLinefile(stmt->Linefile()) ;
        VeriNonBlockingAssign *initial_reset_stmt = new VeriNonBlockingAssign(init_statement, 0, r_val) ;
        initial_reset_stmt->SetLinefile(stmt->Linefile()) ;
        if (is_generate) {
            initial_arr->InsertLast(initial_reset_stmt) ;
        } else {
            VeriInitialConstruct *initial_reset_construct = new VeriInitialConstruct(initial_reset_stmt) ;
            initial_reset_construct->SetLinefile(stmt->Linefile()) ;
            VeriIdDef *owner = _scope ? _scope->GetOwner() : 0 ;
            if (!owner) owner = _module_scope ? _module_scope->GetOwner() : 0 ;
            VeriModuleItem *module_item = owner ? owner->GetModuleItem() : 0 ;
            Array *items = module_item ? module_item->GetItems() : 0 ;
            if (items) items->InsertFirst(initial_reset_construct) ;
        }
    }

    if (is_generate) {
        VeriScope *seq_blk_scope = new VeriScope(1, new_seq_blk_scope) ;
        VeriSeqBlock *initial_seq_blk = new VeriSeqBlock(0, 0, initial_arr, seq_blk_scope) ;
        VeriInitialConstruct *initial_construct = new VeriInitialConstruct(initial_seq_blk) ;
        initial_construct->SetLinefile(stmt->Linefile()) ;
        new_module_items->InsertLast(initial_construct) ;
    }

    // Set block:
    VeriSeqBlock *case_blk = 0 ;
    // VIPER #7881: If we have only one state, then we no need to create an case statements.
    // rather we create a sequential block.
    if (state_count.Size() == 1) {
        VerificCFGRootNode *state_node = (VerificCFGRootNode *)state_count.GetLast() ;
        if (state_node) {
            VeriScope *seq_scope = new VeriScope(1/*empty scope*/, always_scope) ;
            Array *stmt_arr = new Array() ;
            ExplicitStateMachine(state_node, 0, state_map, internal_event_map, mutually_exclusive_event_map, 0, stmt_arr, seq_scope, old2new) ;

            case_blk = new VeriSeqBlock(0, 0, stmt_arr, seq_scope) ;
            case_blk->SetLinefile(state_node->GetLinefile()) ;
        }
        state_map.Reset() ;
        mutually_exclusive_event_map.Reset() ;

        // Infinite loop exists:
        if (_infinite_loop) {
            stmt->Warning("possible infinite loop detected") ; // VIPER #7805: Change message info
        }
    } else { // create case statement if we have more than one state
        VeriScope *set_scope = new VeriScope(1/*empty scope*/, always_scope) ;
        Array *case_items = new Array() ;
        VerificCFGStateNode *state_node ;
        unsigned ai ;
        FOREACH_ARRAY_ITEM(&state_count, ai, state_node) {
            if (!state_node) continue ;
            VeriScope *case_scope = new VeriScope(1/*empty scope*/, set_scope) ;
            Array *expr_arr = new Array() ;
            Array *stmt_arr = new Array() ;
            ExplicitStateMachine(state_node, 0, state_map, internal_event_map, mutually_exclusive_event_map, expr_arr, stmt_arr, case_scope, old2new) ;

            VeriSeqBlock *seq_blck = new VeriSeqBlock(0, 0, stmt_arr, case_scope) ;
            seq_blck->SetLinefile(state_node->GetLinefile()) ;
            VeriCaseItem *case_item = new VeriCaseItem(expr_arr, seq_blck) ;
            case_item->SetLinefile(state_node->GetLinefile()) ;
            case_items->InsertLast(case_item) ;

            state_map.Reset() ;
            mutually_exclusive_event_map.Reset() ;
        }
        state_map.Reset() ;

        // Infinite loop exists:
        if (_infinite_loop) {
            stmt->Warning("possible infinite loop detected") ; // VIPER #7805: Change message info
        }

        // case statement:
        VeriIdRef *case_condition = new VeriIdRef(_var_id) ;
        VeriCaseStatement *case_stmt = new VeriCaseStatement(VERI_CASE, case_condition, case_items) ;
        Array *stmts = new Array(1) ; // contains case statement
        stmts->InsertLast(case_stmt) ;
        case_blk = new VeriSeqBlock(0, 0, stmts, set_scope) ;
        case_blk->SetLinefile(stmt->Linefile()) ;
    }

    Array *always_stmts = new Array(2) ;
    VeriConditionalStatement *cond_stmt = 0 ;
    //if (_reset && !root->IsStateNode()) {
    if (_reset) { // We always create a dummy statement node as the root node, no scope to have state node as root
        VeriIdRef *cond_id_ref = new VeriIdRef(reset_id) ;
        cond_id_ref->SetLinefile(stmt->Linefile()) ;
        cond_stmt = new VeriConditionalStatement(cond_id_ref, reset_blk, case_blk) ;
        cond_stmt->SetLinefile(stmt->Linefile()) ;
        always_stmts->InsertLast(cond_stmt) ;
    }

    if (!cond_stmt) {
        always_stmts->InsertLast(case_blk) ;
    }

    VeriSeqBlock *seq_blck = new VeriSeqBlock(label_id, decl_items, always_stmts, always_scope) ; // scope
    seq_blck->SetLinefile(stmt->Linefile()) ;

    // Create the event control statement:
    Array *event_arr = new Array(2) ;
    Array *arr = _init_state_node ? _init_state_node->GetEventExprList() : 0 ;
    unsigned ai ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(arr, ai, expr) {
        if (!expr) continue ;
        event_arr->InsertLast(expr->CopyExpression(old2new)) ;
    }

    // VIPER #7005: add reset variable in event control expressions, such that
    // the event control statement will be '@(posedge clk or posedge label.reset)'
    // so that the statements before first clock edge will be executed at time 0.
    //if (_reset && !root->IsStateNode()) {
    if (_reset) { // We always create a dummy statement node as the root node, no scope to have state node as root
        const char *reset_id_name = reset_id ? reset_id->Name() : 0 ;
        char *suffix = Strings::save(reset_id_name) ;
        VeriIdRef *selected_prefix = new VeriIdRef(label_id) ;
        VeriSelectedName *selected_expr = new VeriSelectedName(selected_prefix, suffix) ;
        selected_expr->SetLinefile(stmt->Linefile()) ;
        VeriEventExpression *event_expr = new VeriEventExpression(VERI_POSEDGE, selected_expr) ;
        event_expr->SetLinefile(stmt->Linefile()) ;
        event_arr->InsertLast(event_expr) ;
    }
    VeriEventControlStatement *event_control_stmt = new VeriEventControlStatement(event_arr, seq_blck) ;
    event_control_stmt->SetLinefile(stmt->Linefile()) ;

    // Generate always block:
    VeriAlwaysConstruct *always = new VeriAlwaysConstruct(event_control_stmt) ;
    always->SetLinefile(stmt->Linefile()) ;

    VeriSeqBlock *new_seq_block = 0 ;
    if (is_generate) {
        new_module_items->InsertLast(always) ;
        new_seq_block = new VeriSeqBlock(0, 0, new_module_items, new_seq_blk_scope) ;
        new_seq_block->SetLinefile(stmt->Linefile()) ;
    }

    _var_id = 0 ;
    _init_state_node = 0 ;
    if (!is_generate) {
        delete initial_arr ;
        delete new_module_items ;
    }

    // return new item
    if (is_generate) return new_seq_block ;
    return always ;
}

/*-----------------------------------------------------------------*/
//       VeriFsmEventVisitor
/*-----------------------------------------------------------------*/

VeriFsmEventVisitor::VeriFsmEventVisitor():
    VeriVisitor(),
    _has_event(0),
    _has_local_block(0),
    _has_jump_or_disable(0)
{
}

VeriFsmEventVisitor::~VeriFsmEventVisitor()
{
    _has_event = 0 ;
    _has_local_block = 0 ;
    _has_jump_or_disable = 0 ;
}

// Traverse the event control statement:
void
VeriFsmEventVisitor::VERI_VISIT(VeriEventControlStatement, event)
{
    (void) event ;
    _has_event = 1 ;
}

// Traverse the event trigger statement:
void
VeriFsmEventVisitor::VERI_VISIT(VeriEventTrigger, event)
{
    (void) event ;
    _has_event = 1 ;
}

// Traverse sequential block:
void VeriFsmEventVisitor::VERI_VISIT(VeriSeqBlock, seq_blk)
{
    if (seq_blk.GetDeclItems()) {
        _has_local_block = 1 ;
    }
    TraverseArray(seq_blk.GetDeclItems()) ;

    // Traverse the block statements
    TraverseArray(seq_blk.GetStatements()) ;
}

// Traverse the jump statement:
void
VeriFsmEventVisitor::VERI_VISIT(VeriJumpStatement, jump)
{
    (void) jump ;
    _has_jump_or_disable = 1 ;
}

// Traverse the disable  statement:
void
VeriFsmEventVisitor::VERI_VISIT(VeriDisable, disable)
{
    (void) disable ;
    _has_jump_or_disable = 1 ;
}

/*-----------------------------------------------------------------*/
//           VeriFsmProcessVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VeriFsmProcessVisitor::VeriFsmProcessVisitor():
    VeriVisitor(),
    _parent_node(0),
    _current_node(0),
    _loop_node(0),
    _seq_blk_end(0),
    _decl_node(0),
    _seq_blk_map(POINTER_HASH),
    _state_num(0),
    _scope(0),
    _continue_nodes(new Array()),
    _newly_created_arr(0),
    _temp_array(0),
    _decl_map(POINTER_HASH)
{
}

VeriFsmProcessVisitor::~VeriFsmProcessVisitor()
{
    delete _continue_nodes ;

    _temp_array = 0 ;
    _newly_created_arr = 0 ;
    _decl_map.Reset() ;
    _seq_blk_map.Reset() ;

    _loop_node = 0 ;
    _seq_blk_end = 0 ;
    _decl_node = 0 ;
    _current_node = 0 ;
    _parent_node = 0 ;
    _scope = 0 ;
}

/*-----------------------------------------------------------------*/
//           VeriFsmProcessVisitor: routine
/*-----------------------------------------------------------------*/

// Add parent node
void VeriFsmProcessVisitor::AddMyParent(VerificCFGRootNode *parent, VerificCFGRootNode *child) const
{
    if (!child) return ;

    (void) child->AddParent(parent) ;
}

/*-----------------------------------------------------------------*/
//        Visit Methods :
/*-----------------------------------------------------------------*/

// Traverse the statements:
void VeriFsmProcessVisitor::VERI_VISIT(VeriStatement, stmt)
{
    // If the _current_node is a statement node, we do not create a new statement node
    // rather store the statement in the array of the current statement node:
    if (!_current_node || (!_current_node->IsSimpleStatementNode() || _current_node->IsStateNode())) {
        VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
        AddMyParent(_current_node, statement) ;
        _current_node = statement ;
    }

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    (void) _current_node->AddStatement(&stmt, _scope) ;
}

// Traverse the event control statements:
void VeriFsmProcessVisitor::VERI_VISIT(VeriEventControlStatement, event)
{
    Array *event_arr = event.GetAt() ;
    if (!event_arr || !event_arr->Size()) {
        TraverseNode(event.GetStmt()) ;
    } else {
            // Create a state node:
            VerificCFGStateNode *state = new VerificCFGStateNode() ;
            AddMyParent(_current_node, state) ;
            _current_node = state ;

            // Set the parent node if there is no parent node:
            if (!_parent_node) _parent_node = _current_node ;

            // @(posedge clk) event_stmt
            // Get the list of events
            TraverseArray(event.GetAt()) ;

            // Get the state, increase that by one and
            // set the new state to the _current_node:
            state->SetState(_state_num++) ;
            state->AddEventExprList(event_arr) ;
            state->SetLinefile(event.Linefile()) ;

        // Get event statement:
        VeriStatement *event_stmt = event.GetStmt() ;
        if (event_stmt && !event_stmt->IsNullStatement()) {
            TraverseNode(event_stmt) ;
        }
    }
}

void VeriFsmProcessVisitor::VERI_VISIT(VeriEventTrigger, event)
{
    VerificCFGStatementNode *event_stmt = new VerificCFGStatementNode() ;
    AddMyParent(_current_node, event_stmt) ;
    _current_node = event_stmt ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    VERIFIC_ASSERT(event_stmt == _current_node) ;

    (void) _current_node->AddStatement(&event, _scope) ;
}

// Traverse the conditional statements:
void VeriFsmProcessVisitor::VERI_VISIT(VeriConditionalStatement, cond_stmt)
{
    // Check for event control/trigger within the conditional block:
    VeriFsmEventVisitor visitor ;
    cond_stmt.Accept(visitor) ;

    // If no event control within the conditional block
    // store the whole block in the arr of statement node
    // othewise create a new conditional node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, cond_stmt) ;
    } else {
        // Create a conditional node
        VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
        AddMyParent(_current_node, cond) ;
        _current_node = cond ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Add conditions:
        cond->SetConditionExpression((void*)cond_stmt.GetIfExpr()) ;

        VERIFIC_ASSERT(cond == _current_node) ;

        // scope end node:
        VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond) ;
        (void) cond->SetScopeEnd(cond_end) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of conditional node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process then and else part:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse then statement
        TraverseNode(cond_stmt.GetThenStmt()) ;
        (void) cond->AddBranch(0, _parent_node) ;
        AddMyParent(_current_node, cond_end) ;

        // Again make the _current_node and
        // the _parent_node set to null:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse else statement
        TraverseNode(cond_stmt.GetElseStmt()) ;
        (void) cond->AddBranch(0, _parent_node) ;
        if (_current_node) {
            // Has else part
            AddMyParent(_current_node, cond_end) ;
        } else { // no else part
            AddMyParent(cond, cond_end) ;
        }

        // Keep a link between the conditional node and
        // the scope end node:
        _current_node = cond_end ;
        // Restore the original parent node:
        _parent_node = orig_parent_node ;
    }
}

// Traverse case statement
void VeriFsmProcessVisitor::VERI_VISIT(VeriCaseStatement, case_stmt)
{
    // Check for event control/trigger within the case block:
    VeriFsmEventVisitor visitor ;
    case_stmt.Accept(visitor) ;

    // If no event control within the case block
    // store the whole block in the arr of statement node
    // othewise create a new conditional node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, case_stmt) ;
    } else {
        // Create conditional node
        VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
        AddMyParent(_current_node, cond) ;
        _current_node = cond ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Add conditions:
        cond->SetConditionExpression((void*)case_stmt.GetCondition()) ;

        VERIFIC_ASSERT(cond == _current_node) ;

        // Keep the original parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // Keep a link between the conditional node and
        // the scope end node:
        VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond) ;
        (void) cond->SetScopeEnd(cond_end) ;

        // For each branch of conditional node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process each case item:
        // Traverse case items
        unsigned ai ;
        VeriCaseItem *case_item, *default_item = 0 ;
        FOREACH_ARRAY_ITEM(case_stmt.GetCaseItems(), ai, case_item) {
            if (!case_item) continue ;
            if (!case_item->GetConditions()) {
                default_item = case_item ;
                continue ;
            }
            TraverseNode(case_item) ;
        }
        if (default_item) TraverseNode(default_item) ;

        _current_node = cond_end ;
        // Restore the original parent node:
        _parent_node = orig_parent_node ;
    }
}

// Traverse the case items:
void VeriFsmProcessVisitor::VERI_VISIT(VeriCaseItem, case_item)
{
    // Keep the previous scope:
    VeriScope *prev_scope = _scope ;
    VeriScope *case_item_scope = case_item.GetScope() ;
    if (case_item_scope) _scope = case_item_scope ;

    VerificCFGRootNode *cond = _current_node ;
    if (!cond || !cond->IsConditionalNode()) {
        _scope = prev_scope ;
        return ;
    }

    _current_node = 0 ;
    _parent_node = 0 ;
    TraverseArray(case_item.GetConditions()) ;

    _current_node = 0 ;
    _parent_node = 0 ;
    TraverseNode(case_item.GetStmt()) ;

    Array *case_cond_arr = case_item.GetConditions() ;
    Array *cond_arr = 0 ;
    if (case_cond_arr) cond_arr = new Array(*(case_cond_arr)) ;
    (void) cond->AddBranch(cond_arr, _parent_node) ;

    VerificCFGRootNode *cond_end  = cond->GetScopeEnd() ;
    AddMyParent(_current_node, cond_end) ;

    _current_node = cond ;

    // Retrive the previous scope:
    _scope = prev_scope ;
}

// Traverse while statement
void VeriFsmProcessVisitor::VERI_VISIT(VeriWhile, while_loop)
{
    // Check for event control/trigger within the loop block:
    VeriFsmEventVisitor visitor ;
    while_loop.Accept(visitor) ;

    // If no event control within the loop block
    // store the whole block in the arr of statement node
    // othewise create a new loop node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, while_loop) ;
    } else {
        // Keep the previous scope:
        VeriScope *prev_scope = _scope ;
        _scope = while_loop.GetScope() ;

        // Create a loop node:
        VerificCFGConditionalStatementNode *loop = new VerificCFGConditionalStatementNode() ;
        AddMyParent(_current_node, loop) ;
        _current_node = loop ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Store the previous loop node and set the current
        // loop node to _loop_node for treating break/continuous statements:
        VerificCFGRootNode *prev_loop_node = _loop_node ;
        _loop_node = loop ;

        // Add conditions:
        loop->SetConditionExpression(while_loop.GetCondition()) ;

        VERIFIC_ASSERT(loop == _current_node) ;

        // Scope end node:
        VerificCFGScopeEndNode *loop_end = new VerificCFGScopeEndNode(loop) ;
        (void) loop->SetScopeEnd(loop_end) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of loop node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process each case item:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse the loop statement
        TraverseNode(while_loop.GetStmt()) ;
        // For loop body (true branch):
        (void) loop->AddBranch(0, _parent_node) ;
        AddMyParent(_current_node, loop) ;

        // For other branch(false): out of loop body:
        (void) loop->AddBranch(0, loop_end) ;

        // Treate the continue statements
        // Whenever get a continue satetment node,
        // set the immediate loop node as its child
        unsigned ai ;
        VerificCFGRootNode *node ;
        FOREACH_ARRAY_ITEM(_continue_nodes, ai, node) {
            if (!node) continue ;
            (void) node->AddChild(_loop_node) ;
        }
        // Reset the array after each loops
        _continue_nodes->Reset() ;

        // Keep a link between the loop node and
        // the scope end node:
        _current_node = loop_end ;

        // Restore the original parent node:
        _parent_node = orig_parent_node ;

        // Restore the previous loop node:
        _loop_node = prev_loop_node ;
        _scope = prev_scope ;
    }
}

// Tranverse the VeriDoWhile statement:
void VeriFsmProcessVisitor::VERI_VISIT(VeriDoWhile, do_while)
{
    // Check for event control/trigger within the loop block:
    VeriFsmEventVisitor visitor ;
    do_while.Accept(visitor) ;

    // If no event control within the loop block
    // store the whole block in the arr of statement node
    // othewise create a new loop node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, do_while) ;
    } else {
        // Keep the previous scope
        VeriScope *prev_scope = _scope ;
        _scope = do_while.GetScope() ;

        // Create a loop node:
        VerificCFGConditionalStatementNode *loop = new VerificCFGConditionalStatementNode() ;

        // Store the previous loop node and set the current
        // loop node to _loop_node for treating break/continuous statements:
        VerificCFGRootNode *prev_loop_node = _loop_node ;
        _loop_node = loop ;

        VerificCFGScopeEndNode *loop_end = new VerificCFGScopeEndNode(loop) ;
        (void) loop->SetScopeEnd(loop_end) ;

        // Traverse the loop statement
        // The body of the loop executed at least once before nay condition check:
        TraverseNode(do_while.GetStmt()) ;

        AddMyParent(_current_node, loop) ;
        _current_node = loop ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Add conditions:
        loop->SetConditionExpression((void*)do_while.GetCondition()) ;

        VERIFIC_ASSERT(loop == _current_node) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of loop node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process each case item:
        // Keep the original node current and parent nodes:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse the loop statement
        TraverseNode(do_while.GetStmt()) ;
        // For other loop body:
        (void) loop->AddBranch(0, _parent_node) ;
        AddMyParent(_current_node, loop) ;

        // Treate the continue statements
        // Whenever get a continue satetment node,
        // set the immediate loop node as its child
        unsigned ai ;
        VerificCFGRootNode *node ;
        FOREACH_ARRAY_ITEM(_continue_nodes, ai, node) {
            if (!node) continue ;
            (void) node->AddChild(_loop_node) ;
        }
        // Reset the array after each loop
        _continue_nodes->Reset() ;

        // For other branch(out of loop body):
        (void) loop->AddBranch(0, loop_end) ;

        // Keep a link between the loop node and
        // the scope end node:
        _current_node = loop_end ;

        // Restore the original parent node:
        _parent_node = orig_parent_node ;

        // Restore the previous loop node:
        _loop_node = prev_loop_node ;
        _scope = prev_scope ;
    }
}

// Traverse forever statement
void VeriFsmProcessVisitor::VERI_VISIT(VeriFor, for_loop)
{
    // Check for event control/trigger within the loop block:
    VeriFsmEventVisitor visitor ;
    for_loop.Accept(visitor) ;

    // If no event control within the loop block
    // store the whole block in the arr of statement node
    // othewise create a new loop node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, for_loop) ;
    } else {
        // keep the previous scope
        VeriScope *prev_scope = _scope ;
        _scope = for_loop.GetScope() ;

        // Traverse initial assignment
        // VIPER #7023: Add loop variable declarations declared within loop into
        // _decl_node and add those variables to the newly created always scope
        VeriMapForCopy old_2_new(POINTER_HASH) ;
        unsigned i ;
        VeriModuleItem *item ;
        FOREACH_ARRAY_ITEM(for_loop.GetInitials(), i, item) {
            if (!item) continue ;
            if (item->IsDataDecl()) { // loop variable declarations declared
                // within loop ie: for(int i=0;i<2;i++)
                if (_decl_node) (void) _decl_node->AddDeclaration(item, _scope) ;
                Array *ids = item->GetIds() ;
                unsigned j ;
                VeriIdDef *id ;
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    if (!id) continue ;
                    VeriExpression *init_expr = id->GetInitialValue() ;
                    VeriExpression *right_expr = init_expr ? init_expr->CopyExpression(old_2_new) : 0 ;
                    VeriIdRef *left_expr = new VeriIdRef(id) ;
                    VeriBlockingAssign *init_stmt = new VeriBlockingAssign(left_expr, 0, right_expr) ;
                    if (_newly_created_arr) _newly_created_arr->InsertLast(init_stmt) ; // store in the array for deletion
                    TraverseNode(init_stmt) ; // create statement node for assignment statement
                }
            } else {
                // loop variable declarations declared outside the loop
                // ie. int i=0; for(i=0;i<2;i++)
                TraverseNode(item) ; // create statement node for assignment statement
            }
        }

        // Check the condition of the for loop
        VeriExpression *expr = for_loop.GetCondition() ;
        VeriExpression *right_expr = expr ? expr->GetRight() : 0 ;
        // Get the integer value of right expression
        int range = right_expr ? right_expr->Integer() : 0 ;
        // For constant bound unroll the for loop
        if (right_expr && right_expr->IsInt() && range <= THRESHOLD_FOR_UNROLLING) {
            VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
            AddMyParent(_current_node, statement) ;
            _current_node = statement ;

            // Set the parent node if there is no parent node:
            if (!_parent_node) _parent_node = _current_node ;

            VERIFIC_ASSERT(statement == _current_node) ;

            // Loop end node
            VerificCFGScopeEndNode *scope_end_node = new VerificCFGScopeEndNode(statement) ;
            (void) statement->SetScopeEnd(scope_end_node) ;

            // Store the previous loop node and set the current
            // loop node to _loop_node:
            VerificCFGRootNode *prev_loop_node = _loop_node ;
            _loop_node = statement ;

            // The loop iterates over range times
            int j ;
            for (j=0; j<range; j++) {
                // Keep the current and parent node:
                VerificCFGRootNode *last_node = _current_node ;
                VerificCFGRootNode *orig_parent_node = _parent_node ;

                // Create incremental node
                _current_node = 0 ;
                _parent_node = 0 ;
                // Traverse step assignment
                TraverseArray(for_loop.GetRepetitions()) ;
                VerificCFGRootNode *incr_stmt_node = _current_node ;

                // Retrive the last node and parent node
                _current_node = last_node ;
                _parent_node = orig_parent_node ;

                // Traverse the loop statement
                TraverseNode(for_loop.GetStmt()) ;

                last_node = _current_node ;
                _current_node = incr_stmt_node ;

                if (_current_node && last_node) {
                    (void) last_node->AddChild(_current_node) ;
                }

                // Treate the continue statements
                // Whenever get a continue satetment node,
                // set the immediate loop node as its child
                unsigned ai ;
                VerificCFGRootNode *node ;
                FOREACH_ARRAY_ITEM(_continue_nodes, ai, node) {
                    if (!node) continue ;
                    if (incr_stmt_node) {
                        (void) node->AddChild(incr_stmt_node) ;
                    } else {
                        (void) node->AddChild(statement) ;
                    }
                }
                // Reset the array after each loop
                _continue_nodes->Reset() ;
            }

            // Out of loop body:
            AddMyParent(_current_node, scope_end_node) ;
            _current_node = scope_end_node ;

            // Restore the previous loop node:
            _loop_node = prev_loop_node ;
            _scope = prev_scope ;
        } else {
            // Create a loop node
            VerificCFGConditionalStatementNode *loop = new VerificCFGConditionalStatementNode() ;
            AddMyParent(_current_node, loop) ;
            _current_node = loop ;

            // Set the parent node if there is no parent node:
            if (!_parent_node) _parent_node = _current_node ;

            // Store the previous loop node and set the current
            // loop node to _loop_node:
            VerificCFGRootNode *prev_loop_node = _loop_node ;
            _loop_node = loop ;

            // Add conditions:
            loop->SetConditionExpression((void*)for_loop.GetCondition()) ;

            VERIFIC_ASSERT(loop == _current_node) ;

            VerificCFGScopeEndNode *loop_end = new VerificCFGScopeEndNode(loop) ;
            (void) loop->SetScopeEnd(loop_end) ;

            // Keep the parent node:
            VerificCFGRootNode *orig_parent_node = _parent_node ;

            // For each branch of loop node create a fresh parse tree
            // keeping the _parent_node and _current_node in stack and make
            // each of them null before going to process each case item:
            // Keep the original node current and parent nodes:
            _current_node = 0 ;
            _parent_node = 0 ;

            // Traverse the loop statement
            TraverseNode(for_loop.GetStmt()) ;

            // Inside loop body:
            (void) loop->AddBranch(0, _parent_node) ;
            VerificCFGRootNode *last_node = _current_node ;

            _current_node = 0 ;
            _parent_node = 0 ;

            // Traverse step assignment
            TraverseArray(for_loop.GetRepetitions()) ;
            VerificCFGRootNode *incr_stmt_node = _current_node ;

            if (_current_node) {
                (void) last_node->AddChild(_current_node) ;
                AddMyParent(_current_node, loop) ;
            } else {
                AddMyParent(last_node, loop) ;
            }

            // Treate the continue statements
            // Whenever get a continue satetment node,
            // set the immediate loop node as its child
            unsigned ai ;
            VerificCFGRootNode *node ;
            FOREACH_ARRAY_ITEM(_continue_nodes, ai, node) {
                if (!node) continue ;
                if (incr_stmt_node) {
                    (void) node->AddChild(incr_stmt_node) ;
                } else {
                    (void) node->AddChild(loop) ;
                }
            }
            // Reset the array after each loop
            _continue_nodes->Reset() ;

            // Out of loop body:
            (void) loop->AddBranch(0, loop_end) ;

            // Keep a link between the loop node and
            // the scope end node:
            _current_node = loop_end ;
            // Restore the original parent node:
            _parent_node = orig_parent_node ;

            // Restore the previous loop node:
            _loop_node = prev_loop_node ;
            _scope = prev_scope ;
        }
    }
}

// Traverse forever statement
void VeriFsmProcessVisitor::VERI_VISIT(VeriForever, for_ever)
{
    // Check for event control/trigger within the forever block:
    VeriFsmEventVisitor visitor ;
    for_ever.Accept(visitor) ;

    // If no event control within the loop block
    // store the whole block in the arr of statement node
    // othewise create a new loop node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, for_ever) ;
    } else {
        // Keep the previous scope
        VeriScope *prev_scope = _scope ;
        _scope = for_ever.GetScope() ;

        // Create loop node
        VerificCFGConditionalStatementNode *loop = new VerificCFGConditionalStatementNode() ;
        AddMyParent(_current_node, loop) ;
        _current_node = loop ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Store the previous loop node and set the current
        // loop node to _loop_node:
        VerificCFGRootNode *prev_loop_node = _loop_node ;
        _loop_node = loop ;

        VERIFIC_ASSERT(loop == _current_node) ;

        VerificCFGScopeEndNode *scope_end = new VerificCFGScopeEndNode(loop) ;
        (void) _current_node->SetScopeEnd(scope_end) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of loop node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process each case item:
        // Keep the original node current and parent nodes:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse the loop statement
        TraverseNode(for_ever.GetStmt()) ;
        (void) loop->AddBranch(0, _parent_node) ;
        AddMyParent(_current_node, loop) ;

        // Treate the continue statements
        // Whenever get a continue satetment node,
        // set the immediate loop node as its child
        unsigned ai ;
        VerificCFGRootNode *node ;
        FOREACH_ARRAY_ITEM(_continue_nodes, ai, node) {
            if (!node) continue ;
            (void) node->AddChild(loop) ;
        }
        // Reset the array after each loop
        _continue_nodes->Reset() ;

        (void) loop->AddBranch(0, scope_end) ;

        _current_node = scope_end ;
        // Restore the original parent node:
        _parent_node = orig_parent_node ;
        // Restore the previous loop node:
        _loop_node = prev_loop_node ;
        // Restore the previous scope
        _scope = prev_scope ;
    }
}

// Traverse repeat statement:
void VeriFsmProcessVisitor::VERI_VISIT(VeriRepeat, repeat)
{
    // Check for event control/trigger within the loop block:
    VeriFsmEventVisitor visitor ;
    repeat.Accept(visitor) ;

    // If no event control within the loop block
    // store the whole block in the arr of statement node
    // othewise create a new loop node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, repeat) ;
    } else {
        // Keep the previous scope
        VeriScope *prev_scope = _scope ;
        _scope = repeat.GetScope() ;

        VeriExpression *condition = repeat.GetCondition() ;

        VeriMapForCopy old_2_new(POINTER_HASH) ;

        // Repeat var declaration:
        char *prefix = VeriFsmAlwaysVisitor::CreateVariableName("repeat_var", 0, _scope) ;
        VeriIdDef *repeat_id = new VeriVariable(prefix) ;
        repeat_id->SetLinefile(repeat.Linefile()) ;

        // VIPER #7338: Create proper datatype for repeat variables:
        int cond_val = VeriFsmAlwaysVisitor::GetExprSize(condition) ;

        unsigned size = GET_CONTEXT_SIZE(cond_val) ; // init-val size
        if (size>0) size = size-1 ;

        unsigned sign = GET_CONTEXT_SIGN(cond_val) ; // init-val sign
        unsigned signing = sign ? VERI_SIGNED : VERI_UNSIGNED ;

        VeriExpression *left = new VeriIntVal((int)size) ;
        VeriExpression *right = new VeriIntVal(0) ;
        VeriRange *range = new VeriRange(left, right) ;
        range->SetLinefile(repeat.Linefile()) ;

        VeriDataType *data_type = new VeriDataType(VERI_REG, signing, range) ;
        VeriDataDecl *data_decl = new VeriDataDecl(VERI_REG, data_type, repeat_id) ;
        data_decl->SetLinefile(repeat.Linefile()) ;
        if (_decl_node) (void) _decl_node->AddDeclaration(data_decl, 0) ;
        if (_temp_array) _temp_array->InsertLast(data_decl) ;

        VeriIdRef *repeat_var = new VeriIdRef(repeat_id) ;
        VeriExpression *cond_expr = condition ? condition->CopyExpression(old_2_new) : 0 ;
        VeriBlockingAssign *stmt = new VeriBlockingAssign(repeat_var->CopyExpression(old_2_new), 0, cond_expr) ;
        stmt->SetLinefile(repeat.Linefile()) ;
        if (_newly_created_arr) _newly_created_arr->InsertLast(stmt) ;
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, *stmt) ; // Add the statement to the array of statement node

        VerificCFGConditionalStatementNode *loop = new VerificCFGConditionalStatementNode() ;
        AddMyParent(_current_node, loop) ;
        _current_node = loop ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Add conditions:
        // Need to create the id_ref with id_def instead of name
        // then we may be able to get rid of the resolve call:
        VeriBinaryOperator *repeat_condition = new VeriBinaryOperator(VERI_GT, repeat_var->CopyExpression(old_2_new), right->CopyExpression(old_2_new)) ;
        repeat_condition->SetLinefile(repeat.Linefile()) ;
        if (_newly_created_arr) _newly_created_arr->InsertLast(repeat_condition) ;
        loop->SetConditionExpression(repeat_condition) ;

        VERIFIC_ASSERT(loop == _current_node) ;

        VerificCFGScopeEndNode *loop_end = new VerificCFGScopeEndNode(loop) ;
        (void) loop->SetScopeEnd(loop_end) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of loop node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process each case item:
        // Keep the original node current and parent nodes:
        VerificCFGRootNode *orig_current_node = _current_node ;
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse the loop statement
        TraverseNode(repeat.GetStmt()) ;

        VeriBinaryOperator *decrement_expr = new VeriBinaryOperator(VERI_MIN, repeat_var, new VeriIntVal(1));
        decrement_expr->SetLinefile(repeat.Linefile()) ;
        VeriBlockingAssign *decrement_stmt = new VeriBlockingAssign(repeat_var->CopyExpression(old_2_new), 0, decrement_expr) ;
        decrement_stmt->SetLinefile(repeat.Linefile()) ;
        if (_newly_created_arr) _newly_created_arr->InsertLast(decrement_stmt) ;
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, *decrement_stmt) ; // Add the statement to the array of statement node

        // Loop body:
        (void) loop->AddBranch(0, _parent_node) ;
        AddMyParent(_current_node, orig_current_node) ;

        // Out of loop body:
        (void) loop->AddBranch(0, loop_end) ;

        // Keep a link between the loop node and
        // the scope end node:
        _current_node = loop_end ;

        // Restore the original parent node:
        _parent_node = orig_parent_node ;
        // Restore the previous scope
        _scope = prev_scope ;
    }
}

// Traverse seq_blk statement
void VeriFsmProcessVisitor::VERI_VISIT(VeriSeqBlock, seq_blk)
{
    // Check for event control/trigger within the sequential block:
    VeriFsmEventVisitor visitor ;
    seq_blk.Accept(visitor) ;

    // Create only one declaration node
    if (!_decl_node) _decl_node = new VerificCFGDeclarationNode() ;

    // Store the local declarations
    unsigned ai ;
    VeriDataDecl *data_decl ;
    FOREACH_ARRAY_ITEM(seq_blk.GetDeclItems(), ai, data_decl) {
        // if the data declaration is already added to decl node,
        // no need to add that decl again.
        if (!_decl_map.Insert(data_decl, seq_blk.GetScope())) continue ;
        (void) _decl_node->AddDeclaration(data_decl, seq_blk.GetScope()) ;
    }

    // If no event control within the sequential block
    // then visit the VeriStatement visitor routine and
    // store the whole block in the arr of statement node
    // othewise traverse the array of statements:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, seq_blk) ;
    } else {
        // Keep the previous scope
        VeriScope *prev_scope = _scope ;
        _scope = seq_blk.GetScope() ;

        // Get the _current_node:
        VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
        AddMyParent(_current_node, statement) ;
        _current_node = statement ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Scope end node
        VerificCFGScopeEndNode *scope_end = new VerificCFGScopeEndNode(_current_node) ;
        (void) _current_node->SetScopeEnd(scope_end) ;
        VerificCFGRootNode *prev_seq_blk_end = _seq_blk_end ;
        _seq_blk_end = scope_end ;

        // For disabling a loop we store label_id vrs scope end node
        // of the loop node in the map '_seq_blk_map':
        VeriIdDef *label_id = seq_blk.GetLabel() ;
        if (label_id) (void) _seq_blk_map.Insert(label_id, scope_end) ;

        // Traverse the block statements
        TraverseArray(seq_blk.GetStatements()) ;

        AddMyParent(_current_node, scope_end) ;
        _current_node = scope_end ;
        _seq_blk_end = prev_seq_blk_end ;
        // Restore the previous scope
        _scope = prev_scope ;
    }
}

// Traverse disable statements:
void VeriFsmProcessVisitor::VERI_VISIT(VeriDisable, disable)
{
    // Create a dummy statement node:
    VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
    AddMyParent(_current_node, statement) ;
    _current_node = statement ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    VeriName *name = disable.GetTaskBlockName() ;
    if (name) { // named block
        // Disable from that named block as specified in the disable statement
        VeriIdDef *name_id = name->GetId() ;
        VerificCFGRootNode *seq_blk_end = (VerificCFGRootNode*)_seq_blk_map.GetValue(name_id) ;
        (void) _current_node->AddChild(seq_blk_end) ;
        _current_node = 0 ;
    } else { // disabled from the immediate sequential block
        (void) _current_node->AddChild(_seq_blk_end) ;
        _current_node = 0 ;
    }
}

// Traverse jump statements:
void VeriFsmProcessVisitor::VERI_VISIT(VeriJumpStatement, jump)
{
    // Create a dummy statement node:
    VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
    AddMyParent(_current_node, statement) ;
    _current_node = statement ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    VerificCFGRootNode *scope_end = _loop_node ? _loop_node->GetScopeEnd() : 0 ;
    unsigned token = jump.GetJumpToken() ;
    if (token == VERI_BREAK) { // jump to the loop end node
        (void) _current_node->AddChild(scope_end) ;
        _current_node = 0 ;
    } else if (token == VERI_CONTINUE) { // store the statement nodes in an array
        _continue_nodes->InsertLast(_current_node) ;
        _current_node = 0 ;
    }
}

// Tranverse the Assertion statements
void VeriFsmProcessVisitor::VERI_VISIT(VeriAssertion, assertion)
{
    // Check for event control/trigger within the conditional block:
    VeriFsmEventVisitor visitor ;
    assertion.Accept(visitor) ;

    // If no event control within the conditional block
    // store the whole block in the arr of statement node
    // othewise create a new conditional node:
    if (!visitor.HasEvent() && !visitor.HasJumpOrDisable() && !visitor.HasLocalBlock()) {
        VeriFsmProcessVisitor::VERI_VISIT_NODE(VeriStatement, assertion) ;
    } else {
        // Create a conditional node
        VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
        AddMyParent(_current_node, cond) ;
        _current_node = cond ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Add conditions:
        cond->SetConditionExpression((void*)assertion.GetPropertySpec()) ;

        VERIFIC_ASSERT(cond == _current_node) ;

        // scope end node:
        VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond) ;
        (void) cond->SetScopeEnd(cond_end) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of conditional node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process then and else part:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse then statement
        TraverseNode(assertion.GetThenStmt()) ;
        (void) cond->AddBranch(0, _parent_node) ;
        AddMyParent(_current_node, cond_end) ;

        // Again make the _current_node and
        // the _parent_node set to null:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse else statement
        TraverseNode(assertion.GetElseStmt()) ;
        (void) cond->AddBranch(0, _parent_node) ;
        if (_current_node) {
            // Has else part
            AddMyParent(_current_node, cond_end) ;
        } else { // no else part
            AddMyParent(cond, cond_end) ;
        }

        // Keep a link between the conditional node and
        // the scope end node:
        _current_node = cond_end ;
        // Restore the original parent node:
        _parent_node = orig_parent_node ;
    }
}

/*-----------------------------------------------------------------*/
//      VeriFsmBlkLabelVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VeriFsmBlkLabelVisitor::VeriFsmBlkLabelVisitor(VeriScope *scope, VeriMapForCopy &old2new):
    VeriVisitor(),
    _old2new(&old2new),
    _scope(scope)
{
}

VeriFsmBlkLabelVisitor::~VeriFsmBlkLabelVisitor()
{
    _old2new = 0 ;
    _scope = 0 ;
}

/*-----------------------------------------------------------------*/
//    Visit routines of VeriFsmBlkLabelVisitor
/*-----------------------------------------------------------------*/

// VIPER #7021: Visitor of VeriFsmBlkLabelVisitor overwrite VeriStatement, VeriSeqBlock
// and VeriParBlock visitor routine and create new label id for unnamed block and declare
// the id in the given scope, for unnamed block traverse the block statements.
// For statement:
//     blk: for (int i=0; i<2; i++)
//         begin
//             begin :blk1
//             end
//         end
// Visit routine on VeriStatement will be called and a new label id will be created and
// declared in the given scope, then for blk1 visit routine on sequential block will be called.
void VeriFsmBlkLabelVisitor::VERI_VISIT(VeriStatement, stmt)
{
    if (!_scope) return ;

    VeriIdDef *label_id = 0 ;
    // Get label
    if (stmt.GetOpeningLabel()) {
        label_id = stmt.GetOpeningLabel() ;
    }

    if (label_id) {
        const char *label_name = label_id->Name() ;
        // If name conflicts, create new name
        char *new_label_name = VeriFsmAlwaysVisitor::CreateVariableName(label_name, 0, _scope) ;
        // VIPER #8303: Consider scope of label too
        VeriScope *label_scope = label_id->GetScope() ;
        VeriIdDef *new_label_id = (label_scope) ? new VeriLabelId(new_label_name): new VeriBlockId(new_label_name) ;
        if (_old2new) (void) _old2new->Insert(label_id, new_label_id, 1, 0) ; // VIPER #7957: do force over write
        new_label_id->SetLinefile(label_id->Linefile()) ;
        (void) _scope->DeclareBlockId(new_label_id) ;
    }

    // Call of Base class Visit
    VeriVisitor::VERI_VISIT_NODE(VeriStatement, stmt) ;
}

// For sequential block:
//     begin
//         begin blk
//             begin blk1
//                 x=1 ;
//             end
//         end
//     end
// At first create a new label id for blk and declare it in the upper scope. For blk1 we need not
// to create new label id as it is local to a named block blk.
void VeriFsmBlkLabelVisitor::VERI_VISIT(VeriSeqBlock, seq_blk)
{
    if (!_scope) return ;

    VeriScope *scope = seq_blk.GetScope() ;
    VeriIdDef *label_id = 0 ;
    // Get label
    if (seq_blk.GetLabel()) {
        label_id = seq_blk.GetLabel() ;
    } else if (seq_blk.GetOpeningLabel()) {
        label_id = seq_blk.GetOpeningLabel() ;
    }

    if (label_id) {
        const char *label_name = label_id->Name() ;
        // If name conflicts, create new name
        char *new_label_name = VeriFsmAlwaysVisitor::CreateVariableName(label_name, 0, _scope) ;
        VeriIdDef *new_label_id = 0 ;
        // VIPER #8303: Consider scope of label too
        VeriScope *label_scope = label_id->GetScope() ;
        if (label_scope) {
            new_label_id = new VeriLabelId(new_label_name) ;
        } else {
            new_label_id = new VeriBlockId(new_label_name) ;
        }
        if (_old2new) (void) _old2new->Insert(label_id, new_label_id, 1, 0) ; // VIPER #7957: do force over write
        //VeriIdDef *new_label_id = new VeriBlockId(new_label_name) ;
        //if (_old2new) (void) _old2new->Insert(label_id, new_label_id, 1, 0) ; // VIPER #7957: do force over write
        new_label_id->SetLinefile(label_id->Linefile()) ;
        (void) _scope->DeclareBlockId(new_label_id) ;
    }

    if (scope && scope->IsEmptyScope()) TraverseArray(seq_blk.GetStatements()) ;
}

// For parallel block:
void VeriFsmBlkLabelVisitor::VERI_VISIT(VeriParBlock, par_blk)
{
    if (!_scope) return ;

    VeriScope *scope = par_blk.GetScope() ;
    VeriIdDef *label_id = 0 ;
    // Get label
    if (par_blk.GetLabel()) {
        label_id = par_blk.GetLabel() ;
    } else if (par_blk.GetOpeningLabel()) {
        label_id = par_blk.GetOpeningLabel() ;
    }

    if (label_id) {
        const char *label_name = label_id->Name() ;
        // If name conflicts, create new name
        char *new_label_name = VeriFsmAlwaysVisitor::CreateVariableName(label_name, 0, _scope) ;
        VeriIdDef *new_label_id = 0 ;
        // VIPER #8303: Consider scope of label too
        VeriScope *label_scope = label_id->GetScope() ;
        if (label_scope) {
            new_label_id = new VeriLabelId(new_label_name) ;
        } else {
            new_label_id = new VeriBlockId(new_label_name) ;
        }
        if (_old2new) (void) _old2new->Insert(label_id, new_label_id, 1, 0) ; // VIPER #7957: do force over write
        //VeriIdDef *new_label_id = new VeriBlockId(new_label_name) ;
        //if (_old2new) (void) _old2new->Insert(label_id, new_label_id, 0, 1) ;
        new_label_id->SetLinefile(label_id->Linefile()) ;
        (void) _scope->DeclareBlockId(new_label_id) ;
    }

    if (scope && scope->IsEmptyScope()) TraverseArray(par_blk.GetStatements()) ;
}

// Count the total number of event controls in an always/initial construct
void VeriFsmGenerateVisitor::CountStateNode(VerificCFGRootNode *node, Array &state_count, Set *visited, Set &state_set, Map &state_map, Map &duplicate_state_map)
{
    if (!node) return ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && visited->GetItem(node)) return ;

    Set visited_set(POINTER_HASH) ;
    if (!visited) visited = &visited_set ;
    (void) visited->Insert(node) ;

#ifdef OPTIMIZE_EXPLICIT_STATE_MACHINE
    // VIPER #7031: For state node check for all its children
    // if any two state nodes are required to be merged
    if (node->IsStateNode()) {
        SetIter si ;
        VerificCFGRootNode *child ;
        FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
            MergeDuplicateStateNodes(node, child, state_map, duplicate_state_map) ;
        }
    }
#endif

    // Count the states: store the state nodes in an array
    if (node->IsStateNode()) {
        // VIPER #7031: Mark the traversed state node: Set 'state_set' store the state number(increased by 1,
        // as we can have state '0', and as '0' cannot be inserted to a set, we store state number by increasing
        // its value by 1 and will be retrived by decreasing its value by 1 when required).
        // Whenever a state number of a state is already stored in state_set, we do not
        // store the state in the set state_count.
        if (state_set.Insert((void*)(unsigned long)(node->GetState()+1))) {
            state_count.Insert(node) ;
        }
    }

    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
        CountStateNode(child, state_count, visited, state_set, state_map, duplicate_state_map) ;
    }
}

// VIPER #7031: Megre duplicate state nodes: if a particular node is
// child of two state node, we can merge that two state nodes.
// eg: @(posedge clk)
//     count = 0 ;
//     if (cond > 2) begin
//         count++ ;
//         @(posedge clk) ;
//     end
//     else (cond > 2) begin
//         count++ ;
//         @(posedge clk) ;
//     end
// Here the last statement of both if and else branch is a clocking statement ie. state node.
// and both the state node is parent of statement node 'count=0', so we merge the two state
// node into one state node.
void VeriFsmGenerateVisitor::MergeDuplicateStateNodes(VerificCFGRootNode *start, VerificCFGRootNode *node, Map &state_map, Map &duplicate_state_map)
{
    if (!node) return ;

    SetIter si ;
    // Go to the actual child node after traversing the dummy end nodes
    if (node->IsScopeEndNode()) {
        VerificCFGRootNode *child ;
        FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
            MergeDuplicateStateNodes(start, child, state_map, duplicate_state_map) ;
        }
    } else if (node->IsSimpleStatementNode() && !node->IsStateNode() && !node->GetStatementList()) {
        // VIPER #8282: Penetrate through transperant statement nodes while merging state nodes
        VerificCFGRootNode *child ;
        FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
            MergeDuplicateStateNodes(start, child, state_map, duplicate_state_map) ;
        }
    } else { // Find the node in the map, if exists set the state of the
        // present state node with the same of the existing one.
        if (state_map.GetItem(node)) {
            VerificCFGRootNode *exist = (VerificCFGRootNode*)state_map.GetValue(node) ;
            if (exist && start) {
                start->SetState(exist->GetState()) ;
                // VIPER #7034: Add all the duplicate nodes against state node exist
                (void) duplicate_state_map.Insert(exist, start, 0, 1) ;
            }
        } else { // if not found store the node vrs the state node in state_map
            (void) state_map.Insert(node, start) ;
        }
    }
}

// VIPER #7034: Renumber the state nodes
// This routine number all the state nodes of the CFG beginning from 0 as 0,1,2.....
// Previously we have some situations where we have state nodes numbered as 0,2,1.
// now these states will be renumbered as 0,1,2. We also can have some situations
// where we have states 0,1,3 (after merging duplicate state nodes:VIPER#7031),
// in these cases we will renumber the state nodes as 0,1,2.
// When renumbering the state nodes of the array 'state_count' we also renumber all
// the duplicate state nodes stored in the duplicate_state_map.
void VeriFsmGenerateVisitor::ReNumberStateNode(Array &state_count, Map &duplicate_state_map)
{
    unsigned state_num = 0 ;
    unsigned ai ;
    VerificCFGRootNode *node1 ;
    FOREACH_ARRAY_ITEM(&state_count, ai, node1) {
        if (!node1) continue ;
        node1->SetState(state_num++) ;
        if (node1->GetState() == 0) _init_state_node = node1 ;

        if (!duplicate_state_map.Size()) continue ; // no duplicate state nodes
        // Renumber the duplicate state nodes against node1
        MapItem *item ;
        VerificCFGRootNode *node2 ;
        FOREACH_SAME_KEY_MAPITEM(&duplicate_state_map, node1, item) {
            node2 = (VerificCFGRootNode*)item->Value() ;
            if (!node2) continue ;
            node2->SetState(node1->GetState()) ;
        }
    }
}

// Copy the statements of the statement node and call ExplicitStateMachine routine on its child and GenerateCaseState routine if the child is a state node:
void
VeriFsmGenerateVisitor::ExplicitStateMachine(VerificCFGRootNode *node, const VerificCFGRootNode *start, Map &state_map, Map *internal_event_map, Map &mutually_exclusive_event_map, Array *expr_arr, Array *stmts, VeriScope *scope, VeriMapForCopy &old2new)
{
    if (!stmts || !node) return ;

    // If this node is visited more than once in the same event clock
    // then there shold be an infinite loop:
    MapItem *item ;
    VerificCFGRootNode *exist ;
    FOREACH_SAME_KEY_MAPITEM(&state_map, node, item) {
        exist = (VerificCFGRootNode*)item->Value() ;
        if (exist==start) {
            _infinite_loop = 1 ;
            return ;
        }
    }
    (void) state_map.Insert(node, start, 0, 1) ;

    // For state node:
    if (node->IsStateNode() && expr_arr) { //JJ: cleaned memleak by rearranging conditional (don't allocate for expr_arr unless we have expr_arr
        VeriExpression *state = new VeriIntVal((int)node->GetState()) ;
        expr_arr->InsertLast(state) ;
    }

    // For statement node:
    if (node->IsSimpleStatementNode()) {
        MapIter mi ;
        VeriStatement *stmt ;
        VeriScope *stmt_scope ;
        FOREACH_MAP_ITEM(node->GetStatementList(), mi, &stmt, &stmt_scope) {
            if (stmt) {
                // VIPER #7021: If no event control within a block, we store the whole
                // block in the statement node: so when we are going to copy that statement
                // (if it contains label) we need to declare the label id in the first named
                // block and also add the old vrs new label id in a new map temp(also add all
                // the items of old2new map in temp).
                VeriMapForCopy temp ;
                MapIter mii ;
                VeriTreeNode *id1, *id2 ;
                FOREACH_MAP_ITEM(&old2new, mii, &id1, &id2) {
                    if (!id1) continue ;
                    (void) temp.Insert(id1, id2) ;
                }

                // VIPER #7021: Visitor of VeriFsmBlkLabelVisitor overwrite VeriStatement, VeriSeqBlock
                // and VeriParBlock visitor routine and create new label id for named block and declare
                // the id in the given scope, for unnamed block traverse the block statements for checking
                // inner named sequential block/statement with label.
                // begin
                //     begin blk
                //         begin blk1
                //             x=1 ;
                //         end
                //     end
                // end
                // At first create a new label id for blk and declare it in the upper scope. For blk1 we need not
                // to create new label id as it is local to a named block blk.
                // blk: for (int i=0; i<2; i++) : create new label id with the opening label for blk and declare
                // in the given scope also.
                VeriFsmBlkLabelVisitor label_visitor(scope, temp) ;
                stmt->Accept(label_visitor) ;

                // Copy the scope in temp map
                if (stmt_scope)  (void) temp.Insert(stmt_scope, scope);

                // Copy statement
                VeriStatement *statement = stmt->CopyStatement(temp) ;
                stmts->InsertLast(statement) ;
            }
        }
    }

    // For state/statement/scope_end nodes
    if (!node->IsConditionalNode()) {
        SetIter si ;
        VerificCFGRootNode *child ;
        FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
            if (child->IsStateNode()) {
                GenerateCaseState(child, stmts) ;
            } else {
                ExplicitStateMachine(child, node, state_map, internal_event_map, mutually_exclusive_event_map, expr_arr, stmts, scope, old2new) ;
            }
        }
    } else { // For conditional nodes
        VeriExpression *cond_expr = (VeriExpression *)node->GetConditionExpression() ;
        VeriExpression *condition = cond_expr ? cond_expr->CopyExpression(old2new) : 0 ;

        unsigned case_stmt = 0 ;
        unsigned mutually_exclusive = 0 ;

        unsigned i ;
        unsigned branch_cond_size = node->NumBranch() ;
        // For case statement the branch size is one or more
        // for loop/if-else statement the branch size is zero
        if (branch_cond_size) case_stmt = 1 ;
        Map new_state_map(POINTER_HASH) ;
        if (case_stmt) {
            Array *case_items = new Array(branch_cond_size) ;
            // For default case the branch enry is zero, so the branch
            // statement size is one more than branch condition size
            for (i=0; i< branch_cond_size+1; i++) {
                new_state_map.Reset() ;
                MapIter mi ;
                VerificCFGRootNode *node1, *node2 ;
                FOREACH_MAP_ITEM(&state_map, mi, &node1, &node2) {
                    (void) new_state_map.Insert(node1, node2, 0, 1) ;
                }

                VeriScope *new_scope = new VeriScope(1/*empty scope*/, scope) ;
                Array *cond_stmt_arr = new Array() ;
                VerificCFGRootNode *statement = (VerificCFGRootNode *)node->GetBranchStatement(i) ;
                if(statement) {
                    if (statement->IsStateNode()) {
                        GenerateCaseState(statement, cond_stmt_arr) ;
                    } else {
                        ExplicitStateMachine(statement, node, new_state_map, internal_event_map, mutually_exclusive_event_map, 0, cond_stmt_arr, new_scope, old2new) ;
                    }
                }

                VeriSeqBlock *seq_case_blck = new VeriSeqBlock(0, 0, cond_stmt_arr, new_scope) ;
                seq_case_blck->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                Array *cond_arr = 0 ;
                if (i<branch_cond_size) cond_arr = node->GetBranchConditions(i) ;

                if (cond_arr) { // For case statement create seq_if_blck block
                    Array *case_conds = new Array(cond_arr->Size()) ;
                    unsigned ai ;
                    VeriExpression *expr ;
                    FOREACH_ARRAY_ITEM(cond_arr, ai, expr) {
                        VeriExpression *expression = expr ? expr->CopyExpression(old2new) : 0 ;
                        case_conds->InsertLast(expression) ;
                    }
                    VeriCaseItem *case_item = new VeriCaseItem(case_conds, seq_case_blck) ;
                    case_item->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                    case_items->InsertLast(case_item) ;
                } else { // For default case statement create seq_else_blck block
                    VeriCaseItem *case_item = new VeriCaseItem(0, seq_case_blck) ;
                    case_item->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                    case_items->InsertLast(case_item) ;
                }
            }
            VeriCaseStatement *case_statement = new VeriCaseStatement(VERI_CASE, condition, case_items) ;
            case_statement->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
            stmts->InsertLast(case_statement) ;
        } else {
            // For if-else/loop:
            VeriSeqBlock *seq_if_blck = 0 ;
            VeriSeqBlock *seq_else_blck = 0 ;
            VerificCFGRootNode *statement = (VerificCFGRootNode *)node->GetBranchStatement(0) ; // if branch
            // If we have 2 conditional node with mutually exclusive conditions, then if the true branch of
            // of first condition is executed then the false branch of the other condition will be executed
            // and vice versa. For this we maintain a map that will contain the other mutually conditional
            // node and its branch through which we will not traverse.
            unsigned branch = 0 ;
            Map new_mutually_exclusive_event_map(POINTER_HASH) ;
            if (!mutually_exclusive_event_map.Size()) {
                if (internal_event_map && internal_event_map->GetItem(node)) {
                    VerificCFGRootNode *other_node = (VerificCFGRootNode*)internal_event_map->GetValue(node) ;
                    (void) new_mutually_exclusive_event_map.Insert(other_node, (void*) (unsigned long)1) ;
                }
            } else {
                if (mutually_exclusive_event_map.GetItem(node)) { // Get the mutually exclusive conditional node
                    branch = (unsigned) ((unsigned long)mutually_exclusive_event_map.GetValue(node)) ;
                } else { //  conditional nodes other than mutually exclusive conditions
                    MapIter mi ;
                    VerificCFGRootNode *node1, *node2 ;
                    FOREACH_MAP_ITEM(&mutually_exclusive_event_map, mi, &node1, &node2) {
                        if (!node1 || !node2) continue ;
                        (void) new_mutually_exclusive_event_map.Insert(node1, node2) ;
                    }
                }
            }

            if (branch == 1) {
                statement = 0 ;
                mutually_exclusive = 1 ;
            }

            if (statement) {
                new_state_map.Reset() ;
                MapIter mi ;
                VerificCFGRootNode *node1, *node2 ;
                FOREACH_MAP_ITEM(&state_map, mi, &node1, &node2) {
                    (void) new_state_map.Insert(node1, node2, 0, 1) ;
                }

                VeriScope *new_scope = new VeriScope(1/*empty scope*/, scope) ;
                Array *cond_stmt_arr = new Array() ;
                if (statement->IsStateNode()) {
                    GenerateCaseState(statement, cond_stmt_arr) ;
                    new_mutually_exclusive_event_map.Reset() ;
                } else {
                    ExplicitStateMachine(statement, node, new_state_map, internal_event_map, new_mutually_exclusive_event_map, 0, cond_stmt_arr, new_scope, old2new) ;
                }

                if (!cond_expr) { // forever has no condition
                    unsigned ai ;
                    VeriStatement *new_stmt ;
                    FOREACH_ARRAY_ITEM(cond_stmt_arr, ai, new_stmt) {
                        if (new_stmt) {
                            stmts->InsertLast(new_stmt) ;
                        }
                    }
                    delete cond_stmt_arr ;
                    delete new_scope ;
                } else {
                    unsigned infinite_loop = 0 ;
                    if (!cond_stmt_arr->Size()) { // infinite_loop
                        cond_stmt_arr->InsertLast(0) ;
                        infinite_loop = 1 ;
                    }
                    seq_if_blck = new VeriSeqBlock(0, 0, cond_stmt_arr, new_scope) ;
                    seq_if_blck->SetLinefile(cond_expr->Linefile()) ;

                    if (infinite_loop) { // print comment for possible infinite loop
                        linefile_type line_file = node->GetLinefile() ;
                        // Create a comment node to store the comments:
                        VeriCommentNode *comment_node = new VeriCommentNode(line_file) ;
                        char *str = Strings::save("/*infinite loop*/") ;
                        comment_node->AppendComment(str) ;
                        Strings::free(str) ;
                        Array *comment_arr = new Array(1) ;
                        comment_arr->InsertLast(comment_node) ;
                        seq_if_blck->AddComments(comment_arr, 1) ;
                    }
                }
            }

            if (cond_expr) { // else part if in-else/loop
                statement = (VerificCFGRootNode *)node->GetBranchStatement(1) ; // else branch
                branch = 0 ;
                new_mutually_exclusive_event_map.Reset() ;
                if (!mutually_exclusive_event_map.Size()) {
                    if (internal_event_map && internal_event_map->GetItem(node)) {
                        VerificCFGRootNode *other_node = (VerificCFGRootNode*)internal_event_map->GetValue(node) ;
                        (void) new_mutually_exclusive_event_map.Insert(other_node, (void*) (unsigned long)2) ;
                    }
                } else {
                    if (mutually_exclusive_event_map.GetItem(node)) {
                        branch = (unsigned) ((unsigned long)mutually_exclusive_event_map.GetValue(node)) ;
                    } else {
                        MapIter mi ;
                        VerificCFGRootNode *node1, *node2 ;
                        FOREACH_MAP_ITEM(&mutually_exclusive_event_map, mi, &node1, &node2) {
                            if (!node1 || !node2) continue ;
                            (void) new_mutually_exclusive_event_map.Insert(node1, node2) ;
                        }
                    }
                }

                if (branch == 2) {
                    statement = 0 ;
                    mutually_exclusive = 1 ;
                }

                if (statement) {
                    new_state_map.Reset() ;
                    MapIter mi ;
                    VerificCFGRootNode *node1, *node2 ;
                    FOREACH_MAP_ITEM(&state_map, mi, &node1, &node2) {
                        (void) new_state_map.Insert(node1, node2, 0, 1) ;
                    }

                    VeriScope *new_scope = new VeriScope(1/*empty scope*/, scope) ;
                    Array *cond_stmt_arr = new Array() ;
                    if (statement->IsStateNode()) {
                        GenerateCaseState(statement, cond_stmt_arr) ;
                        new_mutually_exclusive_event_map.Reset() ;
                    } else {
                        ExplicitStateMachine(statement, node, new_state_map, internal_event_map, new_mutually_exclusive_event_map, 0, cond_stmt_arr, new_scope, old2new) ;
                    }

                    unsigned infinite_loop = 0 ;
                    if (!cond_stmt_arr->Size()) { // infinite_loop
                        cond_stmt_arr->InsertLast(0) ;
                        infinite_loop = 1 ;
                    }
                    seq_else_blck = new VeriSeqBlock(0, 0, cond_stmt_arr, new_scope) ;
                    seq_else_blck->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                    if (infinite_loop) { // print comment for possible nfinite loop
                        linefile_type line_file = node->GetLinefile() ;
                        // Create a comment node to store the comments:
                        VeriCommentNode *comment_node = new VeriCommentNode(line_file) ;
                        char *str = Strings::save("/*infinite loop*/") ;
                        comment_node->AppendComment(str) ;
                        Strings::free(str) ;
                        Array *comment_arr = new Array(1) ;
                        comment_arr->InsertLast(comment_node) ;
                        seq_else_blck->AddComments(comment_arr, 1) ;
                    }
                } else {
                    if (!mutually_exclusive) {
                        Array *cond_stmt_arr = new Array() ;
                        VeriScope *new_scope = new VeriScope(1/*empty scope*/, scope) ;
                        // For if without else we generate the seq_else_blck with the scope end node of the conditional node
                        VerificCFGRootNode *scope_end = node->GetScopeEnd() ;
                        ExplicitStateMachine(scope_end, node, new_state_map, internal_event_map, mutually_exclusive_event_map, 0, cond_stmt_arr, new_scope, old2new) ;
                        VERIFIC_ASSERT(!seq_else_blck) ;

                        unsigned infinite_loop = 0 ;
                        if (!cond_stmt_arr->Size()) { // infinite_loop
                            cond_stmt_arr->InsertLast(0) ;
                            infinite_loop = 1 ;
                        }
                        seq_else_blck = new VeriSeqBlock(0, 0, cond_stmt_arr, new_scope) ;
                        seq_else_blck->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                        if (infinite_loop) { // print comment for possible nfinite loop
                            linefile_type line_file = node->GetLinefile() ;
                            // Create a comment node to store the comments:
                            VeriCommentNode *comment_node = new VeriCommentNode(line_file) ;
                            char *str = Strings::save("/*infinite loop*/") ;
                            comment_node->AppendComment(str) ;
                            Strings::free(str) ;
                            Array *comment_arr = new Array(1) ;
                            comment_arr->InsertLast(comment_node) ;
                            seq_else_blck->AddComments(comment_arr, 1) ;
                        }
                    }
                }

                VeriConditionalStatement *cond_stmt = new VeriConditionalStatement(condition, seq_if_blck, seq_else_blck) ;
                cond_stmt->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                stmts->InsertLast(cond_stmt) ;
            }
        }
    }
}

// Whenver visit a state node create a non blocking assignment statement and store it in the stmts array:
void
VeriFsmGenerateVisitor::GenerateCaseState(VerificCFGRootNode *node, Array *stmts)
{
    if (!stmts) return ;

    // Get the id-ref from the id-def:
    //VERIFIC_ASSERT(_var_id) ;
    if (!_var_id) return ;

    VeriIdRef *l_val = new VeriIdRef(_var_id) ;

    unsigned state_num = node->GetState() ;
    VeriExpression *val = new VeriIntVal((int)state_num) ;

    VeriNonBlockingAssign *stmt = new VeriNonBlockingAssign(l_val, 0, val) ;
    stmt->SetLinefile(node->GetLinefile()) ;
    stmts->InsertLast(stmt) ;
}

/*-----------------------------------------------------------------*/
//               Generate ExplicitStateMachine
/*-----------------------------------------------------------------*/

// Algorithm:
// 1) Traverse the parse tree with VeriFsmAlwaysVisitor.
// 2) For always/initial construct:
//       - check for some limitations and conditions.
//       - if conditions are fulfilled do the following:
//             1. Generate control flow graph with a new visitor VeriFsmProcessVisitor.
//                   1) For generating CFG we need to declare 4 classes- statement node for
//                      plain statements, state node for event statements, conditional node
//                      for if-else/case/loop statements and end node for scope end of conditional/
//                      loop/sequential block statements.
//                   2) During CFG generation each time we visit a node we check for event control
//                      within the block, otherwise we store the whole block of statements in statement node.
//                   3) For loop/conditional node we have more than one child: process each branch
//                      and create new parse tree branch for each branch and store the parent pointer
//                      in the conditional/loop node.
//                   4) Set an end node at the end of each branch of conditional node and else branch
//                      of loop node.
//                   5) Create a dummy statement node as the top node of the cfg.
//                   6) Set the last node as the parent of the child of the dummy parent node.
//             2. Collect all the root node vrs their module item in a map.
//             3. If any one of the always constructs of the processing module cannot be converted
//                then do not proceed further with the other always constructs.
//             4. In the first created cfg we do not process the event trigger and event wait statements,
//                process them by traversing the cfg and rearrange the cfg as required.
//             5. Count the total number of events.
//             6. For each state nodes generate case item and store them in an array of items.
//             7. Generate a reset block for first time starting the simulation.
//             8. Generate a conditional statement with the reset and set block(case statement).
//             9. Keep the reset block in a compile flag.
//             10. Create an event control statement with the clocking event of the original parse tree.
//             11. Create an initial construct initializing the reset/state variables for generate items
//                 otherwise add the initialization statements in the module scope.
//             12. Create an always construct.
//             13. Create a sequential block with the above always and initial construct for generate items
//             14. Return the new module item(above sequential block for generate items/other wise
//                 the above always construct for replacing the old one.
//             15. Delete the cfg.
// 3) If we can able to create an always construct then replace the old item with it.
// 4) Return the new item.

// Debug Routines:
// 1) Print the CFG.
// 2) Print the total number of event controls.

// Limitations:
// 1) We do not support multiple cloking events: this is yet to be supported.
// 2) We do not support edged named event.
// 3) We do not support mixture of clock and event.

// Conditions:
// 1) No need to convert to explicit state m/c with always block without event control.
// 2) No need to convert to explicit state m/c with initial block without forever.
// 3) If there is only one cloking enent in a always/initial construct,
//    then no need to convert it to the explicit state machine.
// 4) If a node in the CFG is visited more than once at a same cloking
//    event then it will be an infinite loop, we warn it here.

// Compile flags:
// 1) Print the CFG and state machine if 'EXPLICIT_STATE_MACHINE_DEBUG' compile flag is on.

unsigned VeriModule::GenerateExplicitStateMachine(unsigned reset)
{
    // Create cfg for explicit state machine:
    // Iterate through the module items (such as various object declarations, assignments,
    // instantiations, constructs, and so forth).
    if (Message::ErrorCount()) return 0 ;
    VeriFsmAlwaysVisitor visitor ;
    Accept(visitor) ;

    Map *always_map = visitor.TakeAlwaysMap() ; // map containing module items(initial/always construct) vrs root node of cfg
    Map *decl_node_map = visitor.TakeDeclNodeMap() ; // map containing module items(initial/always construct) vrs _decl_node
    Map *first_blk_id_map = visitor.TakeFirstBlockIdMap() ; // map containing module items(initial/always construct) vrs labe id of the first block within always/initial construct
    Map *event_variable_map = visitor.TakeEventVarMap() ; // map containing orig_event_id event id vrs newly created event id during cfg creation
    Map *internal_event_node_map = visitor.TakeEventNodeMap() ;
    Map *root_vrs_event_var_map = visitor.TakeRootVrsEventVarMap() ; // map containing root vrs original event id set

    // VIPER #7422:
    // Collect all the cfg root node vrs all its related cfg root nodes in collected_node_map.
    // Related cfgs are identified by checking the event ids as always blocks can be related by
    // event wait/event trigger. After collecting them in a map check for the conversion status
    // of the each node of the map. If any node of the map cannot be converted then reset the
    // conversion status of all its related nodes. In the same way if any one of the related nodes
    // cannot be converted then reset the conversion status of the node(of the map) and also all
    // other related nodes of the node of the map. After marking all the cfg root node, remove the
    // cfg root nodes and corresponding module items from the always map. Then with the remaining
    // always map proceed the conversion.
    // eg. n1, n2, n3, n4 are the cfg root nodes for four always constructs
    // collect n1 vrs (n2, n3) if n1 is related to n2 and n3
    // collect n2 vrs (n1, n3, n4) if n2 is related to n1, n3 and n4
    // collect n3 vrs (n1, n2) if n3 is related to n1 and n2
    // collect n4 vrs (n2, n3) if n4 is related to n2 and n3
    // If n1 cannot be converted then reset conversion status for n2 and n3.
    // Now as n2, n3 cannot be converted, so reset conversion status for n4 also.
    // Thus all of the always constructs cannot be converted.
    Map collected_node_map(POINTER_HASH) ;
    unsigned event_var_map_size = root_vrs_event_var_map ? root_vrs_event_var_map->Size() : 0 ;
    unsigned i ;
    MapIter mi ;
    VerificCFGRootNode *node1, *node2 ;
    Set *event_set1 ;
    FOREACH_MAP_ITEM(root_vrs_event_var_map, mi, &node1, &event_set1) {
        Array *related_node_arr = new Array(1) ;
        for(i=0; i<event_var_map_size; i++) {
            MapItem *map_item = root_vrs_event_var_map->GetItemAt(i) ;
            node2 = map_item ? (VerificCFGRootNode*)map_item->Key() : 0 ;
            if (node1 == node2) continue ;
            Set *event_set2 = (Set*) root_vrs_event_var_map->GetValue(node2) ;

            unsigned related = 0 ;
            SetIter si ;
            VeriIdDef *event_id ;
            FOREACH_SET_ITEM(event_set2, si, &event_id) {
                if (!event_id) continue ;
                if (event_set1->GetItem(event_id)) {
                    related = 1 ;
                    break ;
                }
            }
            if (related) related_node_arr->InsertLast(node2) ;
        }
        (void) collected_node_map.Insert(node1, related_node_arr) ;
    }

    // Reset the conversion status of the node1 and all its related cfg root nodes.
    // If any node of the collected_node_map or any of the related_arr cannot be
    // converted, then reset conversion status of node1 and all its related nodes.
    Array *related_arr ;
    FOREACH_MAP_ITEM(&collected_node_map, mi, &node1, &related_arr) {
        if (!node1) continue ;
        if (!node1->GetConversionStatus()) {
            FOREACH_ARRAY_ITEM(related_arr, i, node2) {
                if (!node2) continue ;
                node2->SetConversionStatus(0) ;
            }
        } else { // reset the conversion status for all the related nodes:
            unsigned reset_conversion_status = 0 ;
            FOREACH_ARRAY_ITEM(related_arr, i, node2) {
                if (!node2) continue ;
                if (node2->GetConversionStatus()) continue ;

                node1->SetConversionStatus(0) ;
                reset_conversion_status = 1 ;
                break ;
            }
            if (reset_conversion_status) {
                FOREACH_ARRAY_ITEM(related_arr, i, node2) {
                    if (!node2) continue ;
                    node2->SetConversionStatus(0) ;
                }
            }
        }
    }

    // delete the related array:
    FOREACH_MAP_ITEM(&collected_node_map, mi, 0, &related_arr) delete related_arr ;
    collected_node_map.Reset() ;

    // delete the event ids created for events, if the always constructs containing
    // these events are not converted:
    FOREACH_MAP_ITEM(root_vrs_event_var_map, mi, &node1, &event_set1) {
        if (!node1) continue ;
        if (node1->GetConversionStatus()) continue ;

        SetIter si ;
        VeriIdDef *event_id ;
        FOREACH_SET_ITEM(event_set1, si, &event_id) {
            if (!event_id) continue ;
            MapIter mii ;
            VeriIdDef *orig_event_id ;
            Array *arr ;
            FOREACH_MAP_ITEM(event_variable_map, mii, &orig_event_id, &arr) {
                if (orig_event_id == event_id) {
                    (void) event_variable_map->Remove(orig_event_id) ;
                    unsigned ai ;
                    VeriIdDef *id ;
                    FOREACH_ARRAY_ITEM(arr, ai, id) delete id ;
                    delete arr ;
                }
            }
        }
    }

    // Remove the cfg root nodes from always_map that are not going to be converted:
    VeriModuleItem *item ;
    FOREACH_MAP_ITEM(always_map, mi, &item, &node1) {
        if (!node1) continue ;
        if (node1->GetConversionStatus()) continue ;
        delete node1 ;
        (void) always_map->Remove(item) ;
    }

    // Get module items:
    Array *module_items = GetItems() ;
    VeriScope *module_scope = GetScope() ;
    VeriIdDef *orig_event_id ;
    // Initialization of event variables:
    if (event_variable_map && event_variable_map->Size()) {
        Array *initial_stmt_arr = new Array(2) ;
        Array *arr ;
        FOREACH_MAP_ITEM(event_variable_map, mi, &orig_event_id, &arr) {
            unsigned ai ;
            VeriIdDef *event_id ;
            FOREACH_ARRAY_ITEM(arr, ai, event_id) {
                if (!event_id) continue ;
                VeriIdRef *l_val = new VeriIdRef(event_id) ;
                VeriExpression *r_val = new VeriIntVal((int)0) ;
                VeriNonBlockingAssign *initial_stmt = new VeriNonBlockingAssign(l_val, 0, r_val) ;
                initial_stmt->SetLinefile(Linefile()) ;
                initial_stmt_arr->InsertLast(initial_stmt) ;
            }
        }

        VeriScope *seq_blk_scope = new VeriScope(1, module_scope) ;
        VeriSeqBlock *initial_block = new VeriSeqBlock(0, 0, initial_stmt_arr, seq_blk_scope) ;
        VeriInitialConstruct *initial_construct = new VeriInitialConstruct(initial_block) ;
        initial_construct->SetLinefile(Linefile()) ;
        ((Array*)module_items)->InsertFirst(initial_construct) ;
    }

    // Declare event variables:
    Array *arr ;
    FOREACH_MAP_ITEM(event_variable_map, mi, &orig_event_id, &arr) {
        unsigned ai ;
        VeriIdDef *event_id ;
        FOREACH_ARRAY_ITEM(arr, ai, event_id) {
            if (!event_id) continue ;
            (void) module_scope->Declare(event_id) ;
            VeriDataType *data_type = new VeriDataType(VERI_REG, 0, 0) ;
            VeriDataDecl *data_decl = new VeriDataDecl(VERI_REG, data_type, event_id) ;
            data_decl->SetLinefile(Linefile()) ;
            ((Array*)module_items)->InsertFirst(data_decl) ;
        }
    }

    // Generate explicit state machine:
    VeriFsmGenerateVisitor generate_visitor ;
    generate_visitor.SetResetVariable(reset) ;
    generate_visitor.SetAlwaysMap(always_map) ;
    generate_visitor.SetDeclNodeMap(decl_node_map) ;
    generate_visitor.SetFirstBlockIdMap(first_blk_id_map) ;
    generate_visitor.SetEventNodeMap(internal_event_node_map) ;
    Accept(generate_visitor) ;

    // delete the taken maps:
    VerificCFGRootNode *node ;
    FOREACH_MAP_ITEM(always_map, mi, 0, &node) delete node ;
    if (always_map) always_map->Reset() ;
    delete always_map ;

    VerificCFGRootNode *decl_node ;
    FOREACH_MAP_ITEM(decl_node_map, mi, 0, &decl_node) delete decl_node ;
    if (decl_node_map) decl_node_map->Reset() ;
    delete decl_node_map ;

    if (first_blk_id_map) first_blk_id_map->Reset() ;
    delete first_blk_id_map ;

    FOREACH_MAP_ITEM(event_variable_map, mi, 0, &arr) delete arr ;
    if (event_variable_map) event_variable_map->Reset() ;
    delete event_variable_map ;

    if (internal_event_node_map) internal_event_node_map->Reset() ;
    delete internal_event_node_map ;

    Set *event_set ;
    FOREACH_MAP_ITEM(root_vrs_event_var_map, mi, 0, &event_set) {
        if (event_set) event_set->Reset() ;
        delete event_set ;
    }
    delete root_vrs_event_var_map ;

    return generate_visitor.GetNumOfItemReplaced() ; // return total number of items replaced
}

/*-----------------------------------------------------------------*/
//            VeriFsmGenerateVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VeriFsmGenerateVisitor::VeriFsmGenerateVisitor():
    VeriVisitor(),
    _module_scope(0),
    _scope(0),
    _always(0),
    _always_construct(0),
    _var_id(0),
    _infinite_loop(0),
    _old_ids_seqblk_map(new Map(POINTER_HASH)),
    _old2new_ids_map(new Map(POINTER_HASH)),
    _reset(0),
    _init_state_node(0),
    _always_map(0),
    _decl_node_map(0),
    _first_blk_id_map(0),
    _internal_event_node_map(0),
    _items_replaced(0)
{
}

VeriFsmGenerateVisitor::~VeriFsmGenerateVisitor()
{
    delete _old2new_ids_map ;
    delete _old_ids_seqblk_map ;

    _items_replaced = 0 ;
    _internal_event_node_map = 0 ;
    _first_blk_id_map = 0 ;
    _decl_node_map = 0 ;
    _always_map = 0 ;
    _init_state_node = 0 ;
    _reset = 0 ;
    _infinite_loop = 0 ;
    _var_id = 0 ;
    _always_construct = 0 ;
    _always = 0 ;
    _scope = 0 ;
    _module_scope = 0 ;
}

/*-----------------------------------------------------------------*/

void VeriFsmGenerateVisitor::TraverseArray(const Array *array)
{
    if (!array) return ;
    VERIFIC_ASSERT(!_always) ;

    // For each module item visit the corresponding visit routine
    // and for any always/initial construct if we able to construct
    // the new module item replace the old one with the new one.
    VeriTreeNode *node_item = 0 ;
    unsigned int i = 0 ;
    FOREACH_ARRAY_ITEM(array, i, node_item) {
        unsigned size = array->Size() ;
        if (node_item) node_item->Accept(*this) ;
        unsigned incr = array->Size() - size ;
        if (incr) i = i + incr ;

        if (_always) {
            ((Array*)array)->Insert(i, _always) ;
            delete node_item ;
            _always = 0 ;
        }
    }
}

/*-----------------------------------------------------------------*/
//    Visit routines of VeriFsmGenerateVisitor
/*-----------------------------------------------------------------*/

// Traverse VeriModule:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriModule, module)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = module.GetScope() ;
    _module_scope = _scope ;

    // Traverse module items
    TraverseArray(module.GetModuleItems()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse Generate block:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriGenerateBlock, gen_block)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_block.GetScope() ;

    // Traverse block items
    TraverseArray(gen_block.GetItems()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse Generate constructs:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriGenerateConstruct, gen_construct)
{
    // Traverse generate items
    TraverseArray(gen_construct.GetItems()) ;
}

// Traverse generate conditional statement:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriGenerateConditional, gen_cond)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_cond.GetThenScope() ;

    // Traverse the module item that will be executed when the condition is true
    TraverseNode(gen_cond.GetThenItem()) ;
    VeriTreeNode *old_then_item = gen_cond.GetThenItem() ;
    if (_always) { // replace the old item with the newer one
        gen_cond.SetThenItem(_always) ;
        delete old_then_item ;
        _always = 0 ;
    }

    _scope = gen_cond.GetElseScope() ;

    // Traverse the module item that will be executed when the condition is fals
    TraverseNode(gen_cond.GetElseItem()) ;
    VeriTreeNode *old_else_item = gen_cond.GetElseItem() ;
    if (_always) {
        // Replace the old item with the newer one
        gen_cond.SetElseItem(_always) ;
        delete old_else_item ;
        _always = 0 ;
    }

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse the generate case item:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriGenerateCaseItem, gen_case_item)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_case_item.GetScope() ;

    // Traverse the module item for the case item
    TraverseNode(gen_case_item.GetItem()) ;
    VeriTreeNode *old_item = gen_case_item.GetItem() ;
    if (_always) { // replace the old item with the newer one
        gen_case_item.SetItem(_always) ;
        delete old_item ;
        _always = 0 ;
    }

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse generate for statement:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriGenerateFor, gen_for)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = gen_for.GetLoopScope() ;

    // Traverse block items
    TraverseArray(gen_for.GetItems()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse always construct
void VeriFsmGenerateVisitor::VERI_VISIT(VeriAlwaysConstruct, always)
{
    VeriModuleItem *new_item = 0 ;
    // Convert the module item that is need to be converted: get it from _always_map:
    if (_always_map && _always_map->GetItem(&always)) {
        VerificCFGRootNode *node = (VerificCFGRootNode*)_always_map->GetValue(&always) ; // Get the root node of the cfg corresponding to this module item
        VerificCFGDeclarationNode *decl_node = _decl_node_map ? (VerificCFGDeclarationNode*)_decl_node_map->GetValue(&always) : 0 ; // Get the decl_node containing all the new declarations during cfg generation
        VeriIdDef *first_blk_id = _first_blk_id_map ? (VeriIdDef*)_first_blk_id_map->GetValue(&always) : 0 ; // Get the label id of the first block

        // Get the internal event nodes map:
        Map *internal_event_map = _internal_event_node_map ? (Map*)_internal_event_node_map->GetValue(&always) : 0 ;

        // Create new module item
        new_item = CreateExplicitStateMachine(&always, node, decl_node, first_blk_id, internal_event_map, ++_always_construct) ;
    }

    if (Message::ErrorCount()) {
        // some error/parse-tree inconsistency during the conversion
        // delete this faulty conversion node..
        // reset the error count to start fresh subsequently..
        Message::ClearErrorCount() ;
        delete new_item ;
        new_item = 0 ;
    }

    // Call resolve:
    if (new_item) new_item->Resolve(_scope ? _scope->Upper() : 0, VeriTreeNode::VERI_UNDEF_ENV) ;

    // Store the new item:
    if (new_item) {
        // Visit the new generated module item to rename the selected names
        VeriFsmSelectedNameVisitor select_visitor ;
        select_visitor.SetOld2NewIdsMap(_old2new_ids_map) ;
        select_visitor.SetOldIdsSeqBlkMap(_old_ids_seqblk_map) ;
        // Visit for renaming selected name
        new_item->Accept(select_visitor) ;

        _items_replaced++ ;
    }

    Reset() ;
    _always = new_item ;
}

// Traverse initial construct
void VeriFsmGenerateVisitor::VERI_VISIT(VeriInitialConstruct, initial)
{
    VeriModuleItem *new_item = 0 ;
    // Convert the module item that is need to be converted: get it from _always_map:
    if (_always_map && _always_map->GetItem(&initial)) {
        VerificCFGRootNode *node = (VerificCFGRootNode*)_always_map->GetValue(&initial) ; // Get the root node of the cfg corresponding to this module item
        VerificCFGDeclarationNode *decl_node = _decl_node_map ? (VerificCFGDeclarationNode*)_decl_node_map->GetValue(&initial) : 0 ; // Get the decl node
        VeriIdDef *first_blk_id = _first_blk_id_map ? (VeriIdDef*)_first_blk_id_map->GetValue(&initial) : 0 ; // Get the label id of the first block

        // Get the internal event nodes map:
        Map *internal_event_map = _internal_event_node_map ? (Map*)_internal_event_node_map->GetValue(&initial) : 0 ;

        // Create new module item
        new_item = CreateExplicitStateMachine(&initial, node, decl_node, first_blk_id, internal_event_map, ++_always_construct) ;
    }

    if (Message::ErrorCount()) {
        // some error/parse-tree inconsistency during the conversion
        // delete this faulty conversion node..
        // reset the error count to start fresh subsequently..
        Message::ClearErrorCount() ;
        delete new_item ;
        new_item = 0 ;
    }

    // Call resolve:
    if (new_item) new_item->Resolve(_scope ? _scope->Upper() : 0, VeriTreeNode::VERI_UNDEF_ENV) ;

    // Store the new item:
    if (new_item) {
        // Visit the new generated module item to rename the selected names
        VeriFsmSelectedNameVisitor select_visitor ;
        select_visitor.SetOld2NewIdsMap(_old2new_ids_map) ;
        select_visitor.SetOldIdsSeqBlkMap(_old_ids_seqblk_map) ;
        // Visit for renaming selected name
        new_item->Accept(select_visitor) ;

        _items_replaced++ ;
    }

    _always = new_item ;
    Reset() ;
}

// Traverse sequential block:
void VeriFsmGenerateVisitor::VERI_VISIT(VeriSeqBlock, seq_blk)
{
    // Keep the old scope:
    VeriScope *old_scope = _scope ;
    _scope = seq_blk.GetScope() ;

    TraverseArray(seq_blk.GetDeclItems()) ;

    // Traverse the block statements
    TraverseArray(seq_blk.GetStatements()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Reset all the flages and expression set after each always block traversal:
void
VeriFsmGenerateVisitor::Reset()
{
    // Reset all flags:
    _var_id = 0 ;
    _infinite_loop = 0 ;
}
