/*
 *
 * [ File Version : 1.168 - 2014/01/08 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h>   /* for sprintf */
#include <string.h> /* for strchr memset etc */
#include <ctype.h>
#include <stdlib.h>

#include "Strings.h"
#include "Array.h"

#include "VeriConstVal.h"
#include "VeriId.h"
#include "VeriScope.h"
#include "VeriMisc.h"

#include "veri_tokens.h" /* For (VERI) tokens */
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
#include "../vhdl/VhdlIdDef.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/* Set/Get bit values in VeriConst's streams */
#define SET_VALUE_FROM(S,B,V) ((S) ? ((S)[(B)/8] |= ((char)((V)<<((B)%8)))):0)
#define SET_BIT(S,B) SET_VALUE_FROM(S,B,1)
#define GET_BIT(S,B) ((S)?(((S)[(B)/8]>>((B)%8))&1):0)

/*******************************************************************/
/********************** Constructors ***************************/
/*******************************************************************/

/* General class of tree-node constants */
VeriConst::VeriConst() : VeriExpression() {}
VeriConst::~VeriConst() {}

/* Integers in the tree */
VeriIntVal::VeriIntVal(int n) : VeriConst(), _num(n) {}
VeriIntVal::~VeriIntVal() {}

/* Reals in the tree */
VeriRealVal::VeriRealVal(double n) : VeriConst(), _num(n) {}
VeriRealVal::~VeriRealVal() {}

// Arbitrary sized constants (in the tree)
VeriConstVal::VeriConstVal(const char *txt, unsigned type, unsigned size) :
    VeriConst(),
    _size(0),
    _is_signed(0),
    _is_string(0),
    _is_unsized_bit(0),
    _is_unsized_number(0),
    _value(0),
    _zvalue(0),  // Ab changed order because of compiler warnings (gcc)
    _xvalue(0)   // re-ordered to match declaration oder
#ifdef VERILOG_PRESERVE_LITERALS
    , _orig_value(0) // VIPER #2587 & #2770: Preserve the original value string
#endif
{
    //
    // Create a constant _value (arbitrary bit length) from lex text
    // Type determines the constant type.
    // This routine handles numbers :
    //   VERI_STRING,
    //   VERI_SIZED_BASED_NUM and
    //   VERI_UNSIZED_BASED_NUM
    // and puts them into a generic internal (bit-stream) data structure.
    // The bit-stream is organized LSB->MSB to make padding on MSB easier.
    // On second thought, this was not an issue, so we could have made it
    // run MSB->LSB, like all other values in Verilog.

    if (!txt) return ;
    VERIFIC_ASSERT(txt) ;
    // txt is NOT absorbed inside !
    char *str = Strings::save(txt) ;

#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & #2770: Preserve the original value string:
    switch (type) {
    case VERI_STRING:
        // Strings are stored as it is, no need to store again
        break ;
    case VERI_UNSIZED_LITERAL:
        // Ignore size for this unsized literal type:
        _orig_value = Strings::save(txt) ;
        break ;
    //case VERI_BASED_NUM: // Both sized and unsized
    default:
        // Include size only if it is not zero.
        char *val_size = (size) ? Strings::itoa((int)size) : 0 ;
        _orig_value = Strings::save(((size) ? val_size : ""), txt) ;
        Strings::free(val_size) ;
        break ;
    }
#endif

    unsigned mem_size ;

    if (type == VERI_STRING) {
        // String is stored as-is, but reversed : LSB->MSB
        // So that 0 is always the index of LSB (both for within-byte bits,
        // as for within-value byte).  Its still an array of characters.
        // It creates exactly the number we need internally.
        // For convenience, we also store the \0 character at the end,
        // although this should not be explicitly needed (since 'size'
        // (the bit-size) keeps track of the number of valid bits.
        unsigned len = (unsigned)Strings::len(str) ;
        // VIPER #4483: Support '\0' inside strings:
        // VIPER #4737: Support "\\" (escaped back-slash) inside strings:
        char *src = str ;
        char *dest = str ;
        while (*src != '\0') {
            if ((*src == '\\') && ((*(src+1) == '0') || (*(src+1) == '\\'))) {
                *dest++ = (*(src+1) == '0') ? '\0' : '\\' ; // Take '\0' or '\\' into destination
                src += 2 ; // Skip the "\0" or "\\" in source
                len-- ; // Decrease length by 1
                continue ;
            }
            *dest++ = *src++ ;
        }
        *dest = *src ; // Copy over the terminating '\0'
        _size = len*8 ;
        VERIFIC_ASSERT(_size == (len*8)) ; // _size is a bit field, so do this check just to be sure (VIPER #1608)
        mem_size = GetMemSize() ;
        _value = VFC_ARRAY_NEW(unsigned char, mem_size) ;
        memset(_value,0,mem_size) ; // Initialize memory with 'zero' values
        // Swap the characters
        unsigned i ;
        for (i=0; i<len; i++) _value[i] = (unsigned char) str[(len-i)-1] ;
        _value[i] = '\0' ; // close it with a \0 just for safety. This is not part of the actual value.

        // Label as coming from a string value
        _is_string = 1 ;

        Strings::free(str) ; // Remember to free str before returning
        return ;
    }

    // First, a quick translation to lower case characters
    // and remove _ and spaces
    char *tmp, *repl ;
    repl = tmp = str ;
    unsigned has_x = 0 ;
    unsigned has_z = 0 ;
    for (; *tmp!='\0'; tmp++) {
        if (isspace(*tmp) || *tmp == '_') continue ;
        if (*tmp == '?') *tmp = 'z' ; // '?' is a 'z-digit' : LRM 2.5 table.
        *tmp = (char)tolower(*tmp) ;
        if (*tmp=='x') has_x = 1 ;
        if (*tmp=='z') has_z = 1 ;
        *repl++ = *tmp ;
    }
    *repl = *tmp ; // Close the replacement (if done) with '\0'

    // Based number : Text in, Created VeriValue out.
    // Txt is of the form <_size>[space]*'<base>[space]*BasedNum
    // With <base> : [dDbBhHoO] for decimal, binary, hex or octal
    // With BasedNum a string of [01xzXZ?_] or [a-fA-F] characters.
    // '_' is ignored, ? is alternative for 'z'.
    // Translate to a string of [01xz] according to IEEE 1364 std paragraph 2.5.1
    // and paragraph 4.4.1 (length of unsigned const is sizeof(integer)
    //
    // We will store the constant LSB->MSB in the resulting string,
    // ('num' will point to the LSB character).
    // Since the 'txt' incoming string is MSB->LSB, we will need to
    // scan the string right-to-left.
    //
    // Chop-off if text does not fit in stated _size, and pad (on left)
    // if _size if larger than needed. Use LRM 2.5.1 padding rules.

    // VIPER #1740: Set flags for unsized bit and number literals
    if (str[0] == '\'') {
        switch(str[1])  {
        case '0':
        case '1':
        case 'x':
        case 'z': // This is an intentional fall through
            // This is a SystemVerilog unsized single bit litertal without base, like '1.
            // So, it should have a termination '\0' after this literal
            if (str[2] == '\0') _is_unsized_bit = 1 ;
            break ;
        case 'b':
        case 'o':
        case 'd':
        case 'h': // This is an intentional fall through
            // This is an unsized multi bit literal with base like 'b1, only if size is not specified
            if (!size) _is_unsized_number = 1 ;
            break ;
        case 's':
            switch(str[2]) {
            case 'b':
            case 'o':
            case 'd':
            case 'h': // This is an intentional fall through
                // This is a signed unsized multi bit litertal with base like 'sb1, only if size is not specified
                if (!size) _is_unsized_number = 1 ;
                break ;
            default:
                break ;
            }
            break ;
        default:
            break ;
        }
    }

    // Determine _size. A bit odd, but we'll use this both for
    // sized and unsized based numbers

    // Do a proper determination of the size of the literal.
    if (size) {
        // size was separate token in front of the based literal. Determined by caller.
    } else if (str[0] == '\'') {
        // VIPER #3588 (issue 1): Try to determine the size of the value from the literal.
        // It should be at-least 32 bit, but we should make it of such a size that it can
        // accommodate the full literal value specified here (we should not truncate it):
        // Determine the base of the value if it is based balue:
        char *base = (str[1] == 's') ? str+2 : str+1 ;
        unsigned literal_len = Strings::len(base+1) ;
        switch (*base) {
            case 'b': size = literal_len ; break ;
            case 'o': size = literal_len * 3 ; break ;
            // VIPER #4616: Do not set size for decimal literals, we will set it to 32 below.
            // From experiments with different tools it seems that all of them truncate the
            // unsized decimal numbers to 32 bit. Nothing is there in the LRM regarding this.
            // Since other tools behave this way we choose to do the same. They do not match
            // this behaviour for binary or hex literals, so, we do not change anything for
            // other bases. VIPER #4616 is about decimal literals - so this fixes the issue.
            case 'd': break ; // 'size' remains 0 for this decimal base
#if 0 // VIPER #4616: Disable this code that sets size by computing the value
            {
                // Check whether this decimal number can fit in 32 bits. If it can,
                // then we don't need to check for size here, since size must be at
                // least 32 bit in this unsized literal.
                if (literal_len < 10) break ;
                char sum ;
                char borrow ;
                char *digit ;
                char *number = Strings::save(base+1) ;
                char *tmp = number ;
                while (*number!='\0') {
                    // Divide by 2:
                    borrow = 0 ;
                    digit = number ;
                    while (*digit!='\0') {
                         sum = (*digit - '0') ;     // Calculate value of digit
                         if (borrow) sum += 10 ;    // Add the borrow (base 10)
                         borrow = sum & 1 ;         // Update borrow : take the LSB bit
                         *digit = (sum>>1) + '0' ;  // Update digit : divide by two.
                         digit++ ;                  // Shift to next digit
                    }
                    // Prune off leading '0's:
                    if (*number=='0') number++ ;
                    size++ ;
                }
                Strings::free(tmp) ;
                break ;
            }
#endif
            case 'h': size = literal_len * 4 ; break ;
            default : size = literal_len + 1 ; break ;
        }
        // This is an 'unsized' literal. LRM decribes that this is (at least) 32 bits.
        if (size < (sizeof(int)*8)) size = sizeof(int) * 8 ;
    } else {
        // Normal sized literal. Size is in an integer in front of '.
        size = (unsigned) Strings::atoi(str) ;
    }

    _size = size ;
    // VIPER #3528: Do not assert on trancation of size here, clip instead:
    //VERIFIC_ASSERT(_size == size) ; // _size is a bit field, so do this check just to be sure (VIPER #1608)
    if (_size != size) {
        // Does not fit in 28 bit, so we need to clip to 28 bits with a warning:
        _size = size % (1<<28) ;
        if (_size == 0) _size = 1 ; // VIPER #7892: Represent value in 1 bit instead of 0
        char size_str[16] ;
        sprintf(size_str, "%u", size) ;
        Warning("decimal constant %s is too large, using %d instead", size_str, _size) ;
    }

    // Allocate space for the _value(s) and initialize
    // Allocate 3 extra bytes to avoid writing in unallocated memory,
    // since octal numbers are grouped in 3-byte clusters.
    mem_size = GetMemSize() ;
    _value = VFC_ARRAY_NEW(unsigned char, mem_size) ;
    memset(_value,0,mem_size) ;
    _xvalue = (has_x) ? VFC_ARRAY_NEW(unsigned char, mem_size) : 0 ;
    _zvalue = (has_z) ? VFC_ARRAY_NEW(unsigned char, mem_size) : 0 ;
    if (_xvalue) memset(_xvalue,0,mem_size) ;
    if (_zvalue) memset(_zvalue,0,mem_size) ;

    // Now, set tmp to last (LSB) character in 'str'
    // since we will start at LSB and work back in string to MSB.
    //
    tmp = repl-1 ;
    // It ends at the 'base' character (right after ')
    char *base = ::strchr(str,'\'') ; // We will walk backward
    VERIFIC_ASSERT(base) ; // Just checkin that we really have a based number here *
    base++ ; // Now AT the base character

    // 01/03/01 : Verilog 2000 extension : after the base ', we can now
    // have an additional optional 's' for signed
    // indication of the value. Process it here.
    if (*base == 's') { _is_signed = 1 ; base++ ; }

    // Always run LSB to MSB (right to left) until we hit the base again.
    unsigned bit = 0 ;
    unsigned value_too_large = 0 ;
    switch (*base) {
    case 'b' : // normal based literal
        // Run LSB to MSB (right to left) until we hit the base
        for (;tmp!=base; tmp--) {
            if (bit >= _size) {
                // We are beyond the size that was designated.
                // Still OK if this is '0' :
                if (*tmp=='0') { bit++ ; continue ; } ;
                // Otherwise, the value will be truncated :
                value_too_large = 1 ;
                break ;
            }
            switch (*tmp) {
            case '1' : SET_BIT(_value,bit) ; break ;
            case '0' : break ; // 0 is default initial _value
            case 'x' : SET_BIT(_xvalue,bit)  ; break ;
            case 'z' : SET_BIT(_zvalue,bit)  ; break ;
            default : Error("illegal character in binary number") ; break ;
            }
            bit++ ;
        }
        break ;
    case 'd' :
        {
        // Run LSB to MSB (right to left) until we hit the base
        // Check for illegal characters. Note that x and z are allowed (issue 1483)
        // VIPER #6715: According to LRM BNF (for all Verilog dialects):
        //   1) decimal_number           ::= unsigned_number
        //                                 | [ size ] decimal_base unsigned_number
        //                                 | [ size ] decimal_base x_digit { _ }
        //                                 | [ size ] decimal_base z_digit { _ }
        //   2) unsigned_number          ::= decimal_digit { _ | decimal_digit }
        //   3) size                     ::= non_zero_unsigned_number
        //   4) non_zero_unsigned_number ::= non_zero_decimal_digit { _ | decimal_digit}
        //   5) non_zero_decimal_digit   ::= 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
        //   6) decimal_digit            ::= 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
        //   7) decimal_base             ::= '[s|S]d | '[s|S]D
        //   8) x_digit                  ::= x | X
        //   9) z_digit                  ::= z | Z | ?
        // So, decimal number CAN contain 'x' or 'z', but ONLY as a single digit.
        unsigned found_x_z_digit = 0 ;
        unsigned found_decimal_digit = 0 ;
        for (; tmp!=base; tmp--) {
            //if ((*tmp < '0' || *tmp > '9') && (*tmp!='x') && (*tmp!='z')) {
            if ((*tmp >= '0') && (*tmp <= '9')) {
                found_decimal_digit = 1 ;
            } else if ((*tmp == 'x') || (*tmp == 'z')) {
                found_x_z_digit++ ;
            } else {
                Error("illegal character in decimal number") ;
            }
        }
        if ((found_x_z_digit && found_decimal_digit) || (found_x_z_digit > 1)) {
            // VIPER #6715: Either x/z used with decimal digit or used multiple times, error:
            Error("illegal use of x or z character in decimal number") ;
        }
        // If there is an x or z in the value, the whole value becomes x or z (issue 1483).
        if (has_x) {
            SET_BIT(_xvalue,0) ; // MSB extension will happen below
            bit++ ;
        } else if (has_z) {
            SET_BIT(_zvalue,0) ; // MSB extension will happen below
            bit++ ;
        } else {
            // Cannot use atoi, since that is restricted to 32 bit.
            // Use Abhijit's divide and conquer algorithm. (Viper issue 1774)
            // Divide by 2 until number is 0. The carry is the LSB bit after each division.
            // Start at MSB :
            char *number = tmp+1 ;
            while (*number!='\0') {
                // Divide by 2 :
                char sum ;
                char borrow = 0 ;
                char *digit = number ;
                while (*digit!='\0') {
                     sum = (*digit - '0') ;     // Calculate value of digit
                     if (borrow) sum += 10 ;    // Add the borrow (base 10)
                     borrow = sum & 1 ;         // Update borrow : take the LSB bit
                     *digit = (sum>>1) + '0' ;  // Update digit : divide by two.
                     digit++ ;                  // Shift to next digit
                }
                // Prune off leading '0's :
                if (*number=='0') number++ ;

                // Set this bit if division created a borrow.
                if (borrow) {
                    if (bit >= _size) {
                        value_too_large = 1 ;
                        break ;
                    }
                    // set this bit:
                    SET_BIT(_value,bit) ;
                }
                bit++ ; // Go to next bit upward.
                // 'number' is now divided by two (and can be printed..). Recur until it is zero.
            }
        }
        }
        break ;
    case 'o' :
        {
            // Do this 8 octal numbers at a time (24 bits == 3 bytes)
            unsigned i, vnum, xnum, znum, bit_num, word_shift ;
            vnum = xnum = znum = bit_num = word_shift = 0 ;
            for (; tmp!=base; tmp--) {
                if (bit >= _size) {
                    // We are beyond the size that was designated.
                    // Still OK if this is '0' :
                    if (*tmp=='0') { bit += 3 ; continue ; } ;
                    // Otherwise, the value will be truncated :
                    value_too_large = 1 ;
                    break ;
                }
                if (bit_num==24) {
                    for (i=0; i!=24; i+=8) SET_VALUE_FROM(_value,word_shift+i,vnum>>i) ;
                    if (_xvalue) for (i=0; i!=24; i+=8) SET_VALUE_FROM(_xvalue,word_shift+i,xnum>>i) ;
                    if (_zvalue) for (i=0; i!=24; i+=8) SET_VALUE_FROM(_zvalue,word_shift+i,znum>>i) ;
                    bit_num = 0 ;
                    word_shift+=24 ; /* word_shift shift : 3 bytes */
                    vnum = xnum = znum = 0 ;
                }
                /* Set 3 bits at a time into the temporary result(s) */
                if (*tmp == 'x') {
                    xnum |= 7<<bit_num ;
                } else if (*tmp == 'z') {
                    znum |= 7<<bit_num ;
                } else if (*tmp >= '0' && *tmp <= '7') {
                    unsigned val = (unsigned) ((int)(*tmp-'0'));
                    vnum |= (val<<bit_num) ;
                } else {
                    Error("illegal character in octal number") ;
                }
                bit += 3 ;
                bit_num += 3 ;
            }
             /* Set the remainder */
            for (i=0; i!=24; i+=8) SET_VALUE_FROM(_value,word_shift+i,vnum>>i) ;
            if (_xvalue) for (i=0; i!=24; i+=8) SET_VALUE_FROM(_xvalue,word_shift+i,xnum>>i) ;
            if (_zvalue) for (i=0; i!=24; i+=8) SET_VALUE_FROM(_zvalue,word_shift+i,znum>>i) ;
        }
        break ;
    case 'h' :
        for (; tmp!=base; tmp--) {
            if (bit >= _size) {
                // We are beyond the size that was designated.
                // Still OK if this is '0' :
                if (*tmp=='0') { bit += 4 ; continue ; } ;
                // Otherwise, the value will be truncated :
                value_too_large = 1 ;
                break ;
            }
            /* set 4 bits at once */
            if (*tmp == 'x') {
                SET_VALUE_FROM(_xvalue,bit,15) ;
            } else if (*tmp == 'z') {
                SET_VALUE_FROM(_zvalue,bit,15) ;
            } else if (*tmp >= '0' && *tmp <= '9') {
                unsigned val = (unsigned) ((int)(*tmp-'0')) ;
                SET_VALUE_FROM(_value,bit,val) ;
            } else if (*tmp >= 'a' && *tmp <= 'f') {
                unsigned val = ((unsigned) ((int)(*tmp-'a'))) + 10;
                SET_VALUE_FROM(_value,bit,val) ;
            } else {
                Error("illegal hexadecimal number") ;
            }
            bit += 4 ; /* hex does 4 bits per character */
        }
        break ;
    case '0' : // System Verilog unsized literals
    case '1' :
    case 'x' :
    case 'z' :
        switch (*tmp) {
        case '1' : SET_BIT(_value,bit) ; break ;
        case '0' : break ; /* 0 is default initial _value */
        case 'x' : SET_BIT(_xvalue,bit)  ; break ;
        case 'z' : SET_BIT(_zvalue,bit)  ; break ;
        default : Error("illegal character in binary number") ; break ;
        }
        // Just check that it was only a single bit :
        if (tmp!=base) value_too_large = 1 ;
        bit++ ;
        break ;
    default :
        VERIFIC_ASSERT(0) ; /* Something wrong in flex token parser */
    }

    if (value_too_large) {
        // the value is too large for the designated size :
        Warning("literal value truncated to fit in %d bits",_size) ;
    }

    if (_size > bit) {
        /* padding needed */
        if (GET_BIT(_xvalue, bit-1)) {
            /* Pad 'x's */
            while (bit != _size) { SET_BIT(_xvalue,bit) ; bit++ ; }
        } else if (GET_BIT(_zvalue, bit-1)) {
            /* Pad 'z's */
            while (bit != _size) { SET_BIT(_zvalue,bit) ; bit++ ; }
        } /* else : Pad '0's : (the initial _value) (so nothing to do) */
    }

    // Remember to free up allocated memory
    Strings::free(str) ;
}

VeriConstVal::~VeriConstVal()
{
    VFC_ARRAY_DELETE(_value) ;
    VFC_ARRAY_DELETE(_xvalue) ;
    VFC_ARRAY_DELETE(_zvalue) ;
#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & #2770: Free the original string:
    Strings::free(_orig_value) ;
    _orig_value = 0 ;
#endif
}

VeriTimeLiteral::VeriTimeLiteral(char *literal) : VeriConst(), _literal(literal) {} // Absorb incoming string

VeriTimeLiteral::~VeriTimeLiteral() { Strings::free(_literal) ; }

unsigned VeriTimeLiteral::IsStep() const
{
    if (_literal && strstr(_literal, "step")) return 1 ; // 1step literal
    return 0 ;
}

/******************************** Resolve **************************************/

void
VeriConst::Resolve(VeriScope *scope, veri_environment environment)
{
    if ((environment == VERI_UNDEF_ENV) || (environment == VERI_COVERPOINT_ENV)) return ; // Don't test anything.

    // Usage of a constant.
    // This is allowed only in some expressions.
    // FIX ME : Rather than state where it IS allowed, we should check where it is NOT allowed.
    // That leaves more freedom in adding different kind of expressions.
    switch (environment) {
    case VERI_CONSTANT_EXPRESSION :
    case VERI_PARAM_INITIAL_EXP :
    case VERI_PARAM_VALUE : // value of parameter, constant for normal parameter
    case VERI_BINS_INIT_VALUE :
    case VERI_PROCE_CONT_ASSIGN_VAL : // VIPER #6110: value of RHS of procedural continuous assignments
    case VERI_EXPRESSION :
    case VERI_EXPRESSION_UNDECL_USE_GOOD :
    case VERI_ABORT_EXPRESSION :
    case VERI_LET_EXPRESSION :
    case VERI_WAIT_EXP :
    case VERI_STRING_TARGET : // for VIPER #4259
    case VERI_CONSTRAINT_EXPR : // Constant values are allowed as constraint expr
    case VERI_INLINE_CONSTRAINT_EXPR :
    case VERI_PROPERTY_ARG :
    case VERI_SEQUENCE_ARG :
    case VERI_SEQ_PROP_INIT :
    case VERI_DELAY_CONTROL :
    case VERI_PORT_CONNECTION :
    case VERI_EVENT_CONTROL :
    case VERI_EVENT_CONTROL_UNDECL_USE_GOOD :
    case VERI_SYSTEM_TASK_ARG :
        break ;
    case VERI_PROPERTY_ENV :
    case VERI_SEQUENCE_ENV :
    case VERI_CLOCKING_BLOCK :
        break ;
        // constants are allowed in property and sequence expressions.
    case VERI_TYPE_ENV : // constant is allowed in a 'type', for type-casting... CHECK ME: maybe finetune this ?
        break ;
    case VERI_BIN_VAL_ENV :
        break ; // constants are allowed as bin values
    case VERI_INTEGRAL_CONSTANT : // VIPER 2539 : comes here for int, based number also
        break ;
    case VERI_VALUE_RANGE : // VIPER 4124
    case VERI_GATE_INOUT_PORT_CONNECTION : // VIPER 2543 : port connection to trans gate, ok to have a constant hooked to an inout
        break ;
    default :
        Error("constant is not allowed here") ;
        break ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void
VeriConst::ResolveLabel(VeriScope *scope, veri_environment environment, VeriExpression * /* target */)
{
    if ((environment == VERI_UNDEF_ENV) || (environment == VERI_COVERPOINT_ENV)) return ; // Don't test anything.

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriRealVal::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2539 : illegal context for real value, expect integral constant
    if (environment == VERI_INTEGRAL_CONSTANT) {
        Error("illegal context for real expression") ;
        return ;
    }
    VeriConst::Resolve(scope, environment) ; // Verify all other environments
}

/************************* acceptance of constant is a onehot var ***************/

void
VeriConst::LinkOnehot(VeriIdDef *onehot_var)
{
    // Always accept a constant against a onehot variable
    // so that constants can be used freely to define FSM states.

    if (!onehot_var) return ;
    if (!onehot_var->IsVar()) return ; // its not a variable.
    if (!onehot_var->CanBeOnehot()) return ; // variable is not onehot encoded. Constant will be used as 'normal'.

    // Add the constant as a state to the onehot variable.
    onehot_var->AddFsmStateConstant(this) ;
}

/******************************** Size **************************************/

unsigned VeriIntVal::Size(VeriDataFlow * /*df*/) const   { return sizeof(int) * 8 ; }
unsigned VeriRealVal::Size(VeriDataFlow * /*df*/) const  { return sizeof(double) * 8 ; }
unsigned VeriConstVal::Size(VeriDataFlow * /*df*/) const { return (!_size && _is_string) ? 8 : _size ; } // VIPER #4670 & #4678: Empty string ("") should have size 8
unsigned VeriTimeLiteral::Size(VeriDataFlow * /*df*/) const { return sizeof(double) * 8 ; }

/******************************** Sign (4.5.1 rules) ************************/

unsigned VeriIntVal::Sign() const   { return 1 ; }
unsigned VeriRealVal::Sign() const  { return 1 ; }
unsigned VeriConstVal::Sign() const { return _is_signed ; }
unsigned VeriTimeLiteral::Sign() const  { return 0 ; }

/******************************** Image **************************************/

/** Image is like 'pretty-print', but returns an allocated string. **/
char *VeriIntVal::Image() const
{
    char *r = Strings::save("[ some big number ]") ;
    ::sprintf(r,"%d",_num) ;
    return r ;
}
char *VeriRealVal::Image() const
{
    return Strings::dtoa(_num) ;
}
char *VeriConstVal::Image() const
{
#ifdef VERILOG_PRESERVE_LITERALS
    // If we have the original value preserved, return it as the image:
    // Viper #4994 : added _is_string check
    if (_orig_value && !_is_string) return Strings::save(_orig_value) ;
#endif

    // Similar to NonconstVal::String :
    // Create a char* representation for this value.
    unsigned char r ;
    unsigned i, j ;
    char *result ;
    if (_is_string && (_size % 8 == 0) && !_zvalue && !_xvalue) {
        // Value should and can be printed as a Verilog string.
        // The characters are saved in opposite order (MSB-LSB). So swap them.
        // Also, start and end with a ". (Return as a string)
        unsigned len = _size / 8 ;
        if (!_value) return 0 ;
        // VIPER #4483: Do not use Strings::save(_value), _value can have \0 in it:
        result = Strings::allocate(len+3) ; // Allocate more spaces for enclosing quotes
        memset(result, '\"', len+2) ; // Initialize memory with all quotes for strings
        result[len+2] = '\0' ; // Terminate the string
        char *p = result + len ;
        for (i=0; i<len; i++) {
            r = _value[i] ;
            switch (r) {
            case '\n' : r = 'n' ; break ;
            case '\t' : r = 't' ; break ;
            case '\r' : r = 'r' ; break ;
            case '\0' : r = '0' ; break ;
            case '\\' : break ;  // No need to redefine
            case '"' : break ;   // No need to redefine
            default :
            {
                // Viper #3591 : Support escaped character - octal values
                // If the character is non-printable, change it to its octal
                // representation
                if (isprint(r)) {
                    *p-- = (char) r ; r = '\0' ;
                } else {
                    // Change character to octal value
                    unsigned nLen = (unsigned) (p - result) ;

                    char *tmp = result ;
                    result = Strings::save("\"  ", tmp) ; // extend by 3 char.
                    p = result + nLen + 3 ;
                    Strings::free(tmp) ;

                    *p-- = (char) (r%8) + '0' ; r = r/8 ;
                    *p-- = (char) (r%8) + '0' ; r = r/8 ;
                    *p-- = (char) (r%8) + '0' ; // r%8 must be same as r
                    *p-- = '\\' ;

                    continue ;
                }
                break ;
            }
            }

            // In the case there is an escaped character, modify result string
            if (r) {
                char *tmp = result ;
                unsigned nLen = (unsigned) (p - result) ;
                // VIPER #5107: Properly print '\0'. If the last printed character was a
                // digit, then we need to print three 0 here for the '\0'. Otherwise we
                // are in trouble of including the last printed digit in the '\'!
                unsigned print_octal_0 = ((r == '0') && i && isdigit(_value[i-1])) ? 1 : 0 ;
                // Extend by one/three characters depending on whether we need to print octal or not:
                result = Strings::save(((print_octal_0) ? "\"  " : "\""), tmp) ;
                p = result + nLen + 1 ;
                Strings::free(tmp) ;
                if (print_octal_0) {
                    p = p + 2 ; // Add another 2, 1 is already added above
                    // Print three 0s:
                    *p-- = '0' ;
                    *p-- = '0' ;
                    // 3rd 0 will be printed below...
                }
                *p-- = (char) r ;
                *p-- = '\\' ;
            }
        }
        return result ;
    }

    // Otherwise, build a bit-string, one character per bit
    char *buffer = Strings::allocate(_size + 33) ; // The 33 is for taking into consideration the header and a possibly large _size
    if (_is_unsized_bit) {
        // Don't print size and base for SystemVerilog unsized bit literal (#1740)
        ::sprintf(buffer,"'") ;
    } else if (_is_unsized_number) {
        // Don't print size for SystemVerilog unsized bit literal (#1740)
        ::sprintf(buffer,"'%sb", (_is_signed) ? "s" : "") ;
    } else {
        // Print bit-literal header : size (issue 1313) and optional sign
        ::sprintf(buffer,"%d'%sb", _size, (_is_signed) ? "s" : "") ;
    }
    result = Strings::save(buffer) ;

    // Go MSB to LSB. MSB is highest index. 0 the lowest.
    unsigned print_the_rest = 0 ; // Flag used to control printing of remaining bits
    char last_value = 0 ;         // Last bit encountered.  Used to determine when to start printing.
    char current_value ;
    i = _size ;
    j = 0 ;
    while (i-- != 0) {
        // Determine the value of this bit
        if (GET_BIT(_xvalue,i))         { current_value = 'x' ; }
        else if (GET_BIT(_zvalue,i))    { current_value = 'z' ; }
        else if (GET_BIT(_value,i))     { current_value = '1' ; }
        else                            { current_value = '0' ; }
        // Determine if you need to print this bit or trim it.
        if (print_the_rest || (current_value == '1' && !_is_unsized_bit) || (last_value && (last_value != current_value))) { // Don't print anything if it's redundant
            if (!print_the_rest) {
                // Print last_value if last_value is defined and it's not the same as the current_value
                if (last_value && (last_value != current_value)) buffer[j++] = last_value ;
                // Force printing of remainder bits
                print_the_rest = 1 ;
            }
            buffer[j++] = current_value ;
        }
        last_value = current_value ;
    }
    // If nothing was printed, then print the last_value.  This is the case
    // for all 0's, x's, z's (and 1's only _is_unsized_bit).
    if (!print_the_rest) buffer[j++] = last_value ;

    // Terminate the string properly :
    buffer[j] = '\0' ;
    char *tmp = result ;
    result = Strings::save(result,buffer) ;
    Strings::free(tmp) ;
    Strings::free(buffer) ;
    return result ;
}
char *VeriTimeLiteral::Image() const
{
    return Strings::save(_literal) ;
}

#if 0
// To Vhdl BitStringLiteral of size 'size' From ConstVal
char *VeriIntVal::ToBitStringLiteral(unsigned size /* = 0 */ )
{
    unsigned out_size = (size > 0) ? size : 32 ;
    char *s = Strings::allocate(4+ out_size) ; // 32 + 2(quote) + 1(B) +1(\0)
    unsigned i = Size(0) ;
    unsigned idx = 0 ;
    s[idx++] = 'B' ; //Make it a bitstring literal
    s[idx++] = '\"' ;
    while (i-- != 0) {
        s[idx++] = ((_num>>i)&1) ? '1' : '0' ;
        if (idx==out_size+2) break ;
    }
    s[idx++] = '\"' ;
    s[idx] = 0 ;
    return s ;
}
#endif

/******************************** ImageVhdl **************************************/

/** ImageVhdl is like Image, but it returns the value as a VHDL - formatted value. */
// Viper 6649 added a new argument size to indicate the size of of the
// output. This is useful when there is no formal type known, as
// in the case when it is called from ConvertModule.
char *VeriIntVal::ImageVhdl(VhdlIdDef *formal_type, unsigned size) const
{
// For mixed lang, The size should have been evaluated from
// the constraint of the formal. However the constraint is
// not set here . We change this string during evaluation
// when the constraint is present
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    if ((formal_type && formal_type->IsArray()) || size) {
        unsigned out_size = size ? size : Size(0) ;
        char *s = Strings::allocate(out_size + 4) ; // 32 + 2(quote) + 1(B) +1(\0)
        unsigned i = out_size ;
        unsigned idx = 0 ;
        s[idx++] = 'B' ; //Make it a bitstring literal
        s[idx++] = '\"' ;
        while (i-- != 0) {
            s[idx++] = ((_num>>i)&1) ? '1' : '0' ;
        }
        s[idx++] = '\"' ;
        s[idx] = 0 ;
        return s ;
    }
#else
    (void) size ;
#endif //VERILOG_DUAL_LANGUAGE_ELABORATION
    (void) formal_type ;
    char *r = Strings::save("[ some big number ]") ;
    ::sprintf(r,"%d",_num) ;
    return r ;
}
char *VeriRealVal::ImageVhdl(VhdlIdDef * /*formal_type*/, unsigned size) const
{
    if (size) {
        unsigned out_size = size ;
        char *s = Strings::allocate(out_size + 4) ; // 32 + 2(quote) + 1(B) +1(\0)
        unsigned i = out_size ;
        int num = Integer() ;
        unsigned idx = 0 ;
        s[idx++] = 'B' ; //Make it a bitstring literal
        s[idx++] = '\"' ;
        while (i-- != 0) {
            s[idx++] = ((num>>i)&1) ? '1' : '0' ;
        }
        s[idx++] = '\"' ;
        s[idx] = 0 ;
        return s ;
    } else {
        return Strings::dtoa(_num) ;
    }
}
char *VeriConstVal::ImageVhdl(VhdlIdDef *formal_type, unsigned size) const
{
    char *result ;

    // Similar to NonconstVal::String :
    // Create a char* representation for this value.
    unsigned char r ;
    unsigned i ;

    if (_is_string && (_size % 8 == 0) && !_zvalue && !_xvalue && !size) {
        // Value should and can be printed as a Verilog string.
        // The characters are saved in opposite order (MSB-LSB). So swap them.
        // Also, start and end with a ". (Return as a string)
        unsigned len = _size / 8 ;
        if (!_value) return 0 ;
        // VIPER #3985 : _value can have multiple 0 entries in the MSB side if it
        // was size extended ignore such 0 entries in the MSB side for VHDL only:
        i = len ;
        while (i--) {
            if (_value[i] != '\0') break ; // Terminate at first non-zero char
            len-- ; // Otherwise skip this 0, it is in the MSB side
        }
        // VIPER #4483: Do not use Strings::save(_value), _value can have \0 in it:
        result = Strings::allocate(len+3) ; // Allocate more spaces for enclosing quotes
        memset(result, '\"', len+2) ; // Initialize memory with all quotes for string
        result[len+2] = '\0' ; // Terminate the string
        char *p = result + len ;
        for (i=0; i<len; i++) {
            r = _value[i] ;
            switch (r) {
            case '\n' : r = 'n' ; break ;
            case '\t' : r = 't' ; break ;
            case '\r' : r = 'r' ; break ;
            case '\0' : *p-- = '0' ; r = '\0' ; break ; // VIPER #4483: Store as character '0'
            case '\\' : break ;  // No need to redefine
            case '"' : break ;   // No need to redefine
            default : *p-- = (char) r ; r = '\0' ; break ;
            }

            // In the case there is an escaped character, modify result string
            if (r) {
                char *tmp = result ;
                unsigned nLen = ((unsigned) (p - result)) ;
                result = Strings::save("\"", tmp) ; // extend by one char.  need to make space for '\'
                p = result + nLen + 1 ;
                Strings::free(tmp) ;
                *p-- = (char) r ;
                *p-- = '\\' ;
            }
        }
        return result ;
    }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // Viper 8300: for XZ we populate 'U' in the Verilog. This is
    // as per popular simulator behavior
    if (formal_type && formal_type->IsEnumerationType() && HasXZ()) {
        VhdlIdDef* enum_id = formal_type->GetEnumAt(0) ;
        if (enum_id) {
            char* lit_name = Strings::save(enum_id->Name()) ;
            return lit_name ;
        }
    }
#endif

    // Otherwise, build a bit-string, one character per bit
    // Build in lumps of 32 bits
    char buffer[34] ;
    // print bit-literal header : size (issue 1313) and optional sign
    // Viper 8300: for XZ we create a string literal and not bit string literal
    if (HasXZ()) {
        ::sprintf(buffer,"\"") ; // start with a B".
    } else {
        ::sprintf(buffer,"B\"") ; // start with a B".
    }
    result = Strings::save(buffer) ;
    // Append literal itself..
    char *tmp ;
    unsigned shift = 0 ;
    unsigned convert_to_int = 1 ; // Flag needed for #2362
    // Run MSB-LSB
    // Viper 7613. If the formal size (size) is greater than _size then
    // pad the remaining bits with zero. We will create a string of length
    // of size.
    unsigned max_size = _size < size ? size : _size ;
    i = 0 ;
    if (_size < size) {
        // Zero Padding
        for (; i < size-_size; i++) {
           if (i-shift == 32) {
               buffer[i-shift] = '\0' ;
               tmp = result ;
               result = Strings::save(result,buffer) ;
               Strings::free(tmp) ;
               shift = shift+32 ;
           }
           buffer[i-shift] = '0' ;
        }
    }
    unsigned j = 0 ; // This is used to access bits of this
                     // i is used as a counter for the result
    for (; i<max_size; i++) {
        // Determine the value of this bit
        if (i-shift == 32) {
            buffer[i-shift] = '\0' ;
            tmp = result ;
            result = Strings::save(result,buffer) ;
            Strings::free(tmp) ;
            shift = shift+32 ;
        }
        // Determine value of this bit.
        if (GET_BIT(_xvalue,(_size-1)-j)) {
            r = 'X' ;
            convert_to_int = 0 ;
        } else if (GET_BIT(_zvalue,(_size-1)-j)) {
            r = 'Z' ;
            convert_to_int = 0 ;
        } else if (GET_BIT(_value,(_size-1)-j)) {
            r = '1' ;
        } else {
            r = '0' ;
        }
        buffer[i-shift] = (char)r ;
        j++ ;
    }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    if ((formal_type && formal_type->IsArray()) || size) {
        buffer[i-shift] = '\0' ; // terminate the string properly :
        tmp = result ;
        result = Strings::save(result,buffer,"\"") ; // Terminate with a " :

        if (size) {
            //return size number LSB bits.. i.e with higher indices in the right
            unsigned long ulong_size = (unsigned long) size ; // JJ: 130110 cast size to ulong to bypass size comparison Coverity issue, as Strings::len returns ulong also
            unsigned long front = (Strings::len(result) - 1) /* for " */ - ulong_size ; // JJ: 130110 must make fron ulong also to avoid overflow.
            char *tmp1 = result ;
            // Viper 8300: for XZ we create a string literal and not bit string literal
            if (HasXZ()) {
                result = Strings::save("\"", result+front) ;
            } else {
                result = Strings::save("B\"", result+front) ;
            }
            Strings::free(tmp1) ;
        }
        Strings::free(tmp) ;

        // Viper 7501 : Create string aggregate for array type formal where the
        // element type is enum. Create an aggregate with enum names as aggregate.
        // For bit and std_logic create bit string literal as there the bit characters are
        // matched by value and not by position unlike other enumerated types.
        VhdlIdDef *element_type = formal_type ? formal_type->ElementType() : 0 ;

        // Viper 8300: for std logic/bit/character type enumeration literals create aggregate.
        // As per popular simulator for vectors direct string value is mapped to vhdl for
        // example 1 is mapped to '1' 0 to '0', x to 'X' ...
        if (element_type && (element_type->IsStdULogicType() || element_type->IsStdBitType() || element_type->IsStdCharacterType())) {
            // Create an aggregate
            unsigned len = Strings::len(result) ;
            char* curr = Strings::save("(") ;
            unsigned first = 1 ;
            unsigned n = HasXZ() ? 1 : 2 ;
            for ( ; n < len-1 ; n++) {
                char* tmp1 = curr ;
                char enum_str[4] ;
                ::sprintf(enum_str, "%c%c%c", '\'', result[n], '\'') ;
                if (!first) {
                    curr = Strings::save(curr, ",", enum_str) ;
                } else {
                    curr = Strings::save(curr, enum_str) ;
                }
                first = 0 ;
                Strings::free(tmp1) ;
            }
            char* tmp1 = curr ;
            curr = Strings::save(curr, ")") ;
            Strings::free(tmp1) ;
            Strings::free(result) ;
            return curr ;
        }

        if (element_type && element_type->IsEnumerationType() && !element_type->IsStdULogicType() &&
                                                                 !element_type->IsStdBitType() &&
                                                                 !element_type->IsStdCharacterType()) {
            char* start = ::strchr(result, '\"') ;
            if (start) {
                char *bit_string = result ;
                unsigned enum_not_found = 0 ;
                char *tmp2 ;
                result = Strings::save("(") ;
                char *curr = 0 ;
                unsigned first = 1 ;
                for (curr = start + 1; *curr != '\"'; curr++) {
                    int pos = *curr - '0' ;
                    VhdlIdDef* elem = element_type->GetEnumAt((unsigned)pos) ;
                    if (!elem) {
                        enum_not_found = 1 ;
                        break ;
                        // enum not found back off!
                    }
                    tmp2 = result ;
                    result = Strings::save(result,(first) ? "" : ",", elem->Name()) ;
                    first = 0 ;
                    Strings::free( tmp2 ) ;
                }
                tmp2 = result ;
                result = Strings::save(result,")") ;
                Strings::free( tmp2 ) ;
                if (enum_not_found) {
                    // restore old value
                    Strings::free(result) ;
                    result = bit_string ;
                } else {
                    Strings::free(bit_string) ;
                }
            }
        }
        return result ;
    }
#else
    (void) formal_type ;
#endif //VERILOG_DUAL_LANGUAGE_ELABORATION
    if (convert_to_int) {
        // This string only consists of ones and zeros, so convert integer
        Strings::free(result) ;
        result = Strings::itoa(Integer()) ; // VIPER #2362
    } else {
        // terminate the string properly :
        buffer[i-shift] = '\0' ;
        tmp = result ;
        // Terminate with a " :
        result = Strings::save(result,buffer,"\"") ;
        Strings::free(tmp) ;
    }

    return result ;
}
char *VeriTimeLiteral::ImageVhdl(VhdlIdDef * /*formal_type*/, unsigned /*size*/) const
{
    return Image() ;
}

/******************************** Integer **************************************/

int
VeriIntVal::Integer() const
{
    return _num ;
}

int
VeriRealVal::Integer() const
{
    //return (int)_num ; // C-cast.
    return (int)VeriNode::RoundToNearestInteger(_num) ;
}

int
VeriConstVal::Integer() const
{
    // Modifications for '1 type values: '1 is -1 in integer
    // all other values ('0, 'x and 'z) will be 0 in integer
    if (!_value) return -1 ;
    if (_is_unsized_bit) {
        // Just check the LSB bit of the value for unsized single bit literal
        if (_value[0]) return -1 ;
        return 0 ;
    }

    int result = 0 ;
    for (unsigned i=0; i<_size; i+=8) {
        // Can't do larger than integer values. Chop-off MSB side :
        // VIPER #2623 : Can't even do this for i==32, since we are 'left shifting'
        // an unsigned value 'i' bits which will have no effect, so, it's not required.
        if (i>=(unsigned)sizeof(int)*8) break ;
        // Do this one byte at a time :
        unsigned one_byte = (unsigned)_value[i/8] ;
        result |= (int)(one_byte<<i) ;
    }
    // VIPER #2623 : Consider the signedness of this value only if it's less than 32 bit.
    // For 32 bit values, we already computed it correctly above and for more than 32 bit
    // values we overflowed. So, no need to do anything in those cases:
    // FIXME: For 32'b11111111111111111111111111111111, we can't return 4294967295 (2^32-1),
    // we return -1. This is becasue the return type of this function is 'int'.
    // For 64 bit machines, it calculates this correctly but not 64'b111...111 (64 1s).
    if (_is_signed && (GET_BIT(_value, (_size-1))) && (_size < ((unsigned)sizeof(int)*8))) {
        // This constant value is signed and the MSB bit is set, so, it is a negative number
        // and we need to return the negative of the 2's complement of this value:
        result = (1<<_size) - result ; // 2's complement of 'result'
        result = -result ; // Make it negative
    }
    return result ;
}
int
VeriTimeLiteral::Integer() const
{
    return 0 ; // FIXME : should multiple time unit with prefix number
}
/////////////// Constraint ////////////////////////////

VeriConstraint *VeriConstVal::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/)
{
    // It is always packed type and its packed size is same as its size
    unsigned size = Size(0) ;
    if (size == 0) return 0 ;
    // Create range constraint with size
    VeriConstraint *range = new VeriRangeConstraint((int)size - 1, 0) ;
    range->SetAsPacked() ;

    // Its type will be reg
    VeriConstraint *element_type = new VeriScalarTypeConstraint(VERI_REG) ;

    return new VeriArrayConstraint(range, element_type) ;
}
VeriConstraint *VeriIntVal::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/)
{
    return new VeriScalarTypeConstraint(VERI_INTEGER) ;
}
VeriConstraint *VeriRealVal::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/)
{
    return new VeriScalarTypeConstraint(VERI_REAL) ;
}
VeriConstraint *VeriTimeLiteral::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/)
{
    return new VeriScalarTypeConstraint(VERI_REALTIME) ;
}
