/*
 *
 * [ File Version : 1.129 - 2014/02/06 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_CONST_VAL_H_
#define _VERIFIC_VERI_CONST_VAL_H_

#include "VeriExpression.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VeriIdDef ;
class SaveRestore ;
class VhdlIdDef ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
class VhdlExpression ;
#endif

/* -------------------------------------------------------------- */

/*
   VeriConstVal, VeriIntVal and VeriRealVal are parse-tree nodes
   representing constant values in Verilog.
   They are created from lex, for different style constants.
   When they get processed for elaboration, we create their
   appropriate VeriValue constructs for them that traverse the
   expressions
*/
class VFC_DLL_PORT VeriConst : public VeriExpression
{
public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; // Resolve an expression (done after analysis of a module)
    virtual char *              Image() const = 0 ;                    // Return a allocated string that represents this constant.
    // Return a allocated string that represents this constant in VHDL form. if size > 0 then represent as a size length BitStringLiteral
    virtual char *              ImageVhdl(VhdlIdDef *formal_type, unsigned size) const = 0 ;
    virtual int                 Integer() const = 0 ;                  // Return a integer representation (correct if <32 bits and no 'x'or'z')
    virtual unsigned            Size(VeriDataFlow *df) const = 0 ;     // Size of the expression. Used in elaboration, but always valid for constants (set df to 0).
    virtual unsigned            Sign() const = 0 ;                     // Return sign (4.5.1 rules) of expression.

    virtual unsigned            HasXZ() const           { return 0 ; } // See if constant has x or z.
    virtual unsigned            IsConst() const         { return 1 ; } // See if this expression node is a constant.
    virtual unsigned            IsConstExpr() const     { return 1 ; } // constant expression

#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & 2770: Get the original string representation of this literal (return 0 for string literals):
    virtual const char *        GetOriginalNumber() const { return 0 ; }
    virtual void                SetOriginalNumber(const char* /*orig_value*/) { }
#endif

    virtual VeriConst *         ConstCast() const       { return const_cast<VeriConst*>(this) ; }  // Dynamic Cast to a VeriConstVal class (returns non-zero only if this expression is a constant value).
    // string 'type' info :
    virtual unsigned            IsString() const        { return 0 ; } // Indicate that this constant originated from a Verilog string.
    virtual void                SetAsString()           { } ;          // Interpret this constant value as a Verilog string.

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
protected: // Abstract class
    VeriConst() ;

    VeriConst(const VeriConst &veri_const, VeriMapForCopy &id_map_table) ;

// Persistence (Binary restore via SaveRestore class)
    explicit VeriConst(SaveRestore &save_restore) ;

public:
    virtual ~VeriConst() ;

protected: // Abstract class
    VeriConst(VeriConst &veri_const, unsigned use_original_linefile):VeriExpression(veri_const, use_original_linefile) {}

private:
    // Prevent compiler from defining the following
    VeriConst(const VeriConst &) ;            // Purposely leave unimplemented
    VeriConst& operator=(const VeriConst &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                ResolveLabel(VeriScope *scope, veri_environment environment, VeriExpression *target) ; // Resolves constant labels of the concat items of assignment patterns
    virtual void                LinkOnehot(VeriIdDef *onehot_var) ;               // Discard onehot variable if expression is not appropriate for onehot compare/assignment.

    virtual Array *             SplitTerm(unsigned master_width, const VeriIdDef *formal_id, unsigned instance_count, VeriPseudoTreeNode *node, Map *top_mods, Map *mod_map_tab, unsigned *select_in_up_dim) ;
    virtual void                CheckConstraint(VeriConstraint *target_constraint, veri_type_env env) ;
    virtual unsigned            IsSizedPackedExpr() ; // VIPER #8384

//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConst

/* -------------------------------------------------------------- */

/*
   VeriConstVal is for constant, unsigned, sized values.
   They represent an arbitrary (bit) length, constant value
   of 0,1,x and z (bit) values.
   Represented very efficiently in a stream of bits of a char*.
   Access macros GET_BIT() and SET_BIT() retrieve bits from the stream.
   Stored LSB first ! for convenient zero-padding...
*/
class VFC_DLL_PORT VeriConstVal : public VeriConst
{
public:
    // Construct string, based numbers and integers from Verilog lex text
    VeriConstVal(const char *txt, unsigned type, unsigned size = 0) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERICONSTVAL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual char *              Image() const ;                // Return a allocated string that represents this constant.
    // Return a allocated string that represents this constant in VHDL form. if size > 0 then represent as a size length BitStringLiteral
    virtual char *              ImageVhdl(VhdlIdDef *formal_type, unsigned size) const ;
    virtual int                 Integer() const ;              // Return a integer representation (correct if <32 bits and no 'x'or'z')
    virtual unsigned            Size(VeriDataFlow *df) const ; // Number of bits in the 'expression' (defined since this is constant).
    virtual unsigned            Sign() const ;
    unsigned                    IsSigned() const        { return _is_signed ; }  // Same as Sign().  to be consistent with accessor naming
    virtual unsigned            HasXZ() const           { return (_xvalue || _zvalue) ? 1 : 0 ; } // See if constant has x or z.

    // string 'type' info :
    virtual unsigned            IsString() const        { return _is_string ; } // Indicate that this constant originated from a Verilog string.
    virtual void                SetAsString()           { _is_string = 1 ; }    // Interpret this constant value as a Verilog string.

    // Unsized literal info:
    virtual unsigned            IsUnsizedConst() const  { return (_is_unsized_bit || _is_unsized_number) ? 1 : 0 ; }    // Indicate that this constant is an unsized bit like '1 or unsized number like 'b11
    virtual unsigned            IsUnsizedBit() const    { return _is_unsized_bit ; }    // Indicate that this constant is an unsized bit like '1
    virtual unsigned            IsUnsizedNumber() const { return _is_unsized_number ; } // Indicate that this constant is an unsized number like 'b11

    // Member accessor methods
    unsigned                    GetMemSize() const      { return _size/8 + 3 ; } // The 3 is padding for octal values
    unsigned char *             GetValue() const        { return _value ; }
    unsigned char *             GetZValue() const       { return _zvalue ; }
    unsigned char *             GetXValue() const       { return _xvalue ; }

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
#ifdef VERILOG_PRESERVE_LITERALS
    // VIPER #2587 & 2770: Get the original string representation of this literal (return 0 for string literals):
    virtual const char *        GetOriginalNumber() const { return _orig_value ; }
    virtual void                SetOriginalNumber(const char* orig_value) { if (!orig_value) return ; Strings::free(_orig_value) ; _orig_value = Strings::save(orig_value) ; }
#endif

    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table)const;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriConstVal(const VeriConstVal &const_val, VeriMapForCopy &id_map_table) ;

// Persistence (Binary restore via SaveRestore class)
    explicit VeriConstVal(SaveRestore &save_restore) ;

    virtual ~VeriConstVal() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriConstVal>(this, name) ; }

protected:
    VeriConstVal(VeriConstVal &veri_const, unsigned use_original_linefile) : VeriConst(veri_const, use_original_linefile),
        _size(veri_const._size), _is_signed(veri_const._is_signed), _is_string(veri_const._is_string),
        _is_unsized_bit(veri_const._is_unsized_bit), _is_unsized_number(veri_const._is_unsized_number),
        _value(veri_const._value), _zvalue(veri_const._zvalue), _xvalue(veri_const._xvalue)
#ifdef VERILOG_PRESERVE_LITERALS
        , _orig_value(veri_const._orig_value)
#endif
       { veri_const._size = 0 ; veri_const._is_signed = 0 ; veri_const._is_string = 0 ;
         veri_const._is_unsized_bit = 0 ; veri_const._is_unsized_number = 0 ;
         veri_const._value = 0 ; veri_const._zvalue = 0 ; veri_const._xvalue = 0 ;
#ifdef VERILOG_PRESERVE_LITERALS
         veri_const._orig_value = 0 ;
#endif
       }

private:
    // Prevent compiler from defining the following
    VeriConstVal() ;                                // Purposely leave unimplemented
    VeriConstVal(const VeriConstVal &) ;            // Purposely leave unimplemented
    VeriConstVal& operator=(const VeriConstVal &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual VeriBaseValue *     StaticEvaluateInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    // VIPER #3528: Please look into the constructor while changing the size of this bit field '_size'.
    unsigned       _size:28 ;             // Bit-size of the value (mem_size will be determined from this - VIPER #1608)
    unsigned       _is_signed:1 ;         // Flag the constant as 'signed'
    unsigned       _is_string:1 ;         // Flag the constant as a 'string' type constant (originated from a Verilog string).
    // VIPER #1740: add two flags to differentiate it from the normal sized based constants
    unsigned       _is_unsized_bit:1 ;    // Flag the constant as unsized bit (ex: '1 not 'b1010 or 4'b1010)
    unsigned       _is_unsized_number:1 ; // Flag the constant as unsized number (ex: 'b1010 not 4'b1010 or '1)
    unsigned char *_value ;               // Bit-stream in 8 bit chunks (there are size/8 + 1 chars)
    unsigned char *_zvalue ;              // Parallel bit-stream for 'z' bits. NULL if no z is constant.
    unsigned char *_xvalue ;              // Parallel bit-stream for 'x' bits. NULL if no x is constant.
#ifdef VERILOG_PRESERVE_LITERALS
    char          *_orig_value ;          // VIPER #2587 & #2770: Preserve the original value string (radix and value) from the input RTL.
#endif
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriConstVal

/* -------------------------------------------------------------- */

/*
  VeriIntVal representing an integer constant in Verilog parse tree
*/
class VFC_DLL_PORT VeriIntVal : public VeriConst
{
public:
    explicit VeriIntVal(int n) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIINTVAL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;
    virtual unsigned            IsInt() const             { return 1 ; }

    virtual char *              Image() const ;     // Return a allocated string that represents this constant.
    // Return a allocated string that represents this constant in VHDL form. if size > 0 then represent as a size length BitStringLiteral
    virtual char *              ImageVhdl(VhdlIdDef *formal_type, unsigned size) const ;
    virtual int                 Integer() const ;   // Return a integer representation (correct if <32 bits and no 'x'or'z')
    virtual unsigned            Size(VeriDataFlow *df) const ;
    virtual unsigned            Sign() const ;

    int                         GetNum() const { return _num ; }
    virtual unsigned            IsUnsizedConst() const  { return 1 ; } // integer literal is always unsized constant
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table)const;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriIntVal(const VeriIntVal &int_val, VeriMapForCopy &id_map_table) ;

// Persistence (Binary restore via SaveRestore class)
    explicit VeriIntVal(SaveRestore &save_restore) ;

    virtual ~VeriIntVal() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriIntVal>(this, name) ; }

protected:
    VeriIntVal(VeriIntVal &veri_const, unsigned use_original_linefile) : VeriConst(veri_const, use_original_linefile), _num(veri_const._num) { veri_const._num = 0 ; }

private:
    // Prevent compiler from defining the following
    VeriIntVal() ;                              // Purposely leave unimplemented
    VeriIntVal(const VeriIntVal &) ;            // Purposely leave unimplemented
    VeriIntVal& operator=(const VeriIntVal &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual VeriBaseValue *     StaticEvaluateInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlExpression *    ConvertExpr() const ;
#endif

protected:
    int _num ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriIntVal

/* -------------------------------------------------------------- */

/*
   VeriRealVal representing a floating point constant in Verilog parse tree
*/
class VFC_DLL_PORT VeriRealVal : public VeriConst
{
public:
    explicit VeriRealVal(double n) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERIREALVAL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                Resolve(VeriScope *scope, veri_environment environment) ; // Resolve an expression (done after analysis of a module)

    virtual unsigned            IsReal() const             { return 1 ; }

    virtual char *              Image() const ;     // Return a allocated string that represents this constant.
    // Return a allocated string that represents this constant in VHDL form. if size > 0 then represent as a size length BitStringLiteral
    virtual char *              ImageVhdl(VhdlIdDef *formal_type, unsigned size) const ;
    virtual int                 Integer() const ;   // Return a integer representation (correct if <32 bits and no 'x'or'z')
    virtual unsigned            Size(VeriDataFlow *df) const ;
    virtual unsigned            Sign() const ;

    double                      GetNum() const          { return _num ; }
    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression

    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table)const;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriRealVal(const VeriRealVal &real_val, VeriMapForCopy &id_map_copy) ;

// Persistence (Binary restore via SaveRestore class)
    explicit VeriRealVal(SaveRestore &save_restore) ;

    virtual ~VeriRealVal() ;

    virtual VeriExpression *GetNameExtendedExpression(char *name) { return new VeriNameExtendedExpression<VeriRealVal>(this, name) ; }

protected:
    VeriRealVal(VeriRealVal &veri_const, unsigned use_original_linefile) : VeriConst(veri_const, use_original_linefile), _num(veri_const._num) { veri_const._num = 0 ; }

private:
    // Prevent compiler from defining the following
    VeriRealVal() ;                               // Purposely leave unimplemented
    VeriRealVal(const VeriRealVal &) ;            // Purposely leave unimplemented
    VeriRealVal& operator=(const VeriRealVal &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual VeriBaseValue*      StaticEvaluateInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    virtual VhdlExpression *    ConvertExpr() const ;
#endif

protected:
    double _num ;
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriRealVal

/* -------------------------------------------------------------- */
/*
  VeriTimeLiteral representing constant time literal in Verilog parse tree
*/
class VFC_DLL_PORT VeriTimeLiteral : public VeriConst
{
public:
    explicit VeriTimeLiteral(char *literal) ;

    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VERI_CLASS_ID       GetClassId() const      { return ID_VERITIMELITERAL; } // Defined in VeriClassIds.h

    // Visitor design pattern
    virtual void                Accept(VeriVisitor &v) ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual char *              Image() const ;     // Return a allocated string that represents this constant.
    // Return a allocated string that represents this constant in VHDL form. if size > 0 then represent as a size length BitStringLiteral
    virtual char *              ImageVhdl(VhdlIdDef *formal_type, unsigned size) const ; // Return a allocated string that represents this constant in VHDL form.
    virtual int                 Integer() const ;   // Return a integer representation (correct if <32 bits and no 'x'or'z')
    virtual unsigned            Size(VeriDataFlow *df) const ;
    virtual unsigned            IsStep() const ; // VIPER #6961 : For 1step
    virtual unsigned            Sign() const ;

    const char *                GetLiteral() const { return _literal ; }

    virtual VeriTypeInfo *      CheckType(VeriTypeInfo *expected_type, veri_type_env environment) ; // Check the type of expression
    virtual VeriExpression *    CopyExpression(VeriMapForCopy &id_map_table)const;

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    VeriTimeLiteral(const VeriTimeLiteral &int_val, VeriMapForCopy &id_map_table) ;

// Persistence (Binary restore via SaveRestore class)
    explicit VeriTimeLiteral(SaveRestore &save_restore) ;

    virtual ~VeriTimeLiteral() ;

private:
    // Prevent compiler from defining the following
    VeriTimeLiteral() ;                              // Purposely leave unimplemented
    VeriTimeLiteral(const VeriTimeLiteral &) ;            // Purposely leave unimplemented
    VeriTimeLiteral& operator=(const VeriTimeLiteral &) ; // Purposely leave unimplemented

public:
    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual VeriBaseValue *     StaticEvaluateInternal(int contextSize, ValueTable *n, VeriConstraint *target_constraint, unsigned expect_nonconst) const;
    virtual verific_int64       StaticSizeSignInternal(ValueTable *tab, const VeriConstraint *target_context) const ; // Viper #7711:return  in 64 bits
    virtual VeriConstraint *    EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df) ;

protected:
    char *_literal ; // Store time literal like 1.0ps in string format
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class VeriTimeLiteral

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_CONST_VAL_H_
