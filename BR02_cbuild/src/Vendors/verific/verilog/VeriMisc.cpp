/*
 *
 * [ File Version : 1.268 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Strings.h"
#include "Array.h"
#include "Set.h"
#include "Map.h"

#include "VeriModule.h"
#include "VeriMisc.h"
#include "veri_file.h"
#include "veri_tokens.h"
#include "VeriId.h"
#include "VeriExpression.h"
#include "VeriStatement.h"
#include "VeriModuleItem.h"
#include "VeriScope.h"
#include "VeriLibrary.h"

#include "VeriConstVal.h"
#include "../vhdl/VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VeriStrength::VeriStrength(unsigned lval, unsigned rval) :
    VeriTreeNode(),
    _lval(lval),
    _rval(rval) {}
VeriStrength::~VeriStrength() {}

VeriNetRegAssign::VeriNetRegAssign(VeriExpression *lval, VeriExpression *val) :
    VeriTreeNode(),
    _lval(lval),
    _val(val)
{
}
VeriNetRegAssign::~VeriNetRegAssign() {
    delete _lval ;
    delete _val ;
}

VeriDefParamAssign::VeriDefParamAssign(VeriName *lval, VeriExpression *val) :
    VeriTreeNode(),
    _lval(lval),
    _val(val)
{
    // VIPER #8138: Defparams cannot be pointing to some functions:
    if (_lval) _lval->SetFunctionType(0) ;
}
VeriDefParamAssign::~VeriDefParamAssign() {
    delete _lval ;
    delete _val ;
}

VeriCaseItem::VeriCaseItem(Array *conditions, VeriStatement *stmt) : VeriTreeNode()
    , _conditions(conditions)
    , _stmt(stmt)
    , _scope(0)
{
}
// New constructor, needed to represent pattern matching case item. Implicit identifiers
// of pattern should be declared in this scope
VeriCaseItem::VeriCaseItem(Array *conditions, VeriStatement *stmt, VeriScope *scope) : VeriTreeNode()
    , _conditions(conditions)
    , _stmt(stmt)
    , _scope(scope)
{}
VeriCaseItem::~VeriCaseItem()
{
    delete _stmt ;
    unsigned i ;
    VeriExpression *cond ;
    FOREACH_ARRAY_ITEM(_conditions, i, cond) delete cond ;
    delete _conditions ;
    delete _scope ;
}

VeriCaseOperatorItem::VeriCaseOperatorItem(Array *conditions, VeriExpression *property_expr) : VeriTreeNode()
    , _conditions(conditions)
    , _property_expr(property_expr)
{
}
VeriCaseOperatorItem::~VeriCaseOperatorItem()
{
    delete _property_expr ;
    unsigned i ;
    VeriExpression *cond ;
    FOREACH_ARRAY_ITEM(_conditions, i, cond) delete cond ;
    delete _conditions ;
}

void VeriCaseOperatorItem::Resolve(VeriScope *scope, VeriExpression *case_condition)
{
    // case items are expressions :
    VeriExpression *condition ;
    unsigned i ;
    VeriIdDef *case_id = (case_condition) ? case_condition->FullId() : 0 ;
    veri_environment cond_env = VERI_EXPRESSION ;
    FOREACH_ARRAY_ITEM(_conditions, i, condition) {
        if (!condition) continue ;

        // Resolve the case item as a possible onehot encoded expression
        condition->ResolveOnehot(scope, cond_env) ;

        // Check onehot compliance or discard as onehot comparison.
        condition->LinkOnehot(case_id) ;

        // Check the type of condition
        if (case_condition) case_condition->CheckAssignmentContext(condition, CASE_CONDITION) ;
    }

    // Resolve property expression :
    if (_property_expr) _property_expr->Resolve(scope, VERI_UNDEF_ENV) ;
}

VeriGenerateCaseItem::VeriGenerateCaseItem(Array *conditions, VeriModuleItem *item, VeriScope *scope) :
    VeriTreeNode(),
    _conditions(conditions),
    _item(item),
    _scope(scope)
{
    if (_scope) _scope->SetGenerateScope() ;
}
VeriGenerateCaseItem::~VeriGenerateCaseItem() {
    delete _item ;
    unsigned i ;
    VeriExpression *cond ;
    FOREACH_ARRAY_ITEM(_conditions, i, cond) delete cond ;
    delete _conditions ;
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        VeriIdDef *block_id = _scope ? _scope->GetOwner(): 0 ;
        if (block_id) block_id->SetModuleItem(0) ;
    }
//#endif // VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    delete _scope ;
}

// Simple module path constructor
VeriPath::VeriPath(Array *in_terminals, unsigned polarity_oper, unsigned path_token, Array *out_terminals) :
    VeriTreeNode(),
    _polarity_operator(polarity_oper),
    _path_token(path_token),
    _edge(0),
    _in_terminals(in_terminals),
    _out_terminals(out_terminals),
    _out_polarity_operator(0),
    _data_source(0)
{
#if 0
    // V2K LRM : 14.2.2 - parallel path may have only one input term desc
    // and only one output terminal descriptor
    if ((path_token == VERI_LEADTO) && (out_terminals->Size() > 1) || (in_terminals->Size() > 1)) {
        Warning("parallel path description may only have single input terminal descriptor and single output terminal descriptor") ;
    }
#endif
}

// Edge-sensitve module path constructor
VeriPath::VeriPath(unsigned edge, Array *in_terminals, unsigned path_token, Array *out_terminals, unsigned polarity_oper, VeriExpression *data_source) :
    VeriTreeNode(),
    _polarity_operator(0),
    _path_token(path_token),
    _edge(edge),
    _in_terminals(in_terminals),
    _out_terminals(out_terminals),
    _out_polarity_operator(polarity_oper),
    _data_source(data_source)
{
#if 0
    // V2K LRM : 14.2.2 - parallel path may have only one input term desc
    // and only one output terminal descriptor
     if ((path_token == VERI_LEADTO) && (out_terminals->Size() > 1) || (in_terminals->Size() > 1)) {
         Warning("parallel path description may only have single input terminal descriptor and single output terminal descriptor") ;
    }
#endif
}

VeriPath::VeriPath(unsigned edge, Array *in_terminals, unsigned in_polarity_oper, unsigned path_token, Array *out_terminals, unsigned out_polarity_oper, VeriExpression *data_source) :
    VeriTreeNode(),
    _polarity_operator(in_polarity_oper),
    _path_token(path_token),
    _edge(edge),
    _in_terminals(in_terminals),
    _out_terminals(out_terminals),
    _out_polarity_operator(out_polarity_oper),
    _data_source(data_source)
{}

VeriPath::~VeriPath() {
    unsigned i ;
    VeriExpression *terminal ;
    FOREACH_ARRAY_ITEM(_in_terminals, i, terminal) delete terminal ;
    delete _in_terminals ;
    FOREACH_ARRAY_ITEM(_out_terminals, i, terminal) delete terminal ;
    delete _out_terminals ;
    delete _data_source ;
}
/*
VeriDelayOrEventControl::VeriDelayOrEventControl(VeriExpression *delay_control, VeriExpression *repeat_event, Array *event_control) :
    VeriTreeNode(),
    _delay_control(delay_control),
    _repeat_event(repeat_event),
    _event_control(event_control)
    ,_is_cycle_delay(0)
{
    // VIPER 2766 : @* not allowed in '95 mode :
    if (IsVeri95() && _event_control && _event_control->Size()==0) {
        Error("%s not allowed in this dialect. Use v2k mode","@*") ;
    }
}
VeriDelayOrEventControl::~VeriDelayOrEventControl() {
    delete _delay_control ;
    delete _repeat_event ;
    VeriExpression *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_event_control,i,expr) delete expr ;
    delete _event_control ;
}
*/
// Base class for all config rules (VeriInstanceConfig, VeriCellConfig and VeriDefaultConfig)
VeriConfigRule::VeriConfigRule(Array *liblist) :
    VeriTreeNode(),
    _liblist(liblist)
{
    // FIXME: IEEE 1364.1 LRM section 13.3.1.5 - "The liblist clause":
    // The current library list is selected by the selection clauses. If no library
    // list clause is selected, or the selected library list is empty, then the
    // library list contains the single name which is the library in which the cell
    // containing the unbound instance is found (i.e., the parent cell's library).
    // NOTE: Our current approach of setting the global work library to the parent
    // library of a module whenever we go to process it takes care of this.
}

VeriConfigRule::~VeriConfigRule()
{
    // Free the library name strings:
    unsigned i ;
    char *lib_name ;
    FOREACH_ARRAY_ITEM(_liblist, i, lib_name) Strings::free(lib_name) ;
    delete _liblist ;
}

VeriInstanceConfig::VeriInstanceConfig(VeriName *inst_ref, Array *liblist) :
    VeriConfigRule(liblist),
    _inst_ref(inst_ref),
    _use_clause(0)
{ }

VeriInstanceConfig::VeriInstanceConfig(VeriName *inst_ref, VeriUseClause *use_clause) :
    VeriConfigRule(0),
    _inst_ref(inst_ref),
    _use_clause(use_clause)
{ }

VeriInstanceConfig::~VeriInstanceConfig()
{
    delete _inst_ref ;
    delete _use_clause ;
}

void
VeriInstanceConfig::AccumulateInstantiatedModuleNames(Set &mod_names, const char *current_lib)
{
    if (!_use_clause) return ;

    // Insert the name of the cell referred for this instance:
    VeriName *cell_name = _use_clause->GetCellName() ;
    // Check whether this cell name is a selected name and is from a different library:
    if (current_lib && cell_name && cell_name->IsHierName()) {
        VeriName *prefix = cell_name->GetPrefix() ;
        const char *lib_name = (prefix) ? prefix->GetName() : 0 ;
        // In that case we should not insert the cell name info the Set:
        if (!Strings::compare("work", lib_name) && !Strings::compare(current_lib, lib_name)) return ;
    }

    const char *name = (cell_name) ? cell_name->GetName() : 0 ;
    if (name) (void) mod_names.Insert(name) ;
}

VeriCellConfig::VeriCellConfig(VeriName *cell_ref, Array *liblist) :
    VeriConfigRule(liblist),
    _cell_ref(cell_ref),
    _use_clause(0)
{
    // IEEE 1364.1 LRM section 13.3.1.4 - "The cell clause":
    // It is an error if a library name is included in a cell selection clause and
    // the corresponding expansion clause is a library list expansion clause.
    if (_cell_ref && _cell_ref->IsHierName() && _liblist && _liblist->Size()) {
        // This is an error
        Error("configuration cell clause cannot contain a cell library and a liblist") ;
    }
}

VeriCellConfig::VeriCellConfig(VeriName *cell_ref, VeriUseClause *use_clause) :
    VeriConfigRule(0),
    _cell_ref(cell_ref),
    _use_clause(use_clause)
{ }

VeriCellConfig::~VeriCellConfig()
{
    delete _cell_ref ;
    delete _use_clause ;
}

void
VeriCellConfig::AccumulateInstantiatedModuleNames(Set &mod_names, const char *current_lib)
{
    VeriName *cell_name = (_use_clause) ? _use_clause->GetCellName() : 0 ;

    // FIXME: Should we remove the name of _cell_ref from the Set when _use_clause is specified?
    // Since, in that case, it is generally not instantited any more, but could be specifically
    // instantiated using instance clause. We never know that in this context!
    // FIXME: What if none of the two is not instantiated anywhere? We never know!
    // Other tools seem to do exactly the same thing as what we are doing here.

    // Check whether this cell name is a selected name and is from a different library:
    if (current_lib && cell_name && cell_name->IsHierName()) {
        VeriName *prefix = cell_name->GetPrefix() ;
        const char *lib_name = (prefix) ? prefix->GetName() : 0 ;
        // In that case we should not insert the cell name info the Set:
        if (!Strings::compare("work", lib_name) && !Strings::compare(current_lib, lib_name)) return ;
    }

    // Insert the name of the cell referred for this clause into the Set:
    const char *name = (cell_name) ? cell_name->GetName() : 0 ;
    if (name) (void) mod_names.Insert(name) ;
}

VeriDefaultConfig::VeriDefaultConfig(Array *liblist) :
    VeriConfigRule(liblist)
{
    // FIXME: IEEE 1364.1 LRM section 13.3.1.5 - "The liblist clause":
    // The current library list is selected by the selection clauses. If no library
    // list clause is selected, or the selected library list is empty, then the
    // library list contains the single name which is the library in which the cell
    // containing the unbound instance is found (i.e., the parent cell's library).
    // NOTE: Our current approach of setting the global work library to the parent
    // library of a module whenever we go to process it takes care of this.
}

VeriDefaultConfig::~VeriDefaultConfig()
{ }

VeriUseClause::VeriUseClause(VeriName *cell_ref, unsigned is_config, Array *param_values) :
    VeriTreeNode(),
    _cell_ref(cell_ref),
    _is_config(is_config),
    _param_values(param_values) // VIPER #7416
{
    // VIPER #7416 : Produce error if _param_values is not named association
    VeriExpression *assoc ;
    unsigned i ;
    unsigned empty_param_values = 0 ;
    FOREACH_ARRAY_ITEM(_param_values, i, assoc) {
        if (assoc && assoc->NamedFormal()) continue ;
        if (!assoc && (_param_values->Size()==1)) {
            // #()
            empty_param_values = 1 ; continue ;
        }
        if (assoc) assoc->Error("only named parameter assignment is allowed in use clause") ;
    }
    // Remove the null entry from empty parameter value list
    if (empty_param_values && _param_values) _param_values->Reset() ;
}

VeriUseClause::~VeriUseClause()
{
    delete _cell_ref ;
    unsigned i ;
    VeriExpression *assoc ;
    FOREACH_ARRAY_ITEM(_param_values, i, assoc) delete assoc ;
    delete _param_values ;
}

VeriModule *VeriUseClause::GetCell(VeriLibrary *current_lib) const
{
    if (!_cell_ref) return 0 ;

    // Cell ref is a ID.ID or a simple ID. For ID.ID, the first id is the library and
    // the second is the module. For Single id, it is the module inside work library:

    VeriLibrary *lib =  current_lib ; // Current working library
    if (_cell_ref->IsHierName()) {
        // It is library_name.module_name, need to find it in the specified library:
        VeriName *prefix = _cell_ref->GetPrefix() ;
        const char *lib_name = (prefix) ? prefix->GetName() : 0 ;
        // Viper #8125/8130 : VCS considers work to be the library named work
        // "work" means current library, so skip that:
        if (VeriNode::InRelaxedCheckingMode() || !Strings::compare("work", lib_name)) lib = veri_file::GetLibrary(lib_name) ;
        // Modelsim finds specified unit from current_lib when unit is not present
        // in specified library. But VCS does not suport that

        if (!VeriNode::InRelaxedCheckingMode()) {
            VeriModule *module = lib ? lib->GetModule(_cell_ref->GetName(), 1 /* case sensitive */) : 0 ;
            if (module) return module ;
            lib = current_lib ; // Will find unit in current lib now
        }
    }

    return (lib) ? lib->GetModule(_cell_ref->GetName(), 1 /* case sensitive */) : 0 ;
}

VeriClockingDirection::VeriClockingDirection(unsigned direction, unsigned edge, VeriDelayOrEventControl *delay_control) :
    VeriTreeNode(),
    _dir(direction),
    _edge(edge),
    _delay_control(delay_control)
{}
VeriClockingDirection::~VeriClockingDirection()
{
    delete _delay_control ; // Delete delay control
}
VeriProduction::VeriProduction(VeriDataType *data_type, VeriIdDef *prod_id, Array *formals, Array *rules, VeriScope *scope) :
    VeriTreeNode(),
    _data_type(data_type),
    _id(prod_id),
    _formals(formals),
    _items(rules),
    _ports(0),
    _scope(scope)
{
    // Set pointer back to me
    if (_id) _id->SetProduction(this) ;

    // Set-up port list : done by order
    _ports = new Array() ;

    unsigned i ;
    VeriExpression *port_decl ;
    FOREACH_ARRAY_ITEM(_formals, i, port_decl) {
        if (!port_decl) continue ;
        port_decl->AccumulatePorts(*_ports) ;
    }

    // Default direction setting : SV 1800 LRM 12.2: default direction is 'input'.
    // Need to do this NOW, since in Resolve(), we check against formals of function calls,
    // and functions can be used before declared.
    // NOTE : we can only set the default 'direction' here, not the default 'type'.
    // Default type is only known at 'Resolve' time (due to forward type decls).
    VeriIdDef *portid ;
    unsigned previous_dir = VERI_INPUT ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_ports, i, portid) {
        if (!portid) continue ;
        // get direction of this port :
        dir = portid->Dir() ;
        // Inherit previous port direction :
        if (!dir) {
            dir = previous_dir ;
            portid->SetDir(dir) ;
        }
        // update previous port direction
        previous_dir = dir ;
    }
}

VeriProduction::~VeriProduction()
{
    delete _data_type ; // Delete the data type

    // Delete the list of ansi port decalarations
    VeriAnsiPortDecl *port_decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_formals,i,port_decl) delete port_decl ;
    delete _formals ;

    // Delete the production items ie. the rs_rules
    VeriProductionItem *prod_item ;
    FOREACH_ARRAY_ITEM(_items,i,prod_item) delete prod_item ;
    delete _items ;

    // Delete the ordered list of ports
    delete _ports ;

    // VIPER #3369: Clear the data type pointer
    if (_id) _id->ClearAllBckPtrs() ;
    _id = 0 ; // Don't delete id which is owned by scope

    // Delete the local scope
    delete _scope ;
}
VeriProductionItem::VeriProductionItem(unsigned is_rand_join, VeriExpression *expr, Array *items) :
    VeriTreeNode(),
    _is_rand_join(is_rand_join),
    _expr(expr),
    _items(items),
    _w_spec(0),
    _code_blk(0)
{}
VeriProductionItem::~VeriProductionItem()
{
    delete _expr ;

    // Delete the list of items
    VeriStatement *stmt ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_items,i,stmt) delete stmt ;
    delete _items ;

    delete _w_spec ;
    delete _code_blk ;
}
#ifdef VERILOG_PATHPULSE_PORTS
// VIPER #6119 : Class to store input and output terminal descriptor associated with pulse control specparam
VeriPathPulseValPorts::VeriPathPulseValPorts(VeriName *input_port, VeriName *output_port) :
    VeriTreeNode(),
    _input_port(input_port),
    _output_port(output_port)
{}
VeriPathPulseValPorts::~VeriPathPulseValPorts()
{
    delete _input_port ;
    _input_port = 0 ;
    delete _output_port ;
    _output_port = 0 ;
}
#endif

/*********************************************************************************/
// Resolve
// Most of these objects can only be used in specific context that is already checked in yacc.
// So mostly, the incoming environment does not need to be checked.
/*********************************************************************************/

void VeriStrength::Resolve(VeriScope * /*scope*/, veri_environment /*environment*/)
{
}

void VeriNetRegAssign::Resolve(VeriScope *scope, veri_environment environment)
{
    // This is a net/reg assignment.
    // assign environment passed in from above (VeriAssign/VeriForce/VeriContinuousAssign).
    VeriTypeInfo *target_type = 0 ;
    if (_lval) _lval->Resolve(scope, environment) ;
    if (_lval && !_lval->IsConcat() && !_lval->IsMultiConcat() && !_lval->IsMultiAssignPattern() && !_lval->IsAssignPattern()) target_type = _lval->CheckType(0, NO_ENV) ;
    if (target_type && target_type->IsString()) { //It's added to ensure, non-constant repeater in multi-concat is legal when assigned to string type object: VIPER# 4258
        if (_val) _val->Resolve(scope, VERI_STRING_TARGET) ;
    } else if (environment == VERI_REG_LVALUE || environment == VERI_FORCE_LVALUE) { // VIPER #6110
        // For procedural continuous assignments pass VERI_PROCE_CONT_ASSIGN_VAL as environment
        if (_val) _val->Resolve(scope, VERI_PROCE_CONT_ASSIGN_VAL) ;
    } else {
        if (_val) _val->Resolve(scope, VERI_EXPRESSION) ;
    }
    VeriTypeInfo *base_type = (target_type) ? target_type->BaseType() : 0 ;
    unsigned net_type = (base_type) ? base_type->IsNetType() : 1 ; // 1 to avoid spurious errors
    delete target_type ;

    // Check type of _val passing target context. VIPER #2249.
    unsigned context = VERI_ASSIGN ;
    if ((environment == VERI_REG_LVALUE) || (environment == VERI_FORCE_LVALUE)) {
        // Viper 4682. If this net/reg assign is from procedural continuous assignment
        // then the _lval shall not be a memory word (array reference) or a bit-select
        // or a part-select of a variable. As per LRM 9.3 1364-2001. We only check for
        // assign here for force check is done under environment VERI_FORCE_LVALUE in
        // VeriIndexedId::Resolve
        if (_lval && environment == VERI_REG_LVALUE) {
            VeriExpression *idx = _lval->GetIndexExpr() ;
            Array *indexes = _lval->GetIndexes() ;
            if (idx || (indexes && indexes->Size() > 0) || (_lval->ContainsPartBitSelect())) {
                VeriIdDef *id = _lval->GetId() ;
                Error("bit-select or part-select is not allowed in a %s statement for non-net %s", "assign", id ? id->GetName() : "") ;
            }
        }
        // If this net/reg assign is from procedural continuous assignment, pass
        // token VERI_EQUAL. For continuous assignment pass VERI_ASSIGN
        context = VERI_EQUAL ;
    }
    if (_lval && _val) _lval->CheckAssignment(_val, context) ;

    // VIPER #5178: Check if non-net type variable assigned here has initial value:
    VeriIdDef *l_id = (_lval) ? _lval->GetId() : 0 ;
    if (IsSystemVeri() && (environment == VERI_NET_LVALUE) && l_id && l_id->GetInitialValue() && !net_type) {
        Error("variable %s is written by both continuous and procedural assignments", l_id->Name()) ;
    }
}

void VeriDefParamAssign::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // This is a defparam assignment.
    if (_lval) _lval->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ; // FIX ME : For now, pass in VERI_UNDEF_ENV environment, until we have a DefParamAssign environment.

    // Temporarily put in at least some checks (if lval is resolved) :
    VeriIdDef *param = (_lval) ? _lval->GetId() : 0 ;
    if (param && !param->IsParam()) {
        if (_lval) _lval->Error("%s is not a parameter", _lval->GetId()->Name()) ;
    }

    // Check that LRM 12.2.1 "a defparam statement in a hierarchy under a generate scope shall
    // not change a parameter value outside that hierarchy". (VIPER #1598)
    // Cannot check that directly, but for non-hierarchical names, it should be easy :
    // If a single-name parameter is not declared locally (then it must be declared above).
    // VIPER #5487 : Do not produce error here, this may be in false path of generate
    //if (_lval && !_lval->IsHierName() && scope && scope->Upper() && !scope->FindLocal(_lval->GetName())) {
        //Error("defparam under generate scope cannot change parameter value outside of hierarchy") ;
    //}

    if (_val) _val->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
}

void VeriCaseItem::Resolve(VeriScope *scope, VeriExpression *case_condition, unsigned case_style)
{
    if (_scope) scope = _scope ; // Set the scope if exists
    // case items are expressions :
    VeriExpression *condition ;
    unsigned i ;
    VeriIdDef *case_id = (case_condition) ? case_condition->FullId() : 0 ;

    veri_environment cond_env = VERI_EXPRESSION ;

    // If this is case_inside_item, condition is value range list. So pass
    // different environment :
    if (case_style == VERI_INSIDE) cond_env = VERI_VALUE_RANGE ;

    // VIPER #3033 : Type check should not be performed if either condition or
    // item condition is enumeration type. LRM says that enum type should be type
    // checked in relational operator, but other tools does not check type for
    // enum in relational/euality operators.
    //VeriDataType *condition_base_type = (case_condition) ? case_condition->GetBaseDataType(): 0 ;
    //unsigned cond_enum = (condition_base_type && condition_base_type->GetEnums()) ? 1 : 0 ; // Check if condition is enumeration

    FOREACH_ARRAY_ITEM(_conditions, i, condition) {
        if (!condition) continue ;
        // Resolve the case item as a possible onehot encoded expression
        condition->ResolveOnehot(scope, cond_env) ;

        // Check onehot compliance or discard as onehot comparison.
        condition->LinkOnehot(case_id) ;

        // VIPER #4835 : In randcase, case item condition cannot be negative,
        // test that for literals only
        if ((case_style == VERI_RANDCASE) && (condition->OperType() == VERI_MIN)) {
            VeriExpression *arg = condition->GetArg() ;
            // Produce error if operand is constant. Otherwise we will produce
            // error for -(-2) too.
            if (arg && arg->IsConst()) condition->Error("weight in randcase is negative") ;
        }
        //VeriDataType *expr_type = condition->GetBaseDataType() ; // Get type of condition
        // Do not check type if case condition or item condition is enum
        //if (cond_enum || (expr_type && expr_type->GetEnums())) continue ;

        // Check the type of condition
        if (case_condition) case_condition->CheckAssignmentContext(condition, CASE_CONDITION) ;
    }

    // Resolve statement :
    if (_stmt) _stmt->Resolve(scope, VERI_UNDEF_ENV) ;
}

void VeriGenerateCaseItem::Resolve(VeriScope *scope, VeriExpression *case_condition)
{
    unsigned i ;
    VeriExpression *condition ;
    // case items are constant expressions :
    FOREACH_ARRAY_ITEM(_conditions, i, condition) {
        if (condition) condition->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;

        // VIPER #7945: Check the type of condition:
        if (case_condition) case_condition->CheckAssignmentContext(condition, CASE_CONDITION) ;
    }
    // Resolve statement :
    if (_item) _item->Resolve(_scope, VERI_UNDEF_ENV) ;
}

void VeriPath::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Specify path delays/decls  (LRM A.7.4)

    // terminal descriptors are 'port' expressions... (only ports can show up there).
    unsigned i ;
    VeriExpression *terminal ;
    FOREACH_ARRAY_ITEM(_in_terminals, i, terminal) {
        if (terminal) terminal->Resolve(scope, VERI_PATH_EXPRESSION) ;
    }
    FOREACH_ARRAY_ITEM(_out_terminals, i, terminal) {
        if (terminal) terminal->Resolve(scope, VERI_PATH_EXPRESSION) ;
    }

    // data source is normal expression
    if (_data_source) _data_source->Resolve(scope, VERI_EXPRESSION) ;
}

/*
void VeriDelayOrEventControl::Resolve(VeriScope *scope, veri_environment environment)
{
    (void) environment ; // Needed to prevent "unused" build warnings

    // VIPER 2447 : Delay/timing control is not suported inside always_comb/always_latch
    if (environment == VERI_SPECIAL_ALWAYS) {
        Warning("%s in always_comb/always_latch is not allowed", _event_control ? "event control" : "timing control") ;
    }
    // VIPER #4204 : Delay/timing control is not suported inside final block
    if (environment == VERI_FINAL_BLOCK) {
        Warning("%s in final block is illegal", _event_control ? "event control" : "timing control") ;
    }

    // delay control is LRM A.2.2.3. VeriDelay is a normal expression ?
    if (_delay_control) _delay_control->Resolve(scope, VERI_DELAY_CONTROL) ;

    // repeat condition :
    if (_repeat_event) _repeat_event->Resolve(scope, VERI_EXPRESSION) ;

    // Now event control : list of event expressions :
    unsigned i ;
    VeriExpression *event_expr ;
    FOREACH_ARRAY_ITEM(_event_control, i, event_expr) {
        if (event_expr) event_expr->Resolve(scope, VERI_EVENT_CONTROL) ;
    }
}
*/

void VeriClockingDirection::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_delay_control) _delay_control->Resolve(scope, VERI_DELAY_CONTROL) ; // Resolve the delay control
}
void VeriProduction::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve the data type
    if (_data_type) _data_type->Resolve(scope, VERI_TYPE_ENV) ;

    // Resolve the formals
    unsigned i ;
    VeriAnsiPortDecl *port_decl ;
    FOREACH_ARRAY_ITEM(_formals, i, port_decl) {
        if (port_decl) port_decl->Resolve(scope, VERI_EXPRESSION) ;
    }

    // Resolve the production items
    VeriProductionItem *prod_item ;
    FOREACH_ARRAY_ITEM(_items, i, prod_item) {
        if (prod_item) prod_item->Resolve(scope, environment) ;
    }
}
void VeriProductionItem::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_is_rand_join && (!_items || (_items->Size()<2))) {
        // VIPER #6758: rand join production control must have at-least two production
        // items according to IEEE 1800 (2009) LRM section 18.17.5 and its BNF:
        Error("rand join production control must have at least two production items") ;
    }

    // Resolve rand join expression
    if (_expr) _expr->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve the production items
    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_items, i, stmt) {
        if (stmt) stmt->Resolve(scope, environment) ;
    }
}
#ifdef VERILOG_PATHPULSE_PORTS
void VeriPathPulseValPorts::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve the input terminal descriptor
    if (_input_port) _input_port->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve the output terminal descriptor
    if (_output_port) _output_port->Resolve(scope, VERI_EXPRESSION) ;
}
#endif
////////////////// Classes to represent type //////////////////////////////////

//unsigned VeriTypeInfo::issue_msg = 1 ; // Default : issue error/warning
// Constructor/destructor
VeriTypeInfo::VeriTypeInfo(): _nettype(0)  { }
VeriTypeInfo::~VeriTypeInfo() {}

VeriBuiltInType::VeriBuiltInType(unsigned type) :
    VeriTypeInfo(),
    _type(type),
    _sign(0)
{}
VeriBuiltInType::~VeriBuiltInType() {}

VeriArrayType::VeriArrayType(VeriTypeInfo *element_type, unsigned packed_dims):
    VeriTypeInfo(),
    _element_type(element_type),
    _packed_dims(packed_dims),
    _string_literal(0)
{}
VeriArrayType::~VeriArrayType()
{
    delete _element_type ;
}
VeriUnpackedArray::VeriUnpackedArray(VeriTypeInfo *element_type):
    VeriTypeInfo(),
    _element_type(element_type)
{}
VeriUnpackedArray::~VeriUnpackedArray()
{
    delete _element_type ;
}
VeriFixedSizeArray::VeriFixedSizeArray(VeriTypeInfo *element_type, unsigned unpacked_dims) :
    VeriUnpackedArray(element_type),
    _unpacked_dims(unpacked_dims)
{}
VeriFixedSizeArray::~VeriFixedSizeArray() {}

VeriAssociativeArray::VeriAssociativeArray(VeriTypeInfo *element_type, VeriTypeInfo *index_type):
    VeriUnpackedArray(element_type),
    _index_type(index_type)
{}
VeriAssociativeArray::~VeriAssociativeArray()
{
    delete _index_type ;
}

VeriDynamicArray::VeriDynamicArray(VeriTypeInfo *element_type):
    VeriUnpackedArray(element_type)
{}
VeriDynamicArray::~VeriDynamicArray() {}

VeriQueue::VeriQueue(VeriTypeInfo *element_type) :
    VeriUnpackedArray(element_type)
{}
VeriQueue::~VeriQueue() {}

VeriStructUnionType::VeriStructUnionType(unsigned is_struct, unsigned is_packed, unsigned is_tagged, Map *elements, VeriScope *scope) :
    VeriTypeInfo(),
    _elements(elements),
    _is_struct(is_struct),
    _is_packed(is_packed),
    _is_tagged(is_tagged),
    _sign(0),
    _scope(scope)
{ }
VeriStructUnionType::~VeriStructUnionType()
{
    // VIPER #6434 : Do not delete _elements here, it is not owned by this class
    // but owned by VeriStructUnion
    //MapIter mi ;
    //VeriTypeInfo *mem_type ;
    //FOREACH_MAP_ITEM(_elements, mi, 0, &mem_type) delete mem_type ;
    //delete _elements ;
    _elements = 0 ;
    _scope = 0 ;
}

VeriEnumType::VeriEnumType(VeriTypeInfo *base_type, Map *enum_ids):
    VeriTypeInfo(),
    _base_type(base_type),
    _enum_ids(enum_ids)
{}
VeriEnumType::~VeriEnumType()
{
    delete _base_type ;
    _enum_ids = 0 ; // Not owned by this class, owned by VeriEnum
}
VeriNamedType::VeriNamedType(VeriName *type_name) :
    VeriTypeInfo(),
    _name(type_name),
    _id(0),
    _is_virtual(0)
{   if (_name) _id = _name->GetId() ; }
VeriNamedType::VeriNamedType(VeriIdDef *type_id) :
    VeriTypeInfo(),
    _name(0),
    _id(type_id),
    _is_virtual(0)
{}
VeriNamedType::~VeriNamedType() { _name = 0 ; _id = 0 ; }

/******************************************************************************
*                            Member functions                                 *
*******************************************************************************/
unsigned VeriBuiltInType::IsSigned() const
{
    if (_sign == VERI_SIGNED) return 1 ; // Explicitly signed
    if (_sign == VERI_UNSIGNED) return 0 ; // Explicitly unsigned
    // Check whether type is signed by definition
    switch (_type) {
    case VERI_INTEGER :
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_GENVAR :
    case VERI_BYTE :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_SHORTREAL :
    case VERI_CHAR :
        return 1 ; // Signed types
    default : break ;
    }
    return 0 ;
}
unsigned VeriEnumType::IsSigned() const
{
    return (_base_type) ? _base_type->IsSigned(): 0 ;
}
void VeriBuiltInType::SetAsSigned()          { _sign = VERI_SIGNED ; }
void VeriBuiltInType::SetAsUnsigned()        { _sign = VERI_UNSIGNED ; }  // set type as unsigned
// unpacked array types :
unsigned VeriUnpackedArray::PackedDimension() const
{
    return (_element_type) ? _element_type->PackedDimension() : 0 ;
}
// Struct/union :
unsigned VeriStructUnionType::NumOfElements() const
{
    return (_elements) ? _elements->Size() : 0 ;
}
VeriIdDef* VeriStructUnionType::GetMemberAt(unsigned pos) const
{
    MapItem *item = (_elements && (_elements->Size() > pos)) ? _elements->GetItemAt(pos) : 0 ;
    return (item) ? (VeriIdDef*)item->Key() : 0 ;
}
VeriIdDef* VeriStructUnionType::GetMember(const char *name) const
{
    // Find member in the local scope
    return (_scope) ? _scope->FindLocal(name): 0 ;
}
VeriTypeInfo *VeriStructUnionType::GetMemberType(const char *name) const
{
    VeriIdDef *member = GetMember(name) ;
    if (!member) return 0 ; // No member of 'name'
    return (_elements) ? (VeriTypeInfo*)_elements->GetValue(member) : 0 ;
}
VeriTypeInfo *VeriStructUnionType::GetMemberTypeAt(unsigned pos)  const
{
    VeriIdDef *member = GetMemberAt(pos) ;
    if (!member) return 0 ; // No member of 'name'
    return (_elements) ? (VeriTypeInfo*)_elements->GetValue(member) : 0 ;
}
unsigned VeriStructUnionType::HasMembersOfEquivalentTypes() const
{
    if (!_is_struct) return 0 ;

    // Check whether element types are equivalent
    MapIter mi ;
    VeriIdDef *id ;
    VeriTypeInfo *ele_type, *prev_type = 0 ;
    FOREACH_MAP_ITEM(_elements, mi, &id, &ele_type) {
        if (!prev_type) {
            prev_type = ele_type ;
            continue ;
        }
        if (ele_type && !ele_type->IsEquivalent(prev_type, 0, 0, 0)) return 0 ; // not equivalent
        prev_type = ele_type ;
    }
    return 1 ; // All elements are of equivalent types
}
// Enum type :
unsigned VeriEnumType::NumOfEnums() const
{
    return (_enum_ids) ? _enum_ids->Size() : 0 ;
}
VeriIdDef *VeriEnumType::GetEnum(const char *name) const
{
    return (_enum_ids) ? (VeriIdDef*)_enum_ids->GetValue(name) : 0 ;
}
VeriIdDef *VeriEnumType::GetEnumAt(unsigned pos) const
{
    return (_enum_ids) ? (VeriIdDef*)_enum_ids->GetValueAt(pos) : 0 ;
}
// Named type :
VeriIdDef *VeriNamedType::GetNameId() const
{
    return _id ;
}
VeriIdDef *VeriNamedType::GetMember(const char *name)  const
{
    if (IsUnresolvedNameType()) return 0 ; // no resolved type id available.

    // Look in the local scope of this id (interface / modport or so..).
    VeriScope *scope = (_id) ? _id->LocalScope(): 0 ;
    // For interface instantiatation, find the name inside interface decl:
    if (!scope && _id && _id->IsInterfaceInst()) {
        VeriModule *inst_mod = _id->GetInstantiatedModule() ;
        scope = (inst_mod) ? inst_mod->GetScope() : 0 ;
    }
    VeriIdDef *id = 0 ;
    if (scope) {
        id = scope->FindLocal(name) ;
    }
    if (id || !scope) return id ;

    // VIPER #3806 : If this is class, 'name' can exist in its parent class, check that
    id = (_id && _id->IsClass()) ? scope->FindInBaseClass(name): 0 ;

    // Members defined in interface classes are also available from inherited
    // interface class having extends clause
    //if (!id && _id && _id->IsClass()) id = scope->FindInInterfaceClasses(name) ;
    return id ;
}
// VIPER #6550: Replace the old generate item with the newer one:
void
VeriGenerateCaseItem::SetItem(VeriModuleItem *new_item)
{
    // The application api should catch the old item and delete it
    // otherwise there will be memory leak:
    _item = new_item ;
}

/******************************************************************************
*                            Copy Routines                                    *
*******************************************************************************/
VeriTypeInfo *VeriBuiltInType::Copy() const
{
    VeriTypeInfo *ret_type = new VeriBuiltInType(_type) ;
    // VIPER #4120 : Consider sign during copy :
    if (IsSigned()) ret_type->SetAsSigned() ;
    return ret_type ;
}
VeriTypeInfo *VeriArrayType::Copy() const
{
    VeriTypeInfo *type = new VeriArrayType(_element_type ? _element_type->Copy(): 0, _packed_dims) ;
    if (_string_literal) type->SetAsStringLiteral() ;
    return type ;
}
VeriTypeInfo *VeriFixedSizeArray::Copy() const
{
    return new VeriFixedSizeArray(_element_type ? _element_type->Copy(): 0, _unpacked_dims) ;
}
VeriTypeInfo *VeriAssociativeArray::Copy() const
{
    return new VeriAssociativeArray(_element_type ? _element_type->Copy(): 0, _index_type ? _index_type->Copy(): 0) ;
}
VeriTypeInfo *VeriDynamicArray::Copy() const
{
    return new VeriDynamicArray(_element_type ? _element_type->Copy(): 0) ;
}
VeriTypeInfo *VeriQueue::Copy() const
{
    return new VeriQueue(_element_type ? _element_type->Copy(): 0) ;
}
VeriTypeInfo *VeriStructUnionType::Copy() const
{
    // VIPER #6434 : _elements is not owned by this class, so used _elements in copied type
    VeriTypeInfo *type = 0 ;
    // Copy element types and create new Map to store those
    //Map *new_elements = (_elements) ? new Map(POINTER_HASH, _elements->Size()) : 0 ;
    //MapIter mi ;
    //VeriIdDef *id ;
    //VeriTypeInfo *id_type ;
    //FOREACH_MAP_ITEM(_elements, mi, &id, &id_type) {
        //if (!id) continue ;
        //if (new_elements) (void) new_elements->Insert(id, id_type ? id_type->Copy(): 0) ;
    //}
    type = new VeriStructUnionType(_is_struct, _is_packed, _is_tagged, _elements, _scope) ;
    // VIPER #5368 : Set sign in copied type
    if (_sign) type->SetAsSigned() ;

    return type ;
}
VeriTypeInfo *VeriEnumType::Copy() const
{
    return new VeriEnumType((_base_type) ? _base_type->Copy(): 0, _enum_ids) ;
}
VeriTypeInfo *VeriNamedType::Copy() const
{
    VeriTypeInfo *ret_type = 0 ;
    if (_name) {
        ret_type = new VeriNamedType(_name) ;
    } else {
        ret_type = new VeriNamedType(_id) ;
    }
    if (_is_virtual) ret_type->SetIsVirtualInterface() ;
    return ret_type ;
}
/******************************************************************************
*                            Image Routines (for debug purpose only)          *
*******************************************************************************/
char *VeriBuiltInType::Image() const
{
    return Strings::save((IsSigned()?"signed ":""), VeriNode::PrintToken(_type)) ;
}
char *VeriArrayType::Image() const
{
    char *elem_type = (_element_type) ? _element_type->Image() : 0 ;
    char *num = Strings::itoa((int)_packed_dims) ;
    char *result = Strings::save((elem_type)?elem_type:"", " [packed dim count:", num, "]") ;
    Strings::free(num) ;
    Strings::free(elem_type) ;
    // _string_literal
    return result ;
}
char *VeriFixedSizeArray::Image() const
{
    char *elem_type = (_element_type) ? _element_type->Image() : 0 ;
    char *num = Strings::itoa((int)_unpacked_dims) ;
    char *result = Strings::save((elem_type)?elem_type:"", " [unpacked dim count:", num, "]") ;
    Strings::free(num) ;
    Strings::free(elem_type) ;
    return result ;
}
char *VeriAssociativeArray::Image() const
{
    char *elem_type = (_element_type) ? _element_type->Image() : 0 ;
    char *index_type = (_index_type) ? _index_type->Image() : 0 ;
    char *result = Strings::save((elem_type)?elem_type:"", " [", (index_type)?index_type:"", "]") ;
    Strings::free(index_type) ;
    Strings::free(elem_type) ;
    return result ;
}
char *VeriDynamicArray::Image() const
{
    return (_element_type) ? _element_type->Image(): 0 ;
}
char *VeriQueue::Image() const
{
    return (_element_type) ? _element_type->Image(): 0 ;
}
char *VeriStructUnionType::Image() const
{
    char *result = Strings::save((_is_struct)?"struct ":"union ",
                                 (_is_packed)?"packed ":"",
                                 (_is_tagged)?"tagged ":"",
                                 (_sign)?"signed ":"", " {\n") ;
    MapIter mi ;
    VeriIdDef *id ;
    VeriTypeInfo *id_type ;
    FOREACH_MAP_ITEM(_elements, mi, &id, &id_type) {
        if (!id) continue ;
        char *tmp = result ;
        char *elem_type = (id_type) ? id_type->Image() : 0 ;
        result = Strings::save(result, (elem_type)?elem_type:"", " ", id->Name(), " ;\n") ;
        Strings::free(elem_type) ;
        Strings::free(tmp) ;
    }
    char *tmp = result ;
    result = Strings::save(result, "}") ;
    Strings::free(tmp) ;
    return result ;
}
char *VeriEnumType::Image() const
{
    char *elem_type = (_base_type) ? _base_type->Image() : 0 ;
    char *result = Strings::save("enum ", (elem_type)?elem_type:"", " {") ;
    MapIter mi ;
    const char *id ;
    unsigned first = 1 ;
    FOREACH_MAP_ITEM(_enum_ids, mi, &id, 0) {
        if (!id) continue ;
        char *tmp = result ;
        result = Strings::save(result, (first)?"":", ", id) ;
        Strings::free(tmp) ;
        first = 0 ;
    }
    return result ;
}
char *VeriNamedType::Image() const
{
    return Strings::save((_is_virtual)?"virtual ":"", (_name)?_name->GetName():((_id)?_id->Name():"")) ;
}
/******************************************************************************
*                           Type checking                                     *
*******************************************************************************/
unsigned VeriTypeInfo::CheckAgainst(VeriTypeInfo * /*other*/, VeriTreeNode * /*from*/, VeriTreeNode::veri_type_env /*environment*/) const
{
    return 0 ;
}
unsigned VeriBuiltInType::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other) return 1 ; // Cannot do anything without value type
    if (IsTypeParameterType() || (other->IsTypeParameterType() || other->IsUnresolvedNameType())) return 1 ; // Do not check anything for type parameter type
    // If one side is property/sequence, do not check type
    if (IsInterconnectType() || IsPropertyType() || IsSequenceType() || IsUntyped() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;

    unsigned status = 1 ;
    // VIPER #2249 : Type checking for enumerated types
    // This checking should not issue error if
    // 1. Enumerated type is in value, other type is target
    // 2. Initial value of parameter of enumerated type is not enum variable/enum literal.
    // 3. Initial value of enum literal is not enum variable/enum literal.
    // 4. Return type of function is enum and return value is not enum variable/enum literal.
    //if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE) && (environment != VeriTreeNode::PARAM_INIT) && (environment != VeriTreeNode::RETURN_EXPR)
    // VIPER #8293 : Issue error if initial value of parameter of enumerated type is not enum variable/enum literal
    // and return type of function is enum and return value is not enum variable/enum literal
    if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE)
       && other->IsEnumeratedType()) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        status = 0 ;
    }
    // VIPER #2960: Check for chandle data type - only chandle type can be assigned to another chandle type:
    if (IsChandleType() != other->IsChandleType()) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    // Check for event type : an event variable can be assigned to another event variable or null
    // In property/sequence argument/default value, event expression can be used for event type port
    if ((IsEventType() != other->IsEventType()) && (environment != VeriTreeNode::PROP_SEQ_INIT_VAL)) {
        if (from) from->Error("cannot mix event and non-event types in this operation") ;
        status = 0 ;
    }
    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        status = 0 ;
    }
    // Prepare a flag to determine which type is target type and which one is value type.
    // This can be used to issue error more exactly. We can get this information from environment
    unsigned is_this_target = ((environment == VeriTreeNode::WRITE) || (environment == VeriTreeNode::INST_WRITE)) ? 1 : 0 ; // Flag to indicate if 'this' is target or not
    // One side is string type and other type is not string literal/string type
    // Viper 4712. String types can be assigned to packed array. 1800 LRM 4.7
    if (IsString() && (!other->IsStringLiteral() && !other->IsString() && !other->IsPacked())) {
        if (is_this_target) { // target string
            // VIPER #7331 : Produce different message for case item comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", other->IsPacked() ? "a packed" : "an unpacked", "a string") ;
            } else {
                if (from && (environment != VeriTreeNode::PARAM_INIT)) from->Error("assigning a %s type to a string requires a cast", other->IsPacked() ? "packed" : "unpacked") ;
            }
        } else { // value is string type, target is not string/string literal
            // VIPER #7331 : Produce different message for case item comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a string", other->IsPacked() ? "a packed" : "an unpacked") ;
            } else {
                if (from) from->Error("cannot assign a string to %s type", other->IsPacked() ? "a packed" : "an unpacked") ;
            }
        }
        return 0 ;
    }
    // One side is string type and other type is not string literal/string type
    if (!IsString() && other->IsString()) {
        if (is_this_target) { // target not string, value string
            // VIPER #7331 : Produce different message for case item comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a string", "a packed") ;
            } else {
                if (from) from->Error("cannot assign a string to %s type", "a packed") ;
            }
        } else { // target string, value not string
            // VIPER #7331 : Produce different message for case item comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a packed", "a string") ;
            } else {
                if (from && (environment != VeriTreeNode::PARAM_INIT)) from->Error("assigning a %s type to a string requires a cast", "packed") ;
            }
        }
        return 0 ;
    }
    // VIPER #2046 :A packed type cannot be assigned to unpacked type
    //if ((IsRealType() && !other->IsPacked() && !other->IsRealType()) || (IsPacked() && other->IsUnpacked() && !other->IsBuiltInType())) {
    if (other->IsUnpacked() && !other->IsBuiltInType()) {
        // VIPER #7331 : Produce different message for case item condition comparison
        if (environment == VeriTreeNode::CASE_CONDITION) {
            if (from) from->Error("illegal case item comparison of %s type with %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        } else {
            if (from) from->Error("cannot assign %s type to %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        }
        status = 0 ;
    }
    return status ;
}
unsigned VeriArrayType::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other) return 1 ; // Cannot do anything without value type
    // Do not check anything for type parameter type/unresolved name type
    if (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType()) return 1 ;
    // If one side is property/sequence, do not check type
    if (IsInterconnectType() || IsPropertyType() || IsSequenceType() || IsUntyped() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;
    unsigned status = 1 ;
    // VIPER #2249 : Type checking for enumerated types
    // This checking should not issue error if
    // 1. Enumerated type is in value, other type is target
    // 2. Initial value of parameter of enumerated type is not enum variable/enum literal.
    // 3. Initial value of enum literal is not enum variable/enum literal.
    // 4. Return type of function is enum and return value is not enum variable/enum literal.
    //if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE) && (environment != VeriTreeNode::PARAM_INIT) && (environment != VeriTreeNode::RETURN_EXPR)
    // VIPER #8293 : Issue error if initial value of parameter of enumerated type is not enum variable/enum literal
    // and return type of function is enum and return value is not enum variable/enum literal
    if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE)
       && other->IsEnumeratedType()) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        status = 0 ;
    }

    // VIPER #2960: Check for chandle data type - only chandle type can be assigned to another chandle type:
    if (other->IsChandleType()) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    // Check for event type : an event variable can be assigned to another event variable or null
    // In property/sequence argument/default value, event expression can be used for event type port
    if (other->IsEventType() && environment != VeriTreeNode::PROP_SEQ_INIT_VAL) {
        if (from) from->Error("cannot mix event and non-event types in this operation") ;
        status = 0 ;
    }
    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        status = 0 ;
    }
    // Prepare a flag to determine which type is target type and which one is value type.
    // This can be used to issue error more exactly. We can get this information from environment
    unsigned is_this_target = ((environment == VeriTreeNode::WRITE) || (environment == VeriTreeNode::INST_WRITE)) ? 1 : 0 ; // Flag to indicate if 'this' is target or not
    // One side is string type/string literal and other type is not string literal
    if (!_string_literal && other->IsString()) {
        if (is_this_target) { // target packed, value string
            // VIPER #7331 : Produce different message for case item comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a string", "a packed") ;
            } else {
                if (from) from->Error("cannot assign a string to %s type", "a packed") ;
            }
        } else { // target string, value packed
            // VIPER #7331 : Produce different message for case item comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a packed", "a string") ;
            } else {
                if (from && (environment != VeriTreeNode::PARAM_INIT)) from->Error("assigning a %s type to a string requires a cast", "packed") ;
            }
        }
        return 0 ;
    }
    // VIPER #2046 :A packed type cannot be assigned to unpacked type
    if (!_string_literal && (other->IsUnpacked() && !other->IsBuiltInType())) {
        // VIPER #7331 : Produce different message for case item condition comparison
        if (environment == VeriTreeNode::CASE_CONDITION) {
            if (from) from->Error("illegal case item comparison of %s type with %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        } else {
            if (from) from->Error("cannot assign %s type to %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        }
        status = 0 ;
    }
    return status ;
}
unsigned VeriUnpackedArray::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other) return 1 ; // Cannot do anything without value type
    // Do not check anything for type parameter type/unresolved name type
    if (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType()) return 1 ;
    // If one side is property/sequence, do not check type
    if (IsInterconnectType() || IsPropertyType() || IsSequenceType() || IsUntyped() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;

    unsigned status = 1 ;
    // VIPER #2249 : Type checking for enumerated types
    // This checking should not issue error if
    // 1. Enumerated type is in value, other type is target
    // 2. Initial value of parameter of enumerated type is not enum variable/enum literal.
    // 3. Initial value of enum literal is not enum variable/enum literal.
    // 4. Return type of function is enum and return value is not enum variable/enum literal.
    //if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE) && (environment != VeriTreeNode::PARAM_INIT) && (environment != VeriTreeNode::RETURN_EXPR)
    // VIPER #8293 : Issue error if initial value of parameter of enumerated type is not enum variable/enum literal
    // and return type of function is enum and return value is not enum variable/enum literal
    if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE)
       && other->IsEnumeratedType()) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        status = 0 ;
    }
    // VIPER #2960: Check for chandle data type - only chandle type can be assigned to another chandle type:
    if (other->IsChandleType()) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    // Check for event type : an event variable can be assigned to another event variable or null
    if (other->IsEventType()) {
        if (from) from->Error("cannot mix event and non-event types in this operation") ;
        status = 0 ;
    }
    // Prepare a flag to determine which type is target type and which one is value type.
    // This can be used to issue error more exactly. We can get this information from environment
    unsigned is_this_target = ((environment == VeriTreeNode::WRITE) || (environment == VeriTreeNode::INST_WRITE)) ? 1 : 0 ; // Flag to indicate if 'this' is target or not
    // One side is string type and other type is not string literal/string type
    if (other->IsString()) {
        if (is_this_target) { // target unpacked, value string
            // VIPER #7331 : Produce different message for case item condition comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a string", "an unpacked") ;
            } else {
                if (from) from->Error("cannot assign a string to %s type", "an unpacked") ;
            }
        } else { // target string, value unpacked
            // VIPER #7331 : Produce different message for case item condition comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "an unpacked", "a string") ;
            } else {
                if (from) from->Error("assigning a %s type to a string requires a cast", "unpacked") ;
            }
        }
        return 0 ;
    }
    if ((other->IsPacked() || other->IsBuiltInType()) && !other->IsStringLiteral()) {
        // VIPER #7331 : Produce different message for case item condition comparison
        if (environment == VeriTreeNode::CASE_CONDITION) {
            if (from) from->Error("illegal case item comparison of %s type with %s type", (is_this_target) ? "a packed": "an unpacked", (is_this_target) ? "an unpacked": "a packed") ;
        } else {
            if (from) from->Error("cannot assign %s type to %s type", (is_this_target) ? "a packed": "an unpacked", (is_this_target) ? "an unpacked": "a packed") ;
        }
        status = 0 ;
    }
    if (status && !other->IsUnpackedArrayType() && !other->IsStringLiteral()) {
        // One side is unpacked array, other type is not
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    unsigned this_unpacked_dims = UnpackedDimension() ;
    unsigned other_unpacked_dims = other->UnpackedDimension() ;
    if (this_unpacked_dims && other_unpacked_dims && (this_unpacked_dims != other_unpacked_dims)) {
        // Both target and value are unpacked, but with different dimensions
        if (from) from->Error("incompatible unpacked dimensions in assignment") ;
        status = 0 ;
    }
    unsigned this_packed_dims = PackedDimension() ;
    unsigned other_packed_dims = other->PackedDimension() ;
    // VIPER #2838 : Check the base types when target and value have same unpacked(not 0)
    // and packed dimension
    if (this_unpacked_dims && (this_unpacked_dims == other_unpacked_dims) && (this_packed_dims == other_packed_dims)) {
        unsigned other_bitsize = other->BaseTypeSize() ; // Get bit size of other's base type
        unsigned this_bitsize = BaseTypeSize() ; // Get bit size of its base type
        if (other_bitsize && this_bitsize && (other_bitsize != this_bitsize)) {
            // Both target and value contain base type, but with unequal size, error
            if (from) from->Error("array element widths (%d, %d) don't match", (is_this_target) ? this_bitsize : other_bitsize, (is_this_target) ? other_bitsize: this_bitsize) ;
            status = 0 ;
        }
    }
    if (this_unpacked_dims && (this_unpacked_dims == other_unpacked_dims)) {
        // VIPER #7092 : Do not check sign and state mismatch in elements
        // for actual/formal association
        unsigned ignore_sign_state = 0 ;
        if ((environment == VeriTreeNode::INST_READ) || (environment == VeriTreeNode::INST_WRITE)) ignore_sign_state = 1 ;
        unsigned equivalent = 1 ;
        //VeriTypeInfo *this_basetype = BaseType() ;
        //VeriTypeInfo *other_basetype = other->BaseType() ;
        VeriTypeInfo *this_basetype = ElementType() ;
        while (this_basetype && this_basetype->IsUnpackedArrayType()) {
            this_basetype = this_basetype->ElementType() ;
        }
        VeriTypeInfo *other_basetype = other->ElementType() ;
        while (other_basetype && other_basetype->IsUnpackedArrayType()) {
            other_basetype = other_basetype->ElementType() ;
        }
        // For unpacked array assignment each element should be of an equivalent type
        equivalent = (this_basetype && other_basetype) ? this_basetype->IsEquivalent(other_basetype, 1, from, ignore_sign_state) : 0 ;
        if (this_basetype && other_basetype && !equivalent) {
            if (from) from->Error("arrays have different elements") ;
            status = 0 ;
        }
        if (status && equivalent && this_basetype && this_basetype->IsClassType()) { // Both are equivalent class type
            // Check base class object not be assigned to derived class object
            if ((is_this_target && other_basetype && this_basetype->IsDerivedFrom(other_basetype)) || // This is target and is derived from value
                (!is_this_target && other_basetype && other_basetype->IsDerivedFrom(this_basetype))) { // Other is target and derived class
                if (from) from->Error("incompatible complex type assignment") ;
                status = 0 ;
            }
        }
    }

    return status ;
}
unsigned VeriFixedSizeArray::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other || IsTypeParameterType() || IsUnresolvedNameType() || (other->IsTypeParameterType() || other->IsUnresolvedNameType())) return 1 ; // Cannot check further
    // If one side is property/sequence, do not check type
    if (IsInterconnectType() || IsPropertyType() || IsSequenceType() || IsUntyped() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;

    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        return 0 ;
    }
    if (!VeriUnpackedArray::CheckAgainst(other, from, environment)) return 0 ; // error
    return 1 ;
}
unsigned VeriAssociativeArray::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other || (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType())) return 1 ; // Cannot check further
    // If one side is property/sequence, do not check type
    if (IsInterconnectType() || IsPropertyType() || IsSequenceType() || IsUntyped() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;

    // Check for associative array: cannot mix associative and non-associative arrays
    if (!other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        return 0 ;
    }
    // Both are associative array. Check index type of both arrays, they should be same:
    VeriTypeInfo *other_index_type = other->IndexType() ;
    if ((!_index_type && other_index_type) || (_index_type && !other_index_type)) {
        // Index type of this is '*', but not '*' for other
        if (from) from->Error("associative array keys do not match") ;
        return 0 ;
    }
    // Both index exists, they should be same
    if (_index_type && other_index_type && !_index_type->IsEqual(other_index_type, 0)) {
        if (from) from->Error("associative array keys do not match") ;
        return 0 ;
    }
    if (!VeriUnpackedArray::CheckAgainst(other, from, environment)) return 0 ; // error
    return 1 ;
}
unsigned VeriDynamicArray::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    // Do not check anything for type parameter type/unresolved name type
    if (!other || (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType())) return 1 ; // Cannot check further
    // If one side is property/sequence, do not check type
    if (IsInterconnectType() || IsPropertyType() || IsSequenceType() || IsUntyped() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;

    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        return 0 ;
    }
    if (!VeriUnpackedArray::CheckAgainst(other, from, environment)) return 0 ; // error
    return 1 ;
}
unsigned VeriQueue::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other || (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType())) return 1 ; // Cannot check further
    // If one side is property/sequence, do not check type
    if (IsPropertyType() || IsSequenceType() || IsUntyped() || IsInterconnectType() || other->IsInterconnectType() || other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped()) return 1 ;

    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        return 0 ;
    }
    if (!VeriUnpackedArray::CheckAgainst(other, from, environment)) return 0 ; // error
    return 1 ;
}
unsigned VeriStructUnionType::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other) return 1 ; // Cannot do anything without value type
    // Do not check anything for type parameter type/unresolved name type
    if (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType()) return 1 ;
    // If one side is property/sequence, do not check type
    if (other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped() || other->IsInterconnectType()) return 1 ;

    unsigned status = 1 ;
    // VIPER #2249 : Type checking for enumerated types
    // This checking should not issue error if
    // 1. Enumerated type is in value, other type is target
    // 2. Initial value of parameter of enumerated type is not enum variable/enum literal.
    // 3. Initial value of enum literal is not enum variable/enum literal.
    // 4. Return type of function is enum and return value is not enum variable/enum literal.
    //if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE) && (environment != VeriTreeNode::PARAM_INIT) && (environment != VeriTreeNode::RETURN_EXPR)
    // VIPER #8293 : Issue error if initial value of parameter of enumerated type is not enum variable/enum literal
    // and return type of function is enum and return value is not enum variable/enum literal
    if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE)
       && other->IsEnumeratedType()) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        status = 0 ;
    }
    // VIPER #2960: Check for chandle data type - only chandle type can be assigned to another chandle type:
    if (other->IsChandleType()) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    // Check for event type : an event variable can be assigned to another event variable or null
    if (other->IsEventType() && environment != VeriTreeNode::PROP_SEQ_INIT_VAL) {
        if (from) from->Error("cannot mix event and non-event types in this operation") ;
        status = 0 ;
    }
    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        status = 0 ;
    }
    // Prepare a flag to determine which type is target type and which one is value type.
    // This can be used to issue error more exactly. We can get this information from environment
    unsigned is_this_target = ((environment == VeriTreeNode::WRITE) || (environment == VeriTreeNode::INST_WRITE)) ? 1 : 0 ; // Flag to indicate if 'this' is target or not
    // One side is string type and other type is not string literal/string type
    if (other->IsString()) {
        if (is_this_target) { // target struct/union, value string
            // VIPER #7331 : Produce different message for case item condition comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a string", (_is_packed) ? "a packed" : "an unpacked") ;
            } else {
                if (from) from->Error("cannot assign a string to %s type", (_is_packed) ? "a packed" : "an unpacked") ;
            }
        } else { // target string, value unpacked
            // VIPER #7331 : Produce different message for case item condition comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", (_is_packed) ? "a packed" : "an unpacked", "a string") ;
            } else {
                if (from) from->Error("assigning a %s type to a string requires a cast", (_is_packed) ? "packed" : "unpacked") ;
            }
        }
        return 0 ;
    }
    // VIPER #2046 :A packed type cannot be assigned to unpacked type
    if (_is_packed && (other->IsUnpacked() && !other->IsBuiltInType())) {
        // VIPER #7331 : Produce different message for case item condition comparison
        if (environment == VeriTreeNode::CASE_CONDITION) {
            if (from) from->Error("illegal case item comparison of %s type with %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        } else {
            if (from) from->Error("cannot assign %s type to %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        }
        status = 0 ;
    }
    if (!_is_packed && (other->IsPacked() || other->IsRealType()) && !other->IsStringLiteral()) {
        // VIPER #7331 : Produce different message for case item condition comparison
        if (environment == VeriTreeNode::CASE_CONDITION) {
            if (from) from->Error("illegal case item comparison of %s type with %s type", (is_this_target) ? "a packed": "an unpacked", (is_this_target) ? "an unpacked": "a packed") ;
        } else {
            if (from) from->Error("cannot assign %s type to %s type", (is_this_target) ? "a packed": "an unpacked", (is_this_target) ? "an unpacked": "a packed") ;
        }
        status = 0 ;
    }
    if (!_is_packed && other->IsUnpacked() && !IsEquivalent(other, 0, 0, 0)) {
        // One side is unpacked structure, other type is not
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    return status ;
}
unsigned VeriEnumType::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other) return 1 ; // Cannot do anything without value type
    // Do not check anything for type parameter type/unresolved name type
    if (IsTypeParameterType() || IsUnresolvedNameType() || other->IsTypeParameterType() || other->IsUnresolvedNameType()) return 1 ;
    // If one side is property/sequence, do not check type
    if (other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped() || other->IsInterconnectType()) return 1 ;

    unsigned status = 1 ;
    // VIPER #2249 : Type checking for enumerated types
    // This checking should not issue error if
    // 1. Enumerated type is in value, other type is target
    // 2. Initial value of parameter of enumerated type is not enum variable/enum literal.
    // 3. Initial value of enum literal is not enum variable/enum literal.
    // 4. Return type of function is enum and return value is not enum variable/enum literal.
    //if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE) && (environment != VeriTreeNode::PARAM_INIT) && (environment != VeriTreeNode::RETURN_EXPR)
    // VIPER #8293 : Issue error if initial value of parameter of enumerated type is not enum variable/enum literal
    // and return type of function is enum and return value is not enum variable/enum literal
    if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE)
       && (other->IsEnumeratedType() && !IsEquivalent(other, 0, 0, 0))) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        status = 0 ;
    }
    // VIPER #2960: Check for chandle data type - only chandle type can be assigned to another chandle type:
    if (other->IsChandleType()) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    // Check for event type : an event variable can be assigned to another event variable or null
    if (other->IsEventType()) {
        if (from) from->Error("cannot mix event and non-event types in this operation") ;
        status = 0 ;
    }
    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        status = 0 ;
    }
    // Prepare a flag to determine which type is target type and which one is value type.
    // This can be used to issue error more exactly. We can get this information from environment
    unsigned is_this_target = ((environment == VeriTreeNode::WRITE)|| (environment == VeriTreeNode::INST_WRITE)) ? 1 : 0 ; // Flag to indicate if 'this' is target or not
    // One side is string type and other type is not string literal/string type
    if (other->IsString()) {
        if (is_this_target) { // target enum, value string
            // VIPER #7331 : Produce different message for case item condition comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a string", "a packed") ;
            } else {
                if (from) from->Error("cannot assign a string to %s type", "a packed") ;
            }
        } else { // target string, value enum
            // VIPER #7331 : Produce different message for case item condition comparison
            if (environment == VeriTreeNode::CASE_CONDITION) {
                if (from) from->Error("illegal case item comparison of %s type with %s type", "a packed", "a string") ;
            } else {
                if (from) from->Error("assigning a %s type to a string requires a cast", "packed") ;
            }
        }
        return 0 ;
    }
    // VIPER #2046 :A packed type cannot be assigned to unpacked type
    if (other->IsUnpacked()) {
        // VIPER #7331 : Produce different message for case item condition comparison
        if (environment == VeriTreeNode::CASE_CONDITION) {
            if (from) from->Error("illegal case item comparison of %s type with %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        } else {
            if (from) from->Error("cannot assign %s type to %s type", (is_this_target) ? "an unpacked": "a packed", (is_this_target) ? "a packed": "an unpacked") ;
        }
        status = 0 ;
    }
    return status ;
}
unsigned VeriNamedType::CheckAgainst(VeriTypeInfo *other, VeriTreeNode *from, VeriTreeNode::veri_type_env environment) const
{
    if (environment == VeriTreeNode::CASTING) return 1 ; // Do not type check for casting
    if (!other) return 1 ; // Cannot do anything without value type
    // Do not check anything for type parameter type/unresolved name type
    if (IsUnresolvedNameType() || (other->IsTypeParameterType() || other->IsUnresolvedNameType())) return 1 ;
    // If one side is property/sequence, do not check type
    if (other->IsPropertyType() || other->IsSequenceType() || other->IsUntyped() || other->IsInterconnectType()) return 1 ;

    unsigned status = 1 ;
    // VIPER #2249 : Type checking for enumerated types
    // This checking should not issue error if
    // 1. Enumerated type is in value, other type is target
    // 2. Initial value of parameter of enumerated type is not enum variable/enum literal.
    // 3. Initial value of enum literal is not enum variable/enum literal.
    // 4. Return type of function is enum and return value is not enum variable/enum literal.
    //if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE) && (environment != VeriTreeNode::PARAM_INIT) && (environment != VeriTreeNode::RETURN_EXPR)
    // VIPER #8293 : Issue error if initial value of parameter of enumerated type is not enum variable/enum literal
    // and return type of function is enum and return value is not enum variable/enum literal
    if ((environment != VeriTreeNode::WRITE) && (environment != VeriTreeNode::INST_WRITE)
       && other->IsEnumeratedType()) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        status = 0 ;
    }
    // VIPER #2960: Check for chandle data type - only chandle type can be assigned to another chandle type:
    if (other->IsChandleType()) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    // Check for event type : an event variable can be assigned to another event variable or null
    if (other->IsEventType()) {
        if (from) from->Error("cannot mix event and non-event types in this operation") ;
        status = 0 ;
    }
    // Check for associative array: cannot mix associative and non-associative arrays
    if (other->IsAssociativeArrayType()) {
        if (from) from->Error("cannot mix assoc and non-assoc arrays") ;
        status = 0 ;
    }
    unsigned is_this_target = ((environment == VeriTreeNode::WRITE) || (environment == VeriTreeNode::INST_WRITE)) ? 1 : 0 ; // Flag to indicate if 'this' is target or not
    // One side is string type and other type is not string literal/string type
    if (other->IsString()) {
        if (is_this_target) { // target enum, value string
            if (from) from->Error("cannot assign a string to %s type", "an unpacked") ;
        } else { // target string, value enum
            if (from) from->Error("assigning a %s type to a string requires a cast", "unpacked") ;
        }
        return 0 ;
    }
    unsigned equivalent = IsEquivalent(other, 0, 0, 0) ;
    if (status && ((!other->IsNamedType() && !equivalent) || !equivalent)) {
        if (from) from->Error("incompatible complex type assignment") ;
        status = 0 ;
    }
    if (status && equivalent && IsClassType()) { // Both are equivalent class type
        // Check base class object not be assigned to derived class object
        if ((is_this_target && IsDerivedFrom(other)) || // This is target and is derived from value
            (!is_this_target && other->IsDerivedFrom(this))) { // Other is target and derived class
            if (from) from->Error("incompatible complex type assignment") ;
            status = 0 ;
        }
    }
    return status ;
}
unsigned VeriBuiltInType::IsUnpacked() const
{
    // real/shortreal data types are unpacked
    switch (_type) {
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_EVENT :
    case VERI_SHORTREAL :
    case VERI_CHANDLE :
    // VIPER #3113 : LRM is not clear about whether the string data type is packed
    // or unpacked. Since, strings are dynamic it is more like unpacked than packed.
    // Also other standard simulators treat strings as unpacked. So we do the same:
    case VERI_STRINGTYPE :
        return 1 ; // Unpacked
    default :
        return 0 ;
    }
}
unsigned VeriNamedType::IsUnpacked() const
{
    if (!IsUnresolvedNameType()) return 1 ; // Always unpacked for resolved name type. Can only be class/interface/modport
    return 0 ; // FIX ME : packed for unresolved name
}
unsigned VeriBuiltInType::IsPacked() const
{
    if (IsUnpacked()) return 0 ;
    return 1 ;
}
unsigned VeriNamedType::IsPacked() const
{
    return 0 ;
}
/******************************************************************************
*              Routine to check if type is for type parameter                 *
******************************************************************************/
unsigned VeriBuiltInType::IsTypeParameterType() const
{
    // While parsing _data_type of type parameter is 'type', we cannot check
    // anything with it as it can be overwritten. Elabortaion will set correct
    // type to its _data_type field and then this routine will return 0 and we
    // can type check with this type:
    return (_type == VERI_TYPE) ;
}
unsigned VeriArrayType::IsTypeParameterType() const
{
    return (_element_type) ? _element_type->IsTypeParameterType() : 0 ;
}
unsigned VeriUnpackedArray::IsTypeParameterType() const
{
    return (_element_type) ? _element_type->IsTypeParameterType() : 0 ;
}
/******************************************************************************
*              Routine to check if unresolved named type                      *
******************************************************************************/
unsigned VeriArrayType::IsUnresolvedNameType() const
{
    // VIPER #5271 : Treat it unresolved when no element type is present
    return (_element_type) ? _element_type->IsUnresolvedNameType(): 1 ;
}
unsigned VeriUnpackedArray::IsUnresolvedNameType() const
{
    // VIPER #5271 : Treat it unresolved when no element type is present
    return (_element_type) ? _element_type->IsUnresolvedNameType(): 1 ;
}
unsigned VeriNamedType::IsUnresolvedNameType() const
{
    if (!_id) return 1 ; // No resolved identifier, unresolved

    // Type identifier having no data type and data type as module item back pointer is forward
    // declared type. Treat as unresolved
    VeriModuleItem *tree = _id->GetModuleItem() ;
    if (_id->IsType() && !_id->GetDataType() && (!tree || tree->IsDataDecl())) return 1 ;

    // If parameter value assignment list and name is scope name then named type is unresolved name type
    if (_name && _name->GetParamValues() && _name->IsScopeName())  return 1 ;

    // VIPER #4084 : If this is class type, check whether it is unelaborated class
    // having type parameter as base class in any level of its hierarchy. If so assume
    //  it is unresolved, as it is not known what is the base class
    if (_id->IsClass()) {
        // Get the class declaration :
        VeriIdDef *class_id = _id ;
        while (class_id && class_id->IsClass()) {
            VeriModuleItem *class_decl = class_id->GetModuleItem() ;
            if (class_decl && !class_decl->IsClass()) {
                return 1 ; // VIPER #5992: module item is not class, may be forward typedef
            } else if (class_decl && !class_decl->IsClassInstantiated()) {
#ifdef VERILOG_DO_NOT_TOUCH_PARSE_TREE
                // VIPER #7508 :If 'VERILOG_DO_NOT_TOUCH_PARSE_TREE' compile flag is on, we
                // will copy even the classes without base and parameters. So
                // do not check type for unelaborated classes.
                if (VeriNode::IsStaticElab()) return 1 ; // not yet elaborated, so unresolved
#endif //VERILOG_DO_NOT_TOUCH_PARSE_TREE
                // This class is not yet elaborated :
                // Check whether its base class contains type parameter :
                VeriName *base = class_decl->GetBaseClassName() ;
                VeriIdDef *base_id = (base) ? base->GetId() : 0 ;
                // If base class contains type parameter, assume it is unresolved
                if (base_id && base_id->IsType() && base_id->IsParam()) return 1 ;
                // VIPER #5992 : if base id is not yet resolved, it is unresolved name type
                if (base_id && !base_id->IsClass()) return 1 ;

                // VIPER #6553 : If this class contains parameters, it is unresolved name type
                // as parameter overwrite can change its type
                if (class_decl->GetParameters()) return 1 ;
                class_id = base_id ;
            } else {
                break ;
            }
        }
    }
    return 0 ; // resolved
}
/*****************************************************************************
*             Routine to check if property type
******************************************************************************/
unsigned VeriBuiltInType::IsPropertyType() const
{
    unsigned ret_type = 0 ;
    ret_type = (_type == VERI_PROPERTY) ;
    return ret_type ;
}
unsigned VeriArrayType::IsPropertyType() const
{
    return (_element_type) ? _element_type->IsPropertyType() : 0 ;
}
unsigned VeriUnpackedArray::IsPropertyType() const
{
    return (_element_type) ? _element_type->IsPropertyType() : 0 ;
}
/*****************************************************************************
*             Routine to check if sequence type
******************************************************************************/
unsigned VeriBuiltInType::IsSequenceType() const
{
    unsigned ret_type = 0 ;
    ret_type = (_type == VERI_SEQUENCE) ;
    return ret_type ;
}
unsigned VeriArrayType::IsSequenceType() const
{
    return (_element_type) ? _element_type->IsSequenceType() : 0 ;
}
unsigned VeriUnpackedArray::IsSequenceType() const
{
    return (_element_type) ? _element_type->IsSequenceType() : 0 ;
}
/*****************************************************************************
*             Routine to check if untyped type
******************************************************************************/
unsigned VeriBuiltInType::IsUntyped() const
{
    unsigned ret_type = 0 ;
    ret_type = (_type == VERI_UNTYPED) ;
    return ret_type ;
}
unsigned VeriArrayType::IsUntyped() const
{
    return (_element_type) ? _element_type->IsUntyped() : 0 ;
}
unsigned VeriUnpackedArray::IsUntyped() const
{
    return (_element_type) ? _element_type->IsUntyped() : 0 ;
}
/*****************************************************************************
*             Routine to check if interconnect type
******************************************************************************/
unsigned VeriBuiltInType::IsInterconnectType() const
{
    unsigned ret_type = 0 ;
    ret_type = (_type == VERI_INTERCONNECT) ;
    return ret_type ;
}
unsigned VeriArrayType::IsInterconnectType() const
{
    return (_element_type) ? _element_type->IsInterconnectType() : 0 ;
}
unsigned VeriUnpackedArray::IsInterconnectType() const
{
    return (_element_type) ? _element_type->IsInterconnectType() : 0 ;
}
/******************************************************************************
*              Routine to increase packed dimension count                     *
******************************************************************************/
// Packed dimension can be increased by multiple level declaration of a type.
// However packed dimension can be added only to packed type. Packed dimension
// on unpacked type is error.
VeriTypeInfo *VeriBuiltInType::IncreasePackedDimBy(unsigned size, const VeriTreeNode *from)
{
    if (!size) return this ; // no dimension to increase, return itself

    // Base type is built-in type. Packed dimensions are not allowed on non-integer
    // type/integer-atom type
    switch (_type) {
    // non-integer type
    case VERI_TIME :
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_SHORTREAL :
    // integer_atom_type (no dimensions allowed) :
    case VERI_INTEGER :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_BYTE :
    case VERI_CHAR :
        // VIPER #5248 (test11): Allow packed dimension of packed type for type parameters under compatibility mode:
        // This routine is only called for creating type of parameters. Direct packed data type with packed dimensions are checked elsewhere.
        if (from && !VeriNode::InRelaxedCheckingMode()) from->Error("cannot have packed dimensions of type %s", VeriNode::PrintToken(_type)) ; // VIPER #3202 : produce more appropriate message
        break ;
    default :
        break ;
    }
    return new VeriArrayType(this, size) ;
}
VeriTypeInfo * VeriArrayType::IncreasePackedDimBy(unsigned size, const VeriTreeNode * /*from*/)
{
    _packed_dims = _packed_dims + size ;
    return this ;
}
VeriTypeInfo * VeriUnpackedArray::IncreasePackedDimBy(unsigned size, const VeriTreeNode *from)
{
    if (!size) return this ; // no need to increase packed count
    // Packed dimension not allowed on unpacked type
    if (from) from->Error("cannot have packed dimensions of unpacked type %s", "") ;
    delete this ;
    return 0 ;
}
VeriTypeInfo * VeriStructUnionType::IncreasePackedDimBy(unsigned size, const VeriTreeNode *from)
{
    if (!size) return this ; // no need to increase packed count
    // Packed dimension not allowed on unpacked type
    if (!_is_packed) {
        if (from) from->Error("cannot have packed dimensions of unpacked type %s", "") ;
        delete this ;
        return 0 ;
    }
    return new VeriArrayType(this, size) ;
}
VeriTypeInfo * VeriEnumType::IncreasePackedDimBy(unsigned size, const VeriTreeNode *from)
{
    if (!size) return this ; // no need to increase packed count

    // Packed dimension can be increase if base type of enum is not integer atom type
    unsigned base_size = (_base_type) ? _base_type->BaseTypeSize() : 0 ;
    if (base_size > 1) {
        if (from) from->Error("cannot have packed dimensions of type %s", "enum here") ;
    }
    return new VeriArrayType(this, size) ;
}
VeriTypeInfo * VeriNamedType::IncreasePackedDimBy(unsigned size, const VeriTreeNode *from)
{
    if (!size) return this ; // no need to increase packed count
    if (IsUnresolvedNameType()) {
        delete this ; // unresolved type, cannot increase dimension
        return 0 ;
    }
    // Packed dimension not allowed on unpacked type
    if (from) from->Error("cannot have packed dimensions of unpacked type %s", _name ? _name->GetName() : (_id ? _id->GetName() : "")) ;
    delete this ;
    return 0 ;
}

/******************************************************************************
*          Bit size of base type (only for built-in types)                    *
*******************************************************************************/
unsigned VeriBuiltInType::BaseTypeSize() const
{
    // VIPER #6640: Call the merged routine to get the size of the built-in type:
    return (_type) ? VeriNode::GetSizeOfBuiltInType(_type) : 1 ;
}
unsigned VeriArrayType::BaseTypeSize() const
{
    if (!_element_type) return 1 ; // Implicit single bit
    return _element_type->BaseTypeSize() ;
}
unsigned VeriEnumType::BaseTypeSize() const
{
    // If a subtype is given, we should follow that.
    if (_base_type) return _base_type->BaseTypeSize() ;
    return 32 ; // Size of enum is default size integer : 32 bits.
}
unsigned VeriUnpackedArray::BaseTypeSize() const
{
    return (_element_type) ? _element_type->BaseTypeSize() : 1 ;
}
/******************************************************************************
*                          Unpacked dimension                                 *
******************************************************************************/
unsigned VeriFixedSizeArray::UnpackedDimension()  const
{
    unsigned dim_count = _unpacked_dims ; // dimension of this array
    // Consider unpacked dimension of element
    return (_element_type) ? (dim_count + _element_type->UnpackedDimension()) : dim_count ;
}
unsigned VeriUnpackedArray::UnpackedDimension()  const
{
    unsigned dim_count = 1 ; // Implicit dimension of queue/dynamic array/associative array
    // Consider unpacked dimension of element
    return (_element_type) ? (dim_count + _element_type->UnpackedDimension()) : dim_count ;
}
unsigned VeriNamedType::UnpackedDimension()  const
{
    if (_id && _id->IsInst()) return _id->UnPackedDimension() ;
    return  0 ;
}
// VIPER #3854 : Check validity of index expression :
void VeriTypeInfo::CheckIndex(VeriExpression *index) const
{
    if (!index) return ;
    if (index->IsRange()) {
        CheckIndex(index->GetLeft()) ;
        CheckIndex(index->GetRight()) ;
    } else {
        VeriTypeInfo *idx_type = index->CheckType(0, VeriTreeNode::READ) ; // index type
        if (!idx_type || idx_type->IsTypeParameterType() || idx_type->IsUnresolvedNameType()) {
            delete idx_type ;
            return ;
        }

        if (idx_type->IsRealType()) {
            index->Error("illegal context for real expression") ;
        } else if (!idx_type->IsIntegralType()) {
            index->Error("illegal index expression") ;
        }
        delete idx_type ;
    }
}
/******************************************************************************
*    Check for dual Type. Corresponding function for TypeMatchDual            *
*******************************************************************************/
unsigned VeriTypeInfo::TypeMatchDual(const VhdlIdDef *vhdl_type) const
{
    // At present this is used only to check type matching among ports
    // for vhdl design entity in a Verilog module.
    // Vhdl string, real and integer in port is not matched with verilog port Viper #4216
    // Viper 5404. Get the typeinfo instead of datatype. Viper 5404.
    // For literals the datatype do not exist
    if (vhdl_type) {
        if (vhdl_type->IsStringType()) {
            if (!IsString() && !IsStringLiteral()) {
                return 0 ;
            }
        }
        if (IsString() || IsStringLiteral()) {
            if (!vhdl_type->IsStringType()) {
                return 0 ;
            }
        }

        if (vhdl_type->IsRealType() && !IsRealType()) return 0 ;
        if (IsRealType() && !vhdl_type->IsRealType()) return 0 ;

        if (vhdl_type->IsIntegerType() || vhdl_type->IsNaturalType() || vhdl_type->IsPositiveType()) { // Viper 4560
            // Viper 5443
            if (!IsIntegerType() && !IsNetType() && !IsRegType()) { //Viper 6649
                return 0 ;
            }
        }
    }
    return 1 ;
}

/******************************************************************************
*    Reduce packed/unpacked dimension: may convert to new type                *
*******************************************************************************/
VeriTypeInfo *VeriBuiltInType::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return 0 ;

    unsigned size = idxs->Size() ;
    VeriExpression *idx = (size) ? (VeriExpression*)idxs->RemoveLast() : 0 ;
    // Dimension can be reduced if built-in type is interger vector type
    // But we can reduce dimension only by 1. If request is to reduce dimension
    // greater than 1, it is illegal select
    if (size > 1) {
        if (from) from->Warning("too many indices into %s", from->GetName() ? from->GetName() : "") ;
        delete this ; // Delete itself
        return 0 ;
    } else if (!size) {
        VERIFIC_ASSERT(0) ;
    }
    unsigned is_indexing = 0 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_indexing = 1 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_indexing = 1 ;
    }
    CheckIndex(idx) ;
    VeriTypeInfo *ret_type = 0 ;
    switch (_type) {
    case VERI_BYTE :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_CHAR :
        // Select on 2 value vector type, return type is bit
        if (is_indexing) ret_type = new VeriBuiltInType(VERI_BIT) ; break ;
    case VERI_INTEGER : // Select on 4 value vector type, return type is reg
    case VERI_GENVAR : // Select on genvar
    case VERI_TIME : // Select on time
        if (is_indexing) ret_type = new VeriBuiltInType(VERI_REG) ; break ;
    case VERI_STRINGTYPE : // Select on string type variable, return byte
        if (is_indexing) ret_type = new VeriBuiltInType(VERI_BYTE) ;
        break ;
    default :
        // Dimension reduction i.e. indexing not allowed
        if (from) from->Warning("cannot index into non-array %s", from->GetName() ? from->GetName() : "") ;
        break ;
    }
    // For indexing new types are created, so for indexing delete itself
    if (is_indexing) delete this ; // Delete itself
    return (is_indexing) ? ret_type: this ;
}
VeriTypeInfo *VeriArrayType::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ; // No dimension needs to reduce

    // This type contains packed dimension and element type. So following situation
    // may arise here
    // 1. dim_count < _packed_dims when _packed_dims > 0
    // 2. dim_count = _packed_dims when _packed_dims > 0
    // 3. dim_count > _packed_dims  when _packed_dims > 0
    // 4. _packed_dims = 0
    unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->GetLast() : 0 ;
    unsigned is_range = 1 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_range = 0 ;
    }
    if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        return this ;
    }
    VeriTypeInfo *type = 0 ;
    unsigned i ;
    unsigned to_remove = 0 ;
    FOREACH_ARRAY_ITEM_BACK(idxs, i, idx) {
        is_range = 1 ;
        if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
            if (idx->RangeWidth() == 1) is_range = 0 ;
        } else {
            is_range = 0 ;
        }
        if (is_range) {
            CheckIndex(idx) ;
            break ; // It is last index and range, do not decrease dimension
        }
        if (_packed_dims) { // packed dimension exists
            CheckIndex(idx) ;
            // Decrease dimension by one
            to_remove++ ;
            _packed_dims-- ;
        } else { // No packed dimension remains
            // Dimension should be reduced from element
            idxs->Remove(idxs->Size() - to_remove, to_remove) ;
            type = (_element_type) ? _element_type->ReduceDimBy(idxs, from): 0 ;
            _element_type = 0 ;
            delete this ;
            return type ;
        }
    }
    if (_packed_dims) { // All dimensions are not selected
        type = this ;
    } else { // All dimensions are selected
        // Return type will be element type of this unpacked type
        type = _element_type ;
        _element_type = 0 ; // Clear element type used in return type
        delete this ;
    }
    return type ;
}
VeriTypeInfo *VeriFixedSizeArray::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ; // No dimension needs to reduce

    // This type contains unpacked dimension, packed dimension and element type.
    // So following situation may arise here.
    // 1. _unpacked_dims > dim_count when _unpacked_dims > 0
    // 2. _unpacked_dims < dim_count when _unpacked_dims > 0
    // 3. _unpacked_dims == dim_count
    // 4. _unpacked_dims = 0
    unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->GetLast() : 0 ;
    unsigned is_range = 1 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_range = 0 ;
    }
    if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        return this ;
    }
    VeriTypeInfo *type = 0 ;
    unsigned i ;
    unsigned to_remove = 0 ;
    FOREACH_ARRAY_ITEM_BACK(idxs, i, idx) {
        is_range = 1 ;
        if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
            //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
            // Viper 5033 and Viper 6603: This change was not done initially for 5033
            // and hence we got 6603.
        } else {
            is_range = 0 ;
        }
        if (is_range) {
            if (_unpacked_dims) CheckIndex(idx) ;
            break ; // It is last index and range, do not decrease dimension
        }
        if (_unpacked_dims) { // Unpacked dimension exists
            CheckIndex(idx) ;
            // Decrease dimension by one
            to_remove++ ;
            _unpacked_dims-- ;
        } else { // No unpacked dimension remains
            // Dimension should be reduced from element
            idxs->Remove(idxs->Size() - to_remove, to_remove) ;
            type = (_element_type) ? _element_type->ReduceDimBy(idxs, from): 0 ;
            _element_type = 0 ;
            delete this ;
            return type ;
        }
    }
    if (_unpacked_dims) { // All dimensions are not selected
        type = this ;
    } else { // All dimensions are selected
        // Return type will be element type of this unpacked type
        type = _element_type ;
        _element_type = 0 ; // Clear element type used in return type
        delete this ;
        if (is_range) { // Part select is on type
            // Check the validity
            idxs->Remove(idxs->Size() - to_remove, to_remove) ;
            type = (type) ? type->ReduceDimBy(idxs, from) : type ;
        }
    }
    return type ;
}
VeriTypeInfo *VeriAssociativeArray::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ; // No dimension needs to reduce

    unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->RemoveLast() : 0 ;
    unsigned is_range = 1 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_range = 0 ;
    }
    if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        if (idx) idx->Error("illegal slicing of associative array") ;
        return this ;
    }
    // if last index is range, original dimension to be reduced is one less than dim_count
    dim_count = (is_range) ? (dim_count - 1) : dim_count ;

    // Check index expression against 'index_type'
    VeriTypeInfo *idx_type = (idx) ? idx->CheckType(0, VeriTreeNode::READ) : 0 ; // Get type of index
    CheckIndexAgainst(idx_type, idx) ;
    delete idx_type ;
    // Unpacked dimension for this type is 1.
    VeriTypeInfo *type = 0 ;
    if (dim_count == 1) {
        // Return type will be packed type of this unpacked type
        type = _element_type ;
    } else {
        // Dimension to be reduced is greater than unpacked dimension of this type.
        // So dimension is to be reduced from packed type
        type = (_element_type) ? _element_type->ReduceDimBy(idxs, from): 0 ;
    }
    _element_type = 0 ; // Clear element type used in return type
    delete this ;
    return type ;
}
VeriTypeInfo *VeriDynamicArray::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ; // No dimension needs to reduce

    unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->RemoveLast() : 0 ;
    unsigned is_range = 1 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_range = 0 ;
    }
    if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        return this ;
    }
    // if last index is range, original dimension to be reduced is one less than dim_count
    dim_count = (is_range) ? (dim_count - 1) : dim_count ;
    // Unpacked dimension for this type is 1.
    VeriTypeInfo *type = 0 ;
    if (dim_count == 1) {
        // Return type will be packed type of this unpacked type
        type = _element_type ;
    } else {
        // Dimension to be reduced is greater than unpacked dimension of this type.
        // So dimension is to be reduced from packed type
        type = (_element_type) ? _element_type->ReduceDimBy(idxs, from): 0 ;
    }
    _element_type = 0 ; // Clear element type used in return type
    delete this ;
    return type ;
}
VeriTypeInfo *VeriQueue::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ; // No dimension needs to reduce

    unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->RemoveLast() : 0 ;
    unsigned is_range = 1 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        // VIPER #4764 : Q[n:n] = {Q[n]} i.e. queue with one element. So do not ignore
        // range when both bounds are same.
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_range = 0 ;
    }
    if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        return this ;
    }
    // if last index is range, original dimension to be reduced is one less than dim_count
    dim_count = (is_range) ? (dim_count - 1) : dim_count ;
    // Unpacked dimension for this type is 1.
    VeriTypeInfo *type = 0 ;
    if (dim_count == 1) {
        // Return type will be packed type of this unpacked type
        type = _element_type ;
    } else {
        // Dimension to be reduced is greater than unpacked dimension of this type.
        // So dimension is to be reduced from packed type
        type = (_element_type) ? _element_type->ReduceDimBy(idxs, from): 0 ;
    }
    _element_type = 0 ; // Clear element type used in return type
    delete this ;
    return type ;
}
VeriTypeInfo *VeriStructUnionType::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ;

    unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->RemoveLast() : 0 ;
    unsigned is_range = 1 ;
    if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    } else {
        is_range = 0 ;
    }
    if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        // But slicing on unpacked structure/union is not alowed
        if (!_is_packed) {
            if (from) from->Error("illegal select on unpacked structure/union type variable %s", from->GetName() ? from->GetName() : 0) ;
            delete this ;
            return 0 ;
        }
        return this ;
    }
    // if last index is range, original dimension to be reduced is one less than dim_count
    dim_count = (is_range) ? (dim_count - 1) : dim_count ;
    // Selection cannot be performed on unpacked structure/union. That is illegal
    // For select on packed structure/union, if dimension to be reduced is greater
    //  than 1, it is an error
    if (!_is_packed || (dim_count > 1)) {
        if (from) from->Error("illegal select on unpacked structure/union type variable %s", from->GetName() ? from->GetName() : 0) ;
        delete this ;
        return 0 ;
    }
    // It is treated as array of bits. Return type will be reg
    VeriTypeInfo *type = new VeriBuiltInType(VERI_REG) ;
    delete this ;
    return type ;
}
VeriTypeInfo *VeriEnumType::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ;

    // LRM 1800 Sec 4.10.4 says 'An enum variable or identifier used as part of an
    // expression is automatically cast to base type of enum declaration.
    // VIPER #5454 : Do not use enum type when slicing is performed on enum variable:
    //unsigned dim_count = idxs->Size() ; // Dimension to be selected

    // Check whether last index is range or index
    //VeriExpression *idx = (dim_count) ? (VeriExpression*)idxs->GetLast() : 0 ;
    //unsigned is_range = 1 ;
    //if (idx && idx->IsRange() && idx->GetLeft() && idx->GetRight()) {
        //if (idx->RangeWidth() == 1) is_range = 0 ; //Commented out for VIPER 5033
        // Viper 5033: Modelsim also treats p[0:0] as array types rather than an element type
    //} else {
        //is_range = 0 ;
    //}
    //if (is_range && (dim_count == 1)) {
        // One index expression remains and that is range. So it is slicing on this
        // type and as slicing does not change dimension count return this
        //return this ;
    //}
    // Dimension to be reduced from the base type of enum
    VeriTypeInfo *copy_base_type = (_base_type) ? _base_type->Copy() : 0 ;
    VeriTypeInfo *type = (copy_base_type) ? copy_base_type->ReduceDimBy(idxs, from): 0 ;
    delete this ;
    // FIXME base type should always return created type
    return type ;
}
VeriTypeInfo *VeriNamedType::ReduceDimBy(Array *idxs, VeriName *from)
{
    if (!idxs) return this ;
    if (IsUnresolvedNameType()) { // Unresolved type
        // Dimension cannot be reduced
        delete this ;
        return 0 ; // Return 0
    }
    // Selection cannot be performed
    if (from) {
            from->Error("illegal reference to %s", (from->GetName()) ? from->GetName() : (_name ? _name->GetName(): "")) ;
    }
    delete this ;
    return 0 ;
}
// Check the validity of index used to select element of associative array :
void VeriAssociativeArray::CheckIndexAgainst(VeriTypeInfo *idx_type, VeriTreeNode *idx)
{
    if (!idx_type || (idx_type->IsTypeParameterType() || idx_type->IsUnresolvedNameType())) return ;
    if (!_index_type) { // wild-card index
        // Index should be integral type
        // Allow string literal and string type
        if (!idx_type->IsIntegralType() && !idx_type->IsString() && !idx_type->IsStringLiteral()) {
            if (idx) idx->Error("illegal index for associative array") ;
        }
    } else if (_index_type->IsString()) { // string index
        // Index can be sring or string literal
        if (!idx_type->IsString() && !idx_type->IsStringLiteral()) {
            if (idx) idx->Error("illegal index for associative array") ;
        }
    } else if (_index_type->IsClassType()) { // class index
        // Index can be same or derived class type
        if (!idx_type->IsEquivalent(_index_type, 0, 0, 0)) {
            if (idx) idx->Error("illegal index for associative array") ;
        }
    } else if (_index_type->IsIntegralType()) { // integer/packed array/packed structure
        // Index can be any integral type
        if (!idx_type->IsIntegralType()) {
            if (idx) idx->Error("illegal index for associative array") ;
        }
    } else if (_index_type->IsUnpacked() && _index_type->IsBuiltInType()) {
        if (idx_type->IsUnpackedArrayType() || (idx_type->IsStructure() && idx_type->IsUnpacked())) {
            if (idx) idx->Error("illegal index for associative array") ;
        }
    } else if (_index_type->IsUnpacked()) {
        // Index should be unpacked
        if (!idx_type->IsUnpacked()) {
            if (idx) idx->Error("illegal index for associative array") ;
        }
    } else if (!_index_type->IsTypeParameterType() && !_index_type->IsUnresolvedNameType()) { // Invalid index
        if (idx) idx->Error("illegal index for associative array") ;
    }
}
/******************************************************************************
*                                  Type                                       *
*******************************************************************************/
// Dynamic types :
// String, class, dynamic array, queue, associative arrays are of dynamic types
unsigned VeriBuiltInType::IsDynamicType() const      { return  (_type == VERI_STRINGTYPE) ; }
unsigned VeriAssociativeArray::IsDynamicType() const { return 1 ; }
unsigned VeriDynamicArray::IsDynamicType() const     { return 1 ; }
unsigned VeriQueue::IsDynamicType() const            { return 1 ; }
unsigned VeriNamedType::IsDynamicType() const        { return IsClassType() ; }

// String type :
unsigned VeriBuiltInType::IsString() const { return (_type == VERI_STRINGTYPE) ; }

// Reg type :
unsigned VeriBuiltInType::IsRegType() const { return (_type == VERI_REG) ; }
// Net type :
// VIPER #8325: Check nettype for typeinfo.
unsigned VeriTypeInfo::IsNetType() const
{
    unsigned nettype = _nettype ? _nettype : Type() ;
    switch (nettype) {
    case VERI_WIRE :
    case VERI_TRI  :
    case VERI_TRI1 :
    case VERI_SUPPLY0 :
    case VERI_WAND :
    case VERI_TRIAND :
    case VERI_TRIOR :
    case VERI_TRI0 :
    case VERI_SUPPLY1 :
    case VERI_WOR :
    case VERI_TRIREG :
    case VERI_UWIRE:
    case VERI_INTERCONNECT:
        return 1 ;
    default : break ;
    }
    return 0 ; // Not net
}
unsigned VeriNamedType::IsNetType() const
{
    return 0 ;
}

// Real type :
// Real/shortreal/realtime
unsigned VeriBuiltInType::IsRealType() const
{
    switch (_type) {
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_SHORTREAL :
        return 1 ;
    default :
        break ;
    }
    return 0 ; // Not real
}
unsigned VeriBuiltInType::IsIntegerType() const
{
    // Viper 5443
    switch (_type) {
    case VERI_INTEGER :
    case VERI_INT :
        return 1 ;
    default :
        break ;
    }
    return 0 ; // Not integer
}
// Event type :
unsigned VeriBuiltInType::IsEventType() const
{
    switch (_type) {
    case VERI_EVENT : return 1 ;
    default : break ;
    }
    return 0 ; // Not event
}
// Chandle type :
unsigned VeriBuiltInType::IsChandleType() const
{
    if (_type == VERI_CHANDLE) return 1 ;
    return 0 ;
}

// Class type :
unsigned VeriNamedType::IsClassType() const
{
    if (_id && _id->IsClass()) return 1 ; // Class type
    return 0 ;
}

// Covergroup type :
unsigned VeriNamedType::IsCovergroupType() const
{
    if (_id && _id->IsCovergroup()) return 1 ; // covergroup type
    return 0 ;
}
// Integral type : integer data type, packed array, packed structure, packed union, enum or time
unsigned VeriBuiltInType::IsIntegralType() const
{
    if (!_type) return 1 ; // No _type for 'output [1:0] x'.
    switch (_type) {
    // Integer atom types :
    case VERI_BYTE     :
    case VERI_CHAR     :
    case VERI_SHORTINT :
    case VERI_INT      :
    case VERI_LONGINT  :
    case VERI_INTEGER  :
    case VERI_TIME     :
    case VERI_GENVAR   :
    case VERI_UNSIGNED : // used in casting as type
    case VERI_SIGNED   : // used in casting as type

    // Integer vector type
    case VERI_BIT      :
    case VERI_LOGIC    :
    case VERI_REG      :
    case VERI_WIRE     :
    case VERI_TRI      :
    case VERI_TRI1     :
    case VERI_SUPPLY0  :
    case VERI_WAND     :
    case VERI_TRIAND   :
    case VERI_TRIOR    :
    case VERI_TRI0     :
    case VERI_SUPPLY1  :
    case VERI_WOR      :
    case VERI_TRIREG   :
    case VERI_UWIRE    :
        return 1 ;
    default :
        break ;
    }
    return 0 ;
}
unsigned VeriArrayType::IsIntegralType() const   { return 1 ; } // Packed array
unsigned VeriStructUnionType::IsIntegralType() const   { return _is_packed ; } // Packed struct/union
unsigned VeriEnumType::IsIntegralType() const   { return 1 ; } // Enum

unsigned VeriBuiltInType::IsRegisterType() const
{
    unsigned type = (_nettype) ? _nettype : _type ;
    switch (type) {
    // Integer atom types :
    case VERI_BYTE     :
    case VERI_CHAR     :
    case VERI_SHORTINT :
    case VERI_INT      :
    case VERI_LONGINT  :
    case VERI_INTEGER  :
    case VERI_TIME     :
    case VERI_GENVAR   :
    case VERI_UNSIGNED : // used in casting as type
    case VERI_SIGNED   : // used in casting as type

    // Integer vector type
    case VERI_BIT      :
    case VERI_LOGIC    :
    case VERI_VAR      : // VIPER #6325 : var is a variable type
    case VERI_REG      :
        return 1 ;
    default :
        break ;
    }
    return 0 ;
}
unsigned VeriNamedType::IsIntegralType() const
{
    return 0 ;
}
unsigned VeriArrayType::IsRegisterType() const
{
    return (_element_type) ? _element_type->IsRegisterType(): 0 ;
}

// Bit-stream type :
// * any integral, packed or string type
// * unpacked array, structure or classes of above type
// * dynamically sized arrays (dynamic, associative, queue) of any of above type
unsigned VeriBuiltInType::IsBitStreamType() const
{
    if (IsIntegralType() || IsString()) return 1 ;
    return 0 ;
}
unsigned VeriArrayType::IsBitStreamType() const  { return 1 ; }
unsigned VeriUnpackedArray::IsBitStreamType() const
{
    // It is for unpacked array and dynamically sized arrays (dynamic, associative, queue)
    // Check element type
    if (_element_type && _element_type->IsBitStreamType()) return 1 ;
    return (_element_type) ? 0 : 1 ; // No element type means scalar type (bit stream)
}
unsigned VeriStructUnionType::IsBitStreamType() const
{
    if (_is_packed) return 1 ; // Packed is bit stream type
    if (!_is_struct) return 0 ; // Union is not bit-stream type

    // Every element should be of bit-stream type
    MapIter mi ;
    VeriTypeInfo *member_type ;
    FOREACH_MAP_ITEM (_elements, mi, 0, &member_type) {
        // Return 0 if member is not of bit-stream type
        if (member_type && !member_type->IsBitStreamType()) return 0 ;
    }
    return 1 ; // All members are of bit-stream type
}
unsigned VeriEnumType::IsBitStreamType() const  { return 1 ; } // Always bit-stream type
unsigned VeriNamedType::IsBitStreamType() const
{
    if (!_id) return 0 ;

    if (_id->IsProcessing()) return 1 ; // VIPER #7662 : Check recursion
    _id->SetIsProcessing() ; // Set processing
    if (IsIntegralType()) {
        _id->ClearIsProcessing() ;
        return 1 ;
    }
    if(!_id->IsClass()) {
        _id->ClearIsProcessing() ;
        return 0 ; // Not class type, not bit-stream
    }

    // All members of class should be of bit-stream type
    VeriScope *class_scope = _id->LocalScope() ;

    Map *this_scope = (class_scope) ? class_scope->GetThisScope() : 0 ;

    // Iterate over all declared ids and check type
    MapIter mi ;
    VeriIdDef *eid ;

    // Iterate over all declared ids and check type
    FOREACH_MAP_ITEM (this_scope, mi, 0, &eid) {
        // VIPER #5517 : Ignore methods while checking bit-stream types
        if (!eid || !eid->IsVar() || eid->IsFunction() || eid->IsTask()) continue ; // Ignore ids other than variables
        VeriTypeInfo *id_type = eid->CreateType(0) ;
        if (id_type && (id_type->IsTypeParameterType() || id_type->IsUnresolvedNameType())) {
            delete id_type ;
            continue ;
        }
        if (id_type && !id_type->IsBitStreamType()) { // Member is not bit-strea type
            delete id_type ;
            _id->ClearIsProcessing() ;
            return 0 ;
        }
        delete id_type ;
    }
    _id->ClearIsProcessing() ;
    // FIXME :Check for base class
    return 1 ;
}
// Singular type :
// A singular type can be any data type except unpacked structure, unpacked union
// or unpacked array.
unsigned VeriBuiltInType::IsSingularType() const { return 1 ; } // Singular
unsigned VeriArrayType::IsSingularType() const { return 1 ; } // Singular
unsigned VeriUnpackedArray::IsSingularType() const { return 0 ; } // Not singular
unsigned VeriStructUnionType::IsSingularType() const
{
    if (_is_packed) return 1 ; // Singular type
    return 0 ; // unpacked struct/union, not singular
}
unsigned VeriEnumType::IsSingularType() const { return 1 ; } // Singular type
unsigned VeriNamedType::IsSingularType() const
{
    if (IsTypeParameterType() || IsUnresolvedNameType()) return 0 ; // Not resolved
    // Class handles are singular
    if (IsClassType()) return 1 ;

    return 0 ; // Not singular
}

// Equivalent type :
// 1. If two types match, they are equivalent.
// 2. An anonymous enum, unpacked structure, unpacked union type is equivalent to
// itself among data objects declared within same declaration statement and no
// other data types.
// 3. Packed arrays, packed structures, packed unions, and built-in integral types
// are equivalent if they contain same number of total bits, are either all 2 state
// or all 4-state, either all signed or all unsigned
// 4. Unpacked array types are equivalent by having equivalent element types and
// identifier shape. Shape is defined as number of dimensions and number of elements
// in each dimension, not the actual range of the dimension.
unsigned VeriBuiltInType::IsEquivalent(VeriTypeInfo *other, unsigned produce_error, VeriTreeNode *from, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsPacked() && !other->IsBuiltInType()) return 0 ; // Built-in type can be matched with other packed types
    VeriTypeInfo *other_type = other ;
    // If other is packed array, take its element type
    if (other->IsPackedArrayType()) other_type = other->ElementType() ;
    if (other_type && !other_type->IsBuiltInType()) return 1 ; // other is packed structure
    // VIPER #7092 : Ignore sign mismatch checking
    if (!ignore_sign_state && other_type && (IsSigned() != other_type->IsSigned())) {
        if (produce_error && from) from->Error("elements must both be signed or both be unsigned") ;
        return 0 ; // Sign mismatch, not equivalent
    }
    // VIPER #7092 : Ignore state mismatch
    if (!ignore_sign_state && other_type && ((Is2State() && !other_type->Is2State()) || (Is4State() && !other_type->Is4State()))) {
        if (produce_error && from) from->Error("elements must both be 2-state or both be 4-state") ;
        return 0 ; // 2 state/4 state mixed, not equivalent
    }
    if (IsUnpacked() && other_type) {
        if (Type() == other_type->Type()) return 1 ; // two types match
        return 0 ; // type should match
    }

    // Now 'other' is built-in type or bit (other == 0): Check
    unsigned is_other_integral = (other_type) ? other_type->IsIntegralType(): 1 ;
    if (is_other_integral != IsIntegralType()) {
       return 0 ; // if both are integrals, then equivalent else not
    }
    // If both are built-in-type, check their sizes
    if (other->IsBuiltInType() && (VeriNode::GetSizeOfBuiltInType(Type()) != VeriNode::GetSizeOfBuiltInType(other->Type()))) {
        if (produce_error && from) from->Error("elements must have same number of bits") ;
        return 0 ;
    }
    return 1 ; // equivalent
}

unsigned VeriArrayType::IsEquivalent(VeriTypeInfo *other, unsigned produce_error, VeriTreeNode *from, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsPacked()) return 0 ; // Built-in type can be matched with other packed types
    if (_string_literal && (other->IsStringLiteral() || other->IsString())) return 1 ; // Both are string literal
    if (!_string_literal && (other->IsStringLiteral() || other->IsString())) return 0 ;
    if (!other->IsIntegralType()) return 0 ; // this is integral, not other
    // VIPER #7092 : Ignore sign mismatch checking
    if (!ignore_sign_state && (IsSigned() != other->IsSigned())) {
        if (produce_error && from) from->Error("elements must both be signed or both be unsigned") ;
        return 0 ; // Sign mismatch, not equivalent
    }
    // VIPER #7092 : Ignore state mismatch
    if (!ignore_sign_state && ((Is2State() && !other->Is2State()) || (Is4State() && !other->Is4State()))) {
        if (produce_error && from) from->Error("elements must both be 2-state or both be 4-state") ;
        return 0 ; // 2 state/4 state mixed, not equivalent
    }

    return 1 ; // equivalent
}
unsigned VeriUnpackedArray::IsEquivalent(VeriTypeInfo *other, unsigned produce_error, VeriTreeNode *from, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsUnpackedArrayType()) return 0 ; // other not unpacked array
    // Now check element types

    VeriTypeInfo *other_element = other->ElementType() ;
    // If no element types i.e. element is scalar, so other can be packed
    if (!_element_type && (!other_element || other_element->IsPacked())) return 1 ;

    // Element of other is scalar, so element of this must be packed
    if (!other_element && _element_type && _element_type->IsPacked()) return 1 ;

    // Elements types are equivalent
    if (other_element && _element_type && other_element->IsEquivalent(_element_type, produce_error, from, ignore_sign_state)) return 1 ;
    return 0 ; // Not equivalent
}
// VIPER #5445: Dynamic array, associative array and queue are equivalent if they are
// same kind of array, have equivalent index types (for associative array), and have
// equivalent element type.
unsigned VeriAssociativeArray::IsEquivalent(VeriTypeInfo *other, unsigned produce_error, VeriTreeNode *from, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsAssociativeArrayType()) return 0 ; // other not associative array

    // Check index type:
    VeriTypeInfo *other_index = other->IndexType() ;

    if (_index_type && other_index && !other_index->IsEquivalent(_index_type, produce_error, from, ignore_sign_state)) return 0 ; // index types not equivalent

    // Now check element type
    return VeriUnpackedArray::IsEquivalent(other, produce_error, from, ignore_sign_state) ;
}
unsigned VeriDynamicArray::IsEquivalent(VeriTypeInfo *other, unsigned produce_error, VeriTreeNode *from, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsDynamicArrayType()) return 0 ; // other not dynamic array
    // Now check element type
    return VeriUnpackedArray::IsEquivalent(other, produce_error, from, ignore_sign_state) ;
}
unsigned VeriQueue::IsEquivalent(VeriTypeInfo *other, unsigned produce_error, VeriTreeNode *from, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsQueue()) return 0 ; // other not queue

    // Now check element type
    return VeriUnpackedArray::IsEquivalent(other, produce_error, from, ignore_sign_state) ;
}
unsigned VeriStructUnionType::IsEquivalent(VeriTypeInfo *other, unsigned /*produce_error*/, VeriTreeNode * /*from*/, unsigned ignore_sign_state) const
{
    if (!other) return 0 ; // Nothing to check

    // VIPER #7092 : Ignore sign mismatch
    if (!ignore_sign_state && (IsSigned() != other->IsSigned())) return 0 ; // Sign mismatch, not equivalent

    // Packed struct/union is equivalent to other packed types
    if (_is_packed) return other->IsPacked() ;

    // Two unpacked structure/union can be equivalent if members of both are same
    // having same data types. This can be check by pointer matching their member
    // identifiers easily.
    Map *other_members = other->GetMembers() ;
    if (!other_members) return 0 ; // 'other' is not struct/union: not equivalent
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM (_elements, mi, &id, 0) {
        if (!other_members->GetValue(id)) return 0 ; // Member is different
    }
    return 1 ; // Equivalent
}
unsigned VeriEnumType::IsEquivalent(VeriTypeInfo *other, unsigned /*produce_error*/, VeriTreeNode * /*from*/, unsigned /*ignore_sign_state*/) const
{
    if (!other) return 0 ; // Nothing to check
    // Enum type can be equivalent to only same enum type. Match pointers of enum
    // literals to check this
    Map *other_ids = other->GetEnumIds() ;

    if (!other_ids) return 0 ; // 'other' is not enum
    MapIter mi ;
    VeriIdDef *id ;
    char *name ;
    FOREACH_MAP_ITEM(_enum_ids, mi, &name, &id) {
        if (id != (VeriIdDef*)other_ids->GetValue(name)) return 0 ; // Different enum literal
    }
    return 1 ; // Equivalent
}
unsigned VeriNamedType::IsEquivalent(VeriTypeInfo *other, unsigned /*produce_error*/, VeriTreeNode * /*from*/, unsigned /*ignore_sign_state*/) const
{
    if (!other) return 0 ; // other is not named type

    if (!other->IsNamedType()) return 0 ;

    VeriIdDef *other_id = other->GetNameId() ;
    if (_id == other_id) return 1 ; // Both are same

    // If one type is interface and other is modport of same interface, types
    // are equivalent
    if (_id && other_id && ((_id->IsInterface() && other_id->IsModport()) ||
           (_id->IsModport() && other_id->IsInterface()))) {
        VeriScope *modport_scope = (_id->IsModport()) ? _id->LocalScope() : other_id->LocalScope() ;
        VeriScope *module_scope = (_id->IsInterface()) ? _id->LocalScope() : other_id->LocalScope() ;
        if (modport_scope && module_scope && (modport_scope->ModuleScope() == module_scope)) return 1 ; // Both are from same interface
    }
    // VIPER #4815 : If both sides are diferent interface identifiers, check whether
    // one is copied from another. If so they are equivalent
    VeriIdDef *this_interface = (_id && _id->IsInterface()) ? _id : 0 ;
    VeriIdDef *other_interface = (other_id && other_id->IsInterface()) ? other_id : 0 ;
    // If any side is modport, get its interface :
    if (!this_interface && _id && _id->IsModport()) {
        VeriScope *modport_scope = _id->LocalScope() ;
        this_interface = (modport_scope) ? modport_scope->GetContainingModule(): 0 ;
    }
    if (!other_interface && other_id && other_id->IsModport()) {
        VeriScope *modport_scope = other_id->LocalScope() ;
        other_interface = (modport_scope) ? modport_scope->GetContainingModule(): 0 ;
    }
    if (this_interface && other_interface) { // Both interfaces
        if (this_interface == other_interface) return 1 ; // Both same
        // If any one is copied interface, get the original one from which copy was made
        VeriModule *this_body = this_interface->GetModule() ;
        this_body = (this_body && this_body->GetOriginalModuleName()) ? this_body->GetOriginalModule() : this_body ;
        this_interface = (this_body) ? this_body->GetId(): 0 ;

        VeriModule *other_body = other_interface->GetModule() ;
        other_body = (other_body && other_body->GetOriginalModuleName()) ? other_body->GetOriginalModule() : other_body ;
        other_interface = (other_body) ? other_body->GetId(): 0 ;
        if (this_interface == other_interface) return 1 ; // Both same
    }
    // Both ids are not same. They are not equivalent for other types except class
    // If one is base class and other derived, both are equivalent
    if (_id && other_id && _id->IsClass() && other_id->IsClass()) {
        // FIXME : Currently we have no mechanism to determine whether one is derived
        // from other. We simply set the scope of base class as upper scope of derived
        // scope. This is also true for nested class. So we cannot rely on that to issue
        // error.
        if (IsDerivedFrom(other) || other->IsDerivedFrom(this)) return 1 ; // one is base, other is derived
    }
    return 0 ; // Not-Equivalent
}
// Assignment compatible types :
// All packed types are assignment compatible
unsigned VeriBuiltInType::IsAssignmentCompatible(VeriTypeInfo *other) const
{
    if (!other) return 0 ; // Nothing to check
    if (IsRealType() && !other->IsPacked() && !other->IsRealType()) return 0 ;
    if (IsPacked() && !other->IsPacked() && !other->IsRealType()) return 0 ; // Built-in type can be matched with other packed types
    if (IsChandleType() != other->IsChandleType()) return 0 ; // only chandle can be assigned to other chandle
    if (IsEventType() != other->IsEventType()) return 0 ; // event type mismatch
    if (IsString() && (!other->IsStringLiteral() && !other->IsString())) return 0 ; // one string, other not string/string literal
    if (!IsString() && other->IsString()) return 0 ; // both side not string type
    return 1 ; // assignment compatible
}

unsigned VeriArrayType::IsAssignmentCompatible(VeriTypeInfo *other) const
{
    if (!other) return 0 ; // Nothing to check
    if (other->IsChandleType() || other->IsEventType()) return 0 ; // both side not chandle/event
    if (_string_literal && (other->IsStringLiteral() || other->IsString())) return 1 ; // Both are string literal
    if (!_string_literal && (other->IsStringLiteral() || other->IsString() || other->IsUnpackedArrayType() || (other->IsStructure() && other->IsUnpacked()))) return 0 ;

    return 1 ; // assignment compatible
}
unsigned VeriUnpackedArray::IsAssignmentCompatible(VeriTypeInfo *other) const
{
    if (!other) return 0 ; // Nothing to check
    if (!other->IsUnpackedArrayType() && !other->IsStringLiteral()) return 0 ; // other not unpacked array
    return 1 ; // assignment compatible
}
unsigned VeriStructUnionType::IsAssignmentCompatible(VeriTypeInfo *other) const
{
    if (!other) return 0 ; // Nothing to check

    if (other->IsChandleType() || other->IsEventType() || other->IsString()) return 0 ;
    // Packed struct/union is assignment compatible to other packed types
    if (_is_packed && (other->IsUnpackedArrayType() || (other->IsStructure() && other->IsUnpacked()))) return 0 ;
    if (!_is_packed && other->IsPacked() && !other->IsStringLiteral()) return 0 ;
    if (!_is_packed && other->IsUnpacked() && !IsEquivalent(other, 0, 0, 0)) return 0 ;

    return 1 ; // Assignment compatible
}
unsigned VeriEnumType::IsAssignmentCompatible(VeriTypeInfo *other) const
{
    if (!other) return 0 ; // Nothing to check
    if (other->IsChandleType() || other->IsEventType() || other->IsString()) return 0 ; // these cannot be assigned to enum
    if (other->IsUnpacked()) return 0 ;
    return 1 ; // assignment compatible
}
unsigned VeriNamedType::IsAssignmentCompatible(VeriTypeInfo *other) const
{
    if (!other || !other->IsNamedType()) return 0 ; // other is not named type

    if (!IsEquivalent(other, 0, 0, 0)) return 0 ; // Not equivalent types
    return 1 ; // Assignment compatible
}
unsigned VeriNamedType::IsDerivedFrom(const VeriTypeInfo *other) const
{
    if (!_id || !other || !other->GetNameId()) return 0 ; // Not resolved names

    VeriIdDef *other_id = other->GetNameId() ;
    if (!_id->IsClass() || !other_id || !other_id->IsClass()) return 0 ; // Both should be class

    VeriIdDef *class_id = _id ;
    while (class_id) {
        VeriModuleItem *this_class_decl = class_id->GetModuleItem() ;
        VeriName *this_base_class = (this_class_decl) ? this_class_decl->GetBaseClassName() : 0 ;
        Array *interface_classes = (this_class_decl) ? this_class_decl->GetInterfaceClasses(): 0 ;
        if (this_base_class) {
            // This is derived class. Check whether other is base class of this
            if (this_base_class->GetId() == other_id) return 1 ; //Other is base class of this
            class_id = this_base_class->GetId() ; // Update class_id to its base
        } else if (interface_classes && interface_classes->Size()) {
            unsigned i ;
            VeriName *interface_class ;
            FOREACH_ARRAY_ITEM(interface_classes, i, interface_class) {
                if (!interface_class) continue ;
                VeriIdDef *interface_class_id = interface_class->GetId() ;
                if (CheckInInterfaceClass(interface_class_id, other_id)) return 1 ;
            }
            break ;
        } else {
            break ; // Not a derived class
        }
    }
    return 0 ;
}
unsigned VeriNamedType::CheckInInterfaceClass(const VeriIdDef *interface_class_id, const VeriIdDef *other_id) const
{
    if (!interface_class_id || !other_id) return 0 ;

    if (interface_class_id == other_id) return 1 ;

    // Not matching with argument specified interface class, check extends clause
    // specified classes
    VeriModuleItem *class_body = interface_class_id->GetModuleItem() ;
    Array *interface_classes = class_body ? class_body->GetInterfaceClasses(): 0 ;
    unsigned i ;
    VeriName *interface_class ;
    VeriIdDef *class_id ;
    FOREACH_ARRAY_ITEM(interface_classes, i, interface_class) {
        class_id = interface_class ? interface_class->GetId(): 0 ;
        if (class_id && CheckInInterfaceClass(class_id, other_id)) return 1 ;
    }
    return 0 ;
}

// Base type : Base type is built-in types/struct/union/enum/named type
VeriTypeInfo *VeriTypeInfo::BaseType() const { return const_cast<VeriTypeInfo*>(this) ; }
VeriTypeInfo *VeriArrayType::BaseType() const
{
    if (_element_type) return _element_type->BaseType() ;
    return const_cast<VeriArrayType*>(this) ;
}
VeriTypeInfo *VeriUnpackedArray::BaseType() const
{
    return (_element_type) ? _element_type->BaseType() : 0 ;
}

// VIPER #5949: Check for net data type mismatch:
unsigned VeriTypeInfo::CheckNetType(const VeriTypeInfo *other) const
{
    if (!other) return 0 ;
    if (IsNetType() && other->IsNetType()) {
        unsigned nettype = GetNetType() ;
        unsigned other_nettype = other->GetNetType() ;
        if (nettype != other_nettype) return 1 ;
    }
    return 0 ;
}

// Equal type :
unsigned VeriBuiltInType::IsEqual(VeriTypeInfo *other, unsigned /*is_in_alias*/) const
{
    if (!other || !other->IsBuiltInType()) return 0 ; // Not equal
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    if (_type != other->Type()) return 0 ; // Type mismatch
    return 1 ; // Same
}
unsigned VeriArrayType::IsEqual(VeriTypeInfo *other, unsigned is_in_alias) const
{
    if (!other || !other->IsPackedArrayType()) return 0 ; // Not same
    // Viper 5496. Alias should not match number of packed dimentions between the two type
    if (PackedDimension() != other->PackedDimension() && !is_in_alias) return 0 ; // packed dimension mismatch
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    VeriTypeInfo *other_element_type = other->ElementType() ;
    if (_element_type && other_element_type && !_element_type->IsEqual(other_element_type, is_in_alias)) return 0 ; // Not same
   return 1 ; // Same
}
unsigned VeriFixedSizeArray::IsEqual(VeriTypeInfo *other, unsigned is_in_alias) const
{
    if (!other || !other->IsFixedArrayType()) return 0 ; // Not same
    if (UnpackedDimension() != other->UnpackedDimension()) return 0 ; // unpacked dimension mismatch
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    VeriTypeInfo *other_element_type = other->ElementType() ;
    if (_element_type && other_element_type && !_element_type->IsEqual(other_element_type, is_in_alias)) return 0 ; // Not same
    return 1 ; // Same
}
unsigned VeriAssociativeArray::IsEqual(VeriTypeInfo *other, unsigned is_in_alias) const
{
    if (!other || !other->IsAssociativeArrayType()) return 0 ; // Not same
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    VeriTypeInfo *other_ele_type = other->ElementType() ;
    if (_element_type && other_ele_type && !_element_type->IsEqual(other_ele_type, is_in_alias)) return 0 ; // Not same
    return 1 ; // Same
}
unsigned VeriDynamicArray::IsEqual(VeriTypeInfo *other, unsigned is_in_alias) const
{
    if (!other || !other->IsDynamicArrayType()) return 0 ; // Not same
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    VeriTypeInfo *other_ele_type = other->ElementType() ;
    if (_element_type && other_ele_type && !_element_type->IsEqual(other_ele_type, is_in_alias)) return 0 ; // Not same
    return 1 ; // Same
}
unsigned VeriQueue::IsEqual(VeriTypeInfo *other, unsigned is_in_alias) const
{
    if (!other || !other->IsQueue()) return 0 ; // Not same
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    VeriTypeInfo *other_ele_type = other->ElementType() ;
    if (_element_type && other_ele_type && !_element_type->IsEqual(other_ele_type, is_in_alias)) return 0 ; // Not same
    return 1 ; // Same
}
unsigned VeriStructUnionType::IsEqual(VeriTypeInfo *other, unsigned /*is_in_alias*/) const
{
    if (!other) return 0 ;
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    if (_scope != other->Scope()) return 0 ; // Pointer match scopes
    return 1 ; // Same
}
unsigned VeriEnumType::IsEqual(VeriTypeInfo *other, unsigned /*is_in_alias*/) const
{
    if (!other || !other->IsEnumeratedType()) return 0 ; // Not same
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch

    // Enum ids should be same
    if (!IsEquivalent(other, 0, 0, 0)) return 0 ; // ids not same
    return 1 ;
}
unsigned VeriNamedType::IsEqual(VeriTypeInfo *other, unsigned /*is_in_alias*/) const
{
    if (CheckNetType(other)) return 0 ; // VIPER #5949: Net type mismatch
    if (!IsEquivalent(other, 0, 0, 0)) return 0 ; // Not same
    return 1 ;
}
// 2 state/4 state type:
unsigned VeriBuiltInType::Is2State() const
{
    switch (_type) {
    case VERI_SHORTINT :
    case VERI_BYTE     :
    case VERI_LONGINT  :
    case VERI_INT      :
    case VERI_BIT      :
    case VERI_CHAR     :
    case VERI_REAL     :
    case VERI_SHORTREAL:
    case VERI_REALTIME :
        return 1 ;
    default :
        break ;
    }
    return 0 ;
}
unsigned VeriArrayType::Is2State() const
{
    return (_element_type) ? _element_type->Is2State() : 0 ;
}
unsigned VeriUnpackedArray::Is2State() const
{
    return (_element_type) ? _element_type->Is2State() : 0 ;
}
unsigned VeriStructUnionType::Is2State() const
{
    if (!_is_packed) return 0 ; // 2 state and 4 state is valid for packed only

    // Check type of each member
    MapIter mi ;
    VeriIdDef *id ;
    VeriTypeInfo *type ;
    FOREACH_MAP_ITEM(_elements, mi, &id, &type) {
        if (!type) continue ;
        if (!type->Is2State()) return 0 ; // Not 2 state
    }
    return 1 ; // 2 state
}
unsigned VeriEnumType::Is2State() const
{
    return (_base_type) ? _base_type->Is2State() : 1 ;
}
unsigned VeriNamedType::Is2State() const
{
    return 0 ; // FIXME check members
}
unsigned VeriBuiltInType::Is4State() const
{
    switch (_type) {
    case VERI_TIME     :
    case VERI_INTEGER  :
    case VERI_REG      :
    case VERI_LOGIC    :
    // net types :
    case VERI_UWIRE    :
    case VERI_WIRE     :
    case VERI_TRI      :
    case VERI_TRI1     :
    case VERI_SUPPLY0  :
    case VERI_WAND     :
    case VERI_TRIAND   :
    case VERI_TRIOR    :
    case VERI_TRI0     :
    case VERI_SUPPLY1  :
    case VERI_WOR      :
    case VERI_TRIREG   :
        return 1 ;
    default : break ;
    }
    return 0 ; // not 4 state
}
unsigned VeriArrayType::Is4State() const
{
    return (_element_type) ? _element_type->Is4State() : 0 ;
}
unsigned VeriUnpackedArray::Is4State() const
{
    return (_element_type) ? _element_type->Is4State() : 0 ;
}
unsigned VeriStructUnionType::Is4State() const
{
    if (!_is_packed) return 0 ; // 2 state and 4 state is valid for packed only

    // Check type of each member
    MapIter mi ;
    VeriIdDef *id ;
    VeriTypeInfo *type ;
    FOREACH_MAP_ITEM(_elements, mi, &id, &type) {
        if (!type) continue ;
        // if any bit is 4 state, it is 4 state
        if (type->Is4State()) return 1 ; // 4 state
    }
    return 0 ; // not 4 state
}
unsigned VeriEnumType::Is4State() const
{
    return (_base_type) ? _base_type->Is4State() : 0 ;
}
unsigned VeriNamedType::Is4State() const
{
    return 0 ; // FIXME check members
}
unsigned VeriArrayType::Type() const
{
    return (_element_type) ? _element_type->Type() : VERI_REG ;
}
unsigned VeriUnpackedArray::Type() const
{
    return (_element_type) ? _element_type->Type() : VERI_REG ;
}
unsigned VeriStructUnionType::Type() const
{
    return (_is_struct) ? VERI_STRUCT : VERI_UNION ;
}
unsigned VeriEnumType::Type() const
{
    return VERI_ENUM ;
}

// VIPER #3397 : Check the usage of identifier in type checking
void VeriTypeInfo::CheckIdentifierUsage(const VeriIdDef *id, const VeriTypeInfo *id_type, VeriTreeNode::veri_type_env environment, const VeriTreeNode *from)
{
    if (!id || !from ) return ; // Cann't check anything without id
    if (id->IsUnresolvedSubprog()) return ; // unresolved id

    switch(environment) {
    case VeriTreeNode::READ_INSTANCE :
        // Instance identifier/modport/interface type ports are allowed
        if (!id->IsInst() && !id->IsModport() && !id->IsInterfacePort() && (!id_type || !id_type->IsVirtualInterface())) {
            from->Error("%s is not an instance of %s", id->Name(), "interface") ;
        }
        break ;
    case VeriTreeNode::INTERFACE_TYPE :
        // Interface instance/virtual interface type objects are allowed
        // VIPER #4782: Allow interface port :
        // VIPER #5220 : Allow modports :
        if (!id->IsInst() && !id->IsVirtualInterface() && !id->IsInterfacePort() && !id->IsModport() && (!id_type || !id_type->IsVirtualInterface())) {
            from->Error("%s is not an instance of %s", id->Name(), "interface") ;
        }
        break ;
    case VeriTreeNode::CASE_CONDITION : // VIPER #7331
    case VeriTreeNode::READ :
    case VeriTreeNode::INST_READ :
    case VeriTreeNode::WRITE :
    case VeriTreeNode::INST_WRITE :
    case VeriTreeNode::INIT_VAL :
    case VeriTreeNode::PROP_SEQ_INIT_VAL :
    case VeriTreeNode::RETURN_EXPR :
    case VeriTreeNode::VALUE_RANGE_LIST :
        // usage of id in a regular (RHS) expression
        if (!id->IsVar() && !id->IsParam() && !id->IsEvent() && !id->IsGenVar() && !id->IsCovergroup() && !id->IsProduction() && !id->IsClass() && !id->IsSequence() && !id->IsProperty() && !id->IsLetId()) {
                from->Error("%s is not valid in an expression", id->Name()) ;
        }
        break ;
    case VeriTreeNode::RANGE_BOUND :
    case VeriTreeNode::PARAM_INIT :
        // usage of id in initial value of parameter and range : can be type identifier
        if (!id->IsVar() && !id->IsParam() && !id->IsType() && !id->IsGenVar() && !id->IsCovergroup() && !id->IsProduction() && !id->IsLetId()) {
            from->Error("%s is not valid in an expression", id->Name()) ;
        }
        break ;
    default :
        break ;
    }
}
void VeriBuiltInType::CheckNetDataType(const VeriTreeNode *from, unsigned allow_reg) const
{
    // VIPER #5716: Ignore type parameter type
    if (!from || !_type || IsTypeParameterType()) return ; // If no _type exist, it is VERI_LOGIC
    if (!IsIntegralType()) from->Error("illegal net data type") ;

    // 'reg' is not allowed as net data type
    if (!allow_reg && (_type == VERI_REG)) from->Error("illegal net data type") ;

    if (!Is4State()) from->Error("net data types must be 4 state") ;
}
void VeriArrayType::CheckNetDataType(const VeriTreeNode *from, unsigned allow_reg) const
{
    if (_element_type) _element_type->CheckNetDataType(from, allow_reg) ;
}
void VeriFixedSizeArray::CheckNetDataType(const VeriTreeNode *from, unsigned allow_reg) const
{
    if (_element_type) _element_type->CheckNetDataType(from, allow_reg) ;
}
void VeriAssociativeArray::CheckNetDataType(const VeriTreeNode *from, unsigned /*allow_reg*/) const
{
    if (from) from->Error("%s net arrays are not allowed", "associative") ;
}
void VeriDynamicArray::CheckNetDataType(const VeriTreeNode *from, unsigned /*allow_reg*/) const
{
    if (from) from->Error("%s net arrays are not allowed", "dynamic") ;
}
void VeriQueue::CheckNetDataType(const VeriTreeNode *from, unsigned /*allow_reg*/) const
{
    if (from) from->Error("%s net arrays are not allowed", "dynamic") ;
}
void VeriStructUnionType::CheckNetDataType(const VeriTreeNode *from, unsigned /*allow_reg*/) const
{
    // Check type of each member
    MapIter mi ;
    VeriIdDef *id ;
    VeriTypeInfo *type ;
    FOREACH_MAP_ITEM(_elements, mi, &id, &type) {
        // Always allow 'reg', as data type starts with struct/union i.e.
        // struct/union is token after net type
        if (type) type->CheckNetDataType(from, 1) ;
    }
}
void VeriEnumType::CheckNetDataType(const VeriTreeNode *from, unsigned /*allow_reg*/) const
{
    // Always allow 'reg', as data type starts with keyword 'enum' i.e
    // 'enum' is the lexical token after net type
    if (_base_type) _base_type->CheckNetDataType(from, 1) ;
}
void VeriNamedType::CheckNetDataType(const VeriTreeNode *from, unsigned /*allow_reg*/) const
{
    // Type parameter type or unresolved name type is allowed
    if (IsTypeParameterType() || IsUnresolvedNameType()) return ;

    // Other types are illegal net type
    if (from) from->Error("illegal net data type") ;
}
////////////////              Scope     ///////////////////////////////
VeriScope *VeriArrayType::Scope() const
{
    return (_element_type) ? _element_type->Scope(): 0 ;
}
VeriScope *VeriUnpackedArray::Scope() const
{
    return (_element_type) ? _element_type->Scope(): 0 ;
}
VeriScope *VeriNamedType::Scope() const
{
    VeriScope *scope = (_id) ? _id->LocalScope(): 0 ;
    if (!scope && _id && _id->IsInterfaceInst()) {
        VeriModule *inst_mod = _id->GetInstantiatedModule() ;
        scope = (inst_mod) ? inst_mod->GetScope() : 0 ;
    }
    return scope ;
}

/******************************************************************************/
// Constraint :
/******************************************************************************/
VeriConstraint::VeriConstraint() : VeriNode() {}
VeriConstraint::~VeriConstraint() {}
VeriScalarTypeConstraint::VeriScalarTypeConstraint(unsigned type)
    : VeriConstraint(),
    _type(type),
    _sign(0)
{}
VeriScalarTypeConstraint::~VeriScalarTypeConstraint() {}
VeriEnumConstraint::VeriEnumConstraint(VeriConstraint *base, Array *enum_literals)
    : VeriConstraint(),
    _base(base),
    _ids(enum_literals)
{}
VeriEnumConstraint::~VeriEnumConstraint()
{
    delete _base ;
    _ids = 0  ; // not owned by this class, owned by VeriEnum
}
VeriRangeConstraint::VeriRangeConstraint(int left, int right)
    : VeriConstraint(),
    _left(left),
    _right(right),
    _is_to(0),
    _is_packed(0),
    _is_dynamic(0)
{
    if (_left < _right) _is_to = 1 ;
}
VeriRangeConstraint::~VeriRangeConstraint() {}
VeriArrayConstraint::VeriArrayConstraint(VeriConstraint *index_constraint, VeriConstraint *element_constraint)
    : VeriConstraint(),
    _index_constraint(index_constraint),
    _element_constraint(element_constraint)
{}
VeriArrayConstraint::~VeriArrayConstraint()
{
    delete _index_constraint ;
    delete _element_constraint ;
}
VeriRecordConstraint::VeriRecordConstraint(Array *element_constraints, Map *element_ids, unsigned origin_type, unsigned is_packed)
    : VeriConstraint(),
    _element_constraints(element_constraints),
    _element_ids(element_ids),
    _full_constraint(0),
    _type(origin_type),
    _is_packed(is_packed),
    _is_signed(0)
{
    if (_is_packed || (_type == VERI_UNION)) {
        // Create the full constraint for this record constraint if it is packed or represents an union:
        unsigned size = 0 ;
        unsigned i ;
        VeriConstraint *element_constraint ;
        FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
            if (!element_constraint) continue ;
            unsigned ele_size = element_constraint->NumOfBits() ;
            if (_type == VERI_UNION) {
                // For unions, calculate the maximum size:
                if (ele_size > size) size = ele_size ;
            } else {
                // For others, calculate the total size:
                size = size + ele_size ;
            }
        }
        // It should be a range constraint of (size-1 downto 0) for the largest element constraint:
        _full_constraint = new VeriScalarTypeConstraint(VERI_REG) ;
        if (size > 1) {
            // Only do this if size is greater than one, create an array constraint with appropriate range:
            VeriRangeConstraint *range = new VeriRangeConstraint((int)size-1, 0) ;
            if (_is_packed) range->SetAsPacked() ;
            _full_constraint = new VeriArrayConstraint(range, _full_constraint) ;
        }
    }
}

VeriRecordConstraint::~VeriRecordConstraint()
{
    // VIPER #6434 : Donot delete _element_constraints and _element_ids, as those are
    // owned by VeriStructUnion class
    //unsigned i ;
    //VeriConstraint *elem_constraint ;
    //FOREACH_ARRAY_ITEM (_element_constraints, i, elem_constraint) delete elem_constraint ;
    //delete _element_constraints ;
    //delete _element_ids ;
    _element_constraints = 0 ;
    _element_ids = 0 ;
    delete _full_constraint ;
}

/******************************************************************************/
// Copy
/******************************************************************************/
VeriConstraint *VeriScalarTypeConstraint::Copy() const
{
    VeriConstraint *copied_constraint = new VeriScalarTypeConstraint(_type) ;
    if (_sign) copied_constraint->SetSign(_sign) ;
    return copied_constraint ;
}
VeriConstraint *VeriScalarTypeConstraint::CopyInternal(Map * /*copy_map*/) const
{
    return Copy() ;
}
VeriConstraint *VeriEnumConstraint::Copy()  const
{
    return new VeriEnumConstraint((_base) ? _base->Copy():0, _ids) ;
}
VeriConstraint *VeriEnumConstraint::CopyInternal(Map *copy_map) const
{
    return new VeriEnumConstraint((_base) ? _base->CopyInternal(copy_map): 0, _ids) ;
}
VeriConstraint *VeriRangeConstraint::Copy() const
{
    VeriConstraint *constraint = new VeriRangeConstraint(_left, _right) ;
    if (_is_packed) constraint->SetAsPacked() ;
    if (_is_dynamic) constraint->SetAsDynamicRange() ;
    return constraint ;
}
VeriConstraint *VeriRangeConstraint::CopyInternal(Map * /*copy_map*/) const
{
    return Copy() ;
}
VeriConstraint *VeriArrayConstraint::Copy() const
{
    return new VeriArrayConstraint((_index_constraint) ? _index_constraint->Copy(): 0, (_element_constraint) ? _element_constraint->Copy(): 0) ;
}
VeriConstraint *VeriArrayConstraint::CopyInternal(Map *copy_map) const
{
    return new VeriArrayConstraint((_index_constraint) ? _index_constraint->CopyInternal(copy_map): 0, (_element_constraint) ? _element_constraint->CopyInternal(copy_map): 0) ;
}
VeriConstraint *VeriRecordConstraint::Copy() const
{
    if (!_element_constraints || !_element_ids) return 0 ;
    // VIPER #6434 : Donot copy _element_constraints and _element_ids, they are not owned
    // by this class
    /*Array *element_constraints = new Array(_element_constraints->Size()) ;
    unsigned i ;
    VeriConstraint *elem_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, elem_constraint) {
        element_constraints->InsertLast((elem_constraint) ? elem_constraint->Copy(): 0) ;
    }
    Map *all_ids = new Map(STRING_HASH, _element_ids->Size()) ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(_element_ids, mi, 0, &id) {
        if (!id) continue ;
        (void) all_ids->Insert(id->Name(), id) ;
    }*/
    VeriConstraint *copied_constraint = new VeriRecordConstraint(_element_constraints, _element_ids, _type, _is_packed) ;
    if (IsSigned()) copied_constraint->SetSign(VERI_SIGNED) ;
    return copied_constraint ;
}
VeriConstraint *VeriRecordConstraint::CopyInternal(Map *copy_map) const
{
    if (!_element_constraints || !_element_ids) return 0 ;
    Array *new_constraints = copy_map ? (Array*)copy_map->GetValue(_element_constraints): _element_constraints ;
    if (!new_constraints) new_constraints = _element_constraints ;
    VeriConstraint *copied_constraint =  new VeriRecordConstraint(new_constraints, _element_ids, _type, _is_packed) ;
    if (IsSigned()) copied_constraint->SetSign(VERI_SIGNED) ;
    return copied_constraint ;
}
/******************************************************************************/
//  Sign
/******************************************************************************/
void VeriArrayConstraint::SetSign(unsigned sign)
{
    if (_element_constraint) _element_constraint->SetSign(sign) ;
}
void VeriRecordConstraint::SetSign(unsigned sign)
{
    _is_signed = (sign == VERI_SIGNED) ? 1 : 0 ;
}

unsigned VeriScalarTypeConstraint::IsSigned() const
{
    if (_sign == VERI_SIGNED) return 1 ;
    if (_sign == VERI_UNSIGNED) return 0 ;

    // Check whether type is signed by definition
    switch (_type) {
    case VERI_INTEGER :
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_BYTE :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_SHORTREAL :
    case VERI_CHAR :
        return 1 ; // Signed types
    default : break ;
    }
    return 0 ;
}
unsigned VeriEnumConstraint::IsSigned() const
{
    return (_base) ? _base->IsSigned(): 0 ;
}
unsigned VeriArrayConstraint::IsSigned() const
{
    return _element_constraint ? _element_constraint->IsSigned(): 0 ;
}
unsigned VeriRecordConstraint::IsSigned() const
{
    return _is_signed ;
}
/******************************************************************************/
// 2-state and 4-state
/******************************************************************************/
unsigned VeriConstraint::Is2State() const { return 0 ; }
unsigned VeriScalarTypeConstraint::Is2State() const
{
    switch (_type) {
    case VERI_SHORTINT :
    case VERI_BYTE     :
    case VERI_LONGINT  :
    case VERI_INT      :
    case VERI_BIT      :
    case VERI_CHAR     :
    case VERI_SHORTREAL:
    case VERI_REAL     :
    case VERI_REALTIME : return 1 ;
    default : break ;
    }
    return 0 ;
}
unsigned VeriEnumConstraint::Is2State() const
{
    return _base ? _base->Is2State(): 1 ;
}
unsigned VeriArrayConstraint::Is2State() const
{
    return _element_constraint ? _element_constraint->Is2State(): 0 ;
}
unsigned VeriRecordConstraint::Is2State() const
{
    if (!_is_packed) return 0 ; // 2 state and 4 state is valid for packed only

    unsigned i ;
    VeriConstraint *ele_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele_constraint) {
        if (ele_constraint && !ele_constraint->Is2State()) return 0 ;
    }
    return 1 ; // 2 state
}
unsigned VeriConstraint::Is4State() const { return 0 ; }
unsigned VeriScalarTypeConstraint::Is4State() const
{
    switch (_type) {
    case VERI_TIME     :
    case VERI_INTEGER  :
    case VERI_REG      :
    case VERI_LOGIC    :
    // net types :
    case VERI_UWIRE    :
    case VERI_WIRE     :
    case VERI_TRI      :
    case VERI_TRI1     :
    case VERI_SUPPLY0  :
    case VERI_WAND     :
    case VERI_TRIAND   :
    case VERI_TRIOR    :
    case VERI_TRI0     :
    case VERI_SUPPLY1  :
    case VERI_WOR      :
    case VERI_TRIREG   :
        return 1 ;
    default : break ;
    }
    return 0 ; // not 4 state
}
unsigned VeriEnumConstraint::Is4State() const
{
    return _base ? _base->Is4State(): 0 ;
}
unsigned VeriArrayConstraint::Is4State() const
{
    return _element_constraint ? _element_constraint->Is4State(): 0 ;
}
unsigned VeriRecordConstraint::Is4State() const
{
    if (!_is_packed) return 0 ; // 2 state and 4 state is valid for packed only

    unsigned i ;
    VeriConstraint *ele_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele_constraint) {
        // if any element is 4 state, it is 4 state
        if (ele_constraint && ele_constraint->Is4State()) return 1 ;
    }
    return 0 ; // not 4 state
}
// Constraint Access Routines
/******************************************************************************/
VeriConstraint *VeriConstraint::ElementConstraint() const        { return 0 ; }
VeriConstraint *VeriArrayConstraint::ElementConstraint() const   { return _element_constraint ; }
//VeriConstraint *VeriEnumConstraint::ElementConstraint() const    { return (_base) ? _base->ElementConstraint(): 0 ; }
// VIPER #7807 : Element constraint of enum should be its base
VeriConstraint *VeriEnumConstraint::ElementConstraint() const    { return _base ; }

VeriConstraint *VeriConstraint::IndexConstraint() const          { return 0 ; }
VeriConstraint *VeriArrayConstraint::IndexConstraint() const     { return _index_constraint ; }
VeriConstraint *VeriEnumConstraint::IndexConstraint() const      { return (_base) ? _base->IndexConstraint() : 0 ; }
VeriConstraint *VeriConstraint::ElementConstraintAt(unsigned /*pos*/) const       { return 0 ; }
VeriConstraint *VeriArrayConstraint::ElementConstraintAt(unsigned /*pos*/) const  { return _element_constraint ; }
VeriConstraint *VeriEnumConstraint::ElementConstraintAt(unsigned pos) const       { return (_base) ? _base->ElementConstraintAt(pos) : 0 ; }
VeriConstraint *VeriRecordConstraint::ElementConstraintAt(unsigned pos) const
{
    if (!_element_constraints || (_element_constraints->Size() <= pos)) return 0 ;
    return (VeriConstraint*)_element_constraints->At(pos) ;
}

VeriConstraint *VeriConstraint::ElementConstraint(const char * /*element_name*/) const  { return 0 ; } // Virtual catcher
VeriConstraint *VeriRecordConstraint::ElementConstraint(const char *element_name) const
{
    if (!element_name || !_element_ids) return 0 ;

    // Get the constraint by position of the element:
    // VIPER #4315 : Use element_name instead of id to get index
    unsigned pos = _element_ids->IndexOf(element_name) ;
    return ElementConstraintAt(pos) ;
}

VeriConstraint *VeriConstraint::ElementConstraintAtDepth(unsigned depth)
{
    if (!depth) return this ;

    VeriConstraint *constraint = ElementConstraint() ;
    return constraint ? constraint->ElementConstraintAtDepth(depth-1) : 0 ;
}

VeriConstraint *VeriConstraint::BitElementConstraint() const { return 0 ; }
VeriConstraint *VeriScalarTypeConstraint::BitElementConstraint() const
{
    unsigned bit_type = 0 ;
    switch (_type) {
    // All net types are 1 bit :
    case VERI_WIRE    :
    case VERI_TRI     :
    case VERI_TRI1    :
    case VERI_SUPPLY0 :
    case VERI_WAND    :
    case VERI_TRIAND  :
    case VERI_TRIOR   :
    case VERI_TRI0    :
    case VERI_SUPPLY1 :
    case VERI_WOR     :
    case VERI_TRIREG  :
    case VERI_UWIRE   :

    // Register types :
    case VERI_INTEGER :

    // register single bit type :
    case VERI_REG :
    // SV register bit types :
    case VERI_LOGIC :
        bit_type = VERI_REG ; // 4 state bit
        break ;
    default :
        bit_type = VERI_BIT ; // 2 state
        break ; // a single-bit type
    }
    return (bit_type) ? new VeriScalarTypeConstraint(bit_type): 0 ;
}
VeriConstraint *VeriEnumConstraint::BitElementConstraint() const
{
    return (_base) ? _base->BitElementConstraint(): 0 ;
}
// Get scalar / index range bounds
int VeriConstraint::Left() const                         { return 0 ; }
int VeriConstraint::Right() const                        { return 0 ; }
int VeriConstraint::Low() const                          { return 0 ; }
int VeriConstraint::High() const                         { return 0 ; }
unsigned VeriConstraint::IsTo() const                    { return 1 ; } // for record constraints
int VeriScalarTypeConstraint::Left() const               { return (int)NumOfBits() -1 ; }
int VeriRangeConstraint::Left() const                    { return _left ; }
int VeriRangeConstraint::Right() const                   { return _right ; }
int VeriRangeConstraint::Low() const                     { return (_is_to) ? _left: _right ; }
int VeriRangeConstraint::High() const                    { return (_is_to) ? _right: _left ; }
unsigned VeriRangeConstraint::IsTo() const               { return _is_to ; }

int VeriArrayConstraint::Left() const                    { return (_index_constraint) ? _index_constraint->Left(): 0 ; }
int VeriArrayConstraint::Right() const                   { return (_index_constraint) ? _index_constraint->Right() : 0 ; }
int VeriArrayConstraint::Low() const                     { return (_index_constraint) ? _index_constraint->Low(): 0 ; }
int VeriArrayConstraint::High() const                    { return (_index_constraint) ? _index_constraint->High(): 0 ; }
unsigned VeriArrayConstraint::IsTo() const               { return (_index_constraint) ? _index_constraint->IsTo(): 0 ; }

unsigned VeriConstraint::Length() const                  { return 0 ; }
int VeriEnumConstraint::Left() const                     { return (_base) ? _base->Left() : 0 ; }
int VeriEnumConstraint::Right() const                    { return (_base) ? _base->Right() : 0 ; }
int VeriEnumConstraint::Low() const                      { return (_base) ? _base->Low() : 0 ; }
int VeriEnumConstraint::High() const                     { return (_base) ? _base->High() : 0 ; }
unsigned VeriEnumConstraint::IsTo() const                { return (_base) ? _base->IsTo(): 0 ; }
unsigned VeriEnumConstraint::Length() const              { return (_base) ? _base->Length(): 0 ; }
unsigned VeriArrayConstraint::Length() const             { return (_index_constraint) ? _index_constraint->Length(): 0 ; }
unsigned VeriRangeConstraint::Length() const
{
    return (_is_to) ? (unsigned)((_right - _left) +1) : (unsigned)((_left - _right) +1) ;
}

int VeriRecordConstraint::Left() const                   { return (_full_constraint) ? _full_constraint->Left() : 0 ; }
int VeriRecordConstraint::Right() const                  { return (_full_constraint) ? _full_constraint->Right() : 0 ; }
int VeriRecordConstraint::Low() const                    { return (_full_constraint) ? _full_constraint->Low() : 0 ; }
int VeriRecordConstraint::High() const                   { return (_full_constraint) ? _full_constraint->High() : 0 ; }
unsigned VeriRecordConstraint::IsTo() const              { return (_full_constraint) ? _full_constraint->IsTo() : 0 ; }
unsigned VeriRecordConstraint::IsPacked() const          { return _is_packed ; }
unsigned VeriRecordConstraint::Length() const
{
    if (_type==VERI_UNION) return 1 ; // Length of union is 1
    return (_element_constraints) ? _element_constraints->Size(): 0 ;
}

char *VeriScalarTypeConstraint::Image() const
{
    return Strings::save(PrintToken(_type)) ;
}
char *VeriEnumConstraint::Image() const
{
    char *image = 0 ;
    unsigned i ;
    VeriIdDef *enum_id ;
    FOREACH_ARRAY_ITEM(_ids, i, enum_id) {
        if (!enum_id) continue ;

        if (!image) {
            image = Strings::save(enum_id->Name()) ;
        } else {
            char *tmp = image ;
            image = Strings::save(image, ", ", enum_id->Name()) ;
            Strings::free(tmp) ;
        }
    }
    return image ;
}
char *VeriRangeConstraint::Image() const
{
    char *left = Strings::itoa(_left) ;
    char *right = Strings::itoa(_right) ;
    char *image = Strings::save(left, " : ", right) ;
    Strings::free(left) ;
    Strings::free(right) ;
    return image ;
}
char *VeriArrayConstraint::Image() const
{
    // Print only the index constraint..
    return (_index_constraint) ? _index_constraint->Image(): 0 ;
}
char *VeriRecordConstraint::Image() const
{
    char *image = 0 ;
    MapIter mi ;
    char *element ;
    FOREACH_MAP_ITEM(_element_ids, mi, &element, 0) {
        if (!element) continue ;
        if (!image) {
            image = Strings::save(element) ;
        } else {
            char *tmp = image ;
            image = Strings::save(image, ", ", element) ;
            Strings::free(tmp) ;
        }
    }
    return image ;
}
unsigned VeriConstraint::IsRangeConstraint() const                { return 0 ; }
unsigned VeriRangeConstraint::IsRangeConstraint() const           { return 1 ; }
unsigned VeriConstraint::IsArrayConstraint() const                { return 0 ; }
unsigned VeriArrayConstraint::IsArrayConstraint() const           { return 1 ; }
unsigned VeriConstraint::IsRecordConstraint() const               { return 0 ; }
unsigned VeriRecordConstraint::IsRecordConstraint() const         { return 1 ; }
unsigned VeriConstraint::IsScalarTypeConstraint() const           { return 0 ; }
unsigned VeriScalarTypeConstraint::IsScalarTypeConstraint() const { return 1 ; }
unsigned VeriEnumConstraint::IsScalarTypeConstraint() const       { return 1 ; }
unsigned VeriEnumConstraint::IsEnumConstraint() const             { return 1 ; }
unsigned VeriConstraint::IsEnumConstraint() const                 { return 0 ; }

unsigned VeriScalarTypeConstraint::IsIntegerDataType() const
{
    // This is from table 6-8 in Section 6.11 of
    // IEEE P1800 2009.
    unsigned result = 0 ;

    switch (_type) {
    // All net types are 1 bit :
    // Register types :
    case VERI_INTEGER :
    case VERI_TIME :
    // register single bit type :
    case VERI_REG :
    // SV register bit types :
    case VERI_LOGIC :
    case VERI_BIT :
    // LRM 3.3 scalar data types
    case VERI_BYTE :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
        {
            result = 1 ;
            break ;
        }
    default :
        {
            result = 0 ;
            break ;
        }
    }
    return result ;
}

unsigned VeriConstraint::NumOfElements() const                    { return 0 ; }
unsigned VeriScalarTypeConstraint::NumOfElements() const          { return 1 ; }
unsigned VeriEnumConstraint::NumOfElements() const                { return 1 ; }
unsigned VeriRangeConstraint::NumOfElements() const               { return Length() ; }
unsigned VeriArrayConstraint::NumOfElements() const
{
    // VIPER #6573 : Take the original number of elements for packed too
    //if (IsPacked()) return 1 ; // It is a packed array, element count 1
    unsigned dim_count = (_index_constraint) ? _index_constraint->Length(): 0 ;
    return dim_count * ((_element_constraint) ? _element_constraint->NumOfElements(): 0) ;
}
unsigned VeriRecordConstraint::NumOfElements() const
{
    unsigned element_count = 0 ;
    unsigned i ;
    VeriConstraint *element_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
        element_count += (element_constraint) ? element_constraint->NumOfElements(): 0 ;
    }
    return element_count ;
}

// Viper #7783: Need to know the size of the full constraint to check if initial values can be preserved
verific_uint64 VeriConstraint::GetNumBitsForConstraint () const
{
    // return log2(width) : where width is number of bits for the value
    // integer : 32 bit shall return log2(32) = 5
    unsigned top = NumBitsOfRange() ;
    verific_uint64 size = 0 ;
    for (; top!=0; top=top>>1) size++ ;
    if (!size) size++ ;

    return size ;
}

verific_uint64 VeriArrayConstraint::GetNumBitsForConstraint() const
{
    // return log2(width) : where width is number of bits for the value
    // 4 element array of integer returns log2(4*32) = log2(4) + log2(32) = 7
    unsigned top = Length()-1 ; // length-1:0
    verific_uint64 size = 0 ;
    for (; top!=0; top=top>>1) size++ ;
    if (!size) size++ ;

    // Exponents are added for multiplication
    // TODO: It still does for one level.
    // e.g. record of array would still get integer overflow
    verific_uint64 element_size = (_element_constraint) ? _element_constraint->GetNumBitsForConstraint() : 0 ;
    if (element_size == 1) element_size = 0 ; // size count of 1 bits => size
    if (size == 1 && element_size) size = 0 ; // 1 element_size => element_size

    return size + element_size ;
}

unsigned VeriConstraint::NumOfBits() const   { return 0 ; }
unsigned VeriScalarTypeConstraint::NumOfBits() const
{
    // VIPER #6640: Call the merged routine to get the size of the built-in type:
    return (_type) ? VeriNode::GetSizeOfBuiltInType(_type) : 1 ;
}
unsigned VeriEnumConstraint::NumOfBits() const
{
    if (_base) return _base->NumOfBits() ;
    return 32 ; // Size of enum is default size integer : 32 bits.
}
unsigned VeriRangeConstraint::NumOfBits() const { return 0 ; }
unsigned VeriArrayConstraint::NumOfBits() const
{
    // Total number of bits is 'bits required for each element' multiplied by range length
    unsigned index_length = Length() ;
    return index_length * ((_element_constraint) ? _element_constraint->NumOfBits(): 1) ;
}
unsigned VeriRecordConstraint::NumOfBits() const
{
    // For unions, return the number of bits of the union constraint
    if (_full_constraint) return _full_constraint->NumOfBits() ;

    // Calculate number of bits of each element and sum those to get total number :
    unsigned result = 0 ;
    unsigned i ;
    VeriConstraint *element_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
        if (!element_constraint) continue ;
        result += element_constraint->NumOfBits() ;
    }
    return result ;
}

// VIPER #8069: Calculate number of bits in 64 bits:
// Calculating number of bits for large arrays/memories in 32 bits
// leads to math overflow, so for those calculation 64 bits is required.
// NumOfBits64() on constriants are introduced to calculate number of bits in 64 bits.
// NumOfBits64() routines are not used in our flow, it has to be called explicitly
// if number of bits in 64 bits is required.
verific_uint64 VeriConstraint::NumOfBits64() const   { return 0 ; }
verific_uint64 VeriScalarTypeConstraint::NumOfBits64() const
{
    return (_type) ? VeriNode::GetSizeOfBuiltInType(_type) : 1 ;
}
verific_uint64 VeriEnumConstraint::NumOfBits64() const
{
    if (_base) return _base->NumOfBits64() ;
    return 32 ; // Size of enum is default size integer : 32 bits.
}
verific_uint64 VeriRangeConstraint::NumOfBits64() const { return 0 ; }
verific_uint64 VeriArrayConstraint::NumOfBits64() const
{
    // Total number of bits is 'bits required for each element' multiplied by range length
    unsigned index_length = Length() ;
    return index_length * ((_element_constraint) ? _element_constraint->NumOfBits64(): 1) ;
}
verific_uint64 VeriRecordConstraint::NumOfBits64() const
{
    // For unions, return the number of bits of the union constraint
    if (_full_constraint) return _full_constraint->NumOfBits64() ;

    // Calculate number of bits of each element and sum those to get total number :
    verific_uint64 result = 0 ;
    unsigned i ;
    VeriConstraint *element_constraint ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, element_constraint) {
        if (!element_constraint) continue ;
        result += element_constraint->NumOfBits64() ;
    }
    return result ;
}

unsigned VeriConstraint::NumBitsOfRange() const
{
    if (!NumOfBits()) return 0 ;

    unsigned top = NumOfBits()-1 ;

    // VIPER Issue #7384 : Now calculate the number of bits needed for that
    unsigned bits = 0 ;
    for (; top!=0; top=top>>1) bits++ ;

    if (bits==0) bits = 1 ; // Happens for _range 0 to 0

    return bits ;
}

unsigned VeriRangeConstraint::NumBitsOfRange() const
{
    unsigned bits = 0 ;
    int l = Left() ;
    int r = Right() ;
    if (l<0) { l=-l-1 ; bits = 1 ; }
    if (r<0) { r=-r-1 ; bits = 1 ; }
    unsigned top = (unsigned)((l>r) ? l : r) ; // top is max of abs of either index _range bound

    // Now calculate the number of bits needed for that
    for (; top!=0; top=top>>1) bits++ ;

    if (bits==0) bits = 1 ; // Happens for _range 0 to 0

    return bits ;
}

unsigned VeriArrayConstraint::NumBitsOfRange() const
{
    unsigned bits = 0 ;
    int l = Left() ;
    int r = Right() ;
    if (l<0) { l=-l-1 ; bits = 1 ; }
    if (r<0) { r=-r-1 ; bits = 1 ; }
    unsigned top = (unsigned)((l>r) ? l : r) ; // top is max of abs of either index _range bound

    // Now calculate the number of bits needed for that
    for (; top!=0; top=top>>1) bits++ ;

    if (bits==0) bits = 1 ; // Happens for _range 0 to 0

    return bits ;
}

unsigned VeriRecordConstraint::NumBitsOfRange() const
{
    unsigned bits = 0 ;
    int l = Left() ;
    int r = Right() ;
    if (l<0) { l=-l-1 ; bits = 1 ; }
    if (r<0) { r=-r-1 ; bits = 1 ; }
    unsigned top = (unsigned)((l>r) ? l : r) ; // top is max of abs of either index _range bound

    // Now calculate the number of bits needed for that
    for (; top!=0; top=top>>1) bits++ ;

    if (bits==0) bits = 1 ; // Happens for _range 0 to 0

    return bits ;
}

unsigned VeriConstraint::PosOf(int  /*idx*/) const { return 0 ; }
unsigned VeriScalarTypeConstraint::PosOf(int idx) const
{
    // Scalar types are bit vectors with range [size-1 : 0]
    int msb = (int)NumOfBits() - 1 ;
    // Viper 6470: Corrected the operation.
    // VIPER #8240: Check index to be greater than msb/lsb
    int lsb = 0 ;
    if ((msb < idx) || (idx < lsb)) return 0 ;

    return (unsigned)(msb - idx) ;
}
unsigned VeriEnumConstraint::PosOf(int idx) const
{
    return (_base) ? _base->PosOf(idx) : 0 ;
}
unsigned VeriArrayConstraint::PosOf(int idx) const  { return (_index_constraint) ? _index_constraint->PosOf(idx): 0 ; }
unsigned VeriRangeConstraint::PosOf(int idx) const
{
    return (IsTo()) ? (unsigned)(idx - _left) : (unsigned)(_left - idx) ;
}

unsigned VeriConstraint::ElementPosOf(int  /*idx*/) const { return 0 ; }
unsigned VeriScalarTypeConstraint::ElementPosOf(int idx) const { return PosOf(idx) ; }
unsigned VeriEnumConstraint::ElementPosOf(int idx) const { return _base ? _base->ElementPosOf(idx) : 0 ; }
unsigned VeriArrayConstraint::ElementPosOf(int idx) const
{
    // Get position of index in array's range :
    unsigned pos = (_index_constraint) ? _index_constraint->PosOf(idx): 0 ;

    // If 'idx' represent the msb position of range, 'idx' will represent the starting
    // point of first element in array constraint.
    // If 'idx' represents any other position in range, to calculate which element
    // of this array is represented by 'idx', we have to consider the element counts
    // of msb-pos range.
    // If array is defined as
    //  byte  x [3:0] = '{1:5, ...};
    //
    // and we want to know for '1:5' which element of this array should get value 5
    // So element number will be
    // '1 (element count for index 3) + 1 (element count for index 2)' i.e. 2
    unsigned element_pos = 0 ;
    if (pos > 0) {
        // Get the number of elements contains in each element :
        unsigned per_index_element_count = (_element_constraint) ? _element_constraint->NumOfElements(): 0 ;
        unsigned i ;
        for (i = 0; i < pos; i++) {
            element_pos += per_index_element_count ;
        }
    }
    return element_pos ;
}

unsigned VeriConstraint::PosOfMember(const char * /*member*/) const { return 0 ; }
unsigned VeriRecordConstraint::PosOfMember(const char *member) const
{
    return (_element_ids) ? _element_ids->IndexOf(member): 0 ;
}

unsigned VeriConstraint::ElementPosOfMember(const char * /*member*/) const { return 0 ; }
unsigned VeriRecordConstraint::ElementPosOfMember(const char *member) const
{
    unsigned pos = (_element_ids) ? _element_ids->IndexOf(member): 0 ;
    unsigned element_pos = 0 ;
    unsigned i ;
    for (i = 0; i < pos; i++) {
        VeriConstraint *element_constraint = (_element_constraints) ? (VeriConstraint*)_element_constraints->At(i): 0 ;
        if (!element_constraint) continue ;
        element_pos += element_constraint->NumOfElements() ;
    }
    return element_pos ;
}
// Packed dimension count
unsigned VeriScalarTypeConstraint::PackedDimCount() const
{
    return (NumOfBits() > 1) ? 1 : 0 ;
}
unsigned VeriArrayConstraint::PackedDimCount() const
{
    if (!IsPacked()) return 0 ;
    unsigned packed_dim_count = 0 ;
    VeriConstraint *ele_constraint = _element_constraint ;
    VeriConstraint *prev_ele_constraint = ele_constraint ;
    while (ele_constraint) {
        packed_dim_count++ ;
        prev_ele_constraint = ele_constraint ;
        ele_constraint = ele_constraint->ElementConstraint() ;
    }
    packed_dim_count = packed_dim_count + (prev_ele_constraint ? prev_ele_constraint->PackedDimCount() : 0) ;
    return packed_dim_count ;
}
// Equality of two constraints :
unsigned VeriConstraint::IsEqual(VeriConstraint * /*other*/) const { return 0 ; }
unsigned VeriScalarTypeConstraint::IsEqual(VeriConstraint *other) const
{
    if (!other || !other->IsScalarTypeConstraint()) return 0 ; // Not same constraint
    if (_type != other->ScalarType()) return 0 ; // Two are not of same type
    return 1 ; // Same constraint
}
unsigned VeriEnumConstraint::IsEqual(VeriConstraint *other) const
{
    if (!other || !other->IsEnumConstraint()) return 0 ;

    // Check base :
    if (_base && !_base->IsEqual(other->BaseConstraint())) return 0 ;
    Array *other_enums = other->GetEnumeratedIds() ;
    if (!_ids || !other_enums) return 0 ;

    // Check enumerated ids :
    if (_ids->Size() !=  other_enums->Size()) return 0 ;
    unsigned i ;
    VeriIdDef *this_enum, *other_enum ;
    FOREACH_ARRAY_ITEM(_ids, i, this_enum) {
        other_enum = (VeriIdDef*)other_enums->At(i) ;
        if (this_enum != other_enum) return 0 ; // Not equal
    }
    return 1 ;
}
unsigned VeriRangeConstraint::IsEqual(VeriConstraint *other) const
{
    if (!other || !other->IsRangeConstraint()) return 0 ;

    if ((_left != other->Left()) || (_right != other->Right())) return 0 ;
    return 1 ;
}
unsigned VeriArrayConstraint::IsEqual(VeriConstraint *other) const
{
    if (!other || !other->IsArrayConstraint()) return 0 ;

    // Check index constraint :
    if (_index_constraint && !_index_constraint->IsEqual(other->IndexConstraint())) return 0 ;

    // Check element constraint :
    if (_element_constraint && !_element_constraint->IsEqual(other->ElementConstraint())) return 0 ;
    return 1 ;
}
unsigned VeriRecordConstraint::IsEqual(VeriConstraint *other) const
{
    if (!other || !other->IsRecordConstraint()) return 0 ;

    if (_is_packed != other->IsPacked()) return 0 ;
    if (_type != other->OriginatingType()) return 0 ;

    Map *element_ids = other->GetElementIds() ;
    if (!_element_ids || !element_ids) return 0 ;

    // Match every element of record, do only pointer match :
    MapIter mi;
    VeriIdDef *id, *other_id ;
    char *element ;
    FOREACH_MAP_ITEM(_element_ids, mi, &element, &id) {
        other_id = (VeriIdDef*)element_ids->GetValue(element) ;
        if (id != other_id) return 0 ;
    }
    return 1 ;
}
// Matching constraints (VIPER #6573)
unsigned VeriConstraint::IsMatching(VeriConstraint * /*other*/) const { return 0 ; }
unsigned VeriScalarTypeConstraint::IsMatching(VeriConstraint *other) const
{
    if (!other || (!other->IsScalarTypeConstraint() && !other->IsPacked())) return 0 ; // Matches only with packed

    // Every built-in type matches every occurrence of itself
    if ((_type == other->ScalarType()) && other->IsScalarTypeConstraint()) return 1 ; // Matching types

    // Matching if
    // 1. Both are 2 state or 4 state
    // 2. Both are signed or unsigned
    // 3. Both have same width
    if (other->IsRecordConstraint()) return 0 ; // does not match with packed struct/union
    if (IsSigned() != other->IsSigned()) return 0 ; // sign mismatch
    if ((Is2State() && !other->Is2State()) || (Is4State() && !other->Is4State())) return 0 ;  // state mismatch
    if (NumOfBits() != other->NumOfBits()) return 0 ;  // size mismatch

    if (PackedDimCount() != other->PackedDimCount()) return 0 ; // packed dimension count mismatch
    if ((Left() != other->Left()) || (Right() != other->Right())) return 0 ; // left/right bound mismatch
    return 1 ; // Matching types
}
unsigned VeriEnumConstraint::IsMatching(VeriConstraint *other) const
{
    return IsEqual(other) ;
}
unsigned VeriRangeConstraint::IsMatching(VeriConstraint *other) const
{
    if (!other || !other->IsRangeConstraint()) return 0 ;

    if ((_left != other->Left()) || (_right != other->Right())) return 0 ;
    if ((_is_packed && !other->IsPacked()) || (!_is_packed && other->IsPacked())) return 0 ;
    if ((_is_dynamic && !other->IsDynamicRange()) || (!_is_dynamic && other->IsDynamicRange())) return 0 ;
    return 1 ;
}
unsigned VeriArrayConstraint::IsMatching(VeriConstraint *other) const
{
    // Packed array constraint can match with scalar type constraint
    if (!other) return 0 ;
    if (IsPacked() && !other->IsArrayConstraint()) {
        if (!other->IsPacked()) return 0 ; // packed/unpacked mismatch
        if (IsSigned() != other->IsSigned()) return 0 ; // sign mismatch
        if ((Is2State() && !other->Is2State()) || (Is4State() && !other->Is4State())) return 0 ; // state mismatch
        if (NumOfBits() != other->NumOfBits()) return 0 ;  // size mismatch

        if (PackedDimCount() != other->PackedDimCount()) return 0 ; // packed dimension count mismatch
        if ((Left() != other->Left()) || (Right() != other->Right())) return 0 ; // left/right bound mismatch
        return 1 ;
    }
    if (!other->IsArrayConstraint()) return 0 ;

    // Check index constraint :
    if (_index_constraint && !_index_constraint->IsMatching(other->IndexConstraint())) return 0 ;

    // Check element constraint :
    if (_element_constraint && !_element_constraint->IsMatching(other->ElementConstraint())) return 0 ;
    return 1 ;
}
unsigned VeriRecordConstraint::IsMatching(VeriConstraint *other) const
{
    return IsEqual(other) ;
}
unsigned VeriConstraint::IsPacked() const { return 0 ; }
unsigned VeriArrayConstraint::IsPacked() const { return (_index_constraint) ? _index_constraint->IsPacked(): 0 ; }
unsigned VeriConstraint::IsDynamicArrayConstraint() const { return 0 ; }
unsigned VeriScalarTypeConstraint::IsDynamicArrayConstraint() const
{
    return (_type == VERI_STRINGTYPE) ? 1 : 0 ;
}
unsigned VeriArrayConstraint::IsDynamicArrayConstraint() const {  return (_index_constraint) ? _index_constraint->IsDynamicRange(): 0 ; }

unsigned VeriConstraint::SubTypeOf(const VeriConstraint * /*other*/) const  { return 0 ; }
unsigned VeriScalarTypeConstraint::SubTypeOf(const VeriConstraint *other) const
{
    // Scalar constraint can only be subtype of another scalar constraint
    if (!other || !other->IsScalarTypeConstraint()) return 0 ;

    switch (_type) {
    case VERI_REG :
    case VERI_LOGIC :
    case VERI_BIT :
         {
             unsigned other_type = other->ScalarType() ;
             switch(other_type) {
             case VERI_BYTE :
             case VERI_SHORTINT :
             case VERI_INT :
             case VERI_LONGINT :
             case VERI_INTEGER :
             case VERI_TIME :
                 return 1 ; // This is subtype of 'other'
             default : break ;
             }
             break ;
         }
    case VERI_BYTE :
         {
             unsigned other_type = other->ScalarType() ;
             switch(other_type) {
             case VERI_SHORTINT :
             case VERI_INT :
             case VERI_LONGINT :
             case VERI_INTEGER :
             case VERI_TIME :
                 return 1 ; // This is subtype of 'other'
             default : break ;
             }
             break ;
         }
    // FIXME : Not sure about shortreal, real and realtime
    default : break ;
    }
    return 0 ;
}
//////////////// Constraint to dimensions ///////////////////////////
VeriRange *VeriConstraint::CreateDimensions(unsigned /*only_unpacked*/, const VeriTreeNode * /* from*/)
{
    return 0 ;
}
VeriRange *VeriEnumConstraint::CreateDimensions(unsigned only_unpacked, const VeriTreeNode *from)
{
    if (only_unpacked) return 0 ; // enum cannot have unpacked dimensions
    return _base ? _base->CreateDimensions(only_unpacked, from): 0 ;
}
VeriRange *VeriArrayConstraint::CreateDimensions(unsigned only_unpacked, const VeriTreeNode *from)
{
    VeriRange *dims = _index_constraint ? _index_constraint->CreateDimensions(only_unpacked, from) : 0 ;
    VeriRange *next_dim = _element_constraint ? _element_constraint->CreateDimensions(only_unpacked, from) : 0 ;
    if (dims && next_dim) dims->SetNext(next_dim) ;
    return dims ;
}
VeriRange *VeriRangeConstraint::CreateDimensions(unsigned only_unpacked, const VeriTreeNode *from)
{
    if (only_unpacked && _is_packed) return 0 ; // This is packed and unpacked is required only
    VeriExpression *left = new VeriIntVal(_left) ;
    if (from) left->SetLinefile(from->Linefile()) ;

    VeriExpression *right = new VeriIntVal(_right) ;
    if (from) right->SetLinefile(from->Linefile()) ;

    VeriRange *range = new VeriRange(left, right, (!left && !right)? VERI_NONE:0) ;
    if (from) range->SetLinefile(from->Linefile()) ;
    return range ;
}
unsigned VeriConstraint::IsEquivalent(VeriConstraint * /*other*/, VeriTreeNode * /*from*/, unsigned /*ignore_sign_state*/)
{
    return 0 ;
}
unsigned VeriScalarTypeConstraint::IsEquivalent(VeriConstraint *other, VeriTreeNode *from, unsigned ignore_sign_state)
{
    if (!other) return 0 ;
    if (IsMatching(other)) return 1 ; // Matching types are equivalent
    if (!other->IsPacked()) return 0 ; // not equivalent with unpacked type

    if (!ignore_sign_state && (IsSigned() != other->IsSigned())) {
        if (from) from->Error("elements must both be signed or both be unsigned") ;
        return 0 ; // Sign mismatch, not equivalent
    }

    if (!ignore_sign_state && ((Is2State() && !other->Is2State()) || (Is4State() && !other->Is4State()))) {
        if (from) from->Error("elements must both be 2-state or both be 4-state") ;
        return 0 ; // 2 state/4 state mixed, not equivalent
    }

    unsigned this_num_of_bits = NumOfBits() ;
    unsigned other_num_of_bits = other->NumOfBits() ;
    if (this_num_of_bits != other_num_of_bits) {
        if (from) from->Error("array element widths (%d, %d) don't match", this_num_of_bits, other_num_of_bits) ;
        return 0 ; // width mismatch
    }
    return 1 ; // equivalent constraint
}
unsigned VeriEnumConstraint::IsEquivalent(VeriConstraint *other, VeriTreeNode * /*from*/, unsigned /*ignore_sign_state*/)
{
    return IsMatching(other) ;
}
unsigned VeriRangeConstraint::IsEquivalent(VeriConstraint *other, VeriTreeNode *from, unsigned /*ignore_sign_state*/)
{
    if (!other || !other->IsRangeConstraint()) return 0 ;
    if ((_is_packed && !other->IsPacked()) || (!_is_packed && other->IsPacked())) {
        if (from) from->Error("incompatible unpacked dimensions in assignment") ;
        return 0 ;
    }
    if ((_is_dynamic && !other->IsDynamicRange()) || (!_is_dynamic && other->IsDynamicRange())) {
        if (from) from->Error("incompatible complex type assignment") ;
        return 0 ;
    }
    return 1 ;
}
unsigned VeriArrayConstraint::IsEquivalent(VeriConstraint *other, VeriTreeNode *from, unsigned ignore_sign_state)
{
    if (!other) return 0 ;
    if (IsPacked() && !other->IsPacked()) return 0 ; // not equivalent with unpacked type
    if (IsPacked() && other->IsPacked()) {
        if (!ignore_sign_state && (IsSigned() != other->IsSigned())) {
            if (from) from->Error("elements must both be signed or both be unsigned") ;
            return 0 ; // Sign mismatch, not equivalent
        }

        if (!ignore_sign_state && ((Is2State() && !other->Is2State()) || (Is4State() && !other->Is4State()))) {
            if (from) from->Error("elements must both be 2-state or both be 4-state") ;
            return 0 ; // 2 state/4 state mixed, not equivalent
        }

        unsigned this_num_of_bits = NumOfBits() ;
        unsigned other_num_of_bits = other->NumOfBits() ;
        if (this_num_of_bits != other_num_of_bits) {
            if (from) from->Error("array element widths (%d, %d) don't match", this_num_of_bits, other_num_of_bits) ;
            return 0 ; // width mismatch
        }
        return 1 ; // equivalent constraint
    }
    if (!other->IsArrayConstraint()) return 0 ;

    // Elements must be equivalent
    if (_element_constraint && !_element_constraint->IsEquivalent(other->ElementConstraint(), from, ignore_sign_state)) return 0 ;

    if (_index_constraint && _index_constraint->IsEquivalent(other->IndexConstraint(), from, ignore_sign_state)) return 1 ;

    unsigned dim_width = _index_constraint ? _index_constraint->NumOfBits(): 0 ;
    VeriConstraint *other_index = other->IndexConstraint() ;
    unsigned other_dim_width = other_index ? other_index->NumOfBits(): 0 ;
    if (dim_width != other_dim_width) {
        if (from) from->Error("size of the corresponding unpacked dimensions do not match") ;
        return 0 ;
    }
    return 1 ;
}
unsigned VeriRecordConstraint::IsEquivalent(VeriConstraint *other, VeriTreeNode * /*from*/, unsigned /*ignore_sign_state*/)
{
    if (!other) return 0 ;
    if (IsPacked() && other->IsPacked()) return 1 ;
    return IsMatching(other) ;
}
unsigned VeriConstraint::IsAssignmentCompatible(VeriConstraint * /*other*/, VeriTreeNode * /*from*/, VeriTreeNode::veri_type_env /*env*/)
{
    return 0 ;
}
unsigned VeriScalarTypeConstraint::IsAssignmentCompatible(VeriConstraint *other, VeriTreeNode *from, VeriTreeNode::veri_type_env /*env*/)
{
    if (!other) return 0 ;
    if (!other->IsPacked()) {
        if (from) from->Error("cannot assign %s type to %s type", "unpacked", "packed") ;
        return 0 ;
    }
    return 1 ;
}
unsigned VeriEnumConstraint::IsAssignmentCompatible(VeriConstraint *other, VeriTreeNode *from, VeriTreeNode::veri_type_env /*env*/)
{
    if (!other) return 0 ;
    if (other->IsEnumConstraint()) {
        if (!IsMatching(other)) {
            if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
            return 0 ;
        }
    } else if (!other->IsPacked() || !other->IsIntegerDataType()) {
        if (from) from->Error("an enum variable may only be assigned to same enum typed variable or one of its values") ;
        return 0 ;
    }
    return 1 ;
}
unsigned VeriRangeConstraint::IsAssignmentCompatible(VeriConstraint *other, VeriTreeNode *from, VeriTreeNode::veri_type_env /*env*/)
{
    return IsEquivalent(other, from, 0) ;
}
unsigned VeriArrayConstraint::IsAssignmentCompatible(VeriConstraint *other, VeriTreeNode *from, VeriTreeNode::veri_type_env env)
{
    if (!other) return 0 ;
    if (IsPacked() && !other->IsPacked()) {
        if (from) from->Error("cannot assign %s type to %s type", "unpacked", "packed") ;
        return 0 ;
    } else if (!IsPacked() && other->IsPacked()) {
        if (from) from->Error("cannot assign %s type to %s type", "packed", "unpacked") ;
        return 0 ;
    }
    if (IsPacked() && other->IsPacked()) return 1 ; // assignment compatible
    // LRM 7.6 : For unpacked array assignment element types must be equivalent
    VeriConstraint *other_ele = other->ElementConstraint() ;
    // VIPER #7092 : Donot check sign and state mismatch for actual/formal association of instance
    unsigned ignore_sign_state = ((env == VeriTreeNode::INST_READ) || (env == VeriTreeNode::INST_WRITE)) ? 1: 0 ;
    if (_element_constraint && other_ele && !_element_constraint->IsEquivalent(other_ele, from, ignore_sign_state)) return 0 ;

    VeriConstraint *other_index = other->IndexConstraint() ;
    if (_index_constraint && other_index && !_index_constraint->IsDynamicRange() && !other_index->IsDynamicRange()) {
        // Both are fixed size array. Dimension width must match
        unsigned dim_width = _index_constraint->NumOfBits() ;
        unsigned other_dim_width = other_index->NumOfBits() ;
        if (dim_width != other_dim_width) {
            if (from) from->Error("size of the corresponding unpacked dimensions do not match") ;
            return 0 ;
        }
    }
    return 1 ;
}
unsigned VeriRecordConstraint::IsAssignmentCompatible(VeriConstraint *other, VeriTreeNode *from, VeriTreeNode::veri_type_env /*env*/)
{
    if (_type == VERI_INTERFACE || _type == VERI_MODPORT || other->OriginatingType() == VERI_INTERFACE || other->OriginatingType() == VERI_MODPORT) return 1 ; // Do not check
    if (!IsEquivalent(other, from, 0)) {
        if (from) from->Error("incompatible complex type assignment") ;
        return 0 ;
    }
    return 1 ;
}
unsigned VeriRecordConstraint::HasMembersOfEquivalentTypes() const
{
    // Check whether all elements are of equivalent types
    unsigned i ;
    VeriConstraint *ele_constraint, *prev_constraint = 0 ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, ele_constraint) {
        if (!ele_constraint) continue ;
        if (!prev_constraint) {
            prev_constraint = ele_constraint ;
            continue ;
        }
        if (!ele_constraint->IsEquivalent(prev_constraint, 0, 0)) return 0 ;
        prev_constraint = ele_constraint ;
    }
    return 1 ; // All members are of equivalent types
}

void VeriTypeInfo::CheckTemporalExprType(const VeriIdDef *id, const VeriTreeNode *from, unsigned is_in_procedural_concurrent_assertion /*=0*/) const
{
    if (!id || !from) return ; // Cannot produce error without identifier

    // Do not check if type is specified by type parameter or unresolved name type
    if (IsTypeParameterType() || IsUnresolvedNameType()) return ;

    if (IsRealType()) {
        from->Error("illegal reference to %s %s", "real/shortreal/realtime", id->Name()) ;
    } else if (IsString()) { // VIPER #3939
        // VIPER #4769 string variables as parameters are allowed in sequence.
        if (!id->IsParam()) from->Error("illegal reference to %s %s", "string", id->Name()) ;
    } else if (IsEventType()) { // VIPER #3941
        from->Error("illegal reference to %s %s", "event", id->Name()) ;
    } else if (IsChandleType()) { // VIPER #3940
        from->Error("illegal reference to %s %s", "chandle", id->Name()) ;
    } else if (IsClassType()) { // VIPER #3957
        from->Error("illegal reference to %s %s", "class", id->Name()) ;
    } else if (IsAssociativeArrayType()) { // VIPER #3943
        from->Error("illegal reference to %s %s", "associative array", id->Name()) ;
    } else if (IsDynamicArrayType()) { // VIPER #3942
        from->Error("illegal reference to %s %s", "dynamic array", id->Name()) ;
    }

    // Viper 7084: LRM Section 16.15.6.1 IEEE_SV-2009 automatic variables are allowed to be
    // used in procedural concurrent assertions.
    // VIPER #8503: IEEE 1800-2012 section 16.6: Functions should be automatic
    if (!is_in_procedural_concurrent_assertion && id->IsAutomatic() && !id->IsFunction()) {
        from->Error("illegal reference to automatic variable %s", id->Name()) ;
    }
}

VeriDataType *
VeriScalarTypeConstraint::ToDataType(const VeriTreeNode *from) const
{
    VeriDataType *type = new VeriDataType(_type, _sign, 0) ;
    if (from) type->SetLinefile(from->Linefile()) ;
    return type ;
}

VeriDataType *
VeriEnumConstraint::ToDataType(const VeriTreeNode *from) const
{
    VeriDataType *base = (_base) ? _base->ToDataType(from) : 0 ;
#if 0
    Array *ids = (_ids) ? new Array(*_ids) : 0 ; // FIXME: Create a copies of id? What about scope and initial value?
    VeriDataType *type = new VeriEnum(base, ids) ;
    if (from) type->SetLinefile(from->Linefile()) ;
    return type ;
#else
    // FIXME: Only return the basetype for enums for the above issues with ids:
    return base ;
#endif
}

VeriDataType *
VeriRangeConstraint::ToDataType(const VeriTreeNode * /*from*/) const
{
    //VERIFIC_ASSERT(0) ;
    return 0 ; // Should not be used on its own
}

VeriDataType *
VeriArrayConstraint::ToDataType(const VeriTreeNode *from) const
{
    VeriRange *index_range = (_index_constraint) ? _index_constraint->CreateDimensions(0, from) : 0 ;
    VeriDataType *elem_type = (_element_constraint) ? _element_constraint->ToDataType(from) : 0 ;

    if (!index_range) return elem_type ;
    if (!elem_type) {
        delete index_range ;
        return 0 ; // Something wrong
    }

    // CHECKME: No unpacked dimensions remain!
    VeriRange *elem_range = elem_type->TakeDimensions() ;
    index_range->SetNext(elem_range) ;
    elem_type->SetDimensions(index_range) ;

    return elem_type ;
}

VeriDataType *
VeriRecordConstraint::ToDataType(const VeriTreeNode *from) const
{
    if ((_type != VERI_STRUCT) && (_type != VERI_UNION)) return 0 ;

    VeriScope *scope = new VeriScope((VeriScope *)0) ; // CHECKME: unnamed, top scope?
    Array *decls = (_element_constraints) ? new Array(_element_constraints->Size()) : 0 ;
    unsigned i ;
    VeriConstraint *constr ;
    FOREACH_ARRAY_ITEM(_element_constraints, i, constr) {
        MapItem *item = (_element_ids && (_element_ids->Size()>i)) ? _element_ids->GetItemAt(i) : 0 ;
        const char *id_name = (item) ? (const char *)item->Key() :  0 ;
        if (!id_name) continue ;
        VeriIdDef *id = new VeriVariable(Strings::save(id_name)) ;
        if (!scope->Declare(id)) { delete id ; continue ; }
        VeriDataType *id_type = (constr) ? constr->ToDataType(from) : 0 ;
        VeriDataDecl *decl = new VeriDataDecl(VERI_STRUCT /* CHECKME: yacc uses it, why? */, id_type, id) ;
        if (from) decl->SetLinefile(from->Linefile()) ;
        decls->InsertLast(decl) ;
    }
    VeriDataType *type = new VeriStructUnion(_type, ((_is_signed)?VERI_SIGNED:0), 0 /*dimensions*/, decls, scope) ;
    if (from) type->SetLinefile(from->Linefile()) ;
    if (_is_packed) type->SetPacked() ;
    return type ;
}

