/*
 *
 * [ File Version : 1.966 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h>    // For sprintf

#include "Array.h"
#include "Set.h"
#include "Map.h"
#include "Strings.h"

#include "VeriExpression.h"
#include "VeriStatement.h"
#include "VeriMisc.h"
#include "VeriId.h"
#include "VeriScope.h"
#include "VeriConstVal.h"
#include "veri_file.h" // For GetWorkLib and GetModule, also for Viper #4711
#include "VeriCopy.h"
#include "VeriLibrary.h"
#include "VeriModule.h"

#include "VeriBaseValue_Stat.h"
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
#include "../vhdl/VhdlIdDef.h"
#endif

#include "veri_tokens.h" // Tokens (unsigned numbers) are used in parse tree

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VeriExpression::VeriExpression()
    : VeriTreeNode()
{ }
VeriExpression::~VeriExpression()
{
    // VIPER #6906: Attributes and comments are now deleted from VeriTreeNode destructor only:

    // Delete attributes if present
    //DeleteAttributes() ;

    // Delete comments if there
    //VeriNode::DeleteComments(TakeComments()) ;
}

// Base class for all Names (IdRef, IndexedId, HierId).
VeriName::VeriName() : VeriExpression() { }
VeriName::~VeriName() { }

VeriIdRef::VeriIdRef(char *name) :
    VeriName(),
    _name(name), // absorb the string !
    _id(0)
{
#ifdef VERILOG_SHARE_STRINGS
    _name = Strings::CreateConstantString(name) ; // used shared string
    Strings::free(name) ;
#endif
}

VeriIdRef::VeriIdRef(VeriIdDef *id) :
    VeriName(),
    _name(0),
    _id(id)
{
    // Set the _name field also
#ifndef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
    if (id) {
#ifdef VERILOG_SHARE_STRINGS
        _name = Strings::CreateConstantString(id->Name()) ; // used shared string
#else
        _name = Strings::save(id->Name()) ;
#endif
    }
#endif
}

VeriIdRef::~VeriIdRef()
{
    // Don't free '_id'. Its a pointer to an VeriIdDef
    _id = 0 ;
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ;
#endif
    _name = 0 ;
}

VeriIndexedId::VeriIndexedId(char *name, VeriExpression *idx) :
    VeriName(),
    _prefix(0),
    _idx(idx),
    _id(0)
{
    _prefix = new VeriIdRef(name) ;
}
VeriIndexedId::VeriIndexedId(VeriIdDef *id, VeriExpression *idx) :
    VeriName(),
    _prefix(0),
    _idx(idx),
    _id(id)
{
    _prefix = new VeriIdRef(id) ;
}
VeriIndexedId::VeriIndexedId(VeriName *prefix, VeriExpression *idx) :
    VeriName(),
    _prefix(prefix),
    _idx(idx),
    _id(0)
{
}
VeriIndexedId::~VeriIndexedId()
{
    delete _prefix ;
    delete _idx ;
    _id = 0 ;
}

VeriSelectedName::VeriSelectedName(VeriName *prefix, char *suffix) :
    VeriName(),
    _prefix(prefix),
    _suffix(suffix),
    _prefix_id(0),
    _suffix_id(0),
    _function_type(0),
    _suffix_is_token(0),
    _cross_lang_name(0)
{
#ifdef VERILOG_SHARE_STRINGS
    _suffix = Strings::CreateConstantString(suffix) ; // used shared string
    Strings::free(suffix) ;
#endif

    // Find the token for the suffix (in case this is a 'method' on the prefix).
    // methods are only allowed in System Verilog mode :
    if (IsSystemVeri()) {
        _function_type = GetFuncToken(_suffix, this /* VIPER #8138: from */) ;
        // Note: This could return a non-method token (such as a system call name).
        // If so, discard this as a valid method in 'Resolve'.
    }
}

VeriSelectedName::~VeriSelectedName()
{
    delete _prefix ;
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_suffix) ; // We own this string.
#endif
    _suffix = 0 ;
    _prefix_id = 0 ;
    _suffix_id = 0 ;
}
unsigned VeriSelectedName::IsHierName() const
{
    // VIPER #4267 : If both _suffix and _suffix_id is null, this name is
    // modified by static elaborator and so whether it is still hier name
    // or not will be determined by prefix
    if (_prefix && !_suffix) return _prefix->IsHierName() ;
    return 1 ; // normally always hier name
}
VeriIdDef *VeriSelectedName::FullId() const
{
    return GetId() ;
}
VeriIdDef *VeriSelectedName::GetId() const
{
    // If this name contains no _suffix_id and _suffix, we can conclude
    // that this selected name is to be treated as id-ref, so call GetId on prefix
    if (_prefix && !_suffix_id && !_suffix) return _prefix->GetId() ;
    return _suffix_id ;
}
VeriIndexedMemoryId::VeriIndexedMemoryId(char *name, Array *indexes) :
    VeriName(),
    _prefix(0),
    _indexes(indexes),
    _id(0)
{
    _prefix = new VeriIdRef(name) ;
}
VeriIndexedMemoryId::VeriIndexedMemoryId(VeriIdDef *id, Array *indexes) :
    VeriName(),
    _prefix(0),
    _indexes(indexes),
    _id(id)
{
    _prefix = new VeriIdRef(id) ;
}
VeriIndexedMemoryId::VeriIndexedMemoryId(VeriName *prefix, Array *indexes) :
    VeriName(),
    _prefix(prefix),
    _indexes(indexes),
    _id(0)
{
}
VeriIndexedMemoryId::~VeriIndexedMemoryId() {
    delete _prefix ;

    unsigned i ;
    VeriExpression *index ;
    FOREACH_ARRAY_ITEM(_indexes, i, index) delete index ;
    delete _indexes ;
    _id = 0 ;
}
VeriIndexedExpr::VeriIndexedExpr(VeriExpression *prefix, VeriExpression *index) :
    VeriExpression(),
    _prefix(prefix),
    _idx(index)
{
}
VeriIndexedExpr::~VeriIndexedExpr()
{
    delete _prefix ;
    delete _idx ;
}
VeriScopeName::VeriScopeName(VeriName *prefix, char *suffix, Array *param_value_assignment) :
    VeriSelectedName(prefix, suffix),
    _param_values(param_value_assignment),
    _modport_name(0)
{}
VeriScopeName::~VeriScopeName()
{
    unsigned i ;
    VeriExpression *value ;
    FOREACH_ARRAY_ITEM(_param_values, i, value) delete value ;
    delete _param_values ;
    delete _modport_name ;
}
VeriIdDef *VeriScopeName::GetId() const
{
    // If modport identifier exists, return that
    VeriIdDef *modport_id = (_modport_name) ? _modport_name->GetId(): 0 ;
    if (modport_id) return modport_id ;

    return (!_suffix) ? _prefix_id : _suffix_id ;
}     // Get resolved referenced (suffix) identifier if _suffix exists
VeriKeyword::VeriKeyword(unsigned token) :
    VeriIdRef(Strings::save(VeriNode::PrintToken(token))),
    _token(token)
{
}

VeriKeyword::~VeriKeyword()
{
}

VeriConcat::VeriConcat(Array *exprs) :
    VeriExpression(),
    _exprs(exprs)
{
}
VeriConcat::~VeriConcat() {
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) delete expr ;
    delete _exprs ;
}

VeriMultiConcat::VeriMultiConcat(VeriExpression *repeat, Array *exprs) :
    VeriExpression(),
    _repeat(repeat),
    _exprs(exprs)
{
}
VeriMultiConcat::~VeriMultiConcat() {
    delete _repeat ;
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) delete expr ;
    delete _exprs ;
}

VeriFunctionCall::VeriFunctionCall(VeriName *func_name, Array *args) :
    VeriName(),
    _func_name(func_name),
    _args(args),
    _function_type(0)
{
    if (args && _args && args->Size()==1 && !args->GetFirst()) (void) _args->RemoveLast() ;
    // Find the token for the suffix (in case this is a 'method' on the prefix).
    // methods are only allowed in System Verilog mode :
    if (IsSystemVeri()) {
        _function_type = GetFuncToken(_func_name ? _func_name->GetName():0, _func_name /* VIPER #8138: from */) ;
        // Note: This could return a non-method token (such as a system call name).
        // If so, discard this as a valid method in 'Resolve'.
    }
    if (!IsSystemVeri()) {
        // VIPER #6395 : Produce error for empty argumnent
        unsigned i ;
        VeriExpression *arg ;
        FOREACH_ARRAY_ITEM(args, i, arg) {
            arg = arg ? arg->GetConnection(): 0 ;
            if (!arg || arg->IsOpen()) Error("empty arguments are allowed only in system verilog") ;
        }
    }
}
VeriFunctionCall::~VeriFunctionCall() {
    // Delete arguments
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) delete arg ;
    delete _args ;
    // Delete function name reference
    delete _func_name ;
}

VeriSystemFunctionCall::VeriSystemFunctionCall(char *name, Array *args) :
    VeriExpression(),
    _name(name),
    _args(args),
    _sva_clock_expression(0),
    _function_type(0)
{
    // VIPER #2641: Check if it is actually a system task, error out in that case:
    if (IsSystemTask(_name)) Warning("expected a system function, not system task $%s", _name) ;

    // Find the VERI token for this system call.
    _function_type = GetFuncToken(_name) ;
    // We could mistakenly get a token that is not a system call (like a SV method, or a system task).
    // FIX ME : Should discard such tokens in Resolve(), once we know the context.
}
VeriSystemFunctionCall::~VeriSystemFunctionCall()
{
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) delete arg ;
    delete _args ;
    Strings::free(_name) ;
    _name = 0 ;
    // This expression is not owned by this node. Make it zero
    _sva_clock_expression = 0 ;
}

VeriMinTypMaxExpr::VeriMinTypMaxExpr(VeriExpression *min_expr, VeriExpression *typ_expr, VeriExpression *max_expr) :
    VeriExpression(),
    _min_expr(min_expr),
    _typ_expr(typ_expr),
    _max_expr(max_expr)
{
}
VeriMinTypMaxExpr::~VeriMinTypMaxExpr() {
    delete _min_expr ;
    delete _typ_expr ;
    delete _max_expr ;
}

VeriUnaryOperator::VeriUnaryOperator(unsigned oper_type, VeriExpression *arg) :
    VeriExpression(),
    _oper_type(oper_type),
    _parenthesized(0),
    _arg(arg)
{
}
VeriUnaryOperator::~VeriUnaryOperator() {
    delete _arg ;
}

VeriBinaryOperator::VeriBinaryOperator(unsigned oper_type, VeriExpression *left, VeriExpression *right) :
    VeriExpression(),
    _oper_type(oper_type),
    _parenthesized(0),
    _left(left),
    _right(right)
{
}
VeriBinaryOperator::~VeriBinaryOperator() {
    delete _left ;
    delete _right ;
}

VeriQuestionColon::VeriQuestionColon(VeriExpression *if_expr, VeriExpression *then_expr, VeriExpression *else_expr) :
    VeriExpression(),
    _if_expr(if_expr),
    _then_expr(then_expr),
    _else_expr(else_expr)
{
}
VeriQuestionColon::~VeriQuestionColon()
{
    delete _if_expr ;
    delete _then_expr ;
    delete _else_expr ;
}

VeriEventExpression::VeriEventExpression(unsigned edge, VeriExpression *expr, VeriExpression *iff_condition) :
    VeriExpression(),
    _edge(edge),
    _expr(expr),
    _iff_condition(iff_condition),
    _event_with_at(0)
{
    if (_edge && _expr && (_expr->GetFunctionType()==VERI_SYS_CALL_GLOBAL_CLOCK)) {
        // VIPER #7396: Do not allow $global_clock to be used with another edge.
        // $global_clock itself should have an associated edge with it.
        _expr->Error("syntax error near %s", "$global_clock") ;
    }
}
VeriEventExpression::~VeriEventExpression()
{
    delete _expr ;
    delete _iff_condition ;
}

VeriPortConnect::VeriPortConnect(char *port_ref_name, VeriExpression *connection) :
    VeriExpression(),
    _port_ref_name(port_ref_name),
    _connection(connection)
{
#ifdef VERILOG_SHARE_STRINGS
    _port_ref_name = Strings::CreateConstantString(port_ref_name) ; // used shared string
    Strings::free(port_ref_name) ;
#endif
}
VeriPortConnect::~VeriPortConnect() {
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_port_ref_name) ;
#endif
    _port_ref_name = 0 ;
    delete _connection ;
}

VeriPortOpen::VeriPortOpen() :
    VeriExpression() {}
VeriPortOpen::~VeriPortOpen() {}

VeriAnsiPortDecl::VeriAnsiPortDecl(unsigned dir, VeriDataType *data_type, Array *ids) :
    VeriExpression(),
    _dir(dir),
   _is_local(0),
    _data_type(data_type),
    _ids(ids)
   ,_qualifier(0)
{
    // VIPER #6423: There may not be any id for function/task prototypes
    // Set the port direction, type and signed
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;

        // Set direction on the identifier :
        // Cannot set default direction, because (in SV) it is inherited from previous ansi port decl.
        // So, need to wait until resolving time for default setting.
        if (_dir) id->SetDir(_dir) ;

        // VIPER #2890: Set the ANSI port flag on this identifier:
        id->SetIsAnsiPort() ;

        // Set the data type (if there) back pointer :
        if (_data_type) id->SetDataType(_data_type) ;
    }
}

// constructor with a single id
VeriAnsiPortDecl::VeriAnsiPortDecl(unsigned dir, VeriDataType *data_type, VeriIdDef *id) :
    VeriExpression(),
    _dir(dir),
   _is_local(0),
    _data_type(data_type),
    _ids(0)
   ,_qualifier(0)
{
    // Constructor with a single 'id'.
    // FIX ME : Later, provide better storage than the costly Array.
    _ids = new Array() ;

    // VIPER #6423: There may not be any id for function/task prototypes
    if (!id) return ; // something bad happened

    _ids->InsertLast(id) ;

    // Set direction on the identifier :
    // Cannot set default direction, because (in SV) it is inherited from previous ansi port decl.
    // So, need to wait until resolving time for default setting.
    if (_dir) id->SetDir(_dir) ;

    // VIPER #2890: Set the ANSI port flag on this identifier:
    id->SetIsAnsiPort() ;

    // Set the data type (if there) back-pointer :
    if (_data_type) id->SetDataType(_data_type) ;
}
VeriAnsiPortDecl::~VeriAnsiPortDecl() {
    delete _data_type ;
    // Identifiers themselves are part of the scope. Don't delete them. Just delete the array.
    delete _ids ;
}

void
VeriAnsiPortDecl::AddPortDecl(VeriIdDef *id)
{
    // Add one more port to the ansi-port decl list.
    if (!id || !_ids) return ;
    _ids->InsertLast(id) ;

    // Set direction on the identifier :
    // Cannot set default direction, because (in SV) it is inherited from previous ansi port decl.
    // So, need to wait until resolving time for default setting.
    if (_dir) id->SetDir(_dir) ;

    // VIPER #2890: Set the ANSI port flag on this identifier:
    id->SetIsAnsiPort() ;

    // Set the data type (if there) back pointer :
    if (_data_type) id->SetDataType(_data_type) ;

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Now, adjust the line-column info of this data declaration to include this id:
    linefile_type decl_lf = Linefile() ;
    linefile_type id_lf = id->Linefile() ;
    VeriExpression *init_val = id->GetInitialValue() ;
    if (init_val && init_val->Linefile()) {
        // VIPER #7020: Take the linefile of the initial value, if there:
        id_lf = init_val->Linefile() ;
    } else {
        // VIPER #7020: Otherwise, take the linefile of the unpacked dimensions, if there:
        VeriRange *unpacked_dims = id->GetDimensions() ;
        // Take the last one in the range:
        while (unpacked_dims && unpacked_dims->GetNext()) unpacked_dims = unpacked_dims->GetNext() ;
        if (unpacked_dims && unpacked_dims->Linefile()) id_lf = unpacked_dims->Linefile() ;
    }
    if (decl_lf && id_lf) {
        // Set the ending line-column same as that of the id:
        decl_lf->SetRightLine(id_lf->GetRightLine()) ;
        decl_lf->SetRightCol(id_lf->GetRightCol()) ;
    }
#endif
}

VeriTimingCheckEvent::VeriTimingCheckEvent(VeriExpression *terminal_desc, unsigned edge /* = 0 */, VeriExpression *condition /* = 0 */, char *edge_descriptors /* = 0 */) :
    VeriExpression(),
    _edge(edge),
    _edge_desc_str(edge_descriptors),
    _terminal_desc(terminal_desc),
    _condition(condition)
{
    if (_edge_desc_str && (_edge != VERI_EDGE)) terminal_desc->Error("edge descriptors are invalid here") ;
}

VeriTimingCheckEvent::~VeriTimingCheckEvent()
{
    Strings::free(_edge_desc_str) ;
    _edge_desc_str = 0 ;
    delete _terminal_desc ;
    delete _condition ;
}

VeriRange::VeriRange(VeriExpression *left, VeriExpression *right, unsigned part_select_token, VeriRange *next) :
    VeriExpression(),
    _left(left),
    _right(right),
    _part_select_token(part_select_token),
    _next(next)
{
    if (VeriNode::IsVeri95() &&
        ((_part_select_token==VERI_PARTSELECT_UP) ||
         (_part_select_token==VERI_PARTSELECT_DOWN))) {
        // VIPER #6691: +: or -: is not allowed in Verilog 95 mode:
        Warning("fixed width part select is not allowed in verilog 95 mode") ;
    }
}

VeriRange::~VeriRange()
{
    delete _next ; // delete a multi-dim range back-to-front..
    delete _left ;
    delete _right ;
}

// Late parse-tree additions : Add a range to the LSB side of 'this' :
void VeriRange::SetNext(VeriRange *next)
{
    // Anticipate : add this range at the 'end' of 'this' set of ranges, even though
    // currently (bison) construction rules don't use this (_next will always be 0 when this is called from bison).
    if (_next) {
        _next->SetNext(next) ;
        return ;
    }
    // Set the field as owned by this :
    _next = next ;
}
// Set bounds and next null without deleting those: Bounds are used else where
void VeriRange::SetFieldsNull()
{
    _left = 0 ;
    _right = 0 ;
    _next = 0 ;
}
// VIPER #8003:
VeriExpression* VeriRange::TakeLeft()
{
    VeriExpression *left = _left ;
    _left = 0 ;
    return left ;
}
VeriDelayOrEventControl::VeriDelayOrEventControl(VeriExpression *delay_control, VeriExpression *repeat_event, Array *event_control) :
    VeriExpression(),
    _delay_control(delay_control),
    _repeat_event(repeat_event),
    _event_control(event_control)
    ,_is_cycle_delay(0)
{
    // VIPER 2766 : @* not allowed in '95 mode :
    if (IsVeri95() && _event_control && _event_control->Size()==0) {
        Error("%s not allowed in this dialect. Use v2k mode","@*") ;
    }
}
VeriDelayOrEventControl::~VeriDelayOrEventControl()
{
    delete _delay_control ;
    delete _repeat_event ;
    VeriExpression *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_event_control,i,expr) delete expr ;
    delete _event_control ;
}
unsigned VeriDelayOrEventControl::GetEdgeToken() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->GetEdgeToken(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
VeriExpression *VeriDelayOrEventControl::GetExpr() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->GetExpr(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
VeriExpression *VeriDelayOrEventControl::GetIffCondition() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->GetIffCondition(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
VeriIdDef *VeriDelayOrEventControl::FullId() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->FullId(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
const char *VeriDelayOrEventControl::GetName() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->GetName(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
VeriName *VeriDelayOrEventControl::GetPrefix() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->GetPrefix(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
unsigned VeriDelayOrEventControl::IsHierName() const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->IsHierName(): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}

// Data Type (expression). Essentially a reference to a type, with (packed) dimensions and sign info.
VeriDataType::VeriDataType(unsigned type, unsigned signing, VeriRange *dimensions) :
    VeriExpression(),
    _type(type),
    _signing(signing),
    _is_virtual_interface(0),
    _is_processing(0),
    _dimensions(dimensions)
{
    if (!IsSystemVeri() && (_signing == VERI_UNSIGNED)) {
        // VIPER #3366: Non System Verilog mode does not allow 'unsigned' keyword, produce warning:
        // "unsigned" is introduced in Verilog 2001 as a 'reserved word' but it has no use there.
        // Verilog 95, Verilog AMS 2.2 and Verilog PSL 1.1 does not have special meaning for unsigned
        Warning("keyword '%s' is not allowed here in this mode of verilog", PrintToken(VERI_UNSIGNED)) ;
    }

    // VIPER #7976: Moved to VeriDataType::Resolve() routine:
    //if (dimensions && (!dimensions->GetLeft() && !dimensions->GetRight())) Error("packed dimension must specify a range") ; // Viper #4885
}

// Late parse-tree additions :
void
VeriDataType::SetDimensions(VeriRange *dimensions)
{
    if (!dimensions) return ; // nothing to do.
    VERIFIC_ASSERT(!_dimensions) ; // This member should not have been here already.
    // Set the field as owned by this :
    _dimensions = dimensions ;
}
VeriRange *
VeriDataType::TakeDimensions()
{
    VeriRange *dimensions = _dimensions ;
    _dimensions = 0 ;
    return dimensions ;
}

VeriDataType::~VeriDataType()
{
    delete _dimensions ;
}
unsigned VeriDataType::IsVirtualInterface() const
{
    unsigned ret_val = 0 ;
    ret_val = _is_virtual_interface ;
    return ret_val ;
}
void VeriDataType::SetIsVirtualInterface()
{
    _is_virtual_interface = 1 ;
}

void VeriDataType::SetDecls(Array * /*decls*/) { }
Array *VeriDataType::TakeDecls() { return 0 ; }

VeriNetDataType::VeriNetDataType(unsigned nettype, VeriDataType *data_type) :
    VeriDataType(nettype /* store nettype in base*/, 0, 0),
    _data_type(data_type)
{
    // If data type is not specified, data type of net is logic
    //if (!_data_type) _data_type = new VeriDataType(VERI_LOGIC, 0, 0) ;

    // SV 2012: 6.7.1: Check for generic interconnect
    if (_data_type && (_type == VERI_INTERCONNECT))  Error("generic interconnect must not specify any data type") ;
}
VeriNetDataType::~VeriNetDataType()
{
    delete _data_type ;
}
VeriPathPulseVal::VeriPathPulseVal(VeriExpression *reject_limit, VeriExpression *error_limit) :
    VeriExpression(),
    _reject_limit(reject_limit),
    _error_limit(error_limit)
{}
VeriPathPulseVal::~VeriPathPulseVal()
{
    delete _reject_limit ;
    delete _error_limit ;
}

// System Verilog additions

VeriTypeRef::VeriTypeRef(char *name, unsigned signing, VeriRange *dimensions) :
    VeriDataType(0/*base type not defined yet*/, signing, dimensions),
    _type_name(0),
    _virtual_token(0),
    _id(0)
{
    _type_name = new VeriIdRef(name) ; // unresolved reference (absorbs 'name').
}

VeriTypeRef::VeriTypeRef(VeriIdDef *id, unsigned signing, VeriRange *dimensions) :
    VeriDataType(0/*base type not defined yet*/, signing, dimensions),
    _type_name(0),
    _virtual_token(0),
    _id(id)
{
    _type_name = new VeriIdRef(id) ;     // resolved reference
    if (id) _id = id ; // VIPER #3115 : Set _id back pointer if it is already resolved
}

VeriTypeRef::VeriTypeRef(VeriName *type_name, unsigned signing, VeriRange *dimensions) :
    VeriDataType(0/*base type not defined yet*/, signing, dimensions),
    _type_name(type_name),
    _virtual_token(0),
    _id(0)
{
    if (_type_name && _type_name->GetId()) _id = _type_name->GetId() ; // VIPER #3115 : Set _id back pointer if it is already resolved
}
VeriTypeRef::VeriTypeRef(unsigned virtual_keyword, VeriName *type_name) :
    VeriDataType(0/*base type not defined yet*/, 0, 0),
    _type_name(type_name),
    _virtual_token(virtual_keyword),
    _id(0)
{ if (virtual_keyword) SetIsVirtualInterface() ; }

VeriTypeRef::~VeriTypeRef()
{
    delete _type_name ;
    // _id is a (resolved) 'reference' pointer. Id is owned by scope. Don't delete it here.
    _id = 0 ;
}
//Viper 6439: Virtual functions are written to get typename and
// class declaration from the datatype. This was written to get
// the correct typename and class item for datatypes which are
// written as typedef.
VeriName* VeriTypeRef::GetClassName()
{
    VeriModuleItem *item = _id ? _id->GetModuleItem() : 0 ;
    if (item && item->IsTypeAlias()) {
        VeriDataType *data_type = item->GetDataType() ;
        if (data_type) return data_type->GetClassName() ;
    }
    return GetTypeName() ;
}
VeriName* VeriDataType::GetClassName()
{
    return GetTypeName() ;
}
VeriModuleItem* VeriTypeRef::GetClassItem()
{
    VeriModuleItem *item = _id ? _id->GetModuleItem() : 0 ;
    if (item && item->IsTypeAlias()) {
        VeriDataType *data_type = item->GetDataType() ;
        if (data_type) return data_type->GetClassItem() ;
    }
    return item ;
}
VeriModuleItem* VeriDataType::GetClassItem()
{
    VeriIdDef *type_id = GetId() ;
    return type_id ? type_id->GetModuleItem() : 0 ;
}

unsigned VeriTypeRef::IsVirtualInterface() const
{
    if (_virtual_token) return 1 ;
    if ( _is_virtual_interface) return 1 ;
    if (!_id) return 0 ;
    if (_id->IsProcessing()) return 0 ;
    _id->SetIsProcessing() ;
    VeriDataType *data_type = _id ? _id->GetDataType(): 0 ;
    unsigned is_virtual_interface = data_type ? data_type->IsVirtualInterface() : 0 ;
    _id->ClearIsProcessing() ;
    return is_virtual_interface ;
}
void VeriTypeRef::SetVirtualToken()
{
    _virtual_token = VERI_VIRTUAL ;
    SetIsVirtualInterface() ;
}

VeriStructUnion::VeriStructUnion(unsigned type, unsigned signing, VeriRange *dimensions, Array *decls, VeriScope *scope) :
    VeriDataType(type,signing,dimensions),
    _decls(decls),
    _scope(scope),
    _ids(0),
    _is_packed(0),
    _is_tagged(0),
    // These are for internal use
    _contain_typeparam_type_ele(0),
    _ele_types(0),
    _ele_ids(0)
{
    VERIFIC_ASSERT(type==VERI_STRUCT || type==VERI_UNION) ; // gotta be one of the two.

    // Viper 6980 :
    if (_scope) _scope->SetStructUnionScope() ;

    // Set parent pointers on the members to this.

    // Create the 'ids' array : array of the struct/union member IdDef's,
    // in order of appearance (MSB->LSB) :
    _ids = new Array() ;

    VeriModuleItem *item ;
    Array *item_ids ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decls, i, item) {
        if (!item) continue ;
        item_ids = item->GetIds() ;
        _ids->Append(item_ids) ;

        // Also flag all ids as a 'member' of a struct/union.
        unsigned j ;
        VeriIdDef *member ;
        FOREACH_ARRAY_ITEM(item_ids, j, member) {
            if (!member) continue ;
            member->SetIsMember() ;
        }
    }
}

void VeriStructUnion::SetDecls(Array *decls)
{
    if (!decls) return ;
    VERIFIC_ASSERT(!_decls) ;
    _decls = decls ;
}

Array *VeriStructUnion::TakeDecls()
{
    Array *decls = _decls ;
    _decls = 0 ;
    return decls ;
}

VeriStructUnion::~VeriStructUnion()
{
    // Delete the data declarations
    unsigned i ;
    VeriDataDecl *data_decl ;
    FOREACH_ARRAY_ITEM(_decls, i, data_decl) delete data_decl ;
    delete _decls ;

    delete _ids ; // the array of member identifiers.

    MapIter mi ;
    VeriTypeInfo *ele_type ;
    FOREACH_MAP_ITEM(_ele_types, mi, 0, &ele_type) delete ele_type ;
    delete _ele_types ;

    delete _ele_ids ;
    // Delete the local scope
    delete _scope ;
}

VeriEnum::VeriEnum(unsigned type, unsigned signing, VeriRange *dimensions, Array *ids, VeriRange *pkd_dim) :
    VeriDataType(0, 0, pkd_dim),
    _base_type(0),
    _ids(ids),
    _enum_ids(0)
{
    _base_type = (type || signing || dimensions) ? new VeriDataType(type, signing, dimensions): 0 ;
    // Create a Map for enumerated literals
    _enum_ids = new Map(STRING_HASH, _ids ? _ids->Size(): 1) ;
    // Set the datatype of each enum id back to 'this' datatype.
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        id->SetDataType(this) ;
        (void) _enum_ids->Insert(id->Name(), id) ;
    }
}

// VIPER 2336 : New constructor to support user defined type as base type of enumeration
VeriEnum::VeriEnum(VeriDataType *base_type, Array *ids, VeriRange *pkd_dim) :
    VeriDataType(0, 0, pkd_dim),
    _base_type(base_type),
    _ids(ids),
    _enum_ids(0)
{
    // Create a Map for enumerated literals
    _enum_ids = new Map(STRING_HASH, _ids ? _ids->Size(): 1) ;
    // Set the datatype of each enum id back to 'this' datatype.
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        id->SetDataType(this) ;
        (void) _enum_ids->Insert(id->Name(), id) ;
    }
}

VeriEnum::~VeriEnum()
{
    // Clear the data type of the ids which point back to 'this' enum:
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        // Only clear the data type that points back to me:
        id->ClearMatchedDataType(this) ;
    }

    delete _base_type ; // Delete base type
    // Don't delete the ids. They are owned by the scope they are in. Just delete the array
    delete _ids ;

    delete _enum_ids ;
}

// Dotstar : nothing to do in construction/destruction..
VeriDotStar::VeriDotStar() : VeriExpression(), _dot_star_scope(0) {}
VeriDotStar::~VeriDotStar() { _dot_star_scope = 0 ; }

// IfOper : inherit everything from its base class VeriQuestionColon
VeriIfOperator::VeriIfOperator(VeriExpression *if_expr, VeriExpression *then_expr, VeriExpression *else_expr) :
    VeriQuestionColon(if_expr, then_expr, else_expr)
{
}
VeriIfOperator::~VeriIfOperator()
{
}

VeriCaseOperator::VeriCaseOperator(VeriExpression *cond, Array *case_items) :
    VeriExpression(), _cond(cond), _case_items(case_items)
{
}
VeriCaseOperator::~VeriCaseOperator()
{
    delete _cond ;
    unsigned i ;
    VeriCaseOperatorItem *ci ;
    FOREACH_ARRAY_ITEM(_case_items, i, ci) delete ci ;
    delete _case_items ;
}

void VeriCaseOperator::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    //ValidateDeferredAssertStmt(environment) ;

    if (_cond) _cond->ResolveOnehot(scope, VERI_EXPRESSION_UNDECL_USE_GOOD) ; // resolve as RHS, to allow onehot expressions.

    // Pass resolved case condition into the case items resolving
    unsigned i ;
    VeriCaseOperatorItem *ci ;
    unsigned default_count = 0 ; // Contains the number of 'default' case
    FOREACH_ARRAY_ITEM(_case_items, i, ci) {
        if (!ci) continue ;
        // Resolve case item :
        ci->Resolve(scope, _cond) ;

        // VIPER 2884: Error out if the 'default' case item appears more than once
        if (ci->IsDefault()) default_count++ ; // Increase default count for default case item
        if (default_count > 1) ci->Error("default case should appear only once") ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

VeriSequenceConcat::VeriSequenceConcat(VeriExpression *left, VeriExpression *cycle_delay_range, VeriExpression *right) :
    VeriBinaryOperator(VERI_CYCLE_DELAY_OP, left, right),
    _cycle_delay_range(cycle_delay_range)
{
}

VeriSequenceConcat::~VeriSequenceConcat()
{
    delete _cycle_delay_range ;
}

VeriClockedSequence::VeriClockedSequence(VeriExpression *event_expr, VeriExpression *sequence) :
    VeriUnaryOperator(VERI_AT, sequence),
    _event_expr(event_expr)
{
}

VeriClockedSequence::~VeriClockedSequence()
{
    delete _event_expr ;
}

VeriAssignInSequence::VeriAssignInSequence(VeriExpression *expr, Array *stmts) :
    VeriUnaryOperator(VERI_COMMA, expr),
    _stmts(stmts)
{
}

VeriAssignInSequence::~VeriAssignInSequence()
{
    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) delete stmt ;
    delete _stmts ;
}

VeriDistOperator::VeriDistOperator(VeriExpression *expr, Array *dist_list) :
    VeriUnaryOperator(VERI_DIST, expr),
    _dist_list(dist_list)
{
}

VeriDistOperator::~VeriDistOperator()
{
    unsigned i ;
    VeriExpression *dist_item ;
    FOREACH_ARRAY_ITEM(_dist_list, i, dist_item) delete dist_item ;
    delete _dist_list ;
}

VeriSolveBefore::VeriSolveBefore(Array *solve_list, Array *before_list) :
VeriExpression(),
_solve_list(solve_list),
_before_list(before_list)
{
}

VeriSolveBefore::~VeriSolveBefore()
{
    // Delete all members :
    unsigned i ;
    VeriExpression *name ;
    FOREACH_ARRAY_ITEM(_solve_list, i, name) delete name ;
    delete _solve_list ;
    FOREACH_ARRAY_ITEM(_before_list, i, name) delete name ;
    delete _before_list ;
}

VeriCast::VeriCast(VeriExpression *target_type, VeriExpression *expr) :
    VeriExpression(),
    _target_type(target_type),
    _expr(expr)
{
}

VeriCast::~VeriCast()
{
    delete _target_type ;
    delete _expr ;
}

VeriNew::VeriNew(VeriExpression *size_expr, Array *args) :
    VeriExpression(),
    _size_expr(size_expr),
    _args(args),
    _copy_constructor(0)
{
}

VeriNew::~VeriNew()
{
    delete _size_expr ;

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_args,i,expr) delete expr ;
    delete _args ;
}

VeriConcatItem::VeriConcatItem(VeriExpression *member_label, VeriExpression *expr) :
    VeriExpression(),
    _member_label(member_label),
    _expr(expr)
{
}

VeriConcatItem::~VeriConcatItem()
{
    delete _member_label ;
    delete _expr ;
}

VeriNull::VeriNull() :
    VeriExpression()
{
}

VeriNull::~VeriNull()
{
}

VeriDollar::VeriDollar() :  VeriExpression() {}

VeriDollar::~VeriDollar()  {}

VeriAssignmentPattern::VeriAssignmentPattern(VeriExpression *type, Array *exprs) :
    VeriConcat(exprs),
    _type(type)
{
    unsigned concat_item = 0 ;
    unsigned i ;
    VeriExpression *expr ;
    // VIPER #5586: Initialize 'concat_item' with the fist concat element:
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        concat_item = expr->IsConcatItem() ;
        break ;
    }
    // VIPER #5586: Check whether all concat elements are of same type or not:
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        if ((concat_item && !expr->IsConcatItem()) || (!concat_item && expr->IsConcatItem())) {
            // Found one different positional/other type item, error out:
            expr->Error("positional notation should not be mixed with other type of notation") ;
            break ;
        }
    }
}

VeriAssignmentPattern::~VeriAssignmentPattern() {
    delete _type ;
}

VeriMultiAssignmentPattern::VeriMultiAssignmentPattern(VeriExpression *type, VeriExpression *repeat, Array *exprs) :
    VeriMultiConcat(repeat, exprs),
    _type(type)
{}
VeriMultiAssignmentPattern::~VeriMultiAssignmentPattern() {
    delete _type ;
}

VeriForeachOperator::VeriForeachOperator(VeriName *array_id, Array *loop_indexs, VeriExpression *constraint_set, VeriScope *scope) :
    VeriExpression(),
    _array_name(array_id),
    _loop_indexes(loop_indexs),
    _constraint_set(constraint_set),
    _scope(scope),
    _implicit_decl_list(0)
{}
VeriForeachOperator::~VeriForeachOperator() {
    delete _array_name ; // Delete array name

    // Don't delete loop index variables, only delete array
    delete _loop_indexes ;
    delete _constraint_set ; // Delete constraint set
    unsigned i ;
    VeriDataDecl *decl ;
    FOREACH_ARRAY_ITEM(_implicit_decl_list, i, decl) delete decl ;
    delete _implicit_decl_list ;
    delete _scope ; // Delete scope
}
VeriStreamingConcat::VeriStreamingConcat(unsigned stream_operator, VeriExpression *slice_size, Array *stream_exprs) :
    VeriConcat(stream_exprs),
    _stream_operator(stream_operator),
    _slice_size(slice_size)
{}
VeriStreamingConcat::~VeriStreamingConcat()
{
    delete _slice_size ;
}
VeriCondPredicate::VeriCondPredicate(VeriExpression *left, VeriExpression *right) :
    VeriExpression(),
    _left(left),
    _right(right)
{}
VeriCondPredicate::~VeriCondPredicate()
{
    delete _left ;
    delete _right ;
}
VeriScope * VeriCondPredicate::GetScope() const
{
    // Condpredicate represents '&&&' separated pattern matching expression
    // It can be
    // | <expr> matches  <pattern>
    // | <cond_predicate> "&&&" <expr>
    // | <cond_predicate> "&&&" <expr> matches <pattern>
    // Every pattern introduces a scope, this scope extends to the remaining
    // clauses in the predicate and to the corresponding true arm of if-statement
    // So, If right expression contains a scope, return that otherwise return
    // the scope of left expression
    VeriScope *scope = _right ? _right->GetScope() : 0 ;
    if (!scope) scope = _left ? _left->GetScope() : 0 ;
    return scope ;
}
VeriDotName::VeriDotName(VeriName *name) :
    VeriExpression(),
    _name(name)
{}
VeriDotName::~VeriDotName()
{
    // Delete the name
    delete _name ;
}
VeriTaggedUnion::VeriTaggedUnion(VeriName *member_identifier, VeriExpression *expr) :
    VeriExpression(),
    _member(member_identifier),
    _expr(expr)
{}
VeriTaggedUnion::~VeriTaggedUnion()
{
    delete _member ; // Delete union member name
    delete _expr ; // Delete its value
}
VeriWith::VeriWith(VeriExpression *left) :
    VeriExpression(),
    _left(left),
    _scope(0)
{}
VeriWith::~VeriWith()
{
    delete _left ; // Delete left hand side of 'with' keyword
    delete _scope ;
}
VeriWithExpr::VeriWithExpr(VeriExpression *left, VeriExpression *right) :
    VeriWith(left),
    _right(right)
{}
VeriWithExpr::~VeriWithExpr()
{
    delete _right ; // Delete right hand side of 'with' keyword
}
VeriInlineConstraint::VeriInlineConstraint(VeriExpression *left, Array *constraint_exprs) :
    VeriWith(left),
    _id_refs(0),
    _constraint_block(constraint_exprs)
{}
VeriInlineConstraint::VeriInlineConstraint(VeriExpression *left, Array *id_refs, Array *constraint_exprs) :
    VeriWith(left),
    _id_refs(id_refs),
    _constraint_block(constraint_exprs)
{
    CheckIdentifierList() ; // VIPER #6756: Check for legal/valid id-ref list
}
VeriInlineConstraint::~VeriInlineConstraint()
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_constraint_block, i, expr) delete expr ; // Delete constraint expressions
    delete _constraint_block ;
    FOREACH_ARRAY_ITEM(_id_refs, i, expr) delete expr ;
    delete _id_refs ;
}
void VeriInlineConstraint::SetConstraintBlock(Array *blk)
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_constraint_block, i, expr) delete expr ;
    delete _constraint_block ; // Memory leak fix
    _constraint_block = blk ;
}
void VeriInlineConstraint::SetIdentifierList(Array *refs) // VIPER #6756
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_id_refs, i, expr) delete expr ;
    delete _id_refs ;
    _id_refs = refs ;

    CheckIdentifierList() ; // VIPER #6756: Check for legal/valid id-ref list
}
void VeriInlineConstraint::CheckIdentifierList() const
{
    // VIPER #6756: This list must of all simple id-refs:
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_id_refs, i, expr) {
        if (!expr || expr->IsIdRef()) continue ;
        // This is not a simple id-ref, error out:
        expr->Error("syntax error near %s", (expr->GetName())?expr->GetName():((i)?",":"(")) ;
    }
}

VeriBinValue::VeriBinValue(VeriExpression *iff_expr) :
    VeriExpression(),
    _iff_expr(iff_expr)
{}
VeriBinValue::~VeriBinValue()
{
    delete _iff_expr ;
}
VeriOpenRangeBinValue::VeriOpenRangeBinValue(Array *open_range_list, VeriExpression *iff_expr) :
    VeriBinValue(iff_expr),
    _open_range_list(open_range_list)
{}
VeriOpenRangeBinValue::~VeriOpenRangeBinValue()
{
    // Delete open range list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM (_open_range_list, i, expr) delete expr ;
    delete _open_range_list ;
}
VeriTransBinValue::VeriTransBinValue (Array *trans_list, VeriExpression *iff_expr) :
    VeriBinValue(iff_expr),
    _trans_list(trans_list)
{}
VeriTransBinValue::~VeriTransBinValue ()
{
    // Delete list of trans sets
    unsigned i ;
    VeriTransSet *trans_set ;
    FOREACH_ARRAY_ITEM (_trans_list, i, trans_set) delete trans_set ;
    delete _trans_list ;
}
VeriDefaultBinValue::VeriDefaultBinValue (unsigned sequence, VeriExpression *iff_expr) :
    VeriBinValue(iff_expr),
    _sequence(sequence)
{}
VeriDefaultBinValue::~VeriDefaultBinValue ()
{}
VeriSelectBinValue::VeriSelectBinValue (VeriExpression *select_expr, VeriExpression *iff_expr) :
    VeriBinValue(iff_expr),
    _select_expr(select_expr)
{}
VeriSelectBinValue::~VeriSelectBinValue ()
{
    delete _select_expr ;
}
VeriTransSet::VeriTransSet(Array *array_of_trans_range_list) :
   VeriExpression(),
   _trans_range_list(array_of_trans_range_list)
{}
VeriTransSet::~VeriTransSet()
{
    // Delete list of trans sets
    unsigned i ;
    VeriTransRangeList *range_list ;
    FOREACH_ARRAY_ITEM (_trans_range_list, i, range_list) delete range_list ;
    delete _trans_range_list ;
}
VeriTransRangeList::VeriTransRangeList(Array *trans_item, VeriExpression *repetition) :
    VeriExpression(),
    _trans_item(trans_item),
    _repetition(repetition)
{}
VeriTransRangeList::~VeriTransRangeList()
{
    // Delete range list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM (_trans_item, i, expr) delete expr ;
    delete _trans_item ;

    delete _repetition ;
}
VeriSelectCondition::VeriSelectCondition(VeriExpression *bins_expr, Array *open_range_list) :
    VeriExpression(),
    _bins_expr(bins_expr),
    _range_list(open_range_list)
{}
VeriSelectCondition::~VeriSelectCondition()
{
    delete _bins_expr ;

    // Delete open range list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM (_range_list, i, expr) delete expr ;
    delete _range_list ;
}
VeriTypeOperator::VeriTypeOperator(VeriExpression *expression) :
    VeriDataType(0,0,0),
    _expr_or_data_type(expression)
{}
VeriTypeOperator::~VeriTypeOperator()
{
    delete _expr_or_data_type ;
}

VeriPatternMatch::VeriPatternMatch(VeriExpression *expr, VeriExpression *pattern, VeriScope *pattern_scope) :
    VeriExpression(),
    _left(expr),
    _right(pattern),
    _scope(pattern_scope)
{}
VeriPatternMatch::~VeriPatternMatch()
{
    delete _left ; // Delete expression
    delete _right ; // Delete right pattern
    delete _scope ; // Delete scope
}
VeriConstraintSet::VeriConstraintSet(Array *exprs) :
    VeriExpression(),
    _exprs(exprs)
{}
VeriConstraintSet::~VeriConstraintSet()
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) delete expr ;
    delete _exprs ;
}

//////////////////////////////////////////////////////////////////////
// Obtain a token from a Verilog system call or method name :
//////////////////////////////////////////////////////////////////////

Map *VeriExpression::_name_to_token = 0 ;

/* static */
unsigned
VeriExpression::GetFuncToken(const char *func_name, const VeriExpression *from /*=0*/)
{
    if (!func_name) return 0 ;

    if (!_name_to_token) {
        _name_to_token = new Map(STRING_HASH, 347) ; // case-sensitive hash table
        // Add system calls that we need to use :
//        _name_to_token->Insert("signed",(void*)VERI_SIGNED) ; // replaced by VERI_SYS_CALL_SIGNED
//        _name_to_token->Insert("unsigned",(void*)VERI_UNSIGNED) ; // replaced by VERI_SYS_CALL_UNSIGNED
// Display tasks
        _name_to_token->Insert("display",(void*)VERI_SYS_CALL_DISPLAY) ;
        _name_to_token->Insert("displayb",(void*)VERI_SYS_CALL_DISPLAYB) ;
        _name_to_token->Insert("displayh",(void*)VERI_SYS_CALL_DISPLAYH) ;
        _name_to_token->Insert("displayo",(void*)VERI_SYS_CALL_DISPLAYO) ;
        _name_to_token->Insert("monitor",(void*)VERI_SYS_CALL_MONITOR) ;
        _name_to_token->Insert("monitorb",(void*)VERI_SYS_CALL_MONITORB) ;
        _name_to_token->Insert("monitorh",(void*)VERI_SYS_CALL_MONITORH) ;
        _name_to_token->Insert("monitoro",(void*)VERI_SYS_CALL_MONITORO) ;
        _name_to_token->Insert("monitoroff",(void*)VERI_SYS_CALL_MONITOROFF) ;
        _name_to_token->Insert("strobe",(void*)VERI_SYS_CALL_STROBE) ;
        _name_to_token->Insert("strobeb",(void*)VERI_SYS_CALL_STROBEB) ;
        _name_to_token->Insert("strobeh",(void*)VERI_SYS_CALL_STROBEH) ;
        _name_to_token->Insert("strobeo",(void*)VERI_SYS_CALL_STROBEO) ;
        _name_to_token->Insert("write",(void*)VERI_SYS_CALL_WRITE) ;
        _name_to_token->Insert("writeb",(void*)VERI_SYS_CALL_WRITEB) ;
        _name_to_token->Insert("writeh",(void*)VERI_SYS_CALL_WRITEH) ;
        _name_to_token->Insert("writeo",(void*)VERI_SYS_CALL_WRITEO) ;
        _name_to_token->Insert("monitoron",(void*)VERI_SYS_CALL_MONITORON) ;
// VCD system tasks
        _name_to_token->Insert("dumpvars",(void*)VERI_SYS_CALL_DUMPVARS) ; // Syscall added as per LRM IEEE Std P1364-2000.
        _name_to_token->Insert("dumpall",(void*)VERI_SYS_CALL_DUMPALL) ;
        _name_to_token->Insert("dumpon",(void*)VERI_SYS_CALL_DUMPON) ;
        _name_to_token->Insert("dumpoff",(void*)VERI_SYS_CALL_DUMPOFF) ;
        _name_to_token->Insert("dumpfile",(void*)VERI_SYS_CALL_DUMPFILE) ;
        _name_to_token->Insert("dumplimit",(void*)VERI_SYS_CALL_DUMPLIMIT) ;
        _name_to_token->Insert("dumpflush",(void*)VERI_SYS_CALL_DUMPFLUSH) ;
        _name_to_token->Insert("dumpports",(void*)VERI_SYS_CALL_DUMPPORTS) ;
        _name_to_token->Insert("dumpportson",(void*)VERI_SYS_CALL_DUMPPORTSON) ;
        _name_to_token->Insert("dumpportsoff",(void*)VERI_SYS_CALL_DUMPPORTSOFF) ;
        _name_to_token->Insert("dumpportsall",(void*)VERI_SYS_CALL_DUMPPORTSALL) ;
        _name_to_token->Insert("dumpportslimit",(void*)VERI_SYS_CALL_DUMPPORTSLIMIT) ;
        _name_to_token->Insert("dumpportsflush",(void*)VERI_SYS_CALL_DUMPPORTSFLUSH) ;
// File I/O
        _name_to_token->Insert("fclose",(void*)VERI_SYS_CALL_FCLOSE) ;
        _name_to_token->Insert("fdisplay",(void*)VERI_SYS_CALL_FDISPLAY) ;
        _name_to_token->Insert("fdisplayb",(void*)VERI_SYS_CALL_FDISPLAYB) ;
        _name_to_token->Insert("fdisplayh",(void*)VERI_SYS_CALL_FDISPLAYH) ;
        _name_to_token->Insert("fdisplayo",(void*)VERI_SYS_CALL_FDISPLAYO) ;
        _name_to_token->Insert("fgetc",(void*)VERI_SYS_CALL_FGETC) ;
        _name_to_token->Insert("fgets",(void*)VERI_SYS_CALL_FGETS) ;
        _name_to_token->Insert("fmonitor",(void*)VERI_SYS_CALL_FMONITOR) ;
        _name_to_token->Insert("fmonitorb",(void*)VERI_SYS_CALL_FMONITORB) ;
        _name_to_token->Insert("fmonitorh",(void*)VERI_SYS_CALL_FMONITORH) ;
        _name_to_token->Insert("fmonitoro",(void*)VERI_SYS_CALL_FMONITORO) ;
        _name_to_token->Insert("readmemb",(void*)VERI_SYS_CALL_READMEMB) ;
        _name_to_token->Insert("swrite",(void*)VERI_SYS_CALL_SWRITE) ;
        _name_to_token->Insert("swriteo",(void*)VERI_SYS_CALL_SWRITEO) ;
        _name_to_token->Insert("sformat",(void*)VERI_SYS_CALL_SFORMAT) ;
        _name_to_token->Insert("fscanf",(void*)VERI_SYS_CALL_FSCANF) ;
        _name_to_token->Insert("fread",(void*)VERI_SYS_CALL_FREAD) ;
        _name_to_token->Insert("fseek",(void*)VERI_SYS_CALL_FSEEK) ;
        _name_to_token->Insert("fopen",(void*)VERI_SYS_CALL_FOPEN) ;
        _name_to_token->Insert("fstrobe",(void*)VERI_SYS_CALL_FSTROBE) ;
        _name_to_token->Insert("fstrobeb",(void*)VERI_SYS_CALL_FSTROBEB) ;
        _name_to_token->Insert("fstrobeh",(void*)VERI_SYS_CALL_FSTROBEH) ;
        _name_to_token->Insert("fstrobeo",(void*)VERI_SYS_CALL_FSTROBEO) ;
        _name_to_token->Insert("ungetc",(void*)VERI_SYS_CALL_UNGETC) ;
        _name_to_token->Insert("ferror",(void*)VERI_SYS_CALL_FERROR) ;
        _name_to_token->Insert("rewind",(void*)VERI_SYS_CALL_REWIND) ;
        _name_to_token->Insert("fwrite",(void*)VERI_SYS_CALL_FWRITE) ;
        _name_to_token->Insert("fwriteb",(void*)VERI_SYS_CALL_FWRITEB) ;
        _name_to_token->Insert("fwriteh",(void*)VERI_SYS_CALL_FWRITEH) ;
        _name_to_token->Insert("fwriteo",(void*)VERI_SYS_CALL_FWRITEO) ;
        _name_to_token->Insert("readmemh",(void*)VERI_SYS_CALL_READMEMH) ;
        _name_to_token->Insert("swriteb",(void*)VERI_SYS_CALL_SWRITEB) ;
        _name_to_token->Insert("swriteh",(void*)VERI_SYS_CALL_SWRITEH) ;
        _name_to_token->Insert("sdf_annotate",(void*)VERI_SYS_CALL_SDF_ANNOTATE) ;
        _name_to_token->Insert("sscanf",(void*)VERI_SYS_CALL_SSCANF) ;
        _name_to_token->Insert("ftell",(void*)VERI_SYS_CALL_FTELL) ;
// Timescale tasks
        _name_to_token->Insert("printtimescale",(void*)VERI_SYS_CALL_PRINTTIMESCALE) ;
        _name_to_token->Insert("timeformat",(void*)VERI_SYS_CALL_TIMEFORMAT) ;
// Simulation control tasks
        _name_to_token->Insert("finish",(void*)VERI_SYS_CALL_FINISH) ;
        _name_to_token->Insert("stop",(void*)VERI_SYS_CALL_STOP) ;
// PLA modeling tasks
        _name_to_token->Insert("async$and$array",(void*)VERI_SYS_CALL_ASYNC__AND__ARRAY) ;
        _name_to_token->Insert("async$nand$array",(void*)VERI_SYS_CALL_ASYNC__NAND__ARRAY) ;
        _name_to_token->Insert("async$or$array",(void*)VERI_SYS_CALL_ASYNC__OR__ARRAY) ;
        _name_to_token->Insert("async$nor$array",(void*)VERI_SYS_CALL_ASYNC__NOR__ARRAY) ;
        _name_to_token->Insert("sync$and$array",(void*)VERI_SYS_CALL_SYNC__AND__ARRAY) ;
        _name_to_token->Insert("sync$nand$array",(void*)VERI_SYS_CALL_SYNC__NAND__ARRAY) ;
        _name_to_token->Insert("sync$or$array",(void*)VERI_SYS_CALL_SYNC__OR__ARRAY) ;
        _name_to_token->Insert("sync$nor$array",(void*)VERI_SYS_CALL_SYNC__NOR__ARRAY) ;
        _name_to_token->Insert("async$and$plane",(void*)VERI_SYS_CALL_ASYNC__AND__PLANE) ;
        _name_to_token->Insert("async$nand$plane",(void*)VERI_SYS_CALL_ASYNC__NAND__PLANE) ;
        _name_to_token->Insert("async$or$plane",(void*)VERI_SYS_CALL_ASYNC__OR__PLANE) ;
        _name_to_token->Insert("async$nor$plane",(void*)VERI_SYS_CALL_ASYNC__NOR__PLANE) ;
        _name_to_token->Insert("sync$and$plane",(void*)VERI_SYS_CALL_SYNC__AND__PLANE) ;
        _name_to_token->Insert("sync$nand$plane",(void*)VERI_SYS_CALL_SYNC__NAND__PLANE) ;
        _name_to_token->Insert("sync$or$plane",(void*)VERI_SYS_CALL_SYNC__OR__PLANE) ;
        _name_to_token->Insert("sync$nor$plane",(void*)VERI_SYS_CALL_SYNC__NOR__PLANE) ;
// Stochastic Analysis tasks
        _name_to_token->Insert("q_initialize",(void*)VERI_SYS_CALL_Q_INITIALIZE) ;
        _name_to_token->Insert("q_remove",(void*)VERI_SYS_CALL_Q_REMOVE) ;
        _name_to_token->Insert("q_exam",(void*)VERI_SYS_CALL_Q_EXAM) ;
        _name_to_token->Insert("q_add",(void*)VERI_SYS_CALL_Q_ADD) ;
        _name_to_token->Insert("q_full",(void*)VERI_SYS_CALL_Q_FULL) ;
// Simulation time functions
        _name_to_token->Insert("realtime",(void*)VERI_SYS_CALL_REALTIME) ;
        _name_to_token->Insert("time",(void*)VERI_SYS_CALL_TIME) ;
        _name_to_token->Insert("stime",(void*)VERI_SYS_CALL_STIME) ;
// Conversion functions
        _name_to_token->Insert("bitstoreal",(void*)VERI_SYS_CALL_BITSTOREAL) ;
        _name_to_token->Insert("itor",(void*)VERI_SYS_CALL_ITOR) ;
        _name_to_token->Insert("signed",(void*)VERI_SYS_CALL_SIGNED) ;
        _name_to_token->Insert("realtobits",(void*)VERI_SYS_CALL_REALTOBITS) ;
        _name_to_token->Insert("rtoi",(void*)VERI_SYS_CALL_RTOI) ;
        _name_to_token->Insert("unsigned",(void*)VERI_SYS_CALL_UNSIGNED) ;
// Probabilistic distribution functions
        _name_to_token->Insert("dist_chi_square",(void*)VERI_SYS_CALL_DIST_CHI_SQUARE) ;
        _name_to_token->Insert("dist_exponential",(void*)VERI_SYS_CALL_DIST_EXPONENTIAL) ;
        _name_to_token->Insert("dist_poisson",(void*)VERI_SYS_CALL_DIST_POISSON) ;
        _name_to_token->Insert("dist_uniform",(void*)VERI_SYS_CALL_DIST_UNIFORM) ;
        _name_to_token->Insert("dist_erlang",(void*)VERI_SYS_CALL_DIST_ERLANG) ;
        _name_to_token->Insert("dist_normal",(void*)VERI_SYS_CALL_DIST_NORMAL) ;
        _name_to_token->Insert("dist_t",(void*)VERI_SYS_CALL_DIST_T) ;
        _name_to_token->Insert("random",(void*)VERI_SYS_CALL_RANDOM) ;
// Command line input
        _name_to_token->Insert("test$plusargs",(void*)VERI_SYS_CALL_TEST__PLUSARGS) ;
        _name_to_token->Insert("value$plusargs",(void*)VERI_SYS_CALL_VALUE__PLUSARGS) ;
// LRM Verilog 2000. Section 15 timing checks
        _name_to_token->Insert("setup",(void*)VERI_SYS_CALL_SETUP) ;
        _name_to_token->Insert("hold",(void*)VERI_SYS_CALL_HOLD) ;
        _name_to_token->Insert("setuphold",(void*)VERI_SYS_CALL_SETUPHOLD) ;
        _name_to_token->Insert("recovery",(void*)VERI_SYS_CALL_RECOVERY) ;
        _name_to_token->Insert("removal",(void*)VERI_SYS_CALL_REMOVAL) ;
        _name_to_token->Insert("recrem",(void*)VERI_SYS_CALL_RECREM) ;
        _name_to_token->Insert("skew",(void*)VERI_SYS_CALL_SKEW) ;
        _name_to_token->Insert("timeskew",(void*)VERI_SYS_CALL_TIMESKEW) ;
        _name_to_token->Insert("fullskew",(void*)VERI_SYS_CALL_FULLSKEW) ;
        _name_to_token->Insert("period",(void*)VERI_SYS_CALL_PERIOD) ;
        _name_to_token->Insert("width",(void*)VERI_SYS_CALL_WIDTH) ;
        _name_to_token->Insert("nochange",(void*)VERI_SYS_CALL_NOCHANGE) ;

//
// System Verilog (3.1) system calls
//
        _name_to_token->Insert("assertkill",(void*)VERI_SYS_CALL_ASSERTKILL) ;
        _name_to_token->Insert("assertoff",(void*)VERI_SYS_CALL_ASSERTOFF) ;
        _name_to_token->Insert("asserton",(void*)VERI_SYS_CALL_ASSERTON) ;
        _name_to_token->Insert("bits",(void*)VERI_SYS_CALL_BITS) ;
        _name_to_token->Insert("bitstoshortreal",(void*)VERI_SYS_CALL_BITSTOSHORTREAL) ;
        _name_to_token->Insert("cast",(void*)VERI_SYS_CALL_CAST) ;
        _name_to_token->Insert("countones",(void*)VERI_SYS_CALL_COUNTONES) ;
        _name_to_token->Insert("dimensions",(void*)VERI_SYS_CALL_DIMENSIONS) ;
        _name_to_token->Insert("error",(void*)VERI_SYS_CALL_ERROR) ;
        _name_to_token->Insert("exit",(void*)VERI_SYS_CALL_EXIT) ;
        _name_to_token->Insert("fatal",(void*)VERI_SYS_CALL_FATAL) ;
        _name_to_token->Insert("fell",(void*)VERI_SYS_CALL_FELL) ;
        _name_to_token->Insert("high",(void*)VERI_SYS_CALL_HIGH) ;
        _name_to_token->Insert("increment",(void*)VERI_SYS_CALL_INCREMENT) ;
        _name_to_token->Insert("info",(void*)VERI_SYS_CALL_INFO) ;
        _name_to_token->Insert("inset",(void*)VERI_SYS_CALL_INSET) ;
        _name_to_token->Insert("insetz",(void*)VERI_SYS_CALL_INSETZ) ;
        _name_to_token->Insert("isunknown",(void*)VERI_SYS_CALL_ISUNKNOWN) ;
        _name_to_token->Insert("left",(void*)VERI_SYS_CALL_LEFT) ;
        _name_to_token->Insert("length",(void*)VERI_SYS_CALL_LENGTH) ;
        _name_to_token->Insert("low",(void*)VERI_SYS_CALL_LOW) ;
        _name_to_token->Insert("onehot",(void*)VERI_SYS_CALL_ONEHOT) ;
        _name_to_token->Insert("past",(void*)VERI_SYS_CALL_PAST) ;
        _name_to_token->Insert("right",(void*)VERI_SYS_CALL_RIGHT) ;
        _name_to_token->Insert("root",(void*)VERI_SYS_CALL_ROOT) ;
        _name_to_token->Insert("rose",(void*)VERI_SYS_CALL_ROSE) ;
        _name_to_token->Insert("shortrealtobits",(void*)VERI_SYS_CALL_SHORTREALTOBITS) ;
        _name_to_token->Insert("srandom",(void*)VERI_SYS_CALL_SRANDOM) ;
        _name_to_token->Insert("stable",(void*)VERI_SYS_CALL_STABLE) ;
        _name_to_token->Insert("urandom",(void*)VERI_SYS_CALL_URANDOM) ;
        _name_to_token->Insert("urandom_range",(void*)VERI_SYS_CALL_URANDOM_RANGE) ;
        _name_to_token->Insert("warning",(void*)VERI_SYS_CALL_WARNING) ;
// System Verilog 3.1a additions :
        _name_to_token->Insert("onehot0",(void*)VERI_SYS_CALL_ONEHOT0) ;

        _name_to_token->Insert("coverage_control",(void*)VERI_SYS_CALL_COVERAGE_CONTROL) ;
        _name_to_token->Insert("coverage_merge",(void*)VERI_SYS_CALL_COVERAGE_MERGE) ;
        _name_to_token->Insert("coverage_save",(void*)VERI_SYS_CALL_COVERAGE_SAVE) ;
        _name_to_token->Insert("get_coverage",(void*)VERI_SYS_CALL_GET_COVERAGE) ;
        _name_to_token->Insert("isunbounded",(void*)VERI_SYS_CALL_ISUNBOUNDED) ;
        _name_to_token->Insert("load_coverage_db",(void*)VERI_SYS_CALL_LOAD_COVERAGE_DB) ;
        _name_to_token->Insert("sampled",(void*)VERI_SYS_CALL_SAMPLED) ;
        _name_to_token->Insert("set_coverage_db_name",(void*)VERI_SYS_CALL_SET_COVERAGE_DB_NAME) ;
        _name_to_token->Insert("size",(void*)VERI_SYS_CALL_SIZE) ;
        _name_to_token->Insert("typename",(void*)VERI_SYS_CALL_TYPENAME) ;
        _name_to_token->Insert("typeof",(void*)VERI_SYS_CALL_TYPEOF) ;
        _name_to_token->Insert("unit",(void*)VERI_SYS_CALL_UNIT) ;
        _name_to_token->Insert("writememb",(void*)VERI_SYS_CALL_WRITEMEMB) ;
        _name_to_token->Insert("writememh",(void*)VERI_SYS_CALL_WRITEMEMH) ;

// System Verilog (3.1) methods ;
        _name_to_token->Insert("atobin",(void*)VERI_METHOD_ATOBIN) ;
        _name_to_token->Insert("atohex",(void*)VERI_METHOD_ATOHEX) ;
        _name_to_token->Insert("atoi",(void*)VERI_METHOD_ATOI) ;
        _name_to_token->Insert("atooct",(void*)VERI_METHOD_ATOOCT) ;
        _name_to_token->Insert("atoreal",(void*)VERI_METHOD_ATOREAL) ;
        _name_to_token->Insert("back",(void*)VERI_METHOD_BACK) ;
        _name_to_token->Insert("bintoa",(void*)VERI_METHOD_BINTOA) ;
        _name_to_token->Insert("clear",(void*)VERI_METHOD_CLEAR) ;
        _name_to_token->Insert("compare",(void*)VERI_METHOD_COMPARE) ;
        _name_to_token->Insert("constraint_mode",(void*)VERI_METHOD_CONSTRAINT_MODE) ;
        _name_to_token->Insert("data",(void*)VERI_METHOD_DATA) ;
        _name_to_token->Insert("delete",(void*)VERI_METHOD_DELETE) ;
        _name_to_token->Insert("empty",(void*)VERI_METHOD_EMPTY) ;
        _name_to_token->Insert("eq",(void*)VERI_METHOD_EQ) ;
        _name_to_token->Insert("erase",(void*)VERI_METHOD_ERASE) ;
        _name_to_token->Insert("erase_range",(void*)VERI_METHOD_ERASE_RANGE) ;
        _name_to_token->Insert("exists",(void*)VERI_METHOD_EXISTS) ;
        _name_to_token->Insert("first",(void*)VERI_METHOD_FIRST) ;
        _name_to_token->Insert("front",(void*)VERI_METHOD_FRONT) ;
        _name_to_token->Insert("getc",(void*)VERI_METHOD_GETC) ;
        _name_to_token->Insert("hextoa",(void*)VERI_METHOD_HEXTOA) ;
        _name_to_token->Insert("icompare",(void*)VERI_METHOD_ICOMPARE) ;
        _name_to_token->Insert("insert",(void*)VERI_METHOD_INSERT) ;
        _name_to_token->Insert("insert_range",(void*)VERI_METHOD_INSERT_RANGE) ;
        _name_to_token->Insert("itoa",(void*)VERI_METHOD_ITOA) ;
        _name_to_token->Insert("last",(void*)VERI_METHOD_LAST) ;
        _name_to_token->Insert("len",(void*)VERI_METHOD_LEN) ;
        _name_to_token->Insert("name",(void*)VERI_METHOD_NAME) ;
        _name_to_token->Insert("neq",(void*)VERI_METHOD_NEQ) ;
        _name_to_token->Insert("next",(void*)VERI_METHOD_NEXT) ;
        _name_to_token->Insert("num",(void*)VERI_METHOD_NUM) ;
        _name_to_token->Insert("octtoa",(void*)VERI_METHOD_OCTTOA) ;
        _name_to_token->Insert("pop_back",(void*)VERI_METHOD_POP_BACK) ;
        _name_to_token->Insert("pop_front",(void*)VERI_METHOD_POP_FRONT) ;
        _name_to_token->Insert("post_randomize",(void*)VERI_METHOD_POST_RANDOMIZE) ;
        _name_to_token->Insert("pre_randomize",(void*)VERI_METHOD_PRE_RANDOMIZE) ;
        _name_to_token->Insert("prev",(void*)VERI_METHOD_PREV) ;
        _name_to_token->Insert("purge",(void*)VERI_METHOD_PURGE) ;
        _name_to_token->Insert("push_back",(void*)VERI_METHOD_PUSH_BACK) ;
        _name_to_token->Insert("push_front",(void*)VERI_METHOD_PUSH_FRONT) ;
        _name_to_token->Insert("putc",(void*)VERI_METHOD_PUTC) ;
        _name_to_token->Insert("rand_mode",(void*)VERI_METHOD_RAND_MODE) ;
        _name_to_token->Insert("randomize",(void*)VERI_METHOD_RANDOMIZE) ;
        _name_to_token->Insert("realtoa",(void*)VERI_METHOD_REALTOA) ;
        _name_to_token->Insert("set",(void*)VERI_METHOD_SET) ;
//        _name_to_token->Insert("size",(void*)VERI_METHOD_SIZE) ; // VERI_SYS_CALL_SIZE will be returned
        _name_to_token->Insert("start",(void*)VERI_METHOD_START) ;
        _name_to_token->Insert("substr",(void*)VERI_METHOD_SUBSTR) ;
        _name_to_token->Insert("swap",(void*)VERI_METHOD_SWAP) ;
        _name_to_token->Insert("tolower",(void*)VERI_METHOD_TOLOWER) ;
        _name_to_token->Insert("toupper",(void*)VERI_METHOD_TOUPPER) ;

// System Verilog 3.1a method additions :
        _name_to_token->Insert("and",(void*)VERI_METHOD_AND) ;
        _name_to_token->Insert("await",(void*)VERI_METHOD_AWAIT) ;
        _name_to_token->Insert("find_first",(void*)VERI_METHOD_FIND_FIRST) ;
        _name_to_token->Insert("find_first_index",(void*)VERI_METHOD_FIND_FIRST_INDEX) ;
        _name_to_token->Insert("find_index",(void*)VERI_METHOD_FIND_INDEX) ;
        _name_to_token->Insert("find_last",(void*)VERI_METHOD_FIND_LAST) ;
        _name_to_token->Insert("find_last_index",(void*)VERI_METHOD_FIND_LAST_INDEX) ;
//        _name_to_token->Insert("finish",(void*)VERI_METHOD_FINISH) ; // VERI_SYS_CALL_FINISH will be returned
        _name_to_token->Insert("find",(void*)VERI_METHOD_FIND) ;
        _name_to_token->Insert("get_randstate",(void*)VERI_METHOD_GET_RANDSTATE) ;
        _name_to_token->Insert("kill",(void*)VERI_METHOD_KILL) ;
        _name_to_token->Insert("max",(void*)VERI_METHOD_MAX) ;
        _name_to_token->Insert("min",(void*)VERI_METHOD_MIN) ;
        _name_to_token->Insert("new",(void*)VERI_METHOD_NEW) ;
        _name_to_token->Insert("or",(void*)VERI_METHOD_OR) ;
        _name_to_token->Insert("process",(void*)VERI_METHOD_PROCESS) ;
        _name_to_token->Insert("product",(void*)VERI_METHOD_PRODUCT) ;
        _name_to_token->Insert("resume",(void*)VERI_METHOD_RESUME) ;
        _name_to_token->Insert("reverse",(void*)VERI_METHOD_REVERSE) ;
        _name_to_token->Insert("rsort",(void*)VERI_METHOD_RSORT) ;
        _name_to_token->Insert("self",(void*)VERI_METHOD_SELF) ;
        _name_to_token->Insert("set_randstate",(void*)VERI_METHOD_SET_RANDSTATE) ;
        _name_to_token->Insert("shuffle",(void*)VERI_METHOD_SHUFFLE) ;
        _name_to_token->Insert("sie",(void*)VERI_METHOD_SIE) ;
        _name_to_token->Insert("sort",(void*)VERI_METHOD_SORT) ;
        _name_to_token->Insert("status",(void*)VERI_METHOD_STATUS) ;
        _name_to_token->Insert("sum",(void*)VERI_METHOD_SUM) ;
        _name_to_token->Insert("suspend",(void*)VERI_METHOD_SUSPEND) ;
        _name_to_token->Insert("unique",(void*)VERI_METHOD_UNIQUE) ;
        _name_to_token->Insert("unique_index",(void*)VERI_METHOD_UNIQUE_INDEX) ;
        _name_to_token->Insert("xor",(void*)VERI_METHOD_XOR) ;
        _name_to_token->Insert("ended",(void*)VERI_METHOD_ENDED) ; // adhoc LRM method
        _name_to_token->Insert("matched",(void*)VERI_METHOD_MATCHED) ; // adhoc LRM method
// System Verilog Std 1800 method additions :
        _name_to_token->Insert("unpacked_dimensions",(void*)VERI_SYS_CALL_UNPACKED_DIMENSIONS) ;
        _name_to_token->Insert("coverage_get_max",(void*)VERI_SYS_CALL_COVERAGE_GET_MAX) ;
        _name_to_token->Insert("coverage_get",(void*)VERI_SYS_CALL_COVERAGE_GET) ;
        _name_to_token->Insert("index",(void*)VERI_METHOD_INDEX) ;
//        _name_to_token->Insert("get_coverage",(void*)VERI_METHOD_GET_COVERAGE) ; // VERI_SYS_CALL_GET_COVERAGE will be returned
        _name_to_token->Insert("get_inst_coverage",(void*)VERI_METHOD_GET_INST_COVERAGE) ;
        _name_to_token->Insert("set_inst_name",(void*)VERI_METHOD_SET_INST_NAME) ;
//        _name_to_token->Insert("stop",(void*)VERI_METHOD_STOP) ; // VERI_SYS_CALL_STOP will be returned
        _name_to_token->Insert("sample",(void*)VERI_METHOD_SAMPLE) ;
        _name_to_token->Insert("triggered",(void*)VERI_METHOD_TRIGGERED) ; // VIPER #3189 : add sequence method triggered
        // System task/function aded in P1800-2009 LRM
        // I/O tasks and functions (21.3)
        _name_to_token->Insert("fflush", (void*)VERI_SYS_CALL_FFLUSH) ;
        _name_to_token->Insert("feof", (void*)VERI_SYS_CALL_FEOF) ;
        // Math functions (20.8)
        // Moved these function to VERILOG_2000 mode
        // LRM section 17.11.1/17.11.2 Viper 6807
        // These functions are available in AMS 2.3 also
        _name_to_token->Insert("clog2", (void*)VERI_SYS_CALL_CLOG2) ;
        _name_to_token->Insert("ln", (void*)VERI_SYS_CALL_LN) ;
        _name_to_token->Insert("log10", (void*)VERI_SYS_CALL_LOG10) ;
        _name_to_token->Insert("exp", (void*)VERI_SYS_CALL_EXP) ;
        _name_to_token->Insert("sqrt", (void*)VERI_SYS_CALL_SQRT) ;
        _name_to_token->Insert("pow", (void*)VERI_SYS_CALL_POW) ;
        _name_to_token->Insert("floor", (void*)VERI_SYS_CALL_FLOOR) ;
        _name_to_token->Insert("ceil", (void*)VERI_SYS_CALL_CEIL) ;
        _name_to_token->Insert("sin", (void*)VERI_SYS_CALL_SIN) ;
        _name_to_token->Insert("cos", (void*)VERI_SYS_CALL_COS) ;
        _name_to_token->Insert("tan", (void*)VERI_SYS_CALL_TAN) ;
        _name_to_token->Insert("asin", (void*)VERI_SYS_CALL_ASIN) ;
        _name_to_token->Insert("acos", (void*)VERI_SYS_CALL_ACOS) ;
        _name_to_token->Insert("atan", (void*)VERI_SYS_CALL_ATAN) ;
        _name_to_token->Insert("atan2", (void*)VERI_SYS_CALL_ATAN2) ;
        _name_to_token->Insert("hypot", (void*)VERI_SYS_CALL_HYPOT) ;
        _name_to_token->Insert("sinh", (void*)VERI_SYS_CALL_SINH) ;
        _name_to_token->Insert("cosh", (void*)VERI_SYS_CALL_COSH) ;
        _name_to_token->Insert("tanh", (void*)VERI_SYS_CALL_TANH) ;
        _name_to_token->Insert("asinh", (void*)VERI_SYS_CALL_ASINH) ;
        _name_to_token->Insert("acosh", (void*)VERI_SYS_CALL_ACOSH) ;
        _name_to_token->Insert("atanh", (void*)VERI_SYS_CALL_ATANH) ;
        // Assertion action control tasks (20.12)
        _name_to_token->Insert("assertpasson", (void*)VERI_SYS_CALL_ASSERTPASSON) ;
        _name_to_token->Insert("assertpassoff", (void*)VERI_SYS_CALL_ASSERTPASSOFF) ;
        _name_to_token->Insert("assertfailon", (void*)VERI_SYS_CALL_ASSERTFAILON) ;
        _name_to_token->Insert("assertfailoff", (void*)VERI_SYS_CALL_ASSERTFAILOFF) ;
        _name_to_token->Insert("assertnonvacuouson", (void*)VERI_SYS_CALL_ASSERTNONVACUOUSON) ;
        _name_to_token->Insert("assertvacuousoff", (void*)VERI_SYS_CALL_ASSERTVACUOUSOFF) ;
        // Assertion functions (20.13)
        _name_to_token->Insert("past_gclk", (void*)VERI_SYS_CALL_PAST_GCLK) ;
        _name_to_token->Insert("fell_gclk", (void*)VERI_SYS_CALL_FELL_GCLK) ;
        _name_to_token->Insert("changed_gclk", (void*)VERI_SYS_CALL_CHANGED_GCLK) ;
        _name_to_token->Insert("rising_gclk", (void*)VERI_SYS_CALL_RISING_GCLK) ;
        _name_to_token->Insert("steady_gclk", (void*)VERI_SYS_CALL_STEADY_GCLK) ;
        _name_to_token->Insert("changed", (void*)VERI_SYS_CALL_CHANGED) ;
        _name_to_token->Insert("rose_gclk", (void*)VERI_SYS_CALL_ROSE_GCLK) ;
        _name_to_token->Insert("stable_gclk", (void*)VERI_SYS_CALL_STABLE_GCLK) ;
        _name_to_token->Insert("future_gclk", (void*)VERI_SYS_CALL_FUTURE_GCLK) ;
        _name_to_token->Insert("falling_gclk", (void*)VERI_SYS_CALL_FALLING_GCLK) ;
        _name_to_token->Insert("changing_gclk", (void*)VERI_SYS_CALL_CHANGING_GCLK) ;
        _name_to_token->Insert("global_clock", (void*)VERI_SYS_CALL_GLOBAL_CLOCK) ;
    }
    // Go via unsigned long to avoid problems on 64bit compiles
    unsigned long rlong = (unsigned long)_name_to_token->GetValue(func_name) ;
    if (from && rlong) {
        // VIPER #8138: Check if the function token can be applied in the context of the given 'from' expression:
        unsigned sys_task = IsSystemTaskFunction((unsigned)rlong) ;
        unsigned built_in_method = IsBuiltInMethod((unsigned)rlong) ;
        if (from->IsHierName() && sys_task && !built_in_method) rlong = 0 ;  // Hier-names cannot be a system task/function
        if (!from->IsHierName() && !sys_task && built_in_method) rlong = 0 ; // Non-hier-names cannot be a built-in method
    }
    return (unsigned)rlong ;
}

/* static */ void
VeriExpression::ResetNameToTokens()
{
    delete _name_to_token ;
    _name_to_token = 0 ;
}

/* static */ unsigned
VeriExpression::IsSystemTaskFunction(unsigned token)
{
    // VIPER #8138: Check if the given function token is a system task/function.
    // Following are the system task/functions that I could find in SV-2012 and AMS 2.2 LRMs:
    switch (token) {
    case VERI_SYS_CALL_ACOS :
    case VERI_SYS_CALL_ACOSH :
    case VERI_SYS_CALL_ASIN :
    case VERI_SYS_CALL_ASINH :
    case VERI_SYS_CALL_ASSERTFAILOFF :
    case VERI_SYS_CALL_ASSERTFAILON :
    case VERI_SYS_CALL_ASSERTKILL :
    case VERI_SYS_CALL_ASSERTNONVACUOUSON :
    case VERI_SYS_CALL_ASSERTOFF :
    case VERI_SYS_CALL_ASSERTON :
    case VERI_SYS_CALL_ASSERTPASSOFF :
    case VERI_SYS_CALL_ASSERTPASSON :
    case VERI_SYS_CALL_ASSERTVACUOUSOFF :
    case VERI_SYS_CALL_ASYNC__AND__ARRAY :
    case VERI_SYS_CALL_ASYNC__AND__PLANE :
    case VERI_SYS_CALL_ASYNC__NAND__ARRAY :
    case VERI_SYS_CALL_ASYNC__NAND__PLANE :
    case VERI_SYS_CALL_ASYNC__NOR__ARRAY :
    case VERI_SYS_CALL_ASYNC__NOR__PLANE :
    case VERI_SYS_CALL_ASYNC__OR__ARRAY :
    case VERI_SYS_CALL_ASYNC__OR__PLANE :
    case VERI_SYS_CALL_ATAN :
    case VERI_SYS_CALL_ATAN2 :
    case VERI_SYS_CALL_ATANH :
    case VERI_SYS_CALL_BITS :
    case VERI_SYS_CALL_BITSTOREAL :
    case VERI_SYS_CALL_BITSTOSHORTREAL :
    case VERI_SYS_CALL_CAST :
    case VERI_SYS_CALL_CEIL :
    case VERI_SYS_CALL_CHANGED :
    case VERI_SYS_CALL_CHANGED_GCLK :
    case VERI_SYS_CALL_CHANGING_GCLK :
    case VERI_SYS_CALL_CLOG2 :
    case VERI_SYS_CALL_COS :
    case VERI_SYS_CALL_COSH :
    case VERI_SYS_CALL_COUNTONES :
    case VERI_SYS_CALL_COVERAGE_CONTROL :
    case VERI_SYS_CALL_COVERAGE_GET :
    case VERI_SYS_CALL_COVERAGE_GET_MAX :
    case VERI_SYS_CALL_COVERAGE_MERGE :
    case VERI_SYS_CALL_COVERAGE_SAVE :
    case VERI_SYS_CALL_DIMENSIONS :
    case VERI_SYS_CALL_DISPLAY :
    case VERI_SYS_CALL_DISPLAYB :
    case VERI_SYS_CALL_DISPLAYH :
    case VERI_SYS_CALL_DISPLAYO :
    case VERI_SYS_CALL_DIST_CHI_SQUARE :
    case VERI_SYS_CALL_DIST_ERLANG :
    case VERI_SYS_CALL_DIST_EXPONENTIAL :
    case VERI_SYS_CALL_DIST_NORMAL :
    case VERI_SYS_CALL_DIST_POISSON :
    case VERI_SYS_CALL_DIST_T :
    case VERI_SYS_CALL_DIST_UNIFORM :
    case VERI_SYS_CALL_DUMPALL :
    case VERI_SYS_CALL_DUMPFILE :
    case VERI_SYS_CALL_DUMPFLUSH :
    case VERI_SYS_CALL_DUMPLIMIT :
    case VERI_SYS_CALL_DUMPOFF :
    case VERI_SYS_CALL_DUMPON :
    case VERI_SYS_CALL_DUMPPORTS :
    case VERI_SYS_CALL_DUMPPORTSALL :
    case VERI_SYS_CALL_DUMPPORTSFLUSH :
    case VERI_SYS_CALL_DUMPPORTSLIMIT :
    case VERI_SYS_CALL_DUMPPORTSOFF :
    case VERI_SYS_CALL_DUMPPORTSON :
    case VERI_SYS_CALL_DUMPVARS :
    case VERI_SYS_CALL_ERROR :
    case VERI_SYS_CALL_EXIT :
    case VERI_SYS_CALL_EXP :
    case VERI_SYS_CALL_FALLING_GCLK :
    case VERI_SYS_CALL_FATAL :
    case VERI_SYS_CALL_FCLOSE :
    case VERI_SYS_CALL_FDISPLAY :
    case VERI_SYS_CALL_FDISPLAYB :
    case VERI_SYS_CALL_FDISPLAYH :
    case VERI_SYS_CALL_FDISPLAYO :
    case VERI_SYS_CALL_FELL :
    case VERI_SYS_CALL_FELL_GCLK :
    case VERI_SYS_CALL_FEOF :
    case VERI_SYS_CALL_FERROR :
    case VERI_SYS_CALL_FFLUSH :
    case VERI_SYS_CALL_FGETC :
    case VERI_SYS_CALL_FGETS :
    case VERI_SYS_CALL_FLOOR :
    case VERI_SYS_CALL_FMONITOR :
    case VERI_SYS_CALL_FMONITORB :
    case VERI_SYS_CALL_FMONITORH :
    case VERI_SYS_CALL_FMONITORO :
    case VERI_SYS_CALL_FOPEN :
    case VERI_SYS_CALL_FREAD :
    case VERI_SYS_CALL_FSCANF :
    case VERI_SYS_CALL_FSEEK :
    case VERI_SYS_CALL_FSTROBE :
    case VERI_SYS_CALL_FSTROBEB :
    case VERI_SYS_CALL_FSTROBEH :
    case VERI_SYS_CALL_FSTROBEO :
    case VERI_SYS_CALL_FTELL :
    case VERI_SYS_CALL_FULLSKEW :
    case VERI_SYS_CALL_FUTURE_GCLK :
    case VERI_SYS_CALL_FWRITE :
    case VERI_SYS_CALL_FWRITEB :
    case VERI_SYS_CALL_FWRITEH :
    case VERI_SYS_CALL_FWRITEO :
    case VERI_SYS_CALL_GLOBAL_CLOCK :
    case VERI_SYS_CALL_HIGH :
    case VERI_SYS_CALL_HOLD :
    case VERI_SYS_CALL_HYPOT :
    case VERI_SYS_CALL_INCREMENT :
    case VERI_SYS_CALL_INFO :
    case VERI_SYS_CALL_ISUNBOUNDED :
    case VERI_SYS_CALL_ISUNKNOWN :
    case VERI_SYS_CALL_ITOR :
    case VERI_SYS_CALL_LEFT :
    case VERI_SYS_CALL_LN :
    case VERI_SYS_CALL_LOAD_COVERAGE_DB :
    case VERI_SYS_CALL_LOG10 :
    case VERI_SYS_CALL_LOW :
    case VERI_SYS_CALL_MONITOR :
    case VERI_SYS_CALL_MONITORB :
    case VERI_SYS_CALL_MONITORH :
    case VERI_SYS_CALL_MONITORO :
    case VERI_SYS_CALL_MONITOROFF :
    case VERI_SYS_CALL_MONITORON :
    case VERI_SYS_CALL_NOCHANGE :
    case VERI_SYS_CALL_ONEHOT :
    case VERI_SYS_CALL_ONEHOT0 :
    case VERI_SYS_CALL_PAST :
    case VERI_SYS_CALL_PAST_GCLK :
    case VERI_SYS_CALL_PERIOD :
    case VERI_SYS_CALL_POW :
    case VERI_SYS_CALL_PRINTTIMESCALE :
    case VERI_SYS_CALL_Q_ADD :
    case VERI_SYS_CALL_Q_EXAM :
    case VERI_SYS_CALL_Q_FULL :
    case VERI_SYS_CALL_Q_INITIALIZE :
    case VERI_SYS_CALL_Q_REMOVE :
    case VERI_SYS_CALL_RANDOM :
    case VERI_SYS_CALL_READMEMB :
    case VERI_SYS_CALL_READMEMH :
    case VERI_SYS_CALL_REALTIME :
    case VERI_SYS_CALL_REALTOBITS :
    case VERI_SYS_CALL_RECOVERY :
    case VERI_SYS_CALL_RECREM :
    case VERI_SYS_CALL_REMOVAL :
    case VERI_SYS_CALL_REWIND :
    case VERI_SYS_CALL_RIGHT :
    case VERI_SYS_CALL_RISING_GCLK :
    case VERI_SYS_CALL_ROOT :
    case VERI_SYS_CALL_ROSE :
    case VERI_SYS_CALL_ROSE_GCLK :
    case VERI_SYS_CALL_RTOI :
    case VERI_SYS_CALL_SAMPLED :
    case VERI_SYS_CALL_SDF_ANNOTATE :
    case VERI_SYS_CALL_SET_COVERAGE_DB_NAME :
    case VERI_SYS_CALL_SETUP :
    case VERI_SYS_CALL_SETUPHOLD :
    case VERI_SYS_CALL_SFORMAT :
    case VERI_SYS_CALL_SHORTREALTOBITS :
    case VERI_SYS_CALL_SIGNED :
    case VERI_SYS_CALL_SIN :
    case VERI_SYS_CALL_SINH :
    case VERI_SYS_CALL_SKEW :
    case VERI_SYS_CALL_SQRT :
    case VERI_SYS_CALL_SSCANF :
    case VERI_SYS_CALL_STABLE :
    case VERI_SYS_CALL_STABLE_GCLK :
    case VERI_SYS_CALL_STEADY_GCLK :
    case VERI_SYS_CALL_STIME :
    case VERI_SYS_CALL_STROBE :
    case VERI_SYS_CALL_STROBEB :
    case VERI_SYS_CALL_STROBEH :
    case VERI_SYS_CALL_STROBEO :
    case VERI_SYS_CALL_SWRITE :
    case VERI_SYS_CALL_SWRITEB :
    case VERI_SYS_CALL_SWRITEH :
    case VERI_SYS_CALL_SWRITEO :
    case VERI_SYS_CALL_SYNC__AND__ARRAY :
    case VERI_SYS_CALL_SYNC__AND__PLANE :
    case VERI_SYS_CALL_SYNC__NAND__ARRAY :
    case VERI_SYS_CALL_SYNC__NAND__PLANE :
    case VERI_SYS_CALL_SYNC__NOR__ARRAY :
    case VERI_SYS_CALL_SYNC__NOR__PLANE :
    case VERI_SYS_CALL_SYNC__OR__ARRAY :
    case VERI_SYS_CALL_SYNC__OR__PLANE :
    case VERI_SYS_CALL_TAN :
    case VERI_SYS_CALL_TANH :
    case VERI_SYS_CALL_TEST__PLUSARGS :
    case VERI_SYS_CALL_TIME :
    case VERI_SYS_CALL_TIMEFORMAT :
    case VERI_SYS_CALL_TIMESKEW :
    case VERI_SYS_CALL_TYPENAME :
    case VERI_SYS_CALL_UNGETC :
    case VERI_SYS_CALL_UNIT :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS :
    case VERI_SYS_CALL_UNSIGNED :
    case VERI_SYS_CALL_URANDOM :
    case VERI_SYS_CALL_URANDOM_RANGE :
    case VERI_SYS_CALL_VALUE__PLUSARGS :
    case VERI_SYS_CALL_WARNING :
    case VERI_SYS_CALL_WIDTH :
    case VERI_SYS_CALL_WRITE :
    case VERI_SYS_CALL_WRITEB :
    case VERI_SYS_CALL_WRITEH :
    case VERI_SYS_CALL_WRITEMEMB :
    case VERI_SYS_CALL_WRITEMEMH :
    case VERI_SYS_CALL_WRITEO :
        return 1 ; // This is a system task/function
    case VERI_SYS_CALL_SIZE :
    case VERI_SYS_CALL_STOP :
    case VERI_SYS_CALL_FINISH :
    case VERI_SYS_CALL_GET_COVERAGE :
        return 1 ; // This is both a system task/function and a built-in method
    default: break ;
    }

    return 0 ; // Not a system task/function
}

/* static */ unsigned
VeriExpression::IsBuiltInMethod(unsigned token)
{
    switch (token) {
    case VERI_METHOD_AND :
    case VERI_METHOD_ATOBIN :
    case VERI_METHOD_ATOHEX :
    case VERI_METHOD_ATOI :
    case VERI_METHOD_ATOOCT :
    case VERI_METHOD_ATOREAL :
    case VERI_METHOD_AWAIT :
    case VERI_METHOD_BACK :
    case VERI_METHOD_BINTOA :
    case VERI_METHOD_CLEAR :
    case VERI_METHOD_COMPARE :
    case VERI_METHOD_CONSTRAINT_MODE :
    case VERI_METHOD_DATA :
    case VERI_METHOD_DELETE :
    case VERI_METHOD_EMPTY :
    case VERI_METHOD_ENDED :
    case VERI_METHOD_EQ :
    case VERI_METHOD_ERASE :
    case VERI_METHOD_ERASE_RANGE :
    case VERI_METHOD_EXISTS :
    case VERI_METHOD_FIND :
    case VERI_METHOD_FIND_FIRST :
    case VERI_METHOD_FIND_FIRST_INDEX :
    case VERI_METHOD_FIND_INDEX :
    case VERI_METHOD_FIND_LAST :
    case VERI_METHOD_FIND_LAST_INDEX :
    case VERI_METHOD_FIRST :
    case VERI_METHOD_FRONT :
    case VERI_METHOD_GETC :
    case VERI_METHOD_GET_INST_COVERAGE :
    case VERI_METHOD_GET_RANDSTATE :
    case VERI_METHOD_HEXTOA :
    case VERI_METHOD_ICOMPARE :
    case VERI_METHOD_INDEX :
    case VERI_METHOD_INSERT :
    case VERI_METHOD_INSERT_RANGE :
    case VERI_METHOD_ITOA :
    case VERI_METHOD_KILL :
    case VERI_METHOD_LAST :
    case VERI_METHOD_LEN :
    case VERI_METHOD_MATCHED :
    case VERI_METHOD_MAX :
    case VERI_METHOD_MIN :
    case VERI_METHOD_NAME :
    case VERI_METHOD_NEQ :
    case VERI_METHOD_NEW :
    case VERI_METHOD_NEXT :
    case VERI_METHOD_NUM :
    case VERI_METHOD_OCTTOA :
    case VERI_METHOD_OR :
    case VERI_METHOD_POP_BACK :
    case VERI_METHOD_POP_FRONT :
    case VERI_METHOD_POST_RANDOMIZE :
    case VERI_METHOD_PRE_RANDOMIZE :
    case VERI_METHOD_PREV :
    case VERI_METHOD_PROCESS :
    case VERI_METHOD_PRODUCT :
    case VERI_METHOD_PURGE :
    case VERI_METHOD_PUSH_BACK :
    case VERI_METHOD_PUSH_FRONT :
    case VERI_METHOD_PUTC :
    case VERI_METHOD_RAND_MODE :
    case VERI_METHOD_RANDOMIZE :
    case VERI_METHOD_REALTOA :
    case VERI_METHOD_RESUME :
    case VERI_METHOD_REVERSE :
    case VERI_METHOD_RSORT :
    case VERI_METHOD_SAMPLE :
    case VERI_METHOD_SELF :
    case VERI_METHOD_SET :
    case VERI_METHOD_SET_INST_NAME :
    case VERI_METHOD_SET_RANDSTATE :
    case VERI_METHOD_SHUFFLE :
    case VERI_METHOD_SIE :
    case VERI_METHOD_SORT :
    case VERI_METHOD_START :
    case VERI_METHOD_STATUS :
    case VERI_METHOD_SUBSTR :
    case VERI_METHOD_SUM :
    case VERI_METHOD_SUSPEND :
    case VERI_METHOD_SWAP :
    case VERI_METHOD_TOLOWER :
    case VERI_METHOD_TOUPPER :
    case VERI_METHOD_TRIGGERED :
    case VERI_METHOD_UNIQUE :
    case VERI_METHOD_UNIQUE_INDEX :
    case VERI_METHOD_XOR :
    case VERI_SYS_CALL_INSET :
    case VERI_SYS_CALL_INSETZ :
    case VERI_SYS_CALL_LENGTH :
    case VERI_SYS_CALL_SRANDOM :
    case VERI_SYS_CALL_TYPEOF :
    case VERI_SYS_CALL_UNSIGNED :
        return 1 ; // This is a built-in method
    case VERI_SYS_CALL_SIZE :
    case VERI_SYS_CALL_STOP :
    case VERI_SYS_CALL_FINISH :
    case VERI_SYS_CALL_GET_COVERAGE :
        return 1 ; // This is both a system task/function and a built-in method
    default: break ;
    }

    return 0 ; // Not a built-in method
}

/* static */ unsigned VeriExpression::GetFuncTokenOfClass(const char *func_name)
{
    unsigned function_type = GetFuncToken(func_name) ;

    if (!function_type) return 0 ; // Not any built-in method name
    // Check whether these tokens can be used within class. If yes, return token,
    // otherwise return 0
    switch (function_type) {
    case VERI_METHOD_GET_RANDSTATE : // Check on class obj
    case VERI_METHOD_SET_RANDSTATE :
    case VERI_METHOD_NEW : // new (on super is allowed only)
    case VERI_METHOD_RANDOMIZE : // function int randomize()
    case VERI_METHOD_PRE_RANDOMIZE :  // function void pre_randomize()
    case VERI_METHOD_POST_RANDOMIZE : // function void post_randomize()
    case VERI_METHOD_CONSTRAINT_MODE : // can be on class object/constraint block
    case VERI_METHOD_RAND_MODE : // can be called on class object/random variable
    case VERI_SYS_CALL_SRANDOM :
        return function_type ;
    default :
        return 0 ;
    }
    return 0 ;
}

//////////////////////////////////////////////////////////////////////
// Resolve identifier references (run after analysis of a module).
//////////////////////////////////////////////////////////////////////

void
VeriExpression::ResolveOnehot(VeriScope *scope, veri_environment environment)
{
    // Resort to normal expression resolving.
    Resolve(scope, environment) ;
}

void
VeriIdRef::ResolveOnehot(VeriScope *scope, veri_environment environment)
{
    // Special 'Resolve' routine to catch if this expression can be used as a onehot expression.
    // We are not sure if it will be used as onehot expression, so here we just
    // prevent discarding IdRef's as valid (onehot) state variables/constants.

    const char *name = GetName() ;
    // Resolve the identifier as normal (if we have a scope and a name and no previous setting yet).
    // VIPER #6337 : Do not try to find identifier if this is part of default actual
    // argument of sequence/property port. It can only refer already declared identifier
    // VIPER #6919 : Find whether 'name' is illegally imported
    if (scope) (void) scope->FindImplicitlyImportedErrorCond(name, this) ;
    if (!_id && (environment != VERI_SEQ_PROP_INIT) && scope && name) _id = scope->Find(name, /*from*/this) ;

    // VIPER #6487 : Reference of named port should be discarded
    if (_id && _id->IsNamedPort()) SetId(0) ;

    // Checks on this identifier :
    if (!_id) {
        // Inline constraint block (randomize() ..with) can use member variables of
        // object class and also local variables. So member variables of object class
        // referencing cannot be resolved during analysis.
        // (Called from VeriInlineConstraint::Resolve)
        if (environment != VERI_INLINE_CONSTRAINT_EXPR)
        Error("%s is not declared", name) ;
    }
    // Viper 5359. Only integral types are allowed in Constraint expression
    if (_id && (environment == VERI_CONSTRAINT_EXPR || environment == VERI_INLINE_CONSTRAINT_EXPR)) {
        VeriTypeInfo *type = CheckType(0, READ) ;
        // Do not error out for UnresolvedNameType and ParameterType VIPER 5359(new test)
        if (type && !type->IsUnresolvedNameType() && !type->IsTypeParameterType() && !type->IsIntegralType()) Error("non integral type variables are not allowed in constraint declaration") ;
        delete type ;
    }

    // VIPER #6506 : Implicit non-ansi ports ports cannot be referenced :
    if (_id && _id->IsImplicitNonAnsiPort()) {
        Error("%s is not valid in an expression", _id->Name()) ;
    }

//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        // Cannot resolve to unnamed generate block, it is not known whether it is active or not
        if (_id && _id->IsUnnamedBlock()) SetId(0) ;
    }
//#endif

    // VIPER #8342: Do not resolve to modules/packages in non-SV mode other than the current module:
    if (_id && _id->IsModule() && !IsSystemVeri() && scope && (_id != scope->GetContainingModule())) SetId(0) ;

    if (!_id) return ; // cannot check anything else

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ; _name = 0 ;
#endif
#endif

    // Environment of lhs :
    //veri_environment env = environment ;

    // VIPER #4039 : Instance constant can be assigned inside the constructor of

    // class. So if we are here processing the body of constructor, pass a different
    // environment :
    // VIPER #5481 : Passed VERI_CONSTRUCTOR_LVALUE from blocking assign
    /*
    if (env == VERI_REG_LVALUE) {
        VeriIdDef *owner = (scope) ? scope->GetSubprogram(): 0 ;
        if (IsSystemVeri() && owner && owner->IsFunction() && Strings::compare_nocase(owner->Name(), "new")) {
            env = VERI_CONSTRUCTOR_LVALUE ;
        }
    }
    */
    // Check the usage of this identifier in this environment
    _id->CheckEnvironment(environment, /*from*/this) ;

    // VIPER issue 2729 : check that function id is not used by itself, other than inside a function with the same name.
    // We do not have the scope available withing 'CheckEnvironment', else we should put this check there.
    // Note that issue 2767 pretty much excludes actual functions being called without ()'s. Only allowed for sequence and property calls.
    // So we could simplify this test to _id->IsFunction() && !_id->IsSequence() && !id->IsProperty(), without the scope check, and move this test into CheckEnvironment.
    if (scope && _id->IsFunction() && // _id->GetPorts() && (_id->GetPorts()->Size()!=0) && // we are referencing a function that has ports
        // VIPER #8011: Allow function call without argument if function
        // 1) does not contain any port
        // 2) contains port(s) with default value
        !_id->CanCallWithoutArg() &&
        (environment != VERI_UPWARD_SCOPE_NAME) && // we are not simply looking for a function name
        (_id != scope->GetSubprogram()) ) { // we are not inside this function itself
        Error("%s is not valid in an expression", _id->Name()) ;
    }

    // By all means, DON'T discard the _id as a onehot variable.  Thats the whole purpose of this routine.

    // Extra environment test for direct identifier reference :
    // These access checks are not needed for System Verilog (assignment/access of unpacked vars always allowed).
    if (_id->IsMemory() && !IsSystemVeri()) {
        switch (environment) {
        case VERI_LVALUE :
        case VERI_NET_LVALUE :
        case VERI_NET_LVALUE_UNDECL_USE_GOOD :
        case VERI_REG_LVALUE :
        case VERI_FORCE_LVALUE :
        case VERI_GENVAR_LVALUE :
        case VERI_CONSTRUCTOR_LVALUE :
            Error("cannot assign to memory %s directly", _id->Name()) ;
            break ;
        case VERI_EXPRESSION :
        case VERI_EXPRESSION_UNDECL_USE_GOOD :
        case VERI_ABORT_EXPRESSION :
        case VERI_LET_EXPRESSION :
        case VERI_PROCE_CONT_ASSIGN_VAL : // VIPER #6110: value of RHS of procedural continuous assignments
        case VERI_WAIT_EXP :
        case VERI_STRING_TARGET : // for VIPER #4259
        case VERI_CONSTANT_EXPRESSION :
        case VERI_PARAM_INITIAL_EXP :
        case VERI_INTEGRAL_CONSTANT : // integral constant is allowed where constant required
        case VERI_PARAM_VALUE :
        case VERI_BINS_INIT_VALUE :
        case VERI_PORT_CONNECTION :
        case VERI_GATE_INOUT_PORT_CONNECTION : // VIPER 2543 : Actual of primitive trans gate cann't be memory
            Error("cannot access memory %s directly", _id->Name()) ;
            break ;
        default : break ;
        }
    }

    // Use before declaration check: VIPER #4379, 5609, 2775, 3121, 5712, 6151, 6599, 3503, 5604
    if (UsedBeforeDeclare(scope, environment)) Warning("identifier %s is used before its declaration", GetName()) ;

    // VIPER #4043 : Check usage of private and protected members of a class
    // VIPER #6241: Check for public automatic variables too for nested class:
    if (_id->IsPrivate() || _id->IsProtected() || _id->IsAutomatic()) {
        _id->CheckUsageOfClassMember(scope, this) ;
    }
    // VIPER #7043 : If this is sequence/property formal, Resolve the associated
    // actual with coming environment
    if (_id->IsSequenceFormal() || _id->IsPropertyFormal()) {
        VeriExpression *actual = _id->GetActual() ;
        if (!_id->IsProcessing() && actual) {
            _id->SetIsProcessing() ; // Mark starting of processing
            // VIPER #7883 : Do not resolve identifier references of actual, so pass null scope
            actual->Resolve(0, environment) ;
            _id->ClearIsProcessing() ; // finished processing
        }
    }
}

void
VeriQuestionColon::ResolveOnehot(VeriScope *scope, veri_environment environment)
{
    // VIPER #5635 : Condition of ?: operator will always be expression
    if (_if_expr) _if_expr->Resolve(scope, VERI_EXPRESSION) ;
    // check type of condition
    VeriTypeInfo *cond_type = (_if_expr) ? _if_expr->CheckType(0, READ) : 0 ; ; // condition is context independent
    // VIPER #6478: Produce error if condition is unpacked type
    // VIPER #7922: Do not produce error for virtual interface instance.
    // 1800-2009 LRM Section 25.9 states virtual interfaces can be used in
    // the context of conditional expressions
    if (cond_type && !cond_type->IsTypeParameterType() && !cond_type->IsUnresolvedNameType() && (cond_type->IsUnpackedArrayType() || (cond_type->IsStructure() && cond_type->IsUnpacked())) && !cond_type->IsVirtualInterface() && !cond_type->IsClassType() && _if_expr) {
        _if_expr->Error("illegal context for %s", "unpacked expression") ;
    }
    delete cond_type ;

    // Allow onehot expressions in then/else part.
    // As long as they complyy with the other onehot constraints (linkonehot etc).
    VeriScope *then_scope = (_if_expr && _if_expr->GetScope()) ? _if_expr->GetScope() : scope ;
    if (_then_expr) _then_expr->ResolveOnehot(then_scope, environment) ;
    if (_else_expr) _else_expr->ResolveOnehot(scope, environment) ;

    // VIPER 7445 :
    // Link the expressions by finding a full identifier variable on either side :
    if (_then_expr && _else_expr && _then_expr->FullId()) {
        _else_expr->LinkOnehot(_then_expr->FullId()) ;
    } else if (_then_expr && _else_expr && _else_expr->FullId()) {
        _then_expr->LinkOnehot(_else_expr->FullId()) ;
    }
}

//////////////////////////////////////////////////////////////////////
// ResolveOnehot : two expressions that can be used in onehot
//////////////////////////////////////////////////////////////////////

void VeriExpression::LinkOnehot(VeriIdDef *onehot_var)
{
    // onehot var is linked to some expression. Not allowed.
    // discard onehot_var as onehot :
    if (onehot_var) onehot_var->SetCannotBeOnehot() ;
}

void VeriIdRef::LinkOnehot(VeriIdDef *onehot_var)
{
    // onehot var is linked to a identifier reference

    // This we will allow if :
    //    1) both are onehot variables (plain assign/compare between two identifiers)
    //    2) one side is onehot variable and the idref is a parameter (plain assign (or compare) var and parameter)
    //    3) one side is a parameter and the idref is a onehot variable (compare between var and parameter)
    // All other uses should discard both id and ref as onehot.

    // Check if onehot var is really a onehot variable :
    if (onehot_var && onehot_var->CanBeOnehot() && onehot_var->IsVar()) {
        if (_id && _id->IsVar() && _id->CanBeOnehot()) {
            // onehot variable assignment/compare. Just link the variables :
            _id->LinkOnehotVar(onehot_var) ;
            // Accept as valid
            return ;
        }

        VeriExpression *init_val = (_id) ? _id->GetInitialValue() : 0 ;
        if (_id && _id->IsParam() && init_val && init_val->IsConst()) {
            // RHS is a constant parameter.
            // insert it as a valid state constant.
            // Add as fsm constant to state variable lid.
            onehot_var->AddFsmStateConstant(this) ;
            // Accept as valid
            return ;
        }
    }

    // Check if this expression (RHS) is a onehot variable :
    if (_id && _id->CanBeOnehot() && _id->IsVar()) {
        VeriExpression *init_val = (onehot_var) ? onehot_var->GetInitialValue() : 0 ;
        if (onehot_var && onehot_var->IsParam() && init_val && init_val->IsConst()) {
            // LHS is a constant parameter.
            // Accept as valid
            return ;
        }
    }

    // Discard other uses of onehot variables :
    if (_id) _id->SetCannotBeOnehot() ;
    if (onehot_var) onehot_var->SetCannotBeOnehot() ;

    // VIPER #6506 : Implicit non-ansi ports ports cannot be referenced :
    if (_id && _id->IsImplicitNonAnsiPort()) {
        Error("%s is not valid in an expression", _id->Name()) ;
    }
}

void VeriQuestionColon::LinkOnehot(VeriIdDef *onehot_var)
{
    // To make a valid link to ?: operands, link both sides :
    if (_then_expr) _then_expr->LinkOnehot(onehot_var) ;
    if (_else_expr) _else_expr->LinkOnehot(onehot_var) ;
}

//////////////////////////////////////////////////////////////////////
// Resolve identifier references (run after analysis of a module).
//////////////////////////////////////////////////////////////////////

    // Resolve a name that is the prefix of a IndexedNamed or SelectedName
void VeriIdRef::ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned /*allow_range_index*/)
{
    const char *name = GetName() ;

    if (environment==VERI_COVERPOINT_ENV) {
        // If environment is VERI_COVERPOINT_ENV, it is needed to discard block ids defined
        // upto covergroup's scope. After that normal scoping rule can be applied. (1800-2009 19.5)
        VeriScope *curr_scope = scope ;
        VeriIdDef *id = 0 ;
        VeriIdDef *curr_scope_owner = 0 ;
        while (curr_scope && !id) {
            id = curr_scope->FindLocal(name) ;
            if (id && id->IsBlock()) id = 0 ;
            curr_scope_owner = curr_scope->GetOwner() ;
            curr_scope = curr_scope->Upper() ;
            if (curr_scope_owner && curr_scope_owner->IsCovergroup()) break ;
        }
        if (id) {
            _id = id ;
        } else {
            _id = curr_scope ? curr_scope->Find(name, /*from*/this): 0 ;
        }
    }
    // If environment is a PORT_EXPRESSION (where references are made before declaration),
    // then make sure that any id resolved during parsing gets re-set (and looked up in this final scope)
    if (_id && (environment==VERI_PORT_EXPRESSION || environment==VERI_PATH_EXPRESSION)) SetId(0) ;

    // VIPER #6919 : Find whether 'name' is illegally imported
    if (scope) (void) scope->FindImplicitlyImportedErrorCond(name, this) ;
    // Resolve the identifier as normal (if we have a scope and a name and no previous setting yet).
    if (!_id && scope && name) _id = scope->Find(name, /*from*/this) ;

    // VIPER #6487 : Reference of named port should be discarded
    if (_id && _id->IsNamedPort()) SetId(0) ;

    // VIPER #4924: Function/task can also be used before their definition. So
    // it may be the case that function/task call is already resolved with an
    // incorrect identifier. So when environment is VERI_UPWARD_SCOPE_NAME, try
    // to re-resolve the identifier reference. If we can get new identifier, use
    // that, otherwise keep old one. VERI_UPWARD_SCOPE_NAME environment is also
    // used to resolve name of instantiated module in instantiation. For instantiation
    // it may be the case that from yacc instantiated module name is resolved, but
    // when we are trying to re-resolve, we may find null. So keep previous one for that.

    // VIPER #5450 : Always try to re-resolve the identifier reference. Hierarchical name
    // can appear before declaration of actual identifier.
    // VIPER #5465 :  This function is called even for indexed name like 'a[i]'
    // for such cases we should not re-resolve the reference. So re-resolve
    // only for upward name reference
    if (environment==VERI_UPWARD_SCOPE_NAME && scope) {
        VeriIdDef *prev_id = _id ;
        _id = scope->Find(name, /*from*/this) ;
        if (!_id) _id = prev_id ;
    }
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        // Cannot resolve to unnamed generate block, it is not known whether it is active or not
        if (_id && _id->IsUnnamedBlock()) SetId(0) ;
    }
//#endif

    // VIPER #8342: Do not resolve to modules/packages in non-SV mode other than the current module:
    if (_id && _id->IsModule() && !IsSystemVeri() && scope && (_id != scope->GetContainingModule())) SetId(0) ;

    // Since this is a prefix identifier (prefix of a hierarchical name),
    // do NOT complain if it is not declared.
    // It could be a module identifier, and modules don't need to be declared at analysis time.
    if (!_id) return ;

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ; _name = 0 ;
#endif
#endif

    // Viper #2186
    // Use before declaration check: VIPER #4379, 5609, 2775, 3121, 5712, 6151, 6599, 3503, 5604
    if (UsedBeforeDeclare(scope, environment)) Warning("identifier %s is used before its declaration", GetName()) ;

    // Still, we do want to set IsUsed() and IsAssigned() based on environment...
    // and error out on some environments ?...
}

void VeriSelectedName::ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned /*allow_range_index*/)
{
    (void) environment ; // to prevent unused argument compiler warnings
    // VIPER #5465: Prefix can refer identifier declared later or in upper scope,
    // so use 'VERI_UPWARD_SCOPE_NAME' environment
    if (_prefix) _prefix->ResolvePrefix(scope, VERI_UPWARD_SCOPE_NAME, 0) ;
    _suffix_id = 0 ;

    // Now pick up the identifier of the prefix :
    _prefix_id = (_prefix) ? _prefix->GetId() : 0 ;

    // Viper 5353. Access to class members from class type is not allowed
    if (_prefix && _prefix->GetToken()!= VERI_THIS && _prefix->GetToken()!=VERI_SUPER &&
        _prefix_id && _prefix_id->IsType() && _prefix_id->IsClass()) {
        _prefix->Error("illegal prefix for %s", _suffix) ;
    }

    // Resolve suffix_id

    // Look for the suffix identifier under the prefix id.
    if (!_prefix_id) return ; // cant do anything without a prefix identifier

    // Check if we are about to point into a generate-for loop.
    VeriModuleItem *prefix_module_item = _prefix_id->GetModuleItem() ;
    if ((prefix_module_item && (prefix_module_item->IsGenerateFor() || prefix_module_item->IsGenerateBlock())) || _prefix_id->IsUnnamedBlock()) {
        // Do NOT try to resolve an identifier declared inside a generate-for loop or generate block.
        // It would not identify a unique identifier, since inside generate-for loops we
        // will 'generate' new identifiers.
        // This is similar to trying to resolve an identifier via a instance, or into a separate module.
        // VIPER #6817: When prefix is generate-for/generate-block/block, suffix
        // cannot be method. So reset _function_type
        _function_type = 0 ;
        return ;
    }

    // VIPER #2954: Do not resolve a suffix defined under an (packed/unpacked) array
    // for which the prefix is not an indexed name. This should not be allowed:
    // VIPER #2980: Match the number of packed/unpacked dimension in declaration and
    // dimension in the prefix. We will not resolve the suffix if they don't match:
    if (_prefix && (_prefix_id->IsVar() || _prefix_id->IsInst()) && !_function_type) {
        // First get the number of dimensions in the prefix of this hier identifier:
        unsigned select_dims = 0 ;
        if (_prefix->GetIndexExpr()) {
            select_dims = 1 ; // VeriIndexedId with only one dimension
        } else {
            Array *indices = _prefix->GetIndexes() ;
            if (indices) select_dims = indices->Size() ; // VeriIndexedMemoryId with multiple dimensions
        }

        // Now get the number of dimensions that must be indexed to refer elements declared
        // inside the prefix. This is unpacked dimensions and packed/unpacked dimenisons for
        // structure and union type variables:
        // Make it same as 'select_dims' so that we do not error out in case we could not create id-type below:
        unsigned declare_dims = select_dims ;

        // VIPER #4096 : Use created type instead of data type back pointer to
        // check if selected name is legal or not.
        VeriTypeInfo *datatype = _prefix_id->CreateType(0) ;
        if (datatype) {
            declare_dims = datatype->UnpackedDimension() ;
            VeriTypeInfo *base_type = datatype->BaseType() ;
            if (base_type && (base_type->IsStructure() || base_type->IsUnion())) {
                // Add the packed dimensions only for the structure/unions:
                declare_dims = declare_dims + datatype->PackedDimension() ;
            } else if (datatype->IsTypeParameterType() || datatype->IsUnresolvedNameType()) {
                // This data type is a type parameter, set 'declare_dims' such that we don't error out.
                // We cannot fully check for dimension in case of type parameters, so better be silent:
                declare_dims = select_dims ;
            }
        }
        delete datatype ;

        // Check that the two types of dimensions matches for this hier-id:
        if (select_dims != declare_dims) {
            Warning("cannot select %s inside %s due to dimension mismatch", _suffix, _prefix->GetName()) ;
            return ; // Don't resolve the suffix here
        }
    }

    // First, check if this is a selected name of Verilog 95/2001 style.
    // In that case, we need to look in the prefix scope :
    VeriScope *prefix_scope = _prefix_id->LocalScope() ;

    // Find the suffix local in that scope :
    // VIPER #7370: It will be done after data type checking
    //if (prefix_scope) {
        //_suffix_id = prefix_scope->FindLocal(_suffix) ;
        // VIPER #3327 : If '_suffix' is declared in unnamed child scope, return
        // It may be declared in multiple paths
        // VIPER #5588: Moved below so that normal ids get resolved as usual (1).
        //if (!_suffix_id && prefix_scope->FindInUnnamedChildScopes(_suffix)) return ;
    //}

    // If that did not work, check if we are dealing with an instantiation :
    if (_prefix_id->IsInst()) {
        // Look through the instantiation !
        // Needed for direct selected name into an instance (of a interface or class or so)
        VeriModuleInstantiation *instance = _prefix_id->GetModuleInstance() ;
        VeriName *unit_name = (instance) ? instance->GetModuleNameRef() : 0 ;
        VeriIdDef *unit_id = (unit_name) ? unit_name->GetId() : 0 ;

        // We should not actually do anything here, since instance-path specific identifiers are not unique.
        // But for now, we have to do this for Interface and for Class instances,
        // to make later (elaboration) work. FIX ME later.
        if (unit_id && !unit_id->IsInterface() && !unit_id->IsClass()) {
            unit_id = 0 ; // eliminate this for any other instance.
        }

        // Get the scope of this unit :
        prefix_scope = (unit_id) ? unit_id->LocalScope() : 0 ;
        // Find the suffix local in that scope :
        if (prefix_scope) {
            _suffix_id = prefix_scope->FindLocal(_suffix) ;
            // VIPER #3327 : If '_suffix' is declared in unnamed child scope, return
            // It may be declared in multiple paths
            // VIPER #5588: Moved below so that normal ids get resolved as usual (2).
            //if (!_suffix_id && prefix_scope->FindInUnnamedChildScopes(_suffix)) return ;
        }
    }

    // If the prefix  (such as a function id) has a scope, then we still might need to look in the datatype.
    // see example 10.3/strReturn1 of an unnamed struct data type on a function.
    // So just check if we resolved the suffix id. If not, then we will check the data type.
    if (!_suffix_id) {
        // If the identifier itself is a variable,
        // we could find the suffix in the data type of the identifier.
        // Works for variables where the data type is a class or struct/union.
        // Also works for ports where the data type denotes a interface, or modport.
        //VeriDataType *data_type = _prefix_id->GetDataType() ;

        // Check for member in the data type :
        // This includes any reference to class/type/ or interface.
        //if (data_type) _suffix_id = data_type->GetMember(_suffix) ;
        VeriTypeInfo *data_type = (_prefix) ? _prefix->CheckType(0, NO_ENV): 0 ;
        // Data type is defined by type parameter or unresolved name
        if (data_type && (data_type->IsTypeParameterType() || data_type->IsUnresolvedNameType())) {
            delete data_type ;
            return ;
        }

        // Check for member in the data type :
        // This includes any reference to class/type/ or interface.
        if (data_type) _suffix_id = data_type->GetMember(_suffix) ;
        delete data_type ;
    }
    // VIPER #7370 : Look at the scope after checking data type
    if (!_suffix_id && prefix_scope) {
        _suffix_id = prefix_scope->FindLocal(_suffix) ;
    }

    // VIPER #5588: Move (1) & (2) down here so that normal ids get resolved as usual.
    // Prefix scope would have been available in either of (1) or (2) above. This is
    // because if prefix-id has local scope it cannot be an instance-id as instance-ids
    // do not have local scope. So, any one of (1) or (2) would have been active and that
    // still holds for this position down here. So, here it satisfies both the two above.
    // VIPER #3327 : If '_suffix' is declared in unnamed child scope, return
    // It may be declared in multiple paths which is only usable in elaborator.
    if (!_suffix_id && prefix_scope && prefix_scope->FindInUnnamedChildScopes(_suffix)) return ;

    // Viper 8036: Also do similar checking done for 3736 in ResolvePrefix like done
    // in SelectedName::Resolve.
    // Copied this code from VeriSelectedName::Resolve
    // VIPER #3736 : If _suffix_id is an object defined within interface and used
    // in a module where that interface is a port, do not set the assigned/used field
    unsigned is_prefix_interface_port = 0 ;
    VeriName *pre = _prefix ;
    VeriIdDef *pid ;
    while(pre) {
        pid = pre->GetId() ;
        if (pid && (pid->IsInterfacePort() || pid->IsInterfaceInst()) ) {
            is_prefix_interface_port = 1 ;
            break ;
        }
        pre = pre->GetPrefix() ;
    }

    // Check if the suffix is a struct/union member.
    // If so, apply environment checks to the prefix (if it is not a struct member itself)
    // This way, only real variables get their environment checked.
    if (_suffix_id && _suffix_id->IsMember() && !_prefix_id->IsMember() && !is_prefix_interface_port) {
        _prefix_id->CheckEnvironment(environment, /*from*/this) ;
    }

    if (_suffix_id) {
        _suffix_id->CheckEnvironment(environment, this, is_prefix_interface_port) ;
    }

    // VIPER #8469: Cannot select inside a type using dot (.). Must use scoping (::):
    if (_prefix && _prefix_id->IsType() && !_prefix_id->IsCovergroup() && !_prefix->GetToken() && !_function_type) {
        // Skip keywords like 'this', 'super' etc which are resolved to corresponding type ids.
        // Also skip any built-in method calls, they can be referred for enum types.
        _prefix->Error("cannot select inside %s using hierarchical reference", _prefix_id->Name()) ;
    }

    // We cannot do anything here if this name seems to be a 'method',
    // since it might be user-declared (field in an (so far unresolved) interface or so).
    // We need to wait until AnalyzeFull() before we know if the function is really not user-declared.

    // VIPER #3073 : Reset function type if suffix id has been resolved.
    if (_suffix_id) _function_type = 0 ;
    if (!_suffix_id && !_function_type) {
        // Cannot find a suffix identifier.
        // If prefix is an instance, then we cannot error out during analysis since instantiated module does not exist yet.
        if (_prefix_id->IsInst()) return ;
        // SV : Also be silent if prefix_id is a 'interface' port, since interfaces can be used before defined.
        if (_prefix_id->IsInterfacePort()) return ; // no built-in type and no actual type : interface port ? FIX ME : keep in line with bison rules

        // Otherwise, error out :
        // VIPER #3904: Do not error out here. Erroring out will be illegal for AMS specific potential
        // access as well as access to the member of an yet to be defined base class (VIPER #3828):
        //Error("%s is not declared under prefix %s", _suffix, _prefix_id->Name()) ;
    }
}

void VeriIndexedId::ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index)
{
    if (_prefix) _prefix->ResolvePrefix(scope, environment, 0) ;

    // Now pick up the identifier of the prefix :
    _id = (_prefix) ? _prefix->GetId() : 0 ;

    if (!_idx) return ;

    // Ranged index should not be allowed in a prefix :
    if (_idx->IsRange()) {
        if (allow_range_index && InRelaxedCheckingMode()) {
            _idx->Warning("range is not allowed in a prefix") ;
        } else {
            _idx->Error("range is not allowed in a prefix") ;
        }
    }

    // If this is a net lvalue, then range must be constant (LRM A.8.5)
    // VIPER 2543 : If it a port connection to a trans gate, range must be constant
    // VIPER #5166: If the id being indexed is instance or interface port, index expr must be constant:
    // But for interface type ports, index may be non-constant for 'virtual' qualified ports, so discard it:
    if ((environment==VERI_NET_LVALUE) || (environment==VERI_NET_LVALUE_UNDECL_USE_GOOD) || (environment==VERI_GATE_INOUT_PORT_CONNECTION) ||
        (_id && (_id->IsInst() || (_id->IsInterfacePort() && !_id->IsVirtualInterface())))) {
        _idx->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    } else {
        // otherwise, just resolve as normal expression.
        _idx->Resolve(scope, VERI_EXPRESSION) ;
    }
}

void VeriIndexedMemoryId::ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned allow_range_index)
{
    if (_prefix) _prefix->ResolvePrefix(scope, environment, 0) ;

    // Now pick up the identifier of the prefix :
    _id = (_prefix) ? _prefix->GetId() : 0 ;

    // Resolve all indexes :
    unsigned last_index = (_indexes) ? _indexes->Size() : 0 ;
    if (last_index) last_index-- ;
    unsigned i ;
    VeriExpression *idx ;
    FOREACH_ARRAY_ITEM(_indexes, i, idx) {
        if (!idx) continue ;

        // Ranged index should not be allowed in a prefix :
        if (idx->IsRange()) {
            if (allow_range_index && InRelaxedCheckingMode() && (last_index == i)) {
                idx->Warning("range is not allowed in a prefix") ;
            } else {
                idx->Error("range is not allowed in a prefix") ;
            }
        }

        // If this is a net lvalue, then range must be constant (LRM A.8.5)
        // VIPER 2543 : If it a port connection to a trans gate, range must be constant
        if (environment==VERI_NET_LVALUE || (environment==VERI_NET_LVALUE_UNDECL_USE_GOOD) || environment==VERI_GATE_INOUT_PORT_CONNECTION) {
            idx->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
        } else {
            // otherwise, just resolve as normal expression.
            idx->Resolve(scope, VERI_EXPRESSION) ;
        }
    }
}

void VeriFunctionCall::ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned /*allow_range_index*/)
{
    if (_func_name) _func_name->ResolvePrefix(scope, VERI_UPWARD_SCOPE_NAME, 0) ; // Can be upward reference name

    // Pick-up the identifier (if there) :
    VeriIdDef *func_id = (_func_name) ? _func_name->GetId() : 0 ;

    // Check if this is appropriate for a function call :
    if (func_id && !func_id->IsFunction()) {
        if (!func_id->IsSequence() && !func_id->IsProperty())
            Error("%s is not a function", func_id->Name()) ;
    }
    // Pick up the formals :
    Array *formals = (func_id) ? func_id->GetPorts() : 0 ;
    // Viper 5216. For covergroup pick the ports from moduleitem.
    if (func_id && func_id->IsCovergroup()) {
        VeriModuleItem *cover_grp = func_id->GetModuleItem() ;
        formals = cover_grp ? cover_grp->GetPorts() : 0 ;
    }
    VeriScope *formal_scope = (func_id) ? func_id->LocalScope() : 0 ;
    // Just resolve the arguments FIXME : Need to do some checks by extracting formal info
    unsigned i ;
    VeriExpression *arg ;
    VeriIdDef *formal ;
    unsigned named_formal = 0 ;
    FOREACH_ARRAY_ITEM(_args, i, arg) {
        // Find the formal
        formal = 0 ;
        if (arg && arg->NamedFormal()) {
            named_formal = 1 ;
            // This is a (SV-style) named formal.
            formal = (formal_scope) ? formal_scope->FindLocal(arg->NamedFormal()) : 0 ;
            if (func_id && !formal) {
                arg->Error("%s has no port called %s",func_id->Name(),arg->NamedFormal()) ;
                func_id->Info("%s is declared here",func_id->Name()) ;
            }
            if (formal && !formal->IsPort()) {
                arg->Error("no definition for port %s", formal->Name()) ; // VIPER #8172
                formal = 0 ;
            }
        } else {
            // Viper 7917: Viper 4618 did not performed the check if the expr is NULL.
            // VIPER #4618 : all the positional arguments must come before named arguments
            if (named_formal) {
                if (arg) {
                    arg->Error("positional arguments must occur before named arguments") ;
                } else {
                    Error("positional arguments must occur before named arguments") ;
                }
            }
            // This is a ordered formal
            if (i>=((formals) ? formals->Size() : 0)) {
                if (func_id) Error("%s expects %d arguments", func_id->Name(), ((formals) ? formals->Size() : 0)) ;
                    // no use checking more. No use resolving either, since these actuals are obsolete.
                if (func_id) break ; // Don't bail out if identifier was not resolved. We still want to resolve a bit.
            } else {
                formal = (formals && formals->Size() > i) ? (VeriIdDef*)formals->At(i) : 0 ;
            }
        }
        if (func_id && formal && (func_id->IsSequence() || func_id->IsProperty() || func_id->IsLetId())) formal->SetActual(arg) ;
        // VIPER issue 2303 : make sure to pass the current environment through to 'input' arguments...
        // This is because it could be a variety of environments, including 'property', 'sequence', 'expression', 'event' etc.
        veri_environment local_environment = VERI_UNDEF_ENV ;
        if (formal) {
            switch(formal->Dir()) {
            case VERI_OUTPUT :  local_environment = VERI_REG_LVALUE ; break ; // reg assignment ?
            case VERI_INOUT :   local_environment = VERI_REG_LVALUE ; break ;
            default : break ;
            }
        }

        // VIPER #4416: If formal is not present, it may be the case that formal
        // can be virtual interface. So pass VERI_PORT_CONNECTION to resolve actual
        if (!formal) local_environment = VERI_PORT_CONNECTION ;
        switch (environment) {
            case VERI_PROPERTY_ENV :
            case VERI_SEQUENCE_ENV :
                local_environment = VERI_EVENT_CONTROL ;
                if (func_id && func_id->IsProperty()) local_environment = VERI_PROPERTY_ARG ;
                if (func_id && func_id->IsSequence()) local_environment = VERI_SEQUENCE_ARG ;
                break;
            case VERI_CLASS_NAME : // Resolving argument list of base class specification
                local_environment = VERI_EXPRESSION ;
                break ;
            default :
                break;
        }
        // If the type of formal is unresolved/type parameter type, then also VERI_PORT_CONNECTION
        // environment should pass to resolve actual
        VeriTypeInfo *formal_type = (formal && !formal->IsOutput() && !formal->IsInout()) ? formal->CreateType(0): 0 ;
        if (formal_type && (formal_type->IsTypeParameterType() || formal_type->IsUnresolvedNameType() || formal_type->IsVirtualInterface())) local_environment = VERI_PORT_CONNECTION ;
        delete formal_type ;
        if (formal && formal->IsVirtualInterface()) local_environment = VERI_PORT_CONNECTION ;
        if (arg) arg->Resolve(scope, local_environment) ; // allow anything as argument
    }
    // Function call is created for sequence and property instance, check that
    VeriModuleItem *body = (func_id) ? func_id->GetModuleItem() : 0 ;
    if (func_id && body) {
        // VIPER #3370: Resolve the sequence/property declarations with the actual arguments:
        if (func_id->IsSequence() || func_id->IsProperty() || func_id->IsLetId()) {
            body->Resolve(scope, environment) ;
            FOREACH_ARRAY_ITEM(formals, i, formal) if (formal) formal->SetActual(0) ;
        }
    }
}

void VeriIdRef::Resolve(VeriScope *scope, veri_environment environment)
{
    // Direct access of a single identifier...

    const char *name = GetName() ;

    if (environment==VERI_COVERPOINT_ENV) {
        // If environment is VERI_COVERPOINT_ENV, it is needed to discard block ids defined
        // upto covergroup's scope. After that normal scoping rule can be applied. (1800-2009 19.5)
        VeriScope *curr_scope = scope ;
        VeriIdDef *id = 0 ;
        VeriIdDef *curr_scope_owner = 0 ;
        while (curr_scope && !id) {
            id = curr_scope->FindLocal(name) ;
            if (id && id->IsBlock()) id = 0 ;
            curr_scope_owner = curr_scope->GetOwner() ;
            curr_scope = curr_scope->Upper() ;
            if (curr_scope_owner && curr_scope_owner->IsCovergroup()) break ;
        }
        if (id) {
            _id = id ;
        } else {
            _id = curr_scope ? curr_scope->Find(name, /*from*/this): 0 ;
        }
    }
    // VIPER #6919 : Find whether 'name' is illegally imported
    if (scope) (void) scope->FindImplicitlyImportedErrorCond(name, this) ;
    // If environment is a PORT_EXPRESSION (where references are made before declaration),
    // then make sure that any id resolved during parsing gets re-set (and looked up in this final scope)
    if (_id && (environment==VERI_PORT_EXPRESSION || environment==VERI_PATH_EXPRESSION)) SetId(0) ;

    // Resolve the identifier as normal (if we have a scope and a name and no previous setting yet).
    // VIPER #6337 : Do not try to find identifier if this is part of default actual
    // argument of sequence/property port. It can only refer already declared identifier
    if (!_id && (environment != VERI_SEQ_PROP_INIT) && scope && name) _id = scope->Find(name, /*from*/this) ;

    // VIPER #6487 : Reference of named port should be discarded
    if (_id && _id->IsNamedPort()) SetId(0) ;

    if (_id && environment == VERI_TYPE_ENV && !_id->IsType() && !_id->IsInterface() && !_id->IsModport() && !_id->IsClass() && !_id->IsCovergroup()) {
        // Reset _id if it is of type var because we have picked up a wrong iddef.
        // For example if type name and variable name are same. Viper 5162
        // Viper 5961 : create the name as it was in _id->_name
        SetId(0) ;
    }

    // VIPER #4924: Function/task can also be used before their definition. So
    // it may be the case that function/task call is already resolved with an
    // incorrect identifier. So when environment is VERI_UPWARD_SCOPE_NAME, try
    // to re-resolve the identifier reference. If we can get new identifier, use
    // that, otherwise keep old one. VERI_UPWARD_SCOPE_NAME environment is also
    // used to resolve name of instantiated module in instantiation. For instantiation
    // it may be the case that from yacc instantiated module name is resolved, but
    // when we are trying to re-resolve, we may find null. So keep previous one for that.
    if (environment==VERI_UPWARD_SCOPE_NAME && scope) {
        VeriIdDef *prev_id = _id ;
        _id = scope->Find(name, /*from*/this) ;
        if (!_id) _id = prev_id ;
    }

    if (!_id && name) {
        // Here, direct reference to a single identifier.
        // error out if this identifier is not declared..

        // Don't error out for tasks, functions, and named blocks, since they can do
        // single-id upward name referencing (LRM 12.5), and that cannot be resolved during analysis.  (VIPER #1591)
        if (environment != VERI_UPWARD_SCOPE_NAME &&
            // Inline constraint block (randomize() ..with) can use member variables of
            // object class and also local variables. So member variables of object class
            // referencing cannot be resolved during analysis.
            environment != VERI_INLINE_CONSTRAINT_EXPR &&
            environment != VERI_SYSTEM_TASK_ARG &&
            environment != VERI_TYPE_ENV &&
            environment != VERI_COVERPOINT_ENV &&
            environment != VERI_UNDEF_ENV) { // undef env should not produce any error
            // 'interface' type on module ports might be used before defined. Check after analysis.
            // Simple identifiers should have been declared in the module.
            // So error out if we cant find it now. LRM 12.6 scope rules...
            Error("%s is not declared", name) ;
        }
    }
//#ifdef VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK
    if (RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block")) {
        // Cannot resolve to unnamed generate block, it is not known whether it is active or not
        if (_id && _id->IsUnnamedBlock()) SetId(0) ;
    }
//#endif

    // VIPER #8342: Do not resolve to modules/packages in non-SV mode other than the current module:
    if (_id && _id->IsModule() && !IsSystemVeri() && scope && (_id != scope->GetContainingModule())) SetId(0) ;

    if (!_id) return ;

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ; _name = 0 ;
#endif
#endif

    // VIPER 5871: for ranges in bins declaration of covergroup it may contain
    // constant expressions and non ref arguments of the cover group.
    // This is according to LRM IEEE p1800 2005 Section 18.4
    if (_id->IsAnsiPort() && _id->Dir()!= VERI_REF && environment == VERI_BINS_INIT_VALUE) {
        VeriScope *curr_scope = scope ;
        VeriModuleItem *cov_grp = 0 ;
        while(curr_scope) {
            VeriIdDef *owner = curr_scope->GetOwner() ;
            if (owner && owner->IsCovergroup()) {
                cov_grp = owner->GetModuleItem() ;
                break ;
            }
            curr_scope = curr_scope->Upper() ;
        }
        environment = VERI_PARAM_VALUE ;
        if (cov_grp) {
            Array *ports = cov_grp->GetPorts() ;
            VeriIdDef *port ;
            unsigned i ;
            FOREACH_ARRAY_ITEM(ports, i, port) {
                if (port == _id) {
                    environment = VERI_BINS_INIT_VALUE ;
                    break ;
                }
            }
        }
    }

    // Check the usage of this identifier in this environment
    _id->CheckEnvironment(environment, /*from*/this) ;

    // VIPER issue 2729 : check that function id is not used by itself, other than inside a function with the same name.
    // We do not have the scope available withing 'CheckEnvironment', else we should put this check there.
    // Note that issue 2767 pretty much excludes actual functions being called without ()'s. Only allowed for sequence and property calls.
    // So we could simplify this test to _id->IsFunction() && !_id->IsSequence() && !id->IsProperty(), without the scope check, and move this test into CheckEnvironment.
    // VIPER #5462 : Allow function with ports having default value as expression
    if (scope && (_id->IsFunction() || _id->IsLetId()) && !_id->CanCallWithoutArg() && //_id->GetPorts() && (_id->GetPorts()->Size()!=0) && // we are referencing a function that has ports
        (environment != VERI_UPWARD_SCOPE_NAME) && // we are not simply looking for a function name
        // VIPER #4575 & VIPER #5248 (test_36): Allow function id as system task argument.
        // CHECKME: We will not produce error for all system tasks, then.
        (environment != VERI_SYSTEM_TASK_ARG) &&
        (environment != VERI_UNDEF_ENV) &&
        (_id != scope->GetSubprogram()) ) { // we are not inside this function itself
        Error("%s is not valid in an expression", _id->Name()) ;
    }

    // Discard id as onehot here, unless we are in an event or delay control expression. There encoding does not matter.
    if (_id->CanBeOnehot() && (environment != VERI_UNDEF_ENV) && (environment != VERI_COVERPOINT_ENV) && (environment != VERI_EVENT_CONTROL) && (environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD) && (environment != VERI_SEQ_PROP_INIT) && (environment != VERI_SYSTEM_TASK_ARG) && (environment != VERI_DELAY_CONTROL)) _id->SetCannotBeOnehot() ;

    // Extra environment test for direct identifier reference :
    // These access checks are not needed for System Verilog (assignment/access of unpacked vars always allowed).
    // In ams memory can be accessed as actual of analog functions :
    if (_id->IsMemory() && !IsSystemVeri() && !IsAms()) {
        switch (environment) {
        case VERI_LVALUE :
        case VERI_NET_LVALUE :
        case VERI_NET_LVALUE_UNDECL_USE_GOOD :
        case VERI_REG_LVALUE :
        case VERI_FORCE_LVALUE :
        case VERI_GENVAR_LVALUE :
        case VERI_CONSTRUCTOR_LVALUE :
            Error("cannot assign to memory %s directly", _id->Name()) ;
            break ;
        case VERI_EXPRESSION :
        case VERI_EXPRESSION_UNDECL_USE_GOOD :
        case VERI_ABORT_EXPRESSION :
        case VERI_LET_EXPRESSION :
        case VERI_PROCE_CONT_ASSIGN_VAL : // VIPER #6110: value of RHS of procedural continuous assignments
        case VERI_WAIT_EXP :
        case VERI_STRING_TARGET : // for VIPER# 4259
        case VERI_CONSTANT_EXPRESSION :
        case VERI_INTEGRAL_CONSTANT : // integral constant is allowed where constant required
        case VERI_PORT_CONNECTION :
        case VERI_GATE_INOUT_PORT_CONNECTION : // VIPER 2543 : Actual of primitive trans gate cann't be memory
            Error("cannot access memory %s directly", _id->Name()) ;
            break ;
        default : break ;
        }
    }

    // Function call is created for sequence and property instance, check that
    if ((_id->IsSequence()  || _id->IsProperty() || _id->IsLetId()) && (environment != VERI_UPWARD_SCOPE_NAME)) {
        // VIPER #4081 : Report for unconnected ports :
        Array *formals = _id->GetPorts() ;
        unsigned i ;
        VeriIdDef *formal ;
        FOREACH_ARRAY_ITEM(formals, i, formal) { // Iterate over ports
            if (!formal || formal->GetInitialValue()) continue ; // Port having initial value is connected :
            // Port is not connected, produce error
            Error("port %s is not connected on %s call %s", formal->Name(), "function", _id->Name()) ;
        }
        VeriModuleItem *body = _id->GetModuleItem() ;
        // Viper 6361: if the environment is VERI_CLOCKING_BLOCK change the environment
        // as we no longer have an explicit clocking event.
        if (environment == VERI_CLOCKING_BLOCK) environment = _id->IsSequence() ? VERI_SEQUENCE_ENV : VERI_PROPERTY_ENV ;
        if (body) body->Resolve(scope, environment) ;
    }
    // VIPER #4043 : Check usage of private and protected members of a class
    // VIPER #7410 : Check for public members of class also
    if (_id->IsPrivate() || _id->IsProtected() || _id->IsAutomatic()) {
        _id->CheckUsageOfClassMember(scope, this) ;
    }
    // VIPER #7043 : If this is sequence/property formal, Resolve the associated
    // actual with coming environment
    if (_id->IsSequenceFormal() || _id->IsPropertyFormal()) {
        VeriExpression *actual = _id->GetActual() ;
        if (!_id->IsProcessing() && actual) {
            _id->SetIsProcessing() ; // Mark starting of processing
            // VIPER #7883 : Do not resolve identifier references of actual, so pass null scope
            actual->Resolve(0, environment) ;
            _id->ClearIsProcessing() ; // finished processing
        }
    }

    // VIPER #6506 : Implicit non-ansi ports ports cannot be referenced :
    if (_id->IsImplicitNonAnsiPort()) {
        Error("%s is not valid in an expression", _id->Name()) ;
    }

    // Function call is created for sequence and property instance, check that
    VeriModuleItem *body = _id->GetModuleItem() ;
    if (body && body->IsFunctionDecl()) {
        // VIPER #7131: Set constant flag on the function in case it is called in constant environment:
        if ((environment == VERI_PARAM_VALUE) || (environment == VERI_CONSTANT_EXPRESSION) || (environment == VERI_PARAM_INITIAL_EXP) ||
            (environment == VERI_INTEGRAL_CONSTANT) || (environment == VERI_BINS_INIT_VALUE)) body->SetCalledAsConstFunction() ; // It is called as constant

        // VIPER #7131: Check whether this function is called from a constant environment
        // and whether it can be a potential constant function. If not, produce error:
        if (body->IsCalledAsConstFunction() && !body->CanBeConstFunction()) {
            Error("%s is not a valid constant function call", _id->Name()) ;
            // Do not error this out here - function may not be constant for many reasons:
            //body->Error("global or hierarchical references may not be used in a constant function") ;
        }
    }

    // Use before declaration check: VIPER #4379, 5609, 2775, 3121, 5712, 6151, 6599, 3503, 5604
    if (UsedBeforeDeclare(scope, environment)) Warning("identifier %s is used before its declaration", GetName()) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

unsigned VeriIdRef::UsedBeforeDeclare(const VeriScope *scope, veri_environment environment) const
{
    if (IsStaticElab()) return 0 ;

    if (!scope || !_id ||
        Message::ErrorCount() ||
        (environment == VERI_EVENT_CONTROL_UNDECL_USE_GOOD) ||
        (environment == VERI_SEQUENCE_ENV) ||
        (environment == VERI_PROPERTY_ENV) ||
        (environment == VERI_UPWARD_SCOPE_NAME) ||
        (environment == VERI_NET_LVALUE_UNDECL_USE_GOOD) ||
        (environment == VERI_EXPRESSION_UNDECL_USE_GOOD) ||
        (environment == VERI_PORT_CONNECTION && !_id->IsParam()) || // Viper #6152: check !param
        _id->IsPort() ||
        _id->IsFunction() ||
        _id->IsClocking() ||
        _id->IsIterator() ||
        _id->IsImplicitNet() ||
        !(_id->IsVar() || _id->IsParam()) ||
        !LineFile::LessThan(Linefile(), _id->Linefile())) {
        return 0 ;
    }

    // Following scope checks could be costly.. so defer till now
    VeriIdDef *seq_or_prop_id = 0 ;
    VeriIdDef *class_id = 0 ;
    class_id = scope->GetClass() ;
    seq_or_prop_id = scope->GetSequenceProperty() ;
    VeriIdDef *module_id = scope->GetContainingModule() ;
    VeriModule *module = module_id ? module_id->GetModule() : 0 ;
    if (!seq_or_prop_id && !class_id && module && !module->IsStaticElaborated()) return 1 ;

    return 0 ;
}

void VeriIndexedId::Resolve(VeriScope *scope, veri_environment environment)
{
    if (!_prefix) return ;

    // This can be an indexed name, or a part-select name.
    // Difference exposed if _idx->IsRange().

    // Resolve the prefix first.
    _prefix->ResolvePrefix(scope, environment, 0) ;

    // Now pick up the identifier of the prefix :
    _id = _prefix->GetId() ;

    // This is the 'top' of an indexed name usage.
    // We should always have an identifier here, unless the prefix is a hierarchical name :
    if (!_id && !_prefix->IsHierName()) {
        Error("%s is not declared",_prefix->GetName()) ;
    }

    // Check the usage of this identifier in this environment
    if (_id && !_prefix->IsHierName()) _id->CheckEnvironment(environment, /*from*/this) ;

    // Discard id as onehot here, unless we are in an event or delay control expression. There encoding does not matter.
    if (_id && _id->CanBeOnehot() && (environment != VERI_UNDEF_ENV) && (environment != VERI_COVERPOINT_ENV) && (environment != VERI_EVENT_CONTROL) && (environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD) && (environment != VERI_SEQ_PROP_INIT) && (environment != VERI_SYSTEM_TASK_ARG) && (environment != VERI_DELAY_CONTROL)) _id->SetCannotBeOnehot() ;

    // Discard id as a dual-port memory if this indexed name is used inside a loop.
    // Find a loop label in the scope :
    // Also check that we are actually in a expression that will be synthesized (so there is SOME environment).
    // Don't discard if there is no environment (if memory is used in a $display argument or other benign place)
    // RD: Note: since 1/2008, this code is ineffective, because Set/UnSet of DualPort RAM is done in VarUsage, not in Resolve() (reason: VIPER 3736).
#if 0
    if (_id && _id->CanBeDualPortRam() && scope && environment && (environment != VERI_SYSTEM_TASK_ARG)) {
        // Find loop label (for sequential loop or generate loop)
        VeriIdDef *loop_label = scope->Find(" loop_label") ;
        if (!loop_label) loop_label = scope->Find(" gen_loop_label") ;
        // Discard the id if inside a loop:
        if (loop_label) _id->SetCannotBeDualPortRam() ;
    }
#endif

    /*
    if (_id && !_id->IsType() && !_id->GetDimensionAt(0)) {
        // no array dimensions.
        // Check that the base type is single bit atomic type :
        // FIX ME : maybe should pull 'ElementSize()' into IdDef or expressions.
        switch (_id->Type()) {
        // All net types are 1 bit :
        case VERI_WIRE    :
        case VERI_TRI     :
        case VERI_TRI1    :
        case VERI_SUPPLY0 :
        case VERI_WAND    :
        case VERI_TRIAND  :
        case VERI_TRIOR   :
        case VERI_TRI0    :
        case VERI_SUPPLY1 :
        case VERI_WOR     :
        case VERI_TRIREG  :
        case VERI_UWIRE   :
        // VIPER #2960 side effect: don't allow indexing into chandle:
        case VERI_CHANDLE :
         // 1 bit Register types :
        case VERI_REG :
        case VERI_BIT :
        case VERI_LOGIC :
        case VERI_SHORTREAL : // Select on real not allowed
        // Real types :
        case VERI_REAL : // VIPER #2976 : Indexing on real not allowed
        case VERI_REALTIME :
            Warning("cannot index into non-array %s",_id->Name()) ;
            break ;
        default : break ;
        }
    }
    */

    // For memories in Verilog 2001 : We can still index a one-dimensional memory of single-bits, but no more.
    // This test is not required for System Verilog (multi-dim unpacked arrays can be accessed with one index).
    if (_id && _id->UnPackedDimension()>1 && !IsSystemVeri()) {
        Error("%s needs %d dimensions", _id->Name(), _id->Dimension()) ;
    }

    // Extra checks if this is a part-select.
    if (_idx && _idx->IsRange()) {
        // Verilog 95/2001 : Memory cannot be part-selected.
        // In System Verilog, this is always allowed.
        // In branch terminal part select of memory is allowed
        if (_id && _id->IsMemory() && !IsSystemVeri() && (environment != VERI_BRANCH_TERM)) {
            Error("part-select of memory %s is not allowed", _id->Name()) ;
        }
    }
    // VIPER 2473 : Issue error if bits are selected from an unpacked structure/union
    // Covered by CheckType
    //if (_id && !_id->UnPackedDimension() && _id->IsUnpacked() && ((_id->Type() == VERI_STRUCT) || (_id->Type() == VERI_UNION))) {
        //Error("illegal select on unpacked structure/union type variable %s", _id->Name()) ;
    //}

#if 0
// RD: 6/02 : silly rule. Does not always seem to hold either..
    // If environment is 'CONSTANT', indexed naming is not allowed ! (A.8.4)
    if (environment==VERI_CONSTANT_EXPRESSION) {
        // Make it a warning, since many user designs seem to have it
        Warning("indexing (of %s) is not allowed in a constant expression", (_id) ? _id->Name() : "indexed name") ;
    }
#endif

    // The type calculation is moved from Viper 3499 specific fix area to here.
    // It is now also used to pass correct environment for queues in VIPER 4476
    VeriTypeInfo *prefix_type = (_prefix) ? _prefix->CheckType(0 /* no expected type */, NO_ENV) : 0 ;
    // If this is a net lvalue, then range must be constant (LRM A.8.5)
    // VIPER 2543 : If it a port connection to a trans gate, range must be constant
    // VIPER #5166: If the id being indexed is instance or interface port, index expr must be constant:
    // But for interface type ports, index may be non-constant for 'virtual' qualified ports, so discard it:
    if (_idx && (environment==VERI_NET_LVALUE || (environment==VERI_NET_LVALUE_UNDECL_USE_GOOD) || environment==VERI_GATE_INOUT_PORT_CONNECTION ||
        (_id && (_id->IsInst() || (_id->IsInterfacePort() && !_id->IsVirtualInterface()))))) {
        _idx->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
        _idx->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    } else if (_id && _idx && environment==VERI_FORCE_LVALUE) {
        // VIPER 2948 : If this is in LHS of 'force' statement, range or index must be constant.
        if (_id->IsNet()) {
            _idx->Resolve(scope,VERI_CONSTANT_EXPRESSION) ;
        } else {
            // VIPER 2948 : if this is LHS of 'force' statement, then if prefix is a variable (non-net), this is always an error
            Error("bit-select or part-select is not allowed in a %s statement for non-net %s", "force", _id->Name()) ;
        }
    } else if (_idx) {
        // otherwise, just resolve as normal expression.
        veri_environment temp_environment = VERI_EXPRESSION ;
        // VIPER 4476 for Queue we pass VERI_VALUE_RANGE as the range can be expression
        // VIPER #5717 : If prefix is unresolved name type or type parameter type, use VERI_VALUE_RANGE
        // environment to resolve indes
        if (_idx->IsRange() && prefix_type && (prefix_type->IsQueue() || prefix_type->IsUnresolvedNameType() || prefix_type->IsTypeParameterType())) temp_environment = VERI_VALUE_RANGE ;
        _idx->Resolve(scope, temp_environment) ;
    }
    // VIPER #4104 : Call CheckType on it after resolving the index expression
    // Here, do specific checks, knowing we are from a VeriIndexedId.

    // Viper 1485 and 2326 : Test indexing into a scalar of 1 bit is a warning :
    // VIPER #2709: Do not check/warn this for type-ids:
    if (_id && !_id->IsType()) {
        // Check validity of this indexing :
        VeriTypeInfo *this_type = CheckType(0, NO_ENV) ;
        delete this_type ;
    }

    // VIPER #3499: Check for $ in indexing for prefix other than queue:
    if (_idx && _idx->HasDollar()) {
        // VIPER #5177 : Do not produce error if prefix is unresolved type.
        if (prefix_type && !prefix_type->IsQueue() && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType()) _idx->Error("illegal context for $") ;
    }
    delete prefix_type ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSelectedName::Resolve(VeriScope *scope, veri_environment environment)
{
    // Cant do anything without a prefix or suffix :
    if (!_prefix || !_suffix) return ;

    _suffix_id = 0 ; // VIPER #3370 : Set suffix_id to 0. This routine will set it always

    // Resolve the prefix first.
    // VIPER #5465: Prefix can refer identifier declared later or in upper scope,
    // so use 'VERI_UPWARD_SCOPE_NAME' environment
    // VIPER #5248 (test_85): Allow range in the prefix for array ordering method "reverse" under compatibility mode:
    unsigned allow_range_in_prefix = 0 ;
    if (_function_type==VERI_METHOD_REVERSE) allow_range_in_prefix = 1 ;
    _prefix->ResolvePrefix(scope, _prefix->IsFunctionCall() ? environment: VERI_UPWARD_SCOPE_NAME, allow_range_in_prefix) ;

    // Now check if we can pick up the identifier of the prefix :
    _prefix_id = _prefix->GetId() ;

    // Cant do much without a prefix identifier.
    if (!_prefix_id) {
        // LRM A.8.4 : Issue 1636 : constant primary cannot contain a hierarchical name :
        if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
            // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
            Warning("constant expression cannot contain a hierarchical identifier") ;
        }
        return ;
    }

    // Check if we are about to point into a generate-for loop.
    VeriModuleItem *prefix_module_item = _prefix_id->GetModuleItem() ;
    if ((prefix_module_item && (prefix_module_item->IsGenerateFor() || prefix_module_item->IsGenerateBlock())) || _prefix_id->IsUnnamedBlock()) {
        // Do NOT try to resolve an identifier declared inside a generate-for loop.
        // It would not identify a unique identifier, since inside generate-for loops we
        // will 'generate' new identifiers.
        // This is similar to trying to resolve an identifier via a instance, or into a separate module.
        _function_type = 0 ;
        // LRM A.8.4 : Issue 1636 : constant primary cannot contain a hierarchical name :
        if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
            // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
            Warning("constant expression cannot contain a hierarchical identifier") ;
        }
        return ;
    }

    // VIPER #2954: Do not resolve a suffix defined under an (packed/unpacked) array
    // for which the prefix is not an indexed name. This should not be allowed:
    // VIPER #2980: Match the number of packed/unpacked dimension in declaration and
    // dimension in the prefix. We will not resolve the suffix if they don't match:
    if ((_prefix_id->IsVar() || _prefix_id->IsInst()) && !_function_type) {
        // First get the number of dimensions in the prefix of this hier identifier:
        unsigned select_dims = 0 ;
        if (_prefix->GetIndexExpr()) {
            select_dims = 1 ; // VeriIndexedId with only one dimension
        } else {
            Array *indices = _prefix->GetIndexes() ;
            if (indices) select_dims = indices->Size() ; // VeriIndexedMemoryId with multiple dimensions
        }

        // Now get the number of dimensions that must be indexed to refer elements declared
        // inside the prefix. This is unpacked dimensions and packed/unpacked dimenisons for
        // structure and union type variables:
        // Make it same as 'select_dims' so that we do not error out in case we could not create id-type below:
        unsigned declare_dims = select_dims ;

        // VIPER #4096 : Use created type instead of data type back pointer to
        // check if selected name is legal or not.
        VeriTypeInfo *datatype = _prefix_id->CreateType(0) ;
        if (datatype) {
            declare_dims = datatype->UnpackedDimension() ;
            VeriTypeInfo *base_type = datatype->BaseType() ;
            if (base_type && (base_type->IsStructure() || base_type->IsUnion())) {
                // Add the packed dimensions only for the structure/unions:
                declare_dims = declare_dims + datatype->PackedDimension() ;
            } else if (datatype->IsTypeParameterType() || datatype->IsUnresolvedNameType()) {
                // This data type is a type parameter, set 'declare_dims' such that we don't error out.
                // We cannot fully check for dimension in case of type parameters, so better be silent:
                declare_dims = select_dims ;
            }
        }
        delete datatype ;

        // Check that the two types of dimensions matches for this hier-id:
        if (select_dims != declare_dims) {
            Warning("cannot select %s inside %s due to dimension mismatch", _suffix, _prefix->GetName()) ;
            // LRM A.8.4 : Issue 1636 : constant primary cannot contain a hierarchical name :
            if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
                // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
                Warning("constant expression cannot contain a hierarchical identifier") ;
            }
            return ; // Don't resolve the suffix here
        }
    }

    // Look for the suffix identifier.
    // VIPER #7147: If prefix is a function, check for member of function return type:
    //if (_prefix_id->IsFunction()) {
        //VeriDataType *fn_type = _prefix_id->GetDataType() ;
        //if (fn_type) _suffix_id = fn_type->GetMember(_suffix) ;
    //}

    // First, check if this is a selected name of Verilog 95/2001 style.
    // In that case, we need to look in the prefix scope :
    VeriScope *prefix_scope = _prefix_id->LocalScope() ;
    if (IsSystemVeri2009OrLater() && !prefix_scope && _prefix_id->IsLabel()) {
        VeriModuleItem *body = _prefix_id->GetModuleItem() ;
        // VIPER #4881: This may be a statement label, get the scope via its module item (statement):
        if (body) prefix_scope = body->GetScope() ;
    }

    // Find the suffix local in that scope :
    // VIPER #7370 : It will be done after data type checking
    //if (!_suffix_id && prefix_scope) {
        //_suffix_id = prefix_scope->FindLocal(_suffix) ;
        // VIPER #3327 : If '_suffix' is declared in unnamed child scope, return
        // It may be declared in multiple paths
        // VIPER #5588: Moved below so that normal ids get resolved as usual (1).
        //if (!_suffix_id && prefix_scope->FindInUnnamedChildScopes(_suffix)) return ;
    //}

    unsigned is_prefix_interface_port = 0 ;
    // If that did not work, check if we are dealing with an instantiation :
    //if (!_suffix_id && _prefix_id->IsInst()) {
    if (_prefix_id->IsInst()) {
        // Look through the instantiation for scope-resolved nested module instance or class instance.
        // Needed for direct selected name into an instance (of a interface or class or so)
        VeriModuleInstantiation *instance = _prefix_id->GetModuleInstance() ;
        VeriName *unit_name = (instance) ? instance->GetModuleNameRef() : 0 ;
        VeriIdDef *unit_id = (unit_name) ? unit_name->GetId() : 0 ;
        // Get the scope of this unit :
        prefix_scope = (unit_id) ? unit_id->LocalScope() : 0 ;
        // Find the suffix local in that scope :
        if (prefix_scope) {
            _suffix_id = prefix_scope->FindLocal(_suffix) ;
            // VIPER #3327 : If '_suffix' is declared in unnamed child scope, return
            // It may be declared in multiple paths
            // VIPER #5588: Moved below so that normal ids get resolved as usual (2).
            //if (!_suffix_id && prefix_scope->FindInUnnamedChildScopes(_suffix)) return ;
        }
    }

    // If the prefix  (such as a function id) has a scope, then we still might need to look in the datatype.
    // see example 10.3/strReturn1 of an unnamed struct data type on a function.
    // So just check if we resolved the suffix id. If not, then we will check the data type.
    if (!_suffix_id) {
        // If the identifier itself is a variable,
        // we could find the suffix in the data type of the identifier.
        // Works for variables where the data type is a class or struct/union.
        // Also works for ports where the data type denotes a interface, or modport.
        VeriTypeInfo *data_type = _prefix->CheckType(0, NO_ENV) ;
        // Data type is defined by type parameter or unresolved name
        if (data_type && (data_type->IsTypeParameterType() || data_type->IsUnresolvedNameType())) {
            delete data_type ;
            // LRM A.8.4 : Issue 1636 : constant primary cannot contain a hierarchical name :
            if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
                // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
                Warning("constant expression cannot contain a hierarchical identifier") ;
            }
            return ;
        }

        // Check for member in the data type :
        // This includes any reference to class/type/ or interface.
        if (data_type) _suffix_id = data_type->GetMember(_suffix) ;
        if (!_suffix_id && data_type) {
            // VIPER #7390: Type and enum literals are visible via modport even if they are not exported:
            VeriIdDef *named_id = data_type->GetNameId() ;
            VeriScope *lscope = (named_id && named_id->IsModport()) ? named_id->LocalScope() : 0 ;
            if (lscope) lscope = lscope->ModuleScope() ; // Get the interface scope
            if (lscope) _suffix_id = lscope->FindLocal(_suffix) ; // Find the suffix from interface scope
            // Ignore non-type, non-enum literals identifiers found above:
            // VIPER #8093: IEEE 1800-2012 LRM section 25.10 says objects that
            // are not permissible to be listed in a modport shall remain
            // accessible
            if (_suffix_id && !_suffix_id->CanBeAccessedAlways()) _suffix_id = 0 ;
            //if (_suffix_id && !_suffix_id->IsType() && (!_suffix_id->IsParam() || (_suffix_id->Type() != VERI_ENUM))) _suffix_id = 0 ;
        }
        delete data_type ;

        // Check if 'data_type' of '_prefix_id' is specified by type parameter.
        // If so do not resolve '_suffix', type identifier can be overwritten by another data type
        //if (!_suffix_id && data_type) {
            //VeriIdDef *type_id = data_type->GetId() ;
            //if (type_id && type_id->IsParam()) return ;
        //}
    }
    // VIPER #7370: Check in prefix scope after prefix's data type
    // Find the suffix local in that scope :
    if (!_suffix_id && prefix_scope) {
        _suffix_id = prefix_scope->FindLocal(_suffix) ;
    }

    // VIPER #5588: Move (1) & (2) down here so that normal ids get resolved as usual.
    // Prefix scope would have been available in either of (1) or (2) above. This is
    // because if prefix-id has local scope it cannot be an instance-id as instance-ids
    // do not have local scope. So, any one of (1) or (2) would have been active and that
    // still holds for this position down here. So, here it satisfies both the two above.
    // VIPER #3327 : If '_suffix' is declared in unnamed child scope, return
    // It may be declared in multiple paths which is only usable in elaborator.
    if (!_suffix_id && prefix_scope && prefix_scope->FindInUnnamedChildScopes(_suffix)) {
        // LRM A.8.4 : Issue 1636 : constant primary cannot contain a hierarchical name :
        if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
            // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
            Warning("constant expression cannot contain a hierarchical identifier") ;
        }
        return ;
    }

    // VIPER #3736 : If _suffix_id is an object defined within interface and used
    // in a module where that interface is a port, do not set the assigned/used field
    VeriName *pre = _prefix ;
    VeriIdDef *pid ;
    while(pre) {
        pid = pre->GetId() ;
        if (pid && (pid->IsInterfacePort() || pid->IsInterfaceInst()) ) {
            is_prefix_interface_port = 1 ;
            break ;
        }
        pre = pre->GetPrefix() ;
    }
    // If suffix id is modport, prefix must be interface/interface-instance/interface-port
    // from containing module
    if (_suffix_id && _suffix_id->IsModport() && (environment == VERI_TYPE_ENV) && _prefix_id->IsVar()) {
        if (scope && scope->Find(_prefix_id->Name(), this)==_prefix_id) {
            // Prefix is variable/type defined in this scope, so this is not
            // resolved properly, Reset _prefix_id
            _prefix_id = 0 ;
            _prefix->SetId(0) ;
            return ;
        }
    }

    // Check if the suffix is a struct/union member.
    // If so, apply environment checks to the prefix (if it is not a struct member itself)
    // This way, only real variables get their environment checked.
    // VIPER #3736 : Do not check suffix if it is member of struct/union defined within interface
    if (_suffix_id && _suffix_id->IsMember() && !_prefix_id->IsMember() && !is_prefix_interface_port) {
        _prefix_id->CheckEnvironment(environment, /*from*/this) ;
    }

    // We cannot do anything here if this name seems to be a 'method',
    // since it might be user-declared (field in an (so far unresolved) interface or so).
    // We need to wait until AnalyzeFull() before we know if the function is really not user-declared.

    // VIPER #3073 : Reset function type if suffix id has been resolved.
    if (_suffix_id) _function_type = 0 ;

    if (!_suffix_id && !_function_type) {
        // Cannot find a suffix identifier.
        // If prefix is an instance, then we cannot error out during analysis since instantiated module does not exist yet.
        if (_prefix_id->IsInst()) {
            if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
                // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
                Warning("constant expression cannot contain a hierarchical identifier") ;
            }
            return ;
        }
        // SV : Also be silent if prefix_id is a 'interface' port, since interfaces can be used before defined.
        if (_prefix_id->IsInterfacePort()) {
            if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
                // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
                Warning("constant expression cannot contain a hierarchical identifier") ;
            }
            return ; // generic interface port FIX ME : keep in line with bison rules.
        }

        // Otherwise, error out :
        // VIPER #3904: Do not error out here. Erroring out will be illegal for AMS specific potential
        // access as well as access to the member of an yet to be defined base class (VIPER #3828):
        //Error("%s is not declared under prefix %s", _suffix, _prefix_id->Name()) ;
    }

    // VIPER #3191 : Signals declared within clocking block can only be driven with
    // a non-blocking assignment. Moreover both nettype and reg type are allowed as lvalue
    if (_suffix_id && _prefix_id->IsClocking() && ((environment == VERI_REG_LVALUE) || (environment == VERI_NET_LVALUE) || (environment==VERI_NET_LVALUE_UNDECL_USE_GOOD) || (environment == VERI_CONSTRUCTOR_LVALUE))) {
        // '_suffix_id' is declared within clocking block, issue error if it is not
        // non-blocking assignment
        // FIXME : environment 'VERI_REG_LVALUE' comes both for blocking and non-blocking assignment.
        // So cannot issue error for blocking assignment. Test is done in VarUsage
        if (environment == VERI_NET_LVALUE || (environment==VERI_NET_LVALUE_UNDECL_USE_GOOD)) {
            Error("clocking block signal %s can be driven only with a non-blocking assignment", _suffix_id->Name()) ;
        }
        // Change action to process _suffix_id to avoid error issuing for net assignment
        // in procedural area
        environment = VERI_LVALUE; // FIXME : Set environment VERI_LVALUE. Should be VERI_CLOCKING_DRIVE ?
    }
    // Check the environment in which this identifier is used :
    // VIPER #4039, #4863 : Instance constant can be assigned inside the constructor of

    // class. So if we are here processing the body of constructor, pass a different
    // environment :
    // VIPER #5481 : Passed VERI_CONSTRUCTOR_LVALUE from blocking assign
    /*
    if (environment == VERI_REG_LVALUE) {
        VeriIdDef *owner = (scope) ? scope->GetSubprogram(): 0 ;
        if (IsSystemVeri() && owner && owner->IsFunction() && Strings::compare_nocase(owner->Name(), "new")) {
            environment = VERI_CONSTRUCTOR_LVALUE ;
        }
    }
    */

    // Viper 6984 : Issue Warning for acces of interface members using interface name.
    // VIPER #8351 : Produced warning if any object defined within interface is accessed.
    // FIXME : Allowing modport, task, function ?
    if (_suffix_id && _prefix_id->IsInterface() && !_suffix_id->IsModport() && !_suffix_id->IsInst() && !_suffix_id->IsModule() &&
                  /* _suffix_id->IsVar() &&*/ !_suffix_id->IsFunction() && !_suffix_id->IsTask()) {
        Warning("illegal reference of %s inside interface %s", _suffix_id->GetName(), _prefix_id->GetName()) ;
    }

    if (_suffix_id) {
        _suffix_id->CheckEnvironment(environment, this, is_prefix_interface_port) ;
    }

    // VIPER #3846: Cannot select inside a type using dot (.). Must use scoping (::):
    if (_prefix_id->IsType() && !_prefix_id->IsCovergroup() && !_prefix->GetToken() && !_function_type) {
        // Skip keywords like 'this', 'super' etc which are resolved to corresponding type ids.
        // Also skip any built-in method calls, they can be referred for enum types.
        _prefix->Error("cannot select inside %s using hierarchical reference", _prefix_id->Name()) ;
    }

    // Discard resolved id as onehot here, unless we are in an event or delay control expression. There encoding does not matter.
    if (_suffix_id && _suffix_id->CanBeOnehot() && (environment != VERI_UNDEF_ENV) && (environment != VERI_COVERPOINT_ENV) && (environment != VERI_EVENT_CONTROL) && (environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD) && (environment != VERI_SEQ_PROP_INIT) && (environment != VERI_SYSTEM_TASK_ARG) && (environment != VERI_DELAY_CONTROL)) _suffix_id->SetCannotBeOnehot() ;

    // VIPER #4043 : Check usage of private and protected members of a class
    if (_suffix_id && (_suffix_id->IsPrivate() || _suffix_id->IsProtected())) {
        _suffix_id->CheckUsageOfClassMember(scope, this) ;
    }
    // Function call is created for sequence and property instance, check that
    if ((_suffix_id && (_suffix_id->IsSequence()  || _suffix_id->IsProperty() || _suffix_id->IsLetId())) && (environment != VERI_UPWARD_SCOPE_NAME)) {
        VeriModuleItem *body = _suffix_id->GetModuleItem() ;
        if (body) body->Resolve(scope, environment) ;
    }

    // Viper 6289: Set the _sva_clock_expr from clocking block if not set from the assertion
    if (!_sva_clock_expr && _prefix_id->IsClocking() && prefix_module_item && (environment == VERI_PROPERTY_ENV || environment == VERI_SEQUENCE_ENV)) {
        _sva_clock_expr = prefix_module_item->GetClockingEvent() ;
    }

    // LRM A.8.4 : Issue 1636 : constant primary cannot contain a hierarchical name :
    if (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_VALUE || environment == VERI_BINS_INIT_VALUE || environment == VERI_INTEGRAL_CONSTANT || environment == VERI_PARAM_INITIAL_EXP) {
        // VIPER 5347 : make this a warning, since ModelSim allows it, and VCS does not.
        // VIPER #8291: Do not error out for member selecte, which is not actually a hier name.
        // struct packed { logic [4:0] logic5;} p;
        // reg [p.logic5:0] r1;
        if (!_suffix_id || !_suffix_id->IsMember()) {
            Warning("constant expression cannot contain a hierarchical identifier") ;
        }
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriIndexedMemoryId::Resolve(VeriScope *scope, veri_environment environment)
{
    if (!_prefix) return ;

    // Resolve the prefix first.
    _prefix->ResolvePrefix(scope, environment, 0) ;

    // Now check if we can pick up the identifier of the prefix :
    _id = _prefix->FullId() ;

    // This is the 'top' of an indexed name usage.
    // We should always have an identifier here, unless the prefix is a hierarchical name :
    if (!_id && !_prefix->IsHierName()) {
        Error("%s is not declared",_prefix->GetName()) ;
    }

    // Check the usage of this identifier in this environment
    if (_id && !_prefix->IsHierName()) _id->CheckEnvironment(environment, /*from*/this) ;

    // Discard id as onehot here, unless we are in an event or delay control expression. There encoding does not matter.
    if (_id && _id->CanBeOnehot() && (environment != VERI_UNDEF_ENV) && (environment != VERI_COVERPOINT_ENV) && (environment != VERI_EVENT_CONTROL) && (environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD) && (environment != VERI_SEQ_PROP_INIT) && (environment != VERI_SYSTEM_TASK_ARG) && (environment != VERI_DELAY_CONTROL)) _id->SetCannotBeOnehot() ;

    if (!_indexes) return ; // something wrong..

    // VIPER #2958: Check whether the prefix involves type parameter or incomplete forward reference:
    VeriTypeInfo *prefix_type = _prefix->CheckType(0 /* no expected type */, NO_ENV) ;

    VeriTypeInfo *copied_prefix_type = prefix_type ? prefix_type->Copy(): 0 ;
    // Resolve all indexes :
    // VIPER 5352 moved the resolve of _indexes before checking
    // prefix_Type as parametertype or unresolvedname type and returning
    // this causes incorrect error in VeriIndexedMemoryId::VarUsage
    unsigned i ;
    VeriExpression *idx ;
    FOREACH_ARRAY_ITEM(_indexes, i, idx) {
        if (!idx) continue ;

        // Ranged index should not be allowed in a prefix.
        // That means in all indices except for the last one :
        // Also, VIPER 2709 : if prefix id is a type, then this is actually a parameter type expression,
        // which declares a subtype. Something like bit[6:0][7:0]. And that should be allowed.
        if (idx->IsRange() && (i!=_indexes->Size()-1) && (!_id || !_id->IsType())) {
            idx->Error("range is not allowed in a prefix") ;
        }

        // If this is a net lvalue, then range must be constant (LRM A.8.5)
        // VIPER 2543 : If it a port connection to a trans gate, range must be constant
        if (environment==VERI_NET_LVALUE || (environment==VERI_NET_LVALUE_UNDECL_USE_GOOD) || environment==VERI_GATE_INOUT_PORT_CONNECTION) {
            idx->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
        } else if (_id && environment==VERI_FORCE_LVALUE) {
            // VIPER 2948 : If this is in LHS of 'force' statement, range or index must be constant.
            if (_id->IsNet()) {
                idx->Resolve(scope,VERI_CONSTANT_EXPRESSION) ;
            } else {
                // VIPER 2948 : if this is LHS of 'force' statement, then if prefix is a variable (non-net), this is always an error
                Error("bit-select or part-select is not allowed in a %s statement for non-net %s", "force", _id->Name()) ;
            }
        } else {
            // otherwise, just resolve as normal expression.
            veri_environment index_env = VERI_EXPRESSION ;
            if ((i == _indexes->Size()-1) && idx->IsRange() &&
                    ((copied_prefix_type && (copied_prefix_type->IsQueue() || copied_prefix_type->IsUnresolvedNameType() || copied_prefix_type->IsTypeParameterType())) ||
                    (prefix_type && (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType())))) {
                index_env = VERI_VALUE_RANGE ;
            }
            idx->Resolve(scope, index_env) ;
        }
        if (copied_prefix_type) {
            // VIPER #4984: Modify the prefix type to consider this dimension as indexed on prefix type:
            Array reduce_idx(1) ;
            reduce_idx.Insert(idx) ;
            copied_prefix_type = copied_prefix_type->ReduceDimBy(&reduce_idx, 0 /* do not produce messages */) ;
        }
    }
    delete copied_prefix_type ;

    // Do not check anything for items invloving type parameters or incomplete forward references:
    if (prefix_type && (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType())) {
        delete prefix_type ;
        return ;
    }

    // Additional checks on the indexed memory name :
    // Check indexing of the identifier, Verilog 2001-style
    // VIPER #2709: Do not check/warn this for type-ids:
    if (_id && !_id->IsType() && !IsSystemVeri()) {
        if (!_id->UnPackedDimension()) {
            // Cannot have multiple indexes into a non-memory (no unpacked dimensions)
            Error("%s is not a memory", _id->Name()) ;
        } else if (_id->UnPackedDimension()==_indexes->Size()) {
            // Index or part-select of one memory word
            // Verilog 95/2001 : Memory cannot be part-selected.
            VeriExpression *last_index = (VeriExpression*)_indexes->GetLast() ;
            if (last_index && last_index->IsRange()) {
                Error("part-select of memory %s is not allowed", _id->Name()) ;
            }
        } else if (_id->UnPackedDimension()+1==_indexes->Size()) {
            // Index or part-select of one element of the memory.
            // So that needs to be an array.. Verilog is very forgiving on this,
            // and there is not much we can check in Resolve here (without elaboration)
            // VIPER 2895 : cannot even do this test if base type is 'integer' or other multi-bit scalar.
            // FIX ME : maybe we need a IsMultibitScalar() analysis API routine ?
            //if (!_id->IsArray()) {
            //    Error("%s is not an array of vectors", _id->Name()) ;
            //}
        } else {
            Error("%s needs %d dimensions", _id->Name(), _id->Dimension()) ;
        }
    }
    // VIPER #2046 : Issue error if selection is illegal
    // It _id is an array of vector atom type, indexing into the atom is
    // allowed, if _id is an array of scalar, indexing on scalar type is
    // not allowed.
    unsigned packed_dims = _id ? _id->PackedDimension(): 0 ;
    unsigned unpacked_dims = _id ? _id->UnPackedDimension() : 0 ;
    // VIPER #2709: Do not check/warn this for type-ids:
    if (_id && !_id->IsType() && (_indexes->Size() > (packed_dims + unpacked_dims))) {
        // Number of indexes greater than total dimension count.
        // Number of indexes can atmost be (total dimension count + 1) for
        // atom types.
        if (_indexes->Size() > (packed_dims + unpacked_dims + 1)) {
            // Selection illegal
            Warning("too many indices into %s", _id->Name()) ;
        }
    }
    /*
        } else { // _indexes->Size() = (packed_dims + unpacked_dims + 1)
            // _id should not be of scalar type
            switch (_id->Type()) {
            // All net types are 1 bit :
            case VERI_WIRE    :
            case VERI_TRI     :
            case VERI_TRI1    :
            case VERI_SUPPLY0 :
            case VERI_WAND    :
            case VERI_TRIAND  :
            case VERI_TRIOR   :
            case VERI_TRI0    :
            case VERI_SUPPLY1 :
            case VERI_WOR     :
            case VERI_TRIREG  :
            case VERI_UWIRE   :
            // VIPER #2960 side effect: don't allow indexing into chandle:
            case VERI_CHANDLE :
            // Real types :
            case VERI_REAL : // VIPER #2976 : Indexing on real not allowed
            case VERI_REALTIME :
            case VERI_SHORTREAL :
             // 1 bit Register types :
            case VERI_REG :
            case VERI_BIT :
            case VERI_LOGIC :
                Warning("too many indices into %s",_id->Name()) ;
                break ;
            default : break ;
            }
        }
    }
    */

#if 0
// RD: 6/02 : silly rule. Does not always seem to hold either..
    // If environment is 'CONSTANT', indexed naming is not allowed ! (A.8.4)
    if (environment==VERI_CONSTANT_EXPRESSION) {
        // Make it a warning, since many user designs seem to have it
        Warning("indexing (of %s) is not allowed in a constant expression",  (_id) ? _id->Name() : _name) ;
    }
#endif

    // adjust prefix_type with indexes : Viper 5352
    FOREACH_ARRAY_ITEM(_indexes, i, idx) {
        if (!idx) continue ;
        //if (prefix_type) {
            // VIPER #4984: Modify the prefix type to consider this dimension as indexed on prefix type:
            //Array reduce_idx(1) ;
            //reduce_idx.Insert(idx) ;
            //prefix_type = prefix_type->ReduceDimBy(&reduce_idx, 0 /* do not produce messages */) ;
        //}
        // VCS Compatibility issue (test11) : modified prefix type after checking the index
        // VIPER #3499: Check for $ in indexing for prefix other than queue:
        if (idx->HasDollar() && (prefix_type && !prefix_type->IsQueue() && !prefix_type->IsTypeParameterType() && !prefix_type->IsUnresolvedNameType())) idx->Error("illegal context for $") ;
        if (prefix_type) {
            // VIPER #4984: Modify the prefix type to consider this dimension as indexed on prefix type:
            Array reduce_idx(1) ;
            reduce_idx.Insert(idx) ;
            prefix_type = prefix_type->ReduceDimBy(&reduce_idx, 0 /* do not produce messages */) ;
        }
    }
    delete prefix_type ;

    // VIPER 2473 : Issue error if bits are selected from an unpacked structure/union
    // VIPER 2558: Check both packed and unpacked dimensions for comparing with the indexed size.
    // VIPER #3263 : Use Base data type instead of normal data type to get correct
    // base type as type can be defined in multiple levels.
    // Covered by CheckType
    //VeriDataType *data_type = _id ? _id->GetBaseDataType(): 0 ;
    //if (_id && ((_id->PackedDimension() + _id->UnPackedDimension()) < _indexes->Size()) &&  data_type && data_type->IsUnpacked() && ((_id->Type() == VERI_STRUCT) || (_id->Type() == VERI_UNION))) {
        //Error("illegal select on unpacked structure/union type variable %s", _id->Name()) ;
    //}
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void
VeriIndexedExpr::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve the prefix first:
    if (_prefix) _prefix->Resolve(scope, environment) ;

    // Next resolve the index with appropriate type:
    if (_idx) {
        veri_environment idx_env = VERI_EXPRESSION ;
        if (_idx->IsRange()) idx_env = VERI_VALUE_RANGE ;
        _idx->Resolve(scope, idx_env) ;
    }

    // Finally check validity of this indexing
    VeriTypeInfo *this_type = CheckType(0, NO_ENV) ;
    delete this_type ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriName::ResolveScopeName(VeriScope * /*scope*/, veri_environment /*environment*/)
{
    Error("syntax error near %s", GetName()) ;
}

void VeriIdRef::ResolveScopeName(VeriScope *scope, veri_environment /*environment*/)
{
    const char *name = GetName() ;

    // Resolve the identifier as normal (if we have a scope): Can get class id
    if (!_id && scope && name) _id = scope->Find(name, /*from*/this) ;

    if (!_id) { // Id not found, can be package id
        // Try to find this in the 'compilation' scope (the current working 'library') :
        VeriModule *module = veri_file::GetModule(name, 1/*case_sensitive*/, veri_file::GetWorkLib()->GetName()) ;
        if (!module) {
            // Check if it is std::<some_obj>
            module = veri_file::GetModule(name, 1/*case_sensitive*/, "std" /* std library*/) ;
        }
        if (!module) {
            // VIPER #5801: Try to resolve from -L option library:
            module = veri_file::GetModuleFromLOptions(name) ;
        }
        if (module && module->IsPackage()) _id = module->GetId() ;
    }

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    if (_id) {
        Strings::free(_name) ; _name = 0 ;
    }
#endif
#endif
}

void VeriScopeName::ResolveScopeName(VeriScope *scope, veri_environment environment)
{
    // Scope name appears as prefix of another scope name i.e class_name::class_name::type
    if (_prefix) _prefix->ResolveScopeName(scope, environment) ;

    // Resolve param value assignments
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_param_values, i, expr) {
        if (expr) expr->Resolve(scope, VERI_PARAM_VALUE) ; // Can be constant expression/data type (for type parameter)
    }

    // Now pick up the identifier of the prefix :
    _prefix_id = (_prefix) ? _prefix->GetId() : 0 ;

    if (!_prefix_id) { // Type should be declared before use
        Error("%s is not declared", (_prefix) ? _prefix->GetName() : "<unknown>") ;
        return ;
    }

    // VIPER #3822 : If prefix is type parameter type or unresolved name type, do
    // not produce error or check suffix. It can be bound with class later.
    VeriTypeInfo *prefix_type = _prefix_id->CreateType(0) ;
    if (prefix_type && (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType())) {
        delete prefix_type ;
        return ;
    }
    // VIPER #6590 : if '_prefix_id' is type-id, but prefix_type is 0, it means, the
    // type is not resolved yet. Donot try to check suffix
    if (_prefix_id->IsType() && !prefix_type) return ;

    // Prefix can only be class/package identifier
    // or covergroup/coverpoint: VIPER #4244
    // Prefix type can also be of class type
    VeriModuleItem *module_item = _prefix_id->GetModuleItem() ;
    if (!_prefix_id->IsClass() && !_prefix_id->IsPackage() && !_prefix_id->IsCovergroup() && prefix_type
        && !(module_item && (module_item->GetType() == VERI_COVERPOINT)) && !prefix_type->IsClassType()
        && ((_prefix && _prefix->GetToken() != VERI_LOCAL))) { // VIPER #6391: prefix can be "local::" scope
        Error("%s is not a type", _prefix_id->Name()) ;
        delete prefix_type ;
        return ;
    }

    // Look for the suffix identifier. we need to look in the prefix scope :
    VeriScope *prefix_scope = 0 ;
    // VIPER #6551: Do not check whether prefix_id is a class, it may be a type parameter
    // and the value of the parameter is a class. In that case IsClass() may not be true.
    //VeriIdDef *scope_id = _prefix_id ;
    //if (_prefix_id->IsClass()) {
    //    // Prefix can be a type identifier whose type is a class.
    //    // typedef class_c this_type ;
    //    // this_type::x ;
    //    // In that case LocalScope() should be get from class identifier instead of type identifier
    //    scope_id = (prefix_type && prefix_type->IsClassType()) ? prefix_type->GetNameId() : _prefix_id ;
    //}
    // VIPER #6551: Instead directly check for class in the created type-info prefix_type:
    VeriIdDef *scope_id = (prefix_type && prefix_type->IsClassType()) ? prefix_type->GetNameId() : _prefix_id ;
    prefix_scope = (scope_id) ? scope_id->LocalScope(): _prefix_id->LocalScope() ;
    delete prefix_type ;

    // Find the suffix local in that scope :
    if (prefix_scope && _suffix) {
        _suffix_id = prefix_scope->FindLocal(_suffix) ;
    }
    // If prefix is a derived class, suffix can present in its base class too
    if (!_suffix_id && prefix_scope && scope_id && scope_id->IsClass()) {
        _suffix_id = prefix_scope->FindInBaseClass(_suffix) ;
    }

    // VIPER #2880 : Add the referenced package in the dependency list of containing
    // module's scope. Don't use 'AddImportItem', as it will make this identifier
    // directly visible from current scope
    if (_suffix_id && scope && _prefix_id->IsPackage()) {
        scope->Using(prefix_scope) ;
    }
    // VIPER #4601 : error should be flagged if suffix of the scope name is not declared
    // in the specified scope
    if (!_suffix_id && _suffix && !_function_type) Error("%s is not declared under prefix %s", _suffix, _prefix_id->Name()) ;

    if (_modport_name) _modport_name->Resolve(scope, environment) ;
}

void VeriScopeName::Resolve(VeriScope *scope, veri_environment environment)
{
    if (!_prefix) return ; // Cant do anything without a prefix, suffix null represents 'class_id #(...)'
    if (!_suffix) {
        // VIPER #5388: For data type like '<interface_name> #(3)', VeriScopeName
        // is created, but semantic of this is not like scope name. As interface
        // can be declared later. So resolve the _prefix only as normal id-ref.
        _prefix->Resolve(scope, environment) ;
        // Now check if we can pick up the identifier of the prefix :
        _prefix_id = _prefix->GetId() ;
        // Resolve param value assignments
        unsigned i ;
        VeriExpression *expr ;
        FOREACH_ARRAY_ITEM(_param_values, i, expr) {
            if (expr) expr->Resolve(scope, VERI_PARAM_VALUE) ; // Can be constant expression/data type (for type parameter)
        }
        return ;
    }
    // Prefix of a scope name can only have package/class name. Package/class name
    // can be used in class scope if only they are declared previously. Moreover
    // packages are stored in library and classes in root-scope/module-scope.
    // 'Resolve' routine does not find 'name' in libraries. So we need to resolve
    // class scope specially, so that prefix can be resolved searching libraries.

    // Resolve the prefix first.
    if (!_prefix_id) {
         // Make this function reentrant. Viper 7154:
         // If the _prefix_id is already present do not call ResolveScopeName again
         // this fails for $unit because veri_file::_root_module is reset during veri_file::EndCompilationUnit.
        _prefix->ResolveScopeName(scope, environment) ;

        // Now check if we can pick up the identifier of the prefix :
        _prefix_id = _prefix->GetId() ;
    }

    if (!_prefix_id) { // Type should be declared before use
        // VIPER #4747: Do not produce the following error if it starts with forward typedef:
        VeriName *prefix_runner = _prefix ;
        // Get the last resolved prefix id:
        while (prefix_runner && !prefix_runner->GetId()) prefix_runner = prefix_runner->GetPrefix() ;
        // Check if this resolved id is a type not fully defined by forward declaration:
        VeriIdDef *resolved_id = (prefix_runner) ? prefix_runner->GetId() : 0 ;
        unsigned forward_type = (resolved_id && resolved_id->IsType() && !resolved_id->GetDataType()) ? 1 : 0 ;
        // VIPER #6441 : If last resolved prefix id is a type declaration and its data type is class name
        // having parameter overwrite, such type should be considered as unresolved and should not produce
        // error here.
        VeriTypeInfo *resolved_id_type = (!forward_type && resolved_id) ? resolved_id->CreateType(0): 0 ;
        unsigned is_unresolved_type = (resolved_id_type && (resolved_id_type->IsTypeParameterType() || resolved_id_type->IsUnresolvedNameType())) ? 1 : 0 ;
        delete resolved_id_type ;
        // Do not produce the error if it is a forward typedef which is not yet actually defined:
        if (!forward_type && !is_unresolved_type) Error("%s is not declared", _prefix->GetName()) ;
        return ;
    }

    // VIPER #3822 : If prefix is type parameter type or unresolved name type, do
    // not produce error or check suffix. It can be bound with class later.
    VeriTypeInfo *prefix_type = _prefix->CheckType(0, NO_ENV) ; //_prefix_id->CreateType(0) ;
    unsigned prefix_not_resolved = 0 ;
    if (_prefix_id->IsType() && (!prefix_type || (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType()))) {
        prefix_not_resolved = 1 ;
    }

    // Prefix can only be class/package identifier
    // or covergroup/coverpoint: VIPER #4663
    // Prefix type can also be of class type
    VeriModuleItem *module_item = _prefix_id->GetModuleItem() ;
    if (!prefix_not_resolved && !_prefix_id->IsClass() && !_prefix_id->IsPackage() && !_prefix_id->IsCovergroup()
        && !(module_item && (module_item->GetType() == VERI_COVERPOINT)) && prefix_type && !prefix_type->IsClassType()
        && (_prefix->GetToken() != VERI_LOCAL)) { // VIPER #6391: prefix can be "local::" scope
    // Prefix can only be class/package identifier.
    //if (!prefix_not_resolved && !_prefix_id->IsClass() && !_prefix_id->IsPackage()) {
        Error("%s is not a type", _prefix_id->Name()) ;
        delete prefix_type ;
        return ;
    }
    // Look for the suffix identifier. we need to look in the prefix scope :
    VeriScope *prefix_scope = 0 ;
    // VIPER #6551: Do not check whether prefix_id is a class, it may be a type parameter
    // and the value of the parameter is a class. In that case IsClass() may not be true.
    //VeriIdDef *scope_id = _prefix_id ;
    //if (_prefix_id->IsClass()) {
    //    // Prefix can be a type identifier whose type is a class.
    //    // typedef class_c this_type ;
    //    // this_type::x ;
    //    // In that case LocalScope() should be get from class identifier instead of type identifier
    //    scope_id = (prefix_type && prefix_type->IsClassType()) ? prefix_type->GetNameId() : _prefix_id ;
    //}
    // VIPER #6551: Instead directly check for class in the created type-info prefix_type:
    VeriIdDef *scope_id = (prefix_type && prefix_type->IsClassType()) ? prefix_type->GetNameId() : ((prefix_not_resolved) ? 0: _prefix_id) ;
    prefix_scope = (scope_id) ? scope_id->LocalScope(): _prefix_id->LocalScope() ;

    // If prefix_type is type parameter type we do not error out and checks for suffix
    if (prefix_type && _prefix_id->IsParam() && prefix_type->IsClassType()) {
        VeriIdDef *prefix_id = prefix_type->GetNameId() ;
        if (prefix_id) prefix_scope = prefix_id->LocalScope() ;
    }
    delete prefix_type ;

    // Find the suffix local in that scope :
    if (prefix_scope) {
        // Viper 6887 : local::id in constraint block not resolved correctly
        // According to LRM 2009 Section 18.7.1 the id should be searched in the
        // scope containing the function and then in the local scope and not in the
        // class scope of [randomize() with object] class.
        VeriScope *extended_scope = prefix_scope->GetExtendedScope() ;
        if (_prefix->GetToken() == VERI_LOCAL) {
            prefix_scope->SetExtendedScope(prefix_scope->Upper()) ;
        }
        _suffix_id = prefix_scope->FindLocal(_suffix) ;
        if (_prefix->GetToken() == VERI_LOCAL) {
            prefix_scope->SetExtendedScope(extended_scope) ;
        }
    }
    // If prefix is a derived class, suffix can present in its base class too
    if (!_suffix_id && prefix_scope && scope_id && scope_id->IsClass()) {
        _suffix_id = prefix_scope->FindInBaseClass(_suffix) ;
    }

    // VIPER #5303 : Reset function type if suffix id has been resolved.
    if (_suffix_id) _function_type = 0 ;

    // Issue error if _suffix not declared under prefix
    if (!prefix_not_resolved && !_suffix_id) {
        // Do not produce error if prefix is a parameterized class
        VeriModuleItem *class_body = (scope_id && scope_id->IsClass()) ? scope_id->GetModuleItem() : 0 ;
        Array *params = (class_body) ? class_body->GetParameters() : 0 ;
        // Do not produce error if suffix is built-in method
        if (!(params && params->Size()) && !_function_type) Error("%s is not declared under prefix %s", _suffix, _prefix_id->Name()) ;
    }

    // VIPER #2880 : Add the referenced package in the dependency list of containing
    // module's scope. Don't use 'AddImportItem', as it will make this identifier
    // directly visible from current scope
    if (_suffix_id && scope && _prefix_id->IsPackage()) {
        scope->Using(prefix_scope) ;
    }
    if (_suffix_id) {
        _suffix_id->CheckEnvironment(environment, this) ;
    } //else { // No _suffix, check environment for prefix
       // if (!_suffix) _prefix_id->CheckEnvironment(environment, this) ;
    //}
    unsigned legal_scope_suffix = 0 ;
    if (_prefix_id->IsClass() && _suffix_id) {
        if (_suffix_id->IsType()) legal_scope_suffix = 1 ;
        if (!legal_scope_suffix) {
            VeriDataType *suffix_data_type = _suffix_id->GetDataType() ;
            if (suffix_data_type && (suffix_data_type->IsEnumType() || suffix_data_type->IsUnionType() || suffix_data_type->IsStructType())) legal_scope_suffix = 1 ;
            VeriIdDef *suffix_data_type_id = suffix_data_type ? suffix_data_type->GetId() : 0 ;
            if ((suffix_data_type_id && suffix_data_type_id->IsClass()) || (_suffix_id->IsClass())) legal_scope_suffix = 1 ;
            if (!_suffix_id->IsAutomatic()) legal_scope_suffix = 1 ;

            // Now check if prefix is the base class of this module_item defined in Scope "scope"
            VeriIdDef *prefix_scope_class = prefix_scope ? prefix_scope->GetClass() : 0 ;
            VeriIdDef *this_scope_class = scope ? scope->GetClass() : 0 ;
            if (prefix_scope_class && this_scope_class) {
                while (this_scope_class) {
                    VeriModuleItem *this_item = this_scope_class->GetModuleItem() ;
                    VeriName *base_name = this_item ? this_item->GetBaseClassName() : 0 ;
                    VeriIdDef *base_id = base_name ? base_name->GetId() : 0 ;
                    if (base_id == 0) {
                        this_scope_class = 0 ;
                        continue ;
                    }
                    if (base_id == prefix_scope_class) {
                        legal_scope_suffix = 1 ;
                        break ;
                    }
                    this_scope_class = base_id ;
                }
            }
        }
    }
    if (_prefix_id->IsClass() && !legal_scope_suffix && _suffix_id) {
        // Viper 4455. Class::var access requiers var to be static
        Error("illegal reference to non-static id %s from class scope", _suffix_id->GetName()) ;
    }

    // Function call is created for sequence and property instance, check that
    // Add this call Viper 2627
    if ((_suffix_id && (_suffix_id->IsSequence()  || _suffix_id->IsProperty() || _suffix_id->IsLetId())) && (environment != VERI_UPWARD_SCOPE_NAME)) {
        VeriModuleItem *body = _suffix_id->GetModuleItem() ;
        if (body) body->Resolve(scope, environment) ;
    }

    // Resolve param value assignments
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_param_values, i, expr) {
        if (expr) expr->Resolve(scope, VERI_PARAM_VALUE) ; // Can be constant expression/data type (for type parameter)
    }

    if (_modport_name) _modport_name->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriKeyword::ResolvePrefix(VeriScope *scope, veri_environment environment, unsigned /*allow_range_index*/)
{
    // Just call the normal resolve routine:
    Resolve(scope, environment) ;
}

void VeriKeyword::ResolveScopeName(VeriScope *scope, veri_environment environment)
{
    // keyword (like '$unit') can appear in the prefix of a scope name. Resolve as usual
    Resolve(scope,environment) ;
}

void VeriKeyword::Resolve(VeriScope *scope, veri_environment environment)
{
    if (!_token || !scope) return ; // Can't do anything without scope

    // Keywords can be INTERFACE (generic interface keyword), SUPER and THIS (the class-related keywords),
    // DOLLAR_ROOT and DOLLAR_UNIT (keywords for special scope access)
    // If keyword is unknown, do not do anything

    switch (_token) {
    case VERI_INTERFACE :
        break ; // Nothing to resolve for a generic interface
    case VERI_THIS :
        {
        // Since 'this' can only be used inside a function of the class to which
        // it refers, the current scope is a scope of the function containing
        // the 'this' or any inner scope of the function body. The class scope is
        // situated hierarchically upwards of this scope
        // Get the encompasses class identifier
        VeriIdDef *class_id = scope->GetClass() ;
        // If the id is not a class-id, reset the id:
        if (class_id && !class_id->IsClass()) class_id = 0 ;

        // Error if 'this' is not in a class ?

        // For 'this', the class id is the resolved id:
        SetId(class_id) ;
        // Resolved name is class name (needed?)
        if (class_id) SetName(Strings::save(class_id->GetName())) ;
        break ;
        }
    case VERI_SUPER :
        {
        // 'super' must occur in a class.
        VeriIdDef *class_id = scope->GetClass() ;
        // If the id is not a class-id, reset the id:
        if (class_id && !class_id->IsClass()) class_id = 0 ;

        // Error if 'super' is not in a class ?

        // Get the class declaration, we need to get the base class of this class:
        VeriModuleItem *class_decl = (class_id) ? class_id->GetModuleItem() : 0 ;
        VeriName *base_class = (class_decl) ? class_decl->GetBaseClassName() : 0 ;
        // This references to this base class, set the id to its id:
        VeriIdDef *super_id = (base_class) ? base_class->GetId() : 0 ;
        // This also must be a class-id, if it is not, reset this id:
        if (super_id && !super_id->IsClass()) super_id = 0 ;

        // Error if there is no base class ?

        // For 'super', this class id is the resolved id:
        SetId(super_id) ;

        // Resolved name is class name (needed?)
        if (super_id) SetName(Strings::save(super_id->GetName())) ;
        break ;
        }
    case VERI_DOLLAR_UNIT :
        {
        // '$unit' refers to the compilation scope. Pick that up here :
        VeriModule *unit_module = veri_file::GetRootModule() ;
        VeriIdDef *unit_id = (unit_module) ? unit_module->GetId() : 0 ;

        if (unit_module && !unit_module->GetLibrary()) {
            // VIPER #6585: Ref to root module; add it into the library.
            // Otherwise it will get deleted leading to memory corruptions:

            // Viper 7153 : Do not set the unit_id if veri_file::AddRootModuleToWorkLib returns
            // NULL. This is because then the unit_module is deleted and reference to it in $unit
            // will cause corruption when we try to access it later.

            if (veri_file::AddRootModuleToWorkLib(unit_module)) {
                // No error if compilation unit is not defined.
                // Dependency or Error will occur if someone wants something from it.
                SetId(unit_id) ;
            }
        } else {
            // No error if compilation unit is not defined.
            // Dependency or Error will occur if someone wants something from it.
            SetId(unit_id) ;
        }
        break ;
        }
    case VERI_DOLLAR_ROOT :
        // '$root' refers to the overall set of modules (in the library?).
        // We do not know which module yet, and library has no id and no scope, so need to resolve module name in the selected name resolver.
        break ;
    case VERI_NEW:
        VeriIdRef::Resolve(scope, environment) ;
        break ;
    case VERI_LOCAL: // VIPER #6391
        {
        // IEEE 1800-2009 LRM section 18.7.1: 'local' resolves to the local (function?) scope:
        VeriScope *local = scope ;
        while (local && !local->GetOwner()) local = local->Upper() ;
        // Resolve to owner of the local named scope:
        if (local) SetId(local->GetOwner()) ;
        break ;
        }
    default :
        // do not do anything for unknown token
        break ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriConcat::Resolve(VeriScope *scope, veri_environment environment)
{
    unsigned i ;
    VeriExpression *expr ;
    // Other expressions follow environment
    //Viper 5579. empty concat not allowed for static variables in v2k mode
    if (!IsSystemVeri() && !IsAms() && (!_exprs || !_exprs->Size())) Warning("empty concatenation not allowed here") ;

    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        expr->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriMultiConcat::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION :
        Error("illegal expression in target") ;
        break ;
    case VERI_REG_LVALUE :
        // VIPER #5301 (test_50): Added new message to isolate this case:
        // Here the multi-concat is used in output/inout port for a function/task.
        Error("illegal multiconcat expression in target") ;
        break ;
    default :
        break ;
    }

    // Other expressions follow environment
    unsigned i ;
    VeriExpression *expr ;
    VeriTypeInfo *ele_type = 0 ;
    unsigned is_ele_string = 0 ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        expr->Resolve(scope, environment) ;
        ele_type = expr->CheckType(0, NO_ENV) ;
        if (ele_type && (ele_type->IsString() || ele_type->IsStringLiteral())) is_ele_string = 1 ;
        delete ele_type ;
    }

    // repeat expression is 'constant' (LRM A.8.1)
    // However, if the multi-concat with non-constant repeat is used as the right-hand of
    // an assignment with the type of the left hand side operand is string, it's valid. VIPER# 4259
    // VIPER #5992 : If any element of concat is string or string literal,
    // repeat expression can be non-constant
    if ((environment == VERI_STRING_TARGET) || is_ele_string) {
        if (_repeat) _repeat->Resolve(scope, VERI_EXPRESSION) ;
    } else {
        if (_repeat) _repeat->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriFunctionCall::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve the function identifier :
    if (!_func_name) return ;
    // VIPER #4470 : Resolve function name before accessing its identifier
    _func_name->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ; // Can be upward reference name

    // If prefix is name of built-in method call, get that function token and set here
    _function_type = _func_name->GetFunctionType() ;

    // Pick-up the identifier (if there) :
    VeriIdDef *func_id = _func_name->GetId() ;

    // If function id is from export created id without body, ignore that
    if (func_id && func_id->IsUnresolvedSubprog()) func_id = 0 ;

    // If function id is created from extern "C" declaration without body, ignore it:
    if (func_id && func_id->GetModuleItem() && !func_id->GetModuleItem()->IsVerilogMethod()) func_id = 0 ;

    // If identifier does not exist and function type does exist and prefix is
    // not hierarchical name, it can be the case that it is built-in method call
    // using only method name inside the class : example 'get_randstate()'
    if (!func_id && !_function_type && !_func_name->IsHierName()) {
        _function_type = GetFuncTokenOfClass(_func_name->GetName()) ;
        if (_function_type) { // It is built-in method call
            // Check whether we are within class
            if (scope && !scope->GetClass()) {
                Error("prefix of method %s should be %s", PrintToken(_function_type), "a class object") ;
            }
        }
    }

    // Check if this is appropriate for a function call :
    if (func_id && !func_id->IsFunction()) {
        // Function call is created for sequence and property instance, check that
        // Viper 5216. Added check for func_id not a cover group
        if (!func_id->IsSequence() && !func_id->IsProperty() && !func_id->IsCovergroup() && !(func_id->IsClass() && (environment == VERI_CLASS_NAME)) && !func_id->IsLetId())
            Error("%s is not a function", func_id->Name()) ;
    }

    // Check environment of function call : Error out only if we are sure that we should :
    switch (environment) {
    case VERI_LVALUE :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_PATH_EXPRESSION :
    case VERI_PORT_EXPRESSION : // Verilog'95 port expression. No function allowed.
    // case VERI_EVENT_CONTROL : // issue 1487 : function call allowed in event control expression.
    // case VERI_PORT_CONNECTION : // issue 1507 : function call allowed in port connection list
    case VERI_GATE_INOUT_PORT_CONNECTION : // VIPER 2543 : illegal expression used as actual of bidirectional pass switch instance
        Error("function call %s is not allowed here", _func_name->GetName()) ;
        break ;
    case VERI_SEQUENCE_ENV :
        if (func_id && func_id->IsProperty()) {
            Error("property instance %s is not allowed in sequence expression", func_id->Name()) ;
        }

    case VERI_PROPERTY_ENV :
        // FIX ME : do same checks as we would do in PSL here..
        break ;
    case VERI_EXPRESSION :
    case VERI_EXPRESSION_UNDECL_USE_GOOD :
    case VERI_ABORT_EXPRESSION :
    case VERI_LET_EXPRESSION :
    case VERI_PROCE_CONT_ASSIGN_VAL : // VIPER #6110: value of RHS of procedural continuous assignments
    case VERI_WAIT_EXP :
    case VERI_STRING_TARGET :
    // VIPER #4496 : void function cannot be used in constant expression, port connection, event control :
    case VERI_CONSTANT_EXPRESSION :
    case VERI_PARAM_INITIAL_EXP :
    case VERI_PORT_CONNECTION :
    case VERI_SYSTEM_TASK_ARG : //Viper 5525
    case VERI_EVENT_CONTROL :
    case VERI_EVENT_CONTROL_UNDECL_USE_GOOD :
    case VERI_SEQ_PROP_INIT :
        if (func_id && func_id->Type() == VERI_VOID) { //||
            // VIPER #4593 : Produce error if built-in method of return type void
            // is used as expression :
            Error("invalid use of void function %s", (func_id) ? func_id->Name(): (_func_name->GetName() ? _func_name->GetName(): "")) ;  // Issue error
        }
        if ((environment == VERI_EVENT_CONTROL) || (environment == VERI_EVENT_CONTROL_UNDECL_USE_GOOD) || (environment == VERI_SEQ_PROP_INIT)) {
            // VIPER #4741 : Sequence instance can be used in event expression, but not property
            if (func_id && func_id->IsProperty()) {
                Error("%s is not valid in an expression", func_id->Name()) ;
            }
        } else {
            if (func_id && (func_id->IsProperty() || func_id->IsSequence())) {
                Error("%s is not valid in an expression", func_id->Name()) ;
            }
        }
        break ;
    case VERI_CONSTRAINT_EXPR :
    case VERI_INLINE_CONSTRAINT_EXPR :
        if (_function_type == VERI_METHOD_RAND_MODE || _function_type == VERI_METHOD_CONSTRAINT_MODE) {
           // Viper 5438 LRM 13.4.11 P1800
           Warning("illegal call to %s in constraint block", PrintToken(_function_type)) ;
        }
        if (func_id) {
           // Viper 5438 LRM 13.4.11 P1800
            if (!func_id->IsAutomatic()) Error("only automatic functions allowed in constraint block") ;
            // Pick up the formals :
            Array *formals = (func_id) ? func_id->GetPorts() : 0 ;
            unsigned i ;
            VeriIdDef *formal ;
            FOREACH_ARRAY_ITEM(formals, i, formal) {
                if (!formal) continue ;
                if (formal->Dir() != VERI_INPUT && formal->Dir() != VERI_CONST) Error("only input and const ref ports allowed in function called from constraint block") ;
            }
            // TODO Check for cyclic dependecies. Described in LRM 13.4.11
        }
        break ;
    default :
        break ;
    }

    // We cannot do anything here if the prefix seems to be a 'method', since it might be user-declared (func in an interface or so).
    // We need to wait until AnalyzeFull() before we know if the function is really not user-declared.
    unsigned do_not_check_against_formal = 0 ;
    if (func_id && (func_id->GetFunctionType() == VERI_METHOD_RANDOMIZE)) {
        // It is calling of function 'randomize' which is defined in std package.
        // This function can have any number of inputs. So do not check port
        // connection for this function. Only resolve args.
        //do_not_check_against_formal = 1 ;
        // VIPER #8149: This 'randomize' can be locally declared function. We
        // should check formal/actual mismatch for such functions.
        // Check whether this function is defined inside 'std' package
        VeriScope *func_scope = func_id->LocalScope() ;
        VeriIdDef *container_id = func_scope ? func_scope->GetContainingModule(): 0 ;
        if (container_id && (Strings::compare(container_id->Name(), "std"))) {
            VeriModule *container_ptr = container_id ? container_id->GetModule(): 0 ;
            VeriLibrary *lib = (container_ptr) ? container_ptr->GetLibrary(): 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) {
                do_not_check_against_formal = 1 ;
            }
        }
        // Reset function type when it is not randomize of std package
        if (!do_not_check_against_formal) func_id->ResetFunctionType() ;
    }
    if (func_id && func_id->IsClass() && (environment == VERI_CLASS_NAME)) {
        // VIPER #4032 : This function call is actually base class specification
        // with argument list in derived class definition
        // class D extends B (4, 5)
        // So do not check connection, only resolve them.
        do_not_check_against_formal = 1 ;
    }
    unsigned scope_randomize = (_func_name && _func_name->IsScopeName()) ? 1 : 0 ;

    // Pick up the formals :
    Array *formals = (func_id) ? func_id->GetPorts() : 0 ;
    // Viper 5216. For covergroup pick the ports from moduleitem.
    if (func_id && func_id->IsCovergroup()) {
        VeriModuleItem *cover_grp = func_id->GetModuleItem() ;
        formals = cover_grp ? cover_grp->GetPorts() : 0 ;
    }
    VeriScope *formal_scope = (func_id) ? func_id->LocalScope() : 0 ;

    // Resolve the actuals, and check formals against actuals :
    unsigned i ;
    VeriExpression *expr ;
    VeriIdDef *formal ;
    unsigned all_ports_connected = 1 ;
    Map connected_formals(POINTER_HASH, (formals) ? formals->Size() : 1) ;
    unsigned named_formal = 0 ; // VIPER #4618
    FOREACH_ARRAY_ITEM(_args, i, expr) {
        // VIPER #5416 & #5810: Arguments to class randomize cannot be
        // 1) Empty Expression
        // 2) Any Expression (other than class property)
        // 3) If null is present then it should be the only expression
        // This is described in LRM Section 13.10 IEEE P1800
        if (!do_not_check_against_formal && _function_type == VERI_METHOD_RANDOMIZE && !expr) {
            Error("%s expressions not allowed in argument of %s randomize call", "empty", (scope_randomize)?"scope":"class") ;
            continue ;
        }

        // We want to 'Resolve()' the actual expression, even if func_id is not yet resolved.
        // So drop through this code knowing that pointers can be 0 and formals can be empty.
        // Complain only if func_id IS resolved.

        // Find the formal
        formal = 0 ;
        if (!do_not_check_against_formal) {
            if (expr && expr->NamedFormal()) {
                named_formal = 1 ;
                // This is a (SV-style) named formal.
                formal = (formal_scope) ? formal_scope->FindLocal(expr->NamedFormal()) : 0 ;
                if (func_id && !formal) {
                    expr->Error("%s has no port called %s",func_id->Name(),expr->NamedFormal()) ;
                    func_id->Info("%s is declared here",func_id->Name()) ;
                }
                if (formal && !formal->IsPort()) {
                    expr->Error("no definition for port %s", formal->Name()) ; // VIPER #8172
                    formal = 0 ;
                }
            } else {
                // Viper 7917: Viper 4618 did not performed the check if the expr is NULL.
                // VIPER #4618 : all the positional arguments must come before named arguments
                if (named_formal) {
                    if (expr) {
                        expr->Error("positional arguments must occur before named arguments") ;
                    } else {
                        Error("positional arguments must occur before named arguments") ;
                    }
                }
                // This is a ordered formal
                if (i>=((formals) ? formals->Size() : 0)) {
                    if (func_id) Error("%s expects %d arguments", func_id->Name(), ((formals) ? formals->Size() : 0)) ;
                        // no use checking more. No use resolving either, since these actuals are obsolete.
                    if (func_id) break ; // Don't bail out if identifier was not resolved. We still want to resolve a bit.
                } else {
                    formal = (formals && formals->Size() > i) ? (VeriIdDef*)formals->At(i) : 0 ;
                }
            }
        }

        if (!expr) continue ;

        if (func_id && formal && (func_id->IsSequence() || func_id->IsProperty() || func_id->IsLetId())) formal->SetActual(expr) ;
        // add this as a connected formal :
        // VIPER #3506: Treat open actuals as unconnected too:
        VeriExpression *conn_expr = expr->GetConnection() ;
        unsigned long connected = (!conn_expr || conn_expr->IsOpen()) ? 0 : 1 ;
        all_ports_connected = all_ports_connected & connected ; // Update this flag with the status of this actual
        if (formal && !connected_formals.Insert(formal, (void *)connected)) {
            // Formal is already connected. Complain :
            if (!do_not_check_against_formal)
                expr->Error("port %s is already connected", formal->Name()) ;
        }

        // Resolve the actual :
        // Determine the environment in which the actual should be resolved (SV can have outputs)
        // Best guess is is the existing environment of the caller :
        // VIPER issue 2303 : make sure to pass the current environment through to 'input' arguments...
        // This is because it could be a variety of environments, including 'property', 'sequence', 'expression', 'event' etc.
        veri_environment local_environment = environment ;
        if (formal) {
            switch(formal->Dir()) {
            case VERI_OUTPUT :  local_environment = VERI_REG_LVALUE ; break ; // reg assignment ?
            // case VERI_INPUT :   local_environment = (environment==VERI_CONSTANT_EXPRESSION) ? VERI_CONSTANT_EXPRESSION : VERI_EXPRESSION ; break ;
            case VERI_INOUT :   local_environment = VERI_REG_LVALUE ; break ;
            default : break ;
            }
        }

        // VIPER #4416: If formal is not present, it may be the case that formal
        // can be virtual interface. So pass VERI_PORT_CONNECTION to resolve actual
        if (!formal) local_environment = VERI_PORT_CONNECTION ;
        switch (environment) {
            case VERI_PROPERTY_ENV :
            case VERI_SEQUENCE_ENV :
                local_environment = VERI_EVENT_CONTROL ;
                if (func_id && func_id->IsProperty()) local_environment = VERI_PROPERTY_ARG ;
                if (func_id && func_id->IsSequence()) local_environment = VERI_SEQUENCE_ARG ;
                break;
            case VERI_CLASS_NAME : // Resolving argument list of base class specification
                local_environment = VERI_EXPRESSION ;
                break ;
            default :
                break;
        }
        // If the type of formal is unresolved/type parameter type, then also VERI_PORT_CONNECTION
        // environment should pass to resolve actual
        // VIPER #8010: Do not check formal type for sequence/property
        if (!func_id || (!func_id->IsProperty() && !func_id->IsSequence())) {
            VeriTypeInfo *formal_type = (formal && !formal->IsOutput() && !formal->IsInout()) ? formal->CreateType(0): 0 ;
            if (formal_type && (formal_type->IsTypeParameterType() || formal_type->IsUnresolvedNameType() || formal_type->IsVirtualInterface())) local_environment = VERI_PORT_CONNECTION ;
            delete formal_type ;
            if (formal && formal->IsVirtualInterface()) local_environment = VERI_PORT_CONNECTION ;
        }

        // Finally resolve the actual expression
        expr->Resolve(scope, local_environment) ;
        if (!do_not_check_against_formal) {
            if (_function_type == VERI_METHOD_RANDOMIZE) {
                // VIPER #5416 & #5810: Arguments to class randomize cannot be
                // 1) Empty Expression
                // 2) Any Expression (other than class property)
                // 3) If null is present then it should be the only expression
                // This is described in LRM Section 13.10 IEEE P1800
                if (expr->IsNull()) {
                    if (scope_randomize) {
                        expr->Error("%s expressions not allowed in argument of %s randomize call", "null", "scope") ;
                    } else if (_args->Size() > 1) {
                        expr->Error("%s expressions not allowed in argument of %s randomize call", "null with other", "class") ;
                    }
                    continue ;
                } else if (scope_randomize) {
                    if (!expr->GetId() && !InRelaxedCheckingMode()) expr->Error("%s expressions not allowed in argument of %s randomize call", "complex", "scope") ;
                    continue ;
                } else if (!expr->FullId()) {
                    expr->Error("%s expressions not allowed in argument of %s randomize call", "non class property", "class") ;
                    continue ;
                }
            }
            // Viper: 5276. Moved the call of CheckAssociation afer expr->Resolve()
            veri_type_env context = NO_ENV ;
            if (func_id) {
                if (func_id->IsProperty() || func_id->IsSequence()) context = PROP_SEQ ;
                if (func_id->IsFunction()) context = FUNC ;
                if (func_id->IsTask()) context = TASK ;
            }
            if (expr->IsClockedSequence()) {
                if (context == FUNC || context == TASK) {
                    Error("clocked expression not valid in function/task argument") ;
                    continue ;
                } else if (context == PROP_SEQ){
                    Warning("clocked expression not valid in property/sequence argument, violates IEEE 1800 syntax") ;
                }
            }

            (void) expr->CheckAssociation(formal, context) ;
        }
    }

    // Now check if all formals have an actual or initial value
    if (((((formals) ? formals->Size() : 0) != connected_formals.Size()) || !all_ports_connected)
            && !do_not_check_against_formal
            ) {
        // This can be legal for System Verilog, but not for anything else :
        if (IsSystemVeri() && func_id) {
            // There is a mismatch. Some must have an initial value
            FOREACH_ARRAY_ITEM(formals, i, formal) {
                if (!formal) continue ;
                if (connected_formals.GetValue(formal)) continue ;
                // Here, this formal is not connected.
                // Only allowed if it is an output, or if it has an initial value :
                VeriExpression *initial_expr = formal->GetInitialValue() ;
                VeriScope *func_scope = func_id->LocalScope() ;
                if (initial_expr && (func_id->IsProperty() || func_id->IsSequence() || func_id->IsLetId())) continue ;
                if (initial_expr) {
                    // Viper 5122 call a Resolve on the initial expression from the function/task call's Resolve
                    if (func_scope) {
                        veri_environment local_environment = environment ;
                        switch(formal->Dir()) {
                        case VERI_OUTPUT :  local_environment = VERI_REG_LVALUE ; break ; // reg assignment ?
                        // case VERI_INPUT :   local_environment = (environment==VERI_CONSTANT_EXPRESSION) ? VERI_CONSTANT_EXPRESSION : VERI_EXPRESSION ; break ;
                        case VERI_INOUT :   local_environment = VERI_REG_LVALUE ; break ;
                        default : break ;
                        }
                        // VIPER #6651/#5740: If the type of formal is unresolved/type parameter type, then also VERI_PORT_CONNECTION environment should pass to resolve actual
                        VeriTypeInfo *formal_type = (!formal->IsOutput() && !formal->IsInout()) ? formal->CreateType(0) : 0 ;
                        if (formal_type && (formal_type->IsTypeParameterType() || formal_type->IsUnresolvedNameType() || formal_type->IsVirtualInterface())) local_environment = VERI_PORT_CONNECTION ;
                        delete formal_type ;
                        // VIPER #4350 (#6651/#5740): Pass VERI_PORT_CONNECTION environment to allow instance as actual when formal is virtual interface
                        if (formal->IsVirtualInterface()) local_environment = VERI_PORT_CONNECTION ;
                        initial_expr->Resolve(0, local_environment) ;
                    }
                    continue ;
                }
                // VIPER #2902 : Issue error if any formal having no default value
                // is not connected. This is according to IEEE P1800 LRM section 12.4.3
                //if (formal->IsOutput()) continue ;
                // Complain :
                // Issue 2542 : mention both the port and the subprogram name:
                // Better error: No actual value has been specified for a formal port that does not have a default value:
                Error("port %s is not connected on %s call %s", formal->Name(), "function", func_id->Name()) ;
            }
        } else {
            // Any other dialect should report this mismatch :
            if (func_id) Error("%s expects %d arguments", func_id->Name(), ((formals) ? formals->Size() : 0)) ;
        }
    }
    // VIPER #5501 : Check type for methods after resolving arguments
    VeriTypeInfo *func_type = 0 ;
    if (!func_id && _function_type) {
        func_type = _func_name->CreateTypeForMethods(_args, 0 /* function*/, NO_ENV) ; // Check type of the function VIPER 4407
        //delete func_type ;
    }
    switch (environment) {
    case VERI_EXPRESSION :
    case VERI_EXPRESSION_UNDECL_USE_GOOD :
    case VERI_ABORT_EXPRESSION :
    case VERI_LET_EXPRESSION :
    case VERI_PROCE_CONT_ASSIGN_VAL : // VIPER #6110: value of RHS of procedural continuous assignments
    case VERI_WAIT_EXP :
    case VERI_STRING_TARGET :
    // VIPER #4496 : void function cannot be used in constant expression, port connection, event control :
    case VERI_CONSTANT_EXPRESSION :
    case VERI_PARAM_INITIAL_EXP :
    case VERI_PORT_CONNECTION :
    case VERI_EVENT_CONTROL :
    case VERI_EVENT_CONTROL_UNDECL_USE_GOOD :
    case VERI_SEQ_PROP_INIT :
        // VIPER #4593 : Produce error if built-in method of return type void
        // is used as expression :
        if (func_type && func_type->Type() == VERI_VOID) { // void function in expression
            Error("invalid use of void function %s", (func_id) ? func_id->Name(): (_func_name->GetName() ? _func_name->GetName(): "")) ;  // Issue error
        }
        break ;
    default :
        break ;
    }
    delete func_type ;

    // Function call is created for sequence and property instance, check that
    VeriModuleItem *body = (func_id) ? func_id->GetModuleItem() : 0 ;
    if (func_id && body) {
        // VIPER #3370: Resolve the sequence/property declarations with the actual arguments:
        if (func_id->IsSequence() || func_id->IsProperty() || func_id->IsLetId()) {
            body->Resolve(scope, environment) ;
            FOREACH_ARRAY_ITEM(formals, i, formal) if (formal) formal->SetActual(0) ;
        }

        if ((environment == VERI_PARAM_VALUE) || (environment == VERI_CONSTANT_EXPRESSION) || (environment == VERI_PARAM_INITIAL_EXP) ||
            (environment == VERI_INTEGRAL_CONSTANT) || (environment == VERI_BINS_INIT_VALUE)) body->SetCalledAsConstFunction() ; // It is called as constant

        // VIPER #3276: Check whether this function is called from a constant environment
        // and whether it can be a potential constant function. If not, produce error:
        if (body->IsCalledAsConstFunction() && !body->CanBeConstFunction()) {
            Error("%s is not a valid constant function call", _func_name->GetName()) ;
            // Do not error this out here - function may not be constant for many reasons:
            //body->Error("global or hierarchical references may not be used in a constant function") ;
        }
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSystemFunctionCall::Resolve(VeriScope *scope, veri_environment environment)
{
    // System function argument checks needed ?

    // System function call is officially only allowed in normal (non-constant) expressions.
    switch (environment) {
    case VERI_CONSTANT_EXPRESSION : // VIPER Issue #6376
    {
        if (IsSystemVeri()) break ;

        // VIPER #6761: Check if it is a conversion function:
        unsigned allow_sys_calls = 0 ;
        switch (_function_type) {
        case VERI_SYS_CALL_BITSTOREAL :
        case VERI_SYS_CALL_ITOR       :
        case VERI_SYS_CALL_SIGNED     :
        case VERI_SYS_CALL_REALTOBITS :
        case VERI_SYS_CALL_RTOI       :
        case VERI_SYS_CALL_UNSIGNED   : allow_sys_calls = 1 ; break ;
        default: break ;
        }

        // VIPER #6761: Allow conversion functions here under constant environment:
        // Other tools seem to allow it, checking for argument to be constant done below.
        if (allow_sys_calls) break ;
    }

    case VERI_LVALUE :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_PATH_EXPRESSION :
    case VERI_PORT_EXPRESSION : // Verilog'95 port expression. No function allowed.
    // case VERI_EVENT_CONTROL : // Issue 1487 : (system) function call is allowed in event expression.
    // case VERI_PORT_CONNECTION : // Issue 2085 : system call allowed in a port connection..
    case VERI_GATE_INOUT_PORT_CONNECTION : // VIPER 2543 : illegal expression used as actual of bidirectional pass switch instance
        Error("system function call %s is not allowed here", _name) ;
        break ;
    default :
        break ;
    }

    unsigned i ;

    // Pick up the first argument :
    VeriExpression *expr = (_args && _args->Size()) ? (VeriExpression*)_args->At(0) : 0 ;

    // Intercept system functions which we know something about
    switch (_function_type) {
    case VERI_SYS_CALL_SIGNED :
    case VERI_SYS_CALL_UNSIGNED :
    // VIPER #6761: Add the following conversion functions too...
    case VERI_SYS_CALL_BITSTOREAL :
    case VERI_SYS_CALL_ITOR       :
    case VERI_SYS_CALL_REALTOBITS :
    case VERI_SYS_CALL_RTOI       :
        // Type-conversions to signed/unsigned
        // Check num of arguments (actually, we should do this in the analyser).
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Resolve the expression in the environment this was called in :
        if (expr) expr->Resolve(scope,environment) ;
        return ; // we are done.

    case VERI_SYS_CALL_ASSERTKILL :
    case VERI_SYS_CALL_ASSERTOFF :
    case VERI_SYS_CALL_ASSERTON :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect 1 or more arguments. First one is 'levels'. (unclear what that is).
        // second and later is a (hierarchical) name of assertion or module.
        if (!_args || _args->Size()<1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Infer 'levels' expression (unclear what that is: assume a constant expression?)..
        if (expr) expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;

        // Infer other arguments (module or assertion names) :
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (i==0) continue ; // this is the first one. Already done.
            if (!expr) continue ;
            // Resolve as a module name (FIX ME : better environment?).
            expr->Resolve(scope,VERI_UPWARD_SCOPE_NAME) ;
        }
        return ;
    case VERI_SYS_CALL_BITS :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect one argument (an expression for which we want to know the number of bits).
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Now the argument is not really 'read', since we only want to pick up the size.
        // We don't have a good environment for that, so lets call with '0'.
        // That does a minimum check (report undeclared id references), but does not set flags in the ids :
        if (expr) expr->Resolve(scope,VERI_UNDEF_ENV) ;
        return ; // we are done

    case VERI_SYS_CALL_BITSTOSHORTREAL :
    case VERI_SYS_CALL_SHORTREALTOBITS :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect one argument (an expression)
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // As a cast : pass in VERI_EXPRESSION ? Or the 'environment' ?
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ;
        return ; // we are done

    case VERI_SYS_CALL_CAST :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect two arguments. First one is a target variable,
        // second is an expression which should be assigned to the variable
        if (!_args || _args->Size()!=2) {
            if (_name) Error("%s expects %d arguments", _name, 2) ;
        }
        // Resolve the first argument (the dest_var) as an assignment (blocking assignment?)
        if (expr) expr->Resolve(scope,VERI_REG_LVALUE) ; // general (blocking?) assignment.

        // Pick up the source expression (second argument) :
        expr = (_args && _args->Size()>1) ? (VeriExpression*)_args->At(1) : 0 ;
        // Resolve as a normal expression ?
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ;
        return ; // we are done

    case VERI_SYS_CALL_DIMENSIONS :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS : // System Verilog Std 1800 addition
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Argument should contain a single (array) identifier.
        // As with other 'probing' system calls, this one does NOT constitute a read or write on the id.
        // So call with environment==VERI_UNDEF_ENV (which only reports undeclared ids)
        if (expr) {
            expr->Resolve(scope,VERI_UNDEF_ENV) ;
            // Now check if it contains a (single) identifier :
            VeriIdDef *id = expr->FullId() ;
            // VIPER #6476 : Allow bit/part select expression
            // P1800 -2009 Section 20.7.1 says IndexExpressions and MemoryIndexIds should be allowed
            VeriName *prefix = expr->GetPrefix() ;
            if (!id && prefix && (expr->GetIndexExpr() || expr->GetIndexes())) id = prefix->GetId() ;
            // VIPER #4427: Check whether it is a simple selected name (not indexed id or index memory id).
            // In that case should not produce the error message it may be an unresolved hierarchical name:
            unsigned simple_selected_name = (expr->IsHierName() && !expr->GetIndexExpr() && !expr->GetIndexes()) ? 1 : 0 ;
            if (!id && !simple_selected_name && !expr->DataTypeCast()) { // VIPER #4778: It can be a data type too
                if (_name) Error("argument of %s should be an identifier", _name) ;
            }
        }
        return ; // we are done

    case VERI_SYS_CALL_ISUNBOUNDED :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Exprect a 'constant expression' :
        if (expr) expr->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
        return ;

    case VERI_SYS_CALL_LEFT :
    case VERI_SYS_CALL_RIGHT :
    case VERI_SYS_CALL_LOW :
    case VERI_SYS_CALL_HIGH :
    case VERI_SYS_CALL_INCREMENT :
    case VERI_SYS_CALL_LENGTH : // 3.1 only
    case VERI_SYS_CALL_SIZE : // 3.1a
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect one (or two) arguments
        if (!_args || _args->Size()>2) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Argument should contain a single (array) identifier.
        // As with other 'probing' system calls, this one does NOT constitute a read or write on the id.
        // So call with environment==VERI_UNDEF_ENV (which only reports undeclared ids)
        if (expr) {
            expr->Resolve(scope,VERI_UNDEF_ENV) ;
            // Now check if it contains a (single) identifier :
            VeriIdDef *id = expr->FullId() ;
            // VIPER 5768: Now check if it is a IndexExpression or MemoryIndexId
            // P1800 -2009 Section 20.7.1 says IndexExpressions and MemoryIndexIds should be allowed
            VeriName *prefix = expr->GetPrefix() ;
            if (!id && prefix && (expr->GetIndexExpr() || expr->GetIndexes())) id = prefix->GetId() ;
            // VIPER #4427: Check whether it is a simple selected name (not indexed id or index memory id).
            // In that case should not produce the error message it may be an unresolved hierarchical name:
            unsigned simple_selected_name = (expr->IsHierName() && !expr->GetIndexExpr() && !expr->GetIndexes()) ? 1 : 0 ;
            if (!id && !simple_selected_name && !expr->DataTypeCast()) { // VIPER #4778: It can be a data type too
                if (_name) Error("argument of %s should be an identifier", _name) ;
            }
        }

        // Second argument (if there) contains a (constant?) expression :
        expr = (_args && _args->Size()>1) ? (VeriExpression*)_args->At(1) : 0 ;
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ; // VIPER #4779: Allow any expression (non-const too)
        return ; // we are done

    // SVA system functions :
    case VERI_SYS_CALL_ROSE :
    case VERI_SYS_CALL_FELL :
    case VERI_SYS_CALL_STABLE :
    case VERI_SYS_CALL_SAMPLED :
    case VERI_SYS_CALL_CHANGED :  // Viper #5756
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect one argument (expression).
        // Second argument (clock expr) optional.
        if (!_args || (_args->Size()!=1 && _args->Size()!=2)) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }

        // Viper 6482 : Add error for no _sva_clock_expr or no explicit clock in the argument list
        // _args->At(1) is the clocking event.
        // for $sampled no error is thrown as it does not requires a clock.
        if (!_sva_clock_expr && _args && _args->Size() < 2 && !_in_property_expr && _function_type != VERI_SYS_CALL_SAMPLED &&
            // VIPER #7720 : Consider system function call from let expression
            // let expression is resolved only from call
            //environment != VERI_LET_EXPRESSION &&
            environment != VERI_SEQUENCE_ENV && environment != VERI_PROPERTY_ENV) {
            if (_contains_event_control || _contains_delay_control || _contains_even_id_in_body || _multiple_inferred_clock) {
                // Viper 6477: Issue warning as most simulators do not report based on this 2009 feature described in 16.15.6
                if (_contains_event_control) Warning("no clock inferred, multiple event control found") ;
                if (_contains_delay_control) Warning("no clock inferred, delay control found") ;
                if (_contains_even_id_in_body) Warning("no clock inferred, event control id used in procedural code") ;
                if (_multiple_inferred_clock) Warning("no clock inferred, multiple inferred clock") ;
            } else {
                Error("the system function %s not sensitive to clock", PrintToken(_function_type)) ;
            }
        } else if ((environment == VERI_ALWAYS_BLOCK || environment == VERI_INITIAL_BLOCK || environment == VERI_ALWAYS_FF_BLOCK) &&
                    _sva_clock_expr && !_sva_clock_expr->IsEdge(0)) {
            // Viper 3955. The inferred clock must be preceded with an edge
            // Viper 6517: For Checker arguments check if is of event type
            VeriExpression *sva_expr = _sva_clock_expr->GetExpr() ;
            VeriIdDef *formal_id = sva_expr ? sva_expr->GetId() : 0 ;
            if (!formal_id || !formal_id->IsEvent()) Error("edge must be specified for the contextually inferred clock") ;
        }

        // VIPER 6491 : system function call under abort functions should have explicit clock except sampled. 2009 LRM 16.13.14
        if (environment == VERI_ABORT_EXPRESSION && _function_type != VERI_SYS_CALL_SAMPLED && _args && _args->Size() < 2) {
            Error("system function call %s inside abort function should have explicit clock", _name) ;
        }

        // Argument contains one expression :
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ;

        // Optional second argument contains the clock expression :
        if (_args && _args->Size()>1) {
            // Forth argument is 'clocking_event' (an expression).
            expr = (VeriExpression*)_args->At(1) ;
            // VIPER #7882: Clocking argument should contain @, otherwise issue an error.
            if (expr && !expr->IsDelayOrEventControl()) {
                Error("clocking argument for %s is not of type event control", PrintToken(_function_type)) ;
            }
            // VIPER 2577 : Second argument is clocking event. Resolve with
            // VERI_EVENT_CONTROL instead of VERI_EXPRESSION
            if (expr) expr->Resolve(scope,VERI_EVENT_CONTROL) ;
        }

        // Viper 6456: Store the sva clock expression
        _sva_clock_expression = _sva_clock_expr ;
        return ; // we are done

    case VERI_SYS_CALL_PAST :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // Expect 1 or 2 or 3 or 4 arguments.
        // Arguments' interpretation is ambiguous (in 3.1a) :
        // $past ( expression [, number_of_ticks] [,expression] [,clocking_event] ).
        // To resolve ambiguity, LRM states that unused intermediate args must be left empty.
        // So, for example, clocking_event is always the forth argument (if there).
        // Last argument is the clock.
        if (!_args || (_args->Size()>4 || _args->Size()<1)) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // First argument contains one expression :
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ;

        // Viper 6482 : Add error for no _sva_clock_expr
        // _args->At(3) is the clocking event for $past
        if (!_sva_clock_expr && _args && _args->Size() < 4 && !_in_property_expr && environment != VERI_SEQUENCE_ENV &&
            // VIPER #7720 : Consider system function call from let expression
            // let expression is resolved only from call
            //environment != VERI_LET_EXPRESSION &&
            environment != VERI_PROPERTY_ENV) {
            if (_contains_event_control || _contains_delay_control || _contains_even_id_in_body) {
                // Viper 6477: Issue warning as most simulators do not report based on this 2009 feature described in 16.15.6
                if (_contains_event_control) Warning("no clock inferred, multiple event control found") ;
                if (_contains_delay_control) Warning("no clock inferred, delay control found") ;
                if (_contains_even_id_in_body) Warning("no clock inferred, event control id used in procedural code") ;
            } else {
                Error("the system function %s not sensitive to clock", PrintToken(_function_type)) ;
            }
        }  else if ((environment == VERI_ALWAYS_BLOCK || environment == VERI_INITIAL_BLOCK || environment == VERI_ALWAYS_FF_BLOCK) &&
                     !_sva_clock_expr->IsEdge(0)) {
            // Viper 3955. The inferred clock must be preceded with an edge
            Error("edge must be specified for the contextually inferred clock") ;
        }

        // VIPER 6491 : system function call under abort functions should have explicit clock except sampled. 2009 LRM 16.13.14
        if (environment == VERI_ABORT_EXPRESSION && _args && _args->Size() < 4) {
            Error("system function call %s inside abort function should have explicit clock", _name) ;
        }

        // Second argument is 'number_of_ticks'. No explanation what that is. Assume constant expression.
        expr = (_args && _args->Size()>1) ? (VeriExpression*)_args->At(1) : 0 ;
        // Viper 4586. At-least for constant argument we can create error if expr < 1
        if (expr && expr->IsConst()) {
            int val = expr->Integer() ;
            if (val < 1 ) expr->Error("number of ticks for $past must be 1 or greater") ;
        }
        if (expr) expr->Resolve(scope,VERI_CONSTANT_EXPRESSION) ;

        // Third argument is 'expression2'.
        expr = (_args && _args->Size()>2) ? (VeriExpression*)_args->At(2) : 0 ;
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ;

        // Forth argument is 'clocking_event' (an expression).
        if (_args && _args->Size()>3) {
            // Forth argument is 'clocking_event' (an expression).
            expr = (VeriExpression*)_args->At(3) ;
            // VIPER #7882: Clocking argument should contain @, otherwise issue an error.
            if (expr && !expr->IsDelayOrEventControl()) {
                Error("clocking argument for %s is not of type event control", PrintToken(_function_type)) ;
            }
            // VIPER #4347 : This argument is clocking event, resolve with VERI_EVENT_CONTROL
            // instead of VERI_EXPRESSION
            if (expr) expr->Resolve(scope,VERI_EVENT_CONTROL) ;
        }

        // Viper 6456: Store the sva clock expression
        _sva_clock_expression = _sva_clock_expr ;

        return ; // we are done

    case VERI_SYS_CALL_COUNTONES :
    case VERI_SYS_CALL_ONEHOT :
    case VERI_SYS_CALL_ONEHOT0 :
    case VERI_SYS_CALL_ISUNKNOWN :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // These guys are normal functions (operating on a single combinational expression) :
        if (!_args || _args->Size()>1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Argument contains one expression :
        if (expr) expr->Resolve(scope,VERI_EXPRESSION) ;
        return ; // we are done

    case VERI_SYS_CALL_INSET :
    case VERI_SYS_CALL_INSETZ :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // SV 3.1 only..(disappeared in 3.1a)
        // These accept any number of expression, but at least two.
        if (!_args || _args->Size()<2) {
            if (_name) Error("%s expects %d arguments", _name, 2) ;
        }
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            if (expr) expr->Resolve(scope, VERI_EXPRESSION) ;
        }
        return ; // we are done

    case VERI_SYS_CALL_COVERAGE_CONTROL :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // These functions expect four arguments
        if (!_args || _args->Size()!=4) {
            if (_name) Error("%s expects %d arguments", _name, 4) ;
        }
        return ; // we are done

    case VERI_SYS_CALL_COVERAGE_MERGE   :
    case VERI_SYS_CALL_COVERAGE_SAVE    :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // These functions expect two arguments
        if (!_args || _args->Size()!=2) {
            if (_name) Error("%s expects %d arguments", _name, 2) ;
        }
        return ; // we are done

    case VERI_SYS_CALL_COVERAGE_GET_MAX : // SV Std 1800 addition
    case VERI_SYS_CALL_COVERAGE_GET     :
        // Do language-dialect check :
        if (!IsSystemVeri()) Error("system call %s not allowed in this dialect. Use System Verilog mode",_name) ;

        // These functions expect three arguments
        if (!_args || _args->Size()!=3) {
            if (_name) Error("%s expects %d arguments", _name, 3) ;
        }
        return ; // we are done
    case VERI_SYS_CALL_FEOF :
        // This function expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Resolve the expression in the environment this was called in :
        if (expr) expr->Resolve(scope,environment) ;
        return ;
    // Moved these function to VERILOG_2000 mode
    // LRM section 17.11.1/17.11.2 Viper 6807
    // Math functions (20.8)
    case VERI_SYS_CALL_CLOG2 :
    case VERI_SYS_CALL_SIN :
    case VERI_SYS_CALL_COS :
    case VERI_SYS_CALL_TAN :
    case VERI_SYS_CALL_ASIN :
    case VERI_SYS_CALL_ACOS :
    case VERI_SYS_CALL_ATAN :
    case VERI_SYS_CALL_SINH :
    case VERI_SYS_CALL_COSH :
    case VERI_SYS_CALL_TANH :
    case VERI_SYS_CALL_ASINH :
    case VERI_SYS_CALL_ACOSH :
    case VERI_SYS_CALL_ATANH :
    case VERI_SYS_CALL_LN  :
    case VERI_SYS_CALL_LOG10 :
    case VERI_SYS_CALL_EXP :
    case VERI_SYS_CALL_SQRT :
    case VERI_SYS_CALL_FLOOR :
    case VERI_SYS_CALL_CEIL :
        // This function expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Resolve the expression in the environment this was called in :
        if (expr) expr->Resolve(scope,environment) ;
        return ;
    case VERI_SYS_CALL_ATAN2 :
    case VERI_SYS_CALL_HYPOT :
    case VERI_SYS_CALL_POW :
        // This function expect one argument
        if (!_args || _args->Size()!=2) {
            if (_name) Error("%s expects %d arguments", _name, 2) ;
        }
        // Resolve the expression in the environment this was called in :
        if (expr) expr->Resolve(scope,environment) ;
        expr = (_args && _args->Size() > 1) ? (VeriExpression*)_args->At(1): 0 ;
        if (expr) expr->Resolve(scope,environment) ;
        return ;
    // Assertion functions (20.13)
    case VERI_SYS_CALL_PAST_GCLK :
    case VERI_SYS_CALL_FELL_GCLK :
    case VERI_SYS_CALL_CHANGED_GCLK :
    case VERI_SYS_CALL_RISING_GCLK :
    case VERI_SYS_CALL_STEADY_GCLK :
    case VERI_SYS_CALL_ROSE_GCLK :
    case VERI_SYS_CALL_STABLE_GCLK :
    case VERI_SYS_CALL_FUTURE_GCLK :
    case VERI_SYS_CALL_FALLING_GCLK :
    case VERI_SYS_CALL_CHANGING_GCLK :
        // Can have only one argument : expression
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Resolve the expression in the environment this was called in :
        if (expr) expr->Resolve(scope,environment) ;
        return ;
     case VERI_SYS_CALL_TYPEOF :
        // Can have only one argument : expression
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Argument can be expression or data type, use no environment
        if (expr) expr->Resolve(scope,VERI_UNDEF_ENV) ;
        return ;

    default :
        break ;
    }

    // unknown system function call
    // By default : Resolve the arguments with a very accepting environment
    FOREACH_ARRAY_ITEM(_args, i, expr) {
        if (!expr) continue ;
        // Issue 1736 is for system task arguments we need to use VERI_SYSTEM_TASK_ARG, because
        // some unresolvable identifiers (module names) can be present in some system task arguments.
        // System function arguments are also special, since test cases show that for example
        // memory identifiers can be accessed directly in an argument, while this is not allowed
        // for normal function arguments (in Verilog 2k). So use special 'environment' here too :
        expr->Resolve(scope, VERI_SYSTEM_TASK_ARG) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriMinTypMaxExpr::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    if (_min_expr) _min_expr->Resolve(scope, environment) ;
    if (_typ_expr) _typ_expr->Resolve(scope, environment) ;
    if (_max_expr) _max_expr->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriUnaryOperator::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // Unary operator resolving for SVA operators
    // If the incoming environment is VERI_PROPERTY/VERI_SEQUENCE and this operator
    // is non-sequence/property operator, the environment of arguments is
    // changed to VERI_EXPRESSION.
    //
    // If this operator is sequence/property operator and environment is non-sequence
    // or non-property, error message is produced.
    switch (_oper_type) {
    case VERI_NOT :
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        break ; // Don't change environment
    case VERI_FIRST_MATCH : // sequence/property operator
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV &&  environment!=VERI_PROPERTY_ENV && environment!=VERI_SEQUENCE_ENV && environment!=VERI_PROPERTY_ARG && environment!=VERI_SEQUENCE_ARG) {
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), "property/sequence expr") ;
        }
        // The argument of first-match operator is sequence_expr
        if (environment == VERI_PROPERTY_ENV || environment == VERI_PROPERTY_ARG) environment = VERI_SEQUENCE_ENV ;
        break ;
    case VERI_BEGIN : // VIPER #4776 : These are block event expression for covergroup, argument is block/task/function id
    case VERI_END :
        environment = VERI_UPWARD_SCOPE_NAME ;
        break ;
    case VERI_STRONG :
    case VERI_WEAK :
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        // VIPER #6386 : Operand of strong and weak operator is sequence expression
        environment = VERI_SEQUENCE_ENV ;
        break ;
    default : // Normal expression
        // If incoming environment is sequence/property, arguments should be expression
        if (environment == VERI_PROPERTY_ENV || environment == VERI_SEQUENCE_ENV) { // || environment == VERI_PROPERTY_ARG || environment == VERI_SEQUENCE_ARG) {
            environment = VERI_EXPRESSION ;
        }
        // VIPER #4229 : If incoming environment is property arg, argument should
        // be event control
        if (environment == VERI_PROPERTY_ARG || environment == VERI_SEQUENCE_ARG) environment = VERI_EVENT_CONTROL_UNDECL_USE_GOOD ;
        break ;
    }
    // VIPER #6876 : Operand cannot be streaming concat without casting
    if (_arg && _arg->IsStreamingConcat()) {
        Error("use casting to use streaming concatenation with other operators") ;
    }
    if (_arg) _arg->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriBinaryOperator::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // Default argument environment :
    // VIPER #4229 : If coming environment is VERI_PROPERTY_ARG, operands should
    // be resolved with VERI_EVENT_CONTROL environment
    //veri_environment left_env = (environment == VERI_PROPERTY_ARG) ? VERI_EVENT_CONTROL : environment ;
    //veri_environment right_env = (environment == VERI_PROPERTY_ARG) ? VERI_EVENT_CONTROL : environment ;
    veri_environment left_env = environment ;
    veri_environment right_env = environment ;
    // Binary operator resolving for SVA operators
    // If the incoming environment is VERI_PROPERTY_ENV/VERI_SEQUENCE_ENV and this operator
    // is non-sequence/property operator, the environment of arguments is
    // changed to VERI_EXPRESSION.
    //
    // If this operator is sequence/property operator and environment is non-sequence
    // or non-property, error message is produced.
    // If this is property operator and incoming environment is VERI_SEQUENCE_ENV, error
    // message is produced.

    // Made this change for Viper 6369:
    if (environment == VERI_CLOCKING_BLOCK) environment = VERI_UNDEF_ENV ;
    unsigned check_type = 0 ;
    switch (_oper_type) {
    case VERI_OVERLAPPED_IMPLICATION :
    case VERI_NON_OVERLAPPED_IMPLICATION :
    case VERI_OVERLAPPED_FOLLOWED_BY :
    case VERI_NON_OVERLAPPED_FOLLOWED_BY :
        // LRM 1800-2005 : 17.11.2 : |-> and |=> can appear only in a property.
        // Left side is a sequence, right side is a property.
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        // For these operators left operand is sequence expression
        left_env = VERI_SEQUENCE_ENV ;
        break ;
    case VERI_DISABLE :
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        left_env = VERI_EXPRESSION_UNDECL_USE_GOOD ; // Left argument of disable is expression
        right_env = VERI_PROPERTY_ENV ; // Right argument is property expression
        break ; // Don't change environment
    case VERI_CONSECUTIVE_REPEAT :
    case VERI_NON_CONSECUTIVE_REPEAT :
    case VERI_GOTO_REPEAT :
        // Repetition operators can be used in coverage spec
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_SEQUENCE_ENV && environment!=VERI_BIN_VAL_ENV && environment!=VERI_PROPERTY_ARG && environment!=VERI_SEQUENCE_ARG) {
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), "property/sequence expr") ;
        }
        if (_oper_type == VERI_CONSECUTIVE_REPEAT) {
            left_env = VERI_SEQUENCE_ENV ; // Left operand of consecutive repeat is sequence expression
        } else {
            left_env = VERI_EXPRESSION_UNDECL_USE_GOOD ; // Left operand of other repeatations is expression or dist
        }
        // Issue 2502 : even though LRM subscribes a constant expression, VCS and ModelSim both allow
        // property formals (variables) there. So go for that :
        // right_env = VERI_CONSTANT_EXPRESSION ; // For repeatation operators right argument is to be constant
        // VIPER #3370 : Issue error for real, but accept all constant expression
        right_env = VERI_INTEGRAL_CONSTANT ;
        break ;
    case VERI_INTERSECT :
    case VERI_THROUGHOUT :
    case VERI_WITHIN : // These operators can be used in both sequence and property
        right_env = VERI_SEQUENCE_ENV ; // Right operand should be sequence expression
        // For throughut operator left operand is expression_or_dist, but for others
        // left operand should be sequence expression
        if (_oper_type == VERI_THROUGHOUT) {
            left_env = VERI_EXPRESSION_UNDECL_USE_GOOD ;
        } else {
            left_env = VERI_SEQUENCE_ENV ;
        }
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_SEQUENCE_ENV && environment!=VERI_SEQUENCE_ARG && environment!=VERI_PROPERTY_ARG) {
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), "property/sequence expr") ;
        }
        break ;
    case VERI_AND :
    case VERI_PROPERTY_AND : // Should match with this branch if resolve is called again (#3370)
    case VERI_SEQ_AND :
        // Viper 2625
        switch (environment) {
            case VERI_PROPERTY_ENV:
                _oper_type = VERI_PROPERTY_AND ;
                break ;
            case VERI_SEQUENCE_ENV:
                _oper_type = VERI_SEQ_AND ;
                break ;
            default:
                break ;
        }
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_SEQUENCE_ENV && environment!=VERI_SEQUENCE_ARG && environment!=VERI_PROPERTY_ARG) {
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), "property/sequence expr") ;
        }
        break ; // Don't change environment
    case VERI_OR : // Allowed in property/sequence and event-or expression
    case VERI_PROPERTY_OR : // Should match with this branch if resolve is called again (#3370)
    case VERI_SEQ_OR :
        // Viper 2625
        // This creates a problem for sequence parameters which are OR-ed.
        // This OR also gets converted to VERI_PROPERTY_OR/VERI_SEQ_OR which
        // is wrong. This requires a fix in VeriPrettyPrint.cpp.
        // Need a fix for this in future.
        switch (environment) {
            case VERI_PROPERTY_ENV:
                _oper_type = VERI_PROPERTY_OR ;
                break ;
            case VERI_SEQUENCE_ENV:
                _oper_type = VERI_SEQ_OR ;
                break ;
            default:
                break ;
        }
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_SEQUENCE_ENV && environment!=VERI_EXPRESSION && environment!=VERI_EXPRESSION_UNDECL_USE_GOOD && environment != VERI_ABORT_EXPRESSION && environment!=VERI_LET_EXPRESSION && environment!=VERI_PROCE_CONT_ASSIGN_VAL && environment!=VERI_WAIT_EXP && environment!=VERI_EVENT_CONTROL && environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD && environment != VERI_SEQ_PROP_INIT && environment!=VERI_PROPERTY_ARG && environment!=VERI_SEQUENCE_ARG) {
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), "property/sequence/event-or expr") ;
        }
        break ; // Don't change environment
    case VERI_INSIDE : // VIPER #4208 : right operand of inside operator is value range list
        left_env = VERI_EXPRESSION ;
        // Keep VERI_PARAM_INITIAL_EXP. Viper 4603
        if (environment != VERI_PARAM_INITIAL_EXP) right_env = VERI_VALUE_RANGE ; // right operand value range list
        check_type = 1 ;
        break ;
    case VERI_CASEEQ :
    case VERI_CASENEQ :
    case VERI_WILDEQUALITY :
    case VERI_WILDINEQUALITY :
        // VIPER #4803 : 4 state operators are not allowed in constraint expression
        if ((environment == VERI_CONSTRAINT_EXPR) || (environment == VERI_INLINE_CONSTRAINT_EXPR)) {
            Error("operator %s is not allowed in %s", PrintToken(_oper_type), PrintEnvironment(environment)) ;
        }
        break ;
    // VIPER #5710 : VCS Compatibility issue (if_in_property), abort properties
    case VERI_ACCEPT_ON :
    case VERI_REJECT_ON :
    case VERI_SYNC_ACCEPT_ON :
    case VERI_SYNC_REJECT_ON :
        // Left side is an expression, right side is a property.
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        // For these operators left operand is expression or dist and right is property expr
        left_env = VERI_ABORT_EXPRESSION ;
        right_env = VERI_PROPERTY_ENV ;
        break ;
    // VIPER #5710 : VCS Compatibility issue (iff_in_property)
    case VERI_IFF :
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_EXPRESSION && environment!=VERI_EXPRESSION_UNDECL_USE_GOOD && environment != VERI_ABORT_EXPRESSION && environment!=VERI_LET_EXPRESSION && environment!=VERI_PROCE_CONT_ASSIGN_VAL && environment!=VERI_WAIT_EXP && environment!=VERI_EVENT_CONTROL && environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD && environment != VERI_SEQ_PROP_INIT && environment!=VERI_PROPERTY_ARG && environment!=VERI_SEQUENCE_ARG) {
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), "property/property_instance/event-or expr") ;
        }
        // 'iff' can be part of event expression or property expression
        break ; // Don't change environment
    case VERI_IMPLIES :
    case VERI_UNTIL :
    case VERI_S_UNTIL :
    case VERI_UNTIL_WITH :
    case VERI_S_UNTIL_WITH :
        // Left side is property, right side is a property.
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        // For these operators left operand and right operand are property expr
        left_env = VERI_PROPERTY_ENV ;
        right_env = VERI_PROPERTY_ENV ;
        break ;
    case VERI_NEXTTIME :
    case VERI_S_NEXTTIME :
    case VERI_EVENTUALLY :
    case VERI_S_EVENTUALLY :
    case VERI_ALWAYS :
    case VERI_S_ALWAYS :
        if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment!=VERI_PROPERTY_ENV && environment!=VERI_PROPERTY_ARG) { // property only operator
            Error("operator %s is only allowed in a %s", PrintToken(_oper_type), PrintEnvironment(VERI_PROPERTY_ENV)) ;
        }
        left_env = VERI_EXPRESSION_UNDECL_USE_GOOD ;
        right_env = VERI_PROPERTY_ENV ;
        break ;
    default : // Normal expression
        // If incoming environment is sequence/property, arguments should be expression
        if (environment == VERI_PROPERTY_ENV || environment == VERI_SEQUENCE_ENV || environment == VERI_PROPERTY_ARG || environment == VERI_SEQUENCE_ARG) {
            // (void) CheckType(0, READ) ; // condition is context independent Viper 4946
            //VeriTypeInfo * vtip =  CheckType(0, READ) ; // condition is context independent Viper 4946
            //delete vtip ; //JJ: assign to ptr and delete return
            left_env = VERI_EXPRESSION ;
            right_env = VERI_EXPRESSION ;
            check_type = 1 ;
        } else if ((environment == VERI_CONSTRAINT_EXPR) || (environment == VERI_INLINE_CONSTRAINT_EXPR)) { // VIPER #5154
            check_type = 1 ;
            //VeriTypeInfo *t = CheckType(0, READ) ;
            //delete t ;
        }
        break ;
    }
    // VIPER #6876 : Operand cannot be streaming concat without casting
    if ((_left && _left->IsStreamingConcat()) || (_right && _right->IsStreamingConcat())) {
        Error("use casting to use streaming concatenation with other operators") ;
    }

    // Disallow ONEHOT encoding on any operator, except 'equal'.
    // For that, we have a proper way to elaborate with onehot values.
    switch (_oper_type) {
    case VERI_CASEEQ:
    case VERI_CASENEQ:
    case VERI_LOGEQ:
    case VERI_LOGNEQ:
        if (_left) _left->ResolveOnehot(scope, left_env) ;
        if (_right) _right->ResolveOnehot(scope, right_env) ;
        // Link the expressions by finding a full identifier variable on either side :
        if (_left && _right && _left->FullId()) {
            _right->LinkOnehot(_left->FullId()) ;
        } else if (_left && _right && _right->FullId()) {
            _left->LinkOnehot(_right->FullId()) ;
        }
        break ;
    case VERI_OVERLAPPED_IMPLICATION :
        {
            // Just a normal operator call :
            if (_left) _left->Resolve(scope, left_env) ;
            VeriExpression *left_clk_expr = _sva_clock_expr ;
            if (_right) _right->Resolve(scope, right_env) ;
            // Viper 6168: Pick the left most clock expr from _right
            VeriExpression *right_clk_expr = (_right && _right->IsClockedSequence()) ? _right->GetEventExpression() : 0 ;

            // VIper 4181. Issue error for change in clock across |-> operator
            // It is difficult to do the check for arbitrary expression. Only check
            // for VeriIdRefs. Also note the _right must be a clocked sequence

            // Viper 6168: System Verilog 2009 allow multiclock properties to have different clocks for
            // if and then or else expression. LRM Section 16.14.2 of 2009 LRM previously in 2005 LRM
            // Section 17.12.2 dis-allowed this type of properties.
            if (!IsSystemVeri2009OrLater() && left_clk_expr && right_clk_expr && right_clk_expr!=left_clk_expr && _right && _right->IsClockedSequence()) {
                VeriIdDef *left_clk_id = left_clk_expr->FullId() ;
                VeriIdDef *right_clk_id = right_clk_expr->FullId() ;
                if ((left_clk_expr->GetEdgeToken() != right_clk_expr->GetEdgeToken()) || (left_clk_id && right_clk_id && right_clk_id!=left_clk_id)){
                    Error("the clock flow cannot change in the RHS of %s operator", PrintToken(_oper_type)) ;
                }
            }
        }
        break ;
    default :
        // Just a normal operator call :
        if (_left) _left->Resolve(scope, left_env) ;
        if (_right) _right->Resolve(scope, right_env) ;
    }

    if (VERI_EQUAL_ASSIGN == _oper_type && !IsSystemVeri()) Warning("assignment sub expression is allowed only in System Verilog mode") ; // Viper #5674

    if (check_type) {
        VeriTypeInfo * vtip =  CheckType(0, READ) ; // condition is context independent Viper 4946
        delete vtip ; //JJ: assign to ptr and delete return
    }
    // For SystemVerilog, assignment operators require that
    // the result be (blocking) assigned back to the LHS.
    // So we should also Resolve LHS with the blocking-assignment environment :
    switch (_oper_type) {
    case VERI_EQUAL_ASSIGN :
    case VERI_PLUS_ASSIGN :
    case VERI_MIN_ASSIGN :
    case VERI_MUL_ASSIGN :
    case VERI_DIV_ASSIGN :
    case VERI_MOD_ASSIGN :
    case VERI_AND_ASSIGN :
    case VERI_OR_ASSIGN :
    case VERI_XOR_ASSIGN :
    case VERI_LSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
        // Assignment operators not allowed in constraint expression/sequence/property, error out
        // LRM 7.3 : also not allowed in a event expression
        if (environment==VERI_EVENT_CONTROL || environment == VERI_EVENT_CONTROL_UNDECL_USE_GOOD || environment == VERI_SEQ_PROP_INIT || environment == VERI_CONSTRAINT_EXPR || environment == VERI_INLINE_CONSTRAINT_EXPR || environment == VERI_SEQUENCE_ENV || environment == VERI_PROPERTY_ENV || environment==VERI_PROPERTY_ARG || environment==VERI_SEQUENCE_ARG) {
            Error("operator %s is not allowed in %s", PrintToken(_oper_type), PrintEnvironment(environment)) ;
        }

        // If environment is constant expression, lvalue should be genvar
        if (_left) _left->Resolve(scope, (environment==VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_INITIAL_EXP) ? VERI_GENVAR_LVALUE : VERI_REG_LVALUE) ; // assignment operator
        break ;
    // same thing for the in-expression ++/-- operations :
    case VERI_INC_OP :
    case VERI_DEC_OP :
        // increment/decrement operators not allowed in constraint/sequence/property expression, error out
        if (environment == VERI_CONSTRAINT_EXPR || environment == VERI_INLINE_CONSTRAINT_EXPR || environment == VERI_SEQUENCE_ENV || environment == VERI_PROPERTY_ENV) {
            Error("operator %s is not allowed in %s", PrintToken(_oper_type), PrintEnvironment(environment)) ;
        }

        if (_left) _left->Resolve(scope, (environment==VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_INITIAL_EXP) ? VERI_GENVAR_LVALUE: VERI_REG_LVALUE) ; // <expr>++ or <expr>-- assignment operator
        if (_right) _right->Resolve(scope, (environment==VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_INITIAL_EXP) ? VERI_GENVAR_LVALUE: VERI_REG_LVALUE) ; // ++<expr> or --<expr> assignment operator
        break ;
    default :
        break ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriQuestionColon::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // VIPER #5635 : Condition of ?: operator will always be expression
    if (_if_expr) _if_expr->Resolve(scope, VERI_EXPRESSION) ;
    // check type of condition
    VeriTypeInfo *cond_type = (_if_expr) ? _if_expr->CheckType(0, READ) : 0 ; ; // condition is context independent
    // VIPER #6478: Produce error if condition is unpacked type
    if (cond_type && !cond_type->IsTypeParameterType() && !cond_type->IsUnresolvedNameType() && (cond_type->IsUnpackedArrayType() || (cond_type->IsStructure() && cond_type->IsUnpacked())) && !cond_type->IsClassType() && !cond_type->IsVirtualInterface() && _if_expr) {
        _if_expr->Error("illegal context for %s", "unpacked expression") ;
    }
    delete cond_type ;
    // VIPER #6876 : Operand cannot be streaming concat without casting
    if (_if_expr && _if_expr->IsStreamingConcat()) {
        Error("use casting to use streaming concatenation with other operators") ;
    }

    VeriScope *then_scope = (_if_expr && _if_expr->GetScope()) ? _if_expr->GetScope() : scope ;
    if (_then_expr) _then_expr->Resolve(then_scope, environment) ;
    if (_else_expr) _else_expr->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriEventExpression::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2180: Event expression is only allowed in event control, sequence and property.
    if (environment != VERI_EVENT_CONTROL && environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD && environment != VERI_SEQ_PROP_INIT && environment != VERI_SEQUENCE_ENV && environment != VERI_PROPERTY_ENV && environment!= VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment != VERI_PROPERTY_ARG && environment != VERI_PATH_EXPRESSION /*vcs verific incompatibility*/ && environment != VERI_SEQUENCE_ARG && environment != VERI_PORT_CONNECTION) {
        Error("event expression is not allowed here") ;
    }
    // First one defines the expression on which the event from which the event will be taken.
    // (Either posedge, negedge or both edges, depending on the '_edge' token.
    if (_expr) _expr->Resolve(scope, environment) ;

    // Adding following warning for Viper #4063
    VeriIdDef *expr_id = _expr ? _expr->GetId() : 0 ;
    if (((environment == VERI_EVENT_CONTROL) || (environment == VERI_EVENT_CONTROL_UNDECL_USE_GOOD) || (environment == VERI_SEQ_PROP_INIT)) && _edge && expr_id && expr_id->IsEvent()) Warning("illegal edge on named event") ;

    // Second one (iff_condition) only set for SV. It is a normal (level) condition, so resolve with VERI_EXPRESSION ?.
    if (_iff_condition) _iff_condition->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;

    // Chandles cannot be used in event expression :
    VeriTypeInfo *expr_type = (_expr) ? _expr->CheckType(0, READ): 0 ;
    if (expr_type && expr_type->IsChandleType()) {
        Warning("illegal reference to chandle type") ;
    } else if (expr_type && !expr_type->IsSingularType() && !expr_type->IsUnresolvedNameType() && !expr_type->IsTypeParameterType()) {
        // VIPER #3527 : Make this error to warning.
        Warning("event expressions must result in a singular type") ;
    }
    // VIPER #5253 : Produce error if posedge/negedge is applied on real expression :
    if ((_edge == VERI_POSEDGE || _edge == VERI_NEGEDGE) && expr_type && expr_type->IsRealType()) {
        Warning("posedge or negedge on real is illegal") ;
    }
    delete expr_type ;

    // iff condition cannot have reference to event
    VeriTypeInfo *iff_expr_type = (_iff_condition) ? _iff_condition->CheckType(0, READ) : 0 ;
    if (iff_expr_type && !iff_expr_type->IsTypeParameterType() && !iff_expr_type->IsUnresolvedNameType() && iff_expr_type->IsEventType()) {
        Error("illegal reference to event type") ;
    }
    delete iff_expr_type ;
}

void VeriPortConnect::Resolve(VeriScope *scope, veri_environment environment)
{
    // Pass environment as-is.
    // PortConnect is used both in instantiations (VERI_PORT_CONNECTION)
    // as well as for module port definition (port expressions) (VERI_PORT_EXPRESSION).

    // Don't resolve the port ref name. We don't know the instantiated module yet,
    // and for module ports, there is no declaration for these formals.

    if (_connection) _connection->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriPortOpen::Resolve(VeriScope *scope, veri_environment environment)
{
    // Nothing to resolve down here..
    // Issue 2092 : give a warning for empty port declaration (if this is a port expression) :
    if (environment == VERI_PORT_EXPRESSION || environment == VERI_PATH_EXPRESSION) {
        Warning("empty port in module declaration") ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriAnsiPortDecl::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // 3/2006: This routine is no longer used, now that we call ResolveAnsiPortDecl on ansi port lists.

    // Resolve the data type :
    if (_data_type) _data_type->Resolve(scope, VERI_TYPE_ENV) ;

    // VIPER #6177: Check for real type ports in Verilog95/2000 mode:
    if ((IsVeri95() || IsVeri2001()) && _data_type && (_data_type->Type()==VERI_REAL)) {
        VeriIdDef *scope_owner = (scope) ? scope->GetOwner() : 0 ;
        // Error out only for module ports, task/function ports can be of type real:
        if (scope_owner && scope_owner->IsModule()) Error("real type port is not allowed in this mode of verilog") ;
    }

    // VIPER #3786 : Mark data type as virtual interface :
    if (GetQualifier(VERI_VIRTUAL) && _data_type) _data_type->SetIsVirtualInterface() ; // virtual interface object
    // Resolve the (declared) identifiers :
    VeriIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        // Viper 6895 : Check for correct dimensions in instantiations
        // It may be noted that vhdl types of the form
        // "subtype bit_arr is std_ulogic_vector" can be mapped to
        // "bit_arr b[1:0]" or "bit_arr b" or "bit_arr [1:0] b"

        VeriIdDef *type_id = _data_type ? _data_type->GetId() : 0 ;
        if (type_id && type_id->IsConvertedVhdlType() && _data_type) {
            unsigned datatype_dim_cnt = _data_type->GetDimensions() ? _data_type->PackedDimension() : 0 ;
            unsigned id_dim_cnt = id->GetDimensions() ? id->UnPackedDimension() : 0 ;
            unsigned num_unconstraint = type_id->GetUnpackedUnconstraintDimCount() + type_id->GetPackedUnconstraintDimCount() ;
            // Check for total number of dimensions is at-least not greater than the number of unconstrained dimensions
            // types with unconstrained dimensions are still allowed. Example above.
            if ((id_dim_cnt + datatype_dim_cnt) > num_unconstraint) {
                id->Error("incorrect number of dimensions in instantiation") ;
            }
            // If the vhdl type has unpacked unconstrained dimension then there should be at-least that many dimensions in instantiation
            if (id_dim_cnt < type_id->GetUnpackedUnconstraintDimCount()) {
                id->Error("number of unpacked dimensions %d does not match the number of dimensions %d of VHDL unconstrained array",id->Dimension(),  type_id->GetUnpackedUnconstraintDimCount()) ;
                if (_data_type->PackedDimension()) id->Error("illegal to define packed dimensions for vhdl unconstrained arrays") ;
            }
        }
#endif

        // Set used/assigned based on port direction, before we hit resolving.
        // Needed for ANSI ports, since they don't have a port expression.
        switch (id->Dir()) {
        case VERI_INPUT  : id->SetAssigned() ; break ;
        case VERI_OUTPUT : id->SetUsed() ; break ;
        case VERI_INOUT  : id->SetUsed() ; id->SetAssigned() ; break ;
        // VIPER #4425 : Direction VERI_CONST refers to const ref port
        case VERI_CONST  : id->SetIsConstant() ; break ;
        default : break ;
        }

        id->Resolve(scope, VERI_UNDEF_ENV) ; // port decl.
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

// Ansi Port Decl resolving : need previous type and direction to be set on ids.
// If not a ansi port decl, then resolve as PORT_EXPRESSION.
void
VeriExpression::ResolveAnsiPortDecl(VeriScope *scope, unsigned * /*previous_direction*/, unsigned * /*previous_kind*/, unsigned /*default_kind*/, unsigned * /*previous_type*/, unsigned /*default_type*/)
{
    Resolve(scope, VERI_PORT_EXPRESSION) ;
}

void
VeriAnsiPortDecl::ResolveAnsiPortDecl(VeriScope *scope, unsigned *previous_direction, unsigned *previous_kind, unsigned default_kind, unsigned *previous_type, unsigned default_type)
{
    // Pick up the data type and direction (if any) of this port decl :
    VeriDataType *data_type = _data_type ;
    unsigned direction = _dir ;

    // Resolve the data type
    if (_data_type) _data_type->Resolve(scope, VERI_TYPE_ENV) ;

    // VIPER #8195: Check for data type netdata declaration:
    // LRM IEEE-1800-2012: Sec-6.7 : Certain restrictions apply to the data type of a net for net declaration.
    // i) a 4 state intergral type, including a packed array or packed struct
    // ii) an unpacked array or unpacked struct, where each element has a valid data type for a net
    // But it does not state this for port declaration. But for compatibility with ModelSim we error out here.
    if (data_type && data_type->IsNetDataType()) {
        unsigned is_analog_net_decl = 0 ;

        VeriTypeInfo *type_info = data_type->CreateType(0) ;
        if (type_info && !is_analog_net_decl) type_info->CheckNetDataType(data_type, 0) ;
        delete type_info ;
    }

    // VIPER #6177: Check for real type ports in Verilog95/2000 mode:
    if ((IsVeri95() || IsVeri2001()) && _data_type && (_data_type->Type()==VERI_REAL)) {
        VeriIdDef *scope_owner = (scope) ? scope->GetOwner() : 0 ;
        // Error out only for module ports, task/function ports can be of type real:
        if (scope_owner && scope_owner->IsModule()) Error("real type port is not allowed in this mode of verilog") ;
    }

    // VIPER #5949: Set and modify the _type and _kind field of the ports
    // extract the 'type' of declaration (if any).
    unsigned type = (data_type) ? data_type->Type() : 0 ;
    unsigned kind = 0 ;
    unsigned set_from_qualifier = 0 ;

    // VIPEr #5949: For 'var' type variable set the _kind field with 'var'
    if (_qualifier == VERI_VAR) {
        if (!type) type = VERI_LOGIC ; // Viper #5511
        kind = VERI_VAR ;
        set_from_qualifier = 1 ;
    }

    // VIPER #5949: Rules for determining port type kind and direction:
    // For Verilog 2000 and System Verilog, type and direction depend on previous type/direction.
    // LRM 19.8 (for module ports) and 12.2 (for task/function ports) (System Verilog 1800 LRM).
    // According to LRM:P1800-2009(Section: 23.2.2.3: Rules for determining port kind type and direction):
    //  (1) no direction, no data type, no kind : inherit these three from previous port.
    //  (2) no direction, but a data type : inherit direction from previous port.
    //  (3) direction present, but no data type, then the data type is default data type (already a 2000 feature).
    // and kind to default_kind :
    // If the port kind is omitted it would be determined as:
    //  (1) For input and inout ports, the port kind shall default to a net of default net type
    // the net type can be changed using the `default_nettype compiler directive.
    //  (2) For output ports, the default port kind depends on how the data type is specified:
    //      If the data type is omitted or declared with the implicit_data_type syntax, then the kind is
    // is default to a net of default net type.
    //      If the data type is declared with the explicit data_type syntax, the port kind shall be
    // default to a variable.
    //  (3) A ref port is always a variable.

    // So direction is always inherited. type and kind only inherited if there is no direction.

    // inherit 'type' and 'kind' from previous port if there is no direction. Otherwise, inherit 'default' type and kind:
    unsigned interface_port = 0 ;
    if (!type) {
        // No base-type info available. Set default:
        // For system verilog, catch generic and other 'interface' ports first.
        // These are unresolved at this point, and they do not return a 'type'.
        if ((direction==VERI_INTERFACE || (!direction && data_type && data_type->GetTypeName() && !data_type->GetId()))) { /* (1) */
            // (named, unresolved (no id) data type). Must be interface port, because normal types are resolved.
            // Don't set a direction. Just keep 'type' at 0.
            // direction = VERI_INTERFACE ;
            // type = 0 ; // keep 'type' at 0.
            interface_port = 1 ;
        } else
        if (direction) {
            // something like "input foo;".
            // Set default type and kind onto the id :
            if (IsVeri95() || IsVeri2001()) {
                type = default_kind ;
                kind = default_kind ;
            } else {
                // VIPER #6527 : Insert the fix of VIPER #6438 here.
                // VIPER #6438 : If kind is set from default_nettype and that is none, set default type as none
                type = (default_kind == VERI_NONE) ? default_kind: default_type ;
                if (direction == VERI_INPUT || direction == VERI_INOUT || direction == VERI_OUTPUT) {
                    kind = default_kind ;
                }
                else if (direction == VERI_REF) {
                    kind = VERI_VAR ;
                }
            }

            // Check if there is a 'named' type name here.
            // If so, we should error out on the type name (since types must be declared before used) :
            // We should actually issue this message when doing _data_type->Resolve(),
            // but we cannot error out there (on TYPE_ENV environment) because there could be an 'interface' in the data type.
            if (data_type && data_type->GetTypeName() && !data_type->GetId()) {
                // VIPER #5344 : Do not produce error for virtual interface :
                if (!GetQualifier(VERI_VIRTUAL) && !(_data_type && _data_type->IsVirtualInterface())) {
                    // VIPER #7350: In relaxed checking mode produce warning for
                    // only module's unknown type port with direction. For such
                    // port in subprogram/sequence/property, we will produce error
                    if (InRelaxedCheckingMode() && scope && !scope->GetSubprogram() && !scope->GetSequenceProperty()) {
                        data_type->Warning("%s is not declared",data_type->GetName()) ;
                    } else {
                        data_type->Error("%s is not declared",data_type->GetName()) ;
                    }
                }
            }
        } else {
            // No direction, and no base type info : something like port decl "signed [3:0] b".
            // This can legally only happen in System Verilog, where the previous port type and kind is inherited:
            // Inherit 'type' and 'kind' from previous port.
            type = (previous_type) ? *previous_type : 0 ;
            kind = (previous_kind) ? *previous_kind : 0 ;
        }
    }

    // 'direction' is always inherited from previous port.
    if (!direction) {
        // No direction, but some base type given : "wire foo;"
        // Inherit the direction from 'previous' direction.
        // VIPER #6651: Set direction of interface ports as inout, though it should not matter
        // but make it consistent. Interface/modport item direction should be interesting to check.
        direction = (interface_port) ? VERI_INOUT : ((previous_direction) ? *previous_direction : 0) ;
    }

    // VIPER #3236 : Check whether we are processing ports of automatic task/function
    // If so then marked these ports as automatic.
    VeriIdDef *subprogram = (scope) ? scope->GetSubprogram(): 0 ;
    unsigned is_automatic = (subprogram) ? subprogram->IsAutomatic() : 0 ;
    // VIPER #3786 : Mark data type virtual checking qualifier
    if (GetQualifier(VERI_VIRTUAL)) {
        if (_data_type) _data_type->SetIsVirtualInterface() ;
        // VIPER #4640 : Virtual interface type objects cannot be automatic
        is_automatic = 0 ;
    } else if (_data_type && _data_type->IsVirtualInterface()) {
        is_automatic = 0 ; // VIPER #4640 : Virtual interface type objects cannot be automatic
    }

    // Determine if this is function/task port
    VeriIdDef *owner = scope ? scope->GetOwner() : 0 ;
    unsigned is_function_task = owner ? owner->IsFunction() || owner->IsTask() : 0 ;
    // Set flag for class/checker:
    VeriIdDef *data_id = data_type ? data_type->GetId() : 0 ;
    unsigned is_class_type = data_id ? data_id->IsClass() : 0 ;
    unsigned is_checker = data_id ? data_id->IsChecker() : 0 ;

    // Set kind and type if data_type is present:
    // Do not modify the type and kind for interface_port and
    // for variable with 'var' qualifier:
    if (data_type && !interface_port && !set_from_qualifier) {
        if (data_type->IsNetDataType()) { // For net data type
            kind = data_type->NetType() ;
            VeriDataType *d_type = data_type->GetDataType() ;
            type = d_type ? d_type->Type() : 0 ;
        } else {
            if (is_class_type || is_checker) {
                kind = data_type->Type() ;
            } else if (!data_type->IsNetType()) { // data_type is non-net type ie. 'reg sig'
                if (IsVeri95() || IsVeri2001() || IsAms()) { // set type of data_type to kind
                     // VIPER #7309: Also check for ams: if in ams mode populate kind field with the type of the date type
                    kind = data_type->Type() ;
                } else { // for system verilog and forth if kind is not set already set kind as follows:
                    if (!kind) {
                        // According to LRM-2009 default kind of input port
                        // should be default_net_type but for backward compatibility
                        // and as ModelSim does we are treating input ports as register
                        // type or logic type not as net type.
                        switch(direction) {
                        case VERI_INPUT  : { // kind = VERI_REG ;
                                           // VIPER #8077: Modelsim has changed its behaviour: some testcases
                                           // are attached to the viper where we faced that in some cases
                                           // ModelSim does not error out but in some other similar situation it does.
                                           // So we set _kind field according to LRM IEEE-1800-2012(Section: 23.2.2.3):
                                           // set default kind to _kind field for input port.
                                           // VIPER #8195: Set kind checking data type: If it is 4-state the kind will be
                                           // set to default kind otherwise it will be set to variable. It is not acrroding
                                           // to lrm, but for compatibility with ModelSim we set kind field checking data type.
                                           // kind = default_kind ;
                                           VeriTypeInfo *data_type_info = data_type->CreateType(0) ;
                                           if (data_type_info) {
                                               if (data_type_info->Is4State() && data_type_info->Type() != VERI_REG) {
                                                   kind = default_kind ;
                                               } else {
                                                   kind = VERI_VAR ;
                                               }
                                           }
                                           delete data_type_info ;
                                           break ;
                                           }
                        case VERI_OUTPUT : //if (IsVeri95() || IsVeri2001()) {
                                            //  kind = VERI_REG ;
                                           //} else {
                                              kind = VERI_VAR ;
                                           //}
                                           break ;
                        case VERI_INOUT  : kind = default_kind ; break ;
                        case VERI_REF    : kind = VERI_VAR ; break ;
                        default: break ;
                        }
                    }
                }
            } else { // for net-type port ie, 'wire sig'
                if (IsVeri95() || IsVeri2001()) {
                    type = data_type->Type() ;
                } else {
                    type = default_type ;
                }
                // VIPER #6892 : Set kind from data type
                if (!kind) kind = data_type->Type() ;
            }
        }
    }
    // Pick up the identifier(s) of the ansi port list :
    unsigned j ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, j, id) {
        if (!id) continue ;
        // Set (implicit or explicit) direction and type info onto the identifier:
        // id->SetDataType(data_type) ; // this is done before.
        id->SetDir(direction) ;
        if (kind) id->SetKind(kind) ; // set kind
        id->SetType(type) ;
        if (is_automatic) id->SetIsAutomatic() ; // Mark as automatic

        unsigned is_multiple_decl = _data_type != id->GetDataType() ;

        // Viper 5667: Issue error for initialization of not reg ports in declaration
        // 1364 LRM A.2.1.2 and P1800 lrm A2.6/2.7 and A2.1.2
        // FIXME : We do not error out for multiple declarations in ports
        // Forexample Verific do not error out for output o = 1'b1; wire o ;
        // this is because it cannot distinguish between the above style of
        // declaration and declaration like output o ; wire o =1'b1 or like
        // wire o =1'b1 ; output o as we only maintain a single definition of
        // id o (the first one) and the initial value is stored on the id.

        VeriDataType *id_data_type = id->GetDataType() ;
        unsigned is_null_type = !id_data_type || !id_data_type->Type() ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
        // Viper 6895 : Check for correct dimensions in instantiations
        // It may be noted that vhdl types of the form
        // "subtype bit_arr is std_ulogic_vector" can be mapped to
        // "bit_arr b[1:0]" or "bit_arr b" or "bit_arr [1:0] b"

        VeriIdDef *type_id = id_data_type ? id_data_type->GetId() : 0 ;
        if (type_id && type_id->IsConvertedVhdlType() && id_data_type) {
            unsigned datatype_dim_cnt = id_data_type->GetDimensions() ? id_data_type->PackedDimension() : 0 ;
            unsigned id_dim_cnt = id->GetDimensions() ? id->UnPackedDimension() : 0 ;
            unsigned num_unconstraint = type_id ? type_id->GetUnpackedUnconstraintDimCount() + type_id->GetPackedUnconstraintDimCount() : 0 ;
            // Check for total number of dimensions is at-least not greater than the number of unconstrained dimensions
            // types with unconstrained dimensions are still allowed. Example above.
            if ((id_dim_cnt + datatype_dim_cnt) > num_unconstraint) {
                id->Error("incorrect number of dimensions in instantiation") ;
            }
            // If the vhdl type has unpacked unconstrained dimension then there should be at-least that many dimensions in instantiation
            if (id_dim_cnt < type_id->GetUnpackedUnconstraintDimCount()) {
                id->Error("number of unpacked dimensions %d does not match the number of dimensions %d of VHDL unconstrained array",id->Dimension(),  type_id->GetUnpackedUnconstraintDimCount()) ;
                if (id_data_type->PackedDimension()) id->Error("illegal to define packed dimensions for vhdl unconstrained arrays") ;
            }
        }
#endif

        if (!is_multiple_decl && (id->IsNet() || is_null_type) && id->GetInitialValue() && id->IsPort()) {
            if (IsSystemVeri() && GetDir()) {
                // VIPER #7990 : Produce error if owner of scope exists. For method
                // prototypes owner will be null
                if (owner && !is_function_task) id->Error("invalid initialization in declaration") ;
            } else {
                // VIPER #7990 : Produce error if owner of scope exists. For method
                // prototypes owner will be null
                if (owner && _dir == VERI_OUTPUT) id->Error("invalid initialization in declaration") ;
            }
        }

        // Set used/assigned based on port direction, before we hit resolving.
        // Needed for ANSI ports, since they don't have a port expression.
        switch (id->Dir()) {
        case VERI_INPUT  : id->SetAssigned() ; break ;
        case VERI_OUTPUT : id->SetUsed() ; break ;
        case VERI_INOUT  : id->SetUsed() ; id->SetAssigned() ; break ;
        // VIPER #4425 : Direction VERI_CONST refers to const ref port
        case VERI_CONST  : id->SetIsConstant() ; break ;
        default : break ;
        }

        // Resolve identifier (initial value etc) :
        id->Resolve(scope, VERI_UNDEF_ENV) ; // port decl.

        if (owner && owner->IsChecker() && id->IsInterfacePort()) id->Error("checker formal argument '%s' may not be of interface type", id->Name()) ; // // LRM 2009, 17.2
    }

    // Update 'previous' with new kind, data types and direction :
    // VIPER #6651: Do not update direction for interface ports, other tools do not.
    if (previous_direction && !interface_port) *previous_direction = direction ;
    if (previous_type) *previous_type = type ;
    if (previous_kind && kind) *previous_kind = kind ;
}

void VeriTimingCheckEvent::Resolve(VeriScope *scope, veri_environment environment)
{
    // Changed for Viper# 3288, 3289
    if (_terminal_desc) {
        if (environment == VERI_TIMING_CHECK_TERMINAL_DESCRIPTOR) {
            _terminal_desc->Resolve(scope, VERI_EXPRESSION) ;
            VeriIdDef *id = _terminal_desc->GetId() ;
            if (!id) {
                Warning("illegal expression in target") ;
            } else if (!(id->IsNet() || id->IsReg())) {
                Warning("no definition for port %s", id->Name()) ; // VIPER #8172
            }
        } else if (environment == VERI_TIMING_CHECK_REGISTER_IDENTIFIER) {
            _terminal_desc->Resolve(scope, VERI_EXPRESSION) ;
            VeriIdDef *id = _terminal_desc->GetId() ;
            if (!id || !id->IsReg()) {
                Warning("notifier variable %s must be a single bit register", id ? id->Name() : "") ;
            }
        } else {
            _terminal_desc->Resolve(scope, environment) ;
        }
    }
    if (_condition) _condition->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriRange::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_part_select_token==VERI_PARTSELECT_UP || _part_select_token==VERI_PARTSELECT_DOWN) {
        // VIPER #3270 : Illegal partselect in declaration
        if (environment==VERI_TYPE_ENV) Error("part-select %s is not allowed in declaration", VeriNode::PrintToken(_part_select_token)) ;
        // base expression (left) does not need to be constant.
        // VIPER 3253 : if environment requires a constant expression, then even in +: or -: both sides must be constant.
        // Maybe left side always follows environment ?
        if (_left)  _left->Resolve(scope, (environment==VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_INITIAL_EXP || environment == VERI_PARAM_INITIAL_EXP) ? environment : VERI_EXPRESSION) ;
        veri_environment right_environment = VERI_CONSTANT_EXPRESSION ;
        // VIPER #4888: When coming environment is VERI_VALUE_RANGE, right operand can be expression
        if (environment==VERI_VALUE_RANGE) right_environment = VERI_EXPRESSION ;
        if (_right) _right->Resolve(scope, right_environment) ;
    } else if (environment==VERI_BIN_VAL_ENV) {
        if (_left)  _left->Resolve(scope, VERI_BIN_VAL_ENV) ;
        if (_right) _right->Resolve(scope, VERI_BIN_VAL_ENV) ;
    } else if (environment==VERI_INTEGRAL_CONSTANT) { // It is cycle delay range
        // VIPER 2539 : Only integral constant is allowed
        if (_left)  _left->Resolve(scope, environment) ;
        if (_right) _right->Resolve(scope, environment) ;
    } else if (environment==VERI_VALUE_RANGE) { // VIPER #4124 : Value range of distribution
        if (_left)  _left->Resolve(scope, VERI_EXPRESSION) ;
        if (_right) _right->Resolve(scope, VERI_EXPRESSION) ;
    } else if (environment==VERI_BINS_INIT_VALUE) { // VIPER #5871 : range exp in VeriBinsId initialization
        if (_left)  _left->Resolve(scope, VERI_BINS_INIT_VALUE) ;
        if (_right) _right->Resolve(scope, VERI_BINS_INIT_VALUE) ;
    } else {
        // Other type of range : Both sides must be constant :
        // In System verilog dimension of an associative array can contain
        // data type. So allow constant expression and data type for _left
        // and _right.
        if (_left)  _left->Resolve(scope, VERI_PARAM_VALUE) ;
        if (_right) _right->Resolve(scope, VERI_PARAM_VALUE) ;

        // VIPER #7581: Error out if left/right expression is not constant
        if (_left && _right && (!_left->IsConstExpr() || !_right->IsConstExpr())) {
            // If the expression is a hiername, we do not produce error
            VeriIdDef *module_id = (scope) ? scope->GetContainingModule() : 0 ;
            if ((!_left->IsHierarchicalName() && !_right->IsHierarchicalName()) &&
                // VIPER #7657: Do not produce this error if we are inside root module
                // and within active parser state (parsing is not yet over). The reason:
                // We do not know if there are more items in the root module to be added or not.
                // So, if the range includes call to some yet undefined functions which is treated
                // as non-constant can be actually constant if the function is defined to be so later.
                (!module_id || !module_id->IsRootModule() || !veri_file::IsParserActive())) {
                Error("range must be bounded by constant expressions") ;
            }
        }
    }

    if (_next) _next->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriDelayOrEventControl::Resolve(VeriScope *scope, veri_environment environment)
{
    (void) environment ; // Needed to prevent "unused" build warnings

    // VIPER 2447 : Delay/timing control is not suported inside always_comb/always_latch
    if (environment == VERI_SPECIAL_ALWAYS) {
        Warning("%s in always_comb/always_latch is not allowed", _event_control ? "event control" : "timing control") ;
    }
    // VIPER #4204 : Delay/timing control is not suported inside final block
    if (environment == VERI_FINAL_BLOCK) {
        Warning("%s in final block is illegal", _event_control ? "event control" : "timing control") ;
    }

    // delay control is LRM A.2.2.3. VeriDelay is a normal expression ?
    if (_delay_control) _delay_control->Resolve(scope, VERI_DELAY_CONTROL) ;

    // repeat condition :
    if (_repeat_event) _repeat_event->Resolve(scope, VERI_EXPRESSION) ;

    // Now event control : list of event expressions :
    if (environment != VERI_EVENT_CONTROL_UNDECL_USE_GOOD) environment = VERI_EVENT_CONTROL ;
    unsigned i ;
    VeriExpression *event_expr ;
    FOREACH_ARRAY_ITEM(_event_control, i, event_expr) {
        if (event_expr) event_expr->Resolve(scope, environment) ;
    }
}

void VeriDataType::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER #8289: According to IEEE LRM 1800-2009, section 7.4.2:
    // C Style width specification for arrays is allowed only for unpacked arrays.
    // So error out for packed dimension in both SV and non-SV mode if it contains single value range.
    // VIPER #3403: Check whether it contains SV specific [n] type single value range in non-SV mode:
    if (_dimensions && /*!IsSystemVeri() &&*/ _dimensions->IsSingleValueRange()) { // commnet out sv mode
        // Can't check it in the constructor for late parse tree bindings:
        //_dimensions->Error("single value range is not allowed in this mode of verilog") ;
        _dimensions->Error("packed dimension must specify a range") ;
    }

    // VIPER #7976: Check only if we are not inside DPI function/task.
    // Data type of DPI function/tasks formals are allowed to have open arrays
    // according to IEEE 1800-2009 LRM section 35.5.6.1:
    VeriIdDef *owner = (scope) ? scope->GetOwner() : 0 ;
    VeriModuleItem *module_item = (owner) ? owner->GetModuleItem() : 0 ;
    VeriRange *dim = (!module_item || !module_item->IsDPIMethod()) ? _dimensions : 0 ;
    while (dim) {
        if (!dim->GetLeft() && !dim->GetRight()) Error("packed dimension must specify a range") ; // Viper #4885
        dim = dim->GetNext() ; // Check all the packed dimensions
    }

    // Check if dimensions are allowed for this type.
    // Could do in constructor, but safer is here.
    // VCS gives a warning, so so should we...
    switch (_type) {
    // non-integer type (neither dimensions nor signing allowed) :
    case VERI_TIME :
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_SHORTREAL :
        if (_signing) {
            Warning("signing ignored on type %s",PrintToken(_type)) ;
            _signing = 0 ; // Don't let it enter the parse tree, to avoid problems elsewhere.
        }
        // drop through to dimension check..

    // integer_atom_type (no dimensions allowed) :
    case VERI_INTEGER :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_BYTE :
        if (_dimensions) {
            Error("cannot have packed dimensions of type %s",PrintToken(_type)) ; // VIPER #3202 : produce more appropriate message
            delete _dimensions ; // Don't let it enter the parse tree, to avoid problems elsewhere.
            _dimensions = 0 ;
        }
        break ;
    default :
        break ;
    }

    // Resolve the dimensions (should be all ranges or constant expressions..?) :
    if (_dimensions) _dimensions->Resolve(scope, environment) ; //VERI_CONSTANT_EXPRESSION) ;

    switch (environment) {
    case VERI_CONSTANT_EXPRESSION :
    case VERI_PARAM_INITIAL_EXP :
        // VIPER #3128: Had to allow data type in non-type parameters for this issue.
        // But this data type is illegally used as initial value of a non-type parameter:
        Error("constant expression required") ;
        break ;
    default:
        break ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriNetDataType::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_data_type) _data_type->Resolve(scope, environment) ;

    // VIPER #4732 : Check moved to VerNetDecl
    // LRM 6.5 : Certain restrictions apply to the data type of a net
    // i) a 4 state intergral type, including a packed array or packed struct
    // ii) an unpacked array or unpacked struct, where each element has a valid
    //     data type for a net
    //VeriTypeInfo *type = (_data_type) ? _data_type->CreateType(0) : 0 ;

    //if (type) type->CheckNetDataType(_data_type) ;
    //delete type ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
// VIPER #3278 : Use VeriPathPulse class to store the value of reject/error limits
void VeriPathPulseVal::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_reject_limit) _reject_limit->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
    if (_error_limit) _error_limit->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;
}

// System Verilog additions
void VeriTypeRef::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the type-reference (if not yet done)
    if (_type_name) _type_name->Resolve(scope, VERI_TYPE_ENV) ; // expect a 'type' identifier in here..

    // Pick up/and set the resolve identifier (if there) :
    _id = (_type_name) ? _type_name->GetId() : 0 ;

    // VIPER #5106 : Produce error if type is not defined and we are not in system verilog mode
    if (!_id && (IsVeri2001() || IsVeri95())) {
        if (_type_name && _type_name->GetName()) Error("%s is an unknown type", _type_name->GetName()) ;
    }

    // Signing is not allowed on named type references...
    if (_signing) {
        Warning("signing ignored on type %s", (_id) ? _id->Name() : "") ;
        _signing = 0 ; // Don't let it enter the parse tree, to avoid problems elsewhere.
    }

    if (_id && _dimensions) {
        // Check if dimensions are allowed for this type.
        // VCS gives a warning, so should we...
        switch (_id->Type()) {
        // non-integer type (no dimensions allowed) :
        case VERI_ENUM :
            {
            // VIPER 5880. Issue cannot have packed dimensions of type enum error for enum types
            VeriDataType *id_type = _id->GetDataType() ;
            // VIPER #6215: Only error out in case the base size of enum is > 1:
            if (!id_type || (id_type->BaseTypeSize() <= 1)) break ;
            }
        case VERI_CLASS : // VIPER #7430: Classes are resolved in Resolve() flow, so add this check here
        case VERI_INTERFACE : // VIPER #7430: Interfaces are not resolved, but have it anyway for local interfaces
        case VERI_TIME :
        case VERI_REAL :
        case VERI_REALTIME :
        case VERI_SHORTREAL :
        // integer_atom_type (no dimensions allowed) :
        case VERI_INTEGER :
        case VERI_SHORTINT :
        case VERI_INT :
        case VERI_LONGINT :
        case VERI_BYTE :
            // VCS and Modelsim now both issue an error on this.
            Error("cannot have packed dimensions of type %s",_id->Name()) ; // VIPER #3202 : produce more appropriate message
            delete _dimensions ; // Don't let it enter the parse tree, to avoid problems elsewhere.
            _dimensions = 0 ;
            break ;
        default :
            break ;
        }
    }

    // Resolve the dimensions (should be all ranges or constant expressions..?) :
    if (_dimensions) _dimensions->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;

    // Now, we cannot have packed dimensions of a unpacked structure.
    // That can syntactically only happen for a type-reference, so check that here :
    if (_dimensions && _id && _id->IsUnpacked()) {
        Error("cannot have packed dimensions of unpacked type %s",_id->Name()) ;
    }

    // VIPER #5179: Check for Use before declaration for type-refs.
    // Use before declaration check: VIPER #2186, 2775, 3121, 3503, 4379, 5604, 5609, 5712, 6151, 6599
    unsigned static_elab = 0 ;
    static_elab = IsStaticElab() ;
    if (!Message::ErrorCount() && scope && _id && _id->IsType() && !static_elab && LineFile::LessThan(Linefile(), _id->Linefile())) {
        // Following scope checks could be costly.. so defer till now
        VeriIdDef *module_id = scope->GetContainingModule() ;
        VeriModule *module = module_id ? module_id->GetModule() : 0 ;
        if (module && !module->IsStaticElaborated()) Warning("type %s is used before its declaration", _id->Name()) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriStructUnion::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve base type :
    VeriDataType::Resolve(scope, environment) ;

    // Swap the scope to local :
    if (_scope) scope = _scope ;

    // VIPER #2558 related check, if the struct union decls have packed dimensions
    // they should have a 'packed' qualifier (P1800 LRM A.2.2.1 with foot note 12):
    if (_dimensions && !_is_packed) {
        Error("cannot have packed dimensions of unpacked type %s", "structure/union") ;
        delete _dimensions ; // Don't let it enter the parse tree, to avoid problems elsewhere.
        _dimensions = 0 ;
    }
    if (!_is_packed) {
        if (_signing == VERI_SIGNED) { // Signing on unpacked struct/union
            Error("illegal usage of signing with unpacked struct/union type") ;
        }
        // VIPER #4894 : Produce warning if unsigned is used for unpacked struct/union
        if (_signing == VERI_UNSIGNED) {
            Warning("illegal usage of signing with unpacked struct/union type") ;
        }
    }

    // Call resolve on the member declarations
    VeriIdDef *rand_id = 0 ;
    unsigned has_union = 0 ;
    VeriIdDef *id_with_init_val = 0 ;
    unsigned i ;
    VeriDataDecl *data_decl ;
    FOREACH_ARRAY_ITEM(_decls, i, data_decl) {
        if (!data_decl) continue ;
        data_decl->Resolve(scope, VERI_UNDEF_ENV) ;
        if (data_decl->GetDataType() && (data_decl->GetDataType()->IsUnionType())) has_union = 1 ; // VIPER #4804
        // VIPER 2473 : If it is packed structure/union, check its members
        // Issue error, if they are unpacked
        Array *ids = data_decl->GetIds() ;
        unsigned j ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            if (_is_packed) { // Unpacked member
                VeriTypeInfo *id_type = id->CreateType(0) ; // Create type for member
                if (id_type && id_type->IsUnpacked()) { // Unpacked member
                    id->Error("unpacked member %s is not allowed in packed struct/union", id->Name()) ;
                }
                delete id_type ;
            }
            // Chandle cannot be used in untagged union
            if ((Type() == VERI_UNION) && !_is_tagged && (id->Type() == VERI_CHANDLE)) {
                Error("illegal reference to chandle type") ;
            }
            // Member of packed struct/union may not be declared 'rand' or 'randc'
            if (data_decl->GetQualifier(VERI_RAND) || data_decl->GetQualifier(VERI_RANDC)) {
                if (!rand_id) rand_id = id ; // VIPER #4804: Take only the first id
                if (_is_packed) id->Error("field %s of packed struct/union may not be declared rand or randc", id->Name()) ;
            }
            // VIPER #4826 : Produce error if void type variable is declared
            // inside structure/untagged union
            if ((id->Type() == VERI_VOID) && !_is_tagged) {
                id->Error("a void type may only be used in a tagged union") ;
            }
            // VIPER #8052 : 1800-2009 LRM Section 7.2.2 : Members of unpacked structure
            // having an union or members of packed structure shall not be assigned
            // individual default member values. Members of union cannot be initialized
            if (id->GetInitialValue() && ((Type() == VERI_UNION) || ((Type() == VERI_STRUCT) && _is_packed))) {
                id->Error("struct/union member field %s cannot be initialized at declaration", id->Name()) ;
            }
            if (id->GetInitialValue()) id_with_init_val = id ;
        }
    }

    // VIPER #4804: Check for rand/randc modifier in packed structure or unpacked structure with union:
    // LRM does not say anything about unpacked unions but other tools error out even if it does not
    // contain a union in it, so should we...
    if (rand_id && !_is_packed && ((_type == VERI_UNION) || has_union)) {
        // Random member of packed strcuture/union has already been checked above.
        rand_id->Error("member %s of unpacked structure/union may not be declared rand or randc", rand_id->Name()) ;
    }

    // VIPER #8052 : Members of unpacked structure having union cannot be initialized
    // at declaration
    if (!_is_packed && has_union && (_type == VERI_STRUCT) && id_with_init_val) {
        id_with_init_val->Error("struct/union member field %s cannot be initialized at declaration", id_with_init_val->Name()) ;
    }
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriEnum::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve base type (for dimensions) :
    if (_base_type) _base_type->Resolve(scope, VERI_TYPE_ENV) ;

    // VIPER #5971: Produce error if unpacked type is used as base type of enumeration
    VeriTypeInfo *base_type = (_base_type) ? _base_type->CreateType(0): 0 ;

    // VIPER #8294: According to IEEE LRM 1800-2009, section 6.19:
    // base_type of enumeration shall be an integer_atom_type(byte/int/longint/shortint/integer/time)/
    // integer_vector_type(bit/reg/logic)/type identifier, where the legal type of type identifier shall be
    // integer_atom_type, with which an additional packed dimension is not permitted, or an integer_vector_type.
    // So the base type cannot be struct/union/enum typed.
    // VIPER #8446 : Produce error if base type of enum is real/event/chandle
    // VIPER #8500: Check if the base-type has multiple packed dimensions and error out:
    VeriTypeInfo *base_type_info = base_type ? base_type->BaseType() : 0 ;
    if (base_type && (base_type->IsUnpacked() || (base_type_info && (base_type_info->IsStructure() ||
        base_type_info->IsUnion() || base_type_info->IsEnumeratedType())) || base_type->IsRealType() ||
        base_type->IsEventType() || base_type->IsChandleType() || (base_type->PackedDimension() > 1))) {
        Error("illegal enumeration base type") ;
    }
    delete base_type ;

    // Just call resolve on the (declared) ids. Does not do much though..
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        id->Resolve(scope, VERI_UNDEF_ENV) ;
    }
    // VIPER #5710: (vcs_compatibility-9000388573): Resolve base type:
    VeriDataType::Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriDotStar::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Set the back-pointer to the scope in which, once the formals are known,
    // the (missing) actuals can be found :
    _dot_star_scope = scope ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriIfOperator::Resolve(VeriScope *scope, veri_environment environment)
{
    // if-operator is allowed in property and constraint expression, error out
    // if incoming environment is not VERI_PROPERTY_ENV || VERI_CONSTRAINT_EXPR
    if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment != VERI_CLOCKING_BLOCK && environment != VERI_PROPERTY_ENV && environment != VERI_CONSTRAINT_EXPR && environment != VERI_INLINE_CONSTRAINT_EXPR && environment != VERI_PROPERTY_ARG) {
        Error("operator %s is only allowed in a %s", "if-else", "property/constraint expression") ;
    }
    // Resolve arguments, condition must be expression or dist, then and
    // else expression can be property-expression/constraint-set
    VeriExpression *save_sva_clk = (environment == VERI_PROPERTY_ENV) ? _sva_clock_expr : 0 ;

    if (_if_expr) _if_expr->Resolve(scope, VERI_EXPRESSION_UNDECL_USE_GOOD) ;
    if (_then_expr) _then_expr->Resolve(scope, environment) ;

    // Viper 6168: System Verilog 2009 allow multiclock properties to have different clocks for
    // if and then or else expression. LRM Section 16.14.2 of 2009 LRM previously in 2005 LRM
    // Section 17.12.2 dis-allowed this type of properties.
    if (!IsSystemVeri2009OrLater() && _then_expr && save_sva_clk && _sva_clock_expr && (save_sva_clk != _sva_clock_expr)) {
        VeriIdDef *save_clk_id = save_sva_clk->FullId() ;
        VeriIdDef *now_clk_id = _sva_clock_expr->FullId() ;
        if ((save_sva_clk->GetEdgeToken() != _sva_clock_expr->GetEdgeToken()) || (save_clk_id && now_clk_id && now_clk_id!=save_clk_id)){
            _then_expr->Error("the clock flow cannot change in the RHS of %s operator", "if property") ;
        }
    }

    if (_else_expr) _else_expr->Resolve(scope, environment) ;

    // Viper 6168: System Verilog 2009 allow multiclock properties to have different clocks for
    // if and then or else expression. LRM Section 16.14.2 of 2009 LRM previously in 2005 LRM
    // Section 17.12.2 dis-allowed this type of properties.
    if (!IsSystemVeri2009OrLater() && _else_expr && save_sva_clk && _sva_clock_expr && (save_sva_clk != _sva_clock_expr)) {
        VeriIdDef *save_clk_id = save_sva_clk->FullId() ;
        VeriIdDef *now_clk_id = _sva_clock_expr->FullId() ;
        if ((save_sva_clk->GetEdgeToken() != _sva_clock_expr->GetEdgeToken()) || (save_clk_id && now_clk_id && now_clk_id!=save_clk_id)){
            _else_expr->Error("the clock flow cannot change in the RHS of %s operator", "if property") ;
        }
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSequenceConcat::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER #4337 : This sequence expression and can only be specified where
    // sequence/property expression is required.
    if (environment != VERI_UNDEF_ENV && environment != VERI_COVERPOINT_ENV && environment != VERI_CLOCKING_BLOCK && environment != VERI_PROPERTY_ENV && environment != VERI_SEQUENCE_ENV && environment != VERI_PROPERTY_ARG && environment != VERI_SEQUENCE_ARG) {
        Error("illegal SVA sequence/property expression") ;
    }

    // First resolve left and right side.
    // Both must be a sequence. left side is optional. If missing, it represents sequence '1' (true).
    if (_left) _left->Resolve(scope, VERI_SEQUENCE_ENV) ;
    if (_right) _right->Resolve(scope, VERI_SEQUENCE_ENV) ;

    // Resolve the cycle_delay_range.
    // Can be a range or an expression.
    // Either way, we need a CONSTANT_EXPRESSION environment there (LRM 17.5).
    // VIPER 2539 : LRM only allows integral constant here
    if (_cycle_delay_range) _cycle_delay_range->Resolve(scope, VERI_INTEGRAL_CONSTANT) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriClockedSequence::Resolve(VeriScope *scope, veri_environment environment)
{
    VeriExpression *save_sva_clock_expr = 0 ;
    if (_sva_clock_expr && IsParenthesized()) save_sva_clock_expr = _sva_clock_expr ;
    VeriExpression *event_expr = 0 ;
    if (_event_expr) {
        VeriIdDef *id = _event_expr->FullId() ;
        if (id && (id->IsSequenceFormal() || id->IsPropertyFormal())) {
            // If event expression is sequence/property formal, get actual/initial expr
            event_expr = id->GetActual() ;
            if (!event_expr) event_expr = id->GetInitialValue() ;
        }
        if (!event_expr) event_expr = _event_expr ;
        event_expr = event_expr->GetConnection() ;
    } else {
        event_expr = _sva_clock_expr ;
    }
    _sva_clock_expr = event_expr ;
    //_sva_clock_expr = _event_expr ?  _event_expr : _sva_clock_expr ;

    // First resolve argument : must be a sequence ?
    // Or can be property also ? Yes. clocked property spec.
    // So pass in existing environment if it's a property :
    if (environment != VERI_CLOCKING_BLOCK && _arg) _arg->Resolve(scope, (environment==VERI_PROPERTY_ENV || environment==VERI_PROPERTY_ARG) ? VERI_PROPERTY_ENV : VERI_SEQUENCE_ENV) ;

    if (IsParenthesized() && save_sva_clock_expr) _sva_clock_expr = save_sva_clock_expr ;

    // According to IEEE 1800-2009 section 16.17 (see rule b.1), explicit
    // clocking event is not permitted inside clocking block. Viper 6369
    if (environment == VERI_CLOCKING_BLOCK && _event_expr) Error("clocking event not allowed in clocking block") ;

    // Resolve the event expression :
    if (_event_expr) _event_expr->Resolve(scope, VERI_EVENT_CONTROL_UNDECL_USE_GOOD) ; // expect a event expression

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriAssignInSequence::Resolve(VeriScope *scope, veri_environment environment)
{
    // First resolve argument.
    // We know that the environment can be a sequence expression.
    if (_arg) _arg->Resolve(scope,VERI_SEQUENCE_ENV) ;

    // Resolve the assignment statements :
    unsigned i ;
    VeriStatement *stmt ;
    FOREACH_ARRAY_ITEM(_stmts, i, stmt) {
        // pass in environment ?
        if (stmt) stmt->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriDistOperator::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // First resolve base class.
    // Can be only expression
    if (_arg) _arg->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve the dist items :
    unsigned i ;
    VeriExpression *dist_item ;
    FOREACH_ARRAY_ITEM(_dist_list, i, dist_item) {
        if (dist_item) dist_item->Resolve(scope, VERI_VALUE_RANGE) ; // could fine tune to dist-item expression (could be expression or a range)...
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSolveBefore::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Construct appears in a constraint block (constraint expression)..
    // Resolve the solve_list and before_list (lists of identifiers) :
    unsigned i ;
    VeriExpression *name ;
    FOREACH_ARRAY_ITEM(_solve_list, i, name) {
        if (name) name->Resolve(scope, VERI_RAND_VAR) ; // All identifiers in these names should be variables with 'rand' in their declaration.
    }
    FOREACH_ARRAY_ITEM(_before_list, i, name) {
        if (name) name->Resolve(scope, VERI_RAND_VAR) ; // All identifiers in these names should be variables with 'rand' in their declaration.
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriCast::Resolve(VeriScope *scope, veri_environment environment)
{
    // Target type expects a type expression :
    if (_target_type) {
        // The target_type can be a VeriDataType (for type-referencing),
        // or a data type with VERI_SIGNED/VERI_UNSIGNED as the type,
        // or it can be a VeriConstVal (for 'sized' cast).
        // These are separate styles of casts, so test them differently :
        if (_target_type->IsConst()) {
            // nothing to test ?
            // int size = _target_type->Integer() ;
        } else if (_target_type->Type()==VERI_SIGNED) {
            // nothing to resolve..
        } else if (_target_type->Type()==VERI_UNSIGNED) {
            // nothing to resolve..
        } else if (_target_type->Type()==VERI_CONST) {
            // nothing to resolve..
        } else {
            // VIPER #5095: This should be either a type or a constant:
            // Resolve with param value; it supports both type and constants.
            _target_type->Resolve(scope, VERI_PARAM_VALUE) ;

            // VIPER #5095: If the target is a reference to a type, create type-ref here:
            VeriIdDef *resolved_id = (_target_type) ?_target_type->GetId() : 0 ;
            VeriName *target_name = (_target_type) ? _target_type->NameCast() : 0 ;
            if (resolved_id && resolved_id->IsType() && target_name) {
                _target_type = new VeriTypeRef(target_name, 0, 0) ;
                _target_type->SetLinefile(target_name->Linefile()) ;
                // Resolve the type reference
                _target_type->Resolve(scope, VERI_TYPE_ENV) ;
            }

            // VIPER #2970 : Check if target is type identifier. If so produce
            // error if that is not resolved
            //VeriDataType *data_type = (_target_type->IsDataType()) ? _target_type->DataTypeCast() : 0 ;
            //VeriName *type_name = (data_type) ? data_type->GetTypeName() : 0 ;
            //if (type_name && !type_name->GetId()) {
                // Target is unresolved type identifier : error
                //_target_type->Error("%s is not declared", type_name->GetName()) ;
            //}
        }
    }

    // Cast environment is passed through to expression.
    if (_expr) _expr->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriNew::Resolve(VeriScope *scope, veri_environment environment)
{
    // Test 'environment' ? (if something smart would be passed in we can check if this 'new' is valid here).

    // 'size' can be any expression : LRM P1800 Section 12.18
    if (_size_expr) _size_expr->Resolve(scope, VERI_EXPRESSION) ;

    // 'args' are expressions?
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) {
        // VIPER #4561 : Coming environment constant expression means arguments of new should be constant.
        //if (arg) arg->Resolve(scope, (environment == VERI_CONSTANT_EXPRESSION) ? VERI_CONSTANT_EXPRESSION : VERI_EXPRESSION) ;
        // VIPER #4685: Argument to new can also be virtual interface. So pass
        // VERI_PORT_CONNECTION as environment.
        if (arg) arg->Resolve(scope, (environment == VERI_CONSTANT_EXPRESSION || environment == VERI_PARAM_INITIAL_EXP) ? environment : VERI_PORT_CONNECTION) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriConcatItem::Resolve(VeriScope *scope, veri_environment environment)
{
    // Member label is a constant expression, or a variable identifier, or a type-identifier..
    // VIPER #2554: 07/2006: Call ResolveLabel to resolve this label:
    if (_member_label) _member_label->ResolveLabel(scope,VERI_UPWARD_SCOPE_NAME, 0 /* unknown target */) ;

    // environment is passed through to the expression.
    if (_expr) _expr->Resolve(scope, environment) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriNull::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // Check that this a VERI_EXPRESSION ? More restrictions where 'null' literal can be used ?

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriDollar::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // Check that this a VERI_EXPRESSION ? More restrictions where '$' can be used ?

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriAssignmentPattern::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve the target type
    if (_type) _type->Resolve(scope, VERI_TYPE_ENV) ;

    // VIPER #5095: If the type is a reference to a type, create type-ref here:
    VeriIdDef *resolved_id = (_type) ? _type->GetId() : 0 ;
    VeriName *type_name = (_type) ? _type->NameCast() : 0 ;
    if (resolved_id && resolved_id->IsType() && type_name) {
        _type = new VeriTypeRef(type_name, 0, 0) ;
        _type->SetLinefile(type_name->Linefile()) ;
        // Resolve the type reference
        _type->Resolve(scope, VERI_TYPE_ENV) ;
    }

    // Issue error if target type is not a data type
    if (_type && !_type->IsDataType())  Error("prefix of assignment pattern must be a data type") ;

    // RD: Do not use VeriConcat::Resolve, since that reports problem for unsized elements.
    // (Which is not a problem for assignment patterns).
    unsigned i ;
    VeriExpression *expr ;
    // Other expressions follow environment
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriMultiAssignmentPattern::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // Issue error if target type is not a data type
    VeriIdDef *type_id = _type ? _type->GetId() : 0 ;
    // Do not issue error if _type is named type. Viper 5323
    if (_type && !_type->IsDataType() && (!type_id || !type_id->IsType()))  Error("prefix of assignment pattern must be a data type") ;
    // Resolve the target type
    if (_type)  _type->Resolve(scope, VERI_TYPE_ENV) ;

    // RD: Do not use VeriMultiConcat::Resolve, since that reports problem for unsized elements.
    // (Which is not a problem for assignment patterns).

    // repeat expression is 'constant' (LRM A.8.1)
    if (_repeat) _repeat->Resolve(scope, VERI_CONSTANT_EXPRESSION) ;

    // Other expressions follow environment
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->Resolve(scope, environment) ;
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriForeachOperator::Resolve(VeriScope *scope, veri_environment environment)
{
    // Resolve array name
    if (_array_name) _array_name->Resolve(scope, VERI_EXPRESSION) ;

    // Flip to local scope (if there)
    if (_scope) {
        if (scope != _scope->Upper()) {
            // Viper 5464
            //detach _scope from _scope->Upper() and set scope as
            // upper scope of _scope all this is done in SetUpper()
            _scope->SetUpper(scope) ;
        }
        scope = _scope ;
    }

    // _array_name should be reference to array identifier
    VeriIdDef *array_id = _array_name ? _array_name->GetId() : 0 ;
    VeriTypeInfo *array_type = (array_id) ? array_id->CreateType(0): 0 ;
    unsigned to_check = 1 ;
    if (array_type && (array_type->IsTypeParameterType() || array_type->IsUnresolvedNameType())) {
        // Cannot validate array as data type is not defined
        delete array_type ;
        array_type = 0 ;
        to_check = 0 ; // Donot do semantic checks
    }
    unsigned packed_dims = (array_type) ? array_type->PackedDimension() : 0 ;
    unsigned unpacked_dims = (array_type) ? array_type->UnpackedDimension() : 0 ;
    //VeriDataType *base_type = array_id ? array_id->GetDataType() : 0 ; // Find out the base data type
    //unsigned base_bitsize  = (base_type) ? base_type->BaseTypeSize() : 0 ; // Find out the base data type size

    // VIPER #3834 : If identifier is of type string, though 'base_bitsize' is 0, it is
    // allowed in foreach loop
    //if (array_id && array_id->Type() == VERI_STRINGTYPE) base_bitsize = 2 ; // Be it greater than 1
    // VIPER #3086 : Introduce check to allow  vector type variables without any dimension in 'foreach' loops.
    //if (array_id && !array_id->PackedDimension() && !array_id->UnPackedDimension() && base_bitsize <= 1) {
    if (array_id && array_type && !packed_dims && !unpacked_dims && !(array_type->IsBuiltInType() && ((array_type->BaseTypeSize() > 1) || (array_type->IsString())))) {
        // Identifier does not have packed / unpacked dimensions
        if (to_check) Error("%s is not an array of vectors", array_id->Name()) ;
    }
    // VIPER #3083: Error out if the number of loop variables is greater than the
    // array dimensions.
    //unsigned dim_size = array_id  ? (array_id->PackedDimension()  + array_id->UnPackedDimension()) : 0 ;
    unsigned dim_size = (array_type) ? (packed_dims + unpacked_dims) : 0 ;
    VeriTypeInfo *base_type = (array_type) ? array_type->BaseType() : array_type ;
    if (array_type && ((array_type->BaseTypeSize() > 1) || (base_type && base_type->IsString()))) dim_size++ ;
    unsigned loop_index_count = _loop_indexes ? _loop_indexes->Size() : 0 ;
    //if (base_bitsize > 1) dim_size++ ; // consider vector type as implicit one-dimensional packed array of bit/logic
    if (array_id && (dim_size < loop_index_count ) && to_check) Error("number of loop variables in the FOREACH loop are more than the array dimensions") ;
    //delete array_type ;

    // Type of loop variable will be type of array index
    // Only for associative array array index type is to be extracted from
    // array declaration, for others array index type is int
    if (array_type && array_type->IsAssociativeArrayType()) {
        VeriTypeInfo *idx_type = array_type->IndexType() ;
        // Index type of associative array is not defined
        if (idx_type && (idx_type->IsTypeParameterType() || idx_type->IsUnresolvedNameType())) {
            delete array_type ;
            /* coverity[copy_paste_error] */
            array_type = 0 ;
            to_check = 0 ;
        }
    }
    unsigned index_type = VERI_INT ;
    /*
    // Get the Msb dimension
    VeriRange *dim = array_id ? array_id->GetDimensionAt(0): 0 ;
    VeriExpression *left = dim ? dim->GetLeft() : 0 ;
    VeriExpression *right = dim ? dim->GetRight() : 0 ;

    if (right && right->IsDataType() && !left) {
        index_type = right->Type() ;
    } else if (left && left->GetId() && left->GetId()->IsType() && !right) {
        index_type = left->Type() ;
    }*/

    // Resolve loop index variables
    unsigned i ;
    VeriIdDef *loop_index ;
    VeriTypeInfo *arr_type = array_type ;
    unsigned index_kind = VERI_VAR ;
    FOREACH_ARRAY_ITEM(_loop_indexes, i, loop_index) {
        if (!loop_index) {
            arr_type = arr_type ? arr_type->ElementType(): 0 ;
            continue ;
        }
        // Name of loop index variable should not be same as array id : Check
        if (array_id && Strings::compare(array_id->Name(), loop_index->Name())) {
            // Both names are same : Error
            Error("%s is already defined", loop_index->Name()) ;
        }
        // If this 'Resolve' is called from static elaboration, we are
        // re-resolving this(from VeriClass::StaticElaborate). So no need
        // to create implicit declarations or type/kind setting
        if (loop_index->GetDataType()) {
            arr_type = arr_type ? arr_type->ElementType(): 0 ;
            continue ;
        }
        loop_index->Resolve(scope, environment) ;

        if (arr_type && arr_type->IsAssociativeArrayType()) {
            VeriRange *dim = array_id ? array_id->GetDimensionAt(i) : 0 ;
            VeriTypeInfo *idx_type = arr_type->IndexType() ;
            // Index type of associative array is not defined, cannot proceed
            if (!dim || (idx_type && (idx_type->IsTypeParameterType() || idx_type->IsUnresolvedNameType()))) {
                arr_type = arr_type->ElementType() ;
                continue ;
            } else {
                VeriExpression *type_expr = dim->GetLeft() ;
                if (!type_expr) type_expr = dim->GetRight() ;
                if (type_expr) {
                    VeriMapForCopy old2new ;
                    VeriDataType *loop_index_type = type_expr->DataTypeCast() ;
                    if (loop_index_type) {
                        loop_index_type = loop_index_type->CopyDataType(old2new) ;
                    } else {
                        loop_index_type = new VeriTypeOperator(type_expr->CopyExpression(old2new)) ;
                    }
                    VeriDataDecl *implicit_decl = new VeriDataDecl(index_kind, loop_index_type, loop_index) ;
                    implicit_decl->SetLinefile(Linefile()) ;
                    if (!_implicit_decl_list) _implicit_decl_list = new Array(1) ;
                    _implicit_decl_list->Insert(implicit_decl) ;
                } else {
                    if (to_check) { // VIPER #7108: do not set type and kind if no semantic check is required
                        loop_index->SetType(index_type) ; // Set index type
                        loop_index->SetKind(index_kind) ; // VIPER #5949: Set index kind
                    }
                }
            }
        } else {
            if (to_check) { // VIPER #7108: do not set type and kind if no semantic check is required
                loop_index->SetType(index_type) ; // Set index type
                loop_index->SetKind(index_kind) ; // VIPER #5949: Set index kind
            }
        }
        arr_type = arr_type ? arr_type->ElementType(): 0 ;
    }
    delete array_type ;
    // Resolve constraint set
    if (_constraint_set) _constraint_set->Resolve(scope, VERI_CONSTRAINT_EXPR) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriStreamingConcat::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_slice_size) _slice_size->Resolve(scope, VERI_PARAM_VALUE) ; // Slice size can be simple type/constant expression

    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr) expr->Resolve(scope, VERI_EXPRESSION) ; // VIPER #3180 : Each stream expression is to be resolved with VERI_EXPRESSION environment
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriCondPredicate::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_left) _left->Resolve(scope, environment) ; // Resolve left operand

    VeriScope *prefix_scope = _left ? _left->GetScope() : 0 ; // Right hand side should be resoved with left hand side's scope (as as selected name)
    if (_right) _right->Resolve(prefix_scope ? prefix_scope: scope, environment) ; // Resolve right operand
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriDotName::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    if (_name) _name->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ; // Resolve the name
    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTaggedUnion::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // _member is reference to a member of tagged union. This cannot be resolved
    // by direct scoping rule. So, use environment 'VERI_UPWARD_SCOPE_NAME' so that
    // parser does not error out here.
    if (_member) _member->Resolve(scope, VERI_UPWARD_SCOPE_NAME) ;

    // VIPER #4434: _expr may contain reference to an enum literal defined inside tagged union.
    // This cannot be resolved by direct scoping rule. So, use environment 'VERI_UNDEF_ENV'
    // so that it does not error out, we will try to resolve it from the CheckType routine:
    // Using 'VERI_UPWARD_SCOPE_NAME' errors out if it is a constant literal. Using VERI_EXPRESSION
    // produces error for unresolved references, so use VERI_UNDEF_ENV to shut itself up.
    if (_expr) _expr->Resolve(scope, VERI_UNDEF_ENV) ; // Resolve member value

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
/* -------------------------------------------------------------- */
// Visitor to reset identifier in id-refs
class ResetIdRefsVisitor : public VeriVisitor
{
public:
    ResetIdRefsVisitor() : VeriVisitor() {}
    ~ResetIdRefsVisitor() {}
private:
    // Prevent compiler from defining the following
    ResetIdRefsVisitor(const ResetIdRefsVisitor &) ;            // Purposely leave unimplemented
    ResetIdRefsVisitor& operator=(const ResetIdRefsVisitor &) ; // Purposely leave unimplemented
public:

    virtual void VERI_VISIT(VeriIdRef, node) {
        node.SetId(0) ; // Reset identifier
    }
} ; // class ResetIdRefsVisitor
/* -------------------------------------------------------------- */
void VeriExpression::ResolveArrayMethod(VeriScope * /*scope*/, veri_environment /*environment*/)
{
    Error("illegal left hand side of with clause") ;
}
void VeriSelectedName::ResolveArrayMethod(VeriScope *scope, veri_environment environment)
{
    // First resolve it
    Resolve(scope, environment) ;
    // Check whether it is a valid array method call which accepts 'with' clause
    switch (_function_type) {
    case VERI_METHOD_FIND :
    case VERI_METHOD_FIND_INDEX :
    case VERI_METHOD_FIND_FIRST :
    case VERI_METHOD_FIND_FIRST_INDEX :
    case VERI_METHOD_FIND_LAST :
    case VERI_METHOD_FIND_LAST_INDEX :
    case VERI_METHOD_MAX :
    case VERI_METHOD_MIN :
    case VERI_METHOD_UNIQUE :
    case VERI_METHOD_UNIQUE_INDEX :
    case VERI_METHOD_SORT :
    case VERI_METHOD_RSORT :
    case VERI_METHOD_SUM :
    case VERI_METHOD_PRODUCT :
    case VERI_METHOD_AND :
    case VERI_METHOD_OR :
    case VERI_METHOD_XOR :
        break ; // Valid LHS of 'with' clause
    default :
        Error("illegal left hand side of with clause") ;
        break ;
    }
}
void VeriFunctionCall::ResolveArrayMethod(VeriScope *scope, veri_environment environment)
{
    if (!_func_name) return ;

    // Resolve function name first
    _func_name->ResolveArrayMethod(scope, VERI_UPWARD_SCOPE_NAME) ;

    // Check number of arguments (0 or 1) for array method:
    if (_args && _args->Size()>1) {
        Error("illegal left hand side of with clause") ;
        return ;
    }

    // Extract the array identifier on which array method is called. It resides at
    // the prefix of _func_name
    VeriName *prefix = _func_name->GetPrefix() ;

    VeriIdDef *array_id = prefix ? prefix->GetId() : 0 ;

    // Function contains 1 argument. It must be simple name and we will declare a
    // identifier for that
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) {
        if (!arg) continue ;
        const char *arg_name = arg->GetName() ;
        if (scope && arg_name && !arg->GetPrefix()) { // It is simple name reference
            VeriIdDef *arg_id = scope->FindLocal(arg_name) ;
            //if (!scope->FindLocal(arg_name)) { // Do not create another id, if it is already created
            if (!arg_id) { // Do not create another id, if it is already created
                // Create variable identifier for iterator variable
                VeriVariable *id = new VeriVariable(Strings::save(arg_name)) ;
                // Declare in the local scope of 'with' expression
                (void) scope->Declare(id) ;
                // VIPER #3029. Set the new variable id as an iterator.
                id->SetIsIterator() ;
                // Type of iterator variable will be that of array
                if (array_id && array_id->GetDataType()) {
                    id->SetDataType(array_id->GetDataType()) ;
                }
                // VIPER #8218 : Create dimension from prefix not from id, as prefix can
                // be indexed-name
                VeriRange *range = prefix ? prefix->CreateDimensions(1): 0 ;
                if (!range) {
                    range = array_id ? array_id->GetDimensions() : 0 ;
                    VeriMapForCopy old2new ;
                    if (range) range = range->CopyRange(old2new) ;
                }
                VeriRange *tmp = range ;
                range = range ? range->GetNext() : 0 ;
                if (tmp) tmp->SetNextNull() ;
                delete tmp ;
                if (array_id && range) {
                    id->SetMemoryRange(range) ; // Set array id's data type
                }
                arg_id = id ;
            }
            arg->SetId(arg_id) ;
        } else { // Illegal argument to array method : Error
            Error("illegal left hand side of with clause") ;
        }
        // Resolve the argument
        arg->Resolve(scope, environment) ;
    }
}
void VeriWithExpr::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // It can be array method call or stream expression. For stream expression
    // _right can only be VeriRange
    if (_right && _right->IsRange()) { // It is stream expression: Resolve it
        if (_left) _left->Resolve(scope, environment) ;
        // VIPER #4888 : In with expression both sides of range will be expression.
        // For this pass VERI_VALUE_RANGE as environment
        _right->Resolve(scope, VERI_VALUE_RANGE) ;
        return ;
    }

    // It is array method call :
    // '_left' can be built-in array method call, which can use undefined variables
    // as iterator. We should create variables for those undefined variables. They will
    // be local to this expression.
    // Make a local scope here to declare iterator variable used as a argument of array method
    // Do not create the scope, if it is already created:
    if (!_scope) _scope = new VeriScope(scope, 0 /* no owner name*/) ;

    if (_left) _left->ResolveArrayMethod(_scope, environment) ; // Resolve left hand side of 'with'
    // 'item' is a default iterator variable. Declare that one here if not declared during
    // argument resolve. It can be referenced in 'with' expression
    if (!_scope->FindLocal("item")) {
        VeriVariable *id = new VeriVariable(Strings::save("item")) ;
        (void) _scope->Declare(id) ;
        // VIPER #3029. Set the new variable id as an iterator.
        id->SetIsIterator() ;
        // Extract the array id whose data type is to be set to 'id'
        VeriExpression *array_ref = _left ;

        while (array_ref) {
            // Viper 5275. For array function call we have to call GetPrefix twice.
            if (array_ref->GetFunctionType() && !array_ref->IsFunctionCall()) { // 'array_ref' has function name as suffix
                // Its prefix is array reference
                array_ref = array_ref->GetPrefix() ;
                break ;
            }
            array_ref = array_ref->GetPrefix() ;
        }
        VeriIdDef *array_id = array_ref ? array_ref->GetId() : 0 ;
        if (array_id && array_id->GetDataType()) {
            id->SetDataType(array_id->GetDataType()) ; // Set array id's data type
        }
        // Viper 6893 : Set the unpacked data type on the item
        //VeriRange *range = array_id ? array_id->GetDimensions() : 0 ;
        // VIPER #8218 : Create dimension from prefix not from id, as prefix can
        // be indexed-name
        VeriRange *range = array_ref ? array_ref->CreateDimensions(1): 0 ;
        if (!range) {
            range = array_id ? array_id->GetDimensions() : 0 ;
            VeriMapForCopy old2new ;
            if (range) range = range->CopyRange(old2new) ;
        }
        VeriRange *tmp = range ;
        range = range ? range->GetNext() : 0 ;
        if (tmp) tmp->SetNextNull() ;
        delete tmp ;
        if (array_id && range) {
            id->SetMemoryRange(range) ; // Set array id's data type
        }
    }
    // Identifiers referenced in _right should look in _scope first. But from ycc
    // references can be resolved. So need to re-resolve them
    ResetIdRefsVisitor obj ;
    if (_right) _right->Accept(obj) ;
    if (_right) _right->Resolve(_scope, environment) ; // Resolve right hand side of 'with';

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriInlineConstraint::Resolve(VeriScope *scope, veri_environment environment)
{
    // VIPER 2543 : illegal expression used as lvalue (target of assignment or
    // port connection to inout/output port)
    switch(environment) {
    case VERI_NET_LVALUE :
    case VERI_NET_LVALUE_UNDECL_USE_GOOD :
    case VERI_REG_LVALUE :
    case VERI_CONSTRUCTOR_LVALUE :
    case VERI_LVALUE :
    case VERI_FORCE_LVALUE :
    case VERI_GENVAR_LVALUE :
    case VERI_GATE_INOUT_PORT_CONNECTION : // actual of bidirectional pass switch instance
        Error("illegal expression in target") ;
        break ;
    default :
        break ;
    }

    // Donot resolve _left here, left is randomize function call and objects of
    // class can be used as arguments of randomize call.
    //if (_left) _left->Resolve(scope, environment) ; // Resolve left hand side of 'with'

    // VIPER #3901 : Create a scope for this inline contraint.
    // VIPER #4503 : Scope for this can be created in yacc to maintain scope
    // hierarchy : If not created in yacc, create now :
    if (!_scope) _scope = new VeriScope(scope, 0 /* no owner name*/) ;

    // Find the name of the class whose object is used here as prefix :
    // _left should be object.randomize(..)
    // Get function name from prefix
    VeriExpression *func_name = (_left) ? (_left->IsFunctionCall() ? _left->GetPrefix() : _left) : 0 ;
    // Extract the prefix part from function name :
    VeriName *prefix = (func_name) ? func_name->GetPrefix(): 0 ;

    // VIPER #4473 : If the function name is scope name, the prefix denotes the class/package
    // to which this function belongs. Therefore ResolveScopeName is called upon it.
    unsigned scope_randomize = 0 ;
    unsigned is_std_randomize = 0 ;
    if (func_name && func_name->IsScopeName()) {
        scope_randomize = 1 ;
        if (prefix) prefix->ResolveScopeName(scope, environment) ;
        // VCS Compatibility issue (test_301): if prefix of this randomize call
        // is package name, add package scope as using scope of current module
        VeriIdDef *package_id = prefix ? prefix->GetId(): 0 ;
        if (package_id && package_id->IsPackage()) {
            // VIPER #8149: Check if it is randomize of std package
            if (Strings::compare(package_id->Name(), "std")) {
                VeriModule *container_ptr = package_id->GetModule() ;
                VeriLibrary *lib = container_ptr ? container_ptr->GetLibrary(): 0 ;
                if (lib && Strings::compare(lib->GetName(), "std")) is_std_randomize = 1 ;
            }
            if (scope) scope->Using(package_id->LocalScope()) ;
        }
        if (prefix && !package_id) Error("%s is not declared", prefix->GetName()) ;
        if (!is_std_randomize) {
            if (_id_refs || _constraint_block) {
                Error("illegal randomize method") ;
            } else {
                if (_left) _left->Resolve(scope, environment) ;
            }
        }
    } else {
        // First resolve the prefix i.e. the object part of object.randomize(..) to
        // extract the name of class on which randomize is invoked. Objects of that
        // class can be used as argument to randomize. So function call should be
        // resolved after setting class scope as extended scope of this local scope.
        if (prefix) {
            prefix->Resolve(scope, environment) ;
        } else {
            // VIPER #8149 : Not object.randomize(..). So 'randomize' must be visible
            if (func_name && func_name->GetFunctionType() == VERI_METHOD_RANDOMIZE) {
                VeriIdDef *func_id = scope ? scope->Find(func_name->GetName()): 0 ;
                if (func_id && func_id->IsFunction()) {
                    VeriScope *func_scope = func_id->LocalScope() ;
                    VeriIdDef *container_id = func_scope ? func_scope->GetContainingModule(): 0 ;
                    if (container_id && (Strings::compare(container_id->Name(), "std"))) {
                        VeriModule *container_ptr = container_id->GetModule() ;
                        VeriLibrary *lib = container_ptr ? container_ptr->GetLibrary(): 0 ;
                        if (lib && Strings::compare(lib->GetName(), "std")) is_std_randomize = 1 ;
                    }
                }
                if (!is_std_randomize) {
                    if (_id_refs || _constraint_block) {
                        Error("illegal randomize method") ;
                    } else {
                        if (_left) _left->Resolve(scope, environment) ;
                    }
                }
            }
        }
    }

    // Try to evaluate the type of object
    VeriTypeInfo *type = (prefix) ? prefix->CheckType(0, NO_ENV): 0 ;
    unsigned resolve_constraint = 1 ;
    if (type && type->IsClassType()) {
        VeriIdDef *class_id = type->GetNameId() ;
        VeriScope *class_scope = (class_id && class_id->IsClass()) ? class_id->LocalScope() : 0 ;
        // If prefix is object of a class, set the class scope as extended
        // scope of _scope (scope of inline constraint)
        _scope->SetExtendedScope(class_scope) ;
    } else if (type && (type->IsTypeParameterType() || type->IsUnresolvedNameType())) {
        // Object is defined by type parameter or unresolved named type, donot resolve constraint block :
        resolve_constraint = 0 ;
    }
    delete type ;
    if (resolve_constraint) {
        //if (_left) _left->Resolve(_scope, environment) ; // Resolve left hand side of 'with'
        unsigned i ;
        VeriExpression *expr ;
        if (func_name && func_name->GetFunctionType() == VERI_METHOD_RANDOMIZE) {
            if (_left && _left->IsFunctionCall()) _left->SetFunctionType(VERI_METHOD_RANDOMIZE) ;
            // VIPER #5489 : Do not try to re-resolve _left, it is already resolved above.
            // _left is randomize method call. So VeriFunctionCall::Resolve will try to
            // re-resolve the name. Since prefix specified class scope is set as extended
            // scope of '_scope', resolved class object in _left can be modified to incoorect
            // value. So name of _left (function call) is already resolved. Now resolve its arguments
            Array *args = _left ? _left->GetArgs() : 0 ;
            FOREACH_ARRAY_ITEM(args, i, expr) {
                // VIPER #5416 & #5810: Arguments to class randomize cannot be
                // 1) Empty Expression
                // 2) Any Expression (other than class property)
                // 3) If null is present then it should be the only expression
                // This is described in LRM Section 13.10 IEEE P1800
                if (!expr) {
                    Error("%s expressions not allowed in argument of %s randomize call", "empty", (scope_randomize)?"scope":"class") ;
                    continue ;
                }
                // Resolve the actual :
                // VIPER #4416: If formal is not present, it may be the case that formal
                // can be virtual interface. So pass VERI_PORT_CONNECTION to resolve actual
                expr->Resolve(_scope, VERI_PORT_CONNECTION) ;
                // VIPER #5416 & #5810: Arguments to class randomize cannot be
                // 1) Empty Expression
                // 2) Any Expression (other than class property)
                // 3) If null is present then it should be the only expression
                // This is described in LRM Section 13.10 IEEE P1800
                if (expr->IsNull()) {
                    if (scope_randomize) {
                        expr->Error("%s expressions not allowed in argument of %s randomize call", "null", "scope") ;
                    } else if (args->Size() > 1) {
                        expr->Error("%s expressions not allowed in argument of %s randomize call", "null with other", "class") ;
                    }
                    continue ;
                } else if (scope_randomize) {
                    if (!expr->GetId() && !InRelaxedCheckingMode()) expr->Error("%s expressions not allowed in argument of %s randomize call", "complex", "scope") ;
                    continue ;
                } else if (!expr->FullId()) {
                    expr->Error("%s expressions not allowed in argument of %s randomize call", "non class property", "class") ;
                    continue ;
                }
            }
        } else {
            if (_left) _left->Resolve(_scope, environment) ; // Resolve left hand side of 'with'
        }

        // VIPER #6756: Resolve the indentifiers referenced:
        FOREACH_ARRAY_ITEM(_id_refs, i, expr) {
            if (expr) expr->Resolve(_scope, environment /* VERI_EXPRESSION ? */) ;
        }

        // Resolve the constraint block
        FOREACH_ARRAY_ITEM(_constraint_block, i, expr) {
            // VIPER #6818 : Use different environment for inline constraint
            if (expr) expr->Resolve(_scope, VERI_INLINE_CONSTRAINT_EXPR) ; // expecting a constraint expression
        }
    }

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriOpenRangeBinValue::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve iff expression
    if (_iff_expr) _iff_expr->Resolve(scope, VERI_EXPRESSION) ;
    // Resolve the value ranges
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM (_open_range_list, i, expr) expr->Resolve(scope, VERI_BIN_VAL_ENV) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTransBinValue::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve iff expression
    if (_iff_expr) _iff_expr->Resolve(scope, VERI_EXPRESSION) ;
    // Resolve the trans_sets
    unsigned i ;
    VeriTransSet *trans_set ;
    FOREACH_ARRAY_ITEM (_trans_list, i, trans_set) trans_set->Resolve(scope, VERI_BIN_VAL_ENV) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriDefaultBinValue::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve iff expression
    if (_iff_expr) _iff_expr->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSelectBinValue::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve iff expression
    if (_iff_expr) _iff_expr->Resolve(scope, VERI_EXPRESSION) ;
    // Resolve the select expression
    if (_select_expr) _select_expr->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTransSet::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the trans range lists
    unsigned i ;
    VeriTransRangeList *range_list ;
    FOREACH_ARRAY_ITEM (_trans_range_list, i, range_list) range_list->Resolve(scope, VERI_BIN_VAL_ENV) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTransRangeList::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the trans items
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM (_trans_item, i, expr) expr->Resolve(scope, VERI_BIN_VAL_ENV) ;

    // Resolve the repetition expression
    if (_repetition) _repetition->Resolve(scope, VERI_BIN_VAL_ENV) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriSelectCondition::Resolve(VeriScope *scope, veri_environment /*environment*/)
{
    // Resolve the bins expression
    if (_bins_expr)_bins_expr->Resolve(scope, VERI_UNDEF_ENV) ;

    // Resolve the open range list
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM (_range_list, i, expr) expr->Resolve(scope, VERI_BIN_VAL_ENV) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

void VeriTypeOperator::Resolve(VeriScope *scope, veri_environment environment)
{
    (void) environment ;
    // Resolve the type expression.
    // Should show up only when 'type' expressions are allowed. But that's difficult to validate (type expressions can show up in some operator calls (like ==) in normal expressions.
    // Difficult to test environment...
    // Argument can be expression or a data type. We won't 'read' the expression, so 'environment' is and difficult to force.
    if (_expr_or_data_type) _expr_or_data_type->Resolve(scope, VERI_UNDEF_ENV) ;

    // VIPER #7607: Create and set the memory range here:
    // VIPER #7802: Create both packed and unpacked dimensions, since it is a data type:
    if (!_dimensions) _dimensions = CreateDimensions(0 /* both packed and unpacked */) ;

    // VIPER #8218: Pass VERI_EXPRESSION as environment instead of coming
    // environment, as we get this dimension from argument of type operator
    // and that argument can be expression with +:/-:
    if (_dimensions) _dimensions->Resolve(scope, VERI_EXPRESSION) ;

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriPatternMatch::Resolve(VeriScope *scope, veri_environment environment)
{
    if (_scope) scope = _scope ; // Set pattern scope where pattern specified identifiers are declared
    if (_left) _left->Resolve(scope, environment) ; // Resolve left operand

    if (_right) _right->Resolve(scope, environment) ; // Resolve the pattern. Its target type is that of _left

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}
void VeriConstraintSet::Resolve(VeriScope *scope, veri_environment environment)
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) if (expr) expr->Resolve(scope, environment) ;
}

void VeriExpression::ResolveLabel(VeriScope *scope, veri_environment environment, VeriExpression * /* target */)
{
    if (environment == VERI_UNDEF_ENV) return ; // Don't test anything.

    // FIXME: This catches VeriSelectedName::ResolveLabel() also.
    // IEEE P1800 LRM (BNF) does not have any indication that we can use hier-name
    // to assign nested structure element. So this routine is not implemented on
    // VeriSelectedName.

    // When we are here, it means 'this' must be a constant expression, do not error
    // out. Instead resolve 'this' with VERI_PARAM_VALUE environment. This allows
    // both constant expression as well as 'types' and we need exactly that.
    // NOTE: Since we will allow types, we don't need VeriDataType::ResolveLabel().
    //Error("illegal expression in assignment pattern item label") ;
    Resolve(scope, VERI_PARAM_VALUE) ;
}

void VeriIdRef::ResolveLabel(VeriScope *scope, veri_environment environment, VeriExpression *target)
{
    if (environment == VERI_UNDEF_ENV) return ; // Don't test anything.

    const char *name = GetName() ;

    // Here it is either a type-ref or an id-ref.
    // If it is an id-ref, it can be a member name. So, first try the member name:
    if (target) {
        // The target should be a structure and the id must be a member of that structure.
        VeriDataType *target_type = target->GetDataType() ;
        // Resolve the id from the member defined in the datatype:
        _id = (target_type) ? target_type->GetMember(name) : 0 ;
    }

    // If it is not a member, it must be a type-ref or id-ref:
    if (!_id) {
        // We should find it from here:
        if (scope && name) _id = scope->Find(name, /*from*/this) ;

        // Check the usage of this identifier in environment VERI_PARAM_VALUE.
        // This allows constant expression and type-refs, which is exactly what we need:
        // FIXME: In some cases like initialize with assigment pattern, we can't provide
        // the 'target' hence we will not be able to resolve the member labels and will
        // come to here. Now, if we can resolve 'this' with something declared and which
        // is not costant, we will incorrectly error out from this check, so, currently,
        // only check the environment when we have a target. When we come out of the
        // limitation we need to check always irrespective of existence of target:
        if (_id && target) _id->CheckEnvironment(VERI_PARAM_VALUE, /*from*/this) ;
    }

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    if (_id) {
        Strings::free(_name) ; _name = 0 ;
    }
#endif
#endif

    // Resolve any attribute expressions
    ResolveAttributes(scope) ;
}

VeriIdDef* VeriDotStar::GetActualIdForFormal(const char* formal_name, unsigned is_vhdl) const  // Get the actual id with the name of the formal
{
    // An unconnected port formal needs to be hooked up to the same-named actual.
    if (!formal_name) return 0 ;
    if (!_dot_star_scope) return 0 ;

    VeriIdDef *actual_id = _dot_star_scope->Find(formal_name,/*from*/this) ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // Viper 7090: If the instantiation is VHDL then look for actuals in the dot_stat_scope
    // ignoring the case. This is becasue like popular simulators we would pick a name differing
    // in case if no all small case name is found in the above step
    if (!actual_id && is_vhdl) {
        const char* fname = formal_name ;
        unsigned is_escaped = (fname[0] == '\\' && fname[Strings::len(fname) - 1] == '\\') ;
        if (is_escaped) {
            char *unescaped_name = VhdlNode::UnescapeVhdlName(fname) ;
            actual_id = _dot_star_scope->Find(unescaped_name,/*from*/this) ;
            Strings::free(unescaped_name) ;
        } else {
            Map *vars = _dot_star_scope->GetThisScope() ;
            MapIter mi ;
            VeriIdDef *id ;
            FOREACH_MAP_ITEM(vars, mi, 0, &id) {
                if (!id) continue ;
                const char *name = id->GetName() ;
                if (Strings::compare_nocase(name, fname)) {
                    actual_id = id ;
                    break ;
                }
            }
        }
    }
#endif
    return actual_id ;
}

//////////////////////////////////////////////////////////////////////
// Tests
//////////////////////////////////////////////////////////////////////

unsigned VeriEventExpression::IsEdge(unsigned edge_type) const
{
    /*
     * if 'edge_type==0' this question is :
     *     Is this VeriEventExpression a single edge (either positive or negative) ?
     * if 'edge_type' is not zero, the question is :
     *     Is this VeriEventExpression a single edge of type 'edge_type' ?
    */
    // VIPER 5854 : For clocking block we need to pick up the clocking event
    VeriIdDef *expr_id = _expr ? _expr->FullId() : 0 ;
    if (expr_id && expr_id->IsClocking()) {
        VeriModuleItem *clocking_blk = expr_id->GetModuleItem() ;
        VeriExpression *clocking_expr = clocking_blk ? clocking_blk->GetClockingEvent() : 0 ;
        if (clocking_expr) return clocking_expr->IsEdge(edge_type) ;
    }
    if (_edge==0) {
        // VIPER #3688 : Check _expr for edge (it could be an actual parameter
        // that was an edge.
        return (_expr) ? _expr->IsEdge(edge_type) : 0 ; /* This is not a single-edge */
    }
    if (edge_type==0) return 1 ; /* This is a single-edge */
    return (edge_type==_edge) ? 1 : 0 ; /* Check the single-edge type */
}

unsigned VeriIdRef::IsEdge(unsigned edge_type) const
{
    // Get the actual associated with this identifier reference
    VeriExpression *actual = (_id) ? _id->GetActual() : 0 ;
    if (actual && actual->IsEdge(edge_type)) return 1 ; // Actual is an edge
    return 0 ; // Not an edge
}
unsigned VeriDelayOrEventControl::IsEdge(unsigned edge_type) const
{
    if (_event_control && _event_control->Size() == 1) {
        VeriExpression *event_expr = (VeriExpression*)_event_control->At(0) ;
        return event_expr ? event_expr->IsEdge(edge_type): 0 ;
    }
    return 0 ; // 'comma' or 'or' separated event list
}
unsigned VeriSystemFunctionCall::IsEdge(unsigned edge_type) const
{
    (void) edge_type ; // Prevent unused compiler warning
    return 0 ;
}

// Sign on a data type :
unsigned VeriDataType::Sign() const
{
    if (_signing==VERI_SIGNED) return 1 ;
    if (_signing==VERI_UNSIGNED) return 0 ;

    // Check all implicitly signed base types :
    switch (_type) {
    case VERI_INTEGER :
    // case VERI_TIME : // TIME is unsigned 64 bit.
    case VERI_REAL :
    case VERI_REALTIME :
    // LRM 3.3 signed scalar data types
    case VERI_BYTE :
    case VERI_SHORTINT :
    case VERI_INT :
    case VERI_LONGINT :
    case VERI_SHORTREAL :
        return 1 ;
    default :
        break ;
    }
    return 0 ; // must be unsigned
}
unsigned VeriNetDataType::Sign() const
{
    return (_data_type) ? _data_type->Sign() : 0 ;
}

unsigned VeriTypeRef::Sign() const
{
    // Officially, we cannot have a sign on a named type ref. Check any way :
    if (_signing==VERI_SIGNED) return 1 ;
    if (_signing==VERI_UNSIGNED) return 0 ;

    // To get the sign of a type identifier, simply get the sign of its base type :
    VeriDataType *data_type = (_id) ? _id->GetDataType() : 0 ;
    if (data_type) return data_type->Sign() ;

    return 0 ;
}
unsigned VeriStructUnion::Sign() const { return (_signing == VERI_SIGNED) ? 1: 0 ; } // packed struct/union can have sign (VIPER #4004)
unsigned VeriEnum::Sign() const
{
    // Standard DataType sign, but if no type and no signing and no dimensions is mentioned,
    // then it is default 'integer' (thus signed).
    //if (!_type && !_signing && !_dimensions) return 1 ;
    if (!_base_type) return 1 ;
    // Otherwise, its the sign of the type here :
    return _base_type->Sign() ;
}
unsigned VeriTypeOperator::Sign() const
{
    // type(data_type) or type(expression)
    if (!_expr_or_data_type) return 0 ; // don't have the info

    VeriTypeInfo *type = _expr_or_data_type->CheckType(0, NO_ENV) ;
    unsigned ret_sign = (type) ? type->IsSigned() : 0 ;
    delete type ;
    return ret_sign ;
}
unsigned VeriTypeOperator::GetSigning() const
{
    return Sign()? VERI_SIGNED : VERI_UNSIGNED ;
}

// return the number of packed (array) dimensions until we hit a scalar (or struct in SV)
unsigned VeriDataType::PackedDimension() const
{
    unsigned dim = 0 ;
    VeriRange *runner = _dimensions ;
    while (runner) {
        dim++ ;
        runner = runner->GetNext() ;
    }
    return dim ;
}
unsigned VeriNetDataType::PackedDimension() const
{
    return (_data_type) ? _data_type->PackedDimension() : 0 ;
}
unsigned VeriTypeRef::PackedDimension() const {
    // Take the local packed dimension :
    unsigned result = VeriDataType::PackedDimension() ;
    // Add the dimension of the base type (_id) :
    if (_id) result = result + _id->PackedDimension() ;
    // That's the total :
    return result ;
}
unsigned VeriStructUnion::PackedDimension() const { return VeriDataType::PackedDimension() ; }
unsigned VeriEnum::PackedDimension() const
{
    // VIPER #5710: (vcs_compatibility-9000388573): If enum type has packed dimension return
    // its packed dimension otherwise return the packed dimension of its base type:
    if (_dimensions) return VeriDataType::PackedDimension() ;
    return (_base_type) ? _base_type->PackedDimension(): 0 ;
}
unsigned VeriTypeOperator::PackedDimension() const
{
    VeriTypeInfo *type = _expr_or_data_type ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned packed_dim_count = (type) ? type->PackedDimension(): 0 ;
    delete type ;
    return packed_dim_count ;
}

// return the number of unpacked (memory) dimensions until we hit a scalar (or struct in SV)
unsigned VeriDataType::UnPackedDimension() const { return 0 ; }
unsigned VeriNetDataType::UnPackedDimension() const { return (_data_type) ? _data_type->UnPackedDimension(): 0 ; }
unsigned VeriTypeRef::UnPackedDimension() const { return (_id) ? _id->UnPackedDimension() : 0 ; }
unsigned VeriStructUnion::UnPackedDimension() const { return 0 ; }
unsigned VeriEnum::UnPackedDimension() const { return 0 ; }
unsigned VeriTypeOperator::UnPackedDimension() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned unpacked_dim_count = (type) ? type->UnpackedDimension(): 0 ;
    delete type ;
    return unpacked_dim_count ;
}

// return the base scaler type (or struct/enum) of this data type (looking through all array/memory dimensions).
unsigned VeriDataType::Type() const { return _type ; }
//unsigned VeriNetDataType::Type() const { return (_data_type) ? _data_type->Type() : VERI_LOGIC /* default type for net data type*/; }
unsigned VeriNetDataType::Type() const { return _type ; } // Return nettype as type to fall every thing in proper place
unsigned VeriTypeRef::Type() const {
    return (_id) ? _id->Type() : 0 ;
}
unsigned VeriTypeRef::IsNetType() const
{
    if (!_id) return 0 ;

    if (_id->IsProcessing()) return 0 ;
    _id->SetIsProcessing() ;

    VeriDataType *data_type = _id->GetDataType() ;
    unsigned type = data_type ? data_type->IsNetType() : 0 ;
    _id->ClearIsProcessing() ;
    return type ;
}
unsigned VeriStructUnion::Type() const { return _type ; /* is VERI_STRUCT or VERI_UNION */ }
unsigned VeriEnum::Type() const { return (_base_type && _base_type->Type()) ? _base_type->Type() : VERI_ENUM ; } // default data type of enum is 'VERI_ENUM'..
unsigned VeriTypeOperator::Type() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned ret_type = (type) ? type->Type(): 0 ;
    delete type ;
    return ret_type ;
}
unsigned VeriTypeOperator::NetType() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned ret_type = (type) ? type->GetNetType(): 0 ;
    delete type ;
    return ret_type ;
}
unsigned VeriTypeOperator::IsNetType() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned ret_type = (type) ? type->IsNetType(): 0 ;
    delete type ;
    return ret_type ;
}

//////////////////////////////////////////////////////////////////////
// Obtain pointers to a member (struct/union) of this data type :
//////////////////////////////////////////////////////////////////////

// return struct-member by name (if data type is a structure/union)

VeriIdDef *VeriTypeRef::GetMember(const char *member_name) const
{
    // Find member in the data type of the referred type identifier
    if (!_id) return 0 ; // no resolved type id available.

    // Look in the local scope of this id (could be an interface / modport or so..).
    VeriScope *scope = _id->LocalScope() ;
    if (scope) {
        VeriIdDef *member = scope->FindLocal(member_name) ;
        if (member) return member ;

        // VIPER #4954 & #4859: If the referred id is a class, find it in its base:
        if (_id->IsClass()) member = scope->FindInBaseClass(member_name) ;
        if (member) return member ;
    }

    // Look in the data type of this type identifier :
    VeriDataType *data_type = _id->GetDataType() ;
    if (data_type) return data_type->GetMember(member_name) ;

    // Cant find any member or selected identifier :
    return 0 ;
}

VeriIdDef *VeriStructUnion::GetMember(const char *member_name) const
{
    // Find member in the local scope
    return (_scope) ? _scope->FindLocal(member_name) : 0 ;
}
VeriIdDef * VeriTypeOperator::GetMember(const char *member_name) const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    VeriIdDef *member = (type) ? type->GetMember(member_name): 0 ;
    delete type ;
    return member ;
}
VeriIdDef * VeriNetDataType::GetMember(const char *member_name) const
{
    return (_data_type) ? _data_type->GetMember(member_name): 0 ;
}

VeriScope *VeriNetDataType::GetMemberScope() const
{
    return (_data_type) ? _data_type->GetMemberScope(): 0 ;
}
VeriScope *VeriTypeRef::GetMemberScope() const
{
    // Return the scope which declares the members (struct/union/interface/modport) of this type.
    if (!_id) return 0 ; // no resolved type id available.

    // Look for the local scope of this id (could be an interface / modport or so..).
    VeriScope *scope = _id->LocalScope() ;
    if (scope) return scope ;

    // Look in the data type for the scope
    VeriDataType *data_type = _id->GetDataType() ;
    if (data_type) return data_type->GetMemberScope() ;

    return 0 ;
}

Array *VeriTypeRef::GetEnums() const
{
    // Return the array of enum identifiers, for enum typerefs only
    if (!_id) return 0 ; // no resolved type id available.

    // Look in the data type for this typeref :
    VeriDataType *data_type = _id->GetDataType() ;
    if (data_type) return data_type->GetEnums() ;

    return 0 ;
}
Array *VeriNetDataType::GetEnums() const
{
    return (_data_type) ? _data_type->GetEnums(): 0 ;
}

const char * VeriIdRef::GetName() const
{
    return (_name ? _name : (_id ? _id->GetName() : 0)) ;  // Get unresolved name or name of resolved id
}
const char * VeriSelectedName::GetName() const
{
    // If this name contains no _suffix_id and _suffix, we can conclude
    // that this selected name is to be treated as id-ref, so call GetName on prefix
    if (_prefix && !_suffix_id && !_suffix) return _prefix->GetName() ;
    return (_suffix ? _suffix : (_suffix_id ? _suffix_id->GetName() : 0)) ;
}
VeriScope * VeriTypeOperator::GetMemberScope() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    VeriScope *scope = (type) ? type->Scope(): 0 ;
    delete type ;
    return scope ;
}

Array * VeriTypeOperator::GetEnums() const
{
    Array *ids = 0 ;
    VeriConstraint *constraint = (_expr_or_data_type) ? _expr_or_data_type->EvaluateConstraintInternal(0, 0): 0 ;
    ids = constraint ? constraint->GetEnumeratedIds(): 0 ;
    delete constraint ;
    return ids ;
}

VeriName * VeriTypeOperator::GetTypeName() const
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) return data_type->GetTypeName() ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
    return 0 ; // If the member is not a data type then return 0
}
VeriIdDef * VeriTypeOperator::GetId() const
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) return data_type->GetId() ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
    return (_expr_or_data_type) ? _expr_or_data_type->GetId(): 0 ; // If the member is not a data type then return 0
}
const char * VeriTypeOperator::GetName() const
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) return data_type->GetName() ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
    return (_expr_or_data_type) ? _expr_or_data_type->GetName(): 0 ; // If the member is not a data type then return 0
}
VeriName * VeriTypeOperator::GetPrefix() const
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) return data_type->GetPrefix() ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
    return (_expr_or_data_type) ? _expr_or_data_type->GetPrefix(): 0 ; // If the member is not a data type then return 0
}

//////////////////////////////////////////////////////////////////////
// Obtain pointers to the nearest packed/unpacked dimension(s) :
//////////////////////////////////////////////////////////////////////

VeriRange *VeriDataType::GetDimensionAt(unsigned dimension)
{
    // Decent into the data type's dimensions
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    return runner ;
}
VeriRange *VeriNetDataType::GetDimensionAt(unsigned dimension)
{
    return (_data_type) ? _data_type->GetDimensionAt(dimension) : 0 ;
}
VeriRange *VeriNetDataType::GetDimensions()
{
    return (_data_type) ? _data_type->GetDimensions() : 0 ;
}

VeriRange *VeriTypeRef::GetDimensionAt(unsigned dimension)
{
    // First decent into the local (packed) data type's dimensions :
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) return runner ;

    // Otherwise, decend into the resolved id dimensions :
    if (_id) return _id->GetDimensionAt(dimension) ;

    return 0 ;
}

unsigned
VeriTypeRef::IsCircular(Set &defined_type_ids) const
{
    // Get the datatype of the id that this typeref refers to:
    VeriDataType *dt = (_id) ? _id->GetDataType() : 0 ;
    if (!dt) return 0 ; // Can't be a circular type

    // Try to insert the id into the Set:
    if (!defined_type_ids.Insert(_id)) return 1 ; // Can't insert, already there. So, it is circular

    // Check the datatype of this id for recursive references:
    unsigned circular = dt->IsCircular(defined_type_ids) ;

    // Don't forget to remove this id from the Set:
    (void) defined_type_ids.Remove(_id) ;

    return circular ;
}
unsigned VeriTypeOperator::IsCircular(Set &defined_type_ids) const
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) return data_type->IsCircular(defined_type_ids) ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
    return 0 ;
}

// VIPER 2336 : Need to overwrite following routines, as VeriEnum now contains VeriDataType field as base type
VeriRange *VeriEnum::GetDimensionAt(unsigned dimension)
{
    // VIPER #5710: (vcs_compatibility-9000388573): If enum type has packed dimension:
    // return the required range which is 'dimension' of the packed dimension deep into
    // this data typeotherwise call this routine for its base type.
    // First decent into the local (packed) data type's dimensions :
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) return runner ;
    // Call this routine for its base type
    return (_base_type) ? _base_type->GetDimensionAt(dimension) : 0 ;
}
VeriRange *VeriEnum::GetDimensions()
{
    // VIPER #5710: (vcs_compatibility-9000388573): If enum type has packed dimension return
    // call this routine of its base class otherwise call this routine for its base type:
    if (_dimensions) return VeriDataType::GetDimensions() ;
    return (_base_type) ? _base_type->GetDimensions() : 0 ;
}
VeriRange * VeriTypeOperator::GetDimensionAt(unsigned dimension)
{
#if 0
    (void) dimension ;
    delete _dimensions ;
    VeriConstraint *constraint = (_expr_or_data_type) ? _expr_or_data_type->EvaluateConstraintInternal(0, 0): 0 ;
    _dimensions = constraint ? constraint->CreateDimensions(0 /* packed and unpacked*/, this): 0 ;

    delete constraint ;
    VeriRange *runner = _dimensions ;
    while (dimension && runner) {
        runner = runner->GetNext() ;
        dimension-- ;
    }
    if (runner && dimension==0) return runner ;
    return 0 ; // dimension not found
#else
    // VIPER #7607: Dimension is now always set properly, return from base class:
    return VeriDataType::GetDimensionAt(dimension) ;
#endif
}
VeriRange *VeriTypeOperator::GetDimensions()
{
#if 0
    delete _dimensions ;
    VeriConstraint *constraint = (_expr_or_data_type) ? _expr_or_data_type->EvaluateConstraintInternal(0, 0): 0 ;
    _dimensions = constraint ? constraint->CreateDimensions(1 /* only unpacked*/, this): 0 ;

    delete constraint ;
    return _dimensions ;
#else
    // VIPER #7607: Dimension is now always set properly, return from base class:
    return VeriDataType::GetDimensions() ;
#endif
}

unsigned
VeriStructUnion::IsCircular(Set &defined_type_ids) const
{
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        // Get the datatype of the id that this typeref refers to:
        VeriDataType *dt = (id) ? id->GetDataType() : 0 ;
        if (!dt) continue ; // Can't be a circular type

        // Try to insert the id into the Set:
        // FIXME: Can't diferntiate between an inplace struct decl and variable decl, so insert it!
        if (!defined_type_ids.Insert(id)) return 1 ; // Can't insert, already there. So, it is circular

        // Check the datatype of this id for recursive references:
        unsigned circular = dt->IsCircular(defined_type_ids) ;

        // Don't forget to remove this id from the Set:
        (void) defined_type_ids.Remove(id) ;

        if (circular) return 1 ; // Found a circular reference
    }

    return 0 ;
}

//////////////////////////////////////////////////////////////////////
// Check whether data type is unpacked
//////////////////////////////////////////////////////////////////////
unsigned VeriDataType::IsUnpacked() const
{
    // real/shortreal data types are unpacked
    switch (_type) {
    //case VERI_TIME : Modelsim don't treat it unpacked
    case VERI_REAL :
    case VERI_REALTIME :
    case VERI_EVENT :
    case VERI_SHORTREAL :
    case VERI_CHANDLE :
    // VIPER #3113 : LRM is not clear about whether the string data type is packed
    // or unpacked. Since, strings are dynamic it is more like unpacked than packed.
    // Also other standard simulators treat strings as unpacked. So we do the same:
    case VERI_STRINGTYPE :
        return 1 ; // Unpacked
    default :
        return 0 ;
    }
}
unsigned VeriNetDataType::IsUnpacked() const
{
    return (_data_type) ? _data_type->IsUnpacked() : 0 ;
}
unsigned VeriNetDataType::IsPacked() const
{
    return (_data_type) ? _data_type->IsPacked() : 0 ;
}
unsigned VeriTypeRef::IsUnpacked() const
{
    if (_id) return _id->IsUnpacked() ; // Check unpacked on resolved type identifier
    return 0 ;
}
// VIPER #8055 : Overwrite IsPacked for type-ref
unsigned VeriTypeRef::IsPacked() const
{
    if (_id) return _id->IsPacked() ;
    return 0 ;
}
unsigned VeriStructUnion::IsUnpacked() const
{
    if (_is_packed) return 0 ; // packed struct/union
    return 1 ; // unpacked struct/union
}
unsigned VeriEnum::IsUnpacked() const
{
    return 0 ; // Can't be unpacked
}
unsigned VeriTypeOperator::IsUnpacked() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned is_unpacked = (type) ? type->IsUnpacked(): 0 ;
    delete type ;
    return is_unpacked ;
}
//////////////////////////////////////////////////////////////////////
//    Some type checking :
//////////////////////////////////////////////////////////////////////
unsigned VeriDataType::IsUnionType() const { return 0 ; }
unsigned VeriDataType::IsStructType() const { return 0 ; }
unsigned VeriDataType::IsIntegerType() const { return (_type == VERI_INTEGER) ; }
unsigned VeriNetDataType::IsUnionType() const { return (_data_type) ? _data_type->IsUnionType() : 0 ; }
unsigned VeriNetDataType::IsStructType() const { return (_data_type) ? _data_type->IsStructType() : 0 ; }
unsigned VeriNetDataType::IsIntegerType() const { return (_data_type) ? _data_type->IsIntegerType() : 0 ; }
// VIPER #5949: Check for net type of the data_type:
// ie. check whether the data_type is wire/wand etc.
unsigned VeriDataType::IsNetType() const
{
    switch(_type) {
    case VERI_WIRE:
    case VERI_TRI:
    case VERI_TRI1:
    case VERI_SUPPLY0:
    case VERI_WAND:
    case VERI_TRIAND:
    case VERI_TRIOR:
    case VERI_TRI0:
    case VERI_SUPPLY1:
    case VERI_WOR:
    case VERI_TRIREG:
    case VERI_UWIRE:
    case VERI_INTERCONNECT:
        return 1 ;
    default:
        break ;
    }
    return 0 ;
}

unsigned VeriDataType::IsInterconnectType() const
{
    if (_type == VERI_INTERCONNECT) return 1 ;
    return 0 ;
}

unsigned VeriAnsiPortDecl::IsInterconnectType() const
{
    return _data_type ? _data_type->IsInterconnectType() : 0 ;
}

unsigned VeriStructUnion::IsUnionType() const { return (_type == VERI_UNION) ; }
unsigned VeriStructUnion::IsStructType() const { return (_type == VERI_STRUCT) ; }
unsigned VeriEnum::IsIntegerType() const {  return (_base_type) ? _base_type->IsIntegerType(): 0 ; }
unsigned VeriTypeRef::IsTagged() const
{
    VeriDataType *data_type = (_id) ? _id->GetDataType() : 0 ;
    return (data_type) ? data_type->IsTagged(): 0 ;
}
unsigned VeriTypeRef::IsEnumType() const
{
    // Do not check enum type through type-ref, it will create problem
    return 0 ;
}
unsigned VeriTypeRef::IsUnionType() const
{
    VeriDataType *data_type = (_id) ? _id->GetDataType() : 0 ;
    return (data_type) ? data_type->IsUnionType() : 0 ;
}
unsigned VeriTypeRef::IsStructType() const
{
    VeriDataType *data_type = (_id) ? _id->GetDataType() : 0 ;
    return (data_type) ? data_type->IsStructType() : 0 ;
}
unsigned VeriTypeRef::IsIntegerType() const
{
    VeriDataType *data_type = (_id) ? _id->GetDataType() : 0 ;
    return (data_type) ? data_type->IsIntegerType() : 0 ;
}
unsigned VeriTypeOperator::IsTagged() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned is_tagged = (type) ? type->IsTagged(): 0 ;
    delete type ;
    return is_tagged ;
}
unsigned VeriTypeOperator::IsEnumType() const
{
    // Do not check enum type through type-ref, it will create problem
    //VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    //unsigned is_enum_type = (type) ? type->IsEnumeratedType(): 0 ;
    //delete type ;
    return 0 ;
}
unsigned VeriTypeOperator::IsUnionType() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned is_union = (type) ? type->IsUnion(): 0 ;
    delete type ;
    return is_union ;
}
unsigned VeriTypeOperator::IsStructType() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned is_struct = (type) ? type->IsStructure(): 0 ;
    delete type ;
    return is_struct ;
}
unsigned VeriTypeOperator::IsIntegerType() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned is_integer = (type) ? type->IsIntegerType(): 0 ;
    delete type ;
    return is_integer ;
}
unsigned VeriTypeOperator::IsVirtualInterface() const
{
    VeriTypeInfo *type = (_expr_or_data_type) ? _expr_or_data_type->CheckType(0, NO_ENV): 0 ;
    unsigned is_virtual_interface = (type) ? type->IsVirtualInterface(): 0 ;
    delete type ;
    return is_virtual_interface ;
}

//////////////////////////////////////////////////////////////////////
// Additional support routines
//////////////////////////////////////////////////////////////////////

// Support routine for imaging constants (mintypmax-expr can be a constant)
char *
VeriMinTypMaxExpr::Image() const
{
    // Get the Image() of all three sub expressions
    char *min_str = (_min_expr) ? _min_expr->Image() : 0 ;
    char *typ_str = (_typ_expr) ? _typ_expr->Image() : 0 ;
    char *max_str = (_max_expr) ? _max_expr->Image() : 0 ;

    // All of them should be constant and thus should return valid string
    if (!min_str || !typ_str || !max_str) return 0 ;

    // Create the string representation of this min-typ-max expression
    char *ret_str = Strings::save("(", min_str, ":", typ_str, ":", max_str, ")") ;

    // Free the temporary variables
    Strings::free(min_str) ;
    Strings::free(typ_str) ;
    Strings::free(max_str) ;

    // Return the image
    return ret_str ;
}

//Wrote this for Viper 6649 to rename the module with
//params in VeriModule::CreateUniqueUnit
char*
VeriConcat::Image() const
{
   char *ret_str = Strings::save("{") ;
   VeriExpression *item ; // Make it expression as it is printing expression list
   unsigned i ;
   unsigned first = 1 ;
   FOREACH_ARRAY_ITEM(_exprs, i, item) {
        if (!item) continue ;
        char *tmp = ret_str ;
        char *tmp1 = item->Image() ;
        ret_str = Strings::save(ret_str, ((first) ? "" : ","), tmp1) ;
        first = 0 ;
        Strings::free(tmp) ;
        Strings::free(tmp1) ;
   }
   char *tmp = ret_str ;
   ret_str = Strings::save(ret_str, "}") ;
   Strings::free(tmp) ;
   return (ret_str) ;
}
char *
VeriIdRef::Image() const
{
    if (!_id) return 0 ; // Can't do anything without resolved identifier
    // This identifier reference is constant when identifier is enumeration literal
    // So we will create image of it when _id is enumeration literal
    // Check if it is enumeration literal
    VeriDataType *data_type = _id->GetDataType() ;
    Array *enum_ids = data_type ? data_type->GetEnums() : 0 ;
    // If data type of _id is enumeration type, we cannot conclude that it is
    // enumeration literal, as it can be enumeration type variable.
    // So, check it in the enumeration literal list
    if (enum_ids) {
        unsigned i ;
        VeriIdDef *enum_id ;
        FOREACH_ARRAY_ITEM(enum_ids, i, enum_id) {
            if (enum_id == _id) { // It is enum literal
                return Strings::save(_id->Name()) ;
            }
        }
    }
    return 0 ; // Not enumeration literal
}

// VIPER #6594 (issue 131): Image routine on unary operator.
char *
VeriUnaryOperator::Image() const
{
    char *oper = (_arg) ? _arg->Image() : 0 ;
    if (!oper || !_oper_type) return 0 ;

    char *result = Strings::save(PrintToken(_oper_type), oper) ;
    Strings::free(oper) ;

    return result ;
}

// VIPER #7743 (issue 2): Image() routine on VeriRange:
char *
VeriRange::Image() const
{
    const VeriRange *dims = this ;
    char *image = 0 ;
    while (dims) {
        char *tmp = image ;
        if (IsStaticElab()) {
            // Get the dimensions of the range:
            int msb = 0, lsb = 0 ;
            (void) dims->GetWidth(&msb, &lsb, 0) ;
            char *lidx = Strings::itoa(msb) ;
            char *ridx = Strings::itoa(lsb) ;
            image = Strings::save(((image)?image:""), " [", lidx, ":", ridx, "]") ;
            Strings::free(ridx) ;
            Strings::free(lidx) ;
        } else
            image = Strings::save(((image)?image:""), " []") ;
        Strings::free(tmp) ;
        dims = dims->_next ;
    }
    return image ;
}

// Image routines for data types...
char *
VeriDataType::Image() const
{
    char *image = Strings::save(PrintToken(_type)) ;
    if (_signing) {
        char *tmp = image ;
        image = Strings::save(image, " ", PrintToken(_signing)) ;
        Strings::free(tmp) ;
    }

    // Append something for the dimensions:
    VeriRange *dims = _dimensions ;
    if (dims) {
        // VIPER #7743 (issue 2): Need to print the range bounds too:
        char *tmp = image ;
        char *dims_image = dims->Image() ;
        image = Strings::save(image, dims_image) ;
        Strings::free(dims_image) ;
        Strings::free(tmp) ;
    }
    return image ;
}

char *
VeriTypeRef::Image() const
{
    // Type reference is a type-identifier with optional dimensions :
    VeriIdDef *id = (_type_name) ? _type_name->GetId() : 0 ;
    char *image = 0 ;
    if (id) {
        // This type-ref refers to a resolved type id:
        if (!id->IsType() || !id->IsParam()) {
            // VIPER #4139: This is not a type parameter, include the id name in image:
            image = Strings::save(id->Name()) ;
        } else {
            // Include the data type for non-type-prameters:
            VeriDataType *id_type = id->GetDataType() ;
            if (id_type) image = id_type->Image() ;
        }
    } else {
        // Token type reference - print the toke name:
        image = Strings::save(PrintToken(_type)) ;
    }

    // Append something for the dimensions:
    // Only if it is not a resolved ref or we could not create an image above.
    // For resolved type-refs, the name of the resolved id is sufficient as image.
    VeriRange *dims = (!id || !image) ? _dimensions : 0 ;
    if (dims) {
        // VIPER #7743 (issue 2): Need to print the range bounds too:
        char *tmp = image ;
        char *dims_image = dims->Image() ;
        image = Strings::save(image, dims_image) ;
        Strings::free(dims_image) ;
        Strings::free(tmp) ;
    }
    return image ;
}
char * VeriTypeOperator::Image() const
{
    if (!_expr_or_data_type) return 0 ;

    // Get the image of the subexpression
    char *tmp = _expr_or_data_type->Image() ;

    // Create the string representation for the 'VeriTypeOperator'
    char *ret_str = Strings::save("(", tmp, ")") ;

    // Free the temporary variable
    Strings::free(tmp) ;

    // Return the char* image
    return ret_str ;
}
char * VeriDollar::Image() const
{
    return Strings::save("$") ;
}
char * VeriStructUnion::Image() const
{
    unsigned i, j ;
    VeriModuleItem *decl ;
    VeriIdDef *id ;
    char *names = 0 ;
    FOREACH_ARRAY_ITEM(_decls, i, decl) {
        Array *ids = decl ? decl->GetIds(): 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            if (!names) {
                names = Strings::save(id->Name()) ;
            } else {
                char *tmp = names ;
                names = Strings::save(tmp, ",", id->Name()) ;
                Strings::free(tmp) ;
            }
        }
    }
    char *image = Strings::save(PrintToken(_type), "(", names, ")") ;
    Strings::free(names) ;
    return image ;
}
char * VeriEnum::Image() const
{
    char *names = 0 ;
    unsigned i ;
    VeriIdDef *id ;
    FOREACH_ARRAY_ITEM(_ids, i, id) {
        if (!id) continue ;
        if (!names) {
            names = Strings::save(id->Name()) ;
        } else {
            char *tmp = names ;
            names = Strings::save(tmp, ",", id->Name()) ;
            Strings::free(tmp) ;
        }
    }
    char *image = Strings::save(PrintToken(VERI_ENUM), "(", names, ")") ;
    Strings::free(names) ;
    return image ;
}

/******************************/
/* AccumulatePorts */
/******************************/
// Routine, to be called on ANSI port decls, or on resolved '95 port expressions.
// Accumulate declared ports by order into one array 'ports'.
// Just add the portid in this port expression
void VeriExpression::AccumulatePorts(Array &ports) const
{
    // Must be a port-expression of named form, (VeriPortConnect)
    // or a sliced/indexed/concat port expression.
    // That is always a single port, but since we do not have an identifier, insert 0 here.
    //ports.InsertLast(0) ;
    VeriIdDef *port_id = 0 ;
    if (!IsDotStar()) {
        // VIPER #5954 : Create and declare an identifier for non-ansi port
        const char *port_name = NamedFormal() ;
        VERIFIC_ASSERT(_present_scope) ;
        VeriIdDef *owner = _present_scope->GetOwner() ;
        VeriModuleItem *owner_tree = owner ? owner->GetModuleItem(): 0 ;
        if (owner_tree && owner_tree->IsModule()) {
            // Create an identifier for unnamed/named non-ansi style port of module
            unsigned port_nr = ports.Size() ; // Get the order number of this port.
            port_id = owner_tree->DeclareNonAnsiPort(port_name, PortExpressionDir(), ExprCast(), port_nr) ;
        }
    }
    ports.InsertLast(port_id) ;
}
void VeriIdRef::AccumulatePorts(Array &ports) const
{
    // A full-id Verilo 95 port expression.
    // Insert the resolved identifier.
    ports.InsertLast(_id) ;
}
void VeriAnsiPortDecl::AccumulatePorts(Array &ports) const
{
    if (!_ids) return ;
    // A number of ANSI ports are declared here.
    // Insert all of them.
    ports.Append(_ids) ;
}

//////////////////////////////////////////////////////////////////////
// PortExpressionDir
//////////////////////////////////////////////////////////////////////
// Derive the direction of a port from a (Verilog 95) port expression.
// Also check for a valid port expression

unsigned VeriExpression::PortExpressionDir() const
{
    Error("illegal port-expression") ;
    return 0 ;
}

unsigned VeriPortOpen::PortExpressionDir() const
{
    // An 'open' port (dangling) expression is allowed.
    // (LRM 12.3.2). Assume INPUT port ?
    return VERI_INPUT ;
}

unsigned VeriIdRef::PortExpressionDir() const
{
    if (!_id) return 0 ;
    if (!_id->IsPort()) {
        Error("%s in port-expression is not a port", _id->Name()) ;
        return 0 ;
    }
    return _id->Dir() ;
}

unsigned VeriIndexedId::PortExpressionDir() const
{
    // Return the direction of the prefix :
    return (_prefix) ? _prefix->PortExpressionDir() : 0 ;
}

unsigned VeriIndexedMemoryId::PortExpressionDir() const
{
    // Return the direction of the prefix :
    return (_prefix) ? _prefix->PortExpressionDir() : 0 ;
}

unsigned VeriSelectedName::PortExpressionDir() const
{
    // Can this legally happen ? A hierarchical name in a port expression ?
    // Return the prefix expression port-direction
    return (_prefix) ? _prefix->PortExpressionDir() : 0 ;
}

//Viper 5959 Check of bit/part selects inside concat
unsigned VeriConcat::ContainsPartBitSelect() const
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        VeriExpression *idx = expr->GetIndexExpr() ;
        Array *indexes = expr->GetIndexes() ;
        if (idx || (indexes && indexes->Size() > 0)) return 1 ;
    }
    return 0 ;
}
unsigned VeriMultiConcat::ContainsPartBitSelect() const
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        if (expr->IsConcat()) {
            if (expr->ContainsPartBitSelect()) return 1 ;
        } else {
            VeriExpression *idx = expr->GetIndexExpr() ;
            Array *indexes = expr->GetIndexes() ;
            if (idx || (indexes && indexes->Size() > 0)) return 1 ;
        }
    }
    return 0 ;
}

unsigned VeriConcat::PortExpressionDir() const
{
    unsigned i ;
    VeriExpression *expr ;
    unsigned result = 0 ;
    unsigned dir ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        dir = expr->PortExpressionDir() ;
        if (result!=dir) {
            //if (result && dir) {
                //Error("all ports in concatenation should have the same direction") ;
                //return 0 ;
            //}
            if (((result == VERI_INPUT) && (dir == VERI_OUTPUT)) ||
                 ((dir == VERI_INPUT) && (result == VERI_OUTPUT)) ||
                 (result == VERI_INOUT) || (dir == VERI_INOUT)) {
                return VERI_INOUT ;
            }
            result = dir ;
        }
    }
    return result ;
}

void VeriExpression::CollectAllIds(Set *ids)
{
    if (!ids) return ; //JJ: check parameter
    VeriIdDef *id = GetId() ;
    if (id) (void) ids->Insert(id) ;
}
void VeriConcat::CollectAllIds(Set *ids)
{
    if (!ids) return ; //JJ: check parameter
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        VeriIdDef *id = expr->FullId() ;
        if (id) {
            (void) ids->Insert(id) ;
        } else {
            expr->CollectAllIds(ids) ;
        }
    }
}

unsigned VeriPortConnect::PortExpressionDir() const
{
    // VIPER 2910 : implement for VeriPortConnect.
    return (_connection) ? _connection->PortExpressionDir() : 0 ;
}

///////////////////////////////////////////////////////////////////////
//    Change reference name and resolved id                         //
//////////////////////////////////////////////////////////////////////

// Change reference names :
void VeriIdRef::SetName(char* name)
{
#ifndef VERILOG_SHARE_STRINGS
    Strings::free(_name) ;
    _name = name ;
#else
    _name = Strings::CreateConstantString(name) ;
    Strings::free(name) ;
#endif
}

void VeriIndexedId::SetName(char* name)
{
    if (_prefix) _prefix->SetName(name) ;
}

void VeriSelectedName::SetName(char* name)
{
    // If this name contains no _suffix_id and _suffix, we can conclude
    // that this selected name is to be treated as id-ref, so call SetName on prefix
    if (_prefix && !_suffix_id && !_suffix) {
        _prefix->SetName(name) ;
    } else {
#ifdef VERILOG_SHARE_STRINGS
        _suffix = Strings::CreateConstantString(name) ; // used shared string
        Strings::free(name) ;
#else
        Strings::free(_suffix) ;
        _suffix = name ;
#endif
    }
}

void VeriIndexedMemoryId::SetName(char* name)
{
    if (_prefix) _prefix->SetName(name) ;
}
// VIPER #7887 : SetName of scope-name should set name of _modport_name
void VeriScopeName::SetName(char* name)
{
    if (_modport_name) {
        _modport_name->SetName(name) ;
    } else {
        VeriSelectedName::SetName(name) ;
    }
}
void VeriIdRef::SetId(VeriIdDef *id)
{
    if (!id && _id && !_name) {
        // We are about to invalidate a _id pointer, but there is no _name left.
        // Make to to save the name before we loose all reference info :
#ifndef VERILOG_SHARE_STRINGS
        _name = Strings::save(_id->Name()) ;
#else
        _name = Strings::CreateConstantString(_id->Name()) ;
#endif
    }
    _id = id ;

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    if (_id) {
        Strings::free(_name) ; _name = 0 ;
    }
#endif
#endif
}
void VeriFunctionCall::SetName(char* name)
{
    if (_func_name) _func_name->SetName(name) ;
}
void VeriTypeOperator::SetName(char *name)
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) data_type->SetName(name) ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
}
void VeriIndexedId::SetId(VeriIdDef *id)
{
    _id = id ;
    if (_prefix) _prefix->SetId(id) ;
}
void VeriSelectedName::SetId(VeriIdDef *id)
{
    // If this name contains no _suffix_id and _suffix, we can conclude
    // that this selected name is to be treated as id-ref, so call SetName on prefix
    if (_prefix && !_suffix_id && !_suffix) {
        _prefix->SetId(id) ;
    } else {
        _suffix_id = id ;
    }
}
void VeriIndexedMemoryId::SetId(VeriIdDef *id)
{
    _id = id ;
    if (_prefix) _prefix->SetId(id) ;
}
void VeriFunctionCall::SetId(VeriIdDef *id)
{
    if (_func_name) _func_name->SetId(id) ;
}
void VeriTypeOperator::SetId(VeriIdDef *id)
{
    if (_expr_or_data_type && _expr_or_data_type->IsDataType()) {
        VeriDataType * data_type = _expr_or_data_type->DataTypeCast() ; // Use dynamic casting to cast the member element to VeriDataType
        if (data_type) data_type->SetId(id) ; // Call the corresponding routine for VeriDataType
    }
    // FIXME : Need to do something if _expr_or_data_type is an expression
}
// VIPER #7887 : SetId of scope-name should set id of _modport_name
void VeriScopeName::SetId(VeriIdDef *id)
{
    if (_modport_name) {
        _modport_name->SetId(id) ;
    } else {
        VeriSelectedName::SetId(id) ;
    }
}

void VeriTypeRef::SetId(VeriIdDef *id)
{
    _id = id ;
    if (_type_name) _type_name->SetId(id) ;
}
void VeriSelectedName::SetPrefixId(VeriIdDef *id)
{
    if (!id) return ;
    _prefix_id = id ;
    if (_prefix) _prefix->SetId(id) ;
}
void VeriSelectedName::SetPrefix(VeriName *prefix)
{
    delete _prefix ;
    _prefix = prefix ;
    _prefix_id = _prefix ? _prefix->GetId(): 0 ;
}
void VeriSelectedName::ResetPrefixId()
{
    _prefix_id = 0 ;
    if (_prefix) _prefix->SetId(0) ;
}
VeriName *VeriName::SetParamValues(Array *values)
{
    // VIPER #6609 (test2.sv): Do not produce syntax error, produce
    // a different error which can be suppressed if required by the user.
    //Error("syntax error near %s", GetName() ? GetName(): "") ;
    Error("use of %s #(params) here violates IEEE 1800 syntax", (GetName()) ? GetName() : "") ;
    // Memory leak fix:
    unsigned i ;
    VeriExpression *val ;
    FOREACH_ARRAY_ITEM(values, i, val) delete val ;
    delete values ;
    return this ; // 0 ; // Do not return 0 to avoid memory leak, or we have to delete 'this' here
}
VeriName *VeriIdRef::SetParamValues(Array *values)
{
    // Id-ref has no field to add 'values', but 'class_id #(..)' can appear as
    // data type. Create VeriScopeName with 'this' and 'values
    return new VeriScopeName(this, 0/* suffix null*/, values) ;
}
VeriName *VeriScopeName::SetParamValues(Array *values)
{
    // Delete old entries
    unsigned i ;
    VeriExpression *val ;
    FOREACH_ARRAY_ITEM(_param_values, i, val) delete val ;
    delete _param_values ;
    _param_values = values ; // Set incoming value
    return this ; // Return itself
}
void VeriName::SetModportName(VeriName * /*s*/) {}
void VeriScopeName::SetModportName(VeriName *modport_name)
{
    delete _modport_name ;
    _modport_name = modport_name ;
}
////////////////// Data type from expression ////////////////////////
VeriDataType *VeriIdRef::GetDataType() const
{
    return _id ? _id->GetDataType() : 0 ;
}
VeriDataType *VeriIndexedId::GetDataType() const
{
    return _id ? _id->GetDataType() : 0 ;
}
VeriDataType *VeriSelectedName::GetDataType() const
{
    return _suffix_id ? _suffix_id->GetDataType() : 0 ;
}
VeriDataType *VeriIndexedMemoryId::GetDataType() const
{
    return _id ? _id->GetDataType() : 0 ;
}

//////////////////////////////////////////////////////////////////////////////////////
/// Obtain pointers to base data type (built-in type/struct/union/enum)
//  from data-type/lvalue
//////////////////////////////////////////////////////////////////////////////////////

// Data types like built-in type, structure, union or enumeration are the base
// data types. So, to get based data type we have to look through named types
// and all other data types are based types.
VeriDataType *VeriDataType::GetBaseDataType() const
{
    // Return itself for VeriDataType (built-in type), VeriStructUnion (struct/union)
    // and VeriEnum (enumeration)
    return const_cast<VeriDataType*>(this) ;
}
VeriDataType *VeriTypeRef::GetBaseDataType() const
{
    return (_id) ? _id->GetBaseDataType() : 0 ; // look through type identifier
}
VeriDataType *VeriTypeOperator::GetBaseDataType() const
{
    return (_expr_or_data_type) ? _expr_or_data_type->GetBaseDataType() : 0 ; // return base type of containing expression/data type
}

// Base data type of lvalue expressions can be obtained from resolved identifiers
VeriDataType *VeriIdRef::GetBaseDataType() const
{
    return (_id) ? _id->GetBaseDataType() : 0 ; // return base type of resolved identifier
}
VeriDataType *VeriIndexedId::GetBaseDataType() const
{
    return (_id) ? _id->GetBaseDataType() : 0 ; // return base type of resolved identifier
}
VeriDataType *VeriIndexedMemoryId::GetBaseDataType() const
{
    return (_id) ? _id->GetBaseDataType() : 0 ; // return base type of resolved identifier
}
VeriDataType *VeriSelectedName::GetBaseDataType() const
{
    return (_suffix_id) ? _suffix_id->GetBaseDataType() : 0 ; // return base type of resolved identifier
}
VeriDataType *VeriAssignmentPattern::GetBaseDataType() const
{
    return (_type) ? _type->GetBaseDataType() : 0 ; // return base type of casting type
}

////////////////////////////////////////////////////////////////////////////////
//// Obtain the number of packed (array) dimensions of lvalue expressions :
////////////////////////////////////////////////////////////////////////////////

unsigned VeriIdRef::PackedDimension() const
{
    // Packed dimension count of simple identifier reference is packed dimension
    // count of resolved identifier
    return (_id) ? _id->PackedDimension() : 0 ;
}
unsigned VeriIndexedId::PackedDimension() const
{
    if (!_id || !_idx || !_prefix) return 0 ; // Not resolved, can't calculate packed dimension count

    if (_idx->IsRange()) { // Part select 'var[2:0]'
        // Part selection cannot change dimension count of identifier
        return _prefix->PackedDimension() ; // packed dim(s) remains same
    }
    if (_id->UnPackedDimension()) {
        // Identifier contains unpacked dimension, so select is on unpacked dimension
        // Packed dimension of referenced identifier will not be changed
        // bit [3:0][2:0] var [2:0] ;
        // ...var[2]
        return _id->PackedDimension() ;
    } else { // Bit/word selection
        // Identifier contains no unpacked dimension. So, select is on packed dimension
        // Get the number of packed dimensions
        unsigned packed_dim_count = _id->PackedDimension() ;
        if (packed_dim_count) {
            // Referred identifier contains packed dimension, so selection is
            // on least varying packed dimension
            // bit [3:0][2:0] var ;
            // ...var[2] ;
            return (packed_dim_count - 1) ; // return one less packed dimension
        } else { // no packed dimension
            // Select on vectored type or on scalar object
            // int a ;
            // ..a[2] ;
            return 0 ; // no packed dimension
        }
    }
}
unsigned VeriIndexedMemoryId::PackedDimension() const
{
    if (!_id || !_indexes) return 0 ; // Not resolved, can't calculate dimension count

    // This is bit/part selection on a object having multiple dimensions (packed
    // and unpacked). Select may happen either in packed or unpacked dimension.

    // Get the number of unpacked dimensions of resolved identifier
    unsigned unpacked_dim_count = _id->UnPackedDimension() ;
    // Get the number of packed dimensions of resolved identifier
    unsigned packed_dim_count = _id->PackedDimension() ;

    // Get the last index
    VeriExpression *last_index = (VeriExpression*)_indexes->GetLast() ;

    // Part select on a packed /unpacked dimension does not reduce its
    // dimension count, but bit selection reduces dimension count.
    // Get the number of dimensions those are not selected
    unsigned dims_not_selected = (unpacked_dim_count + packed_dim_count) - ((last_index && last_index->IsRange()) ?  (_indexes->Size() - 1) : _indexes->Size()) ;

    // Now check if this selection is on packed/unpacked dimension.
    //  bit [3:0][4:0] var [3:0][2:0][1:0] ;
    //
    //  ...var[3][2]        // Select on unpacked dimension
    //  ...var[3][2][1][3]  // Select on packed dimension
    if (dims_not_selected <= packed_dim_count) {
        // All unpacked dimension dimensions are covered, select is on packed
        // dimension or last unpacked dimension
        //
        // ....var[3][2][1]     // Select on last unpacked dimension
        // ....var[3][2][1][2]  // Select on least varying packed dimension
        // 'dims_not_selected' is the packed dimension count of this expression
        return dims_not_selected ;
    } else {
        // Select is on unpacked dimension. So, packed dimension of this expression
        // is same as packed dimension count of resolved identifier
        return packed_dim_count ;
    }
}
unsigned VeriSelectedName::PackedDimension() const
{
    // Packed dimension count of selected name is that of selected identifier
    return (_suffix_id) ? _suffix_id->PackedDimension() : 0 ;
}
unsigned VeriAssignmentPattern::PackedDimension() const
{
    if (!_type) return 0 ; // No type field, cannot calculate number of packed dimension
    // FIXME : Should do something if _type is not present
    return _type->PackedDimension() ; // return number of packed dimension of _type
}

////////////////////////////////////////////////////////////////////////////////
// Obtain the number of unpacked (memory) dimensions until we hit a base type
//////////////////////////////////////////////////////////////////////////////////
unsigned VeriIdRef::UnPackedDimension() const
{
    // Number of unpacked dimension of simple identifier reference is same as
    // number of unpacked dimension of the identifier
    return (_id) ? _id->UnPackedDimension() : 0 ;
}
unsigned VeriIndexedId::UnPackedDimension() const
{
    if (!_id || !_idx) return 0 ; // Can't calculate on unresolved name reference
    // Get the number of unpacked dimension of identifier
    unsigned unpacked_dim_count = _id->UnPackedDimension() ;

    if (_idx->IsRange()) { // Part select
        // Part selection does not change dimension count unless range is [val : val]
        // i.e. range width 1. In that case select should behave like indexing.
        if (_idx->RangeWidth() == 1 && unpacked_dim_count) unpacked_dim_count-- ; // Indexing, decrease dimension count by 1
        return unpacked_dim_count ; // unpacked dimension count same as that of identifier
    } else { // bit/word selection
        if (unpacked_dim_count) {
            // Object contains unpacked dimension. So, select is on unpacked
            // dimension
            return (unpacked_dim_count - 1) ; // index selection reduces dimension
        }
    }
    return 0 ; // No unpacked dimension
}
unsigned VeriIndexedMemoryId::UnPackedDimension() const
{
    if (!_id || !_indexes) return 0 ; // Not resolved, can't calculate dimensions
    VeriExpression *last_index = (VeriExpression*)_indexes->GetLast() ; // Get last index

    // Part selection does not reduce dimension count when range width is greater
    // than 1, but index selection reduces dimension count by one.
    // Calculate the number of selected dimensions
    unsigned dims_selected = (last_index && last_index->IsRange() && (last_index->RangeWidth() != 1)) ? (_indexes->Size() - 1) : _indexes->Size() ;

    // Get the number of unpacked dimension of the identifier
    unsigned unpacked_dim_count = _id->UnPackedDimension() ;

    // If 'dims_selected' is less than or equal to number of unpacked dimenison
    // of the identifier, selection is on unpacked dimension. Otherwise selection
    // is on packed dimension
    // bit [3:0][2:0] x [2:0][3:0][2:0] ;
    //
    // .......x[2][3]        // selection on unpacked dimension, unpacked count 1
    // .......x[2][3][2]     // selection on unpacked dimension, unpacked count 0
    // .......x[2][3][2][1]  // selection on packed dimension, unpacked dim count 0
    if (dims_selected <= unpacked_dim_count) {
        // Select on unpacked dimension
        return (unpacked_dim_count - dims_selected) ;
    }
    return 0 ; // No unpacked dimension
}
unsigned VeriSelectedName::UnPackedDimension() const
{
    // Number of unpacked dimension of simple identifier reference is same as
    // number of unpacked dimension of the identifier
    return (_suffix_id) ? _suffix_id->UnPackedDimension() : 0 ;
}
unsigned VeriAssignmentPattern::UnPackedDimension() const
{
    if (!_type) return 0 ; // No type field, cannot calculate number of unpacked dimension
    // FIXME : Should do something if _type is not present
    return _type->UnPackedDimension() ;
}
unsigned VeriRange::RangeWidth()
{
    // This routine returns the width of this range if bounds are specified by constants
    // If bounds are not constant literals, this routine returns 0
    if (_part_select_token) { // +:/-: range: width is specified by _right
        int width = (_right) ? _right->Integer() : 0 ;
        return (unsigned)((width < 0) ? ABS(width) : width) ;
    }
    // Normal range: get left and right bounds id those are constant literals
    if (_left && _right && _left->IsConst() && _right->IsConst()) {
        int l_bound = _left->Integer() ;
        int r_bound = _right->Integer() ;
        unsigned width = 0 ;
        if (l_bound > r_bound) {
            width = (unsigned)((l_bound - r_bound) + 1) ;
        } else {
            width = (unsigned)((r_bound - l_bound) + 1) ;
        }
        return width ;
    }
    return 0 ; // Bounds are not specified with literals
}
unsigned VeriRange::IsSingleValueRange() const
{
    // First check myself: am I a single value range?
    if (_left && !_right && (VERI_NONE == _part_select_token)) return 1 ;

    // Check the next range in the linked list:
    return ((_next) ? _next->IsSingleValueRange() : 0) ;
}

unsigned VeriBinaryOperator::HasDollar() const
{
    // Check whether this binary operator involves $:
    return ((_left && _left->HasDollar()) || (_right && _right->HasDollar())) ? 1 : 0 ;
}

unsigned VeriRange::HasDollar() const
{
    // Check whether the range contains $ in any of its branches:
    return ((_left && _left->HasDollar()) || (_right && _right->HasDollar())) ? 1 : 0 ;
}

unsigned VeriRange::IsDynamicRange() const
{
    // Dynamic ranges can be [] or [$] or [int].
    VeriIdDef *left_id = _left ? _left->GetId(): 0 ;
    if (HasDollar() ||
        (!_left && !_right && ((_part_select_token == VERI_NONE) || (_part_select_token == VERI_MUL))) ||
        (!_left && _right && _right->DataTypeCast() && (_part_select_token == VERI_NONE)) ||
        (_left && left_id && left_id->IsType() && !_right)) { // lint !e613
         return 1 ;
     }

    return 0 ;
}

/////////////////////////////////////////////////////////////////////////////
/////  Check type of expressions in assignment like context :
/////////////////////////////////////////////////////////////////////////////
VeriTypeInfo *VeriConstVal::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // It is always packed type and its packed size is same as its size
    VeriTypeInfo *this_type = new VeriArrayType(new VeriBuiltInType(VERI_REG), 1) ;
    if (_is_string) this_type->SetAsStringLiteral() ; // 'this_type' is for string literal
    if (Sign()) this_type->SetAsSigned() ;
    if (this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriIntVal::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = new VeriBuiltInType(VERI_INTEGER) ; // Create integer type

    if (this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriRealVal::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = new VeriBuiltInType(VERI_REAL) ; // Create real type

    if (this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriTimeLiteral::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = new VeriBuiltInType(VERI_REALTIME) ; // It is interpreted as a realtime value

    if (this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriIdRef::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    if (!_id && expected_type && expected_type->IsEnumeratedType()) {
        // VIPER #4434: Try to resolve enum literal from the expected type:
        _id = expected_type->GetEnum(_name) ;
    }

    // VIPER #4077 : If identifier is not resolved, created unresolved name type
    if (!_id) return new VeriNamedType(this) ; // Identifier not resolved

#ifdef VERILOG_FREE_NAME_FROM_RESOLVED_IDREF
#ifndef VERILOG_SHARE_STRINGS
    // Do not modify parse tree in CheckType routine
    //Strings::free(_name) ; _name = 0 ;
#endif
#endif
    // Get the full-type information of this identifier
    VeriTypeInfo *this_type = _id->CreateType(0) ;

    // Check the usage of this identifier
    VeriTypeInfo::CheckIdentifierUsage(_id, this_type, environment, /*from*/this) ;

    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriIdRef::CreateTypeForMethods(Array *arguments, unsigned is_task, veri_type_env environment)
{
    unsigned function_type = GetFuncTokenOfClass(_name) ;
    if (!function_type) return 0 ;
    return CreateTypeForMethodsInt(0, function_type, arguments, is_task, environment) ;
}
VeriTypeInfo *VeriSelectedName::CreateTypeForMethods(Array *arguments, unsigned is_task, veri_type_env environment)
{
    if (!_function_type || !_prefix) return 0 ; // No function type can't do anything
    return CreateTypeForMethodsInt(_prefix, _function_type, arguments, is_task, environment) ;
}
VeriTypeInfo *VeriName::CreateTypeForMethodsInt(VeriName *prefix, unsigned function_type, const Array *arguments, unsigned is_task, veri_type_env environment) const
{
    if (!function_type) return 0 ;

    VeriTypeInfo *prefix_type = (prefix) ? prefix->CheckType(0, NO_ENV): 0 ;
    //if (!prefix_type) return 0 ; // FIXME : is it correct
    // Do not create type if prefix_type is from type parameter/unresolved name
    if (prefix_type && (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType())) {
        delete prefix_type ;
        return 0 ;
    }

    VeriIdDef *prefix_id = (prefix) ? prefix->GetId() : 0 ;
    // Viper 5314. index type methods cannot have associative wildcard array as prefix
    //VeriRange *msb_dim = prefix_id ? prefix_id->GetDimensionAt(0): 0 ;
    //if (msb_dim && msb_dim->GetPartSelectToken() == VERI_MUL && ((function_type == VERI_METHOD_FIND_FIRST_INDEX)||
    if (prefix_type && prefix_type->IsAssociativeArrayType() && !prefix_type->IndexType()
            && ((function_type == VERI_METHOD_FIND_FIRST_INDEX)    ||
                    (function_type == VERI_METHOD_FIND_INDEX)      ||
                    (function_type == VERI_METHOD_FIND_LAST_INDEX) ||
                    (function_type == VERI_METHOD_UNIQUE_INDEX))) {
        Error("prefix of array method %s should not be %s", PrintToken(function_type), "a wildcard associative array") ;
    }

    VeriTypeInfo *ret_type = 0 ;
    VeriExpression *arg = 0 ;
    VeriTypeInfo *arg_type = 0 ;
    // VIPER #7907: Move this type check for each built-in methods. Do the type checking with their expected type.
    //VeriExpression *arg = (arguments && arguments->Size()) ? (VeriExpression*)arguments->At(0) : 0 ;
    //VeriTypeInfo *arg_type = (arg) ? arg->CheckType(0, READ) : 0 ;
    //if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
    //    delete arg_type ;
    //    arg_type = 0 ;
    //}

    // Create type depending on return value of method
    switch (function_type) {
    // 3.1 methods :
    // string conversion functions :
    case VERI_METHOD_LEN : // str.len()
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s", PrintToken(function_type), PrintToken(VERI_STRINGTYPE)) ;
        }
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        // Return type is 'int'
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_METHOD_PUTC : // str.putc
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }

        // Number of arguments should be 2
        if (!arguments || (arguments->Size() != 2)) Error("%s expects %d arguments", PrintToken(function_type), 2) ;

        // VIPER #7907: First argument for putc method is integer.
        // So do type check with expected type as integer.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }

            if (arg_type && !arg_type->IsIntegralType()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }
            delete arg_type ;
            arg_type = 0 ;
        }

        // VIPER #7907: Second argument for putc method is byte.
        // So do type check with expected type as byte.
        if (arguments && arguments->Size() > 1) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_BYTE) ;
            arg = (VeriExpression *)arguments->At(1) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }

            if (arg_type && !arg_type->IsIntegralType() && !arg_type->IsString()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "byte") ;
            }
        }

        break ; // It is a task no return type
    case VERI_METHOD_TOLOWER : // str.tolower()
    case VERI_METHOD_TOUPPER : // str.tolower()
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        ret_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
        break ;
    case VERI_METHOD_ATOBIN : // str.atobin()
    case VERI_METHOD_ATOHEX : // str.atohex()
    case VERI_METHOD_ATOI :   // str.atoi()
    case VERI_METHOD_ATOOCT : // str.atooct()
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        ret_type = new VeriBuiltInType(VERI_INTEGER) ;
        break ;
    case VERI_METHOD_ATOREAL : // str.atoreal()
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        ret_type = new VeriBuiltInType(VERI_REAL) ;
        break ;
    case VERI_METHOD_ITOA : // tasks on string
    case VERI_METHOD_HEXTOA :
    case VERI_METHOD_OCTTOA :
    case VERI_METHOD_BINTOA :
    case VERI_METHOD_REALTOA :
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        // Number of arguments should be 1
        if (!arguments || (arguments->Size() != 1)) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

        // VIPER #7907: Argument for the above methods is integer/real(for realtoa).
        // So do type check with expected type as integer/real(for realtoa).
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = (function_type != VERI_METHOD_REALTOA) ? new VeriBuiltInType(VERI_INT) : new VeriBuiltInType(VERI_REAL) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }

            if (arg_type && (function_type != VERI_METHOD_REALTOA) && !arg_type->IsIntegralType()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }
            if (arg_type && (function_type == VERI_METHOD_REALTOA) && !arg_type->IsRealType() && !arg_type->IsIntegralType()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "real") ;
            }
        }
        break ; // It is a task no return type
    case VERI_METHOD_COMPARE : // str.compare(str)
    case VERI_METHOD_ICOMPARE : // str.icompare(str)
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        // Check argument count :
        if (!arguments || arguments->Size() != 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

        // VIPER #7907: Argument for the above methods is string.
        // So do type check with expected type as string.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Argument should be string/string literal
            if (arg_type && !arg_type->IsString() && !arg_type->IsStringLiteral()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "string") ;
            }
        }

        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_METHOD_SUBSTR : // task (assign to prefix) : str.substr(i,j)
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        // VIPER #5248 (vcs_compatibility_issue-9000387999): According to LRM P1800(2009) this function expect 2 arguments,
        // thus we are giving correct error but vcs does not error out. So we put this check into relaxed check in mode.
        if (InRelaxedCheckingMode()) {
            if (!arguments || (arguments->Size() != 1 && arguments->Size() !=2)) Error("%s needs at least %d arguments", PrintToken(function_type), 1) ;
        } else { // expect at least one argument
            if (!arguments || arguments->Size() != 2) Error("%s expects %d arguments", PrintToken(function_type), 2) ;
        }

        // VIPER #7907: First argument for the above methods is integer.
        // So do type check with expected type as integer.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            if (arg_type && !arg_type->IsIntegralType()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }
            delete arg_type ;
            arg_type = 0 ;
        }

        // VIPER #7907: Second argument for the above methods is integer.
        // So do type check with expected type as integer.
        if (arguments && arguments->Size() > 1) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
            arg = (VeriExpression *)arguments->At(1) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            if (arg_type && !arg_type->IsIntegralType()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }
        }

        ret_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
        break ;
    case VERI_METHOD_GETC : // str.getc(i)
        // Prefix of these methods should be of type string
        if (prefix_type && !prefix_type->IsString()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_STRINGTYPE)) ;
        }
        // Check argument count
        if (!arguments || arguments->Size() != 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

        // VIPER #7907: Argument for the getc method is integer.
        // So do type check with expected type as integer.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Argument should be of type 'int'
            if (arg_type && !arg_type->IsIntegralType()) {
                Error("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }
        }

        ret_type = new VeriBuiltInType(VERI_BYTE) ;
        break ;
    // enum (and associative/dynamic arrays) :
    case VERI_METHOD_NUM :   // enum.num() or array.num()
        // Prefix should be enum/associative array
        if (prefix_type && (!prefix_type->IsEnumeratedType() && !prefix_type->IsAssociativeArrayType())) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
        }
        // Check argument count :
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_METHOD_NAME :  // enum.name()
        if (prefix_type && !prefix_type->IsEnumeratedType()) {
            // VIPER #5248 (test_81): Do not produce the error if the method 'name' is called on a port of clocking block
            // for which we could not set the data type because of the fact that the initial value could not be resolved.

            // First, find whether the method is called on something defined inside clocking:
            VeriName *last_prefix = prefix ;
            while (last_prefix && (!last_prefix->GetId() || !last_prefix->GetId()->IsClocking())) {
                last_prefix = last_prefix->GetPrefix() ;
            }
            VeriIdDef *clocking_id = (last_prefix) ? last_prefix->GetId() : 0 ;

            // Second, find whether the id on which 'name' is called has data type set. If not check
            // whether it has initial value and the value is resolved (id of initial value is set):
            VeriExpression *prefix_id_init_val = (prefix_id) ? prefix_id->GetInitialValue() : 0 ;
            if (!clocking_id || ((prefix_id && prefix_id->GetDataType()) ||
                                 !prefix_id_init_val || !prefix_id_init_val->IsHierName())) {
                // So, here, prefix is not enum type but it is...
                // - either not defined in clocking block,
                // - or has data type set (but surely is not enum type),
                // - or has no initial value (so data type could not be inferred),
                // - or has initial value and is not a hier-name. In such case the initial value
                // must be resolved and data type must be set, so, produce the error. If the initial
                // value is hier-name, we may not be able to resolve it, so do not error out in such case:
                Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
            }
        }
        // Check argument count :
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        ret_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
        break ;
    // enum (and associative/dynamic arrays). first(), last() takes 0 or 1 argument - SV LRM 3.1, section 4.10.4 :
    case VERI_METHOD_FIRST : // enum.first() or array.first(N)
    case VERI_METHOD_LAST :  // enum.last() or array.last(N)
    case VERI_METHOD_NEXT :  // enum.next(N=1)  or array.next(N)
    case VERI_METHOD_PREV :  // enum.prev(N=1)  or array.prev(N)
        {
        if (prefix_type && prefix_type->IsEnumeratedType()) {
            ret_type = prefix_type->Copy() ; // Return type is same enum type
            // Enum method first, last expects 0 argument
            if ((function_type == VERI_METHOD_FIRST) || (function_type == VERI_METHOD_LAST)) {
                if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
            } else {
                if (arguments && arguments->Size() > 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

                // VIPER #7907: Argument for the above methods is of type integer.
                // So do type check with expected type as integer.
                if (arguments && arguments->Size() > 0) {
                    VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
                    arg = (VeriExpression *)arguments->At(0) ;
                    arg_type = arg->CheckType(ele_type, READ) ;

                    if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                        delete arg_type ;
                        arg_type = 0 ;
                        delete ele_type ;
                        ele_type = 0 ;
                    }
                    // Argument should be of type 'int'
                    if (arg_type && !arg_type->IsAssignmentCompatible(ele_type)) {
                        Error ("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
                    }
                    delete ele_type ;
                    ele_type = 0 ;
                }
            }
        } else if (prefix_type && prefix_type->IsAssociativeArrayType()) {
            ret_type = new VeriBuiltInType(VERI_INT) ;
            // It should have 1 argument
            if (!arguments || arguments->Size() != 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

            // VIPER #7907: Type of argument should be compatible with the index type of associative array.
            // So do type check with expected type as indexed type.
            if (arguments && arguments->Size() > 0) {
                VeriTypeInfo *ele_type = prefix_type->IndexType() ;
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;

                if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                    delete arg_type ;
                    arg_type = 0 ;
                }
                // Argument should be of type 'int'
                if (arg_type && ele_type && !arg_type->IsAssignmentCompatible(ele_type)) {
                    Error ("illegal argument in %s, require %s", PrintToken(function_type), "index type of associative array") ;
                }
            }
        } else if (prefix_type) {
            Error("prefix of method %s should be %s",PrintToken(function_type),PrintToken(VERI_ENUM)) ;
        }
        break ;
        }
    //case VERI_METHOD_SIZE : // array.size() (queue or dynamic arrays only ?)
    case VERI_SYS_CALL_SIZE : // the actual token returned from string 'size'.
        if (prefix_type && !prefix_type->IsDynamicArrayType() && !prefix_type->IsQueue() && !prefix_type->IsAssociativeArrayType()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a dynamic array or associative array or queue") ;
        }
        // Check argument count :
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_METHOD_EXISTS : // associative_array.exists(index)
        {
        if (prefix_type && !prefix_type->IsAssociativeArrayType()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"an associative array") ;
        }
        // It should have 1 argument
        if (!arguments || arguments->Size() != 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

        // VIPER #7907: Do type checking.
        if (arguments && arguments->Size() > 0) {
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(0, READ) ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Type of argument should be compatible with the index type of associative array :
            VeriTypeInfo *index_type = prefix_type ? prefix_type->IndexType() : 0 ;
            if (arg_type && index_type && !arg_type->IsAssignmentCompatible(index_type)) {
                // VIPER #5707 : Standard simulators do not produce this error so produce warning
                Warning ("illegal argument in %s, require %s", PrintToken(function_type), "index type of associative array") ;
            }
        }
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
        }
    case VERI_METHOD_DELETE : // array.delete() or array.delete(index)
        {
        // Prefix should be dynamic array/associative array/queue
        if (prefix_type && prefix_type->IsDynamicArrayType()) {
            // It cannot have any argument
            if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        } else if (prefix_type && prefix_type->IsAssociativeArrayType()) {
            if (arguments && arguments->Size() > 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

            // VIPER #7907: Argument should be of indexed type.
            // So do type check with expected type as indexed type.
            if (arguments && arguments->Size() > 0) {
                VeriTypeInfo *ele_type = prefix_type ? prefix_type->IndexType() : 0 ;
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;

                if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                    delete arg_type ;
                    arg_type = 0 ;
                }
                // Argument should be of index type
                if (arg_type && ele_type && !arg_type->IsAssignmentCompatible(ele_type)) {
                    Error ("illegal argument in %s, require %s", PrintToken(function_type), "index type of associative array") ;
                }
            }
        } else if (prefix_type && prefix_type->IsQueue()) {
            // As per p1800 LRM Section 5.14.2.3. It clearly says that the prototype of Queue
            // method delete is function void delete (int index). this is different from
            // the associative array method delete defined in 5.10.2. (which is function void delete ([int index]))
            // The associative array delete can be called without any arguments as the argument is optional there.
            // Hence as per lrm queue.delete is an error. However, most of the common simulation tools like
            // modelsim and vcs does not seem to distinguish between queue method delete and array method delete at
            // least from the function prototype point of view. Hence for Viper 5172 changed the argument checking
            // algorithm to error out only when # arguments of delete is greater than one.
            if (arguments && arguments->Size() > 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

            // VIPER #7907: Argument for the above methods is of type integer.
            // So do type check with expected type as integer.
            if (arguments && arguments->Size() > 0) {
                VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;
                delete ele_type ;
                ele_type = 0 ;

                if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                    delete arg_type ;
                    arg_type = 0 ;
                }
                // Argument should be of 'int' type
                if (arg_type && !arg_type->IsIntegralType()) {
                    Error ("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
                }
            }
        } else if (prefix_type) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a dynamic array or associative array or queue") ;
        }
        // This method returns void type
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        // Return type is void, donot create any type
        break ;
        }
    case VERI_METHOD_INSERT : // queue.insert(index,item)
        {
        if (prefix_type && !prefix_type->IsQueue()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a queue") ;
        }
        if (!arguments || arguments->Size() > 2) Error("%s expects %d arguments", PrintToken(function_type), 2) ;

        // VIPER #7907: First Argument for the above methods is of type integer.
        // So do type check with expected type as integer.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Argument should be of 'int' type
            if (arg_type && !arg_type->IsIntegralType()) {
                Error ("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }
            delete arg_type ;
            arg_type = 0 ;
        }

        // VIPER #7907: Second argument should be of element type.
        // So do type check with expected type as element type.
        if (arguments && arguments->Size() > 1) {
            VeriTypeInfo *ele_type = (prefix_type) ? prefix_type->ElementType(): 0 ;
            arg = (VeriExpression *)arguments->At(1) ;
            arg_type = arg->CheckType(ele_type, READ) ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            if (arg_type && ele_type && !arg_type->IsAssignmentCompatible(ele_type)) {
                Error ("illegal argument in %s, require %s", PrintToken(function_type), "element type of queue") ;
            }
        }

        // These methods return void type
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ; // Return type is void, donot create any type
        }
    case VERI_METHOD_PUSH_BACK : // queue.push_back(item)
    case VERI_METHOD_PUSH_FRONT : // queue.push_front(item)
        {
        if (prefix_type && !prefix_type->IsQueue()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a queue") ;
        }
        if (!arguments || arguments->Size() > 1) Error("%s expects %d arguments", PrintToken(function_type), 1) ;

        // VIPER #7907: Argument should be of element type.
        // So do type check with expected type as element type.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = (prefix_type) ? prefix_type->ElementType() : 0 ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Type of argument should be compatible to element type of queue
            if (arg_type && ele_type && !arg_type->IsAssignmentCompatible(ele_type)) {
                Error ("illegal argument in %s, require %s", PrintToken(function_type), "element type of queue") ;
            }
        }

        // These methods return void type
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ; // Return type is void, donot create any type
        }
    case VERI_METHOD_POP_BACK : // queue.pop_back()
    case VERI_METHOD_POP_FRONT : // queue.pop_front()
        {
            // Prefix should be queue
            if (prefix_type && !prefix_type->IsQueue()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a queue") ;
            }
            if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments", PrintToken(function_type), 0) ;

            // Return type is the element type of queue
            VeriTypeInfo *element_type = (prefix_type) ? prefix_type->ElementType(): 0 ; // Get type of element
            unsigned packed_dim = (prefix_type) ? prefix_type->PackedDimension(): 0 ; // Get packed dimension of element
            // Create new element type :
            ret_type = (packed_dim) ? new VeriArrayType(element_type ? element_type->Copy(): 0, packed_dim) : (element_type ? element_type->Copy() : 0) ;
            break ;
        }
    case VERI_METHOD_FIND_FIRST_INDEX :
    case VERI_METHOD_FIND_INDEX :
    case VERI_METHOD_FIND_LAST_INDEX :
    case VERI_METHOD_UNIQUE_INDEX :
        {
            if (prefix_type && !prefix_type->IsUnpackedArrayType()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
            }
            if (arguments && arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type),1) ;

            // VIPER #7907: Argument should be of element type.
            // So do type check with expected type as element type.
            // VIPER #8218: Element type should be obtained by reducing dimension
            // by 1
            VeriTypeInfo *ele_type = 0 ; //(prefix_type) ? prefix_type->ElementType() : 0 ; // Take the element type
            if (prefix_type) {
                Array idx_arr(1) ;
                idx_arr.InsertLast(0) ;
                ele_type = prefix_type->Copy()->ReduceDimBy(&idx_arr, prefix)  ;
            }
            if (arguments && arguments->Size() > 0) {
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;
            }
            delete ele_type ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Return type will be index type for associative array and int type for other arrays
            VeriTypeInfo *index_type = (prefix_type) ? prefix_type->IndexType() : 0 ; // Index type of associative array
            ret_type = new VeriQueue(index_type ? index_type->Copy() : new VeriBuiltInType(VERI_INT)) ;
            break ;
        }
    case VERI_METHOD_FIND : // array.find(item) with (item-expr)
    case VERI_METHOD_FIND_FIRST :
    case VERI_METHOD_FIND_LAST :
    case VERI_METHOD_MAX :
    case VERI_METHOD_MIN :
    case VERI_METHOD_UNIQUE :
        {
            if (prefix_type && !prefix_type->IsUnpackedArrayType()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
            }
            if (arguments && arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type),1) ;

            // VIPER #7907: Argument should be of element type.
            // So do type check with expected type as element type.
            //VeriTypeInfo *ele_type = (prefix_type) ? prefix_type->ElementType() : 0 ; // Get type of element
            // VIPER #8218: Element type should be obtained by reducing dimension
            // by 1
            VeriTypeInfo *ele_type = 0 ; //(prefix_type) ? prefix_type->ElementType() : 0 ; // Take the element type
            if (prefix_type) {
                Array idx_arr(1) ;
                idx_arr.InsertLast(0) ;
                ele_type = prefix_type->Copy()->ReduceDimBy(&idx_arr, prefix)  ;
            }
            if (arguments && arguments->Size() > 0) {
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;
            }

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Return type is queue and each element will be of element type of prefix array
            ret_type = new VeriQueue(ele_type) ;
            break ;
        }
    case VERI_METHOD_INDEX : // array.index(int)
        {
            // Prefix should be unpacked array
            // FIXME : Do not check this for array iterator. We cannot set unpacked type to array iterator
            if ((prefix_id && !prefix_id->IsIterator()) && (prefix_type && !prefix_type->IsUnpackedArrayType())) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
            }
            if (arguments && arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type),1) ;

            // VIPER #7907: Argument should be of integer type.
            // So do type check with expected type as integer type.
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_INT) ;
            if (arguments && arguments->Size() > 0) {
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;
            }
            delete ele_type ;
            ele_type = 0 ;

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            if (arg_type && !arg_type->IsIntegralType()) {
                Error ("illegal argument in %s, require %s", PrintToken(function_type), "int") ;
            }

            // Return type will be index type for associative array and int type for other arrays
            VeriTypeInfo *index_type = (prefix_type) ? prefix_type->IndexType(): 0 ; // Index type of associative array
            ret_type = (index_type) ? index_type->Copy() : new VeriBuiltInType(VERI_INT) ;
            break ;
        }
    case VERI_METHOD_REVERSE : // array.reverse(arg=item) with (item-expr)
        // Viper #5248 (test_84) : Allow built-in data type having size > 1
        //if (prefix_type && !((prefix_type->IsUnpackedArrayType() || prefix_type->IsPackedArrayType()) && (prefix_type->UnpackedDimension() == 1 || prefix_type->PackedDimension())) && !(prefix_type->IsBuiltInType() && prefix_type->BaseTypeSize()> 1)) {
        // VIPER #8354: According to IEEE 1800-2009 LRM section 7.12.2, array
        // ordering methods work on unpacked array. But VCS supports this for
        // packed array too
        // So allowed packed array only in relaxed checking mode
        if (InRelaxedCheckingMode()) {
            if (prefix_type && ((!prefix_type->UnpackedDimension() && !prefix_type->PackedDimension()) &&
                 //((prefix_type->UnpackedDimension()> 1) &&
                 (!(prefix_type->IsBuiltInType() && prefix_type->BaseTypeSize()> 1)))) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"a packed or unpacked array") ;
            }
        } else if (prefix_type && !prefix_type->UnpackedDimension()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
        }
        // According to IEEE 1800-2009 LRM section 7.12.2, array ordering methods
        // are not allowed for associative array
        if (prefix_type && prefix_type->IsAssociativeArrayType() && !InRelaxedCheckingMode()) {
            Error("prefix of array method %s should not be %s", PrintToken(function_type),"an associative array") ;
        }
        if (arguments && arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type),1) ;

        // VIPER #7907: Argument should be of element type.
        // So do type check with expected type as element type.
        if (arguments && arguments->Size() > 0) {
            //VeriTypeInfo *ele_type = prefix_type ? prefix_type->ElementType() : 0 ;
            // VIPER #8218: Element type should be obtained by reducing dimension
            // by 1
            VeriTypeInfo *ele_type = 0 ; //(prefix_type) ? prefix_type->ElementType() : 0 ; // Take the element type
            if (prefix_type) {
                Array idx_arr(1) ;
                idx_arr.InsertLast(0) ;
                ele_type = prefix_type->Copy()->ReduceDimBy(&idx_arr, prefix)  ;
            }
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
        }

        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }
        // These methods return void type
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_RSORT :
    case VERI_METHOD_SORT :
    case VERI_METHOD_SHUFFLE :
        // VIPER #8354: According to IEEE 1800-2009 LRM section 7.12.2, array
        // ordering methods work on unpacked array.
        if (prefix_type && !prefix_type->UnpackedDimension()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
        }
        // According to IEEE 1800-2009 LRM section 7.12.2, array ordering methods
        // are not allowed for associative array
        if (prefix_type && prefix_type->IsAssociativeArrayType() && !InRelaxedCheckingMode()) {
            Error("prefix of array method %s should not be %s", PrintToken(function_type),"an associative array") ;
        }
        if (arguments && arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type),1) ;

        // VIPER #7907: Argument should be of element type.
        // So do type check with expected type as element type.
        if (arguments && arguments->Size() > 0) {
            //VeriTypeInfo *ele_type = prefix_type ? prefix_type->ElementType() : 0 ;
            // VIPER #8218: Element type should be obtained by reducing dimension
            // by 1
            VeriTypeInfo *ele_type = 0 ; //(prefix_type) ? prefix_type->ElementType() : 0 ; // Take the element type
            if (prefix_type) {
                Array idx_arr(1) ;
                idx_arr.InsertLast(0) ;
                ele_type = prefix_type->Copy()->ReduceDimBy(&idx_arr, prefix)  ;
            }
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
        }

        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }
        // These methods return void type
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_AND :  // array.and(prefix=item) with item
    case VERI_METHOD_OR :   // array.or
    case VERI_METHOD_XOR :  // array.xor
    case VERI_METHOD_SUM :
    case VERI_METHOD_PRODUCT :
        {
            // Prefix should be unpacked array
            if (prefix_type && !prefix_type->IsUnpackedArrayType()) {
                Error("prefix of method %s should be %s",PrintToken(function_type),"an unpacked array") ;
            }
            if (arguments && arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type),1) ;

            // VIPER #7907: Do type check.
            if (arguments && arguments->Size() > 0) {
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(0, READ) ;
            }

            if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
                delete arg_type ;
                arg_type = 0 ;
            }
            // Return type is array element type
            // Viper 6893: Create the element type by reducing dimension of Prefix type by one.
            if (prefix_type) {
                Array idx_arr(1) ;
                VeriIntVal *idx = new VeriIntVal(0) ;
                idx_arr.InsertLast(idx) ;
                ret_type = prefix_type->Copy()->ReduceDimBy(&idx_arr, prefix)  ;
                delete idx ;
            }
            break ;
        }
    //case VERI_METHOD_GET_COVERAGE :
    case VERI_SYS_CALL_GET_COVERAGE :
    case VERI_METHOD_GET_INST_COVERAGE :
        // Argument count can be 0 or 2
        if (arguments && arguments->Size() != 2 && arguments->Size() != 0) Error("%s expects %d arguments",PrintToken(function_type),2) ;

        // VIPER #7907: Do type checking:
        if (arguments && arguments->Size() > 0) {
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(0, READ) ;
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }
        delete arg_type ;
        arg_type = 0 ;

        // VIPER #7907: Do type check.
        if (arguments && arguments->Size() > 1) {
            arg = (VeriExpression *)arguments->At(1) ;
            arg_type = arg->CheckType(0, READ) ;
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }

        // Return type real
        ret_type = new VeriBuiltInType(VERI_REAL) ;
        break ;
    case VERI_METHOD_SAMPLE :
    //case VERI_METHOD_STOP :
    case VERI_SYS_CALL_STOP :
    case VERI_METHOD_START :
    {
        // VIPER #5094: Do not produce the error if prefix is not cover-group/point/cross.
        // The built-in methods sample(), stop() and start() are only valid on some of them:
        unsigned valid_call = 0 ;
        VeriModuleItem *prefix_item = (prefix_id) ? prefix_id->GetModuleItem() : 0 ;
        if (prefix_id && (prefix_id->IsCovergroup() || (prefix_item &&
            ((prefix_item->GetType() == VERI_COVERPOINT) ||
             (prefix_item->GetType() == VERI_CROSS))))) valid_call = 1 ;
        if (valid_call && arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Return type void
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    }
    case VERI_METHOD_SET_INST_NAME :
        if (!arguments || arguments->Size() > 1) Error("%s expects %d arguments",PrintToken(function_type), 1) ;

        // VIPER #7907: Do type checking.
        if (arguments && arguments->Size() > 0) {
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(0, READ) ;
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }
        // Return type void
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_GET_RANDSTATE : // Check on class obj
        // VIPER #4258 : The check for expr is added to ensure that "$get_randstate()" doesn't error out

        if (arguments && arguments->Size() > 0) Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        // Return type string
        ret_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
        break ;
    case VERI_METHOD_SET_RANDSTATE :
        if (!arguments || arguments->Size() != 1) { // Expect one argument (an expression)
            Error("system call %s expects one argument", PrintToken(function_type)) ;
        }

        // VIPER #7907: Do type checking.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
            ele_type = 0 ;
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }

        // VIPER #5887: Only string, string literal, packed array/structure/union is allowed as argument:
        // Warn if the argument is parameter with dollar as initial value
        if (arg_type && !arg_type->IsString() && !arg_type->IsStringLiteral() && !arg_type->IsPacked()) {
            Warning("invalid expression in system call %s", PrintToken(function_type)) ;
        } else {
            VeriIdDef *id = arg ? arg->GetId() : 0 ;
            if (id && id->IsParam()) { // warn if the argument is parameter with dollar as initial value
                VeriExpression *initial_val = id->GetInitialValue() ;
                if (initial_val && initial_val->IsDollar()) {
                    Warning("illegal use of parameter %s with unbounded value", id->Name()) ;
                }
            }
        }
        // Return type void
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_NEW : // new (on super is allowed only)
        if (prefix_type && !prefix_type->IsClassType()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a class") ;
        }
        // If prefix type exists, new will create object of that type
        ret_type = prefix_type ? prefix_type->Copy() : 0 ;
        break ;
    case VERI_METHOD_RANDOMIZE : // function int randomize()
        if (prefix_type && !prefix_type->IsClassType()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a class") ;
        }
        // Return type is 'int'
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_METHOD_PRE_RANDOMIZE :  // function void pre_randomize()
    case VERI_METHOD_POST_RANDOMIZE : // function void post_randomize()
        if (prefix_type && !prefix_type->IsClassType()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a class") ;
        }
        if (arguments && arguments->Size() > 0) {
            Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        }
        // Return type is 'void'
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_CONSTRAINT_MODE : // can be on class object/constraint block
        // FIXME : Cannot check whether prefix is constraint block id. We create
        // variable for that identifier.
        //if (!((prefix_id && prefix_id->IsConstraintBlock()) || (prefix_type && prefix_type->IsClass()))) {
            //Error("prefix of method %s should be %s",PrintToken(function_type),"class/constraint_block") ;
        //}
         // IEEE 1800-2012 (8.26.9) : Call to rand_mode/constraint_mode on interface class handle is illegal
        if (prefix_type && prefix_type->IsClassType()) {
             VeriIdDef *prefix_type_id = prefix_type->GetNameId() ;
             VeriModuleItem *type_body = (prefix_type_id) ? prefix_type_id->GetModuleItem(): 0 ;
             if (type_body && type_body->GetQualifier(VERI_INTERFACE)) {
                 Error("call to rand_mode/constraint_mode on interface class handle is illegal") ;
             }
        }
        // Number of arguments should be 0/1
        // VIPER #4833 : If it is function call, number of arguments should be 0,
        // for task call it will be 1
        if (is_task && (arguments && arguments->Size() != 1)) {
            Error("%s expects %d arguments", PrintToken(function_type), 1) ;
        } else if (!is_task && arguments && arguments->Size()) {
            Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        }

        // VIPER #7907: Do type checking.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_BIT) ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
            delete ele_type ;
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }

        // Return type is 'int', if it is function
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_METHOD_RAND_MODE : // can be called on class object/random variable
         // IEEE 1800-2012 (8.26.9) : Call to rand_mode/constraint_mode on interface class handle is illegal
        if (prefix_type && prefix_type->IsClassType()) {
             VeriIdDef *prefix_type_id = prefix_type->GetNameId() ;
             VeriModuleItem *type_body = (prefix_type_id) ? prefix_type_id->GetModuleItem(): 0 ;
             if (type_body && type_body->GetQualifier(VERI_INTERFACE)) {
                 Error("call to rand_mode/constraint_mode on interface class handle is illegal") ;
             }
        }
        // Number of arguments should be 0/1
        // VIPER #4833 : If it is function call, number of arguments should be 0,
        // for task call it will be 1
        if (is_task && arguments) {
            if ((arguments->Size() != 1)) {
                Error("%s expects %d arguments", PrintToken(function_type), 1) ;
            }

            // VIPER #7907: Argument should be of type bit.
            // So do type check with expected type as bit type.
            if (arguments->Size() > 0) {
                VeriTypeInfo *ele_type = new VeriBuiltInType(VERI_BIT) ;
                arg = (VeriExpression *)arguments->At(0) ;
                arg_type = arg->CheckType(ele_type, READ) ;
                delete ele_type ;
                ele_type = 0 ;
            }
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }
        if (!is_task && arguments && arguments->Size() > 0) {
            Error("%s expects %d arguments", PrintToken(function_type), 0) ;
        }
        // Return type is 'int' if it is a function
        ret_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_SYS_CALL_SRANDOM :
        if (!arguments || arguments->Size() != 1) {
            Error("%s expects %d arguments", PrintToken(function_type), 1) ;
        }

        // VIPER #7907: Argument should be of element type.
        // So do type check with expected type as element type.
        if (arguments && arguments->Size() > 0) {
            VeriTypeInfo *ele_type = prefix_type ? prefix_type->ElementType() : 0 ;
            arg = (VeriExpression *)arguments->At(0) ;
            arg_type = arg->CheckType(ele_type, READ) ;
        }
        if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
            delete arg_type ;
            arg_type = 0 ;
        }
        // VIPER #5887: Only integral type argument is allowed and warn if other than integral type
        // argument is used. Also warn if the argument is parameter with dollar as initial value.
        if (arg_type && !arg_type->IsIntegralType() && !arg_type->IsString() && !arg_type->IsStringLiteral()) {
            Warning("invalid expression in system call %s", PrintToken(function_type)) ;
        } else {
            VeriIdDef *id = arg ? arg->GetId() : 0 ;
            if (id && id->IsParam()) { // warn if the argument is parameter with dollar as initial value
                VeriExpression *initial_val = id->GetInitialValue() ;
                if (initial_val && initial_val->IsDollar()) {
                    Warning("illegal use of parameter %s with unbounded value", id->Name()) ;
                }
            }
        }
        // Return type is 'void'
        ret_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_ENDED :
    case VERI_METHOD_MATCHED :
        // Check number of arguments (0) :
        if (arguments && arguments->Size() && arguments->GetFirst()) Error("%s expects %d arguments",PrintToken(function_type),0) ;
        // Check that prefix is a sequence object
        if (prefix_id && !prefix_id->IsSequence()) {
            Error("prefix of method %s should be %s",PrintToken(function_type),"a sequence") ;
        }
        break ;
    case VERI_METHOD_TRIGGERED :
    default :
        break ; // do not do any more tests..
    }
    // VIPER #5139 : Produce error if built-in function is used in lhs of assignment
    if (prefix_type && ret_type && ((environment == WRITE) || (environment == INST_WRITE))) {
        Error("function call %s is not allowed here", PrintToken(function_type)) ;
    }
    delete arg_type ;
    delete prefix_type ; // delete prefix type
    return ret_type ;
}
VeriTypeInfo *VeriSelectedName::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // Call CheckType on prefix, when both _suffix_id and _suffix are null. It is
    // elaboration created name
    if (!_suffix_id && !_suffix && _prefix) return _prefix->CheckType(expected_type, environment) ;

    // VIPER #5935 : If prefix of
    //if (_prefix_id && _prefix_id->IsClocking()) return 0 ;

    // Get the full-type information from resolved identifier
    VeriTypeInfo *this_type = _suffix_id ? _suffix_id->CreateType(_prefix_id): 0 ;

    // Check the usage of this identifier
    if (_suffix_id) VeriTypeInfo::CheckIdentifierUsage(_suffix_id, this_type, environment, /*from*/this) ;

    if (!this_type) { // This can be built-in method call
        // Type information will be created depending on function type
        this_type = CreateTypeForMethods(0, ((environment != WRITE) && (environment != INST_WRITE)) ? 0:1, environment) ;
        // Method with return type void cannot be used as expression
        // FIXME : Following message can be produced for some wrong selected name
        // as this routine will be called in analyze time, where cross referenced
        // hierarchical ids are not resolved properly.
        if ((environment != NO_ENV) && this_type && (this_type->Type() == VERI_VOID)) {
            Warning("invalid use of void function %s", _suffix) ;
        }
        // VIPER #5139 : Produce error if method call is in lhs
        //if (this_type && (environment == WRITE)) {
            //Error("function call %s is not allowed here", PrintToken(_function_type)) ;
        //}
        // VIPER #4077 : If identifier is not resolved, created unresolved name type
        if (!this_type && !_suffix_id) this_type = new VeriNamedType(this) ;
    }
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriIndexedExpr::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *prefix_type = (_prefix) ? _prefix->CheckType(0, environment) : 0 ;
    if (!prefix_type) return 0 ;

    // Check the validity of indexing/slicing :
    Array idx_arr(1) ;
    idx_arr.InsertLast(_idx) ;
    VeriTypeInfo *type = prefix_type->ReduceDimBy(&idx_arr, 0) ;
    if (!expected_type) return type ;

    // Now type of this expression is already calculated, check it with expected type
    if (type && type->CheckAgainst(expected_type, this, environment)) return type ;
    return type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriScopeName::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    if (!_param_values && !_modport_name)  return VeriSelectedName::CheckType(expected_type, environment) ;
    // If no parameter value assignment list create unresolved name type
    return new VeriNamedType(this) ;
}
VeriTypeInfo *VeriIndexedId::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // VIPER #4077 : If identifier is not resolved, created unresolved name type
    if (!_id) return new VeriNamedType(this) ;
    if (_id->IsType()) return 0 ; // Not resolved, cannot extract type

    VeriTypeInfo *prefix_type = _id->CreateType(0) ;
    // Check the usage of this identifier
    VeriTypeInfo::CheckIdentifierUsage(_id, prefix_type, environment, /*from*/this) ;

    if (!prefix_type || !_idx) return prefix_type ; // No index, same as prefix
    // Do not create type if prefix_type is from type parameter/unresolved name
    if (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType()) {
        return prefix_type ;
    }
    // Check the validity of indexing/slicing :
    VeriTypeInfo *type = 0 ;
    Array idx_arr(1) ;
    idx_arr.InsertLast(_idx) ;
    type = prefix_type->ReduceDimBy(&idx_arr, this) ;
    if (!expected_type) return type ;

    // Now type of this expression is already calculated, check it with expected type
    if (type && type->CheckAgainst(expected_type, this, environment)) return type ;
    return type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriIndexedMemoryId::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // VIPER #4077 : If identifier is not resolved, created unresolved name type
    if (!_id) return new VeriNamedType(this) ;
    if (!_indexes || _id->IsType()) return 0 ; // Not resolved, can't do anything

    VeriTypeInfo *prefix_type = _id->CreateType(0) ;
    // Check the usage of this identifier
    VeriTypeInfo::CheckIdentifierUsage(_id, prefix_type, environment, /*from*/this) ;

    if (!prefix_type) return 0 ;
    // Do not create type if prefix_type is from type parameter/unresolved name
    if (prefix_type->IsTypeParameterType() || prefix_type->IsUnresolvedNameType()) {
        return prefix_type ;
    }

    // Part select on a packed /unpacked dimension does not reduce its dimension
    // count, but bit selection reduces dimension count.
    Array idx_arr(_indexes->Size()) ;
    unsigned i ;
    VeriExpression *idx ;
    // Traverse index array backwards and add those in new array, so that we can use
    // 'RemoveLast' for reducing dimension
    FOREACH_ARRAY_ITEM_BACK(_indexes, i, idx) idx_arr.InsertLast(idx) ;

    prefix_type = prefix_type->ReduceDimBy(&idx_arr, this) ;
    if (!expected_type) return prefix_type ;

    // Now type of this expression is already calculated, check it with expected type
    if (prefix_type && prefix_type->CheckAgainst(expected_type, this, environment)) return prefix_type ;
    return prefix_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriConcat::CheckConcatType(const Array *exprs, VeriTypeInfo *expected_type, veri_type_env environment)
{
    unsigned i ;
    VeriExpression *expr ;
    VeriTypeInfo *ele_type = 0 ;
    if (expected_type && expected_type->IsQueue()) {
        // When target is queue, value can be concatenation having reference to
        // target queue and normal expression of type same as element of queue
        FOREACH_ARRAY_ITEM(exprs, i, expr) {
            if (!expr) continue ;
            ele_type = expr->CheckType(0, environment) ;
            if (!ele_type) continue ;
            VeriTypeInfo *expected_ele_type = expected_type->ElementType() ;
            if (!ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() && !ele_type->IsEquivalent(expected_type, 0, 0, 0) && (expected_ele_type && !ele_type->IsAssignmentCompatible(expected_ele_type))) {
                if (environment != CASTING) expr->Warning("expression should be of type queue or element of queue") ;
                delete ele_type ;
                return 0 ;
            }
            delete ele_type ;
        }
        return expected_type->Copy() ; // Return queue type too
    }
    // Concatenation is a packed vector of bits. So it is always packed.
    unsigned is_real_allowed = 0 ; // Real is not allowed as concatenation element
    // If expected type is unpacked array of real type parameter and we are processing
    // ams designs, real is allowed as concatenation element. Check that
    if (VeriNode::IsAms()) { // ams dialect
        if (expected_type && expected_type->IsUnpackedArrayType()) { // Unpacked array
            VeriTypeInfo *element_type = expected_type->ElementType() ;
            if (element_type && element_type->IsRealType()) is_real_allowed = 1 ; // element is real
        }
    }
    unsigned valid_ele = 1 ;
    unsigned is_any_ele_string = 0 ;
    unsigned is_all_str_literals = 1 ;
    unsigned is_any_four_state_ele = 0 ;
    unsigned type = VERI_REG ;
    // VIPER #5949 set nettype to VERI_REG
    // use nettype to typeinfo of the nase type of complex_type data_type
    unsigned nettype = VERI_REG ;
    // Type infer each element
    FOREACH_ARRAY_ITEM(exprs, i, expr) {
        if (!expr) continue ;
        ele_type = expr->CheckType(0, environment) ;
        // VIPER #3779, 3820 : If element type cannot be determined, do not return type for this concat
        if (!ele_type || ele_type->IsTypeParameterType() || ele_type->IsUnresolvedNameType()) valid_ele = 0 ;
        if (ele_type && ele_type->IsString()) is_any_ele_string = 1 ;
        // VIPER #8131 : Check whether any element is 4 state
        if (ele_type && ele_type->Is4State()) is_any_four_state_ele = 1 ;
        if (ele_type && !ele_type->IsStringLiteral()) is_all_str_literals = 0 ;
        if (environment == VALUE_RANGE_LIST) {
            // VIPER #6894: 'ele_type' can be singular type or unpacked array of singular type
            if (ele_type && valid_ele) {
                if (ele_type->IsUnpackedArrayType()) {
                    VeriTypeInfo *base_type = ele_type->BaseType() ;

                    if (base_type && !base_type->IsSingularType()) {
                        expr->Error("bad operand to set membership operator") ;
                    }
                } else if (!ele_type->IsSingularType()) {
                    expr->Error("bad operand to set membership operator") ;
                }
            }
            delete ele_type ;
            continue ;
        } // Do not check elements if it is value range list
        if (ele_type && ele_type->IsRealType() && !is_real_allowed) {
            // VIPER #2909 : Issue error when real expression is used in concat
            expr->Error("illegal concatenation of real expression") ;
            valid_ele = 0 ;
        }
        // VIPER #2290 : Warn on concatenation with unsized constant :
        // VIPER #6045: Unsized bit ('1 etc) are 1 bit in self context so we should not
        // produce the warning in that case. Ignore that by checking !IsUnsizedBit():
        if (expr->IsUnsizedConst() && !expr->IsUnsizedBit() && !is_real_allowed) {
            expr->Warning("concatenation with unsized literal; will interpret as 32 bits") ;
            // If self determined type of concat is required, do not return type
            // for concat having unsized literal
            if (!expected_type) valid_ele = 0 ;
        }
        // VIPER #2619 : invalid target for field:value initialization
        if (expr->IsConcatItem()) {
            expr->Error("invalid target for field:value initialization") ;
            valid_ele = 0 ;
        }
        if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() && ((ele_type->IsString() && ((environment == WRITE) || (environment == INST_WRITE))) ||
                (ele_type->IsUnpacked() && !ele_type->IsBuiltInType()))) {
            expr->Error("illegal concatenation of unpacked value") ;
            valid_ele = 0 ;
        }
        // VIPER #5949: Find the nettype of the ele_type
        // if (ele_type && ele_type->IsNetType()) type = ele_type->Type() ;
        if (ele_type && ele_type->IsNetType()) {
            type = ele_type->Type() ;
            nettype = ele_type->GetNetType() ;
            if (!nettype) {
                VeriTypeInfo *base_type = ele_type->BaseType() ;
                nettype = base_type->GetNetType() ;
            }
        }
        delete ele_type ;
    }
    if (!valid_ele) return 0 ;
    // If any element of concatenation is of type string or all elements are string
    // literals, concatenation is treated as string literal, otherwise it is packed vector
    VeriTypeInfo *ret_type = 0 ;
    if (is_any_ele_string) { // If any element is string, type is string (VIPER #4133)
        ret_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
    } else if (is_all_str_literals) {
    //if (is_any_ele_string || is_all_str_literals) {
        ret_type = new VeriArrayType(new VeriBuiltInType(VERI_REG), 1) ;
        ret_type->SetAsStringLiteral() ;
    } else if (is_real_allowed) { // Concatenation of real : it is for ams
        ret_type = (expected_type) ? expected_type->Copy() : 0 ;
    } else {
        // VIPER #5949: Set nettype to the base type:
        // ret_type = new VeriArrayType(new VeriBuiltInType(type), 1) ;
        // VIPER #8131 : If no element of cancat is 4-state type, create
        // array of 2-state bit as concat type
        if (!is_any_four_state_ele && (type == VERI_REG)) type = VERI_BIT ;
        VeriTypeInfo *base_type = new VeriBuiltInType(type) ;
        base_type->SetNetType(nettype) ;
        ret_type = new VeriArrayType(base_type, 1) ;
    }
    return ret_type ;
}
VeriTypeInfo *VeriConcat::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = 0 ;
    if (InRelaxedCheckingMode() && CanBeAssignmentPattern(expected_type)) {
        // VIPER #5248 (test_10/test_43): Treat this multi-concat as assignment pattern:
        if (expected_type && VeriAssignmentPattern::CheckAssignmentPatternType(_exprs, expected_type,
                                           environment, this)) {
            //return expected_type->Copy() ;
            this_type = expected_type->Copy() ;
        }
        //return 0 ;
    // VIPER #6008 : Support for unpacked array concatenation LRM SV-2009 10.10
    // Target can be fixed unpacked array, dynamic array or queue :
    } else if (expected_type && expected_type->IsUnpackedArrayType() &&
           (expected_type->IsFixedArrayType() || expected_type->IsDynamicArrayType() || expected_type->IsQueue())) {
        this_type = CheckUnpackedArrayConcat(expected_type, environment) ;
    } else {
        this_type = CheckConcatType(_exprs, expected_type, environment) ;
    }

    // Now type of this expression is already calculated, check it with expected type
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriConcat::CheckUnpackedArrayConcat(VeriTypeInfo *expected_type, veri_type_env environment) const
{
    if (!expected_type) return 0 ; // Cannot check without context info

    // VIPER #6008 : Unpacked array concatenation cannot contain replication,
    // defaulting and explicit typing
    // Every element shall represent one or many elements of the resulting array
    // value, interpreted as followings:
    // -- an item whose self-determined type is assignment-compatible with the
    //    element type of the target array shall represent a single element
    // -- an item whose self determined type is an array whose slowest varying
    //    dimension's element type is assignment compatible with the element type
    //    of target array shall represent as many elements as exist in that item
    // -- an item of any other type, or an item that has no self-determined type,
    //    shall be illegal except that the literal value null shall be legal if
    //    the target array's elements are of class type.
    VeriExpression *item ;
    VeriTypeInfo *item_type ;
    VeriTypeInfo *expected_ele_type = expected_type->ElementType() ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_exprs, i, item) {
        if (!item) continue ;
        if (item->IsConcatItem()) {
            item->Error("invalid target for field:value initialization") ;
            continue ;
        }
        if (item->IsMultiConcat()) {
            item->Error("replication is not allowed in unpacked array concatenation") ;
            continue ;
        }
        if ((item->IsAssignPattern() || item->IsMultiAssignPattern()) && !item->GetTargetType()) {
            item->Error("wrong element type in unpacked array concatenation") ;
            continue ;
        }
        item_type = item->CheckType(0 /* self determined type*/, environment) ;
        if (item_type && (item_type->IsUnresolvedNameType() || item_type->IsTypeParameterType())) {
            delete item_type ;
            continue ;
        }
        // Item is null and element type is class type
        if (!item_type && item->IsNull() && expected_ele_type && (expected_ele_type->IsClassType() || expected_ele_type->IsUnresolvedNameType() || expected_ele_type->IsTypeParameterType())) continue ;

        if (!item_type) { // Should have self-determined type
            item->Error("wrong element type in unpacked array concatenation") ;
            continue ;
        }
        VeriTypeInfo *item_ele_type = item_type->ElementType() ;
        if (!item_type->IsAssignmentCompatible(expected_ele_type) // item assignment-compatible with the target's element type
             && !(item_type->IsEquivalent(expected_type, 0, 0, 0) && item_ele_type && item_ele_type->IsAssignmentCompatible(expected_ele_type))) {
            item->Error("wrong element type in unpacked array concatenation") ;
        }
        delete item_type ;
    }
    return expected_type->Copy() ;
}

VeriTypeInfo *VeriMultiConcat::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // Check repeat expression
    VeriTypeInfo *rtype = (_repeat) ? _repeat->CheckType(0, environment) : 0 ;
    delete rtype ; // It is not needed any more

    VeriTypeInfo *this_type = 0 ;
    if ((environment != CASTING) && InRelaxedCheckingMode() && CanBeAssignmentPattern(expected_type)) {
        // VIPER #5248 (test_10/test_43): Treat this multi-concat as assignment pattern:
        if (expected_type && VeriMultiAssignmentPattern::CheckMultiAssignmentPatternType(_exprs, expected_type,
                                           environment, this)) {
            //return expected_type->Copy() ;
            this_type = expected_type->Copy() ;
        }
        //return 0 ;
    // VIPER #6008 : Support for unpacked array concatenation LRM SV-2009 10.10
    // Target can be fixed unpacked array, dynamic array or queue :
    } else if ((environment != CASTING) && expected_type && expected_type->IsUnpackedArrayType() &&
           (expected_type->IsFixedArrayType() || expected_type->IsDynamicArrayType() || expected_type->IsQueue())) {
        Error("replication is not allowed in unpacked array concatenation") ;
    } else {
        this_type = VeriConcat::CheckConcatType(_exprs, expected_type, environment) ;
    }

    // FIXME : If any element is of type string, replicator operator can be non-constant
    // If any element is of type string, ret_type is of type string
    // Now type of this expression is already calculated, check it with expected type
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriFunctionCall::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    if (!_func_name) return 0 ; // Analyze error
    // Type of function call is type of its return type

    VeriIdDef *function_id = GetId() ;
    if (!function_id && _func_name->GetFunctionType()) {
        return _func_name->CreateTypeForMethods(_args, 0, environment) ;
    }
    VeriTypeInfo *type = _func_name->CheckType(expected_type, environment) ;
    if (!function_id) return type ;

    if (function_id->GetFunctionType() == VERI_METHOD_RANDOMIZE) {
        // VIPER #4743 : It is calling of function 'randomize' which is defined in std package.
        // This function can have any number of inputs. So do not check port
        // connection for this function.
        return type ; // Return return type of function
    }
    // VIPER #3260 : Type checking for formal/actual association :
    Array *formals = function_id->GetPorts() ; // Pick up the formals :
    VeriScope *formal_scope = function_id->LocalScope() ;

    // traverse the actuals
    unsigned i ;
    VeriExpression *actual ;
    VeriIdDef *formal ;
    FOREACH_ARRAY_ITEM(_args, i, actual) {
        if (!actual) continue ;

        // Find the formal
        formal = 0 ;
        if (actual->NamedFormal()) { // This is a (SV-style) named formal.
            formal = (formal_scope) ? formal_scope->FindLocal(actual->NamedFormal()) : 0 ;
        } else { // This is a ordered formal
            if (i<((formals) ? formals->Size() : 0)) {
                formal = (formals) ? (VeriIdDef*)formals->At(i) : 0 ;
            }
        }

        if (!formal) continue ; // the formal cannot be found
        // VIPER #2046 :Check the type of actual against formal
        (void) actual->CheckAssociation(formal, (function_id->IsProperty() || function_id->IsSequence()) ? PROP_SEQ : FUNC) ;
    }
    return type ;
}

// VIPER #5887: Check for the type of the argument of some specific system functions and warn if they
// are not of integral type. Also warn if argument is parameter with dollar as initial value.
void VeriSystemFunctionCall::CheckIntegralArgument(VeriExpression *expr, veri_type_env environment)
{
   if (!expr) return ;
   VeriTypeInfo *arg_type = expr->CheckType(0, environment) ;
   if (arg_type && (arg_type->IsUnresolvedNameType() || arg_type->IsTypeParameterType())) {
       delete arg_type ;
       return ;
   }

   if (arg_type && !arg_type->IsIntegralType() && !arg_type->IsString() && !arg_type->IsStringLiteral()) {
       Warning("invalid expression in system call %s", _name) ;
   } else { // warn if the argument is parameter with dollar as initial value
       VeriIdDef *id = expr->GetId() ;
       if (id && id->IsParam()) {
           VeriExpression *initial_val = id->GetInitialValue() ;
           if (initial_val && initial_val->IsDollar()) {
               Warning("illegal use of parameter %s with unbounded value", id->Name()) ;
           }
       }
   }
   delete arg_type ;
}

unsigned VeriSystemFunctionCall::IsGlobalClockingFunction() const
{
    switch (_function_type) {
        case VERI_SYS_CALL_PAST_GCLK :
        case VERI_SYS_CALL_FELL_GCLK :
        case VERI_SYS_CALL_CHANGED_GCLK :
        case VERI_SYS_CALL_RISING_GCLK :
        case VERI_SYS_CALL_STEADY_GCLK :
        case VERI_SYS_CALL_ROSE_GCLK :
        case VERI_SYS_CALL_STABLE_GCLK :
        case VERI_SYS_CALL_FUTURE_GCLK :
        case VERI_SYS_CALL_FALLING_GCLK :
        case VERI_SYS_CALL_CHANGING_GCLK :
            return 1 ;
        default :
            return 0 ;
     }
}

VeriTypeInfo *VeriSystemFunctionCall::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = 0 ;

    // Pick up the first argument :
    VeriExpression *expr = (_args && _args->Size()) ? (VeriExpression*)_args->At(0) : 0 ;
    VeriTypeInfo *arg_type = 0 ;
    unsigned i ;

    // Intercept system functions which we know something about
    switch (_function_type) {
    case VERI_SYS_CALL_SIGNED :
    case VERI_SYS_CALL_UNSIGNED :
        // Type-conversions to signed/unsigned
        // Type is same as type of argument
        this_type = expr ? expr->CheckType(0, environment): 0 ;
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        break ;
    case VERI_SYS_CALL_REALTOBITS :
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        // Argument should be of type real
        if (arg_type && !arg_type->IsRealType() && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType()) {
            if (_name) Error("illegal argument in %s, require %s", _name, "real type") ;
        }
        delete arg_type ;
        // Return type is 64 bit vector
        this_type = new VeriArrayType(new VeriBuiltInType(VERI_REG), 1) ;
        break ;
    case VERI_SYS_CALL_BITSTOREAL :
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        delete arg_type ;
        // Return type is real
        this_type = new VeriBuiltInType(VERI_REAL) ;
        break ;
    case VERI_SYS_CALL_RANDOM :
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        if (_args && _args->Size() > 1) { // Expect one optional argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        //VIPER #5887: warn if the argument is parameter with dollar as initial value
        if (expr) {
            VeriIdDef *id = expr->GetId() ;
            if (id && id->IsParam()) { //  warn if the argument is parameter with dollar as initial value
                VeriExpression *initial_val = id->GetInitialValue() ;
                if (initial_val && initial_val->IsDollar()) {
                    Warning("illegal use of parameter %s with unbounded value", id->Name() ) ;
                }
            }
        }
        // FIXME : Is unpacked is allowed as argument ? Standard simulators does not give error
        delete arg_type ;
        // Return type int
        this_type = new VeriBuiltInType(VERI_INTEGER) ;
        break ;
    case VERI_SYS_CALL_TYPENAME :
        // Check argument
        arg_type = expr ? expr->CheckType(0, NO_ENV): 0 ; // Check with accepting environment
        delete arg_type ;
        // Returns string that represent the resolved type of its argument
        //this_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
        // VIPER #4118 : Make the return type of typename string literal, so that
        // it can be assigned to other packed object :
        this_type = new VeriArrayType(new VeriBuiltInType(VERI_REG), 1) ;
        this_type->SetAsStringLiteral() ; // Mark this is as string literal
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        break ;
    case VERI_SYS_CALL_ONEHOT :
    case VERI_SYS_CALL_ONEHOT0 :
    case VERI_SYS_CALL_ISUNKNOWN :
        arg_type = expr ? expr->CheckType(0, environment): 0 ; // Check with accepting environment
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        delete arg_type ;
        // Returns boolean
        this_type = new VeriBuiltInType(VERI_BIT) ;
        break ;
    case VERI_SYS_CALL_COUNTONES : // Counts number of ones
        arg_type = expr ? expr->CheckType(0, environment): 0 ; // Check with accepting environment
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        delete arg_type ;
        this_type = new VeriBuiltInType(VERI_INT) ; // Return type int ? (FIXME)
        break ;
    case VERI_SYS_CALL_BITS :
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Returns number of bits require ti hold the expression
        this_type = new VeriBuiltInType(VERI_INT) ;
        // It cannot be used on dynamically sized expression when that is empty
        // FIXME: check whether dynamically sized expression is empty before issuing error
        arg_type = (expr) ? expr->CheckType(0, NO_ENV) : 0 ;
        // VIPER #4469 : Produce error if dynamic type expression is used as argument of $bits
        if (arg_type && arg_type->IsDynamicType() && !arg_type->IsClassType() && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType()) {
            Warning("expression with variable or undeterminable width as argument to $bits") ;
        }
        delete arg_type ;
        break ;
    case VERI_SYS_CALL_ISUNBOUNDED :
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        delete arg_type ;
        // Return value is true/false :
        this_type = new VeriBuiltInType(VERI_BIT) ;
        break ;
    case VERI_SYS_CALL_BITSTOSHORTREAL :
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        delete arg_type ;
        // Return shortreal :
        this_type = new VeriBuiltInType(VERI_SHORTREAL) ;
        break ;
    case VERI_SYS_CALL_SHORTREALTOBITS :
        if (!_args || _args->Size()!=1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        // Argument should be of type real
        if (arg_type && !arg_type->IsRealType() && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType()) {
            if (_name) Error("illegal argument in %s, require %s", _name, "real type") ;
        }
        delete arg_type ;
        // Return type is 32-bit vector:
        this_type = new VeriArrayType(new VeriBuiltInType(VERI_BIT), 1) ;
        break ;
    case VERI_SYS_CALL_DIMENSIONS :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS : // System Verilog Std 1800 addition
        // VIPER #4072 : Check argument with accepting type, can be expression or data type
        arg_type = expr ? expr->CheckType(0, NO_ENV): 0 ;
        // Argument should be array data type or object, or integral data type or object IEEE P1800 Section 22.6.
        // Viper 5852: Section 20.7(IEEE 2009) says for string types $dimension should return 1
        if (arg_type && !arg_type->IsPackedArrayType() && !arg_type->IsUnpackedArrayType() && !arg_type->IsIntegralType() && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType() && !(_function_type == VERI_SYS_CALL_DIMENSIONS && arg_type->IsString())) {
            // Viper 5833. Made the error to Warning. $dimension and $unpacked_dimension can
            // take any type but it should then evaluate to zero. This is in accordance to
            // P1800 LRM 22.6. However since VCS errors out decided to keep the check at its place.
            // To make VCS compliant change the error to Warning
            if (_name) Warning("illegal argument in %s, require %s", _name, "array/integral type") ;
        }
        if (!_args || _args->Size()!=1) { // // Expect one argument
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        delete arg_type ;
        // Return type is integer
        this_type = new VeriBuiltInType(VERI_INTEGER) ;
        break ;
    case VERI_SYS_CALL_LEFT :
    case VERI_SYS_CALL_RIGHT :
    case VERI_SYS_CALL_LOW :
    case VERI_SYS_CALL_HIGH :
    case VERI_SYS_CALL_INCREMENT :
    case VERI_SYS_CALL_LENGTH : // 3.1 only
    case VERI_SYS_CALL_SIZE : // 3.1a
        // VIPER #4072 : Check argument with accepting type, can be expression or data type
        arg_type = expr ? expr->CheckType(0, NO_ENV): 0 ;
        // Argument should be array data type or object, or integral data type or object
        // IEEE P1800 Section 22.6
        if (arg_type && !arg_type->IsPackedArrayType() && !arg_type->IsUnpackedArrayType() && !arg_type->IsIntegralType() && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType()) {
            if (_name) Error("illegal argument in %s, require %s", _name, "array/integral type") ;
        }
        delete arg_type ;
        if (!_args || _args->Size()>2) { // Expect one (or two) arguments
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Second argument (if there) contains a (constant?) expression :
        expr = (_args && _args->Size()>1) ? (VeriExpression*)_args->At(1) : 0 ;
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        delete arg_type ;
        // Return type is integer
        this_type = new VeriBuiltInType(VERI_INTEGER) ;
        break ;
    case VERI_SYS_CALL_URANDOM :
        if (_args && _args->Size() > 1) { // Expect one optional argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // VIPER #5887: Check for the type of the argument of some specific system functions and warn if they
        // are not of integral type. Also warn if parameter with dollar as initial value used as argument.
        CheckIntegralArgument(expr, environment) ;
        // Return type unsigned int
        this_type = new VeriBuiltInType(VERI_INT) ;
        this_type->SetAsUnsigned() ;
        break ;
    case VERI_SYS_CALL_URANDOM_RANGE :
        if (!_args || _args->Size() > 2) { // Expect one/two arguments
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // VIPER #5887: Check for the type of the argument of some specific system functions and warn if they
        // are not of integral type. Also warn if parameter with dollar as initial value used as argument.
        CheckIntegralArgument(expr, environment) ;
        expr = (_args && _args->Size()>1) ? (VeriExpression*)_args->At(1) : 0 ;
        CheckIntegralArgument(expr, environment) ;
        // Return type unsigned int
        this_type = new VeriBuiltInType(VERI_INT) ;
        this_type->SetAsUnsigned() ;
        break ;
    case VERI_SYS_CALL_SRANDOM :
        if (!_args || _args->Size() != 1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // VIPER #5887: Check for the type of the argument of some specific system functions and warn if they
        // are not of integral type. Also warn if parameter with dollar as initial value used as argument.
        CheckIntegralArgument(expr, environment) ;
        // Return type void
        this_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_METHOD_GET_RANDSTATE :
        // VIPER #4258 : The check for expr is added to ensure that "$get_randstate()" doesn't error out
        if (_args && _name && expr) {
            if (_args->Size()==1) {
                // Viper 5248
                VeriTypeInfo *expr_type = expr->CheckType(0, NO_ENV) ;
                if (expr_type && expr_type->IsClassType()) {
                    Warning("%s expects %d arguments, violates IEEE 1800 syntax", _name, 0) ;
                }
                delete expr_type ;
            } else {
                Error("%s expects %d arguments", _name, 0) ;
            }
        }
        // Return type string
        this_type = new VeriBuiltInType(VERI_STRINGTYPE) ;
        break ;
    case VERI_METHOD_SET_RANDSTATE :
        arg_type = expr ? expr->CheckType(0, environment): 0 ;
        if (!_args || _args->Size() != 1) { // Expect one argument (an expression)
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // VIPER #5887: Only string, string literal, packed array/structure/union is allowed as argument:
        if (arg_type && !arg_type->IsString() && !arg_type->IsStringLiteral() && !arg_type->IsPacked() && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType()) {
            Warning("invalid expression in system call %s", _name) ;
        } else {
            VeriIdDef *id = expr ? expr->GetId() : 0 ;
            if (id && id->IsParam()) { // warn if the argument is parameter with dollar as initial value
                VeriExpression *initial_val = id->GetInitialValue() ;
                if (initial_val && initial_val->IsDollar()) {
                    Warning("illegal use of parameter %s with unbounded value", id->Name()) ;
                }
            }
        }
        delete arg_type ;
        // Return type void
        this_type = new VeriBuiltInType(VERI_VOID) ;
        break ;
    case VERI_SYS_CALL_CAST :
        if (!_args || _args->Size()!=2) {
            if (_name) Error("%s expects %d arguments", _name, 2) ;
        }
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            arg_type = expr ? expr->CheckType(0, environment) : 0 ;
            if (arg_type && !arg_type->IsSingularType() && !arg_type->IsUnresolvedNameType() && !arg_type->IsTypeParameterType()) {
                if (_name)  Error("illegal argument in %s, require %s", _name, "singular type") ;
            }
            delete arg_type ;
        }
        // Return type int
        this_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    // SVA system functions :
    case VERI_SYS_CALL_ROSE :
    case VERI_SYS_CALL_FELL :
    case VERI_SYS_CALL_STABLE :
    case VERI_SYS_CALL_CHANGED : // Viper #5756
        if (!_args || (_args->Size()!=1 && _args->Size()!=2)) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        FOREACH_ARRAY_ITEM(_args, i, expr) {
            arg_type = expr ? expr->CheckType(0, environment) : 0 ;
            delete arg_type ;
        }
        // Return value is true/false :
        this_type = new VeriBuiltInType(VERI_BIT) ;
        break ;
    case VERI_SYS_CALL_ROSE_GCLK :
    case VERI_SYS_CALL_FELL_GCLK :
    case VERI_SYS_CALL_STABLE_GCLK :
    case VERI_SYS_CALL_CHANGED_GCLK :
    case VERI_SYS_CALL_RISING_GCLK :
    case VERI_SYS_CALL_FALLING_GCLK :
    case VERI_SYS_CALL_CHANGING_GCLK :
    case VERI_SYS_CALL_STEADY_GCLK :
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Return value is true/false :
        this_type = new VeriBuiltInType(VERI_BIT) ;
        break ;
    case VERI_SYS_CALL_SAMPLED :
    case VERI_SYS_CALL_PAST :
    case VERI_SYS_CALL_PAST_GCLK :
    case VERI_SYS_CALL_FUTURE_GCLK :
        {
            VeriTypeInfo *first_type = 0 ;
            FOREACH_ARRAY_ITEM(_args, i, expr) {
                arg_type = expr ? expr->CheckType(0, environment) : 0 ;
                if (!i) {
                    first_type = arg_type ;
                    continue ;
                }
                delete arg_type ;
            }
            // Return type same as argument ? (FIXME)
            this_type = first_type ;
            break ;
        }
    case VERI_SYS_CALL_INSET :
    case VERI_SYS_CALL_INSETZ :
        break ;
    case VERI_SYS_CALL_COVERAGE_MERGE   :
    case VERI_SYS_CALL_COVERAGE_SAVE    :
    case VERI_SYS_CALL_COVERAGE_CONTROL :
    case VERI_SYS_CALL_COVERAGE_GET_MAX : // SV Std 1800 addition
    case VERI_SYS_CALL_COVERAGE_GET     :
        // Type infer the arguments with accepting environment
        FOREACH_ARRAY_ITEM (_args, i, expr) {
            arg_type = expr ? expr->CheckType(0, NO_ENV) : 0 ;
            delete arg_type ;
        }
        // Return type int
        this_type = new VeriBuiltInType(VERI_INT) ;
        break ;
    case VERI_SYS_CALL_FEOF :
        // This function expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Return type integer
        this_type = new VeriBuiltInType(VERI_INTEGER) ;
        break ;
    case VERI_SYS_CALL_TYPEOF :
        // VIPER #5819: Support for $typeof system function call:
        this_type = (expr) ? expr->CheckType(0, NO_ENV) : 0 ;
        break ;
    // Moved these function to VERILOG_2000 mode
    // LRM section 17.11.1/17.11.2 Viper 6807
    // Math functions (20.8)
    case VERI_SYS_CALL_CLOG2 :
        // This function expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Return type integer
        this_type = new VeriBuiltInType(VERI_INTEGER) ;
        break ;
    case VERI_SYS_CALL_SIN :
    case VERI_SYS_CALL_COS :
    case VERI_SYS_CALL_TAN :
    case VERI_SYS_CALL_ASIN :
    case VERI_SYS_CALL_ACOS :
    case VERI_SYS_CALL_ATAN :
    case VERI_SYS_CALL_SINH :
    case VERI_SYS_CALL_COSH :
    case VERI_SYS_CALL_TANH :
    case VERI_SYS_CALL_ASINH :
    case VERI_SYS_CALL_ACOSH :
    case VERI_SYS_CALL_ATANH :
    case VERI_SYS_CALL_LN  :
    case VERI_SYS_CALL_LOG10 :
    case VERI_SYS_CALL_EXP :
    case VERI_SYS_CALL_SQRT :
    case VERI_SYS_CALL_FLOOR :
    case VERI_SYS_CALL_CEIL :
        // This function expect one argument
        if (!_args || _args->Size()!=1) {
            if (_name) Error("system call %s expects one argument", _name) ;
        }
        // Return type real
        this_type = new VeriBuiltInType(VERI_REAL) ;
        break ;
    case VERI_SYS_CALL_ATAN2 :
    case VERI_SYS_CALL_HYPOT :
    case VERI_SYS_CALL_POW :
        // This function expect one argument
        if (!_args || _args->Size()!=2) {
            if (_name) Error("%s expects %d arguments", _name, 2) ;
        }
        // Return type real
        this_type = new VeriBuiltInType(VERI_REAL) ;
        break ;
    default : // FIXME : Check any thing left
        break ;
    }
    // Now type of this expression is already calculated, check it with expected type
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriMinTypMaxExpr::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = (_typ_expr) ? _typ_expr->CheckType(expected_type, environment) : 0 ;
    return this_type ;
}
VeriTypeInfo *VeriUnaryOperator::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // VIPER #4776 : Do not check type for block event expression, contains block/func/task identifier
    if ((_oper_type == VERI_BEGIN) || (_oper_type == VERI_END)) return 0 ;
    VeriTypeInfo *this_type = 0 ;

    // Get argument type :
    VeriTypeInfo *arg_type = (_arg) ? _arg->CheckType(0, environment) : 0 ;

    // Do not check operands if they are defined by type parameter or unresolved name type
    if (arg_type && (arg_type->IsTypeParameterType() || arg_type->IsUnresolvedNameType())) {
        delete arg_type ; // Delete created types and return 0
        return 0 ;
    }

    // VIPER #3193 : Check type of operands in binary operator and issue error for
    // illegal operands
    switch (_oper_type) {
    case VERI_PLUS :
    case VERI_MIN :
    case VERI_LOGNOT :
    case VERI_REDNOT :
        // Operand must be of type integral/real/shortreal :
        if (arg_type && !arg_type->IsIntegralType() && !arg_type->IsRealType()) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        if (_oper_type == VERI_LOGNOT) {
            this_type = new VeriBuiltInType(VERI_REG) ;
        } else {
            this_type = arg_type ;
            arg_type = 0 ;
        }
        break ;
    case VERI_REDAND :
    case VERI_REDNAND :
    case VERI_REDOR :
    case VERI_REDNOR :
    case VERI_REDXOR :
    case VERI_REDXNOR :
        // Operand can only be integral type :
        if (arg_type && arg_type->IsRealType()) {
            Error("illegal context for real expression") ;
        } else if (arg_type && !arg_type->IsIntegralType()) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // Result is 1-bit
        this_type = new VeriBuiltInType(VERI_REG) ;
        break ;
    default :
        break ; // Type 0
    }
    delete arg_type ;
    // If any enumerated type is used as operand in unary operator, return type
    // will be type of enumerated literal.
    if (this_type && this_type->IsEnumeratedType()) {
        VeriTypeInfo *ret_type = this_type->GetLiteralType() ;
        ret_type = ret_type ? ret_type->Copy() : 0 ;
        delete this_type ;
        this_type = ret_type ;
    }
    // Now type of this expression is already calculated, check it with expected type
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriBinaryOperator::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = 0 ;

    VeriTypeInfo *ltype = (_left) ? _left->CheckType(0, environment) : 0 ;

    // For set membership i.e '_oper_type' VERI_INSIDE, right operand is value range
    // list and we create concatenation for that. But that concatenation cannot have
    // same features as concatenation. It can have unpacked elements. So check type of
    // rhs with a different environment so that we can suppress error if rhs contains
    // unpacked array as element.
    veri_type_env right_env = environment ;
    if (_oper_type == VERI_INSIDE) right_env = VALUE_RANGE_LIST ;
    VeriTypeInfo *rtype = (_right) ? _right->CheckType(0, right_env) : 0 ;

    // Do not check operands if they are defined by type parameter or unresolved name type
    if ((ltype && (ltype->IsTypeParameterType() || ltype->IsUnresolvedNameType())) ||
        (rtype && (rtype->IsTypeParameterType() || rtype->IsUnresolvedNameType())))
    {
        delete ltype ; // Delete created types and return 0
        delete rtype ;
        return 0 ;
    }

    // VIPER #3193 : Check type of operands in binary operator and issue error for
    // illegal operands
    switch (_oper_type) {
    case VERI_INSIDE :
        // VIPER #4579 : Produce error if left operand is non-singular expression
        if (ltype && !ltype->IsSingularType()) {
            Error("left hand side of inside operator is not singular expression") ;
        }
        // Return type is 1 bit
        this_type = new VeriBuiltInType(VERI_REG) ;
        delete ltype ;
        delete rtype ;
        break ;
    case VERI_PLUS :
    case VERI_MIN :
    case VERI_MUL :
    case VERI_DIV :
    case VERI_POWER :
        // Operands can be of type integral/real/shortreal
        if ((ltype && !ltype->IsIntegralType() && !ltype->IsRealType())||
            (rtype && !rtype->IsIntegralType() && !rtype->IsRealType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // If any operand is of type real, return type is real
        if (rtype && rtype->IsRealType()) {
            this_type = rtype ;
            delete ltype ;
        } else { // Right operand not real, return type can be left operand type
            this_type = ltype ;
            delete rtype ;
        }
        break ;
    case VERI_LOGAND :
    case VERI_LOGOR :
    case VERI_RIGHTARROW : /* "->" logical implication */
    case VERI_LOG_EQUIVALENCE : /* "<->" logical equivalence */
        // Operands can be of type integral/real/shortreal
        if ((ltype && !ltype->IsIntegralType() && !ltype->IsRealType())||
            (rtype && !rtype->IsIntegralType() && !rtype->IsRealType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // Return type is bit
        this_type = new VeriBuiltInType(VERI_REG) ;
        delete ltype ;
        delete rtype ;
        break ;
    case VERI_LT :
    case VERI_GT :
    case VERI_GEQ :
    case VERI_LEQ :
        // Operands can be of type integral/real/shortreal/string
        // For string comparison both operands can be of type string or one of them
        // can be string literal
        if (ltype && rtype && ((ltype->IsString() && !rtype->IsString() && !rtype->IsStringLiteral()) ||
                   (rtype->IsString() && !ltype->IsString() && !ltype->IsStringLiteral()))) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        } else if ((ltype && !ltype->IsIntegralType() && !ltype->IsRealType() && !ltype->IsString() && !ltype->IsStringLiteral())||
            (rtype && !rtype->IsIntegralType() && !rtype->IsRealType() && !rtype->IsString() && !rtype->IsStringLiteral())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // Return type is bit
        this_type = new VeriBuiltInType(VERI_REG) ;
        delete ltype ;
        delete rtype ;
        break ;
    case VERI_MODULUS :
        // VIPER #5478 : Real opeands can be used for % operator in ams:
        if (IsAms()) {
            if ((ltype && !ltype->IsIntegralType() && !ltype->IsRealType()) || (rtype  && !rtype->IsIntegralType() && !rtype->IsRealType())) {
                Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
            }
            // If any operand is of type real, return type is real
            if (rtype && rtype->IsRealType()) {
                this_type = rtype ;
                delete ltype ;
            } else { // Right operand not real, return type can be left operand type
                this_type = ltype ;
                delete rtype ;
            }
            break ;
        }

    case VERI_REDAND :
    case VERI_REDOR :
    case VERI_REDXOR :
    case VERI_REDXNOR :
    case VERI_LSHIFT :
    case VERI_RSHIFT :
    case VERI_ARITLSHIFT :
    case VERI_ARITRSHIFT :
        // Operands can be integral type
        if ((ltype && ltype->IsRealType()) || (rtype && rtype->IsRealType())) {
            Error("illegal context for real expression") ;
        } else if ((ltype && !ltype->IsIntegralType()) || (rtype && !rtype->IsIntegralType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // Return type is integral type
        this_type = ltype ;
        delete rtype ;
        break ;
    case VERI_CASEEQ :
    case VERI_CASENEQ :
        // Real cannot be used as operand :
        if ((ltype && ltype->IsRealType()) || (rtype && rtype->IsRealType())) {
            // VCS Compatibility issue (test16) : Produce warning in relaxed checking mode
            if (InRelaxedCheckingMode()) {
                Warning("illegal context for real expression") ;
            } else {
                Error("illegal context for real expression") ;
            }
        }

    case VERI_LOGEQ :
    case VERI_LOGNEQ :
    {
        unsigned is_left_type_op = (_left && (_left->IsTypeOperator() || (_left->GetFunctionType() == VERI_SYS_CALL_TYPEOF))) ? 1 : 0 ;
        unsigned is_right_type_op = (_right && (_right->IsTypeOperator() || (_right->GetFunctionType() == VERI_SYS_CALL_TYPEOF))) ? 1 : 0 ;
        if (ltype && rtype
                && !is_left_type_op && !is_right_type_op
                ) {
            // String type allowed as operand
            // Both sides can be of type string or one of them can be string literal
            if ((ltype->IsString() && !rtype->IsString() && !rtype->IsStringLiteral()) ||
                (rtype->IsString() && !ltype->IsString() && !ltype->IsStringLiteral())) {
                Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
            } else if ((ltype->IsPacked() && rtype->IsUnpacked() && !rtype->IsBuiltInType()) ||
                       (ltype->IsUnpacked() && !ltype->IsBuiltInType() && rtype->IsPacked())) {
            //} else if (ltype->IsPacked() != rtype->IsPacked()) {
                // Mixture of packed and unpacked as operand is not allowed
                Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
            }
        }
        // Return type is bit
        this_type = new VeriBuiltInType(VERI_REG) ;
        delete ltype ;
        delete rtype ;
        break ;
    }
    case VERI_WILDEQUALITY :
    case VERI_WILDINEQUALITY :
        // Only integral types are allowed
        // According to SV-2009 (11.4.6) class objects, chandle and null are allowed
        // as operands
        if ((ltype && (ltype->IsChandleType() || ltype->IsClassType()) &&
                rtype && !rtype->IsChandleType() && !rtype->IsClassType()) ||
            (rtype && (rtype->IsChandleType() || rtype->IsClassType()) &&
                ltype && !ltype->IsChandleType() && !ltype->IsClassType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;

        } else if ((ltype && !ltype->IsIntegralType() && !ltype->IsChandleType() && !ltype->IsClassType()) ||
                   (rtype && !rtype->IsIntegralType() && !rtype->IsChandleType() && !rtype->IsClassType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // Return type is bit
        this_type = new VeriBuiltInType(VERI_REG) ;
        delete ltype ;
        delete rtype ;
        break ;
    case VERI_INC_OP :
    case VERI_DEC_OP :
        // Operands can be of type integral/real/shortreal
        // Enum data type cannot be used here
        if ((ltype && ltype->IsEnumeratedType()) || (rtype && rtype->IsEnumeratedType())) {
            Error("enum data type cannot be used with %s operator", PrintToken(_oper_type)) ;
        } else if ((ltype && !ltype->IsIntegralType() && !ltype->IsRealType()) ||
                   (rtype && !rtype->IsIntegralType() && !rtype->IsRealType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        // Return type is same as operand
        if (ltype) {
            this_type = ltype ;
            delete rtype ;
        } else if (rtype) {
            this_type = rtype ;
        }
        break ;
    case VERI_PLUS_ASSIGN :
    case VERI_MIN_ASSIGN :
    case VERI_MUL_ASSIGN :
    case VERI_DIV_ASSIGN :
        // Operands can be of type integral/real/shortreal
        // Enum data type cannot be used here
        if (ltype && ltype->IsEnumeratedType()) {
            Error("invalid operator %s in assignment to enum", PrintToken(_oper_type)) ;
        } else if ((ltype && !ltype->IsIntegralType() && !ltype->IsRealType()) ||
            (rtype && !rtype->IsIntegralType() && !rtype->IsRealType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        if (rtype && rtype->IsRealType()) {
            this_type = rtype ;
            delete ltype ;
        } else {
            this_type = ltype ;
            delete rtype ;
        }
        break ;
    case VERI_EQUAL_ASSIGN :
        if (_left && _right) _left->CheckAssignment(_right, _oper_type) ;
        this_type = ltype ;
        delete rtype ;
        break ;
    case VERI_MOD_ASSIGN :
    case VERI_AND_ASSIGN :
    case VERI_OR_ASSIGN :
    case VERI_XOR_ASSIGN :
    case VERI_LSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
        // Operands can be of type integral
        // Enum data type cannot be used here
        if (ltype && ltype->IsEnumeratedType()) {
            Error("invalid operator %s in assignment to enum", PrintToken(_oper_type)) ;
        } else if ((ltype && ltype->IsRealType()) || (rtype && rtype->IsRealType())) {
            Error("illegal context for real expression") ;
        } else if ((ltype && !ltype->IsIntegralType()) || (rtype && !rtype->IsIntegralType())) {
            Error("illegal operand for operator %s", PrintToken(_oper_type)) ;
        }
        this_type = ltype ;
        delete rtype ;
        break ;
    case VERI_CONSECUTIVE_REPEAT :
    case VERI_GOTO_REPEAT :
    case VERI_NON_CONSECUTIVE_REPEAT :
    //case VERI_EQUAL_ASSIGN :
    // Sequence/property
    case VERI_OR :
    case VERI_INTERSECT :
    case VERI_AND :
    case VERI_THROUGHOUT :
    case VERI_WITHIN :
    default :
        delete ltype ;
        delete rtype ;
        break ; // FIXME : What will be the type
    }
    // If any enumerated type is used as operand in binary operator, return type
    // will be type of enumerated literal.
    if (this_type && this_type->IsEnumeratedType()) {
        VeriTypeInfo *ret_type = this_type->GetLiteralType() ;
        ret_type = ret_type ? ret_type->Copy() : 0 ;
        delete this_type ;
        this_type = ret_type ;
    }
    // Now type of this expression is already calculated, check it with expected type
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriQuestionColon::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // VIPER #4225 : Check type of condition :
    // VIPER #5635 : Condition of ?: operator will always be expression
    VeriTypeInfo *if_type = (_if_expr) ? _if_expr->CheckType(0, READ) : 0 ;
    delete if_type ;
    VeriTypeInfo *true_type = (_then_expr) ? _then_expr->CheckType(expected_type, environment) : 0 ;
    VeriTypeInfo *false_type = (_else_expr) ? _else_expr->CheckType(expected_type, environment) : 0 ;
    // Do not create type if prefix_type is from type parameter/unresolved name
    if ((true_type && (true_type->IsTypeParameterType() || true_type->IsUnresolvedNameType()))
       || (false_type && (false_type->IsTypeParameterType() || false_type->IsUnresolvedNameType()))) {
        delete true_type ;
        delete false_type ;
        return 0 ;
    }

    // Mixture of packed/unpacked type as operand is not allowed
    if (true_type && false_type && ((true_type->IsPacked() && false_type->IsUnpacked() && !false_type->IsBuiltInType()) ||
        (true_type->IsUnpacked() && !true_type->IsBuiltInType() && false_type->IsPacked()))) {
        Error("illegal operand for operator %s", "?:") ;
    }
    VeriTypeInfo *this_type = 0 ;

    // If any operand is real, result is real
    if (false_type && false_type->IsRealType()) {
        this_type = false_type ;
        delete true_type ;
    } else {
        this_type = true_type ;
        delete false_type ;
    }
    // Now type of this expression is already calculated, check it with expected type
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriEventExpression::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env /*environment*/)
{
    return 0 ; // Cannot be used in expression
}
VeriTypeInfo *VeriPortConnect::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // Check type of connection
    return (_connection) ? _connection->CheckType(expected_type, environment) : 0 ;
}
VeriTypeInfo *VeriPortOpen::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env /*environment*/)
{
    return 0 ; // No type, return 0 ;
}
VeriTypeInfo *VeriPatternMatch::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env environment)
{
    if (!_right || !_left) return 0 ; // Analyze error, cannot proceed further
    // _right is pattern which contains pattern identifiers. Data types
    // of pattern identifiers should be set extracting type information from _left
    VeriTypeInfo *left_type = _left->CheckType(0, environment) ;
    VeriTypeInfo *right_type = _right->CheckType(left_type, environment) ;
    delete left_type ; delete right_type ;
    return 0 ; // FIXME : Need to return anything
}
VeriTypeInfo *VeriConstraintSet::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env /*environment*/)
{
    // FIXME: What to do here?
    return 0 ;
}
VeriTypeInfo *VeriCast::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    if (!_target_type) return 0 ; // No casting type, can't check anything

    // Type of this expression is same as target type
    // VIPER #5095: Call check type with parameter initial value as environment:
    VeriTypeInfo *this_type = _target_type->CheckType(0, PARAM_INIT) ;

    VeriTypeInfo *expr_type = (_expr) ? _expr->CheckType(this_type, CASTING): 0 ;

    if (_target_type->Type() == VERI_CONST) {
        // If casting type is 'const', type of casting will be type of
        // expression
        delete this_type ;
        this_type = expr_type ;
    } else {
        // For size and sign cast _expr must be integral type or string (VIPER #4429)
        if (expr_type && (_target_type->IsConst() || // size cast
             (_target_type->Type()==VERI_SIGNED) || (_target_type->Type()==VERI_UNSIGNED))) { // sign cast
            if (!expr_type->IsIntegralType() && !expr_type->IsString() && !expr_type->IsTypeParameterType() && !expr_type->IsUnresolvedNameType()) { // Expression is not integral type
                if (_target_type->IsConst() /* _target_type->IsConstExpr()? */ && expr_type->IsRealType()) {
                    // VIPER #5248 (test_41): Allow size castring on real type under compatibility mode:
                    // So, add a new message, such that we can have the severity downgraded for this only.
                    Error("size casting a real expression violates IEEE 1800 LRM") ;
                } else {
                    Error("illegal cast operation") ;
                }
            }
            // SV-2009 section 6.24.1 : For size and sign casting return type
            // will be packed array. Element type will be 'bit', if expression is 2 state,
            // otherwise element type will be 'logic'. For size casting signedness of return
            // type will be the signedness of expression. For sign casting signedness of return
            // type will be signedness specified by casting type.
            delete this_type ;
            VeriTypeInfo *element_type = new VeriBuiltInType(expr_type->Is2State()? VERI_BIT : VERI_LOGIC) ;
            if (_target_type->Type()==VERI_SIGNED) {
                element_type->SetAsSigned() ;
            } else if (_target_type->Type()==VERI_UNSIGNED) {
                element_type->SetAsUnsigned() ;
            } else {
                if (expr_type->IsSigned()) {
                    element_type->SetAsSigned() ;
                } else {
                    element_type->SetAsUnsigned() ;
                }
            }
            this_type = new VeriArrayType(element_type, 1) ;
        } else if (this_type && (!expr_type || (!expr_type->IsTypeParameterType() && !expr_type->IsUnresolvedNameType() && !expr_type->IsAssignmentCompatible(this_type)))) {
            // VIPER #7662 : If expression is not assignment compatible with
            // casting type, then casting type can be enumerated type or bit-stream
            // type
            if (this_type->IsEnumeratedType()) {
                if (expr_type && expr_type->IsUnpacked()) {
                    Error("illegal cast operation") ;
                }
            } else if (this_type->IsBitStreamType()) {
                // Don't produce error for bit-stream type. The behaviour of
                // simulators does not match with LRM.
                //
                // We need to check size too FIXME
                //if (expr_type && !expr_type->IsBitStreamType()) {
                    //Error("illegal cast operation") ;
                //}
            }
        }

        // VIPER #8400: Do not produce error when expr_type is type parameter type
        if (this_type && (this_type->IsPacked() || (this_type->IsUnpacked() && this_type->IsBuiltInType())) && expr_type && !expr_type->IsTypeParameterType() && !expr_type->IsUnresolvedNameType() && expr_type->IsUnpacked() && !expr_type->IsBuiltInType()) {
            if (!this_type->IsBitStreamType()) {
                // FIXME : For BitStream Type we need to check dimensions
                Error("illegal cast operation") ;
            }
        }
        delete expr_type ;
    }
    if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    return this_type ; // VIPER #7907: error: always return this type
}
VeriTypeInfo *VeriNew::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // New expression can be used to assign a dynamic array, class or covergroup
    // variable. Any other type as target is illegal
    if (expected_type && !expected_type->IsDynamicArrayType() &&
          !expected_type->IsClassType() && !expected_type->IsCovergroupType() &&
          !expected_type->IsTypeParameterType() && !expected_type->IsUnresolvedNameType()) {
        Error("new expression can only be assigned to a class/covergroup handle/dynamic array") ;
    }
    // When 'expected_type' ia dynamic array and _args exists, type of argument
    // should be same as 'expected_type'
    if (expected_type && expected_type->IsDynamicArrayType()) {
        // VIPER #5475: Size expression should be specified for dynamic array target
        if (!_size_expr) {
            Error("new expression can only be assigned to a class/covergroup") ;
        }
        if (_args) {
            // _args should have only one argument
            if (_args->Size() > 1) Error("%s expects %d arguments", "dynamic array new", 1) ;
            VeriExpression *arg = (_args->Size()) ? (VeriExpression*)_args->At(0): 0 ;
            // VIPER #5798 : Argument is used to initialze the dynamic array. So
            // it can be an array or array assignment pattern. If argument is
            // assignment pattern, pass expected type to check type of that assignment
            // pattern
            VeriTypeInfo *arg_type = (arg) ? arg->CheckType((arg->IsAssignPattern() || arg->IsMultiAssignPattern() || arg->IsConcat() || arg->IsMultiConcat()) ? expected_type: 0, environment): 0 ;
            //if (arg && arg_type && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType() && !arg_type->IsEquivalent(expected_type, 0, 0, 0)) {
            // VIPER #7335 : LRM P1800-2009 section 7.5.1 says argument should be
            // an array having assignment compatible with target dynamic array
            if (arg && arg_type && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType() && !arg_type->IsAssignmentCompatible(expected_type)) {
                arg->Error("argument should be dynamic array") ;
            }
            delete arg_type ;
        }
    }
    // VIPER #5164: When expected type is class type, validate the new function call:
    if (expected_type && expected_type->IsClassType()) {
        // Get class identifier
        VeriIdDef *class_id = expected_type->GetNameId() ;
        VeriModuleItem *class_def = (class_id) ? class_id->GetModuleItem(): 0 ;

        // Get new function
        VeriIdDef *new_id = (class_def) ? class_def->GetNewFunctionId(): 0 ;
        unsigned copy_constructor = 0 ;
        if (_args && _args->Size() == 1) {
            // If number of arguments are 1, it may be copy constructor call :
            VeriExpression *arg = (VeriExpression*)_args->At(0) ;
            VeriTypeInfo *arg_type = (arg && (arg->IsConcat() || arg->IsMultiConcat() || arg->IsAssignPattern() || arg->IsMultiAssignPattern())) ? 0 : (arg ? arg->CheckType(0, NO_ENV): 0) ;
            if (arg_type && !arg_type->IsTypeParameterType() && !arg_type->IsUnresolvedNameType() && arg_type->IsEquivalent(expected_type, 0, 0, 0)) {
                copy_constructor = 1 ;
            }
            delete arg_type ;
        }
        // IEEE 1800-2012, Section 8.26.5 : An object of an interface-class/abstract-class type shall not be constructed
        if (!copy_constructor && class_def && class_def->GetQualifier(VERI_INTERFACE)) {
            Error("illegal use of new with interface/abstract class") ;
        }
        // Viper 5649. 5466. Moved the fix for viper 5466 from VeriVarUsage
        if (!new_id && !copy_constructor && _args && _args->Size()) Error("%s expects %d arguments", "new", 0) ;
        if (new_id && !copy_constructor) {
            Array *ports = new_id->GetPorts() ;
            unsigned all_ports_connected = 1 ;
            unsigned i ;
            VeriExpression *expr ;
            VeriIdDef *formal ;
            VeriScope *formal_scope = new_id->LocalScope() ;
            Map connected_formals(POINTER_HASH, (ports) ? ports->Size():1) ;
            FOREACH_ARRAY_ITEM(_args, i, expr) {
                if (!expr) continue ;
                formal = 0 ;
                if (expr->NamedFormal()) {
                    formal = (formal_scope) ? formal_scope->FindLocal(expr->NamedFormal()) : 0 ;
                    if (!formal) {
                        expr->Error("%s has no port called %s",new_id->Name(), expr->NamedFormal()) ;
                        new_id->Info("%s is declared here",new_id->Name()) ;
                    }
                    if (formal && !formal->IsPort()) {
                        expr->Error("no definition for port %s",formal->Name()) ; // VIPER #8172
                        formal = 0 ;
                    }
                } else {
                    if (i>=((ports) ? ports->Size(): 0)) {
                        Error("%s expects %d arguments", new_id->Name(), ((ports) ? ports->Size(): 0)) ;
                        break ;
                    } else {
                        formal = (ports && ports->Size() > i) ? (VeriIdDef*)ports->At(i): 0 ;
                    }
                }
                VeriExpression *conn_expr = expr->GetConnection() ;
                unsigned long connected = (!conn_expr || conn_expr->IsOpen()) ? 0 : 1 ;
                all_ports_connected = all_ports_connected & connected ;
                if (formal && !connected_formals.Insert(formal, (void *)connected)) {
                    expr->Error("port %s is already connected", formal->Name()) ;
                }
                (void) expr->CheckAssociation(formal, FUNC) ;
            }
            if ((((ports) ? ports->Size() : 0) != connected_formals.Size()) || !all_ports_connected) {
                FOREACH_ARRAY_ITEM(ports, i, formal) {
                    if (!formal) continue ;
                    if (connected_formals.GetValue(formal)) continue ;
                    VeriExpression *initial_expr = formal->GetInitialValue() ;
                    if (initial_expr) continue ;
                    Error("port %s is not connected on %s call %s", formal->Name(), "function", "new") ;
                }
            }
        }
    }
    // Return type will be same as expected type
    VeriTypeInfo *this_type = (expected_type) ? expected_type->Copy() : 0 ;
    return this_type ;
}
VeriTypeInfo *VeriConcatItem::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *ret_type = 0 ;
    if (!_member_label) { // default : value
        // VIPER #2725 : Value of default does not depend on target type. It can be
        // applied recursively. Default expression is to be of packed type
        VeriTypeInfo *packed_type = new VeriArrayType(new VeriBuiltInType(VERI_REG), 1) ;
        if (_expr) ret_type = _expr->CheckType(packed_type, NO_ENV) ;
        delete packed_type ;
        return ret_type ;
    }
    // If 'base_type' is struct, label can be type or member name.
    // If 'base_type' is an array type, label can be type or constant expression

    // _member_label can contain named type as data type
    VeriIdDef *type_id = _member_label->GetId() ;
    if (_member_label->IsDataType() || (type_id && type_id->IsType())) { // Label can be data type for both struct and array
        // Label is data type, _expr should be of that type
        VeriTypeInfo *expr_type = _member_label->CheckType(0, NO_ENV) ;
        // Simulators allow enum_type : 1'b1, so if member_label type is enum,
        // pass CASTING as environment, otherwise 'environment'
        if (_expr) ret_type = _expr->CheckType(expr_type, (expr_type && expr_type->IsEnumeratedType()) ? CASTING: environment) ;
        delete expr_type ;
        return ret_type ;
    }
    if (expected_type && expected_type->IsStructure()) { // Target is structure
        // _member_label should be member name
        VeriIdDef *member_id = expected_type->GetMember(_member_label->GetName()) ;
        if (member_id) {
            _member_label->SetId(member_id) ; // Set member id
            // Type of expression will be that of the member
            VeriTypeInfo *member_type = expected_type->GetMemberType(_member_label->GetName()) ;
            if (_expr) ret_type = _expr->CheckType(member_type, environment) ;
        } else if (_member_label->GetName()) {
            Error("%s is not declared", _member_label->GetName()) ;
        } else {
            Error("illegal concat label for %s", "struct") ;
        }
    } else if (expected_type && !expected_type->IsTypeParameterType() && !expected_type->IsUnresolvedNameType()) { // Target type is not structure, it is array/vector type
        // Member name is not allowed here as label
        //const char *member_name = _member_label->GetName() ;
        //VeriIdDef *id = _member_label->GetId() ;
        // Parameter/genvar/constant identifier can be used as label
        //if ((id && !id->IsParam() && !id->IsGenVar() && !id->IsVar()) || (!id && member_name)) {
        // VIPER #8392 : Allow constant expression as index label of array assignment pattern
        if (!_member_label->IsConstExpr()) {
            Error("illegal concat label for %s", "array") ;
        }
        // FIXME : Issue error if _member_label is not constant expression
        // For this we need a routine like VeriExpression::IsConstantExpr
        // Label is constant expression i.e. array index, check expr

        // VIPER #5135 : If expected type contains dimension, each element will be checked
        // with a type having one less dimension
        VeriTypeInfo *ele_expected_type = 0 ;
        unsigned is_created = 0 ;
        if (expected_type->UnpackedDimension() || expected_type->PackedDimension()) {
            ele_expected_type = expected_type->Copy() ;
            Array idx_arr(1) ;
            idx_arr.InsertLast(0 /* no index expr*/) ;
            ele_expected_type = ele_expected_type->ReduceDimBy(&idx_arr, 0) ;
            is_created = 1 ;
        } else {
            ele_expected_type = expected_type->ElementType() ;
        }
        //if (_expr) ret_type = _expr->CheckType(expected_type->ElementType(), environment) ;
        if (_expr) ret_type = _expr->CheckType(ele_expected_type, environment) ;
        if (is_created) delete ele_expected_type ;
    }
    return ret_type ;
}
VeriTypeInfo *VeriNull::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // Null value can be used for chandle type, event type, as index of associative
    // array, class object, argument to randomize call and virtual interface.
    if (!expected_type || (expected_type->IsTypeParameterType() || expected_type->IsUnresolvedNameType())) return 0 ;

    // VIPER #5131 : Produce error is exprected type is other than chandle, event,
    // object
    // VIPER #5706 : null expression can be used as initial value of property/sequence formal
    // Standard simulators support this.
    if (!expected_type->IsChandleType() && !expected_type->IsTypeParameterType() && !expected_type->IsUnresolvedNameType() && !expected_type->IsEventType() && !expected_type->IsClassType() && !expected_type->IsCovergroupType() && !expected_type->IsVirtualInterface() && (environment != PROP_SEQ_INIT_VAL)) {
        Error("null as source expression is not allowed here") ;
    }
    return 0 ;
}
VeriTypeInfo *VeriDollar::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    if (!expected_type || expected_type->IsTypeParameterType() || expected_type->IsUnresolvedNameType()) return 0 ;
    // '$' can be used as initial value of integer type parameters and as range bounds
    // or as an actual to checker instance
    if (!(expected_type->IsIntegralType() && (environment == PARAM_INIT))
            && (environment != RANGE_BOUND)) {
        Error("illegal context for $") ;
    }
    return 0 ; // FIXME : Should return unbound type
}
VeriTypeInfo *VeriAssignmentPattern::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = 0 ;
    if (_type) this_type = _type->CheckType(0, environment) ; // Get type info

    // VIPER #3054 : Issue error for illegal context of assignment pattern
    // VIPER #3256 : If target contains dimensions, assignment pattern is legal as value
    if (expected_type && expected_type->IsUnion()) {
        // VIPER #5248/#5301 (test_47): Different error message for target union:
        if (environment != CASTING) Error("illegal assignment of structure literals to union") ;
        return this_type ; // VIPER #7907: error: always return this type
    } else if (!expected_type && !_type && (environment != WRITE) && (environment != INST_WRITE) && (environment != NO_ENV)) {
        // Type of assignment pattern should be determined from context
        if (environment != CASTING) Error("illegal context for %s", "assignment pattern") ;
        return this_type ; // VIPER #7907: error: always return this type
    }
    VeriTypeInfo *passing_type = (this_type) ? this_type : expected_type ;
    if (!CheckAssignmentPatternType(_exprs, passing_type, environment, this)) {
        passing_type = 0 ;
    }

    if (this_type && !this_type->CheckAgainst(expected_type, this, environment)) {
        return this_type ; // VIPER #7907: error: always return this type
    }
    VeriTypeInfo *ret_type = (passing_type) ? passing_type->Copy() : 0 ;
    delete this_type ;
    return ret_type ;
}
// VIPER #7104 : Remove checking for multi-assignment pattern
unsigned  VeriAssignmentPattern::CheckAssignmentPatternType(const Array *exprs, VeriTypeInfo *expected_type, veri_type_env environment, const VeriTreeNode *from)
{
    if (!expected_type) return 1 ; // Cannot check type without expected type
    // Do not check anything if expected type is type parameter type/unresolved name type
    if (expected_type->IsTypeParameterType() || expected_type->IsUnresolvedNameType()) return 1 ;

    // Type infer elements :
    // If expected type contains unpacked dimension, each element will be checked
    // with a type having one less unpacked dimension. Do that
    unsigned ret_status = 1 ;
    VeriTypeInfo *ele_expected_type = 0 ;
    unsigned is_created = 0 ;
    unsigned unpacked_dim = expected_type->UnpackedDimension() ;
    unsigned packed_dim = expected_type->PackedDimension() ;
    if (unpacked_dim || packed_dim) {
        ele_expected_type = expected_type->Copy() ;
        Array idx_arr(1) ;
        idx_arr.InsertLast(0 /* no index expr*/) ;
        ele_expected_type = ele_expected_type->ReduceDimBy(&idx_arr, 0) ;
        is_created = 1 ;
    } else {
        ele_expected_type = expected_type ;
    }
    unsigned is_target_queue = expected_type->IsQueue() ;
    unsigned i ;
    unsigned default_label = 0 ;
    unsigned all_concat_item = 1 ; // Do not check/error out if all items are not concat item
    VeriExpression *expr ;
    VeriTypeInfo *ele_type = 0 ;
    // VIPER #5160: Insert all the member type against member id into map
    Map *member_ids = expected_type ? expected_type->GetMembers() : 0 ;
    Set members(POINTER_HASH) ;
    unsigned has_unpacked_arr_struct_mem = 0 ;
    FOREACH_ARRAY_ITEM(exprs, i, expr) {
        if (!expr) continue ;
        // Member of multi-assignment pattern cannot be name/type : expression
        if (!expr->IsConcatItem()) {
            all_concat_item = 0 ; // This is not a concat item
        }
        ele_type = 0 ;
        unsigned processed = 0 ;
        if (unpacked_dim || packed_dim) { // unpacked dimensions : array literal
            // VIPER #4429: If target is queue, allow queue type as element.
            if (is_target_queue && !expr->IsConcat() && !expr->IsMultiConcat()) {
                ele_type = expr->CheckType(0, environment) ;
                processed = 1 ;
            }
            if (!is_target_queue || !processed || (ele_type && !ele_type->IsQueue())) {
                delete ele_type ;
                ele_type = expr->CheckType((expr->IsConcatItem()) ? expected_type :ele_expected_type, environment) ;
            }
            if (expr->IsConcatItem()) { // member_name/type: expression
                // When assignment pattern is used as lhs, positional notation is required
                if ((environment == WRITE) || (environment == INST_WRITE)) expr->Error("illegal concat label for %s", "array") ;
            }

            // VIPER #8240: According to LRM Section 10.9 'Assignment Patterns' -
            // When an assignment pattern expression is used in a left-hand expression,
            // the positional notation shall be required; and each member expression shall
            // have a bit-stream data type that is assignment compatible with the corresponding
            // element in the data type of the assignment pattern expression.
            if ((environment == WRITE) || (environment == INST_WRITE)) {
                //  Each member expression of assignment pattren should have a bitstream datatype:
                if (ele_type && !ele_type->IsBitStreamType() && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType()) {
                    expr->Error("each member expression of assignment pattren should have a bitstream datatype") ;
                }

                // Element should be assignment compatible with the corresponding element of the datatype of the assignment pattern.
                if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() && !ele_type->IsAssignmentCompatible(ele_expected_type)) {
                    expr->Error("incompatible assignment") ;
                }
            }

            // If context is associative array, 'expr' should be index_type : element value
            // Check index type :
            if (expected_type && expected_type->IsAssociativeArrayType()) {
                if (expr->IsConcatItem()) {
                    VeriExpression *label = expr->GetMemberLabel() ;
                    VeriTypeInfo *label_type = (label) ? label->CheckType(0, READ) : 0 ;
                    // VIPER #5492 : Do not check for equivalent type, do explicit
                    // check for each index type of associative array :
                    expected_type->CheckIndexAgainst(label_type, label) ;
                    delete label_type ;
                } else {
                    if (environment != CASTING) expr->Error("use '{index : value} syntax in associative array literal") ;
                }
            }
        } else if (ele_expected_type && ele_expected_type->IsStructure()) { // 'base_type' is structure
            // VIPER #4129 : Take structure member only if target is not packed array of structure :
            // 'expr' can be strcuture member value or name/type : expression
            if (expr->IsConcatItem()) { // member_name/type: expression
                // When assignment pattern is used as lhs, positional notation is required
                // i.e label : value is illegal
                if ((environment == WRITE) || (environment == INST_WRITE)) {
                   if (environment != CASTING) expr->Error("illegal concat label for %s", "struct") ;
                }

                ele_type = expr->CheckType(ele_expected_type, environment) ;

                // VIPER #5160: Error out if all the members of structure is not initialised, for that we check that
                // the structure member name/type of expression and that of structure declaration is different or not
                const char *name = 0 ;
                VeriTypeInfo *expr_type = 0 ;
                VeriExpression *member_label = expr->GetMemberLabel() ;
                // GetMemberLabel() returns 0 for default label
                if (!member_label && !default_label) {
                    default_label = 1 ;
                    if (members.Size() > 0) members.Reset() ; // For default member label
                }

                // VIPER #5415: Consider named type in member label
                VeriIdDef *type_id = member_label ? member_label->GetId(): 0 ;
                // 'expr' can be strcuture strcuture member type : expression
                if (!default_label && member_label && (member_label->IsDataType() ||  (type_id && type_id->IsType()))) {
                    expr_type = member_label->CheckType(0, NO_ENV) ;
                // 'expr' can be strcuture member name : expression
                } else if (!default_label && member_label) {
                    name = member_label->GetName() ;
                }

                MapIter mi ;
                VeriTypeInfo *member_type = 0 ;
                VeriIdDef *member_id = 0 ;
                if (expr_type) { // 'expr' is strcuture member type : expression
                    // For each member of the expression we check against each map member
                    FOREACH_MAP_ITEM(member_ids, mi, &member_id, &member_type) {
                        if (!member_id || !member_type) continue ;
                        VeriTypeInfo *member_base_type = member_type->BaseType() ;

                        // VIPER #8240: According to LRM Section 10.9 'Assignment Patterns' -
                        // When an assignment pattern expression is used in a left-hand expression,
                        // the positional notation shall be required; and each member expression shall
                        // have a bit-stream data type that is assignment compatible with the corresponding
                        // element in the data type of the assignment pattern expression.
                        if ((environment == WRITE) || (environment == INST_WRITE)) {
                            // Each member expression of assignment pattren should have a bitstream datatype:
                            if (ele_type && !ele_type->IsBitStreamType() && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType()) {
                                expr->Error("each member expression of assignment pattren should have a bitstream datatype") ;
                            }
                            // Element should be assignment compatible with the corresponding element of the datatype of the assignment pattern.
                            if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() && !ele_type->IsAssignmentCompatible(member_type)) {
                                expr->Error("incompatible assignment") ;
                            }
                        }

                        // Structure member type of expression and that of structure declaration is same
                        // VIPER #5510 : consider original member type for type checking
                        //if ((member_base_type && expr_type->IsEqual(member_base_type, 0)) || (expr_type->IsEqual(member_type, 0))) {
                        // VIPER #6573 : label type should be matching type of member/member's base type
                        if ((member_base_type && expr_type->IsEquivalent(member_base_type, 0, 0, 0)) || (expr_type->IsEquivalent(member_type, 0, 0, 0))) {
                            (void) members.Insert(member_id) ; // Add to Set
                        }
                        if (member_type->IsStructure() || member_type->IsUnpackedArrayType()) {
                            has_unpacked_arr_struct_mem = 1 ;
                        }
                    }
                    delete expr_type ; //JJ: delete here to avoid leak
                }

                // 'expr' is strcuture member name : expression
                if (name) (void) members.Insert(ele_expected_type->GetMember(name)) ; // Add to Set
            } else { // Get member of position 'i'
                // If number of assignment pattern elements is not same as
                // member count, it may be the case that target is packed
                // array and then we can assign any single packed type expression
                //if ((exprs->Size() == 1) && (member_ids && member_ids->Size() > 1) && (ele_expected_type && !ele_expected_type->IsUnpacked())) {
                    // target packed structure, assign a packed expression
                    //ele_type = expr->CheckType(ele_expected_type, environment) ;
                //} else {
                    VeriTypeInfo *member_type = ele_expected_type->GetMemberTypeAt(i) ;
                    if (!member_type && (ele_expected_type->NumOfElements() != exprs->Size())) {
                        if (environment != CASTING) expr->Error("number of elements does not match with the type") ;
                        ret_status = 0 ;
                    }

                    // Find the element type:
                    ele_type = expr->CheckType(member_type, environment) ;

                    // VIPER #8240: According to LRM Section 10.9 'Assignment Patterns' -
                    // When an assignment pattern expression is used in a left-hand expression,
                    // the positional notation shall be required; and each member expression shall
                    // have a bit-stream data type that is assignment compatible with the corresponding
                    // element in the data type of the assignment pattern expression.
                    if ((environment == WRITE) || (environment == INST_WRITE)) {
                        // When assignment pattern is used as lhs, positional notation is required
                        if (expr->IsConcatItem()) { // member_name/type: expression
                            expr->Error("illegal concat label for %s", "array") ;
                        }

                        // Each member expression of assignment pattren should have a bitstream datatype:
                        if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() && !ele_type->IsBitStreamType()) {
                            expr->Error("each member expression of assignment pattren should have a bitstream datatype") ;
                        }
                        // Element should be assignment compatible with the corresponding element of the datatype of the assignment pattern.
                        if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() && !ele_type->IsAssignmentCompatible(member_type)) {
                            expr->Error("incompatible assignment") ;
                        }
                    }

                    // VIPER #5741 : When it is multi-assignment pattern, element
                    // can be structure member expr or expression of structure type
                    // VIPER #7104 : Multi assignment pattern is handled in
                    // VeriMultiAssignmentPattern::CheckType routine
                    //if (is_muti_assign && (expected_type->IsPackedArrayType() || expected_type->IsUnpackedArrayType()) &&
                            //!expr->IsConcat() && !expr->IsMultiConcat() && !expr->IsAssignPattern() && !expr->IsMultiAssignPattern()) {
                        //ele_type = expr->CheckType(0, environment) ;
                        //if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() &&
                                //ele_type->IsEquivalent(ele_expected_type, 0, 0, 0) && (exprs->Size() == 1)) {
                            //// All members are covered
                            //unsigned j ;
                            //for(j = 0; j < ele_expected_type->NumOfElements(); j++) {
                                //(void) members.Insert(ele_expected_type->GetMemberAt(j)) ;
                            //}
                        //} else if (ele_type && !ele_type->IsTypeParameterType() && !ele_type->IsUnresolvedNameType() &&
                            //ele_type->IsAssignmentCompatible(member_type)) {
                            //(void) members.Insert(ele_expected_type->GetMemberAt(i)) ; // Add to Set
                        //} // else error
                    //} else {
                    // VIPER #5160: Error out the non initialized members that are structure type but not concat item
                    (void) members.Insert(ele_expected_type->GetMemberAt(i)) ; // Add to Set
                    //}
                //}
            }
        } else {
            // VIPER #5248 (9000383278/union_initialisation): Set 'default_label' for union members:
            if (InRelaxedCheckingMode() && ele_expected_type && ele_expected_type->IsUnion() &&
                expr->IsConcatItem() && !expr->GetMemberLabel()) default_label = 1 ;
            ele_type = expr->CheckType(ele_expected_type, environment) ;
        }
        if (!ele_type) ret_status = 0 ; // Some error in elements
        delete ele_type ;
    }

    // VIPER #5160: Error out if all the members of structure is not initialised, for that we check that
    // the structure member name/type of expression and that of structure declaration is different or not
    if (all_concat_item && !default_label && member_ids && (members.Size() != member_ids->Size()) && from) {
        MapIter mi ;
        VeriTypeInfo *member_type = 0 ;
        VeriIdDef *member_id = 0 ;
        // Error out the members of the map those are not in the set
        FOREACH_MAP_ITEM(member_ids, mi, &member_id, &member_type) {
            // For srting data type we do not error out as it is automatiaclly initialized to null
            if (member_type && member_type->IsString()) continue ;
            if (member_id && !members.Get(member_id)) {
                if ((environment != CASTING) && !has_unpacked_arr_struct_mem) from->Error("incomplete structure literal, no value is specified for field %s", member_id ? member_id->GetName() : "<unknown>") ;
                //if ((environment != CASTING)) from->Error("incomplete structure literal, no value is specified for field %s", member_id ? member_id->GetName() : "<unknown>") ;
            }
        }
    }

    if (is_created) delete ele_expected_type ; // Delete copied type
    return ret_status ;
}
VeriTypeInfo *VeriMultiAssignmentPattern::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    VeriTypeInfo *this_type = 0 ;
    if (_type) this_type = _type->CheckType(0, NO_ENV) ; // Get type info. Pass NO_ENV for type. Viper 5323

    // VIPER #3054 : Issue error for illegal context of assignment pattern
    // VIPER #3256 : If target contains dimensions, assignment pattern is legal as value
    if (expected_type && expected_type->IsUnion()) {
        // VIPER #5248/#5301 (test_47): Different error message for target union:
        if (environment != CASTING) Error("illegal assignment of structure literals to union") ;
        return this_type ; // VIPER #7907: error: always return this type
    } else if (!expected_type && !_type && (environment != WRITE) && (environment != INST_WRITE)) {
        // Type of assignment pattern should be determined from context
        if (environment != CASTING) Error("illegal context for %s", "multi-assignment pattern") ;
        return this_type ; // VIPER #7907: error: always return this type
    }
    VeriTypeInfo *passing_type = (this_type) ? this_type: expected_type ;
    if (!CheckMultiAssignmentPatternType(_exprs, passing_type, environment, this)) {
        passing_type = 0 ;
    }

    if (this_type && !this_type->CheckAgainst(expected_type, this, environment)) {
        return this_type ; // VIPER #7907: error: always return this type
    }
    VeriTypeInfo *ret_type = (passing_type) ? passing_type->Copy() : 0 ;
    delete this_type ;
    return ret_type ;
}
// VIPER #7104 : Separate type checking for multi-assignment pattern from assignment
// pattern.
unsigned VeriMultiAssignmentPattern::CheckMultiAssignmentPatternType(const Array *exprs, const VeriTypeInfo *passing_type, veri_type_env environment, const VeriTreeNode *from) {
    if (!passing_type) return 1 ; // Cannot check type without expected type

    // Do not check anything if passing_typee is type parameter type/unresolved name type
    if (passing_type->IsTypeParameterType() || passing_type->IsUnresolvedNameType()) {
        return 1 ;
    }

    // Type infer elements :
    // If expected type contains unpacked dimension, each element will be checked
    // with a type having one less unpacked dimension. Do that
    unsigned ret_status = 1 ;
    VeriTypeInfo *ele_expected_type = 0 ;
    unsigned is_created = 0 ;
    unsigned unpacked_dim = passing_type->UnpackedDimension() ;
    unsigned packed_dim = passing_type->PackedDimension() ;
    // VIPER #7104 : Allow built-in type as target of multi assignment pattern
    if (unpacked_dim || packed_dim || passing_type->IsBuiltInType()) {
        ele_expected_type = passing_type->Copy() ;
        Array idx_arr(1) ;
        idx_arr.InsertLast(0 /* no index expr*/) ;
        ele_expected_type = ele_expected_type->ReduceDimBy(&idx_arr, 0) ;
        is_created = 1 ;
    } else if (passing_type->IsStructure() || passing_type->IsUnion()) {
        // All elements of the structure should have equivalent types to
        // use with multi assignment pattern
        if (!passing_type->HasMembersOfEquivalentTypes()) {
            if (from && (environment != CASTING)) from->Error("all elements of structure are not type equivalent, cannot use multi assignment pattern") ;
        }
        ele_expected_type = passing_type->GetMemberTypeAt(0) ;
    }
    if (!ele_expected_type) { // No element type, illegal for multi-assignment pattern
        if (from && (environment != CASTING)) from->Error("illegal context for %s", "multi-assignment pattern") ;
    }
    unsigned is_target_queue = passing_type->IsQueue() ;
    unsigned i ;
    VeriExpression *expr ;
    VeriTypeInfo *ele_type = 0 ;

    FOREACH_ARRAY_ITEM(exprs, i, expr) {
        if (!expr) continue ;
        // Member of multi-assignment pattern cannot be name/type : expression
        if (expr->IsConcatItem()) {
            if (environment != CASTING) expr->Error("concatenation member label not allowed in multiple assignment pattern") ;
            continue ;
        }
        ele_type = 0 ;
        unsigned processed = 0 ;
        // VIPER #4429: If target is queue, allow queue type as element.
        if (is_target_queue && !expr->IsConcat() && !expr->IsMultiConcat()) {
            ele_type = expr->CheckType(0, environment) ;
            processed = 1 ;
        }
        if (!is_target_queue || !processed || (ele_type && !ele_type->IsQueue())) {
            delete ele_type ;
            ele_type = expr->CheckType(ele_expected_type, environment) ;
        }

        // If context is associative array, 'expr' should be index_type : element value
        // Check index type :
        if (passing_type && passing_type->IsAssociativeArrayType()) {
            if (environment != CASTING) expr->Error("use '{index : value} syntax in associative array literal") ;
        }
        if (!ele_type) ret_status = 0 ; // Some error in elements
        delete ele_type ;
    }

    if (is_created) delete ele_expected_type ; // Delete copied type
    return ret_status ;
}

// VIPER #3206 : Overwrite 'CheckType' for streaming concat as it is not like concatenation
VeriTypeInfo *VeriStreamingConcat::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    //if (expected_type && (expected_type->IsTypeParameterType() || expected_type->IsUnresolvedNameType())) {
    //}
    // Streaming concatenation can be used both as lhs and rhs. In either case
    // expected type should be bit-stream type
    if (expected_type && !expected_type->IsBitStreamType() &&
         !expected_type->IsTypeParameterType() && !expected_type->IsUnresolvedNameType()) {
        Error("illegal operand with streaming operator") ;
        return 0 ;
    }
    // Streaming concatenation can operate directly on integral types and streams.
    // Check whether each element is of bit-stream type.
    unsigned i ;
    VeriExpression *expr ;
    VeriTypeInfo *expr_type = 0 ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        expr_type = expr->CheckType(0, environment) ;
        // VIPER #4646: Produce error if each element type is resolved, but not
        // of bit-stream type.
        if (expr_type && !expr_type->IsTypeParameterType() && !expr_type->IsUnresolvedNameType() && !expr_type->IsBitStreamType()) {
            expr->Error("illegal operand with streaming operator") ;
        }
        if (!expr_type) {
            // Viper 7112: If the expt_type is NULL and if the argument is assignment pattern we error out.
            // Assignment pattern does not have a self determined type and hence cannot be an argument of
            // a VeriStreamingConcat which requires BitStreamType. However, streaming concat itself also
            // is a non self determinable type but it can be used as an argument of a streaming concat.
            // However all the simulators and the LRM also seems to allow that type of argument passing
            // Hence do a parse tree type checking instead of null type checking.
            if (expr->IsAssignPattern() || expr->IsMultiAssignPattern()) Error("illegal operand with streaming operator") ;
        }
        delete expr_type ;
    }
    return (expected_type) ? expected_type->Copy() : 0 ; // Return coming type
    // Create any bit-stream type when streaming expression is used for packed and return
    //VeriTypeInfo *this_type = (environment != WRITE) ? new VeriArrayType(0, 1): 0 ;
    //if (this_type && this_type->CheckAgainst(expected_type, this, environment)) return this_type ;
    //return 0 ;
}
VeriTypeInfo *VeriCondPredicate::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // Set data type to pattern identifiers from context type
    VeriTypeInfo *ltype = (_left) ? _left->CheckType(expected_type, environment): 0 ;
    delete ltype ;
    VeriTypeInfo *type = 0 ;
    // VIPER #3039 : Do not pass coming data type information to type check _right
    // It will be simple filter expression (pattern matching case statement) or
    // condition pattern like 'expr matches patern'. So _right does not depend on context type.
    if (_right) type = _right->CheckType(0, environment) ;
    return type ; // VIPER #7907: always return this type
}
VeriTypeInfo *VeriDotName::CheckType(VeriTypeInfo *expected_type, veri_type_env /*environment*/)
{
    // Get pattern identifier
    VeriIdDef *pattern_id = _name ? _name->FullId() : 0 ;

    // Set target type to pattern identifier
    if (pattern_id && expected_type) {
        VeriDataType *packed_type = expected_type->ToDataType(Linefile()) ;
        pattern_id->SetDataType(packed_type) ; // FIXME :Data type is not owned by identifier
    }
    return 0 ;
}
VeriTypeInfo *VeriTaggedUnion::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    if (!expected_type) return 0 ; // No valid target : Error

    VeriTypeInfo *ret_type = 0 ;
    VeriTypeInfo *member_type = 0 ;
    // The type of tagged union must be known from context
    // _member is reference to a member of tagged union. So base_type should be
    // tagged union. We need to resolve _member from base_type
    if (expected_type->IsUnion() && expected_type->IsTagged() && _member) {
        VeriIdDef *member_id = expected_type->GetMember(_member->GetName()) ; // Get member from tagged union
        if (member_id) { // Member is present in tagged union
            _member->SetId(member_id) ; // Set member identifier
            // Resolve member value with member type
            member_type =  expected_type->GetMemberType(_member->GetName()) ;
            if (_expr) ret_type = _expr->CheckType(member_type, environment) ;
            delete ret_type ;
        } else { // _member is not a member of target tagged union
            _member->Error("%s is not declared", _member->GetName()) ;
        }

    } else if (!expected_type->IsTypeParameterType() && !expected_type->IsUnresolvedNameType()) {
        // VIPER #3054 : Issue error for illegal context of tagged union
        Error("illegal context for %s", "tagged union") ;
        return 0 ;
    }
    return expected_type->Copy() ; // Return tagged union type itself
}
VeriTypeInfo *VeriWithExpr::CheckType(VeriTypeInfo *expected_type, veri_type_env environment)
{
    // It can be array method call or stream expression. For stream expression
    // _right can only be VeriRange
    if (_right && _right->IsRange()) { // It is stream expression:
        // _left expression should be one dimensional unpacked array
        // Get the type of left
        VeriTypeInfo *left_type = (_left) ? _left->CheckType(expected_type, environment) : 0 ;
        if (_left && left_type && (left_type->UnpackedDimension() != 1) &&
             !left_type->IsTypeParameterType() && !left_type->IsUnresolvedNameType()) { // Type not from type parameter/unresolved
            _left->Error("illegal stream expression") ;
        }
        return left_type ;
    } else if (_right && _left) { //Viper 4387. Viper 5327, 5314. Function call may not have braces
        // VIPER #5660: Section 5.15.3 LRM 1800-2005: The array reduction methods returns a single
        // value of the same type as the array element type or, if specified, returns the type of
        // the expression in the with clause. LRM says that for array reduction methods the type
        // of whole expression is the type of the expression in with clause, but for other methods
        // expression type is the array element type.

        unsigned left_func_type = _left->GetFunctionType() ;
        switch(left_func_type) {
        case VERI_METHOD_AND     :  // array.and(prefix=item) with item
        case VERI_METHOD_OR      :  // array.or
        case VERI_METHOD_XOR     :  // array.xor
        case VERI_METHOD_SUM     :
        case VERI_METHOD_PRODUCT :
        {
            VeriTypeInfo *return_type = _right->CheckType(expected_type, environment) ;
            if (return_type && return_type->CheckAgainst(expected_type, this, environment)) return return_type ;
            return return_type ; // VIPER #7907
        }
        default                  :
        {
            VeriTypeInfo *return_type = _left->CheckType(expected_type, environment) ;
            if (return_type && return_type->CheckAgainst(expected_type, this, environment)) return return_type ;
            return return_type ; // VIPER #7907
        }
        }
    }
    return 0 ;
}
VeriTypeInfo *VeriRange::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  environment)
{
    if (environment != VALUE_RANGE_LIST) return 0 ;
    // VIPER #6894: This is value range of set membership operator, operands should be of
    // singular type
    VeriTypeInfo *left_type = (_left) ? _left->CheckType(0, environment): 0 ;
    VeriTypeInfo *right_type = (_right) ? _right->CheckType(0, environment): 0 ;
    if (left_type && (left_type->IsTypeParameterType() || left_type->IsUnresolvedNameType())) {
        delete left_type ; left_type = 0 ;
    }
    if (right_type && (right_type->IsTypeParameterType() || right_type->IsUnresolvedNameType())) {
        delete right_type ; right_type = 0 ;
    }
    if ((left_type && !left_type->IsSingularType()) || (right_type && !right_type->IsSingularType())) {
        Error("bad operand to set membership operator") ;
    }

    delete left_type ; // fix mem leak
    delete right_type ;
    return 0 ;
}
VeriTypeInfo *VeriAnsiPortDecl::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriInlineConstraint::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriForeachOperator::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriOpenRangeBinValue::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriTransBinValue::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriDefaultBinValue::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriSelectBinValue::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriTransSet::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriTransRangeList::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriSelectCondition::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriTimingCheckEvent::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriPathPulseVal::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriDotStar::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriIfOperator::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriSequenceConcat::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriClockedSequence::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriAssignInSequence::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriDistOperator::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriSolveBefore::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriCaseOperator::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env  /*environment*/) { return 0 ; }
VeriTypeInfo *VeriDataType::CheckType(VeriTypeInfo * /*expected_type*/, veri_type_env /*environment*/)
{
    return CreateType(0) ;
}

unsigned VeriExpression::IsConstRange() const
{
    VeriIdDef *id = FullId() ;
    VeriRange *range = id ? id->GetDimensions() : 0;
    if (!range) return 0 ;
    while (range) {
        VeriExpression *left = range->GetLeft() ;
        VeriExpression *right = range->GetRight() ;
        if (left && !left->IsConst()) return 0 ;
        if (right && !right->IsConst()) return 0 ;
        range = range->GetNext() ;
    }
    return 1 ;
}

// VIPER #7581: Recursive routine to check for hirename
unsigned VeriBinaryOperator::IsHierarchicalName() const
{
    if (_left && _left->IsHierarchicalName()) return 1 ; // check left operand
    return (_right) ? _right->IsHierarchicalName() : 1 ; // check right operand
}

unsigned VeriFunctionCall::IsHierarchicalName() const
{
    if (_func_name && _func_name->IsHierarchicalName()) return 1 ;
    return 0 ;
}

unsigned VeriSelectedName::IsHierarchicalName() const
{
    if (IsHierName()) return 1 ;
    return 0 ;
}

void VeriExpression::CheckArraySize(VeriExpression* value)
{
    if (!value || (value->IsConcat() || value->IsMultiConcat() || value->IsAssignPattern() || value->IsMultiAssignPattern())) return ;
    if (IsStaticElab()) {
        // VIPER 4390. Check Size of Part Array SIze defined by part select
        value->CheckForZeroOrNegArrayWidth() ;
    }

    VeriTypeInfo *target_type = CheckType(0, WRITE) ;

    if (target_type && !target_type->IsFixedArrayType() && (target_type->IsTypeParameterType() || target_type->IsUnresolvedNameType())) {
        delete target_type ;
        return ;
    }
    delete target_type ;
    VeriConstraint *target_constraint = EvaluateConstraintInternal(0, 0) ;
    if (target_constraint && target_constraint->IsPacked()) {
        delete target_constraint ;
        return ;
    }
    VeriConstraint *from_constraint = value->EvaluateConstraintInternal(0, 0) ;
    if (from_constraint && from_constraint->IsPacked()) {
        delete target_constraint ;
        delete from_constraint ;
        return ;
    }
    VeriConstraint *target_index_constraint = target_constraint ? target_constraint->IndexConstraint() : 0 ;
    VeriConstraint *from_index_constraint = from_constraint ? from_constraint->IndexConstraint() : 0 ;
    VeriConstraint *target_element_constraint = target_constraint ? target_constraint->ElementConstraint() : 0 ;
    VeriConstraint *from_element_constraint = from_constraint ? from_constraint->ElementConstraint() : 0 ;
    if (target_index_constraint && target_element_constraint && from_index_constraint && from_element_constraint) {
        if ((target_index_constraint->NumOfElements() != from_index_constraint->NumOfElements()) && !target_index_constraint->IsDynamicRange() && !from_index_constraint->IsDynamicRange()) {
            Error("array element widths (%d, %d) don't match", target_index_constraint->NumOfElements(), from_index_constraint->NumOfElements()) ;
        }
    }
    if (IsStaticElab()) {
        // VIPER: 4362 In static elaborator check for sizes on right expr
        // of a range for partselecttoken
        if (!from_constraint && target_index_constraint && target_element_constraint) {
            VeriExpression *from_range = value->GetIndexExpr() ;
            if (from_range && from_range->GetPartSelectToken()) {
                VeriExpression *from_range_right = from_range->GetRight() ;
                VeriBaseValue *from_range_right_value = from_range_right ? from_range_right->StaticEvaluateInternal(0, 0, 0, 0) : 0 ;
                int size = from_range_right_value ? from_range_right_value->GetIntegerValue() : 0 ;
                if (from_range_right_value && (from_range_right_value->Type() != REAL) && !from_range_right_value->HasXZ() && size) {
                    if ((unsigned)size != target_index_constraint->NumOfElements()) {
                        Error("array element widths (%d, %d) don't match", target_index_constraint->NumOfElements(), size) ;
                    }
                }
                delete from_range_right_value ; // Memory leak fix
            }
        }
        if (!target_constraint && from_index_constraint && from_element_constraint) {
            VeriExpression *target_range = GetIndexExpr() ;
            if (target_range && target_range->GetPartSelectToken()) {
                VeriExpression *target_range_right = target_range->GetRight() ;
                VeriBaseValue *target_range_right_value = target_range_right ? target_range_right->StaticEvaluateInternal(0, 0, 0, 0) : 0 ;
                int size = target_range_right_value ? target_range_right_value->GetIntegerValue() : 0 ;
                if (target_range_right_value && (target_range_right_value->Type() != REAL) && !target_range_right_value->HasXZ() && size) {
                    if ((unsigned)size != from_index_constraint->NumOfElements()) {
                        Error("array element widths (%d, %d) don't match", from_index_constraint->NumOfElements(), size) ;
                    }
                }
            }
        }
    }
    delete (target_constraint) ;
    delete (from_constraint) ;
}

void VeriExpression::CheckConstRangeArraySize(VeriExpression* value)
{
    if (!value) return ;
    VeriTypeInfo *target_type = CheckType(0, WRITE) ;
    if (target_type && !target_type->IsFixedArrayType() && (target_type->IsTypeParameterType() || target_type->IsUnresolvedNameType())) {
        delete target_type ;
        return ;
    }
    delete target_type ;
    VeriConstraint *target_constraint = EvaluateConstraintInternal(0, 0) ;
    if (target_constraint && target_constraint->IsPacked()) {
        delete target_constraint ;
        return ;
    }
    VeriConstraint *from_constraint = value->EvaluateConstraintInternal(0, 0) ;
    if (from_constraint && from_constraint->IsPacked()) {
        delete target_constraint ;
        delete from_constraint ;
        return ;
    }
    delete (target_constraint) ;
    delete (from_constraint) ;
    VeriIdDef *this_id = FullId() ;
    VeriRange *range = this_id ? this_id->GetDimensions() : 0;
    if (!range) return ;
    int this_size = 1 ;
    while (range) {
        int curr_size = 0 ;
        VeriExpression *left = range->GetLeft() ;
        VeriExpression *right = range->GetRight() ;
        if (left && right) {
            int left_bound = left->Integer() ;
            int right_bound = right->Integer() ;
            curr_size = (left_bound > right_bound) ? (left_bound - right_bound) + 1 : (right_bound - left_bound) + 1 ;
        } else {
            if (left) {
                curr_size = left->Integer() ;
            } else if (right) {
                curr_size = right->Integer() ;
            } else {
                return ;
            }
        }
        this_size *= curr_size ;
        range = range->GetNext() ;
    }
    VeriIdDef *value_id = value->FullId() ;
    range = value_id ? value_id->GetDimensions() : 0;
    if (!range) return ;
    int value_size = 1 ;
    while (range) {
        int curr_size = 0 ;
        VeriExpression *left = range->GetLeft() ;
        VeriExpression *right = range->GetRight() ;
        if (left && right) {
            int left_bound = left->Integer() ;
            int right_bound = right->Integer() ;
            curr_size = (left_bound > right_bound) ? (left_bound - right_bound) + 1 : (right_bound - left_bound) + 1 ;
        } else {
            if (left) {
                curr_size = left->Integer() ;
            } else if (right) {
                curr_size = right->Integer() ;
            } else {
                return ;
            }
        }

        value_size *= curr_size ;
        range = range->GetNext() ;
    }
    if (value_size != this_size) Error("array element widths (%d, %d) don't match", this_size, value_size) ;
}

/******************************************************************************
*                     Check assignment context                                *
*******************************************************************************/
// Routine to check type in assignment like context
void VeriExpression::CheckAssignmentContext(VeriExpression *value, veri_type_env environment)
{
    // Get the type of the target
    VeriTypeInfo *target_type = CheckType(0, ((environment == CASE_CONDITION) ? NO_ENV:WRITE)) ;
    // Do not check further if target type is type parameter type or unresolved type
    if (target_type && (target_type->IsTypeParameterType() || target_type->IsUnresolvedNameType())) {
        delete target_type ;
        return ;
    }
    // VIPER #3033 : Type check should not be performed if either condition or
    // item condition is enumeration type. LRM says that enum type should be type
    // checked in relational operator, but other tools does not check type for
    // enum in relational/euality operators.
    // VIPER #5926 : Type check should not be performed if either condition or
    // item condition is type operator or $typeof system function
    if ((environment == CASE_CONDITION) && ((target_type && target_type->IsEnumeratedType())
           || (IsTypeOperator() || (GetFunctionType() == VERI_SYS_CALL_TYPEOF) ||
            (value && (value->IsTypeOperator() || (value->GetFunctionType() == VERI_SYS_CALL_TYPEOF))))
           )) {
        delete target_type ;
        return ;
    }

    // Try to type infer value with target type
    veri_type_env value_env = NO_ENV ;
    switch (environment) {
    case INIT_VAL : value_env = INIT_VAL ; break ;
    case PROP_SEQ_INIT_VAL : value_env = PROP_SEQ_INIT_VAL ; break ;
    case PARAM_INIT : value_env = PARAM_INIT ; break ;
    case ASSOC : value_env = READ ; break ;
    case CASE_CONDITION : value_env = CASE_CONDITION ; break ;
    case RETURN_EXPR : value_env = RETURN_EXPR ; break ;
    default : break ;
    }
    // VIPER #6454 : Mark the target parameter as processing before checking
    // type of its expression to determine whether parameter value depends on itself
    VeriIdDef *this_id = GetId() ;
    // VIPER #6503 : Set target parameter as processing only for parameter declaration
    if (this_id && this_id->IsParam() && (environment == PARAM_INIT)) this_id->SetIsProcessing() ;

    // VIPER #3786 : Check if target is virtual interface type, check type of value
    // with different environment so that instance identifier can only be allowed.
    if (target_type && target_type->IsVirtualInterface()) value_env = INTERFACE_TYPE ;
    VeriTypeInfo *value_type = value ? value->CheckType(target_type, value_env): 0 ;

    if (this_id && this_id->IsParam()) this_id->ClearIsProcessing() ; // VIPER #6454

    // If target type is null and value type not null, check type of target again
    if (!target_type && value_type && !value_type->IsTypeParameterType() && !value_type->IsUnresolvedNameType()) target_type = CheckType(value_type, WRITE) ;

    delete target_type ;
    delete value_type ;
}
void VeriExpression::CheckAssignment(VeriExpression *value, unsigned context)
{
    // Get the type of the target
    VeriTypeInfo *target_type = CheckType(0, WRITE) ;
    // Do not check further if target type is type parameter type or unresolved type
    if (target_type && (target_type->IsTypeParameterType() || target_type->IsUnresolvedNameType())) {
        delete target_type ;
        return ;
    }
    if (!value) { // Increment/decrement operator
        unsigned increment = 0 ;
        switch (context) {
        case VERI_INC_OP :
             increment = 1 ;

        case VERI_DEC_OP :
            // Operands can be of type integral/real/shortreal
            // Enum data type cannot be used here (VIPER #3296)
            if (target_type && target_type->IsEnumeratedType()) {
                Error("enum data type cannot be used with %s operator", (increment) ? "++" : "--") ;
            } else if (target_type && !target_type->IsIntegralType() && !target_type->IsRealType()) {
                Error("illegal operand for operator %s", (increment) ? "++" : "--") ;
            }
            delete target_type ; // Delete target type
            return ; // Checking complete, return
        default :
            break ;
        }
    }

    veri_type_env value_env =  READ ;
    // VIPER #3786 : Check if target is virtual interface type, check type of value
    // with different environment so that instance identifier can only be allowed.
    if (target_type && target_type->IsVirtualInterface()) value_env = INTERFACE_TYPE ;
    VeriTypeInfo *value_type = value ? value->CheckType(target_type, value_env): 0 ;
    if (!value_type || value_type->IsTypeParameterType() || value_type->IsUnresolvedNameType()) {
        delete target_type ;
        delete value_type ;
        return ;
    }
    // If target type is null and value type not null, check type of target again
    if (!target_type) target_type = CheckType(value_type, WRITE) ;

    //if (IsConstRange() && value && value->IsConstRange()) CheckConstRangeArraySize(value) ; // This check is done by CheckUnpackedArraySize

    // Check some assignment specific semantics :
    switch (context) {
    case VERI_ASSIGN : // Continuous assignment
        // Chandle type cannot be used as lvalue in continuous assignment
        if (target_type && target_type->IsChandleType()) {
            Error("illegal reference to chandle type") ;
        }
        if (!IsSystemVeri() && target_type) {
            // In verilog 95/2k mode register types cannot assigned in continuous assignment
            VeriTypeInfo *base_type = target_type->BaseType() ;
            if (base_type && base_type->IsRegisterType()) {
                if (GetName()) {
                    Error("concurrent assignment to a non-net %s is not permitted", GetName()) ;
                } else {
                    Error("illegal expression in target") ;
                }
            }
        }
        break ;
    case VERI_EQUAL : // Procedural continuous assignment
        break ;
    case VERI_LEQ : // Non-blocking assignment
    case VERI_EQUAL_ASSIGN : // Blocking assignment
        if (target_type) {
            // VIPER #3315 : net types cannot assigned in blocking assignment
            VeriTypeInfo *base_type = target_type->BaseType() ;
            //if (base_type && base_type->IsNetType() && !IsSystemVeri()) { // SV excluded for VIPER #4711/#4729/#6570
            if (base_type && base_type->IsNetType()) { // VIPER #8077: Remove the guard IsSystemVeri()
                if (GetName()) {
                    Error("procedural assignment to a non-register %s is not permitted", GetName()) ;
                } else {
                    Error("illegal expression in target") ;
                }
            }
        }
        if (!IsSystemVeri()) { // Viper #5159 : Produce error if target/value is unpacked in non-system verilog mode
            if ((target_type && target_type->IsUnpackedArrayType()) || (value_type->IsUnpackedArrayType())) {
                Error("unpacked value/target cannot be used in assignment") ;
            }
        }
        break ;
    case VERI_PLUS_ASSIGN :
    case VERI_MIN_ASSIGN :
    case VERI_MUL_ASSIGN :
    case VERI_DIV_ASSIGN :
        // Operands can be of type integral/real/shortreal
        // Enum data type cannot be used here
        if (target_type && target_type->IsEnumeratedType()) {
            Error("invalid operator %s in assignment to enum", PrintToken(context)) ;
        } else if ((target_type && !target_type->IsIntegralType() && !target_type->IsRealType() && !target_type->IsTypeParameterType() && !target_type->IsUnresolvedNameType()) ||
            (!value_type->IsIntegralType() && !value_type->IsRealType() && !value_type->IsTypeParameterType() && !value_type->IsUnresolvedNameType())) {
            Error("illegal operand for operator %s", PrintToken(context)) ;
        }
        break ;
    case VERI_MOD_ASSIGN :
    case VERI_AND_ASSIGN :
    case VERI_OR_ASSIGN :
    case VERI_XOR_ASSIGN :
    case VERI_LSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
        // Operands can be of type integral
        // Enum data type cannot be used here
        if (target_type && target_type->IsEnumeratedType()) {
            Error("invalid operator %s in assignment to enum", PrintToken(context)) ;
        } else if ((target_type && target_type->IsRealType()) || value_type->IsRealType()) {
            Error("illegal context for real expression") ;
        } else if ((target_type && !target_type->IsIntegralType() && !target_type->IsTypeParameterType() && !target_type->IsUnresolvedNameType()) || (!value_type->IsIntegralType() && !value_type->IsTypeParameterType() && !value_type->IsUnresolvedNameType())) {
            Error("illegal operand for operator %s", PrintToken(context)) ;
        }
        break ;
    default :
        break ;
    }
    CheckUnpackedArrayAssignment(value) ;
    //CheckUnpackedArraySize(value->FullId()) ;
    // Viper 4609: Added call to check Packed Dimension
    //CheckPackedArraySize(value->FullId()) ;
    delete target_type ;
    delete value_type ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// Viper 6397: Check for illegal connection of ports across language
void VeriExpression::CheckAssociationVhdl(const VhdlIdDef *formal, veri_type_env context)
{
    if (!formal) return ;
    // Viper 6896 if the type is from Verilog Package use that type itself
    VhdlIdDef* formal_type_id = formal->Type() ;
    if (formal_type_id && formal_type_id->IsVerilogType()) {
        VeriTypeRef type_ref(formal_type_id->GetVerilogTypeFromPackage(),0 ,0) ;
        char *id_name = Strings::save(formal->Name()) ;
        VeriVariable v(id_name) ;
        v.SetDataType(&type_ref) ;
        (void) CheckAssociation(&v, context) ;
        return ;
    }

    VeriTypeInfo *value_type = CheckType(0, READ) ;
    // VIPER #4353: Check the base type not the full type to see if it is a net type object:
    VeriTypeInfo *value_base_type = (value_type) ? value_type->BaseType() : 0 ;
    // VIPER #4255: For instances, check if inout type ports are connected to non-net variables:
    if ((context == INSTANCE) && formal->IsInout() && value_base_type && !value_base_type->IsNetType() &&
         !value_base_type->IsTypeParameterType() && !value_base_type->IsUnresolvedNameType()) {
        // This is not-legal according to section 19.12.2 of IEEE 1800 LRM, error out:
        // VIPER #5602 : Produce error for name reference of non-net variables.
        // For other expressions produce warning. This is to be done in
        // relaxed checking mode :
        if (InRelaxedCheckingMode()) {
            if (GetId() || IsHierName()) {
                Error("non-net variable cannot be connected to inout port %s", formal->Name()) ;
            } else {
                Warning("non-net variable cannot be connected to inout port %s", formal->Name()) ;
            }
        } else {
            Error("non-net variable cannot be connected to inout port %s", formal->Name()) ;
        }
    }
    delete value_type ;
}
#endif

unsigned VeriExpression::CheckAssociation(VeriIdDef *formal, veri_type_env context)
{
    if (!formal) return 0 ; // Can't do anything without formal

    unsigned error_given = 0 ;
    // Assignment pattern cannnot be used as actual to ref port of subprogram
    // Direction VERI_CONST means VERI_CONST VERI_REF
    if (((formal->Dir() == VERI_REF) || (formal->Dir() == VERI_CONST)) && IsAssignPattern() && !(context == INSTANCE)) {
        Error("assignment pattern is illegal for use with %s port %s", "ref", formal->Name()) ;
        error_given = 1 ;
    }
    VeriTypeInfo *target_type = 0 ;
    VeriTypeInfo *value_type = 0 ;
    if (formal->IsOutput() && !formal->IsInterfacePort()) {
        // For output port we should check the type of formal in the context of actual
        target_type = CheckType(0, WRITE) ; // // Get the type of the target
        value_type = formal->CreateType(0) ;
    } else { // Input, inout port
        target_type = formal->CreateType(0) ;
        // VIPER #4037 :  Do not check further if target type is type parameter type or unresolved type
        if (target_type && (target_type->IsTypeParameterType() || target_type->IsUnresolvedNameType())) {
            delete target_type ;
            return error_given ;
        }
        // VIPER #3397 : Pass special environment for interface port, as its actual can be instance
        // VIPER #7092 : Special environment to indicate READING for actual/formal association of instance
        veri_type_env value_env =  (context == INSTANCE) ? INST_READ: READ ;
        // VIPER #5526: Rely on 'IsInterfacePort' in presence of valid formal type.
        // IsInterfacePort returns 1 for any unresolved named type :
        if (formal->IsInterfacePort() /*&& target_type*/) value_env = READ_INSTANCE ;
        if (formal->IsVirtualInterface()) value_env = INTERFACE_TYPE ;

        if (target_type && target_type->IsVirtualInterface()) value_env = INTERFACE_TYPE ;
        if (context == PROP_SEQ) value_env = PROP_SEQ_INIT_VAL ;

        value_type = CheckType(target_type, value_env) ;
        // VIPER #4353: Check the base type not the full type to see if it is a net type object:
        VeriTypeInfo *value_base_type = (value_type) ? value_type->BaseType() : 0 ;
        // VIPER #4255: For instances, check if inout type ports are connected to non-net variables:
        if ((context == INSTANCE) && (formal->Dir() == VERI_INOUT) && !formal->IsInterfacePort() && value_base_type &&
            !value_base_type->IsNetType() && !value_base_type->IsTypeParameterType() && !value_base_type->IsUnresolvedNameType()) {
            // This is not-legal according to section 19.12.2 of IEEE 1800 LRM, error out:
            // VIPER #5602 : Produce error for name reference of non-net variables.
            // For other expressions produce warning. This is to be done in
            // relaxed checking mode :
            if (InRelaxedCheckingMode()) {
                if (GetId() || IsHierName()) {
                    Error("non-net variable cannot be connected to inout port %s", formal->Name()) ;
                } else {
                    Warning("non-net variable cannot be connected to inout port %s", formal->Name()) ;
                }
            } else {
                Error("non-net variable cannot be connected to inout port %s", formal->Name()) ;
            }
            error_given = 1 ;
        }
    }
    // Do not check further if any type is type parameter type or unresolved type
    if ((target_type && (target_type->IsTypeParameterType() || target_type->IsUnresolvedNameType()))
        || (!value_type || value_type->IsTypeParameterType() || value_type->IsUnresolvedNameType())) {
        delete target_type ;
        delete value_type ;
        return error_given ;
    }
    // If target type is null and value type not null, check type of target again
    if (!target_type && formal->IsOutput()) target_type = CheckType(value_type, (context == INSTANCE) ? INST_WRITE: WRITE) ;

    // For ref formal actual and formal must be of equivalent types
    // VIPER #5841 : If dir is VERI_CONST, it is const ref type argument
    if ((formal->Dir() == VERI_REF) || (formal->Dir() == VERI_CONST)) {
        if (!value_type->IsEquivalent(target_type, 0, 0, 0)) {
            Error("actual and formal for a ref must be equivalent types") ;
            error_given = 1 ;
        } else if (((context == TASK) || (context == FUNC) || (context == PROP_SEQ))) {
            if (value_type->IsNetType()) {
                // VIPER #4906: Error out if net type actual is connected to ref-type function/task port:
                Error("illegal actual value for ref port %s for %s", formal->Name(), ((context==TASK)?"task":"function")) ;
                error_given = 1 ;
            } else {
                // Viper 4832, 4326: IsEquivalent cannot check for size of datatypes we can check those if they are constant
                VeriIdDef *this_id = FullId() ;
                VeriDataType *formal_datatype = formal->GetDataType() ;
                VeriDataType *this_datatype = this_id ? this_id->GetDataType() : 0 ;
                // VIPER #5248 (test_2): We only want to check packed size, since type-refs
                // can possibly have unpacked dimensions, we have to get to the non-type-refs:
                // FIXME: We should have check full size of 'this' with size of formal id,
                // but that could lead to issues with dynamic types/dimensions in unpacked side.
                // So, for now, check only the size of the packed side of both formal and actual.
                if (formal_datatype) formal_datatype = formal_datatype->GetBaseDataType() ;
                if (this_datatype) this_datatype = this_datatype->GetBaseDataType() ;
                if (formal_datatype && this_datatype) {
                    verific_int64 length = this_datatype->StaticSizeSignInternal(0, 0) ;
                    verific_uint64 this_length = (verific_uint64)ABS(length) ;
                    length = formal_datatype->StaticSizeSignInternal(0, 0) ;
                    verific_uint64 formal_length = (verific_uint64)ABS(length) ;
                    if (formal_length != this_length)  Error("actual and formal for a ref must be equivalent types") ;
                }
            }
        }
    } else {
        (void) value_type->CheckAgainst(target_type, this, (context==PROP_SEQ) ? PROP_SEQ_INIT_VAL:((context == INSTANCE) ? INST_READ: READ)) ; // Check value against target type
    }
    // Viper 4832, 4813, 4619 Check Constant Array widths in formal to actual
    // Check for unpacked dimensions only.
    if (context == FUNC || context == TASK) {
        VeriIdRef formal_exp(formal) ;
        CheckUnpackedArrayAssignment(&formal_exp) ;
        //CheckUnpackedArraySize(formal) ;
        //CheckPackedArraySize(formal) ; // Check for packed dim Viper 4813
    }
    delete target_type ;
    delete value_type ;
    return error_given ;
}
void VeriExpression::CheckUnpackedArrayAssignment(const VeriExpression *against) const
{
    if (!against) return ;
    VeriIdDef *formal = against->FullId() ;

    // Viper 4832 check for unpacked dimensions if the ranges are constant
    // Check unpacked dimensions
    VeriIdDef *this_id = FullId() ;

    if (!formal) {
        if (!this_id) return ;
        // Viper 5981: For assignments like struct_var = concat; We need to unfold the ids of the structure
        // and the expressions of the concat. Then recursively call CheckUnpackedArrayAssignment with an id of
        // struct with a corresponding expression of the sequence.
        VeriDataType *this_data_type = this_id->GetDataType() ;
        if (this_data_type && (this_data_type->IsStructType() || this_data_type->IsUnionType()) && against->IsConcat()) {
             Array *concat_exprs = against->GetExpressions() ;
             if (!concat_exprs || !concat_exprs->Size()) return ;
             unsigned k = 0 ;
             unsigned i ;
             VeriDataDecl *data_decl ;
             Array *decls = this_data_type->GetDecls() ;
             FOREACH_ARRAY_ITEM(decls, i, data_decl) {
                 if (!data_decl) continue ;
                 Array *ids = data_decl->GetIds() ;
                 unsigned j ;
                 VeriIdDef *id ;
                 FOREACH_ARRAY_ITEM(ids, j, id) {
                     if (!id) continue ;
                     VeriIdRef id_ref(id) ;
                     id_ref.SetLinefile(Linefile()) ; // VIPER #6075: Set my linefile to this id-ref
                     // Only pick if there is enough expressions in concat_expr
                     VeriExpression *expr = k < concat_exprs->Size() ? (VeriExpression*)concat_exprs->At(k) : 0 ;
                     k++ ;
                     if (!expr) continue ;
                     id_ref.CheckUnpackedArrayAssignment(expr) ;
                 }
             }
        }
        return ;
    }

    if (!this_id) return ; // Cannot do anything without resolved identifier
    unsigned this_unpacked_dim_count =  this_id->UnPackedDimension() ;
    unsigned formal_unpacked_dim_count = formal->UnPackedDimension() ;

    if (this_unpacked_dim_count != formal_unpacked_dim_count) {
        Error("incompatible unpacked dimensions in assignment") ;
        return ;
    }
    if (!this_unpacked_dim_count) return ; // No unpacked dimensions, cannot check any thing
    unsigned i ;
    unsigned this_unpacked_size = 1, formal_unpacked_size = 1 ;
    VeriRange *this_range = 0, *formal_range = 0 ;
    for(i=0; i < this_unpacked_dim_count; i++) {
        this_range = this_id->GetDimensionAt(i) ;
        formal_range = formal->GetDimensionAt(i) ;
        unsigned formal_size = formal_range ? formal_range->GetUnpackedWidth(): 0 ;
        unsigned this_size = this_range ? this_range->GetUnpackedWidth(): 0 ;
        // Viper 5163 if formal_size and this_size are both greater than zero check for size mismatch
        if (formal_size && this_size && formal_size != this_size) Error("array element widths (%d, %d) don't match", this_size, formal_size) ;
        this_unpacked_size = this_unpacked_size * this_size ;
        formal_unpacked_size = formal_unpacked_size * formal_size ;
    }
    if (this_unpacked_size != formal_unpacked_size) return ; // Already error is produced, no need to check packed size
    if (!this_unpacked_size || !formal_unpacked_size) return ; // Cannot evaluate dimensions
    // Viper 4813.
    // compare the packed dimentions of the formal with the actual this
    // VIPER #5644 : Use only size of formal/actual instead of data type to produce error
    // for array element width mismatch
    verific_int64 this_size_sign = this_id->StaticSizeSignInternal(0, 0) ;
    verific_uint64 this_size = (verific_uint64)ABS(this_size_sign) ;

    verific_int64 formal_size_sign = formal->StaticSizeSignInternal(0, 0) ;
    verific_uint64 formal_size = (verific_uint64)ABS(formal_size_sign) ;

    verific_uint64 this_packed_size = this_size/this_unpacked_size ;

    verific_uint64 formal_packed_size = formal_size/this_unpacked_size ;
    // Viper 6048: Only error if evaluation is successful. If the evaluation is successful then
    // the size will always be evaluated as a non zero constant.
    if (formal_packed_size && this_packed_size && this_packed_size != formal_packed_size) Error("array element widths (%d, %d) don't match", (unsigned)this_packed_size, (unsigned)formal_packed_size) ;
}
/*
void VeriExpression::CheckUnpackedArraySize(const VeriIdDef *formal) const
{
    if (!formal) return ;
    // Viper 4832 check for unpacked dimensions if the ranges are constant
    // Check unpacked dimensions
    VeriIdDef *this_id = FullId() ;
    if (this_id && this_id->UnPackedDimension() && formal->UnPackedDimension()) {
        VeriRange *formal_dim = formal->GetDimensions() ;
        if (!formal_dim) {
            // Get the dimension from DataTypeId
            VeriDataType *data_type = formal->GetDataType() ;
            VeriIdDef *data_type_id = data_type ? data_type->GetId() : 0 ;
            formal_dim = data_type_id ? data_type_id->GetDimensions() : 0 ;
        }

        VeriRange *this_dim = this_id ? this_id->GetDimensions() : 0 ;
        if (!this_dim) {
            // Get the dimension from DataTypeId
            VeriDataType *data_type = this_id->GetDataType() ;
            VeriIdDef *data_type_id = data_type ? data_type->GetId() : 0 ;
            this_dim = data_type_id ? data_type_id->GetDimensions() : 0 ;
        }

        while (formal_dim && this_dim) {
            unsigned formal_size = formal_dim->GetUnpackedWidth() ;
            unsigned this_size = this_dim->GetUnpackedWidth() ;

            this_dim = this_dim->GetNext() ;
            formal_dim = formal_dim->GetNext() ;
            // Viper 5163 if formal_size and this_size are both greater than zero check for size mismatch
            if (formal_size && this_size && formal_size != this_size) Error("array element widths (%d, %d) don't match", this_size, formal_size) ;
        }
    }
}

void VeriExpression::CheckPackedArraySize(const VeriIdDef *formal) const
{
    // Viper 4813.
    // compare the packed dimentions of the formal with the actual this
    if (!formal) return ;
    VeriIdDef *this_id = FullId() ;
    if (!this_id) return ;
    // arrays with packed dimension only can be associated
    if (!this_id->UnPackedDimension() && !formal->UnPackedDimension()) return ;
    unsigned this_length = 1 ;
    unsigned formal_length = 1 ;
    // Viper 5292 to check for built in types get the base data type of ids
    VeriDataType *base_formal_datatype = formal->GetBaseDataType() ;
    VeriDataType *base_this_datatype = this_id->GetBaseDataType() ;
    // Need to find size if the type is a built in type like real, int etc
    VeriTypeInfo *base_this_type = base_this_datatype ? base_this_datatype->CheckType(0, NO_ENV) : 0 ;
    VeriTypeInfo *base_formal_type = base_formal_datatype ? base_formal_datatype->CheckType(0, NO_ENV) : 0 ;

    // Viper 5499. IsPacked is used to check if the datatype is packed struct union.
    if (base_this_datatype && (base_this_datatype->PackedDimension() || base_this_datatype->IsPacked())) {
        // Viper 5508 calculate the size from base type
        int length = base_this_datatype->StaticSizeSignInternal(0) ;
        this_length = (unsigned)ABS(length) ;
    } else if (base_this_datatype && base_this_type && base_this_type->IsBuiltInType()) {
        // Viper 5292 check the size of base type
        int length = (int)base_this_datatype->StaticSizeSignInternal(0) ;
        this_length = (unsigned)ABS(length) ;
    }
    if (base_formal_datatype && (base_formal_datatype->PackedDimension() || base_formal_datatype->IsPacked())) {
        // Viper 5508 calculate the size from base type
        int length = base_formal_datatype->StaticSizeSignInternal(0) ;
        formal_length = (unsigned)ABS(length) ;
    } else if (base_formal_datatype && base_formal_type && base_formal_type->IsBuiltInType()) {
        // Viper 5292 check the size of base type
        int length = base_formal_datatype->StaticSizeSignInternal(0) ;
        formal_length = (unsigned)ABS(length) ;
    }
    delete base_this_type ;
    delete base_formal_type ;
    if (this_length != formal_length) Error("array element widths (%d, %d) don't match", this_length, formal_length) ;
}
*/
/******************************************************************************
*                     Create type from data type                              *
*******************************************************************************/
VeriTypeInfo *VeriDataType::CreateType(VeriIdDef *for_id)
{
    unsigned base_type = (_type) ? _type : ((for_id) ? for_id->Type(): 0) ;

    // VIPER #5949: If the data type is net type(eg. 'wire sig') then the
    // type set to for_id will be its base_type:
    if (IsNetType()) {
        base_type = for_id ? for_id->Type() : 0 ;
    }

    // VIPER #4688 : Create unresolved type when port contains no type. This
    // means identifier is still not resolved (VeriVariable::Resolve not called yet)
    if (for_id && for_id->IsPort() && !base_type) {
        return new VeriBuiltInType(VERI_TYPE) ;
    }
    if (base_type == VERI_ENUM) base_type = 0 ;

    // Create built-in type
    VeriTypeInfo *type = new VeriBuiltInType(base_type) ;

    // VIPER #5949: Set nettype to the typeinfo
    unsigned kind = for_id ? for_id->Kind() : 0 ;
    type->SetNetType(kind) ;

    // Get packed dimension count
    unsigned dim_count = 0 ;
    VeriRange *dim = _dimensions ;
    while(dim) {
        dim_count++ ;
        dim = dim->GetNext() ;
    }
    if (_signing == VERI_SIGNED) type->SetAsSigned() ;
    if (_signing == VERI_UNSIGNED) type->SetAsUnsigned() ;
    // If packed dimension exists, create array type
    if (_dimensions) type = new VeriArrayType(type, dim_count) ;
    return type ;
}
VeriTypeInfo *VeriNetDataType::CreateType(VeriIdDef *for_id)
{
    // Consider _data_type as data type of net :
    VeriTypeInfo *ret_type = (_data_type) ? _data_type->CreateType(for_id) : 0 ;
    if (ret_type) {
        VeriTypeInfo *base_type = ret_type->BaseType() ;
        if (!base_type) base_type = ret_type ;
        base_type->SetNetType(_type) ;
        return ret_type ;
    }

    return VeriDataType::CreateType(for_id) ;
}
VeriTypeInfo *VeriTypeRef::CreateType(VeriIdDef *for_id)
{
    VeriTypeInfo *base_type = 0 ;
    VeriModuleItem *tree = _id ? _id->GetModuleItem() : 0 ;
    unsigned interface_port = (_id) ? _id->IsInterfacePort() : 0 ;
    if (interface_port) {
        // VIPER #6286: For interface port for which are not directly interface
        // type, do not treat them as interface port while creating type here:
        VERIFIC_ASSERT(_id) ; // Must hold, since 'interface_port' is set through _id
        VeriDataType *port_type = _id->GetDataType() ;
        VeriIdDef *type_id = (port_type) ? port_type->GetId() : 0 ;
        if (type_id && !type_id->IsInterface()) interface_port = 0 ;
    }
    if (!_id) {
        if (for_id && (for_id->IsInterfacePort() || for_id->IsVirtualInterface())) {
            // Create unresolved type for unresolved interface type port
            base_type = new VeriNamedType(_type_name) ;
        } else {
            return 0 ; // Do not create type for unresolved ports
        }
    } else if ((tree && tree->IsClass() && _id->IsClass())  // IsClass returns true for enum type with base type class
            || _id->IsInterface() || _id->IsModport() || _id->IsCovergroup() ||
            interface_port || //_id->IsInterfaceInst() || // Interface inst/port
            // If identifier exists, that can be unresolved if both data type and
            // module item backpointer are null
            (_id->IsType() && !_id->IsParam() && !_id->GetDataType() && !_id->GetModuleItem())) { // Type reference is not resolved/class/interface/modport:
        // Create Named type :
        base_type = new VeriNamedType(_type_name) ;
    } else if (_type_name) {
        // It is user defined type :
        base_type = _type_name->CheckType(0, NO_ENV) ;
        // VIPER #8325: Set nettype to the typeinfo
        if (base_type) {
            unsigned kind = for_id ? for_id->Kind() : 0 ;
            base_type->SetNetType(kind) ;
        }
    } else {
        base_type = _id->CreateType(0) ;
    }
    if (for_id && base_type && for_id->IsVirtualInterface()) base_type->SetIsVirtualInterface() ;

    if (!_dimensions) return base_type ; // If no dimension exists, return base_type
    // Get packed dimension count
    unsigned dim_count = 0 ;
    VeriRange *dim = _dimensions ;
    while(dim) {
        dim_count++ ;
        dim = dim->GetNext() ;
    }
    // Packed dimension exists. Now 'base_type' can be built-in type, packed array
    // type, unpacked array type, stuct/union or enum type. If 'base_type' is
    // packed array or unpacked array type, packed dimension is already included in
    // type and so we have to increase that dimension count. For other types we have to
    // create new packed array types
    if (base_type) base_type = base_type->IncreasePackedDimBy(dim_count, this) ;
    return base_type ;
}
VeriTypeInfo *VeriStructUnion::CreateType(VeriIdDef * /*for_id*/)
{
    if (!_decls) return 0 ;
    unsigned is_struct = (_type == VERI_STRUCT) ? 1 : 0 ; // Determine whether it is struct/union

    if (!_is_packed) {
        if (_signing == VERI_SIGNED) { // Signing on unpacked struct/union
            Error("illegal usage of signing with unpacked struct/union type") ;
        }
        // VIPER #4894 : Produce warning if unsigned is used for unpacked struct/union
        if (_signing == VERI_UNSIGNED) {
            Warning("illegal usage of signing with unpacked struct/union type") ;
        }
    }
    // VIPER #6434 : If type for members are not yet created, create those
    if (!_ele_types || _contain_typeparam_type_ele) {
        // Create map for member_iddef vs VeriTypeInfo* for member
        if (!_ele_types) _ele_types = new Map(POINTER_HASH, _decls->Size()) ;
        //_ele_types = new Map(POINTER_HASH, _decls->Size()) ;

        unsigned current_contain_typeparam_type_ele = 0 ;
        // Iterate over member declarations to create map information _id->IsCovergroup() ||
        unsigned i, j ;
        VeriDataDecl *decl ;
        VeriIdDef *id ;
        VeriTypeInfo *id_type ;
        VeriTypeInfo *existing_id_type ;
        FOREACH_ARRAY_ITEM(_decls, i, decl) {
            Array *ids = decl ? decl->GetIds() : 0 ;  // Get declared identifiers
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id) continue ;
                existing_id_type = (VeriTypeInfo*)_ele_types->GetValue(id) ;
                if (existing_id_type && !existing_id_type->IsTypeParameterType() && !existing_id_type->IsNamedType()) continue ; // Already populated
                id_type = id->CreateType(0) ; // Create type for member
                // Chandle cannot be used in untagged union
                if ((Type() == VERI_UNION) && !_is_tagged && id_type && id_type->IsChandleType()) {
                    Error("illegal reference to chandle type") ;
                }
                if (_is_packed && id_type && id_type->IsUnpacked()) { // Unpacked member
                    id->Error("unpacked member %s is not allowed in packed struct/union", id->Name()) ;
                }
                // Member of packed struct/union may not be declared 'rand' or 'randc'
                if (_is_packed && decl && (decl->GetQualifier(VERI_RAND) || decl->GetQualifier(VERI_RANDC))) {
                    id->Error("field %s of packed struct/union may not be declared rand or randc", id->Name()) ;
                }
                if (id_type && (id_type->IsTypeParameterType() || id_type->IsUnresolvedNameType() || id_type->IsClassType())) {
                    _contain_typeparam_type_ele = 1 ;
                    current_contain_typeparam_type_ele = 1 ;
                }
                (void) _ele_types->Insert(id, id_type, 1 /*force overwrite*/) ; // Add to map
                delete existing_id_type ;
            }
        }
        if (!current_contain_typeparam_type_ele) _contain_typeparam_type_ele = 0 ;
    }
    // Create type information
    VeriTypeInfo *type = new VeriStructUnionType(is_struct, _is_packed, _is_tagged, _ele_types, _scope) ;
    if (_signing == VERI_SIGNED) type->SetAsSigned() ;
    if (_signing == VERI_UNSIGNED) type->SetAsUnsigned() ;

    if (!_dimensions) return type ; // No packed dimension, return struct/union type

    // Get packed dimension count
    unsigned dim_count = 0 ;
    VeriRange *dim = _dimensions ;
    while(dim) {
        dim_count++ ;
        dim = dim->GetNext() ;
    }
    // Create array type with dimension and return
    return new VeriArrayType(type, dim_count) ;
}
VeriTypeInfo *VeriEnum::CreateType(VeriIdDef *for_id)
{
    if (!_ids) return 0 ; // No literals, return 0

    // Create type for base type of enum. If no base type exists, base type is int
    VeriTypeInfo *base_type = _base_type ? _base_type->CreateType(for_id) : new VeriBuiltInType(VERI_INT) ;

    // Issue error if base type of enum is unpacked
    // VIPER #8500: Check if the base-type has multiple packed dimensions and error out:
    //if (base_type && base_type->IsUnpacked()) {
    VeriTypeInfo *base_type_info = base_type ? base_type->BaseType() : 0 ;
    if (base_type && (base_type->IsUnpacked() || (base_type_info && (base_type_info->IsStructure() ||
        base_type_info->IsUnion() || base_type_info->IsEnumeratedType())) || base_type->IsRealType() ||
        base_type->IsEventType() || base_type->IsChandleType() || (base_type->PackedDimension() > 1))) {
        Error("illegal enumeration base type") ;
    }
    VeriTypeInfo *type ;
    if (_enum_ids) {
        type = new VeriEnumType(base_type, _enum_ids) ;
    } else {
        // Create a Map for enumerated literals
        _enum_ids = new Map(STRING_HASH, _ids->Size()) ;
        unsigned i ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(_ids, i, id) {
            if (!id) continue ;
            (void) _enum_ids->Insert(id->Name(), id) ;
        }
        type = new VeriEnumType(base_type, _enum_ids) ;
    }

    // VIPER #5710: (vcs_compatibility-9000388573): Enum type may have packed dimension
    // For enum literal return the type of its base type and do not consider the packed
    // dimension of the enum type:
    if (for_id) {
        VeriIdDef *enum_id = (VeriIdDef *) _enum_ids->GetValue(for_id->Name()) ;
        if (enum_id == for_id) return type ;
    }

    // If the enum has packed dimension then create array type with dimension
    // otherwise return the enum type:
    if (!_dimensions) return type ; // No packed dimension, return enum type

    // Get packed dimension count
    unsigned dim_count = 0 ;
    VeriRange *dim = _dimensions ;
    while(dim) {
        dim_count++ ;
        dim = dim->GetNext() ;
    }
    // Create array type with dimension and return
    return new VeriArrayType(type, dim_count) ;
}
VeriTypeInfo *VeriTypeOperator::CreateType(VeriIdDef * /*for_id*/)
{
    if (!_expr_or_data_type) return 0 ;
    VeriTypeInfo *expr_type = _expr_or_data_type->CheckType(0, NO_ENV) ;
    // Don't check for dynamic type. It is illegal as per LRM, but simulators
    // do not produce error
    if ((!_expr_or_data_type->GetTargetType() && (_expr_or_data_type->IsAssignPattern() || _expr_or_data_type->IsMultiAssignPattern())) || _expr_or_data_type->IsStreamingConcat()) { // || (expr_type && expr_type->IsDynamicType())) {
        Error("illegal expression in type operator") ;
    }

    return expr_type ;
}

///////////////////////////////////////////////////////////////////////////////////////
///////////////       Bit size of base types like bit, int, real   ////////////////////
///////////////////////////////////////////////////////////////////////////////////////
// VIPER #2838 : Calculate the bit size of base types not considering packed dimensions
unsigned VeriDataType::BaseTypeSize() const
{
    // VIPER #6640: Call the merged routine to get the size of the built-in type:
    return (_type) ? VeriNode::GetSizeOfBuiltInType(_type) : 1 ;
}
unsigned VeriNetDataType::BaseTypeSize() const
{
    return (_data_type) ? _data_type->BaseTypeSize() : VeriDataType::BaseTypeSize() ;
}
// System Verilog additions
unsigned VeriTypeRef::BaseTypeSize() const
{
    // Base type size is the size of the referred type identifier's base type :
    VeriDataType *data_type = (_id) ? _id->GetDataType() : 0 ;
    return (data_type) ? data_type->BaseTypeSize() : 0 ; // return 0 if type-id is not resolved.
}
unsigned VeriStructUnion::BaseTypeSize() const
{
    // Size of a struct is accumulation of size of its members.
    // Size of a union is the MAX of size of its members.
    // As we are not considering the size here, we cannot calculate
    // the size of this base type

    return 0 ;
}
unsigned VeriEnum::BaseTypeSize() const
{
    if (_base_type && _base_type->Type()) {
        // If a subtype is given, we should follow that.
        return _base_type->BaseTypeSize() ;
    } else if (_base_type && !_base_type->Type() && _base_type->GetDimensions()) {
        // no 'integer' element type was given, but there is a (packed) range.
        // This is not according to the LRM, but DC accepts it as a bit-range.
        // So the element size is 1 :
        return 1 ;
    }
    return 32 ; // Size of enum is default size integer : 32 bits.
}
unsigned VeriTypeOperator::BaseTypeSize() const
{
    unsigned base_size = 0 ;
    VeriConstraint *constraint = (_expr_or_data_type) ? _expr_or_data_type->EvaluateConstraintInternal(0, 0): 0 ;
    VeriConstraint *base_constraint = constraint ;
    VeriConstraint *runner = constraint ;
    while (runner) {
        if (runner->IsEnumConstraint()) {
            base_constraint = runner->BaseConstraint() ;
            break ;
        }
        if (runner->IsArrayConstraint()) {
            runner = runner->ElementConstraint() ;
            base_constraint = runner ;
        } else {
            break ;
        }
    }
    base_size = (base_constraint) ? base_constraint->NumOfBits(): 0 ;
    delete constraint ;
    return base_size ;
}

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
unsigned
VeriConcat::FindSize()
{
    unsigned size = 0 ;
    unsigned i ;
    VeriExpression *expr ;
    // Iterate through the concat expressions
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        // Add up the sizes of the concat elements to get the total size
        size = size + expr->FindSize() ;
    }
    return size ;
}
unsigned
VeriIdRef::FindSize()
{
    // returns 1 for non array
    // Multi dimensions  not supported yet
    // Check only packed dimension
    VeriDataType *data_type = _id ? _id->GetDataType() : 0 ;
    VeriRange *dims = data_type ? data_type->GetDimensions() : 0 ;
    if (!dims) {
        // Get datatype from datatype id for type defs
        VeriIdDef *data_type_id = data_type ? data_type->GetId() : 0 ;
        dims = data_type_id ? data_type_id->GetDimensions() : 0 ;
    }
    // If dims->FindSize is 0 then this is a bit of size 1
    if (!dims) return 1 ;
    unsigned size = 1 ;
    while (dims) {
        size *= dims->FindSize() ;
        dims = dims->GetNext() ;
    }
    return size ;
}
unsigned
VeriIndexedId::FindSize()
{
    unsigned size = 0 ;
    if (_idx) size = _idx->FindSize() ;
    return size ? size : ((_idx && _idx->IsConst()) ? 1 : 0) ; // If _idx->FindSize is 0 then this is a bit-select if _idx is an const so return 1
}
#endif

//////////////////////// Constant expression ///////////////////////////
unsigned VeriIdRef::IsConstExpr() const
{
    if (!_id) return 0 ;

    // VIPER #7581: Seq/property formal is constant expression
    if (_id->IsSequenceFormal() || _id->IsPropertyFormal()) return 1 ;

    // Viper 4841. Removed the call of _id->IsConstant() from IsConstExpr
    // VIPER #7127: For functions without any argument, check if it can be constant:
    VeriModuleItem *func = _id->GetModuleItem() ;
    // VIPER #7646: For constant expression parameter should not be type parameter
    if (_id->IsGenVar() || (_id->IsFunction() && func && func->CanBeConstFunction()) || (_id->IsParam() && !_id->IsType())) return 1 ; // constant identifier
    return 0 ;
}
unsigned VeriIndexedId::IsConstExpr() const
{
    if (!_prefix || !_idx) return 0 ; // Analyze error

    // First check if prefix is constant
    if (!_prefix->IsConstExpr()) return 0 ; // Prefix non-constant

    if (_idx->IsRange()) {
        VeriExpression *left = _idx->GetLeft() ;
        VeriExpression *right = _idx->GetRight() ;
        if (left && left->IsConstExpr() && right && right->IsConstExpr()) return 1 ; // constant select on constant
    } else {
        return _idx->IsConstExpr() ;
    }
    return 0 ; // No constant
}
unsigned VeriSelectedName::IsConstExpr() const
{
    if (_suffix_id && (_suffix_id->IsGenVar() || _suffix_id->IsFunction() || (_suffix_id->IsParam() && !_suffix_id->IsType()) || _suffix_id->IsConstant())) return 1 ; // constant expression
    if (_prefix && _prefix->IsConstExpr()) return 1 ; // VIPER #8291: If suffix is not a constant expression then go for checking its prfix to be constant expression

    return 0 ;
}
unsigned VeriIndexedMemoryId::IsConstExpr() const
{
    if (!_prefix || !_prefix->IsConstExpr()) return 0 ; // Prefix non-constant

    // Check indexes
    unsigned i ;
    VeriExpression *idx ;
    FOREACH_ARRAY_ITEM(_indexes, i, idx) {
        if (!idx) continue ;
        if (idx->IsRange()) {
            VeriExpression *left = idx->GetLeft() ;
            VeriExpression *right = idx->GetRight() ;
            if (left && !left->IsConstExpr()) return 0 ; // Non constant index
            if (right && !right->IsConstExpr()) return 0 ; // Non constant index
        } else {
            if (!idx->IsConstExpr()) return 0 ; // Non constant index
        }
    }
    return 1 ; // constant index
}
unsigned VeriConcat::IsConstExpr() const
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        if (!expr->IsConstExpr()) return 0 ; // Non constant element
    }
    return 1 ; // constant concat
}
unsigned VeriMultiConcat::IsConstExpr() const
{
    if (_repeat && !_repeat->IsConstExpr()) return 0 ; // non constant repeat expression
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        if (!expr->IsConstExpr()) return 0 ; // Non constant element
    }
    return 1 ; // constant concat
}
// VIPER #7581: Check for function name to be a constant expression
unsigned VeriFunctionCall::IsConstExpr() const
{
    if (!_func_name) return 0 ;
    if (!_func_name->IsConstExpr()) return 0 ;

    // VIPER #7705: Also check constantness of the arguments
    // of the function call.
    unsigned i ;
    VeriExpression *arg ;
    FOREACH_ARRAY_ITEM(_args, i, arg) {
        if (!arg) continue ;
        if (!arg->IsConstExpr()) return 0 ;
    }
    return 1 ; // constant function call
}
// VIPER #8516: Check for system function to be a constant expression:
// According to LRM-IEEE-1800-2012: Section: 11.2.1: Constant system function calls are calls to
// certain built-in system functions where the arguments are constant expressions.
// Specifically, these are the conversion system functions, the mathematical system functions
// and the bit vector system functions where we checks for their arguments to be constant expression.
// The data query system functions and the array query system functions are normally also constant
// system function calls even when their arguments are not constant.
unsigned VeriSystemFunctionCall::IsConstExpr() const
{
    if (!_args || !_args->Size()) return 0 ;

    switch (_function_type) {
    // conversion system functions:
    case VERI_SYS_CALL_RTOI :
    case VERI_SYS_CALL_ITOR :
    case VERI_SYS_CALL_REALTOBITS :
    case VERI_SYS_CALL_BITSTOREAL :
    case VERI_SYS_CALL_SIGNED :
    case VERI_SYS_CALL_UNSIGNED :
    case VERI_SYS_CALL_SHORTREALTOBITS :
    case VERI_SYS_CALL_BITSTOSHORTREAL :
    case VERI_SYS_CALL_CAST :
    // mathematical system functions:
    case VERI_SYS_CALL_CLOG2 :
    case VERI_SYS_CALL_LN :
    case VERI_SYS_CALL_LOG10 :
    case VERI_SYS_CALL_EXP :
    case VERI_SYS_CALL_SQRT :
    case VERI_SYS_CALL_FLOOR :
    case VERI_SYS_CALL_CEIL :
    case VERI_SYS_CALL_SIN :
    case VERI_SYS_CALL_COS :
    case VERI_SYS_CALL_TAN :
    case VERI_SYS_CALL_ASIN :
    case VERI_SYS_CALL_ACOS :
    case VERI_SYS_CALL_ATAN :
    case VERI_SYS_CALL_SINH :
    case VERI_SYS_CALL_COSH :
    case VERI_SYS_CALL_TANH :
    case VERI_SYS_CALL_ASINH :
    case VERI_SYS_CALL_ACOSH :
    case VERI_SYS_CALL_ATANH :
    case VERI_SYS_CALL_POW :
    case VERI_SYS_CALL_ATAN2 :
    case VERI_SYS_CALL_HYPOT :
// bit vector system functions:
    case VERI_SYS_CALL_COUNTONES :
    case VERI_SYS_CALL_ONEHOT0 :
    case VERI_SYS_CALL_ONEHOT :
    case VERI_SYS_CALL_ISUNKNOWN :
        {
            unsigned ai ;
            VeriExpression *arg ;
            FOREACH_ARRAY_ITEM(_args, ai, arg) {
                if (!arg) continue ;
                if (arg->IsDataType()) continue ;
                if (!arg->IsConstExpr()) return 0 ;
            }
            return 1 ;
        }
// data query system functions are constant system functions: without depending on their arguments whether they are constant or not
    case VERI_SYS_CALL_TYPENAME :
    case VERI_SYS_CALL_ISUNBOUNDED :
    case VERI_SYS_CALL_BITS :

// array query functions are constant system functions: without depending on their arguments whether they are constant or not
    case VERI_SYS_CALL_DIMENSIONS :
    case VERI_SYS_CALL_LEFT :
    case VERI_SYS_CALL_RIGHT :
    case VERI_SYS_CALL_LOW :
    case VERI_SYS_CALL_HIGH :
    case VERI_SYS_CALL_INCREMENT :
    case VERI_SYS_CALL_LENGTH :
    case VERI_SYS_CALL_SIZE :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS : // SV Std 1800 addition
        return 1 ; // are constant system functions
    default: break ; // treat all other system function calls as non-constant
    }

    return 0 ;
}
// VIPER #7581: Check for mintypemax expression to be a constant expression
unsigned VeriMinTypMaxExpr::IsConstExpr() const
{
    if (_min_expr && !_min_expr->IsConstExpr()) return 0 ;
    return (_max_expr) ? _max_expr->IsConstExpr() : 0 ;
}
unsigned VeriUnaryOperator::IsConstExpr() const
{
    return (_arg) ? _arg->IsConstExpr() : 0 ;
}
unsigned VeriBinaryOperator::IsConstExpr() const
{
    if (_left && !_left->IsConstExpr()) return 0 ; // non constant left operand
    return (_right) ? _right->IsConstExpr() : 0 ; // check right operand
}
unsigned VeriQuestionColon::IsConstExpr() const
{
    if (_if_expr && !_if_expr->IsConstExpr()) return 0 ; // non constant condition
    if (_then_expr && !_then_expr->IsConstExpr()) return 0 ; // non constant operand
    return (_else_expr) ? _else_expr->IsConstExpr() : 0 ; // Check false expression
}
unsigned VeriPortConnect::IsConstExpr() const
{
    return _connection ? _connection->IsConstExpr() : 0 ;
}

// VIPER #7864: Constant expression check for cast operator.
// For that we check the constantness of the expression which is casted.
unsigned VeriCast::IsConstExpr() const
{
    return _expr ? _expr->IsConstExpr() : 0 ;
}
unsigned VeriIndexedExpr::IsConstExpr() const
{
    if (!_prefix || !_idx) return 0 ; // Analyze error

    // First check if prefix is constant
    if (!_prefix->IsConstExpr()) return 0 ; // Prefix non-constant

    if (_idx->IsRange()) {
        VeriExpression *left = _idx->GetLeft() ;
        VeriExpression *right = _idx->GetRight() ;
        if (left && left->IsConstExpr() && right && right->IsConstExpr()) return 1 ; // constant select on constant
    } else {
        return _idx->IsConstExpr() ;
    }
    return 0 ; // No constant
}
unsigned VeriDollar::IsConstExpr() const
{
    return 1 ; // '$' is constant
}

unsigned VeriIdRef::IsDollar() const
{
    VeriExpression *actual = (_id) ? _id->GetActual(): 0 ;
    // Viper 6592: If the actual is null take the initial value of the parameter
    VeriExpression *expr_to_eval = actual ? actual :( _id ? _id->GetInitialValue() : 0 );
    if (expr_to_eval && expr_to_eval->IsDollar()) return 1 ; // Actual is '$'
    return 0 ; // Not dollar
}
void VeriExpression::MarkInterfaceTypePort(const VeriScope * /*scope*/)
{ }
void VeriIdRef::MarkInterfaceTypePort(const VeriScope *scope)
{
    VeriIdDef *id = (scope) ? scope->FindLocal(_name) : 0 ;
    VeriDataType *data_type = (id) ? id->GetDataType() : 0 ;
    if (id && !id->IsPort() && data_type && data_type->GetTypeName()) {
        // VIPER #3672 :This identifier is in non-ansi port list, but no direction is specified
        // by declaration and type is named type. If type name is not resolved or
        // it is interface/modport, it can be interface type port. So set inout direction
        VeriIdDef *port_type = data_type->GetId() ;
        if (!port_type || port_type->IsInterface() || port_type->IsModport()) {
            id->SetDir(VERI_INOUT) ;
        }
    }
}
void VeriPortConnect::MarkInterfaceTypePort(const VeriScope *scope)
{
    if (_connection) _connection->MarkInterfaceTypePort(scope) ;
}
void VeriConcat::MarkInterfaceTypePort(const VeriScope *scope)
{
    unsigned i ;
    VeriExpression *item ;
    FOREACH_ARRAY_ITEM(_exprs, i, item) {
        if (item) item->MarkInterfaceTypePort(scope) ;
    }
}
void VeriIndexedId::MarkInterfaceTypePort(const VeriScope *scope)
{
    if (_prefix) _prefix->MarkInterfaceTypePort(scope) ;
}
void VeriIndexedMemoryId::MarkInterfaceTypePort(const VeriScope *scope)
{
    if (_prefix) _prefix->MarkInterfaceTypePort(scope) ;
}

VeriIdDef *
VeriIdRef::DeclareImplicitNet(VeriScope *scope, unsigned net_type) const
{
    if (!scope || !net_type || (net_type == VERI_NONE)) return 0 ; // Can't define
    if (_id || !_name) return 0 ;
    if (scope->Find(_name, this)) return 0 ; // Already defined

    // Here, it is not defined, so we need to define it implicitly now:
    VeriIdDef *id = new VeriVariable(Strings::save(_name)) ;
    id->SetImplicitNet() ; // Mark it to be an implicit net
    id->SetType(net_type) ; // Set the type of the net
    id->SetKind(net_type) ; // VIPER #6254: Set the kind of the net
    (void) scope->Declare(id) ; // Declare it in the scope

    return id ;
}

VeriIdDef *
VeriTimingCheckEvent::DeclareImplicitNet(VeriScope *scope, unsigned net_type) const
{
    // Only define implicit nets for simple expressions:
    // Also include posedge/negedge expressions
    if (_edge_desc_str || _condition) return 0 ; // It is not a simple id-ref

    // For simple id-refs, call this routine on the terminal descriptor:
    return (_terminal_desc) ? _terminal_desc->DeclareImplicitNet(scope, net_type) : 0 ;
}

/******************************************************************************/
// Constraint evaluation :
/******************************************************************************/

VeriConstraint *VeriExpression::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/) { return 0 ; }
VeriConstraint *VeriIdRef::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    if (!_id) return 0 ;

    return _id->EvaluateConstraintInternal(tab, df, 0) ;
}
VeriConstraint *VeriIndexedId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    VeriConstraint *prefix_constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(tab, df): 0 ;
    if (!prefix_constraint) return 0 ; // Cannot do anything without prefix constraint
    if (!_idx) return prefix_constraint ;

    VeriConstraint *result = 0 ;
    // VIPER #6445: Determine element constraint for multi-bit scalar type like 'int':
    VeriConstraint *ele_constraint = prefix_constraint->ElementConstraint() ;
    if (!ele_constraint) {
        if (prefix_constraint->IsScalarTypeConstraint() && (prefix_constraint->NumOfBits() > 1)) {
            // Prefix is a multi-bit scalar type, use reg as its element constraint:
            ele_constraint = new VeriScalarTypeConstraint(VERI_REG) ;
        }
    } else {
        ele_constraint = ele_constraint->Copy() ;
    }
    if (_idx->IsRange()) { // Expression like vector[1:0]
        VeriRange *msb_dim = _id ? _id->GetDimensionAt(0): 0 ;
        VeriExpression *left = msb_dim ? msb_dim->GetLeft() : 0 ;
        if (left && left->IsDollar()) { // It is select on queue
            // Select on queue creates a queue
            delete ele_constraint ;
            return prefix_constraint ;
        }
        // Evaluate the slice bounds and create a new constraint.
        VeriConstraint *range = _idx->EvaluateConstraintInternal(tab, df) ;
        if (!range) {
            delete prefix_constraint ;
            delete ele_constraint ;
            return 0 ;
        }
        // Create a new Array constraint, using the element constraint
        // of the prefix as element constraint, and the newly created range
        // as the index constraint
        // If prefix_constraint is unpacked array constraint, new 'range' will
        // be unpacked, otherwise packed
        if (prefix_constraint->IsPacked()) {
            range->SetAsPacked() ;
        }
        result = new VeriArrayConstraint(range, ele_constraint) ;
    } else { // Something like "vector[3]"
        // Return the element constraint of the prefix.
        result = ele_constraint ;
    }
    delete prefix_constraint ;
    return result ;
}
VeriConstraint *VeriSelectedName::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    if (_prefix && !_suffix_id && !_suffix) return _prefix->EvaluateConstraintInternal(tab, df) ;
    if (!_suffix_id) return 0 ;
    // Constraint of suffix is the constraint of this selected name :
    return _suffix_id->EvaluateConstraintInternal(tab, df, 0) ;
}

// Viper 7398 : For scope name pick up the statically stored values/constraints
VeriConstraint *VeriScopeName::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    return (VeriSelectedName::EvaluateConstraintInternal(tab, df)) ; // call base class routine
}

VeriConstraint *VeriIndexedMemoryId::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    // Evaluate constraint of prefix :
    VeriConstraint *prefix_constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(tab, df): 0 ;
    if (!prefix_constraint) return 0 ; // Cannot do anything without prefix constraint
    if (!_indexes) return prefix_constraint ;

    // The last index specifies whether it is array indexing or array slicing
    // Decent as many dimensions as the index list indicates except the last one
    VeriConstraint *elem_constraint = prefix_constraint ;
    unsigned i ;
    for (i = 0 ; i < (_indexes->Size() -1); i++) {
        elem_constraint = (elem_constraint) ? elem_constraint->ElementConstraint(): 0 ;
    }
    VeriConstraint *result = 0 ;
    // VIPER #6445: Determine element constraint for multi-bit scalar type like 'int':
    VeriConstraint *ele_constraint = (elem_constraint) ? elem_constraint->ElementConstraint() : 0 ;
    if (!ele_constraint) {
        if (elem_constraint && elem_constraint->IsScalarTypeConstraint() && (elem_constraint->NumOfBits() > 1)) {
            // Prefix is a multi-bit scalar type, use reg as its element constraint:
            ele_constraint = new VeriScalarTypeConstraint(VERI_REG) ;
        }
    } else {
        ele_constraint = ele_constraint->Copy() ;
    }
    // Now consider the last index
    VeriExpression *last_idx = (VeriExpression*)_indexes->GetLast() ;
    if (last_idx->IsRange()) { // Expression like vector[1][1:0]
        // Evaluate the slice bounds and create a new constraint.
        VeriConstraint *range = last_idx->EvaluateConstraintInternal(tab, df) ;
        if (!range) {
            delete ele_constraint ;
            delete prefix_constraint ;
            return 0 ;
        }
        // Create a new Array constraint, using the element constraint
        // of the prefix as element constraint, and the newly created range
        // as the index constraint
        if (elem_constraint && elem_constraint->IsPacked()) range->SetAsPacked() ;
        result = new VeriArrayConstraint(range, ele_constraint) ;
    } else { // Something like "vector[1][3]"
        // Return the element constraint of the prefix.
        result = ele_constraint ;
    }
    delete prefix_constraint ;
    return result ;
}
VeriConstraint *VeriFunctionCall::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    // Get the resolved function id:
    VeriIdDef *func_id = (_func_name) ? _func_name->GetId() : 0 ;
    // Evaluate and return the constraint of that data type:
    return (func_id) ? func_id->EvaluateConstraintInternal(tab, df, 0) : 0 ;
}
VeriConstraint *VeriPortConnect::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    return (_connection) ? _connection->EvaluateConstraintInternal(tab, df): 0 ;
}
VeriConstraint *VeriConcat::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    unsigned i ;
    VeriExpression *expr ;
    VeriConstraint *ele_constraint = 0 ;
    unsigned size = 0 ;
    unsigned is_any_ele_4_state = 0 ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        ele_constraint = (expr) ? expr->EvaluateConstraintInternal(tab, df) : 0 ;
        if (ele_constraint && ele_constraint->Is4State()) is_any_ele_4_state = 1 ;
        size += (ele_constraint) ? ele_constraint->NumOfBits(): 0 ;
        delete ele_constraint ;
    }
    if (size == 0) return 0 ; // Constraint cannot be determined
    VeriConstraint *range = new VeriRangeConstraint((int)size -1, 0) ;
    range->SetAsPacked() ;
    unsigned type = VERI_REG ; // 4-state type or verilog-2001 mode
    // VIPER #8131 : If no element of cancat is 4-state type, create
    // array of 2-state bit as concat type
    if (!is_any_ele_4_state) type = VERI_BIT ; // 2-state type
    return new VeriArrayConstraint(range, new VeriScalarTypeConstraint(type)) ;
}
VeriConstraint *VeriMultiConcat::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    unsigned repeat_count = 0 ;
    if (IsStaticElab()) {
        VeriBaseValue *val = (_repeat) ? _repeat->StaticEvaluateInternal(0,tab,0, 0): 0 ;
        if (!val) {
            Error("repetition multiplier is not constant") ;
            return 0 ;
        }
        int rep_count = val->GetIntegerValue() ;
        delete val ;
        if (rep_count < 0) return 0 ;
        repeat_count = (unsigned)rep_count ;
    }

    unsigned i ;
    VeriExpression *expr ;
    VeriConstraint *ele_constraint = 0 ;
    unsigned size = 0 ;
    unsigned is_any_ele_4_state = 0 ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        ele_constraint = (expr) ? expr->EvaluateConstraintInternal(tab, df) : 0 ;
        if (ele_constraint && ele_constraint->Is4State()) is_any_ele_4_state = 1 ;
        size += (ele_constraint) ? ele_constraint->NumOfBits(): 0 ;
        delete ele_constraint ;
    }
    if (size == 0) return 0 ; // Constraint cannot be determined

    VeriRangeConstraint *range = new VeriRangeConstraint((int)(repeat_count * size) -1, 0) ;
    range->SetAsPacked() ;
    unsigned type = VERI_REG ; // 4-state type or verilog-2001 mode
    // VIPER #8131 : If no element of cancat is 4-state type, create
    // array of 2-state bit as concat type
    if (!is_any_ele_4_state) type = VERI_BIT ; // 2-state type
    return new VeriArrayConstraint(range, new VeriScalarTypeConstraint(type)) ;
}
VeriConstraint *VeriSystemFunctionCall::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    int size = 0 ;
    VeriExpression *arg ;
    switch (_function_type) {
    case VERI_SYS_CALL_SIGNED :
    case VERI_SYS_CALL_UNSIGNED :
    case VERI_SYS_CALL_TYPEOF :
        if (!_args || _args->Size()!=1) return 0 ;
        arg = (VeriExpression*)_args->At(0) ;
        return (arg) ? arg->EvaluateConstraintInternal(tab, df) : 0 ;
    case VERI_SYS_CALL_TIME :
        size = 64 ;
        break ;
    case VERI_SYS_CALL_BITSTOREAL :
    case VERI_SYS_CALL_ITOR :
    case VERI_SYS_CALL_REALTIME :
        size = 64 ;
        break ;
    case VERI_SYS_CALL_STIME :
    case VERI_SYS_CALL_URANDOM :
    case VERI_SYS_CALL_URANDOM_RANGE :
        size = 32 ;
        break ;
    case VERI_SYS_CALL_RTOI :
    case VERI_SYS_CALL_RANDOM :
    // File I/O operations
    case VERI_SYS_CALL_FGETC: // returns a byte, but should hold -1 when error. Simulators return 32 bit
    case VERI_SYS_CALL_FGETS:
    case VERI_SYS_CALL_UNGETC:
    case VERI_SYS_CALL_FSCANF:
    case VERI_SYS_CALL_SSCANF:
    case VERI_SYS_CALL_FREAD:
    case VERI_SYS_CALL_FOPEN:
    case VERI_SYS_CALL_REWIND:
    case VERI_SYS_CALL_FTELL:
    case VERI_SYS_CALL_FSEEK:
    case VERI_SYS_CALL_FERROR:
    // Added other distribution functions for VIPER #3756
    // std P1364-Y2K section 17.9.3
    case VERI_SYS_CALL_DIST_CHI_SQUARE :
    case VERI_SYS_CALL_DIST_EXPONENTIAL :
    case VERI_SYS_CALL_DIST_NORMAL :
    case VERI_SYS_CALL_DIST_T :
    case VERI_SYS_CALL_DIST_UNIFORM :
    case VERI_SYS_CALL_DIST_ERLANG :
    case VERI_SYS_CALL_DIST_POISSON :
    case VERI_SYS_CALL_BITS :
    case VERI_SYS_CALL_DIMENSIONS :
    case VERI_SYS_CALL_LEFT :
    case VERI_SYS_CALL_RIGHT :
    case VERI_SYS_CALL_LOW :
    case VERI_SYS_CALL_HIGH :
    case VERI_SYS_CALL_INCREMENT :
    case VERI_SYS_CALL_LENGTH :
    case VERI_SYS_CALL_SIZE :
    case VERI_SYS_CALL_UNPACKED_DIMENSIONS : // SV Std 1800 addition
    case VERI_SYS_CALL_BITSTOSHORTREAL :
    case VERI_SYS_CALL_SHORTREALTOBITS :
        size = 32 ;
        break ;
    default:
        break ;
    }
    if (size) {
        VeriRangeConstraint *range = new VeriRangeConstraint(size -1, 0) ;
        range->SetAsPacked() ;
        return new VeriArrayConstraint(range, new VeriScalarTypeConstraint(VERI_REG)) ;
    }
    return 0 ;
}
VeriConstraint *VeriMinTypMaxExpr::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    return (_typ_expr) ? _typ_expr->EvaluateConstraintInternal(tab, df): 0 ;
}
VeriConstraint *VeriUnaryOperator::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    VeriConstraint *arg_constraint = (_arg) ? _arg->EvaluateConstraintInternal(tab, df) : 0 ;
    VeriConstraint *constraint = 0 ;
    switch (_oper_type) {
    case VERI_PLUS :
    case VERI_MIN :
    case VERI_REDNOT :
        constraint = arg_constraint ;
        arg_constraint = 0 ;
        break ;
    case VERI_LOGNOT :
    case VERI_REDAND :
    case VERI_REDNAND :
    case VERI_REDOR :
    case VERI_REDNOR :
    case VERI_REDXOR :
    case VERI_REDXNOR :
    case VERI_STRONG :
    case VERI_WEAK :
        constraint = new VeriScalarTypeConstraint(VERI_REG) ;
        delete arg_constraint ;
        arg_constraint = 0 ;
        break ;
    default : break ;
    }
    if (constraint && constraint->IsEnumConstraint()) {
        VeriConstraint *temp = constraint ;
        constraint = constraint->ElementConstraint() ;
        if (constraint) constraint = constraint->Copy() ;
        delete temp ;
    }
    return constraint ;
}
VeriConstraint *VeriBinaryOperator::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    VeriConstraint *constraint = 0, *left_constraint = 0, *right_constraint = 0 ;
    unsigned lsize = 0, rsize = 0 ;
    switch (_oper_type) {
    case VERI_INSIDE :
    case VERI_LEADTO:
    case VERI_COLON_EQUAL:
    case VERI_COLON_NEQUAL:
    case VERI_AT :
    case VERI_CYCLE_DELAY_OP :
    case VERI_DISABLE :
    case VERI_OVERLAPPED_IMPLICATION :
    case VERI_NON_OVERLAPPED_IMPLICATION :
    case VERI_OVERLAPPED_FOLLOWED_BY :
    case VERI_NON_OVERLAPPED_FOLLOWED_BY :
    case VERI_CONSECUTIVE_REPEAT :
    case VERI_NON_CONSECUTIVE_REPEAT :
    case VERI_GOTO_REPEAT :
    case VERI_OR :
    case VERI_AND :
    case VERI_INTERSECT :
    case VERI_THROUGHOUT :
    case VERI_WITHIN :
    case VERI_ACCEPT_ON : // Abort property
    case VERI_REJECT_ON : // Abort property
    case VERI_SYNC_ACCEPT_ON : // Abort property
    case VERI_SYNC_REJECT_ON : // Abort property
    case VERI_IFF :
    case VERI_IMPLIES :
    case VERI_UNTIL :
    case VERI_S_UNTIL :
    case VERI_UNTIL_WITH :
    case VERI_S_UNTIL_WITH :
    case VERI_NEXTTIME :
    case VERI_S_NEXTTIME :
    case VERI_EVENTUALLY :
    case VERI_S_EVENTUALLY :
    case VERI_ALWAYS :
    case VERI_S_ALWAYS :
    case VERI_RIGHTARROW :
    case VERI_LOG_EQUIVALENCE :
    case VERI_LOGAND :
    case VERI_LOGOR :
    case VERI_LT :
    case VERI_GT :
    case VERI_GEQ :
    case VERI_LEQ :
    case VERI_CASEEQ :
    case VERI_CASENEQ :
    case VERI_LOGEQ :
    case VERI_LOGNEQ :
    case VERI_WILDEQUALITY :
    case VERI_WILDINEQUALITY :
        // Return type is 1 bit
        constraint = new VeriScalarTypeConstraint(VERI_REG) ;
        break ;
    case VERI_PLUS :
    case VERI_MIN :
    case VERI_MUL :
    case VERI_DIV :
    case VERI_MODULUS :
    case VERI_REDAND :
    case VERI_REDOR :
    case VERI_REDXOR :
    case VERI_REDXNOR :
    case VERI_PLUS_ASSIGN :
    case VERI_MIN_ASSIGN :
    case VERI_MUL_ASSIGN :
    case VERI_DIV_ASSIGN :
    case VERI_MOD_ASSIGN :
    case VERI_AND_ASSIGN :
    case VERI_OR_ASSIGN :
    case VERI_XOR_ASSIGN :
        left_constraint = (_left) ? _left->EvaluateConstraintInternal(tab, df) : 0 ;
        right_constraint = (_right) ? _right->EvaluateConstraintInternal(tab, df) : 0 ;
        lsize = (left_constraint) ? left_constraint->NumOfBits() : 0 ;
        rsize = (right_constraint) ? right_constraint->NumOfBits() : 0 ;
        if (lsize > rsize) {
            constraint = left_constraint ;
            delete right_constraint ;
        } else if (lsize < rsize) {
            constraint = right_constraint ;
            delete left_constraint ;
        } else if (right_constraint && (right_constraint->ScalarType() == VERI_REAL)) {
        // If any operand is of type real, return type is real
            constraint = right_constraint ;
            delete left_constraint ;
        } else { // Right operand not real, return type can be left operand type
            constraint = left_constraint ;
            delete right_constraint ;
        }
        left_constraint = 0 ; right_constraint = 0 ;
        break ;
    case VERI_POWER :
    case VERI_LSHIFT :
    case VERI_RSHIFT :
    case VERI_ARITLSHIFT :
    case VERI_ARITRSHIFT :
    case VERI_LSHIFT_ASSIGN :
    case VERI_ALSHIFT_ASSIGN :
    case VERI_RSHIFT_ASSIGN :
    case VERI_ARSHIFT_ASSIGN :
    case VERI_EQUAL_ASSIGN :

        constraint = (_left) ? _left->EvaluateConstraintInternal(tab, df) : 0 ;
        break ;
    case VERI_INC_OP :
    case VERI_DEC_OP :
        left_constraint = (_left) ? _left->EvaluateConstraintInternal(tab, df) : 0 ;
        right_constraint = (_right) ? _right->EvaluateConstraintInternal(tab, df) : 0 ;

        // Return type is same as operand
        if (left_constraint) {
            constraint = left_constraint ;
            delete right_constraint ;
        } else if (right_constraint) {
            constraint = right_constraint ;
            delete left_constraint ;
        }
        left_constraint = 0 ; right_constraint = 0 ;
        break ;
    default :
        break ; // FIXME : What will be the type
    }
    // If any enumerated type is used as operand in binary operator, return type
    // will be type of enumerated literal.
    if (constraint && constraint->IsEnumConstraint()) {
        VeriConstraint *temp = constraint ;
        constraint = constraint->ElementConstraint() ;
        if (constraint) constraint = constraint->Copy() ;
        delete temp ;
    }
    return constraint ;
}
VeriConstraint *VeriQuestionColon::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    VeriConstraint *left = (_then_expr) ? _then_expr->EvaluateConstraintInternal(tab, df) : 0 ;
    VeriConstraint *right = (_else_expr) ? _else_expr->EvaluateConstraintInternal(tab, df) : 0 ;

    VeriConstraint *constraint = 0 ;
    if (left && right && (left->NumOfBits() > right->NumOfBits())) {
        constraint = left ;
        delete right ;
    } else {
        constraint = right ;
        delete left ;
    }
    return constraint ;
}
VeriConstraint *VeriCast::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    if (!_target_type) return 0 ;

    if (_target_type->IsConst()) {
        int size = _target_type->Integer() ;
        // VIPER #8287: Error out if size of traget type is zero/negative.
        if (!size || size < 0) {
            Error("zero or negative value for size") ; // VCS compliant
            return 0 ;
        }
        VeriConstraint *range = new VeriRangeConstraint(size -1, 0) ;
        range->SetAsPacked() ;
        return new VeriArrayConstraint(range, new VeriScalarTypeConstraint(VERI_REG)) ;
    } else if (_target_type->IsConstExpr()) {
        // VIPER #8287: Error out if size of traget type is zero/negative.
        if (VeriNode::IsStaticElab()) {
            VeriBaseValue *target_val = _target_type->StaticEvaluateInternal(0, 0, 0, 0) ;
            int size = target_val ? target_val->GetIntegerValue() : 0 ;
            delete target_val ;
            if (!size || size < 0) {
                Error("zero or negative value for size") ; // VCS compliant
                return 0 ;
            }
        }
        (void) tab ; // to prevent unused argument compiler warnings
        return _target_type->EvaluateConstraintInternal(tab, df) ;
    } else if ((_target_type->Type()==VERI_SIGNED) || (_target_type->Type()==VERI_UNSIGNED)) {
        return (_expr) ? _expr->EvaluateConstraintInternal(tab, df) : 0 ;
    } else {
        return _target_type->EvaluateConstraintInternal(tab, df) ;
    }
    return 0 ;
}
VeriConstraint *VeriConcatItem::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    return (_expr) ? _expr->EvaluateConstraintInternal(tab, df): 0 ;
}
VeriConstraint *VeriAssignmentPattern::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    // If type exists, constraint should be extracted from type,
    // otherwise constraint evaluation is not possible
    return (_type) ? _type->EvaluateConstraintInternal(tab, df): 0 ;
}
VeriConstraint *VeriMultiAssignmentPattern::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    // If type exists, constraint should be extracted from type,
    // otherwise constraint evaluation is not possible
    return (_type) ? _type->EvaluateConstraintInternal(tab, df): 0 ;
}
VeriConstraint *VeriStreamingConcat::EvaluateConstraintInternal(ValueTable * /*tab*/, VeriDataFlow * /*df*/)
{
    return 0 ; // Cannot evaluate its constraint
}
// Constraint evaluation for data types :
VeriConstraint *VeriDataType::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    // Create base constraint from built-in type :
    VeriConstraint *constraint = (_type) ? new VeriScalarTypeConstraint(_type) : new VeriScalarTypeConstraint(VERI_REG) ;

    // Incorporate packed dimensions
    VeriRange *dim = _dimensions ;
    VeriConstraint *range_constraint ;
    Array dim_arr(4) ;
    while (dim) {
        dim_arr.InsertLast(dim) ;
        dim = dim->GetNext() ;
    }
    // Create packed array constraint :
    unsigned i;
    FOREACH_ARRAY_ITEM_BACK(&dim_arr, i, dim) {
        range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
        if (!range_constraint) {
            delete constraint ;
            constraint = 0 ;
            break ;
        }
        range_constraint->SetAsPacked() ;
        constraint = new VeriArrayConstraint(range_constraint, constraint) ;
    }
    if (_signing && constraint) constraint->SetSign(_signing) ;
    return constraint ;
}
VeriConstraint *VeriNetDataType::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    return (_data_type) ? _data_type->EvaluateConstraintInternal(tab, df) : VeriDataType::EvaluateConstraintInternal(tab, df) ;
}
VeriConstraint *VeriTypeRef::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    if (!_id) return 0 ;
    //if (_id->IsClass()) {
        // If this is the typeref of a class, it is really a 'pointer'.
        // So it is like chandle
        //VeriConstraint *constraint = new VeriScalarTypeConstraint(VERI_CHANDLE) ;
        //VeriRange *dim = _id->GetDimensions() ;
    //}
    VeriConstraint *constraint = 0 ;
    if (_type_name && _type_name->IsScopeName()) {
        constraint = _type_name->EvaluateConstraintInternal(tab, df) ;
    }
    if (!constraint) constraint = _id->EvaluateConstraintInternal(tab, df, 0) ;

    // Incorporate packed dimensions
    VeriRange *dim = _dimensions ;
    VeriConstraint *range_constraint ;
    Array dim_arr(4) ;
    while (dim) {
        dim_arr.InsertLast(dim) ;
        dim = dim->GetNext() ;
    }
    // Create packed array constraint :
    unsigned i;
    FOREACH_ARRAY_ITEM_BACK(&dim_arr, i, dim) {
        range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
        if (!range_constraint) {
            delete constraint ;
            constraint = 0 ;
            break ;
        }
        range_constraint->SetAsPacked() ;
        constraint = new VeriArrayConstraint(range_constraint, constraint) ;
    }
    return constraint ;
}
VeriConstraint *VeriStructUnion::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    if (!_decls) return 0 ; // No element in structure/union

    // Use _scope if _present_scope is not set, so that we do not at-least leak here:
    VeriScope *scope = (_present_scope) ? _present_scope : _scope ;
    Array *element_constraints = (scope) ? scope->GetElementConstraints(this): 0 ;

    if (!element_constraints || !_ele_ids) {
        unsigned evaluate_ele_constraint = 0 ;
        if (!element_constraints) {
            element_constraints = new Array(_decls->Size()) ;
            evaluate_ele_constraint = 1 ;
        }
        unsigned add_ele_ids = 0 ;
        if (!_ele_ids) {
            _ele_ids = new Map(STRING_HASH, _decls->Size()) ;
            add_ele_ids = 1 ;
        }

        // Iterate over all members to create element constraint
        unsigned i, j ;
        VeriDataDecl *decl ;
        VeriConstraint *element_constraint ;
        VeriIdDef *id ;
        FOREACH_ARRAY_ITEM(_decls, i, decl) {
            Array *ids = decl ? decl->GetIds(): 0 ;
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id) continue ;
                // VIPER #7487 : If this id does NOT point to this datadecl's data_type,
                // then we should not consider it here. There must be multiple identifier
                // with same name in this structure
                if (decl && (id->GetDataType() != decl->GetDataType())) continue ;
                if (evaluate_ele_constraint) {
                    element_constraint = id->EvaluateConstraintInternal(tab, df, 0) ;
                    element_constraints->InsertLast(element_constraint) ;
                }
                if (add_ele_ids) (void) _ele_ids->Insert(id->Name(), id) ;
            }
        }
        if (evaluate_ele_constraint && scope) scope->AddElementConstraints(this, element_constraints) ;
    }
    VeriConstraint *constraint = new VeriRecordConstraint(element_constraints, _ele_ids, _type /* VERI_STRUCT or VERI_UNION */, _is_packed) ;

    // VIPER #4328 : Incorporate packed dimensions in struct union
    VeriRange *dim = _dimensions ;
    VeriConstraint *range_constraint ;
    Array dim_arr(4) ;
    while (dim) {
        dim_arr.InsertLast(dim) ;
        dim = dim->GetNext() ;
    }
    // Create packed array constraint :
    unsigned i ;
    FOREACH_ARRAY_ITEM_BACK(&dim_arr, i, dim) {
        range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
        if (!range_constraint) {
            delete constraint ;
            constraint = 0 ;
            break ;
        }
        range_constraint->SetAsPacked() ;
        constraint = new VeriArrayConstraint(range_constraint, constraint) ;
    }
    if (constraint) constraint->SetSign(GetSigning()) ;
    return constraint ;
}
VeriConstraint *VeriEnum::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    if (!_ids) return 0 ;

    // Create constraint for base type. If no base type exists, base type is int
    VeriConstraint *base = (_base_type) ? _base_type->EvaluateConstraintInternal(tab, df) : new VeriScalarTypeConstraint(VERI_INT) ;
    //return new VeriEnumConstraint(base, _ids) ;
    VeriConstraint *constraint = new VeriEnumConstraint(base, _ids) ;
    if (!_dimensions) return constraint ;

    // VIPER #5710: (vcs_compatibility-9000388573): Enum type may have packed dimension
    // Incorporate packed dimensions in enum
    VeriRange *dim = _dimensions ;
    VeriConstraint *range_constraint ;
    Array dim_arr(4) ;
    while (dim) {
        dim_arr.InsertLast(dim) ;
        dim = dim->GetNext() ;
    }
    // Create packed array constraint :
    unsigned i ;
    FOREACH_ARRAY_ITEM_BACK(&dim_arr, i, dim) {
        range_constraint = dim->EvaluateConstraintInternal(tab, df) ;
        if (!range_constraint) {
            delete constraint ;
            constraint = 0 ;
            break ;
        }
        range_constraint->SetAsPacked() ;
        constraint = new VeriArrayConstraint(range_constraint, constraint) ;
    }
    return constraint ;
}
VeriConstraint *VeriTypeOperator::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    return (_expr_or_data_type) ? _expr_or_data_type->EvaluateConstraintInternal(tab, df): 0 ;
}
VeriConstraint *VeriIndexedExpr::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    VeriConstraint *prefix_constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(tab, df): 0 ;
    if (!prefix_constraint) return 0 ; // Cannot do anything without prefix constraint
    if (!_idx) return prefix_constraint ;

    VeriConstraint *result = 0 ;
    if (_idx->IsRange()) { // Expression like vector[1:0]
        // Evaluate the slice bounds and create a new constraint.
        VeriConstraint *range = _idx->EvaluateConstraintInternal(tab, df) ;
        if (!range) {
            delete prefix_constraint ;
            return 0 ;
        }
        // VIPER #8281 : Mark range as packed if prefix is packed
        if (prefix_constraint->IsPacked()) range->SetAsPacked() ;
        VeriConstraint *ele_constraint = prefix_constraint->ElementConstraint() ;
        // Create a new Array constraint, using the element constraint
        // of the prefix as element constraint, and the newly created range
        // as the index constraint
        result = new VeriArrayConstraint(range, (ele_constraint) ? ele_constraint->Copy(): 0) ;
    } else { // Something like "vector[3]"
        // Return the element constraint of the prefix.
        VeriConstraint *ele_constraint = prefix_constraint->ElementConstraint() ;
        result = (ele_constraint) ? ele_constraint->Copy() : 0 ;
    }
    delete prefix_constraint ;
    return result ;
}

VeriConstraint *VeriRange::EvaluateConstraintInternal(ValueTable *tab, VeriDataFlow *df)
{
    (void) df ; // use of variable is product-config dependent
    VeriConstraint *range = 0 ;
    int left = 0 ;
    int right = 0 ;
    if (VeriNode::IsStaticElab()) {
        // VIPER #4723: The range can be a dynamic range like [$], in that case GetWidth()
        // returns 0. So, continue to create the constraint even if it returns 0.
        unsigned width = GetWidth(&left, &right, tab) ;
        // VIPER #4723: Width must be non-zero in case of non-dynamic range:
        if (!width && !IsDynamicRange()) return 0 ;
        range = new VeriRangeConstraint(left, right) ;
        if (!width) range->SetAsDynamicRange() ; // Since width is zero, this must be dynamic range here
        return range ;
    }
    (void) tab ; // to prevent unused argument compiler warnings
    return 0 ;
}
// VIPER #4740: Routine to create type of expression without producing any
// error. It is needed to check type of expression and later decide whether
// we should produce error or not
VeriTypeInfo *VeriExpression::CreateTypeWithoutError()
{
    //VeriTypeInfo::issue_msg = 1 ; // Do not issue warning/error
    VeriTypeInfo *ret_type = CheckType(0, NO_ENV) ;
    //VeriTypeInfo::issue_msg = 1 ; // issue warning/error
    //if (ret_type && (ret_type->IsTypeParameterType() || ret_type->IsUnresolvedNameType())) {
        //delete ret_type ;
        //ret_type = 0 ;
    //}
    return ret_type ;
}

unsigned
VeriRange::GetUnpackedWidth() const
{
     if (!_left && !_right) return 0 ;
     if (_left && !_left->IsConst()) return 0 ;
     if (_right && !_right->IsConst()) return 0 ;

     if (!_left && _right) return (_right->Integer() > 0 ? unsigned(_right->Integer()) : unsigned(-(_right->Integer()))) ;
     if (!_right && _left) return (_left->Integer() > 0 ? unsigned(_left->Integer()) : unsigned(-(_left->Integer()))) ;

     if (!_left || !_right) return 0 ;

     int left = _left->Integer() ;
     int right = _right->Integer() ;
     // Convert +:, -: to normal :
     switch (_part_select_token) {
     case VERI_PARTSELECT_DOWN:
         right = (left - right) + 1;
         break ;
     case VERI_PARTSELECT_UP:
         right = (left + right) - 1;
         break ;
     default:
         break;
     }
     return((left > right) ? unsigned((left - right) + 1) : unsigned((right - left) + 1)) ;
}

// VIPER #5048: Check whether the expression involves real in it or in its sub-expressions:
unsigned
VeriExpression::IsInvolvingReal()
{
    // As of now, use create-type and check for real. But later, we may want to write
    // a virtual routine on VeriExpression for checking if real is involved within.
    VeriTypeInfo *my_type = CheckType(0, NO_ENV) ;
    unsigned real_type = (my_type && my_type->IsRealType()) ? 1 : 0 ;
    delete my_type ;
    return real_type ;
}

void VeriNew::ResolveNewCall(VeriIdDef *func_id)
{
    if (!func_id) return ;
    // Viper 5116 Check whether is a copy constructor call
    // Do not call VeriFuctionCall::Resolve for copyconstructor calls
    if (!_copy_constructor && !_size_expr) {
        VeriName *new_func_name = new VeriIdRef(func_id) ;
        VeriFunctionCall *new_func = new VeriFunctionCall(new_func_name, _args) ;
        new_func->SetLinefile(Linefile()) ;
        new_func->Resolve(0, VERI_EXPRESSION) ; // Pass VERI_EXPRESSION by default Viper 5117
        new_func_name->SetId(0) ;
        (void) new_func->SetArgs(0) ;
        delete new_func ;
    }
}

unsigned
VeriConcat::CanBeAssignmentPattern(const VeriTypeInfo *context) const
{
    // If this concat or multi-concat uses concat item like label:value, default:value
    // then it should be treated as assignment pattern. If any one of the expressions
    // is a concat item all of them must be concat item, so, just check the first:
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        if (expr->IsConcatItem()) return 1 ;
        break ;
    }

    // If this concat or multi-concat used in unpacked array or unpacked structure
    // type environment, then it can be treated as assignment pattern:
    if (context && (context->IsUnpackedArrayType() ||
        (context->IsUnpacked() && context->IsStructure()))) return 1 ;

    return 0 ; // Cannot be an assignment pattern
}

unsigned
VeriMultiConcat::CanBeAssignmentPattern(const VeriTypeInfo *context) const
{
    // If this concat or multi-concat uses concat item like label:value, default:value
    // then it should be treated as assignment pattern. If any one of the expressions
    // is a concat item all of them must be concat item, so, just check the first:
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (!expr) continue ;
        if (expr->IsConcatItem()) return 1 ;
        break ;
    }

    // If this concat or multi-concat used in unpacked array or unpacked structure
    // type environment, then it can be treated as assignment pattern:
    if (context && (context->IsUnpackedArrayType() ||
        (context->IsUnpacked() && context->IsStructure()))) return 1 ;

    return 0 ; // Cannot be an assignment pattern
}
// VIPER #7308 : Determine if expression contains interface instance/modport
unsigned VeriExpression::HasInterfaceInstOrPort() const
{
    VeriIdDef *this_id = GetId() ;

    if (this_id && (this_id->IsInterfaceInst() || this_id->IsInterfacePort())) return 1 ; // Contains interface instance/modport
    return 0 ;
}
unsigned VeriConcat::HasInterfaceInstOrPort() const
{
    unsigned i ;
    VeriExpression *expr ;
    FOREACH_ARRAY_ITEM(_exprs, i, expr) {
        if (expr && expr->HasInterfaceInstOrPort()) return 1 ;
    }
    return 0 ;
}

VeriRange *
VeriExpression::CreateDimensions(unsigned only_unpacked) const
{
    if (only_unpacked) return 0 ; // Do not know about unpacked dimensions

    // Default catcher: [$bits(this)-1:0]
    // Note that for single value range like [N] it is treated as [0:N-1]
    // But for dimention of an expression, simulators treat it as [size-1:0]
    Array *dollar_bits_args = new Array(1) ;
    VeriMapForCopy id_map_table ;
    dollar_bits_args->InsertLast(CopyExpression(id_map_table)) ;
    // CHECKME: Linefile info?
    // VIPER #7802: Set NULL linefile info since these are not in the source RTL but we are creating them:
    VeriExpression *left_oper = new VeriSystemFunctionCall(Strings::save("bits"), dollar_bits_args) ; left_oper->SetLinefile(0) ; //Linefile()) ;
    VeriExpression *right_oper = new VeriIntVal(1) ; right_oper->SetLinefile(0) ; //Linefile()) ;
    VeriExpression *msb = new VeriBinaryOperator(VERI_MIN, left_oper, right_oper) ; msb->SetLinefile(0) ; //Linefile()) ;
    VeriExpression *lsb = new VeriIntVal(0) ; lsb->SetLinefile(0) ; //Linefile()) ;
    VeriRange *result = new VeriRange(msb, lsb) ; result->SetLinefile(0) ; //Linefile()) ;
    return result ;
}

VeriRange *
VeriIdRef::CreateDimensions(unsigned only_unpacked) const
{
    return (_id) ? _id->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriIndexedId::CreateDimensions(unsigned only_unpacked) const
{
    VeriRange *result = (_id) ? _id->CreateDimensions(only_unpacked) : 0 ;
    if (!result || !_idx) return 0 ;

    VeriRange *tmp = result->GetNext() ;
    result->SetNextNull() ;
    delete result ;
    if (_idx->IsRange()) {
        // First dimension will be changed to this range
        // bit x[2:0][3:0] ;
        // CreateDimensions for 'x[1:0]' should return [1:0][3:0]
        VeriRange *this_idx = _idx->RangeCast() ;
        VeriMapForCopy old2new ;
        if (this_idx) {
            result = this_idx->CopyRange(old2new) ;
            if (result) result->SetNext(tmp) ;
        } else {
            result = tmp ;
        }
    } else {
        // Skip the first index which is already selected here:
        result = tmp ;
    }

    return result ;
}

VeriRange *
VeriSelectedName::CreateDimensions(unsigned only_unpacked) const
{
    return (_suffix_id) ? _suffix_id->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriIndexedMemoryId::CreateDimensions(unsigned only_unpacked) const
{
    VeriRange *result = (_id) ? _id->CreateDimensions(only_unpacked) : 0 ;
    if (!result) return 0 ;

    VeriRange *tmp ;
    unsigned idx_size = (_indexes) ? _indexes->Size() : 0 ;
    VeriExpression *last_index = (_indexes && _indexes->Size()) ? (VeriExpression*)_indexes->GetLast(): 0 ;
    // Reduce dimension excluding last index, if last index is range
    if (last_index && last_index->IsRange()) idx_size-- ;
    while (result && idx_size--) {
        tmp = result->GetNext() ;
        result->SetNextNull() ;
        delete result ;
        result = tmp ;
    }
    if (last_index && last_index->IsRange()) {
        // If last index is range, dimension should not reduce. Range specified
        // here should be new range
        tmp = result ? result->GetNext(): 0 ;
        if (result) result->SetNextNull() ;
        delete result ;
        VeriRange *this_idx = last_index->RangeCast() ;
        VeriMapForCopy old2new ;
        if (this_idx) {
            result = this_idx->CopyRange(old2new) ;
            if (result) result->SetNext(tmp) ;
        } else {
            result = tmp ;
        }
    }
    return result ;
}

VeriRange *
VeriIndexedExpr::CreateDimensions(unsigned only_unpacked) const
{
    if (only_unpacked) return 0 ; // Do not know about unpacked dimensions

    // The index dimension is the actual dimension here:
    return (_idx) ? _idx->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriFunctionCall::CreateDimensions(unsigned only_unpacked) const
{
    VeriIdDef *fn_id = GetId() ;
    return (fn_id) ? fn_id->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriMinTypMaxExpr::CreateDimensions(unsigned only_unpacked) const
{
    return (_typ_expr) ? _typ_expr->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriPortConnect::CreateDimensions(unsigned only_unpacked) const
{
    return (_connection) ? _connection->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriAnsiPortDecl::CreateDimensions(unsigned only_unpacked) const
{
    // CHECKME: Declared ids may have unpacked dimensions on their own.
    if (only_unpacked) return 0 ; // Do not know about unpacked dimensions

    return (_data_type) ? _data_type->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriRange::CreateDimensions(unsigned /*only_unpacked*/) const
{
    // CHECKME: return VeriExpression::CreateDimensions(only_unpacked)?
    VeriMapForCopy id_map_table ;
    return CopyRange(id_map_table) ;
}

VeriRange *
VeriDataType::CreateDimensions(unsigned only_unpacked) const
{
    if (only_unpacked) return 0 ; // Dimension of data type is always packed

    return (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriTypeRef::CreateDimensions(unsigned only_unpacked) const
{
    // _id can have more unpacked dimensions:
    VeriRange *packed_dims = (_id /*&& !only_unpacked*/) ? _id->CreateDimensions(only_unpacked) : 0 ;
    VeriRange *unpacked_dims = (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;

    VeriRange *result = 0 ;
    if (!packed_dims) result = unpacked_dims ;
    if (!unpacked_dims) result = packed_dims ;

    if (!result && packed_dims && unpacked_dims) {
        result = unpacked_dims ;
        while (unpacked_dims->GetNext()) unpacked_dims = unpacked_dims->GetNext() ;
        unpacked_dims->SetNext(packed_dims) ;
    }

    return result ;
}

VeriRange *
VeriStructUnion::CreateDimensions(unsigned only_unpacked) const
{
    if (only_unpacked) return 0 ; // Dimension of data type is always packed

    return (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriEnum::CreateDimensions(unsigned only_unpacked) const
{
    if (only_unpacked) return 0 ; // Dimension of data type is always packed

    VeriRange *packed_dims = (_dimensions) ? _dimensions->CreateDimensions(only_unpacked) : 0 ;
    VeriRange *unpacked_dims = (_base_type) ? _base_type->CreateDimensions(only_unpacked) : 0 ;

    VeriRange *result = 0 ;
    if (!packed_dims) result = unpacked_dims ;
    if (!unpacked_dims) result = packed_dims ;

    if (!result && packed_dims && unpacked_dims) {
        result = unpacked_dims ;
        while (unpacked_dims->GetNext()) unpacked_dims = unpacked_dims->GetNext() ;
        unpacked_dims->SetNext(packed_dims) ;
    }

    return result ;
}

VeriRange *
VeriCast::CreateDimensions(unsigned only_unpacked) const
{
    return (_target_type) ? _target_type->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriConcatItem::CreateDimensions(unsigned only_unpacked) const
{
    VeriIdDef *type_id = (_member_label) ? _member_label->GetId() : 0 ;
    return (type_id) ? type_id->CreateDimensions(only_unpacked) : ((_expr) ? _expr->CreateDimensions(only_unpacked) : 0) ;
}

VeriRange *
VeriAssignmentPattern::CreateDimensions(unsigned only_unpacked) const
{
    return (_type) ? _type->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriMultiAssignmentPattern::CreateDimensions(unsigned only_unpacked) const
{
    return (_type) ? _type->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriStreamingConcat::CreateDimensions(unsigned only_unpacked) const
{
    return (_slice_size) ? _slice_size->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriTaggedUnion::CreateDimensions(unsigned only_unpacked) const
{
    VeriIdDef *type_id = (_member) ? _member->GetId() : 0 ;
    return (type_id) ? type_id->CreateDimensions(only_unpacked) : ((_expr) ? _expr->CreateDimensions(only_unpacked) : 0) ;
}

VeriRange *
VeriWithExpr::CreateDimensions(unsigned only_unpacked) const
{
    return (_left) ? _left->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriInlineConstraint::CreateDimensions(unsigned only_unpacked) const
{
    return (_left) ? _left->CreateDimensions(only_unpacked) : 0 ;
}

VeriRange *
VeriTypeOperator::CreateDimensions(unsigned only_unpacked) const
{
    return (_expr_or_data_type) ? _expr_or_data_type->CreateDimensions(only_unpacked) : 0 ;
}

unsigned
VeriCast::SetTargetType(VeriExpression *new_type)
{
    if (!new_type) return 0 ;

    delete _target_type ;
    _target_type = new_type ;

    return 1 ;
}

unsigned
VeriAssignmentPattern::SetTargetType(VeriExpression *new_type)
{
    if (!new_type) return 0 ;

    delete _type ;
    _type = new_type ;

    return 1 ;
}

unsigned
VeriMultiAssignmentPattern::SetTargetType(VeriExpression *new_type)
{
    if (!new_type) return 0 ;

    delete _type ;
    _type = new_type ;

    return 1 ;
}

// VIPER #5335 : Create a data type from the expression (overwritten value of type parameter)
// VIPER #7188 : Make this API virtual to handle every type actual properly.
VeriDataType *VeriExpression::CreateDataType(const VeriIdDef *formal, unsigned from_typeof) const
{
    VeriDataType *actual_type = 0 ;
    // First, Check if this actual is a VeriDataType :
    VeriDataType *actual_data_type = DataTypeCast() ;
    if (!actual_data_type && (GetFunctionType()==VERI_SYS_CALL_TYPEOF)) {
        // VIPER #6815: Make $typeof system function call behave similar to SV type() operator
        Array *args = GetArgs() ; // Get the argument via the GetArgs() API
        VeriExpression *arg = (args && args->Size()) ? (VeriExpression *)args->GetFirst() : 0 ;
        // VIPER #7188 : Extract the data type from the argument of type/typeof
        actual_data_type = arg ? arg->CreateDataType(formal, 1): 0 ;
        return actual_data_type ;
    }
    // Here it must be a named data type actual :
    // Possibly a named data type, written with an indexed (sliced) name :
    VeriIdDef *actual_type_id = GetId() ;
    // When from_typeof is 1, non-type identifier can come (type(var))
    if (!from_typeof && (!actual_type_id || !actual_type_id->IsType())) {
        Error("actual for type parameter %s should be a data type",formal->Name()) ;
        return 0 ;
    }
    // Actual is type identifier
    if (actual_type_id && actual_type_id->IsType()) {
        // Check dimensions :
        VeriExpression *dimensions = GetIndexExpr() ; // FIX ME : Use GetDimensions().

        // VIPER #2427 : Support one dimension in actual types by incorporating
        // evaluated dimension in new actual node.
        VeriRange *new_dimensions = 0 ;
        if (dimensions && dimensions->IsRange() && !dimensions->GetPartSelectToken()) {
            VeriExpression *left = dimensions->GetLeft() ;
            VeriExpression *right = dimensions->GetRight() ;
            if (left && right) { // Ignore []
                if (!new_dimensions && VeriNode::IsStaticElab()) {
                    int left_val = 0; int right_val = 0 ;
                    unsigned width = dimensions->GetWidth(&left_val, &right_val, 0) ;
                    if (!width) {
                        Error("range index value is not constant") ;
                    } else {
                        // Create new range node
                        new_dimensions = new VeriRange(new VeriIntVal(left_val), new VeriIntVal(right_val), 0) ;
                    }
                }
            }
        } else {
            // Only expression is used, range is [size] and it is not
            // allowed as packed dimension. Issue error
            if (dimensions) Error("only [expr1:expr2] syntax is allowed for packed ranges of types") ;
        }
        // FIXME : Error out for indexed memory id
        // There is never any signing.
        actual_type = new VeriTypeRef(actual_type_id, 0, new_dimensions) ;
    } else if (actual_type_id && FullId()) {
        // VIPER #7188 : Actual is non-type identifier, extract data type from the id
        VeriDataType *id_type = actual_type_id->GetDataType() ;
        actual_type = id_type ? id_type->CreateDataType(formal, 0) : 0 ;
    } else {
        // VIPER #7188 : Argument of type/typeof is complex expression, create
        // type operator with that expression
        VeriMapForCopy old2new ;
        actual_type = new VeriTypeOperator(CopyExpression(old2new)) ;
    }
    if (actual_type) actual_type->SetLinefile(Linefile()) ; // set temp linefile info
    // VIPER 2427 : Resolve created type to give error for packed
    // dimensions on unpacked types
    if (actual_type) actual_type->Resolve(0, VERI_TYPE_ENV) ;
    return actual_type ;
}

VeriDataType *VeriDataType::CreateDataType(const VeriIdDef * /*formal*/, unsigned /*from_typeof*/) const
{
    // VIPER #2427 : Support dimensions in actual types by incorporating
    // evaluated dimensions in new actual node.
    VeriRange *dimensions = _dimensions ;
    VeriRange *new_dims = 0 ;
    while(dimensions) {
        VeriExpression *left = dimensions->GetLeft() ;
        VeriExpression *right = dimensions->GetRight() ;
        if (!left && !right) continue ; // Dimension is []
        if (!left || !right) { // Dimension is [size] : not allowed as packed dimension
            Error("only [expr1:expr2] syntax is allowed for packed ranges of types") ;
            break ;
        }
        VeriRange *range = 0 ;
        if (!range && VeriNode::IsStaticElab()) {
            int left_val = 0; int right_val = 0 ;
            unsigned width = dimensions->GetWidth(&left_val, &right_val, 0) ;
            if (!width) {
                Error("range index value is not constant") ;
                break ;
            }
            // Create new range node
            range = new VeriRange(new VeriIntVal(left_val), new VeriIntVal(right_val), 0) ;
            if (!new_dims) { // First dimension
                new_dims = range ;
            } else { // Set new range as next dimension
                new_dims->SetNext(range) ;
            }
        }
        dimensions = dimensions->GetNext() ; // get next dimension
    }
    // Create new actual node :
    VeriDataType *actual_type = new VeriDataType(_type,_signing,new_dims) ;
    actual_type->SetLinefile(Linefile()) ; // set temp linefile info
    // VIPER 2427 : Resolve created type to give error for packed
    // dimensions on unpacked types
    actual_type->Resolve(0, VERI_TYPE_ENV) ;
    return actual_type ;
}
VeriDataType *VeriNetDataType::CreateDataType(const VeriIdDef * formal, unsigned /*from_typeof*/) const
{
    VeriDataType *actual_type = _data_type ? _data_type->CreateDataType(formal, 0): 0 ;
    if (!actual_type) return 0 ;
    actual_type = new VeriNetDataType(_type, actual_type) ;
    actual_type->SetLinefile(Linefile()) ; // set temp linefile info
    // VIPER 2427 : Resolve created type to give error for packed
    // dimensions on unpacked types
    actual_type->Resolve(0, VERI_TYPE_ENV) ;
    return actual_type ;
}
VeriDataType *VeriEnum::CreateDataType(const VeriIdDef * /*formal*/, unsigned /*from_typeof*/) const
{
    VeriMapForCopy old2new ;
    return CopyDataType(old2new) ;
}
VeriDataType *VeriStructUnion::CreateDataType(const VeriIdDef * /*formal*/, unsigned /*from_typeof*/) const
{
    VeriMapForCopy old2new ;
    return CopyDataType(old2new) ; // FIXME : Need to evaluate dimensions
}
VeriDataType *VeriTypeOperator::CreateDataType(const VeriIdDef *formal, unsigned /*from_typeof*/) const
{
    (void) GetType() ; // To catch errors
    VeriDataType *actual_type = _expr_or_data_type ? _expr_or_data_type->CreateDataType(formal, 1): 0 ;
    if (actual_type) actual_type->SetLinefile(Linefile()) ; // set temp linefile info
    // VIPER 2427 : Resolve created type to give error for packed
    // dimensions on unpacked types
    if (actual_type) actual_type->Resolve(0, VERI_TYPE_ENV) ;
    return actual_type ;
}
VeriDataType *VeriTypeRef::CreateDataType(const VeriIdDef * /*formal*/, unsigned /*from_typeof*/) const
{
    if (!_id) return 0 ;

    VeriRange *dimensions = _dimensions ;
    VeriRange *new_dims = 0 ;
    while(dimensions) {
        VeriExpression *left = dimensions->GetLeft() ;
        VeriExpression *right = dimensions->GetRight() ;
        if (!left && !right) {
            dimensions = dimensions->GetNext() ; // get next dimension
            continue ; // Dimension is []
        }
        if (!left || !right) { // Dimension is [size] : not allowed as packed dimension
            Error("only [expr1:expr2] syntax is allowed for packed ranges of types") ;
            break ;
        }
        VeriRange *range = 0 ;
        if (!range && VeriNode::IsStaticElab()) {
            int left_val = 0; int right_val = 0 ;
            unsigned width = dimensions->GetWidth(&left_val, &right_val, 0) ;
            if (!width) {
                Error("range index value is not constant") ;
                break ;
            }
            // Create new range node
            range = new VeriRange(new VeriIntVal(left_val), new VeriIntVal(right_val), 0) ;
            if (!new_dims) { // First dimension
                new_dims = range ;
            } else { // Set new range as next dimension
                new_dims->SetNext(range) ;
            }
        }
        dimensions = dimensions->GetNext() ; // get next dimension
    }
    VeriDataType *actual_type = new VeriTypeRef(_id, _signing, new_dims) ;
    actual_type->SetLinefile(Linefile()) ; // set temp linefile info
    // VIPER 2427 : Resolve created type to give error for packed
    // dimensions on unpacked types
    actual_type->Resolve(0, VERI_TYPE_ENV) ;
    return actual_type ;
}

unsigned
VeriExpression::IsEqualLiteral(const VeriExpression *other) const
{
    // VIPER #8251: Check for equal literal value/types
    if (!other) return 0 ;
    if (!IsConst() || !other->IsConst()) return 0 ;

    char *my_image = Image() ;
    char *other_image = other->Image() ;

    unsigned result = Strings::compare(my_image, other_image) ;
    Strings::free(other_image) ;
    Strings::free(my_image) ;

    return result ;
}

