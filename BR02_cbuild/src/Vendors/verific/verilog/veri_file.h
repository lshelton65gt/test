/*
 *
 * [ File Version : 1.249 - 2014/03/20 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_FILE_H_
#define _VERIFIC_VERI_FILE_H_

#include "VerificSystem.h"
#include "VeriCompileFlags.h" // Verilog-specific compile flags
#include "RuntimeFlags.h" // For 'global' run-time variables.
#include "LineFile.h" // we use line/file type in header

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Map ;
class Set ;
class VeriModule ;
class VeriExpression ;
class Library ;
class Netlist ;
class VeriLibrary ;
class VeriLibraryDecl ;
class VeriScope ;
class Protect ;
class VeriStaticElaborator ;
class Cell ;
#ifdef VERILOG_FLEX_READ_FROM_STREAM
class verific_stream ;
#endif

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

// First, macros to iterate over parse trees of analyzed Verilog modules.

// To iterate over all module parse-tree's that were analyzed :
// use FOREACH_VERILOG_MODULE :
//       argument       type            function
//        MI        :   MapIter         An iterator
//        MODULETREE:   VeriModule *    Returned Pointer to the module parse tree (include VeriModule.h before using member functions)

#define FOREACH_VERILOG_MODULE(MI,MODULETREE)   FOREACH_MAP_ITEM(VN::veri_file::AllModules(), MI, 0, &(MODULETREE))

//  To iterate over all (Verilog pre-processor) macro's defined during the
//  lifetime of a 'veri_file' object : use FOREACH_VERILOG_MACRO
//
//       argument      type            function
//        VERIFILE :   veri_file*      The veri_file object that contains the macrodefs
//        MI       :   MapIter         An iterator
//        MACRONAME:   const char *    Returned name of the macro   E.g. "mymacro"
//        MACROVALUE:  const char *    Returned value of the macro  E.g. "myvalue"

#define FOREACH_VERILOG_MACRO(VERIFILE,MI,MACRONAME,MACROVALUE) if (VERIFILE) FOREACH_MAP_ITEM((VERIFILE)->AllMacroDefs(), MI, &(MACRONAME), &(MACROVALUE))

// To iterate over all library unit parse-tree's that were ever analyzed :
// use FOREACH_VERILOG_LIBRARY :
//       argument       type            function
//        MI        :   MapIter         An iterator
//        LIBRARYTREE:  VeriLibrary *   Returned Pointer to the library parse tree (include Units.h before using member functions)

#define FOREACH_VERILOG_LIBRARY(MI,LIBRARYTREE) FOREACH_MAP_ITEM(VN::veri_file::AllLibraries(), MI, 0, &(LIBRARYTREE))

// Look in VeriLibrary.h to check iterators and member functions on VeriLibrary structures

/* -------------------------------------------------------------- */

/*
    veri_file is a placeholder to call the Verilog IEEE 1364 RTL reader.
    Typical calling sequence is as follows :

      veri_file f ;
      f.Read("veri_file_name","work") ;
    In absence of the 'work_lib' name in the Read command,
    the library called "work" will be used to store the cells defined
    in the Verilog file.

    The Read command will create hierarchical database objects
    from all objects defined in the source file.
    The reader uses the Libset::Global() command to find the
    libraries, cells and netlists that are mentioned in the Verilog
    file.

    If the reader finds a library, cell or netlist that is already
    present in the database, it will overwrite the existing database
    object, issuing a warning.
    If the reader does not find a library, cell or netlist in the
    database, it will create a new one.

    The reader creates a parse-tree while reading the Verilog file.
    This parse-tree is NOT deleted at the end of the Read command,
    since subsequent calls of Read might need the Verilog parse-trees
    of previously parsed modules to create a proper result.

    Applications should call the RemoveAllModules() routine after
    all Verilog files have been read in. This will clean up all the
    parse-tree structures of previous Read() calls.

    **** Analyze / Elaborate ****

    Language synthesis (for RTL) goes through two steps, which have separate routines :

   'Analyze()'  which will do syntax checks and create parse-trees
            for all modules in the file and the resulting parse
            tree is stored in a module library.
            Default library is called "work", but this can be
            overridden in this Analyze() call.

   'Elaborate()' which takes one top-level module name (and optional library name),
            finds the parse-tree for it, and executes the design
            starting from this top-level all the way down the hierarchy.
            During elaboration, Netlist objects will be created in the
            Verific hierarchical Netlist design database.
            The Netlist::PresentDesign() pointer will be set to the top-level Netlist.

    There is also a second elaboration routine, which does not require a
    specific top-level module name to be mentioned :

    'ElaborateAll()' This routine will execute every module in the design
            as if it is a top-level module. All modules in the design will obtain
            an equivalent Netlist in the Verific Netlist database.
            This routine is useful if a library of cells is read in, or if you simply
            do not know which module is the top-level module.
            The Netlist::PresentDesign() pointer will be set to one Netlist which
            comes from a Verilog top-level module.

    If multiple files are read in, it is preferable to first 'Analyze()'
    every file before calling 'Elaborate()'. This will make sure that
    modules that are instantiated are actually known to the reader.
    Otherwise, the elaborator will need to create a unknown module
    when instantiating a module that is not yet analyzed. The creation
    of unknown modules has to make many assumptions about the modules
    boundary (port names/direction/size etc) so it is error-prone.

    Analyze() parses the entire IEEE 1364 language and stores in in parse-tree.

    Elaborate() supports only a subset of the Verilog language.
    The subset is restricted to RTL statements (behavioral and structural),
    and conforms with the subsets parsed by popular logic synthesis tools.
    The elaborator does NOT do anything with timing information and other
    'non-synthesizable' constructs in the Verilog file.
    For details on the exact subset supported, in a particular release of the code,
    please contact us at info@verific.com.

    Read actually calls Analyze() on the file, followed by ElaborateAll(),
    so EVERY module read in gets elaborated and will create a Netlist object
    in the database.

    **** Line/Filename information ****

    For the logic that is created by the elaborator, every Instance, Port
    Netlist and Cell that is created is annotated with LineFile information.
    This will make the line number and file name from where the object was
    created available to applications that operate on the database.
    The LineFile info is stored in a field of the (DesignObj) object.

    **** Print a parse-tree ****

    'PrettyPrint()' will dump the parse tree for one module into a file
    with Verilog syntax. Since the parse-tree preserves all Verilog
    file information (except for pre-processor statements and comments),
    the pretty-print version will look very much like the original file.

    **** Verilog Macro handling ****

    The veri_file object also has some routines to handle Verilog
    macro's (before or after a file is read). These routines are

        unsigned DefineMacro(const char *macro_name, const char *macro_value = 0, const char *macro_formal_arguments = 0) ;
        unsigned UndefineMacro(const char *macro_name) ;
        void     UndefineAllMacros() ;

    examples : define a macro before a Verilog file is read in :

        DefineMacro("mymacro")                   as `define mymacro
        DefineMacro("mymacro","myvalue")         as `define mymacro myvalue
        DefineMacro("mymacro","a+b","(a,b)")     as `define mymacro(a,b) a+b

    **** Verilog XL Compliance ****

    The veri_file object has Verilog XL support for the -y option with the AddYDir()
    routine. This allows you to add a (search) directory where the reader can
    look for unknown modules (modules that are not yet analyzed, but are instantiated
    during analysis).
    The veri_file object also has a AddVFile() to support the Verilog XL -v
    option. This allows you to add a (search) file where the unknown modules
    (modules that are not yet analyze, but are instantiated during analysis)
    will be looked up.

    Also, Verilog XL -v support can be mimiced by calling Analyze() on the (-v) file name.

    Routine SetUpperCaseIds() implements XL's -u option (upper-case all identifiers (making Verilog case-insensitive)
    This routine is also 'static', so once set, it retains it behavior over multiple calls to Analyze.
    Re-reset it SetUpperCaseIds(0) to go back to original (case-sensitive) parsing

    **** Access to all analyzed modules ****

    We try to keep the parse-tree's contents hidden for any applications,
    to increase object-oriented programming. If you need to write an
    application function on the parse-trees, we recommend you write it
    as a member function of the parse-tree object classes.

    However, if you just need 'shallow' access to the analyzed module
    parse-trees (for example, to find out which modules have been analyzed),
    use the (static) veri_file::AllModules() and veri_file::GetModule()
    functions to get access to the parse-trees of the modules.

    Make sure to include header file VeriModule.h if you want to use any
    VeriModule access functions (like Name()).

    Here are two example application code pieces of how to use the parse-tree
    access routines :

    1) To find out if a particular module (of which you know the name) is
       analyzed or not :

            #include "VeriModule.h"

            VeriModule *themodule = veri_file::GetModule("foo") ;
        'themodule' is a pointer to the parse-tree of module "foo".
        If module named "foo" is NOT analyzed yet, 'themodule' is 0.
        Now you can use all VeriModule class member functions to check the module.
        Check VeriModule.h for these member functions.

        For example :

            themodule->Name()           returns the name of the module
            themodule->IsCompiled()     tells if the module is already elaborated or not.
            themodule->GetScope()       returns a Map of name->VeriIdDef pointer (identifier parse tree nodes) of identifiers declared inside the module
            themodule->Parameters()     returns an array of VeriIdDef pointers to parameters declared inside the module. In order of declaration.
            themodule->Ports()          returns an array of VeriIdDef pointers to ports declared on the module. In order of declaration.
            ...

    2) To iterate over all modules that were analyzed in the _work_library:

            #include "VeriModule.h"

            const char *name ;    // <- name of the module ends up here.
            VeriModule *module ;  // <- pointer to the module's parse tree.
            MapIter mi ;

            FOREACH_MAP_ITEM(veri_file::AllModules(), mi, &name, &module) {
              // Do something with 'module'
            }

        Alternatively, use the predefined iterator : FOREACH_VERILOG_MODULE :

            FOREACH_VERILOG_MODULE(mi, module) {
              // Do something with 'module'
              name = module->Name() ;
            }

    NOTE: Verilog designs with uselib will be spread out over multiple
          libararies, but that the main design will still be in the
          library "work".

    Lastly, the general Verilog API will expand over time, and we always do
    our best to support backward compatibility between releases.
*/
class VFC_DLL_PORT veri_file
{
public:
    veri_file() { } ;

    // Language analysis mode. Encoding is historically defined as integer argument to Analyze and Read :
    // VIPER #5965: Added SYSTEM_VERILOG_2005 member with value 2. Note the change of meaning of 2 in this case.
    //CARBON_BEGIN
    typedef enum { VERILOG_95=0, VERILOG_2K=1, SYSTEM_VERILOG_2005=2, SYSTEM_VERILOG_2009=3, SYSTEM_VERILOG=4, VERILOG_AMS=5, VERILOG_PSL=6, UNDEFINED } verilog_lang_variant ;
    //CARBON_END

    //CARBON_BEGIN
    static bool IsAnySystemVerilogVariant(verilog_lang_variant val) {
      // implemented as a switch so that any change to the enum will cause compiler warning due to unhandled case (thus do not add default to this switch stmt)
      switch (val) {
      case SYSTEM_VERILOG_2005: /* fallthrough */
      case SYSTEM_VERILOG_2009: /* fallthrough */
      case SYSTEM_VERILOG: return true;
      case VERILOG_95:          /* fallthrough */
      case VERILOG_2K:          /* fallthrough */
      case VERILOG_AMS:         /* fallthrough */
      case VERILOG_PSL:         /* fallthrough */
      case UNDEFINED: return false;
      }
      return false; // can never get here, but this line keeps compiler from generating warning about potentially no return value
    }
    //CARBON_END

    // Compilation unit mode. Can be multi-file compilation unit (MFCU) i.e. for
    // multiple files specified in command line only one compilation unit will be
    // created. Another mode can be single-file compilation unit(SFCU) i.e. for
    // each file specified in command line different compilation unit (if needed)
    // will be created.
    enum {MFCU=0, SFCU=1, NO_MODE} ;

    // VIPER #6604: Flags for the ProcessFFile API (initially only one for now):
    // F_FILE_CAPITAL: To treat the file name being processed as -F instead of -f:
    typedef enum { F_FILE_NONE=0x0, F_FILE_CAPITAL=0x1 } f_file_flags ;

    // VIPER #6878: Different mode of pretty printing attributes and pragmas:
    // PRINT_AS_ORIGINAL  : Prints RTL attributes as attribute, RTL pragmas as pragma
    // PRINT_AS_ATTRIBUTE : Prints RTL attributes and pragmas as attribute
    // PRINT_AS_PRAGMA    : Prints RTL attributes and pragmas as as pragma
    typedef enum { PRINT_AS_ORIGINAL=0, PRINT_AS_ATTRIBUTE=1, PRINT_AS_PRAGMA=2 } attr_pretty_print_mode ;

    // Pre-process the file(s) and write the pre-processed output into the given file:
    static unsigned      PreProcess(const char *file_name, const char *out_file, unsigned verilog_mode=VERILOG_2K) ;
    static unsigned      PreProcessMultipleFiles(const Array *file_names, const char *out_file, unsigned verilog_mode=VERILOG_2K, unsigned cu_mode=NO_MODE) ;

    // Analyze (build parse-trees), and perform basic syntax/semantic checks
    static unsigned      Analyze(const char *file_name, unsigned verilog_mode=VERILOG_2K, const char *lib_name="work", unsigned cu_mode=NO_MODE) ;

    // Analyze input string into a Verilog expression
    static VeriExpression* AnalyzeExpr(const char *expr, unsigned verilog_mode=VERILOG_2K, const linefile_type line_file = 0) ;

    // VIPER #2932: API to analyze multiple files.
    static unsigned      AnalyzeMultipleFiles(const Array *file_names, unsigned verilog_mode=VERILOG_2K, const char *lib_name="work", unsigned cu_mode=NO_MODE) ;

    // API to process -f files. It returns all the file names encountered and sets all other options:
    // VIPER #6604: The argument 'flags' supplies different settings for the processing. For now,
    // we accept only 'F_FILE_CAPITAL'. This indicates if the file was an argument of '-F'.
    // For -F we prepend path of the 'file_name' to each relative file specified within it.
    // The argument 'analysis_mode' is an output argument; it is set from the -f/-F file.
    // Caller of this function is responsible to delete the returned Array and its char * content.
    // Note that some of the tools that support -f files, works in Verilog 95 mode by default.
    // So, you may choose to do so to match them by using VERILOG_95 mode if this API does not
    // set the analysis_mode argument after it is called. The other way is to initialize the
    // analysis_mode argument to VERILOG_95 and use it normally for AnalyzeMultipleFiles() call.
    static Array *       ProcessFFile(const char *file_name, f_file_flags flags, unsigned &analysis_mode) ;

    // PrettyPrint a module that was previously analyzed
    // VIPER #6878: Provide way to control pretty print of attributes and pragmas (default is print as what was specified in RTL):
    static unsigned      PrettyPrint(const char *file_name, const char *module_name, const char *work_lib=0, attr_pretty_print_mode mode=PRINT_AS_ORIGINAL) ;

    // Print the modules to original file name path appended with suffix (if specified).  If output_directory is defined, then output files get written to this directory.
    static unsigned      PrettyPrintDesign(const char *suffix, const char *output_directory=0, const char *module_name=0, const char *work_lib=0) ;

    // Perform semantic checks and cross-module resolving of all parsed modules. This routine should be run after entire design is parsed. Also does -v/-y module resolving.
    static unsigned      AnalyzeFull(const char *work_lib=0) ;

    // Process user libraries : process all -y/-v/uselib specified library files
    // to find all unresolved modules of design files.
    static void          ProcessUserLibraries() ;

    // Top-level veri_file reset routine (calls all appropriate reset/cleanup routines)
    static void          Reset() ;

    // Call this API to reset the parser to go to the initial state.
    // It will delete all data-structure (including parse trees) created on veri_file.
    // Note that it does not reset flag setting like SetIgnoreTranslateOff().
    // Does not delete the netlist database created with RTL elaboration, if any, either.
    static void          ResetParser() ;

    // Routine to end a compilation end. This routine should be called to mark the
    // end of a compilation unit. This will do all sorts of removal.
    static void          EndCompilationUnit() ;

    // Routine to start compilation unit. This should is called from lex only. This
    // routine creates a new root module and set global present_scope pointer of yacc
    static void          StartCompilationUnit() ;

    // Static elaborate an analyzed module, store in library 'work_lib'.  Parameters are char*param_name->char*param_value assocs. param_name can be 0 for ordered association.
    static unsigned      ElaborateStatic(const char *module_name, const char *work_lib="work", const Map *parameters=0) ;

    // VIPER #3099: Static elaborate an array of top level modules(VeriModule *, each can belong to different libraries). Returns an allocated array of elaborated modules (VeriModule *).
    // NOTE: This is the recommended API for static elaborating a module.
    // It takes an Array of VeriModule * of modules to be elaborated and returns the VeriModule * of elaborated modules.
    // We may have 3 different situations for static elaboration:
    //   1. Static elaborate a single module (by name or by pointer).
    //      a) If you have the name use veri_file::GetModule(name, 1 /* case sensitive */, lib) to get its pointer.
    //      b) Insert the VeriModule * into an Array and call this routine.
    //   2. Static elaborate multiple modules (by names or by pointers).
    //      a) If you have the names use veri_file::GetModule(name, 1 /* case sensitive */, lib) to get its pointers.
    //      b) Insert all the VeriModule * into an Array and call this routine. You can even call this with modules from different libraries.
    //   3. Static elaborate all the top level modules.
    //      a) Use veri_file::GetTopModules(lib) to get the top modules of the library into an Array.
    //      b) Call this routine with the returned Array and then delete the Array.
    // In all the above cases, you have to either use the returned Array or delete it.
    // VIPER #6545 : parameters are char* param_name->param_value assocs for
    // providing parameter values to top level modules
    static Array *       ElaborateMultipleTopStatic(const Array *top_mods, const Map *parameters=0) ;

    // VIPER #7727 : Static elaborate an array of top level verilog modules (VeriModule*)
    // and array of top level vhdl units (VhdlPrimaryUnit*). Populate 'elaborated_mods'
    // with the elaborated top level modules and populate 'elaborated_units' with
    // elaborated top level vhdl units. Parameters are char* param_name->param_value
    // assocs for providing parameter values to top level modules/units
    static unsigned      ElaborateMixedHdlMultipleTopsStatic(const Array *top_mods, const Array *top_units, const Map *parameters=0, Array *elaborated_mods=0, Array *elaborated_units=0) ;

    // Static elaborate all modules of library 'work_lib'.
    static unsigned      ElaborateAllStatic(const char *work_lib="work") ;

    // -incdir option in Verilog XL
    // Manage directories where `include directive will search
    // These directories are all 'static', so will survive different 'veri_file' objects.
    // These settings take effect during Analyze().
    static void          AddIncludeDir(const char *directory) ;
    static void          RemoveIncludeDir(const char *directory) ;
    static char *        IncludeFileName(const char *filename, linefile_type linefile) ; // Return the ALLOCATED full file name by looking for the file through the include directories. Return 0 if file not found.
    static Array *       IncludeDirs() { return _include_dirs; } // Array of 'const char*' of the presently active include directories
    static void          RemoveAllIncludeDirs() ; // Clear out all incdir

    //CARBON_BEGIN
    static Array *       PragmaTriggers() { return _pragma_triggers; } // Array of 'const char*' of the presently active pragma triggers 
    static void          AddPragmaTrigger(const char *pragma) ;
    static unsigned      IsPragmaTrigger(const char *id) ;
    static void          RemoveAllPragmaTriggers() ; // Clear out all pragma triggers
    //CARBON_END

    // -y/-v/-libext options in Verilog XL.
    // Used to resolve unknown module instantiations (will look for them in YDir or VFiles).
    // These settings are all 'static', so will survive different 'veri_file' objects
    // These settings take effect during Analyze().
    static void          AddYDir(const char *directory) ; // -y
    static void          AddVFile(const char *filename) ; // -v
    static void          AddLibExt(const char *lib_ext) ; // option to -y
    static void          RemoveAllYDirs() { RemoveAllYDirsVFiles() ; } // Backward compatibility from before we had VFiles.
    static void          RemoveAllYDirsVFiles() ;         // Clear out -y/-v settings
    static void          SetLibReScan(unsigned lib_rescan_active=1) ; // Set -librescan option for -y/-v processing (VIPER #2132)

    // VIPER #4115: File extension to language dialect selection (like: +verilog2001ext+ext)
    static void          AddFileExtMode(const char *file_extension, unsigned verilog_mode) ;
    static unsigned      GetModeFromFileExt(const char *file_extension) ;
    static void          RemoveFileExt(const char *file_extension) ;
    static void          ClearAllFileExt() ;

    // -L options: Moved to analyzer for VIPER #5618
    // Used to specify libraries where to search instantiated modules
    static void          AddLOption(const char *libname) ; // -L
    static Set *         GetAllLOptions() { return _ordered_libs ; } // Return set of L options
    static void          RemoveAllLOptions() ; // Clear out -L settings: to be called after each elaborate command
    static VeriModule *  GetModuleFromLOptions(const char *module_name, unsigned case_sensitive=1, unsigned restore=1) ; // Get argument specified module searching in -L options in order
    static VeriModule *  GetPackageFromLOptions(const char *package_name, unsigned case_sensitive=1, unsigned restore=1) ; // Get argument specified package searching in -L options in order

    // VIPER : 2947/4152 - APIs for customer to control/access loop limit
    static unsigned      GetLoopLimit()                 { return RuntimeFlags::GetVar("veri_loop_limit") ; }
    static void          SetLoopLimit(unsigned limit)   { (void) RuntimeFlags::SetVar("veri_loop_limit", limit) ; }

    // Loop limit variable is sticky across multiple elaboration runs
    // User might set a new value using SetLoopLimit or Reset this variable
    // to use the default limits (by LOOPLIMIT #defines)
    static void          ResetLoopLimit()               { (void) RuntimeFlags::SetVar("veri_loop_limit", 0) ;}

    // VIPER #5980: Dynamic control for error message: array size is
    // larger than 2**_max_array_size (VERI-1170)
    static void          SetMaxArraySize(unsigned size) { (void) RuntimeFlags::SetVar("veri_max_array_size", size) ; }
    static unsigned      GetMaxArraySize()              { return RuntimeFlags::GetVar("veri_max_array_size") ; }

    // VIPER #7125: APIs for customer to control/access stack limit:
    static unsigned      GetMaxStackDepth()                { return RuntimeFlags::GetVar("veri_max_stack_depth") ; }
    static void          SetMaxStackDepth(unsigned depth)  { (void) RuntimeFlags::SetVar("veri_max_stack_depth", depth) ; }

    // By default, the analyzer errors out on constructs that are illegal as defined in the Verilog LRM.
    // However, some simulators are accepting more than the LRM defines as legal.
    // This switch enables a 'relaxed' checking mode, which allows (System)Verilog to be parsed and checked in better complience with the most accepting simulators.
    static void          SetRelaxedLanguageChecking(unsigned on) ; // relaxed syntax/semantic checking.

    // Macro definition/undefinition
    // These settings are all 'static', so will survive different 'veri_file' objects and survive different Analyze() calls.
    // DefineMacro examples :
    //    DefineMacro("mymacro")                   as `define mymacro
    //    DefineMacro("mymacro","myvalue")         as `define mymacro myvalue
    //    DefineMacro("mymacro","a+b","(a,b)")     as `define mymacro(a,b) a+b
    // These settings take effect during Analyze().
    static unsigned      DefineMacro(const char *macro_name, const char *macro_value=0, const char *macro_formal_arguments=0, linefile_type lf=0) ;
    static unsigned      DefineCmdLineMacro(const char *macro_name, const char *macro_value=0) ; // VIPER 2407
    static unsigned      UndefineMacro(const char *macro_name) ;
    static void          UndefineAllMacros() ;          // Clear out all macro definitions (expect predefined)
    static void          UndefineCmdLineMacros() ;      // VIPER #4356: Clear macros defined through command line
    static void          UndefineUserDefinedMacros() ;  // Clear out all macros specified in design files
    static void          UndefineMacroPreDefs() ;       // Clear out preprocessor predefined macros
    //CARBON_BEGIN
    static void          UndefineCarbonUserDefinedMacros() ; // Clear out all macros specified in design files (multimap carbon version)
    //CARBON_END

    static unsigned      IsDefinedMacro(const char *macro_name) ;
    static unsigned      IsPredefinedMacro(const char *macro_name) ;
    static unsigned      IsCmdLineDefinedMacro(const char *macro_name) ;

    static const char *  MacroKey(const char *macro_name) ;
    static const char *  MacroValue(const char *macro_name) ;
    //CARBON_BEGIN
    static const char *  CarbonMacroValue(linefile_type lf);
    //CARBON_END
    static const char *  MacroArgs(const char *macro_name) ;

    static Map *         AllMacroDefs()     { return _macro_defs ; }    // Return name->value Map of all preprocessor macros set
    static Map *         AllMacroArgs()     { return _macro_argdefs ; } // Return name->formal_arguments Map of all preprocessor macros with arguments
    static Map *         AllMacroPreDefs()  { InitPredefMacros() ; return _macro_predefs ; } // Return name->formal_arguments Map of all preprocessor predefined macros with arguments set
    static Map *         AllCmdLineMacros() { return _cmdline_macro_defs ; } // Return name->formal_arguments Map of all preprocessor user defined macros with arguments set (VIPER #2407)

    // VIPER #7459: Store the macro ref vs. def location (linefile), only works properly in linefile-column mode:
    static Map *         GetMacroRefLinefiles() { return _macro_ref_linefiles ; }
    //CARBON_BEGIN
    static Map *         GetCarbonMacroDefLinefiles() { return _carbon_macro_def_linefiles ; }
    //CARBON_END
    static void          AddMacroRefLinefile(const char *macro_name, linefile_type ref) ;
    static void          RemoveMacroRefLinefiles() ;

    // Cleanup parse trees
    static unsigned      RemoveModule(const char *module_name, const char *lib_name="work") ; // Delete a module (parse tree) by name
    static void          RemoveSimpleModules() ;                 // Delete only modules without parameters
    static void          RemoveUncompiledModules() ;             // Delete all modules that are not yet elaborated
    static void          RemoveAllModules() ;                    // Delete all module parse trees

    // Previously parsed VeriModule access. Routines with VeriModule class parse-tree access
    static void          AddModule(VeriModule *module) ;
    static VeriModule *  GetModule(const char *module_name, unsigned case_sensitive=1, const char *lib_name="work", unsigned restore=1) ;
    static Map *         AllModules(const char *lib_name="work") ; // Return a (const char*name -> VeriModule*module) associated Map of all analyzed modules (guaranteed to be there)

    // Verilog XL support for -u (upper-case all identifiers (making Verilog case-insensitive) :
    static void          SetUpperCaseIds(unsigned upper_case_all_ids=1) ;

    // VIPER #7175: Set the comment string to be added with VeriModule::TbdmAdd*() and VeriModule::TbdmChange*() APIs.
    // Responsibility of the caller: wrap the string within long comment start/end characters /* and */.
    static void          SetTbdmCommentString(const char *comment) ;
    static const char *  GetTbdmCommentString() ; //        { return _tbdm_comment_string ; }

    // Register a function of yours with profile 'unsigned interrupt_func()' which
    // should return 1 if you want Verific to stop executing, 0 if you want us to
    // proceed.  We will call this function before elaboration of any statement or
    // module-item, so your registered routine better be fast :
    static void          RegisterInterrupt(unsigned(*interrupt_func)(linefile_type)) ; // Register your interrupt-check routine.
    static unsigned      CheckForInterrupt(linefile_type linefile) ; // Called by verific to check if interrupt is needed (by calling your function)

    // Direct (without callback) way to Interrupt the reader functions during analyze/elaborate.
    // This routine gives a "user-interrupt" message and reader will exit gracefully.
    static void          Interrupt() ;

    // Register a function of yours with profile 'unsigned compile_as_blackbox(const char *)'
    // which should return 1 if you want the module by that name to be elaborated as a black-box,
    // and return 0 if you want the module to be compiled normal.  We will call this function
    // at the time of attaching it to a library (VIPER #7337) and start of the first elaboration call:
    static void          RegisterCompileAsBlackbox(unsigned(*compile_as_blackbox_func)(const char *)) ;
    static unsigned      CheckCompileAsBlackbox(const char *module_name) ; // Called by verific to check if module should be compiled as black-box.
    // The Direct (no callback) way of setting a module to be compiled as black-box is
    // to find the module (with GetModule() or FOREACH_VERILOG_MODULE) and call VeriModule::SetCompileAsBlackbox().

    // Control if verific should ignore translate_off pragma's during parsing
    // (1 is ignore, 0 is don't-ignore (skip translate_off->translate_on blocks)
    static void          SetIgnoreTranslateOff(unsigned ignore_pragma) ;
    static void          SetIgnoreAllPragmas(unsigned ignore_pragma) ;

    // Runtime switches to control RAM extraction
    static unsigned      ExtractMPRams()                    { return RuntimeFlags::GetVar("veri_extract_multiport_rams") ; }
    static void          SetExtractMPRams(unsigned val)     { (void) RuntimeFlags::SetVar("veri_extract_multiport_rams",val) ; if (val) (void) RuntimeFlags::SetVar("veri_extract_dualport_rams",0) ; } // Run-time switch to extract multi-port RAMs 1==on, 0==off.  Make sure that dual- and multi-port ram extraction are not on simultaneously.
    static unsigned      ExtractRams()                      { return RuntimeFlags::GetVar("veri_extract_dualport_rams") ; }
    static void          SetExtractRams(unsigned val)       { (void) RuntimeFlags::SetVar("veri_extract_dualport_rams",val) ; if (val) (void) RuntimeFlags::SetVar("veri_extract_multiport_rams",0) ; } // Run-time switch to extract dual-port RAMs  1==on, 0==off.  Make sure that dual- and multi-port ram extraction are not on simultaneously.

    static unsigned      GetMinimumRamSize()                { return RuntimeFlags::GetVar("veri_minimum_ram_size") ; } // Viper #3903
    static void          SetMinimumRamSize(unsigned size)   { (void) RuntimeFlags::SetVar("veri_minimum_ram_size",size) ; } // Viper #3903

    // VIPER #6550: Generate explict state machine:
    static unsigned      GenerateExplicitStateMachines(const char *module_name, const char *lib_name, unsigned reset) ;

    // VIPER #3097 : Routines to set directives :
    static void          SetCellDefine(unsigned v = 0) ; // Set celldefine directive : default 0
    static void          SetDefaultNettype(unsigned type = 0) ; // Set default_nettype directive : default VERI_WIRE
    static void          SetUnconnectedDrive(unsigned value = 0) ; // Set unconnected drive, default not specified in LRM : set 0
    static void          SetTimeScale(const char *value = 0) ; // Set timescale directive, default not specified : set 0. WARNING : Use of this API has NO impact after analysis. If you want to modify timescale on the fly during elaboration, use SetUserTimeScale instead
    static unsigned      SetUserTimeScale(const char *value) ; // Set timescale to overrides the timescale set in the design, the input string must be in "1 ns/1 ps" format. To set only the time scale, not time precision, input string must be "<times_scale>/", e.g. "10 ns/", to set only time precision, not the time scale, input string must be "/<time_precision>", e.g. "/ 100 fs". Used during elaboration
    static unsigned      SetDefaultTimeScale(const char *value) ; // Set timescale to module only when no timescale is set on the module, the input string must be in "1 ns/1 ps" format. Used during elaboration

    // Set/Get the protection object for user encryption/decryption of vdbs
    static Protect      *GetProtectionObject() { return _protect ; }
    static void          SetProtectionObject(Protect *protect) { _protect = protect ; }

    /*
      Use of the following API, SetTickProtectObject, will enable Verific
      parser read in the protected RTL and generate regular parse tree.
      THE PARSE TREE NODES CARRY INFORMATION WHETHER IT CAME FROM THE
      PROTECTED RTL at the ModuleItem level. If user application does
      Binary Dump of the parse tree, any other third party software using
      Verific front-end will be able to restore the entire dump file,
      including the protected region. This makes the protected RTL visible
      to everyone restoring the binary dump. Application using this API,
      therefore, MUST encrypt the entire binary dump file, providing
      encoding algorithm using the API SetProtectionObject, so that only
      the same user application can understand and decrypt(restore) the
      encrypted binary dump.

      In addition to guarding the binary dump file, user application must
      not use PrettyPrint in any user visible context to guard the protected
      RTL.

      User must be careful and understand his application to take every
      possible measures to guard against any leakage of the protected code
      as a result of use of API SetTickProtectObject.
     */

    static Protect      *GetTickProtectObject() { return _tick_protect ; }
    static void          SetTickProtectObject(Protect *protect) { _tick_protect = protect ; }

    // VIPER #6071: Support for IEEE 1800 (2009) `pragma protect (section 34):
    static Protect      *GetPragmaProtectObject() { return _pragma_protect ; }
    static void          SetPragmaProtectObject(Protect *protect) { _pragma_protect = protect ; }

#ifdef VERILOG_FLEX_READ_FROM_STREAM
    // VIPER #5726: Read from streams:
    static void            RegisterFlexStreamCallBack(verific_stream *(*stream_func)(const char *file_name)) ;
    static verific_stream *GetUserFlexStream(const char *file_name) ;
#endif

public:
    // Persistence (Binary save/restore of Verilog design units to a binary file off-line)

    // Explicitly link a Verilog library to a directory
    static unsigned      AddLibraryPath(const char *lib_name, const char *dir_name) ;
    static char *        GetLibraryPath(const char *lib_name) ;
    static unsigned      RemoveLibraryPath(const char *lib_name) ;
    static void          ResetAllLibraryPaths() ;

    // Set default directory (prefix) for Verilog libraries which do not have an explicit link
    static unsigned      SetDefaultLibraryPath(const char *path) ;
    static char *        GetDefaultLibraryPath() ;
    static void          ClearDefaultLibraryPath() ;

    // Save/Restore a single Verilog design unit.
    static unsigned      Save(const char *lib_name, const char *module_name) ; // Does not delete the parse tree of the unit
    static unsigned      Restore(const char *lib_name, const char *module_name, unsigned silent=0) ;

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // VIPER 2944 : library-wide save/restore files
    static unsigned      SaveLib(const char *lib_name) ; // Save entire library to a library-wide dump file.
    static unsigned      RestoreLib(const char *lib_name) ; // Restore an entire library from a library-wide dump file
    static long          PosInLibFile(const char *lib_name, const char *module_name) ; // find position of unit in a lib-wide dump file. Or return 0 if unit is not there.
    static unsigned      RestoreLibUnitMap(const char *lib_name) ; // Populate the internal tables used to speedup lookups in library-wide save/restore files for this library from the saved side file
    static void          ClearLibUnitMap() ; // clear internal tables used to speedup lookups in library-wide save/restore files
    static const char *  FindCaseInsensitiveInLibFile(const char *lib_name, const char *module_name) ; // VIPER #6310: Find the module in library using case insensitive search and return the actual name
#endif

public:

    // Returns the top level modules(allocated array of VeriModule*) of the argument specific library.
    static Array *       GetTopModules(const char *work_lib="work") ;

    // Verilog directive `include argument storage (VIPER #1990/2712)
    static void          AddIncludedFile(const char *file_included, linefile_type included_from) ; // Store location of `include directive's argument
    static const Map *   GetIncludedFiles()     { return _included_files ; } // Retrieve Map containing `include locations
    static void          ClearIncludedFiles() ; // Clear out previous stored `included location information

    // Veriog 2000 library mapping:
    static void          AddLibraryDecl(const VeriLibraryDecl *lib_decl) ; // Add a library decl with library name and file pattern
    static void          ClearAllLibraryDecls() ;                    // Clear all library constructs
    static const char *  GetMatchedLibrary(const char *file_name) ;  // Returns the matching library name for the given file

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************
public:
    ~veri_file() { } ;

private:
    // Prevent compiler from defining the following
    veri_file(const veri_file &) ;            // Purposely leave unimplemented
    veri_file& operator=(const veri_file &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

#ifdef VERILOG_QUICK_PARSE_V_FILES
    // Verific internal APIs to do a quick parse on -v filename to determine if it contains any unresolved module definition
    static unsigned      HasUndefinedModuleInFile(const char* file_name, const Map *list_of_undefined_modules, Set *defined_modules) ;
    static void          ApplyActiveMacros() ;
    static void          ClearActiveMacros() ;
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
    static unsigned      HasActiveMacroDefRef() ;
#endif
#endif

    // VIPER #6855 & #7029: Keep track of visited arguments for recursion check.
    static unsigned      VerifyMacro(const char *macro_name, const char *macro_text, Set *visited_args=0) ; // Checks for infinite recursion
    // Used internally (check if unit is stored off-line under a library path) :
// retired 12/12 : static unsigned      IsSDBValid(const char *lib_name, const char *module_name) ; // Obsolete 12/1/12 : Logic moved into veri_file::Restore

private :
    // The following is used only by AddLibraryPath() and SetDefaultLibraryPath()
    static unsigned      VerifyAndModifyPath(char **path) ;

public: // Below this : For Verific only. Routines used DURING analysis.

    // For Verific only : pick up the 'scope' during analysis :
    static VeriScope    *GetPresentScope() ;

    // VIPER #3584 : This is only for Verific during analysis
    // This API remembers the pragma when they are given BEFORE the actual
    //  objects (that they apply on) are declared
    static void          AddPragmaToScope(const char *id, const char *attr, const char *val) ;

    // For Verific only : From the lexer (used during analyze).
    static linefile_type GetLineFile() ;            // Get linefile info for last token parsed.
    static linefile_type GetRuleLineFile() ;        // Get linefile info for last syntax rule parsed

    // For Verific only : Predefined macro settings, available from lexer during analyze.
    static unsigned      GetCelldefine() ;          // Check if `celldefine is set (return 0/1)
    static unsigned      GetDefaultNettype() ;      // Return `default_nettype setting (return a VERI token)
    static unsigned      GetUserDefaultNettype() ;  // Return `default_nettype setting (return a VERI token) only if user specified it (Verific internal use only API for VIPER #4581, #7128)
    static unsigned      GetUnconnectedDrive() ;    // Return `unconnected_drive setting (return a VERI token)
    static const char *  GetTimeScale() ;           // Return `timescale setting (return a pointer to the string (e.g. "1 ns/1 ps" ))
    static char *        GetUserTimeScale() ;       // Return the timescale set by the user that overrides the design `timescale setting.. If user did not set anything through API, return the design `timescale value, and if there is no `timescale set, return the default timescale set by the user, if present (return a pointer to the string (e.g. "1 ns/1 ps" ))
    static char *        GetEffectiveTimeScale(const VeriModule *mod) ; // Given the module, this API returns the effective timescale to be used during elaboration, keeping UserTimeScale and DefualtTiemScale into account

    // VIPER #7379: Add compiler directives from Annex-E of IEEE 1800 (2009) LRM:
    static char *        GetDefaultDecayTime() ;      // Return `default_decay_time
    static char *        GetDefaultTriregStrength() ; // Return `default_trireg_strength
    static unsigned      GetDelayModeDistributed() ;  // Return `delay_mode_distributed: 0/1
    static unsigned      GetDelayModePath() ;         // Return `delay_mode_path: 0/1
    static unsigned      GetDelayModeUnit() ;         // Return `delay_mode_unit: 0/1
    static unsigned      GetDelayModeZero() ;         // Return `delay_mode_zero: 0/1
    static void          ResetOptionalCompilerDirectives() ; // Resets the static info of the above six compiler directives

    // For Verific only : These are for VeriLibrary manipulation
    static void          AddLibrary(const VeriLibrary *library) ;
    static void          RemoveLibrary(const VeriLibrary *library) ;
    static VeriLibrary * GetLibrary(const char *library_name, unsigned create_if_needed=0, unsigned case_sensitive=1) ;
    static Map *         AllLibraries()                           { return _all_libraries ; }
    static void          ResetAllLibraries() ;     // only deletes a empty _all_libraries map. otherwise asserts
    // VIPER #6199: Delete library APIs: Before calling this make sure that
    // there is no reference to the library being deleted in the parse tree
    // otherwise, you corrupt the parse tree. This is user responsibility:
    static void          DeleteLibrary(const char *name) ;
    static void          DeleteAllLibraries() ;

    // For Verific only : CurrentUserLibrary used for -y/-v/uselib library setting during analysis / elaboration
    static VeriLibrary * GetCurrentUserLibrary()                  { return _current_user_library ; }
    static void          SetCurrentUserLibrary(VeriLibrary *lib)  { _current_user_library = lib ; }
    static VeriLibrary * GetYVUserLibrary()                       { return _yv_user_library ; }
    static void          SetYVUserLibrary(VeriLibrary *lib)       { _yv_user_library = lib ; }

    // For Verific only : WorkLib used as the 'work' library during analysis / elaboration.
    static VeriLibrary * GetWorkLib(const char *file_name = 0) ; // Specifying file name will search for library mapping for this file
    static void          SetWorkLib(VeriLibrary *lib)             { _work_library = lib ; }

    // For Verific only : pick up pragma's and comments during parsing
    static void          AddPragmas(Map *&pragmas) ;
    static Map *         TakePragmas() ;
    static unsigned      GetLastPragmaLine() ; // VIPER #6838: Returns the ending-line of last parsed paragma specification
    static Array *       TakeComments() ;
    static void          SetComments(Array *comments) ;

    // For Verific only : Include this file in the current source text. This is called both for "`include" and library maping "include".
    static void          IncludeFile(const char *fname) ;

    // For Verific only : Setting analyzer mode (dialect)
    static unsigned      GetAnalysisMode()                              { return _analysis_mode ; }
    static void          SetAnalysisMode(unsigned analysis_mode)        { if (analysis_mode>UNDEFINED) analysis_mode = VERILOG_2K ; _analysis_mode = analysis_mode ; } // Set the dialect to use during parsing. Default to Verilog 2K if we don't know the mode.
    // VIPER #5434: Default analysis mode in case of file extension vs. dialect mode mapping:
    static unsigned      GetDefaultAnalysisMode()                       { return _default_analysis_mode ; }
    static void          SetDefaultAnalysisMode(unsigned analysis_mode) { if (analysis_mode>UNDEFINED) analysis_mode = VERILOG_2K ; _default_analysis_mode = analysis_mode ; }

    // System verilog only : Setting compilation unit mode :
    static unsigned      IsSingleFileCompilationUnitMode() { return (_compilation_unit_mode == SFCU) ; }
    static unsigned      IsMultiFileCompilationUnitMode() { return (_compilation_unit_mode == MFCU) ; }

    // Pick up the root module. This is only useful for System Verilog.
    static VeriModule *  GetRootModule(unsigned create_if_needed = 0) ;
    static void          ResetRootModule() ;
    static void          ProcessRootModule() ;
    static unsigned      AddRootModuleToWorkLib(VeriModule *root_tree) ;
    static unsigned      AddRootModuleToLib(VeriModule *root_tree, VeriLibrary *library) ;

    // For Verific only : VIPER #2932. Returns the array of file names
    static Array *       GetFileNameArray() { return _file_names ; }
    static void          ResetFileNames() ;

    // Internal routine to analyze -y/-v specified file :
    static unsigned      AnalyzeLibFile(const char *file_name, unsigned verilog_mode=VERILOG_2K, const char *lib_name="work") ;

    // Internal routine used in flex for extends...endextends support
    static unsigned      IsWithinClass() ;
    static unsigned      IsWithinBinsRepeat() ; // VIPER #5248 (open_sqr_coveragebins): Context information for bins repeat tokens

    // Verific internal routine to know whether we are inside nested design unit or outside of any design unit (only useful at parsing time)
    static int           GetDesignUnitDepth() ;
    static int           ConfigIsIdentifier() ; // VIPER #6079: Status of the 'config' string in input RTL

    // VIPER #5932: Verific internal routine required to process user library
    static unsigned      SetProcessingUserLibrary(unsigned status) { unsigned prev = _processing_user_library ; _processing_user_library = status ; return prev ; }
    static unsigned      IsProcessingUserLibrary()                 { return _processing_user_library ; }

    // VIPER #5932: Verific internal routine used to clean up linefile structures in case of unnecessary user library modules:
    static Array *       TakeLinefileStructures() ;
    static void          SetupLinefileStructures() ;

    // VIPER #8405: Utility routines for interfacing between lexer and -v resolver:
    static unsigned      IsUnresolvedModule(const char *module_name) ;
    static void          SetIgnoredModuleNames(Set *list_of_module_names) ;
    static void          AddToIgnoredModuleNames(const char *module_name) ;
    static unsigned      HasAnyModuleInIgnoredList(const Set *new_modules_added, const Set *ignored_module_names) ;

public:
    // VIPER #5624 : Set/get routine for global static elaboration object
    static void          SetStaticElabObj(VeriStaticElaborator *o) ;
    static VeriStaticElaborator *GetStaticElabObj() { return _static_elab ; }

    // Get the parsed expression. Verific Internal routine used from yacc: Viper 3782
    static void          SetParsedExpr(VeriExpression *expr)  { _parsed_expr = expr ; }

    // Verific internal routine to check if the parser is in active state (VIPER #7657):
    static unsigned      IsParserActive() ;

// Viper #6212: begin : Runtime switch to control RAM extraction
// RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.
// private:
//    static unsigned  _extract_mp_rams ;
//    static unsigned  _extract_rams ;
private:
    // Private routines, mainly filled in by lex/yacc
    static void          InitPredefMacros() ;
    static unsigned      AnalyzeInternal(const char *file_name, unsigned verilog_mode=VERILOG_2K, const char *lib_name="work") ;
    static unsigned      AnalyzeExprInternal(const char *expr, unsigned verilog_mode=VERILOG_2K, const linefile_type line_file = 0) ;

    static unsigned      StartFlex(const char *file_name) ;
    static unsigned      StartFlexExpr(const char* expr, const linefile_type line_file = 0) ;
    static unsigned      EndFlex() ;

    static void          StartYacc() ;
    static void          EndYacc() ;
    static int           Parse() ;

    // Verific internal APIs to do a quick parse on -v filename to determine if it contains any unresolved module definition
    static unsigned      ParseFileForModuleNames(const Map *list_of_undefined_modules, Set *defined_modules) ;

    // The following is used only by PrettyPrintDesign() API:
    static char *        GetOutputFileNameForPrintDesign(const char *design_file, const char *suffix, const char *output_directory) ;

    // VIPER #8515: Check if the given Verilog language/dialect mode can be parsed with the available products:
    static unsigned      CheckLanguageModeAndAvailableProducts(unsigned verilog_mode) ;

public:
    // NOTE: Verific internal use only (should never be called by users):
    // Return the name of the top module (from not-yet elaborated modules)
    static const char *  TopModule() ;

    // NOTE: Verific internal use only (should never be called by users):
    // VIPER #3379 : Keep all top level modules for elaboration
    static void          AddTopModule(const VeriModule *module) ; // Add 'module' as top level
    static VeriModule *  GetTopModule(const char *name) ; // Return top level module by name. Issue error if multiple module named 'name' exists
    static void          ClearTopModuleList() ; // Clear out top module setting: to be called after each elaborate command

private:
    static unsigned      _analysis_mode ; // Flag to indicate which dialect we are parsing
    static unsigned      _default_analysis_mode ; // VIPER #5434: Flag to indicate which dialect is the default in case of file extension vs dialect mode mapping.
    static VeriExpression *_parsed_expr ;         // VeriExpression parsed in Expression Parsing mode
    static unsigned      _compilation_unit_mode ; // Flag to indicate how compilation unit(s) will be defined
    static unsigned      _processing_user_library ; // VIPER #5932: Flag to indicate that we are processing -y/-v/`uselib

    static Array        *_include_dirs ;         // char* elements. List of directories where `include directive will search.
    static Map          *_all_libraries ;        // List of all the libraries
    static VeriLibrary  *_work_library ;         // The current library where design files will be read
    static VeriLibrary  *_current_user_library ; // The current user library
    static VeriLibrary  *_yv_user_library ;      // VIPER #5659: The -y/-v specific user library

    // Macro definitions.
    static Map          *_macro_defs ;           // char* -> char* elements
    //CARBON_BEGIN
    static Map          *_carbon_macro_defs ;    // char* -> char* elements, this is multimap version and doesn't get deleted at the end of compilation unit - so same key can appear multiple times (to accumulate same name defines across different files - instead of simple replace, this is needed for dumpHierarchy support)
    //CARBON_END
    static Map          *_macro_argdefs ;        // char* -> char* elements
    static Map          *_macro_predefs ;        // char* -> char* elements
    static Map          *_cmdline_macro_defs ;   // char* -> char* elements (VIPER #2407)
    static Map          *_macro_def_linefiles ;  // char* -> linefile_type of macro defs for VIPER #7459
    //CARBON_BEGIN
    static Map          *_carbon_macro_def_linefiles ;  // char* -> linefile_type of macro defs for VIPER #7459, this is multimap version and doesn't get deleted at the end of compilation unit - so same key can appear multiple times (to accumulate same name defines across different files - instead of simple replace, this is needed for dumpHierarchy support)
    //CARBON_END
    static Map          *_macro_ref_linefiles ;  // linefile_type (macro ref) -> linefile_type (macro def) for VIPER #7459
#ifdef VERILOG_QUICK_PARSE_V_FILES
    static Map          *_active_macro_defs ;    // char* -> char* elements - the active ones (first preference)
    static Map          *_active_macro_argdefs ; // char* -> char* elements - the active ones (first preference)
    static Set          *_active_macro_undefs ;  // VIPER #5944: char* - the active undefined macros (first preference)
    static unsigned      _quick_parsing ;        // Flag to indicate that we quickly parsing -y/-v files
#ifdef VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
    static unsigned      _has_active_macro_ref ; // Flag to indicate that the quick parsed file has references to some macros
#endif
    static unsigned      UndefineActiveMacro(const char *macro_name, unsigned remove_only = 0) ;
#endif

    static char         *_tbdm_comment_string ;  // VIPER #7175: Comment string to be added with VeriModule::TbdmAdd*() and VeriModule::TbdmChange*() APIs

    // Functions, registered by the user :
    static unsigned    (*_interrupt_func)(linefile_type) ;
    static unsigned    (*_compile_as_blackbox_func)(const char *) ;

    // For System Verilog, this is the root module. Not directly visible for API.
    static VeriModule   *_root_module ;
    static Array        *_file_names ; // VIPER #2932. Contains the names of all the verilog files to be analyzed.
// Library search path
    static char         *_default_library_path ;
    static Map          *_map_library_paths ; // char* library name -> char* directory name

#ifdef VERILOG_USE_LIB_WIDE_SAVERESTORE
    // VIPER 2944 : library-wide save/restore files
    static Map          *_lib_unit_map ; // Map of lib_name->(Map of unitnames->pos_in_libfile) to quickly check if a unit is in a lib-wide map file.
#endif

    // Verilog 2000 library mapping :
    static Array        *_library_decls ;        // Array of VeriLibraryDecl * : stores all the 'library <lib_name> <pattern(s)> ;' declarations
    static Map          *_matched_files ;        // Map of const char * (file path) -> const char * (library name) : stores already matched files

    // Verilog directive `include storage (VIPER #1990/2712)
    static Map          *_included_files ;       // `include location storage : (char*)->(linefile_type) elements

    // VIPER #4115: File extension to language dialect selection (like: +verilog2001ext+ext)
    static Map          *_file_ext_vs_mode ;     // char * vs unsigned (VERILOG_95, VERILOG_2K, SYSTEM_VERILOG, VERILOG_AMS, VERILOG_PSL or UNDEFINED)

    static Set          *_ordered_libs ;   // Set of library names in char* form  (VIPER #2762/#5618)

// RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.
//    static unsigned      _loop_limit ;    // VIPER : 2947/4152 - Runtime control of loop limit
//    static unsigned      _max_stack_depth ;   // VIPER #7125: Runtime control of stack limit
//    static unsigned      _max_array_size ; // VIPER #5980: Dynamic control for error message: array size is larger than 2**_max_array_size (VERI-1170)
    static Array        *_top_mods ;      // Array of top level modules in VeriModule* form (VIPER #3379)

//    static unsigned      _minimum_ram_size ; // Viper #3903 // RD: since VIPER 7208 run-time flags moved to 'RuntimeFlags' utility.

    static Protect      *_protect ; // The object for the class that defines encryption/decryption algorithms for VDB/SDBs

    static Protect      *_tick_protect ; // The object for the class that defines encryption/decryption algorithms verilog `protected region

    static Protect      *_pragma_protect ; // VIPER #6071: The object for the class that defines encryption/decryption algorithms verilog `pragma protect

#ifdef VERILOG_FLEX_READ_FROM_STREAM
    static verific_stream *(*_stream_callback_func)(const char *file_name) ; // VIPER #5726: Read from streams
#endif

#if defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
#ifndef VERIFIC_LINEFILE_COLUMN_ALLOCATE_IN_CHUNK
    static Array        *_linefile_structs ; // VIPER #5932: Stores all the linefile structures created while parsing user library
#endif
#endif // defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS) || defined(VERIFIC_LARGE_LINEFILE)
    static VeriStaticElaborator *_static_elab ; // Object for static elaborator class
    //CARBON_BEGIN
    static Array        *_pragma_triggers ; // List of custom pragma trigger names (char*)
    //CARBON_END
    static unsigned      _ignore_library_mapping ; // VIPER #8123: Flag to ignore library mapping when -work option is used in analyze
    static Set          *_ignored_module_names ; // VIPER #8405: Set of ignored modules. Set from VeriUserLibrary::ProcessFileOption() only
//#*# VERIFIC_END DOCUMENT_INTERNAL
} ; // class veri_file

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VERI_FILE_H_

