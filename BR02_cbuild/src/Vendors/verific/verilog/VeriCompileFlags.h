/*
 *
 * [ File Version : 1.123 - 2014/01/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VERI_COMPILEFLAGS_H_
#define _VERIFIC_VERI_COMPILEFLAGS_H_

/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
 |                                                                                  |
 |    This header file contains all verilog-related compile flags.  These flags     |
 |    can either be manually set (#defined) within this file, or they can be set    |
 |    from the Makefile/(Msdev project settings) environment.  If you want to       |
 |    control compile flag settings from the Makefile/(Msdev project settings)      |
 |    environment, then make sure all #define's are commented out below.            |
 |                                                                                  |
  \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

/*-------------------------------------------------------------------------------------
    VERIFIC_NAMESPACE : (active in all projects)
  -------------------------------------------------------------------------------------
    This flag is controlled in the util/VerificSystem.h header file.  Please go
    there for a detailed description.
*/

/*-------------------------------------------------------------------------------------
    VERILOG_USE_STARTING_LINEFILE_INFO :
  -------------------------------------------------------------------------------------
    When printing (outputting) linefile information during Info, Warning, and
    Error messages, the linefile corresponding to the starting line of the
    construct will be used, instead of the default behavior of using the ending
    linefile info.  You will only see a linefile difference from the different
    flag settings for multi-line constructs.
*/

// JJ: 5/2013: Customer req this to be on, email sub: question about Verilog support for concats with multipliers of 0
#define VERILOG_USE_STARTING_LINEFILE_INFO // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_ANSI_INCLUDE_FILE_SEARCH :
  -------------------------------------------------------------------------------------
    When resolving a relative `include filename, we will (by default) search
    for the file relative to the PWD (present working directory) of the command
    line.  If the file is not found, we will then iterate through any -incdir
    directories which may have been specified.  It seems all major Verilog EDA
    vendors use this algorithm when resolving the `include directive.

    This search algorithm is in constrast to how ANSI C/C++ resolves #include
    relative filenames.  They search relative to the directory the file containing
    the #include statement resides.  Please enable this flag if you wish to use
    the ANSI C/C++ include path algorithm when resolving Verilog `include directives.
*/
// RD: 6/2012: VERILOG_ANSI_INCLUDE_FILE_SEARCH is now run-time flag RuntimeFlags::GetVar("veri_include_ansi_file_search") initialized in file VeriRuntimeFlags.h
//#define VERILOG_ANSI_INCLUDE_FILE_SEARCH // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_DUAL_LANGUAGE_ELABORATION :
  -------------------------------------------------------------------------------------
    This compile flag enables that the Verilog elaborator will probe the VHDL
    side while it is elaborating a unresolved instantiation (instantiation for
    which there is no definition). On the VHDL side, this compile switch allows
    probing the Verilog elaborator for unresolved VHDL units.  The cross-elaboration
    calls will take the parameter setting into consideration which cross-elaborating
    a design. This compile switch thus creates a fully transparent, fully automated
    cross-language elaborator.  Obviously, the compile switch will only operate if
    you purchased both VHDL and Verilog elaboration products.
*/

#define VERILOG_DUAL_LANGUAGE_ELABORATION // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*------------------------------------------------------------------------------------
    VERILOG_ELABORATE_TOP_LEVEL_MODULES_HAVING_INTERFACE_PORTS
  ------------------------------------------------------------------------------------
    By default, Verific does not elaborate top level modules having interface
    type ports. The reason is we do not know the actual interface or modport
    type in that case. This is the way simulators behave.

    This compile flag will make Verific elaborate even top level modules
    having interface ports. We assume the interface type specified as the
    type of the port as actual interface. Having said this, we cannot
    elaborate the modules having generic interface type ports, since in
    that case we do not know the interface to use as actual type.

*/

// DD: 9/2012: VERILOG_ELABORATE_TOP_LEVEL_MODULES_HAVING_INTERFACE_PORTS is now a run-time flag RuntimeFlags::GetVar("veri_elaborate_top_level_modules_having_interface_ports") initialized in file VeriRuntimeFlags.h.
// FD: Opened flag to match-up with previous behavioral
#define VERILOG_ELABORATE_TOP_LEVEL_MODULES_HAVING_INTERFACE_PORTS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_PRESERVE_COMMENTS :
  -------------------------------------------------------------------------------------
    Typically, comments in HDL source code are dropped in token-parsing
    (very early in the reader). For some applications, it is important
    to preserve the comments all the way to the parse-tree or further.

    This compile switch performs comment preservation by attaching the
    comments (as char* inside a VeriComment class node) into the parse
    tree nodes of Verilog constructs that are nearest the comment.

    Parse tree classes VeriModuleItem (which includes all 'modules',
    declarations and all statements in Verilog) contains a field accessible
    with the VeriModuleItem::GetComments() routine (returns an Array
    of VeriComment nodes applicable for that module item).

    Both block comments as well as line-comments are accepted and stored as comment.

    Rules of comment attachment are rather simple :
    A comment will be attached to a Verilog module item if :

      - the comment appears before the construct and
        was not attached to any previous construct, or
      - the comment during or after the construct at the same
        line as where the construct ends.

    For example :

       // something about this module
       module (....) ; // something more about this module
       input a ; // something about port a
       // something about port b
       input b ;

    the comments will be attached to the applicable VeriModuleItem nodes
    created for the module and the port-declarations.

    Note that we do NOT attach comment to identifiers (VeriIdDef's).
    Only to the declaration construct (VeriModuleItem) in which they appear.

    Comments attached to each module-item are printed ahead of construct
    in the PrettyPrint() routines. The above example will thus be
    pretty_print-ed like this :

       // something about this module
       // something more about this module
       module (....) ;
       // something about port a
       input a ;
       // something about port b
       input b ;

    Downside of using this compile switch is that more memory is needed
    to parse the design (extra field on VeriModuleItem and the comment
    char*'s themselves).
*/
// RD: 6/2012: VERILOG_PRESERVE_COMMENTS is now a run-time flag RuntimeFlags::GetVar("veri_preserve_comments") initialized in file VeriRuntimeFlags.h.
// JJ: 5/2013: Customer req this to be on, email sub: question about Verilog support for concats with multipliers of 0
#define VERILOG_PRESERVE_COMMENTS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_ID_SCOPE_BACKPOINTER :
  -------------------------------------------------------------------------------------
    Verific's parse tree datastructures are designed to minimize memory usage.
    There are a number of back-pointers and flags on identifiers (VeriIdDef) and other leaf tree nodes,
    and these are of great help to obtain most important information that applications need.
    However, when writing application software on the Verific parse trees, you might not
    always be able to readily find information that you need.

    For example, a number of customers have asked to be able to access the 'context' in which
    any identifier is declared, if you only know the identifier. The context includes the primary
    or secondary unit or block or subprogram in which an identifier is declared.
    This information is available once we are at the VeriScope in which the identifier is declared.

    If this compile switch is set, we will add a field to every identifier, which contains a
    pointer to the scope in which the identifier is declared.
    The API routine that will be added under this compile switch is VeriIdDef::GetOwningScope().
    The owning scope should always be set (for validly declared identifiers), except for the
    design unit identifiers. These have a scope by themselves (VeriIdDef::LocalScope()), but are
    not contained in a scope.

    The VeriScope API then provides a number of subroutines that identify the context (see VeriScope.h),
    including the module in which the id is declared with GetContainingModule() and immediate context GetOwner().

    We will maintain validity of this scope back-pointer through identifier construction, destruction and copying.
    This compile switch creates one more pointer per declared identifier. This means that it causes
    a slight increase in memory usage for parse trees.
*/

// JJ: 5/2013: Customer req this to be on, email sub: question about Verilog support for concats with multipliers of 0
#define VERILOG_ID_SCOPE_BACKPOINTER  // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_PRESERVE_LITERALS :
  -------------------------------------------------------------------------------------
    By default Verific converts all based numbers and unsized literals into binary domain
    and stores that value. But it may be of some help to get the exact string representation
    of that constant literal in some cases. This compile flag makes this possible as well as
    keeps the memory usage to minimum.

    A number of customers have asked to be able to get the exact string representation of
    the constant literal that was there in the input RTL design. Turning this compile flag
    stores that exact format as string including the radix and value. This also pretty print
    the original format string.

    If this compile switch is set, we will add a field to every constant value (VeriConstVal),
    which contains a pointer to character which stores the exact constant string (except in
    case of string value literals, since strings are stored as it is. So there is no need to
    store them again).

    The API GetOriginalNumber() returns the exact string representation of the number literal
    and 0 for string literal. This also gets saved and restored with Verilog Binary Save Restore
    process. We save a null string while saving and ignore the restored string if this flag is
    not defined.
*/

//#define VERILOG_PRESERVE_LITERALS  // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_UNIQUIFY_ALL_INSTANCES :
  -------------------------------------------------------------------------------------
    This compile flag enables that the Verilog static elaborator will create
    unique module for every instantiation. If the design instantiates a module
    twice with same parameter settings, two copies of instantiated module will
    be created with same parameter settings, but with different names and those
    modules will be bounded with the instances.

    Consider the following example :

         module top ;
           child #(2) I1() ;
           child #(3) I2() ;
           child #(2) I3() ;
           child I4() ;
        endmodule

    Now if this compile switch is on new copy of module child will be created
    for every instances and so after static elaboration module top will look like :

          module top ;
            child_uniq1 I1 () ;
            child_uniq2 I2 () ;
            child_uniq3 I3 () ;
            child_uniq4 I4 () ;
          endmodule

    But if this compile switch is off, single copy of module child is created
    for a specific parameter settings (instances I1 and I3) and original module
    is used if no parameter overridden occurs. So after static elaboration module
    top will look like :

          module top ;
            child_uniq_1 I1 () ;
            child_uniq_2 I2 () ;
            child_uniq_1 I3 () ; // same as instance I1
            child I4() ;
         endmodule
*/

// VIPER #7352: VERILOG_UNIQUIFY_ALL_INSTANCES is now a run-time flag RuntimeFlags::GetVar("veri_uniquify_all_instances") initialized in file VeriRuntimeFlags.h.
//#define VERILOG_UNIQUIFY_ALL_INSTANCES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_COPY_TOP_BEFORE_STATIC_ELAB :
  -------------------------------------------------------------------------------------
    This compile flag enables that the Verilog static elaborator will copy the
    specified top level module first and then statically elaborate the copied
    module. Static elaborator modifies the parse trees so that all parameters
    become locally constant and complex constructs like generates are replaced
    by appropriate module items. So, copy of top level becomes essential to
    support the following features:

    1) Recursion of top level module. As recursive instantiation generally
       resides inside generate and to process that instantiation unchanged body
       of original top level module is required.

    2) Instantiation of top level module from vhdl design with different sets
       of parameter values during dual language elaboration. If verilog top level
       module is elaborated first and then during vhdl elaboration same module
       instantiation is hit, unmodified top level module is required to process
       that instance.

    If designs containing above specified features are statically elaborated
    without enabling this flag, incorrect result will occur.
*/

// DD: 5/2013: VERILOG_COPY_TOP_BEFORE_STATIC_ELAB is now a run-time flag RuntimeFlags::GetVar("veri_copy_top_before_static_elab") initialized in file VeriRuntimeFlags.h.
//#define VERILOG_COPY_TOP_BEFORE_STATIC_ELAB // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_REPLACE_CONST_EXPRS :
 -------------------------------------------------------------------------------------
    This compile flag enables the Verilog static elaborator and allows it to replace
    the constant expressions used at some particular positions in the parse tree. This
    replacement is done by constant value only in statically elaborated modules.
    The following constant expressions will be replaced if this flag is on :

    1. Bounds of packed and unpacked ranges in all data declarations.
    2. Default values of all declared objects if those are constant.
    3. Delay values
    4. Reject and error limits of VeriPathPulse values.
    5. Indexed expressions only if the indexed expression is a bit-select on the LHS of a
       continuous assignment or output/inout port of module or gate instantiation.

    If this switch is on, an extra pass is done on elaborated module parse-trees.
    So, elaborator will take longer time to finish.

    Above specific replacements are not done on un-elaborated modules, as their
    parameter values might be changed later from other applications and constant
    expressions depends on parameters.
*/

// DD: 8/2012: VERILOG_REPLACE_CONST_EXPRS is now a run-time flag RuntimeFlags::GetVar("veri_replace_const_exprs") initialized in file VeriRuntimeFlags.h.
// JJ: 5/2013: Customer req this to be on, email sub: question about Verilog support for concats with multipliers of 0
#define VERILOG_REPLACE_CONST_EXPRS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_CLEANUP_BASE_MODULES :
 -------------------------------------------------------------------------------------
    NOTE: Enabling this flag will break Verilog incremental and mixed-languague
          static elaboration!

    This compile flag enables deletion of base modules which are replicated during
    static elaboration.  More specifically, static elaboration replicates original
    modules of the design depending on their overridden parameter values, hierarchical
    identifiers and the hierarchy under them.  So, originally instantiated modules
    become uninstantiated after static elaboration and this creates extra top level
    modules.  These original modules will be deleted when this flag is enabled.

    If this compile flag is disabled, extra top level modules created by static
    elaboration remain in the parse tree and the elaborated parse tree can
    be used for incremental and mixed elaboration. If this compile flag is on,
    spurious modules created by elaboration are deleted and elaborated parse tree
    can no longer be used for incremental and mixed elaboration.

    Example -
         module top ;
             child #(5) I() ;
         endmodule
         module child ;
             parameter p = 10 ;
         endmodule

    If this design is elaborated by turning this flag off, the elaborated parse tree is

        module top ;
            child_uniq1 I() ; // Copied module instantiated
        endmodule
        module child ; // original module becomes extra top level
            parameter p = 10 ;
        endmodule
        module child_uniq1 ; // Copied module
            parameter p = 5 ;
        endmodule

    If design is elaborated by enabling this compile flag, extra top level module
    'child' created by elaboration will be removed and design becomes

        module top ;
            child_uniq1 I() ;
        endmodule
        module child_uniq1 ;
            parameter p = 5 ;
        endmodule

    The default behavior of static elaboration utility is to support incremental
    and mixed elaboration. So, by default this flag remains off.
*/

// DD: 9/2012: VERILOG_CLEANUP_BASE_MODULES is now a run-time flag RuntimeFlags::GetVar("veri_cleanup_base_modules") initialized in file VeriRuntimeFlags.h.
//#define VERILOG_CLEANUP_BASE_MODULES // <--- Comment this line out to control flag from build environment, or uncomment to force on.


/*--------------------------------------------------------------------------------------------
  VERILOG_CARBON_STYLE_MODULE_NAMES:
  --------------------------------------------------------------------------------------------
    This switch modifies the action of the following switch named VERILOG_DO_NOT_CREATE_LONG_MODULE_NAMES.
    If VERILOG_CARBON_STYLE_MODULE_NAMES is defined then the module names created after static elaboration are of the style used at Carbon.
    The name is the original module name with an optional suffix of the form _#<number> for example foo_#1
    The first parameterized configuration of a module always gets the original module name.  If there is a configuration with different
    parameter values then that module will have the name <original_name>_#1.  Then next configuration will get #2 and so on.

    Note it only makes sense to enable this switch if VERILOG_DO_NOT_CREATE_LONG_MODULE_NAMES is also enabled
*/
#define VERILOG_CARBON_STYLE_MODULE_NAMES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*--------------------------------------------------------------------------------------------
    VERILOG_DO_NOT_CREATE_LONG_MODULE_NAMES:
  --------------------------------------------------------------------------------------------

    Sometimes long module names are created after static elaboration. This is beause we
    generally append name of the parameter and its value in the copied/unique module name.
    This compile flag makes those long names shorter. By default, we treat more than 200
    character names to be long names.

    Example:

        module top(input a, output b) ;
            sub u0(a, b);
            defparam u0.P = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" ;
        endmodule

        module sub (input a, output b) ;
            parameter P = "original value" ;
            assign b = a ;
        endmodule

    Signature of the node for u0 instance of module sub is too large. The signature may be
    ridiculously large for a module with a large number of parameters. If this design is
    elaborated by turning this flag off the elaborated parse tree will be:

        module top (input a, output b) ;
            \sub(P=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA)  (a, b) ;
        endmodule

        module \sub(P=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA  (input a, output b) ;
            parameter P = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" ;
            assign b = a ;
        endmodule

    If design is elaborated by enabling this compile flag, the elaborated parse tree becomes

        module top (input a, output b) ;
            sub_renamed_due_excessive_length_1 u0 (a, b) ; // Renamed for large name
        endmodule

        module sub_renamed_due_excessive_length_1 (input a, output b) ; // Note the module name here
            parameter P = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" ;
            assign b = a ;
        endmodule
*/
// VIPER #7853: VERILOG_DO_NOT_CREATE_LONG_MODULE_NAMES is now run-time flag RuntimeFlags::GetVar("veri_do_not_create_long_module_names") initialized in file VeriRuntimeFlags.h.
#define VERILOG_DO_NOT_CREATE_LONG_MODULE_NAMES   // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_PROCESS_PRAGMAS_AS_COMMENTS
  -------------------------------------------------------------------------------------
    Normally pragmas are parsed specially and are annotated into the created parse
    tree as attributes. Comments are also parsed and are either preserved or ignored
    depending on VERILOG_PRESERVE_COMMENTS compile flag setting. Comments are preserved
    as simple strings and are printed as comments. Pragmas are differentiated from
    general comments by the pragma triggers. Pragma triggers are strings like
    "synthesis", "verific" etc. inside comments as the starting text.
    For example the pragma:

    // pragma delay = 20

    will be printed as an attribute like:

    (* delay = 20 *)

    and comment:

    // it is a comment

    will be printed as (if we are preserving comments, otherwise it is ignored):

    // it is a comment

    Turning on this compile flag will treat the pragmas as comments. Depending on the
    setting of the compile flag VERILOG_PRESERVE_COMMENTS the pragmas are stored as
    comments or are ignored. If the compile flag VERILOG_PRESERVE_COMMENTS and this
    VERILOG_PROCESS_PRAGMAS_AS_COMMENTS are both turned ON the pragmas are printed
    as comments (at a more accurate position) rather than as attributes.

    Note that this compile switch ONLY affects pragma's that would otherwise be
    translated to Verilog attributes. Behavior of all other pragma's (specifically
    the translate_off/on pragma's) remain unchanged. These can be controlled with the
    run-time switch veri_file::SetIgnoreTranslateOff().
*/

// DD: 5/2013: VERILOG_PROCESS_PRAGMAS_AS_COMMENTS is now a run-time flag RuntimeFlags::GetVar("veri_process_pragmas_as_comments") initialized in file VeriRuntimeFlags.h.
//#define VERILOG_PROCESS_PRAGMAS_AS_COMMENTS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_STORE_ID_PRAGMAS :
  -------------------------------------------------------------------------------------
    Pragma's (as attributes) normally get translated to Verilog 2000 attributes, and applied
    to the data declaration (a VeriModuleItem) that the pragma is 'closest' to (positionally).
    VIPER 3239 brings up a different style of pragma's : non-positional pragmas which are
    applied to a single identifier (not to a full data declaration)
    Examples of such pragmas are :

      // synthesis attribute <attribute_name> of <target_identifier> is <attribute_value>

    These pragma's can appear anywhere in the scope where the target_identifier is declared.
    We can find these identifiers during parsing, but we do not have a way to store the pragma
    (attribute name and value) on a single identifier. So by default, these style pragmas are ignored.

    Under this compile flag, we store these non-positional attributes as char*->char* in a table
    directly on the target identifier, as a char*->char* association.
    We create a compile flag for this, since it requires an extra pointer on the VeriIdDef structure.
    If your application does not need to process such 'non-positional' style pragma's as attributes,
    then to save memory it is better not to set this compile flag.

    NOTE : if compile flag VERILOG_PROCESS_PRAGMAS_AS_COMMENTS is set, then all pragmas as attributes
    will always be treated as comments, so there is no use in setting this compile flag only.
    NOTE2: currently, these pragmas are processed during RTL elaboration, and will create attributes
    on the design objects that the identifier represents (instance, net(s), netlist etc), similar to
    what we do with Verilog 2000 attributes stored on VeriModuleItems.
*/

#define VERILOG_STORE_ID_PRAGMAS  // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_USE_LIB_WIDE_SAVERESTORE :
  -------------------------------------------------------------------------------------
  Normally, binary save/restore of Verilog parse trees to disk (using physical library mapping),
  we save/restore ONE primary unit (with its secondary units) into one binary (sdb) file.
  If an entire library needs to be saved, with hundreds or thousands of design units, then
  the number of binary dumped files can become excessive. For this reason, a customer filed VIPER issue 2944.
  With this compile switch enabled, the routine veri_file::SaveLib(char *libname) becomes available,
  which allows all units in one library to be saved to one library-wide binary dump file (an sdbl file).

  This file is created in the normal Verilog physical library mapping search path (using standard veri_file
  GetLibraryPath() and GetDefaultLibraryPath() API routines).
  After creation of an library-wide dump file (and if this compile switch is set), routine
  veri_file::Restore() will look BOTH in the standard vdb files AND into this library-wide sdbl file
  to find and restore the requested unit from disk (vdb files have higher priority than units in a sdbl file).

  The sdbl file can only be created as a whole, at once, using veri_file::SaveLib(). Individual units cannot be replaced.
  However, individual unit saves (using standard veri_file::Save() will create a sdb file any way, which
  has higher priority during restore. So, in essence, sdbl files provide a transparent second option
  to store and retrieve Verilog design units to/from disk.
*/

#define VERILOG_USE_LIB_WIDE_SAVERESTORE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_PROCESS_STRING_PRAGMAS_AS_ATTRIBUTES
  -------------------------------------------------------------------------------------
    Normally pragmas are parsed specially and are annotated into the created parse
    tree as attributes. String pragmas are stored as it is in the parse tree, but
    if string values are given to attributes, that string is treated as LRM specified
    string literal and so special processing is performed for escaped characters.
    If this compile flag is on, string pragmas will be processed as string literal
    and will be stored in parse tree and netlist in the same format as string attributes.
    This will work when VERILOG_PROCESS_PRAGMAS_AS_COMMENTS compile flag is off.

*/

//#define VERILOG_PROCESS_STRING_PRAGMAS_AS_ATTRIBUTES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_SPECIALIZED_VISITORS (VIPER #4959)
  -------------------------------------------------------------------------------------
    Historically, the visitor class uses the same method name for all the classes.
    This kind of visitor implementation is problematic/error prone in some cases
    because of the C++ inheritance and overload behavior at once. An example:

        Class MyVisitor: public VeriVisitor
        {
        public:
            virtual void Visit(VeriTreeNode &node) ;
            virtual void Visit(VeriForever  &node) ;
        } ;

        void MyVisitor::Visit(VeriTreeNode &node)
        {
            cout << "CLASS_ID: " << node.GetClassId() << endl ;
        }

        void MyVisitor::Visit(VeriForever &node)
        {
            Visit((VeriLoop &)node) ;
        }

    One would expect the visitor for a 'forever' node to traverse its statement,
    but it will not. The call to the Visit method for the VeriLoop class actually
    call the Visit method of the MyVisitor class for the nearest parent class of
    the VeriLoop class (which is the Visit method for VeriTreeNode in MyVisitor).

    One could, then, explicitly call the Visit method of the VeriVisitor class:

        void MyVisitor::Visit(VeriForever &node)
        {
            VeriVisitor::Visit((VeriLoop &)node) ;
        }

    With this implementation the visitor works fine in the development step. But
    when a Visit method is added in the MyVisitor class for VeriLoop it is not
    called for VeriForever nodes, because they still call the method of the
    VeriVisitor class.

    Finally one could call the Visit method of VeriVisitor class for the current
    node class:

        void MyVisitor::Visit(VeriForever &node)
        {
            VeriVisitor::Visit(node);
        }

    With this implementation, MyVisitor::Visit(VeriLoop &) is called if it exists
    and VeriVisitor::Visit(VeriLoop &) is called if it does not. But this method
    may not be used for a class that has children nodes in case user have to do
    some processing after visiting the base class but before visiting the child
    nodes.

    Compared to this, a visitor implementation with different method names for
    each class does not have the pitfalls of the first two methods and allows
    inserting user code in the correct place of the traversal process, which
    the third method cannot.

    So, we now implemented our visitor with different method names. At the same
    time we have to be backward compatible for already written codes that our
    customers may have. Here is a short description about how to use the Visitor:

    With compile flag VERILOG_SPECIALIZED_VISITORS turned ON:

        Class VeriVisitor
        {
            ...

        public:
            virtual void VisitVeriTreeNode(VeriTreeNode &node) ;
            virtual void VisitVeriModule(VeriModule &node) ;
            ...
        } ;

    So, the user need to override the functions VisitVeriTreeNode(VeriTreeNode &),
    VisitVeriModule(VeriModule &) etc in his/her derived visitor class.

    And with compile flag VERILOG_SPECIALIZED_VISITORS turned OFF:

        Class VeriVisitor
        {
            ...

        public:
            virtual void Visit(VeriTreeNode &node) ;
            virtual void Visit(VeriModule &node) ;
            ...
        } ;

    So, the user need to override the functions Visit(VeriTreeNode &),
    Visit(VeriModule &) etc in his/her derived visitor class as before.

    We have implemented three macros (VERI_VISIT, VERI_VISIT_NODE, VERI_ACCEPT) to handle
    the situation. This macros depend on the compile flag and define the proper names
    that can be used in the code. Please, check the VeriVisitor.h/VeriVisitor.cpp file
    for more details of these macros. Customers are encouraged to implement visitors
    with these macros, then the code becomes independent of this compile switch.
    Please see the implementation or the class FuncVisitor in VeriModuleItem.cpp
    for an example.
*/
// JJ: 5/2013: Customer req this to be on, email sub: question about Verilog support for concats with multipliers of 0
#define VERILOG_SPECIALIZED_VISITORS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_FLEX_READ_FROM_STREAM (VIPER #5726)
  -------------------------------------------------------------------------------------
    Historically, flex uses FILE * to read from input files. With this compile flag
    user can make flex read from (C++) streams instead of (C) FILE *.

    Verific has defined a new abstract stream class 'verific_stream'. Flex uses object
    of this class to read from streams. Another class 'verific_istream' is defined and
    it is derived from 'verific_stream'. This class takes a C++ stream object and reads
    from it.

    So, to simply read from an istream (or derived) class, use 'verific_istream' with
    that object and flex will read from that stream.

    To read from other sources, you have to do the following:
      1) Derive your own class from 'verific_stream'
      2) Define a function that takes a const char *file_name and returns an object
         of your derived class.
      3) Register this function with veri_file::RegisterFlexStreamCallBack() and you
         are done. Now, flex will read from your stream.

    For an example, we implemented reading from gzipped files as an application.
    Please check example_apps/verilog_apps/stream_apps directory for more details.
*/

//#define VERILOG_FLEX_READ_FROM_STREAM // --- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_FLEX_DECRYPT_PROTECTED_BLOCKS_USING_STREAM (VIPER #6665)
  -------------------------------------------------------------------------------------
    By default Verific decrypts all the protected block string at once
    by calling Protect::decrypt() method on the user specified cipher
    object.

    But decrypting the whole protected block at once may be considered
    to be a large security flaw because it makes hacking the software
    to obtain the decrypted model too easy. It may even become visible
    in crash dumps.

    This compile flag can be used to decrypt the protected string in
    user control with specially coded streams. This stream can decrypt
    the encrypted text block by block or even character by character
    at users will. Verific will not control the decryption method.

    To use it, the decrypt routines that returns verific_stream object
    in Protect class need to be defined in user derived decryption
    object. Along with that user needs to derive a custom stream class
    from verific_stream class that should have the special read() and
    gcount() method defined properly to decrypt the string part by part.
    Object of this special stream class should be returned from the
    overridden decrypt() methods.

    Note that to use this facility, you need to define the compile flag
    VERILOG_FLEX_READ_FROM_STREAM above since it makes use of streams.
*/

#ifdef VERILOG_FLEX_READ_FROM_STREAM
// This flag can only be used if VERILOG_FLEX_READ_FROM_STREAM compile flag is set.
//#define VERILOG_FLEX_DECRYPT_PROTECTED_BLOCKS_USING_STREAM // --- Comment this line out to control flag from build environment, or uncomment to force on.
#endif

/*-------------------------------------------------------------------------------------
    VERILOG_MOVE_YV_MODULES_INTO_WORK_LIBRARY (VIPER #5816)
  -------------------------------------------------------------------------------------
    Historically, Verific keeps -y/-v specific modules in an user-library.
    The same thing is being done for `uselib directives also.

    But other tools seems to include the resolved modules into the current
    work library. Also, customers may want to elaborate a -y/-v specific
    modules resolved from those directives.

    So, Verific added this capability under this compile switch. If this
    switch is turned on, we will move the -y/-v resolved modules into the
    current work library. Otherwise we keep them in the user-library as
    usual. Please note that `uselib resolved modules are always kept in
    the user-library irrespective of this compile switch. This is because
    module with same name may reside in multiple `uselib specific library
    and tools use them as directed. So, to mimic this behaviour we have
    to keep the `uselib specific libraries and their modules as it is.
*/

// DD: 3/2013: VERILOG_MOVE_YV_MODULES_INTO_WORK_LIBRARY is now a run-time flag RuntimeFlags::GetVar("veri_move_yv_modules_into_work_library") initialized in file VeriRuntimeFlags.h.
#define VERILOG_MOVE_YV_MODULES_INTO_WORK_LIBRARY // --- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK (VIPER #5727)
  -------------------------------------------------------------------------------------

    This compile flag will enable name creation for unnamed generate blocks according
    to System verilog LRM P1800-2009 section 27.6.

    So, if design is like

    generate
         if (p == 1)
         begin
            mod I() ;
         end

         if (p == 2)
         begin
            mod1 I() ;
         end
    endgenerate

    Then unnamed generate blocks will get name and so the design will be

    generate
         if (p == 1)
         begin : genblk1   // get name genblk1
            mod I() ;
         end

         if (p == 2)
         begin : genblk2   // get name genblk2
            mod1 I() ;
         end
    endgenerate

    When this compile flag is enabled, instantiation of first if-generate should be
    visible from outside generate by name 'genblk1.I'. If this compile flag is off,
    same instantiation will be visible from outside generate by name 'I'.

    According to section 27.5 of LRM,
    Generate blocks in conditional generate constructs can be named or unnamed,
    and they may consist of only one item, which need not be surrounded by begin-end
    keywords. Even if the begin-end keywords are absent, it is still a generate
    block, which, like all generate blocks, comprises a separate scope and a new
    level of hierarchy when it is instantiated.

    So as per above section, if generate code is

             if (p)
                mod I() ;

    The item of the above if-generate is an unnamed generate block and according
    to section 27.6 that block should get a name, i.e. above if-generate should become

              if (p) // if-branch will have a generate block named 'genblk1'
              begin : genblk1
                  mod I() ;
              end : genblk1

    Same section of LRM also says :

    "The generate blocks of the directly nested construct are treated as if they
    belong to the outer construct.  Therefore, they can have the same name as the
    generate blocks of the outer construct, and they cannot have the same name as
    any declaration in the scope enclosing the outer construct (including other
    generate blocks in other generate constructs in that scope)."

    That means following design is illegal :

          logic b ;
          if (p)
            if (q)
            begin : b  // 'b' is declared in scope enclosing outer construct
                       // So using of name 'b' is illegal here
               reg x ;
            end

    Now this also implicitly says that, since outer if-generate does not have a
    generate block, it will not get any name. Only the nested if-generate will get a
    name.
    So if design is

          if (p)
             if (q)
                reg x ;
                         
    After specifying names to unnamed block, design will be

           if (p) // This will not get any block name
              if (q)
              begin : genblk1
                 reg x ;
              end : genblk1

*/

// DD: 9/2013: VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK is now a run-time flag RuntimeFlags::GetVar("veri_create_name_for_unnamed_gen_block") initialized in file VeriRuntimeFlags.h.
#define VERILOG_CREATE_NAME_FOR_UNNAMED_GEN_BLOCK // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_QUICK_PARSE_V_FILES
  -------------------------------------------------------------------------------------

    Normally, -v files may contain multiple module definitions. Verilog parser
    is supposed to pick only the required modules from that file.

    Now, it may be the case that the file does not contain any of the required
    modules instead it contains many many other modules and is of huge size.
    In such case, analyzing and creating parse tree for the full file will take
    time and memory.

    To prevent this, we provided this compile flag which will quickly parse
    the file and see if it is actually required or not without creating the
    parse tree. If it is not required we will ignore the file otherwise we will
    fully parse it and pick the required modules.

    The compile flag VERILOG_QUICK_PARSE_V_FILES enables or disables this feature.
    When it is enabled, we quickly parse all the -v files otherwise we always
    fully parse and create parse trees for them.
*/

//#define VERILOG_QUICK_PARSE_V_FILES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS
  -------------------------------------------------------------------------------------

    With VERILOG_QUICK_PARSE_V_FILES we quickly parse each -v files to see if the file
    provides one of the required modules. But we do not store the name of all the modules
    for later use. The reason is macros. In case the file uses or defines any macro,
    parsing it again may actually change its content. Thus a previously visible module
    may no longer be available and also modules which were not visible may become
    avaialble now. So, we cannot really store the info of modules in there.

    However, if there is no macro definition or reference in the -v file, we can store
    the module info. This compile flag, when enabled, stores that info.

    Note that turning the compile flag ON, will increase memory usage and possibly
    decrease run-time. Also note that this compile flag has no effect if the compile
    flag VERILOG_QUICK_PARSE_V_FILES is OFF.
*/

//#define VERILOG_QUICK_PARSE_V_FILES_STORE_INFO_OF_FILES_WITHOUT_MACROS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_ALTERNATE_GENERATE_ELAB
  -------------------------------------------------------------------------------------

    Normally, static elaboration removes generate constructs like generate-if,
    generate-case, generate-for and generate-block all together from the parse
    tree and moves the non generate module-items present inside those constructs
    to module level.
    For example if generate for-loop is like

             for (genvar i = 0; i < 2; i++) begin : b
                 mod I() ;
             end

    After static elaboration elaborated for-generate-loop will look like

            mod \b[0].I () ;
            mod \b[1].I () ;

    Now if VERILOG_ALTERNATE_GENERATE_ELAB compile flag is turned on, generate
    elaboration will change a lot. Generate blocks will remain in the parse tree,
    for-generate-loop will be converted to multiple generate blocks and depth of
    non-generate-module items defined inside generate items will remain same

    So above for-generate loop should be elaborated like :

           begin : \b[0]
               mod I() ;
           end

           begin : \b[1]
               mod I() ;
           end
*/

// VIPER #8114: VERILOG_ALTERNATE_GENERATE_ELAB is now a run-time flag RuntimeFlags::GetVar("veri_alternative_generate_elab") initialized in file VeriRuntimeFlags.h
#define VERILOG_ALTERNATE_GENERATE_ELAB // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB
  -------------------------------------------------------------------------------------

    This flag should be enabled only when VERILOG_ALTERNATE_GENERATE_ELAB
    is turned on. Otherwise it will not create any local parameter.
    When VERILOG_ALTERNATE_GENERATE_ELAB compile flag is on, loop generate
    elaboration will create multiple generate blocks. In those generate blocks
    reference of the loop index variable are replaced by its appropriate iteration
    value. But P1800-2009 SV LRM Section 27.4 says 'Within the generate block of
    a loop generate construct, there is an implicit localparam declaration. This is
    an integer parameter that has the same name and type as the loop index variable,
    and its value within each instance of the generate block is the value of the
    index variable at the time the instance was elaborated. This parameter can be
    used anywhere within the generate block that a normal parameter with an integer
    value can be used. It can be referenced with a hierarchical name.'
    Now if this compile flag is enabled, reference of loop generate variable within
    the created generate blocks will not be replaced by its value. In each created
    generate block, a localparam will be declared with same name as the loop index
    variable and its default value will be corresponding value of loop index variable.
    All references of genvars within those generate blocks will be replaced by the
    references of this new localparameter.
    For example if generate for-loop is like

             for (genvar i = 0; i < 2; i++) begin : b
                 mod I(in[i], out[i]) ;
             end

    For-generate loop should be elaborated like :

           begin : \b[0]
               localparam i = 0 ;
               mod I(in[i], out[i]) ;
           end

           begin : \b[1]
               localparam i = 1 ;
               mod I(in[i], out[i]) ;
           end
*/

#define VERILOG_DEFINE_LOCALPARAM_FOR_FORGEN_ELAB // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_PATHPULSE_PORTS
  -------------------------------------------------------------------------------------

  Pulse control specparam declaration can specify input and output terminal descriptor as a part of
  specparam name using following syntax

  pulse_control_specparam ::=
    PATHPULSE$ = ( reject_limit_value [ , error_limit_value ] ) ;
  | PATHPULSE$specify_input_terminal_descriptor$specify_output_terminal_descriptor
       = ( reject_limit_value [ , error_limit_value ] ) ;

  If this compile flag is set, we will add an extra member 'VeriPathPulseValPorts *_path_pulse_ports'
  in VeriParamId to store the input and output terminal descriptor associated with the pulse control
  specparam name.

  For example, if specparam is declared as
      PATHPULSE$clk$q = (2,9) ;

  A VeriParamId will be created with name 'PATHPULSE$clk$q' and its member VeriPathPulseValPorts will
  also be created to store reference of 'clk' and 'q' as VeriName*.

*/

//#define VERILOG_PATHPULSE_PORTS // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_DO_NOT_TOUCH_PARSE_TREE :
  -------------------------------------------------------------------------------------
    This compile flag enables that the Verilog static elaborator will copy a
    module if that module needs to be modified during static elaboration.
    This way an original version of every module will remain in parse tree and
    copied version will be elaborated and modified. This applies to both top
    level and instantiated modules.
*/

//#define VERILOG_DO_NOT_TOUCH_PARSE_TREE // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VERILOG_DO_NOT_UNIQUIFY_INTERFACE_INSTANCES :
  -------------------------------------------------------------------------------------
    This compile flag enables that the Verilog static elaborator will not create
    unique interface for every interface instantiation, when VERILOG_UNIQUIFY_ALL_INSTANCES is on.
    If the design instantiates an interface twice with same parameter settings,
    one copy of instantiated module will be created.

    Consider the following example :

         module top ;
           my_interface I1() ;
           my_interface I2() ;

           my_interface #(3) I3() ;
           my_interface #(3) I4() ;
           child  I5(I1) ;
           child  I6(I2) ;
           child  I7(I3) ;
           child  I8(I4) ;
        endmodule

    Now if VERILOG_UNIQUIFY_ALL_INSTANCES is on and this compile switch is on,
    new copy of module child will be created for every instances, but for interface
    instance only two copies of interface will be created. So after elaboration
    design will look like :

          module top ;

            my_interface I1() ;
            my_interface I2() ;

            \my_interface(p=3)  I3() ;
            \my_interface(p=3)  I4() ;

            child_uniq1 I5 (I1) ;
            child_uniq2 I6 (I2) ;
            child_uniq3 I7 (I3) ;
            child_uniq4 I8 (I4) ;
          endmodule

    But if this compile switch is off and VERILOG_UNIQUIFY_ALL_INSTANCES is on,
    new copy of interface 'my_interface' will be created for every instance i.e
    four copies of interface 'my_interface' will be created.
*/

//#define VERILOG_DO_NOT_UNIQUIFY_INTERFACE_INSTANCES // <--- Comment this line out to control flag from build environment, or uncomment to force on.

#endif // #ifndef _VERIFIC_VERI_COMPILEFLAGS_H_
