/*
 *
 * [ File Version : 1.128 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>   // for strchr
#include <stdio.h>   // for strchr

#include "Map.h"
#include "Set.h"
#include "Strings.h"

#include "VeriScope.h"
#include "VeriId.h"
#include "VeriModule.h" // for module deletion
#include "VeriExpression.h" // for object finding in parent class
#include "VeriLibrary.h" // to find if item of 'std' package is accessed through root
#include "VeriMisc.h" // for VeriConstraint
#include "veri_tokens.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VeriScope::VeriScope(VeriScope *upper, VeriIdDef *owner) :
    _upper(upper),
    _owner(owner),
    _this_scope(0),
    _using(0),
    _used_by(0),
    _children(0),
    _imported_scopes(0),
    _empty_scope(0),
    _generate_scope(0),
    _struct_union_scope(0)
    , _seq_block_scope(0)
    , _loop_scope(0)
    , _is_std_pkg_used(0)
    , _par_block_scope(0)
    , _exported_subprog_scope(0)
    , _extended_scope(0)
    , _base_class_scope(0)
    , _struct_constraints(0)
    , _exported_scopes(0)
{
    VERIFIC_ASSERT(this!=_upper) ;

    _this_scope = new Map(STRING_HASH) ;

    // Set a scope pointer back from the owner..
    if (_owner) _owner->SetLocalScope(this) ;

    // Add this scope into the upper scope as child:
    if (_upper) _upper->AddChildScope(this) ;
}
// Usually empty scope should not have a owner, so that field of constructor is removed
// and added new argument first to resolve ambiguity.
VeriScope::VeriScope(unsigned empty, VeriScope *upper) :
    _upper(upper),
    _owner(0),
    _this_scope(0),
    _using(0),
    _used_by(0),
    _children(0),
    _imported_scopes(0),
    _empty_scope(empty),
    _generate_scope(0),
    _struct_union_scope(0)
    , _seq_block_scope(0)
    , _loop_scope(0)
    , _is_std_pkg_used(0)
    , _par_block_scope(0)
    , _exported_subprog_scope(0)
    , _extended_scope(0)
    , _base_class_scope(0)
    , _struct_constraints(0)
    , _exported_scopes(0)
{
    VERIFIC_ASSERT(this!=upper) ;

    _this_scope = new Map(STRING_HASH) ;

    // Add this scope into the upper scope as child:
    if (_upper) _upper->AddChildScope(this) ;
}

VeriScope::~VeriScope()
{
    // Delete a scope level

    // Delete the dependency lists.
    // First of all, let the scopes that I am using know that I am no longer there :
    if (_using) {
        SetIter si ;
        VeriScope *other_scope ;
        FOREACH_SET_ITEM(_using, si, &other_scope) {
            // VIPER #5115 : Do not delete _using within 'NoLongerUsing' when
            // we are iterating the _using
            NoLongerUsing(other_scope, 0 /* do not delete _using*/) ;
        }
        delete _using ;
        _using = 0 ;
    }

    // Let the scopes that rely on 'this' know that we are no longer there.
    // Now, if somebody still relies on me, gotta delete them too.
    // Need to delete their design unit actually, since we don't delete sub-trees.
    if (_used_by && _used_by->Size()) {
        SetIter si ;
        VeriScope *other_scope ;
        VeriIdDef *this_design_unit = GetContainingModule() ;
        VERIFIC_ASSERT(this_design_unit) ; // module id should not be destroyed before its scope.
        VeriIdDef *other_design_unit ;
        VeriModule *other_module ;
        FOREACH_SET_ITEM(_used_by, si, &other_scope) {
            other_scope->NoLongerUsing(this, 0) ; // Remove me from their list.

            other_design_unit = other_scope->GetContainingModule() ;
            if (!other_design_unit) continue ;
            if (other_design_unit == this_design_unit) continue ; // no need to delete myself (again)

            // pick up the parse tree :
            other_module = other_design_unit->GetModule() ;
            if (!other_module) continue ;

            // Do not delete this other design unit if it is not (or no longer) in a library.
            // This way, we cannot accidentally delete a nested module (part of another one),
            // delete a module twice (by mistake, during circular dependencies),
            // and it offers a way to avoid recursive deletions (with messages) during RemoveAllModules().
            if (!other_module->GetLibrary()) continue ;

            // Give warning and destroy the unit that is now out of date
            other_design_unit->Warning("re-analyze unit %s since unit %s is overwritten or removed",
                other_design_unit->Name(), this_design_unit->Name()) ;

            // Delete the parse tree for this module.
            // Use the veri_file interface for that (until destructor is self-detaching).
            delete other_module ; // will first self-detach from lib, and then destroy itself.
        }
    }

    delete _used_by ;
    _used_by = 0 ;

    // Remove 'this' scope from the upper scope:
    if (_upper) _upper->RemoveChildScope(this) ;
    SetIter si ;
    VeriScope *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        // Reset 'upper' scope of the children:
        if (child) child->SetUpper(0) ;
    }
    // And also delete the 'Set' containing the children scopes:
    delete _children ;
    _children = 0 ;

    // If owner of this scope is still there, set its scope pointer back to 0 (to avoid any stale pointers)
    if (_owner) _owner->SetLocalScope(0) ;

    // Presently, all identifiers are owned by the scope they are declared in.
    // So, delete them here, with the scope.
    MapIter miter ;
    VeriIdDef *v ;
    FOREACH_MAP_ITEM(_this_scope,miter,0,&v) {
        // VIPER #5457: No need to reset the local scope. Let it remain here,
        // otherwise the scope may still have the owner set to this 'v'.
        // If the scope is deleted, the local scope is reset any way.
        //v->SetLocalScope(0) ;   // Set IdDef's local scope (if it exists) to NULL.
        delete v ;
    }

    delete _this_scope ;
    _upper = 0 ;
    _owner = 0 ;
    Set *s ;
    FOREACH_MAP_ITEM(_imported_scopes,miter,0,&s) delete s ;
    delete _imported_scopes ; // Delete imported scope map
    _extended_scope = 0 ;
    _base_class_scope = 0 ;
    MapIter mi ;
    Array *constraints ;
    FOREACH_MAP_ITEM(_struct_constraints, mi, 0, &constraints) {
        unsigned i ;
        VeriConstraint *ele_constraint ;
        FOREACH_ARRAY_ITEM(constraints, i, ele_constraint) delete ele_constraint ;
        delete constraints ;
    }
    delete _struct_constraints ; // FIXME Delete its content
    Map *items ;
    FOREACH_MAP_ITEM(_exported_scopes, mi, 0, &items) delete items ;
    delete _exported_scopes ; // VIPER #7232
}

// Add identifier to the scope
// VIPER #7773: Set the 'existing_id' to the onle already declared if Declare() fails because of an already existing id.
unsigned
VeriScope::Declare(VeriIdDef *id, unsigned force_insert, VeriIdDef **existing_id /*=0*/)
{
    if (existing_id) *existing_id = 0 ; // VIPER #7773
    if (!_this_scope || !id) return 0 ;
    // VIPER 2573, 2595, 2603 : Reset empty field, any variable declaration make an unnamed scope non-empty
    // VIPER #5952: Do not reset the empty scope if the id being declared is a gen-var-id.
    // Normally, we should reset the empty scope if anything is declared in it, but since
    // all the tools produce error for the design of this VIPER we do not reset it here.
    // So, the block id will be declared in the module scope, as block ids are declared
    // only in non-empty scopes. Thus, there will be an already declared-id, in that case.
    if (!id->IsGenVar()) _empty_scope = 0 ; // Reset only if it is not a genvar

#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    // Set the owning scope of the identifier :
    id->SetOwningScope(this) ;
#endif

    if (!force_insert) { // Multiple declaration of an identifier is not allowed
        // VIPER #3188 :If identifier is already declared in this scope or explicitly imported
        // here, another identifier of same name cannot be declared.
        // This means we need to check for the existence of this identifier in more places than just _this_scope.
        // So, call FindLocal() and do not declare it in this scope if it already exists in the 'local' scope.
        VeriIdDef *exist = FindLocal(id->Name()) ;
        if (exist) {
            if (existing_id) *existing_id = exist ; // VIPER #7773
            return 0 ;
        }
    }

    // Add declared identifier to scope. Return 1 if id was not in the table.
    // Without force_insert set, a previously declared identifier name will NOT be declared.
    if (existing_id) *existing_id = (VeriIdDef *)_this_scope->GetValue(id->Name()) ; // VIPER #7773
    return _this_scope->Insert(id->Name(), id, 0, force_insert) ; // returns 0 if id was previously in the table.
}

char*
VeriScope::GenerateUniqueVarName(char* id_name) const
{
    // Create and declare a variable
    // Create an unique name to create and declare a variable with that name enclosing module scope
    char *unique_name = id_name ;
    VeriIdDef *var = FindLocal(unique_name) ;
    if (var) { // Name conflict, create unique name
        unsigned i = 0 ;
        char *id_full_name = 0 ;
        char temp_id_name[100] ;
        do {
            sprintf(temp_id_name, "%d", i++) ;
            Strings::free(id_full_name) ;
            id_full_name = Strings::save(unique_name, temp_id_name) ;
        } while (FindLocal(id_full_name)) ;
        Strings::free(unique_name) ;
        unique_name = id_full_name ; // Memory leak fix was: Strings::save(id_full_name) ;
    }
    return unique_name ;
}

// Declare design defined block ids in non-empty scope.
unsigned
VeriScope::DeclareBlockId(VeriIdDef *id, unsigned force_insert, VeriIdDef **existing_id /*=0*/)
{
    if (existing_id) *existing_id = 0 ; // VIPER #7773
    if (!id || !id->IsBlock()) return 0 ;
    // VIPER 2573, 2595, 2603 : Do not declare block identifier in an empty scope
    // Traverse upwards until an non-empty scope is found to declare it.
    // VIPER #8303 : If this empty scope is the scope of label, allow declaration of block id here
    // and make it non-empty
    if (_owner && _owner->IsLabelWithScope() && _empty_scope) {
        ResetEmptyScope() ; // Reset empty scope
    } else if (_empty_scope && _upper) {
        return _upper->DeclareBlockId(id, force_insert, existing_id) ;
    }

    return Declare(id, force_insert, existing_id) ; // Declare it in this scope
}

unsigned
VeriScope::Undeclare(VeriIdDef *id)
{
    // Remove identifier from scope
    if (!_this_scope || !id) return 0 ;

#ifdef VERILOG_ID_SCOPE_BACKPOINTER
    // Un-set the owning scope of the identifier :
    id->SetOwningScope(0) ;
#endif

    // Issue 3057 : Make sure that only the exact identifier gets removed.
    // Not just one that matches by name.
    MapItem *mi ;
    VeriIdDef *exist ;
    FOREACH_SAME_KEY_MAPITEM(_this_scope, id->Name(), mi) {
        exist = (VeriIdDef*)mi->Value() ;
        if (exist==id) {
            // Remove from this scope :
            return _this_scope->Remove(mi) ;
        }
    }
    // Mmm. id was not in this scope..
    return 0 ;
}

VeriScope *
VeriScope::ModuleScope()
{
    // Give pointer to the module level scope (owner should be a module)
    if (_owner && _owner->IsModule()) return this ;
    if (_upper) return _upper->ModuleScope() ;
    return this ;
}

VeriScope *
VeriScope::TopScope()
{
    // Give pointer to the upper most scope (should be scope of 'root module', but can also be single module.
    if (_upper) return _upper->TopScope() ;
    return this ;
}
// Find identifier in explicitly imported items
VeriIdDef *
VeriScope::FindExplicitlyImported(const char *name) const
{
    MapIter mi ;
    VeriScope *scope ;
    Set *names ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, &names) { // Iterate over explicitly imported scopes
        if (!scope || !names) continue ;

        // Check if 'name' is explicitly imported :
        if (names->GetItem(name)) { // 'name' is explicitly imported
            VeriIdDef *id =  scope->FindLocal(name) ; // Return identifier from package scope
            if (!id) id = scope->FindExported(name) ; // Find exported items also (VIPER #7232)
            // VIPER #4977 : Store the information that how many times it is referenced
            // VIPER #6270 : Increase reference count if it is locally declared in 'scope'
            Map *id_map = scope->GetThisScope() ;
            if (id_map && (id_map->GetValue(name) == id)) VeriNode::IncreaseReferenceCount(id, 1) ;
            if (id) return id ;
        }
    }
    return 0 ;
}
unsigned VeriScope::IsStdPackageExplicitlyImported() const
{
    // VIPER #4957 : Depend on flag to determine whether objects of package 'std' is used
    // FIXME : Undo #4957 in VeriScope::Find. So we do not need this flag
    if (_is_std_pkg_used) return 1 ;
    MapIter mi ;
    VeriScope *scope ;
    Set *names ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, &names) { // Iterate over explicitly imported scopes
        if (!scope || !names) continue ;
        if ((names->Size() == 1) && names->GetItem("*")) continue ;

        // This is implicitly imported scope
        scope = scope->ModuleScope() ;
        VeriIdDef *package_id = scope ? scope->GetOwner() : 0 ;
        // Check whether it is package 'std' compiled in library 'std'
        if (package_id && package_id->IsPackage() && Strings::compare(package_id->Name(), "std")) {
            VeriModule *package_body = package_id->GetModule() ;
            VeriLibrary *lib = (package_body) ? package_body->GetLibrary(): 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) return 1 ;
        }
    }
    return 0 ;
}

unsigned VeriScope::IsStdPackageVisible() const
{
    if (_is_std_pkg_used) return 1 ;

    MapIter mi ;
    VeriScope *scope ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, 0) {
        VeriIdDef *package_id = (scope) ? scope->GetContainingModule() : 0 ;
        // Check whether it is package 'std' compiled in library 'std'
        if (package_id && package_id->IsPackage() && Strings::compare(package_id->Name(), "std")) {
            VeriModule *package_body = package_id->GetModule() ;
            VeriLibrary *lib = (package_body) ? package_body->GetLibrary() : 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) return 1 ;
        }
    }

    if (_upper) return _upper->IsStdPackageVisible() ; // Check the upper scopes too

    return 0 ;
}

// Find identifiers in the scope
VeriIdDef *
VeriScope::Find(const char *name, const VeriTreeNode *from)
{
    if (!name) return 0 ;

    // Find it local
    VeriIdDef *id = FindLocal(name) ;
    if (id) return id ;

    // VIPER #4954: Find in base class scope:
    //if (_base_class_scope) id = _base_class_scope->FindInBaseClass(name) ;
    //if (id) return id ;
    id = FindInBaseClass(name, from) ;
    if (id) return id ;

    // MB/RD 7/2006:
    // VIPER #3188 :FindLocal routine already searched locally declared identifiers and explicitly
    // imported identifiers.

    // 'name' is not explicitly imported, find in wildcard imported scopes
    // If two wildcard imported scopes contain 'name', it is an error
    MapIter mi ;
    VeriScope *scope ;
    Set *names ;
    VeriIdDef *decl_id ;
    VeriScope *decl_scope = 0 ;
    unsigned error = 0 ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, &names) {
        if (!scope || !names) continue ;

        if (!names->GetItem("*")) continue ; // This scope is not wildcard imported, continue
        // Find the declared identfiers in the imported scope :
        decl_id = scope->FindLocal(name) ;
        if (!decl_id) decl_id = scope->FindExported(name) ; // Find exported items also (VIPER #7232)
        if (!decl_id) continue ; // no such identifier included

        if (id && id!=decl_id) {
            // Same 'name' is imported from multiple packages
            // FIXME : In wildcard import, each identifier is imported only when
            // it is referenced in the importing scope. If design has two import clauses
            // P :: *
            // Q :: mem
            // and package 'P' also contains 'mem', it is an error only if 'mem' is
            // referenced before second import declaration (Q::mem). Here we do not
            // implement this scenario. Currently we will give error for the above case
            // irrespective of the fact that 'mem' is used after first import decl or not
            // Issue error.
            error = 1 ;
            /* VIPER #6919 : Donot produce error now, Error will be given from
            // VeriIdRef::Resolve calling API FindImplicitlyImportedErrorCond
            if (!from) from = decl_id ; // use 'from' for linefile, but in absence of that, use decl_id's linefile.
            // Issue 3135 : better message :
            from->Error("%s is visible via multiple package imports", id->Name()) ;
            // Mention both identifiers :
            decl_id->Info("%s is declared here",id->Name()) ;
            id->Info("%s is declared here",id->Name()) ;
            */
            break ; // stop looking for more.
        }
        // Set this id as the one included :
        id = decl_id ;
        decl_scope = scope ;
    }
    if (id && !error) {
        // This 'name' declared in scope 'decl_scope' is wildcard imported. Now
        // it is referenced and so it will be explicitly imported.
        names = (_imported_scopes) ? (Set*)_imported_scopes->GetValue(decl_scope): 0 ;
        // VIPER #4957 : Do not explicitly import referred identifier. If we do that
        // we will produce illegal redeclaration warning for
        // import P::* ;
        // wire x ; // 'x' is declared within package P.
        // If we do not explicitly import referred identifier, #3188 will be re-open
        // MB: 040809 : Undo fix for #4957 here. It is creating other issues for
        // scope name resolving. File viper #4977 for the real issue.
        if (names) (void) names->Insert(id->Name()) ; // make it explicitly imported
        // VIPER #4977 : Store the information that how many times it is referenced
        VeriNode::IncreaseReferenceCount(id) ;

        // But, we will mark the scope if this identifier 'id' is from package 'std'.
        // This will help us to determine whether std package(root module as std package
        // is implicitly imported in root scope) is to be included in the using list of
        // module. If any identifier declared in std package is used in a module, we need
        // root scope (std package scope) in the using list of module. Otherwise Viper #4951
        // will fail.
        VeriScope *m_scope = (decl_scope) ? decl_scope->ModuleScope(): 0 ;
        VeriIdDef *package_id = m_scope ? m_scope->GetOwner() : 0 ;
        // Check whether it is package 'std' compiled in library 'std'
        if (package_id && package_id->IsPackage() && Strings::compare(package_id->Name(), "std")) {
            VeriModule *package_body = package_id->GetModule() ;
            VeriLibrary *lib = (package_body) ? package_body->GetLibrary(): 0 ;
            if (lib && Strings::compare(lib->GetName(), "std")) {
                _is_std_pkg_used = 1 ; // Mark 'std' package is used here
            }
        }
        return id ;
    }

    // Otherwise, find in upper level :
    if (_upper) {
        id = _upper->Find(name,from) ;
        if (id) return id ;
    }

    // On our way back, if id was not found,
    // check if its a local reference into the local module.
    if (_owner && _owner->IsModule() && Strings::compare(_owner->Name(), name)) {
        return _owner ;
    }

    // Here, cannot find it.
    return 0 ;
}

VeriIdDef *
VeriScope::FindLabelId(const char *name) const
{
    // Label (block) ids are declared in the parent (_upper) scope for empty scopes, so,
    // if this scope is not empty or is orphan, we will not find the label id here.
    if (!_empty_scope || !_upper) return 0 ;

    // This scope is already searched for the name (from FindLocal), so, it does not exist.
    // Directly find in parent scope to avoid finding in local scope again:
    VeriIdDef *id = (VeriIdDef *)_upper->_this_scope->GetValue(name) ;
    if (id && !id->IsLabel()) return 0 ; // found but not a label id

    // If it is not found, try to find it in (empty) parent scope:
    if (!id) id = _upper->FindLabelId(name) ;

    return id ;
}

VeriIdDef *
VeriScope::FindLocal(const char *name) const
{
    VeriIdDef *id = 0 ;
    // VIPER #3901 : If extended scope exists, for inline constraint we should
    // first find object in extended scope. If not found, follow normal finding
    if (_extended_scope) {
        //id = _extended_scope->FindLocal(name) ;
        //if (id) return id ;
        // VIPER #4180 : If _extended_scope is class scope, find its base class
        id = _extended_scope->FindInBaseClass(name, 0) ;
        if (id) return id ;
        // Find in extends clause specified classes for interface class
        //VeriIdDef *owner = _extended_scope->GetOwner() ;
        //if (owner && owner->IsClass()) id = _extended_scope->FindInInterfaceClasses(name) ;
        //if (id) return id ;
    }
    // Find the object if declared in this immediate, local scope
    if (!_this_scope) return 0 ;
    if (!name) return 0 ;
    id = (VeriIdDef*)_this_scope->GetValue(name) ;
    // VIPER #3188 : Consider explicitly imported identifiers as locally declared
    if (!id) id = FindExplicitlyImported(name) ;
    // VIPER #4788 & #4866: Find the declared statement label id (in the upper scope for empty scopes):
    if (!id) id = FindLabelId(name) ;
    // If it is exported function scope, check owner name to get required identifier :
    if (_exported_subprog_scope && _owner && Strings::compare(_owner->Name(), name)) {
        return _owner ;
    }
    return id ;
}
VeriIdDef *
VeriScope::FindInBaseClass(const char *name, const VeriTreeNode *from) const
{
    // First, find locally in this scope:
    VeriIdDef *id = FindLocal(name) ;
    if (id) return id ;

    // Then find in the base class only, do not find in upper scope:
    id = (_base_class_scope) ? _base_class_scope->FindInBaseClass(name, from) : 0 ;
    if (id) return id ;

    // Get class body
    VeriModuleItem *class_body = _owner ? _owner->GetModuleItem(): 0 ;

    // Only interface class can extend other interface class and can use objects
    // declared there
    if (!class_body || !class_body->GetQualifier(VERI_INTERFACE)) return 0 ;

    Array *interface_classes = class_body->GetInterfaceClasses() ;
    Set ids(POINTER_HASH) ;
    unsigned i ;
    VeriName *class_name ;
    FOREACH_ARRAY_ITEM(interface_classes, i, class_name) {
        VeriIdDef *class_id = class_name ? class_name->GetId(): 0 ;
        VeriScope *class_scope = class_id ? class_id->LocalScope(): 0 ;
        if (!class_scope) continue ;
        id = class_scope->FindInBaseClass(name, 0) ;
        if (id) (void) ids.Insert(id) ;
    }
    id = (ids.Size()) ? (VeriIdDef*)ids.GetAt(0): 0 ;
    if ((ids.Size() > 1) && id && (id->IsType() || id->IsParam())) {
        const VeriTreeNode *node = from ? from : 0 ;
        if (node) node->Error("the name %s must be defined in %s due to having conflicting visibility in multiple base classes", name, _owner ? _owner->Name(): "") ;
    }
    return id ;
}
VeriIdDef *
VeriScope::FindInInterfaceClasses(const char *name) const
{
    // Get class body
    VeriModuleItem *class_body = _owner ? _owner->GetModuleItem(): 0 ;

    // Only interface class can extend other interface class and can use objects
    // declared there
    if (!class_body || !class_body->GetQualifier(VERI_INTERFACE)) return 0 ;

    Array *interface_classes = class_body->GetInterfaceClasses() ;
    unsigned i ;
    VeriName *class_name ;
    FOREACH_ARRAY_ITEM(interface_classes, i, class_name) {
        VeriIdDef *class_id = class_name ? class_name->GetId(): 0 ;
        VeriScope *class_scope = class_id ? class_id->LocalScope(): 0 ;
        VeriIdDef *id = class_scope ? class_scope->FindLocal(name): 0 ;
        if (id) return id ;
        if (class_scope) id = class_scope->FindInInterfaceClasses(name) ;
        if (id) return id ;
    }
    return 0 ;
}

// Find identifiers that own the scopes */

VeriIdDef *
VeriScope::GetSubprogram() const
{
    // Get the subprogram (task/function) that encompasses this scope. If any.
    if (_owner && (_owner->IsTask() || _owner->IsFunction())) return _owner ;
    if (_upper) return _upper->GetSubprogram() ;
    return 0 ;
}

VeriIdDef *
VeriScope::GetSequenceProperty() const
{
    if (_owner && (_owner->IsSequence() || _owner->IsProperty())) return _owner ;
    if (_upper) return _upper->GetSequenceProperty() ;
    return 0 ;
}

VeriIdDef *
VeriScope::GetBlockLabel() const
{
    // Get the block label that most closely encompasses this scope. If any.
    if (_owner && _owner->IsBlock()) return _owner ;
    if (_upper) return _upper->GetBlockLabel() ;
    return 0 ;
}

VeriIdDef *
VeriScope::GetContainingModule() const
{
    // Get the module identifier that encompasses this scope. If any (it should)
    if (_owner && _owner->IsModule()) return _owner ;
    if (_upper) return _upper->GetContainingModule() ;
    return 0 ;
}
VeriIdDef *
VeriScope::GetClass() const
{
    // Get the class identifier that encompasses this scope. If any
    if (_owner && _owner->IsClass()) return _owner ;
    if (_upper) return _upper->GetClass() ;
    return 0 ;
}

void
VeriScope::CheckIncompleteTypedef() const
{
    // Check whether this scope contains an incomplete typedef:
    MapIter mi ;
    VeriIdDef *id ;
    VeriModuleItem *tree = 0 ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        // Skip all the ids that are not type and type parameter:
        // VIPER #7998: Check for IsForwardTypeDefined() to make sure it was really a forward typedef:
        if (!id || !id->IsType() || id->IsParam() || !id->IsForwardTypeDefined()) continue ;
        tree = id->GetModuleItem() ;
        // Here this is a forward declaration or class identifier:
        if (!id->GetDataType() && (!tree || !(tree->IsClass() || tree->IsCovergroup()))) {
            // VIPER #6609 (test1.sv): Check if this type-def was fully defined above/before:
            // 'Find' is not a 'const' function, so Find in _upper, FindLocal in this scope already failed.
            // CHECKME: Do we need to find in base class scope or imported scope of this scope?
            VeriIdDef *exist = (_upper) ? _upper->Find(id->Name()) : 0 ;
            tree = (exist) ? exist->GetModuleItem() : 0 ;
            // Check if this previous id is defined as a type and is *fully* defined, otherwise ignore it:
            if (exist && !exist->GetDataType() && (!tree || !(tree->IsClass() || tree->IsCovergroup()))) exist = 0 ;
            if (exist) {
                // Produce error here, user can suppress it if required:
                id->Error("forward typedef %s is already fully defined", id->Name()) ;
                exist->Info("%s is declared here", exist->Name()) ;
                // Now, update the data type, module-item and local scope back pointers:
                id->SetDataType(exist->GetDataType()) ;
                // CHECKME: Updating the module-item and local-scope back pointers may lead to corruptions:
                //if (!id->GetModuleItem()) id->SetModuleItem(exist->GetModuleItem()) ;
                //if (!id->LocalScope()) id->SetLocalScope(exist->LocalScope()) ;
            } else {
                // VIPER #3031: This forward typedef was actually never defined in this scope:
                id->Error("forward typedef %s was not defined in the scope where it is declared", id->Name()) ;
            }
        }
    }
}

void
VeriScope::AddExternalScope(VeriScope *scope)
{
    if (!scope) return ;

    // Make a cross-link between the two modules.

    // If the incoming scope's owner and the 'this' scope's owner have the same module name,
    // then they will refer to the same module.  In this case we should not link the scopes,
    // or else a core dump may occur. (VIPER #2583)
    if (GetOwner() && scope->GetOwner() && Strings::compare(GetOwner()->Name(), scope->GetOwner()->Name())) {
        GetOwner()->Warning("self-reference to %s ignored", GetOwner()->Name()) ;
        return ;
    }

    // This will make the external 'scope' visible from within 'this' scope.
    // Restrictions : works for 'module'-level scopes only for now (since we
    // also do intermodule-dependency checking with this list)
    // Register scope dependancy and add 'scope' to Map '_imported_scopes' so that
    // FindLocal routine can look in '_imported_scopes' instead of '_using' list
    AddImportItem(scope, 0, 0) ;
}

void
VeriScope::SetUpper(VeriScope *scope)
{
    if (_upper == scope) return ; // Nothing to do, already set as upper

    // Remove myself from the previous 'upper' scope:
    if (_upper) _upper->RemoveChildScope(this) ;
    _upper = scope ; // Set upper here
    // And insert myself in the list of children of the 'upper' scope:
    if (_upper) _upper->AddChildScope(this) ;
}

void
VeriScope::GetDeclaredIds(Set &ids) const
{
    // First insert my owner, if it is there:
    if (_owner) (void) ids.Insert(_owner) ;

    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        // Insert the id into the given Set:
        if (id) (void) ids.Insert(id) ;
    }

    SetIter si ;
    VeriScope *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        // Call myself recursively on my children:
        if (child) child->GetDeclaredIds(ids) ;
    }
}

/**************************************************************/
// Dependency handling
/**************************************************************/

void
VeriScope::Using(VeriScope *other_scope)
{
    if (!other_scope) return ;

    // Scope dependency is based on design unit dependency, so get
    // design unit's scope via Upper().
    other_scope = other_scope->ModuleScope() ;

    // This scope uses another scope. Update '_using'
    VeriScope *this_scope = ModuleScope() ;

    // Exit if we find that the two scopes are part of the same design unit
    if (this_scope == other_scope) return;

#if 0
    // VIPER #4523: Quickly check that the otherscope is not already using this scope:
    // If it is using, do not proceed, we would not like to introduce recursion:
    if (other_scope->_using && other_scope->_using->GetItem(this_scope)) return ;
#endif

    if (!this_scope->_using) this_scope->_using = new Set(POINTER_HASH) ;
    (void) this_scope->_using->Insert(other_scope) ;

    // Also update the 'usedby' field in 'other_scope' :
    if (!other_scope->_used_by) other_scope->_used_by = new Set(POINTER_HASH) ;
    (void) other_scope->_used_by->Insert(this_scope) ;
}

void
VeriScope::NoLongerUsing(const VeriScope *other_scope, unsigned delete_if_empty)
{
    if (!other_scope) return ;

    // Delete 'other_scope' from my '_using' list :
    if (_using) {
        (void) _using->Remove(other_scope) ;
        // VIPER #5115 : After removing element from _using, if its size becomes
        // 0, delete the _using. _using with size 0 can create problem in save/restore
        if (delete_if_empty && !_using->Size()) {
            delete _using ;
            _using = 0 ;
        }
    }

    // Also remove me from the other scope
    if (other_scope->_used_by) (void) other_scope->_used_by->Remove(this) ;
}

void
VeriScope::DependentScopes(Set &dependent) const
{
    if (!_using) return ;

    SetIter si ;
    VeriScope *using_scope ;
    FOREACH_SET_ITEM(_using, si, &using_scope) {
        if (!using_scope) continue ;
        // Take this scope into the Set:
        (void) dependent.Insert(using_scope) ;
        // And also get the dependent scopes of this scope:
        using_scope->DependentScopes(dependent) ;
    }
}

void
VeriScope::AddChildScope(const VeriScope *child)
{
    if (!child) return ;

    // 'this' must be set as 'upper' scope of the child:
    VERIFIC_ASSERT(child->_upper == this) ;

    // If the Set for children is not yet created, create it now.
    if (!_children) _children = new Set(POINTER_HASH) ;

    // Add this child scope into my list of children:
    (void) _children->Insert(child) ;
}

void
VeriScope::RemoveChildScope(VeriScope *child)
{
    if (!child) return ;

    // Remove this child scope from my list of children:
    if (_children) (void) _children->Remove(child) ;

    // Also reset the 'upper' scope of the child, which was 'this':
    child->_upper = 0 ;
}

// Make 'id' directly visible to 'this' scope. If 'id' is null, all identifiers declared
// within 'package_scope' are visible to 'this' scope.
void
VeriScope::AddImportItem(VeriScope *package_scope, const char *id, const VeriTreeNode *from)
{
    if (!package_scope) return ; // Nothing to do without pacakge scope

    // First process wildcard import item
    if (!id) {
        // First check if 'package_scope' is already imported
        MapItem *item = _imported_scopes ? _imported_scopes->GetItem(package_scope) : 0 ;
        Set *names = 0 ;
        if (item) { // It is already imported
            // Check whether it is already imported with wildcard
            names = (Set*)item->Value() ;
            // If 'package_scope' is imported previously by wildcard, no need to import again
            if (names && names->GetItem("*")) return ;
        }
        if (!_imported_scopes) _imported_scopes = new Map(POINTER_HASH) ;
        if (!names) {
            names = new Set(STRING_HASH) ; // Create new Set to store '*'
            // Import 'package_scope'
            (void) _imported_scopes->Insert(package_scope, names) ;
        }
        (void) names->Insert("*") ;

        Using(package_scope) ; // Register scope dependancy
        return ;
    }

    // Now process explicit import item
    // Explicit import shall be illegal if imported identifier is
    // declared in 'this' scope or explicitly imported from another package.
    VeriIdDef *exist = (_this_scope) ? (VeriIdDef *)_this_scope->GetValue(id) : 0 ;
    if (exist) {
        if (from) { // 'id' already declared in 'this' scope
            from->Error("%s is already declared", id) ;
            exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
        }
        return ;
    }

    VeriIdDef *id_def = package_scope->FindLocal(id) ;
    // VIPER #7232 : Find 'id' in exported items of 'package_scope' also
    if (!id_def) id_def = package_scope->FindExported(id) ;

    // Check if 'id' is explicitly declared by another package
    MapIter mi ;
    VeriScope *imported_scope ;
    Set *names ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &imported_scope, &names) {
        if (imported_scope == package_scope) continue ; // Ignore same scope
        if (!names) continue ;
        if (names->GetItem(id)) { // 'id' is in other imported scope
            // VIPER #7232 : It is error if imported id is different identifier
            // Because of the presence of export construct same 'id' can be imported from different packages
            VeriIdDef *imp_id = imported_scope->FindLocal(id) ;
            if (!imp_id) imp_id = imported_scope->FindExported(id) ;

            if (!imp_id || (imp_id != id_def)) {
                if (from) {
                    from->Error("%s is already declared", id) ;
                    exist = (imp_id) ? imp_id : id_def ;
                    if (exist) exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
                }
                return ;
            }
        }
    }

    // Check if 'id' is already imported explicitly, if so no need to import it again
    // Get the set of names if present for this scope
    names = (_imported_scopes) ? (Set*)_imported_scopes->GetValue(package_scope) : 0 ;
    if (names && names->GetItem(id)) return ; // 'id' is already explicitly imported

    // Make 'id' visible to 'this' scope
    if (!_imported_scopes) _imported_scopes = new Map(POINTER_HASH) ;
    //VeriIdDef *id_def = package_scope->FindLocal(id) ;
    // VIPER #7232 : Find 'id' in exported items of 'package_scope' also
    //if (!id_def) id_def = package_scope->FindExported(id) ;
    if (!names) {
        names = new Set(STRING_HASH) ; // Create set to store imported names
        (void) _imported_scopes->Insert(package_scope, names) ;
    }

    if (id_def) (void) names->Insert(id_def->Name()) ;

    Using(package_scope) ; // Register scope dependancy
}
// VIPER #3327 : Find argument specific name in unnamed child scopes. While parsing
// some scopes having no owner are created for generate case item/generate if etc.
// Identifiers declared within that scope are eventually declared in upper named
// scope, as elaboration will move those identifiers to module level during generate elaboration.
VeriIdDef *VeriScope::FindInUnnamedChildScopes(const char *name) const
{
    SetIter si ;
    VeriScope *child ;
    VeriIdDef *id = 0 ;
    VeriIdDef *owner ;
    const char *owner_name ;
    FOREACH_SET_ITEM(_children, si, &child) {
        owner = child->GetOwner() ;
        owner_name = (owner) ? owner->GetName() : 0 ;
        // Scope created for generate case item etc. can have implicitly declared
        // owner whose name is strating with space. Do not ignore such scopes
        if (owner && owner_name && (*owner_name != ' ')) continue ; // Ignore named scopes
        id = child->FindLocal(name) ;
        // Find in unnamed child scopes of 'child'
        if (!id) id = child->FindInUnnamedChildScopes(name) ;
        if (id) return id ; // Identifier is declared
    }
    return id ;
}

// Find the scope where argument specified identifier is declared starting
// from this scope :
VeriScope *VeriScope::GetDeclarationScopeOf(const VeriIdDef *id)
{
    if (!id) return 0 ;

    const VeriIdDef *found_id = 0 ;
    // 1. Check whether it is local to this scope
    // If extended scope exists, for inline constraint we should
    // first find object in extended scope. If not found, follow normal finding
    if (_extended_scope) {
        found_id = _extended_scope->FindLocal(id->Name()) ;
        if (found_id == id) return _extended_scope ;
        VeriScope *base_class_scope = _extended_scope->GetBaseClassScope() ;
        while (base_class_scope) {
            found_id = base_class_scope->FindLocal(id->Name()) ;
            if (found_id == id) return base_class_scope ;
            base_class_scope = base_class_scope->GetBaseClassScope() ;
        }
    }
    found_id = (_this_scope) ? (VeriIdDef*)_this_scope->GetValue(id->Name()) : 0 ;
    if (found_id == id) return this ;

    // 2. Find in explicitly imported items
    MapIter mi ;
    VeriScope *scope ;
    Set *names ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, &names) {
        if (!scope || !names) continue ;

        if (names->GetItem(id->Name())) {
            //return scope->GetDeclarationScopeOf(id) ;
            VeriScope *result = scope->GetDeclarationScopeOf(id) ;
            // Variable with same name may exist in this scope,
            // but need to explicitly check pointer values too.
            // See VIPER #5611 design SV_2009/Positive/26.3/test1.
            if (result) return result ;
        }
    }
    // 3. Find in base class scope
    if (_base_class_scope) {
        VeriScope *base_class_scope = _base_class_scope ;
        while (base_class_scope) {
            found_id = base_class_scope->FindLocal(id->Name()) ;
            if (found_id == id) return base_class_scope ;
            base_class_scope = base_class_scope->GetBaseClassScope() ;
        }
    }

    // 4. Find in upper scopes
    if (_upper) {
        VeriScope *decl_scope = _upper->GetDeclarationScopeOf(id) ;
        if (decl_scope) return decl_scope ;
    }
    return 0 ;
}
// If 'id' in explicitly imported in this scope, undeclare that identifier
// from this scope. So that it cannot be found by FindLocal API.
unsigned VeriScope::UndeclareExplicitImport(const VeriIdDef *id)
{
    if (!id) return 0 ;
    MapIter mi ;
    VeriScope *scope ;
    Set *names ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, &names) { // Iterate over explicitly imported scopes
        if (!scope || !names) continue ;

        if (names->GetItem(id->Name())) {
            return names->Remove(id->Name()) ;
        }
    }
    if (_upper) return _upper->UndeclareExplicitImport(id) ;
    return 0 ;
}

void VeriScope::SetGenerateScope()
{
    // Set this scope as generate-scope:
    _generate_scope = 1 ;

    // CHECKME: Set all the child scope to be generate-scope as well?
    //SetIter si ;
    //VeriScope *child ;
    //FOREACH_SET_ITEM(_children, si, &child) if (child) child->SetGenerateScope() ;
}

unsigned VeriScope::CanBeConsideredEmpty() const
{
    if (!_this_scope || !_this_scope->Size()) return 1 ;
    MapIter mi ;
    VeriIdDef *id ;
    FOREACH_MAP_ITEM(_this_scope,mi,0, &id) {
        if (!id || id->IsBlock()) continue ;
        return 0 ;
    }
    return 1 ;
}
// VIPER #6434 : Routines to hash VeriConstraint for structure elements

// Hash VeriStructUnion parse tree along with its element constraints
void VeriScope::AddElementConstraints(const VeriTreeNode *st_un_node, const Array *constraints)
{
    if (!st_un_node || !constraints) return ;
    if (!_struct_constraints) _struct_constraints = new Map(POINTER_HASH) ;
    (void) _struct_constraints->Insert(st_un_node, constraints) ;
}
Array * VeriScope::GetElementConstraints(const VeriTreeNode *node)
{
    if (!node) return 0 ;
    // First look at this scope
    Array *constraints = _struct_constraints ? (Array*)_struct_constraints->GetValue(node): 0 ;

    if (constraints) return constraints ;

    // Find in upper scope
    if (_upper) return _upper->GetElementConstraints(node) ;
    return 0 ; // not hashed yet
}
void VeriScope::DeleteStructConstraints()
{
    Array *constraints ;
    MapIter mi ;
    FOREACH_MAP_ITEM(_struct_constraints, mi, 0, &constraints) {
        unsigned i ;
        VeriConstraint *ele_constraint ;
        FOREACH_ARRAY_ITEM(constraints, i, ele_constraint) delete ele_constraint ;
        delete constraints ;
    }
    delete _struct_constraints ;
    _struct_constraints = 0 ;
    // Reset child scopes too
    SetIter si ;
    VeriScope *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (child) child->DeleteStructConstraints() ;
    }
}
void VeriScope::SetStructConstraints(Map *st_constraints)
{
    _struct_constraints = st_constraints ;
}

// Call SetStructConstraints(0) for this scope and all children scopes
void VeriScope::SetStructConstraintsNull()
{
    SetStructConstraints(0) ;
    SetIter si ;
    VeriScope *child ;
    FOREACH_SET_ITEM(_children, si, &child) {
        if (child) child->SetStructConstraintsNull() ;
    }
}

VeriIdDef *
VeriScope::FindImplicitlyImportedErrorCond(const char *name, const VeriTreeNode *from)
{
    if (!name) return 0 ;
    (void) name ;
    (void) from ;

    // Find it local
    VeriIdDef *id = FindLocal(name) ;
    if (id) return id ;

    // VIPER #4954: Find in base class scope:
    //if (_base_class_scope) id = _base_class_scope->FindInBaseClass(name) ;
    //if (id) return id ;
    id = FindInBaseClass(name) ;
    if (id) return id ;
    // MB/RD 7/2006:
    // VIPER #3188 :FindLocal routine already searched locally declared identifiers and explicitly
    // imported identifiers.

    // 'name' is not explicitly imported, find in wildcard imported scopes
    // If two wildcard imported scopes contain 'name', it is an error
    MapIter mi ;
    VeriScope *scope ;
    Set *names ;
    VeriIdDef *decl_id ;
    FOREACH_MAP_ITEM(_imported_scopes, mi, &scope, &names) {
        if (!scope || !names) continue ;

        if (!names->GetItem("*")) continue ; // This scope is not wildcard imported, continue
        // Find the declared identfiers in the imported scope :
        decl_id = scope->FindLocal(name) ;
        if (!decl_id) decl_id = scope->FindExported(name) ; // VIPER #7232 : Find exported items also
        if (!decl_id) continue ; // no such identifier included

        if (id && id!=decl_id) {
            // Same 'name' is imported from multiple packages
            // FIXME : In wildcard import, each identifier is imported only when
            // it is referenced in the importing scope. If design has two import clauses
            // P :: *
            // Q :: mem
            // and package 'P' also contains 'mem', it is an error only if 'mem' is
            // referenced before second import declaration (Q::mem). Here we do not
            // implement this scenario. Currently we will give error for the above case
            // irrespective of the fact that 'mem' is used after first import decl or not
            // Issue error.
            if (!from) from = decl_id ; // use 'from' for linefile, but in absence of that, use decl_id's linefile.
            // Issue 3135 : better message :
            from->Error("%s is visible via multiple package imports", id->Name()) ;
            // Mention both identifiers :
            decl_id->Info("%s is declared here",id->Name()) ;
            id->Info("%s is declared here",id->Name()) ;
            break ; // stop looking for more.
        }
        // Set this id as the one included :
        id = decl_id ;
    }

    // Otherwise, find in upper level :
    if (_upper) {
        id = _upper->FindImplicitlyImportedErrorCond(name,from) ;
        if (id) return id ;
    }

    // On our way back, if id was not found,
    // check if its a local reference into the local module.
    if (_owner && _owner->IsModule() && Strings::compare(_owner->Name(), name)) {
        return _owner ;
    }

    // Here, cannot find it.
    return 0 ;
}
void VeriScope::AddExportItem(const VeriScope *package_scope, const char *id, const VeriTreeNode *from)
{
    // First process wildcard export item (p::* and *::* (both 'package_scope' and 'id' null))
    Map *names = 0 ;
    if (!id) {
        // First check if 'package_scope' is already imported
        MapItem *item = _exported_scopes ? _exported_scopes->GetItem(package_scope) : 0 ;
        if (item) { // It is already exported
            // Check whether it is already exported with wildcard
            names = (Map*)item->Value() ;
            // If 'package_scope' is exported previously by wildcard, no need to export again
            if (names && names->GetItem("*")) return ;
        }
        if (!_exported_scopes) _exported_scopes = new Map(POINTER_HASH) ;
        if (!names) {
            names = new Map(STRING_HASH) ; // Create new Set to store '*'
            // Export 'package_scope'
            (void) _exported_scopes->Insert(package_scope, names) ;
        }
        // Store linefile of export statement to produce error later with proper line number
        (void) names->Insert("*", (void*)(from ? from->Linefile(): 0)) ;

        return ;
    }
    if (!package_scope) return ;

    // Now process explicit export item
    // Check if 'id' is already exported explicitly, if so no need to export it again
    // Get the set of names if present for this scope
    names = (_exported_scopes) ? (Map*)_exported_scopes->GetValue(package_scope) : 0 ;
    if (names && names->GetItem(id)) return ; // 'id' is already explicitly exported

    // Make 'id' visible to 'this' scope
    if (!_exported_scopes) _exported_scopes = new Map(POINTER_HASH) ;
    VeriIdDef *id_def = package_scope->FindLocal(id) ;
    // VIPER #7232 : Find 'id' in exported items of 'package_scope' also
    if (!id_def) id_def = package_scope->FindExported(id) ;
    if (!names) {
        names = new Map(STRING_HASH) ; // Create map to store exported names
        (void) _exported_scopes->Insert(package_scope, names) ;
    }
    // Store linefile of export statement to produce error later with proper line number
    if (id_def) (void) names->Insert(id_def->Name(), (void*)(from ? from->Linefile(): 0)) ;
}
void VeriScope::CheckExportedItems()
{
    if (!_exported_scopes) return ; // No exported items to check

    // Each exported items should be imported either explicitly or by wildcard import
    MapIter mii ;
    Map *names ; Set *imported_names ;
    MapIter mi ;
    VeriScope *pkg_scope, *imported_pkg_scope ;
    char *name, *imported_name ;
    SetIter si ;
    unsigned all_exported = 0 ;
    linefile_type lf ;
    linefile_type all_export_lf = 0 ;
    FOREACH_MAP_ITEM(_exported_scopes, mi, &pkg_scope, &names) {
        if (!pkg_scope && names) { // export *::*
            all_exported = 1 ;
            all_export_lf = (linefile_type)names->GetValue("*") ;
            continue ;
        }
        if (!pkg_scope) continue ;
        VeriIdDef *pkg_id= pkg_scope->GetOwner() ;
        // Check whether any item of exported package is actually imported
        imported_names = _imported_scopes ? (Set*)_imported_scopes->GetValue(pkg_scope): 0 ;
        unsigned export_all_of_pkg = 0 ;
        linefile_type all_export_pkg_lf = 0 ;
        FOREACH_MAP_ITEM(names, mii, &name, &lf) {
            if (!name) continue ;
            if (Strings::compare(name, "*")) { // Need to export all imported items
                export_all_of_pkg = 1 ;
                all_export_pkg_lf = lf ;
            } else { // Export only 'name'
                // Check whether 'name' is imported
                if (imported_names && imported_names->Get(name)) {
                    // Imported, so export is legal
                } else {
                    // It is an error if 'name' is locally declared identifier
                    VeriIdDef *exist = _this_scope ? (VeriIdDef*)_this_scope->GetValue(name) : 0 ;
                    if (exist) { // 'name' already in scope
                        VeriTreeNode from_node(lf) ;
                        from_node.Error("package export %s::%s failed, name locally declared", pkg_id ? pkg_id->Name(): "", name) ;
                    } else if (imported_names && imported_names->GetItem("*")) {
                        // Check whether wildcard import is done, if so export will be considered
                        // a reference of that object
                    } else {
                        // 'name' can be imported from other package
                        // First check whether 'name' is declared in exported package
                        VeriIdDef *export_id = pkg_scope->FindLocal(name) ;
                        if (!export_id) export_id = pkg_scope->FindExported(name) ;
                        // If 'export_id' exists, check whether it is imported
                        // from other package
                        VeriIdDef *imported_id = 0 ;
                        MapIter iter ;
                        VeriScope *im_scope ;
                        Set *im_names ;
                        FOREACH_MAP_ITEM(_imported_scopes, iter, &im_scope, &im_names) {
                            if (!im_scope || !im_names) continue ;
                            // Check if 'name' is explicitly/wildcard imported :
                            imported_id = im_scope->FindLocal(name) ;
                            if (!imported_id) imported_id = im_scope->FindExported(name) ;
                            if (export_id && (export_id == imported_id)) {
                                break ;
                            } else {
                                imported_id = 0 ;
                            }
                        }

                        if (!imported_id || !export_id) {
                            VeriTreeNode from_node(lf) ;
                            // Not imported, error
                            from_node.Error("package export %s::%s failed, name not found", pkg_id ? pkg_id->Name(): "", name) ;
                        }
                    }
                }
            }
        }
        if (export_all_of_pkg) {
            FOREACH_SET_ITEM(imported_names, si, &imported_name) {
                if (!imported_name || Strings::compare(imported_name, "*")) continue ; // Ignore '*'
                if (names) (void) names->Insert(imported_name, (void*)all_export_pkg_lf) ;
            }
        }
    }
    if (all_exported) {
        if (!_imported_scopes) { // No import statement for this export, ERROR
            // FIXME : Simulators are not producing error now
        }
        // Exports all imported declarations from all imported packages
        FOREACH_MAP_ITEM(_imported_scopes, mii, &imported_pkg_scope, &imported_names) {
            if (!imported_pkg_scope || !imported_names) continue ;
            FOREACH_SET_ITEM(imported_names, si, &name) {
                if (!name || Strings::compare(name, "*")) continue ; // Need only actaully imported items
                // Check wheather imported item is already exported
                names = (Map*)_exported_scopes->GetValue(imported_pkg_scope) ;
                if (names && names->GetValue(name)) continue ;
                // It is an error if 'name' is locally declared identifier
                VeriIdDef *exist = _this_scope ? (VeriIdDef*)_this_scope->GetValue(name) : 0 ;
                if (exist) { // 'name' already in scope
                    VeriTreeNode tmp(all_export_lf) ;
                    tmp.Error("%s is already declared", name) ;
                    exist->Info("previous declaration of %s is from here", exist->Name()) ; // VIPER #7773
                    continue ;
                }
                if (!names) {
                    names = new Map(STRING_HASH) ; // Create new Set
                    (void) _exported_scopes->Insert(imported_pkg_scope, names) ;
                }
                (void) names->Insert(name, (void*)all_export_lf) ;
            }
        }
    }
}
// Find identifier in exported items
VeriIdDef *
VeriScope::FindExported(const char *name) const
{
    if (!_exported_scopes) return 0 ;

    MapIter mi, mii ;
    VeriScope *pkg_scope ;
    Map *names ;
    FOREACH_MAP_ITEM(_exported_scopes, mi, &pkg_scope, &names) { // Iterate over exported items
        if (!pkg_scope || !names) continue ;

        if (names->GetItem(name)) { // 'name' is exported
            VeriIdDef *id = pkg_scope->FindLocal(name) ;
            // Find in exported items of 'pkg_scope'
            if (!id) id = pkg_scope->FindExported(name) ;
            Map *id_map = pkg_scope->GetThisScope() ;
            if (id_map && (id_map->GetValue(name) == id)) VeriNode::IncreaseReferenceCount(id, 1) ;
            if (id) return id ;
        }
    }
    return 0 ;
}

