/*
 *
 * [ File Version : 1.47 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "veri_file.h" // Compile-time switches are defined within here
#include "Message.h"
#include "Map.h"
#include "Array.h"
#include "Strings.h"
#include "VeriScope.h"
#include "VeriModule.h"
#include "VeriLibrary.h"
#include "VeriConstVal.h"
#include "VeriElab_Stat.h"
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
// Viper 2056 : dual language elaboration support
#include "../vhdl/VhdlUnits.h"
#include "../vhdl/VhdlIdDef.h"
#include "../vhdl/vhdl_file.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Statically elaborate an analyzed module, store in library 'work_lib'
unsigned veri_file::ElaborateStatic(const char *module_name, const char *work_lib, const Map *parameters)
{
    //if (!module_name || Message::ErrorCount()) return 0 ;
    if (!module_name) return 0 ;

    if (!work_lib) work_lib = "work" ;
    // Find/Set the work library where design units will be compiled into.
    _work_library = GetLibrary(work_lib, 1) ;

    // Just in case
    Message::ClearErrorCount() ;

    // Find the module by name in the parsed modules
    VeriModule *module = _work_library->GetModule(module_name, 1) ;
    if (!module) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("module ",module_name, " in library ", _work_library->GetName(), " is not yet analyzed") ;
        VeriTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Comment("module %s in library %s is not yet analyzed", module_name, _work_library->GetName()) ;

        return 0 ;
    }

    // Set style of elaboration : static
    VeriNode::SetStaticElab() ;

    // Set up the actual parameters
    Map *actual_params = module->CreateActualParameters(parameters) ;

    unsigned ret_status = 1 ;
    if (!module->StaticElaborate(actual_params)) ret_status = 0 ;

    // Cleanup the actual parameters Map
    // The following block of code is the inline implementation of VeriNode::DeleteDefParams.
    // This is needed since VeriNode::DeleteDefParams is currently only supported under rtl elaboration.
    MapIter mi ;
    //VeriConst *actual ;
    VeriExpression *actual ;
    char *name ;
    FOREACH_MAP_ITEM(actual_params, mi, &name, &actual) {
        Strings::free(name) ;
        delete actual ;
    }
    delete actual_params ;

    // Reset static elaboration style
    VeriNode::ResetStaticElab() ;

    return ret_status ;
}

// Statically elaborate all modules, store in library 'work_lib'
unsigned veri_file::ElaborateAllStatic(const char *work_lib)
{
    //if (Message::ErrorCount()) return 0 ;

    if (!work_lib) work_lib = "work" ;
    // Find/Set the work library where design units will be compiled into.
    _work_library = GetLibrary(work_lib, 1) ;

    Message::ClearErrorCount() ;

    // Set style of elaboration : static
    VeriNode::SetStaticElab() ;

    // Run full post-parsing analysis :
    //AnalyzeFull(work_lib) ;

    // Bail out if there was an error in analysis :
    //if (Message::ErrorCount()) return 0 ;

    // Elaborate this work library :
    unsigned ret_status = 1 ;
    if (!_work_library->StaticElaborate()) ret_status = 0 ;

    // Reset static elaboration style
    VeriNode::ResetStaticElab() ;

    return ret_status ;
}

// veri_file::ElaborateMultipleTopStatic uses VeriStaticElaborator class which is locally
// decalred in VeriElab_Stat.cpp. So, this routine can't be defined here in this file.
// DD: 11/24/2010: Now, VeriStaticElaborator is defined in VeriElab_Stat.h file, so moved
// veri_file::ElaborateMultipleTopStatic() in this file here.
// VIPER #3099: Statically elaborate an array of top modules and then return an allocated
// array of statically elaborated modules.
// VIPER #6545 : Add char*param_name->char*param_value assoc to apply to all top
// level modules. 'param_name' can be simple name like 'p1' or can be parameter
// name of one particular module like 'module_name/param_name'
Array *veri_file::ElaborateMultipleTopStatic(const Array *top_mods, const Map *parameters)
{
    //if (!top_mods || Message::ErrorCount()) return 0 ; // Don't continue under error
    if (!top_mods) return 0 ; // Don't continue under error
    Message::ClearErrorCount() ;

    // Find the last valid module within the given top level modules:
    VeriModule *valid_top_mod = 0 ;
    unsigned i ;
    VeriModule *top_mod ;
    FOREACH_ARRAY_ITEM(top_mods, i, top_mod) {
        if (!top_mod) continue ;
        // Don't treat nested/out-of-library modules as valid top, error out for that?
        if (top_mod->GetLibrary()) valid_top_mod = top_mod ;
    }

    if (!valid_top_mod) return 0 ; // No valid top level module found, can't continue
    // Convert char*name->char*value map to char*name->VeriConst*value map :
    Map *actual_params = (parameters) ? new Map(STRING_HASH) : 0 ;

    MapIter mi ;
    const char *name ;
    char *value ;
    VeriExpression *actual ;
    FOREACH_MAP_ITEM(parameters, mi, &name, &value) {
        if (!value || !name) continue ;
        // VIPER #6594 (issue 131): Use better string to expression parsing routine:
        //actual = VeriTreeNode::StringToExpression(value) ;
        actual = veri_file::AnalyzeExpr(value, veri_file::GetAnalysisMode()) ;
        if (actual_params) (void) actual_params->Insert(Strings::save(name), actual) ;
    }

    // VIPER #3772: Set the global work library to the parent library of the last
    // given valid module so that we don't have it set to a NULL value in which case
    // we may crash later. Since the modules may be in different library and we have
    // to elaborate them in one shot (static elaborator cannot elaborate modules one
    // by one), so we can't set work library per module while elaborating from here.
    // If the files containing the modules were 'analyzed' and elaborated then it was
    // the last module whose library would have been set as work library anyway. Also
    // all veri_file routines to elaborate modules set work library, so set it here too:
    veri_file::SetWorkLib(valid_top_mod->GetLibrary()) ;

    // Reset the veri_file::_current_user_library
    veri_file::SetCurrentUserLibrary(0) ;

    // Set style of elaboration : static
    VeriNode::SetStaticElab() ;

    // Now elaborate the given modules assuming that they are top level modules:
    VeriStaticElaborator elabObj(top_mods, actual_params) ;
    elabObj.ElaborateVerilogTopLevelModules() ;

    Array *created_top_modules = new Array() ;
    // Insert the top level modules yielded/created out of this elaboration:
    elabObj.GetTopLevelModules(created_top_modules) ;

    // Cleanup the actual parameters Map
    char *pname = 0 ;
    FOREACH_MAP_ITEM(actual_params, mi, &pname, &actual) {
        Strings::free(pname) ;
        delete actual ;
    }
    delete actual_params ;

    // Reset static elaboration style
    VeriNode::ResetStaticElab() ;

    return created_top_modules ;
}

void veri_file::AddTopModule(const VeriModule *module)
{
    if (!module) return ; // Nothing to add
    if (!_top_mods) _top_mods = new Array() ; // Create array if not exists
    _top_mods->InsertLast(module) ;
}
VeriModule *veri_file::GetTopModule(const char *name)
{
    if (!name || !_top_mods) return 0 ; // No name to search
    // Search 'name' in top module list. List may contain copied top level modules
    // and in that case we will search original top levels also.
    unsigned i ;
    VeriModule *mod ;
    VeriModule *ret_mod = 0 ;
    FOREACH_ARRAY_ITEM(_top_mods, i, mod) {
        if (!mod) continue ;
        VeriModule *matched_module = 0 ;
        if (Strings::compare(mod->Name(), name)) { // Matched
            matched_module = mod ;
        }
        // Check original module name if 'mod' is copied one
        const char *orig_name = mod->GetOriginalModuleName() ;
        if (orig_name && !matched_module && Strings::compare(orig_name, name)) {
            matched_module = mod->GetOriginalModule() ;
        }
        if (!matched_module) continue ;
        // Matched module found
        // Check if multiple module matches
        if (ret_mod) {
            VeriLibrary *lib = matched_module->GetLibrary() ;
            // VIPER #7969: No need to do anything here. This is the situation when multiple modules with same name from different library
            // has been specified to be static elaborated and the design contains a hier-ref to one of those top level modules.
            matched_module->Warning("top-level design unit %s specified more than once, ignoring %s of library %s", matched_module->Name(), matched_module->Name(), lib ? lib->GetName() : "") ;
            return ret_mod ;
        }
        ret_mod = matched_module ;
    }
    return ret_mod ; // Return matching top level
}
void veri_file::ClearTopModuleList()
{
    delete _top_mods ; // Delete top module list
    _top_mods = 0 ;
}
// VIPER #7727 : Elaborate multiple tops from verilog and vhdl
unsigned veri_file::ElaborateMixedHdlMultipleTopsStatic(const Array *top_mods, const Array *top_units, const Map *parameters, Array *elaborated_mods, Array *elaborated_units)
{
    if (!top_mods && !top_units) return 0 ; // Return, if no top level is specified
    Message::ClearErrorCount() ;

    // Find the last valid module within the given top level modules:
    VeriModule *valid_top_mod = 0 ;
    unsigned i ;
    VeriModule *top_mod ;
    FOREACH_ARRAY_ITEM(top_mods, i, top_mod) {
        if (!top_mod) continue ;
        // Don't treat nested/out-of-library modules as valid top, error out for that?
        if (top_mod->GetLibrary()) valid_top_mod = top_mod ;
    }

    //if (top_mods && !valid_top_mod) return 0 ; // No valid top level module found, can't continue

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    Array modified_top_units(top_units ? top_units->Size(): 2) ;
    // Check the validity of top vhdl units
    VhdlPrimaryUnit *unit ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(top_units, i, unit) {
        if (!unit /*|| unit->IsElaborated()*/) continue ;

        id = unit->Id() ;
        // Don't elaborate packages (we don't need to)
        if (!id->IsEntity() && !id->IsConfiguration()) continue ;

        // Don't elaborate entities without an architecture
        //if (id->IsEntity() && unit->NumOfSecondaryUnits()==0) continue ;

        //if (!parameters && unit->HasUninitializedGenerics()) {
         //   unit->Warning("design unit %s contains uninitialized generic(s)", unit->Name()) ;
          //  unit->Warning("cannot be considered as top level, ignoring") ;
           // continue ;
        //}
        if (unit->HasUnconstrainedUninitializedPorts()) {
            unit->Warning("design unit %s contains unconstrained port(s)", unit->Name()) ;
            unit->Warning("cannot be considered as top level, ignoring") ;
            continue ;
        }
        modified_top_units.Insert(unit) ;
    }
    if (modified_top_units.Size()) {
        VhdlNode::ClearNamedPath() ;
        VhdlNode::ClearStructuralStack() ;
        VhdlNode::SetStaticElab() ;
        vhdl_file::_top_unit_name = 0 ;
    }
#endif

    // VIPER #3772: Set the global work library to the parent library of the last
    // given valid module so that we don't have it set to a NULL value in which case
    // we may crash later. Since the modules may be in different library and we have
    // to elaborate them in one shot (static elaborator cannot elaborate modules one
    // by one), so we can't set work library per module while elaborating from here.
    // If the files containing the modules were 'analyzed' and elaborated then it was
    // the last module whose library would have been set as work library anyway. Also
    // all veri_file routines to elaborate modules set work library, so set it here too:
    if (valid_top_mod) veri_file::SetWorkLib(valid_top_mod->GetLibrary()) ;

    // Reset the veri_file::_current_user_library
    veri_file::SetCurrentUserLibrary(0) ;

    // Set style of elaboration : static
    VeriNode::SetStaticElab() ;
   
    // From parameter/generic overwrite list, create parameter overwrite list
    // and generic overwrite list
    Map verilog_parameters(STRING_HASH) ; // Parameter overwrite list
    Map vhdl_generics(STRING_HASH) ; // Generic overwrite list
    if (parameters && parameters->Size()) {
        VeriStaticElaborator::CreateVerilogAndVhdlParamsGenerics(parameters, &verilog_parameters, &vhdl_generics) ;
    }

    // Create param-name vs VeriExpression* to overwrite parameter values in
    // verilog top level modules
    Map *actual_parameters = 0 ;
    MapIter mi ;
    char *name, *value ;
    VeriExpression *actual ;
    if (top_mods && top_mods->Size()) { // Verilog top levels exist
        actual_parameters = (verilog_parameters.Size()) ? new Map(STRING_HASH): 0 ;
        FOREACH_MAP_ITEM(&verilog_parameters, mi, &name, &value) {
            if (!value) continue ;
            actual = veri_file::AnalyzeExpr(value, veri_file::GetAnalysisMode()) ;
            if (!actual) {
                VeriTreeNode tmp_print_node ;
                tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
                tmp_print_node.Error("unknown literal value %s for parameter %s ignored", value, name) ;
                continue ;
            }
            if (actual_parameters) (void) actual_parameters->Insert(Strings::save(name), actual) ;
        }
    }
    // Create static elaborator object to initialize verilog top
    // level modules and their parameter override
    VeriStaticElaborator elabObj(top_mods, actual_parameters) ;

    // Elaborate verilog top units before vhdl to add
    // bind instances with target vhdl units to proper vhdl units first
    // Now elaborate the verilog top level modules
    elabObj.ElaborateVerilogTopLevelModules() ;
#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    VhdlNode::SetConstNets() ;
    // Elaborate the top level vhdl units.  Pseudo tree nodes will be created
    // for elaborated vhdl and verilog(instantiated from vhdl) units.
    FOREACH_ARRAY_ITEM(&modified_top_units, i, unit) {
        if (!unit) continue ;
        // Create generics validating for 'unit'
        Array *generics = (vhdl_generics.Size()) ? unit->CreateActualGenerics(&vhdl_generics, unit->Linefile(), 0, 1/*ignore if not present*/): 0 ;
        (void) unit->StaticElaborate(0, generics, 0, 0, 0) ; // Elaborate top level unit
        // Cleanup generics
        VhdlTreeNode *node ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(generics, j, node) delete node ;
        delete generics ;
    }
#endif // VERILOG_DUAL_LANGUAGE_ELABORATION

    // Now elaborate the verilog top level modules
    //elabObj.ElaborateVerilogTopLevelModules() ;

    // Insert the top level modules yielded/created out of this elaboration:
    if (elaborated_mods) elabObj.GetTopLevelModules(elaborated_mods) ;

    // Insert the top level vhdl units created out of this elaboration
    if (elaborated_units) elabObj.GetTopLevelUnits(elaborated_units) ;

    // Cleanup the actual parameters Map
    FOREACH_MAP_ITEM(actual_parameters, mi, &name, &actual) {
        Strings::free(name) ;
        delete actual ;
    }
    delete actual_parameters ;

    // Reset static elaboration style
    VeriNode::ResetStaticElab() ;

    FOREACH_MAP_ITEM(&verilog_parameters, mi, &name, &value) Strings::free(value) ;
    FOREACH_MAP_ITEM(&vhdl_generics, mi, &name, &value) Strings::free(value) ;

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    if (modified_top_units.Size()) {
        VhdlTreeNode::CleanConfigNumMap() ;
        VhdlTreeNode::CleanUnitNumMap() ;
    }
#endif
    return (Message::ErrorCount()) ? 0 : 1 ;
}

