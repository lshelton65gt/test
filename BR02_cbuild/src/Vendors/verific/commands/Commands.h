/*
 *
 * [ File Version : 1.81 - 2014/02/03 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_COMMANDS_H_
#define _VERIFIC_COMMANDS_H_

/* #include "tcl.h"   // For Tcl_Interp ; needed to let 'Process()' return a Tcl List*/
struct Tcl_Interp ; // we work only with pointers to Tcl_Interp, so no need to include tcl.h here

#include "VerificSystem.h"
#include "LineFile.h"
#include "Map.h"
#include "Array.h"

/*
    Routines to support command definition :
    Definition of arguments and options, and their type.
    and automatic parsing of a command line
    Also added file-format routines for commands that deal
    with file names.
*/

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Argument ;
class Command ;
class Commands ;
class Array ;
class DesignObj ;
class SaveRestore ;

typedef enum _format_enum {
    FORM_EDIF,
    FORM_VERILOG,
    FORM_SYS_VERILOG,
    FORM_VHDL,
    FORM_SYNLIB,
    FORM_BINARY_DUMP,
    FORM_SDF,
    FORM_BLIF,
    FORM_UPF,
    FORM_UNKNOWN
} format_enum ;

#define FOREACH_COMMAND(CMDS,I,COM)     if (CMDS) FOREACH_MAP_ITEM(&((CMDS)->_commands),I,0,&COM)

/* -------------------------------------------------------------- */

class VFC_DLL_PORT Commands
{
public:
    Commands() ;
    virtual ~Commands() ;

private:
    // Prevent compiler from defining the following
    Commands(const Commands &) ;            // Purposely leave unimplemented
    Commands& operator=(const Commands &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    Command *        Get(const char *name) const ; // Get a command by name
    void             Add(const Command *command) ;
    void             Remove(const Command *command) ;

public: // for iterator
    Map     _commands ; // name->command mapping
} ; // class Commands

/* -------------------------------------------------------------- */

class VFC_DLL_PORT Command
{
public:
    explicit Command(const char *name, const char *help=0) ;
    virtual ~Command() ;

protected:
    Command(const Command &orig) ;

private:
    // Prevent compiler from defining the following
    Command() ;                           // Purposely leave unimplemented
    Command& operator=(const Command &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Parse command line and set option/arg values
    virtual unsigned        ParseArgs(int argc, const char * const argv[]) const ;

    // Execute the command
    virtual unsigned        Process(Tcl_Interp * /*interp*/)   { return 0 ; }

    // Accessor methods
    const char *            Name() const                       { return (_name) ? _name : "" ; }
    const char *            Help() const                       { return _help ; }
    const Map *             Options() const                    { return &_options ; }
    void                    Usage() const ; // Print usage

    // File format routines
    // Derive format from file (extension) name
    format_enum             FormatFromFileName(const char *file_name) const ;

    // Go back-and-forth between string and format enum
    format_enum             Format(const char *text) const ;
    char *                  Format(format_enum format) const ;

    // Create/remove options
    void                    Add(Argument *arg) ;
    void                    Remove(Argument *arg) ;

    // Check if option is set (i.e. already parsed)
    unsigned                IsSet(const char *option_name) const ;

    // Get option's value (you will get default value if option not set)
    unsigned                BoolVal(const char *option_name) const ;
    int                     IntVal(const char *option_name) const ;
    const char *            StringVal(const char *option_name) const ;
    double                  DoubleVal(const char *option_name) const ;
    Array *                 ArrayVal(const char *option_name) const ;

    // Return object by name
    Argument *              Get(const char *name) const ;
    // Intelligent search for option
    Argument *              FindOption(const char *string) const ;

    // Message handling support : Message id table, output formatted, varargs message functions
    static const Map *      GetMessageMap() { (void) GetMessageId("") ; return _msgs ; } // Return the entire map : (char*)formatted_string -> (char*)message_id
    static const char *     GetMessageId(const char *msg) ;
    static void             ResetMessageMap() ; // Reset message table
    virtual void            Comment(const char *format, ...) const ;
    virtual void            Error(const char *format, ...) const ;
    virtual void            Warning(const char *format, ...) const ;
    virtual void            Info(const char *format, ...) const ;

    // Linefile support
    linefile_type           GetLineFile() const                 { return _linefile ; }
    void                    SetLineFile(linefile_type lf)       { _linefile = lf ; }
    // This routine returns the current linefile information of the TCL command
    // being executed. Note that TCL always seem to return an absolute file name.
    // So, user may choose to pass a relative file path in optional 'file' argument.
    static linefile_type    GetTclLineFile(Tcl_Interp *interp, const char *file=0) ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Tcl linefile with column support (line lookup files)
    static void             ResetLookupFile(const char *file) ;
    static void             ResetLookupFiles() ;
#endif

protected:
    // For derived class use only
    void                    ClearParseFlags() const ;
    void                    CreateHandle(DesignObj *p, char type, char handle[]) const ;
    char                    HandleGetType(const char *handle) const ;
    DesignObj *             HandleGetObject(const char *handle) const ;

private:
    char            *_name ;       // Command name
    char            *_help ;       // One-line help description
    Map              _options ;    // name->argument mapping

protected: // make directly available to derived classes
    linefile_type    _linefile ;   // Source line/file info stored here
    static Map      *_msgs ;       // Static table to store formatted message->messageID
} ; // class Command

/* -------------------------------------------------------------- */

class VFC_DLL_PORT Argument
{
protected: // Abstract class
    friend class Command ;
    Argument(const char *name, const char *help, unsigned optional, int group, unsigned multiple_args) ;

public:
    virtual ~Argument() ;

protected: // Abstract class
    Argument(const Argument &orig) ;

private:
    virtual Argument * Copy() const = 0 ;
    // Prevent compiler from defining the following
    Argument() ;                            // Purposely leave unimplemented
    Argument& operator=(const Argument &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Accessor methods
    const Command *      Owner() const                  { return _owner ; }
    const char *         Name() const                   { return (_name) ? _name : "" ; }
    const char *         Help() const                   { return _help ; }
    unsigned             Overlap() const                { return _overlap ; }
    int                  Group() const                  { return _group ; }
    unsigned             Optional() const               { return _optional ; }
    unsigned             MultipleArgs() const           { return _multiple_args ; }

    void                 Usage(unsigned ident_offset=0) const ;

    // Type support
    typedef enum _ArgType { BOOL, INT, DOUBLE, STRING, ARRAY, ARRAY_OF_ARRAY, ARRAY_WITH_OPTION, MAP, MAP_OF_ARRAY } ArgType ;
    virtual ArgType      Type() const = 0 ;
    virtual unsigned     IsBool() const                 { return 0 ; }
    virtual unsigned     IsInt() const                  { return 0 ; }
    virtual unsigned     IsDouble() const               { return 0 ; }
    virtual unsigned     IsString() const               { return 0 ; }
    virtual unsigned     IsArray() const                { return 0 ; }
    unsigned             IsOption() const               { return (_name) ? 1 : 0 ; }

    // Derived classes must override one of the following SetValues :
    virtual unsigned     SetValue(const char * /*string*/)                              { VERIFIC_ASSERT(0); return 0 ; }

    // Virtual funcs. Derived classes have implementation
    // If these asserts hit, it means that the argument was NOT of that requested type.
    virtual unsigned     BoolVal() const                { VERIFIC_ASSERT(0); return 0 ; }
    virtual int          IntVal() const                 { VERIFIC_ASSERT(0); return 0 ; }
    virtual const char * StringVal() const              { VERIFIC_ASSERT(0); return 0 ; }
    virtual double       DoubleVal() const              { VERIFIC_ASSERT(0); return 0.0 ; }
    virtual Array *      ArrayVal() const               { VERIFIC_ASSERT(0); return 0 ; }

    // Command line parsing support methods
    unsigned             IsSet() const                  { return _set ; }
    unsigned             Set() ;
    void                 UnSet()                        { _set = 0 ; }

protected:
    Command    *_owner ;            // Command 'this' is an argument of.
    char       *_name ;             // Name of the option. NIL for command arguments.
    char       *_help ;             // Name of argument of the option (if any).
    unsigned    _overlap ;          // Number of characters overlap between other options.
    int         _group ;            // Cluster number for mutual exclusive options :
                                    //  - Positive groups are mutual exclusive.
                                    //  - Negative groups are NOT mutual exclusive.

    unsigned    _optional:1 ;       // Option is optional or required
    unsigned    _multiple_args:1 ;  // Option expects multiple arguments

     // Dynamic info : changes after each command line parsing
    unsigned    _set ;              // Flag if argument is set in last ParseArgs
} ; // class Argument

/* -------------------------------------------------------------- */

class VFC_DLL_PORT BoolArg : public Argument
{
public:
    BoolArg(const char *name, const char *help, unsigned optional=1, int group=0, unsigned default_val=0) ;
    virtual ~BoolArg() ;

protected:
    BoolArg(const BoolArg &orig) ;

private:
    Argument * Copy() const                 { return new BoolArg(*this) ; }
    // Prevent compiler from defining the following
    BoolArg() ;                           // Purposely leave unimplemented
    BoolArg& operator=(const BoolArg &) ; // Purposely leave unimplemented

public:
    virtual ArgType     Type() const        { return BOOL ; }
    virtual unsigned    IsBool() const      { return 1 ; }
    virtual unsigned    BoolVal() const ;
    virtual unsigned    SetValue(const char *string) ;

private :
    unsigned    _default_val ;
    unsigned    _value ;
} ; // class BoolArg

/* -------------------------------------------------------------- */

class VFC_DLL_PORT StringArg : public Argument
{
public:
    StringArg(const char *name, const char *help, unsigned optional=1, int group=0, const char *default_val=0) ;
    virtual ~StringArg() ;

protected:
    StringArg(const StringArg &orig) ;

private:
    Argument * Copy() const                 { return new StringArg(*this) ; }
    // Prevent compiler from defining the following
    StringArg() ;                             // Purposely leave unimplemented
    StringArg& operator=(const StringArg &) ; // Purposely leave unimplemented

public:
    virtual ArgType     Type() const        { return STRING ; }
    virtual unsigned    IsString() const    { return 1 ; }
    virtual const char *StringVal() const ;
    virtual unsigned    SetValue(const char *string) ;

private :
    char *_default_val ;  // owned
    char *_value ;        // owned
} ; // class StringArg

/* -------------------------------------------------------------- */

class VFC_DLL_PORT IntArg : public Argument
{
public:
    IntArg(const char *name, const char *help, unsigned optional=1, int group=0, int default_val=0) ;
    virtual ~IntArg() ;

protected:
    IntArg(const IntArg &orig) ;

private:
    Argument * Copy() const                 { return new IntArg(*this) ; }
    // Prevent compiler from defining the following
    IntArg() ;                          // Purposely leave unimplemented
    IntArg& operator=(const IntArg &) ; // Purposely leave unimplemented

public:
    virtual ArgType     Type() const        { return INT ; }
    virtual unsigned    IsInt() const       { return 1 ; }
    virtual int         IntVal() const ;
    virtual unsigned    SetValue(const char *string) ;

private:
    int     _default_val ;
    int     _value ;
} ; // class IntArg

/* -------------------------------------------------------------- */

class VFC_DLL_PORT DoubleArg : public Argument
{
public:
    DoubleArg(const char *name, const char *help, unsigned optional=1, int group=0, double default_val=0.0) ;
    virtual ~DoubleArg() ;

protected:
    DoubleArg(const DoubleArg &orig) ;

private:
    Argument * Copy() const                 { return new DoubleArg(*this) ; }
    // Prevent compiler from defining the following
    DoubleArg() ;                             // Purposely leave unimplemented
    DoubleArg& operator=(const DoubleArg &) ; // Purposely leave unimplemented

public:
    virtual ArgType     Type() const        { return DOUBLE ; }
    virtual unsigned    IsDouble() const    { return 1 ; }
    virtual double      DoubleVal() const ;
    virtual unsigned    SetValue(const char *string) ;

private:
    double  _default_val ;
    double  _value ;
} ; // class DoubleArg

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ArrayArg : public Argument
{
public:
    ArrayArg(const char *name, const char *help, unsigned optional=1, int group=0) ;
    virtual ~ArrayArg() ;

protected:
    ArrayArg(const ArrayArg &orig) ;

private:
    Argument * Copy() const                 { return new ArrayArg(*this) ; }
    // Prevent compiler from defining the following
    ArrayArg() ;                            // Purposely leave unimplemented
    ArrayArg& operator=(const ArrayArg &) ; // Purposely leave unimplemented

public:
    virtual ArgType     Type() const        { return ARRAY ; }
    virtual unsigned    IsArray() const     { return 1 ; }
    virtual Array *     ArrayVal() const ;
    virtual unsigned    SetValue(const char *string) ;

protected:
    Array   _value ;
} ; // class ArrayArg

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_COMMANDS_H_

