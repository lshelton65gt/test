/*
 *
 * [ File Version : 1.170 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // for sprintf() in 64 bit
#include <string.h>
#include <stdlib.h>
#include <ctype.h> // for isspace()
#include <iostream>
using namespace std ;

// To add a command :
//   - Create command in ComXXX file ( see other commands ) .
//   - Include the header file here
//   - Add a constructor call in 'Commands::Commands' constructor (below)

#include "tcl.h"

#include "Commands.h"
#include "Strings.h"
#include "Message.h"
#include "ComList.h"
#include "ComRead.h"
#include "ComWrite.h"

// Enable location support inside TCL.
// Note that we do not support column info here.
//#define COMMAND_ENABLE_TCL_LINEFILE_INFO
#if defined(COMMAND_ENABLE_TCL_LINEFILE_INFO) && defined(VERIFIC_LINEFILE_INCLUDES_COLUMNS)
#include <fstream> // ifstream
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Static member initialization
Map *Command::_msgs = 0 ;

// ****************************************************************

// Default file extentions, and which format they represent
// Also, the -format switch is sensitive to the strings given

static const char *formats[][2] = {
    {"edif",    (char*)FORM_EDIF},
    {"edf",     (char*)FORM_EDIF},
    {"edn",     (char*)FORM_EDIF},
    {"verilog", (char*)FORM_VERILOG},
    {"v",       (char*)FORM_VERILOG},
    {"veri",    (char*)FORM_VERILOG},
    {"sv",      (char*)FORM_SYS_VERILOG},
    {"sysv",    (char*)FORM_SYS_VERILOG},
    {"vhdl",    (char*)FORM_VHDL},
    {"vhd",     (char*)FORM_VHDL},
    {"lib",     (char*)FORM_SYNLIB},
    {"syn",     (char*)FORM_SYNLIB},
    {"sdf",     (char*)FORM_SDF},
    {"vdb",     (char*)FORM_BINARY_DUMP},
    {"vbd",     (char*)FORM_BINARY_DUMP},
    {"blif",    (char*)FORM_BLIF},
    {"upf",     (char*)FORM_UPF},
    {0,0}
} ;

// ****************************************************************
// Class Commands
// ****************************************************************

Commands::Commands()
  : _commands(STRING_HASH)
{
    // Now add the commands

    Add(new ComPreProcess()) ;

    // Parse-tree commands
    Add(new ComAnalyze()) ;
    Add(new ComPrettyPrint()) ;

    // VIPER #1777, 1781, 2023, 3570: VHDL Library alias:
    Add(new ComSetVhdlLibraryAlias()) ;
    Add(new ComUnsetVhdlLibraryAlias()) ;

    Add(new ComElaborate()) ;

    // Elaboration commands
    Add(new ComRead()) ;
    Add(new ComVhdlReadAll()) ;

    // VHDL binary save-restore (persistence) commands
    Add(new ComGetVhdlLibraryPath()) ;
    Add(new ComSetVhdlLibraryPath()) ;
    // VERILOG binary save-restore (persistence) commands
    Add(new ComGetVeriLibraryPath()) ;
    Add(new ComSetVeriLibraryPath()) ;
    Add(new ComSave());
    Add(new ComRestore());

    // Message handler commands
    Add(new ComSetMsgType()) ;
    Add(new ComClearMsgType()) ;

    // Cleanup (parse-tree/netlist) command
    Add(new ComCleanup()) ;

    // VHDL File Sorter commands
    Add(new ComVhdlRegister()) ;
    Add(new ComVhdlClearRegistry()) ;
    Add(new ComListSortedVhdlFiles()) ;

    Add(new ComExplicitStateMachine()) ; // VIPER #6813
}

Commands::~Commands()
{
    MapIter i ;
    Command *com ;
    FOREACH_COMMAND(this, i, com) delete com ;
    // commands 'Map' will be removed by destructor
}

void
Commands::Add(const Command *command)
{
    VERIFIC_ASSERT(command) ;
    unsigned ret = _commands.Insert(command->Name(), command) ;
    VERIFIC_ASSERT(ret) ; (void)ret ;
}

void
Commands::Remove(const Command *command)
{
    VERIFIC_ASSERT(command) ;
    unsigned ret = _commands.Remove(command->Name()) ;
    VERIFIC_ASSERT(ret) ; (void)ret ;
}

Command *
Commands::Get(const char *name) const // Get a command by name
{
    return (Command*)_commands.GetValue(name) ;
}

// Check if the user set this option :
unsigned
Command::IsSet(const char *option_name) const
{
    // This will assert if 'option_name' is not defined in the constructor of this command
    Argument *arg = Get(option_name) ;
    VERIFIC_ASSERT(arg) ;
    return arg->IsSet() ;
}

// ****************************************************************
// Class Command
// ****************************************************************

Command::Command(const char *name, const char *help)
  : _name(Strings::save(name)),
    _help(Strings::save(help)),
    _options(STRING_HASH),
    _linefile(0)
{
    // Every command get boolean option -help
    // Set help on help to 0, so we don't print in 'usage'
    Add(new BoolArg("help", 0)) ;
}

Command::Command(const Command &orig)
  : _name(Strings::save(orig._name)),
    _help(Strings::save(orig._help)),
    _options(orig._options.HashType(), orig._options.Size()),
    _linefile(orig._linefile)
{
    MapIter mi ;
    const Argument *arg ;
    FOREACH_MAP_ITEM(&orig._options, mi, 0, &arg) {
        VERIFIC_ASSERT(arg) ;
        Argument *copy = arg->Copy() ;
        (void) _options.Insert(copy->Name(), copy) ;
    }

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    // Need to make a copy of the linefile if column info is enabled.
    if (_linefile) _linefile = _linefile->Copy() ;
#endif
}

Command::~Command()
{
    Strings::free(_name) ;
    Strings::free(_help) ;

    MapIter i ;
    Argument *arg ;
    FOREACH_MAP_ITEM(&_options, i, 0, &arg) {
        delete arg ;
    }
}

// Get value of option by option name
// Make sure that you (C++ application programmer) use the correct routine
// with the same type and same name that you declared in the TCL command constructor.
// Otherwise we will assert here.

unsigned
Command::BoolVal(const char *option_name) const
{
    // This will assert if 'option_name' is not defined in the constructor of this command
    Argument *arg = Get(option_name) ;
    VERIFIC_ASSERT(arg) ;
    return arg->BoolVal() ;
}

int
Command::IntVal(const char *option_name) const
{
    // This will assert if 'option_name' is not defined in the constructor of this command
    Argument *arg = Get(option_name) ;
    VERIFIC_ASSERT(arg) ;
    return arg->IntVal() ;
}

const char *
Command::StringVal(const char *option_name) const
{
    // This will assert if 'option_name' is not defined in the constructor of this command
    Argument *arg = Get(option_name) ;
    VERIFIC_ASSERT(arg) ;
    return arg->StringVal() ;
}

double
Command::DoubleVal(const char *option_name) const
{
    // This will assert if 'option_name' is not defined in the constructor of this command
    Argument *arg = Get(option_name) ;
    VERIFIC_ASSERT(arg) ;
    return arg->DoubleVal() ;
}

Array *
Command::ArrayVal(const char *option_name) const
{
    // This will assert if 'option_name' is not defined in the constructor of this command
    Argument *arg = Get(option_name) ;
    VERIFIC_ASSERT(arg) ;
    return arg->ArrayVal() ;
}

// Internal usage : add/remove option
void
Command::Add(Argument *arg)
{
    VERIFIC_ASSERT(arg) ;
    if (!_options.Insert(arg->Name(),arg)) {
        VERIFIC_ASSERT(0) ; // Trying to insert same-name option
        return ;
    }

    // Made 'this' the owner of arg
    arg->_owner = this ;

    // Update the 'overlap' in the existing options
    MapIter i ;
    Argument *option ;
    unsigned common ;
    FOREACH_MAP_ITEM(&_options,i,0,&option) {
        if (option==arg) continue ;

        // Don't do key checking for arguments (which have no key)
        if (!arg->_name || !option->_name) continue ;

        // Here, we are comparing two option names
        // Get the length of common characters between this option and the new one
        // Don't know a better way to do this
        common = 0 ;
        while (arg->_name[common] == option->_name[common]) common++ ;

        // Update the 'overlap' field in the present and existing option
        if (common > option->_overlap) option->_overlap = common ;
        if (common > arg->_overlap) arg->_overlap = common ;
    }
}

void
Command::Remove(Argument *option)
{
    VERIFIC_ASSERT(option) ;
    unsigned ret = _options.Remove(option->Name()) ;
    VERIFIC_ASSERT(ret) ; (void)ret ;
    // Make option ownerless
    option->_owner = 0 ;
}

Argument *
Command::Get(const char *name) const
{
    return (Argument*)_options.GetValue((name) ? name : "") ;
}

Argument *
Command::FindOption(const char *string) const
{
    // Determine if 'string' is option or unnamed argument
    if (string[0] == '-') {
        // It's an option.
        string++ ; // Skip '-' prefix
    } else {
        // It's an argument, so return the unnamed 'argument' option.
        Argument *option = Get(0) ;
        if (!option) {
            Error("%s: expected an argument (near '%s')", Name(), string) ;
            return 0 ;
        }
        return option ;
    }

    // Try exact match
    Argument *option = Get(string) ;
    if (option) return option ;

    // Perform option name completion, based on min overlap of option names.
    unsigned len = (unsigned)Strings::len(string) ;
    MapIter i ;
    FOREACH_MAP_ITEM(&_options, i, 0, &option) {
        if (!option->_name) continue ; // It's an argument
        // Match at least to the length of the string itself
        if (strncmp(string, option->_name, len) != 0) continue ;
        // Here, we have a match ; check ambiguity
        if (len<=option->_overlap) {
            // There will be more options that start with 'string' print a list of all matches
            MapIter j ;
            char *all_options = Strings::save("") ;
            FOREACH_MAP_ITEM(&_options,j,0,&option) {
                if (!option->_name) continue ;
                if (strncmp(string, option->_name, len)==0) {
                    char *tmp = all_options ;
                    all_options = Strings::save(all_options, " ", option->_name) ;
                    Strings::free(tmp) ;
                }
            }
            Error("%s: ambiguous option -%s (possible options : %s)", Name(), string, all_options) ;
            Strings::free(all_options) ;
            return 0 ;
        }
        return option ;
    }

    Error("%s: unknown option -%s", Name(), string) ;
    return 0 ; // No match
}

unsigned
Command::ParseArgs(int argc, const char * const argv[]) const
{
    // Parse arguments and set the values

    // Clear previous error count
    Message::ClearErrorCount() ;

    // First clear the 'set' flags from previous run
    ClearParseFlags() ;

    // Now walk over the command line; find and set options/arguments
    Argument *option = 0, *prev_option = 0 ;
    int i = 0 ;
    while (++i != argc) {
        if (prev_option) {
                // Set option value
                if (!prev_option->SetValue(argv[i])) return 0 ;
                prev_option = 0 ;
        } else {
            // Corner-case: If 'argv[i]' contains only spaces, skip.  This situation can arise via back-slash use.
            if (argv[i] && isspace(*argv[i])) {
                unsigned is_empty_string = 0 ;
                const char *str = argv[i] ;
                while (str++) {
                    if (*str == '\0') { is_empty_string = 1 ; break ; }
                    if (!isspace(*str)) break ;
                }
                if (is_empty_string) continue ;
            }
            // No previous option. Get the new one
            option = FindOption(argv[i]) ;
            if (!option) return 0 ; // Error was given
            if (option->IsOption()) {
                if (option->IsBool()) {
                    // Boolean options don't take an argument
                    if (!option->SetValue((char*)"1")) return 0 ;
                } else {
                    prev_option = option ;
                }
            } else {
                // It's a real argument
                if (!option->SetValue(argv[i])) return 0 ;
            }
        }
    }

    // If prev_option is present without value argument, produce error
    if (prev_option && prev_option->Optional()) {
        VERIFIC_ASSERT(option) ;
        Error("%s: option -%s expects an argument", Name(), option->Name()) ;
        return 0 ;
    }

    if (BoolVal("help")) return 1 ; // Usage will be printed further up

    // Check for non-optional arguments
    MapIter mi ;
    FOREACH_MAP_ITEM(Options(), mi, 0, &option) {
        if (option->Group()) continue ;
        if (!option->Optional() && !option->IsSet() && !option->Group()) {
            if (Strings::compare(option->Name(), "")) {
                Error("%s: command expects an argument", argv[0]) ;
            } else {
                Error("%s: option -%s needs to be specified", Name(), option->Name()) ;
            }
            return 0 ;
        }
    }

    // Check for proper setting of groups :
    //     - Positive groups are mutual exclusive.
    //     - Negative groups are NOT mutual exclusive.
    FOREACH_MAP_ITEM(Options(), mi, 0, &prev_option) {
        if (!prev_option->Group()) continue ;
        unsigned group_set = prev_option->IsSet() ;
        MapIter mi2 ;
        // Check setting of any members of the group
        FOREACH_MAP_ITEM(Options(), mi2, 0, &option) {
            if (option->Group() != prev_option->Group()) continue ;
            if (option==prev_option) continue ;
            if (option->IsSet()) group_set = 1 ;
            // Mutual Exclusivity check. Not for negative group numbers
            if (option->Group() < 0) continue ;
            if (prev_option->IsSet() && option->IsSet()) {
                Error("%s: cannot set both options -%s and -%s", Name(), option->Name(), prev_option->Name()) ;
                return 0 ;
            }
        }
        // Check that non-optional groups are indeed set
        if (!group_set && !prev_option->Optional()) {
            if (!Strings::compare(prev_option->Name(), "")) {
                Error("%s: command expects an argument", argv[0]) ;
            } else {
                Error("%s: option -%s needs to be specified", Name(), prev_option->Name()) ;
            }
            return 0 ;
        }
    }

    return 1 ; // Successful parsing
}

void
Command::Usage() const
{
    // Print usage for this command to cout (Usage only useful in interactive mode)
    char *usage = Strings::save("\"", Name(), "\" : ", (_help) ? _help : "") ;
    Message::Msg(VERIFIC_NONE, 0, 0, usage) ;
    Strings::free(usage) ;
    MapIter iter ;
    Argument *option ;
    // First find max name length of all option description
    unsigned max_len = 0 ;
    FOREACH_MAP_ITEM(&_options, iter, 0, &option) {
        unsigned len = Strings::len(option->Name()) + 1 ;
        if (option->Optional())       { len += 2 ; }
        if (option->MultipleArgs())   { len += 7 ;
        } else if (!option->IsBool()) { len += 6 ; }
        max_len = (len > max_len) ? len : max_len ;
    }
    // Now print each option's usage
    FOREACH_MAP_ITEM(&_options, iter, 0, &option) {
        option->Usage(max_len) ;
    }
}

void
Command::ClearParseFlags() const
{
    // First clear the 'set' flags from previous run
    MapIter iter ;
    Argument *option ;
    FOREACH_MAP_ITEM(&_options, iter, 0, &option) {
        option->_set = 0 ;
    }
}

/**************** Object Handles : Created and used ************/

char
Command::HandleGetType(const char *address) const
{
    if (!address || *address!='@') return 0 ;
    return address[1] ;
}

DesignObj *
Command::HandleGetObject(const char *address) const
{
    if (!address || *address!='@') return 0 ;
    return (DesignObj*)(Strings::atoul(address+2)) ;
}

void
Command::CreateHandle(DesignObj *p, char type, char result[]) const
{
    ::sprintf(result,"@%c%ld",type,(unsigned long)p) ;
}

// ****************************************************************
// Utilities for file formats for commands that deal with files
// ****************************************************************

format_enum
Command::FormatFromFileName(const char *file_name) const
{
    if (!file_name) return FORM_UNKNOWN ;

    // Find last file/extension character (.) in file name
    const char *extension = strrchr(file_name,'.') ;
    if (!extension) return FORM_UNKNOWN ;

    return Format(extension+1) ;
}

format_enum
Command::Format(const char *text) const
{
    if (!text) return FORM_UNKNOWN ;

    unsigned i = 0 ;
    while (formats[i][0]) {
        // Compare
        if (Strings::compare_nocase(text, formats[i][0])) {
            return (format_enum)(unsigned long)(formats[i][1]) ;
        }
        i++ ;
    }
    return FORM_UNKNOWN ;
}

char *
Command::Format(format_enum format) const
{
    unsigned i = 0 ;
    while (formats[i][0]) {
        if (formats[i][1]==(char*)format) return (char*)formats[i][0] ;
        i++ ;
    }
    return (char *)"unknown" ;
}

#ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO

#ifndef Tcl_DictObjGet_TCL_DECLARED
// Define this API, if it is not defined yet (it should be defined, though, if TCL version >= 8.5).
// This can be checked with ((TCL_MAJOR_VERSION > 8) || ((TCL_MAJOR_VERSION == 8) && (TCL_MINOR_VERSION >= 5)))
// But our local tcl.h file is from version 8.4 which always fails even if the installer version is 8.5!
// So, not using this for now. Using the API directly, may fail to link if TCL version < 8.5.
// In that case we cannot support linefile in TCL and request customer to use TCL having verion >= 8.5
extern "C" int Tcl_DictObjGet(Tcl_Interp *interp, Tcl_Obj *dictPtr, Tcl_Obj *keyPtr, Tcl_Obj **valuePtrPtr) ;
#endif // #ifndef Tcl_DictObjGet_TCL_DECLARED

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS

/* -------------------------------------------------------------- */

// Class definition needed for Tcl linefile with column support
class LookupFile : public std::ifstream
{
public:
    explicit LookupFile(const char *file_name)
      : std::ifstream(file_name),
        _file_name(Strings::save(file_name)),
        _line_offset(0) { }
    ~LookupFile()                             { close() ; Strings::free(_file_name) ; }
    const char * FileName() const             { return _file_name ; }
    unsigned     LineOffset() const           { return _line_offset ; }
    void         LineOffset(unsigned offset)  { _line_offset = offset ; }
    void         Reset()                      { seekg(0); _line_offset = 0 ; }

private:
    LookupFile() ; // Purposely leave unimplemented
    char         *_file_name ;
    unsigned      _line_offset ;
} ; // class LookupFile

/* -------------------------------------------------------------- */

// Global hidden Map storing open LookupFiles
static Map *ifiles = 0 ; // (STRING_HASH) (char*)file_name -> *LookupFile
#endif // #ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS

// TCL seems to return file name with absolute path, which does not look good.
// Caller of this function may choose to pass a (relative) file path name
// which will be used instead of the one returned by TCL for better look.
linefile_type
Command::GetTclLineFile(Tcl_Interp *interp, const char *file /*=0*/)
{
    if (!interp) return 0 ;

    // Following finds the line in a simpler way, but the structure "Interp"
    // is not available here, it is defined in TCL internal data-structure.
    //Interp *iPtr = (Interp *)interp ;
    //int line = (iPtr && iPtr->cmdFramePtr) ? iPtr->cmdFramePtr->line : 0 ;

    // Save the current result:
    Tcl_Obj *old_result = Tcl_GetObjResult(interp) ;
    Tcl_IncrRefCount(old_result) ; // Increment ref-count to make it alive

    linefile_type lf = 0 ;
    // Now, evaluate 'info frame -1' command to get the info about caller of this routine:
    if (Tcl_EvalEx(interp, "info frame -1", -1, TCL_EVAL_GLOBAL) == TCL_OK) {
        //  Get the result of the evaluation:
        Tcl_Obj *result_obj = Tcl_GetObjResult(interp) ;

        if (result_obj) {
            // Now get the line info from this result (which is a dictionary/map):
            Tcl_Obj *key = Tcl_NewStringObj("line", -1 /* take the whole string */) ;
            Tcl_Obj *value = 0 ;
            int lineno = 0 ;
            if (Tcl_DictObjGet(interp, result_obj, key, &value) == TCL_OK) {
                if (value && (Tcl_GetIntFromObj(interp, value, &lineno) != TCL_OK)) {
                    // Something wrong happened, reset line number:
                    lineno = 0 ;
                }
            }

            // Now get the file info from the same result in the same way:
            if (!file) {
                // Only do this if file is not specified in the argument:
                Tcl_SetStringObj(key, "file", -1 /* take the whole string */) ;
                value = 0 ;
                if (Tcl_DictObjGet(interp, result_obj, key, &value) == TCL_OK) {
                    // NOTE: This always seem to return an absolute filename:
                    //file = (value) ? Tcl_GetStringFromObj(value, 0 /* length of the string is not required */) : 0 ;
                    file = (value) ? Tcl_GetString(value) : 0 ;
                    // Now make filename relative to PWD.
                    char cmd[4096] ;
                    sprintf(cmd, "regsub \"[pwd]/\" {%s} {}", file) ;
                    if (Tcl_EvalEx(interp, cmd, -1, TCL_EVAL_GLOBAL) == TCL_OK) {
                        file = Tcl_GetStringResult(interp) ;
                    }
                    // Note that the TCL help says: The storage referenced by the returned byte pointer
                    // is owned by the object manager and should not be modified by the caller.
                }
            }

            // And create linefile with the line and file info, if present:
            if (file && lineno) {
                lf = LineFile::EncodeLineFile(file, (unsigned)lineno) ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
                // With column information enabled, we now need to do string matching to determine
                // beginning and ending line/column information.
                if (Tcl_EvalEx(interp, "dict get [info frame -1] cmd", -1, TCL_EVAL_GLOBAL) == TCL_OK) {
                    char cmd[4096] ;
                    sprintf(cmd, "%s", Tcl_GetStringResult(interp)) ; // Store actual command (string representation)
                    // Check next level to see if this was eval'd.
                    if (Tcl_EvalEx(interp, "dict get [info frame -2] cmd", -1, TCL_EVAL_GLOBAL) == TCL_OK) {
                        const char *cmd2 = Tcl_GetStringResult(interp) ;
                        if (strstr(cmd2, "eval") == cmd2) {
                            // Yes, this was eval'd.
                            // Need to update cmd and lineno
                            if (strstr(cmd2, "_load_upf_protected")) {
                                // This was a _load_upf_protected call.  Go down one more level for info.
                                if (Tcl_EvalEx(interp, "dict get [info frame -3] cmd", -1, TCL_EVAL_GLOBAL) == TCL_OK) {
                                    const char *cmd2 = Tcl_GetStringResult(interp) ;
                                    sprintf(cmd, "%s", cmd2) ;
                                    if (Tcl_EvalEx(interp, "dict get [info frame -3] line", -1, TCL_EVAL_GLOBAL) == TCL_OK) {
                                        lineno = Strings::atoi(Tcl_GetStringResult(interp)) ;
                                    }
                                }
                            } else {
                                sprintf(cmd, "%s", cmd2) ;
                                if (Tcl_EvalEx(interp, "dict get [info frame -2] line", -1, TCL_EVAL_GLOBAL) == TCL_OK) {
                                    lineno = Strings::atoi(Tcl_GetStringResult(interp)) ;
                                }
                            }
                        }
                    }
                    // LEFT LINE NUMBER: ----------------
                    unsigned line_left = (unsigned)lineno ;
                    // Get actual line in file
                    char line[4096] ;
                    if (!ifiles) ifiles = new Map(STRING_HASH) ;
                    LookupFile *ifile = (LookupFile*)ifiles->GetValue(file) ;
                    if (!ifile) {
                        ifile = new LookupFile(file) ;
                        ifiles->Insert(ifile->FileName(), ifile) ;
                    }
                    // If open ifile line offset is past requested line offset, reset ifile back to beginning
                    if (ifile->LineOffset() > line_left) {
                       ifile->Reset() ;
                    }
                    unsigned i = ifile->LineOffset() ;
                    while (i++ < line_left) {
                        ifile->getline(line, sizeof(line), '\n') ;
                    }
                    ifile->LineOffset(line_left) ;
                    // LEFT COLUMN OFFSET: ----------------
                    char *p = strchr(cmd, '\n') ; // Compare up to first carriage return.
                    if (p) *p = '\0' ;
                    char *col = strstr(line, cmd) ;
                    unsigned col_left = col-line+1 ;
                    if (p) *p = '\n' ; // add '\n' back in
                    // RIGHT LINE NUMBER: ----------------
                    unsigned line_right = line_left ;
                    char *last_ret = 0 ;
                    p = strchr(cmd, '\n') ;
                    while (p) {
                        line_right++ ;
                        last_ret = p ;
                        p = strchr(p+1, '\n') ;
                    }
                    // RIGHT COLUMN OFFSET: ----------------
                    unsigned col_right = (last_ret) ? Strings::len(last_ret+1) : col_left+Strings::len(cmd)-1 ;
                    // Now update linefile information
                    lf->SetLeftLine(line_left) ;
                    lf->SetRightLine(line_right) ;
                    lf->SetLeftCol(col_left) ;
                    lf->SetRightCol(col_right) ;
                }
#endif // #ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            }

            // Now, delete the Tcl_Obj *key:
            Tcl_DecrRefCount(key) ;
        }
    }

    // Restore back the old result:
    Tcl_SetObjResult(interp, old_result) ;
    // We are not longer using it, decrement the ref-count we increased above:
    Tcl_DecrRefCount(old_result) ;

    return lf ;
}
#else // #ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO
linefile_type Command::GetTclLineFile(Tcl_Interp * /*interp*/, const char * /*file=0*/) { return 0 ; }
#endif // #ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
/*static*/ void
Command::ResetLookupFile(const char *file)
{
#ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO
    if (!ifiles || !ifiles->GetValue(file)) return ;
    LookupFile *ifile = (LookupFile*)ifiles->GetValue(file) ;
    ifiles->Remove(file) ;
    delete ifile ;
#else
    (void) file ;
#endif // #ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO
}

/*static*/ void
Command::ResetLookupFiles()
{
#ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO
    MapIter mi ;
    LookupFile *ifile ;
    FOREACH_MAP_ITEM(ifiles, mi, 0, &ifile) delete ifile ;
#endif // #ifdef COMMAND_ENABLE_TCL_LINEFILE_INFO
}
#endif // #ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS

/* -------------------------------------------------------------- */
/* Messaging */
/* -------------------------------------------------------------- */

/* static */ const char *
Command::GetMessageId(const char *msg)
{
    if (!msg) return 0 ;

    // Initialize static map if undefined.  NOTE, entire Tcl projects may be derived from Command and may
    // extend this _msg message table.  Because of this, initialize appropriately.
    static unsigned initialized = 0 ;
    if (!_msgs || !initialized) {
        if (!_msgs) _msgs = new Map(STRING_HASH, 89) ;
        initialized = 1 ;
        _msgs->Insert("%s: expected an argument (near '%s')", "CMD-1000") ;
        _msgs->Insert("%s: ambiguous option -%s (possible options : %s)", "CMD-1001") ;
        _msgs->Insert("%s: unknown option -%s", "CMD-1002") ;
        _msgs->Insert("%s: map argument '%s' has no pairing", "CMD-1003") ;
        _msgs->Insert("%s: map argument '%s' cannot be parsed", "CMD-1004") ;
        _msgs->Insert("%s: option -%s needs to be specified", "CMD-1005") ;
        _msgs->Insert("%s: command expects an argument", "CMD-1006") ;
        _msgs->Insert("%s: cannot set both options -%s and -%s", "CMD-1007") ;
        _msgs->Insert("%s: default option was already specified", "CMD-1008") ;
        _msgs->Insert("%s: option -%s was already specified", "CMD-1009") ;
        _msgs->Insert("%s: option -%s expected an integer (near '%s')", "CMD-1010") ;
        _msgs->Insert("%s: option -%s expected a floating point number (near '%s')", "CMD-1011") ;
        _msgs->Insert("%s: option -%s previously set; overwriting value '%s'", "CMD-1012") ;
        _msgs->Insert("%s: option -%s expects an argument", "CMD-1013") ;
        _msgs->Insert("%s: failed", "CMD-1014") ;
        _msgs->Insert("%s: failed because %s", "CMD-1015") ;

        _msgs->Insert("%s: present design is not set", "CMD-2000") ;
        _msgs->Insert("%s: object is not a netlist", "CMD-2001") ;
        _msgs->Insert("%s: first object is not a netlist", "CMD-2002") ;
        _msgs->Insert("%s: second object is not a netlist", "CMD-2003") ;
        _msgs->Insert("%s: expected 1 or 2 arguments", "CMD-2004") ;
        _msgs->Insert("%s: both objects represent the same netlist. No need to compare", "CMD-2005") ;
        _msgs->Insert("%s: Need module/unit name", "CMD-2006") ;
        _msgs->Insert("%s: Can't get present_design.  Have you elaborated anything?", "CMD-2007") ;
        _msgs->Insert("Present Design is %s(%s) in library %s", "CMD-2008") ;
        _msgs->Insert("%s: Library %s does not exist", "CMD-2009") ;
        _msgs->Insert("%s: Can't find cell %s in library", "CMD-2010") ;
        _msgs->Insert("%s: Can't find any netlist of cell %s in specified library", "CMD-2011") ;
        _msgs->Insert("%s: Can't find cell %s(%s) in specified library", "CMD-2012") ;
        _msgs->Insert("%s: container should be an object handle", "CMD-2013") ;
        _msgs->Insert("%s: present design is not set. Cannot determine area", "CMD-2014") ;
        _msgs->Insert("%s: optimizing netlist %s(%s)", "CMD-2015") ;
        _msgs->Insert("netlist %s(%s) had %d operator optimizations", "CMD-2016") ;
        _msgs->Insert("netlist %s(%s) had %d resource sharing optimizations", "CMD-2017") ;
        _msgs->Insert("timing before optimization : %d levels", "CMD-2018") ;
        _msgs->Insert("timing after optimization : %d levels", "CMD-2019") ;
        _msgs->Insert("Now available : use command 'map' for full technology mapping, with LUTs", "CMD-2020") ;
        _msgs->Insert("%s: library %s is not yet compiled", "CMD-2021") ;
        _msgs->Insert("%s: mapping netlist %s(%s)", "CMD-2022") ;
        _msgs->Insert("Default VHDL Library Search Path: %s", "CMD-2023") ;
        _msgs->Insert("VHDL Library Path for '%s': %s", "CMD-2024") ;
        _msgs->Insert("%s: 'associate' expects : <libname>=<dirname>. found : %s", "CMD-2025") ;
        _msgs->Insert("Default Verilog Library Search Path: %s", "CMD-2026") ;
        _msgs->Insert("Verilog Library Path for '%s': %s", "CMD-2027") ;
        _msgs->Insert("Default Library Search Path: %s", "CMD-2028") ;
        _msgs->Insert("Library Path for '%s': %s", "CMD-2029") ;
        _msgs->Insert("%s: probably incorrect message id '%s'.  Message id's are typically formatted <PREFIX>-<number>", "CMD-2030") ;
        _msgs->Insert("%s: the VHDL library %s does not exist", "CMD-2031") ;
        _msgs->Insert("%s: the VHDL primary unit %s does not exist", "CMD-2032") ;
        _msgs->Insert("VHDL primary unit %s is now considered a black-box", "CMD-2033") ;
        _msgs->Insert("%s: Verilog module %s does not exist", "CMD-2034") ;
        _msgs->Insert("Verilog module %s is now considered a black-box", "CMD-2035") ;
        _msgs->Insert("%s: library %s does not exist", "CMD-2036") ;
        _msgs->Insert("%s: netlist %s does not exist", "CMD-2037") ;
        _msgs->Insert("netlist %s is now considered a black-box", "CMD-2038") ;
        _msgs->Insert("All registered VHDL files were deleted from the sorted list", "CMD-2039") ;
        _msgs->Insert("%s: cannot open file %s", "CMD-2040") ;
        _msgs->Insert("%s: cannot derive format from file %s. Use -format option", "CMD-2041") ;
        _msgs->Insert("%s: use 'read' for binary dump files", "CMD-2042") ;
        _msgs->Insert("%s: option -design_top must be specified when analyzing a UPF file", "CMD-2043") ;
        _msgs->Insert("%s: cannot read format %s in this product", "CMD-2044") ;
        _msgs->Insert("%s: The file '%s' contains an unknown binary dump format", "CMD-2045") ;
        _msgs->Insert("%s: option -all cannot be used with netlist arguments", "CMD-2046") ;
        _msgs->Insert("%s: option -statics can only be used when option -all is specified", "CMD-2047") ;
        _msgs->Insert("The vhdl parse-tree '%s' was deleted from the library '%s'", "CMD-2048") ;
        _msgs->Insert("%s: The vhdl parse-tree '%s' was not deleted from library '%s'", "CMD-2049") ;
        _msgs->Insert("All vhdl parse-trees (not including packages) were deleted", "CMD-2050") ;
        _msgs->Insert("All vhdl parse-trees (including packages) were deleted", "CMD-2051") ;
        _msgs->Insert("The verilog parse-tree '%s' was deleted", "CMD-2052") ;
        _msgs->Insert("%s: The verilog parse-tree '%s' was not deleted (could not be found)", "CMD-2053") ;
        _msgs->Insert("All verilog parse-trees were deleted", "CMD-2054") ;
        _msgs->Insert("All parse-trees and netlists were deleted", "CMD-2055") ;
        _msgs->Insert("%s: The Database library '%s' does not exist", "CMD-2056") ;
        _msgs->Insert("%s: The netlist '%s' does not exist in library '%s'", "CMD-2057") ;
        _msgs->Insert("The netlist '%s' was deleted from library '%s'", "CMD-2058") ;
        _msgs->Insert("All netlists in library '%s' were deleted", "CMD-2059") ;
        _msgs->Insert("%s: The netlist '%s' was not deleted (could not be found)", "CMD-2060") ;
        _msgs->Insert("All netlists in all libraries were deleted", "CMD-2061") ;
        _msgs->Insert("%s: Save Restore failed; library/unit search path not set; cannot restore", "CMD-2062") ;
        _msgs->Insert("The VhdlLibrary alias '%s' was set to '%s'", "CMD-2063") ;
        _msgs->Insert("The VhdlLibrary alias '%s' was unset", "CMD-2064") ;
        _msgs->Insert("%s: cannot derive format from file extension. Use -format option", "CMD-2065") ;
        _msgs->Insert("%s: library %s is not yet loaded", "CMD-2066") ;
        _msgs->Insert("%s: present design is not set. Cannot write it", "CMD-2067") ;
        _msgs->Insert("%s: cannot write format %s yet", "CMD-2068") ;
        _msgs->Insert("%s: present design is not set - cannot write delay file", "CMD-2069") ;
        _msgs->Insert("%s: cannot pretty-print format %s", "CMD-2070") ;
        _msgs->Insert("%s: unknown format '%s'", "CMD-2071") ;
   }

    return (const char*)_msgs->GetValue(msg) ;
}

/* static */ void
Command::ResetMessageMap()
{
    delete _msgs ;
    _msgs = 0 ;
}

void
Command::Comment(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_COMMENT, GetMessageId(format), _linefile, format, args) ;
    va_end(args) ;
}

void
Command::Error(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_ERROR, GetMessageId(format), _linefile, format, args) ;
    va_end(args) ;
}

void
Command::Warning(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_WARNING, GetMessageId(format), _linefile, format, args) ;
    va_end(args) ;
}

void
Command::Info(const char *format, ...) const
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_INFO, GetMessageId(format), _linefile, format, args) ;
    va_end(args) ;
}

// ****************************************************************
// Class Argument (Option)
// ****************************************************************

Argument::Argument(const char *n, const char *h, unsigned o, int g, unsigned m)
  : _owner(0),
    _name(Strings::save(n)),
    _help(Strings::save(h)),
    _overlap(0),
    _group(g),
    _optional(o),
    _multiple_args(m),
    _set(0)
{}

Argument::Argument(const Argument &orig)
  : _owner(orig._owner),
    _name(Strings::save(orig._name)),
    _help(Strings::save(orig._help)),
    _overlap(orig._overlap),
    _group(orig._group),
    _optional(orig._optional),
    _multiple_args(orig._multiple_args),
    _set(orig._set)
{}

Argument::~Argument()
{
    Strings::free(_name) ;
    Strings::free(_help) ;
}

unsigned
Argument::Set()
{
    VERIFIC_ASSERT(_owner) ;

    if (_set && !_multiple_args) {
        // FIX ME for array vars
        if (!_name) {
            _owner->Error("%s: default option was already specified", _owner->Name()) ;
        } else {
            _owner->Error("%s: option -%s was already specified", _owner->Name(), Name()) ;
        }
        return 0 ;
    }
    _set = 1 ;
    return 1 ;
}

void
Argument::Usage(unsigned ident_offset/*=0*/) const
{
    // FIX ME for groups
    // output only to cout since this is only useful in interactive mode

    if (!_help) return ; // Don't print undocumented options..

    unsigned len = 0 ;  // Length of text left-of argument 'help' description
    char *tmp ;
    char *usage = Strings::save("") ;
    if (_optional) {
        tmp = usage ;
        usage = Strings::save(usage, "[") ;
        Strings::free(tmp) ;
        len++ ;
    }
    if (_name) {
        tmp = usage ;
        usage = Strings::save(usage, "-", _name) ;
        Strings::free(tmp) ;
        len += 1 + (unsigned)Strings::len(_name) ;
    }
    if (_multiple_args) {
        tmp = usage ;
        usage = Strings::save(usage, " <args>") ;
        Strings::free(tmp) ;
        len += 7 ;
    } else if (!IsBool()) {
        tmp = usage ;
        usage = Strings::save(usage, " <arg>") ;
        Strings::free(tmp) ;
        len += 6 ;
    }
    if (_optional) {
        tmp = usage ;
        usage = Strings::save(usage, "]") ;
        Strings::free(tmp) ;
        len++ ;
    }

    char buf[2048] ;
    if (ident_offset) len = ident_offset ;
    sprintf(buf, "    %-*s : %s", len, usage, _help) ;
    Message::Msg(VERIFIC_NONE, 0, 0, buf) ;
    Strings::free(usage) ;
}

// ****************************************************************

BoolArg::BoolArg(const char *name, const char *help, unsigned optional/*=1*/, int group/*=0*/, unsigned default_val/*=0*/)
  : Argument(name, help, optional, group, 0/*multiple_args*/),
    _default_val(default_val),
    _value(default_val)
{}

BoolArg::BoolArg(const BoolArg &orig)
  : Argument(orig),
    _default_val(orig._default_val),
    _value(orig._value)
{}

BoolArg::~BoolArg() {}

unsigned
BoolArg::SetValue(const char *string)
{
    if (!Set()) return 0 ;
    // Very crude. Need better check later
    _value = (unsigned)Strings::atoi(string) ;
    return 1 ;
}

unsigned
BoolArg::BoolVal() const
{
    return (_set) ? _value : _default_val ;
}

// ****************************************************************

StringArg::StringArg(const char *name, const char *help, unsigned optional/*=1*/, int group/*=0*/, const char* default_val/*=0*/)
  : Argument(name, help, optional, group, 0/*multiple_args*/),
    _default_val(Strings::save(default_val)),
    _value(Strings::save(default_val))
{}

StringArg::StringArg(const StringArg &orig)
  : Argument(orig),
    _default_val(Strings::save(orig._default_val)),
    _value(Strings::save(orig._value))
{}

StringArg::~StringArg() {
    Strings::free(_default_val) ;
    Strings::free(_value) ;
}

unsigned
StringArg::SetValue(const char *string)
{
    if (!Set()) return 0 ;
    if (_value) Strings::free(_value) ;
    _value = Strings::save(string) ;
    return 1 ;
}

const char *
StringArg::StringVal() const
{
    return (_set) ? _value : _default_val ;
}

// ****************************************************************

IntArg::IntArg(const char *name, const char *help, unsigned optional/*=1*/, int group/*=0*/, int default_val/*=0*/)
  : Argument(name, help, optional, group, 0/*multiple_args*/),
    _default_val(default_val),
    _value(default_val)
{}

IntArg::IntArg(const IntArg &orig)
  : Argument(orig),
    _default_val(orig._default_val),
    _value(orig._value)
{}

IntArg::~IntArg() {}

unsigned
IntArg::SetValue(const char *string)
{
    if (!Set()) return 0 ;
    char *remain = 0;
    _value = (int)strtol(string,&remain,10) ; // Casted explicitly for CC (HP-UX10)
    if (remain && remain[0]!='\0') {
        VERIFIC_ASSERT(_owner) ;
        _owner->Error("%s: option -%s expected an integer (near '%s')", _owner->Name(), Name(), string) ;
        return 0 ;
    }
    return 1 ;
}

int
IntArg::IntVal() const
{
    return (_set) ? _value : _default_val ;
}

// ****************************************************************

DoubleArg::DoubleArg(const char *name, const char *help, unsigned optional/*=1*/, int group/*=0*/, double default_val/*=0.0*/)
  : Argument(name, help, optional, group, 0/*multiple_args*/),
    _default_val(default_val),
    _value(default_val)
{}

DoubleArg::DoubleArg(const DoubleArg &orig)
  : Argument(orig),
    _default_val(orig._default_val),
    _value(orig._value)
{}

DoubleArg::~DoubleArg() {}

unsigned
DoubleArg::SetValue(const char *string)
{
    if (!Set()) return 0 ;
    char *remain = 0;
    _value = strtod(string, &remain) ;
    if (remain && remain[0]!='\0') {
        _owner->Error("%s: option -%s expected a floating point number (near '%s')", _owner->Name(), Name(), string) ;
        return 0 ;
    }
    return 1 ;
}

double
DoubleArg::DoubleVal() const
{
    return (_set) ? _value : _default_val ;
}

// ****************************************************************
// Array of String

ArrayArg::ArrayArg(const char *name, const char *help, unsigned optional/*=1*/, int group/*=0*/)
  : Argument(name, help, optional, group, 1/*multiple_args*/),
    _value()
{}

ArrayArg::ArrayArg(const ArrayArg &orig)
  : Argument(orig),
    _value(orig._value.Size())
{
    // Now copy char* contents of underlying Array member
    unsigned i ;
    const char *str ;
    FOREACH_ARRAY_ITEM(&orig._value, i, str) {
        _value.InsertLast(Strings::save(str)) ;
    }
}

ArrayArg::~ArrayArg()
{
    unsigned i ;
    char *str ;
    FOREACH_ARRAY_ITEM(&_value, i, str) {
        Strings::free(str) ;
    }
}

unsigned
ArrayArg::SetValue(const char *string)
{
    if (!_set) {
        unsigned i ;
        char *str ;
        FOREACH_ARRAY_ITEM(&_value, i, str) {
            Strings::free(str) ;
        }
        _value.Reset() ; // Reset the array
    }
    if (!Set()) return 0 ;

    // Split single arguments into multiple ones. This is only needed
    // for Tcl Interface, but I see no better place to do it then here.
    //
    // Issue 2871 : a TCL list can be with {}'s or with ""'s.
    // Either way we obtain a string without the "'s, so we do not know here
    // if it was a single argument with spaces, or a list of separate arguments.
    //
    // The only way to specify a file name with spaces is thus to write it as a single element in a list : {"My file name"}.
    // In that case, TCL returns us the "My file name" WITH the "'s. So we know it is a single argument.
    // Then we can skip (here) any argument enclosed in "'s, and see it as a single argument :
    //
    char *str = Strings::save(string) ;
    char *next_word = str ;
    while (next_word) {
        // Skip leading spaces :
        while (isspace(*next_word)) next_word++ ;
        if (*next_word=='\0') break ; // reached the end of the string.

        // found the start of a new word :
        char *word = next_word ;

        // See a ""ed string as a single argument :
        if (*word=='"') {
            // Set the start of the word one char further (right after the "):
            word++ ;
            // find the end of the string :
            next_word = (char*) strchr(word,'"') ;
            // Set that char to 0 and increment :
            if (next_word) *next_word++ = '\0' ;
            // Now, 'word' is set to the argument between the "'s.
            // next_word is set to the character after the string (which should be a space in normal cases).
            // So drop to normal next-word processing now.
        }
        // find end of this word :
        if (next_word) {
            next_word = (char*) strchr(next_word,' ') ;
            // set that to 0 :
            if (next_word) *next_word++ = '\0' ;
        }
        // And save the extracted word :
        _value.InsertLast(Strings::save(word)) ;
    }
    Strings::free(str) ;

    // Note : We do not save the string itself ! Should we ?
    return 1 ;
}

Array *
ArrayArg::ArrayVal() const
{
    return (_set) ? (Array*)&_value : 0 ;
}

// ****************************************************************

