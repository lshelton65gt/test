/*
 *
 * [ File Version : 1.53 - 2014/01/16 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_COMREAD_H_
#define _VERIFIC_COMREAD_H_

#include "Commands.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComPreProcess : public Command
{
public :
    ComPreProcess() ;
    virtual ~ComPreProcess() ;

private:
    // Prevent compiler from defining the following
    ComPreProcess(const ComPreProcess &) ;            // Purposely leave unimplemented
    ComPreProcess& operator=(const ComPreProcess &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComPreProcess

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComAnalyze : public Command
{
public :
    ComAnalyze() ;
    virtual ~ComAnalyze() ;

private:
    // Prevent compiler from defining the following
    ComAnalyze(const ComAnalyze &) ;            // Purposely leave unimplemented
    ComAnalyze& operator=(const ComAnalyze &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComAnalyze

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComElaborate : public Command
{
public :
    ComElaborate() ;
    virtual ~ComElaborate() ;

private:
    // Prevent compiler from defining the following
    ComElaborate(const ComElaborate &) ;            // Purposely leave unimplemented
    ComElaborate& operator=(const ComElaborate &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComElaborate

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComRead : public Command
{
public :
    ComRead() ;
    virtual ~ComRead() ;

private:
    // Prevent compiler from defining the following
    ComRead(const ComRead &) ;            // Purposely leave unimplemented
    ComRead& operator=(const ComRead &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComRead

/* -------------------------------------------------------------- */

// Clean up of parse-trees and netlists
class VFC_DLL_PORT ComCleanup : public Command
{
public :
    ComCleanup();
    ~ComCleanup();

private:
    // Prevent compiler from defining the following
    ComCleanup(const ComCleanup &) ;            // Purposely leave unimplemented
    ComCleanup& operator=(const ComCleanup &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *pTclInterp);

} ; // class ComCleanup

/* -------------------------------------------------------------- */

// Binary restore of a parse-tree
class VFC_DLL_PORT ComRestore : public Command
{
public :
    ComRestore();
    ~ComRestore();

private:
    // Prevent compiler from defining the following
    ComRestore(const ComRestore &) ;            // Purposely leave unimplemented
    ComRestore& operator=(const ComRestore &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *pTclInterp);

private :
} ; // class ComRestore

/* -------------------------------------------------------------- */

// VIPER #1777, 1781, 2023, 3570: VHDL Library alias:
class VFC_DLL_PORT ComSetVhdlLibraryAlias : public Command
{
public:
    ComSetVhdlLibraryAlias() ;
    virtual ~ComSetVhdlLibraryAlias() {} ;

private:
    // Prevent compiler from defining the following
    ComSetVhdlLibraryAlias(const ComSetVhdlLibraryAlias &) ;            // Purposely leave unimplemented
    ComSetVhdlLibraryAlias& operator=(const ComSetVhdlLibraryAlias &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComSetVhdlLibraryAlias

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComUnsetVhdlLibraryAlias : public Command
{
public:
    ComUnsetVhdlLibraryAlias() ;
    virtual ~ComUnsetVhdlLibraryAlias() {} ;

private:
    // Prevent compiler from defining the following
    ComUnsetVhdlLibraryAlias(const ComUnsetVhdlLibraryAlias &) ;            // Purposely leave unimplemented
    ComUnsetVhdlLibraryAlias& operator=(const ComUnsetVhdlLibraryAlias &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComUnsetVhdlLibraryAlias

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComVhdlReadAll : public Command
{
public :
    ComVhdlReadAll() ;
    virtual ~ComVhdlReadAll() ;

private:
    // Prevent compiler from defining the following
    ComVhdlReadAll(const ComVhdlReadAll &) ;            // Purposely leave unimplemented
    ComVhdlReadAll& operator=(const ComVhdlReadAll &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComVhdlReadAll

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_COMREAD_H_

