/*
 *
 * [ File Version : 1.73 - 2014/02/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.Er
 *
 *
*/

#include "ComWrite.h"

#include "Message.h"
#include "Strings.h"

// Get access to VHDL pretty-print
#include "vhdl_file.h"
// VIPER #6895: Getting the number of primary units in a vhdl library
#include "VhdlUnits.h"
// Get access to Verilog pretty-print
#include "veri_file.h"
// Get access to binary dump
#include "SaveRestore.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/********************************************************************/
/* Pretty Printing HDL parse trees                                  */
/********************************************************************/

// Constructor for the 'pretty_print' command
ComPrettyPrint::ComPrettyPrint() :
    Command("pretty_print","print out VHDL/Verilog parse-trees")
{
    Add(new StringArg("work","(work) library to print from")) ;
    Add(new StringArg("unit","print only this VHDL primary unit",1/* optional group*/,1)) ;
    Add(new StringArg("module","print only this Verilog module",1/* optional group*/,1)) ;
    Add(new StringArg("format","file format to write (vhdl,verilog)")) ;
    Add(new StringArg(0,"file name to print into",0 /*Not optional*/)) ;
}

// Destructor for the 'pretty_print' command
ComPrettyPrint::~ComPrettyPrint() {}

// Execute command 'pretty_print'
unsigned
ComPrettyPrint::Process(Tcl_Interp * /*interp*/)
{
    const char *file_name = StringVal(0) ;
    const char *unit_name = StringVal("unit") ;
    const char *module_name = StringVal("module") ;
    const char *lib_name = StringVal("work") ;
    unsigned no_lib_provided = 0;
    if (!lib_name) {
        lib_name = "work" ;
        no_lib_provided = 1;
    }

    if (unit_name) {
        return vhdl_file::PrettyPrint(file_name,unit_name,lib_name) ;
    }

    if (module_name) {
        return veri_file::PrettyPrint(file_name,module_name, lib_name) ;
    }

    // Default (no module name nor unit name).
    // Print an entire library to file.
    // Derive the language that we should write from the format option :
    const char *format_string = StringVal("format") ;
    format_enum format = FORM_UNKNOWN ;
    if (!format_string) {
        // Derive format from file extension
        format = FormatFromFileName(file_name) ;
        if (format==FORM_UNKNOWN) {
            Error("%s: cannot derive format from file extension. Use -format option", Name()) ;
            return 0 ;
        }
    } else {
        // Take format as-is
        format = Format(format_string) ;
        if (format==FORM_UNKNOWN) {
            Error("%s: unknown format '%s'", Name(), format_string) ;
            return 0 ;
        }
    }
    switch (format) {
    case FORM_VHDL :
        {
            return vhdl_file::PrettyPrint(file_name,0/*print whole library*/,lib_name) ;
        }
    case FORM_VERILOG :
        {
            if(no_lib_provided) {
                return veri_file::PrettyPrint(file_name, 0, 0) ;
            } else {
                return veri_file::PrettyPrint(file_name,0/*print whole library*/,lib_name) ;
            }
        }
    default :
        Error("%s: cannot pretty-print format %s", Name(), Format(format)) ;
        break ;
    }

    return 0 ; // fail
}

/********************************************************************/
/* Binary save of a parse-tree                                      */
/********************************************************************/

ComSave::ComSave()
  : Command("save", "save a parse-tree in binary format")
{
    Add(new StringArg("work", "(work) library where the unit(s) is analyzed", 0 /*required*/)) ;
    Add(new StringArg("unit", "name of VHDL primary unit to save", 1 /*optional*/)) ;
    Add(new StringArg("module", "name of Verilog module to save", 1 /*optional*/)) ;
}

// -----------------------------------------

ComSave::~ComSave()
{
}

// -----------------------------------------

unsigned
ComSave::Process(Tcl_Interp * /*interp*/)
{
    const char *lib_name = StringVal("work") ;  // Lib name is a required arg

    // If unit name is specified, attempt VHDL binary save
    const char *unit_name = StringVal("unit") ;
    if (unit_name) {
        return vhdl_file::Save(lib_name, unit_name) ;
    }

    // If module name is specified, attempt Verilog binary save
    const char *module_name = StringVal("module") ; // Module name is optional
    if (module_name) {
        return veri_file::Save(lib_name, module_name) ;
    }

    // At this point neither module name nor unit name has been specified.  We need to do
    // a library check before attempting to binary save the entiry library.
    // (Yes, it's possible to do an entire library save for both VHDL and Verilog, if they
    // have the library by the specified name.)

    unsigned ret_val = 0 ;
    if (vhdl_file::GetLibrary(lib_name)) {
        // VIPER #6895: If no vhdl unit then no need to save restore
        VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(lib_name) ;
        if(vhdl_lib && vhdl_lib->NumOfPrimUnits()) {
            // Since a VHDL library exists by the name
            ret_val = vhdl_file::Save(lib_name, unit_name) ;
            if (!ret_val) return 0 ;
        }
    }
    if (veri_file::GetLibrary(lib_name)) {
        ret_val = veri_file::Save(lib_name, module_name) ;
    }

    return ret_val; // 1=Pass, 0=Fail
}

// -----------------------------------------

// -----------------------------------------

// VIPER #6813:
/********************************************************************/
/*  Generate Explicit State Machine                                 */
/********************************************************************/
// Constructor for the 'convert_implicit_state_machine' command
ComExplicitStateMachine::ComExplicitStateMachine() :
    Command("convert_implicit_state_machine","convert implicit VHDL/Verilog state machines to explicit state machine")
{
    Add(new StringArg("lib","(work) library to take implicit state machine from", 1)) ;
    Add(new StringArg("unit","convert implicit state machines only of this VHDL primary unit",1/* optional group*/,1)) ;
    Add(new StringArg("module","convert implicit state machines only of this Verilog module",1/* optional group*/,1)) ;
    Add(new BoolArg("reset","switch to generate a reset block that executes during initializations at time 0 before the first clock edge")) ;
}

// Destructor for the 'convert_implicit_state_machine' command
ComExplicitStateMachine::~ComExplicitStateMachine() {}

// Execute command 'convert_implicit_state_machine'
unsigned
ComExplicitStateMachine::Process(Tcl_Interp * /*interp*/)
{
    const char *unit_name = StringVal("unit") ;
    const char *module_name = StringVal("module") ;
    const char *lib_name = StringVal("lib") ;
    if (!lib_name) {
        lib_name = "work" ;
    }

    unsigned reset = (BoolVal("reset")) ? 1 : 0 ;

    unsigned result = 0 ;
    unsigned proceed = 1 ;

    if (unit_name) {
        result = vhdl_file::GenerateExplicitStateMachines(unit_name, lib_name, reset) ;
        proceed = 0 ;
    }

    if (proceed && module_name) {
        result = veri_file::GenerateExplicitStateMachines(module_name, lib_name, reset) ;
        proceed = 0 ;
    }

    if (proceed) {
        result = vhdl_file::GenerateExplicitStateMachines(0, lib_name, reset) ;
        result += veri_file::GenerateExplicitStateMachines(0, lib_name, reset) ;
    }

    return (Message::ErrorCount()) ? 0 : result ;
}

// -----------------------------------------

