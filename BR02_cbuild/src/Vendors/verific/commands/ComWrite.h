/*
 *
 * [ File Version : 1.43 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_COMWRITE_H_
#define _VERIFIC_COMWRITE_H_

#include "Commands.h" /* For format enum */

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComPrettyPrint : public Command
{
public:
    ComPrettyPrint();
    virtual ~ComPrettyPrint();

private:
    // Prevent compiler from defining the following
    ComPrettyPrint(const ComPrettyPrint &) ;            // Purposely leave unimplemented
    ComPrettyPrint& operator=(const ComPrettyPrint &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
private:
} ; // class ComPrettyPrint

/* -------------------------------------------------------------- */

// Binary save of a parse-tree
class VFC_DLL_PORT ComSave : public Command
{
public :
    ComSave();
    ~ComSave();

private:
    // Prevent compiler from defining the following
    ComSave(const ComSave &) ;            // Purposely leave unimplemented
    ComSave& operator=(const ComSave &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *pTclInterp);
} ; // class ComSave

/* -------------------------------------------------------------- */

// VIPER #6813:
/********************************************************************/
/*  Generate Explicit State Machine                                 */
/********************************************************************/

class VFC_DLL_PORT ComExplicitStateMachine : public Command
{
public:
    ComExplicitStateMachine();
    virtual ~ComExplicitStateMachine();

private:
    // Prevent compiler from defining the following
    ComExplicitStateMachine(const ComExplicitStateMachine &) ;            // Purposely leave unimplemented
    ComExplicitStateMachine& operator=(const ComExplicitStateMachine &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
private:
} ; // class ComExplicitStateMachine

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_COMWRITE_H_

