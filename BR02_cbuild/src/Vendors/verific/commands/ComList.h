/*
 *
 * [ File Version : 1.51 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_COMLIST_H_
#define _VERIFIC_COMLIST_H_

#include "Commands.h" /* For Command */

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Netlist ;
class Map ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComGetVhdlLibraryPath : public Command
{
public:
    ComGetVhdlLibraryPath();
    virtual ~ComGetVhdlLibraryPath();

private:
    // Prevent compiler from defining the following
    ComGetVhdlLibraryPath(const ComGetVhdlLibraryPath &) ;            // Purposely leave unimplemented
    ComGetVhdlLibraryPath& operator=(const ComGetVhdlLibraryPath &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComGetVhdlLibraryPath

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComSetVhdlLibraryPath : public Command
{
public:
    ComSetVhdlLibraryPath();
    virtual ~ComSetVhdlLibraryPath();

private:
    // Prevent compiler from defining the following
    ComSetVhdlLibraryPath(const ComSetVhdlLibraryPath &) ;            // Purposely leave unimplemented
    ComSetVhdlLibraryPath& operator=(const ComSetVhdlLibraryPath &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComSetVhdlLibraryPath

/* -------------------------------------------------------------- */
class VFC_DLL_PORT ComGetVeriLibraryPath : public Command
{
public:
    ComGetVeriLibraryPath();
    virtual ~ComGetVeriLibraryPath();

private:
    // Prevent compiler from defining the following
    ComGetVeriLibraryPath(const ComGetVeriLibraryPath &) ;            // Purposely leave unimplemented
    ComGetVeriLibraryPath& operator=(const ComGetVeriLibraryPath &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComGetVeriLibraryPath

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComSetVeriLibraryPath : public Command
{
public:
    ComSetVeriLibraryPath();
    virtual ~ComSetVeriLibraryPath();

private:
    // Prevent compiler from defining the following
    ComSetVeriLibraryPath(const ComSetVeriLibraryPath &) ;            // Purposely leave unimplemented
    ComSetVeriLibraryPath& operator=(const ComSetVeriLibraryPath &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComSetVeriLibraryPath

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComSetMsgType : public Command
{
public:
    ComSetMsgType();
    virtual ~ComSetMsgType();

private:
    // Prevent compiler from defining the following
    ComSetMsgType(const ComSetMsgType &) ;            // Purposely leave unimplemented
    ComSetMsgType& operator=(const ComSetMsgType &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComSetMsgType

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComClearMsgType : public Command
{
public:
    ComClearMsgType();
    virtual ~ComClearMsgType();

private:
    // Prevent compiler from defining the following
    ComClearMsgType(const ComClearMsgType &) ;            // Purposely leave unimplemented
    ComClearMsgType& operator=(const ComClearMsgType &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComClearMsgType

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComVhdlRegister : public Command
{
public:
    ComVhdlRegister() ;
    virtual ~ComVhdlRegister() ;

private:
    // Prevent compiler from defining the following
    ComVhdlRegister(const ComVhdlRegister &) ;            // Purposely leave unimplemented
    ComVhdlRegister& operator=(const ComVhdlRegister &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComVhdlRegister

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComVhdlClearRegistry : public Command
{
public:
    ComVhdlClearRegistry() ;
    virtual ~ComVhdlClearRegistry() ;

private:
    // Prevent compiler from defining the following
    ComVhdlClearRegistry(const ComVhdlClearRegistry &) ;            // Purposely leave unimplemented
    ComVhdlClearRegistry& operator=(const ComVhdlClearRegistry &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComVhdlClearRegistry

/* -------------------------------------------------------------- */

class VFC_DLL_PORT ComListSortedVhdlFiles : public Command
{
public:
    ComListSortedVhdlFiles() ;
    virtual ~ComListSortedVhdlFiles() ;

private:
    // Prevent compiler from defining the following
    ComListSortedVhdlFiles(const ComListSortedVhdlFiles &) ;            // Purposely leave unimplemented
    ComListSortedVhdlFiles& operator=(const ComListSortedVhdlFiles &) ; // Purposely leave unimplemented

public:
    unsigned Process(Tcl_Interp *interp) ;
} ; // class ComVhdlClearRegistry

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_COMLIST_H_

