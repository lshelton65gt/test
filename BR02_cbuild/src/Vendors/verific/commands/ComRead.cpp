/*
 *
 * [ File Version : 1.218 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>

#include "Array.h"
#include "Commands.h"
#include "ComRead.h"

#include "Strings.h"
#include "Message.h"
#include "FileSystem.h"
#include "TextBasedDesignMod.h" // for cleanup
#include "RuntimeFlags.h"       // for cleanup

// For binary save/restore :
#include "SaveRestore.h"

// For regular formats
#include "veri_file.h"
#include "VeriLibrary.h"
#include "VeriModule.h"
#include "VeriTreeNode.h"
#include "vhdl_file.h"
#include "vhdl_sort.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**************************/
/* Create command ComPreProcess */
/**************************/

ComPreProcess::ComPreProcess() : Command("preprocess", "preprocess Verilog file(s), writes output to another file")
{
    Add(new ArrayArg(0, "<file_name> <file_name>...", 0 /*not optional*/, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new StringArg("out_file", "preprocessed output file", 0 /*not optional*/)) ;
    Add(new ArrayArg("file", "verilog -f command file", 1, -1)) ; // non-optional but alternative options allowed
    Add(new ArrayArg("File", "verilog -F command file", 1, -1)) ; // non-optional but alternative options allowed
    Add(new ArrayArg("f", "verilog -f command file", 1, -1)) ; // non-optional but alternative options allowed
    Add(new BoolArg("verilog_95","read in Verilog '95 mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("verilog_2000","read in Verilog 2001 mode. this is the default",1,2)) ; // Verilog language switches are in group 2, optional and exclusive (can't have multiple different settings)
    Add(new BoolArg("sysv_2005","read in System Verilog 2005 mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("sysv_2009","read in System Verilog 2009 mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("sysv","read in System Verilog mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("sfcu","single file compilation unit, each file in command line is a separate compilation unit. This is the default", 1, 2)) ;
    Add(new BoolArg("mfcu","multi-file compilation unit, all files in command line make up a compilation unit. Default is sfcu", 1, 2)) ;
    Add(new ArrayArg("incdir","Verilog XL compatibility ; find include files in these directories")) ;
    Add(new ArrayArg("define","Verilog macro definition: 'name=value' or 'value'")) ;
    Add(new ArrayArg("veri1995ext","extension of files to be read in Verilog 1995 mode")) ;
    Add(new ArrayArg("veri2000ext","extension of files to be read in Verilog 2001 mode")) ;
    Add(new ArrayArg("sysveri2005ext","extension of files to be read in SystemVerilog 2005 mode")) ;
    Add(new ArrayArg("sysveri2009ext","extension of files to be read in SystemVerilog 2009 mode")) ;
    Add(new ArrayArg("sysveriext","extension of files to be read in SystemVerilog mode")) ;
}

ComPreProcess::~ComPreProcess() {}

unsigned
ComPreProcess::Process(Tcl_Interp* /*interp*/)
{
    unsigned verilog_mode = veri_file::VERILOG_2K ; // default is 'verilog_2000'
    unsigned explicit_mode = 0 ; // flag if mode is explicitly set
    unsigned cu_mode = veri_file::NO_MODE ;
    if (BoolVal("verilog_95"))   { explicit_mode = 1 ; verilog_mode = veri_file::VERILOG_95 ; } // explicit 95 mode
    if (BoolVal("verilog_2000")) { explicit_mode = 1 ; verilog_mode = veri_file::VERILOG_2K ; } // explicit 2000 mode
    if (BoolVal("sysv_2005"))    { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG_2005 ; } // Explicit System Verilog 2005 mode
    if (BoolVal("sysv_2009"))    { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG_2009 ; } // Explicit System Verilog 2009 mode
    if (BoolVal("sysv"))         { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG ; } // Explicit System Verilog mode
    if (BoolVal("mfcu"))         { cu_mode = veri_file::MFCU ; } // multi-file compilation unit
    if (BoolVal("sfcu"))         { cu_mode = veri_file::SFCU ; } // single-file compilation unit

    unsigned i ;
    const char *ext ;
    Array *file_ext = ArrayVal("veri1995ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::VERILOG_95) ;
    file_ext = ArrayVal("veri2000ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::VERILOG_2K) ;
    file_ext = ArrayVal("sysveri2005ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG_2005) ;
    file_ext = ArrayVal("sysveri2009ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG_2009) ;
    file_ext = ArrayVal("sysveriext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG) ;

    // Process -incdir : Set search dirs for unknown modules
    veri_file::RemoveAllIncludeDirs() ; // First clear existing (static) incdirs
    Array *incdirs = ArrayVal("incdir") ; // Get -incdir directory names
    char *dir_name ;
    FOREACH_ARRAY_ITEM(incdirs, i, dir_name) {
        veri_file::AddIncludeDir(dir_name) ;
    }

    // Process the -define option
    Array *defines = ArrayVal("define") ;
    if (defines) {
        char *g, *v, *f ;
        FOREACH_ARRAY_ITEM(defines, i, g) {
            // Find if it is of the form name=value :
            v = ::strchr(g,'=') ;
            if (v) {
                f = g ; // name of the macro
                *v++ = '\0' ; // Kill off the '=' character, let v point to the value after it.
                (void) veri_file::DefineCmdLineMacro(f, v) ; // Separate treatment for user defined macro (VIPER #2407)
            } else {
                f = 0 ; // just name, no value
                v = g ;
                (void) veri_file::DefineCmdLineMacro(v) ; // Separate treatment for user defined macro (VIPER #2407)
            }
        }
    }

    // Now read in all the files
    Array *file_names = ArrayVal(0) ; // Get the file name
    const char *out_file = StringVal("out_file") ; // The preprocessed output file name

    Array *owned_f_files = 0 ;
    unsigned f_analysis_mode = veri_file::UNDEFINED ; // Not used in preprocessing
    unsigned files_size = (file_names) ? file_names->Size() : 0 ;

    Array *f_file_names = ArrayVal("file") ;
    const char *f_file ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    f_file_names = ArrayVal("f") ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    f_file_names = ArrayVal("File") ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_CAPITAL, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    if ((f_analysis_mode != veri_file::UNDEFINED) && !explicit_mode) {
        verilog_mode = f_analysis_mode ;
        explicit_mode = 1 ;
    }

    veri_file::SetDefaultAnalysisMode(verilog_mode) ;
    if (!explicit_mode) {
        veri_file::AddFileExtMode(".sv", veri_file::SYSTEM_VERILOG) ;
        veri_file::AddFileExtMode(".sysv", veri_file::SYSTEM_VERILOG) ;
    }

    if (file_names && (file_names->Size()==1)) {
        const char *file_name = (const char *)file_names->GetFirst() ;
        (void) veri_file::PreProcess(file_name, out_file, verilog_mode) ;
    } else {
        (void) veri_file::PreProcessMultipleFiles(file_names, out_file, verilog_mode, cu_mode) ;
    }

    while (file_names && (file_names->Size() > files_size)) Strings::free((char *)file_names->RemoveLast()) ;
    delete owned_f_files ; // 'file_names' may be corrupted below this line
    veri_file::ClearAllFileExt() ; // Clear all mapped file extensions

    return (Message::ErrorCount()) ? 0 : 1 ;
}

/**************************/
/* Create command Analyze */
/**************************/

ComAnalyze::ComAnalyze() : Command("analyze","read a design from a file, don't execute the modules")
{
    Add(new ArrayArg(0,"<file_name> <file_name>..", 0 /*not optional*/, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new StringArg("format","file format (vhdl,verilog..)")) ;
    Add(new StringArg("work","work library to compile in")) ;

    Add(new ArrayArg("file", "verilog -f command file", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new ArrayArg("File", "verilog -F command file", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new ArrayArg("f", "verilog -f command file", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new ArrayArg("L", "search library for imported design units")) ; // VIPER #5618: -L option: library search order in 'import'
    // VIPER #4115: File extension to language dialect selection:
    Add(new ArrayArg("veri1995ext","extension of files to be read in Verilog 1995 mode")) ;
    Add(new ArrayArg("veri2000ext","extension of files to be read in Verilog 2001 mode")) ;
    Add(new ArrayArg("sysveri2005ext","extension of files to be read in SystemVerilog 2005 mode")) ; // VIPER #5965
    Add(new ArrayArg("sysveri2009ext","extension of files to be read in SystemVerilog 2009 mode")) ;
    Add(new ArrayArg("sysveriext","extension of files to be read in SystemVerilog mode")) ;

    // VHDL related options
    Add(new BoolArg("vhdl_87","read in VHDL 87 mode. Default is VHDL 93",1,4)) ; // VHDL language switches are in group 4, optional and exclusive (can't have multiple different settings)
    Add(new BoolArg("vhdl_2k","read in VHDL 2000 mode. Default is VHDL 93",1,4)) ;
    Add(new BoolArg("vhdl_2008","read in VHDL 2008 mode. Default is VHDL 93",1,4)) ;
    Add(new BoolArg("vhdl_sort", "analyze registered VHDL files in correct order", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative

    // Verilog related options
    // Flavor : part of 'optional', 'mutually-exclusive' group.
    Add(new BoolArg("verilog_2000","read in Verilog 2001 mode. this is the default",1,2)) ; // Verilog language switches are in group 2, optional and exclusive (can't have multiple different settings)
    Add(new BoolArg("verilog_95","read in Verilog '95 mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("sysv_2005","read in System Verilog 2005 mode. Default is Verilog 2001",1,2)) ; // VIPER #5965
    Add(new BoolArg("sysv_2009","read in System Verilog 2009 mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("sysv","read in System Verilog mode. Default is Verilog 2001",1,2)) ;
    Add(new BoolArg("sfcu","single file compilation unit, each file in command line is a separate compilation unit. This is the default",1,3)) ;
    Add(new BoolArg("mfcu","multi-file compilation unit, all files in command line make up a compilation unit. Default is sfcu",1,3)) ;

    Add(new ArrayArg("v","Verilog XL compatibility ; find unknown modules in these files")) ;
    Add(new ArrayArg("y","Verilog XL compatibility ; find unknown modules in these directories")) ;
    Add(new BoolArg("u","Verilog XL compatibility ; upper-case all Verilog identifiers")) ;
    Add(new ArrayArg("incdir","Verilog XL compatibility ; find include files in these directories")) ;
    Add(new ArrayArg("libext","Verilog file extensions for -y -v option.")) ;
    Add(new BoolArg("librescan", "Verilog search unknown modules always starting with first library.")) ; // VIPER 2132 : add -librescan option
    Add(new ArrayArg("define","Verilog macro definition: 'name=value' or 'name'")) ;
}

ComAnalyze::~ComAnalyze() {}

unsigned
ComAnalyze::Process(Tcl_Interp* /*interp*/)
{
    unsigned i ;
    const char *file_name ;

    const char *work_lib = StringVal("work") ;

    // Verilog analysis

    // Set language mode.
    unsigned verilog_mode = veri_file::VERILOG_2K ; // default is 'verilog_2000'
    unsigned explicit_mode = 0 ; // flag if mode is explicitly set
    unsigned cu_mode = veri_file::NO_MODE ;  // Track compilation unit mode
    if (BoolVal("verilog_95"))   { explicit_mode = 1 ; verilog_mode = veri_file::VERILOG_95 ; } // explicit 95 mode
    if (BoolVal("verilog_2000")) { explicit_mode = 1 ; verilog_mode = veri_file::VERILOG_2K ; } // explicit 2000 mode
    if (BoolVal("sysv_2005"))    { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG_2005 ; } // VIPER #5965: explicit System Verilog 2005 mode
    if (BoolVal("sysv_2009"))    { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG_2009 ; } // explicit System Verilog 2009 mode
    if (BoolVal("sysv"))         { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG ; } // explicit System Verilog mode
    if (BoolVal("mfcu"))         { cu_mode = veri_file::MFCU ; } // multi-file compilation unit
    if (BoolVal("sfcu"))         { cu_mode = veri_file::SFCU ; } // single-file compilation unit

    // VIPER #4115: File extension to language dialect selection:
    const char *ext ;
    Array *file_ext = ArrayVal("veri1995ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::VERILOG_95) ;
    file_ext = ArrayVal("veri2000ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::VERILOG_2K) ;
    file_ext = ArrayVal("sysveri2005ext") ; // VIPER #5965
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG_2005) ;
    file_ext = ArrayVal("sysveri2009ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG_2009) ;
    file_ext = ArrayVal("sysveriext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG) ;

    // Verilog XL complience options
    // All these are static information, so they survive multiple file reads.
    // Optionally clear some of this static info :
    //    veri_file::RemoveAllYDirsVFiles() ; // First clear existing (static) Ydirs/Vfile info
    //    veri_file::RemoveAllIncludeDirs() ; // Clear out include (search) directories
    //    veri_file::UndefineAllMacros() ; // Clear out all Verilog macro settings (from previous files)
    //    veri_file::RemoveAllModules() ;  // Delete all (previously analyzed modules) parse trees
    // Process the -v option if present
    // these are the file names
    Array *v_option = ArrayVal("v") ;
    if (v_option) {
        FOREACH_ARRAY_ITEM(v_option, i, file_name) {
            veri_file::AddVFile(file_name) ;
        }
    }

    // Process the -y options
    // these are the dir names
    Array *y_option = ArrayVal("y") ; // Get -y directory names
    if (y_option) {
        char * dir_name = 0 ;
        FOREACH_ARRAY_ITEM(y_option, i, dir_name) {
            veri_file::AddYDir(dir_name) ;
        }
    }

    // Process the -libext option
    // These are the file extentions
    Array *lib_ext = ArrayVal("libext") ;
    if (lib_ext) {
        char *ext_name = 0 ;
        unsigned l = 0 ;
        FOREACH_ARRAY_ITEM(lib_ext, l, ext_name) {
            veri_file::AddLibExt(ext_name) ;
        }
    }

    // Process the -librescan option
    if (BoolVal("librescan")) veri_file::SetLibReScan() ; // Set -librescan option for -y/-v processing (VIPER 2132)

    // Process -u option :
    if (BoolVal("u")) {
        veri_file::SetUpperCaseIds(1) ;
    } else {
        veri_file::SetUpperCaseIds(0) ;
    }

    // Process -incdir : Set search dirs for unknown modules
    veri_file::RemoveAllIncludeDirs() ; // First clear existing (static) incdirs
    Array *incdirs = ArrayVal("incdir") ; // Get -incdir directory names
    char *dir_name ;
    FOREACH_ARRAY_ITEM(incdirs, i, dir_name) {
        veri_file::AddIncludeDir(dir_name) ;
    }

    // Process the -define option
    Array *defines = ArrayVal("define") ;
    if (defines) {
        char *g, *v, *f ;
        FOREACH_ARRAY_ITEM(defines, i, g) {
            // Find if it is of the form name=value :
            v = ::strchr(g,'=') ;
            if (v) {
                f = g ; // name of the macro
                *v++ = '\0' ; // Kill off the '=' character, let v point to the value after it.
                (void) veri_file::DefineCmdLineMacro(f, v) ; // Separate treatment for user defined macro (VIPER #2407)
            } else {
                f = 0 ; // just name, no value
                v = g ;
                (void) veri_file::DefineCmdLineMacro(v) ; // Separate treatment for user defined macro (VIPER #2407)
            }
        }
    }

    // VHDL analysis
    unsigned vhdl_mode = vhdl_file::VHDL_93 ;
    if (BoolVal("vhdl_87")) vhdl_mode = vhdl_file::VHDL_87 ;
    if (BoolVal("vhdl_2k")) vhdl_mode = vhdl_file::VHDL_2K ;
    if (BoolVal("vhdl_2008")) vhdl_mode = vhdl_file::VHDL_2008 ;

    // VIPER #2762/#5618: Add -L option specified libraries
    // Clear out all -L options specified for previous elaboration
    veri_file::RemoveAllLOptions() ; // Clean out all -L options from verilog

    // VIPER #2762/#5618: Add -L option specified libraries
    Array *ordered_libs = ArrayVal("L") ;
    if (ordered_libs) {
        char* ordered_lib_name ;
        FOREACH_ARRAY_ITEM(ordered_libs, i, ordered_lib_name) {
            if (!ordered_lib_name) continue ;
            veri_file::AddLOption(ordered_lib_name) ;
        }
    }

    // Now read in all the files
    Array *file_names = ArrayVal(0) ; // Get the file name
    Array *owned_f_files = 0 ;
    unsigned f_analysis_mode = veri_file::UNDEFINED ;
    unsigned files_size = (file_names) ? file_names->Size() : 0 ;

    Array *f_file_names = ArrayVal("file") ;
    const char *f_file ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    f_file_names = ArrayVal("f") ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    f_file_names = ArrayVal("File") ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_CAPITAL, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    if ((f_analysis_mode != veri_file::UNDEFINED) && !explicit_mode) {
        explicit_mode = 1 ;
        verilog_mode = f_analysis_mode ;
    }
    // VIPER #5434 & #5469: Set default analysis mode and insert .sv files as SystemVerilog:
    veri_file::SetDefaultAnalysisMode(verilog_mode) ;
    if (!explicit_mode) {
        // VIPER #5469: In case of no explicit mode specification, treat .sv/.sysv files as SV file:
        veri_file::AddFileExtMode(".sv", veri_file::SYSTEM_VERILOG) ;
        veri_file::AddFileExtMode(".sysv", veri_file::SYSTEM_VERILOG) ;
    }

    // Check whether the vhdl_sort option is specified
    unsigned vhdl_sorter = BoolVal("vhdl_sort") ;

    // VIPER #2932: Analyze multiple files using single API.
    Array *veri_files = 0 ; // Contains the names of the verilog files to be analyzed
    FOREACH_ARRAY_ITEM(file_names, i, file_name) {
        // For each new file, determine the format
        const char *format_string = StringVal("format") ;
        format_enum format = FORM_UNKNOWN ;
        if (!format_string) {
            // Derive format from file extension
            format = FormatFromFileName(file_name) ;
            if ((format==FORM_UNKNOWN) && explicit_mode) format = FORM_VERILOG ; // Use verilog mode
            if (format==FORM_UNKNOWN) {
                Error("%s: cannot derive format from file %s. Use -format option", Name(), file_name) ;
                continue ;
            }
        } else {
            // Take format as-is
            format = Format(format_string) ;
            if (format==FORM_UNKNOWN) {
                Error("%s: unknown format '%s'", Name(), format_string) ;
                continue ;
            }
        }

        if (format == FORM_SYS_VERILOG) {
            // format setting is System Verilog. Indicate that
            format = FORM_VERILOG ;
            if (!explicit_mode) verilog_mode = veri_file::SYSTEM_VERILOG ; // set System Verilog mode if no explicitly set before.
        }

        switch (format) {
        case FORM_VERILOG:
            // VIPER #2932: Use API for multiple file analysis
            //(void) veri_file::Analyze(file_name, verilog_mode, work_lib) ;
            // create new array containing the verilog files
            if (!veri_files) veri_files = new Array() ;
            veri_files->InsertLast(file_name) ;
            break ;

        case FORM_VHDL:
            if (vhdl_sorter) {
                // Register any filename specified on command line
                (void) vhdl_sort::RegisterFile(file_name, work_lib, vhdl_mode) ;
                break ;
            }
            (void) vhdl_file::Analyze(file_name, work_lib, vhdl_mode) ;
            break ;

        default:
            Error("%s: cannot read format %s in this product", Name(), Format(format)) ;
            break ;
        }
    }

    // VIPER #2932. Call API to analyze multiple files.
    if (veri_files) {
        if (veri_files->Size()) {
            if (veri_files->Size() > 1) {
                (void) veri_file::AnalyzeMultipleFiles(veri_files, verilog_mode, work_lib, cu_mode) ;
            }else { // For analysis of a single file call Analyze routine
                char *file = (char*) veri_files->At(0) ;
                (void) veri_file::Analyze(file, verilog_mode, work_lib, cu_mode) ;
                // For sfcu, compilation unit must be ended before user library processing
                if (veri_file::IsSingleFileCompilationUnitMode()) veri_file::EndCompilationUnit() ;
            }
            veri_file::ProcessUserLibraries() ; // Process user libraries

            // VIPER #3825 : Remove include directories now. It is not part of
            // 'EndCompilationUnit'
            veri_file::RemoveAllIncludeDirs() ; // Remove the include directory
            // End compilation unit
            veri_file::EndCompilationUnit() ;
        }
        delete veri_files ; // clean up
    }

    if (vhdl_sorter) {
        const char *analyze_lib ;
        unsigned local_vhdl_mode ;
        unsigned default_mode = (vhdl_mode==vhdl_file::VHDL_93) ? 1 : 0 ; // Default mode in this command
        FOREACH_SORTED_FILE(i, file_name, analyze_lib, local_vhdl_mode) {
            // Analyze this file from the sorted list (use the specific mode if given, otherwise use the default mode):
            (void) vhdl_file::Analyze(file_name, analyze_lib, (default_mode)?local_vhdl_mode:vhdl_mode) ;
        }

        // Need to clear registered files, since they have already been analyzed.
        vhdl_sort::ClearRegisteredFiles() ;
    }

    // VIPER #2726: Clear the library constructs, it has been applied for this session
    veri_file::ClearAllLibraryDecls() ;
    // VIPER #4356: Undefine all command line macros after the command is processed:
    veri_file::UndefineCmdLineMacros() ;
    // VIPER #5434: Reset default analysis mode:
    veri_file::SetDefaultAnalysisMode(veri_file::UNDEFINED) ;
    veri_file::RemoveAllLOptions() ; // Clean out all -L options from verilog

    while (file_names && (file_names->Size() > files_size)) Strings::free((char *)file_names->RemoveLast()) ;
    delete owned_f_files ; // 'file_names' may be corrupted below this line
    veri_file::ClearAllFileExt() ; // Clear all mapped file extensions

    return (Message::ErrorCount()) ? 0 : 1 ;
}

/******************************/
/* Create command 'elaborate' */
/******************************/

ComElaborate::ComElaborate() : Command("elaborate","elaborate (execute) a VHDL or Verilog module")
{
    Add(new StringArg(0,"name of the top-level module (default is elaborate all modules)",1,1)) ;
    Add(new StringArg("work","(work) library where the unit is analyzed")) ;
    Add(new ArrayArg("L","search library for instantiated design units")) ; // VIPER #2762 : -L option : library search order

    Add(new ArrayArg("module","explicitly elaborate Verilog modules",1,-1)) ;

    // VIPER #7727: Allow multiple units
    Add(new ArrayArg("unit","explicitly elaborate VHDL units",1,-1)) ;
    Add(new StringArg("architecture","optional VHDL architecture name")) ;

    Add(new ArrayArg("generic","top-level generic parameter association : 'name=constant' or 'constant'")) ;

    Add(new BoolArg("static","do static elaboration (not RTL elaboration)", 1, 3)) ;
}

ComElaborate::~ComElaborate() {}

unsigned
ComElaborate::Process(Tcl_Interp* /*interp*/)
{
    const char *arg = StringVal(0) ; // unit name

    Array *modules = ArrayVal("module") ;

    Array *units = ArrayVal("unit") ;
    //const char *unit = StringVal("unit") ;
    const char *unit = (units && units->Size()) ? (const char*)units->At(0): 0 ;
    const char *arch = StringVal("architecture") ;

    const char *lib_name = StringVal("work") ;

    Array *generics = ArrayVal("generic") ;

    unsigned static_elab = 1 ; // default - always on for a static-elab-only product config

    unsigned hier_tree_elab = 0 ;

    Map *generic_map = 0 ;
    if (generics) {
        // Create a Map of the name-value associations :
        generic_map = new Map(STRING_HASH_CASE_INSENSITIVE) ;
        unsigned i ;
        char *g, *v, *f ;
        FOREACH_ARRAY_ITEM(generics, i, g) {
            // Find if it is name-associated or not :
            v = ::strchr(g,'=') ;
            if (v) {
                f = g ; // name of the generic
                *v++ = '\0' ; // Kill off the '=' character, let v point to the value after it.
            } else {
                f = 0 ; // its order-based association
                v = g ;
            }

            // Insert in the map :
            (void) generic_map->Insert(f, v, 0, 1) ; // Force insert (for f==0 items)
        }
    }

    vhdl_file::RemoveAllLOptions() ; // Clean out all -L options from vhdl

    veri_file::RemoveAllLOptions() ; // Clean out all -L options from verilog

    // VIPER #2762 : Add -L option specified libraries
    Array *ordered_libs = ArrayVal("L") ;
    if (ordered_libs) {
        unsigned i ;
        char* ordered_lib_name ;
        FOREACH_ARRAY_ITEM(ordered_libs, i, ordered_lib_name) {
            if (!ordered_lib_name) continue ;
            veri_file::AddLOption(ordered_lib_name) ;
            vhdl_file::AddLOption(ordered_lib_name) ;
        }
    }

    // VIPER #3910: Always clear out list of top modules from veri_file if exists from previous elaboration
    veri_file::ClearTopModuleList() ;

    unsigned result = 0 ;
    unsigned proceed = 1 ; // Need this variable because of Verific's preprocessing flow

    Array top_modules(((modules)?modules->Size():0)+1) ;
    Array top_units(((units)?units->Size():0)+1) ;
    if (static_elab || hier_tree_elab) {
        unsigned i ;
        VeriTreeNode veri_tmp_print_node ;
        veri_tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        VeriModule *mod ;
        const char *module ;
        FOREACH_ARRAY_ITEM(modules, i, module) {
            mod = veri_file::GetModule(module, 1 /* case sensitive */, (lib_name)?lib_name:"work") ;
            if (mod) {
                top_modules.InsertLast(mod) ;
            } else {
                veri_tmp_print_node.Comment("module %s in library %s is not yet analyzed", module, (lib_name)?lib_name:"work") ;
                proceed = 0 ;
            }
        }
        VhdlPrimaryUnit *top_unit_ptr ;
        VhdlLibrary *lib = vhdl_file::GetLibrary(lib_name ? lib_name: "work", 1) ;
        VhdlTreeNode vhdl_tmp_print_node ;
        vhdl_tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        const char *top_unit ;
        FOREACH_ARRAY_ITEM(units, i, top_unit) {
            top_unit_ptr = lib ? lib->GetPrimUnit(top_unit): 0 ;
            if (top_unit_ptr) {
                VhdlIdDef *unit_id = top_unit_ptr->Id() ;
                if (unit_id && !unit_id->IsEntity() && !unit_id->IsConfiguration()) {
                    vhdl_tmp_print_node.Comment("unit %s is not an entity or configuration", unit_id->Name()) ;
                    proceed = 0 ;
                } else {
                    top_units.InsertLast(top_unit_ptr) ;
                }
            } else {
                vhdl_tmp_print_node.Comment("unit %s is not yet analyzed", top_unit) ;
                proceed = 0 ;
            }
        }

        // Find the module/unit specific to the default argument also:
        mod = veri_file::GetModule(arg, 1 /* case sensitive */, (lib_name)?lib_name:"work") ;
        if (mod) top_modules.InsertLast(mod) ;
        top_unit_ptr = (lib && arg) ? lib->GetPrimUnit(arg) : 0 ;
        if (top_unit_ptr) {
            VhdlIdDef *unit_id = top_unit_ptr->Id() ;
            if (unit_id && !unit_id->IsEntity() && !unit_id->IsConfiguration()) {
                vhdl_tmp_print_node.Comment("unit %s is not an entity or configuration", unit_id->Name()) ;
                top_unit_ptr = 0 ;
            } else {
                top_units.InsertLast(top_unit_ptr) ;
            }
        }
        if (arg
           && !mod
           && !top_unit_ptr
           ) {
            vhdl_tmp_print_node.Comment("unit %s is not an entity or configuration", arg) ;
            veri_tmp_print_node.Comment("module %s in library %s is not yet analyzed", arg, (lib_name)?lib_name:"work") ;
            proceed = 0 ; // Could not find any of verilog module or vhdl unit
        }

        if (hier_tree_elab && !arg
            && !units
            && !modules
            ) {
            // CHECKME: Use vhdl_file::TopUnit(lib_name?lib_name:"work") too?
#if 0
            // No argument given, similar to ElaborateAll
            VhdlPrimaryUnit *punit ;
            MapIter mi ;
            FOREACH_VHDL_PRIMARY_UNIT(lib, mi, punit) {
                if (punit->IsElaborated()) continue ;
                VhdlIdDef *id = punit->Id() ;

                if (!id->IsEntity() && !id->IsConfiguration()) continue ;
                if (id->IsEntity() && punit->NumOfSecondaryUnits()==0) continue ;

                if (punit->HasUninitializedGenerics()) continue ;
                if (punit->HasUnconstrainedPorts()) continue ;

                top_units.InsertLast(punit) ;
            }
#endif
            // No argument given, take Verilog top level modules only:
            Array *tops = veri_file::GetTopModules((lib_name)?lib_name:"work") ;
            if (tops || top_units.Size()) {
                if (!tops) tops = new Array(1) ;
                // Add root module in top module list to consider items declared in root
                // module but after module definitions
                MapIter mii ;
                VeriModule *root_module ;
                VeriLibrary *work_library = veri_file::GetLibrary((lib_name)?lib_name:"work") ;
                FOREACH_VERILOG_MODULE_IN_LIBRARY(work_library, mii, root_module) {
                    if (!root_module || !root_module->IsRootModule()) continue ;
                    tops->InsertFirst(root_module) ;
                }
                top_modules.Append(tops) ;
                delete tops ;
            }
        }
    }

#ifdef VERILOG_DUAL_LANGUAGE_ELABORATION
    // VIPER #7727 : If both verilog modules and vhdl units are specified
    // as top level units in static elaboration mode, elaborate all units
    if (static_elab && top_modules.Size() && top_units.Size()) {
        if (proceed) {
            Array ret_mods(top_modules.Size()) ;
            Array ret_units(top_units.Size()) ;
            // Elaborate multiple top levels
            (void) veri_file::ElaborateMixedHdlMultipleTopsStatic(&top_modules, &top_units, generic_map, &ret_mods, &ret_units) ;
            VhdlLibrary *lib = vhdl_file::GetLibrary(lib_name ? lib_name: "work", 1) ;
            if (lib) lib->DeleteElaboratedBindings() ;
            result = (Message::ErrorCount()) ? 0 : 1 ;
            proceed = 0 ;
        }
    }
#endif

    if (proceed && unit) {
        // Explicitly use a VHDL unit
        result = vhdl_file::Elaborate(unit, lib_name, arch, generic_map, static_elab) ;
        proceed = 0 ;
    }

    if (proceed && modules && modules->Size()) {
        // Explicitly use a Verilog unit
        const char *module ;
        if (modules->Size() == 1) {
            module = (const char *)modules->GetFirst() ;
            if (static_elab) {
                result = veri_file::ElaborateStatic(module, lib_name, generic_map) ;
            } else {
            }
        } else {
            Array top_mods(modules->Size()) ;
            unsigned i ;
            VeriModule *mod ;
            FOREACH_ARRAY_ITEM(modules, i, module) {
                mod = veri_file::GetModule(module, 1 /* case sensitive */, (lib_name)?lib_name:"work") ;
                if (mod) top_mods.InsertLast(mod) ;
            }
            if (static_elab) {
                Array *ret_mods = veri_file::ElaborateMultipleTopStatic(&top_mods, generic_map) ;
                result = (ret_mods) ? 1 : 0 ;
                delete ret_mods ;
            } else {
            }
        }
        proceed = 0 ;
    }

    if (proceed && arg) {
        // Explicitly use a unit, but test if it is there before we proceed.
        unsigned mode = 0 ;
        mode = (veri_file::GetModule(arg, 1, ((lib_name) ? lib_name : "work"))) ? 1 : 0 ;
        if (mode) {
            // Its Verilog
            if (static_elab) {
                result = veri_file::ElaborateStatic(arg, lib_name, generic_map) ;
            } else {
            }
            proceed = 0 ;
        }

        if (!mode) {
            // Its VHDL
            result = vhdl_file::Elaborate(arg, lib_name, arch, generic_map, static_elab) ;
            proceed = 0 ;
        }
    }

    if (proceed) {
        // VIPER #3379 : When we are elaborating a library, first elaborate the verilog
        // modules and then elaborate the vhdl units. This will make mixed language
        // elaboration effective.

        // Elaborate all verilog modules :
        if (static_elab) {
            result += veri_file::ElaborateAllStatic(lib_name) ;
        } else {
        }
        // Elaborate all vhdl units :
        result += vhdl_file::ElaborateAll(lib_name, static_elab) ;
    }

    // Cleanup
    delete generic_map ;
    // VIPER #3910: Always clear out list of top modules from veri_file if exists from previous elaboration
    veri_file::ClearTopModuleList() ;
    veri_file::RemoveAllLOptions() ; // Clean out all -L options from verilog
    vhdl_file::RemoveAllLOptions() ; // Clean out all -L options from vhdl

    return result ; // 0 is an error.
}

/**************************/
/* Create command 'read'  */
/**************************/

ComRead::ComRead() : Command("read","read a design from a file, and execute all its modules")
{
    Add(new ArrayArg(0,"<file_name> <file_name>..",0 /*not optional*/, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new StringArg("format","file format (edif,vhdl,verilog..)")) ;
    Add(new StringArg("work","work library to compile in")) ;

    Add(new ArrayArg("file", "verilog -f command file", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new ArrayArg("File", "verilog -F command file", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new ArrayArg("f", "verilog -f command file", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new ArrayArg("L", "search library for imported/instantiated design units")) ; // VIPER #5618/#2762: -L option: library search order (in 'import')

    // VIPER #4115: File extension to language dialect selection:
    Add(new ArrayArg("veri1995ext","extension of files to be read in Verilog 1995 mode")) ;
    Add(new ArrayArg("veri2000ext","extension of files to be read in Verilog 2001 mode")) ;
    Add(new ArrayArg("sysveri2005ext","extension of files to be read in SystemVerilog 2005 mode")) ; // VIPER #5965
    Add(new ArrayArg("sysveri2009ext","extension of files to be read in SystemVerilog 2009 mode")) ;
    Add(new ArrayArg("sysveriext","extension of files to be read in SystemVerilog mode")) ;

    Add(new BoolArg("vhdl_87","read in VHDL 87 mode. Default is VHDL 93",1,4)) ; // VHDL language switches are in group 4, optional and exclusive (can't have multiple different settings)
    Add(new BoolArg("vhdl_2k","read in VHDL 2000 mode. Default is VHDL 93",1,4)) ;
    Add(new BoolArg("vhdl_2008","read in VHDL 2008 mode. Default is VHDL 93",1,4)) ;
    Add(new BoolArg("vhdl_sort", "analyze registered VHDL files in correct order", 1, -1)) ; // not optional argument, but other options in -1 group allowed as alternative

    // Flavor : part of 'optional', 'mutually-exclusive' group.
    Add(new BoolArg("verilog_2000","read in Verilog 2001 mode. this is the default",1,1)) ; // Verilog language switches are in group 2, optional and exclusive (can't have multiple different settings)
    Add(new BoolArg("verilog_95","read in Verilog '95 mode. Default is Verilog 2001",1,1)) ;
    Add(new BoolArg("sysv_2005","read in System Verilog 2005 mode. Default is Verilog 2001",1,1)) ; // VIPER #5965
    Add(new BoolArg("sysv_2009","read in System Verilog 2009 mode. Default is Verilog 2001",1,1)) ;
    Add(new BoolArg("sysv","read in System Verilog mode. Default is Verilog 2001",1,1)) ;
    Add(new BoolArg("sfcu","single file compilation unit, each file in command line is a separate compilation unit. This is the default",1,2)) ;
    Add(new BoolArg("mfcu","multi-file compilation unit, all files in command line make up a compilation unit. Default is sfcu",1,2)) ;
    // XL-complience switches
    Add(new ArrayArg("v","Verilog XL compatibility ; find unknown modules in these files")) ;
    Add(new ArrayArg("y","Verilog XL compatibility ; find unknown modules in these directories")) ;
    Add(new BoolArg("u","Verilog XL compatibility ; upper-case all Verilog identifiers")) ;
    Add(new ArrayArg("incdir","Verilog XL compatibility ; find include files in these directories")) ;
    Add(new ArrayArg("libext","Verilog file extensions for -y -v option.")) ;
    Add(new BoolArg("librescan", "Verilog search unknown modules always starting with first library.")) ; // VIPER 2132 : add -librescan option
    Add(new ArrayArg("define","Verilog macro definition: 'name=value' or 'value'")) ;

    Add(new StringArg("top","name of top level module to be executed")) ;
    Add(new BoolArg("dont_clean","Normally, 'read' cleans up elaborated parse-trees. This option will prevent that")) ;
    Add(new BoolArg("static","do static elaboration (not RTL elaboration)", 1, 3)) ;
}

ComRead::~ComRead() {}

unsigned
ComRead::Process(Tcl_Interp* /*interp*/)
{
    Array *file_names = ArrayVal(0) ; // Get the file name

    const char *file_name ;

    // Get the library where to store the designs from this file.
    // if -work is not set, let each reader define itself where to store the design
    const char *work_lib = StringVal("work") ;

    unsigned i ;

    // Verilog analysis

    // Set language mode.
    unsigned verilog_mode = veri_file::VERILOG_2K ; // default is 'verilog_2000'
    unsigned explicit_mode = 0 ; // flag if mode is explicitly set
    unsigned cu_mode = veri_file::NO_MODE ; // In non-system verilog mode mfcu is default
    if (BoolVal("verilog_95"))   { explicit_mode = 1 ; verilog_mode = veri_file::VERILOG_95 ; } // explicit 95 mode
    if (BoolVal("verilog_2000")) { explicit_mode = 1 ; verilog_mode = veri_file::VERILOG_2K ; } // explicit 2000 mode
    if (BoolVal("sysv_2005"))    { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG_2005 ; } // VIPER #5965: explicit System Verilog 2005 mode
    if (BoolVal("sysv_2009"))    { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG_2009 ; } // explicit System Verilog 2009 mode
    if (BoolVal("sysv"))         { explicit_mode = 1 ; verilog_mode = veri_file::SYSTEM_VERILOG ; } // explicit System Verilog mode
    if (BoolVal("sfcu"))         { cu_mode = veri_file::SFCU ; } // single-file compilation unit
    if (BoolVal("mfcu"))         { cu_mode = veri_file::MFCU ; } // multi-file compilation unit

    // VIPER #4115: File extension to language dialect selection:
    const char *ext ;
    Array *file_ext = ArrayVal("veri1995ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::VERILOG_95) ;
    file_ext = ArrayVal("veri2000ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::VERILOG_2K) ;
    file_ext = ArrayVal("sysveri2005ext") ; // VIPER #5965
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG_2005) ;
    file_ext = ArrayVal("sysveri2009ext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG_2009) ;
    file_ext = ArrayVal("sysveriext") ;
    FOREACH_ARRAY_ITEM(file_ext, i, ext) veri_file::AddFileExtMode(ext, veri_file::SYSTEM_VERILOG) ;

    unsigned seen_verilog = 0 ;

    // Verilog XL complience options
    // All these are static information, so they survive multiple file reads.
    // Optionally clear some of this static info :
    //    veri_file::RemoveAllYDirsVFiles() ; // First clear existing (static) Ydirs/Vfile info
    //    veri_file::RemoveAllIncludeDirs() ; // Clear out include (search) directories
    //    veri_file::UndefineAllMacros() ; // Clear out all Verilog macro settings (from previous files)
    //    veri_file::RemoveAllModules() ;  // Delete all (previously analyzed modules) parse trees
    // Process the -v option if present
    // these are the file names
    Array *v_option = ArrayVal("v") ;
    if (v_option) {
        FOREACH_ARRAY_ITEM(v_option, i, file_name) {
            veri_file::AddVFile(file_name) ;
        }
    }

    // Process the -y options
    // these are the dir names
    Array *y_option = ArrayVal("y") ; // Get -y directory names
    if (y_option) {
        char * dir_name = 0 ;
        FOREACH_ARRAY_ITEM(y_option, i, dir_name) {
            veri_file::AddYDir(dir_name) ;
        }
    }

    // Process the -libext option
    // These are the file extentions
    Array *lib_ext = ArrayVal("libext") ;
    if (lib_ext) {
        char *ext_name = 0 ;
        FOREACH_ARRAY_ITEM(lib_ext, i, ext_name) {
            veri_file::AddLibExt(ext_name) ;
        }
    }

    // Process the -librescan option
    if (BoolVal("librescan")) veri_file::SetLibReScan() ; // Set -librescan option for -y/-v processing (VIPER 2132)

    // Process -u option :
    if (BoolVal("u")) {
        veri_file::SetUpperCaseIds(1) ;
    } else {
        veri_file::SetUpperCaseIds(0) ;
    }

    // Process -incdir : Set search dirs for unknown modules
    veri_file::RemoveAllIncludeDirs() ; // First clear existing (static) incdirs
    Array *incdirs = ArrayVal("incdir") ; // Get -incdir directory names
    char *dir_name ;
    FOREACH_ARRAY_ITEM(incdirs, i, dir_name) {
        veri_file::AddIncludeDir(dir_name) ;
    }

    // Process the -define option
    Array *defines = ArrayVal("define") ;
    if (defines) {
        char *g, *v, *f ;
        FOREACH_ARRAY_ITEM(defines, i, g) {
            // Find if it is of the form name=value :
            v = ::strchr(g,'=') ;
            if (v) {
                f = g ; // name of the macro
                *v++ = '\0' ; // Kill off the '=' character, let v point to the value after it.
                (void) veri_file::DefineCmdLineMacro(f, v) ; // Separate treatment for user defined macro (VIPER #2407)
            } else {
                f = 0 ; // just name, no value
                v = g ;
                (void) veri_file::DefineCmdLineMacro(v) ; // Separate treatment for user defined macro (VIPER #2407)
            }
        }
    }

    // VHDL analysis
    unsigned vhdl_mode = vhdl_file::VHDL_93 ;
    if (BoolVal("vhdl_87")) vhdl_mode = vhdl_file::VHDL_87 ;
    if (BoolVal("vhdl_2k")) vhdl_mode = vhdl_file::VHDL_2K ;
    if (BoolVal("vhdl_2008")) vhdl_mode = vhdl_file::VHDL_2008 ;
    unsigned seen_vhdl = 0 ;

    veri_file::RemoveAllLOptions() ; // Clean out all -L options from verilog
    vhdl_file::RemoveAllLOptions() ; // Clean out all -L options from vhdl

    // VIPER #2762/#5618: Add -L option specified libraries
    Array *ordered_libs = ArrayVal("L") ;
    if (ordered_libs) {
        char* ordered_lib_name ;
        FOREACH_ARRAY_ITEM(ordered_libs, i, ordered_lib_name) {
            if (!ordered_lib_name) continue ;
            veri_file::AddLOption(ordered_lib_name) ;
            vhdl_file::AddLOption(ordered_lib_name) ;
        }
    }

    // Elaboration options
    const char *top = StringVal("top") ;

    unsigned static_elab = 1 ; // default - always on for a static-elab-only product config

    // Check whether the vhdl_sort option is specified
    unsigned vhdl_sorter = BoolVal("vhdl_sort") ;

    Array *owned_f_files = 0 ;
    unsigned f_analysis_mode = veri_file::UNDEFINED ;
    unsigned files_size = (file_names) ? file_names->Size() : 0 ;

    Array *f_file_names = ArrayVal("file") ;
    const char *f_file ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    f_file_names = ArrayVal("f") ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_NONE, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    f_file_names = ArrayVal("File") ;
    FOREACH_ARRAY_ITEM(f_file_names, i, f_file) {
        Array *f_files = veri_file::ProcessFFile(f_file, veri_file::F_FILE_CAPITAL, f_analysis_mode) ;
        if (!file_names) {
            file_names = f_files ;
            owned_f_files = f_files ;
        } else {
            file_names->Append(f_files) ;
            delete f_files ;
        }
    }

    if ((f_analysis_mode != veri_file::UNDEFINED) && !explicit_mode) {
        explicit_mode = 1 ;
        verilog_mode = f_analysis_mode ;
    }
    // VIPER #5434 & #5469: Set default analysis mode and insert .sv files as SystemVerilog:
    veri_file::SetDefaultAnalysisMode(verilog_mode) ;
    if (!explicit_mode) {
        // VIPER #5469: In case of no explicit mode specification, treat .sv/.sysv files as SV file:
        veri_file::AddFileExtMode(".sv", veri_file::SYSTEM_VERILOG) ;
        veri_file::AddFileExtMode(".sysv", veri_file::SYSTEM_VERILOG) ;
    }

    // VIPER #2932: Analyze multiple files using single API.
    Array *veri_files = 0 ; // Contains the names of the verilog files to be analyzed
    // Now read in all the files
    FOREACH_ARRAY_ITEM(file_names,i,file_name) {
        // For each new file, determine the format
        const char *format_string = StringVal("format") ;
        format_enum format = FORM_UNKNOWN ;
        if (!format_string) {
            // Derive format from file extension
            format = FormatFromFileName(file_name) ;
            if ((format==FORM_UNKNOWN) && explicit_mode) format = FORM_VERILOG ; // Use verilog mode
            if (format==FORM_UNKNOWN) {
                Error("%s: cannot derive format from file %s. Use -format option", Name(), file_name) ;
                continue ;
            }
        } else {
            // Take format as-is
            format = Format(format_string) ;
            if (format==FORM_UNKNOWN) {
                Error("%s: unknown format '%s'", Name(), format_string) ;
                continue ;
            }
        }

        // format dependent mode :
        if (format == FORM_SYS_VERILOG) {
            // format setting is System Verilog. Indicate that
            format = FORM_VERILOG ;
            if (!explicit_mode) verilog_mode = veri_file::SYSTEM_VERILOG ; // set System Verilog mode if not explicitly set before.
        }

        switch (format) {
        case FORM_VERILOG:
            seen_verilog = 1 ;
            // VIPER #2932. Call API to analyze multiple files.
            //(void) veri_file::Analyze(file_name, verilog_mode, work_lib) ;
            // create new array containing the verilog files
            if (!veri_files) veri_files= new Array() ;
            veri_files->InsertLast(file_name) ;
            break ;

        case FORM_VHDL:
            seen_vhdl = 1 ;
            if (vhdl_sorter) {
                // Register any filename specified on command line
                (void) vhdl_sort::RegisterFile(file_name, work_lib, vhdl_mode) ;
                break ;
            }
            (void) vhdl_file::Analyze(file_name, work_lib, vhdl_mode) ;
            break ;

        default:
            Error("%s: cannot read format %s in this product", Name(), Format(format)) ;
            break ;
        }
    }
    unsigned verilog_status = 1 ; // Store status of verilog (analysis + elaboration)
    if (seen_verilog) {
        // VIPER #2932: Analyze multiple files using single API.
        if (veri_files && veri_files->Size()) {
            if (veri_files->Size() > 1) {
                verilog_status = veri_file::AnalyzeMultipleFiles(veri_files, verilog_mode, work_lib, cu_mode) ;
            } else if (veri_files->Size()) { // For analysis of a single file call Analyze routine
                char *file = (char*) veri_files->At(0) ;
                verilog_status = veri_file::Analyze(file, verilog_mode, work_lib, cu_mode) ;
                // For sfcu, compilation unit must be ended before user library processing
                if (veri_file::IsSingleFileCompilationUnitMode()) veri_file::EndCompilationUnit() ;
            }
            veri_file::ProcessUserLibraries() ; // Process user libraries
            // End compilation unit :
            veri_file::EndCompilationUnit() ;
        }
        delete veri_files ; // clean up
    }

    if (vhdl_sorter) {
        const char *analyze_lib ;
        unsigned local_vhdl_mode ;
        unsigned default_mode = (vhdl_mode==vhdl_file::VHDL_93) ? 1 : 0 ; // Default mode in this command
        FOREACH_SORTED_FILE(i, file_name, analyze_lib, local_vhdl_mode) {
            // Analyze this file from the sorted list (use the specific mode if given, otherwise use the default mode):
            (void) vhdl_file::Analyze(file_name, analyze_lib, (default_mode)?local_vhdl_mode:vhdl_mode) ;
        }

        // Need to clear registered files, since they have already been analyzed.
        vhdl_sort::ClearRegisteredFiles() ;
    }

    // VIPER #2726: Clear the library constructs, it has been applied for this session.
    // We need to clear this before elaboration, otherwise elaboration time
    // -y/-v processing may use this which might not be legal.
    veri_file::ClearAllLibraryDecls() ;

    // VIPER #3910: Always clear out list of top modules from veri_file if exists from previous elaboration
    veri_file::ClearTopModuleList() ;

    // Elaborate top-level Verilog module, or if none is set, elaborate everything
    if (top && seen_verilog) {
        if (static_elab) {
            if (!veri_file::ElaborateStatic(top, work_lib)) verilog_status = 0 ;
        } else {
        }
    }
    else if (!top && seen_verilog) {
        if (static_elab) {
            if (!veri_file::ElaborateAllStatic(work_lib)) verilog_status = 0 ;
        } else {
        }
    }

    // Elaborate top-level VHDL unit, or if none is set, elaborate everything
    if (top && seen_vhdl) {
        (void) vhdl_file::Elaborate(top, work_lib, 0, 0, static_elab) ;
    }
    else if (!top && seen_vhdl) {
        (void) vhdl_file::ElaborateAll(work_lib, static_elab) ;
    }

    // VIPER #3910: Always clear out list of top modules from veri_file if exists from previous elaboration
    veri_file::ClearTopModuleList() ;

    // VIPER #4356: Undefine all command line macros after the command is processed:
    veri_file::UndefineCmdLineMacros() ;
    // VIPER #5434: Reset default analysis mode:
    veri_file::SetDefaultAnalysisMode(veri_file::UNDEFINED) ;
    veri_file::RemoveAllLOptions() ; // Clean out all -L options from verilog

    while (file_names && (file_names->Size() > files_size)) Strings::free((char *)file_names->RemoveLast()) ;
    delete owned_f_files ; // 'file_names' may be corrupted below this line
    veri_file::ClearAllFileExt() ; // Clear all mapped file extensions
    vhdl_file::RemoveAllLOptions() ; // Clean out all -L options from vhdl

    return (Message::ErrorCount() || !verilog_status) ? 0 : 1 ;
}

/**************************/
/* Create command 'vhdl_read_all' */
/**************************/

ComVhdlReadAll::ComVhdlReadAll() : Command("vhdl_read_all", "analyzes all the VHDL design files and elaborates ALL the top level design units")
{
    Add(new ArrayArg(0, "<file_name> <file_name>..", 1, 1)) ;
    Add(new StringArg("work", "work library to compile in")) ;
    Add(new BoolArg("vhdl_87","read in VHDL 87 mode. Default is VHDL 93",1,4)) ; // VHDL language switches are in group 4, optional and exclusive (can't have multiple different settings)
    Add(new BoolArg("vhdl_2k","read in VHDL 2000 mode. Default is VHDL 93",1,4)) ;
    Add(new BoolArg("vhdl_2008","read in VHDL 2008 mode. Default is VHDL 93",1,4)) ;
    Add(new BoolArg("static","do static elaboration (not RTL elaboration)")) ;
}

ComVhdlReadAll::~ComVhdlReadAll() {}

unsigned
ComVhdlReadAll::Process(Tcl_Interp * /* interp */)
{
    Array *file_names = ArrayVal(0) ; // Get the file name

    // Get the library where to store the designs from this file.
    // if -work is not set, let each reader define itself where to store the design
    const char *work_lib = StringVal("work") ;

    // VHDL analysis
    unsigned vhdl_mode = vhdl_file::VHDL_93 ;
    if (BoolVal("vhdl_87")) vhdl_mode = vhdl_file::VHDL_87 ;
    if (BoolVal("vhdl_2k")) vhdl_mode = vhdl_file::VHDL_2K ;
    if (BoolVal("vhdl_2008")) vhdl_mode = vhdl_file::VHDL_2008 ;

    // No RTL elaboration, always static elaborate:
    unsigned static_elab = 1 ;

    // Now read in all the files
    unsigned i ;
    const char *file_name ;
    FOREACH_ARRAY_ITEM(file_names, i, file_name) {
        // Register any filename specified on command line
        (void) vhdl_sort::RegisterFile(file_name, work_lib, vhdl_mode) ;
    }

    const char *analyze_lib ;
    unsigned local_vhdl_mode ;
    unsigned default_mode = (vhdl_mode==vhdl_file::VHDL_93) ? 1 : 0 ; // Default mode in this command
    FOREACH_SORTED_FILE(i, file_name, analyze_lib, local_vhdl_mode) {
        // Analyze this file from the sorted list (use the specific mode if given, otherwise use the default mode):
        (void) vhdl_file::Analyze(file_name, analyze_lib, (default_mode)?local_vhdl_mode:vhdl_mode) ;
    }

    const char *unit_name ;
    FOREACH_TOP_UNIT(i, analyze_lib, unit_name) {
        (void) vhdl_file::Elaborate(unit_name, analyze_lib, 0, 0, static_elab) ;
    }

    // Need to clear registered files, since they have already been analyzed.
    vhdl_sort::ClearRegisteredFiles() ;

    return (Message::ErrorCount()) ? 0 : 1 ;
}

/********************************************************************/
/* Clean-up of parse-trees and netlists                             */
/********************************************************************/

ComCleanup::ComCleanup() :
    Command("cleanup","Delete a parse-tree and/or netlist")
{
    Add(new StringArg("work","library where the unit(s)/module(s) is analyzed(elaborated)", 1, 0)) ;
    Add(new StringArg("unit","name of VHDL primary unit to delete (parse-tree)",    1, 1)) ;
    Add(new BoolArg("save_packages","don't delete VHDL packages (parse-tree)",      1, 2)) ;
    Add(new StringArg("module","name of Verilog module to delete (parse-tree)",     1, 1)) ;
    Add(new BoolArg("parse_trees","delete all parse-trees",                         1, 1)) ;
    Add(new BoolArg("all","delete all parse-trees, netlists, and related data-structures", 1, 1)) ;
    Add(new BoolArg("silent","don't print info messages",                           1, 0)) ;
    Add(new BoolArg("statics","cleanup static structures (only active with -all))", 1, 2)) ;
}

// -----------------------------------------

ComCleanup::~ComCleanup()
{
}

// -----------------------------------------

unsigned
ComCleanup::Process(Tcl_Interp * /*interp*/)
{
    const char *lib_name         = StringVal("work") ;
    const char *unit_name        = StringVal("unit") ;  // VHDL unit name
    unsigned save_packages       = BoolVal("save_packages") ;
    const char *module_name      = StringVal("module") ; // Verilog module name
    unsigned delete_parse_trees  = BoolVal("parse_trees") ;
    unsigned delete_all          = BoolVal("all") ;
    unsigned verbose             = (BoolVal("silent")) ? 0 : 1 ;
    unsigned delete_statics      = BoolVal("statics") ;

    // Argument combination check :
    if (delete_statics && !delete_all){
        Error("%s: option -statics can only be used when option -all is specified", Name()) ;
        return 0 ;
    }

    // Clean up VHDL primary units (parse-trees)
    if (delete_all || delete_parse_trees || unit_name) {
        if (unit_name) {
            const char *tmp_lib_name = (lib_name) ? lib_name : "work" ;
            if (vhdl_file::RemoveUnit(tmp_lib_name, unit_name)) {
                if (verbose) Info("The vhdl parse-tree '%s' was deleted from the library '%s'", unit_name, tmp_lib_name) ;
            } else {
                if (verbose) Error("%s: The vhdl parse-tree '%s' was not deleted from library '%s'", Name(), unit_name, tmp_lib_name) ;
            }
        } else {
            vhdl_file::RemoveAllUnits(lib_name, (save_packages) ? 0 : 1) ;
            if (!delete_all) {
                if (save_packages) {
                    if (verbose) Info("All vhdl parse-trees (not including packages) were deleted") ;
                } else {
                    if (verbose) Info("All vhdl parse-trees (including packages) were deleted") ;
                }
            }
        }
    }

    // Clean up Verilog primary units (parse-trees)
    if (delete_all || delete_parse_trees || module_name) {
        if (module_name) {
            if (veri_file::RemoveModule(module_name)) {
                if (verbose) Info("The verilog parse-tree '%s' was deleted", module_name) ;
            } else {
                if (verbose) Error("%s: The verilog parse-tree '%s' was not deleted (could not be found)", Name(), module_name) ;
            }
        } else {
            veri_file::RemoveAllModules() ;
            // Need to clear out all static macro definitions
            veri_file::UndefineAllMacros() ;
            if (!delete_all && verbose) Info("All verilog parse-trees were deleted") ;
        }
    }

    if (delete_all) {
        // Print a single info message if '-all' was specified
        if (verbose) Info("All parse-trees and netlists were deleted") ;

        if (delete_statics) {
            // Delete remaining relevant data-structures
            Command::ResetMessageMap() ;
            veri_file::Reset() ;
            vhdl_file::Reset() ;
            vhdl_sort::Reset() ;

            // Utility cleanup
            Message::ClearErrorCount() ;
            Message::ClearAllMessageTypes() ;
            Message::ClearAllNewFormattedStrings() ;
            SaveRestore::ResetMessageMap() ;
            TextBasedDesignMod::ResetMessageMap() ;
            RuntimeFlags::DeleteAllFlags() ;
            Strings::CleanupStaticStringSet() ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
            Command::ResetLookupFiles() ;
#endif

            // WARNING: Please be absolutely sure that there is no reference of linefile
            //          anymore in the memory before calling this routine. Otherwise we will corrupt.
            LineFile::DeleteAllLineFiles() ;
        }
    }

    return 1 ;
}

/********************************************************************/
/* Binary restore of a parse-tree                                   */
/********************************************************************/

ComRestore::ComRestore()
  : Command("restore", "restore a parse-tree stored in binary format")
{
    Add(new StringArg("work",   "(work) library where the unit is found",   0 /*Not optional*/));
    Add(new StringArg("unit",   "VHDL unit",                        0 /*Not optional*/, 1 /*group id*/));
    Add(new StringArg("module", "Verilog module",                           0 /*Not optional*/, 1 /*group id*/));
}

// -----------------------------------------

ComRestore::~ComRestore()
{
}

// -----------------------------------------

unsigned
ComRestore::Process(Tcl_Interp * /*interp*/)
{
    const char *lib_name = StringVal("work");
    if (!lib_name) { // If there is no library name exit
        Error("%s: Save Restore failed; library/unit search path not set; cannot restore", Name()) ;
        return 0 ;
    }

    // Verilog SDB restore
    const char *module_name = StringVal("module") ;
    if (module_name) { // Restore verilog module
        return veri_file::Restore(lib_name, module_name) ;
    }

    // VHDL VDB restore
    const char *unit_name = StringVal("unit");
    if (unit_name) { // Restore VHDL unit
        return vhdl_file::Restore(lib_name, unit_name) ;
    }

    return 1 ;
}

// -----------------------------------------

// VIPER #1777, 1781, 2023, 3570: VHDL Library alias:
ComSetVhdlLibraryAlias::ComSetVhdlLibraryAlias()
  : Command("setvhdllibraryalias", "Set a library alias by inserting a mapping to a logical VhdlLibrary")
{
    Add(new StringArg("alias",  "name of library alias to set", 0)) ;
    Add(new StringArg("target", "name of library to be aliased", 0)) ;
}

unsigned
ComSetVhdlLibraryAlias::Process(Tcl_Interp * /*interp*/)
{
    const char *alias = StringVal("alias") ;
    const char *target= StringVal("target") ;

    if (vhdl_file::SetLibraryAlias(alias, target)) {
        Info("The VhdlLibrary alias '%s' was set to '%s'", alias, target) ;
        return 1 ;
    }

    return 0 ;
}

// -----------------------------------------

ComUnsetVhdlLibraryAlias::ComUnsetVhdlLibraryAlias()
  : Command("unsetvhdllibraryalias", "Unset an existing library alias by removing the mapping to the logical VhdlLibrary")
{
    Add(new StringArg("alias",  "name of library alias to unset", 0)) ;
}

unsigned
ComUnsetVhdlLibraryAlias::Process(Tcl_Interp * /*interp*/)
{
    const char *alias = StringVal("alias") ;

    if (vhdl_file::UnsetLibraryAlias(alias)) {
        Info("The VhdlLibrary alias '%s' was unset", alias) ;
        return 1 ;
    }

    return 0 ;
}

// -----------------------------------------

