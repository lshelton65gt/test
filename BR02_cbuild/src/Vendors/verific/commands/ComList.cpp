/*
 *
 * [ File Version : 1.101 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <fstream>
using namespace std ;
#include <stdlib.h>
#include <string.h> // strtok, strchr
#include "tcl.h" // Using Tcl_AppendElement etc in this file

#include "ComList.h"

#include "Message.h"

#include "vhdl_sort.h"
#include "vhdl_file.h"
#include "VhdlUnits.h"
#include "veri_file.h"
#include "VeriModule.h"

#include "Strings.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ComGetVhdlLibraryPath::ComGetVhdlLibraryPath()
  : Command("getvhdllibrarypath","report physical search path (directory) for a VHDL library")
{
    // Either -default or -associate, or both, needs to be set
    // So make it a non-optional, negative group.
    Add(new BoolArg("default",    "report default search path for all libraries", 0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
    Add(new StringArg("associate",  "<lib_name>",           0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
}

ComGetVhdlLibraryPath::~ComGetVhdlLibraryPath()
{
}

unsigned
ComGetVhdlLibraryPath::Process(Tcl_Interp * /*interp*/)
{
    unsigned get_default = BoolVal("default");
    const char *library_name= StringVal("associate");

    // Get default path
    if (get_default) {
        char *path = vhdl_file::GetDefaultLibraryPath();
        Info("Default VHDL Library Search Path: %s", (path) ? path : "<NOT DEFINED>");
    }

    // Get libraray->path association
    if (library_name) {
        char *path = vhdl_file::GetLibraryPath(library_name);
        Info("VHDL Library Path for '%s': %s", library_name, (path) ? path : "<NOT DEFINED>");
    }

    return 1 ;
}

/* ----------------------------------------- */

ComSetVhdlLibraryPath::ComSetVhdlLibraryPath()
  : Command("setvhdllibrarypath","link a VHDL library to a physical library path (a directory)")
{
    // Either -default or -associate, or both, needs to be set
    // So make it a non-optional, negative group.
    Add(new StringArg("default",    "default search path for all libraries(a directory)", 0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
    Add(new StringArg("associate",  "<lib_name=directory>",           0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
}

ComSetVhdlLibraryPath::~ComSetVhdlLibraryPath()
{
}

unsigned
ComSetVhdlLibraryPath::Process(Tcl_Interp * /*interp*/)
{
    // Set default path
    const char *default_path = StringVal("default") ;
    if (default_path) {
        if (!vhdl_file::SetDefaultLibraryPath(default_path)) {
            return 0;
        }
    }

    // Assign associations
    const char *assoc = StringVal("associate");
    if (assoc){
        // Make a copy before we split library name and path name
        char *buf = Strings::save(assoc);

        // Split library name and path name :
        unsigned result = 0 ;
        char *library = buf ;
        char *path = ::strchr(buf,'=') ;
        if (path) {
            *path++ = '\0' ; // Kill off the '=' character (so 'library' is terminated), and let path point to the character after it.
            result = vhdl_file::AddLibraryPath(library, path);
        } else {
            Error("%s: 'associate' expects : <libname>=<dirname>. found : %s", Name(), buf) ;
        }
        Strings::free(buf);
        return result;
    }

    return 1 ; // success
}

/* ----------------------------------------- */
ComGetVeriLibraryPath::ComGetVeriLibraryPath()
  : Command("getveriloglibrarypath","report physical search path (directory) for a Verilog library")
{
    // Either -default or -associate, or both, needs to be set
    // So make it a non-optional, negative group.
    Add(new BoolArg("default",    "report default search path for all libraries", 0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
    Add(new StringArg("associate",  "<lib_name>",           0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
}

ComGetVeriLibraryPath::~ComGetVeriLibraryPath()
{
}

unsigned
ComGetVeriLibraryPath::Process(Tcl_Interp * /*interp*/)
{
    unsigned get_default = BoolVal("default");
    const char *library_name= StringVal("associate");

    // Get default path
    if (get_default) {
        char *path = veri_file::GetDefaultLibraryPath();
        Info("Default Verilog Library Search Path: %s", (path) ? path : "<NOT DEFINED>");
    }

    // Get libraray->path association
    if (library_name) {
        char *path = veri_file::GetLibraryPath(library_name);
        Info("Verilog Library Path for '%s': %s", library_name, (path) ? path : "<NOT DEFINED>");
    }

    return 1 ;
}

/* ----------------------------------------- */

ComSetVeriLibraryPath::ComSetVeriLibraryPath()
  : Command("setveriloglibrarypath","link a verilog library to a physical library path (a directory)")
{
    // Either -default or -associate, or both, needs to be set
    // So make it a non-optional, negative group.
    Add(new StringArg("default",    "default search path for all libraries(a directory)", 0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
    Add(new StringArg("associate",  "<lib_name=directory>",           0 /* not optional */, -1 /*non-mutually exclusive group*/)) ;
}

ComSetVeriLibraryPath::~ComSetVeriLibraryPath()
{
}

unsigned
ComSetVeriLibraryPath::Process(Tcl_Interp * /*interp*/)
{
    // Set default path
    const char *default_path = StringVal("default") ;
    if (default_path) {
        if (!veri_file::SetDefaultLibraryPath(default_path)) {
            return 0;
        }
    }

    // Assign associations
    const char *assoc = StringVal("associate");
    if (assoc){
        // Make a copy before we split library name and path name
        char *buf = Strings::save(assoc);

        // Split library name and path name :
        unsigned result = 0 ;
        char *library = buf ;
        char *path = ::strchr(buf,'=') ;
        if (path) {
            *path++ = '\0' ; // Kill off the '=' character (so 'library' is terminated), and let path point to the character after it.
            result = veri_file::AddLibraryPath(library, path);
        } else {
            Error("%s: 'associate' expects : <libname>=<dirname>. found : %s", Name(), buf) ;
        }
        Strings::free(buf);
        return result;
    }

    return 1 ; // success
}

/* ----------------------------------------- */

ComSetMsgType::ComSetMsgType()
  : Command("setmsgtype","Associate a message id to a particular message type.")
{
    Add(new ArrayArg("ignore",  "{Tcl list of msg_ids} (i.e. {VHDL-1023 VERI-1102}) Ignore listed message(s)", 1 /* optional */)) ;
    Add(new ArrayArg("info",  "{Tcl list of msg_ids} (i.e. {VDB-1003}) Output listed message(s) as Info's", 1 /* optional */)) ;
    Add(new ArrayArg("warning",  "{Tcl list of msg_ids} (i.e. {VERI-1087}) Output listed message(s) as Warning's", 1 /* optional */)) ;
//CARBON_BEGIN
    Add(new ArrayArg("alert",  "{Tcl list of msg_ids} (i.e. {VDB-1003}) Output listed message(s) as Alerts's", 1 /* optional */)) ;
//CARBON_END
    Add(new ArrayArg("error",  "{Tcl list of msg_ids} (i.e. {VDB-1003}) Output listed message(s) as Error's", 1 /* optional */)) ;
}

ComSetMsgType::~ComSetMsgType()
{
}

unsigned
ComSetMsgType::Process(Tcl_Interp * /*interp*/)
{
    Array msg_ids(4) ;
    msg_ids.InsertLast(ArrayVal("ignore")) ;   // index 0 == VERIFIC_IGNORE
    msg_ids.InsertLast(ArrayVal("info")) ;     // index 1 == VERIFIC_INFO
    msg_ids.InsertLast(ArrayVal("warning")) ;  // index 2 == VERIFIC_WARNING
//CARBON_BEGIN
    // msg_ids.InsertLast(ArrayVal("error")) ;    // index 3 == VERIFIC_ERROR // OldVerificVersion
    msg_ids.InsertLast(ArrayVal("alert")) ;    // index 3 == VERIFIC_ALERT
    msg_ids.InsertLast(ArrayVal("error")) ;    // index 4 == VERIFIC_ERROR
//CARBON_END

    unsigned i, j ;
    char *msg_id ;
    Array *types ;
    msg_type_t target_type ;
    FOREACH_ARRAY_ITEM(&msg_ids, i, types) {
        if (!types) continue ;

        // Determine which type we're forcing this message to :
        switch(i){
        case 0: target_type = VERIFIC_IGNORE ; break ;
        case 1: target_type = VERIFIC_INFO ; break ;
        case 2: target_type = VERIFIC_WARNING ; break ;
//CARBON_BEGIN
        // case 3: target_type = VERIFIC_ERROR ; break ;   // OldVerificVersion
        case 3: target_type = VERIFIC_ALERT ; break ;
        case 4: target_type = VERIFIC_ERROR ; break ;
//CARBON_END
        default: return 0; // something bad is wrong.
        }

        // Now loop over the messages in this target type :
        FOREACH_ARRAY_ITEM(types, j, msg_id) {
            // Do some checking on acceptable msg_ids prefixes (message id's normally have a hyphen : VHDL-4688, VERI-6993, VDB-7987 etc)
            char *p = strchr(msg_id, '-') ;
            if (p) {
                // Set this message id to the target type :
                (void) Message::SetMessageType(msg_id, target_type) ;

            // Undocumented entry to get to SetAllMessageType() (VIPER 2695).
            // Check for special words errors, comments, infos, etc which point to a whole class of messages.
            } else if (Strings::compare(msg_id,"errors")) {  // -ignore errors will switch all errors off.
                Message::SetAllMessageType(VERIFIC_ERROR, target_type) ;
//CARBON_BEGIN
            } else if (Strings::compare(msg_id,"alert")) {  // -ignore alerts will switch all alerts off.
                Message::SetAllMessageType(VERIFIC_ALERT, target_type) ;
//CARBON_END
            } else if (Strings::compare(msg_id,"warnings")) {  // -error warnings will upgrade all warnings to errors
                Message::SetAllMessageType(VERIFIC_WARNING, target_type) ;
            } else if (Strings::compare(msg_id,"infos")) {  // -ignore infos will switch all errors off.
                Message::SetAllMessageType(VERIFIC_INFO, target_type) ;
            } else if (Strings::compare(msg_id,"comments")) {  // -ignore comments will switch all comments off (VIPER 2695).
                Message::SetAllMessageType(VERIFIC_COMMENT, target_type) ;

            } else { // message has no hyphen :
                Warning("%s: probably incorrect message id '%s'.  Message id's are typically formatted <PREFIX>-<number>", Name(), msg_id) ;
                // Let this go into the table. Maybe it was simply a special message id :
                (void) Message::SetMessageType(msg_id, target_type) ;
            }
        }
    }

    return 1 ;
}

/* ----------------------------------------- */

ComClearMsgType::ComClearMsgType()
  : Command("clearmsgtype","Clear a message_id->message_type association.")
{
    Add(new ArrayArg(0,  "{Tcl list of msg_ids} (i.e. {VHDL-1023 VERI-1087 VDB-1003}", 1 /* optional */)) ;
    Add(new BoolArg("all",  "clear all user-defined message forcings", 1 /* optional */)) ;
}

ComClearMsgType::~ComClearMsgType()
{
}

unsigned
ComClearMsgType::Process(Tcl_Interp * /*interp*/)
{
    Array *msg_ids = ArrayVal(0) ;
    unsigned clear_all = BoolVal("all") ;

    if (clear_all) {
        Message::ClearAllMessageTypes() ;
    } else if (msg_ids) {
        unsigned i ;
        char *msg_id ;
        FOREACH_ARRAY_ITEM(msg_ids, i, msg_id) {
            // Do some checking on acceptable msg_ids prefixes (Only allow VHDL-, VERI-, and VDB-)
            char *p = strchr(msg_id, '-') ;
            if (p) {
// We now support many packages (possibly customized ones also), so no need to check prefix.
//                *p = '\0' ;  // Truncate string at hyphen
//                if (!Strings::compare(msg_id, "VHDL") && !Strings::compare(msg_id, "VERI") && !Strings::compare(msg_id, "VDB")) {
//                    Message::Info(0, "Incorrect message id \"", msg_id, "\".  Currently only the following prefixes are supported : VHDL-, VERI-, and VDB-") ;
//                }
//                *p = '-' ;  // Add the hyphen back in
            } else {
                Warning("%s: probably incorrect message id '%s'.  Message id's are typically formatted <PREFIX>-<number>", Name(), msg_id) ;
            }
            Message::ClearMessageType(msg_id) ;
        }
    }

    return 1 ;
}

/* ----------------------------------------- */

ComVhdlRegister::ComVhdlRegister() : Command("vhdl_register", "register VHDL files (along with the library they should be associated with)")
{
    Add(new ArrayArg(0,"<file_name> <file_name>..", 0 /*not optional*/, -1)) ; // not optional argument, but other options in -1 group allowed as alternative
    Add(new StringArg("work", "work library to register in")) ;
    Add(new BoolArg("vhdl_87","read in VHDL 87 mode. Default is VHDL 93")) ;
    Add(new BoolArg("vhdl_2k","read in VHDL 2000 mode. Default is VHDL 93")) ;
    Add(new BoolArg("vhdl_2008","read in VHDL 2008 mode. Default is VHDL 93")) ;
    Add(new ArrayArg("directory", "register directories to be checked for files to find design units.", 1, -1)) ; // optional argument, but other options in -1 group allowed as alternative
}

ComVhdlRegister::~ComVhdlRegister() {}

unsigned
ComVhdlRegister::Process(Tcl_Interp* /*interp*/)
{
    const char *lib_name = StringVal("work") ;

    // Now register all the files
    Array *file_names = ArrayVal(0) ; // Get the file names
    //VERIFIC_ASSERT(file_names) ; // Should not happen ... argument is required

    unsigned vhdl_mode = vhdl_sort::VHDL_93 ;
    if (BoolVal("vhdl_87")) vhdl_mode = vhdl_sort::VHDL_87 ;
    if (BoolVal("vhdl_2k")) vhdl_mode = vhdl_sort::VHDL_2K ;
    if (BoolVal("vhdl_2008")) vhdl_mode = vhdl_sort::VHDL_2008 ;

    unsigned i ;
    const char *file_name ;
    // Register files one by one, incrementally
    FOREACH_ARRAY_ITEM(file_names, i, file_name) {
        if (!file_name) continue ;
        //Message::PrintLine("Registering VHDL file ", file_name, " in library ", (lib_name) ? lib_name : "work") ;
        (void) vhdl_sort::RegisterFile(file_name, lib_name, vhdl_mode) ;
        // Count the number of files successfully registered
    }

    // VIPER #7676: Register the dirs also:
    Array *dir_names = ArrayVal("directory") ;
    const char *dir_name ;
    FOREACH_ARRAY_ITEM(dir_names, i, dir_name) {
        if (!dir_name) continue ;
        (void) vhdl_sort::RegisterDir(dir_name, lib_name, vhdl_mode) ;
    }

    return 1 ;
}

/* ----------------------------------------- */

ComVhdlClearRegistry::ComVhdlClearRegistry() : Command("vhdl_clear_registry", "clear out registered VHDL filenames")
{
}

ComVhdlClearRegistry::~ComVhdlClearRegistry() {}

unsigned
ComVhdlClearRegistry::Process(Tcl_Interp* /*interp*/)
{
//*******************************************************
//    Message::PrintLine("Sorted list of files") ;
//    unsigned i ;
//    const char *file_name ;
//    const char *lib_name ;
//    FOREACH_SORTED_FILE(i, file_name, lib_name) {
//        if (!file_name) continue ;
//        VERIFIC_ASSERT(lib_name) ; // It must be valid
//        Message::Msg(VERIFIC_INFO, 0, 0, "analyze %s -work %s", file_name, lib_name) ;
//    }
//*******************************************************
    // Just call this routine to clear out the sorted file list
    vhdl_sort::ClearRegisteredFiles() ;

    // Print an info message
    Info("All registered VHDL files were deleted from the sorted list") ;

    return 1 ;
}

/* ----------------------------------------- */

ComListSortedVhdlFiles::ComListSortedVhdlFiles() : Command("list_sorted_vhdl_files", "list registered VHDL files in sorted order")
{
    Add(new StringArg("print_to_file", "file name to dump list into <default is to console>")) ;
}

ComListSortedVhdlFiles::~ComListSortedVhdlFiles() {}

unsigned
ComListSortedVhdlFiles::Process(Tcl_Interp* /*interp*/)
{
    unsigned i ;
    unsigned vhdl_mode ;
    const char *file_name = StringVal("print_to_file") ;
    const char *lib_name ;
    const char *mode_str ;

    if (file_name) {
        // Open the file for writing
        ofstream f(file_name, ios::out) ;
        if (!f.rdbuf()->is_open()) {
            Error("%s: cannot open file %s", Name(), file_name) ;
            return 0 ;
        }
        FOREACH_SORTED_FILE(i, file_name, lib_name, vhdl_mode) {
            if (!file_name) continue ;
            VERIFIC_ASSERT(lib_name) ; // It must be valid
            mode_str = (vhdl_mode==vhdl_sort::VHDL_PSL)?"-psl ":((vhdl_mode==vhdl_sort::VHDL_87)?"-vhdl_87 ":((vhdl_mode==vhdl_sort::VHDL_2K)?"-vhdl_2k ":((vhdl_mode==vhdl_sort::VHDL_2008)?"-vhdl_2008 ":""))) ;
            f << "analyze " << mode_str << "-work " << lib_name << " " << file_name << endl ; ;
        }
        f.close() ;
    } else {
        FOREACH_SORTED_FILE(i, file_name, lib_name, vhdl_mode) {
            if (!file_name) continue ;
            VERIFIC_ASSERT(lib_name) ; // It must be valid
            mode_str = (vhdl_mode==vhdl_sort::VHDL_PSL)?"-psl ":((vhdl_mode==vhdl_sort::VHDL_87)?"-vhdl_87 ":((vhdl_mode==vhdl_sort::VHDL_2K)?"-vhdl_2k ":((vhdl_mode==vhdl_sort::VHDL_2008)?"-vhdl_2008 ":""))) ;
            Message::Msg(VERIFIC_NONE, 0, 0, "analyze %s-work %s %s", mode_str, lib_name, file_name) ;
        }
    }

    return 1 ;
}

/* ----------------------------------------- */

