/*
 *
 * [ File Version : 1.53 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_SET_H_
#define _VERIFIC_SET_H_

#include "VerificSystem.h"

#include "Hash.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

 // Iterator over a Set
#ifdef VERIFIC_TEMPLATED_ITERATOR
/*CARBON_BEGIN*/
  // #define FOREACH_SET_ITEM(S,I,K)         if (S) for ((I).First(S);(I).Item((K));(I).Next())  // OldVerificVersion
  // #define FOREACH_SET_ITEM_BACK(S,I,K)    if (S) for ((I).Last(S);(I).Item((K));(I).Previous()) // OldVerificVersion
#define FOREACH_SET_ITEM(S,I,K)      if ((void*)(S) != NULL) for ((I).First(S);(I).Item((K));(I).Next())
#define FOREACH_SET_ITEM_BACK(S,I,K) if ((void*)(S) != NULL) for ((I).Last(S);(I).Item((K));(I).Previous())
/*CARBON_END*/
#else
/*CARBON_BEGIN*/
  // #define FOREACH_SET_ITEM(S,I,K)         if (S) for ((I).First(S);(I).Item((void**)(K));(I).Next())    // OldVerificVersion
  // #define FOREACH_SET_ITEM_BACK(S,I,K)    if (S) for ((I).Last(S);(I).Item((void**)(K));(I).Previous())  // OldVerificVersion
#define FOREACH_SET_ITEM(S,I,K)      if ((void*)(S) != NULL) for ((I).First(S);(I).Item((void**)(K));(I).Next())
#define FOREACH_SET_ITEM_BACK(S,I,K) if ((void*)(S) != NULL) for ((I).Last(S);(I).Item((void**)(K));(I).Previous())
/*CARBON_END*/
#endif

class SetItem ;
class SetIter ;

/* -------------------------------------------------------------- */

/*
    This file contains the definition of a Verific Set container.  A
    Set is a container class which acts as a hash table of key entries.
    Key is a 'void*' but the table knows of STRING, STRING_CASE_INSENSITIVE,
    POINTER and NUMERIC hash functions.  User-defined hash functions are
    presently not supported.

    If you need a key-value pair, use Verific's Map container class.

    Set has very low memory usage (comparable to a single-linked list) and
    is very fast on either key or index access.  The order in which items is
    inserted is maintained, as will be visible by iterating, or access by
    index (insert at end of list).  After 'remove' is called, items will be
    inserted in the removed spots before they are again inserted at the end.

    Rehash will enter table sequence 2N+1 :
    For init_bucket_size==2 the sequence will be 2/5/11/23/47/95/191/383/767/...
    This sequence has a nice prime number hit rate, and performs well.
    Choose init size in this sequence, if needed. In general, rehashing
    is so fast that you will not gain time by starting at a larger
    initial bucket size.

    Memory usage : examples :
       init_bucket_size=2  :  58 bytes allocated initially.
       init_bucket_size=5  :  92 bytes allocated initially.

    First rehash happens when num-of-items exceeds init_bucket_size.
    after this, 12 bytes/item will be allocated until next rehash
    (3 pointers : One in the buckets, One 'next' in the item, One 'key'
    in the item.)  On average (depending on how close to rehash),
    Set will use 12-24 bytes/item.  Use 'MemUsage()' to get memory usage
    of the table at any time.

    SPEED:
        Settings are so that on average, each 'Get' by key will cause 1
        hash and average of 0.8 key-compare calls.  (This is for random
        string access ; for 'Get' on existing items, it's closer to 1.6
        compare calls on average).  'PrintStats()' will give info on 'Get'
        speed in 'average list length' and 'maximum list length'.

    GET:
        'Get' by index is on direct index (super fast), unless
        the (internal) garbage list is not empty (there were more
        removes than inserts lately). In that case, 'Get' by index
        will be a bit slower, since it needs maximum of N/2
        pointer compares to exclude garbage list items in index count.

    REMOVE:
        'Remove' by key is as fast as 'Get' by key.  Remove labels item
        as garbage, to be used by next Insert.  There is no memory freeing.
        This is needed so that iterators will not die if the item they
        work on is removed.

    INSERT:
        'Insert' is as fast as 'Get' by key, not counting rehashing.
        rehashing is pretty darn fast : one rehash per entry and a lump
        memory copy for items in there. Just before a rehash, each item
        in the table will have been re-hashed once (1/2 of item was not rehashed,
        1/4 was rehashed twice, 1/8 was rehashed twice, 1/16 rehashed 3 times etc.)

    Important limitation : 'key' cannot be 0. key==0 is used internally as a
    indication of a garbage list item.
*/

class VFC_DLL_PORT Set
{
    friend class SetIter ;

public :
    // Constructors
    explicit Set(hash_type type=POINTER_HASH, unsigned init_bucket_size=2) ; // Standard hash/compare functions
    explicit Set(unsigned long (*compare)(const void*, const void*), unsigned init_bucket_size=2) ; // Custom hash/compare function

    // Destructor
    ~Set() ; // note that destructor is NOT virtual (to save memory). This means you should not delete a derived class object by base type pointer.

private :
    // Prevent compiler from defining the following
    Set(const Set &) ;            // Purposely leave unimplemented
    Set& operator=(const Set &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Functionality
    unsigned        Size() const ;    // Number of set items currently inserted
    void            Reset() ;         // Clears all items in the table
    unsigned        HasGarbage() const { return (_garbage) ? 1 : 0 ; } // check if this table has items on the garbage list

    // Insertion method
    unsigned        Insert(const void *key,             // Key value to insert
                           unsigned force_overwrite=0) ;// If key exists, overwrite existing value with new value (rather than refuse insertion)

    // Removal method
    unsigned        Remove(const void *key) ;           // Remove item from table by key

    // Retrieval methods
    void *          Get(const void *key) const ;        // Get item's key from table by key
    SetItem *       GetItem(const void *key) const ;    // Get item from table by key
    void *          GetAt(unsigned idx) const ;         // Get key by index. Like an array
    void *          GetLast() const ;                   // Get key with highest index. Like an array

    // Informational method
    hash_type       HashType() const ;                  // Get type of hash this Set uses.
    unsigned        IndexOfItem(const void *key) const; // Get index of the item under this key w.r.t. first inserted item

    // Memory usage method
    unsigned long   MemUsage() const ;
    //void          PrintStats() const ;

private:
    void            Rehash() ;                          // Rehashes (grows) table

    unsigned long (*_compare_func)(const void *key1, const void *key2) ; // Compare function pointer

    SetItem   **_buckets ;     // Allocated memory heap
    unsigned    _num_buckets ; // Total number of SetItems
    unsigned    _size ;        // Current number of inserted SetItems
    unsigned    _entry_size ;  // Index for next item in 'entries'
    SetItem    *_entries ;     // An array of inserted SetItems
    SetItem    *_garbage ;     // Garbage list : pointer to first removed item
} ; // class Set

/* -------------------------------------------------------------- */

/*
    The following class, SetItem, represents one item in an associated
    hash table.  To get a handle to a SetItem, use the method Set::GetItem().
*/

class VFC_DLL_PORT SetItem
{
private:
    // Only a Set will be allowed to create a SetItem object
    friend class Set ;
    SetItem() : _key(0), _next(0) { } ;
    ~SetItem() { } ;

private:
    // Prevent compiler from defining the following
    SetItem(const SetItem &) ;            // Purposely leave unimplemented
    SetItem& operator=(const SetItem &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    void *      Key()   { return _key ; }
    SetItem *   Next()  { return _next ; }

private:
    void       *_key ;
    SetItem    *_next ;
} ; // class SetItem

/* -------------------------------------------------------------- */

/*
    The following class, SetIter, represents an iterator object used
    to iterate a Set structure.
*/

class VFC_DLL_PORT SetIter
{
public :
    // Constructor/destructor
    SetIter() : _idx(0), _real_index(0), _set(0) {};
    ~SetIter() { } ;

private :
    // Prevent compiler from defining the following
    SetIter(const SetIter &) ;            // Purposely leave unimplemented
    SetIter& operator=(const SetIter &) ; // Purposely leave unimplemented

public :    // Accessor methods
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    unsigned    First(const Set *s) ;      // Sets iterator to first item. Returns its index (always 0)
    unsigned    Last(const Set *s) ;       // Sets iterator to last item. Returns its index (always 0)
    unsigned    Next() ;                   // Sets iterator to next item. Returns its index. (could be one too high for the list !)
    unsigned    Previous() ;               // Sets iterator to previous item. Returns its index (could be one too low for the list !)

    // Modifier methods
#ifdef VERIFIC_TEMPLATED_ITERATOR
    template<class T> unsigned Item(T *key) const ; // Return item via the 'item' argument
#else
    unsigned    Item(void **key=0) const ; // Sets key from this item. Returns 0 if end of list. 1 normally
#endif

    SetItem    *GetSetItem() const; // Returns current SetItem pointer, or 0 if current iterator setting is invalid (outside Set bounds).

private:
    unsigned    _idx ;        // Internally used entry index value
    unsigned    _real_index ; // Actual index value (of only valid MapItems)
    Set        *_set ;        // Handle to Set object this iterator is traversing
} ; // class SetIter

/* -------------------------------------------------------------- */
#ifdef VERIFIC_TEMPLATED_ITERATOR
template<class T> unsigned SetIter::Item(T *key) const // Return item via the 'item' argument
{
    // Sets key from this item. Returns 0 if end of list. 1 normally
    if (!_set) return 0 ;
    if (_idx>=_set->_entry_size) return 0 ;

    SetItem *he = &(_set->_entries[_idx]) ;
    if (key) *key = (T)he->Key() ;
    return 1 ;
}
#endif // VERIFIC_TEMPLATED_ITERATOR

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_SET_H_

