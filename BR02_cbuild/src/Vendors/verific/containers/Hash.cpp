/*
 *
 * [ File Version : 1.30 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <ctype.h>  // For tolower
#include <string.h>  // For strcmp

#include "Hash.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*------------------------------------------------*/

// NUMBER COMPARE METHOD

/*static*/ unsigned long
Hash::NumCompare(const void *key1, const void *key2)
{
    if (key2) {
        return (key1 == key2) ;
    } else {
        return (unsigned long)key1 ;
    }
}

/*------------------------------------------------*/

// POINTER COMPARE METHOD

/*static*/ unsigned long
Hash::PointerCompare(const void *key1, const void *key2)
{
    if (key2) {
        return (key1 == key2) ;
    } else {
        return (unsigned long)key1 >> 2 ;
    }
}

/*------------------------------------------------*/

// HASH-BASED CASE-SENSITIVE STRING COMPARE METHOD

/*static*/ unsigned long
Hash::StringCompare(const void *key1, const void *key2)
{
    register unsigned char *tmp1 = (unsigned char*)key1 ;
    register unsigned char *tmp2 = (unsigned char*)key2 ;

    if (tmp2) {
        // Use system's strcmp routine to get runtime improvement
        if (::strcmp((char*)key1, (char*)key2) == 0) return 1 ;
        //while (*tmp1 == *(tmp2++)) {
            //if (*(tmp1++)=='\0') return 1 ; // They are the same
        //}
        return 0 ; // They are different
    } else if (tmp1) {
        // Regular string hashing
        register unsigned long h ;
        // The following is good HASH function.  If you have something better,
        // please let us (viper@verific.com) know!!! :)
        for (h=5381; *tmp1!='\0' ; h = ((h << 5) + h) + *(tmp1++) /*hash * 33 + c*/) {} ; // djb2 algorithm (k==33). Works like a charm
        //for (h=0; *tmp1!='\0' ; h = h*997 + *(tmp1++)) {} ; // sucks : (twice as slow as djb2 and more collisions)
        return h ;
    } else {
        // 0-key hashing
        return 0 ;
    }
}

/*------------------------------------------------*/

// HASH-BASED CASE-INSENSITIVE STRING COMPARE METHOD

/*static*/ unsigned long
Hash::StringCaseInsensitiveCompare(const void *key1, const void *key2)
{
    register unsigned char *tmp1 = (unsigned char*)key1 ;
    register unsigned char *tmp2 = (unsigned char*)key2 ;

    if (tmp2) {
        while (tolower(*tmp1) == ::tolower(*(tmp2++))) {
           if (*(tmp1++)=='\0') return 1 ; // They are the same
        }
        return 0 ; // They are different
    } else if (tmp1) {
        // Regular string hashing
        register unsigned long h ;
        // The following is good HASH function.  If you have something better,
        // please let us (viper@verific.com) know!!! :)
        for (h=5381; *tmp1!='\0' ; h = ((h << 5) + h) + (unsigned long)::tolower(*(tmp1++)) /*hash * 33 + c*/) {} ; // djb2 algorithm (k==33). Works like a charm
        //for (h=0; *tmp1!='\0' ; h = h*997 + tolower(*(tmp1++))) {} ; // sucks : (twice as slow as djb2 and more collisions)
        return h ;
    } else {
        // 0-key hashing
        return 0 ;
    }
}

/*------------------------------------------------*/

