/*
 *
 * [ File Version : 1.47 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <memory.h>   // For memcpy/memset

#include "Map.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Note that initial_bucket_size * FILL_RATE _entries will be allocated
// with FILL_RATE=1, INITIAL_TABLE_SIZE=5, 112 bytes are allocated
static const unsigned FILL_RATE = 1 ;
static const unsigned MAX_DIRECT_SIZE = 5 ;

/*------------------------------------------------*/

// Use '_compare_func' both as HASH function as well as COMPARE function.
// If the second argument is 0, it is a HASH function
#define HASH_IDX(KEY)       (_compare_func((KEY),0) % _num_buckets)
#define COMPARE(KEY1,KEY2)  ((KEY1)==(KEY2) || ((KEY1) && (KEY2) && _compare_func((KEY1),(KEY2))))

/*------------------------------------------------*/

Map::Map(hash_type type, unsigned init_bucket_size)
  : _compare_func(0),
    _buckets(0),
    _num_buckets((init_bucket_size) ? init_bucket_size : 2),
    _size(0),
    _entry_size(0),
    _entries(0),
    _garbage(0)
{
    switch (type) {
    case NUM_HASH :
        _compare_func = Hash::NumCompare ;
        break ;
    case POINTER_HASH :
        _compare_func = Hash::PointerCompare ;
        break ;
    case STRING_HASH :
        _compare_func = Hash::StringCompare ;
        break ;
    case STRING_HASH_CASE_INSENSITIVE :
        _compare_func = Hash::StringCaseInsensitiveCompare ;
        break ;
    case USER_DEFINED_HASH : // Intentional fallthrough to default
    default :
        _compare_func = 0 ;
        VERIFIC_ASSERT(0) ; // Don't know this type
    }

    VERIFIC_ASSERT(_compare_func) ;

    if (_num_buckets > MAX_DIRECT_SIZE) {
        _buckets = VFC_ARRAY_NEW(MapItem*, _num_buckets) ;
        // Initialize _buckets with 0
        ::memset(_buckets,0,_num_buckets*sizeof(MapItem*)) ;
    } else {
        _buckets = 0 ;
    }

    // Create FILL_SIZE*num_num_buckets _entries
    _entries = new MapItem[_num_buckets * FILL_RATE] ;
}

/*------------------------------------------------*/

Map::Map(unsigned long (*compare)(const void*, const void*), unsigned init_bucket_size)
  : _compare_func(compare),
    _buckets(0),
    _num_buckets((init_bucket_size) ? init_bucket_size : 2),
    _size(0),
    _entry_size(0),
    _entries(0),
    _garbage(0)
{
    VERIFIC_ASSERT(_compare_func) ;

    if (_num_buckets > MAX_DIRECT_SIZE) {
        _buckets = VFC_ARRAY_NEW(MapItem*, _num_buckets) ;
        // Initialize _buckets with 0
        ::memset(_buckets, 0, _num_buckets * sizeof(MapItem*)) ;
    } else {
        // Work with linear search for now. No _buckets needed.
        _buckets = 0 ;
    }

    // Create FILL_SIZE*num_num_buckets _entries
    _entries = new MapItem[_num_buckets * FILL_RATE] ;
}

/*------------------------------------------------*/

Map::Map(const Map &orig)
  : _compare_func(orig._compare_func),
    _buckets(0),
    _num_buckets(orig._num_buckets),
    _size(orig._size),
    _entry_size(orig._entry_size),
    _entries(0),
    _garbage(0)
{
    VERIFIC_ASSERT(_compare_func) ;

    // Fill _buckets with NIL
    if (_num_buckets > MAX_DIRECT_SIZE) {
        _buckets = VFC_ARRAY_NEW(MapItem*, _num_buckets) ;
        // Initialize _buckets with 0
        ::memset(_buckets, 0, _num_buckets * sizeof(MapItem*)) ;
    } else {
        _buckets = 0 ;
    }

    _entries = new MapItem[_num_buckets * FILL_RATE] ;
    // Copy the existing _entries in there
    ::memcpy(_entries, orig._entries, _entry_size * sizeof(MapItem)) ;

    _garbage = 0 ; // _garbage list : pointer to first removed item

    // Now fill _buckets and _entries
    // Rehash each entry into new _buckets
    // Use the new _entries array to get to each one easily
    // FIX ME : Right now, we re-hash all _entries.
    // This might not be needed : We know the index from
    // the original bucket array. We could use that to
    // get the right index in the new bucket array...??
    MapItem *he ;
    unsigned check_size = 0 ;
    unsigned long idx ;
    for (he = _entries + _entry_size; he-- != _entries;) {
        if (!he->Key() && !he->Value()) {
            // _garbage list item
            he->_next = _garbage ;
            _garbage = he ;
        } else {
            // Regular item
            if (_buckets) {
                idx = HASH_IDX(he->Key()) ;
                he->_next = _buckets[idx] ;
                _buckets[idx] = he ;
            }
            check_size++ ;
        }
    }
    VERIFIC_ASSERT(check_size == _size) ;
}

/*------------------------------------------------*/

Map::~Map()
{
    if (_buckets) VFC_ARRAY_DELETE(_buckets) ;
//CARBON_BEGIN
    _buckets = 0;
//CARBON_END
    delete [] _entries ; // NOTE: Don't replace with VFC_ARRAY_DELETE, this is a MapItem array
//CARBON_BEGIN
    _entries = 0;
//CARBON_END
}

// **************************************************************/
// Functionality
// **************************************************************/

unsigned
Map::Size() const
{
    return _size ;
}

/*------------------------------------------------*/

unsigned
Map::Insert(const void *key, const void *value, unsigned force_overwrite, unsigned force_insert)
{
    register unsigned long idx = 0 ;
    unsigned new_item = 1 ;
    register MapItem *he ;

    // A 0 key is an internal flag that the item is a _garbage list item.
    // So, we cannot insert/remove/retrieve 0-key items.
    if (!key && !value) return 0 ; // Insert failed : We cannot insert key/value 0 since this represents a _garbage list item

    if (_size == (_num_buckets * FILL_RATE)) Rehash() ;

     // Find the hash entry
    idx = HASH_IDX(key) ;

    if (_buckets) {
        // Use hash _buckets to get the entry
        he = _buckets[idx] ;

        // Run over the list and check existing
        for (;he;he=he->_next) {
            if (COMPARE(key,he->Key())) {
                // Already there
                if (force_overwrite && !force_insert) {
                    he->_key = (void*)key ;
                    he->_value = (void*)value ;
                }
                new_item = 0 ; break ;
            }
        }
    } else {
        // Scan all _entries
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->Key() && !he->Value()) continue ; // _garbage list item
            if (COMPARE(key,he->Key())) {
                if (force_overwrite && !force_insert) {
                    he->_key = (void*)key ;
                    he->_value = (void*)value ;
                }
                new_item = 0 ; break ;
            }
        }
    }

    if (!new_item && !force_insert) return 0 ; // Don't insert new item

    // Insert key/value pair as a new item
    // Now get a new MapItem.
    if (_garbage) {
        // pop the _garbage list
        he = _garbage ;
        _garbage = _garbage->_next ;
    } else {
        // Otherwise, get the next one from the _entries array
        he = &(_entries[_entry_size++]) ;
    }

    // Initialize the structure
    he->_key = (void*)key ;
    he->_value = (void*)value ;

    if (_buckets) {
        // OK. Insert in the bucketlist. Up-front is easiest
        he->_next = _buckets[idx] ;
        _buckets[idx] = he ;
    }

    _size++ ;
    return new_item ; // 1 if this was a new item and it was inserted.
                      // 0 if it was existing
}

/*------------------------------------------------*/

unsigned
Map::Remove(const void *key)
{
    unsigned long idx ;
    MapItem *he, *last ;
     // Remove item from the table.

     // Find the hash entry
    if (_buckets) {
        idx = HASH_IDX(key) ;
        he = _buckets[idx] ;

        // Run over the list and check existing
        last = 0 ;
        for (;he;he=he->_next) {
            // No need to check for _garbage list item : bucket-list does not contain them
            if (COMPARE(key,he->Key())) {
                // Got it !
                if (last) {
                    last->_next = he->_next ;
                } else {
                    // First entry in bucket
                    _buckets[idx] = he->_next ;
                }
                // Push this entry on the _garbage list
                he->_next = _garbage ;
                _garbage = he ;

                // Map the key and value to 0 so iterator can skip this
                he->_key = 0 ;
                he->_value = 0 ;

                _size-- ;
                return 1 ; // Remove worked !
            }
            last = he ;
        }
    } else {
        // Scan all _entries
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->Key() && !he->Value()) continue ; // _garbage list item
            if (COMPARE(key,he->Key())) {
                // Push on the _garbage list
                he->_next = _garbage ;
                _garbage = he ;
                he->_key = 0 ;
                he->_value = 0 ;
                _size-- ;
                return 1 ;
            }
        }
    }
    return 0 ; // Entry is not in the table
}

/*------------------------------------------------*/

unsigned
Map::Remove(MapItem *item)
{
    if (!item) return 0 ;

    // Removing a mapitem is tricky, since some 'next' pointer
    // might still be pointing to it. To get that, hash to
    // the beginning of the linked list and proceed as
    // with standard remove. Only an issue if there are hash _buckets.
    unsigned long idx ;
    MapItem *he, *last ;
    void *key = item->Key() ;

     // Find the hash entry
    if (_buckets) {
        idx = HASH_IDX(key) ;
        he = _buckets[idx] ;

        // Run over the list and check existing
        last = 0 ;
        for (;he;he=he->_next) {
            if (he==item) {
                // Got it !
                if (last) {
                    last->_next = he->_next ;
                } else {
                    // First entry in bucket
                    _buckets[idx] = he->_next ;
                }
                // Push this entry on the _garbage list
                he->_next = _garbage ;
                _garbage = he ;

                // Map the key/value to 0 so iterator can skip this
                he->_key = 0;
                he->_value = 0 ;

                _size-- ;
                return 1 ; // Remove worked !
            }
            last = he ;
        }
    } else {
        // This is easy. next pointer is not in use since there are no _buckets
        // Push item on the _garbage list
        item->_next = _garbage ;
        _garbage = item ;
        item->_key = 0 ;
        item->_value = 0 ;
        _size-- ;
        return 1 ;
    }
    return 0 ; // Entry is not in the table
}

/*------------------------------------------------*/

MapItem *
Map::GetItem(const void *key) const
{
    register MapItem *he ;

    if (_buckets) {
        // Find the hash entry
        he = _buckets[HASH_IDX(key)] ;

        // Run over the list and check existing
        for (;he;he=he->_next) {
            if (COMPARE(key,he->Key())) {
                // Got it !
                return he ;
            }
        }
    } else {
        // Scan all _entries
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->Key() && !he->Value()) continue ; // _garbage list item
            if (COMPARE(key,he->Key())) {
                // Got it
                return he ;
            }
        }
    }
    return 0 ; // Can't find it
}

/*------------------------------------------------*/

MapItem *
Map::GetNextSameKeyItem(MapItem *item) const
{
    // Given an item, find the next item with the same key.
    // There could be more than one, if we used multimap insert.
    if (!item) return 0 ;
    MapItem *he ;

    if (_buckets) {
        // Run over the list and check existing
        for (he=item->_next; he; he=he->_next) {
            if (COMPARE(item->_key,he->Key())) {
                // Got it !
                return he ;
            }
        }
    } else {
        // Here, the 'next' field will not have been set yet
        // Just continue scanning (same way as 'GetItem' produced the
        // first item...
        for (he = item; he-- != _entries;) {
            if (!he->Key() && !he->Value()) continue ; // _garbage list item
            if (COMPARE(item->_key,he->Key())) {
                // Got it
                return he ;
            }
        }
    }
    return 0 ; // There are no other items with this same key
}

/*------------------------------------------------*/

void *
Map::GetValue(const void *key) const
{
    // Get value by key
    MapItem *s = GetItem(key) ;
    return (s) ? s->Value() : 0 ;
}

/*------------------------------------------------*/

void *
Map::GetValueAt(unsigned idx) const
{
    // Get value by index (order of insertion)
    MapItem *item = GetItemAt(idx) ;
    return (item) ? item->Value() : 0 ;
}

/*------------------------------------------------*/

MapItem *
Map::GetItemAt(unsigned idx) const
{
    // Get MapItem by index (order of insertion)
    VERIFIC_ASSERT (idx < _size) ;
    if (_size == _entry_size) {
        // Garbage list is empty. Access directly
        return _entries + idx ;
    }
    // Oops. There are _garbage items.
    // Cant get to my index directly
    // Need to count. Start at end or begin depending on how
    // close 'idx' in to either _size.
    MapItem *he ;
    if (idx > _size/2) {
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->Key() && !he->Value()) continue ;
            if (++idx == _size) return he ;
        }
    } else {
        for (he = _entries; he != _entries+_entry_size; he++) {
            if (!he->Key() && !he->Value()) continue ;
            if (idx-- == 0) return he ;
        }
    }
    return 0 ; // Can this happen ? Should we assert ?
}

/*------------------------------------------------*/

void
Map::Rehash()
{
    unsigned long idx ;
    register MapItem *he, *new_entries ;

    // If you understand why these asserts hold,
    // you can relax since you understand the algorithms here
    VERIFIC_ASSERT(_entry_size==_size) ;
    VERIFIC_ASSERT(_entry_size==FILL_RATE*_num_buckets) ;
    VERIFIC_ASSERT(!_garbage) ;

    // Define new number of _buckets : 2*N+1 has a good prime number hit rate
    _num_buckets = _num_buckets * 2 + 1 ;

    // Create a new _entries array (as always : FILL_RATE*_num_buckets)
    new_entries = new MapItem[FILL_RATE*_num_buckets] ;
    // Copy the existing _entries in there
    ::memcpy(new_entries, _entries, _entry_size * sizeof(MapItem)) ;
    // Swap old for new _entries. Careful with memory here
    delete [] _entries ; // NOTE: Don't replace with VFC_ARRAY_DELETE, this is a MapItem array
    _entries = new_entries ;

     // Create new _buckets array
    if (_num_buckets > MAX_DIRECT_SIZE) {
        // Delete the old bucket array
        VFC_ARRAY_DELETE(_buckets) ;
        _buckets = VFC_ARRAY_NEW(MapItem*, _num_buckets) ;
        // Initialize with 0
        ::memset(_buckets, 0, _num_buckets * sizeof(MapItem*)) ;

        // Rehash each entry into new _buckets
        // Use the new _entries array to get to each one easily
        // Aint this beautiful ?
        for (he = _entries + _entry_size; he-- != _entries;) {
            idx = HASH_IDX(he->Key()) ;
            he->_next = _buckets[idx] ;
            _buckets[idx] = he ;
        }
    }
}

/*------------------------------------------------*/

void
Map::Reset()
{
    // Just like a 'Remove()' for each entry.
    MapItem *he, *last ;

    // Clear the table
    if (_buckets) {
        (void) ::memset(_buckets, 0, _num_buckets * sizeof(MapItem*)) ;
    }

    // Put all _entries on the _garbage list
    he = _entries + _entry_size ;
    last = 0 ;
    while (he-- != _entries) {
        he->_next = last ;
        he->_key = 0 ;
        he->_value = 0 ;
        last = he ;
    }
    _garbage = last ;

    // ReMap the _size counter. _entry_size remains where it is,
    // as does _num_buckets.
    _size = 0 ;
}

/*------------------------------------------------*/

unsigned
Map::IndexOf(const void *key) const
{
    // Retrieve index of item by key
    return IndexOfItem(GetItem(key)) ;
}

/*------------------------------------------------*/

unsigned
Map::IndexOfItem(const MapItem *item) const
{
    if (!item) return 0 ;

    // This is easy and fast if there is no _garbage list
    // Slower if there is _garbage
    unsigned diff = (unsigned)(item - _entries) ;

    // If there is _garbage,
    // the _garbage elements that are in index smaller than 'item'
    // should be subtracted from 'diff'
    MapItem *runner = _garbage ;
    while (runner) {
        if (runner < item) diff-- ;
        runner = runner->_next ;
    }
    return diff ;
}

/*------------------------------------------------*/

hash_type
Map::HashType() const
{
    VERIFIC_ASSERT(_compare_func) ;

    // Return the type of hash this Map is using
    if (_compare_func == Hash::NumCompare) {
        return NUM_HASH ;
    } else if (_compare_func == Hash::PointerCompare) {
        return POINTER_HASH ;
    } else if (_compare_func == Hash::StringCompare)  {
        return STRING_HASH ;
    } else if (_compare_func == Hash::StringCaseInsensitiveCompare)  {
        return STRING_HASH_CASE_INSENSITIVE ;
    } else {
        return USER_DEFINED_HASH ;
    }
}

/*------------------------------------------------*/

unsigned long
Map::MemUsage() const
{
    // Count the allocated, but not yet used, memory in too
     return (
                sizeof(Map) +                                         // struct itself
                (_buckets ? (_num_buckets * sizeof(MapItem*)) : 0) +  // _buckets
                (FILL_RATE * _num_buckets * sizeof(MapItem))          // _entries
            ) ;
}

/*------------------------------------------------*/

#if 0
void
Map::PrintStats() const
{
    unsigned filled_num_buckets = 0 ;
    unsigned single_item = 0 ;
    unsigned max_length = 0 ;
    unsigned i, len ;
    MapItem *he ;

    cout << "Map: have " << _size << " entered items, using " <<
            MemUsage() << " bytes. So " <<
            ((_size) ? MemUsage()/_size : 0) << " bytes/entry\n" ;

    cout << _entry_size << " out of " << (_num_buckets*FILL_RATE) << " _entries capacity used\n";
    cout << "of which there are " << (_entry_size-_size) << " _entries on the _garbage list\n" ;
    if (_buckets) {
        for (i=0;i<_num_buckets;i++) {
            he = _buckets[i] ;
            for (len=0;he;he=he->_next) len++ ;

            if (len>0) filled_num_buckets++ ;
            if (len==1) single_item++ ;
            if (len>max_length) max_length=len ;
        }
        cout << filled_num_buckets << " _buckets out of " <<
             _num_buckets << " filled : " <<
             (100*filled_num_buckets/_num_buckets) << " % filling\n" ;
        cout << (filled_num_buckets?(100*single_item)/filled_num_buckets:0) <<
          " % of filled num _buckets are single-item\n" ;
        cout << "Average list length random key : " << (filled_num_buckets?(double)(_size)/(double)(_num_buckets):0) << " \n" ;
        cout << "Average list length existing key : " << (filled_num_buckets?(double)(_size)/(double)(filled_num_buckets):0) << " \n" ;
        cout << "Maximum list length : " << max_length << "\n" ;
    } else {
        cout << "Map uses linear search to access _entries. No _buckets allocated" << endl ;
    }
}
#endif

/**************************************************/
// Iterator
/**************************************************/

// Have to do all this with an index, since
// the Map might change under me by Rehashing, removing etc.
// between calls to the iterator.
// Garbage list handling makes sure that the iterator will
// never access freed memory, no matter how much you
// remove or insert in the Map during iterator.

unsigned
MapIter::First(const Map *m)
{
    // Maps iterator to first item. Returns its index (always 0)
    // This is the starter for forward iteration
    _map = const_cast<Map*>(m) ;
    if (!_map) return 0 ;
    // Skip the _garbage list items
    register MapItem *he ;
    for (_idx=0;_idx<_map->_entry_size;_idx++) {
        he = &(_map->_entries[_idx]) ;
        if (!he->Key() && !he->Value()) continue ; // _garbage list item
        break ; // We have the real first item
    }
    _real_index = 0 ;
    return _real_index ;
}

/*------------------------------------------------*/

unsigned
MapIter::Last(const Map *m)
{
    // Maps iterator to last item. Returns its index
    // This is the starter for backward iteration
    _map = const_cast<Map*>(m) ;
    if (!_map) return 0 ;
    // Start at the end
    _idx = _map->_entry_size ;
    // Skip the _garbage list items
    // Carefull for underflow of '_idx' I want index '0' too
    register MapItem *he ;
    while (_idx--!=0) {
        he = &(_map->_entries[_idx]) ;
        if (!he->Key() && !he->Value()) continue ; // _garbage list item
        break ; // Got a real item
    }
    _real_index = _map->_size - 1 ;
    return _real_index ;
}

/*------------------------------------------------*/

unsigned
MapIter::Next()
{
    // Maps iterator to next item. Returns its index. (could be one too high for the list !)
    if (!_map) return 0 ;
    // Skip the _garbage list items
    register MapItem *he ;
    while (++_idx != _map->_entry_size) {
        he = &(_map->_entries[_idx]) ;
        if (!he->Key() && !he->Value()) continue ; // _garbage list item
        break ; // Got a real item
    }
    return ++_real_index ;
}

/*------------------------------------------------*/

unsigned
MapIter::Previous()
{
    // Maps iterator to previous item. Returns its index (could be one too low for the list !)
    if (!_map) return 0 ;
    // Skip the _garbage list items
    // Carefull for underflow of '_idx' I want index '0' too
    register MapItem *he ;
    while (_idx--!=0) {
        he = &(_map->_entries[_idx]) ;
        if (!he->Key() && !he->Value()) continue ; // _garbage list item
        break ; // Got a real item
    }
    return --_real_index ;
}

/*------------------------------------------------*/

#ifndef VERIFIC_TEMPLATED_ITERATOR
unsigned
MapIter::Item(void **key, void **value) const
{
    // Maps key and value from this item. Returns 0 if end of list. 1 normally
    if (!_map) return 0 ;
    if (_idx>=_map->_entry_size) return 0 ;

    register MapItem *he = &(_map->_entries[_idx]) ;
    if (key) *key = he->Key() ;
    if (value) *value = he->Value() ;
    return 1 ;
}
#endif // VERIFIC_TEMPLATED_ITERATOR

/*------------------------------------------------*/

MapItem *
MapIter::GetMapItem() const
{
    if (!_map) return 0 ;
    if (_idx>=_map->_entry_size) return 0 ;

    register MapItem *he = &(_map->_entries[_idx]) ;
    return he ;
}

/*------------------------------------------------*/

void
MapIter::SetValue(void *value) const
{
    // Overwrite present item's value
    VERIFIC_ASSERT (_map && _idx<_map->_entry_size) ;
    MapItem *he = &(_map->_entries[_idx]) ;
    he->_value = value ;
}

/*------------------------------------------------*/

