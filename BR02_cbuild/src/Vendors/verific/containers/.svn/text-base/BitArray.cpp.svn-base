/*
 *
 * [ File Version : 1.23 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>             // For memset, memcpy

#include "Array.h"              // For use in CalculateProduct and CalculateSum
#include "BitArray.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*------------------------------------------------*/
//  BitArray Constant Values
/*------------------------------------------------*/

// The size of a single word in the data array
//   (the value is sizeof( unsigned )*8 and is
//   defined in BitArray.cpp)
static const unsigned SINGLE_WORD_SIZE = (unsigned)sizeof( unsigned ) * 8;

// the number of bytes in a single word
//   (the value is sizeof( unsigned ) and is
//   defined in BitArray.cpp)
static const unsigned BYTES_PER_WORD   = (unsigned)sizeof( unsigned );

// Utility fcns for the data array
unsigned BitArray::NumWords ( unsigned size ) const
{
    return ( size == 0 ) ? 0 : ( size / SINGLE_WORD_SIZE ) + 1;
}
unsigned BitArray::WordIndex( unsigned indx ) const
{
    return indx / SINGLE_WORD_SIZE;
}
unsigned BitArray::BitIndex ( unsigned indx ) const
{
    return indx & ( SINGLE_WORD_SIZE - 1 );
}

/*------------------------------------------------*/
//  BitArray Methods
/*------------------------------------------------*/

BitArray::BitArray( unsigned size ): _size( 0 ), _val( 0 )
{
    if( size == 0 ) return;
    ChangeSize( size );
}

/*------------------------------------------------*/
// Copy constructor

BitArray::BitArray(const BitArray &ref): _size( 0 ), _val( 0 )
{
    CopyValue( ref );
}

/*------------------------------------------------*/

BitArray::~BitArray()
{
    VFC_ARRAY_DELETE(_val);
}

/*------------------------------------------------*/
// Set a particular bit in the array to "1".  Increase
// the size if necessary

void
BitArray::SetBit( unsigned indx )
{
    if(( _size == 0 ) || ( indx >= _size )) {
        ChangeSize( indx + 1 );
    }
    if( !_val ) return;
    _val[WordIndex( indx )] |= ( 1 << BitIndex( indx ) );
}

/*------------------------------------------------*/
// Set a particular bit in the array to "0".  The size
// will not be increased if "indx" is outside the array
// because elements of the array which do not exist
// are assumed to be Zero

void
BitArray::ClearBit( unsigned indx )
{
    // non-existing elements in this array are assumed to be ZERO.
    // So if the index is outside of the array, just return;
    if( _size <= indx ) return;
    if( !_val ) return;

    unsigned mask = 1 << BitIndex( indx );
    _val[WordIndex( indx )] &= ~mask;
}

/*------------------------------------------------*/
// Retrieve the value of a bit in the array. If the "indx"
// is outside the array, return Zero

unsigned
BitArray::GetBit( unsigned indx ) const
{
    if( !_size || !_val ) return 0;
    if( indx >= _size ) return 0;
    return (_val[WordIndex( indx )] & ( 1 << BitIndex( indx ) )) ? 1:0;
}

/*------------------------------------------------*/
// Copy all the bitValues from "value" into "this"

void
BitArray::CopyValue( const BitArray& value )
{
    // Don't try to copy into myself
    if (this == &value) return ;

    // resize the array
    ChangeSize( value.Size() );

    // copy the data
    if( !_val ) return;
    ::memcpy( _val, value._val, NumWords( value.Size() ) * BYTES_PER_WORD );
}

/*------------------------------------------------*/
// resize an existing BitArray, keeping the same data

void
BitArray::ChangeSize( unsigned new_size )
{
    if( new_size == 0 ) {
        VFC_ARRAY_DELETE(_val);
        _val = 0;
        _size = 0;
        return;
    }
    if( new_size == _size ) return;
    VERIFIC_ASSERT( new_size < ( 1 << 30 ));

    unsigned old_nw = NumWords( _size );
    unsigned new_nw = NumWords( new_size );

    // if the new and old array have different sizes, we need to resize
    // the array
    if( old_nw != new_nw ) {
        // resize the data array and copy the existing data
        unsigned* tmp = _val;
        _val = VFC_ARRAY_NEW(unsigned, new_nw);
        if( tmp ) {
            ::memcpy( _val, tmp, (( old_nw < new_nw )? old_nw : new_nw ) * BYTES_PER_WORD );
            VFC_ARRAY_DELETE(tmp) ;
        }
        // clear the words added to the end of the array
        if( old_nw < new_nw ) {
            ::memset( _val + old_nw, 0, (new_nw - old_nw) * BYTES_PER_WORD );
        }
    }

    // clear the unused bits in the uppermost data word
    VERIFIC_ASSERT( _val );
    _val[WordIndex( new_size )] &= (( 1 << BitIndex( new_size )) - 1 );

    _size = new_size;
}

/*------------------------------------------------*/
// Bitwise AND the "operand" into the existing bitArray
// The size of the bitArray is the size of the smallest
// operand array

void
BitArray::And( const BitArray& operand )
{
    unsigned new_size = ( _size < operand.Size() ) ? _size : operand.Size();
    unsigned nw = NumWords( new_size );
    ChangeSize( new_size );
    if( !_val ) return;

    // AND together all the bits
    unsigned i = 0;
    for( ; i < nw ; i++ ) {
        _val[i] = GetWord(i) & operand.GetWord(i);
    }
}

/*------------------------------------------------*/
// Bitwise OR the "operand" into the existing bitArray
//   The bitArray will be extended if it is too small

void
BitArray::Or( const BitArray& operand )
{
    unsigned new_size = ( _size > operand.Size() ) ? _size : operand.Size();
    unsigned nw = NumWords( new_size );
    ChangeSize( new_size );
    if( !_val ) return;

    // OR together all the bits - GetWord() will return 0
    // for words outside the array
    unsigned i = 0;
    for( ; i < nw ; i++ ) {
        _val[i] = GetWord(i) | operand.GetWord(i);
    }
}

/*------------------------------------------------*/
// Bitwise XOR the "operand" into the existing bitArray
//   The bitArray will be extended if it is too small

void
BitArray::Xor( const BitArray& operand )
{
    unsigned new_size = ( _size > operand.Size() ) ? _size : operand.Size();
    unsigned nw = NumWords( new_size );
    ChangeSize( new_size );
    if( !_val ) return;

    // OR together all the bits - GetWord() will return 0
    // for words outside the array
    unsigned i = 0;
    for( ; i < nw ; i++ ) {
        _val[i] = GetWord(i) ^ operand.GetWord(i);
    }
}
/*------------------------------------------------*/
// Retrieve a particular word from the data array

unsigned
BitArray::GetWord( unsigned word_indx ) const
{
    if( !_val ) return 0;
    if( word_indx >= NumWords( _size )) return 0; // issue 5251 : use >=, not >.
    return _val[word_indx];
}

/*------------------------------------------------*/
// boolean AND operation on a collection of BitArrays.
// the result is returned in "this". The size is the
// maximum of the sizes of the elements of "BitArrays"

void
BitArray::ReduceAND( const Array& bit_arrays )
{
    // first calculate the max size of all elements of BitArrays
    unsigned max_size = 0;
    BitArray *ba ;
    unsigned i;
    FOREACH_ARRAY_ITEM( &bit_arrays, i, ba) {
        if( !ba ) continue;
        if( max_size < ba->Size() ) max_size = ba->Size();
    }

    // Create the new BitArray data of the proper size
    ChangeSize( max_size );

    // calculate the vector AND
    FOREACH_ARRAY_ITEM( &bit_arrays, i, ba) {
        if( !ba ) {
            ChangeSize(0) ;  // An empty entry in the array is interpreted as all Zeroes
            continue ;
        }
        if( i == 0 ) {
            CopyValue( *ba );
        } else {
            And( *ba );
        }
    }
}

/*------------------------------------------------*/
// boolean OR operation on a collection of BitArrays.
// the result is returned in "this". The size is the
// maximum of the sizes of the elements of "BitArrays"

void
BitArray::ReduceOR( const Array& bit_arrays )
{
    // first calculate the max size of any BitArray in BitArrays
    unsigned max_size = 0;
    BitArray *ba ;
    unsigned i;
    FOREACH_ARRAY_ITEM( &bit_arrays, i, ba) {
        if( !ba ) continue;
        if( max_size < ba->Size() ) max_size = ba->Size();
    }

    // Create the new BitArray data of the proper size
    ChangeSize( max_size );

    // calculate the vector OR
    FOREACH_ARRAY_ITEM( &bit_arrays, i, ba) {
        if(!ba) continue ;  // Interpret empty entries as all Zero.  Then Ignore.
        if( i == 0 ) {
            CopyValue( *ba );
        } else {
            Or( *ba );
        }
    }
}
void
BitArray::ReduceXOR( const Array& bit_arrays )
{
    // first calculate the max size of any BitArray in BitArrays
    unsigned max_size = 0;
    BitArray *ba ;
    unsigned i;
    FOREACH_ARRAY_ITEM( &bit_arrays, i, ba) {
        if( !ba ) continue;
        if( max_size < ba->Size() ) max_size = ba->Size();
    }

    // Create the new BitArray data of the proper size
    ChangeSize( max_size );

    // calculate the vector OR
    FOREACH_ARRAY_ITEM( &bit_arrays, i, ba) {
        if(!ba) continue ;  // Interpret empty entries as all Zero.  Then Ignore.
        if( i == 0 ) {
            CopyValue( *ba );
        } else {
            Xor( *ba );
        }
    }
}

