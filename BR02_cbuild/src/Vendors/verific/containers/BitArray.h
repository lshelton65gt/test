/*
 *
 * [ File Version : 1.27 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_BIT_ARRAY_H_
#define _VERIFIC_BIT_ARRAY_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array;

/* -------------------------------------------------------------- */

/*
    This class implements efficient storage for a vector of bits and simple
    manipulations of those vectors.

    The array is indexed using positive numbers but always starting from Zero.

    The array has a fixed size, but elements outside of the array can be
    accessed -- it is assumed that these elements always have a value of Zero.

    AND and OR operations detween different sized arrays automatically adjust
    the size of the result
*/

class VFC_DLL_PORT BitArray
{
public:
    explicit BitArray(unsigned size = 0) ;
    BitArray(const BitArray &ref) ;  // copy constructor
    ~BitArray(); // note that destructor is NOT virtual (to same memory). This means you should not delete a derived class object by base type pointer.

private :
    // Prevent compiler from defining the following
    BitArray& operator=(const BitArray &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    unsigned        GetBit(unsigned indx) const ;
    void            SetBit(unsigned indx) ;
    void            ClearBit(unsigned indx) ;

    inline unsigned Size() const    { return _size ; }
    void            ChangeSize(unsigned s) ;  // make the array exactly this size

    // Boolean operation between "this" and another BitArray.
    // The result is returned in "this". The original
    // value of "this" will be used for the calculation,
    // and then overwritten.
    void            And(const BitArray& operand) ;
    void            Or(const BitArray& operand) ;
    void            Xor(const BitArray& operand) ;

    // Boolean operations on collections of BitArrays.
    // the result is returned in "this". The original
    // value of "this" will be ignored for the calculation,
    // and then overwritten.
    void            ReduceAND(const Array& bit_arrays) ;
    void            ReduceOR(const Array& bit_arrays) ;
    void            ReduceXOR(const Array& bit_arrays) ;

private:
    // Utility fcns for indexing the data bits/words
    inline unsigned NumWords(unsigned size) const;
    inline unsigned WordIndex(unsigned indx) const;
    inline unsigned BitIndex(unsigned indx) const;

    unsigned        GetWord(unsigned word_indx) const;
    void            CopyValue(const BitArray& value);

private:
    unsigned    _size;
    unsigned   *_val;
} ; // class BitArray

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_BIT_ARRAY_H_

