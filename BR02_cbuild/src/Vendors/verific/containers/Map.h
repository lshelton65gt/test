/*
 *
 * [ File Version : 1.57 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_MAP_H_
#define _VERIFIC_MAP_H_

#include "VerificSystem.h"

#include "Hash.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/*
    MAP ITERATOR MACROS:

    Iterators over a Map.  Take a Map* and return key and value
    for each item. Use a 'MapIter' as iterator.

    These iterators are memory-safe ! Delete or Insert
    ANY item from the map that you are iterating on.
    Order remains intact and memory will NOT be violated.

    Ordering : Inserted items normally end up at the end of the map,
    so iteration will get to them when you insert them while
    iterating. This is not guaranteed however, when you deleted
    items before inserting the new ones. In that case iteration
    might NOT get to them (It will use up the 'deleted' spots first,
    from its own garbage list).
    You will then need to start a new iteration to get to them.
*/

#ifdef VERIFIC_TEMPLATED_ITERATOR
//CARBON_BEGIN
  //#define FOREACH_MAP_ITEM(M,I,K,V)       if (M) for ((I).First(M);(I).Item((K),(V));(I).Next())     // OldVerificVersion
  //#define FOREACH_MAP_ITEM_BACK(M,I,K,V)  if (M) for ((I).Last(M);(I).Item((K),(V));(I).Previous())  // OldVerificVersion
#define FOREACH_MAP_ITEM(M,I,K,V)      if ((void*)(M) != NULL) for ((I).First(M);(I).Item((K),(V));(I).Next())
#define FOREACH_MAP_ITEM_BACK(M,I,K,V) if ((void*)(M) != NULL) for ((I).Last(M);(I).Item((K),(V));(I).Previous())
//CARBON_END
#else
//CARBON_BEGIN
//#define FOREACH_MAP_ITEM(M,I,K,V)       if (M) for ((I).First(M);(I).Item((void**)(K),(void**)(V));(I).Next())       // OldVerificVersion
//#define FOREACH_MAP_ITEM_BACK(M,I,K,V)  if (M) for ((I).Last(M);(I).Item((void**)(K),(void**)(V));(I).Previous())    // OldVerificVersion
#define FOREACH_MAP_ITEM(M,I,K,V)      if ((void*)(M) != NULL) for ((I).First(M);(I).Item((void**)(K),(void**)(V));(I).Next())
#define FOREACH_MAP_ITEM_BACK(M,I,K,V) if ((void*)(M) != NULL) for ((I).Last(M);(I).Item((void**)(K),(void**)(V));(I).Previous())
//CARBON_END
#endif

/*
    This next iterator iterates over MultiMap items (same key)
    inserted with 'Insert(K,V,0,1)' (force-insert)
    This iterator is therefor NOT memory-proof (Cannot Remove
    the item that you are iterating on, without a risk of
    stopping the iteration loop, and don't insert items while
    iterating; since the table might rehash, which will cause
    MapItem to become invalid.
*/
//CARBON_BEGIN
  //#define FOREACH_SAME_KEY_MAPITEM(M,K,ITEM)  if (M) for ((ITEM)=(M)->GetItem(K);(ITEM);(ITEM)=(M)->GetNextSameKeyItem(ITEM))   // OldVerificVersion
#define FOREACH_SAME_KEY_MAPITEM(M,K,ITEM) if ((void*)(M) != NULL) for ((ITEM)=(M)->GetItem(K);(ITEM);(ITEM)=(M)->GetNextSameKeyItem(ITEM))
//CARBON_END

class MapItem ;
class MapIter ;

/* -------------------------------------------------------------- */

/*
    This file contains the definition of a Verific Map container.  A
    Map is a container class which supports key-to-value mappings.
    Hash table of key-value associations. key and value are both 'void*'
    but table knows of STRING, STRING_CASE_INSENSITIVE, POINTER and
    NUMERIC hash functions. User-defined hash functions are presently not supported.

    Map has very low memory usage (comparable to a double-linked list) and
    is very fast on either key or index access.
    The order in which items is inserted is maintained, as will be
    visible by iterating, or access by index (insert at end of list).
    After 'remove' is called, items will be inserted in the removed
    spots before they are again inserted at the end (garbage list).

    Rehash will enter table sequence 2N+1 :
    For init_bucket_size==2 the sequence will be 2/5/11/23/47/95/191/383/767/...
    This sequence has a nice prime number hit rate, and performs well.
    Choose init size in this sequence, if possible. In general, rehashing
    is so fast that you will not gain time by starting at a larger
    initial bucket size.

    Memory usage : examples :
        init_bucket_size=2  :  66 bytes allocated initially.
        init_bucket_size=5  : 112 bytes allocated initially.

    First rehash happens when num-of-items exceeds init_bucket_size.
    after this, 16 bytes/item will be allocated until next rehash.
    On average (depending on how close to rehash),  Map will use 16-32
    bytes/item.  Use 'MemUsage()' to get memory usage of the table at any time.

    SPEED :
        Mappings are so that on average, each 'Get' by key will cause
        1 hash and average of 0.8 key-compare calls.  (This is for random
        string access ; for 'Get' on existing items, it's closer to 1.6
        compare calls on average)  'PrintStats()' will give info on 'Get'
        speed in 'average list length' and 'maximum list length'.

    GET:
        'Get' by index is on direct index (super fast), unless
        the (internal) garbage list is not empty (there were more
        removes than inserts lately).  In that case, 'Get' by index
        will be a bit slower, since it needs maximum of N/2
        pointer compares to exclude garbage list items in index count.

    REMOVE:
        'Remove' by key is as fast as 'Get' by key.  Remove labels item
        as garbage, to be used by next Insert.  There is no memory freeing.
        This is needed so that iterators will not die if the item they
        work on is removed.

    INSERT:
        'Insert' is as fast as 'Get' by key, not counting rehashing.
        rehashing is pretty darn fast : one rehash per entry and a lump
        memory copy for items in there. Just before a rehash, each item
        in the table will have been re-hashed on average once :
        (1/2 of item was not rehashed, 1/4 was rehashed once, 1/8 was rehashed
        twice, 1/16 rehashed 3 times etc.)

    Important limitation : 'key' and 'value' cannot BOTH be 0.  This (key==0
    and value==0) is used internally as a indication of a garbage list item.
    There is no problem handling either key==0 OR value==0. (Map accepts 0
    as a valid key).
*/

class VFC_DLL_PORT Map
{
    friend class MapIter ; // Need to make the MapIter class a friend

public :
    // Don't set a default 'type'. Too risky here :
    // Mapping is often by string, and it will take long before
    // you figure out a mistake if created with 'Map' (no arguments)
    explicit Map(hash_type type, unsigned init_bucket_size=2) ; // standard hash/compare functions
    explicit Map(unsigned long (*compare)(const void*, const void*), unsigned init_bucket_size=2) ; // custom hash/compare function

    // Copy the table (NOT the elements)
    Map(const Map &orig) ;  // Copy constructor

    // Destructor
    ~Map() ; // note that destructor is NOT virtual (to save memory). This means you should not delete a derived class object by base type pointer.

private :
    // Prevent compiler from defining the following
    Map() ;                       // Purposely leave unimplemented
    Map& operator=(const Map &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Functionality
    unsigned        Size() const ; // Number of map items currently inserted
    void            Reset() ;      // Clears all items in the table (does not deallocate memory)
    unsigned        HasGarbage() const { return (_garbage) ? 1 : 0 ; } // check if this table has items on the garbage list

    // Insert method (return 1 if this was a new 'key', 0 if the 'key' was already present)
    unsigned        Insert(const void *key, const void *value,
                           unsigned force_overwrite = 0,// If key exists, overwrite existing value with new value (rather than refuse insertion)
                           unsigned force_insert = 0    // Insert key/value pair even if there are already items under this key. Multiple items can thus exist under one key.
                    ) ;                                 // Insert item in the table. return 1 if item was not already in the table. 0 otherwise.

    // Removal methods (return 1 if item is successfully removed, 0 otherwise)
    unsigned        Remove(const void *key) ;           // Remove item from table by key. Return 1 if item existed, 0 otherwise
    unsigned        Remove(MapItem *item) ;             // Remove item from table by MapItem. Return 1 if item existed, 0 otherwise

    // Retrieval methods
    MapItem *       GetItem(const void *key) const;     // Get the item (key/value pair) associated with this key (this is normal hash-lookup)
    void *          GetValue(const void *key) const;    // Get the value associated with this key (this is normal hash-lookup)
    MapItem *       GetItemAt(unsigned idx) const;      // Get the item (key/value pair) by index. Like an array
    void *          GetValueAt(unsigned idx) const;     // Get the value by index. Like an array

    // Get index of an item (fast). This is opposite from GetItemAt
    unsigned        IndexOf(const void *key) const;     // Get index of the item under this key w.r.t. first inserted item
    unsigned        IndexOfItem(const MapItem *item) const; // Get index of the item w.r.t. first inserted item

    hash_type       HashType() const ;                  // Get type of hash this Map uses.

    // For multiple items under same key (inserted with force_insert) iteration.
    MapItem *       GetNextSameKeyItem(MapItem *item) const; // Used in FOREACH_SAME_KEY_MAPITEM iterator

    // Memory usage method
    unsigned long   MemUsage() const ;
    //void PrintStats() const ;

protected :
    void            Rehash() ;         // Rehashes (grows) table

    unsigned long (*_compare_func)(const void *key1, const void *key2) ; // Compare function pointer

    MapItem    **_buckets ;     // Allocated memory heap
    unsigned     _num_buckets ; // Total number of possible map entries
    unsigned     _size ;        // Number of elements in Map
    unsigned     _entry_size ;  // Index for next item in 'entries'
    MapItem     *_entries ;     // Really an array
    MapItem     *_garbage ;     // Garbage list : pointer to first removed item
} ; // class Map

/* -------------------------------------------------------------- */

/*
    The following class, MapItem, represents one item in an associated
    hash table.  To get a handle to a MapItem, use the method Map::GetItem().
*/

class VFC_DLL_PORT MapItem
{
    friend class MapIter ;
    friend class Map ;

private :
    // MapItem can only be created/destroyed by a Map or MapIter
    MapItem() : _key(0), _value(0), _next(0) { } ;
    ~MapItem() { } ;

    // Prevent compiler from defining the following
    MapItem(const MapItem &) ;            // Purposely leave unimplemented
    MapItem& operator=(const MapItem &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Accessor methods
    void *      Key() const    { return _key ; }
    void *      Value() const  { return _value ; }

    // Allow the value field to be changed, while leaving the key unaffected :
    void        SetValue(void *value) { _value = value ; }

private :
    // Members
    void        *_key ;     // Key
    void        *_value ;   // Value
    MapItem     *_next ;    // Pointer to next MapItem (for linked-lists)
} ; // class MapItem

/* -------------------------------------------------------------- */

/*
    The following class, MapIter, represents an iterator object used
    to iterate of a Map structure.
*/

class VFC_DLL_PORT MapIter
{
public :
    // Constructor/Destructor
    MapIter() : _idx(0), _real_index(0), _map(0) { } ;
    ~MapIter() { } ;

private :
    // Prevent compiler from defining the following
    MapIter(const MapIter &) ;            // Purposely leave unimplemented
    MapIter& operator=(const MapIter &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Accessor methods
    unsigned    First(const Map *m) ;   // Maps iterator to first item. Returns its index (always 0)
    unsigned    Last(const Map *m) ;    // Maps iterator to last item. Returns its index (always 0)
    unsigned    Next() ;                // Maps iterator to next item. Returns its index. (could be one too high for the list !)
    unsigned    Previous() ;            // Maps iterator to previous item. Returns its index (could be one too low for the list !)

    // Modifier methods
#ifdef VERIFIC_TEMPLATED_ITERATOR
    template<class T1, class T2> unsigned Item(T1 *key, T2 *value) const ; // Sets key and value from this item. Returns 0 if end of list. 1 normally
    template<class T> unsigned Item(int /*key*/, T *value) const ; // Sets value from this item. Returns 0 if end of list. 1 normally
    template<class T> unsigned Item(T *key, int /*value*/) const ; // Sets key from this item. Returns 0 if end of list. 1 normally
#else
    unsigned    Item(void **key=0, void **value=0) const; // Sets key and value from this item. Returns 0 if end of list. 1 normally
#endif

    MapItem    *GetMapItem() const; // Returns current MapItem pointer, or 0 if current iterator setting is invalid (outside Map bounds).

    void        SetValue(void *value) const ; // Overwrite value

private :
    unsigned    _idx ;        // Internally used entry index value
    unsigned    _real_index ; // Actual index value (of only valid MapItems)
    Map        *_map ;        // Handle to Map object this iterator is traversing
} ; // class MapIter

/*------------------------------------------------*/

#ifdef VERIFIC_TEMPLATED_ITERATOR
template<class T1, class T2> unsigned MapIter::Item(T1 *key, T2 *value) const // Sets key and value from this item. Returns 0 if end of list. 1 normally
{
    // Maps key and value from this item. Returns 0 if end of list. 1 normally
    if (!_map) return 0 ;
    if (_idx>=_map->_entry_size) return 0 ;

    MapItem *he = &(_map->_entries[_idx]) ;
    if (key) *key = (T1)he->Key() ;
    if (value) *value = (T2)he->Value() ;
    return 1 ;
}

template<class T> unsigned MapIter::Item(int /*key*/, T *value) const // Sets value from this item. Returns 0 if end of list. 1 normally
{
    // Maps key and value from this item. Returns 0 if end of list. 1 normally
    if (!_map) return 0 ;
    if (_idx>=_map->_entry_size) return 0 ;

    MapItem *he = &(_map->_entries[_idx]) ;
    if (value) *value = (T)he->Value() ;
    return 1 ;
}

template<class T> unsigned MapIter::Item(T *key, int /*value*/) const // Sets key from this item. Returns 0 if end of list. 1 normally
{
    // Maps key and value from this item. Returns 0 if end of list. 1 normally
    if (!_map) return 0 ;
    if (_idx>=_map->_entry_size) return 0 ;

    MapItem *he = &(_map->_entries[_idx]) ;
    if (key) *key = (T)he->Key() ;
    return 1 ;
}
#endif // VERIFIC_TEMPLATED_ITERATOR
/*------------------------------------------------*/

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_MAP_H_

