/*
 *
 * [ File Version : 1.52 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h>

#include "Array.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*------------------------------------------------*/

Array::Array(unsigned init_capacity)
  : _data(0),
    _max_size((init_capacity) ? init_capacity : 1),
    _size(0)
{
    if (_max_size>>1) {
        _data = VFC_ARRAY_NEW(void*, _max_size) ;
    } // else : use _data as the container.
}

// Copy constructor
Array::Array(const Array &orig)
  : _data(0),
    _max_size(0),
    _size(orig._size)
{
    _max_size = (_size) ? _size : 1 ; // _max_size should never be 0

    // Copy data over :
    if ((orig._max_size)>>1) {
        // Original data is in orig._data.
        if (_max_size>>1) {
            // Orig is more than one element. Allocate memory :
            _data = VFC_ARRAY_NEW(void*, _max_size) ;
            // data goes to _data
            memcpy(_data, orig._data, _size * sizeof(void*)) ;
        } else {
            // data goes to &_data
            _data = (void**)*orig._data ; // memcpy(&_data, orig._data, _size * sizeof(void*)) ;
        }
    } else {
        // orig and new data both are in &_data
        _data = orig._data ;
    }
}

// Sliced Copy constructor ; both indices are inclusive
Array::Array(const Array &orig, unsigned left, unsigned right)
  : _data(0),
    _max_size(0),
    _size(0)
{
    // Should assert that slice does not exceed original array size :
    VERIFIC_ASSERT((orig._size>left) && (orig._size>right)) ;

    // Calculate the size of the resulting slice :
    _size = (left > right) ? (left-right)+1 : (right-left)+1 ;
    _max_size = (_size) ? _size : 1 ; // _max_size should never be 0

    // Copy data over :
    if (_max_size>>1) {
        // Slice is more than one element. Allocate memory :
        _data = VFC_ARRAY_NEW(void*, _max_size) ;
        // Copy data from orig._data.
        memcpy(_data, orig._data + ((left>right)?right:left), _size * sizeof(void*)) ;
    } else {
        // slice is one element.
        if ((orig._max_size)>>1) {
            // Element is in the allocated _data field. Get it from there :
             _data = (void**)*(orig._data + ((left>right)?right:left));
        } else {
            // slice one element from one element :
            // Element is in field _data
            _data = orig._data ;
        }
    }
}

Array::~Array()
{
    if (_max_size>>1) VFC_ARRAY_DELETE(_data) ;
}

/*------------------------------------------------*/

unsigned
Array::Size() const
{
    return _size ;
}

/*------------------------------------------------*/

void
Array::Reset()
{
    _size = 0 ; // Resize array to 0 elements.  Note, memory does not get freed.
}

/*------------------------------------------------*/

void
Array::ReSize(unsigned new_size)
{
    if (new_size > _max_size) {
        // Increase the allocated memory (to be able to hold up to 'new_size' data entries)
        void **old = _data ;
        _data = VFC_ARRAY_NEW(void*, new_size) ;
        if (_max_size>>1) {
            // old data is in _data[]
            memcpy(_data, old, _size * sizeof(void*)) ;
            VFC_ARRAY_DELETE(old) ;
        } else {
            // old data is in _data pointer field
            // Copy just that pointer into the first data slot
            _data[0] = old ;
        }
        _max_size = new_size ;
    } else if (new_size <= _size) {
        // Strip elements from the end
        // This is the historical functionality.
        _size = new_size ;
    } else {
        // Don't know what the user wants. new_size is larger than the currently held number of elements.
        // Safest is to assert :
        VERIFIC_ASSERT(0) ;
    }
}

/*------------------------------------------------*/
//            INSERTION METHODS
/*------------------------------------------------*/

void
Array::Insert(unsigned pos, const void *elem)
{
    // This routine overwrites the element at position 'pos'
    if (pos >= _size) {
        // We will actually insert an element. At the end. No overwrites.
        VERIFIC_ASSERT(pos == _size) ; // Never insert element more than one slot forward

        // Double the allocated memory if we are at the limit
        if (_size == _max_size) ReSize(2*_max_size) ;

        _size++ ; // indicate that we will add one element (not override)
    }
    // Else, we will overwrite an element
    if (_data && _max_size>>1) {
        _data[pos] = (void*)elem ; // store the element in the allocated data array
    } else {
        _data = (void**)elem ; // store the element in the _data pointer.
    }
}

void
Array::InsertFirst(const void *elem)
{
    // Insert element at first position.
    // This is rather slow, since we will need to shift the
    // existing elements up-ward to make space.

    // First make enough space (if we are at the limit)
    if (_size == _max_size) ReSize(2*_max_size) ;

    // Shift the existing element one upward (if there is any data)
    // And put new one in position 0
    if (_max_size>>1) {
        if (_size) memmove(_data+1, _data, _size * sizeof(void*)) ;
        *_data = (void*)elem ; // put new one in position 0
    } else {
        // we know that 'size' is 0 here (else we would have had _max_size>1 now).
        VERIFIC_ASSERT(_size==0) ;
        // No shifting over needed. (no element was inserted before yet).
        _data = (void**)elem ; // put new one in position 0
    }

    _size++ ;
}

void Array::InsertAfter(unsigned pos, const void *data)
{
    // Insert second argument specific element after
    // the first argument specific element.
    // So the new element will be at a position 'pos + 1'

    VERIFIC_ASSERT(pos < _size) ;

    // First make enough space (if we are at the limit)
    if (_size == _max_size) ReSize(2*_max_size) ;

    // Shift the existing elements those were after first argument specific element
    if (_data && _max_size>>1) {
        if ((pos+2) < _max_size) memmove(_data + pos + 2, _data + pos + 1, ((_size - pos) - 1) * sizeof(void*)) ;
        _data[pos + 1] = (void*)data ; // Put new one at position 'pos + 1'
    } else {
        // Put new one at position 'pos + 1'
        // Note : this cannot happen, since insertafter pos=0 is not allowed if size==0. Still, allow it here :
        _data = (void**)data ; // put new one in position 0
    }

    _size++ ;
}

void Array::InsertBefore(unsigned pos, const void *data)
{
    // Insert the second argument specific element before
    // first argument specific element.
    // New element is inserted at first argument specific
    // position and the existing elements from position
    // 'pos' to '_size' are shifted to make space for
    // the new element

    VERIFIC_ASSERT(pos < _size) ;

    // First make enough space (if we are at the limit)
    if (_size == _max_size) ReSize(2*_max_size) ;

    // Shift existing elements
    memmove(_data + pos + 1, _data + pos, (_size - pos) * sizeof(void*)) ;

    VERIFIC_ASSERT(_data) ;

    // Put the new one
    _data[pos] = (void*)data ;
    _size++ ;
}

void
Array::Insert(const Array *slice, unsigned from)
{
    // Insert a slice. (overwrite me from position 'from' with the _data in slice.
    // Assert that it fits for now witout need to change my _size.
    // Later, we could adjust _size if 'slice' would exceed the boundary
    if (!slice || !slice->Size()) return ;

    VERIFIC_ASSERT((slice->_size + from) <= _size) ;

    // Make sure this array is in 'array' form :
    if (!(_max_size>>1)) ReSize(2*_max_size) ;

    // Copy the data from slice into this data :
    if (slice->_max_size>>1) {
        memcpy(_data+from, slice->_data, (slice->_size)*sizeof(void*)) ;
    } else {
        if (_data) _data[from] = slice->_data ;
    }
}

/*------------------------------------------------*/
//           DELETION (REMOVE) METHODS
/*------------------------------------------------*/

void
Array::Remove(unsigned pos, unsigned num)
{
    if (!num) return ; // nothing to do.
    // Remove 'num_of_elems' elements, starting at position 'pos'

    VERIFIC_ASSERT (pos+num <= _size) ; // cannot remove a non-existing element

    _size -= num ;

    // Shift the rest num positions back
    // Note : '_size' is now the new _size ! (old_size-num)
    if (_max_size>>1) {
        memmove(_data+pos, _data+pos+num, (_size-pos)*sizeof(void*) ) ;
    } else {
        // This must be removing the one singe element that was in there
        VERIFIC_ASSERT(pos==0 && num==1 && _size==0) ;
        // no action required.
    }
}

void *
Array::RemoveLast()
{
    VERIFIC_ASSERT(_size) ;
    _size-- ; // reduce size by one
    return (_max_size>>1) ? _data[_size] : (void*)_data ; // that is the last element
}

/*------------------------------------------------*/
//          ELEMENT RETRIEVAL METHODS
/*------------------------------------------------*/

void *
Array::At(unsigned pos) const
{
    VERIFIC_ASSERT(pos < _size) ;
    return (_data && _max_size>>1) ? _data[pos] : (void*)_data ;
}

void *
Array::GetLast() const
{
    VERIFIC_ASSERT(_size) ;
    return (_data && _max_size>>1) ? _data[_size-1] : (void*)_data ;
}

void *
Array::GetFirst() const
{
    VERIFIC_ASSERT(_size) ;
    return (_data && _max_size>>1) ? _data[0] : (void*)_data ;
}

#ifndef VERIFIC_TEMPLATED_ITERATOR
unsigned
Array::Item(unsigned pos, void **item) const
{
    // return 0 if pos out of bound ; for iterators
    if (pos >= _size) return 0 ;
    *item = (_data && _max_size>>1) ? _data[pos] : (void*)_data ;
    return 1 ;
}
#endif

/*------------------------------------------------*/
//          APPEND/PREPEND METHODS
/*------------------------------------------------*/

void
Array::Append(const Array *array)
{
    if (!array || !array->Size()) return ; // nothing to do

    // Append 'array' to the end of me
    unsigned new_size = _size + array->_size ;

    // Allocate enough space if needed
    if (new_size > _max_size) {
        // Make enough space for 'new_size' elements
        ReSize(new_size) ;
    }

    // Make sure this array is in 'array' form :
    if (!(_max_size>>1)) ReSize(2*_max_size) ;

    // Append the new array's _data
    if (array->_max_size>>1) {
        memcpy(_data + _size, array->_data, array->_size * sizeof(void*)) ;
    } else {
        _data[_size] = array->_data ; // Take from the single entry in 'array'.
    }

    // update the new size :
    _size = new_size ;
}

void
Array::Prepend(const Array *array)
{
    if (!array || !array->Size()) return ; // nothing to do

    // Prepend 'array' to the beginning of me
    unsigned new_size = _size + array->_size ;

    // Allocate enough space if needed
    if (new_size > _max_size) {
        // Double the allocated memory
        ReSize(new_size) ;
    }

    // Make sure this array is in 'array' form :
    if (!(_max_size>>1)) ReSize(2*_max_size) ;

    // Now push the old _data forward
    memmove(_data + array->_size, _data, _size * sizeof(void*)) ;

    // And push the 'array' _data into the first part
    if (array->_max_size>>1) {
        memmove(_data, array->_data, array->_size * sizeof(void*)) ;
    } else {
        _data[0] = array->_data ; // Take from the single entry in 'array'.
    }

    // update new size
    _size = new_size ;
}

/*------------------------------------------------*/

unsigned long
Array::MemUsage() const
{
    return (sizeof(Array) + (_max_size * sizeof(void*))) ;
}

/*------------------------------------------------*/
