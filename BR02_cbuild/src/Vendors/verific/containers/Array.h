/*
 *
 * [ File Version : 1.52 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_ARRAY_H_
#define _VERIFIC_ARRAY_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

#ifdef VERIFIC_TEMPLATED_ITERATOR
/*CARBON_BEGIN*/
  //#define FOREACH_ARRAY_ITEM(A,I,E)       if (A) for ((I)=0;(A)->Item((I),&(E));(I)++)             // OldVerificVersion
  //#define FOREACH_ARRAY_ITEM_BACK(A,I,E)  if (A) for ((I)=(A)->Size()-1;(A)->Item((I),&(E));(I)--) // OldVerificVersion
#define FOREACH_ARRAY_ITEM(A,I,E)       if ((void*)(A) != NULL) for ((I)=0;(A)->Item((I),&(E));(I)++)
#define FOREACH_ARRAY_ITEM_BACK(A,I,E)  if ((void*)(A) != NULL) for ((I)=(A)->Size()-1;(A)->Item((I),&(E));(I)--)
/*CARBON_END*/
#else
/*CARBON_BEGIN*/
  //#define FOREACH_ARRAY_ITEM(A,I,E)       if (A) for ((I)=0;(A)->Item((I),(void**)&(E));(I)++)              // OldVerificVersion
  //#define FOREACH_ARRAY_ITEM_BACK(A,I,E)  if (A) for ((I)=(A)->Size()-1;(A)->Item((I),(void**)&(E));(I)--)  // OldVerificVersion
#define FOREACH_ARRAY_ITEM(A,I,E)      if ((void*)(A) != NULL) for ((I)=0;(A)->Item((I),(void**)&(E));(I)++) 
#define FOREACH_ARRAY_ITEM_BACK(A,I,E) if ((void*)(A) != NULL) for ((I)=(A)->Size()-1;(A)->Item((I),(void**)&(E));(I)--)
/*CARBON_END*/
#endif

/*
    This file contains the definition of Verific's Array class.  The
    Array class is a generic array container class which grows by
    necessity.
*/

/* -------------------------------------------------------------- */

// Dynamic Array storing void* elements
class VFC_DLL_PORT Array
{
public :
    // Standard constructor
    explicit Array(unsigned init_capacity=0) ;
    // Copy Constructor
    Array(const Array &orig) ;
    // Sliced Copy constructor (create a slice). Both indices are inclusive
    Array(const Array &orig, unsigned left, unsigned right) ;
    // Destructor
    ~Array() ;

private :
    // Prevent compiler from defining the following
    Array& operator=(const Array &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Size information method
    unsigned        Size() const ;                           // Return size of array (num elements)

    // Resizing methods
    void            ReSize(unsigned new_size) ;              // Make array smaller (drop data on new_size and higher positions). This is fast.
    void            Reset() ;                                // Same as ReSize(0)

    // Insertion methods
    // True inserts : do not overwrite existing elements :
    void            Insert(const void *elem) { Insert(_size, elem) ; }     // Alias for 'InsertLast'
    void            InsertLast(const void *elem) { Insert(_size, elem) ; } // Add one element, at the end
    void            InsertFirst(const void *elem) ;               // Add new element at FIRST position ! Move all other ones up (slow)
    void            InsertBefore(unsigned pos, const void *data); // Add new element before the element at position 'pos'
    void            InsertAfter(unsigned pos, const void *data);  // Add new element after the element at position 'pos'
    // Overwrite insertion (or add at the end)
    void            Insert(unsigned pos, const void *elem) ;      // Overwrite element 'pos' (or add at the end)
    void            Insert(const Array *slice, unsigned from) ;   // Overwrite from position 'pos' with data from 'slice'

    // Deletion (remove) methods
    void            Remove(unsigned pos, unsigned num=1) ;   // Remove 'num' elements, starting at position 'pos'. This re-organizes data above pos+num position.
    void *          RemoveLast() ;                           // Get last item and remove from array

    // Element retrieval methods.
    // NOTE : These routines call 'VERIFIC_ASSERT' if you request a non-existing position !
    //        That is done to distinguish between a valid position with value 0, and a non-existing position.
    void *          At(unsigned pos) const ;                 // Get an item by position (index)
    void *          GetLast() const ;                        // Get item in last position (index = size - 1)
    void *          GetFirst() const ;                       // Get item in first position (index = 0)
#ifdef VERIFIC_TEMPLATED_ITERATOR
    template<class T> unsigned Item(unsigned pos, T *item) const ; // Return item via the 'item' argument
#else
    unsigned        Item(unsigned pos, void **item) const ;  // Return item via the 'item' argument.
#endif

    // Append/prepend methods
    void            Append(const Array *array) ;             // Append contents of incoming array to this array
    void            Prepend(const Array *array) ;            // Prepend contents of incoming array to this array

    // Memory usage method
    unsigned long   MemUsage() const ;                       // Return sizeof(Array) + allocated array size(not including elements' size)

private :
    void      **_data ;     // Allocated memory
    unsigned    _max_size ; // Max number of elements current memory allocation can hold
    unsigned    _size ;     // Size (num of elements) of array
} ; // class Array

/* -------------------------------------------------------------- */

#ifdef VERIFIC_TEMPLATED_ITERATOR
template<class T> unsigned Array::Item(unsigned pos, T *item) const // Return item via the 'item' argument
{
    // return 0 if pos out of bound ; for iterators
    if (pos >= _size) return 0 ;
    // JJ 131017: null check _data for corner case
    *item = (_data && _max_size>>1) ? (T) _data[pos] : (T)_data ; // Viper #5241 : rectified for array with 1 element
    return 1 ;
}
#endif // VERIFIC_TEMPLATED_ITERATOR

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_ARRAY_H_

