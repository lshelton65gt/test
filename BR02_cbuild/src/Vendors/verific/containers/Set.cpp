/*
 *
 * [ File Version : 1.51 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <memory.h>  // For memcpy/memset

#include "Set.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Note that initial_bucket_size * FILL_RATE _entries will be allocated
// with FILL_RATE=1, INITIAL_TABLE_SIZE=5, 112 bytes are allocated
static const unsigned FILL_RATE = 1 ;
// To minimize memory, don't use _buckets uptill this _size
static const unsigned MAX_DIRECT_SIZE = 5 ;

/*------------------------------------------------*/

#define HASH_IDX(KEY) (_compare_func((KEY),0) % _num_buckets)

/*------------------------------------------------*/

Set::Set(hash_type type, unsigned init_bucket_size)
  : _compare_func(0),
    _buckets(0),
    _num_buckets((init_bucket_size) ? init_bucket_size : 2),
    _size(0),
    _entry_size(0),
    _entries(0),
    _garbage(0)
{
    switch (type) {
    case NUM_HASH :
        _compare_func = Hash::NumCompare ;
        break ;
    case POINTER_HASH :
        _compare_func = Hash::PointerCompare ;
        break ;
    case STRING_HASH :
        _compare_func = Hash::StringCompare ;
        break ;
    case STRING_HASH_CASE_INSENSITIVE :
        _compare_func = Hash::StringCaseInsensitiveCompare ;
        break ;
    case USER_DEFINED_HASH : // Intentional fallthrough to default
    default :
        _compare_func = 0 ;
        VERIFIC_ASSERT(0) ;
    }

    VERIFIC_ASSERT(_compare_func) ;

    if (_num_buckets > MAX_DIRECT_SIZE) {
        _buckets = VFC_ARRAY_NEW(SetItem*, _num_buckets) ;
        // Initialize _buckets with 0
        ::memset(_buckets, 0, _num_buckets * sizeof(SetItem*)) ;
    } else {
        // Work with linear search for now. No _buckets needed.
        _buckets = 0 ;
    }

     // Create FILL_SIZE*num_num_buckets _entries
    _entries = new SetItem[_num_buckets*FILL_RATE] ;
}

/*------------------------------------------------*/

Set::Set(unsigned long (*compare)(const void*, const void*), unsigned init_bucket_size)
  : _compare_func(compare),
    _buckets(0),
    _num_buckets((init_bucket_size) ? init_bucket_size : 2),
    _size(0),
    _entry_size(0),
    _entries(0),
    _garbage(0)
{
    VERIFIC_ASSERT(_compare_func) ;

    if (_num_buckets > MAX_DIRECT_SIZE) {
        _buckets = VFC_ARRAY_NEW(SetItem*, _num_buckets) ;
        // Initialize _buckets with 0
        ::memset(_buckets, 0, _num_buckets * sizeof(SetItem*)) ;
    } else {
        // Work with linear search for now. No _buckets needed.
        _buckets = 0 ;
    }

    // Create FILL_SIZE*num_num_buckets _entries
    _entries = new SetItem[_num_buckets * FILL_RATE] ;
}

/*------------------------------------------------*/

Set::~Set()
{
    if (_buckets) VFC_ARRAY_DELETE(_buckets) ;
    delete [] _entries ; // NOTE: Don't replace with VFC_ARRAY_DELETE, this is a SetItem array
}

// **************************************************************/
// Functionality
// **************************************************************/

unsigned
Set::Size() const
{
    return _size ;
}

/*------------------------------------------------*/

unsigned
Set::Insert(const void *key, unsigned force_overwrite)
{
    register unsigned long idx = 0 ;
    register SetItem *he ;

    if (!key) return 0 ; // Insert failed

    if (_size == _num_buckets*FILL_RATE) Rehash() ;

    if (_buckets) {
        // Find the hash entry
        idx = HASH_IDX(key) ;
        he = _buckets[idx] ;

         // Run over the list and check existing
        for (;he;he=he->_next) {
            if (_compare_func(key,he->_key)) {
                // Already there
                if (force_overwrite) {
                    he->_key = (void*)key ;
                }
                return 0 ;
            }
        }
    } else {
        // Search the _entries directly
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->_key) continue ; // _garbage list item
            if (_compare_func(key,he->_key)) {
                if (force_overwrite) {
                    he->_key = (void*)key ;
                }
                return 0 ;
            }
        }
    }

    // Now get a new SetItem.
    if (_garbage) {
        // pop the _garbage list
        he = _garbage ;
        _garbage = _garbage->_next ;
    } else {
        // Otherwise, get the _next one from the _entries array
        he = &(_entries[_entry_size++]) ;
    }

    // Initialize the structure
    he->_key = (void*)key ;

    if (_buckets) {
        // OK. Insert in the bucket list. Up-front is easiest
        he->_next = _buckets[idx] ;
        _buckets[idx] = he ;
    }

    _size++ ;
    return 1 ;
}

/*------------------------------------------------*/

unsigned
Set::Remove(const void *key)
{
    unsigned long idx ;
    SetItem *he, *last ;

    if (!key) return 0 ;

    // Remove item from the table.
    // GOTCHA here : If someone is iterating over the
    // list and is at this point at this item, then
    // the iterator will access freed memory, to get '_next' item.
    if (_buckets) {
        // Find the hash entry
        idx = HASH_IDX(key) ;
        he = _buckets[idx] ;

        // Run over the list and check existing
        last = 0 ;
        for (;he;he=he->_next) {
            if (_compare_func(key,he->_key)) {
                // Got it !
                if (last) {
                    last->_next = he->_next ;
                } else {
                    // First entry in bucket
                    _buckets[idx] = he->_next ;
                }
                // Push this entry on the _garbage list
                he->_next = _garbage ;
                _garbage = he ;

                // Set the key to 0 so iterator can skip this
                he->_key = 0;

                _size-- ;
                return 1 ; // Remove worked !
            }
            last = he ;
        }
    } else {
        // Search the _entries directly
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->_key) continue ; // _garbage list item
            if (_compare_func(key,he->_key)) {
                // Got it. Push on _garbage list
                he->_next = _garbage ;
                _garbage = he ;
                he->_key = 0 ;
                _size-- ;
                return 1 ; // Remove worked
            }
        }
    }
    return 0 ; // Entry is not in the table
}

/*------------------------------------------------*/

void *
Set::Get(const void *key) const
{
    SetItem *si = GetItem(key) ;
    return (si) ? si->Key() : 0 ;
}

/*------------------------------------------------*/

SetItem *
Set::GetItem(const void *key) const
{
    register SetItem *he ;

    if (!key) return 0 ; // It's not there

    if (_buckets) {
        // Find the hash entry
        he = _buckets[HASH_IDX(key)] ;

        // Run over the list and check existing
        for (;he;he=he->_next) {
            if (_compare_func(key,he->_key)) {
                // Got it !
                return he ;
            }
        }
    } else {
        // Search the _entries directly
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->_key) continue ; // _garbage list item
            if (_compare_func(key,he->_key)) {
                // Got it
                return he ;
            }
        }
    }
    return 0 ; // Can't find it
}

/*------------------------------------------------*/

void *
Set::GetAt(unsigned idx) const
{
    // Get key by index (or order of insertion). Just like an array
    VERIFIC_ASSERT (idx < _size) ;
    if (!_garbage) {
        // Garbage list is empty. Access directly
        return _entries[idx]._key ;
    }
    // Oops. There are _garbage items.
    // Cant get to my index directly
    // Need to count. Start at end or begin depending on how
    // close 'idx' in to either _size.
    SetItem *he ;
    if (idx > _size/2) {
        // Start at end
        for (he = _entries+_entry_size; he-- != _entries;) {
            if (!he->_key) continue ;
            if (++idx == _size) return he->_key ;
        }
    } else {
        // Start at beginning
        for (he = _entries; he != _entries+_entry_size; he++) {
            if (!he->_key) continue ;
            if (idx-- == 0) return he->_key ;
        }
    }
    return 0 ; // Can this happen ? Should we assert ?
}

/*------------------------------------------------*/

void *
Set::GetLast() const
{
    if (_size==0) return 0 ;
    return GetAt(_size-1) ;
}

/*------------------------------------------------*/

void
Set::Rehash()
{
    unsigned long idx ;
    register SetItem *he, *new_entries ;

    // If you understand why these asserts hold,
    // you can relax since you understand the algorithms here
    VERIFIC_ASSERT (_entry_size == _size) ;
    VERIFIC_ASSERT (_entry_size == (FILL_RATE * _num_buckets)) ;
    VERIFIC_ASSERT (!_garbage) ;

    // Define new number of _buckets : 2*N+1 has a good prime number hit rate
    _num_buckets = _num_buckets * 2 + 1 ;

    // Create a new _entries array (as always : FILL_RATE*_num_buckets)
    new_entries = new SetItem[FILL_RATE * _num_buckets] ;
    // Copy the existing _entries in there
    ::memcpy(new_entries, _entries, _entry_size * sizeof(SetItem)) ;
    // Swap old for new _entries. Careful with memory here
    delete [] _entries ; // NOTE: Don't replace with VFC_ARRAY_DELETE, this is a SetItem array
    _entries = new_entries ;

    // Create new _buckets array
    if (_num_buckets > MAX_DIRECT_SIZE) {
        // Delete the old bucket array
        VFC_ARRAY_DELETE(_buckets) ;
        _buckets = VFC_ARRAY_NEW(SetItem*, _num_buckets) ;
        // Initialize with 0
        ::memset(_buckets,0,_num_buckets * sizeof(SetItem*)) ;

        // Rehash each entry into new _buckets
        // Use the new _entries array to get to each one easily
        // Aint this beautiful ?
        for (he = _entries+_entry_size ; he-- != _entries ; ) {
            idx = HASH_IDX(he->_key) ;
            he->_next = _buckets[idx] ;
            _buckets[idx] = he ;
        }
    }
}

/*------------------------------------------------*/

void
Set::Reset()
{
    // Just like a 'Remove()' for each entry.
    SetItem *he, *last ;

    // Clear the table
    if (_buckets) {
        ::memset(_buckets, 0, _num_buckets * sizeof(SetItem*)) ;
    }

    // Put all _entries on the _garbage list
    last = 0 ;
    for (he = _entries + _entry_size ; he-- != _entries ; ) {
        he->_next = last ;
        he->_key = 0 ;
        last = he ;
    }
    _garbage = last ;

    // ReSet the _size counter. _entry_size remains where it is,
    // as does _num_buckets.
    _size = 0 ;
}

/*------------------------------------------------*/

unsigned
Set::IndexOfItem(const void *key) const
{
    // This is easy and fast if there is no _garbage list
    // Slower if there is _garbage
    SetItem *item = GetItem(key) ;
    if (!item) return 0 ; // or whatever

    unsigned diff = (unsigned)(item - _entries) ;

    // If there is _garbage,
    // the _garbage elements that are in index smaller than 'item'
    // should be subtracted from 'diff'
    SetItem *runner = _garbage ;
    while (runner) {
        if (runner < item) diff-- ;
        runner = runner->_next ;
    }
    return diff ;
}

/*------------------------------------------------*/

hash_type
Set::HashType() const
{
    VERIFIC_ASSERT(_compare_func) ;

    // Return the type of hash this Map is using
    if (_compare_func == Hash::NumCompare) {
        return NUM_HASH ;
    } else if (_compare_func == Hash::PointerCompare) {
        return POINTER_HASH ;
    } else if (_compare_func == Hash::StringCompare)  {
        return STRING_HASH ;
    } else if (_compare_func == Hash::StringCaseInsensitiveCompare)  {
        return STRING_HASH_CASE_INSENSITIVE ;
    } else {
        return USER_DEFINED_HASH ;
    }
}

/*------------------------------------------------*/

unsigned long
Set::MemUsage() const
{
    // Count the allocated, but not yet used, memory in too
    return (
                sizeof(Set) +                                          // struct itself
                ((_buckets) ? (_num_buckets * sizeof(SetItem*)) : 0) + // _buckets
                (FILL_RATE * _num_buckets * sizeof(SetItem))           // _entries
           ) ;
}

/*------------------------------------------------*/

#if 0
void
Set::PrintStats() const
{
    unsigned filled_num_buckets = 0 ;
    unsigned single_item = 0 ;
    unsigned max_length = 0 ;
    unsigned i, len ;
    SetItem *he ;

    cout << "Set: have " << _size << " entered items, using " <<
            MemUsage() << " bytes. So " <<
            ((_size) ? MemUsage()/_size : 0) << " bytes/entry\n" ;

    cout << _entry_size << " out of " << (_num_buckets*FILL_RATE) << " _entries capacity used\n";
    cout << "of which there are " << (_entry_size-_size) << " _entries on the _garbage list\n" ;
    if (_buckets) {
        for (i=0;i<_num_buckets;i++) {
            he = _buckets[i] ;
            for (len=0;he;he=he->_next) len++ ;

            if (len>0) filled_num_buckets++ ;
            if (len==1) single_item++ ;
            if (len>max_length) max_length=len ;
        }
        cout << filled_num_buckets << " _buckets out of " <<
             _num_buckets << " filled : " <<
             (100*filled_num_buckets/_num_buckets) << " % filling\n" ;
        cout << (filled_num_buckets?(100*single_item)/filled_num_buckets:0) <<
          " % of filled num _buckets are single-item\n" ;
        cout << "Average list length random key : " << (filled_num_buckets?(double)(_size)/(double)(_num_buckets):0) << " \n" ;
        cout << "Average list length existing key : " << (filled_num_buckets?(double)(_size)/(double)(filled_num_buckets):0) << " \n" ;
        cout << "Maximum list length : " << max_length << "\n" ;
    } else {
        cout << "Set uses linear search to access _entries. No _buckets allocated" << endl ;
    }
}
#endif

/**************************************************/
// Iterator
/**************************************************/

// Have to do all this with an index, since
// the Set might change under me by Rehashing, removing etc.
// between calls to the iterator.
// Garbage list handling makes sure that the iterator will
// never access freed memory, no matter how much you
// remove or insert in the Set during iterator.

unsigned
SetIter::First(const Set *s)
{
    // Sets iterator to first item. Returns its index (always 0)
    // This is the starter for forward iteration
    _set = const_cast<Set*>(s) ;
    if (!_set) return 0 ;
    // Skip the _garbage list items
    for (_idx=0;_idx<_set->_entry_size;_idx++) {
        if ((&(_set->_entries[_idx]))->Key()) break ;
    }
    _real_index = 0 ;
    return _real_index ;
}

/*------------------------------------------------*/

unsigned
SetIter::Last(const Set *s)
{
    // Sets iterator to last item. Returns its index
    // This is the starter for backward iteration
    _set = const_cast<Set*>(s) ;
    if (!_set) return 0 ;
    // Skip the _garbage list items
    _idx = _set->_entry_size ;
    // Carefull for underflow of '_idx' I want index '0' too
    while (_idx--!=0) {
        if ((&(_set->_entries[_idx]))->Key()) break ;
    }
    _real_index = _set->_size - 1 ;
    return _real_index ;
}

/*------------------------------------------------*/

unsigned
SetIter::Next()
{
    // Sets iterator to _next item. Returns its index. (could be one too high for the list !)
    if (!_set) return 0 ;
    // Skip the _garbage list items
    while (++_idx != _set->_entry_size) {
        if ((&(_set->_entries[_idx]))->Key()) break ;
    }
    return ++_real_index ;
}

/*------------------------------------------------*/

unsigned
SetIter::Previous()
{
    // Sets iterator to previous item. Returns its index (could be one too low for the list !)
    if (!_set) return 0 ;
    // Skip the _garbage list items
    // Carefull for underflow of '_idx' I want index '0' too
    while (_idx--!=0) {
        if ((&(_set->_entries[_idx]))->Key()) break ;
    }
    return --_real_index ;
}

/*------------------------------------------------*/

#ifndef VERIFIC_TEMPLATED_ITERATOR
unsigned
SetIter::Item(void **key) const
{
    // Sets key from this item. Returns 0 if end of list. 1 normally
    if (!_set) return 0 ;
    if (_idx>=_set->_entry_size) return 0 ;

    SetItem *he = &(_set->_entries[_idx]) ;
    if (key) *key = he->Key() ;
    return 1 ;
}
#endif

/*------------------------------------------------*/

SetItem *
SetIter::GetSetItem() const
{
    // Get current item in the iterator. Returns 0 if end of list.
    if (!_set) return 0 ;
    if (_idx>=_set->_entry_size) return 0 ;

    SetItem *he = &(_set->_entries[_idx]) ;
    return he ;
}

/*------------------------------------------------*/

