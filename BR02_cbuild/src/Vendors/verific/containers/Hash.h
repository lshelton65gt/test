/*
 *
 * [ File Version : 1.37 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_HASH_H_
#define _VERIFIC_HASH_H_

#include "VerificSystem.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

typedef enum enum_hash_type
{
    USER_DEFINED_HASH,
    STRING_HASH,
    STRING_HASH_CASE_INSENSITIVE,
    POINTER_HASH,
    NUM_HASH
} hash_type ;

#ifdef WINDOWS
// Windows file system is case-insensitive
#define STRING_HASH_FILE_PATH STRING_HASH_CASE_INSENSITIVE
#else
#define STRING_HASH_FILE_PATH STRING_HASH
#endif
/* -------------------------------------------------------------- */

// Base class for hash tables (hash functions)
class VFC_DLL_PORT Hash
{
private :
    // This class should never get instantiated (make these private)
    Hash() ;
    ~Hash() ;

    // Prevent compiler from defining the following
    Hash(const Hash &) ;            // Purposely leave unimplemented
    Hash& operator=(const Hash &) ; // Purposely leave unimplemented

public :
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Compare methods
    static unsigned long    NumCompare(const void *key1, const void *key2) ;
    static unsigned long    PointerCompare(const void *key1, const void *key2) ;
    static unsigned long    StringCompare(const void *key1, const void *key2) ;
    static unsigned long    StringCaseInsensitiveCompare(const void *key1, const void *key2) ;
} ; // class Hash

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_HASH_H_

