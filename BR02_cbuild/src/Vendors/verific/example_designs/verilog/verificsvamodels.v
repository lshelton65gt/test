/*
 *
 * [ File Version : 1.15 - 2009/08/21 23:04:33 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

// SVA Directives

module sva_immediate_assert (c); input c; endmodule
module sva_assert (c); input c; endmodule
module sva_cover (c); input c; endmodule
module sva_assume (c); input c; endmodule
module sva_expect (c); input c; endmodule

// Unary SVA Operators

module sva_posedge (o, i); output o; input i; endmodule
module sva_not (o, i); output o; input i; endmodule
module sva_first_match (o, i); output o; input i; endmodule
module sva_ended (o, i); output o; input i; endmodule
module sva_matched (o, i); output o; input i; endmodule
module sva_consec_repeat (o, i); output o; input i; endmodule
module sva_non_sonsec_repeat (o, i); output o; input i; endmodule
module sva_goto_repeat (o, i); output o; input i; endmodule
module sva_match_item_trigger (o, i); output o; input i; endmodule

// Binary SVA Operators

module sva_and (o, a1, a0); output o; input a1, a0; endmodule
module sva_or (o, a1, a0); output o; input a1, a0; endmodule
module sva_seq_and (o, a1, a0); output o; input a1, a0; endmodule
module sva_seq_or (o, a1, a0); output o; input a1, a0; endmodule
module sva_event_or (o, a1, a0); output o; input a1, a0; endmodule
module sva_overlapped_impl (o, a1, a0); output o; input a1, a0; endmodule
module sva_non_overlapped_impl (o, a1, a0); output o; input a1, a0; endmodule
module sva_intersect (o, a1, a0); output o; input a1, a0; endmodule
module sva_throughout (o, a1, a0); output o; input a1, a0; endmodule
module sva_within (o, a1, a0); output o; input a1, a0; endmodule
module sva_at (o, a1, a0); output o; input a1, a0; endmodule
module sva_disable_iff (o, a1, a0); output o; input a1, a0; endmodule
module sva_sampled (o, a1, a0); output o; input a1, a0; endmodule
module sva_rose (o, a1, a0); output o; input a1, a0; endmodule
module sva_fell (o, a1, a0); output o; input a1, a0; endmodule
module sva_stable (o, a1, a0); output o; input a1, a0; endmodule
module sva_past (o, a1, a0); output o; input a1, a0; endmodule
module sva_match_item_assign (o, a1, a0); output o; input a1, a0; endmodule
module sva_seq_concat (o, a1, a0); output o; input a1, a0; endmodule

// Ternary SVA Operators

module sva_if (o, a1, a0, c); output o; input a1, a0, c; endmodule

