/*
 *
 * [ File Version : 1.7 - 2009/08/21 23:10:45 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
*/

module VERIFIC_BUF (i, o);
input i;
output o;
buf (o, i);
endmodule


module VERIFIC_INV (i, o);
input i;
output o;
not (o, i);
endmodule


module VERIFIC_XOR (a0, a1, o);
input a0, a1;
output o;
xor (o, a0, a1);
endmodule


module VERIFIC_OR (a0, a1, o);
input a0, a1;
output o;
or (o, a0, a1);
endmodule


module VERIFIC_AND (a0, a1, o);
input a0, a1;
output o;
and (o, a0, a1);
endmodule


module VERIFIC_MUX (a0, a1, c, o);
input a0, a1, c;
output o;
assign o = c ? a1 : a0;
endmodule


module VERIFIC_DFF (d, clk, async_cond, async_val, q) ;
input d ;
input clk ;
input async_cond ;
input async_val ;
output q ;
reg q ;
wire s, r ;
assign s = async_cond & async_val ;
assign r = async_cond & !async_val ;
always @ (posedge r or posedge s or posedge clk) begin
   if (r) 
      q <= 1'b0 ;
   else if (s) 
      q <= 1'b1 ;
   else // posedge (clk)
      q <= d ;
end
endmodule


module VERIFIC_DFFRS (d, clk, s, r, q) ;
input d ;
input clk ;
input s ;
input r ;
output q ;
reg q ;
always @ (posedge r or posedge s or posedge clk) begin
   if (r) 
      q <= 1'b0 ;
   else if (s) 
      q <= 1'b1 ;
   else // posedge (clk)
      q <= d ;
end
endmodule


module VERIFIC_DLATCH (d, gate, async_cond, async_val, q) ;
input d ;
input gate ;
input async_cond ;
input async_val ;
output q ;
reg q ;
wire s, r ;
assign s = async_cond & async_val ;
assign r = async_cond & !async_val ;
always @ (r or s or gate or d) begin
   if (r) 
      q <= 1'b0 ;
   else if (s) 
      q <= 1'b1 ;
   else if (gate) 
      q <= d ;
end
endmodule


module VERIFIC_DLATCHRS (d, gate, s, r, q) ;
input d ;
input gate ;
input s ;
input r ;
output q ;
reg q ;
always @ (r or s or gate or d) begin
   if (r) 
      q <= 1'b0 ;
   else if (s) 
      q <= 1'b1 ;
   else if (gate) 
      q <= d ;
end
endmodule


module VERIFIC_FADD (cin, a, b, o, cout) ;
input cin ;
input a ;
input b ;
output o ;
output cout ;
assign o = (a ^ b) ^ cin ;
assign cout = cin ? (a | b) : (a & b) ;
endmodule


module VERIFIC_PWR (o) ;
output o;
assign o = 1'b1;
endmodule


module VERIFIC_GND (o) ;
output o;
assign o = 1'b0;
endmodule


module VERIFIC_X (o) ;
output o;
assign o = 1'bX;
endmodule


module VERIFIC_Z (o) ;
output o;
assign o = 1'bZ;
endmodule


module VERIFIC_NAND (a0, a1, o);
input a0, a1;
output o;
nand (o, a0, a1);
endmodule


module VERIFIC_NOR (a0, a1, o);
input a0, a1;
output o;
nor (o, a0, a1);
endmodule


module VERIFIC_XNOR (a0, a1, o);
input a0, a1;
output o;
xnor (o, a0, a1);
endmodule


module VERIFIC_PULLUP (o) ;
output o;
pullup (o);
endmodule


module VERIFIC_PULLDOWN (o) ;
output o;
pulldown (o);
endmodule


module VERIFIC_TRI (o, c, i) ;
input c, i;
output o;
bufif1 (o, i, c);
endmodule


module VERIFIC_NMOS (o, c, d) ;
input c, d;
output o;
nmos (o, d, c);
endmodule


module VERIFIC_RNMOS (o, c, d) ;
input c, d;
output o;
rnmos (o, d, c);
endmodule


module VERIFIC_PMOS (o, c, d) ;
input c, d;
output o;
pmos (o, d, c);
endmodule


module VERIFIC_RPMOS (o, c, d) ;
input c, d;
output o;
rpmos (o, d, c);
endmodule


module VERIFIC_CMOS (o, nc, pc, d) ;
input nc, pc, d;
output o;
cmos (o, d, nc, pc);
endmodule


module VERIFIC_RCMOS (o, nc, pc, d) ;
input nc, pc, d;
output o;
rcmos (o, d, nc, pc);
endmodule


module VERIFIC_TRAN (inout1, inout2, control) ;
input control;
inout inout1, inout2;
tranif1 (inout1, inout2, control);
endmodule


module VERIFIC_RTRAN (inout1, inout2, control) ;
input control;
inout inout1, inout2;
tranif1 (inout1, inout2, control);
endmodule


