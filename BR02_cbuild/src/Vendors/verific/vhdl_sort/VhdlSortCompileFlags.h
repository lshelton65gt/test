/*
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDLSORT_COMPILEFLAGS_H_
#define _VERIFIC_VHDLSORT_COMPILEFLAGS_H_

/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
 |                                                                                  |
 |    This header file contains all vhdlsort-related compile flags. These flags     |
 |    can either be manually set (#defined) within this file, or they can be set    |
 |    from the Makefile/(Msdev project settings) environment.  If you want to       |
 |    control compile flag settings from the Makefile/(Msdev project settings)      |
 |    environment, then make sure all #define's are commented out below.            |
 |                                                                                  |
  \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

/*-------------------------------------------------------------------------------------
    VERIFIC_NAMESPACE : (active in all projects)
  -------------------------------------------------------------------------------------
    This flag is controlled in the util/VerificSystem.h header file.  Please go
    there for a detailed description.
*/

/*-------------------------------------------------------------------------------------
    VHDLSORT_CONSIDER_VHDL_LIBRARY_MAPPING
  -------------------------------------------------------------------------------------
    Normally VHDL sort does not depend on the physical mapping of the VHDL libraries.
    It just cares about the logical libraries defined in the source files while sorting
    a set of VHDL files. Turning this flag ON will make the VHDL sorter depend on the
    physical library maps specified for VHDL. This maps include the "default" library
    path and the "associated" library path. The reason the sorter needs to depend on
    them is that if two or more different logical libraries are mapped into a single
    physical library, it treats the logical libraries to be same. For example in a
    situation like this:

       -----------------------------------
       | Logical library | Physical path |
       | ----------------|---------------|
       |    work         |    work       |
       |    library_1    |    work       |
       |    library_2    |    work1      |
       |    "default"    |    work1      |
       -----------------------------------

    The sorter looks for "work.entity1" into "library_1" also if "entity1" is not found
    in the "work" library, since both "work" and "library_1" shares the same physical
    library path. Similarly it will look for "library_2.entity2" into all the logical
    libraries other than "work" and "library_1" if "entity2" is not found in the library
    "library_2" for the same reason (assuming no other mapping specified).

    The VHDL side of this functionality is implemented due to VIPER #2823 and in the
    VHDL sort side it is VIPER #2933. Please look into the issue for more details at:
    https://viper.verific.com/show_bug.cgi?bug_id=2933.

*/
//#define VHDLSORT_CONSIDER_VHDL_LIBRARY_MAPPING // <--- Comment this line out to control flag from build environment, or uncomment to force on.

/*-------------------------------------------------------------------------------------
    VHDLSORT_FLEX_READ_FROM_STREAM (VIPER #6493)
  -------------------------------------------------------------------------------------
    Historically, flex uses FILE * to read from input files. With this compile flag
    user can make flex read from (C++) streams instead of (C) FILE *.

    Verific has defined a new abstract stream class 'verific_stream'. Flex uses object
    of this class to read from streams. Another class 'verific_istream' is defined and
    it is derived from 'verific_stream'. This class takes a C++ stream object and reads
    from it.

    So, to simply read from an istream (or derived) class, use 'verific_istream' with
    that object and flex will read from that stream.

    To read from other sources, you have to do the following:
      1) Derive your own class from 'verific_stream'
      2) Define a function that takes a const char *file_name and returns an object
         of your derived class.
      3) Register this function with vhdl_sort::RegisterFlexStreamCallBack() and you
         are done. Now, flex will read from your stream.

    For an example, we implemented reading from gzipped files as an application in Verilog side.
    Please check example_apps/verilog_apps/stream_apps directory for more details.
*/
//#define VHDLSORT_FLEX_READ_FROM_STREAM // <--- Comment this line out to control flag from build environment, or uncomment to force on.

#endif // #ifndef _VERIFIC_VHDLSORT_COMPILEFLAGS_H_

