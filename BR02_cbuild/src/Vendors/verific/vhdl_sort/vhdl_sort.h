/*
 *
 * [ File Version : 1.32 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_SORT_H_
#define _VERIFIC_VHDL_SORT_H_

#include "VerificSystem.h"
#include "VhdlSortCompileFlags.h"
#include "LineFile.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Set ;
class Map ;
class Array ;
#ifdef VHDLSORT_FLEX_READ_FROM_STREAM
class verific_stream ;
#endif

class VhdlFileElement ;
class VhdlFileDependency ;

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

// To iterate over all files in sorted order (dependency satisfied) :
// use FOREACH_SORTED_FILE(I, FILE_NAME, LIBRARY_NAME) :
// (This iterator internally sorts the list of registered files)
//       -------------------------------------------------------------------
//         argument         type          function
//       -------------------------------------------------------------------
//       I             :   unsigned      Array iterator
//       FILE_NAME     :   const char *  Returned Pointer to the file name
//       LIBRARY_NAME  :   const char *  Returned Pointer to the library name
//       VHDL_MODE     :   unsigned      Returned unsigned VHDL analyze mode number
//
#define FOREACH_SORTED_FILE(I,FILE_NAME,LIBRARY_NAME,VHDL_MODE)      for (VN::vhdl_sort::Sort(), (I)=0; VN::vhdl_sort::FileAt((I), &(FILE_NAME), &(LIBRARY_NAME), &(VHDL_MODE)); (I)++)
#define FOREACH_SORTED_FILE_BACK(I,FILE_NAME,LIBRARY_NAME,VHDL_MODE) for (VN::vhdl_sort::Sort(), (I)=(VN::vhdl_sort::Size()-1; (VN::vhdl_sort::FileAt((I), &(FILE_NAME), &(LIBRARY_NAME), &(VHDL_MODE)); (I)--)

// To iterate over all the top level design units from the registered files :
// use FOREACH_TOP_UNIT(I, LIBRARY_NAME, TOP_UNIT_NAME) :
// (This iterator internally determines the top unit from the list of registered files)
//       -------------------------------------------------------------------
//         argument         type          function
//       -------------------------------------------------------------------
//       I             :   unsigned      Array iterator
//       LIBRARY_NAME  :   const char *  Returned Pointer to the library name
//       TOP_UNIT_NAME :   const char *  Returned Pointer to the top unit name
//
#define FOREACH_TOP_UNIT(I,LIBRARY_NAME,TOP_UNIT_NAME)      for (VN::vhdl_sort::DetermineTopUnits(), (I)=0; VN::vhdl_sort::TopUnitAt((I), &(LIBRARY_NAME), &(TOP_UNIT_NAME)); (I)++)
#define FOREACH_TOP_UNIT_BACK(I,LIBRARY_NAME,TOP_UNIT_NAME) for (VN::vhdl_sort::DetermineTopUnits(), (I)=(VN::vhdl_sort::NumTopUnits()-1; (VN::vhdl_sort::TopUnitAt((I), &(LIBRARY_NAME), &(TOP_UNIT_NAME)); (I)--)

/* -------------------------------------------------------------- */

/*
    vhdl_sort is a placeholder to call the VHDL file dependency sorter.
    Typical calling sequence is as follows :

    vhdl_sort::RegisterFile("vhdl_file_name1", "work", vhdl_sort::VHDL_93) ;
    vhdl_sort::RegisterFile("vhdl_file_name2", "work", vhdl_sort::VHDL_2008) ;
    // More files can be registerd here
    ...

    // Now to access the sorted files, use this macro:
    unsigned i ;
    const char *file_name ;
    const char *library ;
    unsigned vhdl_mode ;
    FOREACH_SORTED_FILE(i, file_name, library, vhdl_mode) {
        // Process the file 'file_name' which can be analyzed into library 'library'
        // Normally 'Analyze()' is called to analyze the file into the library
        ...
    }

    In absence of the 'lib_name' name in the RegisterFile command, the library
    called "work" will be used to register the VHDL file into the library.

    The macro internally calls the routine Sort() to sort the registered files
    and then it iterates over that sorted files. The Sort() can be called explicitly.

    Once the list is sorted, calling Sort() will not try to sort it again until another
    file is registered to be sorted.

    Applications should call the ClearRegisteredFiles() routine to clear all registered
    files after they are processed. This will clean up the registered files in the list.

    The sorting algorithm sorts the registered files within themselves. If some of
    the dependencies are not satisfied for some files, then analyzing the files in
    the sorted order may generate error because of use-before-declaration. This is
    true even for the vhdl package 'standard'. So, to get a fully dependency
    satisfied sorted list of files all the files must be registered first.
*/

class VFC_DLL_PORT vhdl_sort
{
private:
    vhdl_sort() { } ;
    ~vhdl_sort() { } ;

    // Prevent compiler from defining the following
    vhdl_sort(const vhdl_sort &) ;            // Purposely leave unimplemented
    vhdl_sort& operator=(const vhdl_sort &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // VIPER #6295: VHDL supported dialect enum (exactly same as VHDL side):
    enum { VHDL_93=0, VHDL_87=1, VHDL_2K=2, VHDL_2008=3, VHDL_PSL=6, UNDEFINED=7 } ;

    // Add a file to be sorted in a list (VIPER #6295: Also take a vhdl dialect mode)
    static unsigned      RegisterFile(const char *file_name, const char *lib_name = "work", unsigned vhdl_mode=VHDL_93) ;

    // VIPER #7676: Register directory and file extension using which we should pick files by name of unresolved units:
    static unsigned      RegisterDir(const char *dir_name, const char *lib_name = "work", unsigned vhdl_mode=VHDL_93) ;

    // Sort the registered files
    static unsigned      Sort() ;

    // Determine the top level design units among the registered files
    static unsigned      DetermineTopUnits() ;

    // Returns the sorted list of registered file (internally calls Sort() to sort the list)
    static const Array * GetSortedFiles()  { (void) Sort() ; return _registered_files ; }

    // Access to registered files/list, used by Verific internally for the iterator.
    static unsigned      Size() ; // Returns the number of files in the registered list. This is required for the reverse-iterator of the registered file
    static unsigned      FileAt(unsigned pos, const char **file, const char **library, unsigned *vhdl_mode) ; // Returns 0 if end of list, 1 normally. Sets the file and library names

    // Access to top level design units, used by Verific internally for the iterator.
    static unsigned      NumTopUnits() ; // Returns the number of top level design units determined. This is required for the reverse-iterator of the top units
    static unsigned      TopUnitAt(unsigned pos, const char **library, const char **top_unit) ; // Returns 0 if end of list, 1 normally. Sets the top unit and library names

    // Returns the top level units from registered files (internally calls DetermineTopUnits() to prepare the list)
    static const Array * GetTopUnits()     { (void) DetermineTopUnits() ; return _top_units ; }

    // VIPER #6295: Analysis mode:
    static unsigned      IsVhdl87()    { return (_analysis_mode == VHDL_87) ? 1 : 0 ; }
    static unsigned      IsVhdl93()    { return (_analysis_mode == VHDL_93) ? 1 : 0 ; }
    static unsigned      IsVhdlPsl()   { return (_analysis_mode == VHDL_PSL) ? 1 : 0 ; }
    static unsigned      IsVhdl2K()    { return (_analysis_mode == VHDL_2K) ? 1 : 0 ; }
    static unsigned      IsVhdl2008()  { return (_analysis_mode == VHDL_2008) ? 1 : 0 ; }

    // VIPER #6399: Easy finding of satisfying file for a requirement:
    static VhdlFileElement *FindSatisfyingFile(const VhdlFileDependency *requirement, const VhdlFileElement *requiring_node, Map **multiple_files) ;

#ifdef VHDLSORT_FLEX_READ_FROM_STREAM
    // VIPER #6493: Read from streams:
    static void            RegisterFlexStreamCallBack(verific_stream *(*stream_func)(const char *file_name)) ;
    static verific_stream *GetUserFlexStream(const char *file_name) ;
#endif

    // VIPER #8080: API to flip the ignore translate off pragma setting (default is: always ignore translate off pragmas):
    static void          SetIgnoreTranslateOff(unsigned ignore_pragma) ;
    static unsigned      GetIgnoreTranslateOff() ;

    // Clear/Reset support (including deallocation of statically-held memory)
    static void          Reset() ;                // Top-level vhdl_sort reset routine (calls all reset/clear methods)
    static void          ClearRegisteredFiles() ; // Clean up the registered list of files
    static void          ClearAllLibraries() ;    // Remove all recognized libraries
    static void          ClearTopUnits() ;        // Remove all recognized top level design units
    static void          ClearPrimaryUnits() ;    // Remove all primary units
    static void          ClearDependencies() ;    // Remove any lingering QuckParsed requirements

    // Message handling support : Message id table, output formatted, varargs message functions
    static const Map *   GetMessageMap()    { (void) GetMessageId("") ; return _msgs ; } // Return the entire map : (char*)formatted_string -> (char*)message_id
    static const char *  GetMessageId(const char *msg) ; // Aid to get message ID from formatted string. Accessed by Error/Warning/Error
    static void          ResetMessageMap() ;
    static void          Error(linefile_type lf, const char *format, ...) ;
    static void          Warning(linefile_type lf, const char *format, ...) ;
    static void          Info(linefile_type lf, const char *format, ...) ;

private:
    // Quickly parse the file and create a node with the require and provide list
    static VhdlFileElement *CreateNode(const char *file_name, const char *lib_name = "work") ;

    // Quickly analyze the file and create the data structure internally. The CreateNode
    // routine calls this routine to create the data structure. This routine internally
    // calls the QuickParse() routine declared later.
    static unsigned      QuickAnalyze(const char *file_name, const char *lib_name = "work") ;

    // Return the requirements for a file quickly analyzed (take ownership)
    static Array *       TakeDependencies() ;
    // Return the primary units provided by the file (take ownership)
    static Set *         TakePrimaryUnits() ;

    // Call Flex; these are similar to there counter part in vhdl_file
    static unsigned      StartFlex(const char *file_name) ;
    static unsigned      EndFlex() ;

    // Returns the next token by calling yylex(). yylex(), if called second time
    // after it returns 0, it crashes. This routine creates a wraper over yylex()
    static int           GetNextToken() ;
    // Read and skip the specified 'token' 'skip_number_of_times' (default is 1)
    static void          SkipTokensUntil(int token, unsigned skip_number_of_times = 1) ;

    // Read a name that is coming from lex. If hier_name is set, only accept
    // a hier name, otherwise accept a normal name also
    static void          ReadName(const char *start, unsigned hier_name, unsigned for_comp_inst) ;

    // Read a hier or normal name that is coming from lex, it uses the ReadName routine
    static void          ReadIdentifier(const char *id) ;

    // Call manually written VHDL parser subset
    static int           QuickParse() ;

    // These two routines add the required and provided items in there containers
    static unsigned      AddPrimaryUnit(const char *prim_unit, unsigned architecture = 0) ;
    static unsigned      AddDependency(char *prim_unit, char *lib_name = 0, unsigned for_sec_unit = 0, unsigned for_comp_inst = 0) ;
    static unsigned      AddLibrary(const char *lib_name) ;

    // VIPER #7676: Resolve required units from files from the registered library/directories:
    static void          ResolveFiles() ;

private:
    static Array        *_registered_files ;  // Stores registered VhdlFileElement *
    static Map          *_all_design_units ;  // VIPER #6399: Stores const char *library name vs. Map of design units.
                                              // Design unit map stores char *unit name vs. VhdlFileElement where it is defined
    static int           _last_token ;        // Token returned by the last yylex call
    static int           _previous_token ;    // Token returned by the previous yylex call (VIPER #7676)

    static Array        *_registered_dirs ;   // VIPER #7676: Stores registered VhdlDirElement *
    static Set          *_extensions ;        // VIPER #7676: List of file extensions

    // From the QuickParse routine, these will be created and filled in (they are taken away by VhdlFileElement)
    static Array        *_dependencies ;      // List of requirements for a file being QuickParsed
    static Set          *_prim_units ;        // List of provided primary units of the file

    // The following is persistent information for this class
    static Set          *_all_libraries ;     // List of recognized library names from the 'library clasue'
    static unsigned      _is_sorted ;         // Flag to indicate whether the list is sorted, set when sorted,
                                              // reset when a file registered. Requires only a single bit.
    // The following are required for finding the top level design units
    static Set          *_architecture_units ;// List of architectures that are inserted as provided item
    static Array        *_top_units ;         // Array of VhdlUnitElement * of top units, created with
                                              // DetermineTopUnits() call and reset/freed with ClearTopUnits()
    static Map          *_msgs ;              // Static table to store formatted message->messageID
    static unsigned      _analysis_mode ;     // VIPER #6295: Current analysis mode

#ifdef VHDLSORT_FLEX_READ_FROM_STREAM
    static verific_stream *(*_stream_callback_func)(const char *file_name) ; // VIPER #6493: Read from streams
#endif
} ; // class vhdl_sort

/* -------------------------------------------------------------- */

/*
    VhdlFileElement is a class to represent a registered files with all its
    dependencies and the primary units it provides. This also includes the
    name to the library into which it is going to be analyzed.
*/

class VFC_DLL_PORT VhdlFileElement
{
public:
    VhdlFileElement(const char *file_name, const char *lib_name, unsigned vhdl_mode,
                             Set *provides /* absorbed */, Array *requires /* absorbed */) ;
    ~VhdlFileElement() ;

private:
    // Prevent compiler from defining the following
    VhdlFileElement() ;                                   // Purposely leave unimplemented
    VhdlFileElement(const VhdlFileElement &) ;            // Purposely leave unimplemented
    VhdlFileElement& operator=(const VhdlFileElement &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Add 'this' into the Set after satisfying all the requirements by the files in the list
    void                 Process(Set &sorted_files, Set &processed_files, Array *files) const ;

    // Access routines
    const char *         GetFileName() const        { return _file_name ;    }
    const char *         GetLibraryName() const     { return _library_name ; }
    unsigned             GetAnalysisMode() const    { return _analysis_mode ; }
    Array *              GetDependencies() const    { return _dependencies ; }
    Set *                GetPrimaryUnits() const    { return _prim_units ;   }

    // VIPER #7676: Resolve required units from files from the registered library/directories:
    void                 ResolveFiles(const Array *registered_dirs, const Set *extensions, const Map *all_design_units) const ;

private:
    // Check whether this node satisfies the given requirement:
    // Following API is not used anymore - commenting it out:
    //unsigned             Satisfies(const VhdlFileDependency *requirement, const VhdlFileElement *requiring_node) const ;

private:
    char                *_file_name ;    // Name of the file this class repersents
    char                *_library_name ; // Name of library into which this file will be analyzed
    unsigned             _analysis_mode ;// VIPER #6295: VHDL analyze dialect mode
    Set                 *_prim_units ;   // Stores char * name of primary units this file provides
    Array               *_dependencies ; // Stores VhdlFileDependency * on which this file depends
} ; // class VhdlFileElement

/* -------------------------------------------------------------- */

/*
    VhdlDirElement is a class to represent a registered directories with all its
    content (files) and the analysis mode for them. This also includes the
    name to the library into which it is going to be analyzed. Added for VIPER #7676.
*/

class VFC_DLL_PORT VhdlDirElement
{
public:
    VhdlDirElement(const char *path_name, const char *lib_name, unsigned vhdl_mode) ;
    ~VhdlDirElement() ;

private:
    // Prevent compiler from defining the following
    VhdlDirElement() ;                                  // Purposely leave unimplemented
    VhdlDirElement(const VhdlDirElement &) ;            // Purposely leave unimplemented
    VhdlDirElement& operator=(const VhdlDirElement &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Access routines
    const char *         GetPathName() const        { return _path_name ;     }
    const char *         GetLibraryName() const     { return _library_name ;  }
    unsigned             GetAnalysisMode() const    { return _analysis_mode ; }
    Set *                GetFiles() const           { return _files ;         }

    // VIPER #7676: Resolve required units from files from the registered library/directories:
    unsigned             ResolveFiles(const VhdlFileDependency *requirement, const Set *extensions) ;

private:
    char                *_path_name ;    // Name of the file this class repersents
    char                *_library_name ; // Name of library into which this file will be analyzed
    unsigned             _analysis_mode ;// VIPER #6295: VHDL analyze dialect mode
    Set                 *_files ;        // Stores char * name of primary units this file provides
} ; // class VhdlDirElement

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlUnitElement
{
public:
    VhdlUnitElement(char *lib_name, char *primary_unit) ;
    virtual ~VhdlUnitElement() ;

private:
    // Prevent compiler from defining the following
    VhdlUnitElement() ;                                      // Purposely leave unimplemented
    VhdlUnitElement(const VhdlUnitElement &) ;            // Purposely leave unimplemented
    VhdlUnitElement& operator=(const VhdlUnitElement &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Access routines
    const char *         GetLibrary() const                     { return _library_name ; }
    const char *         GetPrimaryUnit() const                 { return _primary_unit ; }

protected:
    char                *_library_name ; // Name of the library this dependecy requires
    char                *_primary_unit ; // Name of the primary unit this dependecy requires
    // NOTE: We can add another member char *_file_name where the unit is declared if necessary
} ; // class VhdlUnitElement

/* -------------------------------------------------------------- */

/*
    VhdlFileDependency is a class to represent a single dependency of a file.
    Dependency is only on a primary unit of a specific library. So, it only
    takes a library name and a primary unit name.
*/

class VFC_DLL_PORT VhdlFileDependency : public VhdlUnitElement
{
public:
    VhdlFileDependency(char *lib_name, char *primary_unit, unsigned for_sec_unit = 0, unsigned for_comp_inst = 0) ;
    ~VhdlFileDependency() ;

private:
    // Prevent compiler from defining the following
    VhdlFileDependency() ;                                      // Purposely leave unimplemented
    VhdlFileDependency(const VhdlFileDependency &) ;            // Purposely leave unimplemented
    VhdlFileDependency& operator=(const VhdlFileDependency &) ; // Purposely leave unimplemented

public:
    // Checking routine whether this dependency is satisfied or not
    unsigned             IsSatisfied() const                    { return (_satisfied_by) ? 1 : 0 ; }
    void                 SetSatisfied(VhdlFileElement *elem)    { _satisfied_by = elem ; }
    unsigned             ForSecondaryUnitDeclaration() const    { return _for_sec_unit ; }
    unsigned             ForComponentInstantiation() const      { return _for_comp_inst ; }

    // VIPER #7676: Resolve required units from files from the registered library/directories:
    void                 ResolveFiles(const Array *registered_dirs, const Set *extensions, const Map *all_design_units, const VhdlFileElement *requiring_node) const ;

private:
    VhdlFileElement     *_satisfied_by ; // Pointer to the node which satisfied this dependency (if any)
    unsigned             _for_sec_unit:1 ; // Flag to indicate that this item is required only for its secondary unit declaration only not for instantiation or use clause
    unsigned             _for_comp_inst:1 ; // Flag to indicate that this item is required only for a component instantiation somewhere in the design
} ; // class VhdlFileDependency

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_SORT_H_

