/*
 *
 * [ File Version : 1.12 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_SORT_TOKENS_H_
#define _VERIFIC_VHDL_SORT_TOKENS_H_

/* NOTE : This file was once created by yacc (y.tab.h), to get
 * tokens defined for the system and to expose
 * YYSTYPE to vhdl.l ( for yylval)
 * Now, it's manually modified for the purpose of
 * avoiding exposure to all classes defined in the
 * yacc parse tree nodes (so that we don't need to
 * define them all the time for all files that include
 * this.
 * If there is a change in tokens (in verilog.y) or
 * a change in lex->yacc token passing, this file
 * NEEDS TO BE UPDATED MANUALLY AGAIN !!
*/

/*********************** So far YACC generated ***************************/

/* These added for 'environment' purposes (used to check VHDL semantics in analysis) */
// Start at a number above any yacc token numbers,
// but within 10 bits, so that, if needed, we can still store these tokens in a restricted unsigned field.
#define VHDL_SIGUPDATE 887
#define VHDL_VARUPDATE 888
#define VHDL_READ 889

/* These added for operator convenience */
#define VHDL_INCR 900
#define VHDL_DECR 901
#define VHDL_REDAND  902
#define VHDL_REDNAND 903
#define VHDL_REDOR   904
#define VHDL_REDNOR  905
#define VHDL_REDXOR  906
#define VHDL_REDXNOR 907

/* This was manually added to support pragmas */
#define VHDL_UMINUS 950
#define VHDL_FEEDTHROUGH 951
#define VHDL_WIRED_THREE_STATE 952
#define VHDL_PULLUP 953
#define VHDL_PULLDOWN 954
#define VHDL_TRSTMEM 955
#define VHDL_buf 956
#define VHDL_builtin_operator 957
#define VHDL_rising_edge 958
#define VHDL_falling_edge 959
#define VHDL_IGNORE_SUBPROGRAM 960

/* This was manually added for named predefined functions/procedures */
#define VHDL_deallocate 980   /* procedure for access types */
#define VHDL_file_open 981    /* procedure for file types */
#define VHDL_file_close 982   /* procedure for file types */
#define VHDL_read 983         /* procedure for file types */
#define VHDL_write 984        /* procedure for file types */
#define VHDL_endfile 985      /* procedure for file types */
#define VHDL_now 986    /* function in standard.vhd */

/* This was manually added to define pre-defined attributes. No bit-with restriction on these */

#define VHDL_ATT_base  1001
#define VHDL_ATT_left   1002
#define VHDL_ATT_right   1003
#define VHDL_ATT_high   1004
#define VHDL_ATT_low   1005
#define VHDL_ATT_ascending   1006
#define VHDL_ATT_image   1007
#define VHDL_ATT_value   1008
#define VHDL_ATT_pos   1010
#define VHDL_ATT_val   1011
#define VHDL_ATT_succ   1012
#define VHDL_ATT_pred   1013
#define VHDL_ATT_leftof   1014
#define VHDL_ATT_rightof   1015
#define VHDL_ATT_range   1016
#define VHDL_ATT_reverse_range   1017
#define VHDL_ATT_length   1018
#define VHDL_ATT_delayed   1019
#define VHDL_ATT_stable   1020
#define VHDL_ATT_quiet   1021
#define VHDL_ATT_transaction   1022
#define VHDL_ATT_event   1023
#define VHDL_ATT_active   1024
#define VHDL_ATT_last_event   1025
#define VHDL_ATT_last_active   1026
#define VHDL_ATT_last_value   1027
#define VHDL_ATT_driving   1028
#define VHDL_ATT_driving_value   1029
#define VHDL_ATT_simple_name   1030
#define VHDL_ATT_instance_name   1031
#define VHDL_ATT_path_name   1032
#define VHDL_ATT_structure  1033    // '87 LRM Block attribute (no longer supported in '93 LRM)
#define VHDL_ATT_behavior   1034    // '87 LRM Block attribute (no longer supported in '93 LRM)

/* Copied from vhdl/vhdl_yacc.h */
#define VHDL_AMPERSAND 257
#define VHDL_TICK 258
#define VHDL_OPAREN 259
#define VHDL_CPAREN 260
#define VHDL_STAR 261
#define VHDL_PLUS 262
#define VHDL_COMMA 263
#define VHDL_MINUS 264
#define VHDL_DOT 265
#define VHDL_SLASH 266
#define VHDL_COLON 267
#define VHDL_SEMI 268
#define VHDL_STHAN 269
#define VHDL_EQUAL 270
#define VHDL_GTHAN 271
#define VHDL_VERTICAL 272
#define VHDL_BANG 273
#define VHDL_OBRACK 274
#define VHDL_CBRACK 275
#define VHDL_ARROW 276
#define VHDL_EXPONENT 277
#define VHDL_VARASSIGN 278
#define VHDL_NEQUAL 279
#define VHDL_GEQUAL 280
#define VHDL_SEQUAL 281
#define VHDL_BOX 282
#define VHDL_abs 283
#define VHDL_access 284
#define VHDL_after 285
#define VHDL_alias 286
#define VHDL_all 287
#define VHDL_and 288
#define VHDL_architecture 289
#define VHDL_array 290
#define VHDL_assert 291
#define VHDL_attribute 292
#define VHDL_begin 293
#define VHDL_block 294
#define VHDL_body 295
#define VHDL_buffer 296
#define VHDL_bus 297
#define VHDL_case 298
#define VHDL_component 299
#define VHDL_configuration 300
#define VHDL_constant 301
#define VHDL_disconnect 302
#define VHDL_downto 303
#define VHDL_else 304
#define VHDL_elsif 305
#define VHDL_end 306
#define VHDL_entity 307
#define VHDL_exit 308
#define VHDL_file 309
#define VHDL_for 310
#define VHDL_function 311
#define VHDL_generate 312
#define VHDL_generic 313
#define VHDL_group 314
#define VHDL_guarded 315
#define VHDL_if 316
#define VHDL_impure 317
#define VHDL_in 318
#define VHDL_inertial 319
#define VHDL_inout 320
#define VHDL_is 321
#define VHDL_label 322
#define VHDL_library 323
#define VHDL_linkage 324
#define VHDL_literal 325
#define VHDL_loop 326
#define VHDL_map 327
#define VHDL_mod 328
#define VHDL_nand 329
#define VHDL_new 330
#define VHDL_next 331
#define VHDL_nor 332
#define VHDL_not 333
#define VHDL_null 334
#define VHDL_of 335
#define VHDL_on 336
#define VHDL_open 337
#define VHDL_or 338
#define VHDL_others 339
#define VHDL_out 340
#define VHDL_package 341
#define VHDL_port 342
#define VHDL_postponed 343
#define VHDL_procedure 344
#define VHDL_process 345
#define VHDL_pure 346
#define VHDL_range 347
#define VHDL_record 348
#define VHDL_register 349
#define VHDL_reject 350
#define VHDL_rem 351
#define VHDL_report 352
#define VHDL_return 353
#define VHDL_rol 354
#define VHDL_ror 355
#define VHDL_select 356
#define VHDL_severity 357
#define VHDL_signal 358
#define VHDL_shared 359
#define VHDL_sla 360
#define VHDL_sll 361
#define VHDL_sra 362
#define VHDL_srl 363
#define VHDL_subtype 364
#define VHDL_then 365
#define VHDL_to 366
#define VHDL_transport 367
#define VHDL_type 368
#define VHDL_unaffected 369
#define VHDL_units 370
#define VHDL_until 371
#define VHDL_use 372
#define VHDL_variable 373
#define VHDL_wait 374
#define VHDL_when 375
#define VHDL_while 376
#define VHDL_with 377
#define VHDL_xnor 378
#define VHDL_xor 379
#define VHDL_INTEGER 380
#define VHDL_REAL 381
#define VHDL_STRING 382
#define VHDL_BIT_STRING 383
#define VHDL_ID 384
#define VHDL_EXTENDED_ID 385
#define VHDL_CHARACTER 386
#define VHDL_FATAL_ERROR 387
#define VHDL_PSL_EQUIV 388
#define VHDL_PSL_IMPL 389
#define VHDL_PSL_SUFFIX_IMPL 390
#define VHDL_PSL_OSUFFIX_IMPL 391
#define VHDL_PSL_NONCONS_REP 392
#define VHDL_PSL_GOTO_REP 393
#define VHDL_PSL_CONS_REP 394
#define VHDL_PSL_CONS_REP1_INF 395
#define VHDL_PSL_SEQ_AND_LEN 396
#define VHDL_PSL_Seq_And 397
#define VHDL_PSL_Seq_Or 398
#define VHDL_PSL_AT 399
#define VHDL_PSL_Concat 400
#define VHDL_PSL_Fusion 401
#define VHDL_PSL_Not 402
#define VHDL_PSL_And 403
#define VHDL_PSL_Or 404
#define VHDL_PSL_Seq_Impl 405
#define VHDL_PSL_A 406
#define VHDL_PSL_AF 407
#define VHDL_PSL_AG 408
#define VHDL_PSL_AX 409
#define VHDL_PSL_ABORT 410
#define VHDL_PSL_ALWAYS 411
#define VHDL_PSL_ASSERT 412
#define VHDL_PSL_ASSUME 413
#define VHDL_PSL_ASSUME_GUARANTEE 414
#define VHDL_PSL_BEFORE 415
#define VHDL_PSL_BEFOREBANG 416
#define VHDL_PSL_BEFOREBANG_ 417
#define VHDL_PSL_BEFORE_ 418
#define VHDL_PSL_BOOLEAN 419
#define VHDL_PSL_CLOCK 420
#define VHDL_PSL_CONST 421
#define VHDL_PSL_COVER 422
#define VHDL_PSL_DEFAULT 423
#define VHDL_PSL_E 424
#define VHDL_PSL_EF 425
#define VHDL_PSL_EG 426
#define VHDL_PSL_EX 427
#define VHDL_PSL_ENDPOINT 428
#define VHDL_PSL_EVENTUALLYBANG 429
#define VHDL_PSL_F 430
#define VHDL_PSL_FAIRNESS 431
#define VHDL_PSL_FELL 432
#define VHDL_PSL_FORALL 433
#define VHDL_PSL_G 434
#define VHDL_PSL_INF 435
#define VHDL_PSL_INHERIT 436
#define VHDL_PSL_NEVER 437
#define VHDL_PSL_NEXT 438
#define VHDL_PSL_NEXTBANG 439
#define VHDL_PSL_NEXT_A 440
#define VHDL_PSL_NEXT_ABANG 441
#define VHDL_PSL_NEXT_E 442
#define VHDL_PSL_NEXT_EBANG 443
#define VHDL_PSL_NEXT_EVENT 444
#define VHDL_PSL_NEXT_EVENTBANG 445
#define VHDL_PSL_NEXT_EVENT_ABANG 446
#define VHDL_PSL_NEXT_EVENT_A 447
#define VHDL_PSL_NEXT_EVENT_EBANG 448
#define VHDL_PSL_NEXT_EVENT_E 449
#define VHDL_PSL_PREV 450
#define VHDL_PSL_PROPERTY 451
#define VHDL_PSL_RESTRICT 452
#define VHDL_PSL_RESTRICT_GUARANTEE 453
#define VHDL_PSL_ROSE 454
#define VHDL_PSL_SEQUENCE 455
#define VHDL_PSL_STRONG 456
#define VHDL_PSL_U 457
#define VHDL_PSL_UNION 458
#define VHDL_PSL_UNTIL 459
#define VHDL_PSL_UNTILBANG 460
#define VHDL_PSL_UNTILBANG_ 461
#define VHDL_PSL_UNTIL_ 462
#define VHDL_PSL_VMODE 463
#define VHDL_PSL_VPROP 464
#define VHDL_PSL_VUNIT 465
#define VHDL_PSL_W 466
#define VHDL_PSL_WHILENOT 467
#define VHDL_PSL_WHILENOTBANG 468
#define VHDL_PSL_WHILENOTBANG_ 469
#define VHDL_PSL_WHILENOT_ 470
#define VHDL_PSL_WITHIN 471
#define VHDL_PSL_WITHINBANG 472
#define VHDL_PSL_WITHINBANG_ 473
#define VHDL_PSL_WITHIN_ 474
#define VHDL_PSL_X 475
#define VHDL_PSL_XBANG 476
#define VHDL_PSL_ATBANG 477
#define VHDL_PSL_Suffix_ImplBANG 478
#define VHDL_PSL_OSuffix_ImplBANG 479
#define VHDL_PSL_STRONG_FAIRNESS 480
#define VHDL_PSL_EMBED_START 483
#define VHDL_OPT_COMMENT 484
#define vhdl_no_comment 485
#define no_oparen 486
#define VHDL_PSL_COUNTONES 487
#define VHDL_PSL_ISUNKNOWN 488
#define VHDL_PSL_ONEHOT 489
#define VHDL_PSL_ONEHOT0 490
#define VHDL_PSL_STABLE 491
#define VHDL_implicit_guard 492
#define VHDL_protected 493
#define VHDL_interfacerange 494
#define VHDL_interfaceread 495
#define VHDL_interfaceprefixread 496
#define VHDL_minimum 550
#define VHDL_maximum 551
#define VHDL_condition 552
#define VHDL_matching_equal 553
#define VHDL_matching_nequal 554
#define VHDL_matching_gthan 555
#define VHDL_matching_sthan 556
#define VHDL_matching_gequal 557
#define VHDL_matching_sequal 558
#define VHDL_context 559
#define VHDL_end_generate 560
#define VHDL_parameter 561
#define VHDL_default 562
#define VHDL_matching_op 563
#define VHDL_LANGLEQ 564
#define VHDL_RANGLEQ 565
#define VHDL_AT 566
#define VHDL_ACCENT 567

#endif // #ifndef _VERIFIC_VHDL_SORT_TOKENS_H_

