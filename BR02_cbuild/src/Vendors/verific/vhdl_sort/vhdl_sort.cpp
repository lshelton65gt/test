/*
 *
 * [ File Version : 1.30 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "vhdl_sort.h"

#include "Array.h"
#include "Set.h"
#include "Map.h"

#include "Message.h"
#include "Strings.h"
#include "FileSystem.h" // for ReadDirectory()

#ifdef VHDLSORT_FLEX_READ_FROM_STREAM
#include <VerificStream.h> // For reading from streams
#endif

#include "vhdl_file.h" // for VHDL library path mapping and logical library checking
#include "VhdlUnits.h" // for checking units from VHDL logical libraries

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Initialize with zero, will be created when required
/* static */ Array *vhdl_sort::_registered_files = 0 ;

// VIPER #7676: Initialize with zero, will be created when required
/* static */ Array *vhdl_sort::_registered_dirs = 0 ;
/* static */ Set *vhdl_sort::_extensions = 0 ;

// VIPER #6399: All design units are initialized to 0
/* static */ Map *vhdl_sort::_all_design_units = 0 ;

// List is sorted without any file being registered
/* static */ unsigned vhdl_sort::_is_sorted = 1 ;

// These are created only when required
/* static */ Array *vhdl_sort::_dependencies = 0 ;
/* static */ Set   *vhdl_sort::_prim_units = 0 ;
/* static */ Set   *vhdl_sort::_architecture_units = 0 ;

// Whenever we get a library clause, library is inserted into this Set
/* static */ Set   *vhdl_sort::_all_libraries = 0 ;

// The array of determined top level design units
/* static */ Array *vhdl_sort::_top_units = 0 ;

/* static */ Map *vhdl_sort::_msgs = 0 ;

// VIPER #6295: Current analysis mode:
/* static */ unsigned vhdl_sort::_analysis_mode = VHDL_93 ; // Default is VHDL-93

#ifdef VHDLSORT_FLEX_READ_FROM_STREAM
// VIPER #6493: Read from streams:
verific_stream *(*vhdl_sort::_stream_callback_func)(const char *file_name) = 0 ;
#endif

/* -------------------------------------------------------------- */

// Add a file to be sorted in the list
/* static */ unsigned
vhdl_sort::RegisterFile(const char *file_name, const char *lib_name /* = "work" */, unsigned vhdl_mode /* = VHDL_93 */)
{
    if (!file_name) return 0 ; // Can't insert
    if (!lib_name) lib_name = "work" ;
    if (!vhdl_mode) vhdl_mode = VHDL_93 ;

    // Set the static analysis mode for now:
    _analysis_mode = vhdl_mode ;

    // Quickly parse the file, create a node with provides and require list for the given file:
    VhdlFileElement *new_node = CreateNode(file_name, lib_name) ;
    if (!new_node) return 0 ;

    // Check whether this is the first file node:
    if (!_registered_files) _registered_files = new Array() ; // Create the list

    _registered_files->InsertLast(new_node) ; // Insert the node

    _is_sorted = 0 ; // Now, the list becomes unsorted
    ClearTopUnits() ; // Clear the current determined top units

    return 1 ; // Always inserted
}

// VIPER #7676: Add a directory to be searched for file/units (similar to Verilog -y)
/* static */ unsigned
vhdl_sort::RegisterDir(const char *dir_name, const char *lib_name /* = "work" */, unsigned vhdl_mode /* = VHDL_93 */)
{
    if (!dir_name) return 0 ;
    if (!lib_name) lib_name = "work" ;

    if (!_registered_dirs) _registered_dirs = new Array() ;

    // Insert into registered dir list:
    _registered_dirs->InsertLast(new VhdlDirElement(dir_name, lib_name, vhdl_mode)) ;

    return 1 ; // Registered successfully
}

// VIPER #7676: Resolve files for required units from the mapped libraries/directories
/* static */ void
vhdl_sort::ResolveFiles()
{
    if (!_registered_dirs || !_registered_dirs->Size()) return ; // Nothing to do

    if (!_extensions) {
        // Default extensions are .vhd and .vhdl
        _extensions = new Set(STRING_HASH) ;
        (void) _extensions->Insert(Strings::save(".vhd")) ;
        (void) _extensions->Insert(Strings::save(".vhdl")) ;
    }

    unsigned i ;
    VhdlFileElement *file ;
    FOREACH_ARRAY_ITEM(_registered_files, i, file) {
        if (!file) continue ;
        // Resolve the files for the requirements of this registered files:
        file->ResolveFiles(_registered_dirs, _extensions, _all_design_units) ;
    }
}

// Sort the registered files
/* static */ unsigned
vhdl_sort::Sort()
{
    if (!_registered_files || !_registered_files->Size()) return 0 ; // Nothing to sort
    if (_is_sorted) return 1 ; // Already sorted, don't sort it again

    // VIPER #7676: Some registered directories exist, check and resolve/register files
    // required units from files in those library/directories first, before sorting:
    if (_registered_dirs) ResolveFiles() ;

    // We will keep the sorted file elements in this Set.
    Set sorted_files(POINTER_HASH, _registered_files->Size()) ;
    // This Set stores the files being or have been processed
    Set processed_files(POINTER_HASH, _registered_files->Size()) ;

    // Sort all the nodes, resolve its dependencies using all other files in the list.
    unsigned i ;
    VhdlFileElement *file ;
    FOREACH_ARRAY_ITEM(_registered_files, i, file) {
        if (!file) continue ;
        processed_files.Reset() ; // Reset this Set for current requirements
        file->Process(sorted_files, processed_files, _registered_files) ;
    }

    // Here, all the items in the list are also inserted into the Set in sorted order
    VERIFIC_ASSERT(sorted_files.Size() == _registered_files->Size()) ;
    // Clear out the Array
    _registered_files->Reset() ;

    SetIter si ;
    // Now, insert the sorted items in the list Array in that order
    FOREACH_SET_ITEM(&sorted_files, si, &file) {
        if (!file) continue ;
        _registered_files->InsertLast(file) ;
    }

    _is_sorted = 1 ; // The list is now sorted

    return 1 ; // Done
}

// Determine the top level design units within the registered files:
/* static */ unsigned
vhdl_sort::DetermineTopUnits()
{
    if (_top_units && _top_units->Size()) return 1 ; // Already determined

    // First sort the files, otherwise we will not be able to determine the top level
    // design units within a single loop. This routine is supposed to be called after
    // calling Sort() anyway, but since, Sort does not do anything if it is sorted we
    // can always call it again here without any harm :-)
    if (!Sort()) return 0 ; // Can't sort the files

    Map top_units(STRING_HASH_CASE_INSENSITIVE) ; // Map of char * -> VhdlUnitElement *

    // Since the files are now sorted, the strategy to find the top level units is:
    //  1. Insert all the primary units provided by a file into a set
    //  2. Remove all the primary units required by a file from the set
    // Since all the required units come first, they gets removed alter when needed.
    // If the files are not sorted we have to traverse the list at-least twice.
    // We maintain a Set of ALL the architecture units recognized so far to discard them.
    // We also added a flag to determine whether a dependency requires a primary unit only
    // for its architecture/body declaration or because it has been instantiated/'used'.
    // We discard all the requirements which says it is required for body decl only.

    unsigned i ;
    VhdlFileElement *file ;
    FOREACH_ARRAY_ITEM(_registered_files, i, file) {
        if (!file) continue ;
        Set *prim_units = file->GetPrimaryUnits() ;
        // Insert all the primary units into the top design set:
        SetIter si ;
        const char *prim_name ;
        FOREACH_SET_ITEM(prim_units, si, &prim_name) {
            if (_architecture_units && _architecture_units->GetItem(prim_name)) continue ; // It is an architecture
            // Insert this primary unit into the Map:
            char *prov_name = Strings::save(file->GetLibraryName(), "::", prim_name) ;
            char *lib_name = Strings::save(file->GetLibraryName()) ;
            VhdlUnitElement *unit = new VhdlUnitElement(lib_name, Strings::save(prim_name)) ;
            if (!top_units.Insert(prov_name, unit)) {
                // Can't insert, so clean up!
                Strings::free(prov_name) ;
                delete unit ;
            }
        }

        // Remove all the required units from the top design set:
        Array *deps = file->GetDependencies() ;
        unsigned j ;
        VhdlFileDependency *dep ;
        FOREACH_ARRAY_ITEM(deps, j, dep) {
            if (!dep || dep->ForSecondaryUnitDeclaration() || dep->ForComponentInstantiation()) continue ; // Not instantiated, only body declaration or component needs it
            if (_architecture_units && _architecture_units->GetItem(dep->GetPrimaryUnit())) continue ; // It is an architecture
            // Check whether it is in the Map or not (for all valid cases, it must be in the map other than standard units):
            char *dep_name = Strings::save(dep->GetLibrary(), "::", dep->GetPrimaryUnit()) ;
            MapItem *item = top_units.GetItem(dep_name) ;
            Strings::free(dep_name) ;
            if (item) {
                // Remove from Map and clean up the allocated memory!
                char *key = (char *)item->Key() ;
                VhdlUnitElement *unit = (VhdlUnitElement *)item->Value() ;
                if (top_units.Remove(key)) {
                    Strings::free(key) ;
                    delete unit ;
                }
            }
        }
    }

    // Now, the Map contains only those items that are not instantiated or 'used'.
    // So, these are the top level design units that we wanted to find.
    // Copy over the top units from the Map into the Array:
    if (!_top_units) _top_units = new Array() ;
    MapIter mi ;
    char *name ;
    VhdlUnitElement *unit ;
    FOREACH_MAP_ITEM(&top_units, mi, &name, &unit) {
        Strings::free(name) ; // This name is not required anymore
        _top_units->InsertLast(unit) ;
    }

    // Automatic Map 'top_units' will be deleted out-of-scope automatically.

    return 1 ; // Done
}

VhdlFileElement *
vhdl_sort::FindSatisfyingFile(const VhdlFileDependency *requirement, const VhdlFileElement *requiring_node, Map **multiple_files)
{
    if (multiple_files) *multiple_files = 0 ;

    if (!_all_design_units) return 0 ;
    if (!requirement || !requirement->GetLibrary()) return 0 ;
    if (!requiring_node || !requiring_node->GetLibraryName()) return 0 ;

    Map *units = 0 ; //(Map *)_all_design_units->GetValue(requirement->GetLibrary()) ;
    unsigned work_lib_requirement = Strings::compare_nocase("work", requirement->GetLibrary()) ;
    // VIPER #8079: If it is "work" library, it must be searched in the library of requiring_node only:
    //unsigned work_lib_requirement_honoured = 0 ;
    if (/*!units &&*/ work_lib_requirement) {
        // Library name "work" is equivalent to current library:
        units = (Map *)_all_design_units->GetValue(requiring_node->GetLibraryName()) ;
    //    work_lib_requirement_honoured = 1 ; // We have honoured the use "work" library here
    } else {
        units = (Map *)_all_design_units->GetValue(requirement->GetLibrary()) ;
    }

    if (!requirement->GetPrimaryUnit()) {
        // Should reset 'units' in case it depends on the whole of "work" or requiring library. We cannot fully satify it then:
        if (work_lib_requirement || Strings::compare_nocase(requirement->GetLibrary(), requiring_node->GetLibraryName())) units = 0 ;
        if (multiple_files) *multiple_files = units ;
        return 0 ;
    }

    if (!units) return 0 ;

    // VIPER #7075: If this library does not provide the unit and the requirement is on work library,
    // check the current library to see if that provides the unit. We should count on that then:
    VhdlFileElement *providing_node = (VhdlFileElement *)units->GetValue(requirement->GetPrimaryUnit()) ;
    // VIPER #8079: Following is not required anymore since we already processed "work" above:
    //if (!providing_node && work_lib_requirement && !work_lib_requirement_honoured) {
    //    // Library name "work" is equivalent to current library:
    //    units = (Map *)_all_design_units->GetValue(requiring_node->GetLibraryName()) ;
    //    if (units) providing_node = (VhdlFileElement *)units->GetValue(requirement->GetPrimaryUnit()) ;
    //}

    return providing_node ;
}

// VIPER #7676: Resolve files for all the unresolved requirements of this registered file.
void
VhdlFileElement::ResolveFiles(const Array *registered_dirs, const Set *extensions, const Map *all_design_units) const
{
    unsigned i ;
    VhdlFileDependency *requirement ;
    FOREACH_ARRAY_ITEM(_dependencies, i, requirement) {
        // If multiple primary units are required (for .all), cannot resolve, ignore:
        if (!requirement || !requirement->GetPrimaryUnit()) continue ;

        // Try to resolve this requirement from the registered directories:
        requirement->ResolveFiles(registered_dirs, extensions, all_design_units, this) ;
    }
}

// VIPER #7676: Resolve files for this particular requirement from all the registered directories.
void
VhdlFileDependency::ResolveFiles(const Array *registered_dirs, const Set *extensions, const Map *all_design_units, const VhdlFileElement *requiring_node) const
{
    VERIFIC_ASSERT(all_design_units && requiring_node) ;

    // First check if it is in one of the already registered files:
    Map *units = (Map *)all_design_units->GetValue(_library_name) ;
    if (units && units->GetItem(_primary_unit)) return ; // Already resolved
    unsigned work_lib_requirement = Strings::compare_nocase("work", _library_name) ;
    if (work_lib_requirement) {
        // Library name "work" is equivalent to current library:
        units = (Map *)all_design_units->GetValue(requiring_node->GetLibraryName()) ;
        if (units && units->GetItem(_primary_unit)) return ; // Already resolved
    }

    // Next, check if it is in logical library (in-memory or in-disk):
    VhdlLibrary *lib = vhdl_file::GetLibrary(_library_name) ;
    if (lib && lib->GetPrimUnit(_primary_unit)) return ; // Already analyzed/exists in logical library
    if (work_lib_requirement) {
        // Library name "work" is equivalent to current library:
        lib = vhdl_file::GetLibrary(requiring_node->GetLibraryName()) ;
        if (lib && lib->GetPrimUnit(_primary_unit)) return ; // Already analyzed/exists in logical library
    }

    // Now, try to resolve from directories registered:
    unsigned i ;
    VhdlDirElement *lib_dir ;
    FOREACH_ARRAY_ITEM(registered_dirs, i, lib_dir) {
        if (!lib_dir) continue ;

        // Resolve the dependency from this directory if it is mapped to the same directory:
        if (Strings::compare_nocase(_library_name, lib_dir->GetLibraryName()) || // Same directory
            // Work library matches with requring node library which is being treated as current library:
            (work_lib_requirement && Strings::compare_nocase(requiring_node->GetLibraryName(), lib_dir->GetLibraryName()))) {
            if (lib_dir->ResolveFiles(this, extensions)) return ; // Resolved
        }
    }
}

// VIPER #7676: Resolve files from this directory for the given requirement.
unsigned
VhdlDirElement::ResolveFiles(const VhdlFileDependency *requirement, const Set *extensions)
{
    VERIFIC_ASSERT(requirement && requirement->GetPrimaryUnit() && extensions) ;

    if (!_files) {
        // Files of this directory is not cached yet, cache them now:
        // Note: VHDL is case insensitive, so unit name may be in any character case.
        // So, use case-insensitive hashing here to get the file we need.
        // FIXME: What if there are two files with name only differing in case in Unix?
        _files = new Set(STRING_HASH_CASE_INSENSITIVE) ;
        Array tmp_files ;
        // Read all files from directory:
        FileSystem::ReadDirectory(_path_name, &tmp_files) ;
        unsigned i ;
        char *file_name ;
        // Insert them into the Set created for easy searching:
        FOREACH_ARRAY_ITEM(&tmp_files, i, file_name) {
            if (!_files->Insert(file_name)) Strings::free(file_name) ;
        }
    }

    SetIter si ;
    SetItem *item ;
    char *file_name ;
    const char *extension ;
    // Now, match the requirement with extension appended as file name:
    FOREACH_SET_ITEM(extensions, si, &extension) {
        if (!extension) continue ;
        file_name = Strings::save(requirement->GetPrimaryUnit(), extension) ;
        if ((item = _files->GetItem(file_name)) != 0) {
            // Found this file:
            Strings::free(file_name) ;
            file_name = Strings::save(_path_name, "/", (const char *)item->Key()) ;
            // Simply register it the file to have it resolved:
            (void) vhdl_sort::RegisterFile(file_name, _library_name, _analysis_mode) ;
            Strings::free(file_name) ;
            // CHECKME: Re-check registered units to see the requirement actually resolved?
            return 1 ;
        }
        Strings::free(file_name) ;
    }

    return 0 ; // Cannot resolve this requirement
}

// This recursive function inserts 'this' file in the given set after all the files
// which satisfies its requirements are inserted into the Set. So, the files which
// satisfies a requirement is inserted before this so that the dependency gets satisfied.
void
VhdlFileElement::Process(Set &sorted_files, Set &processed_files, Array *files) const
{
    // This file is already sorted, no need to check it
    if (sorted_files.GetItem(this)) return ;
    // Check whether we are processing or have processed this file before:
    if (!processed_files.Insert(this)) {
        vhdl_sort::Warning(0, "circular dependecy found for file %s while ordering", _file_name) ;
        // Don't try to process the requirements of this file
        return ;
    }

    unsigned i ;
    VhdlFileDependency *requirement ;
    Map *multiple_files ;
    // Process all the requirements of this file
    FOREACH_ARRAY_ITEM(_dependencies, i, requirement) {
        // VIPER #7676: Ignore requirements created for component instantiations while sorting:
        if (!requirement || requirement->ForComponentInstantiation()) continue ;
        VhdlFileElement *providing_node = vhdl_sort::FindSatisfyingFile(requirement, this, &multiple_files) ; // VIPER #6399
        if (providing_node) {
            // Set the satisfying node
            requirement->SetSatisfied(providing_node) ;
            // Process this node so that it gets inserted into the sorted list
            // before this file. This guarantees resolving the dependencies:
            if (this != providing_node) providing_node->Process(sorted_files, processed_files, files) ;
        } else {
            // Multiple entry: don't set the requirement to be satisfied which is yet to be fully satisfied
            MapIter mi ;
            FOREACH_MAP_ITEM(multiple_files, mi, 0, &providing_node) {
                if (!providing_node) continue ;
                if (this != providing_node) providing_node->Process(sorted_files, processed_files, files) ;
            }
        }

#if 0
        // VIPER #6399: This part of the code is moved into vhdl_sort::FindSatisfyingFile().
        // We also modify the sequential search to hashed search for faster results.
        unsigned j ;
        VhdlFileElement *providing_node ;
        // Satisfy the requirements by any of the files in the list:
        FOREACH_ARRAY_ITEM(files, j, providing_node) {
            if (!providing_node) continue ;
            // Check whether this file satisfy the requirement
            if (providing_node->Satisfies(requirement, this)) {
                // Set the satisfying node
                requirement->SetSatisfied(providing_node) ;
                // Process this node so that it gets inserted into the sorted list
                // before this file. This guarantees resolving the dependencies:
                if (this != providing_node) providing_node->Process(sorted_files, processed_files, files) ;
                break ;
            } else if (!requirement->GetPrimaryUnit()) {
                // This must be something like...
                // library lib1 ;
                // use lib1.all ;
                // So, all nodes analyzed in the same library may partly satisfy these requirements
                // VIPER #2832: Only process the providing node (file) if it is registered into a different
                // library than 'this' file is registered into and the requirement is for the library that the
                // providing node (file) is registered into, otherwise we coplain about circular dependency:
                // For example:
                //  -- file1.vhd (registered into lib1)   | -- file 2.vhd (registered into lib1)
                //  use lib1.all ;                        | use lib1.all ;
                // Here, both files require lib1 and both are registered into lib1. So, when we process the
                // dependency for 'use lib1.all ;' of file1.vhd we should not process for file2.vhd (normally
                // we need to process file2.vhd because it may partly satisfy the 'use' clause as the required
                // library is lib1 and file2.vhd is registered into lib1), since file2.vhd is also registered
                // into library lib1 which is same as the library in which file1.vhd is registered.
                if (Strings::compare_nocase(providing_node->GetLibraryName(), requirement->GetLibrary()) &&
                    !Strings::compare_nocase(providing_node->GetLibraryName(), _library_name)) {
                    // Don't set the requirement to be satisfied which is yet to be fully satisfied
                    // but do process this node before inserting 'this':
                    if (this != providing_node) providing_node->Process(sorted_files, processed_files, files) ;
                    // Don't break out of this loop, check all other files too
                }
            }
        }
#endif

#ifdef VHDLSORT_CONSIDER_VHDL_LIBRARY_MAPPING
        // VIPER #2933: Need to consider the VHDL library mapping for this requirement:
        // If the requirement is satisfied or it requires a whole library, don't go further:
        if (requirement->IsSatisfied() || !requirement->GetPrimaryUnit()) continue ;

        // We will now check the physically associated libraries for this requirements:
        Map *all_lib_paths = vhdl_file::GetAllLibraryPaths() ;
        char *lib_path = (all_lib_paths) ? (char *)all_lib_paths->GetValue(requirement->GetLibrary()) : 0 ;
        // Check whether the library of the current requirement have an associated physical library:
        if (!lib_path) continue ; // Can't check the associated libraries without this path

        // Accumulate the name of the physically associated libraries into a Set:
        Set assoc_libs(STRING_HASH_CASE_INSENSITIVE) ;
        MapIter mi ;
        char *assoc_lib_name ;
        char *assoc_lib_path ;
        FOREACH_MAP_ITEM(all_lib_paths, mi, &assoc_lib_name, &assoc_lib_path) {
            if (!Strings::compare(lib_path, assoc_lib_path)) continue ; // Paths do not match, not physically associated
            // Check whether the associated library is same as the library of this requirement.
            // We already matched that case above, it does not work, that is why we are here:
            if (Strings::compare(requirement->GetLibrary(), assoc_lib_name)) continue ;
            (void) assoc_libs.Insert(assoc_lib_name) ; // We need to check this library - take it into the Set
        }

        // Check whether the physical path of the required library matches with the default library path:
        unsigned matches_with_default_lib_path = Strings::compare(lib_path, vhdl_file::GetDefaultLibraryPath()) ;

        // Now check all the files that are analyzed into the associated libraries:
        unsigned j ;
        FOREACH_ARRAY_ITEM(files, j, providing_node) {
            if (!providing_node) continue ;
            const char *providing_lib = providing_node->GetLibraryName() ;
            // Check whether this is an associated library of the requirement library:
            if (!assoc_libs.GetItem(providing_lib) &&  // Not associated by physical path and
                ((all_lib_paths && all_lib_paths->GetItem(providing_lib)) || // Is mapped to a different library or
                 !matches_with_default_lib_path)) {    // Does not matches with default library path
                // In a single word, this is not physically associated with the required library:
                continue ;
            }

            // So, this node is one of the associated library, check whether the node provides the
            // required primary unit. First get the Set of all provided primary units from the node:
            Set *provided_prim_units = providing_node->GetPrimaryUnits() ;
            if (provided_prim_units && provided_prim_units->GetItem(requirement->GetPrimaryUnit())) {
                // This node does provide the required primary unit, set the satisfying node:
                requirement->SetSatisfied(providing_node) ;
                // Process this node so that it gets inserted into the sorted list
                // before this file. This guarantees resolving the dependencies:
                if (this != providing_node) providing_node->Process(sorted_files, processed_files, files) ;
            }
        }
#else
        (void) files ; // Avoid unused compiler warning
#endif // VHDLSORT_CONSIDER_VHDL_LIBRARY_MAPPING
    }

    // Since all the dependency satisfying nodes are inserted into the
    // Set, insert 'this' now in the same set.
//    vhdl_sort::Info(0, "Inserting %s into the sorted list", _file_name) ;
    (void) sorted_files.Insert(this) ;
}

// Returns the number of files in the sorted list
/* static */ unsigned
vhdl_sort::Size()
{
    return (_registered_files) ? _registered_files->Size() : 0 ;
}

// Sets the file and library names in the given postion. Returns 0 if end of list, 1 normally.
/* static */ unsigned
vhdl_sort::FileAt(unsigned pos, const char **file, const char **library, unsigned *vhdl_mode)
{
    if (!_registered_files) return 0 ; // List is not there, use RegisterFile to create the list

    if (pos >= _registered_files->Size()) return 0 ; // Non-existing positioni, no file is there!

    // Get the file element at this position:
    VhdlFileElement *elem = (VhdlFileElement *) _registered_files->At(pos) ;

    // Set the name of the file and the library:
    if (file) (*file) = elem->GetFileName() ;
    if (library) (*library) = elem->GetLibraryName() ;
    if (vhdl_mode) (*vhdl_mode) = elem->GetAnalysisMode() ;

    return 1 ;
}

// Returns the number of top level design units determined so far
/* static */ unsigned
vhdl_sort::NumTopUnits()
{
    return (_top_units) ? _top_units->Size() : 0 ;
}

// Sets the library and top unit names in the given postion. Returns 0 if end of list, 1 normally.
/* static */ unsigned
vhdl_sort::TopUnitAt(unsigned pos, const char **library, const char **top_unit)
{
    if (!_top_units) return 0 ; // List is not there, use RegisterFile to create the list

    if (pos >= _top_units->Size()) return 0 ; // Non-existing positioni, no file is there!

    // Get the file element at this position:
    VhdlUnitElement *unit = (VhdlUnitElement *) _top_units->At(pos) ;

    // Set the name of the library and the primary unit:
    if (library) (*library) = unit->GetLibrary() ;
    if (top_unit) (*top_unit) = unit->GetPrimaryUnit() ;

    return 1 ;
}

// Quickly parse the file and create a node with the require and provide list
/* static */ VhdlFileElement *
vhdl_sort::CreateNode(const char *file_name, const char *lib_name /* = "work" */)
{
    if (!file_name) return 0 ;
    if (!lib_name) lib_name = "work" ;

    // Quickly analyze the file:
    if (!QuickAnalyze(file_name, lib_name)) {
        // File contains error(s), cannot create node!
        return 0 ;
    }

    // The above function call creates the data structures provides
    // and requires for the given file. When it encounters selected
    // name, it creates the requirements list and for primary or
    // secondary unit declaration creates the provides list.

    // Get them from the vhdl_file class and creates the node.
    Set *prim_units = TakePrimaryUnits() ;
    if (!prim_units || !prim_units->GetItem("standard") || !Strings::compare_nocase(lib_name, "std")) {
        // VIPER #7660: This file does not define the implicitly imported std.standard.
        // So it implicitly depends on std.standard itself. Add this dependency now:
        (void) AddDependency(Strings::save("standard"), Strings::save("std")) ;
    }
    VhdlFileElement *elem = new VhdlFileElement(file_name, lib_name, _analysis_mode, prim_units, TakeDependencies()) ;

    // VIPER #6399: Create a global Map of all units vs the file node in which they can be found:
    if (!_all_design_units) _all_design_units = new Map(STRING_HASH_CASE_INSENSITIVE) ;
    Map *lib_unit = (Map *)_all_design_units->GetValue(lib_name) ;
    if (!lib_unit) {
        lib_unit = new Map(STRING_HASH_CASE_INSENSITIVE) ;
        (void) _all_design_units->Insert(elem->GetLibraryName(), lib_unit) ;
    }
    SetIter si ;
    const char *unit ;
    FOREACH_SET_ITEM(prim_units, si, &unit) (void) lib_unit->Insert(unit, elem) ;
    // VIPER #6399: end

    return elem ;
}

// Quickly analyze the file and create the data structure internally. The CreateNode
// routine calls this routine to create the data structure. This routine internally
// calls the QuickParse() routine declared later.
/* static */ unsigned
vhdl_sort::QuickAnalyze(const char *file_name, const char *lib_name /* = "work" */)
{
    if (!file_name) return 0 ;
    if (!lib_name) lib_name = "work" ;

    // StartFlex is similar to current vhdl_file::StartFlex()
    if (!StartFlex(file_name)) return 0 ;

    // Add the "std" and "work" libraries and the library into which this file is being analyzed
    // All of the above libraries can be referenced as the starting library in a hierarchical
    // name without first specifying them as libraries (using 'library ...;'). So, add them:
    (void) AddLibrary("std") ;
    (void) AddLibrary("work") ;
    (void) AddLibrary(lib_name) ;

    // Manually parse the required VHDL constructs only
    //Message::PrintLine("Quick Parsing VHDL file: ", file_name, " in library ", lib_name) ;
    (void) QuickParse() ;

    // Also clear out all recognized library names
    ClearAllLibraries() ;

    // EndFlex is similar to current vhdl_file::EndFlex()
    (void) EndFlex() ;

    return 1 ;
}

// Add a single entity provided
/* static */ unsigned
vhdl_sort::AddPrimaryUnit(const char *prim_unit, unsigned architecture /* = 0 */)
{
    if (!prim_unit) return 0 ;

    // Case insensitive hashing
    if (!_prim_units) _prim_units = new Set(STRING_HASH_CASE_INSENSITIVE) ;
    if (!_architecture_units) _architecture_units = new Set(STRING_HASH_CASE_INSENSITIVE) ;

    if (_prim_units->Get(prim_unit)) return 0 ; // Primary unit already in Set

    if (architecture) {
        char *saved_prim_unit = Strings::save(prim_unit) ;
        if (!_architecture_units->Insert(saved_prim_unit)) Strings::free(saved_prim_unit) ;
    }

    return _prim_units->Insert(Strings::save(prim_unit)) ;
}

// Add a single requirement
/* static */ unsigned
vhdl_sort::AddDependency(char *prim_unit, char *lib_name /* = 0 */, unsigned for_sec_unit /* = 0 */, unsigned for_comp_inst /* = 0 */)
{
    if (!lib_name) lib_name = Strings::save("work") ;
    // Accept 0 as the primary unit for 'use work.all;'

    if (!_dependencies) _dependencies = new Array() ;

    _dependencies->InsertLast(new VhdlFileDependency(lib_name, prim_unit, for_sec_unit, for_comp_inst)) ;

    return 1 ; // Always inserted
}

// Add a recognized library in the Set
/* static */ unsigned
vhdl_sort::AddLibrary(const char *lib_name)
{
    if (!lib_name) return 0 ;

    // Case insensitive hashing
    if (!_all_libraries) _all_libraries = new Set(STRING_HASH_CASE_INSENSITIVE) ;

    if (_all_libraries->Get(lib_name)) {
        return 0 ; // Library already in Set
    } else {
        return _all_libraries->Insert(Strings::save(lib_name)) ;
    }
}

// Return the requirements for a file quickly analyzed
/* static */ Array *
vhdl_sort::TakeDependencies()
{
    // Return the current requirements and set that to 0
    Array *requires = _dependencies ;
    _dependencies = 0 ;
    return requires ;
}

// Return the primary units provided by the file
/* static */ Set *
vhdl_sort::TakePrimaryUnits()
{
    // Return the current provides Set and set that to 0
    Set *provides = _prim_units ;
    _prim_units = 0 ;
    return provides ;
}

// Top-level vhdl_sort reset routine (calls all Reset/Clear methods)
/* static */ void
vhdl_sort::Reset()
{
    ClearRegisteredFiles() ; // Clean up the registered list of files
    ClearAllLibraries() ;    // Remove all recognized libraries
    ClearTopUnits() ;        // Remove all recognized top level design units
    ClearPrimaryUnits() ;    // Remove all primary units
    ClearDependencies() ;    // Remove any lingering QuckParsed requirements
    ResetMessageMap() ;    // Reset message table
}

// Clean up the sorted list of files
/* static */ void
vhdl_sort::ClearRegisteredFiles()
{
    // VIPER #6399: First delete the all design unit map:
    MapIter mi ;
    Map *units ;
    FOREACH_MAP_ITEM(_all_design_units, mi, 0, &units) delete units ;
    delete _all_design_units ;
    _all_design_units = 0 ;

    // Remove all file elements
    unsigned i ;
    VhdlFileElement *file_elem ;
    FOREACH_ARRAY_ITEM(_registered_files, i, file_elem) delete file_elem ;
    // Delete the Array and set it to 0
    delete _registered_files ;
    _registered_files = 0 ;

    // Delete the architecture set also:
    SetIter si ;
    char *arc_unit ;
    FOREACH_SET_ITEM(_architecture_units, si, &arc_unit) Strings::free(arc_unit) ;
    delete _architecture_units ;
    _architecture_units = 0 ;

    // Delete all registered directories:
    VhdlDirElement *dir_elem ;
    FOREACH_ARRAY_ITEM(_registered_dirs, i, dir_elem) delete dir_elem ;
    delete _registered_dirs ;
    _registered_dirs = 0 ;

    // Delete all extensions too:
    char *extension ;
    FOREACH_SET_ITEM(_extensions, si, &extension) Strings::free(extension) ;
    delete _extensions ;
    _extensions = 0 ;

    // And delete the determined top level design units too:
    ClearTopUnits() ;
    delete _top_units ;
    _top_units = 0 ;
}

// Remove all the recognized libraries
/* static */ void
vhdl_sort::ClearAllLibraries()
{
    if (!_all_libraries) return ;

    SetIter si ;
    char *lib_name ;
    FOREACH_SET_ITEM(_all_libraries, si, &lib_name) Strings::free(lib_name) ;
    delete _all_libraries ;
    _all_libraries = 0 ;
}

// Remove all primary units
/* static */ void
vhdl_sort::ClearPrimaryUnits()
{
    if (!_prim_units) return ; // Nothing to do
    SetIter si ;
    char *unit_name ;
    FOREACH_SET_ITEM(_prim_units, si, &unit_name) Strings::free(unit_name) ;
    delete _prim_units ;
    _prim_units = 0 ;
}

// Remove any lingering QuckParsed requirements
/* static */ void
vhdl_sort::ClearDependencies()
{
    if (!_dependencies) return ; // Nothing to do
    unsigned i ;
    VhdlFileDependency *depend ;
    FOREACH_ARRAY_ITEM(_dependencies, i, depend) delete depend ;
    delete _dependencies ;
    _dependencies = 0 ;
}

// Remove all the recognized top level design units
/* static */ void
vhdl_sort::ClearTopUnits()
{
    if (!_top_units) return ; // Nothing to do

    // Delete all the units:
    unsigned i ;
    VhdlUnitElement *unit ;
    FOREACH_ARRAY_ITEM(_top_units, i, unit) delete unit ;
    delete _top_units ;
    _top_units = 0 ;
}

/* -------------------------------------------------------------- */

VhdlFileElement::VhdlFileElement(const char *file_name, const char *lib_name, unsigned vhdl_mode, Set *provides, Array *requires)
  : _file_name(Strings::save(file_name)),
    _library_name(Strings::save(lib_name)),
    _analysis_mode(vhdl_mode),
    _prim_units(provides),
    _dependencies(requires)
{
}

VhdlFileElement::~VhdlFileElement()
{
    // Free the file and library names
    Strings::free(_file_name) ;
    Strings::free(_library_name) ;

    // Delete the names of primary units provided and the Set
    SetIter si ;
    char *primary_unit ;
    FOREACH_SET_ITEM(_prim_units, si, &primary_unit) Strings::free(primary_unit) ;
    delete _prim_units ;

    // Delete the requirements and the Array
    unsigned i ;
    VhdlFileDependency *depend ;
    FOREACH_ARRAY_ITEM(_dependencies, i, depend) delete depend ;
    delete _dependencies ;
}

#if 0
// Following API is not used anymore - commenting it out:
// Check whether this node satisfies the given requirement:
unsigned
VhdlFileElement::Satisfies(const VhdlFileDependency *requirement, const VhdlFileElement *requiring_node) const
{
    // No requirement, hence always satisfied!
    if (!requirement) return 1 ;
    // Cannot satisfy with nothing provided!
    if (!_prim_units) return 0 ;

    // If the library name does not match, we don't satisfy anything:
    // Requirements within work library matches with the current library
    // in which the file is analyzed, so, check only if the requirement
    // is outside of the "work" library:
    if (!(Strings::compare_nocase("work", requirement->GetLibrary()) && // This is a work library requirement library of the provider
          Strings::compare_nocase(_library_name, requiring_node->GetLibraryName())) && // and the requiring node must match in this case
        !Strings::compare_nocase(_library_name, requirement->GetLibrary())) return 0 ; // Otherwise, both provider and the requirement must be in the same library

    // If this requires the whole library, then we don't know
    // whether we have it or not, so, we don't satisfy that:
    if (!requirement->GetPrimaryUnit()) return 0 ;

    SetItem *primary_unit = _prim_units->GetItem(requirement->GetPrimaryUnit()) ;
    // This node does not provide the required primary unit
    if (!primary_unit) return 0 ;

    return 1 ; // Provides the primary unit
}
#endif

/* -------------------------------------------------------------- */

VhdlDirElement::VhdlDirElement(const char *path_name, const char *lib_name, unsigned vhdl_mode)
  : _path_name(Strings::save(path_name)),
    _library_name(Strings::save(lib_name)),
    _analysis_mode(vhdl_mode),
    _files(0)
{
}

VhdlDirElement::~VhdlDirElement()
{
    Strings::free(_path_name) ;    _path_name = 0 ;
    Strings::free(_library_name) ; _library_name = 0 ;

    SetIter si ;
    char *file_name ;
    FOREACH_SET_ITEM(_files, si, &file_name) Strings::free(file_name) ;
    delete _files ;
}

/* -------------------------------------------------------------- */

VhdlUnitElement::VhdlUnitElement(char *lib_name, char *primary_unit)
  : _library_name(lib_name),
    _primary_unit(primary_unit)
{
    //_library_name = Strings::save(lib_name) ;
    //_primary_unit = Strings::save(primary_unit) ;
}

VhdlUnitElement::~VhdlUnitElement()
{
    Strings::free(_library_name) ;
    _library_name = 0 ;
    Strings::free(_primary_unit) ;
    _primary_unit = 0 ;
}

/* -------------------------------------------------------------- */

VhdlFileDependency::VhdlFileDependency(char *lib_name, char *primary_unit, unsigned for_sec_unit /* = 0 */, unsigned for_comp_inst /* = 0 */)
  : VhdlUnitElement(lib_name, primary_unit),
    _satisfied_by(0),
    _for_sec_unit(for_sec_unit),
    _for_comp_inst(for_comp_inst)
{
}

VhdlFileDependency::~VhdlFileDependency()
{
    _satisfied_by = 0 ;
}

/* -------------------------------------------------------------- */

#ifdef VHDLSORT_FLEX_READ_FROM_STREAM
// VIPER #6493: Read from streams:
/* static */ void
vhdl_sort::RegisterFlexStreamCallBack(verific_stream *(*stream_func)(const char *file_name))
{
    _stream_callback_func = stream_func ;
}

/* static */ verific_stream *
vhdl_sort::GetUserFlexStream(const char *file_name)
{
    if (!_stream_callback_func) return 0 ;

    return _stream_callback_func(file_name) ;
}

/* -------------------------------------------------------------- */
#endif

/* static */ void
vhdl_sort::Error(linefile_type lf, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_ERROR, GetMessageId(format), lf, format, args) ;
    va_end(args) ;
}

/* static */ void
vhdl_sort::Warning(linefile_type lf, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_WARNING, GetMessageId(format), lf, format, args) ;
    va_end(args) ;
}

/* static */ void
vhdl_sort::Info(linefile_type lf, const char *format, ...)
{
    va_list args ;
    va_start(args, format) ;
    Message::MsgVaList(VERIFIC_INFO, GetMessageId(format), lf, format, args) ;
    va_end(args) ;
}

/* static */ const char *
vhdl_sort::GetMessageId(const char *msg)
{
    if (!msg) return 0 ;

    if (!_msgs) {
        _msgs = new Map(STRING_HASH, 29) ; // Table size : choose prime number for good hashing. Enough initial storage until message VSRT-1019.

        _msgs->Insert("cannot open vhdl file %s", "VSRT-1001") ;
        _msgs->Insert("error in protected region near %s", "VSRT-1002") ;
        _msgs->Insert("illegal identifier : %s", "VSRT-1003") ;
        _msgs->Insert("base value %d must be at least two and at most sixteen", "VSRT-1004") ;
        _msgs->Insert("exponent of integer literal cannot be negative", "VSRT-1005") ;
        _msgs->Insert("literal %s exceeds maximum integer value", "VSRT-1006") ;
        _msgs->Insert("digit '%c' is larger than base in %s", "VSRT-1007") ;
        _msgs->Insert("construct '%s' not allowed in VHDL'87; use VHDL'93 mode", "VSRT-1008") ;
        _msgs->Insert("syntax error near %s", "VSRT-1009") ;
        _msgs->Insert("unexpected EOF", "VSRT-1010") ;
        _msgs->Insert("circular dependecy found for file %s while ordering", "VSRT-1011") ;

        /* Following messages are not required (THEY NEVER MADE IT TO THE MESSAGE TABLE):
        _msgs->Insert("translate_on found outside a translate_off block", "VSRT-1012") ;
        _msgs->Insert("unmatched %s translate/synthesis off pragma found; matching pair with same keywords is required", "VSRT-1013") ;
        _msgs->Insert("Inserting %s into the sorted list", "VSRT-1014") ;
        _msgs->Insert("  = Library: %s", "VSRT-1015") ;
        _msgs->Insert("    -> Provides Architecture: %s", "VSRT-1016") ;
        _msgs->Insert("    <- Requires%s: %s%s%s", "VSRT-1017") ;
        _msgs->Insert("    -> Provides Entity-Architecture: %s", "VSRT-1018") ;
        _msgs->Insert("    <- Requires Entity (for Architecture declaration): %s", "VSRT-1019") ;
        _msgs->Insert("    -> Provides Entity/Context: %s", "VSRT-1020") ;
        _msgs->Insert("    <- Requires Package (for Package Body declaration): %s", "VSRT-1021") ;
        _msgs->Insert("    -> Provides Package: %s", "VSRT-1022") ;
        _msgs->Insert("    -> Provides Configuration: %s", "VSRT-1023") ;
        _msgs->Insert("    <- Requires Entity (for Configuration declaration): %s", "VSRT-1024") ;
        _msgs->Insert("    <- Requires Entity-Architecture (for Configuration declaration): %s", "VSRT-1025") ;
        _msgs->Insert("    -> Provides VUnit/VProp/VMode: %s", "VSRT-1026") ;
        _msgs->Insert("    -> Provides Entity-VUnit/VProp/VMode: %s", "VSRT-1027") ;
        _msgs->Insert("    <- Requires Entity (for VUnit/VProp/VMode declaration): %s", "VSRT-1028") ;
        */

        //_msgs->Insert("/* in comment", "VSRT-1029") ;
    }

    return (const char*)_msgs->GetValue(msg) ;
}

/* static */ void
vhdl_sort::ResetMessageMap()
{
    delete _msgs ;
    _msgs = 0 ;
}
