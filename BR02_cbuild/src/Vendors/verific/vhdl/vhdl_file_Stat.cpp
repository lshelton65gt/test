/*
 *
 * [ File Version : 1.291 - 2014/03/18 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "vhdl_file.h" // Compile flags are defined within here
#include <stdio.h>  // for sprintf()
#include <cstring>  // for strcmp on older compilers

#include "Array.h"
#include "Set.h"
#include "Strings.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "Message.h"
#include "VhdlCopy.h"
#include "VhdlScope.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlConfiguration.h"
#include "VhdlSpecification.h"
#include "VhdlName.h"
#include "VhdlMisc.h"
#include "vhdl_tokens.h"
#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"
#include "VhdlTreeNode.h"
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
#include "../verilog/VeriModule.h"
#include "../verilog/VeriScope.h"
#endif
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
#include "../verilog/VeriId.h"
#include "../verilog/VeriExpression.h"
#include "../verilog/VeriConstVal.h"
#include "../verilog/veri_file.h"
#include "../verilog/VeriLibrary.h"
#include "../verilog/VeriElab_Stat.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// This routine is to be called on top level entity. It will copy itself and then elaborate the copied one
VhdlPrimaryUnit *
VhdlEntityDecl::StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)
{
    if (!_id) return 0 ; // need this member

    // VIPER #3576 : Check if we need to compile this entity as a black-box or not :
    if (!_compile_as_blackbox) {
        _compile_as_blackbox = vhdl_file::CheckCompileAsBlackbox(_id->Name()) ; // call user-registered call-back method
    }
    if (_compile_as_blackbox) return 0 ;

    // Set global constant flags and VhdlNode::_static_elab to do static elaboration properly
    VhdlNode::SetConstNets() ;
    VhdlNode::SetStaticElab() ;

    // VIPER #2995 : Set the _work_lib as library of this unit.
    VhdlLibrary *old_worklib = vhdl_file::GetWorkLib() ; // Remember previous one
    vhdl_file::SetWorkLib(_owner) ;
    // VIPER #6937 : Set current analysis mode to be that of this unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;
    vhdl_file::SetAnalysisMode(GetAnalysisDialect()) ;

    VhdlSecondaryUnit *arch = GetSecondaryUnit(secondary_unit_name) ;
    if (!secondary_unit_name && arch) secondary_unit_name = arch->Id() ? arch->Id()->OrigName(): 0 ;
    // Viper #5051 : path_name/instance_name attribute handling
    Array *old_path_name = _path_name ;
    Array *old_instance_name = _instance_name ;
    _path_name = 0 ;
    _instance_name = 0 ;
    PushAttributeSpecificPath(0, _id->Name(), secondary_unit_name) ;

    VhdlPrimaryUnit *prim_unit = this ;
    Array *copied_actual_generic = actual_generics ;
#ifdef VHDL_COPY_TOP_BEFORE_STATIC_ELAB
    // Create a new name <original_name>_default for this entity
    char *new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(_id->OrigName(), "_default") ;

    // Get the library, owner of the entity
    const char *lib_name = _id->GetContainingLibraryName() ;
    if (!lib_name) lib_name = vhdl_file::GetWorkLib()->Name() ;
    VhdlLibrary *lib = vhdl_file::GetLibrary(lib_name) ;

    VhdlMapForCopy old2new ;

    // Check if entity of that name already exists
    VhdlPrimaryUnit *exists = lib ? lib->GetPrimUnit(new_entity_name) : 0 ;
    if (exists) { // Entity exists
        if (arch && !exists->GetSecondaryUnit(secondary_unit_name)) {
            // Original entity does not have architecture 'secondary_unit_name', so
            // we have to copy the architecture. But architecture can use ports/generics
            // of entity, so in copied architecture those references should point to
            // iddefs of 'exists' not 'this'
            old2new.MakeAssociations(this, exists) ;

            // Copy the architecture to be elaborated
            VhdlSecondaryUnit *sec_unit = arch->CopySecondaryUnit(0, old2new) ;
            // Add secondary unit to existing entity
            if (exists->AddSecondaryUnit(sec_unit)) {
                sec_unit->Id()->DeclareArchitectureId(exists->Id()) ;
                // Change the entity name referred by this architecture
                sec_unit->ChangeEntityReference(exists->Id()) ;
            }
        }
        prim_unit = exists ;
    } else {
        // Copy the entity
        prim_unit = secondary_unit_name ? CopyEntityArchitecture(new_entity_name, secondary_unit_name, old2new) : CopyPrimaryUnit(new_entity_name, old2new, 0) ;

        // Add created entity to library
        if (lib && !lib->AddPrimUnit(prim_unit)) {
            vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
            vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
            return 0 ;  // Cannot add copied entity to library, return
        }
    }
    Strings::free(new_entity_name) ;
    // VIPER #6430 : If top unit is copied before elaboration, change the formals
    // of argument specified 'actual_generics'
    if ((prim_unit != this) && actual_generics) {
        copied_actual_generic = prim_unit->CopyInterfaceAssociationList(actual_generics) ;
    }
    // VIPER #6793 : Store the top level unit to be elaborated when VHDL_COPY_TOP_BEFORE_STATIC_ELAB is off
    vhdl_file::_top_unit = 0 ;
#else
    // VIPER #6793 : Store the top level unit to be elaborated when VHDL_COPY_TOP_BEFORE_STATIC_ELAB is off
    vhdl_file::_top_unit = prim_unit ;

#endif
    // VIPER #7367 : Set the top unit name used in elaboration
    vhdl_file::_top_unit_name = prim_unit ? prim_unit->Name(): 0 ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #7727 : Donot create new VeriStaticElaborator class when there is
    // existing one. Elaboration may be strated for mixed language tops.
    VeriStaticElaborator *existing_elab_obj = veri_file::GetStaticElabObj() ;
    // VIPER #5624 : In mixed language node now create pseudo tree node for
    // vhdl units also to resolve cross labguage hierarchical names properly
    VeriStaticElaborator *elab_obj = 0 ;
    if (!existing_elab_obj) elab_obj = new VeriStaticElaborator(prim_unit, secondary_unit_name) ;
#endif

    if (prim_unit) (void) VhdlNode::PushStructuralStack(prim_unit->Id(), prim_unit->Id()) ;
    // Static elaborate the entity
    VhdlPrimaryUnit *elaborated_unit = (prim_unit) ? prim_unit->StaticElaborateInternal(secondary_unit_name, copied_actual_generic, actual_ports, block_config, actual_component) : 0 ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // Full hierarchical tree is created for the design. Now resolve
    // hierarchical ids
    //if (elab_obj) elab_obj->Elaborate(1 /* from vhdl*/) ;
    if (elab_obj) elab_obj->ElaborateVhdlTop() ;
    delete elab_obj ;
#endif // VHDL_DUAL_LANGUAGE_ELABORATION

    CleanConfigNumMap() ; // VIPER #6032 : Clean configuration map
    CleanUnitNumMap() ; // VIPER #7054
    CleanLong2ShortMap() ; // VIPER #7853
    vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
    if (prim_unit) VhdlNode::PopStructuralStack() ;
    ClearAttributeSpecificPath() ;
    _path_name = old_path_name ;
    _instance_name = old_instance_name ;

    // VIPER #6430 : If actual_generic is copied, clear that
    if (copied_actual_generic != actual_generics) {
        unsigned i ;
        VhdlDiscreteRange *assoc ;
        FOREACH_ARRAY_ITEM(copied_actual_generic, i, assoc) delete assoc ;
        delete copied_actual_generic ;
    }
    // VIPER #6793 : Reset the top level unit stored in vhdl_file
    vhdl_file::_top_unit = 0 ;
     vhdl_file::_top_unit_name = 0 ; // VIPER #7367 : Set top unit name
    return elaborated_unit ;
}

// Internal routine to elaborate entity
VhdlPrimaryUnit *
VhdlEntityDecl::StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)
{
    if (!_id) return 0 ; // need this member
    // VIPER #3576 : Check if we need to compile this entity as a black-box or not :
    if (!_compile_as_blackbox) {
        _compile_as_blackbox = vhdl_file::CheckCompileAsBlackbox(_id->Name()) ; // call user-registered call-back method
    }
    if (_compile_as_blackbox) return 0 ;

    // VIPER #2995 : Set the _work_lib as library of this unit.
    VhdlLibrary *old_worklib = vhdl_file::GetWorkLib() ; // Remember previous one
    vhdl_file::SetWorkLib(_owner) ;
    // VIPER #6937 : Set current analysis mode to be that of this unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;
    vhdl_file::SetAnalysisMode(GetAnalysisDialect()) ;

    // Find the architecture by name.
    // If name is not there, take the last one compiled
    VhdlSecondaryUnit *architecture = 0 ;
    architecture = GetSecondaryUnit(secondary_unit_name) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    VeriStaticElaborator *elab_obj = veri_file::GetStaticElabObj() ;
    VeriPseudoTreeNode *saved_curr_node = elab_obj ? elab_obj->GetCurrentNode(): 0 ;
    if (IsStaticElaborated() && elab_obj && ((architecture && architecture->IsStaticElaborated()) || _is_verilog_module)) {
        // This unit is already elaborated, so create pseudo tree node for this unit and its hierarchy
        elab_obj->CopyCurrentNode(this) ;
        elab_obj->SetCurrentNode(saved_curr_node) ;
    } else {
        // Create pseudo tree node for vhdl unit
        if (elab_obj && !_is_verilog_module) {
            elab_obj->AddVhdlNode(saved_curr_node, this, architecture ? architecture->Name(): 0, elab_obj->GetVhdlInstanceName()) ;
            VeriPseudoTreeNode *curr_node = elab_obj->GetCurrentNode() ;
            // VIPER #6909 : Mark created node as elaborated
            if (curr_node) curr_node->SetAsElaborated() ;
        }
    }
#endif // VHDL_DUAL_LANGUAGE_ELABORATION

    // Clear error count
    Message::ClearErrorCount() ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    VhdlIdDef *save_boolean_type = BooleanType() ;
    _boolean_type = StdType("boolean") ;

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    _present_scope = old_scope ;
    // Split the generic list and set proper values of generics
    SplitAndAssociateGenerics(actual_generics, actual_component) ;
    // Split the port list, so that every unconstrained
    // port can get proper constraint
    SplitAndAssociatePorts(actual_ports, actual_component) ;
    _present_scope = _local_scope ;

    // Never execute an already executed entity architecture pair if there is no block configuration
    if (IsStaticElaborated() && ((architecture && architecture->IsStaticElaborated()) || _is_verilog_module)) {
        _boolean_type = save_boolean_type ;
        _present_scope = old_scope ;
        vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
        vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
        return this ;
    }

    if (!_is_verilog_module) _id->Info("executing %s(%s)", _id->OrigName(), architecture ? architecture->Id()->OrigName(): "")  ;

    // Set that this primary unit is executed
    SetStaticElaborated() ;

    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow decl_flow(0, 0, _local_scope) ;
    decl_flow.SetInInitial() ;

    unsigned i ;
    // Elaborate the declarative part
    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (elem) elem->Elaborate(&decl_flow) ;
    }

    // Elaborate the statements
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) stmt->Elaborate(0,0) ;
    }

    if (Message::ErrorCount() && !_is_verilog_module) _id->Info("execution of entity %s failed", _id->Name()) ;

    // Elaborate the architecture
    if (architecture) architecture->StaticElaborate(block_config) ;

    VhdlPrimaryUnit *ret_unit = this ;

// Viper 4577: Elaborate the verilog module here.
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    if (_is_verilog_module && Id()) {
        VhdlIdDef *unit = Id() ;
        // Viper 7053: Here we should treat work as library named work so pass from_lib_options
        VeriModule *verilog_module = vhdl_file::GetVerilogModuleFromlib(unit->GetContainingLibraryName(), Name(), 1/*from_lib_options*/) ;
        if (verilog_module) {
            // VIPER #3772: Set the work library here:
            VeriLibrary *work_lib = veri_file::GetWorkLib() ;
            veri_file::SetWorkLib(verilog_module->GetLibrary()) ;

            // Viper 7285 : Go back to instantiating scope to evaluate StdType("std_ulogic") in ImageVerilog
            _present_scope = old_scope ;
            Map parameters(STRING_HASH) ;
            GetParameterFromGeneric(parameters, 0, 0, 0) ;
            _present_scope = _local_scope ;

            Map *actual_params = 0 ;
            actual_params = verilog_module->CreateActualParameters(&parameters) ;
            Array top_mods ; // This will contain the actual top modules after static elaboration
            // If instantiated verilog in turn instantiates vhdl unit containing
            // path_name attribute, the following unsigned field will be 1
            unsigned has_path_name = 0 ;
            VeriModule *old_mod = verilog_module ;
            (void) verilog_module->StaticElaborateInternal(actual_params, &top_mods, 1 /* from vhdl*/, &has_path_name) ;
            if (top_mods.Size() >= 1) {
                if (top_mods.Size() > 1) {
                     VERIFIC_ASSERT(verilog_module->IsConfiguration()) ; // It must be a configuration
                     // It should not have more than one top modules (even for the configuration):
                     verilog_module->Error("configuration with multiple designs cannot be instantiated") ;
                }
                verilog_module = (VeriModule *)top_mods.GetLast() ; // Get the last 'design' top module anyway
                // Propagate the path_name attribute i.e. set in this entity
                // and its instantiating entity
                if (has_path_name) PropagateAffectOfPathName() ;
            }
            // Viper 3402 needs special checking. Since the vhdl ports of the converted module is
            // unconstrained (because it is difficult to evaluate size in ConvertModule) the constraint
            // is set from the actual
            CheckPortBinding(verilog_module, actual_component) ;
            // VIPER #3772: Reset the work library to the original one:
            // But do this only when the stored work lib is valid here:
            if (work_lib) veri_file::SetWorkLib(work_lib) ;
            // VIPER #5805 : Instantiated verilog contains vhdl instance having
            // path name attribute. So instantiated verilog is copied with
            // different name, convert this new verilog to vhdl unit :
            if (/*has_path_name &&*/ verilog_module && (verilog_module != old_mod)) {
                ret_unit = vhdl_file::GetVhdlUnitFromVerilog(verilog_module->GetLibrary()->GetName(), verilog_module->Name()) ;
                if (ret_unit && has_path_name) ret_unit->SetAffectedByPathName() ;
                if (ret_unit) {
                    // VIPER #7079 : Set generic values and port constraints to
                    // this new converted entity also
                    // Split the generic list and set proper values of generics
                    ret_unit->SplitAndAssociateGenerics(actual_generics, actual_component) ;
                    // Split the port list, so that every unconstrained
                    // port can get proper constraint
                    ret_unit->SplitAndAssociatePorts(actual_ports, actual_component) ;

                    // Set that this primary unit is executed
                    ret_unit->SetStaticElaborated() ;
                }
                // VIPER #7624 : Verilog elaboration creats another module, so
                // this module/entity is not elaborated no more. Reset the marking
                _is_static_elaborated = 0 ;
            }
            // Viper 5933. Set the initial value of the generic from the module
            // The module is already elaborated and hence has its parameters evaluated
            SetGenericInitial(verilog_module) ;

            MapIter mi ;
            char *image ;
            FOREACH_MAP_ITEM(&parameters, mi, 0, &image) Strings::free(image) ;
            VeriExpression *actual ;
            char *name ;
            FOREACH_MAP_ITEM(actual_params, mi, &name, &actual) {
                Strings::free(name) ;
                delete actual ;
            }
            delete actual_params ;
        }
    }
#endif
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
        if (elab_obj) elab_obj->SetCurrentNode(saved_curr_node) ; // Pop current node setting
#endif

    _boolean_type = save_boolean_type ;

    // Reset the global scope
    _present_scope = old_scope ;
    vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937

    return ret_unit ;
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// This function sets initial value of generics of converted entity
// from the corresponding elaborate Verilog Module.
void
VhdlEntityDecl::SetGenericInitial(VeriModule *module)
{
    if (!_is_verilog_module || !module || !module->IsStaticElaborated()) return ;
    Array *parameters = module->GetParameters() ;
    Map id_to_val(STRING_HASH) ;
    unsigned i ;
    VeriIdDef *veri_id ;
    // Iterate through the array of parameters
    FOREACH_ARRAY_ITEM(parameters, i, veri_id) {
        if (!veri_id) continue ;
        VhdlName *vhdl_init_val = 0 ;
        VeriExpression *expr = veri_id->GetInitialValue() ; // Get the initial value of the parameter
        // Use the VeriExpression::ConstCast function to cast the expression into a VeriConst*
        VeriConst *const_expr = (expr) ? expr->ConstCast() : 0 ;
        if (!const_expr) continue ;

        if (const_expr->IsString()) { // If the constant expression is a string
            char *image = const_expr->Image() ;
            vhdl_init_val = new VhdlStringLiteral(image) ; // Convert to VhdlExpression*
        } else if (const_expr->IsReal()) { // If the constant expression is of 'real' type
            char *image = const_expr->Image() ; // Find the string equivalent
            double init_val = Strings::atof(image) ; // Convert the string to real
            vhdl_init_val = new VhdlReal(init_val) ; // Convert to VhdlExpression*
        } else { // if the constant expression is an integer
            if (const_expr->HasXZ()) continue ;
            if (!((const_expr->IsUnsizedConst() == 0) && (const_expr->Size(0) > 32))) {
                verific_int64 init_val = const_expr->Integer() ;
                vhdl_init_val = new VhdlInteger(init_val) ; // Convert to VhdlExpression*
            }
        }
        if (vhdl_init_val) {
            id_to_val.Insert(VeriNode::CreateEscapedNameFromVerilog(veri_id->Name(), 1, module->GetPorts(), parameters), vhdl_init_val) ;
            vhdl_init_val->SetLinefile(veri_id->Linefile()) ;
        }
    }

    VhdlInterfaceDecl *decl ;

    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) { // Iterate over interface declarations
        if (!decl) continue ;
        Array *ids = decl->GetIds() ;
        // There is only one id in ids for converted module
        // Valid from creation in ConvertModule
        VhdlIdDef *id = (VhdlIdDef*)ids->At(0) ;
        if (!id) continue ;
        VhdlExpression *new_val = (VhdlExpression*)id_to_val.GetValue(id->OrigName()) ;
        if (new_val) decl->SetInitAssign(new_val) ;
    }
    MapIter mi ;
    char *created_name ;
    FOREACH_MAP_ITEM(&id_to_val, mi, &created_name, 0) Strings::free(created_name) ;
}
#endif

VhdlPrimaryUnit *
VhdlConfigurationDecl::StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)
{
    VhdlPrimaryUnit *u = StaticElaborateConfig(secondary_unit_name, actual_generics, actual_ports, block_config, actual_component, 1 /* from top*/) ;
    CleanConfigNumMap() ; // VIPER #6032 : Clean configuration map
    CleanUnitNumMap() ; // VIPER #7054
    CleanLong2ShortMap() ; // VIPER #7853
    return u ;
}
VhdlPrimaryUnit *
VhdlConfigurationDecl::StaticElaborateConfig(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component, unsigned is_top)
{
    (void) is_top ;
    // VIPER #3576 : Check if we need to compile this entity as a black-box or not :
    if (!_compile_as_blackbox) {
        _compile_as_blackbox = vhdl_file::CheckCompileAsBlackbox(_id ? _id->Name() : 0) ; // call user-registered call-back method
    }
    if (_compile_as_blackbox) return 0 ;

    // Set global constant flags and VhdlNode::_static_elab to do static elaboration properly
    VhdlNode::SetConstNets() ;
    VhdlNode::SetStaticElab() ;

    // VIPER #2995 : Set the _work_lib as library of this unit.
    VhdlLibrary *old_worklib = vhdl_file::GetWorkLib() ; // Remember previous one
    vhdl_file::SetWorkLib(_owner) ;
    // VIPER #6937 : Set current analysis mode to be that of this unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;
    vhdl_file::SetAnalysisMode(GetAnalysisDialect()) ;

    // Path stack handling : push the entity name in the path stack :
    VhdlIdDef *entity_id = (_id) ? _id->GetEntity() : 0 ;
    VhdlPrimaryUnit *entity_decl = (entity_id) ? entity_id->GetPrimaryUnit() : 0 ;
    VhdlSecondaryUnit *sec_unit = (entity_decl) ? entity_decl->GetSecondaryUnit(secondary_unit_name) : 0 ;
    // Viper #5051 : path_name/instance_name attribute handling
    Array *old_path_name = _path_name ;
    Array *old_instance_name = _instance_name ;
    _path_name = 0 ;
    _instance_name = 0 ;
    PushAttributeSpecificPath(0, entity_id ? entity_id->Name() : 0, sec_unit ? sec_unit->Name() : 0) ;

    VhdlPrimaryUnit *prim_unit = this ;
#ifdef VHDL_COPY_TOP_BEFORE_STATIC_ELAB
    // Create a new name <original_name>_default for this configuration
    char *new_config_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(_id ? _id->OrigName() : 0, "_default") ;

    // Get the library, owner of the entity
    const char *lib_name = _id ? _id->GetContainingLibraryName() : 0 ;
    if (!lib_name) lib_name = vhdl_file::GetWorkLib()->Name() ;
    VhdlLibrary *lib = vhdl_file::GetLibrary(lib_name) ;

    // Check if configuration named <original_name>_default already in library
    VhdlPrimaryUnit *exists = lib ? lib->GetPrimUnit(new_config_name) : 0 ;

    if (exists) { // Unit already exists
        prim_unit = exists ; // Elaborate the existing unit
    } else {
        // Copy the configuration
        VhdlMapForCopy old2new ;
        prim_unit = CopyPrimaryUnit(new_config_name, old2new, 1) ;
        // Add created entity to library
        if (lib && !lib->AddPrimUnit(prim_unit)) {
            vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
            vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
            return 0 ;  // Cannot add copied configuration to library, return
        }
    }
    Strings::free(new_config_name) ;
#endif
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #5624 : If this is the top level unit, create static elaboration object
    // VIPER #7727 : Donot create new VeriStaticElaborator class when there is
    // existing one. Elaboration may be strated for mixed language tops.
    VeriStaticElaborator *existing_elab_obj = veri_file::GetStaticElabObj() ;
    VeriStaticElaborator *elab_obj = (is_top && !existing_elab_obj) ? new VeriStaticElaborator(prim_unit, secondary_unit_name): 0 ;
#endif
    if (prim_unit) (void) VhdlNode::PushStructuralStack(prim_unit->Id(), prim_unit->Id()) ;
    // Static elaborate the configuration
    VhdlPrimaryUnit *elaborated_unit = (prim_unit) ? prim_unit->StaticElaborateInternal(secondary_unit_name, actual_generics, actual_ports, block_config, actual_component) : 0 ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    if (is_top && elab_obj) {
        //elab_obj->Elaborate(1 /* from vhdl*/) ; // Elaborate the full hierarchy
        elab_obj->ElaborateVhdlTop() ; // Elaborate the full hierarchy
        delete elab_obj ;
    }
#endif
    vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
    ClearAttributeSpecificPath() ;
    _path_name = old_path_name ;
    _instance_name = old_instance_name ;
    if (prim_unit) VhdlNode::PopStructuralStack() ;
    return elaborated_unit ;
}

VhdlPrimaryUnit *
VhdlConfigurationDecl::StaticElaborateInternal(const char * /*secondary_unit_name*/, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef *actual_component)
{
    if (!_id) return 0 ;
    // VIPER #3576 : Check if we need to compile this entity as a black-box or not :
    if (!_compile_as_blackbox) {
        _compile_as_blackbox = vhdl_file::CheckCompileAsBlackbox(_id->Name()) ; // call user-registered call-back method
    }
    if (_compile_as_blackbox) return 0 ;

    // Find the entity configured by this configuration
    VhdlIdDef *the_entity = _id->GetEntity() ;
    if (!the_entity) return 0 ; // analysis failed
    VhdlPrimaryUnit *entity = (!_is_verilog_module) ? the_entity->GetPrimaryUnit() : 0 ;

    SetStaticElaborated() ;

    // VIPER #2995 : Set the _work_lib as library of this unit.
    VhdlLibrary *old_worklib = vhdl_file::GetWorkLib() ; // Remember previous one
    vhdl_file::SetWorkLib(_owner) ;
    // VIPER #6937 : Set current analysis mode to be that of this unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;
    vhdl_file::SetAnalysisMode(GetAnalysisDialect()) ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    VhdlIdDef *save_boolean_type = BooleanType() ;
    _boolean_type = StdType("boolean") ;

     _id->Info("executing configuration %s", _id->OrigName()) ;

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow decl_flow(0, 0, _local_scope) ;
    decl_flow.SetInInitial() ;

    // Elaborate the declarations
    unsigned i ;
    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        elem->Elaborate(&decl_flow) ;
    }

    // Find the architecture to be configured
    VhdlIdDef *arch = (_block_configuration) ? _block_configuration->BlockLabel() : 0 ;
    const char *arch_name = (arch) ? arch->Name() : 0 ;

    // Now elaborate the entity
    unsigned is_master_copied = 0 ;
    Array *copied_generics = actual_generics ;
    Array *copied_ports = actual_ports ;
    VhdlBlockConfiguration *copied_blk_config = _block_configuration ;

    VhdlMapForCopy old2new ;
    // Copy the instantiated entity architecture pair for different generic values
    if (entity) {
        // Get the unique entity to be elaboarted.  It may be the case that
        // the original entity is to be copied. If copy is not required,
        // return the original one.
        entity = entity->CreateUniqueUnit(&copied_generics, &copied_ports, _block_configuration, actual_component, arch_name, old2new, _id->OrigName()) ;
        if (!entity) {
            _boolean_type = save_boolean_type ;
            return 0 ;
        }
        is_master_copied = 1 ;
    }

    // Here 'actual_generics', 'actual_ports' and block configuration may come from
    // configuration declaration.  In block configuration, there is a back pointer
    // to the scope of the block to be configured. For example 'architecture-scope'
    // can be set in block configuration'.  Now we copy the architecture to be
    // configured, so the back pointer of architecture-scope in configuration
    // should now point to the copied architecture-scope. If this back pointer is
    // not set properly, the label of the component instantiation inside arch will
    // not obtain their proper binding statement. Again if the port map/generic map
    // is specified within block configuration in named association form, then those
    // generic/port map aspect can have back pointers to the ports/generics of the
    // entity (copied here).  To set all the back pointers properly, the following
    // arguments are copied when the entity to be configured is copied.
    // 'actual_generics', 'actual_ports' are copied within function 'CreateUniqueUnit'
    if (is_master_copied) {
        copied_blk_config = _block_configuration ? new VhdlBlockConfiguration(*_block_configuration, old2new) : 0 ;
    }
    // Elaborate the block configuration :
    // VIPER 2012 : stop config executation here
    //if (copied_blk_config) copied_blk_config->Elaborate() ;

    // Elaborate the entity
    if (entity) (void) entity->StaticElaborateInternal(arch_name, copied_generics, copied_ports, copied_blk_config, actual_component) ;

// Viper 4577: Elaborate the verilog module here.
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    if (_is_verilog_module && Id()) {
        // The configuration is a converted one. Pick up the verilog configuration and elaborate Viper 4972.
        VhdlIdDef *unit = Id() ;
        // Viper 7053: Here we should treat work as library named work so pass from_lib_options
        VeriModule *verilog_module = vhdl_file::GetVerilogModuleFromlib(unit->GetContainingLibraryName(), Name(), 1/*from_l_option*/) ;
        if (verilog_module) {
            // VIPER #3772: Set the work library here:
            VeriLibrary *work_lib = veri_file::GetWorkLib() ;
            veri_file::SetWorkLib(verilog_module->GetLibrary()) ;
            AssociateGenerics(actual_generics, actual_component) ;
            Map parameters(STRING_HASH) ;
            GetParameterFromGeneric(parameters, copied_generics, verilog_module, actual_component) ;
            Map *actual_params = 0 ;
            actual_params = verilog_module->CreateActualParameters(&parameters) ;
            Array top_units ; // This will contain the actual top modules after static elaboration.
                              // It is required for verilog configurations having a vhdl design entity
            (void) verilog_module->StaticElaborateInternalToGetUnits(actual_params, &top_units, 1 /* from vhdl*/, IsAffectedByPathName()) ;
            if (top_units.Size() >= 1) {
                if (top_units.Size() > 1) {
                     VERIFIC_ASSERT(verilog_module->IsConfiguration()) ; // It must be a configuration
                     // It should not have more than one top modules (even for the configuration):
                     verilog_module->Error("configuration with multiple designs cannot be instantiated") ;
                }
                entity = (VhdlPrimaryUnit*)top_units.GetLast() ; // Get the last 'design' top unit anyway
                // Propagate the information about presence of path_name upwards
                if (entity && entity->IsAffectedByPathName()) PropagateAffectOfPathName() ;
            }
            // Viper 3402 needs special checking. Since the vhdl ports of the converted module is
            // unconstrained (because it is difficult to evaluate size in ConvertModule) the constraint
            // is set from the actual
            CheckPortBinding(verilog_module, actual_component) ;
            // VIPER #3772: Reset the work library to the original one:

            veri_file::SetWorkLib(work_lib) ;

            MapIter mi ;
            char *image ;
            FOREACH_MAP_ITEM(&parameters, mi, 0, &image) Strings::free(image) ;
            VeriExpression *actual ;
            char *name ;
            FOREACH_MAP_ITEM(actual_params, mi, &name, &actual) {
                Strings::free(name) ;
                delete actual ;
            }
            delete actual_params ;
        }
    }
#endif

    if (is_master_copied) {
        FOREACH_ARRAY_ITEM(copied_generics, i, elem) delete elem ;
        FOREACH_ARRAY_ITEM(copied_ports, i, elem) delete elem ;
        delete copied_generics ;
        delete copied_ports ;
        delete copied_blk_config ;
    }

    _boolean_type = save_boolean_type ;

    // Reset the global scope
    _present_scope = old_scope ;
    vhdl_file::SetWorkLib(old_worklib) ; // Pop old one

    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
    // Return the elaborated entity. If any component
    // configuration is bound to this configuration,
    // the component is to be bound with this entity.
    return (entity) ;
}
VhdlPrimaryUnit *
VhdlPackageDecl::StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)
{
    // VIPER #2995 : Set the _work_lib as library of this unit.
    VhdlLibrary *old_worklib = vhdl_file::GetWorkLib() ; // Remember previous one
    vhdl_file::SetWorkLib(_owner) ;
    // VIPER #6937 : Set current analysis mode to be that of this unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;
    vhdl_file::SetAnalysisMode(GetAnalysisDialect()) ;

    // Set global constant flags and VhdlNode::_static_elab to do static elaboration properly
    VhdlNode::SetConstNets() ;
    VhdlNode::SetStaticElab() ;

    // Do not copy package, only call 'StaticElaborateInternal'
    VhdlPrimaryUnit *elaborated_unit = StaticElaborateInternal(secondary_unit_name, actual_generics, actual_ports, block_config, actual_component) ;

    vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937

    return elaborated_unit ;
}
VhdlPrimaryUnit *
VhdlPackageDecl::StaticElaborateInternal(const char * /*secondary_unit_name*/, Array *actual_generics, Array * /*actual_ports*/, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef * /*actual_component*/)
{
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Conversion of verilog package to vhdl package:
    // Fill up the vhdl ranges with vhdl expression during elaboration:
    FillRangesAndInitialExpr() ;
#endif

    // VHDL-2008 standard defines 3 types of packages
    // 1. Simple package : Package without generic clause and generic map aspect
    // 2. Uninstantiated package : Package with generic clause and no generic map aspect
    // 3. Generic mapped package : Package with generic clause and generic map aspect

    // Elaboration should be done on simple and generic mapped package. Uninstantiated package
    // will be elaborated when generic map aspect will be supplied from package instantiation
    if (_generic_clause && !_generic_map_aspect && !actual_generics) return 0 ;

    // Elaborate a package header.
    // Check if there is a body we need to execute
    // Body always has the same name as the header
    VhdlSecondaryUnit *body  ;
    body = GetSecondaryUnit(Name()) ;

    if (!actual_generics && IsStaticElaborated() && (!body || body->IsStaticElaborated())) return 0;

    SetStaticElaborated() ;

    // VIPER #2995 : Set the _work_lib as library of this unit.
    VhdlLibrary *old_worklib = vhdl_file::GetWorkLib() ; // Remember previous one
    vhdl_file::SetWorkLib(_owner) ;

    // VIPER #6937 : Set current analysis mode to be that of this unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;
    vhdl_file::SetAnalysisMode(GetAnalysisDialect()) ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    VhdlIdDef *save_boolean_type = BooleanType() ;
    _boolean_type = StdType("boolean") ;

    // First elaborate packages that this scope depends on
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Viper #5051 : path_name/instance_name attribute handling
    Array *old_path_name = _path_name ;
    Array *old_instance_name = _instance_name ;
    _path_name = 0 ;
    _instance_name = 0 ;

    if (_owner) PushAttributeSpecificPath(_owner->Name()) ;
    if (_id) PushAttributeSpecificPath(_id->Name()) ;

    _present_scope = old_scope ;
    // Initialize/associate Generics
    SplitAndAssociateGenerics(actual_generics, 0) ;
    //AssociateGenerics(actual_generics, 0) ;
    _present_scope = _local_scope ;

    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow decl_flow(0, 0, _local_scope) ;
    decl_flow.SetInInitial() ;

    unsigned i ;
    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        elem->Elaborate(&decl_flow) ;
    }

    // Elaborate the body
    if (body) body->StaticElaborate(0) ;

    _boolean_type = save_boolean_type ;

    // Reset the global scope
    _present_scope = old_scope ;
    vhdl_file::SetWorkLib(old_worklib) ; // Pop old one
    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
    ClearAttributeSpecificPath() ;
    _path_name = old_path_name ;
    _instance_name = old_instance_name ;
    return this ;
}

VhdlPrimaryUnit *
VhdlContextDecl::StaticElaborate(const char * /*secondary_unit_name*/, Array * /*actual_generics*/, Array * /*actual_ports*/, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef * /*actual_component*/)
{
    return this ;
}

VhdlPrimaryUnit *
VhdlContextDecl::StaticElaborateInternal(const char * /*secondary_unit_name*/, Array * /*actual_generics*/, Array * /*actual_ports*/, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef * /*actual_component*/)
{
    return this ;
}
VhdlPrimaryUnit *
VhdlPackageInstantiationDecl::StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)
{
    return StaticElaborateInternal(secondary_unit_name, actual_generics, actual_ports, block_config, actual_component) ;
}
VhdlPrimaryUnit *
VhdlPackageInstantiationDecl::StaticElaborateInternal(const char * /*secondary_unit_name*/, Array * /*actual_generics*/, Array * /*actual_ports*/, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef * /*actual_component*/)
{
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _inst_scope ;
    // Static elaboration will be same as rtl elaboration
    ElaborateInternal() ;
    // Create an empty array if generic map does not exist, otherwise we will not
    // elaborate the uninstantiated package :
    Array *actual_generics = _generic_map_aspect ? _generic_map_aspect : new Array(1) ;
    if (_elab_pkg) (void) _elab_pkg->StaticElaborateInternal(0, actual_generics, 0, 0, 0) ;
    if (!_generic_map_aspect) delete actual_generics ;
    _present_scope = old_scope ;
    return 0 ;
#if 0 // handled in ::Elaborate
    if (!_id) return 0 ;
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _inst_scope ;
    if (_inst_scope) _inst_scope->ElaborateDependencies() ;

    // Copy the _initial_pkg and populate and elaborate _elab_pkg

    // Elaboration of package instantiation declaration is elaboration of uninstantiated package
    (void) VhdlPackageDecl::StaticElaborateInternal(0, _generic_map_aspect, 0, 0, 0) ;
    _present_scope = old_scope ;
    return this ;
#endif
#if 0
    // FIXME : For the time being we will not modify the package instance
    // with instantiation specific generic map setting
    VhdlIdDef *uninstantiated_package_id = _uninst_package_name ? _uninst_package_name->GetId(): 0 ;
    VhdlPrimaryUnit *uninstantiated_package = (uninstantiated_package_id) ? uninstantiated_package_id->GetPrimaryUnit(): 0 ;

    if (!uninstantiated_package || !uninstantiated_package_id) {
        _present_scope = old_scope ;
        return 0 ;
    }
    // We need to copy the uninstantiated package with this instantiation specific generic
    // map setting.
    VhdlDesignUnit *orig_prim_unit = uninstantiated_package->GetOriginalUnit() ;

    // If this instantiation is already processed, return
    if (orig_prim_unit && uninstantiated_package->IsStaticElaborated()) {
        _present_scope = old_scope ;
        return 0 ;
    }
    VhdlMapForCopy tmp_old2new ;
    if (!orig_prim_unit) orig_prim_unit = uninstantiated_package ;
    const char *orig_name = (orig_prim_unit && orig_prim_unit->Id()) ? orig_prim_unit->Id()->OrigName(): Id()->OrigName() ;
    VhdlPrimaryUnit *new_package = orig_prim_unit->CopyPrimaryUnit(orig_name, tmp_old2new, 1) ;
    if (!new_package) {
        _present_scope = old_scope ;
        return 0 ;
    }
    VhdlScope *new_pkg_scope = new_package->LocalScope() ;
    if (new_pkg_scope) new_pkg_scope->ElaborateDependencies() ;

    Array *copied_generics = new_package->CopyInterfaceAssociationList(_inst_generic_map_aspect) ;

    new_package->AssociateGenerics(copied_generics, 0) ;

    char *new_package_name = new_package->Id()->CreateUniqueName(0) ;
    if (!VhdlTreeNode::IsLegalIdentifier(new_package_name)) {
        char *tmp = new_package_name ;
        new_package_name = VhdlTreeNode::CreateLegalIdentifier(new_package_name) ;
        Strings::free(tmp) ;
    }

    unsigned unique_required = 0 ;
    VhdlLibrary *lib = uninstantiated_package->GetOwningLib() ;
    VhdlPrimaryUnit *exist = lib ? lib->GetPrimUnit(new_package_name) : 0 ;
    if (exist && !exist->GetOriginalUnitName()) {
        exist = 0 ;
        unique_required = 1 ;
    }
    VhdlScope *container = 0 ;
    if (!lib) { // uninstantiated_package may be nested package
        VhdlScope *scope = uninstantiated_package->LocalScope() ;
        container = scope ? scope->Upper(): 0 ;
        VhdlIdDef *id = container ? container->FindSingleObject(new_package_name): 0 ;
        exist = (id  && id->IsPackage()) ? id->GetPrimaryUnit(): 0 ;
        if (exist && !exist->GetOriginalUnitName()) {
            exist = 0 ;
            unique_required = 1 ;
        }
    }
    if (unique_required) {
        char *tmp = new_package_name ;
        new_package_name = VhdlTreeNode::CreateUniqueUnitName(new_package_name, lib, container, 0) ;
        Strings::free(tmp) ;
    }
    exist = lib ? lib->GetPrimUnit(new_package_name): 0 ;
    if (!lib) {
        VhdlIdDef *id = container ? container->FindSingleObject(new_package_name): 0 ;
        exist = (id  && id->IsPackage()) ? id->GetPrimaryUnit(): 0 ;
    }
    new_package->Id()->ChangeName(new_package_name) ;

    // Create a VhdlMapForCopy containing old package id vs new
    // package id and use that to set entity reference in attribute specification
    // to copied package identifier.
    VhdlMapForCopy tmpold2new2 ;
    tmpold2new2.SetCopiedId(uninstantiated_package_id, new_package->Id()) ;
    unsigned i ;
    // Now we are changing the name of entity identifier, entity identifier can have
    // reference in Attribute specification. So there we should change the name of
    // entity reference. Do that now.
    VhdlDeclaration *decl ;
    Array *decls = new_package->GetDeclPart() ;
    FOREACH_ARRAY_ITEM(decls, i, decl) { // Iterate all declarations
        if (decl) decl->UpdateDecl(tmpold2new2) ; // Set entity references
    }
    if (exist) {
        new_package->SetOwningLib(0) ;
        delete new_package ;
        new_package = exist ;
        // if we are picking an existing package, we need to modify the generic_map_aspect also
        // delete copied one
        VhdlDiscreteRange *assoc ;
        FOREACH_ARRAY_ITEM(copied_generics, i, assoc) delete assoc ;
        delete copied_generics ;
        copied_generics = _inst_generic_map_aspect ? new_package->CopyInterfaceAssociationList(_inst_generic_map_aspect): new Array(1) ;
    } else {
        new_package->SetOriginalUnitName(uninstantiated_package_id->Name()) ;
        // Add copied one to library/declarative region
        if (lib && !lib->AddPrimUnit(new_package)) {
            new_package = 0 ;
        }
        if (!lib && container) {
            // We need to add new package in the declarative region
            // Get the container array for this nested package
            Array *container_array = uninstantiated_package->GetContainerArray() ;
            // Find the position of original uninstantiated_package and add newone just after that
            FOREACH_ARRAY_ITEM(container_array, i, decl) {
                if (decl == uninstantiated_package) {
                    container_array->InsertAfter(i, new_package) ;
                    if (new_package) new_package->SetContainerArray(container_array) ;
                    break ;
                }
            }
        }
        if (new_package) {
            tmp_old2new.SetCopiedId(uninstantiated_package_id, new_package->Id()) ;
            // Copy the package body if exist
            VhdlSecondaryUnit *body = uninstantiated_package->GetSecondaryUnit(uninstantiated_package_id->Name()) ;
            VhdlSecondaryUnit *copied_body = body ? body->CopySecondaryUnit(new_package_name, tmp_old2new): 0 ;
            if (!new_package->AddSecondaryUnit(copied_body)) copied_body = 0 ;
        }
    }
    if (new_package && _uninst_package_name) {
        VhdlIdRef *new_unit_name = new VhdlIdRef(new_package->Id()) ;
        VhdlName *new_uninstantiated_unit_name = new_unit_name ;
        new_uninstantiated_unit_name->SetLinefile(_uninst_package_name->Linefile()) ;
        VhdlName *prev_prefix = _uninst_package_name->GetPrefix() ;
        if (prev_prefix) {
            VhdlMapForCopy old2new ;
            VhdlName *new_prefix = prev_prefix->CopyName(old2new) ;
            new_uninstantiated_unit_name = new VhdlSelectedName(new_prefix, new_unit_name) ;
            new_uninstantiated_unit_name->SetLinefile(_uninst_package_name->Linefile()) ;
        }
        delete _uninst_package_name ;
        _uninst_package_name = new_uninstantiated_unit_name ;
        _id->SetUninstantiatedPackageId(new_package->Id()) ;
        (void) new_package->StaticElaborate(0, copied_generics, 0, 0, 0) ;
        if (_local_scope) _local_scope->Using(new_package->LocalScope()) ;
    }
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(copied_generics, i, assoc) delete assoc ;
    delete copied_generics ;
    FOREACH_ARRAY_ITEM(_inst_generic_map_aspect, i, assoc) delete assoc ;
    delete _inst_generic_map_aspect ;
    _inst_generic_map_aspect = 0 ;
    _present_scope = old_scope ;
    return 0 ;
#endif
}

void
VhdlEntityDecl::SplitAndAssociatePorts(Array *actual_ports, VhdlIdDef *component)
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    if ((actual_ports || component) && _port_clause) {
        Array *new_port_clause = new Array(_port_clause->Size()) ;
        // Convert declaration with array of identifiers
        // to declarations with single identifier, so that
        // different constraint can be set to each unconstrained
        // port.
        FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
            if (!decl) continue ;
            decl->Split(new_port_clause) ;
            _port_clause->Insert(i, 0) ;
        }
        delete _port_clause ;
        _port_clause = new_port_clause ;
    }
    // Set the constraints and values to the ports
    AssociatePorts(0, actual_ports, component) ;

    // Create a set of ports for which actual is provided :
    VhdlDiscreteRange *assoc ;
    VhdlIdDef *id ;
    Set port_with_actual(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(actual_ports, i, assoc) {
        id = assoc ? assoc->FindFormal(): 0 ;
        if (id) (void) port_with_actual.Insert(id->Name()) ;
    }
    if (!actual_ports && component) {
        VhdlIdDef *actual ;
        for (i=0; i<component->NumOfPorts(); i++) {
            actual = component->GetPortAt(i) ;
            if (!actual) continue ;
            id = _id->GetPort(actual->Name()) ;
            if (id) (void) port_with_actual.Insert(id->Name()) ;
        }
    }
    // Create range constraint for unconstrained ports
    unsigned j ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (!decl) continue ;
        Array *ids = decl->GetIds() ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id || !id->IsUnconstrained()) continue ;
            // If actual is present for unconstrained port, delete its initial value
            if (!port_with_actual.Get(id->Name())) continue ; // no actual, continue
            decl->SetInitAssign(0) ;
        }
        decl->CreateRangeConstraint() ;

        // VIPER #2858 : Static elaboration should not use value (initial)
        // of signal later, so set it to null
        // VIPER #4369/4374/4397 : Do not reset here
        //FOREACH_ARRAY_ITEM(ids, j, id) {  // Iterate over ids
            //if (id) id->SetValue(0) ;
        //}
    }
}
void
VhdlEntityDecl::SplitAndAssociateGenerics(Array *actual_generics, VhdlIdDef *component)
{
    // Convert declaration with array of identifiers to
    // declarations with single identifier, so that different
    // generic can obtain different initial value.
    if ((actual_generics || component) && _generic_clause) {
        unsigned i ;
        VhdlInterfaceDecl *decl ;
        Array *new_generic_clause = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
            if (!decl) continue ;
            decl->Split(new_generic_clause) ;
            _generic_clause->Insert(i, 0) ;
        }
        delete _generic_clause ;
        _generic_clause = new_generic_clause ;
    }

    // Set proper values to generic ids.
    AssociateGenerics(actual_generics, component) ;

    // VIPER #2661 : Set constraint to unconstrained generics
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        if (!decl) continue ;
        decl->CreateRangeConstraint() ;
    }

    //unsigned from_verilog = 0 ;

    // Viper 5947. from_verilog should be computed looking at all the assocs
    //VhdlExpression *generic_expr ;
    //FOREACH_ARRAY_ITEM(actual_generics, i, generic_expr) {
        //if (!generic_expr) continue ;
        //if (generic_expr->FromVerilog()) {
            //from_verilog = 1 ;
            //break ;
        //}
    //}

    // Set/override initial value of each generic
    if ((actual_generics || component) && _generic_clause) {
        FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
            if (!decl) continue ;
            if (decl->IsInterfaceTypeDecl() || decl->IsInterfaceSubprogDecl() || decl->IsInterfacePackageDecl()) continue ;
            Array *ids = decl->GetIds() ;
            VERIFIC_ASSERT(ids && ids->Size() == 1) ;
            VhdlIdDef *id = (VhdlIdDef*)ids->GetFirst() ;
            // Get the value of the generic
            VhdlValue *id_val = id->Value() ;
            // Create expression from the value
            VhdlExpression *new_expr = id_val ? id_val->CreateConstantVhdlExpressionInternal(decl->Linefile(), id->BaseType(), -1, 0) : 0 ;
            if (!new_expr) continue ;
            //unsigned is_integer_expr = 0 ;
            //if (from_verilog && new_expr->IsInteger()) is_integer_expr = 1 ;

            // Viper 5889: For integer from verilog do not call typeinfer as it cannot be inferred to vector_types of vhdl
            // this is captured by the is_integer_expr flag. It also takes into account that assoc is from Verilog
            // Vhdl operatation remains unchanged.
            // VIPER #2661 :Type infer the created initial value
            // VIPER 5886. Avoid type infer for generic types for verilog in Vhdl
            // VIPER 5889. Avoid type infer for generic types for Vhdl in Verilog if the Verilog type is integer
            // For vhdl-2008 do not call TypeInfer during elaboration as proper context
            // is not available to call scope->FindXXXX
            //if (!_is_verilog_module && !is_integer_expr) (void) new_expr->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ;
            // Set initial value to the generic
            decl->SetInitAssign(new_expr) ;
        }
    }
}
void
VhdlPackageDecl::SplitAndAssociateGenerics(Array *actual_generics, VhdlIdDef * /*component*/)
{
    // Convert declaration with array of identifiers to
    // declarations with single identifier, so that different
    // generic can obtain different initial value.
    if (_generic_clause) {
        unsigned i ;
        VhdlInterfaceDecl *decl ;
        Array *new_generic_clause = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
            if (!decl) continue ;
            decl->Split(new_generic_clause) ;
            _generic_clause->Insert(i, 0) ;
        }
        delete _generic_clause ;
        _generic_clause = new_generic_clause ;
    }

    // Set proper values to generic ids.
    AssociateGenerics(actual_generics, 0) ;

    // VIPER #2661 : Set constraint to unconstrained generics
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        if (!decl) continue ;
        decl->CreateRangeConstraint() ;
    }

    //unsigned from_verilog = 0 ;

    // Viper 5947. from_verilog should be computed looking at all the assocs
    //VhdlExpression *generic_expr ;
    //FOREACH_ARRAY_ITEM(actual_generics, i, generic_expr) {
        //if (!generic_expr) continue ;
        //if (generic_expr->FromVerilog()) {
            //from_verilog = 1 ;
            //break ;
        //}
    //}

    // Set/override initial value of each generic
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        if (!decl) continue ;
        if (decl->IsInterfaceTypeDecl() || decl->IsInterfaceSubprogDecl() || decl->IsInterfacePackageDecl()) continue ;
        Array *ids = decl->GetIds() ;
        VERIFIC_ASSERT(ids && ids->Size() == 1) ;
        VhdlIdDef *id = (VhdlIdDef*)ids->GetFirst() ;
        // Get the value of the generic
        VhdlValue *id_val = id->Value() ;
        // Create expression from the value
        VhdlExpression *new_expr = id_val ? id_val->CreateConstantVhdlExpressionInternal(decl->Linefile(), id->BaseType(), -1, 0) : 0 ;
        if (!new_expr) continue ;
        //unsigned is_integer_expr = 0 ;
        //if (from_verilog && new_expr->IsInteger()) is_integer_expr = 1 ;

        // Viper 5889: For integer from verilog do not call typeinfer as it cannot be inferred to vector_types of vhdl
        // this is captured by the is_integer_expr flag. It also takes into account that assoc is from Verilog
        // Vhdl operatation remains unchanged.
        // VIPER #2661 :Type infer the created initial value
        // VIPER 5886. Avoid type infer for generic types for verilog in Vhdl
        // VIPER 5889. Avoid type infer for generic types for Vhdl in Verilog if the Verilog type is integer
        // For vhdl-2008 do not call TypeInfer during elaboration as proper context
        // is not available to call scope->FindXXXX
        //if (!_is_verilog_module && !is_integer_expr) (void) new_expr->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ;
        // Set initial value to the generic
        decl->SetInitAssign(new_expr) ;
    }
}
void
VhdlInterfaceDecl::SetInitAssign(VhdlExpression *expr)
{
    //if (!expr) return ;
    // Delete old initial expr
    delete _init_assign ;
    // Set new expr
    _init_assign = expr ;
    unsigned i ;
    VhdlIdDef *id ;
    // Set that the interface ids has initial value
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        if (_init_assign) id->SetHasInitAssign() ;
        if (!_init_assign) id->ResetHasInitAssign() ;
    }
}

// Convert declaration with array of identifiers
// to the declarations with single identifier
void
VhdlInterfaceDecl::Split(Array *new_decl_list)
{
    if (!_id_list || (_id_list->Size() == 1)) {
        // Size of id list is one.
        new_decl_list->Insert(this) ;
        return ;
    }
    VhdlMapForCopy old2new ;
    unsigned i ;
    VhdlIdDef *id ;
    VhdlSubtypeIndication *subtype = _subtype_indication ;
    VhdlExpression *init_expr = _init_assign ;
    VhdlInterfaceDecl *new_decl = 0 ;
    unsigned first = 1 ;
    Array *id_list = _id_list ;
    FOREACH_ARRAY_ITEM(id_list, i, id) {
        if (first) {
            // Reuse this declaration.
            // Add one id to this id list
            _id_list = new Array(1) ;
            _id_list->Insert(id) ;
            new_decl_list->Insert(this) ;
            first = 0 ;
            continue ;
        }
        // Create new interface declaration with each id (excluding
        // first one) of the id list.
        VhdlSubtypeIndication *new_subtype = subtype ? subtype->CopySubtypeIndication(old2new) : 0 ;
        VhdlExpression *new_init = init_expr ? init_expr->CopyExpression(old2new) : 0 ;
        new_decl = new VhdlInterfaceDecl(_interface_kind, id, _mode, new_subtype, _signal_kind, new_init) ;
        new_decl->SetLinefile(Linefile()) ; // VIPER #4171 : Set linefile to created decl
        new_decl_list->Insert(new_decl) ;
    }
    delete id_list ;
}
void
VhdlInterfaceTypeDecl::Split(Array *new_decl_list)
{
    new_decl_list->Insert(this) ;
}
void
VhdlInterfaceSubprogDecl::Split(Array *new_decl_list)
{
    new_decl_list->Insert(this) ;
}
void
VhdlInterfacePackageDecl::Split(Array *new_decl_list)
{
    new_decl_list->Insert(this) ;
}
// This constructor is used to create new port/generic
// declaration. This simple constructor does not check
// anything. This constructor should not be used as
// general one.
VhdlInterfaceDecl::VhdlInterfaceDecl(unsigned interface_kind, const VhdlIdDef *id, unsigned mode, VhdlSubtypeIndication *subtype_indication, unsigned signal_kind, VhdlExpression *init_assign)
  : VhdlDeclaration(),
    _interface_kind(interface_kind),
    _id_list(new Array(1)),
    _mode(mode),
    _subtype_indication(subtype_indication),
    _signal_kind(signal_kind),
    _init_assign(init_assign)
{
    _id_list->Insert(id) ;
}

// Change the name of the identifier.
// In static elaboration we have to copy the entity and
// then propagate the generic values and port constraint
// there. After generic/port setting we can compute the
// unique name of that entity. So the name of the entity
// is to be change later for further processing.
void
VhdlIdDef::ChangeName(char *new_name, unsigned add_in_lib)
{
    VhdlPrimaryUnit *prim_unit = GetPrimaryUnit() ;
    // VIPER #5548: Do not return if this is not primary design unit.
    // I can be used to change name of other ids too.
    if (!prim_unit && IsDesignUnit()) return ;

    VhdlLibrary *lib = (prim_unit) ? prim_unit->GetOwningLib(): 0 ;

    if (add_in_lib && prim_unit) {
        if (lib) (void) lib->RemovePrimUnit(prim_unit) ;
        prim_unit->SetOwningLib(0) ;
    }

    VhdlScope *scope = LocalScope() ;
    if (scope) scope->Undeclare(this) ;
    Strings::free( _name ) ;
#ifdef VHDL_PRESERVE_ID_CASE
    Strings::free( _orig_name ) ;
#endif
    _name = new_name ;
#ifdef VHDL_PRESERVE_ID_CASE
    _orig_name = Strings::save(new_name) ;
    if (_name && *_name != '\\' && *_name != '\'') _name = Strings::strtolower(_name) ;
#else
    if (_name && *_name == '"') _name = Strings::strtolower(_name) ;
#endif
    if (scope) (void) scope->Declare(this) ;
    if (prim_unit && lib && add_in_lib && !lib->AddPrimUnit(prim_unit)) return ;

    // The name of entity is referred in architecture.
    // If we change the name of an entity, its references
    // should be changed
    VhdlSecondaryUnit *sec_unit ;
    Map *sec_units = prim_unit ? prim_unit->AllSecondaryUnits(): 0 ;
    MapIter mi ;
    FOREACH_MAP_ITEM(sec_units, mi, 0, &sec_unit) sec_unit->ChangeEntityReference(this) ;
}

void
VhdlPrimaryUnit::AssignUniqueName(const VhdlLibrary *lib)
{
    if (!lib) return ;
    if (_owner) (void) _owner->RemovePrimUnit(this) ;
    SetOwningLib(0) ;
    const char *old_name = Id() ? Id()->OrigName() : Name() ;
    if (old_name) {
        char *copy_name = Strings::save(old_name) ;
        char *orig_name = copy_name ;
        if (old_name[0] == '\\' && old_name[Strings::len(old_name) - 1] == '\\') {
            // remove escape characters
            orig_name[Strings::len(orig_name) - 1 ] = '\0' ;
            orig_name++ ;
        }

        // create an unique name of this primary unit
        unsigned unique_no = 0 ;
        char buffer[20] ;
        sprintf(buffer, "_c%d", ++unique_no) ;
        char *new_name = Strings::save(orig_name, buffer) ;
        if (!VhdlTreeNode::IsLegalIdentifier(new_name)) {
            // create escaped name to make the name legal
            char *tmp = new_name ;
            new_name = VhdlTreeNode::CreateLegalIdentifier(new_name) ;
            Strings::free(tmp) ;
        }
        while (lib && lib->GetPrimUnit(new_name)) {
            char *tmp = new_name ;
            sprintf(buffer, "_c%d", ++unique_no) ;
            new_name = Strings::save(orig_name, buffer) ;
            if (!VhdlTreeNode::IsLegalIdentifier(new_name)) {
                char *tmp1 = new_name ;
                new_name = VhdlTreeNode::CreateLegalIdentifier(new_name) ;
                Strings::free(tmp1) ;
            }
            Strings::free(tmp) ;
        }
        // Id absorbs argument specific name
        if (Id()) Id()->ChangeName(new_name) ;
        Strings::free(copy_name) ;
    }
}

void
VhdlArchitectureBody::StaticElaborate(VhdlBlockConfiguration *block_config)
{
    SetStaticElaborated() ;

    unsigned i ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow decl_flow(0, 0, _local_scope) ;
    decl_flow.SetInInitial() ;

    // Access Value cleanup: (VIPER #4464, 7292)
    Set *old_inner_values = VhdlNode::_access_inner_values ;
    if (!_access_allocated_values) _access_allocated_values = new Set(POINTER_HASH) ;
    VhdlNode::_access_inner_values = _access_allocated_values ;

    // Elaborate the declarations
    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (elem) elem->Elaborate(&decl_flow) ;
    }

    // VIPER 2012 : Elaborate block config one level
    if (block_config) block_config->Elaborate() ;

    // Configure this block config's component configurations.
    //if (block_config) block_config->SetLabels() ;

    // Elaborate the statements
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (!stmt) continue ;
        stmt->Elaborate(0,block_config) ;
    }
    if (Message::ErrorCount()) {
        // If error occurs, it may be the case that some component instantiation statements
        // are not elaborated yet. So label of such instantiations has back pointers to
        // binding indication. Reset that back pointer before deleting configuration spec
        Map *decl_area = _local_scope ? _local_scope->DeclArea(): 0 ;
        MapIter mi ;
        VhdlIdDef *label ;
        FOREACH_MAP_ITEM(decl_area, mi, 0, &label) {
            if (!label || !label->IsLabel()) continue ;
            VhdlStatement *label_statement = label->GetStatement() ;
            if (!label_statement) continue ; // bad label
            label->SetBinding(0) ;
        }
    }
    // Delete the component specification. Component instantiations are replaced
    // by entity instantiation in elaboration. This replacement makes the component
    // specifications use less, so delete those.
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (!elem) continue ;
        // VIPER #3661 : No need to initialize signals here, signals are already
        // initialize in VhdlSignalDecl::Elaborate.
        //elem->InitializeSignals() ;
        elem->DeleteUnusedComSpec(this) ;
    }
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (!stmt) continue ;
        stmt->DeleteUnusedComSpec() ;
    }
    if (!GetOriginalUnitName()) { // Not copied arch
        // This architecture is elaborated, i.e. its generate constructs are processed
        // and component instantiations are binded. So it there is any configuration
        // configuring this architecture, that configuration is to be invalidated.
        // Do that now by traversing used by list.
        Set *used_by = (_local_scope) ? _local_scope->GetUsedBy(): 0 ;
        SetIter si ;
        VhdlScope *scope ;
        FOREACH_SET_ITEM (used_by, si, &scope) {
            if (!scope) continue ;
            VhdlIdDef *other_design_unit = scope->GetContainingDesignUnit() ;
            if (!other_design_unit) continue ;
            if (other_design_unit->IsConfiguration()) {
                scope->NoLongerUsing(_local_scope) ;
                // Viper 5286: changed the warning to Info
                other_design_unit->Info("re-analyze unit %s since unit %s is overwritten or removed", other_design_unit->Name(), Name()) ;
                delete other_design_unit->GetDesignUnit() ;
            }
        }
    }

    // Access Value cleanup: (VIPER #4464, 7292)
    ResetAccessValueSet() ;
    VhdlNode::_access_inner_values = old_inner_values ;

    // Reset the global scope.
    _present_scope = old_scope ;
}
void
VhdlPackageBody::StaticElaborate(VhdlBlockConfiguration * /*block_config*/)
{
    SetStaticElaborated() ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow decl_flow(0, 0, _local_scope) ;
    decl_flow.SetInInitial() ;

    // Elaborate the declaration items :
    VhdlDeclaration *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        elem->Elaborate(&decl_flow) ;
    }

    // Reset the global scope
    _present_scope = old_scope ;
}

// Create unique entity depending on its proper values of generics
// and the constraint of unconstrained ports.
// Now the unique name of an entity can be generated only
// when its generics/ports are elaborated.
// To obtain the name of the entity, we copy the entity without
// copying its architectures and elaborate its generic/port list.
// Checks if such an entity is already exists in the library
// (library of 'this' entity), if so returns it.
// If such an entity is not in the library, set the unique name
// to the copied entity and put that in the same library as 'this'.
// If the binding architecture also exists, the gets duplicated too.
VhdlPrimaryUnit *
VhdlEntityDecl::CreateUniqueUnit(Array **arg_actual_generics, Array **arg_actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component, const char *arch_name, VhdlMapForCopy &old2new, const char *config_name)
{
    // VIPER #2912 : Arguments 'arg_actual_generics' and 'arg_actual_ports' can
    // contain valid arrays which are intended to be replaced by copied arrays.
    // But if some error occurs, content of these arguments will not be replaced
    // and will cause corruption if the arrays passed to this function along
    // with returned arrays (through arguments) are freed. So clear the content
    // of argument specified arrays and only fill with copied version.
    Array *actual_generics = 0 ;
    Array *actual_ports = 0 ;
    if (arg_actual_generics && *arg_actual_generics) {
        actual_generics = *arg_actual_generics ;
        *arg_actual_generics = 0 ;
    }
    if (arg_actual_ports && *arg_actual_ports) {
        actual_ports = *arg_actual_ports ;
        *arg_actual_ports = 0 ;
    }

    if (!_id) return 0 ;

    if (Message::ErrorCount()) return 0 ;
    // Get the library, owner of the entity
    const char *lib_name = _id->GetContainingLibraryName() ;
    if (!lib_name) lib_name = vhdl_file::GetWorkLib()->Name() ;
    VhdlLibrary *lib = vhdl_file::GetLibrary(lib_name) ;

    VhdlPrimaryUnit *new_entity_wo_arch = 0 ;
    char *entity_name = 0 ;
    (void) entity_name ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    VhdlIdDef *unit = Id() ;
    VeriModule *verilog_module = 0 ;
    // Viper 7053: Here we should treat work as library named work so pass from_lib_options
    if (_is_verilog_module && Id()) verilog_module = vhdl_file::GetVerilogModuleFromlib(unit->GetContainingLibraryName(), Name(), 1/*from loption*/) ;
#endif

    if (RuntimeFlags::GetVar("vhdl_uniquify_all_instances")) {
        // Copy the instantiated entity architecture pair with a unique name

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
        if (verilog_module) {
            verilog_module = verilog_module->CreateUniqueUnit(0, _present_scope, 0) ;
            if (!verilog_module) return 0 ; // verilog_module is a black box Viper 5022
            entity_name = vhdl_file::CreateVhdlEntityName(verilog_module->GetName()) ;
        }
#endif
    
#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
        // VIPER 2436 : If we are now creating entity for component instantion and want to
        // preserve component instantiation, we need to create an unique component
        // So, examine container of existing component scope during unique name
        // creation along with library
        // Get enclosing scope of 'actual_component'

        VhdlScope *component_scope = actual_component ? actual_component->LocalScope() : 0 ;
        // Get enclosing scope
        VhdlScope *upper_scope = component_scope ? component_scope->Upper() : 0 ;
        VhdlIdDef *container_id = (upper_scope) ? upper_scope->GetOwner() : 0 ; // Get owner of containing scope
        if (container_id && container_id->IsPackage()) {
            // VIPER #2788 :Instantiated component is declared in a package. We will not add any new
            // component in package as this package can also be included in other instantiated
            // entity and this scenario can hamper our save/restore flow. So we will add
            // new component in enclosing architecture.
            VhdlIdDef *arch_id = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ; // Get enclosing architecture
            upper_scope = (arch_id) ? arch_id->LocalScope() : 0 ; // Get scope of architecture
        }
        // Get an unique name of the entity
        char *new_entity_name = entity_name ? entity_name : VhdlTreeNode::CreateUniqueUnitName(_id->OrigName(), lib, upper_scope, 0) ;
#else
        (void) actual_component ; // Prevent "unused" compile warning
        // Get an unique name of the entity
        char *new_entity_name = entity_name ? entity_name : VhdlTreeNode::CreateUniqueUnitName(_id->OrigName(), lib, 0, 0) ;
#endif // VHDL_PRESERVE_COMPONENT_INSTANCE
        // VIPER #7508: If this entity is using a verilog package and static elaboration
        // has copied that package, copied entity should use the converted vhdl package of
        // copied verilog package
        ProcessedUsedPackage(old2new) ;
        // Copy entity architecture pair
        new_entity_wo_arch = CopyEntityArchitecture(new_entity_name, arch_name, old2new) ;
        Strings::free(new_entity_name) ; // Free allocated name

        // Add created entity architecture pair to library
        if (lib && !lib->AddPrimUnit(new_entity_wo_arch)) {
            return 0 ;  // Cannot add copied entity to library, return
        }
        // Copy the actual generics and ports so that formal part now points to copied entity
        if (arg_actual_generics && new_entity_wo_arch) *arg_actual_generics = new_entity_wo_arch->CopyInterfaceAssociationList(actual_generics) ;
        if (arg_actual_ports && new_entity_wo_arch) *arg_actual_ports = new_entity_wo_arch->CopyInterfaceAssociationList(actual_ports) ;

    } else { // vhdl_uniquify_all_instances

        // Copy the entity to be instantiated. If the copied entity is already existed in
        // the library, copied one should be deleted. To minimise this redundant copy, we copy
        // only the entity.
        // We cannot avoid the entity copy, as to obtain the proper name we have to elaborate its generic
        // and port list and if we do it on the original one, the generic/port setting of the original
        // one will be lost.
        VhdlMapForCopy tmp_old2new ;
        // VIPER #7508: If this entity is using a verilog package and static elaboration
        // has copied that package, copied entity should use the converted vhdl package of
        // copied verilog package
        ProcessedUsedPackage(tmp_old2new) ;
        VhdlDesignUnit *orig_prim_unit = GetOriginalUnit() ;
        const char *orig_name = (orig_prim_unit && orig_prim_unit->Id()) ? orig_prim_unit->Id()->OrigName(): Id()->OrigName() ;
        new_entity_wo_arch = new VhdlEntityDecl(orig_name, (*this), tmp_old2new, 1) ;
        if (new_entity_wo_arch->LocalScope()) new_entity_wo_arch->LocalScope()->ElaborateDependencies() ;

        Array *copied_generics = 0 ;
        Array *copied_ports = 0 ;

        // VIPER 1863 & 1472 : begin
        // Create generic and port list properly
        if ((actual_generics || actual_ports) && actual_component) {
            // Actuals are from binding use normal copy
            copied_generics = tmp_old2new.CopyDiscreteRangeArray(actual_generics) ;
            copied_ports = tmp_old2new.CopyDiscreteRangeArray(actual_ports) ;
        } else {
            // Copy actual ports and generics so that formal parts contain copied iddefs and
            // actual parts contain original iddefs
            copied_generics = new_entity_wo_arch->CopyInterfaceAssociationList(actual_generics) ;
            copied_ports = new_entity_wo_arch->CopyInterfaceAssociationList(actual_ports) ;
        }
        // VIPER 1863 & 1472 : end
        // Provide values to generic ids
        new_entity_wo_arch->AssociateGenerics(copied_generics, actual_component) ;

        // Provide proper constraint to unconstraint ports
        new_entity_wo_arch->AssociatePorts(0, copied_ports, actual_component) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
        if (verilog_module) {
            Map parameters(STRING_HASH) ;
            new_entity_wo_arch->GetParameterFromGeneric(parameters, 0, 0, 0) ;
            Map *actual_params = 0 ;
            actual_params = verilog_module->CreateActualParameters(&parameters) ;
            // Viper 4625. The vhdl scope is passed to CreateUniqueUnit so that the generated name is
            // unique also in vhdl scope
            verilog_module = verilog_module->CreateUniqueUnit(actual_params, _present_scope, _local_scope) ;
            MapIter mi ;
            char *image ;
            FOREACH_MAP_ITEM(&parameters, mi, 0, &image) Strings::free(image) ;
            VeriExpression *actual ;
            char *name ;
            FOREACH_MAP_ITEM(actual_params, mi, &name, &actual) {
                Strings::free(name) ;
                delete actual ;
            }
            delete actual_params ;

            if (!verilog_module) {
                new_entity_wo_arch->SetOwningLib(0) ;
                delete new_entity_wo_arch ;
                unsigned i ;
                VhdlDiscreteRange *assoc ;
                FOREACH_ARRAY_ITEM(copied_generics, i, assoc) delete assoc ;
                FOREACH_ARRAY_ITEM(copied_ports, i, assoc) delete assoc ;
                delete copied_ports ;
                delete copied_generics ;
                return 0 ; // verilog_module is a black box Viper 5022
            }
            entity_name = vhdl_file::CreateVhdlEntityName(verilog_module->GetName()) ;
        }
#endif

        if (Message::ErrorCount()) {
            new_entity_wo_arch->SetOwningLib(0) ;
            delete new_entity_wo_arch ;
            unsigned i ;
            VhdlDiscreteRange *assoc ;
            FOREACH_ARRAY_ITEM(copied_generics, i, assoc) delete assoc ;
            FOREACH_ARRAY_ITEM(copied_ports, i, assoc) delete assoc ;
            delete copied_ports ;
            delete copied_generics ;
            Strings::free(entity_name) ;
            return 0 ;
        }
        // Now that the generics and ports are associated, the generics
        // are set and that the ports are constrained.
        // Create the unique name for this configuration.
        // For top-level units, (no actual anything), use the simple entity.
        char *new_entity_name ;
        if (copied_generics || block_config || actual_component || HasUnconstrainedPorts()) {
            // VIPER #6032 : Consider block configuration in signature creation
            new_entity_name = new_entity_wo_arch->Id()->CreateUniqueName((block_config && !config_name)? block_config: 0) ;
            if (!VhdlTreeNode::IsLegalIdentifier(new_entity_name)) {
                // Create escaped name to make the name of copied entity legal
                char *tmp = new_entity_name ;
                new_entity_name = VhdlTreeNode::CreateLegalIdentifier(new_entity_name) ;
                Strings::free(tmp) ;
            }
        } else {
            new_entity_name = Strings::save(new_entity_wo_arch->Id()->OrigName()) ;
        }
        if (!new_entity_name) {
            Strings::free(entity_name) ;
            delete new_entity_wo_arch ;
            return this ;
        }
        // Find the architecture by name.
        // If name is not there, take the last one compiled
        VhdlSecondaryUnit *architecture = GetSecondaryUnit(arch_name) ;
        if (!arch_name && architecture) arch_name = architecture->Id()->OrigName() ;

#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
        // VIPER 2436 : If we are now creating entity for component instantion and want to
        // preserve component instantiation, check if container of existing
        // component contains a component declaration named 'new_entity_name'.
        // If yes and existing component is not created from elaboration, we need
        // to create an unique name, so that we can copy instantiating component
        // with a different name
        // Get enclosing scope of 'actual_component'
        VhdlScope *comp_scope = actual_component ? actual_component->LocalScope(): 0 ;
        // Get enclosing scope
        VhdlScope *upper_scope = comp_scope ? comp_scope->Upper() : 0 ;
        VhdlIdDef *container_id = (upper_scope) ? upper_scope->GetOwner() : 0 ; // Get owner of containing scope
        if (container_id && container_id->IsPackage()) {
            // VIPER #2788 :Instantiated component is declared in a package. We will not add any new
            // component in package as this package can also be included in other instantiated
            // entity and this scenario can hamper our save/restore flow. So we will add
            // new component in enclosing architecture.
            VhdlIdDef *arch_id = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ; // Get enclosing architecture
            upper_scope = (arch_id) ? arch_id->LocalScope() : 0 ; // Get scope of architecture
        }
#endif // VHDL_PRESERVE_COMPONENT_INSTANCE
        unsigned unique_required = 0 ; // Flag to indicate whether we need unique name

        unsigned ignore_copied = 1 ; // Flag to indicate whether we should ignore copied units while creating unique name
        char *path_name = (_affected_by_path_name) ? PathNameAttribute(_id, 0, 0) : 0 ;
        if (_affected_by_path_name && path_name) {
            char *tmp = new_entity_name ;
            new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(GetOriginalUnitName() ? GetOriginalUnitName(): _id->OrigName(), path_name) ;
            Strings::free(tmp) ;
            Strings::free(path_name) ;
        } else if (Strings::compare_nocase(new_entity_wo_arch->Id()->OrigName(), new_entity_name) && !block_config) {
            // No configuration, use <orig_name>_default entity
            // To create new name by appending '_default', check that if the prefix is an
            // escaped name. If so remove escape characters and create new name
            if (!_is_verilog_module) {
               Strings::free(new_entity_name) ;
               new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(orig_name, "_default") ;
            }
        } else if (block_config && config_name) { // configured by configuration 'config_name'
            // Name of copied entity will be <orig_name>_config_name
            char *config_name_with_prefix = Strings::save("_", config_name) ;
            char *tmp = new_entity_name ;
            new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(tmp, config_name_with_prefix) ;
            Strings::free(config_name_with_prefix) ;
            Strings::free(tmp) ;
        }

        // Check if any entity with computed name is already
        // in the library
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
        if (entity_name) {
            Strings::free(new_entity_name) ;
            new_entity_name = entity_name ;
        }
#endif
        VhdlPrimaryUnit *orig_punit = lib->GetPrimUnit(new_entity_name) ;
        // If existing entity is not created by elaboration(copied one), it
        // is written in design, i.e. original design already contains an
        // entity 'new_entity_name'. We need to create an unique name
        // Viper 6122 : Do not check for converted entities. they have _original_unit_name = 0
        if (orig_punit && !orig_punit->GetOriginalUnitName() && !orig_punit->IsVerilogModule()) {
            orig_punit = 0 ; // Clear orig unit pointer
            unique_required = 1 ;
        }
#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
        /* Moved upwards as 'upper_scope' is required above
        // VIPER 2436 : If we are now creating entity for component instantion and want to
        // preserve component instantiation, check if container of existing
        // component contains a component declaration named 'new_entity_name'.
        // If yes and existing component is not created from elaboration, we need
        // to create an unique name, so that we can copy instantiating component
        // with a different name
        // Get enclosing scope of 'actual_component'
        VhdlScope *comp_scope = actual_component ? actual_component->LocalScope(): 0 ;
        // Get enclosing scope
        VhdlScope *upper_scope = comp_scope ? comp_scope->Upper() : 0 ;
        VhdlIdDef *container_id = (upper_scope) ? upper_scope->GetOwner() : 0 ; // Get owner of containing scope
        if (container_id && container_id->IsPackage()) {
            // VIPER #2788 :Instantiated component is declared in a package. We will not add any new
            // component in package as this package can also be included in other instantiated
            // entity and this scenario can hamper our save/restore flow. So we will add
            // new component in enclosing architecture.
            VhdlIdDef *arch_id = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ; // Get enclosing architecture
            upper_scope = (arch_id) ? arch_id->LocalScope() : 0 ; // Get scope of architecture
        }
        */
        if (actual_component) { // component instantiation is processed here
            VhdlIdDef *exist = (upper_scope) ? upper_scope->FindSingleObjectLocal(new_entity_name): 0 ;
            VhdlComponentDecl *existing_decl = exist ? exist->GetComponentDecl() : 0 ;
            // Viper 6589: For component for verilog module do not uniqify it like the entity.
            if ((existing_decl && !existing_decl->GetOriginalComponentName() && !_is_verilog_module) || (exist && !exist->IsComponent())){
                unique_required = 1 ;
            }
        }
        // If entity of this name exist, but that does not contain architecture
        // 'arch_name' then we have to create unique name. Entity with multiple
        // architectures cannot be represented by a single component
        if (!_is_verilog_module && orig_punit && !orig_punit->GetSecondaryUnit(arch_name)) unique_required  = 1 ;
#endif // VHDL_PRESERVE_COMPONENT_INSTANCE
        if (unique_required) { // Create an unique name
            char *tmp = new_entity_name ;
#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
            // VIPER 2436 : Consider container scope of instantiated component to
            // preserve component instantiation. Elaborated unique entity name should
            // not conflict with the identifiers of the scope where instantiated
            // component is declared
            new_entity_name = VhdlTreeNode::CreateUniqueUnitName(new_entity_name, lib, upper_scope, ignore_copied) ;
#else
            new_entity_name = VhdlTreeNode::CreateUniqueUnitName(new_entity_name, lib, 0, ignore_copied) ;
#endif // VHDL_PRESERVE_COMPONENT_INSTANCE
            Strings::free(tmp) ;
        }
        // We have ignored copied entities in unique name creation. Check
        // if copied entity of name 'new_entity_name' is exists. If so,
        // return that one.
        orig_punit = lib->GetPrimUnit(new_entity_name) ;

        // Change the name of the copied entity
        new_entity_wo_arch->Id()->ChangeName(new_entity_name) ;

        // VIPER #3192 : Create a VhdlMapForCopy containing old entity id vs new
        // entity id and use that to set entity reference in attribute specification
        // to copied entity identifier.
        VhdlMapForCopy tmpold2new ;
        tmpold2new.SetCopiedId(_id, new_entity_wo_arch->Id()) ;
        unsigned i ;
        // Now we are changing the name of entity identifier, entity identifier can have
        // reference in Attribute specification. So there we should change the name of
        // entity reference. Do that now.
        VhdlDeclaration *decl ;
        Array *decls = new_entity_wo_arch->GetDeclPart() ;
        FOREACH_ARRAY_ITEM(decls, i, decl) { // Iterate all declarations
            if (decl) decl->UpdateDecl(tmpold2new) ; // Set entity references
        }
        if (orig_punit) {
            // Entity exits, it comes directly from configuration or no configuration
            // exists to differentiate its architecture.
            // Copied one is useless.  Delete the copied one.
            new_entity_wo_arch->SetOwningLib(0) ;
            delete new_entity_wo_arch ;
            // Return the original entity
            new_entity_wo_arch = orig_punit ;
            if (architecture && !new_entity_wo_arch->GetSecondaryUnit(arch_name)) {
                // Original entity does not have architecture 'arch_name', so
                // we have to copy the architecture. But architecture can use ports/generics
                // of entity, so in copied architecture those references should point to
                // iddefs of 'new_entity_wo_arch' not 'this'
                old2new.MakeAssociations(this, new_entity_wo_arch) ;
            }
            // No need of copied association list, delete those
            VhdlDiscreteRange *assoc ;
            FOREACH_ARRAY_ITEM(copied_generics, i, assoc) delete assoc ;
            FOREACH_ARRAY_ITEM(copied_ports, i, assoc) delete assoc ;
            delete copied_ports ;
            delete copied_generics ;
            // VIPER #2877 : 'orig_punit' may be other than 'this'. So copy the association lists, so that
            // formal part of each association points to entity 'orig_punit'
            if (arg_actual_generics) *arg_actual_generics = orig_punit->CopyInterfaceAssociationList(actual_generics) ;
            if (arg_actual_ports) *arg_actual_ports = orig_punit->CopyInterfaceAssociationList(actual_ports) ;
        } else {
            // Copied entity is required for uniquify entities
            tmp_old2new.MoveAssociations(old2new) ;
            // Add the copied one to the library
            if (!lib->AddPrimUnit(new_entity_wo_arch)) {
                new_entity_wo_arch = 0 ;
            }
            // Set copied generic and port list to argument specific generic/port list
            if (arg_actual_generics) *arg_actual_generics = copied_generics ;
            if (arg_actual_ports) *arg_actual_ports = copied_ports ;
        }

        if (architecture && new_entity_wo_arch && !new_entity_wo_arch->GetSecondaryUnit(arch_name)) {
            // VIPER #7508: If this entity is using a verilog package and static elaboration
            // has copied that package, copied entity should use the converted vhdl package of
            // copied verilog package
            architecture->ProcessedUsedPackage(old2new) ;
            // Copy the architecture to be instantiated
            VhdlSecondaryUnit *sec_unit = architecture->CopySecondaryUnit(0, old2new) ;
            if (new_entity_wo_arch->AddSecondaryUnit(sec_unit)) {
                sec_unit->Id()->DeclareArchitectureId(new_entity_wo_arch->Id()) ;
                // Change the entity name referred by this architecture
                sec_unit->ChangeEntityReference(new_entity_wo_arch->Id()) ;
                // VIPER #2998 : Create a VhdlMapForCopy containing old entity id vs new
                // entity id and use that to set entity reference in attribute specification
                // to copied entity identifier.
                VhdlMapForCopy tmpold2new2 ;
                tmpold2new2.SetCopiedId(_id, new_entity_wo_arch->Id()) ;
                // Attribute specification of architecture can have entity references.
                // We have copied architecture after removing entity id from 'old2new',
                // so any reference to entity is pointing to original entity not the
                // copied one. But in attribute specification entity reference should
                // point to copied entity. Do that now :
                decls = sec_unit->GetDeclPart() ;
                FOREACH_ARRAY_ITEM(decls, i, decl) { // Iterate all declarations
                    if (decl) decl->UpdateDecl(tmpold2new2) ; // Set entity references
                }
            }
        }
    } // vhdl_uniquify_all_instances

    // Return the copied entity
    return new_entity_wo_arch ;
}

// Create unique entity from the entity referred by this component instantiation.
// Elaborate the unique entity.
// Convert 'this' component instantiation to unique entity instantiation.
void
VhdlComponentInstantiationStatement::StaticElaborate(Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component, const char *arch_name, VhdlPrimaryUnit *actual_master)
{
    if (Message::ErrorCount()) return ; // Error, no need to proceed further

    // VIPER #6793 : If any instantiated unit is top level unit, produce
    // warning. Elaboration may produce incorrect result if self recursion exists
    if (actual_master && (actual_master == vhdl_file::_top_unit)) {
        Warning("top level unit is instantiated, use VHDL_COPY_TOP_BEFORE_STATIC_ELAB compile flag") ;
    }

    // VIPER #3576 : Check if we need to compile this entity as a black-box or not :
    if (actual_master && !actual_master->GetCompileAsBlackbox()) {
        if (vhdl_file::CheckCompileAsBlackbox(actual_master->Id()->Name())) actual_master->SetCompileAsBlackbox() ; // call user-registered call-back method
    }
    if (actual_master && actual_master->GetCompileAsBlackbox()) return ;

    unsigned is_master_copied = 0 ;
    Array *copied_generics = actual_generics ;
    Array *copied_ports = actual_ports ;
    VhdlBlockConfiguration *copied_blk_config = block_config ;
    VhdlBlockConfiguration *allocated_blk_config = 0 ;

    // Path name stack handling : push the instance label along with the instantiated
    // entity/architecture in the path stack :
    char *path_name = 0 ;
    if (actual_master) {
        VhdlIdDef *unit_id = actual_master->Id() ;
        if (unit_id && unit_id->IsConfiguration()) unit_id = unit_id->GetEntity() ;
        VhdlPrimaryUnit *p_unit = (unit_id) ? unit_id->GetPrimaryUnit(): 0 ;
        VhdlSecondaryUnit *s_unit = (p_unit) ? p_unit->GetSecondaryUnit(arch_name): 0 ;
        PushAttributeSpecificPath(_label ? _label->Name(): 0, (p_unit && p_unit->GetOriginalUnitName()) ? p_unit->GetOriginalUnitName(): (p_unit ? p_unit->Name():0), (s_unit && s_unit->GetOriginalUnitName()) ? s_unit->GetOriginalUnitName(): (s_unit ? s_unit->Name():0)) ;
        path_name = PathNameAttribute(unit_id, 0, 0) ;
    }

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #5624 : Set instance name from label, so that pseudo node for instantiated unit
    // can be created with proper instance name :
    VeriStaticElaborator *elab_obj = veri_file::GetStaticElabObj() ;
    if (elab_obj) elab_obj->SetVhdlInstanceName(_label ? _label->OrigName(): 0) ;
#endif

    VhdlMapForCopy old2new ;
    // Copy the instantiated entity architecture pair for different
    // generic values to create unique entity.
    if (actual_master && actual_master->Id()->IsEntity()) {
        is_master_copied = 1 ;
        // Get unique entity.
        actual_master = actual_master->CreateUniqueUnit(&copied_generics, &copied_ports, block_config, actual_component, arch_name, old2new, 0) ;
        if (!actual_master) {
            Strings::free(path_name) ;
            PopAttributeSpecificPath() ;
            return ;
        }

        // When the entity is copied, we have to copy the
        // 'actual_generics', 'actual_ports' and 'block_config'.
        // The above arguments can come from configuration.
        // A back pointer to the scope to be configured is
        // existed in block configuration and that scope is
        // used to set proper binding information to the
        // component instantiations.
        // For example pointer to 'architecture-scope'
        // can be existed in block configuration.
        // Now we copy the architecture to be configured.
        // So the back pointer (arch-scope) in the configuration
        // should now point to the copied-architecture-scope. As
        // the labels of the component instantiations inside the
        // architecture are also copied now, original block
        // configuration cannot provide proper binding information
        // to the copied component instantiations.
        // Again port map/generic map of configuration can have
        // back pointers to the generics/ports of original entity.
        // So we copy these arguments with the same 'map' (used
        // for entity copy) so that back pointers now point to
        // the copied design unit. Generic and port list
        // are copied within 'CreateUniqueUnit'.
        copied_blk_config = block_config ? new VhdlBlockConfiguration(*block_config, old2new) : 0 ;
        allocated_blk_config = copied_blk_config ;
    }

    // Elaborate the instantiated design unit
    if (actual_master && actual_master->IsVerilogModule()) Info("going to verilog side to elaborate module %s", _unit->OrigName()) ;
    unsigned is_affected_by_path_name_set = actual_master ? actual_master->IsAffectedByPathName(): 0 ;
    if (actual_master && !VhdlNode::PushStructuralStack(actual_master->Id(), actual_master->Id())) {
        delete actual_master ;
        actual_master = 0 ;
    }
    // VIPER #5203 : Produce error for illegal recursive instantiation :
    if (actual_master && VhdlNode::IsPresentInStructuralStack(actual_master->Name(), arch_name)) {
        VhdlSecondaryUnit *arch = actual_master->GetSecondaryUnit(arch_name) ;
        if (arch && arch->IsStaticElaborated()) {
            Error("illegal recursive instantiation of %s(%s)", actual_master->Name(), arch->Name()) ;
        }
    }

    actual_master = actual_master ? actual_master->StaticElaborateInternal(arch_name, copied_generics, copied_ports, copied_blk_config, actual_component) : 0 ;
    if (actual_master && actual_master->IsVerilogModule()) Info("back to vhdl to continue elaboration") ;

    delete allocated_blk_config ;
    unsigned i ;
    // Return if there is no actual master
    if (!actual_master) {
        if (is_master_copied) {
            VhdlDiscreteRange *ele ;
            FOREACH_ARRAY_ITEM(copied_generics, i, ele) delete ele ;
            delete copied_generics ;
            FOREACH_ARRAY_ITEM(copied_ports, i, ele) delete ele ;
            delete copied_ports ;
        }
        Strings::free(path_name) ;
        PopAttributeSpecificPath() ;
        return ;
    }
    // If instantiated entity uses path_name attribute, propagate the information upwards to instantiating entity :
    if (actual_master->IsAffectedByPathName()) {
        PropagateAffectOfPathName() ;

        // Before elaboration affected_by_path_name was not set, but now it is set.
        // This means this entity is elaborated for the first time. So change its name
        // to reflect that it is modified for path_name/instance_name attribute
        // VIPER #5805 : If instantiated unit is verilog, do not change the name
        // of instantiated unit as it is already modified
        if (!is_affected_by_path_name_set && !Message::ErrorCount() && !actual_master->IsVerilogModule()) {
            VhdlIdDef *unit_id = actual_master->Id() ;
            if (unit_id) {
                char *new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(actual_master->GetOriginalUnitName() ? actual_master->GetOriginalUnitName(): unit_id->Name(), path_name) ;
                unit_id->ChangeName(new_entity_name, 1) ;
            }
        }
    }
    Strings::free(path_name) ;

    // This instantiation is originally proper entity instantiation.
    if (_unit && _unit == actual_master->Id()) {
        // VIPER #7230 : Instantiated unit is not changed, but positional
        // association should be converted to named association
        CreatePortAssociation(copied_ports, actual_component, actual_component ? actual_master->Id(): 0) ;
        VhdlDiscreteRange *ele ;
        FOREACH_ARRAY_ITEM(copied_generics, i, ele) delete ele ;
        delete copied_generics ;
        FOREACH_ARRAY_ITEM(copied_ports, i, ele) delete ele ;
        delete copied_ports ;
        PopAttributeSpecificPath() ;
        VhdlNode::PopStructuralStack() ;
        return ;
    }

    // VIPER #6467 : Associated instantiated unit's library name, entity name and architecture name :
    const char *lib_name = actual_master->Id()->GetContainingLibraryName() ;
    Strings::free(_library_name) ;
    _library_name = Strings::save(lib_name) ;

    Strings::free(_instantiated_unit_name) ;
    VhdlIdDef *actual_master_id = actual_master->Id() ;
    _instantiated_unit_name = actual_master_id ? Strings::save(actual_master_id->Name()) : 0 ;

    // Get the architecture to be instantiated
    VhdlSecondaryUnit *architecture = actual_master->GetSecondaryUnit(arch_name) ;
    VhdlIdDef *arch_id = architecture ? architecture->Id(): 0 ;
    Strings::free(_arch_name) ;
    _arch_name = arch_id ? Strings::save(arch_id->Name()): 0 ;

#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
    // VIPER 2436 : Now create component instantiation if it is originally component
    // instantiation
    if (actual_component) { // It is component instantiation
        // Get enclosing scope of 'actual_component'
        VhdlScope *comp_scope = actual_component->LocalScope() ;
        // Get enclosing scope (block scope/arch scope/package scope)
        VhdlScope *upper_scope = comp_scope ? comp_scope->Upper(): 0 ;
        VhdlIdDef *container_id = (upper_scope) ? upper_scope->GetOwner() : 0 ; // Get owner of containing scope
        if (container_id && container_id->IsPackage()) {
            // VIPER #2788 :Instantiated component is declared in a package. We will not add any new
            // component in package as this package can also be included in other instantiated
            // entity and this scenario can hamper our save/restore flow. So we will add
            // new component in enclosing architecture.
            VhdlIdDef *tmp_arch_id = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ; // Get enclosing architecture
            upper_scope = (tmp_arch_id) ? tmp_arch_id->LocalScope() : 0 ; // Get scope of architecture
        }
        // first check if component with this name is created
        VhdlIdDef *instantiated_comp = (upper_scope) ? upper_scope->FindSingleObjectLocal(actual_master->Name()) : 0 ;
        _present_scope->Using(actual_master->LocalScope()) ;

        if (!instantiated_comp) { // Component not defined, create one
            // Create a component for entity 'actual_master'. The upper scope of
            // created component will be 'upper_scope'
            instantiated_comp = actual_master->CreateComponent(upper_scope /* containing scope*/, actual_component /* actual component */, copied_generics, copied_ports) ;
        }
        // Change the _instantiated_unit, make it simple component reference
        if (_instantiated_unit) {
            linefile_type line_file = _instantiated_unit->Linefile() ;
            VhdlIdRef *component_name = new VhdlIdRef(instantiated_comp) ;
            component_name->SetLinefile(line_file) ;
            VhdlName *new_instantiated_unit = 0 ;
            if (_instantiated_unit->GetEntityClass()) {
                // If this instantiated unit contains 'component' keyword
                // replicate that in created name
                new_instantiated_unit = new VhdlInstantiatedUnit(VHDL_component, component_name) ; // Create new one
            } else {
                // No 'component' keyword, do not introduce that in created name
                new_instantiated_unit = component_name ;
            }
            delete _instantiated_unit ; // Delete old one
            _instantiated_unit = new_instantiated_unit ;
            _instantiated_unit->SetLinefile(line_file) ;
        }
        _unit = instantiated_comp ; // Set new component id at _unit
    } else {
#endif
        // Now create entity instantiation from component instantiation
        char *lib_name_to_use = 0 ;

        VhdlIdDef *existing_lib_id = _present_scope ? _present_scope->FindSingleObject(lib_name) : 0 ;
        if (existing_lib_id && !existing_lib_id->IsLibrary()) {
            // VIPER #7143: The library name can be current working library and
            // for that case we can use 'work' instead of proper library name to
            // remove conflict
            VhdlLibrary *work_lib = vhdl_file::GetWorkLib() ;
            if (work_lib && Strings::compare_nocase(work_lib->Name(), lib_name)) {
                existing_lib_id = _present_scope ? _present_scope->FindSingleObject("work"): 0 ;
                // Direct entity instance should be created using work.<entity_name>
                lib_name_to_use = Strings::save("work") ;
            } else {
                Error("library name %s of instantiated unit conflicts with visible identifier", lib_name) ;
                existing_lib_id = 0 ;
            }
        }
        // Create library id for library name not in using clause of this design unit.
        // The created id is inserted in architecture's scope (_present_scope here).
        // This creats a memory leak in VhdlScope.
        // This library id is created so that we can create instantiated unit like
        //   my_lib.entity_name(arch_name) for this instantiation.
        // Library 'my_lib' is not specified as library clause/use clause before this
        // design unit, so constructor of VhdlInstantiatedUnit gives error if we
        // do not create this library id.
        VhdlLibraryId *lib_id = new VhdlLibraryId(lib_name_to_use ? lib_name_to_use: Strings::save(lib_name)) ;
        if (existing_lib_id) {
            // VIPER #2866 : Declare the library identifier forcefully in the scope
            // as predefined operator
            if (_present_scope) { _present_scope->DeclarePredefined(lib_id) ; }
        } else {
            // VIPER #7059 : After creating library clause, use 'Declare' not 'DeclarePredefined'
            if (_present_scope) { (void) _present_scope->Declare(lib_id) ; }

            // VIPER #7059 : Create a library clause and add that in context clause of
            // containing unit

            VhdlIdDef *containing_unit_id = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ; // Get enclosing architecture
            Array *lib_list = new Array(1) ; lib_list->InsertLast(lib_id) ;
            VhdlLibraryClause *lib_clause = new VhdlLibraryClause(lib_list, 0) ;
            VhdlDesignUnit *du = containing_unit_id ? containing_unit_id->GetDesignUnit(): 0 ;
            if (du) {
                du->AddContextClause(lib_clause) ;
            } else {
                delete lib_clause ; lib_clause = 0 ;
            }
        }

        // Create instantiated unit.
        // Instantiated unit will be entity
        linefile_type line_file = _instantiated_unit ? _instantiated_unit->Linefile() : 0 ;
        VhdlIdRef *arch = 0 ;

        // Create VhdlIdRef from VhdlIdDef Viper 4927
        //if (architecture) arch = new VhdlIdRef(Strings::save(architecture->Id()? architecture->Id()->OrigName() : "")) ;
        if (architecture) {
            arch = new VhdlIdRef(architecture->Id()) ;
            arch->SetLinefile(line_file) ;
        }

        Array *assoc_list = new Array(1) ;
        assoc_list->Insert(arch) ;

        // Create VhdlIdRef from VhdlIdDef Viper 4927
        //VhdlIdRef *lib_name_node = new VhdlIdRef(Strings::save(lib_name)) ;
        VhdlIdRef *lib_name_node = new VhdlIdRef(lib_id) ;
        lib_name_node->SetLinefile(line_file) ;

        // Create VhdlIdRef from VhdlIdDef Viper 4927
        //VhdlIdRef *entity_name_node = new VhdlIdRef(Strings::save(actual_master->Id() ? actual_master->Id()->OrigName() : "")) ;
        VhdlIdRef *entity_name_node = new VhdlIdRef(actual_master->Id()) ;
        entity_name_node->SetLinefile(line_file) ;

        VhdlSelectedName *entity_spec = new VhdlSelectedName(lib_name_node, entity_name_node) ;
        entity_spec->SetId(actual_master->Id()) ;
        entity_spec->SetLinefile(line_file) ;

        VhdlName *ent_name = entity_spec ;
        if (architecture) {
             VhdlIndexedName *entity_name = new VhdlIndexedName(entity_spec, assoc_list) ;
             entity_name->SetLinefile(line_file) ;
             ent_name = entity_name ;
        } else {
            delete assoc_list ; // otherwise we leak the Array
        }

        VhdlInstantiatedUnit *new_instantiated_unit = new VhdlInstantiatedUnit(VHDL_entity, ent_name) ;
        new_instantiated_unit->SetLinefile(line_file) ;

        delete _instantiated_unit ;
        _instantiated_unit = new_instantiated_unit ;
        // VIPER #2786 : Add new instantiated unit in the dependency list of instantiating unit

        // The EntityAspect call is no longer required. This might create error if the entity is
        // compiled in the "work" named library. Thus calling EntityAspect on work.entity will
        // look for entity in the working library which may be different from library "work"
        // and hence will error out from FindObjects. Please look at VIPER 4927

        //(void) _instantiated_unit->EntityAspect() ;
        if (_present_scope) _present_scope->Using(actual_master->Id()->LocalScope()) ;
        _unit = actual_master->Id() ;

#ifdef VHDL_PRESERVE_COMPONENT_INSTANCE
    }
#endif

    // Generics are already propagated. Delete this
    //unsigned i ;
    //VhdlDiscreteRange *aspect ;
    //FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) delete aspect ;
    //delete _generic_map_aspect ;
    //_generic_map_aspect = 0 ;
    CreateGenericAssociation() ;

    // Create port map aspect inspecting component specification, existing port map
    //  aspect of instantiation, default values of component and instantiated entity.
    CreatePortAssociation(copied_ports, actual_component, actual_component ? actual_master->Id(): 0) ;

    // Delete the copied stuffs.
    if (is_master_copied) {
        VhdlDiscreteRange *ele ;
        FOREACH_ARRAY_ITEM(copied_generics, i, ele) delete ele ;
        delete copied_generics ;
        FOREACH_ARRAY_ITEM(copied_ports, i, ele) delete ele ;
        delete copied_ports ;
    }
    PopAttributeSpecificPath() ;
    VhdlNode::PopStructuralStack() ;
}

// In static elaboration component instantiation is replaced
// with direct unique entity instantiation or copied component instantiation when
// VHDL_PRESERVE_COMPONENT_INSTANCE is on. This function creates the port map aspect
// of new instantiation.
// The following steps are performed
//
// * Inspect the port map of component instantiation and populate a Map
//   containing component-port-id->actual/association-elements. (Multiple
//   entry for a single id in case of partial association.
//
// * Insert the default values of component-port-id in the above map.
//
// * Inspect the port map from binding construct.
//   If it is like  new_unit_port_id=>expr or new_unit_port_id(1 to 3)=>expr
//   then create an association element of the new port map by copying the above.
//
//   If it is like new_unit_port_id=>component_port_id
//   then create association elements fetching the actuals from 1 st step
//   specific map and copying them.
//
// * If the port map from binding construct is not present, create new port
//   map from the port map aspect of component instantiation and default
//   values of component-port-ids.
//
// * If all new_unit-port-ids are not covered, consider default values of new_unit-port-ids.
//
// VIPER #7200: Add new argument 'actual_unit', to check instantiated entity against
// component when PRESERVE_COMPONENT is on
void
VhdlComponentInstantiationStatement::CreatePortAssociation(const Array *actual_ports, const VhdlIdDef *actual_component, const VhdlIdDef *actual_unit)
{
    Array* new_port_map = CreatePortAssociationArray(actual_ports, actual_component, actual_unit) ;
    if (!new_port_map) return ;
    // Delete existing port map aspect of component instantiation
    VhdlAssocElement* assoc;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, assoc) delete assoc ;
    delete _port_map_aspect ;
    // Set new port map aspect
    _port_map_aspect = new_port_map ;
}

Array*
VhdlComponentInstantiationStatement::CreatePortAssociationArray(const Array *actual_ports, const VhdlIdDef *actual_component, const VhdlIdDef *actual_unit) const
{
    // Instantiated unit not set, return
    if (!_unit) return 0 ;

    // No need to consider 'actual_unit' when it is direct entity instantiation
    if (actual_unit == _unit) actual_unit = 0 ;

    // To set correct line file information to the created
    // association elements of the port map
    linefile_type curr_linefile = 0;

    // Consider port map aspect to populate a Map
    // component-port-id->actuals/assoc
    unsigned i ;
    VhdlDiscreteRange *assoc ;
    VhdlIdDef *id ;
    Map comp_id_actual_map(POINTER_HASH) ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, assoc) {
        if (!assoc) continue ;
        if (!actual_component) continue ;
        // Set curr_linefile from port map aspect
        if (i == 0) curr_linefile = assoc->Linefile() ;
        VhdlDiscreteRange *actual = assoc->ActualPart() ;
        VhdlName *formal_part = assoc->FormalPart() ;
        // VIPER #2634 :For open connection default value is to be taken
        // VIPER #3376 : For partial association if actual is open, we should not ignore that
        // We will add partial_formal=>open in created port association list
        if (!actual || (actual->IsOpen() && (!formal_part || formal_part->FindFullFormal()))) continue ; // VIPER #2634 :For open connection default value is to be taken
        if (assoc->IsAssoc()) {
            id = assoc->FindFormal() ;
        } else {
            id = actual_component->GetPortAt(i) ;
        }
        if (!id) continue ;
        // Insert repeat, so that multiple actuals exists for
        // same key. Need for partial association
        (void) comp_id_actual_map.Insert(id, assoc, 0, 1) ;
    }
    VhdlComponentDecl *component = actual_component ? actual_component->GetComponentDecl(): 0 ;
    if (actual_component) {
        // Consider default values of component ports
        unsigned j ;
        VhdlInterfaceDecl *decl ;
        Array* port_clause_array = component ? component->GetPortClause() : 0 ;
        FOREACH_ARRAY_ITEM(port_clause_array, i, decl) {
            if (!decl) continue ;
            if (!decl->GetInitAssign()) continue ;

            FOREACH_ARRAY_ITEM(decl->GetIds(), j, id) {
                if (!id || !id->IsInput()) continue ;
                // Put default values in Map
                (void) comp_id_actual_map.Insert(id, decl->GetInitAssign()) ;
            }
        }
    }
    // Current line file can't be extracted from port map, get
    // line file from instance itself
    if (!curr_linefile) curr_linefile = Linefile() ;

    // We need to create port map aspect from port map of this instantiation
    // and port map from binding specification. If it was component instantiation
    // and port map contains named association, formal contains original component's
    // port ids. Again if 'actual_ports' comes from binding specification, its
    // formal part contains port ids of instantiated entity.
    // In static elaboration we always copy instantiated entity and so in modified
    // port map we need to use port ids of new instantiated unit as formal.
    // For this we need two association
    // 1. Port ids of Original instantiated entity vs port ids of copied instantiated
    //    entity (_unit is copied instantiated entity)
    // 2. Port ids of original instantiated component vs port ids of copied instantiated
    //    entity ('comp_vs_inst_unit')
    VhdlMapForCopy comp_vs_inst_unit ;

    // Set of associated entity ports. Need to specify the unassociated ports
    Set en_id_considered_map(POINTER_HASH) ;

    // VIPER #7200 : Set of associated entity ports when instantiation is componnet instance
    Set actual_en_id_considered(POINTER_HASH) ;
    VhdlMapForCopy comp_vs_actual_inst_unit ; // for PRESERVE_COMPONENT

    Set open_output_ports(POINTER_HASH) ; // Viper #7583 store o/p which are open
    // Set of associated component ports. Need to inspect the unassociated ports,
    // whose default values can be considered as actuals.
    Set comp_id_considered_map(POINTER_HASH) ;
    Array *new_port_map = 0 ;
    VhdlIdDef *inst_id_port = 0 ;
    FOREACH_ARRAY_ITEM(actual_ports, i, assoc) {
        if (!assoc) continue ;
        VhdlExpression *actual = 0 ;
        VhdlExpression *new_actual = 0 ;
        VhdlDiscreteRange *new_assoc = 0 ;
        id = 0 ; inst_id_port = 0 ;
        if (assoc->IsAssoc() ) {
            // Find formal id
            id = assoc->FindFormal() ;
        }
        if (id) {
            id = _unit->GetPort(id->Name()) ;
            inst_id_port = actual_unit ? actual_unit->GetPort(id->Name()): 0 ;
        } else {
            id = _unit->GetPortAt(i) ;
            inst_id_port = actual_unit ? actual_unit->GetPortAt(i): 0 ;
        }
        actual = assoc->ActualPart() ;
        VhdlName *formal_part = assoc->FormalPart() ;
        // VIPER 2634 : Do not consider open connection as valid actual
        // VIPER #3376 : For partial association if actual is open, we should not ignore that
        // We will add partial_formal=>open in created port association list
        if (!id || !actual) continue ;
        if (actual->IsOpen() && !id->IsInput()) {
            (void) open_output_ports.Insert(id) ; // VIPER #7583
        }
        if (actual->IsOpen() && (!formal_part || formal_part->FindFullFormal())) continue ;
        // Create new port map aspect
        if (!new_port_map) new_port_map = new Array(actual_ports->Size()) ;
        // Actual of this entity port is specified
        (void) en_id_considered_map.Insert(id) ;
        if (inst_id_port) (void) actual_en_id_considered.Insert(inst_id_port) ;
        VhdlIdDef *component_port_id = 0 ;
        component_port_id = actual->FindFormal() ;
        // Checks if component port id is in actual. If the id used in formal part is port and it
        // is declared in the component declaration, we can say that the id belongs to component.
        // If so get actuals from map 'comp_id_actual_map'
        if (component_port_id && component_port_id->IsPort() && component && component->LocalScope() && component->LocalScope()->IsDeclaredHere(component_port_id)) {
            if (!comp_id_considered_map.Insert(component_port_id)) continue ;
            if (!actual->FindFullFormal() || (assoc->IsAssoc() && !assoc->FormalPart()->FindFullFormal())) {
                // Actual part has component port id and actual or formal
                // has partial association. In this case we cannot create
                // association element as we do not create non constant
                // net for partial association.
                assoc->Error("partial association of component port '%s' with formal '%s' is not supported in static elaboration", component_port_id->Name(), id->Name()) ;
                unsigned k ; VhdlExpression *elem ;
                FOREACH_ARRAY_ITEM(new_port_map, k, elem) delete elem ;
                delete new_port_map ;
                return 0 ;
            }
            comp_vs_inst_unit.SetCopiedId(component_port_id, id) ;
            if (inst_id_port) comp_vs_actual_inst_unit.SetCopiedId(component_port_id, inst_id_port) ;
            MapItem *item = comp_id_actual_map.GetItem(component_port_id) ;
            // Multiple values for same component id, for
            // partial association
            while(item) {
                VhdlExpression *val = (VhdlExpression*)item->Value() ;
                if (!val) continue ;
                // Create new actual
                if (val->IsAssoc()) {
                    new_actual = val->CopyExpression(comp_vs_inst_unit) ;
                    ProcessFormal(new_actual->FormalPart(), component_port_id, id) ;
                } else {
                    new_actual = new VhdlAssocElement(new VhdlIdRef(id), val->CopyExpression(comp_vs_inst_unit)) ;
                    new_actual->SetLinefile(curr_linefile) ;
                    new_actual->FormalPart()->SetLinefile(curr_linefile) ;
                }
                new_port_map->Insert(new_actual) ;
                item = comp_id_actual_map.GetNextSameKeyItem(item) ;
            }

        } else {
            VhdlMapForCopy tmp_copyobj ;
            // No component port id in actual, create port map element
            if (assoc->IsAssoc()) {
                new_assoc = assoc->CopyDiscreteRange(tmp_copyobj) ; // Copy association
                formal_part = new_assoc ? new_assoc->FormalPart() : 0 ; // Get formal part
                // VIPER #3994 : If formal is a selected name, set formal identifier in prefix part
                // VIPER #4279 : If formal is type conversion set identifier on
                // argument
                if (formal_part) formal_part->SetFormal(id) ;
            } else {
                new_assoc = new VhdlAssocElement(new VhdlIdRef(id), actual->CopyExpression(tmp_copyobj)) ;
                new_assoc->SetLinefile(curr_linefile) ;
                new_assoc->FormalPart()->SetLinefile(curr_linefile) ;
            }
            new_port_map->Insert(new_assoc) ;
        }
    }
    // Port map not in component specification, create port map
    // from existing port map aspect and default values of component ports
    if (!actual_ports && actual_component && _unit->NumOfPorts()) {
        VhdlIdDef *en_port_id ;
        VhdlIdDef *com_port_id ;
        VhdlIdDef *actual_en_port_id ; // VIPER #7200
        // In existing port map aspect formals are component ports
        // Need to change them to instantiated entity ports.
        // So put those in the map used for copy, copy
        // routines will swap the ports
        for (i = 0; i < actual_component->NumOfPorts(); i++) {
            com_port_id = actual_component->GetPortAt(i) ;
            if (!com_port_id) continue ;
            en_port_id = _unit->GetPort(com_port_id->Name()) ;
            actual_en_port_id = actual_unit ? actual_unit->GetPort(com_port_id->Name()): 0 ;
            if (!en_port_id) continue ;
            comp_vs_inst_unit.SetCopiedId(com_port_id, en_port_id) ;
            if (actual_unit) comp_vs_actual_inst_unit.SetCopiedId(com_port_id, actual_en_port_id) ;
        }
    }

    if ((!actual_ports && actual_component && _unit->NumOfPorts()) || (_port_map_aspect && !new_port_map)) {
        VhdlIdDef *port_id ;
        VhdlMapForCopy tmp_copyobj ;
        FOREACH_ARRAY_ITEM(_port_map_aspect, i, assoc) {
            if (!assoc) continue ;
            VhdlExpression *actual = assoc->ActualPart() ;
            VhdlName *formal_part = assoc->FormalPart() ;
            // VIPER #7583: Store open connection
            if (assoc->IsAssoc()) {
                port_id = assoc->FindFormal() ;
            } else {
                port_id = (actual_component) ? actual_component->GetPortAt(i) : 0 ;
            }
            if (port_id && !port_id->IsInput() && actual && actual->IsOpen()) {
                (void) open_output_ports.Insert(comp_vs_inst_unit.GetCopiedId(port_id)) ;
            }
            // VIPER #2634 :For open connection default value is to be taken
            // VIPER #3376 : For partial association if actual is open, we should not ignore that
            // We will add partial_formal=>open in created port association list
            if (!actual || (actual->IsOpen() && (!formal_part || formal_part->FindFullFormal()))) continue ;
            VhdlDiscreteRange *new_actual = 0;
            if (assoc->IsAssoc()) {
                //port_id = assoc->FindFormal() ;
                // Copy the association
                new_actual = assoc->CopyDiscreteRange(comp_vs_inst_unit) ;
                ProcessFormal(new_actual->FormalPart(), port_id, comp_vs_inst_unit.GetCopiedId(port_id)) ;
                actual = 0 ;
            } else {
                //port_id = (actual_component) ? actual_component->GetPortAt(i) : 0 ;
                actual = assoc->ActualPart() ;
            }
            // Get entity port id for this component port id
            VhdlIdDef *en_port_id = comp_vs_inst_unit.GetCopiedId(port_id) ;
            if (!en_port_id) continue ; // some went wrong
            // This entity port is connected
            (void) en_id_considered_map.Insert(en_port_id) ;
            VhdlIdDef *actual_en_port_id = comp_vs_actual_inst_unit.GetCopiedId(port_id) ;
            if (actual_unit) (void) actual_en_id_considered.Insert(actual_en_port_id) ;
            // This component port is connected
            (void) comp_id_considered_map.Insert(port_id) ;

            // Create actual for this entity poty
            if (!new_actual) {
                new_actual = new VhdlAssocElement(new VhdlIdRef(en_port_id), actual ? actual->CopyExpression(tmp_copyobj) : 0) ;
                new_actual->SetLinefile(curr_linefile) ;
                new_actual->FormalPart()->SetLinefile(curr_linefile) ;
            }
            if (!new_port_map) new_port_map = new Array(_port_map_aspect->Size()) ;
            new_port_map->Insert(new_actual) ;
        }
    }
    if (!actual_ports && actual_component && _unit->NumOfPorts() && component) {
        // Consider default values of component ports to
        // create the port association list of new entity instantiation
        if (!new_port_map) new_port_map = new Array(_unit->NumOfPorts()) ;
        VhdlInterfaceDecl *decl ;
        FOREACH_ARRAY_ITEM(component->GetPortClause(), i, decl) {
            if (decl) decl->DefaultValues(&comp_id_considered_map, new_port_map, _unit, &en_id_considered_map, curr_linefile) ;
            // VIPER #7200 : Populate actual_en_id_considered from default values
            if (actual_unit && decl) decl->DefaultValues(&comp_id_considered_map, 0 /* no port map*/, actual_unit, &actual_en_id_considered, curr_linefile) ;
        }
    }
    // All ports of instantiated entity are not associated, consider
    // default values of ports of instantiated entity/component
    if (en_id_considered_map.Size() != _unit->NumOfPorts()) {
        if (!new_port_map) new_port_map = new Array(1) ;
        VhdlPrimaryUnit *prim_unit = _unit->GetPrimaryUnit() ;
        if (prim_unit) prim_unit->DefaultValues(&en_id_considered_map, new_port_map, _unit, curr_linefile) ;
        VhdlComponentDecl *comp = _unit->GetComponentDecl() ;
        Array *ports = comp ? comp->GetPortClause() : 0 ;
        VhdlInterfaceDecl *decl ;
        FOREACH_ARRAY_ITEM(ports, i, decl) {
            if (decl) decl->DefaultValues(&en_id_considered_map, new_port_map, _unit, &en_id_considered_map, curr_linefile) ;
        }
    }
    unsigned error_given = 0 ;
    for (i = 0; i < _unit->NumOfPorts(); i++) {
        VhdlIdDef *port_id = _unit->GetPortAt(i) ;
        if (!port_id) continue ;

        // VIPER #3486 : Added the second check, if a port is not connected
        // or has no default value and it is an unconstrained port, then
        // error out..
        // if (!port_id->IsInput()) continue ;
        if (!port_id->IsInput() && !port_id->IsUnconstrained()) continue ;
        if (en_id_considered_map.GetItem(port_id)) continue ;
        // This port is not connected, error
        port_id->Error("formal %s has no actual or default value", port_id->Name()) ;
        error_given = 1 ;
    }
    // VIPER #7200 : Catch error for unconnected port
    if (actual_unit && !error_given) {
        for (i = 0; i < actual_unit->NumOfPorts(); i++) {
            VhdlIdDef *port_id = actual_unit->GetPortAt(i) ;
            if (!port_id) continue ;

            // VIPER #3486 : Added the second check, if a port is not connected
            // or has no default value and it is an unconstrained port, then
            // error out..
            // if (!port_id->IsInput()) continue ;
            if (!port_id->IsInput() && !port_id->IsUnconstrained()) continue ;
            if (port_id->HasInitAssign()) continue ; // Has default value
            if (actual_en_id_considered.GetItem(port_id)) continue ;
            // This port is not connected, error
            port_id->Error("formal %s has no actual or default value", port_id->Name()) ;
        }
    }
    // Size of the created port map is zero, i.e. no
    // actual is specified, do not change the existing
    // port map.
    if (new_port_map && (new_port_map->Size() == 0)) {
        delete new_port_map ;
        new_port_map = 0 ;
    }
    if (new_port_map) {
        // VIPER #7583 : Create port map element with specified open
         SetIter si ;
        VhdlIdDef *out_id ;
        FOREACH_SET_ITEM (&open_output_ports, si, &out_id) {
            if (!out_id) continue ;
            if (!en_id_considered_map.GetItem(out_id)) {
                // open un connected output port : Create
                VhdlDiscreteRange *new_assoc = new VhdlAssocElement(new VhdlIdRef(out_id), new VhdlOpen()) ;
                new_assoc->SetLinefile(curr_linefile) ;
                new_assoc->FormalPart()->SetLinefile(curr_linefile) ;
                new_assoc->ActualPart()->SetLinefile(curr_linefile) ;
                new_port_map->Insert(new_assoc) ;
            }
        }
    }
    return new_port_map ;
}
Array*
VhdlComponentInstantiationStatement::CreateGenericAssociationArray(const Array *actual_generics, const VhdlIdDef *actual_component, const VhdlIdDef *actual_unit) const
{
    // Instantiated unit not set, return
    if (!_unit) return 0 ;

    // No need to consider 'actual_unit' when it is direct entity instantiation
    if (actual_unit == _unit) actual_unit = 0 ;

    // To set correct line file information to the created
    // association elements of the generic map
    linefile_type curr_linefile = 0;

    // Consider generic map aspect to populate a Map
    // component-generic-id->actuals/assoc
    unsigned i ;
    VhdlDiscreteRange *assoc ;
    VhdlIdDef *id ;
    Map comp_id_actual_map(POINTER_HASH) ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) {
        if (!assoc) continue ;
        if (!actual_component) continue ;
        // Set curr_linefile from generic map aspect
        if (i == 0) curr_linefile = assoc->Linefile() ;
        VhdlDiscreteRange *actual = assoc->ActualPart() ;
        VhdlName *formal_part = assoc->FormalPart() ;
        // VIPER #2634 :For open connection default value is to be taken
        // VIPER #3376 : For partial association if actual is open, we should not ignore that
        // We will add partial_formal=>open in created generic association list
        if (!actual || (actual->IsOpen() && (!formal_part || formal_part->FindFullFormal()))) continue ; // VIPER #2634 :For open connection default value is to be taken
        if (assoc->IsAssoc()) {
            id = assoc->FindFormal() ;
        } else {
            id = actual_component->GetGenericAt(i) ;
        }
        if (!id) continue ;
        // Insert repeat, so that multiple actuals exists for
        // same key. Need for partial association
        (void) comp_id_actual_map.Insert(id, assoc, 0, 1) ;
    }
    VhdlComponentDecl *component = actual_component ? actual_component->GetComponentDecl(): 0 ;
    if (actual_component) {
        // Consider default values of component generics
        unsigned j ;
        VhdlInterfaceDecl *decl ;
        Array* generic_clause_array = component ? component->GetGenericClause() : 0 ;
        FOREACH_ARRAY_ITEM(generic_clause_array, i, decl) {
            if (!decl) continue ;
            if (!decl->GetInitAssign()) continue ;

            FOREACH_ARRAY_ITEM(decl->GetIds(), j, id) {
                if (!id || !id->IsInput()) continue ;
                // Put default values in Map
                (void) comp_id_actual_map.Insert(id, decl->GetInitAssign()) ;
            }
        }
    }
    // Current line file can't be extracted from generic map, get
    // line file from instance itself
    if (!curr_linefile) curr_linefile = Linefile() ;

    // We need to create generic map aspect from generic map of this instantiation
    // and generic map from binding specification. If it was component instantiation
    // and generic map contains named association, formal contains original component's
    // generic ids. Again if 'actual_generics' comes from binding specification, its
    // formal part contains generic ids of instantiated entity.
    // In static elaboration we always copy instantiated entity and so in modified
    // generic map we need to use generic ids of new instantiated unit as formal.
    // For this we need two association
    // 1. generic ids of Original instantiated entity vs generic ids of copied instantiated
    //    entity (_unit is copied instantiated entity)
    // 2. generic ids of original instantiated component vs generic ids of copied instantiated
    //    entity ('comp_vs_inst_unit')
    VhdlMapForCopy comp_vs_inst_unit ;

    // Set of associated entity generics. Need to specify the unassociated generics
    Set en_id_considered_map(POINTER_HASH) ;

    // VIPER #7200 : Set of associated entity generics when instantiation is componnet instance
    Set actual_en_id_considered(POINTER_HASH) ;
    VhdlMapForCopy comp_vs_actual_inst_unit ; // for PRESERVE_COMPONENT

    // Set of associated component generics. Need to inspect the unassociated generics,
    // whose default values can be considered as actuals.
    Set comp_id_considered_map(POINTER_HASH) ;
    Array *new_generic_map = 0 ;
    VhdlIdDef *inst_id_generic = 0 ;
    FOREACH_ARRAY_ITEM(actual_generics, i, assoc) {
        if (!assoc) continue ;
        VhdlExpression *actual = 0 ;
        VhdlExpression *new_actual = 0 ;
        VhdlDiscreteRange *new_assoc = 0 ;
        id = 0 ; inst_id_generic = 0 ;
        if (assoc->IsAssoc() ) {
            // Find formal id
            id = assoc->FindFormal() ;
        }
        if (id) {
            id = _unit->GetGeneric(id->Name()) ;
            inst_id_generic = actual_unit ? actual_unit->GetGeneric(id->Name()): 0 ;
        } else {
            id = _unit->GetGenericAt(i) ;
            inst_id_generic = actual_unit ? actual_unit->GetGenericAt(i): 0 ;
        }
        actual = assoc->ActualPart() ;
        VhdlName *formal_part = assoc->FormalPart() ;
        // VIPER 2634 : Do not consider open connection as valid actual
        // VIPER #3376 : For partial association if actual is open, we should not ignore that
        // We will add partial_formal=>open in created generic association list
        if (!id || !actual) continue ;
        if (actual->IsOpen() && (!formal_part || formal_part->FindFullFormal())) continue ;
        // Create new generic map aspect
        if (!new_generic_map) new_generic_map = new Array(actual_generics->Size()) ;
        // Actual of this entity generic is specified
        (void) en_id_considered_map.Insert(id) ;
        if (inst_id_generic) (void) actual_en_id_considered.Insert(inst_id_generic) ;
        VhdlIdDef *component_generic_id = 0 ;
        component_generic_id = actual->FindFormal() ;
        // Checks if component generic id is in actual. If the id used in formal part is generic and it
        // is declared in the component declaration, we can say that the id belongs to component.
        // If so get actuals from map 'comp_id_actual_map'
        if (component_generic_id && component_generic_id->IsGeneric() && component && component->LocalScope() && component->LocalScope()->IsDeclaredHere(component_generic_id)) {
            if (!comp_id_considered_map.Insert(component_generic_id)) continue ;
            if (!actual->FindFullFormal() || (assoc->IsAssoc() && !assoc->FormalPart()->FindFullFormal())) {
                // Actual part has component generic id and actual or formal
                // has partial association. In this case we cannot create
                // association element as we do not create non constant
                // net for partial association.
                assoc->Error("partial association of component generic '%s' with formal '%s' is not supported in static elaboration", component_generic_id->Name(), id->Name()) ;
                unsigned k ; VhdlExpression *elem ;
                FOREACH_ARRAY_ITEM(new_generic_map, k, elem) delete elem ;
                delete new_generic_map ;
                return 0 ;
            }
            comp_vs_inst_unit.SetCopiedId(component_generic_id, id) ;
            if (inst_id_generic) comp_vs_actual_inst_unit.SetCopiedId(component_generic_id, inst_id_generic) ;
            MapItem *item = comp_id_actual_map.GetItem(component_generic_id) ;
            // Multiple values for same component id, for
            // partial association
            while(item) {
                VhdlExpression *val = (VhdlExpression*)item->Value() ;
                if (!val) continue ;
                // Create new actual
                if (val->IsAssoc()) {
                    new_actual = val->CopyExpression(comp_vs_inst_unit) ;
                    ProcessFormal(new_actual->FormalPart(), component_generic_id, id) ;
                } else {
                    new_actual = new VhdlAssocElement(new VhdlIdRef(id), val->CopyExpression(comp_vs_inst_unit)) ;
                    new_actual->SetLinefile(curr_linefile) ;
                    new_actual->FormalPart()->SetLinefile(curr_linefile) ;
                }
                new_generic_map->Insert(new_actual) ;
                item = comp_id_actual_map.GetNextSameKeyItem(item) ;
            }

        } else {
            VhdlMapForCopy tmp_copyobj ;
            // No component generic id in actual, create generic map element
            if (assoc->IsAssoc()) {
                new_assoc = assoc->CopyDiscreteRange(tmp_copyobj) ; // Copy association
                formal_part = new_assoc ? new_assoc->FormalPart() : 0 ; // Get formal part
                // VIPER #3994 : If formal is a selected name, set formal identifier in prefix part
                // VIPER #4279 : If formal is type conversion set identifier on
                // argument
                if (formal_part) formal_part->SetFormal(id) ;
            } else {
                new_assoc = new VhdlAssocElement(new VhdlIdRef(id), actual->CopyExpression(tmp_copyobj)) ;
                new_assoc->SetLinefile(curr_linefile) ;
                new_assoc->FormalPart()->SetLinefile(curr_linefile) ;
            }
            new_generic_map->Insert(new_assoc) ;
        }
    }
    // generic map not in component specification, create generic map
    // from existing generic map aspect and default values of component generics
    if (!actual_generics && actual_component && _unit->NumOfGenerics()) {
        VhdlIdDef *en_generic_id ;
        VhdlIdDef *com_generic_id ;
        VhdlIdDef *actual_en_generic_id ; // VIPER #7200
        // In existing generic map aspect formals are component generics
        // Need to change them to instantiated entity generics.
        // So put those in the map used for copy, copy
        // routines will swap the generics
        for (i = 0; i < actual_component->NumOfGenerics(); i++) {
            com_generic_id = actual_component->GetGenericAt(i) ;
            if (!com_generic_id) continue ;
            en_generic_id = _unit->GetGeneric(com_generic_id->Name()) ;
            actual_en_generic_id = actual_unit ? actual_unit->GetGeneric(com_generic_id->Name()): 0 ;
            if (!en_generic_id) continue ;
            comp_vs_inst_unit.SetCopiedId(com_generic_id, en_generic_id) ;
            if (actual_unit) comp_vs_actual_inst_unit.SetCopiedId(com_generic_id, actual_en_generic_id) ;
        }
    }

    if ((!actual_generics && actual_component && _unit->NumOfGenerics()) || (_generic_map_aspect && !new_generic_map)) {
        VhdlIdDef *generic_id ;
        VhdlMapForCopy tmp_copyobj ;
        FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) {
            if (!assoc) continue ;
            VhdlExpression *actual = assoc->ActualPart() ;
            VhdlName *formal_part = assoc->FormalPart() ;
            // VIPER #7583: Store open connection
            if (assoc->IsAssoc()) {
                generic_id = assoc->FindFormal() ;
            } else {
                generic_id = (actual_component) ? actual_component->GetGenericAt(i) : 0 ;
            }
            // VIPER #2634 :For open connection default value is to be taken
            // VIPER #3376 : For partial association if actual is open, we should not ignore that
            // We will add partial_formal=>open in created generic association list
            if (!actual || (actual->IsOpen() && (!formal_part || formal_part->FindFullFormal()))) continue ;
            VhdlDiscreteRange *new_actual = 0;
            if (assoc->IsAssoc()) {
                //generic_id = assoc->FindFormal() ;
                // Copy the association
                new_actual = assoc->CopyDiscreteRange(comp_vs_inst_unit) ;
                ProcessFormal(new_actual->FormalPart(), generic_id, comp_vs_inst_unit.GetCopiedId(generic_id)) ;
                actual = 0 ;
            } else {
                //generic_id = (actual_component) ? actual_component->GetGenericAt(i) : 0 ;
                actual = assoc->ActualPart() ;
            }
            // Get entity generic id for this component generic id
            VhdlIdDef *en_generic_id = comp_vs_inst_unit.GetCopiedId(generic_id) ;
            if (!en_generic_id) continue ; // some went wrong
            // This entity generic is connected
            (void) en_id_considered_map.Insert(en_generic_id) ;
            VhdlIdDef *actual_en_generic_id = comp_vs_actual_inst_unit.GetCopiedId(generic_id) ;
            if (actual_unit) (void) actual_en_id_considered.Insert(actual_en_generic_id) ;
            // This component generic is connected
            (void) comp_id_considered_map.Insert(generic_id) ;

            // Create actual for this entity poty
            if (!new_actual) {
                new_actual = new VhdlAssocElement(new VhdlIdRef(en_generic_id), actual ? actual->CopyExpression(tmp_copyobj) : 0) ;
                new_actual->SetLinefile(curr_linefile) ;
                new_actual->FormalPart()->SetLinefile(curr_linefile) ;
            }
            if (!new_generic_map) new_generic_map = new Array(_generic_map_aspect->Size()) ;
            new_generic_map->Insert(new_actual) ;
        }
    }
    if (!actual_generics && actual_component && _unit->NumOfGenerics() && component) {
        // Consider default values of component generics to
        // create the generic association list of new entity instantiation
        if (!new_generic_map) new_generic_map = new Array(_unit->NumOfGenerics()) ;
        VhdlInterfaceDecl *decl ;
        FOREACH_ARRAY_ITEM(component->GetGenericClause(), i, decl) {
            if (decl) decl->DefaultValues(&comp_id_considered_map, new_generic_map, _unit, &en_id_considered_map, curr_linefile) ;
            // VIPER #7200 : Populate actual_en_id_considered from default values
            if (actual_unit && decl) decl->DefaultValues(&comp_id_considered_map, 0 /* no generic map*/, actual_unit, &actual_en_id_considered, curr_linefile) ;
        }
    }
    // All generics of instantiated entity are not associated, consider
    // default values of generics of instantiated entity/component
    if (en_id_considered_map.Size() != _unit->NumOfGenerics()) {
        if (!new_generic_map) new_generic_map = new Array(1) ;
        VhdlPrimaryUnit *prim_unit = _unit->GetPrimaryUnit() ;
        if (prim_unit) prim_unit->DefaultValues(&en_id_considered_map, new_generic_map, _unit, curr_linefile) ;
        VhdlComponentDecl *comp = _unit->GetComponentDecl() ;
        Array *generics = comp ? comp->GetGenericClause() : 0 ;
        VhdlInterfaceDecl *decl ;
        FOREACH_ARRAY_ITEM(generics, i, decl) {
            if (decl) decl->DefaultValues(&en_id_considered_map, new_generic_map, _unit, &en_id_considered_map, curr_linefile) ;
        }
    }
    unsigned error_given = 0 ;
    for (i = 0; i < _unit->NumOfGenerics(); i++) {
        VhdlIdDef *generic_id = _unit->GetGenericAt(i) ;
        if (!generic_id) continue ;

        // VIPER #3486 : Added the second check, if a generic is not connected
        // or has no default value and it is an unconstrained generic, then
        // error out..
        // if (!generic_id->IsInput()) continue ;
        if (!generic_id->IsInput() && !generic_id->IsUnconstrained()) continue ;
        if (en_id_considered_map.GetItem(generic_id)) continue ;
        // This generic is not connected, error
        generic_id->Error("formal %s has no actual or default value", generic_id->Name()) ;
        error_given = 1 ;
    }
    // VIPER #7200 : Catch error for unconnected generic
    if (actual_unit && !error_given) {
        for (i = 0; i < actual_unit->NumOfGenerics(); i++) {
            VhdlIdDef *generic_id = actual_unit->GetGenericAt(i) ;
            if (!generic_id) continue ;

            // VIPER #3486 : Added the second check, if a generic is not connected
            // or has no default value and it is an unconstrained generic, then
            // error out..
            // if (!generic_id->IsInput()) continue ;
            if (!generic_id->IsInput() && !generic_id->IsUnconstrained()) continue ;
            if (generic_id->HasInitAssign()) continue ; // Has default value
            if (actual_en_id_considered.GetItem(generic_id)) continue ;
            // This generic is not connected, error
            generic_id->Error("formal %s has no actual or default value", generic_id->Name()) ;
        }
    }
    // Size of the created generic map is zero, i.e. no
    // actual is specified, do not change the existing
    // generic map.
    if (new_generic_map && (new_generic_map->Size() == 0)) {
        delete new_generic_map ;
        new_generic_map = 0 ;
    }
    return new_generic_map ;
}

// Consider default values of unassociated ports to create port
// association list of entity instantiation.
void
VhdlEntityDecl::DefaultValues(Set *id_considered, Array *port_map, const VhdlIdDef *instantiated_unit, linefile_type linefile)
{
    if (!id_considered || !port_map) return ;
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (!decl) continue ;
        decl->DefaultValues(id_considered, port_map, instantiated_unit, id_considered, linefile) ;
    }
}

// Get an interface declaration, if the port is not associated, then
// take default value of that port to create new port association
// element
void
VhdlInterfaceDecl::DefaultValues(const Set *id_considered, Array *port_map, const VhdlIdDef *instantiated_unit, Set *en_id_considered, linefile_type linefile) const
{
    //if (!id_considered || !port_map || !en_id_considered || !instantiated_unit) return ;
    if (!id_considered || !en_id_considered || !instantiated_unit) return ;

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0) ;
    df.SetInInitial() ;

    unsigned j ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, j, id) {
        if (!id) continue ;
        VhdlIdDef *port_id = instantiated_unit->GetPort(id->Name()) ;
        if (!port_id) continue ;
        // This port is already associated with actual
        // Check component ids
        if (id_considered->GetItem(id)) continue ;
        // Take the default values only for input
        if (!id->IsInput()) continue ;
        if (!_init_assign) continue ;
        (void) en_id_considered->Insert(port_id) ; // As initial value exists for this, consider this port
        if (!port_map) continue ; // No port_map, no need to create association
        // Evaluate default value
        VhdlValue *value = _init_assign->Evaluate(id->Constraint(), &df, 0) ;
        VhdlExpression *new_init = value ? value->CreateConstantVhdlExpressionInternal(Linefile(), id->BaseType(), -1, 0) : 0 ;
        // For vhdl-2008 do not call TypeInfer during elaboration as proper context
        // is not available to call scope->FindXXXX
        //if (new_init) (void) new_init->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ; // VIPER 2661 : Type infer created value
        delete value ;
        // VIPER #4599 : If '_init_assign' cannot be evaluated to a valid expression, ignore that.
        if (!new_init) continue ;
        // Create port map element
        VhdlAssocElement *new_assoc = 0 ;
        new_assoc = new VhdlAssocElement(new VhdlIdRef(port_id), new_init) ;
        new_assoc->SetLinefile(linefile) ;
        new_assoc->FormalPart()->SetLinefile(linefile) ;
        port_map->Insert(new_assoc) ;
    }
}

// Unroll the generate statement.
// Replace generate statement with unrolled block statements.
void
VhdlGenerateStatement::StaticElaborate(VhdlBlockConfiguration *block_config, VhdlDataFlow *df)
{
    if (!_scheme) return ;
    if (_label) PushNamedPath(_label->OrigName()) ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // VIPER #3393 : Keep an array to store elaboration created block identifiers.
    // We create new block statements for each iteration of for-generate and single
    // block statement for if-generate. We will store block identifiers of those
    // blocks in this array instead of declaring those immediately in generate scope.
    // This will be done as created block names can create conflict with already
    // declared identifiers within this generate statement.
    Map new_ids(STRING_HASH_CASE_INSENSITIVE) ;

    // Unroll the generate statement according to the scheme
    Array * new_stmts = _scheme->StaticElaborate(df, block_config, this, &new_ids, _label) ;
    if (!new_stmts || !new_stmts->Size() || _scheme->IsForScheme()) {
        // VIPER #6793: Create empty block when condition of if is false or
        // _scheme is for-scheme.
        VhdlMapForCopy old2new ;
        VhdlBlockStatement *blk_stmt = VhdlIterScheme::CreateBlockStatement(_label, _local_scope, 0, 0, 0, old2new, &new_ids, this) ;
        blk_stmt->SetStaticElaborated() ;
        blk_stmt->SetIsElabCreatedEmpty() ;
        if (!new_stmts) new_stmts = new Array(1) ;
        new_stmts->Insert(blk_stmt) ;
    }
    // Reset the global scope
    _present_scope = old_scope ;
    if (_label) PopNamedPath() ; // Pop named path

    // Get the container scope of generate, so that we can declare declared identifiers there :
    VhdlScope *container = _local_scope ? _local_scope->Upper(): 0 ;
    ReplaceByBlocks(container, &new_ids, new_stmts, this) ;
}
void VhdlGenerateStatement::ReplaceByBlocks(VhdlScope *container, const Map *new_ids, Array *new_stmts, VhdlStatement *gen_stmt)
{
    VhdlIdDef *container_id = (container) ? container->GetOwner(): 0 ;

    // Undeclare label of this generate. This will allow us to declare identifier
    // with label name in container scope :
    if (container && gen_stmt) {
        VhdlIdDef *label_id = gen_stmt->GetLabel() ;
        // VIPER #6793 : With the label of generate statement create an empty
        // block. This will keep the existing label id alive and this may be
        // referred from attribute specification or attribute name.
        container->Undeclare(label_id) ;
        gen_stmt->SetLabelNull() ;
        // VIPER #7923 : The label id of generate statement is now owner of
        // another block. But it is also set as owner of local-scope of
        // generate statement. So make the owner of generate statement's
        // local-scope as null.
        VhdlScope *gen_scope = gen_stmt->LocalScope() ;
        if (gen_scope) gen_scope->SetOwner(0) ;
    }

    // Iterate over created identifiers and declare those in container scope:
    MapIter mi ;
    VhdlIdDef *id ;
    // Iterate over elaboration created identifiers
    FOREACH_MAP_ITEM(new_ids, mi, 0, &id) {
        if (!id) continue ;
        // Declare identifier in local scope
        if (container) (void) container->Declare(id) ;
    }
    // Container can be block statement or statement part of architecture.
    // In the container replace this generate statement with elaborated statement(s)
    VhdlStatement *container_stmt = (container_id) ? container_id->GetStatement() : 0 ;
    VhdlSecondaryUnit *container_unit = (container_id) ? container_id->GetSecondaryUnit(): 0 ;
    unsigned not_replaced = 0 ;
    if (container_stmt) {
        if (!container_stmt->ReplaceChildBy(gen_stmt, new_stmts)) not_replaced = 1 ;
    } else if (container_unit) {
        if (!container_unit->ReplaceChildBy(gen_stmt, new_stmts)) not_replaced = 1 ;
    } else {
        not_replaced = 1 ;
    }
    if (not_replaced) { // Cannot replace generate :
        // Delete created statements
        VhdlStatement *stmt ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(new_stmts, i, stmt) delete stmt ;
    }
    delete new_stmts ;
}

// Iterate over the possible values of loop index variable.
// For each value of loop index variable, create a block
// statement and elaborate that statement.
// Return the created block statements.
Array *
VhdlForScheme::StaticElaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config, VhdlStatement *gen_stmt, Map *new_block_ids, VhdlIdDef *stmt_label)
{
    //VhdlIdDef *stmt_label = gen_stmt->GetLabel() ;
    Array *declarations = gen_stmt->GetDeclPart() ;
    Array *statements = gen_stmt->GetStatementPart() ;
    VhdlScope *block_scope = gen_stmt->LocalScope() ;
    //CARBON_BEGIN
    // Comment this out, to have StaticElaborate() run on non-generate for loops
    if (!statements) statements = gen_stmt->GetStatements();
    //CARBON_END
    //if (block_scope) block_scope = block_scope->Upper() ;

    //if (!statements && !declarations) return 0 ;
    if (!_range || !_iter_id) return 0 ;

    // Get constraint of the loop index variable
    VhdlConstraint *range = _range->EvaluateConstraint(df, 0) ;

    if (!range || range->IsNullRange() || !range->Left() || !range->Right()) {
        delete range ;
        return 0 ;
    }
    // VIPER #4373 : Check out of range accessing of loop index variable in block configuration
    if (block_config) block_config->CheckLoopIndexRange(stmt_label, range) ;
    // VIPER #7719: We may create empty Array for declarations if there was "begin" keyword specified.
    // So, check the size of the declarations Array and ignore if size is 0:
    if (!statements && (!declarations || !declarations->Size())) {
        delete range ;
        return 0 ;
    }

    VhdlValue *runner = range->Left()->Copy() ;
    Array *new_stmts = new Array(3) ;
    Array *copied_config_array = block_config ? new Array((unsigned)range->Length()) : 0 ;
    VhdlBlockConfiguration *copied_config = 0 ;
    // Iterate over possible values of loop index variable
    while (1) {
        // Set current value to loop index identifier
        _iter_id->SetValue(runner->Copy()) ;
        char *cur_val = runner->Image() ;
        PushAttributeSpecificPath(stmt_label ? stmt_label->Name(): 0, 0, 0, cur_val) ;
        Strings::free(cur_val) ;

        VhdlMapForCopy old2new ;

        // Extract configuration of the block specified by 'stmt_label'
        VhdlBlockConfiguration *subblock_config = block_config ? block_config->ExtractSubBlock(stmt_label, runner, 0) : 0 ;

        // Create block statement for current value of
        // loop index. All the decls and statements of
        // the for generate will be added within the
        // newly created block statement
        VhdlBlockStatement *blk_stmt = 0 ;
        blk_stmt = CreateBlockStatement(stmt_label, block_scope, _iter_id, declarations, statements, old2new, new_block_ids, this) ;

        // Insert the created block statement to an array to be returned
        new_stmts->Insert(blk_stmt) ;

        // Elaborate the declarations and statements of
        // the newly created block statement
        copied_config = ProcessBlockStatement(blk_stmt, runner, subblock_config, old2new, df) ;
        if (copied_config_array && copied_config) copied_config_array->Insert(copied_config) ;
        PopAttributeSpecificPath() ;

        delete subblock_config ;
        // Check for the terminating condition of the loop
        if (runner->Position()==range->Right()->Position()) break ;
        // Increment or decrement the value of loop index
        (void) runner->UnaryOper((range->Dir()==VHDL_to) ? VHDL_INCR : VHDL_DECR,this) ;
    }
    unsigned i ;
    FOREACH_ARRAY_ITEM(copied_config_array, i, copied_config) delete copied_config ;
    delete copied_config_array ;
    _iter_id->SetValue(0) ;
    delete runner ;
    delete range ;
    return new_stmts ;
}

// Evaluate the condition of the scheme.
// If condition is true, create block statement with
// the declarations and statements of the generate.
// Return the create statement inserting in an array
Array *
VhdlIfScheme::StaticElaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config, VhdlStatement *gen_stmt, Map *new_block_ids, VhdlIdDef *stmt_label)
{
    //VhdlIdDef *stmt_label = gen_stmt->GetLabel() ;
    Array *declarations = gen_stmt->GetDeclPart() ;
    Array *statements = gen_stmt->GetStatementPart() ;
    VhdlScope *block_scope = gen_stmt->LocalScope() ;
    //if (block_scope) block_scope = block_scope->Upper() ;

    // Set _present_scope here, as it will be executed for elsif and else branch too
    VhdlScope *old_scope = _present_scope ;
    _present_scope = block_scope ;

    // VIPER #6959: Do not return 0 when scheme contains no statement. We need
    // to proceed as condition can be true.
    //if (!statements) return 0 ;
    if (!_condition && !IsElseScheme()) return 0 ;

    VhdlDataFlow *decl_flow = (df) ? df : new VhdlDataFlow(0, 0, (stmt_label) ? stmt_label->LocalScope() : 0) ; // Viper #5699
    if (!df) decl_flow->SetInInitial() ;

    // Evaluate the condition of the if-scheme
    VhdlValue *condition = _condition ? _condition->Evaluate(0,decl_flow,0): 0 ;

    if (!df) delete decl_flow ; // Viper #5699

    // Condition should be constant
    if (_condition && (!condition || !condition->IsConstant())) {
        _condition->Error("generate condition is not constant") ;
        delete condition ;
        return 0 ;
    }
    PushAttributeSpecificPath(stmt_label ? stmt_label->Name():0) ;

    Array *new_stmts = new Array(1) ;
    if (!_condition || (condition && condition->IsTrue())) {
        // Create block statement with statements inside
        // the generate construct
        VhdlMapForCopy old2new ;

        // Extract configuration of the block specified by 'stmt_label'
        VhdlBlockConfiguration *subblock_config = block_config ? block_config->ExtractSubBlock(stmt_label, 0, _alternative_label_id) : 0 ;

        VhdlBlockStatement *blk_stmt = 0 ;
        blk_stmt = CreateBlockStatement(stmt_label, block_scope, 0, declarations, statements, old2new, new_block_ids, this) ;
        new_stmts->Insert(blk_stmt) ;

        // Elaborate the declarations and statements of the
        // created block statement
        VhdlBlockConfiguration *copied_config = ProcessBlockStatement(blk_stmt, 0, subblock_config, old2new, df) ;
        delete copied_config ;
        delete subblock_config ;
    }
    PopAttributeSpecificPath() ;
    delete condition ;
    _present_scope = old_scope ;
    return new_stmts ;
}

// Create block label considering the current value of loop index variable.
// Declare loop index variable as a constant in the block.
// The initial value of the constant is the current value of
// loop index variable.
// Copy the declarations, statements of the generate statement and
// use those to create a new block statement.
VhdlBlockStatement *
VhdlIterScheme::CreateBlockStatement(VhdlIdDef *stmt_label, const VhdlScope *gen_scope, const VhdlIdDef *iter_id, const Array *declarations, const Array *statements, VhdlMapForCopy &old2new, Map *new_block_ids, const VhdlTreeNode *from)
{
    // Block statements are created within generate scope
    // Create new block label with the current value of iterator

    // VIPER #4437 : Use API to generate name of the block. User can modify it as per their requirement
    char *new_blk_name = CreateBlockName(stmt_label, iter_id ? iter_id->Value(): 0, _present_scope, new_block_ids) ;
    //VhdlIdDef *block_label =  new VhdlLabelId(new_blk_name) ;
    VhdlIdDef *block_label =  0 ;
    if ((iter_id && iter_id->Value()) || !stmt_label) {
        block_label = new VhdlBlockId(new_blk_name) ;
    } else {
        // VIPER #6793 : For if-scheme, use the statement label of generate
        // statement to create new block statement.
        block_label = stmt_label ;
        Strings::free(new_blk_name) ;
    }
    if (stmt_label) block_label->SetLinefile(stmt_label->Linefile()) ;
    // Declare the label id in the generate scope
    // VIPER #3393 : Do not declare it now. upper_scope can already have identifier
    // of same name. Insert new label id to 'new_block_ids' to declare those later.
    //if (block_label && upper_scope) (void) upper_scope->Declare(block_label) ;
    VhdlScope *upper_scope = (gen_scope) ? gen_scope->Upper(): 0 ;
    if (upper_scope) (void) new_block_ids->Insert(block_label->Name(), block_label, 0, 1) ;
    old2new.SetCopiedId(stmt_label, block_label) ;

    old2new.CopyPredefinedOperators(gen_scope) ;

    // Create local scope of the block
    VhdlScope *block_scope = new VhdlScope(upper_scope, block_label, 0) ;

    old2new.SetCopiedScope(gen_scope, block_scope) ;

    // Declare loop index variable as a constant. The initial
    // value of the constant will be the current value
    // of the loop index variable
    VhdlConstantDecl *const_decl = 0 ;
    Array *new_decls = new Array(declarations ? declarations->Size() +1 : 1) ;
    if (iter_id) {
        const_decl = CreateConstantDecl(iter_id, old2new, block_scope, from) ;
        new_decls->Insert(const_decl) ;
    }

    // Copy the declarations of generate statements
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(declarations, i, decl) {
        if (!decl) continue ;
        VhdlDeclaration *new_decl = decl->CopyDeclaration(old2new) ;
        new_decls->Insert(new_decl) ;
    }

    // Copy the statements of the generate construct
    Array *new_stmts = old2new.CopyStatementArray(statements) ;
    block_scope->Update(gen_scope, old2new) ;
    block_scope->SetOwner(block_label) ; // Set owner id, may contain alternative label

    // Create block statement
    VhdlBlockStatement *blk_stmt = new VhdlBlockStatement(new_decls, new_stmts, block_scope) ;
    if (from) blk_stmt->SetLinefile(from->Linefile()) ;
    blk_stmt->SetLabel(block_label) ;
    blk_stmt->SetGenerateElabCreated() ; // Mark generate elaboration created block statement

    // Set backpointers to attribute specifications
    FOREACH_ARRAY_ITEM(new_decls, i, decl) {
        if (decl) decl->UpdateDecl(old2new) ;
    }
    return blk_stmt ;
}

// Create constant declaration from the loop index variable.
VhdlConstantDecl *
VhdlIterScheme::CreateConstantDecl(const VhdlIdDef *iter_id, VhdlMapForCopy &old2new, VhdlScope * /*block_scope*/, const VhdlTreeNode *from)
{
    // Create a constant declaration for the loop index variable.
    // The initial value will be the current value of the
    // loop index variable

    // Copy the loop index variable
    VhdlIdDef *new_iter_id = iter_id ? iter_id->CopyIdDef(old2new) : 0 ;
    old2new.SetCopiedId(iter_id, new_iter_id) ;

    // Extract the data type of the loop index variable
    VhdlIdDef *data_type = new_iter_id ? new_iter_id->Type() : 0 ;

    // Create the data type
    VhdlIdRef *type = 0 ;
    if (data_type) {
        type = new VhdlIdRef(data_type) ;
        if (from) type->SetLinefile(from->Linefile()) ;
    }

    // Create the identifier list
    Array *id_list = new Array(1) ;
    id_list->Insert(new_iter_id) ;
    if (new_iter_id) new_iter_id->SetDecl(new_iter_id) ;
    VhdlExpression *init_assign = 0 ;

    // Create initial value of the constant with the current
    // value of the loop index variable
    VhdlValue *curr_value = new_iter_id ? new_iter_id->Value() : 0 ;
    if (curr_value && new_iter_id) {
        init_assign = curr_value->CreateConstantVhdlExpressionInternal(from?from->Linefile():0, new_iter_id->BaseType(), -1, 0) ;
        // For vhdl-2008 do not call TypeInfer during elaboration as proper context
        // is not available to call scope->FindXXXX
        //if (init_assign) (void) init_assign->TypeInfer(new_iter_id->BaseType(), 0, VHDL_READ, 0) ; // VIPER 2661 : Type infer created value
    }

    // Create the declaration
    VhdlConstantDecl *decl = new VhdlConstantDecl(id_list, type, init_assign) ;
    if (from) decl->SetLinefile(from->Linefile()) ;
    return decl ;
}

// Convert VhdlValue to expression
//

VhdlExpression *
VhdlValue::CreateConstantVhdlExpression(linefile_type linefile, VhdlIdDef *type, int dim)
{
    return (CreateConstantVhdlExpressionInternal(linefile, type, dim, 0 /*without_other*/)) ;
}

VhdlExpression *
VhdlAccessValue::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other)
{
    VhdlExpression *expr = 0 ;
    if (_value) {
        expr = _value->CreateConstantVhdlExpressionInternal(linefile, type, dim, without_other) ;
    } else {
        expr = new VhdlNull() ;
        expr->SetLinefile(linefile) ;
    }
    return expr ;
}

VhdlExpression *
VhdlInt::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int /*dim*/, unsigned /*without_other*/)
{
    // integer value to VhdlInteger
    VhdlExpression *expr = 0 ;
    expr = new VhdlInteger(_value) ;
    expr->SetLinefile(linefile) ;
    if (type && type->IsPhysicalType()) {
        // For physical type generic create physical literal
        // The unit of the literal will be the lowest unit.
        Map *unit_list = type->GetTypeDefList() ;
        VhdlIdDef *physical_unit_id = unit_list ? (VhdlIdDef*)unit_list->GetValueAt(0) : 0 ;

        // Viper #4643: The value is normalized against the time resolution
        // set by the user (Position for the corresponding unit would be 1)
        MapIter mi ;
        VhdlIdDef *unit_id ;
        FOREACH_MAP_ITEM(unit_list, mi, 0, &unit_id) {
            if (!unit_id || unit_id->Position() != 1) continue ;
            physical_unit_id = unit_id ;
            break ;
        }

        if (physical_unit_id) {
            //VhdlIdRef *unit = new VhdlIdRef(Strings::save(physical_unit_id->Name())) ;
            VhdlIdRef *unit = new VhdlIdRef(physical_unit_id) ;
            unit->SetLinefile(linefile) ;
            expr = new VhdlPhysicalLiteral(expr, unit) ;
            expr->SetLinefile(linefile) ;
        }
    }
    return expr ;
}

VhdlExpression *
VhdlEnum::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef * /*type*/, int /*dim*/, unsigned /*without_other*/)
{
    VERIFIC_ASSERT(_enum_id) ;
    const char *enum_name = _enum_id->Name() ;
    VhdlExpression *expr = 0 ;
    if (enum_name && Strings::len(enum_name) && (enum_name[0]=='\'')) {
        expr = new VhdlCharacterLiteral(Strings::save(enum_name)) ;
        expr->SetId(_enum_id) ;
    } else {
        // enumeration to identifier reference
        expr = new VhdlIdRef(_enum_id) ;
    }
    expr->SetLinefile(linefile) ;
    return expr ;
}
VhdlExpression *
VhdlDouble::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int /*dim*/, unsigned /*without_other*/)
{
    // double to VhdlReal
    VhdlExpression *expr = new VhdlReal(_value) ;
    expr->SetLinefile(linefile) ;
    if (type && type->IsPhysicalType()) {
        // For physical type generic create physical literal
        // The unit of the literal will be the lowest unit.
        Map *unit_list = type->GetTypeDefList() ;
        VhdlIdDef *physical_unit_id = unit_list ? (VhdlIdDef*)unit_list->GetValueAt(0) : 0 ;

        // Viper #4643: The value is normalized against the time resolution
        // set by the user (Position for the corresponding unit would be 1)
        MapIter mi ;
        VhdlIdDef *unit_id ;
        FOREACH_MAP_ITEM(unit_list, mi, 0, &unit_id) {
            if (!unit_id || unit_id->Position() != 1) continue ;
            physical_unit_id = unit_id ;
            break ;
        }

        if (physical_unit_id) {
            VhdlIdRef *unit = new VhdlIdRef(Strings::save(physical_unit_id->Name())) ;
            unit->SetLinefile(linefile) ;
            expr = new VhdlPhysicalLiteral(expr, unit) ;
            expr->SetLinefile(linefile) ;
        }
    }
    return expr ;
}
VhdlExpression *
VhdlCompositeValue::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int dim, unsigned without_other)
{
    unsigned i ;
    VhdlValue *val ;
    VhdlIdDef *elem_type = 0 ;
    int new_dim = dim ;
    if (type && type->IsArrayType()) {
        if ((dim != 1) && (type->Dimension() > 1)) {
            elem_type = type ;
            new_dim = (dim > 0) ? (dim-1) : (((int)type->Dimension())-1) ;
        } else {
            elem_type = type->ElementType() ;
            new_dim = -1 ;
        }
    } else if (type && type->IsRecordType()) {
        // elem_type will be set per element
    } else {
        elem_type = type ;
    }
    if (!_values || !_values->Size()) {
        if (type && Strings::compare_nocase(type->Name(), "string")) {
            // empty array. Can only be empty array
            VhdlExpression *expr = new VhdlStringLiteral(Strings::save("\"\"")) ;
            expr->SetId(type) ;
            expr->SetLinefile(linefile) ;
            return expr ;
        } else {
            return 0 ; // Cannot interpret any legal way
        }
    }
    // VIPER #3297 : Create string literal for array of characters
    //if (elem_type && Strings::compare_nocase(elem_type->Name(), "character")) {
    if (type && Strings::compare_nocase(type->Name(), "string")) {
        char *str_eqv = StringImage() ;
        // Check whether image contains any non graphic literal character :
        char *tmp = str_eqv ;
        unsigned char c ;
        unsigned contains_non_graphic = 0 ;
        while (tmp && *tmp!=0) {
            c = (unsigned char)(*tmp) ;
            if ((c <= 0x1F) || ((c >= 0x7F) && (c <= 0x9F))) {
                *tmp = ' ' ; // replace by space
                contains_non_graphic = 1 ;
                break ;
            }
            tmp++ ;
        }
        if (!contains_non_graphic) {
            VhdlExpression *expr = new VhdlStringLiteral(str_eqv) ;
            expr->SetId(type) ;
            expr->SetLinefile(linefile) ;
            return expr ;
        } else {
            if (_values->Size() == 1) {
                val = (VhdlValue*)_values->At(0) ;
                VhdlExpression *expr =  (val) ? val->CreateConstantVhdlExpressionInternal(linefile, elem_type, -1, without_other): 0 ;
                VhdlExpression *null_str = new VhdlStringLiteral(Strings::save("\"\"")) ;
                if (elem_type) null_str->SetId(elem_type) ;
                null_str->SetLinefile(linefile) ;
                expr = new VhdlOperator (null_str, VHDL_AMPERSAND, expr) ;
                expr->SetLinefile(linefile) ;
                return expr ;
            } else {
                Strings::free(str_eqv) ;
            }
        }
    }
    // VIPER #3512 : Since aggregate with one element should not be positional
    // we need to create named element association when this composite value
    // contains 1 element.
    unsigned create_named = 0 ;
    if (type && (type->IsArrayType() || type->IsRecordType()) && (_values->Size() == 1)) {
        create_named = 1 ;
    }
    unsigned j ;
    unsigned all_null = without_other ? 0 : 1 ;
    FOREACH_ARRAY_ITEM(_values, j, val) {
        if (val) {
            all_null = 0 ;
            break ;
        }
    }
    Array *assoc_list = 0 ;
    if (!all_null) {
        assoc_list = new Array(_values->Size()) ;
        FOREACH_ARRAY_ITEM(_values, i, val) {
            if (!val) val = _default_elem_val ; // default applies
            if (!val) continue ;
            if (type && type->IsRecordType()) {
                VhdlIdDef *elem_id = type->GetElementAt(i) ;
                elem_type = (elem_id) ? elem_id->Type() : 0 ;
            }
            VhdlExpression *expr = 0 ;
            expr = val->CreateConstantVhdlExpressionInternal(linefile, elem_type, new_dim, without_other) ;
            if (create_named) { // Create named type other => <expr>
                Array *choices = new Array(1) ;
                VhdlExpression *name = new VhdlOthers() ;
                name->SetLinefile(linefile) ;
                choices->InsertLast(name) ;
                expr = new VhdlElementAssoc(choices, expr) ;
                expr->SetLinefile(linefile) ;
            }
            assoc_list->Insert(expr) ;
        }
    } else {
       // Viper 6093: Create others => value as the
       // expression instead of a large aggregate
       // this resulted in large memory reduction
       // in some cases.
       assoc_list = new Array(1) ;
       VhdlExpression *expr = _default_elem_val ? _default_elem_val->CreateConstantVhdlExpressionInternal(linefile, elem_type, new_dim, without_other) : 0 ;
       if (expr) {
           // VIPER #6128: Set the linefile on the newly created node:
           VhdlExpression *choice = new VhdlOthers() ;
           choice->SetLinefile(linefile) ;
           Array *choices = new Array(1) ;
           choices->InsertLast(choice) ;
           expr = new VhdlElementAssoc(choices, expr) ;
           expr->SetLinefile(linefile) ;
           assoc_list->Insert(expr) ;
       }
    }
    // composite value to VhdlAggregate
    VhdlAggregate *expr = new VhdlAggregate(assoc_list) ;
    expr->SetAggregateType(type) ;
    expr->SetLinefile(linefile) ;

    return expr ;
}
VhdlExpression*
VhdlNonconstBit::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int /*dim*/, unsigned /*without_other*/)
{
    VhdlExpression *expr = 0 ;
    // Could be an enumeration type, or...
    if (type && type->IsEnumerationType()) {
        // Pick-up the right enumeration literal :
        VhdlIdDef *enum_id = type->GetEnumWithBitEncoding((_net==Gnd())?'0':(_net==Pwr())?'1':(_net==Z())?'Z':'X') ;
        if (!enum_id) {
            // Must be a two-value regular encoded regular enum type (boolean or so).... 0 is first, 1 is second
            enum_id = type->GetEnumAt((_net==Gnd())?0:1) ;
        }
        VERIFIC_ASSERT(enum_id) ; // If this fails, I have no idea what sort of enum type/value this is
        expr = new VhdlIdRef(enum_id) ;
    } else {
        // Otherwise : assume it is a integer type.
        verific_int64 val = Integer() ;
        expr = new VhdlInteger(val) ;
        expr->SetLinefile(linefile) ;
        if (type && type->IsPhysicalType()) {
            // For physical type generic create physical literal
            // The unit of the literal will be the lowest unit.
            Map *unit_list = type->GetTypeDefList() ;
            VhdlIdDef *physical_unit_id = unit_list ? (VhdlIdDef*)unit_list->GetValueAt(0) : 0 ;

            // Viper #4643: The value is normalized against the time resolution
            // set by the user (Position for the corresponding unit would be 1)
            MapIter mi ;
            VhdlIdDef *unit_id ;
            FOREACH_MAP_ITEM(unit_list, mi, 0, &unit_id) {
                if (!unit_id || unit_id->Position() != 1) continue ;
                physical_unit_id = unit_id ;
                break ;
            }

            if (physical_unit_id) {
                VhdlIdRef *unit = new VhdlIdRef(Strings::save(physical_unit_id->Name())) ;
                unit->SetLinefile(linefile) ;
                expr = new VhdlPhysicalLiteral(expr, unit) ;
            }
        }
    }
    expr->SetLinefile(linefile) ;
    return expr ;
}
VhdlExpression*
VhdlNonconst::CreateConstantVhdlExpressionInternal(linefile_type linefile, VhdlIdDef *type, int /*dim*/, unsigned /*without_other*/)
{
    VhdlExpression *expr = 0 ;
    // First, translate the value to an integer
    verific_int64 val = Integer() ;
    if (type && type->IsEnumerationType()) {
        if (OnehotEncoded()) { // Viper #7813
            unsigned idx = 0 ;
            while ((val = val/2) != 0) ++idx ;
            val = (unsigned) ((type->NumOfEnums() - 1) - idx) ;
        }

        VERIFIC_ASSERT(val >= 0) ;
        VhdlIdDef *enum_id = type->GetEnumAt((unsigned)val) ;
        VERIFIC_ASSERT(enum_id) ;
        expr = new VhdlIdRef(enum_id) ;
    } else {
        expr = new VhdlInteger(val) ;
        expr->SetLinefile(linefile) ;
        if (type && type->IsPhysicalType()) {
            // For physical type generic create physical literal
            // The unit of the literal will be the lowest unit.
            Map *unit_list = type->GetTypeDefList() ;
            VhdlIdDef *physical_unit_id = unit_list ? (VhdlIdDef*)unit_list->GetValueAt(0) : 0 ;

            // Viper #4643: The value is normalized against the time resolution
            // set by the user (Position for the corresponding unit would be 1)
            MapIter mi ;
            VhdlIdDef *unit_id ;
            FOREACH_MAP_ITEM(unit_list, mi, 0, &unit_id) {
                if (!unit_id || unit_id->Position() != 1) continue ;
                physical_unit_id = unit_id ;
                break ;
            }

            if (physical_unit_id) {
                VhdlIdRef *unit = new VhdlIdRef(Strings::save(physical_unit_id->Name())) ;
                unit->SetLinefile(linefile) ;
                expr = new VhdlPhysicalLiteral(expr, unit) ;
            }
        }
    }
    expr->SetLinefile(linefile) ;
    return expr ;
}
void VhdlAggregate::SetAggregateType(VhdlIdDef *t) { _aggregate_type = t ; }
// Generate elaboration creates block statement.
// Elaborate the block statement
// The prototype of this function is changed to avoid corruption introduced
// when we run elaboration on an restored design having for generate
VhdlBlockConfiguration*
VhdlIterScheme::ProcessBlockStatement(VhdlBlockStatement *blk_stmt, VhdlValue * /*runner*/, const VhdlBlockConfiguration *block_config, VhdlMapForCopy &old2new, VhdlDataFlow *df)
{
    if (!blk_stmt) return 0 ;
    VhdlScope *block_scope = blk_stmt->LocalScope() ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #5624: Create pseudo tree node for generate elaboration created block scope,
    // as instantiation can present inside block scope.
    VeriStaticElaborator *elab_obj = veri_file::GetStaticElabObj() ;
    VeriPseudoTreeNode *saved_curr_node = elab_obj ? elab_obj->GetCurrentNode():0 ;
    if (elab_obj) elab_obj->AddVhdlBlockNode(saved_curr_node, block_scope) ;
#endif
    // Set the global scope.
    VhdlScope *old_scope = _present_scope ;
    _present_scope = block_scope ;

    // Block configuration contains back pointer to the scope to be configured.
    // As the containing statements of the generate are copied to create a new block,
    // then the instantiations within the copied statements are to be bound with
    // this block configuration. So the back pointer of 'this' configuration
    // should now point to new block statement.
    // So copy the configuration to reset back pointers.
    VhdlBlockConfiguration *config = block_config ? new VhdlBlockConfiguration(*block_config, old2new) : 0 ;

    if (block_scope) block_scope->ElaborateDependencies() ;

    // VIPER 2012 : Elaborate block config one level
    if (config) config->Elaborate() ;

    // Set binding/configuration on the component instantiation labels of 'blk_stmt'.
    // if (config) config->SetLabels() ;

    // Elaborate declarations
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(blk_stmt->GetDeclPart(), i, decl) decl->Elaborate(df) ;

    // Elaborate statements
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(blk_stmt->GetStatements(), i, stmt) {
        if (stmt) stmt->Elaborate(df, config) ;
    }

    // Mark block statement as elaborated :
    blk_stmt->SetStaticElaborated() ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    if (elab_obj) elab_obj->SetCurrentNode(saved_curr_node) ; // Pop current node setting
#endif
    // Reset the global scope.
    _present_scope = old_scope ;
    return config ;
}

// Convert constraint to discrete range
// Required to set proper range constraint to unconstrained ports.
VhdlDiscreteRange *
VhdlRangeConstraint::CreateRangeNode(linefile_type linefile)
{
    VERIFIC_ASSERT(_left && _right) ;

    VhdlExpression *left = _left ? _left->CreateConstantVhdlExpressionInternal(linefile, 0, -1, 0) : 0 ;
    VhdlExpression *right = _right ? _right->CreateConstantVhdlExpressionInternal(linefile, 0, -1, 0) : 0 ;
    VhdlDiscreteRange *range = new VhdlRange(left, _dir, right) ;
    range->SetLinefile(linefile) ;
    return range ;
}
VhdlDiscreteRange *
VhdlArrayConstraint::CreateRangeNode(linefile_type linefile)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    return _index_constraint->CreateRangeNode(linefile) ;
}
VhdlDiscreteRange *
VhdlRecordConstraint::CreateRangeNode(linefile_type /*linefile*/)
{
    VERIFIC_ASSERT(0) ;
    return 0 ;
}

// If 'this' declaration declares an unconstrained port,
// create range constraint from 'VhdlConstraint'
void
VhdlInterfaceDecl::CreateRangeConstraint()
{
    VhdlIdDef *id = 0 ;
    if (!_id_list || !_id_list->Size()) return ;
    id = (VhdlIdDef*)_id_list->GetFirst() ;

    // Port is not unconstrained. Return
    if (!id->IsUnconstrained()) return ;

    // Get constraint of the identifier
    VhdlConstraint *constraint = id->Constraint() ;
    // Viper 5570. Do not create parse tree for unconstrained null range
    if (!constraint || constraint->IsUnconstrained() || constraint->IsNullRange()) return ;
    // Create subtype with proper range extrating from 'VhdlConstraint'
    _subtype_indication = _subtype_indication ? _subtype_indication->CreateSubtypeWithConstraint(constraint) : 0 ;
    if (_subtype_indication) (void) _subtype_indication->TypeInfer(0,0,VHDL_range,0) ;
    // Now the interface id becomes constrained, so change its '_is_unconstrained' field
    id->SetConstrained() ;
}

// Create type-mark extracting range from 'constraint'
VhdlSubtypeIndication *
VhdlExplicitSubtypeIndication::CreateSubtypeWithConstraint(VhdlConstraint *constraint)
{
    // range already here, not unconstrained, return
    if (_range_constraint) return this ;

    // Create range from 'constraint'
    VhdlDiscreteRange *range = constraint->CreateRangeNode(Linefile()) ;
    _range_constraint = range ;
    return this ;
}
VhdlSubtypeIndication *
VhdlIdRef::CreateSubtypeWithConstraint(VhdlConstraint *constraint)
{
    VhdlSubtypeIndication *type = 0 ;
    // Range is not provided. Create range
    if (!constraint->IsRecordConstraint() && !constraint->IsArrayConstraint()) {
        // create explicit subtype for range-constraint only
        type = new VhdlExplicitSubtypeIndication(0, this, constraint->CreateRangeNode(Linefile())) ;
    } else if (constraint->IsArrayConstraint()) {
        // Constraint is array constraint.
        // Create indexed name
        // VIPER 2849 : Pass type id so that association list size can be same as type's dimension count
        type = new VhdlIndexedName(this, constraint->CreateAssocList(Linefile(), _single_id)) ;
    } else {
        return this ;
    }
    type->SetLinefile(Linefile()) ;
    return type ;
}
VhdlSubtypeIndication *
VhdlSelectedName::CreateSubtypeWithConstraint(VhdlConstraint *constraint)
{
    VhdlSubtypeIndication *type = 0 ;
    if (!constraint->IsRecordConstraint() && !constraint->IsArrayConstraint()) {
        // create explicit subtype for range-constraint only
        type = new VhdlExplicitSubtypeIndication(0, this, constraint->CreateRangeNode(Linefile())) ;
    } else if (constraint->IsArrayConstraint()) {
        // Constraint is array constraint.
        // Create indexed name
        type = new VhdlIndexedName(this, constraint->CreateAssocList(Linefile(), _unique_id)) ;
    } else {
        return this ;
    }
    type->SetLinefile(Linefile()) ;
    return type ;
}
VhdlSubtypeIndication *
VhdlAttributeName::CreateSubtypeWithConstraint(VhdlConstraint *constraint)
{
    VhdlSubtypeIndication *type = 0 ;
    if (!constraint->IsRecordConstraint() && !constraint->IsArrayConstraint()) {
        // create explicit subtype for range constraint only
        type = new VhdlExplicitSubtypeIndication(0, this, constraint->CreateRangeNode(Linefile())) ;
    } else if (constraint->IsArrayConstraint()) {
        // Constraint is array constraint.
        // Create indexed name
        type = new VhdlIndexedName(this, constraint->CreateAssocList(Linefile(), _result_type)) ;
    } else {
        return this ;
    }
    type->SetLinefile(Linefile()) ;
    return type ;
}
VhdlSubtypeIndication *
VhdlIndexedName::CreateSubtypeWithConstraint(VhdlConstraint * /*constraint*/)
{
    // type with range constraint. Return this
    return this ;
}

// Create association list from constraint. This list will be
// range of the port declaration
// VIPER #2849 : Added type identifier as an argument of this routine. Length of
// created association list will be same as dimension count of type identifier
Array *
VhdlRangeConstraint::CreateAssocList(linefile_type linefile, VhdlIdDef * /*type*/)
{
    VERIFIC_ASSERT(_left && _right) ;

    Array *ret_array = new Array(1) ;
    ret_array->Insert(CreateRangeNode(linefile)) ;
    return ret_array ;
}
Array *
VhdlArrayConstraint::CreateAssocList(linefile_type linefile, VhdlIdDef *type)
{
    VERIFIC_ASSERT(_index_constraint && _element_constraint) ;

    Array *ret_array = new Array(1) ;
    unsigned dims = type ? type->Dimension() : 0 ;
    VhdlConstraint *ele = this ;
    while(dims) {
        VhdlConstraint *index = ele->IndexConstraint() ;
        VhdlDiscreteRange *range = index ? index->CreateRangeNode(linefile) : 0 ;
        ret_array->Insert(range) ;
        ele = ele->ElementConstraint() ;
        dims-- ;
    }
    return ret_array ;
}
Array *
VhdlRecordConstraint::CreateAssocList(linefile_type /*linefile*/, VhdlIdDef * /*type*/)
{
    // Unconstraint record type port not possible
    VERIFIC_ASSERT(0) ;
    return 0 ;
}
void
VhdlBlockStatement::DeleteUnusedComSpec()
{
    if (Message::ErrorCount()) {
        // If error occurs, it may be the case that some component instantiation statements
        // are not elaborated yet. So label of such instantiations has back pointers to
        // binding indication. Reset that back pointer before deleting configuration spec
        Map *decl_area = _local_scope ? _local_scope->DeclArea(): 0 ;
        MapIter mi ;
        VhdlIdDef *label ;
        FOREACH_MAP_ITEM(decl_area, mi, 0, &label) {
            if (!label || !label->IsLabel()) continue ;
            VhdlStatement *label_statement = label->GetStatement() ;
            if (!label_statement) continue ; // bad label
            label->SetBinding(0) ;
        }
    }
    // Delete configuration specification if any in declaration part
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl || !decl->IsConfigurationSpec()) continue ;
        delete decl ;
        _decl_part->Insert(i, 0) ;
    }
}
// Delete 'this'.
void
VhdlConfigurationSpec::DeleteUnusedComSpec(VhdlArchitectureBody *parent_arch)
{
    if (!parent_arch) return ;
    (void) parent_arch->ReplaceChildDecl(this, 0) ;
}

// Set statics.
// We use same constant nets for the whole static
// elaboration process.
// So create those at the start of static elaboration
void
VhdlNode::SetConstNets()
{
    if (!_nl && _gnd) return ; // VIPER 2334 :Don't set constant nets if they are already set
    _nl = 0 ;
    _gnd = new Net("_0") ;
    _pwr = new Net("_1") ;
    _x = new Net("_X") ;
    _z = new Net("_Z") ;
    //_named_path = 0 ;
}

// Reset statics.
void
VhdlNode::ResetConstNets()
{
    delete _gnd ;
    _gnd = 0 ;
    delete _pwr ;
    _pwr = 0 ;
    delete _x ;
    _x = 0 ;
    delete _z ;
    _z = 0 ;
}

// Delete binding information from the statically elaborated
// configuration declarations, as after static elaboration all
// component instantiations are converted to direct entity
// instantiation with the help of binding information from
// configuration declarations.
void
VhdlLibrary::DeleteElaboratedBindings() const
{
    MapIter mi ;
    VhdlPrimaryUnit *prim_unit ;
    // Iterate over all primary units of the library
    FOREACH_VHDL_PRIMARY_UNIT(this, mi, prim_unit) {
        if (!prim_unit) continue ;
        VhdlIdDef *id = prim_unit->Id() ;
        if (id && id->IsConfiguration() && prim_unit->IsStaticElaborated()) {
            // Primary unit is configuration and statically executed.
            // Delete binding information
            prim_unit->DeleteBindings() ;
        }
    }
}

// Delete binding information
void
VhdlConfigurationDecl::DeleteBindings()
{
    if (_compile_as_blackbox) return ; // Do not delete body of black-box
    if (_block_configuration) _block_configuration->DeleteBindings() ;
}

// Delete use clauses declared within the configuration.
// Delete binding information from all configuration items
void
VhdlBlockConfiguration::DeleteBindings()
{
    unsigned i ;
    VhdlUseClause *use_cl ;
    FOREACH_ARRAY_ITEM(_use_clause_list, i, use_cl) delete use_cl ;
    delete _use_clause_list ;
    _use_clause_list = 0 ;

    VhdlConfigurationItem *ci ;
    FOREACH_ARRAY_ITEM(_configuration_item_list, i, ci) delete ci ;
    delete _configuration_item_list ;
    _configuration_item_list = 0 ;
}

// Delete binding information from component configuration.
void
VhdlComponentConfiguration::DeleteBindings()
{
    delete _binding ;
    _binding = 0 ;
    if (_block_configuration) _block_configuration->DeleteBindings() ;
}

VhdlBlockStatement::VhdlBlockStatement(Array *decl_part, Array *statements, VhdlScope *local_scope)
  : VhdlStatement(),
    _guard(0),
    _generics(0),
    _ports(0),
    _decl_part(decl_part),
    _statements(statements),
    _local_scope(local_scope),
    _guard_id(0),
    _is_static_elaborated(0),
    _is_elab_created_empty(0),
    _is_gen_elab_created(0)
{ }

// VIPER 2375 : Split the generic list of component declaration so that each generic
// declaration contains only one generic and change initial values of generics according to argument
// specific generic map (from component instantiation)
void VhdlComponentDecl::SplitAndAssociateGenerics(const Array *actual_generics)
{
    // Convert declaration with array of identifiers to
    // declarations with single identifier, so that different
    // generic can obtain different initial value.
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    if (actual_generics && _generic_clause) {
        Array *new_generic_clause = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
            if (!decl) continue ;
            decl->Split(new_generic_clause) ;
            _generic_clause->Insert(i, 0) ;
        }
        delete _generic_clause ;
        _generic_clause = new_generic_clause ;
    }

    // Set proper values to generic ids.
    AssociateGenerics(actual_generics) ;

    // VIPER #2661 : Set constraint to unconstrained generics
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (!decl) continue ;
        decl->CreateRangeConstraint() ;
    }

    // Set/override initial value of each generic
    if (actual_generics && _generic_clause) {
        FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
            if (!decl) continue ;
            if (decl->IsInterfaceTypeDecl() || decl->IsInterfaceSubprogDecl() || decl->IsInterfacePackageDecl()) continue ;
            Array *ids = decl->GetIds() ;
            VERIFIC_ASSERT(ids && ids->Size() == 1) ;
            VhdlIdDef *id = (VhdlIdDef*)ids->GetFirst() ;
            // Get the value of the generic
            VhdlValue *id_val = id->Value() ;
            // Create expression from the value
            VhdlExpression *new_expr = id_val ? id_val->CreateConstantVhdlExpressionInternal(decl->Linefile(), id->BaseType(), -1, 0) : 0 ;
            if (!new_expr) continue ;
            // VIPER #2661 :Type infer the created initial value
            // For vhdl-2008 do not call TypeInfer during elaboration as proper context
            // is not available to call scope->FindXXXX
            //(void) new_expr->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ;
            // Set initial value to the generic
            decl->SetInitAssign(new_expr) ;
        }
    }
}
char *VhdlTreeNode::CreateUniqueUnitName(const char *prefix, const VhdlLibrary *lib, const VhdlScope *scope, unsigned ignore_copied)
{
    if (!prefix || !lib) return 0 ;
    // Created name should be unique in library 'lib' and in scope 'scope'
    // First create a made up name appending 'uniq0' to existing name
    //char *new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(prefix, "_uniq_0") ;
    // VIPER #7026 : Do not call 'ConcatAndCreateLegalIdentifier' within
    // loop, check separately whether 'prefix' is escaped or ilegal
    char *new_prefix = Strings::save(prefix) ;
    unsigned is_escaped = 0 ;
    if (new_prefix[0] == '\\' && new_prefix[Strings::len(new_prefix)-1] == '\\') {
        is_escaped = 1 ;
        char *copy_name = new_prefix ;
        char *tmp = copy_name ;
        tmp[Strings::len(tmp)-1] = '\0' ;
        tmp++ ;
        new_prefix = Strings::save(tmp) ;
        Strings::free(copy_name) ;
    }
    unsigned is_legal_id = IsLegalIdentifier(prefix) ;
    unsigned long count = 0 ;
    char *old_key = 0 ;
    if (RuntimeFlags::GetVar("vhdl_uniquify_all_instances")) {
        // VIPER #7054 : Instead of starting with suffix _uniq_0 always to create
        // unique name, store the last created unique number and start searching
        // just incrementing that
        MapItem *item = _unit_num_map ? _unit_num_map->GetItem(new_prefix): 0 ;
        old_key = item ? (char*)item->Key(): 0 ;
        count = item ? (unsigned long)item->Value(): 0 ;
    } // vhdl_uniquify_all_instances

    //if (count > 0) count++ ; // Increment if something is in map
    char start_num[40] ; sprintf(start_num, "%d", (unsigned)count) ;
    if (!_unit_num_map) _unit_num_map = new Map(STRING_HASH) ;
    char *new_entity_name = (is_escaped || !is_legal_id) ?  Strings::save("\\", new_prefix, "_uniq_", start_num, "\\") : Strings::save(new_prefix, "_uniq_", start_num) ;
    //unsigned count = 0 ;
    VhdlPrimaryUnit *existing_unit = 0 ;
    VhdlIdDef *exist_id = 0 ;
    do { // Begin loop
        existing_unit = lib->GetPrimUnit(new_entity_name) ;
        // If existing unit is copied unit and 'ignore_copied' is 1, don't consider copied units
        if (existing_unit && existing_unit->GetOriginalUnitName() && ignore_copied) existing_unit = 0 ;

        // VIPER #7026: Check in scope when 'existing_unit' is not present
        if (!existing_unit) {
            // Check if 'scope' contains 'new_entity_name'
            exist_id = scope ? scope->FindSingleObjectLocal(new_entity_name) : 0 ;
        }

        if (exist_id) {
            // Don't consider copied components if 'ignore_copied' is 1
            VhdlComponentDecl *component = exist_id->GetComponentDecl() ;
            if (component && component->GetOriginalComponentName() && ignore_copied) exist_id = 0 ;
        }
        if (existing_unit || exist_id) {
            // Entity with 'new_entity_name' is already in library
            // Or 'scope' contains 'new_entity_name'
            char *tmp = new_entity_name ; // Store created name
            count++ ; // Increment the count
            char num[40] ;
            sprintf(num, "_uniq_%d", (unsigned)count) ;

            //new_entity_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(prefix, num) ; // Create a new name with new suffix i.e uniq1, uniq2....
            new_entity_name = (is_escaped || !is_legal_id) ? Strings::save("\\", new_prefix, num, "\\") : Strings::save(new_prefix, num) ;
            Strings::free(tmp) ; // Free previously allocated name
        }
    } while(existing_unit || exist_id) ;
    if (RuntimeFlags::GetVar("vhdl_uniquify_all_instances")) {
        (void) _unit_num_map->Insert(Strings::save(new_prefix), (void*)(count+1), 1) ;
        Strings::free(old_key) ;
    } // vhdl_uniquify_all_instances
    Strings::free(new_prefix) ; // Memory leak fix
    return new_entity_name ;
}
// Create equivalent component declaration from this entity. First argument
// is the design scope, where created component is to be added. Second
// argument is the actual component which will behave as original component
// of this created component. This will help us to check whether a component
// is already created for a component instantiation by static elaborator.
VhdlIdDef *VhdlEntityDecl::CreateComponent(VhdlScope *upper_scope, VhdlIdDef *actual_component, Array *actual_generics, Array *actual_ports)
{
    if (!upper_scope || !actual_component || !_id) return 0 ; // Can't create component

    // Create new component id with new name
    VhdlComponentId *new_id = new VhdlComponentId(Strings::save(_id->OrigName())) ;
    new_id->SetLinefile(_id->Linefile()) ;

    // Declare the component id in the scope of architecture
    if (!upper_scope->Declare(new_id)) { // Identifier already exists
        delete new_id ; // Delete created component id
        return 0 ;
    }
    VhdlScope *save_scope = _present_scope ; // Push present scope
    _present_scope = upper_scope ; // Switch to Secondary Uhit scope

    // Create component's local scope : child of architecture scope
    VhdlScope *component_scope = new VhdlScope(upper_scope, new_id) ;

    VhdlMapForCopy old2new ;
    // Now create generic ids from this entity with same name and value
    Array *generics = 0 ;
    if (_is_verilog_module) {
        generics = CreateInterfaceDecls(actual_component, actual_generics, old2new, 1 /* generics*/) ;
        // Set/override initial value of each generic similar to SplitAndAssociateGenerics : Viper 6769
        unsigned i ;
        VhdlInterfaceDecl *g_decl ;
        FOREACH_ARRAY_ITEM(generics,i,g_decl) {
            if (!g_decl) continue ;
            if (g_decl->IsInterfaceTypeDecl() || g_decl->IsInterfaceSubprogDecl() || g_decl->IsInterfacePackageDecl()) continue ;
            Array *ids = g_decl->GetIds() ;
            VERIFIC_ASSERT(ids && ids->Size() == 1) ;
            VhdlIdDef *id = (VhdlIdDef*)ids->GetFirst() ;
            // Get the value of the generic
            VhdlValue *id_val = id->Value() ;
            // Create expression from the value
            VhdlExpression *new_expr = id_val ? id_val->CreateConstantVhdlExpressionInternal(g_decl->Linefile(), id->BaseType(), -1, 0) : 0 ;
            if (!new_expr) continue ;
            // VIPER #2661 :Type infer the created initial value
            // For vhdl-2008 do not call TypeInfer during elaboration as proper context
            // is not available to call scope->FindXXXX
            //(void) new_expr->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ;
            // Set initial value to the generic
            g_decl->SetInitAssign(new_expr) ;
        }
    } else {
        generics = old2new.CopyDeclarationArray(_generic_clause) ;
    }

    // VIPER #3091 : Instantiated component may have different port type than associated
    // entity. We are here creating elaborated component from associated entity. So it may
    // be the case that subtype indication of associated entity have used some package defined
    // objects which are not directly visible from instantiating entity. So we have to made
    // those identifier references explicit i.e if type is defined as
    //  port (i1 : in some_type ;....
    // and 'some_type' is in package 'P' of library 'work', change the declaration to
    // port (i1 : in work.P.some_type; ...

    // Now declare copied generics to component scope
    unsigned i, j ;
    VhdlInterfaceDecl *decl ;
    VhdlIdDef *id ;

    Map lib_ids(STRING_HASH) ;
    VhdlIdDef *containing_unit = upper_scope->GetContainingDesignUnit() ;
    VhdlScope *containing_unit_scope = (containing_unit) ? containing_unit->LocalScope() : upper_scope ;
    FOREACH_ARRAY_ITEM(generics, i, decl) {
        Array *ids = decl ? decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (component_scope && id) (void) component_scope->Declare(id) ;
        }
        // Get subtype indication
        VhdlSubtypeIndication *type = decl ? decl->GetSubtypeIndication(): 0 ;
        // VIPER #3091 : Convert out of scope identifier references to selected names:
        VhdlSubtypeIndication *new_type = 0 ;
        if (type) new_type = type->ChangeOutOfScopeRefs(component_scope, _local_scope, containing_unit_scope, &lib_ids) ;
        if (new_type && decl) {
            (void)decl->ReplaceChildSubtypeIndication(type, new_type) ;
            // VIPER 4929. _sub_type_indication was not set
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (id) id->SetSubtypeIndication(new_type) ;
            }
#endif
        }
    }

    Array *ports = 0 ;
    if (_is_verilog_module) {
        ports = CreateInterfaceDecls(actual_component, actual_ports, old2new, 0 /* ports*/) ;
        // Set range of ports similar to SplitAndAssociatePorts: Viper 6769
        unsigned k ;
        VhdlInterfaceDecl *p_decl ;
        FOREACH_ARRAY_ITEM(ports, k, p_decl) {
            if (!p_decl) continue ;
            p_decl->CreateRangeConstraint() ;
        }
    } else {
        // Now create port ids from this entity with same name, type and value
        ports = old2new.CopyDeclarationArray(_port_clause) ;
    }

    // Now declare copied port ids to component scope
    FOREACH_ARRAY_ITEM(ports, i, decl) {
        Array *ids = decl ? decl->GetIds() : 0 ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (component_scope && id) (void) component_scope->Declare(id) ;
        }
        // Get subtype indication
        VhdlSubtypeIndication *type = decl ? decl->GetSubtypeIndication(): 0 ;
        // Convert out of scope identifier references :
        VhdlSubtypeIndication *new_type = 0 ;
        if (type) new_type = type->ChangeOutOfScopeRefs(component_scope, _local_scope, containing_unit_scope, &lib_ids) ;
        if (new_type && decl) {
            (void)decl->ReplaceChildSubtypeIndication(type, new_type) ;
            (void) new_type->TypeInfer(0,0,VHDL_range,0) ;
            type = new_type ;
            // VIPER 4929. _sub_type_indication was not set
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (id) id->SetSubtypeIndication(new_type) ;
            }
#endif
        }
    }

    // Now create component declaration
    VhdlComponentDecl *new_decl = new VhdlComponentDecl(new_id, generics, ports, component_scope) ;
    new_decl->SetLinefile(Linefile()) ;

    // New component is created for component 'actual_component' i.e instantiation
    // of component 'actual_component' can be replaced by instantiation of new
    // component. So set 'actual_component''s name as original name of created
    // component
    VhdlComponentDecl *orig_comp_decl = actual_component->GetComponentDecl() ;
    if (orig_comp_decl) {
        new_decl->SetOriginalComponentName(orig_comp_decl->GetOriginalComponentName() ? orig_comp_decl->GetOriginalComponentName(): actual_component->OrigName()) ;
        new_decl->SetLinefile(orig_comp_decl->Linefile()) ;
    }

    // VIPER #4266 : Create library clause for each library identifier used to
    // replace out of scope identifier references with full path name
    MapIter mi ;
    VhdlIdDef *lib_id ;
    Array *lib_id_defs = 0 ;
    FOREACH_MAP_ITEM(&lib_ids, mi, 0, &lib_id) {
        if (!lib_id) continue ;
        if (!lib_id_defs) lib_id_defs = new Array(2) ;
        lib_id_defs->InsertLast(lib_id) ;
    }
    // Now add this library clause to containing design unit
    if (lib_id_defs && lib_id_defs->Size()) {
        VhdlLibraryClause *lib_clause = new VhdlLibraryClause(lib_id_defs, 0) ;
        lib_clause->SetLinefile(new_decl->Linefile()) ;
        VhdlDesignUnit *du = containing_unit ? containing_unit->GetDesignUnit(): 0 ;
        if (du) {
            du->AddContextClause(lib_clause) ;
        } else {
            delete lib_clause ;
        }
    } else {
        delete lib_id_defs ; //JJ: clean mem leak
    }

    new_decl->AddAfter(orig_comp_decl, upper_scope) ;
    /*
    // Add created component to declarative region of block/architecture/package
    // where 'actual_component' is declared
    // Get owner of 'upper_scope'
    VhdlIdDef *owner_id = upper_scope->GetOwner() ;
    // 'upper_scope' can be scope of VhdlPackageDecl /VhdlSecondaryUnit/ VhdlBlockStatement
    // Check if container of 'actual_component' is primary unit. If so add
    // new component decl there
    VhdlPrimaryUnit *prim_unit = owner_id ? owner_id->GetPrimaryUnit(): 0 ;
    unsigned is_added = 0 ;
    if (prim_unit) is_added = prim_unit->InsertAfterDecl(orig_comp_decl, new_decl) ;

    if (!is_added) { // Check if container of 'actual_component' is secondary unit
        VhdlSecondaryUnit *sec_unit = owner_id ? owner_id->GetSecondaryUnit() : 0 ;
        if (sec_unit) is_added = sec_unit->InsertAfterDecl(orig_comp_decl, new_decl) ;
        if (sec_unit && !is_added) {
            // We want to add new component declaration in secondary unit, but component
            // is actually declared in package. So add component declaration using 'Add'
            sec_unit->AddDeclaration(new_decl) ;
        }
    }
    if (!is_added) { // Check if container of 'actual_component' is block
        VhdlStatement *stmt = owner_id ? owner_id->GetStatement() : 0 ;
        if (stmt) is_added = stmt->InsertAfterDecl(orig_comp_decl, new_decl) ;
    }
    */

    _present_scope = save_scope ; // Pop present scope
    return new_id ; // Return created component identifier
}

void VhdlComponentDecl::SetOriginalComponentName(const char *name)
{
    Strings::free(_orig_component) ; // Delete old name
    _orig_component = Strings::save(name) ;
}

// VIPER #2877 :Copy the generic/port association list so that formal part of association
// list points to interface ids of this entity
Array  *VhdlPrimaryUnit::CopyInterfaceAssociationList(const Array *assoc_list) const
{
    if (!assoc_list || !_local_scope) return 0 ; // Nothing to copy
    Array *new_assoc_list = new Array(assoc_list->Size()) ; // Create new list
    VhdlDiscreteRange *assoc ;
    VhdlDiscreteRange *new_assoc ;
    VhdlName *formal ;
    VhdlName *copied_formal ;
    VhdlMapForCopy copy_obj ;
    VhdlIdDef *id ;
    VhdlIdDef *interface_id ;
    unsigned i ;

    FOREACH_ARRAY_ITEM(assoc_list, i, assoc) { // Iterate over association list
        if (!assoc) continue ;
        new_assoc = assoc->CopyDiscreteRange(copy_obj) ; // Copy association
        new_assoc_list->InsertLast(new_assoc) ; // Insert in new list
        if (assoc->IsAssoc()) { // Find formal identifier for named association
            formal = assoc->FormalPart() ;
            id = assoc->FindFullFormal() ;
            if (!id) {
                id = formal ? formal->FindFormal(): 0 ;
                if (!id) continue ;
            }
            interface_id = _local_scope->FindSingleObjectLocal(id->Name()) ; // Find formal in this entity
            copied_formal = new_assoc->FormalPart() ;
            if (copied_formal && interface_id) {
                // VIPER #3994 : If formal is a selected name, set formal
                // identifier in prefix part
                copied_formal->SetFormal(interface_id) ;
            }
        }
    }
    return new_assoc_list ;
}

VhdlName * VhdlIdRef::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    if (!_single_id || !ref_scope || !def_scope) return 0 ; // Can't check anything without identifier

    if (ref_scope->FindSingleObject(Name()) == _single_id) return 0 ; // '_single_id' is visible in referenced scope.

    // '_single_id' is not visible in referenced location, have to use selected name.
    // Try to find out where it is defined from using list of defined scope.
    Set *using_list = def_scope->GetUsing() ;

    SetIter si ;
    VhdlScope *other_scope ;
    VhdlName *ret_name = 0 ;
    FOREACH_SET_ITEM(using_list, si, &other_scope) {
        if (!other_scope) continue ;
        // Check if it is defined in 'other_scope'
        if (other_scope->FindSingleObjectLocal(Name()) != _single_id) continue ; // Not in 'other_scope'
        VhdlIdDef *design = other_scope->GetContainingPrimaryUnit() ;
        if (!design || !design->IsPackage()) continue ;
        VhdlPrimaryUnit *prim_unit = design->GetPrimaryUnit() ;
        VhdlLibrary *lib = prim_unit ? prim_unit->GetOwningLib() : 0 ;
        if (!lib) continue ; // Cann't extract unit/library information
        VhdlLibraryId *lib_id = (VhdlLibraryId*)lib_ids->GetValue(lib->Name()) ;
        if (!lib_id) {
            lib_id = new VhdlLibraryId(Strings::save(lib->Name())) ;
            (void) container_scope->Declare(lib_id) ;
            (void) lib_ids->Insert(lib_id->Name(), lib_id) ;
        }
        VhdlIdRef *lib_name_node = new VhdlIdRef(lib_id) ;
        lib_name_node->SetLinefile(Linefile()) ;
        VhdlIdRef *name = new VhdlIdRef(design) ;
        name->SetLinefile(Linefile()) ;
        ret_name = new VhdlSelectedName(lib_name_node, name) ;
        // VIPER #5967 : Set resolved identifier so that EvaluateConstraint works on modified type
        ret_name->SetId(design) ;
        ret_name->SetLinefile(Linefile()) ;
        name = new VhdlIdRef(_single_id) ;
        name->SetLinefile(Linefile()) ;
        ret_name = new VhdlSelectedName(ret_name, name) ;
        // VIPER #5967 : Set resolved identifier so that EvaluateConstraint works on modified type
        ret_name->SetId(_single_id) ;
        ret_name->SetLinefile(Linefile()) ;
        return ret_name ;
    }
    return 0 ;
}
VhdlName *VhdlIndexedName::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)

{
    // Change type information if needed :
    VhdlName *new_prfefix = _prefix ? _prefix->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids) : 0 ;
    if (new_prfefix) {
        delete _prefix ;
        _prefix = new_prfefix ;
    }
    // Iterate over association list and change identifier references if needed :
    VhdlDiscreteRange *range, *new_range ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, range) {
        new_range = (range) ? range->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
        if (new_range) {
            _assoc_list->Insert(i, new_range) ;
            delete range ;
        }
    }
    return 0 ;
}
VhdlName *VhdlSignaturedName::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    VhdlName *new_name = (_name) ? _name->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_name) {
        delete _name ;
        _name = new_name ;
    }
    return 0 ;
}
VhdlName *VhdlAttributeName::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    VhdlName *new_prefix = (_prefix) ? _prefix->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_prefix) {
        delete _prefix ;
        _prefix = new_prefix ;
    }
    return 0 ;
}
VhdlName *VhdlExplicitSubtypeIndication::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    // Change type information if needed :
    VhdlName *new_type_mark = (_type_mark) ? _type_mark->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_type_mark) {
        delete _type_mark ;
        _type_mark = new_type_mark ;
    }
    // Change range information if needed :
    if (_range_constraint) (void) _range_constraint->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids) ;

    return 0 ;
}
VhdlName *VhdlRange::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    VhdlExpression *new_left = (_left) ? _left->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_left) {
        delete _left ;
        _left = new_left ;
    }
    VhdlExpression *new_right = (_right) ? _right->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_right) {
        delete _right ;
        _right = new_right ;
    }
    return 0 ;
}
VhdlName *VhdlOperator::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    VhdlName *new_left = (_left) ? _left->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_left) (void) ReplaceChildExpr(_left, new_left) ;
    VhdlName *new_right = (_right) ? _right->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_right) (void) ReplaceChildExpr(_right, new_right) ;
    return 0 ;
}
VhdlName *VhdlAllocator::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    VhdlName *new_type = (_subtype) ? _subtype->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_type) (void) ReplaceChildExpr(_subtype, new_type) ;
    return 0 ;
}
VhdlName *VhdlQualifiedExpression::ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids)
{
    VhdlName *new_name = (_prefix) ? _prefix->ChangeOutOfScopeRefs(ref_scope, def_scope, container_scope, lib_ids): 0 ;
    if (new_name) {
        delete _prefix ;
        _prefix = new_name ;
    }
    return 0 ;
}

void VhdlBlockPorts::StaticElaborate()
{
    // VIPER #3103 : Process port map aspect to do following transformations :
    // 1. Convert positional association to named association.
    // 2. If actual is open, replace it with default value from the formal.

    Array *new_port_map_aspect = 0  ; // New port association list
    Set id_considered(POINTER_HASH) ; // Set of port identifiers considered
    VhdlDiscreteRange *assoc ;
    VhdlIdDef *id ;
    unsigned i ;
    linefile_type curr_linefile = 0 ;
    // Convert positional association to named one. Ignore open connections
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, assoc) {
        if (!assoc) continue ;
        VhdlExpression *actual = assoc->ActualPart() ;
        if (!actual || actual->IsOpen()) continue ; // Ignore open

        // Find the formal.
        if (assoc->IsAssoc() ) { // Named association :
            id = assoc->FindFormal() ;
        } else { // Positional association :
            id = _block_label->GetPortAt(i) ;
        }
        if (!id) continue ;
        curr_linefile = actual->Linefile() ;
        (void) id_considered.Insert(id) ; // This port is already considered
        if (!new_port_map_aspect) new_port_map_aspect = new Array(_block_label->NumOfPorts()) ;
        VhdlMapForCopy old2new ;
        VhdlDiscreteRange *new_assoc = 0 ;
        if (assoc->IsAssoc()) {
            new_assoc = assoc->CopyDiscreteRange(old2new) ;
        } else {
            VhdlIdRef *formal_part = new VhdlIdRef(id) ;
            formal_part->SetLinefile(curr_linefile) ;
            new_assoc = new VhdlAssocElement(formal_part, actual->CopyExpression(old2new)) ;
            new_assoc->SetLinefile(curr_linefile) ;
        }
        new_port_map_aspect->Insert(new_assoc) ;
    }
    // Now association list is already processed, we will consider default-values
    if (!new_port_map_aspect) new_port_map_aspect = (_port_clause) ? new Array(_block_label->NumOfPorts()): 0 ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
        if (decl) decl->DefaultValues(&id_considered, new_port_map_aspect, _block_label, &id_considered, curr_linefile ? curr_linefile: Linefile() ) ;
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, assoc) delete assoc ;
    delete _port_map_aspect ;
    _port_map_aspect = new_port_map_aspect ;
}
// Evaluate overridden value of generics from generic association and set that
// as initial value of generics. Also delete the processed generic association list.
void VhdlBlockGenerics::StaticElaborate()
{
    // Convert declaration with array of identifiers to declarations with single
    // identifier, so that different generic can obtain different initial value.
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    if (_generic_clause) {
        Array *new_generic_clause = new Array(_generic_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
            if (!decl) continue ;
            decl->Split(new_generic_clause) ;
            _generic_clause->Insert(i, 0) ;
        }
        delete _generic_clause ;
        _generic_clause = new_generic_clause ;
    }
    // Set constraint to unconstrained generics (Is Needed?)
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        if (decl) decl->CreateRangeConstraint() ;
    }
    // Set/override initial value of each generic
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        if (!decl) continue ;
        Array *ids = decl->GetIds() ;
        VhdlIdDef *id = (ids) ? (VhdlIdDef*)ids->GetFirst(): 0 ;
        if (!id) continue ;
        VhdlValue *id_val = id->Value() ; // Get the value of the generic
        VhdlExpression *new_expr = id_val ? id_val->CreateConstantVhdlExpressionInternal(decl->Linefile(), id->BaseType(), -1, 0) : 0 ; // Create expression from the value
        if (!new_expr) continue ;
        // For vhdl-2008 do not call TypeInfer during elaboration as proper context
        // is not available to call scope->FindXXXX
        //(void) new_expr->TypeInfer(id->BaseType(), 0, VHDL_READ, 0) ;
        decl->SetInitAssign(new_expr) ; // Set initial value to the generic
    }
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) delete assoc ;
    delete _generic_map_aspect ;
    _generic_map_aspect = 0 ;
}

// VIPER #3510, #3513 : For a component instantiation range of instantiated component can be different
// from range of instantiated entity. So when we are converting component instantiation to direct entity
// instantiation and there are partial association in port map, indexing of formal part should refer to
// ranges of instantiated entity. Do that here.
void VhdlTreeNode::ProcessFormal(VhdlName *formal_part, const VhdlIdDef *comp_port, const VhdlIdDef *entity_id)
{
    if (!formal_part || !comp_port || !entity_id) return ; // Nothing can be done without these
    if (comp_port == entity_id) return ; // Both are same, no need to change anything

    VhdlExpression *formal = formal_part ;
    if (formal_part->IsFunctionCall() || formal_part->IsTypeConversion()) {
        // A conversion function in the formal. Take its argument
        Array *func_arg_list = formal_part->GetAssocList() ;
        if (!func_arg_list || func_arg_list->Size() != 1) return ;
        VhdlDiscreteRange *assoc = (VhdlDiscreteRange *) func_arg_list->At(0) ;
        formal = (assoc) ? assoc->ActualPart(): 0 ;
    }
    VhdlConstraint *inst_unit_constraint = entity_id->Constraint() ;
    VhdlConstraint *com_constraint = comp_port->Constraint() ;
    // Modify indices if any in formal comparing constraint of component port with entity port
    if(formal) formal->ModifyIndex(inst_unit_constraint, com_constraint) ;
}
// Routine to modify range/index of indexed name in formal part according to the range of instantiated entity port :
void VhdlExpression::ModifyIndex(VhdlConstraint *&inst_unit_constraint, VhdlConstraint *&comp_constraint)
{
    VhdlName *prefix = GetPrefix() ;
    if (prefix) prefix->ModifyIndex(inst_unit_constraint, comp_constraint) ;
}
void VhdlIndexedName::ModifyIndex(VhdlConstraint *&inst_unit_constraint, VhdlConstraint *&comp_constraint)
{
    if (!_assoc_list || !inst_unit_constraint || !comp_constraint || (!_is_array_index && !_is_array_slice)) return ; // It is not bit/part select

    unsigned i ;
    VhdlDiscreteRange *index ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, index) { // Iterate over indices
        if (!index) continue ;
        if (index->IsRange()) { // Slice
            VhdlConstraint *range = index->EvaluateConstraint(0, 0) ; // evaluate range
            if (!range) continue ; // non-constant index
            // Get the actual position of the values in the constraint of component
            unsigned left_pos = comp_constraint->PosOf(range->Left()) ;
            unsigned right_pos = comp_constraint->PosOf(range->Right()) ;

            // Get the value of a particular position in the range
            VhdlValue *left = inst_unit_constraint->ValueOf(left_pos) ;
            VhdlValue *right = inst_unit_constraint->ValueOf(right_pos) ;

            // Create expression from value
            VhdlExpression *new_lhs = (left) ? left->CreateConstantVhdlExpressionInternal(index->Linefile(), 0, -1, 0): 0 ;
            VhdlExpression *new_rhs = (right) ? right->CreateConstantVhdlExpressionInternal(index->Linefile(), 0, -1, 0): 0 ;
            // Create new range
            if (new_lhs && new_rhs) {
                VhdlRange *new_range = new VhdlRange(new_lhs, inst_unit_constraint->Dir(), new_rhs) ;
                new_range->SetLinefile(index->Linefile()) ;
                delete index ;
                _assoc_list->Insert(i, new_range) ; // Modify range
            } else {
                delete new_lhs ;
                delete new_rhs ;
            }
            delete range ;
            delete left ;
            delete right ;
        } else {
            VhdlValue *element_pos = index->Evaluate(0,0,0) ;
            if (!element_pos || !element_pos->IsConstant()) {
                delete element_pos ;
                continue ;
            }
            unsigned pos = comp_constraint->PosOf(element_pos) ;
            delete element_pos ;
            // Get the value of a particular position in the range
            VhdlValue *new_idx = inst_unit_constraint->ValueOf(pos) ;

            // Update constraint according to indexing :
            comp_constraint = comp_constraint->ElementConstraintAt(pos) ;
            inst_unit_constraint = inst_unit_constraint->ElementConstraintAt(inst_unit_constraint->PosOf(new_idx)) ;

            // Create new index expression :
            VhdlExpression *new_idx_expr = (new_idx) ? new_idx->CreateConstantVhdlExpressionInternal(index->Linefile(), 0, -1, 0): 0 ;
            if (new_idx_expr) {
                delete index ;
                _assoc_list->Insert(i, new_idx_expr) ;
            }
            delete new_idx ;
        }
    }
}

// Value of a particular position in range
VhdlValue *VhdlConstraint::ValueOf(unsigned /*pos*/) const { return 0 ; }
VhdlValue *VhdlArrayConstraint::ValueOf(unsigned pos) const { return _index_constraint->ValueOf(pos) ; }
VhdlValue *VhdlRangeConstraint::ValueOf(unsigned pos) const
{
    if (!_left || !_right) return 0 ;

    if (IsEnumRange()) { // Enum range
        VhdlIdDef *enum_id = _left->GetEnumId() ;
        verific_uint64 enum_pos = enum_id ? enum_id->Position() : 0 ;
        VhdlIdDef *type = enum_id ? enum_id->Type() : 0 ;
        VhdlIdDef *new_enum_id = 0 ;
        if (_dir == VHDL_to) {
            new_enum_id = type ? type->GetEnumAt((unsigned)enum_pos + pos): 0 ;
        } else {
            new_enum_id = type ? type->GetEnumAt((unsigned)enum_pos - pos): 0 ;
        }
        return (new_enum_id) ? new VhdlEnum(new_enum_id): 0 ;
    }
    if (IsRealRange()) { // Real range
        if (_dir == VHDL_to) {
            return new VhdlDouble(_left->Real() + pos) ;
        } else {
            return new VhdlDouble(_left->Real() - pos) ;
        }
    }
    // Integer range :
    if (_dir == VHDL_to) {
        return new VhdlInt(_left->Integer() + pos) ;
    } else {
        return new VhdlInt(_left->Integer() - pos) ;
    }
    return 0 ;
}

/****************** Constraint to subtype ************************************/
VhdlSubtypeIndication* VhdlConstraint::CreateSubtype(VhdlIdDef * /*base_type*/, VhdlTreeNode * /*from*/){
    return 0 ;
}

VhdlSubtypeIndication* VhdlArrayConstraint::CreateSubtype(VhdlIdDef *base_type, VhdlTreeNode *from)
{
    if (!base_type || !from) return 0 ; // Cannot proceed without these
    // Do not create type for unconstrained array constraint, constraint with null
    // range and if base type is a constrained array type :
    if (IsUnconstrained() || base_type->IsConstrainedArrayType() || IsNullRange()) return 0 ;

    // Create evaluated range list from constraint
    Array *index_list = CreateAssocList(from->Linefile(), base_type) ;
    if (!index_list) return 0 ;

    // Base type :
    VhdlName *base_name = new VhdlIdRef(base_type) ;
    base_name->SetLinefile(from->Linefile()) ;

    // Apply ranges to create subtype
    VhdlSubtypeIndication *type = new VhdlIndexedName(base_name, index_list) ;
    type->SetLinefile(from->Linefile()) ;
    return type ;
}

// VIPER #4437 : API to generate name of block from statement label and iteration value :
// Check name conflict also.
char * VhdlIterScheme::CreateBlockName(const VhdlIdDef *stmt_label, const VhdlValue *iter_val, const VhdlScope *gen_scope, const Map *new_block_ids)
{
    if (!stmt_label) return 0 ; // No label to create name
    char *ret_name = 0 ;
    if (iter_val) {
        // Get char* equivalent to loop index value
        char *idx_val = iter_val->Image() ;
        // create escaped name for the new block to make it legal
        // VIPER #6997 : Use OrigName instead of name
        ret_name = Strings::save("\\", stmt_label->OrigName(), "(", idx_val, ")", "\\") ;
        //CARBON_BEGIN
        //Comment this out to have non-escaped labels (allthough this is just experimental purposes, we can't really change
        //ret_name = Strings::save(stmt_label->OrigName(), "_", idx_val) ;
        //CARBON_END
        Strings::free(idx_val) ;
    } else {
        // No iteration index, use label name :
        ret_name = Strings::save(stmt_label->OrigName()) ;
    }
    // Check for conflict: If any identifier presents in container scope with this name :
    // Argument specified scope is generate scope, we should check name conflict in its upper scope
    VhdlScope *container = gen_scope ? gen_scope->Upper(): 0 ;
    VhdlIdDef *exist = (container) ? container->FindSingleObjectLocal(ret_name) : 0 ;
    if (!exist) exist = (new_block_ids) ? (VhdlIdDef*)new_block_ids->GetValue(ret_name) : 0 ;
    // Discard generate block label as existing item :
    VhdlIdDef *gen_label = gen_scope ? gen_scope->GetOwner() : 0 ;
    if (exist == gen_label) exist = 0 ;
    if (!exist) return ret_name ; // No conflict, return

    // Conflict : Try to create unique name by appending '_<index>' with label name :
    unsigned idx = 0 ; // Start index from 0
    while (exist) {
        char int_name[38] ;
        // Create char* equivalent of index
        sprintf(int_name, "uniq%d", idx++) ;
        char *tmp = ret_name ;
        ret_name = Strings::save(stmt_label->OrigName(), "_", int_name) ;
        Strings::free(tmp) ;
        // Check whether created name is legal vhdl
        if (!VhdlTreeNode::IsLegalIdentifier(ret_name)) {
            // Name not legal, make it legal.
            tmp = ret_name ;
            ret_name = VhdlTreeNode::CreateLegalIdentifier(tmp) ;
            Strings::free(tmp) ;
        }
        exist  = (container) ? container->FindSingleObjectLocal(ret_name) : 0 ;
        // VIPER #4620 : Check already created block identifiers to avoid conflicts
        if (!exist) exist = (new_block_ids) ? (VhdlIdDef*)new_block_ids->GetValue(ret_name) : 0 ;
    }
    return ret_name ;
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void VhdlPrimaryUnit::CheckPortBinding(const VeriModule *verilog_module, VhdlIdDef* component/*passed for 7690*/)
{
    // Viper# 3254 : Mixed language incorrect port binding check
    // Check for non-existing port in verilog module instantiated
    // from vhdl designs, also check for width consistency for ports
    VhdlIdDef *id ;
    VhdlIdDef *component_id ;
    VeriIdDef *port ;
    VeriIdDef *veri_id ;
    VhdlIdDef *unit = _id ;
    if (!unit || !verilog_module) return ;

    Array *ports = verilog_module->GetPorts() ;
    Array *connections = verilog_module->GetPortConnects() ;
    unsigned j ;
    FOREACH_ARRAY_ITEM(ports, j, veri_id) {
        // Viper 3395: do not check size mismatch for implicit non ansi port
        if (!veri_id || veri_id->IsImplicitNonAnsiPort()) continue ;
        // Get the formal data type:
        // Check whether it is named connection or not Viper 4647, 5019
        VeriExpression *conn = (connections && (j < connections->Size()))? (VeriExpression*)connections->At(j) : 0 ;
        // For ansi port declarations do not extract datatype or size information from actual.
        if (conn && conn->IsAnsiPortDecl()) {
            conn = 0 ;
        }
        const char *formal = conn ? conn->NamedFormal() : 0 ;
        VeriIdDef *formal_id = 0 ;
        // In anyway we need the identifier of the formal this is connected:
        if (formal) {
            formal_id = (verilog_module) ? verilog_module->GetPort(formal) : 0 ;
        } else {
            formal_id = veri_id ;
        }

        port = formal_id ? formal_id : veri_id ;

        id = unit->GetPortAt(j) ;

        if (!port || !id) continue ;  // empty port ??

        component_id = component ? component->GetPortAt(j) : 0 ;

        (void) component_id ;

        // Viper 7690:if component port is open then ignore size checking:
        // Commented this check as this is the default behavior of simulator
        // is to issue error for port size mismatch even if the connection
        // is open in component association. If customer does not want to
        // see error they can un-comment the code below

        // if the user do not want to report error for size mismatch in un-associated ports
        // then one should un-comment the line below to check IsActualPresent.

        // if (component_id && !component_id->IsActualPresent()) continue ;

        // VIPER #3896 : Check mode consistency for this component-module port assoc
        if ((port->IsInput() && !(id->IsInput() || id->IsInout() || id->IsBuffer())) ||
            (port->IsOutput() && !(id->IsInout() || id->IsOutput())) ||
            (port->IsInout()  && !(id->IsInout()))) {
            id->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s", port->Name(), port->PrintToken(port->Dir()), id->Name(), EntityClassName(id->Mode())) ;
            id->Info("%s is declared here", id->Name()) ;
        }
        unsigned veri_size = 0 ;
        if (formal_id) {
            // Check for datatype consistency between vhdl and verilog ports
            // Should we use VeriModule::PortWidthDir ??
            veri_size = GET_CONTEXT_SIZE(formal_id->StaticSizeSign()) ;
        } else {
            VeriExpression *actual = conn ? conn->GetConnection() : 0 ;
            veri_size = GET_CONTEXT_SIZE(actual->StaticSizeSign()) ;
        }

        VhdlConstraint *vhd_constr = id->Constraint() ;
        if (!vhd_constr) continue ;
        VhdlIdDef *vhdl_type = id->Type() ;
        if (!vhdl_type) continue ;

        // we do not expect complex interface type in mixed language
        // Viper 6895: now we can have complex vhdl types like enums and records
        verific_uint64 vhd_size = 1 ;
        VhdlConstraint *vhdl_type_constr = vhdl_type->Constraint() ;

        VhdlConstraint *runner = (vhd_constr->IsRangeConstraint() ? vhdl_type_constr : vhd_constr) ;
        VhdlIdDef *running_type = vhdl_type ;
        while (runner && running_type) {
            // Viper 6895: Code added for record constraint
            unsigned is_standard_type = running_type->IsStringType() || running_type->IsRealType() ||
                                        running_type->IsIntegerType() || running_type->IsNaturalType() ||
                                        running_type->IsPositiveType() || running_type->IsRecordType() || running_type->IsEnumerationType();
            if (runner->IsArrayConstraint())
                vhd_size = vhd_size * runner->Length() ;
            else if (runner->IsRecordConstraint()) {
                vhd_size = vhd_size * runner->GetSizeForConstraint() ;
            } else {
                vhd_size = vhd_size * (is_standard_type ? runner->NumBitsOfRange() : 1) ; // Commented out for Viper 5043
            }
            runner = runner->ElementConstraint() ;
            running_type = running_type->ElementType() ;
        }
        if (vhd_size != veri_size) id->Warning("size mismatch in mixed language port association, verilog port %s", port->Name());
    }
}
#endif

// VIPER #4911 : When we are creating component from a verilog module entity, we cannot
// directly copy the ports/generics of entity to make ports/generics of component. Ports/generics
// of entity here can be of type 'vl_type' which can only be used internally for conversion of verilog module
// to its vhdl representation. Those types should not be exposed to user. So we will create
// the type of ports/generics from associated component itself.
// It may be the case that name of component port/generic is different from instantiated entity ports.
// So we will take help of binding information to extract information about ports/generics of entity
// from corresponding component ports/generics.
Array *VhdlEntityDecl::CreateInterfaceDecls(const VhdlIdDef *actual_component, const Array *actual_interfaces, VhdlMapForCopy &old2new, unsigned is_generic)  const
{
    if (!actual_component) return 0 ;

    Array *interface_decls = 0 ;
    VhdlComponentDecl *component_body = actual_component->GetComponentDecl() ;
    if (!component_body) return 0 ;

    // Viper 6769: If verilog module then take the array from component to avoid having vl_types in the
    // created component.
    //Array *generic_clause = _is_verilog_module ? component_body->GetGenericClause() : _generic_clause ;
    //Array *port_clause = _is_verilog_module ? component_body->GetPortClause() : _port_clause ;
    Array *generic_clause = _generic_clause ;
    Array *port_clause = _port_clause ;

    // Check whether 'actual_interfaces' is named association or not. If it is named,
    // it has entity-port-id vs component-port-id mapping. Otherwise it is normal
    // port/generic association. In that case take interface from component
    VhdlDiscreteRange *assoc ;
    unsigned is_named_assoc = 0 ;
    unsigned i, j ;
    FOREACH_ARRAY_ITEM(actual_interfaces, i, assoc) {
        if (!assoc) continue ;
        if (assoc->IsAssoc()) {
            is_named_assoc = 1 ; break ;
        }
    }

    VhdlInterfaceDecl *decl ;
    VhdlIdDef *id ;
    if (!actual_interfaces || !is_named_assoc) {
        generic_clause = _is_verilog_module ? component_body->GetGenericClause() : _generic_clause ;
        port_clause = _is_verilog_module ? component_body->GetPortClause() : _port_clause ;
        // No association exist between ports/generics of component and ports/generics of instantiated verilog module
        // So we can create the port/generic list of new component from the originally instantiated component.
        // Name of ports/generics will be same in component and verilog module.

        // Copy generic list from verilog module entity as it cannot have 'vl_type' as subtype indication.
        // That is not supported in mixed language. Copy the port list from component to get rid of 'vl_type'.
        interface_decls = old2new.CopyDeclarationArray(is_generic ? generic_clause : port_clause) ;
        if (is_generic) {
            // We are copying generics here. Generics can be used in subtype indication of component ports
            // to declare ranges like 'bit_vector (width downto 0)'. We will later copy the subtype indication
            // of component ports to make ports of our new component, but those subtype indication will have
            // references to generics of original component and we should convert those references to generics
            // of newly created component. For that store the generics of original component vs corresponding
            // generics of new component in our map 'old2new' which will be used for copy. As we have created
            // generics of new component from verilog module entity, this association is not present in 'old2new'
            FOREACH_ARRAY_ITEM(interface_decls, i, decl) {
                Array *ids = decl ? decl->GetIds(): 0 ;
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    if (!id) continue ;
                    VhdlIdDef *comp_id = actual_component->GetGeneric(id->Name()) ;
                    if (comp_id) old2new.SetCopiedId(comp_id, id) ;
                }
            }
        }
        // VIPER #7643 : Every identifier of interface declaration can obtain different subtype from
        // component or different value, so split interface decl with multiple ids
        // to multiple interface declarations with single id
        Array *ret_interface_decls = 0 ;
        if (interface_decls) {
            ret_interface_decls = new Array(interface_decls->Size()) ;
            FOREACH_ARRAY_ITEM(interface_decls, i, decl) {
                if (decl) decl->Split(ret_interface_decls) ;
            }
        }
        delete interface_decls ;
        return ret_interface_decls ;
    }
    // Association exist between ports/generics of component and ports/generics of instantiated verilog module. It may be
    // the case that name of interface identifiers are different in component and instantiated entity and that
    // information can be extracted from port/generic map of binding.

    // Copy the interface decls from entity, as we will take name of interface ids from entity
    Array *copied_interfaces = old2new.CopyDeclarationArray(is_generic ? generic_clause: port_clause) ;

    // Every identifier of interface declaration can obtain different subtype from component, so
    // split interface decl with multiple ids to multiple interface declarations with single id
    interface_decls = new Array(copied_interfaces ? copied_interfaces->Size(): 2) ;
    FOREACH_ARRAY_ITEM(copied_interfaces, i, decl) {
        if (!decl) continue ;
        decl->Split(interface_decls) ;
    }
    delete copied_interfaces ;

    Map entity_vs_component(POINTER_HASH) ; // Map to store interface id of entity vs its corresponding component interface

    // Now we will populate a map to store port/generic of instantiated entity vs corresponding port/generic
    // of associated component. This will help us to get subtype information from port/generic of component
    // and name from instantiated entity. For this we will process generic/port map from binding information
    // It contains the association between interface of instantiated entity vs component.
    FOREACH_ARRAY_ITEM(actual_interfaces, i, assoc) {
        if (!assoc) continue ;
        id = 0 ;
        if (assoc->IsAssoc()) id = assoc->FindFormal() ;
        // Get interface id from original entity
        if (id) {
            id = is_generic ? _id->GetGeneric(id->Name()) : _id->GetPort(id->Name()) ;
        } else {
            id = is_generic ? _id->GetGenericAt(i): _id->GetPortAt(i) ;
        }
        // Get corresponding copied interface identifier from 'old2new' map (populate during CopyDeclarationArray above)
        if (id) id = old2new.GetCopiedId(id) ;

        // Process actual part of association. Get identifier reference from there.
        VhdlExpression *actual = assoc->ActualPart() ;
        VhdlIdDef *component_interface_id = (actual) ? actual->FindFormal(): 0 ;

        // Checks if component interface id is in actual.
        if (component_interface_id && (component_interface_id->IsPort() || component_interface_id->IsGeneric()) && actual_component->LocalScope()->IsDeclaredHere(component_interface_id)) {
            // Component interface id is in actual, this is interface identifier corresponding to
            // copied identifier 'id'.
            (void) entity_vs_component.Insert(id, component_interface_id) ;
        }
    }
    // Create a map to store interface identifier of component and its subtype indication. Subtype indication
    // is not directly available from identifier.
    Map component_id_vs_subtype(POINTER_HASH) ;
    Array *comp_interface_decls = is_generic ? component_body->GetGenericClause() : component_body->GetPortClause() ;

    FOREACH_ARRAY_ITEM(comp_interface_decls, i, decl) { // Iterate over interface declarations
        Array *ids = decl ? decl->GetIds() : 0 ;
        VhdlSubtypeIndication *data_type = decl ? decl->GetSubtypeIndication(): 0 ; // Subtype is in declaration
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (id) (void) component_id_vs_subtype.Insert(id, data_type) ; // Populate the map
        }
    }

    // Now we have collected all data and we are ready to change subtype indication of copied interface declarations
    // Subtype-indication of copied interface ids will be same as corresponding identifier in instantiated component.
    FOREACH_ARRAY_ITEM(interface_decls, i, decl) {
        if (!decl) continue ;
        Array *ids = decl->GetIds() ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            // Get current subtype indication of created interface identifier
            VhdlSubtypeIndication *type = decl->GetSubtypeIndication() ;
            // Get corresponding component interface identifier
            VhdlIdDef *comp_id = (VhdlIdDef*)entity_vs_component.GetValue(id) ;
            // Component/entity association is not present in actual_interfaces, i.e. name of corresponding
            // component interface id is same as that of 'id'. Get that from 'actual_component'.
            if (!comp_id) {
                comp_id = is_generic ? actual_component->GetGeneric(id->Name()) : actual_component->GetPort(id->Name()) ;
            }
            if (is_generic && comp_id) {
                // If we are copying generics, store the identifier of component generic vs created generic
                // is map used for copy. It will help us to copy subtype indication of ports
                old2new.SetCopiedId(comp_id, id) ;
            }
            // Get subtype-indication from associated component
            VhdlSubtypeIndication *comp_subtype = (VhdlSubtypeIndication*)component_id_vs_subtype.GetValue(comp_id) ;

            // Copy that subtype-indication and replace current subtype with that
            VhdlSubtypeIndication *new_type = (comp_subtype) ? comp_subtype->CopySubtypeIndication(old2new): 0 ;
            if (new_type) {
                (void)decl->ReplaceChildSubtypeIndication(type, new_type) ;
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
                // VIPER 4929. _sub_type_indication was not set
                id->SetSubtypeIndication(new_type) ;
#endif
            }
        }
    }
    return interface_decls ;
}
void
VhdlIfElsifGenerateStatement::StaticElaborate(VhdlBlockConfiguration *block_config, VhdlDataFlow *df)
{
    if (!_scheme) return ;
    if (_label) PushNamedPath(_label->OrigName()) ;
    // VIPER #3393 : Keep an array to store elaboration created block identifiers.
    // We create new block statements for each iteration of for-generate and single
    // block statement for if-generate. We will store block identifiers of those
    // blocks in this array instead of declaring those immediately in generate scope.
    // This will be done as created block names can create conflict with already
    // declared identifiers within this generate statement.
    Map new_ids(STRING_HASH_CASE_INSENSITIVE) ;

    // Elaborate the then branch
    Array * new_stmts = _scheme->StaticElaborate(df, block_config, this, &new_ids, _label) ;

    if (!new_stmts || !new_stmts->Size()) { // then branch is not active
        delete new_stmts ; new_stmts = 0 ;
        // Need to check elsif branches
        unsigned i ;
        VhdlGenerateStatement *elsif_stmt ;
        VhdlIterScheme *elsif_scheme ;
        FOREACH_ARRAY_ITEM(_elsif_list, i, elsif_stmt) {
            elsif_scheme = elsif_stmt ? elsif_stmt->GetScheme(): 0 ;
            if (!elsif_scheme) continue ;
            new_stmts = elsif_scheme->StaticElaborate(df, block_config, elsif_stmt, &new_ids, _label) ;
            if (new_stmts) break ; // this is active branch
        }
    }
    if ((!new_stmts  || !new_stmts->Size()) && _else_stmt) { // elsif branches are also not active
        delete new_stmts ; new_stmts = 0 ;
        // Elaborate else branch
        VhdlIterScheme *else_scheme = _else_stmt->GetScheme() ;
        new_stmts = else_scheme ? else_scheme->StaticElaborate(df, block_config, _else_stmt, &new_ids, _label): 0 ;
    }
    if (!new_stmts || !new_stmts->Size()) {
        // VIPER #6793: Create empty block when condition of if is false or
        // _scheme is for-scheme.
        VhdlMapForCopy old2new ;
        VhdlBlockStatement *blk_stmt = VhdlIterScheme::CreateBlockStatement(_label, _local_scope, 0, 0, 0, old2new, &new_ids, this) ;
        blk_stmt->SetStaticElaborated() ;
        blk_stmt->SetIsElabCreatedEmpty() ;
        if (!new_stmts) new_stmts = new Array(1) ;
        new_stmts->Insert(blk_stmt) ;
    }
    if (_label) PopNamedPath() ; // Pop named path

    ReplaceByBlocks(_present_scope, &new_ids, new_stmts, this) ;
}

void VhdlCaseGenerateStatement::StaticElaborateActiveAlternative(const VhdlGenerateStatement *active_alt, VhdlIdDef *stmt_label, VhdlDataFlow *df, const VhdlBlockConfiguration *block_config)
{
    if (!active_alt) return ;
    if (stmt_label) PushNamedPath(stmt_label->OrigName()) ;
    // VIPER #3393 : Keep an array to store elaboration created block identifiers.
    // We create new block statements for each iteration of for-generate and single
    // block statement for if-generate. We will store block identifiers of those
    // blocks in this array instead of declaring those immediately in generate scope.
    // This will be done as created block names can create conflict with already
    // declared identifiers within this generate statement.
    Map new_ids(STRING_HASH_CASE_INSENSITIVE) ;

    Array *declarations = active_alt->GetDeclPart() ;
    Array *statements = active_alt->GetStatementPart() ;
    VhdlScope *block_scope = active_alt->LocalScope() ;

    VhdlScope *old_scope = _present_scope ;
    _present_scope = block_scope ;

    PushAttributeSpecificPath(stmt_label ? stmt_label->Name():0) ;
    Array *new_stmts = new Array(1) ;
    // Create block statement with statements inside
    // the generate construct
    VhdlMapForCopy old2new ;

    // Extract configuration of the block specified by 'stmt_label'
    VhdlBlockConfiguration *subblock_config = block_config ? block_config->ExtractSubBlock(stmt_label, 0, 0) : 0 ;

    VhdlBlockStatement *blk_stmt = 0 ;
    blk_stmt = VhdlIterScheme::CreateBlockStatement(stmt_label, block_scope, 0, declarations, statements, old2new, &new_ids, active_alt) ;
    new_stmts->Insert(blk_stmt) ;

    // Elaborate the declarations and statements of the
    // created block statement
    VhdlBlockConfiguration *copied_config = VhdlIterScheme::ProcessBlockStatement(blk_stmt, 0, subblock_config, old2new, df) ;
    delete copied_config ;
    delete subblock_config ;

    PopAttributeSpecificPath() ;
    _present_scope = old_scope ;

    if (stmt_label) PopNamedPath() ; // Pop named path

    VhdlGenerateStatement::ReplaceByBlocks(_present_scope, &new_ids, new_stmts, this) ;
}

void VhdlComponentInstantiationStatement::StaticElaborateBlackBox()
{
    if (!_unit || !_unit->IsComponent()) return ;
    VhdlIdDef *actual_component = _unit ;
#ifdef VHDL_UNIQUIFY_BLACKBOXES
    // VIPER #6281 : Uniquify component for its generic/port constraint settings:
    // Get enclosing scope of actual component
    VhdlScope *comp_scope = _unit->LocalScope() ;
    // Get enclosing scope (block scope/arch scope/package scope)
    VhdlScope *upper_scope = comp_scope ? comp_scope->Upper(): 0 ;
    VhdlIdDef *container_id = (upper_scope) ? upper_scope->GetOwner() : 0 ; // Get owner of containing scope
    if (container_id && container_id->IsPackage()) {
        // VIPER #2788 :Instantiated component is declared in a package. We will not add any new
        // component in package as this package can also be included in other instantiated
        // entity and this scenario can hamper our save/restore flow. So we will add
        // new component in enclosing architecture.
        VhdlIdDef *arch_id = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ; // Get enclosing architecture
        upper_scope = (arch_id) ? arch_id->LocalScope() : 0 ; // Get scope of architecture
    }
    char *component_name = _unit->CreateUniqueName(0) ;
    if (!VhdlTreeNode::IsLegalIdentifier(component_name)) {
        char *tmp = component_name ;
        component_name = VhdlTreeNode::CreateLegalIdentifier(tmp) ;
        Strings::free(tmp) ;
    }
    // Check whether component_name is different than _unit->Name()
    unsigned is_copy_needed = 1 ;
    if (Strings::compare(component_name, _unit->OrigName())) {
        char *tmp = component_name ;
        component_name = Strings::save(_unit->Name()) ;
        Strings::free(tmp) ;
        is_copy_needed = 0 ;
    }

    // first check if component with this name is created
    VhdlIdDef *instantiated_comp = (upper_scope) ? upper_scope->FindSingleObjectLocal(component_name) : 0 ;
    // VIPER #7418: Consider only component from upper_scope
    if (instantiated_comp && !instantiated_comp->IsComponent()) instantiated_comp = 0 ;

    if (!instantiated_comp && is_copy_needed && upper_scope) { // Component not defined, create one
        // Create a component for entity 'actual_master'. The upper scope of
        // created component will be 'upper_scope'
        VhdlComponentDecl *orig_decl = _unit->GetComponentDecl() ;
        VhdlMapForCopy old2new ;
        VhdlComponentDecl *copied_comp_decl = orig_decl->CopyWithName(component_name, old2new) ;
        Map lib_ids(STRING_HASH) ;
        VhdlIdDef *containing_unit = upper_scope->GetContainingDesignUnit() ;
        VhdlScope *containing_unit_scope = (containing_unit) ? containing_unit->LocalScope() : upper_scope ;
        Array *generics = copied_comp_decl->GetGenericClause() ;
        unsigned i ;
        VhdlInterfaceDecl *decl ;
        FOREACH_ARRAY_ITEM(generics, i, decl) {
            // Get subtype indication
            VhdlSubtypeIndication *type = decl ? decl->GetSubtypeIndication(): 0 ;
            // VIPER #3091 : Convert out of scope identifier references to selected names:
            VhdlSubtypeIndication *new_type = 0 ;
            //if (type) new_type = type->ChangeOutOfScopeRefs(copied_comp_decl->LocalScope(), _unit->LocalScope(), containing_unit_scope, &lib_ids) ;
            if (type) new_type = type->ChangeOutOfScopeRefs(_present_scope, comp_scope ? comp_scope->Upper(): 0, containing_unit_scope, &lib_ids) ;
            if (new_type && decl) {
                (void)decl->ReplaceChildSubtypeIndication(type, new_type) ;
                // VIPER 4929. _sub_type_indication was not set
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
                Array *ids = decl ? decl->GetIds() : 0 ;
                VhdlIdDef *id ;
                unsigned j ;
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    if (id) id->SetSubtypeIndication(new_type) ;
                }
#endif
            }
            type = decl ? decl->GetSubtypeIndication(): 0 ;
        }

        Array *ports = copied_comp_decl->GetPortClause() ;
        // Now declare copied port ids to component scope
        FOREACH_ARRAY_ITEM(ports, i, decl) {
            // Get subtype indication
            VhdlSubtypeIndication *type = decl ? decl->GetSubtypeIndication(): 0 ;
            // Convert out of scope identifier references :
            VhdlSubtypeIndication *new_type = 0 ;
            //if (type) new_type = type->ChangeOutOfScopeRefs(copied_comp_decl->LocalScope(), _unit->LocalScope(), containing_unit_scope, &lib_ids) ;
            if (type) new_type = type->ChangeOutOfScopeRefs(_present_scope, comp_scope ? comp_scope->Upper(): 0, containing_unit_scope, &lib_ids) ;
            if (new_type && decl) {
                (void)decl->ReplaceChildSubtypeIndication(type, new_type) ;
                (void) new_type->TypeInfer(0,0,VHDL_range,0) ;
                type = new_type ;
                // VIPER 4929. _sub_type_indication was not set
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
                Array *ids = decl ? decl->GetIds() : 0 ;
                VhdlIdDef *id ;
                unsigned j ;
                FOREACH_ARRAY_ITEM(ids, j, id) {
                    if (id) id->SetSubtypeIndication(new_type) ;
                }
#endif
            }
        }
        // VIPER #4266 : Create library clause for each library identifier used to
        // replace out of scope identifier references with full path name
        MapIter mi ;
        VhdlIdDef *lib_id ;
        Array *lib_id_defs = 0 ;
        FOREACH_MAP_ITEM(&lib_ids, mi, 0, &lib_id) {
            if (!lib_id) continue ;
            if (!lib_id_defs) lib_id_defs = new Array(2) ;
            lib_id_defs->InsertLast(lib_id) ;
        }
        // Now add this library clause to containing design unit
        if (lib_id_defs && lib_id_defs->Size()) {
            VhdlDesignUnit *du = containing_unit ? containing_unit->GetDesignUnit(): 0 ;
            if (du) {
                VhdlLibraryClause *lib_clause = new VhdlLibraryClause(lib_id_defs, 0) ;
                lib_clause->SetLinefile(_unit->Linefile()) ;
                du->AddContextClause(lib_clause) ;
            }
        }
        instantiated_comp = copied_comp_decl ? copied_comp_decl->GetId(): 0 ;
        if (copied_comp_decl) {
            copied_comp_decl->AddAfter(orig_decl, upper_scope) ;
            (void) upper_scope->Declare(instantiated_comp) ;
            Array *copied_generics = old2new.CopyDiscreteRangeArray(_generic_map_aspect) ;
            Array *copied_ports = old2new.CopyDiscreteRangeArray(_port_map_aspect) ;
            copied_comp_decl->SplitAndAssociateGenerics(copied_generics) ;
            copied_comp_decl->SplitAndAssociatePorts(copied_ports) ;

            VhdlDiscreteRange *assoc ;
            FOREACH_ARRAY_ITEM(copied_generics, i, assoc) delete assoc ;
            delete copied_generics ;
            FOREACH_ARRAY_ITEM(copied_ports, i, assoc) delete assoc ;
            delete copied_ports ;
        }
    }
    Strings::free(component_name) ;
    // Change the _instantiated_unit, make it simple component reference
    if (_instantiated_unit && instantiated_comp) {
        linefile_type line_file = _instantiated_unit->Linefile() ;
        VhdlIdRef *component_name_ref = new VhdlIdRef(instantiated_comp) ;
        component_name_ref->SetLinefile(line_file) ;
        VhdlName *new_instantiated_unit = 0 ;
        if (_instantiated_unit->GetEntityClass()) {
            // If this instantiated unit contains 'component' keyword
            // replicate that in created name
            new_instantiated_unit = new VhdlInstantiatedUnit(VHDL_component, component_name_ref) ; // Create new one
        } else {
            // No 'component' keyword, do not introduce that in created name
            new_instantiated_unit = component_name_ref ;
        }
        delete _instantiated_unit ; // Delete old one
        _instantiated_unit = new_instantiated_unit ;
        _instantiated_unit->SetLinefile(line_file) ;
        _unit = instantiated_comp ; // Set new component id at _unit
    }

    // Generics are already propagated. Delete this
    unsigned i ;
    VhdlDiscreteRange *aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) delete aspect ;
    delete _generic_map_aspect ;
    _generic_map_aspect = 0 ;

#endif // VHDL_UNIQUIFY_BLACKBOXES
    // VIPER #3428 : Try to create port association and issue error for ports having no actual/default
    CreatePortAssociation(0, actual_component) ;
}
void VhdlComponentDecl::AddAfter(VhdlComponentDecl *orig_comp_decl, VhdlScope *upper_scope)
{
    if (!orig_comp_decl || !upper_scope) return ;
    // Add created component to declarative region of block/architecture/package
    // where 'actual_component' is declared
    // Get owner of 'upper_scope'
    VhdlIdDef *owner_id = upper_scope->GetOwner() ;
    // 'upper_scope' can be scope of VhdlPackageDecl /VhdlSecondaryUnit/ VhdlBlockStatement
    // Check if container of 'actual_component' is primary unit. If so add
    // new component decl there
    VhdlPrimaryUnit *prim_unit = owner_id ? owner_id->GetPrimaryUnit(): 0 ;
    unsigned is_added = 0 ;
    if (prim_unit) is_added = prim_unit->InsertAfterDecl(orig_comp_decl, this) ;

    if (!is_added) { // Check if container of 'actual_component' is secondary unit
        VhdlSecondaryUnit *sec_unit = owner_id ? owner_id->GetSecondaryUnit() : 0 ;
        if (sec_unit) is_added = sec_unit->InsertAfterDecl(orig_comp_decl, this) ;
        if (sec_unit && !is_added) {
            // We want to add new component declaration in secondary unit, but component
            // is actually declared in package. So add component declaration using 'Add'
            sec_unit->AddDeclaration(this) ;
        }
    }
    if (!is_added) { // Check if container of 'actual_component' is block
        VhdlStatement *stmt = owner_id ? owner_id->GetStatement() : 0 ;
        if (stmt) is_added = stmt->InsertAfterDecl(orig_comp_decl, this) ;
    }
}

void VhdlComponentDecl::SplitAndAssociatePorts(const Array *actual_ports)
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    if (actual_ports && _port_clause) {
        Array *new_port_clause = new Array(_port_clause->Size()) ;
        // Convert declaration with array of identifiers
        // to declarations with single identifier, so that
        // different constraint can be set to each unconstrained
        // port.
        FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
            if (!decl) continue ;
            decl->Split(new_port_clause) ;
            _port_clause->Insert(i, 0) ;
        }
        delete _port_clause ;
        _port_clause = new_port_clause ;
    }
    // Set the constraints and values to the ports
    AssociatePorts(0, actual_ports) ;

    // Create a set of ports for which actual is provided :
    VhdlDiscreteRange *assoc ;
    VhdlIdDef *id ;
    Set port_with_actual(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(actual_ports, i, assoc) {
        id = assoc ? assoc->FindFormal(): 0 ;
        if (id) (void) port_with_actual.Insert(id->Name()) ;
    }
    // Create range constraint for unconstrained ports
    unsigned j ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (!decl) continue ;
        Array *ids = decl->GetIds() ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id || !id->IsUnconstrained()) continue ;
            // If actual is present for unconstrained port, delete its initial value
            if (!port_with_actual.Get(id->Name())) continue ; // no actual, continue
            decl->SetInitAssign(0) ;
        }
        decl->CreateRangeConstraint() ;

        // VIPER #2858 : Static elaboration should not use value (initial)
        // of signal later, so set it to null
        // VIPER #4369/4374/4397 : Do not reset here
        //FOREACH_ARRAY_ITEM(ids, j, id) {  // Iterate over ids
            //if (id) id->SetValue(0) ;
        //}
#ifdef VHDL_REPLACE_CONST_EXPR
        // VIPER #6330 : Evaluate bounds of port
        VhdlSubtypeIndication *subtype = decl->GetSubtypeIndication() ;
        if (subtype) subtype->ReplaceBoundsInSubtypeIndication(0) ;
#endif // VHDL_REPLACE_CONST_EXPR
    }
}
void VhdlIndexedName::ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df)
{
    // Check assoc list
    unsigned i ;
    VhdlDiscreteRange *index_range ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, index_range) {
        if (!index_range) continue ;
        index_range->ReplaceBoundsInSubtypeIndication(df) ;
    }
}
void VhdlExplicitSubtypeIndication::ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df)
{
    if (_range_constraint) _range_constraint->ReplaceBoundsInSubtypeIndication(df) ;
}

void VhdlDiscreteRange::ReplaceBoundsInSubtypeIndication(VhdlDataFlow * /*df*/)
{
}
void VhdlRange::ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df)
{
    VhdlValue *left = _left ? _left->Evaluate(0,df,0): 0 ;

    VhdlValue *right = _right ? _right->Evaluate(0,df,0) : 0 ;

    if (!left || !right) {
        delete left ;
        delete right ;
        return ;
    }
    VhdlIdDef *enum_type = left->GetType() ;
    if (!enum_type) enum_type = right->GetType() ;
    // Now convert to const. If enum_type is 0 (both sides nonconst or both sides integer),
    // then we will translate nonconsts to an integer range.
    if (left->IsConstant()) left =  left->ToConst(enum_type) ;
    if (right->IsConstant()) right = right->ToConst(enum_type) ;

    VhdlExpression *left_bound = (left) ? left->CreateConstantVhdlExpressionInternal(_left ? _left->Linefile(): 0, 0, -1, 0): 0 ;
    VhdlExpression *right_bound = (right) ? right->CreateConstantVhdlExpressionInternal(_right ? _right->Linefile(): 0, 0, -1, 0): 0 ;

    // JJ (111012) free up memory
    delete left ;
    delete right ;

    if (left_bound) {
        delete _left ;
        _left = left_bound ;
    }
    if (right_bound) {
        delete _right ;
        _right = right_bound ;
    }
}
// VIPER #6931 : Keep generic map aspect for generic types
void VhdlComponentInstantiationStatement::CreateGenericAssociation()
{
    // If instantiated unit does not contain generic type, delete generic map aspect
    unsigned i ;
    Array *generics = (_unit) ? _unit->GetGenerics(): 0 ;
    unsigned has_type_generic = 0 ;
    VhdlIdDef *generic ;
    FOREACH_ARRAY_ITEM(generics, i, generic) {
        if (generic && (generic->IsGenericType() || generic->IsPackage() || generic->IsSubprogram())) {
            has_type_generic = 1 ;
            break ;
        }
    }
    VhdlDiscreteRange *aspect ;
    if (!has_type_generic) {
        FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) delete aspect ;
        delete _generic_map_aspect ;
        _generic_map_aspect = 0 ;
        return ;
    }

    // Instantiated unit not set, return
    if (!_unit) return ;

    Array *gneric_map_aspect = new Array(1) ;
    //FOREACH_ARRAY_ITEM(generics, i, generic) {
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) {
        if (!aspect) continue ;

        if (aspect->IsAssoc()) {
            generic = aspect->FindFormal() ;
            if (generic) generic = _unit->GetGeneric(generic->Name()) ;
        } else {
            generic = _unit->GetGenericAt(i) ;
        }
        if (!generic || (!generic->IsGenericType() && !generic->IsPackage() && !generic->IsSubprogram())) continue ;
        if (generic->IsGenericType()) {
            VhdlIdDef *actual_type = generic->ActualType() ;
            if (!actual_type) continue ;
            VhdlSubtypeIndication *subtype_ind = new VhdlIdRef(actual_type) ;
            subtype_ind->SetLinefile(Linefile()) ;
            VhdlConstraint *constraint = generic->Constraint() ;
            if (constraint && !constraint->IsUnconstrained() && constraint->IsArrayConstraint()) {
                subtype_ind = subtype_ind->CreateSubtypeWithConstraint(constraint) ;
            }
            if (subtype_ind) (void) subtype_ind->TypeInfer(0,0,VHDL_range,0) ;

            VhdlIdRef *formal = new VhdlIdRef(generic) ;
            formal->SetLinefile(Linefile()) ;
            VhdlAssocElement *new_assoc = new VhdlAssocElement(formal, subtype_ind) ;
            new_assoc->SetLinefile(Linefile()) ;
            gneric_map_aspect->InsertLast(new_assoc) ;
        }
        if (generic->IsPackage() || generic->IsSubprogram()) {
            VhdlExpression *actual_part = aspect->ActualPart() ;
            VhdlMapForCopy old2new ;
            VhdlExpression *new_actual = actual_part ? actual_part->CopyExpression(old2new): 0 ;
            VhdlIdRef *formal = new VhdlIdRef(generic) ;
            formal->SetLinefile(Linefile()) ;
            VhdlAssocElement *new_assoc = new VhdlAssocElement(formal, new_actual) ;
            new_assoc->SetLinefile(Linefile()) ;
            gneric_map_aspect->InsertLast(new_assoc) ;
        }
    }
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) delete aspect ;
    delete _generic_map_aspect ;
    _generic_map_aspect = gneric_map_aspect ;
}

void VhdlDesignUnit::ProcessedUsedPackage(VhdlMapForCopy &old2new) const
{
    (void) old2new ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    if (!_local_scope) return ;
    Set *using_set = _local_scope->GetUsing() ;
    SetIter si ;
    VhdlScope *other_scope ;
    VhdlIdDef *this_unit = _local_scope->GetContainingDesignUnit() ;
    VhdlIdDef *other_unit ;
    FOREACH_SET_ITEM(using_set, si, &other_scope) {
        other_unit = other_scope->GetContainingDesignUnit() ;
        if (!other_unit) continue ;
        if (other_unit == this_unit) continue ;
        if (other_unit->IsPackage()) {
            VhdlPrimaryUnit *package = other_unit->GetPrimaryUnit() ;
            if (package && package->IsVerilogModule()) {
                char *name = Strings::save(other_unit->Name(), "_default") ;
                VhdlLibrary *lib = package->GetOwningLib() ;
                VhdlPrimaryUnit *copied_pkg = lib ? lib->GetPrimUnit(name, 0, 0): 0 ;
                Strings::free(name) ;
                if (package && copied_pkg) {
                    old2new.SetCopiedId(other_unit, copied_pkg->Id()) ;
                    old2new.MakeAssociations(package, copied_pkg, 1) ;
                }
            }
        }
    }
#endif // VHDL_DUAL_LANGUAGE_ELABORATION
}
// VIPER #7853 : If length of 'name' is greater than 200, create short name
// using sequential index
char *VhdlTreeNode::CreateShortName(const char *unit_name, char *long_name)
{
    // If runtime flag is not set, do not modify the long name
    if (!RuntimeFlags::GetVar("vhdl_do_not_create_long_unit_names")) return long_name ;
    if (!unit_name || !long_name) return 0 ;

    if (Strings::len(long_name) < 200) return long_name ; // Name is not too large
    if (!_long2short) _long2short = new Map(STRING_HASH) ;

    MapItem *ele = _long2short->GetItem(long_name) ;
    if (ele) {
        Strings::free(long_name) ;
        return Strings::save((char *)ele->Value()) ;
    }

    // Not already shortened. Ignore the created signature and append
    // "_renamed_due_excessive_length_" with an unique number at the
    // end of the instantiated unit name
    if (!_unit_num_map) _unit_num_map = new Map(STRING_HASH) ;
    MapItem *item = _unit_num_map->GetItem(unit_name) ;
    char *old_key = item ? (char*)item->Key(): 0 ;
    unsigned long count = item ? (unsigned long)item->Value(): 0 ;
    char *short_sig = Strings::allocate(Strings::len(unit_name)+48) ;
    sprintf(short_sig, "%s_renamed_due_excessive_length_%d", unit_name, (unsigned)count) ;

    // Store long name along with short name
    (void) _long2short->Insert(Strings::save(long_name), short_sig) ;

    // Insert unqiue number used for this entity
    (void) _unit_num_map->Insert(Strings::save(unit_name), (void*)(count+1), 1) ;
    Strings::free(old_key) ;
    Strings::free(long_name) ;
    return Strings::save(short_sig) ;
}

