/*
 *
 * [ File Version : 1.390 - 2014/02/05 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <ctype.h>  // for toupper, isalnum and isdigit
#include <stdio.h>  // for sprintf

#include "Strings.h"
#include "Array.h"
#include "Set.h"
#include "Map.h"

#include "VhdlName.h"
#include "vhdl_file.h"
#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "VhdlUnits.h"

#include "../verilog/veri_file.h"
#include "../verilog/VeriModule.h"
#include "../verilog/VeriLibrary.h"

// Need Value/Constraint for elaboration fields
#include "VhdlValue_Elab.h"

//CARBON_BEGIN
extern bool gMakeVhdlIdsLower;  //from CarbonContext.cxx
extern bool gMakeVhdlIdsUpper;  //from CarbonContext.cxx
//CARBON_END

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*****************************************************************/
/************************* Constructors/Destructors **************/
/*****************************************************************/

VhdlName::VhdlName()
  : VhdlSubtypeIndication()
 { }
VhdlName::~VhdlName() { }

VhdlNull::VhdlNull() : VhdlLiteral() { }
VhdlNull::~VhdlNull() { }

VhdlOpen::VhdlOpen() : VhdlName() { }
VhdlOpen::~VhdlOpen() { }

// Literals
VhdlLiteral::VhdlLiteral() : VhdlName() { }
VhdlLiteral::~VhdlLiteral() { }

VhdlPhysicalLiteral::VhdlPhysicalLiteral(VhdlExpression *value, VhdlName *unit)
  : VhdlLiteral(),
    _value(value),
    _unit(unit)
{ }
VhdlPhysicalLiteral::~VhdlPhysicalLiteral()
{
    delete _value ;
    delete _unit ;
}

VhdlInteger::VhdlInteger(verific_int64 value)
  : VhdlLiteral(),
    _value(value)
{ }
VhdlInteger::~VhdlInteger() { }

VhdlReal::VhdlReal(double value)
  : VhdlLiteral(),
    _value(value)
{ }
VhdlReal::~VhdlReal() { }

// Designators
VhdlDesignator::VhdlDesignator(char *name)
  : VhdlName(),
    _name(name),
    _single_id(0)
{
#ifdef VHDL_PRESERVE_ID_CASE
    // Now here, always down-case the identifier, except when it's an extended identifier
    // or a character literal or a string literal :
//CARBON_BEGIN
    //if (_name && *_name != '\\' && *_name != '\'' && *_name!='"') _name = Strings::strtolower(_name) ;
  // case change is controlled by -vhdlCase command line switch
  if (_name && *_name != '\\' && *_name != '\'' && *_name!='"') {
    if      (gMakeVhdlIdsLower){ _name = Strings::strtolower(_name) ;}
    else if (gMakeVhdlIdsUpper){ _name = Strings::strtoupper(_name) ;}
  }
//CARBON_END
#endif
}
VhdlDesignator::~VhdlDesignator()
{
    Strings::free( _name ) ;
}

VhdlStringLiteral::VhdlStringLiteral(char *string) : VhdlDesignator(string)
{
    // Issue 2312 : catch non-graphic characters (in string literals, character literals etc)
    char *tmp = _name ;
    unsigned char c ;
    while (tmp && *tmp!=0) {
        // Issue 2591 : Need to test character in ISO 8859-1 (8-bit character set), so cannot use "isgraph()".
        // ISO 8859-1 contains 191 graphic characters. Just two sections are not graphic.
        // Cast to unsigned char to do value compares:
        c = (unsigned char)(*tmp) ;
        if ((c <= 0x1F) || ((c >= 0x7F) && (c <= 0x9F))) {
//        if (!isgraph(c) && (c!=' ')) {
            Error("character with value 0x%x is not a graphic literal character",c) ;
            *tmp = ' ' ; // replace by space (to avoid junk messages during type-inference)
        }
        tmp++ ;
    }
}
VhdlStringLiteral::~VhdlStringLiteral() { }

VhdlBitStringLiteral::VhdlBitStringLiteral(char *string)
  : VhdlStringLiteral(0), // Initialize _name with 0 for now, we will set it later!
  _based_string(string)   // Store the based string (all upper case, without the _'s)
{
    // Bit String Literal. Follow rules of LRM 13.7
    // We have already filtered-out the _'s and made it
    // all UPPER CASE from lexer.
    // VIPER #7512: We no longer make it UPPER CASE since non-extended digits
    // are case sensitive. Instead, adjusted code below to compensate it here.
    // Note that we convert extended digits into binary here so those will be
    // automatically be treated as matching irrespective of their lower/upper case.

    // Now translate the bit-string literal (in _based_string) to a string literal (in _name).

    // Set-up a large-enough string
    unsigned elem_size = 0, base = 0, sign = 0, string_size = 0, is_2008 = 0 ;
    char *tmp = string ;
    while (tmp && *tmp) { // Enhanced for Vhdl 2008 LRM 15.8
        // VIPER #7512: Make lower case letters upper case, we skipped that in lexer:
        switch (*tmp) {
        case 'b' : *tmp = 'B' ;
        case 'B' : elem_size=1 ; base=2 ; break ;

        case 'o' : *tmp = 'O' ;
        case 'O' : elem_size=3 ; base=8 ; break ;

        case 'x' : *tmp = 'X' ;
        case 'X' : elem_size=4 ; base=16 ; break ;

        case 'd' : *tmp = 'D' ;
        case 'D' : elem_size=4 ; base = 10 ; is_2008 = 1 ; break ;

        case 'u' : *tmp = 'U' ;
        case 'U' : is_2008 = 1 ; break ;

        case 's' : *tmp = 'S' ;
        case 'S' : is_2008 = 1 ; sign = 1 ; break ;

        default :
        {
            is_2008 = 1 ;
            if (isdigit(*tmp)) {
                string_size = string_size*10 + (unsigned)(*tmp-'0') ;
            } else {
                VERIFIC_ASSERT(0) ;
            }
            break ;
        }
        }
        tmp += 1 ;
        if (base) break ;
    }

    VERIFIC_ASSERT(tmp) ;
    tmp += 1 ;

    if (is_2008 && !IsVhdl2008()) Error("this construct is only supported in VHDL 1076-2008") ;

    unsigned long len = Strings::len(string) ;
    _name = Strings::allocate((elem_size * len) + 2) ;

    char *r = _name ;
    *r++ = '"' ; // Open with a quote

    char *saved_str = (base==10) ? Strings::save(tmp) : 0 ;
    if (saved_str) tmp = saved_str ;

    char sum, borrow, *digit ;

    // Now fill the string with 0's and 1's
    // simplified_bit_value -> expanded_bit_value
    unsigned elem ;
    for (; isalnum(*tmp) || (*tmp == '-') ; tmp++) {
        // VIPER #7512: Match with upper case using toupper here:
        elem = (isdigit(*tmp)) ? (*tmp-'0') : (10+(toupper(*tmp)-'A'));
        // VIPER #6331: Allow all graphics characters in VHDL-2008 mode
        if (elem < base) {
            // VIPER #7512: Adjustment for upper case which we now skipped in lexer:
            *tmp = (char)toupper(*tmp) ;
        } else if (/*elem >= base &&*/ !IsVhdl2008()) {
            Error("digit '%c' is larger than base in %s", *tmp, string) ;
            elem = 0 ;
        }
        if (base==10) { // Vhdl 2008 : Section 15.8: decimal to binary
            borrow = 0 ;
            digit = tmp ;
            while (*digit!='"' && *digit!='\0') {
                sum = (*digit - '0') ;    // Calculate value of digit
                if (sum > 9) Error("digit '%c' is larger than base in %s", *digit, string) ;
                if (borrow) sum += 10 ;   // Add the borrow (base 10)
                borrow = sum & 1 ;        // Update borrow : take the LSB bit
                *digit = (sum>>1) + '0' ; // Update digit : divide by two.
                digit++ ;                 // Shift to next digit
            }
            if (*tmp!='0') --tmp ;

            *r++ = (borrow) ? '1' : '0' ;
        } else {
            int i ;
            for (i=(int)(elem_size-1);i>=0;i--) {
                // Vhdl 2008 : Section 15.8 : when elem >= base
                *r++ = (elem >= base) ? *tmp : ((elem >> i) & 1) ? '1' : '0' ;
            }
        }
    }

    *r++ = '"' ; // Close with a quote
    *r = '\0' ;  // Close the string
    Strings::free(saved_str) ;

    if (base==10) { // Vhdl 2008 : Section 15.8 : reverse the bits
        --r ;
        char *front = _name ;
        char *back = r ;
        while (1) {
            char temp = *front ;
            *front = *back ;
            *back = temp ;
            if (back == front) break ;
            ++front ;
            if (back == front) break ;
            --back ;
        }
    }

    // VIPER #7512: Check size match discarding the quotes in _name.
    // So, 'string_size' should match with length of '_name' - 2:
    if (string_size && (string_size != (Strings::len(_name)-2))) {
        // Vhdl 2008 : Section 15.8 : extend or truncate
        char *length_adjusted_str = Strings::allocate(string_size + 2) ;
        length_adjusted_str[string_size + 2] = '\0' ;
        length_adjusted_str[0] = '"' ;

        unsigned i = Strings::len(_name)-1 ;
        unsigned j = string_size + 1 ;
        while (i && j) { length_adjusted_str[j] = _name[i] ; --j ; --i ; }

        if (i) { // value truncated
            while (i) {
                if ((sign && _name[i] != _name[i+1]) || (!sign && _name[i] != '0')) Error("cannot truncate illegal sized bit string literal %s", string) ;
                --i ;
            }
        } else if (j) { // (sign) extend the value
            while (j) length_adjusted_str[j--] = sign ? _name[1] : '0' ;
        }

        Strings::free(_name) ;
        _name = length_adjusted_str ;
    }
}

VhdlBitStringLiteral::~VhdlBitStringLiteral()
{
    Strings::free(_based_string) ;
}

VhdlCharacterLiteral::VhdlCharacterLiteral(char *str) : VhdlDesignator(str)
{
    // Issue 2312 : catch non-graphic characters (in string literals, character literals etc)
    char *tmp = _name ;
    unsigned char c ;
    while (tmp && *tmp!=0) {
        // Issue 2591 : Need to test character in ISO 8859-1 (8-bit character set), so cannot use "isgraph()".
        // ISO 8859-1 contains 191 graphic characters. Just two sections are not graphic.
        // Cast to unsigned char to do value compares:
        c = (unsigned char)(*tmp) ;
        if ((c <= 0x1F) || ((c >= 0x7F) && (c <= 0x9F))) {
//        if (!isgraph(*tmp) && (*tmp!=' ')) {
            Error("character with value 0x%x is not a graphic literal character",(unsigned)(*tmp)) ;
            *tmp = ' ' ; // replace by space (to avoid junk messages during type-inference)
        }
        tmp++ ;
    }
}
VhdlCharacterLiteral::~VhdlCharacterLiteral() { }

VhdlIdRef::VhdlIdRef(char *name) : VhdlDesignator(name) { }
VhdlIdRef::VhdlIdRef(VhdlIdDef *id) : VhdlDesignator(Strings::save((id) ? id->Name() : ""))
{
    _single_id = id ;
}
VhdlIdRef::~VhdlIdRef() { }

VhdlArrayResFunction::VhdlArrayResFunction(VhdlName *res_function)
: VhdlName(),
_res_function(0)
{
    _res_function = res_function ;
}

VhdlArrayResFunction::~VhdlArrayResFunction()
{
    delete _res_function ;
}

VhdlRecordResFunction::VhdlRecordResFunction(Array *rec_res_function_list)
: VhdlName(),
_rec_res_function_list(0)
{
    _rec_res_function_list = rec_res_function_list ;
}

VhdlRecordResFunction::~VhdlRecordResFunction()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_rec_res_function_list,i,elem) delete elem ;
    delete _rec_res_function_list ;
}

// end-keywords
VhdlAll::VhdlAll() : VhdlDesignator(0) { }
VhdlAll::~VhdlAll() { }

VhdlOthers::VhdlOthers() : VhdlDesignator(0) { }
VhdlOthers::~VhdlOthers() { }

VhdlUnaffected::VhdlUnaffected() : VhdlDesignator(0) { }
VhdlUnaffected::~VhdlUnaffected() { }

// Real, full names
VhdlSelectedName::VhdlSelectedName(VhdlName *prefix, VhdlDesignator *suffix)
  : VhdlName(),
    _prefix(prefix),
    _suffix(suffix),
    _unique_id(0)
{ }
VhdlSelectedName::~VhdlSelectedName()
{
    delete _prefix ;
    delete _suffix ;
}

VhdlIndexedName::VhdlIndexedName(VhdlName *prefix, Array *assoc_list)
  : VhdlName(),
    _prefix(prefix),
    _assoc_list(assoc_list),
    _prefix_id(0),
    _prefix_type(0),
    _is_array_index(0),
    _is_array_slice(0),
    _is_function_call(0),
    _is_index_constraint(0),
    _is_record_constraint(0),
    _is_type_conversion(0),
    _is_group_template_ref(0)
{
}
VhdlIndexedName::~VhdlIndexedName()
{
    delete _prefix ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_assoc_list,i,elem) delete elem ;
    delete _assoc_list ;
}
VhdlSignaturedName::VhdlSignaturedName(VhdlName *name, VhdlSignature *signature)
  : VhdlName(),
    _name(name),
    _signature(signature),
    _unique_id(0)
{ }
VhdlSignaturedName::~VhdlSignaturedName()
{
    delete _name ;
    delete _signature ;
}

Map *VhdlAttributeName::_predef_table = 0 ;

VhdlAttributeName::VhdlAttributeName(VhdlName *prefix, VhdlIdRef *designator, VhdlExpression *expression)
  : VhdlName(),
    _prefix(prefix),
    _designator(designator),
    _expression(expression),
    _attr_enum(0),
    _prefix_type(0),
    _result_type(0)
{   // Set the type of attribute if it's predefined (0 is not predefined)
    if (!_predef_table) InitPredefAttrTable() ;
    unsigned long rlong = (unsigned long) _predef_table->GetValue(_designator->Name()) ;
    _attr_enum = (unsigned)rlong;
}
VhdlAttributeName::~VhdlAttributeName()
{
    delete _prefix ;
    delete _designator ;
    delete _expression ;
    // predef table is static. Don't delete here
}

unsigned
VhdlAttributeName::IsPredefinedAttribute(const char *name)
{
    if (!_predef_table) InitPredefAttrTable() ;
    // Return the token (not-null for predefined attributes)
    unsigned long rlong = (unsigned long) _predef_table->GetValue(name) ;
    return (unsigned)rlong;
}
unsigned
VhdlAttributeName::IsPredefinedAttributeWithOneArg(const char *name)
{
    unsigned token = IsPredefinedAttribute(name) ;

    // Check which attributes need one argument
    switch(token) {
    case VHDL_ATT_left :
    case VHDL_ATT_right :
    case VHDL_ATT_high :
    case VHDL_ATT_low :
    case VHDL_ATT_ascending :
    case VHDL_ATT_image :
    case VHDL_ATT_value :
    case VHDL_ATT_pos :
    case VHDL_ATT_val :
    case VHDL_ATT_succ :
    case VHDL_ATT_pred :
    case VHDL_ATT_leftof :
    case VHDL_ATT_rightof :
    case VHDL_ATT_range :
    case VHDL_ATT_reverse_range :
    case VHDL_ATT_length :
    case VHDL_ATT_delayed :
    case VHDL_ATT_stable :
    case VHDL_ATT_quiet :
    //case VHDL_ATT_transaction :
    //case VHDL_ATT_event :
    //case VHDL_ATT_active :
    //case VHDL_ATT_last_event :
    //case VHDL_ATT_last_active :
    //case VHDL_ATT_last_value :
    //case VHDL_ATT_driving :
    //case VHDL_ATT_driving_value :
    //case VHDL_ATT_simple_name :
    //case VHDL_ATT_instance_name :
    //case VHDL_ATT_path_name :
    case VHDL_ATT_behavior :
    case VHDL_ATT_structure :
        return 1 ;
    default :
        return 0 ;
    }
}

/*static*/ void
VhdlAttributeName::InitPredefAttrTable()
{
    if (_predef_table) return ;

    _predef_table = new Map(STRING_HASH, 37) ;

    _predef_table->Insert("base",(void*)VHDL_ATT_base) ;
    _predef_table->Insert("left",(void*)VHDL_ATT_left) ;
    _predef_table->Insert("right",(void*)VHDL_ATT_right) ;
    _predef_table->Insert("high",(void*)VHDL_ATT_high) ;
    _predef_table->Insert("low",(void*)VHDL_ATT_low) ;
    _predef_table->Insert("ascending",(void*)VHDL_ATT_ascending) ;
    _predef_table->Insert("image",(void*)VHDL_ATT_image) ;
    _predef_table->Insert("value",(void*)VHDL_ATT_value) ;
    _predef_table->Insert("pos",(void*)VHDL_ATT_pos) ;
    _predef_table->Insert("val",(void*)VHDL_ATT_val) ;
    _predef_table->Insert("succ",(void*)VHDL_ATT_succ) ;
    _predef_table->Insert("pred",(void*)VHDL_ATT_pred) ;
    _predef_table->Insert("leftof",(void*)VHDL_ATT_leftof) ;
    _predef_table->Insert("rightof",(void*)VHDL_ATT_rightof) ;
    _predef_table->Insert("range",(void*)VHDL_ATT_range) ;
    _predef_table->Insert("reverse_range",(void*)VHDL_ATT_reverse_range) ;
    _predef_table->Insert("length",(void*)VHDL_ATT_length) ;
    _predef_table->Insert("delayed",(void*)VHDL_ATT_delayed) ;
    _predef_table->Insert("stable",(void*)VHDL_ATT_stable) ;
    _predef_table->Insert("quiet",(void*)VHDL_ATT_quiet) ;
    _predef_table->Insert("transaction",(void*)VHDL_ATT_transaction) ;
    _predef_table->Insert("event",(void*)VHDL_ATT_event) ;
    _predef_table->Insert("active",(void*)VHDL_ATT_active) ;
    _predef_table->Insert("last_event",(void*)VHDL_ATT_last_event) ;
    _predef_table->Insert("last_active",(void*)VHDL_ATT_last_active) ;
    _predef_table->Insert("last_value",(void*)VHDL_ATT_last_value) ;
    _predef_table->Insert("driving",(void*)VHDL_ATT_driving) ;
    _predef_table->Insert("driving_value",(void*)VHDL_ATT_driving_value) ;
    _predef_table->Insert("simple_name",(void*)VHDL_ATT_simple_name) ;
    _predef_table->Insert("instance_name",(void*)VHDL_ATT_instance_name) ;
    _predef_table->Insert("path_name",(void*)VHDL_ATT_path_name) ;
    _predef_table->Insert("behavior",(void*)VHDL_ATT_behavior) ;    // '87-only block attribute
    _predef_table->Insert("structure",(void*)VHDL_ATT_structure) ;  // '87-only block attribute
    _predef_table->Insert("element",(void*)VHDL_ATT_element) ; // VIPER #7502: vhdl_2008: new predefined attribute
    _predef_table->Insert("subtype",(void*)VHDL_ATT_subtype) ; // VIPER #7522: vhdl_2008: new predefined attribute
}

/*static*/ void
VhdlAttributeName::ResetPredefAttrTable()
{
    delete _predef_table ;
    _predef_table = 0 ;
}

VhdlEntityAspect::VhdlEntityAspect(unsigned entity_class, VhdlName *name)
  : VhdlName(),
    _entity_class(entity_class),
    _name(name)
{
    // Find the unit under this entity aspect, and test if it's the right class.
    // The unit name can be a :
    //    - entity alone
    //    - entity(architecture)
    //    - configuration
    VhdlIdDef *id = (_name) ? _name->EntityAspect() : 0 ;
    if (!id) return ;

    // Entity aspect is always called with a entity class. Test here if entity class matches.
    if (id->IsEntity() && _entity_class==VHDL_entity) return ;
    if (id->IsArchitecture() && _entity_class==VHDL_entity) return ; // Happens with indexed entity name
    if (id->IsConfiguration() && _entity_class==VHDL_configuration) return ;

    // Error out here :
    Error("%s is not a %s", id->Name(), EntityClassName(_entity_class)) ;
}
VhdlEntityAspect::~VhdlEntityAspect()
{
    delete _name ;
}

VhdlInstantiatedUnit::VhdlInstantiatedUnit(unsigned entity_class, VhdlName *name)
  : VhdlName(),
    _entity_class(entity_class),
    _name(name)
{
    // All entity aspect checks were moved to VhdlComponentInstantiationStatement::VhdlComponentInstantiationStatement
    // due to VIPER #2786
}
VhdlInstantiatedUnit::~VhdlInstantiatedUnit()
{
    delete _name ;
}
VhdlExternalName::VhdlExternalName(unsigned class_type, unsigned path_token, unsigned hat_count, VhdlName *external_path_name, VhdlSubtypeIndication *subtype_indication)
  : VhdlName(),
    _class_type(class_type),
    _path_token(path_token),
    _hat_count(hat_count),
    _ext_path_name(external_path_name),
    _subtype_indication(subtype_indication),
    _unique_id(0)
{
    if (_subtype_indication) (void) _subtype_indication->TypeInfer(0,0,VHDL_range,0) ;
}
VhdlExternalName::~VhdlExternalName()
{
    delete _ext_path_name ;
    delete _subtype_indication ;
}
unsigned VhdlExternalName::IsPackagePathName() const
{
    return (_path_token == VHDL_AT) ? 1: 0 ;
}
unsigned VhdlExternalName::IsAbsolutePathName() const
{
    return (_path_token == VHDL_DOT) ? 1: 0 ;
}
unsigned VhdlExternalName::IsRelativePathName() const
{
    return (_path_token == VHDL_ACCENT) ? 1: 0 ;
}
/*****************************************************************/
/************************* Functions *****************************/
/*****************************************************************/

unsigned VhdlName::IsAll() const                { return 0 ; }
unsigned VhdlDesignator::IsAll() const          { return 0 ; }
unsigned VhdlAll::IsAll() const                 { return 1 ; }

unsigned VhdlName::IsOthers() const             { return 0 ; }
unsigned VhdlDesignator::IsOthers() const       { return 0 ; }
unsigned VhdlOthers::IsOthers() const           { return 1 ; }

unsigned VhdlName::IsOpen() const               { return 0 ; }
unsigned VhdlDesignator::IsOpen() const         { return 0 ; }
unsigned VhdlOpen::IsOpen() const               { return 1 ; }

unsigned VhdlName::IsNull() const               { return 0 ; }
unsigned VhdlDesignator::IsNull() const         { return 0 ; }
unsigned VhdlNull::IsNull() const               { return 1 ; }

unsigned VhdlName::IsUnaffected() const         { return 0 ; }
unsigned VhdlDesignator::IsUnaffected() const   { return 0 ; }
unsigned VhdlUnaffected::IsUnaffected() const   { return 1 ; }

unsigned VhdlDesignator::IsUsed() const         { return _single_id ? _single_id->IsUsed() || _single_id->IsConcurrentUsed() || _single_id->IsPort() : 1 ; } // Viper #5700

unsigned VhdlName::IsConstant() const           { return 0 ; }
unsigned VhdlDesignator::IsConstant() const     { return (_single_id && _single_id->IsGeneric()) ? 1 : 0 ; }
unsigned VhdlLiteral::IsConstant() const        { return 1 ; }
unsigned VhdlStringLiteral::IsConstant() const  { return 1 ; }
unsigned VhdlCharacterLiteral::IsConstant() const  { return 1 ; }

/*
 ******************* Name ***************************
 * Given a designator, return a char * for it. Used when
 * we need to get a name from a designator to denote
 * something. e.g in a selected_name :
 *       use ieee.std_logc_1164."+"
 ******************* Name **************************
*/

const char *VhdlCharacterLiteral::Name() const  { return _name ; }
const char *VhdlIdRef::Name() const             { return _name ; }
const char *VhdlStringLiteral::Name() const     { return _name ; }

const char *VhdlSignaturedName::Name() const    { return (_name) ? _name->Name() : 0 ; }
const char *VhdlAll::Name() const               { return "all" ; }
const char *VhdlOthers::Name() const            { return "others" ; }
const char *VhdlUnaffected::Name() const        { return "unaffected" ; }

// Viper 7480 7481 : Mark is used in actual outputs from formals
void VhdlIdRef::MarkUsedFlag(SynthesisInfo *info)
{
    if (_single_id) _single_id->SetUsed(info) ;
}

void VhdlIndexedName::MarkUsedFlag(SynthesisInfo *info)
{
    if (_prefix) _prefix->MarkUsedFlag(info) ;
}

void VhdlSelectedName::MarkUsedFlag(SynthesisInfo *info)
{
    if (_suffix) _suffix->MarkUsedFlag(info) ;
}

// Viper #7042: only for (aggregate of) string literals, returns allocated string
char *VhdlStringLiteral::ExprString() const     { return Strings::save(_name) ; } // Viper #7042
char *VhdlCharacterLiteral::ExprString() const  { return Strings::save(_name) ; } // Viper #7042

/*
 ******************* FindObjects ***************************
 * Given a name, accumulate the named entities (VhdlIdDef's)
 * into set 'result'. Uniqueness is quaranteed by the Set.
 * Return 1 if successful. 0 if there was an error.
 * This searches for the objects returned for an expanded name only.
 ******************* FindObjects **************************
*/
unsigned VhdlName::FindObjects(Set & /*result*/, unsigned /*for_use_clause*/)
{
    return 0 ; // default
}

unsigned VhdlDesignator::FindObjects(Set &result, unsigned /*for_use_clause*/)
{
    // See if we have been here before :
    if (_single_id) {
        (void) result.Insert(_single_id) ;
        return 1 ;
    }

    // Use the present scope :
    if (!_present_scope) return 0 ;

    // Viper #3573/#3673 : downcase string literal (operator names)
    char *saved_name = 0 ;
    if (IsStringLiteral()) {
        saved_name = Strings::save(_name) ;
        (void) Strings::strtolower(saved_name) ;
    }

    // Lookup in scope
    if (!_present_scope->FindAll((saved_name) ? saved_name : _name, result)) {
        Strings::free(saved_name) ;
        return 0 ;
    }
    Strings::free(saved_name) ;
    if (result.Size()==1) {
        _single_id = (VhdlIdDef*)result.GetAt(0) ;
    }
    return 1 ;
}

unsigned VhdlSelectedName::FindObjects(Set &result, unsigned /*for_use_clause*/)
{
    if (_unique_id) {
        (void) result.Insert(_unique_id) ;
        return 1 ;
    }
    if (!_prefix || !_suffix) return 0 ;

    // Find the objects for if the selected name is an expanded name.
    // LRM 6.3 : only return 'expanded names' :
    // Don't return anything if it is a record field or access type thing.

    // Prefix of a selected (expanded) name must denote a single object ;
    // selected name should be an expanded name, and
    // (so) prefix should denote a single (containing)
    // object...
    // For records or access values, the prefix could be something
    // other than an object (e.g. an indexed name.
    // In that case, FindSingleObject will return 0.

    // LRM 6.3 : since 'expanded names' prefix cannot contain a function call,
    // there should be no overloading in the prefix.

    VhdlIdDef *prefix_id = _prefix->FindSingleObject() ;
    if (!prefix_id) {
        return 0 ;
    }

    // LRM 6.3 : A record name is NOT an extended name. NO OBJECTS returned.
    if (prefix_id->IsRecord()) return 0 ;

    // LRM 6.3 : check if prefix belongs to an access type and suffix is 'all' :

    if (_suffix->IsAll() && prefix_id->Type() && prefix_id->Type()->IsAccessType()) return 0 ;

    VhdlIdDef *elem ;
    if (prefix_id->IsLibrary()) {
        // Get the primary unit from this library
        // library 'work' has special meaning
        VhdlLibrary *l ;
        if (Strings::compare(prefix_id->Name(),"work")) {
            // use present work library
            l = vhdl_file::GetWorkLib() ;
        } else {
            // Find the library by name
            l = vhdl_file::GetLibrary(prefix_id->Name(),1) ;
        }
        VERIFIC_ASSERT(l) ;

        VhdlDesignUnit *pu ;
        if (_suffix->IsAll()) {
            // Accumulate all primary units from this library
            unsigned j ;
            for (j=0; j < l->NumOfPrimUnits(); j++) {
                pu = l->PrimUnitByIndex(j) ;
                VERIFIC_ASSERT(pu) ;
                elem = pu->Id() ;
                (void) result.Insert(elem) ;
                // Also register the design unit dependency :
                // Don't do this. We are not really using the entire scope of each design unit.
                // we are just using the design unit name. CHECK ME : can stale pointers result from this ?
                // Probably yes. Maybe we should eliminate design unit ids from the _use_clauses in scopes after scope pop-ing.
                //
                // _present_scope->Using(elem->LocalScope()) ;
            }
        } else {
            // Find primary unit by name
            pu = l->GetPrimUnit(_suffix->Name()) ;
            // Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
            if (!pu) {
                // Look into Verilog side :
                pu = vhdl_file::GetVhdlUnitFromVerilog(prefix_id->Name(), _suffix->Name()) ;
                if (!pu) {
                    _suffix->Error("'%s' is not compiled in library %s", _suffix->Name(), l->Name()) ;
                    return 0 ;
                }
            }
            elem = pu->Id() ;
            (void) result.Insert(elem) ;

            // Also register the design unit dependency.
            // Needed since (in direct entity instantiations), we store formals of this entity in the association lists.
            // Also needed because of entity aspect LRM 5.2.1.1 : at analysis time, the design unit containing the entity aspect depends on the denoted entity declaration.
            if (_present_scope) _present_scope->Using(elem->LocalScope()) ;
        }
        // Don't set 'unique_id' for units in library.
        // Units get destroyed sometimes, and we don't allow stale pointers
        return 1 ;
    }

    // Check if this is a named, declared entity (with a scope)
    VhdlScope *local_scope = prefix_id->LocalScope() ;
    // For package instantiation, get local scope of uninstantiated package to find next element
    if (prefix_id->IsPackageInst()) {
        //local_scope = prefix_id->GetUninstantiatedPackageScope() ;
        // Set actual type of generic types from generic map aspect (need to know data types
        // of objects defined within uninstantiated package
        prefix_id->SetActualTypes() ;
    }
    VhdlIdDef *prefix_type = prefix_id->Type() ;
    if (prefix_id->IsProtected() && prefix_type && prefix_type->LocalScope()) local_scope = prefix_type->LocalScope() ;
    if (prefix_id->IsFunction() && _present_scope && !_present_scope->IsContainedIn(local_scope)) {
        // Viper 4983 LRM 6.1 says if the _prefix is a function call
        // then it denotes the result of the function call
        // the scope therefore must be updated accordingly
        // The local declarions can only be used within the construct
        VhdlSpecification *spec = prefix_id->Spec() ;
        VhdlName *return_name = spec ? spec->GetReturnType() : 0 ;
        VhdlIdDef *return_id = return_name ? return_name->GetId() : 0 ;
        VhdlScope *id_scope = return_id ? return_id->LocalScope() : 0 ;
        if (id_scope) local_scope = id_scope ;
    }

    if (local_scope) {
        MapItem *item ;
        MapIter mi ;
        Map *decl_area = local_scope->DeclArea() ;
        if (_suffix->IsAll()) {
            // Accumulate all locally declared entities in the scope
            FOREACH_MAP_ITEM(decl_area, mi, 0, &elem) {
                if (!elem) continue ;
                // library and design-unit objects are not really considered 'declared'
                // inside the scope, so should not be included :
                // These might be there due to the way we store them for visibility in VhdlScope.
                if (elem->IsLibrary() || elem->IsDesignUnit()) continue ;
                (void) result.Insert(elem) ;
            }
        } else {
            // Accumulate the entities by name from the scope
            char *suffix_str = Strings::save(_suffix->Name()) ;
            if (_suffix->IsStringLiteral()) suffix_str = Strings::strtolower(suffix_str) ; // Viper #5538
            FOREACH_SAME_KEY_MAPITEM(decl_area, suffix_str, item) {
                elem = (VhdlIdDef*)item->Value() ;
                (void) result.Insert(elem) ;
            }
            // Vhdl2008 LRM Section 8.3: entity name can be used as a prefix of an extendedname
            // such names can only be used in the architecture defining the entity.
            if (result.Size()==0 && IsVhdl2008()) {
                VhdlIdDef *local_scope_id = local_scope->GetOwner() ;
                if (local_scope_id->IsEntity()) {
                    VhdlScope *top_scope = _present_scope ? _present_scope->TopScope() : 0 ;
                    // Allow this for only architecture: Check if owner of top scope is architecture
                    // As we set the extended scope for configurations as well.
                    VhdlIdDef *top_id = top_scope ? top_scope->GetOwner() : 0 ;
                    if (top_scope && top_id && top_id->IsArchitecture() && top_scope->GetExtendedDeclAreaScope() == local_scope) {
                        decl_area = _present_scope ? _present_scope->DeclArea() : 0 ;
                        FOREACH_SAME_KEY_MAPITEM(decl_area, suffix_str, item) {
                            elem = (VhdlIdDef*)item->Value() ;
                            (void) result.Insert(elem) ;
                        }
                    }
                }
            }
            Strings::free(suffix_str) ;
            // Error out if there is nothing in this ?
            if (result.Size()==0) Error("%s is not declared in %s", _suffix->Name(), prefix_id->Name()) ;
        }
        // If the name was unique, set single_id for next time here
        if (result.Size()==1) {
            _unique_id = (VhdlIdDef*)result.GetAt(0) ;
        }

        // NOTE: We set '_unique_id', but not the IdDef pointer in the (IdRef) '_suffix' field.
        // So rely on '_unique_id' but not on the '_suffix' IdRef id.

        // This was a named entity with scope
        // Also register the scope dependency :
        if (_present_scope) _present_scope->Using(local_scope, this) ;

        return 1 ;
    }

    Error("illegal selected name prefix") ;
    return 0 ;
}

unsigned VhdlArrayResFunction::FindObjects(Set &result, unsigned for_use_clause)
{
    if (_res_function) return _res_function->FindObjects(result, for_use_clause) ;
    return 0 ;
}
unsigned VhdlExternalName::FindObjects(Set &result, unsigned /*for_use_clause*/)
{
    if (_unique_id) {
        (void) result.Insert(_unique_id) ;
        return 1 ;
    }
    /*
    if (_path_token == VHDL_AT) {
        Set ids(POINTER_HASH) ;
        if (_ext_path_name) _ext_path_name->FindObjects(ids, 0) ;
        if (ids.Size() == 1) {
            _unique_id = (VhdlIdDef*)ids.GetLast() ;
            (void) result.Insert(_unique_id) ;
            return 1 ;
        }
    }
    */
    // Needs to be resolved during elaboration
    return 0 ;
}

VhdlIdDef* VhdlArrayResFunction::FindSingleObject()
{
    if (_res_function) return _res_function->FindSingleObject() ;
    return 0 ;
}

unsigned VhdlName::FindObjectsLocal(VhdlScope *scope, Set &result)
{
    if (!scope) return 0 ;

    // Viper #3573/#3673 : downcase string literal (operator names)
    char *saved_name = 0 ;
    if (IsStringLiteral()) {
        saved_name = Strings::save(Name()) ;
        (void) Strings::strtolower(saved_name) ;
    }

    // Lookup in scope
    (void) scope->FindAllLocal(saved_name ? saved_name : Name(), result) ;
    Strings::free(saved_name) ;

    return result.Size() ;
}

unsigned VhdlSignaturedName::FindObjectsLocal(VhdlScope *scope, Set &result)
{
    // Accumulate all directly visible VhdlIdDefs by this name, and by taking into
    // consideration their signatures.  If no signature exists for this signatured name,
    // then just accumulate by name.
    if (!_name || !_signature || !scope) return 0 ;

    Set local_ids(POINTER_HASH) ;
    (void) _name->FindObjectsLocal(scope, local_ids) ;

    // FIX ME : We are using FindSingleObject in FillProfile, and there
    //          is a chance that _present_scope is NULL, so that's why
    //          we do a temporary reassignment of _present_scope here.
    VhdlScope *orig_scope = _present_scope ;
    _present_scope = scope ;
    VhdlOperatorId profile(0) ; // name is not important
    _signature->FillProfile(profile) ;
    _present_scope = orig_scope ;

    SetIter si ;
    VhdlIdDef *id = 0 ;
    FOREACH_SET_ITEM(&local_ids, si, &id) {
        if (profile.IsHomograph(id)) {
            // Signatures match, so insert id into result set.
            (void) result.Insert(id) ;
        }
    }

    return result.Size() ;
}

/****************************************************
 *   FindSingleObject.
 * In many case in VHDL, we know that there should be
 * a single object (entity, architecture, type_mark,
 * discrete_range or single named object in a name.
 * In such cases, use 'FindSingleObject()' on the name.
 * It relies on the knowledge that there SHOULD
 * be a single, valid object represented in the name.
 * It will error out and return 0 if this is not the case.
 ****************************************************/

VhdlIdDef *VhdlName::FindSingleObject()
{
    // Don't error out on this : it's a indexed name or so.
    // Most likely a indexed name as selected name prefix called via FindObjects()
    // FIX ME this is still somewhat messy..
    // Error("ambiguous name") ;
    return 0 ; // default ; should never happen...
}

VhdlIdDef *VhdlDesignator::FindSingleObject()
{
    // Check if we have been here before
    if (_single_id) return _single_id ;

    // Viper #3573/#3673 : downcase string literal (operator names)
    char *saved_name = 0 ;
    if (IsStringLiteral()) {
         saved_name = Strings::save(_name) ;
        (void) Strings::strtolower(saved_name) ;
    }
    // lookup in scope :
    _single_id = (_present_scope) ? _present_scope->FindSingleObject(saved_name ? saved_name : _name) : 0 ;
    Strings::free(saved_name) ;

    // Viper 5218. If the pu is without the library binding search it in the working library
    if (!_single_id && !IsAll() && !IsOthers() && !IsUnaffected() && !IsStringLiteral() && !IsCharacterLiteral()) {
        //implies is idref.
        VhdlLibrary *l = vhdl_file::GetWorkLib() ;
        VhdlPrimaryUnit *pu = vhdl_file::GetVhdlUnitFromVerilog(l->Name(), _name) ;
        _single_id = pu ? pu->Id() : 0 ;
        // VIPER #6774: Moved this below out of this if block for both Verilog and VHDL:
        // Viper 6181: add the pu converted from Verilog module in the using scope of present scope
        //if (_present_scope && _single_id) _present_scope->Using(_single_id->LocalScope()) ;
    }

    // VIPER #6774: Need to mark the scope of the id to be used in the present scope in case
    // this id is a primary unit. So, moved this here to have it work both for Verilog and VHDL.
    // Viper 6181: add the pu converted from Verilog module in the using scope of present scope
    if (_single_id && (_single_id->GetPrimaryUnit() && !_single_id->IsPackageInst()) && _present_scope) {
        _present_scope->Using(_single_id->LocalScope()) ;
    }

    if (!_single_id) Error("%s is not declared", _name) ;
    return _single_id ;
}

VhdlIdDef *VhdlSelectedName::FindSingleObject()
{
    if (_unique_id) return _unique_id ;

    // Do this via 'FindObjects'. It does all the work already
    Set r(POINTER_HASH) ;
    if (!FindObjects(r,0)) {
        return 0 ; // Nothing there by this name
    }
    if (r.Size()>1) {
        Error("ambiguous selected name") ;
        return 0 ;
    }
    _unique_id = (VhdlIdDef*)r.GetLast() ;
    return _unique_id ;
}
VhdlIdDef *VhdlExternalName::FindSingleObject()
{
    if (_unique_id) return _unique_id ;

    // Do this via 'FindObjects'.
    Set r(POINTER_HASH) ;
    if (!FindObjects(r,0)) {
        return 0 ; // Nothing there by this name
    }
    _unique_id = (r.Size()==1) ? (VhdlIdDef*)r.GetLast(): 0 ;
    return _unique_id ;
}

// VIPER 3655 : pick up the resolution function from any subtype constraint
VhdlIdDef *
VhdlIdRef::FindResolutionFunction() const
{
    return (_single_id) ? _single_id->ResolutionFunction() : 0 ;
}

VhdlIdDef *
VhdlArrayResFunction::FindResolutionFunction() const
{
    return (_res_function) ? _res_function->FindResolutionFunction() : 0 ;
    return 0 ;
}

VhdlIdDef *
VhdlIndexedName::FindResolutionFunction() const
{
    if (!_is_index_constraint) return 0 ; // focus only on index constraints
    // return resolution function from the prefix id :
    return (_prefix_id) ? _prefix_id->ResolutionFunction() : 0 ;
}

VhdlIdDef *
VhdlSelectedName::FindResolutionFunction() const
{
    // return resolution function from the unique id :
    return (_unique_id) ? _unique_id->ResolutionFunction() : 0 ;
}

VhdlIdDef *
VhdlRecordResFunction::FindResolutionFunction() const
{
    return 0 ;
}
VhdlIdDef *
VhdlExternalName::FindResolutionFunction() const
{
    // return resolution function from the unique id :
    return (_unique_id) ? _unique_id->ResolutionFunction() : 0 ;
}
/*
 ******************* TypeInfer ***************************
 * Given a name, accumulate the named entities (VhdlIdDef's)
 * Then, find the types for these names.
 * If expected_type is there, filter out the ones that
 * do not match the expected type.
 *
 * If in the end, there is ONE id left over, set it as
 * the 'type-infered' id in this name, and return its type.
 * Error out if NO id's are left over, or if 'expected_type'
 * is not given, and there are multiple id's that match.
 ******************* TypeInfer **************************
*/

VhdlIdDef *
VhdlAll::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return expected_type ;
}

VhdlIdDef *
VhdlOthers::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return expected_type ;
}

VhdlIdDef *
VhdlUnaffected::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return expected_type ;
}

VhdlIdDef *
VhdlInteger::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    // Viper 7619 : Error out for integer in range constraint of subtype indication.
    if (environment == VHDL_interfacerange || environment == VHDL_range || environment == VHDL_recordrange || environment == VHDL_subtyperange) Error("range expression expected here") ;
    // Integer. Matches any integer type. Return UNIVERSAL_INTEGER if no guidence is given
    if (expected_type) {
        // Test against universal_integer
        if (expected_type->IsIntegerType()) {
            // OK. implicit conversion to the expected type
            return expected_type ;
        } else {
            Error("type %s does not match with the integer literal", expected_type->Name()) ;
            return UniversalInteger() ;
        }
    } else {
        if (return_types) {
            (void) return_types->Insert(UniversalInteger()) ;
        }
        return UniversalInteger() ;
    }
}

// To get attribute index value, before elaboration
verific_int64 VhdlInteger::EvaluateConstantInteger() { return _value ; }

verific_int64 VhdlPhysicalLiteral::EvaluateConstantInteger()
{
    // We're assuming that this routine only gets called from secondary
    // unit declaration of a physical type.

    // Section 3.1.3 states that the abstract literal of a secondary unit
    // declartion must be an integer literal.

    // Implied absense of a value : value is 1
    verific_int64 literal = (_value) ? _value->EvaluateConstantInteger() : 1 ;
    // Now get the position number of the unit
    VhdlIdDef *unit = (_unit) ? _unit->FindSingleObject() : 0 ;
    if (!unit) return 1 ; // ??
    return ((verific_int64)unit->Position() * literal) ;
}

// If IdRef points to enumeration literal, then we return 'integer' value
// by its position in the type. If it is not IdRef of enum type, behavior
// of this API is same as VhdlExpression::EvaluateConstantInteger()
verific_int64 VhdlIdRef::EvaluateConstantInteger()
{
    VhdlIdDef *type_id = (_single_id) ? _single_id->Type() : 0 ;
    if (!type_id || !type_id->IsEnumerationType()) {
        // Keep the same behavior as VhdlExpression::EvaluateConstantInteger()
        Error("expected a constant integer literal") ;
        return 1 ;
    }

    return (_single_id) ? (verific_int64)_single_id->Position() : 0 ;
}

// Check for Legal Deffered Constant Viper 3505
// This is a completely new traversal routine (starting at VhdlRange/VhdlExpression) because LRM 2.6,
// line 455 specifies a few cases where deferred constants ARE allowed, even locally.For example,
// unresolved deferred constants are allowed in a (component) generic initial value, but not in it's
// 'subtype indication'. Alternatively, we could adjust the 'environment' flag for TypeInfer for such exceptions.

void VhdlIdRef::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    // If id->Decl() is not defined then the id name is used before its full declaration.
    //  this is illegal for default expression excpet for local generic, port and formal
    // parameter. LRM section2.6

    VhdlIdDef *id = GetId() ;
    if (!id || !id->IsConstant() || id->IsInput()) return ;
    // IsInput is checked to exclude use of constant function input ports

    unsigned illegal_decl = 0 ;

    // Here id is a deferred constant as without its full definition at the moment.
    if (!id->Decl() && !id->HasInitAssign()) illegal_decl = 1 ; // Viper 3505/6607

    // Viper 4510 illegal use of deferred const in its initialization.
    // In this case id->Decl() is defined. Yet we should error out
    if (id->Decl() && unit_ids) {
        if (unit_ids->Get(id) || unit_ids->Get(id->Decl())) illegal_decl = 1 ;
    }

    if(illegal_decl) {
        // Check if the deferred constant is declared in this package. OW do not throw error
        // This is consistent with popular simulators
        VhdlIdDef * package_id = _present_scope ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        VhdlScope * package_scope = package_id ? package_id->LocalScope() : 0 ;
        VhdlIdDef *this_package_id = package_scope ? package_scope->FindSingleObjectLocal(id->Name()) : 0 ;
        if (id == this_package_id) init_assign->Error("illegal use of deferred constant %s", id->Name()) ;
    }
}
void VhdlIndexedName::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    // Check For Deffered Constant VIPER 4368
    unsigned i ;
    VhdlDiscreteRange *assoc_elem ;
    VhdlIdDef *scope_id = _present_scope ? _present_scope->GetContainingPrimaryUnit() : 0 ;
    if (scope_id && scope_id->IsPackage()) {
        FOREACH_ARRAY_ITEM(_assoc_list, i, assoc_elem) {
            // Viper #8385: null check
            if (assoc_elem) assoc_elem->CheckLegalDefferedConst(init_assign, unit_ids) ;
        }
    }
}
void
VhdlName::CheckIdentifierUsage(VhdlIdDef *id, unsigned environment) const
{
    // Check how in this name, the identifier 'id' is used.

    // This routine runs during type-inference of a name.
    // Type inference runs during analysis, and the exact way in which a
    // name is used is passed in as the 'environment'.
    // 'environment' can be
    //     VHDL_read, if the name is used in an expression,
    //     VHDL_VARASSIGN if the name is used in a target of a variable assignment
    //     VHDL_SIGASSIGN if the name is used in a target of a signal assignment
    //     VHDL_range if the name is used in a subtype indication
    //     VHDL_record if the name is used as a record element name.

    // 'id' is representative for the entire name, which could be
    // a simple identifier, an indexed name, a selected name etc.
    // Special 'environment' checks are done in the individual names.

    if (!id) return ; // No identifier to check.

    // Set used/assigned flag.
    // Viper 5821, 5298. Only set SetUsed flag here the appropriate error about
    // illegal usage of identifier from the same interface VhdlInterfaceId::SetObjectKind
    // The environment VHDL_interfaceprefixread is used to understand that we are checking
    // the prefix of an attributename.
    if (environment == VHDL_READ || environment == VHDL_interfaceprefixread || environment == VHDL_interfaceread || environment == VHDL_subtyperangebound) id->SetUsed() ;
    if (environment == VHDL_SIGUPDATE || environment == VHDL_VARUPDATE) id->SetAssigned() ;

    // Check environment : How is this identifier used...
    if (environment == VHDL_VARUPDATE && id->IsSignal()) {
        Error("use <= to assign to signal %s",id->Name()) ;
    } else if (environment == VHDL_SIGUPDATE && id->IsVariable()) {
        Error("use := to assign to variable %s",id->Name()) ;
    } else if (environment == VHDL_VARUPDATE && !id->IsVariable()) {
        Error("%s is not a variable", id->Name()) ;
    } else if (environment == VHDL_SIGUPDATE && !id->IsSignal()) {
        Error("%s is not a signal", id->Name()) ;
    }

    // VIPER #8148 : Produce warning if signal is used in subtype indication
    if (id->IsSignal() && ((environment == VHDL_subtyperange) || (environment == VHDL_subtyperangebound) ||
        (environment == VHDL_interfacerange) || (environment == VHDL_ACCESS_SUBTYPE))) {
        Warning("signal %s is used in subtype-indication/type-definition", id->Name()) ;
    }
    // VIPER #2893 : Produce error if id is of incomplete type and it is used
    // context other than subtype indication of access type (L.R.M section 3.3.1)
    if ((environment != VHDL_ACCESS_SUBTYPE) && id->IsDefinedIncompleteType()) {
        Error("illegal use of incomplete type") ;
    }
    // LRM 4.3.2 read/update checks.
    if (id->IsOutput() && (environment == VHDL_READ ||  environment == VHDL_interfaceread || environment == VHDL_subtyperangebound)) {
        if (!IsVhdlPsl() && !IsVhdl2008()) { // In PSL dialect, this is allowed.
            // It is intended that a port of mode out should be read only for
            // passive activities, that is, for functionality used for verification
            // purposes within monitors or property or assertion checkers. If the value of
            // an output port is read to implement the algorithmic behavior of a description,
            // then the port should be of mode buffer. Lrm Section 6.5.2
            Error("cannot read from 'out' object %s ; use 'buffer' or 'inout'", id->Name()) ;
        }
    }
    if (id->IsLinkage() && (environment == VHDL_READ ||  environment == VHDL_interfaceread || environment==VHDL_VARUPDATE || environment==VHDL_SIGUPDATE || environment == VHDL_subtyperangebound)) {
        Error("cannot read or update 'linkage' object %s",id->Name()) ;
    }
    if (id->IsInput() && (environment==VHDL_VARUPDATE || environment==VHDL_SIGUPDATE)) {
        Error("cannot update 'in' object %s",id->Name()) ;
    }

    // Check record element if we expect that :
    if (environment == VHDL_record && !id->IsRecordElement()) {
        Error("%s is not a record element", id->Name()) ;
    }

    // In 'range' we expect a subtype indication. With an identifier, that can only be a (sub)type.
    if (((environment == VHDL_range) || (environment == VHDL_interfacerange) || (environment == VHDL_ACCESS_SUBTYPE) || (environment == VHDL_subtyperange)) &&
        !id->IsType()) { // &&
        // VIPER #8085: Donot allow record element in 'range'. It is allowed
        // only in record constraint and for that case we will pass 'VHDL_record'
        // environment
        //(!IsVhdl2008() || !id->IsRecordElement())) { // Post Vhdl2008 record elements can be constrained in var/signal declaration
        // A subtype indication 'range' allows only types :
        Error("%s is not a type", id->Name()) ;
    }
    // VIPER #8085: Allow record element only in record constraint
    if ((environment == VHDL_recordrange) &&
        !id->IsType() && !id->IsRecordElement()) {
        Error("%s is not a type", id->Name()) ;
    }

    // Viper 5298,5738 Issue error for objects using signals/objects
    // defined in the same interface. This is done under the environment
    // Vhdl_interfaceread. This enviorment is specially created to
    // understand that this call came from VhdlInterfaceDecl's constructor
    // Cannot 'read' from a non-object (that does not have a 'type').
    if (environment == VHDL_READ ||  environment == VHDL_interfaceread || environment == VHDL_subtyperangebound) {
        // Enumerate all classes that are allowed to be 'read' directly, as an identifier..
        if (!id->IsConstant() && !id->IsSignal() && !id->IsVariable() &&
            !id->IsEnumerationLiteral() && !id->IsPhysicalUnit() &&
            !id->IsSubprogram() &&
            !id->IsRecordElement() &&
            !id->IsType()) {
            // This object does not belong in an expression..
            // THis is the place for error for viper 5298 also
            Error("%s is illegal in an expression", id->Name()) ;
        }
        // External signal access from within pure/impure function is moved to IdRef::TypeInfer
    }

    // VIPER #2286 : Check if global signal is accessed inside pure function, if so give error
    // VIPER #2420 : Add null check for '_present_scope'
    // VIPER #2684 : LRM 2.2 states "a reference to an explicitly declared signal or variable".
    // A reference can be the prefix of an attribute (like 'length), which has no 'environment' set at all.
    // So, we need to extend the test to include all 'environments' (not just VHDL_READ), and to variables as well.
    //VhdlIdDef *owner_id = _present_scope ? _present_scope->GetOwner() : 0 ;
    // VIPER #4219 : Reference can be inside inner scope of subprogram, so check
    // whether reference is within subprogram.
    // VIPER 3414/3412 : Much more refined analysis of what 'external' declaration means, and propagation of impure access to calls of subprograms
    VhdlScope *subprog_scope = _present_scope ? _present_scope->GetSubprogramScope(): 0 ;

    //if (subprog_scope && (id->IsSignal() || id->IsVariable()) && !subprog_scope->IsDeclaredHere(id)) {
    // VIPER #6801 : Consider file also to detect impure access.
    if (subprog_scope && (id->IsSignal() || id->IsVariable() || id->IsFile())) { // && !subprog_scope->IsDeclaredHere(id)) {
        // Here, we access a signal or variable in a subprogram,
        // and the signal/variable is not locally declared (in that subprogram).
        // Do a thorough scan to see if the identifier is really declared outside the current subprogram declarative area.

        // We need to know if this identifier is really declared outside the declarative area of the 'outer' subprogram.
        // That (careful determination of 'id_declared_outside') is done after finding simulator behavior of 'variant6' in VIPER 3414
        // Also need this for VIPER 4585 (signal assignment check) below.
        unsigned id_declared_outside = 0 ;
        if (!subprog_scope->IsDeclaredHere(id)) {
            VhdlScope *scan_scope = _present_scope ; // start scan from the present scope.
            VhdlIdDef *scan_owner = 0 ;
            while (scan_scope) {
                // Check if id is declared here
                if (scan_scope->IsDeclaredHere(id)) {
                    break ; // found id in this scope. No need to go further up.
                }

                // Get the 'owner' of this scope
                scan_owner = scan_scope->GetOwner() ;

                // Check if the owner is a pure function.
                // Do this check only if this is the immediate enclosing subprogram.
                // If we are already outside the immediate enclosing subprogram, then we find illegal access via the call (via SetHasImpureAccess() flag).
                // This is done after we found simulator behavior on 'variant1' in VIPER 3414.
                if (scan_scope==subprog_scope) {
                    VhdlSpecification *spec = (scan_owner && scan_owner->IsFunction()) ? scan_owner->Spec() : 0 ;
                    if (spec && !spec->IsImpure()) {
                        id_declared_outside = 1 ;
                        // VIPER 2286, 2684, 4219 3414
                        Error("cannot access '%s' from inside pure function '%s'.", id->Name(), scan_owner->Name()) ;
                        break ;
                    }
                }

                // If this scope is a subprogram itself, then before we go up one level,
                // check if the scope we go to still is inside a subprogram.
                unsigned leaving_subprog_scope = (scan_owner && (scan_owner->IsFunction() || scan_owner->IsProcedure())) ? 1 : 0 ;

                // Go up one level in scope :
                scan_scope = scan_scope->Upper() ;

                if (!scan_scope || (leaving_subprog_scope && !scan_scope->GetSubprogramScope())) {
                    // we dropped out of the upper most subprogram declaration. id is certainly declared outside outer subprogram declarative region :
                    id_declared_outside = 1 ;
                    break ; // stop the scan
                }
            }
        }

        // Either way (if this is a function or a procedure), flag it as having 'impure' access :
        // This way, when this subprogram is called, we can consider it 'external' access (VIPER 3414, 3412).
        if (id_declared_outside) {
            subprog_scope->SetHasImpureAccess() ;
        }

        // VIPER 4585 : signal assignments (to non-local signals) are not allowed if not within a process:
            // LRM ch08/sc04/sb018/s010110 :
            // -- SENTENCE     : If a given procedure is declared by a declarative item
            // that is not contained within a process statement,
            // and a signal assignment statement appears in that procedure,
            // then the target of the assignment statement must be a formal parameter of the given procedure
            // or of a parent of that procedure, or an aggregate of such formal parameters.
            //
        if (id_declared_outside && (environment == VHDL_SIGUPDATE)) {
            // We have a signal assignment inside a subpogram, where the signal is not declared outside the subprogram decl.
            // Check if we are declared inside a process :
            VhdlScope *process_scope = _present_scope->GetProcessScope() ;
            if (!process_scope && (id->IsSignal() || id->IsVariable())) {
                // Not is a process, so this is illegal
                // subprogram not declared within a process.
                Error("cannot assign signal %s from within a subprogram", id->Name()) ;
            }
        }
        // VIPER #6801 : LRM 2.2 states 'A pure function must not contain a reference to an
        // explicitly declared file object
        if (id->IsFile()) {
            VhdlIdDef *subprog_id = subprog_scope->GetOwner() ;
            VhdlSpecification *func_spec = (subprog_id && subprog_id->IsFunction()) ? subprog_id->Spec() : 0 ;
            if (id_declared_outside) {
                if (func_spec && !func_spec->IsImpure()) {
                    Error("cannot access '%s' from inside pure function '%s'.", id->Name(), subprog_id->Name()) ;
                }
            } else {
                // If locally declared file is accessed from pure function, simulators produces warning
                subprog_scope->SetHasLocalFileAccess() ; // Accessing internal file object
                if (func_spec && !func_spec->IsImpure()) {
                    Warning("cannot access '%s' from inside pure function '%s'.", id->Name(), subprog_id->Name()) ;
                }
            }
        }
    }
}

VhdlIdDef *
VhdlReal::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    // Real. Matches any integer type. Return UNIVERSAL_REAL if no guidence is given
    if (expected_type) {
        // Test against universal_real
        if (expected_type->IsRealType()) {
            // OK. Implicit conversion to the expected type
            return expected_type ;
        } else {
            Error("type %s does not match with the real literal", expected_type->Name()) ;
            return UniversalReal() ;
        }
    } else {
        if (return_types) {
            (void) return_types->Insert(UniversalReal()) ;
        }
        return UniversalReal() ;
    }
}

VhdlIdDef *
VhdlStringLiteral::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    if (!_name) return 0 ;

    // single_id is the return type of the string, as previously infered
    if (_single_id) {
        // Fast determination of result type :
        if (return_types) (void) return_types->Insert(_single_id->BaseType()) ;
        return _single_id ; // Return string type that matches here
    }

    VhdlIdDef *elem_type ;

    // short-cut if expected type is known
    if (expected_type) {
        // If 'expected_type' is given we don't need to call upon all comma operators :
        elem_type = expected_type->ElementType() ;
        if (!elem_type || !elem_type->IsEnumerationType() || expected_type->Dimension() != 1) {
            // VIPER #7717 : When expected_type is vl_logic/vl_logic_vector, produce
            // different message to not specify vl_logic/vl_logic_vector in message text.
            if (!ErrorForVllogicExpectedType(expected_type, 0)) {
                Error("type %s does not match with a string literal", expected_type->Name()) ;
            }
            return expected_type ;
        }

        // Check that all characters are in this type.
        char buffer[4] ;
        elem_type = expected_type->ElementType() ;
        for (char* tmp = _name+1 ; *(tmp+1)!='\0'; tmp++) {
            sprintf(buffer,"'%c'",*tmp) ;
            if (elem_type && !elem_type->GetEnum(buffer)) {
                // VIPER #7717 : When expected_type is vl_logic/vl_logic_vector, produce
                // different message to not specify vl_logic/vl_logic_vector in message text.
                if (!ErrorForVllogicExpectedType(elem_type, buffer)) {
                    Error("character '%c' is not in element type %s", *tmp, elem_type->Name()) ;
                }
                break ;
            }
        }

        // Now set expected type as the 'single_id' :
        _single_id = expected_type ;

        // And return it as the (one and only) type match :
        if (return_types) (void) return_types->Insert(expected_type->BaseType()) ;
        return expected_type ;
    }

    // Note that we do not have an expected type here, so
    // we have no idea which type might be matching.
    // Must be one-dimensional array-of-character type
    // Gotta find all visible one-dimensional array types
    // Pick-up the , operator (as for aggregates) for that
    Set visible_operators(POINTER_HASH,11) ;
    SetIter si ;
    VhdlIdDef *oper ;
    VhdlIdDef *match = 0 ;
    VhdlIdDef *oper_type ;

    Set done_scopes(POINTER_HASH, 11) ; // hash table to avoid exponential explosion in FindCommaOperators.
    //_present_scope->FindAll(OperatorName(VHDL_COMMA), visible_operators) ;
    if (_present_scope) _present_scope->FindCommaOperators(visible_operators, done_scopes) ;

    // Now rule-out the , operators (aggregate operators) that do not match
    // Prune down the list, knowing that we need a 1-dimensional array
    // of character type, or we need the 'expected_type'

    FOREACH_SET_ITEM(&visible_operators, si, &oper) {
        oper_type = oper->Type() ;

        // Check that this is an array or dimension 1
        if (!oper_type || oper_type->Dimension()!=1) {
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        // Check if this has a enumeration type element
        elem_type = oper_type->ElementType() ;
        if (!elem_type || !elem_type->IsEnumerationType()) {
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        // match
        match = oper ;
    }

    if (match && visible_operators.Size()==1) {
        // Here, expected type matches. Must be the match,
        // since we can only determine from the context. So there should be only one match.
        // Just check characters
        char buffer[4] ;
        elem_type = match->ElementType() ;
        for (char* tmp = _name+1 ; *(tmp+1)!='\0'; tmp++) {
            sprintf(buffer,"'%c'",*tmp) ;
            if (elem_type && !elem_type->GetEnum(buffer)) {
                Error("character '%c' is not in element type %s", *tmp, elem_type->Name()) ;
                break ;
            }
        }

        // Set the resolved operator as single_id
        _single_id = match->Type() ;

        // Check that every character is in this type
        if (return_types && _single_id) (void) return_types->Insert(_single_id->BaseType()) ;
        return _single_id ;
    } else {
        if (return_types && visible_operators.Size()) {
            // Just fill-in the result
            FOREACH_SET_ITEM(&visible_operators, si, &oper) {
                (void) return_types->Insert(oper->BaseType()) ;
            }
        } else {
            Error("near string %s ; %d visible types match here",_name,visible_operators.Size()) ;
        }
        return 0 ;
    }
}

// Viper 1493 (4400): This function returns true if type id is non array and the expression
// is an operator ampersand or a String literal. As both of them returns an array type
unsigned
VhdlInteger::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;
    // If type is not an integer type, then it cannot match an integer literal
    if (!type_id->IsIntegerType()) return 1 ;
    return 0 ;
}

unsigned
VhdlReal::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;
    // If type is not an real type, then it cannot match an real literal
    if (!type_id->IsRealType()) return 1 ;
    return 0 ;
}

unsigned
VhdlCharacterLiteral::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;
    // If type is not an enumeration type, then it cannot match an character literal
    if (!type_id->IsEnumerationType()) return 1 ;
    return 0 ;
}

unsigned
VhdlStringLiteral::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;
    // If type is not a single dimensional array type, then it cannot match a string literal
    if (type_id->Dimension() != 1) return 1 ;
    return 0 ;
}

unsigned
VhdlPhysicalLiteral::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;
    // If type is not an physical type, then it cannot match an physical literal
    if (!type_id->IsPhysicalType()) return 1 ;
    return 0 ;
}

VhdlIdDef *
VhdlBitStringLiteral::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    if (!IsVhdl87()) {
        // This is VHDL 93 behavior (behaves like a string).
        return VhdlStringLiteral::TypeInfer(expected_type, return_types, environment,0) ;
    }

    // Here, Vhdl 87 behavior : bitstringliteral is array of bit

    // single_id is the return type of the string, as previously infered
    if (_single_id) {
        // Fast determination of result type :
        if (return_types) (void) return_types->Insert(_single_id->BaseType()) ;
        return _single_id ; // Return string type that matches here
    }

    // Short-cut if expected type is known.
    VhdlIdDef *elem_type ;
    if (expected_type) {
        elem_type = expected_type->ElementType() ;
        // VIPER #7717 : Bit string matches with vl_logic type even in 87 mode
        if (!elem_type || (!elem_type->TypeMatch(StdType("bit")) && !Strings::compare(elem_type->Name(), "vl_ulogic") && !Strings::compare(elem_type->Name(), "vl_logic")) || expected_type->Dimension() != 1) {
            // VIPER #7717 : When expected_type is vl_logic/vl_logic_vector, produce
            // different message to not specify vl_logic/vl_logic_vector in message text.
            if (!ErrorForVllogicExpectedType(expected_type, 0)) {
                Error("type %s does not match with the bit-string literal ; should be array of BIT", expected_type->Name()) ;
            }
            return expected_type ;
        }

        // Element type is already checked as 'bit', so we are done..?

        // Set single_id to be the return type :
        _single_id = expected_type ;
        if (return_types) (void) return_types->Insert(_single_id->BaseType()) ;
        // Done
        return _single_id ;
    }

    // Must be one-dimensional array-of-BIT type
    // Gotta find all visible one-dimensional array types
    // Pick-up the , operator (as for aggregates)
    Set visible_operators(POINTER_HASH,11) ;
    SetIter si ;
    VhdlIdDef *oper ;
    VhdlIdDef *match = 0 ;
    VhdlIdDef *oper_type ;
    Set done_scopes(POINTER_HASH, 11) ; // hash table to avoid exponential explosion in FindCommaOperators.

    //_present_scope->FindAll(, visible_operators) ;
    if (_present_scope) _present_scope->FindCommaOperators(visible_operators, done_scopes) ;

    // Now rule-out the , operators (string types) that do not match
    // Prune down the list, knowing that we need a 1-dimensional array
    // of character type, or we need the 'expected_type'
    FOREACH_SET_ITEM(&visible_operators, si, &oper) {
        oper_type = oper->Type() ;

        // Check return type (if needed)
        if (!oper_type) {
            // Ruled out on return type
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        // String literals are allowed as sub-aggregates in a multi-dim aggregate,
        // so we should NOT rule-out anonymous types here.

        // Check element type (must be 'BIT')
        elem_type = oper->ArgType(0) ;
        if (!elem_type || !elem_type->TypeMatch(StdType("bit"))) {
            (void) visible_operators.Remove(oper) ;
            continue ;
        }
        match = oper ;
    }

    if (visible_operators.Size()==1) {
        // Set the resolved operator as single_id
        if (!match) return 0 ;
        _single_id = match->Type() ;

        // Check that every character is in this type
        if (_single_id && return_types) (void) return_types->Insert(_single_id->BaseType()) ;
        return _single_id ;
    } else {
        if (return_types && visible_operators.Size()) {
            // Just fill-in the result
            FOREACH_SET_ITEM(&visible_operators, si, &oper) {
                (void) return_types->Insert(oper->BaseType()) ;
            }
        } else {
            Error("near bit string literal %s ; %d visible types match here", _name, visible_operators.Size()) ;
        }
        return 0 ;
    }
}

VhdlIdDef *
VhdlCharacterLiteral::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    if (_single_id) {
        // Fast determination of result type :
        if (return_types) (void) return_types->Insert(_single_id->BaseType()) ;
        return _single_id->Type() ;
    }

    // Short-cut if expected_type is given
    VhdlIdDef *id ;
    if (expected_type) {
        id = expected_type->GetEnum(_name) ;
        if (!id) {
            // VIPER #7847 : '_name' can be alias of enum literal, check that
            // Find all characters in the scope
            Set r(POINTER_HASH,11) ;
            (void) _present_scope->FindAll(_name, r) ;
            SetIter si ;
            VhdlIdDef *fid ;
            FOREACH_SET_ITEM(&r, si, &fid) {
                if (fid && !expected_type->TypeMatch(fid->Type())) (void) r.Remove(fid) ;
            }
            if (r.Size()==1) _single_id = (VhdlIdDef*)r.GetLast() ; // Set the resolved character literal as single_id
            if (_single_id && _single_id->IsAlias()) {
                VhdlIdDef *target = _single_id->GetTargetId() ;
                id = target ? expected_type->GetEnum(target->Name()): 0 ;
            }
        }
        if (!id) {
            // VIPER #7717 : When expected_type is vl_logic/vl_logic_vector, produce
            // different message to not specify vl_logic/vl_logic_vector in message text.
            if (!ErrorForVllogicExpectedType(expected_type, _name)) {
                Error("character %s is not in type %s",_name, expected_type->Name()) ;
            }
            return 0 ;
        }
        // Here, it matches
        if (!_single_id) _single_id = id ;
        if (return_types) (void) return_types->Insert(id->BaseType()) ; // Should not be needed..
        return id->Type() ; // Should be same as expected_type
    }

    // Find all characters in the scope
    Set r(POINTER_HASH,11) ;
    SetIter si ;
    (void) _present_scope->FindAll(_name, r) ;

    if (r.Size()==1) {
        // Set the resolved character literal as single_id
        _single_id = (VhdlIdDef*)r.GetLast() ;
        if (return_types) (void) return_types->Insert(_single_id->BaseType()) ;
        return _single_id->Type() ;
    } else {
        if (return_types && r.Size()) {
            // Just fill-in the result
            FOREACH_SET_ITEM(&r, si, &id) {
                (void) return_types->Insert(id->BaseType()) ;
            }
        } else {
            Error("near character %s ; %d visible types match here", _name, r.Size()) ;
            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            FOREACH_SET_ITEM_BACK(&r, si, &id) {
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
        }
        return 0 ;
    }
}

VhdlIdDef *
VhdlPhysicalLiteral::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    // environment check done on the 'unit'.
    // Type-infer the unit with the expected type :
    VhdlIdDef *unit_type = (_unit) ? _unit->TypeInfer(expected_type,0,environment,selection_scope) : 0 ;
    if (unit_type && !unit_type->IsPhysicalType()) {
        Error("physical unit does not denote a physical type") ;
        return unit_type ;
    }

    // Check the value for integer/real type (LRM 3.1.3) .
    VhdlIdDef *value_type = (_value) ? _value->TypeInfer(0,0,environment,selection_scope) : 0 ;
    if (value_type && !value_type->IsIntegerType() && !value_type->IsRealType()) {
        Error("physical value should denote an integer or real value") ;
        return value_type ;
    }

    if (return_types) (void) return_types->Insert(unit_type) ;
    return unit_type ;
}

VhdlIdDef *
VhdlNull::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    // NULL represents value for any access type
    // I don't have an operator for it. Hope that we will never need it,
    // and that the type can always be determined by the context.
    // Have not checked LRM on this, but should be same as allocators.

    // VIPER 3119 : test if NULL is properly used. NULL literal can be :
    // (1) used as a literal for an access-type variable, or
    // (2) can be used as a waveform element (NULL waveform element: switch-off drivers: LRM 8.4.1) for any guarded signal...
    // We cannot test for (2) right here, so instead, whenever we infer a waveform element (in signal assignments) we test for NULL literal explicitly.
    if (expected_type && !expected_type->IsAccessType()) {
        Error("illegal use of NULL literal", expected_type->Name()) ;
    }

    // VIPER 3184 : if 'return_types' is given, we should actually return all visible access types here, because all of them match with NULL.
    // Similar to an integer literal, we don't do that (return all visible integer types), but instead have dedicated code
    // in the VhdlOperator and VhdlIndexedName clauses to deal with that (do implicit conversions where needed).
    return expected_type ;
}

VhdlIdDef *
VhdlIdRef::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    // Find matching id's by name in scope
    // Filter the ones that match expected type
    // Set _single_id once matched
    if (_single_id) {
        // Check environment :
        CheckIdentifierUsage(_single_id, environment) ;

        // Eliminate this id (which is directly referenced unindexed) as a dual-port ram :
        if (environment) _single_id->SetCannotBeDualPortRam() ;

// RD: 10/2011: in the process of moving code to VarUsage, to enable more flexible ram extraction algorithms.
// Not yet complete yet, pending VarUsage run on all expression nodes. So keep this code here for now.
        // Eliminate this id (which is directly referenced unindexed) as a multi-port ram :
        if (environment) _single_id->SetCannotBeMultiPortRam() ;

        // Additional check : if we are in an expression, and id is a type or record element,
        // then we would have passed previous check, but as plain identifier that is still wrong
        if (environment == VHDL_READ && (_single_id->IsRecordElement() || _single_id->IsType())) {
            // cant use a type or record element as an identifier in an expression
            Error("%s is illegal in an expression", _single_id->Name()) ;
        }

        if ((environment == VHDL_SIGUPDATE) && (VHDL_implicit_guard == _single_id->GetSignalKind())) Error("cannot drive implicit signal guard") ; // Viper #3152

        // Still check type-compliance.
        // Previous calls could have been without expected type.
        // and calls from range cannot always guarantee that we used result type
        if (expected_type && !expected_type->TypeMatch(_single_id->Type())) {
            Error("type error near %s ; expected type %s", _name, expected_type->Name()) ;
        }

        if (_single_id->IsSubprogram()) {
            // VIPER 2683 :
            // Subprogram call without arguments. Check if thats legal, right here :
            // Type-infer the association list against the formals of the subprogram.
            _single_id->TypeInferAssociationList(0, 0, 0, this) ;
        }

        // Return the type of the identifier
        if (return_types) (void) return_types->Insert(_single_id->BaseType()) ;

        // Note : if this is a subtype, return itself. Needed for subtype indication with a subtype.
        // Maybe that's always needed (subtype->Type() returns itself) CHECK ME ?
        if (_single_id->IsSubtype() || _single_id->IsType()) return _single_id ;

        // Return the type of the object.
        return _single_id->Type() ;
    }

    // First check if the identifier is visible by selection (LRM 10.3 g,h,i,j,k,l) :
    if (selection_scope) {
        VhdlIdDef *selected_id = selection_scope->FindSingleObjectLocal(_name) ;
        if (selected_id) {
            // We got it.
            _single_id = selected_id ;
            // Run this through the same routine again, to get the environment checks :
            return TypeInfer(expected_type, return_types, environment, selection_scope) ;
        }
        // If it's not by selection, then the identifier is 'directly' visible..
        // So, proceed with normal present scope search.
    }

    // Do full id type inference
    // First get all visible identifiers (by name)
    Set r(POINTER_HASH) ;
    SetIter si ;
    VhdlIdDef *id ;
    (void) _present_scope->FindAll(_name, r) ;

    if (r.Size() == 0) {
        // We now always give an error here.
        // We always bail-out of TypeInfer if no match is found.
        Error("%s is not declared", _name) ;
        return 0 ;
    }

    // Overloading :
    // Prune this list down until we have a single id left over
    VhdlIdDef *match = 0 ; // for subprogram homograph checking.
    unsigned first_time = 1 ;
    FOREACH_SET_ITEM(&r, si, &id) {
        if (r.Size()==1) break ; // stop if there is one left-over.

        // Do a type match against expected type
        if (expected_type && !expected_type->TypeMatch(id->Type())) {
            (void) r.Remove(id) ;
            continue ;
        }

        // It could be a subprogram that does not take any arguments. Test that :
        if (id->IsSubprogram()) {
            // Check if it does not take any arguments, or its first argument as initial value..
            // Otherwise, reject this as a subprogram call.
            VhdlIdDef *arg = id->Arg(0) ;
            if (arg && !arg->HasInitAssign()) {
                (void) r.Remove(id) ;
                continue ;
            }

            // Issue 2182 : There can be multiple subprograms that match without arguments.
            // So we need to do a homograph check here, similar to what we do with normal
            // subprogram call homograph checking.
            //
            // Check for homographs :
            // Do homograph check (since scope does not eliminate homographs)
            // This way, the first definition in visible_operators will make
            // as the first 'match'. Homographs of that will be eliminated here.
            // The first definition is the one that is visible : scope FindAll
            // does not eliminate the other ones, so they are made unvisible here.
            if (match && match->IsHomograph(id)) {
                if (match->IsPredefinedOperator()) {
                    // The existing match (the first mentioned operator) is a predefined one.
                    // That means that someone overloaded a predefined operator.
                    // Language defines that that is not allowed, unless both are in the
                    // same package. But that is already taken care of at declaration time.
                    // So, we should allow this (so that there are two matches).
                    //
                    // BUT : Synopsys overloads the relational operators for std_logic_vector in
                    // another package (std_logic_unsigned/signed), and expect the overloaded
                    // form to prevail over the predefined operator form. That is NOT according
                    // to language rules (both should be visible or invisible).
                    // But any way, try and comply with that here.
                    // Eliminate the match, (and keep the new operator).
                    (void) r.Remove(match) ;
                    match = id ;
                } else {
                    // GIVEN : Locally declared ids always precedes externally included ids.
                    // If previous match is not locally declared, then we have two or more
                    // homographs included via use clauses.  Give an error here appropriately. (VIPER #1311)
                    if (!id->IsPredefinedOperator() && _present_scope && !_present_scope->IsDeclaredHere(match, 1 /*look in upper/extended*/)) {
                        if (first_time) {
                            Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", id->Name()) ;
                            match->Info("%s is declared here", id->Name()) ;
                            first_time = 0 ;
                        }
                        id->Info("another match is here") ;
                    }
                    // Delete the new one (it is declared later than existing match).
                    (void) r.Remove(id) ;
                }
                continue ;
            }
            // otherwise : set this subprogram as a match :
            match = id ;
        }
    }

    // Here r is certainly not 0 (we would have bailed out before).
    // If it has a single id, then that can still be the wrong one
    // since we only did overload resolving. Still need to type-check.
    if (r.Size()==1) {
        // Set the resolved identifier as single_id
        _single_id = (VhdlIdDef*)r.GetLast() ;

        // Run this through the same routine again, to get the environment checks :
        return TypeInfer(expected_type, return_types, environment, selection_scope) ;
    } else {
        if (return_types) {
            // Just fill-in the result
            FOREACH_SET_ITEM(&r, si, &id) {
                (void) return_types->Insert(id->BaseType()) ;
            }
        } else {
            Error("near %s ; %d visible identifiers match here", _name, r.Size()) ;
            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            FOREACH_SET_ITEM_BACK(&r, si, &id) {
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
        }
        return 0 ;
    }
}

VhdlIdDef *
VhdlArrayResFunction::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    if (_res_function) (void) _res_function->TypeInfer(expected_type, return_types, environment, selection_scope) ;
    return 0 ;
}

VhdlIdDef *
VhdlRecordResFunction::TypeInfer(VhdlIdDef * /*expected_type*/, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return 0 ;
}

VhdlIdDef *
VhdlName::TypeInferRecordResFunc(VhdlIdDef * /*record_type*/)
{
    return 0 ;
}

VhdlIdDef *
VhdlRecordResFunction::TypeInferRecordResFunc(VhdlIdDef *record_type)
{
    // LRM 10.5 states that overloading needs to be done based on :
    //    (1) return type
    //    (2) number of parameters
    //    (3) type of parameter
    // We do overloading resolving first :

    // With Vhdl 2008 LRM section 6.3 the resolution func name can be placed
    // in a nesting of parenthesis. The degree of nesting of parentheses
    // indicates how deeply nested in the type structure the resolution
    // function is associated. Two levels indicate that the resolution function
    // is associated with the elements of the elements of the type.

    if (!record_type) return record_type ;
    if (!record_type->IsRecord()) return record_type ;

    unsigned i ;
    VhdlRecResFunctionElement *elem ;

    FOREACH_ARRAY_ITEM(_rec_res_function_list, i, elem) {
        VhdlName *record_id_name = elem->RecordIdName() ;
        VhdlName *record_id_function = elem->RecordResFunction() ;
        Map* element_list = record_type->GetTypeDefList() ;
        VhdlIdDef *element_id = element_list ? (class VhdlIdDef*)element_list->GetValue(record_id_name->Name()) : 0 ;
        VhdlIdDef *element_range_type = element_id ? element_id->Type() : 0 ;
        VhdlName *res_function = record_id_function ;
        while (res_function && res_function->IsArrayResolutionFunction()) {
            if (element_range_type && !element_range_type->IsArrayType()) {
                element_range_type->Error("illegal nesting of resolution indication function") ;
                VERIFIC_ASSERT(element_id) ; // Must hold since 'element_range_type' is valid here
                element_range_type = element_id->Type() ; // Error scenario revert back element_range_type
                break ;
            }
            element_range_type = element_range_type ? element_range_type->ElementType() : 0 ;
            res_function = res_function->GetResolutionFunction() ;
        }

        if (!res_function) continue ;
        Set res_functions(POINTER_HASH) ;
        (void) res_function->FindObjects(res_functions,0) ;

        SetIter si ;
        VhdlIdDef *func ;
        FOREACH_SET_ITEM(&res_functions, si, &func) {
            if (res_functions.Size()==1) break ; // overloading resolved. Unique id determined.
            if (!func->IsSubprogram() ||
                (element_range_type && !element_range_type->TypeMatch(func->Type())) ||
                (func->NumOfArgs()!=1)) {
                // Mismatch on return type/num_args.
                (void) res_functions.Remove(func) ;
                continue ;
            }
            VhdlIdDef *arg_type = func->ArgType(0) ;
            if (!arg_type || (element_range_type && !element_range_type->TypeMatch(arg_type->ElementType()))) {
                // Mismatch on argument type
                (void) res_functions.Remove(func) ;
                continue ;
            }
        }

        if (res_functions.Size() == 1) {
            // Unique
            func = (VhdlIdDef*) res_functions.GetLast() ;
        } else {
            // Ambiguity or not declared resolution function :
            // FIX ME : a bit risky to call Name() on resolution function name :
            Error("near %s ; %d visible identifiers match here", res_function->Name(), res_functions.Size()) ;
            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            VhdlIdDef *id ;
            FOREACH_SET_ITEM_BACK(&res_functions, si, &id) {
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
            return record_type ;
        }

        // LRM 2.4 Requirements for resolution functions :
        //    (1) it's a 'pure' function
        //    (2) return type matches
        //    (3) single formal argument (of class constant)
        //    (4) that is a unconstrained array type
        //    (5) with element type matching resolved type
        // We check this here. We don't check (1) yet.

        if (!func->IsSubprogram() || func->NumOfArgs()!=1) {
            Error("%s is not a (resolution) function", func->Name()) ;
            return record_type ;
        }

        VhdlIdDef *arg_type = func->ArgType(0) ;
        if (!arg_type ||
            (element_range_type && !element_range_type->TypeMatch(func->Type())) ||
            arg_type->Dimension() != 1 ||
            (element_range_type && !element_range_type->TypeMatch(arg_type->ElementType()))) {
            Error("%s is not a valid resolution function for type %s", func->Name(), record_type->Name()) ;
                return record_type ;
        }
        // VIPER #2987 : Set resolved resolution function id in res_function
        res_function->SetId(func) ;
    }
    return 0 ;
}

VhdlIdDef *
VhdlSelectedName::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    VERIFIC_ASSERT(_prefix && _suffix) ;

    // Use existing '_unique_id' if we can.
    if (_unique_id) {
        // Check environment :
        // If unique_id is a record element, then we cannot do a proper check.
        // We will need to trust that the initial type inference has environment check done already.
        // FIX ME : another reason not to store a record element in _unique_id.
        if (!_unique_id->IsRecordElement()) CheckIdentifierUsage(_unique_id, environment) ;

        // Additional check : if we are in an expression, and selected name is a type
        // then we would have passed previous check, but that it is still wrong as selected name.
        if (environment == VHDL_READ && _unique_id->IsType()) {
            // This object does not belong in a selected name..
            Error("%s is illegal in an expression", _unique_id->Name()) ;
        }

        // Still check type-compliance.
        // Previous calls could have been without expected type.
        // and calls from range cannot always guarantee that we used result type
        VhdlIdDef *unique_type = _unique_id->Type() ;
        if (unique_type && _suffix->IsAll() && unique_type->IsAccessType()) unique_type = unique_type->DesignatedType() ; // Viper #5176

        if (expected_type && !expected_type->TypeMatch(unique_type)) {
            Error("%s is not a %s", _unique_id->Name(), expected_type->Name()) ;
        }

        if (_unique_id->IsSubprogram()) {
            // VIPER 2683 :
            // Subprogram call without arguments. Check if thats legal, right here :
            // Type-infer the association list against the formals of the subprogram.
            _unique_id->TypeInferAssociationList(0, 0, 0, this) ;
        }

        // Return the type of the identifier
        if (return_types && unique_type) (void) return_types->Insert(unique_type->BaseType()) ;

        // Note : if this is a subtype, return itself. Needed for subtype indication with a subtype.
        // Maybe that's always needed (subtype->Type() returns itself) CHECK ME ?
        if (_unique_id->IsSubtype() || _unique_id->IsType()) return _unique_id ;

        // Return the type of the object.
        return _unique_id->Type() ;
    }

    // Could be extended name or record element.

    // Check if there is a selection scope. If so, find the prefix id (if we have that name immediately).
    // If that is there, it is an object in the selection scope, and that can never be an extended name.
    VhdlIdDef *prefix_id = 0 ;
    if (selection_scope && _prefix->Name()) {
        // objects by selection (LRM 10.3) are never overloadable, and always local.
        prefix_id = selection_scope->FindSingleObjectLocal(_prefix->Name()) ;
        // no need to insert in the overloading Set 'r'. It's a indexed/sliced name.
    }

    // Then, try if it is an extended name. If so, call FindObjects()
    // If not, it has to be a record (field) or access value.
    // Then, we type-infer the prefix.
    // VIPER 2781 : do NOT call IsExpandedName() if we have a selection scope. Have to assume we are dealing with a 'formal', which is never an expanded name.
    // FIX ME : That does not work for type-infer of actual (of a binding indication). There actual could be an expanded name into normal scope, even though there is a selected name.
    // Temporarily fix this by checking !environment as well as 'selected_scope'. That would indicate that we really came from TypeInferFormal(), and are thus dealing with a formal (which cannot have an expanded name).
    if (!prefix_id && (!(selection_scope && !environment)) && IsExpandedName()) {
        // Pick-up all id's for this selected name.
        Set r(POINTER_HASH) ;

        // Don't do this if we search a name by 'selection' :
        // It should not make a difference, but FindObjects errors out if there is an object from selection scope in this name.
        // FIX ME This should really be transparent. Still not happy with FindObjects. May need redesign.
        (void) FindObjects(r,0) ;

        // Prune down with 'expected_type'
        SetIter si ;
        VhdlIdDef *id ;
        FOREACH_SET_ITEM(&r, si, &id) {
            if (r.Size()==1) break ; // we are unique
            if (expected_type && !expected_type->TypeMatch(id->Type())) {
                // CHECK ME : need to worry about implicit type conversion ??
                // Don't think so. Prefix of selected name cannot be a literal.
                (void) r.Remove(id) ;
                continue ;
            }

            if (id->IsSubprogram()) { // Viper #7181
                // Check if it does not take any arguments, or its first argument as initial value..
                // Otherwise, reject this as a subprogram call.
                VhdlIdDef *arg = id->Arg(0) ;
                if (arg && !arg->HasInitAssign()) {
                    (void) r.Remove(id) ;
                    continue ;
                }

                // How about multiple match with one predefined operator
            }
        }

        if (r.Size()==1) {
            _unique_id = (VhdlIdDef*)r.GetLast() ;

            // Now that unique id is set, run this through the routine again,
            // to get environment and return type checks done.
            return TypeInfer(expected_type, return_types, environment, selection_scope) ;
        }

        // Here, it's ambiguous ; error out if this was self-determined
        if (r.Size() > 1) {
            if (return_types) {
                // Just fill-in the result
                FOREACH_SET_ITEM(&r, si, &id) {
                    (void) return_types->Insert(id->BaseType()) ;
                }
            } else {
                Error("near %s ; %d visible identifiers match here", _suffix->Name(), r.Size()) ;
                // VIPER #2025 - List locations of visible identifiers
                unsigned first = 0 ;
                FOREACH_SET_ITEM_BACK(&r, si, &id) {
                    id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
                }
            }
            return 0 ;
        }
        // VIPER #7893: If prefix is external reference to protected type variable,
        // suffix can be resolved using the type information of external name.
        if (_prefix->IsExternalNameRef()) {
            VhdlIdDef *prefix_type = _prefix->TypeInfer(0,0,environment,selection_scope) ;
            if (prefix_type && prefix_type->IsProtectedType()) {
                if (_suffix->IsAll()) {
                    Error("suffix of protected type name cannot be 'all'") ;
                    return 0 ;
                }
                VhdlIdDef *elem = prefix_type->GetElement(_suffix->Name()) ;
                if (!elem) {
                    Error("element %s is not in protected type %s", _suffix->Name(), prefix_type->Name()) ;
                    return 0 ;
                }

                // Set protected type element as the _unique_id. For possible re-entry.
                // CHECK ME : We still need this, for ::Evaluate() and such ?
                _unique_id = elem ;

                // Viper #5575: for VHDL_range (subtype indication) elem must be a type itself
                if ((environment == VHDL_range) || (environment == VHDL_interfacerange) || (environment == VHDL_recordrange) || (environment == VHDL_subtyperange)) CheckIdentifierUsage(elem, environment) ;

                // Insert 'elem's type in the table
                if (return_types) (void) return_types->Insert(elem->BaseType()) ;
                return elem->Type() ;
            }
        }
        return 0 ; // FindObjects will have complained if expanded name prefix was not declared or so.
    }

    // Here, it is not an expanded name.
    // Will be a record name (or access value), with non-object prefix.
    // Type-infer the prefix. Environment check are done there too.

    // Issue 2053. Overloading in prefix is allowed.
    // So we should do a full type-inference, catching return types from the prefix :
    Set prefix_types(POINTER_HASH) ;
    (void) _prefix->TypeInfer(0,&prefix_types,environment,selection_scope) ;

    // Rule out types which don't match expected type and other criteria :
    SetIter si ;
    VhdlIdDef *prefix_type = 0 ;
    VhdlIdDef *elem ;
    FOREACH_SET_ITEM(&prefix_types, si, &prefix_type) {
        if (prefix_types.Size()==1) break ; // we are unique

        // Check if this is a record type, or access type :
        if (prefix_type->IsRecordType()) {
            // Find the suffix as a field in the type :
            elem = prefix_type->GetElement(_suffix->Name()) ;
            if (!elem || _suffix->IsAll()) { // .all suffix not allowed on a record
                (void) prefix_types.Remove(prefix_type) ;
                continue ;
            }
            // And check if the element matches the expected return type :
            if (expected_type && !expected_type->TypeMatch(elem->Type())) {
                (void) prefix_types.Remove(prefix_type) ;
                continue ;
            }
            // Issue 3536 : This field matches. Insert result (base) type in 'results' set, if there :
            if (return_types) {
                (void) return_types->Insert(elem->BaseType()) ;
            }
        } else if (prefix_type->IsAccessType()) {
            // For an access type, check the type it 'points' to :
            prefix_type = prefix_type->DesignatedType() ;
            if (expected_type && prefix_type && !expected_type->TypeMatch(prefix_type->BaseType())) {
                (void) prefix_types.Remove(prefix_type) ;
                continue ;
            }
            // Issue 3536 : This field matches. Insert result (base) type in 'results' set, if there :
            if (return_types && prefix_type) {
                (void) return_types->Insert(prefix_type->BaseType()) ;
            }
        } else {
            // neither an access, nor a record type. This type is not valid in a selected name :
            (void) prefix_types.Remove(prefix_type) ;
        }
    }

    // Now, overloading is resolved. Last one might not be tested as locally valid (we only removed ambiguity)...
    prefix_type = 0 ;
    if (prefix_types.Size()==0) {
        // No match at all came through. Call prefix hard with prefix_type==0, and error will appear.
    } else if (prefix_types.Size()==1) {
        // Good. Single match is there.
        prefix_type = (VhdlIdDef*)prefix_types.GetLast() ;
    } else {
        // Multiple matches for the prefix survived. They are all tested locally, and correct.
        // So this is an ambiguous selected name. Only valid if there are 'return_types' expected,
        // where the ambiguity will be resolved in the caller.
        if (return_types) {
            // Pass these types through to
            // No need to fill 'return_types'. That is already done in the loop above.
            //FOREACH_SET_ITEM(&prefix_types, si, &prefix_type) {
            //    (void) return_types->Insert(prefix_type->BaseType()) ;
            //}
            return 0 ; // done. ambiguous name. All possible return types inserted in 'return_types'
        }
    }

    // Here, if 'prefix_type' is set, it is the matching unique prefix type.
    // If not, then there is an ambiguity problem, or prefix had no match at all.
    // Call prefix hard with 'prefix_type' to fix the prefix, or unvail the problem :
    (void) _prefix->TypeInfer(prefix_type,0,environment,selection_scope) ;

    // Now, without a valid prefix type, an error will have occurred in the previous call,
    // and we can safely leave.
    if (!prefix_type) return 0 ; // Something went wrong in prefix.

    // Now we have a unique prefix type, lets validate it here for the selected name :
    if (prefix_type->IsAccessType()) {
        // Go to the type that the access type denotes ? CHECK ME !
        if (!prefix_id) prefix_id = _prefix->GetId() ;
        // if (prefix_id && prefix_id->IsOutput()) Error("access type cannot be a formal of mode out") ; //Viper 4514
        if (prefix_id && prefix_id->IsOutput() && (environment != VHDL_SIGUPDATE) && (environment != VHDL_VARUPDATE)) Error("access type cannot be a formal of mode out") ; //Viper 4514, Viper #7039 - to relax it for LHS use
        VhdlIdDef *subtype = prefix_type->DesignatedType() ;
        if (!subtype) return 0 ; // something wrong
        if (_suffix->IsAll()) {
            if (_prefix->FindFullFormal()) _unique_id = _prefix->FindFullFormal() ; // Viper #5176
            // This is the normal way to access the entire value in an access type object.
            // The type expected is the type accessed by the access type.
            // Return the access sub-type :
            if (expected_type && !expected_type->TypeMatch(subtype->BaseType())) {
                Error("access type %s does not access a %s", prefix_type->Name(), expected_type->Name()) ;
                return 0 ;
            }

            // Insert subtype in the table
            if (return_types) (void) return_types->Insert(subtype->BaseType()) ;
            return subtype ;
        }
        // Here, do the rest of checks with the access subtype
        prefix_type = subtype ;
    }

    if (prefix_type->IsRecordType()) {
        // Find the named element in this record
        // For this, first get its (record) type
        if (_suffix->IsAll()) {
            Error("suffix of record name cannot be 'all'") ;
            return 0 ;
        }
        elem = prefix_type->GetElement(_suffix->Name()) ;
        if (!elem) {
            Error("element %s is not in record type %s", _suffix->Name(), prefix_type->Name()) ;
            return 0 ;
        }

        // Infer the suffix (as a choice), with the expected type.
        // so that the suffix name '_single_id' is checked/set to the record element identifier.
        (void) _suffix->TypeInfer(expected_type, 0, VHDL_record, prefix_type->LocalScope()) ;

        // Set record element as the _unique_id. For possible re-entry.
        // CHECK ME : We still need this, for ::Evaluate() and such ?
        _unique_id = elem ;

        // Viper #5575: for VHDL_range (subtype indication) elem must be a type itself
        if ((environment == VHDL_range) || (environment == VHDL_interfacerange) || (environment == VHDL_recordrange) || (environment == VHDL_subtyperange)) CheckIdentifierUsage(elem, environment) ;

        // Insert 'elem's type in the table
        if (return_types) (void) return_types->Insert(elem->BaseType()) ;
        return elem->Type() ;
    }

    Error("record name prefix type %s is not a record type", prefix_type->Name()) ;
    return 0 ;
}

VhdlIdDef *
VhdlIndexedName::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    if (!_assoc_list) return 0 ; // Could happen at syntax error
    //
    // Indexed name in VHDL. This is the most complicated routine in the analyzer.
    // VHDL Indexed name could be
    //   - indexed name (return element type of prefix)
    //   - sliced name (return type of prefix)
    //   - function call (return type of prefix)
    //   - type-conversion (return prefix)
    //   - index constraint    array_type(4 to 10)
    //   - group_template_name (group_constituent_List)  (in group decl)
    //
    // This routine will figure that out and set the appropriate bit-flag to indicate what
    // sort of indexed name it is. If the prefix name is an identifier,
    // then the overloading will determine which identifier it is, and we will
    // set that in the _prefix_id field when done.
    //
    // Error will occur if we find a type mismatch or no id match and we were called 'hard'
    // (no 'return_types' array was passed in to us).
    //
    // So here we go :
    //
    // (1) If it is a type conversion
    //      Type ( Value ) ;
    // type conversion has (single) expression argument,
    // type should be determined from context
    //
    // (2) If it is a index subtype indication
    //      Type ( Range , Range ) ;
    // index subtype indication has 1 or more range indexes,
    // which are of index type of the prefix type
    //
    // (3) If it is a subprogram call (function or procedure)
    //      Function ( arguments )
    // then the function can be overloaded.
    // We have to prune down the matches if more than one identifier matches :
    // Overloading of subprograms is very extended in VHDL (LRM 2.3)
    //   - Depends on return type match
    //   - Depends on all argument type match (either named or positional)
    //   - Depends on number of associations (which means that we cannot reject subprograms
    //     if they have less actuals than formals. Have to consider initial values too
    //     (formals that don't need an actual)
    //     This is contrary to LRM 2.3 statement that initial value presence/absence
    //     has no influence on overloading.
    //
    //
    // (4) Potentially, a subprogram result value can be indexed :
    //      Function    (indexing)
    // this could happen for a function without arguments (or all arguments have initial value)
    // and the function returns an array type of the dimension that matches the num of indexes.
    // This is chosen if ALL normal uses of the subprogram fail (in overloading).
    // Even if the subprogram is the only match from the start.
    //
    // (5) If it is a indexed name,
    //      Array ( Value , Value )
    // the argument types should match the array prefix type index types.
    // The prefix does not need to be an identifier !! It could be a indexed name or so.
    //
    // (6) If it is a sliced name,
    //      Array ( Range )
    // the argument type should match the array prefix type index types.
    // There can be only one argument.
    // The prefix does not need to be an identifier !! It could be a indexed name or so.
    //
    // Fun huh ? Don't you just love VHDL syntax/semantics ?
    // But, here we go :
    //

    // If we have been here before, use the _prefix_id to determine the result :

    // Subtype indication is like 'type_name (3 downto 0)(4 to 8)'.
    // This type of declaration is an error for pre Vhdl2008
    // Viper #4169, #4170
    if (_prefix && _prefix->GetAssocList() && !IsVhdl2008() &&
        ((environment == VHDL_range) || (environment == VHDL_interfacerange) || (environment == VHDL_ACCESS_SUBTYPE) || (environment == VHDL_recordrange) || (environment == VHDL_subtyperange))) {
        Error("illegal syntax for subtype indication") ;
    }

    if (_prefix_id) {
        // First, do the environment checks :
        CheckIdentifierUsage(_prefix_id, environment) ;

        // Additional checks :
        // In 'range', this can only have an index constraint. We can have open constraint post vhdl 2008.
        // open constraint can be viewed as index constraint. Only thing is that we keep the original constraint
        // Viper 7461: Check for _is_record_constraint flag too
        if (((environment == VHDL_range) || (environment == VHDL_interfacerange) || (environment == VHDL_ACCESS_SUBTYPE) || (environment == VHDL_recordrange) || (environment == VHDL_subtyperange)) && _prefix_id->IsType() && !_is_index_constraint && !_is_record_constraint) {
            Error("%s is not a range", _prefix_id->Name()) ;
        }

        // In expression, this can only be a type-conversion
        if (environment == VHDL_READ && _prefix_id->IsType() && !_is_type_conversion) {
            Error("%s is illegal in an expression", _prefix_id->Name()) ;
        }

        // Need environment check to make sure LHS is a valid target (VIPER #1876)
        if (_is_function_call && ((environment == VHDL_VARUPDATE) || (environment == VHDL_SIGUPDATE))) {
            Error("illegal target for assignment") ;
        }

        // Now determine the result type :
        VhdlIdDef *result = 0 ;
        VhdlIdDef *prefix_type = _prefix_id->Type() ;
        if (!prefix_type) return 0 ; // something went wrong earlier

        // result type depends on the style of indexed name we are dealing with :
        if (_is_array_index) {
            result = prefix_type->ElementType() ;
        }
        if (_is_array_slice) {
            // Slice of an access type is its designated type
            if (prefix_type->IsAccessType()) {
                result = prefix_type->DesignatedType() ;
            } else {
                result = prefix_type ;
            }
        }
        if (_is_function_call) result = prefix_type ;
        // Open constraint is a type of index constraint where the constraint remains untouched.
        if (_is_index_constraint) result = _prefix_id ;
        if (_is_record_constraint) result = _prefix_id ;
        if (_is_type_conversion) result = _prefix_id ;

        // Still check type-compliance.
        // Previous calls could have been without expected type.
        // and calls from range cannot always guarantee that we used result type
        // Test against expected type
        if (expected_type && !expected_type->TypeMatch(result)) {
            Error("type error near %s ; expected type %s", _prefix_id->Name(), expected_type->Name());
            return 0 ;
        }

        if (return_types && result) (void) return_types->Insert(result->BaseType()) ;
        return result ;
    }

    unsigned i ;
    unsigned is_slice = 0 ;
    VhdlIdDef *id = 0 ;
    VhdlDiscreteRange *assoc_elem = 0 ;

    // Get all the objects that match the prefix identifier :
    Set r(POINTER_HASH) ;

    // If prefix is a simple name, we need to look in 'selected_scope' too. For partial formals.
    VERIFIC_ASSERT(_prefix) ;
    if (selection_scope && _prefix->Name()) {
        // objects by selection (LRM 10.3) are never overloadable, and always local.
        id = selection_scope->FindSingleObjectLocal(_prefix->Name()) ;
        // no need to insert in the overloading Set 'r'. It's a indexed/sliced name.
    }
    if (!id) {
        // Its not in the selected scope. Check if the _prefix is an expanded name,
        // or a plain identifier. Only then will we search all possible definitions for it.
        // This is to make partial formals work. FIX ME : It's still messy.

        // VIPER 2781 : do NOT call IsExpandedName() if we have a selection scope. Have to
        // assume we are dealing with a 'formal', which is never an expanded name.
        if (_prefix->Name() || (!selection_scope && _prefix->IsExpandedName())) {
            (void) _prefix->FindObjects(r,0) ;
        }
        // VIPER #7893 : If prefix contains external name like
        // << variable .top.sh : protected_type >>.proc_name
        // we can resolve the subprogram name using the type information of
        // external name. Do that here
        if (!r.Size() && !_prefix->IsExternalName() && _prefix->IsExternalNameRef()) {
            (void) _prefix->TypeInfer(0, 0, 0, 0) ;
            if (_prefix->GetId()) (void) r.Insert(_prefix->GetId()) ;
        }
    }

    // r will be empty if the prefix is not an identifier (but an indexed name or so).
    // In that case, we will handle it all the way below : assume the IndexedName is
    // a indexed or sliced name.

    // LRM 2.3 subprogram overloading
    // Prune down the list as long as we have more than one match.
    // Note : This pruning ONLY happens if we don't already have a single id match.
    // Cant use the same rules if there IS a unique match already.
    // This is a perculiar, but very important rule in LRM 2.3 !

    // First, prune on expected type and number of arguments for overloading.
    // Stop when there is one identifier left over. That last one is then run
    // through the hard, solid tests below.
    // Prune the list from last to first. So if none match in the end,
    // the first one remains as the match, which is normally the simples one.
    // So errors in the hard tests will make more sense.
    VhdlIdDef *arg ;
    SetIter si ;
    FOREACH_SET_ITEM_BACK(&r, si, &id) {
        if (r.Size()==1) {
            // its unique already : DON'T continue : overloading rules don't apply now.
            break ;
        }

        // The id can be something else than a subprogram (may be a enumerated value)
        // but then it does not have any parameters. So, does not match.
        if (!id->IsSubprogram()) {
            (void) r.Remove(id) ;
            continue ;
        }

        // The subprogram return type should match the result type. (If any).
        // Viper #5108: The indexed name could potentially be indexing on the
        // subprogram return type. Therefore, when the types do not match, do
        // not reject the id right away. When the subprogram takes no argument
        // AND when return type of such subprogram is array, we should not prune
        // this identifier.
        VhdlIdDef *subprog_type = id->Type() ;
        if (expected_type && !expected_type->TypeMatch(subprog_type) && (id->NumOfArgs() || !(subprog_type && subprog_type->IsArrayType()))) {
            (void) r.Remove(id) ;
            continue ;
        }

        // Count the number of actuals against the number of formals.
        if (_assoc_list->Size() != id->NumOfArgs()) {
            // not exactly the correct number of actuals.
            if (_assoc_list->Size() < id->NumOfArgs()) {
                // Not enough actuals.
                // LRM 4.3.2.2.
                // No association needed for inputs with initial value assignment.
                // LRM 2.3 : overloading should not depend on absence or presence of initial values.
                // but it should check on NUMBER of arguments.
                // That means that we only have to count the NUMBER of formals without a initial that need an actual.
                // Count that number here.
                unsigned min_num_of_args = 0 ;
                for (i=0; i<id->NumOfArgs(); i++) {
                    arg = id->Arg(i) ;
                    if (!arg) continue ;
                    if (arg->HasInitAssign()) {
                        // If this argument has an initial value but is listed in the
                        // association list, then it needs to counted. (VIPER #2029)
                        VhdlDiscreteRange *assoc ;
                        VhdlName *formal ;
                        unsigned dont_continue = 0 ;
                        unsigned j ;
                        FOREACH_ARRAY_ITEM(_assoc_list, j, assoc){
                            formal = assoc->FormalPart() ;
                            if (formal && Strings::compare(arg->Name(), formal->Name())) {
                                dont_continue = 1 ;
                            }
                        }
                        if (!dont_continue) continue ;
                    }
                    min_num_of_args++ ;
                }
                if (_assoc_list->Size() < min_num_of_args) {
                    // There are really not enough actuals. Discard this one.
                    (void) r.Remove(id) ;
                    continue ;
                }
            } else {
                // There are too many actuals.
                // This could only happen if the formals are 'partial'.
                // We do NOT support overloading with partial formals (only supported
                // if there is only ONE match from the start. So discard this solution.
                // VIPER : 2984, 3041 : Now allow partial formals.. Do NOT prune "r" here
                // (void) r.Remove(id) ; // RD: temporarily disable for 3536 debugging
                continue ;
            }
        }
    }

    // Now prune on type of the arguments.
    // Create a hash-table set of argument types, if we need it.
    Set arg_types(POINTER_HASH, (_assoc_list) ? _assoc_list->Size() : 1) ;

    VhdlIdDef *arg_type, *actual_arg_type ;

    // Keep track of any match
    VhdlIdDef *match = 0 ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc_elem) {
        if (r.Size()==0) break ; // Nothing to prune any more.

        // Only prune down if we don't yet have a unique match
        // Do this to avoid that we eliminate array (indexed or  sliced)
        // or type (conversion or subtype indication) prefix identifiers,
        // and to create better messages below (if we pruned down to 1 identifier,
        // we can give more obvious messages about why it does not match.
        // Also, we do this for efficiency reasons : there is no need to evaluate
        // requesting for possible argument types if there is only one subprogram
        // match any way.
        //
        if (r.Size()==1) {
            id = (VhdlIdDef*)r.GetLast() ;
            if (id->IsSubprogram() && id->Dimension()==_assoc_list->Size() &&
                (id->NumOfArgs()==0 || id->Arg(0)->HasInitAssign())) {
                // Last one is a function that can be used as a array, indexed.
                // Let this go through the pruning process, to see if it can
                // be used as a regular subprogram call.
                // If not, 'match' will not be set, and we will interpret the
                // call as a indexed array in the code below.

                // Double-check return type too (this might have been skipped if there was only one match from the start).
                VhdlIdDef *id_type = (id->IsGenericType()) ? id->ActualType(): id->Type() ;
                if (expected_type && !expected_type->TypeMatch(id_type)) {
                    (void) r.Remove(id) ;
                    continue ;
                }
            } else {
                // We have a unique match. Stop further pruning.
                // If match is not found yet, set this one as match.
                // We will check below this loop if it really matches.
                if (!match) match = id ;
                break ;
            }
        }

        // first of all, if association is 'open', we cannot say anything.
        if (!assoc_elem || assoc_elem->IsOpen()) continue ;

        // Start by type-infer this argument, asking for possible arg_types
        // Cannot pass-in environment, until we know what the formal is.
        // Cannot know what the formal is until we type-infer.
        // So, environment check should be done later.

        // Reset the argument types (from the previous argument).
        arg_types.Reset() ;

        // Type-infer argument, catching expected types.
        actual_arg_type = assoc_elem->TypeInfer(0,&arg_types,0/*don't know environment until we know the formal (mode)*/,selection_scope/*pass this in for formal/actualtype conversion functions*/) ;

        // If nothing matches at all, there is something badly wrong with this argument.
        if (arg_types.Size()==0) {
            // If we were called hard, call the argument hard too, so we will issue an error
            // if (!return_types) (void) assoc_elem->TypeInfer(0,0,0,selection_scope) ;
            return 0 ;
        }

        // Scan the objects
        unsigned first_time = 1 ;
        FOREACH_SET_ITEM(&r, si, &id) {
            // Don't break on Size==1, since we need to prune the indexed-subprog (see above) too.

            // Now get to the real thing :
            // Check the subprogram argument types.
            // Could be Named or Positional association.
            arg_type = 0 ;
            if (assoc_elem->IsAssoc()) {
                // Named formal
                // Works for full named formals.
                // FIX ME : overloading with partial formals does not work.
                // NOTE : pre-defined functions do not have a scope, so will fail if called with named formals.
                // Cant use formal type-inference, since it would set id's in the formal to this 'id'.
                // arg_type = assoc_elem->TypeInferFormal(id->LocalScope(),(r.Size()==1)?0:1 // silent) ;
                VhdlDiscreteRange *formal_part = assoc_elem->FormalPart() ;
                VERIFIC_ASSERT(formal_part) ;

                // Is this a partial formal ??
                VhdlDiscreteRange *formal_prefix = formal_part->GetPrefix() ;
                if (id->LocalScope() && formal_prefix) {
                    // Vipers # 2984, 3041
                    // Yes.. it is partial formal. For selected names, look
                    // for the suffixes into the record scope. For indexed
                    // names, check that the number of indices are consistent
                    VhdlDiscreteRange *formal_part_orig = formal_part ;
                    unsigned depth = 1 ;
                    while (formal_prefix) {
                        formal_prefix = formal_prefix->GetPrefix() ;
                        depth += 1 ;
                    }

                    VhdlIdDef *scope_id = id ;
                    while (1) {
                        formal_part = formal_part_orig ;
                        unsigned idx = 0 ;
                        VhdlDiscreteRange *suffix = 0 ;
                        unsigned num_index = 0 ;
                        for (idx = 0 ; idx < depth ; ++idx) {
                            Array *arg_list = formal_part->GetAssocList() ;
                            suffix = formal_part->GetSuffix() ;
                            num_index = arg_list ? arg_list->Size() : 0 ;

                            if (!formal_part->GetPrefix()) suffix = formal_part ;
                            formal_part = formal_part->GetPrefix() ;
                        }

                        if (suffix) {
                            arg = (scope_id->LocalScope()) ? scope_id->LocalScope()->FindSingleObjectLocal(suffix->Name()) : 0 ;
                            scope_id = arg ? arg->Type() : 0 ;
                        } else if (num_index) {
                            // if (scope_id->Dimension() < num_index) scope_id = 0 ;
                            while (num_index && scope_id) {
                                scope_id = scope_id->ElementType() ;
                                --num_index ;
                            }
                        }

                        depth -= 1 ;
                        if (!depth || !scope_id) break ;
                    }

                    arg_type = scope_id ;
                } else {
                    arg = 0 ;
                    const char *formal_name = formal_part->Name() ;
                    if (formal_name && id->LocalScope()) {
                        arg = id->LocalScope()->FindSingleObjectLocal(formal_name) ;
                    } else if (formal_name && id->IsPredefinedOperator()) {
                        // Viper #3698 : Named association on implicit operators
                        // TODO : add proper scope to the pre-defined operators to
                        // store the arguments for constant time lookup
                        VhdlIdDef *oper_arg = 0 ;
                        unsigned arg_idx = 0 ;
                        unsigned max_args = id->NumOfArgs() ;
                        for (arg_idx = 0; arg_idx < max_args; ++arg_idx) {
                            oper_arg = id->Arg(arg_idx) ;
                            if (!oper_arg) continue ;

                            const char *arg_name = oper_arg->Name() ;
                            if (arg_name && Strings::compare_nocase(arg_name, formal_name)) {
                                arg = oper_arg ;
                                break ;
                            }
                        }
                    }

                    // Issue 2131 : local scope search could return a non-port ! Check that here.
                    if (arg && !arg->Mode()) arg = 0 ; // reject this local id if it is not a port.
                    // Get its type :
                    arg_type = (arg) ? arg->Type() : 0 ;
                }

                // arg_type = formal_part->TypeInfer(0, 0, 0, id->LocalScope()) ; // errors out.
            } else {
                arg_type = id->ArgType(i) ;
            }
            // If argument type is specified by generic type, get its actual type
            if (arg_type && arg_type->IsGenericType() && arg_type->ActualType()) arg_type = arg_type->ActualType() ;

            if (!arg_type) {
                // id does have a problem with this argument
                (void) r.Remove(id) ;
                continue ;
            }

            // The argument's type should be in the arg_types array for this subprogram to match
            if (!arg_types.GetItem(arg_type->BaseType())) {
                if ((actual_arg_type && actual_arg_type->IsUniversalInteger() && arg_type->IsIntegerType()) ||
                    (actual_arg_type && actual_arg_type->IsUniversalReal() && arg_type->IsRealType())) {
                    // Implicit conversion to a universal type. Accept this.
                } else if (arg_type->IsAccessType() && assoc_elem && assoc_elem->IsNull()) {
                    // VIPER 3184 : Implicit conversion to a universal (access) type.
                } else {
                    // There is really no match
                    (void) r.Remove(id) ;
                    if (!r.Size()) { //VIPER #7808: There is really no match: issue error.
                        Error("type error near %s ; expected type %s", assoc_elem ? assoc_elem->Name() : 0 , arg_type->Name()) ;
                    }
                    continue ;
                }
            }

            // id (still) survived
            if (i == _assoc_list->Size()-1) {
                // Last actual argument.

                // No longer needed to check initial values here.
                // Was already checked in first-scan loop above (without the need to type infer actuals).

                // Check for homographs :
                // Do homograph check (since scope does not eliminate homographs)
                // This way, the first definition in visible_operators will make
                // as the first 'match'. Homographs of that will be eliminated here.
                // The first definition is the one that is visible : scope FindAll
                // does not eliminate the other ones, so they are made unvisible here.
                // Viper #7142: They need not be homograph, one could have default argument
                // and one of them could be pre-defined, which should be skipped
                unsigned one_predefined = match && (match->IsPredefinedOperator() || id->IsPredefinedOperator()) ;
                if (match && (match->IsHomograph(id) || (!return_types && one_predefined))) {
                    if (match->IsPredefinedOperator()) {
                        // The existing match (the first mentioned operator) is a predefined one.
                        // That means that someone overloaded a predefined operator.
                        // Language defines that that is not allowed, unless both are in the
                        // same package. But that is already taken care of at declaration time.
                        // So, we should allow this (so that there are two matches).
                        //
                        // BUT : Synopsys overloads the relational operators for std_logic_vector in
                        // another package (std_logic_unsigned/signed), and expect the overloaded
                        // form to prevail over the predefined operator form. That is NOT according
                        // to language rules (both should be visible or invisible).
                        // But any way, try and comply with that here.
                        // Eliminate the match, (and keep the new operator).
                        (void) r.Remove(match) ;
                        match = id ;
                    } else {
                        // GIVEN : Locally declared ids always precedes externally included ids.
                        // If previous match is not locally declared, then we have two or more
                        // homographs included via use clauses.  Give an error here appropriately. (VIPER #1311)
                        if (!id->IsPredefinedOperator() && _present_scope && !_present_scope->IsDeclaredHere(match, 1 /*look in upper/extended*/)) {
                            if (first_time) {
                                Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", id->Name()) ;
                                match->Info("%s is declared here", id->Name()) ;
                                first_time = 0 ;
                            }
                            id->Info("another match is here") ;
                        }
                        // Delete the new one (it is declared later than existing match).
                        (void) r.Remove(id) ;
                    }
                    continue ;
                }

                // Set this surviving id as the match
                match = id ;
            }
        }
    }

    // Should have at most 1 match. Test that now.
    if (r.Size()>1 && !return_types) {
        // Viper #5703: If the owner_id is a subprogram and this is a
        // recursive call to this subprogram, discard other choices
        VhdlIdDef *subprog_id = _present_scope ? _present_scope->GetSubprogram() : 0 ;
        if (subprog_id) {
            FOREACH_SET_ITEM(&r, si, &id) {
                // Get the id from the body as well
                VhdlSpecification *spec = id->Spec() ;
                VhdlIdDef *spec_id = spec ? spec->GetId() : 0 ;
                if (id == subprog_id || spec_id == subprog_id) {
                    r.Reset() ;
                    (void) r.Insert(subprog_id) ;
                    break ;
                }
            }
        }
    }

    if (r.Size()>1) {
        // Its ambiguous
        if (return_types) {
            // Just fill-in the result
            FOREACH_SET_ITEM(&r, si, &id) {
                (void) return_types->Insert(id->BaseType()) ;
            }
        } else {
            // Give name of the prefix
            Error("near %s ; %d visible identifiers match here", ((id)?id->Name():"indexed name"), r.Size()) ;
            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            FOREACH_SET_ITEM_BACK(&r, si, &id) {
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
        }
        return 0 ;
    }

    // So, here we have reduced the match to a single prefix identifier ('match') or none at all.
    // We still really don't know what the IndexedName really is (from the
    // 5 possibilities. We will find that out now :
    //    (1) type conversion : 'match' is a TypeId, and the (single) argument is not a range.
    //    (2) index subtype indication : 'match' is a TypeId, and the arguments are all 'range'
    //    (3) subprogram call : 'match' is a SubprogramId
    //    (4) indexed name : with/without match : prefix is an Array, and all arguments are not ranges
    //    (5) sliced name : with/without match : prefix is an Array, and the (single) argument is a range.
    //
    // If a function or procedure call does not match above, it will be interpreted as
    // an indexed array call (this is guaranteed by the pruning process above).
    // This might create confusing messages if the pruning eliminated all subprogram matches. FIX ME.

    match = (r.Size()==1) ? (VhdlIdDef*)r.GetLast() : 0 ;

    // Type conversion can never occur under range environment.
    if (match && match->IsType() &&
        !(environment == VHDL_range || environment == VHDL_interfacerange || environment == VHDL_ACCESS_SUBTYPE || environment == VHDL_recordrange || environment == VHDL_subtyperange)) {
        // if match is type and evironment is not range then
        // it is the case of type conversion.

        _prefix_id = match ;
        _prefix_type = match ;

        // Test against expected type
        if (expected_type && !expected_type->TypeMatch(_prefix_id->Type())) {
            Error("near %s ; type conversion does not match type %s", _prefix_id->Name(), expected_type->Name());
            return 0 ;
        }

        assoc_elem = (VhdlDiscreteRange*)(_assoc_list ? _assoc_list->GetLast() : 0) ;

        if (_assoc_list && _assoc_list->Size()>1) {
            if (assoc_elem) assoc_elem->Error("near %s ; type conversion expects one single argument", _prefix_id->Name()) ;
            return 0 ;
        }
        actual_arg_type = assoc_elem ? assoc_elem->TypeInfer(0,0, environment, selection_scope) : 0 ;
        // Type-conversion checks on this element type
        if (!actual_arg_type) {
            // Issue 1807 : clarify the message.
            // Infer again, this time returning expected types :
            Set visible_operators(POINTER_HASH) ;
            VERIFIC_ASSERT(assoc_elem) ;
            (void) assoc_elem->TypeInfer(0, &visible_operators, environment/*subtype ind. called as range, type-inference called as expression*/,selection_scope) ;

            if (visible_operators.Size() >= 2) {
                VhdlIdDef *t0 = (VhdlIdDef*)visible_operators.GetAt(0) ;
                VhdlIdDef *t1 = (VhdlIdDef*)visible_operators.GetAt(1) ;
                if (t0 && t1) Error("ambiguous type: '%s' or '%s'", t0->Name(), t1->Name()) ;
            }

            Error("near %s ; type conversion expression type cannot be determined uniquely", _prefix_id->Name()) ;
        }

        // VIPER #3648 : Issue error if operand of type conversion is string literal
        VhdlIdDef *prefix_type = _prefix_id->Type() ;
        if (prefix_type && assoc_elem && assoc_elem->IsStringLiteral()) {
            Error("type conversion (to %s) cannot have %s operand", prefix_type->Name(), "string literal") ;
        }
        if (prefix_type && assoc_elem && assoc_elem->IsNull()) {
            Error("type conversion (to %s) cannot have %s operand", prefix_type->Name(), "literal null") ;
        }
        if (prefix_type && assoc_elem && assoc_elem->IsAllocator()) {
            Error("type conversion (to %s) cannot have %s operand", prefix_type->Name(), "allocator") ;
        }
        if (prefix_type && assoc_elem && assoc_elem->IsAggregate()) {
            Error("type conversion (to %s) cannot have %s operand", prefix_type->Name(), "aggregate") ;
        }

        // Another test : (named associations not allowed in type conv/subtype indications :
        if (assoc_elem && assoc_elem->IsAssoc()) assoc_elem->Error("illegal named association in type conversion") ;

        // Test if 'expr_type' can be converted to the (prefix) base-type
        if (actual_arg_type && !actual_arg_type->IsCloselyRelatedType(_prefix_id)) {
            Error("cannot convert type %s to type %s", actual_arg_type->Name(), _prefix_id->Name()) ;
            return 0 ;
        }
        _is_type_conversion = 1 ;
        // Finally, run environment checks :
        CheckIdentifierUsage(_prefix_id, environment) ;

        _is_type_conversion = 1 ;

        // Return '_prefix_id->BaseType()' as the returned type. Hold for either index constraints of type-conversions.
        if (return_types) (void) return_types->Insert(_prefix_id->BaseType()) ;
        // return _prefix_id->BaseType() ;
        // Viper 6175 : Return the prefix_id for type id instead of its basetype

        return _prefix_id ;
    }

    // VHDL-2008 defines the terms "unconstrained" and "constrained" somewhat differently
    // from earlier versions of VHDL, since the situation is somewhat more involved. In
    // VHDL-2008, an array subtype is unconstrained if it has no index constraints, and the element
    // subtype is either not a composite type or is an unconstrained type. An unconstrained
    // type has no constraints anywhere in its structure where a constraint could apply.
    // An array subtype is fully constrained if it has index constraints for all of its indices, and
    // the element subtype is either not a composite type or is a fully constrained type. A fully
    // constrained type has constraints everywhere in its structure where a constraint could VHDL-2008
    // defines the terms "unconstrained" and "constrained" somewhat differently
    // from earlier versions of VHDL, since the situation is somewhat more involved. In
    // VHDL-2008, an array subtype is unconstrained if it has no index constraints, and the element
    // subtype is either not a composite type or is an unconstrained type. An unconstrained
    // type has no constraints anywhere in its structure where a constraint could apply.
    // An array subtype is fully constrained if it has index constraints for all of its indices, and
    // the element subtype is either not a composite type or is a fully constrained type. A fully
    // constrained type has constraints everywhere in its structure where a constraint could

    // Thus we can have declarations like the following:

    // type M_unconstrained is
    // array (natural range <>, natural range <>) of bit;

    // type A_unconstrained is
    // array (character range <>) of M_unconstrained;

    // type AA_unconstrained is
    // array (character range <>) of A_unconstrained;

    // signal temp1 : AA_unconstrained ('a' to 'b')('a' to 'b')(1 to 2, 2 to 3) ;
    // signal temp2 : AA_unconstrained ('x' to 'y')('x' to 'y')(3 to 4, 5 to 6) ;

    VhdlIdDef *prefix_id = 0 ;

    if (!match && IsVhdl2008() && (environment == VHDL_range || environment == VHDL_interfacerange || environment == VHDL_recordrange || environment == VHDL_subtyperange)) {
        Set prefix_types(POINTER_HASH) ;
        // Viper 7937 set the _prefix_type:
        _prefix_type = _prefix->TypeInfer(expected_type, return_types, environment, selection_scope) ;
        prefix_id = _prefix->GetFormal() ; // Finds the id of the prefix going deep recursively
    }

    // For Vhdl2008 _prefix_id is for multiple indexed names represent the id of the idref
    // and not the id of the _prefix which is null. This id is set to enable checks in the
    // following code.

    // Viper 7937: Here if we came with the prefix as a Record element then we may actually
    // be constraining the array. This can be proved if the selection_scope is a record scope.
    // Hence in that case the code should run into this code below treating this like a type constraining name
    VhdlIdDef *owner = selection_scope ? selection_scope->GetOwner() : 0 ;
    unsigned is_record_scope = owner ? owner->IsRecordType() : 0 ;

    if ((match && match->IsType()) || (prefix_id && prefix_id->IsType()) || (prefix_id && prefix_id->IsRecordElement() && is_record_scope)) {
        // !match then it is a case of multiple indices as allowed in Vhdl2008

        // OK. Do the index subtype indication

        // if !match set _prefix_id to the id of the deeper prefix.
        VhdlIdDef *curr_prefix_id = match ? match : ((prefix_id && prefix_id->IsType()) ? prefix_id : _prefix_type ) ;
        _prefix_id = match ;
        if (!_prefix_type) _prefix_type = match ; // may be already set above
        VERIFIC_ASSERT(curr_prefix_id) ;

        // Hard-Infer the arguments with the proper argument type

        // In Vhdl2008 for multi constrained index names curr_prefix_id represent the type of the
        // deeper name and not the type of the prefix. ElementTypeDeep defined of indexnames
        // descends deeper into the index name nesting and retriving the element type of the top most
        // composite type at every stage, Thus getting the correct prefix_type from the curr_prefix_id.
        // This type is called the ret_type which is used to do the checks w.r.t the _assoc_lists

        VhdlIdDef *ret_type = curr_prefix_id ;
        if (IsVhdl2008() && (environment == VHDL_range || environment == VHDL_interfacerange || environment == VHDL_recordrange || environment == VHDL_subtyperange)) {
            ret_type = _prefix->ElementTypeDeep(curr_prefix_id) ;
            // Viper 6812: For error case this function will return NULL
            // To avoid crash get the original prefix_id
            if (!ret_type) ret_type = curr_prefix_id ;
        }
        Set done_record_elements(POINTER_HASH) ;
        actual_arg_type = 0 ;
        FOREACH_ARRAY_ITEM(_assoc_list, i, assoc_elem) {
            if (!assoc_elem) continue ; // error occurred ?

            if (IsVhdl2008() && assoc_elem->IsOpen()) {
                is_slice = 1 ;
                if (_assoc_list->Size() > 1) Error("illegal syntax for subtype indication") ;
                continue ;
            }

            // Viper 6811 : ret_type contains the correct prefix type
            if (ret_type && ret_type->IsRecordType()) {
                // Viper 6811: Need to pass the record scope as selection scope to enable object finding routines.
                // Viper 7461 : pass null type as expected type.
                (void) assoc_elem->TypeInfer(0,0, VHDL_recordrange, ret_type->LocalScope()) ;
                // Viper 7461: Check whether this record field is already constrained
                VhdlIdDef* name_prefix_id = assoc_elem->GetRecordElementName() ;
                if (name_prefix_id && !done_record_elements.Insert(name_prefix_id)) {
                    assoc_elem->Error("record element %s is already constrained", curr_prefix_id->Name()) ;
                }
                _is_record_constraint = 1 ;
            } else {
                // Check if the association is sliced :
                // FIX ME : premature usage of IsRange().
                if (assoc_elem->IsRange()) is_slice = 1 ;

                // Infer the element with the proper type :
                // Take the index type for a index subtype indication,
                // but take 0 for type-conversion (type should be determinable without context !)
                arg_type = (is_slice && ret_type) ? ret_type->IndexType(i) : 0 ;

                // Now infer the argument, hard
                // environment for subtype ind is range and type-inference for expression
                actual_arg_type = assoc_elem->TypeInfer(arg_type,0, environment, selection_scope) ;
                (void) actual_arg_type ;
            }

            // Another test : (named associations not allowed in type conv/subtype indications :
            if (assoc_elem->IsAssoc()) assoc_elem->Error("illegal named association in index constraint") ;
        }

            // Some more checks :
        if (is_slice && ret_type) {
            if (!ret_type->IsArrayType()) {
                Error("index constraint cannot apply to non array type %s", ret_type->Name()) ;
                return 0 ;
            }
            if (ret_type->Dimension() != _assoc_list->Size() && !assoc_elem->IsOpen()) {
                Error("array type %s expects %d index argument(s)", curr_prefix_id->Name(), ret_type->Dimension()) ;
                return 0 ;
            }
            _is_index_constraint = 1 ;
        }
        // Finally, run environment checks :
        CheckIdentifierUsage(curr_prefix_id, environment) ;

        // Return '_prefix_id->BaseType()' as the returned type. Hold for either index constraints of type-conversions.
        if (return_types) (void) return_types->Insert(curr_prefix_id->BaseType()) ;
        // return _prefix_id->BaseType() ;
        // Viper 6175 : Return the prefix_id for type id instead of its basetype
        return curr_prefix_id ;
    }

    if (match && match->IsSubprogram()) {
        // Subprogram called with arguments.
        // We still need to check if it fits exactly.

        // Set the prefix id :
        _prefix_id = match ;

        // Issue 2148 : If this match is an alias to a subprogram, set the subprogram as _prefix_id.
        // That will 'resolve' non-object aliases, and simplify later routines (elaboration etc).
        // Do same for operator calls (VhdlOperator::TypeInfer).
        if (match->IsAlias()) {
            _prefix_id = match->GetTargetId() ;
        }

        // Set flag
        _is_function_call = 1 ;

        VERIFIC_ASSERT(_prefix_id) ;

        // Test against expected type
        if (expected_type && !expected_type->TypeMatch(_prefix_id->Type())) {
                Error("type error near %s ; expected type %s", _prefix_id->Name(), expected_type->Name());
                return 0 ;
        }

        // Vhdl 2008 LRM Section 9.3.4: A function name may not be an uninstantiated subprogram
        if (_prefix_id->GetGenerics() && !_prefix_id->IsSubprogramInst()) Error("a function name may not be an uninstantiated subprogram") ;

        // Type-infer the association list against the formals of the subprogram.
        // Do we pass 'selection_scope' in here (for actual type-conversion functions ?) CHECK ME : needed ?
        match->TypeInferAssociationList(_assoc_list, 0, selection_scope, this) ;

        // Finally, run environment checks :
        CheckIdentifierUsage(_prefix_id, environment) ;

        // Return the subprogram return type
        if (return_types) (void) return_types->Insert(_prefix_id->BaseType()) ;
        return _prefix_id->Type() ;
    }

    // VIPER #7447 : Type infer for group declaration 'group_template (group_constituent_list)'
    if (match && match->IsGroupTemplate()) {
        _is_group_template_ref = 1 ;

        // Set the prefix identifier
        _prefix_id = match ;

        // Type infer the '_assoc_list' against the group template declaration
        match->TypeInferAssociationList(_assoc_list, 0, selection_scope, this) ;

        return _prefix_id ;
    }

    // Here, only array indexes and sliced names are left-over.
    // We might have a 'match' which would then be the prefix array identifier.
    // We can use that match to get the prefix type.

    // Set prefix id.
    _prefix_id = match ;

    // First a basic check on the assoc list for indexed/siced names.
    // We need this any further down.
    is_slice = 0 ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc_elem) {
        if (!assoc_elem) continue ; // ?

        // Named associations are not allowed in indexed/sliced names arguments
        if (assoc_elem->IsAssoc()) {
            assoc_elem->Error("illegal named association in array index") ;
            return 0 ;
        }

        // FIX ME : premature call of IsRange
        // Viper 7461: Also mark is_slice for open constraint of array slice
        if (assoc_elem->IsRange() || (IsVhdl2008() && assoc_elem->IsOpen())) {
            // Error out if this is not a one-dimensional array.
            // Viper 7937 : Do not report this error for Vhdl2008 here the the array slice like a(1 to 2, 3 to 5) is valid.
            if (!IsVhdl2008() && _assoc_list->Size()>1) {
                Error("sliced name is allowed only on single-dimensional arrays") ;
                return 0 ;
            }
            is_slice = 1 ;
        }
    }

    // We will need the prefix type
    // Viper 7283: Get the already stored prefix type
    VhdlIdDef *prefix_type = (match) ? match->Type() : _prefix_type ;

    if (!prefix_type) {
        // non-object prefix cannot determine prefix type : we have to type-infer prefix :
        // We don't know what to expect though :
        // Cant pass-in the expected type for indexed names, since the prefix
        // will be some array type of 'expected_type'.
        // And for sliced names, the prefix can be a access type, so we cannot
        // push the expected name (which can be the designated type) in there either.
        // So we have to do full overloading.
        // Thankfully, we don't get in this section of code very often.
        Set prefix_types(POINTER_HASH) ;
        (void) _prefix->TypeInfer(0, &prefix_types, environment, selection_scope) ;
        if (prefix_types.Size()==0) return 0 ; // error is given already.

        // VIPER #3471 : Issue error if prefix is type conversion
        if (_prefix->IsTypeConversion()) {
            Error("cannot index the result of a type conversion") ;
        }

        // Disambiquate the prefix types based on expected type.
        VhdlIdDef *tmp_type ;
        FOREACH_SET_ITEM(&prefix_types, si, &tmp_type) {
            // If there is one left, stop disambiquation process. This gives a chance for better errors later.
            if (prefix_types.Size()==1) break ;

            // Prefix can be a access type. In that case the indexed name
            // really accesses the access value, which is of the designated type.
            prefix_type = (tmp_type->IsAccessType()) ? tmp_type->DesignatedType() : tmp_type ;

            // At least we can rule-out the array types that don't have the right dimension :
            if (!prefix_type || prefix_type->Dimension() != _assoc_list->Size()) {
                (void) prefix_types.Remove(tmp_type) ;
                continue ;
            }

            // Check against expected type (differs if we have a slice or a indexed name).
            if (expected_type && !expected_type->TypeMatch((is_slice) ? prefix_type : prefix_type->ElementType())) {
                (void) prefix_types.Remove(tmp_type) ;
                continue ;
            }
        }

        // After this, check if we have a unique prefix type :
        prefix_type = 0 ;
        if (prefix_types.Size()==1) {
            prefix_type = (VhdlIdDef*)prefix_types.GetLast() ;
        } else if (return_types && prefix_types.Size()) {
            // Upload the matching types into 'return_types', for final determination up in the expression :
            FOREACH_SET_ITEM(&prefix_types, si, &tmp_type) {
                prefix_type = (tmp_type->IsAccessType()) ? tmp_type->DesignatedType() : tmp_type ;
                // Return either a (designated) prefix type or the element
                (void) return_types->Insert( (is_slice) ? prefix_type : prefix_type->ElementType() ) ;
            }
            return 0 ;
        }

        // Hard type-infer the prefix with this prefix type.
        // If none matches, prefix_type will be zero, and errors will occur :
        (void) _prefix->TypeInfer(prefix_type, 0, environment, selection_scope) ;

        if (!prefix_type) return 0 ; // error will have been given in hard prefix call.

        // Now proceed with the final checks and argument type inference :
    }

    // Viper 7283 : Store the already found prefix type for future calls
    _prefix_type = prefix_type ;

    // Viper 6811: Typeinfer the record types and its indices
    if (IsVhdl2008() && prefix_type->IsRecordType()) {
        _is_record_constraint = 1 ;
        // Infer argument index types hard.
        // Need to pass the record scope as seltion scope to enable object finding routines.
        FOREACH_ARRAY_ITEM(_assoc_list, i, assoc_elem) {
            if (!assoc_elem) continue ;
            if (IsVhdl2008() && assoc_elem->IsOpen()) {
                if (_assoc_list->Size() > 1) Error("illegal syntax for subtype indication") ;
                continue ;
            }
            // Type-infer argument with expected index type (always element type of prefix type)
            // For normal index/sliced names, there should be no objects visible by selection in the argument..
            VhdlIdDef *element_id = prefix_type->GetElementAt(i) ;
            VhdlIdDef *elem_type = element_id ? element_id->Type() : 0 ;
            (void) assoc_elem->TypeInfer(elem_type, 0, environment, prefix_type->LocalScope()) ;
        }
    } else {
        // We now have the prefix type.
        // If the prefix type is an access type, then any indexed/sliced form
        // means an index/slice of the designated type. So work with the designated type.
        if (prefix_type->IsAccessType()) {
            prefix_type = prefix_type->DesignatedType() ;
            if (!prefix_type) return 0 ; // something is corrupt
        }

        if (!prefix_type->IsArrayType()) {
            Error("indexed name prefix type %s is not an array type", prefix_type->Name()) ;
            return 0 ;
        }

        // Check dimensionality
        if (prefix_type->Dimension() != _assoc_list->Size()) {
            // VIper 7937: In case of multidimentional array like
            // type t1 is array(natural range <>, natural range <>) of bit
            // we can define a subtype as s0 t1(open) ; However the array has
            // two dimension but the length of the assoc is only 1. The number
            // of dimension is stored in _prefix_type->Dimension. An open
            // can match with multiple dimensions of the array.
            unsigned is_open = 0 ;
            if (IsVhdl2008() && _assoc_list && _assoc_list->Size()==1 && prefix_type->Dimension()> 1) {
                assoc_elem = (VhdlDiscreteRange*)(_assoc_list->At(0)) ;
                is_open  = assoc_elem ? assoc_elem->IsOpen() : 0 ;
            }
            if (!is_open) {
                Error("indexed name prefix type %s expects %d dimensions", prefix_type->Name(), prefix_type->Dimension()) ;
                return 0 ;
            }
        }

        // Infer argument index types hard.
        FOREACH_ARRAY_ITEM(_assoc_list, i, assoc_elem) {
            if (!assoc_elem) continue ;
            if (IsVhdl2008() && assoc_elem->IsOpen()) {
                if (_assoc_list->Size() > 1) Error("illegal syntax for subtype indication") ;
                continue ;
            }
            // Type-infer argument with expected index type (always element type of prefix type)
            // For normal index/sliced names, there should be no objects visible by selection in the argument..
            (void) assoc_elem->TypeInfer(prefix_type->IndexType(i),0, (is_slice) ? VHDL_range : VHDL_READ,0) ;
        }
        // Set the flag in this tree node :
        if (is_slice) {
            _is_array_slice = 1 ;
        } else {
            _is_array_index = 1 ;

            // VIPER 3467 : check that an indexed name is not used to produce a 'range' :
            if ((environment == VHDL_range) || (environment == VHDL_interfacerange) || (environment == VHDL_ACCESS_SUBTYPE) || (environment == VHDL_recordrange) || (environment == VHDL_subtyperange)) {
                Error("%s is not a range", "indexed name") ;
            }
        }
    }

    // (If prefix_id is set) run this through the environment checks
    CheckIdentifierUsage(_prefix_id, environment) ;

#if 0 // RD: 10/2011: code moved to VarUsage, to enable more flexible ram extraction algorithms.
    // VIPER 3549 : A directly sliced identfier cannot be modeled as a multi-port RAM.
    // That is for two reasons :
    //   (1) direct slicing of a multi-port RAM identifier is not yet supported in multi-port RAM elaboration (reader side)
    //   (2) if the slice constitutes a full range of the identifier (we don't know that here), then read/write ports would
    //       be created with 0 address bits and full-range data bits. That is certainly not intended to be a RAM of any sort.
    if (_prefix_id && _prefix_id->CanBeMultiPortRam()) {
        if (_is_array_slice && _assoc_list->Size()==1) {
            // Direct slice into an identifier. Discard as a (multi-port) RAM :
            _prefix_id->SetCannotBeMultiPortRam() ;
        }
    }
#endif

    // If identifier is used as a sliced name, then it is not allowed as dual-port RAM :
    if (_prefix_id && _prefix_id->CanBeDualPortRam()) {
        if (_is_array_slice) {
            // Discard this id as a dual-port RAM if it is a array slice.
            _prefix_id->SetCannotBeDualPortRam() ;
        } else {
            // Discard this id if it's used inside a loop (issue 1568) :
            // (check if there is an exit label) :
            // We only need to do this here, since indexed name is the only
            // allowed reference of a array identifier used for RAMs.
            VhdlIdDef *label = _present_scope->FindSingleObject(" exit_label") ;
            if (label) _prefix_id->SetCannotBeDualPortRam() ;
        }
    }

    // VIPER 2352 : Discard this name specified prefix id as a dual-port RAM if it
    // has multiple indexing myram(1)(2)i.e it is declared as array of array of array
    // (Check the depth of the prefix. If depth is greater than 1, set that prefix
    // id cannot be dual-port RAM)
    if (!_prefix_id) { // Prefix is not simple name
        VhdlName *prefix = _prefix ;
        unsigned depth = 0 ;
        prefix_id = 0 ;
        while (prefix) { // Recurse through prefix to find prefix identifier and depth
            depth++ ;
            prefix_id = prefix->FindSingleObject() ;
            prefix = prefix->GetPrefix() ;
        }
        if (prefix_id && prefix_id->CanBeDualPortRam() && (depth > 1)) {
            // Depth is greater than 1, cannot be dual-port RAM
            prefix_id->SetCannotBeDualPortRam() ;
        }
    }

    // Otherwise, use the return type info that we just gathered.

    // Define return type
    VhdlIdDef *ret_type = (is_slice || _is_record_constraint) ? prefix_type->Type() : prefix_type->ElementType() ;

    // Check against expected type
    if (expected_type && !expected_type->TypeMatch(ret_type)) {
            Error("indexed name is not a %s", expected_type->Name()) ;
    }

    if (return_types) (void) return_types->Insert(ret_type->BaseType()) ;
    return ret_type->Type() ;
}

VhdlIdDef *
VhdlSignaturedName::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    VERIFIC_ASSERT(_signature) ;

    if (_unique_id) {
        // Run environment checks :
        CheckIdentifierUsage(_unique_id, environment) ;

        // CHECK ME : additional checks needed ?

        // fast determination of result type :
        VhdlIdDef *ret_type = _unique_id->Type() ;
        if (return_types && ret_type) (void) return_types->Insert(ret_type->BaseType()) ;
        return ret_type ;
    }

    // Get prefix id's.
    // filter the ones that match the signature.
    Set r(POINTER_HASH) ;
    VERIFIC_ASSERT(_name) ;
    (void) _name->FindObjects(r,0) ;

    // Filter out the ones that match the signature
    // For that, Create a temporary operator that matches the signature profile,
    // and then run a homograph check against every object in the prefix name.
    VhdlOperatorId profile(0) ; // name is not important
    _signature->FillProfile(profile) ;

    SetIter si ;
    VhdlIdDef *match=0 ;
    VhdlIdDef *id = 0 ;
    FOREACH_SET_ITEM(&r, si, &id) {
        if (!profile.IsHomograph(id)) {
            (void) r.Remove(id) ;
            continue ;
        }
        if (expected_type && !expected_type->TypeMatch(id)) {
            Error("near %s ; signature profile does not match expected type %s", id->Name(), expected_type->Name()) ;
            return id->Type() ;
        }
        match = id ;
    }

    if (r.Size()==1) {
        if (!match) return 0 ;

        _unique_id = match ;

        // Run environment checks :
        CheckIdentifierUsage(_unique_id, environment) ;

        if (return_types) (void) return_types->Insert(match->BaseType()) ;

        // VIPER #7535: Prefix of a signature should be a subprogram/
        // enumeration literal/an alias thereof.
        if (!match->IsSubprogram() && !match->IsEnumerationLiteral()) {
            Error("signature not allowed for non-overloadable entity") ;
            return match->Type() ;
        }

        return match->Type() ;
    } else {
        if (return_types && r.Size()) {
            FOREACH_SET_ITEM(&r, si, &id) {
                (void) return_types->Insert(id->BaseType()) ;
            }
        } else {
            Error("near %s ; %d visible identifiers match here", (id)?id->Name():"signature", r.Size()) ;
            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            FOREACH_SET_ITEM_BACK(&r, si, &id) {
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
        }
        return 0 ;
    }
}

VhdlIdDef *
VhdlAttributeName::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    if (!_designator || !_prefix) return 0 ; // parse error.
    //
    // Attribute names.
    //
    VhdlIdDef *result = 0 ;

    if (_result_type) {
        // Do environment checks, only if result type is set.
        // (We always re-call this routine once a valid attribute/return type is determined)

        // We don't have an identifier to check with. So do this from scratch :

        // Yacc allows attribute names as target, so at least check assignment :
        if (environment == VHDL_SIGUPDATE || environment == VHDL_VARUPDATE) {
            Error("attribute %s is illegal in assignment", _designator->Name()) ;
        }
        if ((environment == VHDL_range) ||  (environment == VHDL_interfacerange) || (environment == VHDL_ACCESS_SUBTYPE) || (environment == VHDL_recordrange) || (environment == VHDL_subtyperange)) {
            // Only a type (range) is allowed.
            // VIPER #7809 : Attribute 'element should be considered as type
            if (_attr_enum == VHDL_ATT_base || (_attr_enum != VHDL_ATT_range && _attr_enum != VHDL_ATT_reverse_range && _attr_enum != VHDL_ATT_element && _attr_enum != VHDL_ATT_subtype)) {
                Error("attribute %s is not a range", _designator->Name()) ;
            }
        }
        if (environment == VHDL_READ || environment ==  VHDL_interfaceread || environment == VHDL_subtyperangebound) {
            // range is NOT allowed in an expression.
            if (_attr_enum == VHDL_ATT_base || _attr_enum == VHDL_ATT_element || _attr_enum == VHDL_ATT_range || _attr_enum == VHDL_ATT_reverse_range || _attr_enum == VHDL_ATT_subtype) {
                Error("attribute %s is illegal in an expression", _designator->Name()) ;
            }
        }

        // Do implicit conversion if not done before :
        if (expected_type && expected_type->IsIntegerType() && _result_type->IsUniversalInteger()) {
            _result_type = expected_type ;
        }

        // VIPER 2983 : when we did an implicit conversion of index type to 'integer' previously (e.g. an index range type of an array),
        // in indexed attributes there can occur a situation where 'expected_type' is 'universal_integer' and _result_type is 'integer'.
        // In such a case, un-do the previous conversion and go back to 'expected_type' :
        if (expected_type && expected_type->IsUniversalInteger() && _result_type->IsIntegerType()) {
            _result_type = expected_type ;
        }

        result = _result_type ;
        if (expected_type && !expected_type->TypeMatch(result)) {
                Error("attribute %s does not return type %s",  _designator->Name(), expected_type->Name()) ;
        }
        if (return_types) (void) return_types->Insert(result->BaseType()) ;
        return result ;
    }

    // First check for user-defined attributes :
    if (!_attr_enum) {
        // user-defined attribute.
        // Prefix should denote a single object.
        // Get that object. How to do this with 'selection_scope' ?
        VhdlIdDef *prefix_id = _prefix->FindSingleObject() ;
        if (!prefix_id) {
            _prefix_type = _prefix->TypeInfer (0, 0, 0, selection_scope) ; // Viper #4462
            // Try again :
            prefix_id = _prefix->GetId() ;
            if (!prefix_id) return 0 ; // something went wrong with the prefix inference.
        }

        // VIPER 4589 : get the attribute identifier from the prefix identifier.
        VhdlIdDef *attr_id = prefix_id->GetAttributeId(_designator->Name()) ;

        if (!attr_id) {
            // If not yet set on the designator, then attribute spec may not yet have been processed :
            // Cannot error out yet : Attributes get 'set' on their id's only when we call ResolveSpecs().
            // That is at the end of analysis of the scope in which the spec occurs. That is NOT yet now.
            // However, such attribute identifiers should be visible in the scope right now.
            // Do not call FindSingleObject() on the designator, since that reports an'not declared' error.
            // Instead, look directly in the scope.
            attr_id = (_present_scope) ? _present_scope->FindSingleObject(_designator->Name()) : 0 ;
            if (!attr_id) {
                // NOW we can error out. Attribute identifier is really not declared (on prefix id).
                Error("%s does not have attribute %s", prefix_id->Name(), _designator->Name()) ;
                return 0 ;
            }
            // If it is there, the attribute id itself is now set
            // in the designator. Check that it is an attribute itself.
            if (!attr_id->IsAttribute()) {
                Error("%s is not an attribute", attr_id->Name()) ;
            }

            // OK. Good to go.
            // FIX ME: The attribute declaration is there, but we are still not sure if this same attribute
            // is actually attached to the designating (prefix) identifier.
            // I cannot check that here in analysis (since attr specs will be done after this)
            // Rely on elaboration to error out (check if attr_id is really set on prefix_id).
        }

        // Set this identifier in the suffix (designator) :
        _designator->SetId(attr_id) ;

        _result_type = attr_id->Type() ;
        // Now re-call type-inference, for environment checks :
        result = (_result_type) ? TypeInfer(expected_type, return_types, environment, selection_scope) : 0 ;
        return result ;
    }

    // Now check for predifined attributes that expect a named entity as prefix :
    unsigned prefix_environment = 0 ; // Viper #3646
    switch (_attr_enum) {
    case VHDL_ATT_simple_name :
    case VHDL_ATT_instance_name :
    case VHDL_ATT_path_name :
        // Get/check that there is a named object in prefix :
        (void) _prefix->FindSingleObject() ;

        // result type is a string.
        _result_type = StdType("string") ;
        // Now re-call type-inference, for environment checks :
        result = (_result_type) ? TypeInfer(expected_type, return_types, environment, selection_scope) : 0 ;
        return result ;

    case VHDL_ATT_structure :
    case VHDL_ATT_behavior :
        {
            // Check to prevent block attributes in '93 (block attributes are supported in '87, but not in '93)
            if (!IsVhdl87()) {
                Error("%s is not an attribute", _designator->Name()) ;
                return 0 ;
            }
            // Get/check that there is a named object in prefix :
            VhdlIdDef *prefix_id = _prefix->FindSingleObject() ;
            if (!prefix_id) return 0 ;
            if (!prefix_id->IsArchitecture() && !prefix_id->IsLabel()) {
                Error("attribute %s can only be applied to a block label or architecture name",  _designator->Name()) ;
                return 0 ;
            }
            // result type is a string.
            _result_type = StdType("boolean") ;
            // Now re-call type-inference, for environment checks :
            result = (_result_type) ? TypeInfer(expected_type, return_types, environment, selection_scope) : 0 ;
            return result ;
        }

    case VHDL_ATT_transaction :
    case VHDL_ATT_event :
    case VHDL_ATT_active :
    case VHDL_ATT_last_active :
    case VHDL_ATT_last_event :
    case VHDL_ATT_stable :
    case VHDL_ATT_quiet :
    case VHDL_ATT_delayed :
    case VHDL_ATT_last_value :
        prefix_environment = VHDL_READ ; // Viper #3646, LRM 4.3.2
        // prefix, if output, will give error
        break ;
    // Viper 5298. an object cannot be used within the same interface as it is declared
    // for these attribute names if the environment states that it came from interface
    // declaration then the prefix_environment is also modified to check if the prefix
    // is declared in the same interface.
    case VHDL_ATT_right :
    case VHDL_ATT_left :
    case VHDL_ATT_high :
    case VHDL_ATT_low :
    case VHDL_ATT_reverse_range :
    case VHDL_ATT_range :
    case VHDL_ATT_length :
    case VHDL_ATT_base :
        // For interface declaration use of variable in 'RANGE defined in the same interface
        // is illegal. Viper 5298, VIper 5821
        if (environment == VHDL_interfacerange || environment == VHDL_interfaceread) prefix_environment = VHDL_interfaceprefixread ;
        break ;
    default : break ; // It's something else..
    }

    // Here, we have a predefined type that always has an expression in the prefix..
    // So, we can type-infer.
    int dim = 0 ; // dimension 1

    // Viper #3483
    unsigned need_static_signal_name = 0 ;

    // VIPER #3717 : Moved type inference of prefix after processing of attributes path_name/instance_name etc.
    // Type-infer the prefix. Should be self-determined expression
    // FIX ME : pass-in right environment : some attributes constitute a read, others don't
    _prefix_type = (_prefix) ? _prefix->TypeInfer(0,0,prefix_environment,selection_scope) : 0 ;
    // Issue 2439 : prefix can easily come back 0 without an error. So error out in each attribute rule below :
    // if (!_prefix_type) return 0 ; // Something went wrong. Error was given.

    // VIPER #3647 : Produce error if prefix of predefined attribute name is signal of mode linkage (4.3.2)
    VhdlIdDef *p_id = (_prefix) ? _prefix->GetId(): 0 ;
    if (p_id && p_id->IsLinkage()) Error("cannot read attribute %s of mode linkage signal %s", (_designator) ? _designator->Name(): _prefix->Name(), p_id->Name()) ;
    // Type-infer the prefix. Should be self-determined expression
    // FIX ME : pass-in right environment : some attributes constitute a read, others don't
    //_prefix_type = (_prefix) ? _prefix->TypeInfer(0,0,0,selection_scope) : 0 ;
    // Issue 2439 : prefix can easily come back 0 without an error. So error out in each attribute rule below :
    // if (!_prefix_type) return 0 ; // Something went wrong. Error was given.

    // VIPER #4370 : Produce error if attribute base is used as prefix of another
    // attribute and prefix of 'base is array type.
    if (_prefix->GetAttributeEnum() == VHDL_ATT_base) {
        VhdlIdDef *prefix_type_of_base = _prefix->GetPrefixType() ;
        if (prefix_type_of_base && prefix_type_of_base->IsArrayType()) {
            Error("attribute %s requires a constrained array prefix type", _designator->Name()) ;
        }
    }

    VhdlIdDef *index_type ; // Type depends on attribute designator. Wait till we have that

    switch (_attr_enum) {
    case VHDL_ATT_base:
        // result = base type of the prefix
        result = (_prefix_type) ? _prefix_type->BaseType() : 0 ;
        break ;
    case VHDL_ATT_element: // VIPER #7502: new predefined attribute: prefix should be an array type
        // Attribute element is supported in vhdl_2008.
        if (!IsVhdl2008()) {
            Error("%s is not an attribute", _designator->Name()) ;
            return 0 ;
        }

        // Prefix should be an array type
        if (_prefix_type && !_prefix_type->IsArrayType()) {
            Error("near %s ; prefix should denote an array type", _designator->Name()) ;
        }
        result = (_prefix_type) ? _prefix_type->ElementType() : 0 ; // Take the element type of the _prefix_type
        _result_type = result ; // set _result_type as result
        return result ; // return result
    case VHDL_ATT_subtype: // VIPER #7522: new predefined attribute:
        {
        // Attribute subtype is supported in vhdl_2008.
        if (!IsVhdl2008()) {
            Error("%s is not an attribute", _designator->Name()) ;
            return 0 ;
        }

        // Prefix of this attribute should be an object or object alias.
        VhdlIdDef *prefix_id = _prefix ? _prefix->FindSingleObject() : 0 ;
        if (prefix_id && prefix_id->IsType()) {
            Error("near %s ; prefix should denote an object or object alias", _designator->Name()) ;
        }

        result = (_prefix) ? _prefix->TypeInfer(0, 0, 0, selection_scope) : 0 ;
        _result_type = result ;
        return result ;
        }
    case VHDL_ATT_range :
    case VHDL_ATT_reverse_range :
        // VIPER #2874/3045
        if (_prefix_type && !_prefix_type->IsArrayType()) {
            Error("near %s ; prefix should denote an array type", _designator->Name()) ;
        }
        // fall-through

    case VHDL_ATT_left :
    case VHDL_ATT_right :
    case VHDL_ATT_high :
    case VHDL_ATT_low :
        // VIPER #3697 : Check parameter if exists irrespective of prefix type :
        // Type-infer the index expression (if there)
        if (_expression) (void) _expression->TypeInfer(UniversalInteger(),0,VHDL_READ,0) ;
        if (_expression && !_expression->IsLocallyStatic()) {
            _expression->Error("attribute %s requires a locally static parameter", _designator->Name()) ;
        }

        if (_prefix_type && _prefix_type->IsArrayType()) {
            // VIPER 3167 : check IF the prefix IS a (sub)type, that it is 'constrained'.
            // VIPER #7809 : Check whether the prefix is IsUnconstrained or not
            // _prefix_type can be Unconstrained but prefix is constrained for situations like
            // obj'element'ascending
            // element type can be bit_vector (unconstrained), but subtype indication of obj is
            // constrained
            if (_prefix->IsUnconstrained(1) && _prefix->IsRange()) {
            //if (_prefix_type->IsUnconstrainedArrayType() && _prefix->IsRange()) {
                Error("attribute %s requires a constrained array prefix type", _designator->Name()) ;
            }

            // For array types, determine dimensionality and take that index' indextype

            // Get the value from '_expression'
            if (_expression) dim = (int)_expression->EvaluateConstantInteger() - 1 ;
            if (dim < 0 || dim >= (int)_prefix_type->Dimension()) {
                Error("near %s ; attribute dimensionality incorrect", _designator->Name()) ;
                dim = 0 ;
            }
            result = _prefix_type->IndexType((unsigned)dim) ;
        } else if (_prefix_type && _prefix_type->IsScalarType()) {
            // VIPER #3696 : When prefix is not an array type, attribute cannot have
            // a parameter
            if (_expression) Error("attribute %s does not take a parameter when prefix is not an array type", _designator->Name()) ;
            // VIPER 4498 : Type conversion in prefix is not allowed
            unsigned is_type_conversion = (_prefix) ? _prefix->IsTypeConversion() : 0 ;
            // VIPER #4172 : If prefix is a scalar type, prefix identifier should be type identifier
            if ((p_id && !p_id->IsType() && !_expression) || (is_type_conversion)) {
                Error("prefix of attribute %s is not a type mark", _designator->Name()) ;
            }

            // result = same type as prefix type
            result = _prefix_type ;
        } else {
            Error("near %s ; prefix should denote a scalar or array type", _designator->Name()) ;
            result = 0 ;
        }
        break ;
    case VHDL_ATT_ascending :
        if (!_prefix_type) {
            Error("near %s ; prefix should denote a scalar or array type", _designator->Name()) ;
            return 0 ;
        }
        // VIPER 3167 : check IF the prefix IS a (sub)type, that it is 'constrained'.
        // VIPER #7809 : Check whether the prefix is IsUnconstrained or not
        // _prefix_type can be Unconstrained but prefix is constrained for situations like
        // obj'element'ascending
        // element type can be bit_vector (unconstrained), but subtype indication of obj is
        // constrained
        //if (_prefix_type->IsUnconstrainedArrayType() && _prefix->IsRange()) {
        if (_prefix->IsUnconstrained(1) && _prefix->IsRange()) {
            Error("attribute %s requires a constrained array prefix type", _designator->Name()) ;
        }

        // Type-infer the index expression (if there)
        if (_expression) (void) _expression->TypeInfer(UniversalInteger(),0,VHDL_READ,0) ;
        // Get the value from '_expression'
#if 0
        // VIPER 2983 : result type does not depend on the parameter of the 'ascending' range, so do NOT elaborate it.
        // If we do, we introduce a dependency on value and constraints (e.g. need to calculate the left/right/low or high of an index range) and that is not possible in the analyser.
        // So, avoid calling EvaluateConstantInteger here. Leave that up to the elaborator.
        // In fact, this is somwhat of a hack. We SHOULD create a full locally static expression evaluator instead of EvaluateConstantInteger.
        // That one should error out if the expressin is not truly locally static.
        if (_expression) dim = _expression->EvaluateConstantInteger() - 1 ;
        // Dimension can be 0 for a scalar (see issue 2183)
        if (dim < 0 || dim > (int)_prefix_type->Dimension()) {
            Error("near %s ; attribute dimensionality incorrect", _designator->Name()) ;
            dim = 0 ;
        }
#endif
        result = StdType("boolean") ;
        break ;
    case VHDL_ATT_image :
        // Check type of image expression
        // Third check is added for Viper 3478
        if (!_prefix_type || !_prefix_type->IsScalarType() || _prefix_type->IsAccessType()) {
            Error("near %s ; prefix should denote a scalar type", _designator->Name()) ;
            return 0 ;
        }
        // Type-infer the index expression (if there)
        if (_expression) (void) _expression->TypeInfer(_prefix_type,0,VHDL_READ,0) ;
        if (!_expression) Error("attribute %s expects an argument", _designator->Name()) ;
        result = StdType("string") ;
        break ;
    case VHDL_ATT_value :
        // Type-infer the index expression (if there)
        (void) _prefix->TypeMark() ; // Viper #5619: Will error if not a type
        if (!_prefix_type || !_prefix_type->IsScalarType()) {
            Error("near %s ; prefix should denote a scalar type", _designator->Name()) ;
            return 0 ;
        }
        if (_expression) (void) _expression->TypeInfer(StdType("string"),0,VHDL_READ,0) ;
        if (!_expression) Error("attribute %s expects an argument", _designator->Name()) ;
        result = _prefix_type->BaseType() ;
        break ;
    case VHDL_ATT_pos :
        // Type-infer the index expression (if there)
        if (_expression) {
            (void) _expression->TypeInfer(_prefix_type,0,VHDL_READ,0) ;
            // prefix must be the type-id
            if (p_id && !p_id->IsType()) Error ("prefix of attribute %s is not a type mark", _designator->Name()) ; // Viper #8119
        } else {
            Error("attribute %s expects an argument", _designator->Name()) ;
        }
        // Implicit conversion handled when we return
        result = UniversalInteger() ;
        break ;
    case VHDL_ATT_val :
        // Type-infer the index expression (if there)
        (void) _prefix->TypeMark() ; // Viper #5619: Will error if not a type
        if (!_prefix_type || !(_prefix_type->IsDiscreteType() || _prefix_type->IsPhysicalType())) {
            Error("near %s ; prefix should denote a discrete or physical type or subtype", _designator->Name()) ; // Viper #5619: More checks
        }
        index_type = (_expression) ? _expression->TypeInfer(0,0,VHDL_READ,0) : 0 ;
        if (!_expression) Error("attribute %s expects an argument", _designator->Name()) ;
        if (index_type && !index_type->IsIntegerType()) {
            Error("attribute %s index type should be integer", _designator->Name()) ;
        }
        result = (_prefix_type) ? _prefix_type->BaseType() : 0 ;
        break ;
    case VHDL_ATT_succ :
    case VHDL_ATT_pred :
    case VHDL_ATT_leftof :
    case VHDL_ATT_rightof :
        if (!_prefix_type || (!_prefix_type->IsDiscreteType() && !_prefix_type->IsPhysicalType())) {
            Error("prefix of attribute %s should be a discrete or physical (sub)type", _designator->Name()) ;
            return 0 ;
        }
        result = _prefix_type->BaseType() ;
        // Type-infer the index expression (if there)
        if (_expression) (void) _expression->TypeInfer(result,0,VHDL_READ,0) ;
        if (!_expression) Error("attribute %s expects an argument", _designator->Name()) ;
        break ;
    case VHDL_ATT_length :
        if (!_prefix_type || !_prefix_type->IsArrayType()) {
            Error("prefix of LENGTH attribute should be an array or array (sub)type") ;
            return 0 ;
        }
        // VIPER 3167 : check IF the prefix IS a (sub)type, that it is 'constrained'.
        //if (_prefix_type->IsUnconstrainedArrayType() && _prefix->IsRange()) {
        // VIPER #7809 : Check whether the prefix is IsUnconstrained or not
        // _prefix_type can be Unconstrained but prefix is constrained for situations like
        // obj'element'ascending
        // element type can be bit_vector (unconstrained), but subtype indication of obj is
        // constrained
        if (_prefix->IsUnconstrained(1) && _prefix->IsRange()) {
            Error("attribute %s requires a constrained array prefix type", _designator->Name()) ;
        }
        // Type-infer the index expression (if there)
        if (_expression) (void) _expression->TypeInfer(UniversalInteger(),0,VHDL_READ,0) ;
        // Get the value from '_expression'
        if (_expression) dim = (int)_expression->EvaluateConstantInteger() - 1 ;
        if (dim < 0 || dim >= (int)_prefix_type->Dimension()) {
            Error("near %s ; attribute dimensionality incorrect", _designator->Name()) ;
            dim = 0 ;
        }
        // Implicit conversion handled when we return
        result = UniversalInteger() ;
        break ;
    case VHDL_ATT_delayed :
        {
        // Type-infer the index expression (if there)
        need_static_signal_name = 1 ;
        if (_expression) (void) _expression->TypeInfer(StdType("time"),0,VHDL_READ,0) ;
        // LRM Section 14.1 on 'delayed. Parameter must be static expression of type time
        if (_expression && !_expression->IsGloballyStatic()) _expression->Error("parameter in attribute delayed must be globally static expression") ;
        // VIPER #3473 : Issue error for reading delayed from formal signal parameter (LRM 2.1.1.2)
        VhdlIdDef *prefix_id = _prefix->GetId() ;
        // Check whether prefix is a formal signal parameter of a subprogram
        if (prefix_id && prefix_id->IsSignalParameterOfSubprogram()) {
            // Prefix is formal signal parameter, issue error
            Error("attribute delayed may not be read from formal signal parameter %s", prefix_id->Name()) ;
        }
        result = (_prefix_type) ? _prefix_type->BaseType() : 0 ;
        break ;
        }
    case VHDL_ATT_stable :
    case VHDL_ATT_quiet :
        {
        // VIPER #3623 : Issue error for reading stable from formal signal parameter
        VhdlIdDef *prefix_id = _prefix->GetId() ;
        // Check whether prefix is a formal signal parameter of subprogram
        if (prefix_id && prefix_id->IsSignalParameterOfSubprogram()) {
            // Prefix is formal signal parameter, issue error
            Error("attribute %s may not be read from formal signal parameter %s", _designator ? _designator->Name(): "", prefix_id->Name()) ;
        }
        // Type-infer the index expression (if there)
        need_static_signal_name = 1 ;
        if (_expression) (void) _expression->TypeInfer(StdType("time"),0,VHDL_READ,0) ;
        result = StdType("boolean") ;
        break ;
        }
    case VHDL_ATT_transaction :
        {
        VhdlIdDef *prefix_id = _prefix->GetId() ;
        // Check whether prefix is a formal signal parameter of subprogram
        if (prefix_id && prefix_id->IsSignalParameterOfSubprogram()) {
            // Prefix is formal signal parameter, issue error
            Error("attribute %s may not be read from formal signal parameter %s", "transaction", prefix_id->Name()) ;
        }
        need_static_signal_name = 1 ;
        result = StdType("bit") ;
        break ;
        }
    case VHDL_ATT_event :
    case VHDL_ATT_active :
        need_static_signal_name = 1 ;
        result = StdType("boolean") ;
        break ;
    case VHDL_ATT_last_active :
    case VHDL_ATT_last_event :
        need_static_signal_name = 1 ;
        result = StdType("time") ;
        break ;
    case VHDL_ATT_last_value :
    case VHDL_ATT_driving_value :
        need_static_signal_name = 1 ;
        result = (_prefix_type) ? _prefix_type->BaseType() : 0;
        break ;
    case VHDL_ATT_driving :
        need_static_signal_name = 1 ;
        result = StdType("boolean") ;
        break ;
    case VHDL_ATT_simple_name :
    case VHDL_ATT_instance_name :
    case VHDL_ATT_path_name :
        result = StdType("string") ;
        break ;
    default :
        // User-defined attributes are already handled on top.
        // So this must be a pre-defined attribute that we forgot :
        Error("no support for attribute %s",_designator->Name()) ;
        break ;
    }

    if (need_static_signal_name && (!_prefix->IsSignal() || !_prefix->IsGloballyStaticName())) {
        _prefix->Error("attribute %s requires a static signal prefix", _designator->Name()) ;
    }

    _result_type = result ;
    // Now re-call type-inference, for environment checks :
    result = (_result_type) ? TypeInfer(expected_type, return_types, environment, selection_scope) : 0 ;
    return result ;
}

VhdlIdDef *
VhdlOpen::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return expected_type ;
}

VhdlIdDef *
VhdlEntityAspect::TypeInfer(VhdlIdDef * /*expected_type*/, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return 0 ;
}

VhdlIdDef *
VhdlInstantiatedUnit::TypeInfer(VhdlIdDef * /*expected_type*/, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return 0 ;
}
VhdlIdDef *
VhdlExternalName::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    Set r(POINTER_HASH) ;
    (void) FindObjects(r, 0) ;

    // If _unique_id is set by 'FindObjects' routine, check its class type and subtype indication
    return (_subtype_indication) ? _subtype_indication->TypeInfer(expected_type,return_types,VHDL_range,0) : 0 ;
}

/*
 ******************* TypeInferFormal ***************************
 * Given a formal_part in a association element.
 * The scope where the formal can be found is passed into this function.
 *
 * return the type of the formal expression.
 * return 0 if the formal part is not valid (by any means).
 * Give an error before returning 0.
 *
 * This is almost the same as normal expression type-inference, only that
 * we have to count with type-conversions in the formal.
 * These are special (1-arg) function calls or type-conversion, where the 'formal'
 * is in a function argument. That's tricky : Can't do normal association list resolving
 * since that does all the normal checks (formal can be an 'out' port).
 * Any way, these guys need special treatment any way, with all the
 * extra tests that the LRM requires. So do them here.
 *
 ******************* TypeInferFormal **************************
*/

VhdlIdDef *VhdlName::TypeInferFormal(VhdlScope * /*local_scope*/, VhdlIdDef * /*expected_type*/)
{
    Error("illegal formal part") ;
    return 0 ;
}

VhdlIdDef *VhdlDesignator::TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type)
{
    if (_single_id) return _single_id->Type() ;
    if (!local_scope) return 0 ;

    // Identifier in formal can only be a real formal designator.
    // that is declared in the local scope.
    _single_id = local_scope->FindSingleObjectLocal(_name) ;
    if (!_single_id) {
        Error("formal %s is not declared", _name) ;
        return 0 ;
    }

    // Still check type-compliance.
    // Previous calls could have been without expected type.
    // and calls from range cannot always guarantee that we used result type
    if (expected_type && !expected_type->TypeMatch(_single_id->Type())) {
        Error("type error near %s ; expected type %s", _name, expected_type->Name()) ;
    }

    // Return type of the formal
    return _single_id->Type() ;
}

VhdlIdDef *VhdlSelectedName::TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type)
{
    // Just call type-inference, with local scope as selection scope.
    // This is a little bit risky, since we don't know for sure if we will get the formal designator
    // from the local scope or not. Might need to pass-in a special 'environment' ?
    // Check the test done in SelectedName::TypeInfer, regarding VIPER 2781.
    return TypeInfer(expected_type, 0, 0, local_scope) ;
}

VhdlIdDef *VhdlIndexedName::TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type)
{
    VERIFIC_ASSERT(_prefix) ;

    // Formal indexed name. Could be type-tranformation or function call,
    // or a regular partial formal. Find the difference first :
    _prefix_id = 0 ; // Clear this out here

    // If prefix is a simple name, we need to look in 'selected_scope'.
    VhdlIdDef *id = 0 ;
    if (local_scope && _prefix->Name()) {
        // Objects by selection (LRM 10.3) are never overloadable, and always local.
        id = local_scope->FindSingleObjectLocal(_prefix->Name()) ;
        // No need to insert in the overloading Set 'r'. It's a indexed/sliced name.
    }

    // Set needed to collect overload functions ids (if they exist) (VIPER #1462)
    Set ids(POINTER_HASH) ;

    if (!id) {
        // It's not in the selected scope. Check if the prefix is an expanded name,
        // or a plain identifier. Only then will we search all possible definitions
        // for it.  This is to make partial formals work. FIX ME : It's still messy.
        VhdlName *prefix = _prefix ;
        while (prefix && prefix->GetPrefix()) prefix = prefix->GetPrefix() ;
        if (local_scope && prefix && prefix->Name() && local_scope->FindSingleObjectLocal(prefix->Name())) {
            // It is an ordinary partial formal
        } else if (_prefix->Name() || _prefix->IsExpandedName()) {
            // OK, must be a type-conversion function or type-conversion.
            // We need to assume it's overloaded, therefore call FindObjects. (VIPER #1462)
            (void) _prefix->FindObjects(ids,0);
            if (ids.Size() == 1) id = (VhdlIdDef*)ids.GetAt(0) ;
        }
    }

    if ((ids.Size()<=1) && (!id || (!id->IsType() && !id->IsSubprogram()))) {
        // It's a regular partial formal.
        // Just call type-inference, with local scope as selection scope :
        return TypeInfer(expected_type, 0, 0, local_scope) ;
    }

    // Here, it's a type-conversion function or type-conversion.
    // There must be ONE and only ONE argument, and it should not be OPEN,
    // and it should not have a named association :

    // Check that we have one argument only.
    if (!_assoc_list || _assoc_list->Size()!=1) {
        // Error is a bit silly...
        Error("designator type-conversion should have one argument") ;
        return 0 ;
    }

    VhdlDiscreteRange *assoc_elem = (VhdlDiscreteRange*)_assoc_list->At(0) ;

    if (!assoc_elem || assoc_elem->IsOpen()) {
        Error("designator in type-conversion cannot be OPEN") ;
        return (id) ? id->Type() : 0 ;
    }

    if (assoc_elem->IsAssoc()) {
        Error("named association not allowed in designator") ;
        return (id) ? id->Type() : 0 ;
    }

    // The argument will contains the formal designator.
    // Just call type-inference, with local scope as selection scope :
    VhdlIdDef *arg_type = assoc_elem->TypeInfer(0,0,0,local_scope) ;

    // Set it as the right one :
    if (id) {
        _prefix_id = id ;
    } else {
        // This is an overloaded function.  Determine the correct id to set the prefix_id to (VIPER #1462)
        SetIter si ;
        FOREACH_SET_ITEM(&ids, si, &id) {
            if (!id) continue ;
            // First look at argument type
            if (arg_type && !arg_type->TypeMatch(id->ArgType(0))) continue ;
            // Now look at return type (incoming expected_type)
            if (expected_type && !expected_type->TypeMatch(id->Type())) continue ;
            // Could do more here : check num of args, check overloading and ambiguity etc..
            _prefix_id = id ; // take the first match
            break ;
        }
    }

    if (!_prefix_id) {
        Error("near %s ; %d visible identifiers match here", (id) ? id->Name() : "formal type conversion function", 0) ;
        return 0 ; // No type match
    }

    if (_prefix_id->IsType()) {
        // Type conversion
        _is_type_conversion=1 ;
        if (arg_type && !arg_type->IsCloselyRelatedType(_prefix_id->BaseType())) {
            Error("near formal type-conversion ; cannot convert type %s to type %s", arg_type->Name(), _prefix_id->Name()) ;
        }
    } else {
        // Conversion function
        _is_function_call=1 ;
        if (arg_type && !arg_type->TypeMatch(_prefix_id->ArgType(0))) {
            Error("function %s does not take type %s as argument", _prefix_id->Name(), arg_type->Name()) ;
        }
        if (expected_type && !expected_type->TypeMatch(id->Type())) {
            Error("type error near %s ; expected type %s", _prefix_id->Name(), expected_type->Name()) ;
        }
    }

    return _prefix_id->Type() ;
}

// *************************ElementTypeDeep*************************
// ElementTypeDeep(VhdlIdDef *prefix_type)  returns the element type
// of the prefix type to that much deep which is equal to the index nesting of the name
// for example in this is a(1 to 2)(3 to 5) then the function should return
// prefix_type->ElementType()->ElementType()

VhdlIdDef *
VhdlIndexedName::ElementTypeDeep(VhdlIdDef *prefix_type)
{
    return (_prefix && prefix_type) ? _prefix->ElementTypeDeep(prefix_type->ElementType()) : 0 ;
}

// *************************GetNamedPrefixId*************************
// Recursively goes to _prefix to find _prefix_id
// viper 7461: Need to check wheather a same record field is contrained multiple times
VhdlIdDef *
VhdlDiscreteRange::GetRecordElementName() const
{
    Error("bad record element constraint") ;
    return 0 ;
}
VhdlIdDef *
VhdlIndexedName::GetRecordElementName() const
{
    VhdlIdDef *record_element = GetNamedPrefixId() ;
    if (record_element && !record_element->IsRecordElement()) {
         Error("%s is not a record element", record_element->Name()) ;
    }
    return record_element ;
}
VhdlIdDef *
VhdlIndexedName::GetNamedPrefixId() const
{
    VhdlIdDef *prefix_id = _prefix ? _prefix->GetId() : 0 ;
    if (prefix_id) return prefix_id ;
    return (_prefix ? _prefix->GetNamedPrefixId() : 0 ) ;
}

//******************* IsRange ***************************
// Given an expression,
// return 1 if this represents a range, 0 if not.
// Range is a name that denotes a discrete type
//******************* IsRange **************************

unsigned VhdlName::IsRange()  { return 0 ; } // default to false

unsigned VhdlIdRef::IsRange()
{
    // The identifier can represent a range, if it is a (sub)type

    // Don't set the _single_id if its not already set (we are not doing
    // type-resolving here) But use it if it is set.
    VhdlIdDef *id = _single_id ;
    if (!id && _present_scope) id = _present_scope->FindSingleObject(_name) ;

    return (id) ? id->IsType() : 0 ;
}

unsigned VhdlAttributeName::IsRange()
{
    // Attribute is only a range if it returns a type.
    // That's only the case for 'base 'range or 'reverse_range
    return (_attr_enum == VHDL_ATT_base ||
            _attr_enum == VHDL_ATT_range ||
            _attr_enum == VHDL_ATT_element || // VIPER #7809 : 'element is subtype
            _attr_enum == VHDL_ATT_subtype ||
            _attr_enum == VHDL_ATT_reverse_range) ? 1 : 0 ;
}

unsigned VhdlSelectedName::IsRange()
{
    // The identifier can represent a range, if it is a (sub)type

    // Don't set the _single_id if its not already set (we are not doing
    // type-resolving here) But use it if it is set.
    VhdlIdDef *id = _unique_id ;
    if (id) return id->IsType() ; // it's a range if the identifier is a type (the type determines the range).

    // Try find it in the scope.. Carefull not to make premature overloading choices, so use FindObjects.
    if (_present_scope && _suffix && !_suffix->IsAll()) {
        Set r ;
        (void) FindObjects(r,0) ;
        id = (VhdlIdDef*)r.GetLast() ;
    }
    return (id) ? id->IsType() : 0 ;
}
unsigned VhdlExternalName::IsRange()
{
    VhdlIdDef *id = _unique_id ;
    if (id) return id->IsType() ; // it's a range if the identifier is a type (the type determines the range).
    return 0 ;
}

/*
 ******************* IsExpandedName ***************************
 * Given a name,
 * return 1 if this represents a expanded name
 * and not an access value or record element.
 * Does not change the parse tree.
 * It does not complain, and is 'pessimistic' : If it's not exactly an expanded name, return 0.
 *
 ******************* IsExpandedName **************************
*/

unsigned VhdlName::IsExpandedName() const { return 0 ; }

unsigned VhdlSelectedName::IsExpandedName() const
{
    if (!_prefix || !_suffix) return 0 ;

    // Check if this thing is already resolved :
    if (_unique_id) {
        // LRM 6.3, rules
        if (_unique_id->IsRecordElement()) return 0 ;
        VhdlIdDef *base_type = _unique_id->BaseType() ;
        // VIPER #5429: Check the type of _unique_id also
        if ((_unique_id->IsAccessType() || (base_type && base_type->IsAccessType())) && _suffix->IsAll()) return 0 ; // correct test ?
        // Otherwise, if resolved, it is an expanded name.
        return 1 ;
    }

    // Look at the prefix :
    VhdlIdDef *prefix_id = 0 ;
    // Check if prefix is expanded name. Otherwise selected name is NOT an expanded name for sure.
    if (_prefix->Name() || _prefix->IsExpandedName()) {
        // complex prefix (as in VIPER issue 2353.
        // Assume that we should not overload the prefix, so must me single identifier there :
        // VIPER #5108 : use FindObjects as FindSingleObject errors out for
        // multiple match. This is common with function overloading
        // prefix_id = _prefix->FindSingleObject() ;
        Set r(POINTER_HASH) ;
        if (!_prefix->FindObjects(r,0)) return 0 ; // Nothing there by this name
        prefix_id = (VhdlIdDef*)r.GetLast() ;
    } else if (_prefix->IsExternalNameRef()) {
        // VIPER #7893 : Prefix can be external reference to protected type variable,
        // we can resolve the suffix from type information of external name
        // as we do not have resolved prefix object
        prefix_id = _prefix->TypeInfer(0, 0, 0, 0) ;
    }
    if (!prefix_id) return 0 ;

    // Check conditions for expanded name from LRM 6.3 :
    if (prefix_id->IsLibrary()) return 1 ;
    if (prefix_id->IsPackage()) return 1 ;
    if (prefix_id->IsProtected()) return 1 ;

    if (!_suffix->IsAll()) {
        // LRM 6.3 : 104-108 : only if suffix is not 'all' :
        if (prefix_id->IsLabel()) return 1 ;
        if (prefix_id->IsEntity()) return 1 ;
        if (prefix_id->IsArchitecture()) return 1 ;
        if (prefix_id->IsSubprogram()) {
            // LRM 6.3 : 109-113 : If prefix is a subprogram, then we always interpret it as a expanded name.
            // Never interpret as a function call.
            // HOWEVER : vi_suite test cases violate this rule (e.g. ch03_sc02_sb02_p001_s010204).
            // There are function calls in prefix.
            // So use a 'heuristic' to work around this LRM violation :

            // If the function has no arguments, it is a function call.
            // Otherwise, it is a function by name.
            if (prefix_id->NumOfArgs()) return 1 ;
            // Here, function does not require arguments.
            // Assume that a function call is intended, so return 0.
            // VIPER 5108 : make sure we re-set the prefix id backpointer, so that type-inference
            // can do overloading :
            _prefix->SetId(0) ;
            return 0 ;
        }
    }

    // not an expanded name
    return 0 ;
}

/**********************************************************/
//  Formal name of an association : find the formal designator
// This is the name behind the type-conversion
// This routine assumes that type-inference already ran
/**********************************************************/

VhdlExpression *VhdlName::FormalDesignator() const { return const_cast<VhdlName*>(this) ; } // plain name

VhdlExpression *VhdlIndexedName::FormalDesignator() const
{
    if (_is_array_index || _is_array_slice) {
        return const_cast<VhdlIndexedName*>(this) ; // partial formal designator
    }
    if (_is_function_call || _is_type_conversion) {
        // type-conversion function
        if (!_assoc_list || _assoc_list->Size()!=1) return const_cast<VhdlIndexedName*>(this) ; // Maybe a actual part expression ? accept as plain name.
        // Designator is in arg 1 :
        return (VhdlExpression*)_assoc_list->At(0) ;
    }
    return const_cast<VhdlIndexedName*>(this) ; // Don't know what it is. Not type-infered yet. Accept as plain name.
}

/**********************************************************/
//  Formal name of an association : find the formal identifier
// This routine assumes that type-inference already ran
/**********************************************************/

// First : return the IdDef only if it's the one and only here :
VhdlIdDef *VhdlName::FindFullFormal() const         { return 0 ; }
VhdlIdDef *VhdlDesignator::FindFullFormal() const   { return _single_id ; }

// Return the IdDef if its in the prefix of partial names and past type-conversion func's too.
VhdlIdDef *VhdlName::FindFormal() const             { return 0 ; }
VhdlIdDef *VhdlDesignator::FindFormal() const       { return _single_id ; }

VhdlIdDef *VhdlSelectedName::FindFormal() const
{
    // If this is not a record element, then it is a expanded name, which we should use right away.
    // Happens for use of FindFormal in actual designators (issue 1438)
    if (_unique_id && !_unique_id->IsRecordElement()) return _unique_id ;
    // Here, its a record element. Find formal in the prefix :
    return (_prefix) ? _prefix->FindFormal() : 0 ;
}

VhdlIdDef *VhdlIndexedName::FindFormal() const
{
    if (_is_array_index || _is_array_slice) {
        return (_prefix) ? _prefix->FindFormal() : 0 ;
    }
    if (_is_function_call || _is_type_conversion) {
        // type-conversion function
        if (!_assoc_list || _assoc_list->Size()!=1) return 0 ; // No formal name visible through expression...
        // Designator is in arg 1 :
        VhdlDiscreteRange *assoc_elem = (VhdlDiscreteRange*)_assoc_list->At(0) ;
        return (assoc_elem) ? assoc_elem->FindFormal() : 0 ;
    }
    return 0 ; // Don't know what it is
}
// VIPER #4279 : Routine to set new identifier as formal :
void VhdlName::SetFormal(VhdlIdDef * /*formal*/)       {}
void VhdlDesignator::SetFormal(VhdlIdDef *formal) { _single_id = formal ; }
void VhdlSelectedName::SetFormal(VhdlIdDef *formal)
{
    // If this is not a record element, then it is a expanded name, which we should use right away.
    if (_unique_id && !_unique_id->IsRecordElement()) {
        _unique_id = formal ;
        return ;
    }
    // Here, its a record element. Set formal in the prefix :
    if (_prefix) _prefix->SetFormal(formal) ;
}
void VhdlIndexedName::SetFormal(VhdlIdDef *formal)
{
    if (_is_array_index || _is_array_slice) {
        if (_prefix) _prefix->SetFormal(formal) ;
    }
    if (_is_function_call || _is_type_conversion) {
        // type-conversion function
        if (!_assoc_list || _assoc_list->Size()!=1) return ; // No formal name visible through expression...
        // Designator is in arg 1 :
        VhdlDiscreteRange *assoc_elem = (VhdlDiscreteRange*)_assoc_list->At(0) ;
        if (assoc_elem) assoc_elem->SetFormal(formal) ;
    }
}

// Return the IdDef by moving across prefix es.
VhdlIdDef *VhdlName::GetFormal() const             { return 0 ; }
VhdlIdDef *VhdlDesignator::GetFormal() const       { return _single_id ; }

VhdlIdDef *VhdlIndexedName::GetFormal() const
{
        return (_prefix) ? _prefix->GetFormal() : 0 ;
}
/**********************************************************/
//  Find the identifier in the target name of an alias
// Mainly needed so we can test the alias id for
// properties like IsSignal() ect.
// This routine assumes that type-inference already ran
/**********************************************************/

VhdlIdDef *VhdlName::FindAliasTarget() const            { return 0 ; }

VhdlIdDef *VhdlDesignator::FindAliasTarget() const      { return _single_id ; }

VhdlIdDef *VhdlSelectedName::FindAliasTarget() const
{
    if (_unique_id && !_unique_id->IsRecordElement()) return _unique_id ;
    return (_prefix) ? _prefix->FindAliasTarget() : 0 ;
}

VhdlIdDef *VhdlSignaturedName::FindAliasTarget() const  { return _unique_id ; }

VhdlIdDef *VhdlIndexedName::FindAliasTarget() const
{
    if (_is_array_index || _is_array_slice) {
        // Alias is a regular array object.
        return (_prefix) ? _prefix->FindAliasTarget() : 0 ;
    }
    if (_is_function_call) {
        // If alias target is a function, return the function id
        return (_prefix) ? _prefix->FindAliasTarget() : 0 ;
    }
    return 0 ; // Don't know what it is
}

/**********************************************************/
//  For Configuration Specification names : find
//  This is block-specification visibility by selection
//  LRM 10.3(b) and ln 155
/**********************************************************/

VhdlIdDef *VhdlName::BlockSpec()
{
    Error("illegal block specification name") ;
    return 0 ;
}
VhdlIdDef *VhdlName::BlockSpecPrefix()
{
    return BlockSpec() ;
}

VhdlIdDef *VhdlDesignator::BlockSpecPrefix()
{
    if (_single_id) return _single_id ; // We have been here before

    // find the block label in the present scope.
    // LRM 1.3.1 states that the block statement must be an
    // architecture or a label, contained IMMEDIATELY within the
    // block of the CONTAINING block or component configuration.
    // So, do a LOCAL search in the EXTENDED area.

    if (_present_scope) {
        if (_present_scope->GetOwner()) {
            if (_present_scope->GetOwner()->IsConfiguration()) {
                // external block config directly inside a configuration declaration
                // LRM 10.4(b) : find architecture by selection
                // Get the entity of this configuration :
                VhdlIdDef *entity = _present_scope->GetOwner()->GetEntity() ;
                VhdlPrimaryUnit *entity_tree = (entity) ? entity->GetPrimaryUnit() : 0 ;
                // Find the architecture of this entity by (this) block spec name :
                VhdlSecondaryUnit *arch_tree = (entity_tree) ? entity_tree->GetSecondaryUnit(_name) : 0 ;
                if (arch_tree) {
                    _single_id = arch_tree->Id() ; // Set this architecture as the block spec
                } else if (entity) {
                    Error("architecture %s not found in entity %s", _name, entity->Name()) ;
                    return 0 ;
                }
            }
        }
        if (!_single_id) {
            // Old way, still needed for external block configs with component configurations :
            // (they are put in the present 'included' scope (by yacc code),
            // for containing configuration declarations and component configurations.
            _single_id = _present_scope->FindSingleObjectIncluded(_name) ;
        }
        if (!_single_id && _present_scope->GetExtendedDeclAreaScope()) {
            // Then check for (block) labels :
            // For containing block configurations, an extended decl area was included. So check in there :
            _single_id = _present_scope->GetExtendedDeclAreaScope()->FindSingleObjectLocal(_name) ;
        }
    }

    if (!_single_id) {
        Error("%s is not found", _name) ;
        return 0 ;
    }
    //if ( (!_single_id->IsLabel() && !_single_id->IsArchitecture()) || !_single_id->LocalScope()) {
    VhdlStatement *gen_stmt = _single_id->GetStatement() ;
    if ( (!_single_id->IsLabel() && !_single_id->IsArchitecture()) || (!_single_id->LocalScope() && !(gen_stmt && (gen_stmt->IsCaseGenerateStatement() || gen_stmt->IsIfElsifGenerateStatement())))) {
        // We allow architectures, generate-statement labels or block labels.
        // These all should have a local scope. Can't test better than that for now.
        Error("%s is not a block label", _name) ;
        return 0 ;
    }

    return _single_id ;
}

VhdlIdDef *VhdlDesignator::BlockSpec()
{
    VhdlIdDef *id = BlockSpecPrefix() ;

    VhdlStatement *gen_stmt = id ? id->GetStatement(): 0 ;

    if (gen_stmt && (gen_stmt->IsCaseGenerateStatement() || gen_stmt->IsIfElsifGenerateStatement())) {
        // If only generate label is specified in block configuration, it applies
        // to first alternative label of if/case generate statement
        // VIPER #7639: Get alternative label for case generate statement from
        // its alternatives
        VhdlStatement *first_stmt = 0 ;
        if (gen_stmt->IsCaseGenerateStatement()) {
            // Get the case generate items
            Array *items = gen_stmt->GetAlternatives() ;
            first_stmt = (items && items->Size()) ? (VhdlStatement*)items->At(0): 0 ;
        } else {
            first_stmt = gen_stmt ;
        }
        VhdlIterScheme *scheme = first_stmt ? first_stmt->GetScheme(): 0 ;
        id = (scheme && scheme->GetAlternativeLabelId()) ? scheme->GetAlternativeLabelId(): id ;
    }
    return id ;
}
VhdlIdDef *VhdlIndexedName::BlockSpec()
{
    // Must be an index_spec of a generation statement label
    // FIX ME : Check that it is a generation statement label ?
    if (!_assoc_list || _assoc_list->Size()!=1) {
        Error("expected exactly one index specification") ;
        return 0 ;
    }

    // type-infer the index range..
    VhdlDiscreteRange *assoc = (VhdlDiscreteRange*)_assoc_list->At(0) ;

    if (!assoc || assoc->IsOpen()) {
        Error("block specification index cannot be open") ;
        return 0 ;
    }

    //VhdlIdDef *assoc_type = assoc->TypeInfer(0,0, (assoc && assoc->IsRange()) ? VHDL_range : VHDL_READ,0) ;

    // Viper #3488 : Label may not have an indexed name
    //if (!assoc->IsGloballyStatic()) Error("index/slice name with label prefix must have static index/slice") ;

    // Viper# 3170 : check index type
    VhdlIdDef *prefix_id = _prefix ? _prefix->BlockSpecPrefix() : 0 ; //_prefix ? _prefix->FindFormal() : 0 ;
    VhdlStatement *generate_stmt = prefix_id ? prefix_id->GetStatement() : 0 ;
    VhdlIterScheme *scheme = 0 ; //generate_stmt ? generate_stmt->GetScheme() : 0 ;
    // VHDL_2008 : If alternative label is specified as 'assoc' here, we need
    // to return the alternative label id from this routine, so that scope of
    // that branch of if/case generate can be associated with block configuration
    VhdlIdDef *alt_label = 0 ;
    // VIPER #7639: Handle case/if generate properly to get proper alternative label
    if (generate_stmt && (generate_stmt->IsCaseGenerateStatement() || generate_stmt->IsIfElsifGenerateStatement())) {
        // Check the validity of alternate label
        if (generate_stmt->IsCaseGenerateStatement()) {
            // Get the case generate items
            Array *items = generate_stmt->GetAlternatives() ;
            unsigned i ;
            VhdlStatement *case_item ;
            FOREACH_ARRAY_ITEM(items, i, case_item) {
                if (!case_item) continue ;
                scheme = case_item->GetScheme() ;
                alt_label = scheme ? scheme->CheckIterType(assoc, this): 0 ;
                if (alt_label) break ;
            }
        } else {
            scheme = generate_stmt->GetScheme() ;
            alt_label = scheme ? scheme->CheckIterType(assoc, this): 0 ;

            Array *elsif_list = generate_stmt->GetElsifList() ;
            VhdlStatement *else_stmt = generate_stmt->GetElseStatement() ;
            if (!alt_label) {
                unsigned i ;
                VhdlStatement *branch_stmt ;
                FOREACH_ARRAY_ITEM(elsif_list, i, branch_stmt) {
                    if (!branch_stmt) continue ;
                    scheme = branch_stmt->GetScheme() ;
                    alt_label = scheme ? scheme->CheckIterType(assoc, this): 0 ;
                    if (alt_label) break ;
                }
            }
            if (!alt_label) {
                scheme = else_stmt ? else_stmt->GetScheme() : 0 ;
                alt_label = scheme ? scheme->CheckIterType(assoc, this): 0 ;
            }
        }
    } else {
        scheme = generate_stmt ? generate_stmt->GetScheme() : 0 ;
    }
    if (!alt_label && scheme) alt_label = scheme->CheckIterType(assoc, this) ;

    //return (_prefix) ? _prefix->BlockSpec() : 0 ;
    return (alt_label) ? alt_label : prefix_id ;
}

/**********************************************************/
//  For Entity aspects / instantiated units:
// matches rules for instantiated units and entity aspects
// Should call this routine during analysis (LRM 5.2.1.1)
/**********************************************************/

/**********************************************************/
// EntityAspect returns the primary unit (entity or configuration) or component
// of a entity aspect or instantiated unit
// It will skip the (optional) architecture name
// Get that with ArchitectureNameAspect
// This routine does NOT check the class of the returned id ; do that outside
/**********************************************************/
VhdlIdDef *VhdlName::EntityAspect()
{
    Error("illegal unit name") ;
    return 0 ;
}

VhdlIdDef *VhdlOpen::EntityAspect()
{
    // OPEN entity aspect.
    return 0 ;
}

VhdlIdDef *VhdlDesignator::EntityAspect()
{
    VhdlIdDef *result = FindSingleObject() ;
    // VIPER #3970: If identifier is alias id, return target identifier.
    if (_single_id && _single_id->IsAlias()) result = _single_id->GetTargetId() ;
    // Issue 1638 : Don't pre-set subprograms as an entity aspect :
    if (_single_id && (_single_id->IsProcedure() || _single_id->IsFunction())) _single_id = 0 ;
    return result ;
}

VhdlIdDef *VhdlSelectedName::EntityAspect()
{
    // VIPER 4657 : entity aspect should expect a library in the prefix.
    // ModelSim seems to NOT search in scope for the library name, but do a plain direct library search :
    // So, do NOT use FindSingleObject. Use something else to get the library identifier
    VhdlIdDef *result = FindSingleObject() ;

    // Issue 1638 : Don't pre-set subprograms as an entity aspect :
    if (_unique_id && (_unique_id->IsProcedure() || _unique_id->IsFunction())) _unique_id = 0 ;
    return result ;
}

VhdlIdDef *VhdlIndexedName::EntityAspect()
{
    // Argument should denote a architecture of the prefix' entity
    // This routine thus just returns the prefix.
    VhdlIdDef *result =  (_prefix) ? _prefix->EntityAspect() : 0 ;
    // Set the result (if primary unit) as prefix identifier
    if (result && (result->IsEntity() || result->IsConfiguration())) _prefix_id = result ;
    return result ;
    // Don't error out here on incorrect index argument :
    // labeled procedure calls go through here too. (the yacc odity).
    // Error out if we would request the architecture name (below).
}

VhdlIdDef *VhdlEntityAspect::EntityAspect()
{
    if (!_name) return 0 ;
    // We already checked the class type. Just call the entityaspect on the name.
    return _name->EntityAspect() ;
}

VhdlIdDef *VhdlInstantiatedUnit::EntityAspect()
{
    if (!_name) return 0 ;
    // We already checked the class type. Just call the entityaspect on the name.
    VhdlIdDef *unit = _name->EntityAspect() ;

    return unit ;
}

/**********************************************************/
// ArchitectureNameAspect returns the name of the architecture
// in a entity aspect or instantiated unit
/**********************************************************/

const char *VhdlName::ArchitectureNameAspect() const
{
    return 0 ;
}

const char *VhdlIndexedName::ArchitectureNameAspect() const
{
    // Name is in the one and only argument
    if (!_assoc_list || _assoc_list->Size()!=1) {
        Error("expected an architecture identifier in index") ;
        return 0 ;
    }
    VhdlDiscreteRange *assoc = (VhdlDiscreteRange*)_assoc_list->At(0) ;
    if (!assoc) return 0 ;
    const char *result = assoc->Name() ; // architecture name is here.
    if (!result) {
        Error("expected an architecture identifier in index") ;
    }

    // Also, check that the prefix is an entity now ?
    return result ;
}

const char *VhdlEntityAspect::ArchitectureNameAspect() const
{
    if (!_name) return 0 ;
    return _name->ArchitectureNameAspect() ;
}

const char *VhdlInstantiatedUnit::ArchitectureNameAspect() const
{
    if (!_name) return 0 ;
    return _name->ArchitectureNameAspect() ;
}

/**********************************************************/
// TypeMark returns the type / subtype id.
/**********************************************************/

VhdlIdDef *VhdlName::TypeMark()
{
    VhdlIdDef *type = FindSingleObject() ;
    // According to the LRM, type_mark : type_name | subtype_name
    if (type) {
        if (type->IsType() || type->IsSubtype()) return type ;
        Error("%s is not a type", type->Name()) ;
    }
    return 0 ;
}

VhdlIdDef *VhdlIndexedName::TypeMark()
{
    // Because FindSingleObject() does not output an error message
    // for VhdlIndexedNames, we need to do this here.
    Error("%s is not a type", "indexed name") ;
    return 0 ;
}

/**************************************************************/
// IsSignal
/**************************************************************/
unsigned VhdlName::IsSignal(void) { return 0 ; }
unsigned VhdlDesignator::IsSignal(void)
{
    return _single_id ? _single_id->IsSignal() : 0 ;
}

unsigned VhdlSelectedName::IsSignal(void)
{
    if (_unique_id && _unique_id->IsSignal()) return 1 ;
    if (_suffix && _suffix->IsSignal()) return 1 ;
    return _prefix ? _prefix->IsSignal() : 0 ;
}

unsigned VhdlIndexedName::IsSignal(void) { return _prefix ? _prefix->IsSignal() : 0 ; }

unsigned VhdlAttributeName::IsSignal(void)
{
    if (!_attr_enum) return 0 ;
    // only few pre-defined attributes return signal
    // e.g. S'delayed returns signal, S'active does not
    switch (_attr_enum) {
    case VHDL_ATT_delayed:
    case VHDL_ATT_stable:
    case VHDL_ATT_quiet:
    case VHDL_ATT_transaction:
        return 1 ;
    default:
        return 0 ;
    }

    return 0 ;
}
unsigned VhdlExternalName::IsSignal(void)
{
    if (_class_type == VHDL_signal) return 1 ;
    return 0 ;
}
/**************************************************************/
// IsAccessType
/**************************************************************/
unsigned VhdlName::IsAccessType() { return 0 ; }

unsigned VhdlDesignator::IsAccessType()
{
    if (!_single_id) return 0 ;

    VhdlIdDef *base_type = _single_id->BaseType() ;
    return base_type ? base_type->IsAccessType() : 0 ;
}

unsigned VhdlSelectedName::IsAccessType()
{
    if (_unique_id) {
        VhdlIdDef *base_type = _unique_id->BaseType() ;
        if (base_type && base_type->IsAccessType()) return 1 ;
    }

    if (_suffix && _suffix->IsAccessType()) return 1 ;
    return _prefix ? _prefix->IsAccessType() : 0 ;
}

unsigned VhdlIndexedName::IsAccessType()
{
    return _prefix ? _prefix->IsAccessType() : 0 ;
}
unsigned VhdlExternalName::IsAccessType()
{
    if (_unique_id) {
        VhdlIdDef *base_type = _unique_id->BaseType() ;
        if (base_type && base_type->IsAccessType()) return 1 ;
    }
    return 0 ;
}

/**************************************************************/
// IsContainsType
/**************************************************************/
unsigned VhdlName::ContainsAccessType() { return 0 ; }

unsigned VhdlDesignator::ContainsAccessType()
{
    if (!_single_id) return 0 ;

    VhdlIdDef *base_type = _single_id->BaseType() ;
    return base_type ? base_type->ContainsAccessType() : 0 ;
}

unsigned VhdlSelectedName::ContainsAccessType()
{
    if (_unique_id) {
        VhdlIdDef *base_type = _unique_id->BaseType() ;
        if (base_type && base_type->ContainsAccessType()) return 1 ;
    }

    if (_suffix && _suffix->ContainsAccessType()) return 1 ;
    return _prefix ? _prefix->ContainsAccessType() : 0 ;
}

unsigned VhdlIndexedName::ContainsAccessType()
{
    return _prefix ? _prefix->ContainsAccessType() : 0 ;
}

/**************************************************************/
// IsProtectedType
/**************************************************************/
unsigned VhdlName::IsProtectedType() { return 0 ; }

unsigned VhdlDesignator::IsProtectedType()
{
    if (!_single_id) return 0 ;

    VhdlIdDef *base_type = _single_id->BaseType() ;
    return base_type ? base_type->IsProtectedType() : 0 ;
}

unsigned VhdlSelectedName::IsProtectedType()
{
    if (_unique_id) {
        VhdlIdDef *base_type = _unique_id->BaseType() ;
        if (base_type && base_type->IsProtectedType()) return 1 ;
    }

    if (_suffix && _suffix->IsProtectedType()) return 1 ;
    return _prefix ? _prefix->IsProtectedType() : 0 ;
}

unsigned VhdlIndexedName::IsProtectedType()
{
    return _prefix ? _prefix->IsProtectedType() : 0 ;
}
unsigned VhdlExternalName::IsProtectedType()
{
    if (_unique_id) {
        VhdlIdDef *base_type = _unique_id->BaseType() ;
        if (base_type && base_type->IsProtectedType()) return 1 ;
    }
    return 0 ;
}
/**************************************************************/
// Locally/Globally Static Names : LRM section 6.1
/**************************************************************/
unsigned VhdlName::IsLocallyStaticName() const        { return 0 ; }
unsigned VhdlName::IsGloballyStaticName() const       { return 0 ; }

unsigned VhdlDesignator::IsLocallyStaticName() const  { return 0 ; }
unsigned VhdlDesignator::IsGloballyStaticName() const { return 0 ; }

unsigned VhdlAttributeName::IsLocallyStaticName() const
{
    if (!_attr_enum) return 0 ;
    if (_prefix && !_prefix->IsLocallyStaticName()) return 0 ;
    return 1 ;
}

unsigned VhdlAttributeName::IsGloballyStaticName() const
{
    if (!_attr_enum) return 0 ;
    if (_prefix && !_prefix->IsGloballyStaticName()) return 0 ;
    return 1 ;
}

unsigned VhdlIdRef::IsLocallyStaticName() const
{
    if (!_single_id) return 1 ; // to avoid false negative
    return _single_id->IsLocallyStaticName() ;
}

unsigned VhdlIdRef::IsGloballyStaticName() const
{
    if (!_single_id) return 1 ; // to avoid false negative
    return _single_id->IsGloballyStaticName() ;
}

unsigned VhdlArrayResFunction::IsLocallyStaticName() const
{
    if (_res_function) return _res_function->IsLocallyStaticName() ; // to avoid false negative
    return 0 ;
}

unsigned VhdlArrayResFunction::IsGloballyStaticName() const
{
    if (_res_function) return _res_function->IsGloballyStaticName() ; // to avoid false negative
    return 0 ;
}

unsigned VhdlIndexedName::IsLocallyStaticName() const
{
    if (_is_function_call) return 0 ;

    VhdlDiscreteRange *assoc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
        if (assoc && !assoc->IsLocallyStatic()) return 0 ;
    }

    if (_prefix && !_prefix->IsLocallyStaticName()) return 0 ;

    return 1 ;
}

unsigned VhdlIndexedName::IsGloballyStaticName() const
{
    if (_is_function_call) return 0 ;

    VhdlDiscreteRange *assoc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
        if (assoc && !assoc->IsGloballyStatic()) return 0 ;
    }

    if (_prefix && !_prefix->IsGloballyStaticName()) return 0 ;

    return 1 ;
}
unsigned VhdlExternalName::IsLocallyStatic() const
{
    return 0 ; // External name is not locally static
}
unsigned VhdlExternalName::IsLocallyStaticName() const
{
    return 0 ; // External name is not locally static
}
unsigned VhdlExternalName::IsGloballyStatic() const
{
    return 0 ; // External name is not globally static
}
unsigned VhdlExternalName::IsGloballyStaticName() const
{
    return 0 ; // External name is not globally static
}
unsigned VhdlIdRef::IsGoodForPortActual() const
{
    if (!_single_id) return 1 ;  // be conservative for false negative

    // Viper 7486: Exclude protected type and shared variables (which are
    // necessarily of protected type). Need to explicitly add this check
    // here, not inside Is(Locally/Globally)StaticName to comply with the
    // popular simulators
    if (_single_id->IsProtected() || _single_id->IsSharedVariable()) return 0 ;
    return (IsGloballyStaticName() || IsGloballyStatic()) ;
}
unsigned VhdlIndexedName::IsGoodForPortActual() const
{
    if (_prefix && !_prefix->IsGoodForPortActual()) return 0 ;

    // Viper #4460 : static_name/static_expr are always good
    if (IsGloballyStaticName() || IsGloballyStatic()) return 1 ;

    // Viper 7645: array slice is allowed in 2008 mode.
    if (!_is_function_call && !_is_type_conversion && (!IsVhdl2008() || !_is_array_slice)) return 0 ;

    if (_is_array_slice) {
        // For array slice the prefix should be good and the assoc a globally static expression
        if (!_prefix || !_prefix->IsGoodForPortActual()) return 0 ;
        VhdlDiscreteRange *assoc_elem = (_assoc_list && (_assoc_list->Size()>0)) ? (VhdlDiscreteRange*)_assoc_list->At(0) : 0 ;
        if (!assoc_elem || !(assoc_elem->IsGloballyStatic() || assoc_elem->IsGloballyStaticName())) return 0 ;
    }

    if (_is_function_call && IsVhdl2008() && _prefix && _prefix_id && !_prefix->IsArrayResolutionFunction() && !_prefix->IsRecordResolutionFunction()) {
        // Viper 7645: For functions which are non resolution function, It is good for actual
        // if the return type is globally static or the function is a pragma function.
        if (_prefix_id->GetPragmaFunction()) return 1 ;
        VhdlSpecification* function_spec = _prefix_id->Spec() ;
        VhdlName* return_type = function_spec ? function_spec->GetReturnType() : 0 ;
        if (return_type && return_type->IsUnconstrained(1)) return 0 ;
        if (return_type && return_type->IsGloballyStatic()) return 1 ;
        return 0 ;
    }

    if (_is_type_conversion && IsVhdl2008() && _prefix_id) {
        if (_prefix_id->IsUnconstrained()) return 0 ;
        if (_prefix_id->IsGloballyStaticType()) return 1 ;
    }

    // This may be a resolution function.. Check it has a single
    // argument and the argument is good for port actual
    if (!_assoc_list || _assoc_list->Size() != 1) return 0 ;

    if (_is_function_call && _prefix_id) {
        if (_prefix_id->NumOfArgs() != 1) return 0 ;
        VhdlIdDef *arg = _prefix_id->Arg(0) ;

        // Viper #3069 : conversion function argument kind must be constant
        if (!arg || (arg->GetKind() && (arg->GetKind() != VHDL_constant))) return 0 ;
    }

    VhdlDiscreteRange *assoc_elem = (VhdlDiscreteRange*)_assoc_list->At(0) ;
    return (assoc_elem && assoc_elem->IsGoodForPortActual()) ;
}

unsigned VhdlExternalName::IsGoodForPortActual() const
{
    // External names are of type signal,variable or constant.
    // All are treated to be good for actual
    return 1 ;
}

unsigned VhdlSelectedName::IsLocallyStaticName() const
{
    if (_suffix && !_suffix->IsLocallyStaticName()) return 0 ;
    if (_prefix && !_prefix->IsLocallyStaticName()) return 0 ;

    return 1 ;
}

unsigned VhdlSelectedName::IsGloballyStaticName() const
{
    if (_suffix && !_suffix->IsGloballyStaticName()) return 0 ;
    if (_prefix && !_prefix->IsGloballyStaticName()) return 0 ;

    return 1 ;
}

/**************************************************************/
// Locally/Globally Static : LRM sections 7.4.1 and 7.4.2
/**************************************************************/
unsigned VhdlName::IsLocallyStatic() const            { return 0 ; }
unsigned VhdlName::IsGloballyStatic() const           { return IsLocallyStatic() ; }
unsigned VhdlName::IsLocallyStaticAttPrefix() const   { return IsLocallyStatic() ; }
unsigned VhdlName::IsGloballyStaticAttPrefix() const  { return IsGloballyStatic() ; }

unsigned VhdlLiteral::IsLocallyStatic() const         { return 1 ; }
unsigned VhdlLiteral::IsGloballyStatic() const        { return 1 ; }

// VhdlDesignator : others, all, unaffected, string/character_literal
unsigned VhdlDesignator::IsLocallyStatic() const      { return 1 ; }
unsigned VhdlDesignator::IsGloballyStatic() const     { return 1 ; }

unsigned VhdlSelectedName::IsLocallyStatic() const
{
    // According to LRM 7.4.1 : selected names are NOT locally, static BUT : Modelsim seems to make an exeption..
    // For strict LRM compliance, uncomment following line
    // return 0 ;

    // Locally static selected name : prefix has to be locally static :
    if (_prefix && !_prefix->IsLocallyStatic()) return 0 ;
    // Watch out : selected names can be 'expanded' names or 'record' elements. Suffix (element) of a record is always static.
    // For 'expanded names' (prefix is a 'block' or 'entity' or library or so), VhdlIdDef::IsLocallyStatic() determines 'static'-ness.
    // For record fields (prefix is a record object), VhdlElementId::IsLocallyStatic()) determines 'static'-ness.
    if (_suffix && !_suffix->IsLocallyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlSelectedName::IsGloballyStatic() const
{
    // VIPER 5098 : LRM 7.4.2(r) : prefix must be globally static for result to be globally static
    if (_prefix && !_prefix->IsGloballyStatic()) return 0 ;
    // Watch out : selected names can be 'expanded' names or 'record' elements. Suffix (element) of a record is always static.
    // For 'expanded names' (prefix is a 'block' or 'entity' or library or so), VhdlIdDef::IsGloballyStatic() determines 'static'-ness.
    // For record fields (prefix is a record object), VhdlElementId::IsGloballyStatic()) determines 'static'-ness.
    if (_suffix && !_suffix->IsGloballyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlPhysicalLiteral::IsLocallyStatic() const
{
    VhdlIdDef *unit = (_unit) ? _unit->FindSingleObject() : 0 ;
    if (unit && unit->IsStdTimeType()) return 0 ;

    return 1 ;
}

unsigned VhdlIdRef::IsLocallyStatic() const
{
    if (!_single_id) return 1 ;  // be conservative for false negative
    return _single_id->IsLocallyStatic() ;
}

unsigned VhdlIdRef::IsGloballyStatic() const
{
    if (!_single_id) return 1 ;  // be conservative for false negative
    return _single_id->IsGloballyStatic() ;
}
unsigned VhdlIdRef::IsLocallyStaticAttPrefix() const
{
    if (!_single_id) return 1 ;  // be conservative for false negative
    if (_single_id->IsLocallyStatic() || _single_id->IsLocallyStaticType()) return 1 ;
    return 0 ;
}
unsigned VhdlIdRef::IsGloballyStaticAttPrefix() const
{
    if (!_single_id) return 1 ;  // be conservative for false negative
    if (_single_id->IsGloballyStatic() || _single_id->IsGloballyStaticType()) return 1 ;
    return 0 ;
}
unsigned VhdlArrayResFunction::IsLocallyStatic() const
{
    if (_res_function) return _res_function->IsLocallyStatic() ; // to avoid false negative
    return 0 ;
}

unsigned VhdlArrayResFunction::IsGloballyStatic() const
{
    if (_res_function) return _res_function->IsGloballyStatic() ; // to avoid false negative
    return 0 ;
}

unsigned VhdlSignaturedName::IsLocallyStatic() const
{
    if (!_unique_id) return 1 ;  // be conservative for false negative
    return _unique_id->IsLocallyStatic() ;
}

unsigned VhdlSignaturedName::IsGloballyStatic() const
{
    if (!_unique_id) return 1 ;  // be conservative for false negative
    return _unique_id->IsGloballyStatic() ;
}

unsigned VhdlIndexedName::IsLocallyStatic() const
{
    // According to LRM 7.4.1 : bit/part select is NOT locally,
    // static even if prefix and all the indices are locally
    // static BUT : Modelsim seems to make an exeption..
    // For strict LRM compliance, uncomment following line
    // if (_is_array_index || _is_array_slice) return 0 ;

    VhdlDiscreteRange *assoc ;
    unsigned i = 0 ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
        if (assoc && !assoc->IsLocallyStatic()) return 0 ;
    }

    if (_is_function_call) {
        if (_prefix_id && !_prefix_id->IsPredefinedOperator()) return 0 ;
        return 1 ;
    }

    if (_prefix_id && _prefix_id->IsType() && _prefix_id->Dimension() == i) {
        // This is an array type with constant indices in the subtype_indication
        // The element of the array must always be static for correct syntax
        return 1 ;
        // VhdlIdDef *elem_type = _prefix_id->ElementType() ;
        // return elem_type->IsLocallyStaticType() ;
    } else if (_prefix && !_prefix->IsLocallyStatic()) {
        return 0 ;
    }

    return 1 ;
}

unsigned VhdlIndexedName::IsGloballyStatic() const
{
    VhdlDiscreteRange *assoc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
        if (assoc && !assoc->IsGloballyStatic()) return 0 ;
    }

    if (_is_function_call) {
        if (!_prefix_id) return 0 ;
        VhdlSpecification *spec = _prefix_id->Spec() ;
        if (spec && spec->IsImpure()) return 0 ;

        return 1  ;
    }

    if (_prefix && !_prefix->IsGloballyStatic()) return 0 ;

    return 1 ;
}

unsigned VhdlIndexedName::IsLocallyStaticAttPrefix() const
{
    if (IsLocallyStatic() || (_prefix && _prefix->IsLocallyStaticAttPrefix())) return 1 ;
    return 0 ;
}
unsigned VhdlIndexedName::IsGloballyStaticAttPrefix() const
{
    if (IsGloballyStatic() || (_prefix && _prefix->IsGloballyStaticAttPrefix())) return 1 ;
    return 0 ;
}
unsigned VhdlAttributeName::IsLocallyStatic() const
{
    switch(_attr_enum) {
    case 0: // user defined attribute
        {
            if (_expression && !_expression->IsLocallyStatic()) return 0 ;

            VhdlIdDef *attr_id = _designator ? _designator->FindSingleObject() : 0 ;
            VhdlIdDef *prefix_id = _prefix ? _prefix->FindSingleObject() : 0 ;

            if (!attr_id || !prefix_id) return 1 ;

            VhdlExpression *attr_value = prefix_id->GetAttribute(attr_id) ;
            if (attr_value && !attr_value->IsLocallyStatic()) return 0 ;

            return 1 ;
        }
    case VHDL_ATT_path_name:
    case VHDL_ATT_event:
    case VHDL_ATT_active:
    case VHDL_ATT_last_event:
    case VHDL_ATT_last_active:
    case VHDL_ATT_last_value:
    case VHDL_ATT_driving_value:
    case VHDL_ATT_driving:
        return 0 ;
    default:
        {
            if (_expression && !_expression->IsLocallyStatic()) return 0 ;

            VhdlIdDef *prefix_id = _prefix ? _prefix->FindSingleObject() : 0 ;
            if (prefix_id && !prefix_id->IsLocallyStaticType()) return 0 ;

            // VIPER #6435: Check whether '_prefix' is locally static
            // VIPER #6823 : Prefix is either a locally static subtype or an
            // an object that is of a locally static subtype.
            if (!prefix_id && _prefix && !_prefix->IsLocallyStaticAttPrefix()) return 0 ;
            return 1 ;
        }
    }

    return 0 ;
}

unsigned VhdlAttributeName::IsGloballyStatic() const
{
    switch(_attr_enum) {
    case VHDL_ATT_event:
    case VHDL_ATT_active:
    case VHDL_ATT_last_event:
    case VHDL_ATT_last_active:
    case VHDL_ATT_last_value:
    case VHDL_ATT_driving_value:
    case VHDL_ATT_driving:
        return 0 ;
    default:
        {
            if (_expression && !_expression->IsGloballyStatic()) return 0 ;

            VhdlIdDef *prefix_id = _prefix ? _prefix->FindSingleObject() : 0 ;
            if (prefix_id && !prefix_id->IsGloballyStaticType()) return 0 ;

            // VIPER #6435: Check whether '_prefix' is globally static
            // VIPER #6823 : Prefix is either a locally static subtype or an
            // an object that is of a locally static subtype.
            if (!prefix_id && _prefix && !_prefix->IsGloballyStaticAttPrefix()) return 0 ;
            return 1 ;
        }
    }

    return 0 ;
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_NAME(SIG, NAME)  (((SIG) << 5) + (SIG) + (Hash::StringCompare((NAME), 0)))
#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlName::CalculateSignature() const
{
    unsigned long sig = VhdlSubtypeIndication::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlDesignator::CalculateSignature() const
{
    // VIPER #7804: If _single_id exists and it is not a design unit
    // call CalculateSignature() on _single_id.
    if (_single_id && !_single_id->IsDesignUnit()) {
        unsigned long sig = HASH_VAL(0, _single_id->CalculateSignature()) ;
        return sig ;
    }

    unsigned long sig = VhdlName::CalculateSignature() ;
    if (_name) sig = HASH_NAME(sig, _name) ;
    // back-pointer : if (_single_id) sig = HASH_VAL(sig, _single_id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlLiteral::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlAll::CalculateSignature() const
{
    unsigned long sig = VhdlDesignator::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlOthers::CalculateSignature() const
{
    unsigned long sig = VhdlDesignator::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlUnaffected::CalculateSignature() const
{
    unsigned long sig = VhdlDesignator::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlInteger::CalculateSignature() const
{
    unsigned long sig = VhdlLiteral::CalculateSignature() ;
    sig = HASH_VAL(sig, (unsigned long)_value) ;
    return sig ;
}

unsigned long
VhdlReal::CalculateSignature() const
{
    unsigned long sig = VhdlLiteral::CalculateSignature() ;
    sig = HASH_VAL(sig, (unsigned long)_value) ;
    return sig ;
}

unsigned long
VhdlStringLiteral::CalculateSignature() const
{
    unsigned long sig = VhdlDesignator::CalculateSignature() ;
    // VIPER #7804: for string create signature with _name
    // one entity with report"Failed"
    // another entity with report"Passed"
    // signature with _name gives the proper signature
    if (_name) sig = HASH_NAME(sig, _name) ;
    return sig ;
}

unsigned long
VhdlBitStringLiteral::CalculateSignature() const
{
    unsigned long sig = VhdlStringLiteral::CalculateSignature() ;
    if (_based_string) sig = HASH_NAME(sig, _based_string) ;
    return sig ;
}

unsigned long
VhdlCharacterLiteral::CalculateSignature() const
{
    unsigned long sig = VhdlDesignator::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlPhysicalLiteral::CalculateSignature() const
{
    unsigned long sig = VhdlLiteral::CalculateSignature() ;
    if (_value) sig = HASH_VAL(sig, _value->CalculateSignature()) ;
    if (_unit) sig = HASH_VAL(sig, _unit->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlNull::CalculateSignature() const
{
    unsigned long sig = VhdlLiteral::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlIdRef::CalculateSignature() const
{
    unsigned long sig = VhdlDesignator::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlArrayResFunction::CalculateSignature() const
{
    if (_res_function) return _res_function->CalculateSignature() ; // to avoid false negative
    return 0 ;
}

unsigned long
VhdlRecordResFunction::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    if (_rec_res_function_list) {
        VhdlExpression *expr ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_rec_res_function_list, i, expr) {
            if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
        }
    }
    return sig ;
}

unsigned long
VhdlSelectedName::CalculateSignature() const
{
    // VIPER #7804: Check for IsType() on _unique_id is required for the following case:
    // checking conformity for UNSIGNED and IEEE.STD_LOGIC_ARITH.UNSIGNED
    // Check for IsSubtype() on _unique_id is required for the following case:
    // subtype u is unsigned ; and conformity check is done on work.p.u and u.
    if (_unique_id && (_unique_id->IsType() || _unique_id->IsSubtype())) {
        unsigned long sig = HASH_VAL(0, _unique_id->CalculateSignature()) ;
        return sig ;
    }

    unsigned long sig = VhdlName::CalculateSignature() ;
    if (_prefix) sig = HASH_VAL(sig, _prefix->CalculateSignature()) ;
    if (_suffix) sig = HASH_VAL(sig, _suffix->CalculateSignature()) ;
    // back-pointer : if (_unique_id) sig = HASH_VAL(sig, _unique_id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlIndexedName::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    if (_prefix) sig = HASH_VAL(sig, _prefix->CalculateSignature()) ;
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_prefix_id) sig = HASH_VAL(sig, _prefix_id->CalculateSignature()) ;
    sig = HASH_VAL(sig, _is_array_index) ;
    sig = HASH_VAL(sig, _is_array_slice) ;
    sig = HASH_VAL(sig, _is_function_call) ;
    sig = HASH_VAL(sig, _is_index_constraint) ;
    sig = HASH_VAL(sig, _is_record_constraint) ;
    sig = HASH_VAL(sig, _is_type_conversion) ;
    return sig ;
}

unsigned long
VhdlSignaturedName::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    if (_name) sig = HASH_VAL(sig, _name->CalculateSignature()) ;
    if (_signature) sig = HASH_VAL(sig, _signature->CalculateSignature()) ;
    // back-pointer : if (_unique_id) sig = HASH_VAL(sig, _unique_id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlAttributeName::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    if (_prefix) sig = HASH_VAL(sig, _prefix->CalculateSignature()) ;
    if (_designator) sig = HASH_VAL(sig, _designator->CalculateSignature()) ;
    if (_expression) sig = HASH_VAL(sig, _expression->CalculateSignature()) ;
    sig = HASH_VAL(sig, _attr_enum) ;
    // back-pointer : if (_prefix_type) sig = HASH_VAL(sig, _prefix_type->CalculateSignature()) ;
    // back-pointer : if (_result_type) sig = HASH_VAL(sig, _result_type->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlOpen::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlEntityAspect::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    sig = HASH_VAL(sig, _entity_class) ;
    if (_name) sig = HASH_VAL(sig, _name->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlInstantiatedUnit::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    sig = HASH_VAL(sig, _entity_class) ;
    if (_name) sig = HASH_VAL(sig, _name->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlExternalName::CalculateSignature() const
{
    unsigned long sig = VhdlName::CalculateSignature() ;
    sig = HASH_VAL(sig, _class_type) ;
    if (_path_token) sig = HASH_VAL(sig, _path_token) ;
    if (_hat_count) sig = HASH_VAL(sig, _hat_count) ;
    if (_ext_path_name) sig = HASH_VAL(sig, _ext_path_name->CalculateSignature()) ;
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    return sig ;
}

///////////////////////////////////////////////////////////////////////
//    Change reference name and resolved id                         ///
///////////////////////////////////////////////////////////////////////

// Set name reference : Absorbs coming char*
void VhdlDesignator::SetName(char *name)
{
    Strings::free( _name ) ;
    _name = name ;
#ifdef VHDL_PRESERVE_ID_CASE
    // Now here, always down-case the identifier, except when it's an extended identifier
    //    if (_name && *_name != '\\' && *_name != '\'' && *_name!='"') _name = Strings::strtolower(_name) ;
//CARBON_BEGIN
    // case change is controlled by -vhdlCase command line switch
    if (_name && *_name != '\\' && *_name != '\'' && *_name!='"') {
      if      (gMakeVhdlIdsLower){ _name = Strings::strtolower(_name) ;}
      else if (gMakeVhdlIdsUpper){ _name = Strings::strtoupper(_name) ;}
    }
//CARBON_END
#endif
}
void VhdlSelectedName::SetName(char *name)
{
    // suffix is referened actually, so change suffix
    if (_suffix) _suffix->SetName(name) ;
}
void VhdlIndexedName::SetName(char *name)
{
    // Prefix contains referenced name, so change prefix
    if (_prefix) _prefix->SetName(name) ;
}
void VhdlInstantiatedUnit::SetName(char *name)
{
    if (_name) _name->SetName(name) ;
}
void VhdlExternalName::SetName(char *name)
{
    if (_ext_path_name) _ext_path_name->SetName(name) ;
}
// Set resolved identifier
void VhdlDesignator::SetId(VhdlIdDef *new_id)
{
    _single_id = new_id ;
}
void VhdlSelectedName::SetId(VhdlIdDef *new_id)
{
    // Set the 'unique' id reference for this selected name :
    _unique_id = new_id ;
    // suffix is referened actually, so change suffix id pointer too :
    if (_suffix) _suffix->SetId(new_id) ;
}
void VhdlIndexedName::SetId(VhdlIdDef *new_id)
{
    // Prefix contains referened name, so change prefix
    _prefix_id = new_id ;
    if (_prefix) _prefix->SetId(new_id) ;
}
void VhdlInstantiatedUnit::SetId(VhdlIdDef *new_id)
{
    if (_name) _name->SetId(new_id) ;
}
void VhdlExternalName::SetId(VhdlIdDef *new_id)
{
    if (_ext_path_name) _ext_path_name->SetId(new_id) ;
}

//////////////// Unconstrained element check ////////////////////////////
// VIPER #7809 : For type'element, check whether element is unconstrained or not
unsigned VhdlIdRef::HasUnconstrainedElement() const
{
    return (_single_id) ? _single_id->HasUnconstrainedElement(): 0 ;
}
unsigned VhdlDiscreteRange::HasUnconstrainedElement() const
{
    return IsUnconstrained(0) ;
}
unsigned VhdlIndexedName::HasUnconstrainedElement() const
{
    // Need to check element of prefix array
    return _prefix ? _prefix->HasUnconstrainedElement(): 0 ;
}
unsigned VhdlSelectedName::HasUnconstrainedElement() const
{
    return _unique_id ? _unique_id->HasUnconstrainedElement(): 0 ;
}
// FIXME : Not sure for other expressions or names
//////////////// Unconstrained type check ////////////////////////////
unsigned VhdlIdRef::IsUnconstrained(unsigned all_level) const
{
    // Check resolved identifier's constraint
    return (_single_id) ? (_single_id->IsUnconstrainedArrayType() || (all_level && _single_id->IsUnconstrained())): 0 ;
}

unsigned VhdlArrayResFunction::IsUnconstrained(unsigned all_level) const
{
    if (_res_function) return _res_function->IsUnconstrained(all_level) ; // to avoid false negative
    return 0 ;
}

unsigned VhdlSelectedName::IsUnconstrained(unsigned all_level) const
{
    // Check resolved identifier's constraint
    return (_unique_id) ? (_unique_id->IsUnconstrainedArrayType() || (all_level && _unique_id->IsUnconstrained())): 0 ;
}
unsigned VhdlIndexedName::IsUnconstrained(unsigned all_level) const
{
    unsigned num_unconstrained = _prefix ? _prefix->NumUnconstrainedRanges() : 0 ;

    if (num_unconstrained) {
        // Prefix is unconstrained array type. Check whether constraint is specified
        // by range
        VhdlDiscreteRange *range ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_assoc_list, i, range) {
            if (!range) continue ;
            // If any dimension is unconstrained, return 1
            if (range->IsUnconstrained(all_level)) {
                // VIPER #7809 : Only check one level
                if ((all_level == 0) && (i == 0)) return 1 ; // This level is unconstrained
                continue ;
            }
            if (all_level == 0) return 0 ; // No need to check other levels (VIPER #7809)
            // Viper 7937: range may contain multiple constraints set. We need to subtract the
            // total number of unconstrained constraints set by this range from the total number of
            // unconstrained ranges.
            VhdlIdDef *named_prefix_id = range->GetNamedPrefixId() ;
            unsigned decrementor = named_prefix_id ? named_prefix_id->NumUnconstrainedRanges() : 1 ;
            num_unconstrained -= decrementor ;
        }
        return (num_unconstrained > 0 ? 1 : 0) ;
    }
    return 0 ; // constrained type
}
unsigned VhdlAttributeName::IsUnconstrained(unsigned all_level) const
{
    switch (_attr_enum) {
    case VHDL_ATT_element :
        // VIPER #7809 : Check whether the prefix is unconstrained instead of '_result_type'
        // type my_type is array (natural range <>) of std_logic_vector(3 downto 0);
        // and we are checking my_type'element, _result_type is std_logic_vector which is unconstrained
        // but we need to check whether element of my_type is unconstrained or not
        return _prefix ? _prefix->HasUnconstrainedElement() : 0 ;
    case VHDL_ATT_subtype :
        // VIPER #7809 : Check whether the prefix is unconstrained instead of '_result_type'
        return _prefix ? _prefix->IsUnconstrained(all_level): 0 ;
    default : break ;
    }
    // Check whether result type is unconstrained or not :
    return (_result_type) ? (_result_type->IsUnconstrainedArrayType() || (all_level && _result_type->IsUnconstrained())) : 0 ;
}
unsigned VhdlExternalName::IsUnconstrained(unsigned all_level) const
{
    // Check resolved identifier's constraint
    return (_unique_id) ? (_unique_id->IsUnconstrainedArrayType() || (all_level && _unique_id->IsUnconstrained())): 0 ;
}
// Viper 7505: Number of unconstrained ranges in the name.
unsigned VhdlIdRef::NumUnconstrainedRanges() const
{
    return (_single_id ? _single_id->NumUnconstrainedRanges() : 0 ) ;
}
unsigned VhdlSelectedName::NumUnconstrainedRanges() const
{
    // Check resolved identifier's constraint
    return (_unique_id) ? (_unique_id->NumUnconstrainedRanges()): 0 ;
}
unsigned VhdlIndexedName::NumUnconstrainedRanges() const
{
    unsigned num_unconstrained = _prefix ? _prefix->NumUnconstrainedRanges() : 0 ;

    if (num_unconstrained) {
        // Prefix is unconstrained array type. Check whether constraint is specified
        // by range
        VhdlDiscreteRange *range ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_assoc_list, i, range) {
            if (!range || range->IsOpen()) continue ;
            // If any dimension is unconstrained, return 1
            if (range->IsUnconstrained(0)) continue ;
            num_unconstrained-- ;
        }
        return (num_unconstrained > 0 ? num_unconstrained : 0) ;
    }
    return 0 ; // constrained type
}
unsigned VhdlAttributeName::NumUnconstrainedRanges() const
{
    // Check whether result type is unconstrained or not :
    return (_result_type ? _result_type->NumUnconstrainedRanges() : 0) ;
}
unsigned VhdlExternalName::NumUnconstrainedRanges() const
{
    // Check resolved identifier's constraint
    return (_unique_id) ? (_unique_id->NumUnconstrainedRanges()): 0 ;
}
////////////// Inverse Position setting in physical unit id ///////////
void VhdlPhysicalLiteral::SetInversePosition(VhdlIdDef *unit_decl_id)
{
    if (!unit_decl_id) return ;

    VhdlIdDef *unit_id = (_unit) ? _unit->FindSingleObject(): 0 ;
    if (!unit_id) return ;
    verific_int64 literal = _value ? _value->EvaluateConstantInteger() : 1 ;

    verific_int64 inv_pos = literal * (verific_int64)unit_decl_id->InversePosition() ;
    unit_id->SetInversePosition((verific_uint64)inv_pos) ;
}

/**********************************************************/
// SubprogramAspect returns the subprogram (function or procedure)
// name from uninstantiated name of subprogram instantiation
/**********************************************************/
VhdlIdDef *VhdlName::SubprogramAspect(VhdlIdDef * /*formal*/)
{
    Error("illegal unit name") ;
    return 0 ;
}

VhdlIdDef *VhdlDesignator::SubprogramAspect(VhdlIdDef * /*formal*/)
{
    VhdlIdDef *result = FindSingleObject() ;
    // VIPER #3970: If identifier is alias id, return target identifier.
    if (_single_id && _single_id->IsAlias()) result = _single_id->GetTargetId() ;
    return result ;
}

VhdlIdDef *VhdlSelectedName::SubprogramAspect(VhdlIdDef *formal)
{
     Set ids(POINTER_HASH) ;
    (void) FindObjects(ids, 0) ;
    // VIPER #7814 : Add null check
    if (formal) formal->Prune(&ids) ;

    VhdlIdDef *result = ids.Size()==1 ? (VhdlIdDef*)ids.GetLast() : 0 ;
    _unique_id = result ;
    return result ;
}
VhdlIdDef *VhdlExternalName::SubprogramAspect(VhdlIdDef *formal)
{
     Set ids(POINTER_HASH) ;
    (void) FindObjects(ids, 0) ;
    // VIPER #7814 : Add null check
    if (formal) formal->Prune(&ids) ;

    VhdlIdDef *result = ids.Size()==1 ? (VhdlIdDef*)ids.GetLast() : 0 ;
    _unique_id = result ;
    return result ;
}
VhdlIdDef *VhdlSignaturedName::SubprogramAspect(VhdlIdDef * /*formal*/)
{
    if (_unique_id) return _unique_id ;

    (void) TypeInfer(0, 0, 0, 0) ;
    return _unique_id ;
}

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
// VIPER #7343 : Get dimension from subtype
VhdlDiscreteRange *VhdlIdRef::GetDimensionAt(unsigned dimension) const
{
    if (!_single_id || !_single_id->IsType()) return 0 ; // Not type identifier

    // Get subtype indication of this type
    VhdlSubtypeIndication *subtype = _single_id->GetSubtypeIndication() ;
    if (subtype) return subtype->GetDimensionAt(dimension) ;
    // For type identifier, subtype indication is not available.
    // Get full type declaration from type id to get dimension
    VhdlDeclaration *type_decl = _single_id->GetDeclaration() ;
    return type_decl ? type_decl->GetDimensionAt(dimension) : 0 ;
}

VhdlDiscreteRange *VhdlSelectedName::GetDimensionAt(unsigned dimension) const
{
    if (!_unique_id || !_unique_id->IsType()) return 0 ; // Not type identifier
    // Get subtype indication of this type
    VhdlSubtypeIndication *subtype = _unique_id->GetSubtypeIndication() ;
    return subtype ? subtype->GetDimensionAt(dimension): 0 ;
}
VhdlDiscreteRange *VhdlIndexedName::GetDimensionAt(unsigned dimension) const
{
    // First accumulate all dimensions mentioned here
    // Expression here can be A1_unconstrained (open)(3 to 5)( open)
    // So accumulate dimensions from prefix also
    Array dims(2) ;
    unsigned i ;
    VhdlDiscreteRange *index_range ;
    const VhdlName *prefix = this ;
    const VhdlName *prev_prefix = prefix ;
    while (prefix) {
        Array *assoc_list = prefix->GetAssocList() ;
        FOREACH_ARRAY_ITEM_BACK(assoc_list, i, index_range) {
            if (index_range) dims.InsertLast(index_range) ;
        }
        prev_prefix = prefix ;
        prefix = prefix->GetPrefix() ; // Go to prefix
    }
    unsigned idx = 0 ;
    FOREACH_ARRAY_ITEM_BACK(&dims, i, index_range) { // Traverse the dimensions
        if (!index_range) continue ;
        if (dimension == idx) { // Required dimension
            if (index_range->IsOpen()) {// open, bounds are specified in type declaration
                return prev_prefix ? prev_prefix->GetDimensionAt(idx): 0 ; // Get dimension from type
            } else {
                return index_range ; // Not open, return the range
            }
        }
        idx++ ; // Keep track of dimension
    }
    return _prefix ? _prefix->GetDimensionAt(dimension): 0 ;
}

#endif // VHDL_ID_SUBTYPE_BACKPOINTER
unsigned VhdlIdDef::IsVlType() const
{
    const char *name = Name() ;
    if (Strings::compare(name, "vl_logic") ||
        Strings::compare(name, "vl_logic_vector") ||
        Strings::compare(name, "vl_ulogic") ||
        Strings::compare(name, "vl_ulogic_vector")) {
        return 1 ;
    }
    return 0 ;
}
// VIPER #7717 : Error generation for mixed language designs from port association
// Formal is vl_logic/vl_logic_vector and actual is string-literal or character-literal
unsigned VhdlDesignator::ErrorForVllogicExpectedType(const VhdlIdDef *expected_type, const char *name)
{
    // We will produce special error message when expected_type is from converted
    // verilog module
    if (!expected_type || !expected_type->IsVlType()) return 0 ;

    if (IsCharacterLiteral() || (IsStringLiteral() && name)) { // For particular character
        // Find visible operators
        Set r(POINTER_HASH,11) ;
        (void) _present_scope->FindAll(name, r) ;
        if (r.Size() > 1) {
            Error("near character %s ; %d visible types match here", _name, "multiple") ;
            SetIter si ;
            unsigned first = 0 ;
            VhdlIdDef *id, *id_type ;
            FOREACH_SET_ITEM_BACK(&r, si, &id) {
                id_type = id ? id->BaseType(): 0 ;
                // Ignore visible vl types
                if (!id || !id_type || id_type->IsVlType()) continue ;
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
            return 1 ;
        } else {
            VhdlIdDef *id = r.Size() ? (VhdlIdDef*)r.GetAt(0): 0 ;
            VhdlIdDef *id_type = id ? id->BaseType(): 0 ;
            Error("character %s of type %s is not expected here", name, id_type ? id_type->Name(): "unknown") ;
            return 1 ;
        }
        return 0 ;
    }
    unsigned is_bit_string = IsBitStringLiteral() ;
    Set visible_operators(POINTER_HASH,11) ;
    SetIter si ;
    VhdlIdDef *oper ;
    VhdlIdDef *oper_type, *elem_type = 0 ;

    Set done_scopes(POINTER_HASH, 11) ; // hash table to avoid exponential explosion in FindCommaOperators.
    if (_present_scope) _present_scope->FindCommaOperators(visible_operators, done_scopes) ;

    // Now rule-out the , operators (aggregate operators) that do not match
    // Prune down the list, knowing that we need a 1-dimensional array
    FOREACH_SET_ITEM(&visible_operators, si, &oper) {
        oper_type = oper->Type() ;

        // Check that this is an array or dimension 1
        if (!oper_type || oper_type->Dimension()!=1) {
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        if (is_bit_string && IsVhdl87()) {
            // Check element type (must be 'BIT')
            elem_type = oper->ArgType(0) ;
            if (!elem_type || !elem_type->TypeMatch(StdType("bit"))) {
                (void) visible_operators.Remove(oper) ;
                continue ;
            }
        } else { // string literal
            // Check if this has a enumeration type element
            elem_type = oper_type->ElementType() ;
            if (!elem_type || !elem_type->IsEnumerationType()) {
                (void) visible_operators.Remove(oper) ;
                continue ;
            }
        }
    }
    if (visible_operators.Size() > 1) {
        char *type_names = 0 ;
        FOREACH_SET_ITEM(&visible_operators, si, &oper) {
            oper_type = oper ? oper->Type(): 0 ;
            // Ignore visible vl types
            if (!oper_type || oper_type->IsVlType()) continue ;
            if (!type_names) {
                type_names = Strings::save(oper_type->Name()) ;
            } else {
                char *tmp = type_names ;
                type_names = Strings::save(type_names, " or ", oper_type->Name()) ;
                Strings::free(tmp) ;
            }
        }
        Error("ambiguous type in %s, %s", is_bit_string ? "bit string literal": "string literal", type_names) ;
        Strings::free(type_names) ;
    } else {
        Error("type mismatch in %s", is_bit_string ? "bit string literal": "string literal") ;
    }
    return 1 ;
}

// VIPER #7890: Check for external name reference on VhdlName and on its derived classes:
unsigned VhdlSelectedName::IsExternalNameRef() const    { return (_prefix) ? _prefix->IsExternalNameRef() : 0 ; }
unsigned VhdlIndexedName::IsExternalNameRef() const     { return (_prefix) ? _prefix->IsExternalNameRef() : 0 ; }
unsigned VhdlSignaturedName::IsExternalNameRef() const  { return (_name)   ? _name->IsExternalNameRef()   : 0 ; }
unsigned VhdlAttributeName::IsExternalNameRef() const   { return (_prefix) ? _prefix->IsExternalNameRef() : 0 ; }

