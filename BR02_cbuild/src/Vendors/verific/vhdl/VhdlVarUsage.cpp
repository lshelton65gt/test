/*
 *
 * [ File Version : 1.73 - 2014/03/19 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlVarUsage.h"

#include "RuntimeFlags.h"

#include "Array.h"              // Make dynamic array class Array available
#include "BitArray.h"
#include "Set.h"
#include "Strings.h"

#include "VhdlUnits.h"          // Definitions of all vhdl units
#include "VhdlDeclaration.h"    // Definitions of all vhdl declarations
#include "VhdlStatement.h"      // Definitions of all vhdl statements
#include "VhdlIdDef.h"          // Definitions of all vhdl identifiers
#include "VhdlName.h"           // Definitions of all vhdl names
#include "VhdlSpecification.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "vhdl_tokens.h"        // for VHDL_in, VHDL_out, ...
#include "vhdl_file.h"          // for ExtractRams

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// This "enumerated type" represents the state of the control flow as it is being traversed.
// VALUE_IGNORED is the return type from top-level concurrent statements which is always ignored
#define NONE 0
#define DEAD 1
#define ALIVE 2
#define VALUE_IGNORED 3

// This "enumerated type" represents the current state of the expression tree
// as it is being traversed.
// (For safety, don't overlap values from the above enum type)
#define USING 4
#define ASSIGNING 5
#define PARTIAL_ASSIGNING 6
#define PARTIAL_USING 7

// Local utility functions

static int
IsInputMode(unsigned inp) {
    return ((inp == 0) ||
            (inp == VHDL_in) ||
            (inp == VHDL_inout) ||
            (inp == VHDL_linkage)) ;
}

static int
IsOutputMode(unsigned outp) {
    return ((outp == VHDL_out) ||
            (outp == VHDL_inout) ||
            (outp == VHDL_buffer) ||
            (outp == VHDL_linkage)) ;
}

static void CheckId(VhdlVarUsageInfo* vui, VhdlIdDef* id, unsigned action) ;

////////////////////////////////////////////////////////////////////
//        Methods for VhdlVarUsage
////////////////////////////////////////////////////////////////////

void
VhdlEntityDecl::VarUsageStartingPoint() const
{
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    unsigned i ;
    VhdlInterfaceDecl *id_decls ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, id_decls) {
        if (!id_decls) continue ;
        id_decls->VarUsage(0, NONE) ;
    }

    FOREACH_ARRAY_ITEM(_port_clause, i, id_decls) {
        if (!id_decls) continue ;
        id_decls->VarUsage(0, NONE) ;
    }

    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (elem) elem->VarUsage(0, NONE) ;
    }
    _present_scope = old_scope ;
}

void
VhdlArchitectureBody::VarUsageStartingPoint() const
{
    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // traverse all the concurrent statements
    unsigned i;
    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (elem) elem->VarUsage(0, NONE) ;
    }

    VhdlStatement* stmt;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) (void) stmt->VarUsage(0, NONE) ;
    }

    _present_scope = old_scope ;
}

/************************ VhdlIdDef objects ***********************/

void
VhdlIdDef::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // do nothing, all appropriate subclasses have defined their own VarUsage() method.
}

void
VhdlInterfaceId::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // add this variable to the table and update it's status
    CheckId(info, this, action) ;
}

//-----------------------------------------------------------------
// iterator schemes
//-----------------------------------------------------------------

void
VhdlIterScheme::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // do nothing, all appropriate subclasses have defined their own VarUsage() method.
}

/*---------------------------------------------*/

void
VhdlWhileScheme::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse condition
    if (_condition) _condition->VarUsage(info, USING) ;
}

/*---------------------------------------------*/

void
VhdlForScheme::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse id
    if (_id) _id->VarUsage(info, ASSIGNING) ;

    // Traverse range
    if (_range) _range->VarUsage(info, USING) ;
}

/*---------------------------------------------*/

void
VhdlIfScheme::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse condition
    if (_condition) _condition->VarUsage(info, USING) ;
}

/*---------------------------------------------*/

void
VhdlCaseItemScheme::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse the condition
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice) choice->VarUsage(info, USING) ;
    }
}

//-----------------------------------------------------------------
// Declarations
//-----------------------------------------------------------------

void
VhdlDeclaration::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // do nothing, all appropriate subclasses have defined their own VarUsage() method.
}

/*-----------------------------------------------------------------*/

void
VhdlVariableDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_init_assign) _init_assign->VarUsage(info, USING) ;
    if (_subtype_indication) _subtype_indication->VarUsage(info, USING) ;

    VhdlIdDef *id = (_id_list && _id_list->Size()) ? (VhdlIdDef *)_id_list->At(0) : (VhdlIdDef *)0 ;
    VhdlIdDef *type = id ? id->Type() : 0 ;

    Set type_recustion_set ;
    unsigned is_file_or_access = type && type->IsTypeContainsFile(1, type_recustion_set) ; // Viper #8328

    SynthesisInfo *this_info = 0 ;

    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;

        // Issue 2041 :
        // Also, check if this variable is a candidate for RAM inferencing.
        //  - Presently, since we only allow a single-index(address)range RAM in elaboration,
        //    with only one 'use' (filtered in analysis),
        //    we consequently only allow single-dimensional arrays.
        //  - Also, exclude array-of-bit as RAM. Just go for array-of-array.
        //    That's not a hard requirement, but it rules out massive amount of (small) RAM detection.
        //  - Also, NEVER infer a RAM for a Port (we need all the bits there).
        // Issue 2041 : Allow RAM extraction, even if there is an initial value assignment... Not sure if this is smart...
        if (type && type->Dimension()==1 &&
            type->ElementType() && type->ElementType()->Dimension()>=1) {
            // Further analysis of the usage of this signal (during analysis)
            // might discard it as a RAM. But for now, it's a candidate.

            // Do NOT create dual-port RAMs for a temporary dataflow like a subprogram :
            if (_present_scope && !_present_scope->GetSubprogram()) {
                id->SetCanBeDualPortRam(this_info) ;
            }
        }

        unsigned type_dims = type ? type->Dimension() : 0 ;
        VhdlIdDef *ele_type = type ? type->ElementType() : 0 ;
        unsigned m_dim_array = (type_dims > 1) ;
        unsigned array_of_array = (type_dims >= 1) && ele_type && (ele_type->Dimension()>=1) ;
        // Multi-port RAM : any array value will do.
        // Element type is now irrelevant : elaborator can work with any element type.
        // Restrict to array-of-array types for now, or else we will create massive amount of RAMs.
        if (type && (array_of_array || m_dim_array) // Viper #7152: for multi-dim array
            && !type->IsTypeContainsRecord() /* condition added for Viper #3751 */
            && !is_file_or_access /* condition added for Viper #8328 */)
        {
            // Do NOT create multi-port RAMs for a temporary dataflow like a subprogram :
            if (_present_scope && !_present_scope->GetSubprogram()) {
                id->SetCanBeMultiPortRam(this_info) ;
            }
        }
    }
}

/*-----------------------------------------------------------------*/
void
VhdlAttributeDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_type_mark) _type_mark->VarUsage(info, USING) ;
}

/*-----------------------------------------------------------------*/
void
VhdlAttributeSpec::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_value) _value->VarUsage(info, NONE) ;
}

/*-----------------------------------------------------------------*/
void
VhdlSubtypeDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_subtype_indication) _subtype_indication->VarUsage(info, USING) ;
}

/*-----------------------------------------------------------------*/
void
VhdlComponentDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;

    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (!decl) continue ;
        decl->VarUsage(info, NONE) ;
    }

    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (!decl) continue ;
        decl->VarUsage(info, NONE) ;
    }
}

/*-----------------------------------------------------------------*/
void
VhdlInterfaceDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_init_assign) _init_assign->VarUsage(info, USING) ;
    if (_subtype_indication) _subtype_indication->VarUsage(info, USING) ;

    VhdlIdDef *id = (_id_list && _id_list->Size()) ? (VhdlIdDef *)_id_list->At(0) : (VhdlIdDef *)0 ;
    VhdlIdDef *type = id ? id->Type() : 0 ;

    Set type_recustion_set ;
    unsigned is_file_or_access = type && type->IsTypeContainsFile(1, type_recustion_set) ; // Viper #8328

    SynthesisInfo *this_info = 0 ;

    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;

    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;

        if (!subprog) break ;
        if (id->IsInput() || !id->IsSignal()) continue ;

        unsigned type_dims = type ? type->Dimension() : 0 ;
        VhdlIdDef *ele_type = type ? type->ElementType() : 0 ;
        unsigned m_dim_array = (type_dims > 1) ;
        unsigned array_of_array = (type_dims >= 1) && ele_type && (ele_type->Dimension()>=1) ;
        // Multi-port RAM : any array value will do.
        // Element type is now irrelevant : elaborator can work with any element type.
        // Restrict to array-of-array types for now, or else we will create massive amount of RAMs.
        if (type && (array_of_array || m_dim_array) // Viper #7152: for multi-dim array
            && !type->IsTypeContainsRecord() /* condition added for Viper #3751 */
            && !is_file_or_access /* condition added for Viper #8328 */)
        {
            id->SetCanBeMultiPortRam(this_info) ;
        }
    }
}

/*-----------------------------------------------------------------*/

void
VhdlSignalDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_init_assign) _init_assign->VarUsage(info, USING) ;
    if (_subtype_indication) _subtype_indication->VarUsage(info, USING) ;

    VhdlIdDef *id = (_id_list && _id_list->Size()) ? (VhdlIdDef *)_id_list->At(0) : (VhdlIdDef *)0 ;
    VhdlIdDef *type = id ? id->Type() : 0 ;

    Set type_recustion_set ;
    unsigned is_file_or_access = type && type->IsTypeContainsFile(1, type_recustion_set) ; // Viper #8328

    SynthesisInfo *this_info = 0 ;

    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;

        if (type && type->Dimension()==1 &&
            type->ElementType() && type->ElementType()->Dimension()>=1) {
            // Further analysis of the usage of this signal (during analysis)
            // might discard it as a RAM. But for now, it's a candidate.
            id->SetCanBeDualPortRam(this_info) ;
        }

        unsigned type_dims = type ? type->Dimension() : 0 ;
        VhdlIdDef *ele_type = type ? type->ElementType() : 0 ;
        unsigned m_dim_array = (type_dims > 1) ;
        unsigned array_of_array = (type_dims >= 1) && ele_type && (ele_type->Dimension()>=1) ;
        // Multi-port RAM : any array value will do.
        // Element type is now irrelevant : elaborator can work with any element type.
        // Restrict to array-of-array types for now, or else we will create massive amount of RAMs.
        if (type && (array_of_array || m_dim_array) // Viper #7152: for multi-dim array
            && !type->IsTypeContainsRecord() /* condition added for Viper #3751 */
            && !is_file_or_access /* condition added for Viper #8328 */)
        {
            id->SetCanBeMultiPortRam(this_info) ;
        }
    }
}

/*-----------------------------------------------------------------*/

void
VhdlConstantDecl::VarUsage(VhdlVarUsageInfo* info, unsigned /*action*/)
{
    if (_init_assign) _init_assign->VarUsage(info, USING) ;
    if (_subtype_indication) _subtype_indication->VarUsage(info, USING) ;
}

/*-----------------------------------------------------------------*/

void
VhdlAliasDecl::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // If this is an object alias, associate the alias name with the ID being aliased

    if (!_designator) return ;

    if (!_alias_target_name) return ;

    VhdlIdDef *target_id = _alias_target_name->FindAliasTarget() ;
    if ((!target_id) || (!target_id->IsVariable()))  return ;

    // make sure the target_id is defined but don't treat
    // this as a use or assignement (i.e. don't update it's status)
    if (info) {
        (void) info->AddVariable(target_id) ;
        info->AddAliasVariable(target_id, _designator) ;
    }
}

/********************** VhdlSubprogramBody **********************/
// A VhdlSubprogramBody can occur in many different contexts.
// We would like to traverse all of the occurrences.
// Consequently, SubProgramBodies are always traversed starting from the
// constructor.  The normal "VarUsage" method is not defined so that
// subprograms are only traversed via this method

void
VhdlSubprogramBody::VarUsageStartingPoint() const
{
    if (!_subprogram_spec) return ;
    VhdlIdDef *subp_id = _subprogram_spec->GetId() ;
    if (!subp_id) return ;

    // We don't need to do this for pragma'ed functions.
    // Their body is typically empty any way.
    if (subp_id->GetPragmaFunction()) return;

    VhdlScope *old_scope = _present_scope ;
    _present_scope = LocalScope() ;

    SynthesisInfo *this_info = 0 ;

    // create the basic data used during traversal
    Map id_table(POINTER_HASH) ;
    VhdlVarUsageInfo info(&id_table) ;

    // Traverse the variable declarations -- vars
    // in a subprogram are always assigned
    unsigned i ;
    VhdlDeclaration *elem ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (elem) elem->VarUsage(&info, ASSIGNING) ;
    }

    // Traverse subprogram body
    unsigned action = USING;
    VhdlStatement* stmt;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) action = stmt->VarUsage(&info, action) ;
    }

    // After traversing the subprogram tree, if the current action is DEAD then
    // there are NO paths from beginning to end which do not involve a RETURN
    // statement. Mark the Subprogram ID appropriately since it is used as a return value.
    if (action == DEAD) {
        subp_id->SetAssignedBeforeUsed(this_info) ;
    } else {
        subp_id->UnSetAssignedBeforeUsed(this_info) ;
        subp_id->SetUsedBeforeAssigned(this_info) ;

        // code is not dead. If this is a function, then there might be a path without a return :
        if (subp_id->IsFunction()) {
            Warning("function %s does not always return a value", subp_id->Name()) ;
        }
    }

#if 0 // nothing needs to be done for interface-id
    // mark all outputs as having been read
    for(i = 0; i < subp_id->NumOfArgs() ; i++) {
        VhdlIdDef* id = subp_id->Arg(i) ;
        if (!id) continue ;

        // for inputs and inout mark the port connection in ASSIGNING mode
        // if (IsOutputMode(id->Mode())) id->VarUsage(&info, USING) ;
    }
#endif

    // update concurrent assignment info - this is necessary because we
    // can access externally defined signals and variables from inside the subprogram
    info.UpdateConcurrentVarStatus() ;

    _present_scope = old_scope ;
}

void
VhdlSubprogramBody::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    VarUsageStartingPoint() ;
}

//-----------------------------------------------------------------
// Expressions
//-----------------------------------------------------------------

void
VhdlDiscreteRange::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // do nothing, all appropriate subclasses have defined their own VarUsage() method.
    // All other subclasses we do not want to add an entry in the table (for efficiency)
}

/*-----------------------------------------------------------------*/

void
VhdlIdRef::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // all information is kept relative to the vhdlIdDef, so
    // convert the IdRef here.
    VhdlIdDef* id = GetSingleId() ;
    if (!id) return ;

    CheckId(info, id, action) ;

    SynthesisInfo *this_info = 0 ;

    // Eliminate this id (if it is directly referenced unindexed) as a multi-port ram :
    if ((action==USING) || (action==ASSIGNING)) {
        if (_single_id->CanBeMultiPortRam(this_info)) _single_id->SetCannotBeMultiPortRam(this_info) ;
        if (_single_id->CanBeDualPortRam(this_info)) _single_id->SetCannotBeDualPortRam(this_info) ;
    }
}

/*-----------------------------------------------------------------*/

void
VhdlArrayResFunction::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;
    if (_res_function) _res_function->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlRecordResFunction::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;
    if (_rec_res_function_list) {
        VhdlExpression *elem ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_rec_res_function_list,i,elem) {
            if (!elem) continue ;
            elem->VarUsage(info, action) ;
        }
    }
}

/*-----------------------------------------------------------------*/

void
VhdlAssocElement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse actual part
    if (_actual_part) _actual_part->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlInertialElement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse actual part
    if (_actual_part) _actual_part->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlRecResFunctionElement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // traverse record id name
    if (_record_id_name) _record_id_name->VarUsage(info, action) ;
    // Traverse the resolution function part
    if (_record_id_function) _record_id_function->VarUsage(info, action) ;
}
/*-----------------------------------------------------------------*/

void
VhdlOperator::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse lhs
    if (_left)  _left->VarUsage(info, action) ;

    // Traverse rhs
    if (_right) _right->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/
void
VhdlAllocator::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse subtype expression (after 'new') :
    if (_subtype) _subtype->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlQualifiedExpression::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse prefix
    if (_prefix)    _prefix->VarUsage(info, action) ;

    // Traverse aggregate
    if (_aggregate) _aggregate->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlElementAssoc::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse choices
    VhdlDiscreteRange *choice ;
    unsigned i;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice) choice->VarUsage(info, action) ;
    }

    // Traverse expression
    if (_expr) _expr->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlWaveformElement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    if (_value) _value->VarUsage(info, action) ;
    if (_after) _after->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlRange::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    if (_left)  _left->VarUsage(info, action) ;
    if (_right) _right->VarUsage(info, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlAttributeName::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // Viper #7712: Need varusage on the expression
    if (_expression) _expression->VarUsage(info, USING) ;

    // only traverse attributes which "read" their prefix
    switch(_attr_enum) {
        case VHDL_ATT_stable:
        case VHDL_ATT_quiet:
        case VHDL_ATT_delayed:
        case VHDL_ATT_transaction:
        case VHDL_ATT_event:
        case VHDL_ATT_active:
        case VHDL_ATT_last_event:
        case VHDL_ATT_last_active:
        case VHDL_ATT_last_value:
           {
                if (_prefix) _prefix->VarUsage(info, action) ;
                return ;
           }
        case 0: // user defined
           {
                VhdlIdDef *prefix_id = _prefix ? _prefix->GetId() : 0 ;
                if (prefix_id && prefix_id->IsAlias()) prefix_id = prefix_id->GetTargetId() ;

                VhdlIdDef *attr = _designator ? _designator->GetId() : 0 ;
                if (!attr || !prefix_id) break ;

                // Get the expression for this attribute on the prefix_id :
                VhdlExpression *attr_expr = prefix_id->GetAttribute(attr) ;
                if (attr_expr) attr_expr->VarUsage(info, action) ;
           }
           break ;
        case VHDL_ATT_right :
        case VHDL_ATT_left :
        case VHDL_ATT_high :
        case VHDL_ATT_low :
        case VHDL_ATT_reverse_range :
        case VHDL_ATT_range :
        case VHDL_ATT_length :
        case VHDL_ATT_base :
            {
                if (_prefix) _prefix->VarUsage(info, 0) ;
                return ;
            }
        default:
            ; // Do Nothing
    }
}

/*-----------------------------------------------------------------*/

void
VhdlAggregate::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    Set target_collision_checker(NUM_HASH) ;

    // Traverse element association list
    VhdlDiscreteRange* elem;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, elem) {
        if (elem) elem->VarUsage(info, action) ;
        if (elem && (action == ASSIGNING)) { // VIPER Issue #3650
            unsigned long element_signature = elem->CalculateSignature() ;
            if (target_collision_checker.GetItem((void *)element_signature)) Error("aggregate target name has already been specified") ;
            (void) target_collision_checker.Insert((void *)element_signature) ;
        }
    }
}

/*---------------------------------------------*/

void
VhdlSelectedName::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    // RD: Sometimes the suffix (IdRef) does not have its IdDef pointer set.
    // Howvere, '_unique_id' WILL be set if this selected name was disabiguated.
    // So, in general it's better to do a CheckId on the _unique_id field.
    // Do not scan both '_unique_id' and '_suffix', because then we will visit the id twice (once in the prefix and once in the suffix)
    // This is important for most selected names into fields of variables/signals.
    if (_unique_id) {
        // add this variable to the table and update it's status
        CheckId(info, _unique_id, action) ;
    }

    // Make sure that any assignment to the prefix of a selected name is marked  as 'partial' :
    // Set the 'action' for the prefix :
    unsigned prefix_action = action ;
    switch (action) {
    case ASSIGNING : prefix_action = PARTIAL_ASSIGNING ; break ;
    case USING : prefix_action = PARTIAL_USING ; break ;
    default : break ;
    }

    if (_prefix) _prefix->VarUsage(info, prefix_action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlIndexedName::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return ;

    SynthesisInfo *this_info = 0 ;

    // Make sure that the prefix ID is not marked as
    // assigned since it (probably) is partially assigned. But
    // if the prefix is an indexed name, we want to check the
    // index expression for variable use.
    // if this indexedName is a task or function call, we want to determine the
    // port I/O directions and use/assign the actuals as appropriate
    if (_is_function_call) {
        // for a subProgram call we must use the port directions
        // to determine the useage/assignment of the port actuals
        VhdlIdDef* subpId = _prefix_id ;

        if (!subpId) {
            if (_prefix) subpId = _prefix->FindSingleObject() ;
        }

        if (!subpId) {
            // something wrong with the design here -- don't crash
            return ;
        }

        //   determine usage/assignement of the actual from the direction of the formal
        unsigned j;
        VhdlDiscreteRange *actual ;
        FOREACH_ARRAY_ITEM(_assoc_list, j, actual) {
            VhdlIdDef *formal = 0;

            // Find the formal.
            if (actual && actual->IsAssoc()) {
                // named association
                VhdlName *formal_part = actual->FormalPart() ;
                formal = formal_part ? formal_part->FindFormal() : 0 ;
                const char *formal_name = formal_part ? formal_part->Name() : 0 ;

                if (!formal && formal_name && subpId->IsPredefinedOperator()) {
                    unsigned arg_idx = 0 ;
                    unsigned max_args = subpId->NumOfArgs() ;
                    for (arg_idx = 0; arg_idx < max_args; ++arg_idx) {
                        VhdlIdDef *oper_arg = subpId->Arg(arg_idx) ;
                        if (!oper_arg) continue ;

                        const char *arg_name = oper_arg->Name() ;
                        if (arg_name && Strings::compare_nocase(arg_name, formal_name)) {
                            formal = oper_arg ;
                            break ;
                        }
                    }
                }
                actual = actual->ActualPart() ;
            } else {
                // positional association
                formal = subpId->Arg(j) ;
            }

            actual = actual ? actual->FormalDesignator() : 0 ; // in case this is a type-conversion function
            if (!actual) continue ;

            if (!formal) {
                actual->VarUsage(0, 0) ;
                continue ; // type-inference checks failed
            }

            // Viper 7599: if the formal is good for ram and also the actual then
            // revert back the non ramibility marking on the actual done by VarUsage.

            // We honor ram marking iff the actual is a full formal and not a indexed name.

            VhdlIdDef *actual_id = actual->FindFullFormal() ;
            unsigned actual_can_be_multi_port_ram = actual_id && actual_id->CanBeMultiPortRam(this_info) ? 1 : 0 ;

            if (IsInputMode(formal->Mode()))  actual->VarUsage(info, USING) ;
            if (IsOutputMode(formal->Mode())) {
                actual->VarUsage(info, ASSIGNING) ;
                if (formal->IsUsed(this_info)) actual->MarkUsedFlag(this_info) ;
            }

            if (formal->CanBeMultiPortRam(this_info) && actual_can_be_multi_port_ram && actual_id) {
                actual_id->SetCanBeMultiPortRam(this_info) ;
            }
        }
    } else {
        // Set the 'action' for the prefix :
        unsigned prefix_action = action ;
        switch (action) {
        case ASSIGNING : prefix_action = PARTIAL_ASSIGNING ; break ;
        case USING : prefix_action = PARTIAL_USING ; break ;
        default : break ;
        }

        if (_prefix) _prefix->VarUsage(info, prefix_action) ;

        // VIPER 3549 : A directly sliced identfier cannot be modeled as a multi-port RAM.
        // That is for two reasons :
        //   (1) direct slicing of a multi-port RAM identifier is not yet supported in multi-port RAM elaboration (reader side)
        //   (2) if the slice constitutes a full range of the identifier (we don't know that here), then read/write ports would
        //       be created with 0 address bits and full-range data bits. That is certainly not intended to be a RAM of any sort.
        if (_is_array_slice && _prefix_id && _prefix_id->CanBeMultiPortRam(this_info)) {
            // Direct slice into an identifier. Discard as a (multi-port) RAM :
            _prefix_id->SetCannotBeMultiPortRam(this_info) ;
        }

        // VIPER 3603, VHDL edition :
        // Discard RAM if it is not clocked when assigned.
        if (_prefix_id && _prefix_id->CanBeMultiPortRam(this_info) &&
            (action == ASSIGNING || action==PARTIAL_ASSIGNING) &&
            (!info || !info->GetIsClocked())) {
            // ((!info || !info->GetIsClocked()) || (info && info->GetIsInForLoop()))) { // Viper #4008: No RAM for write in for loop
            _prefix_id->SetCannotBeMultiPortRam(this_info) ;
        }

        // If identifier is used as a sliced name, then it is not allowed as dual-port RAM :
        if (_prefix_id && _prefix_id->CanBeDualPortRam(this_info)) {
            if (_is_array_slice) {
                // Discard this id as a dual-port RAM if it is a array slice.
                _prefix_id->SetCannotBeDualPortRam(this_info) ;
            } else {
                // Discard this id if it's used inside a loop (issue 1568) :
                // (check if there is an exit label) :
                // We only need to do this here, since indexed name is the only
                // allowed reference of a array identifier used for RAMs.
                VhdlIdDef *label = _present_scope ? _present_scope->FindSingleObject(" exit_label") : 0 ;
                if (label) _prefix_id->SetCannotBeDualPortRam(this_info) ;
            }
        }

        // VIPER 2352 : Discard this name specified prefix id as a dual-port RAM if it
        // has multiple indexing myram(1)(2)i.e it is declared as array of array of array
        // (Check the depth of the prefix. If depth is greater than 1, set that prefix
        // id cannot be dual-port RAM)
        if (!_prefix_id) { // Prefix is not simple name
            VhdlName *prefix = _prefix ;
            unsigned depth = 0 ;
            VhdlIdDef *prefix_id = 0 ;
            while (prefix) { // Recurse through prefix to find prefix identifier and depth
                depth++ ;
                prefix_id = prefix->FindSingleObject() ;
                prefix = prefix->GetPrefix() ;
            }
            if (prefix_id && prefix_id->CanBeDualPortRam(this_info) && (depth > 1)) {
                // Depth is greater than 1, cannot be dual-port RAM
                prefix_id->SetCannotBeDualPortRam(this_info) ;
            }
        }

        // scan the indexes :
        VhdlDiscreteRange *item ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_assoc_list, i, item) {
            if (item) item->VarUsage(info, USING) ; // always using index expressions

            // VIPER 6697 : discard prefix identifier as RAM if an index is constant.
            if (_prefix_id && _prefix_id->CanBeMultiPortRam(this_info)) {
                // Get the index expression identifier (if any).
                VhdlIdDef *expr_id = item ? item->GetId() : 0 ;
                // Check for globally static expression, or loop iterator :
                if ((item && item->IsGloballyStatic()) || (expr_id && (expr_id->IsConstant()))) {
                    _prefix_id->SetCannotBeMultiPortRam(this_info) ;
                }
            }
        }
    }
}

//-----------------------------------------------------------------
// Statements
//-----------------------------------------------------------------

unsigned
VhdlStatement::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned action)
{
    // Do nothing, all appropriate subclasses have defined their own VarUsage() method.
    // All other subclasses we do not want to add an entry in the table (for efficiency)
    return action;
}

/*---------------------------------------------*/

unsigned
VhdlWaitStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;
    if (_condition_clause) _condition_clause->VarUsage(info, USING) ;
    if (_timeout_clause) _timeout_clause->VarUsage(info, USING) ;

    return action ;
}

/*---------------------------------------------*/

unsigned
VhdlIfStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // process the condition in the enclosing CurrStatus scope
    if (_if_cond) _if_cond->VarUsage(info, USING) ;

    // An array to store the assignment info for the THEN, ELSE, and ELSIF regions
    Array array_of_info(2 + ((_elsif_list) ? _elsif_list->Size() : 1)) ;

    // setup the assignment info for the THEN region
    // If statement is in a concurrent area, then assume that the sub statements are also concurrently executed
    VhdlVarUsageInfo* local_info = (info) ? info->Copy() : 0 ;

    // VIPER 3603 : Check if the 'if' condition contains an edge.
    // If so, then we make the 'then' area 'clocked'.
    unsigned is_clocked = 0 ;
    if (info && local_info && !info->GetIsClocked()) {
        if (_if_cond && _if_cond->FindIfEdge()) {
            local_info->SetIsClocked() ;
            is_clocked = 1 ;
        }
    }

    // VIPER 6502 : Check if this is a non-constant condition :
    unsigned is_non_constant_condition = 0 ;
    if (info && local_info && !info->GetIsInCondition() && !is_clocked) {
        if (_if_cond && !_if_cond->IsGloballyStatic()) {
            // non-constant if-condition
            is_non_constant_condition = 1 ; // for elsif/else flows
            local_info->SetIsInCondition() ;
        }
    }

    action = USING;
    unsigned i ;
    VhdlStatement *elem ;
    FOREACH_ARRAY_ITEM(_if_statements, i, elem) {
        if (elem) action = elem->VarUsage(local_info, action) ;
    }

    int all_dead = 1;
    if (action == DEAD) {
        // We used to ignore (delete) a dataflow branch if it ended in 'dead' code.
        // However, that is incorrect. See VIPER 3982. 'ConcurrentUsed' data would not be set.
        // So reconsider : A 'dead' branch will conservatively not return.
        // So, we could consider it to be a 'concurrent' action branch. Treat the same way as the end of a subprogram.
        // That should be correct for 'ever-used', 'ever-assigned' and 'fully-assigned' data.
        // So mimic 'end-of-dataflow' for this branch :
        if (local_info) local_info->UpdateConcurrentVarStatus() ;
        // now delete it
        delete local_info;
    } else {
        all_dead = 0;
        array_of_info.Insert(local_info) ;
    }

    VhdlElsif* elseif;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elseif) {
        if (!elseif) continue;

        // The elsif condition needs to be evaluated in the "initialVarStatus" context
        // and the body of the elsif is evaluated in a copy of "initialVarStatus"
        // Since there are no assignments allowed in the elsif condition, we can
        // safely use the same CurrStatus for both.
        // If statement is in a concurrent area, then assume that the sub statements are also concurrently executed
        local_info = (info) ? info->Copy() : 0 ;

        if (local_info && is_non_constant_condition) local_info->SetIsInCondition() ;

        // Run the 'elsif' statement :
        action = elseif->VarUsage(local_info, USING) ;

        if (action == DEAD) {
            // VIPER 3982 : mimic 'end-of-dataflow' for this branch :
            if (local_info) local_info->UpdateConcurrentVarStatus() ;
            delete local_info ;
        } else {
            array_of_info.Insert(local_info) ;
            all_dead = 0;
        }
    }

    // there always is an ELSE statement -- explicit or implicit.
    // setup the assignment info for the ELSE region. If there is
    // an implicit ELSE region, we still want to add an empty
    // is_fully_assigned array to the array_of_fully_assigned array
    // If statement is in a concurrent area, then assume that the sub statements are also concurrently executed
    local_info = (info) ? info->Copy() : 0 ;

    if (local_info && is_non_constant_condition) local_info->SetIsInCondition() ;

    action = USING;
    if (_else_statements) {
        FOREACH_ARRAY_ITEM(_else_statements, i, elem) {
            if (elem) action = elem->VarUsage(local_info, action) ;
        }
    }

    if (action == DEAD) {
        // VIPER 3982 : mimic 'end-of-dataflow' for this branch :
        if (local_info) local_info->UpdateConcurrentVarStatus() ;
        delete local_info;
    } else {
        array_of_info.Insert(local_info) ;
        all_dead = 0;
    }

    // Combine the VarStatus's from the arms of the IF into
    // an overall VarStatus (i.e. into InitialVarStatus).
    // The array can be empty if all the arms of the IF are DEAD.
    // In this case, we want the VarStatus (i.e. initialVarStatus)
    // at entry into the IF to be the final VarStatus
    if (array_of_info.Size() > 0) {
        if (info) info->Combine(array_of_info) ;

        // cleanup local data
        VhdlVarUsageInfo *v ;
        FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
            delete v ;
        }
    }
    return (all_dead) ? DEAD : ALIVE;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlElsif::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // Traverse condition
    if (_condition) _condition->VarUsage(info, USING) ;

    // VIPER 3603 : Check if the 'if' condition contains an edge.
    // If so, then we make the 'then' area 'clocked'.
    unsigned is_clocked = 0 ;
    if (info && !info->GetIsClocked()) {
        if (_condition && _condition->FindIfEdge()) {
            info->SetIsClocked() ;
            is_clocked = 1 ;
        }
    }

    // VIPER 6502 : check if this is a non-constant condition :
    if (info && !info->GetIsInCondition() && !is_clocked) {
        if (_condition && !_condition->IsGloballyStatic()) {
            // non-constant if-condition
            info->SetIsInCondition() ;
        }
    }

    // Traverse statements
    VhdlStatement* elem;
    unsigned i;
    FOREACH_ARRAY_ITEM(_statements, i, elem) {
        if (elem) action = elem->VarUsage(info, action) ;
    }

    return action;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlVariableAssignmentStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // test the RHS first in case the same variable is
    // on the RHS and the LHS
    if (_value)  _value->VarUsage(info, USING) ;

    if (_target) _target->VarUsage(info, ASSIGNING) ;

    return ALIVE;
}

/*---------------------------------------------*/

unsigned
VhdlSignalAssignmentStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // test the RHS first in case the same variable is on the RHS and the LHS
    unsigned i ;
    VhdlExpression *elem ;
    FOREACH_ARRAY_ITEM(_waveform, i, elem) {
        if (elem) elem->VarUsage(info, USING) ;
    }

    // The LHS needs to be processed to determine RAM status.
    // Any signal on the LHS is by definition UBA
    if (_target) _target->VarUsage(info, ASSIGNING) ;

    return ALIVE;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlCaseStatementAlternative::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // Traverse choices
    VhdlDiscreteRange* dr;
    unsigned i;
    FOREACH_ARRAY_ITEM(_choices, i, dr) {
        if (dr) dr->VarUsage(info, USING) ;
    }

    // Traverse statements
    VhdlStatement* elem;
    FOREACH_ARRAY_ITEM(_statements, i, elem) {
        if (elem) action = elem->VarUsage(info, action) ;
    }

    return action;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlCaseStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    if (_expr) _expr->VarUsage(info, USING) ;

    // VIPER 6502 : Check if this is a non-constant condition
    unsigned is_non_constant_condition = 0 ;
    if (info && !info->GetIsInCondition()) {
        if (_expr && !_expr->IsGloballyStatic()) {
            is_non_constant_condition = 1 ;
        }
    }

    // A place to store the assignment info for CASE item regions
    Array array_of_info((_alternatives) ? _alternatives->Size() : 1) ;

    // traverse the case item expressions and bodies.  Also calculate
    // the whether all paths through this case statement end in a return,
    // next, or exit. In VHDL the case statement is defined to always be
    // FULL -- thus we don't have to do a "fullness" check here.
    int all_caseitems_are_dead = 1;
    unsigned i ;
    VhdlCaseStatementAlternative *item ;
    FOREACH_ARRAY_ITEM(_alternatives, i, item) {
        if (!item) continue;

        // make sure the case choices are evaluated in the initial context
        // and that the body of the caseAlternative is evaluated in a
        // separate copy of the initial context.
        // Since there are no assignments allowed in the choices, we can
        // safely use the same context for both.
        // If statement is in a concurrent area, then assume that the sub statements are also concurrently executed
        VhdlVarUsageInfo* local_info = (info) ? info->Copy() : 0 ;

        if (local_info && is_non_constant_condition) local_info->SetIsInCondition() ;

        // Run the case item :
        action = item->VarUsage(local_info, USING) ;
        if (action == DEAD) {
            // VIPER 3982 : mimic 'end-of-dataflow' for this branch :
            if (local_info) local_info->UpdateConcurrentVarStatus() ;
            delete local_info ;
        } else {
            all_caseitems_are_dead = 0;
            array_of_info.Insert(local_info) ;
        }
    }

    // Combine the VarStatus's from the arms of the CASE into
    // an overall VarStatus (i.e. into InitialVarStatus).
    // The array can be empty if all the arms of the CASE are DEAD.
    // In this case, we want the VarStatus (i.e. initialVarStatus)
    // at entry into the IF to be the final VarStatus
    if (array_of_info.Size() > 0) {
        if (info) info->Combine(array_of_info) ;

        // cleanup local CurrStatusArray
        VhdlVarUsageInfo *v ;
        FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
            if (v) delete v ;
        }
    }

    // if all the case items ended in DEAD control flow, then the entire
    // case statement is DEAD
    return (all_caseitems_are_dead) ? DEAD : ALIVE;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlLoopStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // Traverse iter scheme
    if (_iter_scheme) _iter_scheme->VarUsage(info, action) ;

    // copy the assignment environnment
    // If statement is in a concurrent area, then assume that the sub statements are also concurrently executed
    VhdlVarUsageInfo* loop_info = (info) ? info->Copy() : 0 ;

    // Viper 4008 needs to know this information.. Since this is a new info
    // and info::Combine does not care of this flag, set on it directly
    if (loop_info && _iter_scheme && _iter_scheme->IsForScheme()) loop_info->SetIsInForLoop() ;

    // Ignore the existing assignment info since we don't know
    // if the assignments ever take place (this is conservative).
    // loop_info->ClearFullyAssignedInfo() ;
    // RD: 6/2005 : This is over-conservative. Identifiers which are already fully assigned will remain fully assigned.
    //              It is enough to just create a separate branch of var-assign info (loop_info)
    //              and ignore it after the loop. (delete it without merging back in).

    // Traverse statements
    VhdlStatement * node_item = 0;
    unsigned int i = 0;
    FOREACH_ARRAY_ITEM(_statements, i, node_item) {
        if (node_item) (void) node_item->VarUsage(loop_info, action) ;
    }

    // ignore assignments done inside the loop : this is conservative.
    // Cannot ignore the assignments in the loop :
    // SetConcurrentAssigned/SetConcurrentUsed would not be set.
    // Still, need to be careful that the loop could execute 0 times.

    // Now merge the dataflow back into the main flow,
    // In essence the two flows are mutually exclusive (like an if):
    // Either loop executes at least once, or not at all. So treat it as an if :
    if (info && loop_info) {
        Array array_of_info(2) ;
        array_of_info.Insert(info) ;
        array_of_info.Insert(loop_info) ;
        info->Combine(array_of_info) ;
    }

    delete loop_info ;
    _present_scope = old_scope ;

    return ALIVE;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlReturnStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    if (_expr) _expr->VarUsage(info, USING) ;

    // everything after the return (in the same DF region) is ignored
    // by changing the Action to DEAD
    return DEAD;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlExitStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    if (!_condition) {
        // everything after the EXIT (in the same DF region) is ignored
        // by changing the Action to DEAD
        return DEAD;
    } else {
        _condition->VarUsage(info, USING) ;
        // Conditional EXIT - be conservative and do not terminate the DF
    }
    return ALIVE;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlNextStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    if (!_condition) {
        // everything after the NEXT (in the same DF region) is ignored
        // by changing the Action to DEAD
        return DEAD;
    } else {
        _condition->VarUsage(info, USING) ;
        // Conditional NEXT - be conservative and do not terminate the DF
    }
    return ALIVE;
}

unsigned
VhdlAssertionStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // scan the 3 expressions in this statement.
    // Only do this if we should not ignore assertion statements :
    if (RuntimeFlags::GetVar("vhdl_ignore_assertion_statements")) return ALIVE ;

    if (_condition) _condition->VarUsage(info,USING) ;
    if (_report) _report->VarUsage(info,USING) ;
    if (_severity) _severity->VarUsage(info,USING) ;
    return ALIVE ;
}

unsigned
VhdlReportStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD;

    // scan the 2 expressions in this statement.
    // Only do this if we should not ignore assertion statements :
    if (RuntimeFlags::GetVar("vhdl_ignore_assertion_statements")) return ALIVE ;

    if (_report) _report->VarUsage(info,USING) ;
    if (_severity) _severity->VarUsage(info,USING) ;
    return ALIVE ;
}

//-----------------------------------------------------------------
//    Concurrent statements:
//-----------------------------------------------------------------

unsigned
VhdlProcessStatement::VarUsage(VhdlVarUsageInfo* /*id_table*/, unsigned /*action*/)
{
    // Basic storage
    Map id_table(POINTER_HASH) ;
    VhdlVarUsageInfo info(&id_table) ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    // VIPER 3603 : detect clocked areas.
    // If there is no sensitivity list, then there has to be a wait statement.
    // If there is a wait statement, then it has to specify a clock (or else it is not synthesizable).
    // So if there is no sensitivity list, then the entire process is clocked :
    if (!_sensitivity_list) {
        info.SetIsClocked() ; // indicate that the entire process is clocked.
    }

    // Do not scan sensitivity list itself : its signals are not explicitly 'used' if set on a sens list.

    // handle declaration statements here
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->VarUsage(&info, NONE) ;
    }

    // traverse the sequential statements
    unsigned action = USING;
    VhdlStatement* elem;
    FOREACH_ARRAY_ITEM(_statement_part, i, elem) {
        if (elem) action = elem->VarUsage(&info, action) ;
    }

    info.UpdateConcurrentVarStatus() ;

    _present_scope = old_scope ;
    return VALUE_IGNORED;
}

/*-----------------------------------------------------------------*/
// In this statement, variables cannot become ABU, so we ignore the
// "is_fully_assigned" info and only calculate the concurrent use/assignment
// info

unsigned
VhdlConditionalSignalAssignment::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // If this assignment thing is guarded, then the guard_id is now 'used' :
    if (_guard_id) CheckId(0, _guard_id, USING) ;

    // scan the waveforms :
    if (_conditional_waveforms && (_conditional_waveforms->Size() == 1)) {
        VhdlConditionalWaveform* cw = (VhdlConditionalWaveform*)_conditional_waveforms->GetFirst() ;
        if (cw) cw->VarUsage(0, USING) ;
    } else {
        // We have a complex, multi-armed statement.  We need to store useage info
        // for each arm, and then combine them to determine Concurrent var usages.

        // Basic storage
        Array array_of_info((_conditional_waveforms) ? _conditional_waveforms->Size() : 1) ;
        Map id_table(POINTER_HASH) ;
        VhdlVarUsageInfo master_info(&id_table) ;

        // Process the conditional waveforms.
        // Process the RHS first in case the same variable is
        // on both the RHS and the LHS
        int i;
        VhdlConditionalWaveform* cw;
        FOREACH_ARRAY_ITEM(_conditional_waveforms, i, cw) {
            if (!cw) continue;
            // Issue 2850 : the 'arms' of this conditional signal assignment ARE mutually exclusive, but they are NOT sequential.
            // So, the RTL elaborator cannot (currently does not) create 'dataflow's for the individual arms.
            // This means that any mutual exclusive use of an (RAM) identifier in multiple arms WILL create parallel uses (multiple READs) in the netlist.
            // That causes read-port conflicts for dual-port RAM inference.
            // This shows up in 2850, but also in a spectrum of other regression tests.
            // Decided to consider the arms of this conditional signal assignment to be plain concurrent usage of the expressions.
            // This will discard identifiers that appear in multiple arms (as dual-port RAMs).
            // FIX later in dual-port RAM inference elaboration (by supporting creating dataflows for the individual concurrent arms).
            // This problem is only there for dual-port RAM inference. NOT for multi-port RAM inference, nor for any normal logic
            unsigned infer_dual_ram = 0 ; // Viper #6212
            if (vhdl_file::ExtractRams()) infer_dual_ram = 1 ;

            if (infer_dual_ram) {
                cw->VarUsage(0, USING) ; // dual-port RAM inference : consider these arms concurrent and NOT mutual exclusive
            } else {
                // correct (and normal) analysis of mutual-exclusive arm 'cw'.
                VhdlVarUsageInfo* info_i = master_info.Copy() ;
                array_of_info.Insert(info_i) ;
                cw->VarUsage(info_i, USING) ;
            }
        }

        if (array_of_info.Size() > 0) {
            master_info.Combine(array_of_info) ;
            master_info.UpdateConcurrentVarStatus() ;

            // cleanup local data
            VhdlVarUsageInfo *v ;
            FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
                if (v) delete v ;
            }
        }
    }

    // Process the LHS
    if (_target) _target->VarUsage(0, ASSIGNING) ;

    return VALUE_IGNORED ;
}

unsigned
VhdlConditionalVariableAssignmentStatement::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // scan the waveforms :
    if (_conditional_expressions && (_conditional_expressions->Size() == 1)) {
        VhdlConditionalExpression* ce = (VhdlConditionalExpression*)_conditional_expressions->GetFirst() ;
        if (ce) ce->VarUsage(0, USING) ;
    } else {
        // We have a complex, multi-armed statement.  We need to store useage info
        // for each arm, and then combine them to determine Concurrent var usages.

        // Basic storage
        Array array_of_info((_conditional_expressions) ? _conditional_expressions->Size() : 1) ;
        Map id_table(POINTER_HASH) ;
        VhdlVarUsageInfo master_info(&id_table) ;

        // Process the conditional waveforms.
        // Process the RHS first in case the same variable is
        // on both the RHS and the LHS
        int i;
        VhdlConditionalExpression* ce;
        FOREACH_ARRAY_ITEM(_conditional_expressions, i, ce) {
            if (!ce) continue;
            // Issue 2850 : the 'arms' of this conditional signal assignment ARE mutually exclusive, but they are NOT sequential.
            // So, the RTL elaborator cannot (currently does not) create 'dataflow's for the individual arms.
            // This means that any mutual exclusive use of an (RAM) identifier in multiple arms WILL create parallel uses (multiple READs) in the netlist.
            // That causes read-port conflicts for dual-port RAM inference.
            // This shows up in 2850, but also in a spectrum of other regression tests.
            // Decided to consider the arms of this conditional signal assignment to be plain concurrent usage of the expressions.
            // This will discard identifiers that appear in multiple arms (as dual-port RAMs).
            // FIX later in dual-port RAM inference elaboration (by supporting creating dataflows for the individual concurrent arms).
            // This problem is only there for dual-port RAM inference. NOT for multi-port RAM inference, nor for any normal logic
            unsigned infer_dual_ram = 0 ; // Viper #6212
            if (vhdl_file::ExtractRams()) infer_dual_ram = 1 ;

            if (infer_dual_ram) {
                ce->VarUsage(0, USING) ; // dual-port RAM inference : consider these arms concurrent and NOT mutual exclusive
            } else {
                // correct (and normal) analysis of mutual-exclusive arm 'cw'.
                VhdlVarUsageInfo* info = master_info.Copy() ;
                array_of_info.Insert(info) ;
                ce->VarUsage(info, USING) ;
            }
        }

        if (array_of_info.Size() > 0) {
            master_info.Combine(array_of_info) ;
            master_info.UpdateConcurrentVarStatus() ;

            // cleanup local data
            VhdlVarUsageInfo *v ;
            FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
                if (v) delete v ;
            }
        }
    }

    // Process the LHS
    if (_target) _target->VarUsage(0, ASSIGNING) ;

    return VALUE_IGNORED ;
}

/*-----------------------------------------------------------------*/
// In this statement, variables cannot become ABU, so we ignore the
// "is_fully_assigned" info and only calculate the concurrent use/assignment
// info

unsigned
VhdlSelectedSignalAssignment::VarUsage(VhdlVarUsageInfo* /*id_table*/, unsigned /*action*/)
{
    // If this assignment thing is guarded, then the guard_id is now 'used' :
    if (_guard_id) CheckId(0, _guard_id, USING) ;

    // scan the waveforms :
    if (_selected_waveforms && (_selected_waveforms->Size() == 1)) {
        VhdlSelectedWaveform* sw = (VhdlSelectedWaveform*)_selected_waveforms->GetFirst() ;
        if (sw) sw->VarUsage(0, USING) ;
    } else {
        // We have a complex, multi-armed statement.  We need to store useage info
        // for each arm, and then combine them to determine Concurrent var usages.

        // Basic storage
        Array array_of_info((_selected_waveforms) ? _selected_waveforms->Size() : 1);
        Map id_table(POINTER_HASH) ;
        VhdlVarUsageInfo master_info(&id_table) ;

        // Process the selected waveforms.
        // Process the RHS first in case the same variable is
        // on both the RHS and the LHS
        int i;
        VhdlSelectedWaveform* sw;
        FOREACH_ARRAY_ITEM(_selected_waveforms, i, sw) {
            if (!sw) continue;

            // Issue 2850 : the 'arms' of this selected signal assignment ARE mutually exclusive, but they are NOT sequential.
            // So, the RTL elaborator cannot (currently does not) create 'dataflow's for the individual arms.
            // This means that any mutual exclusive use of an (RAM) identifier in multiple arms WILL create parallel uses (multiple READs) in the netlist.
            // That causes read-port conflicts for dual-port RAM inference.
            // This shows up in 2850, but also in a spectrum of other regression tests.
            // Decided to consider the arms of this conditional signal assignment to be plain concurrent usage of the expressions.
            // This will discard identifiers that appear in multiple arms (as dual-port RAMs).
            // FIX later in dual-port RAM inference elaboration (by supporting creating dataflows for the individual concurrent arms).
            // This problem is only there for dual-port RAM inference. NOT for multi-port RAM inference, nor for any normal logic
            unsigned infer_dual_ram = 0 ; // Viper #6212
            if (vhdl_file::ExtractRams()) infer_dual_ram = 1 ;

            if (infer_dual_ram) {
                sw->VarUsage(0, USING) ; // dual-port RAM inference : consider these arms concurrent and NOT mutual exclusive
            } else {
                // correct (and normal) analysis of mutual-exclusive arm 'cw'.
                VhdlVarUsageInfo* info = master_info.Copy() ;
                array_of_info.Insert(info) ;
                sw->VarUsage(info, USING) ;
            }
        }

        if (array_of_info.Size() > 0) {
            master_info.Combine(array_of_info) ;
            master_info.UpdateConcurrentVarStatus() ;

            // cleanup local data
            VhdlVarUsageInfo *v ;
            FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
                if (v) delete v ;
            }
        }
    }

    // Process the enabling expression
    if (_expr)   _expr->VarUsage(0, USING) ;

    // Process the LHS
    if (_target) _target->VarUsage(0, ASSIGNING) ;

    return VALUE_IGNORED ;
}

/*****************************************************************************/

unsigned
VhdlSelectedVariableAssignmentStatement::VarUsage(VhdlVarUsageInfo* /*id_table*/, unsigned /*action*/)
{
    // scan the waveforms :
    if (_selected_exprs && (_selected_exprs->Size() == 1)) {
        VhdlSelectedExpression* se = (VhdlSelectedExpression*)_selected_exprs->GetFirst() ;
        if (se) se->VarUsage(0, USING) ;
    } else {
        // We have a complex, multi-armed statement.  We need to store useage info
        // for each arm, and then combine them to determine Concurrent var usages.

        // Basic storage
        Array array_of_info((_selected_exprs) ? _selected_exprs->Size() : 1);
        Map id_table(POINTER_HASH) ;
        VhdlVarUsageInfo master_info(&id_table) ;

        // Process the selected waveforms.
        // Process the RHS first in case the same variable is
        // on both the RHS and the LHS
        int i;
        VhdlSelectedExpression* se;
        FOREACH_ARRAY_ITEM(_selected_exprs, i, se) {
            if (!se) continue;

            // Issue 2850 : the 'arms' of this selected signal assignment ARE mutually exclusive, but they are NOT sequential.
            // So, the RTL elaborator cannot (currently does not) create 'dataflow's for the individual arms.
            // This means that any mutual exclusive use of an (RAM) identifier in multiple arms WILL create parallel uses (multiple READs) in the netlist.
            // That causes read-port conflicts for dual-port RAM inference.
            // This shows up in 2850, but also in a spectrum of other regression tests.
            // Decided to consider the arms of this conditional signal assignment to be plain concurrent usage of the expressions.
            // This will discard identifiers that appear in multiple arms (as dual-port RAMs).
            // FIX later in dual-port RAM inference elaboration (by supporting creating dataflows for the individual concurrent arms).
            // This problem is only there for dual-port RAM inference. NOT for multi-port RAM inference, nor for any normal logic
            unsigned infer_dual_ram = 0 ; // Viper #6212
            if (vhdl_file::ExtractRams()) infer_dual_ram = 1 ;

            if (infer_dual_ram) {
                se->VarUsage(0, USING) ; // dual-port RAM inference : consider these arms concurrent and NOT mutual exclusive
            } else {
                // correct (and normal) analysis of mutual-exclusive arm 'cw'.
                VhdlVarUsageInfo* info = master_info.Copy() ;
                array_of_info.Insert(info) ;
                se->VarUsage(info, USING) ;
            }
        }

        if (array_of_info.Size() > 0) {
            master_info.Combine(array_of_info) ;
            master_info.UpdateConcurrentVarStatus() ;

            // cleanup local data
            VhdlVarUsageInfo *v ;
            FOREACH_ARRAY_ITEM(&array_of_info, i, v) {
                if (v) delete v ;
            }
        }
    }

    // Process the enabling expression
    if (_expr)   _expr->VarUsage(0, USING) ;

    // Process the LHS
    if (_target) _target->VarUsage(0, ASSIGNING) ;

    return VALUE_IGNORED ;
}
/*---------------------------------------------*/
// Traverse the block ports, then the concurrent statements

unsigned
VhdlBlockStatement::VarUsage(VhdlVarUsageInfo* /*id_table*/, unsigned /*action*/)
{
    // guard expression is 'used' :
    if (_guard) _guard->VarUsage(0, USING) ;

    // if there is a guard_id, flag it as assigned (by the guard expression) :
    if (_guard_id) CheckId(0, _guard_id, ASSIGNING) ;

    // Check the ports :
    if (_ports) (void) _ports->VarUsage(0, NONE) ;

    unsigned i;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) decl->VarUsage(0, NONE) ;
    }

    // traverse all the concurrent statements
    VhdlStatement* stmt;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt) (void) stmt->VarUsage(0, NONE) ;
    }

    return VALUE_IGNORED ;
}

/*-----------------------------------------------------------------*/
// Process the usage/assignment of the ports on the Block statement.
// Do not process the contents of the block. That will be done directly
// from the concurrent statements

unsigned
VhdlBlockPorts::VarUsage(VhdlVarUsageInfo* /*id_table*/, unsigned /*action*/)
{
    SynthesisInfo *this_info = 0 ;

    // Process the blockports:
    // First the port declaration itself :
    unsigned i ;
    VhdlInterfaceDecl *id_decls ;
    VhdlIdDef *port ;
    FOREACH_ARRAY_ITEM(_port_clause, i, id_decls) {
        if (!id_decls) continue ;

        Array *ports = id_decls->GetIds() ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(ports,j,port) {
            if (!port) continue ;
            if (port->IsInput()) {
                port->SetConcurrentAssigned(this_info) ;
            } else if (port->IsOutput()) {
                port->SetConcurrentUsed(this_info) ;
            } else {
                // assume bi-directional behavior.
                port->SetConcurrentUsed(this_info) ;
                port->SetConcurrentAssigned(this_info) ;
            }
        }
    }

    // Now the portmap aspects (check actuals)
    //   determine usage/assignment of the actual from the direction of the formal
    VhdlDiscreteRange *actual ;
    VhdlIdDef *formal ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, actual) {
        if (!actual) continue ;

        // Find the formal.
        if (actual->IsAssoc()) {
            // named association
            formal = actual->FindFormal() ;
        } else {
            // positional association
            formal = _block_label->GetPortAt(i) ;
        }
        if (!formal) continue ; // the formal cannot be found

        if (IsInputMode(formal->Mode()))  actual->VarUsage(0, USING) ;
        if (IsOutputMode(formal->Mode())) actual->VarUsage(0, ASSIGNING) ;
    }
    return ALIVE ;
}

/*-----------------------------------------------------------------*/
// Process the usage/assignment of the port actuals of the
// instantiation statement.

unsigned
VhdlComponentInstantiationStatement::VarUsage(VhdlVarUsageInfo* /*id_table*/, unsigned /*action*/)
{
    VhdlIdDef* unit = GetInstantiatedUnit() ;

    unsigned i;
    VhdlDiscreteRange *actual ;
    // Process the generics:
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, actual) {
        if (!actual) continue ;
        actual->VarUsage(0, USING) ;
    }

    // Process the ports:
    //   determine usage/assignement of the actual from the direction of the formal
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, actual) {
        VhdlIdDef *formal = 0;

        // Find the formal.
        if (actual && actual->IsAssoc()) {
            // named association
            formal = actual->FindFormal() ;
            actual = actual->ActualPart() ;
        } else {
            // positional association
            formal = (unit) ? unit->GetPortAt(i) : 0 ;
        }

        actual = actual ? actual->FormalDesignator() : 0 ; // in case this is a type-conversion function
        if (!actual) continue ;

        if (!formal) {
            actual->VarUsage(0, 0) ;
            continue ; // the formal cannot be found
        }

        if (IsInputMode(formal->Mode()))  actual->VarUsage(0, USING) ;
        if (IsOutputMode(formal->Mode())) actual->VarUsage(0, ASSIGNING) ;
    }

    return VALUE_IGNORED ;
}

/*---------------------------------------------*/

// For generate statements we don't know if the body is going to be executed even once.
// For the purposes of determining variable usage, the most conservative approach is
// to assume that the body IS going to be executed and therefor we traverse all the
// concurrent statements in the body.

unsigned
VhdlGenerateStatement::VarUsage(VhdlVarUsageInfo* /*info*/, unsigned /*action*/)
{
    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;

    Array *decl_part = _decl_part ;
    Array *statement_part = _statement_part ;

    if (_scheme) _scheme->VarUsage(0, USING) ;

    unsigned i;
    VhdlDeclaration* decl ;
    FOREACH_ARRAY_ITEM(decl_part, i, decl) {
        if (decl) decl->VarUsage(0, NONE) ;
    }

    // traverse all the concurrent statements
    VhdlStatement* stmt;
    FOREACH_ARRAY_ITEM(statement_part, i, stmt) {
        if (stmt) (void) stmt->VarUsage(0, NONE) ;
    }

    _present_scope = old_scope ;

    return VALUE_IGNORED ;
}

/*-----------------------------------------------------------------*/

unsigned
VhdlProcedureCallStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (action == DEAD) return DEAD ;
    if (!_call) return action ;

    // Special is that this call is an IndexedName, which is an expression.
    // So we need to pass in some explicit 'action'.
    // The 'Call' will be a IsFunctionCall(), which will ignore this action...
    // So it does not matter what to pass in.
    _call->VarUsage(info, NONE) ;

    return ALIVE;
}

/*-----------------------------------------------------------------*/
unsigned
VhdlIfElsifGenerateStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    (void) VhdlGenerateStatement::VarUsage(info, action) ;

    unsigned i ;
    VhdlGenerateStatement *elsif_stmt ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif_stmt) {
        if (elsif_stmt) (void) elsif_stmt->VarUsage(info, action) ;
    }
    if (_else_stmt) (void) _else_stmt->VarUsage(info, action) ;
    return VALUE_IGNORED ;
}

/*-----------------------------------------------------------------*/
unsigned
VhdlCaseGenerateStatement::VarUsage(VhdlVarUsageInfo* info, unsigned action)
{
    if (_expr) _expr->VarUsage(0, USING) ;

    unsigned i ;
    VhdlStatement *alternative ;
    FOREACH_ARRAY_ITEM(_alternatives, i, alternative) {
        if (alternative) (void) alternative->VarUsage(info, action) ;
    }
    return VALUE_IGNORED ;
}

//-----------------------------------------------------------------
// Misc Objects
//-----------------------------------------------------------------

void
VhdlConditionalWaveform::VarUsage(VhdlVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse waveforms
    VhdlExpression *wf ;
    unsigned i;
    FOREACH_ARRAY_ITEM(_waveform, i, wf) {
        if (wf) wf->VarUsage(info, action) ;
    }

    // Traverse when condition
    if (_when_condition) _when_condition->VarUsage(0, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlConditionalExpression::VarUsage(VhdlVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse expression
    if (_expr) _expr->VarUsage(info, action) ;

    // Traverse when condition
    if (_condition) _condition->VarUsage(0, action) ;
}

/*-----------------------------------------------------------------*/

void
VhdlSelectedWaveform::VarUsage(VhdlVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse waveforms
    VhdlExpression *wf ;
    unsigned i;
    FOREACH_ARRAY_ITEM(_waveform, i, wf) {
        if (wf) wf->VarUsage(info, action) ;
    }

    // Traverse when choices

    VhdlDiscreteRange* choice;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice) choice->VarUsage(0, action) ;
    }
}

/*-----------------------------------------------------------------*/

void
VhdlSelectedExpression::VarUsage(VhdlVarUsageInfo *info, unsigned action)
{
    if (action == DEAD) return ;

    // Traverse _expr
    if (_expr) _expr->VarUsage(info, action) ;

    // Traverse when choices

    VhdlDiscreteRange* choice;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice) choice->VarUsage(0, action) ;
    }
}

/*-----------------------------------------------------------------*/

void
VhdlSpecification::VarUsage(VhdlVarUsageInfo *info, unsigned /*action*/)
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;

    FOREACH_ARRAY_ITEM(GetGenericClause(), i, decl) {
        if (!decl) continue ;
        decl->VarUsage(info, NONE) ;
    }

    FOREACH_ARRAY_ITEM(GetFormalParamList(), i, decl) {
        if (!decl) continue ;
        decl->VarUsage(info, NONE) ;
    }
}

////////////////////////////////////////////////////////////////////
//      Utility Methods for VhdlVarUsageInfo
////////////////////////////////////////////////////////////////////

VhdlVarUsageInfo::~VhdlVarUsageInfo()
{
    // Do not delete "_indices" it is not owned by VhdlVarUsageInfo!
    _indices = 0 ;
    delete _is_fully_assigned ;
    delete _is_ever_assigned ;
    delete _is_ever_used ;
}

/*---------------------------------------------*/

VhdlVarUsageInfo*
VhdlVarUsageInfo::Copy()
{
    VhdlVarUsageInfo* new_vui = new VhdlVarUsageInfo() ;
    new_vui->_indices = _indices ;
    if (_is_fully_assigned) new_vui->_is_fully_assigned = new BitArray(*_is_fully_assigned) ;
    if (_is_ever_assigned)  new_vui->_is_ever_assigned  = new BitArray(*_is_ever_assigned) ;
    if (_is_ever_used)      new_vui->_is_ever_used      = new BitArray(*_is_ever_used) ;
    new_vui->_is_clocked = _is_clocked ; // copy 'is_clocked' flag
    new_vui->_is_in_loop = _is_in_loop ; // copy this flag
    new_vui->_is_in_condition = _is_in_condition ; // copy this flag
    return new_vui ;
}

/*---------------------------------------------*/

void
VhdlVarUsageInfo::ClearFullyAssignedInfo()
{
    if (!_is_fully_assigned) return;
    delete _is_fully_assigned ;
    _is_fully_assigned = 0 ;
}

/*---------------------------------------------*/

unsigned
VhdlVarUsageInfo::AddVariable(VhdlIdDef* id)
{
    if (!_indices) return 0 ;

    SynthesisInfo *this_info = 0 ;

    // Make sure the ID is in the ID table and it is
    // initialized correctly
    unsigned indx = (unsigned)(unsigned long)_indices->GetValue(id) ;
    if (!indx) {
        indx = _indices->Size() + 1 ;
        (void) _indices->Insert(id, (void*)(unsigned long)indx) ;
        if (id->IsUsedBeforeAssigned(this_info) == 0) {
            // if the variable has not been previously set as UBA,
            // begin following the assignment state of this variable
            // by setting it to ABU.
            id->SetAssignedBeforeUsed(this_info) ;
        }
    }
    return indx;
}

/*---------------------------------------------*/

void
VhdlVarUsageInfo::AddAliasVariable(const VhdlIdDef* old_id, const VhdlIdDef* new_id)
{
    VERIFIC_ASSERT(_indices) ;
    VERIFIC_ASSERT(!_indices->GetValue(new_id)) ;
    unsigned long indx = (unsigned long)_indices->GetValue(old_id) ;
    if (indx) (void) _indices->Insert(new_id, (void*)indx) ;
}

/*---------------------------------------------*/
// Check the use/assign status of the variable based on
// the current action and the previously calculated
// assignment status. This calculation combines
// the concurrent use/assignement info stored
// on the variable/signal with the isEverAssigned and
// isEverUsed info derived from traversing a
// concurrent statement

void
VhdlVarUsageInfo::UpdateConcurrentVarStatus() const
{
    VERIFIC_ASSERT(_indices) ;

    SynthesisInfo *this_info = 0 ;

    MapIter i ;
    VhdlIdDef *id ;
    unsigned long indx;
    FOREACH_MAP_ITEM(_indices, i, &id, &indx) {
        if (!id) continue;
        if (_is_ever_assigned && _is_ever_assigned->GetBit((unsigned)indx)) {
            if (id->IsConcurrentAssigned(this_info)) {
                id->SetCannotBeDualPortRam(this_info) ;
            } else {
                id->SetConcurrentAssigned(this_info) ;
            }
        }
        if (_is_ever_used && _is_ever_used->GetBit((unsigned)indx)) {
            if (id->IsConcurrentUsed(this_info)) {
                id->SetCannotBeDualPortRam(this_info) ;
            } else {
                id->SetConcurrentUsed(this_info) ;
            }
        }
    }
}

/*---------------------------------------------*/

void
VhdlVarUsageInfo::Combine(Array& array_of_info)
{
    Array fully_assigned(array_of_info.Size()) ;
    Array ever_assigned( array_of_info.Size()) ;
    Array ever_used(     array_of_info.Size()) ;

    unsigned i ;
    VhdlVarUsageInfo* info;
    FOREACH_ARRAY_ITEM(&array_of_info, i, info) {
        if (!info) continue ;
        fully_assigned.Insert(info->_is_fully_assigned) ;
        ever_assigned.Insert( info->_is_ever_assigned) ;
        ever_used.Insert(     info->_is_ever_used) ;
    }

    if (!_is_fully_assigned) _is_fully_assigned = new BitArray() ;
    if (!_is_ever_assigned)  _is_ever_assigned  = new BitArray() ;
    if (!_is_ever_used)      _is_ever_used      = new BitArray() ;

    _is_fully_assigned->ReduceAND(fully_assigned) ;
    _is_ever_assigned->ReduceOR(  ever_assigned) ;
    _is_ever_used->ReduceOR(      ever_used) ;
}

/*---------------------------------------------*/

void
VhdlVarUsageInfo::SetFullyAssigned(unsigned indx)
{
    if (!_is_fully_assigned) _is_fully_assigned = new BitArray() ;
    _is_fully_assigned->SetBit(indx) ;
}
void
VhdlVarUsageInfo::SetEverAssigned(unsigned indx)
{
    if (!_is_ever_assigned) _is_ever_assigned = new BitArray() ;
    _is_ever_assigned->SetBit(indx) ;
}
void
VhdlVarUsageInfo::SetEverUsed(unsigned indx)
{
    if (!_is_ever_used)  _is_ever_used = new BitArray() ;
    _is_ever_used->SetBit(indx) ;
}

/*---------------------------------------------*/

unsigned
VhdlVarUsageInfo::GetFullyAssigned(unsigned indx) const
{
    return (_is_fully_assigned) ? _is_fully_assigned->GetBit(indx) : 0 ;
}
unsigned
VhdlVarUsageInfo::GetEverAssigned(unsigned indx) const
{
    return (_is_ever_assigned) ? _is_ever_assigned->GetBit(indx) : 0 ;
}
unsigned
VhdlVarUsageInfo::GetEverUsed(unsigned indx) const
{
    return (_is_ever_used) ? _is_ever_used->GetBit(indx) : 0 ;
}

/*---------------------------------------------*/
// Check the use/assign status of the variable based on
// the current action and the previously calculated
// assignment status.  This calculation is done in the
// context of the VarUsageInfo.  Here, if the "info" field
// is NULL, then we assume we are inside a concurrent statement.

static void
CheckId(VhdlVarUsageInfo* info, VhdlIdDef* id, unsigned action)
{
    if (!id) return;

    SynthesisInfo *this_info = 0 ;

    unsigned indx = 0;
    if (info) indx = info->AddVariable(id) ;

    // calculate changes in variable status based on the
    // previous status and the current action
    switch(action) {
    case USING:
    case PARTIAL_USING:
        // In a concurrent area, using the ID causes it to be UBA
        // In a sequential area, if it has been previously not
        //   fully assigned, using the ID causes it to be UBA
        if (!info || !info->GetFullyAssigned(indx)) {
            id->SetUsedBeforeAssigned(this_info) ;
            id->UnSetAssignedBeforeUsed(this_info) ;
        }

        // two conflicting uses -- cannot be a RAM
        if (id->IsConcurrentUsed(this_info) || (info && info->GetEverUsed(indx))) id->SetCannotBeDualPortRam(this_info) ;

        // update usage info depending on context
        if (info) {
            info->SetEverUsed(indx) ;
        } else {
            id->SetConcurrentUsed(this_info) ;
        }
        break;
    case ASSIGNING:

        // any variable which is directly assigned cannot be a DPRam.
        // All accesses to a DP RAM must be indexed accesses
        id->SetCannotBeDualPortRam(this_info) ;

        if (info) {
            info->SetFullyAssigned(indx) ;
            info->SetEverAssigned(indx) ;
        } else {
            id->SetConcurrentAssigned(this_info) ;
        }
        break ;
    case PARTIAL_ASSIGNING:

        // two conflicting assignments -- cannot be a RAM
        if (id->IsConcurrentAssigned(this_info) || (info && info->GetEverAssigned(indx))) id->SetCannotBeDualPortRam(this_info) ;

        // update assignment info depending on context
        if (info) {
            info->SetEverAssigned(indx) ;
        } else {
            id->SetConcurrentAssigned(this_info) ;
        }
        break;
    default:
        // Should never reach here, so be conservative:
        id->SetCannotBeDualPortRam(this_info) ;
        id->SetUsedBeforeAssigned(this_info) ;
        id->UnSetAssignedBeforeUsed(this_info) ;

        // Don't insert assert here. We may reach here for some corner cases like
        // only selected name as concurrent procedure call.
        //VERIFIC_ASSERT(0) ;  // will kill the program in Debug mode only
    }
}
