/*
 *
 * [ File Version : 1.199 - 2014/03/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_NAME_H_
#define _VERIFIC_VHDL_NAME_H_

// Include headers for base classes
#include "VhdlExpression.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// Define classes used as elements (as pointers)
class Array ;
class Set ;
class Map ;
class VhdlName ;
class VhdlIdRef ;
class VhdlIdDef ;
class VhdlSignature ;
class VhdlDesignUnit ;
class VhdlScope ;

class SynthesisInfo ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Need for conversion of vhdl package to verilog package
class VeriScope ;
class VeriDataType ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6896: Fillup ranges:
class VeriConstraint ;
#endif

// For elaboration
class VhdlValue ;
class VhdlDataFlow ;
class VhdlMapForCopy ;

// Some base classes for names (and literals)

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlName : public VhdlSubtypeIndication
{
protected : // Abstract class
    VhdlName() ;

    // Copy tree node
    VhdlName(const VhdlName &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlName(SaveRestore &save_restore);

public:
    virtual ~VhdlName() ;

private:
    // Prevent compiler from defining the following
    VhdlName(const VhdlName &) ;            // Purposely leave unimplemented
    VhdlName& operator=(const VhdlName &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy name.
    // Any name can be copied by the following routines.
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;
    virtual VhdlSubtypeIndication * CopySubtypeIndication(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)                            { return 0 ; } // Deletes 'old_node', and puts new name in its place.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/)                { return 0 ; } // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildDesignator(VhdlDesignator * /*old_node*/, VhdlDesignator * /*new_node*/)          { return 0 ; } // Delete 'old_node', puts new suffix in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_node*/) { return 0 ; } // Deletes 'old_node', and puts new range in its place
    virtual unsigned            ReplaceChildSignature(VhdlSignature * /*old_node*/, VhdlSignature * /*new_node*/)             { return 0 ; } // Deletes 'old_node', and puts new signature in its place
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef * /*old_node*/, VhdlIdRef * /*new_node*/)                         { return 0 ; }          // Deletes 'old_node', and puts new identifier reference in its place
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)  { return 0 ; } // Insert new range before 'target_node'.
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange * /*target_node*/, VhdlDiscreteRange * /*new_node*/)   { return 0 ; } // Insert new range after 'target_node'.

    // Obtain the resolved identifier of this name. Only works after analysis (after type-inference (or FindObject) routine has run).
    virtual VhdlIdDef *         GetId() const { return 0 ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef * /*new_id*/) { } // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char * /*name*/) {} // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    // Given a (expanded) name, accumulate the named entities (VhdlIdDef's)
    // into set 'result'. Uniqueness is quaranteed by the Set.
    // Return 1 if successful. 0 if the name does not represent a valid (number of) objects.
    // These routines WILL use the global scope variable (VhdlScope::_present_scope) to resolve identifier references.
    virtual unsigned            FindObjects(Set &result, unsigned for_use_clause) ;
    virtual unsigned            FindObjectsLocal(VhdlScope *scope, Set &result) ;
    virtual VhdlIdDef *         FindSingleObject() ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)=0 ;
    virtual VhdlIdDef *         TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type) ;

    void                        CheckIdentifierUsage(VhdlIdDef *id, unsigned environment) const ; // check if this identifier (representing the name) is used correctly in this environment. Run when we type-infer a name.

    virtual VhdlExpression *    FormalDesignator() const ; // Return the formal designator (this looks past formal type-cast) in a formal part of an association. Use after type-inference.
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef * /*formal*/) ;

    virtual unsigned            IsRange() ;                // Can't make this 'const'
    virtual unsigned            IsAll() const ;
    virtual unsigned            IsOthers() const ;
    virtual unsigned            IsUnaffected() const ;
    virtual unsigned            IsOpen() const ;
    virtual unsigned            IsNull() const ; // check if expression is a NULL literal
    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)
    virtual unsigned            IsExpandedName() const ;   // Tells if this name matches LRM 6.3 definition of expanded name. Versus access value or record element. Uses _present_scope
    virtual unsigned            IsExternalName() const      { return 0 ; }
    virtual unsigned            GetClassType() const        { return 0 ; } // Class type of external name
    virtual VhdlSubtypeIndication *     GetSubtypeIndication() const { return 0 ; } // subtype indication for external name

    virtual unsigned            IsArraySlice() const        { return 0 ; }
    virtual unsigned            IsFunctionCall() const      { return 0 ; }
    virtual unsigned            IsTypeConversion() const    { return 0 ; } // Returns 1 if name is type conversion
    //virtual unsigned            IsStringLiteral() const     { return 0 ; }
    virtual unsigned            IsArrayResolutionFunction()  { return 0 ; }
    virtual unsigned            IsRecordResolutionFunction() { return 0 ; }

    // Check if the name denotes a signal
    virtual unsigned            IsSignal(void) ;

    virtual unsigned            IsAccessType() ;
    virtual unsigned            ContainsAccessType() ;
    virtual unsigned            IsProtectedType() ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;
    virtual unsigned            IsLocallyStaticAttPrefix() const ; // VIPER #6823
    virtual unsigned            IsGloballyStaticAttPrefix() const ; // VIPER #6823

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // From the formal part of association, find the formal
    virtual VhdlIdDef *         FindFullFormal() const ;
    virtual VhdlIdDef *         FindFormal() const ;
    virtual VhdlIdDef *         GetFormal() const ;     // From formal name of assoc, find the formal through prefix names of indexed names only
    virtual void                SetFormal(VhdlIdDef *formal) ;

    // From the designator name of an alias decl, find the id for the named target
    virtual VhdlIdDef *         FindAliasTarget() const ;

    virtual VhdlName *          GetPrefix() const       { return 0 ; } // Get prefix of VhdlSelectedName, VhdlIndexedName, VhdlAttributeName
    virtual VhdlDesignator *    GetSuffix() const       { return 0 ; } // Get suffix of VhdlSelectedName
    virtual Array *             GetAssocList() const    { return 0 ; } // Get indexes of VhdlIndexedName
    // virtual VhdlIdDef *         GetUniqueId() const     { return 0 ; } // Get unique id of VhdlSelectedName, VhdlSignaturedName. Replaced by GetId().
    virtual unsigned            GetAttributeEnum() const  { return 0 ; }
    virtual VhdlIdDef *         GetPrefixType() const     { return 0 ; }
    virtual VhdlName *          GetResolutionFunction()   { return 0 ; }
    virtual VhdlIdDef *         ElementTypeDeep(VhdlIdDef *prefix_type) { return prefix_type ; }

    // For configurations
    // Entity aspect : name denotes an entity, configuration or architecture
    virtual VhdlIdDef *         EntityAspect() ;                 // Return the interface unit (entity/component) of a entity aspect or instantiated unit
    virtual const char *        ArchitectureNameAspect() const ; // Return the architecture name in a entity aspect or instantiated unit.

    virtual unsigned            GetEntityClass() const  { return 0 ; }

    // Block configurations : find the (architecture or block label)
    virtual VhdlIdDef *         BlockSpec() ;
    virtual VhdlIdDef *         BlockSpecPrefix() ;

    virtual VhdlIdDef *         TypeInferRecordResFunc(VhdlIdDef * /*record_type*/) ;

    virtual Array *             GetResFunctionList() { return 0 ; }

    // Get signature of an entity name list (attribute specification)
    virtual VhdlSignature *     GetSignature() const { return 0; }

    // Return type_mark
    virtual VhdlIdDef *         TypeMark() ;

    // Elaboration
    // Add a name to the sensitivity list
    virtual void                AddToSensList(Set &s) ;

    // Evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)=0 ;
    VhdlValue *                 EvaluateId(VhdlIdDef *id, VhdlDataFlow *df, unsigned force_driver_value) const ;

    // Assign to a target
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ;}
#endif

    // Create constraint from a discrete range
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ; // Build a constraint from this name : range constraint / index constraint
    virtual VhdlConstraint *    EvaluatePrefixConstraint(VhdlDataFlow * /*df*/)  { return 0 ; }// Evaluate constraint of prefix

    // Range in a block-index-specification (if any)
    virtual VhdlConstraint *    BlockIndexSpec() ;
    virtual VhdlConstraint *    ElementConstraintDeep(VhdlConstraint *prefix_constraint) { return prefix_constraint ; }
    virtual VhdlIdDef *         FindExternalObject(unsigned path_token, unsigned accent_count) ;
    // Internal routine :
    virtual VhdlIdDef *         GetElabSpecificUniqueId() ;

protected:
} ; // class VhdlName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlDesignator : public VhdlName
{
protected: // Abstract class
    explicit VhdlDesignator(char *name) ;

    // Copy tree node
    VhdlDesignator(const VhdlDesignator &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDesignator(SaveRestore &save_restore);

public:
    virtual ~VhdlDesignator() ;

private:
    // Prevent compiler from defining the following
    VhdlDesignator() ;                                  // Purposely leave unimplemented
    VhdlDesignator(const VhdlDesignator &) ;            // Purposely leave unimplemented
    VhdlDesignator& operator=(const VhdlDesignator &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    // Designator and its derived class objects can be copied
    // by the following routines
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         GetId() const { return _single_id ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef *new_id) ; // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char *name) ; // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual unsigned            FindObjects(Set &result, unsigned for_use_clause) ;
    virtual VhdlIdDef *         FindSingleObject() ;
    virtual VhdlIdDef *         GetSingleId() const     { return _single_id ; } // replaced by GetId().

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)=0 ;
    virtual VhdlIdDef *         TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type);
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef * /*formal*/) ;

    virtual unsigned            IsAll() const ;
    virtual unsigned            IsOthers() const ;
    virtual unsigned            IsUnaffected() const ;
    virtual unsigned            IsOpen() const ;
    virtual unsigned            IsNull() const ; // check if expression is a NULL literal
    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)

    virtual unsigned            IsUsed() const ; // Viper #5700

    // Check if the name denotes a signal
    virtual unsigned            IsSignal(void) ;

    virtual unsigned            IsAccessType() ;
    virtual unsigned            ContainsAccessType() ;
    virtual unsigned            IsProtectedType() ;

    virtual const char *        Name() const =0 ;        // Name as this designator is declared. Return 0 if not a real designator (if its a indexed/selected name or so)

    virtual VhdlIdDef *         FindFullFormal() const ; // From formal name of assoc, find the formal (if it's one single id)
    virtual VhdlIdDef *         FindFormal() const ;     // From formal name of assoc, find the formal through prefix names
    virtual VhdlIdDef *         GetFormal() const ;     // From formal name of assoc, find the formal through prefix names of indexed names only
    virtual void                SetFormal(VhdlIdDef *formal) ;
    virtual VhdlIdDef *         FindAliasTarget() const ;

    /* Configurations */
    virtual VhdlIdDef *         BlockSpec() ;
    virtual VhdlIdDef *         BlockSpecPrefix() ;
    virtual VhdlIdDef *         EntityAspect() ;

    // Elaboration
    virtual void                AddToSensList(Set &s) ;
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)=0 ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif
    virtual VhdlIdDef *         FindExternalObject(unsigned path_token, unsigned accent_count) ;
    // Internal routine :
    virtual VhdlIdDef *         GetElabSpecificUniqueId() ;

    // Internal routine : VIPER #7717
    unsigned                     ErrorForVllogicExpectedType(const VhdlIdDef *expected_type, const char *name) ;
protected:
// Leaf node. Identifier reference.
    char        *_name ;      // String representation of the name
// back pointer set at analysis time (not owned)
    VhdlIdDef   *_single_id ; // (resolved) identifier definition to which this designator refers.
} ; // class VhdlDesignator

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlLiteral : public VhdlName
{
protected: // Abstract class
    VhdlLiteral() ;

    // Copy tree node
    VhdlLiteral(const VhdlLiteral &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlLiteral(SaveRestore &save_restore);

public:
    virtual ~VhdlLiteral() ;

private:
    // Prevent compiler from defining the following
    VhdlLiteral(const VhdlLiteral &) ;            // Purposely leave unimplemented
    VhdlLiteral& operator=(const VhdlLiteral &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy literals.
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)             { return 0 ; } // Delete 'old_node', puts new name in its place.

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)=0 ;

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)=0 ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlLiteral

/* -------------------------------------------------------------- */

/************************ Literals/Names **************************/

class VFC_DLL_PORT VhdlAll : public VhdlDesignator
{
public:
    VhdlAll() ;

    // Copy tree node
    VhdlAll(const VhdlAll &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAll(SaveRestore &save_restore);

    virtual ~VhdlAll() ;

private:
    // Prevent compiler from defining the following
    VhdlAll(const VhdlAll &) ;            // Purposely leave unimplemented
    VhdlAll& operator=(const VhdlAll &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLALL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual const char *        Name() const ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            IsAll() const ;

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlAll

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlOthers : public VhdlDesignator
{
public:
    VhdlOthers() ;

    // Copy tree node
    VhdlOthers(const VhdlOthers &node, VhdlMapForCopy &old2new) ;

     // Persistence (Binary restore via SaveRestore class)
    explicit VhdlOthers(SaveRestore &save_restore);

    virtual ~VhdlOthers() ;

private:
    // Prevent compiler from defining the following
    VhdlOthers(const VhdlOthers &) ;            // Purposely leave unimplemented
    VhdlOthers& operator=(const VhdlOthers &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLOTHERS; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

     // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual const char *        Name() const ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            IsOthers() const ;

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlOthers

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlUnaffected : public VhdlDesignator
{
public:
    VhdlUnaffected() ;

    // Copy tree node
    VhdlUnaffected(const VhdlUnaffected &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlUnaffected(SaveRestore &save_restore);

    virtual ~VhdlUnaffected() ;

private:
    // Prevent compiler from defining the following
    VhdlUnaffected(const VhdlUnaffected &) ;            // Purposely leave unimplemented
    VhdlUnaffected& operator=(const VhdlUnaffected &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLUNAFFECTED; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual const char *        Name() const ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            IsUnaffected() const ;

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlUnaffected

/* -------------------------------------------------------------- */

/****************************** Real literals *************************************/

class VFC_DLL_PORT VhdlInteger : public VhdlLiteral
{
public:
    explicit VhdlInteger(verific_int64 value) ;

    // Copy tree node
    VhdlInteger(const VhdlInteger &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInteger(SaveRestore &save_restore);

    virtual ~VhdlInteger() ;

private:
    // Prevent compiler from defining the following
    VhdlInteger() ;                               // Purposely leave unimplemented
    VhdlInteger(const VhdlInteger &) ;            // Purposely leave unimplemented
    VhdlInteger& operator=(const VhdlInteger &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLINTEGER; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    virtual unsigned            IsInteger()             { return 1 ; }

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            CannotMatchType(VhdlIdDef* type_id) const ; // Quick check to see if expression cannot match this type. Viper 1493(4400)

    virtual verific_int64       EvaluateConstantInteger() ;  // For attribute dimension ; before elaboration

    // Accessor method
    verific_int64               GetValue() const        { return _value ; } // The same as EvaluateConstantInteger()

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint *target_constraint) ;
    virtual VhdlConstraint *    EvaluateIntegerRangeConstraint(VhdlDataFlow *df) ; // Viper #6473
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
    verific_int64 _value ;
} ; // class VhdlInteger

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlReal : public VhdlLiteral
{
public:
    explicit VhdlReal(double value) ;

    // Copy tree node
    VhdlReal(const VhdlReal &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlReal(SaveRestore &save_restore);

    virtual ~VhdlReal() ;

private:
    // Prevent compiler from defining the following
    VhdlReal() ;                            // Purposely leave unimplemented
    VhdlReal(const VhdlReal &) ;            // Purposely leave unimplemented
    VhdlReal& operator=(const VhdlReal &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLREAL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            CannotMatchType(VhdlIdDef* type_id) const ; // Quick check to see if expression cannot match this type. Viper 1493(4400)

    // Accessor method
    double                      GetValue() const        { return _value ; }

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
    double _value ;
} ; // class VhdlReal

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlStringLiteral : public VhdlDesignator
{
public:
    explicit VhdlStringLiteral(char *string) ;

    // Copy tree node
    VhdlStringLiteral(const VhdlStringLiteral &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlStringLiteral(SaveRestore &save_restore);

    virtual ~VhdlStringLiteral() ;

private:
    // Prevent compiler from defining the following
    VhdlStringLiteral() ;                                     // Purposely leave unimplemented
    VhdlStringLiteral(const VhdlStringLiteral &) ;            // Purposely leave unimplemented
    VhdlStringLiteral& operator=(const VhdlStringLiteral &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLSTRINGLITERAL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual const char *        Name() const ;
    virtual char *              ExprString() const ; // Viper #7042
    virtual unsigned            IsStringLiteral() const     { return 1 ; }
    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            CannotMatchType(VhdlIdDef* type_id) const ; // Quick check to see if expression cannot match this type. Viper 1493(4400)

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual VhdlValue *         EvaluateForVerilog(VhdlConstraint *target_constraint, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/) ;
#endif

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlStringLiteral

/* -------------------------------------------------------------- */

/*
  BitStringLiteral is a special form of StringLiteral
*/
class VFC_DLL_PORT VhdlBitStringLiteral : public VhdlStringLiteral
{
public:
    explicit VhdlBitStringLiteral(char *string) ;

    // Copy tree node
    VhdlBitStringLiteral(const VhdlBitStringLiteral &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBitStringLiteral(SaveRestore &save_restore);

    virtual ~VhdlBitStringLiteral() ;

private:
    // Prevent compiler from defining the following
    VhdlBitStringLiteral() ;                                        // Purposely leave unimplemented
    VhdlBitStringLiteral(const VhdlBitStringLiteral &) ;            // Purposely leave unimplemented
    VhdlBitStringLiteral& operator=(const VhdlBitStringLiteral &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLBITSTRINGLITERAL; } // Defined in VhdlClassIds.h

    virtual unsigned            IsBitStringLiteral() const  { return 1 ; }

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Routine to get back the raw based bit string literal
    const char *                GetBasedBitString() const   { return _based_string ; }

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    // Elaboration
    // For elaboration : evaluate an expression
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
    char *_based_string ;
} ; // class VhdlBitStringLiteral

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlCharacterLiteral : public VhdlDesignator
{
public:
    explicit VhdlCharacterLiteral(char *c) ;

    // Copy tree node
    VhdlCharacterLiteral(const VhdlCharacterLiteral &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlCharacterLiteral(SaveRestore &save_restore);

    virtual ~VhdlCharacterLiteral() ;

private:
    // Prevent compiler from defining the following
    VhdlCharacterLiteral() ;                                        // Purposely leave unimplemented
    VhdlCharacterLiteral(const VhdlCharacterLiteral &) ;            // Purposely leave unimplemented
    VhdlCharacterLiteral& operator=(const VhdlCharacterLiteral &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLCHARACTERLITERAL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;

    virtual const char *        Name() const ;
    virtual char *              ExprString() const ; // Viper #7042
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            CannotMatchType(VhdlIdDef* type_id) const ; // Quick check to see if expression cannot match this type. Viper 1493(4400)

    virtual unsigned            IsCharacterLiteral() const  { return 1 ; }
    virtual unsigned            IsConstant() const ; // check if expression is a constant literal (or generic)

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlCharacterLiteral

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPhysicalLiteral : public VhdlLiteral
{
public:
    VhdlPhysicalLiteral(VhdlExpression *value, VhdlName *unit) ;

    // Copy tree node
    VhdlPhysicalLiteral(const VhdlPhysicalLiteral &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPhysicalLiteral(SaveRestore &save_restore);

    virtual ~VhdlPhysicalLiteral() ;

private:
    // Prevent compiler from defining the following
    VhdlPhysicalLiteral() ;                                       // Purposely leave unimplemented
    VhdlPhysicalLiteral(const VhdlPhysicalLiteral &) ;            // Purposely leave unimplemented
    VhdlPhysicalLiteral& operator=(const VhdlPhysicalLiteral &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLPHYSICALLITERAL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new name in its place.

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            CannotMatchType(VhdlIdDef* type_id) const ; // Quick check to see if expression cannot match this type. Viper 1493(4400)

    virtual verific_int64       EvaluateConstantInteger() ;
    virtual void                SetInversePosition(VhdlIdDef *unit_decl_id) ;

    // Following API follow LRM sections 7.4.1 : type time is not locally static
    virtual unsigned            IsLocallyStatic() const ;

    // Accessor methods
    VhdlExpression *    GetValueExpr() const        { return _value ; }
    VhdlName *          GetUnit() const             { return _unit ; }

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned)
    VhdlExpression  *_value ;
    VhdlName        *_unit ;
} ; // class VhdlPhysicalLiteral

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlNull : public VhdlLiteral
{
public:
    VhdlNull() ;

    // Copy tree node
    VhdlNull(const VhdlNull &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlNull(SaveRestore &save_restore);

    virtual ~VhdlNull() ;

private:
    // Prevent compiler from defining the following
    VhdlNull(const VhdlNull &) ;            // Purposely leave unimplemented
    VhdlNull& operator=(const VhdlNull &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLNULL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    virtual unsigned            IsNull() const ; // Check if expression is a NULL literal

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlNull

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlIdRef : public VhdlDesignator
{
public :
    explicit VhdlIdRef(char *name) ;     // Basically, a name reference
    explicit VhdlIdRef(VhdlIdDef *id) ;  // Special constructor for if an id is already known

    // Copy tree node
    VhdlIdRef(const VhdlIdRef &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary save/restore via SaveRestore class)
    explicit VhdlIdRef(SaveRestore &save_restore);

    virtual ~VhdlIdRef() ;

private:
    // Prevent compiler from defining the following
    VhdlIdRef() ;                             // Purposely leave unimplemented
    VhdlIdRef(const VhdlIdRef &) ;            // Purposely leave unimplemented
    VhdlIdRef& operator=(const VhdlIdRef &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLIDREF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;
    virtual unsigned            IsLocallyStaticAttPrefix() const ; // VIPER #6823
    virtual unsigned            IsGloballyStaticAttPrefix() const ; // VIPER #6823

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // LRM : 1.1.1.2 and 4.3.2.2 Viper #3183
    // Actual must be static-name or globally-static-expression
    // Viper 7486: Exclude protected type and shared variables (which are
    // necessarily of protected type). Need to explicitly add this check
    // here, not inside Is(Locally/Globally)StaticName to comply with the
    // popular simulators
    virtual unsigned            IsGoodForPortActual() const ;

    virtual unsigned            IsUnconstrained(unsigned all_level) const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    // Copy parse-tree
    virtual VhdlDesignator *    CopyDesignator(VhdlMapForCopy &old2new) const ;
    // Non virtual copy routine.
    VhdlIdRef *                 Copy(VhdlMapForCopy &old2new) const ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ; // VIPER #7343
#endif // VHDL_ID_SUBTYPE_BACKPOINTER
    // Static Elaboration.
    virtual VhdlSubtypeIndication * CreateSubtypeWithConstraint(VhdlConstraint *constraint) ; // Create type mark from this name. Set range constraint in the created type mark with argument constraint.
    virtual VhdlName *              ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual const char *        Name() const ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    virtual verific_int64       EvaluateConstantInteger() ; // For attribute dimension ; before elaboration, If IdRef points to enumeration literal, then we return 'integer' value by its position in the type. If it is not IdRef of enum type, behavior of this API is same as VhdlExpression::EvaluateConstantInteger()
    virtual unsigned            IsRange() ; // Can't make this 'const'
    virtual void                MarkUsedFlag(SynthesisInfo *info) ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895:
    // Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
    //Convert vhdl range to verilog range: during analysis create unconstraint ranges
    virtual VeriRange *         ConvertRange(VeriScope *scope, Array *type_id_arr) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlValue *         EvaluateAllocatorSubtype(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/) ;
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
    virtual void                AssignPartial(VhdlValue *target_val, VhdlConstraint *target_constraint, Net *assign_condition, Array *name_select, VhdlValue *val, VhdlDataFlow *df) ;

    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
    virtual VhdlConstraint *    EvaluateIntegerRangeConstraint(VhdlDataFlow *df) ; // Viper #6473
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
    // Identifier reference. Leaf node.
    // char* name and resolved definition (VhdlIdDef*) are in base class VhdlDesignator already.
} ; // class VhdlIdRef

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlArrayResFunction : public VhdlName
{
public :
    explicit VhdlArrayResFunction(VhdlName *res_function) ;     // Basically, a name reference

    // Copy tree node
    VhdlArrayResFunction(const VhdlArrayResFunction &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary save/restore via SaveRestore class)
    explicit VhdlArrayResFunction(SaveRestore &save_restore);

    virtual ~VhdlArrayResFunction() ;

private:
    // Prevent compiler from defining the following
    VhdlArrayResFunction() ;                             // Purposely leave unimplemented
    VhdlArrayResFunction(const VhdlArrayResFunction &) ;            // Purposely leave unimplemented
    VhdlArrayResFunction& operator=(const VhdlArrayResFunction &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLARRAYRESFUNCTION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // virtual function to identify array resolution function
    virtual unsigned            IsArrayResolutionFunction() { return 1 ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;
    virtual unsigned            IsUnconstrained(unsigned all_level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    virtual unsigned            FindObjects(Set &result, unsigned for_use_clause) ;

    virtual VhdlIdDef *         FindSingleObject() ;

    // Accessor function
    virtual VhdlName *          GetResolutionFunction() { return _res_function ; }

    // Non virtual copy routine.
    VhdlArrayResFunction *      Copy(VhdlMapForCopy &old2new) const ;
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    virtual VhdlValue *         Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
    // reference to the nested function name.
    VhdlName* _res_function ;
} ; // class VhdlArrayResFunction

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlRecordResFunction : public VhdlName
{
public :
    explicit VhdlRecordResFunction(Array *rec_res_function_list) ;     // Basically, a name reference

    // Copy tree node
    VhdlRecordResFunction(const VhdlRecordResFunction &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary save/restore via SaveRestore class)
    explicit VhdlRecordResFunction(SaveRestore &save_restore);

    virtual ~VhdlRecordResFunction() ;

private:
    // Prevent compiler from defining the following
    VhdlRecordResFunction() ;                             // Purposely leave unimplemented
    VhdlRecordResFunction(const VhdlRecordResFunction &) ;            // Purposely leave unimplemented
    VhdlRecordResFunction& operator=(const VhdlRecordResFunction &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLRECORDRESFUNCTION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // virtual function to identify record resolution function
    virtual unsigned            IsRecordResolutionFunction() { return 1 ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Non virtual copy routine.
    VhdlRecordResFunction *     Copy(VhdlMapForCopy &old2new) const ;
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef * /*expected_type*/, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/) ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    virtual VhdlIdDef *         TypeInferRecordResFunc(VhdlIdDef * record_type) ;

    virtual Array *             GetResFunctionList() { return _rec_res_function_list ; }

    virtual VhdlValue *         Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
    // reference to the nested function name.
    Array *_rec_res_function_list ;
} ; // class VhdlRecordResFunction

/* -------------------------------------------------------------- */

// Other names
class VFC_DLL_PORT VhdlSelectedName : public VhdlName
{
public:
    VhdlSelectedName(VhdlName *prefix, VhdlDesignator *suffix) ;

    // Copy tree node
    VhdlSelectedName(const VhdlSelectedName &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSelectedName(SaveRestore &save_restore);

    virtual ~VhdlSelectedName() ;

private:
    // Prevent compiler from defining the following
    VhdlSelectedName() ;                                    // Purposely leave unimplemented
    VhdlSelectedName(const VhdlSelectedName &) ;            // Purposely leave unimplemented
    VhdlSelectedName& operator=(const VhdlSelectedName &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLSELECTEDNAME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;
    virtual unsigned            IsUnconstrained(unsigned all_level) const ;

    virtual unsigned            IsExternalNameRef() const ; // VIPER #7890: Test to see if this VhdlName refers to a VHDL 2008 external-name

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ; // VIPER #7343
#endif // VHDL_ID_SUBTYPE_BACKPOINTER

    // Copy selected name
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new prefix in its place.
    virtual unsigned            ReplaceChildDesignator(VhdlDesignator *old_node, VhdlDesignator *new_node) ; // Delete 'old_node', puts new suffix in its place.

    // Static Elaboration.
    virtual VhdlSubtypeIndication *CreateSubtypeWithConstraint(VhdlConstraint *constraint) ; // Create type mark with this name specifying range constraint by argument specific constraint.

    virtual VhdlIdDef *         GetId() const { return _unique_id ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef *new_id) ; // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char *name) ; // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual unsigned            FindObjects(Set &result, unsigned for_use_clause) ;
    virtual VhdlIdDef *         FindSingleObject() ;
    virtual unsigned            IsRange() ;                // Can't make this 'const'
    virtual unsigned            IsExpandedName() const ;   // Tells if this name matches LRM 6.3 definition of expanded name. Versus access value or record element. Uses _present_scope

    // Check if the name denotes a signal
    virtual unsigned            IsSignal(void) ;

    virtual unsigned            IsAccessType() ;
    virtual unsigned            ContainsAccessType() ;
    virtual unsigned            IsProtectedType() ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual VhdlIdDef *         TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type) ;
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef * formal) ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    virtual VhdlIdDef *         FindFormal() const ;
    virtual void                SetFormal(VhdlIdDef *formal) ;
    virtual VhdlIdDef *         FindAliasTarget() const ;

    // Configurations
    virtual VhdlIdDef *         EntityAspect() ;

    // Accessor methods
    virtual VhdlName *          GetPrefix() const       { return _prefix ; }
    virtual VhdlDesignator *    GetSuffix() const       { return _suffix ; }
    VhdlIdDef *                 GetUniqueId() const     { return _unique_id ; } // replaced by GetId().
    virtual void                MarkUsedFlag(SynthesisInfo *info) ;

    // Elaboration
    virtual void                AddToSensList(Set &s) ;
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;

    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif
    virtual VhdlIdDef *         FindExternalObject(unsigned path_token, unsigned accent_count) ;
    // Internal routine :
    virtual VhdlIdDef *         GetElabSpecificUniqueId() ;

protected:
// Parse tree nodes (owned)
    VhdlName        *_prefix ;
    VhdlDesignator  *_suffix ;
// back pointer set during analysis (not owned)
    VhdlIdDef       *_unique_id ; // (resolved) identifier definition to which this selected name refers. Set when (type) resolved
} ; // class VhdlSelectedName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlIndexedName : public VhdlName
{
public:
    VhdlIndexedName(VhdlName *prefix, Array *assoc_list) ;

    // Copy tree node
    VhdlIndexedName(const VhdlIndexedName &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIndexedName(SaveRestore &save_restore);

    virtual ~VhdlIndexedName() ;

private:
    // Prevent compiler from defining the following
    VhdlIndexedName() ;                                   // Purposely leave unimplemented
    VhdlIndexedName(const VhdlIndexedName &) ;            // Purposely leave unimplemented
    VhdlIndexedName& operator=(const VhdlIndexedName &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLINDEXEDNAME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            IsUnconstrained(unsigned all_level) const ;
    virtual unsigned            NumUnconstrainedRanges() const ;
    virtual unsigned            HasUnconstrainedElement() const ; // VIPER #7809

    virtual unsigned            IsExternalNameRef() const ; // VIPER #7890: Test to see if this VhdlName refers to a VHDL 2008 external-name

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    virtual VhdlDiscreteRange * GetDimensionAt(unsigned dimension) const ; // VIPER #7343
#endif // VHDL_ID_SUBTYPE_BACKPOINTER

    // Copy indexed name
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Deletes 'old_node', and puts new range in its place

    // Insert new range before/after 'target_node' in the
    // list of range. If 'target_node' is null or not
    // found in the assoc list, nothing is inserted.
    virtual unsigned            InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before 'target_node'.
    virtual unsigned            InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range after 'target_node'.

    // Static Elaboration.
    virtual VhdlSubtypeIndication *CreateSubtypeWithConstraint(VhdlConstraint *constraint) ; // Create type-mark with 'this' name. The range of type is created from the argument specific constraint.
    virtual void                   ModifyIndex(VhdlConstraint *&inst_unit_constraint, VhdlConstraint *&comp_constraint) ; // VIPER #3510, 3513 : Modify index of partial formal according to instantiated entity
    virtual VhdlName *             ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;
    virtual void                   ReplaceBoundsInSubtypeIndication(VhdlDataFlow *df) ;

    virtual VhdlIdDef *         GetId() const { return _prefix_id ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef *new_id) ; // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char *name) ; // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual VhdlIdDef *         TypeInferFormal(VhdlScope *local_scope, VhdlIdDef *expected_type) ;
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    virtual VhdlExpression *    FormalDesignator() const ; // Return the formal designator (this looks past formal type-cast) in a formal part of an association. Use after type-inference.
    virtual VhdlIdDef *         FindFormal() const ;               // from formal part of assoc, find the formal IdDef. After type-inference
    virtual VhdlIdDef *         GetFormal() const ;     // From formal name of assoc, find the formal through prefix names of indexed names only
    virtual void                SetFormal(VhdlIdDef *formal) ;
    virtual VhdlIdDef *         FindAliasTarget() const ;

    // Configuration Names
    virtual VhdlIdDef *         BlockSpec() ;
    virtual VhdlIdDef *         EntityAspect() ;
    virtual const char *        ArchitectureNameAspect() const ;  // return the architecture name in a entity aspect or instantiated unit.

    virtual VhdlIdDef *         TypeMark() ;

    // Accessor methods
    virtual VhdlIdDef *         GetNamedPrefixId() const ; // Recursively goes to _prefix to find _prefix_id
    virtual VhdlName *          GetPrefix() const           { return _prefix ; }
    virtual Array *             GetAssocList() const        { return _assoc_list ; }
    VhdlIdDef *                 GetPrefixId() const         { return _prefix_id ; }
    VhdlIdDef *                 GetPrefixTypeId() const     { return _prefix_type ; }
    unsigned                    IsArrayIndex() const        { return _is_array_index ; }
    virtual unsigned            IsArraySlice() const        { return _is_array_slice ; }
    virtual unsigned            IsFunctionCall() const      { return _is_function_call ; }
    virtual unsigned            IsTypeConversion() const    { return _is_type_conversion ; }
    unsigned                    IsIndexConstraint() const   { return _is_index_constraint ; }
    unsigned                    IsRecordConstraint() const  { return _is_record_constraint ; }
    virtual VhdlIdDef *         ElementTypeDeep(VhdlIdDef *prefix_type) ;
//CARBON_BEGIN
    void                        SetIsFunctionCall(void) {_is_function_call = 1; }
//CARBON_END

    // Check if the name denotes a signal
    virtual unsigned            IsSignal(void) ;

    virtual unsigned            IsAccessType() ;
    virtual unsigned            ContainsAccessType() ;
    virtual unsigned            IsProtectedType() ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;
    virtual unsigned            IsLocallyStaticAttPrefix() const ; // VIPER #6823
    virtual unsigned            IsGloballyStaticAttPrefix() const ; // VIPER #6823

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;

    // LRM : 1.1.1.2 and 4.3.2.2 Viper #3183
    // Actual must be static-name or globally-static-expression or
    // resolution function with static-name/static-expression single argument
    virtual unsigned            IsGoodForPortActual() const ;

    virtual unsigned            FindIfEdge() const ; // Find out if this condition expression is a 'clock' edge expression. Used for VIPER 3603, 6156.
    virtual void                MarkUsedFlag(SynthesisInfo *info) ;
    virtual VhdlIdDef *         GetRecordElementName() const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl datatype to verilog datatype
    virtual VeriDataType *      ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Fillup ranges:
    virtual void                FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map) ;
#endif

    // Elaboration
    virtual void                AddToSensList(Set &s) ;

    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
    virtual VhdlConstraint *    EvaluatePrefixConstraint(VhdlDataFlow *df) ; // Evaluate constraint of prefix

    virtual VhdlValue *         EvaluateAllocatorSubtype(VhdlConstraint * /*target_constraint*/, VhdlDataFlow *df) ;

    virtual VhdlConstraint *    BlockIndexSpec() ;

    static unsigned IsRamRead(VhdlName const *idx_name, VhdlIdDef *&prefix_id, VhdlName *&prefix_name, Array &assoc_list) ; // Viper #3925

    virtual VhdlConstraint *    ElementConstraintDeep(VhdlConstraint *prefix_constraint) ;
    virtual VhdlIdDef *         FindExternalObject(unsigned path_token, unsigned accent_count) ;
    // Internal routine :
    virtual VhdlIdDef *         GetElabSpecificUniqueId() ;

protected:
// Parse tree nodes (owned) :
    VhdlName    *_prefix ;
    Array       *_assoc_list ; // Array of VhdlDiscreteRange*

// back pointer set at analysis time (not owned)
    VhdlIdDef   *_prefix_id ;  // (resolved) identifier definition to which the prefix of this indexed name refers. Set when (type) resolved. Might be 0 for deep indexed names (with indexed prefixes).
// back pointer set at analysis time (not owned)
    VhdlIdDef   *_prefix_type ; // Viper 7283 store type of the prefix
// Type-inference will set one of these flags, to identify how this indexed name is used :
    unsigned     _is_array_index:1 ;
    unsigned     _is_array_slice:1 ;
    unsigned     _is_function_call:1 ;
    unsigned     _is_index_constraint:1 ;
    unsigned     _is_record_constraint:1 ; // Viper 6811: added a new flag for record constraint.
    unsigned     _is_type_conversion:1 ;
    unsigned     _is_group_template_ref:1 ; // VIPER #7447: Reference of group template in group decl
} ; // class VhdlIndexedName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSignaturedName : public VhdlName
{
public:
    VhdlSignaturedName(VhdlName *name, VhdlSignature *signature) ;

    // Copy tree node
    VhdlSignaturedName(const VhdlSignaturedName &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSignaturedName(SaveRestore &save_restore);

    virtual ~VhdlSignaturedName() ;

private:
    // Prevent compiler from defining the following
    VhdlSignaturedName() ;                                      // Purposely leave unimplemented
    VhdlSignaturedName(const VhdlSignaturedName &) ;            // Purposely leave unimplemented
    VhdlSignaturedName& operator=(const VhdlSignaturedName &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLSIGNATUREDNAME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy signatured name
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildSignature(VhdlSignature *old_node, VhdlSignature *new_node) ; // Deletes 'old_node', and puts new signature in its place

    virtual VhdlName *          ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual VhdlIdDef *         GetId() const { return _unique_id ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual unsigned            FindObjectsLocal(VhdlScope *scope, Set &result) ;
    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef * /*formal*/) ;

    virtual VhdlIdDef *         FindAliasTarget() const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    virtual const char *        Name() const ;  // This is different from GetName().  This method returns a char* representation of the signature.

    // Accessor methods
    VhdlName *                  GetName() const             { return _name ; }
    virtual VhdlSignature *     GetSignature() const        { return _signature ; }
    VhdlIdDef *                 GetUniqueId() const         { return _unique_id ; } // replaced by GetId()

    virtual VhdlName *          GetPrefix() const           { return (_name) ? _name->GetPrefix() : 0 ; } // Get prefix of VhdlSelectedName, VhdlIndexedName, VhdlAttributeName
    virtual VhdlDesignator *    GetSuffix() const           { return (_name) ? _name->GetSuffix() : 0 ; } // Get suffix of VhdlIndexedName

    virtual unsigned            IsExternalNameRef() const ; // VIPER #7890: Test to see if this VhdlName refers to a VHDL 2008 external-name

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlName        *_name ;
    VhdlSignature   *_signature ;
// back pointer set at analysis time (not owned)
    VhdlIdDef       *_unique_id ; // (resolved) identifier definition to which this signatured name refers
} ; // class VhdlSignaturedName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlAttributeName : public VhdlName
{
public:
    VhdlAttributeName(VhdlName *prefix, VhdlIdRef *designator, VhdlExpression *expression) ;

    // Copy tree node
    VhdlAttributeName(const VhdlAttributeName &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlAttributeName(SaveRestore &save_restore);

    virtual ~VhdlAttributeName() ;

private:
    // Prevent compiler from defining the following
    VhdlAttributeName() ;                                     // Purposely leave unimplemented
    VhdlAttributeName(const VhdlAttributeName &) ;            // Purposely leave unimplemented
    VhdlAttributeName& operator=(const VhdlAttributeName &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLATTRIBUTENAME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    // Check if attribute name denotes a predefined attribute
    static unsigned             IsPredefinedAttribute(const char *name) ;
    static unsigned             IsPredefinedAttributeWithOneArg(const char *name) ;

    // Check if the name denotes a signal
    virtual unsigned            IsSignal(void) ;

    virtual unsigned            IsExternalNameRef() const ; // VIPER #7890: Test to see if this VhdlName refers to a VHDL 2008 external-name

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // LRM section 6.1 does NOT say anything.. static_name'delayed etc
    // should be static name (mimicing other standard compiler behavior)
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;
    virtual unsigned            IsUnconstrained(unsigned all_level) const ;

    // Copy parse-tree.
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ;             // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node) ;          // Deletes 'old_node', and puts new identifier reference in its place
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Deletes 'old_node', and puts new expression in its place

    // Static Elaboration.
    virtual VhdlSubtypeIndication * CreateSubtypeWithConstraint(VhdlConstraint *constraint) ; // Create type mark with this name. Set range constraint in the type mark by argument specific constraint.
    virtual VhdlName *              ChangeOutOfScopeRefs(VhdlScope *ref_scope, VhdlScope *def_scope, VhdlScope *container_scope, Map *lib_ids) ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual unsigned            IsRange() ; // Can't make this 'const'

    // Accessor methods
    virtual VhdlName *          GetPrefix() const           { return _prefix ; }
    VhdlIdRef *                 GetDesignator() const       { return _designator ; }
    VhdlExpression *            GetExpression() const       { return _expression ; }
    virtual unsigned            GetAttributeEnum() const    { return _attr_enum ; }
    virtual VhdlIdDef *         GetPrefixType() const       { return _prefix_type ; }
    VhdlIdDef *                 GetResultType() const       { return _result_type ; }
    virtual unsigned            FindIfEdge() const ; // Find out if this condition expression is a 'clock' edge expression. Used for VIPER 3603, 6156.
    virtual unsigned            FindIfStable() const ; // Find out if this condition expression is a 'clock' edge expression. Used for VIPER 3603, 6156.

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl range to verilog range: during analysis create unconstraint ranges
    virtual VeriRange *         ConvertRange(VeriScope *scope, Array *type_id_arr) const ;
#endif

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif
    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ;
    virtual VhdlConstraint *    EvaluateConstraintPhysical(VhdlDataFlow *df) ; // For evaluating constraint of physical typedefs

protected:
// static table of predefined attribute names. Created once during analysis.
    static Map *_predef_table ;
    static void InitPredefAttrTable() ;
public:
    static void ResetPredefAttrTable() ;

protected:
// Parse tree nodes (owned) :
    VhdlName        *_prefix ;
    VhdlIdRef       *_designator ;
    VhdlExpression  *_expression ;
// set at analysis time
    unsigned         _attr_enum ;     // VHDL_ATT_length style token, for predefined attributes. 0 for user-defined attributes.
// (back) pointers set at analysis time (not owned) :
    VhdlIdDef       *_prefix_type ;   // (resolved) type of the prefix expression.
    VhdlIdDef       *_result_type ;   // (resolved) result type of the attribute name.
} ; // class VhdlAttributeName

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlOpen : public VhdlName
{
public:
    VhdlOpen() ;

    // Copy tree node
    VhdlOpen(const VhdlOpen &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlOpen(SaveRestore &save_restore);

    virtual ~VhdlOpen() ;

private:
    // Prevent compiler from defining the following
    VhdlOpen(const VhdlOpen &) ;            // Purposely leave unimplemented
    VhdlOpen& operator=(const VhdlOpen &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLOPEN; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    virtual unsigned            IsOpen() const ;

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;
    virtual VhdlIdDef *         EntityAspect() ;

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;

protected:
} ; // class VhdlOpen

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlEntityAspect : public VhdlName
{
public:
    VhdlEntityAspect(unsigned entity_class, VhdlName *name) ;

    // Copy tree node
    VhdlEntityAspect(const VhdlEntityAspect &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEntityAspect(SaveRestore &save_restore);

    virtual ~VhdlEntityAspect() ;

private:
    // Prevent compiler from defining the following
    VhdlEntityAspect() ;                                    // Purposely leave unimplemented
    VhdlEntityAspect(const VhdlEntityAspect &) ;            // Purposely leave unimplemented
    VhdlEntityAspect& operator=(const VhdlEntityAspect &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLENTITYASPECT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    // Dedicated instantiated unit pick-up, during analysis
    virtual VhdlIdDef *         EntityAspect() ;
    virtual const char *        ArchitectureNameAspect() const ; // return the architecture name in a entity aspect or instantiated unit.

    // Accessor methods
    virtual unsigned            GetEntityClass() const      { return _entity_class ; }
    VhdlName *                  GetName() const             { return _name ; }

    virtual VhdlName *          GetPrefix() const           { return (_name) ? _name->GetPrefix() : 0 ; } // Get prefix of VhdlSelectedName, VhdlIndexedName, VhdlAttributeName
    virtual VhdlDesignator *    GetSuffix() const           { return (_name) ? _name->GetSuffix() : 0 ; } // Get suffix of VhdlIndexedName

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    unsigned  _entity_class ; // VHDL_entity or VHDL_configuration token.
    VhdlName *_name ;
} ; // class VhdlEntityAspect

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlInstantiatedUnit : public VhdlName
{
public:
    VhdlInstantiatedUnit(unsigned entity_class, VhdlName *name) ;

    // Copy tree node
    VhdlInstantiatedUnit(const VhdlInstantiatedUnit &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInstantiatedUnit(SaveRestore &save_restore);

    virtual ~VhdlInstantiatedUnit() ;

private:
    // Prevent compiler from defining the following
    VhdlInstantiatedUnit() ;                                        // Purposely leave unimplemented
    VhdlInstantiatedUnit(const VhdlInstantiatedUnit &) ;            // Purposely leave unimplemented
    VhdlInstantiatedUnit& operator=(const VhdlInstantiatedUnit &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLINSTANTIATEDUNIT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place.

    virtual VhdlIdDef *         GetId() const { return (_name) ? _name->GetId() : 0 ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef *new_id) ; // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char *name) ; // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    // Dedicated instantiated unit pick-up, during analysis
    virtual VhdlIdDef *         EntityAspect() ;
    virtual const char *        ArchitectureNameAspect() const ; // return the architecture name in a entity aspect or instantiated unit.

    // Accessor methods
    virtual unsigned            GetEntityClass() const      { return _entity_class ; }
    VhdlName *                  GetName() const             { return _name ; }

    virtual VhdlName *          GetPrefix() const           { return (_name) ? _name->GetPrefix() : 0 ; } // Get prefix of VhdlSelectedName, VhdlIndexedName, VhdlAttributeName
    virtual VhdlDesignator *    GetSuffix() const           { return (_name) ? _name->GetSuffix() : 0 ; } // Get suffix of VhdlIndexedName

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    unsigned  _entity_class ; // VHDL_component, VHDL_entity or VHDL_configuration token, or 0.
    VhdlName *_name ;
} ; // class VhdlInstantiatedUnit

/* -------------------------------------------------------------- */
// VHDL-2008 specified external name
// << class external_path_name : subtype_indication >>
class VFC_DLL_PORT VhdlExternalName : public VhdlName
{
public:
    VhdlExternalName(unsigned class_type, unsigned path_token, unsigned hat_count, VhdlName *external_path_name, VhdlSubtypeIndication *subtype_indication) ;

    // Copy tree node
    VhdlExternalName(const VhdlExternalName &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlExternalName(SaveRestore &save_restore);

    virtual ~VhdlExternalName() ;

private:
    // Prevent compiler from defining the following
    VhdlExternalName() ;                                    // Purposely leave unimplemented
    VhdlExternalName(const VhdlExternalName &) ;            // Purposely leave unimplemented
    VhdlExternalName& operator=(const VhdlExternalName &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLEXTERNALNAME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Following two APIs follow LRM sections 7.4.1 and 7.4.2
    virtual unsigned            IsLocallyStatic() const ;
    virtual unsigned            IsGloballyStatic() const ;

    // Following two APIs follow LRM section 6.1
    virtual unsigned            IsLocallyStaticName() const ;
    virtual unsigned            IsGloballyStaticName() const ;
    virtual unsigned            IsUnconstrained(unsigned all_level) const ;

    virtual unsigned            IsGoodForPortActual() const ;

    // Calculate the conformance signature of this expression
    virtual unsigned long       CalculateSignature() const ;
    virtual unsigned            NumUnconstrainedRanges() const ;

    // Copy selected name
    virtual VhdlName *          CopyName(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new prefix in its place.
    virtual unsigned            ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node) ; // Delete 'old_node', puts new subtype indication in its place.

    virtual VhdlIdDef *         GetId() const { return _unique_id ; } // return resolved identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetId(VhdlIdDef *new_id) ; // set identifier for this name. Seleced name suffix, indexed name prefix, or simple idref.
    virtual void                SetName(char *name) ; // set _name field for this name. Seleced name suffix, indexed name prefix, or simple idref.

    virtual unsigned            FindObjects(Set &result, unsigned for_use_clause) ;
    virtual VhdlIdDef *         FindSingleObject() ;
    virtual unsigned            IsRange() ;                // Can't make this 'const'

    // Check if the name denotes a signal
    virtual unsigned            IsSignal(void) ;

    virtual unsigned            IsAccessType() ;
    virtual unsigned            IsExternalName() const { return 1 ; }
    virtual unsigned            IsProtectedType() ;
    virtual unsigned            IsExternalNameRef() const { return 1 ; } // VIPER #7890: Test to see if this VhdlName refers to a VHDL 2008 external-name

    virtual VhdlIdDef *         TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope) ;

    virtual VhdlIdDef *         FindResolutionFunction() const ; // Return the resolution function of a subtype indication.

    // Accessor methods
    VhdlIdDef *                 GetUniqueId() const     { return _unique_id ; } // replaced by GetId().
    virtual unsigned            GetClassType() const    { return _class_type ; }
    unsigned                    GetPathToken() const    { return _path_token ; }
    unsigned                    IsPackagePathName() const ;
    unsigned                    IsAbsolutePathName() const ;
    unsigned                    IsRelativePathName() const ;
    unsigned                    GetHatCount() const     { return _hat_count ; }
    VhdlName *                  GetExtPathName() const  { return _ext_path_name ; }
    virtual VhdlSubtypeIndication *     GetSubtypeIndication() const { return _subtype_indication ; }
    virtual VhdlIdDef *         SubprogramAspect(VhdlIdDef * /*formal*/) ;

    // Elaboration
    virtual VhdlValue *         Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value) ;
    virtual VhdlIdDef *         FindExternalObject(unsigned path_token, unsigned accent_count) ;

    virtual VhdlConstraint *    EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint) ; // Build a constraint from this name : range constraint / index constraint
    // Assign to a target
    virtual void                Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select) ;
    // Internal routine :
    virtual VhdlIdDef *         GetElabSpecificUniqueId() ;

protected:
// Parse tree nodes (owned)
    unsigned               _class_type ; // VHDL_signal/VHDL_constant/VHDL_variable
    unsigned               _path_token ; // VHDL_AT (package path), VHDL_DOT(absolute path), VHDL_ACCENT (relative path)
    unsigned               _hat_count ; // Number of hat('^')s in the path name
    VhdlName              *_ext_path_name ; // path name
    VhdlSubtypeIndication *_subtype_indication ; // subtype indication of referred object
// back pointer set during analysis (not owned)
    VhdlIdDef             *_unique_id ; // (resolved) identifier definition to which this selected name refers. Set when (type) resolved
} ; // class VhdlExternalName

/* -------------------------------------------------------------- */
#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_NAME_H_

