/*
 *
 * [ File Version : 1.239 - 2014/03/12 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "vhdl_file.h" // Compile flags are defined within here

#include <string.h>     // strcmp
#include <stdio.h>      // sprintf
#include <limits.h>     // ULONG_MAX

#include "SaveRestore.h"
#include "FileSystem.h"
#include "Message.h"
#include "Strings.h"
#include "Map.h"
#include "Set.h"
#include "Array.h"
#include "Protect.h"

#include "VhdlDeclaration.h"
#include "VhdlName.h"
#include "VhdlStatement.h"
#include "VhdlSpecification.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlScope.h"
#include "VhdlConfiguration.h"
#include "vhdl_tokens.h" // VIPER #7514

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/* ----------------------------------------- */
// Version History : Introduced on 5/25/04
/* ----------------------------------------- */
// Version 0x0ab00039 (03/05/14) :
//      Addition of new class VhdlSelectedForceAssignment: Viper 8152
//
// Version 0x0ab00038 (11/19/12) :
//      _can_be_multi_port_ram added in class VhdlInterfaceId: Viper 7599
//
// Version 0x0ab00037 (11/19/12) :
//      _element_type_list added in class VhdlAggregate
//
// Version 0x0ab00036 (11/12/12) :
//      Addition of new class VhdlGenericTypeId
//
// Version 0x0ab00035 (05/14/12) :
//      Change class structure for VhdlPackageInstantiationDecl
//
// Version 0x0ab00034 (05/09/2012) :
//      _enum_encoded bit in VhdlEnumerationId. Viper #7182
//
// Version 0x0ab00033 (04/23/12) :
//      Change class name VhdlInterfacePkgElement to VhdlPackageInstElement
//
// Version 0x0ab00032 (03/27/12) :
//      Change class structure for VhdlInterfacePackageDecl
//
// Version 0x0ab00031 (03/19/12) :
//      Change the class structure for VhdlPackageInstantiationDecl. Add field in VhdlPackageId
//
// Version 0x0ab00030 (12/13/11) :
//      VIPER #6896: Added _verilog_pkg_lib_name, _verilog_pkg_name to VhdlTypeId
// Version 0x0ab0002f (12/13/11) :
//      VIPER #6937: Added _analysis_mode in VhdlDesignUnit
//
// Version 0x0ab0002e (12/09/11) :
//      VIPER #6933: Added _lib_ids field in VhdlContextReference
//
// Version 0x0ab0002d (10/25/11) :
//      VIPER #6813: Added _alternative_closing_label in VhdlIfScheme and VhdlCaseItemScheme
//      under VHDL_PRESERVE_EXTRA_TOKENS compile flags to store the alternative labels.
//
// Version 0x0ab0002c (10/17/11) :
//      VIPER #6813: Added _process_stmt_replaced in VhdlPrimaryUnit to keep track of the number
//      of process statements replaced during implicit to explicit state m/c conversion
// Version 0x0ab0002b (10/17/11) :
//      VIPER #6811: Added _is_record_constraint in VhdlIndexedName to identify record constraints
// Version 0x0ab0002a (10/13/11) :
//      VIPER #6653: Added _has_explicit_param field in VhdlProcedureSpec, VhdlFunctionSpec classes
// Version 0x0ab00029 (29/07/11) :
//      VIPER #6666: Added _closing_label field in VhdlStatement, VhdlDeclaration and VhdlTypeDef classes
//
// Version 0x0ab00028 (27/07/11) :
//      VIPER #6660 deleted _is_inertial flag from VhdlAssocElement, added new class VhdlInertialExpression
// Version 0x0ab00027 (07/06/11) :
//      VIPER #6605/#6618 customer specific save-restore version update in the baseline.
// Version 0x0ab00026 (05/23/11) :
//      Vhdl 2008 enhancements : Add _is_inertial flag to class VhdlAssocElement
// Version 0x0ab00025 (02/11/11) :
//      Vhdl 2008 enhancements : Add class VhdlExternalName
//
// Version 0x0ab00024 (01/27/11) :
//      VIPER Issue #6349  : _is_globally_static for ConstantId
// Version 0x0ab00023 (01/10/11) :
//      Vhdl 2008 enhancements : Add class VhdlArrayResFunction, VhdlRecordResFunction,
//      VhdlRecResFunctionElement. These are used to parse resolution functions for
//      array and records
// Version 0x0ab00022 (01/10/11) :
//      Vhdl 2008 enhancements : Add unsigned field in VhdlSelectedSignalAssignment
//      VhdlSelectedVariableAssignmentStatement. Add new classes VhdlForceAssignmentStatement,
//      VhdlReleaseAssignmentStatement and VhdlConditionalForceAssignmentStatement.
//
// Version 0x0ab00021 (01/06/11) :
//      Vhdl 2008 enhancements : Add unsigned field in VhdlCaseStatement
//
// Version 0x0ab00020 (12/26/10) :
//      Vhdl 2008 enhancements : Add VhdlDeclaration back pointer to VhdlPackageId
//
// Version 0x0ab0001f (11/25/10) :
//      Vhdl 2008 enhancements : Add VhdlSubprogInstantiationDecl back pointer to VhdlSubprogramId
//
// Version 0x0ab0001e (09/24/10) :
//      Vhdl 2008 enhancements : Add VhdlPackageInstantiationDecl class and VhdlIdDef* member in VhdlPackageId
//      Also add VhdlSubprogInstantiationDecl class.

// Version 0x0ab0001d (09/24/10) :
//      Add flag in VhdlOperator to determine implicit "??" operator for Vhdl 2008 section 9.2.9
// Version 0x0ab0001c (09/24/10) :
//      Add different new classes for Vhdl 2008 section 11.8 specified generate construct
// Version 0x0ab0001b (08/26/10) :
//      Vhdl 2008 enhancements Section 10.5 and 10.6 conditional and selected variable/signal assignments
// Version 0x0ab0001a (08/26/10) :
//      SubprogramSpecification owns generic clause and generic map aspect for Vhdl 2008 section 4.2.1
// Version 0x0ab00019 (07/26/10) :
//      VhdlPackageDecl owns generic clause and generic map aspect for Vhdl 2008 section 4.7
// Version 0x0ab00018 (06/15/10) :
//      VhdlOperatorId::_owns_arg_id added to fix memory leaks (VIPER #5996)
// Version 0x0ab00017 (08/19/09) :
//      need to save/restore fields _is_locally_static_type/_is_globally_static_type in VhdlAliasId
// Version 0x0ab00016 (08/19/09) :
//      VIPER #5242: need to save/restore new field _inv_position in VhdlPhysicalUnitId

// Version 0x0ab00015 (10/15/08) :
//      VIPER #4301: Use Hash::NumCompare instead of Hash::PointerCompare

// Version 0x0ab00014 (8/03/07) :
//      _is_locally_static, _is_loop_iter bit_fields added in VhdlConstantId

// Version 0x0ab00013 (7/09/07) :
//      _value in VhdlInteger is changed from 'int' to 64 bit integer verific_int64.
//      _position in VhdlPhysicalUnitId is changed from 'unsigned' to 64 bit unsigned verific_uint64.

// Version 0x0ab00012 (6/14/07) :
//      _use_clause_units Map in VhdlScope was introduced and is save/restored.

// Version 0x0ab00011 (5/08/07) :
//      _primary_binding in VhdlLabelId is now set during analysis. Save/Restore it.

// Version 0x0ab00010 (4/24/07) :
//      _attributes table in VhdlIdDef now contains VhdlExpression nodes.

// Version 0x0ab0000f (1/17/07) :
//      Due to VIPER #2791 fix, labeled procedure calls with no arguments are
//      now represented as a VhdlProcedureCallStatement, instead of the
//      previously confusing VhdlComponentInstantiationStatement.

// Version 0x0ab0000e (8/16/06) :
//      Introduced saving/restoring of VhdlScope::_transparent, needed to mark
//      loop scope transparent (VIPER #2597).

// Version 0x0ab0000d (5/29/06) :
//      Introduced new predefined PSL operators.

// Version 0x0ab0000c (10/18/05) :
//      Introduced SaveLong()/RestoreLong functionality in SaveRestore object.
//      Added 32<->64 bit support.

// Version 0x0ab0000b (9/22/05) :
//      Introduced saving/restoring of VhdlDesignUnit::_orig_design_unit.

// Version 0x0ab0000a (7/21/05) :
//      Introduced saving/restoring of bit flags in VhdlDesignUnit.

// Version 0x0ab00009 (6/29/05) :
//      Dumped primary unit's conformance signature into the header portion
//      of the stream.  Added vhdl_file::GetVDBSignature, which does a quick parse
//      of the header portion of a vdb and returns the conformance signature of
//      the stored primary unit. (VIPER #2113).

// Version 0x0ab00008 (5/6/05) :
//      Now saving/restoring bitflags for VarUsage analysis.

// Version 0x0ab00007 (4/26/05) :
//      Now saving/restoring parse-tree comments under the
//      VHDL_PRESERVE_COMMENTS compile switch.

// Version 0x0ab00006 (3/28/05) :
//      CalculateSignature() logic was introduced in this version, which
//      allows us to compare signatures of dependencies at restore time
//      to see if a dependency as gone out-of-date.

// Version 0x0ab00005 (2/27/05) :
//      Added an _is_implicit member to classes VhdlUseClause and
//      VhdlLibraryClause.  Added save restore capability of this member.

// Version 0x0ab00004 (11/16/04) :
//      Added a data member to the class VhdlBitStringLiteral to store
//      the exact string format specified by the user. Added save
//      restore capability of that string.

// Version 0x0ab00003 (7/8/04) :
//      Now saving/restoring whether stream contains PSL constructs, so
//      that we can determined upon restore whether we need to give
//      a warning if PSL is NOT enabled.

// Version 0x0ab00002 (6/29/04) :
//      The _using clause of the VhdlScope is now save/restore.
//      This results in all identifiers, whether they are used or not
//      to be saved/restored from the stream. .

// Version 0x0ab00001 (5/24/04) :
//      - Backward compatibility is now made possible with this version.
//      - Saving/restoring of compile switch settings is introduced.
//        We now use a 32 bit int to represent 32 individual compile
//        settings.

// Version 0x0ab00019 (Previous version) :
//      This versions contains absolutely no extra information, such
//      as compile-switch settings or Linefile bit size settings, in
//      it stream.  This means that backward compatibility is not
//      supported in this version.

/* ----------------------------------------- */

#define VHDL_PARSE_TREE             0xbaccbeef // Define VHDL_PARSE_TREE in Commands.h to match this value!!!
#define VHDL_PERSISTENCE_VER_NUM    0x0ab00039

// Macro for storing bit flags as unsigned integers and restoring them
// a => collection of bit flags, b => bit flag value,  n => size of bit flag
#define SAVE_BIT_FLAG(a,b,n)       ((a) = ((a) << (n)) | (b))
#define RESTORE_BIT_FLAG(a,b,n)    {(b) = (a) & ((1<<(n))-1) ; (a)>>=(n) ;}

/* ----------------------------------------- */

// static member initialization:

// Vhdl library path
/*static*/ char *vhdl_file::_default_library_path = 0;
/*static*/ Map  *vhdl_file::_map_library_paths = 0 ;
#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
/*static*/ Map  *vhdl_file::_lib_unit_map = 0 ; // Map of lib_name->(Map of unitnames->position) to quickly check if a unit is in a lib-wide map file.
#endif

static const unsigned PSL_ENABLED               = 1 ;
static const unsigned PRESERVE_COMMENTS_ENABLED = 2 ;

// Product configuration flags :
static const unsigned int _current_product_flags = 0
//        | PRESERVE_COMMENTS_ENABLED // Run-time switch since 11/2012. Set to value when we save current_product_flags
        ;

//--------------------------------------------------------------
// #included from vhdl_file.h
//--------------------------------------------------------------

unsigned
vhdl_file::Save(const char *lib_name, const char *unit_name)
{
    // Verify arguments
    if (!lib_name) return 0;

#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    if (!unit_name) {
        // attempt to save entire library.
        // Under this compile flag, we will save it in a vdbl file :
        return SaveLib(lib_name) ;
    }
#endif

    // Allocate memory for manipulation
    char *libname  = Strings::save(lib_name) ;

    // If unit_name is NULL, then all primary units in lib_name will be dumped.
    char *unitname = Strings::save(unit_name) ;

    // Force libname and unitname to lowercase, unless they are escaped
    if (*libname != '\\')               libname = Strings::strtolower(libname) ;
    if (unitname && *unitname != '\\')  unitname = Strings::strtolower(unitname) ;

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Get the library :
    VhdlLibrary *work_lib = GetLibrary(libname, 0) ;
    if (!work_lib) {
        tmp_print_node.Error("The library %s was not found", libname) ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0;
    }

    // First check to see if a path was specified for this lib
    char *file_path = 0 ;
    if (vhdl_file::GetLibraryPath(libname)) {
        // VDB explicit path
        file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/") ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // VDB default path
        file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/") ;
    } else {
        tmp_print_node.Error("No VHDL library search path could be found. Please set a search path to where unit can be saved") ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0;
    }

    Strings::free(libname) ;

    // Now create the directory path
    if (!FileSystem::Mkdir(file_path)) {
        tmp_print_node.Error("Save failed due to mkdir failure") ;
        Strings::free(file_path) ;
        Strings::free(unitname) ; // JJ 130830 memleak fix
        return 0 ;
    }

    // Determine vdb destination path based on library name and unit name

    // Flag needed when unitname is valid.
    unsigned specific_unit = (unitname) ? 1 : 0 ;

    MapIter mi ;
    VhdlPrimaryUnit *unit ;
    FOREACH_VHDL_PRIMARY_UNIT(work_lib, mi, unit){
        if (!unit) continue ;
        // If unitname is valid, then only save that unit
        if (specific_unit) {
            if (!unitname || !Strings::compare(unitname, unit->Name())) continue ;
            Strings::free(unitname) ;
            unitname = 0 ;
        }

        char *vdb_name = SaveRestore::GetDumpFileName(unit->Name(), ".vdb" /* extension */) ; // VIPER #3498
        // Append file name to end of path
        char *full_file_path = Strings::save(file_path, vdb_name) ;

        // Create SaveRestore object
        Protect *cipher = vhdl_file::GetProtectionObject() ;
        SaveRestore save_restore(full_file_path, 0 /* Save */, 1, cipher) ;
        Strings::free(full_file_path) ;
        Strings::free(vdb_name) ;

        if (!save_restore.IsOK()) {
            // Error Message is already pumped out
            Strings::free(file_path) ;
            return 0 ;
        }

        // Unlike during restore, we DON'T have to set the SaveRestore version here, since it's only
        // useful for restore checks.

        // Dump vhdl parse-tree identification
        save_restore.SaveInteger(VHDL_PARSE_TREE) ;

        // Dump vhdl persistence version number
        save_restore.SaveInteger(VHDL_PERSISTENCE_VER_NUM) ;

        // Dump an int (32 bits) for potential flag information (used for backward compatibility)
        unsigned current_product_flags = _current_product_flags ; // compile flag setting
        // Add run-time flags that affect the stream:
        if (RuntimeFlags::GetVar("vhdl_preserve_comments")) current_product_flags |= PRESERVE_COMMENTS_ENABLED ;
        save_restore.SaveInteger(current_product_flags) ;

        // Dump lib and unit name, so on the restore side of things it knows where to load unit.
        save_restore.SaveString(unit->Owner()->Name()) ;
        save_restore.SaveString(unit->Name()) ;

        // Dump design unit's save/restore signature
        // VIPER #4301: Use new method Hash::NumCompare to calculate the signature:
        save_restore.SaveLong(unit->CalculateSaveRestoreSignature(1 /* use num_compare */)) ;

        // Message::Msg(VERIFIC_COMMENT, 0, 0, "Saving VHDL parse-tree %s.%s into %s", work_lib->Name(), unit->Name(), save_restore.FileName()) ;
        tmp_print_node.Comment("Saving VHDL parse-tree %s.%s into %s", work_lib->Name(), unit->Name(), save_restore.FileName()) ;

        VhdlTreeNode::SaveNodePtr(save_restore, unit) ;
        // If an error occurred during saving, delete appropriately and return zero
        if (!save_restore.IsOK()) {
            Strings::free(file_path) ;
            return 0;
        }
    }

    Strings::free(file_path) ;

    // Make sure if unit_name was provided, that it actually got save.  (if unitname is still
    // valid here, then it was not saved)
    if (specific_unit && unitname) {
        tmp_print_node.Error("unit %s not found in library %s", unitname, work_lib->Name()) ;
        Strings::free(unitname) ;
        return 0 ;
    }

    return 1 ;
}

/* ----------------------------------------- */

unsigned
vhdl_file::Restore(const char *lib_name, const char *unit_name, unsigned silent /*=0*/)
{
    // Error count is not relied upon during the saving/restoring of a VDB, therefore clearing the error
    // count is not needed.  The SaveRestore::IsOK() is to be used to determine if an 'error' has occurred.

    if (!lib_name || !unit_name) return 0;

    VhdlTreeNode tmp_print_node ;   // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Allocate memory for manipulation
    char *libname = Strings::save(lib_name) ;
    char *unitname = Strings::save(unit_name) ;

    // Force lib_name and unit_name to lowercase, unless they are escaped
    if (*libname != '\\')   libname = Strings::strtolower(libname) ;
    if (*unitname != '\\')  unitname = Strings::strtolower(unitname) ;

    // Determine full path to vdb
    char *file_path = 0 ;
    // First check to see if a path was specified for this lib
    char *vdb_name = SaveRestore::GetDumpFileName(unitname, ".vdb" /* extension */) ; // VIPER #3498
    if (vhdl_file::GetLibraryPath(libname)) {
        file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/", vdb_name) ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/", vdb_name) ;
    }
    Strings::free(vdb_name) ; // Free allocated string

    if (!file_path) {
        if (!silent) tmp_print_node.Error("VHDL library search path not set; cannot restore") ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0;
    }

    // Don't need to do a SaveRestore::FileExists(file_path) check here, since error
    // will be pushed out by SaveRestore::SaveRestore if file doesn't exist.

#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    long startpos = 0 ;
    if (!SaveRestore::FileExists(file_path)) {
        // Try the library-wide dump file
        startpos = PosInLibFile(lib_name, unit_name) ;

        // If startpos is 0, then unit is not in a lib side file.
        // But if it IS, then adjust the restore file to the restore file name.
        // The fact that 'startpos' is set also means that we are dealing with a lib file, and not a unit-only file.
        if (startpos) {
            // Otherwise, create the lib file name that this module is in :
            // First check to see if a path was specified for this lib
            Strings::free(file_path) ;
            file_path = 0 ;
            if (vhdl_file::GetLibraryPath(libname)) {
                file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/", libname, ".vdbl") ;
            } else if (vhdl_file::GetDefaultLibraryPath()) {
                // Use default search path
                file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/", libname, ".vdbl") ;
            }
        }
    }
#endif

    // Return silently if file doesn't exist and 'silent' was specified, else SaveRestore::SaveRestore will generate error.
    if (silent && !SaveRestore::FileExists(file_path)) {
        Strings::free(file_path) ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0 ;
    }

    tmp_print_node.Comment("Restoring VHDL parse-tree %s.%s from %s", libname, unitname, file_path) ;

    // Open the file stream
    Protect *cipher = vhdl_file::GetProtectionObject() ;
    SaveRestore save_restore(file_path, 1 /* Restore */, 1, cipher) ;
    Strings::free(file_path) ; // use save_restore.FileName() from now on

    if (!save_restore.IsOK()) {
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0 ; // Error Message is already pumped out
    }

    // Verify that file contains a binary VHDL parse-tree
    if ((unsigned)save_restore.RestoreInteger() != VHDL_PARSE_TREE) {
        Strings::free(libname) ;
        Strings::free(unitname) ;
        tmp_print_node.Error("%s does not contain a known VHDL parse-tree format", save_restore.FileName()) ;
        return 0 ;
    }

    // Restore vhdl persistence version number
    unsigned ver_num = save_restore.RestoreInteger() ;
    if (ver_num < 0x0ab00001) {
        char szBuf[12];
        sprintf(szBuf, "0x%x", ver_num) ;
        tmp_print_node.Error("%s was created using code corresponding to a vhdl_version_number of %s, backward compatibility of versions before 0x0ab00001 is not supported", save_restore.FileName(), szBuf) ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0 ;
    }
    // Check that vdb was not made with future version of code
    if (ver_num > VHDL_PERSISTENCE_VER_NUM) {
        char szBuf[12];
        sprintf(szBuf, "0x%x", ver_num) ;
        tmp_print_node.Error("%s was created using a later vhdl_version_number, forward compatibility is not supported", save_restore.FileName()) ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return 0 ;
    }

    // Override SaveRestore version with VHDL binary save/restore version.
    // This is needed for backward compatibility.
    save_restore.SetVersion(ver_num) ;

    // Restore flags (used for backward compatibility)
    unsigned flags = save_restore.RestoreInteger() ;
    save_restore.SetUserFlags(flags) ;

    // Warn on product mismatches where the program that created the stream may have saved formats that not supported in the current program.
    if (_current_product_flags != flags) {
        if (~_current_product_flags & flags & PSL_ENABLED) {
            tmp_print_node.Warning("%s was previously created using a PSL-enabled VHDL analyzer, since the current VHDL analyzer is not PSL-enabled, there is a high probability that a restore error will occur due to unknown PSL constructs", save_restore.FileName()) ;
        } else if (!(flags & PSL_ENABLED) && (_current_product_flags & PSL_ENABLED)) {
            tmp_print_node.Warning("%s was previously created using a non-PSL-enabled VHDL analyzer, and since the current VHDL analyzer is PSL-enabled, there is a high probability that a restore error will occur due to newer VHDL 2008 constructs", save_restore.FileName()) ;
        }
    }

#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // if startpos is set, then we need to go to that position (in the lib file) for the unit :
    if (startpos) {
        save_restore.SetPos(startpos) ;
    }
#endif

    // VIPER #6937 : Save the analysis mode. During restore, global analysis
    // mode will be set for each unit
    unsigned prev_dialect = vhdl_file::GetAnalysisMode() ;

    // Restore library name
    char *lib_name2 = save_restore.RestoreString() ;
    Strings::free(lib_name2) ; // Original (logical) library name is not important. We restore into requested logical library name

    // Restore primary unit name
    char *unit_name2 = save_restore.RestoreString() ;
    Strings::free(unit_name2) ; // Original (logical) unit name is not important any more.

    // Restore primary unit's conformance signature
    if (ver_num >= 0x0ab00009) {
        // No need to store this anywhere (... yet).  This field is only used in vhdl_file::GetVDBSignature.
        if (ver_num >= 0x0ab0000c) {
            (void) save_restore.RestoreLong() ;
        } else {
            (void) save_restore.RestoreInteger() ;
        }
    }

    // Finally, really restore the primary unit :
    VhdlPrimaryUnit *primary_unit = (VhdlPrimaryUnit*)VhdlTreeNode::RestoreNodePtr(save_restore) ;

    // If an error has occurred, delete appropriately and return zero
    if (!primary_unit || !save_restore.IsOK()) {
        tmp_print_node.Error("%s.%s failed to restore", libname, unitname) ;
        Strings::free(libname) ;
        Strings::free(unitname) ;
        delete primary_unit;
        vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
        return 0 ;
    }

    Strings::free(libname) ;
    Strings::free(unitname) ;

    // NOTE : We could check/warn here if the unit name is the same as the expected unit name

    // Get handle to the library which this unit will be added to.
    // NOTE, this library may be different (by name) than the library that was
    // originally saved. (VIPER #1892)
    VhdlLibrary *library = GetLibrary(lib_name, 1 /* Create if necessary */) ;
    VERIFIC_ASSERT(library) ; // it must be created if not existing

    // Add the unit to the requested library.
    (void) library->AddPrimUnit(primary_unit, 0/*refuse_on_error*/) ;

    vhdl_file::SetAnalysisMode(prev_dialect) ; // VIPER #6937
    return 1 ;
}

#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE

/*************************************************************************/
/*             VIPER 2944 : library-wide save/restore file               */
/*************************************************************************/
long
vhdl_file::PosInLibFile(const char *lib_name, const char *unit_name)
{
    if (!lib_name) return 0 ;

    // Allocate memory for manipulation
    char *libname = Strings::save(lib_name) ;

    // Force libname to lowercase, unless they are escaped
    if (*libname != '\\') libname = Strings::strtolower(libname) ;

    // First check if we have visited this library before, and have a unit_name->pos_in_file Map for it.
    if (!_lib_unit_map) _lib_unit_map = new Map(STRING_HASH) ;
    // look for the library by name :
    Map *unit_map = (Map*)(_lib_unit_map->GetValue(libname));

    // get unit position from the unit map.
    if (unit_map) {
        // Get unit name (if any).
        // Force unitname to lowercase, unless they are escaped
        char *unitname = Strings::save(unit_name) ;
        if (unitname && *unitname != '\\') unitname = Strings::strtolower(unitname) ;

        long pos = 0 ;
        if (unitname) {
            // Return the position of the unit in the file, or 0 if not found.
            pos = (long)(unit_map->GetValue(unitname));
        } else {
            // If there is NO unit name, then assume user is asking for the position of the FIRST unit in the lib file.
            pos = (long)(unit_map->GetValueAt(0)) ;
        }
        // Free the allocated strings
        Strings::free(libname) ;
        Strings::free(unitname) ;
        return pos ;
    }

    Strings::free(libname) ;

    // Now that we have a library unit map, call myself again.
    // It will then pick up unit position from the map :
    return RestoreLibUnitMap(lib_name) ? PosInLibFile(lib_name, unit_name) : 0 ; // 0 means : unit not found.
}

unsigned
vhdl_file::RestoreLibUnitMap(const char *lib_name)
{
    // First check if we have visited this library before, and have a unit_name->pos_in_file Map for it.
    if (!_lib_unit_map) _lib_unit_map = new Map(STRING_HASH) ; // Map of lib_name->(Map of unitnames) to quickly check if a unit is in a lib-wide map file.

    // Allocate memory for manipulation
    char *libname = Strings::save(lib_name) ;

    // Force libname to lowercase, unless they are escaped
    if (*libname != '\\') libname = Strings::strtolower(libname) ;

    // Check if this library is done before :
    Map *unit_map = (Map*)(_lib_unit_map->GetValue(libname));
    if (unit_map) {
        Strings::free(libname) ;
        return 1 ;
    }

    // If there is no unit_map yet, create one from the side-file for this library (if there).
    // First create an empty unit_map and insert under this library name.
    unit_map = new Map(STRING_HASH);
    _lib_unit_map->Insert(libname, unit_map); // Give memory of the libname char* to the table.

    // First check to see if there is a sidefile for this lib
    char *side_file_path = 0 ;
    if (vhdl_file::GetLibraryPath(libname)) {
        side_file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/", libname, ".vdbx") ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // Use default search path
        side_file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/", libname, ".vdbx") ;
    }

    // Check if this file exists :
    if (!SaveRestore::FileExists(side_file_path)) {
        Strings::free(side_file_path) ;
        return 0 ;
    }

    // Open side file and check if we have 'unitname' in there.
    SaveRestore side_file(side_file_path, 1 /* Restore */) ;
    Strings::free(side_file_path) ; // use side_file_path.FileName() from now on

    if (!side_file.IsOK()) {
        return 0 ; // Error Message is already pumped out
    }

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Verify that side file is correct
    if ((unsigned)side_file.RestoreInteger() != VHDL_PARSE_TREE) {
        tmp_print_node.Error("%s does not contain a known VHDL parse-tree format", side_file.FileName()) ;
        return 0 ;
    }
    // get version number :
    int ver_num = side_file.RestoreInteger() ;
    // Check that vdb was not made with future version of code
    if (ver_num > VHDL_PERSISTENCE_VER_NUM) {
        char szBuf[12];
        sprintf(szBuf, "0x%x", ver_num) ;
        tmp_print_node.Error("%s was created using a later vhdl_version_number, forward compatibility is not supported", side_file.FileName()) ;
        return 0 ;
    }

    // Finally : Fill the 'unit_map' with all available unit->pos associations from the side file
    char *on_file_unit_name ;
    long pos ;
    while (1) {
        // Get next unit name :
        on_file_unit_name = side_file.RestoreString() ; // downcased name of the unit..
        if (!on_file_unit_name) break ; // there are no more units in this file.

        // Get its file position :
        pos = side_file.RestoreLong() ; // unit's position in the file.

        // Insert in the unit map, so we can use it next time.
        if (!unit_map->Insert(on_file_unit_name,(void*)pos)) { // give (the memory of) the string to the map.
            // Insert failed.. already existing in the map
            Strings::free(on_file_unit_name) ;
        }
    }

    return 1 ;
}

/* static */
void
vhdl_file::ClearLibUnitMap()
{
    // clear internal tables used to speedup lookups in library-wide save/restore files
    if (!_lib_unit_map || _lib_unit_map->Size()==0) return ;

    // Clear the lib_unit_map. Free all its elements memory :
    // Map of lib_name->(Map of unitnames->pos_in_libfile)
    MapIter mi, mii ;
    Map *unit_map ;
    char *lib_name, *unit_name ;
    FOREACH_MAP_ITEM(_lib_unit_map, mi, &lib_name, &unit_map) {
        // unit_map is a Map of unitnames->pos_in_libfile.
        FOREACH_MAP_ITEM(unit_map, mii, &unit_name, 0/* pos is 'long' (not allocated) */) {
            Strings::free(unit_name) ;
        }
        delete unit_map ;
        Strings::free(lib_name) ;
    }

    // delete the table, and re-set the _lib_unit_map pointer.
    // NOTE : Instead of this, we could just do a _lib_unit_map->Reset().
    delete _lib_unit_map ;
    _lib_unit_map = 0 ;
}

/* static */
unsigned
vhdl_file::SaveLib(const char *lib_name)
{
    // Save entire library to a library-wide dump file.
    // Verify arguments
    if (!lib_name) return 0;

    // Error count is not relied upon during the saving/restoring of a VDB, therefore clearing the error
    // count is not needed.  The SaveRestore::IsOK() is to be used to determine if an 'error' has occurred.

    // Allocate memory for manipulation
    char *libname  = Strings::save(lib_name) ;

    // Force libname to lowercase, unless they are escaped
    if (*libname != '\\') libname = Strings::strtolower(libname) ;

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Get the library :
    VhdlLibrary *work_lib = GetLibrary(libname, 0) ;
    if (!work_lib) {
        tmp_print_node.Error("The library %s was not found", libname) ;
        Strings::free(libname) ;
        return 0;
    }

    // First check to see if a path was specified for this lib
    char *file_path;
    if (vhdl_file::GetLibraryPath(libname)) {
        file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/") ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/") ;
    } else {
        tmp_print_node.Error("No VHDL library search path could be found. Please set a search path to where unit can be saved") ;
        Strings::free(libname) ;
        return 0;
    }

    // Now create the directory path
    if (!FileSystem::Mkdir(file_path)) {
        tmp_print_node.Error("Save failed due to mkdir failure") ;
        Strings::free(file_path) ;
        Strings::free(libname) ;
        return 0 ;
    }

    // We are now about to create a vdbl file with its vdbx sidefile for this library libname.
    // First remove existing library entry in the _lib_unit_map for this library (if there from a previous saved library/sidefile).
    // Lets just clear the whole map (easier) :
    ClearLibUnitMap() ;

    // Determine vdbl(library)/vdbx(index) destination file based on library name :

    // Append file name to end of path. Target name of file is "<libname>.vdbl".
    char *full_file_path = Strings::save(file_path, libname, ".vdbl") ;
    char *side_file_path = Strings::save(file_path, libname, ".vdbx") ; // index file (or unit names->file position)

    Strings::free(libname) ;
    Strings::free(file_path) ;

    // Create SaveRestore object (open the file) :
    Protect *cipher = vhdl_file::GetProtectionObject() ;
    SaveRestore save_restore(full_file_path, 0 /* Save */, 1, cipher) ;
    Strings::free(full_file_path) ;

    if (!save_restore.IsOK()) {
        // Error Message is already pumped out
        Strings::free(side_file_path) ;
        return 0 ;
    }

    // Open file 'side_file_path' as a file stream :
    SaveRestore side_file(side_file_path, 0 /* save */) ;
    Strings::free(side_file_path) ;

    if (!side_file.IsOK()) {
        // Error Message is already pumped out
        return 0 ;
    }

    // Unlike during restore, we DON'T have to set the SaveRestore version here, since it's only
    // useful for restore checks.

    // Dump vhdl parse-tree identification
    save_restore.SaveInteger(VHDL_PARSE_TREE) ;

    // Dump vhdl persistence version number
    save_restore.SaveInteger(VHDL_PERSISTENCE_VER_NUM) ;

    // Same for the side file :
    side_file.SaveInteger(VHDL_PARSE_TREE) ; // should probably be different id number for lib idx files.

    // Push version number out :
    side_file.SaveInteger(VHDL_PERSISTENCE_VER_NUM) ; // could be lib idx file specific number

    // Dump an int (32 bits) for potential flag information (used for backward compatibility)
    unsigned current_product_flags = _current_product_flags ; // compile flag setting
    // Add run-time flags that affect the stream:
    if (RuntimeFlags::GetVar("vhdl_preserve_comments")) current_product_flags |= PRESERVE_COMMENTS_ENABLED ;
    save_restore.SaveInteger(current_product_flags) ;

    // Now dump the units from this library :
    MapIter mi ;
    VhdlPrimaryUnit *unit ;
    FOREACH_VHDL_PRIMARY_UNIT(work_lib, mi, unit) {
        if (!unit) continue ;

        // reset the pointer registrations :
        save_restore.Reset() ;

        // Save the name of the unit and the position where we are now into the side file :
        side_file.SaveString(unit->Name()) ; // downcased name..
        long pos = save_restore.GetPos() ; // current file position in the main vdb file
        side_file.SaveLong(pos) ;

        // Dump lib and unit name, so on the restore side of things it knows where to load unit.
        save_restore.SaveString(unit->Owner()->Name()) ;
        save_restore.SaveString(unit->Name()) ;

        // Dump design unit's save/restore signature
        // VIPER #4301: Use new method Hash::NumCompare to calculate the signature:
        save_restore.SaveLong(unit->CalculateSaveRestoreSignature(1 /* use num_compare */)) ;

        VhdlTreeNode tmp_print_node2 ; // Viper: 3142 - dummy node for msg printing // JJ 130830 tmp_print_node shadows prev def, move to tmp_print_node2
        tmp_print_node2.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        // Message::Msg(VERIFIC_COMMENT, 0, 0, "Saving VHDL parse-tree %s.%s into %s", work_lib->Name(), unit->Name(), save_restore.FileName()) ;
        tmp_print_node2.Comment("Saving VHDL parse-tree %s.%s into %s", work_lib->Name(), unit->Name(), save_restore.FileName()) ;

        // Save the unit :
        VhdlTreeNode::SaveNodePtr(save_restore, unit) ;

        // If an error has occurred, delete appropriately and return zero
        if (!save_restore.IsOK()) {
            return 0;
        }
    }

    // Dump a NULL pointer string so that we know there are no more units in this file :
    save_restore.SaveString(0) ;

    // and into the side file too :
    side_file.SaveString(0) ;

    // Successfully dumped the whole library.
    return 1 ;
}

/* static */
unsigned
vhdl_file::RestoreLib(const char *lib_name)
{
    // Error count is not relied upon during the saving/restoring of a VDB, therefore clearing the error
    // count is not needed.  The SaveRestore::IsOK() is to be used to determine if an 'error' has occurred.

    if (!lib_name) return 0;

    // Allocate memory for manipulation
    char *libname = Strings::save(lib_name) ;

    // Force lib_name to lowercase, unless they are escaped
    if (*libname != '\\') libname = Strings::strtolower(libname) ;

    // Determine full path to vdb
    char *file_path = 0 ;
    // First check to see if a path was specified for this lib
    if (vhdl_file::GetLibraryPath(libname)) {
        file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/", libname, ".vdbl") ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/", libname, ".vdbl") ;
    }
    Strings::free(libname) ;

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    if (!file_path) {
        tmp_print_node.Error("VHDL library search path not set; cannot restore") ;
        return 0;
    }

    // Open the file stream
    Protect *cipher = vhdl_file::GetProtectionObject() ;
    SaveRestore save_restore(file_path, 1 /* Restore */, 1, cipher) ;
    Strings::free(file_path) ; // use save_restore.FileName() from now on

    if (!save_restore.IsOK()) {
        return 0 ; // Error Message is already pumped out
    }

    // Verify that file contains a binary VHDL parse-tree
    if ((unsigned)save_restore.RestoreInteger() != VHDL_PARSE_TREE) {
        tmp_print_node.Error("%s does not contain a known VHDL parse-tree format", save_restore.FileName()) ;
        // save_restore.FileNotOk() ; // Disable SaveRestore object // RD: not necessary, since save_restore object is local
        return 0 ;
    }

    // Restore vhdl persistence version number
    unsigned ver_num = save_restore.RestoreInteger() ;
    // Check that vdb was not made with future version of code
    if (ver_num > VHDL_PERSISTENCE_VER_NUM) {
        char szBuf[12];
        sprintf(szBuf, "0x%x", ver_num) ;
        tmp_print_node.Error("%s was created using a later vhdl_version_number, forward compatibility is not supported", save_restore.FileName()) ;
        // save_restore.FileNotOk() ; // Disable SaveRestore object // RD: not necessary, since save_restore object is local
        return 0 ;
    }

    // Override SaveRestore version with VHDL binary save/restore version.
    // This is needed for backward compatibility.
    save_restore.SetVersion(ver_num) ;

    // Restore flags (used for backward compatibility)
    unsigned flags = save_restore.RestoreInteger() ;
    save_restore.SetUserFlags(flags) ;

    // If the stream was saved with the VHDL PSL product, and currently the VHDL PSL
    // analyzer is not enabled, then we must give a warning that the restore may
    // fail due to unknown PSL contructs which may be stored in the stream.
    // Look at bit 0 : PM_VHDL_PSL_ANALYZER
    if (flags & PSL_ENABLED) {
        tmp_print_node.Warning("%s was previously created using a PSL-enabled VHDL analyzer, since the current VHDL analyzer is not PSL-enabled, there is a high probability that a restore error will occur due to unknown PSL constructs", save_restore.FileName()) ;
        // Message::Warning(0, save_restore.FileName(), " was previously created using a PSL-enabled VHDL analyzer") ;
        // Message::Warning(0, "since the current VHDL analyzer is not PSL-enabled, there is a high probability that",
        //                     " a restore error will occur due to unknown PSL constructs") ;
    }

    while (1) {
        // reset the pointer registrations :
        save_restore.Reset() ;

        // Restore library name first.
        // If it is there, then there is a unit coming up. Otherwise, we are at the end of the units.
        char *lib_name2 = save_restore.RestoreString() ;
        if (!lib_name2) break ; // done. No more units in this file.

        // Restore primary unit name
        char *unit_name2 = save_restore.RestoreString() ;

        // Restore primary unit's conformance signature
        if (ver_num >= 0x0ab00009) {
            // No need to store this anywhere (... yet).  This field is only used in vhdl_file::GetVDBSignature.
            if (ver_num >= 0x0ab0000c) {
                (void) save_restore.RestoreLong() ;
            } else {
                (void) save_restore.RestoreInteger() ;
            }
        }

        tmp_print_node.Comment("Restoring VHDL parse-tree %s.%s from %s", lib_name2, unit_name2, save_restore.FileName()) ;

        // Finally, really restore the primary unit :
        VhdlPrimaryUnit *primary_unit = (VhdlPrimaryUnit*)VhdlTreeNode::RestoreNodePtr(save_restore) ;

        // If an error occurred, delete appropriately and return zero
        if (!primary_unit || !save_restore.IsOK()) {
            tmp_print_node.Error("%s.%s failed to restore", lib_name2, unit_name2) ;
            delete primary_unit;
            Strings::free(lib_name2) ;
            Strings::free(unit_name2) ;
            return 0 ;
        }

        // NOTE : We could check/warn here if the unit name is the same as the expected unit name

        // Get handle to the library which this unit will be added to.
        // NOTE, this library may be different (by name) than the library that was
        // originally saved. (VIPER #1892)
        VhdlLibrary *library = GetLibrary(lib_name, 1 /* Create if necessary */) ;
        VERIFIC_ASSERT(library) ; // it must be created if not existing

        // Add the unit to the requested library.
        (void) library->AddPrimUnit(primary_unit, 0/*refuse_on_error*/) ;

        // clean up
        Strings::free(lib_name2) ;
        Strings::free(unit_name2) ;
    }

    return 1 ;
}

#endif // VHDL_USE_LIB_WIDE_SAVERESTORE

/*************************************************************************/
/*                  LIBRARY SEARCH PATH METHODS                          */
/*************************************************************************/

unsigned
vhdl_file::VerifyAndModifyPath(char **path)
{
    // Corner case
    if (!*path || !strcmp(*path, "")) {
        Strings::free(*path) ;
        *path = Strings::save(".") ;
        return 1;
    }

    // Make sure that path does NOT end with a "/"
    char *p ;
    for(p = *path + Strings::len(*path) - 1; *p == '/'; p--) { *p = 0; }

    return FileSystem::IsDir(*path) ;
}

#if 0
unsigned
vhdl_file::IsVDBValid(const char *lib_name, const char *unit_name)
{
    if (!lib_name || !unit_name) return 0 ;
    // Allocate memory for manipulation
    char *libname = Strings::save(lib_name) ;
    char *unitname = Strings::save(unit_name) ;

    // Force libname and unitname to lowercase, unless they are escaped
    if (*libname != '\\')  libname = Strings::strtolower(libname) ;
    if (*unitname != '\\') unitname = Strings::strtolower(unitname) ;

    // Determine full path to vdb
    char *file_path = 0 ;
    char *vdb_name = SaveRestore::GetDumpFileName(unitname, ".vdb" /* extension */) ; // VIPER #3498
    // First check to see if a path was specified for this lib
    if (vhdl_file::GetLibraryPath(libname)) {
        file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/", vdb_name) ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/", vdb_name) ;
    }
    Strings::free(libname) ;
    Strings::free(unitname) ;
    Strings::free(vdb_name) ;

    // Now check to see if this file exists
    if (SaveRestore::FileExists(file_path)) {
        Strings::free(file_path) ;
        return 1 ;
    }
    // Here, file does not exist.
    Strings::free(file_path) ;

#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // Check if the unit is in a library-wide side file :
    long pos = PosInLibFile(lib_name,unit_name) ;
    if (pos) {
        return 1 ; // yes. It is in a library side-file.
    }
#endif // VHDL_USE_LIB_WIDE_SAVERESTORE

    return 0 ; // cannot find this unit
}
#endif

unsigned long
vhdl_file::GetVDBSignature(const char *lib_name, const char *unit_name)
{
    // This routine parses the header portion of a vdb and returns the corresponding
    // conformance signature of the primary unit.  A return value of zero means that
    // either the vdb doesn't contain a signature, or that the vdb is of the wrong
    // format.

    if (!lib_name || !unit_name) return 0 ;

    // Allocate memory for manipulation
    char *libname = Strings::save(lib_name) ;
    char *unitname = Strings::save(unit_name) ;

    // Force lib_name and unit_name to lowercase, unless they are escaped
    if (*libname != '\\')  libname = Strings::strtolower(libname) ;
    if (*unitname != '\\') unitname = Strings::strtolower(unitname) ;

    // Determine full path to vdb
    char *file_path = 0 ;
    char *vdb_name = SaveRestore::GetDumpFileName(unitname, ".vdb" /* extension */) ; // VIPER #3498
    // First check to see if a path was specified for this lib
    if (vhdl_file::GetLibraryPath(libname)) {
        file_path = Strings::save(vhdl_file::GetLibraryPath(libname), "/", vdb_name) ;
    } else if (vhdl_file::GetDefaultLibraryPath()) {
        // Use default search path
        file_path = Strings::save(vhdl_file::GetDefaultLibraryPath(), "/", libname, "/", vdb_name) ;
    }
    Strings::free(libname) ;
    Strings::free(unitname) ;
    Strings::free(vdb_name) ;

    if (!file_path) return 0 ;

    if (!SaveRestore::FileExists(file_path)) {
        // If file is not there, then return 0 without erroring out.
        Strings::free(file_path) ;
        return 0 ;
    }

    // Open the file stream
    Protect *cipher = vhdl_file::GetProtectionObject() ;
    SaveRestore save_restore(file_path, 1 /*restore*/, 1, cipher) ;
    Strings::free(file_path) ; // use save_restore.FileName() from now on

    if (!save_restore.IsOK()) {
        return 0 ; // Error Message is already pumped out
    }

    // Restore and throw away parse-tree identification
    if ((unsigned)save_restore.RestoreInteger() != VHDL_PARSE_TREE) {
        save_restore.FileNotOk() ; // Disable SaveRestore object
        return 0 ;
    }

    // Restore vhdl persistence version number
    int ver_num = save_restore.RestoreInteger() ;
    if (ver_num < 0x0ab00009) {
        save_restore.FileNotOk() ; // Disable SaveRestore object
        return 0 ; // Return 0 if stream does not contain a signature
    }

    // Restore and throw away flags
    (void) save_restore.RestoreInteger() ;

    // Restore and throw away library name
    char *tmp = save_restore.RestoreString() ;
    Strings::free(tmp) ;

    // Restore and throw away primary unit name
    tmp = save_restore.RestoreString() ;
    Strings::free(tmp) ;

    // Return primary unit's conformance signature
    unsigned long sig = (ver_num >= 0x0ab00009) ? (unsigned long)save_restore.RestoreLong() : (unsigned long)save_restore.RestoreInteger() ;

    // Disable SaveRestore object before returning so destructor behaves correctly
    save_restore.FileNotOk() ;

    return sig ;
}

/* ----------------------------------------- */

// DEFAULT SEARCH PATH :

// static
char *
vhdl_file::GetDefaultLibraryPath()
{
    return _default_library_path ;
}

// static
unsigned
vhdl_file::SetDefaultLibraryPath(const char *path)
{
    if (_default_library_path) {
        ClearDefaultLibraryPath() ;
    }

    if (path) {
        _default_library_path = FileSystem::Convert2AbsolutePath(path, 1) ;

        VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        if (!VerifyAndModifyPath(&_default_library_path)) {
            Strings::free(_default_library_path) ;
            _default_library_path = 0 ;

            tmp_print_node.Error("The path \"%s\" does not exist", path) ;

            return 0 ;   // path is not a valid path
        }
        tmp_print_node.Info("The default vhdl library search path is now \"%s\"", _default_library_path) ;
    }

    return 1 ;
}

// static
void
vhdl_file::ClearDefaultLibraryPath()
{
    Strings::free(_default_library_path) ;
    _default_library_path = 0 ;
#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // also clear static internal table (if there) that deals with library-wide save/restore files
    ClearLibUnitMap() ;
#endif
}

/* ----------------------------------------- */

// LIBRARY -> SEARCH PATH MAPPING:

// static
char *
vhdl_file::GetLibraryPath(const char *library)
{
    if (!_map_library_paths) return 0 ;

    return (char*)(_map_library_paths->GetValue(library)) ;
}

// static
unsigned
vhdl_file::AddLibraryPath(const char *library, const char *dir_name)
{
    if (!library) return 0 ;
    // All allocation and deallocation of map strings must be handled from within this function
    char *lib_key = Strings::save(library) ;
    char *dir_val = FileSystem::Convert2AbsolutePath(dir_name, 1) ;

    // Force lib_key to lowercase, unless it is escaped
    if (*lib_key != '\\') lib_key = Strings::strtolower(lib_key) ;

    (void) VerifyAndModifyPath(&dir_val) ;  // Don't need to check if it is a valid path.  This will be done at the Save/Restore time.

    // Remove existing entry, if there :
    MapItem *map_item = (_map_library_paths) ? _map_library_paths->GetItem(lib_key) : 0 ;
    if (map_item) {
        char *orig_key = (char*)map_item->Key() ;
        char *orig_val = (char*)map_item->Value() ;
        (void) _map_library_paths->Remove(map_item) ; // remove the entry
        Strings::free(orig_key) ;
        Strings::free(orig_val) ;
#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
        // also clear static internal table (if there) that deals with library-wide save/restore files
        ClearLibUnitMap() ;
#endif
    }

    // Now insert new entry (both saved strings go in there) :
    if (!_map_library_paths) _map_library_paths = new Map(STRING_HASH) ;
    (void) _map_library_paths->Insert(lib_key, dir_val) ;

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
    tmp_print_node.Info("The vhdl library search path for library \"%s\" is now \"%s\"", lib_key, dir_val) ;
    // Message::Info(0, "The vhdl library search path for library \"", lib_key, "\" is now \"", dir_val, "\"") ;

    return 1;
}

// static
unsigned
vhdl_file::RemoveLibraryPath(const char *library)
{
    if (!library || !_map_library_paths) return 0 ;

    // lookup this library mapping :
    MapItem *map_item = _map_library_paths->GetItem(library) ;
    if (!map_item) return 0 ; // library not found

    // cleanup this entry
    char *orig_key = (char*)map_item->Key() ;
    char *orig_val = (char*)map_item->Value() ;
    (void) _map_library_paths->Remove(map_item) ; // actually remove the entry
    Strings::free(orig_key) ;
    Strings::free(orig_val) ;
#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // also clear static internal table (if there) that deals with library-wide save/restore files
    ClearLibUnitMap() ;
#endif
    return 1;
}

// static
void
vhdl_file::ResetAllLibraryPaths()
{
    MapIter mapIter ;
    char *key, *val ;
    FOREACH_MAP_ITEM(_map_library_paths, mapIter, &key, &val) {
        Strings::free(key) ;
        Strings::free(val) ;
    }
    //_map_library_paths->Reset() ;
    delete _map_library_paths ;
    _map_library_paths = 0 ;
#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // also clear static internal table (if there) that deals with library-wide save/restore files
    ClearLibUnitMap() ;
#endif
}

//--------------------------------------------------------------
// #included from VhdlTreeNode.h
//--------------------------------------------------------------

// The following DATA_TO_FOLLOW constant is only used by the VhdlTreeNode utility methods.
static const unsigned DATA_TO_FOLLOW    = 0xb0000001;   // Specifies that the next data to parse is class data

/****************************************************************/
/***************** Utility Persistence Methods ******************/
/****************************************************************/

// static
void
VhdlTreeNode::SaveArray(SaveRestore &save_restore, const Array *array)
{
    if (!array || !array->Size()) {
        // Dump that array does not exist
        save_restore.SaveInteger(0);
    } else {
        // Dump array size
        save_restore.SaveInteger(array->Size());

        // Dump elements appropriately
        VhdlTreeNode *node;
        int i;
        FOREACH_ARRAY_ITEM(array, i, node) {
            // NOTE: a NULL array element is allowed!!!
            SaveNodePtr(save_restore, node);
        }
    }
}

// static
Array *
VhdlTreeNode::RestoreArray(SaveRestore &save_restore)
{
    // Restore array size
    unsigned array_size = save_restore.RestoreInteger();

    if (array_size) {
        Array *array = new Array(array_size);  // LON - might have to adjust array_size for efficiency!

        // Restore elements
        // RD: elements are VhdlTreeNode, not VhdlNode, because if anyone uses multiple inheritance on VhdlTreeNode, we will crash.
        VhdlTreeNode *node;
        for (unsigned i = 0; i < array_size; i++) {
            // NOTE: a NULL array element is allowed!!!
            node = RestoreNodePtr(save_restore);
            array->Insert(node);
        }

        return array;
    }

    return 0;
}

/* ----------------------------------------- */

// static
void
VhdlTreeNode::SaveSet(SaveRestore &save_restore, const Set *set)
{
    if (!set || !set->Size()) {
        // Dump that set does not exist
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(set->Size());

        SetIter si;
        VhdlTreeNode *node;
        // Set is assumed to be a POINTER_HASH
        FOREACH_SET_ITEM(set, si, &node) {
            //VERIFIC_ASSERT(node);  // LON - remove later
            SaveNodePtr(save_restore, node);
        }
    }
}

// static
Set *
VhdlTreeNode::RestoreSet(SaveRestore &save_restore)
{
    // Restore set size
    unsigned set_size = save_restore.RestoreInteger();

    if (set_size) {
        // Restore elements.
        // Set is assumed to be a POINTER_HASH
        Set *set = new Set(POINTER_HASH, set_size);    // LON - might have to adjust set_size for efficiency!
        // Note : elements are VhdlTreeNode, not VhdlNode, because if anyone uses multiple inheritance on VhdlTreeNode, we will crash.
        VhdlTreeNode *node;
        for (unsigned i = 0; i < set_size; i++) {
            node = RestoreNodePtr(save_restore);
            (void) set->Insert(node);
        }

        return set;
    }

    return 0;
}

/* ----------------------------------------- */

// This function is only for self-referencing STRING_HASHed maps!
// static
void
VhdlTreeNode::SaveMap(SaveRestore &save_restore, const Map *map)
{
    if (!map || !map->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(map->Size());

        // Map is assumed to be STRING_HASH or STRING_HASH_CASE_INSENSITIVE
        VhdlTreeNode *node ;
        MapIter mi;
        FOREACH_MAP_ITEM(map, mi, 0, &node) {
            //VERIFIC_ASSERT(node);  // LON - remove later
            SaveNodePtr(save_restore, node);
            // No need to dump the char* key, since its self-referencing
        }
    }
}

// static
Map *
VhdlTreeNode::RestoreIdDefMap(SaveRestore &save_restore)
{
    // Restore map size
    unsigned map_size = save_restore.RestoreInteger();

    if (map_size) {
        Map *map = new Map(STRING_HASH, map_size); // LON - might have to adjust map_size for efficiency!
                                                   // LON, STRING_HASH might cause a problem if STRING_HASH_CASE_INSENSITIVE is expected
        // Restore elements (VhdlNode* in this case)
        // Map is assumed to be STRING_HASH or STRING_HASH_CASE_INSENSITIVE
        VhdlIdDef *id_def;
        for (unsigned i = 0; i < map_size; i++) {
            id_def = (VhdlIdDef*)RestoreNodePtr(save_restore);
            if (id_def) {
                (void) map->Insert(id_def->Name(), id_def, 0 /* overwrite */, 1 /* forced-insert */);
            }
        }

        return map;
    }

    return 0;
}

/* ----------------------------------------- */

// static
void
VhdlTreeNode::SaveNodePtr(SaveRestore &save_restore, const VhdlTreeNode *node)
{
    if (node) {
        // If this instance is not registered, then register it and dump it
        if (!save_restore.IsRegistered(node)) {
            // Register pointer
            save_restore.RegisterPointer(node);
            // Dump object
            save_restore.SaveInteger(node->GetClassId());
            node->Save(save_restore);
        } else {
            unsigned long id = save_restore.Pointer2Id((const void*)node);
            if (id >= ID_VHDL_BEGIN_TABLE) {
                VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
                tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
                tmp_print_node.Error("This parse-tree is too large to be saved");
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return ;
            }
            save_restore.SaveInteger((unsigned)id);
        }
    } else {
        save_restore.SaveInteger(0);
    }
}

// static
VhdlTreeNode *
VhdlTreeNode::RestoreNodePtr(SaveRestore &save_restore)
{
    unsigned val = save_restore.RestoreInteger();

    if (val)  {
        // Check if this pointer 'id' is registered (against a pointer) already :
        VhdlTreeNode *node = (VhdlTreeNode*)save_restore.Id2Pointer(val);

        if (!node) {
            // not registered means that there is a new object coming up. Restore that.
            // in this case 'val' will be a ClassId for the type of object that comes up.
            node = VhdlTreeNode::CreateObject(save_restore, val);
        }

        return node;
    }

    return 0;
}

/* ----------------------------------------- */

// static
void
VhdlTreeNode::SaveScopePtr(SaveRestore &save_restore, const VhdlScope *scope, unsigned for_pkg_inst)
{
    if (scope) {
        // If this instance is not registered, then register it and dump it
        if (!save_restore.IsRegistered(scope)) {
            save_restore.RegisterPointer(scope);
            save_restore.SaveInteger(DATA_TO_FOLLOW);
            scope->Save(save_restore, for_pkg_inst);
        } else {
            unsigned long id = save_restore.Pointer2Id((const void*)scope);
            // If this doesn't hold, our ClassId approach breaks!
            if (id >= ID_VHDL_BEGIN_TABLE) {
                VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
                tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
                tmp_print_node.Error("This parse-tree is too large to be saved");
                save_restore.FileNotOk() ; // Disable SaveRestore object
                return ;
            }
            save_restore.SaveInteger((unsigned)id);
        }
    } else {
        save_restore.SaveInteger(0);
    }
}

// static
VhdlScope *
VhdlTreeNode::RestoreScopePtr(SaveRestore &save_restore, unsigned for_pkg_inst)
{
    VhdlScope *scope = 0 ;

    unsigned val = save_restore.RestoreInteger();
    switch(val)
    {
        case DATA_TO_FOLLOW:    // VhdlScope data to follow
            // Registering of the new object is done in the VhdlScope::VhdlScope(SaveRestore &save_restore)
            scope = new VhdlScope(save_restore, for_pkg_inst) ;
            if (!save_restore.IsOK()) {
                delete scope ;
                scope = 0 ;
            }
            break;

        case 0:  // NULL pointer
            break;

        default: // Valid pointer
            {
                scope = (VhdlScope*)save_restore.Id2Pointer(val);
                if (!scope) {
                    save_restore.FileNotOk(); // Disable SaveRestore object
                }
            }
            break;
    }

    return scope;
}

/* ----------------------------------------- */

// static
VhdlTreeNode *
VhdlTreeNode::CreateObject(SaveRestore &save_restore, unsigned class_id)
{
    // You can only create an object from a save_restore that is in restore mode!
    if (save_restore.IsSaving()) {
        VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
        tmp_print_node.Error("A VHDL parse-tree node is trying to be created from a SaveRestore object that is in save mode!");
        save_restore.FileNotOk(); // Disable SaveRestore object
        return 0;
    }

    VhdlTreeNode *node = 0;

    switch(class_id)
    {
        //case ID_VHDLGOTOAREA :
        //case ID_VHDLDATAFLOW :
        //case ID_VHDLWRITE :
        //    VERIFIC_ASSERT(0);

        // VhdlScope
        //case ID_VHDLLIBRARY :
        //case ID_VHDLSCOPE :
        //case ID_VHDL_FILE :
        //    VERIFIC_ASSERT(0);  // This should N-E-V-E-R get called!

        //case ID_VHDLNODE :
        //case ID_VHDLTREENODE :
        //    VERIFIC_ASSERT(0);  // Abstract class

        case ID_VHDLCOMMENTNODE :
            node = (new VhdlCommentNode(save_restore)); break;

        // VhdlConstraint & VhdlValue pertains to elaboration-specific code, so
        // these objects will not be saved/restored.

        // VhdlBindingIndication
        case ID_VHDLBINDINGINDICATION :
            node = (new VhdlBindingIndication(save_restore)); break;

        // VhdlBlockGenerics
        case ID_VHDLBLOCKGENERICS :
            node = (new VhdlBlockGenerics(save_restore)); break;

        // VhdlBlockPorts
        case ID_VHDLBLOCKPORTS :
            node = (new VhdlBlockPorts(save_restore)); break;

        // VhdlCaseStatementAlternative
        case ID_VHDLCASESTATEMENTALTERNATIVE :
            node = (new VhdlCaseStatementAlternative(save_restore)); break;

        // VhdlConditionalWaveform
        case ID_VHDLCONDITIONALWAVEFORM :
            node = (new VhdlConditionalWaveform(save_restore)); break;

        // VhdlConditionalExpression
        case ID_VHDLCONDITIONALEXPRESSION :
            node = (new VhdlConditionalExpression(save_restore)); break;
        // VhdlConfigurationItem
        //case ID_VHDLCONFIGURATIONITEM :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLBLOCKCONFIGURATION :
            node = (new VhdlBlockConfiguration(save_restore)); break;
        case ID_VHDLCOMPONENTCONFIGURATION :
            node = (new VhdlComponentConfiguration(save_restore)); break;

        // VhdlDeclarations
        //case ID_VHDLDECLARATION :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLALIASDECL :
            node = (new VhdlAliasDecl(save_restore)); break;
        case ID_VHDLATTRIBUTEDECL :
            node = (new VhdlAttributeDecl(save_restore)); break;
        case ID_VHDLATTRIBUTESPEC :
            node = (new VhdlAttributeSpec(save_restore)); break;
        case ID_VHDLCOMPONENTDECL :
            node = (new VhdlComponentDecl(save_restore)); break;
        case ID_VHDLCONFIGURATIONSPEC :
            node = (new VhdlConfigurationSpec(save_restore)); break;
        case ID_VHDLCONSTANTDECL :
            node = (new VhdlConstantDecl(save_restore)); break;
        case ID_VHDLDISCONNECTIONSPEC :
            node = (new VhdlDisconnectionSpec(save_restore)); break;
        case ID_VHDLFILEDECL :
            node = (new VhdlFileDecl(save_restore)); break;
        case ID_VHDLFULLTYPEDECL :
            node = (new VhdlFullTypeDecl(save_restore)); break;
        case ID_VHDLGROUPDECL :
            node = (new VhdlGroupDecl(save_restore)); break;
        case ID_VHDLSUBPROGINSTANTIATIONDECL :
            node = (new VhdlSubprogInstantiationDecl(save_restore)); break;
        case ID_VHDLINTERFACETYPEDECL :
            node = (new VhdlInterfaceTypeDecl(save_restore)); break;
        case ID_VHDLINTERFACESUBPROGDECL :
            node = (new VhdlInterfaceSubprogDecl(save_restore)); break;
        case ID_VHDLINTERFACEPACKAGEDECL :
            node = (new VhdlInterfacePackageDecl(save_restore)); break;
        case ID_VHDLGROUPTEMPLATEDECL :
            node = (new VhdlGroupTemplateDecl(save_restore)); break;
        case ID_VHDLINCOMPLETETYPEDECL :
            node = (new VhdlIncompleteTypeDecl(save_restore)); break;
        case ID_VHDLINTERFACEDECL :
            node = (new VhdlInterfaceDecl(save_restore)); break;
        case ID_VHDLLIBRARYCLAUSE :
            node = (new VhdlLibraryClause(save_restore)); break;
        case ID_VHDLSIGNALDECL :
            node = (new VhdlSignalDecl(save_restore)); break;
        case ID_VHDLSUBPROGRAMBODY :
            node = (new VhdlSubprogramBody(save_restore)); break;
        case ID_VHDLSUBPROGRAMDECL :
            node = (new VhdlSubprogramDecl(save_restore)); break;
        case ID_VHDLSUBTYPEDECL :
            node = (new VhdlSubtypeDecl(save_restore)); break;
        case ID_VHDLUSECLAUSE :
            node = (new VhdlUseClause(save_restore)); break;
        case ID_VHDLCONTEXTREFERENCE :
            node = (new VhdlContextReference(save_restore)); break;
        case ID_VHDLVARIABLEDECL :
            node = (new VhdlVariableDecl(save_restore)); break;

        // VhdlDelayMechanism
        //case ID_VHDLDELAYMECHANISM :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLINERTIALDELAY :
            node = (new VhdlInertialDelay(save_restore)); break;
        case ID_VHDLTRANSPORT :
            node = (new VhdlTransport(save_restore)); break;

        // VhdlDesignUnits
        //case ID_VHDLDESIGNUNIT :
        //    VERIFIC_ASSERT(0);  // Abstract class
        //case ID_VHDLPRIMARYUNIT :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLCONFIGURATIONDECL :
            node = (new VhdlConfigurationDecl(save_restore)); break;
        case ID_VHDLENTITYDECL :
            node = (new VhdlEntityDecl(save_restore)); break;
        case ID_VHDLPACKAGEDECL :
            node = (new VhdlPackageDecl(save_restore)); break;
        case ID_VHDLCONTEXTDECL :
            node = (new VhdlContextDecl(save_restore)); break;
        case ID_VHDLPACKAGEINSTANTIATIONDECL :
            node = (new VhdlPackageInstantiationDecl(save_restore)); break;
        //case ID_VHDLSECONDARYUNIT :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLARCHITECTUREBODY :
            node = (new VhdlArchitectureBody(save_restore)); break;
        case ID_VHDLPACKAGEBODY :
            node = (new VhdlPackageBody(save_restore)); break;

        // VhdlDiscreteRange
        //case ID_VHDLDISCRETERANGE :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLBOX :
            node = (new VhdlBox(save_restore)); break;
        //case ID_VHDLEXPRESSION :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLAGGREGATE :
            node = (new VhdlAggregate(save_restore)); break;
        case ID_VHDLALLOCATOR :
            node = (new VhdlAllocator(save_restore)); break;
        case ID_VHDLASSOCELEMENT :
            node = (new VhdlAssocElement(save_restore)); break;
        case ID_VHDLINERTIALELEMENT :
            node = (new VhdlInertialElement(save_restore)); break;
        case ID_VHDLRECRESFUNCTIONELEMENT :
            node = (new VhdlRecResFunctionElement(save_restore)); break;
        case ID_VHDLELEMENTASSOC :
            node = (new VhdlElementAssoc(save_restore)); break;
        case ID_VHDLOPERATOR :
            node = (new VhdlOperator(save_restore)); break;
        case ID_VHDLQUALIFIEDEXPRESSION :
            node = (new VhdlQualifiedExpression(save_restore)); break;
        //case ID_VHDLSUBTYPEINDICATION :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLEXPLICITSUBTYPEINDICATION :
            node = (new VhdlExplicitSubtypeIndication(save_restore)); break;
        //case ID_VHDLNAME :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLATTRIBUTENAME :
            node = (new VhdlAttributeName(save_restore)); break;
        //case ID_VHDLDESIGNATOR :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLALL :
            node = (new VhdlAll(save_restore)); break;
        case ID_VHDLCHARACTERLITERAL :
            node = (new VhdlCharacterLiteral(save_restore)); break;
        case ID_VHDLIDREF :
            node = (new VhdlIdRef(save_restore)); break;
        case ID_VHDLARRAYRESFUNCTION :
            node = (new VhdlArrayResFunction(save_restore)); break;
        case ID_VHDLRECORDRESFUNCTION :
            node = (new VhdlRecordResFunction(save_restore)); break;
        case ID_VHDLOTHERS :
            node = (new VhdlOthers(save_restore)); break;
        case ID_VHDLSTRINGLITERAL :
            node = (new VhdlStringLiteral(save_restore)); break;
        case ID_VHDLBITSTRINGLITERAL :
            node = (new VhdlBitStringLiteral(save_restore)); break;
        case ID_VHDLUNAFFECTED :
            node = (new VhdlUnaffected(save_restore)); break;
        case ID_VHDLENTITYASPECT :
            node = (new VhdlEntityAspect(save_restore)); break;
        case ID_VHDLINDEXEDNAME :
            node = (new VhdlIndexedName(save_restore)); break;
        case ID_VHDLINSTANTIATEDUNIT :
            node = (new VhdlInstantiatedUnit(save_restore)); break;
        case ID_VHDLEXTERNALNAME :
            node = (new VhdlExternalName(save_restore)); break;
        //case ID_VHDLLITERAL :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLINTEGER :
            node = (new VhdlInteger(save_restore)); break;
        case ID_VHDLNULL :
            node = (new VhdlNull(save_restore)); break;
        case ID_VHDLPHYSICALLITERAL :
            node = (new VhdlPhysicalLiteral(save_restore)); break;
        case ID_VHDLREAL :
            node = (new VhdlReal(save_restore)); break;
        case ID_VHDLOPEN :
            node = (new VhdlOpen(save_restore)); break;
        case ID_VHDLSELECTEDNAME :
            node = (new VhdlSelectedName(save_restore)); break;
        case ID_VHDLSIGNATUREDNAME :
            node = (new VhdlSignaturedName(save_restore)); break;
        case ID_VHDLWAVEFORMELEMENT :
            node = (new VhdlWaveformElement(save_restore)); break;
        case ID_VHDLDEFAULT :
            node = (new VhdlDefault(save_restore)); break;
        case ID_VHDLRANGE :
            node = (new VhdlRange(save_restore)); break;

        // VhdlElementDecl
        case ID_VHDLELEMENTDECL :
            node = (new VhdlElementDecl(save_restore)); break;

        // VhdlElsif
        case ID_VHDLELSIF :
            node = (new VhdlElsif (save_restore)); break;

        // VhdlEntityClassEntry
        case ID_VHDLENTITYCLASSENTRY :
            node = (new VhdlEntityClassEntry(save_restore)); break;

        // VhdlFileOpenInfo
        case ID_VHDLFILEOPENINFO :
            node = (new VhdlFileOpenInfo(save_restore)); break;

        // VhdlIdDefs
        //case ID_VHDLIDDEF :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLPROTECTEDTYPEID :
            node = (new VhdlProtectedTypeId(save_restore)); break;
        case ID_VHDLPROTECTEDTYPEBODYID :
            node = (new VhdlProtectedTypeBodyId(save_restore)); break;
        case ID_VHDLALIASID :
            node = (new VhdlAliasId(save_restore)); break;
        case ID_VHDLARCHITECTUREID :
            node = (new VhdlArchitectureId(save_restore)); break;
        case ID_VHDLATTRIBUTEID :
            node = (new VhdlAttributeId(save_restore)); break;
        case ID_VHDLCOMPONENTID :
            node = (new VhdlComponentId(save_restore)); break;
        case ID_VHDLCONFIGURATIONID :
            node = (new VhdlConfigurationId(save_restore)); break;
        case ID_VHDLCONSTANTID :
            node = (new VhdlConstantId(save_restore)); break;
        case ID_VHDLELEMENTID :
            node = (new VhdlElementId(save_restore)); break;
        case ID_VHDLENTITYID :
            node = (new VhdlEntityId(save_restore)); break;
        case ID_VHDLENUMERATIONID :
            node = (new VhdlEnumerationId(save_restore)); break;
        case ID_VHDLFILEID :
            node = (new VhdlFileId(save_restore)); break;
        case ID_VHDLGROUPID :
            node = (new VhdlGroupId(save_restore)); break;
        case ID_VHDLGROUPTEMPLATEID :
            node = (new VhdlGroupTemplateId(save_restore)); break;
        case ID_VHDLINTERFACEID :
            node = (new VhdlInterfaceId(save_restore)); break;
        case ID_VHDLLABELID :
            node = (new VhdlLabelId(save_restore)); break;
        case ID_VHDLBLOCKID :
            node = (new VhdlBlockId(save_restore)); break;
        case ID_VHDLLIBRARYID :
            node = (new VhdlLibraryId(save_restore)); break;
        case ID_VHDLPACKAGEBODYID :
            node = (new VhdlPackageBodyId(save_restore)); break;
        case ID_VHDLCONTEXTID :
            node = (new VhdlContextId(save_restore)) ; break ; // VIPER #7010: Added restoring of context id
        case ID_VHDLPACKAGEID :
            node = (new VhdlPackageId(save_restore)); break;
        case ID_VHDLPHYSICALUNITID :
            node = (new VhdlPhysicalUnitId(save_restore)); break;
        case ID_VHDLSIGNALID :
            node = (new VhdlSignalId(save_restore)); break;
        case ID_VHDLSUBPROGRAMID :
            node = (new VhdlSubprogramId(save_restore)); break;
        case ID_VHDLOPERATORID :
            node = (new VhdlOperatorId(save_restore)); break;
        case ID_VHDLSUBTYPEID :
            node = (new VhdlSubtypeId(save_restore)); break;
        case ID_VHDLTYPEID :
            node = (new VhdlTypeId(save_restore)); break;
        case ID_VHDLGENERICTYPEID :
            node = (new VhdlGenericTypeId(save_restore)); break;
        case ID_VHDLANONYMOUSTYPE :
            node = (new VhdlAnonymousType(save_restore)); break;
        case ID_VHDLUNIVERSALINTEGER :
            node = (new VhdlUniversalInteger(save_restore)); break;
        case ID_VHDLUNIVERSALREAL :
            node = (new VhdlUniversalReal(save_restore)); break;
        case ID_VHDLVARIABLEID :
            node = (new VhdlVariableId(save_restore)); break;
        case ID_VHDLPACKAGEINSTELEMENT :
            node = (new VhdlPackageInstElement(save_restore)); break;
        // VhdlIterSchem
        //case ID_VHDLITERSCHEME :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLFORSCHEME :
            node = (new VhdlForScheme(save_restore)); break;
        case ID_VHDLIFSCHEME :
            node = (new VhdlIfScheme(save_restore)); break;
        case ID_VHDLWHILESCHEME :
            node = (new VhdlWhileScheme(save_restore)); break;
        case ID_VHDLELSIFELSESCHEME :
            node = (new VhdlElsifElseScheme(save_restore)); break;
        case ID_VHDLCASEITEMSCHEME :
            node = (new VhdlCaseItemScheme(save_restore)); break;

        // VhdlOptions
        case ID_VHDLOPTIONS :
            node = (new VhdlOptions(save_restore)); break;

        // VhdlPhysicalUnitDecl
        case ID_VHDLPHYSICALUNITDECL :
            node = (new VhdlPhysicalUnitDecl(save_restore)); break;

        // VhdlSelectedWaveform
        case ID_VHDLSELECTEDWAVEFORM :
            node = (new VhdlSelectedWaveform(save_restore)); break;

        // VhdlSelectedExpression
        case ID_VHDLSELECTEDEXPRESSION :
            node = (new VhdlSelectedExpression(save_restore)); break;

        // VhdlSignature
        case ID_VHDLSIGNATURE :
            node = (new VhdlSignature(save_restore)); break;

        // VhdlSpecification
        //case ID_VHDLSPECIFICATION :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLCOMPONENTSPEC :
            node = (new VhdlComponentSpec(save_restore)); break;
        case ID_VHDLENTITYSPEC :
            node = (new VhdlEntitySpec(save_restore)); break;
        case ID_VHDLFUNCTIONSPEC :
            node = (new VhdlFunctionSpec(save_restore)); break;
        case ID_VHDLGUARDEDSIGNALSPEC :
            node = (new VhdlGuardedSignalSpec(save_restore)); break;
        case ID_VHDLPROCEDURESPEC :
            node = (new VhdlProcedureSpec(save_restore)); break;

        // VhdlStatement
        //case ID_VHDLSTATEMENT :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLASSERTIONSTATEMENT :
            node = (new VhdlAssertionStatement(save_restore)); break;
        case ID_VHDLBLOCKSTATEMENT :
            node = (new VhdlBlockStatement(save_restore)); break;
        case ID_VHDLCASESTATEMENT :
            node = (new VhdlCaseStatement(save_restore)); break;
        case ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT :
            node = (new VhdlComponentInstantiationStatement(save_restore)); break;
        case ID_VHDLCONDITIONALSIGNALASSIGNMENT :
            node = (new VhdlConditionalSignalAssignment(save_restore)); break;
        case ID_VHDLCONDITIONALFORCEASSIGNMENTSTATEMENT :
            node = (new VhdlConditionalForceAssignmentStatement(save_restore)); break;
        case ID_VHDLCONDITIONALVARIABLEASSIGNMENTSTATEMENT :
            node = (new VhdlConditionalVariableAssignmentStatement(save_restore)); break;
        case ID_VHDLEXITSTATEMENT :
            node = (new VhdlExitStatement(save_restore)); break;
        case ID_VHDLGENERATESTATEMENT :
            node = (new VhdlGenerateStatement(save_restore)); break;
        case ID_VHDLIFSTATEMENT :
            node = (new VhdlIfStatement(save_restore)); break;
        case ID_VHDLLOOPSTATEMENT :
            node = (new VhdlLoopStatement(save_restore)); break;
        case ID_VHDLNEXTSTATEMENT :
            node = (new VhdlNextStatement(save_restore)); break;
        case ID_VHDLNULLSTATEMENT :
            node = (new VhdlNullStatement(save_restore)); break;
        case ID_VHDLPROCEDURECALLSTATEMENT :
            node = (new VhdlProcedureCallStatement(save_restore)); break;
        case ID_VHDLPROCESSSTATEMENT :
            node = (new VhdlProcessStatement(save_restore)); break;
        case ID_VHDLREPORTSTATEMENT :
            node = (new VhdlReportStatement(save_restore)); break;
        case ID_VHDLRETURNSTATEMENT :
            node = (new VhdlReturnStatement(save_restore)); break;
        case ID_VHDLSELECTEDSIGNALASSIGNMENT :
            node = (new VhdlSelectedSignalAssignment(save_restore)); break;
        case ID_VHDLSELECTEDVARIABLEASSIGNMENTSTATEMENT :
            node = (new VhdlSelectedVariableAssignmentStatement(save_restore)); break;
        case ID_VHDLSELECTEDFORCEASSIGNMENT :
            node = (new VhdlSelectedForceAssignment(save_restore)); break;
        case ID_VHDLSIGNALASSIGNMENTSTATEMENT :
            node = (new VhdlSignalAssignmentStatement(save_restore)); break;
        case ID_VHDLFORCEASSIGNMENTSTATEMENT :
            node = (new VhdlForceAssignmentStatement(save_restore)); break;
        case ID_VHDLRELEASEASSIGNMENTSTATEMENT :
            node = (new VhdlReleaseAssignmentStatement(save_restore)); break;
        case ID_VHDLVARIABLEASSIGNMENTSTATEMENT :
            node = (new VhdlVariableAssignmentStatement(save_restore)); break;
        case ID_VHDLWAITSTATEMENT :
            node = (new VhdlWaitStatement(save_restore)); break;
        case ID_VHDLIFELSIFGENERATESTATEMENT :
            node = (new VhdlIfElsifGenerateStatement(save_restore)); break;
        case ID_VHDLCASEGENERATESTATEMENT :
            node = (new VhdlCaseGenerateStatement(save_restore)); break;

        // VhdlTypeDef
        //case ID_VHDLTYPEDEF :
        //    VERIFIC_ASSERT(0);  // Abstract class
        case ID_VHDLPROTECTEDTYPEDEF:
            node = (new VhdlProtectedTypeDef(save_restore)); break;
        case ID_VHDLPROTECTEDTYPEDEFBODY:
            node = (new VhdlProtectedTypeDefBody(save_restore)); break;
        case ID_VHDLACCESSTYPEDEF :
            node = (new VhdlAccessTypeDef(save_restore)); break;
        case ID_VHDLARRAYTYPEDEF :
            node = (new VhdlArrayTypeDef(save_restore)); break;
        case ID_VHDLENUMERATIONTYPEDEF :
            node = (new VhdlEnumerationTypeDef(save_restore)); break;
        case ID_VHDLFILETYPEDEF :
            node = (new VhdlFileTypeDef(save_restore)); break;
        case ID_VHDLPHYSICALTYPEDEF :
            node = (new VhdlPhysicalTypeDef(save_restore)); break;
        case ID_VHDLRECORDTYPEDEF :
            node = (new VhdlRecordTypeDef(save_restore)); break;
        case ID_VHDLSCALARTYPEDEF :
            node = (new VhdlScalarTypeDef(save_restore)); break;
        default: {
            save_restore.FileNotOk(); // Disable SaveRestore object
            return 0;
        }
    }

    // If an error has occurred, delete appropriately
    if (!save_restore.IsOK()) {
        delete node;
        node = 0;
    }

    return node;
}

//--------------------------------------------------------------
// #included from VhdlTreeNode.h
//--------------------------------------------------------------

void
VhdlTreeNode::Save(SaveRestore &save_restore) const
{
    if (IsTickProtectedNode() && !save_restore.HasProtectionObject()) Warning("binary dump database must be encrypted to secure `protect RTL") ;

    // Save base class's members
    // None - VhdlNode has no data to save.

    // Dump data members appropriately.

    // The _linefile member, regardless of the VHDL_USE_STARTING_LINEFILE_INFO
    // switch setting, should be directly dumped.  Don't call Linefile() here.
    save_restore.SaveLinefile(_linefile);

    // Save comments array
    if (RuntimeFlags::GetVar("vhdl_preserve_comments")) SaveArray(save_restore, GetComments());
}

VhdlTreeNode::VhdlTreeNode(SaveRestore &save_restore)
    // Use the default VhdlNode() constructor here, because there is nothing to restore.
  : VhdlNode(),
    _linefile(0)
{
    // Registering of the object occurs here, AND ONLY here!!!
    save_restore.RegisterPointer(this);

    // Restore (and register) the linefile after registering myself, because when saving,
    // we registered myself (from SaveNodePtr) before saving (registering) the linefile:
    _linefile = save_restore.RestoreLinefile();

    // Restore parse-tree comments if stream contains them.
    // Determine if the stream was saved with VHDL_PRESERVE_COMMENTS enabled.
    if (save_restore.GetUserFlags() & PRESERVE_COMMENTS_ENABLED) {
        // Restore comment array
        Array *comment_arr = RestoreArray(save_restore);
        if (RuntimeFlags::GetVar("vhdl_preserve_comments")) {
            if (comment_arr) AddComments(comment_arr) ;
        } else {
            // If VHDL_PRESERVE_COMMENTS is not enabled, deallocate what was restored.
            unsigned i ;
            VhdlCommentNode *node ;
            FOREACH_ARRAY_ITEM(comment_arr, i, node) {
                // We need to unregister pointer before deleting
                save_restore.UnRegister(node) ;
                delete node ;
            }
            delete comment_arr ;
        }
    }
}

/* ---------------------------- */

void
VhdlCommentNode::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveString(_str) ;
}

VhdlCommentNode::VhdlCommentNode(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _str(0)
{
    // Restore data members appropriately
    _str = save_restore.RestoreString() ;
}

//--------------------------------------------------------------
// #included from VhdlUnits.h
//--------------------------------------------------------------

void
VhdlDesignUnit::Save(SaveRestore &save_restore) const
{
    VERIFIC_ASSERT(_id) ;

    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // Print out some user info
    if (_id->IsConfiguration())      { tmp_print_node.Info("Binary saving %s '%s'", "configuration(decl)", _id->Name()); }
    else if (_id->IsEntity())        { tmp_print_node.Info("Binary saving %s '%s'", "entity(decl)", _id->Name()); }
    else if (_id->IsPackage())       { tmp_print_node.Info("Binary saving %s '%s'", "package(decl)", _id->Name()); }
    else if (_id->IsArchitecture())  { tmp_print_node.Info("Binary saving %s '%s'", "architecture(body)", _id->Name()); }
    else if (_id->IsPackageBody())   { tmp_print_node.Info("Binary saving %s '%s'", "package(body)", _id->Name()); }
    else if (_id->IsContext())       { tmp_print_node.Info("Binary saving %s '%s'", "context(decl)", _id->Name()); } // VIPER #7010: Added proper message
    else                             { tmp_print_node.Info("Binary saving %s '%s'", "unknown type of design unit", _id->Name()); }

    // VIPER #6937 : Save analysis dialect
    save_restore.SaveInteger(_analysis_mode) ;

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _id);
    SaveArray(save_restore, _context_clause);

    // Dump extra fields :
    save_restore.SaveLong(_timestamp) ;

    // Bit pack the following
    unsigned flags = 0 ;
    // Set by the conversion routine to indicate that this unit is for Verilog module. By default set to '0' value.
    flags = _is_verilog_module ;
    //save_restore.SaveInteger(_is_elaborated);         // Don't need to save this
    //save_restore.SaveInteger(_is_static_elaborated);  // Don't need to save this
    save_restore.SaveInteger(flags) ;

    save_restore.SaveString(_orig_design_unit) ;
}

VhdlDesignUnit::VhdlDesignUnit(SaveRestore &save_restore, unsigned do_not_restore_members)
  : VhdlDeclaration(save_restore, do_not_restore_members),
    _id(0),
    _local_scope(0),
    _context_clause(0),
    _timestamp(0),
    _is_elaborated(0),
    _is_static_elaborated(0),
// Set by the conversion routine to indicate that this unit is for Verilog module. By default set to '0' value.
    _is_verilog_module(0),
    _affected_by_path_name(0),
    _analysis_mode(0),
    _orig_design_unit(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    , _is_already_processed(0) // VIPER #6896: flag on package that is already elaborated
#endif
{
    if ((save_restore.GetVersion() >= 0x0ab00031) && do_not_restore_members) return ;
    // VIPER #6937 : After restoring _analysis_mode, set the global field
    if (save_restore.GetVersion() >= 0x0ab0002f) {
        _analysis_mode = save_restore.RestoreInteger() ;
        vhdl_file::SetAnalysisMode(_analysis_mode) ;
    } else {
        // For vdbs with older version, set default analysis mode
        _analysis_mode = vhdl_file::GetAnalysisMode() ;
    }
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);

    // Do a validity check here.  If _id is not a design unit, then this means vdb is either
    // stale or erroneous.  Disable save/restore stream so that we will issue an appropriate
    // error later. (VIPER #2048)
    if (!_id || !_id->IsDesignUnit()) {
        save_restore.FileNotOk() ;
    }

    _context_clause = RestoreArray(save_restore);

    // Restore extra fields :
    _timestamp = (save_restore.GetVersion() >= 0x0ab0000c) ? (unsigned long)save_restore.RestoreLong() : (unsigned long)save_restore.RestoreInteger() ;

    if (save_restore.GetVersion() >= 0x0ab0000a) {
        unsigned flags = save_restore.RestoreInteger() ;
        // _is_elaborated = 0;          // Always is zero upon restore (via initializer list).  This member was not saved
        // _is_static_elaborated = 0;   // Always is zero upon restore (via initializer list).  This member was not saved
        // Set by the conversion routine to indicate that this unit is for
        // Verilog module. By default set to '0' value. Set this to flag.
        _is_verilog_module = flags  ;
        (void) flags ;
    }

    if (save_restore.GetVersion() >= 0x0ab0000b) {
        _orig_design_unit = save_restore.RestoreString() ;
    }
}

/* ---------------------------- */

void
VhdlPrimaryUnit::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignUnit::Save(save_restore);

    // Dump data members appropriately
    unsigned compile_as_blackbox = 0 ;
    compile_as_blackbox = _compile_as_blackbox ;
    save_restore.SaveChar((char)compile_as_blackbox); // Dump of this flag needed for some applications (such as VIPER 7670).

    SaveMap(save_restore, &_secondary_units);

    // Dump library name.  Will need to reassign on restore side.
    //save_restore.SaveString(_owner->Name());
    //save_restore.SaveInteger(0);   // VhdlLibrary*
}

VhdlPrimaryUnit::VhdlPrimaryUnit(SaveRestore &save_restore, unsigned do_not_restore_members)
  : VhdlDesignUnit(save_restore, do_not_restore_members),
    _compile_as_blackbox(0),
    _secondary_units(STRING_HASH),
    _owner(0)
    , _process_stmt_replaced(0)
{
    if ((save_restore.GetVersion() >= 0x0ab00031) && do_not_restore_members) return ;
    unsigned compile_as_blackbox ;
    compile_as_blackbox = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreChar() : save_restore.RestoreInteger();
    _compile_as_blackbox = compile_as_blackbox;

    // Restore _secondary_units
    // Clear out contents, just to be sure
    _secondary_units.Reset();

    // Restore map size
    unsigned map_size = save_restore.RestoreInteger();

    if (map_size) {
        // Restore elements (VhdlSecondaryUnit* in this case)
        VhdlSecondaryUnit *sec_unit;
        for (unsigned i = 0; i < map_size; i++) {
            sec_unit = (VhdlSecondaryUnit*)RestoreNodePtr(save_restore);
            // If an error has occurred, exit immediately
            if (!save_restore.IsOK()) {
                delete sec_unit ;
                return ;
            }
            //VERIFIC_ASSERT(sec_unit);   // LON - remove later
            if (sec_unit) { (void) _secondary_units.Insert(sec_unit->Name(), sec_unit, 0 /* overwrite */, 1 /* forced-insert */); }
        }
    }

    // Get library name
    //char *lib_name;
    //RestoreString(save_restore, &lib_name);
    //_owner = vhdl_file::GetLibrary(lib_name);
    //VERIFIC_ASSERT(_owner);
}

/* ---------------------------- */

void
VhdlConfigurationDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlPrimaryUnit::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _entity_name);
    SaveArray(save_restore, _decl_part);
    SaveNodePtr(save_restore, _block_configuration);
}

VhdlConfigurationDecl::VhdlConfigurationDecl(SaveRestore &save_restore)
  : VhdlPrimaryUnit(save_restore),
    _entity_name(0),
    _decl_part(0),
    _block_configuration(0)
{
    // Restore data members appropriately
    _entity_name = (VhdlIdRef*)RestoreNodePtr(save_restore);
    _decl_part = RestoreArray(save_restore);
    _block_configuration = (VhdlBlockConfiguration*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlEntityDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlPrimaryUnit::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _generic_clause);
    SaveArray(save_restore, _port_clause);
    SaveArray(save_restore, _decl_part);
    SaveArray(save_restore, _statement_part);
}

VhdlEntityDecl::VhdlEntityDecl(SaveRestore &save_restore)
  : VhdlPrimaryUnit(save_restore),
    _generic_clause(0),
    _port_clause(0),
    _decl_part(0),
    _statement_part(0)
{
    // Restore data members appropriately
    _generic_clause = RestoreArray(save_restore);
    _port_clause = RestoreArray(save_restore);
    _decl_part = RestoreArray(save_restore);
    _statement_part = RestoreArray(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlPackageDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlPrimaryUnit::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _generic_clause) ;
    SaveArray(save_restore, _generic_map_aspect) ;
    SaveArray(save_restore, _decl_part);
}

VhdlPackageDecl::VhdlPackageDecl(SaveRestore &save_restore, unsigned do_not_restore_members)
  : VhdlPrimaryUnit(save_restore, do_not_restore_members),
    _generic_clause(0),
    _generic_map_aspect(0),
    _decl_part(0),
    _container(0) // No need to save/restore : Need to set from container
{
    if ((save_restore.GetVersion() >= 0x0ab00031) && do_not_restore_members) return ;
    // Restore data members appropriately
    if (save_restore.GetVersion() >= 0x0ab00019) {
         // Vhdl 2008 LRM section 4.7
        _generic_clause = RestoreArray(save_restore) ;
        _generic_map_aspect = RestoreArray(save_restore) ;
    }
    _decl_part = RestoreArray(save_restore);

    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlContextDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlPrimaryUnit::Save(save_restore);
}

VhdlContextDecl::VhdlContextDecl(SaveRestore &save_restore)
  : VhdlPrimaryUnit(save_restore)
{
}

/* ---------------------------- */
// From version 0x0ab00031 this class is no longer backward compatible
void
VhdlPackageInstantiationDecl::Save(SaveRestore &save_restore) const
{
    // Donot save base class's members, we will create them always. So
    // call 'Save' of VhdlTreeNode to save linefile information
    VhdlTreeNode::Save(save_restore) ;

    SaveScopePtr(save_restore, _inst_scope) ;
    SaveArray(save_restore, _context_clause) ;
    SaveScopePtr(save_restore, _local_scope, 1);
    // Save base class member '_id' here
    SaveNodePtr(save_restore, _id) ;
    // Save Uninstantiated package name
    SaveNodePtr(save_restore, _uninst_package_name);
    // We have not saved the _this_scope of _local_scope for package instance
    // So register the iddefs of _local_scope now
    if (_local_scope && !_local_scope->RegisterIdDefs(save_restore)) {
        save_restore.FileNotOk(); // Disable SaveRestore object
        return ;
    }
    // Also register the identifiers of _initial_pkg. For selected names, these
    // can be used directly in container design
    VhdlScope *init_pkg_scope = _initial_pkg ? _initial_pkg->LocalScope(): 0 ;
    if (init_pkg_scope && !init_pkg_scope->RegisterIdDefs(save_restore)) {
        save_restore.FileNotOk(); // Disable SaveRestore object
        return ;
    }
    SaveArray(save_restore, _generic_map_aspect) ;
    if (!_generic_types || !_generic_types->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(_generic_types->Size());

        VhdlTreeNode *node ;
        VhdlTreeNode *key ;
        MapIter mi;
        FOREACH_MAP_ITEM(_generic_types, mi, &key, &node) {
            SaveNodePtr(save_restore, key);
            SaveNodePtr(save_restore, node);
        }
    }
}
VhdlPackageInstantiationDecl::VhdlPackageInstantiationDecl(SaveRestore &save_restore)
    : VhdlPrimaryUnit(save_restore, 1/* do not restore members*/),
    _inst_scope(0),
    _uninst_package_name(0),
    _generic_map_aspect(0),
    _generic_types(0),
    _initial_pkg(0)
    ,_elab_pkg(0)
{
    _inst_scope = RestoreScopePtr(save_restore) ;
    _context_clause = RestoreArray(save_restore) ;
    _local_scope = RestoreScopePtr(save_restore, 1) ;
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore) ;
    _uninst_package_name = (VhdlName*)RestoreNodePtr(save_restore) ;
    VhdlIdDef *uninstantiated_pkg_id = _id ? _id->GetUninstantiatedPackageId(): 0 ;
    InlineUninstantiatedPackage(uninstantiated_pkg_id, _inst_scope) ;
    if (_local_scope && uninstantiated_pkg_id) _local_scope->Using(uninstantiated_pkg_id->LocalScope()) ;
    // We have not saved the _this_scope of _scope for package instance
    // So register the iddefs of _scope now
    if (_local_scope && !_local_scope->RegisterIdDefs(save_restore)) {
        save_restore.FileNotOk(); // Disable SaveRestore object
        return ;
    }
    // Also register the identifiers of _initial_pkg
    VhdlScope *init_pkg_scope = _initial_pkg ? _initial_pkg->LocalScope(): 0 ;
    if (init_pkg_scope && !init_pkg_scope->RegisterIdDefs(save_restore)) {
        save_restore.FileNotOk(); // Disable SaveRestore object
        return ;
    }
    _generic_map_aspect = RestoreArray(save_restore) ;
    // VIPER #7514 : Type infer the association list to set proper actual id
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _inst_scope ;
    if (_id && _generic_map_aspect) _id->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;
    _present_scope = old_scope ;
    // Restore map size
    unsigned map_size = save_restore.RestoreInteger();

    if (map_size) {
        _generic_types = new Map(POINTER_HASH, map_size);
        // Restore elements (VhdlNode* in this case)
        VhdlIdDef *id_def;
        VhdlIdDef *actual_id ;
        for (unsigned i = 0; i < map_size; i++) {
            id_def = (VhdlIdDef*)RestoreNodePtr(save_restore);
            actual_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
            (void) _generic_types->Insert(id_def, id_def ? id_def->ActualType(): actual_id);
        }
    }
}
/* ---------------------------- */

void
VhdlSecondaryUnit::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignUnit::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _owner);
}

VhdlSecondaryUnit::VhdlSecondaryUnit(SaveRestore &save_restore)
  : VhdlDesignUnit(save_restore),
    _owner(0)
{
    // Restore data members appropriately
    _owner = (VhdlPrimaryUnit*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlArchitectureBody::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSecondaryUnit::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _entity_name);
    SaveArray(save_restore, _decl_part);
    SaveArray(save_restore, _statement_part);
}

VhdlArchitectureBody::VhdlArchitectureBody(SaveRestore &save_restore)
  : VhdlSecondaryUnit(save_restore),
    _entity_name(0),
    _decl_part(0),
    _statement_part(0)
    ,_bind_instances(0)
    ,_bind_verilog_instances(0)
    ,_access_allocated_values(0)
{
    // Restore data members appropriately
    _entity_name = (VhdlIdRef*)RestoreNodePtr(save_restore);
    _decl_part = RestoreArray(save_restore);
    _statement_part = RestoreArray(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlPackageBody::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSecondaryUnit::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _decl_part);
}

VhdlPackageBody::VhdlPackageBody(SaveRestore &save_restore)
  : VhdlSecondaryUnit(save_restore),
    _decl_part(0)
{
    // Restore data members appropriately
    _decl_part = RestoreArray(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

//--------------------------------------------------------------
// #included from VhdlScope.h
//--------------------------------------------------------------

// The following NULL_PTR constant is only used by VhdlScope
static const unsigned NULL_PTR = 0xdeadbeef;  // NULL pointer value.

void
VhdlScope::SaveUsingSet(SaveRestore &save_restore) const
{
    Set *set = _using;

    if (!set || !set->Size()) {
        // Dump that set does not exist
        save_restore.SaveInteger(0);
    } else {
        save_restore.SaveInteger(set->Size());

        SetIter si;
        VhdlScope *scope;
        FOREACH_SET_ITEM(set, si, &scope) {
            //VERIFIC_ASSERT(scope); // LON - remove later
            VhdlTreeNode::SaveScopePtr(save_restore, scope);
        }
    }
}

void VhdlScope::RestoreUsingSet(SaveRestore &save_restore)
{
    // Restore _using set size
    unsigned set_size = save_restore.RestoreInteger();

    if (set_size) {
        // Restore elements (VhdlScope* in this case)
        VhdlScope *scope;
        for (unsigned i = 0; i < set_size; i++) {
            scope = VhdlTreeNode::RestoreScopePtr(save_restore);
            Using(scope);
        }
    }
}

/* ---------------------------- */
// Create a new dependent list considering _using list of this scope and _using
// list of secondary units :
void VhdlScope::CreateDependentList(Set &dependent_unit_scopes) const
{
    SetIter si ;
    VhdlScope *scope ;
    FOREACH_SET_ITEM(_using, si, &scope) {
        if (scope) (void) dependent_unit_scopes.Insert(scope) ;
    }
    VhdlPrimaryUnit *prim_unit = (_owner_id) ? _owner_id->GetPrimaryUnit(): 0 ;
    Map *sec_units = (prim_unit) ? prim_unit->AllSecondaryUnits(): 0 ;
    MapIter mi ;
    VhdlSecondaryUnit *sec_unit ;
    FOREACH_MAP_ITEM(sec_units, mi, 0, &sec_unit) {
        VhdlScope *s = (sec_unit) ? sec_unit->LocalScope(): 0 ;
        if (s) s->CreateDependentList(dependent_unit_scopes) ;
    }
}
void
VhdlScope::Save(SaveRestore &save_restore, unsigned for_pkg_inst) const
{
    // This object is registered in VhdlTreeNode::SaveScopePtr, right before this
    // function gets called.

    // Register external dependencies
    // VIPER #3757 : Primary unit can internally use identifier declared in a
    // package, but package is not included by use clause in entity. It is included in
    // secondary unit. In that case save/restore fails. So while registering the
    // dependent units of primary unit, register dependent units of secondary units too
    if (_owner_id && (_owner_id->IsEntity() || _owner_id->IsPackage())) {
        // Make an unique list of dependent units both from itself and sec units
        Set depends(POINTER_HASH) ;
        CreateDependentList(depends) ;
        if (!RegisterDependencies(save_restore, &depends)) return ;
    } else if (!RegisterDependencies(save_restore)) {
        return ; // An error occurred
    }

    // No base class's member data to save

    // Dump data members appropriately
    VhdlTreeNode::SaveScopePtr(save_restore, _upper);
    if (for_pkg_inst) return ; // Do not save _this_scope and other members
    VhdlTreeNode::SaveScopePtr(save_restore, _extended_decl_area);
    VhdlTreeNode::SaveMap(save_restore, _this_scope);

    // BEGIN _use_clauses SAVE -----------------------------
    // With VHDL_PERSISTENCE_VER_NUM of 0x0ab00002 we started to save the
    // _use_clauses map, even though it will cause a significant size increase
    // of the *.vdb file.  (VIPER #1865/1866)
    if (!_use_clauses || !_use_clauses->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(_use_clauses->Size());
        // Map is assumed to be STRING_HASH or STRING_HASH_CASE_INSENSITIVE
        VhdlTreeNode *node ;
        MapIter mi;
        FOREACH_MAP_ITEM(_use_clauses, mi, 0, &node) {
            // If the _use_clauses contains an identifier which has not been
            // registered by this point, then we would create restore issue
            // saving other design units.  Use clauses should ONLY save identifiers
            // if already have been registered.
            VhdlTreeNode::SaveNodePtr(save_restore, (save_restore.IsRegistered(node)) ? node : 0) ;
        }
    }
    // END _use_clauses SAVE -------------------------------

    // BEGIN _use_clause_units SAVE -----------------------------
    // With VHDL_PERSISTENCE_VER_NUM of 0x0ab00012 _use_clause_units Map is introduced and is binary save/restored :
    if (!_use_clause_units || !_use_clause_units->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(_use_clause_units->Size());
        // Map is assumed to be POINTER_HASH.
        // use_clause_units represents all use clauses in this scope "use (lib.)id.all" creates id->NULL entry, "use (lib.)id.name" creates an id->name entry.
        VhdlTreeNode *node ;
        char *name ;
        MapIter mi;
        FOREACH_MAP_ITEM(_use_clause_units, mi, &node, &name) {
            // IdDefs will be mostly external primary unit ids. Some local library ids also possible (for a "use libname.all" clause)
            // Use clauses should ONLY save identifiers if already have been registered.
            // We could assert on that here...?
            // Save the iddef pointer :
            VhdlTreeNode::SaveNodePtr(save_restore, (save_restore.IsRegistered(node)) ? node : 0) ;
            // Save the name field
            save_restore.SaveString(name) ;
        }
    }
    // END _use_clause_units SAVE -------------------------------

    VhdlTreeNode::SaveArray(save_restore, _predefined);
    SaveUsingSet(save_restore);

    //VhdlTreeNode::SaveSet(save_restore, _used_by); // Don't need to save this

    VhdlTreeNode::SaveNodePtr(save_restore, _owner_id);

    //VhdlTreeNode::SaveArray(save_restore, _elaboration_stack); // static
    //VhdlTreeNode::SaveMap(save_restore, _hidden_ids); // Don't need to save this

    // Save flags
    unsigned bit_flags = 0 ;
    bit_flags |= _transparent ; // _transparent flag (VIPER 2597)
    bit_flags |= _is_process_scope << 1 ;
    bit_flags |= _is_subprogram_scope << 2 ;
    bit_flags |= _wait_not_allowed << 3 ;
    bit_flags |= _has_wait_statement << 4 ;
    bit_flags |= _has_impure_access << 5 ;
    bit_flags |= _has_local_file_access << 6 ;
    save_restore.SaveInteger(bit_flags) ;
}

VhdlScope::VhdlScope(SaveRestore &save_restore, unsigned for_pkg_inst)
  : _upper(0),
    _this_scope(0),
    _use_clauses(0),
    _use_clause_units(0),
    _extended_decl_area(0),
    _predefined(0),
    _using(0),
    _used_by(0),
    _owner_id(0),
    _hidden_ids(0),
    // bit flags :
    _transparent(0),
    _is_process_scope(0),
    _is_subprogram_scope(0),
    _wait_not_allowed(0),
    _has_wait_statement(0),
    _has_impure_access(0),
    _has_local_file_access(0)
{
    save_restore.RegisterPointer(this);

    // Register external dependencies
    if (!RegisterDependencies(save_restore)) {
        save_restore.FileNotOk(); // Disable SaveRestore object
        return; // If an restore error has occurred, exit early
    }

    // Restore data members appropriately
    _upper = VhdlTreeNode::RestoreScopePtr(save_restore);
    if (for_pkg_inst) return ;
    _extended_decl_area = VhdlTreeNode::RestoreScopePtr(save_restore);

    // In the above line instead of seting a existing scope pointer,
    // a new scope is created sometimes. This leads to VIPER issue #1913
    // The following modification helps us to fix this viper issue
    if (_extended_decl_area && _extended_decl_area->GetOwner()) {
        // Do not reset '_extended_decl_area' if existing scope is restored
        if (_extended_decl_area->GetOwner()->LocalScope() != _extended_decl_area) {
            VhdlScope *restored_area = _extended_decl_area ;
            // Set '_extended_decl_area' from the local scope of owner (iddef)
            _extended_decl_area = _extended_decl_area->GetOwner()->LocalScope() ;
            save_restore.ReRegister(restored_area, _extended_decl_area) ;
            // 'restored_area' and '_extended_decl_area' has same owner
            // so make the owner of 'restored_area' null
            restored_area->SetOwner(0) ;
            // VIPER #4709: Both 'restored_area' and '_extended_decl_area' contain
            // Same predefined operators. Since the predefined operators are owned
            // by the scopes, we need to remove them from the 'restored_area' which
            // we are going to delete, otherwise we introduce memory corruptions:
            Array *predefined_operators = restored_area->GetPredefinedOperators() ;
            if (predefined_operators) predefined_operators->Reset() ; // Just reset the Array
            delete restored_area ; // to avoid memory leak
        }
    }

    // Restore the main Map of identifiers in this scope :
    _this_scope = VhdlTreeNode::RestoreIdDefMap(save_restore);

#ifdef VHDL_ID_SCOPE_BACKPOINTER
    // Issue 2716 : Each identifier requires a back-pointer to the scope it is contained in.
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (!id) continue ;
        id->SetOwningScope(this) ;
    }
#endif

    // Only if the *.vdb is of version 0x0ab00002 or greater, will we restore the _use_clauses map. (VIPER #1865/1866)
    if (save_restore.GetVersion() >= 0x0ab00002) {
        _use_clauses = VhdlTreeNode::RestoreIdDefMap(save_restore);
    }

    // Only if the *.vdb is of version 0x0ab00012 or greater, will we restore the _use_clause_units map. (VIPER #1865/1866)
    if (save_restore.GetVersion() >= 0x0ab00012) {
        // _use_clause_units is a (VhdlIdDef*)->(char*)name map.
        // That is NOT an IdDefMap, cannot use RestoreIdDefMap(). So, have to do this in-line :
        // Restore map size
        unsigned map_size = save_restore.RestoreInteger();
        if (map_size) {
            _use_clause_units = new Map(POINTER_HASH, map_size);

            // Restore elements (VhdlIdDef*)->(char*) assocs in this case.
            VhdlIdDef *id_def;
            char *name ;
            for (unsigned i = 0; i < map_size; i++) {
                id_def = (VhdlIdDef*)VhdlTreeNode::RestoreNodePtr(save_restore);
                name = save_restore.RestoreString() ;
                // insert assoc into Map :
                (void) _use_clause_units->Insert(id_def, name, 0, 1/*force insert*/);
            }
        }
    }

    // Restore the predefined identifiers (predefined operators) in this scope.
    _predefined = VhdlTreeNode::RestoreArray(save_restore);

    // The identifiers in _predefined are already stored in the _this_scope, so will have their back-pointer set.

    // Restore the _using set :
    RestoreUsingSet(save_restore);

    // Since we are restoring 'this' scope, it is not yet used by anyone else (_used_by is empty)..
    // _used_by will be filled when other units start using it.

    _owner_id = (VhdlIdDef*)VhdlTreeNode::RestoreNodePtr(save_restore);
    // Now quickly check that this _owner_id is indeed a type which can have a
    // scope.  If it can't have a scope, then we can assume that this is a stale,
    // or wrong, vdb, so we should eventually issue an error. (VIPER #2048)
    if (_owner_id && !_owner_id->IsType() && !_owner_id->IsLabel() && !_owner_id->IsDesignUnit() &&
        !_owner_id->IsComponent() && !_owner_id->IsSubprogram() && !_owner_id->IsSubtype() && !_owner_id->IsAlias()) {
        save_restore.FileNotOk() ; // Disable save/restore stream
        // We have to nullify _owner_id here, but don't delete it.
        _owner_id = 0 ;
    }

    //_elaboration_stack = VhdlTreeNode::RestoreArray(save_restore); // static
    //_hidden_ids = VhdlTreeNode::RestoreIdDefMap(save_restore); // Don't need to restore this

    // Only if the *.vdb is of version 0x0ab0000e or greater will bit flags be available :
    if (save_restore.GetVersion() >= 0x0ab0000e) {
        unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
        _transparent = bit_flags & 1 ;    // the _transparent flag. (VIPER #2597)
        _is_process_scope = (bit_flags >> 1) & 1 ;
        _is_subprogram_scope = (bit_flags >> 2) & 1 ;
        _wait_not_allowed = (bit_flags >> 3) & 1 ;
        _has_wait_statement = (bit_flags >> 4) & 1 ;
        _has_impure_access = (bit_flags >> 5) & 1 ;
        _has_local_file_access = (bit_flags >> 6) & 1 ;
    }
}

/* ---------------------------- */
//Viper 3757 : Add one argument to pass extended using unit list. Using list of
// primary unit is extended by adding using list of secondary units too.
unsigned VhdlScope::RegisterDependencies(SaveRestore &save_restore, Set *modified_using) const
{
    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor
    if (save_restore.IsSaving()) { // --- SAVING ---
        Set *using_list = (modified_using) ? modified_using : _using ;
        // Dump num of dependencies
        unsigned num_dependencies = (using_list) ? using_list->Size() : 0;
        save_restore.SaveInteger(num_dependencies);

        // Dump dependencies
        SetIter si;
        const VhdlScope *scope;
        FOREACH_SET_ITEM(using_list, si, &scope) {
            if (!scope->RegisterDependencies(save_restore)) {
                return 0;   // A save error occurred!
            }
        }

        scope = this;

        if (!save_restore.IsRegistered(scope)) {
            save_restore.RegisterPointer(scope);

            // While current scope does not belong to a design unit, go up scope chain
            Array names;
            while (scope->Upper()) {
                // Save IdDef's name to array
                names.Insert((const void*)(scope->GetOwner()->Name()));
                scope = scope->Upper();
            }

            VhdlDesignUnit *design_unit = scope->GetContainingDesignUnit()->GetDesignUnit();
            if (design_unit->IsPrimaryUnit()) {
                // Dump primary unit identifier:
                save_restore.SaveInteger((int)ID_VHDLPRIMARYUNIT);

                // Only dump primary unit name if it wasn't dumped above via the upper pointer
                if (!names.Size()) {   // Dump primary unit name
                    names.Insert((const void*)(design_unit->Name()));
                }

                // Dump library name
                names.Insert((void*)(design_unit->GetOwningLib()->Name()));
            } else if (design_unit->IsSecondaryUnit()) {
                // Dump secondary unit identifier:
                save_restore.SaveInteger((int)ID_VHDLSECONDARYUNIT);

                // Only dump the secondary unit name if it wasn't dumped above via the upper pointer
                if (!names.Size()) {   // Dump secondary unit name
                    names.Insert((const void*)(design_unit->Name()));
                }

                // Dump primary unit name
                names.Insert((const void*)(design_unit->GetOwningUnit()->Name()));

                // Dump library name
                names.Insert((void*)(design_unit->GetOwningLib()->Name()));
            } else {
                //VERIFIC_ASSERT(0);  // Logic bug if this gets executed
                tmp_print_node.Error("Registering Dependencies Error: unknown design unit type");
                save_restore.FileNotOk();
                return 0;
            }

            // Now dump the array in reverse order of how it filled
            save_restore.SaveInteger(names.Size());   // Dump size of path (num elements)
            char *name;
            int i;
            FOREACH_ARRAY_ITEM_BACK(&names, i, name) {
                save_restore.SaveString(name);
            }

            // Now dump the design unit's signature
            // VIPER #4301: Use new method Hash::NumCompare to calculate the signature:
            unsigned long signature = design_unit->CalculateSaveRestoreSignature(1 /* use num_compare */) ;
            save_restore.SaveLong(signature) ;

            // Register pointer but don't dump info.
            if (!save_restore.IsRegistered(design_unit))          { save_restore.RegisterPointer(design_unit); }

            // Register design unit id but don't dump info.
            if (!save_restore.IsRegistered(design_unit->Id()))    { save_restore.RegisterPointer(design_unit->Id()); }

            // Now register each VhdlIdDef in scope's use clauses
            if (!(design_unit->LocalScope()->RegisterIdDefs(save_restore))) {
                save_restore.FileNotOk(); // Disable SaveRestore object
                return 0 ; // Error occurred during save.
            }
        } else {
            save_restore.SaveInteger(NULL_PTR);   // This means the scope is already registered
        }
    } else {   // --- RESTORING ---
        VhdlLibrary *library;
        VhdlDesignUnit *design_unit;

        // Determine number design units this scope depends on
        unsigned num_dependencies = save_restore.RestoreInteger();

        // Register design units this scope depends on
        for (unsigned i = 0; i < num_dependencies; i++) {
            // Need to first recurse through dependencies first
            if (!RegisterDependencies(save_restore)){
                save_restore.FileNotOk(); // Disable SaveRestore object
                return 0;
            }
        }

        // Determine whether it's a primary or secondary unit
        unsigned ptr_type = save_restore.RestoreInteger() ;
        switch(ptr_type)
        {
            case NULL_PTR:
                // This means that this scope is already registered.
                design_unit = 0;
                break;

            case (unsigned)ID_VHDLPRIMARYUNIT:
                {
                    // Determine number of elements that describe IdDef path
                    int num_elements = save_restore.RestoreInteger();

                    // Retrieve library name and unit name
                    char *lib_name = save_restore.RestoreString();
                    char *unit_name = save_restore.RestoreString();

                    // Get Library
                    num_elements--;
                    library = vhdl_file::GetLibrary(lib_name);
                    if (!library) {
                        // Attempt first to do a restore of library/unit
                        // VIPER #4713 : Try to restore vhdl unit only when file exists.
                        if (!vhdl_file::Restore(lib_name, unit_name, 1 /*silent*/)) {
                            // Look into Verilog side :
                            if (!vhdl_file::GetVhdlUnitFromVerilog(lib_name, unit_name)) {
                                // Wasn't able to do a restore
                                tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The library", lib_name);
                                Strings::free(lib_name);
                                Strings::free(unit_name);
                                save_restore.FileNotOk(); // Disable SaveRestore object
                                return 0;
                            }
                        }

                        // Now get the library
                        library = vhdl_file::GetLibrary(lib_name);
                        //VERIFIC_ASSERT(library);   // ERROR: This should never get asserted!
                        if (!library){
                            tmp_print_node.Error("Registering Dependencies Error: library not defined");
                            Strings::free(lib_name);
                            Strings::free(unit_name);
                            save_restore.FileNotOk(); // Disable SaveRestore object
                            return 0;
                        }
                    }

                    num_elements--;
                    // Get Primary Unit from VHDL library :
                    design_unit = library->GetPrimUnit(unit_name);

                    // or look into Verilog side
                    if (!design_unit) design_unit = vhdl_file::GetVhdlUnitFromVerilog(lib_name, unit_name) ;

                    if (!design_unit) {
                        // Wasn't able to do a restore
                        tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The primary unit", unit_name) ;
                        Strings::free(lib_name);
                        Strings::free(unit_name);
                        save_restore.FileNotOk(); // Disable SaveRestore object
                        return 0;
                    }
                    // Retrieve signature (Only if the *.vdb is of version 0x0ab00006 or greater,
                    // will we contain signature information)
                    if (save_restore.GetVersion() >= 0x0ab00006) {
                        unsigned long orig_sig = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreLong() : save_restore.RestoreInteger() ;
                        // Check now if the restored design unit's signature matches the signature restored
                        // from the stream.  If it doesn't then this design_unit has changed in the meantime.
                        // (VIPER #1988/2048)
                        // VIPER #4301: If the vdb version is 0x0ab00015 or newer then use new method Hash::NumCompare
                        // to calculate the signature otherwise use the old method Hash::PointerCompare:
                        unsigned long new_sig = design_unit->CalculateSaveRestoreSignature((save_restore.GetVersion() >= 0x0ab00015) ? 1 : 0) ;
                        if (new_sig != orig_sig) {
#if (ULONG_MAX & 0xffffffff00000000UL)
                            // The following code is not needed for new vdbs > version 0x0ab00015 but required for older vdbs.
                            new_sig &= 0x0FFFFFF ;
                            orig_sig &= 0x0FFFFFF ;
                            // If we're running on a 64bit machine but restoring a 32bit vdb, then we
                            // need to do an 32bit compare, not a 64bit compare.
                            // VIPER #3890: Do not try to restore if the vdb was created in same or greater word size
                            // machine (basically in same size) or the truncated signatures still do not match:
                            if ((save_restore.GetSizeofLong() >= sizeof(long)) || (new_sig != orig_sig))
#endif
                            {
                                tmp_print_node.Error("%s needs to be re-saved since %s.%s changed", save_restore.FileName(), lib_name, unit_name) ;
                                // delete design_unit ; <- We can't delete this because we don't own it - deletion causes illegal memory overwrite
                                Strings::free(lib_name);
                                Strings::free(unit_name);
                                save_restore.FileNotOk(); // Disable SaveRestore object
                                return 0 ;
                            }
                        }
                    }

                    Strings::free(lib_name);
                    Strings::free(unit_name);

                    // Register design unit scope now if there is no more parsing to do, else the registering
                    // of the appropriate scope will be done in the next section of code.
                    if (!num_elements) {
                        if (!save_restore.IsRegistered(design_unit->LocalScope())) {
                            save_restore.RegisterPointer(design_unit->LocalScope());
                        }
                    } else {   // Now iterate over any remaining elements and verify that they currently exist;
                        VhdlScope *scope = design_unit->LocalScope();
                        VERIFIC_ASSERT(scope) ;
                        do {
                            char *name = save_restore.RestoreString();
                            if (!scope->FindSingleObjectLocal(name)) {
                                tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The VhdlIdDef", name) ;
                                Strings::free(name);
                                save_restore.FileNotOk(); // Disable SaveRestore object
                                return 0;
                            }
                            // Register scope if this is the last element to parse
                            if (num_elements == 1) {
                                if (!save_restore.IsRegistered(scope->FindSingleObjectLocal(name)->LocalScope())) {
                                    save_restore.RegisterPointer(scope->FindSingleObjectLocal(name)->LocalScope());
                                }
                            }
                            scope = scope->FindSingleObjectLocal(name)->LocalScope();
                            Strings::free(name);
                        }while(--num_elements);
                    }

                    // Register design unit and its IdDef
                    if (!save_restore.IsRegistered(design_unit))          { save_restore.RegisterPointer(design_unit); }
                    if (!save_restore.IsRegistered(design_unit->Id()))    { save_restore.RegisterPointer(design_unit->Id()); }

                    if (!(design_unit->LocalScope()->RegisterIdDefs(save_restore))) {
                        save_restore.FileNotOk(); // Disable SaveRestore object
                        return 0;   // Error occurred during restore.
                    }
                }
                break;

            case (unsigned)ID_VHDLSECONDARYUNIT:
                {
                    // Determine number of elements that describe IdDef path
                    int num_elements = save_restore.RestoreInteger();

                    // Retrieve library name and primary unit name
                    char *lib_name = save_restore.RestoreString();
                    char *unit_name = save_restore.RestoreString();

                    // Get Library
                    num_elements--;
                    library = vhdl_file::GetLibrary(lib_name);
                    if (!library) {
                        // Attempt first to do a restore of library/unit
                        if (!vhdl_file::Restore(lib_name, unit_name)) {
                            // Wasn't able to do a restore
                            tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The library", lib_name);
                            Strings::free(lib_name);
                            Strings::free(unit_name);
                            save_restore.FileNotOk(); // Disable SaveRestore object
                            return 0;
                        }

                        // Now get the library
                        library = vhdl_file::GetLibrary(lib_name);
                        //VERIFIC_ASSERT(library);   // ERROR: This should never get asserted!
                        if (!library){
                            tmp_print_node.Error("Registering Dependencies Error: library not defined") ;
                            Strings::free(lib_name);
                            Strings::free(unit_name);
                            save_restore.FileNotOk(); // Disable SaveRestore object
                            return 0;
                        }
                    }

                    // Get Primary Unit
                    num_elements--;
                    design_unit = library->GetPrimUnit(unit_name);
                    if (!design_unit) {
                        // Wasn't able to do a restore
                        tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The primary unit", unit_name) ;
                        Strings::free(lib_name);
                        Strings::free(unit_name);
                        save_restore.FileNotOk(); // Disable SaveRestore object
                        return 0;
                    }

                    // Get Secondary Unit
                    num_elements--;
                    char *sec_unit_name = save_restore.RestoreString();
                    design_unit = ((VhdlPrimaryUnit*)design_unit)->GetSecondaryUnit(sec_unit_name);
                    if (!design_unit) {
                        tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The secondary unit", sec_unit_name);
                        Strings::free(lib_name);
                        Strings::free(unit_name);
                        Strings::free(sec_unit_name);
                        save_restore.FileNotOk(); // Disable SaveRestore object
                        return 0;
                    }

                    // Retrieve signature (Only if the *.vdb is of version 0x0ab00006 or greater,
                    // will we contain signature information)
                    if (save_restore.GetVersion() >= 0x0ab00006) {
                        unsigned long orig_sig = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreLong() : save_restore.RestoreInteger() ;
                        // Check now if the restored design unit's signature matches the signature restored
                        // from the stream.  If it doesn't then this design_unit has changed in the meantime.
                        // (VIPER #1988/2048)
                        // VIPER #4301: If the vdb version is 0x0ab00015 or newer then use new method Hash::NumCompare
                        // to calculate the signature otherwise use the old method Hash::PointerCompare:
                        unsigned long new_sig = design_unit->CalculateSaveRestoreSignature((save_restore.GetVersion() >= 0x0ab00015) ? 1 : 0) ;
                        if (new_sig != orig_sig) {
#if (ULONG_MAX & 0xffffffff00000000UL)
                            // The following code is not needed for new vdbs > version 0x0ab00015 but required for older vdbs.
                            // If we're running on a 64bit machine but restoring a 32bit vdb, then we
                            // need to do an 32bit compare, not a 64bit compare.
                            new_sig  &= 0x0FFFFFF ; // Only look at bottom 24 bits
                            orig_sig &= 0x0FFFFFF ; // Only look at bottom 24 bits
                            // VIPER #3890: Do not try to restore if the vdb was created in same or greater word size
                            // machine (basically in same size) or the truncated signatures still do not match:
                            if ((save_restore.GetSizeofLong() >= sizeof(long)) || (new_sig != orig_sig))
#endif
                            {
                                tmp_print_node.Error("%s needs to be re-saved since %s.%s.%s changed", save_restore.FileName(), lib_name, unit_name, sec_unit_name) ;
                                // delete design_unit ;  <- We can't delete this because we don't own it - deletion causes illegal memory overwrite
                                Strings::free(lib_name);
                                Strings::free(unit_name);
                                Strings::free(sec_unit_name);
                                save_restore.FileNotOk(); // Disable SaveRestore object
                                return 0 ;
                            }
                        }
                    }

                    Strings::free(lib_name);
                    Strings::free(unit_name);
                    Strings::free(sec_unit_name);

                    // Register design unit scope now if there is no more parsing to do, else the registering
                    // of the appropriate scope will be done in the next section of code.
                    if (!num_elements) {
                        if (!save_restore.IsRegistered(design_unit->LocalScope())) {
                            save_restore.RegisterPointer(design_unit->LocalScope());
                        }
                    } else {   // Now iterate over any remaining elements and verify that they currently exist;
                        VhdlScope *scope = design_unit->LocalScope();
                        VERIFIC_ASSERT(scope) ;
                        do {
                            char *name = save_restore.RestoreString();
                            if (!scope->FindSingleObjectLocal(name)) {
                                tmp_print_node.Error("Registering Dependencies Error: %s '%s' could not be found during restore", "The VhdlIdDef", name);
                                Strings::free(name);
                                save_restore.FileNotOk(); // Disable SaveRestore object
                                return 0;
                            }
                            // Register scope if this is the last element to parse
                            if (num_elements == 1) {
                                if (!save_restore.IsRegistered(scope->FindSingleObjectLocal(name)->LocalScope())) {
                                    save_restore.RegisterPointer(scope->FindSingleObjectLocal(name)->LocalScope());
                                }
                            }
                            scope = scope->FindSingleObjectLocal(name)->LocalScope();
                            Strings::free(name);
                        } while (--num_elements);
                    }

                    // Register design unit and its IdDef
                    if (!save_restore.IsRegistered(design_unit))          { save_restore.RegisterPointer(design_unit); }
                    if (!save_restore.IsRegistered(design_unit->Id()))    { save_restore.RegisterPointer(design_unit->Id()); }

                    if (!(design_unit->LocalScope()->RegisterIdDefs(save_restore))) {
                        save_restore.FileNotOk(); // Disable SaveRestore object
                        return 0;   // Error occurred during restore.
                    }
                }
                break;

            default:
                //VERIFIC_ASSERT(0);  // This should never get executed!
                tmp_print_node.Error("Registering Dependencies Error: unknown pointer type");
                save_restore.FileNotOk(); // Disable SaveRestore object
                return 0;
        }
    }

    return 1;    // No error occurred
}

/* ---------------------------- */

unsigned VhdlScope::RegisterIdDefs(SaveRestore &save_restore) const
{
#if 0
    if (save_restore.IsSaving()) { // --- SAVING ---
        // Dump the number of IdDefs for verification purposes when restoring
        unsigned map_size = (_this_scope) ? _this_scope->Size() : 0;
        save_restore.SaveInteger(map_size);
    } else {   // --- RESTORING ---
        // Verify that the number of IdDefs dumped is the same number currently in this scope
        unsigned num_id_defs = save_restore.RestoreInteger();
        if (num_id_defs != (_this_scope ? _this_scope->Size() : 0)) {
            Message::Msg(VERIFIC_ERROR, 0, 0, "Restoring Error: Current scope seems to contain a different number of IdDefs");
            Message::Msg(VERIFIC_ERROR, 0, 0, "Restoring Error: Most likely this is due to old version of *.vdb files.");
            return 0;
        }
    }
#endif

    // Register all IdDefs
    char *name;
    VhdlIdDef *id_def;
    MapIter mi;
    FOREACH_MAP_ITEM(_this_scope, mi, &name, &id_def) {
        if (!id_def) continue ;
        if (!save_restore.IsRegistered(id_def)) {
            save_restore.RegisterPointer(id_def);
            // If an IdDef has a scope (entity decl, config decl, record type decl, process statement, etc ...),
            // then register IdDefs in it local scope.
            if (!id_def->IsPackageInstElement() && id_def->LocalScope()) {
                if (!id_def->LocalScope()->RegisterIdDefs(save_restore)) {
                    return 0;
                }
            }
        }
    }

    return 1;    // No error occurred
}

//--------------------------------------------------------------
// #included from VhdlIdDef.h
//--------------------------------------------------------------

void
VhdlIdDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveString(_name);

#ifdef VHDL_PRESERVE_ID_CASE
    save_restore.SaveString(_orig_name);
#else
    save_restore.SaveString(_name);     // It is possible to have a mismatch of VHDL_PRESERVE_ID_CASE settings between
#endif                                  // save and restores runs; thus, to be on the safe side, dump _name for _orig_name
    if (IsGeneric() && IsType()) {
        // Do not save _type for interface generic type. It will be
        // different for different instance
        SaveNodePtr(save_restore, 0) ;
    } else {
        SaveNodePtr(save_restore, _type);
    }

    // Don't need to dump _constraint and _value, since they are both elaboration-only info

    // Save attribute map :
    if (!_attributes || !_attributes->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(_attributes->Size());

        // Map is VhdlAttributeId* -> VhdlExpression*.
        VhdlIdDef *attr_id;
        VhdlExpression *attr_value ;
        MapIter mi;
        FOREACH_MAP_ITEM(_attributes, mi, &attr_id, &attr_value) {
            //VERIFIC_ASSERT(attr_id);  // LON - remove later
            SaveNodePtr(save_restore, attr_id);

            // Since version 0x0ab00010 we save the value expression here (a VhdlExpression pointer) :
            SaveNodePtr(save_restore, attr_value) ;
        }
    }
}

VhdlIdDef::VhdlIdDef(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _name(0),
#ifdef VHDL_PRESERVE_ID_CASE
    _orig_name(0),
#endif
    _type(0),
    _attributes(0)
#ifdef VHDL_ID_SCOPE_BACKPOINTER
    , _owning_scope(0)
#endif
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    , _subtype_indication(0)
#endif
    , _constraint(0)
    , _value(0)
    //CARBON_BEGIN
    , _pragmas(0)
    //CARBON_END
{
    // Restore data members appropriately
    _name = save_restore.RestoreString();
    char *orig_name = save_restore.RestoreString();
#ifdef VHDL_PRESERVE_ID_CASE
    _orig_name = orig_name;
#else
    Strings::free(orig_name);
#endif
    _type = (VhdlIdDef*)RestoreNodePtr(save_restore);

    // Don't need to restore _owning_scope, since it gets set when the identifier is entered into a scope.

    // Don't need to restore _constraint and _value, since they are both elaboration-only info

    // Restore attribute map :
    unsigned map_size = save_restore.RestoreInteger();
    if (map_size) {
        _attributes = new Map(POINTER_HASH, map_size); // LON - might have to adjust map_size for efficiency!
        // Map is VhdlAttributeId* -> VhdlExpression*, but the value portion will be forced to NULL.
        VhdlIdDef *attr_id;
        VhdlExpression *attr_value ;
        for (unsigned i = 0; i < map_size; i++) {
            attr_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
            attr_value = 0 ;
            if (save_restore.GetVersion() >= 0x0ab00010) {
                // Since version 0x0ab00010, the value field is a VhdlExpression pointer.
                attr_value = (VhdlExpression*)RestoreNodePtr(save_restore) ;
            }
            if (attr_id) {
                (void) _attributes->Insert(attr_id, attr_value, 0 /* overwrite */, 1 /* forced-insert */);
            }
        }
    }
}

/* ---------------------------- */

void
VhdlLibraryId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlLibraryId::VhdlLibraryId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore)
{
    // Force creation of library, since it's being asked for - VIPER #1896
    (void) vhdl_file::GetLibrary(Name(), 1 /*create if needed*/) ;

    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlGroupId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlGroupId::VhdlGroupId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlGroupTemplateId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlGroupTemplateId::VhdlGroupTemplateId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _decl(0)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlAttributeId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlAttributeId::VhdlAttributeId(SaveRestore &save_restore)
    : VhdlIdDef(save_restore), _attribute_specs(0)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlComponentId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveArray(save_restore, _ports);
    SaveArray(save_restore, _generics);
    SaveNodePtr(save_restore, _component);
}

VhdlComponentId::VhdlComponentId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _ports(0),
    _generics(0),
    _scope(0),
    _component(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _ports = RestoreArray(save_restore);
    _generics = RestoreArray(save_restore);
    _component = (VhdlComponentDecl*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlAliasId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target_name);
    SaveNodePtr(save_restore, _target_id);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_locally_static_type ;
    bit_flags |= _is_globally_static_type << 1 ;
    bit_flags |= _kind << 2 ;
    save_restore.SaveInteger(bit_flags) ;
}

VhdlAliasId::VhdlAliasId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _target_name(0),
    _target_id(0),
    _is_locally_static_type(1),
    _is_globally_static_type(1),
    _kind(0) // VIPER #6749
{
    // Restore data members appropriately
    _target_name = (VhdlName*)RestoreNodePtr(save_restore);
    _target_id = (VhdlIdDef*)RestoreNodePtr(save_restore);

    if (save_restore.GetVersion() >= 0x0ab00017) {
        unsigned bit_flags = save_restore.RestoreInteger() ; // all bit-flags
        _is_locally_static_type = bit_flags & 1 ;
        _is_globally_static_type = (bit_flags >> 1) & 1 ;
        _kind = (bit_flags >> 2) & 7 ;
    }
}

/* ---------------------------- */

void
VhdlFileId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately

    // No data members
    // _file is NOT saved. It's always re-initialized to 0.
}

VhdlFileId::VhdlFileId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _file(0)
{
    // Restore data members appropriately

    // No data members
    // _file is NOT saved. It's always re-initialized to 0.
}

/* ---------------------------- */

void
VhdlVariableId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately

    // FIX ME : provide this as a general way (in SaveRestore class)
    // to lump bit flags/sub-fields into one integer
    // helpfull for all structures with bit-flags.
    unsigned bit_flags = 0 ;
    bit_flags |= _is_assigned ;
    bit_flags |= _is_used << 1 ;
    bit_flags |= _can_be_dual_port_ram << 2 ;
    bit_flags |= _is_assigned_before_used << 3 ;
    bit_flags |= _is_used_before_assigned << 4 ;
    // VIPER #8312: Fixed typo of _can_be_multi_port_ram (was _can_be_dual_port_ram incorrectly)
    bit_flags |= _can_be_multi_port_ram << 5 ; // flag added for 2730 (multiport-ram extraction)
    bit_flags |= _is_concurrent_assigned << 6 ;
    bit_flags |= _is_concurrent_used << 7 ;
    bit_flags |= _is_locally_static_type << 8 ;
    bit_flags |= _is_globally_static_type << 9 ;
    bit_flags |= _is_shared << 10 ;

    save_restore.SaveInteger(bit_flags);
}

VhdlVariableId::VhdlVariableId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _is_assigned(0),
    _is_used(0),
    _is_concurrent_assigned(0),
    _is_concurrent_used(0),
    _can_be_dual_port_ram(0),
    _can_be_multi_port_ram(0),
    _is_assigned_before_used(0),
    _is_used_before_assigned(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _is_shared(0)
{
    // Restore data members appropriately
    unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
    _is_assigned = bit_flags & 1 ;
    _is_used = (bit_flags >> 1) & 1 ;
    _can_be_dual_port_ram = (bit_flags >> 2) & 1 ;
    _is_assigned_before_used = (bit_flags >> 3) & 1 ;
    _is_used_before_assigned = (bit_flags >> 4) & 1 ;
    _can_be_multi_port_ram = (bit_flags >> 5) & 1 ; // flag added for 2730 (multiport-ram extraction)
    _is_concurrent_assigned = (bit_flags >> 6) & 1 ; // flag added for 2843 (shared variables)
    _is_concurrent_used = (bit_flags >> 7) & 1 ; // flag added for 2843 (shared variables)

    if (save_restore.GetVersion() >= 0x0ab00014) {
        _is_locally_static_type = (bit_flags >> 8) & 1 ;
        _is_globally_static_type = (bit_flags >> 9) & 1 ;
    } else {
        _is_locally_static_type = 1 ;
        _is_globally_static_type = 1 ;
    }

    _is_shared = (bit_flags >> 10) & 1 ;

    if (save_restore.GetVersion() < 0x0ab0000c) {
        (void) save_restore.RestoreInteger(); // obsolete now. Here for backward compatibility.  Was for _is_used.
    }
}

/* ---------------------------- */

void
VhdlSignalId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _resolution_function);

    // FIX ME : provide this as a general way (in SaveRestore class)
    // to lump bit flags/sub-fields into one integer
    // helpfull for all structures with bit-flags.
    unsigned bit_flags = 0 ;
    bit_flags |= _is_assigned ;
    bit_flags |= _is_used << 1 ;
    bit_flags |= _can_be_dual_port_ram << 2 ;
    bit_flags |= _is_concurrent_assigned << 3 ;
    bit_flags |= _is_concurrent_used << 4 ;
    // VIPER #6234: Fixed typo of _can_be_multi_port_ram (was _can_be_dual_port_ram incorrectly)
    bit_flags |= _can_be_multi_port_ram << 5 ; // flag added for 2730 (multiport-ram extraction)
    bit_flags |= _is_locally_static_type << 6 ;
    bit_flags |= _is_globally_static_type << 7 ;
    bit_flags |= _signal_kind << 8 ; // two bits, next bit must shift by 10

    save_restore.SaveInteger(bit_flags);
}

VhdlSignalId::VhdlSignalId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _resolution_function(0),
    _is_assigned(0),
    _is_used(0),
    _is_concurrent_assigned(0),
    _is_concurrent_used(0),
    _can_be_dual_port_ram(0),
    _can_be_multi_port_ram(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _signal_kind(0)
{
    // Restore data members appropriately
    _resolution_function = (VhdlIdDef*)RestoreNodePtr(save_restore);

    unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
    _is_assigned = bit_flags & 1 ;
    _is_used = (bit_flags >> 1) & 1 ;
    _can_be_dual_port_ram = (bit_flags >> 2) & 1 ;
    _is_concurrent_assigned = (bit_flags >> 3) & 1 ;
    _is_concurrent_used = (bit_flags >> 4) & 1 ;
    _can_be_multi_port_ram = (bit_flags >> 5) & 1 ; // flag added for 2730 (multiport-ram extraction)

    if (save_restore.GetVersion() >= 0x0ab00014) {
        _is_locally_static_type = (bit_flags >> 6) & 1 ;
        _is_globally_static_type = (bit_flags >> 7) & 1 ;
    } else {
        _is_locally_static_type = 1 ;
        _is_globally_static_type = 1 ;
    }

    _signal_kind = (bit_flags >> 8) & 3 ; // signal kind is two bits

    if (save_restore.GetVersion() < 0x0ab0000c) {
        (void) save_restore.RestoreInteger(); // obsolete now. Here for backward compatibility.  Was for _is_used.
    }
}

/* ---------------------------- */

void
VhdlConstantId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _decl);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_locally_static ;
    bit_flags |= _is_globally_static << 1 ; // VIPER Issue #6349
    bit_flags |= _is_seq_loop_iter << 2 ;
    bit_flags |= _is_locally_static_type << 3 ;
    bit_flags |= _is_globally_static_type << 4 ;
    save_restore.SaveInteger(bit_flags);
}

VhdlConstantId::VhdlConstantId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _decl(0),
    _is_locally_static(0),
    _is_globally_static(1),
    _is_seq_loop_iter(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0)
{
    // Restore data members appropriately
    _decl = (VhdlIdDef*)RestoreNodePtr(save_restore);

    if (save_restore.GetVersion() >= 0x0ab00014) {
        unsigned bit_flags = save_restore.RestoreInteger() ;
        _is_locally_static = bit_flags & 1 ;
        if (save_restore.GetVersion() >= 0x0ab00024) {
            _is_globally_static = (bit_flags >> 1) & 1 ; // VIPER Issue #6349
            _is_seq_loop_iter = (bit_flags >> 2) & 1 ;
            _is_locally_static_type = (bit_flags >> 3) & 1 ;
            _is_globally_static_type = (bit_flags >> 4) & 1 ;
        } else {
            _is_seq_loop_iter = (bit_flags >> 1) & 1 ;
            _is_locally_static_type = (bit_flags >> 2) & 1 ;
            _is_globally_static_type = (bit_flags >> 3) & 1 ;
        }
    } else {
        _is_locally_static_type = 1 ;
        _is_globally_static_type = 1 ;
    }
}

/* ---------------------------- */

void
VhdlProtectedTypeId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeId::Save(save_restore);
}

VhdlProtectedTypeId::VhdlProtectedTypeId(SaveRestore &save_restore)
  : VhdlTypeId(save_restore),
    _type_def(0),
    _body_id(0) // VIPER #7775 : No need to save/restore
{
}

/* ---------------------------- */

void
VhdlProtectedTypeBodyId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeId::Save(save_restore);
}

VhdlProtectedTypeBodyId::VhdlProtectedTypeBodyId(SaveRestore &save_restore)
  : VhdlTypeId(save_restore),
    _type_def_body(0),
    _typedef_id(0) // VIPER #7775 : No need to save/restore
{
}

/* ---------------------------- */

void
VhdlTypeId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    save_restore.SaveInteger(_type_enum);
    SaveMap(save_restore, _list);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_locally_static_type ;
    bit_flags |= _is_globally_static_type << 1 ;

    save_restore.SaveInteger(bit_flags);

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    save_restore.SaveString(_verilog_pkg_lib_name);
    save_restore.SaveString(_verilog_pkg_name);
#endif
}

VhdlTypeId::VhdlTypeId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _type_enum(UNKNOWN_TYPE),
    _list(0),
    _scope(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    ,_verilog_pkg_lib_name(0)
    ,_verilog_pkg_name(0)
#endif
    ,_type_decl(0) // VIPER #7343 : No need to save/restore
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _type_enum = (type_enum)save_restore.RestoreInteger();
    _list = RestoreIdDefMap(save_restore);

    if (save_restore.GetVersion() >= 0x0ab00014) {
        unsigned bit_flags = save_restore.RestoreInteger();   // all bit-flags
        // not restoring _is_unconstrained to maintain status quo
        _is_locally_static_type = bit_flags & 1 ;
        _is_globally_static_type = (bit_flags >> 1) & 1 ;
        if (_type_enum==ENUMERATION_TYPE) {
            ; //Info ("*** %s is restored locally static = %d", Name(), _is_locally_static_type) ;
        }
    } else {
        _is_locally_static_type = (_type_enum != UNCONSTRAINED_ARRAY_TYPE) ;
        _is_globally_static_type = 1 ;
        if (_type_enum==ENUMERATION_TYPE) {
            ; //Info ("*** %s is restored and made locally static", Name()) ;
        }
    }
    if (save_restore.GetVersion() >= 0x0ab00030) {
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
        _verilog_pkg_lib_name = save_restore.RestoreString();
        _verilog_pkg_name = save_restore.RestoreString();
#endif
    }
}

/* ---------------------------- */

void
VhdlGenericTypeId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeId::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlGenericTypeId::VhdlGenericTypeId(SaveRestore &save_restore)
  : VhdlTypeId(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlAnonymousType::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeId::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlAnonymousType::VhdlAnonymousType(SaveRestore &save_restore)
  : VhdlTypeId(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlUniversalInteger::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeId::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlUniversalInteger::VhdlUniversalInteger(SaveRestore &save_restore)
  : VhdlTypeId(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlUniversalReal::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeId::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlUniversalReal::VhdlUniversalReal(SaveRestore &save_restore)
  : VhdlTypeId(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlSubtypeId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_unconstrained ;
    bit_flags |= _is_locally_static_type << 1 ;
    bit_flags |= _is_globally_static_type << 2 ;

    // Dump data members appropriately
    SaveNodePtr(save_restore, _resolution_function);
    save_restore.SaveInteger(bit_flags);
}

VhdlSubtypeId::VhdlSubtypeId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _resolution_function(0),
    _is_unconstrained(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _type_decl(0)
{
    // Restore data members appropriately
    _resolution_function = (VhdlIdDef*)RestoreNodePtr(save_restore);

    if (save_restore.GetVersion() >= 0x0ab00014) {
        unsigned bit_flags = save_restore.RestoreInteger();   // all bit-flags
        // not restoring _is_unconstrained to maintain status quo
        _is_locally_static_type = (bit_flags >> 1) & 1 ;
        _is_globally_static_type = (bit_flags >> 2) & 1 ;
    } else {
        _is_locally_static_type = 1 ;
        _is_globally_static_type = 1 ;
    }
}

/* ---------------------------- */

void
VhdlSubprogramId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_procedure ;
    bit_flags |= _is_assigned_before_used << 1 ;
    bit_flags |= _is_used_before_assigned << 2 ;
    bit_flags |= _is_label_applies_to_pragma << 3 ;
    bit_flags |= _is_generic << 4 ;
    bit_flags |= _is_overwritten_predefinedOperator << 5 ;

    // Dump data members appropriately
    SaveArray(save_restore, _args);
    SaveNodePtr(save_restore, _spec);
    SaveNodePtr(save_restore, _body);
    save_restore.SaveInteger(_pragma_function);
    save_restore.SaveInteger(_pragma_signed);
    save_restore.SaveInteger(bit_flags);
    SaveArray(save_restore, _generics) ;
    SaveNodePtr(save_restore, _sub_inst) ;
}

VhdlSubprogramId::VhdlSubprogramId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _generics(0),
    _args(0),
    _spec(0),
    _body(0),
    _pragma_function(0),
    _pragma_signed(0),
    _is_assigned_before_used(0),
    _is_used_before_assigned(0),
    _is_procedure(0)
    ,_is_processing(0)
    ,_is_label_applies_to_pragma(0)
    ,_is_generic(0)
    ,_is_overwritten_predefinedOperator(0)
    ,_sub_inst(0)
{
    // Restore data members appropriately
    _args = RestoreArray(save_restore);
    _spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _body = (VhdlSubprogramBody*)RestoreNodePtr(save_restore);
    _pragma_function = save_restore.RestoreInteger();     // bit field
    _pragma_signed = save_restore.RestoreInteger();       // bit field

    unsigned bit_flags = save_restore.RestoreInteger();   // all bit-flags
    _is_procedure            = bit_flags & 1 ;
    _is_assigned_before_used = (bit_flags >> 1) & 1 ;
    _is_used_before_assigned = (bit_flags >> 2) & 1 ;
    _is_label_applies_to_pragma = (bit_flags >> 3) & 1 ;
    _is_generic = (bit_flags >> 4) & 1 ;
    _is_overwritten_predefinedOperator = (bit_flags >> 5) & 1 ;
    if (save_restore.GetVersion() >= 0x0ab0001a) _generics = RestoreArray(save_restore) ;
    if (save_restore.GetVersion() >= 0x0ab0001f) _sub_inst = (VhdlSubprogInstantiationDecl*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlInterfaceId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _resolution_function);
    save_restore.SaveInteger(_object_kind);
    save_restore.SaveInteger(_kind);
    save_restore.SaveInteger(_mode);
    save_restore.SaveInteger(_signal_kind);

    unsigned bit_flags = 0 ;
    bit_flags |= _has_init_assign ;
    bit_flags |= _is_unconstrained << 1 ;
    bit_flags |= _is_assigned << 2 ;
    bit_flags |= _is_used << 3 ;
    bit_flags |= _is_locally_static_type << 4 ;
    bit_flags |= _is_globally_static_type << 5 ;
    bit_flags |= _can_be_multi_port_ram << 6 ;

    save_restore.SaveInteger(bit_flags);
}

VhdlInterfaceId::VhdlInterfaceId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _resolution_function(0),
    _object_kind(0),
    _kind(0),
    _mode(0),
    _signal_kind(0),
    _has_init_assign(0),
    _is_unconstrained(0),
    _is_assigned(0),
    _is_used(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _is_ref_formal(0),
    _can_be_multi_port_ram(0)
   , _verilog_assoc_list(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
   , _verilog_actual_size(0) // Save/restore not needed
#endif
   , _is_actual_present(0)
   , _ref_expr(0)
{
    // Restore data members appropriately
    _resolution_function = (VhdlIdDef*)RestoreNodePtr(save_restore);

    // token fields :
    if (save_restore.GetVersion() < 0x0ab00014) {
        _is_locally_static_type = 1 ;
        _is_globally_static_type = 1 ;
        _object_kind = EncodeObjectKind(save_restore.RestoreInteger()) ;
        _kind = EncodeKind(save_restore.RestoreInteger()) ;
        _mode = EncodeMode(save_restore.RestoreInteger()) ;
        _signal_kind = EncodeSignalKind(save_restore.RestoreInteger()) ;
    } else {
        _object_kind = save_restore.RestoreInteger();
        _kind = save_restore.RestoreInteger();
        _mode = save_restore.RestoreInteger();
        _signal_kind = save_restore.RestoreInteger();
    }

    // bit fields :
    if (save_restore.GetVersion() >= 0x0ab0000c) {
        unsigned bit_flags = save_restore.RestoreInteger();   // all bit-flags
        _has_init_assign    = (bit_flags >> 0) & 1 ;
        _is_unconstrained   = (bit_flags >> 1) & 1 ;
        _is_assigned        = (bit_flags >> 2) & 1 ;
        _is_used            = (bit_flags >> 3) & 1 ;

        if (save_restore.GetVersion() >= 0x0ab00014) {
            _is_locally_static_type = (bit_flags >> 4) & 1 ;
            _is_globally_static_type = (bit_flags >> 5) & 1 ;
        }
        if (save_restore.GetVersion() >= 0x0ab00038) {
            _can_be_multi_port_ram = (bit_flags >> 6) & 1 ;
        }
    } else {
        _has_init_assign    = save_restore.RestoreInteger();
        _is_unconstrained   = save_restore.RestoreInteger();
        _is_assigned        = save_restore.RestoreInteger();
        _is_used            = save_restore.RestoreInteger();
    }
}

/* ---------------------------- */

void
VhdlOperatorId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSubprogramId::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_oper_type);
    save_restore.SaveInteger(_owns_arg_id) ; // VIPER #5996
}

VhdlOperatorId::VhdlOperatorId(SaveRestore &save_restore)
  : VhdlSubprogramId(save_restore),
    _oper_type(0 /* ??? */), _owns_arg_id(0)
{
    // Restore data members appropriately
    _oper_type = save_restore.RestoreInteger();
    if (save_restore.GetVersion() >= 0x0ab00018) _owns_arg_id = save_restore.RestoreInteger() ; // VIPER #5996
}

/* ---------------------------- */

void
VhdlEnumerationId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_position);
    save_restore.SaveInteger(_bit_encoding);
    save_restore.SaveInteger(_onehot_encoded);
    save_restore.SaveString(_encoding);
    save_restore.SaveInteger(_enum_encoded);
}

VhdlEnumerationId::VhdlEnumerationId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _position(0),
    _bit_encoding(0),
    _onehot_encoded(0),
    _enum_encoded(0),
    _encoding(0)
{
    // Restore data members appropriately
    _position = save_restore.RestoreInteger();        // bit field
    _bit_encoding = save_restore.RestoreInteger();    // bit field
    _onehot_encoded = save_restore.RestoreInteger();  // bit field
    _encoding = save_restore.RestoreString();
    if (save_restore.GetVersion() >= 0x0ab00034) _enum_encoded = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VhdlElementId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_position);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_locally_static_type ;
    bit_flags |= _is_globally_static_type << 1 ;
    save_restore.SaveInteger(bit_flags);
}

VhdlElementId::VhdlElementId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _position(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0)
{
    // Restore data members appropriately
    _position = save_restore.RestoreInteger();

    if (save_restore.GetVersion() >= 0x0ab00014) {
        unsigned bit_flags = save_restore.RestoreInteger();
        _is_locally_static_type = bit_flags & 1 ;
        _is_globally_static_type = (bit_flags >> 1) & 1 ;
    } else {
        _is_locally_static_type = 1 ;
        _is_globally_static_type = 1 ;
    }
}

/* ---------------------------- */

void
VhdlPhysicalUnitId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveVerific64(_position);

    // Viper #5242 : Dump inverse position field
    save_restore.SaveVerific64(_inv_position) ;
}

VhdlPhysicalUnitId::VhdlPhysicalUnitId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _position(0),
    _inv_position(1)
{
    // Restore data members appropriately
    // Only if the *.vdb is of version 0x0ab00013 or greater, will we restore a 64 bit integer
    if (save_restore.GetVersion() >= 0x0ab00013) {
        _position = save_restore.RestoreVerific64();
    } else {
        // Otherwise, restore an integer:
        // Come via unsigned cast, big unsigned number might have been saved as -ve integer
        _position = (unsigned)save_restore.RestoreInteger();
    }
    // Only if the *.vdb is of version 0x0ab00016 or greater, we will restore _inv_position
    if (save_restore.GetVersion() >= 0x0ab00016) {
        _inv_position = save_restore.RestoreVerific64();
    }
}

/* ---------------------------- */

void
VhdlEntityId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveArray(save_restore, _ports);
    SaveArray(save_restore, _generics);
    SaveNodePtr(save_restore, _unit);
}

VhdlEntityId::VhdlEntityId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _ports(0),
    _generics(0),
    _scope(0),
    _unit(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _ports = RestoreArray(save_restore);
    _generics = RestoreArray(save_restore);
    _unit = (VhdlPrimaryUnit*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlArchitectureId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveNodePtr(save_restore, _entity);
    SaveNodePtr(save_restore, _unit);
}

VhdlArchitectureId::VhdlArchitectureId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _entity(0),
    _scope(0),
    _unit(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _entity = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _unit = (VhdlSecondaryUnit*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlConfigurationId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveNodePtr(save_restore, _entity);
    SaveNodePtr(save_restore, _unit);
}

VhdlConfigurationId::VhdlConfigurationId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _entity(0),
    _scope(0),
    _unit(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _entity = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _unit = (VhdlPrimaryUnit*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlPackageId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _uninstantiated_pkg_id) ;
    SaveScopePtr(save_restore, _scope, _uninstantiated_pkg_id ? 1: 0);  // Save scope first
    SaveNodePtr(save_restore, _unit);
    SaveArray(save_restore, _uninstantiated_pkg_id ? 0: _generics) ;
    SaveNodePtr(save_restore, _decl) ;
}

VhdlPackageId::VhdlPackageId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _generics(0),
    _scope(0),
    _unit(0),
    _decl(0),
    _uninstantiated_pkg_id(0)
{
    if (save_restore.GetVersion() >= 0x0ab00031) _uninstantiated_pkg_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore, _uninstantiated_pkg_id ? 1: 0);  // Restore scope first
    _unit = (VhdlPrimaryUnit*)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab00019) {
        Array *generics = RestoreArray(save_restore) ;
        if (!_uninstantiated_pkg_id) {
            _generics = generics ;
        } else { // For instantiated package id, generics will be generated
            delete generics ;
        }
    }
    if ((save_restore.GetVersion() >= 0x0ab0001e) && (save_restore.GetVersion() < 0x0ab00020)) (void)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab00032) _decl = (VhdlDeclaration*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlContextId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveNodePtr(save_restore, _unit);
}

VhdlContextId::VhdlContextId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _scope(0),
    _unit(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _unit = (VhdlPrimaryUnit*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlPackageBodyId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveNodePtr(save_restore, _unit);
}

VhdlPackageBodyId::VhdlPackageBodyId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _scope(0),
    _unit(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _unit = (VhdlSecondaryUnit*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlLabelId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveNodePtr(save_restore, _statement);

    // Since version 0x0ab00011, primary binding is set during analysis, and must be saved :
    SaveNodePtr(save_restore, _primary_binding) ;
}

VhdlLabelId::VhdlLabelId(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _scope(0),
    _statement(0),
    _primary_binding(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _statement = (VhdlStatement*)RestoreNodePtr(save_restore);

    // Primary binding is saved since version 0x0ab00011
    if (save_restore.GetVersion() >= 0x0ab00011) {
        _primary_binding = (VhdlBindingIndication*)RestoreNodePtr(save_restore) ;
    }
    // FIX ME : for older versions, we might have to call ResolveSpecs to get bindings set.
}

/* ---------------------------- */

void
VhdlBlockId::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlLabelId::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _generics);
    SaveArray(save_restore, _ports);
}

VhdlBlockId::VhdlBlockId(SaveRestore &save_restore)
  : VhdlLabelId(save_restore),
    _generics(0),
    _ports(0)
{
    // Restore data members appropriately
    _generics = RestoreArray(save_restore);
    _ports = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlPackageInstElement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIdDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _actual_id);
}

VhdlPackageInstElement::VhdlPackageInstElement(SaveRestore &save_restore)
  : VhdlIdDef(save_restore),
    _actual_id(0),
    _target_id(0)
{
    // Restore data members appropriately
    _actual_id = (VhdlIdDef*)RestoreNodePtr(save_restore) ;
}
//--------------------------------------------------------------
// #included from VhdlStatement.h
//--------------------------------------------------------------

void
VhdlStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _label);
    save_restore.SaveChar((char)_postponed);

#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    SaveNodePtr(save_restore, _closing_label) ; // VIPER #6666
#else
    SaveNodePtr(save_restore, 0) ;
#endif
}

VhdlStatement::VhdlStatement(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _label(0),
    _postponed(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{
    // Restore data members appropriately
    _label = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _postponed = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreChar() : save_restore.RestoreInteger();

    VhdlDesignator *tmp = (save_restore.GetVersion() >= 0x0ab00029) ? (VhdlDesignator *)RestoreNodePtr(save_restore) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    _closing_label = tmp ; // VIPER #6666
#else
    delete tmp ;
#endif
}

/* ---------------------------- */

void
VhdlNullStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately

    // No data members to dump
}

VhdlNullStatement::VhdlNullStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlReturnStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveNodePtr(save_restore, _owner);
}

VhdlReturnStatement::VhdlReturnStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _expr(0),
    _owner(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _owner = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlExitStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    SaveNodePtr(save_restore, _condition);
    SaveNodePtr(save_restore, _target_label);
}

VhdlExitStatement::VhdlExitStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _condition(0),
    _target_label(0)
{
    // Restore data members appropriately
    _target = (VhdlName*)RestoreNodePtr(save_restore);
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);
    _target_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlNextStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    SaveNodePtr(save_restore, _condition);
    SaveNodePtr(save_restore, _target_label);
}

VhdlNextStatement::VhdlNextStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _condition(0),
    _target_label(0)
{
    // Restore data members appropriately
    _target = (VhdlName*)RestoreNodePtr(save_restore);
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);
    _target_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlLoopStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _iter_scheme);
    SaveArray(save_restore, _statements);
}

VhdlLoopStatement::VhdlLoopStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _iter_scheme(0),
    _statements(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _iter_scheme = (VhdlIterScheme*)RestoreNodePtr(save_restore);
    _statements = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlCaseStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveArray(save_restore, _alternatives);
    save_restore.SaveInteger(_is_matching_case) ;
    SaveNodePtr(save_restore, _expr_type);
}

VhdlCaseStatement::VhdlCaseStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _expr(0),
    _alternatives(0),
    _is_matching_case(0),
    _expr_type(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _alternatives = RestoreArray(save_restore);
    if (save_restore.GetVersion() >= 0x0ab00021) _is_matching_case = save_restore.RestoreInteger() ;
    _expr_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlIfStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _if_cond);
    SaveArray(save_restore, _if_statements);
    SaveArray(save_restore, _elsif_list);
    SaveArray(save_restore, _else_statements);
}

VhdlIfStatement::VhdlIfStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _if_cond(0),
    _if_statements(0),
    _elsif_list(0),
    _else_statements(0)
{
    // Restore data members appropriately
    _if_cond = (VhdlExpression*)RestoreNodePtr(save_restore);
    _if_statements = RestoreArray(save_restore);
    _elsif_list = RestoreArray(save_restore);
    _else_statements = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlVariableAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    SaveNodePtr(save_restore, _value);
}

VhdlVariableAssignmentStatement::VhdlVariableAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _value(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _value = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlSignalAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    SaveNodePtr(save_restore, _delay_mechanism);
    SaveArray(save_restore, _waveform);
}

VhdlSignalAssignmentStatement::VhdlSignalAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _delay_mechanism(0),
    _waveform(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _delay_mechanism = (VhdlDelayMechanism*)RestoreNodePtr(save_restore);
    _waveform = RestoreArray(save_restore);
}

/* ---------------------------- */
void
VhdlForceAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    save_restore.SaveInteger(_force_mode) ;
    SaveNodePtr(save_restore, _expr);
}
VhdlForceAssignmentStatement::VhdlForceAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _force_mode(0),
    _expr(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _force_mode = save_restore.RestoreInteger() ;
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
}
/* ---------------------------- */
void
VhdlReleaseAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    save_restore.SaveInteger(_force_mode) ;
}
VhdlReleaseAssignmentStatement::VhdlReleaseAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _force_mode(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _force_mode = save_restore.RestoreInteger() ;
}
/* ---------------------------- */

void
VhdlWaitStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _sensitivity_clause);
    SaveNodePtr(save_restore, _condition_clause);
    SaveNodePtr(save_restore, _timeout_clause);
}

VhdlWaitStatement::VhdlWaitStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _sensitivity_clause(0),
    _condition_clause(0),
    _timeout_clause(0)
{
    // Restore data members appropriately
    _sensitivity_clause = RestoreArray(save_restore);
    _condition_clause = (VhdlExpression*)RestoreNodePtr(save_restore);
    _timeout_clause = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlReportStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _report);
    SaveNodePtr(save_restore, _severity);
}

VhdlReportStatement::VhdlReportStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _report(0),
    _severity(0)
{
    // Restore data members appropriately
    _report = (VhdlExpression*)RestoreNodePtr(save_restore);
    _severity = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlAssertionStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _condition);
    SaveNodePtr(save_restore, _report);
    SaveNodePtr(save_restore, _severity);
}

VhdlAssertionStatement::VhdlAssertionStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _condition(0),
    _report(0),
    _severity(0)
{
    // Restore data members appropriately
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);
    _report = (VhdlExpression*)RestoreNodePtr(save_restore);
    _severity = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlProcessStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveArray(save_restore, _sensitivity_list);
    SaveArray(save_restore, _decl_part);
    SaveArray(save_restore, _statement_part);
}

VhdlProcessStatement::VhdlProcessStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _sensitivity_list(0),
    _decl_part(0),
    _statement_part(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _sensitivity_list = RestoreArray(save_restore);
    _decl_part = RestoreArray(save_restore);
    _statement_part = RestoreArray(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlBlockStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _guard);
    SaveNodePtr(save_restore, _generics);
    SaveNodePtr(save_restore, _ports);
    SaveArray(save_restore, _decl_part);
    SaveArray(save_restore, _statements);

    // Set at analysis time
    SaveNodePtr(save_restore, _guard_id);
}

VhdlBlockStatement::VhdlBlockStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _guard(0),
    _generics(0),
    _ports(0),
    _decl_part(0),
    _statements(0),
    _local_scope(0),
    _guard_id(0)
    ,_is_static_elaborated(0) // No need to save/restore it
    ,_is_elab_created_empty(0) // No need to save/restore it
    ,_is_gen_elab_created(0) // No need to save/restore it
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _guard = (VhdlExpression*)RestoreNodePtr(save_restore);
    _generics = (VhdlBlockGenerics*)RestoreNodePtr(save_restore);
    _ports = (VhdlBlockPorts*)RestoreNodePtr(save_restore);
    _decl_part = RestoreArray(save_restore);
    _statements = RestoreArray(save_restore);

    // Set at analysis time
    _guard_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlGenerateStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _scheme);
    SaveArray(save_restore, _decl_part);
    SaveArray(save_restore, _statement_part);
}

VhdlGenerateStatement::VhdlGenerateStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _scheme(0),
    _decl_part(0),
    _statement_part(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _scheme = (VhdlIterScheme*)RestoreNodePtr(save_restore);
    _decl_part = RestoreArray(save_restore);
    _statement_part = RestoreArray(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlComponentInstantiationStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _instantiated_unit);
    SaveArray(save_restore, _generic_map_aspect);
    SaveArray(save_restore, _port_map_aspect);
    SaveNodePtr(save_restore, _unit);
}

VhdlComponentInstantiationStatement::VhdlComponentInstantiationStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _instantiated_unit(0),
    _generic_map_aspect(0),
    _port_map_aspect(0),
    _unit(0)
    // VIPER #6467 : Elaboration specific : No need to save/restore
    ,_library_name(0) // name of instantiated unit's library
    ,_instantiated_unit_name(0) // name of instantiated entity
    ,_arch_name(0) // name of instantiated architecture
{
    // Restore data members appropriately
    _instantiated_unit = (VhdlName*)RestoreNodePtr(save_restore);
    _generic_map_aspect = RestoreArray(save_restore);
    _port_map_aspect = RestoreArray(save_restore);
    _unit = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlProcedureCallStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _call);
}

VhdlProcedureCallStatement::VhdlProcedureCallStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _call(0)
{
    // Restore data members appropriately
    _call = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlConditionalSignalAssignment::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    SaveNodePtr(save_restore, _options);
    SaveNodePtr(save_restore, _delay_mechanism);
    SaveArray(save_restore, _conditional_waveforms);
    SaveNodePtr(save_restore, _guard_id);
}

VhdlConditionalSignalAssignment::VhdlConditionalSignalAssignment(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _options(0),
    _delay_mechanism(0),
    _conditional_waveforms(0),
    _guard_id(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _options = (VhdlOptions*)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab0001b) _delay_mechanism = (VhdlDelayMechanism*)RestoreNodePtr(save_restore);
    _conditional_waveforms = RestoreArray(save_restore);
    _guard_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */
void
VhdlConditionalForceAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    save_restore.SaveInteger(_force_mode) ;
    SaveArray(save_restore, _conditional_expressions) ;
}
VhdlConditionalForceAssignmentStatement::VhdlConditionalForceAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _force_mode(0),
    _conditional_expressions(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore) ;
    _force_mode = save_restore.RestoreInteger() ;
    _conditional_expressions = RestoreArray(save_restore);
}
/* ---------------------------- */

void
VhdlConditionalVariableAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _target);
    SaveArray(save_restore, _conditional_expressions);
}

VhdlConditionalVariableAssignmentStatement::VhdlConditionalVariableAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _target(0),
    _conditional_expressions(0)
{
    // Restore data members appropriately
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _conditional_expressions = RestoreArray(save_restore);
}

/* ---------------------------- */
void
VhdlSelectedSignalAssignment::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveNodePtr(save_restore, _target);
    SaveNodePtr(save_restore, _options);
    SaveNodePtr(save_restore, _delay_mechanism);
    SaveArray(save_restore, _selected_waveforms);
    SaveNodePtr(save_restore, _expr_type);
    SaveNodePtr(save_restore, _guard_id);
    save_restore.SaveInteger(_is_matching) ;
}

VhdlSelectedSignalAssignment::VhdlSelectedSignalAssignment(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _expr(0),
    _target(0),
    _options(0),
    _delay_mechanism(0),
    _selected_waveforms(0),
    _is_matching(0),
    _expr_type(0),
    _guard_id(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _options = (VhdlOptions*)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab0001b) _delay_mechanism = (VhdlDelayMechanism*)RestoreNodePtr(save_restore);
    _selected_waveforms = RestoreArray(save_restore);
    _expr_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _guard_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab00022) _is_matching = save_restore.RestoreInteger() ;
}
/* ---------------------------- */
void
VhdlIfElsifGenerateStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlGenerateStatement::Save(save_restore);

    SaveArray(save_restore, _elsif_list) ;
    SaveNodePtr(save_restore, _else_stmt) ;
}
VhdlIfElsifGenerateStatement::VhdlIfElsifGenerateStatement(SaveRestore &save_restore)
  : VhdlGenerateStatement(save_restore),
    _elsif_list(0),
    _else_stmt(0)
{
    _elsif_list = RestoreArray(save_restore);
    _else_stmt = (VhdlStatement*)RestoreNodePtr(save_restore);
}
/* ---------------------------- */
void
VhdlCaseGenerateStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveArray(save_restore, _alternatives);
    SaveNodePtr(save_restore, _expr_type);
}
VhdlCaseGenerateStatement::VhdlCaseGenerateStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _expr(0),
    _alternatives(0),
    _expr_type(0)
{
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore) ;
    _alternatives = RestoreArray(save_restore);
    _expr_type = (VhdlIdDef*)RestoreNodePtr(save_restore) ;
}
/* ---------------------------- */

void
VhdlSelectedVariableAssignmentStatement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveNodePtr(save_restore, _target);
    SaveArray(save_restore, _selected_exprs);
    SaveNodePtr(save_restore, _expr_type);
    save_restore.SaveInteger(_is_matching) ;
}

VhdlSelectedVariableAssignmentStatement::VhdlSelectedVariableAssignmentStatement(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _expr(0),
    _target(0),
    _selected_exprs(0),
    _is_matching(0),
    _expr_type(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _selected_exprs = RestoreArray(save_restore);
    _expr_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab00022) _is_matching = save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VhdlSelectedForceAssignment::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStatement::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveNodePtr(save_restore, _target);
    save_restore.SaveInteger(_is_matching) ;
    save_restore.SaveInteger(_force_mode) ;
    SaveArray(save_restore, _selected_exprs);
    SaveNodePtr(save_restore, _expr_type);
}

VhdlSelectedForceAssignment::VhdlSelectedForceAssignment(SaveRestore &save_restore)
  : VhdlStatement(save_restore),
    _expr(0),
    _target(0),
    _is_matching(0),
    _force_mode(0),
    _selected_exprs(0),
    _expr_type(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _target = (VhdlExpression*)RestoreNodePtr(save_restore);
    _is_matching = save_restore.RestoreInteger() ;
    _force_mode = save_restore.RestoreInteger() ;
    _selected_exprs = RestoreArray(save_restore);
    _expr_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
}
//--------------------------------------------------------------
// #included from VhdlExpression.h
//--------------------------------------------------------------

void
VhdlDiscreteRange::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlDiscreteRange::VhdlDiscreteRange(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlExpression::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDiscreteRange::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlExpression::VhdlExpression(SaveRestore &save_restore)
  : VhdlDiscreteRange(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlSubtypeIndication::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlSubtypeIndication::VhdlSubtypeIndication(SaveRestore &save_restore)
  : VhdlExpression(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlExplicitSubtypeIndication::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSubtypeIndication::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _res_function);
    SaveNodePtr(save_restore, _type_mark);
    SaveNodePtr(save_restore, _range_constraint);
    SaveNodePtr(save_restore, _range_type);
}

VhdlExplicitSubtypeIndication::VhdlExplicitSubtypeIndication(SaveRestore &save_restore)
  : VhdlSubtypeIndication(save_restore),
    _res_function(0),
    _type_mark(0),
    _range_constraint(0),
    _range_type(0)
{
    // Restore data members appropriately
    _res_function = (VhdlName*)RestoreNodePtr(save_restore);
    _type_mark = (VhdlName*)RestoreNodePtr(save_restore);
    _range_constraint = (VhdlDiscreteRange*)RestoreNodePtr(save_restore);
    _range_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlRange::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDiscreteRange::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _left);
    save_restore.SaveInteger(_dir);
    SaveNodePtr(save_restore, _right);
}

VhdlRange::VhdlRange(SaveRestore &save_restore)
  : VhdlDiscreteRange(save_restore),
    _left(0),
    _dir(0),
    _right(0)
{
    // Restore data members appropriately
    _left = (VhdlExpression*)RestoreNodePtr(save_restore);
    _dir = save_restore.RestoreInteger();
    _right = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlBox::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlBox::VhdlBox(SaveRestore &save_restore)
  : VhdlExpression(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlAssocElement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _formal_part);
    SaveNodePtr(save_restore, _actual_part);

    unsigned bit_flags = 0 ;
    bit_flags = _from_verilog ;
    save_restore.SaveInteger(bit_flags);
}

VhdlAssocElement::VhdlAssocElement(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _formal_part(0),
    _actual_part(0),
    _from_verilog(0)
{
    // Restore data members appropriately
    _formal_part = (VhdlName*)RestoreNodePtr(save_restore);
    _actual_part = (VhdlExpression*)RestoreNodePtr(save_restore);
    // bit fields :
    if (save_restore.GetVersion() >= 0x0ab00026) {
        unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
        if (save_restore.GetVersion() < 0x0ab00028) {
            RESTORE_BIT_FLAG(bit_flags, _from_verilog, 30) ; // restore the oper field
            RESTORE_BIT_FLAG(bit_flags, _from_verilog, 1) ; // restore the oper field
        }
        RESTORE_BIT_FLAG(bit_flags, _from_verilog, 1) ; // restore the oper field
    }
}

/* ---------------------------- */

void
VhdlInertialElement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _actual_part);
}

VhdlInertialElement::VhdlInertialElement(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _actual_part(0)
{
    // Restore data members appropriately
    _actual_part = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlRecResFunctionElement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _record_id_name);
    SaveNodePtr(save_restore, _record_id_function);
}

VhdlRecResFunctionElement::VhdlRecResFunctionElement(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _record_id_name(0),
    _record_id_function(0)
{
    // Restore data members appropriately
    _record_id_name = (VhdlName*)RestoreNodePtr(save_restore);
    _record_id_function = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlOperator::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _left);

    unsigned bit_flags = 0 ;
    bit_flags = _oper ;
    SAVE_BIT_FLAG(bit_flags, _is_implicit, 1) ;
    SAVE_BIT_FLAG(bit_flags, _unused, 3) ;
    save_restore.SaveInteger(bit_flags);

    SaveNodePtr(save_restore, _right);
    SaveNodePtr(save_restore, _operator);
}

VhdlOperator::VhdlOperator(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _left(0),
    _oper(0),
    _is_implicit(0),
    _unused(0),
    _right(0),
    _operator(0)
{
    // Restore data members appropriately
    _left = (VhdlExpression*)RestoreNodePtr(save_restore);
    // bit fields :
    if (save_restore.GetVersion() >= 0x0ab0001d) {
        unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
        RESTORE_BIT_FLAG(bit_flags, _unused, 3) ; // unused flag
        RESTORE_BIT_FLAG(bit_flags, _is_implicit, 1) ; // Flag to check if this is implicit
        RESTORE_BIT_FLAG(bit_flags, _oper, 28) ; // restore the oper field
    } else {
        _oper = save_restore.RestoreInteger();
    }
    _right = (VhdlExpression*)RestoreNodePtr(save_restore);
    _operator = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlAllocator::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _subtype);
}

VhdlAllocator::VhdlAllocator(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _subtype(0)
{
    // Restore data members appropriately
    _subtype = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlAggregate::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _element_assoc_list);
    SaveNodePtr(save_restore, _aggregate_type);
    SaveArray(save_restore, _element_type_list);
}

VhdlAggregate::VhdlAggregate(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _element_assoc_list(0),
    _aggregate_type(0),
    _element_type_list(0)
{
    // Restore data members appropriately
    _element_assoc_list = RestoreArray(save_restore);
    _aggregate_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
    if (save_restore.GetVersion() >= 0x0ab00037) {
        _element_type_list = RestoreArray(save_restore) ;
    }
}

/* ---------------------------- */

void
VhdlQualifiedExpression::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _prefix);
    SaveNodePtr(save_restore, _aggregate);
}

VhdlQualifiedExpression::VhdlQualifiedExpression(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _prefix(0),
    _aggregate(0)
{
    // Restore data members appropriately
    _prefix = (VhdlName*)RestoreNodePtr(save_restore);
    _aggregate = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlElementAssoc::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _choices);
    SaveNodePtr(save_restore, _expr);
}

VhdlElementAssoc::VhdlElementAssoc(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _choices(0),
    _expr(0)
{
    // Restore data members appropriately
    _choices = RestoreArray(save_restore);
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlWaveformElement::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _value);
    SaveNodePtr(save_restore, _after);
}

VhdlWaveformElement::VhdlWaveformElement(SaveRestore &save_restore)
  : VhdlExpression(save_restore),
    _value(0),
    _after(0)
{
    // Restore data members appropriately
    _value = (VhdlExpression*)RestoreNodePtr(save_restore);
    _after = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlDefault::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlExpression::Save(save_restore);
}
VhdlDefault::VhdlDefault(SaveRestore &save_restore)
   : VhdlExpression(save_restore)
{}

//--------------------------------------------------------------
// #included from VhdlName.h
//--------------------------------------------------------------

void
VhdlDesignator::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveString(_name);
    SaveNodePtr(save_restore, _single_id);
}

VhdlDesignator::VhdlDesignator(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _name(0),
    _single_id(0)
{
    // Restore data members appropriately
    _name = save_restore.RestoreString();
    _single_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlAll::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignator::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlAll::VhdlAll(SaveRestore &save_restore)
  : VhdlDesignator(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlOthers::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignator::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlOthers::VhdlOthers(SaveRestore &save_restore)
  : VhdlDesignator(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlUnaffected::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignator::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlUnaffected::VhdlUnaffected(SaveRestore &save_restore)
  : VhdlDesignator(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlLiteral::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlLiteral::VhdlLiteral(SaveRestore &save_restore)
  : VhdlName(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlInteger::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlLiteral::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveVerific64(_value);
}

VhdlInteger::VhdlInteger(SaveRestore &save_restore)
  : VhdlLiteral(save_restore),
    _value(0)
{
    // Restore data members appropriately
    // Only if the *.vdb is of version 0x0ab00013 or greater, will we restore a 64 bit integer
    if (save_restore.GetVersion() >= 0x0ab00013) {
        _value = save_restore.RestoreVerific64();
    } else {
        // Otherwise, restore an integer:
        // Cast to unsigned, because it was always an unsigned number
        _value = (unsigned)save_restore.RestoreInteger();
    }
}

/* ---------------------------- */

void
VhdlReal::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlLiteral::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveDouble(_value);
}

VhdlReal::VhdlReal(SaveRestore &save_restore)
  : VhdlLiteral(save_restore),
    _value(0.0)
{
    // Restore data members appropriately
    _value = save_restore.RestoreDouble();
}

/* ---------------------------- */

void
VhdlStringLiteral::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignator::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlStringLiteral::VhdlStringLiteral(SaveRestore &save_restore)
  : VhdlDesignator(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlBitStringLiteral::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlStringLiteral::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveString(_based_string);
}

VhdlBitStringLiteral::VhdlBitStringLiteral(SaveRestore &save_restore)
  : VhdlStringLiteral(save_restore),
  _based_string(0)
{
    // Restore data members appropriately

    // Restore the based string VIPER #1950
    if (save_restore.GetVersion() >= 0x0ab00004) {
         // Only if the *.vdb is of version 0x0ab00004 or greater, will we restore
        _based_string = save_restore.RestoreString() ;
    }
}

/* ---------------------------- */

void
VhdlCharacterLiteral::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignator::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlCharacterLiteral::VhdlCharacterLiteral(SaveRestore &save_restore)
  : VhdlDesignator(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlPhysicalLiteral::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlLiteral::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _value);
    SaveNodePtr(save_restore, _unit);
}

VhdlPhysicalLiteral::VhdlPhysicalLiteral(SaveRestore &save_restore)
  : VhdlLiteral(save_restore),
    _value(0),
    _unit(0)
{
    // Restore data members appropriately
    _value = (VhdlExpression*)RestoreNodePtr(save_restore);
    _unit = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlNull::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlLiteral::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlNull::VhdlNull(SaveRestore &save_restore)
  : VhdlLiteral(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlIdRef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDesignator::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlIdRef::VhdlIdRef(SaveRestore &save_restore)
  : VhdlDesignator(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlArrayResFunction::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _res_function);
}

VhdlArrayResFunction::VhdlArrayResFunction(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _res_function(0)
{
    // Restore data members appropriately

    _res_function = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlRecordResFunction::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _rec_res_function_list);
}

VhdlRecordResFunction::VhdlRecordResFunction(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _rec_res_function_list(0)
{
    // Restore data members appropriately

    _rec_res_function_list = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlName::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSubtypeIndication::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlName::VhdlName(SaveRestore &save_restore)
  : VhdlSubtypeIndication(save_restore)
{
    // Restore data members appropriately
    // No data members
}

/* ---------------------------- */

void
VhdlSelectedName::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _prefix);
    SaveNodePtr(save_restore, _suffix);
    SaveNodePtr(save_restore, _unique_id);
}

VhdlSelectedName::VhdlSelectedName(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _prefix(0),
    _suffix(0),
    _unique_id(0)
{
    // Restore data members appropriately
    _prefix = (VhdlName*)RestoreNodePtr(save_restore);
    _suffix = (VhdlDesignator*)RestoreNodePtr(save_restore);
    _unique_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlIndexedName::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _prefix);
    SaveArray(save_restore, _assoc_list);
    SaveNodePtr(save_restore, _prefix_id);

    unsigned bit_flags = 0 ;
    bit_flags |= _is_array_index ;
    bit_flags |= _is_array_slice << 1 ;
    bit_flags |= _is_function_call << 2 ;
    bit_flags |= _is_index_constraint << 3 ;
    bit_flags |= _is_record_constraint << 4 ;
    bit_flags |= _is_type_conversion << 5 ;
    bit_flags |= _is_group_template_ref << 6 ;

    save_restore.SaveInteger(bit_flags);
}

VhdlIndexedName::VhdlIndexedName(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _prefix(0),
    _assoc_list(0),
    _prefix_id(0),
    _prefix_type(0),
    _is_array_index(0),
    _is_array_slice(0),
    _is_function_call(0),
    _is_index_constraint(0),
    _is_record_constraint(0),
    _is_type_conversion(0),
    _is_group_template_ref(0)
{
    // Restore data members appropriately
    _prefix = (VhdlName*)RestoreNodePtr(save_restore);
    _assoc_list = RestoreArray(save_restore);
    _prefix_id = (VhdlIdDef*)RestoreNodePtr(save_restore);

    // bit fields :
    if (save_restore.GetVersion() >= 0x0ab0000c) {
        unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
        _is_array_index      = (bit_flags >> 0) & 1 ;
        _is_array_slice      = (bit_flags >> 1) & 1 ;
        _is_function_call    = (bit_flags >> 2) & 1 ;
        _is_index_constraint = (bit_flags >> 3) & 1 ;
        if (save_restore.GetVersion() >= 0x0ab0002b) {
            _is_record_constraint = (bit_flags >> 4) & 1 ;
            _is_type_conversion  = (bit_flags >> 5) & 1 ;
        } else {
            _is_type_conversion  = (bit_flags >> 4) & 1 ;
        }
        _is_group_template_ref = (bit_flags >> 6) & 1 ;
    } else {
        _is_array_index      = save_restore.RestoreInteger();
        _is_array_slice      = save_restore.RestoreInteger();
        _is_function_call    = save_restore.RestoreInteger();
        _is_index_constraint = save_restore.RestoreInteger();
        _is_type_conversion  = save_restore.RestoreInteger();
    }
}

/* ---------------------------- */

void
VhdlSignaturedName::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _name);
    SaveNodePtr(save_restore, _signature);
    SaveNodePtr(save_restore, _unique_id);
}

VhdlSignaturedName::VhdlSignaturedName(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _name(0),
    _signature(0),
    _unique_id(0)
{
    // Restore data members appropriately
    _name = (VhdlName*)RestoreNodePtr(save_restore);
    _signature = (VhdlSignature*)RestoreNodePtr(save_restore);
    _unique_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlAttributeName::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _prefix);
    SaveNodePtr(save_restore, _designator);
    SaveNodePtr(save_restore, _expression);
    save_restore.SaveInteger(_attr_enum);
    SaveNodePtr(save_restore, _prefix_type);
    SaveNodePtr(save_restore, _result_type);

    //static Map *_predef_table does not get dumped ... for now
}

VhdlAttributeName::VhdlAttributeName(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _prefix(0),
    _designator(0),
    _expression(0),
    _attr_enum(0),
    _prefix_type(0),
    _result_type(0)
{
    // Restore data members appropriately
    _prefix = (VhdlName*)RestoreNodePtr(save_restore);
    _designator = (VhdlIdRef*)RestoreNodePtr(save_restore);
    _expression = (VhdlExpression*)RestoreNodePtr(save_restore);
    _attr_enum = save_restore.RestoreInteger();
    _prefix_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _result_type = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlOpen::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlOpen::VhdlOpen(SaveRestore &save_restore)
  : VhdlName(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlEntityAspect::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_entity_class);
    SaveNodePtr(save_restore, _name);
}

VhdlEntityAspect::VhdlEntityAspect(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _entity_class(0),
    _name(0)
{
    // Restore data members appropriately
    _entity_class = save_restore.RestoreInteger();
    _name = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlInstantiatedUnit::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_entity_class);
    SaveNodePtr(save_restore, _name);
}

VhdlInstantiatedUnit::VhdlInstantiatedUnit(SaveRestore &save_restore)
  : VhdlName(save_restore),
    _entity_class(0),
    _name(0)
{
    // Restore data members appropriately
    _entity_class = save_restore.RestoreInteger();
    _name = (VhdlName*)RestoreNodePtr(save_restore);
}
/* ---------------------------- */
void
VhdlExternalName::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlName::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_class_type) ;
    save_restore.SaveInteger(_path_token) ;
    save_restore.SaveInteger(_hat_count) ;
    SaveNodePtr(save_restore, _ext_path_name) ;
    SaveNodePtr(save_restore, _subtype_indication) ;
    SaveNodePtr(save_restore, _unique_id) ;
}
VhdlExternalName::VhdlExternalName(SaveRestore &save_restore)
   : VhdlName(save_restore),
    _class_type(0),
    _path_token(0),
    _hat_count(0),
    _ext_path_name(0),
    _subtype_indication(0),
    _unique_id(0)
{
    // Restore data members appropriately
    _class_type = save_restore.RestoreInteger();
    _path_token = save_restore.RestoreInteger();
    _hat_count = save_restore.RestoreInteger();
    _ext_path_name = (VhdlName*)RestoreNodePtr(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _unique_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

//--------------------------------------------------------------
// #included from VhdlDeclaration.h
//--------------------------------------------------------------

void
VhdlDeclaration::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    SaveNodePtr(save_restore, _closing_label) ; // VIPER #6666
#else
    SaveNodePtr(save_restore, 0) ;
#endif
}

VhdlDeclaration::VhdlDeclaration(SaveRestore &save_restore, unsigned do_not_restore_members)
  : VhdlTreeNode(save_restore)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{
    if ((save_restore.GetVersion() >= 0x0ab00031) && do_not_restore_members) return ;
    // Restore data members appropriately
    VhdlDesignator *tmp = (save_restore.GetVersion() >= 0x0ab00029) ? (VhdlDesignator *)RestoreNodePtr(save_restore) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    _closing_label = tmp ; // VIPER #6666
#else
    delete tmp ;
#endif
}

/* ---------------------------- */

void
VhdlTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    SaveNodePtr(save_restore, _closing_label) ; // VIPER #6666
#else
    SaveNodePtr(save_restore, 0) ;
#endif
}

VhdlTypeDef::VhdlTypeDef(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{
    // Restore data members appropriately
    VhdlDesignator *tmp = (save_restore.GetVersion() >= 0x0ab00029) ? (VhdlDesignator *)RestoreNodePtr(save_restore) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    _closing_label = tmp ; // VIPER #6666
#else
    delete tmp ;
#endif
}

/* ---------------------------- */

void
VhdlScalarTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _scalar_range);
}

VhdlScalarTypeDef::VhdlScalarTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _scalar_range(0)
{
    // Restore data members appropriately
    _scalar_range = (VhdlDiscreteRange*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlArrayTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _index_constraint);
    SaveNodePtr(save_restore, _subtype_indication);
}

VhdlArrayTypeDef::VhdlArrayTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _index_constraint(0),
    _subtype_indication(0)
{
    // Restore data members appropriately
    _index_constraint = RestoreArray(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlRecordTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveArray(save_restore, _element_decl_list);
}

VhdlRecordTypeDef::VhdlRecordTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _element_decl_list(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _element_decl_list = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlProtectedTypeDefBody::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveArray(save_restore, _decl_list);
}

VhdlProtectedTypeDefBody::VhdlProtectedTypeDefBody(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _decl_list(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _decl_list = RestoreArray(save_restore);

    VhdlIdDef *type_id = _local_scope ? _local_scope->GetOwner() : 0 ;
    if (type_id && type_id->IsProtectedBody()) type_id->DeclareProtectedTypeBody(this) ;

    // VIPER #7775 : Find the protected type header id and set back pointer for body there
    VhdlScope *container = _local_scope ? _local_scope->Upper(): 0 ;
    VhdlIdDef *header_id = container ? container->FindSingleObjectLocal(type_id ? type_id->Name(): 0): 0 ;
    if (header_id && type_id) header_id->SetProtectedTypeBodyId(type_id) ;
}

/* ---------------------------- */

void
VhdlProtectedTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveArray(save_restore, _decl_list);
}

VhdlProtectedTypeDef::VhdlProtectedTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _decl_list(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _decl_list = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlAccessTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _subtype_indication);
}

VhdlAccessTypeDef::VhdlAccessTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _subtype_indication(0)
{
    // Restore data members appropriately
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlFileTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _file_type_mark);
}

VhdlFileTypeDef::VhdlFileTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _file_type_mark(0)
{
    // Restore data members appropriately
    _file_type_mark = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlEnumerationTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _enumeration_literal_list);
}

VhdlEnumerationTypeDef::VhdlEnumerationTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _enumeration_literal_list(0)
{
    // Restore data members appropriately
    _enumeration_literal_list = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlPhysicalTypeDef::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTypeDef::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _range_constraint);
    SaveArray(save_restore, _physical_unit_decl_list);
}

VhdlPhysicalTypeDef::VhdlPhysicalTypeDef(SaveRestore &save_restore)
  : VhdlTypeDef(save_restore),
    _range_constraint(0),
    _physical_unit_decl_list(0)
{
    // Restore data members appropriately
    _range_constraint = (VhdlDiscreteRange*)RestoreNodePtr(save_restore);
    _physical_unit_decl_list = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlElementDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _id_list);
    SaveNodePtr(save_restore, _subtype_indication);
}

VhdlElementDecl::VhdlElementDecl(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _id_list(0),
    _subtype_indication(0)
{
    // Restore data members appropriately
    _id_list = RestoreArray(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

/* ---------------------------- */

void
VhdlPhysicalUnitDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveNodePtr(save_restore, _physical_literal);
}

VhdlPhysicalUnitDecl::VhdlPhysicalUnitDecl(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _id(0),
    _physical_literal(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _physical_literal = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlUseClause::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _selected_name_list);
    save_restore.SaveChar(_is_implicit);
}

VhdlUseClause::VhdlUseClause(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _selected_name_list(0),
    _is_implicit(0)
{
    // Restore data members appropriately
    _selected_name_list = RestoreArray(save_restore);

    // Restore the _is_implicit flag (VIPER #1994)
    if (save_restore.GetVersion() >= 0x0ab00005) {
         // Only if the *.vdb is of version 0x0ab00005 or greater, will we restore
        _is_implicit = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreChar() : save_restore.RestoreInteger() ;
    }
}

/* ---------------------------- */
// Vhdl 2008 LRM section 13.4
void
VhdlContextReference::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _selected_name_list);
    // VIPER #6933 : Save library ids
    SaveSet(save_restore, _lib_ids) ;
}

VhdlContextReference::VhdlContextReference(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _selected_name_list(0),
    _lib_ids(0)
{
    // Restore data members appropriately
    _selected_name_list = RestoreArray(save_restore);
    if (save_restore.GetVersion() >= 0x0ab0002e) { // VIPER #6933 : Restore library ids
        _lib_ids = RestoreSet(save_restore) ;
    }
}

/* ---------------------------- */

void
VhdlLibraryClause::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _id_list);
    save_restore.SaveChar(_is_implicit);
}

VhdlLibraryClause::VhdlLibraryClause(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id_list(0),
    _is_implicit(0)
{
    // Restore data members appropriately
    _id_list = RestoreArray(save_restore);

    // Restore the _is_implicit flag (VIPER #1994)
    if (save_restore.GetVersion() >= 0x0ab00005) {
         // Only if the *.vdb is of version 0x0ab00005 or greater, will we restore
        _is_implicit = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreChar() : save_restore.RestoreInteger() ;
    }
}

/* ---------------------------- */

void
VhdlInterfaceDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_interface_kind);
    SaveArray(save_restore, _id_list);
    save_restore.SaveInteger(_mode);
    SaveNodePtr(save_restore, _subtype_indication);
    save_restore.SaveInteger(_signal_kind);
    SaveNodePtr(save_restore, _init_assign);
}

VhdlInterfaceDecl::VhdlInterfaceDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _interface_kind(0),
    _id_list(0),
    _mode(0),
    _subtype_indication(0),
    _signal_kind(0),
    _init_assign(0)
{
    // Restore data members appropriately
    _interface_kind = save_restore.RestoreInteger();
    _id_list = RestoreArray(save_restore);
    _mode = save_restore.RestoreInteger();
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _signal_kind = save_restore.RestoreInteger();
    _init_assign = (VhdlExpression*)RestoreNodePtr(save_restore);

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

/* ---------------------------- */

void
VhdlSubprogramDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _subprogram_spec);
}

VhdlSubprogramDecl::VhdlSubprogramDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _subprogram_spec(0)
{
    // Restore data members appropriately
    _subprogram_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlSubprogramBody::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _subprogram_spec);
    SaveArray(save_restore, _decl_part);
    SaveArray(save_restore, _statement_part);
}

VhdlSubprogramBody::VhdlSubprogramBody(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _subprogram_spec(0),
    _decl_part(0),
    _statement_part(0)
{
    // Restore data members appropriately
    _subprogram_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _decl_part = RestoreArray(save_restore);
    _statement_part = RestoreArray(save_restore);
    // VIPER #5918 : Set container array for nested packages
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl && decl->IsPrimaryUnit()) decl->SetContainerArray(_decl_part) ;
    }
}

/* ---------------------------- */

void
VhdlSubtypeDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveNodePtr(save_restore, _subtype_indication);
}

VhdlSubtypeDecl::VhdlSubtypeDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0),
    _subtype_indication(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    // Get type and resolution function from subtype id itself as those
    // information are already restored.
    VhdlIdDef *type = _id ? _id->Type(): 0 ;
    VhdlIdDef *res_func = _id ? _id->ResolutionFunction(): 0 ;

    // Populate the '_is_unconstrained' field of VhdlSubtypeId in the
    // same way as normal constructor does
    unsigned is_unconstrained = 0 ;

    // Check if the type_mark is an unconstrained array type.
    if (type && type->IsUnconstrainedArrayType()) {
        // Check if the type mark is a single identifier (the unconstrained type)
        // Viper 7505: Repeated Fix for 4544 from the explicit constructor to save restore constructor
        // VhdlIdDef *basetype = _subtype_indication ? _subtype_indication->FindFullFormal(): 0 ;
        // if (basetype) is_unconstrained = 1 ;
        // VIPER #4544 : Check wheather subtype indication is unconstrained or not.
        // If yes we have to mark declared identifiers unconstrained.
        if (_subtype_indication && _subtype_indication->IsUnconstrained(1)) {
            is_unconstrained = 1 ;
        }
    }

    unsigned locally_static_type = _subtype_indication ? _subtype_indication->IsLocallyStatic() : 1 ;
    unsigned globally_static_type = locally_static_type || (_subtype_indication ? _subtype_indication->IsGloballyStatic() : 1) ;

    if (_id) _id->DeclareSubtype(type, res_func, is_unconstrained, locally_static_type, globally_static_type) ;
    if (_id) _id->SetDeclaration(this) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    if (_id) _id->SetSubtypeIndication(_subtype_indication) ;
#endif
}

/* ---------------------------- */

void
VhdlFullTypeDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveNodePtr(save_restore, _type_def);
}

VhdlFullTypeDecl::VhdlFullTypeDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0),
    _type_def(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _type_def = (VhdlTypeDef*)RestoreNodePtr(save_restore);
    // VIPER #7343 : Set declaration back pointer to type id
    if (_id) _id->SetDeclaration(this) ;
}

/* ---------------------------- */

void
VhdlIncompleteTypeDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
}

VhdlIncompleteTypeDecl::VhdlIncompleteTypeDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlConstantDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _id_list);
    SaveNodePtr(save_restore, _subtype_indication);
    SaveNodePtr(save_restore, _init_assign);
    SaveNodePtr(save_restore, _type);
}

VhdlConstantDecl::VhdlConstantDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id_list(0),
    _subtype_indication(0),
    _init_assign(0),
    _type(0)
{
    // Restore data members appropriately
    _id_list = RestoreArray(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _init_assign = (VhdlExpression*)RestoreNodePtr(save_restore);
    _type = (VhdlIdDef*)RestoreNodePtr(save_restore);
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
#endif
        if (save_restore.GetVersion() >= 0x0ab00014) continue ;
        if (id && _init_assign) id->SetLocallyStatic() ; // always set for older versions
    }
}

/* ---------------------------- */

void
VhdlSignalDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _id_list);
    SaveNodePtr(save_restore, _subtype_indication);
    save_restore.SaveInteger(_signal_kind);
    SaveNodePtr(save_restore, _init_assign);
}

VhdlSignalDecl::VhdlSignalDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id_list(0),
    _subtype_indication(0),
    _signal_kind(0),
    _init_assign(0)
{
    // Restore data members appropriately
    _id_list = RestoreArray(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _signal_kind = save_restore.RestoreInteger();
    _init_assign = (VhdlExpression*)RestoreNodePtr(save_restore);
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

/* ---------------------------- */

void
VhdlVariableDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_shared);
    SaveArray(save_restore, _id_list);
    SaveNodePtr(save_restore, _subtype_indication);
    SaveNodePtr(save_restore, _init_assign);
}

VhdlVariableDecl::VhdlVariableDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _shared(0),
    _id_list(0),
    _subtype_indication(0),
    _init_assign(0)
{
    // Restore data members appropriately
    _shared = save_restore.RestoreInteger();
    _id_list = RestoreArray(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _init_assign = (VhdlExpression*)RestoreNodePtr(save_restore);
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

/* ---------------------------- */

void
VhdlFileDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _id_list);
    SaveNodePtr(save_restore, _subtype_indication);
    SaveNodePtr(save_restore, _file_open_info);
}

VhdlFileDecl::VhdlFileDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id_list(0),
    _subtype_indication(0),
    _file_open_info(0)
{
    // Restore data members appropriately
    _id_list = RestoreArray(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _file_open_info = (VhdlFileOpenInfo*)RestoreNodePtr(save_restore);
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetSubtypeIndication(_subtype_indication) ;
    }
#endif
}

/* ---------------------------- */

void
VhdlAliasDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _designator);
    SaveNodePtr(save_restore, _subtype_indication);
    SaveNodePtr(save_restore, _alias_target_name);
}

VhdlAliasDecl::VhdlAliasDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _designator(0),
    _subtype_indication(0),
    _alias_target_name(0)
{
    // Restore data members appropriately
    _designator = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _subtype_indication = (VhdlSubtypeIndication*)RestoreNodePtr(save_restore);
    _alias_target_name = (VhdlName*)RestoreNodePtr(save_restore);
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    if (_designator) _designator->SetSubtypeIndication(_subtype_indication) ;
#endif
}

/* ---------------------------- */

void
VhdlComponentDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _id);
    SaveArray(save_restore, _generic_clause);
    SaveArray(save_restore, _port_clause);

    save_restore.SaveLong(_timestamp) ;
}

VhdlComponentDecl::VhdlComponentDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0),
    _generic_clause(0),
    _port_clause(0),
    _local_scope(0),
    _timestamp(0)
    ,_orig_component(0) // do not restore it
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _generic_clause = RestoreArray(save_restore);
    _port_clause = RestoreArray(save_restore);

    _timestamp = (save_restore.GetVersion() >= 0x0ab0000c) ? save_restore.RestoreLong() : save_restore.RestoreInteger() ;
}

/* ---------------------------- */

void
VhdlAttributeDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveNodePtr(save_restore, _type_mark);
}

VhdlAttributeDecl::VhdlAttributeDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0),
    _type_mark(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _type_mark = (VhdlName*)RestoreNodePtr(save_restore);

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype indication backpointer
    if (_id) _id->SetSubtypeIndication(_type_mark) ;
#endif
}

/* ---------------------------- */

void
VhdlAttributeSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _designator);
    SaveNodePtr(save_restore, _entity_spec);
    SaveNodePtr(save_restore, _value);
}

VhdlAttributeSpec::VhdlAttributeSpec(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _designator(0),
    _entity_spec(0),
    _value(0)
{
    // Restore data members appropriately
    _designator = (VhdlName*)RestoreNodePtr(save_restore);
    _entity_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _value = (VhdlExpression*)RestoreNodePtr(save_restore);

    // Viper #5432
    VhdlIdDef *attr_id = _designator ? _designator->GetId() : 0 ;
    if (attr_id) attr_id->AddAttributeSpec(this) ;
}

/* ---------------------------- */

void
VhdlConfigurationSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _component_spec);
    SaveNodePtr(save_restore, _binding);
}

VhdlConfigurationSpec::VhdlConfigurationSpec(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _component_spec(0),
    _binding(0)
{
    // Restore data members appropriately
    _component_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _binding = (VhdlBindingIndication*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlDisconnectionSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _guarded_signal_spec);
    SaveNodePtr(save_restore, _after);
}

VhdlDisconnectionSpec::VhdlDisconnectionSpec(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _guarded_signal_spec(0),
    _after(0)
{
    // Restore data members appropriately
    _guarded_signal_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _after = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlGroupTemplateDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveArray(save_restore, _entity_class_entry_list);
}

VhdlGroupTemplateDecl::VhdlGroupTemplateDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0),
    _entity_class_entry_list(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _entity_class_entry_list = RestoreArray(save_restore);
    if (_id) _id->SetGroupTemplateDeclaration(this) ; // Set back pointer
}

/* ---------------------------- */

void
VhdlGroupDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveNodePtr(save_restore, _group_template_name);
}

VhdlGroupDecl::VhdlGroupDecl(SaveRestore &save_restore)
  : VhdlDeclaration(save_restore),
    _id(0),
    _group_template_name(0)
{
    // Restore data members appropriately
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _group_template_name = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */
void
VhdlSubprogInstantiationDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDeclaration::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _subprogram_spec) ;
    SaveNodePtr(save_restore, _uninstantiated_subprog_name) ;
    SaveArray(save_restore, _generic_map_aspect) ;
    if (!_generic_types || !_generic_types->Size()) {
        // Dump that map does not exist (versus existing but having a size of zero)
        save_restore.SaveInteger(0);
    } else {
        // Dump map size
        save_restore.SaveInteger(_generic_types->Size());

        VhdlTreeNode *node ;
        VhdlTreeNode *key ;
        MapIter mi;
        FOREACH_MAP_ITEM(_generic_types, mi, &key, &node) {
            SaveNodePtr(save_restore, key);
            SaveNodePtr(save_restore, node);
        }
    }
}
VhdlSubprogInstantiationDecl::VhdlSubprogInstantiationDecl(SaveRestore &save_restore)
   : VhdlDeclaration(save_restore),
    _subprogram_spec(0),
    _uninstantiated_subprog_name(0),
    _generic_map_aspect(0),
    _generic_types(0)
{
    _subprogram_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _uninstantiated_subprog_name = (VhdlName*)RestoreNodePtr(save_restore);
    _generic_map_aspect = RestoreArray(save_restore) ;
    // Restore map size
    unsigned map_size = save_restore.RestoreInteger();

    if (map_size) {
        _generic_types = new Map(POINTER_HASH, map_size);
        // Restore elements (VhdlNode* in this case)
        VhdlIdDef *id_def;
        VhdlIdDef *actual_id ;
        for (unsigned i = 0; i < map_size; i++) {
            id_def = (VhdlIdDef*)RestoreNodePtr(save_restore);
            actual_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
            (void) _generic_types->Insert(id_def, actual_id);
        }
    }
}
/* ---------------------------- */
void
VhdlInterfaceTypeDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlInterfaceDecl::Save(save_restore);

    // Dump data member
    SaveNodePtr(save_restore, _type_id) ;
}
VhdlInterfaceTypeDecl::VhdlInterfaceTypeDecl(SaveRestore &save_restore)
    : VhdlInterfaceDecl(save_restore),
    _type_id(0)
{
    if (save_restore.GetVersion() < 0x0ab00036) {
         VhdlTreeNode tmp_print_node ; //dummy node for msg printing
         tmp_print_node.SetLinefile(0) ;
         tmp_print_node.Error("%s was created using an incompatible version_number, vhdl 2008 backward compatibility is not supported for this version", save_restore.FileName()) ;
    }

    _type_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
}
/* ---------------------------- */
void
VhdlInterfaceSubprogDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlInterfaceDecl::Save(save_restore);

    // Dump data member
    SaveNodePtr(save_restore, _subprogram_spec) ;
}
VhdlInterfaceSubprogDecl::VhdlInterfaceSubprogDecl(SaveRestore &save_restore)
    : VhdlInterfaceDecl(save_restore),
    _subprogram_spec(0),
    _actual_subprog(0) // Set dynamically, do need to save/restore
{
    _subprogram_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */
void
VhdlInterfacePackageDecl::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlInterfaceDecl::Save(save_restore);

    // Dump data member
    SaveScopePtr(save_restore, _local_scope, 1);
    SaveNodePtr(save_restore, _id) ;
    SaveNodePtr(save_restore, _uninst_package_name) ;
    SaveNodePtr(save_restore, _uninst_package_id) ;
    // We have not saved the _this_scope of _scope for package instance
    // So register the iddefs of _scope now
    if (_local_scope && !_local_scope->RegisterIdDefs(save_restore)) {
         save_restore.FileNotOk();
         return ;
    }
    SaveArray(save_restore, _generic_map_aspect) ;
    if (!_generic_types || !_generic_types->Size()) {
        save_restore.SaveInteger(0);
    } else {
        save_restore.SaveInteger(_generic_types->Size()) ;
        VhdlTreeNode *node, *key ;
        MapIter mi;
        FOREACH_MAP_ITEM(_generic_types, mi, &key, &node) {
            SaveNodePtr(save_restore, key);
            SaveNodePtr(save_restore, node);
        }
    }
}
VhdlInterfacePackageDecl::VhdlInterfacePackageDecl(SaveRestore &save_restore)
    : VhdlInterfaceDecl(save_restore),
    _id(0),
    _uninst_package_name(0),
    _generic_map_aspect(0),
    _local_scope(0),
    _initial_pkg(0),
    _uninst_package_id(0),
    _generic_types(0),
    _actual_pkg(0)
    ,_actual_elab_pkg(0)
{
    _local_scope = RestoreScopePtr(save_restore, 1) ;
    _id = (VhdlIdDef*)RestoreNodePtr(save_restore) ;
    _uninst_package_name = (VhdlName*)RestoreNodePtr(save_restore) ;
    _uninst_package_id = (VhdlIdDef*)RestoreNodePtr(save_restore) ;
    CreateSpecialPackageElementIds() ;

    // We have not saved the _this_scope of _scope for package instance
    // So register the iddefs of _scope now
    if (_local_scope && !_local_scope->RegisterIdDefs(save_restore)) {
         save_restore.FileNotOk();
         return ;
    }
    _generic_map_aspect = RestoreArray(save_restore) ;
    // Restore map size
    unsigned map_size = save_restore.RestoreInteger();
    if (map_size) {
        _generic_types = new Map(POINTER_HASH, map_size) ;
        VhdlIdDef *id_def;
        VhdlIdDef *actual_id ;
        for (unsigned i = 0; i < map_size; i++) {
            id_def = (VhdlIdDef*)RestoreNodePtr(save_restore);
            actual_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
            (void) _generic_types->Insert(id_def, actual_id) ;
        }
    }
}
//--------------------------------------------------------------
// #included from VhdlMisc.h
//--------------------------------------------------------------

void
VhdlSignature::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _type_mark_list);
    SaveNodePtr(save_restore, _return_type);
}

VhdlSignature::VhdlSignature(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _type_mark_list(0),
    _return_type(0)
{
    // Restore data members appropriately
    _type_mark_list = RestoreArray(save_restore);
    _return_type = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlFileOpenInfo::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _file_open);
    save_restore.SaveInteger(_open_mode);
    SaveNodePtr(save_restore, _file_logical_name);
}

VhdlFileOpenInfo::VhdlFileOpenInfo(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _file_open(0),
    _open_mode(0),
    _file_logical_name(0)
{
    // Restore data members appropriately
    _file_open = (VhdlExpression*)RestoreNodePtr(save_restore);
    _open_mode = save_restore.RestoreInteger();
    _file_logical_name = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlBindingIndication::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _entity_aspect);
    SaveArray(save_restore, _generic_map_aspect);
    SaveArray(save_restore, _port_map_aspect);
    SaveNodePtr(save_restore, _unit);
}

VhdlBindingIndication::VhdlBindingIndication(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _entity_aspect(0),
    _generic_map_aspect(0),
    _port_map_aspect(0),
    _unit(0)
{
    // Restore data members appropriately
    _entity_aspect = (VhdlName*)RestoreNodePtr(save_restore);
    _generic_map_aspect = RestoreArray(save_restore);
    _port_map_aspect = RestoreArray(save_restore);
    _unit = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlIterScheme::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlIterScheme::VhdlIterScheme(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlWhileScheme::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIterScheme::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _condition);
    SaveNodePtr(save_restore, _exit_label);
    SaveNodePtr(save_restore, _next_label);
}

VhdlWhileScheme::VhdlWhileScheme(SaveRestore &save_restore)
  : VhdlIterScheme(save_restore),
    _condition(0),
    _exit_label(0),
    _next_label(0)
{
    // Restore data members appropriately
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);
    _exit_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _next_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlForScheme::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIterScheme::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _id);
    SaveNodePtr(save_restore, _range);
    SaveNodePtr(save_restore, _iter_id);
    SaveNodePtr(save_restore, _exit_label);
    SaveNodePtr(save_restore, _next_label);
}

VhdlForScheme::VhdlForScheme(SaveRestore &save_restore)
  : VhdlIterScheme(save_restore),
    _id(0),
    _range(0),
    _iter_id(0),
    _exit_label(0),
    _next_label(0)
{
    // Restore data members appropriately
    _id = (VhdlIdRef*)RestoreNodePtr(save_restore);
    _range = (VhdlDiscreteRange*)RestoreNodePtr(save_restore);
    _iter_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _exit_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _next_label = (VhdlIdDef*)RestoreNodePtr(save_restore);

    if (_iter_id && save_restore.GetVersion() < 0x0ab00014) {
        _iter_id->SetLoopIterator() ;
    }
}

/* ---------------------------- */

void
VhdlIfScheme::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIterScheme::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _condition);

    // Dump alternative labels
    SaveNodePtr(save_restore, _alternative_label) ;
    SaveNodePtr(save_restore, _alternative_label_id) ;

#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    SaveNodePtr(save_restore, _alternative_closing_label) ; // VIPER #6666 & #6662
#else
    SaveNodePtr(save_restore, 0) ;
#endif
}

VhdlIfScheme::VhdlIfScheme(SaveRestore &save_restore)
  : VhdlIterScheme(save_restore),
    _condition(0),
    _alternative_label(0),
    _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
      , _alternative_closing_label(0)
#endif
{
    // Restore data members appropriately
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);

    if (save_restore.GetVersion() >= 0x0ab0001b) {
        _alternative_label = (VhdlIdRef*)RestoreNodePtr(save_restore);
        _alternative_label_id = (VhdlIdDef*)RestoreNodePtr(save_restore);
    }

    VhdlDesignator *tmp = (save_restore.GetVersion() >= 0x0ab0002d) ? (VhdlDesignator *)RestoreNodePtr(save_restore) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    _alternative_closing_label = tmp ; // VIPER #6666 & #6662
#else
    delete tmp ;
#endif
}

/* ---------------------------- */

void
VhdlElsifElseScheme::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIfScheme::Save(save_restore);
}

VhdlElsifElseScheme::VhdlElsifElseScheme(SaveRestore &save_restore)
  : VhdlIfScheme(save_restore)
{}

/* ---------------------------- */

void
VhdlCaseItemScheme::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlIterScheme::Save(save_restore);
    SaveArray(save_restore, _choices);
    // Dump alternative labels
    SaveNodePtr(save_restore, _alternative_label) ;
    SaveNodePtr(save_restore, _alternative_label_id) ;

#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    SaveNodePtr(save_restore, _alternative_closing_label) ; // VIPER #6666 & #6662
#else
    SaveNodePtr(save_restore, 0) ;
#endif
}
VhdlCaseItemScheme::VhdlCaseItemScheme(SaveRestore &save_restore)
    : VhdlIterScheme(save_restore),
      _choices(0),
      _alternative_label(0),
      _alternative_label_id(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
      , _alternative_closing_label(0)
#endif
{
    _choices = RestoreArray(save_restore) ;
    _alternative_label = (VhdlIdRef*)RestoreNodePtr(save_restore);
    _alternative_label_id = (VhdlIdDef*)RestoreNodePtr(save_restore);

    VhdlDesignator *tmp = (save_restore.GetVersion() >= 0x0ab0002d) ? (VhdlDesignator *)RestoreNodePtr(save_restore) : 0 ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    _alternative_closing_label = tmp ; // VIPER #6666 & #6662
#else
    delete tmp ;
#endif
}

/* ---------------------------- */

void
VhdlOptions::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_guarded);
    SaveNodePtr(save_restore, _delay_mechanism);
}

VhdlOptions::VhdlOptions(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _guarded(0),
    _delay_mechanism(0)
{
    // Restore data members appropriately
    _guarded = save_restore.RestoreInteger();
    _delay_mechanism = (VhdlDelayMechanism*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlDelayMechanism::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlDelayMechanism::VhdlDelayMechanism(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlTransport::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDelayMechanism::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlTransport::VhdlTransport(SaveRestore &save_restore)
  : VhdlDelayMechanism(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlInertialDelay::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlDelayMechanism::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _reject_expression);
    save_restore.SaveInteger(_inertial);
}

VhdlInertialDelay::VhdlInertialDelay(SaveRestore &save_restore)
  : VhdlDelayMechanism(save_restore),
    _reject_expression(0),
    _inertial(0)
{
    // Restore data members appropriately
    _reject_expression = (VhdlExpression*)RestoreNodePtr(save_restore);
    _inertial = save_restore.RestoreInteger();
}

/* ---------------------------- */

void
VhdlConditionalWaveform::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _waveform);
    SaveNodePtr(save_restore, _when_condition);
}

VhdlConditionalWaveform::VhdlConditionalWaveform(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _waveform(0),
    _when_condition(0)
{
    // Restore data members appropriately
    _waveform = RestoreArray(save_restore);
    _when_condition = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlConditionalExpression::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveNodePtr(save_restore, _condition);
}

VhdlConditionalExpression::VhdlConditionalExpression(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _expr(0),
    _condition(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlSelectedWaveform::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _waveform);
    SaveArray(save_restore, _when_choices);
}

VhdlSelectedWaveform::VhdlSelectedWaveform(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _waveform(0),
    _when_choices(0)
{
    // Restore data members appropriately
    _waveform = RestoreArray(save_restore);
    _when_choices = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlSelectedExpression::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _expr);
    SaveArray(save_restore, _when_choices);
}

VhdlSelectedExpression::VhdlSelectedExpression(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _expr(0),
    _when_choices(0)
{
    // Restore data members appropriately
    _expr = (VhdlExpression*)RestoreNodePtr(save_restore);
    _when_choices = RestoreArray(save_restore);
}
/* ---------------------------- */

void
VhdlBlockGenerics::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _generic_clause);
    SaveArray(save_restore, _generic_map_aspect);
    SaveNodePtr(save_restore, _block_label);
}

VhdlBlockGenerics::VhdlBlockGenerics(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _generic_clause(0),
    _generic_map_aspect(0),
    _block_label(0)
{
    // Restore data members appropriately
    _generic_clause = RestoreArray(save_restore);
    _generic_map_aspect = RestoreArray(save_restore);
    _block_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlBlockPorts::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _port_clause);
    SaveArray(save_restore, _port_map_aspect);
    SaveNodePtr(save_restore, _block_label);
}

VhdlBlockPorts::VhdlBlockPorts(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _port_clause(0),
    _port_map_aspect(0),
    _block_label(0)
{
    // Restore data members appropriately
    _port_clause = RestoreArray(save_restore);
    _port_map_aspect = RestoreArray(save_restore);
    _block_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlCaseStatementAlternative::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _choices);
    SaveArray(save_restore, _statements);
}

VhdlCaseStatementAlternative::VhdlCaseStatementAlternative(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _choices(0),
    _statements(0)
{
    // Restore data members appropriately
    _choices = RestoreArray(save_restore);
    _statements = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlElsif::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    SaveNodePtr(save_restore, _condition);
    SaveArray(save_restore, _statements);
}

VhdlElsif::VhdlElsif(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _condition(0),
    _statements(0)
{
    // Restore data members appropriately
    _condition = (VhdlExpression*)RestoreNodePtr(save_restore);
    _statements = RestoreArray(save_restore);
}

/* ---------------------------- */

void
VhdlEntityClassEntry::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately
    save_restore.SaveInteger(_entity_class);
    save_restore.SaveInteger(_box);
}

VhdlEntityClassEntry::VhdlEntityClassEntry(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore),
    _entity_class(0),
    _box(0)
{
    // Restore data members appropriately
    _entity_class = save_restore.RestoreInteger();    // bit field
    _box = save_restore.RestoreInteger();             // bit field
}

//--------------------------------------------------------------
// #included from VhdlSpecification.h
//--------------------------------------------------------------

void
VhdlSpecification::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore);

    // Dump data members appropriately

    // No data members
}

VhdlSpecification::VhdlSpecification(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlProcedureSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSpecification::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _designator);
    SaveArray(save_restore, _formal_parameter_list);
    SaveArray(save_restore, _generic_clause) ;
    SaveArray(save_restore, _generic_map_aspect) ;
    // Viper 6653
    unsigned bit_flags = 0 ;
    bit_flags |= _has_explicit_param ;
    save_restore.SaveInteger(bit_flags) ;
}

VhdlProcedureSpec::VhdlProcedureSpec(SaveRestore &save_restore)
  : VhdlSpecification(save_restore),
    _designator(0),
    _formal_parameter_list(0),
    _local_scope(0),
    _generic_clause(0),
    _generic_map_aspect(0),
    _has_explicit_param(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _designator = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _formal_parameter_list = RestoreArray(save_restore);
    if (save_restore.GetVersion() >= 0x0ab0001a) {
         // Vhdl 2008 LRM section 4.2.1
        _generic_clause = RestoreArray(save_restore) ;
        _generic_map_aspect = RestoreArray(save_restore) ;
    }
    if (save_restore.GetVersion() >= 0x0ab0002a) {
        unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
        RESTORE_BIT_FLAG(bit_flags, _has_explicit_param, 1) ; // restore the oper field
    }
}

/* ---------------------------- */

void
VhdlFunctionSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSpecification::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    save_restore.SaveInteger(_pure_impure);
    SaveNodePtr(save_restore, _designator);
    SaveArray(save_restore, _formal_parameter_list);
    SaveNodePtr(save_restore, _return_type);
    SaveArray(save_restore, _generic_clause) ;
    SaveArray(save_restore, _generic_map_aspect) ;
    // Viper 6653
    unsigned bit_flags = 0 ;
    bit_flags |= _has_explicit_param ;
    save_restore.SaveInteger(bit_flags) ;
}

VhdlFunctionSpec::VhdlFunctionSpec(SaveRestore &save_restore)
  : VhdlSpecification(save_restore),
    _pure_impure(0),
    _designator(0),
    _formal_parameter_list(0),
    _return_type(0),
    _local_scope(0),
    _generic_clause(0),
    _generic_map_aspect(0),
    _has_explicit_param(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _pure_impure = save_restore.RestoreInteger();
    _designator = (VhdlIdDef*)RestoreNodePtr(save_restore);
    _formal_parameter_list = RestoreArray(save_restore);
    _return_type = (VhdlName*)RestoreNodePtr(save_restore);
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    // update subtype_indication backpointers :
    if (_designator) _designator->SetSubtypeIndication(_return_type) ;
#endif
    if (save_restore.GetVersion() >= 0x0ab0001a) {
         // Vhdl 2008 LRM section 4.2.1
        _generic_clause = RestoreArray(save_restore) ;
        _generic_map_aspect = RestoreArray(save_restore) ;
    }
    if (save_restore.GetVersion() >= 0x0ab0002a) {
        unsigned bit_flags = save_restore.RestoreInteger(); // all bit-flags
        RESTORE_BIT_FLAG(bit_flags, _has_explicit_param, 1) ; // restore the oper field
    }
}

/* ---------------------------- */

void
VhdlComponentSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSpecification::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _scope);  // Save scope first
    SaveArray(save_restore, _id_list);
    SaveNodePtr(save_restore, _name);
    SaveNodePtr(save_restore, _component);
}

VhdlComponentSpec::VhdlComponentSpec(SaveRestore &save_restore)
  : VhdlSpecification(save_restore),
    _id_list(0),
    _name(0),
    _component(0),
    _scope(0)
{
    // Restore data members appropriately
    _scope = RestoreScopePtr(save_restore);  // Restore scope first
    _id_list = RestoreArray(save_restore);
    _name = (VhdlName*)RestoreNodePtr(save_restore);
    _component = (VhdlIdDef*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlGuardedSignalSpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSpecification::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _signal_list);
    SaveNodePtr(save_restore, _type_mark);
}

VhdlGuardedSignalSpec::VhdlGuardedSignalSpec(SaveRestore &save_restore)
  : VhdlSpecification(save_restore),
    _signal_list(0),
    _type_mark(0)
{
    // Restore data members appropriately
    _signal_list = RestoreArray(save_restore);
    _type_mark = (VhdlName*)RestoreNodePtr(save_restore);
}

/* ---------------------------- */

void
VhdlEntitySpec::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlSpecification::Save(save_restore);

    // Dump data members appropriately
    SaveArray(save_restore, _entity_name_list);
    save_restore.SaveInteger(_entity_class);
    SaveSet(save_restore, _all_ids);
}

VhdlEntitySpec::VhdlEntitySpec(SaveRestore &save_restore)
  : VhdlSpecification(save_restore),
    _entity_name_list(0),
    _entity_class(0),
    _all_ids(0)
{
    // Restore data members appropriately
    _entity_name_list = RestoreArray(save_restore);
    _entity_class = save_restore.RestoreInteger();
    _all_ids = RestoreSet(save_restore);
}

//--------------------------------------------------------------
// #included from VhdlConfiguration.h
//--------------------------------------------------------------

void
VhdlConfigurationItem::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlTreeNode::Save(save_restore) ;

    // Dump data members appropriately

    // No data members
}

VhdlConfigurationItem::VhdlConfigurationItem(SaveRestore &save_restore)
  : VhdlTreeNode(save_restore)
{
    // Restore data members appropriately

    // No data members
}

/* ---------------------------- */

void
VhdlBlockConfiguration::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlConfigurationItem::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _block_spec);
    SaveArray(save_restore, _use_clause_list);
    SaveArray(save_restore, _configuration_item_list);
    SaveNodePtr(save_restore, _block_label);
    // _block_range is elaboration-specific, so don't save it.
}

VhdlBlockConfiguration::VhdlBlockConfiguration(SaveRestore &save_restore)
  : VhdlConfigurationItem(save_restore),
    _block_spec(0),
    _use_clause_list(0),
    _configuration_item_list(0),
    _local_scope(0),
    _block_label(0)
    ,_block_range(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _block_spec = (VhdlName*)RestoreNodePtr(save_restore);
    _use_clause_list = RestoreArray(save_restore);
    _configuration_item_list = RestoreArray(save_restore);
    _block_label = (VhdlIdDef*)RestoreNodePtr(save_restore);
    // _block_range is elaboration-specific, so don't restore it.
}

/* ---------------------------- */

void
VhdlComponentConfiguration::Save(SaveRestore &save_restore) const
{
    // Save base class's members
    VhdlConfigurationItem::Save(save_restore);

    // Dump data members appropriately
    SaveScopePtr(save_restore, _local_scope);    // Save scope first
    SaveNodePtr(save_restore, _component_spec);
    SaveNodePtr(save_restore, _binding);
    SaveNodePtr(save_restore, _block_configuration);
}

VhdlComponentConfiguration::VhdlComponentConfiguration(SaveRestore &save_restore)
  : VhdlConfigurationItem(save_restore),
    _component_spec(0),
    _binding(0),
    _block_configuration(0),
    _local_scope(0)
{
    // Restore data members appropriately
    _local_scope = RestoreScopePtr(save_restore);    // Restore scope first
    _component_spec = (VhdlSpecification*)RestoreNodePtr(save_restore);
    _binding = (VhdlBindingIndication*)RestoreNodePtr(save_restore);
    _block_configuration = (VhdlBlockConfiguration*)RestoreNodePtr(save_restore);
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_NAME(SIG, NAME)  (((SIG) << 5) + (SIG) + (Hash::StringCompare((NAME), 0)))
#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (Hash::PointerCompare((const void *)(long)(VAL), 0)))
#define HASH_NUM(SIG, VAL)    (((SIG) << 5) + (SIG) + (Hash::NumCompare((const void *)(long)(VAL), 0)))
#define HASH_VAL_NUM(SIG, VAL, NUM_COMP) ((NUM_COMP) ? HASH_NUM((SIG), (VAL)) : HASH_VAL((SIG), (VAL)))

unsigned long
VhdlScope::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = 0 ;

    MapIter mi ;
    const VhdlIdDef *id ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (id) sig = HASH_VAL_NUM(sig, id->CalculateSaveRestoreSignature(use_num_compare), use_num_compare) ;
    }

    if (_using) sig= HASH_VAL_NUM(sig, _using->Size(), use_num_compare) ;

    return sig ;
}

unsigned long
VhdlDesignUnit::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    // Calculate
    unsigned long sig = 0 ;

    if (_id)          sig = HASH_VAL_NUM(sig, _id->CalculateSaveRestoreSignature(use_num_compare), use_num_compare) ;
    if (_id && _id->IsPackageInst()) return sig ; // donot consider scope for package instance
    if (_local_scope) sig = HASH_VAL_NUM(sig, _local_scope->CalculateSaveRestoreSignature(use_num_compare), use_num_compare) ;

    return sig ;
}

unsigned long
VhdlIdDef::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = HASH_VAL_NUM(0, GetClassId(), use_num_compare) ;
    if (_name) sig = HASH_NAME(0, _name) ;
    // For interface generic type, do not consider _type for signature
    // creation.
    // FIXME : Need to ignore this checking the save-restore version, but
    // that is not available here
    if (IsGeneric() && IsType()) return sig ;
    if (_type) sig = HASH_NAME(sig, _type->Name()) ;
    return sig ;
}

unsigned long
VhdlAliasId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_target_id) sig = HASH_NAME(sig, _target_id->Name()) ;
    return sig ;
}

unsigned long
VhdlSignalId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_resolution_function) sig = HASH_NAME(sig, _resolution_function->Name()) ;
    return sig ;
}

unsigned long
VhdlConstantId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_decl) sig = HASH_NAME(sig, _decl->Name()) ;
    return sig ;
}

unsigned long
VhdlTypeId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_type_enum) sig = HASH_VAL_NUM(sig, (unsigned long)_type_enum, use_num_compare) ;
    if (LocalScope()) {
        sig = HASH_VAL_NUM(sig, LocalScope()->CalculateSaveRestoreSignature(use_num_compare), use_num_compare) ;
    } else {
        MapIter mi ;
        const VhdlIdDef *id ;
        FOREACH_MAP_ITEM(_list, mi, 0, &id) {
            if (id) sig = HASH_NAME(sig, id->Name()) ;
        }
    }
    return sig ;
}

unsigned long
VhdlSubtypeId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_resolution_function) sig = HASH_NAME(sig, _resolution_function->Name()) ;
    return sig ;
}

unsigned long
VhdlSubprogramId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (LocalScope()) {
        sig = HASH_VAL_NUM(sig, LocalScope()->CalculateSaveRestoreSignature(use_num_compare), use_num_compare) ;
    } else {
        unsigned i ;
        const VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(_args, i, id) {
            if (id) sig = HASH_NAME(sig, id->Name()) ;
        }
    }
    return sig ;
}

unsigned long
VhdlInterfaceId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_resolution_function) sig = HASH_NAME(sig, _resolution_function->Name()) ;
    sig = HASH_VAL_NUM(sig, DecodeObjectKind(_object_kind), use_num_compare) ;
    sig = HASH_VAL_NUM(sig, DecodeKind(_kind), use_num_compare) ;
    sig = HASH_VAL_NUM(sig, DecodeMode(_mode), use_num_compare) ;
    sig = HASH_VAL_NUM(sig, DecodeSignalKind(_signal_kind), use_num_compare) ;
    sig = HASH_VAL_NUM(sig, _has_init_assign, use_num_compare) ;
    sig = HASH_VAL_NUM(sig, _is_unconstrained, use_num_compare) ;
    return sig ;
}

unsigned long
VhdlEntityId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    // Could use drop through to VhdlIdDef::CalculateSaveRestoreSignature, but decided to
    // define this routine for consistency sake.
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    return sig ;
}

unsigned long
VhdlArchitectureId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_entity) sig = HASH_NAME(sig, _entity->Name()) ;
    return sig ;
}

unsigned long
VhdlConfigurationId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_entity) sig = HASH_NAME(sig, _entity->Name()) ;
    return sig ;
}

unsigned long
VhdlLabelId::CalculateSaveRestoreSignature(unsigned use_num_compare) const
{
    unsigned long sig = VhdlIdDef::CalculateSaveRestoreSignature(use_num_compare) ;
    if (_scope) sig = HASH_VAL_NUM(sig, _scope->CalculateSaveRestoreSignature(use_num_compare), use_num_compare) ;
    return sig ;
}

