/*
 *
 * [ File Version : 1.114 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // fopen()

#include "VhdlMisc.h" // Compile flags are defined within here

#include "Array.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"
#include "FileSystem.h"

#include "vhdl_file.h"
#include "vhdl_tokens.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"

#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif
/**********************************************************/
//  Elaboration
/**********************************************************/

unsigned VhdlForScheme::Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df)
{
    if (!statements) return 0 ; // Nothing to do
    if (vhdl_file::CheckForInterrupt(Linefile())) return 0 ; // Stop after an error or interrupt
    if (!_range || !_iter_id) return 0 ; // Nothing to do

    // Create the range
    VhdlConstraint *range = _range->EvaluateConstraintInternal(df, 0) ;
    if (!range || range->IsNullRange() || !range->Left() || !range->Right()) {
        // Don't execute anything
        delete range ;
        return 0 ;
    }

    // Create the iterator
    // Start at the left, work towards the right
    VhdlValue *runner = range->Left()->Copy() ;
    char *runner_val ;

    VhdlBlockConfiguration *subblock_config ;

    while (1) {
        // Test for dead code too (created by exit statement in the loop)
        if (df && df->IsDeadCode()) break ;

        // VIPER #3899 : After fixing 3815, it is not needed to set null for iter id,
        // as we will produce warning for out of range index inside non-constant
        // branch. So remove this null value insertion.
        // VIPER #3600 (#3608) : Do not set runner value if we are in non-constant region or not within subprogram body
        //if (IsStaticElab() && df && (df->IsNonconstExpr() || !df->IsTemporaryDataFlow())) {
        // VIPER #7851 : If the dataflow is not in initial, we are inside process
        // We will not rely of value of iterator and execute the loop only once
        if (IsStaticElab() && df && !df->IsInInitial()) {
            _iter_id->SetValue(0) ; // Set null value
        } else {
            // Set the value of _id
            _iter_id->SetValue(runner->Copy()) ; // previous value will be destroyed
        }
        // Push the value of the runner on the named stack if we can expect (signal) declarations inside (if it is a 'generate' loop : if there is no dataflow)
        runner_val = (df) ? 0 : runner->Image() ;
        // VIPER #7683 : Pass runner val along with label
        PushNamedPath(stmt_label ? stmt_label->OrigName(): 0, runner_val) ;
        PushAttributeSpecificPath((stmt_label ? stmt_label->Name(): 0), 0, 0, runner_val) ;

        // VIPER 2012 execute the block config
        if (block_config) block_config->Elaborate() ;
        // Configure the block (if the iteration value is contained)
        // if (block_config) block_config->Configure(0,runner) ;
        subblock_config = (block_config) ? block_config->ExtractSubBlock(stmt_label, runner, 0) : 0 ;

        // VIPER #3367: We are going to process delcarative region, set this flag:
        VhdlDataFlow *decl_flow = (df) ? df : new VhdlDataFlow(0, 0, (stmt_label) ? stmt_label->LocalScope() : 0) ;
        // RD: make sure to store existing state of 'InInitial' :
        unsigned was_in_initial = (decl_flow) ? decl_flow->IsInInitial() : 0 ;
        if (!was_in_initial) decl_flow->SetInInitial() ;

        // Elaborate the statements
        unsigned i ;
        VhdlDeclaration *decl ;
        FOREACH_ARRAY_ITEM(declarations, i, decl) {
            decl->Elaborate(decl_flow) ;
        }

        // VIPER #3367: Done processing delcarative region, reset the flag:
        if (!was_in_initial) decl_flow->SetNotInInitial() ;
        if (!df) delete decl_flow ;

        VhdlStatement *stmt ;
        FOREACH_ARRAY_ITEM(statements, i, stmt) {
            stmt->Elaborate(df,subblock_config) ;
        }

        delete subblock_config ;
        // Resolve 'next' statements :
        if (df) df->Resolve(_next_label, this) ;

        // Pop the named path
        PopNamedPath() ;
        Strings::free(runner_val) ;
        PopAttributeSpecificPath() ;

        if (runner->Position()==range->Right()->Position()) break ; // We are done

        // Don't proceed if errors occurred.
        // Otherwise, we might run forever
        if (Message::ErrorCount()) break ;

        // Incr/Decr the iterator
        (void) runner->UnaryOper((range->Dir()==VHDL_to) ? VHDL_INCR : VHDL_DECR,this) ;

        if (IsStaticElab() && df && (df->IsNonconstExpr() || !df->IsTemporaryDataFlow())) {
            // VIPER #3507 : loop in non-constant region in static elab, break it after iterate once
            // VIPER #3899 : Mark data flow non-constant after one iteration, as
            // we should not rely any more on values of variables
            df->SetNonconstExpr() ;
            // VIPER #7286 : Iterate according to condition, donot rely on 'IsNonconstExpr'
            // break ; // Iterate once for non-constant region
        }
        // VIPER #7851: If the dataflow is not in initial, loop will be executed
        // once
        if (IsStaticElab() && df && !df->IsInInitial()) {
            df->SetNonconstExpr() ;
            break ;
        }
    }

    // Resolve 'exit' statements
    if (df) df->Resolve(_exit_label, this) ;

    // Delete the iterator variable value (set back to 0) since we will go out-of scope now.
    _iter_id->SetValue(0) ;

    delete runner ;
    delete range ;
    return 1 ; // always executed
}

unsigned VhdlWhileScheme::Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df)
{
    if (!statements) return 0 ; // Nothing to do
    if (vhdl_file::CheckForInterrupt(Linefile())) return 0 ; // Stop after an error or interrupt

    // VIPER : 2947/4152 - honor user loop limit, if set by user
    // VIPER #7568: Use user specific or default loop limit from runtime flag
    // Viper 8186: Use different loop limits to control number of loop iterations
    // for while loop in initial and non initial block. The loop_limit controls the
    // number of iterations in initial block. A zero value represents infinite iterations
    // nonconst_loop_limit is used for the limit of while loop iterations in non initial
    // block. Here also zero means infinite iterations
    //
    // Now if the initial block has a while loop with constant condition and the while
    // loop breaks by a break by evaluating a non-constant condition. Then the loop
    // will not stop at any loop limit and will continue till the breaking condition is true.
    unsigned loop_limit = RuntimeFlags::GetVar("vhdl_loop_limit") ;
    unsigned nonconst_loop_limit = RuntimeFlags::GetVar("vhdl_nonconst_loop_limit") ;

    // Viper 8186: For non initial block use the nonconst_loop_limit
    if (df && !df->IsInInitial()) loop_limit = nonconst_loop_limit ;

    unsigned loop_count = 0 ;

    // VIPER #7683 : Push named path
    if (stmt_label) PushNamedPath(stmt_label->OrigName()) ;
    // Test for dead code too (created by exit statement in the loop)
    while (1) {
        // Test for dead code too (created by exit statement in the loop)
        if (df && df->IsDeadCode()) break ;

        VhdlValue *condition = 0 ;
        unsigned is_condition_null = 0 ;

        // _condition can be 0 for an infinite loop (which should break by an exit statement)
        if (_condition) {
            condition = _condition->Evaluate(0,df,0) ;

            // If there is a while-condition, but there is no value result,
            // then we had a failed evaluation. Break from the loop in that case.
            // If condition is null it is infinite loop for static elaboration.
            if (!condition && IsStaticElab() && df) {
                df->SetNonconstExpr() ; // Mark that data flow contains non constant expression
                is_condition_null = 1 ;
            }
            if (!condition && IsRtlElab()) break ;

            // Do some tests on the condition
            if (condition && !condition->IsConstant()) {
                // issue 1506, issue 2350 : continue executing. Only error out if (non-constant) loop limit reached.
                if (nonconst_loop_limit && loop_count >= nonconst_loop_limit) {
                    _condition->Error("non-constant loop limit of %d exceeded", nonconst_loop_limit) ;
                    delete condition ;
                    break ; // get out of the iteration
                }

                // Inform user :
                if (!loop_count) { // first time in the loop
                    _condition->Info("non-constant loop found; will execute up to %d iterations",nonconst_loop_limit) ;
                }

                // non-constant loop condition is a conditional 'break' out of the loop.
                // We break out of the loop if the condition is false.
                // So, invert the condition :
                condition = condition->ToNonconstBit()->Invert(/*from*/_condition) ;
                if (df) df->DoGoto(_exit_label, condition, /*from*/this) ;
                // Proceed with elaboration
            }
            // Added second check for Viper #3681 to stop premature exit for
            // non-constant loop conditions
            if (condition && condition->IsConstant() && !condition->IsTrue()) {
                // Condition is FALSE : we are done
                delete condition ;
                break ; // done
            }
            delete condition ;
        } else {
            is_condition_null = 1 ;
        }

        loop_count++ ;

        // FIX ME : Do we need to configure this block ?
        // VIPER #3367: We are going to process delcarative region, set this flag:
        VhdlDataFlow *decl_flow = (df) ? df : new VhdlDataFlow(0, 0, (stmt_label) ? stmt_label->LocalScope() : 0) ;
        // RD: make sure to store existing state of 'InInitial' :
        unsigned was_in_initial = (decl_flow) ? decl_flow->IsInInitial() : 0 ;
        if (!was_in_initial) decl_flow->SetInInitial() ;

        unsigned i ;
        VhdlDeclaration *decl ;
        FOREACH_ARRAY_ITEM(declarations, i, decl) {
            decl->Elaborate(decl_flow) ;
        }

        // VIPER #3367: Done processing delcarative region, reset the flag:
        if (!was_in_initial) decl_flow->SetNotInInitial() ;
        if (!df) delete decl_flow ;

        VhdlStatement *stmt ;
        FOREACH_ARRAY_ITEM(statements, i, stmt) {
            stmt->Elaborate(df,block_config) ;
        }

        if (IsStaticElab() && df && (df->IsNonconstExpr() || !df->IsTemporaryDataFlow())) {
            // VIPER #3507 : loop in non-constant region in static elab, break it after iterate once
            // VIPER #3899 : Mark data flow non-constant after one iteration, as
            // we should not rely any more on values of variables
            df->SetNonconstExpr() ;
            // VIPER #7286 : Iterate according to condition, donot rely on 'IsNonconstExpr'
            // Viper 7737: Post Viper 7286 we now allow the loop to iterate in static elaboration.
            // This was done // for VhdlForScheme but a similar change for WhileScheme was done.
            // However if the condition is null then we break out of loop as otherwise
            // the while loop will run infinitely.
            if (is_condition_null) break ; // Iterate once for non-constant region
        }
        // VIPER #7851: If the dataflow is not in initial, loop will be executed
        // once
        if (IsStaticElab() && df && !df->IsInInitial()) {
            df->SetNonconstExpr() ;
            break ;
        }
        (void) is_condition_null ;

        // Resolve 'next' statements :
        if (df) df->Resolve(_next_label, this) ;

        // Don't proceed if errors occurred.
        // Otherwise, we might run forever
        if (Message::ErrorCount()) break ;

        // Don't proceed if iteration count is weirdly high (probably constant TRUE while condition).
        if (loop_limit && loop_count >= loop_limit) {
            // VIPER #2858 : Don't issue error in static elaboration. For non-static
            // loop inside function, static elaboration should return 0 without issuing error.
            if (IsRtlElab()) Error("non-static loop limit of %d exceeded", loop_limit) ;
            if (IsStaticElab()) {
                Warning("non-static loop limit of %d exceeded", loop_limit) ;
                if (df) df->SetNonconstExpr() ; // Mark that data flow contains non constant expression
            }
            break ;
        }
    }

    // Resolve 'exit' statements :
    if (df) df->Resolve(_exit_label, this) ;
    if (stmt_label) PopNamedPath() ; // VIPER #7683
    return 1 ; // always executed
}

unsigned VhdlIfScheme::Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df)
{
    if (!statements) return 0 ; // Nothing to do
    if (!_condition && !IsElseScheme()) return 0 ; // Nothing to do
    if (vhdl_file::CheckForInterrupt(Linefile())) return 0 ; // Stop after an error or interrupt

    VhdlValue *condition = _condition ? _condition->Evaluate(0,df,0): 0 ;
    if (_condition && !condition && IsStaticElab() && df) df->SetNonconstExpr() ; // Mark that this data flow contains non-constant expression
    //if (!condition) return ;

    if (_condition && condition && !condition->IsConstant()) {
        _condition->Error("generate condition is not constant") ;
        delete condition ;
        return 0 ;
    }

    // VIPER #7683 : Push named path
    if (stmt_label) PushNamedPath(stmt_label->OrigName()) ;
    PushAttributeSpecificPath(stmt_label ? stmt_label->Name():0) ;

    unsigned is_executed = 0 ;
    if (!_condition || (condition && condition->IsTrue())) {
        is_executed = 1 ;
        // VIPER 2012 : Elaborate the block config
        if (block_config) block_config->Elaborate() ;

        // Configure the block (if needed)
        VhdlBlockConfiguration *subblock_config = (block_config) ? block_config->ExtractSubBlock(stmt_label, 0, _alternative_label_id) : 0 ;

        // VIPER #3367: We are going to process delcarative region, set this flag:
        VhdlDataFlow *decl_flow = (df) ? df : new VhdlDataFlow(0, 0, (stmt_label) ? stmt_label->LocalScope() : 0) ;
        // RD: make sure to store existing state of 'InInitial' :
        unsigned was_in_initial = (decl_flow) ? decl_flow->IsInInitial() : 0 ;
        if (!was_in_initial) decl_flow->SetInInitial() ;

        unsigned i ;
        VhdlDeclaration *decl ;
        FOREACH_ARRAY_ITEM(declarations, i, decl) {
            decl->Elaborate(decl_flow) ;
        }

        // VIPER #3367: Done processing delcarative region, reset the flag:
        if (!was_in_initial) decl_flow->SetNotInInitial() ;
        if (!df) delete decl_flow ;

        VhdlStatement *stmt ;
        FOREACH_ARRAY_ITEM(statements, i, stmt) {
            stmt->Elaborate(df,subblock_config) ;
        }
        delete subblock_config ;
    }
    PopAttributeSpecificPath() ;
    if (stmt_label) PopNamedPath() ; // VIPER #7683
    delete condition ;
    return is_executed ;
}
VhdlIterScheme *VhdlCaseItemScheme::ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, VhdlDataFlow *df, VhdlConstraint const *case_expr_constraint, Set& overlap_check, VhdlIdDef const *case_expr_id, Map *& condition_id_value_map)
{
    verific_uint64 extra_count = 1 ;
    unsigned is_other = VhdlIterScheme::ElaborateChoicesInternal(_choices, choice_values, choice_positions, case_expr, df, 0, case_expr_constraint, overlap_check, case_expr_id, condition_id_value_map, 0, 1, extra_count) ;
    if (is_other) return this ; // If choice is 'others' return this
    return 0 ;
}

void VhdlElsif::ElaborateElsif(VhdlDataFlow *df)
{
    // Just execute the statements. Condition is handled outside of here
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) stmt->Elaborate(df,0) ;
}

FILE *
VhdlFileOpenInfo::Elaborate(VhdlDataFlow *df)
{
    if (df && df->IsDeadCode()) return 0 ; // we are in dead code. Don't execute anything.

    // Open the file if the dataflow is in 'initial' mode.
    // Also, if there is no dataflow, assume that we are in 'initial' mode.
    if (df && !df->IsInInitial()) {
        return 0 ; // in non-initial mode, don't do anything.
    }

    // If we have no file name expression, there is nothing we can do :
    if (!_file_logical_name) return 0 ;

    // First, find the 'open' mode :
    const char *open_mode = "r" ; // default : read
    if (_file_open) {
        // VHDL 93 open mode : 'file_open' is an expression (of enumerated type 'file_open'). Evaluate it :
        VhdlValue *file_open_val = _file_open->Evaluate(0,df,0) ;
        // Assume it is constant (and thus one of the values of enum type 'file_open'
        verific_int64 file_open_pos = (file_open_val) ? file_open_val->Position() : 0 /*default is an error*/;

        // Determine 'open_mode' :
        switch (file_open_pos) {
        case 0 : /* read_mode */  open_mode = "r" ; break ;
        case 1 : /* write_mode */ open_mode = "w" ; break ;
        case 2 : /* append_mode */ open_mode = "a" ; break ;
        default : break ; // non-constant or other problem. Open in 'read' mode (default).
        }
        // delete the value
        delete file_open_val ;
    } else if (_open_mode) {
        // VHDL 87 open mode : is a token : Only 'in' or 'out' are allowed in VHDL 87.
        switch (_open_mode) {
        case VHDL_in :  /* read_mode */  open_mode = "r" ; break ;
        case VHDL_out : /* write_mode */ open_mode = "w" ; break ;
        default : break ; // don't know this open mode. Open in 'read' mode (default)
        }
    }

    // Now get the file name : Elaborate '_file_logical_name'.
    VhdlValue *file_name_val = _file_logical_name->Evaluate(0/*target_constraint?*/, df, 0) ;

    // Constant file name check
    if (!file_name_val || !file_name_val->IsConstant()) {
        _file_logical_name->Error("expression is not constant") ; // maybe better message ?
        delete file_name_val ;
        return 0 ;
    }

    // Turn this into a string. Only type 'string' is allowed (array of character) so use StringImage :
    char *file_name_image = file_name_val->StringImage() ;
    delete file_name_val ;
    if (!file_name_image) return 0 ; // should never happen.

    // First strip the "'s from the file name
    const char *file_name = file_name_image ;
    if (*file_name_image=='"') {
        // Set 'file_name' pointer to after the leading " :
        file_name = file_name_image+1 ;
        // and cut off the trailing ".
        file_name_image[Strings::len(file_name_image)-1] = '\0' ;
    }

    // Now attempt to open the file :
    FILE *file = 0 ;

    // Exception : textio file names "STD_INPUT" and "STD_OUTPUT" open 'stdin' and 'stdout' :
    // Don't open these, (and don't close them either).
    if (Strings::compare(file_name, "STD_INPUT")) {
        file = stdin ;
    } else if (Strings::compare(file_name, "STD_OUTPUT")) {
        file = stdout ;
    } else {
        file = fopen(file_name, open_mode) ;
        if (!file || !FileSystem::IsFile(file_name)) {
            if (file) { fclose(file) ; file = 0 ; }
            Error("cannot open file '%s'",file_name) ; // MessageID !
        }
    }

    // cleanup
    Strings::free(file_name_image) ;

    // Return the file. It is now 'open'.
    return file ;
}

VhdlCaseStatementAlternative *VhdlCaseStatementAlternative::ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, VhdlDataFlow *df, VhdlConstraint const *case_expr_constraint, Set& overlap_check, VhdlIdDef const *case_expr_id, Map *& condition_id_value_map, unsigned is_matching_case, verific_uint64& extra_count, unsigned populate_dc_positions /*=0*/)
{
    unsigned is_other = VhdlIterScheme::ElaborateChoicesInternal(_choices, choice_values, choice_positions, case_expr, df, 0, case_expr_constraint, overlap_check, case_expr_id, condition_id_value_map, is_matching_case, 1, extra_count, populate_dc_positions) ;
    if (is_other) return this ;
    return 0 ;
}
unsigned VhdlIterScheme::ElaborateChoicesInternal(const Array *choices, Map &choice_values, Map &choice_positions, VhdlValue *case_expr, VhdlDataFlow *df, VhdlTreeNode const* from, VhdlConstraint const *case_expr_constraint, Set& overlap_check, VhdlIdDef const *case_expr_id, Map *& condition_id_value_map, unsigned is_matching_case,  unsigned is_case_stmt, verific_uint64& extra_count, unsigned populate_dc_positions /*=0*/)
{
    if (is_case_stmt && !case_expr && !case_expr_constraint) return 0 ;
    // VERIFIC_ASSERT(case_expr) ; // need it, if only for 'size'

    // Evaluate choices one-by-one

    // 'choice_values' ::
    // For each choice store the choice (parse-tree-node) as key in the map,
    // with the applicable DataFlow ('df') as value.
    // All choices (range choice or single choice) will create one entry.
    // OR choices | will create as many inserts as they represent constant choice values.
    // So, the DataFlow pointer (value field) could show up many times in the 'choices' table.

    // 'choice_positions' ::
    // Also, for expressions that fit in an integer value, we calculate the numeric
    // equivalent and store this as a key in the 'positions' table. Value is DataFlow again.
    // This 'positions' table serves two purposes :
    //   - We can check (by hash) if an entry already occurred earlier (VHDL requires uniqueness).
    //   - We can use the numeric value later, as the input number into N-to-1 muxes.

    // For 'others', just return 'this' ; 'others' is handled separately, since there is
    // no choice expression.
    VhdlDiscreteRange *choice ;
    VhdlValue *choice_val ;
    char *image ;
    unsigned i ;
    unsigned is_constant = case_expr ? case_expr->IsConstant() : 0 ;
    FOREACH_ARRAY_ITEM(choices, i, choice) {
        if (!choice) continue ;

        if (choice->IsOthers()) {
            //return this ;
            return 1 ;
        }

        if (choice->IsRange()) {
            // Range choice

            // Evaluate the range :
            VhdlConstraint *range = choice->EvaluateConstraintInternal(df, 0) ;
            if (!range && IsStaticElab() && df) df->SetNonconstExpr() ; // Mark that this data flow contains non constant expression

            // Since a range must be a discrete range (guaranteed by type-inference), we CAN
            // check easily (if the case expression is constant) if the result is in the range :
            if (is_constant) {
                if (!range) continue ; // some problem with this choice.
                if (case_expr && range->Contains(case_expr)) {
                    // this choice applies.
                    (void) choice_values.Insert(choice, is_case_stmt ? (void*)df : (void*)from) ;
                }
            } else {
                // Regular range with nonconstant case-expression. Insert, for resolving later.
                (void) choice_values.Insert(choice, is_case_stmt ? (void*)df : (void*)from) ;
            }

            // if (range && case_expr_constraint) range->CheckAgainst(case_expr_constraint, this) ;

            // Viper #3640
            if (range && range->Length() < 1000) {
                unsigned idx ;
                for (idx = 0 ; idx < range->Length() ; ++idx) {
                    choice_val = range->ValueOf(idx) ;
                    image = choice_val->Image() ;

                    if (case_expr_constraint) {
                        unsigned is_array = case_expr_constraint->IsArrayConstraint() ;
                        unsigned array_len = (unsigned) case_expr_constraint->Length() ;
                        if ((!is_array && !case_expr_constraint->Contains(choice_val)) || (is_array && array_len != choice_val->NumElements())) {
                            char *val_image = choice_val->Image() ;
                            if (!is_array) {
                                choice->Warning("case statement choice %s is outside the range of the select", val_image) ;
                            } else {
                                choice->Error("choice %s should have %d elements", val_image, array_len) ;
                            }
                            Strings::free(val_image) ;
                        }
                    }

                    if (!overlap_check.Insert((void*)image)) {
                        choice->Error("choice %s is already covered", image) ;
                        Strings::free(image) ;
                    }
                    delete choice_val ;
                }
            }

            delete range ;
        } else {
            // Single choice ; Evaluate the expression
            choice_val = choice->Evaluate(0,df,0) ;
            if (!choice_val && IsStaticElab() && df) df->SetNonconstExpr() ; // Mark that this data flow contains non constant expression
            if (!choice_val) continue ;

            if (!choice_val->IsConstant()) {
                choice->Error("choice is not constant") ;
                delete choice_val ;
                continue ;
            }

            if (case_expr_id && choices->Size() == 1) { // Viper #5553
                if (!condition_id_value_map) condition_id_value_map = new Map(POINTER_HASH) ;
                (void) condition_id_value_map->Insert(case_expr_id, choice_val->Copy(), 0, 1) ;
            }

            if (case_expr_constraint) {
                unsigned is_array = case_expr_constraint->IsArrayConstraint() ;
                unsigned array_len = (unsigned)case_expr_constraint->Length() ;
                if ((!is_array && !case_expr_constraint->Contains(choice_val)) || (is_array && array_len != choice_val->NumElements())) {
                    char *val_image = choice_val->Image() ;
                    if (!is_array) {
                        choice->Warning("case statement choice %s is outside the range of the select", val_image) ;
                    } else {
                        choice->Error("choice %s should have %d elements", val_image, array_len) ;
                    }
                    Strings::free(val_image) ;
                }
            }

#if 0

            // Check number of elements (Array values only)
            if (case_expr && (choice_val->NumElements() != case_expr->NumElements())) {
                // Array values only
                char *val_image = choice_val->Image() ;
                choice->Error("choice %s should have %d elements", val_image, case_expr->NumElements()) ;
                Strings::free(val_image) ;
                delete choice_val ;
                continue ;
            }
#endif

            image = choice_val->Image() ;
            if (!overlap_check.Insert((void*)image)) {
                choice->Error("choice %s is already covered", image) ;
                Strings::free(image) ;
            }

            // Test for constant
            if (case_expr && is_constant) {
                // For constant case expressions, use a VHDL equal to compare them.
                // Both case expression and choice value are constant, so we can use IsEqual :
                unsigned matching_equal = 0 ;
                // According to VHDL-2008 LRM section 10.9, for matching case statement
                // case_expr ?= choice should be checked :
                // and if result is 'TRUE' or '1', corresponding case statement alternative will
                // be chosen
                if (is_matching_case) {
                    VhdlValue *result = case_expr->Copy()->BinaryOper(choice_val->Copy(), VHDL_matching_equal, choice) ;
                    matching_equal = (result && result->IsTrue()) ; // Only 'True' and '1' returns 1
                    delete result ;
                }
                if ((is_matching_case && matching_equal) || (!is_matching_case && choice_val->IsEqual(case_expr))) {
                    // Insert this choice
                    (void) choice_values.Insert(choice,is_case_stmt ? (void*)df : (void*)from) ;
                }
                delete choice_val ;
                continue ; // don't use the 'positions' array.
            }

            // The following check was changed for VIPER 1751
            // We should really consider all metalogical values
            // like 'L' 'H' etc and not only 'X' and 'Z'
            if ((is_matching_case && choice_val->HasUXZW()) || (!is_matching_case && choice_val->IsMetalogical())) {
                if (!case_expr) {
                    delete choice_val ;
                    continue ;
                }

                // meta values in non-constant compare
                char *val_image = choice_val->Image() ;
                choice->Warning("choice with meta-value %s is ignored for synthesis", val_image) ;
                Strings::free(val_image) ;
                delete choice_val ;
                continue ;
            }

            // Choices that fit in integer are hashed by position
            if (choice_val->NumBits() <= sizeof(unsigned)*8) {
                if (!choice_val->IsMetalogical()) {
                    // Insert into 'positions' array
                    // FIXME: Containers like Map can't handle more than 32 bit values properly
                    // in 32 bit machines. So, we use long here. May truncate the value:
                    long pos = (long)choice_val->Integer() ;
                    // Viper 7157: For case store df for variable/signal assignment store the selected expr or waveform
                    if (!choice_positions.Insert((void*)pos,is_case_stmt ? (void*)df : (void*)from)) {
                        delete choice_val ;
                        continue ;
                    }
                } else if (populate_dc_positions) { // only metalogical value '-' for matching case/select can come here
                    // Viper 7157: decision was made from calling function to create equals and hence we need to populate positions.
                    Array values(1) ;
                    choice_val->PopulateValues(&values) ;
                    extra_count += values.Size() - 1 ;
                    VhdlValue* val ;
                    unsigned j ;
                    FOREACH_ARRAY_ITEM(&values, j, val) {
                        if (!val) continue ;
                        long pos = (long)val->Integer() ;
                        delete val ;
                        // Viper 7157: For case store df for variable/signal assignment store the selected expr or waveform
                        if (!choice_positions.Insert((void*)pos,is_case_stmt ? (void*)df : (void*)from)) {
                            continue ;
                        }
                    }
                } else {
                    // Just calculate the size contribution
                    extra_count += choice_val->RepresentCount() - 1 ;
                }
            }

            delete choice_val ;

            // Insert this choice
            (void) choice_values.Insert(choice,is_case_stmt ? (void*)df : (void*)from) ;
        }
    }
    return 0 ;
}

void VhdlCaseStatementAlternative::ElaborateAlternative(VhdlDataFlow *df)
{
    // Elaborate statements
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) stmt->Elaborate(df,0) ;
}

VhdlValue *VhdlConditionalWaveform::ElaborateConditionalWaveform(VhdlExpression *target, VhdlDataFlow *df)
{
    // If there is no waveform, just return the previous one.
    if (!_waveform) return 0 ;

    // Verify that _waveform contains at-least one VhdlExpression.  This should be caught in yacc
    if (!_waveform->Size()) return 0 ;

    // Waveform is an array. Take only the last expression
    if (IsRtlElab() && _waveform->Size() > 1) {
        Warning("synthesis ignores all but the first waveform") ;
    }

    // Evaluate first waveform :
    VhdlExpression *expr = (VhdlExpression*)_waveform->GetFirst() ;
    if (!expr) return 0 ;

    // Evaluate the expression
    VhdlValue *value = 0 ;
    if (expr->IsUnaffected()) {
        // Take the value by evaluating the target name
        value = (target) ? target->Evaluate(0,df,0) : 0 ;
    } else {
        // Take the value by evaluating the expression
        // Pass the target constraint in if the expression is a aggregate.
        VhdlConstraint *target_constraint = (expr->IsAggregate() && target) ? target->EvaluateConstraintInternal(df, 0) : 0 ;
        value = expr->Evaluate(target_constraint,df,0) ;
        // VIPER #3658 (3659): Check value against target only when condition can be true.
        if (!value && IsStaticElab()) { // VIPER #2858 : Check size mismatching between target and value
            if (!target_constraint && target) target_constraint = target->EvaluateConstraintInternal(df, 0) ;
            if (target_constraint && !target_constraint->IsUnconstrained()) {
                unsigned i ;
                VhdlConstraint *value_constraint = 0 ;
                FOREACH_ARRAY_ITEM(_waveform, i, expr) {
                    // VIPER #5011 : Do not check constraint for aggregate, it is already
                    // validated in Evaluate
                    //value_constraint = (expr && !expr->IsAggregate()) ? expr->EvaluateConstraintInternal(df, target_constraint): 0 ;
                    value_constraint = (expr) ? expr->EvaluateConstraintInternal(df, target_constraint): 0 ;
                    if (value_constraint && !value_constraint->IsUnconstrained()) (void) target_constraint->CheckAgainst(value_constraint, expr, df) ;
                    delete value_constraint ;
                }
            }
        }
        delete target_constraint ;
    }

    return value ;
}

VhdlValue *VhdlConditionalExpression::ElaborateConditionalExpression(VhdlExpression *target, VhdlDataFlow *df)
{
    // If there is no waveform, just return the previous one.
    if (!_expr) return 0 ;

    // Evaluate the expression
    VhdlValue *value = 0 ;
    if (_expr->IsUnaffected()) {
        // Take the value by evaluating the target name
        value = (target) ? target->Evaluate(0,df,0) : 0 ;
    } else {
        // Take the value by evaluating the expression
        // Pass the target constraint in if the expression is a aggregate.
        VhdlConstraint *target_constraint = (_expr->IsAggregate() && target) ? target->EvaluateConstraintInternal(df, 0) : 0 ;
        value = _expr->Evaluate(target_constraint,df,0) ;
        // VIPER #3658 (3659): Check value against target only when condition can be true.
        if (IsStaticElab()) { // VIPER #2858 : Check size mismatching between target and value
            if (!target_constraint && target) target_constraint = target->EvaluateConstraintInternal(df, 0) ;
            if (target_constraint && !target_constraint->IsUnconstrained()) {
                // VIPER #5011 : Do not check constraint for aggregate, it is already
                // validated in Evaluate
                //VhdlConstraint *value_constraint = (_expr && !_expr->IsAggregate()) ? _expr->EvaluateConstraintInternal(0): 0 ;
                VhdlConstraint *value_constraint = (_expr) ? _expr->EvaluateConstraintInternal(0, target_constraint): 0 ;
                if (value_constraint && !value_constraint->IsUnconstrained()) (void) target_constraint->CheckAgainst(value_constraint, _expr, df) ;
                delete value_constraint ;
            }
        }
        delete target_constraint ;
    }

    return value ;
}

VhdlSelectedWaveform *VhdlSelectedWaveform::ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, const VhdlConstraint *case_expr_constraint, VhdlDataFlow *df, Set& overlap_check, unsigned is_matching, verific_uint64& extra_count, unsigned populate_dc_positions)
{
    Map * condition_id_value_map = 0 ;
    unsigned is_other = VhdlIterScheme::ElaborateChoicesInternal(_when_choices, choice_values, choice_positions, case_expr, df,  this, case_expr_constraint, overlap_check, 0, condition_id_value_map, is_matching, 0, extra_count, populate_dc_positions) ;
    if (is_other) return this ;
    return 0 ;

    // Removing the code duplication below. Using static function VhdlIterScheme::ElaborateChoicesInternal to do the work.
    // This was already used in VhdlCaseStatementAlternative::ElaborateChoices. Viper 7157
#if 0
    //if (!case_expr) return 0 ;
    //VERIFIC_ASSERT(case_expr) ; // need it, if only for 'size'

    // Evaluate choices one-by-one
    // For each choice store the 'choice' as key in the map,
    // with 'this' case-alternative as map-value.
    // Range choices are inserted as-is too.
    // OR choices | will create as many inserts as they represent constant choice values.
    // For 'others', just return it.

    // Elaborate the choice, and store its numeric value->alternative in 'positions'
    // if it is smaller than 32 bits. This allows checks for uniqueness, and provides
    // input-index number for N-to-1 mux resolving.
    VhdlDiscreteRange *choice ;
    VhdlValue *choice_val ;
    unsigned i ;
    unsigned is_constant = (case_expr) ? case_expr->IsConstant(): 0 ;

    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (!choice) continue ;

        if (choice->IsOthers()) return this ;

        if (choice->IsRange()) {
            // Range choice

            // Since a range must be a discrete range (guaranteed by type-inference), we CAN
            // check easily (if the case expression is constant) if the result is in the range :
            if (is_constant) {
                // Evaluate the range :
                VhdlConstraint *range = choice->EvaluateConstraintInternal(df, 0) ;
                if (!range) continue ; // some problem with this choice.
                if (range->Contains(case_expr)) {
                    // this choice applies.
                    (void) choice_values.Insert(choice, this) ;
                }
                delete range ;
            } else {
                // Regular range with nonconstant case-expression. Insert, for resolving later.
                (void) choice_values.Insert(choice, this) ;
            }

            // FIX ME : We are not testing if a value in the range is already covered
            // (in 'positions' hash table)
        } else {
            // Single choice ; Evaluate the expression
            choice_val = choice->Evaluate(0,df,0) ;
            if (!choice_val) continue ;

            if (!choice_val->IsConstant()) {
                choice->Error("choice is not constant") ;
                delete choice_val ;
                continue ;
            }

            // VIPER #3652 : Extract number of elements in case condition from
            // constraint of case condition
            unsigned case_expr_ele_count = (case_expr) ? case_expr->NumElements() : ((case_expr_constraint && !case_expr_constraint->IsUnconstrained() && case_expr_constraint->IsArrayConstraint()) ? (unsigned) case_expr_constraint->Length(): 0) ;
            // VIPER #3995 : Check number of elements if either 'case_expr' or 'case_expr_constraint' is present
            // Check number of elements (Array values only)
            if ((case_expr || case_expr_constraint) && choice_val->NumElements() != case_expr_ele_count) {
                // Array values only
                char *image = choice_val->Image() ;
                choice->Error("choice %s should have %d elements", image, case_expr_ele_count) ;
                Strings::free(image) ;
                delete choice_val ;
                continue ;
            }

            // Test for constant
            if (is_constant && case_expr) {
                // For constant case expressions, use a VHDL equal to compare them.
                // Both case expression and choive value are constant, so we can use IsEqual :
                unsigned matching_equal = 0 ;
                // According to VHDL-2008 LRM section 10.9, for matching case statement
                // case_expr ?= choice should be checked :
                // and if result is 'TRUE' or '1', corresponding case statement alternative will
                // be chosen
                if (is_matching) {
                    VhdlValue *result = case_expr->Copy()->BinaryOper(choice_val->Copy(), VHDL_matching_equal, choice) ;
                    matching_equal = (result && result->IsTrue()) ; // Only 'True' and '1' returns 1
                    delete result ;
                }
                //if (choice_val->IsEqual(case_expr)) {
                if ((is_matching && matching_equal) || (!is_matching && choice_val->IsEqual(case_expr))) {
                    // Insert this choice
                    (void) choice_values.Insert(choice,this) ;
                }
                delete choice_val ;
                continue ; // don't use the 'positions' array.
            }

            if (choice_val->HasX()) {
                // meta values in non-constant compare
                char *image = choice_val->Image() ;
                choice->Warning("choice with meta-value %s is ignored for synthesis", image) ;
                Strings::free(image) ;
                delete choice_val ;
                continue ;
            }

            // Choices that fit in integer (all scalars and all arrays smaller that 32 elements) are hashed by position
            if (choice_val->NumBits() <= sizeof(unsigned)*8) {
                // Insert into 'positions' array
                // FIXME: Containers like Map can't handle more than 32 bit values properly
                // in 32 bit machines. So, we use long here. May truncate the value:
                long pos = (long)choice_val->Integer() ;
                if (!choice_positions.Insert((void*)pos,this)) {
                    // this choice is already covered !
                    // For Single-bit : don't error out on bit-encoded values.
                    // Happens 'L' and '0' both represent 0, and would
                    // error out constantly
                    if (!choice_val->IsBit()) {
                        char *image = choice_val->Image() ;
                        choice->Error("choice %s is already covered", image) ;
                        Strings::free(image) ;
                    }
                    delete choice_val ;
                    continue ;
                }
            }
            delete choice_val ;

            // Insert this choice
            (void) choice_values.Insert(choice,this) ;
        }
    }

    return 0 ;
#endif
}

VhdlValue *
VhdlSelectedWaveform::ElaborateSelectedWaveform(VhdlConstraint *target_constraint, VhdlExpression *target, VhdlDataFlow *df)
{
    VERIFIC_ASSERT(_waveform) ;

    // Verify that _waveform contains at-least one VhdlExpression.  This should be caught in yacc.
    VERIFIC_ASSERT(_waveform->Size());

    // Take only the first waveform
    // Waveform is an array. Take only the last expression
    if (IsRtlElab() && _waveform->Size() > 1) {
        Warning("synthesis ignores all but the first waveform") ;
    }

    VhdlExpression *expr = (VhdlExpression*)_waveform->GetFirst() ;

    VhdlValue *result ;
    if (expr->IsUnaffected()) {
        // Take the value from the target ; value will 'hold' (unaffected)
        result = target->Evaluate(0,df,0) ;
    } else {
        // Take the value from the expression
        result = expr->Evaluate(target_constraint,df,0) ;
    }
    return result ;
}

VhdlSelectedExpression *VhdlSelectedExpression::ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, const VhdlConstraint *case_expr_constraint, VhdlDataFlow *df, Set& overlap_check,  unsigned is_matching, verific_uint64& extra_count, unsigned populate_dc_positions /*= 0*/)
{
    Map * condition_id_value_map = 0 ;
    unsigned is_other = VhdlIterScheme::ElaborateChoicesInternal(_when_choices, choice_values, choice_positions, case_expr, df, this, case_expr_constraint, overlap_check, 0, condition_id_value_map, is_matching, 0, extra_count, populate_dc_positions) ;
    if (is_other) return this ;
    return 0 ;
    // Removing the code duplication below. Using static function VhdlIterScheme::ElaborateChoicesInternal to do the work.
    // This was already used in VhdlCaseStatementAlternative::ElaborateChoices : Viper 7157
#if 0
    //if (!case_expr) return 0 ;
    //VERIFIC_ASSERT(case_expr) ; // need it, if only for 'size'

    // Evaluate choices one-by-one
    // For each choice store the 'choice' as key in the map,
    // with 'this' case-alternative as map-value.
    // Range choices are inserted as-is too.
    // OR choices | will create as many inserts as they represent constant choice values.
    // For 'others', just return it.

    // Elaborate the choice, and store its numeric value->alternative in 'positions'
    // if it is smaller than 32 bits. This allows checks for uniqueness, and provides
    // input-index number for N-to-1 mux resolving.
    VhdlDiscreteRange *choice ;
    VhdlValue *choice_val ;
    unsigned i ;
    unsigned is_constant = (case_expr) ? case_expr->IsConstant(): 0 ;

    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (!choice) continue ;

        if (choice->IsOthers()) return this ;

        if (choice->IsRange()) {
            // Range choice

            // Since a range must be a discrete range (guaranteed by type-inference), we CAN
            // check easily (if the case expression is constant) if the result is in the range :
            if (is_constant) {
                // Evaluate the range :
                VhdlConstraint *range = choice->EvaluateConstraintInternal(df, 0) ;
                if (!range) continue ; // some problem with this choice.
                if (range->Contains(case_expr)) {
                    // this choice applies.
                    (void) choice_values.Insert(choice, this) ;
                }
                delete range ;
            } else {
                // Regular range with nonconstant case-expression. Insert, for resolving later.
                (void) choice_values.Insert(choice, this) ;
            }

            // FIX ME : We are not testing if a value in the range is already covered
            // (in 'positions' hash table)
        } else {
            // Single choice ; Evaluate the expression
            choice_val = choice->Evaluate(0,df,0) ;
            if (!choice_val) continue ;

            if (!choice_val->IsConstant()) {
                choice->Error("choice is not constant") ;
                delete choice_val ;
                continue ;
            }

            // VIPER #3652 : Extract number of elements in case condition from
            // constraint of case condition
            unsigned case_expr_ele_count = (case_expr) ? case_expr->NumElements() : ((case_expr_constraint && !case_expr_constraint->IsUnconstrained() && case_expr_constraint->IsArrayConstraint()) ? (unsigned) case_expr_constraint->Length(): 0) ;
            // VIPER #3995 : Check number of elements if either 'case_expr' or 'case_expr_constraint' is present
            // Check number of elements (Array values only)
            if ((case_expr || case_expr_constraint) && choice_val->NumElements() != case_expr_ele_count) {
                // Array values only
                char *image = choice_val->Image() ;
                choice->Error("choice %s should have %d elements", image, case_expr_ele_count) ;
                Strings::free(image) ;
                delete choice_val ;
                continue ;
            }

            // Test for constant
            if (is_constant && case_expr) {
                // For constant case expressions, use a VHDL equal to compare them.
                // Both case expression and choive value are constant, so we can use IsEqual :
                unsigned matching_equal = 0 ;
                // According to VHDL-2008 LRM section 10.9, for matching case statement
                // case_expr ?= choice should be checked :
                // and if result is 'TRUE' or '1', corresponding case statement alternative will
                // be chosen
                if (is_matching) {
                    VhdlValue *result = case_expr->Copy()->BinaryOper(choice_val->Copy(), VHDL_matching_equal, choice) ;
                    matching_equal = (result && result->IsTrue()) ; // Only 'True' and '1' returns 1
                    delete result ;
                }
                //if (choice_val->IsEqual(case_expr)) {
                if ((is_matching && matching_equal) || (!is_matching && choice_val->IsEqual(case_expr))) {
                    // Insert this choice
                    (void) choice_values.Insert(choice,this) ;
                }
                delete choice_val ;
                continue ; // don't use the 'positions' array.
            }

            if (choice_val->HasX()) {
                // meta values in non-constant compare
                char *image = choice_val->Image() ;
                choice->Warning("choice with meta-value %s is ignored for synthesis", image) ;
                Strings::free(image) ;
                delete choice_val ;
                continue ;
            }

            // Choices that fit in integer (all scalars and all arrays smaller that 32 elements) are hashed by position
            if (choice_val->NumBits() <= sizeof(unsigned)*8) {
                // Insert into 'positions' array
                // FIXME: Containers like Map can't handle more than 32 bit values properly
                // in 32 bit machines. So, we use long here. May truncate the value:
                long pos = (long)choice_val->Integer() ;
                if (!choice_positions.Insert((void*)pos,this)) {
                    // this choice is already covered !
                    // For Single-bit : don't error out on bit-encoded values.
                    // Happens 'L' and '0' both represent 0, and would
                    // error out constantly
                    if (!choice_val->IsBit()) {
                        char *image = choice_val->Image() ;
                        choice->Error("choice %s is already covered", image) ;
                        Strings::free(image) ;
                    }
                    delete choice_val ;
                    continue ;
                }
            }
            delete choice_val ;

            // Insert this choice
            (void) choice_values.Insert(choice,this) ;
        }
    }

    return 0 ;
#endif
}

VhdlValue *
VhdlSelectedExpression::ElaborateSelectedExpression(VhdlConstraint *target_constraint, VhdlExpression *target, VhdlDataFlow *df)
{
    VERIFIC_ASSERT(_expr) ;
    VhdlValue *result = 0 ;
    if (_expr->IsUnaffected()) {
        // Take the value from the target ; value will 'hold' (unaffected)
        result = target->Evaluate(0,df,0) ;
    } else {
        // Take the value from the expression
        result = _expr->Evaluate(target_constraint,df,0) ;
    }
    return result ;
}

void VhdlBlockGenerics::Elaborate()
{
    if (!_block_label) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // Initialize Generics
    VhdlInterfaceDecl *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        decl->Initialize() ; // Constraints only. Set all values to 0.
    }

    // Here we set actuals from block configurations

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, _block_label->LocalScope()) ;
    df.SetInInitial() ;

    // Set actual generic values
    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    VhdlConstraint *constraint ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) {
        if (!assoc) continue ;

        // Find the formal.
        if (assoc->IsAssoc() ) {
            // Find the formal :
            id = assoc->FindFullFormal() ;
            if (!id) {
                // Partial association....
                // If formal is not constrained, set proper constraint to it
                VhdlNode::EvaluateConstraintForPartialAssoc(_generic_map_aspect, assoc, &df) ;
                assoc->PartialAssociation(0, 0) ;
                continue ;
            }
        } else {
            // positional association
            id = _block_label->GetGenericAt(i) ;
        }
        if (!id) continue ; // type-inference checks failed

        // Standard handling of actual-formal association
        constraint = id->Constraint() ;
        if (id->IsGenericType()) {
            // VIPER #5918 : Formal is interface_type, we need to calculate constraint of it from
            // actual and it cannot have any value
            VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
            constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, 0):0 ;
            if (constraint) {
                id->SetConstraint(constraint) ;
                id->SetValue(0) ;
            }
            VhdlIdDef *actual_type = actual_part ? actual_part->TypeInfer(0, 0, VHDL_range, 0): 0 ;
            id->SetActualId(actual_type,actual_part ? actual_part->GetAssocList() : 0) ;
            continue ;
        }
        if (id->IsSubprogram()) {
            // Formal is generic subprogram, actual can only be subprogram name which
            // conforms with formal
            VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
            VhdlIdDef *actual_subprogram = actual_part ? actual_part->SubprogramAspect(id): 0 ;
            if ((!actual_subprogram && actual_part) || (actual_subprogram && !actual_subprogram->IsSubprogram())) {
                assoc->Error("subprogram name should be specified as actual for formal generic subprogram %s", id->Name()) ;
            }
            if (actual_subprogram && !id->IsHomograph(actual_subprogram)) {
                assoc->Error("subprogram %s does not conform with its declaration", actual_subprogram->Name()) ;
            }
            if (actual_subprogram) id->SetActualId(actual_subprogram, 0) ;
            continue ;
        }
        if (id->IsPackage()) {
            // Formal is a generic package, actual can be a package
            VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
            VhdlIdDef *actual_package = actual_part ? actual_part->EntityAspect(): 0 ;
            if ((!actual_package && actual_part) || (actual_package && !actual_package->IsPackage())) {
                assoc->Error("package name should be specified as actual for formal generic package %s", id->Name()) ;
            }
            if (actual_package) id->SetElaboratedPackageId(actual_package) ;
            continue ;
        }
        if (!constraint) continue ; // Something bad happened in initialization

        val = assoc->Evaluate(constraint, &df, 0) ;
        if (!val) continue ;

        if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

        if (!val->CheckAgainst(constraint,assoc, 0)) {
            delete val ;
            continue ;
        }

        // Set the initial value
        id->SetValue(val) ;
    }

    // Check that all generics have a value now, or set initial value
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        decl->InitialValues(0, 0, &df, _present_scope) ;
    }
}

void VhdlBlockPorts::Elaborate()
{
    if (!_block_label) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // Initialize ports
    VhdlInterfaceDecl *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        decl->Initialize() ; // Create Constraints only. Set all values to 0.
    }

    // Here we set actuals from block configurations
    // Set actual port values
    // And make the associations
    // Set actual port values
    // CHECK ME : We do this as we do generics. This might be incorrect
    // since ports have direction and are non-constant..
    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id ;
    VhdlConstraint *constraint ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, assoc) {
        if (!assoc) continue ;

        // Find the formal.
        if (assoc->IsAssoc() ) {
            // Find the formal :
            id = assoc->FindFullFormal() ;
            if (!id) {
                // Partial association....
                // If formal is not constrained, set proper constraint to it
                VhdlNode::EvaluateConstraintForPartialAssoc(_port_map_aspect, assoc, 0) ;
                assoc->PartialAssociation(0, 0) ;
                continue ;
            }
        } else {
            // positional association
            id = _block_label->GetPortAt(i) ;
        }
        if (!id) continue ; // type-inference checks failed

        // Standard handling of actual-formal association
        constraint = id->Constraint() ;
        if (!constraint) continue ; // something bad happened in initialization.

        val = assoc->Evaluate(constraint,0,0) ;
        if (!val) continue ;

        if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

        if (!val->CheckAgainst(constraint,assoc, 0)) {
            delete val ;
            continue ;
        }

        // Set the initial value
        id->SetValue(val) ;
    }

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, _block_label->LocalScope()) ;
    df.SetInInitial() ;

    // Check that all ports have a value now, or set initial value
    FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
        decl->InitialValues(0, 0, &df, _present_scope) ;
    }
}

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
void VhdlFileOpenInfo::InitializeForVariableSlice()
{
    if (_file_open) _file_open->InitializeForVariableSlice() ;
    if (_file_logical_name) _file_logical_name->InitializeForVariableSlice() ;
}

void VhdlWhileScheme::InitializeForVariableSlice()
{
    if (_condition) _condition->InitializeForVariableSlice() ;
}

void VhdlForScheme::InitializeForVariableSlice()
{
    if (_id) _id->InitializeForVariableSlice() ;
    if (_range) _range->InitializeForVariableSlice() ;
}

void VhdlIfScheme::InitializeForVariableSlice()
{
    if (_condition) _condition->InitializeForVariableSlice() ;
}
void VhdlCaseItemScheme::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice) choice->InitializeForVariableSlice() ;
    }
}
void VhdlConditionalWaveform::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlExpression *expr ;
    FOREACH_ARRAY_ITEM(_waveform,i,expr) {
        if (!expr) continue ;
        expr->InitializeForVariableSlice() ;
    }
    if (_when_condition) _when_condition->InitializeForVariableSlice() ;
}

void VhdlConditionalExpression::InitializeForVariableSlice()
{
    if (_expr) _expr->InitializeForVariableSlice() ;
    if (_condition) _condition->InitializeForVariableSlice() ;
}

void VhdlSelectedWaveform::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlExpression *expr ;
    FOREACH_ARRAY_ITEM(_waveform, i, expr) {
        if (!expr) continue ;
        expr->InitializeForVariableSlice() ;
    }
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices,i,choice) {
        if (!choice) continue ;
        choice->InitializeForVariableSlice() ;
    }
}

void VhdlSelectedExpression::InitializeForVariableSlice()
{
    _expr->InitializeForVariableSlice() ;

    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices,i,choice) {
        if (!choice) continue ;
        choice->InitializeForVariableSlice() ;
    }
}

void VhdlBlockGenerics::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,decl) {
        if (!decl) continue ;
        decl->InitializeForVariableSlice() ;
    }
 // Not calling for generic_map_aspect
}

void VhdlBlockPorts::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_port_clause,i,decl) {
        if (!decl) continue ;
        decl->InitializeForVariableSlice() ;
    }
    if (_block_label) _block_label->InitializeForVariableSlice() ;
  // Not calling for port_map_aspect
}

void VhdlCaseStatementAlternative::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices,i,choice) {
        if (!choice) continue ;
        choice->InitializeForVariableSlice() ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements,i,stmt) {
        if (!stmt) continue ;
        stmt->InitializeForVariableSlice() ;
    }
}

void VhdlElsif::InitializeForVariableSlice()
{
    if (_condition) _condition->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements,i,stmt) {
        if (!stmt) continue ;
        stmt->InitializeForVariableSlice() ;
    }
}
#endif

