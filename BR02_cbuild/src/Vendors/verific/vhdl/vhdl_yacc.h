/* A Bison parser, made by GNU Bison 1.875c.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     VHDL_AMPERSAND = 257,
     VHDL_TICK = 258,
     VHDL_OPAREN = 259,
     VHDL_CPAREN = 260,
     VHDL_STAR = 261,
     VHDL_PLUS = 262,
     VHDL_COMMA = 263,
     VHDL_MINUS = 264,
     VHDL_DOT = 265,
     VHDL_SLASH = 266,
     VHDL_COLON = 267,
     VHDL_SEMI = 268,
     VHDL_STHAN = 269,
     VHDL_EQUAL = 270,
     VHDL_GTHAN = 271,
     VHDL_VERTICAL = 272,
     VHDL_BANG = 273,
     VHDL_OBRACK = 274,
     VHDL_CBRACK = 275,
     VHDL_ARROW = 276,
     VHDL_EXPONENT = 277,
     VHDL_VARASSIGN = 278,
     VHDL_NEQUAL = 279,
     VHDL_GEQUAL = 280,
     VHDL_SEQUAL = 281,
     VHDL_BOX = 282,
     VHDL_abs = 283,
     VHDL_access = 284,
     VHDL_after = 285,
     VHDL_alias = 286,
     VHDL_all = 287,
     VHDL_and = 288,
     VHDL_architecture = 289,
     VHDL_array = 290,
     VHDL_assert = 291,
     VHDL_attribute = 292,
     VHDL_begin = 293,
     VHDL_block = 294,
     VHDL_body = 295,
     VHDL_buffer = 296,
     VHDL_bus = 297,
     VHDL_case = 298,
     VHDL_component = 299,
     VHDL_configuration = 300,
     VHDL_constant = 301,
     VHDL_disconnect = 302,
     VHDL_downto = 303,
     VHDL_else = 304,
     VHDL_elsif = 305,
     VHDL_end = 306,
     VHDL_entity = 307,
     VHDL_exit = 308,
     VHDL_file = 309,
     VHDL_for = 310,
     VHDL_function = 311,
     VHDL_generate = 312,
     VHDL_generic = 313,
     VHDL_group = 314,
     VHDL_guarded = 315,
     VHDL_if = 316,
     VHDL_impure = 317,
     VHDL_in = 318,
     VHDL_inertial = 319,
     VHDL_inout = 320,
     VHDL_is = 321,
     VHDL_label = 322,
     VHDL_library = 323,
     VHDL_linkage = 324,
     VHDL_literal = 325,
     VHDL_loop = 326,
     VHDL_map = 327,
     VHDL_mod = 328,
     VHDL_nand = 329,
     VHDL_new = 330,
     VHDL_next = 331,
     VHDL_nor = 332,
     VHDL_not = 333,
     VHDL_null = 334,
     VHDL_of = 335,
     VHDL_on = 336,
     VHDL_open = 337,
     VHDL_or = 338,
     VHDL_others = 339,
     VHDL_out = 340,
     VHDL_package = 341,
     VHDL_port = 342,
     VHDL_postponed = 343,
     VHDL_procedure = 344,
     VHDL_process = 345,
     VHDL_pure = 346,
     VHDL_range = 347,
     VHDL_record = 348,
     VHDL_register = 349,
     VHDL_reject = 350,
     VHDL_rem = 351,
     VHDL_report = 352,
     VHDL_return = 353,
     VHDL_rol = 354,
     VHDL_ror = 355,
     VHDL_select = 356,
     VHDL_severity = 357,
     VHDL_signal = 358,
     VHDL_shared = 359,
     VHDL_sla = 360,
     VHDL_sll = 361,
     VHDL_sra = 362,
     VHDL_srl = 363,
     VHDL_subtype = 364,
     VHDL_then = 365,
     VHDL_to = 366,
     VHDL_transport = 367,
     VHDL_type = 368,
     VHDL_unaffected = 369,
     VHDL_units = 370,
     VHDL_until = 371,
     VHDL_use = 372,
     VHDL_variable = 373,
     VHDL_wait = 374,
     VHDL_when = 375,
     VHDL_while = 376,
     VHDL_with = 377,
     VHDL_xnor = 378,
     VHDL_xor = 379,
     VHDL_INTEGER = 380,
     VHDL_REAL = 381,
     VHDL_STRING = 382,
     VHDL_BIT_STRING = 383,
     VHDL_ID = 384,
     VHDL_EXTENDED_ID = 385,
     VHDL_CHARACTER = 386,
     VHDL_FATAL_ERROR = 387,
     VHDL_OPT_COMMENT = 484,
     VHDL_NO_COMMENT = 485,
     VHDL_NO_OPAREN = 486,
     VHDL_implicit_guard = 492,
     VHDL_protected = 493,
     VHDL_interfacerange = 494,
     VHDL_interfaceread = 495,
     VHDL_interfaceprefixread = 496,
     VHDL_subtyperange = 497,
     VHDL_subtyperangebound = 498,
     VHDL_minimum = 550,
     VHDL_maximum = 551,
     VHDL_condition = 552,
     VHDL_matching_equal = 553,
     VHDL_matching_nequal = 554,
     VHDL_matching_gthan = 555,
     VHDL_matching_sthan = 556,
     VHDL_matching_gequal = 557,
     VHDL_matching_sequal = 558,
     VHDL_context = 559,
     VHDL_end_generate = 560,
     VHDL_parameter = 561,
     VHDL_default = 562,
     VHDL_matching_op = 563,
     VHDL_to_string = 564,
     VHDL_force = 565,
     VHDL_release = 566,
     VHDL_LANGLEQ = 567,
     VHDL_RANGLEQ = 568,
     VHDL_AT = 569,
     VHDL_ACCENT = 570,
     VHDL_BEGIN_EXPR = 571,
     VHDL_to_bstring = 572,
     VHDL_to_binary_string = 573,
     VHDL_to_ostring = 574,
     VHDL_to_octal_string = 575,
     VHDL_to_hstring = 576,
     VHDL_to_hex_string = 577,
     VHDL_recordrange = 578,
     VHDL_resize = 3000,
     VHDL_uns_resize = 3001,
     VHDL_signed_mult = 3002,
     VHDL_unsigned_mult = 3003,
     VHDL_numeric_signed_mult = 3004,
     VHDL_numeric_unsigned_mult = 3005,
     VHDL_numeric_sign_div = 3006,
     VHDL_numeric_uns_div = 3007,
     VHDL_numeric_minus = 3008,
     VHDL_numeric_unary_minus = 3009,
     VHDL_numeric_plus = 3010,
     VHDL_is_uns_less = 3011,
     VHDL_is_uns_gt = 3012,
     VHDL_is_gt = 3013,
     VHDL_is_less = 3014,
     VHDL_is_uns_gt_or_equal = 3015,
     VHDL_is_uns_less_or_equal = 3016,
     VHDL_is_gt_or_equal = 3017,
     VHDL_is_less_or_equal = 3018,
     VHDL_and_reduce = 3019,
     VHDL_or_reduce = 3020,
     VHDL_xor_reduce = 3021,
     VHDL_numeric_mod = 3022,
     VHDL_i_to_s = 3023,
     VHDL_s_to_u = 3024,
     VHDL_s_to_i = 3025,
     VHDL_u_to_i = 3026,
     VHDL_u_to_u = 3027,
     VHDL_s_xt = 3028
   };
#endif
#define VHDL_AMPERSAND 257
#define VHDL_TICK 258
#define VHDL_OPAREN 259
#define VHDL_CPAREN 260
#define VHDL_STAR 261
#define VHDL_PLUS 262
#define VHDL_COMMA 263
#define VHDL_MINUS 264
#define VHDL_DOT 265
#define VHDL_SLASH 266
#define VHDL_COLON 267
#define VHDL_SEMI 268
#define VHDL_STHAN 269
#define VHDL_EQUAL 270
#define VHDL_GTHAN 271
#define VHDL_VERTICAL 272
#define VHDL_BANG 273
#define VHDL_OBRACK 274
#define VHDL_CBRACK 275
#define VHDL_ARROW 276
#define VHDL_EXPONENT 277
#define VHDL_VARASSIGN 278
#define VHDL_NEQUAL 279
#define VHDL_GEQUAL 280
#define VHDL_SEQUAL 281
#define VHDL_BOX 282
#define VHDL_abs 283
#define VHDL_access 284
#define VHDL_after 285
#define VHDL_alias 286
#define VHDL_all 287
#define VHDL_and 288
#define VHDL_architecture 289
#define VHDL_array 290
#define VHDL_assert 291
#define VHDL_attribute 292
#define VHDL_begin 293
#define VHDL_block 294
#define VHDL_body 295
#define VHDL_buffer 296
#define VHDL_bus 297
#define VHDL_case 298
#define VHDL_component 299
#define VHDL_configuration 300
#define VHDL_constant 301
#define VHDL_disconnect 302
#define VHDL_downto 303
#define VHDL_else 304
#define VHDL_elsif 305
#define VHDL_end 306
#define VHDL_entity 307
#define VHDL_exit 308
#define VHDL_file 309
#define VHDL_for 310
#define VHDL_function 311
#define VHDL_generate 312
#define VHDL_generic 313
#define VHDL_group 314
#define VHDL_guarded 315
#define VHDL_if 316
#define VHDL_impure 317
#define VHDL_in 318
#define VHDL_inertial 319
#define VHDL_inout 320
#define VHDL_is 321
#define VHDL_label 322
#define VHDL_library 323
#define VHDL_linkage 324
#define VHDL_literal 325
#define VHDL_loop 326
#define VHDL_map 327
#define VHDL_mod 328
#define VHDL_nand 329
#define VHDL_new 330
#define VHDL_next 331
#define VHDL_nor 332
#define VHDL_not 333
#define VHDL_null 334
#define VHDL_of 335
#define VHDL_on 336
#define VHDL_open 337
#define VHDL_or 338
#define VHDL_others 339
#define VHDL_out 340
#define VHDL_package 341
#define VHDL_port 342
#define VHDL_postponed 343
#define VHDL_procedure 344
#define VHDL_process 345
#define VHDL_pure 346
#define VHDL_range 347
#define VHDL_record 348
#define VHDL_register 349
#define VHDL_reject 350
#define VHDL_rem 351
#define VHDL_report 352
#define VHDL_return 353
#define VHDL_rol 354
#define VHDL_ror 355
#define VHDL_select 356
#define VHDL_severity 357
#define VHDL_signal 358
#define VHDL_shared 359
#define VHDL_sla 360
#define VHDL_sll 361
#define VHDL_sra 362
#define VHDL_srl 363
#define VHDL_subtype 364
#define VHDL_then 365
#define VHDL_to 366
#define VHDL_transport 367
#define VHDL_type 368
#define VHDL_unaffected 369
#define VHDL_units 370
#define VHDL_until 371
#define VHDL_use 372
#define VHDL_variable 373
#define VHDL_wait 374
#define VHDL_when 375
#define VHDL_while 376
#define VHDL_with 377
#define VHDL_xnor 378
#define VHDL_xor 379
#define VHDL_INTEGER 380
#define VHDL_REAL 381
#define VHDL_STRING 382
#define VHDL_BIT_STRING 383
#define VHDL_ID 384
#define VHDL_EXTENDED_ID 385
#define VHDL_CHARACTER 386
#define VHDL_FATAL_ERROR 387
#define VHDL_OPT_COMMENT 484
#define VHDL_NO_COMMENT 485
#define VHDL_NO_OPAREN 486
#define VHDL_implicit_guard 492
#define VHDL_protected 493
#define VHDL_interfacerange 494
#define VHDL_interfaceread 495
#define VHDL_interfaceprefixread 496
#define VHDL_subtyperange 497
#define VHDL_subtyperangebound 498
#define VHDL_minimum 550
#define VHDL_maximum 551
#define VHDL_condition 552
#define VHDL_matching_equal 553
#define VHDL_matching_nequal 554
#define VHDL_matching_gthan 555
#define VHDL_matching_sthan 556
#define VHDL_matching_gequal 557
#define VHDL_matching_sequal 558
#define VHDL_context 559
#define VHDL_end_generate 560
#define VHDL_parameter 561
#define VHDL_default 562
#define VHDL_matching_op 563
#define VHDL_to_string 564
#define VHDL_force 565
#define VHDL_release 566
#define VHDL_LANGLEQ 567
#define VHDL_RANGLEQ 568
#define VHDL_AT 569
#define VHDL_ACCENT 570
#define VHDL_BEGIN_EXPR 571
#define VHDL_to_bstring 572
#define VHDL_to_binary_string 573
#define VHDL_to_ostring 574
#define VHDL_to_octal_string 575
#define VHDL_to_hstring 576
#define VHDL_to_hex_string 577
#define VHDL_recordrange 578
#define VHDL_resize 3000
#define VHDL_uns_resize 3001
#define VHDL_signed_mult 3002
#define VHDL_unsigned_mult 3003
#define VHDL_numeric_signed_mult 3004
#define VHDL_numeric_unsigned_mult 3005
#define VHDL_numeric_sign_div 3006
#define VHDL_numeric_uns_div 3007
#define VHDL_numeric_minus 3008
#define VHDL_numeric_unary_minus 3009
#define VHDL_numeric_plus 3010
#define VHDL_is_uns_less 3011
#define VHDL_is_uns_gt 3012
#define VHDL_is_gt 3013
#define VHDL_is_less 3014
#define VHDL_is_uns_gt_or_equal 3015
#define VHDL_is_uns_less_or_equal 3016
#define VHDL_is_gt_or_equal 3017
#define VHDL_is_less_or_equal 3018
#define VHDL_and_reduce 3019
#define VHDL_or_reduce 3020
#define VHDL_xor_reduce 3021
#define VHDL_numeric_mod 3022
#define VHDL_i_to_s 3023
#define VHDL_s_to_u 3024
#define VHDL_s_to_i 3025
#define VHDL_u_to_i 3026
#define VHDL_u_to_u 3027
#define VHDL_s_xt 3028




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)

typedef union YYSTYPE {
    char      *str ;
    unsigned   unsigned_number ;
    double     real_number ;

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

    VN::Array     *array ;

    VN::VhdlDesignUnit *library_unit ;
    VN::VhdlDeclaration *declaration ;
    VN::VhdlStatement *statement ;
    VN::VhdlDiscreteRange *discrete_range ;
    VN::VhdlSubtypeIndication *subtype_indication ;
    VN::VhdlExpression *expression ;
    VN::VhdlName *name ;
    VN::VhdlDesignator *designator ;
    VN::VhdlIdRef *id ;
    VN::VhdlIdDef *id_def ;
    VN::VhdlTypeDef *type_def ;
    VN::VhdlSignature *signature ;
    VN::VhdlElementDecl *element_decl ;
    VN::VhdlPhysicalUnitDecl *physical_unit_decl ;
    VN::VhdlConfigurationItem *configuration_item ;
    VN::VhdlBlockConfiguration *block_configuration ;
    VN::VhdlComponentConfiguration *component_configuration ;
    VN::VhdlBindingIndication *binding_indication ;
    VN::VhdlSpecification *specification ;
    VN::VhdlInterfaceDecl *interface_decl ;
    VN::VhdlIterScheme *iter_scheme ;
    VN::VhdlOptions *options ;
    VN::VhdlDelayMechanism *delay_mechanism ;
    VN::VhdlBlockGenerics *block_generics ;
    VN::VhdlBlockPorts *block_ports ;
    VN::VhdlCaseStatementAlternative *case_statement_alternative ;
    VN::VhdlFileOpenInfo *file_open_info ;
    VN::VhdlEntityClassEntry *entity_class_entry ;
    VN::VhdlTreeNode *tree_node;
    struct { VN::Array *decls; VN::Array *stmts ; VN::VhdlScope *scope ; } generate_stmt_body ; // structure with two pointers
    struct {unsigned path_token; unsigned hat_count; VN::VhdlName *path ; } ext_path_name ;
} YYSTYPE;
/* Line 1275 of yacc.c.  */

# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE vhdllval;

#if ! defined (YYLTYPE) && ! defined (YYLTYPE_IS_DECLARED)
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYLTYPE vhdllloc;


