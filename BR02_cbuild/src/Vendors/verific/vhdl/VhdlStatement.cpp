/*
 *
 * [ File Version : 1.170 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlStatement.h"

#include "Array.h"
#include "Set.h"
#include "Map.h"
#include "Strings.h"
#include "Message.h"

#include "vhdl_tokens.h"
#include "VhdlName.h"
#include "VhdlIdDef.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "vhdl_file.h"
#include "VhdlScope.h"

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
#include "TextBasedDesignMod.h" // VIPER #7752
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlStatement::VhdlStatement()
  : VhdlTreeNode(),
    _label(0),
    _postponed(0)
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{ }

VhdlStatement::~VhdlStatement()
{
    // The statement label is a parse-tree node so needs to be deleted
    // Its just created after the constructor
    delete _label ;

    // Delete comments if there
    // DeleteComments(GetComments()) ; // JJ: we call this in DTOR now.
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _closing_label ;
#endif
}

void VhdlStatement::SetLabel(VhdlIdDef *label, unsigned postponed)
{
    _postponed = postponed ;

    if (_label) {
        // closing label was set here earlier. Make sure that happened :
        VERIFIC_ASSERT(!_label->GetStatement()) ;

        // Check if it is the same as the real label that we set here :
        if (!label) {
            _label->Error("%s is not a label of this statement", _label->Name()) ;
        } else if (!Strings::compare(_label->Name(),label->Name())) {
            _label->Error("mismatch on label ; expected %s",label->Name()) ;
        }
        // delete the closing label
        delete _label ;
        _label = 0 ;
    }

    // Set this label as the label of this statement.
    if (!label) return ;

    // Let the label point back to this statement :
    label->SetStatement(this) ;
    _label = label ;
}
// VIPER #7494 : Set implicit label without disturbing _postponed
void VhdlStatement::SetImplicitLabel(VhdlIdDef *label)
{
    if (!label) return ;
    VERIFIC_ASSERT(!_label) ;
    label->SetStatement(this) ;
    _label = label ;
}

VhdlIdDef *VhdlStatement::GetLabel() const { return _label ; }

void VhdlStatement::ClosingLabel(VhdlDesignator *id)
{
    if (!id) return ; // nothing to check.
    if (!_label) {
        // ClosingLabel is called before SetLabel.
        // That happens because of yacc rule ordering for 'if' statements and such.
        // Set the closing label as _label for now, until SetLabel comes in.
        // DON'T set the statement pointer of this (closing) label.
        // that way we can recognize that this thing is a closing label, not stored in scope.
        _label = new VhdlLabelId(Strings::save(id->Name())) ;
    } else if (!Strings::compare(_label->Name(),id->Name())) {
        id->Error("mismatch on label ; expected %s",_label->Name()) ;
    }

    SetClosingLabel(id) ; // VIPER #6666 (id may get deleted here)
}

VhdlDesignator *VhdlStatement::GetClosingLabel() const // VIPER #6666
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    return _closing_label ;
#else
    return 0 ;
#endif
}

void VhdlStatement::SetClosingLabel(VhdlDesignator *label) // VIPER #6666
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _closing_label ;
    _closing_label = label ;
#else
    delete label ;
#endif
}

// Check matching of alternative label of if/case generate branch
void VhdlStatement::ClosingAlternativeLabel(VhdlDesignator *id)
{
    VhdlIterScheme *scheme = GetScheme() ;
    VhdlIdDef *alt_label = (scheme) ? scheme->GetAlternativeLabelId() : 0 ;
    // Produce error for following conditions :
    // 1. Alternative label is not specified in scheme, but closing label exists.
    // 2. Name mismatch in starting and closing label.
    if (scheme && id && (!alt_label || !Strings::compare(id->Name(), alt_label->Name()))) {
        id->Error("mismatch on label ; expected %s", alt_label ? alt_label->Name(): id->Name()) ;
    }

    // VIPER #6666 & #6662 (id may get deleted here):
    if (scheme) {
        scheme->SetAlternativeClosingLabel(id) ;
    } else {
        SetClosingLabel(id) ;
    }
}
VhdlNullStatement::VhdlNullStatement() : VhdlStatement() { }
VhdlNullStatement::~VhdlNullStatement() { }

VhdlReturnStatement::VhdlReturnStatement(VhdlExpression *expr, VhdlIdDef *owner)
  : VhdlStatement(),
    _expr(expr),
    _owner(owner)
{
    if (_owner && _owner->IsSubprogram()) {
        if (_expr && _owner->Type()==UniversalVoid()) {
            Error("return statement in procedure should not have an expression") ;
            _owner = 0 ;
        } else if (!_expr && _owner->Type()!=UniversalVoid()) {
            Error("return statement in function needs an expression") ;
            _owner = 0 ;
        } else if (_expr) {
            // Type infer expression against return type
            (void) _expr->TypeInfer(_owner->Type(),0,VHDL_READ,0) ;
        }
    } else {
        Error("return statement outside a subprogram") ;
        _owner = 0 ;
    }
}
VhdlReturnStatement::~VhdlReturnStatement()
{
    delete _expr ;
}

VhdlExitStatement::VhdlExitStatement(VhdlName *target, VhdlExpression *condition)
  : VhdlStatement(),
    _target(target),
    _condition(condition),
    _target_label(0)
{
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    // Find the target label
    if (_target) {
        // Get the label by resolving the target name
        VhdlIdDef *label = target->FindSingleObject() ;
        if (!label) return ; // something went wrong.

        // Now, get the 'exit' label in this label's immediate scope :
        VhdlScope *scope = label->LocalScope() ;
        _target_label = (scope) ? scope->FindSingleObjectLocal(" exit_label") : 0 ;

        if (!_target_label || !label->IsLabel()) {
            Error("%s is not a loop label", label->Name()) ;
        } else if (label->GetStatement()) {
            // Check that this statement is INSIDE the loop.
            // The trick : Inside the loop, the '_statement' on the label itself ('label') is not yet set.
            Error("exit statement is not inside loop %s", label->Name()) ;
        }
    } else {
        // Get the label from the nearest enclosing loop
        _target_label = (_present_scope) ? _present_scope->FindSingleObject(" exit_label"): 0 ;
        if (!_target_label) {
            Error("exit statement is not inside a loop") ;
        }
    }

    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}
VhdlExitStatement::~VhdlExitStatement()
{
    delete _target ;
    delete _condition ;
}

VhdlNextStatement::VhdlNextStatement(VhdlName *target, VhdlExpression *condition)
  : VhdlStatement(),
    _target(target),
    _condition(condition),
    _target_label(0)
{
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    // Find the target label
    if (_target) {
        // Get the label by resolving the target name
        VhdlIdDef *label = target->FindSingleObject() ;
        if (!label) return ; // something went wrong.

        // Now, get the 'next' label in this label's immediate scope :
        VhdlScope *scope = label->LocalScope() ;
        _target_label = (scope) ? scope->FindSingleObjectLocal(" next_label") : 0 ;

        if (!_target_label || !label->IsLabel()) {
            Error("%s is not a loop label", label->Name()) ;
        } else if (label->GetStatement()) {
            // Check that this statement is INSIDE the loop.
            // The trick : Inside the loop, the '_statement' on the label itself ('label') is not yet set.
            Error("next statement is not inside loop %s", label->Name()) ;
        }

    } else {
        // Get the label from the nearest enclosing loop
        _target_label = (_present_scope) ? _present_scope->FindSingleObject(" next_label"): 0 ;
        if (!_target_label) {
            Error("next statement is not inside a loop") ;
        }
    }

    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}
VhdlNextStatement::~VhdlNextStatement()
{
    delete _target ;
    delete _condition ;
}

VhdlLoopStatement::VhdlLoopStatement(VhdlIterScheme *iter_scheme, Array *statements, VhdlScope *local_scope)
  : VhdlStatement(),
    _iter_scheme(iter_scheme),
    _statements(statements),
    _local_scope(local_scope)
{ }

VhdlLoopStatement::~VhdlLoopStatement()
{
    // Delete the contents of the construct
    delete _iter_scheme ;
    unsigned i ;
    VhdlStatement *elem ;
    FOREACH_ARRAY_ITEM(_statements,i,elem) delete elem ;
    delete _statements ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
}

VhdlCaseStatement::VhdlCaseStatement(VhdlExpression *expr, Array *alternatives, unsigned is_matching_case)
  : VhdlStatement(),
    _expr(expr),
    _alternatives(alternatives),
    _is_matching_case(is_matching_case),
    _expr_type(0)
{
    if (!_expr) return ;

    // Infer 'case' expression
    // Viper #5550: pass return_types set and prune the list based on this context
    Set return_types(POINTER_HASH) ;
    _expr_type = 0 ;
    (void) _expr->TypeInfer(0,&return_types,VHDL_READ,0) ;
    unsigned multiple_match_expr = (return_types.Size() > 1) ? 1: 0 ;
    if (return_types.Size() > 1) {
        SetIter si ;
        VhdlIdDef *type ;
        FOREACH_SET_ITEM(&return_types, si, &type) {
            if ((!type->IsDiscreteType() && type->Dimension()!=1) ||
                (type->ElementType() && !type->ElementType()->IsEnumerationType()) ||
                (type->IsAccessType() || type->IsFileType())) {
                (void) return_types.Remove(type) ;
            }

            if (return_types.Size() == 1) break ;
        }
    }

    if (return_types.Size() > 1) {
        // Viper #5550: Give error here as TypeInfer no longer does
        // this check (for we passed the return_types set)
        _expr->Error("near %s ; %d visible identifiers match here", "case_expression", return_types.Size()) ;
    } else {
        _expr_type = (VhdlIdDef*) return_types.GetLast() ;
    }

    if (!_expr_type) return ;
    if (_is_matching_case) {
        VhdlIdDef *base_expr_type = _expr_type->BaseType() ? _expr_type->BaseType() : _expr_type ;
        if ((base_expr_type != StdType("bit")) && (base_expr_type != StdType("std_ulogic")) && base_expr_type->Dimension()!=1) {
            Error("case expression type of matching case is not a bit or a std_ulogic or 1-dimensional array of bit/std_ulogic") ;
            return ;
        }
        VhdlIdDef *element_type = _expr_type->ElementType() ;
        if (element_type && element_type->BaseType()) element_type = element_type->BaseType() ;

        if (element_type && (element_type != StdType("bit")) && (element_type != StdType("std_ulogic"))) {
            Error("case expression type of matching case is not a bit/std_ulogic array type") ;
            return ;
        }
    } else {
        if (!_expr_type->IsDiscreteType() && _expr_type->Dimension()!=1) {
            Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
            return ;
        }
        if (_expr_type->ElementType() && !_expr_type->ElementType()->IsEnumerationType()) {
            Error("case expression type %s is not a character array type", _expr_type->Name()) ;
            // FIX ME : can we check the 'character' part of it ?
            return ;
        }
        // VIPER 3153 : check if it is another bad type :
        if (_expr_type->IsAccessType() || _expr_type->IsFileType()) {
            Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
            return ;
        }
    }
    // VIPER #5789 : If type infer of case expression returns multiple types, and
    // '_expr_type' can be extracted, try to type infer case expression again passing
    // '_expr_type' as expected type. This will help to set proper resolved identifier
    // to case expression :
    if (multiple_match_expr) {
        return_types.Reset() ;
        (void) _expr->TypeInfer(_expr_type,&return_types,VHDL_READ,0) ;
    }

    VhdlIdDef *expr_id = _expr->GetId() ;
    if (expr_id && _expr_type->IsArrayType() && _expr->FindFullFormal() && !expr_id->IsLocallyStaticType()) Error("array type case expression must be of a locally static subtype") ; // Viper #4452, 3699

    // I read this somewhere in the LRM. Cant find it any more.
    if (_expr_type->IsUniversalInteger()) {
        _expr_type = StdType("integer") ;
    }

    VhdlCaseStatementAlternative *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_alternatives, i, elem) {
        if (!elem) continue ;

        // Type-infer the choices with the expression type
        elem->TypeInferChoices(_expr_type) ;

        // Test LRM 8.8
        if (elem->IsOthers() && i!=_alternatives->Size()-1) {
            elem->Error("'others' choice should be last in alternatives") ;
        }
    }
}
VhdlCaseStatement::~VhdlCaseStatement()
{
    delete _expr ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_alternatives,i,elem) delete elem ;
    delete _alternatives ;
}

VhdlIfStatement::VhdlIfStatement(VhdlExpression *if_cond, Array *if_statements, Array *elsif_list, Array *else_statements)
  : VhdlStatement(),
    _if_cond(if_cond),
    _if_statements(if_statements),
    _elsif_list(elsif_list),
    _else_statements(else_statements)
{
    if (_if_cond) _if_cond = _if_cond->GetConditionalExpression() ;
    if (_if_cond) (void) _if_cond->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
}
VhdlIfStatement::~VhdlIfStatement()
{
    delete _if_cond ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_if_statements,i,elem) delete elem ;
    delete _if_statements ;
    FOREACH_ARRAY_ITEM(_elsif_list,i,elem) delete elem ;
    delete _elsif_list ;
    FOREACH_ARRAY_ITEM(_else_statements,i,elem) delete elem ;
    delete _else_statements ;
}

VhdlVariableAssignmentStatement::VhdlVariableAssignmentStatement(VhdlExpression *target, VhdlExpression *value)
  : VhdlStatement(),
    _target(target),
    _value(value)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_VARUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the expression
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    VhdlIdDef *value_type = _value->TypeInfer(target_type, 0, VHDL_READ,0) ;
    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_VARUPDATE,0) ;
    }
}
VhdlVariableAssignmentStatement::~VhdlVariableAssignmentStatement()
{
    delete _target ;
    delete _value ;
}

VhdlSignalAssignmentStatement::VhdlSignalAssignmentStatement(VhdlExpression *target, VhdlDelayMechanism *delay_mechanism, Array *waveform)
  : VhdlStatement(),
    _target(target),
    _delay_mechanism(delay_mechanism),
    _waveform(waveform)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Added for VIPER #4507 : Signal assignments are not be allowed in entity statement part
    // Check what the containing design unit is :
    VhdlIdDef *design_unit = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ;
    if (design_unit && design_unit->IsEntity()) {
        // Signal assignment that occurs in a entity.
        // Check if we are inside a subprogram. That could still be allowed :
        if (!_present_scope->GetSubprogram()) {
            Error("signal assignments not allowed in entity statement part") ;
        }
    }

    // Now call the waveforms
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlExpression *elem ;
    VhdlIdDef *value_type = 0 ;
    FOREACH_ARRAY_ITEM(_waveform, i, elem) {
        if (!elem) continue ;
        if (elem->IsNull()) {
            // Following check is for Viper# 3185
            VhdlIdDef *target_id = _target->FindFormal() ;
            if (target_id && !target_id->GetSignalKind()) Error("cannot assign null waveform to non-guarded signal") ;
            // VIPER 3119 : Either way, do NOT call type-inference on a NULL waveform,
            // since it would trigger an error, and would return 'target_type' any way.
            value_type = target_type ;

            // VIPER #7470 : Type infer the after expression associated with null waveform
            // For null waveform we cannot call TypeInfer for VIPER #3119. So TypeInfer
            // after expression separately here
            VhdlExpression *after_expr = elem->GetAfter() ;
            if (after_expr) (void) after_expr->TypeInfer(StdType("time"),0, VHDL_READ,0) ;
            continue ;
        }
        if (elem->IsUnaffected() && !IsVhdl2008()) {
            // VIPER #7644: Produce error if unaffected is used in non-2008 mode
            Error("unaffected is not allowed in sequential signal assignment statement") ;
        }
        value_type = elem->TypeInfer(target_type, 0, VHDL_READ,0) ;
    }
    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_SIGUPDATE,0) ;
    }
}
VhdlSignalAssignmentStatement::~VhdlSignalAssignmentStatement()
{
    delete _target ;
    delete _delay_mechanism ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) delete elem ;
    delete _waveform ;
}
VhdlForceAssignmentStatement::VhdlForceAssignmentStatement(VhdlExpression *target, unsigned force_mode, VhdlExpression *expr)
  : VhdlStatement(),
    _target(target),
    _force_mode(force_mode),
    _expr(expr)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // VIPER #7731: Error out if force is assigned to any aggregate type.
    if (_target->IsAggregate()) {
        Error("aggregate is not allowed as the target of signal %s", "force") ;
    }

    // Error out if force is assigned to other than a signal.
    if (!_target->IsSignal()) {
        Error("target of signal force must be a signal") ;
    }

    // Added for VIPER #4507 : Signal assignments are not be allowed in entity statement part
    // Check what the containing design unit is :
    VhdlIdDef *design_unit = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ;
    if (design_unit && design_unit->IsEntity()) {
        // Signal assignment that occurs in a entity.
        // Check if we are inside a subprogram. That could still be allowed :
        if (!_present_scope->GetSubprogram()) {
            Error("signal assignments not allowed in entity statement part") ;
        }
    }

    // Now type infer the expression
    VhdlIdDef *value_type = (_expr) ? _expr->TypeInfer(target_type, 0, VHDL_READ,0): 0 ;
    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_SIGUPDATE,0) ;
    }
}
VhdlForceAssignmentStatement::~VhdlForceAssignmentStatement()
{
    delete _target ;
    delete _expr ;
}
VhdlReleaseAssignmentStatement::VhdlReleaseAssignmentStatement(VhdlExpression *target, unsigned force_mode)
  : VhdlStatement(),
    _target(target),
    _force_mode(force_mode)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    (void) _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // VIPER #7731: Error out if release is assigned to any aggregate type.
    if (_target->IsAggregate()) {
        Error("aggregate is not allowed as the target of signal %s", "release") ;
    }

    // Added for VIPER #4507 : Signal assignments are not be allowed in entity statement part
    // Check what the containing design unit is :
    VhdlIdDef *design_unit = (_present_scope) ? _present_scope->GetContainingDesignUnit() : 0 ;
    if (design_unit && design_unit->IsEntity()) {
        // Signal assignment that occurs in a entity.
        // Check if we are inside a subprogram. That could still be allowed :
        if (!_present_scope->GetSubprogram()) {
            Error("signal assignments not allowed in entity statement part") ;
        }
    }
}
VhdlReleaseAssignmentStatement::~VhdlReleaseAssignmentStatement()
{
    delete _target ;
}
VhdlWaitStatement::VhdlWaitStatement(Array *sensitivity_clause, VhdlExpression *condition_clause, VhdlExpression *timeout_clause)
  : VhdlStatement(),
    _sensitivity_clause(sensitivity_clause),
    _condition_clause(condition_clause),
    _timeout_clause(timeout_clause)
{
    if (_condition_clause) _condition_clause = _condition_clause->GetConditionalExpression() ;
    // Sensitivity clause ; Could also contain indexed/sliced signal names.
    // Infer these guys blindly, to set their VhdlIddef*'s, and check they
    // all exist.
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_sensitivity_clause, i, name) {
        if (!name) continue ;
        // Sensitivity list names constitute a 'read' of the names (LRM 4.3.2), but that affects RAM inference. Either way, we should be OK with less checks on sensitivity list.
        (void) name->TypeInfer(0,0,0,0) ;

        // Viper #3149 : LRM Section 8.1 : All entries in the sensitivity
        // must be signal names, and signal names must be static names
        if (name->GetId() && (!name->IsGloballyStaticName() || !name->IsSignal())) {
            name->Error("sensitivity list can have only static signal name") ;
        }

        VhdlIdDef *name_id = name->GetId() ;
        // Viper 7604: Should not error out for Vhdl 2008 mode.
        if (!IsVhdlPsl() && !IsVhdl2008() && name_id && name_id->IsOutput()) { // Viper #3646
            name->Error("cannot read from 'out' object %s ; use 'buffer' or 'inout'", name_id->Name()) ;
        }
    }

    if (_condition_clause) (void) _condition_clause->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
    if (_timeout_clause) (void) _timeout_clause->TypeInfer(StdType("time"),0,VHDL_READ,0) ;

    // Check if this wait statement occurs inside a subprogram :
    VhdlScope *scope = (_present_scope) ? _present_scope->GetSubprogramScope() : 0 ;
    if (scope) {
        // VIPER 4518 (and 3147) : Check if this is inside a function :
        VhdlIdDef *subprog = scope->GetOwner() ;
        if (subprog && subprog->IsFunction()) {
            Error("wait statement not allowed inside a function") ;
        //CARBON_BEGIN
        } else if (subprog && subprog->IsProcedure()) {
            Error("WAIT statement inside a subprogram body is not permitted");
        //CARBON_END
        }
    } else {
        // Check if this wait statement occurs inside a process :
        scope = (_present_scope) ? _present_scope->GetProcessScope() : 0 ;
        if (scope) {
            // VIPER 3154 (and #2966, #3496) : Issue error if this wait statement is in a region
            // where it is not allowed like within process with sensitivity list
            if (scope->IsWaitStatementNotAllowed()) {
                Error("process cannot have both a wait statement and a sensitivity list") ;
            }
        }
    }

    // flag that scope (subprogram or process) as having a wait statement
    if (scope) scope->SetHasWaitStatement() ;
}
VhdlWaitStatement::~VhdlWaitStatement()
{
    VhdlTreeNode *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_sensitivity_clause,i,elem) delete elem ;
    delete _sensitivity_clause ;
    delete _condition_clause ;
    delete _timeout_clause ;
}

VhdlReportStatement::VhdlReportStatement(VhdlExpression *report, VhdlExpression *severity)
  : VhdlStatement(),
    _report(report),
    _severity(severity)
{
    if (_report) (void) _report->TypeInfer(StdType("string"),0,VHDL_READ,0) ;
    if (_severity) (void) _severity->TypeInfer(StdType("severity_level"),0,VHDL_READ,0) ;
}
VhdlReportStatement::~VhdlReportStatement()
{
    delete _report ;
    delete _severity ;
}

VhdlAssertionStatement::VhdlAssertionStatement(VhdlExpression *condition, VhdlExpression *report, VhdlExpression *severity)
  : VhdlStatement(),
    _condition(condition),
    _report(report),
    _severity(severity)
{
    if (_condition) _condition = _condition->GetConditionalExpression() ;
    if (_condition) (void) _condition->TypeInfer(StdType("boolean"),0,VHDL_READ,0) ;
    if (_report) (void) _report->TypeInfer(StdType("string"),0,VHDL_READ,0) ;
    if (_severity) (void) _severity->TypeInfer(StdType("severity_level"),0,VHDL_READ,0) ;
}
VhdlAssertionStatement::~VhdlAssertionStatement()
{
    delete _condition ;
    delete _report ;
    delete _severity ;
}

VhdlProcessStatement::VhdlProcessStatement(Array *sensitivity_list, Array *decl_part, Array *statement_part, VhdlScope *local_scope)
  : VhdlStatement(),
    _sensitivity_list(sensitivity_list),
    _decl_part(decl_part),
    _statement_part(statement_part),
    _local_scope(local_scope)
{
    // Infer sensitivity list names, so that we checked existence,
    // and set their VhdlIdDef*'s
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_sensitivity_list, i, name) {
        if (!name) continue ;
        // Sensitivity list names constitute a 'read' of the names (LRM 4.3.2), but that
        // affects RAM inference. Either way, we should be OK with less checks on sensitivity list.
        (void) name->TypeInfer(0,0,0,0) ;

        // VIPER #3148 : The call above will resolve the signal Check if it is indeed
        // a signal. According to LRM section 9.2, only static signals are allowed on a
        // process sensitivity list
        // if (!name->GetId()) continue ;
        if (name->GetId() && (!name->IsSignal() || !name->IsGloballyStaticName())) {
            name->Error("sensitivity list can have only static signal name") ;
        }
        // VIPER #3739 : Names for which reading is permitted is allowed in sensitivity
        // list of process. Output port cannot be read
        VhdlIdDef *id = name->GetId() ;
        if (id && id->IsOutput()) {
            // out mode ports can be read in 2008 mode
            if (!vhdl_file::IsVhdl2008()) name->Error("%s with mode 'out' cannot be read", id->Name()) ;
        } else if (id && id->IsLinkage()) {
            name->Error("cannot read or update 'linkage' object %s", id->Name()) ;
        }
    }

    // Check if this process contained a wait statement or not
    if (_sensitivity_list && _local_scope && _local_scope->HasWaitStatement()) {
        // error was already given when wait statement occurred (using the 'WaitStatementNotAllowed' flag).
        // Error("process cannot have both a wait statement and a sensitivity list") ;
    } else if (!_sensitivity_list && _local_scope && !_local_scope->HasWaitStatement()) {
        // VIPER 3710 : turn this into a warning, since that is what simulators do
        Warning("possible infinite loop; process does not have a wait statement") ;
    }

    // Resolve any attribute specifications
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(_local_scope) ;
    }
}
VhdlProcessStatement::~VhdlProcessStatement()
{
    // Delete the contents of the construct
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_sensitivity_list,i,elem) delete elem ;
    delete _sensitivity_list ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) delete elem ;
    delete _statement_part ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
}

VhdlBlockStatement::VhdlBlockStatement(VhdlExpression *guard, VhdlBlockGenerics *generics, VhdlBlockPorts *ports, Array *decl_part, Array *statements, VhdlScope *local_scope)
  : VhdlStatement(),
    _guard(guard),
    _generics(generics),
    _ports(ports),
    _decl_part(decl_part),
    _statements(statements),
    _local_scope(local_scope),
    _guard_id(0)
    ,_is_static_elaborated(0)
    ,_is_elab_created_empty(0)
    ,_is_gen_elab_created(0)
{
    if (_guard) _guard = _guard->GetConditionalExpression() ;
    if (_guard) {
        // The implicitly declared signal 'guard' should be declared in the local scope.
        // (It was already put in the scope earlier, to be used during the statements in this block)
        // Set it in the tree now :
        _guard_id = (_local_scope) ? _local_scope->FindSingleObject("guard") : 0 ;
        // Type-infer the guard expression.
        (void) _guard->TypeInfer((_guard_id) ? _guard_id->Type() : 0,0,VHDL_READ,0) ;
    }

    // port/generics infer by themselves during construction

    // Resolve any attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    Map *all_other_map = new Map(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(_local_scope) ;
        if (decl->IsConfigurationSpec()) decl->CheckLabel(all_other_map) ; // Viper 4738, Check whether all or other is the last label. LRM 5.2
    }
    delete (all_other_map) ;

    // VIPER #3487 : Issue error if this statement is in the statement part of entity
    VhdlIdDef *owner_id = (_present_scope) ? _present_scope->GetOwner(): 0 ;
    if (owner_id && owner_id->IsEntity()) {
        Error("%s not allowed in entity statement part", "block statements") ;
    }
}
VhdlBlockStatement::~VhdlBlockStatement()
{
    // Delete the contents of the construct
    delete _guard ;
    delete _generics ;
    delete _ports ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_statements,i,elem) delete elem ;
    delete _statements ;
    delete _guard_id ; // guard_id is 'declared' in this block, so should be deleted here too.

    // Delete the local scope after the contents of the construct
    delete _local_scope ;
}

VhdlGenerateStatement::VhdlGenerateStatement(VhdlIterScheme *scheme, Array *decl_part, Array *statement_part, VhdlScope *local_scope)
  : VhdlStatement(),
    _scheme(scheme),
    _decl_part(decl_part),
    _statement_part(statement_part),
    _local_scope(local_scope)
{
    // Nothing to do yet...

    // Resolve any attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(_local_scope) ;
    }
    // VIPER #3487 : Issue error if this statement is in the statement part of entity
    VhdlIdDef *owner_id = (_present_scope) ? _present_scope->GetOwner(): 0 ;
    if (owner_id && owner_id->IsEntity()) {
        Error("%s not allowed in entity statement part", "generate statements") ;
    }
    // VIPER #7432: VHDL 2008 LRM section 11.8 says range of for generate will
    // be static discrete range
    if (_scheme && _scheme->IsForScheme()) {
        VhdlDiscreteRange *range = _scheme->GetRange() ;
        if (range && !range->IsLocallyStatic() && !range->IsGloballyStatic()) {
            range->Warning("range in for generate must be static") ;
        }
    }
}
VhdlGenerateStatement::~VhdlGenerateStatement()
{
    // Delete the contents of the construct
    delete _scheme ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) delete elem ;
    delete _statement_part ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
}

void VhdlGenerateStatement::TypeInferChoices(VhdlIdDef *case_type)
{
    Array *choices = _scheme ? _scheme->GetChoices(): 0 ;
    VhdlExpression *elem ;
    VhdlIdDef *elem_type ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(choices, i, elem) {
        if (!elem) continue ;

        elem_type = elem->TypeInfer(case_type,0, (elem->IsRange()) ? VHDL_range : VHDL_READ,0) ;
        if (elem_type && elem->IsRange() && !elem_type->IsDiscreteType()) {
            elem->Error("choice must be a discrete range") ;
        }

        // LRM Section 8.8 : TODO : More checks
        if (!elem->IsLocallyStatic()) {
            // Viper #5273, 5494: Relax the check for globally static
            if (elem->IsGloballyStatic()) {
                elem->Warning("case choice must be a locally static expression") ;
            } else {
                elem->Error("case choice must be a locally static expression") ;
            }
        }
    }
}

VhdlComponentInstantiationStatement::VhdlComponentInstantiationStatement(VhdlName *instantiated_unit, Array *generic_map_aspect, Array *port_map_aspect)
  : VhdlStatement(),
    _instantiated_unit(instantiated_unit),
    _generic_map_aspect(generic_map_aspect),
    _port_map_aspect(port_map_aspect),
    _unit(0)
    // VIPER #6467 :
    ,_library_name(0) // name of instantiated unit's library
    ,_instantiated_unit_name(0) // name of instantiated entity
    ,_arch_name(0) // name of instantiated architecture
{
    if (!_instantiated_unit) return ;

    // Pick up the instantiated unit identifier
    // NOTE : This can return (validly) a
    //   - an entity (direct entity instantiation)
    //   - an architecture (direct entity instantiation with an architecture mentioned)
    //   - a configuration of an entity (direct configuration instantiation)
    //   - a component (most cases)

    // We just want the primary unit identifier in here.
    _unit = _instantiated_unit->EntityAspect() ;
    if (!_unit) return ;

    if (_instantiated_unit->GetEntityClass()) {
        // Check if the entity class matches
        unsigned entity_class = _instantiated_unit->GetEntityClass() ;
        //if ((_unit->IsEntity()        && (entity_class != VHDL_entity)) ||
            //(_unit->IsArchitecture()  && (entity_class != VHDL_entity)) || // Happens with indexed entity name
            //(_unit->IsComponent()     && (entity_class != VHDL_component)) ||
            //(_unit->IsConfiguration() && (entity_class != VHDL_configuration)))
        if (((entity_class == VHDL_entity) && !_unit->IsEntity() && !_unit->IsArchitecture()) ||
            ((entity_class == VHDL_configuration) && !_unit->IsConfiguration()) ||
            ((entity_class == VHDL_component) && !_unit->IsComponent()))
        {
            _instantiated_unit->Error("%s is not a %s", _unit->Name(), EntityClassName(entity_class)) ;
        }
    } else {
        // Here, it should denote a component
        if (!_unit->IsComponent()) {
            _instantiated_unit->Error("%s is not a %s", _unit->Name(), EntityClassName(VHDL_component)) ;
        }
    }

    // LRM 10.3 (j/k) : configuration can have generic/port map aspects.
    // Still set _unit to the configuration, but scope search rules need to
    // look at the entity decl (inside TypeInferAssociationList()).

    // FIX ME (in yacc ?) that components are the only ones allowed without explicit entity aspect

    // Check that there is no architecture name if it is not an entity
    if (!_unit->IsEntity() && _instantiated_unit->ArchitectureNameAspect()) {
        Error("%s is not an entity", _unit->Name()) ;
    }

    // Order generic/port map aspects. Check them agains the formal '_unit'
    // This also checks interface actual/formal match/settings.
    // Viper 6998 : For cross language this constructor is called from ConvertInstance
    // without a line_file. In that case we pass the _instantiated_unit as from. This
    // was a side issue the original issue was different.
    VhdlTreeNode *from = Linefile() ? (VhdlTreeNode*)this : (VhdlTreeNode*)_instantiated_unit ;
    _unit->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, from) ;
    _unit->TypeInferAssociationList(_port_map_aspect, VHDL_port, 0, from) ;

    // VIPER #3487 : Issue error if this statement is in the statement part of entity
    VhdlIdDef *owner_id = (_present_scope) ? _present_scope->GetOwner(): 0 ;
    if (owner_id && owner_id->IsEntity()) {
        Error("%s not allowed in entity statement part", "component instantiations") ;
    }
}

VhdlComponentInstantiationStatement::~VhdlComponentInstantiationStatement()
{
    delete _instantiated_unit ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
    FOREACH_ARRAY_ITEM(_port_map_aspect,i,elem) delete elem ;
    delete _port_map_aspect ;
    // VIPER #6467 :
    Strings::free(_library_name) ; // name of instantiated unit's library
    Strings::free(_instantiated_unit_name) ; // name of instantiated entity
    Strings::free(_arch_name) ; // name of instantiated architecture
}

VhdlExpression *
VhdlComponentInstantiationStatement::GetActualExpression(VhdlIdDef* formal_id) const
{
    if (!_unit || !formal_id || !(formal_id->IsPort() || formal_id->IsGeneric())) return 0 ;

    VhdlIdDef *formal = 0 ;
    VhdlName *formal_part = 0 ;
    VhdlExpression *actual_part = 0 ;

    VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(_library_name,1) ;
    VhdlPrimaryUnit *pu = vhdl_lib ? vhdl_lib->GetPrimUnit(_instantiated_unit_name, 1/*restore*/) : 0 ;
    VhdlIdDef *unit = pu ? pu->Id() : _unit ;

    unsigned object_kind = formal_id->IsPort() ? VHDL_port : VHDL_generic ;
    Array *assoc_list = formal_id->IsPort() ? _port_map_aspect : _generic_map_aspect ;

    VhdlDiscreteRange *assoc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
        //if (assoc && (assoc->IsBox() || assoc->IsDefault())) continue ; // assoc for package interface decl

        formal_part = (assoc) ? assoc->FormalPart() : 0 ;
        actual_part = (assoc) ? assoc->ActualPart() : 0 ;

        if (formal_part) {
            // NAMED ASSOCIATION :
            formal = formal_part->FindFormal() ;
        } else {
            // Get the formal at this position :
            switch (object_kind) {
            case VHDL_port    : formal = unit->GetPortAt(i) ; break ;
            case VHDL_generic : formal = unit->GetGenericAt(i) ; break ;
            default           : formal = unit->Arg(i) ; break ;
            }
        }
        if (!formal || formal!=formal_id) continue ;

        return actual_part ;
    }
    return 0 ;
}

VhdlIdDef *VhdlStatement::GetInstantiatedUnit() const                       { return 0 ; }
VhdlIdDef *VhdlComponentInstantiationStatement::GetInstantiatedUnit() const { return _unit ; }

VhdlProcedureCallStatement::VhdlProcedureCallStatement(VhdlName *call)
  : VhdlStatement(),
    _call(call)
{
    // Procedure call. With arguments : _call will be an indexed name.
    if (_call) (void) _call->TypeInfer(UniversalVoid(),0, 0/*no environment*/,0) ;
}
VhdlProcedureCallStatement::~VhdlProcedureCallStatement()
{
    delete _call ;
}

VhdlConditionalSignalAssignment::VhdlConditionalSignalAssignment(VhdlExpression *target, VhdlOptions *options, Array *conditional_waveforms, VhdlDelayMechanism *delay_mechanism /*= 0*/)
  : VhdlStatement(),
    _target(target),
    _options(options),
    _delay_mechanism(delay_mechanism),
    _conditional_waveforms(conditional_waveforms),
    _guard_id(0)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the waveforms
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlIdDef *value_type = 0 ;
    VhdlConditionalWaveform *elem ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms, i, elem) {
        if (!elem) continue ;
        value_type = elem->TypeInfer(target_type) ;
    }
    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_SIGUPDATE,0) ;
    }

    if (options && options->IsGuarded()) {
        // Set the pointer to the guard id :
        _guard_id = (_present_scope) ? _present_scope->FindSingleObject("guard"): 0 ;
        if (!_guard_id) {
            Error("guarded signal assignment is not in a guarded block") ;
        }
    }
    // VIPER #3487 : Issue error if this statement is in the statement part of entity
    VhdlIdDef *owner_id = (_present_scope) ? _present_scope->GetOwner(): 0 ;
    if (owner_id && owner_id->IsEntity()) {
        Error("%s not allowed in entity statement part", "signal assignments") ;
    }
}

VhdlConditionalSignalAssignment::~VhdlConditionalSignalAssignment()
{
    delete _target ;
    delete _options ;
    delete _delay_mechanism ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms,i,elem) delete elem ;
    delete _conditional_waveforms ;
}
VhdlConditionalForceAssignmentStatement::VhdlConditionalForceAssignmentStatement(VhdlExpression *target, unsigned force_mode, Array *conditional_expressions)
  : VhdlStatement(),
    _target(target),
    _force_mode(force_mode),
    _conditional_expressions(conditional_expressions)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the expressions
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlIdDef *value_type = 0 ;
    VhdlConditionalExpression *elem ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, elem) {
        if (!elem) continue ;
        value_type = elem->TypeInfer(target_type) ;
    }
    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_SIGUPDATE,0) ;
    }
}
VhdlConditionalForceAssignmentStatement::~VhdlConditionalForceAssignmentStatement()
{
    delete _target ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_conditional_expressions,i,elem) delete elem ;
    delete _conditional_expressions ;
}
VhdlSelectedSignalAssignment::VhdlSelectedSignalAssignment(VhdlExpression *expr, VhdlExpression *target, VhdlOptions *options, Array *selected_waveforms, VhdlDelayMechanism *delay_mechanism /*= 0*/, unsigned is_matching /*=0*/)
  : VhdlStatement(),
    _expr(expr),
    _target(target),
    _options(options),
    _delay_mechanism(delay_mechanism),
    _selected_waveforms(selected_waveforms),
    _is_matching(is_matching),
    _expr_type(0),
    _guard_id(0)
{
    // Infer the 'case' expression type blindly and hard
    _expr_type = _expr->TypeInfer(0,0,VHDL_READ,0) ;
    if (!_expr_type) return ;
    if (!_expr_type->IsDiscreteType() && _expr_type->Dimension()!=1) {
        Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
        return ;
    }
    if (_expr_type->ElementType() && !_expr_type->ElementType()->IsEnumerationType()) {
        Error("case expression type %s is not a character array type", _expr_type->Name()) ;
        // FIX ME : can we check the 'character' part of it ?
        return ;
    }

    // I read this somewhere in the LRM. Cant find it any more.
    if (_expr_type->IsUniversalInteger()) {
        _expr_type = StdType("integer") ;
    }

    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the waveforms
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlIdDef *value_type = 0 ;
    VhdlSelectedWaveform *elem ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, elem) {
        if (!elem) continue ;
        value_type = elem->TypeInfer(target_type, _expr_type) ;

        if (elem->IsOthers() && i!=_selected_waveforms->Size()-1) {
            elem->Error("'others' choice should be last in alternatives") ;
        }
    }

    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_SIGUPDATE,0) ;
    }

    if (options && options->IsGuarded()) {
        // Set the pointer to the guard id :
        _guard_id = (_present_scope) ? _present_scope->FindSingleObject("guard"): 0 ;
        if (!_guard_id) {
            Error("guarded signal assignment is not in a guarded block") ;
        }
    }
    // VIPER #3487 : Issue error if this statement is in the statement part of entity
    VhdlIdDef *owner_id = (_present_scope) ? _present_scope->GetOwner(): 0 ;
    if (owner_id && owner_id->IsEntity()) {
        Error("%s not allowed in entity statement part", "signal assignments") ;
    }
}

VhdlSelectedSignalAssignment::~VhdlSelectedSignalAssignment()
{
    delete _expr ;
    delete _target ;
    delete _options ;
    delete _delay_mechanism ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_selected_waveforms,i,elem) delete elem ;
    delete _selected_waveforms ;
}
VhdlIfElsifGenerateStatement::VhdlIfElsifGenerateStatement(VhdlIterScheme *scheme, Array *decl_part, Array *statement_part, VhdlScope *local_scope, Array *elsif_list, VhdlStatement *else_stmt)
  : VhdlGenerateStatement(scheme, 0, statement_part, local_scope),
    _elsif_list(elsif_list),
    _else_stmt(else_stmt)
{
    if (decl_part) VhdlGenerateStatement::AddDecls(decl_part) ;
}
VhdlIfElsifGenerateStatement::~VhdlIfElsifGenerateStatement()
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, stmt) delete stmt ;
    delete _elsif_list ;

    delete _else_stmt ;
}
// Late tree addition utility
VhdlStatement *VhdlGenerateStatement::AddElsifElse(VhdlStatement *elsif_else)
{
    if (!elsif_else) return this ; // No elsif/else statement, return itself

    // This is a if-generate with elseif/else statement. So we need to create
    // new class for VHDL-2008 specified if-generate statement.
    // Check whether coming one is elsif/else statement
    VhdlIterScheme *scheme = elsif_else->GetScheme() ;
    if (!scheme) return this ; // Something wrong

    Array *elsif_list = 0 ;
    VhdlStatement *else_stmt = 0 ;
    if (scheme->IsElsifScheme()) {
        elsif_list = new Array(1) ;
        elsif_list->InsertLast(elsif_else) ;
    } else if (scheme->IsElseScheme()) {
        else_stmt = elsif_else ;
    }
    // Now create VhdlIfElsifGenerateStatement :
    VhdlStatement *if_gen = new VhdlIfElsifGenerateStatement(_scheme, 0, _statement_part, _local_scope, elsif_list, else_stmt) ;
    if (_decl_part) if_gen->AddDecls(_decl_part) ;
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    if (_closing_label) {
        // VIPER #6662: Take the closing label of 'this' and set to the new statement:
        if_gen->SetClosingLabel(_closing_label) ;
        // Do not call SetClosingLabel(), that will delete _closing_label!
        _closing_label = 0 ; // Directly reset the closing label
    }
#endif

    // Clear local pointers
    _scheme = 0 ;
    _decl_part = 0 ;
    _statement_part = 0 ;
    _local_scope = 0 ;

    delete this ;
    return if_gen ;
}
void VhdlGenerateStatement::AddDecls(Array *decls)
{
    _decl_part = decls ;
}
// Check matching of alternative label of if/elsif/else generate branch
void VhdlIfElsifGenerateStatement::ClosingAlternativeLabel(VhdlDesignator *id)
{
    // Compare alternative label with last branch of if-elsif-else generate statement
    if (_else_stmt) {
        _else_stmt->ClosingAlternativeLabel(id) ;
        return ;
    }

    VhdlStatement *last_elsif_stmt = _elsif_list ? (VhdlStatement*)_elsif_list->GetLast(): 0 ;
    if (last_elsif_stmt) last_elsif_stmt->ClosingAlternativeLabel(id) ;
}
VhdlStatement *VhdlIfElsifGenerateStatement::AddElsifElse(VhdlStatement *elsif_else)
{
    if (!elsif_else) return this ; // No elsif/else statement, return itself

    // We need to add coming elsif/else generate statement here
    // Check whether coming one is elsif/else statement
    VhdlIterScheme *scheme = elsif_else->GetScheme() ;
    if (!scheme) return this ; // Something wrong

    if (scheme->IsElsifScheme()) {
        if (!_elsif_list) _elsif_list = new Array(1) ;
        _elsif_list->InsertLast(elsif_else) ;
    } else if (scheme->IsElseScheme()) {
        if (_else_stmt) {
            elsif_else->Error("Illegal if generate syntax, else branch should appear last") ;
            delete _else_stmt ;
        }
        _else_stmt = elsif_else ;
    }
    return this ;
}
VhdlCaseGenerateStatement::VhdlCaseGenerateStatement(VhdlExpression *expr, Array *alternatives)
  : VhdlStatement(),
    _expr(expr),
    _alternatives(alternatives),
    _expr_type(0)
{
    // VIPER #3487 : Issue error if this statement is in the statement part of entity
    VhdlIdDef *owner_id = (_present_scope) ? _present_scope->GetOwner(): 0 ;
    if (owner_id && owner_id->IsEntity()) {
        Error("%s not allowed in entity statement part", "generate statements") ;
    }
    // Infer 'case' expression
    // Viper #5550: pass return_types set and prune the list based on this context
    Set return_types(POINTER_HASH) ;
    _expr_type = 0 ;
    (void) _expr->TypeInfer(0,&return_types,VHDL_READ,0) ;
    // VIPER #7432: VHDL 2008 LRM section 11.8 says expression of case generate will
    // be globally static
    if (!_expr->IsExternalName() && !_expr->IsGloballyStatic()) {
        _expr->Warning("expression in case generate must be globally static") ;
    }
    unsigned multiple_match_expr = (return_types.Size() > 1) ? 1: 0 ;
    if (return_types.Size() > 1) {
        SetIter si ;
        VhdlIdDef *type ;
        FOREACH_SET_ITEM(&return_types, si, &type) {
            if ((!type->IsDiscreteType() && type->Dimension()!=1) ||
                (type->ElementType() && !type->ElementType()->IsEnumerationType()) ||
                (type->IsAccessType() || type->IsFileType())) {
                (void) return_types.Remove(type) ;
            }

            if (return_types.Size() == 1) break ;
        }
    }

    if (return_types.Size() > 1) {
        // Viper #5550: Give error here as TypeInfer no longer does
        // this check (for we passed the return_types set)
        _expr->Error("near %s ; %d visible identifiers match here", "case_expression", return_types.Size()) ;
    } else {
        _expr_type = (VhdlIdDef*) return_types.GetLast() ;
    }

    if (!_expr_type) return ;
    if (!_expr_type->IsDiscreteType() && _expr_type->Dimension()!=1) {
        Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
        return ;
    }
    if (_expr_type->ElementType() && !_expr_type->ElementType()->IsEnumerationType()) {
        Error("case expression type %s is not a character array type", _expr_type->Name()) ;
        // FIX ME : can we check the 'character' part of it ?
        return ;
    }
    // VIPER 3153 : check if it is another bad type :
    if (_expr_type->IsAccessType() || _expr_type->IsFileType()) {
        Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
        return ;
    }
    // VIPER #5789 : If type infer of case expression returns multiple types, and
    // '_expr_type' can be extracted, try to type infer case expression again passing
    // '_expr_type' as expected type. This will help to set proper resolved identifier
    // to case expression :
    if (multiple_match_expr) {
        return_types.Reset() ;
        (void) _expr->TypeInfer(_expr_type,&return_types,VHDL_READ,0) ;
    }

    VhdlIdDef *expr_id = _expr->GetId() ;
    if (expr_id && _expr_type->IsArrayType() && _expr->FindFullFormal() && !expr_id->IsLocallyStaticType()) Error("array type case expression must be of a locally static subtype") ; // Viper #4452, 3699

    // I read this somewhere in the LRM. Cant find it any more.
    if (_expr_type->IsUniversalInteger()) {
        _expr_type = StdType("integer") ;
    }

    VhdlGenerateStatement *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_alternatives, i, elem) {
        if (elem) elem->TypeInferChoices(_expr_type) ;
    }
}
VhdlCaseGenerateStatement::~VhdlCaseGenerateStatement()
{
    delete _expr ;
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_alternatives, i, stmt) delete stmt ;
    delete _alternatives ; // Memory leak fix
}
VhdlConditionalVariableAssignmentStatement::VhdlConditionalVariableAssignmentStatement(VhdlExpression *target, Array *conditional_expressions)
  : VhdlStatement(),
    _target(target),
    _conditional_expressions(conditional_expressions)
{
    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_VARUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the expressions
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlIdDef *value_type = 0 ;
    VhdlConditionalExpression *elem ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, elem) {
        if (!elem) continue ;
        value_type = elem->TypeInfer(target_type) ;
    }
    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_VARUPDATE,0) ;
    }
}

VhdlConditionalVariableAssignmentStatement::~VhdlConditionalVariableAssignmentStatement()
{
    delete _target ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_conditional_expressions,i,elem) delete elem ;
    delete _conditional_expressions ;
}

VhdlSelectedVariableAssignmentStatement::VhdlSelectedVariableAssignmentStatement(VhdlExpression *expr, VhdlExpression *target, Array *selected_exprs, unsigned is_matching)
  : VhdlStatement(),
    _expr(expr),
    _target(target),
    _selected_exprs(selected_exprs),
    _is_matching(is_matching),
    _expr_type(0)
{
    // Infer the 'case' expression type blindly and hard
    _expr_type = _expr->TypeInfer(0,0,VHDL_READ,0) ;
    if (!_expr_type) return ;
    if (!_expr_type->IsDiscreteType() && _expr_type->Dimension()!=1) {
        Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
        return ;
    }
    if (_expr_type->ElementType() && !_expr_type->ElementType()->IsEnumerationType()) {
        Error("case expression type %s is not a character array type", _expr_type->Name()) ;
        // FIX ME : can we check the 'character' part of it ?
        return ;
    }

    // I read this somewhere in the LRM. Cant find it any more.
    if (_expr_type->IsUniversalInteger()) {
        _expr_type = StdType("integer") ;
    }

    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_VARUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the waveforms
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlIdDef *value_type = 0 ;
    VhdlSelectedExpression *elem ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, elem) {
        if (!elem) continue ;
        value_type = elem->TypeInfer(target_type, _expr_type) ;

        if (elem->IsOthers() && i!=_selected_exprs->Size()-1) {
            elem->Error("'others' choice should be last in alternatives") ;
        }
    }

    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_VARUPDATE,0) ;
    }
}
VhdlSelectedVariableAssignmentStatement::~VhdlSelectedVariableAssignmentStatement()
{
    delete _expr ;
    delete _target ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_selected_exprs,i,elem) delete elem ;
    delete _selected_exprs ;
}
VhdlSelectedForceAssignment::VhdlSelectedForceAssignment(VhdlExpression *expr, unsigned is_matching, VhdlExpression *target, unsigned force_mode, Array *selected_exprs)
  : VhdlStatement(),
    _expr(expr),
    _target(target),
    _is_matching(is_matching),
    _force_mode(force_mode),
    _selected_exprs(selected_exprs),
    _expr_type(0)
{
    // Infer the 'case' expression type blindly and hard
    _expr_type = _expr->TypeInfer(0,0,VHDL_READ,0) ;
    if (!_expr_type) return ;
    if (!_expr_type->IsDiscreteType() && _expr_type->Dimension()!=1) {
        Error("case expression type %s is not a discrete or 1-dimensional character array type", _expr_type->Name()) ;
        return ;
    }
    if (_expr_type->ElementType() && !_expr_type->ElementType()->IsEnumerationType()) {
        Error("case expression type %s is not a character array type", _expr_type->Name()) ;
        // FIX ME : can we check the 'character' part of it ?
        return ;
    }

    // I read this somewhere in the LRM. Cant find it any more.
    if (_expr_type->IsUniversalInteger()) {
        _expr_type = StdType("integer") ;
    }

    // Call target with list of expected types (so we don't error out if
    // we cannot determine the type from the target alone.
    Set lval_types(POINTER_HASH) ;
    VhdlIdDef *target_type = _target->TypeInfer(0, &lval_types, VHDL_SIGUPDATE,0) ;
    if (lval_types.Size()==0) return ; // error is already given

    // Now call the waveforms
    // Use target type as a guide, but if there is not a unique target
    // type, error out (don't give a list of expected types).
    // This way, expression must be self-determined if target is not.
    // This might be a little bit stricter than the LRM, but it simplifies
    // inference considerably.
    unsigned i ;
    VhdlIdDef *value_type = 0 ;
    VhdlSelectedExpression *elem ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, elem) {
        if (!elem) continue ;
        value_type = elem->TypeInfer(target_type, _expr_type) ;

        if (elem->IsOthers() && i!=_selected_exprs->Size()-1) {
            elem->Error("'others' choice should be last in alternatives") ;
        }
    }

    if (!target_type && value_type) {
        // Infer target with the value_type, since target was not yet inferred
        (void) _target->TypeInfer(value_type,0, VHDL_SIGUPDATE,0) ;
    }
}
VhdlSelectedForceAssignment::~VhdlSelectedForceAssignment()
{
    delete _expr ;
    delete _target ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_selected_exprs,i,elem) delete elem ;
    delete _selected_exprs ;
}
/**************************************************************/
// Calculate Signature (only for entity statement parts (LRM 1.1.3)
/**************************************************************/

#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlStatement::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_label) sig = HASH_VAL(sig, _label->CalculateSignature()) ;
    sig = HASH_VAL(sig, _postponed) ;
    return sig ;
}

unsigned long
VhdlNullStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlReturnStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    // back-pointer : if (_owner) sig = HASH_VAL(sig, _owner->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlExitStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    // back-pointer : if (_target_label) sig = HASH_VAL(sig, _target_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlNextStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    // back-pointer : if (_target_label) sig = HASH_VAL(sig, _target_label->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlLoopStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_iter_scheme) sig = HASH_VAL(sig, _iter_scheme->CalculateSignature()) ;
    VhdlStatement *stmt ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlCaseStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    VhdlCaseStatementAlternative *alt ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_alternatives, i, alt) {
        if (alt) sig = HASH_VAL(sig, alt->CalculateSignature()) ;
    }
    if (_is_matching_case) sig = HASH_VAL(sig, _is_matching_case) ;
    // back-pointer : if (_expr_type) sig = HASH_VAL(sig, _expr_type->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlIfStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_if_cond) sig = HASH_VAL(sig, _if_cond->CalculateSignature()) ;
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_if_statements, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    VhdlElsif *elsif ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif) {
        if (elsif) sig = HASH_VAL(sig, elsif->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_else_statements, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlVariableAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_value) sig = HASH_VAL(sig, _value->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSignalAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_delay_mechanism) sig = HASH_VAL(sig, _delay_mechanism->CalculateSignature()) ;
    VhdlExpression *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_waveform, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlForceAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_force_mode) sig = HASH_VAL(sig, _force_mode) ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlReleaseAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_force_mode) sig = HASH_VAL(sig, _force_mode) ;
    return sig ;
}
unsigned long
VhdlWaitStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_sensitivity_clause, i, name) {
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    if (_condition_clause) sig = HASH_VAL(sig, _condition_clause->CalculateSignature()) ;
    if (_timeout_clause) sig = HASH_VAL(sig, _timeout_clause->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlReportStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_report) sig = HASH_VAL(sig, _report->CalculateSignature()) ;
    if (_severity) sig = HASH_VAL(sig, _severity->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlAssertionStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_condition) sig = HASH_VAL(sig, _condition->CalculateSignature()) ;
    if (_report) sig = HASH_VAL(sig, _report->CalculateSignature()) ;
    if (_severity) sig = HASH_VAL(sig, _severity->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlProcessStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    unsigned i ;
    VhdlName *name ;
    FOREACH_ARRAY_ITEM(_sensitivity_list, i, name) {
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlBlockStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_guard) sig = HASH_VAL(sig, _guard->CalculateSignature()) ;
    if (_generics) sig = HASH_VAL(sig, _generics->CalculateSignature()) ;
    if (_ports) sig = HASH_VAL(sig, _ports->CalculateSignature()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
// back-pointer : if (_guard_id) sig = HASH_VAL(sig, _guard_id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlGenerateStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_scheme) sig = HASH_VAL(sig, _scheme->CalculateSignature()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlComponentInstantiationStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_instantiated_unit) sig = HASH_VAL(sig, _instantiated_unit->CalculateSignature()) ;
    unsigned i ;
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_unit) sig = HASH_VAL(sig, _unit->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlProcedureCallStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_call) sig = HASH_VAL(sig, _call->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlConditionalSignalAssignment::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_options) sig = HASH_VAL(sig, _options->CalculateSignature()) ;
    if (_delay_mechanism) sig = HASH_VAL(sig, _delay_mechanism->CalculateSignature()) ;
    VhdlConditionalWaveform *waveform ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms, i, waveform) {
        if (waveform) sig = HASH_VAL(sig, waveform->CalculateSignature()) ;
    }
    // back-pointer : if (_guard_id) sig = HASH_VAL(sig, _guard_id->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlConditionalForceAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_force_mode) sig = HASH_VAL(sig, _force_mode) ;
    VhdlConditionalExpression *expression ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, expression) {
        if (expression) sig = HASH_VAL(sig, expression->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlConditionalVariableAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    VhdlConditionalExpression *expression ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, expression) {
        if (expression) sig = HASH_VAL(sig, expression->CalculateSignature()) ;
    }
    // back-pointer : if (_guard_id) sig = HASH_VAL(sig, _guard_id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSelectedSignalAssignment::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    if (_options) sig = HASH_VAL(sig, _options->CalculateSignature()) ;
    if (_delay_mechanism) sig = HASH_VAL(sig, _delay_mechanism->CalculateSignature()) ;
    VhdlSelectedWaveform *waveform ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, waveform) {
        if (waveform) sig = HASH_VAL(sig, waveform->CalculateSignature()) ;
    }
    if (_is_matching) sig = HASH_VAL(sig, _is_matching) ;
    // back-pointer : if (_expr_type) sig = HASH_VAL(sig, _expr_type->CalculateSignature()) ;
    // back-pointer : if (_guard_id) sig = HASH_VAL(sig, _guard_id->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlIfElsifGenerateStatement::CalculateSignature() const
{
    unsigned long sig = VhdlGenerateStatement::CalculateSignature() ;
    unsigned i ;
    VhdlGenerateStatement *elsif_stmt ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif_stmt) {
        if (elsif_stmt) sig = HASH_VAL(sig, elsif_stmt->CalculateSignature()) ;
    }
    if (_else_stmt) sig = HASH_VAL(sig, _else_stmt->CalculateSignature()) ;

    return sig ;
}
unsigned long
VhdlCaseGenerateStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;

    unsigned i ;
    VhdlStatement *alternative ;
    FOREACH_ARRAY_ITEM(_alternatives, i, alternative) {
        if (alternative) sig = HASH_VAL(sig, alternative->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlSelectedVariableAssignmentStatement::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    VhdlSelectedExpression *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_expr_type) sig = HASH_VAL(sig, _expr_type->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSelectedForceAssignment::CalculateSignature() const
{
    unsigned long sig = VhdlStatement::CalculateSignature() ;
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    if (_target) sig = HASH_VAL(sig, _target->CalculateSignature()) ;
    VhdlSelectedExpression *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    return sig ;
}
// VIPER #7752: Returns linefile of "begin" keyword or the empty space in there.
linefile_type
VhdlBlockStatement::GetDeclAndStatementSeparatorLinefile() const
{
    linefile_type result = 0 ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    linefile_type my_lf = Linefile() ;
    if (!my_lf) return 0 ; // I have no linefile info

    // Get the last declaration:
    unsigned i ;
    VhdlDeclaration *last_decl = 0 ;
    FOREACH_ARRAY_ITEM_BACK(_decl_part, i, last_decl) if (last_decl) break ;
    linefile_type start_lf = (last_decl) ? last_decl->Linefile() : 0 ;
    if (start_lf) {
        result = start_lf->Copy() ;
        // Starting linefile is the end of last declaration:
        result->SetLeftLine(start_lf->GetRightLine()) ;
        result->SetLeftCol(start_lf->GetRightCol()) ;
    } else {
        // The whole of block statement, should not happen, though
        result = my_lf->Copy() ;
    }
    // Get the first statement:
    VhdlStatement *first_stmt = 0 ;
    FOREACH_ARRAY_ITEM(_statements, i, first_stmt) if (first_stmt) break ;
    linefile_type first_stmt_lf = (first_stmt) ? first_stmt->Linefile() : 0 ;
    if (first_stmt_lf) {
        // Ending linefile is the start of the first statement:
        result->SetRightLine(first_stmt_lf->GetLeftLine()) ;
        result->SetRightCol(first_stmt_lf->GetLeftCol()) ;
    } else if (start_lf) {
        // Otherwise set to the end of my location:
        result->SetRightLine(my_lf->GetRightLine()) ;
        result->SetRightCol(my_lf->GetRightCol()) ;
    }

    TextBasedDesignMod tbdm ;
    // Get the text from the last declaration to first statement, make it lower case (VHDL is case insensitive):
    char *str = Strings::strtolower(tbdm.GetText(result, 0 /* do not include any modification */)) ;
    if (!str) return 0 ;

    // Start with the left side of the location:
    unsigned line = result->GetLeftLine() ;
    unsigned col = result->GetLeftCol() ;

    // And find the position of either "begin" keyword for the "end block":
    unsigned within_long_comment = 0 ; // VHDL 2008 long comment
    unsigned within_short_comment = 0 ;
    unsigned locations_set = 0 ;
    char *s = str ;
    while (*s) {
        if ((*s=='\n') || (*s=='\r')) { line++ ; col = 1 ; }
        else /* non new-line chars */ { col++ ; }

        if (within_long_comment) {
            // Skip VHDL 2008 long comments:
            if ((*s=='*') && (*(s+1)=='/')) { within_long_comment = 0 ; s++ ; col++ ; }
            s++ ;
            continue ;
        } else if (within_short_comment) {
            // Skip short comments:
            if ((*s=='\n') || (*s=='\r')) { within_short_comment = 0 ; }
            s++ ;
            continue ;
        }

        // CHECKME: Are we within translate/synthesis off?
        if (_decl_part && ((*s)=='b') && ((*(s+1))=='e') && ((*(s+2))=='g') &&
            ((*(s+3))=='i') && ((*(s+4))=='n')) {
            // Found the required start/end position with the "begin" keyword
            if (col>1) col-- ;           // It should be before "begin", we are already at 'b', so decrease one
            result->SetLeftLine(line) ;  // Set the left line at the "begin" keyword
            result->SetLeftCol(col) ;    // Set the left col at the start of "begin" keyword
            result->SetRightLine(line) ; // Set the left line at the "begin" keyword
            result->SetRightCol(col+5) ; // Set the left col after the "begin" keyword
            locations_set = 2 ;          // Both start and end locations are now set
            break ;                      // Done
        } else if (!last_decl && !locations_set && ((*s)=='b') && ((*(s+1))=='l') &&
                   ((*(s+2))=='o') && ((*(s+3))=='c') && ((*(s+4))=='k')) {
            // Found the required start position with the "block" keyword
            result->SetLeftLine(line) ;  // Set the left line at the "block" keyword
            result->SetLeftCol(col+4) ;  // Set the left col after the "block" keyword
            locations_set++ ;            // Only start location is set here
            if (first_stmt_lf && !_decl_part) {
                // Here a statement was specified, but no "begin" keyword.
                // Since we alerady set the end line to the start of the first statement
                // we have nothing more to do here
                locations_set++ ;        // End location is already set above
                break ;                  // Done
            }
            s = s+5 ;                    // Go past the "block" keyword
            continue ;                   // Continue to check for end location
        } else if (!first_stmt && locations_set && ((*s)=='e') && ((*(s+1))=='n') && ((*(s+2))=='d')) {
            // Found the required end position with the "end" keyword
            // CHECKME: Assume it is the "end block" here, or check the "block" keyword too?
            if (col>1) col-- ;           // It should be before "end", we are already at 'e', so decrease one
            result->SetRightLine(line) ; // Set the left line at the "end" keywrod
            result->SetRightCol(col) ;    // Set the left col at the start of "end" keywrod
            locations_set++ ;            // Only end location is set here
            break ;                      // Done
        }

        if ((*s=='/') && (*(s+1)=='*'))      { within_long_comment = 1 ; s++ ; col++ ; }  // Go into VHDL 2008 long comment
        else if ((*s=='-') && (*(s+1)=='-')) { within_short_comment = 1 ; s++ ; col++ ; } // Go into short comment
        s++ ;
    }

    Strings::free(str) ; // Cleanup now

    if (locations_set!=2) return 0 ; // We could not find both the start and end locations
#endif

    return result ;
}

// VIPER #7752: Returns linefile of "begin" keyword or the empty space in there.
linefile_type
VhdlGenerateStatement::GetDeclAndStatementSeparatorLinefile() const
{
    linefile_type result = 0 ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    linefile_type my_lf = Linefile() ;
    if (!my_lf) return 0 ; // I have no linefile info

    // Get the last declaration:
    unsigned i ;
    VhdlDeclaration *last_decl = 0 ;
    FOREACH_ARRAY_ITEM_BACK(_decl_part, i, last_decl) if (last_decl) break ;
    linefile_type start_lf = (last_decl) ? last_decl->Linefile() : ((_scheme) ? _scheme->Linefile() : 0) ;
    if (start_lf) {
        result = start_lf->Copy() ;
        // Starting linefile is the end of last declaration:
        result->SetLeftLine(start_lf->GetRightLine()) ;
        result->SetLeftCol(start_lf->GetRightCol()) ;
    } else {
        // The whole of generate statement, should not happen, though
        result = my_lf->Copy() ;
    }

    // Get the first statement:
    VhdlStatement *first_stmt = 0 ;
    FOREACH_ARRAY_ITEM(_statement_part, i, first_stmt) if (first_stmt) break ;
    linefile_type first_stmt_lf = (first_stmt) ? first_stmt->Linefile() : 0 ;
    if (first_stmt_lf) {
        // Ending linefile is the start of the first statement:
        result->SetRightLine(first_stmt_lf->GetLeftLine()) ;
        result->SetRightCol(first_stmt_lf->GetLeftCol()) ;
    } else if (start_lf) {
        // Otherwise set to the end of my location:
        result->SetRightLine(my_lf->GetRightLine()) ;
        result->SetRightCol(my_lf->GetRightCol()) ;
    }

    TextBasedDesignMod tbdm ;
    // Get the text from the last declaration to first statement, make it lower case (VHDL is case insensitive):
    char *str = Strings::strtolower(tbdm.GetText(result, 0 /* do not include any modification */)) ;
    if (!str) return 0 ;

    // Start with the left side of the location:
    unsigned line = result->GetLeftLine() ;
    unsigned col = result->GetLeftCol() ;

    // And find the position of either "begin" keyword for the "end generate":
    unsigned within_long_comment = 0 ; // VHDL 2008 long comment
    unsigned within_short_comment = 0 ;
    unsigned locations_set = 0 ;
    char *s = str ;
    while (*s) {
        if ((*s=='\n') || (*s=='\r')) { line++ ; col = 1 ; }
        else /* non new-line chars */ { col++ ; }

        if (within_long_comment) {
            // Skip VHDL 2008 long comments:
            if ((*s=='*') && (*(s+1)=='/')) { within_long_comment = 0 ; s++ ; col++ ; }
            s++ ;
            continue ;
        } else if (within_short_comment) {
            // Skip short comments:
            if ((*s=='\n') || (*s=='\r')) { within_short_comment = 0 ; }
            s++ ;
            continue ;
        }

        // CHECKME: Are we within translate/synthesis off?
        if (_decl_part && ((*s)=='b') && ((*(s+1))=='e') && ((*(s+2))=='g') &&
            ((*(s+3))=='i') && ((*(s+4))=='n')) {
            // Found the required start/end position with the "begin" keyword
            if (col>1) col-- ;           // It should be before "begin", we are already at 'b', so decrease one
            result->SetLeftLine(line) ;  // Set the left line at the "begin" keyword
            result->SetLeftCol(col) ;    // Set the left col at the start of "begin" keyword
            result->SetRightLine(line) ; // Set the right line at the "begin" keyword
            result->SetRightCol(col+5) ; // Set the right col after the "begin" keyword
            locations_set = 2 ;          // Both start and end locations are now set
            break ;                      // Done
        } else if (!last_decl && !locations_set && ((*s)=='g') && ((*(s+1))=='e') &&
                   ((*(s+2))=='n') && ((*(s+3))=='e') && ((*(s+4))=='r') &&
                   ((*(s+5))=='a') && ((*(s+6))=='t') && ((*(s+7))=='e')) {
            // Found the required start position with the "generate" keyword
            result->SetLeftLine(line) ;  // Set the left line at the "generate" keyword
            result->SetLeftCol(col+7) ;  // Set the left col after the "generate" keyword
            locations_set++ ;            // Only start location is set here
            if (first_stmt_lf && !_decl_part) {
                // Here a statement was specified, but no "begin" keyword.
                // Since we alerady set the end line to the start of the first statement
                // we have nothing more to do here
                locations_set++ ;        // End location is already set above
                break ;                  // Done
            }
            s = s+8 ;                    // Go past the "generate" keyword
            col = col+7 ;                // Update the column accordingly
            continue ;                   // Continue to check for end location
        } else if (!first_stmt && locations_set && ((*s)=='e') && ((*(s+1))=='n') && ((*(s+2))=='d')) {
            // Found the required end position with the "end" keyword
            // CHECKME: Assume it is the "end generate" here, or check the "generate" keyword too?
            if (col>1) col-- ;           // It should be before "end", we are already at 'e', so decrease one
            result->SetRightLine(line) ; // Set the right line at the "end" keywrod
            result->SetRightCol(col) ;    // Set the right col at the start of "end" keywrod
            locations_set++ ;            // Only end location is set here
            break ;                      // Done
        }

        if ((*s=='/') && (*(s+1)=='*'))      { within_long_comment = 1 ; s++ ; col++ ; }  // Go into VHDL 2008 long comment
        else if ((*s=='-') && (*(s+1)=='-')) { within_short_comment = 1 ; s++ ; col++ ; } // Go into short comment
        s++ ;
    }

    Strings::free(str) ; // Cleanup now

    if (locations_set!=2) return 0 ; // We could not find both the start and end locations
#endif

    return result ;
}

