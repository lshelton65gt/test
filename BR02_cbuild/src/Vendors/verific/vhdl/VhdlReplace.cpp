/*
 *
 * [ File Version : 1.15 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <sstream>    // for ostringstream
#include <iostream>    // for ostringstream
#include "Array.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlMisc.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlStatement.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "vhdl_file.h"
#include "VhdlScope.h"
#include "Strings.h"
#include "vhdl_tokens.h"
using namespace std ;
#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

#define REPLACE_IN_NODE(C, O, N) \
    if ((C) == (O)) {            \
        delete (C) ;             \
        C = N ;                  \
        return 1 ;               \
    }

#define REPLACE_IN_ARRAY(A, O, N)    \
{                                    \
    unsigned I ;                     \
    VhdlTreeNode *node ;             \
    FOREACH_ARRAY_ITEM(A, I, node) { \
        if (node == (O)) {           \
            delete (O) ;             \
            (A)->Insert(I, N) ;      \
            return 1 ;               \
        }                            \
    }                                \
}

unsigned
VhdlBlockConfiguration::ReplaceChildName(VhdlName *old_block_spec, VhdlName *new_block_spec)
{
    REPLACE_IN_NODE(_block_spec, old_block_spec, new_block_spec) ;
    return 0 ;
}

unsigned
VhdlBlockConfiguration::ReplaceChildUseClause(VhdlUseClause *old_use_clause, VhdlUseClause *new_use_clause)
{
    REPLACE_IN_ARRAY(_use_clause_list, old_use_clause, new_use_clause) ;
    return 0 ;
}

unsigned
VhdlBlockConfiguration::ReplaceChildConfigItem(VhdlConfigurationItem *old_item, VhdlConfigurationItem *new_item)
{
    REPLACE_IN_ARRAY(_configuration_item_list, old_item, new_item) ;
    return 0 ;
}

unsigned
VhdlComponentConfiguration::ReplaceChildSpec(VhdlSpecification *old_com_spec, VhdlSpecification *new_com_spec)
{
    REPLACE_IN_NODE(_component_spec, old_com_spec, new_com_spec) ;
    return 0 ;
}
unsigned
VhdlComponentConfiguration::ReplaceChildBindingIndication(VhdlBindingIndication *old_binding, VhdlBindingIndication *new_binding)
{
    REPLACE_IN_NODE(_binding, old_binding, new_binding) ;
    return 0 ;
}

unsigned
VhdlComponentConfiguration::ReplaceChildBlockConfig(VhdlBlockConfiguration *old_node, VhdlBlockConfiguration *new_node)
{
    REPLACE_IN_NODE(_block_configuration, old_node, new_node) ;
    return 0 ;
}

unsigned
VhdlScalarTypeDef::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_NODE(_scalar_range, old_node, new_node) ;
    return 0 ;
}

unsigned
VhdlArrayTypeDef::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_index_constraint, old_node, new_node) ;
    return 0 ;
}

unsigned
VhdlArrayTypeDef::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlRecordTypeDef::ReplaceChildElementDecl(VhdlElementDecl *old_node, VhdlElementDecl *new_node)
{
    REPLACE_IN_ARRAY(_element_decl_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAccessTypeDef::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}

unsigned
VhdlFileTypeDef::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_file_type_mark, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlEnumerationTypeDef::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_enumeration_literal_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPhysicalTypeDef::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_NODE(_range_constraint, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPhysicalTypeDef::ReplaceChildPhysicalUnitDecl(VhdlPhysicalUnitDecl *old_node, VhdlPhysicalUnitDecl *new_node)
{
    REPLACE_IN_ARRAY(_physical_unit_decl_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlElementDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlElementDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPhysicalUnitDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPhysicalUnitDecl::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_physical_literal, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlUseClause::ReplaceChildSelectedName(VhdlSelectedName *old_node, VhdlSelectedName *new_node)
{
    REPLACE_IN_ARRAY(_selected_name_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlLibraryClause::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInterfaceDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInterfaceDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInterfaceDecl::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_init_assign, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogramDecl::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_subprogram_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogramBody::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_subprogram_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogramBody::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogramBody::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statement_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubtypeDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubtypeDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFullTypeDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFullTypeDecl::ReplaceChildTypeDef(VhdlTypeDef *old_node, VhdlTypeDef *new_node)
{
    REPLACE_IN_NODE(_type_def, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIncompleteTypeDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConstantDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConstantDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConstantDecl::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_init_assign, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignalDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignalDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignalDecl::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_init_assign, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlVariableDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlVariableDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlVariableDecl::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_init_assign, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFileDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFileDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFileDecl::ReplaceChildFileOpenInfo(VhdlFileOpenInfo *old_node, VhdlFileOpenInfo *new_node)
{
    REPLACE_IN_NODE(_file_open_info, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAliasDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_designator, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAliasDecl::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAliasDecl::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_alias_target_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlComponentDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlComponentDecl::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_generic_clause, old_node, new_node) ;
    REPLACE_IN_ARRAY(_port_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeDecl::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_type_mark, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeSpec::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_designator, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeSpec::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_entity_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeSpec::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_value, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConfigurationSpec::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_component_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConfigurationSpec::ReplaceChildBindingIndication(VhdlBindingIndication *old_node, VhdlBindingIndication *new_node)
{
    REPLACE_IN_NODE(_binding, old_node, new_node) ;
    return 0 ;
}

unsigned
VhdlDisconnectionSpec::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_guarded_signal_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlDisconnectionSpec::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_after, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGroupTemplateDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGroupTemplateDecl::ReplaceChildEntityClassEntry(VhdlEntityClassEntry *old_node, VhdlEntityClassEntry *new_node)
{
    REPLACE_IN_ARRAY(_entity_class_entry_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGroupDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGroupDecl::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_group_template_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogInstantiationDecl::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_uninstantiated_subprog_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogInstantiationDecl::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_subprogram_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSubprogInstantiationDecl::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_generic_map_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInterfaceTypeDecl::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_type_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInterfaceSubprogDecl::ReplaceChildSpec(VhdlSpecification *old_node, VhdlSpecification *new_node)
{
    REPLACE_IN_NODE(_subprogram_spec, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlExplicitSubtypeIndication::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_res_function, old_node, new_node) ;
    REPLACE_IN_NODE(_type_mark, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlExplicitSubtypeIndication::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_NODE(_range_constraint, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlRange::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_left, old_node, new_node) ;
    REPLACE_IN_NODE(_right, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAssocElement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_formal_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAssocElement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_actual_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInertialElement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_actual_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlRecResFunctionElement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_record_id_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlRecResFunctionElement::ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/)
{
    return 0 ;
}
unsigned
VhdlOperator::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_left, old_node, new_node) ;
    REPLACE_IN_NODE(_right, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAllocator::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_subtype, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAggregate::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_element_assoc_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlQualifiedExpression::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_prefix, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlQualifiedExpression::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_aggregate, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlElementAssoc::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_choices, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlElementAssoc::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlWaveformElement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_value, old_node, new_node) ;
    REPLACE_IN_NODE(_after, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignature::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_ARRAY(_type_mark_list, old_node, new_node) ;
    REPLACE_IN_NODE(_return_type, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFileOpenInfo::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_file_open, old_node, new_node) ;
    REPLACE_IN_NODE(_file_logical_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBindingIndication::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_entity_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBindingIndication::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_generic_map_aspect, old_node, new_node) ;
    REPLACE_IN_ARRAY(_port_map_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlWhileScheme::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlWhileScheme::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_exit_label, old_node, new_node) ;
    REPLACE_IN_NODE(_next_label, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlForScheme::ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node)
{
    REPLACE_IN_NODE(_id, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlForScheme::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_NODE(_range, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlForScheme::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_iter_id, old_node, new_node) ;
    REPLACE_IN_NODE(_exit_label, old_node, new_node) ;
    REPLACE_IN_NODE(_next_label, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIfScheme::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlCaseItemScheme::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_ARRAY(_choices, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInertialDelay::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_reject_expression, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlOptions::ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node)
{
    REPLACE_IN_NODE(_delay_mechanism, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalWaveform::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_ARRAY(_waveform, old_node, new_node) ;
    REPLACE_IN_NODE(_when_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalExpression::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedWaveform::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_ARRAY(_waveform, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedWaveform::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_when_choices, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockGenerics::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_generic_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockGenerics::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_generic_map_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockPorts::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_port_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockPorts::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_port_map_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlCaseStatementAlternative::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_choices, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlCaseStatementAlternative::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statements, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlElsif::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlElsif::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statements, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPhysicalLiteral::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_value, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPhysicalLiteral::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_unit, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedName::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_prefix, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedName::ReplaceChildDesignator(VhdlDesignator *old_node, VhdlDesignator *new_node)
{
    REPLACE_IN_NODE(_suffix, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIndexedName::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_prefix, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIndexedName::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_assoc_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignaturedName::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignaturedName::ReplaceChildSignature(VhdlSignature *old_node, VhdlSignature *new_node)
{
    REPLACE_IN_NODE(_signature, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeName::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_prefix, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeName::ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node)
{
    REPLACE_IN_NODE(_designator, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAttributeName::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expression, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlEntityAspect::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlInstantiatedUnit::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlExternalName::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_ext_path_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlExternalName::ReplaceChildSubtypeIndication(VhdlSubtypeIndication *old_node, VhdlSubtypeIndication *new_node)
{
    REPLACE_IN_NODE(_subtype_indication, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlProcedureSpec::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_designator, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlProcedureSpec::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_formal_parameter_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFunctionSpec::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_designator, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFunctionSpec::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_formal_parameter_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlFunctionSpec::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_return_type, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlComponentSpec::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_ARRAY(_id_list, old_node, new_node) ;
    REPLACE_IN_NODE(_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGuardedSignalSpec::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_ARRAY(_signal_list, old_node, new_node) ;
    REPLACE_IN_NODE(_type_mark, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlEntitySpec::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_ARRAY(_entity_name_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlStatement::ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node)
{
    REPLACE_IN_NODE(_label, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlReturnStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlExitStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlExitStatement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlNextStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlNextStatement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlLoopStatement::ReplaceChildIterScheme(VhdlIterScheme *old_node, VhdlIterScheme *new_node)
{
    REPLACE_IN_NODE(_iter_scheme, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlLoopStatement::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statements, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlCaseStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlCaseStatement::ReplaceChildCaseStmtAlt(VhdlCaseStatementAlternative *old_node, VhdlCaseStatementAlternative *new_node)
{
    REPLACE_IN_ARRAY(_alternatives, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIfStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_if_cond, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIfStatement::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_if_statements, old_node, new_node) ;
    REPLACE_IN_ARRAY(_else_statements, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIfStatement::ReplaceChildElsif(VhdlElsif *old_node, VhdlElsif *new_node)
{
    REPLACE_IN_ARRAY(_elsif_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlVariableAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    REPLACE_IN_NODE(_value, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignalAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    REPLACE_IN_ARRAY(_waveform, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSignalAssignmentStatement::ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node)
{
    REPLACE_IN_NODE(_delay_mechanism, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlForceAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlReleaseAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlWaitStatement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_ARRAY(_sensitivity_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlWaitStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition_clause, old_node, new_node) ;
    REPLACE_IN_NODE(_timeout_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlReportStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_report, old_node, new_node) ;
    REPLACE_IN_NODE(_severity, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlAssertionStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_condition, old_node, new_node) ;
    REPLACE_IN_NODE(_report, old_node, new_node) ;
    REPLACE_IN_NODE(_severity, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlProcessStatement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_ARRAY(_sensitivity_list, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlProcessStatement::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlProcessStatement::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statement_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_guard, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockStatement::ReplaceChildBlockGenerics(VhdlBlockGenerics *old_node, VhdlBlockGenerics *new_node)
{
    REPLACE_IN_NODE(_generics, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockStatement::ReplaceChildBlockPorts(VhdlBlockPorts *old_node, VhdlBlockPorts *new_node)
{
    REPLACE_IN_NODE(_ports, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockStatement::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlBlockStatement::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statements, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGenerateStatement::ReplaceChildIterScheme(VhdlIterScheme *old_node, VhdlIterScheme *new_node)
{
    REPLACE_IN_NODE(_scheme, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGenerateStatement::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlGenerateStatement::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statement_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlComponentInstantiationStatement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_instantiated_unit, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlComponentInstantiationStatement::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_generic_map_aspect, old_node, new_node) ;
    REPLACE_IN_ARRAY(_port_map_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlProcedureCallStatement::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_call, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalSignalAssignment::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalSignalAssignment::ReplaceChildOptions(VhdlOptions *old_node, VhdlOptions *new_node)
{
    REPLACE_IN_NODE(_options, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalSignalAssignment::ReplaceChildConditionalWaveform(VhdlConditionalWaveform *old_node, VhdlConditionalWaveform *new_node)
{
    REPLACE_IN_ARRAY(_conditional_waveforms, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalSignalAssignment::ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node)
{
    REPLACE_IN_NODE(_delay_mechanism, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalForceAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalForceAssignmentStatement::ReplaceChildConditionalExpression(VhdlConditionalExpression *old_node, VhdlConditionalExpression *new_node)
{
    REPLACE_IN_ARRAY(_conditional_expressions, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalVariableAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConditionalVariableAssignmentStatement::ReplaceChildConditionalExpression(VhdlConditionalExpression *old_node, VhdlConditionalExpression *new_node)
{
    REPLACE_IN_ARRAY(_conditional_expressions, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedSignalAssignment::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedSignalAssignment::ReplaceChildOptions(VhdlOptions *old_node, VhdlOptions *new_node)
{
    REPLACE_IN_NODE(_options, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedSignalAssignment::ReplaceChildSelectedWaveform(VhdlSelectedWaveform *old_node, VhdlSelectedWaveform *new_node)
{
    REPLACE_IN_ARRAY(_selected_waveforms, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlIfElsifGenerateStatement::ReplaceChildStmt(VhdlStatement *old_stmt, VhdlStatement *new_stmt)
{
    REPLACE_IN_ARRAY(_elsif_list, old_stmt, new_stmt) ;
    REPLACE_IN_NODE(_else_stmt, old_stmt, new_stmt) ;
    return 0 ;
}
unsigned
VhdlCaseGenerateStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlCaseGenerateStatement::ReplaceChildStmt(VhdlStatement *old_stmt, VhdlStatement *new_stmt)
{
    REPLACE_IN_ARRAY(_alternatives, old_stmt, new_stmt) ;
    return 0 ;
}
unsigned
VhdlSelectedSignalAssignment::ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node)
{
    REPLACE_IN_NODE(_delay_mechanism, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedVariableAssignmentStatement::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedVariableAssignmentStatement::ReplaceChildSelectedExpression(VhdlSelectedExpression *old_node, VhdlSelectedExpression *new_node)
{
    REPLACE_IN_ARRAY(_selected_exprs, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedForceAssignment::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    REPLACE_IN_NODE(_target, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedForceAssignment::ReplaceChildSelectedExpression(VhdlSelectedExpression *old_node, VhdlSelectedExpression *new_node)
{
    REPLACE_IN_ARRAY(_selected_exprs, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlEntityDecl::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_generic_clause, old_node, new_node) ;
    REPLACE_IN_ARRAY(_port_clause, old_node, new_node) ;
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    REPLACE_IN_ARRAY(_context_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlEntityDecl::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statement_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConfigurationDecl::ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node)
{
    REPLACE_IN_NODE(_entity_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConfigurationDecl::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    REPLACE_IN_ARRAY(_context_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlConfigurationDecl::ReplaceChildBlockConfig(VhdlBlockConfiguration *old_node, VhdlBlockConfiguration *new_node)
{
    REPLACE_IN_NODE(_block_configuration, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPackageDecl::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    REPLACE_IN_ARRAY(_context_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPackageInstantiationDecl::ReplaceChildName(VhdlName *old_node, VhdlName *new_node)
{
    REPLACE_IN_NODE(_uninst_package_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPackageInstantiationDecl::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_generic_map_aspect, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlArchitectureBody::ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node)
{
    REPLACE_IN_NODE(_entity_name, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlArchitectureBody::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    REPLACE_IN_ARRAY(_context_clause, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlArchitectureBody::ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node)
{
    REPLACE_IN_ARRAY(_statement_part, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlPackageBody::ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node)
{
    REPLACE_IN_ARRAY(_decl_part, old_node, new_node) ;
    REPLACE_IN_ARRAY(_context_clause, old_node, new_node) ;
    return 0 ;
}

unsigned
VhdlBlockConfiguration::InsertBeforeUseClause(const VhdlUseClause *target_node, VhdlUseClause *new_node)
{
    unsigned i ;
    VhdlUseClause *use_clause ;
    FOREACH_ARRAY_ITEM(_use_clause_list, i, use_clause) {
        if (use_clause == target_node) {
            _use_clause_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockConfiguration::InsertBeforeConfigItem(const VhdlConfigurationItem *target_node, VhdlConfigurationItem *new_node)
{
    unsigned i ;
    VhdlConfigurationItem *item ;
    FOREACH_ARRAY_ITEM(_configuration_item_list, i, item) {
        if (item == target_node) {
            _configuration_item_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockConfiguration::InsertAfterUseClause(const VhdlUseClause *target_node, VhdlUseClause *new_node)
{
    unsigned i ;
    VhdlUseClause *use_clause ;
    FOREACH_ARRAY_ITEM(_use_clause_list, i, use_clause) {
        if (use_clause == target_node) {
            _use_clause_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockConfiguration::InsertAfterConfigItem(const VhdlConfigurationItem *target_node, VhdlConfigurationItem *new_node)
{
    unsigned i ;
    VhdlConfigurationItem *item ;
    FOREACH_ARRAY_ITEM(_configuration_item_list, i, item) {
        if (item == target_node) {
            _configuration_item_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlArrayTypeDef::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, range) {
        if (range == target_node) {
            _index_constraint->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlArrayTypeDef::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, range) {
        if (range == target_node) {
            _index_constraint->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlRecordTypeDef::InsertBeforeElementDecl(const VhdlElementDecl *target_node, VhdlElementDecl *new_node)
{
    unsigned i ;
    VhdlElementDecl *decl ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, decl) {
        if (decl == target_node) {
            _element_decl_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlRecordTypeDef::InsertAfterElementDecl(const VhdlElementDecl *target_node, VhdlElementDecl *new_node)
{
    unsigned i ;
    VhdlElementDecl *decl ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, decl) {
        if (decl == target_node) {
            _element_decl_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEnumerationTypeDef::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list, i, id) {
        if (id == target_node) {
            _enumeration_literal_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEnumerationTypeDef::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list, i, id) {
        if (id == target_node) {
            _enumeration_literal_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlPhysicalTypeDef::InsertBeforePhysicalUnitDecl(const VhdlPhysicalUnitDecl *target_node, VhdlPhysicalUnitDecl *new_node)
{
    unsigned i ;
    VhdlPhysicalUnitDecl *decl ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list, i, decl) {
        if (decl == target_node) {
            _physical_unit_decl_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlPhysicalTypeDef::InsertAfterPhysicalUnitDecl(const VhdlPhysicalUnitDecl *target_node, VhdlPhysicalUnitDecl *new_node)
{
    unsigned i ;
    VhdlPhysicalUnitDecl *decl ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list, i, decl) {
        if (decl == target_node) {
            _physical_unit_decl_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlElementDecl::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlElementDecl::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlUseClause::InsertBeforeSelectedName(const VhdlSelectedName *target_node, VhdlSelectedName *new_node)
{
    unsigned i ;
    VhdlSelectedName *name ;
    FOREACH_ARRAY_ITEM(_selected_name_list, i, name) {
        if (name == target_node) {
            _selected_name_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlUseClause::InsertAfterSelectedName(const VhdlSelectedName *target_node, VhdlSelectedName *new_node)
{
    unsigned i ;
    VhdlSelectedName *name ;
    FOREACH_ARRAY_ITEM(_selected_name_list, i, name) {
        if (name == target_node) {
            _selected_name_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlLibraryClause::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlLibraryClause::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlInterfaceDecl::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlInterfaceDecl::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSubprogramBody::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSubprogramBody::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSubprogramBody::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSubprogramBody::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertAfter(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConstantDecl::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConstantDecl::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSignalDecl::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSignalDecl::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlVariableDecl::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlVariableDecl::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlFileDecl::InsertBeforeId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ; // carbon
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlFileDecl::InsertAfterId(const VhdlIdDef *target_node, VhdlIdDef *new_node)
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlComponentDecl::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl == target_node) {
            _generic_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl == target_node) {
            _port_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlComponentDecl::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl == target_node) {
            _generic_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl == target_node) {
            _port_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGroupTemplateDecl::InsertBeforeEntityClassEntry(const VhdlEntityClassEntry *target_node, VhdlEntityClassEntry *new_node)
{
    unsigned i ;
    VhdlEntityClassEntry *entry ;
    FOREACH_ARRAY_ITEM(_entity_class_entry_list, i, entry) {
        if (entry == target_node) {
            _entity_class_entry_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGroupTemplateDecl::InsertAfterEntityClassEntry(const VhdlEntityClassEntry *target_node, VhdlEntityClassEntry *new_node)
{
    unsigned i ;
    VhdlEntityClassEntry *entry ;
    FOREACH_ARRAY_ITEM(_entity_class_entry_list, i, entry) {
        if (entry == target_node) {
            _entity_class_entry_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlAggregate::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, range) {
        if (range == target_node) {
            _element_assoc_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlAggregate::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, range) {
        if (range == target_node) {
            _element_assoc_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlElementAssoc::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_choices, i, range) {
        if (range == target_node) {
            _choices->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlElementAssoc::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_choices, i, range) {
        if (range == target_node) {
            _choices->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSignature::InsertBeforeName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *name ;
    FOREACH_ARRAY_ITEM(_type_mark_list, i, name) {
        if (name == target_node) {
            _type_mark_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSignature::InsertAfterName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *name ;
    FOREACH_ARRAY_ITEM(_type_mark_list, i, name) {
        if (name == target_node) {
            _type_mark_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBindingIndication::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _generic_map_aspect->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _port_map_aspect->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBindingIndication::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _generic_map_aspect->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _port_map_aspect->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalWaveform::InsertBeforeExpr(const VhdlExpression *target_node, VhdlExpression *new_node)
{
    unsigned i ;
    VhdlExpression *wave_ele ;
    FOREACH_ARRAY_ITEM(_waveform, i, wave_ele) {
        if (wave_ele == target_node) {
            _waveform->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalWaveform::InsertAfterExpr(const VhdlExpression *target_node, VhdlExpression *new_node)
{
    unsigned i ;
    VhdlExpression *wave_ele ;
    FOREACH_ARRAY_ITEM(_waveform, i, wave_ele) {
        if (wave_ele == target_node) {
            _waveform->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedWaveform::InsertBeforeExpr(const VhdlExpression *target_node, VhdlExpression *new_node)
{
    unsigned i ;
    VhdlExpression *wave_ele ;
    FOREACH_ARRAY_ITEM(_waveform, i, wave_ele) {
        if (wave_ele == target_node) {
            _waveform->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedWaveform::InsertAfterExpr(const VhdlExpression *target_node, VhdlExpression *new_node)
{
    unsigned i ;
    VhdlExpression *wave_ele ;
    FOREACH_ARRAY_ITEM(_waveform, i, wave_ele) {
        if (wave_ele == target_node) {
            _waveform->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedWaveform::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice == target_node) {
            _when_choices->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedWaveform::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice == target_node) {
            _when_choices->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedExpression::ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node)
{
    REPLACE_IN_NODE(_expr, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedExpression::ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node)
{
    REPLACE_IN_ARRAY(_when_choices, old_node, new_node) ;
    return 0 ;
}
unsigned
VhdlSelectedExpression::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice == target_node) {
            _when_choices->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedExpression::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_when_choices, i, choice) {
        if (choice == target_node) {
            _when_choices->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockGenerics::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl == target_node) {
            _generic_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockGenerics::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl == target_node) {
            _generic_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockGenerics::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *gen_aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, gen_aspect) {
        if (gen_aspect == target_node) {
            _generic_map_aspect->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockGenerics::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *gen_aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, gen_aspect) {
        if (gen_aspect == target_node) {
            _generic_map_aspect->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockPorts::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl == target_node) {
            _port_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockPorts::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl == target_node) {
            _port_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockPorts::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *port_aspect ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, port_aspect) {
        if (port_aspect == target_node) {
            _port_map_aspect->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockPorts::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *port_aspect ;
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, port_aspect) {
        if (port_aspect == target_node) {
            _port_map_aspect->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlCaseStatementAlternative::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice == target_node) {
            _choices->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlCaseStatementAlternative::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice == target_node) {
            _choices->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlCaseStatementAlternative::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlCaseStatementAlternative::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlElsif::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlElsif::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlIndexedName::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
        if (assoc == target_node) {
            _assoc_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlIndexedName::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
        if (assoc == target_node) {
            _assoc_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcedureSpec::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *formal_param ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, formal_param) {
        if (formal_param == target_node) {
            _formal_parameter_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcedureSpec::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *formal_param ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, formal_param) {
        if (formal_param == target_node) {
            _formal_parameter_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlFunctionSpec::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *formal_param ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, formal_param) {
        if (formal_param == target_node) {
            _formal_parameter_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlFunctionSpec::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *formal_param ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, formal_param) {
        if (formal_param == target_node) {
            _formal_parameter_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlComponentSpec::InsertBeforeName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlComponentSpec::InsertAfterName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id == target_node) {
            _id_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGuardedSignalSpec::InsertBeforeName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *signal ;
    FOREACH_ARRAY_ITEM(_signal_list, i, signal) {
        if (signal == target_node) {
            _signal_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGuardedSignalSpec::InsertAfterName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *signal ;
    FOREACH_ARRAY_ITEM(_signal_list, i, signal) {
        if (signal == target_node) {
            _signal_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEntitySpec::InsertBeforeName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *entity_name ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, entity_name) {
        if (entity_name == target_node) {
            _entity_name_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEntitySpec::InsertAfterName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *entity_name ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, entity_name) {
        if (entity_name == target_node) {
            _entity_name_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlLoopStatement::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlLoopStatement::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlCaseStatement::InsertBeforeCaseStmtAlt(const VhdlCaseStatementAlternative *target_node, VhdlCaseStatementAlternative *new_node)
{
    unsigned i ;
    VhdlCaseStatementAlternative *ele ;
    FOREACH_ARRAY_ITEM(_alternatives, i, ele) {
        if (ele == target_node) {
            _alternatives->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlCaseStatement::InsertAfterCaseStmtAlt(const VhdlCaseStatementAlternative *target_node, VhdlCaseStatementAlternative *new_node)
{
    unsigned i ;
    VhdlCaseStatementAlternative *ele ;
    FOREACH_ARRAY_ITEM(_alternatives, i, ele) {
        if (ele == target_node) {
            _alternatives->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlIfStatement::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_if_statements, i, stmt) {
        if (stmt == target_node) {
            _if_statements->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_else_statements, i, stmt) {
        if (stmt == target_node) {
            _else_statements->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlIfStatement::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_if_statements, i, stmt) {
        if (stmt == target_node) {
            _if_statements->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_else_statements, i, stmt) {
        if (stmt == target_node) {
            _else_statements->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlIfStatement::InsertBeforeElsif(const VhdlElsif *target_node, VhdlElsif *new_node)
{
    unsigned i ;
    VhdlElsif *elseif ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elseif) {
        if (elseif == target_node) {
            _elsif_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlIfStatement::InsertAfterElsif(const VhdlElsif *target_node, VhdlElsif *new_node)
{
    unsigned i ;
    VhdlElsif *elseif ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elseif) {
        if (elseif == target_node) {
            _elsif_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSignalAssignmentStatement::InsertBeforeExpr(const VhdlExpression *target_node, VhdlExpression *new_node)
{
    unsigned i ;
    VhdlExpression *expr ;
    FOREACH_ARRAY_ITEM(_waveform, i, expr) {
        if (expr == target_node) {
            _waveform->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSignalAssignmentStatement::InsertAfterExpr(const VhdlExpression *target_node, VhdlExpression *new_node)
{
    unsigned i ;
    VhdlExpression *expr ;
    FOREACH_ARRAY_ITEM(_waveform, i, expr) {
        if (expr == target_node) {
            _waveform->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlWaitStatement::InsertBeforeName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *sen_ele ;
    FOREACH_ARRAY_ITEM(_sensitivity_clause, i, sen_ele) {
        if (sen_ele == target_node) {
            _sensitivity_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlWaitStatement::InsertAfterName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *sen_ele ;
    FOREACH_ARRAY_ITEM(_sensitivity_clause, i, sen_ele) {
        if (sen_ele == target_node) {
            _sensitivity_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcessStatement::InsertBeforeName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *sen_ele ;
    FOREACH_ARRAY_ITEM(_sensitivity_list, i, sen_ele) {
        if (sen_ele == target_node) {
            _sensitivity_list->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcessStatement::InsertAfterName(const VhdlName *target_node, VhdlName *new_node)
{
    unsigned i ;
    VhdlName *sen_ele ;
    FOREACH_ARRAY_ITEM(_sensitivity_list, i, sen_ele) {
        if (sen_ele == target_node) {
            _sensitivity_list->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcessStatement::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcessStatement::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcessStatement::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlProcessStatement::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockStatement::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockStatement::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockStatement::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlBlockStatement::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (stmt == target_node) {
            _statements->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGenerateStatement::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGenerateStatement::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGenerateStatement::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlGenerateStatement::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlComponentInstantiationStatement::InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _generic_map_aspect->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _port_map_aspect->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlComponentInstantiationStatement::InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node)
{
    unsigned i ;
    VhdlDiscreteRange *aspect ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _generic_map_aspect->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect, i, aspect) {
        if (aspect == target_node) {
            _port_map_aspect->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalSignalAssignment::InsertBeforeConditionalWaveform(const VhdlConditionalWaveform *target_node, VhdlConditionalWaveform *new_node)
{
    unsigned i ;
    VhdlConditionalWaveform *waveform ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms, i, waveform) {
        if (waveform == target_node) {
            _conditional_waveforms->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalSignalAssignment::InsertAfterConditionalWaveform(const VhdlConditionalWaveform *target_node, VhdlConditionalWaveform *new_node)
{
    unsigned i ;
    VhdlConditionalWaveform *waveform ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms, i, waveform) {
        if (waveform == target_node) {
            _conditional_waveforms->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalForceAssignmentStatement::InsertBeforeConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node)
{
    unsigned i ;
    VhdlConditionalExpression *expr ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, expr) {
        if (expr == target_node) {
            _conditional_expressions->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalForceAssignmentStatement::InsertAfterConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node)
{
    unsigned i ;
    VhdlConditionalExpression *expr ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, expr) {
        if (expr == target_node) {
            _conditional_expressions->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedSignalAssignment::InsertBeforeSelectedWaveform(const VhdlSelectedWaveform *target_node, VhdlSelectedWaveform *new_node)
{
    unsigned i ;
    VhdlSelectedWaveform *waveform ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, waveform) {
        if (waveform == target_node) {
            _selected_waveforms->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedSignalAssignment::InsertAfterSelectedWaveform(const VhdlSelectedWaveform *target_node, VhdlSelectedWaveform *new_node)
{
    unsigned i ;
    VhdlSelectedWaveform *waveform ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, waveform) {
        if (waveform == target_node) {
            _selected_waveforms->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedVariableAssignmentStatement::InsertBeforeSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node)
{
    unsigned i ;
    VhdlSelectedExpression *expr ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, expr) {
        if (expr == target_node) {
            _selected_exprs->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedVariableAssignmentStatement::InsertAfterSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node)
{
    unsigned i ;
    VhdlSelectedExpression *expr ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, expr) {
        if (expr == target_node) {
            _selected_exprs->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalVariableAssignmentStatement::InsertBeforeConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node)
{
    unsigned i ;
    VhdlConditionalExpression *expr ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, expr) {
        if (expr == target_node) {
            _conditional_expressions->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConditionalVariableAssignmentStatement::InsertAfterConditionalExpression(const VhdlConditionalExpression *target_node, VhdlConditionalExpression *new_node)
{
    unsigned i ;
    VhdlConditionalExpression *expr ;
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, expr) {
        if (expr == target_node) {
            _conditional_expressions->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedForceAssignment::InsertBeforeSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node)
{
    unsigned i ;
    VhdlSelectedExpression *expr ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, expr) {
        if (expr == target_node) {
            _selected_exprs->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlSelectedForceAssignment::InsertAfterSelectedExpression(const VhdlSelectedExpression *target_node, VhdlSelectedExpression *new_node)
{
    unsigned i ;
    VhdlSelectedExpression *expr ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, expr) {
        if (expr == target_node) {
            _selected_exprs->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEntityDecl::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl == target_node) {
            _generic_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl == target_node) {
            _port_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEntityDecl::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl == target_node) {
            _generic_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl == target_node) {
            _port_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEntityDecl::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlEntityDecl::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConfigurationDecl::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlConfigurationDecl::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlPackageDecl::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlPackageDecl::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlArchitectureBody::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlArchitectureBody::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlArchitectureBody::InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlArchitectureBody::InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node)
{
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt == target_node) {
            _statement_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlPackageBody::InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertBefore(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}
unsigned
VhdlPackageBody::InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node)
{
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl == target_node) {
            _decl_part->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl == target_node) {
            _context_clause->InsertAfter(i, new_node) ;
            return 1 ;
        }
    }
    return 0 ;
}

////////////////////////////////////////////////////////////////////////////////
//                 Extended parse tree manipulation routines                  //
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//                  Parse tree  'Find' routines                               //
////////////////////////////////////////////////////////////////////////////////
VhdlIdDef *
VhdlDesignUnit::FindDeclared(const char *name) const
{
    return (_local_scope) ? _local_scope->FindSingleObjectLocal(name) : 0 ; // Find 'name' in local scope
}
VhdlIdDef *
VhdlComponentDecl::FindDeclared(const char *name) const
{
    return (_local_scope) ? _local_scope->FindSingleObjectLocal(name) : 0 ; // Find 'name' in local scope
}

////////////////////////////////////////////////////////////////////////////////
//            Primary Unit Parse-tree  Port Creation/Deletion routines        //
////////////////////////////////////////////////////////////////////////////////

void
VhdlEntityId::AddPort(VhdlIdDef *id)
{
    if (!id) return ;
    if (!_ports) _ports = new Array(1) ; // Create Array of port list if not exists
    _ports->InsertLast(id) ; // Add argument specific port id
}

VhdlIdDef *
VhdlEntityDecl::AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication)
{
    if (!port_name || !subtype_indication || !_local_scope) return 0 ; // port addition not possible

    // Create VhdlInterfaceId for 'port_name'
    VhdlInterfaceId *new_port = new VhdlInterfaceId(Strings::save(port_name)) ;

    // Declare created port to _local_scope, issue error if identifier 'port_name'
    // already exists.
    if (!_local_scope->Declare(new_port)) { // Identifier already declared
        delete new_port ; // Delete created interface id
        return 0 ;        // Return 0 to indicate failure
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to entity scope

    // Create array to hold created port id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_port) ;

    // Create declaration
    VhdlInterfaceDecl *port_decl = new VhdlInterfaceDecl(0, id_list, port_direction, subtype_indication, 0, 0) ;

    // Mark created id as port (VhdlInterfaceId also created for generic)
    new_port->SetObjectKind(VHDL_port) ;

    // Insert port declaration into port clause
    if (!_port_clause) _port_clause = new Array(1) ; // No port exists, create port clause
    _port_clause->InsertLast(port_decl) ;

    // Add created port identifier to the entity id as back pointer
    if (Id()) Id()->AddPort(new_port) ;

    _present_scope = save_scope ; // Pop present scope

    return new_port ; // Return created port
}

VhdlIdDef *
VhdlConfigurationDecl::AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication)
{
    // Get the entity it configures
    VhdlIdDef *entity_id = Id() ? Id()->GetEntity() : 0 ;
    VhdlPrimaryUnit *entity_decl = entity_id ? entity_id->GetPrimaryUnit() : 0 ;

    // Call 'AddPort' of entity to add 'port_name' there
    return (entity_decl) ? entity_decl->AddPort(port_name, port_direction, subtype_indication) : 0 ;
}

unsigned
VhdlEntityDecl::RemovePort(const char *port_name)
{
    // Find 'port_name' specific identifier in local scope
    VhdlIdDef *port_id = FindDeclared(port_name) ;

    if (!port_id) return 0 ; // No port, cannot remove

    // Find the declaration of 'port_id' iterating over port clause
    unsigned i, j ;
    VhdlInterfaceDecl *port_decl ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_port_clause, i, port_decl) {
        Array *ids = port_decl ? port_decl->GetIds() : 0 ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (!id) continue ;
            if (id != port_id) continue ; // Not matched
            // Found, need to remove it from id_list and port list of entity id

            // Get ordered list of port ids (back pointer) from entity id
            Array *port_ids = Id() ? Id()->GetPorts() : 0 ;

            // Find the port id to be removed in the ordered list of port ids
            unsigned k ;
            FOREACH_ARRAY_ITEM(port_ids, k, id) {
                if (id == port_id) port_ids->Remove(k) ; // Remove the matched id from port list
            }

            if (_local_scope) _local_scope->Undeclare(port_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire interface decl
                _port_clause->Remove(i) ;    // Remove decl from _port_clause
                delete port_decl ;           // Delete declaration
                if (!_port_clause->Size()) { // _port_clause contains no element
                    delete _port_clause ;    // Delete _port_clause
                    _port_clause = 0 ;       // Set null to it
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ; // Remove id from port_decl->GetIds()
                delete port_id ; // Delete port id
            }
            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}

unsigned
VhdlConfigurationDecl::RemovePort(const char *port_name)
{
    // Get the entity it configures
    VhdlIdDef *entity_id = Id() ? Id()->GetEntity() : 0 ;
    VhdlPrimaryUnit *entity_decl = entity_id ? entity_id->GetPrimaryUnit() : 0 ;

    // Remove 'port_name' specific port from 'entity_decl'
    return (entity_decl) ? entity_decl->RemovePort(port_name) : 0 ;
}

////////////////////////////////////////////////////////////////////////////////
//      Component declaration Parse-tree Port Creation/Deletion routines      //
////////////////////////////////////////////////////////////////////////////////
void
VhdlComponentDecl::SetPortClause(Array *port_clause)
{
    unsigned i ;
    VhdlInterfaceDecl *port_decl ;
    FOREACH_ARRAY_ITEM(_port_clause, i, port_decl) delete port_decl ; // Delete each port decl

    delete _port_clause ; // Delete _port_clause

    _port_clause = port_clause ; // Set new port_clause
}

void
VhdlComponentId::AddPort(VhdlIdDef *id)
{
    if (!id) return ; // Nothing to add
    if (!_ports) _ports = new Array(1) ; // Create array of port list if not exists
    _ports->InsertLast(id) ; // Add argument specific port id
}

VhdlIdDef *
VhdlComponentDecl::AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication)
{
    if (!port_name || !subtype_indication || !_local_scope) return 0 ; // port addition not possible

    // Create VhdlInterfaceId for 'port_name'
    VhdlInterfaceId *new_port = new VhdlInterfaceId(Strings::save(port_name)) ;

    // Declare created port to _local_scope, issue error if identifier 'port_name' already exists.
    if (!_local_scope->Declare(new_port)) { // Identifier already declared
        delete new_port ; // Delete created interface id
        return 0 ;        // Return 0 to indicate failure
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to entity scope

    // Create array to hold created port id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_port) ;

    // Create declaration
    VhdlInterfaceDecl *port_decl = new VhdlInterfaceDecl(0, id_list, port_direction, subtype_indication, 0, 0) ;

    // Mark created id as port (VhdlInterfaceId also created for generic)
    new_port->SetObjectKind(VHDL_port) ;
    // Insert port declaration into port clause
    if (!_port_clause) _port_clause = new Array(1) ; // No port exists, create port clause
    _port_clause->InsertLast(port_decl) ;

    // Add created port identifier to the component id as back pointer
    if (_id) _id->AddPort(new_port) ;

    _present_scope = save_scope ; // Pop present scope

    return new_port ; // Return created port
}

unsigned
VhdlComponentDecl::RemovePort(const char *port_name)
{
    // Find 'port_name' specific identifier in local scope
    VhdlIdDef *port_id = FindDeclared(port_name) ;

    if (!port_id) return 0 ; // No port, cannot remove

    // Find the declaration of 'port_id' iterating over port clause
    unsigned i, j ;
    VhdlInterfaceDecl *port_decl ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_port_clause, i, port_decl) {
        Array *ids = port_decl ? port_decl->GetIds() : 0 ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != port_id) continue ; // Not matched
            // Found, need to remove it from id_list and port list of component id
            // Get ordered list of port ids (back pointer) from component id
            Array *port_ids = _id ? _id->GetPorts() : 0 ;

            // Find the port id to be removed in the ordered list of port ids
            unsigned k ;
            FOREACH_ARRAY_ITEM(port_ids, k, id) {
                if (id == port_id) port_ids->Remove(k) ; // Remove the matched id from port list
            }

            if (_local_scope) _local_scope->Undeclare(port_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire interface decl
                _port_clause->Remove(i) ;    // Remove decl from _port_clause
                delete port_decl ;           // Delete declaration
                if (!_port_clause->Size()) { // _port_clause contains no element
                    delete _port_clause ;    // Delete
                    _port_clause = 0 ;
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ; // Remove id from port_decl->GetIds()
                delete port_id ; // delete port id
            }

            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}

////////////////////////////////////////////////////////////////////////////////
//      Block statement Parse-tree Port Creation/Deletion routines            //
////////////////////////////////////////////////////////////////////////////////
void
VhdlBlockId::AddPort(VhdlIdDef *id)
{
    if (!id) return ; // Nothing to add
    if (!_ports) _ports = new Array(1) ; // Create array of port list if not exists
    _ports->InsertLast(id) ; // Add argument specific port id
}
VhdlIdDef *
VhdlBlockPorts::AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication)
{
    if (!port_name || !subtype_indication) return 0 ; // port addition not possible

    // Create VhdlInterfaceId for 'port_name'
    VhdlInterfaceId *new_port = new VhdlInterfaceId(Strings::save(port_name)) ;

    VhdlScope *block_scope = (_block_label) ? _block_label->LocalScope() : 0 ;

    // Declare created port to block_scope, issue error if identifier 'port_name' already exists.
    if (block_scope && !block_scope->Declare(new_port)) { // Identifier already declared
        delete new_port ; // Delete created interface id
        return 0 ;        // Return 0 to indicate failure
    }
    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = block_scope ; // switch to block scope

    // Create array to hold port id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_port) ;

    // Create declaration
    VhdlInterfaceDecl *port_decl = new VhdlInterfaceDecl(0, id_list, port_direction, subtype_indication, 0, 0) ;

    // Mark created id as port (VhdlInterfaceId also created for generic)
    new_port->SetObjectKind(VHDL_port) ;

    if (!_port_clause) _port_clause = new Array() ;
    _port_clause->InsertLast(port_decl) ;

    // Add created port identifier to the block id as back pointer
    if (_block_label) _block_label->AddPort(new_port) ;

    _present_scope = save_scope ; // Pop present scope
    return new_port ; // Addition completed
}

VhdlIdDef *
VhdlBlockStatement::AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication)
{
    if (!port_name || !subtype_indication || !_local_scope) return 0 ; // port addition not possible

    // Port clause is in block ports
    if (!_ports) {
        _ports = new VhdlBlockPorts(0, 0) ; // Create block ports if not present
        _ports->SetLinefile(Linefile()) ; // Set linefile to block ports
    }
    return _ports->AddPort(port_name, port_direction, subtype_indication) ;
}
unsigned
VhdlBlockPorts::RemovePort(const char *port_name)
{
    if (!port_name) return 0 ; // Can't remove : no id

    VhdlScope *block_scope = (_block_label) ? _block_label->LocalScope() : 0 ; // Get block scope

    // Find 'port_name' specific identifier in local scope
    VhdlIdDef *port_id = (block_scope) ? block_scope->FindSingleObjectLocal(port_name) : 0 ;

    if (!port_id) return 0 ; // 'port_name' not declared in block

    // Find the declaration of 'port_id' iterating over port clause
    unsigned i, j ;
    VhdlInterfaceDecl *port_decl ;
    VhdlIdDef *id, *id_def ;
    FOREACH_ARRAY_ITEM(_port_clause, i, port_decl) {
        Array *ids = port_decl ? port_decl->GetIds() : 0 ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != port_id) continue ; // Not matched

            // Need to remove port_id from id_list of block id
            // Get ordered list of port ids (back pointer) from block id
            Array *port_ids = (_block_label) ? _block_label->GetPorts() : 0 ;

            // Find the port id to be removed in the ordered list of port ids
            unsigned k ;
            FOREACH_ARRAY_ITEM(port_ids, k, id_def) {
                if (id_def == port_id) port_ids->Remove(k) ; // Remove the matched id from port list
            }

            if (block_scope) block_scope->Undeclare(port_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire interface decl
                _port_clause->Remove(i) ;    // Remove decl from _port_clause
                delete port_decl ;           // Delete declaration
                if (!_port_clause->Size()) { // _port_clause contains no element
                    delete _port_clause ;    // Delete
                    _port_clause = 0 ;
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ; // Remove id from port_decl->GetIds()
                delete port_id ; // delete port id
            }

            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}
unsigned
VhdlBlockStatement::RemovePort(const char *port_name)
{
    if (_ports) return _ports->RemovePort(port_name) ; // Remove port from port clause of block ports

    return 0 ; // Fails to remove
}
////////////////////////////////////////////////////////////////////////////////
//            Primary Unit Parse-tree  Generic Creation/Deletion routines     //
////////////////////////////////////////////////////////////////////////////////
void
VhdlEntityDecl::SetGenericClause(Array *generic_clause)
{
    unsigned i ;
    VhdlInterfaceDecl *generic_decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, generic_decl) delete generic_decl ; // Delete each generic decl

    delete _generic_clause ; // Delete _generic_clause

    _generic_clause = generic_clause ; // Set new generic_clause
}

void
VhdlEntityId::AddGeneric(VhdlIdDef *generic_id)
{
    if (!generic_id) return ; // Nothing to add
    if (!_generics) _generics = new Array(1) ; // Create array of generics if not exists
    _generics->InsertLast(generic_id) ;
}

VhdlIdDef *
VhdlEntityDecl::AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign)
{
    if (!generic_name || !subtype_indication || !_local_scope) return 0 ; // generic addition not possible

    // Create VhdlInterfaceId for 'generic_name'
    VhdlInterfaceId *new_generic = new VhdlInterfaceId(Strings::save(generic_name)) ;

    // Declare created generic to _local_scope, issue error if identifier 'generic_name'
    // already exists.
    if (!_local_scope->Declare(new_generic)) { // Identifier already declared
        delete new_generic ; // Delete created interface id
        return 0 ;           // Return 0 to indicate failure
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to entity scope

    // Create array to hold created generic id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_generic) ;

    // Create declaration
    VhdlInterfaceDecl *generic_decl = new VhdlInterfaceDecl(0, id_list, 0, subtype_indication, 0, initial_assign) ;

    // Mark created id as generic (VhdlInterfaceId also created for port)
    new_generic->SetObjectKind(VHDL_generic) ;

    // Insert generic declaration into generic clause
    if (!_generic_clause) _generic_clause = new Array(1) ; // No generic exists, create generic clause
    _generic_clause->InsertLast(generic_decl) ;

    // Add created generic identifier to the entity id as back pointer
    if (Id()) Id()->AddGeneric(new_generic) ;

    _present_scope = save_scope ; // Pop present scope

    return new_generic ; // Return created generic
}

VhdlIdDef *
VhdlConfigurationDecl::AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign)
{
    // Get the entity it configures
    VhdlIdDef *entity_id = Id() ? Id()->GetEntity() : 0 ;
    VhdlPrimaryUnit *entity_decl = entity_id ? entity_id->GetPrimaryUnit() : 0 ;

    // Call 'AddGeneric' of entity to add 'generic_name' there
    return (entity_decl) ? entity_decl->AddGeneric(generic_name, subtype_indication, initial_assign) : 0 ;
}

unsigned
VhdlEntityDecl::RemoveGeneric(const char *generic_name)
{
    // Find 'generic_name' specific identifier in local scope
    VhdlIdDef *generic_id = FindDeclared(generic_name) ;

    if (!generic_id) return 0 ; // No generic, cannot remove

    // Find the declaration of 'generic_id' iterating over generic clause
    unsigned i, j ;
    VhdlInterfaceDecl *generic_decl ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, generic_decl) {
        Array *ids = generic_decl ? generic_decl->GetIds() : 0 ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != generic_id) continue ; // Not found, continue
            // Found, need to remove it from id_list and generic list of entity id

            // Get ordered list of generic ids (back pointer) from entity id
            Array *generic_ids = Id() ? Id()->GetGenerics() : 0 ;

            // Find the generic id to be removed in the ordered list of generic ids
            unsigned k ;
            FOREACH_ARRAY_ITEM(generic_ids, k, id) {
                if (id == generic_id) generic_ids->Remove(k) ; // Remove the matched id from generic list
            }

            if (_local_scope) _local_scope->Undeclare(generic_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire interface decl
                _generic_clause->Remove(i) ;    // Remove decl from _generic_clause
                delete generic_decl ;           // Delete declaration
                if (!_generic_clause->Size()) { // Unit contains no generics
                    delete _generic_clause ;    // Delete generic array
                    _generic_clause = 0 ;
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ;    // Remove id from generic_decl->GetIds()
                delete generic_id ; // delete generic id
            }
            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}

unsigned
VhdlConfigurationDecl::RemoveGeneric(const char *generic_name)
{
    // Get the entity it configures
    VhdlIdDef *entity_id = Id() ? Id()->GetEntity() : 0 ;
    VhdlPrimaryUnit *entity_decl = entity_id ? entity_id->GetPrimaryUnit() : 0 ;

    // Remove 'generic_name' specific port from 'entity_decl'
    return (entity_decl) ? entity_decl->RemoveGeneric(generic_name) : 0 ;
}

////////////////////////////////////////////////////////////////////////////////
//      Component declaration Parse-tree Generic Creation/Deletion routines   //
////////////////////////////////////////////////////////////////////////////////

void
VhdlComponentDecl::SetGenericClause(Array *generic_clause)
{
    unsigned i ;
    VhdlInterfaceDecl *generic_decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, generic_decl) delete generic_decl ; // Delete each generic decl

    delete _generic_clause ; // Delete _generic_clause

    _generic_clause = generic_clause ; // Set new generic_clause
}

void
VhdlComponentId::AddGeneric(VhdlIdDef *generic_id)
{
    if (!generic_id) return ; // Nothing to add
    if (!_generics) _generics = new Array(1) ; // Create array of generics if not exists
    _generics->InsertLast(generic_id) ;
}

VhdlIdDef *
VhdlComponentDecl::AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign)
{
    if (!generic_name || !subtype_indication || !_local_scope) return 0 ; // generic addition not possible

    // Create VhdlInterfaceId for 'generic_name'
    VhdlInterfaceId *new_generic = new VhdlInterfaceId(Strings::save(generic_name)) ;

    // Declare created generic to _local_scope, issue error if identifier 'generic_name'
    // already exists.
    if (!_local_scope->Declare(new_generic)) { // Identifier already declared
        delete new_generic ; // Delete created interface id
        return 0 ; // Return 0 to indicate failure
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to entity scope

    // Create array to hold created generic id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_generic) ;

    // Create declaration
    VhdlInterfaceDecl *generic_decl = new VhdlInterfaceDecl(0, id_list, 0, subtype_indication, 0, initial_assign) ;

    // Mark created id as generic (VhdlInterfaceId also created for port)
    new_generic->SetObjectKind(VHDL_generic) ;

    // Insert generic declaration into generic clause
    if (!_generic_clause) _generic_clause = new Array(1) ; // No generic exists, create generic clause
    _generic_clause->InsertLast(generic_decl) ;

    // Add created generic identifier to the entity id as back pointer
    if (_id) _id->AddGeneric(new_generic) ;

    _present_scope = save_scope ; // Pop present scope

    return new_generic ; // Return created generic
}

unsigned
VhdlComponentDecl::RemoveGeneric(const char *generic_name)
{
    // Find 'generic_name' specific identifier in local scope
    VhdlIdDef *generic_id = FindDeclared(generic_name) ;

    if (!generic_id) return 0 ; // No generic, cannot remove

    // Find the declaration of 'generic_id' iterating over generic clause
    unsigned i, j ;
    VhdlInterfaceDecl *generic_decl ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, generic_decl) {
        Array *ids = generic_decl ? generic_decl->GetIds() : 0 ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != generic_id) continue ; // Not found, continue
            // Found, need to remove it from id_list and generic list of component id

            // Get ordered list of generic ids (back pointer) from component id
            Array *generic_ids = _id ? _id->GetGenerics() : 0 ;

            // Find the generic id to be removed in the ordered list of generic ids
            unsigned k ;
            FOREACH_ARRAY_ITEM(generic_ids, k, id) {
                if (id == generic_id) generic_ids->Remove(k) ; // Remove the matched id from generic list
            }

            if (_local_scope) _local_scope->Undeclare(generic_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire interface decl
                _generic_clause->Remove(i) ;    // Remove decl from _generic_clause
                delete generic_decl ;           // Delete declaration
                if (!_generic_clause->Size()) { // No generics remains
                    delete _generic_clause ;    // Delete generic clause
                    _generic_clause = 0 ;       // Set null
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ;    // Remove id from generic_decl->GetIds()
                delete generic_id ; // Delete generic id
            }
            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}
////////////////////////////////////////////////////////////////////////////////
//      Block statement Parse-tree generic Creation/Deletion routines         //
////////////////////////////////////////////////////////////////////////////////
void
VhdlBlockId::AddGeneric(VhdlIdDef *id)
{
    if (!id) return ; // Nothing to add
    if (!_generics) _generics = new Array(1) ; // Create array of generic list if not exists
    _generics->InsertLast(id) ; // Add argument specific generic id
}
VhdlIdDef *
VhdlBlockGenerics::AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign)
{
    if (!generic_name || !subtype_indication) return 0 ; // generic addition not possible

    // Create VhdlInterfaceId for 'generic_name'
    VhdlInterfaceId *new_generic = new VhdlInterfaceId(Strings::save(generic_name)) ;

    VhdlScope *block_scope = (_block_label) ? _block_label->LocalScope() : 0 ;

    // Declare created generic to block_scope, issue error if identifier 'generic_name' already exists.
    if (block_scope && !block_scope->Declare(new_generic)) { // Identifier already declared
        delete new_generic ; // Delete created interface id
        return 0 ;        // Return 0 to indicate failure
    }
    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = block_scope ; // switch to block scope

    // Create array to hold generic id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_generic) ;

    // Create declaration
    VhdlInterfaceDecl *generic_decl = new VhdlInterfaceDecl(0, id_list, 0, subtype_indication, 0, initial_assign) ;

    // Mark created id as port (VhdlInterfaceId also created for generic)
    new_generic->SetObjectKind(VHDL_generic) ;

    if (!_generic_clause) _generic_clause = new Array() ;
    _generic_clause->InsertLast(generic_decl) ;

    // Add created port identifier to the block id as back pointer
    if (_block_label) _block_label->AddGeneric(new_generic) ;

    _present_scope = save_scope ; // Pop present scope
    return new_generic ;
}
VhdlIdDef *
VhdlBlockStatement::AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign)
{
    if (!generic_name || !subtype_indication || !_local_scope) return 0 ; // generic addition not possible

    // generic clause is in block generics
    if (!_generics) {
        _generics = new VhdlBlockGenerics(0, 0) ; // Create block generics if not present
        _generics->SetLinefile(Linefile()) ; // Set linefile to block generics
    }

    return _generics->AddGeneric(generic_name, subtype_indication, initial_assign) ;
}
unsigned
VhdlBlockGenerics::RemoveGeneric(const char *generic_name)
{
    if (!generic_name) return 0 ; // Can't remove : no id

    VhdlScope *block_scope = (_block_label) ? _block_label->LocalScope() : 0 ; // Get block scope

    // Find 'generic_name' specific identifier in local scope
    VhdlIdDef *generic_id = (block_scope) ? block_scope->FindSingleObjectLocal(generic_name) : 0 ;

    if (!generic_id) return 0 ; // 'generic_name' not declared in block

    // Find the declaration of 'generic_id' iterating over generic clause
    unsigned i, j ;
    VhdlInterfaceDecl *generic_decl ;
    VhdlIdDef *id, *id_def ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, generic_decl) {
        Array *ids = generic_decl ? generic_decl->GetIds() : 0 ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != generic_id) continue ; // Not matched

            // Need to remove generic_id from id_list of block id
            // Get ordered list of generic ids (back pointer) from block id
            Array *generic_ids = (_block_label) ? _block_label->GetGenerics() : 0 ;

            // Find the generic id to be removed in the ordered list of generic ids
            unsigned k ;
            FOREACH_ARRAY_ITEM(generic_ids, k, id_def) {
                if (id_def == generic_id) generic_ids->Remove(k) ; // Remove the matched id from generic list
            }

            if (block_scope) block_scope->Undeclare(generic_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire interface decl
                _generic_clause->Remove(i) ;    // Remove decl from _generic_clause
                delete generic_decl ;           // Delete declaration
                if (!_generic_clause->Size()) { // _generic_clause contains no element
                    delete _generic_clause ;    // Delete
                    _generic_clause = 0 ;
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ; // Remove id from generic_decl->GetIds()
                delete generic_id ; // delete generic id
            }

            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}
unsigned
VhdlBlockStatement::RemoveGeneric(const char *generic_name)
{
    if (_generics) return _generics->RemoveGeneric(generic_name) ; // Remove generic from generic clause of block generics

    return 0 ; // Fails to remove
}
////////////////////////////////////////////////////////////////////////////////
//      Secondary unit Parse-tree Signal Creation/Deletion routines           //
////////////////////////////////////////////////////////////////////////////////

VhdlIdDef *
VhdlArchitectureBody::AddSignal(const char *signal_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign)
{
    if (!signal_name || !subtype_indication || !_local_scope) return 0 ; // generic addition not possible

    // Create VhdlSignalId for 'signal_name'
    VhdlSignalId *new_signal = new VhdlSignalId(Strings::save(signal_name)) ;

    // Declare created signal to _local_scope, issue error if identifier 'signal_name'
    // already exists.
    if (!_local_scope->Declare(new_signal)) { // Identifier already declared
        delete new_signal ; // Delete created interface id
        return 0 ; // Return 0 to indicate failure
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to Secondary Uhit scope

    // Create array to hold created signal id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_signal) ;

    // Create declaration
    VhdlSignalDecl *signal_decl = new VhdlSignalDecl(id_list, subtype_indication, 0, init_assign) ;

    // Insert signal declaration into signal clause
    if (!_decl_part) _decl_part = new Array(1) ; // No declaration exists, create signal
    _decl_part->InsertLast(signal_decl) ;

    _present_scope = save_scope ; // Pop present scope

    return new_signal ; // Return created generic
}

unsigned
VhdlArchitectureBody::RemoveSignal(const char *signal_name)
{
    // Find 'signal_name' specific identifier in local scope
    VhdlIdDef *signal_id = FindDeclared(signal_name) ;

    if (!signal_id || !signal_id->IsSignal()) return 0 ; // No signal, cannot remove

    // Find the declaration of 'signal_id' iterating over declaration list
    unsigned i, j ;
    VhdlSignalDecl *signal_decl ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_decl_part, i, signal_decl) {
        if (!signal_decl || !(signal_decl->GetClassId() == ID_VHDLSIGNALDECL)) continue ;
        Array *ids = signal_decl->GetIds() ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != signal_id) continue ; // Not found, continue

            // Found, need to remove it from id_list and signal list of Secondary Unit id

            if (_local_scope) _local_scope->Undeclare(signal_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire signal decl
                _decl_part->Remove(i) ;     // Remove decl from decl_part
                delete signal_decl ;        // Delete declaration
                if (!_decl_part->Size()) {  // No declaration
                    delete _decl_part ;     // Delete decl list
                    _decl_part = 0 ;        // Set null to it
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ;    // Remove id from signal_decl->GetIds()
                delete signal_id ;  // delete signal id
            }
            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}
////////////////////////////////////////////////////////////////////////////////
//      Block statement Parse-tree Signal Creation/Deletion routines          //
////////////////////////////////////////////////////////////////////////////////

VhdlIdDef *
VhdlBlockStatement::AddSignal(const char *signal_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign)
{
    if (!signal_name || !subtype_indication || !_local_scope) return 0 ; // generic addition not possible

    // Create VhdlSignalId for 'signal_name'
    VhdlSignalId *new_signal = new VhdlSignalId(Strings::save(signal_name)) ;

    // Declare created signal to _local_scope, issue error if identifier 'signal_name'
    // already exists.
    if (!_local_scope->Declare(new_signal)) { // Identifier already declared
        delete new_signal ; // Delete created interface id
        return 0 ; // Return 0 to indicate failure
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to Secondary Uhit scope

    // Create array to hold created signal id
    Array *id_list = new Array(1) ;
    id_list->InsertLast(new_signal) ;

    // Create declaration
    VhdlSignalDecl *signal_decl = new VhdlSignalDecl(id_list, subtype_indication, 0, init_assign) ;

    // Insert signal declaration into signal clause
    if (!_decl_part) _decl_part = new Array(1) ; // No declaration exists, create signal
    _decl_part->InsertLast(signal_decl) ;

    _present_scope = save_scope ; // Pop present scope

    return new_signal ; // Return created generic
}

unsigned
VhdlBlockStatement::RemoveSignal(const char *signal_name)
{
    // Find 'signal_name' specific identifier in local scope
    VhdlIdDef *signal_id = (_local_scope) ? _local_scope->FindSingleObjectLocal(signal_name): 0 ;

    if (!signal_id || !signal_id->IsSignal()) return 0 ; // No signal, cannot remove

    // Find the declaration of 'signal_id' iterating over declaration list
    unsigned i, j ;
    VhdlSignalDecl *signal_decl ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_decl_part, i, signal_decl) {
        if (!signal_decl || !(signal_decl->GetClassId() == ID_VHDLSIGNALDECL)) continue ;
        Array *ids = signal_decl->GetIds() ; // Get id list
        FOREACH_ARRAY_ITEM(ids, j, id) { // Iterate over id list
            if (id != signal_id) continue ; // Not found, continue

            // Found, need to remove it from id_list and signal list of Secondary Unit id

            if (_local_scope) _local_scope->Undeclare(signal_id) ; // Remove id from local scope

            if (ids->Size() == 1) { // Id list contains 1 element, delete entire signal decl
                _decl_part->Remove(i) ;     // Remove decl from decl_part
                delete signal_decl ;        // Delete declaration
                if (!_decl_part->Size()) {  // No declaration
                    delete _decl_part ;     // Delete decl list
                    _decl_part = 0 ;        // Set null to it
                }
            } else { // Decl contains multiple ids, remove found one
                ids->Remove(j) ;    // Remove id from signal_decl->GetIds()
                delete signal_id ;  // delete signal id
            }
            return 1 ; // Finished return 1
        }
    }
    return 0 ; // Fails to remove
}

////////////////////////////////////////////////////////////////////////////////
//      Secondary unit Parse-tree Instance Creation/Deletion routines         //
////////////////////////////////////////////////////////////////////////////////

// Specific constructor to create component instantiation without port/generic map
// aspect : needed by secondary_unit::AddInstance
VhdlComponentInstantiationStatement::VhdlComponentInstantiationStatement(VhdlName *instantiated_unit)
  :  VhdlStatement(),
    _instantiated_unit(instantiated_unit),
    _generic_map_aspect(0),
    _port_map_aspect(0),
    _unit(0)
    // VIPER #6467 : Elaboration specific
    ,_library_name(0) // name of instantiated unit's library
    ,_instantiated_unit_name(0) // name of instantiated entity
    ,_arch_name(0) // name of instantiated architecture
{
    if (!_instantiated_unit) return ;

    // Pick up the instantiated unit identifier
    // We just want the primary unit identifier in here.
    _unit = _instantiated_unit->EntityAspect() ;
    if (!_unit) return ;

    if (_unit->IsSubprogram()) {
        // As of the VIPER #2791 fix (1/17/07), this check is only needed when doing static
        // elaboration after restoring an "old" VHDL vdb, in which labeled procedure calls with
        // no arguments are still represented as an VhdlComponentInstantiationStatement.

        // Yacc odity : a labeled procedure without arguments ends up
        // here as a component instantiation.
        // Check that there are no generics and/or ports.
        // Just type-infer the unit name (it must be a procedure)
        (void) _instantiated_unit->TypeInfer(UniversalVoid(),0,0/*no environment*/,0) ;
        return ;
    } else if (_instantiated_unit->GetEntityClass()) {
        // Check if the entity class matches
        unsigned entity_class = _instantiated_unit->GetEntityClass() ;
        if ((_unit->IsEntity()        && (entity_class != VHDL_entity)) ||
            (_unit->IsArchitecture()  && (entity_class != VHDL_entity)) || // Happens with indexed entity name
            (_unit->IsComponent()     && (entity_class != VHDL_component)) ||
            (_unit->IsConfiguration() && (entity_class != VHDL_configuration)))
        {
            _instantiated_unit->Error("%s is not a %s", _unit->Name(), EntityClassName(entity_class)) ;
        }
    } else {
        // Here, it should denote a component
        if (!_unit->IsComponent()) {
            Error("%s is not a %s", _unit->Name(), EntityClassName(VHDL_component)) ;
        }
    }

    // LRM 10.3 (j/k) : configuration can have generic/port map aspects.
    // Still set _unit to the configuration, but scope search rules need to
    // look at the entity decl (inside TypeInferAssociationList()).

    // FIX ME (in yacc ?) that components are the only ones allowed without explicit entity aspect

    // Check that there is no architecture name if it is not an entity
    if (!_unit->IsEntity() && _instantiated_unit->ArchitectureNameAspect()) {
        Error("%s is not an entity", _unit->Name()) ;
    }
}

VhdlComponentInstantiationStatement *
VhdlArchitectureBody::AddInstance(const char *instance_name, const char *instantiated_component_name)
{
    if (!instance_name || !instantiated_component_name || !_local_scope) return 0 ; // Nothing to add

    // Create label id with 'instance_name'
    VhdlLabelId *label_id = new VhdlLabelId(Strings::save(instance_name)) ;

    // Declare created id to this scope
    if (!_local_scope->Declare(label_id)) { // Identifier 'instance_name' already declared
        delete label_id ; // Delete created label
        return 0 ; // Failed to add instance
    }

    VhdlScope *save_scope = _present_scope ; // push present scope
    _present_scope = _local_scope ; // switch to Secondary Uhit scope

    // Create instantiated unit
    VhdlIdRef *component_name = new VhdlIdRef(Strings::save(instantiated_component_name)) ;
    VhdlInstantiatedUnit *new_instantiated_unit = new VhdlInstantiatedUnit(VHDL_component, component_name) ;

    // Create new component instance.
    VhdlComponentInstantiationStatement *new_instance = new VhdlComponentInstantiationStatement(new_instantiated_unit) ;

    // Set label to newly created instance
    new_instance->SetLabel(label_id) ;

    // Insert created instance to statement part
    if (!_statement_part) _statement_part = new Array(1) ;
    _statement_part->InsertLast(new_instance) ;

    _present_scope = save_scope ; // Pop present scope

    return new_instance ; // return created instance
}

unsigned
VhdlArchitectureBody::RemoveInstance(const char *instance_name)
{
    if (!instance_name) return 0 ; // Nothing to remove

    // Find the identifier 'instance_name' in local scope
    VhdlIdDef *instance_label = FindDeclared(instance_name) ;

    if (!instance_label) return 0 ; // 'instance_name' not declared in architecture

    // Find statement with label 'instance_name' in the statement list
    unsigned i ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        VhdlIdDef *label_id = stmt ? stmt->GetLabel(): 0 ;
        if (!label_id) continue ;
        if (label_id == instance_label) { // Found desired statement
            // If found statement is not instance, continue
            if (stmt && (stmt->GetClassId() != ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT)) continue ;

            // Remove the instance from statement list
            _statement_part->Remove(i) ;

            // Remove label id from architecture scope
            if (_local_scope) _local_scope->Undeclare(instance_label) ;

            // Delete instance statement
            delete stmt ;

            return 1 ; // Remove completed, return 1
        }
    }
    return 0 ; // Failed to remove
}

////////////////////////////////////////////////////////////////////////////////
//      Secondary unit Parse-tree Instance Port Connection Routines           //
////////////////////////////////////////////////////////////////////////////////

// Type infer the created formal-actual association, not the consistency of entire port connection list
// This function is copied from VhdlIdDef::TypeInferAssociationList, but that routine is
// untouched. FIXME : Make following routine private, call this from TypeInferAssociationList.
void
VhdlIdDef::TypeInferAssociation(VhdlDiscreteRange *assoc, unsigned object_kind, VhdlScope *actual_binding_scope, const VhdlTreeNode * /*from*/)
{
    if (!assoc) return ; // Nothing to type infer

    // Get the local scope to find formals
    VhdlScope *local_scope = 0 ;
    if (IsConfiguration() && GetEntity()) {
        // If unit is a configuration, association lists are for binding entity
        local_scope = GetEntity()->LocalScope() ;
    }
    // In all other cases, we should look for formals here, at this id, local.
    if (!local_scope) local_scope = LocalScope() ;

    VhdlName *formal_part = assoc->FormalPart() ; // Get the formal part
    VhdlDiscreteRange *actual_part = assoc->ActualPart() ; // Get actual

    VhdlIdDef *formal_type = 0, *actual_type = 0 ;
    VhdlIdDef *formal = 0, *actual = 0 ;
    VhdlIdDef *formal_desig_type = 0, *actual_desig_type = 0 ; // These are needed for type conversion

    if (formal_part) {
        // Type infer formal.
        formal_type = formal_part->TypeInferFormal(local_scope,0) ;
        formal = formal_part->FindFormal() ;
        if (!formal) return ; // FIXME should issue error msg
        formal_desig_type = formal->Type() ;

        // Check that we really have the right formal class here :
        if (object_kind==VHDL_port && !formal->IsPort())        formal_part->Error("formal %s is not a %s", formal->Name(), "port") ;
        if (object_kind==VHDL_generic && !formal->IsGeneric())  formal_part->Error("formal %s is not a %s", formal->Name(), "generic") ;
        if (!object_kind && !formal->IsInterfaceObject(0))      formal_part->Error("formal %s is not a %s", formal->Name(), "parameter") ;

#if 0
        // RD: 7/2007: Do not do the 'open' test on partial formals for AddPortRef calls.
        // There are many cases during design manipulation where the actual is later replaced by something else, or .
        if (!formal_part->FindFullFormal()) {
            // Partially associated formal :
            if (!actual_part || actual_part->IsOpen()) {
                // LRM rule check 4.3.2.2 (516-518) partial or type-conversion formals cannot have 'open' assocs :
                // VIPER issues 3100, 2310 and 2675 all ask to downgrade this to a warning
                formal_part->Warning("partially associated formal %s cannot have actual OPEN", formal->Name()) ;
            }
        }
#endif
    }
    // Check if this is 'open' association
    if (!formal || !formal_part || !actual_part || actual_part->IsOpen()) return ;

    // Set 'environment' for actual expression evaluation.
    // If formal is 'in', we expect an expression, so set to READ.
    // Other modes go through rigorous LRM association checks below (after type-inference).
    // Exclude 'file' formals : they require a file actual, which is checked separately below.
    unsigned environment = (formal->IsInput() && !formal->IsFile()) ? VHDL_READ : 0 ;

    // Type inference of the actual :
    VhdlDiscreteRange *actual_designator = 0 ; // actual type inference did not yet run, so wait with that.

    // Formal type-inference already ran, so we can pick-up the formal designator..
    VhdlDiscreteRange *formal_designator = formal_part->FormalDesignator() ;

    // Is the formal a type conversion (conversion function) (named association) ?
    if (formal_designator && (formal_designator != formal_part)) {
        // This formal is a type conversion or conversion function!

        // LRM 4.3.2.2 Association lists (462-473) :
        // The formal part of a named element association may be in the form of a function call
        // or a type conversion, where the sole argument, the formal designator, is of mode
        // out, inout, buffer, or linkage, and if the actual is not open.  In this case the
        // base type denoted by the return value (type mark) must be the same base type of the actual.
        if (formal->IsInput()) {
            // If this is an IN port, that should be an error (LRM 4.3.2.2 (468-473))
            assoc->Error("input designator %s cannot contain a formal type-conversion", formal->Name()) ;
        } else {
            // This port is either OUT, INOUT, BUFFER, LINKAGE
            // Actual type dictates formal type, so first infer actual type using no expected type:
            // No need to pass in environment : it will be difficult to determine, and later checks below require an 'actual' id, where checks are easier to do.
            actual_type = actual_part->TypeInfer(0 ,0, 0, actual_binding_scope) ;
            // Now check to see if actual is a type conversion
            actual_designator = actual_part->FormalDesignator() ;
            if (actual_designator && (actual_designator != actual_part)) {
                // Since actual is also a type conversion (conversion function), we need to
                // re-type-infer the actual using the formal designator as the expected type.
                actual_type = actual_part->TypeInfer(formal_desig_type ,0, 0, actual_binding_scope) ;
                actual_desig_type = actual_designator->TypeInfer(0, 0, 0, actual_binding_scope) ;
            }
            // iNow type infer again the formal, but pass in the actual's type to verify formal return type (VIPER #1956)
            (void) formal_part->TypeInferFormal(local_scope, (actual_desig_type) ? actual_desig_type : actual_type) ;
        }
    } else {
        // Normal : type-infer the actual against formal.
        // If mode is 'in', assume that an expression comes in.
        // Otherwise, the association linking below takes care of updating/reading the actual.
        // Pass environment in here, to make sure actual expression leafs are labeled as 'used'.
        (void) actual_part->TypeInfer(formal_type, 0, environment, actual_binding_scope) ;
    }

    // Now that actual type inference ran, we can surely pick-up the actual designator :
    if (!actual_designator) actual_designator = actual_part->FormalDesignator() ;

    // Is the actual a type conversion (conversion function) (named or positional assocation) ?
    if (actual_designator && (actual_designator != actual_part)) {
        // This actual is a type conversion or conversion function!

        // The formal was already passed into the actual's type inferrence above, so we
        // don't need to do anything specific here, except check for mode compliance, as below.

        // LRM 4.3.2.2 Association lists (473-486) :
        // The actual part of a (named or positional) element association may be in the form of a
        // function call or a type conversion, where the sole argument, the actual designator, is
        // of mode in, inout, or linkage, and if the actual is not open.  In this case the base
        // type denoted by the return value (type mark) must be the same base type of the formal.
        if (formal->IsOutput() || formal->IsBuffer()) {
            // If this is an OUT, BUFFER port, that should be an error (LRM 4.3.2.2 (473-486))
            assoc->Error("%s designator %s cannot contain an actual type-conversion", (formal->IsOutput()) ? "output" : "buffer", formal->Name()) ;
        }
    }

    // Now, after type-inference, the actual's identifiers are set.
    // See if there is a 'single' actual associated
    actual = actual_part->FindFormal() ; // FIX ME : naming of 'FindFormal' should change to 'FindDesignator' or so.

    // Port-association rules of LRM 1.1.1.2 :
    if (object_kind == VHDL_port) {
        // If the actual is an expression, the formal must be mode 'in' :
        if ((!actual || actual->IsConstant())) {
            if (!formal->IsInput()) {
                actual_part->Error("actual of formal %s port %s cannot be an expression", EntityClassName(formal->Mode()), formal->Name()) ;
            }
        }
        // Viper 7666: Modify port vs actual direction criteria for match as per Vhdl2008 rules described in section: 6.5.6.3
        if (actual && actual->IsPort()) {
            if (IsVhdl2008()) {
                if ((formal->IsInput()  && !(actual->IsInput() || actual->IsOutput() || actual->IsInout() || actual->IsBuffer())) ||
                    (formal->IsOutput() && !(actual->IsInout() || actual->IsOutput() || actual->IsBuffer())) ||
                    (formal->IsInout()  && !(actual->IsInout() || actual->IsOutput() || actual->IsBuffer())) ||
                    (formal->IsBuffer() && !(actual->IsBuffer() || actual->IsOutput() || actual->IsInout()))) {
                    // 'linkage' can be associated with any other port, so no check needed.

                    // This is officially an error, but too many designs violate it. So make it a warning.
                    actual_part->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s",
                        formal->Name(), EntityClassName(formal->Mode()),
                        actual->Name(), EntityClassName(actual->Mode())) ;
                }
            } else {
                if ((formal->IsInput()  && !(actual->IsInput() || actual->IsInout() || actual->IsBuffer())) ||
                    (formal->IsOutput() && !(actual->IsInout() || actual->IsOutput())) ||
                    (formal->IsInout()  && !(actual->IsInout())) ||
                    (formal->IsBuffer() && !(actual->IsBuffer()))) {
                    // 'linkage' can be associated with any other port, so no check needed.
                    // This is officially an error, but too many designs violate it. So make it a warning.
                    actual_part->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s",
                        formal->Name(), EntityClassName(formal->Mode()),
                        actual->Name(), EntityClassName(actual->Mode())) ;
                }
            }
        }
    }

    // Missing in LRM is the 'generic-association' rules (should be in LRM 1.1.1.1).
    // VIPER Issue 1709.
    if (object_kind == VHDL_generic) {
        // ModelSim issues this error :
        if (actual && actual->IsSignal()) {
            // VIPER 3081 : turn into a warning.
            actual_part->Warning("actual expression for generic %s cannot reference a signal",formal->Name()) ;
        }
    }

    // Parameter-association rules LRM 2.1.1
    if (!object_kind) {
        if (formal->IsFile()) {
            if (!actual || !actual->IsFile()) {
                actual_part->Error("actual %s of formal %s must be a %s", (actual) ? actual->Name() : "", formal->Name(), EntityClassName(VHDL_file)) ;
            }
        } else if (formal->IsSignal()) {
            if (!actual || !actual->IsSignal()) {
                actual_part->Error("actual %s of formal %s must be a %s", (actual) ? actual->Name() : "", formal->Name(), EntityClassName(VHDL_signal)) ;
            }
        } else if (formal->IsVariable()) {
            // CHECK ME : is a file a variable in vhdl'87 ? '87 textio package thinks so..
            if (!actual || !actual->IsVariable()) {
                actual_part->Error("actual %s of formal %s must be a %s", (actual) ? actual->Name() : "", formal->Name(), EntityClassName(VHDL_variable)) ;
            }
        }
    }

    // Do READ/WRITE checks of actual-formal associations. LRM 4.3.2.
    // Also, set 'used' and 'assigned' for these.
    // In PSL directives/declarations these should be skipped.
    if (actual && !IsVhdlPsl()) {
        // We are associated :

        // Discard this one as a 'ram' (since we don't allow ram's referred in association lists)
        // VIPER issue 2674 : we should allow ram inferencing in association lists.
        // INOUT ports would constitute a read AND a write, which might discard the actual as RAM,
        // but this should be done in the VarUsage routines. Not here.
        // actual->SetCannotBeDualPortRam() ;

        // MultiPort RAMs : currently discard if the formal is OUTPUT or INOUT.
        // Have to discard OUTPUT, since RTL elaboration always does a Evaluate on the actual, which would create a read port.
        // Once that is fixed, we can allow OUTPUT also.
#if 0 // RD: This is now obsolete, since this work is done in VarUsage
        if (actual->CanBeMultiPortRam() && (formal->IsOutput() || formal->IsInout())) {
            actual->SetCannotBeMultiPortRam() ;
        }
#endif

        // Rules in 4.3.2 apply :
        // actual is READ if formal is 'in', 'inout' or 'linkage'
        if (formal->IsInput() || formal->IsInout() || formal->IsLinkage()) {
            // Label this actual as 'used'
            actual->SetUsed() ;
            // If actual is 'out' reading is not allowed. Formal mode linkage can always be associated with any actual port mode.
            // out mode port can be read in 2008 mode
            if (actual->IsOutput() && !formal->IsLinkage() && !vhdl_file::IsVhdl2008()) {
                actual_part->Error("%s with mode 'out' cannot be read", actual->Name()) ;
            }
        }
        // actual is UPDATED if formal is 'out' 'buffer' 'inout' or 'linkage'
        if (formal->IsOutput() || formal->IsInout() || formal->IsBuffer() || formal->IsLinkage()) {
            actual->SetAssigned() ;
            // If actual is 'in' updating is not allowed. Formal mode linkage can always be associated with any actual port mode.
            if (actual->IsInput() && !formal->IsLinkage()) {
                actual_part->Error("%s with mode 'in' cannot be updated", actual->Name()) ;
            }
        }
        // Finally, if actual is 'linkage', we can only read/update from a 'linkage' object
        if (actual->IsLinkage() && !formal->IsLinkage()) {
            actual_part->Error("cannot read or update 'linkage' object %s", actual->Name()) ;
        }
    }
}

void
VhdlComponentInstantiationStatement::AddPortRef(VhdlDiscreteRange *assoc)
{
    if (!assoc) return ; // nothing to add

    // Create _port_map_aspect if not exists
    if (!_port_map_aspect) _port_map_aspect = new Array(1) ;

    // Append 'assoc' to _port_map_aspect
    _port_map_aspect->InsertLast(assoc) ;

    // Map actual->formal
    if (_unit) _unit->TypeInferAssociation(assoc, VHDL_port, 0, this) ;
}

unsigned
VhdlArchitectureBody::AddPortRef(const char *instance_name, const char *formal_port_name, VhdlExpression *actual)
{
    if (!instance_name || !formal_port_name || !actual) return 0 ; // Impossible to add, return 0

    // Find instantiation with label 'instance_name'
    unsigned j ;
    VhdlDiscreteRange *assoc = 0 ;
    VhdlName *formal ;
    VhdlExpression *actual2 ;

    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_instance_name = Strings::save(instance_name) ;
    char *lower_cased_formal_port_name = Strings::save(formal_port_name) ;
    if (instance_name[0]!='\\' || instance_name[Strings::len(instance_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_instance_name) ;
    }
    if (formal_port_name[0]!='\\' || formal_port_name[Strings::len(formal_port_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_formal_port_name) ;
    }

    VhdlIdDef *inst_id = FindDeclared(lower_cased_instance_name) ;
    VhdlComponentInstantiationStatement *instance = inst_id ? (VhdlComponentInstantiationStatement*)inst_id->GetStatement() : 0 ;
    if (!instance || instance->GetClassId() != ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT) {
        Strings::free(lower_cased_formal_port_name) ;
        Strings::free(lower_cased_instance_name) ;
        return 0 ; // Don't add portref since association already exists
    }

    VhdlScope *save_scope = _present_scope ; // Push present scope
    _present_scope = _local_scope ; // Switch to Secondary Unit scope

    unsigned named_assoc = 0 ; // VIPER #6872: Start with positional association
    Array *port_map_aspect = instance->GetPortMapAspect() ;
    // Now check if association already exists for this formal
    FOREACH_ARRAY_ITEM(port_map_aspect, j, assoc) {
        if (!assoc) continue ;
        formal = (assoc->IsAssoc()) ? assoc->FormalPart() : 0 ;
        if (!formal) continue ; // Not named association
        named_assoc = 1 ; // VIPER #6872: Named association
        actual2 = assoc->ActualPart() ;
        if (Strings::compare(formal->Name(), lower_cased_formal_port_name)) { // Found port-ref
            // Formal-actual assocation already exist.
            if (actual2 && actual2->IsOpen()) {
                // If actual is "open", remove and delete association, and continue adding
                // new assocation below.
                port_map_aspect->Remove(j) ; // Remove j'th association
                delete assoc ;
                break ;
            }
            Strings::free(lower_cased_formal_port_name) ;
            Strings::free(lower_cased_instance_name) ;
            return 0 ; // Don't add portref since association already exists
        }
    }

    // Create new association
    VhdlDiscreteRange *port_ref = actual ;
    // VIPER #6872: Create named port association if existing connection are also named association:
    if (named_assoc || !port_map_aspect || !port_map_aspect->Size()) {
        formal = new VhdlIdRef(Strings::save(formal_port_name)) ; // Create formal part
        port_ref = new VhdlAssocElement(formal, actual) ; // Create named association
    }
    instance->AddPortRef(port_ref) ; // Add created port ref to 'instance'

    _present_scope = save_scope ; // Pop present scope

    Strings::free(lower_cased_formal_port_name) ;
    Strings::free(lower_cased_instance_name) ;
    return 1 ; // Finished adding, return 1
}

unsigned
VhdlArchitectureBody::RemovePortRef(const char *instance_name, const char *formal_port_name)
{
    if (!instance_name) return 0 ; // No way to find instance

    unsigned i ;
    unsigned found = 0 ;

    // Find instantiation with label 'instance_name'
    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_instance_name = Strings::save(instance_name) ;
    if (instance_name[0]!='\\' || instance_name[Strings::len(instance_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_instance_name) ;
    }

    VhdlIdDef *label = FindDeclared(lower_cased_instance_name) ;
    VhdlComponentInstantiationStatement *instance = label ? (VhdlComponentInstantiationStatement*)label->GetStatement() : 0 ;
    if (!instance || instance->GetClassId() != ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT) return 0 ;

    Array *port_map_aspect = instance->GetPortMapAspect() ; // Get port map aspect of instance

    if (!port_map_aspect || !port_map_aspect->Size()) return 0 ; // No element in port map aspect

    VhdlDiscreteRange *assoc = 0 ;

    if (formal_port_name) { // Have formal name

        char *lower_cased_formal_port_name = Strings::save(formal_port_name) ;
        if (formal_port_name[0]!='\\' || formal_port_name[Strings::len(formal_port_name)-1]!='\\') {
            (void) Strings::strtolower(lower_cased_formal_port_name) ;
        }

        found = 0 ;
        // Find actual for formal 'formal_port_name'
        // VIPER #8415: Traverse the port association list in beckward direction
        // to remove items properly
        FOREACH_ARRAY_ITEM_BACK(port_map_aspect, i, assoc) {
            if (!assoc) continue ;
            VhdlIdDef *formal = (assoc->IsAssoc()) ? assoc->FindFormal() : 0 ;
            if (formal && Strings::compare(formal->Name(), lower_cased_formal_port_name)) { // Found port-ref
                port_map_aspect->Remove(i) ; // Remove i th assoc
                delete assoc ;
                found = 1 ;
            } else {
                // VIPER #8415 : User can give partial formal port like 'q(7)'.
                // Take the char* form of the formal-part of 'assoc' and check
                // whether that is given as 'formal_port_name'
                ostringstream os ; // Dynamically allocated string buffer
                VhdlName *formal_name = assoc->FormalPart() ;
                if (formal_name) formal_name->PrettyPrint(os, 0) ;
                char *copied_str = Strings::save(os.str().c_str()) ;
                if (Strings::compare(copied_str, lower_cased_formal_port_name)) {
                    port_map_aspect->Remove(i) ; // Remove i th assoc
                    delete assoc ;
                    found = 1 ;
                }
                Strings::free(copied_str) ;
            }
        }
        if (found) {
            Strings::free(lower_cased_formal_port_name) ;
            Strings::free(lower_cased_instance_name) ;
            return 1 ; // Finished removing, return 1
        }

        // 'formal_port_name' is not in a named association, look up its position
        // in instantiated unit
        // Get the instantiated unit from 'instance'
        VhdlIdDef *instantiated_unit = instance->GetInstantiatedUnit() ;

        // Find the position of port 'formal_port_name' by iterating over port list of instantiated unit
        VhdlIdDef *port_id ;
        Array *ports = instantiated_unit ? instantiated_unit->GetPorts() : 0 ; // Get ports of instantiated unit
        FOREACH_ARRAY_ITEM(ports, i, port_id) {
            if (!port_id) continue ;
            if (Strings::compare(port_id->Name(), lower_cased_formal_port_name)) { // Found port
                 // Remove i th element from 'port_map_aspect'
                 if (port_map_aspect->Size() > i) { // i th position exists in port map aspect
                     assoc = (VhdlDiscreteRange*)port_map_aspect->At(i) ; // Get i th port-ref
                     port_map_aspect->Remove(i) ; // Remove i th assoc
                     delete assoc ; // Delete assoc
                     Strings::free(lower_cased_formal_port_name) ;
                     Strings::free(lower_cased_instance_name) ;
                     return 1 ; // Finished removing, return 1
                 }
            }
        }
        Strings::free(lower_cased_formal_port_name) ;
    } else { // No formal name specified remove the last element of port map aspect
        assoc = (VhdlDiscreteRange*)port_map_aspect->RemoveLast() ;
        delete assoc ; // delete assoc
        Strings::free(lower_cased_instance_name) ;
        return 1 ; // Finished removing, return 1
    }
    Strings::free(lower_cased_instance_name) ;
    return 0 ; // Failed to remove
}
///////////////////////////////////////////////////////////////////////////////
//    Add/Remove port reference to/from port map aspect of block statement   //
///////////////////////////////////////////////////////////////////////////////
unsigned
VhdlBlockStatement::AddPortRef(const char *formal_port_name, VhdlExpression *actual)
{
    // Port map aspect is in VhdlBlockPorts, try to add there
    if (_ports) return _ports->AddPortRef(formal_port_name, actual) ;
    return 0 ;
}
unsigned
VhdlBlockPorts::AddPortRef(const char *formal_port_name, VhdlExpression *actual)
{
    if (!formal_port_name || !actual) return 0 ; // Impossible to add, return 0

    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_formal_port_name = Strings::save(formal_port_name) ;
    if (formal_port_name[0]!='\\' || formal_port_name[Strings::len(formal_port_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_formal_port_name) ;
    }

    VhdlScope *block_scope = (_block_label) ? _block_label->LocalScope() : 0 ;

    VhdlScope *save_scope = _present_scope ; // Push present scope
    _present_scope = block_scope ; // Switch to block scope

    unsigned j ;
    VhdlDiscreteRange *assoc ;
    VhdlDiscreteRange *actual2 ;
    unsigned named_assoc = 0 ; // VIPER #6872: Start with positional association
    // Now check if association already exists for this formal
    FOREACH_ARRAY_ITEM(_port_map_aspect, j, assoc) {
        if (!assoc) continue ;
        VhdlName *formal = (assoc->IsAssoc()) ? assoc->FormalPart() : 0 ;
        if (!formal) continue ; // Not named association
        named_assoc = 1 ; // VIPER #6872: Named association
        actual2 = assoc->ActualPart() ;
        if (Strings::compare(formal->Name(), lower_cased_formal_port_name)) { // Found port-ref
            // Formal-actual assocation already exist.
            if (actual2 && actual2->IsOpen()) {
                // If actual is "open", remove and delete assocation, and continue adding
                // new assocation below.
                _port_map_aspect->Remove(j) ; // Remove j'th association
                delete assoc ;
                break ;
            }
            Strings::free(lower_cased_formal_port_name) ;
            return 0 ; // Don't add portref since assocation already exists
        }
    }

    // Create new association
    // VIPER #6872: Create named port association if existing connection are also named association:
    VhdlDiscreteRange *port_ref = actual ;
    if (named_assoc || !_port_map_aspect || !_port_map_aspect->Size()) {
        VhdlName *formal = new VhdlIdRef(Strings::save(formal_port_name)) ; // Create formal part
        port_ref = new VhdlAssocElement(formal, actual) ; // Create named association
    }
    if (!_port_map_aspect) _port_map_aspect = new Array(1) ; // Create _port_map_aspect if not exists
    _port_map_aspect->InsertLast(port_ref) ; // Append 'port_ref' to _port_map_aspect
    if (_block_label) _block_label->TypeInferAssociation(port_ref, VHDL_port, 0, this) ;

    _present_scope = save_scope ; // Pop present scope

    Strings::free(lower_cased_formal_port_name) ;
    return 1 ; // Finished adding, return 1
}

unsigned
VhdlBlockStatement::RemovePortRef( const char *formal_port_name)
{
    // Port map aspect is in VhdlBlockPorts, try to remove from there
    if (_ports) return _ports->RemovePortRef(formal_port_name) ;
    return 0 ;
}
unsigned
VhdlBlockPorts::RemovePortRef( const char *formal_port_name)
{
    if (!_port_map_aspect || !_port_map_aspect->Size()) return 0 ; // No element in port map aspect

    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_formal_port_name = formal_port_name ? Strings::save(formal_port_name) : 0 ;
    if (formal_port_name && (formal_port_name[0]!='\\' || formal_port_name[Strings::len(formal_port_name)-1]!='\\')) {
        (void) Strings::strtolower(lower_cased_formal_port_name) ;
    }

    VhdlDiscreteRange *assoc = 0 ;

    if (lower_cased_formal_port_name) { // Have formal name
        unsigned found = 0 ;
        unsigned i ;
        // Find actual for formal 'formal_port_name'
        // VIPER #8415: Traverse the port association list in beckward direction
        // to remove items properly
        FOREACH_ARRAY_ITEM_BACK(_port_map_aspect, i, assoc) {
            if (!assoc) continue ;
            VhdlIdDef *formal = (assoc->IsAssoc()) ? assoc->FindFormal() : 0 ;
            if (formal && Strings::compare(formal->Name(), lower_cased_formal_port_name)) { // Found port-ref
                _port_map_aspect->Remove(i) ; // Remove i th assoc
                delete assoc ;
                found = 1 ;
            } else {
                // VIPER #8415 : User can give partial formal port like 'q(7)'.
                // Take the char* form of the formal-part of 'assoc' and check
                // whether that is given as 'formal_port_name'
                ostringstream os ; // Dynamically allocated string buffer
                VhdlName *formal_name = assoc->FormalPart() ;
                if (formal_name) formal_name->PrettyPrint(os, 0) ;
                char *copied_str = Strings::save(os.str().c_str()) ;
                if (Strings::compare(copied_str, lower_cased_formal_port_name)) {
                    _port_map_aspect->Remove(i) ; // Remove i th assoc
                    delete assoc ;
                    found = 1 ;
                }
                Strings::free(copied_str) ;
            }
        }
        if (found) {
            Strings::free(lower_cased_formal_port_name) ;
            return 1 ; // Finished removing, return 1
        }

        // 'formal_port_name' is not in a named association, look up its position
        // in block

        // Find the position of port 'formal_port_name' by iterating over port list of block
        VhdlIdDef *port_id ;
        Array *ports = (_block_label) ? _block_label->GetPorts() : 0 ; // Get ports of instantiated unit
        FOREACH_ARRAY_ITEM(ports, i, port_id) {
            if (!port_id) continue ;
            if (Strings::compare(port_id->Name(), lower_cased_formal_port_name)) { // Found port
                 // Remove i th element from 'port_map_aspect'
                 if (_port_map_aspect->Size() > i) { // i th position exists in port map aspect
                     assoc = (VhdlDiscreteRange*)_port_map_aspect->At(i) ; // Get i th port-ref
                     _port_map_aspect->Remove(i) ; // Remove i th assoc
                     delete assoc ; // Delete assoc
                     Strings::free(lower_cased_formal_port_name) ;
                     return 1 ; // Finished removing, return 1
                 }
            }
        }
    } else { // No formal name specified remove the last element of port map aspect
        assoc = (VhdlDiscreteRange*)_port_map_aspect->RemoveLast() ;
        delete assoc ; // delete assoc
        Strings::free(lower_cased_formal_port_name) ;
        return 1 ; // Finished removing, return 1
    }
    Strings::free(lower_cased_formal_port_name) ;
    return 0 ; // Failed to remove
}
////////////////////////////////////////////////////////////////////////////////
//      Secondary unit Parse-tree Instance Generic Connection Routines        //
////////////////////////////////////////////////////////////////////////////////

void
VhdlComponentInstantiationStatement::AddGenericRef(VhdlDiscreteRange *assoc)
{
    if (!assoc) return ; // Nothing to add

    // Create _port_map_aspect if not exists
    if (!_generic_map_aspect) _generic_map_aspect = new Array(1) ;

    // Append 'assoc' to _port_map_aspect
    _generic_map_aspect->InsertLast(assoc) ;

    // Map actual->formal
    if (_unit) _unit->TypeInferAssociation(assoc, VHDL_generic, 0 , this) ;
}

unsigned
VhdlArchitectureBody::AddGenericRef(const char *instance_name, const char *formal_generic_name, VhdlExpression *actual)
{
    if (!instance_name || !actual) return 0 ; // Impossible to add, return 0

    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_instance_name = Strings::save(instance_name) ;
    if (instance_name[0]!='\\' || instance_name[Strings::len(instance_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_instance_name) ;
    }

    char *lower_cased_formal_generic_name = formal_generic_name ? Strings::save(formal_generic_name) : 0 ;
    if (formal_generic_name && (formal_generic_name[0]!='\\' || formal_generic_name[Strings::len(formal_generic_name)-1]!='\\')) {
        (void) Strings::strtolower(lower_cased_formal_generic_name) ;
    }

    // Find instantiation with label 'instance_name'
    VhdlIdDef *label = FindDeclared(lower_cased_instance_name) ;
    VhdlComponentInstantiationStatement *instance = label ? (VhdlComponentInstantiationStatement*)label->GetStatement() : 0 ;
    if (!instance || instance->GetClassId() != ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT) {
        Strings::free(lower_cased_formal_generic_name) ;
        Strings::free(lower_cased_instance_name) ;
        return 0 ;
    }
    // Find instantiation with label 'instance_name'
    unsigned j ;
    VhdlDiscreteRange *assoc ;
    VhdlDiscreteRange *actual2 ;
    unsigned named_assoc = 0 ; // VIPER #6872: Start with positional association
    Array *generic_map_aspect = instance->GetGenericMapAspect() ;
    // Now check if association already exists for this formal
    FOREACH_ARRAY_ITEM(generic_map_aspect, j, assoc) {
        if (!assoc) continue ;
        VhdlName *formal = (assoc->IsAssoc()) ? assoc->FormalPart() : 0 ;
        if (!formal) continue ; // Not named association
        named_assoc = 1 ; // VIPER #6872: Named association
        actual2 = assoc->ActualPart() ;
        if (Strings::compare(formal->Name(), lower_cased_formal_generic_name)) { // Found generic-ref
            // Formal-actual assocation already exist.
            if (actual2 && actual2->IsOpen()) {
                // If actual is "open", remove and delete assocation, and continue adding
                // new assocation below.
                generic_map_aspect->Remove(j) ; // Remove j'th association
                delete assoc ;
                break ;
            }
            Strings::free(lower_cased_formal_generic_name) ;
            Strings::free(lower_cased_instance_name) ;
            return 0 ; // Don't add portref since assocation already exists
        }
    }
    VhdlScope *save_scope = _present_scope ; // Push present scope
    _present_scope = _local_scope ; // Switch to Secondary Uhit scope

    VhdlDiscreteRange *generic_ref = actual ; // To be added generic ref is 'actual'
    // VIPER #6872: Create named port association if existing connection are also named association:
    if (formal_generic_name && (named_assoc || !generic_map_aspect || !generic_map_aspect->Size())) { // Need to create named association
        VhdlIdRef *formal = new VhdlIdRef(Strings::save(formal_generic_name)) ; // Create formal part
        generic_ref = new VhdlAssocElement(formal, actual) ; // Create named association
    }
    instance->AddGenericRef(generic_ref) ; // Add created generic ref to 'instance'
    _present_scope = save_scope ; // Pop present scope
    Strings::free(lower_cased_formal_generic_name) ;
    Strings::free(lower_cased_instance_name) ;
    return 1 ; // Finshed adding, return 1
}

unsigned
VhdlArchitectureBody::RemoveGenericRef(const char *instance_name, const char *formal_generic_name)
{
    if (!instance_name) return 0 ; // No way to find instance

    unsigned i ;
    unsigned found = 0 ;
 
    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_instance_name = Strings::save(instance_name) ;
    char *lower_cased_formal_generic_name = Strings::save(formal_generic_name) ;
    if (instance_name[0]!='\\' || instance_name[Strings::len(instance_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_instance_name) ;
    }
    if (formal_generic_name && (formal_generic_name[0]!='\\' || formal_generic_name[Strings::len(formal_generic_name)-1]!='\\')) {
        (void) Strings::strtolower(lower_cased_formal_generic_name) ;
    }

    // Find instantiation with label 'instance_name'
    VhdlIdDef *label = FindDeclared(lower_cased_instance_name) ;
    VhdlComponentInstantiationStatement *instance = label ? (VhdlComponentInstantiationStatement*)label->GetStatement() : 0 ;
    if (!instance || instance->GetClassId() != ID_VHDLCOMPONENTINSTANTIATIONSTATEMENT) {
        Strings::free(lower_cased_instance_name) ;
        Strings::free(lower_cased_formal_generic_name) ;
        return 0 ;
    }

    Array *generic_map_aspect = instance->GetGenericMapAspect() ; // Get generic map aspect of instance

    if (!generic_map_aspect || !generic_map_aspect->Size()) {
        Strings::free(lower_cased_instance_name) ;
        Strings::free(lower_cased_formal_generic_name) ;
        return 0 ; // No element in generic map aspect
    }

    VhdlDiscreteRange *assoc = 0 ;

    if (formal_generic_name) { // Have formal name
        found = 0 ;
        // Find actual for formal 'formal_generic_name'
        FOREACH_ARRAY_ITEM(generic_map_aspect, i, assoc) {
            VhdlIdDef *formal = (assoc && assoc->IsAssoc()) ? assoc->FindFormal() : 0 ;
            if (!formal) continue ; // Not named association
            if (Strings::compare(formal->Name(), lower_cased_formal_generic_name)) { // Found generic-ref
                generic_map_aspect->Remove(i) ; // Remove i th assoc
                delete assoc ;
                found = 1 ;
            }
        }
        if (found) {
            Strings::free(lower_cased_instance_name) ;
            Strings::free(lower_cased_formal_generic_name) ;
            return 1 ; // Finished removing, return 1
        }

        // 'formal_generic_name' is not in a named association, look up its position
        // in instantiated unit
        // Get the instantiated unit from 'instance'
        VhdlIdDef *instantiated_unit = instance->GetInstantiatedUnit() ;

        // Find the position of port 'formal_generic_name' by iterating over generic list of instantiated unit
        VhdlIdDef *generic_id ;
        Array *generics = instantiated_unit ? instantiated_unit->GetGenerics() : 0 ; // Get generics of instantiated unit
        FOREACH_ARRAY_ITEM(generics, i, generic_id) {
            if (!generic_id) continue ;
            if (Strings::compare(generic_id->Name(), lower_cased_formal_generic_name)) { // Found generic
                 // Remove i th element from 'generic_map_aspect'
                 if (generic_map_aspect->Size() > i) { // i th position exists in generic map aspect
                     assoc = (VhdlDiscreteRange*)generic_map_aspect->At(i) ; // Get i th generic-ref
                     generic_map_aspect->Remove(i) ; // Remove i th assoc
                     delete assoc ; // delete assoc
                     Strings::free(lower_cased_instance_name) ;
                     Strings::free(lower_cased_formal_generic_name) ;
                     return 1 ; // Finished removing, return 1
                 }
            }
        }
    } else { // No formal name specified remove the last element of generic map aspect
        assoc = (VhdlDiscreteRange*)generic_map_aspect->RemoveLast() ;
        delete assoc ; // delete assoc
        Strings::free(lower_cased_instance_name) ;
        Strings::free(lower_cased_formal_generic_name) ;
        return 1 ; // Finished removing, return 1
    }
    Strings::free(lower_cased_instance_name) ;
    Strings::free(lower_cased_formal_generic_name) ;
    return 0 ; // Failed to remove
}
///////////////////////////////////////////////////////////////////////////////
//  Add/Remove generic reference to/from generic map aspect of block         //
///////////////////////////////////////////////////////////////////////////////
unsigned
VhdlBlockStatement::AddGenericRef(const char *formal_generic_name, VhdlExpression *actual)
{
    // Generic map aspect is in VhdlBlockGenerics, try to add there
    if (_generics) return _generics->AddGenericRef(formal_generic_name, actual) ;
    return 0 ;
}
unsigned
VhdlBlockGenerics::AddGenericRef(const char *formal_generic_name, VhdlExpression *actual)
{
    if (!actual || !formal_generic_name) return 0 ; // Impossible to add, return 0

    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_formal_generic_name = Strings::save(formal_generic_name) ;
    if (formal_generic_name[0]!='\\' || formal_generic_name[Strings::len(formal_generic_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_formal_generic_name) ;
    }

    VhdlScope *block_scope = (_block_label) ? _block_label->LocalScope(): 0 ;
    VhdlScope *save_scope = _present_scope ; // Push present scope
    _present_scope = block_scope ; // Switch to block scope

    unsigned j ;
    VhdlDiscreteRange *assoc ;
    VhdlDiscreteRange *actual2 ;
    unsigned named_assoc = 0 ; // VIPER #6872: Start with positional association
    // Now check if association already exists for this formal
    FOREACH_ARRAY_ITEM(_generic_map_aspect, j, assoc) {
        if (!assoc) continue ;
        VhdlName *formal = (assoc->IsAssoc()) ? assoc->FormalPart() : 0 ;
        if (!formal) continue ; // Not named association
        named_assoc = 1 ; // VIPER #6872: Named association
        actual2 = assoc->ActualPart() ;
        if (Strings::compare(formal->Name(), lower_cased_formal_generic_name)) { // Found generic-ref
            // Formal-actual assocation already exist.
            if (actual2 && actual2->IsOpen()) {
                // If actual is "open", remove and delete assocation, and continue adding
                // new assocation below.
                _generic_map_aspect->Remove(j) ; // Remove j'th association
                delete assoc ;
                break ;
            }
            Strings::free(lower_cased_formal_generic_name) ;
            return 0 ; // Don't add genericref since assocation already exists
        }
    }
    VhdlDiscreteRange *generic_ref = actual ; // To be added generic ref is 'actual'
    // Need to create named association
    // VIPER #6872: Create named port association if existing connection are also named association:
    if (named_assoc || !_generic_map_aspect || !_generic_map_aspect->Size()) {
        VhdlIdRef *formal = new VhdlIdRef(Strings::save(formal_generic_name)) ; // Create formal part
        generic_ref = new VhdlAssocElement(formal, actual) ; // Create named association
    }

    if (!_generic_map_aspect) _generic_map_aspect = new Array(1) ; // Create _generic_map_aspect if not exists
    _generic_map_aspect->InsertLast(generic_ref) ;

    if (_block_label) _block_label->TypeInferAssociation(generic_ref, VHDL_generic, 0 , this) ;
    _present_scope = save_scope ; // Pop present scope
    Strings::free(lower_cased_formal_generic_name) ;
    return 1 ; // Finshed adding, return 1
}

unsigned
VhdlBlockStatement::RemoveGenericRef( const char *formal_generic_name)
{
    // Generic map is in VhdlBlockGenerics, try to remove from there
    if (_generics) return _generics->RemoveGenericRef(formal_generic_name) ;
    return 0 ;
}
unsigned
VhdlBlockGenerics::RemoveGenericRef( const char *formal_generic_name)
{
    if (!_generic_map_aspect || !_generic_map_aspect->Size()) return 0 ; // No element in generic map aspect

    VhdlDiscreteRange *assoc = 0 ;

    if (formal_generic_name) { // Have formal name
        // Viper 6425: Convert strings to compare to lowercase as VHDL is case
        // independent. Do this unless the name is escaped
        char *lower_cased_formal_generic_name = Strings::save(formal_generic_name) ;
        if (formal_generic_name[0]!='\\' || formal_generic_name[Strings::len(formal_generic_name)-1]!='\\') {
            (void) Strings::strtolower(lower_cased_formal_generic_name) ;
        }
        unsigned found = 0 ;
        unsigned i ;
        // Find actual for formal 'formal_generic_name'
        FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) {
            VhdlIdDef *formal = (assoc && assoc->IsAssoc()) ? assoc->FindFormal() : 0 ;
            if (!formal) continue ; // Not named association
            if (Strings::compare(formal->Name(), lower_cased_formal_generic_name)) { // Found generic-ref
                _generic_map_aspect->Remove(i) ; // Remove i th assoc
                delete assoc ;
                found = 1 ;
            }
        }
        if (found) {
            Strings::free(lower_cased_formal_generic_name) ;
            return 1 ; // Finished removing, return 1
        }

        // 'formal_generic_name' is not in a named association, look up its position
        // in block

        // Find the position of port 'formal_generic_name' by iterating over generic list of block
        VhdlIdDef *generic_id ;
        Array *generics = (_block_label) ? _block_label->GetGenerics() : 0 ; // Get generics of instantiated unit
        FOREACH_ARRAY_ITEM(generics, i, generic_id) {
            if (!generic_id) continue ;
            if (Strings::compare(generic_id->Name(), lower_cased_formal_generic_name)) { // Found generic
                 // Remove i th element from '_generic_map_aspect'
                 if (_generic_map_aspect->Size() > i) { // i th position exists in generic map aspect
                     assoc = (VhdlDiscreteRange*)_generic_map_aspect->At(i) ; // Get i th generic-ref
                     _generic_map_aspect->Remove(i) ; // Remove i th assoc
                     delete assoc ; // delete assoc
                     Strings::free(lower_cased_formal_generic_name) ;
                     return 1 ; // Finished removing, return 1
                 }
            }
        }
        Strings::free(lower_cased_formal_generic_name) ;
    } else { // No formal name specified remove the last element of generic map aspect
        assoc = (VhdlDiscreteRange*)_generic_map_aspect->RemoveLast() ;
        delete assoc ; // delete assoc
        return 1 ; // Finished removing, return 1
    }
    return 0 ; // Failed to remove
}

///////////////////////////////////////////////////////////////////////////////////
//  Secondary unit Parse-tree Component Declaration Generic Connection Routines  //
///////////////////////////////////////////////////////////////////////////////////

VhdlIdDef *
VhdlArchitectureBody::AddComponentDecl(const char *component_name)
{
    if (!component_name || !_local_scope) return 0 ; // Impossible to create component

    // Create component id of name 'component_name'
    VhdlComponentId *component_id = new VhdlComponentId(Strings::save(component_name)) ;

    // Declare the component id in the scope of architecture
    if (!_local_scope->Declare(component_id)) { // Identifier 'component_name' already exists
        delete component_id ; // Delete created component id
        return 0 ;
    }

    VhdlScope *save_scope = _present_scope ; // Push present scope
    _present_scope = _local_scope ; // Switch to Secondary Uhit scope

    // Create component's local scope : child of architecture scope
    VhdlScope *component_scope = new VhdlScope(_local_scope, component_id) ;

    // Create component declaration
    VhdlComponentDecl *new_decl = new VhdlComponentDecl(component_id, 0, 0, component_scope) ;

    // Created component declaration is to be added to the declaration part of this architecture

    if (!_decl_part) _decl_part = new Array(1) ; // Create new decl list, if not exists

    // Add the created component declaration in the _decl_part
    _decl_part->InsertLast(new_decl) ;

    _present_scope = save_scope ; // Pop present scope
    return component_id ; // Return created component identifier
}

unsigned
VhdlArchitectureBody::RemoveComponentDecl(const char *component_name)
{
    if (!component_name) return 0 ; // Nothing to remove

    // Viper 6425: Convert strings to compare to lowercase as VHDL is case
    // independent. Do this unless the name is escaped
    char *lower_cased_component_name = Strings::save(component_name) ;
    if (component_name[0]!='\\' || component_name[Strings::len(component_name)-1]!='\\') {
        (void) Strings::strtolower(lower_cased_component_name) ;
    }

    // Iterate over _decl_part and find component decl named 'component_name'
    unsigned i ;
    VhdlComponentDecl *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        // Continue for decl other than component declaration
        if (!decl || (decl->GetClassId() != ID_VHDLCOMPONENTDECL)) continue ;
        VhdlIdDef *component_id = decl->GetId() ; // Get component id
        if (component_id && Strings::compare(component_id->Name(), lower_cased_component_name)) { // Found desired id
            _decl_part->Remove(i) ; // Remove component decl from declaration list

            // Remove component id from architecture's scope
            if (_local_scope) _local_scope->Undeclare(component_id) ;

            // Delete component declaration
            delete decl ;

            Strings::free(lower_cased_component_name) ;
            return 1 ; // Removed successful
        }
    }
    Strings::free(lower_cased_component_name) ;
    return 0 ; // Fails to remove
}
///////////////////////////////////////////////////////////////////////////////
//  Add declaration to design unit (primary and secondary) and block         //
///////////////////////////////////////////////////////////////////////////////
void
VhdlSecondaryUnit::AddDeclaration(VhdlDeclaration * /*new_decl*/)
{ VERIFIC_ASSERT(0) ; }
void
VhdlArchitectureBody::AddDeclaration(VhdlDeclaration *new_decl)
{
    if (!new_decl) return ; // No declaration to add
    if (!_decl_part) _decl_part = new Array(1) ; // Create declaration array if not exists
    _decl_part->InsertLast(new_decl) ; // Add argument specific declaration to declaration list
}
void
VhdlPackageBody::AddDeclaration(VhdlDeclaration *new_decl)
{
    if (!new_decl) return ; // No declaration to add
    if (!_decl_part) _decl_part = new Array(1) ; // Create declaration array if not exists
    _decl_part->InsertLast(new_decl) ; // Add argument specific declaration to declaration list
}
void
VhdlPrimaryUnit::AddDeclaration(VhdlDeclaration * /*new_decl*/)
{ }
void
VhdlEntityDecl::AddDeclaration(VhdlDeclaration *new_decl)
{
    if (!new_decl) return ; // No declaration to add
    if (!_decl_part) _decl_part = new Array(1) ; // Create declaration array if not exists
    _decl_part->InsertLast(new_decl) ; // Add argument specific declaration to declaration list
}
void
VhdlConfigurationDecl::AddDeclaration(VhdlDeclaration *new_decl)
{
    if (!new_decl) return ; // No declaration to add
    if (!_decl_part) _decl_part = new Array(1) ; // Create declaration array if not exists
    _decl_part->InsertLast(new_decl) ; // Add argument specific declaration to declaration list
}
void
VhdlPackageDecl::AddDeclaration(VhdlDeclaration *new_decl)
{
    if (!new_decl) return ; // No declaration to add
    if (!_decl_part) _decl_part = new Array(1) ; // Create declaration array if not exists
    _decl_part->InsertLast(new_decl) ; // Add argument specific declaration to declaration list
}
void
VhdlBlockStatement::AddDeclaration(VhdlDeclaration *new_decl)
{
    if (!new_decl) return ; // No declaration to add
    if (!_decl_part) _decl_part = new Array(1) ; // Create declaration array if not exists
    _decl_part->InsertLast(new_decl) ; // Add argument specific declaration to declaration list
}
///////////////////////////////////////////////////////////////////////////////
//  (VIPER #2822) Add statement to secondary unit (architecture)             //
///////////////////////////////////////////////////////////////////////////////
void
VhdlSecondaryUnit::AddStatement(VhdlStatement * /*new_stmt*/)
{ VERIFIC_ASSERT(0) ; }
void
VhdlArchitectureBody::AddStatement(VhdlStatement *new_stmt)
{
    if (!new_stmt) return ; // No statement to add
    if (!_statement_part) _statement_part = new Array(1) ; // Create statement array if not exists
    _statement_part->InsertLast(new_stmt) ; // Add argument specific statement to statement list
}
///////////////////////////////////////////////////////////////////////////////
//             Add statement to primary unit (entity)                        //
///////////////////////////////////////////////////////////////////////////////
void
VhdlPrimaryUnit::AddStatement(VhdlStatement * /*new_stmt*/)
{ }
void
VhdlEntityDecl::AddStatement(VhdlStatement *new_stmt)
{
    if (!new_stmt) return ; // No statement to add
    if (!_statement_part) _statement_part = new Array(1) ; // Create statement array if not exists
    _statement_part->InsertLast(new_stmt) ; // Add argument specific statement to statement list
}
///////////////////////////////////////////////////////////////////////////////
//           Add statement to block statement                                //
///////////////////////////////////////////////////////////////////////////////
void
VhdlBlockStatement::AddStatement(VhdlStatement *new_stmt)
{
    if (!new_stmt) return ; // No statement to add
    if (!_statements) _statements = new Array(1) ; // Create statement array if not exists
    _statements->InsertLast(new_stmt) ; // Add argument specific statement to statement list
}
//////////////////////////////////////////////////////////////////////////////
//    (VIPER #2969) Add context clause to design unit                       //
//////////////////////////////////////////////////////////////////////////////
void VhdlDesignUnit::AddContextClause(VhdlDeclaration *clause)
{
    if (!clause) return ; // Nothing to add
    if (!_context_clause) _context_clause = new Array(1) ; // Create context array if not exists
    _context_clause->InsertLast(clause) ; // Add argument specified clause to list
}
unsigned VhdlStatement::ReplaceChildByInternal(Array *stmts, VhdlStatement *old_stmt, Array *new_stmts)
{
    unsigned old_item_pos = 0 ;
    VhdlStatement *stmt ;
    unsigned bfound = 0;
    FOREACH_ARRAY_ITEM(stmts, old_item_pos, stmt) {
        if (stmt == old_stmt) {
            bfound = 1 ;
            stmts->Insert(old_item_pos, 0) ;
            break ;
        }
    }
    if (!bfound || !stmts) return 0;
    unsigned j ;
    /* Store 'new_items' below the old item 'old_stmt'  */
    Array remaining_elements(stmts->Size() - old_item_pos) ;
    for(j = old_item_pos + 1; j < stmts->Size(); j++)
    {
        remaining_elements.Insert(stmts->At(j)) ;
    }
    // Keep null in the position of generate in array only when generate elaboration
    // does not create any block statement.
    if (!new_stmts || !new_stmts->Size()) old_item_pos++ ;

    /* Add the new statements in the statement part */
    FOREACH_ARRAY_ITEM(new_stmts, j, stmt) {
        if (!stmt) continue ;
        stmts->Insert(old_item_pos, stmt) ;
        old_item_pos++ ;
    }
    /* Add the statements below the old item again in the expanded list */
    FOREACH_ARRAY_ITEM(&remaining_elements, j, stmt) {
        if (stmts) stmts->Insert(old_item_pos, stmt) ;
        old_item_pos++ ;
    }
    delete old_stmt ;
    return 1 ;
}
     
// Replace argument specified statement by list of statements
unsigned VhdlBlockStatement::ReplaceChildBy(VhdlStatement *old, Array *new_stmts)
{
    return ReplaceChildByInternal(_statements, old, new_stmts) ;
}
unsigned VhdlArchitectureBody::ReplaceChildBy(VhdlStatement *old, Array *new_stmts)
{
    return VhdlStatement::ReplaceChildByInternal(_statement_part, old, new_stmts) ;
}

