/*
 *
 * [ File Version : 1.190 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // for sprintf

#include "Set.h"
#include "Map.h"
#include "Array.h"
#include "Strings.h"
#include "Message.h"

#include "VhdlName.h" // VhdlExpression.h

#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlDiscreteRange::VhdlDiscreteRange() : VhdlTreeNode() { }
VhdlDiscreteRange::~VhdlDiscreteRange() { }

VhdlRange::VhdlRange(VhdlExpression *left, unsigned dir, VhdlExpression *right)
  : VhdlDiscreteRange(),
    _left(left),
    _dir(dir),
    _right(right)
{ }
VhdlRange::~VhdlRange()
{
    delete _left ;
    delete _right ;
}

VhdlSubtypeIndication::VhdlSubtypeIndication() : VhdlExpression() { }
VhdlSubtypeIndication::~VhdlSubtypeIndication() { }

VhdlExplicitSubtypeIndication::VhdlExplicitSubtypeIndication(VhdlName *res_function, VhdlName *type_mark, VhdlDiscreteRange *range_constraint)
  : VhdlSubtypeIndication(),
    _res_function(res_function),
    _type_mark(type_mark),
    _range_constraint(range_constraint),
    _range_type(0)
{ }
VhdlExplicitSubtypeIndication::~VhdlExplicitSubtypeIndication()
{
    delete _res_function ;
    delete _type_mark ;
    delete _range_constraint ;
}

VhdlExpression *VhdlExplicitSubtypeIndication::GetLeftExpression() const
{
    return (_range_constraint) ? _range_constraint->GetLeftExpression(): 0 ;
}
unsigned VhdlExplicitSubtypeIndication::GetDir() const
{
    return (_range_constraint) ? _range_constraint->GetDir(): 0 ;
}
VhdlExpression * VhdlExplicitSubtypeIndication::GetRightExpression() const
{
    return (_range_constraint) ? _range_constraint->GetRightExpression(): 0 ;
}

VhdlExpression::VhdlExpression() : VhdlDiscreteRange() { }
VhdlExpression::~VhdlExpression() { }

verific_int64 VhdlExpression::EvaluateConstantInteger()
{
    Error("expected a constant integer literal") ;
    return 1 ; // Safe
}

verific_int64
VhdlOperator::EvaluateConstantInteger()
{
    // VIPER #6646
    unsigned function = (_operator) ? _operator->GetOperatorType() : 0 ; // built-in operators

    verific_int64 left = _left ? _left->EvaluateConstantInteger() : 0 ;
    verific_int64 right = _right ? _right->EvaluateConstantInteger() : 0 ;

    unsigned l_s = (left < 0) ;
    unsigned r_s = (right < 0) ;

    verific_int64 result = 0 ;
    switch (function) {
    case VHDL_PLUS :   result = left+right ; break ;
    case VHDL_MINUS :  result = left-right ; break ;
    case VHDL_STAR :   result = left*right ; break ;
    case VHDL_SLASH :  result = right ? left/right : 0 /* error ? */ ; break ;
    case VHDL_rem :    result = right ? left%right : 0 /* error ? */ ; break ;
    case VHDL_mod :
    {
        if (right) {
            result = left%right ;
            if (l_s != r_s) result = result ? right%result : 0 /* error ? */ ;
        }
        break ;
    }
    default:
        Error("expected a constant integer literal") ;
        break ;
    }
    return result ;
}

// This function inserts a explicit ?? operator for cases
// described in the 2008 LRM where impilicit conversion
// from other types to Boolean occurs. These rules are
// define in Section 9.2.9 of VHDL 1076-2008 LRM.
VhdlExpression* VhdlExpression::GetConditionalExpression()
{
     if (!IsVhdl2008() || IsVhdlPsl()) return this ;
     VhdlExpression *condition = this ;
     unsigned op = GetOperatorToken() ;
     Set return_types(POINTER_HASH) ;
     (void) TypeInfer(0,&return_types,VHDL_READ,0) ;
     // If at least one of the inferred type is Boolean
     // then we know that TypeInfer with Boolean will
     // work correctly. Other wise we insert a implicit
     // conditional operator. The operator in marked
     // implicit so that it is not pretty printed. Also
     // if the explicit operator is present do not do anything
     if (return_types.Size() > 0 && op != VHDL_condition) {
         SetIter si ;
         VhdlIdDef *type ;
         FOREACH_SET_ITEM(&return_types, si, &type) {
             if (type == StdType("boolean")) return condition ;
         }
     }
     if (op!=VHDL_condition) {
         condition = new VhdlOperator(this,VHDL_condition,0) ;
         condition->SetImplicit() ;
     }
     return condition ;
}

VhdlOperator::VhdlOperator(VhdlExpression *left, unsigned oper, VhdlExpression *right)
  : VhdlExpression(),
    _left(left),
    _oper(oper),
    _is_implicit(0),
    _unused(0),
    _right(right),
    _operator(0)
{
}
VhdlOperator::~VhdlOperator()
{
    delete _left ;
    delete _right ;
}

VhdlAllocator::VhdlAllocator(VhdlExpression *subtype)
  : VhdlExpression(),
    _subtype(subtype)
{ }
VhdlAllocator::~VhdlAllocator()
{
    delete _subtype ;
}

VhdlAggregate::VhdlAggregate(Array *element_assoc_list)
  : VhdlExpression(),
    _element_assoc_list(element_assoc_list),
    _aggregate_type(0),
    _element_type_list(0)
{
    unsigned i ;
    VhdlDiscreteRange *elem ;
    unsigned is_named = 0 ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, elem) {
        if (!elem) continue ;

        // LRM 7.3.2 : Check that IF there is an 'others' choice, that it is the last one :
        if (elem->IsOthers() && i!=_element_assoc_list->Size()-1) {
            elem->Error("'others' clause must be last, single choice in an aggregate") ;
        }

        // LRM 7.3.2 : Check correct named/positional association usage
        if (elem->IsAssoc()) {
            is_named = 1 ;
        } else if (is_named) {
            elem->Error("positional association cannot follow named association") ;
            break ;
        }
    }
}
VhdlAggregate::~VhdlAggregate()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_element_assoc_list,i,elem) delete elem ;
    delete _element_assoc_list ;
    delete _element_type_list ;
}

VhdlQualifiedExpression::VhdlQualifiedExpression(VhdlName *prefix, VhdlExpression *aggregate)
  : VhdlExpression(),
    _prefix(prefix),
    _aggregate(aggregate)
{ }
VhdlQualifiedExpression::~VhdlQualifiedExpression()
{
    delete _prefix ;
    delete _aggregate ;
}

VhdlAssocElement::VhdlAssocElement(VhdlName *formal_part, VhdlExpression *actual_part)
  : VhdlExpression(),
    _formal_part(formal_part),
    _actual_part(actual_part),
    _from_verilog(0)
{
}
VhdlAssocElement::~VhdlAssocElement()
{
    delete _formal_part ;
    delete _actual_part ;
}

VhdlInertialElement::VhdlInertialElement(VhdlExpression *actual_part)
  : VhdlExpression(),
    _actual_part(actual_part)
{
}
VhdlInertialElement::~VhdlInertialElement()
{
    delete _actual_part ;
}

VhdlRecResFunctionElement::VhdlRecResFunctionElement(VhdlName *record_id_name, VhdlName *record_id_function)
  : VhdlExpression(),
    _record_id_name(record_id_name),
    _record_id_function(record_id_function)
{
}
VhdlRecResFunctionElement::~VhdlRecResFunctionElement()
{
    delete _record_id_name ;
    delete _record_id_function ;
}

VhdlElementAssoc::VhdlElementAssoc(Array *choices, VhdlExpression *expr)
  : VhdlExpression(),
    _choices(choices),
    _expr(expr)
{
    // Check that the others choice is the only one in the choices (LRM 7.3.2)
    //
    if (_choices && _choices->Size()>1) {
        unsigned i ;
        VhdlDiscreteRange *choice ;
        FOREACH_ARRAY_ITEM(_choices, i, choice) {
            if (!choice) continue ;
            if (choice->IsOthers()) {
                choice->Error("'others' clause must be last, single choice in an aggregate") ;
            }
        }
    }
}
VhdlElementAssoc::~VhdlElementAssoc()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_choices,i,elem) delete elem ;
    delete _choices ;
    delete _expr ;
}

VhdlWaveformElement::VhdlWaveformElement(VhdlExpression *value, VhdlExpression *after)
  : VhdlExpression(),
    _value(value),
    _after(after)
{ }
VhdlWaveformElement::~VhdlWaveformElement()
{
    delete _value ;
    delete _after ;
}

VhdlBox::VhdlBox() : VhdlExpression() { }
VhdlBox::~VhdlBox() { }

VhdlDefault::VhdlDefault() : VhdlExpression() { }
VhdlDefault::~VhdlDefault() {}

// Find the resolution function from a discrete range.

VhdlIdDef *VhdlDiscreteRange::FindResolutionFunction() const { return 0 ; }

VhdlIdDef *
VhdlExplicitSubtypeIndication::FindResolutionFunction() const
{
    // Return the resolution function of a subtype indication.
    // Valid after type-inference.
    // Resolved VhdlIdDef will be set in the _res_function field. Just get it :
    if (!_res_function) return 0 ;

    // For record resolution function we will return zero.
    if (_res_function->IsRecordResolutionFunction()) return 0 ;

    return _res_function->FindSingleObject() ;
}

/*
 ******************* TypeInfer ***************************
 * Given a discrete range or expression,
 * Then, find the type(s) it expresses.
 * If expected_type is there, filter out the matches that
 * do not match the expected type.
 *
 ******************* FindObjects **************************
*/

VhdlIdDef *
VhdlExplicitSubtypeIndication::TypeInfer(VhdlIdDef * /*expected_type*/, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    if (!_type_mark) return 0 ;

    if (environment && (environment != VHDL_range) && (environment != VHDL_interfacerange) && (environment != VHDL_ACCESS_SUBTYPE) && (environment != VHDL_recordrange) && (environment != VHDL_subtyperange)) {
        // Something else than a subtype constraint.
        Error("illegal range constraint in expression") ;
    }

    // CHECK ME : Don't do anything with expected type ? Should always be 0 ?

    // Get the type mark
    //_range_type = _type_mark->TypeMark() ;
    // VIPER #4956 : Call TypeInfer instead of TypeMark() to resolve subtype
    // indication as subtype indication can include index constraint (a(1 to 3))
    // but TypeMark only supports type name :
    _range_type = _type_mark->TypeInfer(0,0,VHDL_range,0) ;
    if (!_range_type) return 0 ;

    if (_type_mark->IsLocallyStatic()) {
        _range_type->SetLocallyStaticType() ;
    } else if (_type_mark->IsGloballyStatic()) {
        _range_type->SetGloballyStaticType() ;
    }

    // Check the range constraint with this type as expected type
    if (_range_constraint) {
        // Error will be given on type mismatch
        (void) _range_constraint->TypeInfer(_range_type,0,environment,0) ;
    }

    //   Check the resolution function :
    //   Check that it is a subprogram
    //   Check that the subprogram's return type is type_mark
    //   Check that it has one argument that has an array type who's element type is type_mark
    if (_res_function) {
        // LRM 4.2 : cant have a resolution function for a
        // access or file type
        if (_range_type->IsAccessType() || _range_type->IsFileType()) {
            _res_function->Error("access type or file type constraint cannot have a resolution function") ;
        }

        // LRM 10.5 states that overloading needs to be done based on :
        //    (1) return type
        //    (2) number of parameters
        //    (3) type of parameter
        // We do overloading resolving first :

        // With Vhdl 2008 LRM section 6.3 the resolution func name can be placed
        // in a nesting of parenthesis. The degree of nesting of parentheses
        // indicates how deeply nested in the type structure the resolution
        // function is associated. Two levels indicate that the resolution function
        // is associated with the elements of the elements of the type.

        if (_res_function->IsRecordResolutionFunction()) {
            VhdlIdDef *record_type =  _res_function->TypeInferRecordResFunc(_range_type) ;
            if (record_type) return record_type ;
            if (return_types) (void) return_types->Insert(_range_type->BaseType()) ;
            return _range_type ;
        }

        // element_range_type represents the type of the element of the array to which
        // the resolution function needs to be applied.

        VhdlIdDef *element_range_type = _range_type ;
        VhdlName *res_function = _res_function ;
        while (res_function && res_function->IsArrayResolutionFunction()) {
            if (element_range_type && !element_range_type->IsArrayType()) {
                element_range_type = _range_type ; // Error scenario revert back element_range_type
                _range_type->Error("illegal nesting of resolution indication function") ;
                break ;
            }
            element_range_type = element_range_type ? element_range_type->ElementType() : 0 ;
            res_function = res_function->GetResolutionFunction() ;
        }

        Set res_functions(POINTER_HASH) ;
        (void) _res_function->FindObjects(res_functions,0) ;

        SetIter si ;
        VhdlIdDef *func ;
        FOREACH_SET_ITEM(&res_functions, si, &func) {
            if (res_functions.Size()==1) break ; // overloading resolved. Unique id determined.
            if (!func->IsSubprogram() ||
                (element_range_type && !element_range_type->TypeMatch(func->Type())) ||
                (func->NumOfArgs()!=1)) {
                // Mismatch on return type/num_args.
                (void) res_functions.Remove(func) ;
                continue ;
            }
            VhdlIdDef *arg_type = func->ArgType(0) ;
            if (!arg_type || (element_range_type && !element_range_type->TypeMatch(arg_type->ElementType()))) {
                // Mismatch on argument type
                (void) res_functions.Remove(func) ;
                continue ;
            }
        }

        if (res_functions.Size() == 1) {
            // Unique
            func = (VhdlIdDef*) res_functions.GetLast() ;
        } else {
            // Ambiguity or not declared resolution function :
            // FIX ME : a bit risky to call Name() on resolution function name :
            Error("near %s ; %d visible identifiers match here", _res_function->Name(), res_functions.Size()) ;
            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            VhdlIdDef *id ;
            FOREACH_SET_ITEM_BACK(&res_functions, si, &id) {
                id->Info("%s match for '%s' found here", (first++) ? "another" : "first", id->Name());
            }
            return _range_type ;
        }

        // LRM 2.4 Requirements for resolution functions :
        //    (1) it's a 'pure' function
        //    (2) return type matches
        //    (3) single formal argument (of class constant)
        //    (4) that is a unconstrained array type
        //    (5) with element type matching resolved type
        // We check this here. We don't check (1) yet.

        if (!func->IsSubprogram() || func->NumOfArgs()!=1) {
            Error("%s is not a (resolution) function", func->Name()) ;
            return _range_type ;
        }

        VhdlIdDef *arg_type = func->ArgType(0) ;
        if (!arg_type ||
            (element_range_type && !element_range_type->TypeMatch(func->Type())) ||
            arg_type->Dimension() != 1 ||
            (element_range_type && !element_range_type->TypeMatch(arg_type->ElementType()))) {
            Error("%s is not a valid resolution function for type %s", func->Name(), _range_type->Name()) ;
            return _range_type ;
        }
        // VIPER #2987 : Set resolved resolution function id in _res_function
        _res_function->SetId(func) ;
    }

    if (return_types) (void) return_types->Insert(_range_type->BaseType()) ;
    return _range_type ;
}

VhdlIdDef *
VhdlRange::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    if (!_left) return expected_type ; // something wrong with parse tree

    if (environment && (environment != VHDL_range) && (environment != VHDL_interfacerange) && (environment != VHDL_ACCESS_SUBTYPE) && (environment != VHDL_recordrange) && (environment != VHDL_subtyperange)) {
        // Something else than a range constraint was expected.
        Error("illegal range in expression") ;
    }

    if (!_right) {
        // Infinite range, Infer left expression only.
        return _left->TypeInfer(expected_type, return_types, VHDL_READ, selection_scope) ;
    }

    // In 99% of the cases, this range is called from a subtype indication,
    // and the expected type is know. Left and Right side can then be immediately inferred.

    VhdlIdDef *unique_left_type = expected_type ;
    VhdlIdDef *unique_right_type = expected_type ;

    if (!expected_type) {
        // Check the remaining 1 % where expected type is not known
        // Happens for integer/real type definitions (LRM 3.1)
        // and for generate/for loop range (LRM ...).
        //
        // In that case, ask for left and right side types, and prune down
        // each side's list until we have a single match. Make sure that we use
        // 'implicit' type conversions only when needed.

        Set left_types(POINTER_HASH) ;
        Set right_types(POINTER_HASH) ;

        unique_left_type = _left->TypeInfer(0, &left_types, VHDL_READ,0) ;
        if (left_types.Size()==0) return 0 ; // error was given already
        unique_right_type = _right->TypeInfer(0, &right_types, VHDL_READ,0) ;
        if (right_types.Size()==0) return 0 ; // error was given already

        // Note that LRM 3.1.2 (range used for type definition) states that
        // left and right (integer/real) types
        // do not have to be the same type.
        // Also, if one side is a universal type, implicit conversion happens.

        // First check if universal match might be there.
        unsigned left_has_universal_integer = left_types.GetItem(UniversalInteger()) ? 1 : 0 ;
        unsigned left_has_universal_real = left_types.GetItem(UniversalReal()) ? 1 : 0 ;
        unsigned right_has_universal_integer = right_types.GetItem(UniversalInteger()) ? 1 : 0 ;
        unsigned right_has_universal_real = right_types.GetItem(UniversalReal()) ? 1 : 0 ;

        // Prune down if there are no unique matches :
        SetIter si ;
        VhdlIdDef *left_type, *right_type ;
        VhdlIdDef *potential_match ;
        if (!unique_left_type) {
            // Prune down the left type matches :
            potential_match = 0 ;
            FOREACH_SET_ITEM(&left_types, si, &left_type) {
                // Find potential match (via conversion), if there.
                if ((left_type->IsIntegerType() && right_has_universal_integer) ||
                    (left_type->IsRealType() && right_has_universal_real)) {
                    potential_match = left_type ; // This type would match if conversion applied.
                }
                if (!right_types.GetItem(left_type->BaseType())) {
                    (void) left_types.Remove(left_type) ;
                }
            }
            if (potential_match && left_types.Size()==0) {
                // Insert potential match if no other match found.
                (void) left_types.Insert(potential_match) ;
            }
            if (left_types.Size()==1) {
                unique_left_type = (VhdlIdDef*)left_types.GetLast() ;
            }
        }
        if (!unique_right_type) {
            // Prune down the left type matches :
            potential_match = 0 ;
            FOREACH_SET_ITEM(&right_types, si, &right_type) {
                // Find potential match (via conversion), if there.
                if ((right_type->IsIntegerType() && left_has_universal_integer) ||
                    (right_type->IsRealType() && left_has_universal_real)) {
                    potential_match = right_type ; // This type would match if conversion applied.
                }
                if (!left_types.GetItem(right_type->BaseType())) {
                    (void) right_types.Remove(right_type) ;
                }
            }
            if (potential_match && right_types.Size()==0) {
                // Insert potential match
                (void) right_types.Insert(potential_match) ;
            }
            if (right_types.Size()==1) {
                unique_right_type = (VhdlIdDef*)right_types.GetLast() ;
            }
        }

        if (!unique_left_type || !unique_right_type) {
            Error("types for range bounds are not compatible") ;
            return StdType("integer") ; // something to not error out further up
        }

        // Check that both types are compatible types
        // LRM 3.1 : they don't have to be same type.
        if (unique_left_type->TypeMatch(unique_right_type) ||
            (unique_left_type->IsIntegerType() && unique_right_type->IsIntegerType()) ||
            (unique_left_type->IsRealType() && unique_right_type->IsRealType())) {
        } else {
            Error("types for range bounds are not compatible") ;
            return StdType("integer") ; // something to not error out further up
        }
    }

    // Now call the type-inference 'hard' on left an right side,
    // with the expected type.

    // Viper 5298. Pass VHDL_interfaceread instead of VHDL_read to
    // catch if it uses a signal defined in the same interface
    unsigned left_env = VHDL_READ ;
    if (environment == VHDL_interfacerange) {
        left_env = VHDL_interfaceread ;
    }
    // VIPER #8148: Pass VHDL_rangebound instead of VHDL_read to
    // catch if it uses signal
    if (environment == VHDL_subtyperange || environment == VHDL_ACCESS_SUBTYPE) {
        // Environment VHDL_rangebound represents range bound used in a subtype indication
        left_env = VHDL_subtyperangebound ;
    }
    unsigned right_env = VHDL_READ ;
    if (environment == VHDL_interfacerange) {
        right_env = VHDL_interfaceread ;
    }
    if (environment == VHDL_subtyperange || environment == VHDL_ACCESS_SUBTYPE) {
        // Environment VHDL_rangebound represents range bound used in a subtype indication
        right_env = VHDL_subtyperangebound ;
    }
    (void) _left->TypeInfer(unique_left_type, 0, left_env, 0) ;
    (void) _right->TypeInfer(unique_right_type, 0, right_env, 0) ;

    if (return_types) {
        // Not sure if this actually happens (asking for types from a range).
        (void) return_types->Insert(unique_left_type->BaseType()) ;
        (void) return_types->Insert(unique_right_type->BaseType()) ;
    }

    // It should not matter which type (left or right) we return.
    // LRM 3.2.1 states that left and right don't need to be of same type.
    return unique_left_type ;
}

VhdlIdDef *
VhdlBox::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned environment, VhdlScope * /*selection_scope*/)
{
    // CHECK ME : I think <> is only allowed with a type mark ? Do we check that ?

    // Any way, it's only allowed in a (index) subtype indication :
    if (environment && (environment != VHDL_range) && (environment != VHDL_interfacerange) && (environment != VHDL_subtyperange) && (environment != VHDL_ACCESS_SUBTYPE) && (environment != VHDL_recordrange)) {
        Error("illegal <> in expression") ;
    }

    return expected_type ;
}

VhdlIdDef *
VhdlAssocElement::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    // VIPER #6645 : Produce warning if inertial is used in assoc element other than in port map
    if ((environment != VHDL_port) && IsInertial()) {
        Warning("keyword inertial can only be used in port map") ;
    }
    // formal-actual association in an association list.
    // FIX ME : what to do with the formal part ?
    return (_actual_part) ? _actual_part->TypeInfer(expected_type, return_types, environment,0) : 0 ;
}

VhdlIdDef *
VhdlInertialElement::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    // VIPER #6645 : Produce warning if inertial is used in assoc element other than in port map
    if (environment != VHDL_port) {
        Warning("keyword inertial can only be used in port map") ;
    }
    // formal-actual association in an association list.
    // FIX ME : what to do with the formal part ?
    return (_actual_part) ? _actual_part->TypeInfer(expected_type, return_types, environment,0) : 0 ;
}

VhdlIdDef *
VhdlRecResFunctionElement::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    // formal-actual association in an association list.
    // FIX ME : what to do with the formal part ?
    return (_record_id_function) ? _record_id_function->TypeInfer(expected_type, return_types, environment,0) : 0 ;
}

VhdlIdDef *
VhdlElementAssoc::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    // choices-expression element in an aggregate.
    // choices are handled in ElementAssoc::TypeInferChoices
    return (_expr) ? _expr->TypeInfer(expected_type, return_types, environment, selection_scope) : 0 ;
}

// Viper 1493(4400): This function returns true if type id is non array and the expression
// is an operator ampersand or a String literal. As both of them returns an array type
unsigned
VhdlOperator::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;

    // Ampersand wil always return an array type:
    if ((_oper == VHDL_AMPERSAND) && !type_id->IsArrayType()) return 1 ;

    return 0 ;
}

unsigned
VhdlAggregate::CannotMatchType(VhdlIdDef *type_id) const
{
    if (!type_id) return 0 ;

    // Aggregate must always be a array or record type :
    if (!type_id->IsArrayType() && !type_id->IsRecordType()) return 1 ;

    return 0 ;
}

// Populate operator_map with the processing operator and all
// its left and right child tree along with their visible operators.
unsigned
VhdlOperator::FindVisibleOperators(Map &operator_map)
{
    // Since there are most likely about a dozen or more operators by any name,
    // set the initial entry size of the Set to a higher number than default.
    // This will reduce re-hashing, saving time and memory fragmentation.
    Set *visible_operators = 0 ;
    Array stack(1) ;
    stack.InsertLast(this) ;
    while(stack.Size()!=0) {
        VhdlExpression *expr = (VhdlExpression *)stack.RemoveLast() ;
        if (!expr) continue ;
        VhdlExpression *right = expr->GetRightExpression() ;
        if (right && right->IsVhdlOperator()) stack.InsertLast(right) ;
        VhdlExpression *left = expr->GetLeftExpression() ;
        if (left && left->IsVhdlOperator()) stack.InsertLast(left) ;
        unsigned oper_token = expr->GetOperatorToken() ;
        visible_operators = new Set(POINTER_HASH, 27) ;
        (void) _present_scope->FindAll(OperatorName(oper_token), *visible_operators) ;
        (void) operator_map.Insert(expr, visible_operators) ;
    }
    return 0 ;
}

// Populate new_operator_map with the processing operator,
// its left/right expressions along with their visible operators.
unsigned
VhdlOperator::FindVisibleOperatorsFromMap(Map &operator_map, Map &new_operator_map)
{
    Set *visible_operators = 0 ;
    MapItem *item = 0 ;
    // Collect this expression:
    item = operator_map.GetItem(this) ;
    if (item) visible_operators = item ? (Set *)item->Value() : 0 ;
    (void) new_operator_map.Insert(this, visible_operators) ;

    // Collect left expression:
    VhdlExpression *left = GetLeftExpression() ;
    if (left && left->IsVhdlOperator()) {
        item = operator_map.GetItem(left) ;
        if (item) visible_operators = item ? (Set *)item->Value() : 0 ;
        (void) new_operator_map.Insert(left, visible_operators) ;
    }
    // Collect right expression:
    VhdlExpression *right = GetRightExpression() ;
    if (right && right->IsVhdlOperator()) {
        item = operator_map.GetItem(right) ;
        if (item) visible_operators = item ? (Set *)item->Value() : 0 ;
        (void) new_operator_map.Insert(right, visible_operators) ;
    }

    return 0 ;
}

// Prune the visible operator set for each operator in bottom up order, depending upon both the return types and checking its left and right expressions.
// Store the operators that have visible operators size 1 after pruning for further processing its child tree.
unsigned
VhdlOperator::PruneVisibleOperators(Map &orig_operator_map, Map &operator_map, Map &expected_types_map, Set &already_visited, unsigned environment, VhdlScope *selection_scope)
{
    Set operators_set(POINTER_HASH) ; // Store the operators that has visible operators 1.
    VhdlIdDef *oper, *oper_type ;
    VhdlIdDef *the_match = 0 ;
    VhdlIdDef *operator_id = 0 ;
    MapIter mi ;
    VhdlOperator *this_oper ;
    Set *visible_operators = 0 ;
    // Prune visible operators in bottom up order:
    FOREACH_MAP_ITEM_BACK(&operator_map, mi, &this_oper, &visible_operators) {
        VERIFIC_ASSERT(visible_operators) ;

        VhdlIdDef *expected_operator_type = 0 ;
        MapItem *item = expected_types_map.GetItem(this_oper) ;
        if (item) expected_operator_type = (VhdlIdDef *)item->Value() ;

        operator_id = this_oper->GetOperator() ;
        if (operator_id) {
            if (!already_visited.Insert(operator_id)) continue ;
            // Previously, we determined a unique operator here.
            // This means that tree under this operator call is successfully type-inferred already.
            // So, just check, and return.
            oper_type = operator_id->Type() ;
            if (expected_operator_type && !expected_operator_type->TypeMatch(oper_type)) {
                if ((oper_type && oper_type->IsUniversalInteger() && expected_operator_type->IsIntegerType()) ||
                    (oper_type && oper_type->IsUniversalReal() && expected_operator_type->IsRealType())) {
                    // implicit conversion
                } else {
                    this_oper->Error("type mismatch at operator %s ; expected type %s", OperatorName(this_oper->GetOperatorToken()), expected_operator_type->Name()) ;
                }
            }
            continue ;
        }

        the_match = 0 ;
        VhdlExpression *left = this_oper->GetLeftExpression() ;
        VhdlExpression *right = this_oper->GetRightExpression() ;

        // Rule out the ones with incorrect number of arguments, and/or with
        // incorrect return type (if thats given).
        SetIter si ;
        FOREACH_SET_ITEM(visible_operators, si, &oper) {
            oper_type = oper->Type() ;
            if (expected_operator_type) {
                if (!expected_operator_type->TypeMatch(oper_type)) {
                    // Mismatch on return type
                    if ((oper_type->IsUniversalInteger() && expected_operator_type->IsIntegerType()) ||
                        (oper_type->IsUniversalReal() && expected_operator_type->IsRealType())) {
                        // This guy could match if we can convert universal type
                        // Don't eliminate this, since it might be the only match in the end.
                        // Don't choose it either, since it has to pass the arguments test still.
                    } else {
                        (void) visible_operators->Remove(oper) ;
                        continue ;
                    }
                }
            }

            // Viper 1493 (4400) : Eliminate operators which have left expression not matching operator's left argument's type
            // and  which have right expression not matching operator's right argument's type
            if ((left && left->CannotMatchType(oper->ArgType(0))) ||
                (right && right->CannotMatchType(oper->ArgType(1)))) {
                (void) visible_operators->Remove(oper) ;
                continue ;
            }

            the_match = oper ; // In case there is only one match
        }

        // Viper 5298. Pass VHDL_interfaceread instead of VHDL_read to
        // catch if it uses a signal defined in the same interface
        unsigned arg_env = VHDL_READ ;
        if (environment == VHDL_interfacerange) {
            arg_env = VHDL_interfaceread ;
        } else if (environment == VHDL_interfaceread) {
            arg_env = VHDL_interfaceread ;
        }
        // VIPER #8148: Pass VHDL_rangebound instead of VHDL_READ to
        // catch if it uses signal
        if (environment == VHDL_subtyperange || environment == VHDL_subtyperangebound || environment == VHDL_ACCESS_SUBTYPE) {
            // Environment VHDL_rangebound represents range bound used in a subtype indication
            arg_env = VHDL_subtyperangebound ;
        }

        // If we are unique already (by return type), then take a short-cut and infer left and right with known argument types.
        // Also we store this operator in operators_set for iterate again.
        if (visible_operators->Size()==1) {
            VERIFIC_ASSERT(the_match) ;

            // If single operator survives, set it as _operator
            this_oper->SetOperator(the_match) ;

            // Issue 2148 : If this match is an alias to a subprogram, set the subprogram as _prefix_id.
            // That will 'resolve' non-object aliases, and simplify later routines (elaboration etc).
            // Do same for function calls (VhdlIndexedName::TypeInfer).
            if (the_match->IsAlias()) {
                this_oper->SetOperator(the_match->GetTargetId()) ;
            }

            VhdlIdDef *left_type = the_match->ArgType(0) ;
            VhdlIdDef *right_type = the_match->ArgType(1) ;

            // VIPER 7081 : Pre-defined operators with an array argument will always have an unconstrained formal.
            // In such cases, check if the actual is an aggregate with an 'others' clause :
            if (the_match->IsPredefinedOperator()) {
                if ((left && left->IsOthersAggregate() && left_type && left_type->IsArrayType()) ||
                    (right && right->IsOthersAggregate() && right_type && right_type->IsArrayType()))
                {
                    this_oper->Error("argument of operator %s cannot be an OTHERS aggregate", OperatorName(this_oper->GetOperatorToken())) ;
                }
            }

            // Infer left and right side, with the match' argument types.
            // Pass in selection scope since this can be an expression in an actual of a binding indication.
            // Call 'TypeInfer()' on the experessions other than operators.
            if (left && !left->IsVhdlOperator()) {
                (void) left->TypeInfer(left_type, 0, arg_env, selection_scope) ;
            }
            if (right && !right->IsVhdlOperator()) {
                (void) right->TypeInfer(right_type, 0, arg_env, selection_scope) ;
            }

            // We have exact match: so we have the return type of both the
            // left and right experssions of the operator.
            // So collect the return types in the map 'expected_types_map'.
            if (left && left->IsVhdlOperator()) (void) expected_types_map.Insert(left, left_type) ;
            if (right && right->IsVhdlOperator()) (void) expected_types_map.Insert(right, right_type) ;

            // Collect the processing operator for further processing.
            (void) operators_set.Insert(this_oper) ;
            continue ;
        }

        // Size of visible operator is not 1:
        // Match each operator against argument and expected type
        the_match = 0 ;
        VhdlIdDef *arg_type ;
        Set left_arg_types ;
        Set right_arg_types ;
        if (left) {
            if (!left->IsVhdlOperator()) {
                (void) left->TypeInfer(0, &left_arg_types, arg_env, selection_scope) ;
            } else {
                Set *visible_ops = (Set *)operator_map.GetValue(left) ;
                VhdlIdDef *op ;
                FOREACH_SET_ITEM(visible_ops, si, &op) {
                    (void) left_arg_types.Insert(op->BaseType()) ;
                }
            }
        }
        if (!left_arg_types.Size()) {
            if (Message::ErrorCount()) {
                operator_map.Reset() ;
                return 0 ;
            }
        }
        if (right) {
            if (!right->IsVhdlOperator()) {
                (void) right->TypeInfer(0, &right_arg_types, arg_env, selection_scope) ;
            } else {
                Set *visible_ops = (Set *)operator_map.GetValue(right) ;
                VhdlIdDef *op ;
                FOREACH_SET_ITEM(visible_ops, si, &op) {
                    (void) right_arg_types.Insert(op->BaseType()) ;
                }
            }
        }
        if (!right_arg_types.Size()) {
            if (Message::ErrorCount()) {
                operator_map.Reset() ;
                return 0 ;
            }
        }

        // First check if universal match might be there.
        unsigned left_has_universal_integer = 0 ;
        unsigned left_has_universal_real = 0 ;
        unsigned right_has_universal_integer = 0 ;
        unsigned right_has_universal_real = 0 ;

        left_has_universal_integer = left_arg_types.GetItem(UniversalInteger()) ? 1 : 0 ;
        left_has_universal_real = left_arg_types.GetItem(UniversalReal()) ? 1 : 0 ;

        right_has_universal_integer = right_arg_types.GetItem(UniversalInteger()) ? 1 : 0 ;
        right_has_universal_real = right_arg_types.GetItem(UniversalReal()) ? 1 : 0 ;

        unsigned first_time = 1 ;
        Array potential_matches ; // Keep track of operators that only match with a type-conversion.
        VhdlIdDef *potential_match = 0 ;
        FOREACH_SET_ITEM(visible_operators, si, &oper) {
            potential_match = 0 ;
            if (left) {
                // Check if oper->ArgType(0) is in the left arguments
                arg_type = oper->ArgType(0) ;
                if (!arg_type) continue ; // num of args must have been tested already
                if (!left_arg_types.GetItem(arg_type->BaseType())) {
                    // Implicit conversion check (implicitly convert argument to the operator argument type :
                    // Issue 3013 : Do NOT implicitly convert a universal type to an access type argument.
                    if ((left_has_universal_integer && arg_type->IsIntegerType() && !arg_type->IsAccessType()) ||
                        (left_has_universal_real && arg_type->IsRealType() && !arg_type->IsAccessType())) {
                        // implicit conversion. Don't choose yet, since it needs to pass other argument test
                        potential_match = oper ;
                    } else if (arg_type->IsAccessType() && left->IsNull()) {
                        // VIPER 3184 : argument is NULL literal and operator has access type argument. That matches by implicit conversion (of NULL).
                        potential_match = oper ;
                    } else {
                        (void) visible_operators->Remove(oper) ;
                        continue ;
                    }
                }
            }
            if (right) {
                arg_type = oper->ArgType(1) ;
                if (!arg_type) continue ; // num of args must have been tested already
                if (!right_arg_types.GetItem(arg_type->BaseType())) {
                    // Implicit conversion check (implicitly convert argument to the operator argument type :
                    // Issue 3013 : Do NOT implicitly convert a universal type to an access type argument.
                    if ((right_has_universal_integer && arg_type->IsIntegerType() && !arg_type->IsAccessType()) ||
                        (right_has_universal_real && arg_type->IsRealType() && !arg_type->IsAccessType())) {
                        // implicit conversion. Don't choose yet, since it needs to pass return type test
                        potential_match = oper ;
                    } else if (arg_type->IsAccessType() && right->IsNull()) {
                        // VIPER 3184 : argument is NULL literal and operator has access type argument. That matches by implicit conversion (of NULL).
                        potential_match = oper ;
                    } else {
                        (void) visible_operators->Remove(oper) ;
                        continue ;
                    }
                }
            }

            // add check for potential match of return type too
            if (expected_operator_type) {
                oper_type = oper->Type() ;
                if (!oper_type) continue ; // return type should have been tested already..?
                if (!expected_operator_type->TypeMatch(oper_type)) {
                    // Mismatch on return type
                    if ((oper_type->IsUniversalInteger() && expected_operator_type->IsIntegerType() && !oper_type->IsAccessType()) ||
                        (oper_type->IsUniversalReal() && expected_operator_type->IsRealType() && !oper_type->IsAccessType())) {
                        // This guy could match if we can convert universal type
                        // Remember it for if there is no explicit other match
                        // Don't throw it out yet. It has to pass the arguments test too.
                        potential_match = oper ;
                    } else {
                        (void) visible_operators->Remove(oper) ;
                        continue ;
                    }
                }
            }

            // Here, this match survived
            if (potential_match) {
                // It survived, but some type conversion is needed.
                (void) visible_operators->Remove(oper) ;

                // Remove it from exact matches.
                potential_matches.InsertLast(potential_match) ;
                continue ;
            }

            // FIX ME : Scoping (FindAll) does not eliminate homographs (outer decl hidden
            // by inner homograph). So, do that here.
            // Delete the last one, since that is furthest away in the scope.
            if (the_match && oper->IsHomograph(the_match)) {
                if (the_match->IsPredefinedOperator()) {
                    // The existing match (the first mentioned operator) is a predefined one.
                    // That means that someone overloaded a predefined operator.
                    // Language defines that that is not allowed, unless both are in the
                    // same package. But that is already taken care of at declaration time.
                    // So, we should allow this (so that there are two matches).
                    //
                    // BUT : Synopsys overloads the relational operators for std_logic_vector in
                    // another package (std_logic_unsigned/signed), and expect the overloaded
                    // form to prevail over the predefined operator form. That is NOT according
                    // to language rules (both should be visible or invisible).
                    // But any way, try and comply with that here.
                    // Eliminate the match, (and keep the new operator).
                    (void) visible_operators->Remove(the_match) ;
                    the_match = oper ;
                } else {
                    // GIVEN : Locally declared ids always precedes externally included ids.
                    // If previous match is not locally declared, then we have two or more
                    // homographs included via use clauses.  Give an error here appropriately. (VIPER #1311)
                    if (!oper->IsPredefinedOperator() && _present_scope && !_present_scope->IsDeclaredHere(the_match, 1 /*look in upper/extended*/)) {
                        if (first_time) {
                            this_oper->Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", oper->Name()) ;
                            the_match->Info("%s is declared here", oper->Name()) ;
                            first_time = 0 ;
                        }
                        oper->Info("another match is here") ;
                    }
                    // Delete the new one (it is declared later than existing match).
                    (void) visible_operators->Remove(oper) ;
                }
                continue ;
            }

            the_match = oper ;
        }

        if (expected_operator_type && visible_operators->Size()>1 && !IsVhdl87()) {
            // VIPER 2982 : if the operator is still ambiguous, and and we have a 'hard' inference test (meaning we would error out below)
            // and an argument is a bit-string-literal, and we are currently in 93 mode,
            // then we can try to eliminate the operators that do NOT have a bit-element type in that argument.
            unsigned something_done = 0 ;
            if (left && left->GetClassId()==ID_VHDLBITSTRINGLITERAL) {
                FOREACH_SET_ITEM(visible_operators, si, &oper) {
                    arg_type = oper->ArgType(0) ;
                    if (arg_type && arg_type->ElementType() && !arg_type->ElementType()->TypeMatch(StdType("bit"))) {
                        (void) visible_operators->Remove(oper) ;
                        something_done = 1 ;
                    }
                }
            }
            if (right && right->GetClassId()==ID_VHDLBITSTRINGLITERAL) {
                FOREACH_SET_ITEM(visible_operators, si, &oper) {
                    arg_type = oper->ArgType(1) ;
                    if (arg_type && arg_type->ElementType() && !arg_type->ElementType()->TypeMatch(StdType("bit"))) {
                        (void) visible_operators->Remove(oper) ;
                        something_done = 1 ;
                    }
                }
            }
            // If, after this, some operators were removed due to the bit-string literal, then we can give the 'ambiguity' warning :
            if (something_done) {
                this_oper->Warning("argument to operator call %s is ambiguous. Use qualified expression or switch to 87 mode",OperatorName(this_oper->GetOperatorToken())) ;
            }
        }

        if (visible_operators->Size()==0) {
            // There are no unique matches.
            // Use the potential matches instead :
            // VIPER 4164 : If there are multiple potential matches (all requiring implicit conversions),
            // and this is a 'hard' type-infer call (no more context info available),
            // then rule out the ones that return a universal type.
            // Otherwise, choose all of them.
            unsigned i ;
            FOREACH_ARRAY_ITEM(&potential_matches, i, potential_match) {
                oper_type = potential_match->Type() ;
                if (!oper_type) continue ; // 0-pointer check.
                if ((oper_type->IsUniversalInteger() || oper_type->IsUniversalReal())) {
                    // Throw this out if there are more potential_matches to come, or already something in 'visible' types.
                    if (visible_operators->Size() || (i<potential_matches.Size()-1)) continue ;
                }
                // Otherwise, insert in visible types.
                (void) visible_operators->Insert(potential_match) ;
                // Normally, this should be enough (resolving potential operators). But :
                // NOTE : because we added a (non-LRM) 'mod' operator (REAL-REAL->REAL) for VIPER 2131,
                // there are now certain cases where homograph  operators 'potentially' match (such as mod operation for VIPER 4679).
                // To accept these as legal, we simply pick the first one if 'expected' type is set :
                if (expected_operator_type) break ; // Only one gets inserted.
            }
        }

        // Done with type inference on this node

        // If we have no expected_type, ie this operator is still not resolved, and have multiple matches,
        // then continue, we will be called again.
        if (!expected_operator_type && visible_operators->Size()>1) {
            // Don't error out ! This is just a dress-rehearsal. Context needed for type-resolving, we will be called again.
            continue ;
        }

        // Here, we are in final call (no request for all matching types).

        // If there is not exactly one match, then error out :
        if (visible_operators->Size()!=1) {
            // Error out (this can be downgraded to warning by user, and we will still pick one below)
            this_oper->Error("%d definitions of operator %s match here", visible_operators->Size(), OperatorName(this_oper->GetOperatorToken())) ;

            // VIPER #2025 - List locations of visible identifiers
            unsigned first = 0 ;
            FOREACH_SET_ITEM_BACK(visible_operators, si, &oper) {
                oper->Info("%s match for '%s' found here", (first++) ? "another" : "first", oper->Name());
            }
        }

        // If there are 0 matches now, nothing we can do.
        if (visible_operators->Size()==0) { // error given, cannot return anything.
            operator_map.Reset() ;
            return 0 ;
        }

        // VIPER 3109 : always pick one, even if multiple matches.
        // If single operator survives, set it as _operator
        the_match = (VhdlIdDef*)visible_operators->GetLast() ;
        VERIFIC_ASSERT(the_match) ;
        this_oper->SetOperator(the_match) ;

        // Issue 2148 : If this match is an alias to a subprogram, set the subprogram as _prefix_id.
        // That will 'resolve' non-object aliases, and simplify later routines (elaboration etc).
        // Do same for function calls (VhdlIndexedName::TypeInfer).
        if (the_match->IsAlias()) {
            this_oper->SetOperator(the_match->GetTargetId()) ;
        }

        VhdlIdDef *left_type = the_match->ArgType(0) ;
        VhdlIdDef *right_type = the_match->ArgType(1) ;

        // VIPER 7081 : Pre-defined operators with an array argument will always have an unconstrained formal.
        // In such cases, check if the actual is an aggregate with an 'others' clause :
        if (the_match->IsPredefinedOperator()) {
            if ((left && left->IsOthersAggregate() && left_type && left_type->IsArrayType()) ||
                (right && right->IsOthersAggregate() && right_type && right_type->IsArrayType()))
            {
                this_oper->Error("argument of operator %s cannot be an OTHERS aggregate", OperatorName(the_match->GetOperatorType())) ;
            }
        }

        // Infer left and right hard now, with argument type of the operator
        if (left && !left->IsVhdlOperator()) {
            (void) left->TypeInfer(left_type, 0, VHDL_READ, selection_scope) ;
        }
        if (right && !right->IsVhdlOperator()) {
            (void) right->TypeInfer(right_type, 0, VHDL_READ, selection_scope) ;
        }

        if (left && left->IsVhdlOperator()) (void) expected_types_map.Insert(left, left_type) ;
        if (right && right->IsVhdlOperator()) (void) expected_types_map.Insert(right, right_type) ;

        // Collect the processing operator for further processing.
        (void) operators_set.Insert(this_oper) ;
    }

    // Reset operator_map and then populate that map with the ithis operator and
    // its left(vhdl operator),  right(vhdl operator) expressions along with their visible operators.
    operator_map.Reset() ;
    SetIter si ;
    FOREACH_SET_ITEM(&operators_set, si, &this_oper) {
        (void) this_oper->FindVisibleOperatorsFromMap(orig_operator_map, operator_map) ;
    }

    return 0 ;
}

// VIPER #8170: Iterative routine of typeinfer of vhdl operators.
// Algorithm:
// - Collect all the visible operators of each operator in a map.
//     For operator (a+b)*(c-d), we collect in map operator_map: i)(a+b)*(c-d), ii) (a+b), iii) (c-d)
// - Prune visible operators in top to bottom order for each operator:
//      - Prune visible operators set depending upon the number of arguments and expected types.
//      - If size of visible operators is 1, then collect expected types of its left/right arguments
//      from the the processing operator.
// - Then prune visible operators in bottom up order:
//      - For each operator prune the visible operators.
//      - If the size of visible opereators of processing operator is 1, then go for processing
//      its left and right expression again for further pruning.
VhdlIdDef *
VhdlOperator::TypeInferIterative(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    // VIPER #8085 : Operator is not valid as subtype indication
    if (environment == VHDL_interfacerange || environment == VHDL_range || environment == VHDL_recordrange || environment == VHDL_subtyperange) Error("range expression expected here") ;
    // Type - inference of operators.
    // This is general : could be 0,1 or 2 argument operator calls.

    VhdlIdDef *oper, *oper_type ;
    VhdlIdDef *operator_id = GetOperator() ;
    if (operator_id) {
        // Previously, we determined a unique operator here.
        // This means that tree under this operator call is successfully type-inferred already.
        // So, just check, and return.
        oper_type = operator_id->Type() ;
        if (expected_type && !expected_type->TypeMatch(oper_type)) {
            if ((oper_type && oper_type->IsUniversalInteger() && expected_type->IsIntegerType()) ||
                (oper_type && oper_type->IsUniversalReal() && expected_type->IsRealType())) {
                // implicit conversion
            } else {
                Error("type mismatch at operator %s ; expected type %s", OperatorName(GetOperatorToken()), expected_type->Name()) ;
            }
        }
        if (return_types && oper_type) (void) return_types->Insert(oper_type->BaseType()) ;
        return oper_type ;
    }

    // Need environment check to make sure LHS is a valid target (VIPER #1876)
    if ((environment == VHDL_VARUPDATE) || (environment == VHDL_SIGUPDATE)) {
        Error("illegal target for assignment") ;
    }

    // Collect all visible_operators in a map(operator vrs visible_operators)
    Map operator_map (POINTER_HASH) ;
    // Find all visible operators of this type
    (void) FindVisibleOperators(operator_map) ;

    // Prune visible operators in top to bottom order: collect the expected types of the left and right
    // operator from the present operator if its visible operator size is 1 and store them in map 'expected_types_map'.
    VhdlIdDef *the_match = 0 ;
    Map expected_types_map (POINTER_HASH) ; // operator vrs its expected type set
    (void) expected_types_map.Insert(this, expected_type) ;
    MapIter mi ;
    VhdlOperator *this_oper ;
    Set *visible_operators = 0 ;
    FOREACH_MAP_ITEM(&operator_map, mi, &this_oper, &visible_operators) {
        VERIFIC_ASSERT(visible_operators) ;

        the_match = 0 ;
        VhdlIdDef *expected_operator_type = 0 ;
        MapItem *item = expected_types_map.GetItem(this_oper) ;
        if (item) expected_operator_type = (VhdlIdDef *)item->Value() ;

        VhdlExpression *left = this_oper->GetLeftExpression() ;
        VhdlExpression *right = this_oper->GetRightExpression() ;

        unsigned num_args = (left) ? ((right) ? 2 : 1) : ((right) ? 1 : 0) ;

        // Rule out the ones with incorrect number of arguments, and/or with
        // incorrect return type (if thats given).
        SetIter si ;
        FOREACH_SET_ITEM(visible_operators, si, &oper) {
            oper_type = oper->Type() ;
            if (!oper_type) {
                // Probably error occurred earlier
                (void) visible_operators->Remove(oper) ;
                continue ;
            }

            // Check if oper has the right amount of arguments
            if (oper->NumOfArgs() != num_args) {
                (void) visible_operators->Remove(oper) ;
                continue ;
            }

            if (expected_operator_type) {
                if (!expected_operator_type->TypeMatch(oper_type)) {
                    // Mismatch on return type
                    if ((oper_type->IsUniversalInteger() && expected_operator_type->IsIntegerType()) ||
                        (oper_type->IsUniversalReal() && expected_operator_type->IsRealType())) {
                        // This guy could match if we can convert universal type
                        // Don't eliminate this, since it might be the only match in the end.
                        // Don't choose it either, since it has to pass the arguments test still.
                    } else {
                        (void) visible_operators->Remove(oper) ;
                        continue ;
                    }
                }
            }

            // Viper 1493 (4400) : Eliminate operators which have left expression not matching operator's left argument's type
            // and  which have right expression not matching operator's right argument's type
            if ((left && left->CannotMatchType(oper->ArgType(0))) ||
                (right && right->CannotMatchType(oper->ArgType(1)))) {
                (void) visible_operators->Remove(oper) ;
                continue ;
            }

            the_match = oper ; // In case there is only one match
        }

        // Viper 5298. Pass VHDL_interfaceread instead of VHDL_read to
        // catch if it uses a signal defined in the same interface
        unsigned arg_env = VHDL_READ ;
        if (environment == VHDL_interfacerange) {
            arg_env = VHDL_interfaceread ;
        } else if (environment == VHDL_interfaceread) {
            arg_env = VHDL_interfaceread ;
        }
        // VIPER #8148: Pass VHDL_rangebound instead of VHDL_READ to
        // catch if it uses signal
        if (environment == VHDL_subtyperange || environment == VHDL_subtyperangebound || environment == VHDL_ACCESS_SUBTYPE) {
            // Environment VHDL_rangebound represents range bound used in a subtype indication
            arg_env = VHDL_subtyperangebound ;
        }

        // If we are unique already (by return type), then take a
        // short-cut and infer left and right with known argument types.
        // For that set 'iterate_again' flag, so that we come to infer its left & right
        // experession with their expected argument types.
        if (visible_operators->Size()==1) {
            VERIFIC_ASSERT(the_match) ;

            // If single operator survives, set it as _operator
            this_oper->SetOperator(the_match) ;

            // Issue 2148 : If this match is an alias to a subprogram, set the subprogram as _prefix_id.
            // That will 'resolve' non-object aliases, and simplify later routines (elaboration etc).
            // Do same for function calls (VhdlIndexedName::TypeInfer).
            if (the_match->IsAlias()) {
                this_oper->SetOperator(the_match->GetTargetId()) ;
            }

            VhdlIdDef *left_type = the_match->ArgType(0) ;
            VhdlIdDef *right_type = the_match->ArgType(1) ;

            // VIPER 7081 : Pre-defined operators with an array argument will always have an unconstrained formal.
            // In such cases, check if the actual is an aggregate with an 'others' clause :
            if (the_match->IsPredefinedOperator()) {
                if ((left && left->IsOthersAggregate() && left_type && left_type->IsArrayType()) ||
                    (right && right->IsOthersAggregate() && right_type && right_type->IsArrayType()))
                {
                    this_oper->Error("argument of operator %s cannot be an OTHERS aggregate", OperatorName(the_match->GetOperatorType())) ;
                }
            }

            // Infer left and right side, with the match' argument types
            // Since operators are always function, their argument is always a READ..
            // Pass in selection scope since this can be an expression in an actual of a binding indication.
            // Call 'TypeInfer()' on the experessions other than operators.
            if (left && !left->IsVhdlOperator()) {
                (void) left->TypeInfer(left_type, 0, arg_env, selection_scope) ;
            }
            if (right && !right->IsVhdlOperator()) {
                (void) right->TypeInfer(right_type, 0, arg_env, selection_scope) ;
            }

            // We have exact match: so we have the return type of both the
            // left and right experssions of the operator.
            // So collect the return types in the map 'expected_types_map' only for vhdl operators.
            if (left && left->IsVhdlOperator()) (void) expected_types_map.Insert(left, left_type) ;
            if (right && right->IsVhdlOperator()) (void) expected_types_map.Insert(right, right_type) ;
        }
    }

    Set already_visited(POINTER_HASH) ; // set of operators for keep tracking of already visited resolved operators
    unsigned iterate_again = 1 ;
    Map new_operator_map(POINTER_HASH) ;
    FOREACH_MAP_ITEM(&operator_map, mi, &this_oper, &visible_operators) (void) new_operator_map.Insert(this_oper, visible_operators) ;

    while(iterate_again) {
        iterate_again = 0 ;
        (void) PruneVisibleOperators(operator_map, new_operator_map, expected_types_map, already_visited, environment, selection_scope) ; // Prune visible_operators
        if (new_operator_map.Size()) {
            iterate_again = 1 ;
        }
    }

    // Now we have pruned all the operators
    the_match = 0 ;
    MapItem *item = operator_map.GetItem(this) ;
    if (item) visible_operators = (Set *)item->Value() ;
    VERIFIC_ASSERT(visible_operators) ;
    the_match = (VhdlIdDef *)visible_operators->GetLast() ;

    SetIter si ;
    // If there is not exactly one match, then error out :
    if (!expected_type && !return_types && visible_operators->Size()!=1) {
        // Error out (this can be downgraded to warning by user, and we will still pick one below)
        Error("%d definitions of operator %s match here", visible_operators->Size(), OperatorName(GetOperatorToken())) ;

        // VIPER #2025 - List locations of visible identifiers
        unsigned first = 0 ;
        FOREACH_SET_ITEM_BACK(visible_operators, si, &oper) {
            oper->Info("%s match for '%s' found here", (first++) ? "another" : "first", oper->Name());
        }
    }

    FOREACH_SET_ITEM(visible_operators, si, &oper) {
        if (return_types) (void) return_types->Insert(oper->BaseType()) ;
    }

    // delete:
    FOREACH_MAP_ITEM(&operator_map, mi, &this_oper, &visible_operators) {
        visible_operators->Reset() ;
        delete visible_operators ;
    }

    if (the_match) return the_match->Type() ;

    return 0 ;
}

VhdlIdDef *
VhdlOperator::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    if (RuntimeFlags::GetVar("vhdl_use_iterative_algorithms")) {
        // VIPER #8170: Use iterative method for TypeInfer() for binary operators:
        if(GetLeftExpression() && GetRightExpression()) {
            return VhdlOperator::TypeInferIterative(expected_type, return_types, environment, selection_scope) ;
        }
    }

    // VIPER #8085 : Operator is not valid as subtype indication
    if (environment == VHDL_interfacerange || environment == VHDL_range || environment == VHDL_recordrange || environment == VHDL_subtyperange) Error("range expression expected here") ;
    // Type - inference of operators.
    // This is general : could be 0,1 or 2 argument operator calls.

    VhdlIdDef *oper, *oper_type ;
    if (_operator) {
        // Previously, we determined a unique operator here.
        // This means that tree under this operator call is successfully type-inferred already.
        // So, just check, and return.
        oper_type = _operator->Type() ;
        if (expected_type && !expected_type->TypeMatch(oper_type)) {
            if ((oper_type && oper_type->IsUniversalInteger() && expected_type->IsIntegerType()) ||
                (oper_type && oper_type->IsUniversalReal() && expected_type->IsRealType())) {
                // implicit conversion
            } else {
                Error("type mismatch at operator %s ; expected type %s", OperatorName(_oper), expected_type->Name()) ;
            }
        }
        if (return_types && oper_type) (void) return_types->Insert(oper_type->BaseType()) ;
        return oper_type ;
    }

    // Need environment check to make sure LHS is a valid target (VIPER #1876)
    if ((environment == VHDL_VARUPDATE) || (environment == VHDL_SIGUPDATE)) {
        Error("illegal target for assignment") ;
    }

    // Find all visible operators of this type
    // Since there are most likely about a dozen or more operators by any name,
    // set the initial entry size of the Set to a higher number than default.
    // This will reduce re-hashing, saving time and memory fragmentation.
    Set visible_operators(POINTER_HASH,27) ;

    // VIPER 2962 : Made special search for & operators here, using FindOperators(), to comply with ModelSim.
    // VIPER 3389 : cannot do special search. We would find too many operators.
    // Instead, we solve this in VhdlScope::AddUseClause() : when a type is included, we include its whole scope.
    (void) _present_scope->FindAll(OperatorName(_oper), visible_operators) ;

    unsigned num_args = (_left) ? ((_right) ? 2 : 1) : ((_right) ? 1 : 0) ;

    // Rule out the ones with incorrect number of arguments, and/or with
    // incorrect return type (if thats given).
    SetIter si ;
    VhdlIdDef *the_match = 0 ;
    FOREACH_SET_ITEM(&visible_operators, si, &oper) {
        oper_type = oper->Type() ;
        if (!oper_type) {
            // Probably error occurred earlier
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        // Check if oper has the right amount of arguments
        if (oper->NumOfArgs() != num_args) {
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        // Rule out obvious return type mismatches
        if (expected_type) {
            if (!expected_type->TypeMatch(oper_type)) {
                // Mismatch on return type
                if ((oper_type->IsUniversalInteger() && expected_type->IsIntegerType()) ||
                    (oper_type->IsUniversalReal() && expected_type->IsRealType())) {
                    // This guy could match if we can convert universal type
                    // Don't eliminate this, since it might be the only match in the end.
                    // Don't choose it either, since it has to pass the arguments test still.
                } else {
                    (void) visible_operators.Remove(oper) ;
                    continue ;
                }
            }
        }

        // Viper 1493 (4400) : Eliminate operators which have _left expression not matching operator's left argument's type
        // and  which have _right expression not matching operator's right argument's type
        if ((_left && _left->CannotMatchType(oper->ArgType(0))) ||
            (_right && _right->CannotMatchType(oper->ArgType(1)))) {
            (void) visible_operators.Remove(oper) ;
            continue ;
        }

        the_match = oper ; // In case there is only one match
    }

    // Viper 5298. Pass VHDL_interfaceread instead of VHDL_read to
    // catch if it uses a signal defined in the same interface
    unsigned arg_env = VHDL_READ ;
    if (environment == VHDL_interfacerange) {
        arg_env = VHDL_interfaceread ;
    } else if (environment == VHDL_interfaceread) {
        arg_env = VHDL_interfaceread ;
    }
    // VIPER #8148: Pass VHDL_rangebound instead of VHDL_READ to
    // catch if it uses signal
    if (environment == VHDL_subtyperange || environment == VHDL_subtyperangebound || environment == VHDL_ACCESS_SUBTYPE) {
        // Environment VHDL_rangebound represents range bound used in a subtype indication
        arg_env = VHDL_subtyperangebound ;
    }

    // If we are unique already (by return type), then take a
    // short-cut and infer left and right with known argument types.
    if (visible_operators.Size()==1) {
        VERIFIC_ASSERT(the_match) ;

        // If single operator survives, set it as _operator
        _operator = the_match ;

        // Issue 2148 : If this match is an alias to a subprogram, set the subprogram as _prefix_id.
        // That will 'resolve' non-object aliases, and simplify later routines (elaboration etc).
        // Do same for function calls (VhdlIndexedName::TypeInfer).
        if (the_match->IsAlias()) {
            _operator = the_match->GetTargetId() ;
        }

        VhdlIdDef *left_type = the_match->ArgType(0) ;
        VhdlIdDef *right_type = the_match->ArgType(1) ;

        // VIPER 7081 : Pre-defined operators with an array argument will always have an unconstrained formal.
        // In such cases, check if the actual is an aggregate with an 'others' clause :
        if (_operator && _operator->IsPredefinedOperator()) {
            if ((_left && _left->IsOthersAggregate() && left_type && left_type->IsArrayType()) ||
                (_right && _right->IsOthersAggregate() && right_type && right_type->IsArrayType()))
            {
                Error("argument of operator %s cannot be an OTHERS aggregate", OperatorName(_operator->GetOperatorType())) ;
            }
        }

        // Infer left and right side, with the match' argument types
        // Since operators are always function, their argument is always a READ..
        // Pass in selection scope since this can be an expression in an actual of a binding indication.
        if (_left) (void) _left->TypeInfer(left_type,0,arg_env,selection_scope) ;
        if (_right) (void) _right->TypeInfer(right_type,0,arg_env,selection_scope) ;

        if (return_types) (void) return_types->Insert(the_match->BaseType()) ;
        return the_match->Type() ;
    }

    // Context info is not enough.
    // Infer the argument types blindly and get their expected types.
    Set left_arg_types ;
    if (_left) (void) _left->TypeInfer(0,&left_arg_types,arg_env,selection_scope) ;
    if (_left && !left_arg_types.Size()) {
        // If there are NO type matches on the right. Return silent if there was already an error.
        // Otherwise, go through (to let this operator issue an error). (VIPER 3184).
        if (Message::ErrorCount()) return 0 ;
    }

    Set right_arg_types ;
    if (_right) (void) _right->TypeInfer(0,&right_arg_types,arg_env,selection_scope) ;
    if (_right && !right_arg_types.Size()) {
        // If there are NO type matches on the right (even while asking for anything). Return silent if there was already an error.
        // Otherwise, go through (to let this operator issue an error). (VIPER 3184).
        if (Message::ErrorCount()) return 0 ;
    }

    // First check if universal match might be there.
    unsigned left_has_universal_integer = left_arg_types.GetItem(UniversalInteger()) ? 1 : 0 ;
    unsigned left_has_universal_real = left_arg_types.GetItem(UniversalReal()) ? 1 : 0 ;
    unsigned right_has_universal_integer = right_arg_types.GetItem(UniversalInteger()) ? 1 : 0 ;
    unsigned right_has_universal_real = right_arg_types.GetItem(UniversalReal()) ? 1 : 0 ;

    // Match each operator against argument and expected type
    the_match = 0 ;
    VhdlIdDef *arg_type ;

    unsigned first_time = 1 ;
    Array potential_matches ; // Keep track of operators that only match with a type-conversion.
    VhdlIdDef *potential_match = 0 ;
    FOREACH_SET_ITEM(&visible_operators, si, &oper) {
        potential_match = 0 ;
        if (_left) {
            // Check if oper->ArgType(0) is in the left arguments
            arg_type = oper->ArgType(0) ;
            if (!arg_type) continue ; // num of args must have been tested already
            if (!left_arg_types.GetItem(arg_type->BaseType())) {
                // Implicit conversion check (implicitly convert argument to the operator argument type :
                // Issue 3013 : Do NOT implicitly convert a universal type to an access type argument.
                if ((left_has_universal_integer && arg_type->IsIntegerType() && !arg_type->IsAccessType()) ||
                    (left_has_universal_real && arg_type->IsRealType() && !arg_type->IsAccessType())) {
                    // implicit conversion. Don't choose yet, since it needs to pass other argument test
                    potential_match = oper ;
                } else if (arg_type->IsAccessType() && _left->IsNull()) {
                    // VIPER 3184 : argument is NULL literal and operator has access type argument. That matches by implicit conversion (of NULL).
                    potential_match = oper ;
                } else {
                    (void) visible_operators.Remove(oper) ;
                    continue ;
                }
            }
        }
        if (_right) {
            arg_type = oper->ArgType(1) ;
            if (!arg_type) continue ; // num of args must have been tested already
            if (!right_arg_types.GetItem(arg_type->BaseType())) {
                // Implicit conversion check (implicitly convert argument to the operator argument type :
                // Issue 3013 : Do NOT implicitly convert a universal type to an access type argument.
                if ((right_has_universal_integer && arg_type->IsIntegerType() && !arg_type->IsAccessType()) ||
                    (right_has_universal_real && arg_type->IsRealType() && !arg_type->IsAccessType())) {
                    // implicit conversion. Don't choose yet, since it needs to pass return type test
                    potential_match = oper ;
                } else if (arg_type->IsAccessType() && _right->IsNull()) {
                    // VIPER 3184 : argument is NULL literal and operator has access type argument. That matches by implicit conversion (of NULL).
                    potential_match = oper ;
                } else {
                    (void) visible_operators.Remove(oper) ;
                    continue ;
                }
            }
        }
        // add check for potential match of return type too
        if (expected_type) {
            oper_type = oper->Type() ;
            if (!oper_type) continue ; // return type should have been tested already..?
            if (!expected_type->TypeMatch(oper_type)) {
                // Mismatch on return type
                if ((oper_type->IsUniversalInteger() && expected_type->IsIntegerType() && !oper_type->IsAccessType()) ||
                    (oper_type->IsUniversalReal() && expected_type->IsRealType() && !oper_type->IsAccessType())) {
                    // This guy could match if we can convert universal type
                    // Remember it for if there is no explicit other match
                    // Don't throw it out yet. It has to pass the arguments test too.
                    potential_match = oper ;
                } else {
                    (void) visible_operators.Remove(oper) ;
                    continue ;
                }
            }
        }

        // Here, this match survived
        if (potential_match) {
            // It survived, but some type conversion is needed.
            (void) visible_operators.Remove(oper) ;

            // Remove it from exact matches.
            potential_matches.InsertLast(potential_match) ;
            continue ;
        }

        // FIX ME : Scoping (FindAll) does not eliminate homographs (outer decl hidden
        // by inner homograph). So, do that here.
        // Delete the last one, since that is furthest away in the scope.
        if (the_match && oper->IsHomograph(the_match)) {
            if (the_match->IsPredefinedOperator()) {
                // The existing match (the first mentioned operator) is a predefined one.
                // That means that someone overloaded a predefined operator.
                // Language defines that that is not allowed, unless both are in the
                // same package. But that is already taken care of at declaration time.
                // So, we should allow this (so that there are two matches).
                //
                // BUT : Synopsys overloads the relational operators for std_logic_vector in
                // another package (std_logic_unsigned/signed), and expect the overloaded
                // form to prevail over the predefined operator form. That is NOT according
                // to language rules (both should be visible or invisible).
                // But any way, try and comply with that here.
                // Eliminate the match, (and keep the new operator).
                (void) visible_operators.Remove(the_match) ;
                the_match = oper ;
            } else {
                // GIVEN : Locally declared ids always precedes externally included ids.
                // If previous match is not locally declared, then we have two or more
                // homographs included via use clauses.  Give an error here appropriately. (VIPER #1311)
                if (!oper->IsPredefinedOperator() && _present_scope && !_present_scope->IsDeclaredHere(the_match, 1 /*look in upper/extended*/)) {
                    if (first_time) {
                        Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", oper->Name()) ;
                        the_match->Info("%s is declared here", oper->Name()) ;
                        first_time = 0 ;
                    }
                    oper->Info("another match is here") ;
                }
                // Delete the new one (it is declared later than existing match).
                (void) visible_operators.Remove(oper) ;
            }
            continue ;
        }

        the_match = oper ;
    }

    if (!return_types && visible_operators.Size()>1 && !IsVhdl87()) {
        // VIPER 2982 : if the operator is still ambiguous, and and we have a 'hard' inference test (meaning we would error out below)
        // and an argument is a bit-string-literal, and we are currently in 93 mode,
        // then we can try to eliminate the operators that do NOT have a bit-element type in that argument.
        unsigned something_done = 0 ;
        if (_left && _left->GetClassId()==ID_VHDLBITSTRINGLITERAL) {
            FOREACH_SET_ITEM(&visible_operators, si, &oper) {
                arg_type = oper->ArgType(0) ;
                if (arg_type && arg_type->ElementType() && !arg_type->ElementType()->TypeMatch(StdType("bit"))) {
                    (void) visible_operators.Remove(oper) ;
                    something_done = 1 ;
                }
            }
        }
        if (_right && _right->GetClassId()==ID_VHDLBITSTRINGLITERAL) {
            FOREACH_SET_ITEM(&visible_operators, si, &oper) {
                arg_type = oper->ArgType(1) ;
                if (arg_type && arg_type->ElementType() && !arg_type->ElementType()->TypeMatch(StdType("bit"))) {
                    (void) visible_operators.Remove(oper) ;
                    something_done = 1 ;
                }
            }
        }
        // If, after this, some operators were removed due to the bit-string literal, then we can give the 'ambiguity' warning :
        if (something_done) {
            Warning("argument to operator call %s is ambiguous. Use qualified expression or switch to 87 mode",OperatorName(_oper)) ;
        }
    }

    if (visible_operators.Size()==0) {
        // There are no unique matches.
        // Use the potential matches instead :
        // VIPER 4164 : If there are multiple potential matches (all requiring implicit conversions),
        // and this is a 'hard' type-infer call (no more context info available),
        // then rule out the ones that return a universal type.
        // Otherwise, choose all of them.
        unsigned i ;
        FOREACH_ARRAY_ITEM(&potential_matches, i, potential_match) {
            oper_type = potential_match->Type() ;
            if (!oper_type) continue ; // 0-pointer check.
            if (!return_types && (oper_type->IsUniversalInteger() || oper_type->IsUniversalReal())) {
                // Throw this out if there are more potential_matches to come, or already something in 'visible' types.
                if (visible_operators.Size() || (i<potential_matches.Size()-1)) continue ;
            }
            // Otherwise, insert in visible types.
            (void) visible_operators.Insert(potential_match) ;
            // Normally, this should be enough (resolving potential operators). But :
            // NOTE : because we added a (non-LRM) 'mod' operator (REAL-REAL->REAL) for VIPER 2131,
            // there are now certain cases where homograph  operators 'potentially' match (such as mod operation for VIPER 4679).
            // To accept these as legal, we simply pick the first one if 'expected' type is set :
            if (expected_type) break ; // Only one gets inserted.
        }
    }

    // Done with type inference on this node

    // If there is a request to return all matching types,
    // and there are multiple matches, then return all these matches :
    if (return_types && visible_operators.Size()>1) {
        // Just fill-in the result
        FOREACH_SET_ITEM(&visible_operators, si, &oper) {
            (void) return_types->Insert(oper->BaseType()) ;
        }
        return 0 ; // Don't error out ! This is just a dress-rehearsal. Context needed for type-resolving, we will be called again.
    }

    // Here, we are in final call (no request for all matching types).

    // If there is not exactly one match, then error out :
    if (visible_operators.Size()!=1) {
        // Error out (this can be downgraded to warning by user, and we will still pick one below)
        Error("%d definitions of operator %s match here", visible_operators.Size(), OperatorName(_oper)) ;

        // VIPER #2025 - List locations of visible identifiers
        unsigned first = 0 ;
        FOREACH_SET_ITEM_BACK(&visible_operators, si, &oper) {
            oper->Info("%s match for '%s' found here", (first++) ? "another" : "first", oper->Name());
        }
    }

    // If there are 0 matches now, nothing we can do.
    if (visible_operators.Size()==0) return 0 ; // error given, cannot return anything.

    // VIPER 3109 : always pick one, even if multiple matches.
    // If single operator survives, set it as _operator
    the_match = (VhdlIdDef*)visible_operators.GetLast() ;
    _operator = the_match ;

    // Issue 2148 : If this match is an alias to a subprogram, set the subprogram as _prefix_id.
    // That will 'resolve' non-object aliases, and simplify later routines (elaboration etc).
    // Do same for function calls (VhdlIndexedName::TypeInfer).
    if (the_match->IsAlias()) {
        _operator = the_match->GetTargetId() ;
    }

    VhdlIdDef *left_type = the_match->ArgType(0) ;
    VhdlIdDef *right_type = the_match->ArgType(1) ;

    // VIPER 7081 : Pre-defined operators with an array argument will always have an unconstrained formal.
    // In such cases, check if the actual is an aggregate with an 'others' clause :
    if (_operator && _operator->IsPredefinedOperator()) {
        if ((_left && _left->IsOthersAggregate() && left_type && left_type->IsArrayType()) ||
            (_right && _right->IsOthersAggregate() && right_type && right_type->IsArrayType()))
        {
            Error("argument of operator %s cannot be an OTHERS aggregate", OperatorName(_operator->GetOperatorType())) ;
        }
    }

    // Infer left and right hard now, with argument type of the operator
    if (_left) (void)_left->TypeInfer(left_type,0,VHDL_READ,selection_scope) ;
    if (_right) (void)_right->TypeInfer(right_type,0,VHDL_READ,selection_scope) ;

    if (return_types) (void) return_types->Insert(the_match->BaseType()) ;
    return the_match->Type() ;
}

VhdlIdDef *
VhdlAggregate::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope *selection_scope)
{
    if (!_element_assoc_list) return 0 ;

    // Gotta find all visible 'aggregate' compatible array and record types

// Don't do the short-cut here if we already know the aggregate type :
// We need the 'environment' checked also.
//
//    if (_aggregate_type) {
//        // It's already determined, and elements are type-inferred.
//        if (return_types) return_types->Insert(_aggregate_type->BaseType()) ;
//        return _aggregate_type ;
//    }

    // We cannot rely on the 'expected' type to be here :
    // For example     if (('0','1','1') /= some_array) then..
    // is a valid expression where we can only know the aggregate type unless
    // the RHS was type-infered first and then passed to LHS as expected type.
    // Type inference does not work that way, so we need a way to return all
    // possible matching types on this aggregate. We use comma opetators for that.

    // Even if expected type is given, then we still need comma operator
    // for if it is a multi-dimensional array type.
    // The 'elements' of a multi-dimensional array type are anonymous types that
    // are only available via the "," operator of an array type.

    // So just always find all comma operators, and do type resolving as for any operator :
    Set visible_operators ;
    SetIter si ;
    VhdlIdDef *oper, *elem_type, *oper_type ;
    Set done_scopes(POINTER_HASH, 11) ; // hash table to avoid exponential explosion in FindCommaOperators.

    //_present_scope->FindAll(OperatorName(VHDL_COMMA), visible_operators) ;
    if (_present_scope) _present_scope->FindCommaOperators(visible_operators, done_scopes) ;

    // Rule-out the , operators () that do not match 'expected_type'
    if (expected_type) {
        FOREACH_SET_ITEM(&visible_operators, si, &oper) {
            oper_type = oper->Type() ;
            // Check return type (if needed)
            if (!expected_type->TypeMatch(oper_type)) {
                // Ruled out on return type
                (void) visible_operators.Remove(oper) ;
                continue ;
            }
        }
    }

    unsigned i, j ;
    VhdlExpression *elem ;
    if (visible_operators.Size()==1) {
        // Context is enough for unique identification
        // Short-cut : Infer the element expressions, and we are done
        oper = (VhdlIdDef*)visible_operators.GetLast() ;
        oper_type = oper->Type() ;
        if (!oper_type) return 0 ;
        elem_type = (oper_type->IsArrayType()) ? oper->ArgType(0) : 0 ;

        // If this is a record aggregate, keep track of the choices that
        // are done, so TypeInferChoices() can figure out which type 'others' choice is.
        Set *done_choices = (oper_type->IsRecordType()) ? new Set(POINTER_HASH) : 0 ;
        unsigned n_assoc_choices = 0 ; // no of choices present, discounting others clause
        unsigned has_named = 0, has_positional = 0 ;
        FOREACH_ARRAY_ITEM(_element_assoc_list, i, elem) {
            if (!elem) continue ;
            Array *choices = elem->GetChoices() ;
            VhdlDiscreteRange *choice ;
            FOREACH_ARRAY_ITEM(choices, j, choice) {
                if (choice->IsOthers()) continue ;
                n_assoc_choices += 1 ;
            }
            if (elem->IsOthers()) {
            } else if (elem->IsAssoc()) {
                has_named = 1 ;
            } else {
                has_positional = 1 ;
            }
        }

        if (!oper_type->IsRecordType() && has_named && has_positional) Error("array aggregate elements must be all named or all positional") ;

        FOREACH_ARRAY_ITEM(_element_assoc_list, i, elem) {
            if (!elem) continue ;

            // Vhdl 2008: (Viper 7477, 7417) This element could be either
            // elem_type or oper_type when oper_type is 1-D array
            VhdlIdDef *this_elem_type = elem_type ;

            if (oper_type->IsRecordType()) {
                if (elem->IsAssoc()) {
                    // Name based element
                    this_elem_type = elem->TypeInferChoices(oper_type, done_choices, 0) ;
                } else {
                    // Positional based element
                    if (i >= oper_type->NumOfElements()) {
                        Error("%s has only %d record elements", oper_type->Name(), oper_type->NumOfElements()) ;
                    } else {
                        if (done_choices) (void) done_choices->Insert(oper_type->GetElementAt(i)) ;
                    }
                    this_elem_type = oper->ArgType(i) ;
                }
            } else {
                // Array type. Test the choices for their type
                // Viper #4512 : Passing n_assoc_choices argument for the check inside
                (void) elem->TypeInferChoices(oper_type, done_choices, n_assoc_choices) ;

                // VHDL 2008 VIPER 5918, LRM 9.3.3.3 : For single-dimensional array type,
                // element can be element, or array. Additional Vipers #7477, #7417
                if (IsVhdl2008() && oper_type->Dimension() == 1) {
                    Set potential_types(POINTER_HASH) ;
                    (void) elem->TypeInfer(0, &potential_types, environment, selection_scope) ;

                    // prune the types.. only oper_type or elem_type stays
                    SetIter psi ;
                    VhdlIdDef *matched_type ;
                    FOREACH_SET_ITEM(&potential_types, si, &matched_type) {
                        if (!matched_type) continue ;
                        if (!matched_type->TypeMatch(oper_type) && !matched_type->TypeMatch(elem_type)) {
                            (void) potential_types.Remove(matched_type) ;
                            continue ;
                        }

                        this_elem_type = matched_type ;
                    }

                    if (potential_types.Size() > 1) {
                        // It can only be 2, elem_type and its array (oper_type)
                        // This is possible when aggregate element itself is an
                        // aggregate. Comma operators of both types would be
                        // retained.
                        VERIFIC_ASSERT(potential_types.Size() == 2) ;

                        // Ignore all messages.
                        Message::StartIgnoreAllMessages() ; // nested call okay
                        if (elem->TypeInfer(elem_type, 0, environment, selection_scope)) {
                            this_elem_type = elem_type ; // TODO: Skip redundant call below
                        } else {
                            this_elem_type = oper_type ;
                        }
                        Message::StopIgnoreAllMessages() ;
                    }
                }
            }

            // Would require this during elaboration, now that the
            // type could be either element_type of the aggregate_type
            if (!_element_type_list) _element_type_list = new Array() ;

            while (i > _element_type_list->Size()) _element_type_list->InsertLast(0) ;
            _element_type_list->Insert(i, this_elem_type) ;

            // Infer the actual
            // pass-in the selection scope, since this can be a aggregate in a binding indication.
            (void) elem->TypeInfer(this_elem_type,0,environment,selection_scope) ;

            if (elem->IsOthers() && this_elem_type == oper_type) Error("choice others is not permitted when aggregate expression is of the aggregate type") ;

            // Viper #3146 : LRM Section 8.4, 8.5 : Aggregate target must be locally-static name
            if (((environment == VHDL_SIGUPDATE) || (environment == VHDL_VARUPDATE)) && !elem->IsLocallyStaticAggregateTarget()) { // Viper #3627
                Error("aggregate target in assignment must be all locally static names") ;
            }
        }

        // CHECK ME : Can we check if all record elements have choices now ?
        if (done_choices && done_choices->Size() < oper_type->NumOfElements()) {
            Error("some record elements are missing in this aggregate of %s", oper_type->Name()) ;
        }

        // Kill off the 'done' record fields list
        delete done_choices ;

        if (return_types) (void) return_types->Insert(oper_type->BaseType()) ;
        _aggregate_type = oper_type ; // Set the only possible return type

        return oper_type ;
    }

    // Context is not enough
    // LRM 7.3.2 states that type of aggregate must be determinable
    // from the context only. This means we should not have to prune
    // down the aggregate type based on blind inference of its
    // elements. Just put the resulting composite types (operator-return-type)
    // into the 'return_types' array.
    if (return_types && visible_operators.Size()) {
        // Just fill-in the result
        FOREACH_SET_ITEM(&visible_operators, si, &oper) {
            (void) return_types->Insert(oper->BaseType()) ;
        }
    } else {
        Error("type of aggregate cannot be determined without context ; %d visible types match here", visible_operators.Size()) ;
    }
    return 0 ;
}

VhdlIdDef *
VhdlQualifiedExpression::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned /*environment*/, VhdlScope *selection_scope)
{
    if (!_prefix || !_aggregate) return 0 ;

    // infer suffix with prefix as expected type.
    // Get the prefix (should denote a type)
    VhdlIdDef *prefix_type = _prefix->TypeMark() ;
    if (!prefix_type) return 0 ;  // Error message was already outputted in TypeMark()

    // Infer the aggregate with this type as expected type.
    // Qualified expression's aggregate is always only 'read'
    // Viper #5448 : No return types should be passed here
    (void) _aggregate->TypeInfer(prefix_type, 0, VHDL_READ,selection_scope) ;

    // Check against 'expected_type' if needed
    if (expected_type && !expected_type->TypeMatch(prefix_type)) {
        Error("type error near %s ; expected type %s", prefix_type->Name(), expected_type->Name()) ;
    }
    if (return_types) (void) return_types->Insert(prefix_type->BaseType()) ;

    return prefix_type ;
}

VhdlIdDef *
VhdlAllocator::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    if (expected_type && !expected_type->IsAccessType()) {
        Error("target of NEW ; type %s is not an access type", expected_type->Name());
    } else if (expected_type) {
        // infer the subtype indication with the designated type :
        // Actually, the expression can also be a qualified expression,
        // in which case we should really pass in 'READ'.
        // But that's caught inside qualified expressions presently.
        expected_type = (_subtype) ? _subtype->TypeInfer(expected_type->DesignatedType(),0,VHDL_range,0) : 0 ;

// Type mismatches will have been cought in the type-inference already.
//        if (!expected_subtype->TypeMatch(subtype)) {
//            Error("near NEW ; result type %s is not a %s",expected_type->Name(), subtype->Name()) ;
//        }
    }

    // FIX ME : what to do if (inappropriately) called without expected type ? Error out ?
    return expected_type ;
}

VhdlIdDef *
VhdlWaveformElement::TypeInfer(VhdlIdDef *expected_type, Set *return_types, unsigned environment, VhdlScope * /*selection_scope*/)
{
    if (!_value) return 0 ;

    // Infer 'after' expression
    if (_after) (void) _after->TypeInfer(StdType("time"),0, environment,0) ;

    // Run through normal expression
    return _value->TypeInfer(expected_type, return_types, environment,0) ;
}
VhdlIdDef *
VhdlDefault::TypeInfer(VhdlIdDef *expected_type, Set * /*return_types*/, unsigned /*environment*/, VhdlScope * /*selection_scope*/)
{
    return expected_type ; // FIXME : return default subprog id
}

/*
 ******************* TypeInferChoices ***************************
    For aggregate choices, we infer type by digging into the
    formal part of a name or choice, using the 'mother' type
    as guidance. LRM 7.3.2.
    If the mother type is a record type, each choice must be an
    identifier, a record element.

    If the mother type is an array type, each choice must be a
    simple expression or discrete range that is of the index type
    of the mother type. Since we solved the multi-dim problem by
    declaring a anonymous type for each dimension, we can just use
    the first index type.

    I think we should be able in infer the choices for index
    types, since the normal scope should be enough. For record
    types, we use local scope of the record type.
 ****************** TypeInferChoices **************************
*/

VhdlIdDef *VhdlExpression::TypeInferChoices(VhdlIdDef *mother_type, Set * /*done_choices*/, unsigned /*n_assoc_choices*/)
{
    // Positional association.
    if (!mother_type) return 0 ; // Need context to infer element type.
    if (mother_type->IsArrayType()) {
        // VHDL 2008 VIPER 5918, LRM 9.3.3.3 : For single-dimensional array type, element can be element, or array.
        // Try something :
        if (IsVhdl2008() && mother_type->Dimension()==1) {
            if (IsStringLiteral() || IsBitStringLiteral()) {
                return mother_type ;
            }
        }

        // Return the element type (for array type this will always be element type).
        return mother_type->ElementType() ;
    }
    return 0 ; // fix for record types
}

VhdlIdDef *VhdlElementAssoc::TypeInferChoices(VhdlIdDef *mother_type, Set *done_choices, unsigned n_assoc_choices)
{
    if (!_choices) return 0 ;
    if (!mother_type) return 0 ; // Need context to infer choices.

    // Named association.
    // Type-infer aggregate choice(s). Check if it is valid for 'mother_type'
    // For record types, return the element type.
    // For array types, return the index type
    VhdlIdDef *choice_type = 0 ;
    unsigned i ;
    VhdlDiscreteRange *choice ;
    if (mother_type->IsRecordType()) {
        // Get the formal scope of record elements
        VhdlScope *formal_scope = mother_type->LocalScope() ;
        VERIFIC_ASSERT(formal_scope) ;

        // All choices should be of the same type. First one infers blindly,
        // the next choices will use the first one in their type tests.
        choice_type = 0 ;
        VhdlIdDef *tmp_type ;
        VhdlIdDef *formal ;
        FOREACH_ARRAY_ITEM(_choices, i, choice) {
            if (!choice) continue ;

            if (choice->IsOthers()) {
                // We don't know the type of 'others' : have to find out which
                // formals (record fields) are not yet done. Figure out from 'done_choices'
                for (unsigned j=0; j<mother_type->NumOfElements(); j++) {
                    if (!done_choices) break ;
                    formal = mother_type->GetElementAt(j) ;
                    if (!formal) continue ;
                    if (!done_choices->Insert(formal)) continue ; // We did this one already

                    // Got it ! a choice (record field) that is not yet done :
                    tmp_type = formal->Type() ;

                    // Check if we had more fields in 'others'. They should type-match (LRM 7.3.2.1)
                    if (choice_type) {
                        if (!choice_type->TypeMatch(tmp_type)) {
                            choice->Error("type mismatch between 'others' choices of record association") ;
                        }
                        continue ;
                    }
                    // Found the type of an 'others' record field.
                    choice_type = tmp_type ;
                }
                if (!choice_type) {
                    choice->Error("choice others in a record aggregate should represent at least one element") ;
                }
                return choice_type ; // We have the type
            }

            // Get the type from the field. This also tests against existing 'choice_type',
            // and/or creates a new one.
            choice_type = choice->TypeInfer(choice_type, 0, VHDL_record, formal_scope) ;

            // Keep track of the formals (record fields) that are done
            // Choice must be a simple record element name.
            formal = choice->FindFullFormal() ;
            if (!formal) {
                choice->Error("choice is not a record element of %s", mother_type->Name()) ;
            }
            if (done_choices && formal) {
                if (!done_choices->Insert(formal)) {
                    // Since formal must be a simple record element name, we CAN make the
                    // check that this formal was not yet covered :
                    choice->Error("choice %s is already covered", formal->Name()) ;
                }
            }
        }
    } else if (mother_type->IsArrayType()) {
        // Can't push/pop. Array types have no local scope
        choice_type = mother_type->IndexType(0) ;
        FOREACH_ARRAY_ITEM(_choices, i, choice) {
            if (!choice) continue ;
            if (choice->IsOthers()) {
                return choice_type ; // Type is element type of the array
            }

            // Regular type-inference ; choice is 'used'
            choice_type = choice->TypeInfer(choice_type,0,0/*could be a range in here*/,0) ;

            // Viper #3160 : LRM 7.3.2.2 : If choice is not locally static, _choices->size
            // must be 1. In addition, no. of assoc in parent aggregate must be 1.
            // Viper #4512 : n_assoc_choices checks for the no of choices in the parent aggregate
            // Modelsim discounts 'others' (gives only warning and proceeds) in this choice count
            if (n_assoc_choices > 1 && !choice->IsLocallyStatic()) {
                choice->Error("in an array aggregate expression, non-locally static choice is allowed only if it is the only choice of the only association") ;
            }
        }
        choice_type = mother_type->ElementType() ; // aggregate element is always the element type of the array type.
    }
    return choice_type ;
}

/**************************************************************/
// Tests
/**************************************************************/

// Check if subtype indication is 'unconstrained' ( has box <> in it).

unsigned VhdlDiscreteRange::IsUnconstrained(unsigned /*all_level*/) const { return 0 ; } // default
unsigned VhdlExpression::IsUnconstrained(unsigned /*all_level*/) const    { return 0 ; } // default
unsigned VhdlBox::IsUnconstrained(unsigned /*all_level*/) const           { return 1 ; }
unsigned VhdlExplicitSubtypeIndication::IsUnconstrained(unsigned all_level) const
{
    // I think that we always need "range <>" for unconstrained array type
    if (_range_constraint) {
        return _range_constraint->IsUnconstrained(all_level) ;
    } else {
        // VIPER #6938 : When _range_constraint is not present, look at _range_type
        return (_range_type && ((all_level && _range_type->IsUnconstrained()) || _range_type->IsUnconstrainedArrayType())) ? 1 : 0 ;
    }
}

unsigned VhdlDiscreteRange::NumUnconstrainedRanges() const { return 0 ; } // default
unsigned VhdlExpression::NumUnconstrainedRanges() const    { return 0 ; } // default
unsigned VhdlBox::NumUnconstrainedRanges() const           { return 1 ; }
unsigned VhdlExplicitSubtypeIndication::NumUnconstrainedRanges() const
{
    // I think that we always need "range <>" for unconstrained array type
    if (_range_constraint) {
        return _range_constraint->NumUnconstrainedRanges() ;
    } else if (_range_type) {
        // VIPER #6938 : When _range_constraint is not present, look at _range_type
        VhdlDeclaration *type_declaration = _range_type->GetDeclaration() ;
        return (type_declaration ? type_declaration->NumUnconstrainedRanges() : 0) ;
    }
    return 0 ;
}

unsigned VhdlDiscreteRange::IgnoreAssignment() const // Viper #5700
{
    if (!vhdl_file::IgnoreDeadAssignment()) return 0 ;
    return !IsUsed() ;
}

unsigned VhdlDiscreteRange::IsUsed() const          { return 1 ; } // default to true
unsigned VhdlElementAssoc::IsUsed() const           { return _expr ? _expr->IsUsed() : 0 ; }

unsigned VhdlAggregate::IsUsed() const
{
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (expr && expr->IsUsed()) return 1 ;
    }

    return 0 ;
}
// Viper 7480 7481: Mark the used flag on actual expression from formal
void VhdlAggregate::MarkUsedFlag(SynthesisInfo *info)
{
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (expr) expr->MarkUsedFlag(info) ;
    }
}

// Check if subtype indication is a range (can't make these 'const')

unsigned VhdlDiscreteRange::IsRange()               { return 1 ; } // default to true
unsigned VhdlExpression::IsRange()                  { return 0 ; } // default to false
unsigned VhdlSubtypeIndication::IsRange()           { return 1 ; } // It denotes a type

// Check if association element is a (named) association

unsigned VhdlDiscreteRange::IsAssoc() const         { return 0 ; }
unsigned VhdlExpression::IsAssoc() const            { return 0 ; }
unsigned VhdlAssocElement::IsAssoc() const          { return (_formal_part) ? 1 : 0 ; }
unsigned VhdlElementAssoc::IsAssoc() const          { return (_choices) ? 1 : 0 ; }

// Check if choice is 'others' choice.

unsigned VhdlDiscreteRange::IsOthers() const        { return 0 ; }
unsigned VhdlExpression::IsOthers() const           { return 0 ; }
unsigned VhdlElementAssoc::IsOthers() const
{
    // Check if any element is 'others'.
    // We already checked that IF it is there, it is the single choice. So just test one choice.
    if (!_choices || !_choices->Size()) return 0 ;
    VhdlDiscreteRange *choice = (VhdlDiscreteRange*)_choices->GetLast() ;
    if (!choice) return 0 ;
    return choice->IsOthers() ;
}

// Check if waveform is 'unaffected'
unsigned VhdlDiscreteRange::IsUnaffected() const    { return 0 ; }
unsigned VhdlExpression::IsUnaffected() const       { return 0 ; }
unsigned VhdlWaveformElement::IsUnaffected() const  { return (_value) ? _value->IsUnaffected() : 0 ; }

// Check if an expression is an aggregate
unsigned VhdlDiscreteRange::IsAggregate() const     { return 0 ; }
unsigned VhdlExpression::IsAggregate() const        { return 0 ; }
unsigned VhdlAggregate::IsAggregate() const         { return 1 ; }
unsigned VhdlWaveformElement::IsAggregate() const   { return (_value) ? _value->IsAggregate() : 0 ; }

// VIPER 7081 : Check if this is an aggregate with an 'others' clause.
unsigned VhdlDiscreteRange::IsOthersAggregate() const     { return 0 ; }
unsigned VhdlExpression::IsOthersAggregate() const        { return 0 ; }
unsigned VhdlAggregate::IsOthersAggregate() const
{
    // Check if this aggregate has an 'others' clause :
    // Most be the last one in the element_assoc_list, if there :
    if (!_element_assoc_list || !_element_assoc_list->Size()) return 0 ;
    VhdlDiscreteRange *elem = (VhdlDiscreteRange*) _element_assoc_list->GetLast() ;
    if (!elem) return 0 ;
    // Check that element :
    return elem->IsOthers() ;
}
unsigned VhdlWaveformElement::IsOthersAggregate() const   { return (_value) ? _value->IsOthersAggregate() : 0 ; }

// Check if this is a qualified expression
unsigned VhdlExpression::IsQualifiedExpression() const { return 0 ; }
unsigned VhdlQualifiedExpression::IsQualifiedExpression() const { return 1 ; }

// Check if association element is 'open'
unsigned VhdlDiscreteRange::IsOpen() const          { return 0 ; }
unsigned VhdlExpression::IsOpen() const             { return 0 ; }
unsigned VhdlAssocElement::IsOpen() const           { return _actual_part ? _actual_part->IsOpen() : 0 ; }
unsigned VhdlInertialElement::IsOpen() const        { return _actual_part ? _actual_part->IsOpen() : 0 ; }
// Only VhdlOpen (in VhdlName) is 'open'

// Check if this association is between
// record id and its resolution function
unsigned VhdlExpression::IsRecResFuncElement() const      { return 0 ; }
unsigned VhdlRecResFunctionElement::IsRecResFuncElement() const { return 1 ; }

// Check if association element is a NULL literal
unsigned VhdlDiscreteRange::IsNull() const          { return 0 ; }
unsigned VhdlExpression::IsNull() const             { return 0 ; }
unsigned VhdlWaveformElement::IsNull() const        { return _value ? _value->IsNull() : 0 ; }
// Only VhdlNull (in VhdlName) is a NULL literal

// Check if association element (or expression) is a constant literal (or generic)
unsigned VhdlDiscreteRange::IsConstant() const      { return 0 ; }
unsigned VhdlExpression::IsConstant() const         { return 0 ; }
 
unsigned VhdlAggregate::NumElements() const
{
    return _element_assoc_list ? _element_assoc_list->Size() : 0 ;
}

VhdlExpression const *VhdlAggregate::GetElementExpressionAt(unsigned idx) const
{
    if (!_element_assoc_list || idx >= NumElements()) return 0 ;

    VhdlDiscreteRange const *elem = (VhdlDiscreteRange const *)_element_assoc_list->At(idx) ;
    if (!elem) return 0 ;

    return elem->GetExpression() ;
}

unsigned VhdlAggregate::IsAggregateTypeAssoc(unsigned assoc_idx) const
{
    if (!_element_type_list || assoc_idx >= NumElements()) return 0 ;
    return ((VhdlIdDef *)_element_type_list->At(assoc_idx) == _aggregate_type) ;
}

// Viper #7042: only for (aggregate of) string literals, returns allocated string
char *VhdlDiscreteRange::ExprString() const { return 0 ; }

char *VhdlAggregate::ExprString() const
{
    if (!_element_assoc_list) return Strings::save("") ;

    char *expr_string = 0 ;
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (!expr) continue ;
        char *ele_string = expr->ExprString() ;
        if (!ele_string) {
            Strings::free(expr_string) ;
            return 0 ;
        }

        if (!expr_string) {
            expr_string = ele_string ;
        } else {
            char *tmp = Strings::save(expr_string, ele_string) ;
            Strings::free(expr_string) ;
            Strings::free(ele_string) ;
            expr_string = tmp ;
        }
    }

    return expr_string ;
}

char *VhdlOperator::ExprString() const
{
    if (VHDL_AMPERSAND != _oper) return 0 ;

    char *left_string = _left ? _left->ExprString() : 0 ;
    if (!left_string || !_right) return left_string ;

    char *right_string = _right->ExprString() ;
    if (!right_string) {
        Strings::free(left_string) ;
        return 0 ;
    }

    char *expr_string = Strings::save(left_string, right_string) ;
    Strings::free(left_string) ;
    Strings::free(right_string) ;
    return expr_string ;
}

/**************************************************************/
// Locally/Globally Static Names : LRM section 6.1
/**************************************************************/
unsigned VhdlDiscreteRange::IsLocallyStaticName() const     { return 0 ; }
unsigned VhdlDiscreteRange::IsGloballyStaticName() const    { return 0 ; }

unsigned VhdlExpression::IsLocallyStaticName() const     { return 0 ; }
unsigned VhdlExpression::IsGloballyStaticName() const    { return 0 ; }

unsigned VhdlElementAssoc::IsLocallyStaticName() const
{
    if (_expr && !_expr->IsLocallyStaticName()) return 0 ;
    return 1 ;
}

unsigned VhdlElementAssoc::IsGloballyStaticName() const
{
    if (_expr && !_expr->IsGloballyStaticName()) return 0 ;
    return 1 ;
}

unsigned VhdlDiscreteRange::IsGoodForPortActual() const
{
    // Viper 7478 : Expressions are allowed as port actuals Vhdl 2008 LRM Section 6.5.7.3
    return (IsGloballyStaticName() || IsGloballyStatic()) ;
}
// Viper 7645: Section 6.5.6.3 2008 LRM New rules to allow port actuals
unsigned VhdlAggregate::IsGoodForPortActual() const
{
    if (IsGloballyStatic()) return 1 ;
    if (!IsVhdl2008()) return  0 ;
    // An aggregate, provided all choices are locally static and all  expressions in
    // element associations are expressions which are good for port actual otherwise.
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (!expr) return 0 ;
        if (IsAssoc()) {
            VhdlName* formal_part = expr->FormalPart() ;
            VhdlExpression* actual_part = expr->ActualPart() ;
            if (!formal_part || !actual_part) return 0 ;
            if (!formal_part->IsLocallyStaticName()) return 0 ;
            if (!actual_part->IsGoodForPortActual()) return 0 ;
        } else {
            if (!expr->IsGoodForPortActual()) return 0 ;
        }
    }
    return 1 ;
}
unsigned VhdlOperator::IsGoodForPortActual() const
{
    if (IsGloballyStatic()) return 1 ;
    if (!IsVhdl2008()) return  0 ;

    // if it is a predefined operator or a pragma function this is allowed to be in compliance with
    // popular simulator. User defined functions if have un constrained types are not good for actuals.
    if (_operator && (_operator->GetPragmaFunction() || _operator->IsPredefinedOperator())) return 1 ;

    VhdlSpecification* function_spec = _operator ? _operator->Spec() : 0 ;
    VhdlName* return_type = function_spec ? function_spec->GetReturnType() : 0 ;
    if (return_type && return_type->IsUnconstrained(1)) return 0 ;
    if (return_type && return_type->IsGloballyStatic()) return 1 ;
    return 0 ;
}

unsigned VhdlQualifiedExpression::IsGoodForPortActual() const
{
    if (IsGloballyStatic()) return 1 ;
    if (!IsVhdl2008()) return  0 ;

    // return 1 if the type mark is globally static.
    if (_prefix && _prefix->IsGloballyStaticName()) return 1 ;

    return 0 ;
}

// Viper #5073
unsigned VhdlDiscreteRange::IsLocallyStaticAggregateTarget() const
{
    return IsLocallyStaticName() ;
}

unsigned VhdlAggregate::IsLocallyStaticAggregateTarget() const
{
    // Strictly by LRM, this is not locally_static_name, but
    // modelsim allows nested aggregate at aggregate target
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (expr && !expr->IsLocallyStaticAggregateTarget()) return 0 ;
    }

    return 1 ;
}

//*************** IsLocally/GloballyStatic *******************
// locally/globally static :  LRM sections 7.4.1 and 7.4.2
//************************************************************
unsigned VhdlExpression::IsLocallyStatic() const     { return 0 ; }
unsigned VhdlDiscreteRange::IsLocallyStatic() const  { return 0 ; }

unsigned VhdlQualifiedExpression::IsLocallyStatic() const
{
    return _aggregate ? _aggregate->IsLocallyStatic() : 0 ;
}

unsigned VhdlAssocElement::IsLocallyStatic() const
{
    if (_actual_part && !_actual_part->IsLocallyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlInertialElement::IsLocallyStatic() const
{
    if (_actual_part && !_actual_part->IsLocallyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlRecResFunctionElement::IsLocallyStatic() const
{
    if (_record_id_function && !_record_id_function->IsLocallyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlElementAssoc::IsLocallyStatic() const
{
    if (_expr && !_expr->IsLocallyStatic()) return 0 ;

    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice && !choice->IsLocallyStatic()) return 0 ;
    }

    return 1 ;
}

unsigned VhdlAggregate::IsLocallyStatic() const
{
    // Strictly by LRM, this is not locally_static,
    // but modelsim allows this with warning
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (expr && !expr->IsLocallyStatic()) return 0 ;
    }

    return 1 ;
}

unsigned VhdlOperator::IsLocallyStatic() const
{
    if (_left && !_left->IsLocallyStatic()) return 0 ;
    if (_right && !_right->IsLocallyStatic()) return 0 ;

    return 1 ; // Need more checks
}

unsigned VhdlExplicitSubtypeIndication::IsLocallyStatic() const
{
    if (_range_constraint && !_range_constraint->IsLocallyStatic()) return 0 ;
    if (_type_mark && !_type_mark->IsLocallyStatic()) return 0 ;

    return 1 ;
}

unsigned VhdlRange::IsLocallyStatic() const
{
    if (_left && !_left->IsLocallyStatic()) return 0 ;
    if (_right && !_right->IsLocallyStatic()) return 0 ;

    return 1 ;
}

unsigned VhdlRange::IsGloballyStatic() const
{
    if (_left && !_left->IsGloballyStatic()) return 0 ;
    if (_right && !_right->IsGloballyStatic()) return 0 ;

    return 1 ;
}

unsigned VhdlExpression::IsGloballyStatic() const    { return 0 ; }
unsigned VhdlDiscreteRange::IsGloballyStatic() const { return 0 ; }
unsigned VhdlBox::IsGloballyStatic() const           { return 1 ; }

unsigned VhdlAllocator::IsGloballyStatic() const
{
    if (_subtype && !_subtype->IsGloballyStatic()) return 0 ;
    return 1;
}

unsigned VhdlQualifiedExpression::IsGloballyStatic() const
{
    return _aggregate ? _aggregate->IsGloballyStatic() : 0 ;
}

unsigned VhdlAggregate::IsGloballyStatic() const
{
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (expr && !expr->IsGloballyStatic()) return 0 ;
    }

    return 1 ;
}

unsigned VhdlExplicitSubtypeIndication::IsGloballyStatic() const
{
    if (_range_constraint && !_range_constraint->IsGloballyStatic()) return 0 ;
    if (_type_mark && !_type_mark->IsGloballyStatic()) return 0 ;

    return 1 ;
}

unsigned VhdlAssocElement::IsGloballyStatic() const
{
    if (_actual_part && !_actual_part->IsGloballyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlInertialElement::IsGloballyStatic() const
{
    if (_actual_part && !_actual_part->IsGloballyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlRecResFunctionElement::IsGloballyStatic() const
{
    if (_record_id_function && !_record_id_function->IsGloballyStatic()) return 0 ;
    return 1 ;
}

unsigned VhdlElementAssoc::IsGloballyStatic() const
{
    if (_expr && !_expr->IsGloballyStatic()) return 0 ;

    unsigned i ;
    VhdlDiscreteRange *choice ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (choice && !choice->IsGloballyStatic()) return 0 ;
    }

    return 1 ;
}

unsigned VhdlOperator::IsGloballyStatic() const
{
    if (_left && !_left->IsGloballyStatic()) return 0 ;
    if (_right && !_right->IsGloballyStatic()) return 0 ;

    return 1 ; // Need more checks
}

/*
 ******************* FindFormal ***************************
 * A choice (discrete range or expression) can contain a
 * formal (name). This routine will get the VhdlIdRef of the formal,
 * after type-inference ran.
 ******************* FindFormal **************************
*/

VhdlIdDef *VhdlDiscreteRange::FindFormal() const        { return 0 ; }
VhdlIdDef *VhdlExpression::FindFormal() const           { return 0 ; } // Only a VhdlName has a formal
VhdlIdDef *VhdlAssocElement::FindFormal() const         { return (_formal_part) ? _formal_part->FindFormal() : 0 ; }

VhdlIdDef *VhdlDiscreteRange::FindFullFormal() const    { return 0 ; }
VhdlIdDef *VhdlExpression::FindFullFormal() const       { return 0 ; } // Only a VhdlName has a formal
VhdlIdDef *VhdlAssocElement::FindFullFormal() const     { return (_formal_part) ? _formal_part->FindFullFormal() : 0 ; }

/**************************************************************/
// Find out if this condition expression is a 'clock' edge expression.
// Currently only simple 'event expressions are detected as 'clock'.
// Used for VIPER 3603.
/**************************************************************/

//Viper 6156: Shows the FindIfEdge is not complete. This should
//be in correspondence to Section 61.12 of IEEE-1076.6 synthesis
// directives. The rules are as follows:
//The general syntax for specifying an edge of a clock shall be the following:
//
// clock_edge ::=
//   RISING_EDGE(clk_signal_name)
// | FALLING_EDGE(clk_signal_name)
// | clock_level and event_expr
// | event_expr and clock_level
//
// clock_level ::= clk_signal_name = '0' | clk_signal_name = '1'
// event_expr ::= clk_signal_name'EVENT | not clk_signal_name'STABLE

unsigned
VhdlAttributeName::FindIfEdge() const
{
    // Return '1' for 'event attribute :
    if (_attr_enum == VHDL_ATT_event) return 1 ;
    return 0 ;
}

unsigned
VhdlIndexedName::FindIfEdge() const
{
    // Return '1' if this is a function call that has only one return statement that is an edge.
    if (_is_function_call) {
        unsigned function = (_prefix_id) ? _prefix_id->GetOperatorType() : 0 ; // built-in operators
        if (!function) function = (_prefix_id) ? _prefix_id->GetPragmaFunction() : 0 ; // pragma'ed operators

        switch (function) {
        case VHDL_and :
            {
            // built-in AND or pragma'ed AND. Gamble on simple arguments.
            VhdlDiscreteRange *left = (VhdlDiscreteRange*)_assoc_list->GetFirst() ;
            unsigned result = (left) ? left->FindIfEdge() : 0 ;
            if (result) return result ;
            VhdlDiscreteRange *right = (VhdlDiscreteRange*)_assoc_list->GetLast() ;
            result = (right) ? right->FindIfEdge() : 0 ;
            return result ;
            }
        case VHDL_not :
            {
            VhdlDiscreteRange *left = (VhdlDiscreteRange*)_assoc_list->GetFirst() ;
            unsigned result = (left) ? left->FindIfStable() : 0 ;
            return result ;
            }
        // Check if the operator has a 'rising_edge/falling_edge' pragma :
        case VHDL_rising_edge :
        case VHDL_falling_edge :
            return 1 ;
        default : break ;
        }

        // Check if function call has a single return statement which is a edge..
        if (function==0) {
            VhdlSubprogramBody *body = (_prefix_id) ? _prefix_id->Body() : 0 ;
            // A bit dirty : algorithm to check if this is a 'user' function that defines a clock edge.
            // Look for the last statement in the body,
            // check if it is a return statement, and if so, call FindIfEdge() on it.
            Array *stmts = (body) ? body->GetStatementPart() : 0 ;
            if (stmts) {
                VhdlStatement *last_stmt = (VhdlStatement*)stmts->GetLast() ;
                if (last_stmt && last_stmt->GetClassId()==ID_VHDLRETURNSTATEMENT) {
                    VhdlReturnStatement *return_statement = (VhdlReturnStatement*)last_stmt ;
                    VhdlExpression *return_expr = return_statement->GetExpression() ;
                    return (return_expr) ? return_expr->FindIfEdge() : 0 ;
                }
            }
        }

        // Cannot find an edge on this thing.
        return 0 ;
    }
    return 0 ;
}

unsigned
VhdlOperator::FindIfEdge() const
{
    unsigned function = (_operator) ? _operator->GetOperatorType() : 0 ; // built-in operators
    if (!function) function = (_operator) ? _operator->GetPragmaFunction() : 0 ; // pragma'ed operators

    // look for 'AND' expressions. Then find if one of the arguments has a clock edge.
    switch (function) {
    case VHDL_and :
        {
        // built-in AND or pragma'ed AND.
        unsigned result = (_left) ? _left->FindIfEdge() : 0 ;
        if (result) return result ;
        result = (_right) ? _right->FindIfEdge() : 0 ;
        return result ;
        }
    case VHDL_not :
        {
        unsigned result = (_left) ? _left->FindIfStable() : 0 ;
        return result ;
        }
    // Check if the operator has a 'rising_edge/falling_edge' pragma :
    case VHDL_rising_edge :
    case VHDL_falling_edge :
        return 1 ;
    default : break ;
    }

    // Check if function call has a single return statement which is a edge..
    // TOBEDONE
    return 0 ;
}

unsigned
VhdlAttributeName::FindIfStable() const
{
    // Return '1' for 'event attribute :
    if (_attr_enum == VHDL_ATT_stable) return 1 ;
    return 0 ;
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlDiscreteRange::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlExpression::CalculateSignature() const
{
    unsigned long sig = VhdlDiscreteRange::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlSubtypeIndication::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlExplicitSubtypeIndication::CalculateSignature() const
{
    unsigned long sig = VhdlSubtypeIndication::CalculateSignature() ;
    if (_res_function) sig = HASH_VAL(sig, _res_function->CalculateSignature()) ;
    if (_type_mark) sig = HASH_VAL(sig, _type_mark->CalculateSignature()) ;
    if (_range_constraint) sig = HASH_VAL(sig, _range_constraint->CalculateSignature()) ;
    // back pointer : if (_range_type) sig = HASH_VAL(sig, _range_type->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlRange::CalculateSignature() const
{
    unsigned long sig = VhdlDiscreteRange::CalculateSignature() ;
    if (_left) sig = HASH_VAL(sig, _left->CalculateSignature()) ;
    sig = HASH_VAL(sig, _dir) ;
    if (_right) sig = HASH_VAL(sig, _right->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlBox::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    return sig ;
}

unsigned long
VhdlAssocElement::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_formal_part) sig = HASH_VAL(sig, _formal_part->CalculateSignature()) ;
    if (_actual_part) sig = HASH_VAL(sig, _actual_part->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlInertialElement::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_actual_part) sig = HASH_VAL(sig, _actual_part->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlRecResFunctionElement::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_record_id_name) sig = HASH_VAL(sig, _record_id_name->CalculateSignature()) ;
    if (_record_id_function) sig = HASH_VAL(sig, _record_id_function->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlOperator::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_left) sig = HASH_VAL(sig, _left->CalculateSignature()) ;
    sig = HASH_VAL(sig, _oper) ;
    if (_right) sig = HASH_VAL(sig, _right->CalculateSignature()) ;
    // back-poitner : if (_operator) sig = HASH_VAL(sig, _operator->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlAllocator::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_subtype) sig = HASH_VAL(sig, _subtype->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlAggregate::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    // back-pointer : if (_aggregate_type) sig = HASH_VAL(sig, _aggregate_type->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlQualifiedExpression::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_prefix) sig = HASH_VAL(sig, _prefix->CalculateSignature()) ;
    if (_aggregate) sig = HASH_VAL(sig, _aggregate->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlElementAssoc::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    VhdlDiscreteRange *expr ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_choices, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    if (_expr) sig = HASH_VAL(sig, _expr->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlWaveformElement::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    if (_value) sig = HASH_VAL(sig, _value->CalculateSignature()) ;
    if (_after) sig = HASH_VAL(sig, _after->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlDefault::CalculateSignature() const
{
    unsigned long sig = VhdlExpression::CalculateSignature() ;
    return sig ;
}

// Check for valid Deffered constants in constant initializaion in package
// declaration Viper 3505
void VhdlOperator::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    if (_left) _left->CheckLegalDefferedConst(init_assign, unit_ids) ;
    if (_right) _right->CheckLegalDefferedConst(init_assign, unit_ids) ;
}
void VhdlWaveformElement::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    if (_value) _value->CheckLegalDefferedConst(init_assign, unit_ids) ;
}
void VhdlAggregate::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    if (_element_assoc_list) {
        VhdlExpression *elem ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_element_assoc_list, i, elem) {
            elem->CheckLegalDefferedConst(init_assign, unit_ids) ;
        }
    }
}
void VhdlElementAssoc::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    if (_expr) _expr->CheckLegalDefferedConst(init_assign, unit_ids) ;
}

void VhdlRange::CheckLegalDefferedConst(const VhdlTreeNode *init_assign, Set* unit_ids) const
{
    if (_left) _left->CheckLegalDefferedConst(init_assign, unit_ids) ;
    if (_right) _right->CheckLegalDefferedConst(init_assign, unit_ids) ;
}
VhdlIdDef *VhdlDiscreteRange::EntityAspect() { return 0 ; }
/**********************************************************/
// SubprogramAspect returns the subprogram (function or procedure)
// name from uninstantiated name of subprogram instantiation
/**********************************************************/
VhdlIdDef *VhdlDiscreteRange::SubprogramAspect(VhdlIdDef * /*formal*/)
{
    Error("illegal unit name") ;
    return 0 ;
}
VhdlIdDef *VhdlBox::SubprogramAspect(VhdlIdDef * /*formal*/)
{
    return 0 ; // FIXME : Need to find from scope
}

// VIPER #6896: Conversion of verilog package to vhdl package
void VhdlRange::FillRange(unsigned left_bound, unsigned right_bound) // fillup range
{
    if (_dir != VHDL_CONVERSION_HANDLING) return ;

    delete _left ;
    delete _right ;

    _left = new VhdlInteger((verific_int64)left_bound) ;
    _left->SetLinefile(Linefile()) ;
    _right = new VhdlInteger((verific_int64)right_bound) ;
    _right->SetLinefile(Linefile()) ;
    _dir = (left_bound > right_bound) ? VHDL_downto : VHDL_to ;
}

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
// VIPER #7343 : Dimension from type of any identifier
VhdlDiscreteRange *VhdlSubtypeIndication::GetDimensionAt(unsigned /*dimension*/) const
{
    return 0 ;
}
VhdlDiscreteRange *VhdlExplicitSubtypeIndication::GetDimensionAt(unsigned dimension) const
{
    if (!_range_constraint) { // No range constraint, go to type
        return _type_mark ? _type_mark->GetDimensionAt(dimension): 0 ;
    }
    // If 'dimension' is 0, this _range_constraint is the required range
    if ((dimension == 0) && !_range_constraint->IsUnconstrained(0)) return _range_constraint ;

    // 'dimension' > 0, need to check _type_mark for required dimension
    return _type_mark ? _type_mark->GetDimensionAt(dimension): 0 ;
}
#endif // VHDL_ID_SUBTYPE_BACKPOINTER

