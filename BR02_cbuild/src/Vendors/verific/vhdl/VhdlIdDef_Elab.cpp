/*
 *
 * [ File Version : 1.167 - 2014/03/14 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // FILE access
#include <math.h>  // Viper #8006: Math-real foreign attribute support

#include "vhdl_file.h"
#include "VhdlIdDef.h" // Compile flags are defined within here

#include "Map.h"
#include "Array.h"
#include "Strings.h"
#include "Message.h"
#include "FileSystem.h"

#include "vhdl_tokens.h"
#include "VhdlDeclaration.h"
#include "VhdlSpecification.h"
#include "VhdlName.h"
#include "VhdlScope.h"
#include "VhdlUnits.h"
#include "VhdlMisc.h"
#include "VhdlConfiguration.h"

// For elaboration
#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
/**********************************************************/
//  Elaboration Routines
/**********************************************************/
/**********************************************************/

char *VhdlIdDef::CreateUniqueName(const VhdlBlockConfiguration * /*block_config*/) const { VERIFIC_ASSERT(0) ; return 0 ; }

char *VhdlEntityId::CreateUniqueName(const VhdlBlockConfiguration *block_config) const
{
    // Create a Cell name for this entity, with set generics
    char *result = Strings::save(OrigName()) ;
    unsigned i ;
    VhdlIdDef *generic ;
    VhdlValue *val ;
    char *suffix, *tmp ;
    VhdlIdDef *type = 0 ;

    // Need to use characters in the name that are syntactically invalid VHDL,
    // to avoid name collisions with normal design unit names (issue 1295)
    unsigned first = 1 ;
    FOREACH_ARRAY_ITEM(_generics, i, generic) {
        if (!generic) continue ;

        // Don't encorporate physical types in unique name. They would create
        // very long names, which they don't affect the functionality of the design.
        // Vital package contains Array of TIME types (VitalDelayType01...etc)
        // and Array-of-Array-of-Time (VitalDelayArrayType01...etc). Issue 1939.
        // So rule out any type which is 'physical' at the scalar level :
        //VhdlIdDef *type = generic->Type() ;
        //while (type && type->IsArrayType()) type = type->ElementType() ;
        //if (!type) continue ; // something bad is wrong with this generic's type.
        // Discard if physical
        // VIPER #3794 : Discard physical only in rtl elaboration :
        //if (IsRtlElab() && type->IsPhysicalType()) continue ;

        // Now get the value for this generic, and create an image for it.
        //val = generic->Value() ;

        // Create image for the value
        // Viper 6093 : Create a shorter image in creating unique name
        if (generic->IsGenericType()) {
            type = generic->ActualType() ; // Get associated type name
            suffix = type ? Strings::save(type->Name()): 0 ;
        } else if (generic->IsSubprogram()) {
            type = generic->GetActualId() ;
            suffix = type ? Strings::save(type->Name()): 0 ;
        } else if (generic->IsPackage()) {
            type = generic->GetElaboratedPackageId() ;
            suffix = type ? type->CreateUniqueName(0): 0 ;
        } else {
            type = generic->Type() ;
            while (type && type->IsArrayType()) type = type->ElementType() ;
            if (!type) continue ; // something bad is wrong with this generic's type.
            // Discard if physical
            // VIPER #3794 : Discard physical only in rtl elaboration :
            if (IsRtlElab() && type->IsPhysicalType()) continue ;
            // Now get the value for this generic, and create an image for it.
            val = generic->Value() ;
            if (!val) continue ;
            suffix = val->ImageShort() ;
        }
        if (!suffix) continue ;

        // Append the generic value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }

    // Append for unconstrained ports.
    // Assume that the unconstrained ports have already been through AssociatePorts,
    // which means that they are now have valid constraints.
    VhdlIdDef *port ;
    VhdlConstraint *constraint ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        if (!port) continue ;
        if (!port->IsUnconstrained()) continue ; // This is using a bit-flag.

        // append left and right bound of the index constraint.
        constraint = port->Constraint() ;
        if (!constraint) continue ;

        // Do the left index bound :
        val = constraint->Left() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the left value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;

        // Do the right index bound :
        val = constraint->Right() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the right value to the name
        tmp = result ;
        result = Strings::save(result, ",", suffix) ; // open with parenthesis, separate with comma
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }

    if (!first) {
        // Close with parenthesis
        tmp = result ;
        result = Strings::save(result, ")") ;
        Strings::free(tmp) ;
    }

    // Viper 6161 append constraints of unconstrained generics to the entity name
    first = 1 ;
    // Append for unconstrained generics.
    // Assume that the unconstrained generics have already been through AssociateGenerics,
    // which means that they are now have valid constraints.
    FOREACH_ARRAY_ITEM(_generics, i, generic) {
        if (!generic) continue ;
        if (!generic->IsUnconstrained()) continue ; // This is using a bit-flag.

        // append left and right bound of the index constraint.
        constraint = generic->Constraint() ;
        if (!constraint) continue ;

        // Do the left index bound :
        val = constraint->Left() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the left value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;

        // Do the right index bound :
        val = constraint->Right() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the right value to the name
        tmp = result ;
        result = Strings::save(result, ",",suffix) ; // open with parenthesis, separate with comma
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }

    if (!first) {
        // Close with parenthesis
        tmp = result ;
        result = Strings::save(result, ")") ;
        Strings::free(tmp) ;
    }

    if (block_config) {
        unsigned uniq_count = block_config->GetHashedNum(result) ;
        tmp = result ;
        char str[32] ;
        sprintf(str, "%d", uniq_count) ;
        result = Strings::save(result, "_uniq_", str) ;
        Strings::free(tmp) ;
    }
    // VIPER #7853 : Create short name if created name is very long
    if (IsStaticElab()) return CreateShortName(Name(), result) ;
    return result ;
}

char *VhdlComponentId::CreateUniqueName(const VhdlBlockConfiguration *block_config) const
{
    // Create a Cell name for this component, with set generics
    char *result = Strings::save(OrigName()) ;
    unsigned i ;
    VhdlIdDef *generic ;
    VhdlValue *val ;
    char *suffix, *tmp ;
    VhdlIdDef *type = 0 ;

    // Need to use characters in the name that are syntactically invalid VHDL,
    // to avoid name collisions with normal design unit names (issue 1295)
    unsigned first = 1 ;
    FOREACH_ARRAY_ITEM(_generics, i, generic) {
        if (!generic) continue ;

        // Don't encorporate physical types in unique name. They would create
        // very long names, which they don't affect the functionality of the design.
        // Vital package contains Array of TIME types (VitalDelayType01...etc)
        // and Array-of-Array-of-Time (VitalDelayArrayType01...etc). Issue 1939.
        // So rule out any type which is 'physical' at the scalar level :
        //VhdlIdDef *type = generic->Type() ;
        //while (type && type->IsArrayType()) type = type->ElementType() ;
        //if (!type) continue ; // something bad is wrong with this generic's type.
        // Discard if physical
        //if (type->IsPhysicalType()) continue ;

        // Now get the value for this generic, and create an image for it.
        //val = generic->Value() ;
        //if (!val) continue ;

        // Create image for the value
        if (generic->IsGenericType()) {
            type = generic->ActualType() ; // Get associated type name
            suffix = type ? Strings::save(type->Name()): 0 ;
        } else if (generic->IsSubprogram()) {
            type = generic->GetActualId() ;
            suffix = type ? Strings::save(type->Name()): 0 ;
        } else if (generic->IsPackage()) {
            type = generic->GetElaboratedPackageId() ;
            suffix = type ? type->CreateUniqueName(0): 0 ;
        } else {
            // Don't encorporate physical types in unique name. They would create
            // very long names, which they don't affect the functionality of the design.
            // Vital package contains Array of TIME types (VitalDelayType01...etc)
            // and Array-of-Array-of-Time (VitalDelayArrayType01...etc). Issue 1939.
            // So rule out any type which is 'physical' at the scalar level :
            type = generic->Type() ;
            while (type && type->IsArrayType()) type = type->ElementType() ;
            if (!type) continue ; // something bad is wrong with this generic's type.
            // Discard if physical
            if (type->IsPhysicalType()) continue ;

            // Now get the value for this generic, and create an image for it.
            val = generic->Value() ;
            if (!val) continue ;
            suffix = val->ImageShort() ;
        }
        //suffix = val->Image() ;
        if (!suffix) continue ;

        // Append the generic value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }
    // Append for unconstrained ports.
    // Assume that the unconstrained ports have already been through AssociatePorts,
    // which means that they are now have valid constraints.
    VhdlIdDef *port ;
    VhdlConstraint *constraint ;
    FOREACH_ARRAY_ITEM(_ports, i, port) {
        if (!port) continue ;
        if (!port->IsUnconstrained()) continue ; // This is using a bit-flag.

        // append left and right bound of the index constraint.
        constraint = port->Constraint() ;
        if (!constraint) continue ;

        // Do the left index bound :
        val = constraint->Left() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the left value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;

        // Do the right index bound :
        val = constraint->Right() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the right value to the name
        tmp = result ;
        result = Strings::save(result, ",", suffix) ; // open with parenthesis, separate with comma
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }

    if (!first) {
        // Close with parenthesis
        tmp = result ;
        result = Strings::save(result, ")") ;
        Strings::free(tmp) ;
    }

    // Viper 6161 append constraints of unconstrained generics to the entity name
    first = 1 ;
    // Append for unconstrained generics.
    // Assume that the unconstrained generics have already been through AssociateGenerics,
    // which means that they are now have valid constraints.
    FOREACH_ARRAY_ITEM(_generics, i, generic) {
        if (!generic) continue ;
        if (!generic->IsUnconstrained()) continue ; // This is using a bit-flag.

        // append left and right bound of the index constraint.
        constraint = generic->Constraint() ;
        if (!constraint) continue ;

        // Do the left index bound :
        val = constraint->Left() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the left value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;

        // Do the right index bound :
        val = constraint->Right() ;
        if (!val) continue ;
        suffix = val->Image() ;
        if (!suffix) continue ;
        // Append the right value to the name
        tmp = result ;
        result = Strings::save(result,",",suffix) ; // open with parenthesis, separate with comma
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }

    if (!first) {
        // Close with parenthesis
        tmp = result ;
        result = Strings::save(result, ")") ;
        Strings::free(tmp) ;
    }
    if (block_config) {
        unsigned uniq_count = block_config->GetHashedNum(result) ;
        tmp = result ;
        char str[32] ;
        sprintf(str, "%d", uniq_count) ;
        result = Strings::save(result, "_uniq_", str) ;
        Strings::free(tmp) ;
    }
    // VIPER #7853 : Create short name if created name is very long
    if (IsStaticElab()) return CreateShortName(Name(), result) ;
    return result ;
}

char *VhdlConfigurationId::CreateUniqueName(const VhdlBlockConfiguration *block_config) const
{
    // Mmmm. Which name. Use entity name with its set generics
    if (!_entity) return 0 ;
    return _entity->CreateUniqueName(block_config) ;
}
char *VhdlPackageId::CreateUniqueName(const VhdlBlockConfiguration * /*block_config*/) const
{
    // Create a Cell name for this component, with set generics
    char *result = Strings::save(OrigName()) ;
    unsigned i ;
    VhdlIdDef *generic ;
    VhdlValue *val ;
    char *suffix, *tmp ;
    VhdlIdDef *type = 0 ;

    // Need to use characters in the name that are syntactically invalid VHDL,
    // to avoid name collisions with normal design unit names (issue 1295)
    unsigned first = 1 ;
    FOREACH_ARRAY_ITEM(_generics, i, generic) {
        if (!generic) continue ;

        // Don't encorporate physical types in unique name. They would create
        // very long names, which they don't affect the functionality of the design.
        // Vital package contains Array of TIME types (VitalDelayType01...etc)
        // and Array-of-Array-of-Time (VitalDelayArrayType01...etc). Issue 1939.
        // So rule out any type which is 'physical' at the scalar level :
        //VhdlIdDef *type = generic->Type() ;
        //while (type && type->IsArrayType()) type = type->ElementType() ;
        //if (!type) continue ; // something bad is wrong with this generic's type.
        // Discard if physical
        //if (type->IsPhysicalType()) continue ;

        // Now get the value for this generic, and create an image for it.
        //val = generic->Value() ;
        //if (!val) continue ;

        // Create image for the value
        if (generic->IsGenericType()) {
            type = generic->ActualType() ; // Get associated type name
            suffix = type ? Strings::save(type->Name()): 0 ;
        } else if (generic->IsSubprogram()) {
            type = generic->GetActualId() ;
            suffix = type ? Strings::save(type->Name()): 0 ;
        } else if (generic->IsPackage()) {
            type = generic->GetElaboratedPackageId() ;
            suffix = type ? type->CreateUniqueName(0): 0 ;
        } else {
            // Don't encorporate physical types in unique name. They would create
            // very long names, which they don't affect the functionality of the design.
            // Vital package contains Array of TIME types (VitalDelayType01...etc)
            // and Array-of-Array-of-Time (VitalDelayArrayType01...etc). Issue 1939.
            // So rule out any type which is 'physical' at the scalar level :
            type = generic->Type() ;
            while (type && type->IsArrayType()) type = type->ElementType() ;
            if (!type) continue ; // something bad is wrong with this generic's type.
            // Discard if physical
            if (type->IsPhysicalType()) continue ;

            // Now get the value for this generic, and create an image for it.
            val = generic->Value() ;
            if (!val) continue ;
            suffix = val->ImageShort() ;
        }
        //suffix = val->Image() ;
        if (!suffix) continue ;

        // Append the generic value to the name
        tmp = result ;
        result = Strings::save(result, (first) ? "(" : ",",suffix) ; // open with parenthesis, separate with comma
        first = 0 ;
        Strings::free(suffix) ;
        Strings::free(tmp) ;
    }
    if (!first) {
        // Close with parenthesis
        tmp = result ;
        result = Strings::save(result, ")") ;
        Strings::free(tmp) ;
    }
    // VIPER #7853 : Create short name if created name is very long
    if (IsStaticElab()) return CreateShortName(Name(), result) ;
    return result ;
}

/**********************************************************/
//  Constraint. Set/Get the constraint of an id
/**********************************************************/

// Set constraint ( hand-over to id)
void VhdlIdDef::SetConstraint(VhdlConstraint *constraint)
{
    delete _constraint ;
    _constraint = constraint ;
}

// Get (pointer to) the constraint
VhdlConstraint *VhdlIdDef::Constraint() const   { return _constraint ; }
VhdlConstraint *VhdlPackageInstElement::Constraint() const
{
    return (_actual_id) ? _actual_id->Constraint(): 0 ;
}
// Take constraint away from the id
VhdlConstraint *VhdlIdDef::TakeConstraint()     { VhdlConstraint *r=_constraint ; _constraint=0 ; return r ; }
VhdlConstraint *VhdlPackageInstElement::TakeConstraint()
{
    return (_actual_id) ? _actual_id->TakeConstraint() : 0 ;
}

/**********************************************************/
//  Value. Set/Get the initial value of an id
/**********************************************************/

void VhdlIdDef::SetValue(VhdlValue *value)
{
    delete _value ;
    _value = value;

    // Viper #7288: Now that the val and id_constraint are compatible, impose
    // id_constraint on value->range. e.g. their direction and bounds must now match
    if (_value && _value->GetRange()) _value->ConstraintBy(_constraint) ;
}

void VhdlConstantId::SetValue(VhdlValue *value)
{
    delete _value ;
    _value = value;

    // Viper #7288: Now that the val and id_constraint are compatible, impose
    // id_constraint on value->range. e.g. their direction and bounds must now match
    // VIPER Issue #8404: missed to update constraint here during 7288 fix.
    if (_value && _value->GetRange()) _value->ConstraintBy(_constraint) ;

    if (_decl && _decl!=this && value) {
        // Set the value of the deferred id too :
        if (!_decl->Value()) {
            _decl->SetValue(value->Copy()) ;
        }
    }
}
void VhdlPackageInstElement::SetValue(VhdlValue *value)
{
    if (_actual_id) _actual_id->SetValue(value) ;
}

void VhdlConstantId::SetConstraint(VhdlConstraint *constraint)
{
    // Viper 4884 Set constraints of the _decl also
    delete _constraint ;
    _constraint = constraint;

    if (_decl && _decl!=this && constraint) {
        // Set the value of the deferred id too :
        VhdlConstraint *decl_constraint = _decl->Constraint() ;
        if (!decl_constraint) {
            _decl->SetConstraint(constraint->Copy()) ;
        } else if (decl_constraint->IsUnconstrained() && !constraint->IsUnconstrained()) {
            decl_constraint->ConstraintBy(constraint) ;
        } else if (!decl_constraint->IsUnconstrained() && !constraint->IsUnconstrained()) {
            (void) decl_constraint->CheckAgainst(constraint, _decl, 0) ;
        }
    }
}
void VhdlPackageInstElement::SetConstraint(VhdlConstraint *constraint)
{
    if (_actual_id) _actual_id->SetConstraint(constraint) ;
}
// Get pointer to initial value of id
VhdlValue *VhdlIdDef::Value() const     { return _value ; }
VhdlValue *VhdlPackageInstElement::Value() const
{
    return (_actual_id) ? _actual_id->Value(): 0 ;
}

// Take value away from id
VhdlValue *VhdlIdDef::TakeValue()       { VhdlValue *r=_value ; _value=0 ; return r ;  }

VhdlValue *VhdlPackageInstElement::TakeValue()
{
    return (_actual_id) ? _actual_id->TakeValue(): 0 ;
}
// Check if id has a value
unsigned VhdlIdDef::HasValue() const    { return (_value) ? 1 : 0 ; }
unsigned VhdlPackageInstElement::HasValue() const
{
    return (_actual_id) ? _actual_id->HasValue(): 0 ;
}

/**********************************************************/
//  Attribute Setting
/**********************************************************/

void VhdlIdDef::SetAttribute(VhdlIdDef *attr, VhdlValue *value)
{
    if (!attr || !value) return ;

    // RD: Since 4/2007, we store the VhdlExpression on the instead.
    // We do elaboration of attribute value at USAGE time (in VhdlAttributeName::Elaborate).
    // That is because attribute elaboration is dynamic (LRM 12.3.2.1 'expression need not be static'.
    // So this routine only has effect now during RTL elaboration (transferring attributes to netlist)
}

// Since 4/2007,
void VhdlTypeId::SetAttribute(VhdlIdDef *attr, VhdlValue *value)
{
    if (!attr || !value) return ;

    // First execute the normal attribute setting
    VhdlIdDef::SetAttribute(attr, value) ;

    // Encoding, below, applies only to enumeration types.
    if (!IsEnumerationType()) return ;

    VhdlIdDef *elem ;
    VhdlValue *elem_bit_val ;
    unsigned i ;

    // Now check for type encoding....
    // FIX ME : Warnings come from place of type decl. Should be from attribute spec.
    if (Strings::compare_nocase(attr->Name(), "logic_type_encoding")) {
        // Exemplar way of encoding a bit
        // Exemplar encodes bit-values in a clean string. Length should fit
        // with the number of enum values :
        if (NumOfEnums() != value->NumElements()) {
            Warning("synthesis directive attribute %s ignored ; incorrect number of bit values for type %s", attr->Name(), Name()) ;
            return ;
        }

        // Now set the elements
        unsigned one_occured=0 , zero_occured=0 ;
        for (i=0;i<value->NumElements();i++) {
            elem_bit_val = value->ValueAt(i) ; // element value in the string
            if (!elem_bit_val) continue ; // Error ?
            elem = GetEnumAt(i) ; // Get my value at this number
            VERIFIC_ASSERT(elem) ; // We tested right number above..
            // Get the matching enumeration item
            // Position() returns the unsigned number that the character represents in the VHDL
            // character type ; this corresponds position in the ASCII set. So, works fine for 'character' type !
            char c = (char)elem_bit_val->Position() ;
            // VIPER 1751: Changed for metalogical values
            // If logic_type_encoding uses '0' or '1' for more than
            // one enum literal then we assume only the first one as
            // real '0' or '1', all the rest ones are considered as
            // 'L' or 'H'
            switch (c) {
            case '0' :
                if (zero_occured) {
                    c = 'L' ;
                } else {
                    zero_occured=1 ;
                }
                break ;
            case '1' :
                if (one_occured) {
                    c = 'H' ;
                } else {
                    one_occured=1 ;
                }
                break ;
            default: break ;
            }
            elem->SetBitEncoding(c) ;

            elem->SetUserEncoded() ; // flag this element as 'user' encoded (VIPER 7182, 7346)
        }
#if 1 // VIPER #6224 : Enable this encoding setting code only for those ids on which
       // encoding setting in analyzer is not possible
        // Viper #3799 : Following code is now moved to VhdlTypeId::SetAttribute in
        // VhdlIdDef.cpp. Now we set enum encoding in analysis during ResolveSpecs
    } else if (Strings::compare_nocase(attr->Name(), "enum_encoding")) {
        // VIPER #6224 : If enum encoding is already set in analyzer, do not overwrite that
        VhdlIdDef *enum_id = (NumOfEnums() > 0) ? GetEnumAt(0) : 0 ;
        if (enum_id && enum_id->GetEncoding()) return ;

        // Synopsys uses 'enum_encoding' for BOTH bit-encoding as well as
        // multi-bit (state) encoding.

        // Keep track of the number of enum fields counted in the attribute value
        unsigned elem_nr = 0 ;
        // allocate enough space for at least one element value (total string length should do)
        char *buffer = Strings::allocate(value->NumElements()+1) ;
        // Set a pointer that will fill with value of one element
        char *buffer_ptr = buffer ;
        buffer[0] = '\0' ; // Start clean
        // keep track of size of element values
        unsigned long size = 0 ;

        // Now run over the characters in the value
        for (i=0;i<value->NumElements()+1;i++) {
            // We go one character further than the string length,
            // to handle the last element too (mimic that there is a space at the end of the string)
            elem_bit_val = (i<value->NumElements()) ? value->ValueAt(i) : 0 ; // element value in the string
            if (!elem_bit_val || elem_bit_val->Position()==' ') {
                if (buffer_ptr == buffer) {
                    // previous character was a space skip this
                    continue ;
                }

                // We are about to go to the next element
                // deal with the previous one ; close the buffer :
                *buffer_ptr = '\0' ;
                // Test for correct length
                if (size == 0) {
                    size = Strings::len(buffer) ;
                } else {
                    if (size != Strings::len(buffer)) {
                        Warning("enum_encoding elements should all have the same length") ;
                        break ;
                    }
                }

                // Get the enumeration literal to assign this value to
                elem = GetEnumAt(elem_nr++) ; // my element number
                if (!elem) {
                    // Warning("enum_encoding attribute specifies too many literals for type %s",Name()) ;
                    break ;
                }

                // Assign encoding : bit or multi-bit
                if (size==1) {
                    elem->SetBitEncoding(*buffer) ;
                } else {
                    elem->SetEncoding(buffer) ;
                }

                elem->SetUserEncoded() ; // flag this element as 'user' encoded (VIPER 7182, 7346)

                // go to next element
                buffer_ptr = buffer ;
            } else {
                // next character in the same element
                *buffer_ptr++ = (char)elem_bit_val->Position() ;
            }
        }
        if (elem_nr != NumOfEnums()) {
            Warning("enum_encoding attribute specifies incorrect number of valid encoding values for type %s ; encoding ignored", Name()) ;
            // Kill off all encoding (we will default to binary)
            for (i=0; i<NumOfEnums(); i++) {
                elem = GetEnumAt(i) ;
                elem->SetEncoding(0) ;
            }
        }
        Strings::free(buffer) ;
#endif // Commented out for Viper #3799 : Enum Encoding is now set at analyzer
    }
}

/**********************************************************/
//  Subprogram execution
/**********************************************************/

/************************/
// Unary operator calls
/************************/
VhdlValue *VhdlIdDef::ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, VhdlTreeNode * /*caller_node*/)
{
    // Can this happen ? Did type-inference fail to identify a subprogram ?
    if (unary_value) unary_value->Error("%s is not a subprogram", Name()) ;
    return 0 ;
}
VhdlValue *VhdlSubprogramId::ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node)
{
    // User defined function
    if (_pragma_function) {
        // evaluate this as pragma function
        return ElaboratePragmaFunction(unary_value, 0, target_constraint, df) ;
    }
    // VIPER #5918 : VHDL-2008 enhancement : If this is subprogram instance, we
    // need to elaborate instance specified uninstantiated subprogram with generics
    if (_sub_inst) {
        // User-defined operator
        // Elaborate this, with ordered arguments
        Array args(2) ;
        args.InsertLast(unary_value) ;
        return _sub_inst->ElaborateSubprogramInst(&args,target_constraint,df, caller_node) ;
    }

    if (_body) {
        // User-defined operator
        // Elaborate this, with ordered arguments
        Array args(2) ;
        args.InsertLast(unary_value) ;
        return _body->ElaborateSubprogram(&args,target_constraint,df, caller_node, 0) ;
    }
    unary_value->Error("subprogram %s does not have a body", Name()) ;
    return 0 ;
}

VhdlValue *VhdlOperatorId::ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * /*caller_node*/)
{
    // Build-in Unary VHDL operator call
    if (!unary_value) return 0 ;

    // Issue 2355 : LRM 7.2.1 : some operators returning an array, the index range follows the LEFT operand.
    // So, for these, adjust (pass) unconstrained target constraint to left arg :
    VhdlConstraint *arg_constraint = 0 ;
    if (target_constraint && target_constraint->IsUnconstrained()) {
        switch (_oper_type) {
        case VHDL_not :
            arg_constraint = target_constraint ;
            break ;
        default :
            break ;
        }
    }

    VhdlValue *val = unary_value->Evaluate(arg_constraint,df,0) ;
    if (!val) return 0 ; // Something went wrong

    // Execute the operation :
    VhdlValue *result = val->UnaryOper(_oper_type, unary_value) ;

    // Issue 1632 : run the result value through a range check :
    const VhdlIdDef *type = Type() ;

    if (!result && type) {
        switch (_oper_type) {
        case VHDL_minimum:
        case VHDL_maximum:
        // Also include here the logical reduction operators 1076-2008 LRM 9.2.2
        case VHDL_and:
        case VHDL_or:
        case VHDL_nand:
        case VHDL_nor:
        case VHDL_xor:
        case VHDL_xnor:
        {
            // VHDL 1076-2008 LRM section 5.3.2.4
            // min/max will return null for null array
            VhdlConstraint *type_constraint = type->Constraint() ;
            VhdlValue *min = type_constraint ? type_constraint->Low() : 0 ;
            VhdlValue *max = type_constraint ? type_constraint->High() : 0 ;

            switch (_oper_type) {
            case VHDL_minimum:
            case VHDL_and:
            case VHDL_nor:
            case VHDL_xnor:
                result = max ? max->Copy() : 0 ;
                break ;
            case VHDL_maximum:
            case VHDL_or:
            case VHDL_xor:
            case VHDL_nand:
                result = min ? min->Copy() : 0 ;
                break ;
            default:
                break ;
            }

            break ;
        }
        default: break ;
        }
    }
    // Viper #5753: Do not error on the built-in operators. Hence the from
    // argument passes is null. Only adjust the widths, if appropriate
    if (result && type) (void) result->CheckAgainst(type->Constraint(), /*from*/ 0, 0) ;

    return result ;
}

/*************************/
// Binary operator calls
/*************************/

VhdlValue *VhdlIdDef::ElaborateOperator(VhdlExpression *left, VhdlExpression * /*right*/, VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, VhdlTreeNode * /*caller_node*/)
{
    // Can this happen ? Did type-inference fail to identify a subprogram ?
    if (left) left->Error("%s is not a subprogram", Name()) ;
    return 0 ;
}

VhdlValue *VhdlSubprogramId::ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node)
{
    // execution of a user-defined function

    if (_pragma_function) {
        // Now evaluate this as pragma function
        return ElaboratePragmaFunction(left, right, target_constraint, df) ;
    }

    // VIPER #5918 : VHDL-2008 enhancement : If this is subprogram instance, we
    // need to elaborate instance specified uninstantiated subprogram with generics
    if (_sub_inst) {
        // User-defined operator call
        // Elaborate this, with ordered arguments
        Array args(2) ;
        args.InsertLast(left) ;
        args.InsertLast(right) ;
        return _sub_inst->ElaborateSubprogramInst(&args,target_constraint,df, caller_node) ;
    }
    // Get the definition
    if (_body) {
        // User-defined operator call
        // Elaborate this, with ordered arguments
        Array args(2) ;
        args.InsertLast(left) ;
        args.InsertLast(right) ;
        return _body->ElaborateSubprogram(&args,target_constraint,df, caller_node, 0) ;
    }
    left->Error("subprogram %s does not have a body", Name()) ;
    return 0 ;
}

VhdlValue *VhdlOperatorId::ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * /*caller_node*/)
{
    if (!left || !right) return 0 ;

    VhdlTreeNode *caller_node = df ? df->GetCallerTreeNode() : 0 ;
    if (!caller_node) caller_node = left ;

    // Execution of a predefined operator

    // Issue 2355 : LRM 7.2.1 : some operators returning an array, the index range follows the LEFT operand.
    // So, for these, adjust (pass) unconstrained target constraint to left arg :
    VhdlConstraint *left_constraint = 0 ;
    if (target_constraint && target_constraint->IsUnconstrained()) {
        switch (_oper_type) {
        case VHDL_and : // LRM 7.2.1 :
        case VHDL_or :
        case VHDL_nand :
        case VHDL_nor :
        case VHDL_xor :
        case VHDL_xnor :
        case VHDL_sll : // LRM 7.2.2
        case VHDL_sla :
        case VHDL_rol :
        case VHDL_srl :
        case VHDL_sra :
        case VHDL_ror :
        // case VHDL_AMPERSAND: // & has complicated constraint handling, but I think post-elab 'AdjustToValue' is accurate. So no need to adjust here.
            left_constraint = target_constraint ;
            break ;
        default :
            break ;
        }
    }

    // Evaluate LHS :
    VhdlValue *left_val = left->Evaluate(left_constraint,df,0) ;

    // LRM 7.2 :
    // 'short-circuit' bit operator with left operand constant requires special treatment
    // all other operations should also evaluate the RHS.
    if (left_val && left_val->IsBit() && left_val->IsConstant()) {
        // Check if it is a 'short-circuit' operator.
        switch (_oper_type) {
        case VHDL_and :
            // If left value is FALSE, return FALSE
            if (!left_val->IsTrue()) return left_val ;
            break ;
        case VHDL_or :
            // If left value is TRUE, return TRUE
            if (left_val->IsTrue()) return left_val ;
            break ;
        case VHDL_nand :
            // if left value is FALSE, return TRUE
            if (!left_val->IsTrue()) return left_val->UnaryOper(VHDL_not, caller_node) ;
            break ;
        case VHDL_nor :
            // if left value is TRUE, return FALSE
            if (left_val->IsTrue()) return left_val->UnaryOper(VHDL_not, caller_node) ;
            break ;
        default : break ;
        }
    }

    // Evaluate RHS :
    VhdlValue *right_val = right->Evaluate(0,df,0) ;
    // VIPER #3914 : 'short-circuit' bit operator with right operand constant requires special treatment
    if (right_val && right_val->IsBit() && right_val->IsConstant()) {
        // Check if it is a 'short-circuit' operator.
        switch (_oper_type) {
        case VHDL_and :
            // If right value value is FALSE, return FALSE
            if (!right_val->IsTrue()) { delete left_val ; return right_val ; }
            // If right value value is TRUE, return left
            if (right_val->IsTrue())  { delete right_val ; return left_val ; }
            break ;
        case VHDL_or :
            // If right value is TRUE, return TRUE
            if (right_val->IsTrue()) { delete left_val ; return right_val ; }
            // If right value value is FALSE, return left
            if (!right_val->IsTrue()) { delete right_val ; return left_val ; }
            break ;
        case VHDL_nand :
            // if right value is FALSE, return TRUE
            if (!right_val->IsTrue()) { delete left_val ; return right_val->UnaryOper(VHDL_not, caller_node) ; }
            // If right value value is TRUE, return not of left
            if (right_val->IsTrue() && left_val)  { delete right_val ; return left_val->UnaryOper(VHDL_not, caller_node) ; }
            break ;
        case VHDL_nor :
            // if right value is TRUE, return FALSE
            if (right_val->IsTrue()) { delete left_val ; return right_val->UnaryOper(VHDL_not, caller_node) ; }
            // If right value value is FALSE, return not of left
            if (!right_val->IsTrue() && left_val)  { delete right_val ; return left_val->UnaryOper(VHDL_not, caller_node) ; }
            break ;
        default : break ;
        }
    }

    // Viper #5613: For eq / neq operators even when operands are non-constants,
    // if they are of different widths, result is known statically
    if (IsStaticElab() && (!left_val || !right_val) && (_oper_type == VHDL_EQUAL || _oper_type == VHDL_NEQUAL)) {
        left_constraint = left->EvaluateConstraint(df, 0) ;
        verific_uint64 left_length = 0 ;
        if (left_constraint && !left_constraint->IsRangeConstraint() && !left_constraint->IsUnconstrained()) left_length = left_constraint->Length() ;

        VhdlConstraint *right_constraint = right->EvaluateConstraint(df, 0) ;
        verific_uint64 right_length = 0 ;
        if (right_constraint && !right_constraint->IsRangeConstraint() && !right_constraint->IsUnconstrained()) right_length = right_constraint->Length() ;

        // Viper 7318: Take the length from value if constraint is null. Works
        // only for composite values. We anyway cannot calculate the length of
        // nonconst because we calculate size based on encoding present.
        if (!left_constraint && left_val) left_length = left_val->NumElements() ;
        if (!right_constraint && right_val) right_length = right_val->NumElements() ;

        delete left_constraint ;
        delete left_val ;
        delete right_constraint ;
        delete right_val ;
        
        if (right_length && left_length && (right_length != left_length)) {
            VhdlValue *result = 0 ;
            if (_oper_type == VHDL_EQUAL) {
                (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                left->Warning("comparison between unequal length arrays always returns %s", "FALSE") ;
            } else { // oper_type == VHDL_NEQUAL
                (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(1)) : result = new VhdlNonconstBit(Pwr()) ;
                left->Warning("comparison between unequal length arrays always returns %s", "TRUE") ;
            }

            return result ;
        }

        return 0 ; // we are still conservative for access values
    }

    if (!left_val || !right_val) { delete left_val ; delete right_val ; return 0 ; }

    VhdlValue *result = 0 ;
    // NOTE : Predef'ed operators have no special constraint requirements,
    // so we don't need to adjust any (unconstrained) target constraint.
    // This will be done by the value assignment operation

    // Special case : & operator
    switch (_oper_type) {
    case VHDL_AMPERSAND:
        {
            // could be
            //     elem  & elem  -> array
            //     elem  & array -> array
            //     array & elem  -> array
            //     array & array -> array

            // Test argument type against return type to figure out what
            // we have here. Then construct the return value.
            unsigned left_is_elem = (ArgType(0) == Type()) ? 0 : 1 ;
            unsigned right_is_elem = (ArgType(1) == Type()) ? 0 : 1 ;
            if (left_is_elem && right_is_elem) { // both are elements
                Array *a = new Array(2) ;
                a->InsertLast(left_val) ;
                a->InsertLast(right_val) ;
                return new VhdlCompositeValue(a) ;
            } else if (left_is_elem) { // right is array
                right_val->PrependElement(left_val) ;
                return right_val ;
            } else if (right_is_elem) { // left is array
                left_val->AppendElement(right_val) ;
                return left_val ;
            } else { // Both are arrays
                // Append 'right' elements to 'left' array. Pointers only
                for (unsigned i=0; i<right_val->NumElements(); i++) {
                    left_val->AppendElement(right_val->TakeValueAt(i)) ;
                }
                // Now delete the shell of 'right' (elements are moved out already)
                delete right_val ;

                if (!left_val->NumElements()) {
                    // Viper #7758 : Both are null ranges
                    // Vhdl 2008 Section 9.2.5 (a)
                    // Vhdl 2002 Section 7.2.4 (a)
                    VhdlConstraint *right_constraint = right->EvaluateConstraint(df, 0) ;
                    if (right_constraint && target_constraint) target_constraint->ConstraintBy(right_constraint) ;
                    delete right_constraint ;
                }

                return left_val ;
            }
        }
    case VHDL_STHAN:
    case VHDL_SEQUAL:
    case VHDL_GTHAN:
    case VHDL_GEQUAL:
    case VHDL_EQUAL:
    case VHDL_NEQUAL:
        {
            // VIPER Issue #6088 : for comparison of a constant with
            // a variable, result can be known in certain cases when
            // the constant lies outside the variable constraint. In
            // such cases the actual value of the variable is not
            // relevant
            if (!right_val->IsConstant() && !left_val->IsConstant()) break ;
            if (right_val->IsConstant() && left_val->IsConstant()) break ;

            VhdlValue *const_val = right_val->IsConstant() ? right_val : left_val ;
            unsigned left_constant = (const_val == left_val) ;
            VhdlExpression *nc_expr = left_constant ? right : left ;
            VhdlExpression *const_expr = left_constant ? left : right ;

            unsigned is_metalogical = const_val && const_val->IsMetalogical() ;
            is_metalogical = is_metalogical && const_expr && const_expr->IsConstant() ;
            unsigned is_boolean_equal = _boolean_type && (_oper_type == VHDL_EQUAL) ;
            if (vhdl_file::IsVhdl2008() && is_boolean_equal && is_metalogical) {
                // Viper #7579
                // Vhdl 2008 LRM 16.8.2.4.4 and 16.8.2.4.10
                // Comparison of non-constant with metalogical values to be considered FALSE
                result = new VhdlEnum(_boolean_type->GetEnumAt(0)) ;
                delete right_val ;
                delete left_val ;
                return result ;
            }

            VhdlConstraint *nc_constraint = nc_expr ? nc_expr->EvaluateConstraint(df, 0) : 0 ;
            VhdlValue *low = nc_constraint ? nc_constraint->Low() : 0 ;
            VhdlValue *high = nc_constraint ? nc_constraint->High() : 0 ;
            if (!nc_constraint || !low || !high || !nc_constraint->IsRangeConstraint()) {
                delete nc_constraint ;
                break ;
            }

            if (high->GetEnumId()) { // enum type range
                // Viper #6173:
                // For EnumId type values, LT/GT does not work unless
                // const_val itself is also VhdlEnum value
                //
                // GT/LT otherwise works with integers, which may be encoded
                // values while comparisons with the Position of enum_id is
                // more appropriate. e.g. for std_ulogic, Integer returns '0'
                // for both High() (which is '-') and Low() (which is 'U')

                // find the corresponding enum-id for this const_val, if exists
                VhdlIdDef *enum_type = high->GetType() ;
                unsigned num_enum = enum_type ? enum_type->NumOfEnums() : 0 ;
                unsigned enum_idx = 0 ;
                unsigned is_in_range = 0 ;
                for (enum_idx = 0 ; enum_idx < num_enum ; ++enum_idx) {
                    VhdlIdDef *enum_i = enum_type ? enum_type->GetEnumAt(enum_idx) : 0 ;
                    VhdlValue *value_i = enum_i ? enum_i->Value() : 0 ;
                    if (!value_i) continue ;

                    if (value_i->IsEqual(const_val)) is_in_range = 1 ;
                    if (is_in_range) break ;
                }

                if (!is_in_range) (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                delete nc_constraint ;

                break ;
            }

            unsigned const_is_more = high->LT(const_val) ;
            unsigned const_is_less = const_val->LT(low) ;
            unsigned is_low_equal = const_val->IsEqual(low) ;
            unsigned is_high_equal = const_val->IsEqual(high) ;
            delete nc_constraint ;

            switch (_oper_type) {
            case VHDL_STHAN:
                if ((is_high_equal && left_constant) || (is_low_equal && !left_constant)) {
                    // return false
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                    break ;
                }

            case VHDL_SEQUAL:
                if ((const_is_less && left_constant) || (const_is_more && !left_constant)) {
                    // return true
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(1)) : result = new VhdlNonconstBit(Pwr()) ;
                } else if ((const_is_more && left_constant) || (const_is_less && !left_constant)) {
                    // return false
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                }
                break ;
            case VHDL_GTHAN:
                if ((is_low_equal && left_constant) || (is_high_equal && !left_constant)) {
                    // return false
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                    break ;
                }

            case VHDL_GEQUAL:
                if ((const_is_less && left_constant) || (const_is_more && !left_constant)) {
                    // return false
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                } else if ((const_is_more && left_constant) || (const_is_less && !left_constant)) {
                    // return true
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(1)) : result = new VhdlNonconstBit(Pwr()) ;
                }
                break ;
            case VHDL_EQUAL:
                if (const_is_less || const_is_more) {
                    // return false
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(0)) : result = new VhdlNonconstBit(Gnd()) ;
                }
                break ;
            case VHDL_NEQUAL:
                if (const_is_less || const_is_more) {
                    // return true
                    (_boolean_type) ? result = new VhdlEnum(_boolean_type->GetEnumAt(1)) : result = new VhdlNonconstBit(Pwr()) ;
                }
                break ;
            default: break ;
            }

            break ;
        }
    case VHDL_and:
    case VHDL_or:
    case VHDL_nand:
    case VHDL_nor:
    case VHDL_xor:
    case VHDL_xnor:
        {
            if (vhdl_file::IsVhdl2008() && left_val->NumBits() != right_val->NumBits()) {
                // Vhdl2008: Section 9.2.2: new semantics "and" (bit, bit_vector) etc..
                if (left_val->NumBits() == 1) {
                    VhdlNonconst *lv_nc = left_val->ToNonconst() ;

                    lv_nc->SetSigned() ;
                    lv_nc->Adjust(right_val->NumBits()) ;

                    left_val = lv_nc->ToComposite(right_val->NumBits()) ;
                }

                if (right_val->NumBits() == 1) {
                    VhdlNonconst *rv_nc = right_val->ToNonconst() ;

                    rv_nc->SetSigned() ;
                    rv_nc->Adjust(right_val->NumBits()) ;

                    right_val = rv_nc->ToComposite(left_val->NumBits()) ;
                }
            }
            break ;
        }
    default : break ;
    }

    // Go through normal predefined operator execution
    if (result) {
        // Memory leak fix: Delete these since result is already set:
        delete left_val ; left_val = 0 ;
        delete right_val ; right_val = 0 ;
    } else {
        // This will absorb both left_val and right_val:
        result = left_val->BinaryOper(right_val, _oper_type, caller_node) ;
    }

    // Issue 1632 : run the result value through a range check :
    const VhdlIdDef *type = Type() ;
    if (result && type) {
        // VIPER #4300: Don't issue error for nonconstant branch in static elaboaration
        if (!(IsStaticElab() && df && df->IsNonconstExpr()))
        {
            // Viper #5753: Do not error on the built-in operators. Hence the from
            // argument passes is null. Only adjust the widths, if appropriate
            (void) result->CheckAgainst(type->Constraint(), /*from*/ 0, 0 ) ;
        }
    }

    return result ;
}

// Regular (named) function or procedure call
VhdlValue *VhdlIdDef::ElaborateSubprogram(Array * /*arguments*/, VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, VhdlTreeNode * /*caller_node*/, VhdlIdDef * /*subprog_inst*/)
{
    Error("%s is not a subprogram", Name()) ;
    return 0 ;
}

VhdlValue *VhdlSubprogramId::ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode * caller_node, VhdlIdDef *subprog_inst)
{
    // VIPER #5918 : LRM 4.2.1 Note 1 : Reference to the uninstantiated subprogram within
    // the uninstantiated subprogram is interpreted as a reference to the instance
    // Check whether it is recursive call
    VhdlDataFlow *tmp_df = subprog_inst ? 0 : df ;
    unsigned is_recursion = 0 ;
    while (tmp_df && !_sub_inst) {
        VhdlScope *scope = tmp_df->GetEnclosingScope() ;
        VhdlIdDef *owner_id = scope ? scope->GetOwner(): 0 ;
        if (owner_id == this) {
            is_recursion = 1 ;
            break ;
        }
        tmp_df = tmp_df->Owner() ;
    }
    if (is_recursion) {
        VhdlIdDef *inst_id = df->GetSubprogInstanceId() ;
        if (inst_id) return inst_id->ElaborateSubprogram(arguments, target_constraint, df, caller_node, subprog_inst) ;
    }
    if (_pragma_function) {
        // Distinguish between a function and a procedure
        if (IsProcedure()) {
            // procedure with a pragma
            // These are often the 'textio' procedures (VIPER 3341),
            // Procedures can have more than two arguments, and also can have 'out' and 'inout' arguments.
            // Pass the whole thing through :
            ElaboratePragmaProcedure(arguments, df) ;
            return 0 ; // procedures do not need to return a value.
        } else {
            // function with a pragma
            // Evaluate left and right side
            // Don't need more than two arguments
            // Viper 7047 : do a null check on arguments.
            VhdlExpression *left  = (arguments && arguments->Size()>0) ? (VhdlExpression*)arguments->At(0) : 0 ;
            VhdlExpression *right = (arguments && arguments->Size()>1) ? (VhdlExpression*)arguments->At(1) : 0 ;

            // Now evaluate this as pragma function
            return ElaboratePragmaFunction(left, right, target_constraint, df) ;
        }
    }

    // VIPER #5918 : VHDL-2008 enhancement : If this is subprogram instance, we
    // need to elaborate instance specified uninstantiated subprogram with generics
    if (_sub_inst) {
        return _sub_inst->ElaborateSubprogramInst(arguments,target_constraint,df, caller_node) ;
    }
    if (_body) {
        // user-defined function called normally
        return _body->ElaborateSubprogram(arguments,target_constraint,df, caller_node, subprog_inst) ;
    }

    if (!arguments && Strings::compare(Name(),"now")) {
        // VIPER #3367: Return 0 value (not NULL) only from initial expressions:
        if (df && df->IsInInitial()) return (new VhdlPhysicalValue((verific_int64)0)) ;
        // 'hack' to get function 'now' in standard package ignored
        // VIPER #2858 : Suppress message for static elaboration
        if (IsRtlElab()) {
            if (!caller_node) caller_node = this ; // get line/file for this message
            caller_node->Warning("function call NOW ignored") ;
        }
        return 0 ;
    }

    Error("subprogram %s does not have a body", Name()) ;
    return 0 ;
}

VhdlValue *VhdlOperatorId::ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef * /*subprog_inst*/)
{
    // built-in operator called as a function

    // catch file operations : LRM 3.4.1 :
    // Apart from 'endfile', these guys are 'procedures' and not normal operators (which are all functions with 1 or 2 args).
    switch (_oper_type) {
    case VHDL_file_open :
        {
        // file_open has at least some arguments :
        if (!arguments) return 0 ;

        // VIPER 3741/3744 : Do not open a file unless we are in 'initial' mode.
        if (!df || !df->IsInInitial()) {
            // might need to invalidate the dataflow..
            return 0 ;
        }

        // keep track of error status : (follow file_open_status enumeration : 0=OK, 1=ERROR, 2=NAME_ERROR, 3=MODE_ERROR
        //      type file_open_status is ( open_ok, status_error, name_error, mode_error);
        unsigned error_status = 0 ;

        // There are two forms of file_open :
        //  (1) file_open ( file : file_type; external_name : in string; open_kind : in file_open_kind := read_mode)
        //  (2) file_open ( status : out file_open_status; <previous args> )
        // Distinguish between the two by checking the first argument type of this type-inferred operator :
        unsigned file_open_form = 0 ;
        VhdlIdDef *first_arg_type = ArgType(0) ;
        if (first_arg_type && !first_arg_type->IsFileType()) {
            file_open_form = 1 ; // second form. : first argument is 'file_open_status'.
        }
        // Use 'file_open_form' to obtain the correct argument for file, file_name and open_kind strings :

        // Open this file.
        // Get the file argument :
        VhdlExpression *file_arg = (arguments->Size()>=(1+file_open_form)) ? (VhdlExpression*)arguments->At(0+file_open_form) : 0 ;
        if (!file_arg) return 0 ; // should not happen ; Type-inference checked it.

        VhdlIdDef *file_id = file_arg->GetId() ;
        if (!file_id) return 0 ; //

        // Check if the file is already set :
        FILE *file = file_id->GetFile() ;
        if (file) {
            // if (file_open_form==0) { // issue error if this is the first file_open form (without status return)
            //     file_arg->Error("file is already open") ; // Needed ??
            // }
            // close the file.
            error_status = 1 ; // file already associated
            fclose(file) ;
            file = 0 ;
        }

        // Now get the file name :
        VhdlExpression *file_name_arg = (arguments->Size()>=(2+file_open_form)) ? (VhdlExpression*)arguments->At(1+file_open_form) : 0 ;
        if (!file_name_arg) return 0 ; // should not happen. Type-inference checked it.

        // Now get the file name : Elaborate '_file_logical_name'.
        VhdlValue *file_name_val = file_name_arg->Evaluate(0/*target_constraint?*/, df, 0) ;

        // Constant file name check
        if (!file_name_val || !file_name_val->IsConstant()) {
            file_name_arg->Error("expression is not constant") ; // maybe better message ?
            delete file_name_val ;
            return 0 ;
        }

        // Turn this into a string (Only type 'string' is allowed here, so use 'StringImage()')
        char *file_name_image = file_name_val->StringImage() ;
        delete file_name_val ;
        if (!file_name_image) return 0 ; // should never happen.

        // First strip the "'s from the file name
        const char *file_name = file_name_image ;
        if (*file_name_image=='"') {
            // Set 'file_name' pointer to after the leading " :
            file_name = file_name_image+1 ;
            // and cut off the trailing ".
            file_name_image[Strings::len(file_name_image)-1] = '\0' ;
        }

        // Check (open_mode) third argument :
        VhdlExpression *file_mode_arg = (arguments->Size()>=(3+file_open_form)) ? (VhdlExpression*)arguments->At(2+file_open_form) : 0 ;

        const char *open_mode = "r" ; // default : read
        if (file_mode_arg) {
            // VHDL 93 open mode : 'file_open' is an expression (of enumerated type 'file_open'). Evaluate it :
            VhdlValue *file_open_val = file_mode_arg->Evaluate(0,df,0) ;
            // Assume it is constant (and thus one of the values of enum type 'file_open'
            verific_int64 file_open_pos = (file_open_val) ? file_open_val->Position() : 0 /*default is an error*/;

            // Determine 'open_mode' :
            switch (file_open_pos) {
            case 0 : /* read_mode */  open_mode = "r" ; break ;
            case 1 : /* write_mode */ open_mode = "w" ; break ;
            case 2 : /* append_mode */ open_mode = "a" ; break ;
            default :
                error_status = 3 ;
                break ; // non-constant or other problem. Open in 'read' mode (default).
            }
            // delete the value
            delete file_open_val ;
        }

        // Now attempt to open the file :

        // Exception : textio file names "STD_INPUT" and "STD_OUTPUT" open 'stdin' and 'stdout' :
        // Don't open these, (and don't close them either).
        if (Strings::compare(file_name, "STD_INPUT")) {
            file = stdin ;
        } else if (Strings::compare(file_name, "STD_OUTPUT")) {
            file = stdout ;
        } else {
            file = fopen(file_name, open_mode) ;
            if (!file || !FileSystem::IsFile(file_name)) {
                if (file) { fclose(file) ; file = 0 ; }
                if (file_open_form==0) { // issue error if this is the first file_open form (without status return)
                    file_name_arg->Error("cannot open file '%s'",file_name) ; // MessageID !
                }
                // set correct error status :
                if (Strings::compare(open_mode,"r")) {
                    // cannot open in read mode : this means problem with the name (file does not exist)
                    error_status = 2 ;
                } else {
                    // file might exist, but cannot be opened in write mode
                    error_status = 3 ;
                }
            }
        }

        // Set the file on the file id (first argument) :
        file_id->SetFile(file) ;

        // cleanup
        Strings::free(file_name_image) ;

        // Finally : Set the file_open_status if needed :
        if (first_arg_type && file_open_form==1) {
            // first argument is 'file_open_status'.
            VhdlExpression *file_open_status_arg = (arguments->Size()>=1) ? (VhdlExpression*)arguments->At(0) : 0 ;
            if (!file_open_status_arg) return 0 ;
            // file_open_status is enum type :
            // Find the correct status enum id (use 'error_status' setting) :
            VhdlIdDef *file_open_status_id = first_arg_type->GetEnumAt(error_status) ;
            if (file_open_status_id) {
                VhdlValue *file_open_status_val = new VhdlEnum(file_open_status_id) ;
                // assign this to the first argument :
                file_open_status_arg->Assign(file_open_status_val,df,0) ;
            }
        }
        return 0 ; // done with this procedure
        }
    case VHDL_file_close :
        { // LRM 3.4.1
        // Get the file argument :
        VhdlExpression *file_arg = (arguments && arguments->Size()>=1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!file_arg) return 0 ; // something bad is wrong.

        // Get file id and the file pointer :
        VhdlIdDef *file_id = file_arg->GetId() ;
        FILE *file = (file_id) ? file_id->GetFile() : 0 ;
        // close the file (if still there).
        if (file) {
            fclose(file) ;
            // Also reset the file pointer in the id :
            file_id->SetFile(0) ;
        }
        return 0 ; // file_close is a procedure. Return 0 value.
        }
    case VHDL_flush:
        { // LRM 3.4.1.
        VhdlExpression *file_arg = (arguments && arguments->Size()>=1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!file_arg) return 0 ; // something bad is wrong.

        // Get file id and the file pointer :
        VhdlIdDef *file_id = file_arg->GetId() ;
        FILE *file = (file_id) ? file_id->GetFile() : 0 ;
        if (file) fflush(file) ;
        return 0 ; // done with this procedure
        }
    case VHDL_read :
        // FIX ME : LRM 3.4.1.
        return 0 ; // done with this procedure
    case VHDL_write :
        // FIX ME : LRM 3.4.1.
        return 0 ; // done with this procedure
    case VHDL_endfile :
        { // LRM 3.4.1
        // Get the file argument :
        VhdlExpression *file_arg = (arguments && arguments->Size()>=1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!file_arg) return 0 ;

        // Get file id and the file pointer :
        VhdlIdDef *file_id = file_arg->GetId() ;
        FILE *file = (file_id) ? file_id->GetFile() : 0 ;
        if (!file) {
            if (file_id && file_id->Name() && df && df->IsInInitial()) file_arg->Error ("cannot open file '%s'", file_id->Name()) ; // file_id really holds the variable name, not the file name. But it is the best info that we have here
            return 0 ; // VIPER 3610 : return 'failure' if file is not opened. Do NOT return a valid VHDL 'true'/'false' value.
        }

        // LRM 4.3.1 (ln 645-650) :
        // endfile return 'true' only if file is open and not at eof, and we can still do a subsequent read.
        unsigned result = 1 ;
        if (!feof(file)) {
            // file is open and not at eof.
            // endfile should return false if we can still do a subsequent 'read' on the file.
            // That means we should be able to read at least one character from it. Test that here.
            int c = fgetc(file) ;
            if (c!=EOF) {
                // a subsequent read will work.
                // push the character back into the stream
                (void) ungetc(c, file) ;
                result = 0 ; // not yet 'end of file'
            }
        }

        // return result as a boolean value. Operator's result type is boolean.
        VhdlIdDef *bool_type = Type() ;
        VhdlIdDef *result_id = (bool_type) ? bool_type->GetEnumAt((result) ? 1 : 0) : 0 ;
        if (result_id) {
            return new VhdlEnum(result_id) ;
        }
        return 0 ; // failure (cannot find type boolean).
        }
    // deallocate function call :
    case VHDL_deallocate :
        {
        VhdlExpression *arg = (arguments && arguments->Size()==1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!arg) return 0 ; // something bad is wrong.

        // Get access type id
        VhdlIdDef *access_id = arg->GetId() ;
        if (!access_id) return 0 ;

        // access_id can never be alias (LRM 6.1, 4.3.3)
        VhdlIdDef *type = access_id->Type() ;

        if (!type || !type->IsAccessType()) return 0 ; // something isn't right

        // get the access_value
        VhdlValue *val = 0 ;
        if (df && access_id->IsVariable()) {
            val = df->IdValue(access_id) ;
        }

        // static elab does not do anything meaningful here
        if (!val && df && IsStaticElab() && df->IsNonconstExpr()) return 0 ;

        // It may not be in the df (only initialized, not assigned)
        // Viper #7439: Update a new null value in the dataflow
        unsigned update_df = 0 ;
        // Modified after checking in #7286. Copy value when value from 'df'
        // is same as value set in identifier
        if (!val || (val == access_id->Value())) {
            val = access_id->Value() ;
            if (df && val && val->GetAccessedValue()) {
                val = val->Copy() ;
                update_df = 1 ;
            }
        }

        if (!val || !val->IsAccessValue()) return 0 ; // something isn't right

        VhdlValue *inner_value = val->GetAccessedValue() ;

        // if inner_value is accessed inside any other AccessValue, trying
        // to access it will lead to core dump : in line with what LRM says
        // but we do not want that.. in-stead a message for free memory
        // access would be a nice touch. -- Viper #7292
        // delete inner_value ;
        SetDeallocatedValue(inner_value) ; // Viper #7292 / 4464

        val->SetAccessedValue(0) ;
        if (update_df && df) df->SetAssignValue(access_id, val) ; // Viper #7439: update the df

        return 0 ;
        }
    case VHDL_rising_edge :
        {
        // Use first argument as the 'clock' in a rising edge
        // We should do some more checking here...
        VhdlExpression *arg = (arguments && arguments->Size()==1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!arg) return 0 ; // something bad is wrong.

        VhdlValue *left_val = arg->Evaluate(0,df,0) ;
        if (!left_val) return 0 ;

        Net *clk = left_val->GetBit() ;
        delete left_val ;
        return new VhdlEdge(clk) ;
        }
    case VHDL_falling_edge :
        {
        // Use first argument as the 'clock' in a falling edge
        // We should do some more checking here...
        VhdlExpression *arg = (arguments && arguments->Size()==1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!arg) return 0 ; // something bad is wrong.

        VhdlValue *left_val = arg->Evaluate(0,df,0) ;
        if (!left_val) return 0 ;

        Net *clk = left_val->GetBit() ;
        clk = Inv(clk) ;
        delete left_val ;
        return new VhdlEdge(clk) ;
        }
    case VHDL_to_string :
    case VHDL_to_bstring :
    case VHDL_to_binary_string :
    case VHDL_to_ostring :
    case VHDL_to_octal_string :
    case VHDL_to_hstring :
    case VHDL_to_hex_string :
        {
        // Vhdl 2008 LRM section 5.7, 5.3.2.4 etc
        VhdlIdDef *char_type = StdType("character") ;
        if (!char_type) return 0 ;

        VhdlExpression *arg = (arguments && arguments->Size()>=1) ? (VhdlExpression*)arguments->At(0) : 0 ;
        if (!arg) return 0 ; // something bad is wrong.

        VhdlExpression *arg2 = (arguments && arguments->Size()>1) ? (VhdlExpression*)arguments->At(1) : 0 ;

        VhdlValue *left_val = arg->Evaluate(0,df,0) ;
        if (!left_val) return 0 ;

        VhdlValue *second_val = (arg2) ? arg2->Evaluate(0,df,0) : 0 ;

        // image of the value, ignoring the format/unit for time being
        char *val_image = left_val->ToString(_oper_type, second_val) ;
        delete left_val ;
        delete second_val ;
        if (!val_image) return 0 ;

        Array *values = new Array((unsigned)Strings::len(val_image)) ;

        VhdlIdDef *elem ;
        char buffer[4] ;
        char *tmp ;
        for (tmp = val_image ; *(tmp) != '\0' ; tmp++) {
            sprintf(buffer,"'%c'",*tmp) ;
            elem = char_type->GetEnum(buffer) ;
            VERIFIC_ASSERT(elem) ; // Or type-inference was not working
            values->InsertLast(new VhdlEnum(elem)) ;
        }

        Strings::free(val_image) ;
        return new VhdlCompositeValue(values) ;
        }
    default :
        break ;
    }

    VERIFIC_ASSERT(arguments->Size()==NumOfArgs()) ; // Type-inference should guarantee this for built-in operators

    // We cast discrete range arguments into expression here :
    // This works since type-inference catches that discrete range is aalways illegal in an expression.
    VhdlExpression *left, *right  ;
    if (NumOfArgs()==1) {
        left = (VhdlExpression*)arguments->At(0) ;
        return ElaborateOperator(left, target_constraint, df, caller_node) ;
    } else if (NumOfArgs()==2) {
        left = (VhdlExpression*)arguments->At(0) ;
        right = (VhdlExpression*)arguments->At(1) ;
        return ElaborateOperator(left, right, target_constraint, df, caller_node) ;
    }
    return 0 ;
}

/**************************************************************/
// Elaborate a function with a pragma
/**************************************************************/

void
VhdlSubprogramId::ElaboratePragmaProcedure(const Array *arguments, VhdlDataFlow *df)
{
    // Function has pragma
    VERIFIC_ASSERT(_pragma_function) ;

    // Get the arguments :
    VhdlExpression *first_arg  = (arguments->Size()>0) ? (VhdlExpression*)arguments->At(0) : 0 ;
    VhdlExpression *second_arg = (arguments->Size()>1) ? (VhdlExpression*)arguments->At(1) : 0 ;
    VhdlExpression *third_arg = (arguments->Size()>2) ? (VhdlExpression*)arguments->At(2) : 0 ;
    // VhdlExpression *forth_arg = (arguments->Size()>3) ? (VhdlExpression*)arguments->At(3) : 0 ; // needed for 'write' procedures

    // Second : textio procedure pragma's. Might need separate routine for these.
    switch (_pragma_function) {
    case VHDL_IGNORE_SUBPROGRAM :
        // VIPER #2858 : Suppress message for static elaboration
        if (IsRtlElab()) {
            if (first_arg) {
                // Give line/file of caller
                first_arg->Warning("subprogram call %s ignored for synthesis", Name()) ;
            } else {
                // Give line/file of definition (only happens if there is no argument at all)
                Warning("subprogram call %s ignored for synthesis", Name()) ;
            }
        }
        return ; // done
    case VHDL_writeline :
        // FIX ME
        break ;
    case VHDL_readline :
        {
        // get out if we don't have two arguments :
        if (!first_arg || !second_arg) return ;
        // readline and writeline have a FILE as first argument.
        // second argument is an 'access' type to some array of character type.

        // Find the target/source character type...
        // Second arg is type LINE which is access type of STRING. Take element type of that.
        VhdlIdDef *line_type = ArgType(1) ;
        VhdlIdDef *char_type = (line_type) ? line_type->ElementType() : 0 ;

        // Pick up the file pointer from the first argument :
        VhdlIdDef *file_id = first_arg->GetId() ;
        FILE *file = (file_id) ? file_id->GetFile() : 0 ;
        if (!file) {
            if (file_id && file_id->Name() && df && df->IsInInitial()) first_arg->Error ("cannot open file '%s'", file_id->Name()) ; // file_id really holds the variable name, not the file name. But it is the best info that we have here
            // VIPER #5225/5226 : Create a null access value when file pointer is not present :
            VhdlIdDef *line_id = second_arg->GetId() ;
            if (line_id) {
                VhdlValue *access_val = new VhdlAccessValue(0, 0) ;

                // Assign that to the second argument :
                second_arg->Assign(access_val,df,0/*name_select*/) ; // FIX ME : assign failure due to constraints.
            }
            return ; // file not there, or not open
        }
        // read a line of data from the file :
        // VIPER #5077 : Ignore short/block comment and blank lines :
        int c ;
        VhdlIdDef *enum_id ;
        Array *chars = new Array(4096) ;
        unsigned end_line = 0 ;
        while ((c = getc(file)) != EOF) {
            switch (c) {
#if 0
            // Disable special processing of comments when reading text file. This is not required by LRM.
            case '/' :
            {
                c1 = getc(file) ;
                if ((c1 != '/') && (c1 != '*')) {
                    // Turn into a character literal :
                    enum_id = (char_type) ? char_type->GetEnumAt((unsigned)(c)): 0 ;
                    // check if this enum is in the target character type :
                    if (!enum_id && char_type) {
                        Error("character '%c' is not in element type %s",c,char_type->Name()) ;
                    }
                    chars->InsertLast(new VhdlEnum(enum_id)) ;
                    // Look for that character literal in 'char_type' :
                    enum_id = (char_type) ? char_type->GetEnumAt((unsigned)(c1)) : 0 ;
                    // check if this enum is in the target character type :
                    if (!enum_id && char_type) {
                        Error("character '%c' is not in element type %s",c,char_type->Name()) ;
                    }
                    // insert into chars array as a enum value
                    chars->InsertLast(new VhdlEnum(enum_id)) ;
                    break ;
                }
                // A comment is coming up - decide type of comment:
                unsigned short_comment = (c1 == '/') ? 1 : 0 ;
                while ((c = getc(file)) != EOF) {
                    if (short_comment) {
                        if (c == '\n') {
                            break ; // Short comment terminated
                        }
                    } else {
                        if ((c == '*') && (getc(file) == '/')) {
                            // Long comment terminated
                            break ;
                        }
                    }
                }
                break ;
            }
#endif
            case '\r' : // VIPER 3683 : include carriage return (\r) in line-termination characters.
                break ;
            case '\n' :
                if (chars->Size()) end_line = 1 ;
                break ;
#if 0
            // Disable special processing of comments when reading text file. This is not required by LRM.
            case ' ' :
                if (!chars->Size()) break ;

#endif
            default :
                // Turn into a character literal :
                // Look for that character literal in 'char_type' :
                enum_id = (char_type) ? char_type->GetEnumAt((unsigned)(c)) : 0 ;
                // check if this enum is in the target character type :
                if (!enum_id && char_type) {
                    Error("character '%c' is not in element type %s",c,char_type->Name()) ;
                }
                // insert into chars array as a enum value
                chars->InsertLast(new VhdlEnum(enum_id)) ;
            }
            if (end_line) break ;
        }

        /*
        // read from the file, store into the second argument.
        // create a buffer of reasonable size (maximum line) :
        char buffer[4096] ;
        // read a line of data from the file :
        if (fgets(buffer,4096,file)) {
            // successfully read in a line. \n is still at the end.
            const char *tmp = buffer ;
            // Create a composite value with target character type from this.
            VhdlIdDef *enum_id ;
            Array *chars = new Array(Strings::len(tmp)) ;
            while (tmp && *tmp!='\0') {
                if (*tmp=='\n' || *tmp=='\r') break ; // VIPER 3683 : include carriage return (\r) in line-termination characters.
                // Turn into a character literal :
                // Look for that character literal in 'char_type' :
                enum_id = (char_type) ? char_type->GetEnumAt((unsigned)(*tmp)) : 0 ;
                // check if this enum is in the target character type :
                if (!enum_id && char_type) {
                    Error("character '%c' is not in element type %s",*tmp,char_type->Name()) ;
                }
                // insert into chars array as a enum value
                chars->InsertLast(new VhdlEnum(enum_id)) ;
                tmp++ ;
            }
            */
        if (chars->Size()) {
            // Turn into a string value.
            VhdlValue *string_val = new VhdlCompositeValue(chars) ;

            VhdlIdDef *line_id = second_arg->GetId() ;
            VhdlConstraint *line_constraint = line_id ? line_id->Constraint() : 0 ;
            // line_constraint: must be unconstrained always after Viper #7288
            VhdlConstraint *inner_constraint = line_constraint ? line_constraint->GetDesignatedConstraint() : 0 ;
            if (inner_constraint) {
                inner_constraint = inner_constraint->Copy() ;
                inner_constraint->SetUnconstrained() ;
                inner_constraint->ConstraintBy(string_val) ;
                string_val->ConstraintBy(inner_constraint) ; // to set the range
            }

            // Create an access value with this :
            VhdlValue *access_val = new VhdlAccessValue(string_val, inner_constraint ? new VhdlAccessConstraint(inner_constraint) : 0) ;

            // Assign that to the second argument :
            second_arg->Assign(access_val,df,0/*name_select*/) ; // FIX ME : assign failure due to constraints.
            /*
            // FIX ME : TEMPORARY : Do a direct assign :
            if (df) {
                df->SetAssignValue(line_id, access_val) ;
            } else {
                // something went wrong.
                delete access_val ; // delete the shell.
                delete string_val ; // delete the value we point to
            }
            */
        } else {
            // error occurred.
            // The null-pointer check is redundant.. Only to fool gimpel
            if(file_id) first_arg->Warning("readline called past the end of file %s", file_id->Name()) ;
            unsigned i ;
            VhdlValue *val ;
            FOREACH_ARRAY_ITEM(chars, i, val) delete val ;
            delete chars ;
        }
        return ; // done with this procedure pragma
        }
    case VHDL_write :
    case VHDL_hwrite :
    case VHDL_owrite :
        // FIX ME
        break ;
    case VHDL_read :
    case VHDL_sread :
    case VHDL_hread :
    case VHDL_oread :
        {
        // textio 'read' and 'write' : LRM 14.3. Work with access values.
        // First argument is the access value.
        // Second argument is the read/write value.
        // 'read' : optional third argument is boolean 'success'
        // 'write' : optional third and forth argument is 'side (left/right) and 'width' (integer) of output fields

        // 'read' : access value is 'pruned' (remove chars) left to right.
        // if second arg is a scalar, prune one character at a time.
        // if second arg is an array, prune multiple characters at a time. Stop when arg2'length is reached (for constrained arg2).
        // third argument set to 'false' if something goes wrong..
        if (!first_arg || !second_arg) return ;

        // Pick up target type (argument 2) :
        VhdlIdDef *arg2_type = ArgType(1) ;
        if (!arg2_type) return ; // need the target type.

        // Get the access value id :
        VhdlIdDef *line_id = first_arg->GetId() ;
        // Get a direct pointer to the current access value of 'line_id' (evaluation of access type inout argument does not yet work..)
        VhdlValue *access_val = (df) ? df->IdValue(line_id) : (line_id) ? line_id->Value() : 0 ;

        if (!access_val) return ; // get out
        // Get the accessed value pointer
        VhdlValue *accessed_value = access_val->GetAccessedValue() ;
        if (!accessed_value) return ; // Was a NULL pointer. Cannot read anything.

        // Lets operate on the access value directly (don't go to full-string image), because we have all elements available already.
        Array *stream_elems = accessed_value->GetValues() ;
        if (!stream_elems) return ; // NULL value..

        // Create target value :
        VhdlValue *result = 0 ;
        VhdlValue *stream_elem_val ;
        VhdlValue *result_elem_val ;
        VhdlIdDef *stream_elem_id ;
        VhdlIdDef *result_elem_id ;
        // Viper #6060 : return failure status when stream_elems is empty
        unsigned success = (stream_elems->Size()) ? 1 : 0 ; // flag to see if 'read' worked ok.
        if (success && arg2_type->IsArrayType()) {
            // Special case : array target. Read characters and translate to target elements one-by-one. Cast to target elem type.
            VhdlIdDef *target_elem_type = arg2_type->ElementType() ;
            if (!target_elem_type) return ;

            // Create a target value array of enough size :
            Array *result_elems = new Array(accessed_value->NumElements()) ;

            // Calculate the target constraint size :
            unsigned size = 0 ; // default : read all characters from the stream
            VhdlConstraint *target_constraint = second_arg->EvaluateConstraint(df, 0) ;
            if (target_constraint && !target_constraint->IsUnconstrained()) {
                size = (unsigned) target_constraint->Length() ;
            }
            delete target_constraint ;

            // Target constraint is important : determines number of elements read from stream.
            if (_pragma_function==VHDL_hread || _pragma_function==VHDL_oread) {
                unsigned elem_size = 4 ; // for hread
                if (_pragma_function==VHDL_oread) elem_size = 3 ;

                // LRM 14.3 : read fails if stream has not enough characters
                if (size && stream_elems->Size()*elem_size<size) success = 0 ;

                unsigned leading_space = 1 ;

                // calculate the 'base'
                unsigned base = 1<<elem_size ;
                // 'hread' reads one character and puts into 4 bit-elements of the target type.

                // VIPER #3892 : Made changes for proper hread/oread elaboration.. Hread/Oread
                // should ignore all the leading white spaces and then read in all the characters
                // from the 'line' (first argument) that are necessary to 'fill-in' the target
                // (second argument).. Hence the while loop...
                while (stream_elems->Size()) {
                    if (!success) break ;
                    stream_elem_val = (VhdlValue*)stream_elems->GetFirst() ;
                    stream_elem_id = (stream_elem_val) ? stream_elem_val->GetEnumId() : 0 ;
                    const char *tmp = (stream_elem_id) ? stream_elem_id->Name() : 0 ;

                    if (!tmp) {
                        delete result_elems ;
                        return ; // not a valid character literal
                    }
                    if (*tmp=='\'') tmp++ ; // skip leading ' in character literal

                    // Get rid of the leading white spaces
                    if (leading_space && (*tmp == ' ')) {
                        // access_val->RemoveValueAt(0) ; // Viper #7288: also adjusts the constraints
                        stream_elems->Remove(0) ;
                        delete stream_elem_val ;
                        continue ;
                    }

                    leading_space = 0 ;

                    // translate hex/oct character to integer :
                    unsigned elem = (*tmp>='0' && *tmp<='9') ? (*tmp-'0') : (*tmp>='A' && *tmp<='Z') ? (10+(*tmp-'A')) : (*tmp>='a' && *tmp<='z') ? (10+(*tmp-'a')) : base+1 ;
                    if (elem >= base) {
                        Error("digit '%c' is larger than base in %s", *tmp, (_pragma_function==VHDL_hread) ? "hex number" : "octal number") ;
                        elem = 0 ;
                        success = 0 ;
                    }
                    // translate the 'elem_size' 0's and 1's in the number to '0' and '1' characters of the target element type :
                    for (int i=(int)(elem_size-1);i>=0;i--) {
                        const char *elem_name = ((elem >> i) & 1) ? "'1'" : "'0'" ;
                        // find the matching element ('0' or '1') in the target type :
                        result_elem_id = target_elem_type->GetEnum(elem_name) ;
                        // Use this to create a element value :
                        result_elem_val = new VhdlEnum(result_elem_id) ;
                        result_elems->InsertLast(result_elem_val) ;
                    }
                    // remove 'stream_elem_val' from the (access value) stream :
                    // access_val->RemoveValueAt(0) ; // Viper #7288: also adjusts the constraints
                    stream_elems->Remove(0) ;
                    delete stream_elem_val ;

                    // Check if we need to break (due to target constraint size) :
                    if (size && size==result_elems->Size()) break ; // valid break. success still good.
                }

                // Viper #5680: Previous check for size was little early
                // (did not discount leading spaces), check again for
                // sufficient length of the result
                if (size && result_elems->Size() < size) success = 0 ;
            } else {
                // 'read' reads a string of character literals, translate to target elements one-by-one.
                // In essence, do a 'cast' of type character to target type.
                // move elements from the stream into the target. Type-convert by element name.

                // LRM 14.3 : read fails if stream has not enough characters
                if (size && stream_elems->Size()<size) success = 0 ;

                while (stream_elems->Size()) {
                    if (!success) break ;
                    stream_elem_val = (VhdlValue*)stream_elems->GetFirst() ;
                    stream_elem_id = (stream_elem_val) ? stream_elem_val->GetEnumId() : 0 ;
                    if (!stream_elem_id) { success = 0 ; break ; } // something bad : there is no character in the stream element
                    // find the matching element in the target type :
                    result_elem_id = target_elem_type->GetEnum(stream_elem_id->Name()) ;
                    if (!result_elem_id) {
                        Error("character %s is not in type %s",stream_elem_id->Name(),target_elem_type->Name()) ;
                        success = 0 ;
                        break ;
                    }
                    // Use this to create a element value :
                    result_elem_val = new VhdlEnum(result_elem_id) ;
                    result_elems->InsertLast(result_elem_val) ;

                    // remove 'stream_elem_val' from the (access value) stream :
                    // access_val->RemoveValueAt(0) ; // Viper #7288: also adjusts the constraints
                    stream_elems->Remove(0) ;
                    delete stream_elem_val ;

                    // Check if we need to break (due to target constraint size) :
                    if (size && size==result_elems->Size()) break ; // valid break. success still good.
                }
            }
            // create resulting target value :
            result = new VhdlCompositeValue(result_elems) ;
        } else if (success && arg2_type->IsEnumerationType()) {
            // Scalar enum type. Get one element off the stream and push into target.
            // Currently only support character literals in the stream.
            // FIX ME : No support yet for full-id enumerations values like boolean's "false/true".
            // FIX ME : we should do leading space pruning for non-string target types..
            stream_elem_val = (VhdlValue*)stream_elems->GetFirst() ;
            stream_elem_id = (stream_elem_val) ? stream_elem_val->GetEnumId() : 0 ;
            if (!stream_elem_id) return ; // something bad : there is no character in the stream element
            // find the matching element in the target type :
            result_elem_id = arg2_type->GetEnum(stream_elem_id->Name()) ;
            if (result_elem_id) {
                // Use this to create the result value :
                result = new VhdlEnum(result_elem_id) ;
                // remove 'stream_elem_val' from the (access value) stream :
                // access_val->RemoveValueAt(0) ; // Viper #7288: also adjusts the constraints
                stream_elems->Remove(0) ;
                delete stream_elem_val ;
            } else {
                Error("character %s is not in type %s",stream_elem_id->Name(),arg2_type->Name()) ;
                success = 0 ;
            }
        } else if (success) {
            // LRM 14.3 : other scalar target type. Certainly a multi-character input value.
            // Put the access value back into a string, and point to the first character :
            char *image = access_val->Image() ;
            const char *runner = image ;
            // skip leading " :
            // Viper #6117: Additionally skip the spaces and update the remaining stream
            while (runner && (*runner=='"' || *runner==' ')) {
                if (*runner==' ') stream_elems->Remove(0) ; // access_val->RemoveValueAt(0) ;
                runner++ ;
            }

            const char *runner1 = runner ;
            while (runner1 && (*runner1!='"') && (*runner1!=' ')) {
                if (*runner1!='"') stream_elems->Remove(0) ; // access_val->RemoveValueAt(0) ;
                runner1++ ;
            }
            // Viper #6117 end: Additionally skip the spaces and update the remaining stream

            // Now, the result and number of characters scanned depends on the target type (of the target value).
            // LRM 14.3, ln.1000 - 1035 :
            // We cannot directly recognize the various 'built-in' types here in the elaborator,
            // but the pattern is clear : for scalar types, look in the string for their literals :
            if (arg2_type->IsIntegerType()) {
                // read an 'integer' literal
                result = new VhdlInt((verific_int64)Strings::atoi(runner)) ;
                // FIX ME : take out number of characters from the stream
            } else if (arg2_type->IsRealType()) {
                // read a 'real' number from the string
                result = new VhdlDouble((double)Strings::atoi(runner)) ;
                // FIX ME : take out number of characters from the stream
            } else if (arg2_type->IsPhysicalType()) {
                // read integer or real plus a 'unit' identifier.
                // FIX ME
                success = 0 ;
            } else {
                // no idea what this is
                success = 0 ;
            }
            // free the string image
            Strings::free(image) ;
        }

        // type-convert characters of the stream to the target value
        // assign this result value to the second argument
        verific_int64 result_len = 0 ;
        if (success) { // only if 'read' succeeded
            result_len = result ? result->NumElements() : 0 ;
            second_arg->Assign(result,df,0) ;
        } else {
            delete result ;
        }

        if (third_arg && (VHDL_sread == _pragma_function)) {
            VhdlValue *str_len_val = new VhdlInt(result_len) ;
            third_arg->Assign(str_len_val, df, 0) ;
        } else {
            // Optional third argument 'success' should be set..
            if (third_arg) {
                // translate 'success' to boolean 'false' or 'true'
                VhdlIdDef *bool_type = ArgType(2) ;
                VhdlIdDef *result_id = (bool_type) ? bool_type->GetEnumAt((success) ? 1 : 0) : 0 ;
                if (result_id) {
                    VhdlValue *success_val = new VhdlEnum(result_id) ;
                    // assign this to the third argument
                    third_arg->Assign(success_val,df,0) ;
                }
            }
        }

        // This is ugly... But here we have explicitly modified the value array. Constraint needs to update.
        VhdlConstraint *value_constraint = accessed_value->GetRange() ;
        if (value_constraint) {
            value_constraint->SetUnconstrained() ;
            value_constraint->ConstraintBy(accessed_value) ;
        }

        // Must reset the constraint on the access_value (for the first argument of
        // type LINE). The new constraint could be null constraint if needed
        VhdlConstraint *access_constraint = access_val->Constraint() ;
        VhdlConstraint *inner_constraint = access_constraint ? access_constraint->GetDesignatedConstraint() : 0 ;
        if (inner_constraint) {
            inner_constraint->SetUnconstrained() ;
            inner_constraint->ConstraintBy(accessed_value) ;
        }

        // No need to update the constraint on the identifier
        // It must be unconstrained always after Viper #7288

        return ; // done with this procedure pragma
        }
    default :
        // not a valid pragma for a procedure. Analysis will have errored out already, and ignored the pragma, so no need to error.
        // So error out just in case we missed something in code here :
        Error("cannot execute unknown pragma for function %s",Name()) ;
        break ;
    }
}

VhdlValue *
VhdlSubprogramId::ElaboratePragmaFunction(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df)
{
    // Function has pragma
    VERIFIC_ASSERT(_pragma_function) ;

    VhdlTreeNode *caller_node = df ? df->GetCallerTreeNode() : 0 ;

    // First check if we should ignore this function altogether
    if (_pragma_function == VHDL_IGNORE_SUBPROGRAM) {
        // VIPER #2858 : Suppress message for static elaboration
        if (IsRtlElab()) {
            if (!caller_node) {
                caller_node = this ;
                if (left) caller_node = left ;
            }
            if (left) {
                // Give line/file of caller
                caller_node->Warning("subprogram call %s ignored for synthesis", Name()) ;
            } else {
                // Give line/file of definition (only happens if there is no argument at all)
                caller_node->Warning("subprogram call %s ignored for synthesis", Name()) ;
            }
        }
        return 0 ;
    }

    // Evaluate left and right side
    VhdlValue *left_val = (left) ? left->Evaluate(0,df,0) : 0 ;
    if (left && !left_val) {
        // Viper 6967 : Add this for static elaboration.
        if (IsStaticElab()) {
            VhdlValue *r_val = (right) ? right->Evaluate(0,df,0) : 0 ;
            delete r_val ;
        }
        return 0 ; // Something failed
    }
    VhdlValue *right_val = (right) ? right->Evaluate(0,df,0) : 0 ;
    if (right && !right_val) { delete left_val ; return 0 ; }

    VhdlValue *result = 0 ;

    if (!caller_node) caller_node = (_pragma_function == VHDL_STAR) ? right : left ;

    // Set 'signed' info on (array) input values :
    // Could be set by '_pragma_signed'.
    // 'signed' could also be from ambit's "signed_type" attribite on the type
    // (of the first argument), (only needed if that is an array).

    // Do also argument size calculation for array types
    int size = 0 ;
    int lsize = 0 ;
    int rsize = 0 ;
    VhdlIdDef *arg ;
    const VhdlIdDef *type ;
    if (left_val && left_val->IsComposite()) {
        arg = Arg(0) ;
        if (_pragma_signed) left_val->SetSigned() ;
        type = arg->Type() ;
        if (type && type->GetAttribute("signed_type")) left_val->SetSigned() ;
        lsize = (int)left_val->NumElements() ;
    }
    if (right_val && right_val->IsComposite()) {
        arg = Arg(1) ;
        if (_pragma_signed) right_val->SetSigned() ;
        type = arg->Type() ;
        if (type && type->GetAttribute("signed_type")) right_val->SetSigned() ;
        rsize = (int)right_val->NumElements() ;
    }

    // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
    double lval_real = left_val ? left_val->Real() : 0.0 ;
    double rval_real = right_val ? right_val->Real() : 0.0 ;
    if (IsMathRealPragma(_pragma_function)) {
        unsigned non_const_arg = !left_val || !left_val->IsConstant() ;
        if (right_val && !right_val->IsConstant()) non_const_arg = 1 ;

        if (non_const_arg) {
            delete left_val ; delete right_val ;
            return 0 ;
        }
    }
    // #args for each function is already tested during analysis.
    // So we do not need to warn or error if there is something wrong with the arguments.

    // Now create the logic. Mostly, we first convert array parameters
    // to nonconst, so we can call normal arithmetic routines on them.

    // So, if an array result is needed, we need to convert the resulting nonconst
    // back to an array. For that reason, calculate the return size for each of
    // these functions, based on the semantics that are implied with the pragma's,
    // and the conventions used for array type return in the (de-facto) standard packages.

    switch (_pragma_function) {
    // arithmetic operators. Unary and binary
    case VHDL_UMINUS :
        if (!left_val) return 0 ;
        // size is fixed : N bits in, N bits out
        size = lsize ;
        result = left_val->ToNonconst()->UnaryMin(/*from*/caller_node, VhdlNode::Pwr()) ;
        delete right_val ; // Delete right_val, since it'll never be used.
        break ;
    case VHDL_MINUS :
        if (!left_val || !right_val) return 0 ;
        // size is fixed : N - M -> MAX(N,M) bits out
        size = MAX(lsize, rsize) ;
        result = left_val->ToNonconst()->Minus(right_val->ToNonconst(),/*from*/caller_node) ;
        break ;
    case VHDL_PLUS :
        if (!left_val || !right_val) return 0 ;
        // size is fixed : N + M -> MAX(N,M) bits out
        size = MAX(lsize, rsize) ;
        result = left_val->ToNonconst()->Plus(right_val->ToNonconst(),Gnd(),/*from*/caller_node) ;
        break ;
    case VHDL_STAR :
        if (!left_val || !right_val) return 0 ;
        // size depends on if arguments are composites.  Logic here is taken from numeric_std.vhd.
        // If both size of non-zero, then take sum, else take two times the non-zero size. (VIPER #2262)
        size = (!lsize) ? 2*rsize : (!rsize) ? 2*lsize : lsize + rsize ;
        result = left_val->ToNonconst()->Multiply(right_val->ToNonconst(),/*from*/caller_node) ;
        break ;
    case VHDL_SLASH :
        if (!left_val || !right_val) return 0 ;
        // size always follows lsize if it's non-zero, else rsize is followed (VIPER #2262)
        size = (lsize) ? lsize : rsize ;
        result = left_val->ToNonconst()->Divide(right_val->ToNonconst(),/*from*/caller_node) ;
        break ;
    case VHDL_abs :
        if (!left_val) return 0 ;
        // size is fixed : N -> N bits out
        size = lsize ;
        result = left_val->ToNonconst()->Abs(/*from*/caller_node) ;
        break ;
    case VHDL_condition :
        // Pragma functions as per 1076-2008 LRM section 9.2.9
        if (!left_val || lsize != 1) {
            delete left_val ;
            return 0 ;
        }
        size = 1 ;
        result = left_val->ToNonconstBit()->UnaryOper(VHDL_condition, caller_node) ;
        break ;
    case VHDL_mod :
        if (!left_val || !right_val) return 0 ;
        // size always follows rsize if it's non-zero, else lsize is followed (VIPER #2262)
        size = (rsize) ? rsize : lsize ;
        result = left_val->ToNonconst()->Modulo(right_val->ToNonconst(),/*from*/caller_node) ;
        break ;
    case VHDL_rem :
        if (!left_val || !right_val) return 0 ;
        // size always follows rsize if it's non-zero, else lsize is followed (VIPER #2262)
        size = (rsize) ? rsize : lsize ;
        result = left_val->ToNonconst()->Remainder(right_val->ToNonconst(),/*from*/caller_node) ;
        break ;
    case VHDL_EXPONENT :
        if (!left_val || !right_val) return 0 ;
        // We support ** now via nonconst function.
        // There are no standard package examples for this pragma, so assume standard array size result :
        size = MAX(lsize,rsize) ;
        result = left_val->ToNonconst()->Power(right_val->ToNonconst(),/*from*/caller_node) ;
        break ;

    // Shifters/Rotators :
    // If they return an array, then they must have a array 'left' value. Size should always match left size :
    case VHDL_sll :
        if (!left_val || !right_val) return 0 ;
        size = lsize ;
        result = left_val->ToNonconst()->Shift(right_val->ToNonconst(),0,0,0,/*from*/caller_node) ;
        break ;
    case VHDL_srl :
        if (!left_val || !right_val) return 0 ;
        size = lsize ;
        result = left_val->ToNonconst()->Shift(right_val->ToNonconst(),1,0,0,/*from*/caller_node) ;
        break ;
    case VHDL_sla :
        if (!left_val || !right_val) return 0 ;
        size = lsize ;
        result = left_val->ToNonconst()->Shift(right_val->ToNonconst(),0,1,0,/*from*/caller_node) ;
        break ;
    case VHDL_sra :
        if (!left_val || !right_val) return 0 ;
        size = lsize ;
        result = left_val->ToNonconst()->Shift(right_val->ToNonconst(),1,1,0,/*from*/caller_node) ;
        break ;
    case VHDL_rol :
        if (!left_val || !right_val) return 0 ;
        size = lsize ;
        result = left_val->ToNonconst()->Shift(right_val->ToNonconst(),0,0,1,/*from*/caller_node) ;
        break ;
    case VHDL_ror :
        if (!left_val || !right_val) return 0 ;
        size = lsize ;
        result = left_val->ToNonconst()->Shift(right_val->ToNonconst(),1,0,1,/*from*/caller_node) ;
        break ;

    // Reduction operations
    case VHDL_REDAND :
    case VHDL_REDNAND :
    case VHDL_REDOR :
    case VHDL_REDNOR :
    case VHDL_REDXOR :
    case VHDL_REDXNOR :
        if (!left_val) return 0 ;
        return left_val->ToNonconst()->Reduce(_pragma_function,/*from*/caller_node) ;

    // Comparators :
    case VHDL_EQUAL :
        if (!left_val || !right_val) return 0 ;
        if (left_val->IsBit() && right_val->IsBit()) {
            // single-bit operation ; special for event handling
            return left_val->ToNonconstBit()->Equal(right_val->ToNonconstBit(),0,/*from*/caller_node) ;
        } else {
            return left_val->ToNonconst()->Equal(right_val->ToNonconst(),0,/*from*/caller_node) ;
        }
    case VHDL_NEQUAL:
        if (!left_val || !right_val) return 0 ;
        if (left_val->IsBit() && right_val->IsBit()) {
            // single-bit operation ; special for event handling
            return left_val->ToNonconstBit()->Equal(right_val->ToNonconstBit(),1,/*from*/caller_node) ;
        } else {
            return left_val->ToNonconst()->Equal(right_val->ToNonconst(),1,/*from*/caller_node) ;
        }
    case VHDL_STHAN :
        if (!left_val || !right_val) return 0 ;
        return left_val->ToNonconst()->LessThan(right_val->ToNonconst(),0,/*from*/caller_node) ;
    case VHDL_GTHAN :
        if (!left_val || !right_val) return 0 ;
        return right_val->ToNonconst()->LessThan(left_val->ToNonconst(),0,/*from*/caller_node) ;
    case VHDL_SEQUAL:
        if (!left_val || !right_val) return 0 ;
        return left_val->ToNonconst()->LessThan(right_val->ToNonconst(),1,/*from*/caller_node) ;
    case VHDL_GEQUAL:
        if (!left_val || !right_val) return 0 ;
        return right_val->ToNonconst()->LessThan(left_val->ToNonconst(),1,/*from*/caller_node) ;

    // not/buf
    case VHDL_not :
        if (!left_val) return 0 ;
        size = lsize ;
        if (left_val->IsBit()) {
            // Single-bit operation ; special for event handling
            result = left_val->ToNonconstBit()->Invert(/*from*/caller_node) ;
        } else {
            result = left_val->ToNonconst()->Invert(/*from*/caller_node) ;
        }
        break ;

    case VHDL_buf :
        if (!left_val) return 0 ;
        // From Synopsys Pragma 'syn_buf'. Put in a real buffer there
        size = lsize ;
        if (left_val->IsBit()) {
            // Single-bit operation
            result = left_val->ToNonconstBit()->PreserveAssignmentBuffer(1, /*from*/caller_node) ;
        } else {
            result = left_val->ToNonconst()->PreserveAssignmentBuffer(1, /*from*/caller_node) ;
        }
        break ;

    // bit-wize wide boolean operators :
    case VHDL_and :
    case VHDL_nand :
    case VHDL_or :
    case VHDL_nor :
    case VHDL_xor :
    case VHDL_xnor :
        if (!left_val || !right_val) return 0 ;
        size = MAX(lsize,rsize) ;
        if (left_val->IsBit() && right_val->IsBit()) {
            // Single-bit value in and out ; special for event handling
            result = left_val->ToNonconstBit()->Binary(right_val->ToNonconstBit(),_pragma_function,/*from*/caller_node) ;
        } else {
            VhdlNonconst *lv_nc = left_val->ToNonconst() ;
            VhdlNonconst *rv_nc = right_val->ToNonconst() ;
            // Vhdl2008: Section 9.2.2: new semantics for "and" (bit, bit_vector) etc..
            if (lv_nc->Size() == 1 && vhdl_file::IsVhdl2008()) {
                lv_nc->SetSigned() ;
                lv_nc->Adjust(rv_nc->Size()) ;
            }
            if (rv_nc->Size() == 1 && vhdl_file::IsVhdl2008()) {
                rv_nc->SetSigned() ;
                rv_nc->Adjust(lv_nc->Size()) ;
            }
            // multibit (composite) operation
            result = lv_nc->Binary(rv_nc,_pragma_function,/*from*/caller_node) ;
        }
        break ;

    // The standard 'feed-through' (from anything to anything) :
    case VHDL_FEEDTHROUGH :
        if (!left_val) return 0 ;

        size = (lsize) ? lsize : (int)left_val->NumBits() ; // default array return size, if there is not size argument.
        type = Type() ;
        if (right_val && type && type->IsArrayType()) {
            // in most feed-through functions, the second argument means a
            // size adjustment if the return type is an array type.
            // But sometimes, (like in to_integer() in mgc_arit) it means some
            // sort of sign-control value, which I don't quite get.
            // So, check if we BOTH return an array type AND the 'right' argument is integer.
            // in that case, we assume that it is a return-size argument.

            type = ArgType(1) ;
            if (type && type->IsIntegerType()) {
                // Array size adjustment requested
                // right_val should be a constant, indicating the size
                if (!right_val->IsConstant()) {
                    right->Error("second argument of %s should be a constant", Name()) ;
                } else {
                    size = (int)right_val->Integer() ;
                }
            }
        }
        delete right_val ;

        // Translate to nonconst regardless of incoming type
        // Translate to bit-value if result is a bit-encoded type.
        // VIPER 7346 : feed-through a bit only when input value is a bit value :
        if (left_val->IsBit()) {
            result = left_val->ToNonconstBit() ;
        } else {
            // Otherwise go to multi-bit nonconst :
            result = left_val->ToNonconst() ;
        }
        break ;
    // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
    case VHDL_math_ceil:
        delete left_val ;
        return new VhdlDouble(ceil(lval_real)) ;
    case VHDL_math_floor:
        delete left_val ;
        return new VhdlDouble(floor(lval_real)) ;
    case VHDL_math_sin:
        delete left_val ;
        return new VhdlDouble(sin(lval_real)) ;
    case VHDL_math_cos:
        delete left_val ;
        return new VhdlDouble(cos(lval_real)) ;
    case VHDL_math_asin:
        if (lval_real > 1.0 || lval_real < -1.0) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
            return left_val ;
        }
        delete left_val ;
        return new VhdlDouble(asin(lval_real)) ;
    case VHDL_math_acos:
        if (lval_real > 1.0 || lval_real < -1.0) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
            return left_val ;
        }
        delete left_val ;
        return new VhdlDouble(acos(lval_real)) ;
    case VHDL_math_atan:
        delete left_val ;
        return new VhdlDouble(atan(lval_real)) ;
    case VHDL_math_asinh:
        {
            // Return the asinh of the value: don't use asinh() function
            // since it is not available on all platforms
            delete left_val ;
            double temp = lval_real + sqrt(pow(lval_real, 2.0) + 1) ;
            return new VhdlDouble(log(temp)) ;
        }
    case VHDL_math_acosh:
        {
            if (lval_real < 1.0) {
                Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
                return left_val ;
            }
            // Return the acosh of the value: don't use acosh() function
            // since it is not available on all platforms
            double temp = lval_real + sqrt(pow(lval_real, 2.0) - 1) ;
            return (new VhdlDouble(log(temp))) ;
        }
    case VHDL_math_atanh:
        {
            if (lval_real >= 1.0 || lval_real <= -1.0) {
                Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
                return left_val ;
            }
            delete left_val ;
            // Return the atanh of the value: don't use atanh() function
            // since it is not available on all platforms
            double temp = sqrt((1+lval_real)/(1-lval_real));
            return (new VhdlDouble(log(temp)/2.0)) ;
        }
    case VHDL_math_sinh:
        delete left_val ;
        return new VhdlDouble(sinh(lval_real)) ;
    case VHDL_math_cosh:
        delete left_val ;
        return new VhdlDouble(cosh(lval_real)) ;
    case VHDL_math_tanh:
        delete left_val ;
        return new VhdlDouble(tanh(lval_real)) ;
    case VHDL_math_sqrt:
        delete left_val ;
        if (lval_real < 0.0) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
            return new VhdlDouble(0.0) ;
        }
        return new VhdlDouble(sqrt(lval_real)) ;
    case VHDL_math_cbrt:
        delete left_val ;
        return new VhdlDouble(exp(log(lval_real)/3.0)) ;
    case VHDL_math_exp:
         {
            delete left_val ;

            // check for boundary condition
            VhdlIdDef *real_type_id = StdType("real") ;
            VhdlConstraint *real_constraint = real_type_id ? real_type_id->Constraint() : 0 ;
            double real_high = real_constraint ? real_constraint->High()->Real() : 0.0 ;
            double real_high_log = real_high ? log(real_high) : 0.0 ;
            double abs_lval = lval_real > 0.0 ? lval_real : -lval_real ;
            if (real_high_log && abs_lval > real_high_log) {
                Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
                return new VhdlDouble(lval_real > 0.0 ? real_high : 0.0) ;
            }
            return new VhdlDouble(exp(lval_real)) ;
        }
    case VHDL_math_log:
        delete left_val ;
        if (lval_real <= 0.0) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;

            // should return real'low
            VhdlIdDef *real_type_id = StdType("real") ;
            VhdlConstraint *real_constraint = real_type_id ? real_type_id->Constraint() : 0 ;
            return real_constraint ? real_constraint->Low()->Copy() : new VhdlDouble(-1.0) ;
        }
        return new VhdlDouble(log(lval_real)) ;
    case VHDL_math_log10:
        delete left_val ;
        if (lval_real <= 0.0) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;

            // should return real'low
            VhdlIdDef *real_type_id = StdType("real") ;
            VhdlConstraint *real_constraint = real_type_id ? real_type_id->Constraint() : 0 ;
            return real_constraint ? real_constraint->Low()->Copy() : new VhdlDouble(-1.0) ;
        }
        return new VhdlDouble(log10(lval_real)) ;
    case VHDL_math_log2:
        delete left_val ;
        if (lval_real <= 0.0) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;

            // should return real'low
            VhdlIdDef *real_type_id = StdType("real") ;
            VhdlConstraint *real_constraint = real_type_id ? real_type_id->Constraint() : 0 ;
            return real_constraint ? real_constraint->Low()->Copy() : new VhdlDouble(-1.0) ;
        }
        return new VhdlDouble(log(lval_real)/log((double)2.0)) ;  // log2 not defined in windows math.h
    case VHDL_math_mod:
        delete left_val ;
        delete right_val ;
        if (!rval_real) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
            return new VhdlDouble(0.0) ;
        }
        return new VhdlDouble(fmod(lval_real, rval_real)) ;
    case VHDL_math_atan2:
        delete left_val ;
        delete right_val ;
        if (!lval_real && !rval_real) {
            Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
            return new VhdlDouble(0.0) ;
        }
        return new VhdlDouble(atan2(lval_real, rval_real)) ;
    case VHDL_math_pow:
        {
            VhdlIdDef *arg_type1 = ArgType(0) ;
            if (arg_type1 && arg_type1->IsIntegerType()) {
                verific_int64 lval_int = left_val ? left_val->Integer() : 0 ;
                lval_real = (double)lval_int ;
            }
            delete left_val ;
            delete right_val ;
            if ((lval_real < 0 && !rval_real) || (!lval_real && rval_real <= 0)) {
                Error("argument out of valid domain in function %s", OperatorName(_pragma_function)) ;
                return new VhdlDouble(0.0) ;
            }
            return new VhdlDouble(pow(lval_real, rval_real)) ;
        }
    default :
        if (left) left->Error("cannot execute unknown pragma for function %s",Name()) ;
        // Actually, don't know how to execute the original body either...
        delete left_val ;
        delete right_val ;
        return 0 ;
    }

    if (!result) return 0 ;

    // Adjust the return value to the expected return type :
    type = Type() ;
    if (type && type->IsArrayType()) {
        // Return an array (composite) value

        // Note that 'size' can be LEGALLY 0, or even negative here.
        // In that case, a NULL array should be returned.
        // Make sure that the conversion code below takes care of that.

        // Since 'size' can be negative, make sure to test sign of 'size'. Return NULL array if its negative.
        VhdlCompositeValue *r = result->ToComposite( (size>=0) ? (unsigned)size : 0) ;

        // Adjust unconstrained target to N-1 downto 0. If there.
        if (target_constraint && target_constraint->IsUnconstrained()) {
            // Create constraint
            VhdlConstraint *index_range = 0 ;
            switch (_pragma_function) { // Viper #3915
            case VHDL_and :
            case VHDL_nand :
            case VHDL_or :
            case VHDL_nor :
            case VHDL_xor :
            case VHDL_xnor :
            case VHDL_not :
            {
                index_range = new VhdlRangeConstraint(new VhdlInt((verific_int64)1), VHDL_to, new VhdlInt((verific_int64)(size))) ;
                // Verified that Modelsim Version 10.1b_1 follow LRM hence deleting 3915 specific change
                /*
                VhdlConstraint *arg_constraint = left ? left->EvaluateConstraint(df, 0) : 0 ;
                VhdlConstraint *index = arg_constraint ? arg_constraint->IndexConstraint() : 0 ;
                index_range = index ? index->Copy() : 0 ;
                if (arg_constraint) delete arg_constraint ;

                // IEEE package dictates (1 to left'length) but Msim returns left'range
                // Please check Viper #3915 for more details..
                */

                break ;
            }
            default:
                break ;
            }

            if (!index_range) index_range = new VhdlRangeConstraint(new VhdlInt((verific_int64)(size-1)), VHDL_downto, new VhdlInt((verific_int64)0)) ;

            VhdlConstraint *elem_constraint = target_constraint->ElementConstraint()->Copy() ;
            VhdlConstraint *array_constraint = new VhdlArrayConstraint(index_range, elem_constraint) ;
            // Adjust the target constraint
            target_constraint->ConstraintBy(array_constraint) ;
            delete array_constraint ; // Deletes the rest below it
        }
        // return the array form
        return r ;
    } else if (type && type->IsScalarType()) {
        // VIPER 7346 : Special care if result type is single-bit-encoded :
        if (type->Constraint() && type->Constraint()->IsBitEncoded()) {
            // single-bit scalar.
            if (!result->IsBit()) {
                if (result->NumBits()>1) {
                    // Error out at the subprogram declaration line/file, since problem is created by the one who set the pragma.
                    Error("pragma function %s cannot fit multi-bit result into single-bit return type %s",OperatorName(_pragma_function),type->Name()) ;
                    // let multibit value go through for now...
                } else {
                    // convert to single-bit scalar :
                    result = result->ToNonconstBit() ;
                }
            }
        } else {
            // multi-bit scalar
            if (result->IsBit()) {
                // convert to multi-bit scalar :
                result = result->ToNonconst() ;
            }
        }
    }

    // return the scalar form

    // Sanity check : return type should match with return value,
    // or we should adjust return value..
    if (!type || !result->CheckAgainst(type->Constraint(),this, df)) {
        delete result ;
        return 0 ;
    }

    return result ;
}

VhdlIdDef *VhdlPackageId::GetElaboratedPackageId() const
{
    if (_unit) return _unit->GetElaboratedPackageId() ;
    if (_decl) return _decl->GetElaboratedPackageId() ;
    return 0 ;
}
VhdlIdDef *VhdlPackageInstElement::GetElaboratedPackageId() const
{
    return _actual_id ? _actual_id->GetElaboratedPackageId(): 0 ;
}
void VhdlPackageId::SetElaboratedPackageId(VhdlIdDef *id)
{
    if (_unit) _unit->SetElaboratedPackageId(id) ;
    if (_decl) _decl->SetElaboratedPackageId(id) ;
}
void VhdlPackageInstElement::SetElaboratedPackageId(VhdlIdDef *id)
{
    if (_actual_id) _actual_id->SetElaboratedPackageId(id) ;
}
VhdlIdDef *VhdlPackageId::TakeElaboratedPackageId()
{
    if (_unit) return _unit->TakeElaboratedPackageId() ;
    if (_decl) return _decl->TakeElaboratedPackageId() ;
    return 0 ;
}
VhdlIdDef *VhdlPackageInstElement::TakeElaboratedPackageId()
{
    return (_actual_id) ? _actual_id->TakeElaboratedPackageId(): 0 ;
}

