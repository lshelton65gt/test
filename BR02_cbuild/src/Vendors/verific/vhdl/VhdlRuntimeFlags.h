/*
 *
 * [ File Version : 1.20 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_RUNTIME_FLAGS_H_
#define _VERIFIC_VHDL_RUNTIME_FLAGS_H_

#include "VerificSystem.h"
#include "VhdlCompileFlags.h"
#include "RuntimeFlags.h" // global utility to manage runtime flags.

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

/* -------------------------------------------------------------- */

// Central Runtime flags mechanism implemented with utility "RuntimeFlags", under VIPER 7208.
//
// Many aspects of Verific's  can be customized by compile-time and run-time switches.
//
// The runtime switches relevant to the VHDL parser (analyzer) and elaborator (static and RTL)
// are defined here in the class VhdlRuntimeFlags, which is derived from the Verific
// global run-time variable utility 'RuntimeFlags' (defined in the file util/RuntimeFlags.h).
//
// Note that some of these global variables may be shared with other components.
// For example, the "preserve_user_nets" switch is common to Verilog RTL elaboration,
// VHDL RTL elaboration and certain netlist (Database) optimization routines.
//
// Creation (and initialization) of each run-time variable is done in the routine 'Initialize',
// which is automatically called once, during program-initialization time.
// During run-time, individual variables can be set to a different value using
// the (static) RuntimeFlags::Set() API, and their value obtained with the RuntimeFlags::Get() API.
//
// Advanced : If you choose to implement Set/Get on this class VhdlRuntimeFlags, then you can
// provide an alternative implementation (than the default RuntimeFlags::Set/Get) for
// callers of VhdlRuntimeFlags::Set/Get.
//
// If all VHDL related variables should be re-set to original (initial) setting, simply call
// VhdlRuntimeFlags::Initialize() again.
//
// For compile time flags (from the VhdlCompileFlags.h header) that have been converted to
// runtime flags, Verific uses the compile-time flags initialize the run-time variable
// as shown in the Initialize() routine below.
//
// As a Verific source-code customer, you are free to change the initial values of the
// existing variables, and are free to add run-time variables yourself (on which you then
// can rely in the source code that you want to customize).
// Either way, if you make changes to source code, please share these with Verific for
// merge into your companies' code branch, so that we can make sure that we are running
// the software in the same way as you do. This (share you source code modifications with us)
// would make it much easier for you to integrate new monthly releases from Verific into your system.
//
// Some of our existing customers have shared their own run-time flag mechanism with us,
// before Verific implemented the 'RuntimeFlags' utility. For these customers, we left your
// existing run-time flags in place (and removed Verific 'RuntimeFlags' calls).
// We would be happy to assist you merging your custom run-time flag mechanism
// with Verific's 'RuntimeFlags' utility, so that you obtain a consistent and extendable
// unified run-time variable management system.
//
class VFC_DLL_PORT VhdlRuntimeFlags : public RuntimeFlags
{
private :
    // Class should never be instantiated
    friend class RuntimeFlags ;
    VhdlRuntimeFlags() ;  // Purposely leave unimplemented
    ~VhdlRuntimeFlags() ; // Purposely not virtual, and leave unimplemented

    // Initialization of all known global variables. This routine is called automatically, the first time any of the (RuntimeFlags::) API routines is called
    static void Initialize() {
        // Set run-time flags to compile time settings
        // Use AddVar() so that variable will get created if it was so far non-existing.

        // HDL analysis flags :
#ifdef VHDL_PRESERVE_COMMENTS
        (void) AddVar("vhdl_preserve_comments", 1) ; // Preserve comments in parse trees during analysis of VHDL designs.
#else
        (void) AddVar("vhdl_preserve_comments", 0) ;
#endif

        // VIPER #7855: Ignore nested translate/synthesis off/on pragmas.
        // We anyway do not support nested translate/synthesis off/on pragmas with same trigger.
        // We only supprt them with different trigger. This flag, when set will not allow such nesting.
        // Default is previous behaviour of supporting nested translate/synthesis off/on pragmas with different trigger.
        (void) AddVar("vhdl_ignore_pragma_nesting", 0) ;

        // Static elaboration flags
#ifdef VHDL_UNIQUIFY_ALL_INSTANCES
        (void) AddVar("vhdl_uniquify_all_instances", 1) ; // Uniquify all instances
#else
        (void) AddVar("vhdl_uniquify_all_instances", 0) ;
#endif

        // RTL elaboration flags
#ifdef VHDL_PRESERVE_USER_NETS
        (void) AddVar("vhdl_preserve_user_nets", 1) ;   // Preserve all user-nets, during RTL elaboration and netlist optimizations
#else
        (void) AddVar("vhdl_preserve_user_nets", 0) ;
#endif
#ifdef VHDL_PRESERVE_ASSIGNMENTS
        (void) AddVar("vhdl_preserve_assignments", 1) ; // Preserve assignments during RTL elaboration.
#else
        (void) AddVar("vhdl_preserve_assignments", 0) ;
#endif
#ifdef VHDL_PRESERVE_DRIVERS
        (void) AddVar("vhdl_preserve_drivers", 1) ;     // Preserve 'drivers' during RTL elaboration.
#else
        (void) AddVar("vhdl_preserve_drivers", 0) ;
#endif
#ifdef VHDL_PRESERVE_X
        (void) AddVar("vhdl_preserve_x", 1) ;           // Preserve 'x' during RTL elaboration (instead of optimizing as don't-care state).
#else
        (void) AddVar("vhdl_preserve_x", 0) ;
#endif
#ifdef VHDL_EXTRACT_MULTI_PORT_RAMS
        (void) AddVar("vhdl_extract_multiport_rams", 1) ; // Extract multi-port RAMs during RTL elaboration.
#else
        (void) AddVar("vhdl_extract_multiport_rams", 0) ;
#endif
#ifdef VHDL_EXTRACT_RAMS
        (void) AddVar("vhdl_extract_dualport_rams", 1) ; // Extract dual-port RAMs during RTL elaboration.
#else
        (void) AddVar("vhdl_extract_dualport_rams", 0) ;
#endif
#ifdef VHDL_PRESERVE_REGISTER_NAMES
        // Viper 8147: After removing VhdlNode::ReNameInstance to Instance::ReNameInstance this flag is obselete
        // use corresponding database flag db_preserve_register_names instead.
        (void) AddVar("vhdl_preserve_register_names", 1) ; // During RTL elaboration, use the name of the target net to be the register instance name.
#else
        (void) AddVar("vhdl_preserve_register_names", 0) ;
#endif
#ifdef VHDL_SYNOPSYS_REGISTER_NAMES
        // Viper 8147: After removing VhdlNode::ReNameInstance to Instance::ReNameInstance this flag is obselete
        // use corresponding database flag db_synopsys_register_names instead.
        (void) AddVar("vhdl_synopsys_register_names", 1) ; // During RTL elaboration, use the name of the target net with a _reg suffix to be the register instance name.
#else
        (void) AddVar("vhdl_synopsys_register_names", 0) ;
#endif

#ifdef VHDL_BUILD_PRIO_SELECTORS
        // Runtime flag for Viper #8324.
        // VHDL_BUILD_PRIO_SELECTORS => db_support_prio_selectors but not otherwise
        (void) AddVar("db_support_prio_selectors", 1) ;
#endif

#ifdef VHDL_IGNORE_ASSERTION_STATEMENTS
        (void) AddVar("vhdl_ignore_assertion_statements", 1) ; // Ignore assertion statements during RTL and static elaboration.
#else
        (void) AddVar("vhdl_ignore_assertion_statements", 0) ;
#endif
#ifdef VHDL_IGNORE_SYNTHESIS_DIRECTIVES
        (void) AddVar("vhdl_ignore_synthesis_directives", 1) ; // Ignore synthesis directives such as functions, enum_encoding and logic_type_encoding attributes during elaboration.
#else
        (void) AddVar("vhdl_ignore_synthesis_directives", 0) ; // Ignore synthesis directives such as functions, enum_encoding and logic_type_encoding attributes during elaboration.
#endif

        // RTL elaboration run-time flags
        (void) AddVar("vhdl_minimum_ram_size", 0) ;        // Minimum size (in bits) a RAM needs to be before RTL elaboration extract it. Value 0 means no lower limit.
// limit on the number of iterations that loop statements can go through.
// Here in VHDL, this only applies to 'while' loops.
// RD: 1/2009: We used to have loop limit of 1000, but there are examples in the math_real package that require higher limit than that (like log2(real'high) when real'high is 1 E203 or so. Shows up in VIPER 3601. limit of 10000 is safer and does not seem to slow regressions down much.
// #define LOOPLIMIT 10000 // higher than this will increase risk of running out of memory on inf loops
        (void) AddVar("vhdl_loop_limit", 10000) ;              // Maximum number of iterations through a non-static loop before RTL elaboration issues an error (maximum loop iterations exceeded).
        (void) AddVar("vhdl_nonconst_loop_limit", 1000) ;  // Maximum number of iterations through a loop with non-constant condition before RTL elaboration issues an error (maximum loop iterations exceeded).

// limit on iterations through loop statements with non-constant condition (issue 2350)
// VIPER #7568: Default limit will be set while adding runtime flag for this
//#define NONCONSTANT_LOOPLIMIT 1000
// Nonconstant loop conditions create a 'goto' area in each iteration,
// which is likely to create a lot of logic.
// So set the nonconstant loop limit to such a value that we are unlikely to run out of memory
// if a loop condition is truly non-constant.

        (void) AddVar("vhdl_max_array_size", 23) ;         // Maximum size (in log2(bits) (number of address bits)) before RTL elaboration issues an error (excessive size) message. Value 0 means no limit.
        (void) AddVar("vhdl_max_stack_depth", 900) ;       // Maximum depth of subprogram recursion before RTL elaboration issues an error (stack depth exceeded) message.
        (void) AddVar("vhdl_max_hierarchy_depth", 1000) ;  // Maximum depth of hierarchy before elaboration issues an error (stack depth exceeded) message. (VIPER #7568)
        (void) AddVar("vhdl_create_implicit_labels", 1) ;  // Enable creation of implicit labels for unlabeled loop and concurrent statements (VIPER #7494)
#ifdef VHDL_DO_NOT_CREATE_LONG_UNIT_NAMES
        (void) AddVar("vhdl_do_not_create_long_unit_names", 1) ; // Create short static elaborated unit names for names > 200 characters
#else
        (void) AddVar("vhdl_do_not_create_long_unit_names", 0) ; // Create short static elaborated unit names for names > 200 characters
#endif
        (void) AddVar("vhdl_use_iterative_algorithms", 0) ;  // Iterative algorithm for TypeInfer() routine of vhdl operators(VIPER #8170).

        (void) AddVar("vhdl_ignore_psl_constructs", 0) ;  // Ignore semantic checks and parse tree for psl constructs like sequence/property/assert (VIPER #8525)
    }
} ; // class VhdlRuntimeFlags

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_RUNTIME_FLAGS_H_

