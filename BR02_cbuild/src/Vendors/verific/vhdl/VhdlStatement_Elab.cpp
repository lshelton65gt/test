/*
 *
 * [ File Version : 1.328 - 2014/03/19 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlStatement.h" // Compile flags are defined within here

#include "Array.h"
#include "Set.h"
#include "Map.h"
#include "RuntimeFlags.h" // For run-time global flags
#include "Strings.h"
#include "Message.h"
#include "BitArray.h"

#include "vhdl_tokens.h"
#include "VhdlName.h"
#include "VhdlIdDef.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "vhdl_file.h"
#include "VhdlScope.h"

#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// Cross-language elaboration support :
#include "../verilog/veri_file.h"
#include "../verilog/VeriLibrary.h"
#include "../verilog/VeriModule.h"
#include "../verilog/VeriId.h"
#include "../verilog/VeriExpression.h"
#include "../verilog/VeriConstVal.h" // for actual_params's value deletion
#endif
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
#include "../verilog/VeriElab_Stat.h" // for VeriStaticElaborator
#endif

#include "VhdlCopy.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
//  Elaboration : Elaborate a statement
/**********************************************************/

void VhdlNullStatement::Elaborate(VhdlDataFlow * /*df*/, VhdlBlockConfiguration * /*block_config*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Do Nothing
    return ;
}

void VhdlReturnStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df || !_owner) return ; // something failed

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (df->IsDeadCode()) return ; // we are in dead code

    VhdlDataFlow *effective_df = df ;

    if (_expr) {
        // Get return constraint of the function
        VhdlConstraint *constraint = _owner->Constraint() ;

        // Evaluate the expression
        VhdlValue *ret_val = _expr->Evaluate(constraint,effective_df,0) ;

        // VIPER #7286 : Mark df non-constant if return expression cannot be evaluated
        if (!ret_val && IsStaticElab()) effective_df->SetNonconstExpr() ;
        // Viper #3151 : Check as per LRM section 8.12 Note 2 for array constraints.
        if (constraint && !constraint->IsUnconstrained() && constraint->IsArrayConstraint()) {
            // We need to extract out original constraint, we have to get it from
            // specification.. as otherwise, unconstrained return type might get
            // differently constrained in a function with multiple return stmt.
            // We should not error out in such cases.... So we need to figure out
            // if original return type was unconstrained.
            VhdlSpecification *func_spec = _owner->Spec() ;
            VhdlName *ret_type = func_spec ? func_spec->GetReturnType() : 0 ;
            VhdlIdDef *ret_type_id = ret_type ? ret_type->GetId() : 0 ;
            VhdlConstraint *orig_constraint = ret_type_id ? ret_type_id->Constraint() : 0 ;

            if (orig_constraint && !orig_constraint->IsUnconstrained()) {
                VhdlConstraint *expr_constr = _expr->EvaluateConstraintInternal(effective_df, orig_constraint) ;
                if (expr_constr && !expr_constr->IsUnconstrained()) {
                    VhdlValue *expr_left = expr_constr->Left() ;
                    VhdlValue *left = constraint->Left() ;

                    VhdlValue *expr_right = expr_constr->Right() ;
                    VhdlValue *right = constraint->Right() ;
                    if ((left && !left->IsEqual(expr_left)) || (right && !right->IsEqual(expr_right))) {
                        unsigned issue_error = 1 ;
                        if (IsStaticElab() && effective_df->IsNonconstExpr()) issue_error = 0 ;
                        if (issue_error) {
                            Error("function return type array bounds do not match function return value array bounds") ;
                        } else {
                            Warning("function return type array bounds do not match function return value array bounds") ;
                        }
                    }
                }
                delete expr_constr ;
            }
        }

#if 0   // VIPER 4145 : do NOT adjust the function's return constraint until we return out of the function.
        // So do NOT do this (LRM 3.2.1.1) adjustment here.
        if (constraint && constraint->IsUnconstrained()) {
            constraint->ConstraintBy(ret_val) ;
        }
#endif

        // Test value against constraint
        if (ret_val) (void) ret_val->CheckAgainst(constraint, _expr, effective_df) ;

        // Now assign return value to the owning subprogram identifier
        // Set it in the present area
        effective_df->SetAssignValue(_owner, ret_val) ;
    }

    // 'return' means 'goto-end-of-function'. Lets do that now
    // Use the function id as the goto-label.
    df->DoGoto(_owner, 0/*means condition is always returning*/, this/*from here*/) ;
}

void VhdlExitStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df) return ;
    if (df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (!_target_label) return ; // Analyze guarantees this, but we don't want to assert (Error could be switched off).

    // target label will be the 'exit' label (set in constructor)
    VhdlValue *c = (_condition) ? _condition->Evaluate(0,df,0) : 0 ;
    df->DoGoto(_target_label, c, this) ;
    delete c ;
}

void VhdlNextStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df) return ;
    if (df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (!_target_label) return ; // Analyze guarantees this, but we don't want to assert (Error could be switched off).

    // target label will be the 'next' label (set in constructor)
    VhdlValue *c = (_condition) ? _condition->Evaluate(0,df,0) : 0 ;
    df->DoGoto(_target_label, c, this) ;
    delete c ;
}

void VhdlLoopStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df) return ;
    if (df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    if (!_iter_scheme) {
        // The parser actually never gets here any more, but just out of safety :
        if (IsRtlElab()) Error("infinite loops not supported for synthesis") ;
        return ;
    }

    VhdlScope *old_scope = _present_scope ;

    // VIPER #7683 : Push/pop of label will be done inside Elaborate routine of scheme
    //if (_label) PushNamedPath(_label->OrigName()) ;
    if (_label) {
        PushAttributeSpecificPath(_label->Name()) ;
        _present_scope = _local_scope ? _local_scope : old_scope ; // Push scope
    }

    (void) _iter_scheme->Elaborate(_label, 0/*no decl*/,_statements,0/*no block-config*/, df) ;

    //if (_label) PopNamedPath() ;
    if (_label) {
        PopAttributeSpecificPath() ;
        _present_scope = old_scope ; // Pop scope
    }
}

void VhdlCaseStatement::CheckSanity(VhdlDataFlow *df)
{
    unsigned is_full_case = 1 ;
    if (!df || !_expr || !_alternatives) return ;

    unsigned i, j, k ;
    VhdlCaseStatementAlternative *alt = 0 ;
    unsigned has_others = 0 ;
    // Has others ? Then full case, no need for further check
    FOREACH_ARRAY_ITEM_BACK(_alternatives, i, alt) {
        // others, if present, should be last and only choice
        if (alt && alt->IsOthers()) {
            has_others = 1 ;
            break ;
        }
    }

    // Others clause is not present. Needs further analysis
    VhdlConstraint *case_expr_constraint = _expr->EvaluateConstraintInternal(df, 0) ;

    if (!case_expr_constraint || case_expr_constraint->IsUnconstrained()) {
        delete case_expr_constraint ;
        return ; // be conservative, _expr may be a constant literal
    }

    VhdlConstraint *element_constraint = case_expr_constraint ;
    // Normal case : expression is constrained with a locally static subtype
    verific_uint64 num_array_elements = 1 ;
    unsigned is_array_constraint = case_expr_constraint->IsArrayConstraint() ;
    if (is_array_constraint) {
        num_array_elements = case_expr_constraint->Length() ;
        if (case_expr_constraint->Length() >= sizeof(unsigned)*8) {
            delete case_expr_constraint ;
            if (!has_others) is_full_case = 0 ;
            return ;
        }

        element_constraint = case_expr_constraint->ElementConstraint() ;
        // Viper #5693: Now use this element constraint below
    }

    // Scalar element_constraint
    verific_uint64 num_element_states = element_constraint->Length() ;

    // num_states should be a sensible number, we would have errored
    // out in case_expr_constraint construction/operations otherwise
    verific_uint64 num_states = 1 ;
    while (num_array_elements) { num_states *= num_element_states ; --num_array_elements ; }

    VhdlIdDef *base_expr_type = _expr_type->BaseType() ? _expr_type->BaseType() : _expr_type ;
    VhdlIdDef *element_type = _expr_type->ElementType() ;
    if (element_type && element_type->BaseType()) element_type = element_type->BaseType() ;

    VhdlConstraint *choice_constraint ;
    verific_uint64 num_states_covered = 0 ;
    VhdlDiscreteRange *choice = 0 ;
    Array choice_constraints ;
    Map choice_position(NUM_HASH, _alternatives->Size()+1) ;
    // Viper #5875: Hash must be case sensitive
    Set overlap_checker(STRING_HASH, _alternatives->Size()+1) ;
    Array item_vals(_alternatives->Size()+1) ;
    unsigned found_error = 0 ;
    unsigned size = 0 ;
    FOREACH_ARRAY_ITEM(_alternatives, i, alt) {
        if (!alt) continue ;

        Array *choices = alt->GetChoices() ;
        FOREACH_ARRAY_ITEM(choices, j, choice) {
            if (!choice || choice->IsOthers()) continue ;

            VhdlConstraint *range = 0 ;
            VhdlValue *choice_val = 0 ;
            if (choice->IsRange()) {
                range = choice->EvaluateConstraintInternal(df, 0) ;
                if (!range) {
                    found_error = 1 ;
                    continue ;
                }
            } else {
                choice_val = choice->Evaluate(case_expr_constraint,df,0) ;
                // VIPER #7265 : Do not create the range when choice is not range
                // choice can be scalar, so store the position of 'choice_val'. This
                // will help us to check overlap when case condition is range type (not array)
                //range = choice_val ? new VhdlRangeConstraint(choice_val, VHDL_to, choice_val->Copy(), 0) : 0 ;
                range = 0 ;
            }

            //if (!range) {
                //found_error = 1 ;
                //continue ;
            //}

            if (is_array_constraint) {
                if (!choice_val) continue ; // VIPER #7558: Added null check
                //VERIFIC_ASSERT(choice_val) ;
                char *image = choice_val->Image() ;
                if (!overlap_checker.Insert((void *)choice_val->Image())) {
                    choice->Error("choice %s is already covered", image) ;
                } else if (_is_matching_case && element_type && element_type->IsStdULogicType()) {
                    // Viper 7157: insert into item_vals only if it is a matching case and the expr has
                    // std_ulogic type or vector of std_ulogic type. item_vals array is later processed
                    // to determine parallel and full case.
                    if (choice_val->HasUXZW()) {
                        // Issue error for literals other than 0,1,H,L, and -.
                        choice->Error("matching choice with value %s does not represent any value", image) ;
                    } else {
                        // Convert the value to non const. Here a net X represent the dc.
                        VhdlValue* copy_val = choice_val ? choice_val->Copy() : 0 ;
                        VhdlNonconst* non_const_choice_val = copy_val->ToNonconst() ;
                        unsigned this_size = non_const_choice_val ? non_const_choice_val->Size() : 0 ;
                        size = size < this_size ? this_size : size ;
                        item_vals.InsertLast(non_const_choice_val) ;
                    }
                }
                Strings::free(image) ;
                delete range ;
            } else {
                // VIPER #7265: Check range and choice_val differently, as we have not created
                // range when 'choice' is not range
                if ((range && (!case_expr_constraint->Contains(range->Low()) || !case_expr_constraint->Contains(range->High()))) ||  // choice is range
                      (choice_val && !case_expr_constraint->Contains(choice_val))) { // choice is single value
                    // range is outside the constraint
                    char *val_image = choice_val ? choice_val->Image() : (range ? range->Image(): 0) ;
                    choice->Warning("case statement choice %s is outside the range of the select", val_image) ;
                    Strings::free(val_image) ;

                    found_error = 1 ; // do not check further
                }

                char *image = choice_val ? choice_val->Image() : (range ? range->Image(): 0) ;
                // VIPER #7265 : We are maintaining a Set 'choice_postion' for the choices
                // which are not range. Also we are maintaining array 'choice_constraints'
                // for the range choices. So need to check both for overlap
                unsigned overlap = 0 ;
                // Check overlap in 'Set 'choice_postion' first
                // FIXME : Position returns 'verific_int64', casting it to
                // (void*) in 32 bit machine can truncate the value
                // However for scalar like integer/enum Position() should
                // not exceed 32 bit value
                if (choice_val && !choice_position.Insert((void*)(unsigned long)choice_val->Position(), choice)) {
                    overlap = 1 ; // already in position set
                }
                if (range && !range->IsNullRange()) { // 'choice' is range
                    // Check if every element of 'choice_postion' is in the 'range'. If so
                    // this is an overlap
                    VhdlValue *left = range->Left() ;
                    VhdlValue *right = range->Right() ;
                    verific_int64 l_pos = left ? left->Position(): 0 ;
                    verific_int64 r_pos = right ? right->Position(): 0 ;
                    unsigned dir = range->Dir() ;
                    MapIter iter ;
                    verific_uint64 pos = 0 ; // Viper 8212 must use datatype >= sizeof(void*) for container iterators (long = 32b in windows)
                    FOREACH_MAP_ITEM(&choice_position, iter, &pos, 0) {
                        verific_int64 ps = (verific_int64)pos ;
                        // Value is OUT of range if its position is below the lowest, or above the highest
                        if (dir == VHDL_to) {
                            if (!((r_pos < ps) || (ps < l_pos))) {
                                overlap = 1 ; break ;
                            }
                        } else {
                            if (!((l_pos < ps) || (ps < r_pos))) {
                                overlap = 1 ; break ;
                            }
                        }
                    }
                }
                if (!overlap) {
                    // Now check the current choice with previously defined ranges
                    FOREACH_ARRAY_ITEM(&choice_constraints, k, choice_constraint) {
                        if (!choice_constraint) continue ;

                        overlap = choice_val ? choice_constraint->Contains(choice_val) : (range ? choice_constraint->CheckAgainst(range, 0, 0): 0) ;
                        if (overlap) {
                            //choice->Error("choice %s is already covered", image) ;
                            // no point to continue this excercise
                            //found_error = 1 ;
                            break ;
                        }
                    }
                }
                if (overlap) {
                    choice->Error("choice %s is already covered", image) ;
                    // no point to continue this excercise
                    found_error = 1 ;
                }
                num_states_covered += range ? range->Length(): 1 ;
                if (range) choice_constraints.Insert(range) ;

                // Viper 7157: insert into item_vals only if it is a matching case and the expr has
                // std_ulogic type or vector of std_ulogic type. item_vals array is later processed
                // to determine parallel and full case.
                if (_is_matching_case && base_expr_type && base_expr_type->IsStdULogicType()) {
                    if (choice_val && choice_val->HasUXZW()) {
                        // Issue error for literals other than 0,1,H,L, and -.
                        choice->Error("matching choice with value %s does not represent any value", image) ;
                    } else {
                        // Convert the value to non const. Here a net X represent the dc.
                        VhdlValue* copy_val = choice_val ? choice_val->Copy() : 0 ;
                        VhdlNonconst* non_const_choice_val = copy_val ? copy_val->ToNonconst() : 0 ;
                        if (non_const_choice_val) item_vals.InsertLast(non_const_choice_val) ;
                        size = 1 ;
                    }
                }

                Strings::free(image) ;
            }
            delete choice_val ;
        }
    }

    // Viper 7157: Do a more stringent check for case item containing meta values.
    unsigned is_full_matching_case = 0 ;
    if (_is_matching_case && item_vals.Size() && !found_error) {
        unsigned is_parallel = 1 ;
        unsigned is_full = 1 ;
        BitArray done_columns(size) ;
        AnalyzeFullParallel(&item_vals, &done_columns, size, &is_parallel, &is_full) ;
        if (!is_parallel) {
            _expr->Error("case statement have same choice in multiple items") ;
            found_error = 1 ;
        }
        if (!found_error && !is_full && !has_others) {
            _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;
            found_error = 1 ;
        }
        if (is_full) is_full_matching_case = 1 ;
    }

    if (!num_states_covered) num_states_covered = overlap_checker.Size() ;
    if (num_states_covered < num_states && !has_others && !found_error && !is_full_matching_case) is_full_case = 0 ;

    FOREACH_ARRAY_ITEM(&choice_constraints, k, choice_constraint) delete choice_constraint ;
    VhdlValue *item_val ;
    FOREACH_ARRAY_ITEM(&item_vals, k, item_val) delete item_val ;

    SetIter si ;
    char * image ;
    FOREACH_SET_ITEM(&overlap_checker, si, &image) Strings::free(image) ;

    if (!is_full_case) _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;

    delete case_expr_constraint ;
    return ;
}

void VhdlSelectedSignalAssignment::CheckSanity(VhdlDataFlow *df)
{
    unsigned is_full_case = 1 ;
    if (!df || !_expr || !_selected_waveforms) return ;

    unsigned i, j, k ;
    VhdlSelectedWaveform *alt = 0 ;
    unsigned has_others = 0 ;
    // Has others ? Then full case, no need for further check
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, alt) {
        // others, if present, should be last and only choice
        if (alt && alt->IsOthers()) has_others = 1 ;
    }

    // Others clause is not present. Needs further analysis
    VhdlConstraint *case_expr_constraint = _expr->EvaluateConstraintInternal(df, 0) ;

    if (!case_expr_constraint || case_expr_constraint->IsUnconstrained()) {
        delete case_expr_constraint ;
        return ; // be conservative, _expr may be a constant literal
    }

    VhdlConstraint *element_constraint = case_expr_constraint ;
    // Normal case : expression is constrained with a locally static subtype
    verific_uint64 num_array_elements = 1 ;
    unsigned is_array_constraint = case_expr_constraint->IsArrayConstraint() ;
    if (is_array_constraint) {
        num_array_elements = case_expr_constraint->Length() ;
        if (case_expr_constraint->Length() >= sizeof(unsigned)*8) {
            delete case_expr_constraint ;
            if (!has_others) is_full_case = 0 ;
            return ;
        }

        element_constraint = case_expr_constraint->ElementConstraint() ;
        // Viper #5693: Now use this element constraint below
    }

    // Scalar element_constraint
    verific_uint64 num_element_states = element_constraint->Length() ;

    // num_states should be a sensible number, we would have errored
    // out in case_expr_constraint construction/operations otherwise
    verific_uint64 num_states = 1 ;
    while (num_array_elements) { num_states *= num_element_states ; --num_array_elements ; }

    VhdlIdDef *base_expr_type = _expr_type->BaseType() ? _expr_type->BaseType() : _expr_type ;
    VhdlIdDef *element_type = _expr_type->ElementType() ;
    if (element_type && element_type->BaseType()) element_type = element_type->BaseType() ;

    VhdlConstraint *choice_constraint ;
    verific_uint64 num_states_covered = 0 ;
    VhdlDiscreteRange *choice = 0 ;
    Array choice_constraints ;
    // Viper #5875: Hash must be case sensitive
    Set overlap_checker(STRING_HASH, _selected_waveforms->Size()+1) ;
    Array item_vals(_selected_waveforms->Size()+1) ;
    unsigned found_error = 0 ;
    unsigned size = 0 ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, alt) {
        if (!alt) continue ;

        Array *choices = alt->GetWhenChoices() ;
        FOREACH_ARRAY_ITEM(choices, j, choice) {
            if (!choice || choice->IsOthers()) continue ;

            VhdlConstraint *range = 0 ;
            VhdlValue *choice_val = 0 ;
            if (choice->IsRange()) {
                range = choice->EvaluateConstraintInternal(df, 0) ;
            } else {
                choice_val = choice->Evaluate(case_expr_constraint,df,0) ;
                range = choice_val ? new VhdlRangeConstraint(choice_val, VHDL_to, choice_val->Copy(), 0) : 0 ;
            }

            if (!range) {
                found_error = 1 ;
                continue ;
            }

            if (is_array_constraint) {
                VERIFIC_ASSERT(choice_val) ;
                char *image = choice_val->Image() ;
                if (!overlap_checker.Insert((void *)choice_val->Image())) {
                    choice->Error("choice %s is already covered", image) ;
                } else if (_is_matching && element_type && (element_type == StdType("std_ulogic"))) {
                    // Viper 7157: insert into item_vals only if it is a matching case and the expr has
                    // std_ulogic type or vector of std_ulogic type. item_vals array is later processed
                    // to determine parallel and full case.
                    if (choice_val->HasUXZW()) {
                        // Issue error for literals other than 0,1,H,L, and -.
                        choice->Error("matching choice with value %s does not represent any value", image) ;
                    } else {
                        // Convert the value to non const. Here a net X represent the dc.
                        VhdlValue* copy_val = choice_val ? choice_val->Copy() : 0 ;
                        VhdlNonconst* non_const_choice_val = copy_val->ToNonconst() ;
                        unsigned this_size = non_const_choice_val ? non_const_choice_val->Size() : 0 ;
                        size = size < this_size ? this_size : size ;
                        item_vals.InsertLast(non_const_choice_val) ;
                    }
                }
                Strings::free(image) ;
                delete range ;
            } else {
                if (!case_expr_constraint->Contains(range->Low()) || !case_expr_constraint->Contains(range->High())) {
                    // range is outside the constraint
                    char *val_image = choice_val ? choice_val->Image() : range->Image() ;
                    choice->Warning("case statement choice %s is outside the range of the select", val_image) ;
                    Strings::free(val_image) ;

                    found_error = 1 ; // do not check further
                }

                char *image = choice_val ? choice_val->Image() : range->Image() ;
                FOREACH_ARRAY_ITEM(&choice_constraints, k, choice_constraint) {
                    if (!choice_constraint) continue ;

                    unsigned overlap = choice_val ? choice_constraint->Contains(choice_val) : choice_constraint->CheckAgainst(range, 0, 0) ;
                    if (overlap) {
                        choice->Error("choice %s is already covered", image) ;
                        // no point to continue this excercise
                        found_error = 1 ;
                        break ;
                    }
                }

                num_states_covered += range->Length() ;
                choice_constraints.Insert(range) ;

                // Viper 7157: insert into item_vals only if it is a matching case and the expr has
                // std_ulogic type or vector of std_ulogic type. item_vals array is later processed
                // to determine parallel and full case.
                if (_is_matching && base_expr_type && base_expr_type == StdType("std_ulogic")) {
                    if (choice_val && choice_val->HasUXZW()) {
                        // Issue error for literals other than 0,1,H,L, and -.
                        choice->Error("matching choice with value %s does not represent any value", image) ;
                    } else {
                        // Convert the value to non const. Here a net X represent the dc.
                        VhdlValue* copy_val = choice_val ? choice_val->Copy() : 0 ;
                        VhdlNonconst* non_const_choice_val = copy_val ? copy_val->ToNonconst() : 0 ;
                        if (non_const_choice_val) item_vals.InsertLast(non_const_choice_val) ;
                        size = 1 ;
                    }
                }

                Strings::free(image) ;
            }
        }
    }

    // Viper 7157: Do a more stringent check for case item containing meta values.
    unsigned is_full_matching_case = 0 ;
    if (_is_matching && item_vals.Size() && !found_error) {
        unsigned is_parallel = 1 ;
        unsigned is_full = 1 ;
        BitArray done_columns(size) ;
        AnalyzeFullParallel(&item_vals, &done_columns, size, &is_parallel, &is_full) ;
        if (!is_parallel) {
            _expr->Error("case statement have same choice in multiple items") ;
            found_error = 1 ;
        }
        if (!found_error && !is_full && !has_others) {
            _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;
            found_error = 1 ;
        }
        if (is_full) is_full_matching_case = 1 ;
    }

    if (!num_states_covered) num_states_covered = overlap_checker.Size() ;
    if (num_states_covered < num_states && !has_others && !found_error && !is_full_matching_case) is_full_case = 0 ;

    FOREACH_ARRAY_ITEM(&choice_constraints, k, choice_constraint) delete choice_constraint ;
    VhdlValue *item_val ;
    FOREACH_ARRAY_ITEM(&item_vals, k, item_val) delete item_val ;

    SetIter si ;
    char * image ;
    FOREACH_SET_ITEM(&overlap_checker, si, &image) Strings::free(image) ;

    if (!is_full_case) _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;

    delete case_expr_constraint ;
    return ;
}

// Viper 7157: determine if a matching case is full and parallel
void
VhdlStatement::AnalyzeFullParallel(const Array *item_vals, BitArray *done_columns, unsigned max_size, unsigned *is_parallel, unsigned *is_full)
{
    // Arguments that should be here (passed as pointer, to avoid BitArray/Array header file inclusion in header files).
    if (!item_vals || !done_columns) return ;

    if (item_vals->Size()==0) {
        // No case items. This is not full.
        *is_full = 0 ;
        // no use to continue without items.
        return ;
    }

    // Case statement items analysis :
    // 'item_vals' is an array of (N) VeriValue* values of (M) (max_size) bits.
    // Analyze if 'item_vals' is 'full', 'parallel' and/or 'constant'.
    // Only DISPROVE full/parallel/constant : (only determine NOT full, NOT parallel and NOT constant).
    // This is what we do :
    //   Choose a bit-column.
    //   Then, for each value in 'item_vals' :
    //   If the value is 0 at that bit column, put it in a new array 'zeros'
    //   If the value is 1 at that bit column, put it in a new array 'ones'
    //   If the value is x at that bit column, put in in both arrays.
    //   Then recur into both arrays (zeros and ones).
    //
    // This is a linear algorithm as long as there are no x'es (array's just get smaller as columns increment). Recursion depth M-1. O(MxN) complexity.
    // However, it is exponential in the presence of x'es (because x'es split over both arrays. O(N x 2**(M)) complexity
    // To prevent exponential explosion, use the following early jump-outs:
    //  (1) If there are 0 items in the array, then it is NOT full.
    //  (2) If there is 1 item in the array, then it is NOT full if remaining columns are not all x'es.
    //  (3) If the minimum number of x'es of all columns equals the number of rows, then the entire field (columns and rows)
    //      has all x'es. That requires no further analysis : It's full but it is NOT parallel if there are more than one rows.
    //  (4) Otherwise (more than 1 item left over), choose the new column with the LEAST amount of x'es (0 x'es is as good as it gets).
    // This means that columns with a lot of x'es get pushed out (with little expansion) to last, where it will end up in fields of x'es, caught quickly by (3) before x-explosion would occur.
    // This means that ANY array of NxM will be done in approximately O(NxM) complexity (is linear with design size).

    unsigned c ;
    unsigned i ;
    unsigned num_of_xes_in_chosen_c = item_vals->Size()+1 ; // number of x'es in selected column. Initialize to number bigger than num_of_columns.
    unsigned num_of_xes ;
    unsigned chosen_c = 0 ;
    VhdlValue *item_val ;
    Net *net ;
    unsigned remaining_columns = 0 ;

    // Viper #4086: Check if all bits in a row are dont_care
    FOREACH_ARRAY_ITEM(item_vals, i, item_val) {
        if (*is_parallel==1) break ; // we still need to analyze for parallel case
        if (!item_val) continue ; // ignore

        unsigned all_bits_dc = 1 ;
        for (c=0;c<max_size;c++) {
            if (done_columns->GetBit(c)) continue ; // already did this column

            net = item_val->GetBit(c) ;
            if (net!=_x && net!=_z) {
                all_bits_dc = 0 ;
                break ;
            }
        }

        if (all_bits_dc) { // full case
            return ; // full case
        }
    } // Viper #4086: end

    // Analyze the field : find the column with the MINIMUM number of x'es in the field.
    // We need that (minimum number of x'es in columns in the field and the column that has that)
    // as well as other info (all columns done?) to test for the special cases (that will reduce complexity to linear).
    for (c=0;c<max_size;c++) {
        if (done_columns->GetBit(c)) continue ; // already did this column
        remaining_columns++ ;
        // count number of x'es in this column :
        num_of_xes = 0 ;
        FOREACH_ARRAY_ITEM(item_vals, i, item_val) {
            if (!item_val) continue ; // ignore
            net = item_val->GetBit(c) ;
            if (net==_x || net==_z) num_of_xes++ ;
            // See if this column is still an option to be chosen :
            if (num_of_xes > num_of_xes_in_chosen_c) break ; // no use to continue. Too many x'es in this column.
        }
        // select the column with the smallest number of x'es :
        if (num_of_xes < num_of_xes_in_chosen_c) {
            // good choice of column
            chosen_c = c ;
            num_of_xes_in_chosen_c = num_of_xes ;
            // Check if we can get any better than this :
            if (num_of_xes_in_chosen_c==0) {
                // there are no xes in this column. Choose it ! It is as good as it gets.
                break ;
            }
        }
    }

    // Check special cases :
    VERIFIC_ASSERT(item_vals) ; // To stop Gimpel
    if (item_vals->Size()==1) {
        // There is only one item left over. No use to split further.
        // If the minimum of x'es in all columns of this remaining item is 0, then at least one column had a 0 or 1 (fixed value).
        // That means it was not full.
        if (num_of_xes_in_chosen_c==0) {
            *is_full = 0 ;
        }
        return ;
    }
    if (remaining_columns==0 || num_of_xes_in_chosen_c==item_vals->Size()) {
        // All columns are analyzed, or there are only x'es left over in the entire field.
        // The minimum number of x'es in any column is 'num_of_xes_in_chosen_c', and this is equal to the number of values (rows).
        // That means that ALL columns of ALL rows contain x'es (a field of all x'es).
        // That is obviously full, but is NOT parallel if there are more than one items in the item_vals array.
        if (item_vals->Size()>1) {
            *is_parallel = 0 ;
        }
        return ;
    }

    // Before we split the array (and potentially increase complexity), check if it makes sense to continue.
    // If 'full' and 'parallel' are already set, then no use to continue.
    if (*is_full==0 && *is_parallel==0) return ;

    // Create 2 arrays, one with item values that have bit at chosen_c set to 0, other with bit set to 1. x gets inserted in both.
    Array zeros(item_vals->Size()) ;
    Array ones(item_vals->Size()) ;
    FOREACH_ARRAY_ITEM(item_vals, i, item_val) {
        if (!item_val) continue ; // ignore
        net = item_val->GetBit(chosen_c) ;
        if (net==_pwr) {
            ones.InsertLast(item_val) ;
        } else if (net==_gnd) {
            zeros.InsertLast(item_val) ;
        } else if (net==_x || net==_z) {
            // insert both sides :
            zeros.InsertLast(item_val) ;
            ones.InsertLast(item_val) ;
        } else {
            // non-constant, may be not full and certainly not parallel.
            *is_full = 0 ;
            *is_parallel = 0 ;
            return ; // no use to proceed.
        }
    }

    // set the 'done' column :
    done_columns->SetBit(chosen_c) ;

    // recur into both arrays :
    AnalyzeFullParallel(&zeros, done_columns, max_size, is_parallel, is_full) ;
    AnalyzeFullParallel(&ones, done_columns, max_size, is_parallel, is_full) ;

    // re-set this column
    done_columns->ClearBit(chosen_c) ;
}

/*
 * We use separate equal comparators with selectors if
 *  num_of_cases < (2**size) / EQ_SEL_BREAKPOINT.
 * otherwise, use a N-1 muxes
*/

#define EQ_SEL_BREAKPOINT 8

void VhdlCaseStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df || !_expr || !_alternatives) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if (df->IsDeadCode()) return ; // we are in dead code

#ifdef CHECK_CASE_STATEMENT_BEFORE_METALOGICAL_PRUNING
    // Some versions of the Vital package, timing_b.vhd has
    // incomplete case statement, if this check includes the
    // metalogical values as possibilities. Synthesis customers
    // using such packages may consider disable this check by
    // turning off the compile switch
    CheckSanity(df) ; // produces error if appropriate
#endif // CHECK_CASE_STATEMENT_BEFORE_METALOGICAL_PRUNING

    // Declare some stuff that we need :
    unsigned i ;
    VhdlCaseStatementAlternative *alt, *others = 0;

    // Hash choice (DiscreteRange)->case-item-dataflow(DataFlow) in 'choices'.
    // These are all just pointers, no memory allocated.
    // There will be an entry inserted by ElaborateChoices() for a case item that matches the case expression.
    Map choices(POINTER_HASH, _alternatives->Size()+1 /*reasonable estimate of size*/) ;
    verific_uint64 extra_count = 0 ;

    // Hash num_value(int)->case-item-dataflow(DataFlow) in 'positions'.
    // In VHDL, all choices are constant. for <32 bit
    // values, we can hash and test for uniqueness.
    // This table also helps to find alternatives by hashing on
    // their numeric value. Only works for <32 bit.
    // So we use this Map only for <32 bit expressions.
    Map positions(NUM_HASH, _alternatives->Size()+1) ;

    // Viper #3598 : choice_positions cannot be used for overlap checking
    // When enum_encoding attribute is not present, the way we generate
    // hash key currently, can easily result in collision of hash keys for
    // distinct values. Need to generate more unique keys, using Image for now

    Set overlap_check(STRING_HASH, _alternatives->Size()+1) ;

    // Evaluate case expression
    VhdlValue *expr = _expr->Evaluate(0,df,0) ;

    if (_is_matching_case && expr && expr->IsMetalogicalWithDC()) {
        // Viper 7539 : LRM Section 10.9 Vhdl 2008 says: For a matching case statement in
        // which the expression is of type STD_ULOGIC, or an array type whose
        // element type is STD_ULOGIC, it is an error if the value of the expression
        // is the scalar value '–' or an array value containing '–' as an element.
        _expr->Error("matching case selector expression contains meta value '-'") ;
    }

    if ((!expr || !expr->IsConstant()) && IsStaticElab()) {
        df->SetNonconstExpr() ; // Mark that this data flow contains non constant expression (VIPER 3815)
    }
    if (!expr && IsRtlElab()) return ;

    VhdlIdDef *case_expr_id = IsRtlElab() ? _expr->FindFullFormal() : 0 ; // Viper #5553

    // Get the case expression constraint :
    VhdlConstraint *case_expr_constraint = _expr->EvaluateConstraintInternal(df, 0) ;

    if (case_expr_constraint && case_expr_constraint->IsUnconstrained()) {
        // Violation of LRM 8.5 : unconstrained array type not allowed
        // This rule is very strict, while not needed. Make it a warning.
        if (!expr || !expr->IsConstant()) _expr->Warning("subtype of case expression is not locally static") ;
        delete case_expr_constraint ;
        case_expr_constraint = 0 ; // Cant proceed with the unconstrained type will blow up the num_states.
    }

    // First check for constant case expression :
    if (expr && expr->IsConstant()) {
        // Special case ! Will be done with this one much quicker than with the non-constant forms :
        // The case expression is constant.
        // What we need to do here is just try every case alternative, until one hits (gets inserted by ElaborateChoices()).
        // If none hit, the 'others' alternative is chosen.
        // Once one hits, we can execute that alternative (in the same dataflow as 'this')
        // and we can discard all other alternatives.
        others = 0 ;
        Map *condition_id_value_map = 0 ;
        FOREACH_ARRAY_ITEM(_alternatives, i, alt) {
            others = alt->ElaborateChoices(choices, positions, expr, df, case_expr_constraint, overlap_check, 0, condition_id_value_map, _is_matching_case, extra_count) ;
            VERIFIC_ASSERT(!condition_id_value_map) ; // The map is not created for constant expr
            if (choices.Size() || alt==others) {
                // This choice did 'hit' (it matches the constant case expression)
                // or this is the 'others' case item.
                // Elaborate this alternative.
                alt->ElaborateAlternative(df) ;
                break ; // we are done.
            }
        }
        // See if anything hit :
        if (!others && !choices.Size()) {
            _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;
        }

        // clean up.
        SetIter si ;
        char * image ;
        FOREACH_SET_ITEM(&overlap_check, si, &image) {
            Strings::free(image) ;
        }

        delete case_expr_constraint ;
        delete expr ;
        // Done with everything.
        return ;
    }

    // Here, the case expression is non-constant.
    // Error out on unsupported edge in case expression
    if (expr && (expr->IsEdge() || expr->IsEvent()) && IsRtlElab()) {
        _expr->Error("case-statement with edge or event expression not supported for synthesis ; use an if-statement instead") ;
        // continue as if it is a combinational expression
    }

    // Calculations up-front : calculate the number of states this case expression can be in :
    verific_uint64 num_states = 0 ; // Viper #4222: type changed from unsigned -> verific_uint64

    // number of bits (handy) :
    unsigned num_bits = expr ? expr->NumBits(): 0 ;

    unsigned is_unconstrained = 0 ;
    // Do a real accurate VHDL analysis using the case expression subtype constraint :
    if (case_expr_constraint) {
        if (case_expr_constraint->IsUnconstrained()) is_unconstrained = 1 ;

        // Viper #5693: Needs proper state calculation for array of
        // non-bit-encoded types
        VhdlConstraint *element_constraint = case_expr_constraint ;
        // Normal case : expression is constrained with a locally static subtype
        verific_uint64 num_array_elements = 1 ;
        if (case_expr_constraint->IsArrayConstraint()) {
            num_array_elements = case_expr_constraint->Length() ;
            if (case_expr_constraint->Length() >= sizeof(unsigned)*8)  is_unconstrained = 1 ;
            element_constraint = case_expr_constraint->ElementConstraint() ;
            // Viper #5693: Now use this element constraint below
        }

        // Scalar element_constraint
        verific_uint64 num_element_states = element_constraint->Length() ;
        if (element_constraint->IsBitEncoded() && (num_element_states > 2)) {
            // Choices with X and Z are already removed from the 'choices' array,
            // so we will not have more than 2 states.
            num_element_states = 2 ;
        }

        // num_states should be a sensible number, we would have errored
        // out in case_expr_constraint construction/operations otherwise
        num_states = 1 ;
        while (num_array_elements) { num_states *= num_element_states ; --num_array_elements ; }

        if (is_unconstrained) num_states = 0 ; // Viper #3840
    } else if (expr) {
        // If we don't have a VHDL constraint (should not happen often any more) :
        // derive number of states from the number of bits.
        if (expr->OnehotEncoded()) {
            num_states = num_bits ;
        } else if (num_bits < sizeof(unsigned)*8) {
            num_states = (verific_uint64)1 << num_bits ;
        }
    }

    // Also store dataflows somewhere, so we can delete them at the end
    Array dataflows(_alternatives->Size()+1) ;

    // Now run over all case alternatives, and collect their choice->dataflows in 'choices' and 'positions'
    VhdlDataFlow *sub_df, *others_df = 0 ;
    unsigned last ;
    unsigned has_range_compare = 0 ;
    unsigned num_choices = 0 ;
    if (_is_matching_case) {
        FOREACH_ARRAY_ITEM(_alternatives, i, alt) {
            if (!alt) continue ;
            Array* case_choices = alt->GetChoices() ;
            num_choices += case_choices ? case_choices->Size() : 0 ;
            // See if there are any 'range' choices in this alternative
            if (alt->HasRangeChoice()) has_range_compare = 1 ;
        }
    }
    // Take the power-of two of the number of bits (set to 0 for >32 bits) :
    unsigned mux_size = (num_bits < sizeof(unsigned)*8) ? (1 << num_bits) : 0;
    unsigned populate_dc_positions = (!mux_size || num_bits==1 || has_range_compare || (!expr || expr->OnehotEncoded()) || num_choices < mux_size/EQ_SEL_BREAKPOINT) ? 0 : 1 ;

    FOREACH_ARRAY_ITEM(_alternatives, i, alt) {
        last = choices.Size() ;

        // Create new dataflow area, and store (for later cleanup)
        sub_df = new VhdlDataFlow(df) ;
        dataflows.InsertLast(sub_df) ;

        Map *condition_id_value_map = 0 ;

        // Elaborate all case conditions, filter out meta-values,
        // test for constant, and store them (choice->df) in 'choices' Map
        // There could be many values, and only a few alternatives
        // In VHDL, 'others' choice should be last. We tested that in the constructor already.
        // When encountered in 'ElaborateChoices', it will be returned.
        // For smaller than integer choices, we also hash (int->df) into 'positions' Map.

        // Viper 7157: Pass two new integers extra_count and populate_dc_positions required for synthesizng case statements
        // with dont_cares. extra_count contains the extra number of choices accomodated by use of  dont_cares
        // term '-', extra_count is passed by reference to ElaborateChoices. populate_dc_positions stores the decision
        // about whether we want to create equal operaor or muxes. If populate_dc_positions is true we  explode the
        // dont_cares  literals to generate all possible literals and populate the positions array. This is done
        // for matching select statements of Vhdl2008 containing meta value '-' in choice.

        others = alt->ElaborateChoices(choices, positions, expr, sub_df, case_expr_constraint, overlap_check, case_expr_id, condition_id_value_map, _is_matching_case, extra_count, populate_dc_positions) ;

        (void) sub_df->SetConditionIdValueMap(condition_id_value_map) ; // Viper #5553

        // See if there are any 'range' choices in this alternative
        if (alt->HasRangeChoice()) has_range_compare = 1 ;

        // Execute statements if some choice was added
        if (last != choices.Size()) {
            // This item matched for some conditions :
            alt->ElaborateAlternative(sub_df) ;
        } else if (alt == others) {
            // This is the others clause. Also assume it will match for some conditions.
            alt->ElaborateAlternative(sub_df) ;
            // Keep a pointer to the dataflow for 'others' case
            others_df = sub_df ;
        } else if (!expr && IsStaticElab()) {
            alt->ElaborateAlternative(sub_df) ;
        }
    }
    delete case_expr_constraint ; // It surved its purpose
    SetIter si ;
    char * image ;
    FOREACH_SET_ITEM(&overlap_check, si, &image) {
        Strings::free(image) ;
    }

    // Check if we are full-case
    // FIX ME : what to do with user parallel/full case pragmas
    unsigned full_case = (expr || num_states) ? 0 : 1 ;
    if (!full_case && num_states && !has_range_compare && num_states<=(choices.Size() + extra_count)) { // add extra count to account for meta value states
        // We have full case
        if (others) {
            // there is an others clause, but we already have 'full' case !
            // So we can ignore (don't-care) the others clause assignments.
            unsigned ignore_others = 1 ;

#ifdef VHDL_DONT_IGNORE_OTHERS_CLAUSE
            // Sometimes a state machine type has a non-power-of-two number of
            // states (enumeration values).
            // In synthesis, the state machine can then actually occur (in real life)
            // in a state that is not possible in simulation.
            // If the state machine has no asynchronous reset, it might never
            // get out of such a state, depending on how the next-state logic is
            // implemented for the 'others' cases (that cannot occur in simulation)
            //
            // To give the user control over this, some synthesis tools don't ignore the
            // 'others' clause.
            //
            // This allows users to set the 'next state' explicitly if the state machine
            // is in an 'unsimulatable' state.
            //
            // Under this compile switch, this is exactly what we do :
            // Check if the number of states that are possible for synthesis (2**num_bit)
            // matches the number of states that are possible according to simulation (num_states).
            //
            // If that number differs, we will NOT ignore the others clause.
            // It then becomes part of synthesis, and the user can define the transitions
            // there, even though the state(s) in the 'others' part cannot occur in simulation.
            //
            //
            // NOTE : This is INDEPENDENT of the actual encoding used for enumeration values !
            // We are just testing if the number of simulation values matches the number of
            // possible states that can occur in real life, given 'num_bits' bits for the representation.
            // So we are doing the right tests here.
            //
            // NOTE 2 : If the user forgets to assign some signals in the 'others' clause,
            // compiling with this switch on can now cause latches (comb feedback loops) to
            // occur, that are not there without this switch.
            //
            // Without this switch, the next state transitions will get unknown (don't-care) values,
            // to mimic simulation and not make assumptions about which choice any other tools make.

            // Calculate the number of physically reachable states (using the number of bits in case expression) :
            unsigned tmp_num_states = 1 << num_bits ;
            // Compare that to the simulation states (using 'num_states') :
            ignore_others = (tmp_num_states == num_states) ;
#endif

            if (ignore_others) {
                // Give the info msg only for rtl elab.. for simulation applications
                // each state may not be bit-encoded, e.g. std_logic can have 9 states
                if (IsRtlElab()) others->Info("others clause is never selected") ;
                others = 0 ; // Pretend it was never written
                others_df = 0 ; // Make sure assigned values in others become 'X'
            }
        }
        full_case = 1 ;
    }

    // Next test also catches integer case-expression without an others clause
    if (!full_case && !has_range_compare && !others) {
        // CHECK ME : Could there still be case statements that are legally full case,
        // but still end up here ?
        // I don't think so. Should always work. Or else we should fix the expr_constraint.
        _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;
    }

    if (!expr) {
        // Clean up the (sub)dataflows (Dataflows stored here).
        unsigned is_alive = 0 ;
        // VIPER #7286 : Invalidate the values of assigned ids
        if (IsStaticElab()) df->InvalidateIdValues(&dataflows, this) ;
//        unsigned is_not_waiting = 0 ;
        FOREACH_ARRAY_ITEM(&dataflows, i, sub_df) {
            if (sub_df && !sub_df->IsDeadCode()) is_alive = 1 ;
            if (IsStaticElab() && sub_df) {
                if (sub_df->IsNonconstExpr()) df->SetNonconstExpr() ;
//                if (!sub_df->IsWaiting()) is_not_waiting = 1 ;
            }
            delete sub_df ;
        }
        if (!is_alive) df->SetDeadCode() ;
//        if (!is_not_waiting) df->SetIsWaiting() ;
        return ;
    }

    // VHDL is always 'parallel' case
    // All values are unique and constant.
    // This is tested when we ElaborateChoices() (above) for each alternative.

    // Select the architecture based on the size of the mux that
    // we would create. That depends on the number of bits in the
    // expression versus the number of choices in the case statement.
    // For user-defined encodings, the number of bits can be much larger
    // than the num_states would require from a binary encoding point of view.

    // Take the power-of two of the number of bits (set to 0 for >32 bits) :
    mux_size = (num_bits < sizeof(unsigned)*8) ? (1 << num_bits) : 0;

    // Viper 7157: Do not convert the expr to non-const. We would use the dont_care
    // information in the composite value to modify the equal operator
    // Translate expression to nonconst
    // VhdlNonconst *cond = expr->ToNonconst() ;
    // Issue 2189 : do not translate to a nonconst unless really needed (like in the Pop routines below)

    // Select the to-be-implemented architecture and resolve id's and goto-areas
    if (!mux_size || num_bits==1 || has_range_compare || expr->OnehotEncoded() ||
        (choices.Size() < mux_size/EQ_SEL_BREAKPOINT)) {
        // Use 'choices' to create equal-comparators plus selectors
        df->PopOutOfCaseWithEquals(expr, &choices, others_df, this, _is_matching_case) ;
        // Clean up the case-expression
        delete expr ;
    } else {
        // Translate expression to nonconst
        VhdlNonconst *cond = expr->ToNonconst() ;
        // Issue 2189 : do not translate to a nonconst unless really needed (like in the Pop routines below)
        // Use 'positions' table to create N-1 muxes
        df->PopOutOfCaseWithMuxes(cond, &positions, others_df, this) ;
        // Clean up the case-expression
        delete cond ;
    }

    // Clean up the (sub)dataflows (Dataflows stored here).
    FOREACH_ARRAY_ITEM(&dataflows, i, sub_df) {
        delete sub_df ;
    }

    // 'choices' has no allocated memory (tree-node->df-pointer)
    // 'positions' has no allocated memory (number->df-pointer).
}

void VhdlIfStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df || !_if_cond) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // df should always be there : if statement cannot occur outside dataflow area
    VERIFIC_ASSERT(df) ;
    if (df->IsDeadCode()) return ; // we are in dead code

    VhdlDataFlow *effective_df = df ;

        // Viper #5696: Eleborate is now a wrapper for variable slice
        // loop under the compile flag. ElaborateCore is the original
        // elaboration routine
        ElaborateCore(effective_df) ;
}

void VhdlIfStatement::ElaborateCore(VhdlDataFlow *df)
{
    if (!df || !_if_cond) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // df should always be there : if statement cannot occur outside dataflow area
    VERIFIC_ASSERT(df) ;
    if (df->IsDeadCode()) return ; // we are in dead code

    // Viper #4233/4234 : Do not mask error in static_elab for conditions in non-constant dataflow
    // TODO: Please review the non-constant dataflow strategy. This looks very fragile.
//    df->SetConditionExpr(1) ; // RD: no longer needed since VIPER 7018 fix.

    Map *condition_id_value_map = 0 ;
    VhdlValue *cond = _if_cond->EvaluateCondition(df, condition_id_value_map) ; // Viper #5553

    if (!cond || cond->IsConstant()) {
        VhdlValue *val ;
        MapIter mi ;
        FOREACH_MAP_ITEM(condition_id_value_map, mi, 0, &val) delete val ;
        delete condition_id_value_map ;
        condition_id_value_map = 0 ;
    }

//    df->SetConditionExpr(0) ; // RD: no longer needed since VIPER 7018 fix.

    unsigned non_constant_if = 0 ;
    if ((!cond || !cond->IsConstant()) && IsStaticElab()) { // Non constant condition in static elaboration
        df->SetNonconstExpr() ; // Mark that data flow contains non constant expr (VIPER 3815)
        non_constant_if = 1 ;
    }
    if (!cond && IsRtlElab()) return ;

    // Issue 2215 : error out if condition is an 'event' (or a 'stable') value :
    if (cond && (cond->IsEvent() || cond->IsStable()) && IsRtlElab()) {
        _if_cond->Error("if-condition is an event, not an edge") ;
    }

    // Check if this condition could represent an implicit clock edge
    // That happens if there is ONE signal of ONE bit on the sensitivity
    // list and this signal is in the condition (or in inverted form).
    // For example : if (clk='1')  means really if (clk'event and clk='1')
    // if clk is the only one (on the sensitivity list)
    // VIPER 3909 : do not do implicit clock extraction if the condition is already a clock edge :
    if (cond && df->IsImplicitClock(cond) && !cond->IsEdge()) {
        // IMPORTANT : If there are 'else' statements, the user probably intended
        // combinational behavior here. Often, some constant is assigned in either if or else part.
        // In that case, we should NOT assume an implied clock.
        // Issue 1914 : also check for elsif's :
        if ((!_else_statements || _else_statements->Size()==0) && (!_elsif_list || _elsif_list->Size()==0)) {
            VhdlValue *clock = new VhdlEdge(cond->GetBit()) ;
            delete cond ;
            cond = clock ;
        }
    }

    unsigned i ;
    VhdlStatement *stmt ;
    VhdlElsif *elsif ;

    Map condition_to_dataflow(POINTER_HASH) ;

    VhdlDataFlow *sub_df ;
    VhdlDataFlow *else_flow = 0 ;
    unsigned execute_the_rest = 1 ;
    Array data_flows(2) ; // VIPER #7286 :
    if (cond && cond->IsConstant()) {
        if (cond->IsTrue()) {
            // For performance improvement, don't use sub_df's :
            // execute the if-statements, as if they are in present df
            FOREACH_ARRAY_ITEM(_if_statements, i, stmt) stmt->Elaborate(df,0) ;
            delete cond ;
            // done
            return ;
        }
        delete cond ; // 'if' part not executed
    } else {
        // Elaborate if-statements in new dataflow area
        sub_df = new VhdlDataFlow(df, (cond && cond->IsEdge()) ? cond->GetBit() : 0) ;
        if (cond && cond->IsEdge()) sub_df->SetClockEnable(cond->GetEnableNet()) ; // Viper #4958
        (void) sub_df->SetConditionIdValueMap(condition_id_value_map) ; // Viper #5553

        (void) condition_to_dataflow.Insert(cond ? cond->ToNonconstBit(): 0, sub_df, 0, 1/* force insert #5213*/) ;
        FOREACH_ARRAY_ITEM(_if_statements, i, stmt) stmt->Elaborate(sub_df,0) ;
        data_flows.InsertLast(sub_df) ; // VIPER #7286
    }

    VhdlExpression *elsif_cond ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif) {
        if (!execute_the_rest) break ;

        elsif_cond = elsif->Condition() ;
        if (!elsif_cond) continue ; // Can this happen ?
        // Viper #4233/4234 : Do not mask error in static_elab for conditions in non-constant dataflow
        // TODO: Please review the non-constant dataflow strategy. This looks very fragile.
        // Viper 6308 : SetConditionExpr before evaluating condition in static elab mode.
//        df->SetConditionExpr(1) ; // RD: no longer needed since VIPER 7018 fix.
        cond = elsif_cond->Evaluate(0,df,0) ;
//        df->SetConditionExpr(0) ; // RD: no longer needed since VIPER 7018 fix.
        if ((!cond || !cond->IsConstant()) && IsStaticElab()) { // Non constant in static elaboration
            // Do not mark data flow non-constant here. We will do that after processing its statements
            //df->SetNonconstExpr() ; // Mark that this data flow contains non constant expr
            non_constant_if = 1 ;
        }
        //if (!cond) break ;

        // Issue 2215 : error out if condition is an 'event' (or a 'stable') value :
        if (cond && (cond->IsEvent() || cond->IsStable())) {
            elsif_cond->Error("if-condition is an event, not an edge") ;
        }

        if (cond && cond->IsConstant()) {
            if (cond->IsTrue()) {
                if (condition_to_dataflow.Size()==0) {
                    // Only this elsif part will ever be executed.
                    // for performance, execute in present df and we are done
                    elsif->ElaborateElsif(df) ;
                    delete cond ;
                    return ;
                }
                // execute as the last (else) one
                sub_df = new VhdlDataFlow(df) ;
                else_flow = sub_df ; // Set as 'else' flow
                elsif->ElaborateElsif(sub_df) ;
                execute_the_rest = 0 ;
                // else_flow will be deleted below, so there is no memory leak.
            }
            delete cond ;
        } else {
            // Elaborate and continue
            // Elaborate elsif-statements in new dataflow area
            sub_df = new VhdlDataFlow(df,((cond) ? ((cond->IsEdge()) ? cond->GetBit() : 0): 0)) ;
            if (cond && cond->IsEdge()) sub_df->SetClockEnable(cond->GetEnableNet()) ; // Viper #4958
            (void) condition_to_dataflow.Insert(cond ? cond->ToNonconstBit(): 0, sub_df, 0, 1/* force insert #5213*/) ;
            elsif->ElaborateElsif(sub_df) ;
            data_flows.InsertLast(sub_df) ; // VIPER #7286
        }
    }

    // else clause. condition is implied TRUE
    if (execute_the_rest) {
        if (condition_to_dataflow.Size()==0) {
            // For performance : 'if' part was not executed, 'else'
            // part is the only area. So execute it in the present area
            // and we are done.
            FOREACH_ARRAY_ITEM(_else_statements, i, stmt) stmt->Elaborate(df,0) ;
            return ;
        }
        // Regular 'else' part
        sub_df = new VhdlDataFlow(df) ;
        else_flow = sub_df ; // Set the 'else' flow
        FOREACH_ARRAY_ITEM(_else_statements, i, stmt) stmt->Elaborate(sub_df,0) ;
        data_flows.InsertLast(sub_df) ; // VIPER #7286
    }

    // Now resolve all id values and got-areas into calling dataflow 'df'.
    if (IsRtlElab()) df->PopOutOfIf(&condition_to_dataflow, else_flow, this) ;

    if (IsStaticElab() && non_constant_if) df->InvalidateIdValues(&data_flows, this) ; // VIPER #7286
    // And clean up conditions and dataflow
    MapIter mi ;
    FOREACH_MAP_ITEM(&condition_to_dataflow, mi, &cond, &sub_df) {
        if (IsStaticElab() && sub_df && sub_df->IsNonconstExpr()) non_constant_if = 1 ;
        delete cond ;
        delete sub_df ;
    }
    if (IsStaticElab()) {
        if (else_flow && else_flow->IsNonconstExpr()) non_constant_if = 1 ;
        if (non_constant_if) df->SetNonconstExpr() ;
    }
    delete else_flow ;
}

void VhdlVariableAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!df || !_target || !_value) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if (df->IsDeadCode() || _target->IgnoreAssignment()) return ; // we are in dead code

    VhdlDataFlow *effective_df = df ;
        // Constraint of target only needed if expression is a aggregate (LRM 7.3.2.2) :
        VhdlConstraint *target_constraint = (_value->IsAggregate()) ? _target->EvaluateConstraintInternal(effective_df, 0) : 0 ;
        VhdlValue *value = _value->Evaluate(target_constraint,effective_df,0) ;

        // Buffer variable assignment ; linefile label from '_target'
        if (value && IsRtlElab() && RuntimeFlags::GetVar("vhdl_preserve_assignments")) {
            // Don't buffer constants, since that would destroy all constant prop don through variable assignments.
            value = value->PreserveAssignmentBuffer(0/*don't buffer constants*/, _target) ;
        }

        if (!value && IsStaticElab()) {
            effective_df->SetNonconstExpr() ; // Mark that this data flow contains non constant expression
            // VIPER #2858 : Check size mismatch between target and value from constraint,
            // it will not be checked in 'Assign' as value is null.
            if (!target_constraint) target_constraint = _target->EvaluateConstraintInternal(effective_df, 0) ;
            if (target_constraint && !target_constraint->IsUnconstrained()) { // Check if target is not unconstrained
                // VIPER #5011 : Do not check constraint for aggregate, it is already
                // validated in Evaluate
                //VhdlConstraint *value_constraint = (!_value->IsAggregate()) ? _value->EvaluateConstraint(effective_df): 0 ;
                VhdlConstraint *value_constraint = _value->EvaluateConstraintInternal(effective_df, target_constraint) ;
                if (value_constraint && !value_constraint->IsUnconstrained()) (void) target_constraint->CheckAgainst(value_constraint, _value, effective_df) ;
                delete value_constraint ;
            }
        }
        delete target_constraint ;
        // do assignment
        _target->Assign(value,effective_df,0) ;
}

void VhdlSignalAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!_target || !_waveform || _waveform->Size()==0) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if ((df && df->IsDeadCode()) || _target->IgnoreAssignment()) return ; // we are in dead code

    if (IsRtlElab() && (_waveform->Size() > 1)) {
        Warning("synthesis ignores all but the first waveform") ;
    }

    // Get just the last waveform element
    VhdlExpression *expr = (_waveform->Size()) ? (VhdlExpression*)_waveform->GetFirst() : 0 ;
    if (!expr) return ;

    VhdlDataFlow *effective_df = df ;

        // Constraint of target only needed if expression is a aggregate (LRM 7.3.2.2) :
        VhdlConstraint *target_constraint = (expr->IsAggregate()) ? _target->EvaluateConstraintInternal(effective_df, 0) : 0 ;

        // Evaluate the waveform element
        VhdlValue *value = expr->IsNull() ? 0 : expr->Evaluate(target_constraint,effective_df,0) ;

        if (!value && IsStaticElab()) {
            if (effective_df) effective_df->SetNonconstExpr() ; // Mark that this data flow contains non constant expression
            // VIPER #2858 : Check size mismatch between target and value from constraint,
            // it will not be checked in 'Assign' as value is null.
            if (!target_constraint) target_constraint = _target->EvaluateConstraintInternal(effective_df, 0) ;
            if (target_constraint && !target_constraint->IsUnconstrained()) { // Check if target is not unconstrained
                VhdlConstraint *value_constraint = 0 ;
                unsigned i ;
                VhdlExpression *ele ;
                FOREACH_ARRAY_ITEM(_waveform, i, ele) {
                    // VIPER #5011 : Do not check constraint for aggregate, it is already
                    // validated in Evaluate
                    //value_constraint = (ele && !ele->IsAggregate()) ? ele->EvaluateConstraintInternal(effective_df, 0) : 0 ;
                    value_constraint = (ele) ? ele->EvaluateConstraintInternal(effective_df, target_constraint) : 0 ;
                    if (value_constraint && !value_constraint->IsUnconstrained()) {
                        //unsigned is_nonconst_branch = 0 ;
                        //if (effective_df && effective_df->IsNonconstBranch()) is_nonconst_branch = 1 ;

                        // VIPER Issue #6561 : Do not error in CheckAgainst when df is non-constant
                        //if (!target_constraint->CheckAgainst(value_constraint, is_nonconst_branch ? 0 : ele)) {
                        // VIPER #7655 : 'CheckAgainst will produce warning for
                        // non-constant branch. So no need to do anything special
                        // for #6561
                        if (!target_constraint->CheckAgainst(value_constraint, ele, effective_df)) {
                            //if (ele && is_nonconst_branch) ele->Warning("expression has incompatible type") ;
                        }
                    }
                    delete value_constraint ;
                }
            }
        }

        delete target_constraint ; // It surved its purpose

        // Buffer signal assignment ; linefile label from '_target'
        if (value && IsRtlElab() && RuntimeFlags::GetVar("vhdl_preserve_assignments")) {
            value = value->PreserveAssignmentBuffer(1/*buffer constants too*/, _target) ;
        }

        // Do the assignment
        _target->Assign(value,effective_df,0) ;
}

void VhdlForceAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (IsRtlElab()) Error("VHDL 1076-2008 construct not yet supported") ;
}

void VhdlReleaseAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (IsRtlElab()) Error("VHDL 1076-2008 construct not yet supported") ;
}
void VhdlConditionalForceAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (IsRtlElab()) Error("VHDL 1076-2008 construct not yet supported") ;
}
void VhdlSelectedForceAssignment::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (IsRtlElab()) Error("VHDL 1076-2008 construct not yet supported") ;
}
void VhdlWaitStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // Flag the dataflow as 'waiting'
//    if (df) df->SetIsWaiting() ;

    if (IsStaticElab()) {
        unsigned i ;
        VhdlName *name ;
        FOREACH_ARRAY_ITEM(_sensitivity_clause, i, name) {
            // VIPER #4398 : In static elaboration mode only evaluate the
            // sensitivity list element just to produce error for illegal usage.
            VhdlValue *v = name->Evaluate(0, df, 0) ;
            delete v ;
        }
        return ; // No processing left for static elaboration
    }

    // General purpose checks :

    // If there is no dataflow, there is something wrong with this wait statement
    if (!df) return ;

    // Handle 'wait-until' processes ; get the clock from the (only) wait-until clause
    VhdlValue *wait_condition = (_condition_clause) ? _condition_clause->Evaluate(0,df,0) : 0 ;

    // Wait-until condition always serves as a clock. Even "wait until clk='1'" is a clock
    Net *clk = (wait_condition) ? wait_condition->GetBit() : 0 ;

    // Viper #4958: Also keep track of the clock enable net
    Net *clk_enable = (wait_condition) ? wait_condition->GetEnableNet() : 0 ;

    // Synthesis checks :
    if (wait_condition && wait_condition->IsEvent()) {
        Error("wait until event is not supported for synthesis") ;
    }

    // Wait statement without until clause is ignore it in the elaboration.
    if (!_condition_clause) {
        // VIPER 2304 : should accept sensitivity list, and set it.
        if (_sensitivity_clause) {
            unsigned i ;
            Set *sens_list = new Set(POINTER_HASH) ;

            // Put signals on the sensitivity list
            // VIPER 2459 : Do not need to create sensitivity list for static elaboration
            VhdlName *name ;
            FOREACH_ARRAY_ITEM(_sensitivity_clause, i, name) {
                name->AddToSensList(*sens_list) ;
            }

            // Set this sens list into the dataflow :
            df->SetSensitivityList(sens_list) ;
        } else {
            Error("wait statement without UNTIL clause not supported for synthesis") ;
        }
    }

    // Error out on multiple wait statements (if clock is already set)
    if (clk && df->GetClock()) {
        // FIX ME : clock could also be there because of a event condition. Different error ?
        // Don't support multiple waits per process
        Error("multiple wait statements in one process not supported for synthesis") ;
        // Viper #4958: Similar check not added for clock enable. This check should have included it
    }

    // Check if wait statement is in a subprogram
    VhdlScope *scope = df->GetEnclosingScope() ;
    if (scope && scope->GetSubprogram()) {
        Error("wait statements in subprograms are not supported for synthesis") ;
    } else if (df->Owner()) { // Contains owner, i.e. wait is inside branch
        // VIPER #4549 : Produce error if wait statement is within branch
        Error("wait statements in branch statements are not supported for synthesis") ;
    }

    delete wait_condition ;

    // Store clock of this wait statement in the dataflow
    if (clk) df->SetClock(clk) ;
    if (clk_enable) df->SetClockEnable(clk_enable) ; // Viper #4958
}

void VhdlReportStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if (IsStaticElab()) return ; // Do not process report statement for static elaboration

    // If ignore_assertion_statements flag is on, do not process this node :
    if (RuntimeFlags::GetVar("vhdl_ignore_assertion_statements")) return ;

    // Print an assertion if it always holds :
    unsigned print_report = df ? df->IsInInitial() : 1 ;
    VhdlDataFlow *owner_df = df ? df->Owner() : 0 ;
    print_report |= owner_df ? owner_df->IsInInitial() : 1 ;
    if (print_report) { // Viper #4751 : IsInInitial check added
        // We are in dataflow area or in a process :
        // Assertion always holds.
        // Print assertion in an info message :
        VhdlValue *message = (_report) ? _report->Evaluate(0,df,0) : 0 ;
        char *image = (message) ? message->StringImage() : Strings::save("Assertion violation") ;

        // Determine which Verific output function to use based on severity
        VhdlValue *severity = (_severity) ? _severity->Evaluate(0, df,0) : 0 ;
        verific_int64 level = (severity) ? severity->Position() : 0 /*default is a note (LRM 8.3) */;
        // Viper #4667 : Changed message and action taken on severity
        switch(level) {
        default :
        case 0 : /*note*/      if (image) Info("%s: %s", "Note", image) ; break ;
        case 1 : /*warning*/   if (image) Info("%s: %s", "Warning", image) ; break ;
        case 2 : /*error*/     if (image) Info("%s: %s", "Error", image) ; break ;
        case 3 : /*failure*/   if (image) Error("%s: %s", image, "exiting elaboration") ; break ;
        }

        Strings::free( image ) ;
        delete message ;
    }
}

void VhdlAssertionStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // If ignore_assertion_statements flag is on, do not process this node :
    if (RuntimeFlags::GetVar("vhdl_ignore_assertion_statements")) return ;

    // Viper 4371. Always execute condition and report expressions, to check consistency
    VhdlValue *cond = (_condition) ? _condition->Evaluate(0,df,0) : 0 ; // Viper 4371. To check consistency in condition
    VhdlValue *message = (_report) ? _report->Evaluate(0,df,0) : 0 ;  // Viper 4371. To check consistency in report

    // Print an assertion if it always holds :
    if (cond && cond->IsConstant() && !cond->IsTrue()) {
        // This local condition is always 'false', so assertion hits.
        // Issue 4354 : Static elab : issue the message if we are in a 'initial' value calculation (where everything is constant), unless we are accidentally in a non-constant branch
        // In Rtl elab, historically, we also issue this message if we are in a concurrent area (no dataflow) or an always executed sequential area.
        if (
            (df && df->IsInInitial() && !df->IsNonconstBranch()) ||
            (IsRtlElab() && (!df || !df->Owner()))) {
            // Assertion always holds.
            // Print assertion in an info message :
            char *image = (message) ? message->StringImage() : Strings::save("Assertion violation") ;

            // Determine which Verific output function to use based on severity
            VhdlValue *severity = (_severity) ? _severity->Evaluate(0, df,0) : 0 ;
            verific_int64 level = (severity) ? severity->Position() : 2 /*default is an error*/;
            delete severity ;
            // Viper #4667 : Changed message and action taken on severity
            switch (level) {
            default :
            case 0 : /*note*/      if (image) Info("%s: %s", "Note", image) ; break ;
            case 1 : /*warning*/   if (image) Info("%s: %s", "Warning", image) ; break ;
            case 2 : /*error*/     if (image) Info("%s: %s", "Error", image) ; break ;
            case 3 : /*failure*/   if (image) Error("%s: %s", image, "exiting elaboration") ; break ;
            }

            Strings::free( image );
        }
    }

    // cleanup
    delete message ;
    delete cond ;
}

void VhdlProcessStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    if (_label) PushNamedPath(_label->OrigName()) ;
    PushAttributeSpecificPath(_label ? _label->Name(): 0) ;

    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ;
    unsigned i ;
    Set *sens_list = 0 ;
    unsigned sens_list_all = 0 ; // Viper #7413: Mark for process(all)

    // Put signals on the sensitivity list
    // VIPER 2459 : Do not need to create sensitivity list for static elaboration
    if (_sensitivity_list) { // && !IsStaticElab()) {
        // create a new sensitivity list, to check in RTL synthesis if identifiers are mentioned on the sens-list.
        if (!IsStaticElab()) sens_list = new Set(POINTER_HASH,_sensitivity_list->Size()) ;

        // Fill it with the iddefs referred in the _sensitivity_list :
        VhdlName *name ;
        FOREACH_ARRAY_ITEM(_sensitivity_list, i, name) {
            if (IsStaticElab()) {
                // VIPER #4398 : In static elaboration mode only evaluate the
                // sensitivity list element just to error for illegal usage.
                VhdlValue *v = name->Evaluate(0, df, 0) ;
                delete v ;
                continue ;
            }
            if (sens_list) name->AddToSensList(*sens_list) ;
            if (name->IsAll()) sens_list_all = 1 ;
        }
    }

    // Create dataflow
    // If the process has no sensitivity list there will be a wait statement later.
    // RD: 5/2007: do not pass-in a sensitivity list for a process without sensitivity list.
    // df = new VhdlDataFlow(0, /*clocked*/(_sensitivity_list)?0:1, _local_scope, &sens_list) ;
    df = new VhdlDataFlow(0, /*not clocked*/0, _local_scope, sens_list) ;

    // VIPER #3367: We are going to process delcarative region, set this flag:
    df->SetInInitial() ;
    df->SetInSubprogramOrProcess() ;

    // Viper #7413: Mark for process(all)
    if (sens_list_all) df->SetSensitivityListAll() ;

    // Elaborate the declarations :
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        decl->Elaborate(df) ;
    }
    //_present_scope = old_scope ;

    df->UpdateInitialValues() ;

    // VIPER #3367: Done processing delcarative region, reset the flag:
    df->SetNotInInitial() ;

    // Elaborate the process statements.
    // How do we know up-front if there is a wait statement ?
    // VHDL defines : Without a sensitivity list, there must be a wait statement.
    VhdlStatement *stmt ;
    if (!_sensitivity_list) {
        // without a sensitivity list, there must be a wait statement.
        // Issue 2097 : process with a wait statement should elaborate starting from the wait statement,
        //    and then loop back to start to do the statements before the wait statement.
        VhdlStatement *first_wait = 0 ;
        // Find the wait statement :
        FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
            if (!stmt) continue ;
            if (!first_wait && stmt->GetClassId()==ID_VHDLWAITSTATEMENT) { // FIX ME : IsWaitStatement() would be better..
                first_wait = stmt ;
            }
            if (!first_wait) continue ; // don't elaborate before we have the first wait statement.

            // Elaborate this statement (first one will be the wait statement) :
            stmt->Elaborate(df,0) ;
        }
        // Now do it again, for statements preceding the first wait statement.
        FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
            if (!stmt) continue ;
            // if (!first_wait) break ; // incorrect vhdl.
            if (first_wait==stmt) break ; // we reached the wait statement. Did the rest already. We are done.
            // Elaborate this statement
            stmt->Elaborate(df,0) ;
        }
        // NOTE : if we can start elaboration with a sub-dataflow starting from each wait statement in the process,
        // and collect sub-flows accordingly, we could support multiple-wait (implicit state machine) style.
        // This is not trivial though.
    } else {
        // Process with a sensitivity list. Regular execution, starting at the first statement :
        FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
            if (!stmt) continue ;
            stmt->Elaborate(df,0) ;
        }
    }
    if (df->IsSensListIncomplete()) Warning("incomplete sensitivity list specified, assuming completeness") ;
    _present_scope = old_scope ;

#if 0 // tests are now moved to analyser (since VIPER 4518).
    // wait-statement versus sensitivity list check
    // VIPER #3538 : Do this checks for static elaboration also. Static elaboration now visits all statements.
    // FIX ME : These two messages are VHDL semantics (not RTL specific), and could/should be done in analyzer.

    // In essence, we should be able to run this in static elab too,
    // but static elab bails out of sequential flows very quickly, and thus we cannot trust 'IsWaiting'
    // to have been processed correctly.
    if (df->IsWaiting() && _sensitivity_list) {
        Error("process cannot have both a wait statement and a sensitivity list") ;
    } else if (!df->IsWaiting() && !_sensitivity_list) {
        // VIPER 3710 : turn this into a warning, since that is what simulators do
        Warning("possible infinite loop; process does not have a wait statement") ;
    }
#endif

    // VIPER #2858 : Return from the process statement after elaborating its statements
    if (IsStaticElab()) {
        delete df ;
        if (_label) PopNamedPath() ;
        PopAttributeSpecificPath() ;
        return ;
    }

    delete df ; // Thats only a shell now
}

void VhdlBlockStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config)
{
    if (df && df->IsDeadCode()) return ; // we are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if (IsStaticElab() && _is_static_elaborated) return ; // It is already elaborated, return

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #5624 : Create pseudo tree node for block statement, as block can contain
    // instantiations:
    VeriStaticElaborator *elab_obj = (IsStaticElab()) ? veri_file::GetStaticElabObj(): 0 ;
    VeriPseudoTreeNode *saved_curr_node = elab_obj ? elab_obj->GetCurrentNode(): 0 ;
    if (elab_obj) elab_obj->AddVhdlBlockNode(saved_curr_node, _local_scope) ;
#endif

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;
    // Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _local_scope ? _local_scope : old_scope ;

    if (_label) PushNamedPath(_label->OrigName()) ;

    if (_label) PushAttributeSpecificPath(_label->Name()) ;

    if (_guard) {
        // The guard signal is in the tree here :
        if (!_guard_id) {
            if (_label) {
                PopNamedPath() ;
                PopAttributeSpecificPath() ;
            }
            return ; // Something wrong with analysis
        }

        // Evaluate the guard expression
        VhdlValue *guard_val = _guard->Evaluate(0,0,0) ;
        if (!guard_val) {
            if (_label) {
                PopNamedPath() ;
                PopAttributeSpecificPath() ;
            }
            return ; // Something wrong with the guard expression.
        }

        // Set the constraint on guard id (the constraint of its type (boolean))
        VhdlConstraint *constraint = (_guard_id->Type()) ? _guard_id->Type()->Constraint() : 0 ;
        _guard_id->SetConstraint(constraint ? constraint->Copy() : 0) ;

        // Set the value of the guard expression onto the guard id
        _guard_id->SetValue(guard_val) ;
    }

    // Elaborate generics and ports
    if (_generics) _generics->Elaborate() ;
    if (_ports) _ports->Elaborate() ;

    // VIPER #3103 : Transform port map aspect of block
    if (_ports && IsStaticElab()) _ports->StaticElaborate() ;
    if (_generics && IsStaticElab()) _generics->StaticElaborate() ;

    // Configure the contents of the block, with a blockconfiguration that matches the label.
    VhdlBlockConfiguration *subblock_config = (block_config) ? block_config->ExtractSubBlock(_label, 0, 0) : 0 ;
    if (subblock_config) subblock_config->Elaborate() ; // VIPER 2012 execute block confic for this block level

    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow *decl_flow = (df) ? df : new VhdlDataFlow(0, 0, _local_scope) ;
    decl_flow->SetInInitial() ;

    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->Elaborate(decl_flow) ;
    }

    // VIPER #3367: Done processing delcarative region, reset the flag:
    decl_flow->SetNotInInitial() ;
    if (!df) delete decl_flow ;

    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements, i, stmt) {
        if (!stmt) continue ;
        stmt->Elaborate(df,subblock_config) ;
    }
    delete subblock_config ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    if (elab_obj) elab_obj->SetCurrentNode(saved_curr_node) ; // Pop current node setting
#endif
    _present_scope = old_scope ;
    if (_label) PopNamedPath() ;

    if (_label) PopAttributeSpecificPath() ;
}

void VhdlGenerateStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config)
{
    if (df && df->IsDeadCode()) return ; // We are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    //if (_label) PushNamedPath(_label->OrigName()) ;

    if (IsStaticElab()) {
        // Unroll loops, select proper statement form if-generate
        StaticElaborate(block_config, df) ;
    } else if (IsRtlElab()) {
    }

    //if (_label) PopNamedPath() ;
}

// Following API is factored out from existing code and is then enhanced to
// support mixed language designs having component instantiation of verilog
// module with primary/incremental binding
// VIPER# 3554 was the main driving force for this change
Array *VhdlComponentInstantiationStatement::CreateActualGenericBinding(VhdlPrimaryUnit const *prim_unit, VeriModule const *verilog_module, const Array *primary_generic_binding, const Array *incremental_generic_binding) const
{
    // RD: Adjust this routine later to work without 'prim_unit' and 'verilog_module' :
    // Just looking at the formal names and formal positions should be enough to create a combined binding.
    (void) verilog_module ;

    // Find actual generic binding for the primary unit :
    Array *generic_binding = 0 ;
    if (primary_generic_binding && incremental_generic_binding) {
        // true case of incremental binding. Need to merge both association lists into one.
        // Generic clause : LRM ...: generics in 'incremental' binding override generics in primary binding.

        // start with incremental binding (since that always wins : we won't have to delete items from the list.
        generic_binding = new Array(*incremental_generic_binding) ;
        // Add primary binding associations. Do NOT override existing items.
        // First make a set of already associated formals :
        Set formals(STRING_HASH,primary_generic_binding->Size()+incremental_generic_binding->Size()) ;
        unsigned i ;
        VhdlDiscreteRange *assoc, *actual_part ;
        VhdlIdDef *id ;

        FOREACH_ARRAY_ITEM(incremental_generic_binding,i,assoc) {
            if (!assoc || assoc->IsOpen()) continue ;

            const char *generic_name = 0 ;
            if (assoc->IsAssoc() ) {
                actual_part = assoc->ActualPart() ;
                if (!actual_part || actual_part->IsOpen()) continue ; // named assoc
                // Find the formal :
                id = assoc->FindFormal() ; // named
                generic_name = id ? id->Name() : 0 ;
            } else {
                if (prim_unit) {
                    id = prim_unit->Id()->GetGenericAt(i) ; // positional
                    generic_name = id ? id->Name() : 0 ;
                } else if (verilog_module) { // verilog_module is present
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
                    VeriIdDef *veri_id = verilog_module->GetParamAt(i) ; // positional
                    generic_name = veri_id ? veri_id->Name() : 0 ;
#endif
                }
            }
            if (!generic_name) continue ; // ??
            (void) formals.Insert(generic_name) ; // insert in the set
        }
        // Now run over the primary binding, and see which assocations have to be added :
        FOREACH_ARRAY_ITEM(primary_generic_binding, i, assoc) {
            if (!assoc || assoc->IsOpen()) continue ;

            const char *generic_name = 0 ;
            // Find the formal.
            if (assoc->IsAssoc() ) {
                actual_part = assoc->ActualPart() ;
                if (!actual_part || actual_part->IsOpen()) continue ; // named assoc
                // Find the formal :
                id = assoc->FindFormal() ;
                generic_name = id ? id->Name() : 0 ;
            } else {
                if (prim_unit) {
                    id = prim_unit->Id()->GetGenericAt(i) ; // positional
                    generic_name = id ? id->Name() : 0 ;
                } else if (verilog_module) { // verilog_module is present
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
                    VeriIdDef *veri_id = verilog_module->GetParamAt(i) ; // positional
                    generic_name = veri_id ? veri_id->Name() : 0 ;
#endif
                }
            }
            if (!generic_name) continue ; // ???
            // Check if it is already in the incremental binding list :
            if (formals.GetItem(generic_name)) continue ; // yep. incremental binding already has this covered.
            // Add this assoc to the 'generic_binding'.
            VhdlDiscreteRange *exist_assoc = 0 ;
            // Viper 4177 the size of incremental_binding and primary_binding can be different
            if (i < generic_binding->Size()) exist_assoc = (VhdlDiscreteRange*)generic_binding->At(i) ;
            if (!exist_assoc || exist_assoc->IsOpen()) {
                // simply put in place here (override the existing 'open' one) :
                generic_binding->Insert(i,assoc) ;
            } else if (assoc->IsAssoc()) {
                // New assoc is named. Existing one can be positional, so add this at end of list.
                generic_binding->InsertLast(assoc) ;
            } else {
                // New assoc is positional. Existing one will be named.
                // Keep new assoc at its position, push the rest of the array forward
                generic_binding->InsertBefore(i,assoc) ;
            }
        }
    } else if (primary_generic_binding) {
        // there is only a primary generic binding
        generic_binding = new Array(*primary_generic_binding) ;
    } else if (incremental_generic_binding) {
        // there is only a incremental generic binding
        generic_binding = new Array(*incremental_generic_binding) ;
    }
    return generic_binding ;
}

Array *VhdlComponentInstantiationStatement::CreateActualComponentGenericBinding(const Array *generic_map_aspect, const Array *additional_generic_binding) const
{
    // (VIPER #2441, 2457, 2584, 2590, 3653, 3269)
    // Normally, the component generics are associated with this instantiation _generic_map_aspect.
    // LRM does not show any exception to that rule.
    // However, ModelSim seems to associate actuals in primary/incremental bindings with the component.
    //
    // Mimic that here, by associating component generics with entity bindings :
    //
    // Additionally, VIPER 3653 and VIPER 3269 require that we set actuals of 'generic_binding' onto the component generic binding
    // on an individual generic-by-generic basis (not an entire list at once).
    // That became a bit complex to handle in-line, so here we go with a separate routine.
    // Only fully-associated generic maps, where each generic is assocated with a constant value, not a IdRef, will be used.
    //
    // What we do essentially is to take the normal 'generic_map_aspect' of the component instantiation,
    // and add associations from the entity primary_binding/incremental_binding, but only associations that set an entity generic to a fixed value (locally static expression),
    // and that have not already been set in the generic_map_aspect.
    // This way, the entity-component port associations in the primary_binding/incremental_binding will NOT accidentally end up on the component generic map aspect,
    // but new constant generic values from the primary_binding/incremental_binding WILL make it to the component generics.
    // This seems to be how ModelSim works.
    //
    // FIX ME : there is a fair amount of gambing involved, with one specifically 'weak' spot :
    // The primary_binding/incremental_binding list has entity ports as formals, while the generic_map_aspect has component ports as formals.
    // There can be serious positional/name confusion if the component and entity are not 'alike'.
    // We may be lucky here, since IF entity generics have a static value actual, then it is apparently not linked to a different-named component generic.

    // FIX ME : ModelSim does not exactly work like this. They seem to skip the component binding altogether in some cases (not quite clear which cases).
    // They skip the component altogether and run as if there is a 'direct entity instantiation' done.
    // We are not sure what these 'situations' are though, but they depend on the incremental/primary binding map as well as the component binding map (and maybe on the initial values of the component).

    // Start with the normal 'generic_map_aspect' :
    Array *generic_binding = (generic_map_aspect) ? new Array(*generic_map_aspect) : 0 ;

    if (additional_generic_binding) {
        // Have to add (or create) additional binding :
        if (!generic_binding) generic_binding = new Array(additional_generic_binding->Size()) ;

        // Now iterate over this primary_binding/incremental_binding association list,
        // and select the associations that set a generic to a constant.
        // Then add these to the resulting binding list for the component :
        unsigned i ;
        VhdlDiscreteRange *assoc, *actual_part ;
        FOREACH_ARRAY_ITEM(additional_generic_binding, i, assoc) {
            if (!assoc || assoc->IsOpen()) continue ;

            // Find the actual expression :
            actual_part = assoc->ActualPart() ;
            if (!actual_part || actual_part->IsOpen()) continue ; // open assoc : ignore

            // Now the key to (VIPER #2441, 2457, 2584, 2590, 3653, 3269) :
            // Check if the actual is a constant expression (not a component port)
            if (!actual_part->IsLocallyStatic()) {
                // if not locally static, then we cannot promote the association from the binding to the component's generic_map_aspect.
                continue ;
            }

            // Do NOT rule out formals that are already set in the generic_map_aspect. Simply override these (with the added actual)

            // Here, we have a binding with a constant expression.
            // Insert it into the generic binding array.
            if (assoc->IsAssoc()) {
                // VIPER 5079 : named bindings : We need to check if there not already a named binding to the same formal.
                // Also, only worry about full name formals. Partial formals should really simply be added.
                VhdlIdDef *assoc_formal = assoc->FindFullFormal() ;
                if (assoc_formal) {
                    // Check if this formal (by name) already occurred in the component generic binding :
                    // See FIX ME notes above. This is sort of a 'hack' and not the way that ModelSim works.
                    unsigned j ;
                    VhdlDiscreteRange *comp_assoc ;
                    VhdlIdDef *comp_assoc_formal ;
                    unsigned inserted = 0 ;
                    FOREACH_ARRAY_ITEM(generic_map_aspect, j, comp_assoc) {
                        if (!comp_assoc || comp_assoc->IsOpen()) continue ;
                        if (!comp_assoc->IsAssoc()) continue ; // it's a positional assoc
                        comp_assoc_formal = comp_assoc->FindFullFormal() ;
                        if (!comp_assoc_formal) continue ;
                        // Here, check if the names match :
                        if (Strings::compare_nocase(assoc_formal->Name(),comp_assoc_formal->Name())) {
                            // They match. Overwrite this one :
                            generic_binding->Insert(j,assoc) ;
                            inserted = 1 ;
                            break ; // we are done
                        }
                    }
                    if (!inserted) {
                        // insert at the end
                        generic_binding->InsertLast(assoc) ;
                    }
                } else {
                    // named binding. Insert at the end.
                    generic_binding->InsertLast(assoc) ;
                }
            } else {
                // positional binding. Tricky. Have to insert at this position, but there might not be one.
                // Fill up with positional NULL ('open') bindings :
                while (generic_binding->Size()<=i) generic_binding->InsertLast(0) ;
                // Insert at this position
                generic_binding->Insert(i,assoc) ;
            }
        }
    }
    return generic_binding ;
}

// Following API is factored out from existing code and is then enhanced to
// support mixed language designs having component instantiation of verilog
// module with primary/incremental binding
// VIPER# 3554 was the main driving force for this change
Array *VhdlComponentInstantiationStatement::CreateActualPortBinding(VhdlPrimaryUnit const *prim_unit, VeriModule const *verilog_module, const Array *primary_port_binding, const Array *incremental_port_binding) const
{
    // RD: Adjust this routine later to work without 'prim_unit' and 'verilog_module' :
    // Just looking at the formal names and formal positions should be enough to create a combined binding.
    (void) verilog_module ;

    // Find actual port binding for the primary unit :
    Array *port_binding = 0 ;
    if (primary_port_binding && incremental_port_binding) {
        // true case of incremental binding. Need to merge both association lists into one.
        // Port clause : LRM ...: ports in 'incremental' binding cannot be in primary binding already.

        // start with incremental binding (since that always wins : we won't have to delete items from the list.
        port_binding = new Array(*incremental_port_binding) ;

        // Add primary binding associations. Do NOT override existing items.
        // First make a set of already associated formals :
        Set formals(POINTER_HASH,primary_port_binding->Size()+incremental_port_binding->Size()) ;
        unsigned i ;
        VhdlDiscreteRange *assoc, *actual_part ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(incremental_port_binding,i,assoc) {
            if (!assoc || assoc->IsOpen()) continue ;
            id = 0 ;
            if (assoc->IsAssoc() ) {
                actual_part = assoc->ActualPart() ;
                if (!actual_part || actual_part->IsOpen()) continue ; // named assoc
                // Find the formal :
                id = assoc->FindFormal() ; // named
            } else if (prim_unit) {
                id = prim_unit->Id()->GetPortAt(i) ; // positional
            }
            if (!id) continue ; // ??
            (void) formals.Insert(id) ; // insert in the set.
        }
        // Now run over the primary binding, and see which assocations have to be added :
        FOREACH_ARRAY_ITEM(primary_port_binding, i, assoc) {
            if (!assoc || assoc->IsOpen()) continue ;
            // Find the formal.
            id = 0 ;
            if (assoc->IsAssoc() ) {
                actual_part = assoc->ActualPart() ;
                if (!actual_part || actual_part->IsOpen()) continue ; // named assoc
                // Find the formal :
                id = assoc->FindFormal() ;
            } else if (prim_unit) {
                id = prim_unit->Id()->GetPortAt(i) ;
            }
            if (!id) continue ; // ???
            // Check if it is already in the incremental binding list :
            if (formals.GetItem(id)) {
                // yep. incremental binding already has this covered.
                // LRM : error out for ports. Report error from the incremental binding.
                if (incremental_port_binding) {
                    // don't know exactly which port assoc in the incremental port binding was responsible.
                    // Take the last one :
                    assoc = (VhdlDiscreteRange*)incremental_port_binding->GetLast() ;
                    if (assoc) assoc->Error("port %s is already associated in the primary binding",id->Name()) ;
                }
                continue ; // don't insert. Incremental binding wins.
            }
            // Add this assoc to the 'port_binding'.
            VhdlDiscreteRange *exist_assoc = 0 ;
            // Viper #4588: the size of incremental_binding and primary_binding can be different
            if (i < port_binding->Size()) exist_assoc = (VhdlDiscreteRange*)port_binding->At(i) ;
            if (!exist_assoc || exist_assoc->IsOpen()) {
                // simply put in place here (override the existing 'open' one) :
                // VIPER #7564: Fill up with positional NULL ('open') bindings :
                while (port_binding->Size()<=i) port_binding->InsertLast(0) ;
                port_binding->Insert(i,assoc) ;
            } else if (assoc->IsAssoc()) {
                // New assoc is named. Existing one can be positional, so add this at end of list.
                port_binding->InsertLast(assoc) ;
            } else {
                // New assoc is positional. Existing one will be named.
                // Keep new assoc at its position, push the rest of the array forward
                port_binding->InsertBefore(i,assoc) ;
            }
        }
    } else if (primary_port_binding) {
        port_binding = new Array(*primary_port_binding) ; // there is only a primary port binding
    } else if (incremental_port_binding) {
        port_binding = new Array(*incremental_port_binding) ; // there is only a incremental port binding
    }
    return port_binding ;
}

void VhdlComponentInstantiationStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config)
{
    if (df && df->IsDeadCode()) return ; // We are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if (!_unit) return ;
    // _unit is the instantiated unit. Could be a component, entity or configuration.
    // However, due to a syntax similarity with procedure calls, it can also be a
    // procedure call. Yacc cannot distinguish in time. So check this here :

    if (_unit->IsSubprogram()) {
        // As of the VIPER #2791 fix (1/17/07), this check is only needed when doing
        // elaboration after restoring an "old" VHDL vdb, in which labeled procedure calls with
        // no arguments are still represented as an VhdlComponentInstantiationStatement.

        // Its a procedure call. Probably one with a label so it ended up here.
        // Call the instantiated unit (an indexed name or plain identifier) : it represents the real procedure call :
        VERIFIC_ASSERT(_instantiated_unit) ;
        (void) _instantiated_unit->Evaluate(0,df,0) ;
        // We are sure that there is no return value ; this is a procedure, right ?
        return ;
    }

    if (!_label) return ; // yacc guarantees this, but we don't want to assert.

    // Here, we are among components, entities and configurations

    // Find binding info

    // Get primary binding from the label (points to the binding in the configuration specification)
    VhdlBindingIndication *primary_binding = _label->GetPrimaryBinding() ;

    // Incremental binding/sub-block config :
    VhdlComponentConfiguration *comp_config = (block_config) ? block_config->ExtractComponentConfig(_label) : 0 ;

    // Reset the binding information on label in static elaboration mode, as static
    // elaboration will set specified primary unit id/uniquified component id in this
    // instantiation and from that _unit one can find binding unit.
    if (IsStaticElab()) _label->SetBinding(0) ;

    // Get the incremental binding (from the matching component configuration)
    VhdlBindingIndication *incremental_binding = (comp_config) ? comp_config->GetBinding() : 0 ;

    // Adjust the block_config to the one that we need inside the instantiated entity.
    // Primary binding (from configuration specification) never has a block config, so
    // get the block config from the incremental binding (component configuration).
    block_config = (comp_config) ? comp_config->GetBlockConfiguration() : 0 ;

    // Find the primary unit and bindings to instantiate :
    VhdlPrimaryUnit *prim_unit = 0 ;
    Array *generic_binding = 0 ;
    Array *port_binding = 0 ;
    Array *allocated_generic_binding = 0 ; // pointers to generic binding if allocated through CreateActualGenericBinding
    Array *allocated_port_binding = 0 ; // pointers to port binding if allocated (happens for incremental binding if we have two port binding arrays)

    VhdlComponentDecl *component = (_unit->IsComponent()) ? _unit->GetComponentDecl() : 0 ;
    if (component) {
        // Find primary/secondary unit from binding or from default :
        // I think incremental binding overrules primary binding.
        if (incremental_binding) {
            // Get the component binding from the incremental binding
            VhdlIdDef *unit = incremental_binding->EntityAspect() ;
            prim_unit = (unit) ? unit->GetPrimaryUnit() : 0 ;
        }
        if (primary_binding) {
            // Get the component binding from the primary binding
            VhdlIdDef *unit = primary_binding->EntityAspect() ;
            VhdlPrimaryUnit *prim_unit1 = (unit) ? unit->GetPrimaryUnit() : 0 ;

            // Check if both are present
            if (prim_unit && prim_unit1 && prim_unit != prim_unit1) {
                char *tmp_path_name = PathName(_label->OrigName()) ; // save return so we don't leak it
                primary_binding->Error("component instantiation %s is already bound with primary unit %s, should not be bound again with %s", tmp_path_name, prim_unit1->Name(), prim_unit->Name()) ; // VIPER 4706
                Strings::free(tmp_path_name) ;
            }
            if (!prim_unit) prim_unit = prim_unit1 ;
        }

        // Viper #3585 : If binding indication present, it is not default binding.
        // Default binding upfront may cause incorrect binding, should the same-named-entity exist in a mixed language design
        unsigned has_entity_aspect = 0 ;
        if (primary_binding) {
            VhdlName *entity_aspect = primary_binding->GetEntityAspectNameNode() ;
            has_entity_aspect = (entity_aspect && !entity_aspect->IsOpen()) ;
        }
        if (!has_entity_aspect && incremental_binding) {
            VhdlName *entity_aspect = incremental_binding->GetEntityAspectNameNode() ;
            has_entity_aspect = (entity_aspect && !entity_aspect->IsOpen()) ;
        }

        if (!prim_unit && !has_entity_aspect) {
            // Issue 2172 : binding rules for default binding.
            // LRM 5.2.2. Find entity by the name of the component.
            // We should first look for 'visible entity declaration', skipping  component with that name
            // This implies that a use clause can make the entity visible,
            // even if there is a local component with that name.
            //
            // It is a bit unclear from where we should start searching :
            // "...at the point of the missing explicit binding indication". But where is that?.
            // Also the process of skipping a component with that name is not clear.
            //
            // LRM tells that this default choosing should be done at analysis time,
            // but ModelSim does it at elaboration time (no error at analysis time shows up).
            // Doing the search at elaboration time makes more sense, otherwise default
            // binding would require declare before use.
            //
            // That's probably why ModelSim does not do this at all.
            // They look directly in libraries : First look in work library.
            // Then look in libraries by name of the library clauses mentioned in this scope...
            // The first library with a correctly named entity is chosen.
            //
            // First comply with the 'direct visiblility' rule from LRM :
            // search for directly visible entities, from the "place of the missing binding" (LRM 5.2.2).
            // We will assume that they mean the current scope (where this instantiation happens).
            // Note : this is an anomaly : we should normally not look in the scope during elaboration.

            // LRM 5.2.2 : Find visible entity declarations with default binding rules :

            VhdlIdDef *unit_id = (_present_scope) ? _present_scope->FindDefaultBinding(_unit->Name(),_unit) : 0 ; // Don't use OrigName here, since we will look into the hash tables.

            // Should be an entity.
            if (unit_id) {
                prim_unit = unit_id->GetPrimaryUnit() ;
            }

            // Check that we have a entity (default binding requires that).
            if (prim_unit && prim_unit->Id() && !prim_unit->Id()->IsEntity()) {
                if (_instantiated_unit) _instantiated_unit->Error("primary unit of binding component %s is not an entity", _unit->Name()) ;
                prim_unit = 0 ; // remain unbound.
            }
        }

        // Create the actual generic and port bindings for the primary unit

        // Get primary and incremental generic bindings into the primary unit
        Array *primary_generic_binding = (primary_binding) ? primary_binding->GetGenericBinding() : 0 ;
        Array *incremental_generic_binding = (incremental_binding) ? incremental_binding->GetGenericBinding() : 0 ;
        // Create a merged array for these two bindings :
        if (primary_generic_binding && incremental_generic_binding) {
            generic_binding = allocated_generic_binding = CreateActualGenericBinding(prim_unit, 0 /* no verilog module */, primary_generic_binding, incremental_generic_binding) ;
        } else if (primary_generic_binding) {
            generic_binding = primary_generic_binding ; // there is only a primary generic binding
        } else if (incremental_generic_binding) {
            generic_binding = incremental_generic_binding ; // there is only a incremental generic binding
        }

        // Get primary and incremental port bindings into the primary unit
        Array *primary_port_binding = (primary_binding) ? primary_binding->GetPortBinding() : 0 ;
        Array *incremental_port_binding = (incremental_binding) ? incremental_binding->GetPortBinding() : 0 ;
        // Create a merged array for these two bindings :
        if (primary_port_binding && incremental_port_binding) {
            port_binding = allocated_port_binding = CreateActualPortBinding(prim_unit, 0 /* no verilog module */, primary_port_binding, incremental_port_binding) ;
        } else if (primary_port_binding) {
            port_binding = primary_port_binding ; // there is only a primary port binding
        } else if (incremental_port_binding) {
            port_binding = incremental_port_binding ; // there is only a incremental port binding
        }

    } else {
        // Direct entity or configuration instantiation.
        prim_unit = _unit->GetPrimaryUnit() ;

        // Direct entity/config instantiation ; direct binding actuals to unit
        generic_binding = _generic_map_aspect ;
        port_binding = _port_map_aspect ;
    }

    // Get the secondary unit (name) :
    const char *arch_name = 0 ;

    // If binding is there, _instantiated_unit must be a component (we check that when we set the binding)
    // so that will not have an architecture name (we check that in the constructor).
    // I think incremental binding has HIGHER preference than primary binding. So :

    // first incremental binding :
    if (incremental_binding) arch_name = incremental_binding->ArchitectureNameAspect() ;
    // then primary binding :
    if (!arch_name && primary_binding) arch_name = primary_binding->ArchitectureNameAspect() ;
    // then instantiation (for direct entity instantiations) :
    if (!arch_name && _instantiated_unit) arch_name = _instantiated_unit->ArchitectureNameAspect() ;

    // Arch name can be 0, in which can we will choose the 'default binding'
    // We will choose last analysed architecture further down.

    // If architecture name is there, check if it exists on the primary unit.
    // It should exist, this is elaboration time.
    VhdlDesignUnit *ps_unit = prim_unit ; // prim or sec unit
    if (prim_unit && prim_unit->IsVerilogModule() == 0) {
        VhdlDesignUnit *secondary = prim_unit->GetSecondaryUnit(arch_name) ;
        ps_unit = secondary ? secondary : prim_unit ;

        if (arch_name && !secondary) {
            // It has to be an entity, otherwise the binding would not allow the arch name.
            Error("architecture %s not found in entity %s", arch_name, prim_unit->Name()) ;
        }
    }

    // VIPER 1863 & 1472
    // Rtl elab : Push primary unit id in structural stack and create value and constraint
    // stack to store value and constraint if entered unit id is instantiated recursively
    if (IsRtlElab() && !VhdlNode::PushStructuralStack((ps_unit ? ps_unit->Id() : 0), _instantiated_unit)) {
        delete allocated_generic_binding ; // array allocated only. Not the elements.
        delete allocated_port_binding ; // array allocated only. Not the elements.
        return ;
    }

    if (component) {
        // (VIPER #2441, 2457, 2584, 2590, 3653, 3269)
        // Normally, the component generics are associated with this instantiation _generic_map_aspect.
        // LRM does not show any exception to that rule.
        // However, ModelSim seems to associate actuals in primary/incremental bindings with the component.
        Array *generics = 0 ;
#if 1 // for ModelSim compliance only :
        if (generic_binding) {
            generics = CreateActualComponentGenericBinding(_generic_map_aspect, generic_binding) ;
        }
#endif
        if (generics && prim_unit) { // Second condition added for Viper #4083
            // Associate (initialize) the generics of the component.
            component->AssociateGenerics(generics) ;
        } else {
            // Associate (initialize) the generics of the component
            component->AssociateGenerics(_generic_map_aspect) ;
        }

        delete generics ; // Moved down for Viper #4083

        // Associate (initialize) the ports of the component
        component->AssociatePorts(0, _port_map_aspect) ;

        // VIPER 1863 & 1472
        // Switch value/constraints between stack and iddefs so that component ids
        // get current value/constraint
        // VIPER #4288 : Switch value/constraint also when primary unit is not present:
        if (VhdlNode::GetValueMap()) VhdlNode::SwitchValueConstraintOfScope(component->LocalScope()) ;
    }

    unsigned is_blackbox = 1 ; // Flag to indicate whether this is blockbox instantiation
    // Elaborate the primary unit if we have it.
    // Viper 4577: Now the prim_unit of a Vhdl entity or a Verilog module are treated identically
    if (prim_unit) {
        is_blackbox = 0 ;
        // prim_unit can be a Vhdl Entity or a Verilog mudule converted to entity
        // Do generic/port binding here, to support unconstrained ports.
        VhdlIdDef *component_id = (_unit->IsComponent()) ? _unit : 0 ; // for default binding
        //if (_generic_map_aspect && prim_unit->IsVerilogModule()) prim_unit->AssociateGenerics(_generic_map_aspect, 0) ;

        if (IsStaticElab()) {
            // Uniquify the instantiated entity, converts 'this' to
            // entity instantiation.
            StaticElaborate(generic_binding, port_binding, block_config, component_id, arch_name, prim_unit) ;
        } else if (IsRtlElab()) {
        }
    }
    // This is block box instantiation: issue warning
    if (!prim_unit && is_blackbox && IsStaticElab()) {
        Warning("%s remains a black-box since it has no binding entity", _unit->Name()) ;
        StaticElaborateBlackBox() ;
        // VIPER #3428 : Try to create port association and issue error for ports having no actual/default
        //CreatePortAssociation(port_binding, _unit) ;
    }

    if (IsStaticElab()) {
        // VIPER 2324 : Pop structural stack before returning for static elab
        // VIPER 1863 & 1472
        //if (prim_unit) VhdlNode::PopStructuralStack() ; // Pop last entered primary unit id from structural stack

        // cleanup allocated arrays (if there)
        delete allocated_generic_binding ;
        delete allocated_port_binding ;
        return ; // For static elaboration remaining parts should be ignored
    }
}

void VhdlProcedureCallStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    VERIFIC_ASSERT(_call) ;

    // VIPER #2858 : Process body of procedure to check size mismatch between formal/actual
    //if (IsStaticElab() && !df) return ; // We are in concurrent procedure call, return for static elaboration
    if (df && df->IsDeadCode()) return ; // We are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // Call the name. Should be enough ?
    VhdlValue *retval = _call->Evaluate(0,df,0) ;
    // pragma'ed procedures could return a value. Delete that one to avoid memory leak.
    delete retval ;
}

void VhdlConditionalSignalAssignment::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!_target || !_conditional_waveforms || _conditional_waveforms->Size()==0) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if ((df && df->IsDeadCode()) || _target->IgnoreAssignment()) return ; // we are in dead code

    VhdlDataFlow *effective_df = df ;

    // VIPER 5143 : We should start at the MSB, and only evaluate LSB waveforms if not blocked by previous conditions.
    // This means we first need to evaluate the conditions, before we can evaluate the values of the waveforms.
    unsigned i ;
    VhdlConditionalWaveform *item ;
    VhdlConditionalWaveform *else_item = 0 ;
    VhdlExpression *item_condition ;
    VhdlValue *cond ;
    Map cond_to_item(POINTER_HASH) ; // will hold (VeriValue*)condition->(VhdlConditionalWaveform*)item pairs.
    // Run MSB(first)->LSB(last else) so we can stop when a condition becomes 'true'.
    // Conditional waveforms are stacked from back (last 'else' comes first in the array pf conditional waveforms).
    // So run BACK through the array :
    Array cond_null_items(1) ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms, i, item) {
        if (!item) continue ;

        // Evaluate the condition under which this waveform executes :
        cond = 0 ;
        item_condition = item->Condition() ;
        if (item_condition) {
            cond = item_condition->Evaluate(0, effective_df, 0) ;

            if (!cond) {
                // Viper 6967: For static elaboration Evaluation of rising_edge(clk) returns 0.
                if (IsStaticElab()) cond_null_items.InsertLast(item) ;
                continue ; // something wrong with this condition. Ignore item.
            }

            // Issue 2049 : constant FALSE condition evaluates 'else' value only :
            if (cond->IsConstant()) {
                // Constant condition.
                if (cond->IsTrue()) {
                    // If 'true' then we treat it as the (last) 'else'. (cond==0 and drop through)
                    delete cond ;
                    cond = 0 ; // treat as if this is the last 'else' part.
                } else {
                    // condition is false :
                    delete cond ;
                    continue ; // essentially ignore this item
                }
            }
        }

        // And determine where it goes :
        if (!cond) {
            // condition was true or this was the 'else' condition.
            else_item = item ;
            break ; // do not evaluate more waveforms after this
        } else {
                // store the (nonconstant) condition and the item that executes :
                (void) cond_to_item.Insert(cond, item) ;
        }
    }

    // Now, if there is  no 'else' value, then there was no 'else' part, and we need to hold the target value in that case :
    // Now build logic for the value/condition pairs.
    // Start with 'else_item'.
    //
    // Note, for static elaboration, the Map will be empty, but the else_item will have been set correctly (to the item that is always executed, or the real 'else' item).
    VhdlValue *value ;
    if (else_item) {
        value = else_item->ElaborateConditionalWaveform(_target, df) ;
    } else {
        // Oops. There is no 'else' value.
        // That means that under 'else', the target holds its value.
        // So have to create 'else' from the target value :
        value = _target->Evaluate(0,effective_df,0) ;
    }

    // If there are additional waveforms, their condition and items will be in the map.
    // Build a MUX tree back (LSB,last) to front (MSB,first) :
    MapIter mi ;
    VhdlValue *item_value ;
    FOREACH_MAP_ITEM_BACK(&cond_to_item, mi, &cond, &item) {
        if (!cond || !item) { delete cond ; continue ; } // something wrong with condition or item
        if (!value) { delete cond ; continue ; } // something bad went wrong creating the value (result of MUX tree).

        // Evaluate the waveform at this condition
        item_value = item->ElaborateConditionalWaveform(_target, df) ;
        if (!item_value) { delete cond ; continue ; } // something wrong with this value. Ignore item.

        // Check if the two waveforms match in array size.
        if (item_value->NumElements() != value->NumElements()) {
            // 'then' and 'else' value don't match
            item->Error("mismatch in number of elements assigned in conditional signal assignment") ;
            // Still let it go through ?
        }

        // Build selector :
        // MUX item value with current value on when_condition.
        VhdlNonconstBit *c = cond->ToNonconstBit() ;
        value = value->SelectOn1(c, item_value, 0, /*from*/item) ; // 'else_value' and 'item_value' are absorbed.
        delete c ; // delete the condition value
    }

    if (IsStaticElab()) {
        // Viper 6967: Now iterate over the cond_null_items array to call the missing elaboration calls.
        VhdlConditionalWaveform *wave_item ;
        unsigned j ;
        FOREACH_ARRAY_ITEM(&cond_null_items, j, wave_item) {
            if (!wave_item) continue ;
            item_value = wave_item->ElaborateConditionalWaveform(_target, df) ;
            // Check if the two waveforms match in array size.
            if (item_value && value && (item_value->NumElements() != value->NumElements())) {
                // 'then' and 'else' value don't match
                wave_item->Error("mismatch in number of elements assigned in conditional signal assignment") ;
                // Still let it go through ?
            }
            delete item_value ; // useless w/o cond called Elaboration to
        }
    }

    // result is now in 'value'.
    // cleanup (of 'cond' values in the map) is already done in the loop above.

    // Now assign 'else_value' to the target
    _target->Assign(value,effective_df,0) ;
}

void VhdlSelectedSignalAssignment::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!_expr || !_selected_waveforms || !_target) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if ((df && df->IsDeadCode()) || _target->IgnoreAssignment()) return ; // we are in dead code

#ifdef CHECK_CASE_STATEMENT_BEFORE_METALOGICAL_PRUNING
    // Some versions of the Vital package, timing_b.vhd has
    // incomplete case statement, if this check includes the
    // metalogical values as possibilities. Synthesis customers
    // using such packages may consider disable this check by
    // turning off the compile switch
    CheckSanity(df) ; // produces error if appropriate
#endif // CHECK_CASE_STATEMENT_BEFORE_METALOGICAL_PRUNING

    VhdlDataFlow *effective_df = df ;

    // Find target constraint (just always get it, even though we only need it for aggregate expressions)
    VhdlConstraint *target_constraint = _target->EvaluateConstraintInternal(effective_df, 0) ;
    if (!target_constraint) {
        return ; // Something bad in the target ?
    }

    // Evaluate case expression
    VhdlValue *expr = _expr->Evaluate(0,effective_df,0) ;

    // See if it is constant case expression
    unsigned is_constant = expr ? expr->IsConstant(): 0 ;

    // Here we could do something special if case_expr is constant. Like CaseStatement.
    // Not absolutely needed, since nonconst logic below also works with constants.

    // Get the case expression constraint :
    VhdlConstraint *case_expr_constraint = _expr->EvaluateConstraintInternal(effective_df, 0) ;

    if (case_expr_constraint && case_expr_constraint->IsUnconstrained()) {
        // Violation of LRM 8.5 : unconstrained array type not allowed
        // This rule is very strict, while not needed. Make it a warning.
        _expr->Warning("subtype of case expression is not locally static") ;
        delete case_expr_constraint ;
        case_expr_constraint = 0 ; // Cant proceed with the unconstrained type will blow up the num_states.
    }

    // Calculations up-front : calculate the number of states this case expression can be in :
    verific_uint64 num_states = 0 ; // Viper #4222: type changed from unsigned -> verific_uint64

    // Calculations up-front
    unsigned num_bits = expr ? expr->NumBits(): 0 ;

    // Do a real accurate VHDL analysis using the case expression subtype constraint :
    if (case_expr_constraint) {
        VhdlConstraint *element_constraint = case_expr_constraint ;
        // Normal case : expression is constrained with a locally static subtype
        verific_uint64 num_array_elements = 1 ;
        if (case_expr_constraint->IsArrayConstraint()) {
            num_array_elements = case_expr_constraint->Length() ;
            element_constraint = case_expr_constraint->ElementConstraint() ;
        }

        // Scalar element_constraint
        verific_uint64 num_element_states = element_constraint->Length() ;
        if (element_constraint->IsBitEncoded() && (num_element_states > 2)) {
            // Choices with X and Z are already removed from the 'choices' array,
            // so we will not have more than 2 states.
            num_element_states = 2 ;
        }

        // num_states should be a sensible number, we would have errored
        // out in case_expr_constraint construction/operations otherwise
        num_states = 1 ;
        while (num_array_elements) { num_states *= num_element_states ; --num_array_elements ; }

        // Just to be sure that 'num_states' is calculated correctly :
        // if the number of bits is larger than 32 bits, and we are not onehot encoded,
        // then we are dealing with an unconstrained integer scalar.
        // In that case, we cannot calculate number of states.
        if ((num_bits >= sizeof(unsigned)*8) && expr && !expr->OnehotEncoded()) {
            num_states = 0 ;
        }
    } else {
        // If we don't have a VHDL constraint (should not happen often any more) :
        // derive number of states from the number of bits.
        if (expr && expr->OnehotEncoded()) {
            num_states = num_bits ;
        } else if (num_bits < sizeof(unsigned)*8) {
            num_states = (verific_uint64)1 << num_bits ;
        }
    }

    //delete case_expr_constraint ; // It surved its purpose

    // Hash choice->SelectedWaveform. choice is pointer to tree a DiscreteRange treenode
    Map choices(POINTER_HASH, _selected_waveforms->Size()+1 /*reasonable estimate of size*/) ;
    verific_uint64 extra_count = 0 ;
    // Hash num_value->SelectedWaveform
    // In VHDL, all choices are constant. for <32 bit
    // values, we can hash and test for uniqueness.
    // This table also helps to find alternatives by hashing on
    // their numeric value. Used only for <32 bit expressions.
    Map positions(NUM_HASH, _selected_waveforms->Size()+1) ;
    Set overlap_check(STRING_HASH, _selected_waveforms->Size()+1) ;

    // Finally, keep track of the values created by each selected waveform.
    // Hash them SelectedWaveform->Value, so we can find them later.
    Map waveform_to_value(POINTER_HASH, _selected_waveforms->Size()+1) ;
    VhdlValue *value ;

    VhdlSelectedWaveform *alt, *others = 0;
    unsigned i ;

    // Now run over all case alternatives, and collect their choice->dataflows in 'choices' and 'positions'
    unsigned num_choices = 0 ;
    unsigned has_range_compare = 0 ;
    if (_is_matching) {
        FOREACH_ARRAY_ITEM(_selected_waveforms, i, alt) {
            if (!alt) continue ;
            Array* s_choices = alt->GetWhenChoices() ;
            num_choices += s_choices ? s_choices->Size() : 0 ;
            if (alt->HasRangeChoice()) has_range_compare = 1;
        }
    }
    // Take the power-of two of the number of bits (set to 0 for >32 bits) :
    unsigned mux_size = (num_bits < sizeof(unsigned)*8) ? (1 << num_bits) : 0;

    unsigned populate_dc_positions = (!mux_size || is_constant || num_bits==1 || has_range_compare || (!expr || expr->OnehotEncoded()) || (num_choices < mux_size/EQ_SEL_BREAKPOINT)) ? 0 : 1 ;

    unsigned last ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, alt) {
        last = choices.Size() ;

        // Elaborate all case conditions, filter out meta-values, test for constant.
        // There could be many values, and only a few alternatives.  In VHDL, 'others'
        // choice should be last. We tested that in the constructor already.  When
        // encountered in 'ElaborateChoices', it will be returned.

        // Viper 7157: Pass two new integers extra_count and populate_dc_positions required for synthesizng select signal statements
        // with  dont_care, extra_count contains the extra number of choices accomodated by use of  dont_cares
        // term '-', extra_count is passed by reference to ElaborateChoices. populate_dc_positions stores the decision
        // about whether we want to create equal operaor or muxes. If populate_dc_positions is true we  explode the
        // dont_cares literals to generate all possible literals and populate the positions array. This is done
        // for matching select statements of Vhdl2008 containing meta value '-' in choice.

        others = alt->ElaborateChoices(choices, positions, expr, case_expr_constraint, df, overlap_check, _is_matching, extra_count, populate_dc_positions) ;

        if (alt->HasRangeChoice()) has_range_compare = 1;

        // Elaborate the selected waveform if some choice was added
        if (last != choices.Size() || (alt==others && !(is_constant && choices.Size()))) {
            value = alt->ElaborateSelectedWaveform(target_constraint, _target, df) ;
            if (!value) continue ;

            // Issue 1545 : Selected signal assignment needs to check the multiple values
            // against each other (or actually against the target constraint), before
            // we create the selection logic that chooses between them.
            // First of all, we need a constained target (could still be unconstrained if aggregate target).
            if (target_constraint->IsUnconstrained()) target_constraint->ConstraintBy(value) ;

            // Check expression against target constraint
            if (!value->CheckAgainst(target_constraint,alt, df)) {
                delete value ;
                continue ;
            }

            (void) waveform_to_value.Insert(alt, value) ;
        }
    }
    delete case_expr_constraint ; // It surved its purpose

    // clean up.
    SetIter si ;
    char * image ;
    FOREACH_SET_ITEM(&overlap_check, si, &image) {
        Strings::free(image) ;
    }

    // VIPER 4520. Moved this check above out of RTL Elab specific code
    // Now static elaboration also issues the warning
    // Check if we are full-case
    // FIX ME : user-driven pragma
    unsigned full_case = 0 ;
    if (num_states && !has_range_compare && num_states<=(choices.Size() + extra_count)) { // add extra count to account for meta value states
        // We have full case
        if (others) {
            // Don't warn. 'others' can be for meta-states
            others = 0 ;
        }
        full_case = 1 ;
    }

    if (!full_case && !has_range_compare && !others) {
        _expr->Warning("statement might not cover all choices ; 'others' clause recommended") ;
    }

    // If condition can't be evaluated, or for static elaboration, do not proceed further
    if (!expr || IsStaticElab()) {
        // Do a target name check (indexing etc) for static elab.
        _target->Assign(0,effective_df,0) ;
        // cleanup
        delete expr ;
        delete target_constraint ;
        MapIter mi ;
        // Clean up the waveform values
        FOREACH_MAP_ITEM(&waveform_to_value, mi, 0, &value) delete value ;

        return ;
    }
}
void VhdlIfElsifGenerateStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config)
{
    if (df && df->IsDeadCode()) return ; // We are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    if (IsStaticElab()) {
        // Unroll loops, select proper statement form if-generate
        StaticElaborate(block_config, df) ;
        return ;
    }

    // VIPER #7683: Label will be push/pop within Elaborate of scheme
    //if (_label) PushNamedPath(_label->OrigName()) ;
    unsigned active_branch = 0 ;
    if (_scheme) {
        active_branch = _scheme->Elaborate(_label, _decl_part,_statement_part, block_config, df) ;
    }
    if (active_branch) { // If branch is active, no need to check remaining branches
        //if (_label) PopNamedPath() ;
        return ;
    }
    // Check elsif branches
    unsigned i ;
    VhdlGenerateStatement *elsif_stmt ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif_stmt) {
        if (!elsif_stmt) continue ;
        active_branch = elsif_stmt->ElaborateElsifElse(_label, block_config, df) ;
        if (active_branch) {
            //if (_label) PopNamedPath() ;
            return ;
        }
    }
    // Execute else branch :
    if (_else_stmt) (void)_else_stmt->ElaborateElsifElse(_label, block_config, df) ;

    //if (_label) PopNamedPath() ;
}
// Elaborate elsif or else branch of if-generate statemnet. Each branch is a VhdlGenerateStatement
unsigned VhdlGenerateStatement::ElaborateElsifElse(VhdlIdDef *gen_label, VhdlBlockConfiguration *block_config, VhdlDataFlow *df) {
    if (df && df->IsDeadCode()) return 0 ; // We are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return 0 ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Pass label of generate for elsif/else branch elaboration
    return (_scheme) ? _scheme->Elaborate(gen_label, _decl_part,_statement_part, block_config, df): 0 ;
}
void VhdlCaseGenerateStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config)
{
    if (df && df->IsDeadCode()) return ; // We are in dead code
    if (!_expr || !_alternatives) return ; // nothing to elaborate
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    VhdlValue *expr = _expr->Evaluate(0,df,0) ;
    if (!expr) return ;

    if (!expr->IsConstant()) {
        _expr->Error("generate condition is not constant") ;
        delete expr ;
        return ;
    }
    // Declare some stuff that we need :
    unsigned i ;
    VhdlGenerateStatement *alt = 0;

    // Hash choice (DiscreteRange)->case-item-dataflow(DataFlow) in 'choices'.
    // These are all just pointers, no memory allocated.
    // There will be an entry inserted by ElaborateChoices() for a case item that matches the case expression.
    Map choices(POINTER_HASH, _alternatives->Size()+1 /*reasonable estimate of size*/) ;

    // Hash num_value(int)->case-item-dataflow(DataFlow) in 'positions'.
    // In VHDL, all choices are constant. for <32 bit
    // values, we can hash and test for uniqueness.
    // This table also helps to find alternatives by hashing on
    // their numeric value. Only works for <32 bit.
    // So we use this Map only for <32 bit expressions.
    Map positions(NUM_HASH, _alternatives->Size()+1) ;

    // Viper #3598 : choice_positions cannot be used for overlap checking
    // When enum_encoding attribute is not present, the way we generate
    // hash key currently, can easily result in collision of hash keys for
    // distinct values. Need to generate more unique keys, using Image for now

    Set overlap_check(STRING_HASH, _alternatives->Size()+1) ;

    // Get the case expression constraint :
    VhdlConstraint *case_expr_constraint = _expr->EvaluateConstraintInternal(df, 0) ;

    if (case_expr_constraint && case_expr_constraint->IsUnconstrained()) {
        // Violation of LRM 8.5 : unconstrained array type not allowed
        // This rule is very strict, while not needed. Make it a warning.
        if (!expr->IsConstant()) _expr->Warning("subtype of case expression is not locally static") ;
        delete case_expr_constraint ;
        case_expr_constraint = 0 ; // Cant proceed with the unconstrained type will blow up the num_states.
    }

    // What we need to do here is just try every case alternative, until one hits (gets inserted by ElaborateChoices()).
    // If none hit, the 'others' alternative is chosen.
    // Once one hits, we can execute that alternative (in the same dataflow as 'this')
    // and we can discard all other alternatives.
    Map *condition_id_value_map = 0 ;
    VhdlIterScheme *case_item_scheme = 0, *others_scheme = 0 ;
    VhdlGenerateStatement *active_alt = 0 ;
    FOREACH_ARRAY_ITEM(_alternatives, i, alt) {
        case_item_scheme = alt ? alt->GetScheme(): 0 ;
        if (!case_item_scheme) continue ;
        others_scheme = case_item_scheme->ElaborateChoices(choices, positions, expr, df, case_expr_constraint, overlap_check, 0, condition_id_value_map) ;
        VERIFIC_ASSERT(!condition_id_value_map) ; // The map is not created for constant expr
        if (choices.Size() || (others_scheme == case_item_scheme)) {
            // This choice did 'hit' (it matches the constant case expression)
            // or this is the 'others' case item.
            // Elaborate this alternative.
            active_alt = alt ;
            break ; // we are done.
        }
    }
    // See if anything hit :
    if (!others_scheme && !choices.Size()) {
        _expr->Error("case statement does not cover all choices. 'others' clause is needed") ;
    }

    // clean up.
    SetIter si ;
    char * image ;
    FOREACH_SET_ITEM(&overlap_check, si, &image) {
        Strings::free(image) ;
    }

    delete case_expr_constraint ;
    delete expr ;

    // Now elaborate the active alternative
    if (active_alt) {
        if (IsRtlElab()) {
            active_alt->ElaborateAlternative(_label, df, block_config) ;
        } else {
            StaticElaborateActiveAlternative(active_alt, _label, df, block_config) ;
        }
    } else if (_label) {
        // VIPER #6793: Create empty block when condition of if is false or
        // _scheme is for-scheme.
        VhdlMapForCopy old2new ;
        Map new_ids(STRING_HASH_CASE_INSENSITIVE) ;
        VhdlBlockStatement *blk_stmt = VhdlIterScheme::CreateBlockStatement(_label, _present_scope, 0, 0, 0, old2new, &new_ids, this) ;
        blk_stmt->SetStaticElaborated() ;
        blk_stmt->SetIsElabCreatedEmpty() ;
        Array *new_stmts = new Array(1) ;
        new_stmts->Insert(blk_stmt) ;
        VhdlGenerateStatement::ReplaceByBlocks(_present_scope, &new_ids, new_stmts, this) ;
    }
}
void VhdlGenerateStatement::ElaborateAlternative(VhdlIdDef *stmt_label, VhdlDataFlow *df, VhdlBlockConfiguration *block_config)
{
    if (df && df->IsDeadCode()) return ; // We are in dead code
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    if (stmt_label) PushNamedPath(stmt_label->OrigName()) ;
    PushAttributeSpecificPath(stmt_label ? stmt_label->Name():0) ;

    if (block_config) block_config->Elaborate() ;

    // Configure the block (if needed)
    VhdlBlockConfiguration *subblock_config = (block_config) ? block_config->ExtractSubBlock(stmt_label, 0, 0) : 0 ;
    // VIPER #3367: We are going to process delcarative region, set this flag:
    VhdlDataFlow *decl_flow = (df) ? df : new VhdlDataFlow(0, 0, (stmt_label) ? stmt_label->LocalScope() : 0) ;
    // RD: make sure to store existing state of 'InInitial' :
    unsigned was_in_initial = (decl_flow) ? decl_flow->IsInInitial() : 0 ;
    if (!was_in_initial) decl_flow->SetInInitial() ;

    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        decl->Elaborate(decl_flow) ;
    }

    // VIPER #3367: Done processing delcarative region, reset the flag:
    if (!was_in_initial) decl_flow->SetNotInInitial() ;
    if (!df) delete decl_flow ;

    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        stmt->Elaborate(df,subblock_config) ;
    }

    delete subblock_config ;
    PopAttributeSpecificPath() ;
    if (stmt_label) PopNamedPath() ;
}

void VhdlConditionalVariableAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!_target || !_conditional_expressions || _conditional_expressions->Size()==0) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    if ((df && df->IsDeadCode()) || _target->IgnoreAssignment()) return ; // we are in dead code

    VhdlDataFlow *effective_df = df ;

    // VIPER 5143 : We should start at the MSB, and only evaluate LSB waveforms if not blocked by previous conditions.
    // This means we first need to evaluate the conditions, before we can evaluate the values of the waveforms.
    unsigned i ;
    VhdlConditionalExpression *item ;
    VhdlConditionalExpression *else_item = 0 ;
    VhdlExpression *item_condition ;
    VhdlValue *cond ;
    Map cond_to_item(POINTER_HASH) ; // will hold (VeriValue*)condition->(VhdlConditionalWaveform*)item pairs.
    // Run MSB(first)->LSB(last else) so we can stop when a condition becomes 'true'.
    // Conditional waveforms are stacked from back (last 'else' comes first in the array pf conditional waveforms).
    // So run BACK through the array :
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, item) {
        if (!item) continue ;

        // Evaluate the condition under which this waveform executes :
        cond = 0 ;
        item_condition = item->Condition() ;
        if (item_condition) {
            cond = item_condition->Evaluate(0, effective_df, 0) ;
            if (!cond) continue ; // something wrong with this condition. Ignore item.

            // Issue 2049 : constant FALSE condition evaluates 'else' value only :
            if (cond->IsConstant()) {
                // Constant condition.
                if (cond->IsTrue()) {
                    // If 'true' then we treat it as the (last) 'else'. (cond==0 and drop through)
                    delete cond ;
                    cond = 0 ; // treat as if this is the last 'else' part.
                } else {
                    // condition is false :
                    delete cond ;
                    continue ; // essentially ignore this item
                }
            }
        }

        // And determine where it goes :
        if (!cond) {
            // condition was true or this was the 'else' condition.
            else_item = item ;
            break ; // do not evaluate more waveforms after this
        } else {
            // store the (nonconstant) condition and the item that executes :
            (void) cond_to_item.Insert(cond,item) ;
        }
    }

    // Now, if there is  no 'else' value, then there was no 'else' part, and we need to hold the target value in that case :
    // Now build logic for the value/condition pairs.
    // Start with 'else_item'.
    //
    // Note, for static elaboration, the Map will be empty, but the else_item will have been set correctly (to the item that is always executed, or the real 'else' item).
    VhdlValue *value ;
    if (else_item) {
        value = else_item->ElaborateConditionalExpression(_target, df) ;
    } else {
        // Oops. There is no 'else' value.
        // That means that under 'else', the target holds its value.
        // So have to create 'else' from the target value :
        value = _target->Evaluate(0,effective_df,0) ;
    }

    // If there are additional waveforms, their condition and items will be in the map.
    // Build a MUX tree back (LSB,last) to front (MSB,first) :
    MapIter mi ;
    VhdlValue *item_value ;
    FOREACH_MAP_ITEM_BACK(&cond_to_item, mi, &cond, &item) {
        if (!cond || !item) continue ; // something wrong with condition or item
        if (!value) continue ; // something bad went wrong creating the value (result of MUX tree).

        // Evaluate the waveform at this condition
        item_value = item->ElaborateConditionalExpression(_target, df) ;
        if (!item_value) continue ; // something wrong with this value. Ignore item.

        // Check if the two waveforms match in array size.
        if (item_value->NumElements() != value->NumElements()) {
            // 'then' and 'else' value don't match
            item->Error("mismatch in number of elements assigned in conditional signal assignment") ;
            // Still let it go through ?
        }

        // Build selector :
        // MUX item value with current value on when_condition.
        VhdlNonconstBit *c = cond->ToNonconstBit() ;
        value = value->SelectOn1(c, item_value, 0, /*from*/item) ; // 'else_value' and 'item_value' are absorbed.
        delete c ; // delete the condition value
    }

    // result is now in 'value'.
    // cleanup (of 'cond' values in the map) is already done in the loop above.

    // Now assign 'else_value' to the target
    _target->Assign(value,effective_df,0) ;
}

void VhdlSelectedVariableAssignmentStatement::Elaborate(VhdlDataFlow *df, VhdlBlockConfiguration * /*block_config*/)
{
    if (!_expr || !_selected_exprs || !_target) return ;
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    if ((df && df->IsDeadCode()) || _target->IgnoreAssignment()) return ; // we are in dead code

    VhdlDataFlow *effective_df = df ;

    // Find target constraint (just always get it, even though we only need it for aggregate expressions)
    VhdlConstraint *target_constraint = _target->EvaluateConstraintInternal(effective_df, 0) ;
    if (!target_constraint) {
        return ; // Something bad in the target ?
    }

    // Evaluate case expression
    VhdlValue *expr = _expr->Evaluate(0,effective_df,0) ;

    // See if it is constant case expression
    unsigned is_constant = expr ? expr->IsConstant(): 0 ;

    // Here we could do something special if case_expr is constant. Like CaseStatement.
    // Not absolutely needed, since nonconst logic below also works with constants.

    // Get the case expression constraint :
    VhdlConstraint *case_expr_constraint = _expr->EvaluateConstraintInternal(effective_df, 0) ;

    if (case_expr_constraint && case_expr_constraint->IsUnconstrained()) {
        // Violation of LRM 8.5 : unconstrained array type not allowed
        // This rule is very strict, while not needed. Make it a warning.
        _expr->Warning("subtype of case expression is not locally static") ;
        delete case_expr_constraint ;
        case_expr_constraint = 0 ; // Cant proceed with the unconstrained type will blow up the num_states.
    }

    // Calculations up-front : calculate the number of states this case expression can be in :
    verific_uint64 num_states = 0 ; // Viper #4222: type changed from unsigned -> verific_uint64

    // Calculations up-front
    unsigned num_bits = expr ? expr->NumBits(): 0 ;

    // Do a real accurate VHDL analysis using the case expression subtype constraint :
    if (case_expr_constraint) {
        VhdlConstraint *element_constraint = case_expr_constraint ;
        // Normal case : expression is constrained with a locally static subtype
        verific_uint64 num_array_elements = 1 ;
        if (case_expr_constraint->IsArrayConstraint()) {
            num_array_elements = case_expr_constraint->Length() ;
            element_constraint = case_expr_constraint->ElementConstraint() ;
        }

        // Scalar element_constraint
        verific_uint64 num_element_states = element_constraint->Length() ;
        if (element_constraint->IsBitEncoded() && (num_element_states > 2)) {
            // Choices with X and Z are already removed from the 'choices' array,
            // so we will not have more than 2 states.
            num_element_states = 2 ;
        }

        // num_states should be a sensible number, we would have errored
        // out in case_expr_constraint construction/operations otherwise
        num_states = 1 ;
        while (num_array_elements) { num_states *= num_element_states ; --num_array_elements ; }

        // Just to be sure that 'num_states' is calculated correctly :
        // if the number of bits is larger than 32 bits, and we are not onehot encoded,
        // then we are dealing with an unconstrained integer scalar.
        // In that case, we cannot calculate number of states.
        if ((num_bits >= sizeof(unsigned)*8) && expr && !expr->OnehotEncoded()) {
            num_states = 0 ;
        }
    } else {
        // If we don't have a VHDL constraint (should not happen often any more) :
        // derive number of states from the number of bits.
        if (expr && expr->OnehotEncoded()) {
            num_states = num_bits ;
        } else if (num_bits < sizeof(unsigned)*8) {
            num_states = (verific_uint64)1 << num_bits ;
        }
    }

    //delete case_expr_constraint ; // It surved its purpose

    // Hash choice->SelectedWaveform. choice is pointer to tree a DiscreteRange treenode
    Map choices(POINTER_HASH, _selected_exprs->Size()+1 /*reasonable estimate of size*/) ;

    // Hash num_value->SelectedExpression
    // In VHDL, all choices are constant. for <32 bit
    // values, we can hash and test for uniqueness.
    // This table also helps to find alternatives by hashing on
    // their numeric value. Used only for <32 bit expressions.
    Map positions(NUM_HASH, _selected_exprs->Size()+1) ;

    Set overlap_check(STRING_HASH, _selected_exprs->Size()+1) ;

    // Now run over all case alternatives, and collect their choice->dataflows in 'choices' and 'positions'
    unsigned num_choices = 0 ;
    unsigned has_range_compare = 0 ;
    VhdlSelectedExpression *s_expr, *others = 0;
    if (_is_matching) {
        unsigned i ;
        FOREACH_ARRAY_ITEM(_selected_exprs, i, s_expr) {
            if (!s_expr) continue ;
            Array* s_choices = s_expr->GetWhenChoices() ;
            num_choices += s_choices ? s_choices->Size() : 0 ;
            if (s_expr->HasRangeChoice()) has_range_compare = 1;
        }
    }
    // Take the power-of two of the number of bits (set to 0 for >32 bits) :
    unsigned mux_size = (num_bits < sizeof(unsigned)*8) ? (1 << num_bits) : 0;

    unsigned populate_dc_positions = (!mux_size || is_constant || num_bits==1 || has_range_compare || (!expr || expr->OnehotEncoded()) || (num_choices < mux_size/EQ_SEL_BREAKPOINT)) ? 0 : 1 ;

    // Finally, keep track of the values created by each selected waveform.
    // Hash them SelectedWaveform->Value, so we can find them later.
    Map waveform_to_value(POINTER_HASH, _selected_exprs->Size()+1) ;
    VhdlValue *value ;
  
    verific_uint64 extra_count = 0 ;
    unsigned i ;
    unsigned last ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, s_expr) {
        last = choices.Size() ;

        // Elaborate all case conditions, filter out meta-values, test for constant.
        // There could be many values, and only a few alternatives.  In VHDL, 'others'
        // choice should be last. We tested that in the constructor already.  When
        // encountered in 'ElaborateChoices', it will be returned.

        // Viper 7157: Pass two new integers extra_count and populate_dc_positions required for synthesizng selected var assignment statements
        // with  dnot_cares. extra_count contains the extra number of choices accomodated by use of  dont_cares
        // term '-', extra_count is passed by reference to ElaborateChoices. populate_dc_positions stores the decision
        // about whether we want to create equal operaor or muxes. If populate_dc_positions is true we  explode the
        // dont_care literals to generate all possible literals and populate the positions array. This is done
        // for matching select statements of Vhdl2008 containing meta value '-' in choice.

        others = s_expr->ElaborateChoices(choices, positions, expr, case_expr_constraint, df, overlap_check, _is_matching, extra_count, populate_dc_positions) ;

        if (s_expr->HasRangeChoice()) has_range_compare = 1;

        // Elaborate the selected waveform if some choice was added
        if (last != choices.Size() || (s_expr==others && !(is_constant && choices.Size()))) {
            value = s_expr->ElaborateSelectedExpression(target_constraint, _target, df) ;
            if (!value) continue ;

            // Issue 1545 : Selected signal assignment needs to check the multiple values
            // against each other (or actually against the target constraint), before
            // we create the selection logic that chooses between them.
            // First of all, we need a constained target (could still be unconstrained if aggregate target).
            if (target_constraint->IsUnconstrained()) target_constraint->ConstraintBy(value) ;

            // Check expression against target constraint
            if (!value->CheckAgainst(target_constraint,s_expr, df)) {
                delete value ;
                continue ;
            }

            (void) waveform_to_value.Insert(s_expr, value) ;
        }
    }
    delete case_expr_constraint ; // It surved its purpose

    // clean up.
    SetIter si ;
    char * image ;
    FOREACH_SET_ITEM(&overlap_check, si, &image) {
        Strings::free(image) ;
    }

    // VIPER 4520. Moved this check above out of RTL Elab specific code
    // Now static elaboration also issues the warning
    // Check if we are full-case
    // FIX ME : user-driven pragma
    unsigned full_case = 0 ;
    if (num_states && !has_range_compare && num_states<=(choices.Size()+extra_count)) {
        // We have full case
        if (others) {
            // Don't warn. 'others' can be for meta-states
            others = 0 ;
        }
        full_case = 1 ;
    }

    if (!full_case && !has_range_compare && !others) {
        _expr->Warning("statement might not cover all choices ; 'others' clause recommended") ;
    }

    // If condition can't be evaluated, or for static elaboration, do not proceed further
    if (!expr || IsStaticElab()) {
        // Do a target name check (indexing etc) for static elab.
        _target->Assign(0,effective_df,0) ;
        // cleanup
        delete expr ;
        delete target_constraint ;
        MapIter mi ;
        // Clean up the waveform values
        FOREACH_MAP_ITEM(&waveform_to_value, mi, 0, &value) delete value ;

        return ;
    }
}

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
void VhdlStatement::InitializeForVariableSlice()
{
    if (_label) _label->InitializeForVariableSlice() ;
}

void VhdlReturnStatement::InitializeForVariableSlice()
{
    if (_expr) _expr->InitializeForVariableSlice() ;
    if (_owner) _owner->InitializeForVariableSlice() ;
}

void VhdlExitStatement::InitializeForVariableSlice()
{
    if (_target) _target->InitializeForVariableSlice() ;
    if (_condition) _condition->InitializeForVariableSlice() ;
    if (_target_label) _target_label->InitializeForVariableSlice() ;
}

void VhdlNextStatement::InitializeForVariableSlice()
{
    if (_target) _target->InitializeForVariableSlice() ;
    if (_condition) _condition->InitializeForVariableSlice() ;
    if (_target_label) _target_label->InitializeForVariableSlice() ;
}

void VhdlLoopStatement::InitializeForVariableSlice()
{
    if (_iter_scheme) _iter_scheme->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlStatement *elem ;
    FOREACH_ARRAY_ITEM(_statements,i,elem) {
        if (!elem) continue ;
        elem->InitializeForVariableSlice() ;
    }
}

void VhdlCaseStatement::InitializeForVariableSlice()
{
    if (_expr) _expr->InitializeForVariableSlice() ;
    VhdlCaseStatementAlternative *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_alternatives, i, elem) {
        if (!elem) continue ;
        elem->InitializeForVariableSlice() ;
    }
    if (_expr_type) _expr_type->InitializeForVariableSlice() ;
}

void VhdlIfStatement::InitializeForVariableSlice()
{
    if (_if_cond) _if_cond->InitializeForVariableSlice() ;

    // Viper #6027: we need to evaluate the elseif conditions
    // as well. They belong in the scope of this statement
    unsigned i ;
    VhdlElsif *elsif ;
    VhdlExpression *elsif_cond ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif) {
        elsif_cond = elsif->Condition() ;
        if (elsif_cond) elsif_cond->InitializeForVariableSlice() ;
    }
    // Viper #5696: return right here.. The statements in the
    // branches will be checked for variable slice during
    // respentive elaboration calls.
    return ;

#if 0
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_if_statements,i,elem) {
        if (!elem) continue ;
        elem->InitializeForVariableSlice() ;
    }
    FOREACH_ARRAY_ITEM(_elsif_list,i,elem) {
        if (!elem) continue ;
        elem->InitializeForVariableSlice() ;
    }
    FOREACH_ARRAY_ITEM(_else_statements,i,elem) {
        if (!elem) continue ;
        elem->InitializeForVariableSlice() ;
    }
#endif
}

void VhdlVariableAssignmentStatement::InitializeForVariableSlice()
{
    unsigned variable_count = VhdlVariableSlice::GetNumVariables() ; // Viper #6552
    if (_target) _target->InitializeForVariableSlice() ;
    unsigned has_var_slice = VhdlVariableSlice::GetNumVariables() > variable_count ;

    VhdlDataFlow *df = VhdlVariableSlice::GetTopDataFlow() ;
    if (df && !has_var_slice && _target && _value && (_target->IsArraySlice() || _value->IsArraySlice())) { // Viper #6455
        VhdlConstraint *constraint = _target->EvaluateConstraintInternal(df, 0) ;
        verific_uint64 length = constraint ? constraint->Length() : 0 ;
        delete constraint ;

        if (length) VhdlVariableSlice::SetConstraintLength(length) ;
    }

    variable_count = VhdlVariableSlice::GetNumVariables() ; // Viper #6552
    if (_value) _value->InitializeForVariableSlice() ;
    has_var_slice = VhdlVariableSlice::GetNumVariables() > variable_count ;

    if (df && !has_var_slice && _target && _value && (_target->IsArraySlice() || _value->IsArraySlice())) { // Viper #6455
        VhdlConstraint *constraint = _value->EvaluateConstraintInternal(df, 0) ;
        verific_uint64 length = constraint ? constraint->Length() : 0 ;
        delete constraint ;

        if (length) VhdlVariableSlice::SetConstraintLength(length) ;
    }
}

void VhdlSignalAssignmentStatement::InitializeForVariableSlice()
{
    unsigned variable_count = VhdlVariableSlice::GetNumVariables() ; // Viper #6552
    if (_target) _target->InitializeForVariableSlice() ;
    unsigned has_var_slice = VhdlVariableSlice::GetNumVariables() > variable_count ;

    verific_uint64 length = 0 ;
    VhdlDataFlow *df = VhdlVariableSlice::GetTopDataFlow() ;
    if (df && !has_var_slice) { // Viper #6455
        VhdlConstraint *constraint = _target->EvaluateConstraintInternal(df, 0) ;
        length = constraint ? constraint->Length() : 0 ;
        delete constraint ;
    }

    unsigned i ;
    VhdlExpression *elem ;
    FOREACH_ARRAY_ITEM(_waveform,i,elem) {
        if (!elem) continue ;
        variable_count = VhdlVariableSlice::GetNumVariables() ; // Viper #6552
        elem->InitializeForVariableSlice() ;
        has_var_slice = VhdlVariableSlice::GetNumVariables() > variable_count ;

        if (df && _target && (_target->IsArraySlice() || elem->IsArraySlice())) { // Viper #6455
            if (length) VhdlVariableSlice::SetConstraintLength(length) ;

            if (!has_var_slice) { // Viper #6455
                VhdlConstraint *constraint = elem->EvaluateConstraintInternal(df, 0) ;
                length = constraint ? constraint->Length() : 0 ;
                delete constraint ;

                if (length) VhdlVariableSlice::SetConstraintLength(length) ;
            }
        }
    }
}

void VhdlWaitStatement::InitializeForVariableSlice()
{
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_sensitivity_clause, i, name) {
        if (!name) continue ;
        name->InitializeForVariableSlice() ;
    }
    if (_condition_clause) _condition_clause->InitializeForVariableSlice() ;
    if (_timeout_clause) _timeout_clause->InitializeForVariableSlice() ;
}

void VhdlReportStatement::InitializeForVariableSlice()
{
    if (_report) _report->InitializeForVariableSlice() ;
    if (_severity) _severity->InitializeForVariableSlice() ;
}

void VhdlAssertionStatement::InitializeForVariableSlice()
{
    if (_condition) _condition->InitializeForVariableSlice() ;
    if (_report) _report->InitializeForVariableSlice() ;
    if (_severity) _severity->InitializeForVariableSlice() ;
}

void VhdlProcessStatement::InitializeForVariableSlice()
{
    VhdlName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_sensitivity_list,i,name) {
        if (!name) continue ;
        name->InitializeForVariableSlice() ;
    }
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part,i,decl) {
        if (!decl) continue ;
        decl->InitializeForVariableSlice() ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part,i,stmt) {
        if (!stmt) continue ;
        stmt->InitializeForVariableSlice() ;
    }
}

void VhdlBlockStatement::InitializeForVariableSlice()
{
    if (_guard) _guard->InitializeForVariableSlice() ;
    if (_generics) _generics->InitializeForVariableSlice() ;
    if (_ports) _ports->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part,i,decl) {
        if (!decl) continue ;
        decl->InitializeForVariableSlice() ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statements,i,stmt) {
        if (!stmt) continue ;
        stmt->InitializeForVariableSlice() ;
    }
    if (_guard_id) _guard_id->InitializeForVariableSlice() ;
}

void VhdlGenerateStatement::InitializeForVariableSlice()
{
    if (_scheme) _scheme->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->InitializeForVariableSlice() ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part,i,stmt) {
        if (!stmt) continue ;
        stmt->InitializeForVariableSlice() ;
    }
}

void VhdlComponentInstantiationStatement::InitializeForVariableSlice()
{
    if (_instantiated_unit) _instantiated_unit->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlDiscreteRange *drng ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,drng) {
        if (!drng) continue ;
        drng->InitializeForVariableSlice() ;
    }
    FOREACH_ARRAY_ITEM(_port_map_aspect,i,drng) {
        if (!drng) continue ;
        drng->InitializeForVariableSlice() ;
    }
    if (_unit) _unit->InitializeForVariableSlice() ;
}

void VhdlProcedureCallStatement::InitializeForVariableSlice()
{
    if (_call) _call->InitializeForVariableSlice() ;
}

void VhdlConditionalSignalAssignment::InitializeForVariableSlice()
{
    if (_target) _target->InitializeForVariableSlice() ;
    if (_options) _options->InitializeForVariableSlice() ;
    if (_guard_id) _guard_id->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlConditionalWaveform *elem ;
    FOREACH_ARRAY_ITEM(_conditional_waveforms,i,elem) {
        if (!elem) continue ;
        VhdlVariableSlice::SetBranchId(i) ; // Viper# 6546
        elem->InitializeForVariableSlice() ;
    }
}

void VhdlSelectedSignalAssignment::InitializeForVariableSlice()
{
    if (_expr) _expr->InitializeForVariableSlice() ;
    if (_target) _target->InitializeForVariableSlice() ;
    if (_expr_type) _expr_type->InitializeForVariableSlice() ;
    if (_guard_id) _guard_id->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlSelectedWaveform *wavf ;
    FOREACH_ARRAY_ITEM(_selected_waveforms,i,wavf) {
        if (!wavf) continue ;
        VhdlVariableSlice::SetBranchId(i) ; // Viper# 6546
        wavf->InitializeForVariableSlice() ;
    }
}

void VhdlIfElsifGenerateStatement::InitializeForVariableSlice()
{
    VhdlGenerateStatement::InitializeForVariableSlice() ;

    unsigned i ;
    VhdlStatement *elsif_stmt ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif_stmt) {
        if (!elsif_stmt) continue ;
        elsif_stmt->InitializeForVariableSlice() ;
    }
    if (_else_stmt) _else_stmt->InitializeForVariableSlice() ;
}
void VhdlCaseGenerateStatement::InitializeForVariableSlice()
{
    if (_expr) _expr->InitializeForVariableSlice() ;

    unsigned i ;
    VhdlStatement *alternative ;
    FOREACH_ARRAY_ITEM(_alternatives, i, alternative) {
        if (!alternative) continue ;
        alternative->InitializeForVariableSlice() ;
    }
    if (_expr_type) _expr_type->InitializeForVariableSlice() ;
}

void VhdlConditionalVariableAssignmentStatement::InitializeForVariableSlice()
{
    if (_target) _target->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlConditionalExpression *elem ;
    FOREACH_ARRAY_ITEM(_conditional_expressions,i,elem) {
        if (!elem) continue ;
        VhdlVariableSlice::SetBranchId(i) ; // Viper# 6546
        elem->InitializeForVariableSlice() ;
    }
}

void VhdlSelectedVariableAssignmentStatement::InitializeForVariableSlice()
{
    if (_expr) _expr->InitializeForVariableSlice() ;
    if (_target) _target->InitializeForVariableSlice() ;
    if (_expr_type) _expr_type->InitializeForVariableSlice() ;
    unsigned i ;
    VhdlSelectedExpression *expr ;
    FOREACH_ARRAY_ITEM(_selected_exprs,i,expr) {
        if (!expr) continue ;
        VhdlVariableSlice::SetBranchId(i) ; // Viper# 6546
        expr->InitializeForVariableSlice() ;
    }
}
#endif
