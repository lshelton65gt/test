/*
 *
 * [ File Version : 1.170 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"

#include "VhdlScope.h"
#include "VhdlIdDef.h"
#include "VhdlUnits.h"
#include "VhdlDeclaration.h" // for default binding resolving
#include "vhdl_file.h"
#include "VhdlName.h"
#include "VhdlCopy.h"
#include "VhdlMisc.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VhdlScope::VhdlScope(VhdlScope *upper, VhdlIdDef *owner_id, unsigned transparent)
  : _upper(upper),
    _this_scope(0),
    _use_clauses(0),
    _use_clause_units(0),
    _extended_decl_area(0),
    _predefined(0),
    _using(0),
    _used_by(0),
    _owner_id(owner_id),
    _hidden_ids(0),
    _transparent(transparent), // transparent scope flag for procedural loop statements : VIPER 2597
    _is_process_scope(0),
    _is_subprogram_scope(0),
    _wait_not_allowed(0),
    _has_wait_statement(0),
    _has_impure_access(0),
    _has_local_file_access(0)
{
    // Make a cross-link between the scope and the 'owner_id' :
    //
    // Set the created scope on the 'owner' id,
    // so that we have the scope visible from the id
    // This is needed it gets referenced inside the statement this scope creates
    if (owner_id) owner_id->SetLocalScope(this) ;

    // Set the 'subprogram_scope' flag :
    if (owner_id && owner_id->IsSubprogram()) SetIsSubprogramScope() ;
}

VhdlScope::~VhdlScope()
{
    // Identifiers in the scope are owned by their tree nodes that declared them
    // So DON'T delete them !

    // BUT : Operator Id's / Anonymous types are created on the fly (by type declarations),
    // and are not owned by a tree node !!
    // Thats why we store them in a separate list (Array).
    // Delete these guys now (if they are there).
    unsigned i ;
    VhdlIdDef *predef ;
    FOREACH_ARRAY_ITEM(_predefined, i, predef) {
        if (!predef) continue ;
        if (predef->IsAlias()) {
            VhdlName* target_name = predef->GetAliasTarget() ;
            delete target_name ;
            predef->SetTargetName(0) ;
        }
        delete predef ;
    }
    delete _predefined ;

    // Delete the scope Map's that I own
    delete _this_scope ;
    delete _use_clauses ;

    // Free char*'s in value fields of the VhdlIdDef->char* _use_clause_units :
    MapIter mi ;
    char *name ;
    FOREACH_MAP_ITEM(_use_clause_units, mi, 0, &name) {
        Strings::free(name) ;
    }
    // now delete the Map.
    delete _use_clause_units ;

    // Delete hidden ids Map
    delete _hidden_ids ;

    // Delete the dependency lists.
    // First of all, let the scopes that I am using know that I am no longer there :
    if (_using) {
        SetIter si ;
        VhdlScope *other_scope ;
        FOREACH_SET_ITEM(_using, si, &other_scope) {
            NoLongerUsing(other_scope) ;
        }
        delete _using ;
        _using = 0 ;
    }

    // Let the scopes that rely on 'this' know that we are no longer there.
    // Now, if somebody still relies on me, gotta delete them too.
    // Need to delete their design unit actually, since we don't delete sub-trees.
    if (!_upper && _used_by && _used_by->Size()) {
        SetIter si ;
        VhdlScope *other_scope ;
        VhdlIdDef *this_design_unit = GetContainingDesignUnit() ;
        VERIFIC_ASSERT(this_design_unit) ;
        VhdlIdDef *other_design_unit ;
        FOREACH_SET_ITEM(_used_by, si, &other_scope) {
            other_scope->NoLongerUsing(this) ; // Remove me from their list.
            if (other_scope->Upper()) continue ; // no need to delete unit with upper

            other_design_unit = other_scope->GetContainingDesignUnit() ;
            if (!other_design_unit) continue ;
            if (other_design_unit == this_design_unit) continue ; // no need to delete myself (again)

            // Exception : If we are presently adding unit to its owner, it
            // would be using itself in an old form ; Could happen when overwriting
            // std.standard or when "use work.all" is there.
            // Should not destroy the new form ('unit').
            // Test by checking if the unit has an owner.
            // This is a delicate check, since the primary unit of this 'other_design_unit' might be being destroyed right now.
            if (other_design_unit->GetDesignUnit() && !other_design_unit->GetDesignUnit()->HasOwner()) continue ;

            // Give warning and destroy the unit that is now out of date
            // Viper 5286: changed the warning to Info
            other_design_unit->Info("re-analyze unit %s since unit %s is overwritten or removed",
                other_design_unit->Name(), this_design_unit->Name()) ;

            // Delete it
            delete other_design_unit->GetDesignUnit() ;
        }
    }

    delete _used_by ;
    _used_by = 0 ;

    if (_owner_id) _owner_id->SetLocalScope(0) ;
    if (this == VhdlTreeNode::_present_scope) VhdlTreeNode::_present_scope = 0 ;
}
void
VhdlScope::Push(VhdlIdDef *owner, unsigned transparent_scope)
 {
    // Create a new scope and set as sub-region of present scope
    // VIPER 2597 : Create transparent scope if 2 nd argument is 1
    VhdlTreeNode::_present_scope = new VhdlScope(VhdlTreeNode::_present_scope, owner, transparent_scope) ;
}

VhdlScope *
VhdlScope::Pop()
{
    // Pop to the upper region. Return the one we poped out of
    VhdlScope *tmp = VhdlTreeNode::_present_scope ;
    // VIPER #3174 : Check declared identifiers to issue error for missing full type definition
    if (tmp) tmp->CheckIncompleteTypeDefs() ;
    VhdlTreeNode::_present_scope = VhdlTreeNode::_present_scope->_upper ;
    return tmp ;
}

void
VhdlScope::SetOwner(VhdlIdDef *owner)
{
    // Make a cross-link between the scope and the 'owner_id' :
    //
    // Set the created scope on the 'owner' id,
    // so that we have the scope visible from the id
    // This is needed it gets referenced inside the statement this scope creates
    _owner_id = owner ;
    if (owner) owner->SetLocalScope(this) ;
}
void
VhdlScope::SetUpper(VhdlScope *parent)
{
    _upper = parent ;
}

Map *
VhdlScope::DeclArea() const
{
    return _this_scope ;
}

Map *
VhdlScope::ExtendedDeclArea() const
{
    return (_extended_decl_area) ? _extended_decl_area->DeclArea() : 0 ;
}

VhdlScope *
VhdlScope::Upper() const
{
    return _upper ;
}

VhdlScope *
VhdlScope::TopScope() const
{
    if (_upper) return _upper->TopScope() ;
    return const_cast<VhdlScope*>(this) ; // reached top scope.
}

unsigned
VhdlScope::IsContainedIn(const VhdlScope *scope) const
{
    // Checks whether this scope is defined inside scope "scope"
    if (!scope) return 0 ;
    const VhdlScope *curr_scope = this ;
    while (curr_scope) {
        if (curr_scope == scope) return 1 ;
        curr_scope = curr_scope->Upper() ;
    }
    return 0 ;
}
void
VhdlScope::DeclarePredefined(VhdlIdDef *id)
{
    // All identifiers are owned by the parse-tree,
    // except for predefined operators and anonymous types.
    // So we need to store them separately, to be able to
    // delete them once the scope gets deleted.
    // So they need to be stored in scope via this
    // special routine.
    // We also don't have to check collision with existing
    // declarations, since there will not be any.

    if (!_predefined) _predefined = new Array(16) ; // Give it some space
    _predefined->InsertLast(id) ;

    // Insert in the normal scope
    if (!_this_scope) _this_scope = new Map(STRING_HASH,47) ;

    (void) _this_scope->Insert(id->Name(),id,0,1) ;

#ifdef VHDL_ID_SCOPE_BACKPOINTER
    // Set the owning scope of the identifier :
    id->SetOwningScope(this) ;
#endif
} 

unsigned
VhdlScope::Declare(VhdlIdDef *id)
{
    // Store an identifier in the scope
    if (!id) return 0 ;

    // Predefined operators / anonymous types are not part of the parse tree.
    // They need to be declared with 'DeclarePredefined()'
    VERIFIC_ASSERT(!id->IsPredefinedOperator() && !id->IsAnonymousType()) ;

    // Declare a identifier in a scope.
    // This will make it directly visible anywhere after this moment of insertion,
    // until the end of the 'declarative region' (that's where the scope pops).

    /////////////////////////////////////////////////////////////////////////////////
    //
    // Development note about library and design_unit identifiers in a scope :
    // Design units and library identifiers are a special case in VHDL.
    // They can never override an otherwise directly visible identifier,
    // which means that they have low priority of being visible.
    // Still, they have higher priority (thus don't behave the same as) 'use-clause' included identifiers.
    // Also, a design unit identifier will overwrite a locally declared library identifier,
    // but not the other way around. So design unit declaration is higher prio than library.
    // This special behavior calls for special treatment in the scope for them.
    //
    // Over the years, we have gone back and forth storing declared library and
    // design unit identifiers in the _this_scope, or in the _use_clauses table,
    // or (for design units) don't insert them at all, but use the fact that the
    // are the 'owner' of a highest level scope. Each time, we had to change the
    // code in the scope search routines (FindAll etc), and in use-clause search
    // routines, and this routine here (VhdlScope::Declare()).
    // Bug reports kept showing up until we reached this solution :
    //
    // Finally, I think we have the cleanest and correct solution by declaring
    // both an library and a design_unit identifier here in the scope.
    // The only adjustments made to reflect the special behavior they have is done :
    //    (1) Here in VhdlScope::Declare, where we let anyone override a lib/designunit
    //        id (by removing it from the local scope), and
    //    (2) in VhdlSelectedName::FindObjects() where we make sure NOT to include
    //        library and design unit names into the list of identifiers made visible, and
    //    (3) in VhdlScope::AddDeclRegion(), when we make library identifiers
    //        invisible that were declared before a secondary unit was linked to
    //        a primary unit (thereby extending the declartive region, which could cause collisions)
    // This way, they are made invisible when a local declaration overrides them,
    // and they are invisible when a use-clause includes the scope of a design unit.
    // This I believe is as much as needed to be done (and nothing more) to make them comply
    // with all scoping rules in VHDL.
    // Advice : don't change this any more, because there is no better way to do it.
    //
    /////////////////////////////////////////////////////////////////////////////////

    // Check for collision with existing declarations
    // It's enough to just look at the first identifier.
    // Since complete homograph checking is done
    // after the profiles are set, overloadable identifiers
    // will have to be passed-in right now any way.
    // Find if the identifier is already declared in this region. ('this' and 'extended' area)
    VhdlIdDef *exist = FindSingleObjectLocal(id->Name()) ;

    // There are likely going to be many identifiers, so
    // give the scope some reasonable size, (for a few dozen items)
    // to prevent many rehashes. Don't go overboard : There are also
    // many small scopes (subprograms, record type, etc).
    if (!_this_scope) _this_scope = new Map(STRING_HASH,47) ;

    // Always error out on any existing one, except when its overloadable, or a unknown type.
    if (exist) {
        // VIPER #7725 : Overloading of interface subprogram is not allowed
        if (((exist->IsSubprogram() && !exist->IsGeneric()) || exist->IsEnumerationLiteral()) &&
            (id->IsSubprogram() || id->IsEnumerationLiteral())) {
            // Homograph checking for overloadable identifiers is done
            // separately, after their return profile is determined.
            // So, for now, accept them
        } else if (id->IsLibrary()) {
            // Try to declare a library with a name that's already declared.
            // Ignore it, library identifiers are always overwritten by other declarations.
            return 0 ;
        } else if (exist->IsLibrary()) {
            // Ignore the existing library identifier. Other declarations always override a library.
            Undeclare(exist) ;
        } else if (exist->IsDesignUnit()) {
            // A design unit is always overwritten
            Undeclare(exist) ;
        } else if (id->IsDesignUnit()) {
            // Something hard is already in place. design unit name will not be visible.
            return 0 ;
        } else if (exist->IsConstant() && id->IsConstant() && !exist->IsGeneric() && !exist->HasInitAssign() && _extended_decl_area) {
            // Definition of a deferred constant that was declared earlier.
            //
            // LRM 4.3.1.1 states that this may only happen in package body
            // so that is why we check for extended region.

            // Create a loop between the two ids, and don't declare the new one.
            id->SetDecl(exist) ;
            exist->SetDecl(id) ;
            return 0 ;
        } else if (exist->IsIncompleteType() && id->IsType()) {
            // Definition of incomplete type.
            // undeclare the existing one :
            Undeclare(exist) ;
            // accept the new one
            // There might be pointers (designated type of access type) to the incomplete type already...
            // To re-direct these to the defined type, declare the unknown type.
            exist->DeclareIncompleteType(id) ;
        } else {
            id->Error("%s is already declared in this region", exist->Name()) ;
            return 0 ;
        }
    }

    // Allow multiple declarations (non-homographs) with the same name
    (void) _this_scope->Insert(id->Name(),id,0,1) ;

#ifdef VHDL_ID_SCOPE_BACKPOINTER
    // Set the owning scope of the identifier :
    id->SetOwningScope(this) ;
#endif

    return 1 ; // Identifier entered in scope, return 1
}

// Declare design defined label ids in non-transparent scope
unsigned
VhdlScope::DeclareLabel(VhdlIdDef *id)
{
    if (!id || !id->IsLabel()) return 0 ;

    // VIPER 2597 : Do not declare label identifier in a transparent scope
    // Traverse upwards until a non-transparent scope is found to declare it.
    if (_transparent && _upper) {
        return _upper->DeclareLabel(id) ;
    }

    return Declare(id) ; // Declare it in this scope
}

void
VhdlScope::UndeclareLabel(VhdlIdDef *id)
{
    if (!id || !id->IsLabel()) return ;

    if (_transparent && _upper) {
        _upper->UndeclareLabel(id) ;
        return ;
    }

    Undeclare(id) ;
    return ;
}

void
VhdlScope::Undeclare(VhdlIdDef *id)
{
    if (!id) return ;

    // Remove this id from the scope
    MapItem *mi ;
    VhdlIdDef *exist ;
    FOREACH_SAME_KEY_MAPITEM(_this_scope, id->Name(), mi) {
        exist = (VhdlIdDef*)mi->Value() ;
        if (exist==id) {
            // Remove from this scope :
            (void) _this_scope->Remove(mi) ;

#ifdef VHDL_ID_SCOPE_BACKPOINTER
            // Unset the owning scope of the identifier :
            id->SetOwningScope(0) ;
#endif
            return ;
        }
    }
    // Mmm. If not in immediate scope. FIX ME : Check extended area ??
}

unsigned
VhdlScope::IsDeclaredHere(VhdlIdDef *id, unsigned look_in_upper) const
{
    if (!id) return 0 ;

    // Since the scope contains multiple ids by same key (name) we
    // have to scan all same-key id's in this scope.
    // 'id' itself is in the 'value' slot of a Map Item.

    MapItem *mi ;
    FOREACH_SAME_KEY_MAPITEM(_this_scope, id->Name(), mi) {
        if (id == mi->Value()) return 1 ;
    }

    if (look_in_upper) {
        if (_upper && _upper->IsDeclaredHere(id, look_in_upper)) {
            return 1 ;
        }
        // Historically this routine did not include extended.
        if (_extended_decl_area && _extended_decl_area->IsDeclaredHere(id, look_in_upper)) {
            return 1 ;
        }
    }

    return 0 ;
}

// Old way to store use clauses in a scope :
// Map VhdlScope::_use_clauses contains VhdlIdDefs that are 'potentially' visible (LRM 10.4).
// This Map is filled by "VhdlScope::AddUseClause(Set &items)"
// We hardly ever use _use_clauses any more (since _use_clause_units was introduced),
// but old restored files can still have them, and we still use them for architecture id's in block configurations.
// This old way also uses _hidden_ids, which is no longer used in new way.
void
VhdlScope::AddUseClause(Set &items)
{
    // Old way of storing use clauses : add entire set of included identifiers :

    // Start use_clause table with a good size
    if (!_use_clauses) _use_clauses = new Map(STRING_HASH, 383) ;

    SetIter si ;
    VhdlIdDef *id ;
    MapItem *exist_item ;
    VhdlIdDef *exist ;
    FOREACH_SET_ITEM(&items, si, &id) {
        // Don't include libraries in use clauses. Side-effect of how we store library identifiers
        if (id->IsLibrary()) continue ;

        // Pick up the first existing definition :
        exist_item = _use_clauses->GetItem(id->Name()) ;
        if (!exist_item) {
            // This is fine. No existing declaration by this name :
            // Push into the use_clause table :
            (void) _use_clauses->Insert(id->Name(),id,0,1) ;
            continue ;
        }
        // The identifier is in the value field of the item
        exist = (VhdlIdDef*) exist_item->Value() ;

        // Could happen that a identifier is included multiple times (similar use clause mentioned twice).
        // In that case, don't insert again.
        if (exist==id) continue ; // no need to insert same identifier multiple times.

        if ((id->IsSubprogram() || id->IsEnumerationLiteral()) &&
            (exist->IsSubprogram() || exist->IsEnumerationLiteral())) {
            // Both are overloadable.
            // LRM 10.4(b) states that both will be directly visible.
            // Overloading will determine which one fits (when referred)
            (void) _use_clauses->Insert(id->Name(),id,0,1) ;
            continue ;
        }

        // Here, id is not overloadable, and there is an existing (therefor homograph)
        // definition about to be loaded.

        // LRM 10.4 (b) dictates that we should NOT make identifiers visible if
        // they have the same name and are homographs.
        // A full check for all homographs should actualy be done every time we refer to any
        // identifier anywhere, (FindAllIncluded) but that is very expensive (runtime-wise).
        // We leave that, and the homograph resolving of 10.4(a) up to type resolving.

        // But at least we can do a fast check here, which will catch 10.4(b) violations
        // at a single scope level (all use clauses done within an immediate scope level),
        // and for non-overloadable operators.
        // That is most common problem and reported in issue 1555 and others.
        // We store the name as being 'hidden' in a special table :

        if (!_hidden_ids) _hidden_ids = new Set(STRING_HASH, 3) ;
        (void) _hidden_ids->Insert(id->Name()) ; // Insert id name which should be hidden from now on.  No need to Strings::save it.

        // Insert the (homograph) identifier any way, so that we can give
        // a better report later (in FindAllIncluded/FindSingleObjectIncluded).
        (void) _use_clauses->Insert(id->Name(),id,0,1) ;
    }
}

// New way of storing use clauses :
// This Map is filled by "VhdlScope::AddUseClause(VhdlIdDef *prefix, const char *suffix, VhdlTreeNode *from)"
// Store the containers of 'potentially visible identifiers'. Not the identifiers themselves.
// Map (VhdlIdDef*)->(char*) represents all use clauses in this scope "use (lib.)id.all" creates id->NULL entry, "use (lib.)id.name" creates an id->name entry.
// "use (lib.)prefix.suffix". for "use prefix.all" store suffix==NULL.
void
VhdlScope::AddUseClause(VhdlIdDef *prefix, const char *suffix, VhdlTreeNode *from)
{
    // New way of storing use clauses :
    // Add use clause "use (lib.)prefix.suffix". for "use prefix.all" store suffix==NULL.
    // Store prefix as (VhdlLibraryId*) or (VhdlPackageId*) and suffix as (saved) (char*).
    if (!prefix) return ;
    if (from && !prefix->IsPackage() && !prefix->IsLibrary()) {
        if (suffix) {
            from->Error("prefix of %s in the use clause must be library or package identifier", suffix) ; // Viper #4486
        } else {
            from->Error("prefix of %s in the use clause must be library or package identifier", "all") ; // Viper #3625
        }
    }

    // LRM selected name check :
    // if suffix is given, and prefix is a library, then we should error out if that unit is not found in that library :
    if (suffix && prefix->IsLibrary()) {
        VhdlLibrary *lib ;
        if (Strings::compare(prefix->Name(),"work")) {
            // use present work library
            lib = vhdl_file::GetWorkLib() ;
        } else {
            // Find the library by name
            lib = vhdl_file::GetLibrary(prefix->Name(),1) ;
        }
        VERIFIC_ASSERT(lib) ; // we would have created it if not there.
        // Now find the primary unit 'name' in this library :
        VhdlPrimaryUnit *pu = lib->GetPrimUnit(suffix) ;
        // VIPER 5217 search in verilog
        if (!pu) pu = vhdl_file::GetVhdlUnitFromVerilog(lib->Name(), suffix) ;
        if (!pu) {
            if (from) from->Error("'%s' is not compiled in library %s", suffix, prefix->Name()) ;
        }
    }

    // Start use_clause_unit table with a good size
    if (!_use_clause_units) _use_clause_units = new Map(POINTER_HASH, 5) ; // most scopes include less than 5 use clauses.

    // If prefix is already added to the extended scope, then there is no need to store it here.
    if (_extended_decl_area) {
        Map *extended_map = _extended_decl_area->GetUseClauseUnits() ;
        MapItem *item = (extended_map) ? extended_map->GetItem(prefix) : 0 ;
        if (item && !item->Value()) {
            // Here, there is a "use prefix.all" clause in the extended decl area.
            // no need to insert it here in the derived scope :
            return ;
        }
    }

    // Check if this one is already added :
    MapItem *item = _use_clause_units->GetItem(prefix) ;
    if (item) {
        // previous 'prefix' included.
        char *name = (char*)item->Value() ;
        if (!name) {
            // Already included "prefix.all". No need to include again.
            return ;
        } else if (!suffix) {
            // included something like "prefix.suffix" but now have "prefix.all" included.
            // Remove existing entry :
            Strings::free(name) ;
            (void) _use_clause_units->Remove(item) ; // remove from table
        }
    }

    // Register the dependency (of this scope using scope of the prefix identifier's scope :
    VhdlScope *prefix_scope = prefix->LocalScope() ;
    if (prefix_scope) {
        Using(prefix_scope) ;
    }

#if 0
// RD: 8/2007: cannot do this quick fix. VIPER 3456.
// We should only include identifiers of the type. Not .all as we do here.
// So we have to solve this problem when we pick up identifiers (with FindAllIncluded()).
    // Solve VIPER 2962 / 3304 / 3389 / 3242 here.
    // If use clause is "somepackage.typename" then ModelSim makes the operators (and enumeration values) of the type visible too (counter to what LRM says) !
    // Mimic that here with the following rule :
    // If a type is explicitly included from some scope, then instead turn it into a .all include clause.
    // This is very powerfull (solves many VIPER issues), and not LRM compliant, but neither is ModelSim's rule.
    if (suffix && prefix_scope) {
        VhdlIdDef *suffix_id = prefix_scope->FindSingleObjectLocal(suffix) ;
        if (suffix_id && suffix_id->IsType()) {
            suffix = 0 ; // essentially : interpret as "use <prefix>.all" instead of "<prefix>.suffix".
        }
    }
#endif

    // Include this one (force insert to support "use prefix.suffix1" and "use prefix.suffix2").
    // VIPER #4195 : always down-case the suffix, except when it's an extended name
    // or a character literal :
    char *suffix_name = 0 ;
    if (suffix) {
        suffix_name = Strings::save(suffix) ;
#ifdef VHDL_PRESERVE_ID_CASE
        if (*suffix_name != '\\' && *suffix_name != '\'') suffix_name = Strings::strtolower(suffix_name) ;
#else
        // If this is declaration of an operator, we need to down-case it
        // for scoping to work ("abs" and "ABS" are the same).
        if (*suffix_name == '"') suffix_name = Strings::strtolower(suffix_name) ;
#endif
    }

    if (suffix_name && *suffix_name == '\"' && *(suffix_name + Strings::len(suffix_name) - 1) == '\"' && !VhdlNode::IsValidOperator(suffix_name)) {
        // Viper 5249. Add unquoted string
        // this implies it is a quoted string. Unquote it to get the function name
        char *unquote_str = Strings::save(suffix_name + 1) ;
        unquote_str[Strings::len(unquote_str) - 1] = '\0' ;
        (void) _use_clause_units->Insert(prefix, unquote_str, 0, 1 /*force insert*/) ;
        Strings::free(suffix_name) ;
        suffix_name = unquote_str ;
    } else {
        (void) _use_clause_units->Insert(prefix, suffix_name, 0, 1 /*force insert*/) ;
    }

    // Viper 7458: Issue error if the suffix name is not found in the package scope.
    if (prefix->IsPackage() && prefix_scope && suffix_name) {
        VhdlIdDef *suffix_id = prefix_scope->FindSingleObjectLocal(suffix_name) ;
        if (!suffix_id) {
            if (from) from->Error("'%s' is not compiled in package %s", suffix, prefix->Name()) ;
        }
    }
    // Note that if prefix identifier is a Library identifier, then no dependency gets registered.
    // That is because we did not store any prim unit pointers, and FindIncluded() routines
    // will look in the library when needed.
}

void
VhdlScope::AddDeclRegion(VhdlScope *spec_scope)
{
    // Set the declarative region from an entity (or
    // other primary unit) into present scope (for a
    // secondaty unit.
    //
    // We also use this to implement scope extension as needed
    // for block/component configurations.
    //
    // We just set a pointer to that area, and make sure
    // when we look in 'local' scope or in the scope in
    // general, that we don't forget to look in the
    // extended_decl_area too. It is part of our
    // declarative region. Whatever that means.

    _extended_decl_area = spec_scope ;

    // LRM 11.2 : library clauses in secondary unit should not have any effect
    // if the primary unit already has the same library clause.
    // Most importantly : they should not mask any declaration made in the
    // primary unit. Check that here (where for the first time, the
    // secondary unit is linked to the primary unit)
    // Happens in vi-suite test /src/ch10/sc03/p005/s010101.vhd
    if (_owner_id && _owner_id->IsDesignUnit() && _extended_decl_area) {
        MapIter mi ;
        VhdlIdDef *id ;
        FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
            if (id->IsLibrary()) {
                if (_extended_decl_area->FindSingleObjectLocal(id->Name())) {
                    Undeclare(id) ;
                }
            }
        }
    }

    // Also label that 'this' scope relies on 'spec_scope'.
    Using(spec_scope) ;
}

VhdlIdDef *
VhdlScope::FindSingleObject(const char *name) const
{
    // LRM 10.4 rules out that directly visible identifiers
    // can never be masked by a use-clause included identifier.
    // So, we first have to decend into the locally declared
    // identifier scopes for directly visible identifiers.
    VhdlIdDef *id = 0 ;

    // Look up in the scope :
    const VhdlScope *runner = this ;
    while (runner) {
        id = runner->FindSingleObjectLocal(name) ;
        // The inner-most declaration wins when we look through the scope tree.
        // so bail out when we find it here :
        if (id) return id ;

        // Otherwise, go one scope level up :
        runner = runner->Upper() ;
    }

    // The scoping rules in the presence of block/component configurations are so
    // confusing that we cannot make both this issues (1377 and 1442) work properly
    // without special code in the ComponentSpec constructor AND first looking at
    // the use-clause included identifiers and THEN into the extended scopes.
    //
    // Issue 2734 forced us to look at this again. Need to look through extended scopes first.
    // Turns out that issue 1442 does NOT pass through ModelSim.
    // So, look through extended scopes first, and THEN through included scopes.

    // look through the extended scopes
    runner = this ;
    while (runner && runner->Upper()) {
        id = (runner->_extended_decl_area) ? runner->_extended_decl_area->FindSingleObject(name) : 0 ;
        // The inner-most declaration wins when we look through the scope tree.
        // so bail out when we find it here :
        if (id) return id ;
        // Otherwise, go one scope level up :
        runner = runner->Upper() ;
    }

    // look in the included scopes :
    runner = this ;
    while (runner) {
        id = runner->FindSingleObjectIncluded(name) ;
        // The inner-most declaration wins when we look through the scope tree.
        // so bail out when we find it here :
        if (id) return id ;

        // Otherwise, go one scope level up :
        runner = runner->Upper() ;
    }

    return id ;
}

VhdlIdDef *
VhdlScope::FindAll(const char *name, Set &result) const
{
    // Accumulate all potentially visible VhdlIdDefs by this name,
    // either directly visible or 'potentially visible' (by use-clause inclusion).
    // Follow the complicated LRM 10.3/4/5 rules :

    // I think this is more-or-less it :
    //
    //   1)Two directly visible declarations that are homographs
    //     in the same (level) immediate scope are not allowed.
    //   2)Two directly visible declarations that are homograph :
    //     eliminate the outer-visible one.
    //   3)Homographs with one directly visible, and one included
    //     by some use clause at any level : eliminate the use-clause
    //     one(s).
    //   4)Homographs with both included by use-clause : all are
    //     eliminated
    //   5)Homographs from 'extended_area' (for configurations) are blocked
    //     by use-clause included identifiers. Actuall all are eliminated.
    //
    // At this point, rule (1) is enforced (in VhdlScope::Declare).
    // Rule 2 and 3 can be done here : ensure that we never insert
    // homographs. But this is VERY time consuming. Instead, allow
    // all non-overloadable same-name identifiers in the 'result' set,
    // and trust overloading to eliminate them when needed.
    // (specifically in operator, function and identifier overloading.
    //
    //  Rule 4 and 5 are not enforced at this point.
    //  We could maybe do this below....
    VhdlIdDef *id = 0 ;

    // Look up in the scope :
    const VhdlScope *runner = this ;
    while (runner) {
        id = runner->FindAllLocal(name, result) ;
        // The inner-most declaration wins when we look through the scope tree.
        // so bail out when we find it here :
        if (id && !id->IsSubprogram() && !id->IsEnumerationLiteral()) {
            return id ;
        }

        // Otherwise, go one scope level up :
        runner = runner->Upper() ;
    }

    // VIPER issues 1863, 2397, 1442 and 2734 seem to indicate that
    // we should FIRST look in extended decl areas, and THEN in included scopes.
    // Even though these VIPER issues only affect implicit homographs (components etc),
    // which go through the FindSingleObject() routine,

    // Look in the extended areas
    // (except for the already done upper-extended decl region)
    runner = this ;
    while (runner && runner->Upper()) {
        if (runner->_extended_decl_area) {
            id = runner->_extended_decl_area->FindAll(name, result) ;
            // The inner-most declaration wins when we look through the scope tree.
            // so bail out when we find it here :
            if (id && !id->IsSubprogram() && !id->IsEnumerationLiteral()) {
                return id ;
            }
        }
        // Otherwise, go one scope level up :
        runner = runner->Upper() ;
    }

    // Now look in the included scopes :
    runner = this ;
    while (runner) {
        id = runner->FindAllIncluded(name, result) ;
        // For efficiency :
        // Bail out when we find a non-overloadable one :
        if (id && !id->IsSubprogram() && !id->IsEnumerationLiteral()) {
            return id ;
        }
        // No need to bail out here. Included id's don't have something like hiding.
        runner = runner->Upper() ;
    }

    return id ;
}

VhdlIdDef *
VhdlScope::FindAllIncluded(const char *name, Set &result) const
{
    if (!name) return 0 ;
    // Accumulate all potentially visible VhdlIdDefs by this name,
    // which are 'included' (with use-clauses)
    // at exactly this (scope) point. (Does not go up in scope hierarchy).
    // It DOES include the primary-unit extended decl region from a sec unit.
    MapItem *mitem ;
    VhdlIdDef *id = 0 ;
    unsigned num_of_direct_matches = result.Size() ; // number of matches in the result before we considered use clauses

// Old way : _use_clauses contains VhdlIdDefs that are 'potentially' visible (LRM 10.4).
// This Map is filled by "VhdlScope::AddUseClause(Set &items)"
// We hardly ever use _use_clauses any more (since _use_clause_units was introduced),
// but old restored files can still have them, and we still use them for architecture id's in block configurations.
// This old way also uses _hidden_ids, which is no longer used in new way.

    // Add the current matching identifiers to the result :
    unsigned hidden = 0 ;
    unsigned first = 1 ;
    FOREACH_SAME_KEY_MAPITEM(_use_clauses, name, mitem) {
        // Get the identifier :
        id = (VhdlIdDef*)mitem->Value() ;
        if (!id) continue ;

        // Enforce rule (4) above.
        // If Homographs were inserted during scope inclusion, All should be invisible.
        // Check if this identifier is 'hidden'. Do this check only once :
        if (first && (_hidden_ids && _hidden_ids->GetItem(name))) {
            // Flag that this is a hidden identifier.
            hidden = 1 ;
            // Issue 2377 : if there were already identifiers in the 'result',
            // then we should truly 'hide' the ones included here.
            // Simple exit from this routine ASAP:
            if (result.Size()) return 0 ;
        }
        if (hidden) {
            // Otherwise, issue 1555 on hidden identifiers :
            // Error out (that all is hidden), but continue looping and inserting identifier(s),
            // to avoid later errors like "undeclared identifier".
            if (first) {
                VhdlIdDef tmp(0) ; // Temporary node to get 'present' line/file info
                tmp.Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", name) ;
                // Optional extra info : report the hidden identifiers :
                id->Info("%s is declared here",id->Name()) ;
            } else {
                id->Info("another match is here") ;
            }
        }

        // Insert it.
        (void) result.Insert(id) ;
        // Flag that we go to the next iteration :
        first = 0 ;
    }
// end of old way

    // New _use_clause_units :
    // This Map is filled by "VhdlScope::AddUseClause(VhdlIdDef *prefix, const char *suffix, VhdlTreeNode *from)"
    // Store the containers of 'potentially visible identifiers'. Not the identifiers themselves.
    // Map (VhdlIdDef*)->(char*) represents all use clauses in this scope "use (lib.)id.all" creates id->NULL entry, "use (lib.)id.name" creates an id->name entry.
    // "use (lib.)prefix.suffix". for "use prefix.all" store suffix==NULL.
    MapIter mi ;
    char *suffix ;
    VhdlIdDef *prefix_id ;
    VhdlIdDef *tmp_id ;
    // Keep track of 'hidden' identifiers (but we do not need the olde '_hidden_ids' table.
    // We can do this on the fly now that we have all use clauses here.
    first = 1 ;
    FOREACH_MAP_ITEM(_use_clause_units, mi, &prefix_id, &suffix) {
        if (!prefix_id) continue ;

        // Check suffix name if there : Not so fast ! VIPER 3456 : intepretation of "use unitname.typename" :
        // we should not right away rule out suffix mismatches. Do check later (below).
        // if (suffix && !Strings::compare(suffix, name)) continue ; // if suffix does not match, skip this one.

        // Note that multiple tmp_id's per entry (per use clause) will only be present if they were overloadable.
        // Non-overloadable id's (certain homographs) will always be by themselves.
        // So insert all matching identifiers, and do homograph check one per use clause entry.
        // reset tmp_id.
        tmp_id = 0 ;
        // Library prefix is special :
        if (prefix_id->IsLibrary()) {
            // LRM 10.4 (a) : Issue 2377 : if there were already identifiers in the 'result' (not included),
            // and the identifier we attempt to include is certainly a homograph (not overloadable)
            // then we should truly 'hide' the included one.
            // We know for sure that this id will be a primary unit, so it is certainly a homograph.
            if (num_of_direct_matches) continue ;

            // If there is a suffix (use libname.someunit") then mismatch means that 'name' is not visible :
            if (suffix && !Strings::compare(suffix, name)) continue ; // if suffix does not match, skip this one.

            // Otherwise, find the selected unit in this library :
            VhdlLibrary *lib ;
            if (Strings::compare(prefix_id->Name(),"work")) {
                // use present work library
                lib = vhdl_file::GetWorkLib() ;
            } else {
                // Find the library by name (create library if needed, so that we can look into on-disk units)
                lib = vhdl_file::GetLibrary(prefix_id->Name(),1) ;
            }
            VERIFIC_ASSERT(lib) ; // we would have created it if not there.
            // Now find the primary unit 'name' in this library :
            VhdlPrimaryUnit *pu = lib->GetPrimUnit(name) ;
            // Get the identifier of this primary unit
            tmp_id = (pu) ? pu->Id() : 0 ;

            // Register dependency now that we actually use this identifier..
            // Oops. Cannot do that here, in a 'const' function. Do the 'Using' at the caller level.
            // if (tmp_id) Using(tmp->LocalScope()) ;

            // Insert it in return array :
            (void) result.Insert(tmp_id) ;
        } else {
            // Otherwise, find 'name' in the scope of this prefix_id :
            VhdlScope *prefix_scope = prefix_id->LocalScope() ;
            //if (prefix_id->IsPackageInst()) prefix_scope = prefix_id->GetUninstantiatedPackageScope() ;
            if (!prefix_scope) continue ;

            // Find the identifier(s) in this local scope (only DeclArea) :
            FOREACH_SAME_KEY_MAPITEM(prefix_scope->DeclArea(), name, mitem) {
                // Get the identifier :
                tmp_id = (VhdlIdDef*)mitem->Value() ;
                if (!tmp_id) continue ;
                // library and design-unit objects are stored in the scope, but not considered 'declared' there.
                // FIXME :
                // Viper : 7408: This removal of tmp_id was commented out probably for 5918
                // Not sure why this was done. Enabling the code again. tmp_id which is a package
                // with no upper scope should be removed.
                if (tmp_id->IsLibrary() || tmp_id->IsDesignUnit()) {
                    VhdlScope *p = tmp_id->IsPackage() ? tmp_id->LocalScope(): 0 ;
                    if (!p || !p->Upper()) {
                        // VIPER #5918 : If it is nested design unit id, consider it
                        tmp_id = 0 ;
                        continue ;
                    }
                }

                // Check if the name matches the 'suffix' (if any).
                if (suffix && !Strings::compare(suffix, name)) {
                    // Normally we would 'continue' (or break) here because suffix name does not match requested name.
                    // But VIPER 3456 : (intepretation of "use unitname.typename") related VIPERs : 2962 / 3304 / 3389 / 3242
                    // We think that ModelSim makes all operators/enumeration literals visible that have the type.
                    // So, lets check if this identifier is of the type that is made visible with the suffix.

                    // VIPER 3536 : need to import ALL built-in operators and enumeration values created by the type in the suffix :
                    // Check first if we are dealing with a built-in operator :
                    // tmp_id may also be a re-written PredefinedOperator. Vhdl 2008 testsuite 12.4
                    // Units of physical type also need to be visible. Vhdl 2008 testsuite 12.4
                    if (tmp_id->IsPhysicalUnit() || tmp_id->IsOverWrittenPredefinedOperator() || tmp_id->IsPredefinedOperator() || tmp_id->IsEnumerationLiteral()) {
                        // Get the suffix (check if it is a type)
                        VhdlIdDef *type_id = prefix_scope->FindSingleObjectLocal(suffix) ;
                        if (type_id && type_id->IsType()) {
                            // Suffix is indeed a type.
                            // Check if this type is responsible for creation of this predefined operator :
                            // Check return type, or an argument type (argument type checking needed for VIPER 3536:
                            if (type_id->TypeMatch(tmp_id->Type()) ||
                                (type_id->TypeMatch(tmp_id->ArgType(0))) ||
                                (type_id->TypeMatch(tmp_id->ArgType(1)))) {
                                // Import this operator as result of its type being in the explicit use clause
                                (void) result.Insert(tmp_id) ;
                                continue ;
                            }
                        }
                    }

                    // suffix name does not match the required identifier. So this one is invisible.
                    tmp_id = 0 ;
                    continue ;
                }

                // Insert it in return array.
                (void) result.Insert(tmp_id) ;
            }
        }

        // LRM 10.4 (a) and (b) : homograph checks on the last inserted identifier :
        if (!tmp_id) continue ; // no identifier found in this use clause

        // check if we accidentally included the same identifier twice.
        if (tmp_id == id) continue ;

        if (num_of_direct_matches && !tmp_id->IsSubprogram() && !tmp_id->IsEnumerationLiteral()) {
            // LRM 10.4 (a) :
            // Issue 2377 : if there were already identifiers in the 'result' (not included),
            // and the identifier we attempt to include is certainly a homograph (not overloadable)
            // then we should truly 'hide' the included one.
            // Silently make this (non-overloadable) invisible.
            (void) result.Remove(tmp_id) ;
            continue ;
        }
        if (id && !tmp_id->IsSubprogram() && !tmp_id->IsEnumerationLiteral() && !tmp_id->IsPhysicalUnit()) {
            // VIPER 4102 : allow 'physical units' to be included just like enumeration literals. This seems to be ModelSim behavior.
            // LRM 10.4 (b) :
            // Check if we already found (a homograph) identifier in another use clause.
            // ModelSim gives an error for this.
            if (first) {
                // first time to print violation :
                VhdlIdDef tmp(0) ; // Temporary node to get 'present' line/file info
                tmp.Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", name) ;
                // Optional extra info : report the hidden identifiers :
                id->Info("%s is declared here",id->Name()) ;
                tmp_id->Info("another match is here") ;
                first = 0 ;
                // Remove the existing identifier (so it becomes invisible)
                // Cheat : Do NOT remove 'exist', so at least one identifier remains visible.
                // That is handy if users want to suppress the error and want us to choose something.
                // result.Remove(exist) ;
            } else {
                tmp_id->Info("another match is here") ;
            }
            // remove tmp_id, because it is invisible.
            // Do not insert this (non-overloadable) id if one was already directly (not included) visible.
            (void) result.Remove(tmp_id) ;
            continue ;
        }

        // Assign this identifier to be the last included one :
        id = tmp_id ;
        // continue with next use clause, since other use clauses might insert more homographs (for which we want to error out)
    }

    // Local includes extended area for design units (if there is no upper scope) :
    // VIPER #5918 : Consider the package and package body declaration present in declarative region of
    // architecture/entity/process/procedure/package etc.
    if (_extended_decl_area && (!_upper || (_owner_id && (_owner_id->IsProtectedBody() || _owner_id->IsPackage() || _owner_id->IsPackageBody())))) {
        // Bail out when we found a non-overloadable one :
        if (id && !id->IsSubprogram() && !id->IsEnumerationLiteral()) {
            return id ;
        }

        id = _extended_decl_area->FindAllIncluded(name, result) ;
    }

    return id ;
}

VhdlIdDef *
VhdlScope::FindSingleObjectIncluded(const char *name) const
{
    // Find the first (single) object by this name,
    // which is 'included' (with use clauses)
    // at exactly this (scope) point. (Does not go up in scope hierarchy).
    // It DOES include the primary-unit extended decl region from a sec unit.
    VhdlIdDef *id = 0 ;
    if (_use_clauses) {
        id = (VhdlIdDef*)_use_clauses->GetValue(name) ;
    }

    // Enforce rule (4) above.
    // If Homographs were inserted during scope inclusion, All should be invisible.
    // Check if this identifier is 'hidden' :
    if (id && _hidden_ids && _hidden_ids->GetItem(name)) {
        VhdlIdDef tmp(0) ; // Temporary node to get 'present' line/file info
        tmp.Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", name) ;

        // Optional extra info : report the hidden identifiers :
        id->Info("%s is declared here",id->Name()) ;
        VhdlIdDef *tmp_id ;
        MapItem *mitem ;
        FOREACH_SAME_KEY_MAPITEM(_use_clauses, name, mitem) {
            tmp_id = (VhdlIdDef*)mitem->Value() ;
            if (tmp_id==id) continue ;
            tmp_id->Info("another match is here") ;
        }
    }

    // New the  use_clause_units map. One entry per use clause.
    MapIter mi ;
    char *suffix ;
    VhdlIdDef *prefix_id ;
    VhdlIdDef *tmp_id ;
    // Keep track of 'hidden' identifiers (but we do not need the olde '_hidden_ids' table.
    // We can do this on the fly now that we have all use clauses here.
    unsigned first = 1 ;
    FOREACH_MAP_ITEM(_use_clause_units, mi, &prefix_id, &suffix) {
        if (!prefix_id) continue ;

        // Check suffix name if there :
        // VIPER 3456 fix should not be needed here : there is only a single id requested, so it should match by name.
        if (suffix && !Strings::compare(suffix, name)) continue ; // if suffix does not match, skip this one.

        // Note that multiple tmp_id's per entry (per use clause) will only be present if they were overloadable.
        // Non-overloadable id's (certain homographs) will always be by themselves.
        // So insert all matching identifiers, and do homograph check one per use clause entry.
        // reset tmp_id.
        tmp_id = 0 ;
        // Library prefix is special :
        if (prefix_id->IsLibrary()) {
            // Otherwise, find the selected unit in this library :
            VhdlLibrary *lib ;
            if (Strings::compare(prefix_id->Name(),"work")) {
                // use present work library
                lib = vhdl_file::GetWorkLib() ;
            } else {
                // Find the library by name (create library if needed, so that we can look into on-disk units)
                lib = vhdl_file::GetLibrary(prefix_id->Name(),1) ;
            }
            VERIFIC_ASSERT(lib) ; // we would have created it if not there.
            // Now find the primary unit 'name' in this library :
            VhdlPrimaryUnit *pu = lib->GetPrimUnit(name) ;
            // Get the identifier of this primary unit
            tmp_id = (pu) ? pu->Id() : 0 ;

            // Register dependency now that we actually use this identifier..
            // Oops. Cannot do that here, in a 'const' function. Do the 'Using' at the caller level.
            // if (tmp_id) Using(tmp->LocalScope()) ;
        } else {
            // Otherwise, find 'name' in the scope of this prefix_id :
            VhdlScope *prefix_scope = prefix_id->LocalScope() ;
            //if (prefix_id->IsPackageInst()) prefix_scope = prefix_id->GetUninstantiatedPackageScope() ;
            if (!prefix_scope) continue ;

            // Find the identifier(s) in this local scope (only DeclArea) :
            MapItem *mitem ;
            FOREACH_SAME_KEY_MAPITEM(prefix_scope->DeclArea(), name, mitem) {
                // Get the identifier :
                tmp_id = (VhdlIdDef*)mitem->Value() ;
                if (!tmp_id) continue ;
                // library and design-unit objects are stored in the scope, but not considered 'declared' there.
                if (tmp_id->IsLibrary() || tmp_id->IsDesignUnit()) {
                    tmp_id = 0 ;
                    continue ;
                }
            }
        }

        // LRM 10.4 (a) and (b) : homograph checks on the last inserted identifier :
        if (!tmp_id) continue ; // no identifier found in this use clause

        // check if we accidentally included the same identifier twice.
        if (tmp_id == id) continue ;

        // RD: Do homograph check on other identifiers included.
        // Also, exclude attributes from this check. Officially they should be in, but our packages have risk of including multiple ones.
        if (id && !tmp_id->IsSubprogram() && !tmp_id->IsEnumerationLiteral() && !tmp_id->IsAttribute()) {
            // LRM 10.4 (b) :
            // Check if we already found (a homograph) identifier in another use clause.
            // ModelSim gives an error for this.
            if (first) {
                // first time to print violation :
                VhdlIdDef tmp(0) ; // Temporary node to get 'present' line/file info
                tmp.Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", name) ;
                // Optional extra info : report the hidden identifiers :
                id->Info("%s is declared here",id->Name()) ;
                tmp_id->Info("another match is here") ;
                first = 0 ;
                // Remove the existing identifier (so it becomes invisible)
                // Cheat : Do NOT remove 'exist', so at least one identifier remains visible.
                // That is handy if users want to suppress the error and want us to choose something.
                // result.Remove(exist) ;
            } else {
                tmp_id->Info("another match is here") ;
            }
            // Do not use this (non-overloadable) id if one was already directly (not included) visible.
            continue ;
        }

        // Assign this identifier to be the last included one :
        id = tmp_id ;
        // continue with next use clause, since other use clauses might insert more homographs (for which we want to error out)
    }

    // Local includes extended area for design units (if there is no upper scope) :
    // VIPER #5918 : Consider the package and package body declaration present in declarative region of
    // architecture/entity/process/procedure/package etc.
    if (!id && _extended_decl_area && (!_upper || (_owner_id && (_owner_id->IsProtectedBody() || _owner_id->IsPackage() || _owner_id->IsPackageBody())))) {
        id = _extended_decl_area->FindSingleObjectIncluded(name) ;
    }

    return id ;
}

void
VhdlScope::FindAllLocal(Set &result) const
{
    // Accumulate all directly visible VhdlIdDefs in this (and extended) scope
    // which are NOT 'included' (with use-clauses) (so they are declared here, locally).
    // Look at exactly this (scope) point. (Does not go up in scope hierarchy).
    // It DOES include the primary-unit extended decl region from a sec unit.
    MapIter miter ;
    VhdlIdDef *id = 0 ;

    FOREACH_MAP_ITEM(_this_scope, miter, 0, &id) {
        if (!id) continue ;

        // VhdlScope::Declare() already guarantees that there are no homographs in 'this'
        // So no need to 'bail-out' from inside the loop.

        // Make hidden identifiers invisible. LRM 10.4.
        if (!id->Type() && !id->LocalScope() && !id->IsLibrary() && !id->IsLabel() && !id->IsGroup() && !id->IsGroupTemplate()) {
            // Id is declared, but does not have its type (or scope) pointer set yet.
            // This means that we have not completed the declaration yet.
            // FIX ME : need a better way to determine if a identifier is declared or not.
            //
            // This means that at this point it is 'hidden' : It can block
            // visibility of other identifiers, but in itself is not visible yet.
            continue ;
        }
        (void) result.Insert(id) ;
    }

    // Local includes extended area for design units (if there is no upper scope) :
    // VIPER #5918 : Consider the package and package body declaration present in declarative region of
    // architecture/entity/process/procedure/package etc.
    if (_extended_decl_area && (!_upper || (_owner_id && (_owner_id->IsProtectedBody() || _owner_id->IsPackage() || _owner_id->IsPackageBody())))) {
        _extended_decl_area->FindAllLocal(result) ;
    }
    return ;
}

VhdlIdDef *
VhdlScope::FindAllLocal(const char *name, Set &result) const
{
    // Accumulate all directly visible VhdlIdDefs by this name,
    // which are NOT 'included' (with use-clauses) (so they are declared here, locally).
    // Look at exactly this (scope) point. (Does not go up in scope hierarchy).
    // It DOES include the primary-unit extended decl region from a sec unit.
    MapItem *mitem ;
    VhdlIdDef *id = 0 ;

    FOREACH_SAME_KEY_MAPITEM(_this_scope, name, mitem) {
        id = (VhdlIdDef*)mitem->Value() ;

        // VhdlScope::Declare() already guarantees that there are no homographs in 'this'
        // So no need to 'bail-out' from inside the loop.

        // Make hidden identifiers invisible. LRM 10.4.
        if (!id->Type() && !id->LocalScope() && !id->IsLibrary() && !id->IsLabel() && !id->IsGroup() && !id->IsGroupTemplate()) {
            // Id is declared, but does not have its type (or scope) pointer set yet.
            // This means that we have not completed the declaration yet.
            // FIX ME : need a better way to determine if a identifier is declared or not.
            //
            // This means that at this point it is 'hidden' : It can block
            // visibility of other identifiers, but in itself is not visible yet.
            id = 0 ;
            continue ;
        }
        (void) result.Insert(id) ;
    }

    // If a non-overloadable identifier was inserted, it will block
    // all other potentially visible identifiers by this name in
    // upper or included scopes. By keeping it in the result, it
    // will also prevent that we look in other scopes (use_clauses etc).
    if (id && !id->IsSubprogram() && !id->IsEnumerationLiteral()) {
        return id ;
    }

    // Local includes extended area for design units (if there is no upper scope) :
    // VIPER #5918 : Consider the package declaration or package body declaration present
    // in declarative region of architecture/entity/block/procedure etc
    if (_extended_decl_area && (!_upper || (_owner_id && (_owner_id->IsProtectedBody() || _owner_id->IsPackage() || _owner_id->IsPackageBody())))) {
        id = _extended_decl_area->FindAllLocal(name, result) ;
    }
    return id ;
}

VhdlIdDef *
VhdlScope::FindSingleObjectLocal(const char *name) const
{
    // Find the first (single) object by this name,
    // which is declared at exactly this (scope) point.
    // (Does not go up in scope hierarchy).
    // It DOES include the primary-unit extended decl region from a sec unit.
    VhdlIdDef *id = 0 ;

    if (_this_scope) {
        id = (VhdlIdDef*)_this_scope->GetValue(name) ;
    }

    // Local includes extended area for design units (if there is no upper scope) :
    //if (!id && !_upper && _extended_decl_area) {
    // VIPER #5918 : Consider the package declaration or package body declaration present
    // in declarative region of architecture/entity/block/procedure etc
    if (!id && _extended_decl_area && (!_upper || (_owner_id && (_owner_id->IsProtectedBody() || _owner_id->IsPackage() || _owner_id->IsPackageBody())))) {
        id = _extended_decl_area->FindSingleObjectLocal(name) ;
    }

    if (!id) {
        // Viper 5554 For mixed language the _name can be escaped as it
        // might not be a valid vhdl name but it might be valid verilog name

        // Viper 7667: If  name is not escaped it can match to a escaped name in the
        // converted entity as it was escaped due to bigger case letter in it.

        // if the name is escaped and so also is the identifier in the converted entity
        // then it should be obtained by the map search.
        VhdlIdDef *owner = GetOwner() ;
        VhdlPrimaryUnit *unit = owner ? owner->GetPrimaryUnit() : 0 ;
        if (unit && name && unit->IsVerilogModule()) {
            unsigned is_escaped = (name[0] == '\\' && name[Strings::len(name) - 1] == '\\') ;
            if (is_escaped) {
                // Create un-escaped name and check whether it exists
                char *unescaped_name = VhdlNode::UnescapeVhdlName(name) ;
                VhdlIdDef *module_id = 0 ;
                MapIter miter ;
                FOREACH_MAP_ITEM(_this_scope, miter, 0, &module_id) {
                    if (!module_id) continue ;
                    if (Strings::compare(module_id->Name(), unescaped_name)) {
                        id = module_id ;
                        break ;
                    }
                }
                Strings::free(unescaped_name) ;
            } else {
                // Create escaped name and check whether it exists
                VhdlIdDef *module_id = 0 ;
                MapIter miter ;
                FOREACH_MAP_ITEM(_this_scope, miter, 0, &module_id) {
                    if (!module_id) continue ;
                    char* escaped_port_name = Strings::save("\\", name, "\\") ;
                    if (Strings::compare_nocase(module_id->Name(), escaped_port_name)) {
                        Strings::free(escaped_port_name) ;
                        id = module_id ;
                        break ;
                    }
                    Strings::free(escaped_port_name) ;
                }
            }
        }
    }

    return id ;
}

void
VhdlScope::FindCommaOperators(Set &result, Set &done_scopes) const
{
    // Find all comma (aggregate) operators. Special since these are not real VHDL operators. Just an artifact of how we do aggregate type-resolving. Generalized for all operators, after VIPER 2962.
    if (!done_scopes.Insert(this)) return ; // already did this scope

    // Find all comma (aggregate) operators visible from this point.
    // Special since these are not real VHDL operators.
    // Just an artifact of how we do aggregate type-resolving.
    // Viper issues Issues 1159, 1361, 1610 and 1798 were filed on this.
    // All related to the fact that normal scoping rules do not apply for
    // comma operators.
    // Basic difference is that a type can be made visible by selection ("package.type"),
    // but the comma operators will still be invisible.
    // But VHDL defines that aggregates should always work if the type is visible.

    // The special treatment is to search in the current scope,
    // upward and also directly in the scopes that this scope is 'using'.
    // That removes the dependency on the '_use_clauses' map, which can
    // thus stay clean for normal VHDL scoping rules.

    const char *oper_name = "\",\"" ; // name of the comma operator.
    // Look in the local scope
    MapItem *mitem ;
    VhdlIdDef *id = 0 ;
    FOREACH_SAME_KEY_MAPITEM(_this_scope, oper_name, mitem) {
        id = (VhdlIdDef*)mitem->Value() ;
        // Insert this identifier, but bail out if we already collected it.
        // May be unnecessary with the 'done_scopes' Set in place, but does not hurt.
        if (!result.Insert(id)) {
            return ;
        }
    }

    // Traverse upward
    if (_upper) {
        _upper->FindCommaOperators(result, done_scopes) ;
    }

    // And into extended decl area :
    if (_extended_decl_area) {
        _extended_decl_area->FindCommaOperators(result, done_scopes) ;
    }

    // Check all 'use_clause_units' (which include all visible types)
    // Have to do this one (and not just _using) because of VIPER 7130.
    if (_use_clause_units) {
        // Use the new 'use_clause_units' map if there. Faster than _using.
        VhdlScope *prefix_scope ;
        VhdlIdDef *prefix_id ;
        MapIter mi ;
        // const char *suffix ;
        FOREACH_MAP_ITEM(_use_clause_units, mi, &prefix_id, 0) {
            if (!prefix_id) continue ;
            // Ignore the "suffix" in the value field, because we want to see all operators.
            prefix_scope = prefix_id->LocalScope() ;
            if (!prefix_scope) continue ; // might be a library. Cannot find operators there.
            // Here, search this scope for operators.
            // Recur into this 'other' scope also. Needed for VIPER issue 1610 and 1964.
            prefix_scope->FindCommaOperators(result, done_scopes) ;
        }
    }

    // Viper #5544, 4528: also check _using, since inference may have been done by 'selection' of a type name.
    if (_using) {
        // old units (old restored units) might still be there that do not have use_clause_units.
        // Use the (more extended) _using Map for these.

        // And then instead of searching '_use_clauses', look directly in the 'using' scopes.
        SetIter si ;
        VhdlScope *other_scope ;
        FOREACH_SET_ITEM(_using, si, &other_scope) {
            if (!other_scope) continue ;
            if (_extended_decl_area==other_scope) continue ; // no need to traverse twice.

            // Recur into this 'other' scope also. Needed for VIPER issue 1610 and 1964.
            other_scope->FindCommaOperators(result, done_scopes) ;
        }
    }
}

// Specialty find routine : find default binding (entity) from a component name.
// This routine can be called during elaboration.
// Routine is silent. Return 0 if none found.
VhdlIdDef *
VhdlScope::FindDefaultBinding(const char *name, const VhdlIdDef *component)
{
    if (!name) return 0 ;

    // Default binding rules : LRM 5.2.2 :
    // From this scope (scope of the 'missing binding indication' (per LRM 5.2.2),
    // Find directly visible entity declarations by this (component) name.
    // Simulators have extensions to these LRM 5.2.2. rules.
    //
    // Note that this routine has seen many iterations :
    // Applicable VIPER issues : 2172, 2496, 2736, 2762, 2941, 3124, 3181.

    // Here is what we finally settled on.
    // (1) look for entity included directly from 'this' scope (directly visible entity) LRM 5.2.2 (a) and (b)
    //     Note : this is an anomaly : we should normally not look in the scope during elaboration.
    // (2) If the component is there, look in the library of the design unit in which the component is declared. LRM 5.2.2(c) (in VHDL-2002 LRM)
    // (3) scan work library (the library of the design unit of 'this' scope.
    // (4) scan other libraries declared (library clauses of current scope). Scan local (sec)design unit lib clauses first->last. (prim)design unit library clauses next, first->last. (ModelSim does this)
    // (5) Check libraries in -L option.
    //

    // (1) look for entity included directly from 'this' scope (directly visible entity) LRM 5.2.2 (a) and (b)
    //     Note : this is an anomaly : we should normally not look in the scope during elaboration.
    // VIPER 2941 : do NOT error out on hidden identifiers, nor on any non-entity identifier that might be visible.
    // always ignore component decls (LRM 5.2.2) and any non-entity identifiers (VIPER 2941/2364),
    // So look in the '_use_clauses' only, and directly (because that is where the visibly entities are) (do not call FindSingleIncluded).
    VhdlScope *scope = this ;
    VhdlIdDef *id ;
    while (scope) {
        id = (scope->_use_clauses) ? (VhdlIdDef*)scope->_use_clauses->GetValue(name) : 0 ;
        if (id && !id->IsEntity()) id = 0 ;
        if (id) return id ; // got it : it was explicitly included. VIPER 2172.
        // go to next scope up :
        if (scope->_upper) {
            scope = scope->_upper ;
        } else if (scope->_extended_decl_area) {
            scope = scope->_extended_decl_area ;
        } else {
            scope = 0 ;
        }
    }

    // New use_clause_units :
    // Map (VhdlIdDef*)->(char*) represents all use clauses in this scope "use (lib.)id.all" creates id->NULL entry, "use (lib.)id.name" creates an id->name entry.
    // "use (lib.)prefix.suffix". for "use prefix.all" store suffix==NULL.
    // For default binding, we are only interested in the "use library.all" and "use library.unitname" clauses.
    scope = this ;
    while (scope) {
        MapIter mi ;
        char *suffix ;
        VhdlIdDef *prefix_id ;
        // FIXME : Should not it be looped over scope->GetUseClauseUnits()
        Map *use_clause_units = scope->GetUseClauseUnits() ;
        FOREACH_MAP_ITEM(use_clause_units, mi, &prefix_id, &suffix) {
        //FOREACH_MAP_ITEM(_use_clause_units, mi, &prefix_id, &suffix) {
            if (!prefix_id) continue ;

            // Check suffix name if there :
            if (suffix && !Strings::compare(suffix, name)) continue ; // if suffix does not match, skip this one.

            // Library prefix is special :
            if (prefix_id->IsLibrary()) {
                VhdlLibrary *lib ;
                if (Strings::compare(prefix_id->Name(),"work")) {
                    // use present work library
                    // Do NOT rely on vhdl_file::GetWorkLib(). That one is only valid during analysis.
                    // Instead, find the library in which 'this' scope is declared :
                    VhdlIdDef *current_unit = GetContainingPrimaryUnit() ;
                    VhdlPrimaryUnit *prim_unit = (current_unit) ? current_unit->GetPrimaryUnit() : 0 ;
                    lib = (prim_unit) ? prim_unit->GetOwningLib() : 0 ;
                    // during analysis, the unit is not yet stored in a library.
                    // in that case, 'lib' will be 0, and we need to use the current work library :
                    if (!lib) lib = vhdl_file::GetWorkLib() ;
                } else {
                    // Find the library by name
                    lib = vhdl_file::GetLibrary(prefix_id->Name(),1) ;
                }
                VERIFIC_ASSERT(lib) ; // we would have created it if not there.
                // Now find the primary unit 'name' in this library :
                VhdlPrimaryUnit *pu = lib->GetPrimUnit(name) ;
                // Or look into Verilog :
                if (!pu) {
                    pu = vhdl_file::GetVhdlUnitFromVerilog(lib->Name(), name) ;
                }
                // Get the id of this primary unit
                id = (pu) ? pu->Id() : 0 ;
                if (id) return id ; // got it : it was explicitly included. VIPER 2172.
            }
        }
        // go to next scope up :
        if (scope->_upper) {
            scope = scope->_upper ;
        } else if (scope->_extended_decl_area) {
            scope = scope->_extended_decl_area ;
        } else {
            scope = 0 ;
        }
    }

    // (2) If the component is there, look in the library of the design unit in which the component is declared. LRM 5.2.2(c) (in VHDL-2002 LRM)
    // Look in component's design unit's library :
    if (component) {
        scope = component->LocalScope() ;
        VhdlIdDef *components_unit = (scope) ? scope->GetContainingPrimaryUnit() : 0 ;
        VhdlPrimaryUnit *prim_unit = (components_unit) ? components_unit->GetPrimaryUnit() : 0 ;
        VhdlLibrary *lib = (prim_unit) ? prim_unit->GetOwningLib() : 0 ;
        // Find in that library
        VhdlPrimaryUnit *binding_unit = (lib) ? lib->GetPrimUnit(name) : 0 ;
        // or look in the Verilog side
        if (!binding_unit && lib) {
            binding_unit = vhdl_file::GetVhdlUnitFromVerilog(lib->Name(), name) ;
        }
        if (binding_unit) {
            // found in the component's design unit library (VIPER 3181)
            return binding_unit->Id() ;
        }
    }

    // (3) scan work library (the library of the design unit of 'this' scope.
    // Scan library clauses. First work library (the library in which this scope is declared):
    // Do NOT rely on vhdl_file::GetWorkLib(). That one is only valid during analysis.
    VhdlIdDef *current_unit = GetContainingPrimaryUnit() ;
    VhdlPrimaryUnit *prim_unit = (current_unit) ? current_unit->GetPrimaryUnit() : 0 ;
    VhdlLibrary *lib = (prim_unit) ? prim_unit->GetOwningLib() : 0 ;
    // Find in that library
    VhdlPrimaryUnit *binding_unit = (lib) ? lib->GetPrimUnit(name) : 0 ;
    // Or look in the Verilog side :
    if (!binding_unit && lib) {
        binding_unit = vhdl_file::GetVhdlUnitFromVerilog(lib->Name(), name) ;
    }
    if (binding_unit) {
        // found in the current work library (where we are).
        return binding_unit->Id() ;
    }

    // (4) scan other libraries declared (library clauses of current scope). Scan local (sec)design unit lib clauses first->last. (prim)design unit library clauses next, first->last. (ModelSim does this)
    // Now the other library clauses. First check this immediate design unit, then (if there) upper primary unit.
    // VIPER #3447 : Update 'scope' with containing design unit scope :
    // VIPER 4949 : Go to the secondary or primary design unit of 'this' scope (not the unit where the scope was last set to) :
    scope = TopScope() ;
    while (scope) {
        // Get the design unit of this scope :
        VhdlIdDef *unit = scope->GetContainingDesignUnit() ;
        VhdlDesignUnit *design_unit = (unit) ? unit->GetDesignUnit() : 0 ;
        // Go for the context clause of this unit. That is where the library clauses are :
        Array *context_clause = (design_unit) ? design_unit->GetContextClause() : 0 ;
        VhdlDeclaration *decl ;
        unsigned i, j ;
        Array *ids ;
        FOREACH_ARRAY_ITEM(context_clause, i, decl) {
            if (!decl) continue ;
            // Get the declared id's in this context clause (only library clause returns array of ids :
            ids = decl->GetIds() ;
            FOREACH_ARRAY_ITEM(ids, j, id) {
                if (!id) continue ;
                if (!id->IsLibrary()) break ; // this is not a library clause
                if (Strings::compare(id->Name(),"std")) continue ; // library std reserved for packages
                if (Strings::compare(id->Name(),"work")) continue ; // already did the work library
                // Check this library :
                lib = vhdl_file::GetLibrary(id->Name(),1/*create if needed, so we can look in bin-saved files*/) ;
                binding_unit = (lib) ? lib->GetPrimUnit(name) : 0 ;
                // Or look in the Verilog side
                if (!binding_unit && lib) {
                    binding_unit = vhdl_file::GetVhdlUnitFromVerilog(lib->Name(), name) ;
                }
                if (binding_unit) {
                    // found in this library
                    return binding_unit->Id() ;
                }
            }
        }
        // Not found in this unit's library.
        // See if there is an upper (primary) unit :
        if (scope->_extended_decl_area) {
            scope = scope->_extended_decl_area ;
        } else {
            scope = 0 ; // essentially break
        }
    }

    // (5) Check libraries in -L option.
    // If not found anywhere, find in -L option specified libraries
    binding_unit = vhdl_file::GetUnitFromLOptions(name) ;
    if (binding_unit) {
        return binding_unit->Id() ;
    }
    return 0 ; // cannot find default binding entity
}

/**************************************************************/
// Dependency handling
/**************************************************************/

void
VhdlScope::Using(VhdlScope *other_scope, VhdlTreeNode const *from)
{
    if (!other_scope) return ;

    VhdlIdDef *other_owner = other_scope->GetOwner() ;
    if (from && other_owner && (other_owner->IsLabel() || other_owner->IsEntity() || other_owner->IsArchitecture() || other_owner->IsSubprogram() || other_owner->IsBlock())) {
        VhdlScope *runner = this ;
        unsigned correct_scope = 0 ;
        do {
            if (other_scope == runner) {
                correct_scope = 1 ;
                break ;
            }
            runner = runner->Upper() ;
        } while (runner) ;

        if (!correct_scope) {
            // Viper #4401
            runner = this ;
            while (runner && runner->Upper()) runner = runner->Upper() ;

            VhdlIdDef *runner_owner = runner ? runner->GetOwner() : 0 ;
            if (runner_owner && runner_owner->GetEntity() == other_owner) correct_scope = 1 ;
        }

        // LRM Section 6.3
        if (!correct_scope) from->Warning("prefix of expanded name does not denote an enclosing construct") ;
    }

    // Scope dependency is based on design unit dependency, so get
    // design unit's scope via Upper().
    while (other_scope->Upper()) other_scope = other_scope->Upper() ;

    // This scope uses another scope. Update '_using'
    VhdlScope *this_scope = this ;
    // Get design unit's scope via Upper().
    while (this_scope->Upper()) this_scope = this_scope->Upper() ;

    // Exit if we find that the two scopes are part of the same design unit
    if (this_scope == other_scope) return;

    if (!this_scope->_using) this_scope->_using = new Set(POINTER_HASH) ;
    (void) this_scope->_using->Insert(other_scope) ;

    // Also update the 'usedby' field in 'other_scope' :
    if (!other_scope->_used_by) other_scope->_used_by = new Set(POINTER_HASH) ;
    (void) other_scope->_used_by->Insert(this_scope) ;
}

void
VhdlScope::NoLongerUsing(const VhdlScope *other_scope)
{
    if (!other_scope) return ;

    // Delete 'other_scope' from my '_using' list :
    if (_using) (void) _using->Remove(other_scope) ;

    // Also remove me from the other scope
    if (other_scope->_used_by) (void) other_scope->_used_by->Remove(this) ;
}

/**************************************************************/
// Get identifiers that encompass this scope
/**************************************************************/

VhdlIdDef *VhdlScope::GetOwner() const
{
    return _owner_id ;
}

VhdlIdDef *VhdlScope::GetSubprogram() const
{
    if (_owner_id && _owner_id->IsSubprogram()) return _owner_id ;
    if (_upper) return _upper->GetSubprogram() ;
    return 0 ;
}

VhdlIdDef *VhdlScope::GetLabel() const
{
    if (_owner_id && _owner_id->IsLabel()) return _owner_id ;
    if (_upper) return _upper->GetLabel() ;
    return 0 ;
}

VhdlIdDef *VhdlScope::GetContainingPrimaryUnit() const
{
    // Go to top-level first :
    if (_upper) return _upper->GetContainingPrimaryUnit() ;

    // for secondary units : go to extended scope :
    if (_extended_decl_area) return _extended_decl_area->GetContainingPrimaryUnit() ;

    // No more upper scope and no more extended decl area. So we are at the primary unit :
    return _owner_id ;
}

VhdlIdDef *VhdlScope::GetContainingDesignUnit() const
{
    // Go to top-level first :
    if (_upper) return _upper->GetContainingDesignUnit() ;

    // Now return this owner (should be there, and should be the design unit).
    return _owner_id ;
}

VhdlScope *VhdlScope::GetSubprogramScope()
{
    if (_is_subprogram_scope) return this ;
    if (_owner_id && _owner_id->IsSubprogram()) return this ; // historical way of getting subprogram scope
    if (_upper) return _upper->GetSubprogramScope() ;
    return 0 ;
}

VhdlScope *VhdlScope::GetProcessScope()
{
    if (_is_process_scope) return this ;
    if (_upper) return _upper->GetProcessScope() ;
    return 0 ;
}

void VhdlScope::CheckIncompleteTypeDefs() const
{
    MapIter mi ;
    VhdlIdDef *id ;
    VhdlIdDef *owner = GetOwner() ;

    // Iterate over this scope declared identifiers and issue error for incomplete types
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (!id) continue ;
        if (id->IsIncompleteType()) id->Error("missing full type definition for %s", id->Name()) ;
        // VIPER #4508: Check added to error out if there are subprograms without body
        if (!id->IsSubprogram() || id->IsPredefinedOperator() || id->IsPslDeclId() || id->IsAlias() || id->IsGeneric() || id->IsSubprogramInst()) continue ;
        if (owner && !(owner->IsPackage() || owner->IsEntity() || owner->IsProtected())) {
            if (!id->Body()) {
                id->Error("subprogram %s does not have a body",id->Name()) ;
            }
        }
    }

    // VIPER #4508: If there is only declaration of a subprogram in the primary design unit but there is
    // no body in the corresponding secondary unit, to address such situations a similar check is added to the
    // extended scope.
    VhdlScope *extended_scope = GetExtendedDeclAreaScope() ;
    if (!extended_scope) return ;

    VhdlIdDef *extended_owner = extended_scope->GetOwner() ;
    if (extended_owner && (extended_owner->IsPackage() || extended_owner->IsEntity() || extended_owner->IsProtected())) {
        FOREACH_MAP_ITEM(extended_scope->GetThisScope(), mi, 0, &id) {
            if (!id) continue ;
            if (id->IsSubprogram() && !id->IsPredefinedOperator() && !id->IsPslDeclId() && !id->IsAlias() && !id->Body() && !id->IsGeneric() && !id->IsSubprogramInst()) {
                id->Error("subprogram %s does not have a body",id->Name()) ;
            }
            // VIPER #3630 : Produce error if deferred constant is not initialized
            if (id->IsConstantId() && !id->Decl()) { // Deferred constant, but no initial value
               id->Error("deferred constant %s needs an initial value", id->Name()) ;
            }
        }
    }
}
void VhdlScope::ResetThisScope()
{
    if (_this_scope) _this_scope->Reset() ;
}

void VhdlScope::ResetPredefinedOps()
{
    // after syntax error, yydestruct would free the type
    // identifiers. However predefined ops wound still point
    // to the type identifiers. This routine blindly clears
    // the mess.
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (id && id->IsPredefinedOperator()) Undeclare(id) ;
    }
}

void VhdlScope::ResetPredefinedOpsForType(const VhdlIdDef *type_id)
{
    if (!type_id) return ;
    if (!type_id->IsType() && !type_id->IsSubtype()) return ;

    // after syntax error, yydestruct would free the type
    // identifiers. However predefined ops wound still point
    // to the type identifiers. Clean it up.
    MapIter mi ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (!id || !id->IsPredefinedOperator()) continue ;

        unsigned match = 0 ;
        for (i = 0 ; i < id->NumOfArgs() ; ++i) {
            if (type_id != id->Arg(i)) continue ;
            match = 1 ;
            break ;
        }

        if (match) Undeclare(id) ;
    }
}

void VhdlScope::ForceDeclare(const VhdlIdDef *id)
{
    if (!id) return ;
    if (!_this_scope) _this_scope = new Map(STRING_HASH,47) ;
    (void) _this_scope->Insert(id->Name(),id,0,1) ;
}

// Viper 8019: create new Alias ids and strore them in the _predefined map of the scope.
// These ids are owned by scope and deleted in its destructor. This routine also creates
// the signature for these alias ids. This is defined in 4.3.3.2 2000 LRM.
void VhdlScope::DeclareImplicitAlias(VhdlIdDef* target_id, const VhdlName* target_type_name)
{
    if (!target_id || !target_type_name) return ;

    VhdlDesignator* new_target_ref = new VhdlIdRef(target_id) ;
    VhdlName* new_target_name = new_target_ref ;
    if (target_type_name->GetPrefix()) {
        VhdlMapForCopy old2new ;
        VhdlName *copied_prefix = target_type_name->GetPrefix()->CopyName(old2new) ;
        new_target_name = new VhdlSelectedName(copied_prefix, new_target_ref) ;
    }
    (void) new_target_name->TypeInfer(target_id->Type(), 0,0,0) ;

    VhdlIdDef *alias_id = new VhdlAliasId(Strings::save(target_id->Name())) ;
    alias_id->SetTargetId(target_id) ; // Viper #8134

    if (!_predefined) _predefined = new Array(16) ; // Give it some space
    _predefined->InsertLast(alias_id) ;
    (void) Declare(alias_id) ;

    VhdlSignature* sig = 0 ;

    if (target_id->IsEnumerationLiteral() || target_id->IsPhysicalUnit()) {
        VhdlMapForCopy old2new ;
        VhdlName *copied_type_name = target_type_name->CopyName(old2new) ;
        sig = new VhdlSignature(0, copied_type_name) ;
    }

    VhdlName* sig_name = new VhdlSignaturedName(new_target_name, sig) ;
    (void) sig_name->TypeInfer(target_id->Type(), 0,0,0) ;

    alias_id->DeclareAlias(target_id->Type(), sig_name, target_id->IsLocallyStaticType(), target_id->IsGloballyStaticType(),0) ;
}
