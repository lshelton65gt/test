/*
 *
 * [ File Version : 1.296 - 2014/03/05 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlName.h" // Compile flags are defined within here

#include <string.h>   // for strchr
#include <stdio.h>    // for sprintf

#include "Strings.h"
#include "Array.h"
#include "Set.h"
#include "Map.h"

#ifdef VHDL_PRESERVE_LITERALS
#include <ctype.h>     // tolower, toupper
#endif

#include "vhdl_file.h"
#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "VhdlUnits.h"

#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
#include "../verilog/veri_file.h"
#include "../verilog/VeriElab_Stat.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
/* Get bit values in VeriConstVal's streams */
#define GET_BIT(S,B) ((S)?(((((S)[(B)/8])<<((B)%8))&(1<<7)) != 0):0)
#endif

/**********************************************************/
//  Constraints and Values : Elaboration code
/**********************************************************/

VhdlConstraint *VhdlName::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    // Not sure if this is legal or not.
    // Especially for case-expressions.
    // Allow it for now, returning 0.
    // CHECK ME !
    // Error("illegal name in constraint") ;
    return 0 ;
}

VhdlConstraint *VhdlIdRef::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    // Could be a type, or an object name. Take the constraint any way.
    if (!_single_id) return 0 ;

    // For access types, get the constraint from designated type
    // This is needed as the constraint on the access type id could be null
    // when designated type was forward declared.. e.g. Viper #3649
    if (_single_id->IsAccessType() && !_single_id->Constraint()) {
        VhdlIdDef *id = _single_id->DesignatedType() ;
        VhdlConstraint *access_constraint = (id && id->Constraint()) ? id->Constraint()->Copy() : 0 ;
        if (access_constraint) access_constraint = new VhdlAccessConstraint(access_constraint) ;
        _single_id->SetConstraint(access_constraint) ;
    } else if (_single_id->IsFunction()) { // Viper: 6496
        VhdlConstraint *constraint = _single_id->Constraint() ;
        if (constraint && constraint->IsUnconstrained()) {
            VhdlValue *val = Evaluate(constraint, df, 0) ;
            delete val ; // After this, the value is not interesting. We just want the 'result' constraint

            return constraint->Copy() ;
        }
    }

    VhdlConstraint *return_constraint = (_single_id->Constraint()) ? _single_id->Constraint()->Copy() : 0 ;
    if (return_constraint && return_constraint->IsUnconstrained()) {
        // Viper #7288: With constraint distributed over value, this should
        // Evaluate the expression and check if its value carry any constrint
        VhdlValue *id_value = EvaluateId(_single_id, df, 0) ;
        return_constraint->ConstraintBy(id_value) ;
        // Please note that: above would return incorrect constraint in case
        // it holds any array constraint with different element constraints.
        // This is because array constraint cannot represent such structures.
    }

    return return_constraint ;
}

VhdlConstraint *VhdlSelectedName::EvaluateConstraintInternal(VhdlDataFlow * df, VhdlConstraint * /*target_constraint*/)
{
    // Get proper unique identifier considering package instance specific alias prefix
    VhdlIdDef *unique_id = GetElabSpecificUniqueId() ;
    VhdlConstraint *id_constr = 0 ;
    // Done with Viper 7505: for record element the evaluated constraint is in the prefix.
    if (unique_id && IsVhdl2008()) {
        if (unique_id->IsRecordElement()) {
            VhdlConstraint *prefix_constraint = _prefix ? _prefix->EvaluateConstraint(df, 0) : 0 ;
            id_constr = prefix_constraint ? prefix_constraint->ElementConstraintFor(unique_id) : 0 ;
            id_constr = id_constr ? id_constr->Copy() : 0 ;
            delete prefix_constraint ;
        }
    }
    // Could be a type, or an object name
    // FIX ME for sliced/indexed prefixes... Need prefix_id_type ?
    if (!id_constr) id_constr = (unique_id && unique_id->Constraint()) ? unique_id->Constraint()->Copy() : 0 ;
    return id_constr ;
}

// Evaluate and Allocate Prefix Constraint
// Memory is always allocated here.
VhdlConstraint *VhdlIndexedName::EvaluatePrefixConstraint(VhdlDataFlow *df)
{
    VhdlConstraint* prefix_constraint = 0 ;
    if (_prefix_id) {
        // No need for prefix constraint copy
        prefix_constraint = _prefix_id->Constraint() ;

        // For pre-defined operators (don't have a constraint), constraint is in the (return) type :
        if (!prefix_constraint && _prefix_id->IsPredefinedOperator()) {
            prefix_constraint = _prefix_id->Type()->Constraint() ;
        }
        prefix_constraint = prefix_constraint ? prefix_constraint->Copy() : 0 ;
    } else {
        prefix_constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
    }
    return prefix_constraint ;
}
VhdlConstraint *VhdlIndexedName::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    VhdlConstraint *result = 0 ;

    // VHDL-2008 defines the terms "unconstrained" and "constrained" somewhat differently
    // from earlier versions of VHDL, since the situation is somewhat more involved. In
    // VHDL-2008, an array subtype is unconstrained if it has no index constraints, and the element
    // subtype is either not a composite type or is an unconstrained type. An unconstrained
    // type has no constraints anywhere in its structure where a constraint could apply.
    // An array subtype is fully constrained if it has index constraints for all of its indices, and
    // the element subtype is either not a composite type or is a fully constrained type. A fully
    // constrained type has constraints everywhere in its structure where a constraint could VHDL-2008
    // defines the terms "unconstrained" and "constrained" somewhat differently
    // from earlier versions of VHDL, since the situation is somewhat more involved. In
    // VHDL-2008, an array subtype is unconstrained if it has no index constraints, and the element
    // subtype is either not a composite type or is an unconstrained type. An unconstrained
    // type has no constraints anywhere in its structure where a constraint could apply.
    // An array subtype is fully constrained if it has index constraints for all of its indices, and
    // the element subtype is either not a composite type or is a fully constrained type. A fully
    // constrained type has constraints everywhere in its structure where a constraint could

    // Thus we can have declarations like the following:

    // type M_unconstrained is
    // array (natural range <>, natural range <>) of bit;

    // type A_unconstrained is
    // array (character range <>) of M_unconstrained;

    // type AA_unconstrained is
    // array (character range <>) of A_unconstrained;

    // signal temp1 : AA_unconstrained ('a' to 'b')('a' to 'b')(1 to 2, 2 to 3) ;
    // signal temp2 : AA_unconstrained ('x' to 'y')('x' to 'y')(3 to 4, 5 to 6) ;

    // No matter what this is, we need the constraint of the prefix :

    // For Vhdl2008 _prefix_id is NULL indexed name with multiple level
    // of indices.

    // Check what to do :
    if (_is_index_constraint) {
        VhdlConstraint *prefix_constraint = EvaluatePrefixConstraint(df) ;
        if (!prefix_constraint) return 0 ;

        VhdlIdDef *prefix_id = _prefix_id ? _prefix_id : (_prefix ? _prefix->GetFormal() : 0) ;
        if (!prefix_id) {
            delete prefix_constraint ;
            return 0 ;
        }

        VhdlDiscreteRange *assoc_elem = (VhdlDiscreteRange*)(_assoc_list ? _assoc_list->GetLast() : 0) ;
        if (assoc_elem && assoc_elem->IsOpen()) {
            //Nothing needs to be done we should return the prefix constraint as it is
            return (prefix_constraint) ;
        }

        // Something like "bit_vector (3 downto 0)" or "arraytype(0 to 5, 10 downto 0)" ;

        result = prefix_constraint ;
        unsigned i ;
        VhdlDiscreteRange *index_range ;
        VhdlConstraint *index_constraint = 0 ;
        FOREACH_ARRAY_ITEM_BACK(_assoc_list, i, index_range) {
            if (!index_range) continue ;

            index_constraint = index_range->EvaluateConstraintInternal(df, 0) ;
            if (!index_constraint) {
                delete result ;
                result = 0 ;
                break ;
            }

            // LRM 3.2.1.1 and 4.2 :
            // non-'Compatibility' of index ranges is an error in LRM...

            // Test result index constraints against type mark's index
            // range. Need to be 'compatible' ranges (new one should not
            // be out of range of old one).
            // Since we build index range LSB to MSB, we need to decend
            // to the right prefix index range each time. This is a bit cumbersome,
            // but since no memory is allocated, it should still be fast.
            VhdlConstraint *tmp = _prefix ? _prefix->ElementConstraintDeep(prefix_constraint) : 0 ;
            unsigned dim = i ;
            while (dim-- != 0) tmp = tmp ? tmp->ElementConstraint() : 0 ;

            if (tmp && tmp->IsArrayConstraint()) tmp = tmp->IndexConstraint() ;

            if (!tmp) { delete index_constraint ; delete prefix_constraint; return 0 ; }

            // LRM 3.2.1.1 : index constraint should not impose index constraints on
            // an already constrained array type

            if (!tmp->IsUnconstrained()) {
                Error("%s already imposes an index constraint", prefix_id->Name()) ;
                delete index_constraint ;
                delete prefix_constraint ;
                return 0 ;
            }

            // Now test 'compatible' range (both bounds are contained by existing range)
            if (!tmp->Contains(index_constraint->Left()) ||
                !tmp->Contains(index_constraint->Right())) {
                // Issue 1769 : don't error if this is a null range
                if (!index_constraint->IsNullRange()) {
                    // Old message not clear :
                    // Error("index constraint is not compatible with index range of %s",_prefix_id->Name()) ;
                    // New message :
                    // Create an image for the value :
                    char *v = 0 ;
                    // Figure out which bound was out-of-range :
                    if (index_constraint->Left() && !tmp->Contains(index_constraint->Left())) {
                        v = index_constraint->Left()->Image() ;
                    } else if (index_constraint->Right() && !tmp->Contains(index_constraint->Right())) {
                        v = index_constraint->Right()->Image() ;
                    } else {
                        // Just to be sure we issue the message.
                        v = Strings::save("") ;
                    }
                    // Create an image for the range :
                    char *r = tmp->Image() ;
                    // Issue message :
                    // VIPER #3815 : Produce warning in non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        index_range->Warning("index value %s is out of range %s",v,r) ;
                    } else {
                        index_range->Error("index value %s is out of range %s",v,r) ;
                    }
                    Strings::free(v) ;
                    Strings::free(r) ;
                }
            }
            tmp->ConstraintBy(index_constraint) ;
            // prefix_constraint is absorved.
            delete index_constraint ; index_constraint = 0 ; // Free up, otherwise we leak memory
        }
    } else if (_is_array_index) {
        // Something like "vector(3)" or "array_object(5,3,2)"

        // Return the element constraint of the prefix.

        // Decent as many dimensions as the index list indicates :
        // Typeinference already proofed that the dimensions are OK.

        VhdlConstraint *prefix_constraint = EvaluatePrefixConstraint(df) ;
        if (!prefix_constraint) return 0 ;

        VhdlConstraint *elem_constraint = prefix_constraint ;
        unsigned i ;
        VhdlDiscreteRange *assoc ;
        FOREACH_ARRAY_ITEM(_assoc_list, i, assoc) {
            if (!elem_constraint) break ; // Something went wrong in type-inference..
            elem_constraint = elem_constraint->ElementConstraint() ;
        }
        result = (elem_constraint) ? elem_constraint->Copy() : 0 ;
        delete prefix_constraint ;
    } else if (_is_array_slice) {
        // Something like "vector(3 downto 0)" ; Only one assoc is there.

        VhdlConstraint *prefix_constraint = EvaluatePrefixConstraint(df) ;
        if (!prefix_constraint) return 0 ;
        // Evaluate the slice bounds and create a new constraint.
        VERIFIC_ASSERT(_assoc_list) ;
        VhdlDiscreteRange *assoc = (VhdlDiscreteRange*)_assoc_list->At(0) ;
        if (!assoc) {
            delete prefix_constraint ;
            return 0 ;
        }
        // Viper 7461 : If assoc is open return the prefix_constraint itself
        if (assoc->IsOpen()) {
            return prefix_constraint ;
        }
        // Need the dataflow, since expression could contain variables
        VhdlConstraint *range = assoc->EvaluateConstraintInternal(df, 0) ;
        if (!range) {
            delete prefix_constraint ;
            return 0 ;
        }

        // Don't test the range. Leave that up to the Evaluate() routine.
        // Create a new Array constraint, using the element constraint
        // of the prefix as element constraint, and the newly created range
        // as the index constraint

        result = new VhdlArrayConstraint(range, prefix_constraint->ElementConstraint()->Copy()) ;
        delete prefix_constraint ;

    } else if (_is_function_call) {
        // Function call or type-conversion :
        // return the constraint of the prefix identifier.
        // This could be unconstrained still, but if so, we
        // adjust that later when we evaluate the prefix that this function call appears in.
        // So, its OK to return a unconstrained constraint.
        // RD: 2/2008: Change of behavior:
        // There are many cases where we do not call-up the value of the function call.
        // For example : attributes (VIPER 3800, 3755 and often in static elaboration of expressions).
        // We could adjust all places in the code where that is done, or we could just do it here.
        // VHDL defines that subprograms are part of 'dynamic' elaboration (LRM 10.2(b)).
        // This means that subprograms might have to be executed to obtain 'static' constraint info.
        // So, we might as well do the subprogram execution right here.
        // NOTE: There may now be cases where we evaluate subprogram calls multiple times.
        // Once to obtain the constraint, once to obtain the value.
        // That is the nature of the split handling of constraints and values in Verific data structures.

        VhdlConstraint *prefix_constraint = EvaluatePrefixConstraint(df) ;
        if (!prefix_constraint) return 0 ;

        result = prefix_constraint ;
        if (result->IsUnconstrained()) {
            // VIPER 3800, 3755 : unconstrained (array) prefix requires 'dynamic' elaboration.
            // The (dynamic) prefix evaluation call itself will adjust the 'result' to the actual constrained range :
            VhdlValue *val = Evaluate(result, df, 0) ;
            // After this, the value is not interesting. We just want the 'result' constraint.
            delete val ;
            // Check that the prefix is really not unconstrained after this dynamic elab call.
            if (result->IsUnconstrained()) {
                // Error out now ?
                return result ;
            }
        }
        // Here prefix_constraint is absorved
    } else if (_is_type_conversion) {
        VhdlConstraint *prefix_constraint = EvaluatePrefixConstraint(df) ;
        if (!prefix_constraint) return 0 ;
        // VIPER #3161 : Evaluate constraint of type conversion from argument
        if (prefix_constraint->IsUnconstrained()) {
            // If prefix constraint is unconstrained, try to evaluate constraint
            // of assoc and make prefix_constraint constrianed
            VhdlDiscreteRange *assoc = (_assoc_list && (_assoc_list->Size() == 1)) ? (VhdlDiscreteRange*)_assoc_list->At(0) : 0 ;
            VhdlConstraint *arg_constraint = (assoc) ? assoc->EvaluateConstraintInternal(df, 0) : 0 ;
            if (arg_constraint && !arg_constraint->IsUnconstrained()) {
                result = prefix_constraint ;
                result->ConstraintBy(arg_constraint) ;
            }
            delete arg_constraint ;
        }
        if (!result) result = prefix_constraint ;
        // Here prefix_constraint is absorved
    } else if (_is_record_constraint) {
        // Viper 6811: We now have a new flag for record_constraint
        VhdlConstraint *prefix_constraint = EvaluatePrefixConstraint(df) ;
        if (!prefix_constraint) return 0 ;
        if (prefix_constraint->IsRecordConstraint()) {
            result = prefix_constraint ;
            unsigned i ;
            VhdlIndexedName *rec_element ;
            VhdlConstraint *rec_element_constraint ;
            FOREACH_ARRAY_ITEM_BACK(_assoc_list, i, rec_element) {
                if (!rec_element) continue ;

                rec_element_constraint = rec_element->EvaluateConstraintInternal(df, 0) ;
                if (!rec_element_constraint) {
                    delete result ;
                    result = 0 ;
                    break ;
                }
                //index_range must itself be an indexed name as rec_ele (index_constraint)
                // Hence get the record element from prefix
                VhdlIdDef *rec_element_id =  rec_element->GetNamedPrefixId() ; // Finds the Id of the named prefix
                VhdlConstraint *rec_ele_constr = result->ElementConstraintFor(rec_element_id) ;
                if (rec_ele_constr) rec_ele_constr->ConstraintBy(rec_element_constraint) ;
                delete rec_element_constraint ;
            }
        }
    }

    return result ;
}

VhdlConstraint *VhdlAttributeName::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    VhdlConstraint *constraint = 0 ;
    verific_int64 dim ;

    switch (_attr_enum) {
    case VHDL_ATT_base :
        {
        // Get the constraint of the base type of the prefix
        VhdlIdDef *base = (_prefix_type) ? _prefix_type->BaseType() : 0 ;
        constraint = (base) ? base->Constraint() : 0 ;
        constraint = (constraint) ? constraint->Copy() : 0 ;
        break ;
        }

    case VHDL_ATT_element : // VIPER #7502: vhdl_2008: new predefined attribute
        {
        // Prefix could be array (sub) type or an array.
        // Get the constraint of the prefix (name).
        VhdlConstraint *prefix_constraint = _prefix ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
        if (!prefix_constraint) return 0 ;
        constraint = prefix_constraint->ElementConstraint() ; // Take the element constraint of the prefix_constraint
        constraint = (constraint) ? constraint->Copy() : 0 ;

        delete prefix_constraint ;
        if (!constraint)  return 0 ;
        break ;
        }
    case VHDL_ATT_subtype : // VIPER #7522: vhdl_2008: new predefined attribute
        {
        // Prefix could be any object/alias thereof.
        // Get the constraint of the prefix (name).
        VhdlConstraint *prefix_constraint = _prefix ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
        if (!prefix_constraint) return 0 ;

        constraint = prefix_constraint ;
        break ;
        }

    case VHDL_ATT_range :
    case VHDL_ATT_reverse_range :
        {
        // Prefix could be scalar (sub) type or array (sub) type
        // or an array. Get the constraint of the prefix (name).
        VhdlConstraint *prefix_constraint = _prefix ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
        if (!prefix_constraint) return 0 ;

        if (prefix_constraint->IsUnconstrained()) { // Viper #7288
            VhdlValue *prefix_value = _prefix ? _prefix->Evaluate(0, df, 0) : 0 ;
            prefix_constraint->ConstraintBy(prefix_value) ;
            delete prefix_value ;
        }
        // As per LRM section 14, the attributes only apply to constrained type/objects
        // VIPER #7878 : Check constraint of index only, as range/reverse_range will work only
        // on index
        VhdlConstraint *index_constraint = prefix_constraint->IndexConstraint() ;
        if (index_constraint && index_constraint->IsUnconstrained()) {
            delete prefix_constraint ;
            return 0 ;
        }

        // Decent to the right dimension
        constraint = prefix_constraint ;
        dim = (_expression) ? _expression->EvaluateConstantInteger()-1 : 0 ;
        while (constraint && dim-- != 0) constraint = constraint->ElementConstraint() ;
        if (!constraint) {
            delete prefix_constraint ;
            return 0 ; // something went wrong in type-inference..
        }

        // Now set-up the return constraint range
        VhdlValue *left = constraint->Left() ;
        VhdlValue *right = constraint->Right() ;
        unsigned dir = constraint->Dir() ;
        if (!left || !right) return 0 ;
        if (_attr_enum==VHDL_ATT_reverse_range) {
            // Swap directions
            VhdlValue *tmp = left ;
            left = right ;
            right = tmp ;
            dir = (dir==VHDL_to) ? VHDL_downto : (dir==VHDL_downto) ? VHDL_to : 0 ;
        }
        VhdlConstraint *new_constraint = new VhdlRangeConstraint(left->Copy(), dir, right->Copy()) ;
        // VIPER #2920 : If we are creating new range constraint from unconstrained type, mark
        // created constraint as unconstrained
        // RD: Not needed, since we now only get here for constrained array types.
        // if (constraint->IsUnconstrained()) new_constraint->SetUnconstrained() ;
        constraint = new_constraint ;
        delete prefix_constraint ;
        break ;
        }

    case VHDL_ATT_image :
        {
        // VIPER #2946 : Evlauate the constraint of attribute 'image'
        // Evaluate the value of the attribute
        VhdlValue *expr_val = (_expression) ? _expression->Evaluate(0, df, 0): 0 ;
        if (!expr_val) break ;
        if (!expr_val->IsConstant()) { // Only handle constant argument (cannot generate a non-constant image)
            Error("expression is not constant") ;
            delete expr_val ;
            break ;
        }
        // LRM 14.1 : "it is an error if the parameter value does
        // not belong to the subtype implied by the prefix" :
        if (!_prefix_type || !expr_val->CheckAgainst(_prefix_type->Constraint(),/*from*/_expression, df)) {
            delete expr_val ;
            break ;
        }
        // Get the string representation for this value.
        char *image = expr_val->Image() ;
        if (!image) break ;
        delete expr_val ;

        if (_prefix_type->IsPhysicalType()) { // Viper #5566
            Map *unit_id_map = _prefix_type->GetTypeDefList() ;
            const char *unit_name = (_prefix_type == StdType("time")) ? vhdl_file::GetTimeUnit() : 0 ;
            if (unit_name) {
                char *tmp = image ;
                image = Strings::save(tmp, " ", unit_name) ;
                Strings::free(tmp) ;
            } else {
                MapIter mi ;
                VhdlPhysicalUnitId *unit_id ;
                FOREACH_MAP_ITEM(unit_id_map, mi, &unit_name, &unit_id) {
                    if (!unit_name || !unit_id) continue ;
                    if (unit_id->Position() == 1) {
                        char *tmp = image ;
                        image = Strings::save(tmp, " ", unit_name) ;
                        Strings::free(tmp) ;
                        break ;
                    }
                }
            }
        }

        // Get the length of string form of this value
        unsigned len = Strings::len(image) ;
        Strings::free(image) ;

        // The return type of image attribute is string and number of characters in the
        // string will be equal to the length of the string created from its _expression
        // So constraint of this attribute will be an array constraint of range
        // (1 to length_of_string) and element constraint of this array will
        // be the constraint of character (string element)
        // Create index constraint:
        VhdlConstraint *index_constraint = new VhdlRangeConstraint(new VhdlInt((verific_int64)1), VHDL_to, new VhdlInt((verific_int64)len)) ;
        // Element constraint :
        VhdlIdDef *char_type = _result_type ? _result_type->ElementType() : 0 ;
        VhdlConstraint *element_constraint = (char_type) ? char_type->Constraint() : 0 ;
        constraint = new VhdlArrayConstraint(index_constraint, (element_constraint) ? element_constraint->Copy(): 0) ;
        break ;
        }
    default :
        // CHECK ME : Only 'base 'range and 'reverse_range are valid to return constraints from a Name ?

        // FIX ME : For all practical purposes we can probably not error out here and just return NULL,
        // because attributes are always allowed in expressions.  We can do this because the number of
        // states will be determined from the number of bits when a NULL is returned from this routine.
        // This  entire routine should be flushed out and made complete according to the LRM,
        // especially for sub-ranges. (VIPER #2144)

        //Error("illegal use of attribute %s in constraint", _designator->Name()) ;
        break ;
    }
    return constraint ;
}

VhdlConstraint *VhdlAttributeName::EvaluateConstraintPhysical(VhdlDataFlow *df)
{
    // We need to implement this only for the 'range' constraint:
    switch (_attr_enum) {
    case VHDL_ATT_range :
    case VHDL_ATT_reverse_range :
        {
        // Prefix could be scalar (sub) type or array (sub) type
        // or an array. Get the constraint of the prefix (name).
            VhdlConstraint *prefix_constraint = _prefix ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
        if (!prefix_constraint) return 0 ;

        // As per LRM section 14, the attributes only apply to constrained type/objects
        // VIPER #7878 : Check constraint of index only, as range/reverse_range will work only
        // on index
        VhdlConstraint *index_constraint = prefix_constraint->IndexConstraint() ;
        if (index_constraint && index_constraint->IsUnconstrained()) {
            delete prefix_constraint ;
            return 0 ;
        }

        // Decent to the right dimension
        VhdlConstraint *constraint = prefix_constraint ;
        verific_int64 dim = (_expression) ? _expression->EvaluateConstantInteger()-1 : 0 ;
        while (constraint && dim-- != 0) constraint = constraint->ElementConstraint() ;
        if (!constraint) return 0 ; // something went wrong in type-inference..

        // Now set-up the return constraint range
        VhdlValue *left = constraint->Left() ;
        VhdlValue *right = constraint->Right() ;
        unsigned dir = constraint->Dir() ;
        if (!left || !right) return 0 ;
        if (_attr_enum==VHDL_ATT_reverse_range) {
            // Swap directions
            VhdlValue *tmp = left ;
            left = right ;
            right = tmp ;
            dir = (dir==VHDL_to) ? VHDL_downto : (dir==VHDL_downto) ? VHDL_to : 0 ;
        }
        // Create the range with physical values, get the values from the constraint:
        VhdlValue *physical_left = new VhdlPhysicalValue(left->Integer()) ;
        VhdlValue *physical_right = new VhdlPhysicalValue(right->Integer()) ;
        VhdlConstraint *new_constraint = new VhdlRangeConstraint(physical_left, dir, physical_right) ;
        // VIPER #2920 : If we are creating new range constraint from unconstrained type, mark
        // created constraint as unconstrained
        // RD: Not needed, since we now only get here for constrained array types.
        // if (constraint->IsUnconstrained()) new_constraint->SetUnconstrained() ;
        delete prefix_constraint ;
        return new_constraint ;
        }
    default : break ;
    }

    // For other cases call the normal evaluate constraint routine:
    return EvaluateConstraintInternal(df, 0) ;
}

VhdlConstraint *VhdlInteger::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
#if 0 // RD: Should no longer be needed (for VIPER 3143). Constants create their value which are then tested when assigned.
    VhdlValue *left = new VhdlInt(_value) ;
    VhdlValue *right = new VhdlInt(_value) ;

    return new VhdlRangeConstraint(left, VHDL_to, right) ;
#endif
    return 0 ;
}
VhdlConstraint *VhdlExternalName::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint)
{
    // VIPER #7765: Constraint of external names are specified within itself as subtype indication, evaluate it:
    VhdlConstraint *constraint = (_subtype_indication) ? _subtype_indication->EvaluateConstraintInternal(df, target_constraint) : 0 ;
    if (constraint) return constraint ;

    if (!_unique_id || IsStaticElab()) {
        _unique_id = FindExternalObject(_path_token, _hat_count) ;
    }
    if (!_unique_id) return 0 ; // type-inference failed
    return (_unique_id && _unique_id->Constraint()) ? _unique_id->Constraint()->Copy() : 0 ;
}
// **************************************************
// VIPER #6473 EvaluateIntegerRangeConstraint(VhdlDataFlow *df)
// Returns the constraint of an integer expression
// **************************************************
VhdlConstraint *VhdlInteger::EvaluateIntegerRangeConstraint(VhdlDataFlow * /*df*/)
{
    if (!vhdl_file::GetPruneLogicForIntegerConstraint()) return 0 ;

    VhdlValue *left = new VhdlInt(_value) ;
    VhdlValue *right = new VhdlInt(_value) ;

    return new VhdlRangeConstraint(left, VHDL_to, right) ;
}

VhdlConstraint *VhdlIdRef::EvaluateIntegerRangeConstraint(VhdlDataFlow *df)
{
    // Viper #6473: evaluate the integer range for this idref
    // works only when this id is of integer type
    if (!vhdl_file::GetPruneLogicForIntegerConstraint() || !_single_id) return 0 ;

    VhdlIdDef *id_type = _single_id->Type() ;
    if (!id_type || !id_type->IsIntegerType()) return 0 ;

    VhdlValue *value = Evaluate(0, df, 0) ;
    if (value && value->IsConstant()) return new VhdlRangeConstraint(value, VHDL_to, value->Copy()) ;
    delete value ; // value is non-constant

    return (_single_id->Constraint()) ? _single_id->Constraint()->Copy() : 0 ;
}

// **************************************************
// ElementTypeDeep(VhdlIdDef *prefix_type)
// returns the element constraint of the prefix to that
// much deep which is equal to the index nesting of
// the name for example in this is a(1 to 2)(3 to 5)
// then the function should return
// prefix_constraint->ElementConstraint()->ElementConstraint()
// **************************************************

VhdlConstraint *
VhdlIndexedName::ElementConstraintDeep(VhdlConstraint *prefix_constraint)
{
    // VIPER #7812 : Consider number of associations present in the indexed name
    unsigned dim = _assoc_list ? _assoc_list->Size(): 0 ;
    // In case of multidimentional array like
    // type t1 is array(natural range <>, natural range <>) of bit
    // we can define a subtype as s0 t1(open) ; However the array has
    // two dimension but the length of the assoc is only 1. The number
    // of dimension is stored in _prefix_type->Dimension. Viper 7937
    if (_prefix_type && _prefix_type->Dimension()) {
        dim = _prefix_type->Dimension() ;
    }
    while (dim-- != 0) prefix_constraint = prefix_constraint ? prefix_constraint->ElementConstraint() : 0 ;
    //return _prefix ? _prefix->ElementConstraintDeep(prefix_constraint->ElementConstraint()) : 0 ;
    return _prefix ? _prefix->ElementConstraintDeep(prefix_constraint) : 0 ;
}

/**********************************************************/
//  Add name to the sensitivity list Checks from LRM 8.1
/**********************************************************/

void VhdlName::AddToSensList(Set & /*s*/)
{
    Error("illegal name on the sensitivity list") ;
    return ;
}

void VhdlDesignator::AddToSensList(Set &s)
{
    if (!_single_id) return ; // type-inference failed
    if (!_single_id->IsSignal()) {
        Error("%s is not a signal", _single_id->Name()) ;
        return ;
    }

    // Issue 2013 : An alias on the sense-list effectively puts its target name on the sense list :
    if (_single_id->IsAlias()) {
        VhdlName *target_name = _single_id->GetAliasTarget() ;
        if (target_name) {
            target_name->AddToSensList(s) ;
            return ; // we should not need to put the alias-id itself on the sense list.
        }
    }

    // add the signal to the sense-list
    (void) s.Insert(_single_id) ;
}

void VhdlSelectedName::AddToSensList(Set &s)
{
    // Get proper unique identifier considering package instance specific alias prefix
    VhdlIdDef *unique_id = GetElabSpecificUniqueId() ;
    if (!_prefix || !unique_id) return ; // type-inference failed ?
    if (unique_id->IsRecordElement()) {
        // The prefix contains the signal id
        _prefix->AddToSensList(s) ;
        return ;
    }
    // If it was not a record element, it was a true selected name.
    // So this must be the signal itself
    if (!unique_id->IsSignal()) {
        Error("%s is not a signal", unique_id->Name()) ;
        return ;
    }

    // Issue 2013 : An alias on the sense-list effectively puts its target name on the sense list :
    if (unique_id->IsAlias()) {
        VhdlName *target_name = unique_id->GetAliasTarget() ;
        if (target_name) {
            target_name->AddToSensList(s) ;
            return ; // we should not need to put the alias-id itself on the sense list.
        }
    }

    (void) s.Insert(unique_id) ;
}

void VhdlIndexedName::AddToSensList(Set &s)
{
    // The signal id must be in the prefix, if anywhere
    if (!_prefix) return ; // constructor failed
    _prefix->AddToSensList(s) ;
}

/**********************************************************/
//  Evaluate an expression
/**********************************************************/

VhdlValue *VhdlAll::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // FIX ME : error out ?
    return 0 ;
}

VhdlValue *VhdlOthers::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // FIX ME : error out ?
    return 0 ;
}

VhdlValue *VhdlUnaffected::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // Unaffected is like literal NULL (no drivers).
    // FIX ME : Return 'Z' value from target-constraint ?
    return 0 ;
}

VhdlValue *VhdlInteger::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    return new VhdlInt(_value) ;
}

VhdlValue *VhdlReal::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    return new VhdlDouble(_value) ;
}

VhdlValue *VhdlStringLiteral::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // _single_id is the return type of this string
    // It's element type is the enumerated type
    // that we are looking for
    if (!_name || !_single_id) return 0 ;

    VhdlIdDef *elem_type = _single_id->ElementType() ;
    if (!elem_type) return 0 ; // ?

    // Create an array of enum values
    Array *values = new Array((unsigned)Strings::len(_name)) ;
    // this used to occur before elem_type check but could lead to memory leaks that way, now if we error out, we do so before allocating the array

    // Run over the characters
    VhdlIdDef *elem ;
    char buffer[4] ;
    char *tmp ;
    for (tmp = _name+1 ; *(tmp+1)!='\0'; tmp++) {
        sprintf(buffer,"'%c'",*tmp) ;
        elem = elem_type->GetEnum(buffer) ;
        VERIFIC_ASSERT(elem) ; // Or type-inference was not working
        values->InsertLast(new VhdlEnum(elem)) ;
    }
    return new VhdlCompositeValue(values) ;
}
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// the _actual_part's name is created using ImageVhdl which do not looks at the
// constraint size. We need to change the constraint here based on target so that
// a later error in checkagainst can be avoided. This all started happening because
// now we are matching integer with vector in generic type after vipers 5909 and 5889

VhdlValue *VhdlStringLiteral::EvaluateForVerilog(VhdlConstraint * target_constraint, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // _single_id is the return type of this string
    // It's element type is the enumerated type
    // that we are looking for
    if (!_name || !_single_id) return 0 ;

    // Viper 7180 ; We can pass a string actual to a enumeration type formal in cross language
    // Typically if we pass string "a" then it should pick the enum at position 97 for string
    // "ab" it will pick enum at 24930 position.
    VhdlIdDef *elem_type = _single_id->IsArrayType() ? _single_id->ElementType() : _single_id ;
    if (!elem_type) return 0 ; // ?

    verific_int64 target_size = target_constraint ? target_constraint->GetSizeForConstraint() : 0 ;

    verific_int64 name_len = Strings::len(_name) ;
    if (!target_size) target_size = (name_len-2)*8 ;

    if (_single_id->IsEnumerationType()) {
        target_size = (name_len-2)*8 ;
    }

    if (!IsBitStringLiteral()) {
        if (target_size > (name_len-2)*8) target_size = (name_len-2)*8 ;
        char *new_name = Strings::allocate((unsigned long)(target_size + 3)) ;
        char *bit_string = Strings::save(_name + 1) ;
        bit_string[name_len-1-1] = '\0' ;
        new_name[0] = new_name[target_size+1] = '"' ;
        new_name[target_size + 2] = '\0' ;
        unsigned i ;
        for (i = 0 ; i < (unsigned)target_size ; i++) {
            new_name[1+i] = '0' + GET_BIT(bit_string, ((name_len -2) * 8) - (1 + i)) ;
        }
        // Viper 7180: if the actual is string and the return type is enum, check whether
        // the integer value falls in the constraint. if not we pick the enum at the integer
        // position and return the enum value.
        VhdlEnum *enum_val = 0 ;
        if (_single_id->IsEnumerationType()) {
            new_name[target_size+1] = '\0' ;
            char *bin_string = new_name + 1 ;
            verific_int64 dec = Strings::BinToDec(bin_string) ;
            VhdlInt int_val(dec) ;
            if (target_constraint) {
                if (!target_constraint->Contains(&int_val)) {
                    char *c = target_constraint->Image() ;
                    char *tmp_img = int_val.Image() ;
                    Error("value %s is out of target constraint range %s", tmp_img, c) ;
                    Strings::free(c) ;
                    Strings::free(tmp_img) ;
                } else {
                    VhdlIdDef *elem = elem_type->GetEnumAt((unsigned)dec) ;
                    VERIFIC_ASSERT(elem) ; // Or type-inference was not working
                    enum_val = new VhdlEnum(elem) ;
                }
            }
        }
        Strings::free(_name) ;
        _name = Strings::save(new_name) ;
        Strings::free(new_name) ;
        Strings::free(bit_string) ;
        if (enum_val) return enum_val ;
    } else {
        if (target_size < name_len-2/*for the two quotes*/) {
            char *new_name = Strings::save(_name) ;
            char *sliced_name = new_name + (name_len - (target_size + 2)) ;
            *sliced_name = '"' ;
            Strings::free(_name) ;
            _name = Strings::save(sliced_name) ;
            Strings::free(new_name) ;
        }
    }
    return (Evaluate(target_constraint,0,0)) ;
}
#endif

VhdlValue *VhdlBitStringLiteral::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow *df, unsigned /*force_driver_value*/)
{
    // as VhdlString
    VhdlValue* ret_val = VhdlStringLiteral::Evaluate(0,df,0) ;
#ifdef VHDL_PRESERVE_LITERALS
     char base = (char)::tolower(*_based_string) ;
     if (base == 'b' || base == 'o' || base == 'x') ret_val->SetBaseSpecifier(base) ;
#endif
    return ret_val ;
}

VhdlValue *VhdlCharacterLiteral::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    return (_single_id) ? new VhdlEnum(_single_id) : 0 ;
}

VhdlValue *VhdlPhysicalLiteral::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow *df, unsigned /*force_driver_value*/)
{
    // Physical literal value can also be a 'real'

    // Implied absense of a value : value is integer 1
    VhdlValue *literal = (_value) ? _value->Evaluate(0,df,0) : new VhdlInt((verific_int64)1) ;
    if (!literal) return 0 ; // Error occurred

    // Now get the position number of the unit
    // Analysis already set IdDef backpointer, so use GetId() :
    VhdlIdDef *unit = (_unit) ? _unit->GetId() : 0 ;
    // multiply literal and unit position
    VhdlValue *result = 0 ;
    if (unit && unit->Position()) {
        result = literal->BinaryOper(new VhdlInt((verific_int64)unit->Position()), VHDL_STAR, this) ;
    } else {
        result = (unit) ? literal->BinaryOper(new VhdlInt((verific_int64)unit->InversePosition()), VHDL_SLASH, this): 0 ;
    }
    // now divide the result by scaling factor specified by user.
    // If user set time resolution as 100 us, first the literal and unit
    // position is multiplied and then the result is divided by scaling
    // factor (100 in this case) to get the proper value of this literal.
    if (result && (vhdl_file::GetTimeScale() > 1)) {
        result = result->BinaryOper(new VhdlInt((verific_int64)vhdl_file::GetTimeScale()), VHDL_SLASH, this) ;
    }

    // Get the integer representation of this physical literal value:
    verific_int64 int_val = (result) ? result->Integer() : literal->Integer() ;
    // Delete this value and return a new value created with this integer
    delete result ;
    return new VhdlPhysicalValue(int_val) ;
}

VhdlValue *VhdlNull::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // Null can be access type assignment or waveform assignment
    // Waveform element null is taken care of one level up inside
    // VhdlWaveformElement::Evaluate

    // Control here implies it is access value null
    return new VhdlAccessValue(0) ;
}

VhdlValue *VhdlIdRef::EvaluateAllocatorSubtype(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/)
{
    if (IsRtlElab()) return 0 ;
    if (!_single_id || !_single_id->Constraint()) return 0 ;

    VhdlConstraint *constraint = _single_id->Constraint() ;
    if (!_single_id->ContainsAccessType()) { // pointer to pointer : Viper #4290
        //if (constraint->IsUnconstrained()) {
        // VIPER #6938 : Consider access type constraint having unconstrained designated type as constrained
        if (constraint->IsUnconstrained()) {
            Error("allocator subtype indication cannot be unconstrained array") ;
            return 0 ;
        }
    }

    // Viper : 3764, 3771 - access_value must carry the value constraint
    VhdlConstraint *alloc_constraint = constraint->Copy() ;
    // VIPER #7759 : Create access value for unconstrained constraint too
    //VhdlValue *allocated_val = alloc_constraint->IsUnconstrained() ? 0 : alloc_constraint->CreateInitialValue(this) ;
    VhdlValue *allocated_val = alloc_constraint ? alloc_constraint->CreateInitialValue(this): 0 ;

    return new VhdlAccessValue(allocated_val, alloc_constraint) ;
}
VhdlValue *VhdlName::EvaluateId(VhdlIdDef *id, VhdlDataFlow *df, unsigned force_driver_value) const
{
    if (!id) return 0 ;

    VhdlValue *val = 0 ;
    // If there is dataflow, and the id is a variable (or forced driving value was requested),
    // take it from the dataflow. Otherwise, take it from the initial value of the id
    if (df && (force_driver_value || id->IsVariable())) {
        // Get the value from the present dataflow
        val = df->IdValue(id) ;
    } else {
        // For signals in a dataflow, check presence on sensitivity list
        // VIPER #2858 : do not issue warning in static elaboration.
        if (IsRtlElab() && df && !df->SensListCheck(id)) {
            df->SetIncompleteSensList() ;
            Warning("%s should be on the sensitivity list of the process", id->Name()) ;
        }
        // VIPER #3661 : In static elaboration take value of signal only in initial
        // region, i.e. where time stamp is 0.
        // VIPER 3789/3795 : do the same with variables.
        val = (IsStaticElab() && (id->IsSignal() || id->IsVariable()) && !(df && df->IsInInitial())) ? 0 : df ? df->SignalValue(id) : id->Value() ;
        if (val && IsStaticElab() && id->IsSignal() && df && df->IsInInitial() && !df->IsInSubprogramOrProcess()) Warning("value in initialization depends on signal %s", id->Name()) ; // VIPER Issue #7344
    }

    // Access Value cleanup: (VIPER #4464, 7292)
    if (IsDeallocatedValue(val)) Warning("access value for %s is not allocated or already free'd", id->Name()) ;
    return val ;
}
VhdlValue *VhdlIdRef::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)
{
    if (!_single_id) return 0 ;

    if (_single_id->IsRecordElement()) {
        // By definition, value of a record element is value of its position.
        // We get here if we evaluate a formal or a choice. CHECK ME : does that still happen ?
        return new VhdlInt((verific_int64)_single_id->Position()) ;
    }

    if (target_constraint && target_constraint->IsUnconstrained()) {
        // Constraint target subtype by my subtype
        // Do this before we go into an alias, so that the alias' constraint
        // defines the returned subtype constraint, and not the alias name itself.
        target_constraint->ConstraintBy(_single_id->Constraint()) ;
    }

    // Return the value of the identifier
    // (enumeration value, or a variable/constant value or so)
    VhdlValue *result ;

    // Aliases : evaluate the alias name. Do before the IsSubprogram check.
    if (_single_id->IsAlias()) {
        // Get the value from the alias.
        result = _single_id->GetAliasTarget()->Evaluate(0, df, force_driver_value) ;
        // Viper #7288: result value must be constrained by alias constraint
        if (result && result->GetRange()) result->ConstraintBy(_single_id->Constraint()) ;

        // Bound checking is already done in the declaration
        return result ;
    }

    // Viper 6871 and 6989 : Evaluate the actual here for reference formal
    if (_single_id->IsRefFormal()) {
        // Get the value from the reference.
        result = _single_id->GetRefTarget()->Evaluate(0, df, force_driver_value) ;
        // Bound checking is already done in the declaration
        return result ;
    }
    // Could also be a subprogram without arguments. Elaborate that.
    if (_single_id->IsSubprogram()) {
        return _single_id->ElaborateSubprogram(0 /*no args*/, target_constraint, df, this, 0) ;
    }

    // Regular identifiers : analysis guarantees that these are all 'objects' (ids that hold a value)
    // No need to test that any more.
    result = EvaluateId(_single_id, df, force_driver_value) ;

    return (result) ? result->Copy() : 0 ;
}
VhdlValue *VhdlArrayResFunction::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    return 0 ;
}

VhdlValue *VhdlRecordResFunction::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    return 0 ;
}

VhdlValue *VhdlSelectedName::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)
{
    if (!_prefix) return 0 ;

    VhdlValue *result ;

    // Get proper unique identifier considering package instance specific alias prefix
    VhdlIdDef *unique_id = GetElabSpecificUniqueId() ;
    if (!unique_id) {
        if (!_suffix || !_suffix->IsAll()) return 0 ; // type-inference failed
        result = _prefix->Evaluate(target_constraint, df, force_driver_value) ;
        if (!result) return 0 ;

        VhdlValue *inner_value = result->GetAccessedValue() ;
        result->SetAccessedValue(0) ;
        delete result ;

        return inner_value ? inner_value->Copy() : 0 ;
    }

    // Constraint the target by the selected name's constraint.
    if (target_constraint && target_constraint->IsUnconstrained()) {
        // Constraint target subtype by my subtype
        // Do this before we go into an alias, so that the alias' constraint
        // defines the returned subtype constraint, and not the alias name itself.
        target_constraint->ConstraintBy(unique_id->Constraint()) ;
    }

    if (unique_id->IsRecordElement()) {
        // Evaluate the prefix, and select the element positioned
        // by this _single_id name
        unsigned pos = (unsigned)unique_id->Position() ; // position of this element in the value

        VhdlValue *prefix = 0 ;
        VhdlIdDef *prefix_id = _prefix->GetId() ;

        // Viper #5002 : Pick up the alias target value if target is FullFormal
        VhdlName *alias_target = (prefix_id && prefix_id->IsAlias()) ? prefix_id->GetAliasTarget() : 0 ;
        if (alias_target && !alias_target->FindFullFormal()) alias_target = 0 ;

        // Viper 6871 and 6989 : Do similar time optimizations for reference target in Indexed name evaluation
        VhdlDiscreteRange *ref_target = (prefix_id && prefix_id->IsRefFormal()) ? prefix_id->GetRefTarget() : 0 ;
        if (ref_target && !ref_target->FindFullFormal()) ref_target = 0 ;

        if (prefix_id && _prefix->FindFullFormal() && (!prefix_id->IsAlias() || alias_target)
                                                               && (!prefix_id->IsRefFormal() || ref_target)
                                                               && !prefix_id->IsSubprogram() && !prefix_id->IsRecordElement()) {
            // VIPER #7300 (#3720) : Fix long runtime by preventing unnecessary copy/delete
            // Prefix is a single, full identifier, which is not an alias and not a subprogram,
            // and not a record element. In that case, we can simply pick-up the identifier value,
            // without making a copy.

            // Pick up the prefix value (as just a pointer)
            VhdlIdDef *alias_target_id = prefix_id->GetTargetId() ; // when alias is full_formal
            // Viper 6871 and 6989 Ref target needs to be Evaluated
            VhdlIdDef *ref_target_id = ref_target ? ref_target->FindFullFormal() : 0 ;
            prefix = EvaluateId(alias_target_id ? alias_target_id : (ref_target_id ? ref_target_id : prefix_id), df, force_driver_value) ;
            result = (prefix) ? prefix->ValueAt(pos) : 0 ;
            if (result) result = result->Copy() ; // Copy only the result
        } else {
            // prefix is too complex for plain pick-up. Evaluate it :

            prefix = _prefix->Evaluate(0, df, force_driver_value) ; // no need to pass-in a constraint. The unique_id type is already enough to determine the right return element
            result = (prefix) ? prefix->TakeValueAt(pos) : 0 ;
            delete prefix ;
        }
        return result ;
    }

    // Aliases : evaluate the alias name. Do before the IsSubprogram check.
    if (unique_id->IsAlias()) {
        // Get the value from the alias.
        result = unique_id->GetAliasTarget()->Evaluate(0, df, force_driver_value) ;
        // Bound checking is already done in the declaration
        return result ;
    }

    // Viper 6871 and 6989 : Evaluate the actual here for reference formal
    if (unique_id->IsRefFormal()) {
        // Get the value from the reference.
        result = unique_id->GetRefTarget()->Evaluate(0, df, force_driver_value) ;
        // Bound checking is already done in the declaration
        return result ;
    }

    // Could also be a subprogram without arguments. (issue 2182). Elaborate that.
    if (unique_id->IsSubprogram()) {
        return unique_id->ElaborateSubprogram(0 /*no args*/, target_constraint, df, this, 0) ;
    }

    // Otherwise, extended name : just return the value of it
    result = EvaluateId(unique_id, df, force_driver_value) ;

    if (_suffix && _suffix->IsAll() && result) result = result->GetInnerValue() ; // Viper #5176
    return (result) ? result->Copy() : 0 ;
}
VhdlValue *VhdlExternalName::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)
{
    // FIXME if _unique_id not set, try to resolve external name first
    if (!_unique_id || IsStaticElab()) {
        _unique_id = FindExternalObject(_path_token, _hat_count) ;
    }

    // Do not return if _unique_id is not set, we now support external name evaluation:
    //if (!_unique_id) return 0 ; // type-inference failed

    // Constraint the target by the selected name's constraint.
    if (_unique_id && target_constraint && target_constraint->IsUnconstrained()) {
        // Constraint target subtype by my subtype
        // Do this before we go into an alias, so that the alias' constraint
        // defines the returned subtype constraint, and not the alias name itself.
        target_constraint->ConstraintBy(_unique_id->Constraint()) ;
    }

    VhdlValue *result ;

    // Aliases : evaluate the alias name. Do before the IsSubprogram check.
    if (_unique_id && _unique_id->IsAlias()) {
        // Get the value from the alias.
        result = _unique_id->GetAliasTarget()->Evaluate(0, df, force_driver_value) ;
        // Bound checking is already done in the declaration
        return result ;
    }

    // Viper 6871 and 6989 : Evaluate the actual here for reference formal
    if (_unique_id && _unique_id->IsRefFormal()) {
        // Get the value from the reference.
        result = _unique_id->GetRefTarget()->Evaluate(0, df, force_driver_value) ;
        // Bound checking is already done in the declaration
        return result ;
    }

    // Otherwise, extended name : just return the value of it
    result = EvaluateId(_unique_id, df, force_driver_value) ;

    return (result) ? result->Copy() : 0 ;
}

VhdlValue *VhdlIndexedName::EvaluateAllocatorSubtype(VhdlConstraint * /*target_constraint*/, VhdlDataFlow *df)
{
    if (IsRtlElab()) return 0 ;

    if (!_prefix || !_assoc_list || !_is_index_constraint || (_assoc_list->Size() != 1)) return 0 ;

    VhdlConstraint *constraint = EvaluateConstraintInternal(df, 0) ;
    // VIPER #7759 : Allow unconstrained here too
    if (!constraint) {// || constraint->IsUnconstrained()) {
        delete constraint ;
        return 0 ;
    }

    // Viper : 3764, 3771 - access_value must carry the value constraint
    VhdlValue *allocated_val = constraint->CreateInitialValue(this) ;
    VhdlValue *return_val = 0 ;
    if (!allocated_val) {
        delete constraint ;
    } else {
        return_val = new VhdlAccessValue(allocated_val, constraint) ;
    }

    return return_val ;
}

// Added for Viper #3925 : To generate ReadPort with all the indices together
unsigned VhdlIndexedName::IsRamRead(VhdlName const *idx_name, VhdlIdDef *&prefix_id, VhdlName *&prefix_name, Array &assoc_list)
{
    if (!VhdlNode::IsRtlElab() || !idx_name) return 0 ;

    VhdlIdDef *id = idx_name->GetId() ;

    Array *assocs = idx_name->GetAssocList() ;
    VhdlName *prefix = idx_name->GetPrefix() ;
    if (!assocs || !prefix) return 0 ; // not a index name

    unsigned is_ram_read = id ? id->CanBeMultiPortRam() : IsRamRead(prefix, prefix_id, prefix_name, assoc_list) ;
    if (!is_ram_read) return 0 ;

    if (id) {
        prefix_id = id ;
        prefix_name = prefix ;
    }

    unsigned i ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(assocs, i, assoc) assoc_list.Insert(assoc) ;

    return 1 ;
}

VhdlValue *VhdlIndexedName::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)
{
    VhdlDiscreteRange *assoc ;
    VhdlValue *idx_val ;

    if (_is_type_conversion) {
        if (!_assoc_list) return 0 ;
        assoc = (VhdlDiscreteRange*)_assoc_list->At(0) ;
        if (!assoc) return 0 ;

        // Type conversion. If array->array type, we
        // need to pass an unconstrained target type in,
        // so it can be adjusted according to LRM 3.2.1.1.
        // and the 7.3.5.
        // If it is constrained, don't pass anything in since
        // Aggregates with 'others' are not allowed in argument
        // of a type conversion.
        idx_val = assoc->Evaluate((target_constraint && target_constraint->IsUnconstrained()) ? target_constraint : 0, df, force_driver_value) ;

        // pull this value through the type converter :
        idx_val = (idx_val) ? idx_val->ToType(_prefix_id, this) : 0 ;

        return idx_val ;
    }

    if (_is_index_constraint || _is_record_constraint) {
        // Its an index constraint
        Error("index constraint illegal in expression") ;
        return 0 ;
    }

    if (_is_function_call) {
        if (!_prefix_id) return 0 ;
        // Function call
        return _prefix_id->ElaborateSubprogram(_assoc_list, target_constraint, df, this, 0) ;
    }

    if (_is_array_slice) {
        VERIFIC_ASSERT(_assoc_list && _assoc_list->Size()==1) ; // or type-inference messed up badly

        // Get the prefix constraint
        VhdlConstraint *prefix_constraint ;
        VhdlConstraint *allocated_prefix_constraint = 0 ; // if prefix constraint is allocated.
        if (_prefix_id) {
            // No need to make a constraint copy
            prefix_constraint = _prefix_id->Constraint() ; // just a pointer
        } else {
            // Evaluate the constraint. This uses memory, so put in a storage pointer.
            allocated_prefix_constraint = (_prefix) ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ; // allocated new constraint
            prefix_constraint = allocated_prefix_constraint ;
        }
        if (!prefix_constraint) return 0 ;

        // Do not evaluate the prefix value until we really need it. Do all constraint tests first.
        // Otherwise, static elaboration will bail out too soon.

        // Evaluate the slice range
        assoc = (_assoc_list) ? (VhdlDiscreteRange*)_assoc_list->At(0) : 0 ;
        if (!assoc) {
            delete allocated_prefix_constraint ;
            return 0 ;
        }

        VhdlConstraint *range = assoc->EvaluateConstraintInternal(df, 0) ;
        if (!range) {
            delete allocated_prefix_constraint ;
            return 0 ;
        }

        // Now elaborate the value :
        VhdlValue *prefix = 0 ; // the prefix value
        unsigned prefix_allocated = 0 ; // flag if prefix allocated (and thus must be freed)

        // Viper #5002 : Pick up the alias target value if target is FullFormal
        VhdlName *alias_target = (_prefix_id && _prefix_id->IsAlias()) ? _prefix_id->GetAliasTarget() : 0 ;
        if (alias_target && !alias_target->FindFullFormal()) alias_target = 0 ;

        // Viper 6871 and 6989 : Do similar time optimizations for reference target in Indexed name evaluation
        VhdlDiscreteRange *ref_target = (_prefix_id && _prefix_id->IsRefFormal()) ? _prefix_id->GetRefTarget() : 0 ;
        if (ref_target && !ref_target->FindFullFormal()) ref_target = 0 ;

        if (_prefix_id && _prefix && _prefix->FindFullFormal() && (!_prefix_id->IsAlias() || alias_target)
                                                               && (!_prefix_id->IsRefFormal() || ref_target)
                                                               && !_prefix_id->IsSubprogram() && !_prefix_id->IsRecordElement()) {
            // VIPER #3720 : Fix long runtime by preventing unnecessary copy/delete
            // Prefix is a single, full identifier, which is not an alias and not a subprogram,
            // and not a record element. In that case, we can simply pick-up the identifier value,
            // without making a copy.

            // Pick up the prefix value (as just a pointer)
            VhdlIdDef *alias_target_id = _prefix_id->GetTargetId() ; // when alias is full_formal
            // Viper 6871 and 6989 Ref target needs to be Evaluated
            VhdlIdDef *ref_target_id = ref_target ? ref_target->FindFullFormal() : 0 ;
            prefix = EvaluateId(alias_target_id ? alias_target_id : (ref_target_id ? ref_target_id : _prefix_id), df, force_driver_value) ;

            // Viper #7288: Since value carries the constraint, and the constraint for
            // alias-id and target can be different, we here need an new allocated value,
            // cannot work with the prefix value any more
            VhdlIdDef *target_id = alias_target_id ;
            if (!target_id) target_id = ref_target_id ;
            if (target_id && prefix && prefix->GetRange()) {
                prefix = prefix->Copy() ;
                prefix_constraint = prefix_constraint->Copy() ;
                prefix_allocated = 1 ;
                prefix->ConstraintBy(prefix_constraint) ;
            }
        } else {
            // prefix is too complex for plain pick-up. Evaluate it :

            // Pass the constraint in there for if the prefix is a function call :
            // adjustment will be needed if prefix is a function call returning a unconstrained type
            // That is dynamic elaboration.
            prefix = (_prefix) ? _prefix->Evaluate(prefix_constraint, df, force_driver_value) : 0 ;

            // flag that 'prefix' is allocated
            prefix_allocated = 1 ;
        }

        if (prefix && prefix->GetRange()) { // Viper #7288: range on composite-value
            delete allocated_prefix_constraint ;
            allocated_prefix_constraint = 0 ;
            prefix_constraint = prefix->GetRange() ; // For constraint check below...
        } else if (prefix && prefix->IsAccessValue() && prefix->Constraint()) {
            // Viper : 3764/3771
            // prefix_constraint may not be the correct constraint, as for array of access_value of
            // unconstrained array i.e. array(0 to 5) of access bit_vector, each of these 6 dimensions
            // could have different lengths. The constraint on the AccessValue reflects the correct
            // updated constraint for this access_type variable
            delete allocated_prefix_constraint ;
            allocated_prefix_constraint = 0 ;
            prefix_constraint = prefix->Constraint() ;
            // This is done before the constraint check below...
        }

        if (!prefix_constraint) return 0 ; // redundant null check to fool gimpel

        // VIPER 3157 : Continue even if 'prefix' is NULL : We still want to do constraint tests without a value (for static elaboration for example).
        // If we ran static elaboration, then the prefix constraint can still be 'unconstrained'.
        // So careful with the tests below...

        // Test the slice range
        // Only test slice direction if the prefix_constraint is NOT unconstrained.
        if (range->Dir()!=prefix_constraint->Dir() && !prefix_constraint->IsUnconstrained()) {
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                Warning("slice direction differs from its index type range") ;
            } else {
                Error("slice direction differs from its index type range") ;
            }
            delete range ;
            delete allocated_prefix_constraint ;
            if (prefix_allocated) delete prefix ;
            return 0 ;
        }
        // Other tests can be done even if prefix_constraint is unconstrained.
        if (range->IsNullRange()) {
            // VIPER #7297 :Adjust the target_constraint to the slice, if it is unconstrained.
            // Do this, since the slice range defines the returned 'subtype' constraint.
            if (target_constraint && target_constraint->IsUnconstrained()) {
                target_constraint->IndexConstraint()->ConstraintBy(range) ;
            }
            delete range ;
            delete allocated_prefix_constraint ;
            if (prefix_allocated) delete prefix ;
            // create a empty composite value
            return new VhdlCompositeValue(new Array()) ;
        } else if (!prefix_constraint->Contains(range->Left())) {
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                Warning("left bound of slice is outside its index type range") ;
            } else {
                Error("left bound of slice is outside its index type range") ;
            }
            delete range ;
            delete allocated_prefix_constraint ;
            if (prefix_allocated) delete prefix ;
            return 0 ;
        } else if (!prefix_constraint->Contains(range->Right())) {
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                Warning("right bound of slice is outside its index type range") ;
            } else {
                Error("right bound of slice is outside its index type range") ;
            }
            delete range ;
            delete allocated_prefix_constraint ;
            if (prefix_allocated) delete prefix ;
            return 0 ;
        }

        // Adjust the target_constraint to the slice, if it is unconstrained.
        // Do this, since the slice range defines the returned 'subtype' constraint.
        if (target_constraint && target_constraint->IsUnconstrained()) {
            target_constraint->IndexConstraint()->ConstraintBy(range) ;
        }

        // Now if prefix value was null, its time to bail out :
        if (!prefix) {
            delete range ;
            delete allocated_prefix_constraint ;
            return 0 ;
        }

        // Build result value
        // Define lower and upper bounds into the value
        unsigned left_pos = prefix_constraint->PosOf(range->Left()) ;
        unsigned right_pos = prefix_constraint->PosOf(range->Right()) ;
        VERIFIC_ASSERT(left_pos<=right_pos) ; // Above rules should guarantee this

        Array *values = new Array((right_pos - left_pos) + 1) ;
        VhdlValue *elem_val ;

        // Stick the resulting elements into the new array
        unsigned i ;
        for (i=left_pos; i<=right_pos; i++) {
            if (prefix_allocated) {
                elem_val = prefix->TakeValueAt(i) ;
            } else {
                elem_val = prefix->ValueAt(i) ;
                if (elem_val) elem_val = elem_val->Copy() ;
            }
            values->InsertLast(elem_val) ;
        }
        if (prefix_allocated) delete prefix ;
        delete range ;
        delete allocated_prefix_constraint ;
        return new VhdlCompositeValue(values) ;
    }

    if (_is_array_index) {
        // Regular indexed array
        if (!_prefix) return 0 ; // cannot do anything without a prefix

        // Evaluate the prefix value and constraint

        // Viper #2927 : Do not evaluate (and therefore copy) the prefix value and constraint
        // if prefix is a simple identifier, simply pick up the prefix value and be careful not to delete it.
        VhdlValue *prefix = 0 ; // the prefix value
        VhdlConstraint *prefix_constraint ; // the prefix constraint
        unsigned prefix_allocated = 0 ; // flag if prefix and prefix_constraint are allocated (and thus must be freed)

        VhdlIdDef *prefix_id = _prefix_id ;
        VhdlName *prefix_name = _prefix ;
        Array *assoc_list = _assoc_list ;

        Array index_list ;
        if (vhdl_file::ExtractMPRams()) { // Viper #7806: check runtime flags.
            // Viper #3925 : Get back through the prefix list
            if (IsRamRead(this, prefix_id, prefix_name, index_list)) assoc_list = &index_list ;
        }

        // Viper #5002 : Pick up the alias target value if target is FullFormal
        VhdlName *alias_target = (prefix_id && prefix_id->IsAlias()) ? prefix_id->GetAliasTarget() : 0 ;
        if (alias_target && !alias_target->FindFullFormal()) alias_target = 0 ;

        // Viper 6871 and 6989 : Do similar time optimizations for reference target in Indexed name evaluation
        VhdlDiscreteRange *ref_target = (prefix_id && prefix_id->IsRefFormal()) ? prefix_id->GetRefTarget() : 0 ;
        if (ref_target && !ref_target->FindFullFormal()) ref_target = 0 ;

        if (prefix_id && prefix_name->FindFullFormal() && (!prefix_id->IsAlias() || alias_target)
                                                       && (!prefix_id->IsRefFormal() || ref_target)
                                                       && !prefix_id->IsSubprogram() && !prefix_id->IsRecordElement()) {
            // Prefix is a single, full identifier, which is not an alias and not a subprogram, and not a record element.
            // In that case, we can simply pick-up the identifier value, without making a copy.

            // Pick up the prefix constraint :
            prefix_constraint = prefix_id->Constraint() ; // just a pointer

            // Pick up the prefix value (as just a pointer)
            VhdlIdDef *alias_target_id = prefix_id->GetTargetId() ; // when alias is full_formal
            // Viper 6871 and 6989 Ref target needs to be Evaluated
            VhdlIdDef *ref_target_id = ref_target ? ref_target->FindFullFormal() : 0 ;
            prefix = EvaluateId(alias_target_id ? alias_target_id : (ref_target_id ? ref_target_id : prefix_id), df, force_driver_value) ;

            // Viper #7288: Since value carries the constraint, and the constraint for
            // alias-id and target can be different, we here need an new allocated value,
            // cannot work with the prefix value any more
            VhdlIdDef *target_id = alias_target_id ;
            if (!target_id) target_id = ref_target_id ;
            if (target_id && prefix && prefix->GetRange()) {
                prefix = prefix->Copy() ;
                prefix_constraint = prefix_constraint->Copy() ;
                prefix_allocated = 1 ;
                prefix->ConstraintBy(prefix_constraint) ;
            }
        } else {
            // prefix is too complex for plain pick-up. Evaluate it :

            prefix_constraint = (prefix_name) ? prefix_name->EvaluateConstraintInternal(df, 0) : 0 ; // evaluate the prefix constraint
            // Pass the constraint in there for if the prefix is a function call :
            // adjustment will be needed if prefix is a function call returning a unconstrained type
            prefix = (prefix_name) ? prefix_name->Evaluate(prefix_constraint, df, force_driver_value) : 0 ;

            // flag that 'prefix' and 'prefix_constraint' are allocated
            prefix_allocated = 1 ;
        }

        if (prefix && prefix->IsAccessValue() && prefix->Constraint()) {
            // Viper : 3764/3771
            // prefix_constraint may not be the correct constraint, as for array of accexx_value of
            // unconstrained array i.e. array(0 to 5) of access bit_vector, each of these 6 dimensions
            // could have different lengths. The constraint on the AccessValue reflects the correct
            // updated constraint for this access_type variable
            if (prefix_allocated) delete prefix_constraint ;
            prefix_constraint = prefix->Constraint() ;
            if (prefix_allocated && prefix_constraint) prefix_constraint = prefix_constraint->Copy() ;
            // This is done before the constraint check below...
        }

        // Test if we have the prefix constraint.
        // Allow 'prefix' (the value) to be 0, so that static elab can do proper constraint tests.
        if (!prefix_constraint) {
            if (prefix_allocated) { delete prefix ; delete prefix_constraint ; }
            return 0 ;
        }

        // Now get result by decending into 'result', according to assoc's index value(s)
        // Evaluate the indexes (all dimensions)
        VhdlConstraint *constraint = prefix_constraint ;
        VhdlValue *result = prefix ;
        unsigned result_allocated = prefix_allocated ;
        Array *address_nets = 0 ; // collect address bits for RAM port (if needed)
        unsigned pos ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
            if (!assoc) break ;

            // VIPER #3164 : Issue error if indexing is on null range
            if (!constraint || (IsStaticElab() && constraint->IsNullRange())) {
                // VIPER #7286 : Produce warning instead of error for non-constant data flow
                if (df && df->IsNonconstBranch()) {
                    Warning("no index value can belong to null index range") ;
                } else {
                    Error("no index value can belong to null index range") ;
                }
                if (result_allocated) delete result ;
                result = 0 ;
                break ;
            }
            // Allow 'result' to be 0, so that static elab can do proper constraint checks.
            // if (!result) break ; // Something is wrong

            // Evaluate the index expression :
            idx_val = assoc->Evaluate(0,df,0) ;
            if (!idx_val) {
                // Something wrong..
                if (result_allocated) delete result ;
                result = 0 ;
                break ;
            }

            if (idx_val->IsConstant()) {
                // Constant indexed array

                // Make sure to work with the real constant :
                idx_val = idx_val->ToConst(constraint->Left()->GetType()) ;

                VhdlConstraint *result_range = constraint ; // Viper #7288
                if (result && result->GetRange()) result_range = result->GetRange() ;
                VERIFIC_ASSERT(result_range) ;

                // Check if it's within the constraint.
                if (!result_range->Contains(idx_val)) {
                    char *v = idx_val->Image() ;
                    char *r = result_range->Image() ;
                    // VIPER #3815 : produce warning for non-constant branch
                    if (df && df->IsNonconstBranch()) {
                        assoc->Warning("index value %s is out of range %s", v, r) ;
                    } else {
                        assoc->Error("index value %s is out of range %s", v, r) ;
                    }
                    Strings::free(v) ;
                    Strings::free(r) ;
                    delete idx_val ;
                    if (result_allocated) delete result ;
                    result = 0 ;
                    break ;
                }

                // Get the hard left->right position of this value in the array
                pos = result_range->PosOf(idx_val) ;
                delete idx_val ;

                // Now decent into the proper element
                VhdlValue *tmp = (result) ? (result_allocated ? result->TakeValueAt(pos) : result->ValueAt(pos)) : 0 ;
                if (result_allocated) delete result ;
                result = tmp ;
            } else if (IsRtlElab()) {
            }

            // And decent the index cnstraint to the next element
            constraint = constraint->ElementConstraint() ;

            // continue with the next dimension
        }

        // Adjust the target constraint to this element's constraint :
        if (result && target_constraint && target_constraint->IsUnconstrained()) {
            target_constraint->ConstraintBy(constraint) ;
        }

        // cleanup
        delete address_nets ;
        if (prefix_allocated) delete prefix_constraint ; // If the prefix was not an object, the prefix constraint was allocated.

        if (result && !result_allocated) result = result->Copy() ;
        return result ;
    }

    // type-inference has failed
    return 0 ; // Can this still happen ?
}

VhdlValue *VhdlSignaturedName::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)
{
    if (_unique_id) {
        //VhdlValue *val ;
        VhdlValue *val = EvaluateId(_unique_id, df, force_driver_value) ;

        if (target_constraint && target_constraint->IsUnconstrained() && val) {
            // Constraint by this id's constraint
            target_constraint->ConstraintBy(_unique_id->Constraint()) ;
        }

        return (val) ? val->Copy() : 0 ;
    }
    // Signatured name should always have _single_id
    return 0 ;
}
VhdlValue *VhdlAttributeName::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned force_driver_value)
{
    if (!_prefix || !_designator) return 0 ;

    VhdlValue *result = 0 ;
    VhdlConstraint *pre, *prefix_constraint ;

    if (!_attr_enum) {
        // user defined attribute
        // prefix should be a single identifier
        // This is already tested in analyzer, so use GetId().
        VhdlIdDef *prefix_id = _prefix->GetId() ;
        if (!prefix_id) return 0 ;

        // Same for the attribute designator :
        VhdlIdDef *attr = _designator->GetId() ;
        if (!attr) return 0 ;

        // LRM 6.6 : If prefix is an aliased name, attribute applies to the alias target
        // except when attribute is (predefined) simple_name/path_name/inst_name
        // Hence for user defined attribute, we should always get the alias target
        if (prefix_id->IsAlias()) prefix_id = prefix_id->GetTargetId() ;
        if (!prefix_id) return 0 ;

        // Get the expression for this attribute on the prefix_id :
        VhdlExpression *attr_expr = prefix_id->GetAttribute(attr) ;

        if (attr_expr) {
            // Viper #3637/6805: The sttribute spec must be present before this
            VhdlAttributeSpec *attr_spec = attr->FindRelevantAttributeSpec(prefix_id) ;
            if (attr_spec && LineFile::LessThan(Linefile(), attr_spec->Linefile())) attr_expr = 0 ;
        }

        // FIX ME : We could do this check in the analyzer :
        if (!attr_expr) {
            Error("%s does not have attribute %s", prefix_id->Name(), _designator->Name()) ;
            return 0 ;
        }

        // Elaborate the expression :
        // Note : LRM not entirely clear on elaboration of user-defined attributes.
        // They only speak of attribute spec elaboration (LRM 12.3.2.1).
        // We are in essence (re)elaborating the attribute specification. So :
        // If attribute constraint is NOT unconstrained, use that as the context (target) constraint into the attribute expression.
        // If attribute constraint IS unconstrained, then do NOT pass it in : attr can be used for many attribute specifications, so we do not want one to constrain it.
        VhdlConstraint *attr_constraint = attr->Constraint() ;
        if (attr_constraint && !attr_constraint->IsUnconstrained()) {
            result = attr_expr->Evaluate(attr_constraint, df, force_driver_value) ;
        } else {
            // use the target constraint, so that potentially if can be constrained by the attr expression.
            result = attr_expr->Evaluate(target_constraint, df, force_driver_value) ;
        }

        // Check against target constraint ?
        return result ; // Got it
    }

    verific_int64 dim = 0 ;
    verific_int64 pos ;
    VhdlConstraint *index_constraint = 0 ;
    switch(_attr_enum) {
    case VHDL_ATT_base :
    case VHDL_ATT_element : // VIPER #7502: vhdl_2008: new predefined attribute
    case VHDL_ATT_range :
    case VHDL_ATT_reverse_range :
        Error("attribute %s illegal in expression", _designator->Name()) ;
        return 0 ;

    case VHDL_ATT_left :
    case VHDL_ATT_right :
    case VHDL_ATT_high :
    case VHDL_ATT_low :
    case VHDL_ATT_ascending :
        // Evaluate the prefix :
        prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;
        if (!prefix_constraint) return 0 ;

        if (prefix_constraint->IsUnconstrained()) { // Viper #7288: access id remains unconstrained
            VhdlValue *prefix_value = _prefix ? _prefix->Evaluate(0, df, 0) : 0 ;
            prefix_constraint->ReConstraintByValue(prefix_value) ;
            delete prefix_value ;
        }

        // As per LRM section 14, the attributes only apply to constrained type/objects
        // VIPER #7878 : Check constraint of index only, as left/right and some other
        // attributes are applicable for index only
        index_constraint = prefix_constraint->IndexConstraint() ;
        if (index_constraint && index_constraint->IsUnconstrained()) {
            delete prefix_constraint ;
            return 0 ;
        }

        // if prefix is a type, return type's constraint bounds
        // if prefix is an array, return array's constraint bounds
        pre = prefix_constraint ;
        // VIPER 2983 : the parameter can contain a full (static) expression, not just a literal.
        // In fact, this is somwhat of a hack. We SHOULD create a full locally static expression evaluator instead of EvaluateConstantInteger.
        // That one should error out if the expression is not truly locally static.
        // For now, call full elaboration on it, not EvaluateConstanInteger :
        // dim = (_expression) ? _expression->EvaluateConstantInteger() - 1 : 0 ;
        dim = 0 ; // default (scalar) is dimension 0.
        if (_expression) {
            VhdlValue *param_val = _expression->Evaluate(0,0/*must be static, so no df required*/,0) ;
            if (param_val) {
                if (!param_val->IsConstant()) _expression->Error("expression is not constant") ;
                dim = param_val->Integer() - 1 ; // parameter value '1' means first dimension. So that is index 0.
            }
            delete param_val ;
        }
        // decent to the right dimension :
        while (dim-- != 0 && pre) pre = pre->ElementConstraint() ;
        // check dimension range :
        if (!pre || (_expression && !pre->IsArrayConstraint())) { // Viper #4499 : Added check IsArrayConstraint
            // assume we did not yet check dimensionality in analyzer :
            Error("near %s ; attribute dimensionality incorrect", _designator->Name()) ;
            pre = prefix_constraint ; // default back to first dimension.
        }

        switch (_attr_enum) {
        case VHDL_ATT_left : result = pre->Left()->Copy() ; break ;
        case VHDL_ATT_right : result = pre->Right()->Copy() ; break ;
        case VHDL_ATT_high : result = pre->High()->Copy() ; break ;
        case VHDL_ATT_low : result = pre->Low()->Copy() ; break ;
        case VHDL_ATT_ascending : result = (pre->Dir()==VHDL_to) ? new VhdlNonconstBit(Pwr()) : new VhdlNonconstBit(Gnd()) ; break ;
        default : result = 0 ; break ;
        }
        delete prefix_constraint ;
        return result ;

    case VHDL_ATT_image :
        {
        // Need to return a string (array of character), so need result type (set in analyzer).
        if (!_result_type || !_expression) return 0 ; // analysis failed
        // Evaluate the expression :
        result = _expression->Evaluate(0,df,0) ;
        if (!result) return 0 ;

        // Only handle constant argument (cannot generate a non-constant image)
        if (!result->IsConstant()) {
            Error("expression is not constant") ; // FIX ME : better message ?
            delete result ;
            return 0 ;
        }

        // LRM 14.1 : "it is an error if the parameter value does
        // not belong to the subtype implied by the prefix" :
        if (!_prefix_type || !result->CheckAgainst(_prefix_type->Constraint(),/*from*/_expression, df)) {
            delete result ;
            // error is already given
            return 0 ;
        }

        // Build the string representation for this value.
        // Image() follow the rules of building the string pretty darn close.
        char *image = result->Image() ;
        if (!image) return 0 ; // something bad happened.
        delete result ; // no longer used.

        if (_prefix_type->IsPhysicalType()) { // Viper #5566
            Map *unit_id_map = _prefix_type->GetTypeDefList() ;
            const char *unit_name = (_prefix_type == StdType("time")) ? vhdl_file::GetTimeUnit() : 0 ;
            if (unit_name) {
                char *tmp = image ;
                image = Strings::save(tmp, " ", unit_name) ;
                Strings::free(tmp) ;
            } else {
                MapIter mi ;
                VhdlPhysicalUnitId *unit_id ;
                FOREACH_MAP_ITEM(unit_id_map, mi, &unit_name, &unit_id) {
                    if (!unit_name || !unit_id) continue ;
                    if (unit_id->Position() == 1) {
                        char *tmp = image ;
                        image = Strings::save(tmp, " ", unit_name) ;
                        Strings::free(tmp) ;
                        break ;
                    }
                }
            }
        }

        // Need to create a composite value (of character VhdlEnum's) now
        VhdlIdDef *char_type = _result_type->ElementType() ;
        VERIFIC_ASSERT(char_type) ; // analysis should have guaranteed this
        Array *elements = new Array((unsigned)Strings::len(image)) ;
        VhdlValue *elem_val ;
        VhdlIdDef *char_id ;
        char buffer[4] ;
        char *tmp = image ;
        while (*tmp != '\0') {
            // Find the right character (enum) identifier.
            ::sprintf(buffer,"'%c'",*tmp++) ;
            char_id = (char_type) ? char_type->GetEnum(buffer) : 0 ;
            VERIFIC_ASSERT(char_id) ; // analysis (type-inference) should have guaranteed this too.
            // Create a VhdlEnum value with that :
            elem_val = new VhdlEnum(char_id) ;
            // And insert that in the array :
            elements->InsertLast(elem_val) ;
        }
        Strings::free(image) ;
        // Return the composite value :
        return new VhdlCompositeValue(elements,/*unsigned*/0) ;
        }
        break ;

    case VHDL_ATT_value :
        {
        // We need the prefix type for type'value("image") :
        if (!_prefix_type) return 0 ; // type-inference already issued a message

        // Evaluate the string argument :
        result = (_expression) ? _expression->Evaluate(0,df,0) : 0 ;
        if (!result) return 0 ;

        // Only handle constant arguments (cannot find value of non-constant image)
        if (!result->IsConstant()) {
            Error("expression is not constant") ; // FIX ME : better message ?
            delete result ;
            return 0 ;
        }

        // Get the 'image' of this argument :
        char *image = result->Image() ;
        delete result ;

        char *str = image ;
        // Image is a string, so strip up the first and  last character ("'s).
        if (str[0]=='"') str++ ;
        if (str[Strings::len(str) - 1]=='"') str[Strings::len(str) - 1] = '\0' ;
        // LRM 14.1 `value : strip off leading and trailing whitespace
        while (str[0]==' ') str++ ;
        while (str[Strings::len(str) - 1]==' ') str[Strings::len(str) - 1] = '\0' ;

        result = 0 ;
        // prefix type can only be a scalar type. Handle each one separate :
        if (_prefix_type->IsEnumerationType()) {
            // enum type. Expect identifiers or character literal. Either way, lookup by name.
            // If it is a plain identifier (not a character literal), downcase the string :
            if (str[0]!='\'') str = Strings::strtolower(str) ;
            VhdlIdDef *enum_id = _prefix_type->GetEnum(str) ;
            if (!enum_id) {
                Error("%s is not declared in %s",str,_prefix_type->Name()) ;
            } else {
                result = new VhdlEnum(enum_id) ;
            }
        } else if (_prefix_type->IsPhysicalType()) {
            // physical type : can have unit name with or without an abstract literal.
            // If there is a abstract literal, then there is a whitespace between it and the unit.
            // Either way, down-case this thing (for unit lookup)
            if (str[0]!='\'') str = Strings::strtolower(str) ;
            verific_int64 val = 1 ;
            // Check if there is a prefix of abstract literal :
            if (strchr(str,' ')) {
                // FIX ME : can be 'real' prefix also..
                val = Strings::atoi(str) ;
                str = strchr(str,' ') + 1 ; // jump to the unit
            }
            // Now pick up the unit :
            VhdlIdDef *unit_id = _prefix_type->GetUnit(str) ;
            if (!unit_id) {
                Error("%s is not declared in %s",str,_prefix_type->Name()) ;
            } else {
                // Multiply abstract literal by the value of the unit :
                val = val * (verific_int64)unit_id->Position() ;
                // Create a physical value
                result = new VhdlPhysicalValue(val) ;
            }
        } else if (_prefix_type->IsRealType()) { // Viper #5776: Moved before IsNumericType check
            // real number. Expect a real literal, or based literal.
            // FIX ME for based literals.
            double val = Strings::atof(str) ;
            result = new VhdlDouble(val) ;
        } else if (_prefix_type->IsNumericType()) {
            // integer type. Expect an integer literal, or based literal.
            // FIX ME for based literals.
            int val = Strings::atoi(str) ;
            result = new VhdlInt((verific_int64)val) ;
        }

        Strings::free(image) ;
        return result ;
        }
        break ;

    case VHDL_ATT_pos :
        result = (_expression) ? _expression->Evaluate(0,df,0) : 0 ;
        if (!result) return 0 ;

        // VIPER #7549: It is an error if the value of the parameter
        // does not belong to the subtype implied by the prefix.
        // Get prefix' constraint (for 'leftof and 'rightof)
        prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;
        if (!prefix_constraint) return 0 ;

        if (prefix_constraint->IsUnconstrained()) { // Viper #7288: access id remains unconstrained
            VhdlValue *prefix_value = _prefix ? _prefix->Evaluate(0, df, 0) : 0 ;
            prefix_constraint->ReConstraintByValue(prefix_value) ;
            delete prefix_value ;
        }

        // Check the value of expression against prefix constraint
        if (result->IsConstant()) {
            if (!prefix_constraint->Contains(result)) {
                Error("parameter value of attribute '%s is out of range", _designator->Name()) ;
                delete prefix_constraint ;
                delete result ;
                return 0 ;
            }
        }

        // Get the position of this value
        if (_prefix_type && _prefix_type->IsIntegerType()) {
            // result unchanged, since since POS is the integer value itself
        } else if (_prefix_type && _prefix_type->IsPhysicalType()) {
            // VIPER #3435: For non-constant values, result is unchanged
            if (result->IsConstant()) {
                // Create a universal integer here for constant physical values:
                pos = result->Position() ;
                delete result ;
                result = new VhdlInt(pos) ;
            }
        } else if (_prefix_type && _prefix_type->IsEnumerationType()) {
            // If the expression is constant, we should always get the enum Position().
            // Otherwise, it's a non-constant.
            if (result->IsConstant()) {
                pos = result->Position() ;
                delete result ;
                result = new VhdlInt(pos) ;
            } else {
                // Transform nonconstant enum -> integer value.
                // Simple pass-through for binary encoded enums.
                // FIX ME for user-encoded types (including onehot and bit-encoded!).
                result = result->ToNonconst() ;
                // VIPER #7517: issue an warning for onehotencoded values.
                if (result && (result->OnehotEncoded() || result->UserEncoded())) {
                    // RTL synthesis IEEE 1076.6 section 7.1.8 : allow user-encoded values, but issue warning :
                    Warning("magnitude comparator on user-encoded values can create simulation-synthesis differences") ;
                }
            }
        }
        delete prefix_constraint ; // Memory leak fix
        return result ;
        break ; // Should never happen if type-checking worked

    case VHDL_ATT_val :
        result = (_expression) ? _expression->Evaluate(0,df,0) : 0 ;
        if (!result) return 0 ;
        // 'result' is a (integer) scalar expression, indicating the 'position' in the prefix type.
        // Get the value of this position
        if (_prefix_type && _prefix_type->IsIntegerType()) {
            // since VAL is the integer position itself
            return result ;
        } else if (_prefix_type && _prefix_type->IsPhysicalType()) {
            // VIPER #3435: For non-constant values, result is unchanged
            if (!result->IsConstant()) return result ;
            // Need to return a physical value from here:
            pos = result->Position() ;
            delete result ;
            return new VhdlPhysicalValue(pos) ;
        } else if (_prefix_type && _prefix_type->IsEnumerationType()) {
            // Transform integer position -> selected enum :
            if (result->IsConstant()) {
                pos = result->Position() ;
                delete result ;
                VhdlIdDef *the_value = _prefix_type->GetEnumAt((unsigned)pos) ;
                if (!the_value) {
                    Error("result of attribute %s is not in range of type %s", _designator->Name(), _prefix_type->Name()) ;
                    return 0 ;
                }
                return new VhdlEnum(the_value) ;
            } else {
                // Transform nonconstant integer -> enum value.
                if (_prefix_type->Constraint() && _prefix_type->Constraint()->IsBitEncoded()) {
                    // Bit-encoded result : if the 'position' matches the enum value with encoding '1',
                    // then the result is '1'. Otherwise '0'. (we don't create 'Z').
                    // This code changed from (incorrect) bit-translation (for issue 1927).
                    VhdlIdDef *one_id = _prefix_type->GetEnumWithBitEncoding('1') ;
                    // and translate to a 'bit' value :
                    result = result->BinaryOper(new VhdlInt((one_id) ? (verific_int64)one_id->Position() : 1), VHDL_EQUAL, this) ;
                } else {
                    // result is already 'nonconst'.
                    // Simple pass-through for binary-encoded types.
                    // FIX ME for user-encoded types (including onehot).
                    // VIPER #7517: issue an warning for onehotencoded values.
                    if (result->OnehotEncoded() || result->UserEncoded()) {
                        // RTL synthesis IEEE 1076.6 section 7.1.8 : allow user-encoded values, but issue warning :
                        Warning("magnitude comparator on user-encoded values can create simulation-synthesis differences") ;
                    }
                    // VIPER #8484: Transfer position to enum by inserting
                    // a mux
                    VhdlNonconst *idx = result->ToNonconst() ;
                    // Calculate the range of values that this 'idx' can represent.
                    int max = ( 1 << ( (idx->IsSigned()) ? idx->NumBits() - 1 : idx->NumBits() )) - 1;
                    int min = (idx->IsSigned()) ? (- (1 << (idx->NumBits() - 1))) : 0 ;
                    // Now check if this is always out of range of the index (target) constraint :
                    VhdlConstraint *constraint = _prefix_type->Constraint() ;
                    if (constraint && ((max < constraint->Low()->Position()) ||
                        (min > constraint->High()->Position()))) {
                    } else if (constraint) {
                        // Calculate how many bits we need to decode all of the array range, and if it is signed or not.
                        unsigned bits = constraint->NumBitsOfRange() ;
                        // Restrict number of bits to the max of the index value
                        if (bits > idx->NumBits()) {
                            bits = idx->NumBits() ;
                        }
                        // Restrict the condition to this size
                        // bits is the number of bits needed to decode the values
                        idx->Adjust(bits) ;
                        // Now calculate all the values we need (2**bits values : (2**N)-1 downto 0 )
                        unsigned top = 1 ;
                        while (bits-- != 0) top*=2 ;
                        Array idx_values(top) ; // temporary array
                        VhdlValue *elem_val ;
                        int j ;
                        for (j=((int)top-1); j >= 0 ; j--) {
                            VhdlIdDef *enum_id = _prefix_type->GetEnumAt((unsigned)j) ;
                            elem_val = enum_id ? enum_id->Value(): 0 ;
                            idx_values.InsertLast(elem_val) ;
                        }
                        VhdlValue *tmp = VhdlValue::Nto1Mux(idx, &idx_values,/*from*/this) ;
                        result = tmp ;
                        delete idx ;
                    }
                }
                return result ;
            }
        }
        break ; // Should never happen if type-checking worked

    case VHDL_ATT_succ :
    case VHDL_ATT_pred :
    case VHDL_ATT_leftof :
    case VHDL_ATT_rightof :
        result = (_expression) ? _expression->Evaluate(0,df,0) : 0 ;
        if (!result) return 0 ;

        // Get prefix' constraint (for 'leftof and 'rightof)
        prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;
        if (!prefix_constraint) return 0 ;

        if (prefix_constraint->IsUnconstrained()) { // Viper #7288: access id remains unconstrained
            VhdlValue *prefix_value = _prefix ? _prefix->Evaluate(0, df, 0) : 0 ;
            prefix_constraint->ReConstraintByValue(prefix_value) ;
            delete prefix_value ;
        }

        // VIPER #3480 : Check the value of expression against prefix constraint
        if (result->IsConstant()) { // VIPER #7517: do not error out for nonconstant value
            if (!prefix_constraint->Contains(result)) {
                Error("parameter value of attribute '%s is out of range", _designator->Name()) ;
                delete prefix_constraint ;
                delete result ;
                return 0 ;
            }
        } else { // not constant
            // VIPER #7517: issue an warning for onehotencoded values.
            if (result->OnehotEncoded() || result->UserEncoded()) {
                // RTL synthesis IEEE 1076.6 section 7.1.8 : allow user-encoded values, but issue warning :
                Warning("magnitude comparator on user-encoded values can create simulation-synthesis differences") ;
                delete prefix_constraint ;
                delete result ;
                return 0 ;
            }
        }

        // These attributes are just incrementor/decrementors on discrete types (enum/integer)
        // bound checks are done inside the unary operation.
        // FIX ME : nonconstant bound checks wont work (they just create integer value)
        switch (_attr_enum) {
        case VHDL_ATT_succ : result = result->UnaryOper(VHDL_INCR,this) ; break ;
        case VHDL_ATT_pred : result = result->UnaryOper(VHDL_DECR,this) ; break ;
        case VHDL_ATT_leftof : result =  result->UnaryOper((prefix_constraint->Dir()==VHDL_to) ? VHDL_DECR : VHDL_INCR,this) ; break ;
        case VHDL_ATT_rightof : result = result->UnaryOper((prefix_constraint->Dir()==VHDL_to) ? VHDL_INCR : VHDL_DECR,this) ; break ;
        default : break ;
        }

        delete prefix_constraint ;
        return result ;

    case VHDL_ATT_length :
        prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;
        if (!prefix_constraint) return 0 ;

        if (prefix_constraint->IsUnconstrained()) { // Viper #7288: access id remains unconstrained
            VhdlValue *prefix_value = _prefix ? _prefix->Evaluate(0, df, 0) : 0 ;
            prefix_constraint->ReConstraintByValue(prefix_value) ;
            delete prefix_value ;
        }

        // As per LRM section 14, the attributes only apply to constrained type/objects
        if (prefix_constraint->IsUnconstrained()) {
            delete prefix_constraint ;
            return 0 ;
        }

        pre = prefix_constraint ;
        dim = (_expression) ? _expression->EvaluateConstantInteger() - 1 : 0 ;
        while (dim-- != 0 && pre) pre = pre->ElementConstraint() ;
        if (!pre) return 0 ; // something bad happened

        if (pre->IsNullRange()) {
            delete prefix_constraint ;
            return new VhdlInt((verific_int64)0) ;
        }
        result = new VhdlInt((verific_int64)pre->Length()) ;
        delete prefix_constraint ;
        return result ;

    case VHDL_ATT_delayed :
        // Signal is delayed, which is ignored
        return _prefix->Evaluate(0,df,0) ;

    case VHDL_ATT_event :
        result = _prefix->Evaluate(0,df,0) ;
        if (!result) return 0 ;
        if (result->IsConstant()) {
            // VIPER #3596: Event attribute on constant is always false:
            delete result ;
            result = new VhdlNonconstBit(Gnd()) ;
        } else if (result->NumBits()!=1) {
            if (IsRtlElab()) Error("attribute %s on multiple bits is not synthesizable", _designator->Name()) ;
            delete result ;
            result = 0 ;
        } else {
            VhdlValue *tmp = result ;
            Net *bit_net = result->GetBit() ;
            // Produce event value when 'GetBit' returns something. 'GetBit' can
            // return null for composite value (may occur if some errors are ignored)
            result = (bit_net) ? new VhdlEvent(bit_net,1): 0 ;
            delete tmp ;
        }
        return result ;

    case VHDL_ATT_stable :
        result = _prefix->Evaluate(0,df,0) ;
        if (!result) return 0 ;
        if (result->IsConstant()) {
            // VIPER #3596: Stable attribute on constant is always true:
            delete result ;
            result = new VhdlNonconstBit(Pwr()) ;
        } else if (result->NumBits()!=1) {
            if (IsRtlElab()) Error("attribute %s on multiple bits is not synthesizable", _designator->Name()) ;
            delete result ;
            result = 0 ;
        } else {
            VhdlValue *tmp = result ;
            result = new VhdlEvent(result->GetBit(),0) ;
            delete tmp ;
        }
        return result ;

    case VHDL_ATT_last_value :
        // 'last_value is not so easy to synthesize or discard.
        // Essentially, it is this :
        //   S'last_value == (S'event) ? NOT(S) : S ;
        // Often, it's used to test explicit (0->1 or 1->0 or X->1 etc) edge expressions like this :
        //   if (S'event and S=='0' and S'last_value=='1') then
        // If used in combination with a 'event in the expression, it behaves as an inversion.
        // Otherwise, it's not synthesizable..
        // Problem is that we cannot really test how/if its used according to these rules (see issue 1470).
        // Cannot make it an event, since there would be multiple events in an expression if used legally.
        result = _prefix->Evaluate(0,df,0) ;
        if (!result) return 0 ;
        if (result->NumBits()!=1) {
            if (IsRtlElab()) Error("attribute %s on multiple bits is not synthesizable", _designator->Name()) ;
            delete result ;
            result = 0 ;
        } else {
            result = result->UnaryOper(VHDL_not,this) ;
        }
        return result ;

    case VHDL_ATT_driving_value :
        // We need to pick-up the PRESENT driving value of a SIGNAL,
        // so, interpret it as a variable to get it from the df :
        return _prefix->Evaluate(0, df, 1) ;

    case VHDL_ATT_driving :
        {
            // We need to pick-up the PRESENT driving value of a SIGNAL,
            // so, interpret it as a variable to get it from the df.
            // 'driving : return PRESENT NOT-z-condition :
            VhdlValue *val = _prefix->Evaluate(0, df, 1) ;
// Var z-condition does not work with new z handling.
// CHECK ME : where/if this is a problem.
// FIX ME : for now, return TRUE in all cases.
// Better would be to do a IsAssigned() check on the (prefix) signal, or IsZ() check on the value ?
            delete val ;
            return new VhdlNonconstBit(Pwr()) ;
//            if (!val) return 0 ;
//            // FIX ME : for composites : need to return the AND of each driving condition.
//            Net *driving = val->GetDriving() ;
//            delete val ;
//            // If there is no 'driving' net, it means that the prefix is always driving
//            // Otherwise, it drives only if the net is high.
//            return new VhdlNonconstBit((!driving)?Pwr():driving) ;
        }

    case VHDL_ATT_last_event :
    case VHDL_ATT_last_active :
        // This occurs always in 'benign' state. (e.g. Vital calls). Don't clutter with warnings.
        return new VhdlPhysicalValue((verific_int64)0) ;

    case VHDL_ATT_quiet :
    case VHDL_ATT_transaction :
    case VHDL_ATT_active :
    case VHDL_ATT_simple_name :     // FIX ME
    case VHDL_ATT_structure :       // FIX ME
    case VHDL_ATT_behavior :        // FIX ME
        break ;
    case VHDL_ATT_instance_name :
    case VHDL_ATT_path_name :
    {
        VhdlIdDef *prefix_id = _prefix->GetId() ;
        if (prefix_id) { // VIPER #5051 :
            // FIXME : Error should be produced if prefix is local port/generic
            // of component declaration :
            char *image = PathNameAttribute(prefix_id, (_attr_enum == VHDL_ATT_instance_name) ? 1: 0, 1) ;
            if (!image) return 0 ; // Viper #5231 : add null check

            // Need to create a composite value (of character VhdlEnum's) now
            VhdlIdDef *char_type = (_result_type) ? _result_type->ElementType(): 0 ;
            VERIFIC_ASSERT(char_type) ; // analysis should have guaranteed this
            Array *elements = new Array((unsigned)Strings::len(image)) ;
            VhdlValue *elem_val ;
            VhdlIdDef *char_id ;
            char buffer[4] ;
            char *tmp = image ;
            while (*tmp != '\0') {
                // Find the right character (enum) identifier.
                ::sprintf(buffer,"'%c'",*tmp++) ;
                char_id = (char_type) ? char_type->GetEnum(buffer) : 0 ;
                VERIFIC_ASSERT(char_id) ; // analysis (type-inference) should have guaranteed this too.
                // Create a VhdlEnum value with that :
                elem_val = new VhdlEnum(char_id) ;
                // And insert that in the array :
                elements->InsertLast(elem_val) ;
            }
            Strings::free(image) ;
            // Return the composite value :
            return new VhdlCompositeValue(elements,/*unsigned*/0) ;
        }
        break ;
    }
    default :
        break ;
    }
    // No synthesis support for this attribute :
    if (IsRtlElab()) Error("no support for attribute %s",_designator->Name()) ;
    return 0 ;
}

VhdlValue *VhdlOpen::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // OPEN means no value.
    return 0 ;
}

VhdlValue *VhdlEntityAspect::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // FIX ME : can this happen ?
    return 0 ;
}

VhdlValue *VhdlInstantiatedUnit::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*force_driver_value*/)
{
    // FIX ME : can this happen ?
    return 0 ;
}

/**********************************************************/
//  Assign to a name
/**********************************************************/

void VhdlName::Assign(VhdlValue *val, VhdlDataFlow * /*df*/, Array *name_select)
{
    Error("illegal target for assignment") ;
    delete val ;
    delete name_select ;
    return ;
}

void VhdlOpen::Assign(VhdlValue *val, VhdlDataFlow * /*df*/, Array *name_select)
{
    // Can happen in association assignment
    delete val ;
    delete name_select ;
    return ;
}

void VhdlIdRef::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    // Do not bail out if val does not exist, do some checks
    //if (!val) {
        //delete name_select ;
        //return ;
    //}

    if (!_single_id) {
        Error("cannot assign to non-object %s",Name()) ;
        delete val ;
        delete name_select ;
        return ;
    }
    if (_single_id->IsConstant() && df && df->IsInSubprogramOrProcess()) {
        Error("cannot assign to constant %s",_single_id->Name()) ;
        delete val ;
        delete name_select ;
        return ;
    }

    if (!name_select) {
        // Full id assignment
        // Check constraint
        VhdlConstraint *id_constraint = _single_id->Constraint() ;
        unsigned is_access_value = 0 ;
        // VIPER #7163 : If 'val' contains access value, always modify the
        // 'id_constraint' with latest access value.
        if (id_constraint && val && val->HasAccessValue()) {
        //if (id_constraint && val && val->Constraint() && val->IsAccessValue()) {
            // Viper : 3764/3771
            // This is access type assignment.. Must reset the constraint
            // This can only happen with variable assignment (df will always
            // be present)
            //id_constraint->SetUnconstrained() ;
            //id_constraint->ConstraintBy(val->Constraint()) ;
            id_constraint->ConstraintByAccessValue(val) ;
            if (val->IsAccessValue()) is_access_value = 1 ;
        }

#if 0 // Viper #7288
        if (!val && id_constraint) { // Viper #5176
            VhdlIdDef *type_id = _single_id->Type() ;
            unsigned is_access_type = type_id ? type_id->IsAccessType() : 0 ;
            VhdlConstraint *type_constraint = type_id ? type_id->Constraint() : 0 ;
            if (type_constraint && is_access_type) {
                id_constraint = type_constraint->Copy() ;
                _single_id->SetConstraint(id_constraint) ;
            }
        }
#endif

        // Viper #3993 : Do not produce null constraint warning msg for access values
        if (id_constraint && id_constraint->IsNullRange() && !is_access_value) {
            // null range warning is already given (when _single_id was declared).
            // Wont assign to a null range
            // VIPER #7851 : Remove this message as it is not informative and not necessary
            // as null range warning is already given (when _single_id was declared)
            //Warning("assignment ignored") ;
            delete val ;
            return ;
        }

        if (val && !val->CheckAgainst(id_constraint,this, df)) {
            delete val ;
            return ;
        }

        // Viper #7288: Now that the val and id_constraint are compatible, impose
        // id_constraint on value->range. e.g. their direction and bounds must now match
        if (val) val->ConstraintBy(id_constraint) ;

        if (_single_id->GetAliasTarget()) {
            // Assign entire value to full alias name
            _single_id->GetAliasTarget()->Assign(val, df, 0) ;
            return ;
        }
        // Viper 6871 and 6989 Ref target needs to be Assigned
        if (_single_id->IsRefFormal()) {
            // Assign entire value to full alias name
            _single_id->GetRefTarget()->Assign(val, df, 0) ;
            return ;
        }

        // If there is dataflow, set the value in there.
        // Otherwise, wire the value into the initial value :
        // In that case, it's a real hard assignment
        if (df) {
            df->SetAssignValue(_single_id, val) ;
        } else if (IsStaticElab() && (_single_id->IsSignal() || _single_id->IsVariable())) {
            // Do not (net)assign to a signal outside a dataflow in static elaboration mode.
            // Check this late, so that all tests on the target name are done normal.
            // VIPER 3789/3795: do same for variables.
            // Note : we sometimes get here for generics, so only refuse for signal/variable
            delete val ;
        } else if (_single_id->Value()) {
            _single_id->Value()->NetAssign(val,_single_id->ResolutionFunction(),this) ;
        } else {
            // No initial value yet. Set it ? CHECK ME : Can this still happen ??
            _single_id->SetValue(val) ;
        }
        return ;
    }

    // partially assigned identifiers

    if (_single_id->IsAlias()) {
        // How do we get the assign value for this partially assigned alias ?
        // Error("partial assignment to an alias is not supported") ;
        //
        // Try this : evaluate the alias name and resolve present name selections on that
        // before we assign..
        //
        //
        // Pick up the present DRIVER (assignment) value of the entire alias.
        VhdlValue *alias_val = _single_id->GetAliasTarget()->Evaluate(0, df, 1) ;

        // Do the partial assignment to that, using alias' constraint info (name_select is w.r.t. alias constraint) :
        AssignPartial(alias_val, _single_id->Constraint(), Pwr(), name_select, val, df) ;

        // Then assign this value to the entire alias :
        if (df) {
            _single_id->GetAliasTarget()->Assign(alias_val, df, 0) ;
        } else {
            // The netassignments is already in AssignPartial. Should not do entire value.
            delete alias_val ;
        }

        delete name_select ;

        // That's it.
        return ;
    }

    // Viper 6871 and 6989 : Assign the actual here.
    if (_single_id->IsRefFormal()) {
        // How do we get the assign value for this partially assigned reference formal ?
        // Error("partial assignment to an alias is not supported") ;
        //
        // Try this : evaluate the referenced actual expression and resolve present name selections on that
        // before we assign..
        //
        //
        // Pick up the present DRIVER (assignment) value of the entire alias.

        VhdlValue *ref_val = _single_id->GetRefTarget()->Evaluate(0, df, 1) ;
        // VIPER #7529 : produce error if there is size mismatch between 'ref_val'
        // and the constraint of _single_id
        unsigned is_ram = 0 ;
        VhdlConstraint *single_id_constraint = _single_id->Constraint() ;
        if (ref_val && !is_ram && single_id_constraint && (ref_val->NumElements() != single_id_constraint->Length())) {
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                Warning("expression has %d elements ; expected %d", ref_val->NumElements(), single_id_constraint->Length()) ;
            } else {
                Error("target slice is %d elements ; value is %d elements", ref_val->NumElements(), single_id_constraint->Length()) ;
            }
            ref_val->Adjust((unsigned)single_id_constraint->Length()) ;
        }

        // Do the partial assignment to that, using formal' constraint info (name_select is w.r.t. formal constraint) :
        AssignPartial(ref_val, _single_id->Constraint(), Pwr(), name_select, val, df) ;

        // Then assign this value to the entire actual :
        if (df) {
            _single_id->GetRefTarget()->Assign(ref_val, df, 0) ;
        } else {
            // The netassignments is already in AssignPartial. Should not do entire value.
            delete ref_val ;
        }

        delete name_select ;

        return ;
    }

    // Get the full assignment value and strip down LAST->FIRST to the right
    // selection of it
    // We do it this way, so we can keep track of the assignment value (a POINTER)
    // and also the subtype constraint which goes with the prefixed name.

    // If there is dataflow, get the assign value for the id from there.
    // Otherwise, take it from the initial value
    // No difference here between variables and signals
    VhdlValue *target_val ;
    if (df) {
        target_val = df->AssignValue(_single_id) ;
    } else {
        // VIPER 3789/3795: do not assign (to variables or signals) in static elab mode
        // Note : we sometimes get here for generics, so only refuse for signal/variable
        target_val = (IsStaticElab() && (_single_id->IsSignal() || _single_id->IsVariable())) ? 0 : _single_id->Value() ;
    }

    if (target_val && target_val->IsNull()) {
        Error("cannot dereference a null access value") ;
        delete name_select ;
        delete val ;
        return ;
    }

    if (IsDeallocatedValue(target_val)) Warning("access value for %s is not allocated or already free'd", _single_id->Name()) ; // Access Value cleanup: (VIPER #4464, 7292)

    // We arrived at the id of a partial assignment (MSB).
    // With the target (full id) value (a pointer)
    // and the target (full id) constraint (a pointer too),
    // recur back (to LSB) over the name constraints, while building the logic needed
    // We recur, since not all assignments are plain value replacements, but some
    // are variable indexed assignments that only happen under a condition.

    // 'val' will be absorbed in here
    AssignPartial(target_val, _single_id->Constraint(), Pwr(), name_select, val, df) ;

    // Delete the name_select array (should be empty now)
    delete name_select ;
}

void
VhdlIdRef::AssignPartial(VhdlValue *target_val, VhdlConstraint *target_constraint, Net *assign_condition, Array *name_select, VhdlValue *val, VhdlDataFlow *df)
{
    // Cannot do assignment if there is no value to assign from.
    // Do not bail out, do some checks. Needed for static elaboration
    //if (!val) return ;

    // Viper : 3764/3771
    // Access value constraints are dynamic (changes by assignments).. Hence the constraint
    // on the last assigned access_value is the correct target constraint
    if (target_val && target_val->IsAccessValue()) target_constraint = target_val->Constraint() ;

    // Can only do real restricted name assignments.
    // This is because on sliced names, we have to replace the value in the containing array.
    VERIFIC_ASSERT (name_select && name_select->Size()!=0) ;

    // Do not bail out if we do not have a target value. Do maximum amount of checks first.
    // Only if we have no target constraint do we bail out (nothing to check).
    if (!target_constraint) { // || !target_val)
        delete val ;
        return ; // something bad happened
    }

    Array *address_nets = 0 ;
    // This code should not get executed unless we allow RAM detection for arguments to subprograms
    // This feature is present in some CS code. But thought to make this change baseline.

    // Important : 'assign_val' is a POINTER into an existing value field.
    // We just modify the right elements according to the name selections :

    // Prune off the last (MSB) name restriction
    VhdlDiscreteRange *assoc = (VhdlDiscreteRange*)name_select->RemoveLast() ;
    if (!assoc) {
        delete val ;
        delete address_nets ; // fix mem leak
        return ; // Something bad happened.
    }

    VhdlValue *elem_val ;

    if (assoc->IsRange()) {
        // Take a slice
        if (name_select->Size() !=0 ) {
            // Somebody wrote x(0 to 3)(2) <= or so.
            // This is similar to x(2) ? So just 'skip' this name restriction
            AssignPartial(target_val, target_constraint, assign_condition, name_select, val, df) ;
            delete address_nets ; // fix mem leak
            return ;
        }

        // Here, slice is the last one
        VhdlConstraint *range = assoc->EvaluateConstraintInternal(df, 0) ;
        if (!range) {
            delete val ;
            delete address_nets ; // fix mem leak
            return ; // Something bad happened.
        }

        // Test the slice range
        if (range->Dir()!=target_constraint->Dir()) {
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                assoc->Warning("slice direction differs from its index subtype range") ;
            } else {
                assoc->Error("slice direction differs from its index subtype range") ;
            }
            delete val ;
            delete range ;
            delete address_nets ; // fix mem leak
            return ;
        }

        if (range->IsNullRange()) {
            delete val ;
            delete range ;
            // null range warning is already given
            // Wont assign to a null range
            // VIPER #7851 : Remove this message as it is not informative and not necessary
            // as null range warning is already given (when _single_id was declared)
            //Warning("assignment ignored") ;
            delete address_nets ; // fix mem leak
            return ;
        } else if (!target_constraint->Contains(range->Left())) {
            delete val ;
            delete range ;
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                assoc->Warning("left bound of slice is outside its index type range") ;
            } else {
                assoc->Error("left bound of slice is outside its index type range") ;
            }
            delete address_nets ; // fix mem leak
            return ;
        } else if (!target_constraint->Contains(range->Right())) {
            delete val ;
            delete range ;
            // VIPER #3815 : produce warning for non-constant branch
            if (df && df->IsNonconstBranch()) {
                assoc->Warning("right bound of slice is outside its index type range") ;
            } else {
                assoc->Error("right bound of slice is outside its index type range") ;
            }
            delete address_nets ; // fix mem leak
            return ;
        }
        // Bail out now if no value exists
        if (!val) {
            delete range ;
            delete val ;
            delete address_nets ; // fix mem leak
            return ;
        }

        // Define lower and upper bounds into the value
        unsigned left_pos = target_constraint->PosOf(range->Left()) ;
        unsigned right_pos = target_constraint->PosOf(range->Right()) ;
        VERIFIC_ASSERT(left_pos<=right_pos) ; // Above rules should guarantee this
        delete range ;

        // Check that there are exactly the right amount of elements in the value
        if (val->NumElements() != ((right_pos - left_pos) + 1)) {
                // VIPER #3815 : produce warning for non-constant branch
                if (df && df->IsNonconstBranch()) {
                    assoc->Warning("target slice is %d elements ; value is %d elements", (right_pos - left_pos) + 1, val->NumElements()) ;
                } else {
                    assoc->Error("target slice is %d elements ; value is %d elements", (right_pos - left_pos) + 1, val->NumElements()) ;
                }

            delete val ;
            delete address_nets ; // fix mem leak
            return ;
        }

        // Bail out now if no target value exists
        if (!target_val) {
            delete val ;
            delete address_nets ; // fix mem leak
            return ;
        }

        // Assign element values, one-by-one
        VhdlValue *elem ;
        // Viper #5255 : For full_selection (when the selected range is same as the prefix range) element by element assignment is sub-optimal. Create only single right port for such cases
        unsigned full_selection = address_nets && ((unsigned)((right_pos-left_pos)+1) == target_constraint->Length()) ;
        for (unsigned j=left_pos; j<=right_pos; j++) {
            elem = full_selection ? val : val->TakeValueAt(j-left_pos) ; // slice element positions run 0...n-1
            // Check element subtype against constraint
            if (!elem || !elem->CheckAgainst(full_selection ? target_constraint : target_constraint->ElementConstraint(),this, df)) {
                delete elem ;
                break ;
            }

            if (assign_condition != Pwr() && IsRtlElab()) {
                // This assignment is done only under a condition
                // Get present value
                elem_val = target_val->ValueAt(j) ;
                // Create a selector value
                VhdlNonconstBit s(assign_condition) ;
                // MUX present value against condition
                elem_val = elem_val->Copy()->SelectOn1(&s, elem, 0, /*from*/assoc) ;
            } else {
                // This assignment is always done
                elem_val = elem ;
            }

            // If there is dataflow, replace the existing element.
            // Otherwise, do a net-assign (since its a hard assignment).
            if (df || (_single_id && _single_id->IsConstant())) {
                target_val->SetValueAt(j,elem_val) ;
            } else {
                target_val->NetAssignAt(j,elem_val) ;
            }
        }
        // 'val' is an empty shell now. delete it
        delete val ;
    } else {
        // indexed or record-selected name
        // VIPER #3164 : Issue error if indexing is on null range
        if (IsStaticElab() && target_constraint->IsNullRange()) {
            Error("no index value can belong to null index range") ;
            delete val ;
            delete address_nets ; // fix mem leak
            return ;
        }
        VhdlValue *idx_val = assoc->Evaluate(0,df,0) ;
        if (!idx_val && IsRtlElab()) {
            delete val ;
            delete address_nets ; // fix mem leak
            return ; // error occurred
        }

        if (idx_val && idx_val->IsConstant()) {
            if (!target_constraint->Contains(idx_val)) {
                char *v = idx_val->Image() ;
                char *r = target_constraint->Image() ;
                // VIPER #3815 : produce warning for non-constant branch
                if (df && df->IsNonconstBranch()) {
                    assoc->Warning("index %s is out of array constraint %s for target %s",v,r,(_single_id) ? _single_id->Name() : "") ;
                } else {
                    assoc->Error("index %s is out of array constraint %s for target %s",v,r,(_single_id) ? _single_id->Name() : "") ;
                }
                Strings::free(v) ;
                Strings::free(r) ;
                delete idx_val ;
                delete val ;
                return ;
            }
        }
        if (idx_val && idx_val->IsConstant()) {
            if (!target_val) { // Cannot proceed further without 'target_val'
                delete val ;
                delete idx_val ;
                return ;
            }
            unsigned pos = target_constraint->PosOf(idx_val) ;
            delete idx_val ;
            if (name_select->Size()==0) {
                // last one : assign to this element
                // Check sub-type assignment of the element

                VhdlConstraint *elem_constraint = target_constraint->ElementConstraintAt(pos) ;
                // Viper : 3764/3771
                // if val is access_value, assignment with access value will always
                // reset the target constraint..
                //if (elem_constraint && val->Constraint() && val->IsAccessValue()) {
                // VIPER #7163 : If 'elem_constraint' is record constraint, constrained its element
                if (elem_constraint && val && val->HasAccessValue()) {
                    //elem_constraint->SetUnconstrained() ;
                    elem_constraint->ConstraintByAccessValue(val) ;
                }
                // VIPER #7577 : If elem_constraint is not present, but 'val' is
                // access value, get element constraint from val
                if (!elem_constraint && val && val->HasAccessValue()) {
                    elem_constraint = val->Constraint() ;
                    // FIXME : Need to set element constraint to target_constraint
                }

                if (val && !val->CheckAgainst(elem_constraint, this, df)) {
                    delete val ;
                    return ;
                }

                if (assign_condition != Pwr() && IsRtlElab()) {
                    // This assignment is done only under a condition
                    // Get present value
                    elem_val = target_val->ValueAt(pos) ;
                    VhdlNonconstBit s(assign_condition) ;
                    // MUX present value against condition
                    elem_val = elem_val->Copy()->SelectOn1(&s, val, 0, /*from*/assoc) ;
                } else {
                    // This assignment is always done
                    elem_val = val ;
                }

                // If there is dataflow, replace the existing element.
                // Otherwise, do a net-assign (since its a hard assignment).
                // VIPER 3754 : also replace value for constants (net-assignments to constants happen for partial generic bindings)
                if (df || (_single_id && _single_id->IsConstant())) {
                    target_val->SetValueAt(pos,elem_val) ;
                } else {
                    target_val->NetAssignAt(pos,elem_val) ;
                }
            } else {
                // decent deeper ; value and constraint
                // RD: semi-temporary fix for 3905. Target value ValueAt() can return a 'default' value at this index,
                // So make sure to create a real element value at this position.
                // Do that with a TakeValueAt()/SetValueAt() combination.
                elem_val = target_val->TakeValueAt(pos) ;
                target_val->SetValueAt(pos,elem_val) ; // assures that target element is real and present (and not the default)
                // original code (assumes that target element value is present)
                target_val = target_val->ValueAt(pos) ;
                target_constraint = target_constraint->ElementConstraintAt(pos) ;
                AssignPartial(target_val, target_constraint, assign_condition, name_select, val, df) ;
            }
            // val is absorbed here. Don't delete.
        } else {
            // Variable index assignment
            // FIX ME for bit-encoded index value

            // VIPER issue 2296 : max and min can be more accurately determined based on the constraint on the index expression.
            // If that is a simple name, the constraint will be accurate :
            VhdlConstraint *index_expr_constraint = assoc->EvaluateConstraintInternal(df, 0) ;

            // Calculate the range of values that this 'idx' can represent.
            // Base this on the number of bits in the expression :
            verific_int64 max = 0 ;
            verific_int64 min = 0 ;
            VhdlNonconst *idx = 0 ;
            if (idx_val) {
                idx = idx_val->ToNonconst() ;
                idx_val = 0 ;

                // Calculate the range of values that this 'idx' can represent.
                // Base this on the number of bits in the expression :
                max = ( (verific_int64)1 << ( (idx->IsSigned()) ? idx->NumBits() - 1 : idx->NumBits() )) - 1;
                min = (idx->IsSigned()) ? (- ((verific_int64)1 << (idx->NumBits() - 1))) : 0 ;
            } else  if (index_expr_constraint) {
                max = index_expr_constraint->High()->Position() ;
                min = index_expr_constraint->Low()->Position() ;
            }

            // Now check if this is always out of range of the index (target) constraint :
            if ((index_expr_constraint) &&
                ((max < target_constraint->Low()->Position()) ||
                (min > target_constraint->High()->Position()))) {
                // FIX ME : nonconst does not print nicely.
                char *v = idx ? idx->Image() : (index_expr_constraint ? index_expr_constraint->Image(): 0);
                char *r = target_constraint->Image() ;
                // VIPER #3815 : produce warning for non-constant branch
                if (df && df->IsNonconstBranch()) {
                    assoc->Warning("index value %s is always out of array index range %s",v,r) ;
                } else {
                    assoc->Error("index value %s is always out of array index range %s",v,r) ;
                }
                Strings::free(v) ;
                Strings::free(r) ;
                delete index_expr_constraint ;
                delete idx ;
                delete val ;
                return ;
            }
            if (!val || !target_val || !idx) { // Cannot proceed further without 'val' /'target_val'
                delete val ;
                delete idx ;
                delete index_expr_constraint ; // VIPER #5213 :
                return ;
            }
            if (IsRtlElab()) {
            }
        }
    }
    delete address_nets ; // fix mem leak
}

void VhdlSelectedName::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    // Get proper unique identifier considering package instance specific alias prefix
    VhdlIdDef *unique_id = GetElabSpecificUniqueId() ;
    //if (!val) return ; Allow fall through

    if (_prefix && _suffix && _suffix->IsAll()) { // Viper #8162
        // prefix.all in LHS => resetting accessed value
        // impose the constraint on value
        // only when full access value is replaced
        if (!name_select || !name_select->Size()) {
            VhdlConstraint *prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;
            if (prefix_constraint) {
                // reconstraint prefix
                prefix_constraint->ConstraintByAccessValue(val) ;
                if (val) val->ConstraintBy(prefix_constraint) ;
            }
            // an access value to be written onto the prefix
            val = new VhdlAccessValue(val, prefix_constraint) ;
        }

        if (unique_id && val && !val->IsNull()) {
            VhdlValue *id_val = df ? df->AssignValue(unique_id) : unique_id->Value() ;
            if (id_val && (id_val->IsNull() || IsDeallocatedValue(id_val))) Warning("access value for %s is not allocated or already free'd", unique_id->Name()) ;
        }

        _prefix->Assign(val, df, name_select) ;
        return ;
    }

    if (!_prefix || !unique_id) {
        // VIPER #3321 : Do not issue error for any type of illegal target in
        // static elab. After this check for all types, no need to suppress error
        // separately for access_var.all (#3131)
        if (IsRtlElab()) Error("illegal selected name for assignment") ;
        delete val ;
        delete name_select ;
        return ;
    }

    if (unique_id->IsRecordElement()) {
        // FIX ME : putting suffix in there. Bit ugly ? Is it type-infered ?
        if (!name_select) name_select = new Array() ;
        name_select->InsertLast(_suffix) ;
        _prefix->Assign(val, df, name_select) ;
        return ;
    }

    // It's a out-of-scope assignment (or at least the identifier is a hierarchical name).

    // Issue 2193 : allow this assignment to happen.
    // We previously did not allow this, probably because we used to have problems with hierarchical names,
    // or simply because of the code-cuplication issue for partial assignments (see below).

    // CHECK ME:
    // There might still be a problem with hierarchical names out of this design unit.
    // or into generate loops, just like we have for Verilog.
    // But these would be a problem both for usage and assignment....

    // Error("no support for selected name assignments to out-of-scope objects") ;

    // We need to support a 'partial assignment' to a extended name too.
    // Routine PartialAssign is only defined on a IdRef, so rather than
    // duplicating the entire IdRef::Assign and IdRef::PartialAssign routine
    // here on a SelectedName, do a shortcut : temporarily create an IdRef of the _unique_id here,
    // and call Assign on it :

    VhdlIdRef tmp_id_ref(unique_id) ;
    tmp_id_ref.SetLinefile(Linefile()) ; // set to this linefile info

    // Call regular Assign on this temporary node :
    tmp_id_ref.Assign(val, df, name_select) ;
}

void VhdlIndexedName::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    if (_is_array_index || _is_array_slice) {
        if (!_prefix) return ;
        // Stack the index expressions (or range) into 'name_select'. LSB->MSB
        if (!name_select) name_select = new Array() ;
        unsigned i ;
        VhdlDiscreteRange *assoc ;
        FOREACH_ARRAY_ITEM_BACK(_assoc_list, i, assoc) {
            name_select->InsertLast(assoc) ;
        }
        // Now call the prefix with the name selection
        _prefix->Assign(val, df, name_select) ;
        // Viper Issue #8411 : To be conservative, invalidate the entire prefix.
        if (IsStaticElab() && !val && df) df->SetAssignValue(_prefix_id, val) ;
    } else if (_is_function_call || _is_type_conversion) {
        // Must be a type-conversion function or type-conversion in a formal
        if (!_assoc_list || _assoc_list->Size()!=1) {
            delete val ;
            delete name_select ;
            Error("illegal function call in assignment") ;
            return ;
        }
        VhdlDiscreteRange *arg = (VhdlDiscreteRange*)_assoc_list->At(0) ;
        if (!arg) return ; // Something is bad ('open' output assoc ?).

        // We should assign directly to the formal designator.
        // The type-converter is use only if we go the other direction..
        arg->Assign(val, df, name_select) ;
    } else {
        Error("illegal name for assignment") ;
        delete val ;
        delete name_select ;
    }
}
void VhdlExternalName::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    //if (!val) return ; Allow fall through

    if (!_unique_id) _unique_id = FindExternalObject(_path_token, _hat_count) ;

    if (!_unique_id) {
        // VIPER #3321 : Do not issue error for any type of illegal target in
        // static elab. After this check for all types, no need to suppress error
        // separately for access_var.all (#3131)
        if (IsRtlElab()) Error("illegal selected name for assignment") ;
        delete val ;
        delete name_select ;
        return ;
    }

    if (_unique_id->IsRecordElement()) {
        if (_ext_path_name) _ext_path_name->Assign(val, df, name_select) ;
    }
    VhdlIdRef tmp_id_ref(_unique_id) ;
    tmp_id_ref.SetLinefile(Linefile()) ; // set to this linefile info

    // Call regular Assign on this temporary node :
    tmp_id_ref.Assign(val, df, name_select) ;
}

/**********************************************************/
// Return the range specified for the generate statement
// for a name that represents a block specification
/**********************************************************/

VhdlConstraint *VhdlName::BlockIndexSpec() { return 0 ; }

VhdlConstraint *VhdlIndexedName::BlockIndexSpec()
{
    VERIFIC_ASSERT(_assoc_list && _assoc_list->Size()==1) ; // or else analysis failed.

    VhdlDiscreteRange *assoc = (VhdlDiscreteRange*)_assoc_list->At(0) ;

    VhdlConstraint *result ;
    if (assoc->IsRange()) {
        // Its a range
        result = assoc->EvaluateConstraintInternal(0, 0) ;
    } else {
        // Must be a single expression. Create a range N to N from that
        VhdlValue *expr = assoc->Evaluate(0,0,0) ;
        if (!expr) return 0 ;
        if (!expr->IsConstant()) {
            // LRM 1.3.1 (ln 139)
            assoc->Error("expression is not constant") ;
            delete expr ;
            return 0 ;
        }
        result = new VhdlRangeConstraint(expr, VHDL_to, expr->Copy());
    }
    return result ;
}

// Added for Variable Slice initialization
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
void VhdlPhysicalLiteral::InitializeForVariableSlice()
{
    if (_value) _value->InitializeForVariableSlice() ;
    if (_unit) _unit->InitializeForVariableSlice() ;
}

void VhdlSelectedName::InitializeForVariableSlice()
{
    // Get proper unique identifier considering package instance specific alias prefix
    VhdlIdDef *unique_id = GetElabSpecificUniqueId() ;
    if (_prefix) _prefix->InitializeForVariableSlice() ;
    if (_suffix) _suffix->InitializeForVariableSlice() ;
    if (unique_id) unique_id->InitializeForVariableSlice() ;
}

void VhdlIndexedName::InitializeForVariableSlice()
{
    if (!_prefix) return ;
    _prefix->InitializeForVariableSlice() ;

    if (_is_array_slice) {
        // This must have only single "range" assoc in the assoc list
        VhdlDataFlow *df = VhdlVariableSlice::GetTopDataFlow() ;

        unsigned i ;
        VhdlDiscreteRange *assoc ;
        FOREACH_ARRAY_ITEM_BACK(_assoc_list, i, assoc) {
            if (!assoc) continue ; // expecting only single assoc for slice

            VhdlExpression *left = assoc->GetLeftExpression() ;
            VhdlExpression *right = assoc->GetRightExpression() ;
            if (!left || !right) continue ;

            VhdlValue *lval = left->Evaluate(0, df, 0) ;
            VhdlValue *rval = right->Evaluate(0, df, 0) ;
            if (!lval || !rval) {
                delete lval ; delete rval ;
                continue ;
            }

            unsigned lval_const = lval->IsConstant() ;
            unsigned rval_const = rval->IsConstant() ;

            unsigned direction = assoc->GetDir() ;

            if (!lval_const || !rval_const) {
                // Get the range of the prefix
                VhdlConstraint *prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;
                VhdlValue *lcval = prefix_constraint ? prefix_constraint->Left() : 0 ;
                VhdlValue *rcval = prefix_constraint ? prefix_constraint->Right() : 0 ;
                if (!lcval || !rcval) return ;

                long lval_const_value = lval_const ? (long) lval->Integer() : 0 ;
                VhdlNonconst *lval_nc = lval->ToNonconst() ;
                lval = 0 ;

                long rval_const_value = rval_const ? (long) rval->Integer() : 0 ;
                VhdlNonconst *rval_nc = rval->ToNonconst() ;
                rval = 0 ;

                long lbound = (long) lcval->Integer() ;
                long rbound = (long) rcval->Integer() ;
                long low = lbound < rbound ? lbound : rbound ;
                long high = lbound > rbound ? lbound : rbound ;
                if (lval_const) {
                    // Viper #7532: Create a variable even for the constant slice bound.
                    // It helps to capture null range later in the flow by imposing
                    // a separate constraint, different from the variable bounds
                    (void) VhdlVariableSlice::AddVariable(left, lval_const_value, lval_const_value, lval_nc) ;
                } else {
                    // the constraint should be the intersection of prefix and left_expr
                    VhdlConstraint *left_constraint = left->EvaluateConstraintInternal(df, 0) ;
                    lcval = left_constraint ? left_constraint->Left() : 0 ;
                    rcval = left_constraint ? left_constraint->Right() : 0 ;
                    long low1 = low ;
                    long high1 = high ;
                    if (lcval && rcval) {
                        // take the intersection
                        long tmp1 = (long) lcval->Integer() ;
                        long tmp2 = (long) rcval->Integer() ;
                        if (low1 < ((tmp1 < tmp2) ? tmp1 : tmp2)) low1 = (tmp1 < tmp2) ? tmp1 : tmp2 ;
                        if (high1 > ((tmp1 > tmp2) ? tmp1 : tmp2)) high1 = (tmp1 > tmp2) ? tmp1 : tmp2 ;
                    }

                    if (prefix_constraint) lval_nc->Adjust(prefix_constraint->NumBitsOfRange()+1) ; // Viper #4974: need one more bit for correct ::Equal logic sign adjustment
                    (void) VhdlVariableSlice::AddVariable(left, low1, high1, lval_nc) ;
                    delete left_constraint ;
                }

                if (rval_const) {
                    // Viper #7532: Create a variable even for the constant slice bound.
                    // It helps to capture null range later in the flow by imposing
                    // a separate constraint, different from the variable bounds
                    (void) VhdlVariableSlice::AddVariable(right, rval_const_value, rval_const_value, rval_nc, direction) ;
                } else {
                    // the constraint should be the intersection of prefix and right_expr
                    VhdlConstraint *right_constraint = right->EvaluateConstraintInternal(df, 0) ;
                    lcval = right_constraint ? right_constraint->Left() : 0 ;
                    rcval = right_constraint ? right_constraint->Right() : 0 ;
                    long low1 = low ;
                    long high1 = high ;
                    if (lcval && rcval) {
                        // take the intersection
                        long tmp1 = (long) lcval->Integer() ;
                        long tmp2 = (long) rcval->Integer() ;
                        if (low1 < ((tmp1 < tmp2) ? tmp1 : tmp2)) low1 = (tmp1 < tmp2) ? tmp1 : tmp2 ;
                        if (high1 > ((tmp1 > tmp2) ? tmp1 : tmp2)) high1 = (tmp1 > tmp2) ? tmp1 : tmp2 ;
                    }

                    // ===> !lval_const && !rval_const : 2 variable entries for this slice
                    // last variable node (for lval) and this one are NOT independent..
                    // based on to/donwto one side is dynamically bound to the other
                    // variable to avoid null range. This will improve our runtime
                    // performance by skipping bogus variable assignments
                    // e.g. for var(i to j) our assignments should satisfy i <= j

                    if (prefix_constraint) rval_nc->Adjust(prefix_constraint->NumBitsOfRange()+1) ; // Viper #4974: need one more bit for correct ::Equal logic sign adjustment
                    (void) VhdlVariableSlice::AddVariable(right, low1, high1, rval_nc, direction) ;
                    delete right_constraint ;
                }

                delete prefix_constraint ;
            }
            delete lval; delete rval ;
        }
    } else {
        unsigned i ;
        VhdlDiscreteRange *elem ;
        FOREACH_ARRAY_ITEM(_assoc_list,i,elem) {
            if (!elem) continue ;
            elem->InitializeForVariableSlice() ;
        }
    }
}

void VhdlSignaturedName::InitializeForVariableSlice()
{
    if (_name) _name->InitializeForVariableSlice() ;
    if (_signature) _signature->InitializeForVariableSlice() ;
    if (_unique_id) _unique_id->InitializeForVariableSlice() ;
}

void VhdlAttributeName::InitializeForVariableSlice()
{
    if (_prefix) _prefix->InitializeForVariableSlice() ;
    if (_expression) _expression->InitializeForVariableSlice() ;
    if (_prefix_type) _prefix_type->InitializeForVariableSlice() ;
    if (_result_type) _result_type->InitializeForVariableSlice() ;
}

void VhdlEntityAspect::InitializeForVariableSlice()
{
    if (_name) _name->InitializeForVariableSlice() ;
}

void VhdlInstantiatedUnit::InitializeForVariableSlice()
{
    if (_name) _name->InitializeForVariableSlice() ;
}
#endif

////////////// External name resolution //////////////////////
VhdlIdDef *VhdlName::FindExternalObject(unsigned /*path_token*/, unsigned /*accent_count*/)
{
    return 0 ;
}
VhdlIdDef *VhdlDesignator::FindExternalObject(unsigned path_token, unsigned accent_count)
{
    // In static elaboration mode always search element ids as parse
    // tree is modified there.
    if (_single_id && IsRtlElab()) return _single_id ;
    _single_id = 0 ;

    if (!_present_scope) return 0 ;
    Set result(POINTER_HASH) ;

    // The first element of external-name can be
    // 1. Library name
    // 2. Entity simple name
    // 3. Component instance label
    // 4. Generate statement label
    // 5. Package simple name

    // and it will depend on the path_token and accent-count
    if (path_token == VHDL_AT) { // Package path name
        // If path token is '@', this will be a library logical name
        (void) _present_scope->FindAll(_name, result) ;

        if (result.Size()==1) _single_id = (VhdlIdDef*)result.GetAt(0) ;

        // VIPER #7242 : Find library
        if (!_single_id) {
            VhdlLibrary *lib = 0 ;
            if (Strings::compare(_name, "work")) {
                lib = vhdl_file::GetWorkLib() ; // use present work library
            } else {
                lib = vhdl_file::GetLibrary(_name, 1) ;
            }
            _single_id = new VhdlLibraryId(Strings::save(lib ? lib->Name(): _name)) ;
            // Declare the library identifier forcefully in the scope
            _present_scope->DeclarePredefined(_single_id) ;
        }
        if (!_single_id->IsLibrary()) {
            Error("package path name should start with library logical name") ;
            _single_id = 0 ;
        }
        return _single_id ;
    }
    if (path_token == VHDL_DOT) { // Absolute path name
        // Absolute path name starts with a '.' and this will be top level unit name
        VhdlLibrary *work_lib = vhdl_file::GetWorkLib() ;
        VhdlPrimaryUnit *prim_unit = work_lib ? work_lib->GetPrimUnit(_name): 0 ;
        _single_id = prim_unit ? prim_unit->Id(): 0 ;
        // VIPER #7367: If 'prim_unit' is top, we are doing static elab and
        // COPY_TOP_BEFORE_STATIC_ELAB is on, use ***_default as top level
#ifdef VHDL_COPY_TOP_BEFORE_STATIC_ELAB
        if (IsStaticElab() && _single_id) {
            char *new_unit_name = VhdlTreeNode::ConcatAndCreateLegalIdentifier(_single_id->OrigName(), "_default") ;
            VhdlPrimaryUnit *exists = work_lib ? work_lib->GetPrimUnit(new_unit_name): 0 ;
            if (exists) _single_id = exists->Id() ;
            Strings::free(new_unit_name) ;
        }
#endif

        // Check whether it is indeed top level unit (from stored path_name attribute specific data structure)
        // FIXME : WE don't have correct information about the proper top level unit
        //char *top_level_unit = _path_name ? (char*)_path_name->At(0): 0 ;

        if (!_single_id || (!_single_id->IsEntity() && !_single_id->IsConfiguration())
                  /*|| !::strcasestr(top_level_unit, _name)*/) {
            Error("absolute path name should start with primary unit name") ;
            _single_id = 0 ;
        }

        if (_single_id && vhdl_file::_top_unit_name && !Strings::compare(_single_id->Name(), vhdl_file::_top_unit_name)) {
            Error("%s is not the top unit", _single_id->Name()) ;
            _single_id = 0 ;
        }

        return _single_id ;
    }
    // First find the current enclosing concurrent scope :
    // We will exclude process-scope, subprogram-scope and internal package-scope
    // if that is inside process/subprogram
    VhdlScope *scope = _present_scope ;
    while (scope) {
        if (scope->IsProcessScope() || scope->IsSubprogramScope()) {
            scope = scope->Upper() ; // process/subprogram scope, do upper
            continue ;
        }
        VhdlIdDef *owner = scope->GetOwner() ;
        if (owner && owner->IsPackage()) {
            VhdlScope *upper = scope->Upper() ;
            if (upper && (upper->IsProcessScope() || upper->IsSubprogramScope())) {
                scope = upper->Upper() ;
                continue ;
            }
        }
        break ;
    }
    if (path_token == VHDL_ACCENT) { // Relative path name
        // Relative path can start with '^' (s). Each '^' indicates that we need
        // to start searching object from a concurrent scope one level higher than
        // the current enclosing concurrent scope
        // Go upward hat_count times
        unsigned tmp_hat_count = accent_count ;
        while (scope && (tmp_hat_count > 0)) {
            scope = scope->Upper() ;
            if (scope) tmp_hat_count-- ;
        }
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
        // VIPER #7893 : There are remaining accents, so we need to traverse
        // hierarchy upwards
        if (IsStaticElab() && (tmp_hat_count > 0)) {
            VeriStaticElaborator *elab_obj = veri_file::GetStaticElabObj() ;
            VeriPseudoTreeNode *curr_node = elab_obj ? elab_obj->GetCurrentNode(): 0 ;
            VeriPseudoTreeNode *target_node = curr_node ;
            while (target_node && (tmp_hat_count > 0)) {
                target_node = target_node->GetParent() ;
                tmp_hat_count-- ;
            }
            scope = target_node ? target_node->GetNodeScope(): 0 ;
        }
#endif // VHDL_DUAL_LANGUAGE_ELABORATION
        // Now 'scope' is the concurrent scope where we can find '_name'
        _single_id = scope ? scope->FindSingleObjectLocal(_name): 0 ;
        if (!_single_id) { // _name may be in architecture
            VhdlSecondaryUnit *sec_unit = 0 ;
            VhdlIdDef *unit_id = scope ? scope->GetOwner(): 0 ;
            VhdlPrimaryUnit *pu = unit_id ? unit_id->GetPrimaryUnit(): 0 ;
            if (pu && (pu->NumOfSecondaryUnits() == 1)) {
                sec_unit = pu->GetSecondaryUnit(0) ;
                scope = sec_unit ? sec_unit->LocalScope(): 0 ;
            }
            _single_id = (scope) ? scope->FindSingleObjectLocal(_name): 0 ;
        }
    }
    if (!_single_id) {
        // It is partial path name, find '_name' from enclosing concurrent scope
        _single_id = scope ? scope->FindSingleObjectLocal(_name): 0 ;
    }
    if (!_single_id) {
        Error("%s is not declared", _name) ;
    } else {
        // Set global present scope to current 'scope'
        _present_scope = scope ;
    }
    return _single_id ;
}
VhdlIdDef *VhdlIndexedName::FindExternalObject(unsigned path_token, unsigned accent_count)
{
    // VIPER #6912: Now we support external name evaluation through Netlist search, disable it:
    //Error("external name referring %s is not yet supported", "generate statement") ;

    // In static elaboration mode always search element ids as parse
    // tree is modified there.
    if (!_prefix_id || IsStaticElab()) {
        _prefix_id = (_prefix) ? _prefix->FindExternalObject(path_token, accent_count): 0 ;
    }
    if (!_prefix_id) return 0 ;

    // Only one assoc-item is allowed here
    if (_assoc_list && _assoc_list->Size() != 1) {
        Error("illegal element in external name") ;
        return 0 ;
    }
    VhdlDiscreteRange *assoc_elem = (_assoc_list && _assoc_list->Size()) ? (VhdlDiscreteRange*)_assoc_list->At(0): 0 ;
    if (!assoc_elem) return _prefix_id ;
    // Type infer the 'assoc_elem'
    (void) assoc_elem->TypeInfer(0, 0, 0, 0) ;

    // VIPER #8330 : Index in external name must be a globally static expression
    if (!assoc_elem->IsGloballyStatic()) {
        assoc_elem->Warning("an index in an external name must be a globally static expression") ;
    }

    // Check whether it is simple identifier reference or
    // a constant expression representing loop iterator value
    VhdlValue *val = assoc_elem->Evaluate(0, 0, 0) ;

    // _prefix_id should be a generate statement
    VhdlStatement *prefix_stmt = _prefix_id->GetStatement() ;

    if (val && val->IsConstant()) { // loop iteration value

        // VIPER #8330 : Get the iter scheme from prefix_stmt and check the value of index
        VhdlIterScheme *scheme = prefix_stmt ? prefix_stmt->GetScheme() : 0 ;

        VhdlDiscreteRange *iter_range = scheme ? scheme->GetRange(): 0 ;
        // Get constraint of the loop index variable
        VhdlConstraint *range = iter_range ? iter_range->EvaluateConstraint(0, 0): 0 ;
        if (range && !range->Contains(val)) {
            char *image_val = val->Image() ;
            assoc_elem->Error("index value %s is out of range for for-generate %s", image_val, _prefix_id->Name()) ;
            Strings::free(image_val) ;
        }
        delete range ;
        // VIPER #8330 : Produce error if prefix_stmt is not generate. Allow block statement
        // as static elaboration of generate can create block statement
        if (!scheme && prefix_stmt && !prefix_stmt->IsIfElsifGenerateStatement() && !prefix_stmt->IsCaseGenerateStatement()
           && !prefix_stmt->IsGenerateElabCreated() // Allow generate elaboration created block statement
            ) {
            Error("%s is not the label of a generate", _prefix_id->Name()) ;
        }

        if (IsStaticElab()) {
            // In static elaboration for loop is unrolled and for each iteration a block is
            // created with name 'generate_label(loop_index_value)'
            if (!scheme) { // Generate is already elabortated
                char *block_name = VhdlIterScheme::CreateBlockName(_prefix_id, val, 0, 0) ;
                VhdlIdDef *block_id = _present_scope->FindSingleObject(block_name) ;
                Strings::free(block_name) ;
                if (block_id) {
                    delete val ;
                    return block_id ;
                } else {
                    char *image_val = val->Image() ;
                    assoc_elem->Error("index value %s is out of range for for-generate %s", image_val, _prefix_id->Name()) ;
                    Strings::free(image_val) ;
                }
            }
        }
        // In rtl elab parse tree is not changed, so for any value of iterator
        // inner block remains same in terms of parse tree
        delete val ;
        return _prefix_id ;
    }

    delete val ;
    // It may be alternative label name :
    VhdlIdDef *id = assoc_elem->FindFullFormal() ;
    if (id && id->IsLabel()) return id ;

    return _prefix_id ; // FIXME : Check assoc list for for-loop index or if/case branch
}
VhdlIdDef *VhdlSelectedName::FindExternalObject(unsigned path_token, unsigned accent_count)
{
    // In static elaboration mode always search element ids as parse
    // tree is modified there.
    if (_unique_id && IsRtlElab()) return _unique_id ;
    _unique_id = 0 ;

    if (!_prefix || !_suffix) return 0 ;

    VhdlIdDef *prefix_id = _prefix->FindExternalObject(path_token, accent_count) ;
    if (!prefix_id) return 0 ;

    VhdlIdDef *elem = 0 ;
    VhdlScope *local_scope = 0 ;
    // Prefix can be
    // 1. Library name
    // 2. Entity simple name
    // 3. Component instance label
    // 4. Generate statement label
    // 5. Package simple name
    // 6. Package instance name
    // 7. Block label
    if (prefix_id->IsLibrary()) {
        VhdlLibrary *l ;
        if (Strings::compare(prefix_id->Name(),"work")) {
            l = vhdl_file::GetWorkLib() ;
        } else {
            l = vhdl_file::GetLibrary(prefix_id->Name(),1) ;
        }
        // Find Package by name
        VhdlPrimaryUnit *du = l ? l->GetPrimUnit(_suffix->Name()): 0 ;
        elem = du ? du->Id(): 0 ;
        if (!elem || !elem->IsPackage()) {
            Error("%s is not a package", _suffix->Name()) ;
            return 0 ;
        }
        // VIPER #7242 : This package may not be in the dependent list of containing unit, so
        // elaborate the package now
        if (VhdlNode::IsStaticElab()) {
            if (du) (void) du->StaticElaborate(0,0,0,0,0) ;
        } else {
        }
        _unique_id = elem ; return elem ;
    }
    if (prefix_id->IsEntity()) {
        // Prefix is top level entity name, find suffix in entity-architecture pair
        local_scope = prefix_id->LocalScope() ;
        elem = (local_scope) ? local_scope->FindSingleObjectLocal(_suffix->Name()): 0 ;
        if (!elem) { // _suffix may be in architecture
            VhdlSecondaryUnit *sec_unit = 0 ;
            VhdlPrimaryUnit *pu = prefix_id->GetPrimaryUnit() ;
            if (pu && (pu->NumOfSecondaryUnits() == 1)) {
                sec_unit = pu->GetSecondaryUnit(0) ;
                local_scope = sec_unit ? sec_unit->LocalScope(): 0 ;
            } else if (pu) { // Top entity contains multiple architectures
                Error("external name referring %s is not yet supported", "top unit with multiple architectures") ;
                return 0 ;
                // Find the elaborated one from 'instance_name' attribute handling
                // specific data structure
                //char *top_unit_name = (_instance_name) ? (char*)_instance_name->At(0): 0 ;
                // 'top_unit_name' contains 'entity_name(arch_name)'
                //char *sec_unit_name = (top_unit_name) ? (top_unit_name + Strings::len(prefix_id->Name()) + 1): 0 ;
                //if (sec_unit_name) sec_unit_name[Strings::len(sec_unit_name)] = 0 ;
                //sec_unit = pu->GetSecondaryUnit(sec_unit_name) ;
                //local_scope = sec_unit ? sec_unit->LocalScope(): 0 ;
            }
            elem = (local_scope) ? local_scope->FindSingleObjectLocal(_suffix->Name()): 0 ;
        }
        if (!elem) {
            Error("%s is not declared in %s", _suffix->Name(), prefix_id->Name()) ;
            return 0 ;
        }
        // Set _present_scope to unit_scope
        _present_scope = local_scope ;
        _unique_id = elem ; return elem ;
    }

    const char *arch_name = 0 ;
    VhdlIdDef *unit_id = 0 ;
    VhdlStatement *labeled_stmt = 0 ;
    if (prefix_id->IsLabel()) { // component/block/generate label
        labeled_stmt = prefix_id->GetStatement() ;
        unit_id = labeled_stmt ? labeled_stmt->GetInstantiatedUnit(): 0 ;
        if (unit_id && unit_id->IsComponent()) { // Component instantiation label
            // For component instance name in look into the entity/architecture declaration scope.
            // LRM Section 8.7
            VERIFIC_ASSERT(labeled_stmt) ; // Must hold since 'unit_id' is valid here
            const char *lib_name = labeled_stmt->GetLibraryOfInstantiatedUnit() ;
            const char* entity_name = labeled_stmt->GetInstantiatedUnitName() ;
            arch_name = labeled_stmt->GetArchitectureOfInstantiatedUnit() ;

            VhdlLibrary *vhdl_lib = vhdl_file::GetLibrary(lib_name,1) ;
            VhdlPrimaryUnit *pu = vhdl_lib ? vhdl_lib->GetPrimUnit(entity_name, 1/*restore*/) : 0 ;
            if (!pu) {
                Error("external name referring %s is not bound", "component instance label") ;
                return 0 ;
            }
            if (pu->IsVerilogModule()) {
                Error("external name referring %s is not bound", "component instance label for a Verilog module") ;
                return 0 ;
            }
            VhdlSecondaryUnit *architecture = pu->GetSecondaryUnit(arch_name) ;
            local_scope = architecture ? architecture->LocalScope() : pu->Id()->LocalScope() ;
        } else if (unit_id) {
            local_scope = unit_id->LocalScope() ; // scope of instantiated unit
            // Extract architecture name too, if 'unit_id' is entity id
            VhdlName *instantiated_name = labeled_stmt ? labeled_stmt->GetInstantiatedUnitNameNode(): 0 ;
            arch_name = (instantiated_name) ? instantiated_name->ArchitectureNameAspect(): 0 ;
        } else {
            // VIPER #7755 : Find the scope from the statement associated with label
            local_scope = labeled_stmt ? labeled_stmt->LocalScope(): 0 ;
        }
    }
    if (!local_scope) local_scope = prefix_id->LocalScope() ;

    // For package instantiation, get local scope of uninstantiated package to find next element
    if (prefix_id->IsPackageInst()) {
        //local_scope = prefix_id->GetUninstantiatedPackageScope() ;
        // Set actual type of generic types from generic map aspect (need to know data types
        // of objects defined within uninstantiated package
        prefix_id->SetActualTypes() ;
    }
    elem = (local_scope) ? local_scope->FindSingleObjectLocal(_suffix->Name()): 0 ;

    if (!elem && local_scope) {
        // If 'local_scope' is scope of package, find in package body/architecture also
        VhdlIdDef *owner_id = local_scope->GetOwner() ;
        VhdlPrimaryUnit *prim_unit = owner_id ? owner_id->GetPrimaryUnit(): 0 ;
        VhdlSecondaryUnit *sec_unit =  (prim_unit && owner_id) ? prim_unit->GetSecondaryUnit(owner_id->IsPackage() ? owner_id->Name(): arch_name): 0 ;
        local_scope = sec_unit ? sec_unit->LocalScope(): 0 ;
        elem = (local_scope) ? local_scope->FindSingleObjectLocal(_suffix->Name()): 0 ;
    }

    if (!elem) {
        Error("%s is not declared in %s", _suffix->Name(), prefix_id->Name()) ;
        return 0 ;
    }

    if (labeled_stmt && unit_id && unit_id->IsComponent() && (elem->IsPort() || elem->IsGeneric())) {
    }
    // Set _present_scope to local_scope
    _present_scope = local_scope ;
    _unique_id = elem ; return elem ;
}
VhdlIdDef *VhdlExternalName::FindExternalObject(unsigned /*path_token*/, unsigned /*accent_count*/)
{
    VhdlScope *old_scope = _present_scope ;
    _unique_id = _ext_path_name ? _ext_path_name->FindExternalObject(_path_token, _hat_count) : 0 ;

    _present_scope = old_scope ;
    if (!_unique_id) return 0 ;

    // Check whether class type and subtype indication match :

    return _unique_id ;
}

// Some elements of selected name may be special alias identifiers of package
// instantiation. Extract elaboration specific actual identifier from those alias
VhdlIdDef * VhdlSelectedName::GetElabSpecificUniqueId()
{
    // VIPER #8013 : Try to resolve selected name again in static elaboration mode.
    // Static elaboration will change parse tree for generate constructs
    if (!_unique_id || !_suffix) return _unique_id ;

    if (!IsStaticElab() && !IsVhdl2008()) return _unique_id ;
    VhdlIdDef *prefix_id = _prefix ? _prefix->GetElabSpecificUniqueId() : 0 ;
    if (prefix_id && _suffix->IsAll() && prefix_id->Type() && prefix_id->Type()->IsAccessType()) return prefix_id ;

    // VIPER #8371: Return 0 if prefix id is null (null means it is from unknown suffix)
    if (!prefix_id) return 0 ;
    if (prefix_id->IsLibrary()) return _unique_id ;
    if (prefix_id->IsFunction()) {
        if (_unique_id->IsPackageInstElement()) {
            return _unique_id->GetConnectedActualId() ;
        } else {
            return _unique_id ;
        }
    }

    // Check if this is a named, declared entity (with a scope)
    VhdlScope *local_scope = prefix_id->LocalScope() ;
    VhdlIdDef *prefix_type = prefix_id->Type() ;
    if (prefix_id->IsProtected() && prefix_type && prefix_type->LocalScope()) local_scope = prefix_type->LocalScope() ;
    if (!local_scope) {
        prefix_type = prefix_id->BaseType() ;
        local_scope = prefix_type ? prefix_type->LocalScope(): 0 ;
    }

    if (local_scope) {
        MapItem *item ;
        MapIter mi ;
        Map *decl_area = local_scope->DeclArea() ;
        char *suffix_str = Strings::save(_suffix->Name()) ;
        if (_suffix->IsStringLiteral()) suffix_str = Strings::strtolower(suffix_str) ;
        VhdlIdDef *elem = 0 ;;
        FOREACH_SAME_KEY_MAPITEM(decl_area, suffix_str, item) {
            elem = (VhdlIdDef*)item->Value() ;
        }
        Strings::free(suffix_str) ;
        if (elem) {
            if (elem->IsPackageInstElement()) {
                return elem->GetConnectedActualId() ;
            } else {
                return elem ;
            }
        } else if (IsStaticElab()) {
            // VIPER #7923: Cannot get suffix identifier, return null
            // Identifier may not exist
            return 0 ;
        }
    }
    return (_unique_id->IsPackageInstElement()) ? _unique_id->GetConnectedActualId() : _unique_id ;
}
VhdlIdDef *VhdlName::GetElabSpecificUniqueId()
{
    return GetId() ;
}
VhdlIdDef *VhdlDesignator::GetElabSpecificUniqueId()
{
    if (!_single_id) return 0 ;
    if (_single_id->IsPackageInstElement()) {
        return _single_id->GetConnectedActualId() ;
    }
    return _single_id ;
}
VhdlIdDef *VhdlIndexedName::GetElabSpecificUniqueId()
{
    if (!_prefix) return 0 ;
    VhdlIdDef *prefix_id = _prefix->GetElabSpecificUniqueId() ;
    if (!prefix_id && !_prefix->GetId()) prefix_id = _prefix_id ;
    if (prefix_id && prefix_id->IsPackageInstElement()) {
        return prefix_id->GetConnectedActualId() ;
    }
    return prefix_id ;
}

VhdlIdDef *VhdlExternalName::GetElabSpecificUniqueId()
{
    if (!_unique_id || IsStaticElab()) {
        _unique_id = FindExternalObject(_path_token, _hat_count) ;
    }
    return _unique_id ;
}
