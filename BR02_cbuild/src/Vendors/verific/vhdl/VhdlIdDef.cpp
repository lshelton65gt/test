/*
 *
 * [ File Version : 1.360 - 2014/03/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <stdio.h> // for FILE handling
#include <ctype.h> // for toupper

#include "Set.h"
#include "Map.h"
#include "Array.h"
#include "Strings.h"
#include "Message.h"

#include "vhdl_file.h"
#include "VhdlIdDef.h"
#include "vhdl_tokens.h"
#include "VhdlDeclaration.h"
#include "VhdlSpecification.h"
#include "VhdlStatement.h"
#include "VhdlName.h"
#include "VhdlScope.h"
#include "VhdlUnits.h"
#include "VhdlMisc.h"

// Need Value/Constraint for elaboration fields
#include "VhdlValue_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//CARBON_BEGIN
extern bool gMakeVhdlIdsLower;  //from CarbonContext.cxx
extern bool gMakeVhdlIdsUpper;  //from CarbonContext.cxx
//CARBON_END


/*****************************************************************/
/************************* Constructors/Destructors **************/
/*****************************************************************/

VhdlIdDef::VhdlIdDef(char *name)
  : VhdlTreeNode(),
    _name(name),
#ifdef VHDL_PRESERVE_ID_CASE
    _orig_name(Strings::save(name)), // save the incoming name of the id.
#endif
    _type(0),
    _attributes(0)
#ifdef VHDL_ID_SCOPE_BACKPOINTER
    , _owning_scope(0)
#endif
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    , _subtype_indication(0)
#endif
    , _constraint(0)
    , _value(0)
    //CARBON_BEGIN
    , _pragmas(0)
    //CARBON_END
{
    // VIPER #3537: Catch non-graphic characters in extended identifiers
    char *tmp = _name ;
    unsigned char c ;
    while (tmp && *tmp) {
        // Need to test character in ISO 8859-1 (8-bit character set), so cannot use "isgraph()".
        // ISO 8859-1 contains 191 graphic characters. Just two sections are not graphic.
        // Cast to unsigned char to do value compares:
        c = (unsigned char)(*tmp) ;
        if ((c <= 0x1F) || ((c >= 0x7F) && (c <= 0x9F))) {
//        if (!isgraph(c) && (c!=' ')) {
            Warning("extended identifier %s contains non-graphic character 0x%x", _name, c) ;
//            *tmp = ' ' ; // replace by space (to avoid junk messages later)
        }
        tmp++ ;
    }

#ifdef VHDL_PRESERVE_ID_CASE
    // Now here, always down-case the identifier, except when it's an extended identifier
    // or a character literal :
//CARBON_BEGIN
    // if (_name && *_name != '\\' && *_name != '\'') _name = Strings::strtolower(_name) ;   OldVerificVersion
    if (_name && *_name != '\\' && *_name != '\'' && *_name!='"'){
      if (gMakeVhdlIdsLower)
        Strings::strtolower(_name) ;
      else if (gMakeVhdlIdsUpper)
        Strings::strtoupper(_name) ;
    }
//CARBON_END
#else
    // If this is declaration of an operator, we need to down-case it
    // for scoping to work ("abs" and "ABS" are the same).
    if (_name && *_name == '"') _name = Strings::strtolower(_name) ;
#endif
}
VhdlIdDef::~VhdlIdDef()
{
    if (_present_scope) _present_scope->Undeclare(this) ;

    Strings::free(_name) ;
#ifdef VHDL_PRESERVE_ID_CASE
    Strings::free(_orig_name) ;
#endif

    delete _value ;
    delete _constraint ;

    if (_attributes) {
#if 0
        // attribute value is now a VhdlExpression backpointer (to the attribute spec). Don't delete.
        MapIter mi ;
        VhdlValue *value ;
        FOREACH_MAP_ITEM(_attributes, mi, 0, &value) {
            // Don't delete the key (VhdlIdDef*)
            delete value ;
        }
#endif
        // Viper #5432: need more cleanup to avoid stale pointers
        MapIter mi ;
        VhdlAttributeId *attribute_id ;
        FOREACH_MAP_ITEM(_attributes, mi, &attribute_id, 0) {
            if (attribute_id) attribute_id->RemoveIdFromAttributeSpec(this) ;
        }
        delete _attributes ;
    }
    //CARBON_BEGIN
    RemovePragmas();
    //CARBON_END
    // The rest (def and type) are pointers
}

VhdlLibraryId::VhdlLibraryId(char *name) : VhdlIdDef(name)
{
    // Force creation of library, since it's being asked for - VIPER #1896
    (void) vhdl_file::GetLibrary(name, 1 /*create if needed*/) ;
}
VhdlLibraryId::~VhdlLibraryId() { }

VhdlGroupId::VhdlGroupId(char *name) : VhdlIdDef(name) { }
VhdlGroupId::~VhdlGroupId() { }

VhdlGroupTemplateId::VhdlGroupTemplateId(char *name) : VhdlIdDef(name), _decl(0) { }
VhdlGroupTemplateId::~VhdlGroupTemplateId() { _decl = 0 ; }

VhdlAttributeId::VhdlAttributeId(char *name)
    : VhdlIdDef(name), _attribute_specs(0)
{ }
VhdlAttributeId::~VhdlAttributeId()
{
    // Viper #5432: Cannot delete the attribute spec as it'd generate
    // another stale pointer in the list of the declarations where it
    // resides.
    SetIter i ;
    VhdlAttributeSpec *attr_spec ;
    FOREACH_SET_ITEM(_attribute_specs, i, &attr_spec) attr_spec->InvalidateSpec() ;
    delete _attribute_specs ;
}

VhdlComponentId::VhdlComponentId(char *name)
  : VhdlIdDef(name),
    _ports(0),
    _generics(0),
    _scope(0),
    _component(0)
{ }
VhdlComponentId::~VhdlComponentId()
{
    // Delete the created arrays
    delete _generics ;
    delete _ports ;
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlAliasId::VhdlAliasId(char *name)
  : VhdlIdDef(name),
    _target_name(0),
    _target_id(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _kind(0)
{ }
VhdlAliasId::~VhdlAliasId() { }

VhdlFileId::VhdlFileId(char *name) : VhdlIdDef(name), _file(0)  { }
VhdlFileId::~VhdlFileId()
{
    // If _file is still set, then close the file. Do not close stdin/out.
    if (_file && _file!=stdin && _file!=stdout) {
        fclose(_file) ;
    }
    _file = 0 ;
}

VhdlVariableId::VhdlVariableId(char *name)
  : VhdlIdDef(name),
    _is_assigned(0),
    _is_used(0),
    _is_concurrent_assigned(0),
    _is_concurrent_used(0),
    _can_be_dual_port_ram(0),
    _can_be_multi_port_ram(0),
    _is_assigned_before_used(0),
    _is_used_before_assigned(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _is_shared(0)
{ }
VhdlVariableId::~VhdlVariableId() { }

VhdlSignalId::VhdlSignalId(char *name)
  : VhdlIdDef(name),
    _resolution_function(0),
    _is_assigned(0),
    _is_used(0),
    _is_concurrent_assigned(0),
    _is_concurrent_used(0),
    _can_be_dual_port_ram(0),
    _can_be_multi_port_ram(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _signal_kind(0)
{ }
VhdlSignalId::~VhdlSignalId() { }

VhdlConstantId::VhdlConstantId(char *name)
  : VhdlIdDef(name),
    _decl(0),
    _is_locally_static(0),
    _is_globally_static(1), // VIPER Issue #6349
    _is_seq_loop_iter(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0)
{ }
VhdlConstantId::~VhdlConstantId()
{
    // Uncouple _decl loop for deferred constants.
    if (_decl && _decl != this) {
        _decl->SetDecl(0) ; // CHECK ME : Should we set it to itself ?
    }
}

VhdlProtectedTypeId::VhdlProtectedTypeId(char *name)
  : VhdlTypeId(name),
    _type_def(0),
    _body_id(0)
{
    _type_enum = PROTECTED_TYPE ;
}

VhdlProtectedTypeId::~VhdlProtectedTypeId()
{
    _type_def = 0 ;
    if (_scope) _scope->SetOwner(0) ;
}

VhdlProtectedTypeBodyId::VhdlProtectedTypeBodyId(char *name)
  : VhdlTypeId(name),
    _type_def_body(0),
    _typedef_id(0)
{
    _type_enum = PROTECTED_TYPE_BODY ;
}

VhdlProtectedTypeBodyId::~VhdlProtectedTypeBodyId()
{
    _type_def_body = 0 ;
    if (_scope) _scope->SetOwner(0) ;
}

VhdlTypeId::VhdlTypeId(char *name)
  : VhdlIdDef(name),
    _type_enum(UNKNOWN_TYPE),
    _list(0),
    _scope(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    ,_verilog_pkg_lib_name(0)
    ,_verilog_pkg_name(0)
#endif
    ,_type_decl(0) // VIPER #7343

{ }
VhdlTypeId::~VhdlTypeId()
{
    // Delete the list. It contains VhdlIdDef pointers, so don't free the elements.
    delete _list ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    Strings::free(_verilog_pkg_lib_name) ;
    Strings::free(_verilog_pkg_name) ;
    _verilog_pkg_lib_name = 0 ;
    _verilog_pkg_name = 0 ;
#endif
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlGenericTypeId::VhdlGenericTypeId(char *name)

  : VhdlTypeId(name)
{
    _type_enum = GENERIC_TYPE ;
}
VhdlGenericTypeId::~VhdlGenericTypeId()

{ }

VhdlUniversalInteger::VhdlUniversalInteger(char *name)
  : VhdlTypeId(name)
{
    _type_enum = INTEGER_TYPE ;
}
VhdlUniversalInteger::~VhdlUniversalInteger() { }

VhdlUniversalReal::VhdlUniversalReal(char *name)
  : VhdlTypeId(name)
{
    _type_enum = REAL_TYPE ;
}
VhdlUniversalReal::~VhdlUniversalReal() { }

VhdlAnonymousType::VhdlAnonymousType(char *name) : VhdlTypeId(name)  { }
VhdlAnonymousType::~VhdlAnonymousType() { }

VhdlSubtypeId::VhdlSubtypeId(char *name)
  : VhdlIdDef(name),
    _resolution_function(0),
    _is_unconstrained(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _type_decl(0)
{ }
VhdlSubtypeId::~VhdlSubtypeId() { }

VhdlSubprogramId::VhdlSubprogramId(char *name)
  : VhdlIdDef(name),
    _generics(0),
    _args(0),
    _spec(0),
    _body(0),
    _pragma_function(0),
    _pragma_signed(0),
    _is_assigned_before_used(0),
    _is_used_before_assigned(0),
    _is_procedure(0)
    ,_is_processing(0)
    ,_is_label_applies_to_pragma(0)
    ,_is_generic(0)
    ,_is_overwritten_predefinedOperator(0)
    ,_sub_inst(0)
{ }
VhdlSubprogramId::~VhdlSubprogramId()
{
    // Delete the argument list. It contains VhdlIdDefs, so don't free the elements.
    delete _args ;
    delete _generics ;
}

// Operator gets created with operator id name
VhdlOperatorId::VhdlOperatorId(unsigned oper_type)
  : VhdlSubprogramId(Strings::save(VhdlTreeNode::OperatorName(oper_type))),
    _oper_type(oper_type), _owns_arg_id(0)
{ }
VhdlOperatorId::~VhdlOperatorId()
{
    if (_args && _owns_arg_id) { // VIPER #5996
        unsigned i ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(_args, i, id) delete id ;
    }
}

VhdlInterfaceId::VhdlInterfaceId(char *name)
  : VhdlIdDef(name),
    _resolution_function(0),
    _object_kind(0),
    _kind(0),
    _mode(0),
    _signal_kind(0),
    _has_init_assign(0),
    _is_unconstrained(0),
    _is_assigned(0),
    _is_used(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0),
    _is_ref_formal(0),
    _can_be_multi_port_ram(0)
   , _verilog_assoc_list(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
   , _verilog_actual_size(0)
#endif
   , _is_actual_present(0)
   , _ref_expr(0)
{ }
VhdlInterfaceId::~VhdlInterfaceId()
{
    VhdlDiscreteRange* range_item ;
    unsigned k ;
    FOREACH_ARRAY_ITEM(_verilog_assoc_list, k, range_item) {
        if (!range_item) continue ;
        delete range_item ;
    }
    delete _verilog_assoc_list ;
}

VhdlEnumerationId::VhdlEnumerationId(char *name)
  : VhdlIdDef(name),
    _position(0),
    _bit_encoding(0),
    _onehot_encoded(0),
    _enum_encoded(0),
    _encoding(0)
{ }
VhdlEnumerationId::~VhdlEnumerationId()
{
    Strings::free(_encoding) ;
}

VhdlElementId::VhdlElementId(char *name)
  : VhdlIdDef(name),
    _position(0),
    _is_locally_static_type(0),
    _is_globally_static_type(0)
{ }
VhdlElementId::~VhdlElementId() { }

VhdlPhysicalUnitId::VhdlPhysicalUnitId(char *name) : VhdlIdDef(name), _position(0), _inv_position(1)  { }
VhdlPhysicalUnitId::~VhdlPhysicalUnitId() { }

VhdlEntityId::VhdlEntityId(char *name)
  : VhdlIdDef(name),
    _ports(0),
    _generics(0),
    _scope(0),
    _unit(0)
{ }
VhdlEntityId::~VhdlEntityId()
{
    delete _ports ;
    delete _generics ;
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlArchitectureId::VhdlArchitectureId(char *name)
  : VhdlIdDef(name),
    _entity(0),
    _scope(0),
    _unit(0)
{ }
VhdlArchitectureId::~VhdlArchitectureId()
{
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlConfigurationId::VhdlConfigurationId(char *name)
  : VhdlIdDef(name),
    _entity(0),
    _scope(0),
    _unit(0)
{ }
VhdlConfigurationId::~VhdlConfigurationId()
{
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlPackageId::VhdlPackageId(char *name)
  : VhdlIdDef(name),
    _generics(0),
    _scope(0),
    _unit(0),
    _decl(0),
    _uninstantiated_pkg_id(0)
{ }
VhdlPackageId::~VhdlPackageId()
{
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
    delete _generics ;
}

VhdlPackageBodyId::VhdlPackageBodyId(char *name)
  : VhdlIdDef(name),
    _scope(0),
    _unit(0)
{ }
VhdlPackageBodyId::~VhdlPackageBodyId()
{
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlContextId::VhdlContextId(char *name)
  : VhdlIdDef(name),
    _scope(0),
    _unit(0)
{ }
VhdlContextId::~VhdlContextId()
{
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlLabelId::VhdlLabelId(char *name)
  : VhdlIdDef(name),
    _scope(0),
    _statement(0),
    _primary_binding(0)
{ }
VhdlLabelId::~VhdlLabelId()
{
    if (_scope) _scope->SetOwner(0); // Disconnect owner from "this"
}

VhdlBlockId::VhdlBlockId(char *name)
  : VhdlLabelId(name),
    _generics(0),
    _ports(0)
{ }
VhdlBlockId::~VhdlBlockId()
{
    delete _generics ;
    delete _ports ;
}
VhdlPackageInstElement::VhdlPackageInstElement(char *name)
    : VhdlIdDef(name),
    _actual_id(0),
    _target_id(0)
{
}
VhdlPackageInstElement::~VhdlPackageInstElement()
{
    _actual_id = 0 ;
    _target_id = 0 ;
}

/*****************************************************************/
/********************** Functions  *******************************/
/*****************************************************************/

/*****************************************************************/
/****** Pointers back to the parse tree, from identifiers ********/
/*****************************************************************/

void VhdlIdDef::SetPrimaryUnit(VhdlPrimaryUnit * /*unit*/)              { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::SetSecondaryUnit(VhdlSecondaryUnit * /*unit*/)          { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::SetComponentDecl(VhdlComponentDecl * /*component*/)     { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::SetStatement(VhdlStatement * /*statement*/)             { VERIFIC_ASSERT(0) ; }

VhdlPrimaryUnit *VhdlIdDef::GetPrimaryUnit() const                      { return 0 ; }
VhdlSecondaryUnit *VhdlIdDef::GetSecondaryUnit() const                  { return 0 ; }
VhdlDesignUnit *VhdlIdDef::GetDesignUnit() const                        { return 0 ; }
VhdlComponentDecl *VhdlIdDef::GetComponentDecl() const                  { return 0 ; }
VhdlStatement *VhdlIdDef::GetStatement() const                          { return 0 ; }

void VhdlEntityId::SetPrimaryUnit(VhdlPrimaryUnit *unit)                { _unit = unit ; }
void VhdlConfigurationId::SetPrimaryUnit(VhdlPrimaryUnit *unit)         { _unit = unit ; }
void VhdlPackageId::SetPrimaryUnit(VhdlPrimaryUnit *unit)               { _unit = unit ; }
void VhdlContextId::SetPrimaryUnit(VhdlPrimaryUnit *unit)               { _unit = unit ; }
void VhdlArchitectureId::SetSecondaryUnit(VhdlSecondaryUnit *unit)      { _unit = unit ; }
void VhdlPackageBodyId::SetSecondaryUnit(VhdlSecondaryUnit *unit)       { _unit = unit ; }
void VhdlComponentId::SetComponentDecl(VhdlComponentDecl *component)    { _component = component ; }
void VhdlLabelId::SetStatement(VhdlStatement *statement)                { _statement = statement ; }

VhdlPrimaryUnit *VhdlEntityId::GetPrimaryUnit() const                   { return _unit ; }
VhdlPrimaryUnit *VhdlConfigurationId::GetPrimaryUnit() const            { return _unit ; }
VhdlPrimaryUnit *VhdlPackageId::GetPrimaryUnit() const                  { return _unit ; }
VhdlPrimaryUnit *VhdlContextId::GetPrimaryUnit() const                  { return _unit ; }
VhdlSecondaryUnit *VhdlArchitectureId::GetSecondaryUnit() const         { return _unit ; }
VhdlSecondaryUnit *VhdlPackageBodyId::GetSecondaryUnit() const          { return _unit ; }
VhdlComponentDecl *VhdlComponentId::GetComponentDecl() const            { return _component ; }
VhdlStatement *VhdlLabelId::GetStatement() const                        { return _statement ; }

VhdlDesignUnit *VhdlEntityId::GetDesignUnit() const                     { return _unit ; }
VhdlDesignUnit *VhdlConfigurationId::GetDesignUnit() const              { return _unit ; }
VhdlDesignUnit *VhdlPackageId::GetDesignUnit() const                    { return _unit ; }
VhdlDesignUnit *VhdlContextId::GetDesignUnit() const                    { return _unit ; }
VhdlDesignUnit *VhdlArchitectureId::GetDesignUnit() const               { return _unit ; }
VhdlDesignUnit *VhdlPackageBodyId::GetDesignUnit() const                { return _unit ; }

/*****************************************************************/
/****** name of id  ********/
/*****************************************************************/

const char *VhdlIdDef::Name() const
{
    return _name ;
}

const char *VhdlIdDef::OrigName() const
{
#ifdef VHDL_PRESERVE_ID_CASE
//CARBON_BEGIN
    if (gMakeVhdlIdsLower)
      Strings::strtolower(_orig_name) ;
    else if (gMakeVhdlIdsUpper)
      Strings::strtoupper(_orig_name) ;
//CARBON_END
    return _orig_name ;
#else
    return _name ;
#endif
}

void
VhdlIdDef::ChangeIdName(char *new_name)
{
    Strings::free(_name) ;
#ifdef VHDL_PRESERVE_ID_CASE
    Strings::free(_orig_name) ;
#endif
    _name = new_name ;
#ifdef VHDL_PRESERVE_ID_CASE
    _orig_name = Strings::save(new_name) ;
//CARBON_BEGIN
    //   if (_name && *_name != '\\' && *_name != '\'') _name = Strings::strtolower(_name) ;   // OldVerificVersion
    if (_name && *_name != '\\' && *_name != '\'' && *_name!='"'){
      if (gMakeVhdlIdsLower)
	Strings::strtolower(_name) ;
      else if (gMakeVhdlIdsUpper)
	Strings::strtoupper(_name) ;
    }
//CARBON_END
#else
    if (_name && *_name == '"') _name = Strings::strtolower(_name) ;
#endif
}

/*****************************************************************/
/****** name of the library in which this id resides   ********/
/*****************************************************************/

const char *VhdlIdDef::GetContainingLibraryName() const
{
    // Works only if we have a local scope of this id.
    VhdlScope *local_scope = LocalScope() ;
    if (!local_scope) return 0 ;

    // Get the primary unit :
    VhdlIdDef *prim_unit = local_scope->GetContainingPrimaryUnit() ;
    if (!prim_unit) return 0 ;

    // Get the tree of that :
    VhdlPrimaryUnit *tree = prim_unit->GetPrimaryUnit() ;
    if (!tree) return 0 ;

    // Get the name of the owner of this tree (which is a library name) :
    VhdlLibrary *lib = tree->Owner() ;
    return (lib) ? lib->Name() : 0 ;
}

/*****************************************************************/
/****** scope of id (the scope that it is associated with) ********/
/*****************************************************************/

void VhdlIdDef::SetLocalScope(VhdlScope * /*scope*/)
{
    // Should VERIFIC_ASSERT here, since we covered all id's with a scope.
    VERIFIC_ASSERT(0) ;
}

void VhdlSubprogramId::SetLocalScope(VhdlScope * /*scope*/)
{
    // Subprogram scope should come from the body (if there)
    // Check out VhdlSubprogramId::LocalScope() .
    // FIX ME : Make consistent with other ids with local scopes.
}

void VhdlTypeId::SetLocalScope(VhdlScope *scope)            { _scope = scope ; }
void VhdlLabelId::SetLocalScope(VhdlScope *scope)           { _scope = scope ; }
void VhdlEntityId::SetLocalScope(VhdlScope *scope)          { _scope = scope ; }
void VhdlArchitectureId::SetLocalScope(VhdlScope *scope)    { _scope = scope ; }
void VhdlConfigurationId::SetLocalScope(VhdlScope *scope)   { _scope = scope ; }
void VhdlPackageId::SetLocalScope(VhdlScope *scope)         { _scope = scope ; }
void VhdlPackageBodyId::SetLocalScope(VhdlScope *scope)     { _scope = scope ; }
void VhdlComponentId::SetLocalScope(VhdlScope *scope)       { _scope = scope ; }
void VhdlContextId::SetLocalScope(VhdlScope *scope)         { _scope = scope ; }

VhdlScope *VhdlIdDef::LocalScope() const
{
    return 0 ; // This id has no scope associated with it.
}

VhdlScope *VhdlSubprogramId::LocalScope() const
{
    // Return the local scope of the construct that this identifier defines
    if (_body) return _body->LocalScope() ;
    if (_spec) return _spec->LocalScope() ;
    return 0 ;
}

VhdlScope *VhdlTypeId::LocalScope() const                   { return _scope ; }
VhdlScope *VhdlLabelId::LocalScope() const                  { return _scope ; }
VhdlScope *VhdlEntityId::LocalScope() const                 { return _scope ; }
VhdlScope *VhdlArchitectureId::LocalScope() const           { return _scope ; }
VhdlScope *VhdlConfigurationId::LocalScope() const          { return _scope ; }
VhdlScope *VhdlPackageId::LocalScope() const                { return _scope ; }
VhdlScope *VhdlContextId::LocalScope() const                { return _scope ; }
VhdlScope *VhdlPackageBodyId::LocalScope() const            { return _scope ; }
VhdlScope *VhdlComponentId::LocalScope() const              { return _scope ; }
VhdlScope *VhdlSubtypeId::LocalScope() const                { return (_type) ? _type->LocalScope() : 0 ; }
VhdlScope *VhdlAliasId::LocalScope() const                  { return (_target_id) ? _target_id->LocalScope() : 0 ; }

VhdlScope *VhdlPackageId::GetUninstantiatedPackageScope() const
{
    VhdlIdDef *uninstantiated_package_id = GetUninstantiatedPackageId() ;
    return (uninstantiated_package_id) ? uninstantiated_package_id->LocalScope(): 0 ;
}
void VhdlPackageId::SetUninstantiatedPackageId(VhdlIdDef *id)
{
    _uninstantiated_pkg_id = id ;
}
/**************************************************************/
// Locally/Globally Static Names : LRM section 6.1
/**************************************************************/
unsigned VhdlIdDef::IsLocallyStaticName() const
{
    if (IsFunction() || IsProcedure() || IsAccessType()) return 0 ;
    return 1 ;
}

unsigned VhdlAliasId::IsLocallyStaticName() const
{
    if (_target_name && !_target_name->IsLocallyStaticName()) return 0 ;
    return 1 ;
}

unsigned VhdlIdDef::IsGloballyStaticName() const
{
    return IsLocallyStaticName() ;
}

unsigned VhdlAliasId::IsGloballyStaticName() const
{
    // LRM 6.1 stipulates alias target to be a locally static name..
    // But popular simulators do not enforce it (no warning / error)
    if (_target_name && !_target_name->IsGloballyStaticName()) return 0 ;
    return 1 ;
}

//******************* Locally/Globally Static ********************/
// Following APIs follow LRM sections 7.4.1 and 7.4.2
//****************************************************************/

unsigned VhdlIdDef::IsLocallyStatic() const                { return IsLibrary() || IsDesignUnit() ; }
void VhdlIdDef::SetLocallyStatic()                         { }
void VhdlConstantId::SetLocallyStatic()                    { _is_locally_static = 1 ; }

void VhdlIdDef::SetGloballyStatic()                        { }

unsigned VhdlIdDef::IsGloballyStatic() const               { return IsGeneric() || IsLibrary() || IsDesignUnit() || IsSubprogram() ; }

unsigned VhdlEnumerationId::IsLocallyStatic() const        { return 1 ; }
unsigned VhdlEnumerationId::IsGloballyStatic() const       { return 1 ; }

unsigned VhdlElementId::IsLocallyStatic() const            { return 1 ; }
unsigned VhdlElementId::IsGloballyStatic() const           { return 1 ; }

unsigned VhdlPhysicalUnitId::IsLocallyStatic() const       { return 1 ; }
unsigned VhdlPhysicalUnitId::IsGloballyStatic() const      { return 1 ; }

unsigned VhdlAliasId::IsLocallyStatic() const
{
    return _target_id ? _target_id->IsLocallyStatic() : 1 ;
}

unsigned VhdlAliasId::IsGloballyStatic() const
{
    return _target_id ? _target_id->IsGloballyStatic() : 1 ;
}

unsigned VhdlIdDef::GetLocallyStatic() const               { return IsLocallyStatic() ; }
unsigned VhdlConstantId::GetLocallyStatic() const          { return _is_locally_static ; }

unsigned VhdlConstantId::IsLocallyStatic() const
{
    if (_is_locally_static) return 1 ;
    return _decl ? _decl->GetLocallyStatic() : 0 ;
}

unsigned VhdlConstantId::IsGloballyStatic() const          { return _is_seq_loop_iter ? 0 : _is_globally_static ; } // Viper #4517: generate loop id is not marked as loop_iter, Viper #6349 - globally_static flag added

unsigned VhdlIdDef::IsLocallyStaticType() const            { return 0 ; }
unsigned VhdlIdDef::IsGloballyStaticType() const           { return 0 ; }

unsigned VhdlEnumerationId::IsLocallyStaticType() const    { return 1 ; }
unsigned VhdlEnumerationId::IsGloballyStaticType() const   { return 1 ; }

unsigned VhdlAliasId::IsLocallyStaticType() const          { return _is_locally_static_type ; }
unsigned VhdlAliasId::IsGloballyStaticType() const         { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlElementId::IsLocallyStaticType() const        { return _is_locally_static_type ; }
unsigned VhdlElementId::IsGloballyStaticType() const       { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlConstantId::IsLocallyStaticType() const       { return _is_locally_static_type ; }
unsigned VhdlConstantId::IsGloballyStaticType() const      { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlInterfaceId::IsLocallyStaticType() const      { return _is_locally_static_type ; }
unsigned VhdlInterfaceId::IsGloballyStaticType() const     { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlSignalId::IsLocallyStaticType() const         { return _is_locally_static_type ; }
unsigned VhdlSignalId::IsGloballyStaticType() const        { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlVariableId::IsLocallyStaticType() const       { return _is_locally_static_type ; }
unsigned VhdlVariableId::IsGloballyStaticType() const      { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlTypeId::IsLocallyStaticType() const           { return _is_locally_static_type ; }
unsigned VhdlTypeId::IsGloballyStaticType() const          { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlTypeId::IsLocallyStatic() const               { return _is_locally_static_type ; }
unsigned VhdlTypeId::IsGloballyStatic() const              { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlSubtypeId::IsLocallyStaticType() const        { return _is_locally_static_type ; }
unsigned VhdlSubtypeId::IsGloballyStaticType() const       { return _is_locally_static_type || _is_globally_static_type ; }

unsigned VhdlSubtypeId::IsLocallyStatic() const            { return _is_locally_static_type ; }
unsigned VhdlSubtypeId::IsGloballyStatic() const           { return _is_locally_static_type || _is_globally_static_type ; }

void VhdlIdDef::SetLocallyStaticType()                     { }
void VhdlIdDef::SetGloballyStaticType()                    { }

void VhdlSignalId::SetLocallyStaticType()                  { _is_locally_static_type = 1 ; _is_globally_static_type = 1 ; }
void VhdlSignalId::SetGloballyStaticType()                 { _is_globally_static_type = 1 ; }

void VhdlVariableId::SetLocallyStaticType()                { _is_locally_static_type = 1 ; _is_globally_static_type = 1 ; }
void VhdlVariableId::SetGloballyStaticType()               { _is_globally_static_type = 1 ; }

void VhdlTypeId::SetLocallyStaticType()                    { _is_locally_static_type = 1 ; _is_globally_static_type = 1 ; }
void VhdlTypeId::SetGloballyStaticType()                   { _is_globally_static_type = 1 ; }

void VhdlSubtypeId::SetLocallyStaticType()                 { _is_locally_static_type = 1 ; _is_globally_static_type = 1 ; }
void VhdlSubtypeId::SetGloballyStaticType()                { _is_globally_static_type = 1 ; }

void VhdlInterfaceId::SetLocallyStaticType()                 { _is_locally_static_type = 1 ; _is_globally_static_type = 1 ; }
void VhdlInterfaceId::SetGloballyStaticType()                { _is_globally_static_type = 1 ; }

//*********************** Homographs ************************/
// Two id's are always homograph inless they are BOTH
// overloadable (subprogram or enum literal
//***********************************************************/

unsigned VhdlIdDef::IsHomograph(VhdlIdDef * /*id*/) const
{
    // One is not overloadable, so these  are homographs
    return 1 ;
}
unsigned VhdlSubprogramId::IsHomograph(VhdlIdDef *id) const
{
    // homograph if at most one is overloadable
    if (!id || (!id->IsSubprogram() && !id->IsEnumerationLiteral())) return 1 ;
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst && !IsGeneric()) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;

    VhdlIdDef *type = uninstantiated_subprog ? uninstantiated_subprog->Type() : _type ;

    // VIPER #7736: If return type is generic type for both identifiers, allow
    // them, but check the names
    VhdlIdDef *id_type = id->Type() ;
    // not a homograph if they have different return types
    if (!type || !id_type || (!(type->IsGeneric() && id_type->IsGeneric() && Strings::compare(type->Name(), id_type->Name())) && !type->TypeMatch(id_type))) return 0 ;

    // If the number of arguments differs, it is not a homograph
    if (NumOfArgs() != id->NumOfArgs()) return 0 ;

    // Each argument must type-match
    for (unsigned i = 0 ; i<NumOfArgs(); i++) {
        VhdlIdDef *arg_type = ArgType(i) ;
        if (!arg_type) return 0 ;
        // VIPER #7736: If arg type is defined by generic for both cases, allow
        // But check the names to not allow t1 with t2
        VhdlIdDef *id_arg_type = id->ArgType(i) ;
        if (!id_arg_type) return 0 ;
        if (arg_type->IsGeneric() && id_arg_type->IsGeneric() && Strings::compare(arg_type->Name(), id_arg_type->Name())) continue ;
        if (arg_type->TypeMatch(id_arg_type)) continue ;
        return 0 ; // arg-type difference
    }
    return 1 ; // Homograph !
}

unsigned VhdlEnumerationId::IsHomograph(VhdlIdDef *id) const
{
    // homograph if at most one is overloadable
    if (!id || (!id->IsSubprogram() && !id->IsEnumerationLiteral())) return 1 ;

    // not a homograph if they have different return types
    if (_type && !_type->TypeMatch(id->Type())) return 0 ;

    // If the subprogram has arguments, it is not a homograph of the enum literal
    if (id->NumOfArgs()) return 0 ;

    return 1; // Homograph !
}

/*************************************************************/
/********* EntityClass of the identifier (LRM 5.1) ***********/
/*************************************************************/

unsigned VhdlIdDef::EntityClass() const             { return 0 ; }
unsigned VhdlLibraryId::EntityClass()  const        { return VHDL_library ; }
unsigned VhdlGroupId::EntityClass() const           { return VHDL_group ; }
unsigned VhdlGroupTemplateId::EntityClass() const   { return VHDL_group ; }
unsigned VhdlAttributeId::EntityClass() const       { return VHDL_attribute ; }
unsigned VhdlComponentId::EntityClass() const       { return VHDL_component ; }
unsigned VhdlAliasId::EntityClass() const           { return (_target_id) ? _target_id->EntityClass() : 0 ; } // alias denotes the object LRM 5.1(53)
unsigned VhdlFileId::EntityClass() const            { return VHDL_file ; }
unsigned VhdlVariableId::EntityClass() const        { return VHDL_variable ; }
unsigned VhdlSignalId::EntityClass() const          { return VHDL_signal ; }
unsigned VhdlConstantId::EntityClass() const        { return VHDL_constant ; }
unsigned VhdlTypeId::EntityClass() const            { return VHDL_type ; }
unsigned VhdlSubtypeId::EntityClass() const         { return VHDL_subtype ; }
unsigned VhdlSubprogramId::EntityClass() const      { return (_is_procedure) ? VHDL_procedure : VHDL_function ; }
unsigned VhdlInterfaceId::EntityClass() const       { return IsVariable() ? VHDL_variable : (IsSignal() ? VHDL_signal : (IsConstant() ? VHDL_constant : 0)) ; }
unsigned VhdlEnumerationId::EntityClass() const     { return VHDL_literal ; }
unsigned VhdlElementId::EntityClass() const         { return 0 ; } // record element has no 'class'
unsigned VhdlPhysicalUnitId::EntityClass() const    { return VHDL_units ; }
unsigned VhdlEntityId::EntityClass() const          { return VHDL_entity ; }
unsigned VhdlArchitectureId::EntityClass() const    { return VHDL_architecture ; }
unsigned VhdlConfigurationId::EntityClass() const   { return VHDL_configuration ; }
unsigned VhdlPackageId::EntityClass() const         { return VHDL_package ; }
unsigned VhdlPackageBodyId::EntityClass() const     { return VHDL_package ; } // does not appear in real life
unsigned VhdlContextId::EntityClass() const         { return VHDL_context ; }
unsigned VhdlLabelId::EntityClass() const           { return VHDL_label ; }

/*************************************************************/
/************* EntityClass of the identifier *****************/
/*************************************************************/

unsigned VhdlIdDef::IsLabel() const                 { return 0 ; }
unsigned VhdlIdDef::IsLibrary() const               { return 0 ; }
unsigned VhdlIdDef::IsEntity() const                { return 0 ; }
unsigned VhdlIdDef::IsArchitecture() const          { return 0 ; }
unsigned VhdlIdDef::IsConfiguration() const         { return 0 ; }
unsigned VhdlIdDef::IsContext() const               { return 0 ; }
unsigned VhdlIdDef::IsPackage() const               { return 0 ; }
unsigned VhdlIdDef::IsPackageBody() const           { return 0 ; }
unsigned VhdlIdDef::IsDesignUnit() const            { return 0 ; }
unsigned VhdlIdDef::IsPackageInst() const           { return 0 ; }
unsigned VhdlIdDef::IsSubprogram() const            { return 0 ; }
unsigned VhdlIdDef::IsSubprogramInst() const        { return 0 ; }
unsigned VhdlIdDef::IsEnumerationLiteral() const    { return 0 ; }
unsigned VhdlIdDef::IsPhysicalUnit() const          { return 0 ; }
unsigned VhdlIdDef::IsComponent() const             { return 0 ; }
unsigned VhdlIdDef::IsComponentInstantiation() const { return 0 ; }
unsigned VhdlIdDef::IsAttribute() const             { return 0 ; }
unsigned VhdlIdDef::IsPredefinedOperator() const    { return 0 ; }
unsigned VhdlIdDef::IsInterfaceObject(unsigned /*object_kind*/) const { return 0 ; }
unsigned VhdlIdDef::IsGeneric() const               { return 0 ; }
unsigned VhdlIdDef::IsPort() const                  { return 0 ; }
unsigned VhdlIdDef::IsSubprogramFormal() const      { return 0 ; }
unsigned VhdlIdDef::IsInput() const                 { return 0 ; }
unsigned VhdlIdDef::IsOutput() const                { return 0 ; }
unsigned VhdlIdDef::IsInout() const                 { return 0 ; }
unsigned VhdlIdDef::IsLinkage() const               { return 0 ; }
unsigned VhdlIdDef::IsBuffer() const                { return 0 ; }
unsigned VhdlIdDef::IsRecordElement() const         { return 0 ; }
unsigned VhdlIdDef::IsConstant() const              { return 0 ; }
unsigned VhdlIdDef::IsVariable() const              { return 0 ; }
unsigned VhdlIdDef::IsSharedVariable() const        { return 0 ; }
unsigned VhdlIdDef::IsSignal() const                { return 0 ; }
unsigned VhdlIdDef::IsFunction() const              { return 0 ; }
unsigned VhdlIdDef::IsProcedure() const             { return 0 ; }
unsigned VhdlIdDef::IsFile() const                  { return (_type) ? _type->IsFileType() : 0 ; }
unsigned VhdlIdDef::IsGroup() const                 { return 0 ; }
unsigned VhdlIdDef::IsGroupTemplate() const         { return 0 ; }
unsigned VhdlIdDef::IsBlock() const                 { return 0 ; }

unsigned VhdlLabelId::IsLabel() const               { return 1 ; }
unsigned VhdlLabelId::IsComponentInstantiation() const { return (_statement && _statement->GetInstantiatedUnitNameNode()) ? 1 : 0 ; }
unsigned VhdlLibraryId::IsLibrary() const           { return 1 ; }
unsigned VhdlEntityId::IsEntity() const             { return 1 ; }
unsigned VhdlArchitectureId::IsArchitecture() const { return 1 ; }
unsigned VhdlConfigurationId::IsConfiguration() const { return 1 ; }
unsigned VhdlPackageId::IsPackage() const           { return 1 ; }
unsigned VhdlPackageId::IsPackageInst() const       { return (_unit && _unit->IsPackageInstDecl()) ? 1: 0 ; }
unsigned VhdlContextId::IsContext() const           { return 1 ; }
unsigned VhdlPackageBodyId::IsPackageBody() const   { return 1 ; }

unsigned VhdlEntityId::IsDesignUnit() const         { return 1 ; }
unsigned VhdlArchitectureId::IsDesignUnit() const   { return 1 ; }
unsigned VhdlConfigurationId::IsDesignUnit() const  { return 1 ; }
unsigned VhdlPackageId::IsDesignUnit() const        { return 1 ; }
unsigned VhdlContextId::IsDesignUnit() const        { return 1 ; }
unsigned VhdlPackageBodyId::IsDesignUnit() const    { return 1 ; }

unsigned VhdlSubprogramId::IsSubprogram() const     { return 1 ; }
unsigned VhdlSubprogramId::IsSubprogramInst() const { return _sub_inst ? 1: 0 ; }
unsigned VhdlEnumerationId::IsEnumerationLiteral() const { return 1 ; }
unsigned VhdlPhysicalUnitId::IsPhysicalUnit() const { return 1 ; }
unsigned VhdlComponentId::IsComponent() const       { return 1 ; }
unsigned VhdlAttributeId::IsAttribute() const       { return 1 ; }
unsigned VhdlOperatorId::IsPredefinedOperator() const { return 1 ; }
unsigned VhdlInterfaceId::IsInterfaceObject(unsigned object_kind) const { return (object_kind || (DecodeObjectKind(_object_kind) == object_kind)) ? 1 : 0 ; }
unsigned VhdlInterfaceId::IsGeneric() const         { return (DecodeObjectKind(_object_kind)==VHDL_generic) ? 1 : 0 ; }
unsigned VhdlInterfaceId::IsPort() const            { return (DecodeObjectKind(_object_kind)==VHDL_port) ? 1 : 0 ; }
unsigned VhdlInterfaceId::IsSubprogramFormal() const { return (!_object_kind || (DecodeObjectKind(_object_kind) == VHDL_parameter)) ? 1 : 0 ; }
unsigned VhdlInterfaceId::IsInput() const           { return (DecodeMode(_mode) == VHDL_in) ; }
unsigned VhdlInterfaceId::IsOutput() const          { return (DecodeMode(_mode) == VHDL_out) ; }
unsigned VhdlInterfaceId::IsInout() const           { return (DecodeMode(_mode) == VHDL_inout) ; }
unsigned VhdlInterfaceId::IsLinkage() const         { return (DecodeMode(_mode) == VHDL_linkage) ; }
unsigned VhdlInterfaceId::IsBuffer() const          { return (DecodeMode(_mode) == VHDL_buffer) ; }
unsigned VhdlElementId::IsRecordElement() const     { return 1 ; }
unsigned VhdlFileId::IsFile() const                 { return 1 ; }

unsigned VhdlConstantId::IsConstant() const         { return 1 ; }
unsigned VhdlInterfaceId::IsConstant() const        { return (DecodeKind(_kind)==VHDL_constant || DecodeObjectKind(_object_kind)==VHDL_generic) ? 1 : 0 ; }
unsigned VhdlVariableId::IsVariable() const         { return 1 ; }
unsigned VhdlInterfaceId::IsVariable() const        { return (DecodeKind(_kind)==VHDL_variable) ? 1 : 0 ; }
unsigned VhdlSignalId::IsSignal() const             { return 1 ; }
unsigned VhdlInterfaceId::IsSignal() const          { return (DecodeKind(_kind)==VHDL_signal) ? 1 : 0 ; }
unsigned VhdlSubprogramId::IsFunction() const       { return (_is_procedure) ? 0 : 1 ; }
unsigned VhdlSubprogramId::IsProcedure() const      { return (_is_procedure) ? 1 : 0 ; }
unsigned VhdlInterfaceId::IsFile() const            { return (DecodeKind(_kind)==VHDL_file) ? 1 : (_type) ? _type->IsFileType() : 0 ; }

void     VhdlIdDef::SetAsProcedure() { }
void     VhdlSubprogramId::SetAsProcedure() { _is_procedure = 1 ; }

void     VhdlIdDef::SetAsGeneric() {}
void     VhdlSubprogramId::SetAsGeneric() { _is_generic = 1 ; }
unsigned VhdlSubprogramId::IsGeneric() const { return _is_generic ; }

unsigned VhdlGroupId::IsGroup() const               { return 1 ; }
unsigned VhdlGroupTemplateId::IsGroupTemplate() const { return 1 ; }

unsigned VhdlBlockId::IsBlock() const               { return 1 ; }

// Alias :
unsigned VhdlAliasId::IsLabel() const               { return (_target_id) ? _target_id->IsLabel() : 0 ; }
unsigned VhdlAliasId::IsLibrary() const             { return (_target_id) ? _target_id->IsLibrary() : 0 ; }
unsigned VhdlAliasId::IsEntity() const              { return (_target_id) ? _target_id->IsEntity() : 0 ; }
unsigned VhdlAliasId::IsArchitecture() const        { return (_target_id) ? _target_id->IsArchitecture() : 0 ; }
unsigned VhdlAliasId::IsConfiguration() const       { return (_target_id) ? _target_id->IsConfiguration() : 0 ; }
unsigned VhdlAliasId::IsPackage() const             { return (_target_id) ? _target_id->IsPackage() : 0 ; }
unsigned VhdlAliasId::IsPackageBody() const         { return (_target_id) ? _target_id->IsPackageBody() : 0 ; }
unsigned VhdlAliasId::IsDesignUnit() const          { return (_target_id) ? _target_id->IsDesignUnit() : 0 ; }
unsigned VhdlAliasId::IsSubprogram() const          { return (_target_id) ? _target_id->IsSubprogram() : 0 ; }
unsigned VhdlAliasId::IsEnumerationLiteral() const  { return (_target_id) ? _target_id->IsEnumerationLiteral() : 0 ; }
unsigned VhdlAliasId::IsComponent() const           { return (_target_id) ? _target_id->IsComponent() : 0 ; }
unsigned VhdlAliasId::IsAttribute() const           { return (_target_id) ? _target_id->IsAttribute() : 0 ; }
// unsigned VhdlAliasId::IsPredefinedOperator() const  { return (_target_id) ? _target_id->IsPredefinedOperator() : 0 ; } // issue 2004 : predefined operator is a property of the IdDef itself. Alias id is never a predefined operator.
unsigned VhdlAliasId::IsInterfaceObject(unsigned object_kind) const { return (_target_id) ? _target_id->IsInterfaceObject(object_kind) : 0 ; }
unsigned VhdlAliasId::IsGeneric() const             { return (_target_id) ? _target_id->IsGeneric() : 0 ; }
unsigned VhdlAliasId::IsPort() const                { return (_target_id) ? _target_id->IsPort() : 0 ; }
unsigned VhdlAliasId::IsSubprogramFormal() const    { return (_target_id) ? _target_id->IsSubprogramFormal() : 0 ; }
unsigned VhdlAliasId::IsInput() const               { return (_target_id) ? _target_id->IsInput() : 0 ; }
unsigned VhdlAliasId::IsOutput() const              { return (_target_id) ? _target_id->IsOutput() : 0 ; }
unsigned VhdlAliasId::IsInout() const               { return (_target_id) ? _target_id->IsInout() : 0 ; }
unsigned VhdlAliasId::IsLinkage() const             { return (_target_id) ? _target_id->IsLinkage() : 0 ; }
unsigned VhdlAliasId::IsBuffer() const              { return (_target_id) ? _target_id->IsBuffer() : 0 ; }
unsigned VhdlAliasId::IsRecordElement() const       { return (_target_id) ? _target_id->IsRecordElement() : 0 ; }
unsigned VhdlAliasId::IsConstant() const            { return (_target_id) ? _target_id->IsConstant() : ((DecodeKind(_kind)==VHDL_constant) ? 1: 0) ; }
unsigned VhdlAliasId::IsVariable() const            { return (_target_id) ? _target_id->IsVariable() : ((DecodeKind(_kind)==VHDL_variable) ? 1: 0) ; }
unsigned VhdlAliasId::IsSharedVariable() const      { return (_target_id) ? _target_id->IsSharedVariable() : 0 ; }
unsigned VhdlAliasId::IsSignal() const              { return (_target_id) ? _target_id->IsSignal() : ((DecodeKind(_kind)==VHDL_signal) ? 1: 0) ; }
unsigned VhdlAliasId::IsFile() const                { return (_target_id) ? _target_id->IsFile() : 0 ; }
unsigned VhdlAliasId::IsBlock() const               { return (_target_id) ? _target_id->IsBlock() : 0 ; }
unsigned VhdlAliasId::IsGroup() const               { return (_target_id) ? _target_id->IsGroup() : 0 ; }
unsigned VhdlAliasId::IsGroupTemplate() const       { return (_target_id) ? _target_id->IsGroupTemplate() : 0 ; }

// Mode (direction) of an interface identifier
unsigned VhdlIdDef::Mode() const                    { return 0 ; }
unsigned VhdlAliasId::Mode() const                  { return (_target_id) ? _target_id->Mode() : 0 ; }
unsigned VhdlInterfaceId::Mode() const              { return DecodeMode(_mode) ; }

void     VhdlIdDef::SetMode(unsigned /*mode*/)      { }
void     VhdlInterfaceId::SetMode(unsigned mode)    { _mode = EncodeMode(mode) ; }

// Object is an array if its type is an array type, and its not a subprogram
unsigned VhdlIdDef::IsArray() const                 { return (_type) ? _type->IsArrayType() : 0 ; }
unsigned VhdlTypeId::IsArray() const                { return IsArrayType() ; }
unsigned VhdlSubprogramId::IsArray() const          { return 0 ; } // CHECK ME : might need to check return type

// Object is a record if its type is a record type, and its not a subprogram
unsigned VhdlIdDef::IsRecord() const                { return (_type) ? _type->IsRecordType() : 0 ; }
unsigned VhdlTypeId::IsRecord() const               { return IsRecordType() ; }
unsigned VhdlSubprogramId::IsRecord() const         { return 0 ; } // CHECK ME : might need to check return type

// Object is a protected if its type is a protected type, and its not a subprogram
unsigned VhdlIdDef::IsProtected() const                { return (_type) ? _type->IsProtectedType() : 0 ; }
unsigned VhdlTypeId::IsProtected() const               { return IsProtectedType() ; }
unsigned VhdlSubprogramId::IsProtected() const         { return 0 ; }

unsigned VhdlIdDef::IsProtectedBody() const            { return (_type) ? _type->IsProtectedTypeBody() : 0 ; }
unsigned VhdlTypeId::IsProtectedBody() const           { return IsProtectedTypeBody() ; }
unsigned VhdlSubprogramId::IsProtectedBody() const     { return 0 ; }

// Alias testing
unsigned VhdlIdDef::IsAlias() const                 { return 0 ; }
unsigned VhdlAliasId::IsAlias() const               { return 1 ; }

// Type Testing
// Default (non-types) :
unsigned VhdlIdDef::IsType() const                  { return 0 ; }
unsigned VhdlIdDef::IsIncompleteType() const        { return 0 ; }
unsigned VhdlIdDef::IsDefinedIncompleteType() const { return 0 ; }
unsigned VhdlIdDef::IsIntegerType() const           { return 0 ; }
unsigned VhdlIdDef::IsRealType() const              { return 0 ; }
unsigned VhdlIdDef::IsPositiveType() const          { return 0 ; }
unsigned VhdlIdDef::IsGenericType() const           { return 0 ; }
unsigned VhdlIdDef::IsNaturalType() const           { return 0 ; }
unsigned VhdlIdDef::IsStringType() const            { return 0 ; }
unsigned VhdlIdDef::IsEnumerationType() const       { return 0 ; }
unsigned VhdlIdDef::IsPhysicalType() const          { return 0 ; }
unsigned VhdlIdDef::IsStdTimeType() const           { return 0 ; }
unsigned VhdlIdDef::IsArrayType() const             { return 0 ; }
unsigned VhdlIdDef::IsUnconstrainedArrayType() const { return 0 ; }
unsigned VhdlIdDef::IsConstrainedArrayType() const  { return 0 ; }
unsigned VhdlIdDef::IsRecordType() const            { return 0 ; }
unsigned VhdlIdDef::IsProtectedType() const         { return 0 ; }
unsigned VhdlIdDef::IsProtectedTypeBody() const     { return 0 ; }
unsigned VhdlIdDef::IsAccessType() const            { return 0 ; }
unsigned VhdlIdDef::ContainsAccessType() const      { return 0 ; }
unsigned VhdlIdDef::IsFileType() const              { return 0 ; }
unsigned VhdlIdDef::IsSubtype() const               { return 0 ; }
unsigned VhdlIdDef::IsScalarType() const            { return 0 ; }
unsigned VhdlIdDef::IsNumericType() const           { return 0 ; }
unsigned VhdlIdDef::IsDiscreteType() const          { return 0 ; }
unsigned VhdlIdDef::IsStdULogicType() const         { return 0 ; }
unsigned VhdlIdDef::IsStdBitType() const            { return 0 ; }
unsigned VhdlIdDef::IsStdBooleanType() const        { return 0 ; }
unsigned VhdlIdDef::IsStdCharacterType() const      { return 0 ; }

unsigned VhdlIdDef::IsUniversalInteger() const      { return 0 ; }
unsigned VhdlIdDef::IsUniversalReal() const         { return 0 ; }
unsigned VhdlIdDef::IsAnonymousType() const         { return 0 ; }

// Type identifiers :
unsigned VhdlTypeId::IsType() const                 { return 1 ; }

unsigned VhdlTypeId::IsIncompleteType() const
{
    if (_type && IsGenericType()) return _type->IsIncompleteType() ;
    return ((_type_enum==UNKNOWN_TYPE) || (_type_enum==INCOMPLETE_TYPE)) ? 1 : 0 ;
}
unsigned VhdlTypeId::IsDefinedIncompleteType() const
{
    if (_type && IsGenericType()) return _type->IsDefinedIncompleteType() ;
    return (_type_enum==INCOMPLETE_TYPE) ? 1 : 0 ;
}
void VhdlTypeId::SetDefinedIncompleteType()
{
    if (_type && IsGenericType()) {
        _type->SetDefinedIncompleteType() ;
    } else {
        _type_enum = INCOMPLETE_TYPE ;
    }
}

unsigned VhdlTypeId::IsIntegerType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsIntegerType() ; // access type : check its target type
    return (_type_enum==INTEGER_TYPE) ? 1 : 0 ;
}
unsigned VhdlTypeId::IsPositiveType() const
{
    if (_type && IsGenericType()) return _type->IsPositiveType() ;
    if (_type_enum == UNKNOWN_TYPE && _name && Strings::compare(_name,"positive")) return 1 ; // VIPER 5401
    return 0 ;
}
unsigned VhdlTypeId::IsGenericType() const
{
    return 0 ;
}
unsigned VhdlGenericTypeId::IsGenericType() const
{
    return 1 ;
}
unsigned VhdlTypeId::IsGeneric() const { return IsGenericType() ; }
unsigned VhdlTypeId::IsNaturalType() const
{
    if (_type && IsGenericType()) return _type->IsNaturalType() ;
    if (_type_enum == UNKNOWN_TYPE && _name && Strings::compare(_name,"natural")) return 1 ; // VIPER 5401
    return 0 ;
}
unsigned VhdlTypeId::IsStringType() const
{
    if (_type && IsGenericType()) return _type->IsStringType() ;
    if (_type_enum == UNCONSTRAINED_ARRAY_TYPE && _name && Strings::compare(_name,"string")) return 1 ; // VIPER 5401
    return 0 ;
}

unsigned VhdlTypeId::IsRealType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsRealType() ; // access type : check its target type
    return (_type_enum==REAL_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsEnumerationType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsEnumerationType() ; // access type : check its target type
    return (_type_enum==ENUMERATION_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsStdTimeType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsStdTimeType() ; // access type : check its target type
    return (_type_enum==PHYSICAL_STD_TIME_TYPE) ? 1 : 0 ;
}
unsigned VhdlTypeId::IsStdULogicType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsStdULogicType() ; // access type : check its target type
    //Fix me : need to replace it with static pointer check
    if (_type_enum == ENUMERATION_TYPE && _name && Strings::compare(_name,"std_ulogic")) return 1 ; // VIPER 5401
    //return (this == StdType("std_ulogic")) ? 1 : 0 ;
    return 0 ;
}
unsigned VhdlTypeId::IsStdBitType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsStdBitType() ; // access type : check its target type
    //Fix me : need to replace it with static pointer check
    if (_type_enum == ENUMERATION_TYPE && _name && Strings::compare(_name,"bit")) return 1 ; // VIPER 7501
    return 0 ;
}
unsigned VhdlTypeId::IsStdBooleanType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsStdBitType() ; // access type : check its target type
    //Fix me : need to replace it with static pointer check
    if (_type_enum == ENUMERATION_TYPE && _name && Strings::compare(_name,"boolean")) return 1 ; // VIPER 7501
    return 0 ;
}
unsigned VhdlTypeId::IsStdCharacterType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsStdCharacterType() ; // access type : check its target type
    //Fix me : need to replace it with static pointer check
    if (_type_enum == ENUMERATION_TYPE && _name && Strings::compare(_name,"character")) return 1 ; // VIPER 7501
    return 0 ;
}
unsigned VhdlTypeId::IsPhysicalType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsPhysicalType() ; // access type : check its target type
    return (_type_enum==PHYSICAL_TYPE || _type_enum==PHYSICAL_STD_TIME_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsArrayType() const
{
    // VIPER #7503 : If this is generic type, check its _type field: Here _type_enum==GENERIC_TYPE means that _type is a subtype
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || _type_enum==GENERIC_TYPE)) return _type->IsArrayType() ; // access type : check its target type
    return (_type_enum==CONSTRAINED_ARRAY_TYPE || _type_enum==UNCONSTRAINED_ARRAY_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsUnconstrainedArrayType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || _type_enum==GENERIC_TYPE)) return _type->IsUnconstrainedArrayType() ; // access type : check its target type
    return (_type_enum==UNCONSTRAINED_ARRAY_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsConstrainedArrayType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || _type_enum==GENERIC_TYPE)) return _type->IsConstrainedArrayType() ; // access type : check its target type
    return (_type_enum==CONSTRAINED_ARRAY_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsRecordType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsRecordType() ; // access type : check its target type
    return (_type_enum==RECORD_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsProtectedType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsProtectedType() ; // access type : check its target type
    return (_type_enum==PROTECTED_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsProtectedTypeBody() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsProtectedTypeBody() ; // access type : check its target type
    return (_type_enum==PROTECTED_TYPE_BODY) ? 1 : 0 ;
}

unsigned VhdlTypeId::ContainsAccessType() const
{
    if (_type_enum==ACCESS_TYPE) return 1 ;

    VhdlIdDef *element_type = ElementType() ;
    if (element_type) return element_type->ContainsAccessType() ;

    // Viper #6499
    unsigned num_ele = NumOfElements() ;
    unsigned i ;
    for (i = 0 ; i < num_ele ; ++i) {
        VhdlIdDef *ele_id = GetElementAt(i) ;
        VhdlIdDef *ele_type = ele_id ? ele_id->Type() : 0 ;
        if (ele_type && ele_type->ContainsAccessType()) return 1 ;
    }

    return 0 ;
}

unsigned VhdlTypeId::IsAccessType() const
{
    if (_type && IsGenericType()) return _type->IsAccessType() ;
    return (_type_enum==ACCESS_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsFileType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsFileType() ; // access type : check its target type
    return (_type_enum==FILE_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsScalarType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsScalarType() ; // access type : check its target type
    // Scalar type is integer,real,physical and enumeration type (LRM 3.1)
    return (_type_enum==INTEGER_TYPE || _type_enum==REAL_TYPE ||
            _type_enum==PHYSICAL_TYPE || _type_enum==ENUMERATION_TYPE ||
            _type_enum==PHYSICAL_STD_TIME_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsNumericType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsNumericType() ; // access type : check its target type
    // numeric types are integer,real and physical (LRM 3.1)
    return (_type_enum==INTEGER_TYPE || _type_enum==REAL_TYPE ||
            _type_enum==PHYSICAL_TYPE || _type_enum==PHYSICAL_STD_TIME_TYPE) ? 1 : 0 ;
}

unsigned VhdlTypeId::IsDiscreteType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IsDiscreteType() ; // access type : check its target type
    // Discrete type is enumeration type and integer type (LRM 3.1)
    return (_type_enum==INTEGER_TYPE || _type_enum==ENUMERATION_TYPE) ? 1 : 0 ;
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void  VhdlTypeId::SetVerilogPkgName(const char *pkg_name)
{
    if (!pkg_name) return ;
    _verilog_pkg_name = Strings::save(pkg_name) ;
}
void  VhdlTypeId::SetVerilogPkgLibName(const char *pkg_lib_name)
{
    if (!pkg_lib_name) return ;
    _verilog_pkg_lib_name = Strings::save(pkg_lib_name) ;
}
#endif

unsigned VhdlUniversalInteger::IsUniversalInteger() const   { return 1 ; }
unsigned VhdlUniversalReal::IsUniversalReal() const         { return 1 ; }
unsigned VhdlAnonymousType::IsAnonymousType() const         { return 1 ; }

// Check subtype : inherits properties from its type :
unsigned VhdlSubtypeId::IsType() const                      { return 1 ; }
unsigned VhdlSubtypeId::IsIntegerType() const               { return (_type) ? _type->IsIntegerType() : 0 ; }
unsigned VhdlSubtypeId::IsRealType() const                  { return (_type) ? _type->IsRealType() : 0 ; }
unsigned VhdlSubtypeId::IsPositiveType() const              { return (_type) ? _type->IsPositiveType() : 0 ; }
unsigned VhdlSubtypeId::IsNaturalType() const               { return (_type) ? _type->IsNaturalType() : 0 ; }
unsigned VhdlSubtypeId::IsStringType() const                { return (_type) ? _type->IsStringType() : 0 ; }
unsigned VhdlSubtypeId::IsEnumerationType() const           { return (_type) ? _type->IsEnumerationType() : 0 ; }
unsigned VhdlSubtypeId::IsPhysicalType() const              { return (_type) ? _type->IsPhysicalType() : 0 ; }
unsigned VhdlSubtypeId::IsStdTimeType() const               { return (_type) ? _type->IsStdTimeType() : 0 ; }
unsigned VhdlSubtypeId::IsArrayType() const                 { return (_type) ? _type->IsArrayType() : 0 ; }
unsigned VhdlSubtypeId::IsUnconstrainedArrayType() const    { return _is_unconstrained ; } // subtype gets this information from its type.
unsigned VhdlSubtypeId::IsConstrainedArrayType() const      { return (_is_unconstrained) ? 0 : ((_type) ? _type->IsArrayType() : 0) ; } // returns 0 if it is subtype of unconstrained array, otherwise check if its type is an array
unsigned VhdlSubtypeId::IsRecordType() const                { return (_type) ? _type->IsRecordType() : 0 ; }
unsigned VhdlSubtypeId::IsProtectedType() const             { return (_type) ? _type->IsProtectedType() : 0 ; }
unsigned VhdlSubtypeId::IsProtectedTypeBody() const         { return (_type) ? _type->IsProtectedType() : 0 ; }
unsigned VhdlSubtypeId::IsAccessType() const                { return (_type) ? _type->IsAccessType() : 0 ; }
unsigned VhdlSubtypeId::ContainsAccessType() const          { return (_type) ? _type->ContainsAccessType() : 0 ; }
unsigned VhdlSubtypeId::IsFileType() const                  { return (_type) ? _type->IsFileType() : 0 ; }
unsigned VhdlSubtypeId::IsSubtype() const                   { return 1 ; }
unsigned VhdlSubtypeId::IsScalarType() const                { return (_type) ? _type->IsScalarType() : 0 ; }
unsigned VhdlSubtypeId::IsNumericType() const               { return (_type) ? _type->IsNumericType() : 0 ; }
unsigned VhdlSubtypeId::IsDiscreteType() const              { return (_type) ? _type->IsDiscreteType() : 0 ; }
unsigned VhdlSubtypeId::IsStdULogicType() const             { return (_type) ? _type->IsStdULogicType(): 0 ; }
unsigned VhdlSubtypeId::IsStdBitType() const                { return (_type) ? _type->IsStdBitType(): 0 ; }
unsigned VhdlSubtypeId::IsStdBooleanType() const            { return (_type) ? _type->IsStdBooleanType(): 0 ; }
unsigned VhdlSubtypeId::IsStdCharacterType() const          { return (_type) ? _type->IsStdCharacterType(): 0 ; }

// Alias too :
unsigned VhdlAliasId::IsType() const                        { return (_target_id) ? _target_id->IsType() : 0 ; }
unsigned VhdlAliasId::IsIncompleteType() const              { return (_target_id) ? _target_id->IsIncompleteType() : 0 ; }
unsigned VhdlAliasId::IsIntegerType() const                 { return (_target_id) ? _target_id->IsIntegerType() : 0 ; }
unsigned VhdlAliasId::IsRealType() const                    { return (_target_id) ? _target_id->IsRealType() : 0 ; }
unsigned VhdlAliasId::IsPositiveType() const                { return (_target_id) ? _target_id->IsPositiveType() : 0 ; }
unsigned VhdlAliasId::IsNaturalType() const                 { return (_target_id) ? _target_id->IsNaturalType() : 0 ; }
unsigned VhdlAliasId::IsStringType() const                  { return (_target_id) ? _target_id->IsStringType() : 0 ; }
unsigned VhdlAliasId::IsEnumerationType() const             { return (_target_id) ? _target_id->IsEnumerationType() : 0 ; }
unsigned VhdlAliasId::IsPhysicalType() const                { return (_target_id) ? _target_id->IsPhysicalType() : 0 ; }
unsigned VhdlAliasId::IsStdTimeType() const                 { return (_target_id) ? _target_id->IsStdTimeType() : 0 ; }
unsigned VhdlAliasId::IsArrayType() const                   { return (_target_id) ? _target_id->IsArrayType() : 0 ; }
unsigned VhdlAliasId::IsUnconstrainedArrayType() const      { return (_target_id) ? _target_id->IsUnconstrainedArrayType() : 0 ; }
unsigned VhdlAliasId::IsConstrainedArrayType() const        { return (_target_id) ? _target_id->IsConstrainedArrayType() : 0 ; }
unsigned VhdlAliasId::IsRecordType() const                  { return (_target_id) ? _target_id->IsRecordType() : 0 ; }
unsigned VhdlAliasId::IsProtectedType() const               { return (_target_id) ? _target_id->IsProtectedType() : 0 ; }
unsigned VhdlAliasId::IsProtectedTypeBody() const           { return (_target_id) ? _target_id->IsProtectedTypeBody() : 0 ; }
unsigned VhdlAliasId::IsAccessType() const                  { return (_target_id) ? _target_id->IsAccessType() : 0 ; }
unsigned VhdlAliasId::ContainsAccessType() const            { return (_target_id) ? _target_id->ContainsAccessType() : 0 ; }
unsigned VhdlAliasId::IsFileType() const                    { return (_target_id) ? _target_id->IsFileType() : 0 ; }
unsigned VhdlAliasId::IsSubtype() const                     { return (_target_id) ? _target_id->IsSubtype() : 0 ; }
unsigned VhdlAliasId::IsScalarType() const                  { return (_target_id) ? _target_id->IsScalarType() : 0 ; }
unsigned VhdlAliasId::IsNumericType() const                 { return (_target_id) ? _target_id->IsNumericType() : 0 ; }
unsigned VhdlAliasId::IsDiscreteType() const                { return (_target_id) ? _target_id->IsDiscreteType() : 0 ; }
unsigned VhdlAliasId::IsStdULogicType() const               { return (_target_id) ? _target_id->IsStdULogicType(): 0 ; }
unsigned VhdlAliasId::IsStdBitType() const                  { return (_target_id) ? _target_id->IsStdBitType(): 0 ; }
unsigned VhdlAliasId::IsStdBooleanType() const              { return (_target_id) ? _target_id->IsStdBooleanType(): 0 ; }
unsigned VhdlAliasId::IsStdCharacterType() const            { return (_target_id) ? _target_id->IsStdCharacterType(): 0 ; }

/*************************************************************/
/*************************** Declare Types ************************/
/*************************************************************/
void VhdlIdDef::DeclareIntegerType()                                    { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareRealType()                                       { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclarePhysicalType(Map * /*physical_units*/)           { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareEnumerationType(Map * /*enumeration_literals*/)  { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareConstrainedArrayType(Map * /*index_constraint*/, VhdlIdDef * /*element_type*/, int /*locally_static*/, int /*globally_static*/)   { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareUnconstrainedArrayType(Map * /*index_constraint*/, VhdlIdDef * /*element_type*/, int /*locally_static*/, int /*globally_static*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareRecordType(Map * /*element_decl_list*/)          { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareProtectedType(VhdlProtectedTypeDef * /*type_def*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareProtectedTypeBody(VhdlProtectedTypeDefBody * /*type_def*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareFileType(VhdlIdDef * /*subtype_id*/)             { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareAccessType(VhdlIdDef * /*subtype_id*/, int /*locally_static*/, int /*globally_static*/)           { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareIncompleteType(VhdlIdDef * /*subtype_id*/)       { VERIFIC_ASSERT(0) ; }

void VhdlTypeId::DeclareIntegerType()
{
    // Declare this as integer type, and create the operators
    _type_enum=INTEGER_TYPE ;
    DeclareOperators() ;
    _is_locally_static_type = 1 ;
    _is_globally_static_type = 1 ;
}

void VhdlTypeId::DeclareRealType()
{
    // Declare this as real type, and create the operators
    _type_enum= REAL_TYPE ;
    DeclareOperators() ;
    _is_locally_static_type = 1 ;
    _is_globally_static_type = 1 ;
}

void VhdlTypeId::DeclarePhysicalType(Map *physical_units)
{
    VERIFIC_ASSERT(!_list) ;

    // Declare this as physical type, and create the operators
    _type_enum = (StdType("time") == this) ? PHYSICAL_STD_TIME_TYPE : PHYSICAL_TYPE ;
    _list = physical_units ;
    DeclareOperators() ;
    _is_locally_static_type = 1 ;
    _is_globally_static_type = 1 ;
}

void VhdlTypeId::DeclareEnumerationType(Map *enumeration_literals)
{
    VERIFIC_ASSERT(!_list) ;

    // Declare this as enumeration type, and create the operators
    _type_enum=ENUMERATION_TYPE ;
    _list = enumeration_literals ;
    DeclareOperators() ;
    _is_locally_static_type = 1 ;
    _is_globally_static_type = 1 ;
}

void VhdlTypeId::DeclareConstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static)
{
    VERIFIC_ASSERT(!_list) ;

    // Declare this as array type, and create the operators
    _type_enum=CONSTRAINED_ARRAY_TYPE ;
    _list = index_constraint ;
    _type = element_type ;
    DeclareOperators() ;

    unsigned all_locally_static_types = (locally_static < 0) ? (element_type ? element_type->IsLocallyStaticType() : 1) : (unsigned)locally_static ; // Viper #5734
    unsigned all_globally_static_types = (globally_static < 0) ? (element_type ? element_type->IsLocallyStaticType() : 1) : (unsigned)globally_static ; // Viper #5734

    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(index_constraint, mi, 0, &id) {
        if (!id) continue ;
        if (!id->IsGloballyStaticType()) all_globally_static_types = 0 ;
        if (!id->IsLocallyStaticType()) all_locally_static_types = 0 ;
    }

    if (all_locally_static_types) {
        SetLocallyStaticType() ;
    } else if (all_globally_static_types) {
        SetGloballyStaticType() ;
    }
}

void VhdlTypeId::DeclareUnconstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static)
{
    VERIFIC_ASSERT(!_list) ;

    // Declare this as array type, and create the operators
    _type_enum=UNCONSTRAINED_ARRAY_TYPE ;
    _list = index_constraint ;
    _type = element_type ;
    DeclareOperators() ;

    // unconstr array cannot be locally static
    unsigned all_globally_static_types = (globally_static < 0) ? (element_type ? element_type->IsLocallyStaticType() : 1) : (unsigned)globally_static ; // Viper #5734

    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(index_constraint, mi, 0, &id) {
        if (!id) continue ;
        if (!id->IsGloballyStaticType()) all_globally_static_types = 0 ;
    }

    if (locally_static > 0) SetLocallyStaticType() ;
    if (all_globally_static_types) SetGloballyStaticType() ;
}

void VhdlProtectedTypeId::DeclareProtectedType(VhdlProtectedTypeDef *type_def)
{
    VERIFIC_ASSERT(!_type_def) ;

    _type_enum = PROTECTED_TYPE ;
    _type_def = type_def ;

    // update the _list Map
    VhdlScope *def_scope = type_def ? type_def->LocalScope() : 0 ;
    Map *list = def_scope ? def_scope->DeclArea() : 0 ;
    _list = list ? new Map(*list) : new Map(STRING_HASH) ;
}

void VhdlProtectedTypeBodyId::DeclareProtectedTypeBody(VhdlProtectedTypeDefBody *type_def_body)
{
    VERIFIC_ASSERT(!_type_def_body) ;

    _type_enum = PROTECTED_TYPE_BODY ;
    _type_def_body = type_def_body ;

    // VIPER #7557: Donot add identifier of body in type id of header
    /*
    VhdlScope *header_scope = _present_scope ;
    if (!header_scope) {
        VhdlScope *body_scope = type_def_body ? type_def_body->LocalScope() : 0 ;
        header_scope = body_scope ? body_scope->Upper() : 0 ;
    }
    VhdlIdDef *header_id = header_scope ? header_scope->FindSingleObjectLocal(Name()) : 0 ;
    Map *list = header_id ? header_id->GetTypeDefList() : 0 ;
    if (list) {
        VhdlScope *scope = type_def_body ? type_def_body->LocalScope() : 0 ;
        Map *scope_list = scope ? scope->DeclArea() : 0 ;

        MapIter mi ;
        VhdlIdDef *id ;
        char *id_name ;
        FOREACH_MAP_ITEM(scope_list, mi, &id_name, &id) {
            if (id_name && id) (void) list->Insert(id_name, id) ;
        }
    }*/
}

void VhdlTypeId::DeclareRecordType(Map *element_decl_list)
{
    VERIFIC_ASSERT(!_list) ;

    // Declare this as record type, and create the operators
    _type_enum=RECORD_TYPE ;
    _list = element_decl_list ;
    DeclareOperators() ;

    unsigned all_locally_static_types = 1 ;
    unsigned all_globally_static_types = 1 ;

    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(element_decl_list, mi, 0, &id) {
        if (!id->IsGloballyStaticType()) all_globally_static_types = 0 ;
        if (!id->IsLocallyStaticType()) all_locally_static_types = 0 ;
    }

    if (all_locally_static_types) {
        SetLocallyStaticType() ;
    } else if (all_globally_static_types) {
        SetGloballyStaticType() ;
    }
}

void VhdlTypeId::DeclareFileType(VhdlIdDef *subtype_id)
{
    // Declare this as file type, and create the operators
    _type_enum=FILE_TYPE ;
    _type = subtype_id ;
    DeclareOperators() ;

    SetLocallyStaticType() ; // also sets globally static type
}

void VhdlTypeId::DeclareAccessType(VhdlIdDef *subtype_id, int locally_static, int globally_static)
{
    // Declare this as access type, and create the operators
    _type_enum=ACCESS_TYPE ;
    _type = subtype_id ;
    DeclareOperators() ;

    // Viper #7758: Improve the static flag settings
    // SetLocallyStaticType() ; // also sets globally static type
    if (locally_static) {
        SetLocallyStaticType() ; // also sets globally static type
    } else if (globally_static) {
        SetGloballyStaticType() ;
    }
}

void VhdlTypeId::DeclareIncompleteType(VhdlIdDef *subtype_id)
{
    // Set the definition of the incomplete type
    _type_enum = UNKNOWN_TYPE ;
    _type = subtype_id ;
}

/*************************************************************/
/******************** Declare Objects ************************/
/*************************************************************/

void VhdlIdDef::DeclareEnumerationId(VhdlIdDef * /*type*/, unsigned /*pos*/)    { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclarePhysicalUnitId(VhdlIdDef * /*type*/, verific_uint64 /*pos*/)   { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareElementId(VhdlIdDef * /*type*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareRecordElement(unsigned /*pos*/)                          { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareInterfaceId(VhdlIdDef * /*type*/, unsigned /*kind*/, unsigned /*mode*/, unsigned /*signal_kind*/, unsigned /*has_init_assign*/, unsigned /*is_unconstrained*/, VhdlIdDef * /*resolution_function*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareSubtype(VhdlIdDef * /*type*/, VhdlIdDef * /*resolution_function*/, unsigned /*is_unconstrained*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareConstant(VhdlIdDef * /*type*/, unsigned /*locally_static*/, unsigned /*loop_iter*/, unsigned /*globally_static*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareVariable(VhdlIdDef * /*type*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/, unsigned /*is_shared*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareSignal(VhdlIdDef * /*type*/, VhdlIdDef * /*resolution_function*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/, unsigned /*signal_kind*/) { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareFile(VhdlIdDef * /*type*/)                               { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareAlias(VhdlIdDef * /*type*/, VhdlName * /*target_name*/, unsigned /*locally_static_type*/, unsigned /*globally_static_type*/, unsigned /*kind*/)  { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareAttribute(VhdlIdDef * /*type*/)                          { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareFunction(VhdlIdDef * /*return_type*/, Array * /*args*/, Array * /*generics*/)  { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareProcedure(Array * /*args*/, Array * /*generics*/)        { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareEntityId(Array * /*generics*/, Array * /*ports*/)        { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareComponentId(Array * /*generics*/, Array * /*ports*/)     { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclarePackageId(Array * /*generics*/)                          { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareBlockId(Array * /*generics*/, Array * /*ports*/)         { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareArchitectureId(VhdlIdDef * /*entity*/)                   { VERIFIC_ASSERT(0) ; }
void VhdlIdDef::DeclareConfigurationId(VhdlIdDef * /*entity*/)                  { VERIFIC_ASSERT(0) ; }

void VhdlEnumerationId::DeclareEnumerationId(VhdlIdDef *type, unsigned pos)     { _type = type ; _position = pos ; }
void VhdlPhysicalUnitId::DeclarePhysicalUnitId(VhdlIdDef *type, verific_uint64 pos)   { _type = type ; _position = pos ; }
// Record elements declared in two steps
void VhdlElementId::DeclareElementId(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type) { _type = type ; _is_locally_static_type |= locally_static_type ; _is_globally_static_type = _is_globally_static_type || locally_static_type || globally_static_type ; }
void VhdlElementId::DeclareRecordElement(unsigned pos)                          { _position = pos ; }
void VhdlSubtypeId::DeclareSubtype(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned is_unconstrained, unsigned locally_static_type, unsigned globally_static_type) { _type = type ; _resolution_function = resolution_function ; _is_unconstrained = is_unconstrained ; _is_locally_static_type |= locally_static_type ; _is_globally_static_type = _is_globally_static_type || locally_static_type || globally_static_type ; }
void VhdlConstantId::DeclareConstant(VhdlIdDef *type, unsigned locally_static, unsigned loop_iter, unsigned globally_static, unsigned locally_static_type, unsigned globally_static_type)
{
    _type = type ;
    _is_locally_static = locally_static ;
    _is_globally_static = globally_static ; // VIPER Issue #6349
    _is_seq_loop_iter = loop_iter ;
    _is_locally_static_type = locally_static_type ;
    _is_globally_static_type = locally_static_type || globally_static_type ;
}

void VhdlVariableId::DeclareVariable(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type, unsigned is_shared)
{
    _type = type ;
    _is_locally_static_type = locally_static_type ;
    _is_globally_static_type = locally_static_type || globally_static_type ;
    _is_shared = is_shared ; // shared variable
}

void VhdlSignalId::DeclareSignal(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type, unsigned signal_kind)
{
    _type = type ;
    _resolution_function = resolution_function ;
    _is_locally_static_type = locally_static_type ;
    _is_globally_static_type = locally_static_type || globally_static_type ;
    _signal_kind = EncodeSignalKind(signal_kind) ;
}

void VhdlFileId::DeclareFile(VhdlIdDef *type) { _type = type ; }

void VhdlAliasId::DeclareAlias(VhdlIdDef *type, VhdlName *target_name, unsigned locally_static_type, unsigned globally_static_type, unsigned kind)
{
    _type = type ;
    _target_name = target_name ;
    // Set the identifier (some named entity or object) inside the alias target name :
    if (_target_name) _target_id = _target_name->FindAliasTarget() ;

    // Viper #5354 : Added these fields in alias_id
    if (locally_static_type != 0 && locally_static_type != 1) locally_static_type = _target_id ? _target_id->IsLocallyStaticType() : 1 ;
    if (globally_static_type != 0 && globally_static_type != 1) globally_static_type = _target_id ? _target_id->IsGloballyStaticType() : 1 ;

    _is_locally_static_type = locally_static_type ;
    _is_globally_static_type = locally_static_type || globally_static_type ;
    _kind = EncodeKind(kind) ;
}

void VhdlAttributeId::DeclareAttribute(VhdlIdDef *type) { _type = type ; }

void VhdlSubprogramId::CheckSubprogramForProtectedType() const
{
    VhdlScope *runner = LocalScope() ;
    if (!runner) runner = _present_scope ;

    unsigned protected_scope = 0 ;
    while (runner) {
        VhdlIdDef *owner_id = runner->GetOwner() ;
        if (owner_id) {
            if (owner_id->IsProtected()) protected_scope = 1 ;
            if (protected_scope || owner_id->IsDesignUnit()) break ;
        }
        runner = runner->Upper() ;
    }

    if (!protected_scope) return ;

    // Okay.. this is a method for the protected type (defined in the header)
    // LRM P1076-2008 Section 5.6.2 : They cannot have file or access type
    // argument or return type. But the local subprograms defined inside the
    // body of the protected type (LRM 5.6.3) are frre to use file/access types
    Set type_recustion_set ;
    if (_type && _type->IsTypeContainsFile(1 /*check access type*/, type_recustion_set)) Error("protected type method cannot return file or access type") ;

    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_args, i, id) {
        if (!id) return ;
        VhdlIdDef *id_type = id->Type() ;
        type_recustion_set.Reset() ;
        if (id_type && id_type->IsTypeContainsFile(1 /*check access type*/, type_recustion_set)) id->Error("protected type method argument cannot be file or access type") ;
    }
}

void VhdlSubprogramId::DeclareFunction(VhdlIdDef *return_type, Array *args, Array *generics)
{
    _type = return_type ;
    _args = args ; // Absorb the arguments
    _is_procedure = 0 ; // Explicitly not a procedure
    _generics = generics ;

    CheckSubprogramForProtectedType() ;

    // VIPER #3166 : Check if this function is an operator
    if (_name && (_name[0] == '"') && (_name[Strings::len(_name) - 1] == '"')) {
        // Yes. This is an operator. Check if it is valid.
        if (!VhdlNode::IsValidOperator(_name)) {
            Error("%s is not a valid operator symbol", _name) ;
        } else { // VIPER #3491 : Check number of operands for overloaded operators
            // '+'/'-'/'abs' and 'not' can be unary operators : expects 1 argument
            if (Strings::compare(_name, "\"abs\"") || Strings::compare(_name, "\"not\"") || Strings::compare(_name, "\"??\"")) {
                if (!args || (args->Size() != 1)) Error("%s expects %d arguments", Name(), 1) ;
            } else if (Strings::compare(_name, "\"+\"") || Strings::compare(_name, "\"-\"") || (vhdl_file::IsVhdl2008() && (Strings::compare(_name, "\"and\"") || Strings::compare(_name, "\"or\"") || Strings::compare(_name, "\"nand\"") || Strings::compare(_name, "\"nor\"") || Strings::compare(_name, "\"xor\"") || Strings::compare(_name, "\"xnor\"")))) { // Vhdl 1076-2008 section 9.2.2 includes logical reduction operators
                if (!args || (args->Size() > 2)) Error("%s expects %d arguments", Name(), 2) ;
            } else if (!Strings::compare(_name, "\",\"")) { // Binary operators : expects 2 arguments
                if (!args || (args->Size() != 2)) Error("%s expects %d arguments", Name(), 2) ;
            }
        }
    }
}

void VhdlSubprogramId::DeclareProcedure(Array *args, Array *generics)
{
    _type = UniversalVoid() ; // Return type of a procedure
    _args = args ; // Absorb the arguments
    _is_procedure = 1 ; // Explicitly a procedure
    _generics = generics ;

    CheckSubprogramForProtectedType() ;
}

void VhdlInterfaceId::DeclareInterfaceId(VhdlIdDef *type, unsigned kind, unsigned mode, unsigned signal_kind, unsigned has_init_assign, unsigned is_unconstrained, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type)
{
    // default settings of interface identifiers depend on the sort of
    // interface list (port, generic, or parameter interface list) that this
    // identifier appears in.
    // So, we wait with default settings until SetObjectKind(), which knows
    // the interface list that this id is in.
    // Also, further interface element checks are done there too.
    _type = type ;
    _mode = EncodeMode(mode) ;
    _kind = EncodeKind(kind) ;
    _signal_kind = EncodeSignalKind(signal_kind) ;
    _resolution_function = resolution_function ;
    _has_init_assign = has_init_assign ;
    _is_unconstrained = is_unconstrained ;
    _is_locally_static_type = locally_static_type ;
    _is_globally_static_type = locally_static_type || globally_static_type ;
}

void VhdlEntityId::DeclareEntityId(Array *generics, Array *ports)
{
    VERIFIC_ASSERT(!_generics && !_ports) ;

    _generics = generics ;
    _ports = ports ;
}

void VhdlPackageId::DeclarePackageId(Array *generics)
{
    VERIFIC_ASSERT(!_generics) ;
    _generics = generics ;
}
void VhdlPackageId::SetActualId(VhdlIdDef *id, Array * /*assoc_list*/)
{
    // For interface package decl both _unit and _decl are set
    if (_decl) _decl->SetActualId(id) ;
}
VhdlIdDef *VhdlPackageId::GetActualId() const
{
    return (_decl) ? _decl->GetActualId(): 0 ;
}
VhdlIdDef *VhdlPackageId::TakeActualId()
{
    return (_decl) ? _decl->TakeActualId(): 0 ;
}
void VhdlIdDef::SetDeclaration(VhdlDeclaration * /*d*/) {}
void VhdlPackageId::SetDeclaration(VhdlDeclaration *d) { _decl = d ; }
VhdlDeclaration *VhdlIdDef::GetDeclaration() const { return 0 ; }
VhdlDeclaration *VhdlPackageId::GetDeclaration() const { return _decl ; }
unsigned VhdlPackageId::IsGeneric() const { return _decl ? 1 : 0 ; }

void VhdlComponentId::DeclareComponentId(Array *generics, Array *ports)
{
    VERIFIC_ASSERT(!_generics && !_ports) ;

    _generics = generics ;
    _ports = ports ;
}

void VhdlBlockId::DeclareBlockId(Array *generics, Array *ports)
{
    // In the block-header, this might be called twice (once for generics, once for ports)
    // So, do this accumulative.
    if (generics) {
        VERIFIC_ASSERT(!_generics) ;
        _generics = generics ;
    }
    if (ports) {
        VERIFIC_ASSERT(!_ports) ;
        _ports = ports ;
    }
}

void VhdlArchitectureId::DeclareArchitectureId(VhdlIdDef *entity)
{
    _entity = entity ;
}

void VhdlConfigurationId::DeclareConfigurationId(VhdlIdDef *entity)
{
    _entity = entity ;
}

/****************************************************************
 * Labels and configuration bindings
 ****************************************************************/

void VhdlLabelId::AddBinding(VhdlBindingIndication *binding)
{
    if (!binding) return ;

    // Set a binding from a configuration specification onto a label
    // Due to incremental bining rules, there could be multiple calls to
    // this routine.

    // If there is no previous binding, set this one.
    if (!_primary_binding) {
        _primary_binding = binding ;
        return ;
    }

    // Second time that someone wants to set a primary binding
    binding->Error("%s already has a binding", Name()) ;
}

/****************************************************************
 * Routines to get Type info out of IdDefs
 ****************************************************************/
// Get Type of an object
VhdlIdDef *VhdlIdDef::Type() const              { return _type ; }
VhdlIdDef *VhdlTypeId::Type() const             { return const_cast<VhdlTypeId*>(this) ; }
VhdlIdDef *VhdlSubprogramId::Type() const
{
    if (_type) return _type ;
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst && !IsGeneric()) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    return uninstantiated_subprog ? uninstantiated_subprog->Type(): 0 ;
}

// Get the base-type
VhdlIdDef *VhdlIdDef::BaseType() const          { return (_type) ? _type->BaseType() : 0 ; }
VhdlIdDef *VhdlTypeId::BaseType() const         //{ return const_cast<VhdlTypeId*>(this) ; }
{
    // VIPER #7504 : Use actual type when this is generic type
    if (_type && IsGenericType()) return _type->BaseType() ;
    return const_cast<VhdlTypeId*>(this) ;
}

// Get Index types
VhdlIdDef *VhdlIdDef::IndexType(unsigned dim) const
{
    // Index type of object is index type of it's type
    return (_type) ? _type->IndexType(dim) : 0 ;
}

VhdlIdDef *VhdlTypeId::IndexType(unsigned dim) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->IndexType(dim) ; // access type : check its target type
    if (!IsArrayType() || !_list) return 0 ;
    if (dim >= _list->Size()) return 0 ;
    return (VhdlIdDef*) _list->GetValueAt(dim) ;
}

// Get Element types (element of array)
VhdlIdDef *VhdlIdDef::ElementType() const
{
    return (_type) ? _type->ElementType() : 0 ;
}

VhdlIdDef *VhdlTypeId::ElementType() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->ElementType() ; // access type : check its target type
    if (!IsArrayType()) return 0 ;
    return _type ; // This is where I store element_type
}
// Get Element types (element of array)
VhdlIdDef *VhdlIdDef::ElementType(unsigned dim)
{
    return (_type) ? _type->ElementType(dim) : 0 ;
}

VhdlIdDef *VhdlTypeId::ElementType(unsigned dim)
{
    if (!dim) return this ;

    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) {
        return _type->ElementType(dim) ; // access type : check its target type
    }
    if (!IsArrayType()) return 0 ;

    if (!_type) return 0 ;

    return _type->ElementType((_list && _list->Size()) ? (dim - _list->Size()):0) ; // This is where I store element_type
}

// Get (array) dimension
unsigned VhdlIdDef::Dimension() const
{
    return (_type) ? _type->Dimension() : 0 ;
}

unsigned VhdlTypeId::Dimension() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->Dimension() ; // access type : check its target type
    if (!IsArrayType() || !_list) return 0 ;
    return _list->Size() ;
}

// Get (array) dimension, calculated all through the element type too
unsigned VhdlIdDef::DeepDimension() const
{
    return (_type) ? _type->DeepDimension() : 0 ;
}

unsigned VhdlTypeId::DeepDimension() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->DeepDimension() ; // access type : check its target type
    if (!IsArrayType() || !_list) return 0 ;

    // ElementType is stored in '_type' of an array type identifier.
    unsigned elem_dimension = (_type) ? _type->DeepDimension() : 0 ;
    // Deep Dimension is dimension of element type plus dimension of this type :
    return _list->Size() + elem_dimension ;
}

// Number of enumeration variables
unsigned VhdlIdDef::NumOfEnums() const
{
    return (_type) ? _type->NumOfEnums() : 0 ;
}

unsigned VhdlTypeId::NumOfEnums() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->NumOfEnums() ; // access type : check its target type
    if (!IsEnumerationType() || !_list) return 0 ;
    return _list->Size() ;
}

// Find a enumeration literal in an enum type. Fast.
// Find by name
VhdlIdDef *VhdlIdDef::GetEnum(const char *name) const
{
    return (_type) ? _type->GetEnum(name) : 0 ;
}

VhdlIdDef *VhdlTypeId::GetEnum(const char *name) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->GetEnum(name) ; // access type : check its target type
    if (!IsEnumerationType() || !_list) return 0 ;
    return (VhdlIdDef*) _list->GetValue(name) ;
}

// Find by position
VhdlIdDef *VhdlIdDef::GetEnumAt(unsigned pos) const
{
    return (_type) ? _type->GetEnumAt(pos) : 0 ;
}
VhdlIdDef *VhdlTypeId::GetEnumAt(unsigned pos) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->GetEnumAt(pos) ; // access type : check its target type
    if (!IsEnumerationType() || !_list) return 0 ;
    if (pos >= _list->Size()) return 0 ;
    return (VhdlIdDef*) _list->GetValueAt(pos) ;
}

// Find by encoded bit value
VhdlIdDef *VhdlIdDef::GetEnumWithBitEncoding(char b) const
{
    return (_type) ? _type->GetEnumWithBitEncoding(b) : 0 ;
}
VhdlIdDef *VhdlTypeId::GetEnumWithBitEncoding(char b) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->GetEnumWithBitEncoding(b) ; // access type : check its target type
    if (!IsEnumerationType() || !_list) return 0 ;
    MapIter mi ;
    VhdlIdDef *literal ;
    FOREACH_MAP_ITEM(_list, mi, 0, &literal) {
        if (b == literal->GetBitEncoding()) return literal ;
    }
    return 0 ;
}

// Number of record elements
unsigned VhdlIdDef::NumOfElements() const
{
    return (_type) ? _type->NumOfElements() : 0 ;
}
unsigned VhdlTypeId::NumOfElements() const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->NumOfElements() ; // access type : check its target type
    if (!IsRecordType() || !_list) return 0 ;
    return _list->Size() ;
}

// Find a record element in record type. Fast.
VhdlIdDef *VhdlIdDef::GetElement(const char *name) const
{
    return (_type) ? _type->GetElement(name) : 0 ;
}
VhdlIdDef *VhdlTypeId::GetElement(const char *name) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->GetElement(name) ; // access type : check its target type
    if (!(IsRecordType() || IsProtectedType()) || !_list) return 0 ;
    return (VhdlIdDef*) _list->GetValue(name) ;
}
VhdlIdDef *VhdlIdDef::GetElementAt(unsigned pos) const
{
    return (_type) ? _type->GetElementAt(pos) : 0 ;
}
VhdlIdDef *VhdlTypeId::GetElementAt(unsigned pos) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->GetElementAt(pos) ; // access type : check its target type
    if (!IsRecordType() || !_list) return 0 ;
    if (pos >= _list->Size()) return 0 ;
    return (VhdlIdDef*) _list->GetValueAt(pos) ;
}

// Find the position in its type of this element
verific_uint64 VhdlIdDef::Position() const                { return 0 ; } // Actually error ?
verific_uint64 VhdlIdDef::InversePosition() const         { return 0 ; } // Actually error ?
verific_uint64 VhdlEnumerationId::Position() const        { return _position ; }
verific_uint64 VhdlElementId::Position() const            { return _position ; }
verific_uint64 VhdlPhysicalUnitId::Position() const       { return _position ; }
verific_uint64 VhdlPhysicalUnitId::InversePosition() const { return _inv_position ; }

// Find a unit in a physical type. Fast.
VhdlIdDef *VhdlIdDef::GetUnit(const char *name) const
{
    return (_type) ? _type->GetElement(name) : 0 ;
}
VhdlIdDef *VhdlTypeId::GetUnit(const char *name) const
{
    if (_type && _type!=Type() && (_type_enum==ACCESS_TYPE || _type_enum==UNKNOWN_TYPE || _type_enum==INCOMPLETE_TYPE || IsGenericType())) return _type->GetUnit(name) ; // access type : check its target type
    if (!IsPhysicalType() || !_list) return 0 ;
    return (VhdlIdDef*) _list->GetValue(name) ;
}

// For access and file types
VhdlIdDef *VhdlIdDef::DesignatedType() const        { return 0 ; }
VhdlIdDef *VhdlSubtypeId::DesignatedType() const    { return (_type) ? _type->DesignatedType() : 0 ; } // subtype of an access type : designated type is from its target type
VhdlIdDef *VhdlTypeId::DesignatedType() const
{
    // Only 'file' and 'access' types have a designated type.
    // But access types could point to unknown type that can point to the real designated type.
    // So just recur into '_type' until we hit the base designated type.
    if (_type && _type->IsIncompleteType()) return _type->DesignatedType() ;
    if (!IsAccessType() && !IsFileType() && !IsIncompleteType()) return 0 ;
    return _type ; // return designated type if there. Otherwise 0.
}

// For signals and subtypes
VhdlIdDef *VhdlIdDef::ResolutionFunction() const        { return 0 ; }
VhdlIdDef *VhdlSignalId::ResolutionFunction() const     { return _resolution_function ; }
VhdlIdDef *VhdlInterfaceId::ResolutionFunction() const  { return _resolution_function ; }
VhdlIdDef *VhdlSubtypeId::ResolutionFunction() const    { return _resolution_function ; }
VhdlIdDef *VhdlTypeId::ResolutionFunction() const
{
    // VIPER 3655 : need to get resolution function from a type.
    // A regular type cannot have a resolution function, but its element type could.
    // Decent there :
    if (ElementType()) return ElementType()->ResolutionFunction() ;
    return 0 ;
}

/****************************************************************
 * Compare two id's by type. Or compare types.
 ****************************************************************/

// Type Matching an id to another one
unsigned VhdlIdDef::TypeMatch(const VhdlIdDef * /*id*/) const
{
    return 0 ;
}
unsigned VhdlSubtypeId::TypeMatch(const VhdlIdDef *id) const
{
    // sub-types are compatible only if they have the same base-type
    if (id == this) return 1 ;
    return (id) ? id->TypeMatch(_type) : 0 ;
}
unsigned VhdlTypeId::TypeMatch(const VhdlIdDef *id) const
{
    if (id == this) return 1 ; // Happens most often.

    if (!id) return 0 ;
    unsigned has_actual_type = 0 ;

    VhdlIdDef const *id1 = this ;
    if (IsIncompleteType()) id1 = DesignatedType() ;
    if (IsGenericType() && ActualType() && id1) {
        id1 = id1->ActualType() ;
        has_actual_type = 1 ;
    }
    VhdlIdDef const *id2 = id->IsIncompleteType() ? id->DesignatedType() : id ;
    if (id2 && id2->IsGenericType() && id2->ActualType()) {
        id2 = id2->ActualType() ;
        has_actual_type = 1 ;
    }
    if (!id1 || !id2) return 0 ;

    // Viper 7519: For generic type call type match with the actual type
    if (has_actual_type) return (id1->TypeMatch(id2)) ;

    if (id1 == id2) return 1 ;

    // types are compatible only if they are the same at their base type
    return (id2->BaseType() == id1->Type()) ? 1 : 0 ;
}
unsigned VhdlAliasId::TypeMatch(const VhdlIdDef *id) const
{
    // use the target alias id to compare types :
    if (_target_id) return _target_id->TypeMatch(id) ;
    return 0 ; // no a type-alias.
}
//Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
unsigned VhdlSubtypeId::TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const
{
    // sub-types are compatible only if they have the same base-type
    if (id == this) return 1 ;
    return (id && _type) ? _type->TypeMatchDual(id, generic_type) : 0 ;
}
unsigned VhdlIdDef::TypeMatchDual(const VhdlIdDef * /*id*/, unsigned /*generic_type*/) const
{
    return 0 ;
}
unsigned VhdlTypeId::TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const
{
    if (id == this) return 1 ; // Happens most often.

    if (!id) return 0 ;

    VhdlIdDef const *id1 = this ;
    if (IsIncompleteType()) id1 = DesignatedType() ;
    VhdlIdDef const *id2 = id->IsIncompleteType() ? id->DesignatedType() : id ;

    if (!id1 || !id2) return 0 ;

    if (id1 == id2) return 1 ;

    // types are compatible only if they are the same at their base type
    if (id2->BaseType() == id1->Type()) return 1 ;

    if (id1->IsArrayType() && id2->IsArrayType()) {
        // Dimension should be the same
        if (id1->Dimension() != id2->Dimension()) return 0 ;
        // Element types are the same
        if (!id1->ElementType()->TypeMatchDual(id2->ElementType(), generic_type)) return 0 ;
        return 1 ;
    }

    if (id1->IsArrayType() && !id2->IsArrayType()) {
        // Viper 5857: For vhdl generics if it is a Array type then
        // at the verilog side we match integer, positive, natural
        if (generic_type) {
            if (id2->IsIntegerType()) return 1 ;
            if (id2->IsPositiveType()) return 1 ;
            if (id2->IsNaturalType()) return 1 ;
            if (id2->IsStdTimeType()) return 1 ; // Viper 6495: support for time type
            if (id2->IsEnumerationType()) return 1 ; // Viper 6649: enum vs array type matching
            return (id1->ElementType()->TypeMatchDual(id2, generic_type)) ; // Viper 6649 allow vector vs scalar
        }
        return 0 ;
    }

    if (!id1->IsArrayType() && id2->IsArrayType()) {
        // Viper 5857: For vhdl generics if it is a Array type then
        // at the verilog side we match integer, positive, natural
        if (generic_type) {
            if (id1->IsIntegerType()) return 1 ;
            if (id1->IsPositiveType()) return 1 ;
            if (id1->IsNaturalType()) return 1 ;
            if (id1->IsStdTimeType()) return 1 ; // Viper 6495: support for time type
            if (id1->IsEnumerationType()) return 1 ; // Viper 6649: enum vs array type matching
            return (id1->TypeMatchDual(id2->ElementType(), generic_type)) ; // Viper 6649 allow vector vs scalar
        }
        return 0 ;
    }

    if ((id2->IsStdBitType() || id2->IsStdBooleanType())) {
        // Viper 8481: For vhdl generics if it is a Boolean or Bit type then
        // at the verilog side we match integer, positive, natural
        if (generic_type) {
            if (id1->IsIntegerType()) return 1 ;
            if (id1->IsPositiveType()) return 1 ;
            if (id1->IsNaturalType()) return 1 ;
            if (id1->IsStdTimeType()) return 1 ; // Viper 6495: support for time type
            if (id1->IsEnumerationType()) return 1 ; // Viper 6649: enum vs array type matching
        }
    }

    if ((id1->IsStdBitType() || id1->IsStdBooleanType())) {
        // Viper 8481: For vhdl generics if it is a Boolean or Bit type then
        // at the verilog side we match integer, positive, natural
        if (generic_type) {
            if (id2->IsIntegerType()) return 1 ;
            if (id2->IsPositiveType()) return 1 ;
            if (id2->IsNaturalType()) return 1 ;
            if (id2->IsStdTimeType()) return 1 ; // Viper 6495: support for time type
            if (id2->IsEnumerationType()) return 1 ; // Viper 6649: enum vs array type matching
        }
    }

    // Viper 4875 Check between vl_types and basetypes
    if (id2->BaseType() != id2) {
        VhdlIdDef *id2_basetype = id2->BaseType() ;
        return (TypeMatchDual(id2_basetype, generic_type)) ;
    }

    // if vl_logic or vl_logic_vector type we match with bit, boolean and
    // std_logic or their corresponding vectors

    // The vl_ulogic is the base type. vl_logic is its subtype
    if (Strings::compare(id1->Name(), "vl_ulogic")) {
        if (Strings::compare(id2->Name(), "bit")) return 1 ;
        if (Strings::compare(id2->Name(), "boolean")) return 1 ;
        if (Strings::compare(id2->Name(), "std_logic")) return 1 ;
        if (Strings::compare(id2->Name(), "std_ulogic")) return 1 ;
    }
#if 0
    if (Strings::compare(id1->Name(),"vl_logic_vector") || Strings::compare(id1->Name(), "vl_ulogic_vector")) {
        if (Strings::compare(id2->Name(), "bit_vector")) return 1 ;
        if (Strings::compare(id2->Name(), "std_logic_vector")) return 1 ;
        if (Strings::compare(id2->Name(), "std_ulogic_vector")) return 1 ;
        if (Strings::compare(id2->Name(), "signed")) return 1 ; //Viper 4652. vl_logic is created for signed verilog ports
        if (Strings::compare(id2->Name(), "unsigned")) return 1 ; //Viper 4652. vl_logic is created for signed verilog ports
    }
    if (Strings::compare(id2->Name(), "vl_logic") || Strings::compare(id2->Name(), "vl_ulogic")) {
        if (Strings::compare(id1->Name(), "bit")) return 1 ;
        if (Strings::compare(id1->Name(), "boolean")) return 1 ;
        if (Strings::compare(id1->Name(), "std_logic")) return 1 ;
        if (Strings::compare(id1->Name(), "std_ulogic")) return 1 ;
    }
    if (Strings::compare(id2->Name(), "vl_logic_vector") || Strings::compare(id2->Name(), "vl_logic_vector")) {
        if (Strings::compare(id1->Name(), "bit_vector")) return 1 ;
        if (Strings::compare(id1->Name(), "std_logic_vector")) return 1 ;
        if (Strings::compare(id1->Name(), "std_ulogic_vector")) return 1 ;
        if (Strings::compare(id2->Name(), "signed")) return 1 ; // Viper 4652. vl_logic is created for signed verilog ports
        if (Strings::compare(id2->Name(), "unsigned")) return 1 ; //Viper 4652. vl_logic is created for signed verilog ports
    }
#endif

    return 0 ;
}
unsigned VhdlAliasId::TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const
{
    // use the target alias id to compare types :
    if (_target_id) return _target_id->TypeMatchDual(id, generic_type) ;
    return 0 ; // no a type-alias.
}
//Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922

// Check closely related types :
unsigned VhdlIdDef::IsCloselyRelatedType(const VhdlIdDef * /*t*/) const { return 0 ; }
unsigned VhdlSubtypeId::IsCloselyRelatedType(const VhdlIdDef *t) const
{
    VhdlIdDef *type = BaseType() ;
    return (type) ? type->IsCloselyRelatedType(t) : 0 ;
}
unsigned VhdlTypeId::IsCloselyRelatedType(const VhdlIdDef *t) const
{
    if (!t || !t->IsType()) return 0 ;

    VhdlIdDef const *id1 = this ;
    if (IsIncompleteType()) id1 = DesignatedType() ;
    VhdlIdDef const *id2 = t->IsIncompleteType() ? t->DesignatedType() : t ;

    if (!id1 || !id2) return 0 ;

    if (id1->TypeMatch(id2)) return 1 ;

    // LRM 7.3.5 : closely related types
    if (id1->IsIntegerType() || id1->IsRealType()) {
        return (id2->IsIntegerType() || id2->IsRealType()) ;
    }

    if (id1->IsArrayType() && id2->IsArrayType()) {
        // Dimension should be the same
        if (id1->Dimension() != id2->Dimension()) return 0 ;
        if (!IsVhdl2008()) {
            // VIPER #7516 : In VHDL 2008 section 9.3.6, index types should not
            // be closely related
            // Each Index type must be closely related
            for (unsigned i=0; i<id1->Dimension() ; i++) {
                if (!id1->IndexType(i)->IsCloselyRelatedType(id2->IndexType(i))) return 0 ;
            }
        }
        if (IsVhdl2008()) {
            // VIPER #7516 : In VHDL 2008 section 9.3.6, element types should be
            // closely related.
            if (!id1->ElementType()->IsCloselyRelatedType(id2->ElementType())) return 0 ;
        } else {
            // Element types are the same
            if (!id1->ElementType()->TypeMatch(id2->ElementType())) return 0 ;
        }
        return 1 ; // Closely related array types
    }

    return 0 ; // Not closely related
}
unsigned VhdlAliasId::IsCloselyRelatedType(const VhdlIdDef *t) const
{
    // Offload to target identifier (for non-object aliases)
    if (_target_id) return _target_id->IsCloselyRelatedType(t) ;
    return 0 ;
}

/****************************************************************
 * Entity/Component : obtain ports/generics by order.
 ****************************************************************/

// Get Port/Generics by position :
VhdlIdDef *VhdlIdDef::GetGenericAt(unsigned /*pos*/) const    { return 0 ; }
VhdlIdDef *VhdlIdDef::GetPortAt(unsigned /*pos*/) const       { return 0 ; }

VhdlIdDef *VhdlAliasId::GetGenericAt(unsigned pos) const      { return (_target_id) ? _target_id->GetGenericAt(pos) : 0 ; }
VhdlIdDef *VhdlAliasId::GetPortAt(unsigned pos) const         { return (_target_id) ? _target_id->GetPortAt(pos) : 0 ; }

VhdlIdDef *VhdlPackageId::GetGenericAt(unsigned pos) const
{
    if (!_generics || _generics->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_generics->At(pos) ;
}
VhdlIdDef *VhdlSubprogramId::GetGenericAt(unsigned pos) const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;

    if (uninstantiated_subprog) return uninstantiated_subprog->GetGenericAt(pos) ;

    if (!_generics || _generics->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_generics->At(pos) ;
}
VhdlIdDef *VhdlEntityId::GetGenericAt(unsigned pos) const
{
    if (!_generics || _generics->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_generics->At(pos) ;
}
VhdlIdDef *VhdlEntityId::GetPortAt(unsigned pos) const
{
    if (!_ports || _ports->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_ports->At(pos) ;
}
VhdlIdDef *VhdlComponentId::GetGenericAt(unsigned pos) const
{
    if (!_generics || _generics->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_generics->At(pos) ;
}
VhdlIdDef *VhdlComponentId::GetPortAt(unsigned pos) const
{
    if (!_ports || _ports->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_ports->At(pos) ;
}
VhdlIdDef *VhdlBlockId::GetGenericAt(unsigned pos) const
{
    if (!_generics || _generics->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_generics->At(pos) ;
}
VhdlIdDef *VhdlBlockId::GetPortAt(unsigned pos) const
{
    if (!_ports || _ports->Size()<=pos) return 0 ;
    return (VhdlIdDef*)_ports->At(pos) ;
}
VhdlIdDef *VhdlConfigurationId::GetGenericAt(unsigned pos ) const   { return (_entity) ? _entity->GetGenericAt(pos) : 0 ; }
VhdlIdDef *VhdlConfigurationId::GetPortAt(unsigned pos) const       { return (_entity) ? _entity->GetPortAt(pos) : 0 ; }

// Get Port/Generics by name
VhdlIdDef *VhdlIdDef::GetGeneric(const char * /*name*/) const       { return 0 ; }
VhdlIdDef *VhdlIdDef::GetPort(const char * /*name*/) const          { return 0 ; }

VhdlIdDef *VhdlAliasId::GetGeneric(const char *name) const          { return (_target_id) ? _target_id->GetGeneric(name) : 0 ; }
VhdlIdDef *VhdlAliasId::GetPort(const char *name) const             { return (_target_id) ? _target_id->GetPort(name) : 0 ; }

VhdlIdDef *VhdlPackageId::GetGeneric(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
    if (!id) return 0 ;
    if (!id->IsGeneric()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlSubprogramId::GetGeneric(const char *name) const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;

    if (uninstantiated_subprog) return uninstantiated_subprog->GetGeneric(name) ;

    // Fast : look up in the scope :
    VhdlIdDef *id = (LocalScope()) ? LocalScope()->FindSingleObjectLocal(name) : 0 ;
    if (!id) return 0 ;
    if (!id->IsGeneric()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlEntityId::GetGeneric(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    VhdlPrimaryUnit *p_unit = GetPrimaryUnit() ;
    if ((!id || !id->IsGeneric()) && p_unit && p_unit->IsVerilogModule()) {
        if(name[0] != '\\' && name[Strings::len(name) -1]!='\\') {
            // Viper 7667 : if the name is not escaped then we have to do a case insensitive search in the
            // converted entity.
            // Create unescaped name and check whether it exists
            unsigned i ;
            VhdlIdDef *id2 ;
            FOREACH_ARRAY_ITEM(_generics, i, id2) {
                if (!id2) continue ;
                char* unescaped_port_name = VhdlNode::UnescapeVhdlName(id2->Name()) ;
                if (!unescaped_port_name) continue ;
                if (Strings::compare_nocase(name, unescaped_port_name)) {
                    Strings::free(unescaped_port_name) ;
                    return id2 ;
                }
                Strings::free(unescaped_port_name) ;
            }
        } else {
            // Viper 7667: If "name" is escaped then if the generic name is also escaped in the converted entity
            // it would be found by FindSingleObjectLocal. However if the name is not escaped in converted
            // entity like if it is a vhdl keyword or a all lower case signal in verilog but is accessed by
            // escape name in Vhdl. Then we escape the name and search. Do a case sensitive search to honour the escaping
            // Create escaped name and check whether it exists
            unsigned i ;
            VhdlIdDef *id2 ;
            FOREACH_ARRAY_ITEM(_generics, i, id2) {
                if (!id2) continue ;
                char* escaped_port_name = Strings::save("\\", id2->Name(), "\\") ;
                if (Strings::compare(name, escaped_port_name)) {
                    Strings::free(escaped_port_name) ;
                    return id2 ;
                }
                Strings::free(escaped_port_name) ;
            }
        }
    }
#endif
    if (!id) return 0 ;
    if (!id->IsGeneric()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlEntityId::GetPort(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    VhdlPrimaryUnit *p_unit = GetPrimaryUnit() ;
    if ((!id || !id->IsPort()) && p_unit && p_unit->IsVerilogModule()) {
        if(name[0] != '\\' && name[Strings::len(name) -1]!='\\') {
            // Viper 7667 : if the name is not escaped then we have to do a case insensitive search in the
            // converted entity.
            // Create unescaped name and check whether it exists
            unsigned i ;
            VhdlIdDef *id2 ;
            FOREACH_ARRAY_ITEM(_ports, i, id2) {
                if (!id2) continue ;
                char* unescaped_port_name = VhdlNode::UnescapeVhdlName(id2->Name()) ;
                if (!unescaped_port_name) continue ;
                if (Strings::compare_nocase(name, unescaped_port_name)) {
                    Strings::free(unescaped_port_name) ;
                    return id2 ;
                }
                Strings::free(unescaped_port_name) ;
            }
        } else {
            // Viper 7667: If the name is escaped then if the port name is also escaped in the converted entity
            // it would be found by FindSingleObjectLocal. However if the name is not escaped in converted
            // entity like if it is a vhdl keyword or a all lower case signal in verilog but is accessed by
            // escape name in Vhdl. Then we escape the name and search.
            // Create escaped name and check whether it exists. Do a case sensitive search to honour the escaping
            unsigned i ;
            VhdlIdDef *id2 ;
            FOREACH_ARRAY_ITEM(_ports, i, id2) {
                if (!id2) continue ;
                char* escaped_port_name = Strings::save("\\", id2->Name(), "\\") ;
                if (Strings::compare(name, escaped_port_name)) {
                    Strings::free(escaped_port_name) ;
                    return id2 ;
                }
                Strings::free(escaped_port_name) ;
            }
        }
    }
#endif
    if (!id) return 0 ;
    if (!id->IsPort()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlComponentId::GetGeneric(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
    if (!id) return 0 ;
    if (!id->IsGeneric()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlComponentId::GetPort(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
    if (!id) return 0 ;
    if (!id->IsPort()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlBlockId::GetGeneric(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
    if (!id) return 0 ;
    if (!id->IsGeneric()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlBlockId::GetPort(const char *name) const
{
    // Fast : look up in the scope :
    VhdlIdDef *id = (_scope) ? _scope->FindSingleObjectLocal(name) : 0 ;
    if (!id) return 0 ;
    if (!id->IsPort()) return 0 ;
    return id ;
}
VhdlIdDef *VhdlConfigurationId::GetGeneric(const char *name) const  { return (_entity) ? _entity->GetGeneric(name) : 0 ; }
VhdlIdDef *VhdlConfigurationId::GetPort(const char *name) const     { return (_entity) ? _entity->GetPort(name) : 0 ; }

// Count generics/ports
unsigned VhdlIdDef::NumOfPorts() const                  { return 0 ; }
unsigned VhdlIdDef::NumOfGenerics() const               { return 0 ; }
unsigned VhdlAliasId::NumOfPorts() const                { return (_target_id) ? _target_id->NumOfPorts() : 0 ; }
unsigned VhdlAliasId::NumOfGenerics() const             { return (_target_id) ? _target_id->NumOfGenerics() : 0 ; }
unsigned VhdlEntityId::NumOfPorts() const               { return (_ports) ? _ports->Size() : 0 ; }
unsigned VhdlEntityId::NumOfGenerics() const            { return (_generics) ? _generics->Size() : 0 ; }
unsigned VhdlPackageId::NumOfGenerics() const            { return (_generics) ? _generics->Size() : 0 ; }
unsigned VhdlComponentId::NumOfPorts() const            { return (_ports) ? _ports->Size() : 0 ; }
unsigned VhdlComponentId::NumOfGenerics() const         { return (_generics) ? _generics->Size() : 0 ; }
unsigned VhdlConfigurationId::NumOfPorts() const        { return (_entity) ? _entity->NumOfPorts() : 0 ; }
unsigned VhdlConfigurationId::NumOfGenerics() const     { return (_entity) ? _entity->NumOfGenerics() : 0 ; }
unsigned VhdlBlockId::NumOfPorts() const                { return (_ports) ? _ports->Size() : 0 ; }
unsigned VhdlBlockId::NumOfGenerics() const             { return (_generics) ? _generics->Size() : 0 ; }

VhdlIdDef *VhdlIdDef::GetEntity() const                 { return const_cast<VhdlIdDef*>(this) ; }
VhdlIdDef *VhdlArchitectureId::GetEntity() const        { return (_entity) ? _entity : const_cast<VhdlArchitectureId*>(this) ; }
VhdlIdDef *VhdlConfigurationId::GetEntity() const       { return (_entity) ? _entity : const_cast<VhdlConfigurationId*>(this) ; }

/****************************************************************
 * Subprogram related routines.
 ****************************************************************/

unsigned VhdlIdDef::NumOfArgs() const                   { return 0 ; }
unsigned VhdlSubprogramId::NumOfArgs() const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst && !IsGeneric()) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog) return uninstantiated_subprog->NumOfArgs() ;

    return (_args) ? _args->Size() : 0 ;
}
unsigned VhdlAliasId::NumOfArgs() const                 { return (_target_id) ? _target_id->NumOfArgs() : 0 ; }

// Access argument types
VhdlIdDef *VhdlIdDef::ArgType(unsigned /*idx*/) const   { return 0 ; }
VhdlIdDef *VhdlSubprogramId::ArgType(unsigned idx) const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst && !IsGeneric()) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog) return uninstantiated_subprog->ArgType(idx) ;

    if (!_args) return 0 ;
    if (idx>=_args->Size()) return 0 ;
    VhdlIdDef *arg = (VhdlIdDef*) _args->At(idx) ;
    return (arg) ? arg->Type() : 0 ;
}
VhdlIdDef *VhdlAliasId::ArgType(unsigned idx) const     { return (_target_id) ? _target_id->ArgType(idx) : 0 ; }

VhdlIdDef *VhdlIdDef::Arg(unsigned /*idx*/) const       { return 0 ; }
VhdlIdDef *VhdlSubprogramId::Arg(unsigned idx) const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst && !IsGeneric()) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog) return uninstantiated_subprog->Arg(idx) ;

    if (!_args) return 0 ;
    if (idx>=_args->Size()) return 0 ;
    return (VhdlIdDef*) _args->At(idx) ;
}
VhdlIdDef *VhdlAliasId::Arg(unsigned idx) const         { return (_target_id) ? _target_id->Arg(idx) : 0 ; }

unsigned VhdlIdDef::HasInitAssign() const               { return 0 ; }
unsigned VhdlInterfaceId::HasInitAssign()  const        { return _has_init_assign ; }
unsigned VhdlConstantId::HasInitAssign() const          { return (_decl) ? 1 : 0 ; }

unsigned VhdlIdDef::GetKind() const                     { return 0 ; }
unsigned VhdlInterfaceId::GetKind() const
{
    return DecodeKind(_kind) ;
}
unsigned VhdlAliasId::GetKind() const
{
    return DecodeKind(_kind) ;
}

unsigned VhdlInterfaceId::GetSignalKind() const
{
    return DecodeSignalKind(_signal_kind) ;
}

unsigned VhdlSignalId::GetSignalKind() const
{
    return DecodeSignalKind(_signal_kind) ;
}

unsigned VhdlInterfaceId::GetObjectKind() const
{
    return DecodeObjectKind(_object_kind) ;
}

void VhdlIdDef::SetObjectKind(unsigned /*object_kind*/) { VERIFIC_ASSERT(0) ; }
void VhdlInterfaceId::SetObjectKind(unsigned object_kind)
{
    // SetObjectKind is set at the end of a interface list declaration.
    // LRM 4.3.2.1 : interface objects cannot be used inside their own interface list decl.
    if ((!IsVhdl2008() || (object_kind != VHDL_generic)) && IsUsed()) {
        Error("%s cannot be used within its own interface list", Name()) ;
    }

    // Set the defaults of the various interface properties (mode, kind etc),
    // if they are not yet set, and also do the interface identifier checks of LRM 4.3.2.1.

    // LRM 4.3.2 (340-341) :
    if (!_mode && DecodeKind(_kind)!=VHDL_file) _mode = EncodeMode(VHDL_in) ; // default mode of any interface element that is not a file.

    // Now set/check the other properties based on the object kind.
    if (object_kind==VHDL_port) {
        // port in a port list
        // Check existing settings.
        // LRM 4.3.2.1 (410) :
        if (_kind && DecodeKind(_kind) != VHDL_signal) {
            Error("%s should be a %s", Name(),EntityClassName(VHDL_signal)) ;
        }
        _kind = EncodeKind(VHDL_signal) ;
    }
    if (object_kind==VHDL_generic) {
        // Generic in a generic list
        // Check existing settings.
        // LRM 4.3.2.1 (410) :
        if (_kind && DecodeKind(_kind) != VHDL_constant) {
            Error("%s should be a %s", Name(),EntityClassName(VHDL_constant)) ;
        }
        _kind = EncodeKind(VHDL_constant) ;
    }
    // Viper 6653: object_kind==0 is parameter w/o explicit keyword parameter
    // Viper 6653: object_kind==VHDL_parameter is parameter with explicit keyword parameter
    if (!object_kind || object_kind==VHDL_parameter) {
        // Parameter of a subprogram.

        // LRM 4.3.2.1. Parameters can be any 'kind'. No check needed.
        // Default is specified in LRM 2.1.1 (43-44) :
        // Set default kind : inputs are 'constant', out/inout are 'variable'
        if (!_kind) _kind = EncodeKind((DecodeMode(_mode)==VHDL_in) ? VHDL_constant : VHDL_variable) ;

        // LRM 4.3.2 (351-353) :
        if (_has_init_assign) {
            if (DecodeKind(_kind) == VHDL_signal) {
                Error("%s %s cannot have a default expression", EntityClassName(_kind), Name()) ;
            }
            if (DecodeKind(_kind) == VHDL_variable && DecodeMode(_mode) != VHDL_in) {
                Error("%s variable %s cannot have a default expression", EntityClassName(DecodeMode(_mode)), Name()) ;
            }
        }
    }

    // LRM 4.3.2 syntax rules that yacc could not do :
    if (DecodeKind(_kind) == VHDL_constant && DecodeMode(_mode) != VHDL_in) {
        Error("constant %s can only be of mode IN", Name()) ;
    }
    if (_signal_kind && DecodeKind(_kind) != VHDL_signal) {
        // check for presence of 'bus' or 'register' :
        Error("%s is only allowed on signals", EntityClassName(DecodeSignalKind(_signal_kind))) ;
    }

    // LRM 4.3.2 (343-344) :
    // LRM specifies that also 'constant' cannot be a file.
    // But subprogram ENDFILE in the old 87 textio package still violates this.
    // So don't do this test in '87 mode
    if (!IsVhdl87() && (DecodeKind(_kind) == VHDL_signal || DecodeKind(_kind) == VHDL_constant) && _type && (_type->IsAccessType() || _type->IsFileType())) {
        Error("formal %s %s cannot be of a file type or access type", EntityClassName(DecodeKind(_kind)), Name()) ;
    }
    // LRM 4.3.2 (345-346) :
    if (DecodeKind(_kind) == VHDL_file && _type && !_type->IsFileType()) {
        Error("formal file %s should be of a file type", Name()) ;
    }
    // LRM 4.3.2 (350) :
    if (_has_init_assign) {
        if (DecodeMode(_mode) == VHDL_linkage) {
            Error("linkage interface object %s cannot have a default expression", Name()) ;
        }
    }

    // Label the interface object kind (port, generic, or 0(parameter)) of this identifier.
    // If 'object_kind' is VHDL_port, this is a port
    // If 'object_kind' is VHDL_generic, this is a generic
    // If 'object_kind' is VHDL_parameter, this is a (subprogram) parameter
    _object_kind = EncodeObjectKind(object_kind) ;
}

unsigned VhdlInterfaceId::DecodeObjectKind(unsigned obj_kind) const
{
    // Viper 6653: object_kind==0 is parameter w/o explicit keyword parameter
    // Viper 6653: object_kind==VHDL_parameter is parameter with explicit keyword parameter
    switch(obj_kind) {
    case 0: return 0 ;
    case 1: return VHDL_generic ;
    case 2: return VHDL_port ;
    case 3: return VHDL_parameter ;
    default: return obj_kind ;
    }

    return 0 ;
}

unsigned VhdlInterfaceId::EncodeObjectKind(unsigned obj_kind) const
{
    // Viper 6653: object_kind==0 is parameter w/o explicit keyword parameter
    // Viper 6653: object_kind==VHDL_parameter is parameter with explicit keyword parameter
    switch(obj_kind) {
    case 0 : return 0 ;
    case VHDL_generic: return 1 ;
    case VHDL_port:    return 2 ;
    case VHDL_parameter: return 3 ;
    default:           VERIFIC_ASSERT(0) ; return 0 ; // do not know this token
    }

    return 0 ;
}

unsigned VhdlIdDef::DecodeKind(unsigned kind) const
{
    switch(kind) {
    case 1:  return VHDL_constant ;
    case 2:  return VHDL_signal ;
    case 3:  return VHDL_variable ;
    case 4:  return VHDL_file ;
    default: return kind ;
    }

    return 0 ;
}

unsigned VhdlIdDef::EncodeKind(unsigned kind) const
{
    switch(kind) {
    case 0:             return 0 ;
    case VHDL_constant: return 1 ;
    case VHDL_signal:   return 2 ;
    case VHDL_variable: return 3 ;
    case VHDL_file:     return 4 ;
    default:            VERIFIC_ASSERT(0) ; return 0 ;  // do not know this token
    }

    return 0 ;
}

unsigned VhdlInterfaceId::DecodeMode(unsigned mode) const
{
    switch(mode) {
    case 1:  return VHDL_in ;
    case 2:  return VHDL_inout ;
    case 3:  return VHDL_out ;
    case 4:  return VHDL_buffer ;
    case 5:  return VHDL_linkage ;
    default: return mode ;
    }

    return 0 ;
}

unsigned VhdlInterfaceId::EncodeMode(unsigned mode) const
{
    switch(mode) {
    case 0:            return 0 ;
    case VHDL_in:      return 1 ;
    case VHDL_inout:   return 2 ;
    case VHDL_out:     return 3 ;
    case VHDL_buffer:  return 4 ;
    case VHDL_linkage: return 5 ;
    default:           VERIFIC_ASSERT(0) ; return 0 ;  // do not know this token
    }

    return 0 ;
}

unsigned VhdlInterfaceId::DecodeSignalKind(unsigned signal_kind) const
{
    switch(signal_kind) {
    case 1:  return VHDL_register ;
    case 2:  return VHDL_bus ;
    default: return signal_kind ;
    }

    return 0 ;
}

unsigned VhdlInterfaceId::EncodeSignalKind(unsigned signal_kind) const
{
    switch(signal_kind) {
    case 0:             return 0 ;
    case VHDL_register: return 1 ;
    case VHDL_bus:      return 2 ;
    default:            VERIFIC_ASSERT(0) ; return 0 ;  // do not know this token
    }

    return 0 ;
}

unsigned VhdlSignalId::DecodeSignalKind(unsigned signal_kind) const
{
    switch(signal_kind) {
    case 1:  return VHDL_register ;
    case 2:  return VHDL_bus ;
    case 3:  return VHDL_implicit_guard ;
    default: return signal_kind ;
    }

    return 0 ;
}

unsigned VhdlSignalId::EncodeSignalKind(unsigned signal_kind) const
{
    switch(signal_kind) {
    case 0:                     return 0 ;
    case VHDL_register:         return 1 ;
    case VHDL_bus:              return 2 ;
    case VHDL_implicit_guard:   return 3 ;
    default:            VERIFIC_ASSERT(0) ; return 0 ;  // do not know this token
    }

    return 0 ;
}
// Viper 7515, 7518,7519 : Prune multiple ids obtained by FindObjects to retain the
// one which confirms with with 'this' id.
void
VhdlAliasId::Prune(Set* ids)
{
    if (_target_id) _target_id->Prune(ids) ;
}

void
VhdlSubprogramId::Prune(Set* ids)
{
    if (!ids || ids->Size()<=1) return ;
    VhdlIdDef *subprog_id ;
    SetIter si ;
    FOREACH_SET_ITEM(ids, si, &subprog_id) {
        if (!subprog_id) continue ;

        if ((subprog_id == (VhdlIdDef*)this) ||
            (subprog_id->IsPackageInstElement() && (subprog_id->GetConnectedActualId()==this))) {
            (void) ids->Remove(subprog_id) ;
            continue ;
        }
        if (!subprog_id->IsSubprogram() || (subprog_id->IsFunction() && !IsFunction()) ||
              (subprog_id->IsProcedure() && !IsProcedure())) {
            (void) ids->Remove(subprog_id) ;
            continue ;
        }
        if (NumOfArgs() !=subprog_id->NumOfArgs()) {
            (void) ids->Remove(subprog_id) ;
            continue ;
        }

        VhdlIdDef* this_ret_type = Type() ;
        VhdlIdDef* subprog_id_ret_type = subprog_id->Type() ;
        if (this_ret_type && !this_ret_type->TypeMatch(subprog_id_ret_type)) {
            (void) ids->Remove(subprog_id) ;
            continue ;
        }
        unsigned i ;
        for (i=0; i<NumOfArgs(); i++) {
            VhdlIdDef *this_arg_type = ArgType(i) ;
            VhdlIdDef *subprog_id_arg_type = subprog_id->ArgType(i) ;
            if (!(this_arg_type && subprog_id_arg_type && this_arg_type->TypeMatch(subprog_id_arg_type))) {
                (void) ids->Remove(subprog_id) ;
                continue ;
            }
        }
    }

    if (ids->Size() <= 1) return ; // No need to prune further

    // VIPER #7543 : Need to skip pre-defined operators
    VhdlIdDef *match = 0 ;
    unsigned first_time = 1 ;
    FOREACH_SET_ITEM(ids, si, &subprog_id) {
        if (!subprog_id) continue ;
        unsigned one_predefined = match && (match->IsPredefinedOperator() || subprog_id->IsPredefinedOperator()) ;
        if (match && (match->IsHomograph(subprog_id) || one_predefined)) {
            if (match->IsPredefinedOperator()) {
                (void) ids->Remove(match) ;
                match = subprog_id ;
            } else {
                if (!subprog_id->IsPredefinedOperator() && _present_scope && !_present_scope->IsDeclaredHere(match, 1 /*look in upper/extended*/)) {
                    if (first_time) {
                        Error("multiple declarations of %s included via multiple use clauses; none are made directly visible", subprog_id->Name()) ;
                        match->Info("%s is declared here", subprog_id->Name()) ;
                        first_time = 0 ;
                    }
                    subprog_id->Info("another match is here") ;
                }
                (void) ids->Remove(subprog_id) ;
            }
            continue ;
        }
        match = subprog_id ;
    }
}
void
VhdlPackageInstElement::Prune(Set* ids)
{
    if (_actual_id) _actual_id->Prune(ids) ;
}

void
VhdlIdDef::TypeInferAssociationList(Array *assoc_list, unsigned object_kind, VhdlScope *actual_binding_scope, const VhdlTreeNode *from)
{
    // Translate this association list from named to ordered.
    // Do this by manipulating the parse tree.
    // Happens before or after type-inference of the instantiation in this assoc,
    // but still during analysis.
    // No static info is used.
    //
    // If 'object_kind' is VHDL_port, this is a port-association list (of a block, component or entity)
    // If 'object_kind' is VHDL_generic, this is a generic-association list (of a block, component or entity)
    // If 'object_kind' is 0, this is a parameter-association list (of a function or procedure)

    // If 'actual_binding_scope' is set, we come from a binding indication.
    // In that case, don't error out on unassociated formals : LRM 5.2.1.2 (316:NOTE) :
    // From a binding indication, not all formals have to be associated,
    // since they can be incrementally bound later.

    // Also, pass the binding scope on to type inferencing of the actual.
    // That's needed for LRM 10.3 h) and i) : they need the component formals to be
    // visible in the actuals of the binding.

    // Get the local scope to find formals (LRM :
    VhdlScope *local_scope = 0 ;
    unsigned is_converted_configuration = 0 ; // flag to indicate it is a converted vhdl configuration from verilog configuration
    VhdlPrimaryUnit *pu = GetPrimaryUnit() ;
    if (IsConfiguration()) {
        if (pu && pu->IsVerilogModule())  is_converted_configuration = 1 ;
    }

    if (IsConfiguration() && GetEntity()) { // issue 1539
        // LRM 10.3 j) and k) : if unit is a configuration, association lists are also allowed,
        // and they apply to the binding entity :
        local_scope = GetEntity()->LocalScope() ;
    }

    // For subprogram instance, get scope of uninstantiated subprog
    if (IsSubprogramInst() && !IsGeneric()) {
        VhdlDeclaration *inst_decl = GetSubprogInstanceDecl() ;
        VhdlIdDef *uninstantiated_subprog = inst_decl ? inst_decl->GetUninstantiatedSubprogId(): 0 ;
        local_scope = uninstantiated_subprog ? uninstantiated_subprog->LocalScope(): 0 ;
        if (!object_kind) { // It is parameter association
            // This is a subprogram instance call. To type infer the assoc
            // list of call, need to set actual types of typed generic from
            // generic map aspect of subprogram instantiation
            if (inst_decl) inst_decl->SetActualTypes() ;
        }
    }

    if (!local_scope && IsSubprogram() && !IsSubprogramInst()) {
        // association list to be resolved using the decl specification scope
        // in the package header. This is important to avoid memory corruption.
        // If the subprogram body is inside a package body and the package
        // is re-analyzed, its existing body is replaced with the newly
        // analyzed package body. So all pointers to the identifiers inside
        // old package body scope become stale. Everything should thus point
        // to the package header.
        VhdlSpecification *spec = Spec() ;
        VhdlIdDef *spec_id = spec ? spec->GetId() : 0 ;
        VhdlSpecification *spec_id_spec = spec_id ? spec_id->Spec() : 0 ;
        local_scope = spec_id_spec ? spec_id_spec->LocalScope() : 0 ;
    }

    // In all other cases, we should look for formals here, at this id, local.
    if (!local_scope) local_scope = LocalScope() ;
    // local_scope can still be 0 for predefined operators. Still process the actuals.

    int pos ;
    VhdlName *formal_part ;
    VhdlDiscreteRange *actual_part ;
    VhdlIdDef *formal, *actual ;

    // Type-infer the association list, and do checks on unassociated formals and too-many actuals.
    VhdlIdDef *formal_type, *actual_type ;
    VhdlIdDef *formal_desig_type, *actual_desig_type ; // These are needed for type conversion
    unsigned is_named = 0 ;
    unsigned has_partials = 0 ;

    // Also keep track of the names of the formals that are associated :
    // Use names, not pointers, since there could be spec<->decl formal diffences
    // Give initial size that is likely big enough (avoid rehashing)
    Set associated_formals(STRING_HASH, (assoc_list) ? assoc_list->Size() : 1) ;

    VhdlDiscreteRange *assoc ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
        if (assoc && (assoc->IsBox() || assoc->IsDefault())) return ; // assoc for package interface decl
        // VIPER #6645 : Produce warning if inertial is used in assoc element other than in port map
        if ((object_kind != VHDL_port) && (assoc && assoc->IsInertial())) {
            assoc->Warning("keyword inertial can only be used in port map") ;
        }
        formal_part = (assoc) ? assoc->FormalPart() : 0 ;
        actual_part = (assoc) ? assoc->ActualPart() : 0 ;

        formal_desig_type = actual_desig_type = 0 ; // Clear these out (used only for type conversion)

        if (formal_part) {
            // NAMED ASSOCIATION :
            is_named = 1 ;  // Label that we are now in named associations.

            // Type infer formal now.  Be aware that if this formal ends up
            // being a type conversion, we'll have to type infer this formal again
            // later on passing in the actual type for the function return value. (VIPER #1956)
            formal_type = formal_part->TypeInferFormal(local_scope,0) ;
            formal = formal_part->FindFormal() ;
            if (!formal) {
                // Viper #5931: type infer assoc list for implicit operator
                const char *formal_name = formal_part->Name() ;
                if (!IsPredefinedOperator() || !formal_name) break ;
                unsigned arg_idx = 0 ;
                unsigned max_args = NumOfArgs() ;
                for (arg_idx = 0; arg_idx < max_args; ++arg_idx) {
                    VhdlIdDef *oper_arg = Arg(arg_idx) ;
                    if (!oper_arg) continue ;

                    const char *arg_name = oper_arg->Name() ;
                    if (arg_name && Strings::compare_nocase(arg_name, formal_name)) {
                        formal = oper_arg ;
                        formal_type = oper_arg->Type() ;
                    }
                }
                if (!formal) break ; // Something bad is wrong.
            }

            formal_desig_type = formal->Type() ;

            // Check that we really have the right formal class here :
            if (object_kind==VHDL_port && !formal->IsPort())        formal_part->Error("formal %s is not a %s", formal->Name(), "port") ;
            if (object_kind==VHDL_generic && !formal->IsGeneric())  formal_part->Error("formal %s is not a %s", formal->Name(), "generic") ;
            // Viper 6653: object_kind==0 is parameter w/o explicit keyword parameter
            // Viper 6653: object_kind==VHDL_parameter is parameter with explicit keyword parameter
            if ((!object_kind || object_kind==VHDL_parameter) && !formal->IsInterfaceObject(0))      formal_part->Error("formal %s is not a %s", formal->Name(), "parameter") ;

            if (!formal_part->FindFullFormal()) {
                // Partially associated formal :
                if (!actual_part || actual_part->IsOpen()) {
                    // LRM rule check 4.3.2.2 (516-518) partial or type-conversion formals cannot have 'open' assocs :
                    // VIPER issues 3100, 2310 and 2675 all ask to downgrade this to a warning
                    formal_part->Warning("partially associated formal %s cannot have actual OPEN", formal->Name()) ;
                }
                has_partials = 1 ;
            }
        } else {
            // POSITIONAL ASSOCIATION :
            // Test basic LRM rule :
            if (is_named) {
                from = (assoc) ? assoc : from ; // Get accurate line/file info
                from->Error("positional association cannot follow named association") ;
                return ; // No use to continue.
            }

            // Get the formal at this position :
            switch (object_kind) {
            case VHDL_port    : formal = GetPortAt(i) ; break ;
            case VHDL_generic : formal = GetGenericAt(i) ; break ;
            default           : formal = Arg(i) ; break ;
            }

            // Viper 5491. For converted configurations we do not have formals for
            // ports and generics. However we should resolve the actuals so that
            // there values can be obtained in static elaboration
            if (!formal && !is_converted_configuration) {
                // There is no formal at this position.
                // Give 'class-specific' error message from the association list (caller).
                switch (object_kind) {
                case VHDL_port    : from->Error("%s has only %d ports", Name(), NumOfPorts()) ; break ;
                case VHDL_generic : from->Error("%s has only %d generics", Name(), NumOfGenerics()) ; break ;
                default           : from->Error("%s expects %d arguments", Name(), NumOfArgs()) ; break ;
                }
                Info("%s is declared here", Name()) ; // Point to me.
                break ; // No use continuing.
            }
            formal_type = formal ? formal->Type() : 0 ;
        }

        // VIPER #5918 : If formal type is in terms of generic type (interface type decl), consider the
        // actual type passed through generic map as formal_type
        if (formal_type && formal_type->IsGenericType()) formal_type = formal_type->ActualType() ;

        // Check if this is 'open' association
        if (!actual_part || actual_part->IsOpen()) {
            // OPEN association. No use proceding with type inference or association checks.
            // Don't report unassociated formals yet.  Do that at the end in separate loop.
            continue ;
        }

        // Label the formal as associated :
        if (formal && !associated_formals.Insert(formal->Name())) {
            // Formal already associated : allowed only if this is a partial formal :
            // Also skip this if formal is a type (happens for predefined functions).
            if ((!formal_part || formal_part->FindFullFormal()) && !formal->IsType()) {
                if (assoc) assoc->Error("formal %s is already associated",formal->Name()) ;
            }
        }

        // Set 'environment' for actual expression evaluation.
        // If formal is 'in', we expect an expression, so set to READ.
        // Other modes go through rigorous LRM association checks below (after type-inference).
        // Exclude 'file' formals : they require a file actual, which is checked separately below.
        unsigned environment = (formal && formal->IsInput() && !formal->IsFile()) ? VHDL_READ : 0 ;

        // Type inference of the actual :
        VhdlDiscreteRange *actual_designator = 0 ; // actual type inference did not yet run, so wait with that.

        // Formal type-inference already ran, so we can pick-up the formal designator.
        VhdlDiscreteRange *formal_designator = (formal_part) ? formal_part->FormalDesignator() : 0 ;

        // Is the formal a type conversion (conversion function) (named association) ?
        if (formal_designator && (formal_designator != formal_part)) {
            // This formal is a type conversion or conversion function!

            // LRM 4.3.2.2 Association lists (462-473) :
            // The formal part of a named element association may be in the form of a function call
            // or a type conversion, where the sole argument, the formal designator, is of mode
            // out, inout, buffer, or linkage, and if the actual is not open.  In this case the
            // base type denoted by the return value (type mark) must be the same base type of the actual.
            if (formal && formal->IsInput()) {
                // If this is an IN port, that should be an error (LRM 4.3.2.2 (468-473))
                if (assoc) assoc->Error("input designator %s cannot contain a formal type-conversion", formal->Name()) ;
            } else {
                // This port is either OUT, INOUT, BUFFER, LINKAGE
                // Actual type dictates formal type, so first infer actual type using no expected type:
                // No need to pass in environment : it will be difficult to determine, and later checks below require an 'actual' id, where checks are easier to do.
                actual_type = actual_part->TypeInfer(0 ,0, 0, actual_binding_scope) ;
                // Now check to see if actual is a type conversion
                actual_designator = actual_part->FormalDesignator() ;
                if (actual_designator && (actual_designator != actual_part)) {
                    // Since actual is also a type conversion (conversion function), we need to
                    // re-type-infer the actual using the formal designator as the expected type.
                    actual_type = actual_part->TypeInfer(formal_desig_type ,0, 0, actual_binding_scope) ;
                    actual_desig_type = actual_designator->TypeInfer(0, 0, 0, actual_binding_scope) ;
                }
                // Now type infer again the formal, but pass in the actual's type to verify formal return type (VIPER #1956)
                if (formal_part) (void) formal_part->TypeInferFormal(local_scope, (actual_desig_type) ? actual_desig_type : actual_type) ;
            }
        } else {
            // Normal : type-infer the actual against formal.
            // If mode is 'in', assume that an expression comes in.
            // Otherwise, the association linking below takes care of updating/reading the actual.
            // Pass environment in here, to make sure actual expression leafs are labeled as 'used'.

            // Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
            if (formal_type && ((Strings::compare(formal_type->Name(), "vl_logic") ||
                                Strings::compare(formal_type->Name(), "vl_logic_vector") ||
                                Strings::compare(formal_type->Name(), "vl_ulogic") ||    // Viper 4660 : Checks for ulogic were missing
                                Strings::compare(formal_type->Name(), "vl_ulogic_vector")) ||
                                (formal && pu && pu->IsVerilogModule() && !formal->IsGenericType() && !formal->IsSubprogram() && !formal->IsPackage()))) {
                // Viper 8481 Check all mixed language types here :  4660 : Checks for ulogic were missing
                //Formal type is passed as NULL in TypeInfer so that the already resolved type
                //of the actual is inferred. aggregate_type is not supported with vl_logic_vector

                // Viper 5082. For string and character literal we cannot pass NULL to Typeinfer
                // For string literal we pass typeid for std_logic_vector and std_logic for character literal
                VhdlIdDef *formal_verilog_type = 0 ;
                //VhdlName *sub_type_indication = 0 ;
                if (actual_part->IsStringLiteral()) {
                    // VIPER #7717 : Use vl type of formal as expected type for string literal
                    // We do not know whether to match with bit_vector or std_logic_vector or string
                    formal_verilog_type = formal_type ;
                } else if (actual_part->IsCharacterLiteral()) {
                    // VIPER #7717 : Use vl type of formal as expected type for character literal
                    formal_verilog_type = formal_type ;
                }

                actual_type = actual_part->TypeInfer(formal_verilog_type, 0, environment, actual_binding_scope) ;

                // Viper 5897. For generics need to pass is_generic flag equals to true in TypeMatchDual
                // Viper 6649: For one array and one scalar need not do type checking here for generic
                // as well as ports. For ports the type matching is done during elaboration for named actuals
                unsigned ignore_typematching = 0 ;
                if (actual_type) {
                    if (formal_type->IsArray() && !actual_type->IsArray()) ignore_typematching = 1 ;
                    if (!formal_type->IsArray() && actual_type->IsArray()) ignore_typematching = 1 ;
                    if (formal_type->IsIntegerType()) ignore_typematching = 1 ; // Viper 8481 : ignore type matching for integer type
                }

                if (formal && actual_type && Strings::compare(formal_type->Name(), "vl_logic_vector")) {
                    Array* verilog_assoc_list = formal->GetVerilogAssocList() ;
                    unsigned dim = verilog_assoc_list ? verilog_assoc_list->Size() : 0 ;
                    if (dim) {
                        actual_type = actual_type->ElementType(dim) ;
                        formal_type = formal_type->ElementType() ;
                    }
                }
                if (formal_type && !ignore_typematching && !formal_type->TypeMatchDual(actual_type, object_kind==VHDL_generic ? 1 : 0)) {
                    char* error_msg = Strings::save("of VHDL ",object_kind==VHDL_generic ? "generic " : "port ", "does not match its Verilog connection") ;
                    if (assoc) assoc->Error("type error near %s ; expected type %s", actual_part->Name(), error_msg) ;
                    Strings::free(error_msg) ;
                }
            // Direct Entity Instantiation Viper: #2410, #2279, #1856, #2922
            } else if ((object_kind == VHDL_generic) && formal && formal->IsGenericType()) {
                // If formal is generic type, actual will be subtype indication
                actual_type = actual_part->TypeInfer(0, 0, VHDL_range, actual_binding_scope) ;
                formal->SetActualId(actual_type, actual_part->GetAssocList()) ;
            } else if ((object_kind == VHDL_generic) && formal && formal->IsSubprogram()) {
                // If formal is subprogram, actual will be a subprogram whose profile
                // conforms to that of the formal
                actual_type = actual_part->SubprogramAspect(formal) ;
                if (actual_type && !actual_type->IsSubprogram()) {
                    actual_part->Error("subprogram name should be specified as actual for formal generic subprogram %s", formal->Name()) ;
                }
                if (actual_type && !formal->IsHomograph(actual_type)) {
                    actual_part->Error("subprogram %s does not conform with its declaration", actual_type->Name()) ;
                }
                formal->SetActualId(actual_type, 0) ;
            } else if ((object_kind == VHDL_generic) && formal && formal->IsPackage()) {
                // If formal is package, actual will be a package name/package instance name
                actual_type = actual_part->EntityAspect() ;
                if (actual_type && !actual_type->IsPackage()) {
                    actual_part->Error("package name should be specified as actual for formal generic package %s", formal->Name()) ;
                }
                formal->SetActualId(actual_type, 0) ;
            } else  {
                actual_type = actual_part->TypeInfer(formal_type, 0, environment, actual_binding_scope) ;
                (void) actual_type ;
            }
        }

        // Now that actual type inference ran, we can surely pick-up the actual designator :
        if (!actual_designator) actual_designator = actual_part->FormalDesignator() ;

        // Is the actual a type conversion (conversion function) (named or positional assocation) ?
        if (actual_designator && (actual_designator != actual_part)) {
            // This actual is a type conversion or conversion function!

            // The formal was already passed into the actual's type inferrence above, so we
            // don't need to do anything specific here, except check for mode compliance, as below.

            // LRM 4.3.2.2 Association lists (473-486) :
            // The actual part of a (named or positional) element association may be in the form of a
            // function call or a type conversion, where the sole argument, the actual designator, is
            // of mode in, inout, or linkage, and if the actual is not open.  In this case the base
            // type denoted by the return value (type mark) must be the same base type of the formal.
            if (formal && (formal->IsOutput() || formal->IsBuffer())) {
                // If this is an OUT, BUFFER port, that should be an error (LRM 4.3.2.2 (473-486))
                if (assoc) assoc->Error("%s designator %s cannot contain an actual type-conversion", (formal->IsOutput()) ? "output" : "buffer", formal->Name()) ;
            }
        }

        if (!formal) break ; // Viper 5491. Now break from here the actuals are already resolved
        // Now, after type-inference, the actual's identifiers are set.
        // See if there is a 'single' actual associated
        actual = actual_part->FindFormal() ; // FIX ME : naming of 'FindFormal' should change to 'FindDesignator' or so.

        // Port-association rules of LRM 1.1.1.2 :
        if (object_kind == VHDL_port) {
            // If the actual is an expression, the formal must be mode 'in' :
            // VIPER #7972: If actual is enumeration literal then it is an error if formal is not input port
            if ((!actual || actual->IsConstant() || actual->IsEnumerationLiteral())) {
                // VIPER #7890: Cannot error out on external names here. External names are resolved in elaboration only:
                if (!formal->IsInput() && !actual_part->IsExternalNameRef()) {
                    actual_part->Error("actual of formal %s port %s cannot be an expression", EntityClassName(formal->Mode()), formal->Name()) ;
                }
            }
            // Viper 7666: Modify port vs actual direction criteria for match as per Vhdl2008 rules described in section: 6.5.6.3
            if (actual && actual->IsPort()) {
                if (IsVhdl2008()) {
                    if ((formal->IsInput()  && !(actual->IsInput() || actual->IsOutput() || actual->IsInout() || actual->IsBuffer())) ||
                        (formal->IsOutput() && !(actual->IsInout() || actual->IsOutput() || actual->IsBuffer())) ||
                        (formal->IsInout()  && !(actual->IsInout() || actual->IsOutput() || actual->IsBuffer())) ||
                        (formal->IsBuffer() && !(actual->IsBuffer() || actual->IsOutput() || actual->IsInout()))) {
                        // 'linkage' can be associated with any other port, so no check needed.

                        // This is officially an error, but too many designs violate it. So make it a warning.
                        actual_part->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s",
                            formal->Name(), EntityClassName(formal->Mode()),
                            actual->Name(), EntityClassName(actual->Mode())) ;
                    }
                } else {
                    if ((formal->IsInput()  && !(actual->IsInput() || actual->IsInout() || actual->IsBuffer())) ||
                        (formal->IsOutput() && !(actual->IsInout() || actual->IsOutput())) ||
                        (formal->IsInout()  && !(actual->IsInout())) ||
                        (formal->IsBuffer() && !(actual->IsBuffer()))) {
                        // 'linkage' can be associated with any other port, so no check needed.

                        // This is officially an error, but too many designs violate it. So make it a warning.
                        actual_part->Warning("formal port %s of mode %s cannot be associated with actual port %s of mode %s",
                            formal->Name(), EntityClassName(formal->Mode()),
                            actual->Name(), EntityClassName(actual->Mode())) ;
                    }
                }
            }

            // LRM : 1.1.1.2 and 4.3.2.2 Viper #3183 Actual must be
            // static-name or globally-static-expression or resolution
            // function with static-name/static-expression single argument

            // In Vhdl2008 mode issue error if the formal is unconstrained.
            if (!actual_part->IsGoodForPortActual()) {
                if (!IsVhdl2008() || (formal->IsUnconstrained())) {
                    actual_part->Warning("actual for formal port %s is neither a static name nor a globally static expression", formal->Name()) ;
                }
            }
        }

        // Missing in LRM is the 'generic-association' rules (should be in LRM 1.1.1.1).
        // VIPER Issue 1709.
        if (object_kind == VHDL_generic) {
            // ModelSim issues this error :
            if (actual && actual->IsSignal()) {
                // VIPER 3081 : turn into a warning.
                actual_part->Warning("actual expression for generic %s cannot reference a signal",formal->Name()) ;
            }
        }

        // Viper 6653: object_kind==0 is parameter w/o explicit keyword parameter
        // Viper 6653: object_kind==VHDL_parameter is parameter with explicit keyword parameter
        // Parameter-association rules LRM 2.1.1
        if (!object_kind || object_kind == VHDL_parameter) {
            // Second check has been added for Viper 3175
            if (formal->IsFile() || formal->IsFileType()) {
                if (!actual || !actual->IsFile()) {
                    actual_part->Error("actual %s of formal %s must be a %s", (actual) ? actual->Name() : "", formal->IsFile() ? formal->Name() : "", EntityClassName(VHDL_file)) ;
                }
            } else if (formal->IsSignal()) {
                if (actual_part->IsFunctionCall() || actual_part->IsTypeConversion()) { // Viper #4341 LRM 2.1.1.2
                    actual_part->Error("signal formal %s cannot be associated with type conversion or function call at actual", formal->Name()) ;
                } else if (!actual_part->IsSignal()) { // Viper #4341 LRM 2.1.1.2
                    actual_part->Error("actual %s of formal %s must be a %s", (actual) ? actual->Name() : "", formal->Name(), EntityClassName(VHDL_signal)) ;
                }
            } else if (formal->IsVariable()) {
                // CHECK ME : is a file a variable in vhdl'87 ? '87 textio package thinks so.
                if (!actual || !actual->IsVariable()) {
                    actual_part->Error("actual %s of formal %s must be a %s", (actual) ? actual->Name() : "", formal->Name(), EntityClassName(VHDL_variable)) ;
                }
            }
            // VIPER #6803 : Check the actual part with proper environment when formal
            // is output/inout and actual is signal
            if ((formal->IsInout() || formal->IsOutput()) && actual && actual->IsSignal()) {
                // It is like assignment on 'actual' inside subprogram. Check whether it is legal
                (void) actual_part->TypeInfer(formal_type, 0, VHDL_SIGUPDATE, actual_binding_scope) ;
            }
            // VIPER #6804 : if an actual signal is associated with a signal parameter
            // of any mode, actual must be denoted by a static signal name
            if (actual && actual->IsSignal() && formal->IsSignal()) {
                if (!actual_part->IsGoodForPortActual()) actual_part->Warning("actual for formal port %s is neither a static name nor a globally static expression", formal->Name()) ;
            }
        }

        // Do READ/WRITE checks of actual-formal associations. LRM 4.3.2.
        // Also, set 'used' and 'assigned' for these.
        // In PSL directives/declarations these should be skipped.
        if (actual && !IsVhdlPsl()) {
            // We are associated :

            // Discard this one as a 'ram' (since we don't allow ram's referred in association lists)
            // VIPER issue 2674 : we should allow ram inferencing in association lists.
            // INOUT ports would constitute a read AND a write, which might discard the actual as RAM,
            // but this should be done in the VarUsage routines. Not here.
            // actual->SetCannotBeDualPortRam() ;

            // MultiPort RAMs : currently discard if the formal is OUTPUT or INOUT.
            // Have to discard OUTPUT, since RTL elaboration always does a Evaluate on the actual, which would create a read port.
            // Once that is fixed, we can allow OUTPUT also.
#if 0 // RD: This is now obsolete, since this work is done in VarUsage
            if (actual->CanBeMultiPortRam() && (formal->IsOutput() || formal->IsInout())) {
                actual->SetCannotBeMultiPortRam() ;
            }
#endif

            // Rules in 4.3.2 apply :
            // actual is READ if formal is 'in', 'inout' or 'linkage'
            if (formal->IsInput() || formal->IsInout() || formal->IsLinkage()) {
                // Label this actual as 'used'
                actual->SetUsed() ;
                // If actual is 'out' reading is not allowed. Formal mode linkage can always be associated with any actual port mode.
                // reading of out port is allowed in 2008 mode
                if (actual->IsOutput() && !formal->IsLinkage() && !vhdl_file::IsVhdl2008()) {
                    actual_part->Error("%s with mode 'out' cannot be read", actual->Name()) ;
                }
            }
            // actual is UPDATED if formal is 'out' 'buffer' 'inout' or 'linkage'
            if (formal->IsOutput() || formal->IsInout() || formal->IsBuffer() || formal->IsLinkage()) {
                actual->SetAssigned() ;
                // If actual is 'in' updating is not allowed. Formal mode linkage can always be associated with any actual port mode.
                if (actual->IsInput() && !formal->IsLinkage()) {
                    actual_part->Error("%s with mode 'in' cannot be updated", actual->Name()) ;
                }
            }
            // Finally, if actual is 'linkage', we can only read/update from a 'linkage' object
            if (actual->IsLinkage() && !formal->IsLinkage()) {
                actual_part->Error("cannot read or update 'linkage' object %s", actual->Name()) ;
            }
        }
    }

    // Final check LRM 4.3.2.2.
    // Check that all formals (at least inputs) are associated.
    // This now works for ANY association list (since we stored all associated formals in a Set).
    // Don't check this for incremental binding assoc lists (with 'actual_binding_scope' set).
    if (!actual_binding_scope) {
        // For predefined operators we can only check number of arguments (since they have no true formals)
        if (!object_kind && IsPredefinedOperator()) {
            // VIPER 3429 : test correct number of arguments for predefined operators/procedures :
            // do not use 'associated_formals' because that has types (and overlap) for predefined operators.
            if (NumOfArgs() != ((assoc_list) ? assoc_list->Size():0)) {
                from->Error("%s expects %d arguments", Name(), NumOfArgs()) ;
            }
        } else {
            // normal (user-defined) subprogram, entity or component.
            // Run over all the formals and see if they are connected (or have initial value)
            for (i=0 ; 1 ; i++) {
                // Just see if there are still more formals beyond the association list size :
                switch (object_kind) {
                case VHDL_port      : formal = GetPortAt(i) ; break ;
                case VHDL_generic   : formal = GetGenericAt(i) ; break ;
                default             : formal = Arg(i) ; break ;
                }
                if (!formal) break ; // There are no more formals ;

                // Check if this one is associated :
                if (associated_formals.GetItem(formal->Name())) continue ; // formal was associated.

                // LRM rule check 4.3.2.2 (511-515)
                // Check if the formal is 'input', and there is no default expression.
                if (formal->IsInput() && !formal->HasInitAssign()) {
                    from->Error("formal %s has no actual or default value", formal->Name()) ;
                    formal->Info("%s is declared here", formal->Name()) ;
                }

                // VIPER 3155 : LRM 8.6 : Check that procedure actual (out or inout) is not 'open'
                // Test explicitly for 'out' or 'inout' so that accidental predefined operators/procedures (without proper formals) slip through.
                if (!object_kind &&  !formal->HasInitAssign() && (formal->IsOutput() || formal->IsInout())) {
                    from->Error("formal %s of mode %s must have an associated actual", formal->Name(), EntityClassName(formal->Mode())) ;
                    formal->Info("%s is declared here", formal->Name()) ;
                }
            }
        }
    }

    // Checks for 'wait' statements.
    // This routine (VhdlIdDef::TypeInferAssociationList()) gets called when this function or procedure is called.
    // If this is a procedure call, and it contains a wait statement, then we need to do some things to propagate
    // that info upward.
    if (IsProcedure()) {
        VhdlScope *proc_scope = LocalScope() ;
        if (proc_scope && proc_scope->HasWaitStatement()) {
            // Procedure call works as a 'wait' statement.

            // Check if this wait statement occurs inside a subprogram :
            VhdlScope *scope = (_present_scope) ? _present_scope->GetSubprogramScope() : 0 ;
            if (scope) {
                // VIPER 4518 (and 3147) : Check if this is inside a function :
                VhdlIdDef *subprog = scope->GetOwner() ;
                if (subprog && subprog->IsFunction()) {
                    from->Error("wait statement not allowed inside a function") ;
                }
            } else {
                // Check if this wait statement occurs inside a process :
                scope = (_present_scope) ? _present_scope->GetProcessScope() : 0 ;
                if (scope) {
                    // VIPER 3154 (and #2966, #3496) : Issue error if this wait statement is in a region
                    // where it is not allowed like within process with sensitivity list
                    if (scope->IsWaitStatementNotAllowed()) {
                        from->Error("process cannot have both a wait statement and a sensitivity list") ;
                    }
                }
            }
            // flag that scope (subprogram or process) as having a wait statement
            if (scope) scope->SetHasWaitStatement() ;
        }
    }

    // VIPER 3414 and 3412 : Check external access for 'pure' and 'impure' functions.
    if (IsFunction() || IsProcedure()) {
        VhdlScope *proc_local_scope = LocalScope() ;
        if (proc_local_scope && proc_local_scope->HasImpureAccess()) {
            // call to an 'impure' subprogram.
            // Check if this call is from within a calling subprogram :
            VhdlScope *scope = (_present_scope) ? _present_scope->GetSubprogramScope() : 0 ;
            // Check that this is not a 'pure' function :
            VhdlIdDef *subprog = (scope) ? scope->GetOwner() : 0 ;
            if (subprog && subprog->IsFunction()) {
                VhdlSpecification *spec = subprog->Spec() ;
                // VIPER #6969 : Do not produce error/warning for 87 mode.
                if (!IsVhdl87() && spec && !spec->IsImpure()) {
                    if (IsFunction()) {
                        // impure function
                        if (proc_local_scope->HasLocalFileAccess()) {
                            // If function becomes impure due to internal file access, produce warning to match with simulators
                            from->Warning("cannot call impure function %s from within pure function %s",Name(),subprog->Name()) ;
                        } else {
                            from->Error("cannot call impure function %s from within pure function %s",Name(),subprog->Name()) ;
                        }
                    } else {
                        // procedure with 'side effects'
                        if (proc_local_scope->HasLocalFileAccess()) {
                            // If function becomes impure due to internal file access, produce warning to match with simulators
                            from->Warning("cannot call side-effect procedure %s from within pure function %s",Name(),subprog->Name()) ;
                        } else {
                            from->Error("cannot call side-effect procedure %s from within pure function %s",Name(),subprog->Name()) ;
                        }
                    }
                }
            }

            // Flag that this one has impure access too :
            if (proc_local_scope->HasLocalFileAccess()) {
                // Has local file access (VIPER #6801)
                if (scope) scope->SetHasLocalFileAccess() ;
            } else {
                if (scope) scope->SetHasImpureAccess() ;
            }
        }
    }

    // Try to change the named associations to positional associations for functions and procedures (VIPER #2236)
    // That is very useful for many (elaboration) applications, but not essential for analyzer.
    if (IsFunction() || IsProcedure()) {
        FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
            if (has_partials || !is_named) break ; // don't try this on complex assoc lists, or on already ordered assoc lists.
            if (!assoc) continue ;
            if (!assoc->FormalPart()) continue ; // assoc is already ordered.

            formal = assoc->FindFullFormal() ;
            if (!formal) continue ; // some 'partial' name. Don't reorder.

            if (!local_scope) continue ; // errors will have been gievn earlier. (named assoc in predefined operator or so).

            // Find position of this guy in the scope
            pos = (int)(local_scope->DeclArea()->IndexOf(formal->Name())) ;

            // Get the first interface object of this interface list :
            VhdlIdDef *first_obj = (object_kind==VHDL_port) ? GetPortAt(0) : (object_kind==VHDL_generic) ? GetGenericAt(0) : Arg(0) ;
            if (!first_obj) continue ; // Something wrong with this assoc. Error should have been given already.
            // Adjust the position to the first object in the interface list :
            pos = pos - (int)(local_scope->DeclArea()->IndexOf(first_obj->Name())) ;

            if (pos < 0) continue ; // Incorrect formal was picked up. Error should have been reported already.
#if 0
            // Assert that we did not mess-up indexing in the scope.
            if (1) {
                VhdlIdDef *arg ;
                switch (object_kind) {
                case VHDL_port : arg = GetPortAt(pos) ; break ;
                case VHDL_generic : arg = GetGenericAt(pos) ; break ;
                default : arg = Arg(pos) ; break ;
                }
                // Check that the name is indeed correct.
                VERIFIC_ASSERT (arg && Strings::compare(formal->Name(), arg->Name()) ) ;
            }
#endif

            // Extend list with 0 if needed :
            while (pos >= (int)assoc_list->Size()) assoc_list->InsertLast(0) ;

            // Pick-up the assoc at that position :
            VhdlDiscreteRange *tmp_assoc = (VhdlDiscreteRange*) assoc_list->At((unsigned)pos) ;

            if ((pos < (int)i) && tmp_assoc) {
                // Since we are trying to put an association behind us in the list,
                // (where everything is 'positional') there is aparently already a
                // association at this position.
                // error is already given earlier.
                // assoc->Error("formal %s is already associated",formal->Name()) ;
            } else {
                // Insert this actual at its designated position.
                if (pos != (int)i) {
                    // If there is an existing actual there, swap with this one.
                    // Swap the one that is there with this one :
                    assoc_list->Insert(i, tmp_assoc) ;
                    // If it's not 0, try this one again.
                    if (tmp_assoc) i-- ;
                }

                // In any case, replace the named association with a positional association :

                // de-couple actual part
                actual_part = assoc->TakeActualPart() ;
                // Insert (overwrite) assoc with actual expression at the right position :
                assoc_list->Insert((unsigned)pos, actual_part) ;
                // delete rest of the assoc (basically the formal part)
                delete assoc ;
            }
        }
    }
}

// VIPER #7447 : Type infer group constituents of group declaration
void VhdlGroupTemplateId::TypeInferAssociationList(Array *assoc_list, unsigned /*object_kind*/, VhdlScope * /*actual_binding_scope*/, const VhdlTreeNode *from)
{
    if (!_decl) return ; // Cannot type infer assoc list without group template declaration

    // Get the entity class entry list from group template declaration
    Array *formal_list = _decl->GetEntityClassEntryList() ;

    if (!formal_list && !assoc_list) return ;
    unsigned formal_list_size = (formal_list) ? formal_list->Size(): 0 ;
    unsigned assoc_list_size = (assoc_list) ? assoc_list->Size(): 0 ;

    if (assoc_list_size < formal_list_size) {
        if (from) from->Error("%s group constituents in group declaration", "too few") ;
    }
    VhdlDiscreteRange *assoc ;
    VhdlEntityClassEntry *formal_entry ;
    unsigned last_entry_box = 0, this_entry_box = 0 ;
    unsigned entity_class = 0, last_entity_class = 0 ;
    VhdlIdDef *actual = 0 ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
        if (!assoc) continue ;
        formal_entry = (formal_list && (formal_list->Size()> i)) ? (VhdlEntityClassEntry*)formal_list->At(i): 0 ;
        // If entry is not present for this in group template and last one is not <>, produce error
        if (!formal_entry && !last_entry_box) {
            assoc->Error("%s group constituents in group declaration", "too many") ;
            break ;
        }
        if (formal_entry) {
            entity_class = formal_entry->GetEntityClass() ;
            this_entry_box = formal_entry->GetBox() ;
            if (this_entry_box && formal_list && (i != (formal_list->Size()-1))) {
                formal_entry->Error("entity class entry with box must be the last one within the entity class entry list") ;
            }
        } else {
            entity_class = last_entity_class ;
            this_entry_box = last_entry_box ;
        }
        // Type infer the 'assoc' to check entity class
        (void) assoc->TypeInfer(0, 0, VHDL_read, 0) ;
        actual = assoc->FindFormal() ;
        // Now check entity class of 'actual' with 'entity_class'
        if (actual && (actual->EntityClass() != entity_class)) {
            assoc->Error("group constituent %s must be %s", actual->Name(), EntityClassName(entity_class)) ;
        }
        last_entity_class = entity_class ;
        last_entry_box = this_entry_box ;
    }
}

/****************************************************************
 * Declare the operators that go with a new type declaration.
 * The operators end up in
 ****************************************************************/

// Handy routine to declare predefined operators (functions only) with 1 or two arguments
void VhdlTypeId::DeclarePredefinedOp(unsigned op, const VhdlIdDef *arg1_type, const VhdlIdDef *arg2_type, VhdlIdDef *ret_type) const
{
    if (!ret_type) return ; // operator return-type or first arg type is not know.
    unsigned is_psl_cons_rep_op = 0 ;
    if (!arg1_type && !is_psl_cons_rep_op) return ; // operator return-type or first arg type is not know.

    // Declare 1, or 2-input predefined operators.
    VhdlIdDef *oper = new VhdlOperatorId(op) ;
    Array *args = new Array(2) ;
    if (arg1_type) args->Insert(arg1_type) ;
    if (arg2_type) args->Insert(arg2_type) ;
    oper->DeclareFunction(ret_type, args, 0) ;
    _present_scope->DeclarePredefined(oper) ;
}

void VhdlTypeId::AddOperatorInterface(Array *args, char *arg_name, VhdlIdDef *type, unsigned kind, unsigned mode)
{
    if (!args || !arg_name || !type) {
        Strings::free(arg_name) ;
        return ;
    }

    VhdlInterfaceId *iface_id = new VhdlInterfaceId(arg_name) ; // consumes arg_name
    iface_id->DeclareInterfaceId(type, kind, mode, 0, 0, 0, 0, 0, 0) ;
    args->Insert(iface_id) ;

    return ;
}

void VhdlIdDef::DeclarePredefinedOper(unsigned /* op */, Array * /* args */, unsigned /* owns_arg_id */, unsigned /* is_func */) const { return ; }

void VhdlTypeId::DeclarePredefinedOper(unsigned op, Array *args, unsigned owns_arg_id, unsigned is_func) const
{
    VERIFIC_ASSERT(args) ;

    VhdlIdDef *oper = new VhdlOperatorId(op) ;
    if (is_func) {
        VhdlIdDef *ret_type = (VhdlIdDef *)args->RemoveLast() ;
        oper->DeclareFunction(ret_type, args, 0) ;
    } else {
        oper->DeclareProcedure(args, 0) ;
    }

    if (owns_arg_id) oper->SetOwnsArgId() ; // VIPER #5996
    _present_scope->DeclarePredefined(oper) ;
}

void VhdlIdDef::DeclareOperators() {}
void VhdlAnonymousType::DeclareOperators()
{
    // First, anonymous types for multi-dimensional arrays
    // These types are invented to get a type halfway in a
    // multi-dimensional aggregate. (dimensions 1..N-1) is N-dimensional array type.

    // Declare a comma operator, for strings and aggregates
    // Declare anonymous types for multi-dimensional array types
    VhdlIdDef *aggregate_element_type = AggregateElementType() ;

    DeclarePredefinedOp(VHDL_COMMA, aggregate_element_type, 0, this) ;

    // For anonymous types, this is it ; only "," operator is declared
}

void VhdlTypeId::DeclareOperators()
{
    // Declare the operators for this type
    // This is implementation of LRM 7.2

    // Declare operators for any type that is declared
    VhdlIdDef *oper ;
    Array *args ;

    // Relational operators.
    // Return BOOLEAN

    // = and /=. Defined for all types
    DeclarePredefinedOp(VHDL_EQUAL, this, this, StdType("boolean")) ;
    DeclarePredefinedOp(VHDL_NEQUAL, this, this, StdType("boolean")) ;

    // Special case : access types.
    if (IsAccessType()) {
        // LRM section 3.3.2 : procedure 'deallocate' predefined
        oper = new VhdlOperatorId(VHDL_deallocate) ;
        args = new Array(1) ;
        args->Insert(this) ;
        oper->DeclareProcedure(args, 0) ;
        _present_scope->DeclarePredefined(oper) ;

        // I'm still not sure how to actually make the API work correctly with
        // access type (right now, access type inherit pretty much all the routines
        // of their designated type.
        // Obviously, we do NOT want to create all operators for Access types
        // (you cannot multiply two access type objects?), so just declare the
        // 'deallocate' procedure (LRM 3.3.2) and don't declare any other operators.
        return ;
    }

    // Non-existing operator "," is invented to find string / aggregate
    // possible types without context info.
    // So all array types, and all record types get the "," operator
    if (IsArrayType()) {
        // Create anonymous types for multi-dimensional array types in AggregateElementType()
        VhdlIdDef *aggregate_element_type = AggregateElementType() ;

        DeclarePredefinedOp(VHDL_COMMA, aggregate_element_type, 0, this) ;
    }

    if (IsRecordType()) {
        // Arguments are stored in _list variable (which is a name->element Map
        oper = new VhdlOperatorId(VHDL_COMMA) ;
        args = new Array(NumOfElements()) ;
        VhdlIdDef *arg ;
        MapIter mi ;
        FOREACH_MAP_ITEM(_list, mi, 0, &arg) {
            args->Insert(arg->Type()) ;
        }
        oper->DeclareFunction(this, args, 0) ;
        _present_scope->DeclarePredefined(oper) ;
    }

    // Logical Operators. Defined on boolean and bit only, or
    // on one-dimensional arrays with bit or boolean as element type

    // NOTE : boolean and bit must be first types in std.standard,
    // and in this order, otherwise StdType("boolean") and StdBit will complain
    if (this == StdType("boolean") || this == StdType("bit") ||
        (IsArrayType() && Dimension()==1 &&
         (ElementType()==StdType("bit") || ElementType()==StdType("boolean")))) {
        DeclarePredefinedOp(VHDL_and, this, this, this) ;
        DeclarePredefinedOp(VHDL_or, this, this, this) ;
        DeclarePredefinedOp(VHDL_nand, this, this, this) ;
        DeclarePredefinedOp(VHDL_nor, this, this, this) ;
        DeclarePredefinedOp(VHDL_xor, this, this, this) ;
        DeclarePredefinedOp(VHDL_xnor, this, this, this) ;

        if (IsArrayType() && vhdl_file::IsVhdl2008()) {
            // logical reduction operators as per 1076-2008 LRM section 9.2.2
            DeclarePredefinedOp(VHDL_and, this, 0, ElementType()) ;
            DeclarePredefinedOp(VHDL_or, this, 0, ElementType()) ;
            DeclarePredefinedOp(VHDL_nand, this, 0, ElementType()) ;
            DeclarePredefinedOp(VHDL_nor, this, 0, ElementType()) ;
            DeclarePredefinedOp(VHDL_xor, this, 0, ElementType()) ;
            DeclarePredefinedOp(VHDL_xnor, this, 0, ElementType()) ;

            // logical operators vector 'op' scalar where the scalar
            // 'op'erates on each element of the vector. Vhdl2008: Section 9.2.2
            DeclarePredefinedOp(VHDL_and, this, ElementType(), this) ;
            DeclarePredefinedOp(VHDL_or, this, ElementType(), this) ;
            DeclarePredefinedOp(VHDL_nand, this, ElementType(), this) ;
            DeclarePredefinedOp(VHDL_nor, this, ElementType(), this) ;
            DeclarePredefinedOp(VHDL_xor, this, ElementType(), this) ;
            DeclarePredefinedOp(VHDL_xnor, this, ElementType(), this) ;

            // Vhdl2008: Section 9.2.2 scalar 'op' vector
            DeclarePredefinedOp(VHDL_and, ElementType(), this, this) ;
            DeclarePredefinedOp(VHDL_or, ElementType(), this, this) ;
            DeclarePredefinedOp(VHDL_nand, ElementType(), this, this) ;
            DeclarePredefinedOp(VHDL_nor, ElementType(), this, this) ;
            DeclarePredefinedOp(VHDL_xor, ElementType(), this, this) ;
            DeclarePredefinedOp(VHDL_xnor, ElementType(), this, this) ;
        }

        if (vhdl_file::IsVhdl2008() && this == StdType("bit")) {
            // Vhdl 1076-2008 LRM section 9.2.9 condition operator
            DeclarePredefinedOp(VHDL_condition, this, 0, StdType("boolean")) ;
        }

        DeclarePredefinedOp(VHDL_not, this, 0, this) ;
    }

    // magnitude comparators
    // Defined for scalar types and discrete array types
    // Discrete array type is array type with discrete elements
    if (IsScalarType() ||
        (IsArrayType() && (ElementType()->IsDiscreteType()))) {
        DeclarePredefinedOp(VHDL_STHAN, this, this, StdType("boolean")) ;
        DeclarePredefinedOp(VHDL_GTHAN, this, this, StdType("boolean")) ;
        DeclarePredefinedOp(VHDL_SEQUAL, this, this, StdType("boolean")) ;
        DeclarePredefinedOp(VHDL_GEQUAL, this, this, StdType("boolean")) ;

        if (vhdl_file::IsVhdl2008()) {
            // 1076-2008 LRM Section 5.3.2.4 and 5.2.6
            // function MINIMUM(L, R: T) return T ;
            DeclarePredefinedOp(VHDL_minimum, this, this, this) ;
            // function MAXIMUM(L, R: T) return T ;
            DeclarePredefinedOp(VHDL_maximum, this, this, this) ;
            if (IsArrayType()) { // 1076-2008 LRM Section 5.3.2.4
                // function MINIMUM(L: T) return E ;
                DeclarePredefinedOp(VHDL_minimum, this, 0, ElementType()) ;
                // function MAXIMUM(L: T) return E ;
                DeclarePredefinedOp(VHDL_maximum, this, 0, ElementType()) ;
            }
        }
    }

    if (vhdl_file::IsVhdl2008()) {
        if (vhdl_file::IsInsideStdPackageAnalysis()) {
            if (this == StdType("boolean") || this == StdType("bit")) {
                // built_in rising/falling_edge
                DeclarePredefinedOp(VHDL_rising_edge, this, 0, StdType("boolean")) ;
                DeclarePredefinedOp(VHDL_falling_edge, this, 0, StdType("boolean")) ;
            }
        } else { // not std.standard package
            // Viper #7160 / 7142 : 2008 LRM 5.2.6 / 5.3.2.4 / 5.7 :
            // For std.standard, to_string to be declared at the end of the package
            VhdlIdDef *this_type = this ;
            if (IsArrayType()) this_type = (Dimension()==1) ? ElementType() : 0 ;
            if (this_type && this_type->IsScalarType() && !this_type->IsAccessType()) {
                VhdlIdDef *string_type = StdType("string") ;
                if (string_type) DeclarePredefinedOp(VHDL_to_string, this, 0, string_type) ;
            }
        }
    }

    VhdlIdDef *matching_op_type = this ;
    if (IsArrayType()) matching_op_type = (Dimension()==1) ? ElementType() : 0 ;
    if (matching_op_type) matching_op_type = matching_op_type->BaseType() ;
    if (matching_op_type && !matching_op_type->IsEnumerationType()) matching_op_type = 0 ;

    unsigned needs_matching_relations = vhdl_file::IsVhdl2008() && matching_op_type && (matching_op_type == StdType("boolean") || matching_op_type == StdType("bit") || matching_op_type->IsStdULogicType()) ;  // What if user defines an enum type ulogic (does not include ieee pkg) ?

    if (needs_matching_relations) {
        // matching relational operators as per 1076-2008 LRM section 9.2.3
        // this is pre-defined only for bit, boolean, ulogic and their arrays
        DeclarePredefinedOp(VHDL_matching_equal, this, this, matching_op_type) ;
        DeclarePredefinedOp(VHDL_matching_nequal, this, this, matching_op_type) ;
    }

    if (needs_matching_relations && !IsArrayType()) {
        // matching relational operators as per 1076-2008 LRM section 9.2.3
        // this is pre-defined only for scalar bit, boolean and ieee ulogic
        DeclarePredefinedOp(VHDL_matching_gthan, this, this, this) ;
        DeclarePredefinedOp(VHDL_matching_sthan, this, this, this) ;
        DeclarePredefinedOp(VHDL_matching_gequal, this, this, this) ;
        DeclarePredefinedOp(VHDL_matching_sequal, this, this, this) ;
    }

    // Shift operators
    // Defined for one-dimensional array types of BIT or BOOLEAN
    if (IsArrayType() && Dimension()==1 &&
        (ElementType() == StdType("boolean") || ElementType() == StdType("bit"))) {
        DeclarePredefinedOp(VHDL_sll, this, StdType("integer"), this) ;
        DeclarePredefinedOp(VHDL_srl, this, StdType("integer"), this) ;
        DeclarePredefinedOp(VHDL_sla, this, StdType("integer"), this) ;
        DeclarePredefinedOp(VHDL_sra, this, StdType("integer"), this) ;
        DeclarePredefinedOp(VHDL_rol, this, StdType("integer"), this) ;
        DeclarePredefinedOp(VHDL_ror, this, StdType("integer"), this) ;
    }

    // Adding operators
    // Defined for numeric types
    if (IsIntegerType() || IsRealType() || IsPhysicalType()) {
        DeclarePredefinedOp(VHDL_PLUS, this, this, this) ; // binary plus
        DeclarePredefinedOp(VHDL_MINUS, this, this, this) ; // binary minus
        DeclarePredefinedOp(VHDL_PLUS, this, 0, this) ; // unary plus
        DeclarePredefinedOp(VHDL_MINUS, this, 0, this) ; // unary minus
        DeclarePredefinedOp(VHDL_abs, this, 0, this) ; // unary minus
    }

    // Concatenation Operator. Defined for all arrays. VIPER 2963 : only ONE-dimensional array types have an & operation (LRM 7.2.4) !!
    if (IsArrayType() && Dimension()==1) {
        VhdlIdDef *elem_type = ElementType() ;
        DeclarePredefinedOp(VHDL_AMPERSAND, this, this, this) ;
        DeclarePredefinedOp(VHDL_AMPERSAND, elem_type, this, this) ;
        DeclarePredefinedOp(VHDL_AMPERSAND, this, elem_type, this) ;
        DeclarePredefinedOp(VHDL_AMPERSAND, elem_type, elem_type, this) ;
    }

    // Multiplying operators
    // Defined for integer and real
    if (IsIntegerType() || IsRealType()) {
        DeclarePredefinedOp(VHDL_STAR, this, this, this) ;
        DeclarePredefinedOp(VHDL_SLASH, this, this, this) ;

        // Viper 8012: Vhdl 2008 LRM Section 9.5. Added three more predefined
        // operators for universal real and universal integer.
        if (IsRealType() && vhdl_file::IsVhdl2008()) {
            DeclarePredefinedOp(VHDL_STAR, this, StdType("integer"), this) ;
            DeclarePredefinedOp(VHDL_STAR, StdType("integer"), this, this) ;
            DeclarePredefinedOp(VHDL_SLASH, this, StdType("integer"), this) ;
        }

        // Exponent operator
        if (IsUniversalInteger()) {
            // Language addition for Ambit :
            // In Ambit's 'standard' package, ** is used inside the definition of StdInteger
            // So, StdInteger is not yet defined as a integer type.
            // To still support this, add a ** operator with UniversalInteger on the exponent side
            // This does not interfere with other ** operator definitions, since we would need
            // a implicit type transformation to let this one match. Operators that only match
            // with implicit transformation have lower priority than exact matches.
            DeclarePredefinedOp(VHDL_EXPONENT, this, this, this) ;
        } else {
            // normal integer or real type : define TYPE ** INTEGER -> TYPE
            DeclarePredefinedOp(VHDL_EXPONENT, this, StdType("integer"), this) ;

            // VIPER #1986: For exponent operator define: universal_integer ** INTEGER -> universal_integer
            DeclarePredefinedOp(VHDL_EXPONENT, UniversalInteger(), StdType("integer"), UniversalInteger()) ;
        }
    }

    // Physical types : arithmetics
    if (IsPhysicalType()) {
        DeclarePredefinedOp(VHDL_STAR, this, StdType("integer"), this) ; //  Type * INTEGER -> Type
        DeclarePredefinedOp(VHDL_STAR, this, StdType("real"), this) ;    //  Type * REAL -> Type
        DeclarePredefinedOp(VHDL_STAR, StdType("integer"), this, this) ; //  INTEGER * Type -> Type
        DeclarePredefinedOp(VHDL_STAR, StdType("real"), this, this) ;    //  REAL * Type -> Type

        DeclarePredefinedOp(VHDL_SLASH, this, StdType("integer"), this) ; //  Type / INTEGER -> Type
        DeclarePredefinedOp(VHDL_SLASH, this, StdType("real"), this) ; //  Type / REAL -> Type
        DeclarePredefinedOp(VHDL_SLASH, this, this, UniversalInteger()) ; // Type / Type -> UniversalInteger
    }

    // mod,rem only for integer types
    // Issue 2131 : added 'mod'/'rem' for 'real' types. is NOT LRM compliant !
    // Vhdl-2008: mod/rem also defined for physical types (LRM 9.2.7)
    if (IsIntegerType() || IsRealType() || (IsPhysicalType() && vhdl_file::IsVhdl2008())) {
        DeclarePredefinedOp(VHDL_mod, this, this, this) ;
        DeclarePredefinedOp(VHDL_rem, this, this, this) ;
    }

    // LRM 7.5 : Universal expressions : Universal types (UNIVERSAL INTEGER/REAL) have
    // a few more operators.
    if (IsUniversalReal()) {
        DeclarePredefinedOp(VHDL_STAR, this, UniversalInteger(), this) ;
        DeclarePredefinedOp(VHDL_STAR, UniversalInteger(), this, this) ;
        DeclarePredefinedOp(VHDL_SLASH, this, UniversalInteger(), this) ;
    }

    // FIX ME : Following pre-defined subprograms should get their actual arguments
    // in the args list. Not just their types.
    // This way, only order-based association will work.
    // Also, the arguments will not have a direction (mode), which hampers checks.
    // Also, their behavior (body) is not defined ! FIX.

    if (IsFileType()) {
        // LRM section 3.4.1 : various procedures pre-defined :
        // FIX ME : We have currently no way to create formals itself. We only create formal 'types'.
        // This also means that there are some problems :
        //   - named associations are not possible
        //   - formals have no recognizable 'mode'.
        //   - formals cannot have initial values, so we need to create one for each permutation.

        // Viper #3698 : implicit procedures on file type must carry the
        // argument names, otherwise, procedure call with named association
        // will fail type_infer. TODO : Add proper spec/scope to such procedure
        // so that during type_infer argument lookup by name would be constant
        // time operation

        // file_open (TYPE, string)

        args = new Array(3) ;
        AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("external_name"), StdType("string"), 0, VHDL_in) ;
        DeclarePredefinedOper(VHDL_file_open, args, 1, 0) ;

        // file_open (TYPE, string, file_open_kind)
        args = new Array(3) ;
        AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("external_name"), StdType("string"), 0, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("open_kind"), StdType("file_open_kind"), 0, VHDL_in) ;
        DeclarePredefinedOper(VHDL_file_open, args, 1, 0) ;

        // file_open (file_open_status, TYPE, string, file_open_kind)
        args = new Array(4) ;
        AddOperatorInterface(args, Strings::save("status"), StdType("file_open_status"), 0, VHDL_out) ;
        AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("external_name"), StdType("string"), 0, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("open_kind"), StdType("file_open_kind"), 0, VHDL_in) ;
        DeclarePredefinedOper(VHDL_file_open, args, 1, 0) ;

        // file_open (file_open_status, TYPE, string)
        args = new Array(3) ;
        AddOperatorInterface(args, Strings::save("status"), StdType("file_open_status"), 0, VHDL_out) ;
        AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("external_name"), StdType("string"), 0, VHDL_in) ;
        DeclarePredefinedOper(VHDL_file_open, args, 1, 0) ;

        // file_close(TYPE)
        args = new Array(1) ;
// Viper #5907 : begin
        // args->Insert(this) ;
        AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
// Viper #5907 : end
        DeclarePredefinedOper(VHDL_file_close, args, 1, 0) ;

        // flush(TYPE)
        if (vhdl_file::IsVhdl2008()) {
            args = new Array(1) ;
            AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
            DeclarePredefinedOper(VHDL_flush, args, 1, 0) ;
        }

        // VIPER Issue #6510 : create the in/out interface-ids for proper var-usage
        args = new Array() ;
        AddOperatorInterface(args, Strings::save("f"), this, VHDL_file, VHDL_in) ;
        AddOperatorInterface(args, Strings::save("value"), DesignatedType(), 0, VHDL_out) ;

        if (DesignatedType() && DesignatedType()->IsUnconstrainedArrayType()) {
            // 'read' for file of unconstrained array type :
            // VIPER Issue #6510
            AddOperatorInterface(args, Strings::save("length"), StdType("integer"), 0, VHDL_out) ;
        }
        DeclarePredefinedOper(VHDL_read, args, 1, 0) ;

        args = new Array(2) ;
        args->Insert(this) ;
        args->Insert(DesignatedType()) ;
        DeclarePredefinedOper(VHDL_write, args, 0, 0) ;

        DeclarePredefinedOp(VHDL_endfile, this, 0, StdType("boolean")) ;
    }
}

VhdlIdDef *
VhdlIdDef::AggregateElementType() const { return 0 ; }
VhdlIdDef *
VhdlTypeId::AggregateElementType() const
{
    // For Multidimensional Array types, declare an anonimous type for each dimension.
    // That type only gets a , operator (aggregate) defined.
    // This is to support types at each aggregate node, so we can do normal type-inference
    if (Dimension()<=1) return ElementType() ;

    // Declare a anonymous type
    // Needs to have a unique name, or the scope will not accept it
    VhdlTypeId *type = new VhdlAnonymousType(Strings::save(Name()," ")) ;

    // As index types, declare all index types except for the MSB (left - index 0) index type
    Map *index_types = new Map(STRING_HASH) ;

    // Skip first one (IndexType(0))
    for (unsigned i=1; i<Dimension(); i++) {
        VhdlIdDef *index_type = IndexType(i) ;
        if (index_type) (void) index_types->Insert(index_type->Name(), index_type, 0, 1) ;
    }

    if (IsUnconstrainedArrayType()) {
        type->DeclareUnconstrainedArrayType(index_types,ElementType(), -1, -1) ;
    } else {
        type->DeclareConstrainedArrayType(index_types,ElementType(), -1, -1) ;
    }
    _present_scope->DeclarePredefined(type) ;
    return type ;
}

// Set/Get Subprogram spec
void VhdlIdDef::SetSpec(VhdlSpecification * /*spec*/)       { VERIFIC_ASSERT(0) ; }
VhdlSpecification *VhdlIdDef::Spec() const                  { return 0 ; }

void VhdlSubprogramId::SetSpec(VhdlSpecification *spec)     { _spec = spec ; }
VhdlSpecification *VhdlSubprogramId::Spec() const           { return _spec ; }

// Set/Get Subprogram body
void VhdlIdDef::SetBody(VhdlSubprogramBody * /*body*/)      { VERIFIC_ASSERT(0) ; }
VhdlSubprogramBody *VhdlIdDef::Body() const                 { return 0 ; }

void VhdlSubprogramId::SetBody(VhdlSubprogramBody *body)    { _body = body ; }
VhdlSubprogramBody *VhdlSubprogramId::Body() const
{
    if (_body) return _body ;
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog) return uninstantiated_subprog->Body() ;
    return 0 ;
}

// Set/get Subprog instance
void VhdlIdDef::SetSubprogInstanceDecl(VhdlDeclaration * /*inst*/) {}
void VhdlSubprogramId::SetSubprogInstanceDecl(VhdlDeclaration *inst) { _sub_inst = inst ; }
VhdlDeclaration *VhdlIdDef::GetSubprogInstanceDecl() const { return 0 ; }
VhdlDeclaration *VhdlSubprogramId::GetSubprogInstanceDecl() const { return _sub_inst ; }

// Set/get actual subprog id
void VhdlIdDef::SetActualId(VhdlIdDef * /*id*/, Array * /*assoc_lis*/) {}
void VhdlSubprogramId::SetActualId(VhdlIdDef *id, Array * /*assoc_lis*/)
{
    if (_sub_inst) _sub_inst->SetActualId(id) ;
}
void VhdlGenericTypeId::SetActualId(VhdlIdDef *id, Array* assoc_list)
{
    // Set actual only for generic type
    // Viper 7505: assoc_list is required so that we know that the type is constrained or unconstrained
    // This information cannot be obtained from the _type.
    _type = id ;
    if (id && id->IsType()) {
        VhdlTypeId* type_id = id->TypeCast() ;
        _type_enum = type_id ? type_id->GetTypeEnum() :  _type_enum ;
        if (assoc_list && assoc_list->Size() && (id->IsUnconstrainedArrayType() || id->IsUnconstrained())) {
            // Prefix is unconstrained array type. Check whether constraint is specified
            // by range
            VhdlDiscreteRange *range ;
            unsigned i ;
            unsigned is_unconstrained = 0 ;
            FOREACH_ARRAY_ITEM(assoc_list, i, range) {
                if (!range) continue ;
                // If any dimension is unconstrained, return 1
                if (range->IsUnconstrained(0)) {
                    is_unconstrained = 1 ;
                    break ;
                }
            }
            if (!is_unconstrained) _type_enum = CONSTRAINED_ARRAY_TYPE ;
        }
    }
}
VhdlIdDef *VhdlIdDef::GetActualId() const { return 0 ; }
VhdlIdDef *VhdlSubprogramId::GetActualId() const { return _sub_inst ? _sub_inst->GetActualId(): 0 ; }
VhdlIdDef *VhdlTypeId::GetActualId() const { return ( IsGenericType()) ? _type : 0 ; }
VhdlIdDef *VhdlIdDef::TakeActualId() { return 0 ; }
VhdlIdDef *VhdlSubprogramId::TakeActualId() { return _sub_inst ? _sub_inst->TakeActualId(): 0 ; }
VhdlIdDef *VhdlTypeId::TakeActualId()
{
    if (IsGenericType()) {
        VhdlIdDef *r = _type ;
        _type = 0 ;
        return r ;
    } else {
        return 0 ;
    }
}

// Set/Get (Deferred) Constant declaration
void VhdlIdDef::SetDecl(VhdlIdDef * /*decl*/)               { VERIFIC_ASSERT(0) ; }
VhdlIdDef *VhdlIdDef::Decl() const                          { return 0 ; }

void VhdlConstantId::SetDecl(VhdlIdDef *decl)               { _decl = decl ; }
VhdlIdDef *VhdlConstantId::Decl() const                     { return _decl ; }

void VhdlIdDef::SetLoopIterator()                           { }
void VhdlConstantId::SetLoopIterator()                      { _is_seq_loop_iter = 1 ; }

/**********************************************************/
//  Bit Value encoding
/**********************************************************/

void VhdlIdDef::SetBitEncoding(char /*b*/) { /*VERIFIC_ASSERT?*/ }
void VhdlEnumerationId::SetBitEncoding(char b)
{
    // Expect a character value in 'value' that identifies this
    // enumeration literal as 0,1,x or z.
    // These are the characters used in the synthesis directives
    // for bit-encoded enumerated types

    // Delete multi-bit encoding if it had this
    Strings::free(_encoding) ;
    _encoding = 0 ;
    _onehot_encoded = 0 ;

    // Now set the bit encoding
    switch (::toupper(b)) {
        // upper case ; normal characters
        case '0' : _bit_encoding = '0' ; break ;
        case '1' : _bit_encoding = '1' ; break ;
        case 'D' : _bit_encoding = 'X' ; break ;
        case 'H' : _bit_encoding = '1' ; break ;
        case 'L' : _bit_encoding = '0' ; break ;
        case 'U' : _bit_encoding = 'X' ; break ;
        case 'X' : _bit_encoding = 'X' ; break ;
        case 'Z' : _bit_encoding = 'Z' ; break ;
        case '-' : _bit_encoding = 'X' ; break ;
        default : // unknown character
        {
            Warning("unknown character '%c' in enumeration encoding is considered 'X'", b) ;
            _bit_encoding = 'X' ;
        }
        break ;
    }
}

char VhdlIdDef::GetBitEncodingForElab() const
{
//    if (IsStaticElab() && IsUserEncoded()) return 0 ;
    char c = GetBitEncoding() ;
    if (!c) return 0 ;
    // VIPER 7346, 7182 : Check if this id has a synthesis directive and check run-time flag to ignore directives, before returning bit-encoding.
    if (IsUserEncoded() && RuntimeFlags::GetVar("vhdl_ignore_synthesis_directives")) return 0 ;
    return c ;
}

char VhdlIdDef::GetBitEncoding() const         { return 0 ; }
char VhdlEnumerationId::GetBitEncoding() const { return (char)_bit_encoding ; }

/**********************************************************/
//  Multi-Bit Value encoding
/**********************************************************/

void VhdlIdDef::SetEncoding(const char * /*encoding*/) { /*VERIFIC_ASSERT?*/ }
void VhdlEnumerationId::SetEncoding(const char *encoding)
{
    // Expect a string of characters in 'encoding' that identifies this
    // enumeration literal's enumeration value in 0,1,x or z bits.
    // These are the characters used in the synthesis directives
    // for bit-encoded enumerated types

    // delete previous encoding (if there)
    _bit_encoding = 0 ;
    _onehot_encoded = 0 ;
    Strings::free( _encoding );
    // We can still set user encoding even if one_hot flag is set.. _onehot_encoded = 0 ;

    // Create the new encoding :
    _encoding = Strings::save(encoding) ;
    if (!_encoding) return ; // encoding was re-set

    // Now translate the characters from synthesis-directive values to Z,X,0 or 1
    for (char *tmp = _encoding; *tmp ; tmp++) {
        switch (::toupper(*tmp)) {
            // upper case ; normal characters
        case '0' : *tmp = '0' ; break ;
        case '1' : *tmp = '1' ; break ;
        case 'D' : *tmp = 'X' ; break ;
        case 'H' : *tmp = '1' ; break ;
        case 'L' : *tmp = '0' ; break ;
        case 'U' : *tmp = 'X' ; break ;
        case 'X' : *tmp = 'X' ; break ;
        case 'Z' : *tmp = 'Z' ; break ;
        case '-' : *tmp = 'x' ; break ; // one-hot style don't care.
        default : // unknown character
            {
                Warning("unknown character '%c' in enumeration encoding is considered 'X'", *tmp) ;
                *tmp = 'X' ;
            }
            break ;
        }
    }
}

const char *VhdlIdDef::GetEncodingForElab() const
{
//    if (IsStaticElab()) return 0 ;
    const char *encoding = GetEncoding() ;
    if (!encoding) return 0 ;
    // VIPER 7346, 7182 : Check if this id has a synthesis directive and check run-time flag to ignore directives, before returning bit-encoding.
    if (IsUserEncoded() && RuntimeFlags::GetVar("vhdl_ignore_synthesis_directives")) return 0 ;
    return encoding ;
}
const char *VhdlIdDef::GetEncoding() const                  { return 0 ; }
const char *VhdlEnumerationId::GetEncoding() const          { return _encoding ; }

// Onehot encoding :
void VhdlIdDef::SetOnehotEncoded()                          { /*VERIFIC_ASSERT?*/ }
void VhdlEnumerationId::SetOnehotEncoded()                  { _onehot_encoded = 1 ; }

unsigned VhdlIdDef::IsOnehotEncoded() const                 { return 0 ; }
unsigned VhdlEnumerationId::IsOnehotEncoded() const         { return (_onehot_encoded) ? 1 : 0 ; }

// Viper #7182
void VhdlIdDef::SetUserEncoded()                            { /*VERIFIC_ASSERT?*/ }
void VhdlEnumerationId::SetUserEncoded()                    { _enum_encoded = 1 ; }

// Viper #7182
unsigned VhdlIdDef::IsUserEncoded() const                   { return 0 ; }
unsigned VhdlEnumerationId::IsUserEncoded() const           { return (_enum_encoded) ? 1 : 0 ; }

/**************************************************************/
// Pragma's on subprograms
/**************************************************************/

void VhdlIdDef::SetPragmaFunction(unsigned /*function*/)    { /*VERIFIC_ASSERT?*/ }
void VhdlIdDef::SetPragmaSigned()                           { /*VERIFIC_ASSERT?*/ }
void VhdlIdDef::SetPragmaLabelAppliesTo()                   { /*VERIFIC_ASSERT?*/ }
void VhdlSubprogramId::SetPragmaFunction(unsigned function) { _pragma_function = function ; }
void VhdlSubprogramId::SetPragmaSigned()                    { _pragma_signed = 1 ; }
void VhdlSubprogramId::SetPragmaLabelAppliesTo()            { _is_label_applies_to_pragma = 1 ; }

unsigned VhdlIdDef::GetPragmaFunction() const               { return 0 ; }
unsigned VhdlIdDef::GetPragmaSigned() const                 { return 0 ; }
unsigned VhdlIdDef::GetPragmaLabelAppliesTo() const         { return 0 ; }
unsigned VhdlSubprogramId::GetPragmaFunction() const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog && !_pragma_function) return uninstantiated_subprog->GetPragmaFunction() ;

    return _pragma_function ;
}
unsigned VhdlSubprogramId::GetPragmaSigned() const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog && !_pragma_signed) return uninstantiated_subprog->GetPragmaSigned() ;

    return _pragma_signed ;
}
unsigned VhdlSubprogramId::GetPragmaLabelAppliesTo() const
{
    // VIPER #5918 : VHDL-2008 enhancement : If this is a subprogram instance,
    // information regarding type and others should be taken from uninstantiated
    // subprogram id
    VhdlIdDef *uninstantiated_subprog = (_sub_inst) ? _sub_inst->GetUninstantiatedSubprogId(): 0 ;
    if (uninstantiated_subprog && !_is_label_applies_to_pragma) return uninstantiated_subprog->GetPragmaLabelAppliesTo() ;

    return _is_label_applies_to_pragma ;
}

void
VhdlIdDef::CheckPragmaSetting()
{
    unsigned pragma_func = GetPragmaFunction() ;
    if (!pragma_func) return ;

    // Exception case : IGNORE_SUBPROGRAM takes any number of arguments.
    if (pragma_func==VHDL_IGNORE_SUBPROGRAM) return ;

    // Get the first two argument types of the subprogram :
    VhdlIdDef *arg_type1 = ArgType(0) ;
    VhdlIdDef *arg_type2 = ArgType(1) ;

    // Get the return type :
    VhdlIdDef *ret_type = Type() ;
    if (!ret_type) return ;

    // Calculate required number of arguments :
    unsigned num_args = NumOfArgs() ;
    // If second and later arguments is optional, it's probably not important for the directive.
    // Assume we will have to do without. This is a sanity check for the user only (issue 1224)
    for (unsigned i=num_args-1;i>0;i--) {
        if (Arg(i) && Arg(i)->HasInitAssign()) num_args-- ;
    }

    // Catch pragma's on procedures :
    // We have 4 such (textio) procedure pragmas : for 'read' 'write' 'readline' and 'writeline'
    if (IsProcedure()) {
        switch (pragma_func) {
        case VHDL_sread :
            if (num_args != 3) {
                Warning("%s expects %d arguments", OperatorName(pragma_func), 3) ;
                SetPragmaFunction(0) ;
            }

        case VHDL_read :
        case VHDL_write :
        case VHDL_hread :
        case VHDL_hwrite :
        case VHDL_oread :
        case VHDL_owrite :
            // Check argument types :
            // read and write should have two or three arguments :
            if (num_args<2 || num_args>3) {
                Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
                SetPragmaFunction(0) ;
            }
            // first argument of 'read' and 'write' is an access type argument
            if (arg_type1 && !arg_type1->IsAccessType()) {
                Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type1->OrigName()) ;
                SetPragmaFunction(0) ;
            }
            // second argument is the 'value' : should be a scalar or 1-dimensional array :
            if (arg_type2 && (!arg_type2->IsScalarType() && arg_type2->Dimension()!=1)) {
                Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type2->OrigName()) ;
                SetPragmaFunction(0) ;
            }
            return ; // done testing this procedure pragma

        case VHDL_readline :
        case VHDL_writeline :
            // read and write should have two arguments :
            if (num_args!=2) {
                Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
                SetPragmaFunction(0) ;
            }
            // first argument of readline and writeline is a FILE.
            if (arg_type1 && !arg_type1->IsFileType()) {
                Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type1->OrigName()) ;
                SetPragmaFunction(0) ;
            }
            // second argument of 'read' and 'write' is an access type argument
            if (arg_type2 && !arg_type2->IsAccessType()) {
                Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type2->OrigName()) ;
                SetPragmaFunction(0) ;
            }
            return ; // done testing this procedure pragma

        default :
            break ; // unknown procedure pragma.
        }
    }

    // Pragma on a function
    if (!IsFunction()) {
        Warning("directive %s is only appropriate for functions", OperatorName(pragma_func)) ;
        SetPragmaFunction(0) ;
        return ;
    }

    // We can only handle 1-dimensional arrays, scalars in or out :
    if (arg_type1 && (!arg_type1->IsScalarType() && arg_type1->Dimension()!=1)) {
        Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type1->OrigName()) ;
        SetPragmaFunction(0) ;
        return ;
    }
    if (arg_type2 && (!arg_type2->IsScalarType() && arg_type2->Dimension()!=1)) {
        Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type2->OrigName()) ;
        SetPragmaFunction(0) ;
        return ;
    }
    if (!ret_type->IsScalarType() && (ret_type->Dimension() != 1)) {
        Warning("directive %s not supported on type %s", OperatorName(pragma_func), ret_type->OrigName()) ;
        SetPragmaFunction(0) ;
        return ;
    }

    // Special case : pragma VHDL_MINUS onm function with one argument is interpreted as a VHDL_UMINUS :
    if (pragma_func==VHDL_MINUS && num_args==1) {
        // A "-" on a function with a single argument is intepreted as a UMINUS.
        // Happens for Ambit built-in operators.
        pragma_func = VHDL_UMINUS ;
        SetPragmaFunction(pragma_func) ;
    }

    // Here, there is a pragma set for this subprogram.
    // Check number of arguments :
    switch (pragma_func) {
    // Operators with exactly one argument :
    case VHDL_UMINUS :
    case VHDL_abs :
        // reduction operators :
    case VHDL_REDAND :
    case VHDL_REDNAND :
    case VHDL_REDOR :
    case VHDL_REDNOR :
    case VHDL_REDXOR :
    case VHDL_REDXNOR :
        // not, buf
    case VHDL_not :
    case VHDL_buf :
        // special cases :
//CARBON_BEGIN
    case VHDL_resize:
    case VHDL_uns_resize :
    case VHDL_signed_mult :
    case VHDL_unsigned_mult :
    case VHDL_numeric_signed_mult:
    case VHDL_numeric_unsigned_mult :
    case VHDL_numeric_sign_div :
    case VHDL_numeric_uns_div :
    case VHDL_numeric_minus :
    case VHDL_numeric_unary_minus:
    case VHDL_numeric_plus:
    case VHDL_is_uns_less:
    case VHDL_is_uns_gt :
    case VHDL_is_gt :
    case VHDL_is_less:
    case VHDL_is_uns_gt_or_equal:
    case VHDL_is_uns_less_or_equal:
    case VHDL_is_gt_or_equal :
    case VHDL_is_less_or_equal:
    case VHDL_and_reduce :
    case VHDL_or_reduce:
    case VHDL_xor_reduce:
    case VHDL_numeric_mod:
    case VHDL_i_to_s:
    case VHDL_s_to_u :
    case VHDL_s_to_i :
    case VHDL_u_to_i :
    case VHDL_u_to_u :
    case VHDL_s_xt :
//CARBON_END
    case VHDL_WIRED_THREE_STATE :
    case VHDL_PULLUP :
    case VHDL_PULLDOWN :
    case VHDL_TRSTMEM :
    case VHDL_falling_edge :
    case VHDL_rising_edge :
    case VHDL_readline :
    case VHDL_writeline :
        if (num_args!=1) {
            Warning("directive %s expects one argument", OperatorName(pragma_func)) ;
            if (!arg_type1) SetPragmaFunction(0) ;
        }
        break ;

    // Operators with exactly two arguments :
    case VHDL_MINUS :
    case VHDL_PLUS :
    case VHDL_STAR :
    case VHDL_SLASH :
    case VHDL_mod :
    case VHDL_rem :
    case VHDL_EXPONENT :
        // shifters/rotators :
    case VHDL_sll :
    case VHDL_srl :
    case VHDL_sla :
    case VHDL_sra :
    case VHDL_rol :
    case VHDL_ror :
        // comparators :
    case VHDL_EQUAL :
    case VHDL_NEQUAL:
    case VHDL_STHAN :
    case VHDL_GTHAN :
    case VHDL_SEQUAL:
    case VHDL_GEQUAL:
        if (num_args!=2) {
            Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
            if (!arg_type2) SetPragmaFunction(0) ;
        }
        break ;
        // binary opers and logical reduction operators (with 1076-2008 LRM 9.2.2)
    case VHDL_and :
    case VHDL_nand :
    case VHDL_or :
    case VHDL_nor :
    case VHDL_xor :
    case VHDL_xnor :
    {
        unsigned is_bad = num_args!=2 ;
        if (vhdl_file::IsVhdl2008()) is_bad = num_args != 1 && num_args != 2 ;
        if (is_bad) {
            Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
            SetPragmaFunction(0) ;
        }
        break ;
    }
    // Special cases :
    case VHDL_FEEDTHROUGH :
        // Works with at least one argument
        if (num_args==0) {
            Warning("directive %s expects one argument", OperatorName(pragma_func)) ;
            if (!arg_type1) SetPragmaFunction(0) ;
        }
        break ;
    case VHDL_read :
    case VHDL_write :
    case VHDL_hread :
    case VHDL_hwrite :
    case VHDL_oread :
    case VHDL_owrite :
        // textio functions : 2 or 3 or 4 arguments :
        if (num_args<2 || num_args>4) {
            Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
            if (!arg_type2) SetPragmaFunction(0) ;
        }
        break ;

    // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
    case VHDL_math_ceil:
    case VHDL_math_floor:
    case VHDL_math_sqrt:
    case VHDL_math_cbrt:
    case VHDL_math_exp:
    case VHDL_math_log:
    case VHDL_math_log2:
    case VHDL_math_log10:
    case VHDL_math_sin:
    case VHDL_math_cos:
    case VHDL_math_tan:
    case VHDL_math_asin:
    case VHDL_math_acos:
    case VHDL_math_atan:
    case VHDL_math_sinh:
    case VHDL_math_cosh:
    case VHDL_math_tanh:
    case VHDL_math_asinh:
    case VHDL_math_acosh:
    case VHDL_math_atanh:
        // one argument, type real, returning real
        if (num_args!=1) {
            Warning("directive %s expects one argument", OperatorName(pragma_func)) ;
            if (!arg_type1) SetPragmaFunction(0) ;
        }

        if (arg_type1 && !arg_type1->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type1->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        if (!ret_type->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), ret_type->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        break ;
    case VHDL_math_mod:
    case VHDL_math_atan2:
        // two arguments, type real, returning real
        if (num_args!=2) {
            Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
            if (!arg_type1) SetPragmaFunction(0) ;
        }

        if (arg_type1 && !arg_type1->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type1->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        if (arg_type2 && !arg_type2->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type2->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        if (!ret_type->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), ret_type->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        break ;

    case VHDL_math_pow:
        // two arguments, type integer,real or real,real, returning real
        if (num_args!=2) {
            Warning("directive %s expects two arguments", OperatorName(pragma_func)) ;
            if (!arg_type1) SetPragmaFunction(0) ;
        }

        if (arg_type1 && !arg_type1->IsRealType() && !arg_type1->IsIntegerType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type1->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        if (arg_type2 && !arg_type2->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), arg_type2->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        if (!ret_type->IsRealType()) {
            Warning("directive %s not supported on type %s", OperatorName(pragma_func), ret_type->OrigName()) ;
            SetPragmaFunction(0) ;
        }

        break ;
    default :
        // Should never happen if we were complete in the hash table. So error out.
        Error("cannot execute unknown pragma for function %s",OrigName()) ;
        SetPragmaFunction(0) ;
        break ;
    }

    // Final check if we ignored the pragma :
    if (!GetPragmaFunction()) {
        // We apparently destroyed the thing.
        Warning("directive %s on function %s ignored", OperatorName(pragma_func), OrigName()) ;
    }
}

//CARBON_BEGIN
/*
Register the pragma with the signal declaration
-- make a pair for the name /value
-- store in the _pragmas array.
*/
void
VhdlIdDef::RegisterPragma(char* name, char* value){
  if (!_pragmas)
    _pragmas = new Map(POINTER_HASH);
  char* value_copied = value ? Strings::save(value):NULL;
  char* key = Strings::save(name);
  _pragmas -> Insert(key,value_copied);
}

/*
  Delete the pragmas
*/
void VhdlIdDef::RemovePragmas()
{
    // delete if _pragmas exist
    if (_pragmas) {
        char * name;
        char * val;
        Verific::MapIter mi;
        FOREACH_MAP_ITEM(_pragmas, mi, &name, &val) {
            if (name) Verific::Strings::free(name);
            if (val) Verific::Strings::free(val);
        }
        delete _pragmas;
    }
}

void VhdlIdDef::CopyPragmas(const VhdlIdDef& node)
{
    if (node.gPragmas()) {
        char * name;
        char * value;
        _pragmas = new Map(POINTER_HASH);
        Verific::MapIter mi;
        FOREACH_MAP_ITEM(node.gPragmas(), mi, &name, &value) {
            char* value_copied = value ? Strings::save(value):NULL;
            char* key = Strings::save(name);
            _pragmas -> Insert(key,value_copied);
        }
    }
}
//CARBON_END


/**************************************************************/
// Define an attribute
/**************************************************************/

unsigned
VhdlIdDef::SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_expr)
{
    // NOTE: we assume *id to be a VhdlAttributeId!
    if (!_attributes) _attributes = new Map(POINTER_HASH) ;

    // Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
    unsigned is_foreign = attr_id && Strings::compare_nocase(attr_id->Name(), "foreign") ;
    const char *func_name = (is_foreign && attr_expr) ? attr_expr->Name() : 0 ;
    if (func_name) {
        // Find the function pragma that goes with that
        unsigned func = PragmaFunction(func_name) ;
        if (!func) {
            // attr_expr->Warning("unknown foreign directive %s",func_name) ;
        } else {
            SetPragmaFunction(func) ;
        }
    }

    // Set attribute_id->expression association into this id's attribute table :
    // NOTE : before 4/2007, we had a VhdlValue (elaboration class) here, valid only at elaboration time.
    // but the expression is much more important that the value, and is available at analysis time.
    // Set right here.
    return _attributes->Insert(attr_id, attr_expr) ;
}

unsigned
VhdlAliasId::SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_expr)
{
    if (_target_id) {
        return _target_id->SetAttribute(attr_id, attr_expr) ;
    } else {
        return VhdlIdDef::SetAttribute(attr_id, attr_expr) ;
    }
}
unsigned
VhdlTypeId::SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_expr)
{
    // Set attribute (with its value expression) on a type identifier :

    // Special : encoding from the 'synthesis' attributes could go here.
    // This code used to be done in TypeId::SetAttribute() at elaboration time.
    // Now we can do this at analysis time.

    // Viper #3799 : Process the enum_encoding attribute right here during analysis
    // CAUTION : TypeDecl::Elaborate might be a better place to evaluate the enum_attribute
    // expression and set encoding for the enum states... This is because attribute_expression
    // may involve generics and thus can only be evaluated during elaboration.
    VhdlIdDef *elem ;
    unsigned i ;
    if (IsEnumerationType() && attr_id && attr_expr) {
        char *attr_string = attr_expr->ExprString() ; // Viper #7042
        // const char *attr_string = attr_expr->Name() ;
        if (attr_string && Strings::compare_nocase(attr_id->Name(), "enum_encoding")) {
            // Synopsys uses 'enum_encoding' for BOTH bit-encoding as well as
            // multi-bit (state) encoding.

            // Keep track of the number of enum fields counted in the attribute value
            unsigned elem_nr = 0 ;
            // allocate enough space for at least one element value (total string length should do)
            unsigned long expr_len = Strings::len(attr_string) ;
            char *buffer = Strings::allocate(expr_len+1) ;
            // Set a pointer that will fill with value of one element
            char *buffer_ptr = buffer ;
            buffer[0] = '\0' ; // Start clean
            // keep track of size of element values
            unsigned long size = 0 ;

            // Now run over the characters in the value
            for (i = 0 ; i < expr_len+1 ; i++) {
                // We go one character further than the string length,
                // to handle the last element too (mimic that there is a space at the end of the string)
                char elem_bit_val = (i < expr_len) ? attr_string[i] : 0 ; // element value in the string
                if (!elem_bit_val || elem_bit_val == ' ' || elem_bit_val == '"') {
                    if (buffer_ptr == buffer) {
                        // previous character was a space skip this
                        continue ;
                    }

                    // We are about to go to the next element
                    // deal with the previous one ; close the buffer :
                    *buffer_ptr = '\0' ;
                    // Test for correct length
                    if (size == 0) {
                        size = Strings::len(buffer) ;
                    } else {
                        if (size != Strings::len(buffer)) {
                            Warning("enum_encoding elements should all have the same length") ;
                            break ;
                        }
                    }

                    // Get the enumeration literal to assign this value to
                    elem = GetEnumAt(elem_nr++) ; // my element number
                    if (!elem) {
                        // Warning("enum_encoding attribute specifies too many literals for type %s",Name()) ;
                        break ;
                    }

                    // Assign encoding : bit or multi-bit
                    if (size==1) {
                        elem->SetBitEncoding(*buffer) ;
                    } else {
                        elem->SetEncoding(buffer) ;
                    }

                    elem->SetUserEncoded() ; // Flag this enum literal as 'user' encoded. Viper #7182/7354

                    // go to next element
                    buffer_ptr = buffer ;
                } else {
                    // next character in the same element
                    *buffer_ptr++ = elem_bit_val ;
                }
            }

            if (elem_nr != NumOfEnums()) {
                Warning("enum_encoding attribute specifies incorrect number of valid encoding values for type %s ; encoding ignored", Name()) ;
                // Kill off all encoding (we will default to binary)
                for (i = 0 ; i < NumOfEnums() ; i++) {
                    elem = GetEnumAt(i) ;
                    if (elem) elem->SetEncoding(0) ;
                }
            }

            Strings::free(buffer) ;
#if 0
            // logic_type_encoding is taken out of the analysis.. If customers use old
            // VDB of std package, since they wont have the encoding bit set, std_logic
            // ulogic will be treated as 4 bit, will definitely cause regressions

            // So we prefer to still do this during elaborate
        } else if (attr_expr->IsAggregate() && Strings::compare_nocase(attr_id->Name(), "logic_type_encoding")) {
            // Exemplar way of encoding a bit
            // Exemplar encodes bit-values in a clean string. Length should fit
            // with the number of enum values :
            if (NumOfEnums() != attr_expr->NumElements()) {
                Warning("synthesis directive attribute %s ignored ; incorrect number of bit values for type %s", attr_id->Name(), Name()) ;
            } else {
                // Now set the elements
                unsigned one_occured=0 , zero_occured=0 ;
                for (i = 0 ; i < attr_expr->NumElements() ; i++) {
                    VhdlExpression const *elem_i = attr_expr->GetElementExpressionAt(i) ; // element value in the string
                    if (!elem_i) continue ; // Error ?

                    VhdlIdDef *elem_id = elem_i->GetId() ;
                    if (!elem_id) continue ; // Error ?

                    char c = (char)elem_id->Position() ;

                    elem = GetEnumAt(i) ; // Get my value at this number
                    VERIFIC_ASSERT(elem) ; // We tested right number above..

                    // VIPER 1751: Changed for metalogical values
                    // If logic_type_encoding uses '0' or '1' for more than
                    // one enum literal then we assume only the first one as
                    // real '0' or '1', all the rest ones are considered as
                    // 'L' or 'H'
                    switch (c) {
                    case '0' :
                        if (zero_occured) {
                            c = 'L' ;
                        } else {
                            zero_occured=1 ;
                        }
                        break ;
                    case '1' :
                        if (one_occured) {
                            c = 'H' ;
                        } else {
                            one_occured=1 ;
                        }
                        break ;
                    default: break ;
                    }
                    elem->SetBitEncoding(c) ;

                    elem->SetUserEncoded() ; // Flag this enum literal as 'user' encoded. Viper #7182/7354
                }
            }
#endif
        }

        Strings::free(attr_string) ;
    }
    // End Viper #3799 : Process the enum_encoding attribute right here during analysis

    // Go through the normal VhdlIdDef SetAttribute
    return VhdlIdDef::SetAttribute(attr_id, attr_expr) ;
}

VhdlExpression *
VhdlIdDef::GetAttribute(const VhdlIdDef *attr) const
{
    // Get the expression of an attribute on this id (or null if the id has no such attribute set).
    if (!_attributes || !attr) return 0 ;
    return (VhdlExpression*)_attributes->GetValue(attr) ;
}

VhdlExpression *
VhdlAliasId::GetAttribute(const VhdlIdDef *attr) const
{
    return (_target_id) ? _target_id->GetAttribute(attr) : VhdlIdDef::GetAttribute(attr) ;
}
unsigned
VhdlIdDef::NumOfAttributes() const
{
    return (_attributes) ? _attributes->Size() : 0 ;
}
unsigned
VhdlAliasId::NumOfAttributes() const
{
    return (_target_id) ? _target_id->NumOfAttributes(): VhdlIdDef::NumOfAttributes() ;
}

VhdlExpression *
VhdlIdDef::GetAttribute(const char *attr_name) const
{
    if (!_attributes) return 0 ;
    // Get the expression of an attribute on this id. Using only the name of the attribute.
    // Since the attributes table is a POINTER_HASH on the VhdlIdDef of the attribute,
    // we need to walk every entry of the table.
    // This is still fast, especially since hardly any objects have more than one attribute.
    MapIter mi ;
    VhdlIdDef *attr_id ;
    VhdlExpression *attr_expr ;
    FOREACH_MAP_ITEM(_attributes, mi, &attr_id, &attr_expr) {
        if (!attr_id) continue ;
        // Do case-insensitive string compare, since VHDL identifiers are case-insensitive.
        if (Strings::compare_nocase(attr_id->Name(),attr_name)) {
            return attr_expr ;
        }
    }
    return 0 ;
}
VhdlExpression *
VhdlAliasId::GetAttribute(const char *attr_name) const
{
    return (_target_id) ? _target_id->GetAttribute(attr_name) : VhdlIdDef::GetAttribute(attr_name) ;
}

VhdlIdDef *
VhdlIdDef::GetAttributeId(const char *attr_name) const
{
    if (!_attributes) return 0 ;
    // Same as GetAttribute(), but this time, return the attribute 'identifier'.
    MapIter mi ;
    VhdlIdDef *attr_id ;
    VhdlExpression *attr_expr ;
    FOREACH_MAP_ITEM(_attributes, mi, &attr_id, &attr_expr) {
        if (!attr_id) continue ;
        // Do case-insensitive string compare, since VHDL identifiers are case-insensitive.
        if (Strings::compare_nocase(attr_id->Name(),attr_name)) {
            return attr_id ;
        }
    }
    return 0 ;
}
VhdlIdDef *
VhdlAliasId::GetAttributeId(const char *attr_name) const
{
    return (_target_id) ? _target_id->GetAttributeId(attr_name) : VhdlIdDef::GetAttributeId(attr_name) ;
}
Map *
VhdlAliasId::GetAttributes() const
{
    return (_target_id) ? _target_id->GetAttributes():  VhdlIdDef::GetAttributes() ;
}

// Viper #5432
void VhdlIdDef::AddAttributeSpec(VhdlAttributeSpec * /*attr_spec*/) { }
void VhdlAttributeId::AddAttributeSpec(VhdlAttributeSpec *attr_spec)
{
    if (!attr_spec) return ;
    if (!_attribute_specs) _attribute_specs = new Set(POINTER_HASH) ;
    (void) _attribute_specs->Insert(attr_spec) ;
}

// Viper #5432
void VhdlIdDef::RemoveAttributeSpec(VhdlAttributeSpec * /*attr_spec*/) { }
void VhdlAttributeId::RemoveAttributeSpec(VhdlAttributeSpec *attr_spec)
{
    if (_attribute_specs && attr_spec) (void) _attribute_specs->Remove(attr_spec) ;
}

// Viper #5432
void VhdlAttributeId::RemoveIdFromAttributeSpec(VhdlIdDef const *id) const
{
    if (!id || !_attribute_specs) return ;

    SetIter i ;
    VhdlAttributeSpec *attr_spec ;
    FOREACH_SET_ITEM(_attribute_specs, i, &attr_spec) {
        if (!attr_spec) continue ;
        VhdlSpecification *entity_spec = attr_spec->GetEntitySpec() ;
        Set *all_ids = entity_spec ? entity_spec->GetAllIds() : 0 ;
        //if (all_ids) (void) all_ids->Remove(id) ; // if present
        if (all_ids) {
            (void) all_ids->Remove(id) ; // if present
            // VIPER #7279: An alias id whose target is 'id' can be in the all_ids
            SetIter si ;
            VhdlIdDef *ai ;
            FOREACH_SET_ITEM(all_ids, si, &ai) {
                if (ai && ai->IsAlias() && (ai->GetTargetId() == id)) {
                    (void) all_ids->Remove(ai) ; // Remove alias of 'id'
                    break ;
                }
            }
        }
    }
}

// Viper #3637/6805: Get the attribute spec that defines the attribute on the argument id
VhdlAttributeSpec *VhdlIdDef::FindRelevantAttributeSpec(VhdlIdDef const * /*id*/) const { return 0 ; }

VhdlAttributeSpec *VhdlAttributeId::FindRelevantAttributeSpec(VhdlIdDef const *id) const
{
    if (!id || !_attribute_specs) return 0 ;

    SetIter i ;
    VhdlAttributeSpec *attr_spec ;
    FOREACH_SET_ITEM(_attribute_specs, i, &attr_spec) {
        if (!attr_spec) continue ;
        VhdlSpecification *entity_spec = attr_spec->GetEntitySpec() ;
        Set *all_ids = entity_spec ? entity_spec->GetAllIds() : 0 ;
        if (all_ids && all_ids->GetItem(id)) return attr_spec ;
    }

    return 0 ;
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_NAME(SIG, NAME)  (((SIG) << 5) + (SIG) + (Hash::StringCompare((NAME), 0)))
#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlIdDef::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_name) sig = HASH_NAME(0, _name) ;
    return sig ;
}

// VIPER #7804: For subtype take its type id, if type id exists
// call CalculateSignature() on its type id, otherwise call
// CalculateSignature() on VhdlIdDef.
unsigned long
VhdlSubtypeId::CalculateSignature() const
{
    VhdlIdDef *type_id = Type() ;
    if (type_id) return HASH_VAL(0, type_id->CalculateSignature()) ;

    unsigned long sig = VhdlIdDef::CalculateSignature() ;
    return sig ;
}

void VhdlVariableId::SetCanBeDualPortRam()  { if (vhdl_file::ExtractRams()) _can_be_dual_port_ram = 1 ; }
void VhdlVariableId::SetCanBeMultiPortRam() { if (vhdl_file::ExtractMPRams()) _can_be_multi_port_ram = 1 ; }

void VhdlSignalId::SetCanBeDualPortRam()    { if (vhdl_file::ExtractRams()) _can_be_dual_port_ram = 1 ; }
void VhdlSignalId::SetCanBeMultiPortRam()   { if (vhdl_file::ExtractMPRams()) _can_be_multi_port_ram = 1 ; }

void VhdlInterfaceId::SetCanBeMultiPortRam()   { if (vhdl_file::ExtractMPRams()) _can_be_multi_port_ram = 1 ; }

unsigned VhdlInterfaceId::CanBeMultiPortRam() const
{
    if (!_can_be_multi_port_ram) return 0 ;

    // Viper 7599: If the ref_target of this (formal) id is present return CanBeMultiPortRam of the ref_id (actual)
    // This API returns the ramability of the interfaceid based on the ref_target if present.
    // Thus during elaboration when the formal is bounded by an actual. this API will
    // return whether the actual can be RAM. During analysis this API will return the value of
    // the flag _can_be_multi_port_ram set by VhdlInterfaceId's constructor.
    if (_ref_expr) {
        VhdlDiscreteRange* actual_expr = _ref_expr->IsAssoc() ? _ref_expr->ActualPart() : _ref_expr ;
        VhdlIdDef *ref_id = actual_expr ? actual_expr->FindFullFormal() : 0 ;
        if (ref_id && ref_id->CanBeMultiPortRam()) return 1 ;
        return 0 ;
    }

    // Flag is set :
    return 1 ;
}

// If type is file type returns 1. If type is composite type, check its subelements
// and returns 1 if that is file type. If argument is 1, function will return 1
// if type/subelement type is access type.
// Viper #3898 : Added second argument - recursion_set
unsigned VhdlTypeId::IsTypeContainsFile(unsigned consider_access_type, Set& recursion_set)
{
    if (IsFileType()) return 1 ;
    if (consider_access_type && IsAccessType()) return 1 ;
    if (recursion_set.Get(this)) return 0 ;

    VhdlIdDef *element_type ;
    if (IsArrayType()) {
        element_type = ElementType() ;
        if (element_type) return element_type->IsTypeContainsFile(consider_access_type, recursion_set) ;
    } else if (IsRecordType()) {
        (void) recursion_set.Insert(this) ;
        unsigned i ;
        VhdlIdDef *element ;
        for(i = 0 ; i < NumOfElements(); i++) {
            element = GetElementAt(i) ;
            element_type = (element) ? element->Type() : 0 ;
            if (element_type && element_type->IsTypeContainsFile(consider_access_type, recursion_set)) return 1 ;
        }
    }
    return 0 ;
}

unsigned VhdlTypeId::IsTypeContainsUnconstrainedArray(Set& recursion_set)
{
    if (IsUnconstrained()) return 1 ;
    if (recursion_set.Get(this)) return 0 ;

    VhdlIdDef *element_type ;
    if (IsArrayType()) {
        element_type = ElementType() ;
        if (element_type) return element_type->IsTypeContainsUnconstrainedArray(recursion_set) ;
    } else if (IsRecordType()) {
        (void) recursion_set.Insert(this) ;
        unsigned i ;
        VhdlIdDef *element ;
        for(i = 0 ; i < NumOfElements(); i++) {
            element = GetElementAt(i) ;
            element_type = (element) ? element->Type() : 0 ;
            if (element_type && element_type->IsTypeContainsUnconstrainedArray(recursion_set)) return 1 ;
        }
    }
    return 0 ;
}

unsigned VhdlTypeId::IsTypeContainsRecord()
{
    if (IsRecordType()) return 1 ;

    VhdlIdDef *element_type ;
    if (IsArrayType()) {
        element_type = ElementType() ;
        if (element_type) return element_type->IsTypeContainsRecord() ;
    }

    return 0 ;
}

unsigned VhdlSubtypeId::IsTypeContainsFile(unsigned consider_access_type, Set& recursion_set)
{
    if (_type) return _type->IsTypeContainsFile(consider_access_type, recursion_set) ;
    return 0 ;
}

unsigned VhdlSubtypeId::IsTypeContainsUnconstrainedArray(Set& recursion_set)
{
    if (_type) return _type->IsTypeContainsUnconstrainedArray(recursion_set) ;
    return 0 ;
}

unsigned VhdlIdDef::IsSignalParameterOfSubprogram() const
{
    if (!IsSignal() || !Mode()) return 0 ; // Not signal, not port
    // Check whether signal is declared within subprogram
    VhdlIdDef *subprogram_id = (_present_scope) ? _present_scope->GetSubprogram(): 0 ;
    VhdlScope *subprogram_scope = (subprogram_id) ? subprogram_id->LocalScope() : 0 ;
    if (subprogram_scope && subprogram_scope->FindSingleObjectLocal(Name()) == this) {
        // This is formal signal parameter of subprogram
        return 1 ;
    }
    return 0 ; // Not signal parametr
}
void VhdlIdDef::SetActualTypes() {}
void VhdlPackageId::SetActualTypes()
{
    if (_unit) _unit->SetActualTypes() ;
}
VhdlScope *VhdlPackageInstElement::LocalScope() const
{
    return (_actual_id) ? _actual_id->LocalScope(): 0 ;
}
unsigned VhdlPackageInstElement::EntityClass() const
{
    return (_actual_id) ? _actual_id->EntityClass(): 0 ;
}
unsigned VhdlPackageInstElement::IsLibrary() const
{
    return (_actual_id) ? _actual_id->IsLibrary(): 0 ;
}
unsigned VhdlPackageInstElement::IsGroup() const
{
    return (_actual_id) ? _actual_id->IsGroup(): 0 ;
}
unsigned VhdlPackageInstElement::IsGroupTemplate() const
{
    return (_actual_id) ? _actual_id->IsGroupTemplate(): 0 ;
}
void VhdlPackageInstElement::DeclareAttribute(VhdlIdDef *type)
{
    if (_actual_id) _actual_id->DeclareAttribute(type) ;
}
unsigned VhdlPackageInstElement::IsAttribute() const
{
    return (_actual_id) ? _actual_id->IsAttribute(): 0 ;
}
void VhdlPackageInstElement::AddAttributeSpec(VhdlAttributeSpec *attr_spec)
{
    if (_actual_id) _actual_id->AddAttributeSpec(attr_spec) ;
}
void VhdlPackageInstElement::RemoveAttributeSpec(VhdlAttributeSpec *attr_spec)
{
    if (_actual_id) _actual_id->RemoveAttributeSpec(attr_spec) ;
}
VhdlAttributeSpec * VhdlPackageInstElement::FindRelevantAttributeSpec(VhdlIdDef const *id) const
{
    return (_actual_id) ? _actual_id->FindRelevantAttributeSpec(id): 0 ;
}
void VhdlPackageInstElement::AddPort(VhdlIdDef *id)
{
    if (_actual_id) _actual_id->AddPort(id) ;
}
void VhdlPackageInstElement::AddGeneric(VhdlIdDef *id)
{
    if (_actual_id) _actual_id->AddGeneric(id) ;
}
void VhdlPackageInstElement::SetComponentDecl(VhdlComponentDecl *component)
{
    if (_actual_id) _actual_id->SetComponentDecl(component) ;
}
VhdlComponentDecl * VhdlPackageInstElement::GetComponentDecl() const
{
    return (_actual_id) ? _actual_id->GetComponentDecl(): 0 ;
}
void VhdlPackageInstElement::SetLocalScope(VhdlScope *scope)
{
    if (_actual_id) _actual_id->SetLocalScope(scope) ;
}
void VhdlPackageInstElement::DeclareComponentId(Array *generics, Array *ports)
{
    if (_actual_id) _actual_id->DeclareComponentId(generics, ports) ;
}
unsigned VhdlPackageInstElement::IsComponent() const
{
    return (_actual_id) ? _actual_id->IsComponent(): 0 ;
}
VhdlIdDef *  VhdlPackageInstElement::GetGenericAt(unsigned pos) const
{
    return (_actual_id) ? _actual_id->GetGenericAt(pos): 0 ;
}
VhdlIdDef *  VhdlPackageInstElement::GetGeneric(const char *name) const
{
    return (_actual_id) ? _actual_id->GetGeneric(name): 0 ;
}
Array * VhdlPackageInstElement::GetGenerics() const
{
    return (_actual_id) ? _actual_id->GetGenerics(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetPortAt(unsigned pos) const
{
    return (_actual_id) ? _actual_id->GetPortAt(pos): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetPort(const char *name) const
{
    return (_actual_id) ? _actual_id->GetPort(name): 0 ;
}
Array *  VhdlPackageInstElement::GetPorts() const
{
    return (_actual_id) ? _actual_id->GetPorts(): 0 ;
}
unsigned  VhdlPackageInstElement::NumOfGenerics() const
{
    return (_actual_id) ? _actual_id->NumOfGenerics(): 0 ;
}
unsigned  VhdlPackageInstElement::NumOfPorts() const
{
    return (_actual_id) ? _actual_id->NumOfPorts(): 0 ;
}

char * VhdlPackageInstElement::CreateUniqueName(const VhdlBlockConfiguration *block_config) const  // Create a name for the entity/component withj set generics
{
    return (_actual_id) ? _actual_id->CreateUniqueName(block_config): 0 ;
}
void VhdlPackageInstElement::SetTargetName(VhdlName *target_name)  // Set back pointer _target_name in alias id.
{
    if (_actual_id) _actual_id->SetTargetName(target_name) ;
}
void VhdlPackageInstElement::SetTargetId(VhdlIdDef *id)
{
    if (_actual_id) _actual_id->SetTargetId(id) ;
}
void VhdlPackageInstElement::DeclareAlias(VhdlIdDef *type, VhdlName *target_name, unsigned locally_static_type, unsigned globally_static_type, unsigned kind)
{
    if (_actual_id) _actual_id->DeclareAlias(type, target_name, locally_static_type, globally_static_type, kind) ;
}
VhdlName * VhdlPackageInstElement::GetAliasTarget() const
{
    return (_actual_id) ? _actual_id->GetAliasTarget(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetTargetId() const
{
    //return (_actual_id) ? _actual_id->GetTargetId(): 0 ;
    return (_target_id) ;
}
unsigned VhdlPackageInstElement::TypeMatch(const VhdlIdDef *id) const
{
    return (_actual_id) ? _actual_id->TypeMatch(id): 0 ;
}
unsigned VhdlPackageInstElement::TypeMatchDual(const VhdlIdDef *id, unsigned generic_type) const
{
    return (_actual_id) ? _actual_id->TypeMatchDual(id, generic_type): 0 ;
}
unsigned VhdlPackageInstElement::IsCloselyRelatedType(const VhdlIdDef *id) const
{
    return (_actual_id) ? _actual_id->IsCloselyRelatedType(id): 0 ;
}
unsigned VhdlPackageInstElement::IsAlias() const
{
    return (_actual_id) ? _actual_id->IsAlias(): 0 ;
}
unsigned VhdlPackageInstElement::IsFile() const
{
    return (_actual_id) ? _actual_id->IsFile(): 0 ;
}
void VhdlPackageInstElement::DeclareFile(VhdlIdDef *type)
{
    if (_actual_id) _actual_id->DeclareFile(type) ;
}
FILE * VhdlPackageInstElement::GetFile() const
{
    return (_actual_id) ? _actual_id->GetFile(): 0 ;
}
void VhdlPackageInstElement::SetFile(FILE *file)
{
    if (_actual_id) _actual_id->SetFile(file) ;
}
unsigned VhdlPackageInstElement::IsVariable() const
{
    return (_actual_id) ? _actual_id->IsVariable(): 0 ;
}
void VhdlPackageInstElement::DeclareVariable(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type, unsigned is_shared)
{
    if (_actual_id) _actual_id->DeclareVariable(type, locally_static_type, globally_static_type, is_shared) ;
}
unsigned VhdlPackageInstElement::IsLocallyStaticType() const
{
    return (_actual_id) ? _actual_id->IsLocallyStaticType(): 0 ;
}
unsigned VhdlPackageInstElement::IsGloballyStaticType() const
{
    return (_actual_id) ? _actual_id->IsGloballyStaticType(): 0 ;
}
void VhdlPackageInstElement::SetLocallyStaticType()
{
    if (_actual_id) _actual_id->SetLocallyStaticType() ;
}
void VhdlPackageInstElement::SetGloballyStaticType()
{
    if (_actual_id) _actual_id->SetGloballyStaticType() ;
}
unsigned VhdlPackageInstElement::IsSharedVariable() const
{
    return (_actual_id) ? _actual_id->IsSharedVariable(): 0 ;
}
unsigned VhdlPackageInstElement::IsSignal() const
{
    return (_actual_id) ? _actual_id->IsSignal(): 0 ;
}
void VhdlPackageInstElement::DeclareSignal(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type, unsigned signal_kind)
{
    if (_actual_id) _actual_id->DeclareSignal(type, resolution_function, locally_static_type, globally_static_type, signal_kind) ;
}
VhdlIdDef * VhdlPackageInstElement::ResolutionFunction() const
{
    return (_actual_id) ? _actual_id->ResolutionFunction(): 0 ;
}
unsigned VhdlPackageInstElement::GetSignalKind() const
{
    return (_actual_id) ? _actual_id->GetSignalKind(): 0 ;
}
void VhdlPackageInstElement::DeclareConstant(VhdlIdDef *type, unsigned locally_static, unsigned loop_iter, unsigned globally_static, unsigned locally_static_type, unsigned globally_static_type)
{
    if (_actual_id) _actual_id->DeclareConstant(type, locally_static, loop_iter, globally_static, locally_static_type, globally_static_type) ;
}
unsigned VhdlPackageInstElement::IsConstant() const
{
    return (_actual_id) ? _actual_id->IsConstant(): 0 ;
}
unsigned VhdlPackageInstElement::IsConstantId() const
{
    return (_actual_id) ? _actual_id->IsConstantId(): 0 ;
}
unsigned VhdlPackageInstElement::HasInitAssign() const
{
    return (_actual_id) ? _actual_id->HasInitAssign(): 0 ;
}
unsigned VhdlPackageInstElement::IsLocallyStatic() const
{
    return (_actual_id) ? _actual_id->IsLocallyStatic(): 0 ;
}
unsigned VhdlPackageInstElement::IsGloballyStatic() const
{
    return (_actual_id) ? _actual_id->IsGloballyStatic(): 0 ;
}
unsigned VhdlPackageInstElement::GetLocallyStatic() const
{
    return (_actual_id) ? _actual_id->GetLocallyStatic(): 0 ;
}
void VhdlPackageInstElement::SetLocallyStatic()
{
    if (_actual_id) _actual_id->SetLocallyStatic() ;
}
void VhdlPackageInstElement::SetLoopIterator()
{
    if (_actual_id) _actual_id->SetLoopIterator() ;
}
void VhdlPackageInstElement::SetDecl(VhdlIdDef *decl)
{
    if (_actual_id) _actual_id->SetDecl(decl) ;
}
VhdlIdDef * VhdlPackageInstElement::Decl() const
{
    return (_actual_id) ? _actual_id->Decl(): 0 ;
}
unsigned long VhdlPackageInstElement::CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const
{
    return (_actual_id) ? _actual_id->CalculateSaveRestoreSignature(use_num_compare): 0 ;
}
void VhdlPackageInstElement::DeclareForCopy(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)  // Set back pointers in type identifier
{
    if (_actual_id) _actual_id->DeclareForCopy(old_type_id, old2new) ;
}
void VhdlPackageInstElement::UpdateType(VhdlMapForCopy &old2new)
{
    if (_actual_id) _actual_id->UpdateType(old2new) ;
}
unsigned VhdlPackageInstElement::IsType() const
{
    return (_actual_id) ? _actual_id->IsType(): 0 ;
}
unsigned VhdlPackageInstElement::IsIncompleteType()  const
{
    return (_actual_id) ? _actual_id->IsIncompleteType(): 0 ;
}
unsigned VhdlPackageInstElement::IsDefinedIncompleteType() const
{
    return (_actual_id) ? _actual_id->IsDefinedIncompleteType(): 0 ;
}
unsigned VhdlPackageInstElement::IsIntegerType() const
{
    return (_actual_id) ? _actual_id->IsIntegerType(): 0 ;
}
unsigned VhdlPackageInstElement::IsRealType() const
{
    return (_actual_id) ? _actual_id->IsRealType(): 0 ;
}
unsigned VhdlPackageInstElement::IsEnumerationType() const
{
    return (_actual_id) ? _actual_id->IsEnumerationType(): 0 ;
}
unsigned VhdlPackageInstElement::IsArrayType() const
{
    return (_actual_id) ? _actual_id->IsArrayType(): 0 ;
}
unsigned VhdlPackageInstElement::IsPhysicalType() const
{
    return (_actual_id) ? _actual_id->IsPhysicalType(): 0 ;
}
unsigned VhdlPackageInstElement::IsStdTimeType() const
{
    return (_actual_id) ? _actual_id->IsStdTimeType(): 0 ;
}
unsigned VhdlPackageInstElement::IsUnconstrainedArrayType() const
{
    return (_actual_id) ? _actual_id->IsUnconstrainedArrayType(): 0 ;
}
unsigned VhdlPackageInstElement::IsConstrainedArrayType() const
{
    return (_actual_id) ? _actual_id->IsConstrainedArrayType(): 0 ;
}
unsigned VhdlPackageInstElement::IsRecordType() const
{
    return (_actual_id) ? _actual_id->IsRecordType(): 0 ;
}
unsigned VhdlPackageInstElement::IsAccessType() const
{
    return (_actual_id) ? _actual_id->IsAccessType(): 0 ;
}
unsigned VhdlPackageInstElement::ContainsAccessType() const
{
    return (_actual_id) ? _actual_id->ContainsAccessType(): 0 ;
}
unsigned VhdlPackageInstElement::IsFileType() const
{
    return (_actual_id) ? _actual_id->IsFileType(): 0 ;
}
unsigned VhdlPackageInstElement::IsScalarType() const
{
    return (_actual_id) ? _actual_id->IsScalarType(): 0 ;
}
unsigned VhdlPackageInstElement::IsNumericType() const
{
    return (_actual_id) ? _actual_id->IsNumericType(): 0 ;
}
unsigned VhdlPackageInstElement::IsDiscreteType() const
{
    return (_actual_id) ? _actual_id->IsDiscreteType(): 0 ;
}
unsigned VhdlPackageInstElement::IsProtectedType() const
{
    return (_actual_id) ? _actual_id->IsProtectedType(): 0 ;
}
unsigned VhdlPackageInstElement::IsProtectedTypeBody() const
{
    return (_actual_id) ? _actual_id->IsProtectedTypeBody(): 0 ;
}
unsigned VhdlPackageInstElement::IsStringType() const
{
    return (_actual_id) ? _actual_id->IsStringType(): 0 ;
}
unsigned VhdlPackageInstElement::IsNaturalType() const
{
    return (_actual_id) ? _actual_id->IsNaturalType(): 0 ;
}
unsigned VhdlPackageInstElement::IsPositiveType() const
{
    return (_actual_id) ? _actual_id->IsPositiveType(): 0 ;
}
unsigned VhdlPackageInstElement::IsGenericType() const
{
    return (_actual_id) ? _actual_id->IsGenericType(): 0 ;
}
unsigned VhdlPackageInstElement::IsGeneric() const
{
    return (_actual_id) ? _actual_id->IsGeneric(): 0 ;
}
unsigned VhdlPackageInstElement::IsStdULogicType() const
{
    return (_actual_id) ? _actual_id->IsStdULogicType(): 0 ;
}
unsigned VhdlPackageInstElement::IsStdBitType() const
{
    return (_actual_id) ? _actual_id->IsStdBitType(): 0 ;
}
unsigned VhdlPackageInstElement::IsStdBooleanType() const
{
    return (_actual_id) ? _actual_id->IsStdBooleanType(): 0 ;
}
unsigned VhdlPackageInstElement::IsStdCharacterType() const
{
    return (_actual_id) ? _actual_id->IsStdCharacterType(): 0 ;
}
unsigned VhdlPackageInstElement::IsArray() const
{
    return (_actual_id) ? _actual_id->IsArray(): 0 ;
}
unsigned VhdlPackageInstElement::IsRecord() const
{
    return (_actual_id) ? _actual_id->IsRecord(): 0 ;
}
unsigned VhdlPackageInstElement::IsProtected() const
{
    return (_actual_id) ? _actual_id->IsProtected(): 0 ;
}
unsigned VhdlPackageInstElement::IsProtectedBody() const
{
    return (_actual_id) ? _actual_id->IsProtectedBody(): 0 ;
}
void VhdlPackageInstElement::DeclareIntegerType()
{
    if (_actual_id) _actual_id->DeclareIntegerType() ;
}
void VhdlPackageInstElement::DeclareRealType()
{
    if (_actual_id) _actual_id->DeclareRealType() ;
}
void VhdlPackageInstElement::DeclarePhysicalType(Map *physical_units)
{
    if (_actual_id) _actual_id->DeclarePhysicalType(physical_units) ;
}
void VhdlPackageInstElement::DeclareEnumerationType(Map *enumeration_literals)
{
    if (_actual_id) _actual_id->DeclareEnumerationType(enumeration_literals) ;
}
void VhdlPackageInstElement::DeclareConstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static)
{
    if (_actual_id) _actual_id->DeclareConstrainedArrayType(index_constraint, element_type, locally_static, globally_static) ;
}
void VhdlPackageInstElement::DeclareUnconstrainedArrayType(Map *index_constraint, VhdlIdDef *element_type, int locally_static, int globally_static)
{
    if (_actual_id) _actual_id->DeclareUnconstrainedArrayType(index_constraint, element_type, locally_static, globally_static) ;
}
void VhdlPackageInstElement::DeclareRecordType(Map *element_decl_list)
{
    if (_actual_id) _actual_id->DeclareRecordType(element_decl_list) ;
}
void VhdlPackageInstElement::DeclareFileType(VhdlIdDef *subtype_id)
{
    if (_actual_id) _actual_id->DeclareFileType(subtype_id) ;
}
void VhdlPackageInstElement::DeclareAccessType(VhdlIdDef *subtype_id, int locally_static, int globally_static)
{
    if (_actual_id) _actual_id->DeclareAccessType(subtype_id, locally_static, globally_static) ;
}
void VhdlPackageInstElement::DeclareIncompleteType(VhdlIdDef *subtype_id)
{
    if (_actual_id) _actual_id->DeclareIncompleteType(subtype_id) ;
}
VhdlIdDef * VhdlPackageInstElement::Type() const
{
    return (_actual_id) ? _actual_id->Type(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::BaseType() const
{
    return (_actual_id) ? _actual_id->BaseType(): 0 ;
}
unsigned VhdlPackageInstElement::SetAttribute(const VhdlIdDef *attr_id, const VhdlExpression *attr_value)
{
    return (_actual_id) ? _actual_id->SetAttribute(attr_id, attr_value): 0 ;
}
unsigned VhdlPackageInstElement::Dimension() const
{
    return (_actual_id) ? _actual_id->Dimension(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::ElementType() const
{
    return (_actual_id) ? _actual_id->ElementType(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::ElementType(unsigned dim)
{
    return (_actual_id) ? _actual_id->ElementType(dim): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::IndexType(unsigned dim) const
{
    return (_actual_id) ? _actual_id->IndexType(dim): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::AggregateElementType() const
{
    return (_actual_id) ? _actual_id->AggregateElementType(): 0 ;
}
unsigned  VhdlPackageInstElement::DeepDimension() const
{
    return (_actual_id) ? _actual_id->DeepDimension(): 0 ;
}
unsigned VhdlPackageInstElement::NumOfEnums() const
{
    return (_actual_id) ? _actual_id->NumOfEnums(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetEnum(const char *name) const
{
    return (_actual_id) ? _actual_id->GetEnum(name): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetEnumAt(unsigned pos) const
{
    return (_actual_id) ? _actual_id->GetEnumAt(pos): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetEnumWithBitEncoding(char b) const
{
    return (_actual_id) ? _actual_id->GetEnumWithBitEncoding(b): 0 ;
}
unsigned VhdlPackageInstElement::NumOfElements() const
{
    return (_actual_id) ? _actual_id->NumOfElements(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetElement(const char *name) const
{
    return (_actual_id) ? _actual_id->GetElement(name): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetElementAt(unsigned pos) const
{
    return (_actual_id) ? _actual_id->GetElementAt(pos): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::GetUnit(const char *name) const      // get a unit from physical type by name. Fast.
{
    return (_actual_id) ? _actual_id->GetUnit(name): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::DesignatedType() const
{
    return (_actual_id) ? _actual_id->DesignatedType(): 0 ;
}
void VhdlPackageInstElement::DeclareOperators()
{
    if (_actual_id) _actual_id->DeclareOperators() ;
}
unsigned VhdlPackageInstElement::IsTypeContainsFile(unsigned consider_access_type, Set& recursion_set)
{
    return (_actual_id) ? _actual_id->IsTypeContainsFile(consider_access_type, recursion_set): 0 ;
}
unsigned VhdlPackageInstElement::IsTypeContainsUnconstrainedArray(Set& recursion_set)
{
    return (_actual_id) ? _actual_id->IsTypeContainsUnconstrainedArray(recursion_set): 0 ;
}
unsigned VhdlPackageInstElement::IsTypeContainsRecord()
{
    return (_actual_id) ? _actual_id->IsTypeContainsRecord(): 0 ;
}

Map * VhdlPackageInstElement::GetTypeDefList() const
{
    return (_actual_id) ? _actual_id->GetTypeDefList(): 0 ;
}
Map * VhdlPackageInstElement::GetList() const
{
    return (_actual_id) ? _actual_id->GetList(): 0 ;
}
void  VhdlPackageInstElement::DeclarePredefinedOp(unsigned op, const VhdlIdDef *arg1_type, const VhdlIdDef *arg2_type, VhdlIdDef *ret_type) const
{
    if (_actual_id) _actual_id->DeclarePredefinedOp(op, arg1_type, arg2_type, ret_type) ;
}
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void VhdlPackageInstElement::SetVerilogPkgName(const char *pkg_name)
{
    if (_actual_id) _actual_id->SetVerilogPkgName(pkg_name) ;
}
void VhdlPackageInstElement::SetVerilogPkgLibName(const char *pkg_lib_name)
{
    if (_actual_id) _actual_id->SetVerilogPkgLibName(pkg_lib_name) ;
}
unsigned VhdlPackageInstElement::IsVerilogType() const
{
    return (_actual_id) ? _actual_id->IsVerilogType(): 0 ;
}
VeriIdDef *  VhdlPackageInstElement::GetVerilogTypeFromPackage() const
{
    return (_actual_id) ? _actual_id->GetVerilogTypeFromPackage(): 0 ;
}

#endif
void VhdlPackageInstElement::DeclareProtectedType(VhdlProtectedTypeDef *type_def)
{
    if (_actual_id) _actual_id->DeclareProtectedType(type_def) ;
}
VhdlProtectedTypeDef * VhdlPackageInstElement::GetProtectedTypeDef() const
{
    return (_actual_id) ? _actual_id->GetProtectedTypeDef(): 0 ;
}
void VhdlPackageInstElement::DeclareProtectedTypeBody(VhdlProtectedTypeDefBody *type_def)
{
    if (_actual_id) _actual_id->DeclareProtectedTypeBody(type_def) ;
}
VhdlProtectedTypeDefBody  * VhdlPackageInstElement::GetProtectedTypeDefBody() const
{
    return (_actual_id) ? _actual_id->GetProtectedTypeDefBody(): 0 ;
}
unsigned VhdlPackageInstElement::IsUniversalInteger() const
{
    return (_actual_id) ? _actual_id->IsUniversalInteger(): 0 ;
}

unsigned VhdlPackageInstElement::IsUniversalReal() const
{
    return (_actual_id) ? _actual_id->IsUniversalReal(): 0 ;
}

unsigned VhdlPackageInstElement::IsSubprogram() const
{
    return (_actual_id) ? _actual_id->IsSubprogram(): 0 ;
}
unsigned VhdlPackageInstElement::IsFunction() const
{
    return (_actual_id) ? _actual_id->IsFunction(): 0 ;
}
unsigned VhdlPackageInstElement::IsProcedure() const
{
    return (_actual_id) ? _actual_id->IsProcedure(): 0 ;
}
unsigned VhdlPackageInstElement::IsPslDeclId() const
{
    return (_actual_id) ? _actual_id->IsPslDeclId(): 0 ;
}

void VhdlPackageInstElement::DeclareFunction(VhdlIdDef *return_type, Array *args, Array *generics)
{
    if (_actual_id) _actual_id->DeclareFunction(return_type, args, generics) ;
}

unsigned VhdlPackageInstElement::NumOfArgs() const
{
    return (_actual_id) ? _actual_id->NumOfArgs(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::Arg(unsigned idx) const
{
    return (_actual_id) ? _actual_id->Arg(idx): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::ArgType(unsigned idx) const
{
    return (_actual_id) ? _actual_id->ArgType(idx): 0 ;
}
Array * VhdlPackageInstElement::GetArgs() const  // Get program parameters
{
    return (_actual_id) ? _actual_id->GetArgs(): 0 ;
}

VhdlValue * VhdlPackageInstElement::ElaborateSubprogram(Array *arguments, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node, VhdlIdDef *subprog_inst)
{
    return (_actual_id) ? _actual_id->ElaborateSubprogram(arguments, target_constraint, df, caller_node, subprog_inst): 0 ;
}
void VhdlPackageInstElement::UpdateAnonymousType(VhdlIdDef *old_type_id, VhdlMapForCopy &old2new)
{
    if (_actual_id) _actual_id->UpdateAnonymousType(old_type_id, old2new) ;
}
unsigned VhdlPackageInstElement::IsAnonymousType() const
{
    return (_actual_id) ? _actual_id->IsAnonymousType(): 0 ;
}
void VhdlPackageInstElement::DeclareSubtype(VhdlIdDef *type, VhdlIdDef *resolution_function, unsigned is_unconstrained, unsigned locally_static_type, unsigned globally_static_type)
{
    if (_actual_id) _actual_id->DeclareSubtype(type, resolution_function, is_unconstrained, locally_static_type, globally_static_type) ;
}
unsigned VhdlPackageInstElement::IsSubtype() const
{
    return (_actual_id) ? _actual_id->IsSubtype(): 0 ;
}
unsigned VhdlPackageInstElement::IsHomograph(VhdlIdDef *id) const
{
    return (_actual_id) ? _actual_id->IsHomograph(id): 0 ;
}
unsigned VhdlPackageInstElement::IsSubprogramInst() const
{
    return (_actual_id) ? _actual_id->IsSubprogramInst(): 0 ;
}
void VhdlPackageInstElement::SetAsGeneric()
{
    if (_actual_id) _actual_id->SetAsGeneric() ;
}
void VhdlPackageInstElement::SetAsProcedure()
{
    if (_actual_id) _actual_id->SetAsProcedure() ;
}
void VhdlPackageInstElement::DeclareProcedure(Array *args, Array *generics)
{
    if (_actual_id) _actual_id->DeclareProcedure(args, generics) ;
}
void VhdlPackageInstElement::SetSpec(VhdlSpecification *spec)
{
    if (_actual_id) _actual_id->SetSpec(spec) ;
}
VhdlSpecification * VhdlPackageInstElement::Spec() const
{
    return (_actual_id) ? _actual_id->Spec(): 0 ;
}
void VhdlPackageInstElement::SetBody(VhdlSubprogramBody *body)
{
    if (_actual_id) _actual_id->SetBody(body) ;
}
VhdlSubprogramBody *VhdlPackageInstElement::Body() const
{
    return (_actual_id) ? _actual_id->Body(): 0 ;
}
unsigned VhdlPackageInstElement::GetPragmaFunction() const
{
    return (_actual_id) ? _actual_id->GetPragmaFunction(): 0 ;
}
unsigned VhdlPackageInstElement::GetPragmaSigned() const
{
    return (_actual_id) ? _actual_id->GetPragmaSigned(): 0 ;
}
unsigned VhdlPackageInstElement::GetPragmaLabelAppliesTo() const
{
    return (_actual_id) ? _actual_id->GetPragmaLabelAppliesTo(): 0 ;
}
void VhdlPackageInstElement::SetPragmaFunction(unsigned function)
{
    if (_actual_id) _actual_id->SetPragmaFunction(function) ;
}
void VhdlPackageInstElement::SetPragmaSigned()
{
    if (_actual_id) _actual_id->SetPragmaSigned() ;
}
void VhdlPackageInstElement::SetPragmaLabelAppliesTo()
{
    if (_actual_id) _actual_id->SetPragmaLabelAppliesTo() ;
}
void VhdlPackageInstElement::SetSubprogInstanceDecl(VhdlDeclaration *inst)
{
    if (_actual_id) _actual_id->SetSubprogInstanceDecl(inst) ;
}
VhdlDeclaration * VhdlPackageInstElement::GetSubprogInstanceDecl() const
{
    return (_actual_id) ? _actual_id->GetSubprogInstanceDecl(): 0 ;
}
void VhdlPackageInstElement::SetActualId(VhdlIdDef *id, Array* assoc_list)
{
    if (_actual_id) _actual_id->SetActualId(id, assoc_list) ;
}
VhdlIdDef * VhdlPackageInstElement::GetActualId() const
{
    return (_actual_id) ? _actual_id->GetActualId(): 0 ;
}
VhdlIdDef * VhdlPackageInstElement::TakeActualId()
{
    return (_actual_id) ? _actual_id->TakeActualId(): 0 ;
}
void VhdlPackageInstElement::SetIsProcessing(unsigned s)
{
    if (_actual_id) _actual_id->SetIsProcessing(s) ;
}
unsigned VhdlPackageInstElement::IsProcessing() const
{
    return (_actual_id) ? _actual_id->IsProcessing(): 0 ;
}

VhdlValue * VhdlPackageInstElement::ElaborateOperator(VhdlExpression *unary_value, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node)
{
    return (_actual_id) ? _actual_id->ElaborateOperator(unary_value, target_constraint, df, caller_node): 0 ;
}
VhdlValue * VhdlPackageInstElement::ElaborateOperator(VhdlExpression *left, VhdlExpression *right, VhdlConstraint *target_constraint, VhdlDataFlow *df, VhdlTreeNode *caller_node)
{
    return (_actual_id) ? _actual_id->ElaborateOperator(left, right, target_constraint, df, caller_node): 0 ;
}

void VhdlPackageInstElement::UpdateArgsAndType(VhdlMapForCopy &old2new)
{
    if (_actual_id) _actual_id->UpdateArgsAndType(old2new) ;
}
unsigned VhdlPackageInstElement::IsPredefinedOperator() const
{
    return (_actual_id) ? _actual_id->IsPredefinedOperator(): 0 ;
}
unsigned VhdlPackageInstElement::GetOperatorType() const
{
    return (_actual_id) ? _actual_id->GetOperatorType(): 0 ;
}
void VhdlPackageInstElement::SetOwnsArgId()
{
    if (_actual_id) _actual_id->SetOwnsArgId() ;
}
void VhdlPackageInstElement::SetHasInitAssign()   // Set back pointer
{
    if (_actual_id) _actual_id->SetHasInitAssign() ;
}
void VhdlPackageInstElement::ResetHasInitAssign()
{
    if (_actual_id) _actual_id->ResetHasInitAssign() ;
}
void VhdlPackageInstElement::SetConstrained()
{
    if (_actual_id) _actual_id->SetConstrained() ;
}
void VhdlPackageInstElement::SetIsActualPresent()
{
    if (_actual_id) _actual_id->SetIsActualPresent() ;
}
unsigned VhdlPackageInstElement::IsActualPresent() const
{
    return (_actual_id) ? _actual_id->IsActualPresent(): 0 ;
}
void VhdlPackageInstElement::SetRefFormal(unsigned is_ref_formal)
{
    if (_actual_id) _actual_id->SetRefFormal(is_ref_formal) ;
}
unsigned VhdlPackageInstElement::IsRefFormal() const
{
    return (_actual_id) ? _actual_id->IsRefFormal(): 0 ;
}

void VhdlPackageInstElement::DeclareInterfaceId(VhdlIdDef *type, unsigned kind, unsigned mode, unsigned signal_kind, unsigned has_init_value, unsigned is_unconstrained, VhdlIdDef *resolution_function, unsigned locally_static_type, unsigned globally_static_type)
{
    if (_actual_id) _actual_id->DeclareInterfaceId(type, kind, mode, signal_kind, has_init_value, is_unconstrained, resolution_function, locally_static_type, globally_static_type) ;
}

void VhdlPackageInstElement::SetObjectKind(unsigned object_kind)
{
    if (_actual_id) _actual_id->SetObjectKind(object_kind) ;
}
unsigned VhdlPackageInstElement::IsInterfaceObject(unsigned object_kind) const
{
    return (_actual_id) ? _actual_id->IsInterfaceObject(object_kind): 0 ;
}
unsigned VhdlPackageInstElement::GetObjectKind() const
{
    return (_actual_id) ? _actual_id->GetObjectKind(): 0 ;
}
unsigned VhdlPackageInstElement::IsPort() const
{
    return (_actual_id) ? _actual_id->IsPort(): 0 ;
}
unsigned VhdlPackageInstElement::IsSubprogramFormal() const
{
    return (_actual_id) ? _actual_id->IsSubprogramFormal(): 0 ;
}
unsigned VhdlPackageInstElement::IsInput() const
{
    return (_actual_id) ? _actual_id->IsInput(): 0 ;
}
unsigned VhdlPackageInstElement::IsOutput() const
{
    return (_actual_id) ? _actual_id->IsOutput(): 0 ;
}
unsigned VhdlPackageInstElement::IsInout() const
{
    return (_actual_id) ? _actual_id->IsInout(): 0 ;
}
unsigned VhdlPackageInstElement::IsLinkage() const
{
    return (_actual_id) ? _actual_id->IsLinkage(): 0 ;
}
unsigned VhdlPackageInstElement::IsBuffer() const
{
    return (_actual_id) ? _actual_id->IsBuffer(): 0 ;
}
unsigned VhdlPackageInstElement::Mode() const
{
    return (_actual_id) ? _actual_id->Mode(): 0 ;
}
void VhdlPackageInstElement::SetMode(unsigned mode)
{
    if (_actual_id) _actual_id->SetMode(mode) ;
}

unsigned VhdlPackageInstElement::GetKind() const
{
    return (_actual_id) ? _actual_id->GetKind(): 0 ;
}

unsigned VhdlPackageInstElement::IsUnconstrained() const
{
    return (_actual_id) ? _actual_id->IsUnconstrained(): 0 ;
}

unsigned VhdlTypeId::IsUnconstrained() const
{
    if (!_type_decl) return 0 ;
    return (_type_decl->NumUnconstrainedRanges()>0 ? 1 : 0) ;
}
unsigned VhdlIdDef::IsUnconstrained() const
{
    return 0 ;
}
unsigned VhdlIdDef::NumUnconstrainedRanges() const
{
    // Made this change for 7937 : the id should return the num unconstrained ranges of its type
    return _type ? _type->NumUnconstrainedRanges() : 0 ;
}
unsigned VhdlTypeId::NumUnconstrainedRanges() const
{
    if (!_type_decl) return 0 ;
    return _type_decl->NumUnconstrainedRanges() ;
}
unsigned VhdlSubtypeId::NumUnconstrainedRanges() const
{
    if (!_type_decl) return 0 ;
    return _type_decl->NumUnconstrainedRanges() ;
}
void VhdlPackageInstElement::SetRefTarget(VhdlDiscreteRange * r)
{
    if (_actual_id) _actual_id->SetRefTarget(r) ;
}
VhdlDiscreteRange* VhdlPackageInstElement::GetRefTarget() const
{
    return (_actual_id) ? _actual_id->GetRefTarget(): 0 ;
}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
void VhdlPackageInstElement::SetVerilogActualWidth(unsigned s)
{
    if (_actual_id) _actual_id->SetVerilogActualWidth(s) ;
}
unsigned VhdlPackageInstElement::GetVerilogActualWidth() const
{
    return (_actual_id) ? _actual_id->GetVerilogActualWidth(): 0 ;
}
#endif
unsigned VhdlPackageInstElement::IsEnumerationLiteral() const
{
    return (_actual_id) ? _actual_id->IsEnumerationLiteral(): 0 ;
}
void VhdlPackageInstElement::DeclareEnumerationId(VhdlIdDef *type, unsigned pos)
{
    if (_actual_id) _actual_id->DeclareEnumerationId(type, pos) ;
}
verific_uint64 VhdlPackageInstElement::Position() const
{
    return (_actual_id) ? _actual_id->Position(): 0 ;
}
void VhdlPackageInstElement::SetBitEncoding(char b)
{
    if (_actual_id) _actual_id->SetBitEncoding(b) ;
}
char VhdlPackageInstElement::GetBitEncoding() const
{
    return (_actual_id) ? _actual_id->GetBitEncoding(): 0 ;
}
void VhdlPackageInstElement::SetEncoding(const char *encoding)
{
    if (_actual_id) _actual_id->SetEncoding(encoding) ;
}
const char * VhdlPackageInstElement::GetEncoding() const
{
    return (_actual_id) ? _actual_id->GetEncoding(): 0 ;
}
void VhdlPackageInstElement::SetOnehotEncoded()
{
    if (_actual_id) _actual_id->SetOnehotEncoded() ;
}
unsigned VhdlPackageInstElement::IsOnehotEncoded() const
{
    return (_actual_id) ? _actual_id->IsOnehotEncoded(): 0 ;
}

void VhdlPackageInstElement::DeclareElementId(VhdlIdDef *type, unsigned locally_static_type, unsigned globally_static_type)
{
    if (_actual_id) _actual_id->DeclareElementId(type, locally_static_type, globally_static_type) ;
}
void VhdlPackageInstElement::DeclareRecordElement(unsigned pos)
{
    if (_actual_id) _actual_id->DeclareRecordElement(pos) ;
}

unsigned VhdlPackageInstElement::IsRecordElement() const
{
    return (_actual_id) ? _actual_id->IsRecordElement(): 0 ;
}
void VhdlPackageInstElement::DeclarePhysicalUnitId(VhdlIdDef *type, verific_uint64 pos)
{
    if (_actual_id) _actual_id->DeclarePhysicalUnitId(type, pos) ;
}
verific_uint64 VhdlPackageInstElement::InversePosition() const
{
    return (_actual_id) ? _actual_id->InversePosition(): 0 ;
}
void VhdlPackageInstElement::SetInversePosition(verific_uint64 ip)
{
    if (_actual_id) _actual_id->SetInversePosition(ip) ;
}
unsigned VhdlPackageInstElement::IsPhysicalUnit() const
{
    return (_actual_id) ? _actual_id->IsPhysicalUnit(): 0 ;
}
void VhdlPackageInstElement::SetPrimaryUnit(VhdlPrimaryUnit *unit)
{
    if (_actual_id) _actual_id->SetPrimaryUnit(unit) ;
}
VhdlPrimaryUnit * VhdlPackageInstElement::GetPrimaryUnit() const
{
    return (_actual_id) ? _actual_id->GetPrimaryUnit(): 0 ;
}
VhdlDesignUnit * VhdlPackageInstElement::GetDesignUnit() const
{
    return (_actual_id) ? _actual_id->GetDesignUnit(): 0 ;
}
void VhdlPackageInstElement::DeclareEntityId(Array *generics, Array *ports)
{
    if (_actual_id) _actual_id->DeclareEntityId(generics, ports) ;
}
unsigned VhdlPackageInstElement::IsEntity() const
{
    return (_actual_id) ? _actual_id->IsEntity(): 0 ;
}
unsigned VhdlPackageInstElement::IsDesignUnit() const
{
    return (_actual_id) ? _actual_id->IsDesignUnit(): 0 ;
}
void VhdlPackageInstElement::SetSecondaryUnit(VhdlSecondaryUnit *unit)
{
    if (_actual_id) _actual_id->SetSecondaryUnit(unit) ;
}
VhdlSecondaryUnit * VhdlPackageInstElement::GetSecondaryUnit() const
{
    return (_actual_id) ? _actual_id->GetSecondaryUnit(): 0 ;
}

unsigned VhdlPackageInstElement::IsArchitecture() const
{
    return (_actual_id) ? _actual_id->IsArchitecture(): 0 ;
}

void VhdlPackageInstElement::DeclareArchitectureId(VhdlIdDef *entity)
{
    if (_actual_id) _actual_id->DeclareArchitectureId(entity) ;
}
VhdlIdDef * VhdlPackageInstElement::GetEntity() const
{
    return (_actual_id) ? _actual_id->GetEntity(): 0 ;
}

unsigned VhdlPackageInstElement::IsConfiguration() const
{
    return (_actual_id) ? _actual_id->IsConfiguration(): 0 ;
}
void VhdlPackageInstElement::DeclareConfigurationId(VhdlIdDef *entity)
{
    if (_actual_id) _actual_id->DeclareConfigurationId(entity) ;
}
VhdlIdDef * VhdlPackageInstElement::GetUninstantiatedPackageId() const
{
    return (_actual_id) ? _actual_id->GetUninstantiatedPackageId(): 0 ;
}
void VhdlPackageInstElement::DeclarePackageId(Array *generics)
{
    if (_actual_id) _actual_id->DeclarePackageId(generics) ;
}
unsigned VhdlPackageInstElement::IsPackage() const
{
    return (_actual_id) ? _actual_id->IsPackage(): 0 ;
}
unsigned VhdlPackageInstElement::IsPackageInst() const
{
    return (_actual_id) ? _actual_id->IsPackageInst(): 0 ;
}
VhdlScope * VhdlPackageInstElement::GetUninstantiatedPackageScope() const
{
    return (_actual_id) ? _actual_id->GetUninstantiatedPackageScope(): 0 ;
}
void VhdlPackageInstElement::SetActualTypes()
{
    if (_actual_id) _actual_id->SetActualTypes() ;
}
void VhdlPackageInstElement::SetDeclaration(VhdlDeclaration *d)
{
    if (_actual_id) _actual_id->SetDeclaration(d) ;
}
VhdlDeclaration *VhdlPackageInstElement::GetDeclaration() const
{
    return (_actual_id) ? _actual_id->GetDeclaration(): 0 ;
}
unsigned VhdlPackageInstElement::IsPackageBody() const
{
    return (_actual_id) ? _actual_id->IsPackageBody(): 0 ;
}
unsigned VhdlPackageInstElement::IsContext() const
{
    return (_actual_id) ? _actual_id->IsContext(): 0 ;
}
unsigned VhdlPackageInstElement::IsLabel() const
{
    return (_actual_id) ? _actual_id->IsLabel(): 0 ;
}
void VhdlPackageInstElement::SetStatement(VhdlStatement *statement)
{
    if (_actual_id) _actual_id->SetStatement(statement) ;
}
VhdlStatement * VhdlPackageInstElement::GetStatement() const
{
    return (_actual_id) ? _actual_id->GetStatement(): 0 ;
}
void VhdlPackageInstElement::AddBinding(VhdlBindingIndication *binding)
{
    if (_actual_id) _actual_id->AddBinding(binding) ;
}
VhdlBindingIndication * VhdlPackageInstElement::GetPrimaryBinding() const
{
    return (_actual_id) ? _actual_id->GetPrimaryBinding(): 0 ;
}
void VhdlPackageInstElement::SetBinding(VhdlBindingIndication *binding)
{
    if (_actual_id) _actual_id->SetBinding(binding) ;
}
unsigned VhdlPackageInstElement::IsBlock() const
{
    return (_actual_id) ? _actual_id->IsBlock(): 0 ;
}
void VhdlPackageInstElement::DeclareBlockId(Array *generics, Array *ports)
{
    if (_actual_id) _actual_id->DeclareBlockId(generics, ports) ;
}
void VhdlIdDef::ConnectActualId(VhdlIdDef * /*ai*/) {}
void VhdlPackageInstElement::ConnectActualId(VhdlIdDef *ai)
{
    _actual_id = ai ;
}
VhdlIdDef * VhdlPackageInstElement::ActualType() const
{
    return (_actual_id) ? _actual_id->ActualType() : 0 ;
}
void VhdlPackageInstElement::SetTargetPkgInstElement(VhdlIdDef *id)
{
    _target_id = id ;
}
#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
// VIPER #7343 : Get particular dimension from subtype of identifier
VhdlDiscreteRange *VhdlIdDef::GetDimensionAt(unsigned pos) const
{
    if (!_subtype_indication) return 0 ; // No subtype indication to check
    return _subtype_indication->GetDimensionAt(pos) ;
}
#endif // VHDL_ID_SUBTYPE_BACKPOINTER

// VIPER #7775 : Set/get protected type body pointer to protected type id
void VhdlProtectedTypeId::SetProtectedTypeBodyId(VhdlIdDef *body_id)
{
    _body_id = body_id ;
    if (body_id) body_id->SetProtectedTypeDefId(this) ;
}
VhdlIdDef *VhdlProtectedTypeId::GetProtectedTypeBodyId() const { return _body_id ; }

void VhdlProtectedTypeBodyId::SetProtectedTypeDefId(VhdlIdDef *id) { _typedef_id = id ; }
VhdlIdDef * VhdlProtectedTypeBodyId::GetProtectedTypeDefId() const { return _typedef_id ; }

//////// VIPER #7809 : Check unconstrained element for type'element
unsigned VhdlPackageInstElement::HasUnconstrainedElement() const
{
    return (_actual_id) ? _actual_id->HasUnconstrainedElement(): 0 ;
}
unsigned VhdlTypeId::HasUnconstrainedElement() const
{
    if (!_type_decl) return 0 ;
    return _type_decl->HasUnconstrainedElement() ;
}
unsigned VhdlIdDef::HasUnconstrainedElement() const
{
    // Normal object, so should be defined with full constrained
    return 0 ; // Cannot have unconstrained element
}
unsigned VhdlSubtypeId::HasUnconstrainedElement() const
{
    return _type_decl ? _type_decl->HasUnconstrainedElement(): 0 ;
}

void VhdlVariableId::SetUsed(SynthesisInfo * /* info */)                        { SetUsed() ; }
void VhdlVariableId::SetAssigned(SynthesisInfo * /* info */)                    { SetAssigned() ; }
unsigned VhdlVariableId::IsUsed(SynthesisInfo * /* info */) const               { return IsUsed() ; }
unsigned VhdlVariableId::IsAssigned(SynthesisInfo * /* info */) const           { return IsAssigned() ; }

void     VhdlVariableId::SetConcurrentUsed(SynthesisInfo * /* info */)          { SetConcurrentUsed() ; }
void     VhdlVariableId::SetConcurrentAssigned(SynthesisInfo * /* info */)      { SetConcurrentAssigned() ; }
unsigned VhdlVariableId::IsConcurrentUsed(SynthesisInfo * /* info */) const     { return IsConcurrentUsed() ; }
unsigned VhdlVariableId::IsConcurrentAssigned(SynthesisInfo * /* info */) const { return IsConcurrentAssigned() ; }

// Usage information
void     VhdlVariableId::SetAssignedBeforeUsed(SynthesisInfo * /* info */)      { SetAssignedBeforeUsed() ; }
void     VhdlVariableId::SetUsedBeforeAssigned(SynthesisInfo * /* info */)      { SetUsedBeforeAssigned() ; }
void     VhdlVariableId::UnSetAssignedBeforeUsed(SynthesisInfo * /* info */)    { UnSetAssignedBeforeUsed() ; }
void     VhdlVariableId::UnSetUsedBeforeAssigned(SynthesisInfo * /* info */)    { UnSetUsedBeforeAssigned() ; }
unsigned VhdlVariableId::IsUsedBeforeAssigned(SynthesisInfo * /* info */) const { return IsUsedBeforeAssigned() ; }
unsigned VhdlVariableId::IsAssignedBeforeUsed(SynthesisInfo * /* info */) const { return IsAssignedBeforeUsed() ; }

// dual-port RAM detection :
unsigned VhdlVariableId::CanBeDualPortRam(SynthesisInfo * /* info */) const     { return CanBeDualPortRam() ; }
void     VhdlVariableId::SetCanBeDualPortRam(SynthesisInfo * /* info */)        { SetCanBeDualPortRam() ; }
void     VhdlVariableId::SetCannotBeDualPortRam(SynthesisInfo * /* info */)     { SetCannotBeDualPortRam() ; }

// multi-port RAM detection :
unsigned VhdlVariableId::CanBeMultiPortRam(SynthesisInfo * /* info */) const    { return CanBeMultiPortRam() ; }
void     VhdlVariableId::SetCanBeMultiPortRam(SynthesisInfo * /* info */)       { SetCanBeMultiPortRam() ; }
void     VhdlVariableId::SetCannotBeMultiPortRam(SynthesisInfo * /* info */)    { SetCannotBeMultiPortRam() ; }

void VhdlSignalId::SetUsed(SynthesisInfo * /* info */)                             { SetUsed() ; }
void VhdlSignalId::SetAssigned(SynthesisInfo * /* info */)                         { SetAssigned() ; }
unsigned VhdlSignalId::IsUsed(SynthesisInfo * /* info */) const                    { return IsUsed() ; }
unsigned VhdlSignalId::IsAssigned(SynthesisInfo * /* info */) const                { return IsAssigned() ; }

void     VhdlSignalId::SetConcurrentUsed(SynthesisInfo * /* info */)               { SetConcurrentUsed() ; }
void     VhdlSignalId::SetConcurrentAssigned(SynthesisInfo * /* info */)           { SetConcurrentAssigned() ; }
unsigned VhdlSignalId::IsConcurrentUsed(SynthesisInfo * /* info */) const          { return IsConcurrentUsed() ; }
unsigned VhdlSignalId::IsConcurrentAssigned(SynthesisInfo * /* info */) const      { return IsConcurrentAssigned() ; }

// dual-port RAM detection :
unsigned VhdlSignalId::CanBeDualPortRam(SynthesisInfo * /* info */) const          { return CanBeDualPortRam() ; }
void     VhdlSignalId::SetCanBeDualPortRam(SynthesisInfo * /* info */)             { SetCanBeDualPortRam() ; }
void     VhdlSignalId::SetCannotBeDualPortRam(SynthesisInfo * /* info */)          { SetCannotBeDualPortRam() ; }

// multi-port RAM detection :
unsigned VhdlSignalId::CanBeMultiPortRam(SynthesisInfo * /* info */) const         { return CanBeMultiPortRam() ; }
void     VhdlSignalId::SetCanBeMultiPortRam(SynthesisInfo * /* info */)            { SetCanBeMultiPortRam() ; }
void     VhdlSignalId::SetCannotBeMultiPortRam(SynthesisInfo * /* info */)         { SetCannotBeMultiPortRam() ; }

// Usage information
void     VhdlSubprogramId::SetAssignedBeforeUsed(SynthesisInfo * /* info */)           { SetAssignedBeforeUsed() ; }
void     VhdlSubprogramId::SetUsedBeforeAssigned(SynthesisInfo * /* info */)           { SetUsedBeforeAssigned() ; }
void     VhdlSubprogramId::UnSetAssignedBeforeUsed(SynthesisInfo * /* info */)         { UnSetAssignedBeforeUsed() ; }
void     VhdlSubprogramId::UnSetUsedBeforeAssigned(SynthesisInfo * /* info */)         { UnSetUsedBeforeAssigned() ; }
unsigned VhdlSubprogramId::IsUsedBeforeAssigned(SynthesisInfo * /* info */) const      { return IsUsedBeforeAssigned() ; }
unsigned VhdlSubprogramId::IsAssignedBeforeUsed(SynthesisInfo * /* info */) const      { return IsAssignedBeforeUsed() ; }

void VhdlInterfaceId::SetUsed(SynthesisInfo * /* info */)                              { SetUsed() ; }
void VhdlInterfaceId::SetAssigned(SynthesisInfo * /* info */)                          { SetAssigned() ; }
unsigned VhdlInterfaceId::IsUsed(SynthesisInfo * /* info */) const                     { return IsUsed() ; }
unsigned VhdlInterfaceId::IsAssigned(SynthesisInfo * /* info */) const                 { return IsAssigned() ; }
void VhdlInterfaceId::SetCanBeMultiPortRam(SynthesisInfo * /* info */)                 { SetCanBeMultiPortRam() ; }
void VhdlInterfaceId::SetCannotBeMultiPortRam(SynthesisInfo * /* info */)              { SetCannotBeMultiPortRam() ; }
unsigned VhdlInterfaceId::CanBeMultiPortRam(SynthesisInfo * /* info */) const          { return CanBeMultiPortRam() ; }
void VhdlInterfaceId::SetConcurrentUsed(SynthesisInfo * /* info */)                    { SetConcurrentUsed() ; }
void VhdlInterfaceId::SetConcurrentAssigned(SynthesisInfo * /* info */)                { SetConcurrentAssigned() ; }

void VhdlPackageInstElement::SetUsed(SynthesisInfo * /* info */)                        { SetUsed() ; }
void VhdlPackageInstElement::SetAssigned(SynthesisInfo * /* info */)                    { SetAssigned() ; }
unsigned VhdlPackageInstElement::IsUsed(SynthesisInfo * /* info */) const               { return IsUsed() ; }
unsigned VhdlPackageInstElement::IsAssigned(SynthesisInfo * /* info */) const           { return IsAssigned() ; }

void     VhdlPackageInstElement::SetConcurrentUsed(SynthesisInfo * /* info */)          { SetConcurrentUsed() ; }
void     VhdlPackageInstElement::SetConcurrentAssigned(SynthesisInfo * /* info */)      { SetConcurrentAssigned() ; }
unsigned VhdlPackageInstElement::IsConcurrentUsed(SynthesisInfo * /* info */) const     { return IsConcurrentUsed() ; }
unsigned VhdlPackageInstElement::IsConcurrentAssigned(SynthesisInfo * /* info */) const { return IsConcurrentAssigned() ; }

// Usage information
void     VhdlPackageInstElement::SetAssignedBeforeUsed(SynthesisInfo * /* info */)      { SetAssignedBeforeUsed() ; }
void     VhdlPackageInstElement::SetUsedBeforeAssigned(SynthesisInfo * /* info */)      { SetUsedBeforeAssigned() ; }
void     VhdlPackageInstElement::UnSetAssignedBeforeUsed(SynthesisInfo * /* info */)    { UnSetAssignedBeforeUsed() ; }
void     VhdlPackageInstElement::UnSetUsedBeforeAssigned(SynthesisInfo * /* info */)    { UnSetUsedBeforeAssigned() ; }
unsigned VhdlPackageInstElement::IsUsedBeforeAssigned(SynthesisInfo * /* info */) const { return IsUsedBeforeAssigned() ; }
unsigned VhdlPackageInstElement::IsAssignedBeforeUsed(SynthesisInfo * /* info */) const { return IsAssignedBeforeUsed() ; }

// dual-port RAM detection :
unsigned VhdlPackageInstElement::CanBeDualPortRam(SynthesisInfo * /* info */) const     { return CanBeDualPortRam() ; }
void     VhdlPackageInstElement::SetCanBeDualPortRam(SynthesisInfo * /* info */)        { SetCanBeDualPortRam() ; }
void     VhdlPackageInstElement::SetCannotBeDualPortRam(SynthesisInfo * /* info */)     { SetCannotBeDualPortRam() ; }

// multi-port RAM detection :
unsigned VhdlPackageInstElement::CanBeMultiPortRam(SynthesisInfo * /* info */) const    { return CanBeMultiPortRam() ; }
void     VhdlPackageInstElement::SetCanBeMultiPortRam(SynthesisInfo * /* info */)       { SetCanBeMultiPortRam() ; }
void     VhdlPackageInstElement::SetCannotBeMultiPortRam(SynthesisInfo * /* info */)    { SetCannotBeMultiPortRam() ; }

