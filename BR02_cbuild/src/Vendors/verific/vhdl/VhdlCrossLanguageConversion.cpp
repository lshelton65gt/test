/*
 *
 * [ File Version : 1.32 - 2014/01/08 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <time.h> // for time stamp

#include <math.h>   // For ciel, log

#include "Array.h"
#include "Map.h"
#include "Strings.h"
#include "Message.h"

#include "vhdl_file.h"
#include "VhdlScope.h"
#include "VhdlUnits.h"
#include "VhdlDeclaration.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlExpression.h"

#include "VhdlValue_Elab.h"

// Need for convert vhdl package to verilog package
#include "../verilog/veri_file.h"
#include "../verilog/VeriLibrary.h"
#include "../verilog/VeriScope.h"
#include "../verilog/VeriModuleItem.h"
#include "../verilog/VeriModule.h"
#include "../verilog/VeriId.h"
#include "../verilog/VeriExpression.h"
#include "../verilog/VeriMisc.h"
#include "../verilog/VeriCopy.h"
#include "../verilog/VeriConstVal.h"
#include "../verilog/veri_tokens.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION

#define PROCESSING_SUBTYPE_DECL 1
#define PROCESSING_PREFIX 2

// VIPER #6895: Convert verilog package to vhdl package
// String matching of vhdl builtin data type and return the corresponding verilog one
unsigned VhdlNode::GetToken(const char *name)
{
    if (Strings::compare_nocase(name, "bit") || Strings::compare_nocase(name, "bit_vector") || Strings::compare_nocase(name, "boolean")) {
        return VERI_BIT ;
    } else if (Strings::compare_nocase(name, "std_logic") || Strings::compare_nocase(name, "std_ulogic") || Strings::compare_nocase(name, "std_logic_vector") || Strings::compare_nocase(name, "std_ulogic_vector")) {
        return VERI_LOGIC ;
    } else if (Strings::compare_nocase(name, "integer") || Strings::compare_nocase(name, "natural") || Strings::compare_nocase(name, "positive")) {
        return VERI_INT ;
    } else if (Strings::compare_nocase(name, "real")) {
        return VERI_REAL ;
    }
    return 0 ;
}

// VIPER #6895: Convert vhdl package to verilog package
VeriPackage *VhdlPackageDecl::ConvertPackage()
{
    if (!_local_scope) return 0 ;

    // Create new package id:
    char *name = Strings::save(_id ? _id->Name() : 0) ;
    char *lower_case_name = Strings::strtolower(name) ;
    VeriModuleId *veri_package_id = new VeriModuleId(lower_case_name) ;
    if (_id) veri_package_id->SetLinefile(_id->Linefile()) ;
    veri_package_id->SetIsPackage() ; // set as package

    // Create a new veri scope:
    VeriScope *veri_scope = new VeriScope(0, veri_package_id) ;

    // Array of declared package items:
    Array *veri_package_items = new Array() ;

    Map *use_clause_units = _local_scope->GetUseClauseUnits() ;
    // Convert all the used packages in another package
    // Consider the following example:
    // vhdl declaration is:
    //     package constants is
    //       type arr is array (0 to 3) of integer ;
    //     end ;
    // The package constant is used in the following package
    //     use work.constants.all ;
    //     package type_pkg is
    //         alias new_arr is arr ; -- alias of type of different package
    //     end ;

    // corresponding verilog declaration is:
    //    package constants ;
    //        typedef int arr [0:3] ;
    //    endpackage
    // The second package using the above package is:
    // package type_pkg ;
    //     import constants:: * ;
    //     typedef arr new_arr ;
    // endpackage
    // For using package elements of one package into another we need to convert
    // the using packages before we convert the second one. Here we need not to
    // convert the packages of standard libraries. After converting the using
    // packages we create import declarations for those converted using packages
    // and add those import declarations into the converted container package.
    // Then go to convert the container package.
    VhdlIdDef *this_unit = _local_scope->GetContainingDesignUnit() ;
    MapIter mi ;
    char *suffix ;
    VhdlIdDef *other_unit ;
    FOREACH_MAP_ITEM(use_clause_units, mi, &other_unit, &suffix) {
        if (!other_unit) continue ;
        if ((other_unit == this_unit) || !other_unit->IsPackage()) continue ; // Don't (re)call myself and no need to convert if not a package
        VhdlPrimaryUnit *vhdl_package = other_unit->GetPrimaryUnit() ;
        if (!vhdl_package) continue ;

        VhdlLibrary *owning_lib = vhdl_package->GetOwningLib() ;
        if (!owning_lib) continue ;
        char *lib = owning_lib->Name() ;
        // Do not convert the packages of the standard libraries
        if (Strings::compare_nocase(lib, "arithmetic") || Strings::compare_nocase(lib, "ieee")
            || Strings::compare_nocase(lib, "qsim_logic") || Strings::compare_nocase(lib, "std")
            || Strings::compare_nocase(lib, "synopsys") || Strings::compare_nocase(lib, "vl")) {
            continue ;
        }

        // Convert the other packages
        VeriPackage *veri_package = vhdl_package->ConvertPackage() ;
        if (!veri_package) continue ;

        // Create import declarations for the converted dependent packages and
        // add all the import declarations to the converted container package.
        VeriIdDef *id = veri_package->GetId() ;
        VeriName *veri_pkg_name = new VeriIdRef(id) ;
        veri_pkg_name->SetLinefile(Linefile()) ;
        VeriScopeName *import_item = 0 ;
        char *suffix_name = 0 ;
        if (suffix) suffix_name = Strings::save(suffix) ;
        import_item = new VeriScopeName(veri_pkg_name, suffix_name, 0) ;
        import_item->SetLinefile(Linefile()) ;

        // Create import declarations
        Array *import_items = new Array() ;
        import_items->InsertLast(import_item) ;
        VeriImportDecl *import_decl = new VeriImportDecl(import_items) ;
        import_decl->SetLinefile(Linefile()) ;
        // Store the import declarations into package declaration items
        veri_package_items->InsertLast(import_decl) ;
        // Add all the imported items of using packages to the container package scope
        VeriScope *pkg_scope = veri_package->GetScope() ;
        veri_scope->AddImportItem(pkg_scope, suffix, import_item) ;
    }

    unsigned i ;
    VhdlDeclaration *decl ;
    // Convert all type, subtype, constant and alias declarations
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        VeriDataDecl *data_decl = decl ? decl->ConvertDataDecl(veri_scope) : 0 ;
        if (data_decl) veri_package_items->InsertLast(data_decl) ;
    }
    VeriPackage *veri_package = new VeriPackage(veri_package_id, veri_package_items, veri_scope) ;
    veri_package->SetLinefile(Linefile()) ; // set linefile
    veri_package->SetVhdlPackage(1) ; // mark the verilog package a converted package from vhdl package

    // Add the converted verilog package to the verilog library:
    VhdlLibrary *vhdl_lib = GetOwningLib() ;
    char *lib_name = vhdl_lib ? Strings::save(vhdl_lib->Name()) : 0 ;
    char *lib_name_lower = Strings::strtolower(lib_name) ;
    VeriLibrary *veri_lib = veri_file::GetLibrary(lib_name_lower, 1) ;
    if (veri_lib && !veri_lib->AddModule(veri_package)) veri_package = 0 ;

    // Set vhdl package name and library name to all the type ids:
    if (veri_package) {
        VeriScope *scope = veri_package->GetScope() ;
        Map *this_scope = scope ? scope->GetThisScope() : 0 ;
        VeriIdDef *id ;
        FOREACH_MAP_ITEM(this_scope, mi, 0, &id) {
            if (!id) continue ;
            if (id->IsType()) {
                id->SetVhdlPackageName(name) ;
                id->SetVhdlLibName(lib_name) ;
            }
        }
    }
    Strings::free(lib_name) ;

    return veri_package ;
}

// Conversion of vhdl type, constants, alias declarations to verilog
// Convert vhdl subtype declaration to verilog typedef
// eg: vhdl declaration: subtype bit_arr1 is std_ulogic_vector (0 to 8)
// corresponding verilog decalaration: typedef logic [0:8] bit_arr1
VeriDataDecl *VhdlSubtypeDecl::ConvertDataDecl(VeriScope *scope) const
{
    if (!_id || !scope) return 0 ;

    char *name = Strings::save(_id->Name()) ;
    char *lower_case_name = Strings::strtolower(name) ;
    VeriTypeId *veri_id = new VeriTypeId(lower_case_name) ;
    veri_id->SetLinefile(_id->Linefile()) ; // set linefile
    (void) scope->Declare(veri_id) ;
    Array veri_id_arr(2) ;
    veri_id_arr.InsertLast(veri_id) ; // array of verilog ids

    VeriDataType *data_type = 0 ;
    // Get data type from its subtype indication:
    if (_subtype_indication) data_type = _subtype_indication->ConvertDataType(scope, &veri_id_arr, PROCESSING_SUBTYPE_DECL) ;

    // Create data declaration:
    VeriDataDecl *data_decl = new VeriDataDecl(VERI_TYPEDEF, data_type, veri_id) ;
    data_decl->SetLinefile(Linefile()) ; // set linefile
    return data_decl ;
}

// Convert vhdl fulltype declaration: array, record, scalar type
// declaration to verilog data declarations :
// Fulltype declaration may be array, enumaration, record and scalar declarations
VeriDataDecl *VhdlFullTypeDecl::ConvertDataDecl(VeriScope *scope) const
{
    if (!_id || !scope) return 0 ;

    char *name = Strings::save(_id->Name()) ;
    char *lower_case_name = Strings::strtolower(name) ;
    VeriTypeId *veri_id = new VeriTypeId(lower_case_name) ;
    veri_id->SetLinefile(_id->Linefile()) ; // set linefile
    (void) scope->Declare(veri_id) ;
    Array veri_id_arr(2) ;
    veri_id_arr.InsertLast(veri_id) ; // array of verilog ids

    VeriDataType *data_type = 0 ;
    if (_type_def) data_type = _type_def->ConvertDataType(scope, &veri_id_arr, 0) ;

    // Create data declaration:
    VeriDataDecl *data_decl = new VeriDataDecl(VERI_TYPEDEF, data_type, veri_id) ;
    data_decl->SetLinefile(Linefile()) ; // set linefile
    return data_decl ;
}

// Convert alias declaration to verilog declaration:
// For object type alias declaration we convert it to verilog typedef
// and for constant type alias we convert it to verilog parameter decalaration
// alias of object: eg: vhdl declaration: type one_d_array is array is array (0 to 8) of std_logic
//                                        type two_d_array is array (0 to 8) of one_d_array
//                                        alias a_two_d_array is two_d_array
//                      corresponding verilog decalaration: typedef two_d_array a_two_d_array
// alias of constants: eg: vhdl declaration: constant s1 : bit_vector (0 to 4) := "10101"
//                                           alias a_s :  bit_vector (0 to 4) is s
//                         corresponding verilog decalaration: parameter bit [0:4] a_s = s
VeriDataDecl *VhdlAliasDecl::ConvertDataDecl(VeriScope *scope) const
{
    if (!_designator || !scope) return 0 ;

    char *designator_name = Strings::save(_designator->Name()) ;
    char *lc_designator_name = Strings::strtolower(designator_name) ;

    // Do the following object/non-object alias checks
    VhdlIdDef *target_id = _alias_target_name ? _alias_target_name->FindAliasTarget() : 0 ; // (used to be FindFormal(), but that's not right) ;

    char *target_name = target_id ? Strings::save(target_id->Name()) : 0 ;
    char *lc_target_name = (target_name) ? Strings::strtolower(target_name) : 0 ;
    VeriIdDef *id = scope->Find(lc_target_name) ;

    if (!id) {
        Error("%s is not declared", target_name) ;
        Strings::free(target_name) ;
        Strings::free(designator_name) ;
        return 0 ;
    }
    Strings::free(target_name) ;

    VeriDataDecl *data_decl = 0 ;

    // First see if this is an 'object' alias or not :
    // For object type alias we create type id
    if (target_id && target_id->IsType()) {
        VeriTypeId *veri_id = new VeriTypeId(lc_designator_name) ;
        veri_id->SetLinefile(_designator->Linefile()) ; // set linefile
        (void) scope->Declare(veri_id) ;

        VeriDataType *data_type = new VeriTypeRef(id, 0, 0) ;
        data_type->SetLinefile(_alias_target_name ? _alias_target_name->Linefile() : 0) ; // set linefile
        // Create data declaration
        data_decl = new VeriDataDecl(VERI_TYPEDEF, data_type, veri_id) ;
        data_decl->SetLinefile(Linefile()) ; // set linefile
    }

    // Alias for constants: we create param id
    if (target_id && target_id->IsConstant()) {
        VeriParamId *veri_id = new VeriParamId(lc_designator_name) ;
        veri_id->SetLinefile(_designator->Linefile()) ; // set linefile
        (void) scope->Declare(veri_id) ;

        // Set initial values:
        VeriIdRef *id_ref = new VeriIdRef(id) ;
        id_ref->SetLinefile(_alias_target_name ? _alias_target_name->Linefile() : 0) ; // set linefile
        veri_id->SetInitialValue(id_ref) ;

        VeriMapForCopy old2new ;
        VeriDataType *d_type = id->GetDataType() ;
        VeriDataType *data_type = d_type ? d_type->CopyDataType(old2new) : 0 ;
        VeriRange *range = id->GetDimensions() ;
        // Add memory range to the veri id
        if (range) veri_id->SetMemoryRange(range->CopyRange(old2new)) ;

        // Create parameter decalaration:
        data_decl = new VeriDataDecl(VERI_PARAMETER, data_type, veri_id) ;
        data_decl->SetLinefile(Linefile()) ; // set linefile
    }

    if (!data_decl) Strings::free(designator_name) ; // We never freed it in VeriIdDef constructor
    return data_decl ;
}

// Convert vhdl constant declaration to verilog parameter decalration
// eg: vhdl declaration: constant s1 : bit_vector (0 to 4) := "10101"
// corresponding verilog decalaration: parameter bit [0:4] s1 = '{1'b1,1'b0,1'b1,1'b0,1'b1}
VeriDataDecl *VhdlConstantDecl::ConvertDataDecl(VeriScope *scope) const
{
    if (!scope) return 0 ;

    // Array for containing constant ids
    Array *veri_id_list = new Array(_id_list ? _id_list->Size() : 2) ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        char *name = Strings::save(id->Name()) ;
        char *lower_case_name = Strings::strtolower(name) ;
        VeriParamId *veri_id = new VeriParamId(lower_case_name) ;
        veri_id->SetLinefile(id->Linefile()) ; // set linefile
        (void) scope->Declare(veri_id) ;
        veri_id_list->InsertLast(veri_id) ;
    }

    // Create data type
    VeriDataType *data_type = _subtype_indication ? _subtype_indication->ConvertDataType(scope, veri_id_list, 0) : 0 ;

    // Create data declaration:
    VeriDataDecl *data_decl = new VeriDataDecl(VERI_PARAMETER, data_type, veri_id_list) ;
    data_decl->SetLinefile(Linefile()) ; // set linefile
    return data_decl ;
}

// Convert vhdl enum to verilog enum declaration:
// eg: vhdl declaration: type state_type is (IDLE, CHK, ADDR, DATA_RW, WAIT_RW, S_TAR, TURN_AR )
// corresponding verilog decalaration: typedef enum { idle, chk, addr, data_rw, wait_rw, s_tar, turn_ar} state_type
VeriDataType *VhdlEnumerationTypeDef::ConvertDataType(VeriScope *scope, Array* /*type_id_arr*/, unsigned /*processing_type*/) const
{
    if (!scope) return 0 ;

    Array *veri_enum_ids = new Array(_enumeration_literal_list ? _enumeration_literal_list->Size() : 2) ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list, i, id) {
        if (!id) continue ;
        char *name = Strings::save(id->Name()) ;
        // Change name to lower case if not character literal
        // Viper 7006: for escaped vhdl names. use the name w/o escaping in Verilog.
        if (name) {
            if (*name == '\\' && *(name + Strings::len(name) -1) == '\\') {
                // Name is escaped
                char *tmp = name ;
                *(name + Strings::len(name) -1) = 0 ;
                name = name + 1 ;
                name = Strings::save(name) ;
                Strings::free(tmp) ;
            } else if (*name != '\'') {
                // Name is not escaped
                name = Strings::strtolower(name) ;
            }
        }
        VeriParamId *veri_id = new VeriParamId(name) ;
        veri_id->SetLinefile(id->Linefile()) ; // set linefile
        (void) scope->Declare(veri_id) ;
        veri_enum_ids->InsertLast(veri_id) ;
    }
    unsigned num_enums = veri_enum_ids ? veri_enum_ids->Size() : 0 ;

    unsigned size = 0 ;
    unsigned num = num_enums>0 ? num_enums-1 : 0 ;
    for (; num!=0; num=num/2) size++ ;
    if (!size) size++ ; // Use one bit for value 0

    VeriRange *dim = new VeriRange(new VeriIntVal(size-1), new VeriIntVal(0)) ;
    VeriDataType *data_type = new VeriEnum(VERI_BIT, 0, dim, veri_enum_ids) ;
    data_type->SetLinefile(Linefile()) ; // set linefile
    return data_type ;
}

// Convert scalar data type to verilog data type:
// For the time being we create integer type datatype for all scalar data type
// eg: vhdl declaration: type my_int is range 4 to 9
// corresponding verilog decalaration: typedef int my_int
VeriDataType *VhdlScalarTypeDef::ConvertDataType(VeriScope* /*scope*/, Array* /*type_id_arr*/, unsigned /*processing_type*/) const
{
    VeriDataType *data_type = new VeriDataType(VERI_INT, 0, 0) ;
    data_type->SetLinefile(Linefile()) ; // set linefile
    return data_type ;
}

// Convert vhdl array type to verilog array type with unconstraint ranges.
// The dimensions will be filled in during elaboration:
// eg: vhdl declaration: type vector is array (1 to 4, 4 downto 1) of bit
// corresponding verilog decalaration: typedef bit vector [1:4][4:1] ;
VeriDataType *VhdlArrayTypeDef::ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const
{
    if (!scope || (type_id_arr && !type_id_arr->Size())) return 0 ;

    unsigned is_constraint = 0 ;
    unsigned is_unconstraint = 0 ;
    unsigned i ;
    VhdlDiscreteRange *range ;
    VeriRange *veri_first_range = 0 ;
    VeriRange *veri_range = 0 ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, range) {
        if (!range) continue ;
        VeriRange *prev_range = veri_range ;
        veri_range = range->ConvertRange(scope, type_id_arr) ; // return verilog unconstaraint ranges
        if (!veri_range) {
            if (is_constraint) { // mixed up of constriant and unconstraint range is not yet supported:VHDL-2008 feature
                Error("VHDL 1076-2008 construct not yet supported") ;
                return 0 ;
            }
            is_unconstraint = 1 ;
            continue ; // for vhdl box (<>)
        }
        if (is_unconstraint) { // mixed up of constriant and unconstraint range is not yet supported:VHDL-2008 feature
            Error("VHDL 1076-2008 construct not yet supported") ;
            return 0 ;
        }
        is_constraint = 1 ;
        if (!veri_first_range) veri_first_range = veri_range ;
        if (prev_range) prev_range->SetNext(veri_range) ;
    }

    // Add range to all the type ids: create an unpacked array:
    if (veri_first_range) {
        VeriMapForCopy old2new ;
        VeriIdDef *type_id ;
        FOREACH_ARRAY_ITEM(type_id_arr, i, type_id) {
            if (!type_id) continue ;
            type_id->SetMemoryRange((i>0) ? veri_first_range->CopyRange(old2new): veri_first_range) ;
        }
    }

    VeriDataType *data_type = 0 ;
    // Create datatype from subtype_indication:
    if (_subtype_indication) data_type = _subtype_indication->ConvertDataType(scope, type_id_arr, processing_type) ;

    return data_type ;
}

// Convert vhdl record type to verilog structure type:
// eg: vhdl declaration:
//     type st_pack is record
//         a: bit_vector (3 downto 0); b: bit; c: integer;
//     end record;
// corresponding verilog decalaration:
//     typedef struct {
//         bit [3:0] a ; bit b ; int c ;
//     } st_pack ;
VeriDataType *VhdlRecordTypeDef::ConvertDataType(VeriScope *scope, Array* /*type_id_arr*/, unsigned processing_type) const
{
    if (!scope) return 0 ;

    // Create a new structure scope as a child of the verilog scope:
    VeriScope *struct_scope = new VeriScope(scope, 0) ;

    // Array for containing all the structure declarations:
    Array *decl_list = new Array(_element_decl_list ? _element_decl_list->Size() : 2) ;
    unsigned i ;
    VhdlElementDecl *elem_decl ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, elem_decl) {
        if (!elem_decl) continue ;

        Array *ids = elem_decl->GetIds() ;
        Array *veri_ids = new Array(ids ? ids->Size() : 2) ;
        unsigned j ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            char *name = Strings::save(id->Name()) ;
            char *lower_case_name = Strings::strtolower(name) ;
            VeriIdDef *veri_id = new VeriVariable(lower_case_name) ;
            veri_id->SetLinefile(id->Linefile()) ; // set linefile
            if (struct_scope) (void) struct_scope->Declare(veri_id) ;
            veri_ids->InsertLast(veri_id) ;
        }

        VhdlSubtypeIndication *subtype_indication = elem_decl->GetSubtypeIndication() ;
        // Create data type from subtype_indication:
        VeriDataType *data_type = subtype_indication ? subtype_indication->ConvertDataType(struct_scope, veri_ids, processing_type) : 0 ;

        // Create verilog data declaration for each vhdl record item declaration
        VeriDataDecl *data_decl = new VeriDataDecl(VERI_REG, data_type, veri_ids) ;
        decl_list->InsertLast(data_decl) ;
    }

    // Create structure declaration:
    VeriDataType *struct_data_type = new VeriStructUnion(VERI_STRUCT, 0, 0, decl_list, struct_scope) ;
    struct_data_type->SetLinefile(Linefile()) ; // set linefile
    return struct_data_type ;
}

// Convert data type:
VeriDataType *VhdlExplicitSubtypeIndication::ConvertDataType(VeriScope *scope, Array* /*type_id_arr*/, unsigned /*processing_type*/) const
{
    if (!scope) return 0 ;

    const char *name = _type_mark ? _type_mark->Name() : 0 ;
    VeriIdDef *veri_id = scope->Find(name) ;
    if (veri_id) {
        VeriTypeRef *veri_type_ref = new VeriTypeRef(veri_id, 0, 0) ;
        veri_type_ref->SetLinefile(Linefile()) ; // set linefile
        return veri_type_ref ;
    } else {
        unsigned token = VhdlNode::GetToken(name) ; // return corresponding token for verilog buildin data type
        VeriDataType *data_type = new VeriDataType(token, 0, 0) ;
        data_type->SetLinefile(Linefile()) ; //set linefile
        return data_type ;
    }
}

// Convert to verilog data type:
VeriDataType *VhdlIdRef::ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned processing_type) const
{
    if (!scope || (type_id_arr && !type_id_arr->Size())) return 0 ;

    char *name = Strings::save(Name()) ;
    char *lower_case_name = Strings::strtolower(name) ;
    VeriIdDef *veri_id = scope->Find(lower_case_name) ;

    if (!veri_id) {
        unsigned token = VhdlNode::GetToken(name) ; // return corresponding token for verilog buildin data type
        if (token) {
            if ((processing_type != PROCESSING_PREFIX) && _single_id && _single_id->IsUnconstrainedArrayType()) {
                unsigned i ;
                VeriIdDef *id ;
                FOREACH_ARRAY_ITEM(type_id_arr, i, id) {
                    if (!id) continue ;
                    id->IncrPackedUnconstraintDimCount(1) ;
                }
            }
            VeriDataType *data_type = new VeriDataType(token, 0, 0) ;
            data_type->SetLinefile(Linefile()) ;
            Strings::free(name) ;
            return data_type ;
        }
    }

    // Viper 8182 : Create data types for Verilog corresponding to signed and unsigned and other
    // predefined types in vhdl.
    if (!veri_id) {
        if (_single_id && _single_id->IsType()) {
            VhdlDeclaration *type_decl = _single_id->GetDeclaration() ;
            if (type_decl) {
                VeriDataDecl *tmp_data_decl = type_decl->ConvertDataDecl(scope) ;
                delete (tmp_data_decl) ;
            }
                veri_id = scope->Find(lower_case_name) ;
        }
    }

    if (veri_id) {
        VeriTypeRef *data_type = new VeriTypeRef(veri_id, 0, 0) ;
        data_type->SetLinefile(Linefile()) ;
        Strings::free(name) ;
        // Mark unconstraint only if we are processing a subtype declaration and
        // not processing a prefix data type
        //if((processing_type == PROCESSING_SUBTYPE_DECL) && (processing_type != PROCESSING_PREFIX)) {
        // Mark unconstraint only if we are not processing a prefix data type
        unsigned num_unconstraint = veri_id->GetUnpackedUnconstraintDimCount() + veri_id->GetPackedUnconstraintDimCount() ;
        if((processing_type != PROCESSING_PREFIX) && num_unconstraint) {
            unsigned i ;
            VeriIdDef *id ;
            FOREACH_ARRAY_ITEM(type_id_arr, i, id) {
                if (!id) continue ;
                id->IncrUnpackedUnconstraintDimCount(veri_id->GetUnpackedUnconstraintDimCount()) ;
                id->IncrPackedUnconstraintDimCount(veri_id->GetPackedUnconstraintDimCount()) ;
            }
        }
        return data_type ;
    } else {
        Error ("%s is not declared", name) ;
        Strings::free(name) ;
        return 0 ;
    }
}

// Convert to verilog data type from vhdl indexed name datatype:
// eg: vhdl indexedname: type vector is array (5 to 9) of bit_vector (0 to 5)
// verilog datatype: typedef bit [0:5] vector [5:9]
// Consider the following example:
// subtype bit_arr1 is std_ulogic_vector (0 to 8)
// subtype bit_a is std_logic_vector  /* we need to mark it as unconstraint/*
// in this case while converting the prefix data type, first we mark type id as
// unconstraint if it is a build in type and vhdl _single_id is unconstraint array
// type. After that if the _prefix_id is a build in type and it is also unconstraint
// array type then decrement unconstraint count of the type id
VeriDataType *VhdlIndexedName::ConvertDataType(VeriScope *scope, Array *type_id_arr, unsigned /*processing_type*/) const
{
    if (!scope || (type_id_arr && !type_id_arr->Size())) return 0 ;

    // Mark that we are processing prefix: so call convert on datatype with PROCESSING_PREFIX
    VeriDataType *data_type = _prefix ? _prefix->ConvertDataType(scope, type_id_arr, PROCESSING_PREFIX) : 0 ;

    unsigned is_constraint = 0 ;
    unsigned is_unconstraint = 0 ;
    unsigned i ;
    VhdlDiscreteRange *range ;
    VeriRange *veri_first_range = 0 ;
    VeriRange *veri_range = 0 ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, range) {
        if (!range) continue ;
        VeriRange *prev_range = veri_range ;
        veri_range = range->ConvertRange(scope, type_id_arr) ;
        if (!veri_range) {
            if (is_constraint) { // mixed up of constriant and unconstraint range is not yet supported:VHDL-2008 feature
                Error("VHDL 1076-2008 construct not yet supported") ;
                return 0 ;
            }
            is_unconstraint = 1 ;
            continue ; // for vhdl box (<>)
        }
        if (is_unconstraint) { // mixed up of constriant and unconstraint range is not yet supported:VHDL-2008 feature
            Error("VHDL 1076-2008 construct not yet supported") ;
            return 0 ;
        }
        is_constraint = 1 ;
        if (!veri_first_range) veri_first_range = veri_range ;
        if (prev_range) prev_range->SetNext(veri_range) ;
    }

    if (veri_first_range) {
        VeriIdDef *data_type_id = data_type ? data_type->GetId() : 0 ;
        unsigned num_unconstraint = data_type_id ? data_type_id->GetUnpackedUnconstraintDimCount() + data_type_id->GetPackedUnconstraintDimCount() : 0 ;
        if (data_type_id && num_unconstraint) { // if data type is buildin data type(std_logic/bit_vector ect) then make packed data type
            VeriMapForCopy old2new ;
            VeriIdDef *type_id ;
            FOREACH_ARRAY_ITEM(type_id_arr, i, type_id) {
                if (!type_id) continue ;
                type_id->AddMemoryRange((i>0) ? veri_first_range->CopyRange(old2new) : veri_first_range) ;
            }
        } else {
            if (data_type) data_type->SetDimensions(veri_first_range) ; // if data type is user defined data type then make unpackade data type
        }
    }
    return data_type ;
}

// Create verilog unconstraint range:
VeriRange *VhdlRange::ConvertRange(VeriScope* /*scope*/, Array* /*type_id_arr*/) const
{
    VeriRange *veri_range = new VeriRange(0, 0, VERI_UNCONSTRAINT) ;
    veri_range->SetLinefile(Linefile()) ;
    return veri_range ;
}

// Convert to verilog range
VeriRange *VhdlExplicitSubtypeIndication::ConvertRange(VeriScope *scope, Array *type_id_arr) const
{
    if (!scope || (type_id_arr && !type_id_arr->Size())) return 0 ;

    if (_range_constraint) return (_range_constraint->ConvertRange(scope, type_id_arr)) ;
    return 0 ;
}

// For vhdl boxes we set a flag that indicates the number of unconstraint ranges
VeriRange *VhdlBox::ConvertRange(VeriScope* /*scope*/, Array *type_id_arr) const
{
    if (type_id_arr && !type_id_arr->Size()) return 0 ;

    unsigned i ;
    VeriIdDef *type_id ;
    FOREACH_ARRAY_ITEM(type_id_arr, i, type_id) {
        if (!type_id) continue ;
        type_id->IncrUnpackedUnconstraintDimCount(1) ;
    }
    return 0 ;
}

// Convert to verilog range: marked it as unconstraint
// vhdl range: type my_enum is (RED, GREEN, BLUE) ;
//             type my_enum1 is array (RED to BLUE) of my_enum ;
// verilog range: my_enum1 [/*unconstraint*/]
VeriRange *VhdlIdRef::ConvertRange(VeriScope* /*scope*/, Array* /*type_id_arr*/) const
{
    VeriRange *veri_range = new VeriRange(0, 0, VERI_UNCONSTRAINT) ;
    veri_range->SetLinefile(Linefile()) ;
    return veri_range ;
}

// Convert to verilog range: marked it as unconstraint
// vhdl range: type vector1 is array (5 to 9) of integer range 4 to 9
//             type vector is array (vector1'reverse_range) of vector1
// corresponding verilog range: verctor [/*unconstriant*/]
VeriRange *VhdlAttributeName::ConvertRange(VeriScope* /*scope*/, Array* /*type_id_arr*/) const
{
    VeriRange *veri_range = new VeriRange(0, 0, VERI_UNCONSTRAINT) ;
    veri_range->SetLinefile(Linefile()) ;
    return veri_range ;
}

// Create verilog expression from vhdl value:
VeriExpression *VhdlAccessValue::ConvertToVerilogExpression(VeriScope* /*scope*/, VhdlIdDef* /*type*/, const linefile_type /*line_file*/)
{
    return 0 ; // do nothin for VhdlAccessValue
}

// Create integer expression for vhdl integer value
VeriExpression *VhdlInt::ConvertToVerilogExpression(VeriScope* /*scope*/, VhdlIdDef* /*type*/, const linefile_type line_file)
{
    verific_int64 int_val = Integer() ;
    VeriExpression *veri_expr = new VeriIntVal((int)int_val) ;
    veri_expr->SetLinefile(line_file) ;

    return veri_expr ;
}

// Create real expression for vhdl real value
VeriExpression *VhdlDouble::ConvertToVerilogExpression(VeriScope* /*scope*/, VhdlIdDef* /*type*/, const linefile_type line_file)
{
    double real_val = Real() ;
    VeriExpression *veri_expr = new VeriRealVal(real_val) ;
    veri_expr->SetLinefile(line_file) ;
    return veri_expr ;
}

// Create verilog expression for vhdl enum
VeriExpression *VhdlEnum::ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef* /*type*/, const linefile_type line_file)
{
    if (!scope) return 0 ;

    VERIFIC_ASSERT(_enum_id) ;

    VeriExpression *veri_expr = 0 ;
    // If this is a bit-encoded literal, print the bit-encoding as Verilog literal :
    // use verilog x for VHDL enums W, X and -
    // use verilog 0 for L and 0
    // use 1 for VHDL H and 1
    // and use verilog z for VHDL Z:
    switch (_enum_id->GetBitEncoding()) {
    case 'L' :
    case '0' :
        veri_expr = new VeriConstVal("1'b0", VERI_UNSIZED_LITERAL, 1) ;
        break ;
    case 'H' :
    case '1' :
        veri_expr = new VeriConstVal("1'b1", VERI_UNSIZED_LITERAL, 1) ;
        break ;
    case 'W' :
    case '-' :
    case 'X' :
        veri_expr = new VeriConstVal("1'bx", VERI_UNSIZED_LITERAL, 1) ;
        break ;
    case 'Z' :
        veri_expr = new VeriConstVal("1'bz", VERI_UNSIZED_LITERAL, 1) ;
        break ;
    default :
        char *name = Strings::save(_enum_id->OrigName()) ; // not (single) bit-encoded
        char *lower_case_name = Strings::strtolower(name) ;
        VeriIdDef *enum_id = scope->Find(lower_case_name) ;
        if (enum_id) veri_expr = new VeriIdRef(enum_id) ;
        Strings::free(name) ;
        break ;
    }

    if (veri_expr) veri_expr->SetLinefile(line_file) ;
    return veri_expr ;
}

// Create verilog expression for non constant vhdl value:
VeriExpression *VhdlNonconst::ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file)
{
    if (!scope) return 0 ;

    VhdlValue *value = ToConst(type) ; // convert to constant value
    return (value ? value->ConvertToVerilogExpression(scope, type, line_file) : 0) ;
}

// Create verilog expression for non constant bit vhdl value:
VeriExpression *VhdlNonconstBit::ConvertToVerilogExpression(VeriScope *scope, VhdlIdDef *type, const linefile_type line_file)
{
    if (!scope) return 0 ;
    VERIFIC_ASSERT(_net) ;

    VhdlValue *value = ToConst(type) ; // convert to constant value
    return (value? value->ConvertToVerilogExpression(scope, type, line_file) : 0) ;
}

// Create verilog expression for vhdl composite value:
VeriExpression *VhdlCompositeValue::ConvertToVerilogExpression(VeriScope* scope, VhdlIdDef* type, const linefile_type line_file)
{
    if (!scope) return 0 ;
    VERIFIC_ASSERT(_values) ;

    unsigned i ;
    VhdlValue *elem ;
    char *tmp2 ;
    char *result = 0 ;
    // Get the element type.
    // This could be a 'virtual' type, if value has multiple dimensions.
    // side effect: Need to check all the value elements for their types.
    unsigned all_character = 1 ;
    unsigned all_bit_encoded = 1 ;
    VhdlIdDef *elem_type ;
    FOREACH_ARRAY_ITEM(_values, i, elem) {
        if (!elem) elem = _default_elem_val ;
        elem_type = (elem) ? elem->GetType() : 0 ;
        // If the type cannot be determined, resert this flags and break out of the loop:
        if (!elem_type) { all_character = 0 ; all_bit_encoded = 0 ; break ; }
        // Compare the type of this element with these types:
        if (all_character && !Strings::compare_nocase(elem_type->Name(), "character")) all_character = 0 ;
        if (!elem_type->Constraint() || !elem_type->Constraint()->IsBitEncoded()) all_bit_encoded = 0 ;
        if (!all_character && !all_bit_encoded) break ; // No need to continue
    }

    if (!_values->Size()) {
        // empty array. Cannot print that any legal way in Verilog, only as an empty string...
        // That's also probably how it originated...
        result = Strings::save("\"\"") ;
        VeriExpression *expr = new VeriConstVal(result, VERI_STRING) ;
        expr->SetLinefile(line_file) ;
        Strings::free(result) ; // VeriConstVal ctor does not free result
        return expr ;
    } else if (all_character) {
        // Array of character.. Print as a string.
        result = Strings::save("\"") ;
        char c ;
        char tmp[3] ;
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            c = (char)elem->Position() ; // ASCII position of character
            ::sprintf(tmp,"%c",c) ;
            tmp2 = result ;
            result = Strings::save(result,tmp) ;
            Strings::free( tmp2 ) ;
        }
        tmp2 = result ;
        result = Strings::save(result,"\"") ;
        Strings::free( tmp2 ) ;
        VeriExpression *expr = new VeriConstVal(result, VERI_STRING) ;
        expr->SetLinefile(line_file) ;
        Strings::free(result) ; // VeriConstVal stor does not free result
        return expr ;
    } else {
        Array *veri_expr_arr = new Array() ;
        // regular array. Print as Verilog aggregate ?
        FOREACH_ARRAY_ITEM(_values, i, elem) {
            if (!elem) elem = _default_elem_val ;
            if (!elem) continue ;
            VeriExpression *expr = elem->ConvertToVerilogExpression(scope, type, line_file) ; // call on the elements.
            veri_expr_arr->InsertLast(expr) ;
        }
        // Create a assignment parttern:
        VeriExpression *veri_expr = new VeriAssignmentPattern(0, veri_expr_arr) ;
        veri_expr->SetLinefile(line_file) ;
        return veri_expr ;
    }
    return 0 ;
}

// Fill up array constraints:
void VhdlArrayConstraint::FillUpRanges(VeriScope *scope, VeriIdDef *id, unsigned pos, const linefile_type line_file)
{
    if (!scope || !id) return ;

    // Fill up the verilog ranges during elaboration: left unconstraint during analysis
    if (_index_constraint && !_index_constraint->IsUnconstrained()) {
        VhdlValue *left_bound = Left() ;
        VhdlValue *right_bound = Right() ;
        VeriExpression *left = left_bound ? left_bound->ConvertToVerilogExpression(scope, left_bound->GetType(), line_file) : 0 ;
        VeriExpression *right = right_bound ? right_bound->ConvertToVerilogExpression(scope, right_bound->GetType(), line_file) : 0 ;

        // Get dimension at specific position of the range
        // and fill it up by newly created verilog expression:
        VeriRange *veri_range = id->GetDimensionAt(pos) ;
        if (!veri_range) {
            veri_range = new VeriRange(left, right) ;
            veri_range->SetLinefile(line_file) ;
            id->AddMemoryRange(veri_range) ;
        } else {
            if (!veri_range->GetPartSelectToken()) {
                delete left ;
                delete right ;
            } else {
                veri_range->SetLeft(left) ;
                veri_range->SetRight(right) ;
                // mark the dimension as constriant by making part_select_token as zero
                veri_range->SetPartSelectToken(0) ;
            }
        }
    }

    if (_element_constraint) _element_constraint->FillUpRanges(scope, id, (_index_constraint && _index_constraint->IsUnconstrained()) ? pos : pos+1, line_file) ;
}

// Fill up record constraints:
void VhdlRecordConstraint::FillUpRanges(VeriScope* /*scope*/, VeriIdDef *id, unsigned /*pos*/, const linefile_type line_file)
{
    if (!id) return ;

    // Get the structure data type
    VeriDataType *data_type = id->GetDataType() ;
    VeriScope *veri_scope = data_type ? data_type->GetScope() : 0 ; // Get the record scope

    // Fill up rannges for each record elements:
    unsigned i ;
    VhdlIdDef *ele_id ;
    FOREACH_ARRAY_ITEM(_element_ids, i, ele_id) {
        if (!ele_id) continue ;
        VhdlConstraint *constraint = _element_constraints ? (VhdlConstraint*)_element_constraints->At(i) : 0 ;
        const char *name = ele_id->Name() ;
        VeriIdDef *veri_id = veri_scope ? veri_scope->Find(name) : 0 ;
        if (constraint) constraint->FillUpRanges(veri_scope, veri_id, 0, line_file) ;
    }
}

// VIPER #6896: Convert verilog package to vhdl one: Fill up the vhdl ranges
// with vhdl expression during elaboration:
void VhdlPackageDecl::FillRangesAndInitialExpr()
{
    // Do nothing if the package is not a converted vhdl package or already elaborated
    if (!IsVerilogModule() || IsAlreadyProcessed()) return ;

    // Set already processed flag when initializing the packege first time.
    // We may come to initialize the package again if the package is imported
    // to more than one design:
    SetAlreadyProcessed(1) ; // set already processed flag when initializing the packege first time

    const char *pkg_name = Name() ;
    VhdlLibrary *lib = GetOwningLib() ; // Get the vhdl library
    const char *lib_name = lib ? lib->Name() : "work" ;
    // Get the correspondig vhdl package from the library
    VeriModule *veri_package = vhdl_file::GetVerilogModuleFromlib(lib_name, pkg_name) ; //Search the Verilog module and return

    if (!veri_package) return ;

    // Get the verilog scope:
    VeriScope *veri_scope = veri_package->GetScope() ;

    // type_id_map collects the declarations that are newly created for array/enum declarations within structures
    // or for enum/structure array
    Map type_id_map(POINTER_HASH) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        VhdlIdDef *type_id = decl->GetTypeId() ;
        const char *name = type_id ? type_id->Name() : 0 ;
        char *veri_name = UnescapeVhdlName(name) ;
        if (!veri_name) veri_name = Strings::save(name) ;
        if (veri_scope && veri_scope->Find(veri_name)) {
            Strings::free(veri_name) ;
            continue ;
        }
        Strings::free(veri_name) ;
        type_id_map.Insert(type_id, decl) ;
    }

    // Keep the present scope.
    // Store the structure constraints: for deleting the element constraints later as only
    // one copy of the element constraints are generated and declared in the scope, so element
    // constraints are not deleted by destructor of constraint rather they have to be deleted from the scope
    VeriScope *save_scope = VeriNode::_present_scope ;
    VeriNode::_present_scope = veri_scope ;
    Map *save_struct_constraints = (veri_scope) ? veri_scope->GetStructConstraints(): 0 ;
    if (veri_scope) veri_scope->SetStructConstraints(0) ;

    // Fillup all the ranges left unfilled in analysis part
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        VhdlIdDef *type_id = decl->GetTypeId() ;
        const char *name = type_id ? type_id->Name() : 0 ;
        char *veri_name = UnescapeVhdlName(name) ;
        if (!veri_name) veri_name = Strings::save(name) ;
        VeriIdDef *veri_id =  veri_scope ? veri_scope->Find(veri_name) : 0 ;
        if (!veri_id) { // do not fillup ranges for new declarations created during conversion: will be fillup from their references
            Strings::free(veri_name) ;
            continue ;
        }
        Strings::free(veri_name) ;
        VeriConstraint *constraint = veri_id->EvaluateConstraintInternal(0, 0, 0) ; // Get the verilog constraints
        decl->FillUpRanges(veri_scope, constraint, type_id_map) ;
        delete constraint ;
    }

    // Delete the structure constraints
    if (veri_scope) {
        veri_scope->DeleteStructConstraints() ;
        veri_scope->SetStructConstraints(save_struct_constraints) ;
    }
    // restore the present scope
    VeriNode::_present_scope = save_scope ;
}

// Fillup ranges for fulltype declarations
void VhdlFullTypeDecl::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map)
{
    if (!veri_scope || !constraint) return ;

    if (_type_def) _type_def->FillUpRanges(veri_scope, constraint, type_id_map) ;
}

// Fillup ranges for subtype declarations
void VhdlSubtypeDecl::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map)
{
    if (!veri_scope || !constraint) return ;

    if (_subtype_indication) _subtype_indication->FillUpRanges(veri_scope, constraint, type_id_map) ;
}

// Fillup ranges for element declarations
void VhdlElementDecl::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map)
{
    if (!veri_scope || !constraint) return ;

    unsigned i ;
    VhdlElementId *elem_id ;
    FOREACH_ARRAY_ITEM(_id_list, i, elem_id) {
        if (!elem_id) continue ;
        if (_subtype_indication) _subtype_indication->FillUpRanges(veri_scope, constraint, type_id_map) ;
    }
}

// Fillup ranges for array datatype
void VhdlArrayTypeDef::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map)
{
    if (!veri_scope || !constraint) return ;

    unsigned i ;
    VhdlDiscreteRange *range ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, range) {
        if (!range) continue ;
        if (constraint && constraint->IsPacked() && constraint->PackedDimCount() > 1) { // For packed simension we marge the multiple dimensions into one dimension
            unsigned length = 0 ;
            while(constraint) {
                length = length + constraint->Length() ;
                constraint = constraint->ElementConstraint() ; // Get the next dimension
            }
            unsigned left_bound = (length > 0) ? length-1 : length ;
            unsigned right_bound = 0 ;
            range->FillRange(left_bound, right_bound) ;

        } else {
            unsigned left_bound  = (constraint) ? constraint->Left()  : 0 ;
            unsigned right_bound = (constraint) ? constraint->Right() : 0 ;
            range->FillRange(left_bound, right_bound) ;

            constraint = (constraint) ? constraint->ElementConstraint() : 0 ; // Get the next dimension
        }
    }

    if (_subtype_indication) {
        // Enumeration array type not supported in cross language port/generic connection:
        // Take the following example where size of enum type is specified:
        // verilog data declaration:
        //     typedef enum bit [3:0] {one, two, three} my_enum ;
        //     typedef my_enum en_arr [1:3] ;
        // corresponding vhdl data declaration is:
        //     type my_enum is (one,two,three);
        //     type en_arr is array (1 to 3) of my_enum;
        // in verilog declaration each enum element is of 4 bits and so the element size of array is of 4 bits.
        // and in corresponding vhdl declaration we need 2 bits for 3 enum elements, so the element size of array
        // is of 2 bits. Hence port connection mismatch occurred.
        // But if we modify the first verilog declaration as
        //     typedef enum bit [1:0] {one, two, three} my_enum ;
        // then we have no issue in port connection.

        // Take another example where size of enum type is not specified:
        // verilog data declaration:
        //     typedef enum {red, green, blue} en ;
        //     typedef en en_arr [1:3] ;
        // corresponding vhdl data declaration is:
        //     type en is (red,green,blue);
        //     type en_arr is array (1 to 3) of en;
        // in verilog declaration as size of enum type is not specified, it will
        // be 32 bits and in corresponding vhdl declaration we need 2 bits
        // for 3 enum elements, hence port connection mismatch occurred.
        VhdlIdDef *type_id = _subtype_indication->GetId() ;
        if (type_id && type_id->IsEnumerationType()) {
            unsigned num_bits = constraint ? constraint->NumOfBits() : 0 ;
            Array *enum_ids = constraint ? constraint->GetEnumeratedIds() : 0 ;
            unsigned num_enums = enum_ids ? enum_ids->Size() : 0 ;
            unsigned size = 0 ;
            unsigned num = num_enums>0 ? num_enums-1 : 0 ;
            for (; num!=0; num=num/2) size++ ;
            if (!size) size++ ; // Use one bit for value 0

            if (num_bits != size) {
                _subtype_indication->Warning("enumeration array type not supported in cross language port/generic connection") ;
            }
        }
        _subtype_indication->FillUpRanges(veri_scope, constraint, type_id_map) ;
    }
}

// Fillup ranges for record datatype
void VhdlRecordTypeDef::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map)
{
    if (!veri_scope || !constraint) return ;

    Array *element_constraints = constraint->GetElementConstraints() ;
    unsigned i ;
    VhdlElementDecl *decl ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, decl) {
        if (!decl) continue ;
        VeriConstraint *element_constraint = element_constraints ? (VeriConstraint*) element_constraints->At(i) : 0 ;
        decl->FillUpRanges(veri_scope, element_constraint, type_id_map) ;
    }
}

// Fillup ranges for indexed name
void VhdlIndexedName::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map& /*type_id_map*/)
{
    if (!veri_scope || !constraint) return ;

    unsigned i ;
    VhdlRange *range ;
    FOREACH_ARRAY_ITEM(_assoc_list, i, range) {
        if (!range) continue ;
        if (constraint && constraint->IsPacked() && constraint->PackedDimCount() > 1) { // For packed simension we marge the multiple dimensions into one dimension
            unsigned length = 1 ;
            while(constraint && constraint->ElementConstraint()) {
                length = length * constraint->Length() ;
                constraint = constraint->ElementConstraint() ; // Get the next dimension
            }
            unsigned left_bound = (length > 0) ? length-1 : length ;
            unsigned right_bound = 0 ;
            range->FillRange(left_bound, right_bound) ;
        } else {
            unsigned left_bound = (constraint) ? constraint->Left() : 0 ;
            unsigned right_bound = (constraint) ? constraint->Right() : 0;
            range->FillRange(left_bound, right_bound) ;

            constraint = (constraint) ? constraint->ElementConstraint() : 0 ; // Get the next dimension
        }
    }
}

// Fillup ranges for references: fillup the newly created declarations
void VhdlIdRef::FillUpRanges(VeriScope *veri_scope, VeriConstraint *constraint, Map &type_id_map)
{
    VhdlDeclaration *decl = (VhdlDeclaration *)type_id_map.GetValue(_single_id) ;
    if (decl) decl->FillUpRanges(veri_scope, constraint, type_id_map) ;
}

// Viper 6896 : Find the Verilog Type corresponding to the converted Vhdl type
VeriIdDef *VhdlTypeId::GetVerilogTypeFromPackage() const
{
    if (!IsVerilogType()) return 0 ;

    VeriModule *veri_package = vhdl_file::GetVerilogModuleFromlib(_verilog_pkg_lib_name, _verilog_pkg_name) ; //Search the Verilog module and return

    if (veri_package) return veri_package->GetMatchingTypeId(Name()) ;

    return 0 ;
}

VeriIdDef *VeriPackage::GetMatchingTypeId(const char* type_name) const
{
     unsigned i ;
     VeriModuleItem *item ;

     char *vhdl_type_name = VhdlNode::UnescapeVhdlName(type_name) ;
     if (!vhdl_type_name) vhdl_type_name = Strings::save(type_name) ;

     FOREACH_ARRAY_ITEM(_module_items, i, item) {
         if (!item || !item->IsDataDecl()) continue ;
         Array *ids = item->GetIds() ; // This is a data declaration.
         unsigned j ;
         VeriIdDef *id ;
         FOREACH_ARRAY_ITEM(ids, j, id) {
             if (!id) continue ;
             if (Strings::compare(id->GetName(), vhdl_type_name)) {
                 Strings::free(vhdl_type_name) ;
                 return id ;
             }
         }
     }
     Strings::free(vhdl_type_name) ;
     return 0 ;
}
#endif
