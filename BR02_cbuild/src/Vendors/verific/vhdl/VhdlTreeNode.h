/*
 *
 * [ File Version : 1.198 - 2014/02/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_TREE_NODE_H_
#define _VERIFIC_VHDL_TREE_NODE_H_

#include <iosfwd> // iostream inclusion needed for PrettyPrint
using std::ostream ;

#include "VerificSystem.h"
#include "VhdlCompileFlags.h" // Vhdl-specific compile flags

#include "LineFile.h"         // For linefile_type
#include "VhdlClassIds.h"     // Get class id

#include "VhdlVisitor.h"      // Visitor design pattern

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Netlist ;
class Net ;
class Instance ;
class Library ;
class Array ;
class Map ;
class Set ;
class SaveRestore;

class VhdlTreeNode ;
class VhdlDesignator ;
class VhdlScope ;
class VhdlDesignUnit ;
class VhdlIdDef ;
class VhdlValue ;
class VhdlConstraint ;
class VhdlMapForCopy ;
class VhdlLibrary ;

class VhdlDataFlow ;

class VhdlInterfaceId ;
class VhdlRange ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Convert vhdl package to verilog package
class VeriScope ;
class VeriRange ;
#endif

/* -------------------------------------------------------------- */

/*
   VhdlNode is the abstract base class for ALL VHDL objects (parse-tree nodes and values).
   This class contains only static data members, and is used mainly to have system-wide
   static info available and organized.
*/
class VFC_DLL_PORT VhdlNode
{
public:
    VhdlNode() ;
    virtual ~VhdlNode() ;

private:
    // Prevent compiler from defining the following
    VhdlNode(const VhdlNode &) ;            // Purposely leave unimplemented
    VhdlNode& operator=(const VhdlNode &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Set mode of analyzer (87, 93, PSL dialect)
    static unsigned         IsVhdl87() ;  // Test if we are in Vhdl87 mode.
    static unsigned         IsVhdl93() ;  // Test if we are in Vhdl93 mode.(or PSL)
    static unsigned         IsVhdlPsl() ; // Test if we are in PSL mode
    static unsigned         IsVhdl2K() ;  // Test if we are in Vhdl 2000 mode
    static unsigned         IsVhdl2008() ;  // Test if we are in Vhdl 2008 mode
    static void             SetVhdl87() ;
    static void             SetVhdl93() ;
    static void             SetVhdl2K() ;
    static void             SetVhdl2008() ;
    static void             SetVhdlPsl() ;

    // Quick access to static names
    static const char *     OperatorSimpleName(unsigned oper_type) ; // Name of operator as it appears in VHDL
    static const char *     OperatorName(unsigned oper_type) ;       // Name of _oper operator, as stored in names tables
    static const char *     EntityClassName(unsigned entity_class) ; // Name of any entity class

    static unsigned         IsMathRealPragma(unsigned pragma_function) ; // Viper #8006: math-real functions annotated with foreign attribute

    // VIPER #3166 : Returns if the user the operator is a valid vhdl
    // operator as dictated by LRM section 7.2
    static unsigned         IsValidOperator(const char *op) ;
    static void             ResetOperatorsTable() ;

    // Pragma / Synthesis directives
    static unsigned         PragmaFunction(const char *name) ; // Value from a (Synopsys, Ambit, Synergy or Exemplar) pragma, derive the function
    static unsigned         PragmaSigned(const char *name) ;   // Return 1 or 0 from a (Synopsys) pragma, see if it interprets arguments as signed
    static unsigned         PragmaKnown(const char *name) ;    // Return 1 or 0 if the pragma is known (PragmaFunction returns 0 for both unknown as well as known functions that should be ignored
    static void             ResetPragmaTables() ;

    // Get quick access to built-in types
    static VhdlIdDef *      UniversalInteger() ;
    static VhdlIdDef *      UniversalReal() ;
    static VhdlIdDef *      UniversalVoid() ;

    // Get quick access to standard types
    static VhdlIdDef *      StdType(const char *std_type_name) ;

    // Message handling support : Message id table, output formatted, varargs message functions
    static const Map *      GetMessageMap()    { (void) GetMessageId("") ; return _msgs ; } // Return the entire map : (char*)formatted_string -> (char*)message_id
    static const char *     GetMessageId(const char *msg) ;     // Aid to get message ID from formatted string. Accessed by Error/Warning/Error
    static void             ResetMessageMap() ;

    // Delete the comments (VhdlComment nodes) in the array.
    static void             DeleteComments(Array *comments) ;

    // returns newly allocated unescaped name, null if input is not escaped
    static char *           UnescapeVhdlName(const char *name) ;

    static unsigned         IsTickProtectedRtlRegion(unsigned update_status) ; // This is internal use only API called inside TreeNode constructor to set create special linefile to mark protected status. When update_status is true, this will reset the tick_protected_region flag, if the entire protected region is already parsed

    static void             SetConstNets() ; // Set constant nets.
    static void             ResetConstNets() ; // Reset constant nets.
    static unsigned         ContainsOnly(const Net *expr, const Net *net) ;
    // Apply re-naming to a flip-flop instance, following a net name
    // Elaboration related routines/members
public:
    // Mode of elaboration : Needs to be accessible from 'vhdl_file' class
    static unsigned         IsStaticElab()          { return _static_elab ; }           // Test if we are presently elaborating in 'static' mode
    static unsigned         IsRtlElab()             { return (_static_elab) ? 0 : 1 ; } // Test if we are presently elaborating in 'RTL' mode
    static void             SetStaticElab()         { _static_elab = 1 ; }
    static void             SetRtlElab()            { _static_elab = 0 ; }

    // Fast access to pwr/gnd/x nodes (during RTL elaboration)
    static inline Net *     Pwr()                   { return _pwr ; }
    static inline Net *     Gnd()                   { return _gnd ; }
    static inline Net *     X()                     { return _x ; }
    static inline Net *     Z()                     { return _z ; }
    static inline VhdlIdDef *BooleanType()          { return _boolean_type ; }

    // Some Net based utilities used during RTL elaboration.
    // Test nets for being constant
    static unsigned         ConstNet(const Net *n) ;

    // Named Path keeps track of block/function depth while initializing (during elaboration)
    // a unit.  It contains an array of Ids.  Needed to create meaningfull names in deeper
    // scopes than the top level.
    static void             ClearNamedPath() ;
    static void             PushNamedPath(const char *string, const char *iter_val=0) ;
    static void             PopNamedPath() ;
    // VIPER #7683 : Change the default value of separator from ":" to "." (to match verilog)
    // Change the 'separator' (make ":") to restore previous behaviour
    static char *           PathName(const char *name, const char *prefix = 0, const char *separator = ".", const char *suffix = 0) ;

    // VIPER #5051 :Store the path names to evaluate path_name/instance_name attributes
    static void             ClearAttributeSpecificPath() ;
    static void             PushAttributeSpecificPath(const char *label, const char *entity = 0, const char *arch = 0, const char *loop_value = 0) ;
    static void             PopAttributeSpecificPath() ;
    static char *           PathNameAttribute(const VhdlIdDef *id, unsigned instance_name, unsigned mark_entity) ;
    static void             CreateSubprogramPathName(const VhdlIdDef *subprog_id) ;
    static void             PropagateAffectOfPathName() ;
    static char *           GetCurrAttributeSpecificPath(const char *unit_name) ;
    static unsigned         IsEmptyAttributeSpecificPath() ;

    // Structural stack keeps track of instantiations ('from') of units (entity or architecture
    // or subprogram or so)  while elaborating.  Presently, its main purpose is to check that
    // structural recursion does not happen
    static void             ClearStructuralStack() ; // Clear all recursion specific stacks
    static unsigned         PushStructuralStack(const VhdlIdDef *unit, const VhdlTreeNode *from) ; // Create a new entry in value stack and constraint stack and returns 0 for infinite recursion
    static void             PopStructuralStack() ; // Pop last entered entry from value/constraint stack
    // VIPER 1863 & 1472  : begin
    static unsigned         IsStructuralRecursion() ; // Returns 1 if last entry of structural stack has recursion

    static unsigned         IsPresentInStructuralStack(const char *cell_name, const char *arch_name) ;

    static void             SwitchValueConstraintId(VhdlIdDef *id) ; // Switch value/constraint of argument specific id
    static void             SwitchValue(VhdlIdDef *id) ;           // Switch value of argument specific id
    static void             SwitchConstraint(VhdlIdDef *id) ;      // Switch constraint of argument specific id
    static void             SwitchActualId(VhdlIdDef *id) ;      // Switch actual id of argument specific id
    static void             SwitchValueConstraintOfScope(const VhdlScope *scope, unsigned only_generic_ids = 0) ; // Switch value/constraint of all 'scope' specific iddefs
    static void             SwitchValueConstraintOfInterfaceIds(const VhdlScope *scope) ; // Switch value/constraint of all 'scope' specific iddefs
    static Map *            GetValueMap() ;                        // Return last entered id vs value map from value stack
    static Map *            GetConstraintMap() ;                   // Return last entered id vs constraint map from constraint stack
    static Map *            GetActualIdMap() ;                   // Return last entered id vs actual id map from actual id stack
    // VIPER 1863 & 1472  : end

    static void             EvaluateConstraintForPartialAssoc(const Array *assoc_list, VhdlDiscreteRange const *this_assoc, VhdlDataFlow *df) ;

    static void             CheckOverlapAndCompletenessOfPartialFormal(const Array *assoc_list, VhdlIdDef const *this_formal) ;

    static void             TakePartialValue(VhdlValue *&full_value, VhdlConstraint *&full_constraint, VhdlDiscreteRange const *partial_formal, unsigned take) ;

    static void             TakePartialValue(VhdlDiscreteRange *element, VhdlConstraint *&full_constraint, VhdlValue *&full_value, unsigned take) ;

    static unsigned         IsEmptyValue(VhdlValue const *val) ;

private:
    // Access Value cleanup: (VIPER #4464, 7292)
    static void             CollectAllValues(VhdlValue const *val, Set *elements, unsigned stop_at_access = 0) ;

public:
    // Access Value cleanup: (VIPER #4464, 7292)
    static void             SetInnerAccessValue(VhdlValue const *val) ;
    static void             SetDeallocatedValue(VhdlValue const *val) ;
    static unsigned         IsDeallocatedValue(VhdlValue const *val) ;
    static void             FreeAccessValueSet(Set *inner_value_set) ;
    static void             ResetAccessValueSet() ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Conversion of vhdl package to verilog package
    static unsigned         GetToken(const char *name) ; // string matching of vhdl builtin data type and return the corresponding verilog one
    static VhdlRange*       CreateConversionRange(linefile_type line_file) ; // Create vhdl special handling range
#endif

private:
    static Map       *_msgs ;                // List of all messages. message->ID table. Accessed only from GetMessageId routine.
    //static unsigned   _analysis_mode ;       // Flag to indicate if we are parsing '87 or '93, or PSL dialect. (already moved in vhdl_file)
    static Set       *_operators ;           // List of valid vhdl operators (LRM : 7.2, VIPER #3166) - in a set for quick check
    static Map       *_pragma_table ;        // List of known pragma's (from various synthesis vendors)
    static Set       *_pragma_signed_table ; // List of known 'signed' function pragma's.

public:
    static VhdlScope *_present_scope ;       // Scope pointer while we are analyzing. Needed for yacc.

protected:
    // Static's set during RTL elaboration.
    static Netlist   *_nl ;                  // Present netlist
    static Net       *_gnd ;                 // Gnd in present netlist
    static Net       *_pwr ;                 // Pwr in present netlist
    static Net       *_x ;                   // X in present netlist
    static Net       *_z ;                   // Z in present netlist
    static VhdlIdDef *_boolean_type ;        // Standard boolean type identifier (improvement for VIPER #7300)
    static unsigned   _static_elab ;         // Flag static or RTL elaboration.

public:
    static Array     *_named_path ;          // Named path public (for iterator only please)
    static Array     *_value_stack ;         // Stack to store value
    static Array     *_constraint_stack ;    // Stack to store constraint
    static Array     *_actual_id_stack ;     // Stack to store actual for subprog/package generic
    static Array     *_structural_stack ;    // Structural stack public (for iterator only)
    static Array     *_path_name ;           // Path name for path_name attribute
    static Array     *_instance_name ;       // Instance based path name for instance_name attribute
    static Set       *_netlist_stack ;       // Stack of Netlist* in the hierarchy (VIPER #6944)

    static Set       *_deallocated_access_values ; // deallocated access values (VIPER #4464, 7292)
    static Set       *_access_inner_values ;       // leaked access values (VIPER #4464, 7292)

} ; // class VhdlNode

/* -------------------------------------------------------------- */

/*
   TreeNode contains a 'linefile' field which encodes the line and file info
   from where the tree node was created.  It also contains some virtual 'catcher'
   functions that have their real definition in the derived classes
   that know what to do with them.
*/
class VFC_DLL_PORT VhdlTreeNode : public VhdlNode
{
public:
    explicit VhdlTreeNode(linefile_type linefile = 0);

    // Copy tree node
    VhdlTreeNode(const VhdlTreeNode &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlTreeNode(SaveRestore &save_restore);

    virtual ~VhdlTreeNode();

private:
    // Prevent compiler from defining the following
    VhdlTreeNode(const VhdlTreeNode &) ;            // Purposely leave unimplemented
    VhdlTreeNode& operator=(const VhdlTreeNode &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLTREENODE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    // Visitor function for finding indexed expression
    virtual void                InitializeForVariableSlice() { return ; }
#endif
    // Checks for deffered constants defined for VhdlExpressions VIPER 3505, 4368
    virtual void                CheckLegalDefferedConst(const VhdlTreeNode * /*init_assign*/, Set* /*unit_ids*/) const { }

    virtual VhdlName *          FormalPart() const       { return 0 ; } // Return the formal. Defined for VeriAssocElement

    static unsigned             GetVhdlAccent() ;
    static VhdlInterfaceId *    CreateVhdlGeneric(const char* name) ;
    static VhdlInterfaceId *    CreateVhdlPort(const char* name) ;
    static VhdlRange *          CreateVhdlRange(verific_int64 left, verific_int64 right) ;
    static VhdlRange *          CreateVhdlRangeWithExpr(VhdlExpression* left, VhdlExpression* right) ;

    static unsigned             GetInputMode() ; //VHDL_in
    static unsigned             GetOutputMode() ; //VHDL_out
    static unsigned             GetInoutMode() ; //VHDL_inout
    static unsigned             GetEntityId() ; //VHDL_Entity
    virtual const VhdlIdDef *   TreeNodeId() const           { return 0 ; } // VIPER 4367. Same as this only made virtual for access from VeriTreeNode

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const ; // don't make this pure, since we use VhdlTreeNode to for temporary nodes in lexer..
    // Persistence helper methods
    static VhdlTreeNode *       CreateObject(SaveRestore &save_restore, unsigned class_id);
    static void                 SaveArray(SaveRestore &save_restore, const Array *array);
    static void                 SaveSet(SaveRestore &save_restore, const Set *set); // POINTER_HASH
    static void                 SaveMap(SaveRestore &save_restore, const Map *map); // STRING_HASH(_CASE_INSENSITIVE)
    static void                 SaveNodePtr(SaveRestore &save_restore, const VhdlTreeNode *node);
    static void                 SaveScopePtr(SaveRestore &save_restore, const VhdlScope *scope, unsigned for_pkg_inst=0);
    static Array *              RestoreArray(SaveRestore &save_restore);
    static Set *                RestoreSet(SaveRestore &save_restore);               // POINTER_HASH
    static Map *                RestoreIdDefMap(SaveRestore &save_restore);          // STRING_HASH(_CASE_INSENSITIVE)   char* -> VhdlIdDef*
    static VhdlTreeNode *       RestoreNodePtr(SaveRestore &save_restore);
    static VhdlScope *          RestoreScopePtr(SaveRestore &save_restore, unsigned for_pkg_inst=0);

    // New, formatted, varargs messages
    void                        Error(const char *format, ...) const ;
    void                        Warning(const char *format, ...) const ;
    void                        Info(const char *format, ...) const ;
    void                        Comment(const char *format, ...) const ;

    // Obtain the line/file(column) info of this tree node
    inline linefile_type        Linefile() const                    { return _linefile ; }     // Get linefile info from this node (see LineFile.h for API).
    inline void                 SetLinefile(linefile_type linefile) { _linefile = linefile ; } // Override existing line/file info.

    unsigned                    IsTickProtectedNode() const ; // check if the tree node is created from a decrypted RTL region

    // Next two ones are here for back-ward compatibility.
    // Since 9/2004, we create start or ending linefile info based on the location info from bison rule-location-range processing.
    // This is much simpler than overloading all classes with a special routine.
    // Thus, every node now has fixed linefile info, which is either start or end linefile (determined by compile switch VHDL_USE_STARTING_LINEFILE_INFO),
    // or it is full location info (start/end column/line all available), (determined by compile switch VERIFIC_LINEFILE_INCLUDES_COLUMNS)
    inline virtual linefile_type StartingLinefile() const  { return _linefile ; }  // Get starting linefile info (this will be overridden by various classes)
    inline linefile_type         EndingLinefile() const    { return _linefile ; }  // Get ending linefile info (purposely not virtual)

    // Print tree node in VHDL format
    // Cannot make it a pure function, since it's not abstract : we instantiate it in lex/yacc to get line-numbering right
    virtual void                PrettyPrint(ostream & /*f*/, unsigned /*level*/) const { } // virtual catcher. Print nothing for unknown nodes.

    // Comments (comment preservation ; only active if compile switch VHDL_PRESERVE_COMMENTS or the equivalent runtime switch is set)
    void                        AddComments(Array *arr) ; // Note: 'arr' is absorbed in this routine and even it may get deleted too
    Array *                     GetComments() const ;
    void                        SetComments(Array *comments) ; // Note: 'comments' is absorbed in this routine
    Array *                     TakeComments() ;
    static void                 ResetComments() ;
    Array *                     ProcessPostComments(Array *comments) ;
    void                        PrettyPrintPreComments(ostream &f, unsigned level, unsigned print_leading_newline=0) const ;
    void                        PrettyPrintPostComments(ostream &f, unsigned print_trailing_newline=1) const ;

    // Printing space for pretty print
    static const char *         PrintLevel(unsigned level) ;
    // Print an identifier, take care of escaped name
    static void                 PrintIdentifier(ostream &f, const char *str) ;
    // VIPER #6534: Print the assoc elements with pre and post comments:
    static void                 PrintAssocListWithComments(Array *list, ostream &f, unsigned level, unsigned *comma_count=0) ;
    // Check whether string is consider a legal VHDL identifier name
    static unsigned             IsLegalIdentifier(const char *str) ;
    // Return a newly allocated legal (if it's not already) VHDL identifier string
    static char *               CreateLegalIdentifier(const char *str) ;

    // Virtual functions to access across TreeNode derived classes
    virtual VhdlScope *         LocalScope() const ; // Get the locally declared scope of a declaration or unit

    virtual void                ClosingLabel(VhdlDesignator *id) ; // Check if a closing label matches previously set one. Absorbs 'id' (closing label not stored in tree)
    virtual void                SetClosingLabel(VhdlDesignator *label) ; // VIPER #6666

    // Elaboration related routines :
    // Optimized forms of binary gates (they know about gnd,pwr,x etc). */
    // These routines use _linefile of this node to label the logic they create */
    Net *                       Inv(Net *n) const ;
    Net *                       Buf(Net *n) const ;
    Net *                       Or(Net *n1, Net *n2) const ;
    Net *                       Nor(Net *n1, Net *n2) const ;
    Net *                       And(Net *n1, Net *n2) const ;
    Net *                       Nand(Net *n1, Net *n2) const ;
    Net *                       Xor(Net *n1, Net *n2) const ;
    Net *                       Xnor(Net *n1, Net *n2) const ;
    Net *                       Mux(Net *n0, Net *n1, Net *c, unsigned opt = 0) const ;
    static char *               ConcatAndCreateLegalIdentifier(const char *prefix, const char *suffix) ; // Concat arguments to create legal vhdl identifier. Escaped prefix name is handled properly

    static char *               CreateUniqueUnitName(const char *prefix, const VhdlLibrary *lib, const VhdlScope *scope, unsigned ignore_copied) ; // Create a name which is unique both in library 'lib' and scope 'scope'
    static void                 ProcessFormal(VhdlName *formal_part, const VhdlIdDef *comp_port, const VhdlIdDef *entity_id) ;
    static Map      *_unit_num_map ; // VIPER #7054 : unit_name vs last used unique number
    static void     CleanUnitNumMap() ; // API to clean above static map

    // VIPER #7853 : Create short name when length of made up name is greater than 200
    static char *   CreateShortName(const char *unit_name, char *long_name) ;
    static Map      *_long2short ; // long_unit_name vs short name
    static void     CleanLong2ShortMap() ; // API to clean above static map
public :
    static Map      *_config_num_map ; // VIPER #6032 : Configuration signature <-> unique number map
    static void     CleanConfigNumMap() ; // API to clean above static map
private:
// The only class member that is common to ALL parse-tree nodes
// The line/file info (formatted via utility LineFile) of the parse-tree node.
    linefile_type    _linefile ;

private:
    static Map       *_map_of_comment_arr ;  // VIPER #7322: Map of VhdlTreeNode -> Array of VhdlComment nodes
} ; // class VhdlTreeNode

/* -------------------------------------------------------------- */

/*
   VhdlCommentNode is a class which is used only when VHDL_PRESERVE_COMMENTS switch or the equivalent
   runtime switch is set. In that case, comments will be annotated on VhdlDesignUnit, VhdlDeclaration
   and VhdlStatement.
*/
class VFC_DLL_PORT VhdlCommentNode : public VhdlTreeNode
{
public:
    explicit VhdlCommentNode(linefile_type linefile = 0) ;

    // Copy tree node
    VhdlCommentNode(const VhdlCommentNode &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlCommentNode(SaveRestore &save_restore);

    ~VhdlCommentNode() ;

private:
    // Prevent compiler from defining the following
    VhdlCommentNode(const VhdlCommentNode &) ;            // Purposely leave unimplemented
    VhdlCommentNode& operator=(const VhdlCommentNode &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID   GetClassId() const { return ID_VHDLCOMMENTNODE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void            Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void            Save(SaveRestore &save_restore) const ;

    // Pretty print tree node
    virtual void            PrettyPrint(ostream & /*f*/, unsigned /*level*/) const { }

    void                    AppendComment(const char *str) ;
    const char *            GetCommentString() const ;
    unsigned                GetStartingLineNumber() const ;
    void                    AdjustClosingLinefile(unsigned right_line, unsigned right_col) const ; // Set the terminating linefile info

    VhdlCommentNode *       CopyComment() const ; // Copy this comment into a new comment.

private:
    char *_str ;
} ; // class VhdlCommentNode

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_TREE_NODE_H_

