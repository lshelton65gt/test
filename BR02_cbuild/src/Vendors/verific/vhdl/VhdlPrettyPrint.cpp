/*
 *
 * [ File Version : 1.86 - 2014/03/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <string.h> // strchr
#include <fstream>
using namespace std ;

#include "vhdl_tokens.h"

#include "Message.h"
#include "Strings.h"
#include "Set.h"
#include "Array.h"

#include "vhdl_file.h"
#include "VhdlUnits.h"
#include "VhdlTreeNode.h"
#include "VhdlMisc.h"
#include "VhdlIdDef.h"
#include "VhdlStatement.h"
#include "VhdlExpression.h"
#include "VhdlName.h"
#include "VhdlDeclaration.h"
#include "VhdlConfiguration.h"
#include "VhdlSpecification.h"

#ifdef VHDL_PRETTY_PRINT_INTERNAL_DATA
#include "VhdlValue_Elab.h"
#endif // VHDL_PRETTY_PRINT_INTERNAL_DATA

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

// Define the maximum number of items of a port_map list, printed on a single line (VIPER #2397)
#define MAX_COMMA_LIST_COUNT 10

#define RETURN_IF_TICK_PROTECTED \
    if (IsTickProtectedNode()) { \
        f << " -- protected rtl " << endl ; \
        return ; \
    }

//--------------------------------------------------------------
// #included from vhdl_file.h
//--------------------------------------------------------------

unsigned
vhdl_file::PrettyPrint(const char *file_name, const char *unit_name, const char *lib_name)
{
    if (!file_name) return 0 ;

    if (!lib_name) lib_name = "work" ;

    // Get the library :
    _work_lib = GetLibrary(lib_name,0) ;
    if (!_work_lib) return 0 ;

    VhdlTreeNode tmp_print_node ; // Viper: 3142 - dummy node for msg printing
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    // If unit_name is not given, print the entire library
    VhdlPrimaryUnit *unit = 0 ;
    if (unit_name) {
        // Get the unit :
        unit = _work_lib->GetPrimUnit(unit_name) ;

        if (!unit) {
            // Message::Error(0,"unit ",unit_name," not found in library ",lib_name) ;
            tmp_print_node.Error("unit %s not found in library %s", unit_name, lib_name) ;
            return 0 ;
        }
    }

    // Now open the file
    ofstream f(file_name,ios::out) ;
    if (!f.rdbuf()->is_open()) {
        // Message::Error(0,"cannot open file ",file_name) ;
        tmp_print_node.Error("cannot open file '%s'", file_name) ;
        return 0 ;
    }

    if (unit) {
        // Pretty-print the unit into this file
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Pretty printing unit ", unit->Name(), " to file ",file_name) ;
        tmp_print_node.Comment("Pretty printing unit %s to file %s", unit->Name(), file_name) ;

        // Print the primary unit with its secondary units.
        unit->PrettyPrint(f,0) ;
    } else {
        // Pretty-print all units from this library into this file
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("Pretty printing all units in library ", lib_name, " to file ",file_name) ;
        tmp_print_node.Comment("Pretty printing all units in library %s to file %s", lib_name, file_name) ;

        // Due to static elaboration some new design units
        // are created and those are inserted in the library after
        // the original units. Now if we print the primary units
        // from the library in the order in which they exist in
        // the library, printed output will not be legal vhdl.
        // To make the output legal vhdl we make an ordered list
        // of design units (primary and secondary). In the list units
        // used by other units will come before the units using other units.
        Set ordered_list(POINTER_HASH) ; // Ordered list of design units
        Set done(POINTER_HASH) ; // VIPER #2845 : Set to catch mutual dependency
        MapIter mi ;
        FOREACH_VHDL_PRIMARY_UNIT(_work_lib,mi,unit) {
            if (!unit) continue ;
            // Inspect the dependent list of 'unit' and
            // populate the 'ordered_list' in proper order.
            unit->CreateOrderedList(&ordered_list, &done) ;
            // Do not consider the secondary units for package instantiation decls
            if (unit->IsPackageInstDecl()) continue ;

            // Consider the secondary units and insert the units in which secondary units
            // depends before secondary units
            VhdlSecondaryUnit *sec_unit ;
            MapIter sec_unit_iter ;
            // Iterate over secondary units
            FOREACH_VHDL_SECONDARY_UNIT(unit,sec_unit_iter,sec_unit) {
                if (!sec_unit) continue ;
                sec_unit->CreateOrderedList(&ordered_list, &done) ;
            }
        }

        // VIPER #2845 : Both primary and secondary units are now in the ordered list
        SetIter si ;
        VhdlDesignUnit *base_unit ;
        FOREACH_SET_ITEM(&ordered_list, si, &base_unit) {
            if (!base_unit) continue ;
            // Since 'ordered_list' is a mix of primary and secondary units,
            // So, make sure to ONLY pretty-print the design unit itself. So use PrettyPrintDesignUnit()
            base_unit->PrettyPrintDesignUnit(f,0) ;
        }
    }

    // f closes automatically in ofstream destructor.
    return 1 ;
}

//--------------------------------------------------------------
// #included from VhdlTreeNode.h
//--------------------------------------------------------------

void
VhdlTreeNode::PrintIdentifier(ostream &f, const char *str)
{
    if (!str || !*str) return ;

    // CreateLegalIdentifier creates a legal identifier if it isn't
    // already one.  The returned string needs to be free'd.
    // 5/10/2013 Commented this code to check CreateLegalIdentifier
    // str is not modified here. Commented out redundent function call
    //char *tmp = CreateLegalIdentifier(str) ;
    f << str ;
    //Strings::free(tmp) ;
}

void
VhdlTreeNode::PrintAssocListWithComments(Array *list, ostream &f, unsigned level, unsigned *comma_count /*=0*/)
{
    // VIPER #6534: Print the assoc elements with pre and post comments:
    unsigned print_newline = 1 ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(list, i, item) {
        if (i) {
            f << ", " ;
            // Cap the number of items printed on a single line (VIPER #2397)
            if (comma_count && ((++(*comma_count) % MAX_COMMA_LIST_COUNT)==0)) {
                f << endl << PrintLevel(level) ;
                print_newline = 0 ;
            }
        }
        if (item) {
            item->PrettyPrintPreComments(f,level, print_newline) ;
            item->PrettyPrint(f,level) ;
            item->PrettyPrintPostComments(f, 0) ;
        } else {
            f << "open" ;
        }
    }
}

//--------------------------------------------------------------
// #included from VhdlUnits.h
//--------------------------------------------------------------

void
VhdlPrimaryUnit::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print this design unit
    PrettyPrintDesignUnit(f, level) ;

    // Print Secondary units here for backward compatibility of PrettyPrint().
    // Separate PrettyPrintPrimUnit() prints primary unit only, for VIPER 2845.

    // Print all secondary units
    MapIter mi ;
    VhdlSecondaryUnit *elem ;
    FOREACH_VHDL_SECONDARY_UNIT(this,mi,elem) {
        if (!elem) continue ;
        elem->PrettyPrintDesignUnit(f,level) ;
    }
}

void
VhdlEntityDecl::PrettyPrintDesignUnit(ostream &f, unsigned level) const
{
    if (_is_verilog_module) return ;
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_context_clause,i,decl) {
        if (!decl || decl->IsImplicit()) continue ;
        decl->PrettyPrint(f,level) ;
    }

    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "entity " ;
    _id->PrettyPrint(f,level) ;
    f << " is" << endl ;
    VhdlTreeNode *elem ;
    if (_generic_clause) {
        f << PrintLevel(level+1) << "generic (" ;
        VhdlInterfaceDecl *interface_elem = 0 ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,interface_elem) {
            if (!interface_elem) continue ;
            if (!i) f << endl ;
            f << PrintLevel(level+2) ; // newline
            interface_elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) {
                f << "; " ;
                // Print the post comments from here, please see
                // VhdlInterfaceDecl::PrettyPrint() for more comments.
                interface_elem->PrettyPrintPostComments(f) ;
            }
        }
        f << ") ;" ;
        // Print the post comments from here, please see
        // VhdlInterfaceDecl::PrettyPrint() for more comments.
        if (interface_elem) interface_elem->PrettyPrintPostComments(f) ;
    }
    if (_port_clause) {
        f << PrintLevel(level+1) << "port (" ;
        VhdlInterfaceDecl *interface_elem = 0 ;
        FOREACH_ARRAY_ITEM(_port_clause,i,interface_elem) {
            if (!interface_elem) continue ;
            if (!i) f << endl ;
            f << PrintLevel(level+2) ; // newline
            interface_elem->PrettyPrint(f,level+2) ;
            if (i!=_port_clause->Size()-1) {
                f << "; " ;
                // Print the post comments from here, please see
                // VhdlInterfaceDecl::PrettyPrint() for more comments.
                interface_elem->PrettyPrintPostComments(f) ;
            }
        }
        f << ") ;" ;
        // Print the post comments from here, please see
        // VhdlInterfaceDecl::PrettyPrint() for more comments.
        if (interface_elem) interface_elem->PrettyPrintPostComments(f) ;
    }
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    if (_statement_part) {
        f << PrintLevel(level) << "begin" << endl ;
        FOREACH_ARRAY_ITEM(_statement_part,i,elem) {
            if (!elem) continue ;
            elem->PrettyPrint(f,level+1) ;
        }
    }
    f << PrintLevel(level) << "end " ;
    _id->PrettyPrint(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;

    f << endl ;
}

void
VhdlArchitectureBody::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_context_clause,i,decl) {
        if (!decl || decl->IsImplicit()) continue ;
        decl->PrettyPrint(f,level) ;
    }

    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "architecture " ;
    _id->PrettyPrint(f, level) ;
    f << " of " ;
    if (_entity_name) _entity_name->PrettyPrint(f,level+1) ;
    f << " is" << endl ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "begin" << endl ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end " ;
    _id->PrettyPrint(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;
    f << endl ;
}

void
VhdlConfigurationDecl::PrettyPrintDesignUnit(ostream &f, unsigned level) const
{
    if (_is_verilog_module) return ;
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_context_clause,i,decl) {
        if (!decl || decl->IsImplicit()) continue ;
        decl->PrettyPrint(f,level) ;
    }

    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "configuration " ;
    _id->PrettyPrint(f, level) ;
    f << " of " ;
    if (_entity_name) _entity_name->PrettyPrint(f,level+1) ;
    f << " is" << endl ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    if (_block_configuration) _block_configuration->PrettyPrint(f,level+1) ;
    f << PrintLevel(level) << "end " ;
    _id->PrettyPrint(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;
    f << endl ;
}

void
VhdlPackageDecl::PrettyPrintDesignUnit(ostream &f, unsigned level) const
{
    // VIPER #6896: do not print the importing vhdl package, converted from verilog package
    if (_is_verilog_module) return ;

    RETURN_IF_TICK_PROTECTED ;

    // First line of defence to prevent that IEEE packages with IEEE copyright distribution restrictions (especially 2008) get written out.
    // NOTE: Source-code customers : PLEASE DO NOT CHANGE THESE LINES. You may risk liability of IEEE copyright infringement if your
    // customers can print out IEEE packages
    VhdlLibrary *lib = GetOwningLib() ;
    if (lib && Strings::compare(lib->Name(),"ieee")) return ; // refuse to print out IEEE library packages.

    // Print context clauses
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_context_clause,i,decl) {
        if (!decl || decl->IsImplicit()) continue ;
        decl->PrettyPrint(f,level) ;
    }

    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "package " ;
    _id->PrettyPrint(f, level) ;
    f << " is" << endl ;
    VhdlTreeNode *elem ;

    if (_generic_clause) { // Vhdl 2008 LRM section 4.7
        f << PrintLevel(level+1) << "generic (" ;
        VhdlInterfaceDecl *interface_elem = 0 ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,interface_elem) {
            if (!interface_elem) continue ;
            if (!i) f << endl ;
            f << PrintLevel(level+2) ; // newline
            interface_elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) {
                f << "; " ;
                // Print the post comments from here, please see
                // VhdlInterfaceDecl::PrettyPrint() for more comments.
                interface_elem->PrettyPrintPostComments(f) ;
            }
        }
        f << ") ;" ;
        // Print the post comments from here, please see
        // VhdlInterfaceDecl::PrettyPrint() for more comments.
        if (interface_elem) interface_elem->PrettyPrintPostComments(f) ;
    }

    if (_generic_map_aspect) {
        f << PrintLevel(level+1) << "generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ") ;" << endl ;
    }

    FOREACH_ARRAY_ITEM(_decl_part,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end " ;
    _id->PrettyPrint(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;
    f << endl ;
}

void
VhdlContextDecl::PrettyPrintDesignUnit(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "context " ;
    _id->PrettyPrint(f, level) ;
    f << " is" << endl ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end context " ;
    _id->PrettyPrint(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;
    f << endl ;
}
void
VhdlPackageInstantiationDecl::PrettyPrintWOSemi(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_context_clause,i,decl) {
        if (!decl || decl->IsImplicit()) continue ;
        decl->PrettyPrint(f,level) ;
    }

    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "package " ;
    _id->PrettyPrint(f, level) ;
    f << " is new " ;

    if (_uninst_package_name) _uninst_package_name->PrettyPrint(f,level) ;

    if (_generic_map_aspect) {
        f << endl ;
        f << PrintLevel(level+1) << "generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ")" ;
    }
}
void
VhdlPackageInstantiationDecl::PrettyPrintDesignUnit(ostream &f, unsigned level) const
{
    PrettyPrintWOSemi(f, level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;
    f << endl ;
}
void
VhdlPackageInstantiationDecl::PrettyPrint(ostream &f, unsigned level) const
{
    PrettyPrintDesignUnit(f, level) ;
}
void
VhdlPackageBody::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // First line of defence to prevent that IEEE packages with IEEE copyright distribution restrictions (especially 2008) get written out.
    // NOTE: Source-code customers : PLEASE DO NOT CHANGE THESE LINES. You may risk liability of IEEE copyright infringement if your
    // customers can print out IEEE packages
    VhdlLibrary *lib = GetOwningLib() ;
    if (lib && Strings::compare(lib->Name(),"ieee")) return ; // refuse to print out IEEE library packages.

    // Print out context clauses:
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_context_clause,i,decl) {
        if (!decl || decl->IsImplicit()) continue ;
        decl->PrettyPrint(f,level) ;
    }

    if (!_id) return ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "package body " ;
    _id->PrettyPrint(f,level) ;
    f << " is" << endl ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end " ;
    _id->PrettyPrint(f,level) ;
    f << " ;" ;
    PrettyPrintPostComments(f) ;
    f << endl ;
}

//--------------------------------------------------------------
// #included from VhdlTreeNode.h
//--------------------------------------------------------------

void
VhdlTreeNode::PrettyPrintPreComments(ostream &f, unsigned level, unsigned print_leading_newline /*=0*/) const
{
    Array *comment_arr = GetComments() ;
    if (!comment_arr) return ;

    unsigned comment_printed = 0 ;
    unsigned i ;
    VhdlCommentNode *node ;
    unsigned this_line_no = LineFile::GetLineNo(Linefile()) ;
    FOREACH_ARRAY_ITEM(comment_arr, i, node) {
        if (!node) continue ;

        unsigned comment_line_no = node->GetStartingLineNumber() ;
        if (comment_line_no >= this_line_no) continue ;

        if (!comment_printed && print_leading_newline) f << endl << PrintLevel(level) ;
        comment_printed = 1 ;

        f << "--" << node->GetCommentString() ;
        // A newline occurred, indent at level.
        f << PrintLevel(level) ;
    }
}

void
VhdlTreeNode::PrettyPrintPostComments(ostream &f, unsigned print_trailing_newline /*=1*/) const
{
    unsigned comment_printed = 0 ;
    Array *comment_arr = GetComments() ;
    if (!comment_arr) {
        if (print_trailing_newline) f << endl ;
        return ;
    }

    unsigned i ;
    VhdlCommentNode *node ;
    unsigned this_line_no = LineFile::GetLineNo(Linefile()) ;
    FOREACH_ARRAY_ITEM(comment_arr, i, node) {
        if (!node) continue ;

        unsigned comment_line_no = node->GetStartingLineNumber() ;
        if (comment_line_no < this_line_no) continue ;

        f << " --" << node->GetCommentString() ;
        comment_printed = 1 ;
        // There should be only a single post comment, so it has
        // printed the newline. We will not indent the line. The
        // caller is responsible for indenting.
    }
    if (!comment_printed && print_trailing_newline) f << endl ;
}

//--------------------------------------------------------------
// #included from VhdlStatement.h
//--------------------------------------------------------------

void
VhdlNullStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "null ;" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlReturnStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "return " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlExitStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "exit " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    if (_condition) {
        if (_target)   f << " when " ;
        else           f << "when " ;
        _condition->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlNextStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "next " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    if (_condition) {
        if (_target) f << " " ;
        f << "when " ;
        _condition->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlLoopStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    if (_iter_scheme) _iter_scheme->PrettyPrint(f,level+1) ;
    f << " loop " ;
    PrettyPrintPostComments(f) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_statements, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end loop; " << endl ;
}

void
VhdlCaseStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "case " ;
    if (_is_matching_case) f << "? " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << " is " ;
    PrettyPrintPostComments(f) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_alternatives, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end case " ;
    if (_is_matching_case) f << "? " ;
    f << "; " << endl ;
}

void
VhdlIfStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "if " ;
    if (_if_cond) _if_cond->PrettyPrint(f,level+1) ;
    f << " then " ;
    PrettyPrintPostComments(f) ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_if_statements, i, elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    FOREACH_ARRAY_ITEM(_elsif_list, i, elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level) ;
    }
    if (_else_statements) {
        f << PrintLevel(level) << "else " << endl ;
        FOREACH_ARRAY_ITEM(_else_statements, i, elem) {
            if (!elem) continue ;
            elem->PrettyPrint(f,level+1) ;
        }
    }
    f << PrintLevel(level) << "end if;" << endl ;
}

void
VhdlVariableAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " := " ;
    if (_value) _value->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlSignalAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= " ;
    if (_delay_mechanism) _delay_mechanism->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_waveform, i, elem) {
        if (!elem) continue ;
        if (i!=0) f << "," ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
void
VhdlForceAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= force " ;

    if (_force_mode) f << ((_force_mode == VHDL_in) ? "in " : "out ") ;

    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlReleaseAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= release " ;

    if (_force_mode) f << ((_force_mode == VHDL_in) ? "in " : "out ") ;

    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlWaitStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "wait " ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_sensitivity_clause) {
        f << "on " ;
        FOREACH_ARRAY_ITEM(_sensitivity_clause, i, item) {
            if (!item) continue ;
            if (i!=0) f << "," ;
            item->PrettyPrint(f,level+1) ;
        }
    }
    if (_condition_clause) {
        f << " until " ;
        _condition_clause->PrettyPrint(f,level+1) ;
    }
    if (_timeout_clause) {
        f << " for " ;
        _timeout_clause->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlReportStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "report " ;
    if (_report) _report->PrettyPrint(f,level+1) ;
    if (_severity) {
        f << " severity " ;
        _severity->PrettyPrint(f,level+2) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlAssertionStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "assert " ;
    if (_condition) _condition->PrettyPrint(f,level+1) ;
    if (_report) {
        f << " report " ;
        _report->PrettyPrint(f,level+1) ;
    }
    if (_severity) {
        f << " severity " ;
        _severity->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlProcessStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "process " ;
    unsigned i ;
    VhdlTreeNode *elem ;
    if (_sensitivity_list) {
        f << "(" ;
        FOREACH_ARRAY_ITEM(_sensitivity_list, i, elem) {
            if (i!=0) f << ", " ;
            elem->PrettyPrint(f,level+1) ;
        }
        f << ")" ;
    }
    PrettyPrintPostComments(f) ;
    FOREACH_ARRAY_ITEM(_decl_part, i, elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "begin " << endl ;
    FOREACH_ARRAY_ITEM(_statement_part, i, elem) {
        if (!elem) continue ;
        elem->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end process ; " << endl ;
}

void
VhdlBlockStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "block " ;
    if (_guard) {
        f << "(" ;
        _guard->PrettyPrint(f,level+1) ;
        f << ")" ;
    }
    PrettyPrintPostComments(f) ;
    if (_generics) _generics->PrettyPrint(f,level+1) ;
    if (_ports) _ports->PrettyPrint(f,level+1) ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_decl_part, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "begin" << endl ;

    FOREACH_ARRAY_ITEM(_statements, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end block ; " << endl ;
}

void
VhdlGenerateStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
// Modified for static elaboration.
// unrolled statements are attached within generate statement
// and its original ones are deleted.
    if (_scheme) {
        _scheme->PrettyPrint(f,_label ? level+1: level) ;
        if (!_scheme->IsCaseScheme()) f << " generate " ;
    } else {
        f << "block" << endl ;
        f << PrintLevel(level) << "begin" ;
    }
    PrettyPrintPostComments(f) ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_decl_part) {
        FOREACH_ARRAY_ITEM(_decl_part, i, item) {
            if (!item) continue ;
            item->PrettyPrint(f,level+1) ;
        }
        f << PrintLevel(level) << "begin" << endl ;
    }
    FOREACH_ARRAY_ITEM(_statement_part, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    if (_scheme && (_scheme->IsIfScheme() || _scheme->IsForScheme())) {
        f << PrintLevel(level) << "end generate ; " << endl ;
    } else if (!_scheme) {
        f << PrintLevel(level) << "end block ; " << endl ;
    }
}

void
VhdlComponentInstantiationStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
    if (_instantiated_unit) _instantiated_unit->PrettyPrint(f,level+1) ;
    unsigned i=0 ;
    if (_generic_map_aspect) {
        f << " generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1, &i) ;
        f << ")" ;
    }
    if (_port_map_aspect) {
        f << " port map (" ;
        PrintAssocListWithComments(_port_map_aspect, f, level+1, &i) ;
        f << ")" ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlProcedureCallStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
    if (_call) _call->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlConditionalSignalAssignment::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= " ;
    if (_options) _options->PrettyPrint(f,level+1) ;
    if (_delay_mechanism) _delay_mechanism->PrettyPrint(f,level+1) ;
    VhdlTreeNode *item ;
    unsigned i ;
    // Conditional waveforms are stacked from back
    FOREACH_ARRAY_ITEM(_conditional_waveforms, i, item) {
        if (!item) continue ;
        if (i!=0) f << " else " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
void
VhdlConditionalForceAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= force " ;
    if (_force_mode) f << ((_force_mode == VHDL_in) ? "in ": "out ") ;

    VhdlTreeNode *item ;
    unsigned i ;
    // Conditional expressions are stacked from back
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, item) {
        if (!item) continue ;
        if (i!=0) f << " else " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlSelectedSignalAssignment::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
    f << "with " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << " select " ;
    if (_is_matching) f << "? " ;
    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= " ;
    if (_options) _options->PrettyPrint(f,level+1) ;
    if (_delay_mechanism) _delay_mechanism->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_waveforms, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," << endl << PrintLevel(level+1) ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
void
VhdlIfElsifGenerateStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;
    if (_scheme) {
        _scheme->PrettyPrint(f, level) ;
        f << " generate " ;
    }
    PrettyPrintPostComments(f) ;
    unsigned i ;
    VhdlTreeNode *item ;
    if (_decl_part) {
        FOREACH_ARRAY_ITEM(_decl_part, i, item) {
            if (!item) continue ;
            item->PrettyPrint(f,level+2) ;
        }
        f << PrintLevel(level) << "begin" << endl ;
    }
    FOREACH_ARRAY_ITEM(_statement_part, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+2) ;
    }
    VhdlStatement *elsif_stmt ;
    FOREACH_ARRAY_ITEM(_elsif_list, i, elsif_stmt) {
        if (elsif_stmt) elsif_stmt->PrettyPrint(f,level+1) ;
    }
    if (_else_stmt) _else_stmt->PrettyPrint(f,level+1) ;

    f << PrintLevel(level) << "end generate ; " << endl ;
}
void
VhdlCaseGenerateStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_label) {
        _label->PrettyPrint(f, level) ;
        f << " : " ;
    }
    if (_postponed) f << "postponed " ;

    f << "case " ;
    if (_expr) _expr->PrettyPrint(f, level) ;
    f << " generate" ;
    f << endl ;

    unsigned i ;
    VhdlStatement *case_stmt_alternative ;
    FOREACH_ARRAY_ITEM(_alternatives, i, case_stmt_alternative) {
        if (case_stmt_alternative) case_stmt_alternative->PrettyPrint(f, level) ;
    }

    f << PrintLevel(level) << "end generate ; " << endl ;
}

void
VhdlSelectedVariableAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;
    f << "with " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << " select " ;
    if (_is_matching) f << "? " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " := " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," << endl << PrintLevel(level+1) ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
void
VhdlSelectedForceAssignment::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;
    f << "with " ;
    if (_expr) _expr->PrettyPrint(f,level+1) ;
    f << " select " ;
    if (_is_matching) f << "? " ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " <= force " ;
    if (_force_mode) f << ((_force_mode == VHDL_in) ? "in " : "out ") ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_exprs, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," << endl << PrintLevel(level+1) ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlConditionalVariableAssignmentStatement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_target) _target->PrettyPrint(f,level+1) ;
    f << " := " ;
    VhdlTreeNode *item ;
    unsigned i ;
    // Conditional waveforms are stacked from back
    FOREACH_ARRAY_ITEM(_conditional_expressions, i, item) {
        if (!item) continue ;
        // Viper 7460 : insert else keyword
        if (i!=0) f << " else " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
//--------------------------------------------------------------
// #included from VhdlSpecification.h
//--------------------------------------------------------------

void
VhdlProcedureSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "procedure " ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;

    if (_generic_clause) { // Vhdl 2008 LRM section 4.2.1
        f << PrintLevel(level+1) << "generic (" ;
        VhdlInterfaceDecl *interface_elem = 0 ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,interface_elem) {
            if (!interface_elem) continue ;
            if (!i) f << endl ;
            f << PrintLevel(level+2) ; // newline
            interface_elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) {
                f << "; " ;
                // Print the post comments from here, please see
                // VhdlInterfaceDecl::PrettyPrint() for more comments.
                interface_elem->PrettyPrintPostComments(f) ;
            }
        }
        f << ") " ;
        // Print the post comments from here, please see
        // VhdlInterfaceDecl::PrettyPrint() for more comments.
        if (interface_elem) interface_elem->PrettyPrintPostComments(f) ;
    }

    if (_generic_map_aspect) { // Vhdl 2008 LRM section 4.2.1
        f << PrintLevel(level+1) << "generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ") " << endl ;
    }

    // Viper 6653: print parameter key word for IsExplicitParameter
    if (_formal_parameter_list) {
        (IsExplicitParameter() ? f << PrintLevel(level+1) << "parameter (" : ((_generic_clause || _generic_map_aspect) ? f << PrintLevel(level+1) << "(" : f << "(")) ;
        FOREACH_ARRAY_ITEM(_formal_parameter_list, i, item) {
            if (!item) continue ;
            item->PrettyPrint(f,level+1) ;
            if (i!=_formal_parameter_list->Size()-1) f << "; " ;
        }
        f << ")" ;
    }
    // No post comment will be printed for the VhdlInterfaceDecl of _formal_parameter_list
    // Since we print all the interface items in a single line, we can
    // not print the post comments from here. To print post comments
    // we have to break the interface items one on a line, just like
    // port or generic declaration. To don't want to change our old
    // behaviour, hence we don't print post comments from here, the
    // post comments get lost. Please, see VhdlEntityDecl::PrettyPrint(),
    // and VhdlInterfaceDecl::PrettyPrint() for cross reference.
}

void
VhdlFunctionSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_pure_impure) f << EntityClassName(_pure_impure) << " " ;
    f << "function " ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;

    if (_generic_clause) { // Vhdl 2008 LRM section 4.2.1
        f << PrintLevel(level+1) << "generic (" ;
        VhdlInterfaceDecl *interface_elem = 0 ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,interface_elem) {
            if (!interface_elem) continue ;
            if (!i) f << endl ;
            f << PrintLevel(level+2) ; // newline
            interface_elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) {
                f << "; " ;
                // Print the post comments from here, please see
                // VhdlInterfaceDecl::PrettyPrint() for more comments.
                interface_elem->PrettyPrintPostComments(f) ;
            }
        }
        f << ") " ;
        // Print the post comments from here, please see
        // VhdlInterfaceDecl::PrettyPrint() for more comments.
        if (interface_elem) interface_elem->PrettyPrintPostComments(f) ;
    }

    if (_generic_map_aspect) { // Vhdl 2008 LRM section 4.2.1
        f << PrintLevel(level+1) << "generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ") " << endl ;
    }

    // Viper 6653: print parameter key word for IsExplicitParameter
    if (_formal_parameter_list) {
        (IsExplicitParameter() ? f << PrintLevel(level+1) << "parameter (" : f << "(") ;
        FOREACH_ARRAY_ITEM(_formal_parameter_list, i, item) {
            if (!item) continue ;
            item->PrettyPrint(f,level+1) ;
            if (i!=_formal_parameter_list->Size()-1) f << "; " ;
        }
        f << ")" ;
    }
    if (_return_type) {
        f << " return " ;
        _return_type->PrettyPrint(f,level+1) ;
    }
    // No post comment will be printed for the VhdlInterfaceDecl of _formal_parameter_list
    // Since we print all the interface items in a single line, we can
    // not print the post comments from here. To print post comments
    // we have to break the interface items one on a line, just like
    // port or generic declaration. To don't want to change our old
    // behaviour, hence we don't print post comments from here, the
    // post comments get lost. Please, see VhdlEntityDecl::PrettyPrint(),
    // and VhdlInterfaceDecl::PrettyPrint() for cross reference.
}

void
VhdlComponentSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list, i, item) {
        if (!item) continue ;
        if (i!=0) f << ", " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ": " ;
    if (_name) _name->PrettyPrint(f,level+1) ;
}

void
VhdlGuardedSignalSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_signal_list, i, item) {
        if (!item) continue ;
        if (i!=0) f << ", " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ": " ;
    if (_type_mark) _type_mark->PrettyPrint(f,level+1) ;
}

void
VhdlEntitySpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_entity_name_list, i, item) {
        if (!item) continue ;
        if (i!=0) f << ", " ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ": " << EntityClassName(_entity_class) ;
}

//--------------------------------------------------------------
// #included from VhdlName.h
//--------------------------------------------------------------

void
VhdlAll::PrettyPrint(ostream &f, unsigned /*level*/) const         { f << "all" ; }

void
VhdlOthers::PrettyPrint(ostream &f, unsigned /*level*/) const      { f << "others" ; }

void
VhdlUnaffected::PrettyPrint(ostream &f, unsigned /*level*/) const  { f << "unaffected" ; }

void
VhdlInteger::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    // Cast to long is a fix for compiler failure on VC++ 6.0.
    // Also, we should not pretty-print more than 32 bit value,
    // or else illegal VHDL will result:
    if (_value > 0) {
        // VIPER #4248: This was originally a +ve value, so cast it to unsigned long.
        // Casting to signed long may incorrectly make it -ve, avoid it before hand:
        unsigned long trunc_value = (unsigned long)_value ;
        if ((verific_int64)trunc_value != _value) { // overflow... make it max_int - hack for #6401
            trunc_value = 1 ;
            trunc_value = trunc_value << 31 ;
            trunc_value = ~trunc_value ;
        }
        f <<  trunc_value ;
    } else {
        f << (long)_value ;
    }
}

void
VhdlReal::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    char *image = Strings::dtoa(_value) ;
    f << image ;
    Strings::free(image) ;
}

void
VhdlStringLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    if (!_name) return ;

    // String is stored in _name, with "'s

    // First walk _name and determine number of occurrences of "
    unsigned count = 0 ;
    char *p = _name ;
    p = strchr(p, '"') ;
    while (p) {
        p++ ; count++ ;
        p = strchr(p, '"') ;
    }
    count -= 2 ;    // Subtract opening and closing double quote

    if (count) {
        // Create a new temp buffer and write _name with double quotes preceded with double quotes
        p = _name + 1;
        char *tmp = Strings::allocate(Strings::len(_name) + count + 1) ;
        tmp[0] = '"' ;  // Write opening double quote
        unsigned i ;
        for(i=1; *p; i++) {
            tmp[i] = *p++ ;
            if (tmp[i] == '"' && *p) {
                tmp[++i] = '"' ; // Write extra double quote
            }
        }
        tmp[i] = '\0' ;
        f << tmp ;
        Strings::free(tmp) ;
    } else {
        f << _name ;
    }
}

void
VhdlBitStringLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    // _based_string could be NULL (ie. if an old vdb is restored which
    // didn't have _based_string in the first place). (VIPER #1950)
    if (_based_string) {
        f << _based_string ; // String with X, B or O with it
    } else {
        // We now print the based string itself, not the binary converted value
        f << "b" << _name ; // String is stored in _name, with "'s
    }
}

void
VhdlCharacterLiteral::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << _name ; // character is stored in _name, with ''s
}

void
VhdlPhysicalLiteral::PrettyPrint(ostream &f, unsigned level) const
{
    if (_value) _value->PrettyPrint(f,level+1) ;
    f << " " ;
    if (_unit) _unit->PrettyPrint(f,level+1) ;
}

void
VhdlNull::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "null" ;
}

void
VhdlIdRef::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print the normal name reference
    PrintIdentifier(f, _name) ;
}

void
VhdlArrayResFunction::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;
    if (_res_function) {
        f << "(" ;
        // Print the normal name reference
        _res_function->PrettyPrint(f,level+1) ;
        f << ")" ;
    }
}

void
VhdlRecordResFunction::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;
    if (_rec_res_function_list) {
        f << "(" ;
        VhdlExpression *elem ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(_rec_res_function_list,i,elem) {
            // Print the normal name reference
            if (!elem) continue ;
            if (i) f << "," ;
            elem->PrettyPrint(f,level+1) ;
        }
        f << ")" ;
    }
}

void
VhdlSelectedName::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "." ;
    if (_suffix) _suffix->PrettyPrint(f,level+1) ;
}

void
VhdlIndexedName::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "(" ;
    PrintAssocListWithComments(_assoc_list, f, level+1) ;
    f << ")" ;
}

void
VhdlSignaturedName::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_name) _name->PrettyPrint(f,level+1) ;
    if (_signature) _signature->PrettyPrint(f,level+1) ;
}

void
VhdlAttributeName::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "'" ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    if (_expression) {
        f << "(" ;
        _expression->PrettyPrint(f,level+1) ;
        f << ")" ;
    }
}

void
VhdlOpen::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "open" ;
}

void
VhdlEntityAspect::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_entity_class) f << EntityClassName(_entity_class) << " " ;
    if (_name) _name->PrettyPrint(f,level+1) ;
}

void
VhdlInstantiatedUnit::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_entity_class) f << EntityClassName(_entity_class) << " " ;
    if (_name) _name->PrettyPrint(f,level+1) ;
}
void
VhdlExternalName::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "<< " ;

    switch(_class_type) {
    case VHDL_signal : f << "signal " ; break ;
    case VHDL_constant : f << "constant " ; break ;
    case VHDL_variable : f << "variable " ; break ;
    default : break ;
    }
    if (_path_token == VHDL_AT) {
        f << "@" ;
    } else if (_path_token == VHDL_DOT) {
        f << "." ;
    } else if (_path_token == VHDL_ACCENT) {
        unsigned i ;
        for (i = 0; i < _hat_count; i++) f << "^." ;
    }
    if (_ext_path_name) _ext_path_name->PrettyPrint(f,level) ;
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level) ;
    f << " >> " ;
}

//--------------------------------------------------------------
// #included from VhdlMisc.h
//--------------------------------------------------------------

void
VhdlSignature::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "[" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_type_mark_list, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    if (_return_type) {
        f << " return " ;
        _return_type->PrettyPrint(f,level+1) ;
    }
    f << "]" ;
}

void
VhdlFileOpenInfo::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_file_open) {
        // VHDL 93 file open mode
        f << " open " ;
        _file_open->PrettyPrint(f,level+1) ;
    }

    f << " is " ;
    if (_open_mode) {
        // VHDL 87 file open mode
        f << EntityClassName(_open_mode) ;
        f << " " ;
    }
    if (_file_logical_name) _file_logical_name->PrettyPrint(f,level+1) ;
}

void
VhdlBindingIndication::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_entity_aspect) {
        f << " use " ;
        _entity_aspect->PrettyPrint(f,level+1) ;
    }
    if (_generic_map_aspect) {
        f << " generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ")" ;
    }
    if (_port_map_aspect) {
        f << " port map (" ;
        PrintAssocListWithComments(_port_map_aspect, f, level+1) ;
        f << ")" ;
    }
}

void
VhdlWhileScheme::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_condition) {
        f << "while " ;
        _condition->PrettyPrint(f,level+1) ;
    }
}

void
VhdlForScheme::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "for " ;
    if (_id) _id->PrettyPrint(f,level+1) ;
    f << " in " ;
    if (_range) _range->PrettyPrint(f,level+1) ;
}

void
VhdlIfScheme::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "if " ;

    if (_alternative_label) {
        _alternative_label->PrettyPrint(f,level+1) ;
        f << ": " ;
    }
    if (_condition) _condition->PrettyPrint(f,level+1) ;
}
void
VhdlElsifElseScheme::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_condition) {
        f << "elsif " ;
    } else {
        f << "else " ;
    }

    if (_alternative_label) {
        _alternative_label->PrettyPrint(f,level+1) ;
        f << ": " ;
    }
    if (_condition) _condition->PrettyPrint(f,level+1) ;
}
void
VhdlCaseItemScheme::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;
    f << "when " ;

    if (_alternative_label) {
        _alternative_label->PrettyPrint(f,level+1) ;
        f << ": " ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_choices, i, item) {
        if (!item) continue ;
        if (i!=0) f << "|" ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " => " ;
}

void
VhdlOptions::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_guarded) {
        f << "guarded " ;
    }
    if (_delay_mechanism) _delay_mechanism->PrettyPrint(f,level+1) ;
}

void
VhdlTransport::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "transport " ;
}

void
VhdlInertialDelay::PrettyPrint(ostream &f, unsigned level) const
{
    if (_reject_expression) {
        f << "reject " ;
        _reject_expression->PrettyPrint(f,level+1) ;
    }
    f << " inertial " ;
}

void
VhdlConditionalWaveform::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_waveform, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    if (_when_condition) {
        f << " when " ;
        _when_condition->PrettyPrint(f,level+1) ;
    }
}

void
VhdlConditionalExpression::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_expr) _expr->PrettyPrint(f, level+1) ;

    if (_condition) {
        f << " when " ;
        _condition->PrettyPrint(f, level+1) ;
    }
}

void
VhdlSelectedWaveform::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_waveform, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " when " ;
    FOREACH_ARRAY_ITEM(_when_choices, i, item) {
        if (!item) continue ;
        if (i!=0) f << "|" ;
        item->PrettyPrint(f,level+1) ;
    }
}

void
VhdlSelectedExpression::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    _expr->PrettyPrint(f,level+1) ;
    unsigned i ;
    VhdlTreeNode *item ;
    f << " when " ;
    FOREACH_ARRAY_ITEM(_when_choices, i, item) {
        if (!item) continue ;
        if (i!=0) f << "|" ;
        item->PrettyPrint(f,level+1) ;
    }
}

void
VhdlBlockGenerics::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    if (_generic_clause) {
        f << PrintLevel(level) << "generic ("  ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,item) {
            if (!item) continue ;
            item->PrettyPrint(f,level+1) ;
            if (i!=_generic_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    if (_generic_map_aspect) {
        f << PrintLevel(level) << "generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ") ;" << endl;
     }
}

void
VhdlBlockPorts::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    if (_port_clause) {
        f << PrintLevel(level) << "port (" ;
        FOREACH_ARRAY_ITEM(_port_clause,i,item) {
            if (!item) continue ;
            item->PrettyPrint(f,level+1) ;
            if (i!=_port_clause->Size()-1) f << "; " ;
        }
        f << ") ; " << endl ;
    }
    if (_port_map_aspect) {
        f << PrintLevel(level) << "port map (" ;
        PrintAssocListWithComments(_port_map_aspect, f, level+1) ;
        f << ") ;" << endl;
     }
}

void
VhdlCaseStatementAlternative::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) << "when " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_choices, i, item) {
        if (!item) continue ;
        if (i!=0) f << "|" ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " => " << endl ;
    FOREACH_ARRAY_ITEM(_statements, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
}

void
VhdlElsif::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) << "elsif " ;
    if (_condition) _condition->PrettyPrint(f,level+1) ;
    f << " then" << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_statements, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
}

void
VhdlEntityClassEntry::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_entity_class) f << EntityClassName(_entity_class) << " " ;
    if (_box) f << "<>" ;
}

//--------------------------------------------------------------
// #included from VhdlIdDef.h
//--------------------------------------------------------------

void
VhdlIdDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    PrintIdentifier(f, OrigName()) ;
    (void) level ;

#ifdef VHDL_PRETTY_PRINT_INTERNAL_DATA
    VhdlConstraint *constraint = _constraint ;
    if (_value && _value->GetRange()) constraint = _value->GetRange() ;
    char *constraint_str = constraint ? constraint->Image() : 0 ;
    if (constraint_str) f << " -- Constraint: " << constraint_str <<  endl << PrintLevel(level+1) ;
    Strings::free(constraint_str) ;

    char *val_str = (_value && (_value->NumBits() < 1000) && _value->IsConstant()) ? _value->Image() : 0 ;
    if (val_str) f << " -- Value: " << val_str <<  endl << PrintLevel(level+1) ;
    Strings::free(val_str) ;

    if (!IsGloballyStatic() && !IsGloballyStaticType()) return ;

    f << " -- " ;
    if (IsLocallyStatic()) {
        f << " locally_static" ;
    } else if (IsGloballyStatic()) {
        f << " globally_static" ;
    }
    if (IsLocallyStaticType()) {
        f << " locally_static_type" ;
    } else if (IsGloballyStaticType()) {
        f << " globally_static_type" ;
    }
    f << endl << PrintLevel(level+1) ;
#endif // VHDL_PRETTY_PRINT_INTERNAL_DATA
}

void VhdlLibraryId::PrettyPrint(ostream &f, unsigned level) const           { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlGroupId::PrettyPrint(ostream &f, unsigned level) const             { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlGroupTemplateId::PrettyPrint(ostream &f, unsigned level) const     { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlAttributeId::PrettyPrint(ostream &f, unsigned level) const         { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlComponentId::PrettyPrint(ostream &f, unsigned level) const         { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlAliasId::PrettyPrint(ostream &f, unsigned level) const             { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlFileId::PrettyPrint(ostream &f, unsigned level) const              { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlVariableId::PrettyPrint(ostream &f, unsigned level) const          { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlSignalId::PrettyPrint(ostream &f, unsigned level) const            { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlConstantId::PrettyPrint(ostream &f, unsigned level) const          { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlTypeId::PrettyPrint(ostream &f, unsigned level) const              { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlUniversalInteger::PrettyPrint(ostream &f, unsigned level) const    { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlUniversalReal::PrettyPrint(ostream &f, unsigned level) const       { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlAnonymousType::PrettyPrint(ostream &f, unsigned level) const       { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlSubtypeId::PrettyPrint(ostream &f, unsigned level) const           { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlProtectedTypeId::PrettyPrint(ostream &f, unsigned level) const     { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlProtectedTypeBodyId::PrettyPrint(ostream &f, unsigned level) const { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlSubprogramId::PrettyPrint(ostream &f, unsigned level) const        { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlOperatorId::PrettyPrint(ostream &f, unsigned level) const          { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlInterfaceId::PrettyPrint(ostream &f, unsigned level) const         { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlEnumerationId::PrettyPrint(ostream &f, unsigned level) const       { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlElementId::PrettyPrint(ostream &f, unsigned level) const           { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlPhysicalUnitId::PrettyPrint(ostream &f, unsigned level) const      { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlEntityId::PrettyPrint(ostream &f, unsigned level) const            { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlArchitectureId::PrettyPrint(ostream &f, unsigned level) const      { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlConfigurationId::PrettyPrint(ostream &f, unsigned level) const     { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlPackageId::PrettyPrint(ostream &f, unsigned level) const           { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlPackageBodyId::PrettyPrint(ostream &f, unsigned level) const       { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlContextId::PrettyPrint(ostream &f, unsigned level) const           { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlLabelId::PrettyPrint(ostream &f, unsigned level) const             { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlBlockId::PrettyPrint(ostream &f, unsigned level) const             { VhdlIdDef::PrettyPrint(f, level) ; }
void VhdlPackageInstElement::PrettyPrint(ostream &f, unsigned level) const             { VhdlIdDef::PrettyPrint(f, level) ; }

//--------------------------------------------------------------
// #included from VhdlExpression.h
//--------------------------------------------------------------

void
VhdlExplicitSubtypeIndication::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_res_function) {
        _res_function->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_type_mark) {
        _type_mark->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_range_constraint) {
        f << "range " ;
        _range_constraint->PrettyPrint(f,level+1) ;
    }
}

void
VhdlRange::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_left) _left->PrettyPrint(f,level+1) ;
    if (_dir) f << " " << ((_dir==VHDL_to) ? "to" : "downto") << " " ;
    if (_right) _right->PrettyPrint(f,level+1) ;
}

void
VhdlBox::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    f << "<>" ;
}

void
VhdlAssocElement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_formal_part) {
        _formal_part->PrettyPrint(f,level+1) ;
        f << " => " ;
    }
    if (_actual_part) {
        _actual_part->PrettyPrint(f,level+1) ;
    }
}

void
VhdlInertialElement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << " inertial " ;
    if (_actual_part) {
        _actual_part->PrettyPrint(f,level+1) ;
    }
}
void
VhdlRecResFunctionElement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_record_id_name) {
        _record_id_name->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_record_id_function) {
        _record_id_function->PrettyPrint(f,level+1) ;
    }
}

void
VhdlOperator::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_is_implicit && _oper == VHDL_condition) {
        if(_left) _left->PrettyPrint(f, level) ;
        return ;
    }

    f << "(" ; // Parenthesize the operator call
    if (!_right) {
        // Unary operator
            f << OperatorSimpleName(_oper) << " " ;
            if (_left) _left->PrettyPrint(f,level+1) ;
    } else {
        // Binary operator
            if (_left) _left->PrettyPrint(f,level+1) ;
            f << " " << OperatorSimpleName(_oper) << " " ;
            _right->PrettyPrint(f,level+1) ;
    }
    f << ")" ;
}

void
VhdlAllocator::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "new " ;
    if (_subtype) _subtype->PrettyPrint(f,level+1) ;
}

void
VhdlAggregate::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    unsigned i ;
    VhdlTreeNode *item ;
    f << "(" ;
    FOREACH_ARRAY_ITEM(_element_assoc_list,i,item) {
        if (i!=0) f << "," ;
        if (item) {
            item->PrettyPrint(f,level+1) ;
        } else {
            f << "open" ;
        }
    }
    f << ")" ;
}

void
VhdlQualifiedExpression::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_prefix) _prefix->PrettyPrint(f,level+1) ;
    f << "'(" ;
    if (_aggregate) _aggregate->PrettyPrint(f,level+1) ;
    f << ")" ;
}

void
VhdlElementAssoc::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_choices) {
        unsigned i ;
        VhdlTreeNode *item ;
        FOREACH_ARRAY_ITEM(_choices, i, item) {
            if (!item) continue ;
            if (i!=0) f << "|" ;
            item->PrettyPrint(f,level+1) ;
        }
        f << " => " ;
    }
    if (_expr) _expr->PrettyPrint(f,level+1) ;
}

void
VhdlWaveformElement::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    if (_value) _value->PrettyPrint(f,level+1) ;
    if (_after) {
        f << " after " ;
        _after->PrettyPrint(f,level+1) ;
    }
}
void
VhdlDefault::PrettyPrint(ostream &f, unsigned /*level*/) const
{
    RETURN_IF_TICK_PROTECTED ;
    f << " default " ;
}
//--------------------------------------------------------------
// #included from VhdlDeclaration.h
//--------------------------------------------------------------

void
VhdlScalarTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "range " ;
    if (_scalar_range) _scalar_range->PrettyPrint(f,level+1) ;
}

void
VhdlArrayTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "array (" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_index_constraint,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ") of " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
}

void
VhdlProtectedTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "protected " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_decl_list, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end protected " ;
}
void
VhdlProtectedTypeDefBody::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "protected body " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_decl_list, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end protected body " ;
}

void
VhdlRecordTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "record " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end record " ;
}

void
VhdlAccessTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "access " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
}

void
VhdlFileTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "file of " ;
    if (_file_type_mark) _file_type_mark->PrettyPrint(f,level+1) ;
}

void
VhdlEnumerationTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "(" ;
    unsigned i;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ")" ;
}

void
VhdlPhysicalTypeDef::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << "range " ;
    if (_range_constraint) _range_constraint->PrettyPrint(f,level+1) ;
    f << " units " << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list, i, item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end units " ;
}

void
VhdlElementDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    f << ";" << endl ;
}

void
VhdlPhysicalUnitDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;
    if (_id) _id->PrettyPrint(f, level) ;
    if (_physical_literal) {
        f << " = " ;
        _physical_literal->PrettyPrint(f,level+1) ;
    }
    f << ";" << endl ;
}

void
VhdlUseClause::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "use " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_name_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlContextReference::PrettyPrint(ostream &f, unsigned level) const
{
    // Vhdl 2008 LRM section 13.4
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "context " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_selected_name_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlLibraryClause::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "library " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlInterfaceDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_interface_kind) {
        f << EntityClassName(_interface_kind) << " " ;
    }
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_mode) f << EntityClassName(_mode) << " " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_signal_kind) f << " " << EntityClassName(_signal_kind) ;
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }

    // Don't print post comments here, they should be printed by the caller,
    // since we don't put the separator(;) from here, we cannot print the
    // post comment here. This function is called from three places:
    //   1. VhdlEntityDecl::PrettyPrint()
    //   2. VhdlProcedureSpec::PrettyPrint()
    //   3. VhdlFunctionSpec::PrettyPrint()
    // The first function prints the post comments properly for both
    // port declaration and generic declaration.
    // The second and third function does not prints post comments yet,
    // Please see the explanation comments in the corresponding functions.
}

void
VhdlSubprogramDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_subprogram_spec) _subprogram_spec->PrettyPrint(f,level) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlSubprogramBody::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_subprogram_spec) _subprogram_spec->PrettyPrint(f,level) ;
    f << " is " ;
    PrettyPrintPostComments(f) ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_decl_part,i,item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "begin" << endl ;
    FOREACH_ARRAY_ITEM(_statement_part,i,item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end ;" << endl ;
}

void
VhdlSubtypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "subtype " ;
    _id->PrettyPrint(f, level) ;
    f << " is " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlFullTypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "type " ;
    _id->PrettyPrint(f, level) ;
    f << " is " ;
    if (_type_def) _type_def->PrettyPrint(f,level+1) ;
    if (_id->IsProtected() || _id->IsProtectedBody()) _id->PrettyPrint(f, level) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlIncompleteTypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) << "type " ;
    _id->PrettyPrint(f, level) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlConstantDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "constant " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlSignalDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "signal " ;
    unsigned i ;
    VhdlIdDef *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_signal_kind) {
        f << ((_signal_kind==VHDL_bus) ? " bus" : " register") ;
    }
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlVariableDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << ((_shared) ? "shared " : "") << "variable " ;

    unsigned i ;
    VhdlIdDef *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_init_assign) {
        f << " := " ;
        _init_assign->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlFileDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "file " ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_id_list,i,item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << " : " ;
    if (_subtype_indication) _subtype_indication->PrettyPrint(f,level+1) ;
    if (_file_open_info) {
        f << " " ;
        _file_open_info->PrettyPrint(f,level+1) ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlAliasDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "alias " << _designator->Name() ;
    if (_subtype_indication) {
        f << " : " ;
        _subtype_indication->PrettyPrint(f,level+1) ;
    }
    f << " is " ;
    if (_alias_target_name) _alias_target_name->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlComponentDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "component " ;
    _id->PrettyPrint(f, level) ;
    f << endl ;
    unsigned i ;
    VhdlTreeNode *elem ;
    if (_generic_clause) {
        f << PrintLevel(level+1) << "generic (" ;
        FOREACH_ARRAY_ITEM(_generic_clause,i,elem) {
            if (!elem) continue ;
            f << endl << PrintLevel(level+2) ; // newline
            elem->PrettyPrint(f,level+2) ;
            if (i!=_generic_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    if (_port_clause) {
        f << PrintLevel(level+1) << "port (" ;
        FOREACH_ARRAY_ITEM(_port_clause,i,elem) {
            if (!elem) continue ;
            f << endl << PrintLevel(level+2) ; // newline
            elem->PrettyPrint(f,level+2) ;
            if (i!=_port_clause->Size()-1) f << "; " ;
        }
        f << ") ;" << endl ;
    }
    f << PrintLevel(level) << "end component;" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlAttributeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "attribute " ;
    _id->PrettyPrint(f, level) ;
    f << " : " ;
    if (_type_mark) _type_mark->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlAttributeSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "attribute " ;
    if (_designator) _designator->PrettyPrint(f,level+1) ;
    f << " of " ;
    if (_entity_spec) _entity_spec->PrettyPrint(f,level+1) ;
    f << " is " ;
    if (_value) _value->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlConfigurationSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "for " ;
    if (_component_spec) {
        _component_spec->PrettyPrint(f,level+1) ;
        f << " " ;
    }
    if (_binding) _binding->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlDisconnectionSpec::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "disconnect " ;
    if (_guarded_signal_spec) _guarded_signal_spec->PrettyPrint(f,level+1) ;
    f << " after " ;
    if (_after) _after->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlGroupTemplateDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "group " ;
    _id->PrettyPrint(f, level) ;
    f << " is (" ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_entity_class_entry_list, i, item) {
        if (!item) continue ;
        if (i!=0) f << "," ;
        item->PrettyPrint(f,level+1) ;
    }
    f << ") ;" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlGroupDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "group " ;
    _id->PrettyPrint(f, level) ;
    f << " : " ;
    if (_group_template_name) _group_template_name->PrettyPrint(f,level+1) ;
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
void
VhdlSubprogInstantiationDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_subprogram_spec) _subprogram_spec->PrettyPrint(f, level) ;

    f << " is new " ;

    if (_uninstantiated_subprog_name) _uninstantiated_subprog_name->PrettyPrint(f, level) ;

    if (_generic_map_aspect) {
        f << " generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ") " ;
    }
    f << ";" ;
    PrettyPrintPostComments(f) ;
}
void
VhdlInterfaceTypeDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << EntityClassName(_interface_kind) << " " ;

    if (_type_id) _type_id->PrettyPrint(f,level) ;

    // Don't print post comments here, they should be printed by the caller,
    // since we don't put the separator(;) from here, we cannot print the
    // post comment here. This function is called from three places:
    //   1. VhdlEntityDecl::PrettyPrint()
    //   2. VhdlProcedureSpec::PrettyPrint()
    //   3. VhdlFunctionSpec::PrettyPrint()
    // The first function prints the post comments properly for both
    // port declaration and generic declaration.
    // The second and third function does not prints post comments yet,
    // Please see the explanation comments in the corresponding functions.
}
void
VhdlInterfaceSubprogDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    if (_subprogram_spec) _subprogram_spec->PrettyPrint(f,level) ;

    if (_init_assign) {
        f << " is " ;
        _init_assign->PrettyPrint(f,level) ;
    }

    // Don't print post comments here, they should be printed by the caller,
    // since we don't put the separator(;) from here, we cannot print the
    // post comment here. This function is called from three places:
    //   1. VhdlEntityDecl::PrettyPrint()
    //   2. VhdlProcedureSpec::PrettyPrint()
    //   3. VhdlFunctionSpec::PrettyPrint()
    // The first function prints the post comments properly for both
    // port declaration and generic declaration.
    // The second and third function does not prints post comments yet,
    // Please see the explanation comments in the corresponding functions.
}
void
VhdlInterfacePackageDecl::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "package " ;
    _id->PrettyPrint(f, level) ;
    f << " is new " ;

    if (_uninst_package_name) _uninst_package_name->PrettyPrint(f,level) ;

    if (_generic_map_aspect) {
        f << endl ;
        f << PrintLevel(level+1) << "generic map (" ;
        PrintAssocListWithComments(_generic_map_aspect, f, level+1) ;
        f << ")" ;
    }

    // Don't print post comments here, they should be printed by the caller,
    // since we don't put the separator(;) from here, we cannot print the
    // post comment here. This function is called from three places:
    //   1. VhdlEntityDecl::PrettyPrint()
    //   2. VhdlProcedureSpec::PrettyPrint()
    //   3. VhdlFunctionSpec::PrettyPrint()
    // The first function prints the post comments properly for both
    // port declaration and generic declaration.
    // The second and third function does not prints post comments yet,
    // Please see the explanation comments in the corresponding functions.
}

//--------------------------------------------------------------
// #included from VhdlConfiguration.h
//--------------------------------------------------------------

void
VhdlBlockConfiguration::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "for " ;
    if (_block_spec) _block_spec->PrettyPrint(f,level+1) ;
    f << endl ;
    unsigned i ;
    VhdlTreeNode *item ;
    FOREACH_ARRAY_ITEM(_use_clause_list,i,item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    FOREACH_ARRAY_ITEM(_configuration_item_list,i,item) {
        if (!item) continue ;
        item->PrettyPrint(f,level+1) ;
    }
    f << PrintLevel(level) << "end for ;" ;
    PrettyPrintPostComments(f) ;
}

void
VhdlComponentConfiguration::PrettyPrint(ostream &f, unsigned level) const
{
    RETURN_IF_TICK_PROTECTED ;

    f << PrintLevel(level) ;

    // Print comments (if they exist)
    PrettyPrintPreComments(f, level) ;

    f << "for " ;
    if (_component_spec) _component_spec->PrettyPrint(f,level+1) ;
    f << " " ;
    if (_binding) {
        _binding->PrettyPrint(f,level+1) ;
        f << ";" ;
    }
    f << " " ;
    if (_block_configuration) _block_configuration->PrettyPrint(f,level+1) ;
    f << " end for ; " ;
    PrettyPrintPostComments(f) ;
}

