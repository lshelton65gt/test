/*
 *
 * [ File Version : 1.18 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <iostream>
#include <sstream>                     // for ostringstream

#include "VhdlExplicitStateMachine.h"  // Make visitor pattern available

#include "Message.h"                   // Make message handlers available
#include "Strings.h"                   // Definitions of all verilog module item tree nodes

#include "vhdl_file.h"                 // Make vhdl reader available
#include "vhdl_tokens.h"

#include "VhdlScope.h"
#include "VhdlUnits.h"                 // Definition of a VhdlUnits
#include "VhdlExpression.h"            // Definitions of all vhdl expression tree nodes
#include "VhdlIdDef.h"                 // Definitions of all vhdl identifier tree nodes
#include "VhdlName.h"
#include "VhdlStatement.h"             // Definitions of all vhdl statement tree nodes
#include "VhdlMisc.h"                  // Definitions of all extraneous vhdl tree nodes

#include "../util/ControlFlow.h"       // Classes for CFG

using namespace std ;

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//#define EXPLICIT_STATE_MACHINE_DEBUG   // <--- Comment this line out to control flag from build environment, or uncomment to force on.

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
static
void print_veri_tree_node(void* obj)
{
    VhdlTreeNode *node = (VhdlTreeNode *)obj ;
    if (node) node->PrettyPrint(cout, 0) ;
}
#endif

/*-----------------------------------------------------------------*/
//      VhdlFsmProcessStatementVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VhdlFsmProcessStatementVisitor::VhdlFsmProcessStatementVisitor():
    VhdlVisitor(),
    _event_expr_set(STRING_HASH),
    _event_control_set(POINTER_HASH),
    _unit_scope(0),
    _scope(0),
    _process(0),
    _process_stmt_replaced(0),
    _has_process(0),
    _process_construct(0),
    _has_wait_stmt(0),
    _is_edged(1),
    _mult_clk(0),
    _infinite_loop(0),
    _var_id(0),
    _digit(0),
    _sensitivity_list(0),
    _decl_node(0),
    _reset(0)
{
}

VhdlFsmProcessStatementVisitor::~VhdlFsmProcessStatementVisitor()
{
    // Delete the string stored in the set
    SetIter si ;
    char *s ;
    FOREACH_SET_ITEM(&_event_expr_set, si, &s) {
        Strings::free(s) ;
    }

    _decl_node = 0 ;
    _var_id = 0 ;
    _process= 0 ;
    _scope = 0 ;
    _unit_scope = 0 ;
}

// Find string literal:
char*
VhdlFsmProcessStatementVisitor::FindStringLiteralOfStateNum(VerificCFGRootNode *node, unsigned digit_count)
{
    unsigned state_num = node->GetState() ;
    Array *bin_arr = new Array(digit_count) ;
    unsigned digit = 0 ;
    unsigned count = 0 ;
    while(state_num) {
        digit = state_num % 2 ;
        bin_arr->InsertLast((void*)(unsigned long)digit) ;
        state_num = state_num >> 1 ;
        count++ ;
    }

    while(count < digit_count) {
        bin_arr->InsertLast((void*)0) ;
        count++ ;
    }

    char *str = 0 ;
    while(bin_arr->Size()) {
        unsigned long element = (unsigned long)bin_arr->RemoveLast() ;
        char *s = Strings::itoa((int) element) ;
        if (str) {
            char *tmp1 = str ;
            str = Strings::save(str, s) ;
            Strings::free(tmp1) ;
            Strings::free(s) ;
        } else {
            str = s ;
        }
    }

    char *tmp = str ;
    str = Strings::save("\"", str, "\"") ;
    Strings::free(tmp) ;
    delete bin_arr ;

    return str ;
}

// Create unique name for variables:
// If find the variable declaration in the present scope or in the  upper scopes,
// create a new name and return the new name:
char*
VhdlFsmProcessStatementVisitor::CreateVariableName(const char *prefix, const Array *label_arr, VhdlScope *present_scope, VhdlScope *process_scope)
{
    char *name = Strings::save(prefix) ;
    unsigned ai ;
    VhdlIdDef *label_id ;
    FOREACH_ARRAY_ITEM(label_arr, ai, label_id) {
        if (!label_id) continue ;
        const char *label = label_id->Name() ;
        if (label) {
            char *temp_name = name ;
            name = Strings::save(label, ".", name) ;
            Strings::free(temp_name) ;
        }
    }

    char *full_name = Strings::save(name) ;
    VhdlScope *scope = present_scope ;
    VhdlScope *s = process_scope ;
    unsigned number = 0 ;
    while(1)
    {
        if ((scope && scope->FindSingleObjectLocal(name)) || (s && s->FindSingleObjectLocal(name))) {
            char *num = Strings::itoa((int)number) ;
            Strings::free(name) ;
            name = Strings::save(full_name, "_", num) ;
            number++ ;
            Strings::free(num) ;
        } else {
            Strings::free(full_name) ;
            return name ;
        }
    }
    return 0 ;
}

// Reset all the flages and expression set after each process statement traversal:
void
VhdlFsmProcessStatementVisitor::Reset()
{
    // Reset all flags:
    _has_process = 0 ;
    _has_wait_stmt = 0 ;
    _is_edged = 1 ;
    _mult_clk = 0 ;
    _infinite_loop = 0 ;
    _var_id = 0 ;

    // Reset the sets:
    SetIter si ;
    char *s ;
    FOREACH_SET_ITEM(&_event_expr_set, si, &s) {
        Strings::free(s) ;
    }
    _event_expr_set.Reset() ;
    _event_control_set.Reset() ;

    delete _decl_node ;
    _decl_node = 0 ;
}

// Cleanup cfg node:
void
VhdlFsmProcessStatementVisitor::CleanupParseTreeArray(Array *arr) const
{
    unsigned ai ;
    VhdlTreeNode *node ;
    FOREACH_ARRAY_ITEM(arr, ai, node) {
        delete node ;
    }
    delete arr ;
}

/*-----------------------------------------------------------------*/
//    TraverseArray routine
/*-----------------------------------------------------------------*/

// Overwrite the VhdlVisitor::TraverseArray()
void VhdlFsmProcessStatementVisitor::TraverseArray(const Array *array)
{
    if (!array) return ;
    VERIFIC_ASSERT(!_process) ;

    // For each module item visit the corresponding visit routine
    // and for any process statement if we able to construct
    // the new process statement, replace the old one with the new one.
    VhdlTreeNode *node_item = 0 ;
    unsigned int i = 0 ;

    FOREACH_ARRAY_ITEM(array, i, node_item) {
        if (node_item) node_item->Accept(*this) ;
        if (_process) {
            ((Array*)array)->Insert(i, _process) ;
            delete node_item ;
            _process= 0 ;
        }
    }
}

void VhdlFsmProcessStatementVisitor::VHDL_VISIT(VhdlPrimaryUnit, unit)
{
    // Traverse secondary units
    MapIter mi ;
    VhdlTreeNode *sec_unit = 0 ;
    FOREACH_MAP_ITEM(unit.AllSecondaryUnits(), mi, 0, &sec_unit) {
        if (!sec_unit) continue ;
        _unit_scope = sec_unit->LocalScope() ;
        _scope = sec_unit->LocalScope() ;
        sec_unit->Accept(*this) ;
    }
}

void VhdlFsmProcessStatementVisitor::VHDL_VISIT(VhdlGenerateStatement, generate_stmt)
{
    // Keep the old scope:
    VhdlScope *old_scope = _scope ;
    _scope = generate_stmt.LocalScope() ;

    // Traverse statements
    TraverseArray(generate_stmt.GetStatementPart()) ;

    // Restore the scope:
    _scope = old_scope ;
}

void VhdlFsmProcessStatementVisitor::VHDL_VISIT(VhdlBlockStatement, blk_stmt)
{
    // Keep the old scope:
    VhdlScope *old_scope = _scope ;
    _scope = blk_stmt.LocalScope() ;

    // Traverse statements
    TraverseArray(blk_stmt.GetStatements()) ;

    // Restore the scope:
    _scope = old_scope ;
}

// Traverse wait statement:
void VhdlFsmProcessStatementVisitor::VHDL_VISIT(VhdlWaitStatement, wait_stmt)
{
    _has_wait_stmt = 1 ;

    // Check for use of multiple cloking:
    // For the time being we do not support use of multiple clocking.
    if (!_is_edged || _mult_clk) return ;

    // Inset all the cloking event nodes in the set: need to count the
    // total number of event statements within a process statement
    if (_has_process) (void)_event_control_set.Insert(&wait_stmt) ;

    // Check for multiple cloking:
    // Store the first clocking event in the set
    // if any other clocking event is present than
    // set the flag _mult_clk.
    // The clocking event is stored as a string:
    VhdlExpression *expression = wait_stmt.UntilClause() ;
    if (expression && !expression->FindIfEdge())  _is_edged = 0 ;

    Array *expr_arr = expression ? expression->GetAssocList() : 0 ;
    VhdlExpression *expr ;
    unsigned ai ;
    char *str = 0 ;
    FOREACH_ARRAY_ITEM(expr_arr, ai, expr) {
        if (!expr) continue ;

        ostringstream os ; // Dynamically allocated string buffer
        expr->PrettyPrint(os, 0) ;
        if (str) {
            char *temp = str ;
            str = Strings::save(str, ",") ;
            Strings::free(temp) ;
        }
        string pp_str = os.str() ;
        const char *s = pp_str.c_str() ;
        char *temp = str ;
        str = str ? Strings::save(str, s) : Strings::save(s) ;
        Strings::free(temp) ;
    }

    if (_is_edged && expr_arr && expr_arr->Size()) { // Collect those expression string that have posedge/negedge clock
        if (!_event_expr_set.Size()) {  // Check whether the clocking event is different
            (void) _event_expr_set.Insert(str) ;
        } else {
            if (!_event_expr_set.GetItem(str)) _mult_clk = 1 ;
            Strings::free(str) ;
        }
    } else {
        Strings::free(str) ; //JJ: mem leak fix
    }
}

void VhdlFsmProcessStatementVisitor::VHDL_VISIT(VhdlProcessStatement, process_stmt)
{
    _has_process = 1 ;

    // process cannot have both a wait statement and a sensitivity list,
    // so, as we are interested in wait statements, do nothing if process
    // has sensitivity_list:
    if (process_stmt.GetSensitivityList()) {
        Reset() ;
        return ;
    }

    // Traverse statements
    // Need for count the total number of clocking events: if not more
    // than one, no need to convert to explicit state m/c
    TraverseArray(process_stmt.GetStatementPart()) ;

    // Check for multiple clocking:
    if (_mult_clk) {
        process_stmt.Info("multiple clocking is not yet supported") ;
        Reset() ;
        return ;
    }

    // Not converting statement with event control without edged expression:
    if (!_is_edged) {
        process_stmt.Info("not converting statement with event control without edged expression") ;
        Reset() ;
        return ;
    }

    // No need to convert to the explicitStateMachine if there is only one wait statement:
    if (_event_control_set.Size() <= 1) {
        process_stmt.Info("single wait statement, no need to convert to explicit state machine") ;
        Reset() ;
        return ;
    }

    // store all the declarations:
    if (!_decl_node) _decl_node = new VerificCFGDeclarationNode() ;
    unsigned ai ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(process_stmt.GetDeclPart(), ai, decl) {
        if (!decl) continue ;
        (void) _decl_node->AddDeclaration(decl, 0) ;
    }

    // Check conditions/limitations, generate the CFG and create a new process statement:
    VhdlProcessStatement *new_process_stmt = ConvertToExplicitStateMachine(&process_stmt) ;
    if (Message::ErrorCount()) {
        // some error/parse-tree inconsistency during the conversion
        // delete this faulty conversion node..
        // reset the error count to start fresh subsequently..
        Message::ClearErrorCount() ;
        delete new_process_stmt ;
        new_process_stmt = 0 ;
    }

    // Store the new item:
    if (new_process_stmt) {
        _process_stmt_replaced++ ;
        _process = new_process_stmt ;
    }

    // Reset all flags:
    Reset() ;
}

// Check some conditions/limitations, create the new module item:
VhdlProcessStatement *
VhdlFsmProcessStatementVisitor::ConvertToExplicitStateMachine(const VhdlProcessStatement *stmt)
{
    if (!stmt) return 0 ;
    // Number the process construct to be processed that will be used during generating
    // label id in explicit state machine generation machine:
    _process_construct++ ;

    // Traverse and generate the control flow graph, print the graph, modify the parse tree
    // and generate the corresponding explicit state machine:
    VhdlProcessStatement *new_process_stmt = ProcessStatement(stmt) ;
    return new_process_stmt ;
}

// Generate the CFG and create a new process statement:
VhdlProcessStatement*
VhdlFsmProcessStatementVisitor::ProcessStatement(const VhdlProcessStatement *process_stmt)
{
    if (!process_stmt) return 0 ;

    // Generate the control flow diagram:
    // Traverse statements
    VhdlFsmProcessVisitor process_visitor ;
    process_visitor.SetScope(_scope) ;
    process_visitor.SetUnitScope(_unit_scope) ;
    process_visitor.SetDeclNode(_decl_node) ;

    // map for copy ids:
    VhdlMapForCopy old2new(POINTER_HASH) ;

    // Label id declaration:
    char *process_construct = Strings::itoa(_process_construct) ;
    char *tmp1 = process_construct ;
    char *prefix = CreateVariableName("verific_implicit_to_explicit_conv_scope", 0, _scope) ;
    char *tmp2 = prefix ;
    VhdlIdDef *label_id = new VhdlLabelId(Strings::save(prefix, "_", process_construct)) ;
    Strings::free(tmp1) ;
    Strings::free(tmp2) ;
    if (_scope) (void) _scope->Declare(label_id) ; // Declare the labeled id in the present scope

    // Copy the old scope or create new process scope from the parent scope:
    VhdlScope *old_scope = process_stmt->LocalScope() ;
    VhdlIdDef *owner = old_scope ? old_scope->GetOwner() : 0 ;
    if (owner) old2new.SetCopiedId(owner, label_id) ;

    VhdlScope *process_scope = old_scope ? old_scope->Copy(old2new) : 0 ;
    if (!process_scope) process_scope = new VhdlScope(_scope) ;
    process_scope->Update(old_scope, old2new) ;
    VhdlNode::_present_scope = process_scope ;

    // VIPER #6879:
    process_visitor.SetProcessScope(process_scope) ;

    unsigned ai ;
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(process_stmt->GetStatementPart(), ai, stmt) {
        stmt->Accept(process_visitor) ;
    }

    VerificCFGRootNode *root = process_visitor.GetParentNode() ;
    if (!root) {
        if (!owner && !_scope) delete label_id ; // conditionally free label_id if not handed off by now
        return 0 ;
    }
    VerificCFGRootNode *child = process_visitor.GetCurrentNode() ;
    // Set the last node as the parent of the first node:
    // eg: 1. statement node
    //     2. state node
    //     3. condition node
    //     4. end node
    // Now here _parent_node is no1 statement node and _current_node is no4 end_node,
    // so we set no4 node as the parent node of no1 node and no1 as the child node of no4 node.
    (void) root->AddParent(child) ;

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
    // Print the control flow diagram:
    root->PrintNode(&print_veri_tree_node, 0) ;
    printf ("--------------------------------------\n") ;
#endif

    // Count the state nodes
    Array state_count ;
    CountStateNode(root, state_count, 0) ;

#ifdef EXPLICIT_STATE_MACHINE_DEBUG
    printf("state_count = %d", state_count.Size()) ;
    printf("\n") ;
    printf ("--------------------------------------\n") ;
#endif

    // Copy parse tree and generate the explicit state machine:
    // Array containing all the declared items
    Array *decl_items = new Array() ;

    VhdlIdDef *reset_id = 0 ;
    if (_reset && !root->IsStateNode()) {
        // Reset variable declaration:
        prefix = CreateVariableName("reset_var", 0, _scope) ;
        reset_id = new VhdlVariableId(prefix) ;
        reset_id->SetLinefile(process_stmt->Linefile()) ;
        Array *reset_id_list = new Array(1) ;
        reset_id_list->InsertLast(reset_id) ;
        (void) process_scope->Declare(reset_id) ;

        VhdlIdDef *reset_type_id = VhdlTreeNode::StdType("bit") ;
        VhdlName *reset_type_mark = new VhdlIdRef(reset_type_id) ;
        VhdlCharacterLiteral *reset_init_expr = new VhdlCharacterLiteral(Strings::save("'1'")) ;
        VhdlVariableDecl *reset_var_decl = new VhdlVariableDecl(0, reset_id_list, reset_type_mark, reset_init_expr) ;
        reset_var_decl->SetLinefile(process_stmt->Linefile()) ;
        decl_items->InsertLast(reset_var_decl) ;
    }

    // State var declaration:
    prefix = CreateVariableName("state_var", 0, _scope) ;
    VERIFIC_ASSERT(!_var_id && "old state id not freed") ;
    _var_id = new VhdlVariableId(prefix) ;
    _var_id->SetLinefile(process_stmt->Linefile()) ;
    Array *state_id_list = new Array(1) ;
    state_id_list->InsertLast(_var_id) ;
    (void) process_scope->Declare(_var_id) ;

    Array *assoc_list = new Array() ;
    VhdlExpression *right = new VhdlInteger((verific_int64)0) ;
    unsigned num = process_visitor.GetStateNum() ;
    unsigned state_num = (num>0) ? (num-1) : num ;
    unsigned digit = 0 ;
    while(state_num) {
        digit++ ;
        state_num = state_num >> 1 ;
    }
    _digit = digit ;
    if (digit) digit = digit-1 ;
    VhdlExpression *left = new VhdlInteger((verific_int64)digit) ;
    VhdlRange *range = new VhdlRange(left, VHDL_downto, right) ;
    assoc_list->InsertLast(range) ;

    VhdlIdDef *state_type_id = VhdlTreeNode::StdType("bit_vector") ;
    VhdlName *state_type_mark = new VhdlIdRef(state_type_id) ;
    VhdlIndexedName *indexed_name = new VhdlIndexedName(state_type_mark, assoc_list) ;
    Array *bin_arr = new Array(_digit) ;
    unsigned count = 0 ;
    while(count < _digit) {
        bin_arr->InsertLast((void*)0) ;
        count++ ;
    }

    char *str = 0 ;
    while(bin_arr->Size()) {
        unsigned long element = (unsigned long)bin_arr->RemoveLast() ;
        char *s = Strings::itoa((int) element) ;
        if (str) {
            tmp1 = str ;
            str = Strings::save(str, s) ;
            Strings::free(tmp1) ;
            Strings::free(s) ;
        } else {
            str = s ;
        }
    }

    char *tmp = str ;
    str = Strings::save("\"", str, "\"") ;
    Strings::free(tmp) ;
    delete bin_arr ;

    VhdlStringLiteral *init_expr = new VhdlStringLiteral(str) ;
    VhdlVariableDecl *state_var_decl = new VhdlVariableDecl(0, state_id_list, indexed_name, init_expr) ;
    state_var_decl->SetLinefile(process_stmt->Linefile()) ;
    decl_items->InsertLast(state_var_decl) ;

    // Declare all the ids in the new process scope
    unsigned num_decls = _decl_node ? _decl_node->NumDeclaration() : 0 ;
    unsigned i ;
    for (i=0; i<num_decls; i++) {
        VhdlDeclaration *data_decl = _decl_node ? (VhdlDeclaration *) _decl_node->GetDeclaration(i) : 0 ;
        if (!data_decl) continue ;
        VhdlDeclaration *new_data_decl = data_decl->CopyDeclaration(old2new) ;
        decl_items->InsertLast(new_data_decl) ; // store in the array of declaring items

        Array *decl_ids = new_data_decl->GetIds() ;
        unsigned j ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(decl_ids, j, id) {
            if (!id) continue ;
            // If name conflicts, change the old name
            char *prefix_name = VhdlFsmProcessStatementVisitor::CreateVariableName(id->Name(), 0, process_scope) ;
            if (!Strings::compare(prefix_name, id->Name())) {
                // Change the name of the identifier
                id->ChangeIdName(prefix_name) ;
            } else {
                Strings::free(prefix_name) ;
            }
            (void) process_scope->Declare(id) ; // declare all ids in the process scope
        }
    }

    // VIPER #6879: update teh mapfor copy id for iter variables declared
    // during cfg generatuon:
    MapIter mi ;
    VhdlIdDef *old_id ;
    VhdlVariableDecl *decl ;
    Map decl_node_map = process_visitor.GetDeclNodeMap() ;
    FOREACH_MAP_ITEM(&decl_node_map, mi, &old_id, &decl) {
        if (!decl) continue ;
        decl_items->InsertLast(decl) ; // store in the array of declaring items

        Array *decl_ids = decl->GetIds() ;
        unsigned j ;
        VhdlIdDef *id ;
        FOREACH_ARRAY_ITEM(decl_ids, j, id) {
            if (!id) continue ;
            old2new.SetCopiedId(old_id, id) ;
        }
    }

    // contains the starting and ending node of a cfg path
    Map state_map(POINTER_HASH) ;

    // Reset block:
    Array *reset_stmt_arr = new Array() ;
    if (_reset && !root->IsStateNode()) {
        // until get a state node
        ExplicitStateMachine(root, 0, state_map, 0, reset_stmt_arr, old2new) ;

        VhdlIdRef *l_val = new VhdlIdRef(reset_id) ;
        VhdlCharacterLiteral *reset_expr = new VhdlCharacterLiteral(Strings::save("'0'")) ;
        VhdlVariableAssignmentStatement *reset_stmt = new VhdlVariableAssignmentStatement(l_val, reset_expr) ;
        reset_stmt->SetLinefile(process_stmt->Linefile()) ;
        reset_stmt_arr->InsertLast(reset_stmt) ;
        state_map.Reset() ;
    }

    // Set block:
    Array *case_alternatives = new Array(state_count.Size()+1) ;
    VerificCFGStateNode *state_node ;
    FOREACH_ARRAY_ITEM(&state_count, ai, state_node) {
        if (!state_node) continue ;
        Array *expr_arr = new Array() ;
        Array *stmt_arr = new Array() ;
        ExplicitStateMachine(state_node, 0, state_map, expr_arr, stmt_arr, old2new) ;

        VhdlCaseStatementAlternative *case_alternative = new VhdlCaseStatementAlternative(expr_arr, stmt_arr) ;
        case_alternative->SetLinefile(state_node->GetLinefile()) ;
        case_alternatives->InsertLast(case_alternative) ;

        state_map.Reset() ;
    }
    state_map.Reset() ;

    // default statements:
    Array *default_stmt_arr = new Array() ;
    Array *default_cond_arr = new Array() ;
    VhdlOthers *others = new VhdlOthers() ;
    default_cond_arr->InsertLast((void*)others) ;

    VhdlIdRef *l_val = new VhdlIdRef(_var_id) ;
    VhdlVariableAssignmentStatement *default_stmt = new VhdlVariableAssignmentStatement(l_val, init_expr->CopyDesignator(old2new)) ;
    default_stmt_arr->InsertLast(default_stmt) ;
    VhdlCaseStatementAlternative *default_alt = new VhdlCaseStatementAlternative(default_cond_arr, default_stmt_arr) ;
    default_alt->SetLinefile(process_stmt->Linefile()) ;
    case_alternatives->InsertLast(default_alt) ;

    // Infinite loop exists:
    if (_infinite_loop) {
        process_stmt->Warning("possible infinite loop in some certain condition") ;
    }

    // case statement:
    Array *case_stmt_arr = new Array() ;
    VhdlIdRef *case_condition = new VhdlIdRef(_var_id) ;
    VhdlCaseStatement *case_stmt = new VhdlCaseStatement(case_condition, case_alternatives) ;
    case_stmt->SetLinefile(process_stmt->Linefile()) ;
    case_stmt_arr->InsertLast(case_stmt) ;

    Array *process_stmts = new Array(1) ;
    VhdlIfStatement *if_stmt = 0 ;
    if (_reset && !root->IsStateNode()) {
        VhdlIdRef *cond_id_ref = new VhdlIdRef(reset_id) ;
        VhdlCharacterLiteral *val = new VhdlCharacterLiteral(Strings::save("'1'")) ;
        VhdlOperator *cond_expr = new VhdlOperator(cond_id_ref, VHDL_EQUAL, val) ;
        if_stmt = new VhdlIfStatement(cond_expr, reset_stmt_arr, 0, case_stmt_arr) ;
        if_stmt->SetLinefile(process_stmt->Linefile()) ;
        process_stmts->InsertLast(if_stmt) ;
    } else {
        case_stmt_arr->Reset() ;
        delete case_stmt_arr ;
        delete reset_stmt_arr ;
    }

    if (!if_stmt) {
        process_stmts->InsertLast(case_stmt) ;
    }

    // Generate process statement:
    VhdlStatement *wait_stmt = (VhdlStatement *) _event_control_set.GetLast() ;
    VhdlStatement *copied_wait_stmt = wait_stmt->CopyStatement(old2new) ;
    process_stmts->InsertLast(copied_wait_stmt) ;

    VhdlProcessStatement *process = new VhdlProcessStatement(0, decl_items, process_stmts, process_scope) ;
    process->SetLabel(label_id) ;
    process->SetLinefile(process_stmt->Linefile()) ;

    // delete newly created items:
    Array *newly_created_arr = process_visitor.GetNewlyCreatedArray() ;
    CleanupParseTreeArray(newly_created_arr) ;

    (void) root->AddParent(0) ; // Delete pointer between the first and last node:

    delete root ;        // Delete the cfg
    return process ;     // return the newly created statement:
}

/*-----------------------------------------------------------------*/
//           VhdlFsmProcessVisitor: Constructor / Destructor
/*-----------------------------------------------------------------*/

VhdlFsmProcessVisitor::VhdlFsmProcessVisitor():
    VhdlVisitor(),
    _parent_node(0),
    _current_node(0),
    _loop_node(0),
    _labeled_loop_map(POINTER_HASH),
    _state_num(0),
    _unit_scope(0),
    _scope(0),
    _process_scope(0),
    _incr_node_map(POINTER_HASH),
    _next_stmt_nodes(POINTER_HASH),
    _cond_expr(0),
    _incr_stmt(0),
    _newly_created_arr(new Array()),
    _decl_node(0),
    _decl_node_map(POINTER_HASH),
    _elsif_arr(0),
    _else_arr(0),
    _left(0),
    _right(0)
{
}

VhdlFsmProcessVisitor::~VhdlFsmProcessVisitor()
{
    _labeled_loop_map.Reset() ;
    _incr_node_map.Reset() ;
    _next_stmt_nodes.Reset() ;
    _cond_expr = 0 ;
    _decl_node = 0 ;
    _decl_node_map.Reset() ;
    _loop_node = 0 ;
    _current_node = 0 ;
    _parent_node = 0 ;
    _process_scope = 0 ;
    _scope = 0 ;
    _unit_scope = 0 ;
}

// Traverse the statements:
void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlStatement, stmt)
{
    // If the _current_node is a statement node, we do not create a new statement node
    // rather store the statement in the array of the current statement node:
    if (!_current_node || (!_current_node->IsSimpleStatementNode() || _current_node->IsStateNode())) {
        VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
        if (_current_node) (void) statement->AddParent(_current_node) ;
        _current_node = statement ;
    }

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    (void) _current_node->AddStatement(&stmt, _scope) ;
}

// Traverse the statements:
void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlWaitStatement, wait_stmt)
{
    // Create a state node:
    VerificCFGStateNode *state = new VerificCFGStateNode() ;
    (void) state->AddParent(_current_node) ;
    _current_node = state ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    // Traverse sensitivity clause
    TraverseArray(wait_stmt.GetSensitivityClause()) ;

    // Traverse condition clause
    TraverseNode(wait_stmt.UntilClause()) ;

    // Traverse timeout clause
    TraverseNode(wait_stmt.GetTimeoutClause()) ;

    // Get the state, increase that by one and
    // set the new state to the _current_node:
    state->SetState(_state_num++) ;
    state->SetLinefile(wait_stmt.Linefile()) ;
}

// Traverse the statements:
void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlIfStatement, if_stmt)
{
    // Create a conditional node
    VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
    (void) cond->AddParent(_current_node) ;
    _current_node = cond ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    // Add conditions:
    cond->SetConditionExpression((void*)if_stmt.GetIfCondition()) ;

    VERIFIC_ASSERT(cond == _current_node) ;

    // scope end node:
    VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond) ;
    (void) cond->SetScopeEnd(cond_end) ;

    // Keep the parent node:
    VerificCFGRootNode *orig_parent_node = _parent_node ;

    // For each branch of conditional node create a fresh parse tree
    // keeping the _parent_node and _current_node in stack and make
    // each of them null before going to process then and else part:
    _current_node = 0 ;
    _parent_node = 0 ;

    // Traverse then statements
    TraverseArray(if_stmt.GetIfStatements()) ;
    (void) cond->AddBranch(0, _parent_node) ;
    (void) cond_end->AddParent(_current_node) ;

    // Again make the _current_node and
    // the _parent_node set to null:
    _current_node = 0 ;
    _parent_node = 0 ;

    if (if_stmt.GetElsifList()) {
        _elsif_arr = if_stmt.GetElsifList() ;
        _else_arr = if_stmt.GetElseStatments() ;

        // Traverse elseif statements
        TraverseNode((VhdlTreeNode*)_elsif_arr->GetFirst()) ;
        (void) cond->AddBranch(0, _parent_node) ;
        (void) cond_end->AddParent(_current_node) ;
    }

    if (!if_stmt.GetElsifList()) {
        // Traverse else statements
        TraverseArray(if_stmt.GetElseStatments()) ;
        (void) cond->AddBranch(0, _parent_node) ;
        if (_current_node) {
            // Has else part
            (void) cond_end->AddParent(_current_node) ;
        } else { // no else part
            (void) cond_end->AddParent(cond) ;
        }
    }

    // Keep a link between the conditional node and
    // the scope end node:
    _current_node = cond_end ;
    // Restore the original parent node:
    _parent_node = orig_parent_node ;
}

// Traverse the statements:
void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlElsif, elsif)
{
    // Create a conditional node
    VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
    (void) cond->AddParent(_current_node) ;
    _current_node = cond ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    // Add conditions:
    cond->SetConditionExpression((void*)elsif.Condition()) ;

    VERIFIC_ASSERT(cond == _current_node) ;

    // scope end node:
    VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond) ;
    (void) cond->SetScopeEnd(cond_end) ;

    // Keep the parent node:
    VerificCFGRootNode *orig_parent_node = _parent_node ;

    // For each branch of conditional node create a fresh parse tree
    // keeping the _parent_node and _current_node in stack and make
    // each of them null before going to process then and else part:
    _current_node = 0 ;
    _parent_node = 0 ;

    // Traverse then statements
    TraverseArray(elsif.GetStatements()) ;
    (void) cond->AddBranch(0, _parent_node) ;
    (void) cond_end->AddParent(_current_node) ;

    // Again make the _current_node and
    // the _parent_node set to null:
    _current_node = 0 ;
    _parent_node = 0 ;

    // Traverse elseif statements
    if (_elsif_arr && _elsif_arr->Size() > 1) {
        _elsif_arr->Remove(0) ;
        VhdlTreeNode *node = (VhdlTreeNode*)_elsif_arr->GetFirst() ;
        TraverseNode(node) ;
    } else {
        TraverseArray(_else_arr) ;
    }
    (void) cond->AddBranch(0, _parent_node) ;
    (void) cond_end->AddParent(_current_node) ;

    // Keep a link between the conditional node and
    // the scope end node:
    _current_node = cond_end ;
    // Restore the original parent node:
    _parent_node = orig_parent_node ;
}

// Traverse the statements:
void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlCaseStatement, case_stmt)
{
    // Create conditional node
    VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
    (void) cond->AddParent(_current_node) ;
    _current_node = cond ;

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    // Add conditions:
    cond->SetConditionExpression((void*)case_stmt.GetExpression()) ;

    VERIFIC_ASSERT(cond == _current_node) ;

    // Keep the original parent node:
    VerificCFGRootNode *orig_parent_node = _parent_node ;

    // Keep a link between the conditional node and
    // the scope end node:
    VerificCFGScopeEndNode *cond_end = new VerificCFGScopeEndNode(cond) ;
    (void) cond->SetScopeEnd(cond_end) ;

    // For each branch of conditional node create a fresh parse tree
    // keeping the _parent_node and _current_node in stack and make
    // each of them null before going to process each case item:
    // Traverse case items
    TraverseArray(case_stmt.GetAlternatives()) ;

    _current_node = cond_end ;
    // Restore the original parent node:
    _parent_node = orig_parent_node ;
}

void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlCaseStatementAlternative, case_alt)
{
    VerificCFGRootNode *cond = _current_node ;
    if (!cond || !cond->IsConditionalNode()) return ;

    _current_node = 0 ;
    _parent_node = 0 ;
    TraverseArray(case_alt.GetChoices()) ;

    _current_node = 0 ;
    _parent_node = 0 ;
    TraverseArray(case_alt.GetStatements()) ;

    Array *case_cond_arr = case_alt.GetChoices() ;
    Array *cond_arr = 0 ;
    if (case_cond_arr) cond_arr = new Array(*(case_cond_arr)) ;
    (void) cond->AddBranch(cond_arr, _parent_node) ;

    VerificCFGRootNode *cond_end  = cond->GetScopeEnd() ;
    if (cond_end) (void) cond_end->AddParent(_current_node) ;

    _current_node = cond ;
}

void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlForScheme, for_scheme)
{
    // If the _current_node is a statement node, we do not create a new statement node
    // rather store the statement in the array of the current statement node:
    if (!_current_node || (!_current_node->IsSimpleStatementNode() || _current_node->IsStateNode())) {
        VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
        if (_current_node) (void) statement->AddParent(_current_node) ;
        _current_node = statement ;
    }

    // Set the parent node if there is no parent node:
    if (!_parent_node) _parent_node = _current_node ;

    VhdlMapForCopy old_2_new(POINTER_HASH) ;
    VhdlIdDef *id = for_scheme.GetIterId() ;
    VhdlIdDef *new_id = 0 ;
    MapItem *item = _decl_node_map.GetItem(id) ;
    if (!item) {
        const char *id_name = id ? id->Name() : 0 ;
        char *new_id_name = VhdlFsmProcessStatementVisitor::CreateVariableName(id_name, 0, _scope, _process_scope) ;
        new_id = new VhdlVariableId(new_id_name) ;
        // Declare the new ids in the new process scope:
        if (_process_scope) (void) _process_scope->Declare(new_id) ;
        Array *id_list = new Array() ;
        id_list->InsertLast(new_id) ;

        // Extract the data type of the loop index variable
        VhdlIdDef *data_type = id ? id->Type() : 0 ;

        // Create the data type
        VhdlIdRef *type = 0 ;
        if (data_type) {
            type = new VhdlIdRef(data_type) ;
        }
        VhdlVariableDecl *var_decl = new VhdlVariableDecl(0, id_list, type, 0) ;

        (void) _decl_node_map.Insert(id, var_decl) ;
    } else { // if the id is already created and declared then collect that id from the declaration
        // and use that for incremental and assignment statement:
        VhdlVariableDecl *decl = (VhdlVariableDecl *) _decl_node_map.GetValue(id) ;
        Array *decl_ids = decl ? decl->GetIds() : 0 ;
        unsigned i ;
        VhdlIdDef *id_def ;
        FOREACH_ARRAY_ITEM(decl_ids, i, id_def) {
            if (!id_def) continue ;
            new_id = id_def ;
        }
    }

    VhdlIdRef *id_ref = new VhdlIdRef(new_id) ;
    VhdlRange *range = (VhdlRange *)for_scheme.GetRange() ;
    VhdlExpression *left_range = range ? range->GetLeftExpression() : 0 ;
    verific_int64 left = left_range ? left_range->EvaluateConstantInteger() : 0 ;
    VhdlExpression *right_range = range ? range->GetRightExpression() : 0 ;
    verific_int64 right = right_range ? right_range->EvaluateConstantInteger() : 0 ;
    VhdlVariableAssignmentStatement *assign_stmt = new VhdlVariableAssignmentStatement(id_ref ? id_ref->Copy(old_2_new): 0 , left_range ? left_range->CopyExpression(old_2_new) : 0) ;
    if (left < right) {
        VhdlOperator *oper = new VhdlOperator(id_ref ? id_ref->Copy(old_2_new) : 0 , VHDL_PLUS, new VhdlInteger((verific_int64)1)) ;
        _incr_stmt = new VhdlVariableAssignmentStatement(id_ref ? id_ref->Copy(old_2_new) : 0, oper) ;
    } else {
        VhdlOperator *oper = new VhdlOperator(id_ref ? id_ref->Copy(old_2_new) : 0 , VHDL_MINUS, new VhdlInteger((verific_int64)1)) ;
        _incr_stmt = new VhdlVariableAssignmentStatement(id_ref ? id_ref->Copy(old_2_new) : 0, oper) ;
    }

    _left = left ;
    _right = right ;

    _newly_created_arr->InsertLast(id_ref) ;
    _newly_created_arr->InsertLast(_incr_stmt) ;
    _newly_created_arr->InsertLast(assign_stmt) ;

    (void) _current_node->AddStatement(assign_stmt, _scope) ;
}

void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlWhileScheme, while_scheme)
{
    _cond_expr = while_scheme.GetCondition() ;
}

// Traverse the statements:
void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlLoopStatement, loop_stmt)
{
    VhdlScope *prev_scope = _scope ;
    _scope = loop_stmt.LocalScope() ;

    // VIPER #6879: Unrolling for loops:
    if (loop_stmt.GetIterScheme()->IsForScheme()) {
        VerificCFGStatementNode *statement = 0 ;

        VerificCFGRootNode *prev_loop_node = 0 ;
        VerificCFGScopeEndNode *scope_end_node = 0 ;
        statement = new VerificCFGStatementNode() ;
        if (_current_node) (void) statement->AddParent(_current_node) ;
        _current_node = statement ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        VERIFIC_ASSERT(statement == _current_node) ;

        scope_end_node = new VerificCFGScopeEndNode(statement) ;
        (void) statement->SetScopeEnd(scope_end_node) ;

        VhdlIdDef *label_id = _scope ? _scope->GetOwner() : 0 ;
        if (label_id) (void) _labeled_loop_map.Insert(label_id, scope_end_node) ;

        // Store the previous loop node and set the current
        // loop node to _loop_node:
        prev_loop_node = _loop_node ;
        _loop_node = statement ;

        // Traverse iter scheme
        TraverseNode(loop_stmt.GetIterScheme()) ;

        VhdlMapForCopy old_2_new(POINTER_HASH) ;

        verific_int64 range = (_left>_right) ? (_left-_right) : (_right-_left) ;
        unsigned i ;
        for(i=0; i<=range; i++) {
            // If the _current_node is a statement node, we do not create a new statement node
            // rather store the statement in the array of the current statement node:
            VerificCFGStatementNode *incr_stmt_node = new VerificCFGStatementNode() ;
            VhdlStatement *incr_stmt = _incr_stmt ? _incr_stmt->CopyStatement(old_2_new) : 0 ;
            (void) incr_stmt_node->AddStatement(incr_stmt, _scope) ;
            _newly_created_arr->InsertLast(incr_stmt) ;
            VhdlVariableAssignmentStatement *old_incr_stmt = _incr_stmt ;
            VhdlIdDef *name_id = _scope ? _scope->GetOwner() : 0 ;
            if (name_id) {
                (void) _incr_node_map.Insert(name_id, incr_stmt_node, 1) ;
            } else {
                (void) _incr_node_map.Insert((void*) (unsigned long)_incr_node_map.Size(), incr_stmt_node, 1) ;
            }
            VerificCFGStatementNode *old_incr_stmt_node = incr_stmt_node ;

            // Traverse the loop statement
            TraverseArray(loop_stmt.GetStatements()) ;

            // Restore the previous incremental statement and node:
            _incr_stmt = old_incr_stmt ;
            incr_stmt_node = old_incr_stmt_node ;

            VerificCFGRootNode *last_node = _current_node ;

            // If the _current_node is a statement node, we do not create a new statement node
            // rather store the statement in the array of the current statement node:
            _current_node = incr_stmt_node ;

            if (_current_node && last_node) {
                (void) last_node->AddChild(_current_node) ;
            }

            // If the _current_node is a statement node, we do not create a new statement node
            // rather store the statement in the array of the current statement node:
            _current_node = incr_stmt_node ;

            // Treate the next statements
            // Whenever get a next satetment node,
            // set the immediate loop node as its child
            MapIter mi ;
            VhdlIdDef *id ;
            VerificCFGRootNode *node ;
            VerificCFGRootNode *incr_node = 0 ;
            FOREACH_MAP_ITEM(&_next_stmt_nodes, mi, &id, &node) {
                if (!id) {
                    unsigned size = _incr_node_map.Size() ;
                    if (size) incr_node = (VerificCFGRootNode*) _incr_node_map.GetValueAt(size-1) ;
                } else {
                    if (_incr_node_map.GetItem(id)) {
                       incr_node = (VerificCFGRootNode*)_incr_node_map.GetValue(id) ;
                    } else {
                       incr_node = _current_node ;
                    }
                }

                if (node->IsConditionalNode()) {
                    (void) node->AddBranch(0, incr_node) ;
                    VerificCFGRootNode *scope_end = node->GetScopeEnd() ;
                    (void) node->AddBranch(0, scope_end) ;
                } else {
                    (void) node->AddChild(incr_node) ;
                }
            }

           // Reset the array after each loops
           _next_stmt_nodes.Reset() ;
        }

        (void) scope_end_node->AddParent(_current_node) ;
        _current_node = scope_end_node ;

        unsigned size = _incr_node_map.Size() ;
        if (size) {
            MapItem *item = _incr_node_map.GetItemAt(size-1) ;
            (void) _incr_node_map.Remove(item) ;
        }

        // Restore the previous loop node:
        _loop_node = prev_loop_node ;
        // Restore the previous scope:
        _scope = prev_scope ;
    } else {
        // Keep the old incremental statement
        VhdlVariableAssignmentStatement *old_incr_stmt = _incr_stmt ;

        // Traverse iter scheme
        TraverseNode(loop_stmt.GetIterScheme()) ;

        // If the _current_node is a statement node, we do not create a new statement node
        // rather store the statement in the array of the current statement node:
        VerificCFGStatementNode *incr_stmt_node = new VerificCFGStatementNode() ;
        (void) incr_stmt_node->AddStatement(_incr_stmt, _scope) ;
        _incr_stmt = 0 ;
        VhdlIdDef *name_id = _scope ? _scope->GetOwner() : 0 ;
        (void) _incr_node_map.Insert(name_id, incr_stmt_node) ;
        VerificCFGStatementNode *old_incr_stmt_node = incr_stmt_node ;

        // Create a loop node
        VerificCFGConditionalStatementNode *loop = new VerificCFGConditionalStatementNode() ;
        (void) loop->AddParent(_current_node) ;
        _current_node = loop ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        // Store the previous loop node and set the current
        // loop node to _loop_node:
        VerificCFGRootNode *prev_loop_node = _loop_node ;
        _loop_node = loop ;

        // Add conditions:
        loop->SetConditionExpression((void*)_cond_expr) ;

        VERIFIC_ASSERT(loop == _current_node) ;

        VerificCFGScopeEndNode *loop_end = new VerificCFGScopeEndNode(loop) ;
        (void) loop->SetScopeEnd(loop_end) ;

        VhdlIdDef *label_id = _scope ? _scope->GetOwner() : 0 ;
        if (label_id) (void) _labeled_loop_map.Insert(label_id, loop_end) ;

        // Keep the parent node:
        VerificCFGRootNode *orig_parent_node = _parent_node ;

        // For each branch of loop node create a fresh parse tree
        // keeping the _parent_node and _current_node in stack and make
        // each of them null before going to process each case item:
        // Keep the original node current and parent nodes:
        _current_node = 0 ;
        _parent_node = 0 ;

        // Traverse the loop statement
        TraverseArray(loop_stmt.GetStatements()) ;

        // Inside loop body:
        (void) loop->AddBranch(0, _parent_node) ;
        VerificCFGRootNode *last_node = _current_node ;

        _current_node = 0 ;
        _parent_node = 0 ;
        // If the _current_node is a statement node, we do not create a new statement node
        // rather store the statement in the array of the current statement node:
        _current_node = incr_stmt_node ;

        // Set the parent node if there is no parent node:
        _parent_node = _current_node ;

        if (_current_node && last_node) {
            (void) last_node->AddChild(_current_node) ;
            (void) loop->AddParent(_current_node) ;
        } else {
            (void) loop->AddParent(_current_node) ;
        }

        // Treate the next statements
        // Whenever get a next satetment node,
        // set the immediate loop node as its child
        MapIter mi ;
        VhdlIdDef *id ;
        VerificCFGRootNode *node ;
        FOREACH_MAP_ITEM(&_next_stmt_nodes, mi, &id, &node) {
            if (_incr_node_map.GetItem(id)) {
                VerificCFGRootNode *incr_node = (VerificCFGRootNode*)_incr_node_map.GetValue(id) ;
                if (node->IsConditionalNode()) {
                    (void) node->AddBranch(0, incr_node) ;
                    VerificCFGRootNode *scope_end = node->GetScopeEnd() ;
                    (void) node->AddBranch(0, scope_end) ;
                } else {
                    (void) node->AddChild(incr_node) ;
                }
            } else {
                (void) node->AddChild(_current_node) ;
            }
        }

        // Reset the array after each loops
        _next_stmt_nodes.Reset() ;

        // Out of loop body:
        (void) loop->AddBranch(0, loop_end) ;

        // Restore the previous incremental statement and node:
        _incr_stmt = old_incr_stmt ;
        incr_stmt_node = old_incr_stmt_node ;

        // Keep a link between the loop node and
        // the scope end node:
        _current_node = loop_end ;
        // Restore the original parent node:
        _parent_node = orig_parent_node ;

        // Restore the previous loop node:
        _loop_node = prev_loop_node ;
        _scope = prev_scope ;
    }
}

void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlExitStatement, exit)
{
    // Create a dummy statement/conditional node:
    if (!exit.GetCondition()) {
        VerificCFGRootNode *statement = new VerificCFGStatementNode() ;

        (void) statement->AddParent(_current_node) ;
        _current_node = statement ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        VhdlName *name = exit.GetTarget() ;
        VerificCFGRootNode *end_node = 0 ;
        if (name) {
            VhdlIdDef *name_id = name->GetId() ;
            end_node = (VerificCFGRootNode*)_labeled_loop_map.GetValue(name_id) ;
        } else {
            end_node = _loop_node ? _loop_node->GetScopeEnd() : 0 ;
        }
        (void) _current_node->AddChild(end_node) ;
        // jump to the loop end node
        _current_node = 0 ;
    } else {
        // Add conditions:
        VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
        (void) cond->AddParent(_current_node) ;
        _current_node = cond ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        VERIFIC_ASSERT(cond == _current_node) ;
        cond->SetConditionExpression((void*)exit.GetCondition()) ;

        VhdlName *name = exit.GetTarget() ;
        VerificCFGRootNode *end_node = 0 ;
        if (name) {
            VhdlIdDef *name_id = name->GetId() ;
            end_node = (VerificCFGRootNode*)_labeled_loop_map.GetValue(name_id) ;
        } else {
            end_node = _loop_node ? _loop_node->GetScopeEnd() : 0 ;
        }

        // scope end node:
        VerificCFGScopeEndNode *end = new VerificCFGScopeEndNode(cond) ;
        (void) cond->SetScopeEnd(end) ;
        (void) _current_node->AddBranch(0, end_node) ;
        (void) end->AddParent(cond) ;
        _current_node = end ;
    }
}

void VhdlFsmProcessVisitor::VHDL_VISIT(VhdlNextStatement, next)
{
    if (!next.GetCondition()) {
        // Create a dummy statement node:
        VerificCFGStatementNode *statement = new VerificCFGStatementNode() ;
        (void) statement->AddParent(_current_node) ;
        _current_node = statement ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        VhdlName *name = next.GetTarget() ;
        VhdlIdDef *id = name ? name->GetId() : 0 ;

        // store the statement nodes in an array
        (void) _next_stmt_nodes.Insert(id, _current_node, 1) ;
        _current_node = 0 ;
    } else {
        VerificCFGConditionalStatementNode *cond = new VerificCFGConditionalStatementNode() ;
        (void) cond->AddParent(_current_node) ;
        _current_node = cond ;

        // Set the parent node if there is no parent node:
        if (!_parent_node) _parent_node = _current_node ;

        VERIFIC_ASSERT(cond == _current_node) ;
        cond->SetConditionExpression((void*)next.GetCondition()) ;

        VhdlName *name = next.GetTarget() ;
        VhdlIdDef *id = name ? name->GetId() : 0 ;

        // scope end node:
        VerificCFGScopeEndNode *end = new VerificCFGScopeEndNode(cond) ;
        (void) cond->SetScopeEnd(end) ;
        (void) _next_stmt_nodes.Insert(id, _current_node, 1) ;
        _current_node = end ;
    }
}

// Count the total number of event controls in an process statement
void VhdlFsmProcessStatementVisitor::CountStateNode(VerificCFGRootNode *node, Array &state_count, Set *visited)
{
    if (!node) return ;

    // Check whether the node is already visited:
    // It may happen during loop/conditional node as we
    // come back to these nodes from their child branchs:
    if (visited && visited->GetItem(node)) return ;

    Set visited_set(POINTER_HASH) ;
    if (!visited) visited = &visited_set ;
    (void) visited->Insert(node) ;

    // Count the states: store the state nodes in an array
    if (node->IsStateNode()) state_count.Insert(node) ;

    SetIter si ;
    VerificCFGRootNode *child ;
    FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
        CountStateNode(child, state_count, visited) ;
    }
}

// Copy the statements of the statement node and call ExplicitStateMachine routine on its child and GenerateCaseState routine if the child is a state node:
void
VhdlFsmProcessStatementVisitor::ExplicitStateMachine(VerificCFGRootNode *node, const VerificCFGRootNode *start, Map &state_map, Array *expr_arr, Array *stmts, VhdlMapForCopy &old2new)
{
    if (!stmts || !node) return ;

    // If this node is visited more than once in the same event clock
    // then there should be an infinite loop:
    MapItem *item ;
    VerificCFGRootNode *exist ;
    FOREACH_SAME_KEY_MAPITEM(&state_map, node, item) {
        exist = (VerificCFGRootNode*)item->Value() ;
        if (exist==start) {
            _infinite_loop = 1 ;
            return ;
        }
    }
    (void) state_map.Insert(node, start, 0, 1) ;

    // For state node:
    if (node->IsStateNode()) {
        if (expr_arr) {
            char *string_lit = FindStringLiteralOfStateNum(node, _digit) ;
            VhdlStringLiteral *state = new VhdlStringLiteral(string_lit) ;
            expr_arr->InsertLast(state) ;
        }
    }

    // For statement node:
    if (node->IsSimpleStatementNode()) {
        MapIter mi ;
        VhdlStatement *stmt ;
        VhdlScope *stmt_scope ;
        FOREACH_MAP_ITEM(node->GetStatementList(), mi, &stmt, &stmt_scope) {
            if (stmt) {
                VhdlStatement *statement = stmt->CopyStatement(old2new) ;
                stmts->InsertLast(statement) ;
            }
        }
    }

    // For state/statement/scope_end nodes
    if (!node->IsConditionalNode()) {
        SetIter si ;
        VerificCFGRootNode *child ;
        FOREACH_SET_ITEM(node->GetChildren(), si, &child) {
            if (child->IsStateNode()) {
                GenerateCaseState(child, stmts) ;
            } else {
                ExplicitStateMachine(child, node, state_map, expr_arr, stmts, old2new) ;
            }
        }
    } else { // For conditional nodes
        VhdlExpression *cond_expr = (VhdlExpression *)node->GetConditionExpression() ;
        VhdlExpression *condition = cond_expr ? cond_expr->CopyExpression(old2new) : 0 ;

        unsigned has_else = 0 ;
        unsigned case_stmt = 0 ;

        unsigned i ;
        unsigned branch_cond_size = node->NumBranch() ;
        // For case statement the branch size is one or more
        // for loop/if-else statement the branch size is zero
        if (branch_cond_size) case_stmt = 1 ;
        Map new_state_map(POINTER_HASH) ;
        if (case_stmt) {
            Array *case_alternatives = new Array(branch_cond_size) ;
            for (i=0; i< branch_cond_size; i++) {
                new_state_map.Reset() ;
                MapIter mi ;
                VerificCFGRootNode *node1, *node2 ;
                FOREACH_MAP_ITEM(&state_map, mi, &node1, &node2) {
                    (void) new_state_map.Insert(node1, node2, 0, 1) ;
                }

                Array *cond_stmt_arr = 0 ;
                VerificCFGRootNode *statement = (VerificCFGRootNode *)node->GetBranchStatement(i) ;
                if(statement) {
                    cond_stmt_arr = new Array() ; // JJ: move allocation on demand to prevent leak
                    if (statement->IsStateNode()) {
                        GenerateCaseState(statement, cond_stmt_arr) ;
                    } else {
                        ExplicitStateMachine(statement, node, new_state_map, 0, cond_stmt_arr, old2new) ;
                    }
                }

                Array *cond_arr = node->GetBranchConditions(i) ;

                if (cond_arr) { // For case statement create seq_if_blck block
                    Array *case_conds = new Array(cond_arr->Size()) ;
                    unsigned ai ;
                    VhdlExpression *expr ;
                    FOREACH_ARRAY_ITEM(cond_arr, ai, expr) {
                        VhdlExpression *expression = expr ? expr->CopyExpression(old2new) : 0 ;
                        case_conds->InsertLast(expression) ;
                    }
                    if (!cond_stmt_arr) cond_stmt_arr = new Array() ; // JJ: move allocation on demand to prevent leak
                    VhdlCaseStatementAlternative *case_alternative = new VhdlCaseStatementAlternative(case_conds, cond_stmt_arr) ;
                    case_alternative->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                    case_alternatives->InsertLast(case_alternative) ;
                }
            }
            VhdlCaseStatement *case_statement = new VhdlCaseStatement(condition, case_alternatives) ;
            case_statement->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
            stmts->InsertLast(case_statement) ;
        } else {
            if (!node->NumBranch()) { // if-else statement
                // For if-else/loop:
                Array *cond_if_stmt_arr = new Array() ;
                Array *cond_else_stmt_arr = new Array() ;
                VerificCFGRootNode *statement = (VerificCFGRootNode *)node->GetBranchStatement(0) ; // if branch
                if (statement) {
                    new_state_map.Reset() ;
                    MapIter mi ;
                    VerificCFGRootNode *node1, *node2 ;
                    FOREACH_MAP_ITEM(&state_map, mi, &node1, &node2) {
                        (void) new_state_map.Insert(node1, node2, 0, 1) ;
                    }

                    if (statement->IsStateNode()) {
                        GenerateCaseState(statement, cond_if_stmt_arr) ;
                    } else {
                        ExplicitStateMachine(statement, node, new_state_map, 0, cond_if_stmt_arr, old2new) ;
                    }
                }

                statement = (VerificCFGRootNode *)node->GetBranchStatement(1) ; // else branch
                if (statement) {
                    has_else = 1 ;
                    new_state_map.Reset() ;
                    MapIter mi ;
                    VerificCFGRootNode *node1, *node2 ;
                    FOREACH_MAP_ITEM(&state_map, mi, &node1, &node2) {
                        (void) new_state_map.Insert(node1, node2, 0, 1) ;
                    }

                    if (statement->IsStateNode()) {
                        GenerateCaseState(statement, cond_else_stmt_arr) ;
                    } else {
                        ExplicitStateMachine(statement, node, new_state_map, 0, cond_else_stmt_arr, old2new) ;
                    }
                }

                VhdlIfStatement *cond_stmt = 0 ;
                if (!has_else) { // if statement without else part
                    // For if without else we generate the seq_else_blck with the scope end node of the conditional node
                    statement = node->GetScopeEnd() ;
                    if(statement) {
                        if (statement->IsStateNode()) {
                            GenerateCaseState(statement, cond_else_stmt_arr) ;
                        } else {
                            ExplicitStateMachine(statement, node, new_state_map, 0, cond_else_stmt_arr, old2new) ;
                        }
                    }
                }
                cond_stmt = new VhdlIfStatement(condition, cond_if_stmt_arr, 0, cond_else_stmt_arr) ;
                cond_stmt->SetLinefile(cond_expr ? cond_expr->Linefile() : 0) ;
                stmts->InsertLast(cond_stmt) ;
            }
        }
    }
}

// Whenver visit a state node create a variable assignment statement and store it in the stmts array:
void
VhdlFsmProcessStatementVisitor::GenerateCaseState(VerificCFGRootNode *node, Array *stmts)
{
    if (!stmts) return ;

    // Get the id-ref from the id-def:
    VERIFIC_ASSERT(_var_id) ;
    VhdlIdRef *l_val = new VhdlIdRef(_var_id) ;

    char *string_lit = FindStringLiteralOfStateNum(node, _digit) ;
    VhdlStringLiteral *val = new VhdlStringLiteral(string_lit) ;
    VhdlVariableAssignmentStatement *stmt = new VhdlVariableAssignmentStatement(l_val, val) ;

    stmts->InsertLast(stmt) ;
}

/*-----------------------------------------------------------------*/
//               Generate ExplicitStateMachine
/*-----------------------------------------------------------------*/

// VIPER #6633: Generate Explicit State Machine: For generating explicit state machine from implicit state machine
// we follow the following algorithm:
// Algorithm:
// 1) Traverse the parse tree with VhdlFsmProcessStatementVisitor.
// 2) For process statement:
//       - check for some limitations and conditions.
//       - if conditions are fulfilled do the following:
//             1. Generate control flow graph with a new visitor VhdlFsmProcessVisitor.
//                   1) For generating CFG we need to declare 4 classes- statement node for
//                      plain statements, state node for event statements, conditional node
//                      for if-else/case/loop statements and end node for scope end of conditional
//                      and loop statements.
//                   2) During CFG generation each time we visit a node we check for event control
//                      within the block, otherwise we store the whole block of statements in statement node.
//                   3) For loop/conditional node we have more than one child: process each branch
//                      and create new parse tree branch for each branch and store the parent pointer
//                      in the conditional/loop node.
//                   4) Set an end node at the end of each branch of conditional node and else branch
//                      of loop node.
//                   5) Set the last node as the parent of the very first node.
//             2. Count the total number of events.
//             3. For each state nodes generate case item and store them in an array of items.
//             4. Generate a reset block for first time starting the simulation.
//             5. Generate a conditional statement with the reset and set block(case statement).
//             6. Create a sensitivity list with the clocking event of the original parse tree.
//             7. Create a new process statement.
//             8. Delete the CFG.
// 3) If we can able to create a new process statement then replace the old one with it.
// 4) Return the new statement.

// Debug Routines:
// 1) Print the CFG.
// 2) Print the total number of event controls.

// Limitations:
// 1) We do not support multiple cloking events: this is yet to be supported.

// Conditions:
// 1) No need to convert to explicit state m/c with process statement without event control.
// 2) If there is only one cloking enent in a process statement, then no need to convert it
//    to the explicit state machine.
// 3) If a node in the CFG is visited more than once at a same cloking event then it will be
//    an infinite loop, we warn it here.

// Compile flags:
// 1) Print the CFG and state machine if 'EXPLICIT_STATE_MACHINE_DEBUG' compile flag is on.

unsigned vhdl_file::GenerateExplicitStateMachine(VhdlPrimaryUnit *unit, unsigned reset)
{
    if (!unit) return 0 ;

    // Iterate through the module items (such as various object declarations, assignments,
    // instantiations, constructs, and so forth).
    if (Message::ErrorCount()) return 0 ;
    VhdlFsmProcessStatementVisitor visitor ;
    visitor.SetResetVariable(reset) ;
    unit->Accept(visitor) ;
    return visitor.GetNumOfProcessStmtReplaced() ;
}

