/*
 *
 * [ File Version : 1.35 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlConfiguration.h" // Compile flags are defined within here

#include "Array.h"
#include "Strings.h"

#include "vhdl_file.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlValue_Elab.h"
#include "VhdlScope.h"
#include "VhdlCopy.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
/*  Elaboration                                           */
/**********************************************************/

void
VhdlBlockConfiguration::Elaborate()
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Create a index range if needed :
    delete _block_range ; // Remove what was there previously.
    _block_range = (_block_spec) ? _block_spec->BlockIndexSpec() : 0 ;

    // Elaborate the use clause (elaborate packages in there if needed)
    unsigned i ;
    VhdlUseClause *use_clause ;
    FOREACH_ARRAY_ITEM(_use_clause_list, i, use_clause) {
        use_clause->Elaborate(0) ;
    }

    // VIPER - 2012 Elaborate one level at a time
    // Now execute lower level block configuration items
    VhdlConfigurationItem *config_item ;
    FOREACH_ARRAY_ITEM(_configuration_item_list, i, config_item) {
        config_item->Elaborate() ;
    }
}

void
VhdlComponentConfiguration::Elaborate()
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Just execute the lower-level configuration item
    //if (_block_configuration) _block_configuration->Elaborate() ;
    // VIPER 2012 execute one level
}

unsigned
VhdlBlockConfiguration::MatchBlockConfig(VhdlIdDef *label, VhdlValue *iter_value, VhdlIdDef *alt_label)
{
    // VIPER #5918 : VHDL-2008 : If _block_label matches with alternative
    // label, this subblock config matches
    if (_block_label == alt_label) return 1 ;

    // If its not the same label, then this block config does not match.
    if (_block_label != label) return 0 ;

    // Check if there is a subblock range :
    if (_block_range) {
        if (!iter_value) {
            // LRM 1.3.1 (ln 408)
            Error("%s does not have a iteration scheme", _block_label->Name()) ;
            label->Error("%s is declared here", label->Name()) ;
            return 0 ; // No match
        }
        if (!_block_range->Contains(iter_value)) {
            return 0 ; // No match
        }
    }

    // This subblock configuration matches.
    return 1 ;
}

unsigned
VhdlComponentConfiguration::MatchComponentConfig(VhdlIdDef *label)
{
    if (!label) return 0 ;

    // Match the labels in this component config to the label requested.
    // Gotta go into the component spec for that
    if (_component_spec) {
        return _component_spec->MatchLabel(label) ;
    }
    return 0 ;
}

VhdlBlockConfiguration *
VhdlBlockConfiguration::ExtractSubBlock(VhdlIdDef *label, VhdlValue *iter_value, VhdlIdDef *alt_label) const
{
    if (!label) return 0 ;

    // Check if any sub-blockconfiguration matches this label and iter_value.
    // Error out if there is more than one match.

    Array *matching_items = 0 ;
    VhdlMapForCopy old2new ;
    VhdlBlockConfiguration *result = 0 ;
    unsigned i ;
    VhdlConfigurationItem *config_item ;
    unsigned match ;
    Array *config_item_list = 0 ;
    // VIPER #7172 : If this is Verific created wrapper block configuration,
    // need to call 'ExtractSubBlock' on its elements. So get those
    if (!_local_scope) {
        FOREACH_ARRAY_ITEM(_configuration_item_list, i, config_item) {
            Array *items = config_item->GetConfigurationItemList() ;
            if (!items) continue ;
            if (!config_item_list) config_item_list = new Array(items->Size()) ;
            config_item_list->Append(items) ;
        }
    } else {
        config_item_list = _configuration_item_list ? new Array(*_configuration_item_list): 0 ;
    }
    FOREACH_ARRAY_ITEM(config_item_list, i, config_item) {
        // Check if this subblock matches :
        match = config_item->MatchBlockConfig(label, iter_value, alt_label) ;
        if (!match) continue ;

        // set the result
        if (result) {
            // VIPER #7172 : Allow and accumulate multiple matches
            if (!matching_items) {
                matching_items = new Array(2) ;
                matching_items->InsertLast(result->CopyConfigurationItem(old2new)) ;
            }
            matching_items->InsertLast(config_item->CopyConfigurationItem(old2new)) ;
            //config_item->Error("multiple configuration items match label %s",label->Name()) ;
            //result->Error("another match is here") ;
        } else {
            // FIX ME : get rid of the cast..
            result = (VhdlBlockConfiguration*)config_item ;
        }
    }
    // VIPER #7172 : Create a new VhdlBlockConfiguration with copied multiple
    // matching items and return that
    if (matching_items) {
        result = new VhdlBlockConfiguration(0, 0, matching_items, 0) ;
    } else { // Original parse tree, return copied one
        result = result ? (VhdlBlockConfiguration*)result->CopyConfigurationItem(old2new): 0 ;
    }
    delete config_item_list ;
    return result ;
}

VhdlComponentConfiguration *
VhdlBlockConfiguration::ExtractComponentConfig(VhdlIdDef *label) const
{
    // Extract the component configuration that matches this label :
    VhdlComponentConfiguration *result = 0 ;
    unsigned i ;
    VhdlConfigurationItem *config_item ;
    unsigned match ;
    Array *config_item_list = 0 ;
    // VIPER #7172 : If _local_scope is null, it is elaboration created wrapper
    // block configuration, need to process its inner elements. So get those
    if (!_local_scope) {
        FOREACH_ARRAY_ITEM(_configuration_item_list, i, config_item) {
            Array *items = config_item->GetConfigurationItemList() ;
            if (!items) continue ;
            if (!config_item_list) config_item_list = new Array(items->Size()) ;
            config_item_list->Append(items) ;
        }
    } else {
        config_item_list = _configuration_item_list ? new Array(*_configuration_item_list): 0 ;
    }
    //FOREACH_ARRAY_ITEM(_configuration_item_list, i, config_item) {
    FOREACH_ARRAY_ITEM(config_item_list, i, config_item) {
        if (!config_item) continue ;

        // Check if this subblock matches :
        match = config_item->MatchComponentConfig(label) ;
        if (!match) continue ;

        // Set the result
        if (result) {
            if (match==2) {
                // There was an 'others' clause in the configuration item.
            } else {
                config_item->Error("multiple configuration items match label %s",label->Name()) ;
                result->Error("another match is here") ;
            }
        } else {
            // FIX ME : get rid of the cast..
            result = (VhdlComponentConfiguration*) config_item ;
        }
    }
    delete config_item_list ;
    return result ;
}

void VhdlBlockConfiguration::CheckLoopIndexRange(const VhdlIdDef *label, const VhdlConstraint *label_range) const
{
    if (!label || !label_range) return ;

    unsigned i ;
    VhdlConfigurationItem *config_item ;
    Array *config_item_list = 0 ;
    // VIPER #7172 : If _local_scope is null, it is elaboration created wrapper
    // block configuration, process its inner elements
    if (!_local_scope) {
        FOREACH_ARRAY_ITEM(_configuration_item_list, i, config_item) {
            Array *items = config_item->GetConfigurationItemList() ;
            if (!items) continue ;
            if (!config_item_list) config_item_list = new Array(items->Size()) ;
            config_item_list->Append(items) ;
        }
    } else {
        config_item_list = _configuration_item_list ? new Array(*_configuration_item_list): 0 ;
    }
    //FOREACH_ARRAY_ITEM(_configuration_item_list, i, config_item) {
    FOREACH_ARRAY_ITEM(config_item_list, i, config_item) {
        // Check if this subblock matches :
        if (!config_item || (config_item->BlockLabel() != label)) continue ;
        VhdlConstraint *block_range = config_item->GetBlockRange() ;
        VhdlValue *left = (block_range) ? block_range->Left() : 0 ;
        VhdlValue *right = (block_range) ? block_range->Right() : 0 ;
        if (left && !label_range->Contains(left)) {
            char *val_image = left->Image() ;
            char *constraint_image = label_range->Image() ;
            config_item->Error("index value %s is out of range %s", val_image, constraint_image) ;
            Strings::free(val_image) ;
            Strings::free(constraint_image) ;
        }
        if (right && !label_range->Contains(right)) {
            char *val_image = right->Image() ;
            char *constraint_image = label_range->Image() ;
            config_item->Error("index value %s is out of range %s", val_image, constraint_image) ;
            Strings::free(val_image) ;
            Strings::free(constraint_image) ;
        }
    }
    delete config_item_list ;
}

