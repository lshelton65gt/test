/*
 *
 * [ File Version : 1.154 - 2014/02/20 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlExpression.h" // Compile flags are defined within here

#include "Set.h"
#include "Array.h"
#include "Strings.h"

#include "vhdl_tokens.h"
#include "vhdl_file.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"
#include "VhdlUnits.h"

#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
//  Elaboration Routines
/**********************************************************/

/**********************************************************/
//  Evaluate an constraint from a Name or subtype indication
/**********************************************************/

// Virtual catcher: call the plain 'EvaluateConstraintInternal' for non-implemented types:
// (This is currently defined only on VhdlRange and VhdlAttributeName classes)
VhdlConstraint *VhdlDiscreteRange::EvaluateConstraintPhysical(VhdlDataFlow *df)  { return EvaluateConstraintInternal(df, 0) ; }

VhdlConstraint *VhdlExpression::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    // This could still be allowed ;
    // Happens if a case-expression is a complex scalar expression.
    // So we cannot error out here.
    //
    // Error("use of an expression is not allowed here") ;
    return 0 ;
}

// Viper 7645: Added evaluate constraint on VhdlOperator. This is needed to
// obtain constraint from Vhdl actual to constrain the formal. This is needed
// because now vhd expressions are allowed as port actual. And unconstrained
// formals get the constraint from these actuals.
VhdlConstraint *VhdlOperator::EvaluateConstraintInternal(VhdlDataFlow * df, VhdlConstraint * /*target_constraint*/)
{
    if (!_operator) return 0 ;

    // Carbon specific begin : VIPER #8023 : Evaluate constraint of '&' operator
    if (_oper == VHDL_AMPERSAND) {
        VhdlConstraint *left_constraint = _left ? _left->EvaluateConstraint(df, 0): 0 ;
        VhdlConstraint *right_constraint = _right ? _right->EvaluateConstraint(df, 0): 0 ;
        if (!left_constraint || !right_constraint || left_constraint->IsUnconstrained() || right_constraint->IsUnconstrained()) {
            delete left_constraint ; delete right_constraint ;
            return 0 ;
        }
        VhdlIdDef *operator_type = _operator->Type() ;
        VhdlConstraint *result = _operator->Constraint() ;
        if (!result) result = operator_type->Constraint() ;
        result = result ? result->Copy(): 0 ;
        if (!result || !result->IsUnconstrained()) return result ;
        unsigned left_is_elem = (_operator->ArgType(0) == _operator->Type()) ? 0 : 1 ;
        unsigned right_is_elem = (_operator->ArgType(1) == _operator->Type()) ? 0 : 1 ;
        unsigned size = 0 ;
        if (left_is_elem && right_is_elem) { // Operands are element type
            size = 2 ; // Number of elements 2
        } else if (left_is_elem) { // Left element type and right array
            VhdlConstraint * right_index_constraint = right_constraint->IndexConstraint() ;
            size = 1 + (right_index_constraint ? right_index_constraint->Length(): 0) ;
        } else if (right_is_elem) { // Left array and right element type
            VhdlConstraint * left_index_constraint = left_constraint->IndexConstraint() ;
            size = 1 + (left_index_constraint ? left_index_constraint->Length(): 0) ;
        } else { // Both operands are array
            VhdlConstraint * right_index_constraint = right_constraint->IndexConstraint() ;
            VhdlConstraint * left_index_constraint = left_constraint->IndexConstraint() ;
            size = (left_index_constraint ? left_index_constraint->Length(): 0) + (right_index_constraint ? right_index_constraint->Length(): 0) ;
        }
        delete left_constraint ; delete right_constraint ;
        // Create array constraint taking element constraint from result and
        // left index of return array will be left index of operator's constraint
        // right index of return array will be adjusted according to element counts
        // of both operands
        VhdlConstraint *result_element_constraint = result->ElementConstraint() ;
        VhdlConstraint *result_index_constraint = result->IndexConstraint() ;
        VhdlValue *result_left_index = result_index_constraint ? result_index_constraint->Left(): 0 ;
        VhdlConstraint *element_constraint = result_element_constraint ? result_element_constraint->Copy(): 0 ;
        // Left index of return array
        VhdlValue *left_val = result_left_index ? result_left_index->Copy(): 0 ;
        // Right index of return array
        VhdlValue *right_val = left_val ? left_val->Copy(): 0 ;
        unsigned i ;
        for (i=1; i < size; i++) if (right_val) (void) right_val->UnaryOper((result->Dir() == VHDL_to) ? VHDL_INCR: VHDL_DECR, 0) ;
        VhdlRangeConstraint *range_constraint = new VhdlRangeConstraint(left_val, result->Dir(), right_val) ;
        // Create array constraint
        VhdlConstraint *array_constraint = new VhdlArrayConstraint(range_constraint, element_constraint) ;
        // Adjust the target constraint
        result->ConstraintBy(array_constraint) ;
        delete array_constraint ; // Deletes the rest below it
        return result ;
    }
    // Carbon specific end :

    if (!_operator->GetPragmaFunction()) return 0 ;

    VhdlIdDef *operator_type = _operator->Type() ;
    if (!operator_type || !operator_type->IsArrayType()) return 0 ;

    VhdlConstraint *result = _operator->Constraint() ;
    if (!result) result = operator_type->Constraint() ;

    // Copy the result
    result = result ? result->Copy() : 0 ;

    if (!result || !result->IsUnconstrained()) return result ;

    int lsize = 0 ;
    int rsize = 0 ;
    int size = 0 ;

    if (_left) {
        VhdlConstraint *arg_constraint = _left->EvaluateConstraint(df, 0) ;
        if (!arg_constraint || arg_constraint->IsUnconstrained()) {
            delete arg_constraint ; // Memory leak fix
            delete result ;
            return 0 ;
        }
        VhdlConstraint *index = arg_constraint->IndexConstraint() ;
        lsize = index ? (int)index->Length() : 0 ;
        delete arg_constraint ;
    }

    if (_right) {
        VhdlConstraint *arg_constraint = _right->EvaluateConstraint(df, 0) ;
        if (!arg_constraint || arg_constraint->IsUnconstrained()) {
            delete arg_constraint ; // Memory leak fix
            delete result ;
            return 0 ;
        }
        VhdlConstraint *index = arg_constraint->IndexConstraint() ;
        rsize = index ? (int)index->Length() : 0 ;
        delete arg_constraint ;
    }

    switch (_operator->GetPragmaFunction()) {
    // arithmetic operators. Unary and binary
    case VHDL_UMINUS :
    case VHDL_abs :
    // Shifters/Rotators :
    // If they return an array, then they must have a array 'left' value. Size should always match left size :
    case VHDL_sll :
    case VHDL_srl :
    case VHDL_sla :
    case VHDL_sra :
    case VHDL_rol :
    case VHDL_ror :
    // Reduction operations . This returns Boolean should be eliminated by ArrayType check
    // Comparators : . This returns Boolean should be eliminated by ArrayType check
    // not/buf
    case VHDL_not :
    case VHDL_buf :
        size = lsize ;
        break ;
    // addition subtraction
    case VHDL_MINUS :
    case VHDL_PLUS :
    // bit-wize wide boolean operators :
    case VHDL_and :
    case VHDL_nand :
    case VHDL_or :
    case VHDL_nor :
    case VHDL_xor :
    case VHDL_xnor :
    // There are no standard package examples for ** pragma, so assume standard array size result :
    case VHDL_EXPONENT :
        size = MAX(lsize,rsize) ;
        break ;
    case VHDL_STAR :
        // If both size of non-zero, then take sum, else take two times the non-zero size. (VIPER #2262)
        size = (!lsize) ? 2*rsize : (!rsize) ? 2*lsize : lsize + rsize ;
        break ;
    case VHDL_SLASH :
        // size always follows lsize if it's non-zero, else rsize is followed (VIPER #2262)
        size = (lsize) ? lsize : rsize ;
        break ;
    case VHDL_condition :
        // Pragma functions as per 1076-2008 LRM section 9.2.9
        if (lsize != 1) {
            size = 0 ;
        }
        size = 1 ;
        break ;
    case VHDL_mod :
    case VHDL_rem :
        // for mod: size always follows rsize if it's non-zero, else lsize is followed (VIPER #2262)
        // for rem: size always follows rsize if it's non-zero, else lsize is followed (VIPER #2262)
        size = (rsize) ? rsize : lsize ;
        break ;
    case VHDL_FEEDTHROUGH :
        {
        // The standard 'feed-through' (from anything to anything) :
        size = lsize ;
        VhdlValue *right_val = (_right) ? _right->Evaluate(0,df,0) : 0 ;
        if (right_val) {
            // in most feed-through functions, the second argument means a
            // size adjustment if the return type is an array type.
            // But sometimes, (like in to_integer() in mgc_arit) it means some
            // sort of sign-control value, which I don't quite get.
            // So, check if we BOTH return an array type AND the 'right' argument is integer.
            // in that case, we assume that it is a return-size argument.

            VhdlIdDef *type = _operator->ArgType(1) ;
            if (type && type->IsIntegerType()) {
                // Array size adjustment requested
                // right_val should be a constant, indicating the size
                if (right_val->IsConstant()) {
                    size = (int)right_val->Integer() ;
                }
            }
        }
        delete right_val ;
        }
        break ;
    default :
        size = 0 ;
        break ;
    }

    // Adjust unconstrained target to size-1 downto 0 or 1 to size.
    if (size) {
        VhdlConstraint *index_range = 0 ;
        // Create constraint
        switch (_oper) { // Viper #3915
        case VHDL_and :
        case VHDL_nand :
        case VHDL_or :
        case VHDL_nor :
        case VHDL_xor :
        case VHDL_xnor :
        case VHDL_not :
        {
            index_range = new VhdlRangeConstraint(new VhdlInt((verific_int64)1), VHDL_to, new VhdlInt((verific_int64)(size))) ;
            break ;
        }
        default:
            index_range = new VhdlRangeConstraint(new VhdlInt((verific_int64)(size-1)), VHDL_downto, new VhdlInt((verific_int64)0)) ;
            break ;
        }
        VhdlConstraint *elem_constraint = result->ElementConstraint()->Copy() ;
        VhdlConstraint *array_constraint = new VhdlArrayConstraint(index_range, elem_constraint) ;
        // Adjust the target constraint
        result->ConstraintBy(array_constraint) ;
        delete array_constraint ; // Deletes the rest below it
    }
    return result ;
}

VhdlConstraint *VhdlWaveformElement::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint)
{
    if (!_value) return 0 ;
    return _value->EvaluateConstraintInternal(df, target_constraint) ;
}

VhdlConstraint *VhdlExplicitSubtypeIndication::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    if (!_range_type) return 0 ;

    // Check the resolution function :
    // We  only support WIRE resolution functions :
    if (_res_function) {
        VhdlIdDef *func = _res_function->FindSingleObject() ;
        if (func) {
            // Issue 1174/1243 : Need to warn how we treat resolution functions..
            if (!func->GetPragmaFunction() && IsRtlElab()) {
                Warning("resolution function %s without synthesis directive will use three-state wiring",func->Name()) ;
                func->Info("%s is declared here",func->Name()) ;
            }
            // NOTE: could warn if the pragma function is not appropriate for a resolution function.
            // We accept REDAND/REDOR/WIRED_THREE_STATE..
        }
    }

    // Constraint of the type mark
    VhdlConstraint *type_constraint = _range_type->Constraint() ;
    if (!type_constraint) return 0 ;

    // VIPER #2864 : If range type is of array type, range constraints cannot be applied on it.
    if (type_constraint->IsArrayConstraint() && _range_constraint) {
        Error("range constraints cannot be applied to array types") ;
        return 0 ;
    }
    // Evaluate the range constraint itself
    VhdlConstraint *range = 0 ;

    if (!_range_constraint) {
        // Take the basic constraints from the type mark
        range = type_constraint->Copy() ;
    } else if (_range_constraint->IsUnconstrained(0)) {
        // box <> in range constraint creates an unconstrained range
        // for this index.
        // Take the basic constraints from the type mark
        range = type_constraint->Copy() ;

        // But label it as unconstrained
        range->SetUnconstrained() ;
    } else {
        // Take it from the range constraint
        range = _range_constraint->EvaluateConstraintInternal(df, 0) ;
        if (!range) return 0 ;

        // LRM 3.1 : each bound of the range must be in the type_mark's
        // range, or the range can be a null-range
        if (range->IsNullRange()) {
            // Null range warning is already given
        } else if (!type_constraint->Contains(range->Left()) ||
                   !type_constraint->Contains(range->Right())) {
            // FIX ME : better message
            Error("range extends beyond constraint of type %s",_range_type->Name()) ;
        }
    }
    return range ;
}

VhdlConstraint *VhdlRange::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    unsigned allow_nonconstant = 0 ; // can be an argument later on
    // Evaluate left and right side. If either side fails, return 0.
    // Check for constant-ness only if the value exists.
    // If it does not exist then an error would have been issued already before.
    VhdlValue *left = 0 ;
    if (!left && _left) left = _left->Evaluate(0,df,0) ;
    if (!left) {
        // VIPER 3990 : Mark data flow non-constant, if bound evaluation returns 0
        if (IsStaticElab() && df) df->SetNonconstExpr() ;
        return 0 ;
    }
    if (!allow_nonconstant && !left->IsConstant()) {
        _left->Error("left range bound is not constant") ;
        delete left ; return 0 ;
    }

    VhdlValue *right = 0 ;
    if (!right && _right) right = _right->Evaluate(0,df,0) ;
    if (!right) {
        // VIPER 3990 : Mark data flow non-constant, if bound evaluation returns 0
        if (IsStaticElab() && df) df->SetNonconstExpr() ;
        delete left ;
        return 0 ;
    }
    if (!allow_nonconstant && !right->IsConstant()) {
        _right->Error("right range bound is not constant") ;
        delete left ; delete right ; return 0 ;
    }
    VERIFIC_ASSERT(left && right) ;

    // In most cases, both bounds will be constant values (not VhdlNonconstBit/VhdlNonconst).
    // But this is not guaranteed.
    // Here : Translate to constant bounds to get rid of constant Net dependency.
    // Needed because ranges should be constant and could be applied
    // in a different RTL netlist (with different constant nets).

    // In translating to constant, the type of the constant is very important
    // to distinguish between integer and enumeration values.
    // So do an efford to find the type from the constant bound, so that one value can be a nonconstval.
    // Without a enum type, we assume the range (if expressed with nonconstval) is an integer range.
    //
    // NOTE : At the time of writing we have a transient problem where sometimes an integer
    VhdlIdDef *enum_type = left->GetType() ;
    if (!enum_type) enum_type = right->GetType() ;
    // Now convert to const. If enum_type is 0 (both sides nonconst or both sides integer),
    // then we will translate nonconsts to an integer range.
    if (!allow_nonconstant || left->IsConstant()) left =  left->ToConst(enum_type) ;
    if (!allow_nonconstant || right->IsConstant()) right = right->ToConst(enum_type) ;

    // Always create a full (left and right valid) range
    VhdlConstraint *result = new VhdlRangeConstraint(left,_dir,right) ;

    // Test for NULL range
    if (result->IsNullRange()) {
            // VIPER #8424 : Do not produce warning if null range is from ieee package
            unsigned range_of_ieee = 0 ;
            if (left && left->IsConstant() && (left->Integer()==0) && right && right->IsConstant() && (right->Integer()==1)) {
                VhdlIdDef *container_unit = _present_scope ? _present_scope->GetContainingPrimaryUnit(): 0 ;
                VhdlPrimaryUnit *prim_unit = container_unit ? container_unit->GetPrimaryUnit(): 0 ;
                VhdlLibrary *lib = prim_unit ? prim_unit->GetOwningLib(): 0 ;
                if (lib && Strings::compare(lib->Name(), "ieee")) range_of_ieee = 1 ;
            }
            if (!range_of_ieee) Warning("range is empty (null range)") ;
    }
    return result ;
}

VhdlConstraint *VhdlRange::EvaluateConstraintPhysical(VhdlDataFlow *df)
{
    unsigned allow_nonconstant = 0 ; // can be an argument later on
    // Evaluate left and right side. If either side fails, return 0.
    // Check for constant-ness only if the value exists.
    // If it does not exist then an error would have been issued already before.
    VhdlValue *left = (_left) ? _left->Evaluate(0,df,0) : 0 ;
    if (!left) return 0 ;
    if (!allow_nonconstant && !left->IsConstant()) {
        _left->Error("left range bound is not constant") ;
        delete left ; return 0 ;
    }

    VhdlValue *right = (_right) ? _right->Evaluate(0,df,0) : 0 ;
    if (!right) { delete left ; return 0 ; }
    if (!allow_nonconstant && !right->IsConstant()) {
        _right->Error("right range bound is not constant") ;
        delete left ; delete right ; return 0 ;
    }
    VERIFIC_ASSERT(left && right) ;

    // In most cases, both bounds will be constant values (not VhdlNonconstBit/VhdlNonconst).
    // But this is not guaranteed.
    // Here : Translate to constant bounds to get rid of constant Net dependency.
    // Needed because ranges should be constant and could be applied
    // in a different RTL netlist (with different constant nets).

    // In translating to constant, the type of the constant is very important
    // to distinguish between integer and enumeration values.
    // So do an efford to find the type from the constant bound, so that one value can be a nonconstval.
    // Without a enum type, we assume the range (if expressed with nonconstval) is an integer range.
    //
    // NOTE : At the time of writing we have a transient problem where sometimes an integer
    VhdlIdDef *enum_type = left->GetType() ;
    if (!enum_type) enum_type = right->GetType() ;
    // Now convert to const. If enum_type is 0 (both sides nonconst or both sides integer),
    // then we will translate nonconsts to an integer range.
    if (!allow_nonconstant || left->IsConstant()) left =  left->ToConst(enum_type) ;
    if (!allow_nonconstant || right->IsConstant()) right = right->ToConst(enum_type) ;

    // Always create a full (left and right valid) range of physical values:
    VhdlValue *physical_left = new VhdlPhysicalValue(left->Integer()) ;
    VhdlValue *physical_right = new VhdlPhysicalValue(right->Integer()) ;
    delete left ; delete right ;
    VhdlConstraint *result = new VhdlRangeConstraint(physical_left, _dir, physical_right) ;

    // Test for NULL range
    if (result->IsNullRange()) {
        Warning("range is empty (null range)") ;
    }
    return result ;
}

VhdlConstraint *VhdlBox::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    // We detect valid places for this with IsUnconstrained.
    // So this is an illegal place for it.
    if (IsRtlElab()) Error("illegal use of <> in subtype indication") ;
    return 0 ;
}
VhdlConstraint *VhdlDefault::EvaluateConstraintInternal(VhdlDataFlow * /*df*/, VhdlConstraint * /*target_constraint*/)
{
    return 0 ;
}

VhdlConstraint *VhdlAggregate::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint)
{
    // VIPER #3077 : If aggregate contains 'others', do not return constraint of
    // its type, return 0
    VhdlDiscreteRange *assoc = (_element_assoc_list && _element_assoc_list->Size()) ? (VhdlDiscreteRange*)_element_assoc_list->GetLast() : 0 ;
    if (assoc && assoc->IsOthers()) return 0 ;

    // Return the constraint of the type of the aggregate.
    // Thats not really accurate if the type is unconstrained (should constraint the
    // subtype based on the position settings in that case), but unconstrained
    // subtype will be constrained once the aggregate gets evaluated..
    //return (_aggregate_type && _aggregate_type->Constraint()) ? _aggregate_type->Constraint()->Copy() : 0 ;
    VhdlConstraint *constraint = target_constraint ? target_constraint : (_aggregate_type ? _aggregate_type->Constraint() : 0) ;
    if (!constraint) return 0 ;

    // VIPER #6448 : Try to calculate the constraint of aggregate using target constraint

    // For aggregates without 'others' clause, but with named elements,
    // we need to calculate the aggregate constraint by calculating the named indexes,
    // and choosing the highest/lowest from them.
    // For all other aggregates, we use the 'target_constraint' :
    // Either the real (context) target constraint, or the constraint of this aggregate type.
    if (constraint->IsArrayConstraint()) {
        // Array aggregates :

        // Now, if there are named associations, there must be one in the last element.
        // This is because positional associations cannot follow named associations.
        // Also, if there is an others clause, it must also be the last one.
        // So check the last association :
        VhdlConstraint *aggregate_constraint = 0 ;
        if (assoc && assoc->IsAssoc()) { // named association
            // There is a named association in the last position, and it is not an others clause.
            // So, we need to calculate the aggregate index constraint from the named assocs.

            // Start with a copy of the target constraint,
            // and make that 'unconstrained', (so that calls to 'EvaluateConstraintByChoice' can adjust the index range)
            // This way, we maintain the 'direction' (and element constraint) of the target constraint,
            // while still adjusting it to the lowest/highest choices in the aggregate :
            aggregate_constraint = constraint->Copy() ;
            aggregate_constraint->SetUnconstrained() ;
            unsigned i ;
            unsigned check_dir = 0 ;
            FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
                if (!assoc) continue ;
                aggregate_constraint = assoc->EvaluateConstraintByChoice(aggregate_constraint, df, IsAggregateTypeAssoc(i), check_dir) ;
                check_dir |= IsAggregateTypeAssoc(i) ;
            }
            return aggregate_constraint ;
        } else if (assoc) { // positional association
            VhdlConstraint *element_constraint = constraint->ElementConstraint() ;
            VhdlConstraint *assoc_constraint = 0 ;

            unsigned i ;
            unsigned element_count = 0 ;
            FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
                if (!assoc) continue ;
                if (IsAggregateTypeAssoc(i)) {
                    VhdlConstraint *assoc_constraint_i = assoc->EvaluateConstraintInternal(df, element_constraint) ;
                    if (assoc_constraint_i) { // Viper #7596
                        element_count += (unsigned) assoc_constraint_i->Length() ;
                        if (!assoc_constraint) {
                            VhdlConstraint *element_constraint_i = assoc_constraint_i->ElementConstraint() ;
                            assoc_constraint = element_constraint_i ? element_constraint_i->Copy(): 0 ;
                        }
                    }
                    delete assoc_constraint_i ;
                } else {
                    element_count += 1 ;
                    if (!assoc_constraint) assoc_constraint = assoc->EvaluateConstraintInternal(df, element_constraint) ;
                }
            }

            if (!assoc_constraint) {
                assoc_constraint = element_constraint ? element_constraint->Copy(): 0 ;
            }

            // The leftmost bound of aggregate constraint will be S'LEFT where S is the
            // index subtype of the base type of the array.
            VhdlValue *left = constraint->Left() ;
            if (left) left = left->Copy() ;
            // The rightmost bound is determined by the direction of the index subtype and
            // the number of elements.
            VhdlValue *right = left ? left->Copy(): 0 ;
            unsigned dir = constraint->Dir() ;
            if (right) {
                for (i=1; i < element_count; i++) {
                    (void) right->UnaryOper((dir == VHDL_to) ? VHDL_INCR : VHDL_DECR, 0) ;
                }
            }
            return new VhdlArrayConstraint(new VhdlRangeConstraint(left, dir, right), assoc_constraint) ;
        }
    }
    return constraint->Copy() ;
}

VhdlConstraint *VhdlAllocator::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint *target_constraint)
{
    VhdlConstraint *subtype_constraint = _subtype ? _subtype->EvaluateConstraintInternal(df, target_constraint) : 0 ;
    return subtype_constraint ? new VhdlAccessConstraint(subtype_constraint) : 0 ;
}

VhdlConstraint *VhdlQualifiedExpression::EvaluateConstraintInternal(VhdlDataFlow *df, VhdlConstraint * /*target_constraint*/)
{
    // Constraint of a qualified expression is its type-mark's subtype :
    // (See LRM on qualified expressions, and even clearer on case-expressions.
    // So, return constraint of the prefix (a type mark) :
    return _prefix ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
    // return prefix_constraint ? new VhdlAccessConstraint(prefix_constraint) : 0 ;
}

// **************************************************
// EvaluateIntegerRangeConstraint(VhdlDataFlow *df)
// Returns the constraint of an integer expression
// **************************************************
VhdlConstraint *VhdlOperator::EvaluateIntegerRangeConstraint(VhdlDataFlow *df)
{
    // Viper #6473: evaluate the integer range for this expression
    // works only when all the sub-expressions are of integer type
    if (!vhdl_file::GetPruneLogicForIntegerConstraint() || !_operator) return 0 ;

    switch(_operator->GetOperatorType()) {
    case VHDL_PLUS:
    case VHDL_MINUS:
        break ;
    default:
        return 0 ;
    }

    VhdlConstraint *left_constraint = _left ? _left->EvaluateIntegerRangeConstraint(df) : 0 ;
    if (!left_constraint) return 0 ;

    VhdlConstraint *right_constraint = _right ? _right->EvaluateIntegerRangeConstraint(df) : 0 ;
    if (_right && !right_constraint) {
        delete left_constraint ;
        return 0 ;
    }

    // apply the operator oper_type on the two constraints
    // e.g. (0 to 5) plus (3 downto -2) should generate (-2 to 8)
    return left_constraint->MergeConstraints(right_constraint, _operator->GetOperatorType()) ;
}

/**********************************************************/
//  Evaluate an expression which appears inside a condition
/**********************************************************/
#if 1
VhdlValue *VhdlDiscreteRange::EvaluateCondition(VhdlDataFlow *df, Map *& /* condition_id_value_map */)
{
    return Evaluate(0, df, 0) ;
}

VhdlValue *VhdlOperator::EvaluateCondition(VhdlDataFlow *df, Map *&condition_id_value_map)
{
    unsigned oper_type = _operator ? _operator->GetOperatorType() : 0 ;
    VhdlIdDef *return_type = _operator ? _operator->Type() : 0 ;
    unsigned is_boolean = return_type ? return_type->IsEnumerationType() : 0 ;
    if (IsStaticElab() || !is_boolean || !_left || !_right || (oper_type != VHDL_and && oper_type != VHDL_EQUAL)) return Evaluate(0, df, 0) ;

    VhdlValue *result = 0 ;

    // Following is needed for the clock enable extraction
    // Such splitting only happens with the first call with
    // top level condition
    if (oper_type == VHDL_and) {
        Array anded_expressions_array ;
        SplitAndedSubExpressions(anded_expressions_array) ;

        Array anded_value_array ;
        VhdlValue *event_val = 0 ;
        unsigned return_null = 0 ;

        unsigned idx ;
        VhdlExpression *expr ;
        FOREACH_ARRAY_ITEM(&anded_expressions_array, idx, expr) {
            if (!expr) continue ;

            VhdlValue *expr_val = expr->EvaluateCondition(df, condition_id_value_map) ;

            if (!expr_val || expr_val->NumBits() != 1) {
                // operator with multi-bit and in boolean context ? abort
                delete expr_val ; // Memory leak fix
                return_null = 1 ;
                break ;
            }

            if (expr_val->IsEvent() && !event_val) {
                // Two events in same condition ??
                event_val = expr_val ;
                continue ;
            }

            anded_value_array.Insert(expr_val) ;
        }

        if (!return_null) {
            VhdlValue *value ;
            result = event_val ;
            FOREACH_ARRAY_ITEM(&anded_value_array, idx, value) {
                if (!value) continue ;
                result = result ? result->BinaryOper(value, VHDL_and, this) : value ;
            }
        } else {
            // cleanup
            VhdlValue *value ;
            FOREACH_ARRAY_ITEM(&anded_value_array, idx, value) {
                delete value ;
            }
            delete event_val ; // Memory leak fix
        }
    } else { // oper_type == VHDL_EQUAL
        // Viper #5748, 5763: Not boolean context. Call Evaluate
        VhdlValue *left_value = _left->Evaluate(0, df, 0) ;
        if (!left_value) return 0 ;

        // Viper #5748, 5763: Not boolean context. Call Evaluate
        VhdlValue *right_value = _right->Evaluate(0, df, 0) ;
        if (!right_value) {
            delete left_value ;
            return 0 ;
        }

        VhdlIdDef *left_id = _left->FindFullFormal() ;
        VhdlIdDef *right_id = _right->FindFullFormal() ;
        if (right_value->IsConstant() && left_id) {
            VhdlConstraint *constraint = left_id->Constraint() ;
            if (constraint && constraint->Contains(right_value)) { // Viper #6732
                if (!condition_id_value_map) condition_id_value_map = new Map(POINTER_HASH) ;
                (void) condition_id_value_map->Insert(left_id, right_value->Copy(), 0, 1) ;
            }
        } else if (left_value->IsConstant() && right_id) {
            VhdlConstraint *constraint = right_id->Constraint() ;
            if (constraint && constraint->Contains(left_value)) { // Viper #6732
                if (!condition_id_value_map) condition_id_value_map = new Map(POINTER_HASH) ;
                (void) condition_id_value_map->Insert(right_id, left_value->Copy(), 0, 1) ;
            }
        }

        VhdlValue *const_val = right_value->IsConstant() ? right_value : left_value ;
        VhdlExpression *const_expr = (const_val == left_value) ? _left : _right ;

        unsigned is_metalogical = const_val && const_val->IsMetalogical() ;
        if (!const_expr->IsConstant()) is_metalogical = 0 ;
        if (right_value->IsConstant() && left_value->IsConstant()) is_metalogical = 0 ;

        if (vhdl_file::IsVhdl2008() && is_metalogical && return_type) {
            // Viper #7579
            // Vhdl 2008 LRM 16.8.2.4.4 and 16.8.2.4.10
            // Comparison of non-constant with metalogical values to be considered FALSE
            result = new VhdlEnum(return_type->GetEnumAt(0)) ;
            delete left_value ;
            delete right_value ;
        } else {
            result = left_value->BinaryOper(right_value, oper_type, this) ;
        }
    }

    return result ;
}
#endif

/**********************************************************/
//  Evaluate an expression
/**********************************************************/

VhdlValue *VhdlExplicitSubtypeIndication::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*get_driver_value*/)
{
    return 0 ; // Actually illegal in an expression : tested in type-inference
}

VhdlValue *VhdlRange::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*get_driver_value*/)
{
    return 0 ; // Actually illegal in an expression : tested in type-inference
}

VhdlValue *VhdlBox::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*get_driver_value*/)
{
    return 0 ; // Actually illegal in an expression : tested in type-inference
}
VhdlValue *VhdlDefault::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*get_driver_value*/)
{
    return 0 ;
}

VhdlValue *VhdlAssocElement::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // the _actual_part's name is created using ImageVhdl which do not looks at the
    // constraint size. We need to change the name here based on target constraint so that
    // a later error in checkagainst can be avoided. This all started happening because
    // now we are matching integer with vector in generic type matching after vipers 5909 and
    // 5889. EvaluateForVerilog alters the _name and then calls Evaluate.
    VhdlExpression *actual_part = (_actual_part ? (_actual_part->IsInertial() ? _actual_part->ActualPart() : _actual_part) : 0) ;
    VhdlIdDef *formal_id = _formal_part ? _formal_part->GetId() : 0 ;
    VhdlIdDef *formal_type = formal_id ? formal_id->Type() : 0 ;
    // Viper 6649 : Do not evaluate string using EvaluateForVerilog when the formal type is string
    if (FromVerilog() && actual_part && actual_part->IsStringLiteral() && formal_type && !formal_type->IsStringType()) {
        VhdlValue *val = actual_part->EvaluateForVerilog(target_constraint,df,0) ;
        if (!actual_part->IsBitStringLiteral()) _from_verilog = 0 ;
        return val ;
    } else {
#endif
        return (_actual_part) ? _actual_part->Evaluate(target_constraint,df,0) : 0 ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    }
#endif
}

VhdlValue *VhdlInertialElement::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    return (_actual_part) ? _actual_part->Evaluate(target_constraint,df,0) : 0 ;
}

VhdlValue *VhdlRecResFunctionElement::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow * /*df*/, unsigned /*get_driver_value*/)
{
    return 0 ;
}

void VhdlExpression::SplitAndedSubExpressions(Array& anded_expressions_array)
{
    // Added for VIPER #3907
    anded_expressions_array.Insert(this) ;
    return ;
}

void VhdlOperator::SplitAndedSubExpressions(Array& anded_expressions_array)
{
    // Added for VIPER #3907
    if (!_operator || _operator->GetOperatorType() != VHDL_and) {
        anded_expressions_array.Insert(this) ;
        return ;
    }

    if (_left) _left->SplitAndedSubExpressions(anded_expressions_array) ;
    if (_right) _right->SplitAndedSubExpressions(anded_expressions_array) ;

    return ;
}

VhdlValue *VhdlOperator::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    if (!_operator) return 0 ; // Not yet type-resolved..?.

    VhdlValue *result = 0 ;

    if (_right) {
        result = _operator->ElaborateOperator(_left,_right,target_constraint,df, this) ;
    } else {
        result = _operator->ElaborateOperator(_left,target_constraint,df, this) ;
    }

#if 0
    VhdlConstraint *range_constraint = 0 ; //result ? EvaluateIntegerRangeConstraint(df) : 0 ;
    if (range_constraint) { // VIPER #6473
        // Info ("range: %s", range_constraint->Image()) ;
        unsigned range_bits = range_constraint->NumBitsOfRange() ;
        if (result && result->NumBits() > range_bits) { // redundant null check
            VhdlNonconst *result_nc = result->ToNonconst() ;
            result_nc->Adjust(range_bits) ;
            result = result_nc ;
        }
        delete range_constraint ;
    }
#endif

#if 0
    VhdlIdDef *op_type = _operator->Type() ;
    if (VhdlNode::_present_scope  && result && op_type && (StdType("integer") == op_type)) result->MakeInteger() ; // Viper #6518
#endif

    return result ;
}

VhdlValue *VhdlAllocator::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    if (IsRtlElab()) {
        Error("allocator is not synthesizable") ;
    } else { // static elaboration
        return _subtype ? _subtype->EvaluateAllocatorSubtype(target_constraint, df) : 0 ;
    }

    return 0 ;
}

VhdlValue *VhdlAggregate::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    if (!_element_assoc_list) return 0 ; // Error ?

    if (!_aggregate_type) return 0 ; // or else type-inference failed

    if (!target_constraint) {
        // LRM 7.3.2.2 :
        // Get constraint from aggregate type
        target_constraint = _aggregate_type->Constraint() ;
    }
    if (!target_constraint) return 0 ; // type did not get the right constraint..

    // Create an array that will hold the values :
    Array *values = new Array(_element_assoc_list->Size()) ;

    // LRM 7.3.2.2
    // Find the smallest choice position and pass that into the real aggregate
    // assigner below, so we can determine the exact position of each element
    unsigned i, j ;
    VhdlExpression *assoc ;
    // Some cases we have to generate a aggregate range (index) constraint by checking the 'choices' :
    VhdlConstraint *aggregate_constraint = target_constraint->Copy() ;

    // Issue 2181:
    // For aggregates without 'others' clause, but with named elements,
    // we need to calculate the aggregate constraint by calculating the named indexes,
    // and choosing the highest/lowest from them.
    // For all other aggregates, we use the 'target_constraint' :
    // Either the real (context) target constraint, or the constraint of this aggregate type.
    if (target_constraint->IsArrayConstraint()) {
        // Array aggregates :

        // Now, if there are named associations, there must be one in the last element.
        // This is because positional associations cannot follow named associations.
        // Also, if there is an others clause, it must also be the last one.
        // So check the last association :
        assoc = (_element_assoc_list->Size()) ? (VhdlExpression*)_element_assoc_list->GetLast() : 0 ;
        if (assoc && (assoc->IsAssoc() && !assoc->IsOthers())) {
            // There is a named association in the last position, and it is not an others clause.
            // So, we need to calculate the aggregate index constraint from the named assocs.

            // Start with a copy of the target constraint,
            // and make that 'unconstrained', (so that calls to 'EvaluateConstraintByChoice' can adjust the index range)
            // This way, we maintain the 'direction' (and element constraint) of the target constraint,
            // while still adjusting it to the lowest/highest choices in the aggregate :
            //aggregate_constraint = target_constraint->Copy() ;
            aggregate_constraint->SetUnconstrained() ;
            unsigned check_dir = 0 ;
            FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
                if (!assoc) continue ;
                aggregate_constraint = assoc->EvaluateConstraintByChoice(aggregate_constraint, df, IsAggregateTypeAssoc(i), check_dir) ;
                check_dir |= IsAggregateTypeAssoc(i) ;
            }

            // VIPER 2592 :  (LRM rules 7.3.2.2) :
            // For named aggregates without others clause : Check index bounds of this choice-generated constraint againt the aggregate base-type bounds (NOT against the target constraint!)
            // For aggregates with a others clause the choices are checked against the target_constraint (NOT just against base type constraint) in EvaluateByChoice() below.
            // For positional aggregates, we do the check number of elements against target constraint when we use the aggregate value (check in CheckAgainst())
            VhdlConstraint *base_type_constraint = _aggregate_type->Constraint() ;
            if (base_type_constraint && !base_type_constraint->Contains(aggregate_constraint->Left())) {
                char *image = aggregate_constraint->Left()->Image() ;
                char *r = base_type_constraint->Image() ;
                assoc->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
            }
            if (base_type_constraint && !base_type_constraint->Contains(aggregate_constraint->Right())) {
                char *image = aggregate_constraint->Right()->Image() ;
                char *r = base_type_constraint->Image() ;
                assoc->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
            }
        }
    }

    // Accumulate the values from the element_assoc_list.
    // Normal elements (no choices) represent order-based
    // association. We can just append them one by one.

    // Associated element (with choices) need to be evaluated
    // one-by-one, so we know how many there are.
    // To know where they fit in the composite,
    // we gotta have the target constraint.
    // That is certainly needed for the 'others' choice
    // that can be in there too.
    unsigned n_elements = 0 ;
    VhdlValue *default_value = 0 ; // Viper #4950
    unsigned is_named = 0 ;
    VhdlValue *value ;
    VhdlConstraint *element_constraint ;
    Set record_ele_pos(NUM_HASH) ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
        if (!assoc) continue ;

        if (assoc->IsAssoc()) {
            // named element.
            // Pass in the generated 'aggregate_constraint' if it is there. (VIPER 2181).
            // Otherwise, use target constraint :
            assoc->EvaluateByChoice(values, (aggregate_constraint) ? aggregate_constraint : target_constraint, df, default_value, &record_ele_pos, IsAggregateTypeAssoc(i)) ;
            is_named = 1 ;

            if (!assoc->IsOthers())  n_elements += 1 ;
        } else {
            // Positional element :
            // Find the element constraint (to pass record elements too)
            element_constraint = target_constraint->ElementConstraintAt(i) ;
            value = assoc->Evaluate(element_constraint,df,0) ;
            if (!value && df && IsStaticElab()) df->SetNonconstExpr() ; // Non constant expression, mark in data flow, Done for Viper #4846

            if (value && value->IsComposite() && IsAggregateTypeAssoc(i)) {
                for (j = 0 ; j < value->NumElements() ; ++j) {
                    // following takes care of default_element_value
                    values->InsertLast(value->TakeValueAt(j)) ;
                }
                n_elements += j ;
                delete value ;
            } else {
                values->InsertLast(value) ;
                n_elements += 1 ;
            }
        }
    }

    // Check if all choices are covered
    // Don't need to do this for positional aggregates : the number of values will
    // be checked when the aggregate is assigned to some target.
    // Viper #4950 : If default_value is present, NULL elements are not error
    if (is_named && !default_value) {
        FOREACH_ARRAY_ITEM(values, i, value) {
            if (!value) {
                // VIPER #2981 : Don't issue error in static elaboration if data flow is absent
                if (IsStaticElab() && ((df && df->IsNonconstExpr()) || !df)) continue ; // Don't issue error for nonconstant elements in static elaboaration
                Error("some element(s) in aggregate are missing") ;
                break ;
            }
        }
    }

    // We should 'constraint' an unconstrained target constraint if we generated a aggregate constraint :
    // ONLY do this if we have a real target constraint (not the constraint pointer of this type).
    // Remember that we do not want to 'constraint' the constraint of this aggregate type, but
    // only the real target constraint (if there).
    if (aggregate_constraint && target_constraint->IsUnconstrained() && (target_constraint!=_aggregate_type->Constraint())) {
        target_constraint->ConstraintBy(aggregate_constraint) ;
    }

    // clean up
    delete aggregate_constraint ;

    // Viper #3475 : mismatch between actual and expected number of element_assoc
    if (IsStaticElab() && !target_constraint->IsUnconstrained() && n_elements > target_constraint->Length()) {
        Error("expression has %d elements ; expected %d", n_elements, target_constraint->Length()) ;
    }

    // NOTE : composite value COULD now contain NULL elements (in error cases).
    // Viper #4950 : If default_value is present, NULL elements are not error
    return new VhdlCompositeValue(values, default_value) ; // values are absorbed
}

VhdlValue *VhdlQualifiedExpression::EvaluateAllocatorSubtype(VhdlConstraint * /*target_constraint*/, VhdlDataFlow *df)
{
    if (IsRtlElab()) return 0 ;

    VhdlConstraint *constraint = _prefix ? _prefix->EvaluateConstraintInternal(df, 0) : 0 ;
    VhdlValue *allocated_val = Evaluate(constraint, df, 0) ;

    if (constraint && constraint->IsUnconstrained()) {
        constraint->ConstraintBy(allocated_val) ;
    }

#if 0 // Viper #7288
    if (constraint && target_constraint) { // For Viper : 3764/3771
        target_constraint->SetUnconstrained() ;
        target_constraint->ConstraintBy(constraint) ;
    }
#endif

    VhdlValue *return_val = 0 ;
    if (allocated_val) {
        allocated_val->ConstraintBy(constraint) ; // Viper #7288
        return_val = new VhdlAccessValue(allocated_val, constraint) ; // access_val will own the constraint
    } else {
        delete constraint ;
    }

    return return_val ;
}

VhdlValue *VhdlQualifiedExpression::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    if (!_prefix || !_aggregate) return 0 ;

    // (target) constraint handling for qualified expressions :
    //
    // LRM is not of much help. Only a few issues :
    // 7.3.2.2 : 'others' in an aggregate is allowed for aggregate in a qualified expression
    //         if the type mark of the qualified expression denotes a constrained array (sub)type
    //    This means that IF the prefix denotes a constrained array subtype (and the expression is
    //    an aggregate), we need to pass the prefix constraint into the expression to
    //    resolve the 'others' choices (that can be in there). Issue 1520 relates.
    // NOTE : based on this (7.3.2.2) rule,  std_logic_vector'(others=>'0') is ILLEGAL in any context !
    //
    // Also, if the 'target_constraint' is 'unconstrained' (happens for actuals of subprog calls),
    //         then we should adjust that constraint to the result constraint of the qualified expression.
    //         (that is normal 'subtype-conversion').
    //     We interpret that as follows : if 'target_constraint' is unconstrained,
    //     then we adjust it if the type mark denotes a constrained array subtype.
    //     Otherwise, we leave the adjustment up to the subtype conversion when we assign the target.
    //     (we don't need to do that here).
    //
    // So, based on these rules, do this :

    //    (1) Evaluate the prefix constraint
    VhdlConstraint *prefix_constraint = _prefix->EvaluateConstraintInternal(df, 0) ;

    //    (2) Evaluate the expression with the constraint from (1) if it is 'constrained'
    // Viper #7758: Even when it is not constrained, the constraint may be set during evaluation
    VhdlValue *result = _aggregate->Evaluate(prefix_constraint, df,0) ;

    //    (3) Test the return value against the prefix constraint from (1).
    if (result) (void)result->CheckAgainst(prefix_constraint, this, df) ;

    //    (4) Adjust 'target_constraint' (if it's unconstrained) to constraint (1).
    if (target_constraint && target_constraint->IsUnconstrained()) {
        target_constraint->ConstraintBy(prefix_constraint) ;
    }

    delete prefix_constraint ;

    return result ;
}

VhdlValue *VhdlElementAssoc::Evaluate(VhdlConstraint * /*target_constraint*/, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    // This probably never gets called. Intercepted by EvaluateByChoice()
    if (!_expr) return 0 ;
    return _expr->Evaluate(0,df,0) ;
}

VhdlValue *VhdlWaveformElement::Evaluate(VhdlConstraint *target_constraint, VhdlDataFlow *df, unsigned /*get_driver_value*/)
{
    if (!_value || _value->IsNull()) return 0 ;
    return _value->Evaluate(target_constraint,df,0) ;
}

/**********************************************************/
//  Evaluate a association
/**********************************************************/

void VhdlDiscreteRange::EvaluateByChoice(Array * /*values*/, VhdlConstraint * /*constraint*/, VhdlDataFlow * /*df*/, VhdlValue *& /*default_value*/, Set * /*record_ele_pos*/, unsigned /*is_aggregate_type*/)
{
    return ; // VERIFIC_ASSERT ?
}

void VhdlElementAssoc::EvaluateByChoice(Array *values, VhdlConstraint *constraint, VhdlDataFlow *df, VhdlValue *&default_value, Set *record_ele_pos, unsigned is_aggregate_type)
{
    if (!values) return ; // no return array. no show.

    VERIFIC_ASSERT(constraint) ;

    VhdlValue *value = 0 ;
    if (_expr && constraint->IsArrayConstraint()) {
        // For array constraints, the expression always gets the element constraint to work with.
        // independent of which element choice.
        value = _expr->Evaluate(constraint->ElementConstraint(),df,0) ;
        if (!value && df && IsStaticElab()) df->SetNonconstExpr() ; // Non constant expression, mark in data flow
    } else {
        // But for record constraints, we cannot evaluate the expression
        // until we know which (record field) choice applies.
        value = 0 ;
    }

    // look at the choices
    VhdlValue *exist ;
    VhdlValue *elem_val ;
    VhdlDiscreteRange *choice ;
    unsigned i ;
    unsigned pos ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        // Check 'others' choice
        if (choice->IsOthers()) {
            // OTHERS choice not allowed if we have an unconstrained target constraint.
            VhdlConstraint *range_constr = constraint->IsArrayConstraint() ? constraint->IndexConstraint() : 0 ;
            VhdlConstraint *tmp = range_constr ? range_constr : constraint ;
            if (tmp->IsUnconstrained()) {
                // VIPER #3561 : For non-constant region, do not issue this error
                // Viper #4233/4234 : Added check IsConditionExpression
                // RD: IsConditionExpression no longer needed : VIPER 7081 catches this in analysis.
                if (df && df->IsNonconstExpr() /* && !df->IsConditionExpression()*/) continue ;
                Error("OTHERS is illegal aggregate choice for unconstrained target") ;
                continue ;
            }

            if (value && _expr && !_expr->IsAllocator()) { // Viper #4950
                if (default_value) delete default_value ; // should assert here
                default_value = value->Copy() ;
            }

            // Fill the array with as many values as needed for the constraint size
            if (!constraint->IsNullRange()) {
                // VIPER 7342 : Test Length()-1, not Length().
                unsigned max_array_size = vhdl_file::GetMaxArraySize() ;
                if (max_array_size > 31) max_array_size = 31 ; // Viper #5866
                verific_uint64 constraint_size = constraint->GetSizeForConstraint() ; // Viper #7798
                if (constraint_size && (constraint_size-1) >> max_array_size) { // Viper #5702
                    Error("array size is larger than 2**%d", vhdl_file::GetMaxArraySize()) ;
                    continue ;
                }

                while (constraint->Length() > values->Size()) {
                    values->InsertLast(0) ;
                }
            }

            // Fill all empty spots in values with 'others' value.
            FOREACH_ARRAY_ITEM(values, pos, exist) {
                if (exist) continue ; // Already filled

                if (_expr && constraint->IsRecordConstraint()) {
                    // VIPER #6719 : Check whether this 'pos+1' is already covered earlier
                    // If value cannot be evaluated in static elab, null is inserted
                    // in 'values'. 'pos+1' is checked as instead of position we have
                    // inserted 'pos+1' as Set cannot handle '0'
                    if (record_ele_pos && record_ele_pos->GetItem((void*)(long)(pos+1))) continue ; // already specified in aggregate
                    elem_val = _expr->Evaluate(constraint->ElementConstraintAt(pos),df,0) ;
                    if (!elem_val && df && IsStaticElab()) df->SetNonconstExpr() ; // Non constant expression, mark in data flow
                } else {
                    elem_val = (value) ? ((_expr && _expr->IsAllocator()) ? value->CopyExplicit() : 0) : 0 ; // Viper #4394
                }

                // Insert the array element value :
                values->Insert(pos, elem_val) ;
            }
            continue ;
        }

        // Find 'position' of the choice

        // For Record elements,
        // Use the feature that 'Evaluate' returns position value and PosOf takes position value.
        // Just like the position for the Array constraint indexes.
        if (choice->IsRange()) {
            // range choices
            VhdlConstraint *range = choice->EvaluateConstraintInternal(df, 0) ;
            if (!range) continue ;

            if (range->IsNullRange()) {
                // null range warning is already given
                choice->Warning("range choice ignored") ;
                delete range ;
                continue ; // Never selected
            }

            // Check if both bounds are in the constraint :
            if (!constraint->Contains(range->Left())) {
                char *image = range->Left()->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete range ;
                continue ;
            }
            if (!constraint->Contains(range->Right())) {
                char *image = range->Right()->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete range ;
                continue ;
            }

            if (is_aggregate_type && range->Dir() != constraint->Dir()) Error("choice direction does not match with the context direction") ;

            // Get the actual position of the values in the constraint :
            unsigned left_pos = constraint->PosOf(range->Left()) ;
            unsigned right_pos = constraint->PosOf(range->Right()) ;

            // We want (for the loop below) to make left_pos <= right_pos.
            // Since 'range' can be in arbitrary direction, swap them if that is not the case :
            if (left_pos > right_pos) {
                unsigned tmp_pos = left_pos ;
                left_pos = right_pos ;
                right_pos = tmp_pos ;
            }

            // Now, left_pos is always <= right_pos.
            VERIFIC_ASSERT (left_pos <= right_pos) ;

            // Find if we need to extend the array
            while (values->Size()<left_pos) {
                values->InsertLast(0) ;
            }

            for (pos = left_pos; pos <= right_pos; pos++) {
                // Check if there is an existing one here
                exist = (values->Size()>pos) ? (VhdlValue*)values->At(pos) : 0 ;
                if (exist) {
                    Error("aggregate already has a choice in this range") ;
                    break ;
                }

                // Insert the value.
                if (_expr && constraint->IsRecordConstraint()) {
                    elem_val = _expr->Evaluate(constraint->ElementConstraintAt(pos),df,0) ;
                    if (!elem_val && df && IsStaticElab()) df->SetNonconstExpr() ; // Non constant expression, mark in data flow
                } else {
                    elem_val = (value) ? ((_expr && _expr->IsAllocator()) ? value->CopyExplicit() : value->Copy()) : 0 ; // Viper #4394
                }

                if (elem_val && is_aggregate_type && elem_val->IsComposite()) {
                    // We need to pick up the correct elem_val from the value wrapper
                    // TODO: it'd be simple to avoid unnecessary copy of the value
                    VhdlValue *inner_val = elem_val->TakeValueAt(pos-left_pos) ;
                    delete elem_val ;
                    elem_val = inner_val ;
                }

                values->Insert(pos, elem_val) ;
            }
            delete range ;
        } else {
            // single choice
            VhdlValue *element_pos = choice->Evaluate(0,df,0) ;
            if (!element_pos) continue ; // something went wrong

            if (!element_pos->IsConstant()) {
                choice->Error("choice is not constant") ;
                delete element_pos ;
                continue ;
            }

            // Check if this element is in the target constraint range :
            if (!constraint->Contains(element_pos)) {
                char *image = element_pos->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete element_pos ;
                continue ;
            }

            // Find the position where to insert the value
            pos = constraint->PosOf(element_pos) ;

            // 'pos' (unsigned number) is now w.r.t. the left position in the constraint
            // Find if we need to extend the array
            while (values->Size()<pos) {
                values->InsertLast(0) ;
            }

            // Check if there is an existing one here
            exist = (values->Size()>pos) ? (VhdlValue*)values->At(pos) : 0 ;
            if (exist) {
                char *image = element_pos->Image() ;
                Error("choice %s is already covered", image) ;
                Strings::free(image) ;
                delete element_pos ;
                continue ;
            }
            delete element_pos ;

            // Insert the value
            if (_expr && constraint->IsRecordConstraint()) {
                elem_val = _expr->Evaluate(constraint->ElementConstraintAt(pos),df,0) ;
                if (!elem_val && df && IsStaticElab()) df->SetNonconstExpr() ; // Non constant expression, mark in data flow
                // VIPER #6719: Insert the position of assigned record member in the set
                // so that during evlaution of others we can find exact information
                // Increase the value of 'pos' by 1, as '0' cannot be inserted in set
                if (record_ele_pos) (void) record_ele_pos->Insert((void*)(long)(pos+1)) ;
            } else {
                elem_val = (value) ? value->Copy() : 0 ;
            }
            values->Insert(pos, elem_val) ;
        }
    }
    delete value ;
}

/**********************************************************/
//  Build a constraint based on the formals (named indexes) of an aggregate
/**********************************************************/

VhdlConstraint *
VhdlDiscreteRange::EvaluateConstraintByChoice(VhdlConstraint *previous_constraint, VhdlDataFlow * /*df*/, unsigned /*is_aggregate_type*/, unsigned /*check_dir*/)
{
    // Build the constraint of an aggregate based on the (named) index formals.
    // Start with 0 and call for each element.
    return previous_constraint ; // Should not happen for a positional association.
}

VhdlConstraint *
VhdlElementAssoc::EvaluateConstraintByChoice(VhdlConstraint *previous_constraint, VhdlDataFlow *df, unsigned is_aggregate_type, unsigned check_dir)
{
    // Build the constraint of an aggregate based on the (named) index formals.
    // Start with 0 and call for each element.
    VhdlDiscreteRange *choice ;
    VhdlValue *element ;
    VhdlConstraint *range ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        if (!choice) continue ;
        // Check 'others' choice
        if (choice->IsOthers()) continue ; // cannot extract constraint info. Error elsewhere..

        if (choice->IsRange()) {
            // Evaluate the range
            range = choice->EvaluateConstraintInternal(df, 0) ;
            if (!range) continue ;

            if (!previous_constraint) {
                // First element is this range. Take it !
                previous_constraint = range ;
                continue ;
            }

            // Check if the left and right sides of the range are in the previous constraint.
            previous_constraint->ConstraintByChoice(range->Left()) ;
            previous_constraint->ConstraintByChoice(range->Right()) ;
            if (is_aggregate_type && (range->Dir() != previous_constraint->Dir())) { // Vhdl-2008
                if (check_dir) Error("choice direction does not match with the context direction") ;
                previous_constraint->ReverseDirection() ;
            }

            // clean up
            delete range ;
        } else {
            // single choice (FIX ME : do I need the DataFlow ?)
            element = choice->Evaluate(0,0,0) ;
            if (!element) continue ;

            if (!previous_constraint) {
                // First element is this element.
                // Create index range : (elem 'to' elem)
                previous_constraint = new VhdlRangeConstraint(element, VHDL_to, element->Copy()) ;
                continue ;
            }

            previous_constraint->ConstraintByChoice(element) ;
            delete element ;
        }
    }
    return previous_constraint ;
}

/**********************************************************/
//  Assign to a 'choiced' target
/**********************************************************/

void VhdlDiscreteRange::AssignByChoice(VhdlValue * /*assign_value*/, VhdlConstraint * /*constraint*/, VhdlDataFlow * /*df*/, unsigned /*is_aggregate_type*/)
{
    return ; // VERIFIC_ASSERT ?
}

void VhdlElementAssoc::AssignByChoice(VhdlValue *assign_value, VhdlConstraint *constraint, VhdlDataFlow *df, unsigned is_aggregate_type)
{
    if (!assign_value) return ; // No value to assign
    if (!_expr) return ; // No expression to assign it too.

    VERIFIC_ASSERT(assign_value->IsComposite()) ;
    VERIFIC_ASSERT(constraint) ;

    // look at the choices
    VhdlValue *exist, *element ;
    VhdlDiscreteRange *choice ;
    unsigned i ;
    unsigned pos ;
    FOREACH_ARRAY_ITEM(_choices, i, choice) {
        // Check 'others' choice
        if (choice->IsOthers()) {
            choice->Error("'others' is illegal in assignment target aggregate") ;
            continue ;
        }

        // Find 'position' in the value, by looking at the choice

        // For Record elements, just like the position for the Array constraint indexes.
        // Use the feature that 'Evaluate' returns position value
        // and PosOf takes position value,
        if (choice->IsRange()) {
            // Does not make sense to assign multiple values to one element,
            if (!is_aggregate_type) choice->Error("range choice is illegal in assignment target aggregate") ;

            VhdlConstraint *range = choice->EvaluateConstraintInternal(df, 0) ;
            if (!range) continue ;

            VhdlValue *left_val = range->Left() ;
            VhdlValue *right_val = range->Right() ;
            if (!left_val || !right_val) continue ;

            if (range->IsNullRange()) {
                // null range warning is already given
                choice->Warning("range choice ignored") ;
                delete range ;
                continue ; // Never selected
            }

            // Check if both bounds are in the constraint :
            if (!constraint->Contains(left_val)) {
                char *image = left_val->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete range ;
                continue ;
            }

            if (!constraint->Contains(right_val)) {
                char *image = right_val->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete range ;
                continue ;
            }

            // Get the actual position of the values in the constraint :
            unsigned left_pos = constraint->PosOf(range->Left()) ;
            unsigned right_pos = constraint->PosOf(range->Right()) ;

            unsigned num_elements = (unsigned)range->Length() ;
            verific_int64 incr = (left_pos < right_pos) ? 1 : -1 ;

            Array *elem_values = new Array(num_elements) ;
            for (pos = 0 ; pos < num_elements ; ++pos) {
                unsigned ele_pos = (unsigned)(left_pos+pos*incr) ;
                if (ele_pos >= assign_value->NumElements()) {
                    char temp[64] ;
                    sprintf(temp, "%lld", left_val->Integer()+pos*incr) ;
                    char *r = constraint->Image() ;
                    choice->Error("choice %s is out of range %s for the index subtype", temp, r) ;
                    Strings::free(r) ;
                    continue ;
                }
                
                exist = assign_value->TakeValueAt(ele_pos) ;
                if (!exist) {
                    // VIPER #6494 : In static elab mode do not produce error if
                    // element in aggregate value does not exist, as due to presence
                    // of non-constant elements, element value can be null.
                    if (IsStaticElab() && ((df && df->IsNonconstExpr()) || !df)) {
                        continue ;
                    }
                    // This value was already taken :
                    char temp[64] ;
                    sprintf(temp, "%lld", left_val->Integer()+pos*incr) ;
                    choice->Error("choice %s is already covered", temp) ;
                    continue ;
                }

                elem_values->Insert(pos, exist) ;
            }

            VhdlValue *slice_value = new VhdlCompositeValue(elem_values) ;
            _expr->Assign(slice_value, df, 0) ;
            delete range ;
        } else {
            // single choice
            element = choice->Evaluate(0,df,0) ;
            if (!element) continue ;
            if (!element->IsConstant()) {
                choice->Error("choice is not constant") ;
                delete element ; // Memory leak fix
                continue ;
            }

            // Check that the element is inside the constraint :
            if (!constraint->Contains(element)) {
                char *image = element->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete element ;
                continue ;
            }

            // Now find the position in the array from 'left_position'
            // 'pos' is deliberately 'unsigned', to fit large differences between left and present position
            // (Which happens for unconstrained arrays)
            pos = constraint->PosOf(element) ;

            if (pos >= assign_value->NumElements()) {
                // Can maybe still happen for incorrect unconstrained subtype aggregate : not correct : (0=>x,500=>y)
                char *image = element->Image() ;
                char *r = constraint->Image() ;
                choice->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
                delete element ;
                continue ;
            }

            // So, get the value at position 'pos' from the assignment value
            exist = assign_value->TakeValueAt(pos) ;
            if (!exist) {
                // VIPER #6494 : In static elab mode do not produce error if
                // element in aggregate value does not exist, as due to presence
                // of non-constant elements, element value can be null.
                if (IsStaticElab() && ((df && df->IsNonconstExpr()) || !df)) {
                    delete element ; // Memory leak fix
                    continue ;
                }
                // This value was already taken :
                char *image = element->Image() ;
                choice->Error("choice %s is already covered", image) ;
                Strings::free(image) ;
                delete element ;
                continue ;
            }

            // And assign it to this choice expression :
            _expr->Assign(exist, df, 0) ;
            delete element ;
        }
    }
}

/**********************************************************/
//  Assign to an expression (name or aggregate)
/**********************************************************/

void VhdlDiscreteRange::Assign(VhdlValue *val, VhdlDataFlow * /*df*/, Array *name_select)
{
    Error("illegal target for assignment") ;
    delete val ;
    delete name_select ;
}

void VhdlExpression::Assign(VhdlValue *val, VhdlDataFlow * /*df*/, Array *name_select)
{
    Error("illegal target for assignment") ;
    delete val ;
    delete name_select ;
}

void VhdlAggregate::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    if (!val) return ;

    if (!_aggregate_type || !_element_assoc_list) return ; // or else something is badly wrong.

    // Check semantics, but I think this is true (others and range choices are illegal in target)
    // range choice legal in 1076-2008. A modified check is done later
    if (!IsVhdl2008() && val->NumElements() != _element_assoc_list->Size()) {
        Error("incorrect number of elements in target aggregate") ;
        delete val ;
        delete name_select ;
        return ;
    }

    // LRM 7.3.2.2 : since a target is not on the LRM list (a) to (i),
    // the constraint gets determined from the aggregate base type :
    VhdlConstraint *constraint = _aggregate_type->Constraint() ;
    if (!constraint) return ; // type constraint was not set correctly

    // LRM 7.3.2.2
    // Find the smallest choice position and pass that into the real aggregate
    // assigner below, so we can determine the exact position of each element
    unsigned i ;
    VhdlExpression *assoc ;
    VhdlConstraint *aggregate_constraint = 0 ;
    // Issue 2181:
    // For aggregates without 'others' clause, but with named elements,
    // we need to calculate the aggregate constraint by calculating the named indexes,
    // and choosing the highest/lowest from them.
    // For all other aggregates, we use the 'target_constraint' :
    // Either the real (context) target constraint, or the constraint of this aggregate type.
    if (constraint->IsArrayConstraint()) {
        // Array aggregates :

        // Now, if there are named associations, there must be one in the last element.
        // This is because positional associations cannot follow named associations.
        // Also, if there is an others clause, it must also be the last one.
        // So check the last association :
        assoc = (_element_assoc_list->Size()) ? (VhdlExpression*)_element_assoc_list->GetLast() : 0 ;
        if (assoc && (assoc->IsAssoc() && !assoc->IsOthers())) {
            // There is a named association in the last position, and it is not an others clause.
            // So, we need to calculate the aggregate index constraint from the named assocs.

            // Start with a copy of the target constraint,
            // and make that 'unconstrained', (so that calls to 'EvaluateConstraintByChoice' can adjust the index range)
            // This way, we maintain the 'direction' (and element constraint) of the target constraint,
            // while still adjusting it to the lowest/highest choices in the aggregate :
            aggregate_constraint = constraint->Copy() ;
            aggregate_constraint->SetUnconstrained() ;
            unsigned check_dir = 0 ;
            FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
                if (!assoc) continue ;
                aggregate_constraint = assoc->EvaluateConstraintByChoice(aggregate_constraint, df, IsAggregateTypeAssoc(i), check_dir) ;
                check_dir |= IsAggregateTypeAssoc(i) ;
            }

            // VIPER 2592 :  (LRM rules 7.3.2.2) :
            // For named aggregates without others clause : Check index bounds of this choice-generated constraint againt the aggregate base-type bounds (NOT against the target constraint!)
            // For aggregates with a others clause the choices are checked against the target_constraint (NOT just against base type constraint) in AssignByChoice() below.
            // For positional aggregates, we do the check number of elements against target constraint when we created this aggregate value (check in CheckAgainst())
            VhdlConstraint *base_type_constraint = _aggregate_type->Constraint() ;
            if (base_type_constraint && !base_type_constraint->Contains(aggregate_constraint->Left())) {
                char *image = aggregate_constraint->Left()->Image() ;
                char *r = base_type_constraint->Image() ;
                assoc->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
            }
            if (base_type_constraint && !base_type_constraint->Contains(aggregate_constraint->Right())) {
                char *image = aggregate_constraint->Right()->Image() ;
                char *r = base_type_constraint->Image() ;
                assoc->Error("choice %s is out of range %s for the index subtype", image, r) ;
                Strings::free(image) ;
                Strings::free(r) ;
            }
        }
    }

    // CHECK ME : should 'name_select' be 0 ? Test for that ?
    // For array aggregate, element_assocs must be either
    // all named or all positional
    unsigned assoc_value_loc = 0 ;
    VhdlValue *elem ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
        if (!assoc) continue ; // error ?
        if (assoc->IsAssoc()) {
            // named element. Use 'aggregate_constraint' if it is there, otherwise use this aggregate type constraint.
            assoc->AssignByChoice(val, (aggregate_constraint) ? aggregate_constraint : constraint, df, IsAggregateTypeAssoc(i)) ;
        } else {
            unsigned num_elements = 1 ;
            if (IsAggregateTypeAssoc(i)) num_elements = (unsigned)assoc->NumAggregateElements(df) ;

            //
            if ((assoc_value_loc+num_elements) > val->NumElements()) {
                Error("incorrect number of elements in target aggregate") ;
                break ;
            }

            // positional element.
            elem = 0 ;
            if (num_elements == 1) {
                elem = val->TakeValueAt(assoc_value_loc) ; // Remove element value from 'val'
            } else if (num_elements > 1) {
                unsigned idx = 0 ;
                Array *slice_elements = new Array(num_elements) ;
                for (idx = 0 ; idx < num_elements ; ++idx) slice_elements->InsertLast(val->TakeValueAt(assoc_value_loc+idx)) ;
                elem = new VhdlCompositeValue(slice_elements) ;
            }
            assoc_value_loc += num_elements ;
            assoc->Assign(elem,df,name_select) ;
        }
    }

    // Now check if all elements are taken from the value ?
    // Not needed : the test above (num_elements==aggregate_size)
    // with the test in AssignByChoice for out-of-bound choices
    // and duplicate choices guarantees that this 'val' is empty
    // (or an error occurred)

    delete val ; // Its only a shell now
    delete name_select ;
    delete aggregate_constraint ;
}

void VhdlAssocElement::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    // Assignment to (actual) of a full association element..
    if (!_actual_part) return ;
    _actual_part->Assign(val, df, name_select) ;
}

void VhdlInertialElement::Assign(VhdlValue *val, VhdlDataFlow *df, Array *name_select)
{
    // Assignment to (actual) of a full association element..
    if (!_actual_part) return ;
    _actual_part->Assign(val, df, name_select) ;
}

/**********************************************************/
// Used for Vhdl 2008 array aggregate elaboration.
// The object is guranteed to be an array type lvalue.
// Returns the number of elements it has.
/**********************************************************/
verific_uint64 VhdlDiscreteRange::NumAggregateElements(VhdlDataFlow *df)
{
    VhdlConstraint *range = EvaluateConstraintInternal(df, 0) ;
    if (!range || !range->IsArrayConstraint()) return 0 ;

    verific_uint64 size = range->Length() ;
    delete range ;

    return size ;
}

verific_uint64 VhdlAggregate::NumAggregateElements(VhdlDataFlow *df)
{
    if (!_element_assoc_list) return 0 ;

    verific_uint64 size = 0 ;

    unsigned i ;
    VhdlExpression *assoc ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, assoc) {
        if (!assoc) continue ;
        size += (IsAggregateTypeAssoc(i)) ? assoc->NumAggregateElements(df) : 1 ;
    }

    return size ;
}

/**********************************************************/
// Association of a partial formal with an actual
// For component/entity/block assoc lists.
/**********************************************************/

void
VhdlDiscreteRange::PartialAssociation(Instance * /*inst*/, VhdlDataFlow * /*df*/) { }

void
VhdlAssocElement::PartialAssociation(Instance *inst, VhdlDataFlow *df)
{
    // Partial association in a concurrent environment (port/generic association).
    // This is not too complicated as long as the formal is not unconstrained
    if (!_actual_part || !_formal_part) return ; // open association..

    // First check the formal : need to know its 'mode' and its 'constraint'
    VhdlIdDef *formal = _formal_part->FindFormal() ;
    if (!formal) return ; // type-inference failed..

    if (IsStaticElab()) {
        // VIPER #4996 : If actual for formal is not open, mark the formal that
        // actual is present for the formal 'formal'
        if (!_actual_part->IsOpen()) formal->SetIsActualPresent() ;
    }
    // VIPER 1863 & 1472 : begin
    // For recursive instantiation, formal value and constraints are in value/constraint
    // stacks. Get the last entered value/constraint list from corresponding stacks.
    Map *id_values = VhdlNode::GetValueMap() ;
    Map *id_constraints = VhdlNode::GetConstraintMap() ;

    // Get formal constraint from 'id_constraints' if exists
    VhdlConstraint *formal_constraint = id_constraints ? (VhdlConstraint*)id_constraints->GetValue(formal) : formal->Constraint() ;
    // VIPER 1863 & 1472 : end

    if (!formal_constraint) return ; // port declaration constraint setting failed..

    if (_formal_part->IsTypeConversion()) { // Viper #4391
        VhdlConstraint *constraint = _formal_part->EvaluateConstraintInternal(0, 0) ;
        formal_constraint->SetUnconstrained() ;
        formal_constraint->ConstraintBy(constraint) ;
        formal_constraint = constraint ;
    }

    // Partial formal..
    if (formal_constraint->IsUnconstrained()) {
        // FIX ME..
        Error("no support for partial association of unconstrained formals") ;
        return ;
    }

    // VIPER 1863 & 1472
    // Create formal value if there is no formal value in stack in case of recursion
    // or no formal value in formal id otherwise
    if ((id_values && !id_values->GetValue(formal)) || !formal->Value()) {
        // There is no formal value yet.
        // Create one (variable value) first, so that we can then assign the partials..
        //
        // If 'inst' is set, we need to create the portrefs into the instance (by formal port name).
        //
        // FIX ME : for 'constants' (generics) we should really not create a variable value...
        // Maybe we should create 'InitialValue' and do the associations with 'AssignPartial' in a 'virtual' dataflow..?
        //
        // Carefull : if there is no instance, we should NOT pass-in a name.
        // Otherwise, named nets will be created, which can collide with existing nets.
        VhdlValue *full_formal_value = formal_constraint->CreateValue( (inst) ? formal->OrigName() : 0,0,0,inst) ;
        if (!full_formal_value && formal->IsGeneric()) full_formal_value = formal_constraint->CreateInitialValue(formal) ;
        // VIPER 1863 & 1472
        if (id_values) { // Recursion, set formal value to stack
            // This routine is called twice with same map 'id_values'. So, need to
            // delete old data from 'id_values' and use force insert
            VhdlValue *old_val = (VhdlValue*)id_values->GetValue(formal) ; // Get old value
            (void) id_values->Insert(formal, full_formal_value, 1 /* force overwrite */) ;
            delete old_val ;
        } else { // Not recursion, set formal value to formal identifier
            formal->SetValue(full_formal_value) ;
        }
    }

    // VIPER 1863 & 1472 : Evaluate formal constraint, setting instantiated unit's constraint to formal id
    // from constraint stack
    if (id_constraints) VhdlNode::SwitchConstraint(formal) ;

    // Evaluate the formal constraint, for usage at the actual :
    VhdlConstraint *constraint = _formal_part->IsTypeConversion() ? formal_constraint : _formal_part->EvaluateConstraintInternal(0, 0) ;

    // VIPER 1863 & 1472
    // Switch back constraint of formal id so that current constraint resides in stack
    if (id_constraints) VhdlNode::SwitchConstraint(formal) ;

    // Determine direction of association from the port direction.
    // For most port, we could go one direction only, but with partial assocs there are
    // conversion functions and such which make the direction more important..
    if (formal->IsInput()) {
        // Regular 'input' port/generics,
        // Evaluate the actual
        // The actual is an expression, so might contain pass-in the (partial-formal) constraint to resolve 'others' and stuff in actual)
        VhdlValue *val = _actual_part->Evaluate(constraint,df,0) ;

        // VIPER 1863 & 1472 : Set current value of formal from stack to formal id
        if (id_values) VhdlNode::SwitchValue(formal) ;

        // Now assign to formal if there is no recursion
        _formal_part->Assign(val,0,0) ;

        // VIPER 1863 & 1472 : Switch back current formal value to stack from formal id
        if (id_values) VhdlNode::SwitchValue(formal) ;

    } else {
        // VIPER 1863 & 1472 : Set current value of formal from stack to formal id
        if (id_values) VhdlNode::SwitchValue(formal) ;

        // Regular 'out'/'inout' ports/generics
        // Evaluate value from formal (from initial value)
        VhdlValue *val = _formal_part->Evaluate(0,0,0) ;

        // VIPER 1863 & 1472 : Switch back current formal value to stack from formal id
        if (id_values) VhdlNode::SwitchValue(formal) ;

        // Assign to actual part (to actual dataflow)
        _actual_part->Assign(val,0,0) ;
    }

    // Now delete temporary constraint
    delete constraint ;
}

#ifdef VHDL_SUPPORT_VARIABLE_SLICE
void VhdlRange::InitializeForVariableSlice()
{
    if (_left) _left->InitializeForVariableSlice() ;
    if (_right) _right->InitializeForVariableSlice() ;
}

void VhdlExplicitSubtypeIndication::InitializeForVariableSlice()
{
    if (_res_function) _res_function->InitializeForVariableSlice() ;
    if (_type_mark) _type_mark->InitializeForVariableSlice() ;
    if (_range_constraint) _range_constraint->InitializeForVariableSlice() ;
    if (_range_type) _range_type->InitializeForVariableSlice() ;
}

void VhdlOperator::InitializeForVariableSlice()
{
    if (_left) _left->InitializeForVariableSlice() ;
    if (_right) _right->InitializeForVariableSlice() ;
    if (_operator) _operator->InitializeForVariableSlice() ;
}

void VhdlAllocator::InitializeForVariableSlice()
{
    if (_subtype) _subtype->InitializeForVariableSlice() ;
}

void VhdlAggregate::InitializeForVariableSlice()
{
    unsigned i ;
    VhdlDiscreteRange *elem ;
    FOREACH_ARRAY_ITEM(_element_assoc_list, i, elem) {
        if (!elem) continue ;
        elem->InitializeForVariableSlice() ;
    }
}

void VhdlQualifiedExpression::InitializeForVariableSlice()
{
    if (_prefix) _prefix->InitializeForVariableSlice() ;
    if (_aggregate) _aggregate->InitializeForVariableSlice() ;
}

void VhdlAssocElement::InitializeForVariableSlice()
{
    if (_formal_part) _formal_part->InitializeForVariableSlice() ;
    if (_actual_part) _actual_part->InitializeForVariableSlice() ;
}

void VhdlInertialElement::InitializeForVariableSlice()
{
    if (_actual_part) _actual_part->InitializeForVariableSlice() ;
}

void VhdlElementAssoc::InitializeForVariableSlice()
{
    if (_choices && _choices->Size()>1) {
        unsigned i ;
        VhdlDiscreteRange *choice ;
        FOREACH_ARRAY_ITEM(_choices, i, choice) {
            if (!choice) continue ;
            choice->InitializeForVariableSlice() ;
        }
    }
    if (_expr) _expr->InitializeForVariableSlice() ;
}

void VhdlWaveformElement::InitializeForVariableSlice()
{
    if (_value) _value->InitializeForVariableSlice() ;
    if (_after) _after->InitializeForVariableSlice() ;
}
#endif
