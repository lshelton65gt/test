/*
 *
 * [ File Version : 1.45 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlVisitor.h"

#include "Map.h"
#include "Array.h"

#include "VhdlTreeNode.h"
#include "VhdlStatement.h"
#include "VhdlDeclaration.h"
#include "VhdlIdDef.h"
#include "VhdlUnits.h"
#include "VhdlConfiguration.h"
#include "VhdlName.h"
#include "VhdlMisc.h"
#include "VhdlSpecification.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlTreeNode.h    */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlTreeNode, node)
{
    // Nothing to traverse
    (void) node ;
}

void VhdlVisitor::VHDL_VISIT(VhdlCommentNode, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing to traverse
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlUnits.h       */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlDesignUnit, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse identifier of this design unit
    TraverseNode(node.Id()) ;

    // Don't traverse local scope

    // Traverse context clause declarations
    TraverseArray(node.GetContextClause()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlPrimaryUnit, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignUnit, node) ;

    // Traverse secondary units
    MapIter mi ;
    VhdlTreeNode *expr = 0 ;
    FOREACH_MAP_ITEM(node.AllSecondaryUnits(), mi, 0, &expr) {
        if (expr) expr->Accept(*this) ;
    }

    // Don't traverse Library (node.Owner())
}

void VhdlVisitor::VHDL_VISIT(VhdlEntityDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlPrimaryUnit, node) ;

    // Traverse generic clause
    TraverseArray(node.GetGenericClause()) ;

    // Traverse port clause
    TraverseArray(node.GetPortClause()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse statements
    TraverseArray(node.GetStatementPart()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlConfigurationDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlPrimaryUnit, node) ;

    // Traverse entity name
    TraverseNode(node.GetEntityName()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse block configuration
    TraverseNode(node.GetBlockConfiguration()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlPackageDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlPrimaryUnit, node) ;

    // Vhdl 2008 LRM section 4.7 - Traverse generic clause
    TraverseArray(node.GetGenericClause()) ;

    // Vhdl 2008 LRM section 4.7 - Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlContextDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlPrimaryUnit, node) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlPackageInstantiationDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlPrimaryUnit, node) ;

    // Traverse uninstantiated package name
    TraverseNode(node.GetUninstantiatedPackageName()) ;

    // Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSecondaryUnit, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignUnit, node) ;

    // Don't traverse owner
}

void VhdlVisitor::VHDL_VISIT(VhdlArchitectureBody, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSecondaryUnit, node) ;

    // Traverse architecture name
    TraverseNode(node.GetEntityName()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse statements
    TraverseArray(node.GetStatementPart()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlPackageBody, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSecondaryUnit, node) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlConfiguration.h */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlConfigurationItem, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlBlockConfiguration, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlConfigurationItem, node) ;

    // Traverse block spec
    TraverseNode(node.GetBlockSpec()) ;

    // Traverse use clause
    TraverseArray(node.GetUseClauseList()) ;

    // Traverse config item list
    TraverseArray(node.GetConfigurationItemList()) ;

    // Don't traverse local scope

    // Traverse blcok label
    TraverseNode(node.BlockLabel()) ;

    // Don't traverse block range, since it is set during elaboration
}

void VhdlVisitor::VHDL_VISIT(VhdlComponentConfiguration, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlConfigurationItem, node) ;

    // Traverse component spec
    TraverseNode(node.GetComponentSpec()) ;

    // Traverse binding
    TraverseNode(node.GetBinding()) ;

    // Traverse block configuration
    TraverseNode(node.GetBlockConfiguration()) ;

    // Don't traverse local scope
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlDeclaration.h */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlDeclaration, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlScalarTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse range
    TraverseNode(node.GetScalarRange()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlArrayTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse index constraints
    TraverseArray(node.GetIndexConstraint()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlRecordTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse element decl list
    TraverseArray(node.GetElementDeclList()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlProtectedTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse element decl list
    TraverseArray(node.GetDeclList()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlProtectedTypeDefBody, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse element decl list
    TraverseArray(node.GetDeclList()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlAccessTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlFileTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse file type mark
    TraverseNode(node.GetFileTypeMark()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlEnumerationTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse enum literal list
    TraverseArray(node.GetEnumLiteralList()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlPhysicalTypeDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeDef, node) ;

    // Traverse range
    TraverseNode(node.GetRangeConstraint()) ;

    // Traverse physical unit decl list
    TraverseArray(node.GetPhysicalUnitDeclList()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlElementDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse id list
    TraverseArray(node.GetIds()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlPhysicalUnitDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse physical literal
    TraverseNode(node.GetPhysicalLiteral()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlUseClause, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse selected name list
    TraverseArray(node.GetSelectedNameList()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlContextReference, node)
{
    // Vhdl 2008 LRM section 13.4
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse selected name list
    TraverseArray(node.GetSelectedNameList()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlLibraryClause, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id list
    TraverseArray(node.GetIds()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlInterfaceDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id list
    TraverseArray(node.GetIds()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;

    // Traverse initial assignment
    TraverseNode(node.GetInitAssign()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSubprogramDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse specification
    TraverseNode(node.GetSubprogramSpec()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSubprogramBody, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse specification
    TraverseNode(node.GetSubprogramSpec()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse statements
    TraverseArray(node.GetStatementPart()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSubtypeDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlFullTypeDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse type def
    TraverseNode(node.GetTypeDef()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlIncompleteTypeDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlConstantDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse ids
    TraverseArray(node.GetIds()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;

    // Traverse initial assignment
    TraverseNode(node.GetInitAssign()) ;

    // Traverse type
    TraverseNode(node.GetType()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSignalDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse ids
    TraverseArray(node.GetIds()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;

    // Traverse initial assignment
    TraverseNode(node.GetInitAssign()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlVariableDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse ids
    TraverseArray(node.GetIds()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;

    // Traverse initial assignment
    TraverseNode(node.GetInitAssign()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlFileDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse ids
    TraverseArray(node.GetIds()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;

    // Traverse file open info
    TraverseNode(node.GetFileOpenInfo()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlAliasDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse designator
    TraverseNode(node.GetDesignator()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;

    // Traverse alias target name
    TraverseNode(node.GetAliasTargetName()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlComponentDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse generic clause
    TraverseArray(node.GetGenericClause()) ;

    // Traverse port clause
    TraverseArray(node.GetPortClause()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlAttributeDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse type mark
    TraverseNode(node.GetTypeMark()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlAttributeSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse designator
    TraverseNode(node.GetDesignator()) ;

    // Traverse entity spec
    TraverseNode(node.GetEntitySpec()) ;

    // Traverse value
    TraverseNode(node.GetValue()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlConfigurationSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse component spec
    TraverseNode(node.GetComponentSpec()) ;

    // Traverse binding
    TraverseNode(node.GetBinding()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlDisconnectionSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse guarded signal spec
    TraverseNode(node.GetGuardedSignalSpec()) ;

    // Traverse 'after'
    TraverseNode(node.GetAfter()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlGroupTemplateDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse entity class entry list
    TraverseArray(node.GetEntityClassEntryList()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlGroupDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse group template name
    TraverseNode(node.GetGroupTemplateName()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSubprogInstantiationDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDeclaration, node) ;

    // Traverse specification
    TraverseNode(node.GetSpecification()) ;

    // Traverse uninstantiated subprogram name
    TraverseNode(node.GetUninstantiatedSubprogName()) ;

    // Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlInterfaceTypeDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlInterfaceDecl, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlInterfaceSubprogDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlInterfaceDecl, node) ;

    // Traverse subprogram spec
    TraverseNode(node.GetSubprogramSpec()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlInterfacePackageDecl, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlInterfaceDecl, node) ;

    // Traverse package instantiation decl
    //TraverseNode(node.GetPackageInstantiationDecl()) ;
}
/*---------------------------------------------------------*/
/* The following methods are declared in VhdlExpression.h */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlDiscreteRange, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlExpression, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDiscreteRange, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlSubtypeIndication, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlExplicitSubtypeIndication, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSubtypeIndication, node) ;

    // Traverse resolution function
    TraverseNode(node.GetResolutionFunction()) ;

    // Traverse type mark
    TraverseNode(node.GetTypeMark()) ;

    // Traverse range constraint
    TraverseNode(node.GetRangeConstraint()) ;

    // Don't traverse back-pointer to range type
}

void VhdlVisitor::VHDL_VISIT(VhdlRange, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDiscreteRange, node) ;

    // Traverse lhs
    TraverseNode(node.GetLeftExpression()) ;

    // Traverse rhs
    TraverseNode(node.GetRightExpression()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlBox, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDiscreteRange, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlAssocElement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse formal part
    TraverseNode(node.FormalPart()) ;

    // Traverse actaul part
    TraverseNode(node.ActualPart()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlInertialElement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse actaul part
    TraverseNode(node.ActualPart()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlRecResFunctionElement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse formal part
    TraverseNode(node.RecordIdName()) ;

    // Traverse actaul part
    TraverseNode(node.RecordResFunction()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlOperator, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse lhs
    TraverseNode(node.GetLeftExpression()) ;

    // Traverse rhs
    TraverseNode(node.GetRightExpression()) ;

    // Traverse operator id
    TraverseNode(node.GetOperator()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlAllocator, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse subtype
    TraverseNode(node.GetSubtype()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlAggregate, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse element association list
    TraverseArray(node.GetElementAssocList()) ;

    // Traverse aggregate type
    TraverseNode(node.GetAggregateType()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlQualifiedExpression, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse prefix
    TraverseNode(node.GetPrefix()) ;

    // Traverse aggregate
    TraverseNode(node.GetAggregate()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlElementAssoc, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse choices
    TraverseArray(node.GetChoices()) ;

    // Traverse expression
    TraverseNode(node.GetExpression()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlWaveformElement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;

    // Traverse value
    TraverseNode(node.GetValue()) ;

    // Traverse after
    TraverseNode(node.GetAfter()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlDefault, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlExpression, node) ;
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlIdDef.h       */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlIdDef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Don't traverse back-pointer to type

    // Don't traverse attributes (they're elaboration specific)
}

void VhdlVisitor::VHDL_VISIT(VhdlLibraryId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlGroupId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlGroupTemplateId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlAttributeId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlComponentId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Traverse ports
    TraverseArray(node.GetPorts()) ;

    // Traverse generics
    TraverseArray(node.GetGenerics()) ;

    // Don't traverse scope

    // Don't traverse back-pointer to component declaration
}

void VhdlVisitor::VHDL_VISIT(VhdlAliasId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointer to target name

    // Don't traverse back-pointer to target id
}

void VhdlVisitor::VHDL_VISIT(VhdlFileId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlVariableId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlSignalId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointer to res function
}

void VhdlVisitor::VHDL_VISIT(VhdlConstantId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointer to declarations
}

void VhdlVisitor::VHDL_VISIT(VhdlTypeId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse list

    // Don't traverse scope
}

void VhdlVisitor::VHDL_VISIT(VhdlGenericTypeId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeId, node) ;

    // Don't traverse list

    // Don't traverse scope
}

void VhdlVisitor::VHDL_VISIT(VhdlUniversalInteger, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeId, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlUniversalReal, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeId, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlAnonymousType, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeId, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlSubtypeId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointer to res function
}

void VhdlVisitor::VHDL_VISIT(VhdlSubprogramId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers to args

    // Don't traverse back-pointer to spec

    // Don't traverse back-pointer to body
}

void VhdlVisitor::VHDL_VISIT(VhdlOperatorId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSubprogramId, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlInterfaceId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointer to res function
}

void VhdlVisitor::VHDL_VISIT(VhdlEnumerationId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlElementId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlPhysicalUnitId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlEntityId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlArchitectureId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlConfigurationId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlPackageId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlPackageBodyId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlContextId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlLabelId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlProtectedTypeId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeId, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlProtectedTypeBodyId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTypeId, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlBlockId, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlLabelId, node) ;

    // Don't traverse back-pointers
}

void VhdlVisitor::VHDL_VISIT(VhdlPackageInstElement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIdDef, node) ;

    // Don't traverse back-pointers
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlMisc.h        */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlSignature, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse type mark list
    TraverseArray(node.GetTypeMarklist()) ;

    // Traverse return type
    TraverseNode(node.GetReturnType()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlFileOpenInfo, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse file open
    TraverseNode(node.GetFileOpen()) ;

    // Traverse logical name
    TraverseNode(node.GetFileLogicalName()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlBindingIndication, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse entity aspect
    TraverseNode(node.GetEntityAspectNameNode()) ;

    // Traverse generic map aspect
    TraverseArray(node.GetGenericBinding()) ;

    // Traverse port map aspect
    TraverseArray(node.GetPortBinding()) ;

    // Traverse IdDef for the primary unit (back pointer)
    TraverseNode(node.EntityAspect()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlIterScheme, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlWhileScheme, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIterScheme, node) ;

    // Traverse condition
    TraverseNode(node.GetCondition()) ;

    // Traverse exit label
    TraverseNode(node.GetExitLabel()) ;

    TraverseNode(node.GetNextLabel()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlForScheme, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIterScheme, node) ;

    // Traverse id
    TraverseNode(node.GetId()) ;

    // Traverse range
    TraverseNode(node.GetRange()) ;

    // Traverse iterator id
    TraverseNode(node.GetIterId()) ;

    // Traverse exit label
    TraverseNode(node.GetExitLabel()) ;

    // Traverse next label
    TraverseNode(node.GetNextLabel()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlIfScheme, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIterScheme, node) ;

    // Traverse condition
    TraverseNode(node.GetCondition()) ;

    // Traverse alternative label
    TraverseNode(node.GetAlternativeLabel()) ;
    TraverseNode(node.GetAlternativeLabelId()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlElsifElseScheme, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIfScheme, node) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlCaseItemScheme, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlIterScheme, node) ;

    // Traverse choices
    TraverseArray(node.GetChoices()) ;

    // Traverse alternative label
    TraverseNode(node.GetAlternativeLabel()) ;
    TraverseNode(node.GetAlternativeLabelId()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlDelayMechanism, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlTransport, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDelayMechanism, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlInertialDelay, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDelayMechanism, node) ;

    // Traverse reject expression
    TraverseNode(node.GetRejectExpression()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlOptions, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse delay mechanism
    TraverseNode(node.GetDelayMechanism()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlConditionalWaveform, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse waveforms
    TraverseArray(node.GetWaveform()) ;

    // Traverse when condition
    TraverseNode(node.Condition()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlConditionalExpression, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse waveforms
    TraverseNode(node.GetExpression()) ;

    // Traverse when condition
    TraverseNode(node.Condition()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlSelectedWaveform, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse waveforms
    TraverseArray(node.GetWaveform()) ;

    // Traverse when choices
    TraverseArray(node.GetWhenChoices()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSelectedExpression, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse waveforms
    TraverseNode(node.GetExpression()) ;

    // Traverse when choices
    TraverseArray(node.GetWhenChoices()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlBlockGenerics, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse generic clause
    TraverseArray(node.GetGenericClause()) ;

    // Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;

    // Don't traverse back-pointer to block label.
}

void VhdlVisitor::VHDL_VISIT(VhdlBlockPorts, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse port clause
    TraverseArray(node.GetPortClause()) ;

    // Traverse port map aspect
    TraverseArray(node.GetPortMapAspect()) ;

    // Don't traverse back-pointer to block label.
}

void VhdlVisitor::VHDL_VISIT(VhdlCaseStatementAlternative, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse choices
    TraverseArray(node.GetChoices()) ;

    // Traverse statements
    TraverseArray(node.GetStatements()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlElsif, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse condition
    TraverseNode(node.Condition()) ;

    // Traverse statements
    TraverseArray(node.GetStatements()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlEntityClassEntry, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing left to traverse
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlName.h        */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlName, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSubtypeIndication, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlDesignator, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Don't traverse back-pointer to single id
}

void VhdlVisitor::VHDL_VISIT(VhdlLiteral, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlAll, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignator, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlOthers, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignator, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlUnaffected, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignator, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlInteger, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlLiteral, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlReal, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlLiteral, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlStringLiteral, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignator, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlBitStringLiteral, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStringLiteral, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlCharacterLiteral, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignator, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlPhysicalLiteral, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlLiteral, node) ;

    // Traverse value
    TraverseNode(node.GetValueExpr()) ;

    // Traverse unit
    TraverseNode(node.GetUnit()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlNull, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlLiteral, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlIdRef, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlDesignator, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlArrayResFunction, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse resolution function
    TraverseNode(node.GetResolutionFunction()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlRecordResFunction, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse assocation list
    TraverseArray(node.GetResFunctionList()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSelectedName, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse prefix
    TraverseNode(node.GetPrefix()) ;

    // Traverse suffix
    TraverseNode(node.GetSuffix()) ;

    // Don't traverse back-pointer to unique id
}

void VhdlVisitor::VHDL_VISIT(VhdlIndexedName, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse prefix
    TraverseNode(node.GetPrefix()) ;

    // Traverse assocation list
    TraverseArray(node.GetAssocList()) ;

    // Don't traverse back-pointer to prefix id
}

void VhdlVisitor::VHDL_VISIT(VhdlSignaturedName, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse name
    TraverseNode(node.GetName()) ;

    // Traverse signature
    TraverseNode(node.GetSignature()) ;

    // Don't traverse back-pointer to unique id
}

void VhdlVisitor::VHDL_VISIT(VhdlAttributeName, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse prefix
    TraverseNode(node.GetPrefix()) ;

    // Traverse designator
    TraverseNode(node.GetDesignator()) ;

    // Traverse expression
    TraverseNode(node.GetExpression()) ;

    // Don't traverse back-pointer to prefix type

    // Don't traverse back-pointer to result type
}

void VhdlVisitor::VHDL_VISIT(VhdlOpen, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlEntityAspect, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse name
    TraverseNode(node.GetName()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlInstantiatedUnit, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse name
    TraverseNode(node.GetName()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlExternalName, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlName, node) ;

    // Traverse name
    TraverseNode(node.GetExtPathName()) ;

    // Traverse subtype indication
    TraverseNode(node.GetSubtypeIndication()) ;
}
/*---------------------------------------------------------*/
/* The following methods are declared in VhdlSpecification.h */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlSpecification, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlProcedureSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSpecification, node) ;

    // Traverse designator
    TraverseNode(node.GetDesignator()) ;

    // Vhdl 2008 LRM section 4.7 - Traverse generic clause
    TraverseArray(node.GetGenericClause()) ;

    // Vhdl 2008 LRM section 4.7 - Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;

    // Traverse formal param list
    TraverseArray(node.GetFormalParamList()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlFunctionSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSpecification, node) ;

    // Traverse designator
    TraverseNode(node.GetDesignator()) ;

    // Vhdl 2008 LRM section 4.7 - Traverse generic clause
    TraverseArray(node.GetGenericClause()) ;

    // Vhdl 2008 LRM section 4.7 - Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;

    // Traverse formal param list
    TraverseArray(node.GetFormalParamList()) ;

    // Traverse return type
    TraverseNode(node.GetReturnType()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlComponentSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSpecification, node) ;

    // Traverse id list
    TraverseArray(node.GetIds()) ;

    // Traverse name
    TraverseNode(node.GetName()) ;

    // Don't traverse back-pointer to component id

    // Don't traverse present scope
}

void VhdlVisitor::VHDL_VISIT(VhdlGuardedSignalSpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSpecification, node) ;

    // Traverse signal list
    TraverseArray(node.GetSignalList()) ;

    // Traverse type mark
    TraverseNode(node.GetTypeMark()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlEntitySpec, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlSpecification, node) ;

    // Traverse entity name list
    TraverseArray(node.GetEntityNameList()) ;

    // Don't traverse all ids in entity_name_list
}

/*---------------------------------------------------------*/
/* The following methods are declared in VhdlStatement.h   */
/*---------------------------------------------------------*/

void VhdlVisitor::VHDL_VISIT(VhdlStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlTreeNode, node) ;

    // Traverse label
    TraverseNode(node.GetLabel()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlNullStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Nothing left to traverse
}

void VhdlVisitor::VHDL_VISIT(VhdlReturnStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse expression
    TraverseNode(node.GetExpression()) ;

    // Don't traverse back-pointer to owner
}

void VhdlVisitor::VHDL_VISIT(VhdlExitStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse condition
    TraverseNode(node.GetCondition()) ;

    // Don't traverse back-pointer to target label
}

void VhdlVisitor::VHDL_VISIT(VhdlNextStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse expression
    TraverseNode(node.GetCondition()) ;

    // Don't traverse back-pointer to target label
}

void VhdlVisitor::VHDL_VISIT(VhdlLoopStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse iter scheme
    TraverseNode(node.GetIterScheme()) ;

    // Traverse statements
    TraverseArray(node.GetStatements()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlCaseStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse expressoin
    TraverseNode(node.GetExpression()) ;

    // Traverse alternatives
    TraverseArray(node.GetAlternatives()) ;

    // Don't traverse back-pointer to expression type
}

void VhdlVisitor::VHDL_VISIT(VhdlIfStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse if condition
    TraverseNode(node.GetIfCondition()) ;

    // Traverse if statements
    TraverseArray(node.GetIfStatements()) ;

    // Traverse elseif statements
    TraverseArray(node.GetElsifList()) ;

    // Traverse else statements
    TraverseArray(node.GetElseStatments()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlVariableAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse value
    TraverseNode(node.GetValue()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSignalAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse delay mechanism
    TraverseNode(node.GetDelayMechanism()) ;

    // Traverse waveforms
    TraverseArray(node.GetWaveform()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlForceAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse expression
    TraverseNode(node.GetExpression()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlReleaseAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlWaitStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse sensitivity clause
    TraverseArray(node.GetSensitivityClause()) ;

    // Traverse condition clause
    TraverseNode(node.UntilClause()) ;

    // Traverse timeout clause
    TraverseNode(node.GetTimeoutClause()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlReportStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse report
    TraverseNode(node.GetReport()) ;

    // Traverse severity
    TraverseNode(node.GetSeverity()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlAssertionStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse condition
    TraverseNode(node.GetCondition()) ;

    // Traverse report
    TraverseNode(node.GetReport()) ;

    // Traverse severity
    TraverseNode(node.GetSeverity()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlProcessStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse sensitivity list
    TraverseArray(node.GetSensitivityList()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse statements
    TraverseArray(node.GetStatementPart()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlBlockStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse guard
    TraverseNode(node.GetGuard()) ;

    // Traverse generics
    TraverseNode(node.GetGenerics()) ;

    // Traverse ports
    TraverseNode(node.GetPorts()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse statements
    TraverseArray(node.GetStatements()) ;

    // Don't traverse local scope

    // Don't traverse guard id, eventhough it's owned by VhdlBlockStatement
}

void VhdlVisitor::VHDL_VISIT(VhdlGenerateStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse iter scheme
    TraverseNode(node.GetScheme()) ;

    // Traverse declarations
    TraverseArray(node.GetDeclPart()) ;

    // Traverse statements
    TraverseArray(node.GetStatementPart()) ;

    // Don't traverse local scope
}

void VhdlVisitor::VHDL_VISIT(VhdlProcedureCallStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse call
    TraverseNode(node.GetCall()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlSelectedSignalAssignment, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse case expression
    TraverseNode(node.GetExpression()) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse options
    TraverseNode(node.GetOptions()) ;

    // Traverse delay mechanism
    TraverseNode(node.GetDelayMechanism()) ;

    // Traverse selected waveforms
    TraverseArray(node.GetSelectedWaveforms()) ;

    // Don't traverse back-pointer to expression type

    // Don't traverse back-pointer to guard id
}

void VhdlVisitor::VHDL_VISIT(VhdlSelectedVariableAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse case expression
    TraverseNode(node.GetExpression()) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse selected waveforms
    TraverseArray(node.GetSelectedExpressions()) ;

    // Don't traverse back-pointer to expression type
}
void VhdlVisitor::VHDL_VISIT(VhdlSelectedForceAssignment, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse case expression
    TraverseNode(node.GetExpression()) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse selected waveforms
    TraverseArray(node.GetSelectedExpressions()) ;

    // Don't traverse back-pointer to expression type
}

void VhdlVisitor::VHDL_VISIT(VhdlConditionalSignalAssignment, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse options
    TraverseNode(node.GetOptions()) ;

    // Traverse delay mechanism
    TraverseNode(node.GetDelayMechanism()) ;

    // Traverse conditional waveforms
    TraverseArray(node.GetConditionalWaveforms()) ;

    // Don't traverse back-pointer to guard id
}
void VhdlVisitor::VHDL_VISIT(VhdlConditionalForceAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse conditional expressions
    TraverseArray(node.GetConditionalExpressions()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlConditionalVariableAssignmentStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse target
    TraverseNode(node.GetTarget()) ;

    // Traverse conditional expressions
    TraverseArray(node.GetConditionalExpressions()) ;
}

void VhdlVisitor::VHDL_VISIT(VhdlComponentInstantiationStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse instantiated unit
    TraverseNode(node.GetInstantiatedUnitNameNode()) ;

    // Traverse generic map aspect
    TraverseArray(node.GetGenericMapAspect()) ;

    // Traverse port map aspect
    TraverseArray(node.GetPortMapAspect()) ;

    // Don't traverse back-pointer to unit id
}
void VhdlVisitor::VHDL_VISIT(VhdlIfElsifGenerateStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlGenerateStatement, node) ;

    // Traverse elsif list
    TraverseArray(node.GetElsifList()) ;

    // Traverse else statement
    TraverseNode(node.GetElseStatement()) ;
}
void VhdlVisitor::VHDL_VISIT(VhdlCaseGenerateStatement, node)
{
    // Call of Base class Visit
    VHDL_VISIT_NODE(VhdlStatement, node) ;

    // Traverse case expression
    TraverseNode(node.GetExpression()) ;

    // Traverse case stemenet alternatives
    TraverseArray(node.GetAlternatives()) ;

    // Don't traverse back-pointer to expression type
}

/*----------------------------------------------------*/

void VhdlVisitor::TraverseNode(VhdlTreeNode *node)
{
    if (node) node->Accept(*this) ;
}

void VhdlVisitor::TraverseAttributes(const Map *attr)
{
    if (!attr) return;

    MapIter mi ;
    VhdlExpression *expr = 0 ;
    FOREACH_MAP_ITEM(attr, mi, 0, &expr) {
        if (expr) expr->Accept(*this) ;
    }
}

void VhdlVisitor::TraverseArray(const Array *array)
{
    if (!array) return;

    VhdlTreeNode * node_item = 0;
    unsigned int i = 0;
    FOREACH_ARRAY_ITEM(array, i, node_item) {
        if (node_item) node_item->Accept(*this) ;
    }
}

/////////////////////  Accept Routines  /////////////////////////////

// The following class definitions can be found in VhdlTreeNode.h
void VhdlTreeNode::Accept(VhdlVisitor &v)                        { VHDL_ACCEPT(VhdlTreeNode, v) ; }
void VhdlCommentNode::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlCommentNode, v) ; }

// The following class definitions can be found in VhdlUnits.h
void VhdlDesignUnit::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlDesignUnit, v) ; }
void VhdlPrimaryUnit::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlPrimaryUnit, v) ; }
void VhdlSecondaryUnit::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlSecondaryUnit, v) ; }
void VhdlEntityDecl::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlEntityDecl, v) ; }
void VhdlConfigurationDecl::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlConfigurationDecl, v) ; }
void VhdlPackageDecl::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlPackageDecl, v) ; }
void VhdlContextDecl::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlContextDecl, v) ; }
void VhdlPackageInstantiationDecl::Accept(VhdlVisitor &v)        { VHDL_ACCEPT(VhdlPackageInstantiationDecl, v) ; }
void VhdlArchitectureBody::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlArchitectureBody, v) ; }
void VhdlPackageBody::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlPackageBody, v) ; }

// The following class definitions can be found in VhdlConfiguration.h
void VhdlConfigurationItem::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlConfigurationItem, v) ; }
void VhdlBlockConfiguration::Accept(VhdlVisitor &v)              { VHDL_ACCEPT(VhdlBlockConfiguration, v) ; }
void VhdlComponentConfiguration::Accept(VhdlVisitor &v)          { VHDL_ACCEPT(VhdlComponentConfiguration, v) ; }

// The following class definitions can be found in VhdlDeclaration.h
void VhdlDeclaration::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlDeclaration, v) ; }
void VhdlTypeDef::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlTypeDef, v) ; }
void VhdlScalarTypeDef::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlScalarTypeDef, v) ; }
void VhdlArrayTypeDef::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlArrayTypeDef, v) ; }
void VhdlRecordTypeDef::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlRecordTypeDef, v) ; }
void VhdlProtectedTypeDef::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlProtectedTypeDef, v) ; }
void VhdlProtectedTypeDefBody::Accept(VhdlVisitor &v)            { VHDL_ACCEPT(VhdlProtectedTypeDefBody, v) ; }
void VhdlAccessTypeDef::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlAccessTypeDef, v) ; }
void VhdlFileTypeDef::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlFileTypeDef, v) ; }
void VhdlEnumerationTypeDef::Accept(VhdlVisitor &v)              { VHDL_ACCEPT(VhdlEnumerationTypeDef, v) ; }
void VhdlPhysicalTypeDef::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlPhysicalTypeDef, v) ; }
void VhdlElementDecl::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlElementDecl, v) ; }
void VhdlPhysicalUnitDecl::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlPhysicalUnitDecl, v) ; }
void VhdlUseClause::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlUseClause, v) ; }
void VhdlContextReference::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlContextReference, v) ; }
void VhdlLibraryClause::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlLibraryClause, v) ; }
void VhdlInterfaceDecl::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlInterfaceDecl, v) ; }
void VhdlSubprogramDecl::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlSubprogramDecl, v) ; }
void VhdlSubprogramBody::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlSubprogramBody, v) ; }
void VhdlSubtypeDecl::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlSubtypeDecl, v) ; }
void VhdlFullTypeDecl::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlFullTypeDecl, v) ; }
void VhdlIncompleteTypeDecl::Accept(VhdlVisitor &v)              { VHDL_ACCEPT(VhdlIncompleteTypeDecl, v) ; }
void VhdlConstantDecl::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlConstantDecl, v) ; }
void VhdlSignalDecl::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlSignalDecl, v) ; }
void VhdlVariableDecl::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlVariableDecl, v) ; }
void VhdlFileDecl::Accept(VhdlVisitor &v)                        { VHDL_ACCEPT(VhdlFileDecl, v) ; }
void VhdlAliasDecl::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlAliasDecl, v) ; }
void VhdlComponentDecl::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlComponentDecl, v) ; }
void VhdlAttributeDecl::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlAttributeDecl, v) ; }
void VhdlAttributeSpec::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlAttributeSpec, v) ; }
void VhdlConfigurationSpec::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlConfigurationSpec, v) ; }
void VhdlDisconnectionSpec::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlDisconnectionSpec, v) ; }
void VhdlGroupTemplateDecl::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlGroupTemplateDecl, v) ; }
void VhdlGroupDecl::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlGroupDecl, v) ; }
void VhdlSubprogInstantiationDecl::Accept(VhdlVisitor &v)        { VHDL_ACCEPT(VhdlSubprogInstantiationDecl, v) ; }
void VhdlInterfaceTypeDecl::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlInterfaceTypeDecl, v) ; }
void VhdlInterfaceSubprogDecl::Accept(VhdlVisitor &v)            { VHDL_ACCEPT(VhdlInterfaceSubprogDecl, v) ; }
void VhdlInterfacePackageDecl::Accept(VhdlVisitor &v)            { VHDL_ACCEPT(VhdlInterfacePackageDecl, v) ; }

// The following class definitions can be found in VhdlExpression.h
void VhdlExpression::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlExpression, v) ; }
void VhdlDiscreteRange::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlDiscreteRange, v) ; }
void VhdlSubtypeIndication::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlSubtypeIndication, v) ; }
void VhdlExplicitSubtypeIndication::Accept(VhdlVisitor &v)       { VHDL_ACCEPT(VhdlExplicitSubtypeIndication, v) ; }
void VhdlRange::Accept(VhdlVisitor &v)                           { VHDL_ACCEPT(VhdlRange, v) ; }
void VhdlBox::Accept(VhdlVisitor &v)                             { VHDL_ACCEPT(VhdlBox, v) ; }
void VhdlAssocElement::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlAssocElement, v) ; }
void VhdlInertialElement::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlInertialElement, v) ; }
void VhdlRecResFunctionElement::Accept(VhdlVisitor &v)           { VHDL_ACCEPT(VhdlRecResFunctionElement, v) ; }
void VhdlOperator::Accept(VhdlVisitor &v)                        { VHDL_ACCEPT(VhdlOperator, v) ; }
void VhdlAllocator::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlAllocator, v) ; }
void VhdlAggregate::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlAggregate, v) ; }
void VhdlQualifiedExpression::Accept(VhdlVisitor &v)             { VHDL_ACCEPT(VhdlQualifiedExpression, v) ; }
void VhdlElementAssoc::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlElementAssoc, v) ; }
void VhdlWaveformElement::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlWaveformElement, v) ; }
void VhdlDefault::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlDefault, v) ; }

// The following class definitions can be found in VhdlIdDef.h
void VhdlIdDef::Accept(VhdlVisitor &v)                           { VHDL_ACCEPT(VhdlIdDef, v) ; }
void VhdlLibraryId::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlLibraryId, v) ; }
void VhdlGroupId::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlGroupId, v) ; }
void VhdlGroupTemplateId::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlGroupTemplateId, v) ; }
void VhdlAttributeId::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlAttributeId, v) ; }
void VhdlComponentId::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlComponentId, v) ; }
void VhdlAliasId::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlAliasId, v) ; }
void VhdlFileId::Accept(VhdlVisitor &v)                          { VHDL_ACCEPT(VhdlFileId, v) ; }
void VhdlVariableId::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlVariableId, v) ; }
void VhdlSignalId::Accept(VhdlVisitor &v)                        { VHDL_ACCEPT(VhdlSignalId, v) ; }
void VhdlConstantId::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlConstantId, v) ; }
void VhdlTypeId::Accept(VhdlVisitor &v)                          { VHDL_ACCEPT(VhdlTypeId, v) ; }
void VhdlGenericTypeId::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlGenericTypeId, v) ; }
void VhdlUniversalInteger::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlUniversalInteger, v) ; }
void VhdlUniversalReal::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlUniversalReal, v) ; }
void VhdlAnonymousType::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlAnonymousType, v) ; }
void VhdlSubtypeId::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlSubtypeId, v) ; }
void VhdlSubprogramId::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlSubprogramId, v) ; }
void VhdlOperatorId::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlOperatorId, v) ; }
void VhdlInterfaceId::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlInterfaceId, v) ; }
void VhdlEnumerationId::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlEnumerationId, v) ; }
void VhdlElementId::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlElementId, v) ; }
void VhdlPhysicalUnitId::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlPhysicalUnitId, v) ; }
void VhdlEntityId::Accept(VhdlVisitor &v)                        { VHDL_ACCEPT(VhdlEntityId, v) ; }
void VhdlArchitectureId::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlArchitectureId, v) ; }
void VhdlConfigurationId::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlConfigurationId, v) ; }
void VhdlPackageId::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlPackageId, v) ; }
void VhdlPackageBodyId::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlPackageBodyId, v) ; }
void VhdlContextId::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlContextId, v) ; }
void VhdlLabelId::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlLabelId, v) ; }
void VhdlBlockId::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlBlockId, v) ; }
void VhdlProtectedTypeId::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlProtectedTypeId, v) ; }
void VhdlProtectedTypeBodyId::Accept(VhdlVisitor &v)             { VHDL_ACCEPT(VhdlProtectedTypeBodyId, v) ; }
void VhdlPackageInstElement::Accept(VhdlVisitor &v)             { VHDL_ACCEPT(VhdlPackageInstElement, v) ; }

// The following class definitions can be found in VhdlMisc.h
void VhdlSignature::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlSignature, v) ; }
void VhdlFileOpenInfo::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlFileOpenInfo, v) ; }
void VhdlBindingIndication::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlBindingIndication, v) ; }
void VhdlIterScheme::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlIterScheme, v) ; }
void VhdlWhileScheme::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlWhileScheme, v) ; }
void VhdlForScheme::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlForScheme, v) ; }
void VhdlIfScheme::Accept(VhdlVisitor &v)                        { VHDL_ACCEPT(VhdlIfScheme, v) ; }
void VhdlElsifElseScheme::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlElsifElseScheme, v) ; }
void VhdlCaseItemScheme::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlCaseItemScheme, v) ; }
void VhdlDelayMechanism::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlDelayMechanism, v) ; }
void VhdlTransport::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlTransport, v) ; }
void VhdlInertialDelay::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlInertialDelay, v) ; }
void VhdlOptions::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlOptions, v) ; }
void VhdlConditionalWaveform::Accept(VhdlVisitor &v)             { VHDL_ACCEPT(VhdlConditionalWaveform, v) ; }
void VhdlConditionalExpression::Accept(VhdlVisitor &v)           { VHDL_ACCEPT(VhdlConditionalExpression, v) ; }
void VhdlSelectedWaveform::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlSelectedWaveform, v) ; }
void VhdlSelectedExpression::Accept(VhdlVisitor &v)              { VHDL_ACCEPT(VhdlSelectedExpression, v) ; }
void VhdlBlockGenerics::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlBlockGenerics, v) ; }
void VhdlBlockPorts::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlBlockPorts, v) ; }
void VhdlCaseStatementAlternative::Accept(VhdlVisitor &v)        { VHDL_ACCEPT(VhdlCaseStatementAlternative, v) ; }
void VhdlElsif::Accept(VhdlVisitor &v)                           { VHDL_ACCEPT(VhdlElsif, v) ; }
void VhdlEntityClassEntry::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlEntityClassEntry, v) ; }

// The following class definitions can be found in VhdlName.h
void VhdlName::Accept(VhdlVisitor &v)                            { VHDL_ACCEPT(VhdlName, v) ; }
void VhdlDesignator::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlDesignator, v) ; }
void VhdlLiteral::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlLiteral, v) ; }
void VhdlAll::Accept(VhdlVisitor &v)                             { VHDL_ACCEPT(VhdlAll, v) ; }
void VhdlOthers::Accept(VhdlVisitor &v)                          { VHDL_ACCEPT(VhdlOthers, v) ; }
void VhdlUnaffected::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlUnaffected, v) ; }
void VhdlInteger::Accept(VhdlVisitor &v)                         { VHDL_ACCEPT(VhdlInteger, v) ; }
void VhdlReal::Accept(VhdlVisitor &v)                            { VHDL_ACCEPT(VhdlReal, v) ; }
void VhdlStringLiteral::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlStringLiteral, v) ; }
void VhdlBitStringLiteral::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlBitStringLiteral, v) ; }
void VhdlCharacterLiteral::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlCharacterLiteral, v) ; }
void VhdlPhysicalLiteral::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlPhysicalLiteral, v) ; }
void VhdlNull::Accept(VhdlVisitor &v)                            { VHDL_ACCEPT(VhdlNull, v) ; }
void VhdlIdRef::Accept(VhdlVisitor &v)                           { VHDL_ACCEPT(VhdlIdRef, v) ; }
void VhdlArrayResFunction::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlArrayResFunction, v) ; }
void VhdlRecordResFunction::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlRecordResFunction, v) ; }
void VhdlSelectedName::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlSelectedName, v) ; }
void VhdlIndexedName::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlIndexedName, v) ; }
void VhdlSignaturedName::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlSignaturedName, v) ; }
void VhdlAttributeName::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlAttributeName, v) ; }
void VhdlOpen::Accept(VhdlVisitor &v)                            { VHDL_ACCEPT(VhdlOpen, v) ; }
void VhdlEntityAspect::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlEntityAspect, v) ; }
void VhdlInstantiatedUnit::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlInstantiatedUnit, v) ; }
void VhdlExternalName::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlExternalName, v) ; }

// The following class definitions can be found in VhdlSpecification.h
void VhdlSpecification::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlSpecification, v) ; }
void VhdlProcedureSpec::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlProcedureSpec, v) ; }
void VhdlFunctionSpec::Accept(VhdlVisitor &v)                    { VHDL_ACCEPT(VhdlFunctionSpec, v) ; }
void VhdlComponentSpec::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlComponentSpec, v) ; }
void VhdlGuardedSignalSpec::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlGuardedSignalSpec, v) ; }
void VhdlEntitySpec::Accept(VhdlVisitor &v)                      { VHDL_ACCEPT(VhdlEntitySpec, v) ; }

// The following class definitions can be found in VhdlStatement.h
void VhdlStatement::Accept(VhdlVisitor &v)                       { VHDL_ACCEPT(VhdlStatement, v) ; }
void VhdlNullStatement::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlNullStatement, v) ; }
void VhdlReturnStatement::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlReturnStatement, v) ; }
void VhdlExitStatement::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlExitStatement, v) ; }
void VhdlNextStatement::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlNextStatement, v) ; }
void VhdlLoopStatement::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlLoopStatement, v) ; }
void VhdlCaseStatement::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlCaseStatement, v) ; }
void VhdlIfStatement::Accept(VhdlVisitor &v)                     { VHDL_ACCEPT(VhdlIfStatement, v) ; }
void VhdlVariableAssignmentStatement::Accept(VhdlVisitor &v)     { VHDL_ACCEPT(VhdlVariableAssignmentStatement, v) ; }
void VhdlSignalAssignmentStatement::Accept(VhdlVisitor &v)       { VHDL_ACCEPT(VhdlSignalAssignmentStatement, v) ; }
void VhdlForceAssignmentStatement::Accept(VhdlVisitor &v)        { VHDL_ACCEPT(VhdlForceAssignmentStatement, v) ; }
void VhdlReleaseAssignmentStatement::Accept(VhdlVisitor &v)      { VHDL_ACCEPT(VhdlReleaseAssignmentStatement, v) ; }
void VhdlWaitStatement::Accept(VhdlVisitor &v)                   { VHDL_ACCEPT(VhdlWaitStatement, v) ; }
void VhdlReportStatement::Accept(VhdlVisitor &v)                 { VHDL_ACCEPT(VhdlReportStatement, v) ; }
void VhdlAssertionStatement::Accept(VhdlVisitor &v)              { VHDL_ACCEPT(VhdlAssertionStatement, v) ; }
void VhdlProcessStatement::Accept(VhdlVisitor &v)                { VHDL_ACCEPT(VhdlProcessStatement, v) ; }
void VhdlBlockStatement::Accept(VhdlVisitor &v)                  { VHDL_ACCEPT(VhdlBlockStatement, v) ; }
void VhdlGenerateStatement::Accept(VhdlVisitor &v)               { VHDL_ACCEPT(VhdlGenerateStatement, v) ; }
void VhdlProcedureCallStatement::Accept(VhdlVisitor &v)          { VHDL_ACCEPT(VhdlProcedureCallStatement, v) ; }
void VhdlSelectedSignalAssignment::Accept(VhdlVisitor &v)        { VHDL_ACCEPT(VhdlSelectedSignalAssignment, v) ; }
void VhdlSelectedVariableAssignmentStatement::Accept(VhdlVisitor &v) { VHDL_ACCEPT(VhdlSelectedVariableAssignmentStatement, v) ; }
void VhdlSelectedForceAssignment::Accept(VhdlVisitor &v) { VHDL_ACCEPT(VhdlSelectedForceAssignment, v) ; }
void VhdlConditionalSignalAssignment::Accept(VhdlVisitor &v)     { VHDL_ACCEPT(VhdlConditionalSignalAssignment, v) ; }
void VhdlConditionalForceAssignmentStatement::Accept(VhdlVisitor &v)    { VHDL_ACCEPT(VhdlConditionalForceAssignmentStatement, v) ; }
void VhdlConditionalVariableAssignmentStatement::Accept(VhdlVisitor &v) { VHDL_ACCEPT(VhdlConditionalVariableAssignmentStatement, v) ; }
void VhdlComponentInstantiationStatement::Accept(VhdlVisitor &v) { VHDL_ACCEPT(VhdlComponentInstantiationStatement, v) ; }
void VhdlIfElsifGenerateStatement::Accept(VhdlVisitor &v) { VHDL_ACCEPT(VhdlIfElsifGenerateStatement, v) ; }
void VhdlCaseGenerateStatement::Accept(VhdlVisitor &v) { VHDL_ACCEPT(VhdlCaseGenerateStatement, v) ; }

