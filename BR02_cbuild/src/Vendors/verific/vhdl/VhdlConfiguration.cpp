/*
 *
 * [ File Version : 1.59 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlConfiguration.h"

#include "Array.h"

#include "VhdlName.h"
#include "VhdlStatement.h"
#include "VhdlSpecification.h"
#include "VhdlDeclaration.h"
#include "VhdlMisc.h"
#include "VhdlUnits.h"
#include "VhdlIdDef.h"
#include "VhdlScope.h"
#include "VhdlValue_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlConfigurationItem::VhdlConfigurationItem() : VhdlTreeNode() { }
VhdlConfigurationItem::~VhdlConfigurationItem() { }

VhdlBlockConfiguration::VhdlBlockConfiguration(VhdlName *block_spec, Array *use_clause_list, Array *configuration_item_list, VhdlScope *local_scope)
  : VhdlConfigurationItem(),
    _block_spec(block_spec),
    _use_clause_list(use_clause_list),
    _configuration_item_list(configuration_item_list),
    _local_scope(local_scope),
    _block_label(0)
    ,_block_range(0)
{
    // Now that we process configurations during analyze time correctly,
    // all scopes are set properly already.
    // We can therefor resolve the block label right here and now.
    _block_label = _block_spec ? _block_spec->BlockSpec(): 0 ;
}
VhdlBlockConfiguration::~VhdlBlockConfiguration()
{
    // Delete the contents of the construct
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_use_clause_list,i,elem) delete elem ;
    delete _use_clause_list ;
    FOREACH_ARRAY_ITEM(_configuration_item_list,i,elem) delete elem ;
    delete _configuration_item_list ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
    // Delete the owner after the scope it owns
    delete _block_spec ;
    // Delete block range
    delete _block_range ;

    // No need to delete _block_label, since we don't own it.
}

VhdlComponentConfiguration::VhdlComponentConfiguration(VhdlSpecification *component_spec, VhdlBindingIndication *binding, VhdlBlockConfiguration *block_configuration, VhdlScope *local_scope)
  : VhdlConfigurationItem(),
    _component_spec(component_spec),
    _binding(binding),
    _block_configuration(block_configuration),
    _local_scope(local_scope)
{
    // In a component configuration specifies the 'incremental' binding of the instantiation labels in the component spec.
    // The instantiations happened already earlier (in the configured block) so call ResolveSpecs with 'incremental' flag :
    // ResolveSpecs was already done before the block_config was added.
    // if (_component_spec) (void) _component_spec->ResolveSpecs(_binding) ;

    // Block configuration handles itself.
}
VhdlComponentConfiguration::~VhdlComponentConfiguration()
{
    // Delete the content of the construct
    delete _binding ;
    delete _block_configuration ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
    // Delete the owner after the scope it owns
    delete _component_spec ;
}

/******************************************************************/
/*  Local scope access */
/******************************************************************/

VhdlScope *VhdlBlockConfiguration::LocalScope() const       { return _local_scope ; }
VhdlScope *VhdlComponentConfiguration::LocalScope() const   { return _local_scope ; }

/**************************************************************/
// Calculate Signature
/**************************************************************/

// VIPER #6032 : To create signature for configuration configuring an entity.
#define HASH_NAME(SIG, NAME)  (((SIG) << 5) + (SIG) + (Hash::StringCompare((NAME), 0)))
#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlConfigurationItem::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlBlockConfiguration::CalculateSignature() const
{
    unsigned long sig = VhdlConfigurationItem::CalculateSignature() ;
    if (_block_spec) sig = HASH_VAL(sig, _block_spec->CalculateSignature()) ;
    unsigned i ;
    VhdlUseClause *clause ;
    FOREACH_ARRAY_ITEM(_use_clause_list, i, clause) {
        if (clause) sig = HASH_VAL(sig, clause->CalculateSignature()) ;
    }
    VhdlConfigurationItem *item ;
    FOREACH_ARRAY_ITEM(_configuration_item_list, i, item) {
        if (item) sig = HASH_VAL(sig, item->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlComponentConfiguration::CalculateSignature() const
{
    unsigned long sig = VhdlConfigurationItem::CalculateSignature() ;
    if (_component_spec) sig = HASH_VAL(sig, _component_spec->CalculateSignature()) ;
    if (_binding) sig = HASH_VAL(sig, _binding->CalculateSignature()) ;
    if (_block_configuration) sig = HASH_VAL(sig, _block_configuration->CalculateSignature()) ;
    return sig ;
}

unsigned
VhdlBlockConfiguration::GetHashedNum(const char *prefix) const
{
    // Consider the name of instantiated entity while creating signature of configuration,
    // otherwise we will use existing unique number when content of configuration is same
    // for different instantiated entity.
    unsigned long sig = HASH_NAME(0, prefix) ; // Consider entity name
    sig = HASH_VAL(sig, CalculateSignature()) ; // Signature of configuration

    // Check whether this configuration is already used to configure entity 'prefix'
    MapItem *item = (_config_num_map) ? _config_num_map->GetItem((void*)sig): 0 ;
    if (item) { // Configuration is already used to configure entity 'prefix'
        return (unsigned)(unsigned long)item->Value() ;
    }
    // Configuration is configuring the entity 'prefix' for the first time.
    // Get an unique number and hash that
    if (!_config_num_map) _config_num_map = new Map(NUM_HASH) ;
    unsigned uniq_num = _config_num_map->Size() ;
    (void) _config_num_map->Insert((void*)sig, (void*)(unsigned long)uniq_num) ;
    return uniq_num ;
}

