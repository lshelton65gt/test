/*
 *
 * [ File Version : 1.234 - 2014/02/19 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlUnits.h"

#include <time.h>      // for timestamp
#include <cctype>      // for tolower on older gcc libs

#include "Set.h"
#include "Strings.h"
#include "Array.h"
#include "Message.h"

#include "vhdl_file.h" // for GetWorkLib
#include "vhdl_tokens.h"
#include "VhdlDeclaration.h"
#include "VhdlName.h"
#include "VhdlConfiguration.h"
#include "VhdlScope.h"
#include "VhdlIdDef.h"
#include "VhdlStatement.h"
#include "VhdlCopy.h"

#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
#include "TextBasedDesignMod.h"
#endif

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*********** Individual VhdlLibrary **************************/

VhdlLibrary::VhdlLibrary(const char *name)
  : _name(Strings::save(name)),
    _primary_units(STRING_HASH)
{
}

VhdlLibrary::~VhdlLibrary()
{
    // Also delete the primary units in here
    MapIter mi ;
    VhdlPrimaryUnit *pu ;

    // Delete the last ones first
    FOREACH_VHDL_PRIMARY_UNIT(this,mi,pu) {
        delete pu ;
    }

    Strings::free( _name ) ;
}

// VIPER #7361 : Added one default argument 'consider_mixed_case' to consider
// mixed language cases
VhdlPrimaryUnit *
VhdlLibrary::GetPrimUnit(const char *name, unsigned restore, unsigned consider_mixed_case) const
{
    // Without a name, return the last one compiled
    if (!name) {
        if (_primary_units.Size() == 0) return 0 ;
        return (VhdlPrimaryUnit*)_primary_units.GetValueAt(_primary_units.Size()-1) ;
    }

    // Check as-is :
    VhdlPrimaryUnit *result = (VhdlPrimaryUnit*)_primary_units.GetValue(name) ;
    if (result) return result ;

    if (consider_mixed_case) {
        // VIPER #7361 : Consider mixed language case under an argument
        // Check if maybe it's requested in mixed case :
        if (name[0]!='\\') { // It's NOT an escaped identifier
            char *downcased = Strings::strtolower(Strings::save(name)) ;
            result = (VhdlPrimaryUnit*)_primary_units.GetValue(downcased) ;
            Strings::free( downcased ) ;
            if (result) return result ;
        } else if (name[0] == '\\' && name[Strings::len(name) - 1] == '\\') {
            // Escaped name, search case sensitive
            // This is for matching \checker\ in entity instantiation with module checker
            char *unescaped_name = VhdlNode::UnescapeVhdlName(name) ;
            result = (VhdlPrimaryUnit*)_primary_units.GetValue(unescaped_name) ;
            Strings::free(unescaped_name) ;
            if (result) return result ;
        }
    }

    (void) restore ; // To prevent unused argument compile warning
    // Code here, only if this release does have VHDL binary save/restore.
    if (restore) {
        // If a library path exists for 'this' VhdlLibrary, search other library paths for
        // same paths, and search corresponding libraries appropriately.  (VIPER #2823)
        char *lib_path = (vhdl_file::GetAllLibraryPaths()) ? (char*)vhdl_file::GetAllLibraryPaths()->GetValue(Name()) : 0 ;
        if (lib_path) {
            MapIter mi ;
            char *lib_name, *lib_path2;
            // First look between "associated" library paths
            FOREACH_MAP_ITEM(vhdl_file::GetAllLibraryPaths(), mi, &lib_name, &lib_path2) {
                if (!Strings::compare(lib_path, lib_path2)) continue ;
                VhdlLibrary *lib = vhdl_file::GetLibrary(lib_name) ;
                if (lib && (lib != this)) {
                    VhdlPrimaryUnit *unit = lib->GetPrimUnit(name, 0) ;
                    if (unit) return unit ;
                }
            }
            // Now look against default library path
            char *default_path = vhdl_file::GetDefaultLibraryPath() ;
            if (default_path && Strings::compare(lib_path, default_path)) {
                VhdlLibrary *lib ;
                FOREACH_VHDL_LIBRARY(mi, lib) {
                    if (!lib || (lib == this)) continue ;
                    if (vhdl_file::GetAllLibraryPaths() && vhdl_file::GetAllLibraryPaths()->GetValue(lib->Name())) continue ;
                    VhdlPrimaryUnit *unit = lib->GetPrimUnit(name, 0) ;
                    if (unit) return unit ;
                }
            }
        }

        // Viper 3546 : vhdl_file::Restore resets the error count with undesired behaviour
        // VIPER 5189 : save/restore flow has been made independent of error count. So now we can always call it :
        //if (Message::ErrorCount()) return 0 ;

        // Attempt to do a restore of unit from a VDB file (VIPER #2042)
        if (vhdl_file::Restore(Name(), name, 1 /*silent*/)) {
            // Now return the restored unit
            return GetPrimUnit(name) ;
        }
    }

    return 0 ; // Cant find it
}

VhdlPrimaryUnit *
VhdlLibrary::GetPrimUnitAnywhere(const char *name, unsigned restore) const
{
    // Default binding search of a design unit by name, in this library :
    // Check in this library first
    VhdlPrimaryUnit *unit = GetPrimUnit(name, restore) ;
    if (unit) return unit ;

    // Check other libraries too, but only for entities :
    // This is outside the LRM 5.2.2 definition, but ModelTech
    // seems to implement it this way :
    VhdlLibrary *lib ;
    MapIter mi ;
    VhdlIdDef *unit_id ;
    FOREACH_VHDL_LIBRARY(mi, lib) {
        if (lib==this) continue ;
        unit = lib->GetPrimUnit(name, restore) ;
        unit_id = unit ? unit->Id() : 0 ;
        if (unit_id && (unit_id->IsEntity() || unit_id->IsConfiguration())) return unit ;
    }

    // Really cannot find it
    return 0 ;
}

unsigned
VhdlLibrary::AddPrimUnit(VhdlDesignUnit *unit, unsigned refuse_on_error /*=1*/)
{
    if (!unit) return 0 ;

    // Don't accept a unit if there was an error
    // RD: If user did not want to do a 'refuse_on_error' check, then this means that the unit was created from a restored object.
    // In that case, error count is irrelevant (since restore does it's own checks irrespective of user design error count since VIPER 5189/3546).
    // So only if 'refuse_on_error' is set (the normal way in which this routine is called) should we issue this message.
    if (refuse_on_error && Message::ErrorCount()) {
        unit->Error("unit %s ignored due to previous errors", unit->Name()) ;
        delete unit ;
        unit = 0 ;
        return 0 ;
    }

    // Check if a primary unit already exists by the same name.
    VhdlPrimaryUnit *exist = (VhdlPrimaryUnit *)_primary_units.GetValue(unit->Name()) ;
    if (exist) {
        // Compare conformance signatures (VIPER #1902 fix)
        // VIPER 3080 : do NOT exclude packages from this signature check.
        if (exist->CalculateSignature() == unit->CalculateSignature()) {
            // Existing and new primary units are the same, so use existing one
            // and delete new one. (VIPER #1902)
            delete unit ;
            unit = 0 ;
            return 0 ; // No new unit was added
        }
        // If new unit and existing one are of different types (i.e. entity decl vs.
        // package decl) then give warning and delete new unit.  (VIPER #2710)
        if (unit->GetClassId() != exist->GetClassId()) {
            // VIPER #4487 : Produce error if 'unit' and 'exist' are configuration and entity. Moreover
            // configuration is defined with the same name it is supposed to configure.
            VhdlIdDef *unit_id = unit->Id() ;
            VhdlIdDef *exist_id = exist->Id() ;
            VhdlIdDef *configured_entity = (unit_id && exist_id && unit_id->IsConfiguration() && exist_id->IsEntity()) ? unit_id->GetEntity(): 0 ;
            if (configured_entity && (configured_entity == exist_id)) { // Name of entity to be configured is same as configuration name
                unit->Error("name of entity to be configured by %s is same as configuration name", unit->Name()) ;
            }
            // Viper 5286: changed the warning to Info
            unit->Info("re-analyze unit %s since unit %s is overwritten or removed", unit->Name(), exist->Name()) ;
            delete unit ;
            unit = 0 ;
            return 0 ;
        }
        // Delete old exising unit since entity declarations are different.
        if (unit->Id()) unit->Id()->Warning("overwriting existing primary unit %s",unit->Name()) ;
        // Remove it from the list
        (void) _primary_units.Remove(unit->Name()) ;
        // VIPER #3521: Do not delete it here, delete after inserting the new unit into the table
        //delete exist ;
    }

    (void) _primary_units.Insert(unit->Name(), unit) ;
    // Set the units owner to 'this' library
    unit->SetOwningLib(this) ;

    unsigned unit_added = 1 ;
    if (exist) {
        // VIPER #3521: Delete 'existing' unit now, it will delete 'unit' also if it depends
        // on 'exist' as the owner is now correctly set. No chance of corruption here because
        // of the chance of deletion of 'unit', since we are not using the 'unit' any more here.
        // But we need to be sure that we don't use it later, so need to check whether the 'unit'
        // is deleted or not. Save the name of the unit before deleting 'exist', we need it later:
        char *unit_name = Strings::save(unit->Name()) ;
        // Set library pointer to 0, so that while deleting 'exist' 'unit' is not removed:
        exist->SetOwningLib(0) ;
        delete exist ;
        // Check whether the unit, too, has been deleted for the above 'delete':
        if (!_primary_units.GetItem(unit_name)) unit_added = 0 ; // unit deleted
        Strings::free(unit_name) ;
    }

    return unit_added ; // Return the status of this unit addition
}

unsigned
VhdlLibrary::RemovePrimUnit(const VhdlDesignUnit *unit)
{
    // Return 1 if successfully removed
    unsigned result = _primary_units.Remove(unit->Name()) ;
    // VIPER #7046: Reset the Map, if all units are removed.
    // This will prevent re-insertion in the garbage list of the Map:
    if (!_primary_units.Size()) _primary_units.Reset() ;
    return result ;
}

unsigned VhdlLibrary::NumOfPrimUnits() const
{
    return _primary_units.Size() ;
}

VhdlPrimaryUnit *VhdlLibrary::PrimUnitByIndex(unsigned idx) const
{
    return (VhdlPrimaryUnit*)_primary_units.GetValueAt(idx) ;
}

/*********** Design Units **************************/

VhdlDesignUnit::VhdlDesignUnit(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope)
  : VhdlDeclaration(),
    _id(id),
    _local_scope(local_scope),
    _context_clause(context_clause),
    _timestamp(0),
    _is_elaborated(0),
    _is_static_elaborated(0),
// Set by the conversion routine to indicate that this unit is for internal use. Set to '0' by constructor.
    _is_verilog_module(0),
    _affected_by_path_name(0),
    _analysis_mode(0),
    _orig_design_unit(0)
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    , _is_already_processed(0) // VIPER #6896: flag on package that is already elaborated
#endif

{
    // Set the timestamp :
    _timestamp = (unsigned long) ::time(0) ;

    // VIPER #6937 : Set the dialect under which we are currently parsing this unit
    _analysis_mode = vhdl_file::GetAnalysisMode() ;
    // design unit dependency is now handled via scopes directly from selected name usage.
    // so don't do anything here any more.

    VhdlScope *id_scope = _id ? _id->LocalScope() : 0 ;
    if (id_scope && Message::ErrorCount() && (VhdlTreeNode::_present_scope != id_scope->Upper())) {
        VhdlTreeNode::_present_scope = id_scope->Upper() ;
        while (_local_scope && _local_scope != id_scope) {
            VhdlScope *tmp = _local_scope ;
            _local_scope = _local_scope->Upper() ;
            delete tmp ;
        }
    }
}

VhdlDesignUnit::~VhdlDesignUnit()
{
    // Design unit dependency is handled in the VhdlScope now.
    // When the _local_scope is deleted, any design unit that depends on this one
    // will be deleted since its scope is using this _local_scope.

    delete _local_scope ; // delete the scope before the _id that owns it.
    delete _id ;

    // VIPER #6933 : Delete _context_clause after _local_scope. Context clause
    // can have context_reference and that owned some library id pointers declared
    // in corresponding context decl
    // Delete the context clause (use clauses)
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_context_clause, i, elem) delete elem ;
    delete _context_clause ;
    // Delete comments if there
    //DeleteComments(GetComments()) ; // JJ: we call this in DTOR now.

    Strings::free(_orig_design_unit) ;
}

void
VhdlDesignUnit::ClosingLabel(VhdlDesignator *id)
{
    if (!id) return ;
    if (_id && !Strings::compare(_id->Name(),id->Name())) {
        id->Error("mismatch on label ; expected %s",_id->Name()) ;
    }
    SetClosingLabel(id) ; // VIPER #6666 (id may get deleted here)
}

const char *
VhdlDesignUnit::Name() const
{
    return (_id) ? _id->Name() : "" ;
}
void
VhdlDesignUnit::SetLocalScope(VhdlScope *s)
{
    _local_scope = s ;
}

/*********** Primary Units **************************/

VhdlPrimaryUnit::VhdlPrimaryUnit(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope)
  : VhdlDesignUnit(context_clause, id, local_scope),
    _compile_as_blackbox(0),
    _secondary_units(STRING_HASH),
    _owner(0)
    , _process_stmt_replaced(0) // VIPER #6633
{
    // Let the id point back to this parse tree :
    if (id) id->SetPrimaryUnit(this) ;
}

VhdlPrimaryUnit::~VhdlPrimaryUnit()
{
    // Remove me from my owner
    if (_owner) (void) _owner->RemovePrimUnit(this) ;

    // Also, delete all secondary units. They certainly have pointers
    // into 'this' primary unit
    MapIter mi ;
    VhdlSecondaryUnit *su ;

    // Delete the last ones first
    FOREACH_VHDL_SECONDARY_UNIT(this, mi, su) {
        delete su ;
    }
}

VhdlSecondaryUnit *
VhdlPrimaryUnit::GetSecondaryUnit(const char *name) const
{
    if (_secondary_units.Size()==0) return 0 ;

    // Without a name, return the last one compiled :
    if (!name) {
        // Make sure we don't return any PSL unit.
        // FIX ME : PSL units are actually not even real sec.units.
        // So, take the last non-psl unit :
        MapIter mi ;
        VhdlSecondaryUnit *sec_unit ;
        VhdlSecondaryUnit *result = 0 ;
        FOREACH_MAP_ITEM_BACK(&_secondary_units, mi, 0, &sec_unit) {
            if (!sec_unit) continue ;
            if (sec_unit->IsPslUnit()) continue ;
            result = sec_unit ;
            break ;
        }
        return result ;
    }

    // Find it by name
    VhdlSecondaryUnit *result = (VhdlSecondaryUnit*)_secondary_units.GetValue(name) ;
    if (result) return result ;

    // Check if maybe it's requested in mixed case :
    if (name[0]!='\\') { // It's NOT an escaped identifier
        char *downcased = Strings::strtolower(Strings::save(name)) ;
        result = (VhdlSecondaryUnit*)_secondary_units.GetValue(downcased) ;
        Strings::free( downcased ) ;
        if (result) return result ;
    }
    return 0 ;
}

unsigned
VhdlPrimaryUnit::AddSecondaryUnit(VhdlDesignUnit *unit)
{
    if (!unit) return 0 ;

    // Don't accept a unit if there was an error
    if (Message::ErrorCount()) {
        unit->Error("unit %s ignored due to previous errors", unit->Name()) ;
        delete unit ;
        return 0 ; // 0 means an error occurred and 'unit' was deleted
    }

    // Delete old one if there was one.
    VhdlSecondaryUnit *exist = (VhdlSecondaryUnit *)_secondary_units.GetValue(unit->Name()) ;
    if (exist) {
        // VIPER 3080 : do NOT exclude packages from this signature check.
        // Also, issue 1015 : if a package body is unchanged, do NOT override existing one.
        if (exist->CalculateSignature() == unit->CalculateSignature()) {
            // Existing and new secondary units are the same, so use existing one
            delete unit ;
            return 0 ; // No new unit was added
        }

        // Remove existing one from the list
        (void) _secondary_units.Remove(unit->Name()) ;
        if (unit->Id()) unit->Id()->Warning("overwriting existing secondary unit %s",unit->Name()) ;
        delete exist ;
    }
    (void) _secondary_units.Insert(unit->Name(), unit) ;

    // Set the unit's owner to me
    unit->SetOwningUnit(this) ;

    _is_static_elaborated = 0 ;

    return 1 ;
}

unsigned
VhdlPrimaryUnit::RemoveSecondaryUnit(const VhdlDesignUnit *unit)
{
    // return 1 if successfully removed
    return _secondary_units.Remove(unit->Name()) ;
}

unsigned VhdlPrimaryUnit::NumOfSecondaryUnits() const
{
    return _secondary_units.Size() ;
}

VhdlSecondaryUnit *VhdlPrimaryUnit::SecondaryUnitByIndex(unsigned idx) const
{
    return (VhdlSecondaryUnit*)_secondary_units.GetValueAt(idx) ;
}

/********************* Secondary Units *****************************/

VhdlSecondaryUnit::VhdlSecondaryUnit(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope)
  : VhdlDesignUnit(context_clause,id,local_scope),
    _owner(0)
{
    // Let the id point back to this unit :
    id->SetSecondaryUnit(this) ;
}

VhdlSecondaryUnit::~VhdlSecondaryUnit()
{
    // no local vars
    // Remove me from my owner
    if (_owner) (void) _owner->RemoveSecondaryUnit(this) ;
}

/********************* Owner Handlers *****************************/

void VhdlDesignUnit::SetOwningLib(VhdlLibrary * /*owner*/)      { VERIFIC_ASSERT(0) ; }
void VhdlPrimaryUnit::SetOwningLib(VhdlLibrary *owner)          { _owner = owner ; }

void VhdlDesignUnit::SetOwningUnit(VhdlPrimaryUnit * /*owner*/) { VERIFIC_ASSERT(0) ; }
void VhdlSecondaryUnit::SetOwningUnit(VhdlPrimaryUnit *owner)   { _owner = owner ; }

unsigned VhdlDesignUnit::HasOwner() const                       { return 0 ; }
unsigned VhdlPrimaryUnit::HasOwner() const                      { return (_owner) ? 1 : 0 ; }
unsigned VhdlSecondaryUnit::HasOwner() const                    { return (_owner) ? 1 : 0 ; }

/********************* Now the individual Units *****************************/

VhdlEntityDecl::VhdlEntityDecl(Array *context_clause, VhdlIdDef *id, Array *generic_clause, Array *port_clause, Array *decl_part, Array *statement_part, VhdlScope *local_scope)
  : VhdlPrimaryUnit(context_clause,id,local_scope),
    _generic_clause(generic_clause),
    _port_clause(port_clause),
    _decl_part(decl_part),
    _statement_part(statement_part)
{
    // Set the ports and generics as being that class of interface object
    Array *ids ;
    VhdlInterfaceDecl *decl ;
    unsigned i ;

    // Accumulate generics and set as such
    Array *generic_list = 0 ;
    if (_generic_clause) {
        generic_list = new Array(_generic_clause->Size()) ;
        VhdlIdDef *gen_id ;
        FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these generics to the order-based generics array
            if (ids) generic_list->Append(ids) ;
            gen_id = decl->GetId() ; // interface type/subprogram and package
            if (gen_id) generic_list->InsertLast(gen_id) ;
        }
    }

    // Accumulate ports and set as such
    Array *port_list = 0 ;
    if (_port_clause) {
        port_list = new Array(_port_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these ports to the order-based ports array
            port_list->Append(ids) ;
        }
    }
    // Set the ordered generics/ports into the entity identifier
    if (_id) _id->DeclareEntityId(generic_list,port_list) ;
    else {
        delete port_list ;
        delete generic_list ;
    }

    // Resolve any attribute specifications
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(local_scope) ;
    }

    // Set/test identifier usage/assignment :
    if( Message::ErrorCount() == 0 ) {
        VhdlEntityDecl::VarUsageStartingPoint();
    }
}

VhdlEntityDecl::~VhdlEntityDecl()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,elem) delete elem ;
    delete _generic_clause ;
    FOREACH_ARRAY_ITEM(_port_clause,i,elem) delete elem ;
    delete _port_clause ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) delete elem ;
    delete _statement_part ;
}

VhdlArchitectureBody::VhdlArchitectureBody(Array *context_clause, VhdlIdDef *id, VhdlIdRef *entity_name, Array *decl_part, Array *statement_part, VhdlScope *local_scope)
  : VhdlSecondaryUnit(context_clause, id,local_scope),
    _entity_name(entity_name),
    _decl_part(decl_part),
    _statement_part(statement_part)
    ,_bind_instances(0)
    ,_bind_verilog_instances(0)
    ,_access_allocated_values(0)
{
    // Resolve any attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(local_scope) ;
    }

    // Determine the assignment status of the vars
    if( Message::ErrorCount() == 0 ) {
        VhdlArchitectureBody::VarUsageStartingPoint();
    }
}
VhdlArchitectureBody::~VhdlArchitectureBody()
{
    unsigned i ;
    if (_bind_instances) {
        VhdlComponentInstantiationStatement *entity_inst ;
        FOREACH_ARRAY_ITEM(_bind_instances,i,entity_inst) delete entity_inst ;
        delete _bind_instances ;
    }
    if (_bind_verilog_instances) {
        _bind_verilog_instances->Reset() ;
        delete _bind_verilog_instances;
    }
    _bind_verilog_instances = 0 ;
    delete _entity_name ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) delete elem ;
    delete _statement_part ;

    // Access Value cleanup: (VIPER #4464, 7292)
    FreeAccessValueSet(_access_allocated_values) ;
}

VhdlConfigurationDecl::VhdlConfigurationDecl(Array *context_clause, VhdlIdDef *id, VhdlIdRef *entity_name, Array *decl_part, VhdlBlockConfiguration *block_configuration, VhdlScope *local_scope)
  : VhdlPrimaryUnit(context_clause,id,local_scope),
    _entity_name(entity_name),
    _decl_part(decl_part),
    _block_configuration(block_configuration)
{
    // Resolve any attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(local_scope) ;
    }
}
VhdlConfigurationDecl::~VhdlConfigurationDecl()
{
    delete _entity_name ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    delete _block_configuration ;
}

VhdlPackageDecl::VhdlPackageDecl(Array *context_clause, VhdlIdDef *id, Array *generic_clause, Array *generic_map_aspect, Array *decl_part, VhdlScope *local_scope)
  : VhdlPrimaryUnit(context_clause,id,local_scope),
    _generic_clause(generic_clause),
    _generic_map_aspect(generic_map_aspect),
    _decl_part(decl_part),
    _container(0)
{
    if ((_generic_clause || _generic_map_aspect) && !vhdl_file::IsVhdl2008()) Error("this construct is only supported in VHDL 1076-2008") ;

    Array *ids ;
    unsigned i ;
    VhdlInterfaceDecl *idecl ;

    // Accumulate generics and set as such
    Array *generic_list = 0 ;
    if (_generic_clause) {
        generic_list = new Array(_generic_clause->Size()) ;
        VhdlIdDef *gen_id ;
        FOREACH_ARRAY_ITEM(_generic_clause, i, idecl) {
            if (!idecl) continue ;
            ids = idecl->GetIds() ;
            // Add these generics to the order-based generics array
            if (ids) generic_list->Append(ids) ;
            gen_id = idecl->GetId() ; // interface type/subprogram and package
            if (gen_id) generic_list->InsertLast(gen_id) ;
        }
    }

    (_id && generic_list) ? _id->DeclarePackageId(generic_list) : delete generic_list ;

    if (_id && _generic_map_aspect) _id->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;

    // Resolve any attribute specifications
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        // VIPER #7620 : Produce error if subprogram body or protected type
        // def body is declared in the declarative part of package
        if (decl->IsSubprogramBody() || decl->IsProtectedTypeDefBody()) {
            decl->Error("%s is not allowed in package declaration", decl->IsSubprogramBody() ? "subprogram body": "protected type body") ;
        }
        decl->ResolveSpecs(local_scope) ;
    }

    // Viper #7160 / 7142 : 2008 LRM 5.2.6 / 5.3.2.4 :
    // For std.standard, to_string to be declared at the end of the package
    VhdlScope *old_scope = VhdlTreeNode::_present_scope ; // should be null
    VhdlTreeNode::_present_scope = local_scope ;
    if (local_scope && vhdl_file::IsVhdl2008() && vhdl_file::IsInsideStdPackageAnalysis()) {
        VhdlIdDef *string_type = local_scope->FindSingleObjectLocal("string") ;
        Map *this_scope = local_scope->GetThisScope() ;
        VhdlIdDef *type_id ;
        MapIter mi ;
        FOREACH_MAP_ITEM(this_scope, mi, 0, &type_id) {
            if (!string_type) break ; // could be an assert
            if (!type_id || !type_id->IsType()) continue ;

            if (type_id->IsScalarType()) {
                type_id->DeclarePredefinedOp(VHDL_to_string, type_id, 0, string_type) ;
            } else if (type_id == StdType("bit_vector")) {
                type_id->DeclarePredefinedOp(VHDL_to_string, type_id, 0, string_type) ;
                type_id->DeclarePredefinedOp(VHDL_to_bstring, type_id, 0, string_type) ;
                type_id->DeclarePredefinedOp(VHDL_to_binary_string, type_id, 0, string_type) ;
                type_id->DeclarePredefinedOp(VHDL_to_ostring, type_id, 0, string_type) ;
                type_id->DeclarePredefinedOp(VHDL_to_octal_string, type_id, 0, string_type) ;
                type_id->DeclarePredefinedOp(VHDL_to_hstring, type_id, 0, string_type) ;
                type_id->DeclarePredefinedOp(VHDL_to_hex_string, type_id, 0, string_type) ;
            }
        }

        // special overloads of predefined to_string
        VhdlIdDef *natural_type = local_scope->FindSingleObjectLocal("natural") ;
        VhdlIdDef *time_type = local_scope->FindSingleObjectLocal("time") ;
        VhdlIdDef *real_type = local_scope->FindSingleObjectLocal("real") ;

        // to_string(value: real; digits: natural) return string ;
        if (real_type && natural_type && string_type) {
            Array *args = new Array(2) ;
            VhdlTypeId::AddOperatorInterface(args, Strings::save("value"), real_type, 0, VHDL_in) ;
            VhdlTypeId::AddOperatorInterface(args, Strings::save("digits"), natural_type, 0, VHDL_in) ;
            args->Insert(string_type) ; // return type
            real_type->DeclarePredefinedOper(VHDL_to_string, args, 1, 1) ;
        }

        // to_string(value: real; format: string) return string ;
        if (real_type && string_type) {
            Array *args = new Array(2) ;
            VhdlTypeId::AddOperatorInterface(args, Strings::save("value"), real_type, 0, VHDL_in) ;
            VhdlTypeId::AddOperatorInterface(args, Strings::save("format"), string_type, 0, VHDL_in) ;
            args->Insert(string_type) ; // return type
            real_type->DeclarePredefinedOper(VHDL_to_string, args, 1, 1) ;
        }

        // to_string(value: time; unit: time) return string ;
        if (time_type && string_type) {
            Array *args = new Array(2) ;
            VhdlTypeId::AddOperatorInterface(args, Strings::save("value"), time_type, 0, VHDL_in) ;
            VhdlTypeId::AddOperatorInterface(args, Strings::save("unit"), time_type, 0, VHDL_in) ;
            args->Insert(string_type) ; // return type
            time_type->DeclarePredefinedOper(VHDL_to_string, args, 1, 1) ;
        }
    }
    VhdlTreeNode::_present_scope = old_scope ;
}
VhdlPackageDecl::~VhdlPackageDecl()
{
    // Delete the parse tree
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,elem) delete elem ;
    delete _generic_clause ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
    // Do not need to delete _container
}

VhdlContextDecl::VhdlContextDecl(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope)
  : VhdlPrimaryUnit(context_clause,id,local_scope)
{
}
VhdlContextDecl::~VhdlContextDecl()
{
    // Delete the parse tree
}
VhdlPackageInstantiationDecl::VhdlPackageInstantiationDecl(Array *context_clause, VhdlIdDef *id, VhdlName *uninstantiated_unit_name, Array *generic_map_aspect, VhdlScope *local_scope)
    : VhdlPrimaryUnit(context_clause, id, 0),
      _inst_scope(local_scope),
      _uninst_package_name(uninstantiated_unit_name),
      _generic_map_aspect(generic_map_aspect),
      _generic_types(0),
      _initial_pkg(0)
     ,_elab_pkg(0)
{
    // This instantiation may be defined as library unit, so now global
    // _present_scope pointer is null. So set that before calling EntityAspect
    // as this API depends on _present_scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = _inst_scope ;

    // '_inst_scope' is just the inner scope of package instance. It will
    // be used to resolve the actual part of generic map.
    // Content of this instance will be copied content of uninstantiated
    // package. So it will behave like a package declaration with name
    // same as instance name (_id->Name()) and content of uninstantiated
    // package. So local-scope of instance id will be scope of copied package
    if (_id) _id->SetLocalScope(0) ;
    if (_inst_scope) _inst_scope->SetOwner(0) ;

    // Pick up the uninstantiated package id
    VhdlIdDef *uninst_package_id = (_uninst_package_name) ? _uninst_package_name->EntityAspect(): 0 ;
    if (_uninst_package_name && uninst_package_id && !uninst_package_id->IsPackage()) {
        _uninst_package_name->Error("%s is not a %s",uninst_package_id->Name(), "package") ;
    }
    if (_id) _id->SetUninstantiatedPackageId(uninst_package_id) ;
    // Check whether '_uninst_package_name' is really a uninstantiated package i.e. a
    // package with generic clause and no generic map aspect.
    VhdlPrimaryUnit *unit = uninst_package_id ? uninst_package_id->GetPrimaryUnit(): 0 ;
    if (unit && _uninst_package_name && uninst_package_id && (!unit->GetGenericClause() || unit->GetGenericMapAspect())) {
        _uninst_package_name->Error("%s is not an uninstantiated package", uninst_package_id->Name()) ;
    }

    if (_id && uninst_package_id) {
        // Create special alias like identifiers for every object defined in
        // uninstantiated package and declare those in _local_scope.
        // Copy the uninstantiated package content and set the actual fields of
        // alias like identifiers
        InlineUninstantiatedPackage(uninst_package_id, _inst_scope) ;
        // Set original uninstantiated package in the dependency list.
        // That will help us to regenerate uninstantiated package information
        // during save/restore
        if (_local_scope) _local_scope->Using(uninst_package_id->LocalScope()) ;
    }
    if (_id && _generic_map_aspect) {
        _id->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;
        _generic_types = new Map(POINTER_HASH) ;
        // Store generic types along with its actual type obtained from 'TypeInferAssociationList'.
        // This will help us to resolve selected names/subprogram calls of package instance
        Array *generics = _id->GetGenerics() ;
        unsigned i ;
        VhdlIdDef *generic_id ;
        FOREACH_ARRAY_ITEM(generics, i, generic_id) {
            if (!generic_id) continue ;
            if (generic_id->IsGenericType()) {
                (void) _generic_types->Insert(generic_id, generic_id->ActualType()) ;
            } else if (generic_id->IsSubprogram() && !generic_id->GetActualId()) {
                // VIPER #7543 : Connect default subprogram during analysis
                // This generic subprogram contains no actual, find default
                VhdlDeclaration *inst_decl = generic_id->GetSubprogInstanceDecl() ;
                VhdlIdDef *default_sbprogram = inst_decl ? inst_decl->GetDefaultSubprogram(_inst_scope): 0 ;
                if (default_sbprogram) generic_id->SetActualId(default_sbprogram, 0) ;
            }
        }
    }
    _present_scope = old_scope ;
}
VhdlPackageInstantiationDecl::~VhdlPackageInstantiationDecl()
{
    delete _uninst_package_name ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect,i,elem) delete elem ;
    delete _generic_map_aspect ;
    delete _generic_types ;
    delete _inst_scope ;
    delete _initial_pkg ;
    delete _elab_pkg ;
    // This _local_scope contains iddefs owned by the scope, delete those
    VhdlIdDef *id ;
    MapIter mi ;
    Map *id_map = _local_scope ? _local_scope->DeclArea(): 0 ;
    FOREACH_MAP_ITEM(id_map, mi, 0, &id) delete id ;
    delete _local_scope ;
    _local_scope = 0 ;
}
void VhdlPrimaryUnit::SetActualTypes() {}
void VhdlPackageInstantiationDecl::SetActualTypes()
{
    return ;
#if 0
    // This code is no longer required : as packages are now copied.
    MapIter mi ;
    VhdlIdDef *generic_id ;
    VhdlIdDef *actual_type ;
    FOREACH_MAP_ITEM(_generic_types, mi, &generic_id, &actual_type) {
        if (generic_id) generic_id->SetActualId(actual_type, 0) ;
    }
#endif
}
// Copy the content of uninstantiated package within package instantiation decl
// Create special alias identifier for each uninstantiated package element
// Connect the alias identifiers to copied uninstantiated package element identifiers
void VhdlPackageInstantiationDecl::InlineUninstantiatedPackage(const VhdlIdDef *uninst_package_id, const VhdlScope *inst_scope)
{
    // Get package declaration
    VhdlPrimaryUnit *decl = uninst_package_id ? uninst_package_id->GetPrimaryUnit() : 0 ;
    if (!decl || !uninst_package_id || !_id) return ;

    // _local_scope will be child scope of container of package instance
    if (!_local_scope) _local_scope = new VhdlScope(inst_scope ? inst_scope->Upper(): 0, 0, 0) ;
    _local_scope->SetOwner(_id) ;

    // Copy the uninstantiated package and set to _initial_pkg
    VhdlMapForCopy old2new ;
    _initial_pkg = decl->CopyPrimaryUnit(0, old2new, 0) ;
    VhdlScope *pkg_scope = _initial_pkg ? _initial_pkg->LocalScope(): 0 ;
    if (pkg_scope && inst_scope) pkg_scope->SetUpper(_inst_scope) ;
    VhdlSecondaryUnit *sec_unit = _initial_pkg ? _initial_pkg->GetSecondaryUnit(0): 0 ;
    VhdlScope *pkg_body_scope = sec_unit ? sec_unit->LocalScope(): 0 ;
    if (pkg_body_scope && inst_scope) pkg_body_scope->SetUpper(_inst_scope) ;
    VhdlIdDef *init_pkg_id = _initial_pkg ? _initial_pkg->Id(): 0 ;
    Map *id_map = pkg_scope ? pkg_scope->DeclArea(): 0 ;
    VhdlIdDef *local_id ;
    VhdlPackageInstElement *new_id ;
    char *name ;
    MapIter mi ;
    Array *generics = 0 ;

    // Create special alias identifier for each object declared in uninstantiated package
    FOREACH_MAP_ITEM(id_map, mi, &name, &local_id) {
        if (!local_id || (local_id == init_pkg_id)) continue ; // Skip package id
        new_id = new VhdlPackageInstElement(Strings::save(name)) ;
        // For alias identifier, set 'special alias identifier' created for
        // alias's target in 'special alias identifier' for original alias id
        if (local_id->IsAlias()) {
            VhdlIdDef *target = local_id->GetTargetId() ;
            VhdlIdDef *new_taregt = target ? _local_scope->FindSingleObjectLocal(target->Name()) : 0 ;
            new_id->SetTargetPkgInstElement(new_taregt) ;
        }
        new_id->ConnectActualId(local_id) ;
        if (local_id->IsGeneric()) {
            if (!generics) generics = new Array(1) ;
            generics->InsertLast(new_id) ;
        }
        _local_scope->ForceDeclare(new_id) ;
#ifdef VHDL_ID_SCOPE_BACKPOINTER
        // VIPER #7258 : Set back pointer
        // Set the owning scope of the identifier :
        new_id->SetOwningScope(_local_scope) ;
#endif
    }
    if (generics) _id->DeclarePackageId(generics) ;
}
Array *VhdlPackageInstantiationDecl::GetGenericClause() const
{
    // During elaboration return from elaboration specific package
    if (_elab_pkg) return _elab_pkg->GetGenericClause() ;
    return (_initial_pkg) ? _initial_pkg->GetGenericClause(): 0 ;
}

VhdlPackageBody::VhdlPackageBody(Array *context_clause,VhdlIdDef *id, Array *decl_part, VhdlScope *local_scope)
  : VhdlSecondaryUnit(context_clause,id,local_scope),
    _decl_part(decl_part)
{
    // Resolve any attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(local_scope) ;
    }
#if 0 // now part of CheckIncompleteTypes()
    // VIPER #3630 : Produce error if deferred constant is not initialized
    // Get Package declaration
    VhdlIdDef *package_decl_id = (local_scope) ? local_scope->GetContainingPrimaryUnit() : 0 ;
    VhdlScope *package_decl_scope = (package_decl_id) ? package_decl_id->LocalScope(): 0 ;
    Map *this_scope = (package_decl_scope) ? package_decl_scope->GetThisScope() : 0 ;
    // Iterate over identifiers declared within package
    MapIter mi ;
    VhdlIdDef *cid ;
    FOREACH_MAP_ITEM(this_scope, mi, 0, &cid) {
        if (!cid || !cid->IsConstantId()) continue ; // Not constant identifier
        if (!cid->Decl()) { // Deferred constant, but no initial value
           Error("deferred constant %s needs an initial value", cid->Name()) ;
        }
    }
#endif
}
VhdlPackageBody::~VhdlPackageBody()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
        // FIX ME : a package header (primary unit) has
        // _body pointers (in the subprogram identifiers) into a body.
        // Deleting the body will leave dangling pointers in the
        // primary unit.
        // This happens if a package body is in a separate file from
        // the package header. Homograph errors will then occur if
        // just the body is re-read.
        // Extra carefull when fixing : often, primary unit of this
        // package body is already deleted (we are in its destructor).
        // So don't fix this problem here, fix it at over-ride time.
}

unsigned
VhdlEntityDecl::HasUninitializedGenerics() const
{
    if (!_id) return 0 ;
    // Example routine that returns 1 if this entity has generics that do not have a 'initial' assignment.
    // This one runs over the _generic_clause parse tree nodes
    unsigned i ;
    VhdlInterfaceDecl *elem ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, elem) {
        if (!elem) continue ;
        if (elem->GetInitAssign()) continue ;
        // this one has no initial assignment
        return 1 ;
    }
    return 0 ;
}

unsigned
VhdlEntityDecl::HasUnconstrainedPorts() const
{
    if (!_id) return 0 ;
    // Example routine that returns 1 if this entity has ports which are unconstrained.
    // This one runs over the pre-set port list of the entities' ports :
    unsigned i ;
    VhdlIdDef *port ;
    FOREACH_ARRAY_ITEM(_id->GetPorts(), i, port) {
        if (!port) continue ;
        if (port->IsUnconstrained()) return 1 ;
    }
    return 0 ;
}
unsigned
VhdlEntityDecl::HasUnconstrainedUninitializedPorts() const
{
    if (!_id) return 0 ;
    // Example routine that returns 1 if this entity has ports which are unconstrained.
    // This one runs over the pre-set port list of the entities' ports :
    unsigned i ;
    VhdlIdDef *port ;
    FOREACH_ARRAY_ITEM(_id->GetPorts(), i, port) {
        if (!port) continue ;
        if (port->IsUnconstrained() && !port->HasInitAssign()) return 1 ;
    }
    return 0 ;
}
// Inspect the dependancy list, insert the units used by 'this'
// in the argument specific list, insert 'this' to the list.
// VIPER #2845 : Add a Set as argument to check mutual dependency and insert both
// primary and secondary units in the ordered list
void
VhdlDesignUnit::CreateOrderedList(Set *list, Set *done) const
{
    if (!list || !done) return ;
    // Skip if we have been here before
    if (!done->Insert(this)) return ; // Return if this unit was already visited before

    // Get the list of design units in which this unit depends.
    // That list is in the 'using' list of the local scope.
    if (!LocalScope()) return ;

    Set *using_scopes = LocalScope()->GetUsing() ;
    SetIter si ;
    VhdlDesignUnit *design_unit ;
    VhdlScope *scope ;
    VhdlIdDef *unit ;

    // Iterate over using list
    FOREACH_SET_ITEM(using_scopes, si, &scope) {
        unit = scope->GetContainingDesignUnit() ;
        if (!unit) continue ;
        design_unit = unit->GetDesignUnit() ;
        if (!design_unit) continue ;

        // Check units only if they are in this work library.
        // Check if it is in this library :
        if (design_unit->GetOwningLib() != vhdl_file::GetWorkLib()) continue ;

        design_unit->CreateOrderedList(list, done) ;
    }
    // Insert as the last (most top) unit :
    (void) list->Insert(this) ;
}

void
VhdlDesignUnit::SetOriginalUnitName(const char* veri_name)
{
    Strings::free(_orig_design_unit) ;
    _orig_design_unit = Strings::save(veri_name) ;
}
/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlDesignUnit::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;

    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_context_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }

    return sig ;
}

unsigned long
VhdlEntityDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDesignUnit::CalculateSignature() ;

    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }

    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }

    return sig ;
}

unsigned long
VhdlConfigurationDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDesignUnit::CalculateSignature() ;

    if (_entity_name) sig = HASH_VAL(sig, _entity_name->CalculateSignature()) ;
    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    if (_block_configuration) sig = HASH_VAL(sig, _block_configuration->CalculateSignature()) ;

    return sig ;
}

unsigned long
VhdlPackageDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDesignUnit::CalculateSignature() ;

    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }

    return sig ;
}

unsigned long
VhdlContextDecl::CalculateSignature() const
{
    return VhdlDesignUnit::CalculateSignature() ;
}
unsigned long
VhdlPackageInstantiationDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDesignUnit::CalculateSignature() ;

    if (_uninst_package_name) sig = HASH_VAL(sig, _uninst_package_name->CalculateSignature()) ;

    unsigned i ;
    VhdlDiscreteRange *expr ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, expr) {
        if (expr) sig = HASH_VAL(sig, expr->CalculateSignature()) ;
    }
    return sig ;
}
unsigned long
VhdlArchitectureBody::CalculateSignature() const
{
    unsigned long sig = VhdlDesignUnit::CalculateSignature() ;

    if (_entity_name) sig = HASH_VAL(sig, _entity_name->CalculateSignature()) ;
    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }

    return sig ;
}

unsigned long
VhdlPackageBody::CalculateSignature() const
{
    unsigned long sig = VhdlDesignUnit::CalculateSignature() ;

    VhdlDeclaration *decl ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }

    return sig ;
}

// Get the design unit from which this unit is copied
VhdlDesignUnit *VhdlDesignUnit::GetOriginalUnit() const
{
    if (!_orig_design_unit) return 0 ; // This is not copied unit, return 0

    // This is copied unit
    // Here we are assuming that copied unit is added in the library where original
    // unit resides.
    // Get the owning library of this unit
    VhdlLibrary *owning_lib = GetOwningLib() ;

    VhdlScope *container = 0 ;
    if (!owning_lib) {
        // Nested package does not have library, it will have upper scope
        container = _local_scope ? _local_scope->Upper(): 0 ;
    }
    if (!owning_lib && !container) return 0 ; // No library, cannot find design unit, return

    if (IsPrimaryUnit()) { // This is copied primary unit
        if (owning_lib) {
            return owning_lib->GetPrimUnit(_orig_design_unit) ; // Return primary unit named '_orig_design'
        } else if (container) {
            VhdlIdDef *orig_id = container->FindSingleObjectLocal(_orig_design_unit) ;
            return orig_id ? orig_id->GetPrimaryUnit(): 0 ;
        }
    } else { // This is copied secondary unit
        // Get the primary unit that owns this secondary unit
        VhdlPrimaryUnit *prim_unit = GetOwningUnit() ;

        // Now 2 cases can arise
        // 1. Copied secondary unit and unit from which it is copied are owned by
        //    single primary unit
        // 2. Copied unit is added in the copied primary unit i.e both primary and
        //    seconadary unit are copied once. So we can get the original secondary
        //    unit from the original primary unit

        // 1. Check if owning primary unit contains the original secondary unit
        VhdlSecondaryUnit *sec_unit = prim_unit ? prim_unit->GetSecondaryUnit(_orig_design_unit) : 0 ;
        if (sec_unit && sec_unit != this) return sec_unit ; // Find original secondary unit, return

        // Get the original primary unit, if owning primary unit is copied
        VhdlDesignUnit *unit = prim_unit ? prim_unit->GetOriginalUnit() : 0 ;
        prim_unit = (unit && owning_lib) ? owning_lib->GetPrimUnit(unit->Name()) : 0 ;

        // Get '_orig_design' named secondary unit from original primary
        return (prim_unit) ? prim_unit->GetSecondaryUnit(_orig_design_unit) : 0 ;
    }

    return 0 ;
}
void VhdlPrimaryUnit::SetUninstantiatedPackageId(VhdlIdDef * /*uninstantiated_package_id*/)
{ }
void VhdlPackageInstantiationDecl::SetUninstantiatedPackageId(VhdlIdDef *uninstantiated_package_id)
{
    if (_uninst_package_name) _uninst_package_name->SetId(uninstantiated_package_id) ;
}
VhdlIdDef *VhdlPrimaryUnit::GetUninstantiatedPackageId() const { return 0 ; }
VhdlIdDef *VhdlPackageInstantiationDecl::GetUninstantiatedPackageId() const
{
    return _uninst_package_name ? _uninst_package_name->GetId(): 0 ;
}

void VhdlPrimaryUnit::SetActualId(VhdlIdDef *id) { SetUninstantiatedPackageId(id) ; }
VhdlIdDef *VhdlPrimaryUnit::GetActualId() const { return GetUninstantiatedPackageId() ; }

// VIPER #6725: Returns the linefile spanning decl or statement part of an *empty* architecture.
// Note that it returns an *allocated* linefile, so avoid unnecessary calling.
// At the same time, you do not need to delete the linefile either!
// It only works under linefile-column mode, if it is not enabled, returns 0.
linefile_type
VhdlArchitectureBody::GetEmptyDeclPartLinefile() const
{
    linefile_type result = 0 ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (_decl_part && _decl_part->Size()) return 0 ; // Decl part is not empty

    linefile_type my_lf = Linefile() ;
    linefile_type entity_name_lf = (_entity_name) ? _entity_name->Linefile() : 0 ; // Linefile of the entity name
    if (!my_lf || !entity_name_lf) return 0 ;

    // Copy the linefile of the entity-name and set as result:
    result = entity_name_lf->Copy() ;
    result->SetLeftLine(entity_name_lf->GetRightLine()) ; // Set the left line at the end of entity-name
    result->SetLeftCol(entity_name_lf->GetRightCol()) ;   // Set the left col at the end of the entity-name
    result->SetRightLine(my_lf->GetRightLine()) ; // End line of the architecture decl
    result->SetRightCol(my_lf->GetRightCol()) ;   // End col of the architecture decl

    // Get the text from architecture name upto the end of the architecture decl:
    TextBasedDesignMod tbdm ;
    char *str = tbdm.GetText(result, 0 /* do not include mods */) ;
    if (!str) return 0 ;

    // Start with the left side of the id linefile:
    unsigned line = entity_name_lf->GetRightLine() ;
    unsigned col = entity_name_lf->GetRightCol() ;

    // And find the position of the ; after the module id
    unsigned within_long_comment = 0 ;
    unsigned within_short_comment = 0 ;
    char *s = str ;
    while (*s) {
        if ((*s=='\n') || (*s=='\r')) { line++ ; col = 1 ; }
        else /* non new-line chars */ { col++ ; }

        if (within_long_comment) {
            // Skip long comments (VHDL 2008):
            if ((*s=='*') && (*(s+1)=='/')) { within_long_comment = 0 ; s++ ; col++ ; }
            s++ ;
            continue ;
        } else if (within_short_comment) {
            // Skip short comments:
            if ((*s=='\n') || (*s=='\r')) { within_short_comment = 0 ; }
            s++ ;
            continue ;
        }

        // CHECKME: Are we within translate/synthesis off?
        if ((tolower(*s)=='i') && (tolower(*(s+1))=='s')) {
            // Found the required start position
            s++ ; col++ ;
            result->SetLeftLine(line) ; // Set the left line at the "is"
            result->SetLeftCol(col) ;   // Set the left col after the "is"
        } else if ((tolower(*s)=='b') && (tolower(*(s+1))=='e') &&
                   (tolower(*(s+2))=='g') && (tolower(*(s+3))=='i') && (tolower(*(s+4))=='n')) {
            // Found the required end position
            if (col>1) col-- ; // It should be before "begin", we are already at 'b', so decrease one
            result->SetRightLine(line) ; // Set the right line at the "begin"
            result->SetRightCol(col) ;   // Set the right col before the "begin"
            break ; // Done
        }

        if ((*s=='/') && (*(s+1)=='*'))      { within_long_comment = 1 ; s++ ; col++ ; }  // Go into long comment (VHDL 2008)
        else if ((*s=='-') && (*(s+1)=='-')) { within_short_comment = 1 ; s++ ; col++ ; } // Go into short comment
        s++ ;
    }

    Strings::free(str) ; // Cleanup now
#endif
    return result ;
}

linefile_type
VhdlArchitectureBody::GetEmptyStmtPartLinefile() const
{
    linefile_type result = 0 ;
#ifdef VERIFIC_LINEFILE_INCLUDES_COLUMNS
    if (_statement_part && _statement_part->Size()) return 0 ; // Statement part is not empty

    linefile_type my_lf = Linefile() ;
    linefile_type entity_name_lf = (_entity_name) ? _entity_name->Linefile() : 0 ; // Linefile of the entity name
    if (!my_lf || !entity_name_lf) return 0 ;

    // Copy the linefile of the entity-name and set as result:
    result = entity_name_lf->Copy() ;
    result->SetLeftLine(entity_name_lf->GetRightLine()) ; // Set the left line at the end of entity-name
    result->SetLeftCol(entity_name_lf->GetRightCol()) ;   // Set the left col at the end of the entity-name
    result->SetRightLine(my_lf->GetRightLine()) ; // End line of the architecture decl
    result->SetRightCol(my_lf->GetRightCol()) ;   // End col of the architecture decl

    // Get the text from architecture name upto the end of the architecture decl:
    TextBasedDesignMod tbdm ;
    char *str = tbdm.GetText(result, 0 /* do not include mods */) ;
    if (!str) return 0 ;

    // Start with the left side of the id linefile:
    unsigned line = entity_name_lf->GetRightLine() ;
    unsigned col = entity_name_lf->GetRightCol() ;

    // And find the position of the ; after the module id
    unsigned within_long_comment = 0 ;
    unsigned within_short_comment = 0 ;
    char *s = str ;
    while (*s) {
        if ((*s=='\n') || (*s=='\r')) { line++ ; col = 1 ; }
        else /* non new-line chars */ { col++ ; }

        if (within_long_comment) {
            // Skip long comments (VHDL 2008):
            if ((*s=='*') && (*(s+1)=='/')) { within_long_comment = 0 ; s++ ; col++ ; }
            s++ ;
            continue ;
        } else if (within_short_comment) {
            // Skip short comments:
            if ((*s=='\n') || (*s=='\r')) { within_short_comment = 0 ; }
            s++ ;
            continue ;
        }

        // CHECKME: Are we within translate/synthesis off?
        if ((tolower(*s)=='b') && (tolower(*(s+1))=='e') &&
            (tolower(*(s+2))=='g') && (tolower(*(s+3))=='i') && (tolower(*(s+4))=='n')) {
            // Found the required start position
            s+=4 ; col+=4 ; // It should be after "begin", we are now at 'b', so increase 4
            result->SetLeftLine(line) ; // Set the left line at the "begin"
            result->SetLeftCol(col) ;   // Set the left col after the "begin"
        } else if ((tolower(*s)=='e') && (tolower(*(s+1))=='n') && (tolower(*(s+2))=='d')) {
            // Found the required end position
            if (col>1) col-- ; // It should be before "end", we are already at 'e', so decrease one
            result->SetRightLine(line) ; // Set the right line at the "end"
            result->SetRightCol(col) ;   // Set the right col before the "end"
            break ; // Done
        }

        if ((*s=='/') && (*(s+1)=='*'))      { within_long_comment = 1 ; s++ ; col++ ; }  // Go into long comment (VHDL 2008)
        else if ((*s=='-') && (*(s+1)=='-')) { within_short_comment = 1 ; s++ ; col++ ; } // Go into short comment
        s++ ;
    }

    Strings::free(str) ; // Cleanup now
#endif
    return result ;
}

