/*
 *
 * [ File Version : 1.266 - 2014/03/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include <time.h> // for time stamp

#include "Array.h"
#include "Set.h"
#include "Strings.h"
#include "Message.h"

#include "VhdlDeclaration.h"

#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlSpecification.h"
#include "VhdlConfiguration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"
#include "vhdl_file.h"
#include "VhdlUnits.h"
#include "VhdlCopy.h" // For VIPER #6933

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

VhdlDeclaration::VhdlDeclaration()
  : VhdlTreeNode()
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{ }

VhdlDeclaration::~VhdlDeclaration()
{
    // Delete comments if there
    //DeleteComments(GetComments()) ; // JJ: we call this in DTOR now.
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _closing_label ;
#endif
}

/******************************************************************/
//  Constructors/Destructors
/******************************************************************/
//  Type Definitions

VhdlTypeDef::VhdlTypeDef()
  : VhdlTreeNode()
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    , _closing_label(0)
#endif
{ }

VhdlTypeDef::~VhdlTypeDef()
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _closing_label ;
#endif
}

VhdlScalarTypeDef::VhdlScalarTypeDef(VhdlDiscreteRange *scalar_range)
  : VhdlTypeDef(),
    _scalar_range(scalar_range)
{ }
VhdlScalarTypeDef::~VhdlScalarTypeDef()
{
    delete _scalar_range ;
}

VhdlArrayTypeDef::VhdlArrayTypeDef(Array *index_constraint, VhdlSubtypeIndication *subtype_indication)
  : VhdlTypeDef(),
    _index_constraint(index_constraint),
    _subtype_indication(subtype_indication)
{ }
VhdlArrayTypeDef::~VhdlArrayTypeDef()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_index_constraint,i,elem) delete elem ;
    delete _index_constraint ;
    delete _subtype_indication ;
}

VhdlRecordTypeDef::VhdlRecordTypeDef(Array *element_decl_list, VhdlScope *local_scope)
  : VhdlTypeDef(),
    _element_decl_list(element_decl_list),
    _local_scope(local_scope)
{ }
VhdlRecordTypeDef::~VhdlRecordTypeDef()
{
    // Delete the contents of the construct
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_element_decl_list,i,elem) delete elem ;
    delete _element_decl_list ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
}

VhdlProtectedTypeDef::VhdlProtectedTypeDef(Array *decl_list, VhdlScope *local_scope)
  : VhdlTypeDef(),
    _decl_list(decl_list),
    _local_scope(local_scope)
{
}
VhdlProtectedTypeDef::~VhdlProtectedTypeDef()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_list,i,elem) delete elem ;
    delete _decl_list ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
}

VhdlProtectedTypeDefBody::VhdlProtectedTypeDefBody(Array *decl_list, VhdlScope *local_scope)
  : VhdlTypeDef(),
    _decl_list(decl_list),
    _local_scope(local_scope)
{
}
VhdlProtectedTypeDefBody::~VhdlProtectedTypeDefBody()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_list,i,elem) delete elem ;
    delete _decl_list ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
}

VhdlAccessTypeDef::VhdlAccessTypeDef(VhdlSubtypeIndication *subtype_indication)
  : VhdlTypeDef(),
    _subtype_indication(subtype_indication)
{ }
VhdlAccessTypeDef::~VhdlAccessTypeDef()
{
    delete _subtype_indication ;
}

VhdlFileTypeDef::VhdlFileTypeDef(VhdlName *file_type_mark)
  : VhdlTypeDef(),
    _file_type_mark(file_type_mark)
{ }
VhdlFileTypeDef::~VhdlFileTypeDef()
{
    delete _file_type_mark ;
}

VhdlEnumerationTypeDef::VhdlEnumerationTypeDef(Array *enumeration_literal_list)
  : VhdlTypeDef(),
    _enumeration_literal_list(enumeration_literal_list)
{ }
VhdlEnumerationTypeDef::~VhdlEnumerationTypeDef()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list,i,elem) delete elem ;
    delete _enumeration_literal_list ;
}

VhdlPhysicalTypeDef::VhdlPhysicalTypeDef(VhdlDiscreteRange *range_constraint, Array *physical_unit_decl_list)
  : VhdlTypeDef(),
    _range_constraint(range_constraint),
    _physical_unit_decl_list(physical_unit_decl_list)
{ }
VhdlPhysicalTypeDef::~VhdlPhysicalTypeDef()
{
    delete _range_constraint ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list,i,elem) delete elem ;
    delete _physical_unit_decl_list ;
}

/******************************************************************/
//  Constructors/Destructors
/******************************************************************/
/******************************************************************/
//  Declarations of the objects is done in the constructors
/******************************************************************/

VhdlLibraryClause::VhdlLibraryClause(Array *id_list, unsigned is_implicit)
  : VhdlDeclaration(),
    _id_list(id_list),
    _is_implicit(is_implicit)
{
    // Don't need to do anything beyond putting the id's in the scope ; which already happened

    VhdlIdDef *prim_unit_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
    unsigned is_context_decl = (prim_unit_id && prim_unit_id->IsContext()) ;

    if (is_context_decl) { // Vhdl 2008 LRM section 13.3
        unsigned i ;
        VhdlIdDef *lib_id ;
        FOREACH_ARRAY_ITEM(id_list, i, lib_id) {
            if (lib_id && Strings::compare_nocase(lib_id->Name(), "work")) {
                Error("use of library logical name WORK is illegal inside context declaration") ;
            }
        }
    }
}
VhdlLibraryClause::~VhdlLibraryClause()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_id_list,i,elem) delete elem ;
    delete _id_list ;
}

VhdlUseClause::VhdlUseClause(Array *selected_name_list, unsigned is_implicit)
  : VhdlDeclaration(),
    _selected_name_list(selected_name_list),
    _is_implicit(is_implicit)
{
    // Add selected-name items to present use-clause-scope-table
    unsigned i ;
    VhdlName *elem ;
#if 0
    // old way of storing use clauses :
    Set items(POINTER_HASH, 383) ; // good initial size. Use clauses normally have a lot of items.
    FOREACH_ARRAY_ITEM(_selected_name_list, i, elem) {
        if (!elem) continue ;
        items.Reset() ;
        // FIX ME : Check that this is an expanded name ? Error if not ?
        (void) elem->FindObjects(items,1/*for_use_clause*/) ;
        // Now add these items to the present scope as use-clause items
        _present_scope->AddUseClause(items) ;
    }
#endif

    VhdlIdDef *prim_unit_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
    unsigned is_context_decl = (prim_unit_id && prim_unit_id->IsContext()) ;

    // new way of storing use clauses :
    VhdlName *prefix ;
    VhdlDesignator *suffix ;
    VhdlIdDef *prefix_id ;
    FOREACH_ARRAY_ITEM(_selected_name_list, i, elem) {
        if (!elem) continue ;
        // find use clauses.
        prefix = elem->GetPrefix() ;
        suffix = elem->GetSuffix() ;
        // valid use clause selected name always has these two :
        if (!prefix || !suffix) continue ; // something bad.
        // resolve the prefix :
        prefix_id = prefix->FindSingleObject() ;
        if (!prefix_id) continue ; // error already given.

        if (is_context_decl && Strings::compare_nocase(prefix_id->Name(), "work")) {
            Error("use of library logical name WORK is illegal inside context declaration") ; // Vhdl 2008 LRM section 13.3
        }
        // Produce error if use clause specified package is
        // an uninstantiated package i.e with generic clause, but
        // no generic map aspect
        if (prefix_id->IsPackage() && !prefix_id->IsPackageInst()) {
            VhdlPrimaryUnit *unit = prefix_id->GetPrimaryUnit() ;
            if (unit && unit->GetGenericClause() && !unit->GetGenericMapAspect()) {
                elem->Error("uninstantiated package cannot be specified in use clause") ;
            }
        }

        // insert into the scope :
        if (suffix->IsAll()) {
            if (_present_scope) _present_scope->AddUseClause(prefix_id,0,/*from*/this) ; // null check to circumvent FORWARD_NULL
        } else {
            const char *suffix_name = suffix->Name() ;
            if (!suffix_name) continue ; // something bad happened.

            // add to use clause :
            if (_present_scope) _present_scope->AddUseClause(prefix_id,suffix_name,/*from*/this) ; // null check to circumvent FORWARD_NULL
        }
    }
}
VhdlUseClause::~VhdlUseClause()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_selected_name_list,i,elem) delete elem ;
    delete _selected_name_list ;
}

VhdlContextReference::VhdlContextReference(Array *selected_name_list)
  : VhdlDeclaration(),
    _selected_name_list(selected_name_list),
    _lib_ids(0)
{
    // Vhdl 2008 LRM section 13.4
    // Add selected-name items to present use-clause-scope-table
    VhdlIdDef *prim_unit_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
    unsigned is_context_decl = (prim_unit_id && prim_unit_id->IsContext()) ;

    VhdlName *prefix ;
    VhdlDesignator *suffix ;
    VhdlIdDef *prefix_id ;
    unsigned i ;

    VhdlName *elem ;
    FOREACH_ARRAY_ITEM(_selected_name_list, i, elem) {
        if (!elem) continue ;
        // find use clauses.
        prefix = elem->GetPrefix() ;
        suffix = elem->GetSuffix() ;

        // valid use clause selected name always has these two :
        if (!prefix || !suffix) continue ; // something bad.

        const char *suffix_name = suffix->Name() ;
        if (!suffix_name) continue ; // something bad happened

        // resolve the prefix :
        prefix_id = prefix->FindSingleObject() ;
        if (!prefix_id) continue ; // error already given.

        if (is_context_decl && Strings::compare_nocase(prefix_id->Name(), "work")) {
            Error("use of library logical name WORK is illegal inside context declaration") ; // Vhdl 2008 LRM section 13.3
        }

        if (!prefix_id->IsPackage() && !prefix_id->IsLibrary()) Error("prefix of %s in the use clause must be library or package identifier", suffix) ;

        VhdlPrimaryUnit *context_decl = 0 ;
        VhdlIdDef *context_id = 0 ;
        if (prefix_id->IsLibrary()) {
            // VIPER #7151 : library 'work' has special meaning. If 'work' is
            // specified pick the present work library
            VhdlLibrary *lib = 0 ;
            if (Strings::compare(prefix_id->Name(),"work")) {
                lib = vhdl_file::GetWorkLib() ;
            } else {
                lib = vhdl_file::GetLibrary(prefix_id->Name(), 1) ;
            }
            context_decl = lib ? lib->GetPrimUnit(suffix_name) : 0 ;
            context_id = context_decl ? context_decl->Id() : 0 ;
        } else { // package
            VhdlScope *prefix_scope = prefix->LocalScope() ;
            context_id = prefix_scope ? prefix_scope->FindSingleObject(suffix_name) : 0 ;
            context_decl = context_id ? context_id->GetPrimaryUnit() : 0 ;
        }

        if (!context_id || !context_id->IsContext()) {
            Error("suffix %s in the context reference does not denote a context declaration", suffix_name) ;
            continue ;
        }

        VhdlScope *context_scope = context_id->LocalScope() ;
        if (!context_scope) continue ;

        Set library_ids ;
        context_scope->FindAllLocal(library_ids) ;

        SetIter si ;
        VhdlIdDef *lib_id, *copied_lib_id ;
        VhdlMapForCopy old2new ;
        FOREACH_SET_ITEM(&library_ids, si, &lib_id) {
            if (!lib_id || !lib_id->IsLibrary()) continue ;
            // VIPER #6933 : Copy each library id and then declare in current
            // unit scope. Every unit should contain different library-id pointer
            // Also store these copied library-ids in a set, so that those can be
            // freed later from destructor of VhdlDesignUnit
            copied_lib_id = lib_id->CopyIdDef(old2new) ;
            if (!_lib_ids) _lib_ids = new Set(POINTER_HASH) ;
            (void) _lib_ids->Insert(copied_lib_id) ;
            (void) _present_scope->Declare(copied_lib_id) ;
        }

        MapIter mi ;
        Map *use_clause_units = context_scope->GetUseClauseUnits() ;
        FOREACH_MAP_ITEM(use_clause_units, mi, &lib_id, &suffix_name) {
            // VIPER #6933 : 'lib_id' will be stored in _present_scope, so
            // use VhdlLibraryId created for _present_scope
            copied_lib_id = old2new.GetCopiedId(lib_id) ;
            _present_scope->AddUseClause(copied_lib_id, suffix_name, this) ;
        }
    }
}

VhdlContextReference::~VhdlContextReference()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_selected_name_list,i,elem) delete elem ;
    delete _selected_name_list ;
    SetIter si ;
    VhdlIdDef *lib_id ;
    FOREACH_SET_ITEM(_lib_ids, si, &lib_id) delete lib_id ;
    delete _lib_ids ;
}

VhdlElementDecl::VhdlElementDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication)
  : VhdlTreeNode(),
    _id_list(id_list),
    _subtype_indication(subtype_indication)
{
    if (!_subtype_indication) return ;
    VhdlIdDef *type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;

    // Issue 2921 : element cannot be a file type
    if (type && type->IsFileType()) {
        Error("elements of file type are not allowed in composite types") ;
    }

    // LRM sections 7.4.1 and 7.4.2
    unsigned is_locally_static_type = _subtype_indication->IsLocallyStatic() ;
    unsigned is_globally_static_type = is_locally_static_type || _subtype_indication->IsGloballyStatic() ;

    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        id->DeclareElementId(type, is_locally_static_type, is_globally_static_type) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        id->SetSubtypeIndication(_subtype_indication) ;
#endif
    }
}
VhdlElementDecl::~VhdlElementDecl()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_id_list,i,elem) delete elem ;
    delete _id_list ;
    delete _subtype_indication ;
}

VhdlPhysicalUnitDecl::VhdlPhysicalUnitDecl(VhdlIdDef *id, VhdlExpression *physical_literal)
  : VhdlTreeNode(),
    _id(id),
    _physical_literal(physical_literal)
{ }
VhdlPhysicalUnitDecl::~VhdlPhysicalUnitDecl()
{
    delete _id ;
    delete _physical_literal ;
}

VhdlSubprogramDecl::VhdlSubprogramDecl(VhdlSpecification *subprogram_spec)
  : VhdlDeclaration(),
    _subprogram_spec(subprogram_spec)
{
    // Get the identifier of this subprogram :
    VhdlIdDef *designator = (subprogram_spec) ? subprogram_spec->GetId() : 0 ;
    if (!designator) return ; // nothing to do if there is no identifier

    // VIPER 6827 : linking of specification is done in the subprogram_spec constructor.
    // For a SubprogramDeclaration, that link should NOT be there (since cannot declare a homograph)
    VhdlSpecification *specification = designator->Spec() ;
    // And get the identifier of that spec :
    if (specification && (specification != subprogram_spec)) {
        // Found a 'declaration' of the subprogram. This is the one that is visible in the scope :
        // Get the identifier of that specification :
        VhdlIdDef *exist = specification->GetDesignator() ;
        // There is already an id with this profile
        designator->Error("a homograph of %s is already declared in this region", designator->Name()) ;
        if (exist) exist->Info("%s is declared here",exist->Name()) ;
        // Now undo the spec-spec link (let them point back to their own specs), or else we may crash (VIPER 6754) :
        designator->SetSpec(subprogram_spec) ;
        if (exist) exist->SetSpec(specification) ;
    }
}

VhdlSubprogramDecl::~VhdlSubprogramDecl()
{
    delete _subprogram_spec ;
}

VhdlSubprogramBody::VhdlSubprogramBody(VhdlSpecification *subprogram_spec, Array *decl_part, Array *statement_part)
  : VhdlDeclaration(),
    _subprogram_spec(subprogram_spec),
    _decl_part(decl_part),
    _statement_part(statement_part)
{
    // Get the identifier of this subprogram :
    VhdlIdDef *designator = (subprogram_spec) ? subprogram_spec->GetId() : 0 ;
    if (!designator || !subprogram_spec) return ; // nothing to do if there is no identifier

    // If there is a pragma on this subprog body, check/discard if not appropriate for the subprogram
    designator->CheckPragmaSetting() ;

    // Always set my body pointer to me :
    designator->SetBody(this) ;

    // VIPER 6827 : linking of declaration to body specification is done in the subprogram_spec constructor.
    // If that link is present then let the body of the linked identifier point to me :
    VhdlSpecification *specification = designator->Spec() ;
    // And get the identifier of that spec :
    if (specification && (specification != subprogram_spec)) {
        // Found a 'declaration' of the subprogram. This is the one that is visible in the scope :
        // Get the identifier of that specification :
        VhdlIdDef *exist = specification->GetDesignator() ;
        // Issue 2185 : LRM 2.7 : Do a conformance check between the existing spec and this new one.
        if (specification->CalculateSignature() != subprogram_spec->CalculateSignature()) {
            // Issue 2764 and 2673 are examples where implementation of conformance rule check
            // is not consistently implemented for various VHDL tools. Since this check is a benign
            // simple language rule check, and the more important type-signature consistency is already
            // checked in the homograph check, decided to turn this conformance check into a warning.
            // VIPER #3159 and Viper #3693: Upgrade this warning to error: Both VCS and ModelSim errors out:
            // designator->Warning("subprogram %s does not conform with its declaration", designator->Name()) ;
            designator->Error("subprogram %s does not conform with its declaration", designator->Name()) ;
            if (exist) exist->Info("%s is declared here", exist->Name()) ;
            // Let the linking happen, so that customers can downgrade this to a warning.
        }
        // Now set the body of the 'spec' to me :
        if (exist) exist->SetBody(this) ;

        // Also, copy pragma's (always set on the body id) to the specification id.
        if (exist && designator->GetPragmaFunction()) exist->SetPragmaFunction(designator->GetPragmaFunction()) ;
        if (exist && designator->GetPragmaSigned()) exist->SetPragmaSigned() ;
    }

    // Resolve any attribute specifications
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (!decl) continue ;
        decl->ResolveSpecs(VhdlSubprogramBody::LocalScope()) ;
    }

    // Determine the assignment status of the vars in this subProgram
    if( Message::ErrorCount() == 0 ) {
        VarUsageStartingPoint();
    }
}
VhdlSubprogramBody::~VhdlSubprogramBody()
{
    delete _subprogram_spec ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_decl_part,i,elem) delete elem ;
    delete _decl_part ;
    FOREACH_ARRAY_ITEM(_statement_part,i,elem) delete elem ;
    delete _statement_part ;
}

VhdlSubtypeDecl::VhdlSubtypeDecl(VhdlIdDef *id, VhdlSubtypeIndication *subtype_indication)
  : VhdlDeclaration(),
    _id(id),
    _subtype_indication(subtype_indication)
{
    if (!_subtype_indication || !_id) return ;

    VhdlIdDef *type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;
    // Check rules on interface declarations inside subprograms :
    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;
    // Check for deffered constants Viper 4368
    if (!subprog) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _subtype_indication->CheckLegalDefferedConst(_subtype_indication, 0) ;
    }
    VhdlIdDef *res_func = _subtype_indication->FindResolutionFunction() ;
    // CHECK ME : What happens if both the base type and the subtypeindication
    // have a resolution function ?

    // Assume that the present one overrules..
    if (!res_func && type) res_func = type->ResolutionFunction() ;

    // LRM sections 7.4.1 and 7.4.2
    unsigned locally_static_type = _subtype_indication->IsLocallyStatic() ;
    unsigned globally_static_type = locally_static_type || _subtype_indication->IsGloballyStatic() ;

    // Check if we are dealing with an unconstrained array type
    unsigned is_unconstrained = 0 ;

    // Check if the type_mark is an unconstrained array type.
    // Viper 7505: Also check if the record type has unconstrained elements
    if (type && (type->IsUnconstrainedArrayType() || type->IsRecordType())) {
        // Check if the type mark is a single identifier (the unconstrained type)
        // (CHECK ME : can do a better test : array_type(integer <>) is also unconstrained.
        //VhdlIdDef *basetype = _subtype_indication->FindFullFormal() ;
        //if (basetype) is_unconstrained = 1 ;
        // VIPER #4544 : Check wheather subtype indication is unconstrained or not.
        // If yes we have to mark declared identifiers unconstrained.
        if (_subtype_indication->IsUnconstrained(1)) {
            is_unconstrained = 1 ;
        }
    }
    _id->DeclareSubtype(type, res_func, is_unconstrained, locally_static_type, globally_static_type) ;
    _id->SetDeclaration(this) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    _id->SetSubtypeIndication(_subtype_indication) ;
#endif
}
VhdlSubtypeDecl::~VhdlSubtypeDecl()
{
    delete _id ;
    delete _subtype_indication ;
}

VhdlFullTypeDecl::VhdlFullTypeDecl(VhdlIdDef *id, VhdlTypeDef *type_def)
  : VhdlDeclaration(),
    _id(id),
    _type_def(type_def)
{
    // Do declaration work for 'id' by executing type_def
    // We need to find out what sort of type is declared
    _type_def->Declare(_id) ;
    // VIPER #7343 : Set declaration back pointer to type id
    if (_id) _id->SetDeclaration(this) ;
}
VhdlFullTypeDecl::~VhdlFullTypeDecl()
{
    // Delete the contents of the construct
    delete _type_def ;
    // Delete the identifier after the contents of the construct
    delete _id ;
}
unsigned VhdlFullTypeDecl::IsProtectedTypeDefBody() const
{
    return (_type_def && _type_def->IsProtectedTypeDefBody()) ? 1: 0 ;
}

VhdlIncompleteTypeDecl::VhdlIncompleteTypeDecl(VhdlIdDef *id)
  : VhdlDeclaration(),
    _id(id)
{
    if (_id) _id->SetDefinedIncompleteType() ;
    // Nothing to do ; id is declared as a type (unknown type)
}
VhdlIncompleteTypeDecl::~VhdlIncompleteTypeDecl()
{
    delete _id ;
}

VhdlConstantDecl::VhdlConstantDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign)
  : VhdlDeclaration(),
    _id_list(id_list),
    _subtype_indication(subtype_indication),
    _init_assign(init_assign),
    _type(0)
{
    if (!_subtype_indication) return ;
    _type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;

    // LRM 4.3.1.1
    // VIPER #3468 : LRM 4.3.1.1 : It is an error if declared constant is of composite
    // type that has a subelement that is a file type, an access type, or a protected type.
    Set type_recustion_set ;
    if (_type && _type->IsTypeContainsFile(1/* consider access type*/, type_recustion_set)) {
        Error("constant declaration cannot be of a file type or access type") ;
    }

    // VIPER 1582 : Type-infer the init_assign before we declare the identifiers
    if (_init_assign && _type) (void) _init_assign->TypeInfer(_type,0,VHDL_READ,0) ;

    unsigned is_package = 0 ;
    if (!_init_assign) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        is_package = scope_id ? scope_id->IsPackage() : 0 ;
    }

    unsigned locally_static = _init_assign ? _init_assign->IsLocallyStatic() : 0 ;
    unsigned globally_static = _init_assign ? _init_assign->IsGloballyStatic() : 1 ; // VIPER Issue #6349

    // LRM sections 7.4.1 and 7.4.2
    // Viper #6402 : A locally static initial value implies a locally static type : the initial value will constrain the unconstrained type
    //             : I think the initial value itself need not be locally static, but keep it for now.
    unsigned is_locally_static_type = (locally_static && _subtype_indication->IsUnconstrained(1)) || _subtype_indication->IsLocallyStatic() ;
    unsigned is_globally_static_type = is_locally_static_type || _subtype_indication->IsGloballyStatic() ;

    // Viper 7782: Constant ids having unconstrained array type in subprogram are not globally static.
    // Change similar to 3700/6058 done here for variables
    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;
    if (subprog) {
        // constant id's array types with unconstrained range are not static
        VhdlIdDef *type_id = _subtype_indication->GetId() ;
        if (!is_locally_static_type && type_id && type_id->IsUnconstrainedArrayType() && !_subtype_indication->GetAssocList()) is_globally_static_type = 0 ;
    }

    // Viper #7965 : LRM Section 7.4.2 Notes 2 specify that a constant can be
    // globally static even if its type or initial expression is not static.
    // However for 2008 the type needs to be globally static
    // 2008 LRM 9.4.3 defines this.
    // However, for dynamic elaboration named entity, we check init expression
    // Following somewhat deviates from the LRM
    if (!globally_static) globally_static = subprog ? 0 : IsVhdl2008() ? is_globally_static_type : 1 ;

    unsigned i ;
    VhdlIdDef *id ;
    //Viper 4510. Set is used to store the Ids if the deferred constant full declaration uses itself in initialization
    Set *unit_ids = new Set(POINTER_HASH) ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        (void) unit_ids->Insert(id) ;
        id->DeclareConstant(_type, locally_static, 0, globally_static, is_locally_static_type, is_globally_static_type) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        id->SetSubtypeIndication(_subtype_indication) ;
#endif
        // If it has initial value assignment,
        // but the id does not know that yet, set it.
        if (_init_assign && !id->Decl()) {
            id->SetDecl(id) ; // normal constant
        }

        VhdlIdDef *exist = id->Decl() ;
        if (!_init_assign && exist) {
            // We created a deferred-constant loop, relying on this id
            // to provide the initial value. But it has none !
            id->Error("deferred constant %s needs an initial value", id->Name()) ;
        }
 
        // Check added for Viper #3694 : LRM : 2.6
        VhdlIdDef *exist_type = exist ? exist->Type() : 0 ;
        // Viper 4509 for deferred constants we must match the exact type
        if (_type && exist_type && _type != exist_type) id->Error("types do not conform for deferred constant %s", id->Name()) ;

        // LRM 4.3.1.1 : deferred-constant declaration only allowed in package decl
        if (!_init_assign && !is_package) id->Error("deferred constant %s is allowed only in package declaration", id->Name()) ;
    }
    // Check for deffered constants Viper 3505
    if (_init_assign) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _init_assign->CheckLegalDefferedConst(_init_assign, unit_ids) ;
    }
    delete unit_ids ; // cleanup prior to return
}
VhdlConstantDecl::~VhdlConstantDecl()
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) delete id ;
    delete _id_list ;
    delete _subtype_indication ;
    delete _init_assign ;
}

VhdlSignalDecl::VhdlSignalDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication, unsigned signal_kind, VhdlExpression *init_assign)
  : VhdlDeclaration(),
    _id_list(id_list),
    _subtype_indication(subtype_indication),
    _signal_kind(signal_kind),
    _init_assign(init_assign)
{
    if (!_subtype_indication) return ;
    VhdlIdDef *type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;
    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;
    // Check for deffered constants Viper 4368
    if (!subprog) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _subtype_indication->CheckLegalDefferedConst(_subtype_indication, 0) ;
    }

    // LRM 4.3.1.2 (181) :
    // Viper #6499: used ContainsAccessType to search recursively
    if (type && (type->ContainsAccessType() || type->IsFileType())) {
        Error("signal declaration cannot be of a file type or access type") ;
    }

    VhdlIdDef *res_func = _subtype_indication->FindResolutionFunction() ;

    // LRM 4.3.1.2 (182)
    if (_signal_kind && type && type->IsScalarType()) {
        if (!type->ResolutionFunction() && !res_func) {
            Error("guarded signal declaration of a scalar type should have a resolution function") ;
        }
    }

    // Type-infer the init_assign before we declare the identifiers :
    if (_init_assign && type) (void) _init_assign->TypeInfer(type,0,VHDL_READ,0) ;
    // Viper 4368
    if (_init_assign) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _init_assign->CheckLegalDefferedConst(_init_assign, 0) ;
    }

    // LRM sections 7.4.1 and 7.4.2
    unsigned is_locally_static_type = _subtype_indication->IsLocallyStatic() ;
    unsigned is_globally_static_type = is_locally_static_type || _subtype_indication->IsGloballyStatic() ;

    // Viper 7782: Constant ids having unconstrained array type in subprogram are not globally static.
    // Change similar to 3700/6058 done here for variables
    if (subprog) {
        // Signal id's array types with unconstrained range are not static
        VhdlIdDef *type_id = _subtype_indication->GetId() ;
        if (!is_locally_static_type && type_id && type_id->IsUnconstrainedArrayType() && !_subtype_indication->GetAssocList()) is_globally_static_type = 0 ;
    }

    Set type_recustion_set ;
    unsigned is_file_or_access = type && type->IsTypeContainsFile(1, type_recustion_set) ; // Viper #8328

    // Now declare the identifiers. This will make them 'visible' (LRM 10.3) after the declaration.
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        id->DeclareSignal(type, res_func, is_locally_static_type, is_globally_static_type, _signal_kind) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        id->SetSubtypeIndication(_subtype_indication) ;
#endif

        // Check if this signal is a candidate for dual port RAM inferencing.
        //  - Presently, since we only allow a single-index(address)range RAM in elaboration,
        //    with only one 'use' (filtered in analysis),
        //    we consequently only allow single-dimensional arrays.
        //  - Also, exclude array-of-bit as RAM. Just go for array-of-array.
        //    That's not a hard requirement, but it rules out massive amount of (small) RAM detection.
        //  - Also, NEVER infer a RAM for a Port (we need all the bits there).
        //  - Finally, if there is an initial assignment to the signal, we assume it's not a RAM.
        //    (RAMs don't get initialized).
        // 3/2005: RD: Issue 2041 shows example of a RAM with initial value, which should be extracted.
        // Since initial values are not synthesizable any way (unless signal is not assigned), we can extract RAM.
        // So : Removed dependency !_init_assign from the condition.
        //
        // Later, we might even annotate initial value on the RAM instance if required by some application..(to be done).
        if (type && type->Dimension()==1 &&
            type->ElementType() && type->ElementType()->Dimension()>=1) {
            // Further analysis of the usage of this signal (during analysis)
            // might discard it as a RAM. But for now, it's a candidate.
            id->SetCanBeDualPortRam() ;
        }

        unsigned type_dims = type ? type->Dimension() : 0 ;
        VhdlIdDef *ele_type = type ? type->ElementType() : 0 ;
        unsigned m_dim_array = (type_dims > 1) ;
        unsigned array_of_array = (type_dims >= 1) && ele_type && (ele_type->Dimension()>=1) ;
        // Multi-port RAM : any array value will do.
        // Element type is now irrelevant : elaborator can work with any element type.
        // Restrict to array-of-array types for now, or else we will create massive amount of RAMs.
        if (type && (array_of_array || m_dim_array) // Viper #7152: for multi-dim array
            && !type->IsTypeContainsRecord() /* condition added for Viper #3751 */
            && !is_file_or_access /* condition added for Viper #8328 */)
        {
            id->SetCanBeMultiPortRam() ;
        }
    }

    // Check VHDL semantics
    if (_present_scope && _present_scope->GetSubprogram()) {
        Error("signal declaration is not allowed in a subprogram") ;
    }
}
VhdlSignalDecl::~VhdlSignalDecl()
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) delete id ;
    delete _id_list ;
    delete _subtype_indication ;
    delete _init_assign ;
}

VhdlVariableDecl::VhdlVariableDecl(unsigned shared, Array *id_list, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign)
  : VhdlDeclaration(),
    _shared(shared),
    _id_list(id_list),
    _subtype_indication(subtype_indication),
    _init_assign(init_assign)
{
    // Viper 7220 : Extract from 2008 LRM Section  6.4.2.4
    // Variables declared immediately within entity declarations, architecture
    // bodies,blocks, and generate statements shall be shared variables. Variables declared
    // immediately within subprograms and processes shall not be shared variables.
    // Variables declared immediately within a package shall be not be shared
    // variables if the package is declared within a subprogram, process, or
    // protected type body; otherwise, the variables shall be shared variables.
    // Variables declared immediately within a protected type body shall not be
    // shared variables.

    unsigned package_under_process_sub_prot = 0 ;
    VhdlIdDef *owner = (_present_scope) ? _present_scope->GetOwner() : 0 ;
    unsigned is_protected_body_scope =  owner ?  owner->IsProtectedBody() : 0 ;
 
    if (IsVhdl2008() && owner && owner->IsPackage()) {
        VhdlScope *scope = (_present_scope) ? _present_scope->Upper() : 0 ;
        while (scope) {
            owner = scope->GetOwner() ;
            if ((owner && (owner->IsSubprogram() || owner->IsProtectedBody())) || scope->IsProcessScope()) {
                package_under_process_sub_prot = 1 ;
                break ;
            }
            scope = scope->Upper() ;
        }
    }

    // Some left-over syntax check that was not done before
    // Shared variables can only occur in concurrent areas (no dataflow)
    if (_present_scope) {
        if (_present_scope->IsSubprogramScope() || _present_scope->IsProcessScope() || package_under_process_sub_prot || is_protected_body_scope) {
            if (_shared) {
                if (package_under_process_sub_prot) {
                    Error("variable in package inside process,subprogram or protected block cannot be 'shared'") ;
                } else {
                    Error("variable in subprogram or process cannot be 'shared'") ;
                }
            }
        } else if (!_shared) {
            Error("variable outside of subprogram or process must be 'shared'") ;
        }
    }

    if (!_subtype_indication) return ;
    VhdlIdDef *type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;
    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;
    // Check for deffered constants Viper 4368
    if (!subprog) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _subtype_indication->CheckLegalDefferedConst(_subtype_indication, 0) ;
    }

    // LRM 4.3.1.3
    if (type && type->IsFileType()) {
        Error("variable declaration cannot be of a file type") ;
    }
    // VIPER #7921: Shared variables must be of protected type in vhdl mode
    // above 93
    if (_shared && type && !type->IsProtected() && !IsVhdl93() && !IsVhdl87()) {
        Warning("shared variables must be of a protected type") ;
    }

    // Type-infer the init_assign before we declare the identifiers :
    if (_init_assign && type) (void) _init_assign->TypeInfer(type,0,VHDL_READ,0) ;
    // Viper 4368
    if (_init_assign) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _init_assign->CheckLegalDefferedConst(_init_assign, 0) ;
    }

    // LRM sections 7.4.1 and 7.4.2
    unsigned is_locally_static_type = 0 ;
    unsigned is_globally_static_type = 0 ;
    if (_subtype_indication->IsLocallyStatic()) {
        is_locally_static_type = 1 ;
    } else if (_subtype_indication->IsGloballyStatic()) {
        is_globally_static_type = 1 ;
    }
    
    // Viper 7782: Constant ids having unconstrained array type in subprogram are not globally static.
    // Change similar to 3700/6058 done here for variables
    if (subprog) {
        // Variable id's array types with unconstrained range are not static
        VhdlIdDef *type_id = _subtype_indication->GetId() ;
        if (!is_locally_static_type && type_id && type_id->IsUnconstrainedArrayType() && !_subtype_indication->GetAssocList()) is_globally_static_type = 0 ;
    }

    Set type_recustion_set ;
    unsigned is_file_or_access = type && type->IsTypeContainsFile(1, type_recustion_set) ; // Viper #8328

    // Now declare the identifiers. This will make them 'visible' (LRM 10.3) after the declaration.
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        id->DeclareVariable(type, is_locally_static_type, is_globally_static_type, shared) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        id->SetSubtypeIndication(_subtype_indication) ;
#endif

        // Issue 2041 :
        // Also, check if this variable is a candidate for RAM inferencing.
        //  - Presently, since we only allow a single-index(address)range RAM in elaboration,
        //    with only one 'use' (filtered in analysis),
        //    we consequently only allow single-dimensional arrays.
        //  - Also, exclude array-of-bit as RAM. Just go for array-of-array.
        //    That's not a hard requirement, but it rules out massive amount of (small) RAM detection.
        //  - Also, NEVER infer a RAM for a Port (we need all the bits there).
        // Issue 2041 : Allow RAM extraction, even if there is an initial value assignment... Not sure if this is smart...
        if (type && type->Dimension()==1 &&
            type->ElementType() && type->ElementType()->Dimension()>=1) {
            // Further analysis of the usage of this signal (during analysis)
            // might discard it as a RAM. But for now, it's a candidate.

            // Do NOT create dual-port RAMs for a temporary dataflow like a subprogram :
            if (_present_scope && !_present_scope->GetSubprogram()) {
                id->SetCanBeDualPortRam() ;
            }
        }

        unsigned type_dims = type ? type->Dimension() : 0 ;
        VhdlIdDef *ele_type = type ? type->ElementType() : 0 ;
        unsigned m_dim_array = (type_dims > 1) ;
        unsigned array_of_array = (type_dims >= 1) && ele_type && (ele_type->Dimension()>=1) ;
        // Multi-port RAM : any array value will do.
        // Element type is now irrelevant : elaborator can work with any element type.
        // Restrict to array-of-array types for now, or else we will create massive amount of RAMs.
        if (type && (array_of_array || m_dim_array) // Viper #7152: for multi-dim array
            && !type->IsTypeContainsRecord() /* condition added for Viper #3751 */
            && !is_file_or_access /* condition added for Viper #8328 */)
        {
            // Do NOT create multi-port RAMs for a temporary dataflow like a subprogram :
            if (_present_scope && !_present_scope->GetSubprogram()) {
                id->SetCanBeMultiPortRam() ;
            }
        }
    }
}
VhdlVariableDecl::~VhdlVariableDecl()
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) delete id ;
    delete _id_list ;
    delete _subtype_indication ;
    delete _init_assign ;
}

VhdlFileDecl::VhdlFileDecl(Array *id_list, VhdlSubtypeIndication *subtype_indication, VhdlFileOpenInfo *file_open_info)
  : VhdlDeclaration(),
    _id_list(id_list),
    _subtype_indication(subtype_indication),
    _file_open_info(file_open_info)
{
    if (!_subtype_indication) return ;
    VhdlIdDef *type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        id->DeclareFile(type) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        id->SetSubtypeIndication(_subtype_indication) ;
#endif
    }
}
VhdlFileDecl::~VhdlFileDecl()
{
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) delete id ;
    delete _id_list ;
    delete _subtype_indication ;
    delete _file_open_info ;
}

VhdlAliasDecl::VhdlAliasDecl(VhdlIdDef *designator, VhdlSubtypeIndication *subtype_indication, VhdlName *alias_target_name)
  : VhdlDeclaration(),
    _designator(designator),
    _subtype_indication(subtype_indication),
    _alias_target_name(alias_target_name)
{
    VhdlIdDef *type = 0 ;
    if (_subtype_indication) {
        type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;
    }

    if (!_alias_target_name) return ; // something bad happened.

    // Match or Get the type against the alias target name
    // If _alias_target_name is an index_name pass environment as VHDL_range
    // if the target is an external name it is illegal to have the subtype indication again.
    if (_alias_target_name->IsExternalName() && _subtype_indication) Warning("illegal subtype indication in alias declaration for external name") ;
    type = _alias_target_name->TypeInfer(type,0,0,0) ;

    // Do the following object/non-object alias checks, as specified by LRM 4.3.3 (VIPER #1259)
    VhdlIdDef *target_id = _alias_target_name->FindAliasTarget() ; // (used to be FindFormal(), but that's not right)  ;

    // LRM sections 7.4.1 and 7.4.2
    unsigned is_locally_static_type = 2 ; // don't know yet
    unsigned is_globally_static_type = 2 ; // don't know yet
    if (_subtype_indication) {
        is_locally_static_type = _subtype_indication->IsLocallyStatic() ;
        is_globally_static_type = is_locally_static_type || _subtype_indication->IsGloballyStatic() ;
    } else {
        // VIPER #7457 : Check 'is_locally_static_type' and 'is_globally_static_type' from _alias_target_name
        // LRM 1076-2008 section 6.6.2
        // 1. If the alias designator denotes a slice of an object, then the slice of the
        //    object is viewed as if it were of the subtype specified by the slice.
        // 2. If the name is an external name, then the object is viewed as if it were of
        //    the subtype specified in the external name.
        // 3. Otherwise, the object is viewed as if it were of the subtype specified in the
        //    declaration of the object denoted by the name.
        if (_alias_target_name->IsArraySlice()) {
            Array *assoc_list = _alias_target_name->GetAssocList() ;
            unsigned i ;
            VhdlDiscreteRange *assoc ;
            unsigned is_locally_static = 1, is_globally_static = 1 ;
            FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
                if (!assoc) continue ;
                if (!assoc->IsLocallyStatic()) is_locally_static = 0 ;
                if (!assoc->IsGloballyStatic()) is_globally_static = 0 ;
            }
            is_locally_static_type = is_locally_static ;
            is_globally_static_type = is_locally_static || is_globally_static ;
        } else if (_alias_target_name->IsExternalName()) {
            VhdlSubtypeIndication *subtype = _alias_target_name->GetSubtypeIndication() ;
            if (subtype) {
                is_locally_static_type = subtype->IsLocallyStatic() ;
                is_globally_static_type = is_locally_static_type || subtype->IsGloballyStatic() ;
            }
        } else if (target_id) {
            is_locally_static_type = target_id->IsLocallyStaticType() ;
            is_globally_static_type = is_locally_static_type || target_id->IsGloballyStaticType() ;
        }
    }

    // First see if this is an 'object' alias or not :
    if (target_id && (target_id->IsConstant() || target_id->IsVariable() || target_id->IsSignal() || target_id->IsFile())) {
        // Test LRM 4.3.3.1 Object aliases rules :

        // FIX ME : Need to add a check to make sure that designator is a static name
        //          This needs to be during elaboration!

        // A signature may not appear in a declaration of an object alias
        if (_alias_target_name->GetSignature()) {
            _alias_target_name->Error("signature may not appear in a declaration of an object alias") ;
        }

        // Viper #3700 : LRM 4.3.3.1(b) : An object alias name must be a static name
        if(!_alias_target_name->IsGloballyStaticName()) {
            _alias_target_name->Error("an alias object must be a static name") ;
        }

        // Viper #3875 : LRM 4.3.3 : character/string literal designator must be for non-object alias
        const char *designator_name = designator->Name() ;
        if (designator_name && ((*designator_name == '\'') || (*designator_name == '"'))) {
            Error("character/string literal may not be designator for an object alias") ;
        }

        // The base type of the alias_target_name must be the same as the base type of the type mark
        // in the subtype indication (if present); this type must not be a multi-dimensional array.
        // VIPER #7784 : Multi-dimensional array is allowed in vhdl-2008 mode
        if ((type && type->Dimension() > 1) && !IsVhdl2008()) { // Viper #3634, LRM 3.3.4.1
            Error("base type of object alias declaration must not be a multi-dimensional array type") ;
        }

        if (_subtype_indication && _subtype_indication->GetTypeMark()) {
            VhdlIdDef *base_type = _subtype_indication->GetTypeMark()->TypeMark() ;
            // VIPER #3702 :  Calling TypeMatch in-stead of hard ==
            if (base_type && !base_type->TypeMatch(type)) {
            // if (type != base_type) {
                Error("base type of object alias declaration must be the same of subtype indication's type mark") ;
            }
#if 0 // Check now done above, for Viper #3634
            if (base_type->Dimension() > 1) { // No need to check if base_type->IsArray, since Dimension will return 0 if it's not one.
                Error("base type of object alias declaration must not be a multi-dimensional array type") ;
            }
#endif
        }
    } else {
        // Test LRM 4.3.3.2 Nonobject aliases rules :

        // Labels, loop parameters, and generate parameters can't be aliased
        if (target_id && target_id->IsLabel()) {
            // FIX ME : need to find check for loop and generate parameters.
            Error("illegal nonobject alias declaration target") ;
        }
        // A subtype indication may not appear in a nonobject alias declaration
        if (target_id && _subtype_indication) {
            Error("subtype indication may not appear in a nonobject alias declaration") ;
        }
        // A signature is required if the nonobject alias name denotes a subprogram or enum literal
        // FIX ME : Need to add check that this signature matches the parameter and result type profile
        //          of exactly one of the subprograms or enumeration literals denoted by the name.
        if (target_id && (target_id->IsSubprogram() || target_id->IsEnumerationLiteral()) && !_alias_target_name->GetSignature()) {
            Error("signature is required if the nonobject alias name denotes a subprogram or enumeration literal") ;
        }

        // The following situations are check and executed :
        //
        // 1) If the _alias_target_name denotes an enumeration type, then one implicit alias
        //    declaration for each of the enum literals follows.  Each such implicit declaration
        //    uses the enum literal as its alias designator, and the enum literal's simple name
        //    as the target name. (VIPER #2306)
        //
        // 2) If the _alias_target_name denotes a physical type, then one implicit alias
        //    declaration for each of the units follows.  Each such implicit declaration uses
        //    the unit as its alias designator, and the unit's simple name as the target name.
        if (target_id && (target_id->IsEnumerationType() || target_id->IsPhysicalType())) {
            VERIFIC_ASSERT(_present_scope) ; // This should never fail!
            MapIter mi ;
            VhdlIdDef *literal ;
            FOREACH_MAP_ITEM(target_id->GetTypeDefList(), mi, 0, &literal) {
                if (!literal || _present_scope->IsDeclaredHere(literal)) continue ;
                // Viper 8019: Declare the implicit alias for literals as per LRM 4.3.3.2
                // earlier we used to declare the literal in this scope. this is wrong.
                // These new ids are stored in the _predefined map of the scope
                // these new alias ids are owned by scope and deleted in the destructor
                // of the scope.
                _present_scope->DeclareImplicitAlias(literal, _alias_target_name) ;
            }
        }

        // 3) If the _alias_target_name denotes a type, then implicit alias declarations for
        //    each predefined operator follows.
        // CHECK ME : Is this check needed to make operators visible?

        // Apart from these checks, there is nothing more to do in analysis.
        // This 'implicit' aliases which the LRM refers to for alias of types
        // is resolved in type-inference by picking up the alias-ed identifier during type checking.
    }

    _designator->DeclareAlias(type, _alias_target_name, is_locally_static_type, is_globally_static_type, _alias_target_name->IsExternalName() ? _alias_target_name->GetClassType():0) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    _designator->SetSubtypeIndication(_subtype_indication) ;
#endif
}
VhdlAliasDecl::~VhdlAliasDecl()
{
    delete _designator ;
    delete _subtype_indication ;
    delete _alias_target_name ;
}

VhdlComponentDecl::VhdlComponentDecl(VhdlIdDef *id, Array *generic_clause, Array *port_clause, VhdlScope *local_scope)
  : VhdlDeclaration(),
    _id(id),
    _generic_clause(generic_clause),
    _port_clause(port_clause),
    _local_scope(local_scope),
    _timestamp(0)
    ,_orig_component(0)
{
    // Set the time stamp
    _timestamp = (unsigned long) ::time(0) ;

    // Let the id point back to this component tree :
    id->SetComponentDecl(this) ;

    // Set the interface objects as specific kind of interface class
    Array *ids ;
    VhdlInterfaceDecl *decl ;
    unsigned i ;

    Array *generic_list = 0 ;
    if (_generic_clause) {
        generic_list = new Array(_generic_clause->Size()) ;
        VhdlIdDef *gen_id ;
        FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these generics to the order-based generics array
            if (ids) generic_list->Append(ids) ;
            gen_id = decl->GetId() ; // interface type/subprogram/package
            if (gen_id) generic_list->InsertLast(gen_id) ;
        }
    }
    Array *port_list = 0 ;
    if (_port_clause) {
        port_list = new Array(_port_clause->Size()) ;
        FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
            if (!decl) continue ;
            ids = decl->GetIds() ;
            // Add these ports to the order-based ports array
            port_list->Append(ids) ;
        }
    }
    // generic_list and port_list are absorbed by the following id
    _id->DeclareComponentId(generic_list, port_list) ;
}
VhdlComponentDecl::~VhdlComponentDecl()
{
    // Delete the contents of the construct
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_generic_clause,i,elem) delete elem ;
    delete _generic_clause ;
    FOREACH_ARRAY_ITEM(_port_clause,i,elem) delete elem ;
    delete _port_clause ;
    // Delete the scope after the contents of the construct
    delete _local_scope ;
    // Delete the identifier after the scope that it owns.
    delete _id ;
    Strings::free(_orig_component) ;
}

VhdlAttributeDecl::VhdlAttributeDecl(VhdlIdDef *id, VhdlName *type_mark)
  : VhdlDeclaration(),
    _id(id),
    _type_mark(type_mark)
{
    VhdlIdDef *type = _type_mark->TypeInfer(0,0,VHDL_subtyperange,0) ;

    Set type_recustion_set ;
    if (type && type->IsTypeContainsFile(1 /*also consider access type*/, type_recustion_set)) {
        Error("attribute may not be of file or access type") ; // Viper #3626
    }

    _id->DeclareAttribute(type) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
    _id->SetSubtypeIndication(_type_mark) ;
#endif
}
VhdlAttributeDecl::~VhdlAttributeDecl()
{
    delete _id ;
    delete _type_mark ;
}

VhdlAttributeSpec::VhdlAttributeSpec(VhdlName *designator, VhdlSpecification *entity_spec, VhdlExpression *value)
  : VhdlDeclaration(),
    _designator(designator),
    _entity_spec(entity_spec),
    _value(value)
{
    // Check type of expression against type of the attribute
    VhdlIdDef *attr = _designator->FindSingleObject() ;
    if (!attr) return ;
    attr->AddAttributeSpec(this) ; // Viper #5432
    if (!attr->IsAttribute()) {
        Error("%s is not an attribute", attr->Name()) ;
        // Viper 7735: if attr is not an attribute then the _designator
        // will incorrectly point to something else like signal or variable
        // this something else is not owned by the spec and may bedeleated earlier.
        // Hence when we reach the destructor we will encounter a invalid mem read.
        // Decided to invalidate the specification completely.
        InvalidateSpec() ;
        return ;
    }
    if (!_value) return ;

    // Infer value expression with attribute type
    (void) _value->TypeInfer(attr->Type(),0,VHDL_READ,0) ;

    // Quick hack : Now that we still have scope available,
    // test if the Exemplar synthesis directives are being set
    // here. The directives are set on declared variables in a
    // subprogram body.
    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;
    // Viper #4343 : Added check vhdl_file::GetIgnoreAllPragmas
    // Viper #4463 : GetIgnoreAllPragmas should be done only for the functions.
    // For procedures, we set it on the textio readline/read/hread/oread/write etc
    // and we count on this attribute even for the simulation customers who normally
    // ignore the synthesis pragmas
    if (subprog && (!vhdl_file::GetIgnoreAllPragmas() || subprog->IsProcedure()) && Strings::compare_nocase(attr->Name(), "synthesis_return")) {
        // Find the function
        // Risky : need to evaluate the value : But assume it is a
        // string : So we can get its name
        const char *func_name = _value->Name() ;
        if (func_name) {
            // Find the function pragma that goes with that
            unsigned func = PragmaFunction(func_name) ;
            if (!func) {
                _value->Warning("unknown Exemplar directive %s",func_name) ;
            } else {
                subprog->SetPragmaFunction(func) ;
            }
        }
    } else if (subprog && Strings::compare_nocase(attr->Name(), "is_signed")) {
        const char *val = _value->Name() ;
        if (val && Strings::compare_nocase(val,"true")) {
            subprog->SetPragmaSigned() ;
        }
    }
    if (_entity_spec) _entity_spec->Validate(_present_scope, attr, _value) ;
}
VhdlAttributeSpec::~VhdlAttributeSpec()
{
    VhdlIdDef *attr = _designator ? _designator->FindSingleObject() : 0 ;
    if (attr) {
        // Viper #5432: cleanup to avoid stale pointers
        attr->RemoveAttributeSpec(this) ;
        Set *all_ids = _entity_spec ? _entity_spec->GetAllIds() : 0 ;

        SetIter i ;
        VhdlIdDef *id ;
        FOREACH_SET_ITEM(all_ids, i, &id) {
            if (!id) continue ;
            // id->RemoveAttribute(attr) ;
            Map *attributes = id->GetAttributes() ;
            if (attributes) (void) attributes->Remove(attr) ;
            // Viper 7578: For an error case this id will reside on another attr spec
            // and this id was deleted before that specification was deleted. And hence
            // resulted in a crash later here during deletion of the other attr spec.

            attr->RemoveIdFromAttributeSpec(id) ;
        }
    }
    delete _designator ;
    delete _entity_spec ;
    delete _value ;
}

void VhdlAttributeSpec::InvalidateSpec()
{
    // Viper #5432: When the attribute-id for this attribute-spec is
    // deleted, invalidate this spec. We could not delete this spec
    // as some decl-list still holds this spec
    VhdlIdDef *attr = _designator ? _designator->FindSingleObject() : 0 ;
    if (attr) {
        attr->RemoveAttributeSpec(this) ;
        Set *all_ids = _entity_spec ? _entity_spec->GetAllIds() : 0 ;

        SetIter i ;
        VhdlIdDef *id ;
        FOREACH_SET_ITEM(all_ids, i, &id) {
            if (!id) continue ;
            // id->RemoveAttribute(attr) ;
            Map *attributes = id->GetAttributes() ;
            if (attributes) (void) attributes->Remove(attr) ;
        }
    }
    delete _designator ;
    delete _entity_spec ;
    delete _value ;

    _designator = 0 ;
    _entity_spec = 0 ;
    _value = 0 ;
}

VhdlConfigurationSpec::VhdlConfigurationSpec(VhdlSpecification *component_spec, VhdlBindingIndication *binding)
  : VhdlDeclaration(),
    _component_spec(component_spec),
    _binding(binding)
{
    // Get the component from the spec, so we can check/infer the binding
    if (_binding && _component_spec) {
        // wait with type-inference and resolving of the component spec until ResolveSpecs().
        // Then we will resolve the labels AND do the type-inference of the component_spec.
    }
}
VhdlConfigurationSpec::~VhdlConfigurationSpec()
{
    delete _component_spec ;
    delete _binding ;
}

VhdlDisconnectionSpec::VhdlDisconnectionSpec(VhdlSpecification *guarded_signal_spec, VhdlExpression *after)
  : VhdlDeclaration(),
    _guarded_signal_spec(guarded_signal_spec),
    _after(after)
{
    // VIPER #6606 : Type infer the after expression
    if (_after) (void) _after->TypeInfer(0, 0, VHDL_read, 0) ;
}
VhdlDisconnectionSpec::~VhdlDisconnectionSpec()
{
    delete _guarded_signal_spec ;
    delete _after ;
}

VhdlGroupTemplateDecl::VhdlGroupTemplateDecl(VhdlIdDef *id, Array *entity_class_entry_list)
  : VhdlDeclaration(),
    _id(id),
    _entity_class_entry_list(entity_class_entry_list)
{
    if (_id) _id->SetGroupTemplateDeclaration(this) ;
}
VhdlGroupTemplateDecl::~VhdlGroupTemplateDecl()
{
    delete _id ;
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_entity_class_entry_list,i,elem) delete elem ;
    delete _entity_class_entry_list ;
}

VhdlGroupDecl::VhdlGroupDecl(VhdlIdDef *id, VhdlName *group_template_name)
  : VhdlDeclaration(),
    _id(id),
    _group_template_name(group_template_name)
{
    // VIPER #7447 : Type infer the group template name
    if (_group_template_name) (void) _group_template_name->TypeInfer(0, 0, VHDL_read, 0) ;
}
VhdlGroupDecl::~VhdlGroupDecl()
{
    delete _id ;
    delete _group_template_name ;
}
VhdlSubprogInstantiationDecl::VhdlSubprogInstantiationDecl(VhdlSpecification *inst_spec, VhdlName *uninstantiated_subprog_name, Array *generic_map_aspect)
    : VhdlDeclaration(),
    _subprogram_spec(inst_spec),
    _uninstantiated_subprog_name(uninstantiated_subprog_name),
    _generic_map_aspect(generic_map_aspect),
    _generic_types(0)
{
    VhdlIdDef *subprogram_inst_id = inst_spec ? inst_spec->GetId(): 0 ;

    // Get uninstantiated subprogram name
    VhdlIdDef *uninstantiated_subprog_id = (_uninstantiated_subprog_name) ? _uninstantiated_subprog_name->SubprogramAspect(0): 0 ;

    VhdlSpecification *spec = uninstantiated_subprog_id ? uninstantiated_subprog_id->Spec() : 0 ;

    if (spec && !spec->IsUninstantiatedSubprogram()) {
        // Viper 7730: Vhdl2008 LRM Section 4.2.1: If the subprogram header contains the reserved
        // word generic, a generic list, and no generic map aspect, the subprogram is called an
        // uninstantiated subprogram.
        Error("source of subprogram instantiation is not an uninstantiated subprogram") ;
    }

    // Check whether subprogram kind is same for both 'subprogram_inst_id' and 'uninstantiated_subprog_id'
    if (subprogram_inst_id && uninstantiated_subprog_id) {
        if ((subprogram_inst_id->IsFunction() != uninstantiated_subprog_id->IsFunction()) || !uninstantiated_subprog_id->IsSubprogram()) {
            if (_uninstantiated_subprog_name) _uninstantiated_subprog_name->Error("subprogram kind mismatch in subprogram instantiation") ;
        }
        //subprogram_inst_id->DeclareSubprogInstanceId(uninstantiated_subprog_id) ;
    }
    if (uninstantiated_subprog_id && _generic_map_aspect) {
        uninstantiated_subprog_id->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;
        _generic_types = new Map(POINTER_HASH) ;
        // Store generic types along with its actual type obtained from 'TypeInferAssociationList'.
        // This will help us to resolve selected names/subprogram calls of subprogram instance
        Array *generics = uninstantiated_subprog_id->GetGenerics() ;
        unsigned i ;
        VhdlIdDef *generic_id ;
        FOREACH_ARRAY_ITEM(generics, i, generic_id) {
            if (!generic_id || !generic_id->IsGenericType()) continue ;
            (void) _generic_types->Insert(generic_id, generic_id->ActualType()) ;
        }
    }
    // Set instance back pointer to subprogram identifier :
    if (subprogram_inst_id) subprogram_inst_id->SetSubprogInstanceDecl(this) ;
}
VhdlSubprogInstantiationDecl::~VhdlSubprogInstantiationDecl()
{
    delete _subprogram_spec ;
    delete _uninstantiated_subprog_name ;
    unsigned i ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) delete assoc ;
    delete _generic_map_aspect ;
    delete _generic_types ;
}
VhdlIdDef *VhdlSubprogInstantiationDecl::GetId() const
{
    return _subprogram_spec ? _subprogram_spec->GetId(): 0 ;
}
VhdlIdDef *VhdlSubprogInstantiationDecl::GetUninstantiatedSubprogId() const
{
    return _uninstantiated_subprog_name ? _uninstantiated_subprog_name->GetId(): 0 ;
}
void VhdlDeclaration::SetActualTypes() {}
void VhdlSubprogInstantiationDecl::SetActualTypes()
{
    MapIter mi ;
    VhdlIdDef *generic_id ;
    VhdlIdDef *actual_type ;
    FOREACH_MAP_ITEM(_generic_types, mi, &generic_id, &actual_type) {
        if (generic_id) generic_id->SetActualId(actual_type, 0) ;
    }
}

VhdlInterfaceDecl::VhdlInterfaceDecl(unsigned interface_kind, Array *id_list, unsigned mode, VhdlSubtypeIndication *subtype_indication, unsigned signal_kind, VhdlExpression *init_assign)
  : VhdlDeclaration(),
    _interface_kind(interface_kind),
    _id_list(id_list),
    _mode(mode),
    _subtype_indication(subtype_indication),
    _signal_kind(signal_kind),
    _init_assign(init_assign)
{
    // Check rules on interface declarations inside subprograms :
    VhdlIdDef *subprog = (_present_scope) ? _present_scope->GetSubprogram() : 0 ;
    if (subprog && _id_list) { // Check only for interface object decl
        if (subprog->IsFunction()) {
            // LRM 2.1.1 mode checks :
            if (_mode && !IsInput()) {
                Error("mode '%s' is not allowed for function parameters", EntityClassName(_mode)) ;
            }
            if (_interface_kind && _interface_kind!=VHDL_constant && _interface_kind!=VHDL_signal && _interface_kind!=VHDL_file) {
                Error("object kind '%s' is not allowed for function parameters", EntityClassName(_interface_kind)) ;
            }
        } else {
            // LRM 2.1.1 mode checks :
            if (_mode && !IsInput() && !IsOutput() && !IsInout()) {
                Error("mode '%s' is not allowed for subprogram parameters", EntityClassName(_mode)) ;
            }
            if (_interface_kind && _interface_kind!=VHDL_constant && _interface_kind!=VHDL_signal && _interface_kind!=VHDL_variable && _interface_kind!=VHDL_file) {
                Error("object kind '%s' is not allowed for subprogram parameters", EntityClassName(_interface_kind)) ;
            }
        }
    }

    // Set defaults in the id's, not in the tree

    // Type-infer the subtype indication
    // Viper 5298. We need to check for illegal usage of variable in the interface.
    // an object cannot be used within the same interface as it is declared
    // hence pass a new environment VHDL_interfacerange to the subtypeindication

    VhdlIdDef *type = (_subtype_indication) ? _subtype_indication->TypeInfer(0,0,VHDL_interfacerange,0) : 0 ;

    // Check for deffered constants Viper 4368
    if (!subprog && _subtype_indication) {
        VhdlIdDef *scope_id = (_present_scope) ? _present_scope->GetContainingPrimaryUnit() : 0 ;
        if (scope_id && scope_id->IsPackage()) _subtype_indication->CheckLegalDefferedConst(_subtype_indication, 0) ;
    }

    // LRM 4.3.2 interface declarations syntax checks that yacc cannot do :
    if (_interface_kind == VHDL_file && (_mode || _init_assign)) {
        Error("mode or default expression is illegal on a interface file declaration") ;
    }

    // LRM 4.3.1.2 (181-182) :
    if (_interface_kind == VHDL_signal && type && (type->IsAccessType() || type->IsFileType())) {
        Error("signal declaration cannot be of a file type or access type") ;
    }

    VhdlIdDef *res_func = (_subtype_indication) ? _subtype_indication->FindResolutionFunction() : 0 ;

    // LRM 4.3.1.2 (182)
    if (_signal_kind && type && type->IsScalarType()) {
        // There should be a explicit resolution function in the subtype indication,
        // or the subtype of the signal should have a resolution function.
        if (!type->ResolutionFunction() && !res_func) {
            Error("guarded signal declaration of a scalar type should have a resolution function") ;
        }
    }

    // Type-infer the init_assign before we declare the identifiers :
    // Viper 5761. We need to check for illegal usage of variable in the interface.
    // an object cannot be used within the same interface as it is declared
    // hence pass a new environment VHDL_interfaceread to the subtypeindication
    if (_init_assign && type) (void) _init_assign->TypeInfer(type,0,VHDL_interfaceread,0) ;

    if (_init_assign && !_init_assign->IsGloballyStatic()) {
        // VIPER #7536: Default initial value of generic is not globally static, produce warning:
        // This is according to IEEE 1076-2008 LRM section 9.4.3 on "Globally static primaries"
        //     NOTE 2: interface constant, variable, and signal declarations require
        //             that their initial value expressions be static expressions.
        // Here the kind can be constant, variable, signal and file. But for file,
        // there cannot be any initial expression. So always check it here.
        _init_assign->Warning("default expression of interface object is not globally static") ;
    }

    // Check if we are dealing with an unconstrained interface element :
    unsigned is_unconstrained = 0 ;

    // Check if the type_mark is an unconstrained array type.
    // Viper 7505: Also check if the record type has unconstrained elements
    if (type && (type->IsUnconstrainedArrayType() || type->IsRecordType())) {
        // Check if the type mark is a single identifier (the unconstrained type)
        // (CHECK ME : can do a better test : array_type(integer <>) is also unconstrained.
        //VhdlIdDef *subtype = (_subtype_indication) ? _subtype_indication->FindFullFormal() : 0 ;
        //if (subtype) is_unconstrained = 1 ;
        // VIPER #4544 : Check wheather subtype indication is unconstrained or not.
        // If yes we have to mark declared identifiers unconstrained.
        if (_subtype_indication && _subtype_indication->IsUnconstrained(1)) {
            is_unconstrained = 1 ;
        }
    }

    // LRM sections 7.4.1 and 7.4.2
    unsigned is_locally_static_type = 1 ;
    unsigned is_globally_static_type = 1 ;
    if (_subtype_indication) {
        is_locally_static_type = _subtype_indication->IsLocallyStatic() ;
        is_globally_static_type = is_locally_static_type || _subtype_indication->IsGloballyStatic() ;
        // Viper: #3700 / 6058 begin:
        // Interface array types with unconstrained range are not static
        VhdlIdDef *type_id = _subtype_indication->GetId() ;
        if (!is_locally_static_type && subprog && type_id && type_id->IsUnconstrainedArrayType() && !_subtype_indication->GetAssocList()) is_globally_static_type = 0 ;
        // Viper: #3700 / 6058 end:
    }

    Set type_recustion_set ;
    unsigned is_file_or_access = type && type->IsTypeContainsFile(1, type_recustion_set) ; // Viper #8328

    // Other checks have to wait until we known in which context this interface
    // declaration is used (when we know the 'default' interface kind and other settings).
    // (when we call SetObjectKind()).

    // Now declare the identifiers. This will make them 'visible' (LRM 10.3) after the declaration.
    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (!id) continue ;
        // Declare id with the proper defaults
        id->DeclareInterfaceId(type, _interface_kind, _mode, _signal_kind, (_init_assign) ? 1 : 0, is_unconstrained, res_func, is_locally_static_type, is_globally_static_type) ;

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
        id->SetSubtypeIndication(_subtype_indication) ;
#endif
        // Viper 7599: Mark the Ramibility of intefaceid. Discard
        // non input and non signal as no ref target(Viper #6871) are created for them in elaboration.
        // Check if this signal is a candidate for dual port RAM inferencing.
        //  - Presently, since we only allow a single-index(address)range RAM in elaboration,
        //    with only one 'use' (filtered in analysis),
        //    we consequently only allow single-dimensional arrays.
        //  - Also, exclude array-of-bit as RAM. Just go for array-of-array.
        //    That's not a hard requirement, but it rules out massive amount of (small) RAM detection.
        //  - Also, NEVER infer a RAM for a Port (we need all the bits there).
        //  - Finally, if there is an initial assignment to the signal, we assume it's not a RAM.
        //    (RAMs don't get initialized).
        // 3/2005: RD: Issue 2041 shows example of a RAM with initial value, which should be extracted.
        // Since initial values are not synthesizable any way (unless signal is not assigned), we can extract RAM.
        // So : Removed dependency !_init_assign from the condition.
        //
        // Later, we might even annotate initial value on the RAM instance if required by some application..(to be done).
        if (!subprog || id->IsInput() || !id->IsSignal()) continue ;
        unsigned type_dims = type ? type->Dimension() : 0 ;
        VhdlIdDef *ele_type = type ? type->ElementType() : 0 ;
        unsigned m_dim_array = (type_dims > 1) ;
        unsigned array_of_array = (type_dims >= 1) && ele_type && (ele_type->Dimension()>=1) ;
        // Multi-port RAM : any array value will do.
        // Element type is now irrelevant : elaborator can work with any element type.
        // Restrict to array-of-array types for now, or else we will create massive amount of RAMs.
        if (type && (array_of_array || m_dim_array) // Viper #7152: for multi-dim array
            && !type->IsTypeContainsRecord() /* condition added for Viper #3751 */
            && !is_file_or_access /* condition added for Viper #8328 */)
        {
            id->SetCanBeMultiPortRam() ;
        }
    }
}
VhdlInterfaceDecl::~VhdlInterfaceDecl()
{
    unsigned i ;
    VhdlTreeNode *elem ;
    FOREACH_ARRAY_ITEM(_id_list,i,elem) delete elem ;
    delete _id_list ;
    delete _subtype_indication ;
    delete _init_assign ;
}

unsigned VhdlInterfaceDecl::IsInput() const      { return (_mode == VHDL_in) ; }
unsigned VhdlInterfaceDecl::IsOutput() const     { return (_mode == VHDL_out) ; }
unsigned VhdlInterfaceDecl::IsInout() const      { return (_mode == VHDL_inout) ; }
unsigned VhdlInterfaceDecl::IsLinkage() const    { return (_mode == VHDL_linkage) ; }
unsigned VhdlInterfaceDecl::IsBuffer() const     { return (_mode == VHDL_buffer) ; }

void VhdlInterfaceDecl::SetMode(unsigned mode)
{
    // The incoming 'mode' argument should be either VHDL_in, VHDL_out, VHDL_inout, ,
    // VHDL_buffer, VHDL_linkage, or 0.  We will not do a check in here to assure
    // that 'mode' is a valid value.  These values are defined in vhdl_tokens.h.

    // Change the Mode of this IO declaration and all of its ids to 'mode'
    _mode = mode ;

    unsigned i ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM(_id_list,i,id) {
        if (id) id->SetMode(mode) ;
    }
}
VhdlIdDef *VhdlInterfaceDecl::GetId() const { return 0 ; }
VhdlInterfaceTypeDecl::VhdlInterfaceTypeDecl(VhdlIdDef *type_id)
    : VhdlInterfaceDecl(VHDL_type, (Array*)0, 0, 0, 0, 0),
    _type_id(type_id)
{
}
VhdlInterfaceTypeDecl::~VhdlInterfaceTypeDecl()
{
    delete _type_id ;
}
VhdlInterfaceSubprogDecl::VhdlInterfaceSubprogDecl(VhdlSpecification *subprog_specification, VhdlExpression *subprogram_default)
    : VhdlInterfaceDecl((subprog_specification && subprog_specification->IsFunction()) ? VHDL_function: VHDL_procedure, (Array*)0, 0, 0, 0, subprogram_default),
    _subprogram_spec(subprog_specification),
    _actual_subprog(0)
{
    VhdlIdDef *spec = _subprogram_spec ? _subprogram_spec->GetId(): 0 ;
    if (spec) {
        spec->SetAsGeneric() ;
        spec->SetSubprogInstanceDecl(this) ;
    }

    VhdlIdDef *default_subprog = (_init_assign) ? _init_assign->SubprogramAspect(0): 0 ;
    if (default_subprog && !default_subprog->IsSubprogram()) {
        Error("%s is not a subprogram", default_subprog->Name()) ;
    }
    // Default subprogram and interface subprogram should have conforming profiles
    // Conforming cannot be checked here, as interface subprogram's arg type/return
    // type can be specified in terms of interface type
    //if (default_subprog && !default_subprog->IsHomograph(spec)) {
        //Error("subprogram %s does not conform with its declaration", default_subprog->Name()) ;
    //}
}
VhdlInterfaceSubprogDecl::~VhdlInterfaceSubprogDecl()
{
    delete _subprogram_spec ;
}
VhdlIdDef *VhdlInterfaceSubprogDecl::GetId() const
{
    return _subprogram_spec ? _subprogram_spec->GetId(): 0 ;
}
void VhdlInterfaceSubprogDecl::SetActualId(VhdlIdDef *id) { _actual_subprog = id ; }
//VhdlIdDef *VhdlInterfaceSubprogDecl::GetActualId() const { return _actual_subprog ? _actual_subprog : (_init_assign ? _init_assign->SubprogramAspect(): 0) ; }
VhdlIdDef *VhdlInterfaceSubprogDecl::GetActualId() const { return _actual_subprog ; }
VhdlIdDef *VhdlInterfaceSubprogDecl::TakeActualId()
{
    VhdlIdDef *r = _actual_subprog ;
    _actual_subprog = 0 ;
    return r ;
}
VhdlIdDef *VhdlInterfaceSubprogDecl::GetDefaultSubprogram(VhdlScope *assoc_list_scope)
{
    VhdlIdDef *default_subprogram = (_init_assign) ? _init_assign->SubprogramAspect(0): 0 ;
    VhdlIdDef *formal = GetId() ;
    if (!formal) return 0 ;
    // Viper 7515 : For multiple match prune the ids by looking at the formal for the generic subrogram.
    // The same thing is also done under SubprogramAspect for Viper 7518 and 7519
    if (!default_subprogram && _init_assign && _init_assign->IsBox()) {
        // There shall be a subprogram directly visible at the place of generic association
        // list and has same designator as the formal and it has a conforming profile to
        // that of formal
        // VIPER #7543 : Search subprogram in the scope of generic association
        VhdlScope *old_scope = _present_scope ;
        _present_scope = assoc_list_scope ;
        VhdlIdRef actual(Strings::save(formal->Name())) ;
        Set ids(POINTER_HASH) ;
        (void) actual.FindObjects(ids, 0) ;
        formal->Prune(&ids) ;
        if (ids.Size() == 1) default_subprogram = (VhdlIdDef*)ids.GetLast() ;
        _present_scope = old_scope ;
    }
    if (default_subprogram) {
        if (!formal->IsHomograph(default_subprogram)) {
            Error("subprogram %s does not conform with its declaration", default_subprogram->Name()) ;
        }
    }
    return default_subprogram ;
}

VhdlInterfacePackageDecl::VhdlInterfacePackageDecl(VhdlIdDef *inst_id, VhdlName *uninst_package_name, Array *generic_map_aspect, VhdlScope *local_scope)
    : VhdlInterfaceDecl(VHDL_package, (Array*)0, 0, 0, 0, 0),
    _id(inst_id),
    _uninst_package_name(uninst_package_name),
    _generic_map_aspect(generic_map_aspect),
    _local_scope(local_scope),
    _initial_pkg(0),
    _uninst_package_id(0),
    _generic_types(0),
    _actual_pkg(0)
    ,_actual_elab_pkg(0)
{
    if (_id) _id->SetDeclaration(this) ;
    // Get the uninstantiated package from _uninst_package_name
    _uninst_package_id = (_uninst_package_name) ? _uninst_package_name->EntityAspect(): 0 ;
    if (_id) _id->SetUninstantiatedPackageId(_uninst_package_id) ;
    if (_uninst_package_name && _uninst_package_id && !_uninst_package_id->IsPackage()) {
        _uninst_package_name->Error("%s is not a %s", _uninst_package_id->Name(), "package") ;
    }
    if (_id && _uninst_package_id) {
        // Create special alias identifier for each locally declared
        // object of '_initial_pkg'
        CreateSpecialPackageElementIds() ;
    }
    if (_id && _generic_map_aspect) {
        // If _generic_map_aspect contains '<>' or 'default', do not type infer
        // association list
        VhdlDiscreteRange *assoc = (VhdlDiscreteRange*)_generic_map_aspect->At(0) ;
        if (!assoc || (!assoc->IsBox() && !assoc->IsDefault())) {
            _id->TypeInferAssociationList(_generic_map_aspect, VHDL_generic, 0, this) ;
            _generic_types = new Map(POINTER_HASH) ;
            // Store generic types along with its actual type obtained from 'TypeInferAssociationList'.
            // This will help us to resolve selected names/subprogram calls of package instance
            Array *generics = _id->GetGenerics() ;
            unsigned i ;
            VhdlIdDef *generic_id ;
            FOREACH_ARRAY_ITEM(generics, i, generic_id) {
                if (!generic_id || !generic_id->IsGenericType()) continue ;
                (void) _generic_types->Insert(generic_id, generic_id->ActualType()) ;
            }
        }
    }
}
void VhdlInterfacePackageDecl::CreateSpecialPackageElementIds()
{
    if (!_local_scope) return ;
    // Check whether '_uninst_package_name' is really a uninstantiated package i.e. a
    // package with generic clause and no generic map aspect.
    VhdlPrimaryUnit *unit = _uninst_package_id ? _uninst_package_id->GetPrimaryUnit(): 0 ;
    if (unit && _uninst_package_name && _uninst_package_id && (!unit->GetGenericClause() || unit->GetGenericMapAspect())) {
        _uninst_package_name->Error("%s is not an uninstantiated package", _uninst_package_id->Name()) ;
    }
    if (!unit) return ;
    // Copy the content of uninstantiated package and set to _initial_pkg
    VhdlMapForCopy old2new ;
    _initial_pkg = unit->CopyPrimaryUnit(0, old2new, 0) ;
    VhdlScope *pkg_scope = _initial_pkg ? _initial_pkg->LocalScope(): 0 ;
    Map *id_map = pkg_scope ? pkg_scope->DeclArea(): 0 ;
    VhdlIdDef *local_id ;
    VhdlPackageInstElement *new_id ;
    char *name ;
    MapIter mi ;
    Array *generics = 0 ;

    // Create alias identifier for each package element and connect to elements
    // of _initial_pkg
    FOREACH_MAP_ITEM(id_map, mi, &name, &local_id) {
        if (!local_id || (pkg_scope && (local_id == pkg_scope->GetOwner()))) continue ; // Skip package id
        new_id = new VhdlPackageInstElement(Strings::save(name)) ;
        if (local_id->IsGeneric()) {
            if (!generics) generics = new Array(1) ;
            generics->InsertLast(new_id) ;
        }
        _local_scope->ForceDeclare(new_id) ;
#ifdef VHDL_ID_SCOPE_BACKPOINTER
        // VIPER #7258 : Set back pointer
        // Set the owning scope of the identifier :
        new_id->SetOwningScope(_local_scope) ;
#endif
        // For alias identifier, set 'special alias identifier' created for
        // alias's target in 'special alias identifier' for original alias id
        if (local_id->IsAlias()) {
            VhdlIdDef *target = local_id->GetTargetId() ;
            VhdlIdDef *new_taregt = target ? _local_scope->FindSingleObjectLocal(target->Name()) : 0 ;
            new_id->SetTargetPkgInstElement(new_taregt) ;
        }
        new_id->ConnectActualId(local_id) ;
    }
    if (_id && generics) _id->DeclarePackageId(generics) ;
}
VhdlInterfacePackageDecl::~VhdlInterfacePackageDecl()
{
    delete _uninst_package_name ;
    unsigned i ;
    VhdlDiscreteRange *elem ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, elem) delete elem ;
    delete _generic_map_aspect ;
    delete _generic_types ;
    delete _id ;
    _uninst_package_id = 0 ;
    delete _initial_pkg ;
    _actual_pkg = 0 ;
    _actual_elab_pkg = 0 ;
    // This _local_scope contains iddefs owned by the scope, delete those
    VhdlIdDef *id ;
    MapIter mi ;
    Map *id_map = _local_scope ? _local_scope->DeclArea(): 0 ;
    FOREACH_MAP_ITEM(id_map, mi, 0, &id) delete id ;
    delete _local_scope ;
}
VhdlIdDef *VhdlInterfacePackageDecl::GetId() const
{
    return _id ;
}
VhdlIdDef *VhdlInterfacePackageDecl::GetActualId() const
{
    return _actual_pkg ;
}
VhdlIdDef *VhdlInterfacePackageDecl::TakeActualId()
{
    VhdlIdDef *r = _actual_pkg ;
    _actual_pkg = 0 ;
    return r ;
}
void VhdlInterfacePackageDecl::SetActualId(VhdlIdDef *id)
{
    _actual_pkg = id ;
    if (!_id || !_actual_pkg) return ;
    CheckFormalActualMismatch(_actual_pkg) ;
    AssociateFormalActualElementIds(_actual_pkg) ;
}
void VhdlInterfacePackageDecl::CheckGenericMapAspect(const VhdlIdDef *actual_pkg) const
{
    if (!_generic_map_aspect) return ; // Nothing to check
    // If generic map aspect of interface package decl contains '<>' or 'default'
    // no need to check actual generic map aspect. That will be done during
    // 'AssociateGenerics'.
    VhdlDiscreteRange *assoc = _generic_map_aspect ? (VhdlDiscreteRange*)_generic_map_aspect->At(0): 0 ;
    if (assoc && assoc->IsBox()) return ;
    if (assoc && assoc->IsDefault()) {
        // When generic map aspect contains 'default', each generic of uninstantiated
        // package should have default expression
        Array *generic_clause = _initial_pkg ? _initial_pkg->GetGenericClause(): 0 ;
        VhdlInterfaceDecl *decl ;
        unsigned i ;
        FOREACH_ARRAY_ITEM(generic_clause, i, decl) {
            if (!decl) continue ;
            //if (!decl->GetInitAssign() && !decl->GetDefaultSubprogram(_present_scope)) {
            if (!decl->GetDefaultSubprogram(_present_scope) && !decl->GetInitAssign()) {
                Error("default expression or subprogram is not specified in uninstantiated package for interface package element %s", _id ? _id->Name(): "") ;
            }
        }
    }

    if (!actual_pkg) return ; // No actual package, nothing to check

    // VIPER #7479: Check mismatch of actual values for value generics
    CheckGenericMapAspectValueMismatch(actual_pkg) ;
}

void VhdlInterfacePackageDecl::CheckFormalActualMismatch(VhdlIdDef *actual_pkg) const
{
    if (!actual_pkg || !_id) return ;
    // This API can be called with package-instance-id/package-interface-id
    // as argument (from AssociateGenerics) or with normal
    // copied package id as argument (during push/pop for recursion).
    // During elaboration of package instance, we will
    // create a copy of instantiated package and elaborated the copied
    // package. So we should use that elaborated package as actual
    // package here during elaboration (for recursion handling)
    if (!actual_pkg->IsPackageInst() && !(actual_pkg->IsPackage() && actual_pkg->IsGenericType())) return ;

    // First check wheather unisntantiated package name of formal matches with actual
    VhdlIdDef *formal_uninstantiated_pkg_id = _id->GetUninstantiatedPackageId() ;
    VhdlIdDef *actual_uninstantiated_pkg_id = actual_pkg->GetUninstantiatedPackageId() ;
    if (!actual_uninstantiated_pkg_id && !actual_pkg->IsPackageInst()) actual_uninstantiated_pkg_id = actual_pkg ;
    VhdlPrimaryUnit *formal_unit = formal_uninstantiated_pkg_id ? formal_uninstantiated_pkg_id->GetPrimaryUnit(): 0 ;
    VhdlPrimaryUnit *actual_unit = actual_uninstantiated_pkg_id ? actual_uninstantiated_pkg_id->GetPrimaryUnit(): 0 ;

    const char *formal_unit_name = formal_unit ? formal_unit->GetOriginalUnitName(): 0 ;
    if (!formal_unit_name) formal_unit_name = formal_uninstantiated_pkg_id ? formal_uninstantiated_pkg_id->Name(): 0 ;

    const char *actual_unit_name = actual_unit ? actual_unit->GetOriginalUnitName(): 0 ;
    if (!actual_unit_name) actual_unit_name = actual_uninstantiated_pkg_id ? actual_uninstantiated_pkg_id->Name(): 0 ;
    // Package name and its library must be same
    if (!formal_uninstantiated_pkg_id || !actual_uninstantiated_pkg_id ||
        (formal_unit_name && actual_unit_name && Strings::compare_nocase(formal_unit_name, actual_unit_name) == 0)) {
        Error("formal generic package %s does not match with actual", _id->Name()) ;
        return ;
    }
    const char *formal_pkg_lib_name = formal_uninstantiated_pkg_id->GetContainingLibraryName() ;
    const char *actual_pkg_lib_name = actual_uninstantiated_pkg_id->GetContainingLibraryName() ;
    if ((formal_pkg_lib_name && !actual_pkg_lib_name) ||
        (!formal_pkg_lib_name && actual_pkg_lib_name) ||
        (Strings::compare(formal_pkg_lib_name, actual_pkg_lib_name) == 0)) {
        Error("formal generic package %s does not match with actual", _id->Name()) ;
        return ;
    }

    // Check generic map association
    CheckGenericMapAspect(actual_pkg) ;
}
void VhdlInterfacePackageDecl::AssociateFormalActualElementIds(VhdlIdDef *actual_pkg) const
{
    // If coming actual_pkg is null, consider _initial_pkg
    VhdlIdDef *pkg_id = actual_pkg ;
    if (!pkg_id) pkg_id = _initial_pkg ? _initial_pkg->Id(): 0 ;
    // Associate this interface package decl with actual instantiated package
    // by setting the actual identifier of local ids from 'actual_pkg'
    // Get the local scope of actual package
    VhdlScope *actual_scope = pkg_id ? pkg_id->LocalScope(): 0 ;
    MapIter mi ;
    VhdlIdDef *formal, *actual ;
    char *name ;
    Map *local_map = _local_scope ? _local_scope->DeclArea(): 0 ;
    Map *actual_map = actual_scope ? actual_scope->DeclArea(): 0 ;
    unsigned idx = 0 ;
    FOREACH_MAP_ITEM(local_map, mi, &name, &formal) {
        actual = (actual_map && (actual_map->Size() > idx))? (VhdlIdDef*)actual_map->GetValueAt(idx): 0 ;
        if (actual == pkg_id) {
            idx++ ;
            actual = actual_map ? (VhdlIdDef*)actual_map->GetValueAt(idx): 0 ;
        }
        idx++ ;
        if (formal && actual) formal->ConnectActualId(actual) ;
    }
}

/******************************************************************/
//  Declaration of TypeDefs
/******************************************************************/

void VhdlScalarTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id || !_scalar_range) return ;

    // First time we declare a integer or real type we should, have universal
    // types in the scope. Otherwise, we cant do type-inference on the scalar
    // range of the type. Call them here to make sure universal types are there.
    (void) UniversalInteger() ;
    (void) UniversalReal() ;
    (void) UniversalVoid() ;

    // Scalar type has a discrete range. From that, we should determine if
    // its an integer or real type declaration, or an error.
    VhdlIdDef *subtype = _scalar_range->TypeInfer(0,0,VHDL_subtyperange,0) ;
    if (!subtype) return ; // Error will have been given...

    // Viper #3171 : LRM 3.1.2/3.1.4 range_bounds must be locally static
    if (!_scalar_range->IsLocallyStatic()) _scalar_range->Error("range constraint in scalar type definition must be locally static") ;

    if (subtype->IsIntegerType()) {
        type_id->DeclareIntegerType() ;
    } else if (subtype->IsRealType()) {
        type_id->DeclareRealType() ;
    } else {
        _scalar_range->Error("scalar type range should denote an integer or floating point type") ;
    }
}

void VhdlPhysicalTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id || !_range_constraint) return ;

    // As scalar type : Type infer the discrete range, and determine it's
    // integer type.  Also declare physical elements with type_id and type_id
    // with list of physical elements.
    if (StdType("time") == type_id && (sizeof(verific_int64) == 8)) { // Viper #6401
        verific_int64 max_time = 1 ;
        max_time = max_time << (sizeof(verific_int64)*8 - 1) ;
        max_time = ~max_time ; // = 2^63

        VhdlInteger *max = new VhdlInteger(max_time) ;
        // for backward compatibility of parse tree structure, add a unary operator
        VhdlOperator *left = new VhdlOperator(max, VHDL_MINUS, 0) ;
        VhdlInteger *right = new VhdlInteger(max_time) ;

        delete _range_constraint ;
        _range_constraint = new VhdlRange(left, VHDL_to, right) ;
    }
    VhdlIdDef *subtype = _range_constraint->TypeInfer(0,0,VHDL_subtyperange,0) ;
    if (!subtype) return ; // Error will have been given

    if (!subtype->IsIntegerType()) {
        _range_constraint->Error("physical range should denote an integer type") ;
    }

    // Viper #3171 : LRM 3.1.3 range_bounds must be locally static
    if (!_range_constraint->IsLocallyStatic()) _range_constraint->Error("range constraint in scalar type definition must be locally static") ;

    unsigned i ;
    VhdlPhysicalUnitDecl *elem ;
    // Accumulate physical units (as Ids)
    Map *unit_ids = new Map(STRING_HASH) ;
    type_id->DeclarePhysicalType(unit_ids) ; // unit_ids are absorbed

    // Now, fill the unit_ids array after we declared their type.
    // This way, we can type-infer the physical literals.
    verific_uint64 pos ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list,i,elem) {
        if (!elem) continue ;
        VhdlIdDef *id = elem->GetId() ;
        if (!id) continue ;
        pos = elem->Infer(type_id) ; // Test the literal that defines this unit ; (like sec = 1000 ms) ; both need to be of the same type. Return position number
        id->DeclarePhysicalUnitId(type_id, pos) ;
        (void) unit_ids->Insert(id->Name(),id) ;
    }
    FOREACH_ARRAY_ITEM_BACK(_physical_unit_decl_list,i,elem) {
        if (!elem) continue ;
        elem->SetInversePosition(type_id) ;
    }
}

void VhdlEnumerationTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id) return ;

    // Here, declare all enum elements with their type_id
    // and their position, and give element info to type.
    unsigned i ;
    VhdlIdDef *id ;
    // Accumulate enumeration Ids
    Map *enum_ids = new Map(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list,i,id) {
        if (!id) continue ;
        id->DeclareEnumerationId(type_id,i) ;

        if (!enum_ids->Insert(id->Name(),id)) {
            Error("enumeration value %s is already declared in type %s",id->Name(), type_id->Name()) ;
            continue ;
        }

        // We don't need to do homograph checks here, since no other enumeration
        // literal will match the return type (type_id) since it is just declared.
        // Later, a function without a parameter could become a homograph, but
        // that we check in the function declaration. Not here.  Sometimes, life is easy.
    }
    type_id->DeclareEnumerationType(enum_ids) ; // enum_ids are absorbed
}

void VhdlArrayTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id || !_subtype_indication) return ;

    // Scan index-elements to find (un)constraint/locally/globally-static-ness
    unsigned all_locally_static_range = 1 ;
    unsigned all_globally_static_range = 1 ;

    unsigned i ;
    VhdlDiscreteRange *elem ;
    unsigned is_constrained = 0 ;
    unsigned is_unconstrained = 0 ;
    VhdlIdDef *index_type ;
    // Accumulate array index types (as Ids)
    Map *index_types = new Map(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(_index_constraint,i,elem) {
        if (!elem) continue ;
        if (elem->IsUnconstrained(1)) {
            if (is_constrained) {
                elem->Error("illegal unconstrained element in constrained array declaration") ;
            }
            is_unconstrained = 1 ;
        } else {
            if (is_unconstrained) {
                elem->Error("illegal constrained element in unconstrained array declaration") ;
            }
            is_constrained = 1 ;
        }
        // Type-infer the index type
        index_type = elem->TypeInfer(0,0,VHDL_subtyperange,0) ;

        // LRM 3.2.1.1 if discrete range both sides are universal_integer, convert to INTEGER
        // Also do this if there is no result type. This will reduce errors further down
        if (!index_type || index_type->IsUniversalInteger()) {
            index_type = StdType("integer") ;
        }
        if (!index_type) {
            delete index_types ;
            return ; // Happens if 'standard' was not read properly
        }

        // Go to the base type (RD : don't know why I wanted that)
        // index_type = index_type->Type() ;

        if (!index_type->IsDiscreteType()) {
            elem->Error("in index constraint; %s is not a discrete type", index_type->Name()) ;
        }

        if (elem->IsLocallyStatic()) {
            index_type->SetLocallyStaticType() ; // also sets globally_static_type
        } else if (elem->IsGloballyStatic()) {
            all_locally_static_range = 0 ;
            index_type->SetGloballyStaticType() ;
        } else {
            all_locally_static_range = 0 ;
            all_globally_static_range = 0 ;
        }

        // Its a 'Map', and by name, so use force-insert
        // FIX ME : This is clunky
        (void) index_types->Insert(index_type->Name(), index_type, 0, 1) ;
    }
     // Find the element type
    VhdlIdDef *elem_type = _subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) ;
    if (!elem_type) {
        delete index_types ;
        return ;
    }

    // Issue 2921 : element cannot be a file type
    if (elem_type->IsFileType()) {
        Error("elements of file type are not allowed in composite types") ;
    }
        
    if (_subtype_indication->IsGloballyStatic()) {
        elem_type->SetGloballyStaticType() ;
    }

    int is_locally_static = (int) (_subtype_indication->IsLocallyStatic() && all_locally_static_range) ; // Viper #5734
    int is_globally_static = (int) (_subtype_indication->IsLocallyStatic() || _subtype_indication->IsGloballyStatic()) && all_globally_static_range ; // Viper #5734

    if (is_unconstrained) {
        type_id->DeclareUnconstrainedArrayType(index_types, elem_type, is_locally_static, is_globally_static) ; // index_types Map will get absorbed
    } else {
        type_id->DeclareConstrainedArrayType(index_types, elem_type, is_locally_static, is_globally_static) ;   // index_types Map will get absorbed
    }

    // Viper 7348: Check for recursive type in record and set it as IncompleteType if so
    if (elem_type == type_id) {
        Error("element type of the array declaration is same as the parent array type") ; // Viper #6520
        type_id->DeclareIncompleteType(0) ;
    }
}

void VhdlProtectedTypeDef::Declare(VhdlIdDef *type_id)
{
    if (type_id) type_id->DeclareProtectedType(this) ;
}

void VhdlProtectedTypeDefBody::Declare(VhdlIdDef *type_id)
{
    if (type_id && type_id->IsProtectedBody()) type_id->DeclareProtectedTypeBody(this) ;
}

void VhdlRecordTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id) return ;

    unsigned i, j ;
    VhdlElementDecl *elem ;
    VhdlIdDef *id ;
    // Accumulate record-elements (as Ids)
    Map *elem_ids = new Map(STRING_HASH) ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, elem) {
        if (!elem) continue ;
        // element decl could denote multiple id's. Accumulate them for the record type.
        Array *ids = elem->GetIds() ;
        if (!ids) continue ;
        VhdlSubtypeIndication *subtype_indication = elem->GetSubtypeIndication() ;
        VhdlIdDef *elem_type = subtype_indication ? subtype_indication->TypeInfer(0,0,VHDL_subtyperange,0) : 0 ;
        // Viper 7348: Check for recursive type in record and set it as IncompleteType if so
        if (elem_type == type_id) {
            elem->Error("element type of the record element is same as the parent record type") ; // Viper #6520
            type_id->DeclareIncompleteType(0) ;
            continue ;
        }
        FOREACH_ARRAY_ITEM(ids, j, id) {
            if (!id) continue ;
            // Now set 'position' of this element (we don't store the record type in the element)
            id->DeclareRecordElement(elem_ids->Size()/*present 'position'*/) ;
            (void) elem_ids->Insert(id->Name(),id) ;
        }
    }
    type_id->DeclareRecordType(elem_ids) ; // elem_ids will be absorbed
}

void VhdlFileTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id || !_file_type_mark) return ;

    VhdlIdDef *file_type = _file_type_mark->TypeInfer(0,0,VHDL_range,0) ;
    Set type_recustion_set ;
    if (file_type && file_type->IsTypeContainsFile(1, type_recustion_set)) { // Viper #3621
        Error("designated type of file type cannot be file or access type") ;
    }

    if (file_type && file_type->Dimension() > 1) { // Viper #3621
        Error("designated type of file type cannot be multidimensional array type") ;
    }

    type_id->DeclareFileType(file_type) ;
}

void VhdlAccessTypeDef::Declare(VhdlIdDef *type_id)
{
    if (!type_id || !_subtype_indication) return ;

    //VhdlIdDef *access_type = _subtype_indication->TypeInfer(0,0,VHDL_range,0) ;
    VhdlIdDef *access_type = _subtype_indication->TypeInfer(0,0,VHDL_ACCESS_SUBTYPE,0) ;
    // VIPER #3156 : The designated type must not be a file type, moreover it
    // may not have a subelement that is a file type.
    Set type_recustion_set ;
    if (access_type && access_type->IsTypeContainsFile(0, type_recustion_set)) {
        Error("designated type of access type cannot be file type") ;
    }

    type_recustion_set.Reset() ;

    int is_locally_static = (int) _subtype_indication->IsLocallyStatic() ; // Viper #7758
    int is_globally_static = (int) _subtype_indication->IsGloballyStatic() ; // Viper #7758
    if (access_type && access_type->IsTypeContainsUnconstrainedArray(type_recustion_set)) is_globally_static = 0 ;

    type_id->DeclareAccessType(access_type, is_locally_static, is_globally_static) ;
}

/******************************************************************/
//  Local scope access
/******************************************************************/

VhdlScope *VhdlRecordTypeDef::LocalScope() const    { return _local_scope ; }
VhdlScope *VhdlSubprogramDecl::LocalScope() const   { return (_subprogram_spec) ? _subprogram_spec->LocalScope() : 0 ; }
VhdlScope *VhdlSubprogramBody::LocalScope() const   { return (_subprogram_spec) ? _subprogram_spec->LocalScope() : 0 ; }
VhdlScope *VhdlComponentDecl::LocalScope() const    { return _local_scope ; }
VhdlScope *VhdlProtectedTypeDef::LocalScope() const { return _local_scope ; }
VhdlScope *VhdlProtectedTypeDefBody::LocalScope() const { return _local_scope ; }

/******************************************************************/
//  Label checking
/******************************************************************/

void VhdlSubprogramBody::ClosingLabel(VhdlDesignator *id)
{
    if (!id) return ;

    // Get the name of this subprogram
    VhdlIdDef *subprog_id = (_subprogram_spec) ? _subprogram_spec->GetId() : 0 ;

    if (subprog_id) {
        // IsStringLiteral check for Viper# 3573
        unsigned same_name = id->IsStringLiteral() ? Strings::compare_nocase(subprog_id->Name(), id->Name()) : Strings::compare(subprog_id->Name(), id->Name()) ;
        if (!same_name) id->Error("mismatch on label ; expected %s", subprog_id->Name()) ;
    }

    SetClosingLabel(id) ; // VIPER #6666 (id may get deleted here)
}

/******************************************************************/
//  Resolve Specifications
/******************************************************************/

void VhdlAttributeSpec::ResolveSpecs(VhdlScope *scope)
{
    if (Message::ErrorCount() || !_entity_spec || !_designator) return ;

    // Find the attribute identifier in the designator :
    VhdlIdDef *attr = _designator->FindSingleObject() ;

    // Set attribute id and its expression into entity specification :
    _entity_spec->ResolveSpecs(scope, attr, _value) ;
}

void VhdlConfigurationSpec::ResolveSpecs(VhdlScope * /*scope*/)
{
    if (Message::ErrorCount()) return ;

    // Find the component labels, and set their back-pointers to the primary binding :
    if (_component_spec) (void) _component_spec->ResolveSpecs(_binding, 0/*not incremental*/) ;
}

void VhdlDisconnectionSpec::ResolveSpecs(VhdlScope * /*scope*/)
{
    if (Message::ErrorCount()) return ;

    // FIX ME : Check what we should do here, if anything (now that entire scope is analyzed).
}

void
VhdlConfigurationSpec::CheckLabel(Map* all_other_map)
{
    // Viper 4738. Need to check whether :all or :other is the last
    // label. LRM Section: 5.2
    if (!_component_spec) return ;
    _component_spec->CheckLabel(all_other_map) ;
}

/******************************************************************/
//  Some utilities
/******************************************************************/

// Special case : Test literal against the physical unit type
verific_uint64 VhdlPhysicalUnitDecl::Infer(VhdlIdDef *type) const
{
    if (!_id) return 0 ;
    // Test if _physical_literal is also of this type. Type-infer for that.
    if (_physical_literal) (void) _physical_literal->TypeInfer(type,0,VHDL_READ,0) ;

    // Now calculate the position number

    // FIX ME :
    // Now, for TIME, the position overflows badly for ms and up.
    // It would still overflow (for 'min') if the resolution is
    // set to 'ns'.
    // NOTE : LRM 3.1.3.1 gives some hints...
    verific_uint64 pos ;

    // Change time resolution limit at will, by name
    // Set now at user specified time unit by vhdl_file::GetTimeUnit.
    // Otherwise set at the default : 'fs'
    if (type==StdType("time") && Strings::compare(_id->Name(),vhdl_file::GetTimeUnit())) {
        pos = 1 ; // TIME resolution limit
    } else if (_physical_literal) {
        pos = (verific_uint64)_physical_literal->EvaluateConstantInteger() ; // This can only be unsigned
    } else { // primary physical unit
        if (type==StdType("time")) {
            pos = 0 ; // below the resolution limit
        } else {
            pos = 1 ;
        }
    }

    return pos ;
}
void VhdlPhysicalUnitDecl::SetInversePosition(const VhdlIdDef *type) const
{
    if (!_id || (_id->Position() > 1) || !_physical_literal || (type != StdType("time"))) return ;

    if (_id->Position() == 1) _id->SetInversePosition((verific_uint64)1) ;

    _physical_literal->SetInversePosition(_id) ;
}

/**************************************************************/
// Calculate Signature
/**************************************************************/

#define HASH_VAL(SIG, VAL)    (((SIG) << 5) + (SIG) + (VAL))

unsigned long
VhdlDeclaration::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlTypeDef::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    return sig ;
}

unsigned long
VhdlScalarTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    if (_scalar_range) sig = HASH_VAL(sig, _scalar_range->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlArrayTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    VhdlDiscreteRange *constraint ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, constraint){
        if (constraint) sig = HASH_VAL(sig, constraint->CalculateSignature()) ;
    }
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlRecordTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    VhdlElementDecl *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, elem){
        if (elem) sig = HASH_VAL(sig, elem->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlProtectedTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    VhdlDeclaration *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decl_list, i, elem){
        if (elem) sig = HASH_VAL(sig, elem->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlProtectedTypeDefBody::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    VhdlDeclaration *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_decl_list, i, elem){
        if (elem) sig = HASH_VAL(sig, elem->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlAccessTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlFileTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    if (_file_type_mark) sig = HASH_VAL(sig, _file_type_mark->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlEnumerationTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    VhdlIdDef *id;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_enumeration_literal_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlPhysicalTypeDef::CalculateSignature() const
{
    unsigned long sig = VhdlTypeDef::CalculateSignature() ;
    if (_range_constraint) sig = HASH_VAL(sig, _range_constraint->CalculateSignature()) ;
    VhdlPhysicalUnitDecl *decl;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_physical_unit_decl_list, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlElementDecl::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id){
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlPhysicalUnitDecl::CalculateSignature() const
{
    unsigned long sig = HASH_VAL(0, GetClassId()) ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_physical_literal) sig = HASH_VAL(sig, _physical_literal->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlUseClause::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    VhdlSelectedName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_selected_name_list, i, name){
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlContextReference::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    VhdlSelectedName *name ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_selected_name_list, i, name){
        if (name) sig = HASH_VAL(sig, name->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlLibraryClause::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id){
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlInterfaceDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    sig = HASH_VAL(sig, _interface_kind) ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    // VIPER #7400: LRM 2.1.1 specifies that 'in' is the mode
    // used whether specified explicitly or not (implicitely):
    sig = HASH_VAL(sig, ((_mode)?_mode:VHDL_in)) ;
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    sig = HASH_VAL(sig, _signal_kind) ;
    if (_init_assign) sig = HASH_VAL(sig, _init_assign->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSubprogramDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_subprogram_spec) sig = HASH_VAL(sig, _subprogram_spec->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSubprogramBody::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_subprogram_spec) sig = HASH_VAL(sig, _subprogram_spec->CalculateSignature()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_decl_part, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    VhdlStatement *stmt ;
    FOREACH_ARRAY_ITEM(_statement_part, i, stmt) {
        if (stmt) sig = HASH_VAL(sig, stmt->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlSubtypeDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlFullTypeDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_type_def) sig = HASH_VAL(sig, _type_def->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlIncompleteTypeDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlConstantDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    if (_init_assign) sig = HASH_VAL(sig, _init_assign->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlSignalDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    sig = HASH_VAL(sig, _signal_kind) ;
    if (_init_assign) sig = HASH_VAL(sig, _init_assign->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlVariableDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    sig = HASH_VAL(sig, _shared) ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    if (_init_assign) sig = HASH_VAL(sig, _init_assign->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlFileDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    VhdlIdDef *id ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_id_list, i, id) {
        if (id) sig = HASH_VAL(sig, id->CalculateSignature()) ;
    }
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    if (_file_open_info) sig = HASH_VAL(sig, _file_open_info->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlAliasDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_designator) sig = HASH_VAL(sig, _designator->CalculateSignature()) ;
    if (_subtype_indication) sig = HASH_VAL(sig, _subtype_indication->CalculateSignature()) ;
    if (_alias_target_name) sig = HASH_VAL(sig, _alias_target_name->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlComponentDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    unsigned i ;
    VhdlDeclaration *decl ;
    FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    FOREACH_ARRAY_ITEM(_port_clause, i, decl) {
        if (decl) sig = HASH_VAL(sig, decl->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlAttributeDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_type_mark) sig = HASH_VAL(sig, _type_mark->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlAttributeSpec::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_designator) sig = HASH_VAL(sig, _designator->CalculateSignature()) ;
    if (_entity_spec) sig = HASH_VAL(sig, _entity_spec->CalculateSignature()) ;
    if (_value) sig = HASH_VAL(sig, _value->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlConfigurationSpec::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_component_spec) sig = HASH_VAL(sig, _component_spec->CalculateSignature()) ;
    if (_binding) sig = HASH_VAL(sig, _binding->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlDisconnectionSpec::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_guarded_signal_spec) sig = HASH_VAL(sig, _guarded_signal_spec->CalculateSignature()) ;
    if (_after) sig = HASH_VAL(sig, _after->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlGroupTemplateDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    VhdlEntityClassEntry *entry ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_entity_class_entry_list, i, entry) {
        if (entry) sig = HASH_VAL(sig, entry->CalculateSignature()) ;
    }
    return sig ;
}

unsigned long
VhdlGroupDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_id) sig = HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_group_template_name) sig = HASH_VAL(sig, _group_template_name->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlSubprogInstantiationDecl::CalculateSignature() const
{
    unsigned long sig = VhdlDeclaration::CalculateSignature() ;
    if (_subprogram_spec) sig = HASH_VAL(sig, _subprogram_spec->CalculateSignature()) ;
    if (_uninstantiated_subprog_name) sig = HASH_VAL(sig, _uninstantiated_subprog_name->CalculateSignature()) ;
    unsigned i ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(_generic_map_aspect, i, assoc) {
        if (assoc) sig = HASH_VAL(sig, assoc->CalculateSignature()) ;
    }
    return sig ;
}
unsigned long
VhdlInterfaceTypeDecl::CalculateSignature() const
{
    unsigned long sig = VhdlInterfaceDecl::CalculateSignature() ;
    if (_type_id) sig = HASH_VAL(sig, _type_id->CalculateSignature()) ;
    return sig ;
}

unsigned long
VhdlInterfaceSubprogDecl::CalculateSignature() const
{
    unsigned long sig = VhdlInterfaceDecl::CalculateSignature() ;
    if (_subprogram_spec) sig = HASH_VAL(sig, _subprogram_spec->CalculateSignature()) ;
    return sig ;
}
unsigned long
VhdlInterfacePackageDecl::CalculateSignature() const
{
    unsigned long sig = VhdlInterfaceDecl::CalculateSignature() ;
    if (_id) sig =  HASH_VAL(sig, _id->CalculateSignature()) ;
    if (_uninst_package_name) sig = HASH_VAL(sig, _uninst_package_name->CalculateSignature()) ;
    return sig ;
}

VhdlIdDef *VhdlTypeDef::GetId() const { return 0 ; }
VhdlIdDef *VhdlProtectedTypeDef::GetId() const { return _local_scope ? _local_scope->GetOwner() : 0 ; }
VhdlIdDef *VhdlProtectedTypeDefBody::GetId() const { return _local_scope ? _local_scope->GetOwner() : 0 ; }

void
VhdlProtectedTypeDefBody::ClosingLabel(VhdlDesignator *id)
{
    if (!id) return ;

    VhdlIdDef *body_id = _local_scope ? _local_scope->GetOwner() : 0 ;

    if (body_id && !Strings::compare(body_id->Name(), id->Name())) {
        id->Error("mismatch on label ; expected %s", body_id->Name()) ;
    }

    SetClosingLabel(id) ; // VIPER #6666 (id may get deleted here)
}

void
VhdlProtectedTypeDef::ClosingLabel(VhdlDesignator *id)
{
    if (!id) return ;

    VhdlIdDef *type_id = _local_scope ? _local_scope->GetOwner() : 0 ;

    if (type_id && !Strings::compare(type_id->Name(), id->Name())) {
        id->Error("mismatch on label ; expected %s", type_id->Name()) ;
    }

    SetClosingLabel(id) ; // VIPER #6666 (id may get deleted here)
}

VhdlDesignator *VhdlDeclaration::GetClosingLabel() const // VIPER #6666
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    return _closing_label ;
#else
    return 0 ;
#endif
}

void VhdlDeclaration::SetClosingLabel(VhdlDesignator *label) // VIPER #6666
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _closing_label ;
    _closing_label = label ;
#else
    delete label ;
#endif
}

VhdlDesignator *VhdlTypeDef::GetClosingLabel() const // VIPER #6666
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    return _closing_label ;
#else
    return 0 ;
#endif
}

void VhdlTypeDef::SetClosingLabel(VhdlDesignator *label) // VIPER #6666
{
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    delete _closing_label ;
    _closing_label = label ;
#else
    delete label ;
#endif
}

#ifdef VHDL_ID_SUBTYPE_BACKPOINTER
// VIPER #7343 : Get dimension from type declaration
VhdlDiscreteRange *VhdlFullTypeDecl::GetDimensionAt(unsigned dimension) const
{
    return _type_def ? _type_def->GetDimensionAt(dimension): 0 ;
}
VhdlDiscreteRange *VhdlArrayTypeDef::GetDimensionAt(unsigned dimension) const
{
    unsigned i ;
    VhdlDiscreteRange *index_range ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, index_range) { // Traverse the index constraints
        if (!index_range) continue ;
        if (dimension == 0) return index_range ; // It is required index
        dimension-- ; // Decrease dimension
    }
    return _subtype_indication ? _subtype_indication->GetDimensionAt(dimension): 0 ;
}
#endif

// VIPER #7505 : Get dimension from type declaration
unsigned VhdlFullTypeDecl::NumUnconstrainedRanges() const
{
    return _type_def ? _type_def->NumUnconstrainedRanges() : 0 ;
}
unsigned VhdlSubtypeDecl::NumUnconstrainedRanges() const
{
    return _subtype_indication ? _subtype_indication->NumUnconstrainedRanges() : 0 ;
}
unsigned VhdlDeclaration::NumUnconstrainedRanges() const
{
    return 0 ;
}
unsigned VhdlTypeDef::NumUnconstrainedRanges() const
{
    return 0 ;
}
unsigned VhdlArrayTypeDef::NumUnconstrainedRanges() const
{
    unsigned i ;
    VhdlDiscreteRange *index_range ;
    unsigned num_unconstrained = 0 ;
    FOREACH_ARRAY_ITEM(_index_constraint, i, index_range) { // Traverse the index constraints
        if (!index_range) continue ;
        num_unconstrained += index_range->NumUnconstrainedRanges() ;
    }
    num_unconstrained += _subtype_indication ? _subtype_indication->NumUnconstrainedRanges() : 0 ;

    return num_unconstrained ;
}
unsigned VhdlRecordTypeDef::NumUnconstrainedRanges() const
{
    unsigned num_unconstrained = 0 ;
    VhdlElementDecl *elem ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, elem) {
        if (elem) num_unconstrained += elem->NumUnconstrainedRanges() ;
    }
    return num_unconstrained ;
}
unsigned VhdlElementDecl::NumUnconstrainedRanges() const
{
    return _subtype_indication ? _subtype_indication->NumUnconstrainedRanges() : 0 ;
}

// VIPER #7809 : Check element constraint
unsigned VhdlFullTypeDecl::HasUnconstrainedElement() const
{
    return _type_def ? _type_def->HasUnconstrainedElement() : 0 ;
}
unsigned VhdlDeclaration::HasUnconstrainedElement() const { return 0 ; }
unsigned VhdlSubtypeDecl::HasUnconstrainedElement() const
{
    return _subtype_indication ? _subtype_indication->HasUnconstrainedElement() : 0 ;
}
unsigned VhdlTypeDef::HasUnconstrainedElement() const { return 0 ; }
unsigned VhdlArrayTypeDef::HasUnconstrainedElement() const
{
    // Check whether immediate level of subtype indication is unconstrained
    return _subtype_indication ? _subtype_indication->IsUnconstrained(0/* only current level*/) : 0 ;
}
unsigned VhdlRecordTypeDef::HasUnconstrainedElement() const
{
    VhdlElementDecl *elem ;
    unsigned i ;
    VhdlSubtypeIndication *subtype_indication ;
    FOREACH_ARRAY_ITEM(_element_decl_list, i, elem) {
        subtype_indication = elem ? elem->GetSubtypeIndication(): 0 ;
        // Check whether immediate level of subtype indication is unconstrained
        if (subtype_indication && subtype_indication->IsUnconstrained(0/* only current level*/)) return 1 ;
    }
    return 0 ;
}

