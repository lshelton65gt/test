/*
 *
 * [ File Version : 1.210 - 2014/02/26 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_UNITS_H_
#define _VERIFIC_VHDL_UNITS_H_

#include "VhdlDeclaration.h"
#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Netlist ;
class Instance ;

class VhdlLibrary ;
class VhdlDesignUnit ;
class VhdlPrimaryUnit ;
class VhdlSecondaryUnit ;

class VhdlScope ;
class VhdlIdDef ;
class VhdlIdRef ;
class VhdlBlockConfiguration ;
class VhdlDesignator ;
class VhdlExpression ;
class VhdlMapForCopy ;
class VeriModuleItem ;
class VeriModuleInstantiation ;
class VeriModule ;
class VeriPseudoTreeNode ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
// VIPER #6895: Convert vhdl package to verilog package
class VeriPackage ;
#endif

// To iterate over all primary unit parse-tree's that were analyzed in a (VhdlLibrary) library :
// use FOREACH_VHDL_PRIMARY_UNIT :
//       argument - : type ----------     function ------------------------------
//        LIB       : VhdlLibrary *       The VHDL library to iterate through
//        MI        : MapIter             An iterator
//        UNITTREE  : VhdlPrimaryUnit *   Returned Pointer to the unit parse tree

// Primary units are VhdlEntityDec, VhdlConfigurationDecl, or VhdlPackageDecl
#define FOREACH_VHDL_PRIMARY_UNIT(LIB,MI,UNITTREE)  if (LIB) FOREACH_MAP_ITEM((LIB)->AllPrimaryUnits(), MI, 0, &(UNITTREE))

/* -------------------------------------------------------------- */

/*
   VhdlLibrary is the design library class which acts as a primary unit container.  Primary units
   can be retrieved by name.  (i.e. the design library "std" contains a primary unit (package
   declaration) called "standard")
*/
class VFC_DLL_PORT VhdlLibrary
{
public:  // Public to get rid of compile warnings. Should never use these directly
    explicit VhdlLibrary(const char *name);
    virtual ~VhdlLibrary();

private:
    // Prevent compiler from defining the following
    VhdlLibrary() ;                               // Purposely leave unimplemented
    VhdlLibrary(const VhdlLibrary &) ;            // Purposely leave unimplemented
    VhdlLibrary& operator=(const VhdlLibrary &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // Here : interface routines to a single library
    char *              Name() const             { return _name ; }   // Get name of library
    Map *               AllPrimaryUnits() const  { return const_cast<Map*>(&_primary_units) ; } // Get map of all primary units contained within this library

    // VIPER #7361: Added one argument to consider mixed language case separately
    // If 'consider_mixed_case' is 1, unescaped name will be downcased and escaped
    // name will be unescaped for searching if not found without any modification
    VhdlPrimaryUnit *   GetPrimUnit(const char *name, unsigned restore = 1, unsigned consider_mixed_case = 1) const ;         // Find primary unit by name in this library.  Attempt to restore from vdb if unit is not already in memory.
    VhdlPrimaryUnit *   GetPrimUnitAnywhere(const char *name, unsigned restore = 1) const ; // Find primary unit in any analyzed library.  Attempt to restore from vdb if unit is not already in memory.
    unsigned            AddPrimUnit(VhdlDesignUnit *unit, unsigned refuse_on_error = 1) ; // Add primary unit to this library, but refuse unit if there was an error and 'refuse_on_error' is set.
    unsigned            RemovePrimUnit(const VhdlDesignUnit *unit) ;  // Remove primary unit by name from this library

    unsigned            NumOfPrimUnits() const ;                      // Number of the primary units contained within this library
    VhdlPrimaryUnit *   PrimUnitByIndex(unsigned idx) const ;         // Get primary unit by insertion index

    // Static Elaborator

    // Static Elaborator binds all component instantiations with proper unique entity, so
    // after elaboration the binding information inside configuration declaration becomes
    // useless. This routine delete the binding information from all elaborated configuration declarations.
    void                DeleteElaboratedBindings() const ;

private:
    char *_name ;           // Library name
    Map   _primary_units ;  // Name->primary_unit (const char* -> VhdlPrimaryUnit*) hash table
} ; // class VhdlLibrary

/* -------------------------------------------------------------- */

/************************ Design Units *****************************/

/*
   VhdlDesignUnit is an abstract base class for primary and secondary design units.
   These include configuration, entity, and package declarations, and architecuture
   and package bodies.
*/
class VFC_DLL_PORT VhdlDesignUnit  : public VhdlDeclaration
{
protected: // Abstract class
    VhdlDesignUnit(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope);

    // Copy tree node
    VhdlDesignUnit(const VhdlDesignUnit &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDesignUnit(SaveRestore &save_restore, unsigned do_not_restore_members = 0);

public:
    virtual ~VhdlDesignUnit();

private:
    // Prevent compiler from defining the following
    VhdlDesignUnit() ;                                  // Purposely leave unimplemented
    VhdlDesignUnit(const VhdlDesignUnit &) ;            // Purposely leave unimplemented
    VhdlDesignUnit& operator=(const VhdlDesignUnit &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID     GetClassId() const = 0 ; // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void              Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void              Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void              PrettyPrint(ostream &f, unsigned level) const = 0 ; // For backward compatibility : On Primary units, print all secondary units too.
    virtual void              PrettyPrintDesignUnit(ostream &f, unsigned level) const = 0 ; // Print only this design unit.

    const char *              Name() const ;                           // Get the design unit's name

    virtual unsigned          IsPrimaryUnit() const     { return 0 ; } // Is this design unit a primary unit?
    virtual unsigned          IsSecondaryUnit() const   { return 0 ; } // Is this design unit a secondary unit?
    virtual unsigned          IsPslUnit() const         { return 0 ; } // Is this design unit a PSL vunit?
    virtual Array *           GetGenericClause() const  { return 0 ; } // Get the design unit's generics array (if it exists)
    virtual Array *           GetPortClause() const     { return 0 ; } // Get the design unit's port array (if it exists)
    virtual Array *           GetDeclPart() const       { return 0 ; } // Get the design unit's declarations array (if it exists)
    virtual Array *           GetStatementPart() const  { return 0 ; } // Get the design unit's statement array (if it exists)

    virtual void              ClosingLabel(VhdlDesignator *id) ;       // Check closing label against my name

    unsigned long             TimeStamp() const         { return _timestamp ; } // The time that this design unit was created (set with ::time(0) in regular constructor).

    // VIPER #6937 : Get the mode (dialect) under which this module was analyzed
    unsigned                  GetAnalysisDialect() const { return _analysis_mode ; }
    void                      SetAnalysisDialect(unsigned d) { _analysis_mode = d ; }
    // Calculate the conformance signature of this design unit.
    virtual unsigned long     CalculateSignature() const ;
    virtual unsigned long     CalculateSaveRestoreSignature(unsigned use_num_compare /* = 1 for new method */) const ; // Used only for binary save/restore backward-compatibility

    // Flags used internally (for elaboration purposes)
    unsigned                  IsElaborated() const       { return _is_elaborated; }         // Has the design unit been visited by RTL elaboration ?
    void                      SetElaborated()            { _is_elaborated = 1 ; }           // Tag this design unit as having been visited by RTL elaboration
    void                      ResetElaborated()          { _is_elaborated = 0 ; }           // Tag this design unit as having been visited by RTL elaboration
    unsigned                  IsStaticElaborated() const { return _is_static_elaborated ; } // Has the design unit been visited by static elaboration ?
    void                      SetStaticElaborated()      { _is_static_elaborated = 1 ; }    // Tag this design unit as having been visited by static elaboration
    void                      ResetStaticElaborated()    { _is_static_elaborated = 0 ; }           // Tag this design unit as having been visited by RTL elaboration
    unsigned                  IsCopied() const           { return (_orig_design_unit) ? 1: 0 ; }      // Flag to test if this unit was copied from another unit

    VhdlDesignUnit *          GetOriginalUnit() const ; // Get the design unit from which this unit is copied
    const char *              GetOriginalUnitName() const{ return _orig_design_unit ; } // Get the design unit name from which this unit is copied

    // Accessor methods
    VhdlScope *               LocalScope() const        { return _local_scope ; }    // Get the local scope (if it exists) of this design unit
    VhdlIdDef *               Id() const                { return _id ; }             // Get the identifier id of this design unit
    Array *                   GetContextClause() const  { return _context_clause ; } // Get array of context clauses (library and use clause declarations)

    // Primary unit owner setting
    virtual VhdlLibrary *     GetOwningLib() const = 0 ;              // Get the library that owns this design unit (only for primary units)
    virtual void              SetOwningLib(VhdlLibrary *owner) ;      // Set the owning library for this design unit (only for primary units)

    // Secondary unit owner setting
    virtual VhdlPrimaryUnit * GetOwningUnit() const = 0 ;             // Get the primary unit that owns this design unit (only for sec. units)
    virtual void              SetOwningUnit(VhdlPrimaryUnit *owner) ; // Set the owning unit for this design unit (only for secondary units)

    virtual unsigned          HasOwner() const ;                      // Does this design unit have an owner?
    virtual void              CreateOrderedList(Set *list, Set *done) const ; // Create an ordered list of VhdlDesignUnits that depend on each other : 'list' will be filled in bottom->up order with VhdlDesignUnit*. Can be used to PrettyPrint out the units in correct (define before use) order.

    // Copy tree node
    virtual VhdlPrimaryUnit * CopyPrimaryUnit(const char *, VhdlMapForCopy &, unsigned ) const { return 0 ; } // If 'exclude_archs' is 1, secondary units are not copied. Otherwise secondary units are copied along with the primary.
    virtual VhdlSecondaryUnit * CopySecondaryUnit(const char *, VhdlMapForCopy &) const { return 0 ; } // Copy unit with 'new_unit_name'. If 'new_unit_name' is null, copied unit will be created with the old name. Copy routine does not attach the copied unit to any primary unit.

    // Parse tree find routine
    VhdlIdDef *               FindDeclared(const char *name) const ; // find the 'name' specific identifier declared in this scope.
    void                      AddContextClause(VhdlDeclaration *clause) ; // VIPER #2969 : Routine to add context clause
    void                      SetLocalScope(VhdlScope *s) ;

// Set by the conversion routine to indicate that this unit has been created for a corresponding verilog module. Set to '0' by constructor.
    unsigned                  IsVerilogModule() const          { return _is_verilog_module ; }
    void                      SetVerilogModule(unsigned value) { _is_verilog_module = value ; }
    void                      SetOriginalUnitName(const char* veri_name) ;
    unsigned                  IsAffectedByPathName() const     { return _affected_by_path_name ; }
    void                      SetAffectedByPathName()          { _affected_by_path_name = 1 ; }

    // VIPER #5918 : Implemented for package declaration and package instantiation
    // declaration as those can be used in declarative region:
    virtual void              Elaborate(VhdlDataFlow * /*df*/) {}

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Convert verilog package to vhdl one:
    unsigned                  IsAlreadyProcessed() const          { return _is_already_processed ; } // Indicates that we have already processed the package: we may come to initialize the package if the package is imported to more than one designs
    void                      SetAlreadyProcessed(unsigned value) { _is_already_processed = value ; } // Set the flag when we are initializing the package for the first time
#endif
    void                      ProcessedUsedPackage(VhdlMapForCopy &old2new) const ; // VIPER #7508

protected:
// Parse tree nodes (owned) :
    VhdlIdDef      *_id ;                       // Identifier id of this design unit
    VhdlScope      *_local_scope ;              // Scope of this design unit
    Array          *_context_clause ;           // Array of VhdlDeclaration*. Can contain VhdlLibraryClause* or VhdlUseClause*

// Extra fields :
    unsigned long   _timestamp ;                // The time that this design unit was created.
    unsigned        _is_elaborated:1 ;          // Flag to test if 'default' (no generics override) is already executed in RTL elaboration
    unsigned        _is_static_elaborated:1 ;   // Flag to test if this unit is already statically elaborated.
// Set by the conversion routine to indicate that this unit is for Verilog module. By default set to '0' value.
    unsigned        _is_verilog_module:1 ;
    unsigned        _affected_by_path_name:1 ;  // Flag to indicate whether this unit is to be make unique for the presence of path_name/instance_name attribute
    unsigned        _analysis_mode:4 ;          // Dialect (87, 93, 2000, 2008) (VIPER #6937)
    char           *_orig_design_unit ;         // Name of design unit from which this unit is copied
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    unsigned        _is_already_processed: 1 ; // VIPER #6896: flag on package that is already elaborated
#endif

// Set during analysis (elements not owned)
// Design unit dependency is now handled via scope dependency lists (VhdlScope::_using).
// Don't do anything here any more.
//    Set       *_using ;           // Set of VhdlDesignUnit* that are using this design unit
//    Set       *_used_by ;         // Set of VhdlDesignUnit* that are used by this design unit
} ; // class VhdlDesignUnit

/* -------------------------------------------------------------- */

/************************ Primary Units *****************************/

// To iterate over all secondary unit parse-tree's that were analyzed in a (VhdlPrimaryUnit) primary unit :
// use FOREACH_VHDL_SECONDARY_UNIT :
//       argument - : type --------------   function -------------------------------
//        PRIM      : VhdlPrimaryUnit *     The VHDL Primary Unit to iterate through
//        MI        : MapIter               An iterator
//        UNITTREE  : VhdlSecondaryUnit *   Returned Pointer to the unit parse tree

// Secondary units are VhdlArchitectureBody or VhdlPackageBody
#define FOREACH_VHDL_SECONDARY_UNIT(PRIM,MI,UNITTREE)   if (PRIM) FOREACH_MAP_ITEM((PRIM)->AllSecondaryUnits(), MI, 0, &(UNITTREE))

/*
   VhdlPrimaryUnit is an abstract base class for primary units.
   These include configuration, entity, and package declarations.
*/
class VFC_DLL_PORT VhdlPrimaryUnit : public VhdlDesignUnit
{
protected: // Abstract class
    VhdlPrimaryUnit(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope);

    // Copy tree node
    VhdlPrimaryUnit(const VhdlPrimaryUnit &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPrimaryUnit(SaveRestore &save_restore, unsigned do_not_restore_members = 0);

public:
    virtual ~VhdlPrimaryUnit();

private:
    // Prevent compiler from defining the following
    VhdlPrimaryUnit() ;                                   // Purposely leave unimplemented
    VhdlPrimaryUnit(const VhdlPrimaryUnit &) ;            // Purposely leave unimplemented
    VhdlPrimaryUnit& operator=(const VhdlPrimaryUnit &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ; // For backward compatibility : On Primary units, print all secondary units too.
    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const = 0 ; // Print only this design unit.

    // Variable usage analysis
    virtual void                VarUsageStartingPoint() const { }

    // Copy primary units.  At top-level, call with empty VhdlMapForCopy. Routine will populate
    // the 'VhdlMapForCopy' with old_node vs new_node. Only a few specific nodes are inserted
    // in 'VhdlMapForCopy'.
    virtual VhdlPrimaryUnit *   CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const ; // If 'exclude_archs' is 1, secondary units are not copied. Otherwise secondary units are copied along with the primary.
    virtual VhdlPrimaryUnit *   CopyEntityArchitecture(const char * /*new_entity_name*/, const char * /*arch_to_copy*/, VhdlMapForCopy & /*old2new*/) const { return 0 ; }  // Do not copy all primary units. Only valid for entity copy.

    // Parse tree manipulation routines:
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration * /*old_node*/, VhdlDeclaration * /*new_node*/)                      { return 0 ; }  // Delete 'old_node' and puts the new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement * /*old_node*/, VhdlStatement * /*new_node*/)                          { return 0 ; }  // Delete 'old_node' and puts the new statement in its place
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef * /*old_node*/, VhdlIdRef * /*new_node*/)                                 { return 0 ; }  // Delete 'old_node' and puts new identifier reference in its position.
    virtual unsigned            ReplaceChildBlockConfig(VhdlBlockConfiguration * /*old_node*/, VhdlBlockConfiguration * /*new_node*/) { return 0 ; }  // Delete 'old_node' and puts new block configuration in its position.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)             { return 0 ; }  // Insert the new declaration before the 'target_node' specific declaration in the declaration list.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)              { return 0 ; }  // Insert the new declaration after the 'target_node' specific declaration in the declaration list.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)                 { return 0 ; }  // Insert the new statement 'new_node' before the target statement 'target_node' in statement list.
    virtual unsigned            InsertAfterStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)                  { return 0 ; }  // Insert the new statement 'new_node' after the target statement 'target_node' in statement list.

    // Parse-tree port creation/deletion routines
    virtual VhdlIdDef *         AddPort(const char * /*port_name*/, unsigned /*port_direction*/, VhdlSubtypeIndication * /*subtype_indication*/) { return 0 ; } // Add new port 'port_name' to primary unit
    virtual unsigned            RemovePort(const char * /*port_name*/) { return 0; } // Remove 'port_name' from port clause

    // Parse-tree generic creation/deletion routines
    virtual void                SetGenericClause(Array * /*generic_clause*/) { } // Set generic clause
    virtual VhdlIdDef *         AddGeneric(const char * /*generic_name*/, VhdlSubtypeIndication * /*subtype_indication*/, VhdlExpression * /*initial_assign*/) { return 0 ; } // Add generic 'generic_name' to primary unit
    virtual unsigned            RemoveGeneric(const char * /*generic_name*/) { return 0 ; } // Remove argument specific generic

    // Parse-tree declaration addition routine
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part
    virtual void                AddStatement(VhdlStatement *new_stmt) ; // Add argument specific statement to the statement part (for entity)

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl package to verilog package
    virtual VeriPackage *       ConvertPackage()   { return 0 ; }
#endif

    // Static elaboration
    // Given a primary unit, elaborate/set its generics, unroll loops, ungroup hierarchy etc.
    virtual VhdlPrimaryUnit *   StaticElaborate(const char * /*secondary_unit_name*/, Array * /*actual_generics*/, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)=0 ;

    // Internal routine :
    virtual VhdlPrimaryUnit *   StaticElaborateConfig(const char * /*secondary_unit_name*/, Array * /*actual_generics*/, Array * /*actual_ports*/, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef * /*actual_component*/, unsigned /*is_top*/) { return 0 ; }

    // Routines used by StaticElaborate() to replace array-of-identifier declarations to single identifier declaration.
    virtual void                SplitAndAssociatePorts(Array * /*actual_ports*/, VhdlIdDef * /*component*/)       { } // port binding. Use component for default binding if needed. Functionality is defined for derived classes only.
    virtual void                SplitAndAssociateGenerics(Array * /*actual_generics*/, VhdlIdDef * /*component*/) { } // generic binding. Use component for default binding if needed. Functionality is defined for derived classes only.

    // Create unique entity inspecting its generics and unconstrained ports. This unique entity is used
    // to replace component instantiation with direct entity instantiation.
    virtual VhdlPrimaryUnit *   CreateUniqueUnit(Array ** /*actual_generics*/, Array ** /*actual_ports*/, VhdlBlockConfiguration * /*block_config*/, VhdlIdDef * /*actual_component*/, const char * /*arch_name*/, VhdlMapForCopy & /*old2new*/, const char * /*config_name*/) { return 0 ; } // Implemented in derived class VhdlEntityDecl.

    // Create a unique name of this entity and the change the name of this unit to new name
    void                        AssignUniqueName(const VhdlLibrary *lib) ;

    // After elaboration the binding information in configuration declaration becomes useless, so those are deleted.
    virtual void                DeleteBindings() {}

    // Consider default values of input ports to create new association element for the terminal
    // list of entity instantiation.
    virtual void                DefaultValues(Set * /*id_considered*/, Array * /*port_map*/, const VhdlIdDef * /*instantiated_unit*/, linefile_type /*linefile*/) { }
    // Internal routine to static elaborate a primary unit without copying it
    virtual VhdlPrimaryUnit *   StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component)=0 ;
    // Routine to create equivalent component from entity
    virtual VhdlIdDef *         CreateComponent(VhdlScope * /*upper_scope*/, VhdlIdDef * /*actual_component*/, Array * /*actual_generics*/, Array * /*actual_ports*/) { return 0 ; }
    // Routine to copy interface association list
    Array *                     CopyInterfaceAssociationList(const Array *assoc_list) const ;
    virtual VhdlMapForCopy *    GetOld2NewMap() const { return 0 ; } // VIPER #6939

    virtual unsigned            IsPrimaryUnit() const              { return 1 ; } // Redefining VhdlDesignUnit's implementation

    virtual unsigned            HasUninitializedGenerics() const   { return 0 ; } // Check if this unit has generics which have no initial value (invalid as top-level unit).
    virtual unsigned            HasUnconstrainedPorts() const      { return 0 ; } // Check if this unit has ports which are unconstrained (invalid as top-level unit).
    virtual unsigned            HasUnconstrainedUninitializedPorts() const { return 0 ; }   // Check if this unit has ports which are unconstrained and not initialized (invalid as top-level unit).
    virtual unsigned            IsPackageDecl() const              { return 0 ; }
    virtual unsigned            IsPackageInstDecl() const          { return 0 ; }
    virtual void                SetActualTypes() ; // defined only for package inst

    // Secondary unit accessor/modifier methods
    VhdlSecondaryUnit *         GetSecondaryUnit(const char *id) const ;        // Get the secondary unit of this primary unit by name
    VhdlSecondaryUnit *         SecondaryUnitByIndex(unsigned idx) const ;      // Get the secondary unit of this primary unit by insertion index
    unsigned                    AddSecondaryUnit(VhdlDesignUnit *unit) ;        // Add a secondary unit to this primary unit.  A value of 0 returned means unit was deleted.
    unsigned                    RemoveSecondaryUnit(const VhdlDesignUnit *unit) ; // Remove the specified secondary unit from this primary unit
    unsigned                    NumOfSecondaryUnits() const ;                   // Get number of secondary units this primary unit contains
    Map *                       AllSecondaryUnits() const        { return const_cast<Map*>(&_secondary_units) ; }    // Get map of all sec. units contained by this primary unit

    // Owner accessor/modifier methods
    virtual VhdlLibrary *       GetOwningLib() const       { return _owner; }   // Get library that owns this primary unit
    virtual void                SetOwningLib(VhdlLibrary *owner) ;              // Set the library that owns this primary unit
    virtual VhdlPrimaryUnit *   GetOwningUnit() const      { return 0; }        // Return NULL.  This only applies to sec. units.  Must implement since this is a pure virtual in VhdlDesignUnit.
    virtual unsigned            HasOwner() const ;                              // Does this primary unit have an owner.  (Is _owner valid?)
    virtual VhdlLibrary *       Owner() const              { return _owner ; }  // Return _owner.
    virtual Array *             GetGenericMapAspect() const { return 0 ; }
    virtual VhdlScope *         GetInstanceScope() const    { return 0 ; }
    virtual void                SetUninstantiatedPackageId(VhdlIdDef *uninstantiated_package_id) ;
    virtual VhdlIdDef *         GetUninstantiatedPackageId() const ;
    virtual void                SetActualId(VhdlIdDef *id) ; // Only for package instantiation decl
    virtual VhdlIdDef *         GetActualId() const ; // Only for package instantiation decl

    // Elaboration
    // Control compilation from users : option to elaborate as a black-box unit (netlist without contents)
    void                        SetCompileAsBlackbox()         { _compile_as_blackbox = 1 ; }
    void                        SetCompileNormal()             { _compile_as_blackbox = 0 ; } // Default
    unsigned                    GetCompileAsBlackbox() const   { return _compile_as_blackbox ; }
protected :
    unsigned _compile_as_blackbox ; // Flag set by user SetCompileAsBlackbox(), will cause elaboration to create a black-box netlist (only interfaces) for this unit.

public :

    // VIPER #3404/3405 : Added line_file argument to indicate correct line for generic formal
    // This is relevant for mixed language designs to show proper line for error/warnings
    Array *                     CreateActualGenerics(const Map *user_generics, linefile_type lf = 0, unsigned from_verilog = 0, unsigned ignore_ifnotexist =0) ; // Create array of generics from a user name->value Map of generic_name->generic_value strings
    VhdlExpression *            CreateGeneric(const char *string, const VhdlIdDef *formal = 0, VhdlIdDef *return_type = 0) const ; // Create a generic (treenode) value from a string. Use line/file info from the 'formal' identifier. 'formal' also used to do implicit type-conversions.
    virtual void                AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component)=0 ; // port binding. Use component for default binding if needed.
    virtual void                AssociateGenerics(Array *actual_generics, VhdlIdDef *component)=0 ; // generic binding. Use component for default binding if needed.
    virtual VhdlIdDef *         GetElaboratedPackageId() const { return 0 ; }
    virtual void                SetElaboratedPackageId(VhdlIdDef * /*id*/) {}
    virtual VhdlIdDef *         TakeElaboratedPackageId() { return 0 ; }

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                GetParameterFromGeneric(Map& /*parameters*/, Array * /*generics*/, VeriModule * /*mod*/, VhdlIdDef * /*component*/)  { return ; }
#endif

    virtual void                ProcessBindInstance(VeriModuleInstantiation *module_instance) ;
    virtual Array *             GetBindInstances() ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                CheckPortBinding(const VeriModule *verilog_module, VhdlIdDef* component) ;
#endif
    // VIPER #6633: Contains the number of process statements replaced during explict state machine conversion:
    unsigned                    GetNumOfReplacedStmt() const                        { return _process_stmt_replaced ; }
    void                        SetProcessStmtReplaced(unsigned num_of_stmt)        { _process_stmt_replaced = num_of_stmt ; }

protected:
// Parse-tree nodes (owned) :
    Map          _secondary_units ; // Id name->secondary_unit (const char* -> VhdlSecondaryUnit*) hash table
// Set during analysis (not owned) :
    VhdlLibrary *_owner ;           // The library this unit is stored in

    // VIPER #6633: Contains the number of replaced process stmt during explict state machine conversion:
    unsigned        _process_stmt_replaced:10 ;

} ; // class VhdlPrimaryUnit

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlEntityDecl : public VhdlPrimaryUnit
{
public:
    VhdlEntityDecl(Array *context_clause, VhdlIdDef *id, Array *generic_clause, Array *port_clause, Array *decl_part, Array *statement_part, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlEntityDecl(const char *new_name, const VhdlEntityDecl &node, VhdlMapForCopy &old2new, unsigned exclude_archs = 0) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEntityDecl(SaveRestore &save_restore);

    virtual ~VhdlEntityDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlEntityDecl() ;                                  // Purposely leave unimplemented
    VhdlEntityDecl(const VhdlEntityDecl &) ;            // Purposely leave unimplemented
    VhdlEntityDecl& operator=(const VhdlEntityDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLENTITYDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    // virtual void                PrettyPrint(ostream &f, unsigned level) const ; // Caught by VhdlPrimaryUnit::PrettyPrint() now for backward compatibility. It prints all secondary units too.
    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const ; // Print only this primary unit. Not secondary units.

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Variable usage analysis
    virtual void                VarUsageStartingPoint() const ;

    // Copy entity declaration.
    virtual VhdlPrimaryUnit *   CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const ; // Copy entity and its architectures. If 'exclude_archs' is 1, only entity is copied.This copy routine do not add the copied unit in any library.
    virtual VhdlPrimaryUnit *   CopyEntityArchitecture(const char *new_entity_name, const char *arch_to_copy, VhdlMapForCopy &old2new) const; // Copy 'this' entity and 'arch_to_copy' architecture. The name of the copied entity will be 'new_entity_name'

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Delete 'old_node' and puts the new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ;     // Delete 'old_node' and puts the new statement in its place

    // Parse-tree port creation/deletion routines
    virtual VhdlIdDef *         AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication) ; // Add new port 'port_name' to primary unit
    virtual unsigned            RemovePort(const char *port_name) ; // Remove 'port_name' from port clause

    // Parse-tree generic creation/deletion routines
    virtual void                SetGenericClause(Array *generic_clause) ; // Set generic clause
    virtual VhdlIdDef *         AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign) ; // Add generic 'generic_name' to primary unit
    virtual unsigned            RemoveGeneric(const char *generic_name) ; // Remove argument specific generic

    // Parse-tree declaration addition routine
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part
    virtual void                AddStatement(VhdlStatement *new_stmt) ; // Add argument specific statement to the statement part

    // These routines insert a specific node in any place in an array-of-that-node. i.e. user can create
    // a declaration node and add that before/after any specific declaration in the declaration list.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert the new declaration before the 'target_node' specific declaration in the declaration list.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;  // Insert the new declaration after the 'target_node' specific declaration in the declaration list.

    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ;     // Insert the new statement 'new_node' before the target statement 'target_node' in statement list.
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ;      // Insert the new statement 'new_node' after the target statement 'target_node' in statement list.

    // Static elaboration.

    // Set proper values to the generics, set proper range constraint to unconstrained ports, unroll
    // generate, replace component instantiation with direct entity instantiation etc.
    virtual VhdlPrimaryUnit *   StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;

    // Routines used by StaticElaborate() to replace array-of-identifier declarations to single
    // identifier declarations.
    virtual void                SplitAndAssociatePorts(Array *actual_ports, VhdlIdDef *component) ;       // Port binding. Use component for default binding if needed. Manipulate port declaration to set proper range constraint to unconstraint ports.
    virtual void                SplitAndAssociateGenerics(Array *actual_generics, VhdlIdDef *component) ; // Generic binding. Use component for default binding if needed. Set proper default/overridden value to the generic declaration.

    // Create unique entity declaration examining its generics and port constraints. This entity is
    // used to replace component instantiation with proper entity instantiation.
    virtual VhdlPrimaryUnit *   CreateUniqueUnit(Array **actual_generics, Array **actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component, const char *arch_name, VhdlMapForCopy &old2new, const char *config_name) ;
    // Consider default values of input ports to create new association element for the terminal list
    // of entity instantiation.
    virtual void                DefaultValues(Set *id_considered, Array *port_map, const VhdlIdDef *instantiated_unit, linefile_type linefile) ;
    // Internal routine to static elaborate a primary unit without copying it
    virtual VhdlPrimaryUnit *   StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;
    // Routine to create equivalent component from entity
    virtual VhdlIdDef *         CreateComponent(VhdlScope *upper_scope, VhdlIdDef *actual_component, Array *actual_generics, Array *actual_ports) ;
    Array *                     CreateInterfaceDecls(const VhdlIdDef *actual_component, const Array *actual_interfaces, VhdlMapForCopy &old2new, unsigned is_generic) const ;

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                GetParameterFromGeneric(Map& parameters, Array * /*generics*/, VeriModule * /*mod*/, VhdlIdDef * /*component*/) ;
#endif

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    void                        SetGenericInitial(VeriModule *module) ;
#endif

    // Accessor methods
    virtual Array *             GetGenericClause() const    { return _generic_clause ; }
    virtual Array *             GetPortClause() const       { return _port_clause ; }
    virtual Array *             GetDeclPart() const         { return _decl_part ; }
    virtual Array *             GetStatementPart() const    { return _statement_part ; }

    virtual void                AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component) ; // Port binding. Use component for default binding if needed.) ;
    virtual void                AssociateGenerics(Array *actual_generics, VhdlIdDef *component) ;            // Generic binding. Use component for default binding if needed.

    virtual unsigned            HasUninitializedGenerics() const ; // Check if this unit has generics which have no initial value (invalid as top-level unit).
    virtual unsigned            HasUnconstrainedPorts() const ;    // Check if this unit has ports which are unconstrained (invalid as top-level unit).
    virtual unsigned            HasUnconstrainedUninitializedPorts() const ;    // Check if this unit has ports which are unconstrained and not initialized (invalid as top-level unit).

protected:
// Parse tree nodes (owned) :
    Array   *_generic_clause ; // Array of VhdlInterfaceDecl*
    Array   *_port_clause ;    // Array of VhdlInterfaceDecl*
    Array   *_decl_part ;      // Array of VhdlDeclaration*
    Array   *_statement_part ; // Array of VhdlStatement*
} ; // class VhdlEntityDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConfigurationDecl : public VhdlPrimaryUnit
{
public:
    VhdlConfigurationDecl(Array *context_clause, VhdlIdDef *id, VhdlIdRef *entity_name, Array *decl_part, VhdlBlockConfiguration *block_configuration, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlConfigurationDecl(const char *new_name, const VhdlConfigurationDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConfigurationDecl(SaveRestore &save_restore);

    virtual ~VhdlConfigurationDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlConfigurationDecl() ;                                         // Purposely leave unimplemented
    VhdlConfigurationDecl(const VhdlConfigurationDecl &) ;            // Purposely leave unimplemented
    VhdlConfigurationDecl& operator=(const VhdlConfigurationDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLCONFIGURATIONDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    // virtual void                PrettyPrint(ostream &f, unsigned level) const ; // Caught by VhdlPrimaryUnit::PrettyPrint() now for backward compatibility. It prints all secondary units too.
    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const ; // Print only primary unit. Not secondary units.

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Copy configuration declaration
    virtual VhdlPrimaryUnit *   CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const ; // Copy this unit with a new name. This copy routine do not add the copied unit in any library.

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node) ; // Delete 'old_node' and puts new identifier reference in its position.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Delete declaration 'old_node' and puts new declartaion in its position
    virtual unsigned            ReplaceChildBlockConfig(VhdlBlockConfiguration *old_node, VhdlBlockConfiguration *new_node) ; // Delete 'old_node' and puts new block configuration in its position.

    // These parse tree manipulation routines insert a specific node in an array-of-that-node. i.e.
    // user can insert a declaration node before/after any specific declaration in the declaration list.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before target declaration 'target_node'
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;  // Insert new declaration after target declaration 'target_node'
    // Parse-tree port creation/deletion routines
    virtual VhdlIdDef *         AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication) ; // Add new port 'port_name' to primary unit
    virtual unsigned            RemovePort(const char *port_name) ; // Remove 'port_name' from port clause

    // Parse-tree generic creation/deletion routines
    virtual VhdlIdDef *         AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign) ; // Add generic 'generic_name' to primary unit
    virtual unsigned            RemoveGeneric(const char *generic_name) ; // Remove argument specific generic

    // Parse-tree declaration addition routine
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part

    // Static Elaborator

    // Static elaborate configuration declaration.  Extract the entity to be configured, elaborate
    // the extraced entity, return the elaborated entity. Extracted entity can be copied if its generics are
    // changed and for that case copied entity is returned.
    virtual VhdlPrimaryUnit *   StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;
    // Internal routine :
    virtual VhdlPrimaryUnit *   StaticElaborateConfig(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component, unsigned is_top) ;

    // After elaboration the binding information in configuration declaration becomes useless, so those are deleted.
    virtual void                DeleteBindings() ;

    // Internal routine to static elaborate a primary unit without copying it
    virtual VhdlPrimaryUnit *   StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;

    // Accessor methods
    VhdlIdRef *                 GetEntityName() const           { return _entity_name ; }
    virtual Array *             GetDeclPart() const             { return _decl_part ; }
    VhdlBlockConfiguration *    GetBlockConfiguration() const   { return _block_configuration ; }

    virtual void                AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component) ; // Port binding. Use component for default binding if needed.
    virtual void                AssociateGenerics(Array *actual_generics, VhdlIdDef *component) ;            // Generic binding. Use component for default binding if needed.

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    virtual void                GetParameterFromGeneric(Map& parameters, Array * generics, VeriModule *mod, VhdlIdDef *component) ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlIdRef               *_entity_name ;
    Array                   *_decl_part ;     // Array of VhdlDeclaration*
    VhdlBlockConfiguration  *_block_configuration ;
} ; // class VhdlConfigurationDecl

/* -------------------------------------------------------------- */

// VHDL 2008 LRM section 13.3
class VFC_DLL_PORT VhdlContextDecl : public VhdlPrimaryUnit
{
public:
    VhdlContextDecl(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlContextDecl(const char *new_name, const VhdlContextDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlContextDecl(SaveRestore &save_restore);

    virtual ~VhdlContextDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlContextDecl() ;                                   // Purposely leave unimplemented
    VhdlContextDecl(const VhdlContextDecl &) ;            // Purposely leave unimplemented
    VhdlContextDecl& operator=(const VhdlContextDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLCONTEXTDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const ; // Print only primary unit. Not secondary units.

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Copy configuration declaration
    virtual VhdlPrimaryUnit *   CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const ; // Copy this unit with a new name. This copy routine do not add the copied unit in any library.

    // Elaborate the units that this unit depends on, elaborate its declarations and body.
    virtual VhdlPrimaryUnit *   StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;
    // Internal routine to static elaborate a primary unit without copying it
    virtual VhdlPrimaryUnit *   StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;

    // _context_clause of the base class serves for the local use clauses
    virtual Array *GetDeclPart() const      { return GetContextClause() ; }

    virtual void                AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component) ; // Port binding. Use component for default binding if needed.
    virtual void                AssociateGenerics(Array *actual_generics, VhdlIdDef *component) ;     // Generic binding. Use component for default binding if needed.

protected:
// Parse tree nodes (owned) :
} ; // class VhdlContextDecl

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPackageDecl : public VhdlPrimaryUnit
{
public:
    VhdlPackageDecl(Array *context_clause, VhdlIdDef *id, Array *generic_clause, Array *generic_map_aspect, Array *decl_part, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlPackageDecl(const char *new_name, const VhdlPackageDecl &node, VhdlMapForCopy &old2new, unsigned exclude_body) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPackageDecl(SaveRestore &save_restore, unsigned do_not_restore_members = 0);

    virtual ~VhdlPackageDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlPackageDecl() ;                                   // Purposely leave unimplemented
    VhdlPackageDecl(const VhdlPackageDecl &) ;            // Purposely leave unimplemented
    VhdlPackageDecl& operator=(const VhdlPackageDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLPACKAGEDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    // virtual void                PrettyPrint(ostream &f, unsigned level) const ; // Caught by VhdlPrimaryUnit::PrettyPrint() now for backward compatibility. It prints all secondary units too.
    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const ; // Print only primary unit. Not secondary units.

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Copy package declaration and its body
    virtual VhdlPrimaryUnit *   CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const ; // If 'exclude_archs' is 1, body is not copied. This copy routine does not attach the copied unit to any library.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const { return CopyPrimaryUnit(0, old2new, 0) ; } // Copy package decl as declaration

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Deletes 'old_node', and puts new declaration in its place

    // User can create any specific node and can insert that node in the array-of-that-type-node.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before the 'target_node' specified declaration.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;  // Insert new declaration after the 'target_node' specified declaration.

    // Parse-tree declaration addition routine
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part

#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6895: Convert vhdl package to verilog package
    virtual VeriPackage *       ConvertPackage() ;
#endif

    // Static elaborator

    // Elaborate the units that this unit depends on, elaborate its declarations and body.
    virtual VhdlPrimaryUnit *   StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;
    // Internal routine to static elaborate a primary unit without copying it
    virtual VhdlPrimaryUnit *   StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;
    virtual void                SplitAndAssociateGenerics(Array *actual_generics, VhdlIdDef *component) ; // Generic binding. Use component for default binding if needed. Set proper default/overridden value to the generic declaration.

    // VIPER #5918 : Set/get routine for container array for nested package
    virtual Array *        GetContainerArray() const        { return _container ; }
    virtual void           SetContainerArray(Array *arr)    { _container = arr ; }

    // Accessor method
    virtual Array *        GetDeclPart() const         { return _decl_part ; }
    virtual Array *        GetGenericMapAspect() const { return _generic_map_aspect ; }
    virtual Array *        GetGenericClause() const    { return _generic_clause ; }

    virtual unsigned       IsPackageDecl() const       { return 1 ; }

    virtual void                AssociatePorts(Instance *inst, Array *actual_ports, VhdlIdDef *component) ; // Port binding. Use component for default binding if needed.
    virtual void                AssociateGenerics(Array *actual_generics, VhdlIdDef *component) ;     // Generic binding. Use component for default binding if needed.
    virtual void                Elaborate(VhdlDataFlow *df) ;
#ifdef VHDL_DUAL_LANGUAGE_ELABORATION
    // VIPER #6896: Convert verilog package to vhdl one: Fill up the vhdl ranges during elaboration:
    virtual void                FillRangesAndInitialExpr() ;
#endif

protected:
// Parse tree nodes (owned) :
    // Vhdl 2008: LRM 4.7 includes generic clause and generic maps
    Array   *_generic_clause ; // Array of VhdlInterfaceDecl*
    Array   *_generic_map_aspect ;  // Array of VhdlDiscreteRange*
    Array   *_decl_part ; // Array of VhdlDeclaration*
    // If this design unit is declared in the declarative region of process/design_unit/subprogram,
    // _container will be the array for that declarative region
    Array   *_container ; // VIPER #5918
} ; // class VhdlPackageDecl

/* -------------------------------------------------------------- */

/*
  Class to represent package instantiation declaration
*/
class VFC_DLL_PORT VhdlPackageInstantiationDecl : public VhdlPrimaryUnit
{
public:
    VhdlPackageInstantiationDecl(Array *context_clause, VhdlIdDef *id, VhdlName *uninstantiated_unit_name, Array *generic_map_aspect, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlPackageInstantiationDecl(const char *new_name, const VhdlPackageInstantiationDecl &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPackageInstantiationDecl(SaveRestore &save_restore);

    virtual ~VhdlPackageInstantiationDecl() ;

private:
    // Prevent compiler from defining the following
    VhdlPackageInstantiationDecl() ;                                                // Purposely leave unimplemented
    VhdlPackageInstantiationDecl(const VhdlPackageInstantiationDecl &) ;            // Purposely leave unimplemented
    VhdlPackageInstantiationDecl& operator=(const VhdlPackageInstantiationDecl &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLPACKAGEINSTANTIATIONDECL; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    // virtual void                PrettyPrint(ostream &f, unsigned level) const ; // Caught by VhdlPrimaryUnit::PrettyPrint() now for backward compatibility. It prints all secondary units too.
    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const ; // Print only primary unit. Not secondary units.
    void                        PrettyPrintWOSemi(ostream &f, unsigned level) const ; // Print without semi colon
    virtual void                PrettyPrint(ostream &f, unsigned level) const ; // For backward compatibility : On Primary units, print all secondary units too.

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Copy package declaration and its body
    virtual VhdlPrimaryUnit *   CopyPrimaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new, unsigned exclude_archs) const ; // If 'exclude_archs' is 1, body is not copied. This copy routine does not attach the copied unit to any library.
    virtual VhdlDeclaration *   CopyDeclaration(VhdlMapForCopy &old2new) const { return CopyPrimaryUnit(0, old2new, 0) ; } // Copy package decl as declaration
    VhdlPackageInstantiationDecl *Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Deletes 'old_node', and puts new generic map/port map aspect element in its place
    // Analyze specific internal routine :
    void                        InlineUninstantiatedPackage(const VhdlIdDef *uninst_package_id, const VhdlScope *inst_scope) ;

    // Static elaborator

    // Elaborate the units that this unit depends on, elaborate its declarations and body.
    virtual VhdlPrimaryUnit *   StaticElaborate(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;
    // Internal routine to static elaborate a primary unit without copying it
    virtual VhdlPrimaryUnit *   StaticElaborateInternal(const char *secondary_unit_name, Array *actual_generics, Array *actual_ports, VhdlBlockConfiguration *block_config, VhdlIdDef *actual_component) ;

    virtual void           SetUninstantiatedPackageId(VhdlIdDef *uninstantiated_package_id) ;
    virtual VhdlIdDef *    GetUninstantiatedPackageId() const ;
    virtual void           SetActualTypes() ; // defined only for package inst
    virtual unsigned       IsPackageInstDecl() const            { return 1 ; }
    // Accessor method
    virtual Array *        GetGenericMapAspect() const          { return _generic_map_aspect ; }
    VhdlName *             GetUninstantiatedPackageName() const { return _uninst_package_name ; }
    virtual VhdlScope *    GetInstanceScope() const             { return _inst_scope ; }
    virtual Array *        GetGenericClause() const ; // Get generic clause of associated package (VIPER #7479)

    virtual void                AssociatePorts(Instance * /*inst*/, Array * /*actual_ports*/, VhdlIdDef * /*component*/) {} // Port binding. Use component for default binding if needed.
    virtual void                AssociateGenerics(Array * /*actual_generics*/, VhdlIdDef * /*component*/) {}     // Generic binding. Use component for default binding if needed.
    virtual void                Elaborate(VhdlDataFlow *df) ;
    virtual VhdlIdDef *         GetElaboratedPackageId() const ;
    virtual void                SetElaboratedPackageId(VhdlIdDef *id) ;
    virtual VhdlIdDef *         TakeElaboratedPackageId() ;
private :
    void                        SwitchActualIds() const ;
    void                        ElaborateInternal() ;

protected:
// Parse tree nodes (owned) :
    VhdlScope       *_inst_scope ;
    VhdlName        *_uninst_package_name ; // Uninstantiated package name
    Array           *_generic_map_aspect ;   // Array of VhdlDiscreteRange*

// Fields created during analysis
    Map             *_generic_types ; // Map of generic_type_id vs iddef for its actual type (from _inst_generic_map_aspect) : Set during analysis
    VhdlPrimaryUnit *_initial_pkg ; // uninstantiated package specified during declaration
    VhdlPrimaryUnit *_elab_pkg ; // Copy of uninstantiated package needed for elaboration
} ; // class VhdlPackageInstantiationDecl

/* -------------------------------------------------------------- */

/************************ Secondary Units *****************************/

/*
   VhdlSecondaryUnit is an abstract base class for secondary units.
   These include architecture and package bodies.
*/
class VFC_DLL_PORT VhdlSecondaryUnit : public VhdlDesignUnit
{
protected: // Abstract class
    VhdlSecondaryUnit(Array *context_clause, VhdlIdDef *id, VhdlScope *local_scope);

    // Copy tree node
    VhdlSecondaryUnit(const VhdlSecondaryUnit &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSecondaryUnit(SaveRestore &save_restore);

public:
    virtual ~VhdlSecondaryUnit();

private:
    // Prevent compiler from defining the following
    VhdlSecondaryUnit() ;                                     // Purposely leave unimplemented
    VhdlSecondaryUnit(const VhdlSecondaryUnit &) ;            // Purposely leave unimplemented
    VhdlSecondaryUnit& operator=(const VhdlSecondaryUnit &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ; // For backward compatibility : On Primary units, print all secondary units too.
    virtual void                PrettyPrintDesignUnit(ostream &f, unsigned level) const { PrettyPrint(f,level) ; } // Print only this design unit if implemented. Otherwise, use standard PrettyPrint().

    // Copy secondary units
    virtual VhdlSecondaryUnit * CopySecondaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new) const ; // Copy unit with 'new_unit_name'. If 'new_unit_name' is null, copied unit will be created with the old name. Copy routine does not attach the copied unit to any primary unit.
    // This virtual routine is implemented for derived classes
    virtual void                ChangeEntityReference(VhdlIdDef * /*entity id*/) { }

    // Parse tree manipulation routines :
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef * /*old_node*/, VhdlIdRef * /*new_node*/)            { return 0 ; }  // Deletes 'old_node', and puts new identifier reference in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration * /*old_node*/, VhdlDeclaration * /*new_node*/) { return 0 ; } // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement * /*old_node*/, VhdlStatement * /*new_node*/)     { return 0 ; } // Deletes 'old_node', and puts new statement in its place
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)               { return 0 ; } // Deletes 'old_node', and puts new name in its place
    virtual unsigned            ReplaceChildBy(VhdlStatement * /*old*/, Array * /*new_stmts*/)                   { return 0 ; }
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)  { return 0 ; }  // Insert new declaration before 'target_node' specific declaration.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)   { return 0 ; }  // Insert new declaration after 'target_node' specific declaration.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)      { return 0 ; }  // Insert new Statement 'new_node' before 'target_node' specific statement
    virtual unsigned            InsertAfterStmt(const VhdlStatement * /*target_node*/, VhdlStatement * /*new_node*/)       { return 0 ; }  // Insert new Statement 'new_node' after 'target_node' specific statement

    // Parse-tree Signal creation/deletion routines
    virtual VhdlIdDef *         AddSignal(const char * /*signal_name*/, VhdlSubtypeIndication * /*subtype_indication*/, VhdlExpression * /*init_assign*/) { return 0 ; } // Create and add signal named 'signal_name'
    virtual unsigned            RemoveSignal(const char * /*signal_name*/) { return 0 ; } // Remove argument specific signal

    // Parse-tree instance creation/deletion routines
    virtual VhdlComponentInstantiationStatement * AddInstance(const char * /*instance_name*/, const char * /*instantiated_component_name*/) { return 0 ; } // Add component instance, implemented for architecture only
    virtual unsigned                              RemoveInstance(const char * /*instance_name*/) { return 0 ; } // Remove argument specific instance

    // Parse-tree instance port connection routines
    virtual unsigned            AddPortRef(const char * /*instance_name*/, const char * /*formal_port_name*/, VhdlExpression * /*actual*/) { return 0 ; } // Add port-ref to 'instance_name' specific instantiation
    virtual unsigned            RemovePortRef(const char * /*instance_name*/, const char * /*formal_port_name*/) { return 0 ; } // Remove actual for formal 'formal_port_name'

    // Parse-tree instance generic connection routines
    virtual unsigned            AddGenericRef(const char * /*instance_name*/, const char * /*formal_generic_name*/, VhdlExpression * /*actual*/) { return 0 ; } // Create and add generic reference
    virtual unsigned            RemoveGenericRef(const char * /*instance_name*/, const char * /*formal_generic_name*/) { return 0 ; } // Remove actual for formal 'formal_generic_name'

    // Parse-tree component declaration creation/deletion routines
    virtual VhdlIdDef *         AddComponentDecl(const char * /*component_name*/) { return 0 ; } // Create and insert a component named 'component_name' in secondary unit
    virtual unsigned            RemoveComponentDecl(const char * /*component_name*/) { return 0 ; } // Remove argument specific component from declaration list of secondary unit.

    // Parse-tree declaration addition routine (VIPER 2375)
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part
    virtual void                AddStatement(VhdlStatement *new_stmt) ; // Add argument specific statement to the statement part (VIPER #2822)

    // Variable usage analysis
    virtual void                VarUsageStartingPoint() const { }

    // Static Elaborator

    // This pure virtual routine is used to elaboarte secondary units.
    virtual void                StaticElaborate(VhdlBlockConfiguration *block_config)=0 ;

    virtual void                AddBindInstance(const char * /*inst_name*/, VhdlComponentInstantiationStatement * /*entity_instance*/) { return ; }
    virtual void                AddVerilogBindInstance(VeriModuleInstantiation * /*module_instance*/) { return ; }
    virtual Array *             BindInstances()             { return 0 ; }

    virtual unsigned            IsSecondaryUnit() const     { return 1; }    // Redefining VhdlDesignUnit's implementation

    // Owner accessor/modifier methods
    virtual VhdlLibrary *       GetOwningLib() const        { return (_owner) ? _owner->GetOwningLib() : 0 ; }  // Get library which owns the primary unit that owns this secondary unit
    virtual VhdlPrimaryUnit *   GetOwningUnit() const       { return _owner; }      // Get primary unit which owns this secondary unit
    virtual void                SetOwningUnit(VhdlPrimaryUnit *owner) ;             // Set primary unit which owns this secondary unit
    virtual unsigned            HasOwner() const ;                                  // Does this secondary unit have a owner?  This should always be 'yes'.
    virtual VhdlPrimaryUnit *   Owner() const               { return _owner ; }   // Get owner (primary unit) of this secondary unit.
    virtual Array *             GetDeclPart() const         { return 0 ; }

protected:
// (Back) pointer set during analysis (not owned) :
    VhdlPrimaryUnit *_owner ; // The primary unit this unit is stored in
} ; // class VhdlSecondaryUnit

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlArchitectureBody : public VhdlSecondaryUnit
{
public:
    VhdlArchitectureBody(Array *context_clause, VhdlIdDef *id, VhdlIdRef *entity_name, Array *decl_part, Array *statement_part, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlArchitectureBody(const char *new_name, const VhdlArchitectureBody &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlArchitectureBody(SaveRestore &save_restore);

    virtual ~VhdlArchitectureBody() ;

private:
    // Prevent compiler from defining the following
    VhdlArchitectureBody() ;                                        // Purposely leave unimplemented
    VhdlArchitectureBody(const VhdlArchitectureBody &) ;            // Purposely leave unimplemented
    VhdlArchitectureBody& operator=(const VhdlArchitectureBody &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLARCHITECTUREBODY; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsageStartingPoint() const ;

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Copy architecture with a 'new_name'. If 'new_name' is null, the duplicate copy obtains original name.
    virtual VhdlSecondaryUnit * CopySecondaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new) const ; // Copied architecture is not attached to any entity by this routine.
    // Change the name of the entity referred by this architecture.
    virtual void                ChangeEntityReference(VhdlIdDef *entity_id) ;

    // VIPER #6725: Returns the linefile spanning decl or statement part of an *empty* architecture.
    // Note that it returns an *allocated* linefile, so avoid unnecessary calling.
    // At the same time, you do not need to delete the linefile either!
    // It only works under linefile-column mode, if it is not enabled, returns 0.
    linefile_type               GetEmptyDeclPartLinefile() const ;
    linefile_type               GetEmptyStmtPartLinefile() const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node) ;             // Deletes 'old_node', and puts new identifier reference in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ;     // Deletes 'old_node', and puts new statement in its place
    virtual unsigned            ReplaceChildBy(VhdlStatement *old, Array *new_stmts) ;

    // Insert a specific type node in the array-of-that-type-nodes
    // i.e user can add a declaration before/after any specific declaration in the declaration list.
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node' specific declaration.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;  // Insert new declaration after 'target_node' specific declaration.
    virtual unsigned            InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ;     // Insert new Statement 'new_node' before 'target_node' specific statement
    virtual unsigned            InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ;      // Insert new Statement 'new_node' after 'target_node' specific statement

    // Parse-tree Signal creation/deletion routines
    virtual VhdlIdDef *         AddSignal(const char *signal_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *init_assign) ; // Create and add signal named 'signal_name'
    virtual unsigned            RemoveSignal(const char *signal_name) ; // Remove argument specific signal

    // Parse-tree instance creation/deletion routines
    virtual VhdlComponentInstantiationStatement * AddInstance(const char *instance_name, const char *instantiated_component_name) ; // Add component instance, implemented for architecture only
    virtual unsigned                              RemoveInstance(const char *instance_name) ; // Remove argument specific instance

    // Parse-tree instance port connection routines
    virtual unsigned            AddPortRef(const char *instance_name, const char *formal_port_name, VhdlExpression *actual) ; // Add port-ref to 'instance_name' specific instantiation
    virtual unsigned            RemovePortRef(const char *instance_name, const char *formal_port_name) ; // Remove actual for formal 'formal_port_name'

    // Parse-tree instance generic connection routines
    virtual unsigned            AddGenericRef(const char *instance_name, const char *formal_generic_name, VhdlExpression *actual) ; // Create and add generic reference
    virtual unsigned            RemoveGenericRef(const char *instance_name, const char *formal_generic_name) ; // Remove actual for formal 'formal_generic_name'

    // Parse-tree component declaration creation/deletion routines
    virtual VhdlIdDef *         AddComponentDecl(const char *component_name) ; // Create and insert a component named 'component_name' in secondary unit
    virtual unsigned            RemoveComponentDecl(const char *component_name) ; // Remove argument specific component from declaration list of secondary unit.

    // Parse-tree declaration addition routine (VIPER 2375)
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part
    virtual void                AddStatement(VhdlStatement *new_stmt) ; // Add argument specific statement to the statement part (VIPER #2822)

    // Static Elaborator

    // Static elaborate the architecture.  Replace component instantiation with direct
    // entity instantiation, delete component configuration, unroll generate.
    virtual void                StaticElaborate(VhdlBlockConfiguration *block_config) ;

    virtual void                 AddBindInstance(const char *inst_name, VhdlComponentInstantiationStatement *entity_instance) ;
    virtual void                 AddVerilogBindInstance(VeriModuleInstantiation *module_instance) ;
    virtual Array *              BindInstances()             { return _bind_instances ; }

    // Accessor methods
    VhdlIdRef *                 GetEntityName() const       { return _entity_name ; }
    virtual Array *             GetDeclPart() const         { return _decl_part ; }
    virtual Array *             GetStatementPart() const    { return _statement_part ; }

protected:
// Parse tree nodes (owned)
    VhdlIdRef   *_entity_name ;     // Architecture name
    Array       *_decl_part ;       // Array of VhdlDeclaration*
    Array       *_statement_part ;  // Array of VhdlStatement*
    Array       *_bind_instances ;  // Array of VhdlComponentInstantiationStatement*
    Array       *_bind_verilog_instances ;  // Array of VeriModuleInstance* these are stored in Varusage and converted in elaboration: Viper 7225

    Set         *_access_allocated_values ; // leaked access values (VIPER #4464, 7292)
} ; // class VhdlArchitectureBody

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlPackageBody : public VhdlSecondaryUnit
{
public:
    VhdlPackageBody(Array *context_clause, VhdlIdDef *id, Array *decl_part, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlPackageBody(const char *new_name, const VhdlPackageBody &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlPackageBody(SaveRestore &save_restore);

    virtual ~VhdlPackageBody() ;

private:
    // Prevent compiler from defining the following
    VhdlPackageBody() ;                                   // Purposely leave unimplemented
    VhdlPackageBody(const VhdlPackageBody &) ;            // Purposely leave unimplemented
    VhdlPackageBody& operator=(const VhdlPackageBody &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLPACKAGEBODY; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ; // Print only this design unit (not secondary units if this is a primary unit)

    // Calculate the conformance signature of this design unit.
    virtual unsigned long       CalculateSignature() const ;

    // Copy package body.
    virtual VhdlSecondaryUnit * CopySecondaryUnit(const char *new_unit_name, VhdlMapForCopy &old2new) const ; // Copy package body as a separate unit. The copied unit is not attached to any package declaration by this routine.

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ;    // Delete 'old_node', puts new declaration in its place.

    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert 'new_node' before the 'target_node' specific declaration.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ;  // Insert 'new_node' after the 'target_node' specific declaration.
    
    // Parse-tree declaration addition routine
    virtual void                AddDeclaration(VhdlDeclaration *new_decl) ; // Add argument specific declaration to the declarative part

    // Static Elaborator

    // Elaborate the declarations of the package body.
    virtual void                StaticElaborate(VhdlBlockConfiguration *block_config) ;
    virtual unsigned            IsPackageBody() const   { return 1 ; } // Returns 1 for package body

    // Accessor method
    virtual Array *             GetDeclPart() const     { return _decl_part ; }

protected:
// Parse tree nodes (owned) :
    Array   *_decl_part ;   // Array of VhdlDeclaration*
} ; // class VhdlPackageBody

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_UNITS_H_

