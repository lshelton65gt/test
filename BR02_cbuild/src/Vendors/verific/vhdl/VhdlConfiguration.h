/*
 *
 * [ File Version : 1.82 - 2014/02/18 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_CONFIGURATION_H_
#define _VERIFIC_VHDL_CONFIGURATION_H_

#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Netlist ;

class VhdlScope ;
class VhdlName ;
class VhdlSpecification ;
class VhdlBindingIndication ;
class VhdlIdDef ;
class VhdlConstraint ;
class VhdlBlockConfiguration ;
class VhdlMapForCopy ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlConfigurationItem : public VhdlTreeNode
{
protected: // Abstract class
    VhdlConfigurationItem() ;

    // Copy tree node
    VhdlConfigurationItem(const VhdlConfigurationItem &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConfigurationItem(SaveRestore &save_restore);

public:
    virtual ~VhdlConfigurationItem() ;

private:
    // Prevent compiler from defining the following
    VhdlConfigurationItem(const VhdlConfigurationItem &) ;            // Purposely leave unimplemented
    VhdlConfigurationItem& operator=(const VhdlConfigurationItem &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const = 0 ;

    // Calculate the conformance signature of this configuration
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse tree
    virtual VhdlConfigurationItem * CopyConfigurationItem(VhdlMapForCopy &old2new) const ;

    virtual Array *             GetConfigurationItemList() const { return 0 ; }
    // Specific parse tree manipulation routines :
    virtual unsigned            ReplaceChildName(VhdlName * /*old_block_spec*/, VhdlName * /*new_block_spec*/)                                  { return 0 ; } // Delete 'old_block_spec', puts new block spec in its place.
    virtual unsigned            ReplaceChildUseClause(VhdlUseClause * /*old_use_clause*/, VhdlUseClause * /*new_use_clause*/)                   { return 0 ; } // Delete 'old_use_clause', puts new use clause in its place.
    virtual unsigned            ReplaceChildConfigItem(VhdlConfigurationItem * /*old_item*/, VhdlConfigurationItem * /*new_item*/)              { return 0 ; } // Delete 'old_item', puts new configuration item in its place.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification * /*old_com_spec*/, VhdlSpecification * /*new_com_spec*/)                    { return 0 ; } // Delete 'old_com_spec', puts  new specification in its place.
    virtual unsigned            ReplaceChildBindingIndication(VhdlBindingIndication * /*old_binding*/, VhdlBindingIndication * /*new_binding*/) { return 0 ; } // Delete 'old_binding', puts new binding indication in its place.
    virtual unsigned            ReplaceChildBlockConfig(VhdlBlockConfiguration * /*old_node*/, VhdlBlockConfiguration * /*new_node*/)           { return 0 ; } // Delete 'old_node', puts new block configuration in its place.
    virtual unsigned            InsertBeforeUseClause(const VhdlUseClause * /*target_node*/, VhdlUseClause * /*new_node*/)                      { return 0 ; } // Insert new use clause before 'target_node' specified use clause in the use clause list
    virtual unsigned            InsertBeforeConfigItem(const VhdlConfigurationItem * /*target_node*/, VhdlConfigurationItem * /*new_node*/)     { return 0 ; } // Insert new configuration item  before 'target_node' specified configuration item  in the configuration item  list
    virtual unsigned            InsertAfterUseClause(const VhdlUseClause * /*target_node*/, VhdlUseClause * /*new_node*/)                       { return 0 ; } // Insert new use clause after 'target_node' specified use clause in the use clause list
    virtual unsigned            InsertAfterConfigItem(const VhdlConfigurationItem * /*target_node*/, VhdlConfigurationItem * /*new_node*/)      { return 0 ; } // Insert new configuration item  after 'target_node' specified configuration item  in the configuration item  list

    // Static Elaboration.
    virtual void                DeleteBindings() = 0 ;

    virtual VhdlIdDef *         BlockLabel() const =0 ; // return the block label for this block configuration. (can be architecture or block label or generate statement label). return 0 if this is a component configuration.

    // Elaboration
    virtual void                Elaborate()=0 ; // elaborate the block config (creates _block_range and calls execute on lower level block config items)
    // And find deeper (sub)block-configurations
    virtual unsigned            MatchBlockConfig(VhdlIdDef * /*label*/, VhdlValue * /*iter_value*/, VhdlIdDef * /*alt_label*/) { return 0 ; }
    virtual unsigned            MatchComponentConfig(VhdlIdDef * /*label*/) { return 0 ; } // check if this configuration item matches the label
    virtual VhdlConstraint *    GetBlockRange() const    { return 0 ; }
} ; // class VhdlConfigurationItem

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlBlockConfiguration : public VhdlConfigurationItem
{
public:
    VhdlBlockConfiguration(VhdlName *block_spec, Array *use_clause_list, Array *configuration_item_list, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlBlockConfiguration(const VhdlBlockConfiguration &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBlockConfiguration(SaveRestore &save_restore);

    virtual ~VhdlBlockConfiguration() ;

private:
    // Prevent compiler from defining the following
    VhdlBlockConfiguration() ;                                          // Purposely leave unimplemented
    VhdlBlockConfiguration(const VhdlBlockConfiguration &) ;            // Purposely leave unimplemented
    VhdlBlockConfiguration& operator=(const VhdlBlockConfiguration &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLBLOCKCONFIGURATION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this configuration
    virtual unsigned long       CalculateSignature() const ;

    // Copy block configuration
    virtual VhdlConfigurationItem * CopyConfigurationItem(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine
    virtual unsigned            ReplaceChildName(VhdlName *old_block_spec, VhdlName *new_block_spec) ;                     // Delete 'old_block_spec', puts new block spec in its place.
    virtual unsigned            ReplaceChildUseClause(VhdlUseClause *old_use_clause, VhdlUseClause *new_use_clause) ;      // Delete 'old_use_clause', puts new use clause in its place.
    virtual unsigned            ReplaceChildConfigItem(VhdlConfigurationItem *old_item, VhdlConfigurationItem *new_item) ; // Delete 'old_item', puts new configuration item in its place.

    virtual unsigned            InsertBeforeUseClause(const VhdlUseClause *target_node, VhdlUseClause *new_node) ; // Insert new use clause before 'target_node' specified use clause in the use clause list
    virtual unsigned            InsertBeforeConfigItem(const VhdlConfigurationItem *target_node, VhdlConfigurationItem *new_node) ; // Insert new configuration item  before 'target_node' specified configuration item  in the configuration item  list
    virtual unsigned            InsertAfterUseClause(const VhdlUseClause *target_node, VhdlUseClause *new_node) ; // Insert new use clause after 'target_node' specified use clause in the use clause list
    virtual unsigned            InsertAfterConfigItem(const VhdlConfigurationItem *target_node, VhdlConfigurationItem *new_node) ; // Insert new configuration item  after 'target_node' specified configuration item  in the configuration item  list

    // Static Elaboration.
    virtual void                DeleteBindings() ; // delete binding information from 'this' block and from its sub-blocks

    // Accessor methods
    VhdlName *                  GetBlockSpec() const             { return _block_spec ; }
    Array *                     GetUseClauseList() const         { return _use_clause_list ; }
    virtual Array *             GetConfigurationItemList() const { return _configuration_item_list ; }
    virtual VhdlScope *         LocalScope() const ;
    virtual VhdlIdDef *         BlockLabel() const               { return _block_label ; } // Label of this block.
    unsigned                    GetHashedNum(const char *prefix) const ; // VIPER #6038/6032
    virtual VhdlConstraint *    GetBlockRange() const            { return _block_range ; }

    // Elaboration
    virtual void                Elaborate() ; // elaborate the block config (creates _block_range and calls execute on lower level block config items)

    VhdlBlockConfiguration *    ExtractSubBlock(VhdlIdDef *label, VhdlValue *iter_value, VhdlIdDef *alt_label) const ;
    VhdlComponentConfiguration *ExtractComponentConfig(VhdlIdDef *label) const ;

    // And find deeper (sub)block-configurations
    virtual unsigned            MatchBlockConfig(VhdlIdDef *label, VhdlValue *iter_value, VhdlIdDef *alt_label) ;
    void                        CheckLoopIndexRange(const VhdlIdDef *label, const VhdlConstraint *label_consraint) const ;

protected:
// Parse tree nodes (owned) :
    VhdlName        *_block_spec ;
    Array           *_use_clause_list ;         // Array of VhdlUseClause*
    Array           *_configuration_item_list ; // Array of VhdlConfigurationItem*
    VhdlScope       *_local_scope ;
// Fields set during analysis :
    VhdlIdDef       *_block_label ;             // Target block label
// Fields set during elaboration :
    VhdlConstraint  *_block_range ;             // Target (generate) range : set at elaboration time.
} ; // class VhdlBlockConfiguration

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlComponentConfiguration : public VhdlConfigurationItem
{
public:
    VhdlComponentConfiguration(VhdlSpecification *component_spec, VhdlBindingIndication *binding, VhdlBlockConfiguration *block_configuration, VhdlScope *local_scope) ;

    // Copy tree node
    VhdlComponentConfiguration(const VhdlComponentConfiguration &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlComponentConfiguration(SaveRestore &save_restore);

    virtual ~VhdlComponentConfiguration() ;

private:
    // Prevent compiler from defining the following
    VhdlComponentConfiguration() ;                                              // Purposely leave unimplemented
    VhdlComponentConfiguration(const VhdlComponentConfiguration &) ;            // Purposely leave unimplemented
    VhdlComponentConfiguration& operator=(const VhdlComponentConfiguration &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCOMPONENTCONFIGURATION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this configuration
    virtual unsigned long       CalculateSignature() const ;

    // Copy component configuration
    virtual VhdlConfigurationItem * CopyConfigurationItem(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildSpec(VhdlSpecification *old_com_spec, VhdlSpecification *new_com_spec) ;                    // Delete 'old_com_spec', puts  new specification in its place.
    virtual unsigned            ReplaceChildBindingIndication(VhdlBindingIndication *old_binding, VhdlBindingIndication *new_binding) ; // Delete 'old_binding', puts new binding indication in its place.
    virtual unsigned            ReplaceChildBlockConfig(VhdlBlockConfiguration *old_node, VhdlBlockConfiguration *new_node) ;           // Delete 'old_node', puts new block configuration in its place.

    // Static Elaboration.
    // Delete binding information for the components specified by 'this'.
    // Also delete binding information from inner blocks.
    virtual void                DeleteBindings() ;

    // Accessor methods
    VhdlSpecification *         GetComponentSpec() const        { return _component_spec ; }
    VhdlBindingIndication *     GetBinding() const              { return _binding ; }
    VhdlBlockConfiguration *    GetBlockConfiguration() const   { return _block_configuration ; }
    virtual VhdlScope *         LocalScope() const ;
    virtual VhdlIdDef *         BlockLabel() const              { return 0 ; } // this is not a block configuration.

    // Elaboration
    virtual void                Elaborate() ; // elaborate the block config (creates _block_range and calls execute on lower level block config items)

    // And find deeper (sub)block-configurations
    virtual unsigned            MatchComponentConfig(VhdlIdDef *label) ; // check if the configuration item matches the label

protected:
// Parse tree nodes (owned) :
    VhdlSpecification       *_component_spec ;
    VhdlBindingIndication   *_binding ;
    VhdlBlockConfiguration  *_block_configuration ;
    VhdlScope               *_local_scope ;
} ; // class VhdlComponentConfiguration

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_CONFIGURATION_H_

