/*
 *
 * [ File Version : 1.101 - 2014/02/05 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_SPECIFICATION_H_
#define _VERIFIC_VHDL_SPECIFICATION_H_

#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Map ;
class VhdlName ;
class VhdlIdDef ;

class VhdlValue ;
class VhdlDataFlow ;
class VhdlBindingIndication ;
class VhdlBlockConfiguration ;
class VhdlMapForCopy ;

class VhdlVarUsageInfo ;

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlSpecification : public VhdlTreeNode
{
protected: // Abstract class
    VhdlSpecification() ;

    // Copy tree node
    VhdlSpecification(const VhdlSpecification &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSpecification(SaveRestore &save_restore);

public:
    virtual ~VhdlSpecification() ;

private:
    // Prevent compiler from defining the following
    VhdlSpecification(const VhdlSpecification &) ;            // Purposely leave unimplemented
    VhdlSpecification& operator=(const VhdlSpecification &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    virtual void                ResolveSpecs(VhdlScope * /*scope*/, VhdlIdDef * /*attr*/, VhdlExpression * /*attr_value*/) { } // attribute specifications
    virtual VhdlIdDef *         ResolveSpecs(VhdlBindingIndication * /*binding*/, unsigned /*incremental*/) { return 0 ; } // component specifications. Tie a binding to the component spec and type-infer the incoming binding. flag if 'incremental' (from component config). Return full binding unit.
    virtual unsigned            MatchLabel(VhdlIdDef * /*label*/) { return 0 ; } // Check if this component spec specifies this label.
    virtual void                CheckLabel(Map * /*all_other_map*/)  {  } // Viper 4738: Check whether : all or :others is present

    // Calculate the conformance signature of this specification
    virtual unsigned long       CalculateSignature() const ;

    virtual VhdlName *          GetReturnType() const           { return 0 ; } // good for function spec
    virtual void                Validate(VhdlScope *s, VhdlIdDef *attr, VhdlExpression *attr_value) ; // VIPER #3645 : check whether used identifiers are already declared or not

    // Copy specification.
    virtual VhdlSpecification * CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const ;
    // Internal routine to copy attribute specification
    virtual void                SetEntityIdsForCopy(VhdlMapForCopy & /*old2new*/, VhdlIdDef * /*attr_id*/, VhdlExpression * /*attr_value*/) {} // Set copied back pointers to entity specification
    virtual void                SetLocalScope(VhdlScope * /*scope*/) { VERIFIC_ASSERT(0) ; } // Set the containing scope in 'this'

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef * /*old_node*/, VhdlIdDef * /*new_node*/)               { return 0 ; } // Deletes 'old_node', and puts new identifier in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration * /*old_node*/, VhdlDeclaration * /*new_node*/) { return 0 ; } // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildName(VhdlName * /*old_node*/, VhdlName * /*new_node*/)               { return 0 ; } // Deletes 'old_node', and puts new name in its place
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)  { return 0 ; } // Insert new declaration before 'target_node' in the array of declarations.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration * /*target_node*/, VhdlDeclaration * /*new_node*/)   { return 0 ; } // Insert new declaration after 'target_node' in the array of declarations.
    virtual unsigned            InsertBeforeName(const VhdlName * /*target_node*/, VhdlName * /*new_node*/)                { return 0 ; } // Insert new name before 'target_node' in the array of names .
    virtual unsigned            InsertAfterName(const VhdlName * /*target_node*/, VhdlName * /*new_node*/)                 { return 0 ; } // Insert new name after 'target_node' in the array of names .

    virtual VhdlIdDef *         GetId() const ;  // Return the object that is specified

    virtual Set *               GetAllIds() const               { return 0 ; } // Viper #5432: For VhdlEntitySpec

    virtual unsigned            IsFunction() const ;
    virtual unsigned            IsProcedure() const ;
    virtual unsigned            IsImpure() const ;
    // Calculate whether explicit parameter is used Vhdl2008
    virtual unsigned            IsExplicitParameter() const     { return 0 ; } // Viper 6653 For explicit parameter
    // Check if a subprogram is uninstantiated
    virtual unsigned            IsUninstantiatedSubprogram() const { return 0 ; } //Defined for function and procedure spec

    // Elaboration
    virtual void                InitialValues(VhdlDataFlow *df, VhdlScope *assoc_list_scope) ; // Fill-in initial values of subprograms
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)
    virtual void                Elaborate(VhdlDataFlow *df) ;    // Elaboration called once only
    // Configurations
    // virtual void                Configure(VhdlBindingIndication *binding, VhdlBlockConfiguration *block_config) ;
    // For entity specs
    virtual void                SetAttribute(VhdlIdDef *attr, VhdlValue *value) ;
    // VIPER #5918 : For generic handling
    void                        AssociateGenerics(Array *actual_generics, Map *generic_to_constraint, Map *generic_to_value, Map *generic_to_id, Array *all_generics) const ;
    void                        SwitchValueConstraintOfGenerics(const Array *all_generics, Map *generic_to_constraint, Map *generic_to_value, Map *generic_to_id) const ;
    virtual VhdlIdDef *         GetDesignator() const           { return 0 ; } // Same functionality as GetId(), but the name is more explicit
    virtual Array *             GetFormalParamList() const      { return 0 ; } // Defined for VhdlProcedureSpec
    virtual Array *             GetGenericMapAspect() const     { return 0 ; } // VHDL-2008 LRM Section 4.2.1
    virtual Array *             GetGenericClause() const        { return 0 ; } // VHDL-2008 LRM Section 4.2.1

protected:
} ; // class VhdlSpecification

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlProcedureSpec : public VhdlSpecification
{
public:
    VhdlProcedureSpec(VhdlIdDef *designator, Array *generic_clause, Array *generic_map_aspect, Array *formal_parameter_list, VhdlScope *local_scope, unsigned has_explicit_param) ;

    // Copy tree node
    VhdlProcedureSpec(const VhdlProcedureSpec &node, unsigned exclude_scope, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlProcedureSpec(SaveRestore &save_restore);

    virtual ~VhdlProcedureSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlProcedureSpec() ;                                     // Purposely leave unimplemented
    VhdlProcedureSpec(const VhdlProcedureSpec &) ;            // Purposely leave unimplemented
    VhdlProcedureSpec& operator=(const VhdlProcedureSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLPROCEDURESPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this specification
    virtual unsigned long       CalculateSignature() const ;

    // Calculate whether explicit parameter is used Vhdl2008
    virtual unsigned            IsExplicitParameter() const { return _has_explicit_param ; }

    // Copy procedure specification.
    // The procedure id is copied, but is not attached to
    // any scope.
    virtual VhdlSpecification * CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const ;
    virtual void                SetLocalScope(VhdlScope *scope) { _local_scope = scope ; } // Set containing scope in 'this'

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Deletes 'old_node', and puts new identifier in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node' in the array of declarations.
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after 'target_node' in the array of declarations.

    virtual VhdlIdDef *         GetId() const ; // Return the designator
    virtual unsigned            IsProcedure() const ;

    // Accessor methods
    virtual VhdlIdDef *         GetDesignator() const           { return _designator ; } // Same functionality as GetId(), but the name is more explicit
    virtual Array *             GetFormalParamList() const      { return _formal_parameter_list ; }
    virtual VhdlScope *         LocalScope() const              { return _local_scope ; }
    virtual Array *             GetGenericMapAspect() const     { return _generic_map_aspect ; } // VHDL-2008 LRM Section 4.2.1
    virtual Array *             GetGenericClause() const        { return _generic_clause ; } // VHDL-2008 LRM Section 4.2.1

    // Check if a subprogram is uninstantiated
    virtual unsigned            IsUninstantiatedSubprogram() const ;

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ; // Elaboration called once only
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)
    virtual void                InitialValues(VhdlDataFlow *df, VhdlScope *assoc_list_scope) ;

protected:
// Parse tree nodes (owned) :
    VhdlIdDef   *_designator ;
    Array       *_formal_parameter_list ; // Array of VhdlInterfaceDecl*
    VhdlScope   *_local_scope ;
    Array       *_generic_clause ; // 2008 4.2.1 Array of VhdlInterfaceDecl*
    Array       *_generic_map_aspect ;  // 4.2.1 Array of VhdlDiscreteRange*
    unsigned    _has_explicit_param:1 ; // Viper 6653: stores whether explicit parameters are used or not
} ; // class VhdlProcedureSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlFunctionSpec : public VhdlSpecification
{
public:
    VhdlFunctionSpec(unsigned pure_impure, VhdlIdDef *designator, Array *generic_clause, Array *generic_map_aspect, Array *formal_parameter_list, VhdlName *return_type, VhdlScope *local_scope, unsigned has_explicit_param) ;

    // Copy tree node
    VhdlFunctionSpec(const VhdlFunctionSpec &node, unsigned exclude_scope, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlFunctionSpec(SaveRestore &save_restore);

    virtual ~VhdlFunctionSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlFunctionSpec() ;                                    // Purposely leave unimplemented
    VhdlFunctionSpec(const VhdlFunctionSpec &) ;            // Purposely leave unimplemented
    VhdlFunctionSpec& operator=(const VhdlFunctionSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFUNCTIONSPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this specification
    virtual unsigned long       CalculateSignature() const ;

    // Copy function specification. Function id is copied,
    // but is not declared in any scope.
    virtual VhdlSpecification * CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const ;
    virtual void                SetLocalScope(VhdlScope *scope) { _local_scope = scope ; } // Set containing scope in 'this' node.

    // Calculate whether explicit parameter is used Vhdl2008
    virtual unsigned            IsExplicitParameter() const { return _has_explicit_param ; }

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Deletes 'old_node', and puts new identifier in its place
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Deletes 'old_node', and puts new declaration in its place
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place

    virtual unsigned            InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node' in the array of declarations .
    virtual unsigned            InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after 'target_node' in the array of declarations .

    virtual VhdlIdDef *         GetId() const ; // Return the designator
    virtual unsigned            IsFunction() const ;

    // Accessor methods
    unsigned                    IsPure() const ;
    virtual unsigned            IsImpure() const ;
    virtual VhdlIdDef *         GetDesignator() const           { return _designator ; } // Same functionality as GetId(), but the name is more explicit
    virtual Array *             GetFormalParamList() const      { return _formal_parameter_list ; }
    virtual VhdlName *          GetReturnType() const           { return _return_type ; }
    // CARBON_BEGIN  
    void                        SetReturnType(VhdlName* ret_type) {_return_type = ret_type; }
    // CARBON_END
    virtual VhdlScope *         LocalScope() const              { return _local_scope ; }
    virtual Array *             GetGenericMapAspect() const     { return _generic_map_aspect ; } // VHDL-2008 LRM Section 4.2.1
    virtual Array *             GetGenericClause() const        { return _generic_clause ; } // VHDL-2008 LRM Section 4.2.1

    // Check if a subprogram is uninstantiated
    virtual unsigned            IsUninstantiatedSubprogram() const ;

    // Elaboration
    virtual void                Elaborate(VhdlDataFlow *df) ; // Elaboration called once only
    virtual void                InitializeConstraints(Map &formal_to_constraint) ; // Create constraints and insert in the Map (id->constraint)
    virtual void                InitialValues(VhdlDataFlow *df, VhdlScope *assoc_list_scope) ;

protected:
// Parse tree nodes (owned) :
    unsigned    _pure_impure ;
    VhdlIdDef   *_designator ;
    Array       *_formal_parameter_list ; // Array of VhdlInterfaceDecl*
    VhdlName    *_return_type ;
    VhdlScope   *_local_scope ;
    Array       *_generic_clause ; // 2008 4.2.1 Array of VhdlInterfaceDecl*
    Array       *_generic_map_aspect ;  // 4.2.1 Array of VhdlDiscreteRange*
    unsigned    _has_explicit_param:1 ; // Viper 6653: stores whether explicit parameters are used or not
} ; // class VhdlFunctionSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlComponentSpec : public VhdlSpecification
{
public:
    VhdlComponentSpec(Array *id_list, VhdlName *name) ;

    // Copy tree node
    VhdlComponentSpec(const VhdlComponentSpec &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlComponentSpec(SaveRestore &save_restore);

    virtual ~VhdlComponentSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlComponentSpec() ;                                     // Purposely leave unimplemented
    VhdlComponentSpec(const VhdlComponentSpec &) ;            // Purposely leave unimplemented
    VhdlComponentSpec& operator=(const VhdlComponentSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCOMPONENTSPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Find labels and set (primary) binding backpointers for this component specification.
    virtual VhdlIdDef *         ResolveSpecs(VhdlBindingIndication *binding, unsigned incremental) ; // component specifications. Tie a binding to the component spec and type-infer the incoming binding. flag if 'incremental' (from component config). Return full binding unit.

    // Check if this component spec specifies this label (works after ResolveSpecs() ran.
    virtual unsigned            MatchLabel(VhdlIdDef *label) ;
    virtual void                CheckLabel(Map *all_other_map) ;

    // Calculate the conformance signature of this specification
    virtual unsigned long       CalculateSignature() const ;

    // Copy component specification.
    virtual VhdlSpecification * CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            InsertBeforeName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name before 'target_node' in the array of names .
    virtual unsigned            InsertAfterName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name after 'target_node' in the array of names .

    virtual VhdlIdDef *         GetId() const ; // Return the component specified

    // Accessor methods
    Array *                     GetIds() const              { return _id_list ; }
    VhdlName *                  GetName() const             { return _name ; }
    VhdlIdDef *                 GetComponent() const        { return _component ; }  // Same functionality as GetId(), but the name is more explicit
    VhdlScope *                 GetScope() const            { return _scope ; }

    // Elaboration
    // virtual void                Configure(VhdlBindingIndication *binding, VhdlBlockConfiguration *block_config) ;

protected:
// Parse tree nodes (owned) :
    Array       *_id_list ;   // list of VhdlName : the labels (instances) of the component.
    VhdlName    *_name ;      // name of the component.
// (back) pointers set at analysis time (not owned) :
    VhdlIdDef   *_component ; // The (name resolved) component that is specified (returned with GetId())
    VhdlScope   *_scope ;     // Present scope, Needed for elaboration, to find the labels in id_list.
} ; // class VhdlComponentSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlGuardedSignalSpec : public VhdlSpecification
{
public:
    VhdlGuardedSignalSpec(Array *signal_list, VhdlName *type_mark) ;

    // Copy tree node
    VhdlGuardedSignalSpec(const VhdlGuardedSignalSpec &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlGuardedSignalSpec(SaveRestore &save_restore);

    virtual ~VhdlGuardedSignalSpec() ;

private:
    // Prevent compiler from defining the following
    VhdlGuardedSignalSpec() ;                                         // Purposely leave unimplemented
    VhdlGuardedSignalSpec(const VhdlGuardedSignalSpec &) ;            // Purposely leave unimplemented
    VhdlGuardedSignalSpec& operator=(const VhdlGuardedSignalSpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLGUARDEDSIGNALSPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this specification
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    virtual VhdlSpecification * CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            InsertBeforeName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name before 'target_node' in the array of names .
    virtual unsigned            InsertAfterName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name before 'target_node' in the array of names .

    // Accessor methods
    Array *                     GetSignalList() const       { return _signal_list ; }
    VhdlName *                  GetTypeMark() const         { return _type_mark ; }

protected:
// Parse tree nodes (owned)
    Array    *_signal_list ; // Array of VhdlName*
    VhdlName *_type_mark ;
} ; // class VhdlGuardedSignalSpec

/* -------------------------------------------------------------- */

class VFC_DLL_PORT VhdlEntitySpec : public VhdlSpecification
{
public:
    VhdlEntitySpec(Array *entity_name_list, unsigned entity_class) ;

    // Copy tree node
    VhdlEntitySpec(const VhdlEntitySpec &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEntitySpec(SaveRestore &save_restore);

    virtual ~VhdlEntitySpec() ;

private:
    // Prevent compiler from defining the following
    VhdlEntitySpec() ;                                  // Purposely leave unimplemented
    VhdlEntitySpec(const VhdlEntitySpec &) ;            // Purposely leave unimplemented
    VhdlEntitySpec& operator=(const VhdlEntitySpec &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLENTITYSPEC; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual void                ResolveSpecs(VhdlScope *scope, VhdlIdDef *attr, VhdlExpression *attr_value) ;
    virtual void                Validate(VhdlScope *s, VhdlIdDef *attr, VhdlExpression *attr_value) ; // VIPER #3645 : check whether used identifiers are already declared or not

    // Calculate the conformance signature of this specification
    virtual unsigned long       CalculateSignature() const ;

    // Copy entity specification.
    virtual VhdlSpecification * CopySpecification(VhdlMapForCopy &old2new, unsigned exclude_scope ) const ;
    // Internal routine to copy attribute specification
    virtual void                SetEntityIdsForCopy(VhdlMapForCopy &old2new, VhdlIdDef *attr_id, VhdlExpression *attr_value) ; // Set copied back pointers to entity specification, update attribute (back-pointers) on them

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Deletes 'old_node', and puts new name in its place
    virtual unsigned            InsertBeforeName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name before 'target_node' in the array of names .
    virtual unsigned            InsertAfterName(const VhdlName *target_node, VhdlName *new_node) ; // Insert new name after 'target_node' in the array of names .

    // Accessor methods
    Array *                     GetEntityNameList() const       { return _entity_name_list ; }
    unsigned                    GetEntityClass() const          { return _entity_class ; }
    virtual Set *               GetAllIds() const               { return _all_ids ; }

    // Elaboration
    virtual void                SetAttribute(VhdlIdDef *attr, VhdlValue *value) ;

protected:
// Parse tree nodes (owned)
    Array       *_entity_name_list ; // Array of VhdlName*
    unsigned     _entity_class ;
// Created at analysis time (elements of Set are not owned)
    Set         *_all_ids ; // Set of VhdlIdDef*.  All identifiers in entity_name_list
} ; // class VhdlEntitySpec

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_SPECIFICATION_H_

