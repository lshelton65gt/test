/*
 *
 * [ File Version : 1.9 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_COPY_H_
#define _VERIFIC_VHDL_COPY_H_
#include "VerificSystem.h"

#include "LineFile.h"       // For linefile_type
#include "Map.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class VhdlIdDef ;
class VhdlScope ;
class VhdlSpecification ;
class Array ;
class VhdlPrimaryUnit ;
class VhdlBindingIndication ;

/* -------------------------------------------------------------- */

/*
   VhdlMapForCopy is used in parse tree node copy routines. It is
   used to contain the information of VhdlIdDef vs. copied VhdlIdDef,
   VhdlScope vs. copied VhdlScope and VhdlSpecification vs. copied
   VhdlSpecification. This information is required for back pointer
   setting.
*/
class VFC_DLL_PORT VhdlMapForCopy : private Map
{
public:
    explicit VhdlMapForCopy(hash_type type = POINTER_HASH, unsigned init_bucket_size = 2) ;

    // Note: Destructor of base class Map is not virtual, do not delete a VhdlMapForCopy object by Map *
    ~VhdlMapForCopy() { _old_entity_id = 0 ; _new_entity_id = 0 ; }

private:
    // Prevent compiler from defining the following
    VhdlMapForCopy(const VhdlMapForCopy &) ;            // Purposely leave unimplemented
    VhdlMapForCopy& operator=(const VhdlMapForCopy &) ; // Purposely leave unimplemented

public:
    INSERT_MM_HOOKS

    // Store VhdlIdDef vs. copied VhdlIdDef for future references
    void SetCopiedId(const VhdlIdDef *old_id, const VhdlIdDef *new_id) ;

    // Get copied VhdlIdDef for argument specific VhdlIdDef
    VhdlIdDef *GetCopiedId(VhdlIdDef *old_id) const ;

    // Store VhdlScope vs. copied VhdlScope
    void SetCopiedScope(const VhdlScope *old_scope, const VhdlScope *new_scope) ;
    // Get copied scope for argument specific scope
    VhdlScope *GetCopiedScope(VhdlScope *old_scope) const ;

    // Store specification to resolve cross link of subprogram specification
    void SetCopiedSpecification(const VhdlSpecification *old_spec, const VhdlSpecification *new_spec) ;
    // Get copied specification
    VhdlSpecification *GetCopiedSpecification(VhdlSpecification *old_spec) const ;

    // Store binding to resolve primary binding backpointers from instantiation labels.
    void SetCopiedBinding(const VhdlBindingIndication *old_binding, const VhdlBindingIndication *new_binding) ;
    // Get copied binding
    VhdlBindingIndication *GetCopiedBinding(VhdlBindingIndication *old_binding) const ;

    // The following generic utility routines are used to copy array
    // of specific type i.e expression array, statement array etc.
    Array *CopyDeclarationArray(const Array *old_array) ;
    Array *CopyConfigurationItemArray(const Array *config_array) ;
    Array *CopyDiscreteRangeArray(const Array *range_array) ;
    Array *CopyIdDefArray(const Array *id_def_array) ;
    Array *CopyNameArray(const Array *name_array) ;
    Array *CopyStatementArray(const Array *stmt_array) ;
    Array *CopyExpressionArray(const Array *expr_list) ;

    // Copy the predefined operators of the argument specific scope
    // and maintain that association old-operator->copied-operator
    void CopyPredefinedOperators(const VhdlScope *scope) ;

    // For each iddef of argument specific array, the function
    // find the copied iddef and put that in an array to be returned
    Array *PopulateIdDefArray(const Array *iddef_list) const ;
    void MoveAssociations(VhdlMapForCopy &to_map) const ;
    // Make association list of old_entity's iddefs vs new_entity's iddefs
    void MakeAssociations(const VhdlPrimaryUnit *old_entity, const VhdlPrimaryUnit *new_entity, unsigned recurse=0) ;
    void MakeAssociationsInternal(const VhdlScope *old_scope, const VhdlScope *new_scope, unsigned recurse) ;
    void RemoveId(const VhdlIdDef *key)  { (void) Map::Remove(key) ; }

    void DonotSwitchIdInFormal()         { _not_switch_formal = 1; }
    unsigned IsDonotSwitchIdInFormal() const  { return _not_switch_formal ; }
    void ResetDonotSwitchIdInFormal()    { _not_switch_formal = 0 ; }

    void SetForLibraryCopy()             { _for_lib_copy = 1 ; }
    unsigned IsForLibraryCopy() const    { return _for_lib_copy ; }

    // Set/get routines of original entity id and copied entity id:
    // While copying architecture this VhdlMapForCopy does not store
    // original entity identifier and its copied version to avoid
    // changing recursive instantiations. But we need those ids if
    // prefix of attribute is entity id :
    void SetOldEntityId(VhdlIdDef *id)   { _old_entity_id = id ; }
    void SetNewEntityId(VhdlIdDef *id)   { _new_entity_id = id ; }
    VhdlIdDef *GetCopiedEntityId(const VhdlIdDef *old_id) const  ;
private :
    unsigned _not_switch_formal:1 ; // VIPER #4552 : Flag to indicate donot switch formal identifiers
    unsigned _for_lib_copy:1 ; // VIPER #5099 : Flag to indicate that whole library is copied
    VhdlIdDef *_old_entity_id ; // For entity copy, keep track of original entity id
    VhdlIdDef *_new_entity_id ; // For entity copy, keep track of copied entity id
} ; // class VhdlMapForCopy

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_COPY_H_

