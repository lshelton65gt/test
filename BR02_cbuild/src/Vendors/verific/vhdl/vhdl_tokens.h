/*
 *
 * [ File Version : 1.35 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_TOKENS_H_
#define _VERIFIC_VHDL_TOKENS_H_

// Don't declare the union in the Verific namespace, since
// lex and yacc cannot find each other's lval any more.
// But since we (might) use Verific fields from Verific namespace,
// we DO need to 'use' Verific namespace here :

#include "VerificSystem.h"

/* NOTE : This file was once created by yacc (y.tab.h), to get
 * tokens defined for the system and to expose
 * YYSTYPE to verilog.l ( for yylval)
 * Now, it's manually modified for the purpose of
 * avoiding exposure to all classes defined in the
 * yacc parse tree nodes (so that we don't need to
 * define them all the time for all files that include
 * this.
 * If there is a change in tokens (in verilog.y) or
 * a change in lex->yacc token passing, this file
 * NEEDS TO BE UPDATED MANUALLY AGAIN !!
*/

// Forward declaration of Verific tree node classes (in verific name space)
// These classes are used in the union between bison/flex (defined in vhdl_yacc.h)
#ifdef VERIFIC_NAMESPACE
namespace Verific { // Start definitions in Verific namespace
#endif

class Array ;
class VhdlScope ;
class VhdlDesignUnit ;
class VhdlDeclaration ;
class VhdlStatement ;
class VhdlDiscreteRange ;
class VhdlSubtypeIndication ;
class VhdlExpression ;
class VhdlName ;
class VhdlDesignator ;
class VhdlIdRef ;
class VhdlIdDef ;
class VhdlTypeDef ;
class VhdlSignature ;
class VhdlElementDecl ;
class VhdlPhysicalUnitDecl ;
class VhdlConfigurationItem ;
class VhdlBlockConfiguration ;
class VhdlComponentConfiguration  ;
class VhdlBindingIndication ;
class VhdlSpecification ;
class VhdlInterfaceDecl ;
class VhdlIterScheme ;
class VhdlOptions ;
class VhdlDelayMechanism ;
class VhdlBlockGenerics ;
class VhdlBlockPorts ;
class VhdlCaseStatementAlternative ;
class VhdlFileOpenInfo ;
class VhdlEntityClassEntry ;
class VhdlTreeNode ;

#ifdef VERIFIC_NAMESPACE
} // End definitions in Verific namespace
#endif

// Include the tokens created by yacc/bison. Also the union is in there.
#include "vhdl_yacc.h"

/*********************** So far YACC generated ***************************/

/* These added for 'environment' purposes (used to check VHDL semantics in analysis) */
// Start at a number above any yacc token numbers,
// but within 10 bits, so that, if needed, we can still store these tokens in a restricted unsigned field.
#define VHDL_SIGUPDATE 887
#define VHDL_VARUPDATE 888
#define VHDL_READ 889
#define VHDL_ACCESS_SUBTYPE 890 // VIPER #2893 : To produce error for illegal use of incomplete type

/* These added for operator convenience */
#define VHDL_INCR 900
#define VHDL_DECR 901
#define VHDL_REDAND  902
#define VHDL_REDNAND 903
#define VHDL_REDOR   904
#define VHDL_REDNOR  905
#define VHDL_REDXOR  906
#define VHDL_REDXNOR 907

/* This was manually added to support pragmas */
#define VHDL_UMINUS 950
#define VHDL_FEEDTHROUGH 951
#define VHDL_WIRED_THREE_STATE 952
#define VHDL_PULLUP 953
#define VHDL_PULLDOWN 954
#define VHDL_TRSTMEM 955
#define VHDL_buf 956
#define VHDL_builtin_operator 957
#define VHDL_rising_edge 958
#define VHDL_falling_edge 959
#define VHDL_IGNORE_SUBPROGRAM 960

/* This was manually added for named predefined functions/procedures */
#define VHDL_deallocate 980   /* procedure for access types */
#define VHDL_file_open 981    /* procedure for file types */
#define VHDL_file_close 982   /* procedure for file types */
#define VHDL_read 983         /* procedure for textio */
#define VHDL_write 984        /* procedure for textio */
#define VHDL_endfile 985      /* procedure for file types */
#define VHDL_now 986          /* function in standard.vhd */
#define VHDL_readline 987     /* procedure for file types (textio function) */
#define VHDL_writeline 988    /* procedure for file types (textio function) */
#define VHDL_hread 989         /* procedure for textio */
#define VHDL_hwrite 990        /* procedure for textio */
#define VHDL_oread 991         /* procedure for textio */
#define VHDL_owrite 992        /* procedure for textio */
#define VHDL_flush 993         /* procedure for textio */
#define VHDL_sread 994         /* procedure for 2008 std.textio */

/* This was manually added to define pre-defined attributes. No bit-with restriction on these */

#define VHDL_ATT_base  1001
#define VHDL_ATT_left   1002
#define VHDL_ATT_right   1003
#define VHDL_ATT_high   1004
#define VHDL_ATT_low   1005
#define VHDL_ATT_ascending   1006
#define VHDL_ATT_image   1007
#define VHDL_ATT_value   1008
#define VHDL_ATT_pos   1010
#define VHDL_ATT_val   1011
#define VHDL_ATT_succ   1012
#define VHDL_ATT_pred   1013
#define VHDL_ATT_leftof   1014
#define VHDL_ATT_rightof   1015
#define VHDL_ATT_range   1016
#define VHDL_ATT_reverse_range   1017
#define VHDL_ATT_length   1018
#define VHDL_ATT_delayed   1019
#define VHDL_ATT_stable   1020
#define VHDL_ATT_quiet   1021
#define VHDL_ATT_transaction   1022
#define VHDL_ATT_event   1023
#define VHDL_ATT_active   1024
#define VHDL_ATT_last_event   1025
#define VHDL_ATT_last_active   1026
#define VHDL_ATT_last_value   1027
#define VHDL_ATT_driving   1028
#define VHDL_ATT_driving_value   1029
#define VHDL_ATT_simple_name   1030
#define VHDL_ATT_instance_name   1031
#define VHDL_ATT_path_name   1032
#define VHDL_ATT_structure  1033    // '87 LRM Block attribute (no longer supported in '93 LRM)
#define VHDL_ATT_behavior   1034    // '87 LRM Block attribute (no longer supported in '93 LRM)
#define VHDL_ATT_element   1035     // VIPER #7502: vhdl_2008: new predefined attribute
#define VHDL_ATT_subtype   1036     // VIPER #7522: vhdl_2008: new predefined attribute
#define VHDL_CONVERSION_HANDLING   1037 // 6896: verilog to vhdl package conversion

// Viper #8006: Vhdl 2008 - math-real package : functions annotated with foreign attribute
#define VHDL_math_ceil         1102
#define VHDL_math_floor        1103
#define VHDL_math_mod          1106
#define VHDL_math_sqrt         1109
#define VHDL_math_cbrt         1110
#define VHDL_math_pow          1111
#define VHDL_math_exp          1112
#define VHDL_math_log          1113
#define VHDL_math_log2         1114
#define VHDL_math_log10        1115
#define VHDL_math_sin          1116
#define VHDL_math_cos          1117
#define VHDL_math_tan          1118
#define VHDL_math_asin         1119
#define VHDL_math_acos         1120
#define VHDL_math_atan         1121
#define VHDL_math_atan2        1122
#define VHDL_math_sinh         1123
#define VHDL_math_cosh         1124
#define VHDL_math_tanh         1125
#define VHDL_math_asinh        1126
#define VHDL_math_acosh        1127
#define VHDL_math_atanh        1128

#endif // #ifndef _VERIFIC_VHDL_TOKENS_H_

