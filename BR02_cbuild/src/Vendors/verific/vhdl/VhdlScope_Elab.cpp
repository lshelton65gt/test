/*
 *
 * [ File Version : 1.37 - 2014/03/18 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlScope.h" // Compile flags are defined within here

#include "Array.h"
#include "Map.h"
#include "Set.h"

#include "vhdl_file.h" // VIPER #7125: User stack limit

#include "VhdlIdDef.h"
#include "VhdlUnits.h"

#include "VhdlValue_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**************************************************************/
// Elaboration : elaborate all packages that this scope depends on
/**************************************************************/

void
VhdlScope::ElaborateDependencies() const
{
    // Elaborate all packages that this unit depends on. LRM 10.2.
    // These guys are stored in the '_using' array.
    if (!_using) return ;

    SetIter si ;
    VhdlScope *other_scope ;
    VhdlIdDef *this_unit = GetContainingDesignUnit() ;
    VhdlIdDef *other_unit ;
    FOREACH_SET_ITEM(_using, si, &other_scope) {
        other_unit = other_scope->GetContainingDesignUnit() ;
        if (!other_unit) continue ;
        if (other_unit == this_unit) continue ; // Don't (re)elaborate myself.
        if (other_unit->IsPackage()) {
            VhdlPrimaryUnit *package = other_unit->GetPrimaryUnit() ;
            if (VhdlNode::IsStaticElab()) {
                if (package) (void) package->StaticElaborate(0,0,0,0,0) ;
            } else {
            }
        }
    }
}

/**************************************************************/
// Elaboration : push/pop declared identifier constraints/values on a static stack
/**************************************************************/

Array *VhdlScope::_elaboration_stack = 0 ;

unsigned VhdlScope::PushStack()
{
    // Push this scope (identifier values/constraints) on a stack
    if (!_elaboration_stack) _elaboration_stack = new Array() ;

    // Error out if we (threaten to enter) an infinite loop :
    // VIPER #7125: Honour user stack limit, if set by user:
    // Also changed the default limit to 900 (it was 1000 before).
    // Default value is set while adding the runtime variable. If value 0
    // is set by user, no recursion will be supported.
    unsigned user_stack_limit = vhdl_file::GetMaxStackDepth() ;
    //unsigned stack_limit = (user_stack_limit) ? user_stack_limit : 900 ;
    if (_elaboration_stack->Size() > user_stack_limit) {
        if (_owner_id) _owner_id->Error("stack overflow on recursion via %s, limit of %d has exceeded", _owner_id->Name(), user_stack_limit) ;
        return 0 ;
    }

    // Create arrays for values and constraints.
    Array *values = new Array((_this_scope) ? _this_scope->Size()+1 : 1) ;
    _elaboration_stack->InsertLast(values) ;
    Array *constraints = new Array((_this_scope) ? _this_scope->Size()+1 :1) ;
    _elaboration_stack->InsertLast(constraints) ;
    // Create array for actual iddefs for subprogram/package type generics
    Array *actual_ids = new Array(2) ;
    _elaboration_stack->InsertLast(actual_ids) ;

    // Save owner :
    if (_owner_id) values->InsertLast(_owner_id->TakeValue()) ; // Move value from id into stack (id->value now becomes 0).
    if (_owner_id) constraints->InsertLast(_owner_id->TakeConstraint()) ; // Move constraint from id into stack (id->constraint now becomes 0).
    // And save all locally declared identifier values/constraints :
    VhdlIdDef *id ;
    MapIter mi ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (id == _owner_id) continue ;
        values->InsertLast(id->TakeValue()) ; // Move value from id into stack (id->value now becomes 0).
        constraints->InsertLast(id->TakeConstraint()) ; // Move constraint from id into stack (id->constraint now becomes 0).
        // Move actual id for type/subprogram/package generic and package instance id into stack
        if (id->IsGenericType()) {
            actual_ids->InsertLast(id->TakeActualId()) ;
        } else if (id->IsGeneric() && id->IsSubprogram()) {
            actual_ids->InsertLast(id->TakeActualId()) ;
        } else if (id->IsGeneric() && id->IsPackage()) {
            VhdlIdDef *actual_id = id->TakeElaboratedPackageId() ;
            actual_ids->InsertLast(actual_id) ;
        } else if (id->IsPackageInst()) {
            VhdlIdDef *actual_id = id->TakeElaboratedPackageId() ;
            actual_ids->InsertLast(actual_id) ;
        }
    }
    return 1 ;
}
unsigned VhdlScope::PushStack(Map* scope_stack)
{
    VERIFIC_ASSERT(scope_stack) ;

    if (scope_stack->GetValue(this)) return 1 ;

    Array* elaboration_stack = new Array() ;
    (void) scope_stack->Insert(this, elaboration_stack) ;
    // Push this scope (identifier values/constraints) on a stack
    // Error out if we (threaten to enter) an infinite loop :
    // VIPER #7125: Honour user stack limit, if set by user:
    // Also changed the default limit to 900 (it was 1000 before).
    // Default value is set while adding the runtime variable. If value 0
    // is set by user, no recursion will be supported.
    // Create arrays for values and constraints.
    VhdlDesignUnit* d_unit = _owner_id ? _owner_id->GetDesignUnit() : 0 ;
    if (d_unit) {
        Array* elaboration_flags = new Array(2) ;
        elaboration_flags->InsertLast((void*)((unsigned long)(d_unit->IsStaticElaborated()))) ;
        elaboration_flags->InsertLast((void*)((unsigned long)(d_unit->IsElaborated()))) ;
        d_unit->ResetElaborated() ;
        d_unit->ResetStaticElaborated() ;
        elaboration_stack->InsertLast(elaboration_flags) ;
    }
    Array *values = new Array((_this_scope) ? _this_scope->Size()+1 : 1) ;
    elaboration_stack->InsertLast(values) ;
    Array *constraints = new Array((_this_scope) ? _this_scope->Size()+1 :1) ;
    elaboration_stack->InsertLast(constraints) ;
    // Create array for actual iddefs for subprogram/package type generics
    Array *actual_ids = new Array(2) ;
    elaboration_stack->InsertLast(actual_ids) ;

    // Save owner :
    if (_owner_id) values->InsertLast(_owner_id->TakeValue()) ; // Move value from id into stack (id->value now becomes 0).
    if (_owner_id) constraints->InsertLast(_owner_id->TakeConstraint()) ; // Move constraint from id into stack (id->constraint now becomes 0).
    // And save all locally declared identifier values/constraints :
    VhdlIdDef *id ;
    MapIter mi ;
    FOREACH_MAP_ITEM(_this_scope, mi, 0, &id) {
        if (id == _owner_id) continue ;
        values->InsertLast(id->TakeValue()) ; // Move value from id into stack (id->value now becomes 0).
        constraints->InsertLast(id->TakeConstraint()) ; // Move constraint from id into stack (id->constraint now becomes 0).
        // Move actual id for type/subprogram/package generic and package instance id into stack
        if (id->IsGenericType()) {
            actual_ids->InsertLast(id->TakeActualId()) ;
        } else if (id->IsGeneric() && id->IsSubprogram()) {
            actual_ids->InsertLast(id->TakeActualId()) ;
        } else if (id->IsGeneric() && id->IsPackage()) {
            VhdlIdDef *actual_id = id->TakeElaboratedPackageId() ;
            actual_ids->InsertLast(actual_id) ;
        } else if (id->IsPackageInst()) {
            VhdlIdDef *actual_id = id->TakeElaboratedPackageId() ;
            actual_ids->InsertLast(actual_id) ;
        }
    }
    return 1 ;
}

/* static */ void
VhdlScope::SetConstraintRecursively(VhdlIdDef *id, VhdlConstraint *constraint)
{
    // Viper #6067: Also set the element_id_constraints for records
    VERIFIC_ASSERT(id && (!id->IsLocallyStaticType() || !id->Constraint())) ;

    id->SetConstraint(constraint) ;
    if (!id->IsRecordType()) return ;

    MapIter mi ;
    VhdlIdDef *element_id ;
    FOREACH_MAP_ITEM(id->GetTypeDefList(), mi, 0, &element_id) {
        if (!element_id) continue ;

        unsigned is_static = element_id->IsLocallyStaticType() || (VhdlNode::IsStaticElab() && element_id->IsGloballyStaticType()) ; // Viper #6059
        if (is_static) continue ;

        VhdlConstraint *elem_constraint = constraint ? constraint->ElementConstraintFor(element_id) : 0 ;
        if (elem_constraint) elem_constraint = elem_constraint->Copy() ;
        SetConstraintRecursively(element_id, elem_constraint) ;
    }
}

unsigned VhdlScope::PopStack()
{
    VERIFIC_ASSERT(_elaboration_stack) ;
    // Pop actual_ids also (needed for subprogram/type/package generic
    Array *actual_ids = (Array*)_elaboration_stack->RemoveLast() ;
    // Pop the last scope off the stack (re-install identifier values/constraints)
    Array *constraints = (Array*)_elaboration_stack->RemoveLast() ;
    Array *values = (Array*)_elaboration_stack->RemoveLast() ;

    // And restore all locally declared identifier values/constraints in opposite order :
    VhdlIdDef *id ;
    MapIter mi ;
    FOREACH_MAP_ITEM_BACK(_this_scope, mi, 0, &id) {
        if (!id || (id == _owner_id)) continue ;
        VhdlConstraint *constraint = (VhdlConstraint*)constraints->RemoveLast() ;
        // Viper #3415 : Locally static type / constraint will never change no matter how many time
        // you compute in whatever context, So do not deprive the elaborated identifier of its constraint
        unsigned is_static = id->IsLocallyStaticType() || (VhdlNode::IsStaticElab() && id->IsGloballyStaticType()) ; // Viper #6059
        if (!id->Constraint() || !is_static) {
            // Viper #6067: Also set the element_id_constraints for records
            // id->SetConstraint(constraint) ;
            SetConstraintRecursively(id, constraint) ;
        } else {
            delete constraint ;
        }

        // Viper #6727: For enumeration identifiers, values will stay as constant, always
        VhdlValue *id_value = (VhdlValue*)values->RemoveLast() ;
        if (id->IsEnumerationLiteral()) {
            delete id_value ;
        } else {
            id->SetValue(id_value) ;
        }
        // Pop actual identifier for type/subprogram/package generic and
        // elaborated package id for package instance
        if (id->IsGenericType()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetActualId(actual_id, 0) ;
        } else if (id->IsGeneric() && id->IsSubprogram()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetActualId(actual_id, 0) ;
        } else if (id->IsGeneric() && id->IsPackage()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetElaboratedPackageId(actual_id) ;
        } else if (id->IsPackageInst()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetElaboratedPackageId(actual_id) ;
        }
    }
    // Restore owner :
    if (_owner_id) _owner_id->SetConstraint((VhdlConstraint*)constraints->RemoveLast()) ;
    if (_owner_id) _owner_id->SetValue((VhdlValue*)values->RemoveLast()) ;

    // Check that we restored the right scope : values and constraint arrays must be empty now :
    VERIFIC_ASSERT(values->Size()==0) ;
    VERIFIC_ASSERT(constraints->Size()==0) ;
    // Delete the popped arrays
    delete values ;
    delete constraints ;
    delete actual_ids ;

    // See if we can delete the stack
    if (_elaboration_stack->Size()==0) {
        delete _elaboration_stack ;
        _elaboration_stack = 0 ;
    }
    return 1 ;
}

unsigned VhdlScope::PopStack(Map* scope_stack)
{
    VERIFIC_ASSERT(scope_stack) ;
    Array* elaboration_stack = (Array*)scope_stack->GetValue(this) ;
    if (!elaboration_stack) return 0 ;
    // Pop actual_ids also (needed for subprogram/type/package generic
    Array *actual_ids = (Array*)elaboration_stack->RemoveLast() ;
    // Pop the last scope off the stack (re-install identifier values/constraints)
    Array *constraints = (Array*)elaboration_stack->RemoveLast() ;
    Array *values = (Array*)elaboration_stack->RemoveLast() ;

    VhdlDesignUnit* d_unit = _owner_id ? _owner_id->GetDesignUnit() : 0 ;

    Array *elaboration_flags = 0 ;
    if (d_unit) {
        elaboration_flags = (Array*)elaboration_stack->RemoveLast() ;
        unsigned long is_static_elaborated = (unsigned long)elaboration_flags->At(0) ;
        unsigned long is_elaborated = (unsigned long)elaboration_flags->At(1) ;

        if (is_static_elaborated) {
            d_unit->SetStaticElaborated() ;
        } else {
            d_unit->ResetStaticElaborated() ;
        }

        if (is_elaborated) {
            d_unit->SetElaborated() ;
        } else {
            d_unit->ResetElaborated() ;
        }
    }

    // And restore all locally declared identifier values/constraints in opposite order :
    VhdlIdDef *id ;
    MapIter mi ;
    FOREACH_MAP_ITEM_BACK(_this_scope, mi, 0, &id) {
        if (!id || (id == _owner_id)) continue ;
        VhdlConstraint *constraint = (VhdlConstraint*)constraints->RemoveLast() ;
        // Viper #3415 : Locally static type / constraint will never change no matter how many time
        // you compute in whatever context, So do not deprive the elaborated identifier of its constraint

        unsigned is_static = id->IsLocallyStaticType() || (VhdlNode::IsStaticElab() && id->IsGloballyStaticType()) ; // Viper #6059
        if (is_static) {
            id->SetConstraint(constraint) ;
        } else {
            SetConstraintRecursively(id, constraint) ;
        }
        /*
        if (!id->Constraint() || !is_static) {
            // Viper #6067: Also set the element_id_constraints for records
            // id->SetConstraint(constraint) ;
            SetConstraintRecursively(id, constraint) ;
        } else {
            delete constraint ;
        }
        */

        // Viper #6727: For enumeration identifiers, values will stay as constant, always
        VhdlValue *id_value = (VhdlValue*)values->RemoveLast() ;
        id->SetValue(id_value) ;
        /*if (id->IsEnumerationLiteral()) {
            delete id_value ;
        } else {
            id->SetValue(id_value) ;
        }*/
        // Pop actual identifier for type/subprogram/package generic and
        // elaborated package id for package instance
        if (id->IsGenericType()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetActualId(actual_id, 0) ;
        } else if (id->IsGeneric() && id->IsSubprogram()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetActualId(actual_id, 0) ;
        } else if (id->IsGeneric() && id->IsPackage()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetElaboratedPackageId(actual_id) ;
        } else if (id->IsPackageInst()) {
            VhdlIdDef *actual_id = (VhdlIdDef*)actual_ids->RemoveLast() ;
            id->SetElaboratedPackageId(actual_id) ;
        }
    }
    // Restore owner :
    if (_owner_id) _owner_id->SetConstraint((VhdlConstraint*)constraints->RemoveLast()) ;
    if (_owner_id) _owner_id->SetValue((VhdlValue*)values->RemoveLast()) ;

    // Check that we restored the right scope : values and constraint arrays must be empty now :
    VERIFIC_ASSERT(values->Size()==0) ;
    VERIFIC_ASSERT(constraints->Size()==0) ;
    // Delete the popped arrays
    delete values ;
    delete constraints ;
    delete actual_ids ;
    delete elaboration_flags ;
    (void) scope_stack->Remove(this) ;
    delete elaboration_stack ;

    return 1 ;
}

/*static*/ void
VhdlScope::ResetStack()
{
    if (!_elaboration_stack) return ;

    while (_elaboration_stack->Size()) {
        Array *actual_ids = (Array*)_elaboration_stack->RemoveLast() ;
        Array *constraints = (Array*)_elaboration_stack->RemoveLast() ;
        Array *values = (Array*)_elaboration_stack->RemoveLast() ;
        delete actual_ids ;
        delete constraints ;
        delete values ;
    }

    delete _elaboration_stack ;
    _elaboration_stack = 0 ;
}

void
VhdlScope::PushStackDependencies(Map* scope_stack) const
{
    // Elaborate all packages that this unit depends on. LRM 10.2.
    // These guys are stored in the '_using' array.
    if (!_using) return ;

    SetIter si ;
    VhdlScope *other_scope ;
    VhdlIdDef *this_unit = GetContainingDesignUnit() ;
    FOREACH_SET_ITEM(_using, si, &other_scope) {
        if (!other_scope || (other_scope == this)) continue ;
        VhdlIdDef* unit = other_scope->GetContainingDesignUnit() ;
        if (!unit || unit==this_unit || _extended_decl_area==other_scope) continue ;
        VhdlDesignUnit* punit = unit->GetDesignUnit() ;
        if (!punit) continue ;
        (void) other_scope->PushStack(scope_stack) ;
    }
}

void
VhdlScope::PopStackDependencies(Map* scope_stack) const
{
    // Elaborate all packages that this unit depends on. LRM 10.2.
    // These guys are stored in the '_using' array.
    if (!_using) return ;

    SetIter si ;
    VhdlScope *other_scope ;
    VhdlIdDef *this_unit = GetContainingDesignUnit() ;
    FOREACH_SET_ITEM(_using, si, &other_scope) {
        if (!other_scope || (other_scope == this)) continue ;
        VhdlIdDef* unit = other_scope->GetContainingDesignUnit() ;
        VhdlDesignUnit* punit = unit ? unit->GetDesignUnit() : 0 ;
        if (!punit) continue ;
        if (unit==this_unit || _extended_decl_area==other_scope) continue ;
        (void) other_scope->PopStack(scope_stack) ;
    }
}

