/*
 *
 * [ File Version : 1.119 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_MISC_H_
#define _VERIFIC_VHDL_MISC_H_

#include <stdio.h> // for FILE
#include "VhdlTreeNode.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class Array ;
class Map ;

class VhdlName ;
class VhdlLiteral ;
class VhdlIdRef ;
class VhdlExpression ;
class VhdlDiscreteRange ;
class VhdlOperatorId ;
class VhdlBlockConfiguration;
class VhdlConstraint;

class VhdlDataFlow ;
class VhdlValue ;
class VhdlMapForCopy ;

class VhdlVarUsageInfo ;

/* -------------------------------------------------------------- */

/*
  Class used in alias names and entity designator name
*/
class VFC_DLL_PORT VhdlSignature : public VhdlTreeNode
{
public:
    VhdlSignature(Array *type_mark_list, VhdlName *return_type) ;

    // Copy tree node
    VhdlSignature(const VhdlSignature &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSignature(SaveRestore &save_restore);

    virtual ~VhdlSignature() ;

private:
    // Prevent compiler from defining the following
    VhdlSignature() ;                                 // Purposely leave unimplemented
    VhdlSignature(const VhdlSignature &) ;            // Purposely leave unimplemented
    VhdlSignature& operator=(const VhdlSignature &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID   GetClassId() const { return ID_VHDLSIGNATURE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void            Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void            Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void            PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlSignature
    virtual unsigned long   CalculateSignature() const ;

    // Copy signature
    VhdlSignature *         Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned        ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new name in its place.

    // Insert new name before/after 'target_node' in the list
    // of names. If 'target_node' is null, nothing is inserted.
    unsigned                InsertBeforeName(const VhdlName *target_node, VhdlName *new_node) ;
    unsigned                InsertAfterName(const VhdlName *target_node, VhdlName *new_node) ;

    void                    FillProfile(VhdlOperatorId &profile) ;

    // Accessor methdos
    Array *                 GetTypeMarklist() const         { return _type_mark_list ; }
    VhdlName *              GetReturnType() const           { return _return_type ; }

protected:
// Parse tree nodes (owned) :
    Array       *_type_mark_list ; // Array of VhdlName*
    VhdlName    *_return_type ;
} ; // class VhdlSignature

/* -------------------------------------------------------------- */

/*
  Class used in file declarations
*/
class VFC_DLL_PORT VhdlFileOpenInfo : public VhdlTreeNode
{
public:
    VhdlFileOpenInfo(VhdlExpression *file_open, unsigned open_mode, VhdlExpression *file_logical_name) ;

    // Copy tree node
    VhdlFileOpenInfo(const VhdlFileOpenInfo &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlFileOpenInfo(SaveRestore &save_restore);

    virtual ~VhdlFileOpenInfo() ;

private:
    // Prevent compiler from defining the following
    VhdlFileOpenInfo() ;                                    // Purposely leave unimplemented
    VhdlFileOpenInfo(const VhdlFileOpenInfo &) ;            // Purposely leave unimplemented
    VhdlFileOpenInfo& operator=(const VhdlFileOpenInfo &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID   GetClassId() const { return ID_VHDLFILEOPENINFO; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void            Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void            Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void            PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlFileOpenInfo
    virtual unsigned long   CalculateSignature() const ;

    // Copy file open information specific parse-tree.
    VhdlFileOpenInfo *      Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned        ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Accessor methods
    VhdlExpression *        GetFileOpen() const             { return _file_open ; }
    unsigned                GetOpenMode() const             { return _open_mode ; }
    VhdlExpression *        GetFileLogicalName() const      { return _file_logical_name ; }

    // Elaboration : Open the file :
    virtual FILE *          Elaborate(VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void            InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_file_open ; // VHDL'93 open mode (a 'open_kind' expression)
    unsigned        _open_mode ; // VHDL'87 open mode (VHDL_in, VHDL_out, VHDL_inout etc) token.
    VhdlExpression *_file_logical_name ;
} ; // class VhdlFileOpenInfo

/* -------------------------------------------------------------- */

/*
  Class used in component configuration and configuration specification
*/
class VFC_DLL_PORT VhdlBindingIndication : public VhdlTreeNode
{
public:
    VhdlBindingIndication(VhdlName *entity_aspect, Array *generic_map_aspect, Array *port_map_aspect) ;

    // Copy tree node
    VhdlBindingIndication(const VhdlBindingIndication &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBindingIndication(SaveRestore &save_restore);

    virtual ~VhdlBindingIndication() ;

private:
    // Prevent compiler from defining the following
    VhdlBindingIndication() ;                                         // Purposely leave unimplemented
    VhdlBindingIndication(const VhdlBindingIndication &) ;            // Purposely leave unimplemented
    VhdlBindingIndication& operator=(const VhdlBindingIndication &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID   GetClassId() const { return ID_VHDLBINDINGINDICATION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void            Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void            Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void            PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlBindingIndication
    virtual unsigned long   CalculateSignature() const ;

    // Copy binding indication.
    VhdlBindingIndication * Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned        ReplaceChildName(VhdlName *old_node, VhdlName *new_node) ; // Delete 'old_node', puts new entity aspect in its place.
    virtual unsigned        ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new port map/generic map aspect element in its place.

    // Insert new range before/after 'target_node' in the array-of-actuals for generic/port. If 'target_node' is null, nothing is inserted.
    unsigned                InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;
    unsigned                InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ;

    // Interface routines to fields of the binding :
    VhdlIdDef *             EntityAspect() const { return _unit ; } // Return the primary unit (entity or configuration) of the binding
    VhdlName *              GetEntityAspectNameNode() const     { return _entity_aspect ; }
    const char *            ArchitectureNameAspect() const ; // Return the (optional) architecture name in the entity aspect.
    Array *                 GetGenericBinding() const { return _generic_map_aspect ; }
    Array *                 GetPortBinding() const { return _port_map_aspect ; }

protected:
// Parse tree nodes (owned) :
    VhdlName    *_entity_aspect ;
    Array       *_generic_map_aspect ;  // (association list) Array of VhdlDiscreteRange*
    Array       *_port_map_aspect ;     // (association list) Array of VhdlDiscreteRange*
// back pointer set at analysis time
    VhdlIdDef   *_unit ;                // (resolved) binding primary unit (entity or configuration)
} ; // class VhdlBindingIndication

/* -------------------------------------------------------------- */

/*
  Base class for iteration schemes (while/for/if)
*/
class VFC_DLL_PORT VhdlIterScheme : public VhdlTreeNode
{
protected: // Abstract class
    VhdlIterScheme() ;

    // Copy tree node
    VhdlIterScheme(const VhdlIterScheme &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIterScheme(SaveRestore &save_restore);

public:
    virtual ~VhdlIterScheme() ;

private:
    // Prevent compiler from defining the following
    VhdlIterScheme(const VhdlIterScheme &) ;            // Purposely leave unimplemented
    VhdlIterScheme& operator=(const VhdlIterScheme &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    //virtual void                CheckIterType(VhdlIdDef const * /*type*/, VhdlTreeNode const * /*from*/) const { }
    virtual VhdlIdDef *           CheckIterType(VhdlDiscreteRange * /*gen_spec*/, VhdlTreeNode const * /*from*/) const { return 0 ; }

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    virtual unsigned            IsForScheme(void)   { return 0 ; }
    virtual unsigned            IsWhileScheme(void) { return 0 ; }
    virtual unsigned            IsIfScheme(void)    { return 0 ; }
    virtual unsigned            IsElsifScheme(void) { return 0 ; }
    virtual unsigned            IsElseScheme(void)  { return 0 ; }
    virtual unsigned            IsCaseScheme(void)  { return 0 ; }
    virtual VhdlIdDef *         GetAlternativeLabelId() const { return 0 ; }
    virtual VhdlDesignator *    GetAlternativeClosingLabel() const { return 0 ; } // VIPER #6666 & #6662
    virtual void                SetAlternativeClosingLabel(VhdlDesignator * /*label*/) { } // VIPER #6666 & #6662
    virtual VhdlDiscreteRange * GetRange() const    { return 0 ; } // Get range of for-scheme
    virtual VhdlIdDef *         GetIterId() const   { return 0 ; }

    virtual Array *             GetChoices() const  { return 0 ; } // Return choices for CaseItemScheme
    // Calculate the conformance signature of this VhdlIterScheme
    virtual unsigned long       CalculateSignature() const ;

    // Copy iteration scheme.
    virtual VhdlIterScheme *    CopyIterScheme(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/)                { return 0 ; } // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildId(VhdlIdDef * /*old_node*/, VhdlIdDef * /*new_node*/)                            { return 0 ; } // Delete 'old_node', puts new exit/next label identifier in its place.
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef * /*old_node*/, VhdlIdRef * /*new_node*/)                         { return 0 ; } // Delete 'old_node', puts new identifier reference in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange * /*old_node*/, VhdlDiscreteRange * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new iteration range in its place.

    // Static Elaborate.
    virtual Array *             StaticElaborate(VhdlDataFlow * /*df*/, VhdlBlockConfiguration * /*block_config*/, VhdlStatement * /*gen_stmt*/, Map * /*new_ids*/, VhdlIdDef * /*label*/) { return 0 ; } // unroll loops, select proper statement
    static VhdlBlockStatement * CreateBlockStatement(VhdlIdDef *stmt_label, const VhdlScope *upper_scope, const VhdlIdDef *iter_id, const Array *declarations, const Array *statements, VhdlMapForCopy &old2new, Map *new_ids, const VhdlTreeNode *from) ; // Create block statement for each iteration of generate loop or for true/false statements of if-generate.
    static VhdlConstantDecl *   CreateConstantDecl(const VhdlIdDef *iter_id, VhdlMapForCopy &old2new, VhdlScope *block_scope, const VhdlTreeNode *from) ; // Create constant declaration with loop index variable. The initial value of the constant will be the current value of the loop index variable.
    static VhdlBlockConfiguration * ProcessBlockStatement(VhdlBlockStatement *blk_stmt, VhdlValue *runner, const VhdlBlockConfiguration *block_config, VhdlMapForCopy &old2new, VhdlDataFlow *df) ; // elaborate the block statement created by generate unroll. Unroll loops, convert component instantiation.

    // VIPER #4437 : API to create name from block label and iteration value :
    static char *               CreateBlockName(const VhdlIdDef *stmt_label, const VhdlValue *iter_val, const VhdlScope *container, const Map *new_ids) ;

    // Elaboration
    virtual unsigned            Elaborate(VhdlIdDef * /*stmt_label*/, Array * /*declarations*/, Array * /*statements*/, VhdlBlockConfiguration * /*block_config*/, VhdlDataFlow * /*df*/) { return 0 ; }
    virtual VhdlIterScheme *    ElaborateChoices(Map & /*choice_values*/, Map & /*choice_positions*/, VhdlValue * /*case_expr*/, VhdlDataFlow * /*df*/, VhdlConstraint const * /*case_expr_constraint*/, Set& /*overlap_check*/, VhdlIdDef const * /*case_expr_id*/, Map *&  /*condition_id_value_map*/) { return 0 ; }
    static unsigned             ElaborateChoicesInternal(const Array *choices, Map &choice_values, Map &choice_positions, VhdlValue *case_expr, VhdlDataFlow *df, VhdlTreeNode const* from, VhdlConstraint const *case_expr_constraint, Set& overlap_check, VhdlIdDef const *case_expr_id, Map *& condition_id_value_map, unsigned is_matching_case, unsigned is_case_stmt, verific_uint64& extra_count, unsigned populate_dc_positions = 0) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() { return ; }
#endif

protected:
} ; // class VhdlIterScheme

/* -------------------------------------------------------------- */

/*
  Class used in loop statememt
*/
class VFC_DLL_PORT VhdlWhileScheme : public VhdlIterScheme
{
public:
    explicit VhdlWhileScheme(VhdlExpression *condition) ;

    // Copy tree node
    VhdlWhileScheme(const VhdlWhileScheme &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlWhileScheme(SaveRestore &save_restore);

    virtual ~VhdlWhileScheme() ;

private:
    // Prevent compiler from defining the following
    VhdlWhileScheme() ;                                   // Purposely leave unimplemented
    VhdlWhileScheme(const VhdlWhileScheme &) ;            // Purposely leave unimplemented
    VhdlWhileScheme& operator=(const VhdlWhileScheme &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLWHILESCHEME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsWhileScheme(void) { return 1 ; }

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlIterScheme
    virtual unsigned long       CalculateSignature() const ;

    // Copy while schame
    virtual VhdlIterScheme *    CopyIterScheme(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new exit/next label identifier in its place.

    // Accessor methods
    VhdlExpression *            GetCondition() const         { return _condition ; }
    VhdlIdDef *                 GetExitLabel() const         { return _exit_label ; }
    VhdlIdDef *                 GetNextLabel() const         { return _next_label ; }

    // Elaboration
    virtual unsigned            Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_condition ;
// Special purpose identifiers, created during analysis (owned). Used to let 'exit'/'next' statements find the iteration scheme they refer to.
    VhdlIdDef       *_exit_label ; // exit identifier label : Created in constructor
    VhdlIdDef       *_next_label ; // next identifier label : Created in constructor
} ; // class VhdlWhileScheme

/* -------------------------------------------------------------- */

/*
  Class used in generate/loop statement
*/
class VFC_DLL_PORT VhdlForScheme : public VhdlIterScheme
{
public:
    VhdlForScheme(VhdlIdRef *id, VhdlDiscreteRange *range, unsigned is_generate) ;

    // Copy tree node
    VhdlForScheme(const VhdlForScheme &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlForScheme(SaveRestore &save_restore);

    virtual ~VhdlForScheme() ;

private:
    // Prevent compiler from defining the following
    VhdlForScheme() ;                                 // Purposely leave unimplemented
    VhdlForScheme(const VhdlForScheme &) ;            // Purposely leave unimplemented
    VhdlForScheme& operator=(const VhdlForScheme &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLFORSCHEME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    virtual VhdlIdDef *         CheckIterType(VhdlDiscreteRange *gen_spec, VhdlTreeNode const *from) const ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsForScheme(void)   { return 1 ; }

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlIterScheme
    virtual unsigned long       CalculateSignature() const ;

    // Copy for iteration scheme
    virtual VhdlIterScheme *    CopyIterScheme(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildIdRef(VhdlIdRef *old_node, VhdlIdRef *new_node) ; // Delete 'old_node', puts new identifier reference in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new iteration range in its place.
    virtual unsigned            ReplaceChildId(VhdlIdDef *old_node, VhdlIdDef *new_node) ; // Delete 'old_node', puts new identifier in its place.

    // Static Elaboration.
    virtual Array *             StaticElaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config, VhdlStatement *gen_stmt, Map *new_ids, VhdlIdDef *label) ; // Unroll generate loop, create block statement for each iteration.

    // Accessor methods
    VhdlIdRef *                 GetId() const               { return _id ; }
    virtual VhdlDiscreteRange * GetRange() const            { return _range ; }
    virtual VhdlIdDef *         GetIterId() const           { return _iter_id ; }
    VhdlIdDef *                 GetExitLabel() const        { return _exit_label ; }
    VhdlIdDef *                 GetNextLabel() const        { return _next_label ; }

    // Elaboration
    virtual unsigned            Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlIdRef           *_id ;          // The iterator identifier name
    VhdlDiscreteRange   *_range ;
// The iterator constant identifier (owned)
    VhdlIdDef           *_iter_id ;     // iterator identifier : Created in constructor
// Special purpose identifiers, created during analysis (owned). Used to let 'exit'/'next' statements find the iteration scheme they refer to.
    VhdlIdDef           *_exit_label ;  // exit identifier label : Created in constructor
    VhdlIdDef           *_next_label ;  // next identifier label : Created in constructor
} ; // class VhdlForScheme

/* -------------------------------------------------------------- */

/*
  Class used in generate statement to represent if-scheme of if-elsif-else generate statement
  if [alternative_label :] condition  (VHDL-2008)
*/
class VFC_DLL_PORT VhdlIfScheme : public VhdlIterScheme
{
public:
    explicit VhdlIfScheme(VhdlExpression *condition) ;
    VhdlIfScheme(VhdlExpression *condition, VhdlIdRef *alternative_label) ;

    // Copy tree node
    VhdlIfScheme(const VhdlIfScheme &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlIfScheme(SaveRestore &save_restore);

    virtual ~VhdlIfScheme() ;

private:
    // Prevent compiler from defining the following
    VhdlIfScheme() ;                                // Purposely leave unimplemented
    VhdlIfScheme(const VhdlIfScheme &) ;            // Purposely leave unimplemented
    VhdlIfScheme& operator=(const VhdlIfScheme &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLIFSCHEME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsIfScheme(void)    { return 1 ; }

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlIterScheme
    virtual unsigned long       CalculateSignature() const ;
    virtual VhdlIdDef *         CheckIterType(VhdlDiscreteRange *gen_spec, VhdlTreeNode const *from) const ;

    // Copy if scheme
    virtual VhdlIterScheme *    CopyIterScheme(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Static Elaborate.
    virtual Array *             StaticElaborate(VhdlDataFlow *df, VhdlBlockConfiguration *block_config, VhdlStatement *gen_stmt, Map *new_ids, VhdlIdDef *label) ; // Select proper statement evaluating the condition of the if-scheme and create block statement with selected statements.

    // Accessor method
    VhdlExpression *            GetCondition() const          { return _condition ; }
    VhdlIdRef *                 GetAlternativeLabel() const   { return _alternative_label ; }
    virtual VhdlIdDef *         GetAlternativeLabelId() const { return _alternative_label_id ; }
    virtual VhdlDesignator *    GetAlternativeClosingLabel() const ; // VIPER #6666 & #6662
    virtual void                SetAlternativeClosingLabel(VhdlDesignator *label) ; // VIPER #6666 & #6662

    // Elaboration
    virtual unsigned            Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_condition ;
    VhdlIdRef       *_alternative_label ; // Parse tree for alternative label
    VhdlIdDef       *_alternative_label_id ; // Alternative label id
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    VhdlDesignator  *_alternative_closing_label ; // VIPER #6666 & #6662
#endif
} ; // class VhdlIfScheme

/* -------------------------------------------------------------- */

/*
   Class to represent elsif and else scheme of if-elsif-else generate statement (VHDL-2008)
   Represent
   1 . elsif [alternative_label :] condition
   2.  else [alternative_label :]
*/
class VFC_DLL_PORT VhdlElsifElseScheme : public VhdlIfScheme
{
public:
    VhdlElsifElseScheme(VhdlExpression *condition, VhdlIdRef *alternative_label) ;

    // Copy tree node
    VhdlElsifElseScheme(const VhdlElsifElseScheme &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlElsifElseScheme(SaveRestore &save_restore);

    virtual ~VhdlElsifElseScheme() ;

private:
    // Prevent compiler from defining the following
    VhdlElsifElseScheme() ;                                       // Purposely leave unimplemented
    VhdlElsifElseScheme(const VhdlElsifElseScheme &) ;            // Purposely leave unimplemented
    VhdlElsifElseScheme& operator=(const VhdlElsifElseScheme &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLELSIFELSESCHEME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsIfScheme(void)       { return 0 ; }
    virtual unsigned            IsElsifScheme(void)    { return _condition ? 1: 0 ; }
    virtual unsigned            IsElseScheme(void)     { return _condition ? 0: 1 ; }

    // Calculate the conformance signature of this VhdlIterScheme
    virtual unsigned long       CalculateSignature() const ;

    // Copy if scheme
    virtual VhdlIterScheme *    CopyIterScheme(VhdlMapForCopy &old2new) const ;

    // Elaboration
    //virtual void                Elaborate(VhdlIdDef *stmt_label, Array *declarations, Array *statements, VhdlBlockConfiguration *block_config, VhdlDataFlow *df) {}

protected:
// Parse tree nodes (owned) :
// Use elements of base class. Null _condition means else scheme
} ; // class VhdlElsifElseScheme

/* -------------------------------------------------------------- */

/*
   Class to represent case generate item scheme like
   when [ alternative_label :] choices =>
   This is in VHDL-2008
*/
class VFC_DLL_PORT VhdlCaseItemScheme : public VhdlIterScheme
{
public:
    VhdlCaseItemScheme(Array *choices, VhdlIdRef *alternative_label) ;

    // Copy tree node
    VhdlCaseItemScheme(const VhdlCaseItemScheme &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlCaseItemScheme(SaveRestore &save_restore);

    virtual ~VhdlCaseItemScheme() ;

private:
    // Prevent compiler from defining the following
    VhdlCaseItemScheme() ;                                      // Purposely leave unimplemented
    VhdlCaseItemScheme(const VhdlCaseItemScheme &) ;            // Purposely leave unimplemented
    VhdlCaseItemScheme& operator=(const VhdlCaseItemScheme &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCASEITEMSCHEME; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    virtual unsigned            IsCaseScheme(void)    { return 1 ; }

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlIterScheme
    virtual unsigned long       CalculateSignature() const ;
    virtual VhdlIdDef *         CheckIterType(VhdlDiscreteRange *gen_spec, VhdlTreeNode const *from) const ;

    // Copy if scheme
    virtual VhdlIterScheme *    CopyIterScheme(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Accessor method
    virtual Array *             GetChoices() const            { return _choices ; }
    VhdlIdRef *                 GetAlternativeLabel() const   { return _alternative_label ; }
    virtual VhdlIdDef *         GetAlternativeLabelId() const { return _alternative_label_id ; }
    virtual VhdlDesignator *    GetAlternativeClosingLabel() const ; // VIPER #6666 & #6662
    virtual void                SetAlternativeClosingLabel(VhdlDesignator *label) ; // VIPER #6666 & #6662

    // Elaboration
    virtual VhdlIterScheme *    ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, VhdlDataFlow *df, VhdlConstraint const *case_expr_constraint, Set& overlap_check, VhdlIdDef const *case_expr_id, Map *& condition_id_value_map) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array           *_choices ; // Array of VhdlDiscreteRange*   (choices can be a range or an expression or 'others')
    VhdlIdRef       *_alternative_label ; // Parse tree for alternative label
    VhdlIdDef       *_alternative_label_id ; // Alternative label id
#ifdef VHDL_PRESERVE_EXTRA_TOKENS
    VhdlDesignator  *_alternative_closing_label ; // VIPER #6666 & #6662
#endif
} ; // class VhdlCaseItemScheme

/* -------------------------------------------------------------- */
// Class obsolete now....?
class VFC_DLL_PORT VhdlDelayMechanism : public VhdlTreeNode
{
protected: // Abstract class
    VhdlDelayMechanism() ;

    // Copy tree node
    VhdlDelayMechanism(const VhdlDelayMechanism &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlDelayMechanism(SaveRestore &save_restore);

public:
    virtual ~VhdlDelayMechanism() ;

private:
    // Prevent compiler from defining the following
    VhdlDelayMechanism(const VhdlDelayMechanism &) ;            // Purposely leave unimplemented
    VhdlDelayMechanism& operator=(const VhdlDelayMechanism &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const = 0 ; // Ids defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const = 0;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const =0 ;

    // Calculate the conformance signature of this VhdlDelayMechanism
    virtual unsigned long       CalculateSignature() const ;

    // Copy delay mechanism
    virtual VhdlDelayMechanism * CopyDelayMechanism(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned             ReplaceChildExpr(VhdlExpression * /*old_node*/, VhdlExpression * /*new_node*/) { return 0 ; } // Delete 'old_node', puts new expression in its place.

} ; // class VhdlDelayMechanism

/* -------------------------------------------------------------- */

/*
  Class used in selected/conditional/regular signal assignments
*/
class VFC_DLL_PORT VhdlTransport : public VhdlDelayMechanism
{
public:
    VhdlTransport() ;

    // Copy tree node
    VhdlTransport(const VhdlTransport &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlTransport(SaveRestore &save_restore);

    virtual ~VhdlTransport() ;

private:
    // Prevent compiler from defining the following
    VhdlTransport(const VhdlTransport &) ;            // Purposely leave unimplemented
    VhdlTransport& operator=(const VhdlTransport &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLTRANSPORT; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlDelayMechanism
    virtual unsigned long       CalculateSignature() const ;

    // Copy transport delay
    virtual VhdlDelayMechanism *CopyDelayMechanism(VhdlMapForCopy &old2new) const ;
} ; // class VhdlTransport

/* -------------------------------------------------------------- */

/*
  Class used in selected/conditional/regular signal assignments
*/
class VFC_DLL_PORT VhdlInertialDelay : public VhdlDelayMechanism
{
public:
    VhdlInertialDelay(VhdlExpression *reject_expression, unsigned inertial) ;

    // Copy tree node
    VhdlInertialDelay(const VhdlInertialDelay &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlInertialDelay(SaveRestore &save_restore);

    virtual ~VhdlInertialDelay() ;

private:
    // Prevent compiler from defining the following
    VhdlInertialDelay() ;                                     // Purposely leave unimplemented
    VhdlInertialDelay(const VhdlInertialDelay &) ;            // Purposely leave unimplemented
    VhdlInertialDelay& operator=(const VhdlInertialDelay &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLINERTIALDELAY; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlDelayMechanism
    virtual unsigned long       CalculateSignature() const ;

    // Copy inertial delay.
    virtual VhdlDelayMechanism *CopyDelayMechanism(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Accessor methods
    VhdlExpression *            GetRejectExpression() const     { return _reject_expression ; }
    unsigned                    GetInertial() const             { return _inertial ; }

protected:
// Parse tree nodes (owned) :
    VhdlExpression *_reject_expression ;
    unsigned        _inertial ; // VHDL_inertial token
} ; // class VhdlInertialDelay

/* -------------------------------------------------------------- */

/*
  Class used in selected/conditional signal assignments
*/
class VFC_DLL_PORT VhdlOptions : public VhdlTreeNode
{
public:
    VhdlOptions(unsigned guarded, VhdlDelayMechanism *delay_mechanism) ;

    // Copy tree node
    VhdlOptions(const VhdlOptions &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlOptions(SaveRestore &save_restore);

    virtual ~VhdlOptions() ;

private:
    // Prevent compiler from defining the following
    VhdlOptions() ;                               // Purposely leave unimplemented
    VhdlOptions(const VhdlOptions &) ;            // Purposely leave unimplemented
    VhdlOptions& operator=(const VhdlOptions &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLOPTIONS; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlOptions
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    VhdlOptions *               Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routine.
    virtual unsigned            ReplaceChildDelayMechanism(VhdlDelayMechanism *old_node, VhdlDelayMechanism *new_node) ; // Delete 'old_node', puts new delay in its place.

    // Accessor methods
    unsigned                    IsGuarded() const ;
    VhdlDelayMechanism *        GetDelayMechanism() const   { return _delay_mechanism ; }

protected:
// Parse tree nodes (owned) :
    unsigned            _guarded ; // VHDL_guarded token or 0, if not guarded.
    VhdlDelayMechanism *_delay_mechanism ;
} ; // class VhdlOptions

/* -------------------------------------------------------------- */

/*
  Class used in conditional signal assignments
*/
class VFC_DLL_PORT VhdlConditionalWaveform : public VhdlTreeNode
{
public:
    VhdlConditionalWaveform(Array *waveform, VhdlExpression *when_condition) ;

    // Copy tree node
    VhdlConditionalWaveform(const VhdlConditionalWaveform &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConditionalWaveform(SaveRestore &save_restore);

    virtual ~VhdlConditionalWaveform() ;

private:
    // Prevent compiler from defining the following
    VhdlConditionalWaveform() ;                                           // Purposely leave unimplemented
    VhdlConditionalWaveform(const VhdlConditionalWaveform &) ;            // Purposely leave unimplemented
    VhdlConditionalWaveform& operator=(const VhdlConditionalWaveform &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONDITIONALWAVEFORM; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlConditionalWaveform
    virtual unsigned long       CalculateSignature() const ;

    // Copy conditional waveform
    VhdlConditionalWaveform *   Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    // Insert new expression before/after 'target_node' specific
    // expression in the array of expression for waveform. If
    // 'target_node' is not found in the array or 'target_node'
    // is null, nothing is inserted.
    unsigned                    InsertBeforeExpr(const VhdlExpression *target_node, VhdlExpression *new_node) ;
    unsigned                    InsertAfterExpr(const VhdlExpression *target_node, VhdlExpression *new_node) ;

    VhdlIdDef *                 TypeInfer(VhdlIdDef *wave_type) ;

    // Accessor methods
    Array *                     GetWaveform() const         { return _waveform ; }
    VhdlExpression *            Condition() const           { return _when_condition ; }
    // unsigned                    IsNull() const ; // check if this is waveform with null element: RD: not a good test, since waveform consists of multiple waveform elements.

    // Elaboration
    VhdlValue *                 ElaborateConditionalWaveform(VhdlExpression *target, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array           *_waveform ; // Array of VhdlExpression*
    VhdlExpression  *_when_condition ;
} ; // class VhdlConditionalWaveform

/* -------------------------------------------------------------- */

/*
  Class used in selected signal assignments in concurrent area VHDL 2008
*/
class VFC_DLL_PORT VhdlSelectedWaveform : public VhdlTreeNode
{
public:
    VhdlSelectedWaveform(Array *waveform, Array *when_choices) ;

    // Copy tree node
    VhdlSelectedWaveform(const VhdlSelectedWaveform &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSelectedWaveform(SaveRestore &save_restore);

    virtual ~VhdlSelectedWaveform() ;

private:
    // Prevent compiler from defining the following
    VhdlSelectedWaveform() ;                                        // Purposely leave unimplemented
    VhdlSelectedWaveform(const VhdlSelectedWaveform &) ;            // Purposely leave unimplemented
    VhdlSelectedWaveform& operator=(const VhdlSelectedWaveform &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSELECTEDWAVEFORM; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlSelectedWaveform
    virtual unsigned long       CalculateSignature() const ;

    // Copy Selected waveform
    VhdlSelectedWaveform *      Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.

    // Insert new expression/range before/after 'target_node' in the
    // array of expression/range. If 'target_node' is null or not
    // found in corresponding array, nothing is inserted.
    unsigned                    InsertBeforeExpr(const VhdlExpression *target_node, VhdlExpression *new_node) ; // Insert new expression before 'target_node'.
    unsigned                    InsertAfterExpr(const VhdlExpression *target_node, VhdlExpression *new_node) ; // Insert new expression after 'target_node'.
    unsigned                    InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before 'target_node'.
    unsigned                    InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range after 'target_node'.

    unsigned                    IsOthers() const ; // check if this is the 'others' waveform
    // unsigned                    IsNull() const ; // check if this is waveform with null element. RD: not a good test, since waveform consists of multiple waveform elements.

    VhdlIdDef *                 TypeInfer(VhdlIdDef *wave_type, VhdlIdDef *cond_type) ;

    // Test if this selected waveform has 'range' choices
    unsigned                    HasRangeChoice() const ;

    // Accessor methods
    Array *                     GetWaveform() const         { return _waveform ; }
    Array *                     GetWhenChoices() const      { return _when_choices ; }

    // Elaboration
    // Accumulate and test choices
    VhdlSelectedWaveform *      ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, const VhdlConstraint *case_expr_constraint, VhdlDataFlow *df, Set& overlap_check, unsigned is_matching, verific_uint64& extra_count, unsigned populate_dc_positions = 0) ;
    VhdlValue *                 ElaborateSelectedWaveform(VhdlConstraint *target_constraint, VhdlExpression *target, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array   *_waveform ;     // Array of VhdlExpression*
    Array   *_when_choices ; // Array of VhdlDiscreteRange*   (choices can be a range or an expression or 'others')
} ; // class VhdlSelectedWaveform

/* -------------------------------------------------------------- */

/*
  Class used in selected signal assignments in sequential area
*/
class VFC_DLL_PORT VhdlSelectedExpression : public VhdlTreeNode
{
public:
    VhdlSelectedExpression(VhdlExpression *expr, Array *when_choices) ;

    // Copy tree node
    VhdlSelectedExpression(const VhdlSelectedExpression &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlSelectedExpression(SaveRestore &save_restore);

    virtual ~VhdlSelectedExpression() ;

private:
    // Prevent compiler from defining the following
    VhdlSelectedExpression() ;                                        // Purposely leave unimplemented
    VhdlSelectedExpression(const VhdlSelectedExpression &) ;            // Purposely leave unimplemented
    VhdlSelectedExpression& operator=(const VhdlSelectedExpression &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLSELECTEDEXPRESSION ; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlSelectedExpression
    virtual unsigned long       CalculateSignature() const ;

    // Copy Selected waveform
    VhdlSelectedExpression *    Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.

    // Insert new expression/range before/after 'target_node' in the
    // array of expression/range. If 'target_node' is null or not
    // found in corresponding array, nothing is inserted.
    unsigned                    InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before 'target_node'.
    unsigned                    InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range after 'target_node'.

    unsigned                    IsOthers() const ; // check if this is the 'others' waveform
    // unsigned                    IsNull() const ; // check if this is waveform with null element. RD: not a good test, since waveform consists of multiple waveform elements.

    VhdlIdDef *                 TypeInfer(VhdlIdDef *expr_type, VhdlIdDef *cond_type) ;

    // Test if this selected waveform has 'range' choices
    unsigned                    HasRangeChoice() const ;

    // Accessor methods
    VhdlExpression *            GetExpression() const       { return _expr ; }
    Array *                     GetWhenChoices() const      { return _when_choices ; }

    // Elaboration
    // Accumulate and test choices
    VhdlSelectedExpression *    ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, const VhdlConstraint *case_expr_constraint, VhdlDataFlow *df, Set& overlap_check, unsigned is_matching, verific_uint64& extra_count, unsigned populate_dc_positions = 0) ;
    VhdlValue *                 ElaborateSelectedExpression(VhdlConstraint *target_constraint, VhdlExpression *target, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression   *_expr ;
    Array   *_when_choices ; // Array of VhdlDiscreteRange*   (choices can be a range or an expression or 'others')
} ; // class VhdlSelectedExpression

/* -------------------------------------------------------------- */

/*
  Class used in conditional signal assignments
*/
class VFC_DLL_PORT VhdlConditionalExpression : public VhdlTreeNode
{
public:
    VhdlConditionalExpression(VhdlExpression *expr, VhdlExpression *condition) ;

    // Copy tree node
    VhdlConditionalExpression(const VhdlConditionalExpression &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlConditionalExpression(SaveRestore &save_restore);

    virtual ~VhdlConditionalExpression() ;

private:
    // Prevent compiler from defining the following
    VhdlConditionalExpression() ;                                           // Purposely leave unimplemented
    VhdlConditionalExpression(const VhdlConditionalExpression &) ;            // Purposely leave unimplemented
    VhdlConditionalExpression& operator=(const VhdlConditionalExpression &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLCONDITIONALEXPRESSION; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual void                VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlConditionalExpression
    virtual unsigned long       CalculateSignature() const ;

    // Copy conditional waveform
    VhdlConditionalExpression * Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.

    VhdlIdDef *                 TypeInfer(VhdlIdDef *wave_type) ;

    // Accessor methods
    VhdlExpression *            GetExpression() const       { return _expr ; }
    VhdlExpression *            Condition() const           { return _condition ; }
    // unsigned                    IsNull() const ; // check if this is waveform with null element: RD: not a good test, since waveform consists of multiple waveform elements.

    // Elaboration
    VhdlValue *                 ElaborateConditionalExpression(VhdlExpression *target, VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression  *_expr ; // Array of VhdlExpression*
    VhdlExpression  *_condition ;
} ; // class VhdlConditionalExpression

/* -------------------------------------------------------------- */

/*
  Class used in block statements
*/
class VFC_DLL_PORT VhdlBlockGenerics : public VhdlTreeNode
{
public:
    VhdlBlockGenerics(Array *generic_clause, Array *generic_map_aspect) ;

    // Copy tree node
    VhdlBlockGenerics(const VhdlBlockGenerics &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBlockGenerics(SaveRestore &save_restore);

    virtual ~VhdlBlockGenerics() ;

private:
    // Prevent compiler from defining the following
    VhdlBlockGenerics() ;                                     // Purposely leave unimplemented
    VhdlBlockGenerics(const VhdlBlockGenerics &) ;            // Purposely leave unimplemented
    VhdlBlockGenerics& operator=(const VhdlBlockGenerics &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLBLOCKGENERICS; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlBlockGenerics
    virtual unsigned long       CalculateSignature() const ;

    // Copy block generics.
    VhdlBlockGenerics *         Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Delete 'old_node', puts new declaration in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.

    // Insert new declaration/range before/after 'target_node' in the
    // array of declaration/range. If 'target_node' is null or not
    // found in corresponding array, nothing is inserted.
    unsigned                    InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node'.
    unsigned                    InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after 'target_node'.
    unsigned                    InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before 'target_node'.
    unsigned                    InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range after 'target_node'.
    // Parse-tree generic addition/deletion
    VhdlIdDef *                 AddGeneric(const char *generic_name, VhdlSubtypeIndication *subtype_indication, VhdlExpression *initial_assign) ;
    unsigned                    RemoveGeneric(const char *generic_name) ;

    // Parse-tree generic connection routines
    unsigned                    AddGenericRef(const char *formal_generic_name, VhdlExpression *actual) ; // Create and add generic reference in generic map aspect of this block
    unsigned                    RemoveGenericRef(const char *formal_generic_name) ; // Remove actual for formal 'formal_generic_name' from generic map aspect of this block statement

    // Static Elabortaion internal routine :
    void                        StaticElaborate() ; // #3103

    // Accessor methods
    Array *                     GetGenericClause() const        { return _generic_clause ; }
    Array *                     GetGenericMapAspect() const     { return _generic_map_aspect ; }
    VhdlIdDef *                 GetBlockLabel() const           { return _block_label ; }

    // Elaboration
    void                        Elaborate() ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array     *_generic_clause ;     // (interface list) Array of VhdlInterfaceDecl*
    Array     *_generic_map_aspect ; // (association list) Array of VhdlDiscreteRange*
// Pointer back to the block label (not owned), set during analysis, for later use.
// From this label pointer, we can get the generics/ports in order, or the local scope etc...
    VhdlIdDef *_block_label ;
} ; // class VhdlBlockGenerics

/* -------------------------------------------------------------- */

/*
  Class used in block statements
*/
class VFC_DLL_PORT VhdlBlockPorts : public VhdlTreeNode
{
public:
    VhdlBlockPorts(Array *port_clause, Array *port_map_aspect) ;

    // Copy tree node
    VhdlBlockPorts(const VhdlBlockPorts &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlBlockPorts(SaveRestore &save_restore);

    virtual ~VhdlBlockPorts() ;

private:
    // Prevent compiler from defining the following
    VhdlBlockPorts() ;                                  // Purposely leave unimplemented
    VhdlBlockPorts(const VhdlBlockPorts &) ;            // Purposely leave unimplemented
    VhdlBlockPorts& operator=(const VhdlBlockPorts &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLBLOCKPORTS; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlBlockPorts
    virtual unsigned long       CalculateSignature() const ;

    // Copy block ports
    VhdlBlockPorts *            Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDecl(VhdlDeclaration *old_node, VhdlDeclaration *new_node) ; // Delete 'old_node', puts new declaration in its place.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.

    // Insert new declaration/range before/after 'target_node' in the
    // array of declaration/range. If 'target_node' is null or not
    // found in corresponding array, nothing is inserted.
    unsigned                    InsertBeforeDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration before 'target_node'.
    unsigned                    InsertAfterDecl(const VhdlDeclaration *target_node, VhdlDeclaration *new_node) ; // Insert new declaration after 'target_node'.
    unsigned                    InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before 'target_node'.
    unsigned                    InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range after 'target_node'.

    // Parse-tree port addition/deletion
    VhdlIdDef *                 AddPort(const char *port_name, unsigned port_direction, VhdlSubtypeIndication *subtype_indication) ;
    unsigned                    RemovePort(const char *port_name) ;

    // Parse-tree port connection routines
    unsigned                    AddPortRef(const char *formal_port_name, VhdlExpression *actual) ; // Add port-ref to port map aspect of this block
    unsigned                    RemovePortRef(const char *formal_port_name) ; // Remove actual for formal 'formal_port_name' from port map aspect of this block statement

    // Static Elabortaion internal routine :
    void                        StaticElaborate() ; // #3103

    // Accessor methods
    Array *                     GetPortClause() const       { return _port_clause ; }
    Array *                     GetPortMapAspect() const    { return _port_map_aspect ; }
    VhdlIdDef *                 GetBlockLabel() const       { return _block_label ; }

    // Elaboration
    void                        Elaborate() ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array     *_port_clause ;     // (interface list) Array of VhdlInterfaceDecl*
    Array     *_port_map_aspect ; // (association list) Array of VhdlDiscreteRange*
// Pointer back to the block label (not owned), set during analysis, for later use.
// From this label pointer, we can get the generics/ports in order, or
// the local scope etc...
    VhdlIdDef *_block_label ;
} ; // class VhdlBlockPorts

/* -------------------------------------------------------------- */

/*
  Class used in case statements
*/
class VFC_DLL_PORT VhdlCaseStatementAlternative : public VhdlTreeNode
{
public:
    VhdlCaseStatementAlternative(Array *choices, Array *statements) ;

    // Copy tree node
    VhdlCaseStatementAlternative(const VhdlCaseStatementAlternative &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlCaseStatementAlternative(SaveRestore &save_restore);

    virtual ~VhdlCaseStatementAlternative() ;

private:
    // Prevent compiler from defining the following
    VhdlCaseStatementAlternative() ;                                                // Purposely leave unimplemented
    VhdlCaseStatementAlternative(const VhdlCaseStatementAlternative &) ;            // Purposely leave unimplemented
    VhdlCaseStatementAlternative& operator=(const VhdlCaseStatementAlternative &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const      { return ID_VHDLCASESTATEMENTALTERNATIVE; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlCaseStatementAlternative
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    VhdlCaseStatementAlternative * Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildDiscreteRange(VhdlDiscreteRange *old_node, VhdlDiscreteRange *new_node) ; // Delete 'old_node', puts new range in its place.
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ; // Delete 'old_node', puts new statement in its place.

    // Insert new statement/range before/after 'target_node' in the
    // array of statement/range. If 'target_node' is null or not
    // found in corresponding array, nothing is inserted.
    unsigned                    InsertBeforeDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range before 'target_node'.
    unsigned                    InsertAfterDiscreteRange(const VhdlDiscreteRange *target_node, VhdlDiscreteRange *new_node) ; // Insert new range after 'target_node'.
    unsigned                    InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before 'target_node'.
    unsigned                    InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after 'target_node'.

    unsigned                    IsOthers() const ; // Check if this is the 'others' choice.

    // test if the alternative contains 'range' choice(s) :
    unsigned                    HasRangeChoice() const ;

    void                        TypeInferChoices(VhdlIdDef *case_type) ;

    // Accessor methods
    Array *                     GetChoices() const          { return _choices ; }
    Array *                     GetStatements() const       { return _statements ; }

    // Elaboration
    // Accumulate and test choices
    VhdlCaseStatementAlternative * ElaborateChoices(Map &choice_values, Map &choice_positions, VhdlValue *case_expr, VhdlDataFlow *df, VhdlConstraint const *case_expr_constraint, Set& overlap_check, VhdlIdDef const *case_expr_id, Map *& condition_id_value_map, unsigned is_matching_case, verific_uint64& extra_count, unsigned populate_dc_positions = 0) ;
    // Elaborate the statements
    void                        ElaborateAlternative(VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    Array   *_choices ;    // Array of VhdlDiscreteRange*   (choices can be a range or an expression or 'others')
    Array   *_statements ; // Array of VhdlStatement*
} ; // class VhdlCaseStatementAlternative

/* -------------------------------------------------------------- */

/*
  Class used in If statements
*/
class VFC_DLL_PORT VhdlElsif : public VhdlTreeNode
{
public:
    VhdlElsif(VhdlExpression *condition, Array *statements) ;

    // Copy tree node
    VhdlElsif(const VhdlElsif &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlElsif(SaveRestore &save_restore);

    virtual ~VhdlElsif() ;

private:
    // Prevent compiler from defining the following
    VhdlElsif() ;                             // Purposely leave unimplemented
    VhdlElsif(const VhdlElsif &) ;            // Purposely leave unimplemented
    VhdlElsif& operator=(const VhdlElsif &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const          { return ID_VHDLELSIF; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Variable usage analysis
    virtual unsigned            VarUsage(VhdlVarUsageInfo* info, unsigned action) ;

    // Calculate the conformance signature of this VhdlElsif
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    VhdlElsif *                 Copy(VhdlMapForCopy &old2new) const ;

    // Specific parse-tree manipulation routines.
    virtual unsigned            ReplaceChildExpr(VhdlExpression *old_node, VhdlExpression *new_node) ; // Delete 'old_node', puts new expression in its place.
    virtual unsigned            ReplaceChildStmt(VhdlStatement *old_node, VhdlStatement *new_node) ; // Delete 'old_node', puts new statement in its place.

    // Insert statement before/after 'target_node' in the
    // list of statements. If 'target_node' is not found
    // in the list or is null, nothing is inserted.
    unsigned                    InsertBeforeStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement before 'target_node'.
    unsigned                    InsertAfterStmt(const VhdlStatement *target_node, VhdlStatement *new_node) ; // Insert new statement after 'target_node'.

    // Accessor methods
    VhdlExpression *            Condition() const           { return _condition ; }
    Array *                     GetStatements() const       { return _statements ; }

    // Elaboration
    virtual void                ElaborateElsif(VhdlDataFlow *df) ;
#ifdef VHDL_SUPPORT_VARIABLE_SLICE
    virtual void                InitializeForVariableSlice() ;
#endif

protected:
// Parse tree nodes (owned) :
    VhdlExpression      *_condition ;
    Array               *_statements ; // Array of VhdlStatement*
} ; // class VhdlElsif

/* -------------------------------------------------------------- */

/*
  Class used in group templates
*/
class VFC_DLL_PORT VhdlEntityClassEntry : public VhdlTreeNode
{
public:
    VhdlEntityClassEntry(unsigned entity_class, unsigned box) ;

    // Copy tree node
    VhdlEntityClassEntry(const VhdlEntityClassEntry &node, VhdlMapForCopy &old2new) ;

    // Persistence (Binary restore via SaveRestore class)
    explicit VhdlEntityClassEntry(SaveRestore &save_restore);

    virtual ~VhdlEntityClassEntry() ;

private:
    // Prevent compiler from defining the following
    VhdlEntityClassEntry() ;                                        // Purposely leave unimplemented
    VhdlEntityClassEntry(const VhdlEntityClassEntry &) ;            // Purposely leave unimplemented
    VhdlEntityClassEntry& operator=(const VhdlEntityClassEntry &) ; // Purposely leave unimplemented

public:
    // Verific's RTTI method.  (ANSI's C++ RTTI is too expensive)
    virtual VHDL_CLASS_ID       GetClassId() const { return ID_VHDLENTITYCLASSENTRY; } // Defined in VhdlClassIds.h

    // Visitor design pattern
    virtual void                Accept(VhdlVisitor &v) ;

    // Persistence (Binary save via SaveRestore class)
    virtual void                Save(SaveRestore &save_restore) const;

    // Pretty print tree node
    virtual void                PrettyPrint(ostream &f, unsigned level) const ;

    // Calculate the conformance signature of this VhdlEntityClassEntry
    virtual unsigned long       CalculateSignature() const ;

    // Copy parse-tree.
    VhdlEntityClassEntry *      Copy(VhdlMapForCopy &old2new) const ;

    // Accessor methods
    unsigned                    GetEntityClass() const      { return _entity_class ; }
    unsigned                    GetBox() const              { return _box ; }

protected:
// Leaf node.
    unsigned _entity_class:16 ; // VHDL_port, VHDL_entity, VHDL_attribute etc. Any (named) entity token.
    unsigned _box:16 ;          // VHDL_box token, or 0 if no <> token was in this entity class entry.
} ; // class VhdlEntityClassEntry

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_MISC_H_
