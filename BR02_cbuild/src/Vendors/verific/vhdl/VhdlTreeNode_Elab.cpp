/*
 *
 * [ File Version : 1.141 - 2014/02/25 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlTreeNode.h" // Compile flags are defined within here

#include "string.h" // JJ 131006 needed for gcc > 445
#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Message.h"
#include "Strings.h"
#include "RuntimeFlags.h"   // run-time switches

#include "vhdl_file.h"
#include "vhdl_tokens.h"
#include "VhdlUnits.h"
#include "VhdlScope.h"      // For std types
#include "VhdlIdDef.h"      // For UniversalInteger
#include "VhdlName.h"       // For VhdlDesignator
#include "VhdlValue_Elab.h" // Net access for static elab
#include "VhdlDeclaration.h" // subprogram body for path_name attr
#include "VhdlSpecification.h" // subprogram body for path_name attr

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Static members
unsigned   VhdlNode::_static_elab = 1 ; // Static elaboration is the default.

Netlist *VhdlNode::_nl = 0 ;
Net *VhdlNode::_gnd = 0 ;
Net *VhdlNode::_pwr = 0 ;
Net *VhdlNode::_x = 0 ;
Net *VhdlNode::_z = 0 ;
VhdlIdDef *VhdlNode::_boolean_type = 0 ; // improvement for VIPER #7300

Array *VhdlNode::_named_path = 0 ;
Array *VhdlNode::_structural_stack = 0 ;
// VIPER 1863 & 1472 : begin
Array *VhdlNode::_value_stack = 0 ; // Global stack to store value/constraint of entity, required for recursion
Array *VhdlNode::_constraint_stack = 0 ;
Array *VhdlNode::_actual_id_stack = 0 ;
// VIPER 1863 & 1472 : end
Array *VhdlNode::_path_name = 0 ; // VIPER #5051 : `path_name
Array *VhdlNode::_instance_name = 0 ; // VIPER #5051 : `instance_name
/* static */ Map *VhdlTreeNode::_config_num_map = 0 ; // VIPER #6032 : Static map to hash signature of configuration
Set *      VhdlNode::_netlist_stack = 0 ; // VIPER #6944
Set *      VhdlNode::_deallocated_access_values = 0 ; // VIPER #4464, #7292
Set *      VhdlNode::_access_inner_values = 0 ; // VIPER #4464, #7292
/* static */ Map *VhdlTreeNode::_unit_num_map = 0 ; // VIPER #7054 : Static map for unique name creation
/* static */ Map *VhdlTreeNode::_long2short = 0 ; // VIPER #7853

/************************ Structural Stack   ***********************/

// Stack that keeps track of the parse-tree units (entity, architecture) that support structural recursion.
// If there is structural recursion (when the currently pushed 'unit' is already in the structural stack),
// then we store the values/constraints for the current unit, to be re-instated when we pop back from the stack.
// We put an upper limit on the depth of structural recursion, so that accidental infinite recursion does not result in a core dump.

// Push value/constraint entry
unsigned VhdlNode::PushStructuralStack(const VhdlIdDef *unit, const VhdlTreeNode *from)
{
    if (!unit) return 1 ;

    if (!_structural_stack) _structural_stack = new Array() ; // Create structural stack if not exists

    _structural_stack->InsertLast(unit) ; // Insert the argument specific unit
    // Give error for recursion, if stack depth is greater than 500
    // VIPER #7568: Control the hierarchy depth using runtime flag.
    // Default value is 1000.
    // If hierarchy depth is greater than user specified or default stack depth,
    // error will be produced
    unsigned max_stack_depth = RuntimeFlags::GetVar("vhdl_max_hierarchy_depth") ;
    if (_structural_stack->Size() > max_stack_depth) {
        if (from) {
            const VhdlIdDef *id = 0 ;
            if (unit->IsArchitecture()) id = unit->GetEntity() ;
            if (!id) id = unit ;
            //from->Error("structural recursion (via %s) is not supported", id->Name()) ;
            from->Error("stack overflow on recursion via %s, limit of %d has exceeded", id->Name(), max_stack_depth) ;
        }
        return 0 ;
    }
    // VIPER 1863 & 1472 : begin
    if (_static_elab) return 1 ; // No need to create value/constraint stack for static elaboration

    if (!IsStructuralRecursion()) return 1 ; // No structural recursion. Do not create value/constraint stack

    if (!_value_stack) _value_stack = new Array(1) ; // Create value stack if not present
    if (!_constraint_stack) _constraint_stack = new Array(1) ; // Create constraint stack if not present
    if (!_actual_id_stack) _actual_id_stack = new Array(1) ; // Create actual id stack if not present

    _value_stack->InsertLast(new Map(POINTER_HASH)) ; // Push Map to store iddef->value
    _constraint_stack->InsertLast(new Map(POINTER_HASH)) ; // Push Map to store iddef->constraint
    _actual_id_stack->InsertLast(new Map(POINTER_HASH)) ; // Push Map to store iddef->iddef
    // VIPER 1863 & 1472 : end
    return 1 ;
}

// Pop value/constraint entry
void VhdlNode::PopStructuralStack()
{
    VERIFIC_ASSERT(_structural_stack && _structural_stack->Size()) ;

    // VIPER 1863 & 1472
    // Check if last entry of structural stack has recursive instantiation, if so
    // we will reduce size of value and constraint stack
    unsigned has_recursion = IsStructuralRecursion() ;

    // Take last unit off the stack
    (void) _structural_stack->RemoveLast() ;

    // Check if we can remove the whole map :
    if (_structural_stack->Size()==0) {
        delete _structural_stack ;
        _structural_stack = 0 ;
    }
    // VIPER 1863 & 1472 : begin
    if (_static_elab || !_value_stack || !_constraint_stack || !has_recursion) return ; // Nothing to pop from

    Map *values = (Map*)_value_stack->RemoveLast() ; // Pop last entry
    Map *constraints = (Map*)_constraint_stack->RemoveLast() ; // Pop last entry
    Map *actuals = (Map*)_actual_id_stack->RemoveLast() ; // Pop last entry

    // Check if we can remove the whole map :
    if (_value_stack->Size()==0) {
        delete _value_stack ;
        _value_stack = 0 ;
    }
    // Check if we can remove the whole map :
    if (_constraint_stack->Size()==0) {
        delete _constraint_stack ;
        _constraint_stack = 0 ;
    }
    // Check if we can remove the whole map :
    if (_actual_id_stack->Size()==0) {
        delete _actual_id_stack ;
        _actual_id_stack = 0 ;
    }
    // Delete value/constraint from Map
    MapIter mi ;
    VhdlValue *val ;
    VhdlConstraint *constraint ;
    FOREACH_MAP_ITEM(values, mi, 0, &val) delete val ;
    FOREACH_MAP_ITEM(constraints, mi, 0, &constraint) delete constraint ;

    VhdlIdDef *id, *actual_id ;
    FOREACH_MAP_ITEM(actuals, mi, &id, &actual_id) {
        if (id && id->IsPackageInst() && actual_id) {
            VhdlPrimaryUnit *pkg =  actual_id->GetPrimaryUnit() ;
            delete pkg ;
        }
    }
    // Delete Maps
    delete values ;
    delete constraints ;
    delete actuals ;
    // VIPER 1863 & 1472 : end
}
// Clear structural recursion related stacks
void VhdlNode::ClearStructuralStack()
{
    // Structural stack just holds pointers, so we don't need to free its contents.
    // Just free the table itself.
    delete _structural_stack ;
    _structural_stack = 0 ;

    // VIPER 1863 & 1472 : begin
    // Delete value stack
    delete _value_stack ;
    _value_stack = 0 ;

    // Delete constraint stack
    delete _constraint_stack ;
    _constraint_stack = 0 ;

    // Delete actual id stack
    delete _actual_id_stack ;
    _actual_id_stack = 0 ;
    // VIPER 1863 & 1472 : end
}
// VIPER 1863 & 1472 : begin
// Returns 1 if last entered primary unit id has multiple entry in structural stack
unsigned VhdlNode::IsStructuralRecursion()
{
    if (_static_elab || !_structural_stack || !_structural_stack->Size()) return 0 ; // Doing static elaboration or no structural stack, return 0

    VhdlIdDef *unit_id = (VhdlIdDef*)_structural_stack->GetLast() ; // Get the last primary unit identifier from this stack
    if (unit_id && unit_id->GetEntity() && unit_id->IsArchitecture()) unit_id = unit_id->GetEntity() ;

    // Iterate through the stack and check if 'unit_id' has multiple entry in stack
    // If yes, there is structural recursion
    unsigned i ;
    unsigned found = 0 ;
    VhdlIdDef *id ;
    FOREACH_ARRAY_ITEM_BACK(_structural_stack, i, id) {
        if (id && id->GetEntity() && id->IsArchitecture()) id = id->GetEntity() ;
        // VIPER #8394: If configuration is instantiated, 'id' can be configuration
        // id. Check instantiated entity for recursion
        if (id && id->IsConfiguration() && id->GetEntity()) id = id->GetEntity() ;
        if (id != unit_id) continue ; // Not last id, continue
        if (found) return 1 ; // We are hitting 'unit_id' twice, so recursion
        found = 1 ; // We found 'unit_id' first time
    }

    return 0 ; // No recursion
}

unsigned VhdlNode::IsPresentInStructuralStack(const char *cell_name, const char *arch_name)
{
    //if (_static_elab || !_structural_stack || !_structural_stack->Size()) return 0 ;
    if (!_structural_stack || !_structural_stack->Size()) return 0 ;

    unsigned i ;
    VhdlIdDef *id ;
    unsigned n_ele = _structural_stack->Size() ;
    FOREACH_ARRAY_ITEM_BACK(_structural_stack, i, id) {
        if (i == n_ele-1) continue ; // Skip the last element..
        if (id && id->IsArchitecture()) {
            if (arch_name && !Strings::compare(arch_name, "")) {
                if (!Strings::compare_nocase(id->OrigName(), arch_name)) continue ;
            }

            id = id->GetEntity() ;
        }

        if (id && Strings::compare_nocase(id->OrigName(), cell_name)) return 1 ;
    }

    return 0 ;
}

// Switch value/constraint for argument specific iddef between value/constraint stack and iddef
void VhdlNode::SwitchValueConstraintId(VhdlIdDef *id)
{
    SwitchValue(id) ; // Switch value
    SwitchConstraint(id) ; // Switch constraint
    SwitchActualId(id) ; // Switch actual id
}
// Switch value for argument specific iddef between value stack and iddef
void VhdlNode::SwitchValue(VhdlIdDef *id)
{
    if (!id) return ; // No id for swap

    // Get last entered value list from corresponding stack
    Map *id_values = GetValueMap() ;

    // Get value from value list
    VhdlValue *val = id_values ? (VhdlValue*)id_values->GetValue(id): 0 ;

    // Insert value of iddef to corresponding list
    if (id_values) (void) id_values->Insert(id, id->TakeValue(), 1) ;

    // Set previous value  of list to iddef
    id->SetValue(val) ;
}

// Switch constraint for argument specific iddef between constraint stack and iddef
void VhdlNode::SwitchConstraint(VhdlIdDef *id)
{
    if (!id) return ; // No id for swap

    // Get last entered constraint list from corresponding stack
    Map *id_constraints = GetConstraintMap() ;

    // Get value from constraint list
    VhdlConstraint *constraint = id_constraints ? (VhdlConstraint*)id_constraints->GetValue(id): 0 ;

    // Insert constraint of iddef to corresponding list
    if (id_constraints) (void) id_constraints->Insert(id, id->TakeConstraint(), 1) ;

    // Set previous constraint  of list to iddef
    id->SetConstraint(constraint) ;
}
// Switch actual id for argument specific iddef between actual id stack and iddef
void VhdlNode::SwitchActualId(VhdlIdDef *id)
{
    if (!id) return ; // No id for swap

    // Get last entered actual id list from corresponding stack
    Map *id_actuals = GetActualIdMap() ;

    // Get value from actual id list
    VhdlIdDef *actual_id = id_actuals ? (VhdlIdDef*)id_actuals->GetValue(id): 0 ;

    // Insert actual id of iddef to corresponding list
    if (id_actuals) {
        VhdlIdDef *from_id_actual = 0 ;
        if (id->IsPackageInst() || (id->IsGeneric() && id->IsPackage())) {
            from_id_actual = id->TakeElaboratedPackageId() ;
        } else {
            from_id_actual = id->TakeActualId() ;
        }
        (void) id_actuals->Insert(id, from_id_actual, 1) ;
    }

    // Set previous iddef of list to iddef
    if (id->IsPackageInst() || (id->IsGeneric() && id->IsPackage())) {
        id->SetElaboratedPackageId(actual_id) ;
    } else {
        id->SetActualId(actual_id, 0) ;
    }
}
Map *VhdlNode::GetValueMap()
{
    // Map can be present if processing entity has no recursive instantiation, but
    // it is instantiated in a recursive entity, so check for recursion before
    // returning map
    if (!_value_stack || !IsStructuralRecursion()) return 0 ; // No stack, return 0
    return (_value_stack->Size()) ? (Map*)_value_stack->GetLast(): 0 ; // Return last entry from value stack
}
Map *VhdlNode::GetConstraintMap()
{
    // Map can be present if processing entity has no recursive instantiation, but
    // it is instantiated in a recursive entity, so check for recursion before
    // returning map
    if (!_constraint_stack || !IsStructuralRecursion()) return 0 ; // No stack, return 0
    return (_constraint_stack->Size()) ? (Map*)_constraint_stack->GetLast(): 0 ; // Return last entry from constraint stack
}
Map *VhdlNode::GetActualIdMap()
{
    // Map can be present if processing entity has no recursive instantiation, but
    // it is instantiated in a recursive entity, so check for recursion before
    // returning map
    if (!_actual_id_stack || !IsStructuralRecursion()) return 0 ; // No stack, return 0
    return (_actual_id_stack->Size()) ? (Map*)_actual_id_stack->GetLast(): 0 ; // Return last entry from actual id stack
}
// Switch value/constraint fields of iddefs
// If global value/constraint stack is active, set value/constraint from those stack,
// otherwise set null to value/constraint fields of iddefs. Set the value/constraint
// from iddefs to appropriate stacks.
void VhdlNode::SwitchValueConstraintOfScope(const VhdlScope *scope, unsigned only_generic_ids)
{
    // Do not switch only generic ids if there is no value/constraint stack
    if (!scope || _static_elab || (only_generic_ids && !GetValueMap())) return ; // Nothing should be swiched for static elab
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(scope->GetThisScope(), mi, 0, &id) {
        if (!id || (id == scope->GetOwner())) continue ;
        // We should switch only generic ids and id is not generic, so continue
        if (only_generic_ids && !id->IsGeneric()) continue ;
        SwitchValueConstraintId(id) ; // Switch value/constraint

        if (id->IsPackageInst()) continue ; // No need to switch for package inst, new ids for every elaboration
        if (id->IsAlias()) continue ; // Viper #5542
        if (id->IsPackage() && id->IsGeneric()) continue ;

        VhdlScope *id_local_scope = id->LocalScope() ;
        // VIPER #6779 : Recurse only child scopes of 'scope'. If owner
        // of id_local_scope is not the 'id', id_local_scope is not the
        // scope created by 'id', so ignore that.
        if (id_local_scope && (id_local_scope->GetOwner() != id)) continue ;
        if (!only_generic_ids && id_local_scope) { // Do not recurse if we need to switch only interface/generic ids
            // Pop nested scope value/constraints too
            SwitchValueConstraintOfScope(id_local_scope, only_generic_ids) ;
        }
    }
}
// VIPER #4001 : Set value/constraint field of interface ids to null for
// non-recursive entity
void VhdlNode::SwitchValueConstraintOfInterfaceIds(const VhdlScope *scope)
{
    // Do not switch only generic ids if there is no value/constraint stack
    if (!scope || _static_elab || GetValueMap()) return ; // Nothing should be swiched for static elab
    MapIter mi ;
    VhdlIdDef *id ;
    FOREACH_MAP_ITEM(scope->GetThisScope(), mi, 0, &id) {
        if (!id || (id == scope->GetOwner())) continue ;
        if (!id->IsGeneric() && !id->IsPort()) continue ;
        // We should switch only generic ids and id is not generic, so continue
        SwitchValueConstraintId(id) ; // Switch value/constraint
    }
}
// VIPER 1863 & 1472 : end
/******** Name Stack (keep track of instance naming during elaboration) ********/

void VhdlNode::ClearNamedPath()
{
    if (!_named_path) return ;
    // Remove whatever strings are still on the stack :
    unsigned i ;
    char *item ;
    FOREACH_ARRAY_ITEM(_named_path, i, item) {
        Strings::free(item) ;
    }
    delete _named_path ;
    _named_path = 0 ;
}

// VIPER #7683: Added new argument iteration value to process block name
// with iteration value
void VhdlNode::PushNamedPath(const char *string, const char *iter_val)
{
    if (!_named_path) _named_path = new Array() ;
    char *name = 0 ;
    if (iter_val) {
        // VIPER #7683 : We are changing the name generation.
        // 1. Separator ':' is replaced by '.'
        // 2. Iteration value is attached with block label using '[]'
        //
        // If previous name generation is to be restored (':' separated names),
        // do the following
        // 1. Uncomment the following line and comment out next line
        // 2. Change the value of default argument 'char *separator' to ":" in
        //    VhdlNode::PathName API (file VhdlTreeNode.h)

        //name = string ? Strings::save(string, ":", iter_val): Strings::save(iter_val) ;
        name = string ? Strings::save(string, "[", iter_val, "]"): Strings::save("[", iter_val, "]") ;
    } else {
        name = Strings::save(string) ;
    }
    // Add the item as a saved string :
    //_named_path->InsertLast(Strings::save(string)) ;
    _named_path->InsertLast(name) ;
}

void VhdlNode::PopNamedPath()
{
    VERIFIC_ASSERT(_named_path && _named_path->Size()) ;
    // Remove the last item :
    char *last = (char*)_named_path->RemoveLast() ;
    Strings::free( last ) ;
    // Remove the entire array if its empty now :
    if (!_named_path->Size()) {
        delete _named_path ;
        _named_path = 0 ;
    }
}

char *VhdlNode::PathName(const char *name, const char *prefix, const char *separator, const char *suffix)
{
    // Create NEWLY ALLOCATED name from the info in the name stack : Run Back->front
    // Return 0 if the stack is empty (this is to save save/free time if there is nothing to do).
    //
    //   Start entire result with 'prefix'
    //   Separate stack items names by 'separator'
    //   End entire result 'suffix'
    //
    if (!_named_path) return 0 ;

    // Run id's in order that they came in and concatenate their names..
    unsigned i ;
    char *item ;
    char *result = 0 ;
    char *tmp ;
    FOREACH_ARRAY_ITEM(_named_path, i, item) {
        // First time :
        if (!result) {
            if (prefix) {
                result = Strings::save(prefix, item) ;
            } else {
                result = Strings::save(item) ;
            }
            continue ;
        }

        // next ones :
        // add separator and id name :
        tmp = result ;
        if (separator) {
            result = Strings::save(result, separator, item) ;
        } else {
            result = Strings::save(result, item) ;
        }
        Strings::free( tmp ) ;
    }

    // add separator and the name
    if (result && name) {
        tmp = result ;
        if (separator) {
            result = Strings::save(result, separator, name) ;
        } else {
            result = Strings::save(result, name) ;
        }
        Strings::free( tmp ) ;
    }

    // Close with a suffix
    if (result && suffix) {
        tmp = result ;
        result = Strings::save(result, suffix) ;
        Strings::free( tmp ) ;
    }
    return result ;
}

/********** Fast Net testing routines ***************/

unsigned VhdlNode::ConstNet(const Net *n)
{
    return (n==_gnd || n==_pwr || n==_x || n==_z) ;
}

unsigned VhdlNode::ContainsOnly(const Net *expr, const Net *net)
{
    // Check if this 'expression' contains only USER net 'net' or an
    // inversion of the net or constants   at its sources.
    // Do this by recurring into the logic that drives 'expr'.
    // Needed for event expression checking.

    if (!net || !expr) return 1 ; // Assume its all OK.

    // Check only against net that is a user net :
    if (!net->IsUserDeclared()) {
        // VIPER #5003: Check the full inverter chain of 'net':
        //net = net->InvNet() ;
        //if (!net || !net->IsUserDeclared()) {
        //    // Cant find a user net. Assume all is OK.
        //    return 1 ;
        //}
        while (net && !net->IsUserDeclared()) net = net->InvNet() ;
        if (!net) return 0 ;
        // Otherwise it is an user declared net, fall-through...
    }

    if (expr == net) {
        // Both nets are from the same source
        return 1 ;
    }
    if (ConstNet(expr)) {
        // Expr is constant. That's OK.
        return 1 ;
    }

    if (expr->IsUserDeclared()) {
        // 'expr' is a user net, and it's not 'net'. So, expr contains other input nets.
        return 0 ;
    }
    return 1 ;
}

/****************************************************************************/

Net *VhdlTreeNode::Inv(Net *n) const
{
    if (!n) return 0 ; // real don't-care
    if (n==_gnd) return _pwr ;
    if (n==_pwr) return _gnd ;
    if (n==_x && RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ;
    // Viper 7874: return _x as derived from the the not table of std_1164 package
    if (n==_z && RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ;
    // VIPER #4412 : In static elaboration _nl is null
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made with what we do when vhdl_preserve_x is on. At least in the
        // static elaboration
        if (n==_x) return _x ;
        if (n==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::Buf(Net *n) const
{
    if (!n) return 0 ; // real don't-care
// Don't optimize buffer insertion
//    if (n==_gnd || n==_pwr || n==_x) return n ;
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made with what we do when vhdl_preserve_x is on. At least in the
        // static elaboration
        if (n==_gnd || n==_pwr || n==_x || n==_z) return n ;
        return 0 ; // Cannot use _nl in static elab, return now
    }
    return 0 ;
}

Net *VhdlTreeNode::Or(Net *n1, Net *n2) const
{
    if (n1==_pwr || n2==_pwr) return _pwr ;
    if (!n1 || !n2) return 0 ; // real don't-care
    if (n1==_gnd) return n2 ; // 0 OR something is something. Even for x.
    if (n2==_gnd) return n1 ;
    if (n1==n2) return n1 ;
    if ((n1==_x || n2==_x) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _pwr ; // x optimization : x OR something is 1 or x. Cant reduce to x.
    // Viper 7874: return _x as derived from the the Or table of std_1164 package
    if ((n1==_z || n2==_z) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _pwr ; // x optimization : x OR something is 1 or x. Cant reduce to x.
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made consistent with the standard table at least in the static elab
        if (n1==_z || n2==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::Nor(Net *n1, Net *n2) const
{
    if (n1==_pwr || n2==_pwr) return _gnd ;
    if (!n1 || !n2) return 0 ; // real don't-care
    if (n1==_gnd) return Inv(n2) ;
    if (n2==_gnd) return Inv(n1) ;
    if (n1==n2) return Inv(n1) ;
    if ((n1==_x || n2==_x) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _gnd ; // x optimization : x NOR something is 0 or x. Cant reduce to x.
    // Viper 7874: return _x as derived from the the Or table of std_1164 package
    if ((n1==_z || n2==_z) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _gnd ; // x optimization : x NOR something is 0 or x. Cant reduce to x.
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made consistent with the standard table at least in the static elab
        if (n1==_z || n2==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::And(Net *n1, Net *n2) const
{
    if (n1==_gnd || n2==_gnd) return _gnd ;
    if (!n1 || !n2) return 0 ; // real don't-care
    if (n1==_pwr) return n2 ; // 1 AND something is something. Even for x.
    if (n2==_pwr) return n1 ;
    if (n1==n2) return n1 ;
    if ((n1==_x || n2==_x) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _gnd ; // x optimization : x AND something is 0 or x. Cant reduce to x.
    // Viper 7874: return _x as derived from the the And table of std_1164 package
    if ((n1==_z || n2==_z) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _gnd ; // x optimization : x AND something is 0 or x. Cant reduce to x.
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made consistent with the standard table at least in the static elab
        if (n1==_z || n2==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::Nand(Net *n1, Net *n2) const
{
    if (n1==_gnd || n2==_gnd) return _pwr ;
    if (!n1 || !n2) return 0 ; // real don't-care
    if (n1==_pwr) return Inv(n2) ;
    if (n2==_pwr) return Inv(n1) ;
    if (n1==n2) return Inv(n1) ;
    if ((n1==_x || n2==_x) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _pwr ; // x optimization : x NAND something is 1 or x. Cant reduce to x.
    // Viper 7874: return _x as derived from the the And table of std_1164 package
    if ((n1==_z || n2==_z) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _pwr ; // x optimization : x NAND something is 1 or x. Cant reduce to x.
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made consistent with the standard table at least in the static elab
        if (n1==_z || n2==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::Xor(Net *n1, Net *n2) const
{
    if (!n1 || !n2) return 0 ; // real don't-care
    if ((n1==_x || n2==_x) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ; // x optimization : x XOR something is always x.
    // Viper 7874: return _x as derived from the the Xor table of std_1164 package
    if ((n1==_z || n2==_z) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ; // x optimization : x XOR something is always x.
    if (n1==n2) return _gnd ;
    if (n1==_gnd) return n2 ;
    if (n2==_gnd) return n1 ;
    if (n1==_pwr) return Inv(n2) ;
    if (n2==_pwr) return Inv(n1) ;
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made consistent with the standard table at least in the static elab
        if (n1==_x || n2==_x) return _x ;
        if (n1==_z || n2==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::Xnor(Net *n1, Net *n2) const
{
    if (!n1 || !n2) return 0 ; // real don't-care
    if ((n1==_x || n2==_x) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ; // x optimization : x XNOR something is always x.
    // Viper 7874: return _x as derived from the the Xor table of std_1164 package
    if ((n1==_z || n2==_z) && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ; // x optimization : x XNOR something is always x.
    if (n1==n2) return _pwr ;
    if (n1==_pwr) return n2 ;
    if (n2==_pwr) return n1 ;
    if (n1==_gnd) return Inv(n2) ;
    if (n2==_gnd) return Inv(n1) ;
    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made consistent with the standard table at least in the static elab
        if (n1==_x || n2==_x) return _x ;
        if (n1==_z || n2==_z) return _x ;
        return 0 ;
    }
    return 0 ;
}

Net *VhdlTreeNode::Mux(Net *n0, Net *n1, Net *c, unsigned opt /*= 0*/) const
{
    if (n0==n1) return n0 ;
    if (c==_gnd) return n0 ;
    if (c==_pwr) return n1 ;

    // don't-care optimization
    if (!c) return 0 ;
    if (!n0) return n1 ;
    if (!n1) return n0 ;

    // X optimization
    if (c==_x && !RuntimeFlags::GetVar("vhdl_preserve_x")) return _x ; // inputs are different, and c==_x
    if (n0==_x && !RuntimeFlags::GetVar("vhdl_preserve_x")) return n1 ;
    if (n1==_x && !RuntimeFlags::GetVar("vhdl_preserve_x")) return n0 ;

// If we want to preserve user nets, we should not create identical
// datalines. So don't mix control lines with data lines.
// See bugs47/test.vhd
// This optimization will occur later in constant prop any way.
    // Plain optimizations on constants driving the mux :
    // VIPER Issue #7244: Optimize this mux when argument 'opt' is set,
    // even when the runtime flag vhdl_preserve_user_nets is set
    if (n0==_gnd && n1==_pwr && (opt || !RuntimeFlags::GetVar("vhdl_preserve_user_nets"))) return c ;
    if (n0==_pwr && n1==_gnd && (opt || !RuntimeFlags::GetVar("vhdl_preserve_user_nets"))) return Inv(c) ;
#if 0
    // Don't minimize the MUX if it does not reduce gate-count :
    // It's better to keep a mux around, even if one side is constant,
    // since we can extract control/data info from it.
    // That is important for set/reset/hold detection for latches and flip-flops and FSMs.
    if (n0==_gnd) return And(n1,c) ; // o is only 1 if both c and n1 are 1. That's an AND
    if (n1==_pwr) return Or(n0,c) ;  // o is only 0 if both n0 and c are 0. That's an OR
    if (n0==_pwr) return Or(n1,Inv(c)) ;  // Some more optimization
    if (n1==_gnd) return And(n0,Inv(c)) ; // Some more optimization
#endif

    if (IsStaticElab()) {
        // Viper 7874 for _x and _z do not return 0 in static elab. Behavior
        // is made with what we do when vhdl_preserve_x is on. At least in the
        // static elaboration
        if (c==_x || c==_z) return _x ; // inputs are different, and c==_x
        if (n0==_x || n0==_z) return n1 ;
        if (n1==_x || n1==_z) return n0 ;
        return 0 ;
    }
    return 0 ;
}

// If the formal is unconstrained, and is used as partial formal in its
// actual association list, the following API walks over the actual
// association list to figure out the constraints
//
// Vipers : 3058, 3065, 3118 and a few more
void VhdlNode::EvaluateConstraintForPartialAssoc(const Array *assoc_list, VhdlDiscreteRange const *this_assoc, VhdlDataFlow *df)
{
    VhdlName *assoc_formal = this_assoc->FormalPart() ;
    if (!assoc_formal) return ; // positional assoc

    VhdlIdDef *this_formal = this_assoc->FindFormal() ;
    if (!this_formal) return ;

    if (assoc_formal->IsFunctionCall() || assoc_formal->IsTypeConversion()) {
        // A conversion function in the formal. Take its argument
        Array *func_arg_list = assoc_formal->GetAssocList() ;
        if (!func_arg_list || func_arg_list->Size() != 1) return ;

        VhdlDiscreteRange *assoc = (VhdlDiscreteRange *) func_arg_list->At(0) ;

        VhdlDiscreteRange *formal_part = assoc->ActualPart() ;
        if (!formal_part) return ;

        if (formal_part->FindFullFormal()) return ; // not partial association
    }

    Map *id_constraints = GetConstraintMap() ;
    VhdlConstraint *formal_constraint = id_constraints ? (VhdlConstraint*)id_constraints->GetValue(this_formal) : this_formal->Constraint() ;

    if (!formal_constraint) return ; // something bad happened

    // If unconstrained, extract the constraint from the assoc_list
    if (!formal_constraint->IsUnconstrained()) {
        CheckOverlapAndCompletenessOfPartialFormal(assoc_list, this_formal) ;
        return ;
    }

    // unconstrained formal must be of array constraint
    VERIFIC_ASSERT(formal_constraint->IsArrayConstraint()) ;

    unsigned n_unconstrained_dim = 0 ;

    unsigned i = 0 ;
    unsigned j = 0 ;
    VhdlDiscreteRange *assoc ;
    FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
        if (!assoc) return ;

        assoc_formal = assoc->FormalPart() ;
        VhdlDiscreteRange *formal_part = assoc_formal ;
        if (!formal_part || !assoc_formal) continue ; // positional assoc

        VhdlIdDef *formal_id = formal_part->FindFormal() ;
        if (!formal_id) continue ; // something bad happened

        if (formal_id != this_formal) continue ;

        if (assoc_formal->IsFunctionCall() || assoc_formal->IsTypeConversion()) {
            // A conversion function in the formal. Take its argument
            Array *func_arg_list = formal_part->GetAssocList() ;
            if (!func_arg_list || func_arg_list->Size() != 1) continue ;

            assoc = (VhdlDiscreteRange *) func_arg_list->At(0) ;

            formal_part = assoc->ActualPart() ;
            if (!formal_part) continue ;
        }

        VhdlDiscreteRange *formal_prefix = formal_part ;
        VhdlDiscreteRange *index = 0 ;
        Array *indexes = 0 ;

        while (formal_prefix) {
            indexes = formal_prefix->GetAssocList() ;
            formal_prefix = formal_prefix->GetPrefix() ;
            if (!formal_prefix || !formal_prefix->GetPrefix()) break ;
        }

        if (!indexes) return ; // cannot constrain, bail out

        VhdlConstraint *element_constraint = formal_constraint ;

        FOREACH_ARRAY_ITEM(indexes, j, index) {
            if (!element_constraint->IsUnconstrained() && !n_unconstrained_dim) break ;
            if (n_unconstrained_dim && (j >= n_unconstrained_dim)) break ;

            if (index->IsRange()) {
                VhdlConstraint *range = index->EvaluateConstraintInternal(df, 0) ;

                if (!range || range->IsNullRange() || range->IsUnconstrained()) {
                    delete range ;
                    return ; // cannot constrain, bail out
                }

                element_constraint->ConstraintByChoice(range->Left()) ;
                element_constraint->ConstraintByChoice(range->Right()) ;

                delete range ;
            } else {
                VhdlValue *index_val = index->Evaluate(0, df, 0) ;
                if (!index_val) return ; // cannot constrain, bail out

                element_constraint->ConstraintByChoice(index_val) ;
                delete index_val ;
            }

            element_constraint = element_constraint->ElementConstraint() ;
        }

        if (!n_unconstrained_dim) n_unconstrained_dim = j ;
    }

    CheckOverlapAndCompletenessOfPartialFormal(assoc_list, this_formal) ;
    return ;
}

// Partial formal test : all partial formals must be mutually exclusive and exhaustive
void VhdlNode::CheckOverlapAndCompletenessOfPartialFormal(const Array *assoc_list, VhdlIdDef const *this_formal)
{
    if (!this_formal) return ;
    VhdlConstraint *formal_constraint = this_formal->Constraint() ;

    // formal must be constrained by now..
    if (!formal_constraint || formal_constraint->IsUnconstrained()) return ;

    // Create a 'fully filled' initial value. The structure only serves to 'sign-off' associated partial formals.
    // FIX ME : Should find a better way to sign-off associated partials formals.
    VhdlValue *full_formal_value = formal_constraint->CreateFullInitialValue(this_formal) ;
    if (!full_formal_value) return ;

    unsigned i ;
    VhdlDiscreteRange *assoc ;
    VhdlDiscreteRange *non_null_assoc = 0 ;
    FOREACH_ARRAY_ITEM(assoc_list, i, assoc) {
        if (!assoc) return ;
        if (!non_null_assoc) non_null_assoc = assoc ;

        VhdlName *assoc_formal = assoc->FormalPart() ;
        VhdlDiscreteRange *formal_part = assoc_formal ;
        if (!formal_part || !assoc_formal) continue ; // positional assoc

        VhdlIdDef *formal_id = formal_part->FindFormal() ;
        if (!formal_id) continue ; // something bad happened

        if (formal_id != this_formal) continue ;

        if (assoc_formal->IsFunctionCall() || assoc_formal->IsTypeConversion()) {
            // A conversion function in the formal. Take its argument
            Array *func_arg_list = formal_part->GetAssocList() ;
            if (!func_arg_list || func_arg_list->Size() != 1) continue ;

            assoc = (VhdlDiscreteRange *) func_arg_list->At(0) ;

            formal_part = assoc->ActualPart() ;
            if (!formal_part) continue ;
        }

        if (IsStaticElab()) {
            // Viper 4659 Checking Array Port sizes partial formals
            VhdlConstraint *formal_part_constraint = formal_part->EvaluateConstraintInternal(0, 0) ;
            VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
            // VIPER #6448: Check the constraint of actual part passing target constraint
            //VhdlConstraint *actual_constraint = (actual_part && !actual_part->IsAggregate()) ? actual_part->EvaluateConstraint(0):0 ;
            VhdlConstraint *actual_constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, formal_part_constraint): 0 ;
            // VIPER #3140 : Check size mismatching in port association
            if (actual_constraint && formal_part_constraint && !actual_constraint->IsUnconstrained() && !formal_part_constraint->IsUnconstrained()) (void) formal_part_constraint->CheckAgainst(actual_constraint, assoc, 0) ;
            delete actual_constraint ;
            delete formal_part_constraint ;
        }

        VhdlConstraint *full_constraint = formal_constraint ;
        VhdlValue *formal_value = full_formal_value ;
        TakePartialValue(formal_value, full_constraint, formal_part, 1) ;
    }

    if (non_null_assoc && !IsEmptyValue(full_formal_value)) non_null_assoc->Error("not all partial formals of %s have actual", this_formal->Name()) ;
    delete full_formal_value ;

    return ;
}

unsigned VhdlNode::IsEmptyValue(VhdlValue const *val)
{
    if (!val) return 1 ;
    if (!val->IsComposite()) return 0 ;

    // Do not use 'ValueAt()' to get element values. They fill in with default value.
    // So get the values array for the composite value directly :
    Array *values = val->GetValues() ;
    if (!values) return 1 ; // no values at all. That's empty.

    unsigned i ;
    VhdlValue *elem_val ;
    FOREACH_ARRAY_ITEM(values,i,elem_val) {
        if (!IsEmptyValue(elem_val)) return 0 ;
    }

    return 1 ;
}

void VhdlNode::TakePartialValue(VhdlValue *&full_value, VhdlConstraint *&full_constraint, VhdlDiscreteRange const *partial_formal, unsigned take)
{
    if (!partial_formal || !full_constraint || !full_value) return ;

    VhdlName *prefix = partial_formal->GetPrefix() ;
    if (!prefix) return ;

    TakePartialValue(full_value, full_constraint, prefix, 0) ;

    VhdlDesignator *suffix = partial_formal->GetSuffix() ;

    if (suffix) {
        TakePartialValue(suffix, full_constraint, full_value, take) ;
    } else {
        Array *indexes = partial_formal->GetAssocList() ;
        if (!indexes) return ;

        unsigned i ;
        VhdlDiscreteRange *index ;
        unsigned num_indices = indexes->Size() ;
        FOREACH_ARRAY_ITEM(indexes, i, index) {
            if (!index) return ; // something went wrong ?

            unsigned take_this = (num_indices == i+1) ? take : 0 ; // last index
            TakePartialValue(index, full_constraint, full_value, take_this) ;
        }
    }

    return ;
}

void VhdlNode::TakePartialValue(VhdlDiscreteRange *element, VhdlConstraint *&full_constraint, VhdlValue *&full_value, unsigned take)
{
    if (!element || !full_constraint || !full_value) return ; // something wrong

    unsigned pos ;
    if (element->IsRange()) {
        if (!take) return ; // range must be the last element

        VhdlConstraint *range = element->EvaluateConstraintInternal(0, 0) ;
        if (!range) return ;

        if (range->IsNullRange()) {
            // null range warning is already given
            element->Warning("range choice ignored") ;
            delete range ;
            return ; // Never selected
        }

        // Check if both bounds are in the constraint :
        if (!full_constraint->Contains(range->Left())) {
            char *image = range->Left()->Image() ;
            char *r = full_constraint->Image() ;
            element->Error("index value %s is out of range %s", image, r) ;
            Strings::free(image) ;
            Strings::free(r) ;
            delete range ;
            return ;
        }

        if (!full_constraint->Contains(range->Right())) {
            char *image = range->Right()->Image() ;
            char *r = full_constraint->Image() ;
            element->Error("index value %s is out of range %s", image, r) ;
            Strings::free(image) ;
            Strings::free(r) ;
            delete range ;
            return ;
        }

        // Get the actual position of the values in the constraint :
        unsigned left_pos = full_constraint->PosOf(range->Left()) ;
        unsigned right_pos = full_constraint->PosOf(range->Right()) ;

        if (left_pos > right_pos) { // swap values so that left < right
            unsigned tmp = right_pos ;
            right_pos  = left_pos ;
            left_pos = tmp ;
        }

        for (pos = left_pos ; pos <= right_pos; ++pos) {
            VhdlValue *partial_val = full_value->TakeValueAt(pos) ;
            if (!partial_val) element->Error("partial formal element already associated") ;

            delete partial_val ;

            if (pos == right_pos) break ;
        }

        delete range ;
        return ;  // must be the last element
    } else {
        VhdlValue *element_pos = element->Evaluate(0,0,0) ;
        if (!element_pos) return ; // something went wrong

        if (!element_pos->IsConstant()) {
            element->Error("expression is not constant") ;
            delete element_pos ;
            return ;
        }

        if (!full_constraint->Contains(element_pos)) {
            char *image = element_pos->Image() ;
            char *r = full_constraint->Image() ;
            element->Error("index value %s is out of range %s", image, r) ;
            Strings::free(image) ;
            Strings::free(r) ;
            delete element_pos ;
            return ;
        }

        pos = full_constraint->PosOf(element_pos) ;
        delete element_pos ;

        full_constraint = full_constraint->ElementConstraintAt(pos) ;
        if (take) {
            VhdlValue *partial_val = full_value->TakeValueAt(pos) ;
            if (!partial_val) element->Error("partial formal element already associated") ;
            delete partial_val ;
        } else {
            full_value = full_value->ValueAt(pos) ;
        }
    }

    return ;
}

// Begin: Access Value cleanup: (VIPER #4464, 7292)
/* static */ void
VhdlNode::SetDeallocatedValue(VhdlValue const *val)
{
    if (!val || !_access_inner_values) return ;
    if (!_deallocated_access_values) _deallocated_access_values = new Set(POINTER_HASH) ;

    (void) _deallocated_access_values->Insert(val) ;

    Array *elements = val->GetValues() ;
    VhdlValue *ele ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(elements, i, ele) SetDeallocatedValue(ele) ;

    (void) _access_inner_values->Insert(val) ;
}

/* static */ void
VhdlNode::SetInnerAccessValue(VhdlValue const *val)
{
    if (!val || !_access_inner_values) return ;
    (void) _access_inner_values->Insert(val) ;
}

/* static */ unsigned
VhdlNode::IsDeallocatedValue(VhdlValue const *val)
{
    if (!val || !_deallocated_access_values) return 0 ;
    if (_deallocated_access_values->GetItem(val)) return 1 ;

    if (val->IsAccessValue()) return IsDeallocatedValue(val->GetAccessedValue()) ;

    // No need to check its sub-elements if this holds no access-value
    if (!val->HasAccessValue()) return 0 ;

    Set elements(POINTER_HASH) ;
    CollectAllValues(val, &elements) ;

    (void) elements.Remove(val) ;

    SetIter si ;
    VhdlValue *ele = 0 ;
    FOREACH_SET_ITEM(&elements, si, &ele) {
        if (IsDeallocatedValue(ele)) return 1 ;
    }

    return 0 ;
}

/* static */ void
VhdlNode::FreeAccessValueSet(Set *inner_value_set)
{
    Set free_items(POINTER_HASH) ;

    SetIter si ;
    VhdlValue *val = 0 ;
    FOREACH_SET_ITEM(inner_value_set, si, &val) {
        if (free_items.GetItem(val)) continue ;
        CollectAllValues(val, &free_items, 1) ;
        delete val ;
    }

    delete inner_value_set ;
}

/* static */ void
VhdlNode::ResetAccessValueSet()
{
    // everything inside here are already part of _access_inner_values
    delete _deallocated_access_values ;
    _deallocated_access_values = 0 ;

    // following is referring to the set in architecture body
    _access_inner_values = 0 ;
}

/* static */ void
VhdlNode::CollectAllValues(VhdlValue const *val1, Set *elements, unsigned stop_at_access /* = 0 */)
{
    if (!val1 || !elements) return ;

    if (!elements->Insert(val1)) return ;
    if (val1->IsAccessValue() && stop_at_access) return ;

    const VhdlValue *val = val1->IsAccessValue() ? val1->GetAccessedValue() : val1 ;
    if ((val != val1) && !elements->Insert(val)) return ;

    Array *values = val ? val->GetValues(): 0 ;
    VhdlValue *ele ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(values, i, ele) CollectAllValues(ele, elements, stop_at_access) ;
}
// End: Access Value cleanup: (VIPER #4464, 7292)

/////////////// path_name and instance_name attribute handling ////////////////
void VhdlNode::ClearAttributeSpecificPath()
{
    // Remove whatever strings are still on the stack :
    unsigned i ;
    char *item ;
    FOREACH_ARRAY_ITEM(_path_name, i, item) Strings::free(item) ;
    delete _path_name ;
    _path_name = 0 ;

    FOREACH_ARRAY_ITEM(_instance_name, i, item) Strings::free(item) ;
    delete _instance_name ;
    _instance_name = 0 ;
}
unsigned VhdlNode::IsEmptyAttributeSpecificPath()
{
    if (!_path_name || !_path_name->Size()) return 1 ;
    return 0 ;
}
void VhdlNode::PushAttributeSpecificPath(const char *label, const char *entity, const char *arch, const char *loop_value)
{
    if (!_path_name) _path_name = new Array() ;
    if (!_instance_name) _instance_name = new Array() ;

    char *path_name_ele = 0, *instance_name_ele = 0 ;
    // For attribute instance_path, each element can be
    // 1. [component_instantiation_label @] entity_simple_name ( architecture_simple_name)
    // 2. block label
    // 3. generate_label [(literal)]
    // 4. process label
    // 5. subprogram_simple_name

    // For attribute path_name, each element can be
    // 1. component_instantiation_label
    // 2. entity_simple_name
    // 3. block label
    // 4. generate_label [(literal)]
    // 5. process label
    // 6. subprogram_simple_name
    if (loop_value) {
        path_name_ele = label ? Strings::save(label, "(", loop_value, ")"): Strings::save("(", loop_value, ")") ;
        instance_name_ele = Strings::save(path_name_ele) ;
    } else if (label && arch && entity) {
        path_name_ele = Strings::save(label) ;
        instance_name_ele = Strings::save(label, "@", entity, "(", arch, ")") ;
    } else if (label) {
        path_name_ele = Strings::save(label) ;
        instance_name_ele = Strings::save(label) ;
    } else if (entity) {
        path_name_ele = Strings::save(entity) ;
        instance_name_ele = arch ? Strings::save(entity, "(", arch, ")") : Strings::save(entity) ;
    }
    _path_name->InsertLast(path_name_ele) ;
    _instance_name->InsertLast(instance_name_ele) ;
}
void VhdlNode::PopAttributeSpecificPath()
{
    VERIFIC_ASSERT(_path_name && _path_name->Size() && _instance_name && _instance_name->Size()) ;
    // Remove the last item :
    char *last = (char*)_path_name->RemoveLast() ;
    Strings::free( last ) ;
    // Remove the entire array if its empty now :
    if (!_path_name->Size()) {
        delete _path_name ;
        _path_name = 0 ;
    }
    last = (char*)_instance_name->RemoveLast() ;
    Strings::free( last ) ;
    // Remove the entire array if its empty now :
    if (!_instance_name->Size()) {
        delete _instance_name ;
        _instance_name = 0 ;
    }
}
// Return the path_name and instance_name attributes :
char *VhdlNode::PathNameAttribute(const VhdlIdDef *id, unsigned instance_name, unsigned mark_entity)
{
    // Find the declaration scope of 'id', we need to create path name for its declarative area :
    VhdlScope *curr_scope = _present_scope ; // _present_scope is the current scope pointer
    VhdlScope *decl_scope = 0 ;

    if (id && id->IsConfiguration()) id = id->GetEntity() ;

    if (!id) return 0 ;

    // Find the scope where 'id' is declared while traversing upwards in the scope
    // starting from present scope :
    unsigned offset = 0 ;
    unsigned find = 0 ;
    Set ids(POINTER_HASH) ;
    while (curr_scope) {
        ids.Reset() ;
        VhdlIdDef *find_id = curr_scope->FindAllLocal(id->Name(), ids) ;
        SetIter si ;
        FOREACH_SET_ITEM(&ids, si, &find_id) {
            // Consider specification specific identifier for subprogram also
            VhdlSubprogramBody *body = find_id ? find_id->Body(): 0 ;
            VhdlSpecification *spec = body ? body->GetSubprogramSpec(): 0 ;
            VhdlIdDef *spec_id = (spec) ? spec->GetId(): 0 ;
            if ((find_id == id) || (spec_id && spec_id == id)) {
                decl_scope = curr_scope ;
                find = 1 ;
                break ;
            }
        }
        if (find) break ;
        offset++ ;
        curr_scope = curr_scope->Upper() ;
    }
    if (!decl_scope) {
        offset = 0 ;
        // 'id' may be declared in the package, use compile flag 'VHDL_ID_SCOPE_BACKPOINTER'
        // to get the declaration scope :
#ifdef VHDL_ID_SCOPE_BACKPOINTER
        decl_scope = id->GetOwningScope() ;
#else
        decl_scope = _present_scope ;
#endif
    }
    if (!decl_scope) return 0 ;
    Array *path = instance_name ? _instance_name : _path_name ;
    if (!path) return 0 ;

    // If 'id' is declared within package, create name now from scope. scope offset
    // will not give us proper result, as current path stack is not the stack of package.
    VhdlIdDef *container = decl_scope->GetContainingPrimaryUnit() ;
    unsigned i ;
    char *item, *tmp ;
    char *result = 0 ;
    if (container && container->IsPackage()) { // Identifier is declared in package
        // Path_name will be :library_name:package_name:object_name
        Set scopes(POINTER_HASH, 3) ;
        VhdlScope *scope = decl_scope ;
        const char *lib_name = 0 ;
        VhdlIdDef *owner = 0 ;
        while (scope) {
            owner = scope->GetOwner() ;
            if (owner && owner->IsPackage()) {
                VhdlPrimaryUnit *unit = owner->GetPrimaryUnit() ;
                VhdlLibrary *lib = (unit) ? unit->GetOwningLib(): 0 ;
                lib_name = lib ? lib->Name(): 0 ;
            }
            (void) scopes.Insert(scope) ;
            scope = scope->Upper() ;
        }
        // Create a path upto the declaration scope of object :
        SetIter si ;
        FOREACH_SET_ITEM_BACK(&scopes, si, &scope) {
            owner = scope ? scope->GetOwner(): 0 ;
            if (!owner) continue ;
            if (!result) {
                result = Strings::save(":", lib_name, ":", owner->Name()) ;
                continue ;
            }
            tmp = result ;
            result = Strings::save(tmp, ":", owner->Name()) ;
            Strings::free(tmp) ;
        }
    } else { // 'id' not declared within package, use stack offset from scope to create path_name
        // 'id' is declared 'offset' level upper from current scope, so we need to
        // take off last 'offset' elements from current stack to get path_name for 'id'
        unsigned size = path->Size() - offset ;

        // Create path name upto declaration scope of 'id'
        for (i = 0; i < size; i++) {
            item = (path->Size()>i) ? (char*)path->At(i): 0 ;
            if (!result) {
                result = Strings::save(":", item) ;
                continue ;
            }
            tmp = result ;
            result = item ? Strings::save(tmp, ":", item) : Strings::save(tmp, ":") ;
            Strings::free(tmp) ;
        }
        // Path_name attribute value will be unique for every path from top level. So
        // we need to make the units using this attribute unique i.e. if entity using path_name
        // is instantiated multiple times, we need to copy that entity multiple times
        if (mark_entity) PropagateAffectOfPathName() ;
    }
    tmp = result ;

    if (id->IsDesignUnit() || id->IsLabel()) {
        // If we need attribute value for design unit/label, so not include identifier again
        // It is already included while creating the path stack
        result = (tmp) ?  ((id->IsDesignUnit()) ? Strings::save(tmp, ":") : Strings::save(tmp)) : Strings::save(":", id->Name(), ":") ;
    } else { // include identifier on which attribute is called
        result = (tmp) ?  Strings::save(tmp, ":", id->Name()): Strings::save(":", id->Name()) ;
    }
    Strings::free(tmp) ;
    return result ;
}
// Get the present path i.e. whole path stack
char *VhdlNode::GetCurrAttributeSpecificPath(const char *unit_name)
{
    if (!unit_name) return 0 ;
    unsigned size = (_path_name) ? _path_name->Size(): 0 ;
    char *tmp = 0 ; char *result = 0 ; char *item = 0 ;
    unsigned i ;

    // Create path name upto declaration scope of 'id'
    for (i = 0; i < size; i++) {
        item = (_path_name->Size()>i) ? (char*)_path_name->At(i): 0 ;
        if (!result) {
            result = Strings::save(":", item) ;
            continue ;
        }
        tmp = result ;
        result = item ? Strings::save(tmp, ":", item) : Strings::save(tmp, ":") ;
        Strings::free(tmp) ;
    }
    tmp = result ;

    result = (tmp) ?  Strings::save(tmp, ":") : Strings::save(":", unit_name, ":") ;
    Strings::free(tmp) ;
    return result ;
}
// Mark the containing primary unit, so that for other instances of this unit,
// unit can be uniquified :
void VhdlNode::PropagateAffectOfPathName()
{
    if (!_present_scope) return ;
    VhdlIdDef *container = _present_scope->GetContainingPrimaryUnit() ;
    VhdlPrimaryUnit *u = (container) ? container->GetPrimaryUnit(): 0 ;
    if (u) u->SetAffectedByPathName() ;
    VhdlDesignUnit *org = u ? u->GetOriginalUnit(): 0 ;
    if (org) org->SetAffectedByPathName() ;
}
// Create path stack for subprogram, so that this stack can be extended/used during
// elaboration of subprogram body :
void VhdlNode::CreateSubprogramPathName(const VhdlIdDef *subprog_id)
{
    if (!subprog_id) return ;

    // path_name and instance_name attributes of subprogram depend upon the location
    // where subprogram is declared, it does not depend on the path from where it is
    // called. So we cannot just append subprogram name in the current stack. We have
    // already stored the size of stack when subprogram was hit during declarative part
    // elaboration of design unit. We will now create new stack for subprogram so that
    // path_name/instance_name attributes within subprogram body can be evaluated correctly.
    VhdlScope *scope = subprog_id->LocalScope() ;
    VhdlIdDef *container = scope ? scope->GetContainingPrimaryUnit(): 0 ;

    if (container && container->IsPackage()) {
        // Subprogram is within package, we need to create path_name now :
        _path_name = 0 ; _instance_name = 0 ; // Reset previous stacks
        Set scopes(POINTER_HASH, 3) ;
        const char *lib_name = 0 ;
        VhdlIdDef *owner = 0 ;
        while (scope) {
            owner = scope->GetOwner() ;
            if (owner && owner->IsPackage()) {
                VhdlPrimaryUnit *unit = owner->GetPrimaryUnit() ;
                VhdlLibrary *lib = (unit) ? unit->GetOwningLib(): 0 ;
                lib_name = lib ? lib->Name(): 0 ;
            }
            (void) scopes.Insert(scope) ;
            scope = scope->Upper() ;
        }
        // First push the library name :
        PushAttributeSpecificPath(lib_name) ;
        SetIter si ;
        FOREACH_SET_ITEM_BACK(&scopes, si, &scope) { // Now package name
            owner = scope ? scope->GetOwner(): 0 ;
            if (owner) PushAttributeSpecificPath(owner->Name()) ;
        }
    } else {
        if (!_path_name || !_instance_name) return ;
        // Subprogram is within declarative region of architecture/block etc.
        // Create path name considering paths upto 'stack_pos'

        // Find the scope where subprogram is declared traversing upwards from current
        // scope. This will tell us how many elements of current stack are to be considered
        // to create path of subprogram
        VhdlScope *curr_scope = _present_scope ;
        unsigned offset = 0 ;
        unsigned find = 0 ;
        Set ids(POINTER_HASH) ;
        while (curr_scope) {
            ids.Reset() ;
            VhdlIdDef *find_id = curr_scope->FindAllLocal(subprog_id->Name(), ids) ;
            SetIter si ;
            FOREACH_SET_ITEM(&ids, si, &find_id) {
                VhdlSubprogramBody *body = find_id ? find_id->Body(): 0 ;
                VhdlSpecification *spec = body ? body->GetSubprogramSpec(): 0 ;
                VhdlIdDef *spec_id = (spec) ? spec->GetId(): 0 ;
                if ((find_id == subprog_id) || (spec_id && spec_id == subprog_id)) {
                    find = 1 ;
                    break ;
                }
            }
            if (find) break ;
            offset++ ;
            curr_scope = curr_scope->Upper() ;
        }
// CARBON_BEGIN
        if ( find && (_path_name->Size() < offset) ) find = 0; // insufficient information in _path_name to create new _path_name
// CARBON_END
        Array *new_path_name = new Array(_path_name->Size()) ;
        Array *new_instance_name = new Array(_instance_name->Size()) ;
        if (find) {
            const char *item ;
            unsigned i ;
            unsigned stack_pos = _path_name->Size() - offset ;
            for (i = 0; i < stack_pos; i++) {
                item = (_path_name->Size()>i) ? (const char*)_path_name->At(i): 0 ;
                new_path_name->InsertLast(item ? Strings::save(item): 0) ;
                item = (_instance_name->Size()>i) ? (const char*)_instance_name->At(i): 0 ;
                new_instance_name->InsertLast(item ? Strings::save(item): 0) ;
            }
            //_path_name = new_path_name ;
            //_instance_name = new_instance_name ;
        } else { // Subprogram not found
            // This can happen when subprogram is defined within a nested package and
            // we are now processing 'process_statement', 'block_statement' etc
            // 1. Populate a set traversing from subprogram scope to upward direction
            Set scope_list(POINTER_HASH) ;
            scope = subprog_id->LocalScope() ;
            while (scope) {
                (void) scope_list.Insert(scope) ;
                scope = scope->Upper() ;
            }
            // 2. Now traverse from current processing scope to upward and in each step find
            // whether that is a scope in the upward path of subprogram scope
            curr_scope = _present_scope ;
            offset = 0 ;
            find = 0 ;
            VhdlScope *matching_container = 0 ;
            while (curr_scope) {
                if (scope_list.Get(curr_scope)) {
                    matching_container = curr_scope ;
                    find = 1 ;
                    break ;
                }
                offset++ ;
                curr_scope = curr_scope->Upper() ;
            }
// CARBON_BEGIN
            if ( find && (_path_name->Size() < (offset-1)) ) find = 0; // insufficient information in _path_name to create new _path_name
// CARBON_END

            if (find) {
                // So path of subprogram will be
                // 1. '_path_name->Size() - offset' number of elements from current stack
                // 2. Append with that path name from scope 'matching_container' to subprogram's parent scope
                //Array *new_path_name = new Array(_path_name->Size()) ;
                //Array *new_instance_name = new Array(_instance_name->Size()) ;
                const char *item ;
                unsigned i ;
                unsigned stack_pos = _path_name->Size() - (offset-1) ;
                for (i = 0; i < stack_pos; i++) {
                    item = (_path_name->Size()>i) ? (const char*)_path_name->At(i): 0 ;
                    new_path_name->InsertLast(item ? Strings::save(item): 0) ;
                    item = (_instance_name->Size()>i) ? (const char*)_instance_name->At(i): 0 ;
                    new_instance_name->InsertLast(item ? Strings::save(item): 0) ;
                }
                scope = subprog_id->LocalScope() ;
                VhdlScope *parent = (scope) ? scope->Upper(): 0 ; // Parent scope of subprogram
                unsigned start_pos = scope_list.IndexOfItem(matching_container) ;
                unsigned end_pos = scope_list.IndexOfItem(parent) ;
                for (i = start_pos-1; i >= end_pos; i--) {
                    scope = (VhdlScope*)scope_list.GetAt(i) ;
                    VhdlIdDef *owner = scope ? scope->GetOwner(): 0 ;
                    if (!owner) continue ;
                    new_path_name->InsertLast(Strings::save(owner->Name())) ;
                    new_instance_name->InsertLast(Strings::save(owner->Name())) ;
                }
                //_path_name = new_path_name ;
                //_instance_name = new_instance_name ;
            }
        }
        _path_name = new_path_name ;
        _instance_name = new_instance_name ;
    }
    PushAttributeSpecificPath(subprog_id->Name()) ; // Lastly subprogram name
}

// Concat two arguments to make a legal vhdl identifier
char *VhdlTreeNode::ConcatAndCreateLegalIdentifier(const char *prefix, const char *suffix)
{
    if (!prefix || !suffix) return 0 ; // Nothing to concat

    char *name = 0 ;
    char *tmp = Strings::save(prefix) ;
    unsigned already_escaped = 0 ;
    if (tmp[0] == '\\' && tmp[Strings::len(tmp) - 1] == '\\') { // Remove escape characters from prefix
        char *copy_name = tmp ;
        char *tmp1 = copy_name ;
        tmp1[Strings::len(tmp1) - 1 ] = '\0' ;
        tmp1++ ;
        tmp = Strings::save(tmp1) ;
        Strings::free(copy_name) ;
        already_escaped = 1 ; // VIPER #7565 : Store information that it is already escaped
    }

    name = Strings::save(tmp, suffix) ;
    Strings::free(tmp) ;
    if (already_escaped) {
        // VIPER #7565 : Create escape identifier if original name is escaped
        tmp = name ;
        name = Strings::save("\\", name, "\\") ;
        Strings::free(tmp) ;
    } else if (!VhdlTreeNode::IsLegalIdentifier(name)) {
        // Create escaped name to make the name of copied entity legal
        tmp = name ;
        name = VhdlTreeNode::CreateLegalIdentifier(name) ;
        Strings::free(tmp) ;
    }
    return name ;
}

// VIPER #6032/6038 : Clean config map
void VhdlTreeNode::CleanConfigNumMap()
{
    delete _config_num_map ;
    _config_num_map = 0 ;
}
void VhdlTreeNode::CleanUnitNumMap()
{
    MapIter mi ; char *name ;
    FOREACH_MAP_ITEM(_unit_num_map, mi, &name, 0) Strings::free(name) ;
    delete _unit_num_map ; _unit_num_map = 0 ;
}
void VhdlTreeNode::CleanLong2ShortMap()
{
    MapIter mi ;
    char *key, *value ;
    FOREACH_MAP_ITEM(_long2short, mi, &key, &value) {
        Strings::free(key) ;
        Strings::free(value) ;
    }
    delete _long2short ; _long2short = 0 ;
}
