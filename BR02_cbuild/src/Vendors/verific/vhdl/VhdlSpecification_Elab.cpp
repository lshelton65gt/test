/*
 *
 * [ File Version : 1.48 - 2014/01/02 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "VhdlSpecification.h" // Compile flags are defined within here

#include "Array.h"
#include "Map.h"
#include "Set.h"
#include "Strings.h"

#include "vhdl_file.h"
#include "vhdl_tokens.h"
#include "VhdlIdDef.h"
#include "VhdlName.h"
#include "VhdlIdDef.h"
#include "VhdlConfiguration.h"
#include "VhdlDeclaration.h"
#include "VhdlStatement.h"
#include "VhdlMisc.h"
#include "VhdlScope.h"

#include "VhdlValue_Elab.h"
#include "VhdlDataFlow_Elab.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

/**********************************************************/
// Set an attribute
/**********************************************************/

void VhdlSpecification::SetAttribute(VhdlIdDef * /*attr*/, VhdlValue * /*value*/)
{
    // Don't think this can happen, but don't kill me if it does..
    // VERIFIC_ASSERT(0) ;
}

void VhdlEntitySpec::SetAttribute(VhdlIdDef *attr, VhdlValue *value)
{
    if (!attr || !value) return ;

    // Set attribute on all identifiers in _all_ids (will be set by now)
    VhdlIdDef *id ;
    SetIter si ;
    FOREACH_SET_ITEM(_all_ids, si, &id) {
        id->SetAttribute(attr, value) ; // makes value copy
    }
}

/**********************************************************/
// Elaboration
/**********************************************************/

// "Elaborate" is called only once ; when the function/procedure declaration is elaborated.
// We don't do much :
// We just set the constraint on the subprogram id (functions only),
// so that a constraint can be returned before the function is called
// (Needed for indexed function results).
//
// We can also check here and now if the subprogram has a body. It should, since
// analysis is completely done (so spec's in package headers must have a body).
//

void
VhdlSpecification::Elaborate(VhdlDataFlow * /*df*/)
{
    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt
    // Do nothing
}

void
VhdlProcedureSpec::Elaborate(VhdlDataFlow * /*df*/)
{
    VERIFIC_ASSERT(_designator) ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Check that the procedure has a body now :
    if (!_designator->Body()) {
        _designator->Error("subprogram %s does not have a body", _designator->Name()) ;
    }
}

void
VhdlFunctionSpec::Elaborate(VhdlDataFlow *df)
{
    VERIFIC_ASSERT(_designator && _return_type) ;

    if (vhdl_file::CheckForInterrupt(Linefile())) return ; // Stop after an error or interrupt

    // Check that the procedure has a body now :
    if (!_designator->Body()) {
        if (!_formal_parameter_list && Strings::compare(_designator->Name(),"now")) {
            // Ignore function 'now' (in standard package). It has built-in behavior.
        } else {
            _designator->Error("subprogram %s does not have a body", _designator->Name()) ;
        }
    }

    // First elaborate packages that this scope depends on (LRM 12.1) :
    if (_local_scope) _local_scope->ElaborateDependencies() ;

    // Set constraint (even if unconstrained) on function id :
    VhdlConstraint *constraint = _return_type->EvaluateConstraintInternal(df, 0) ;
    _designator->SetConstraint(constraint) ;
}

/**********************************************************/
// Initialize constraints for a subprogram
/**********************************************************/

void
VhdlSpecification::InitializeConstraints(Map &/*formal_to_constraint*/) { }

void
VhdlFunctionSpec::InitializeConstraints(Map &formal_to_constraint)
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, decl) {
        decl->InitializeConstraints(formal_to_constraint) ;
    }

    // Set the constraint of the result
    VERIFIC_ASSERT(_return_type) ;
    VhdlConstraint *constraint = (_return_type) ? _return_type->EvaluateConstraintInternal(0, 0) : 0 ;
    (void) formal_to_constraint.Insert(_designator, constraint) ;
}

void
VhdlProcedureSpec::InitializeConstraints(Map &formal_to_constraint)
{
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, decl) {
        decl->InitializeConstraints(formal_to_constraint) ;
    }
}

/**********************************************************/
// Initialize values of subprogram (after actuals are associated)
/**********************************************************/

void VhdlSpecification::InitialValues(VhdlDataFlow * /* df */, VhdlScope * /*assoc_list_scope*/) { }

void VhdlFunctionSpec::InitialValues(VhdlDataFlow *df, VhdlScope *assoc_list_scope)
{
    // Check / Set all formals with a value
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    //FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        //if (decl) decl->InitialValues(0, 0, df) ;
    //}
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, decl) {
        decl->InitialValues(0, 0, df, assoc_list_scope) ;
    }
}

void VhdlProcedureSpec::InitialValues(VhdlDataFlow *df, VhdlScope *assoc_list_scope)
{
    // Check / Set all formals with a value
    unsigned i ;
    VhdlInterfaceDecl *decl ;
    //FOREACH_ARRAY_ITEM(_generic_clause, i, decl) {
        //if (decl) decl->InitialValues(0, 0, df) ;
    //}
    FOREACH_ARRAY_ITEM(_formal_parameter_list, i, decl) {
        decl->InitialValues(0, 0, df, assoc_list_scope) ;
    }
}

/**********************************************************/
// Generic handling for subprogram
/**********************************************************/
void VhdlSpecification::AssociateGenerics(Array *actual_generics, Map *generic_to_constraint, Map *generic_to_value, Map *generic_to_id, Array *all_generics) const
{
    VhdlIdDef *subprog_id = GetDesignator() ;
    if (!subprog_id) return ;

    // VIPER 2334 : Set the global pointer to this scope
    VhdlScope *old_scope = _present_scope ;
    _present_scope = LocalScope() ;

    // VIPER #3367: We are going to process something in the time frame 0, set this flag:
    VhdlDataFlow df(0, 0, LocalScope()) ;
    df.SetInInitial() ;

    VhdlDiscreteRange *assoc ;
    VhdlValue *val ;
    VhdlIdDef *id, *gen_id ;
    VhdlConstraint *constraint ;

    Array *generics = actual_generics ? actual_generics : GetGenericMapAspect() ;
    unsigned i, j ;
    VhdlInterfaceDecl *decl ;
    Array *generic_clause = GetGenericClause() ;
    // VHDL-2008 LRM section 6.5.6.1 says 'A name that denotes an interface
    // declaration in a generic interface list may appear in an interface
    // declaration within the interface list containing the denoted interface
    // declaration'
    // To support the above, change the flow of generic association. We need to
    // consider one interface declaration at a time and perform
    // 1) Initialization of constraint
    // 2) Evaluate and associate actual
    // 3) Evaluate initial value if actual not present
    // After the above 3 steps we need to consider next interface declaration.
    // So first create a map containing generic id vs its actual from 'actual_generics'
    Map actuals(POINTER_HASH) ;
    FOREACH_ARRAY_ITEM(generics, i, assoc) {
        if (!assoc) continue ;

        if (assoc->IsAssoc()) {
            id = assoc->FindFormal() ;
        } else {
            // positional association
            id = subprog_id->GetGenericAt(i) ;
        }
        if (id) (void) actuals.Insert(id, assoc, 0, 1 /* for partial assoc*/) ;
    }

    // Collect all generics from generic_clause
    FOREACH_ARRAY_ITEM(generic_clause,i,decl) {
        if (!decl) continue ;
        Array *ids = decl->GetIds() ;
        if (ids && ids->Size()) {
            if (all_generics) all_generics->Append(ids) ;
        } else {
            id = decl->GetId() ;
            if (id && all_generics) all_generics->InsertLast(id) ;
        }
    }
     
    // Set generics using actual generics or generic map aspect

    // Set value/constraint of instantiating unit in maps
    SwitchValueConstraintOfGenerics(all_generics, generic_to_constraint, generic_to_value, generic_to_id) ;
    FOREACH_ARRAY_ITEM(generic_clause,i,decl) {
        if (!decl) continue ;
        // First initialize constraints of generics
        decl->Initialize() ; // Constraints only

        // To evaluate the actual set value/constraint of containing unit in ids
        SwitchValueConstraintOfGenerics(all_generics, generic_to_constraint, generic_to_value, generic_to_id) ;

        // Get the identifiers from generic decl
        Array *ids = decl->GetIds() ;
        Array tmp_ids(2) ;
        if (ids && ids->Size()) { // For value generics
            tmp_ids.Append(ids) ;
        } else { // For VHDL-2008 specific type/package/subprogram generic
            gen_id = decl->GetId() ;
            tmp_ids.InsertLast(gen_id) ;
        }
        FOREACH_ARRAY_ITEM(&tmp_ids, j, gen_id) { // Iterate over ids
            if (!gen_id) continue ;
            MapItem *item ;
            FOREACH_SAME_KEY_MAPITEM(&actuals, gen_id, item) { // Get proper actual
                assoc = item ? (VhdlDiscreteRange*)item->Value(): 0 ;
                if (!assoc) continue ;

                if (assoc->IsAssoc()) {
                    id = assoc->FindFullFormal() ;
                    if (!id) { // Partial association
                        // If formal is not constrained, set proper constraint to it
                        VhdlNode::EvaluateConstraintForPartialAssoc(actual_generics, assoc, &df) ;
                        assoc->PartialAssociation(0, 0) ;
                        continue ;
                    }
                } else {
                    // positional association
                    id = gen_id ; //subprog_id->GetGenericAt(i) ;
                }
                if (!id) continue ; // type-inference checks failed

                constraint = (generic_to_constraint) ? (VhdlConstraint*)generic_to_constraint->GetValue(id): 0 ; // Formal-constraint
                if (id->IsGenericType()) {
                    // VIPER #5918 : Formal is interface_type, we need to calculate constraint of it from
                    // actual and it cannot have any value
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    constraint = actual_part ? actual_part->EvaluateConstraintInternal(0, 0):0 ;
                    if (constraint) {
                        if (generic_to_constraint && generic_to_value) {
                            (void) generic_to_constraint->Insert(id, constraint, 1) ;
                            (void) generic_to_value->Insert(id, 0, 1) ;
                        } else {
                            id->SetConstraint(constraint) ;
                            id->SetValue(0) ;
                        }
                    }
                    VhdlIdDef *actual_type = actual_part ? actual_part->TypeInfer(0, 0, VHDL_range, 0): 0 ;
                    if (!actual_type) continue ;
                    if (generic_to_id) {
                        (void) generic_to_id->Insert(id, actual_type, 1) ;
                    } else {
                        id->SetActualId(actual_type, actual_part ? actual_part->GetAssocList() : 0) ;
                    }
                    continue ;
                }
                if (id->IsSubprogram()) {
                    // Formal is generic subprogram, actual can only be subprogram name which
                    // conforms with formal
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_subprogram = actual_part ? actual_part->SubprogramAspect(id): 0 ;
                    if ((!actual_subprogram && actual_part) || (actual_subprogram && !actual_subprogram->IsSubprogram())) {
                        assoc->Error("subprogram name should be specified as actual for formal generic subprogram %s", id->Name()) ;
                    }
                    if (actual_subprogram && !id->IsHomograph(actual_subprogram)) {
                        assoc->Error("subprogram %s does not conform with its declaration", actual_subprogram->Name()) ;
                    }
                    if (!actual_subprogram) continue ;
                    if (generic_to_id) {
                        (void) generic_to_id->Insert(id, actual_subprogram, 1) ;
                    } else {
                        id->SetActualId(actual_subprogram, 0) ;
                    }
                    continue ;
                }
                if (id->IsPackage()) {
                    // Formal is a generic package, actual can be a package
                    VhdlExpression *actual_part = assoc->ActualPart() ; // Get actual
                    VhdlIdDef *actual_package = actual_part ? actual_part->EntityAspect(): 0 ;
                    if ((!actual_package && actual_part) || (actual_package && !actual_package->IsPackage())) {
                        assoc->Error("package name should be specified as actual for formal generic package %s", id->Name()) ;
                    }
                    if (!actual_package) continue ;
                    if (generic_to_id) {
                        VhdlIdDef *elab_pkg = actual_package->GetElaboratedPackageId() ;
                        (void) generic_to_id->Insert(id, elab_pkg? elab_pkg: actual_package, 1) ;
                    } else {
                        id->SetElaboratedPackageId(actual_package) ;
                    }
                    continue ;
                }

                if (!constraint) continue ; // something bad happened in initializatio

                _present_scope = old_scope ;
                val = assoc->Evaluate(constraint, &df, 0) ; // Evaluate actuals
                _present_scope = LocalScope() ;
                if (!val) continue ;
                // Generics are constant :
                if (!val->IsConstant()) {
                    assoc->Error("value for generic %s is not constant", id->Name()) ;
                    continue ;
                }

                // Translate back to a constant (get rid of any constant NonConstVal's)
                val = val->ToConst(id->Type()) ;

                // LRM 3.2.1.1
                if (constraint->IsUnconstrained()) constraint->ConstraintBy(val) ;

                // Check actual against constraint
                // Viper 5897: Do not call CheckAgainst if the type is vl_logic_vector as we are matching it with integer
                // Viper 5889 and 5909. Call checkAgainst only when both are vector in mixed language flow as we can
                // have integer vs vhdl vector. There are two cases to it 1) when vhdl in verilog and 2) verilog in vhdl
                VhdlIdDef *id_type = id->Type() ;
                if ((!Strings::compare(id_type->Name(), "vl_logic_vector") || (val->IsComposite())) && // For Verilog in Vhdl
                    (!assoc->FromVerilog() || (!id_type->IsArrayType() || val->IsComposite()))) {      // For Vhdl in Verilog
                    if (!val->CheckAgainst(constraint,assoc, &df)) {
                        delete val ;
                        continue ;
                    }
                }

                if (generic_to_value) {
                    (void) generic_to_value->Insert(id, val, 1) ; // Insert actual to map
                } else { // Set value in id
                    id->SetValue(val) ;
                }
            }
        }
        // Now evaluate initial expression :
        // To evaluate the initial set value/constraint of instantiated unit in ids
        SwitchValueConstraintOfGenerics(all_generics, generic_to_constraint, generic_to_value, generic_to_id) ;
        decl->InitialValues(0, 0, &df, old_scope) ;
    }

    // VIPER 2334 : Reset the global scope
    _present_scope = old_scope ;
}

void VhdlSpecification::SwitchValueConstraintOfGenerics(const Array *all_generics, Map *generic_to_constraint, Map *generic_to_value, Map *generic_to_id) const
{
    if (!all_generics) return ;
    unsigned i ;
    VhdlIdDef *gen_id ;
    FOREACH_ARRAY_ITEM(all_generics, i, gen_id) {
        if (!gen_id) continue ;
        // Switch value :
        VhdlValue *val = generic_to_value ? (VhdlValue*)generic_to_value->GetValue(gen_id): 0 ;
        if (generic_to_value) (void) generic_to_value->Insert(gen_id, gen_id->TakeValue(), 1) ;
        gen_id->SetValue(val) ;

        // Switch constraint :
        VhdlConstraint *constraint = generic_to_constraint ? (VhdlConstraint*)generic_to_constraint->GetValue(gen_id): 0 ;
        if (generic_to_constraint) (void) generic_to_constraint->Insert(gen_id, gen_id->TakeConstraint(), 1) ;
        gen_id->SetConstraint(constraint) ;

        // Switch actual id :
        VhdlIdDef *actual_id = generic_to_id ? (VhdlIdDef*)generic_to_id->GetValue(gen_id) : 0 ;
        if (gen_id->IsPackage()) {
            if (generic_to_id) (void) generic_to_id->Insert(gen_id, gen_id->GetElaboratedPackageId(), 1) ;
            gen_id->SetElaboratedPackageId(actual_id) ;
        } else {
            if (generic_to_id) (void) generic_to_id->Insert(gen_id, gen_id->GetActualId(), 1) ;
            gen_id->SetActualId(actual_id, 0) ;
        }
    }
}

