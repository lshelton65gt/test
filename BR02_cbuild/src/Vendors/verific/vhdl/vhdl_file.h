/*
 *
 * [ File Version : 1.154 - 2014/01/09 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_FILE_H_
#define _VERIFIC_VHDL_FILE_H_

#include "VerificSystem.h"
#include "LineFile.h" // for linefile_type
#include "VhdlCompileFlags.h" // Vhdl-specific compile flags
#include "RuntimeFlags.h" // for global variable (runtime flag) set/get.

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

class SaveRestore;
class Map ;
class Set ;
class VhdlLibrary;
class VhdlPrimaryUnit ;
class VhdlIdDef ;
class Array ;
class VhdlDesignUnit ;
class Protect ;
class VhdlTreeNode ;
class Cell ;
class VeriModule ;
class VhdlExpression ;
#ifdef VHDL_FLEX_READ_FROM_STREAM
class verific_stream ;
#endif

#undef VN
#ifdef VERIFIC_NAMESPACE
#define VN Verific // Need to define this to get things working with Verific namespace active
#else
#define VN
#endif

// To iterate over all library unit parse-tree's that were analyzed :
// use FOREACH_VHDL_LIBRARY(MI,LIBRARYTREE) :
//       --------------------------------------------------------------
//         argument       type            function
//       --------------------------------------------------------------
//       MI          :   MapIter         An iterator
//       LIBRARYTREE :   VhdlLibrary *   Returned Pointer to the library parse-tree
//                                       (include Units.h before using member functions)
#define FOREACH_VHDL_LIBRARY(MI,LIBRARYTREE) FOREACH_MAP_ITEM(VN::vhdl_file::AllLibraries(), MI, 0, &(LIBRARYTREE))

// Look in VhdlUnits.h to check iterators and member functions on VhdlLibrary structures

/* -------------------------------------------------------------- */

/*
    vhdl_file is a placeholder to call the VHDL IEEE 1076-1993 RTL reader.
    Typical calling sequence is as follows :

        vhdl_file f ;
        f.Read("vhdl_file_name", "work") ;

    In absence of the 'work_lib' name in the Read command, the library
    call "work" will be used to store the cells defined in the VHDL file.

    The Read command will create hierarchical database objects from all
    objects defined in the source file.  The reader uses the
    Libset::Global() command to find the libraries, cells, and netlists
    that are mentioned in the VHDL file.

    If the reader finds a library, cell, or netlist that is already
    present in the database, it will overwrite the existing database
    object, issuing a warning.  If the reader does not find a library,
    cell, or netlist in the database, it will create a new one.

    The reader creates a parse-tree while reading the VHDL file.  This
    parse-tree is NOT deleted at the end of the Read command, since
    subsequent calls of Read might need the VHDL parse-trees of previously
    parsed units to create a proper result.

    Applications should call the RemoveAllUnits() routine after all VHDL
    files have been read in.  This will clean up all the parse-tree
    structures of previous Read() and Analyze() calls.

    **** IMPORTANT ****

    The VHDL language is defined by syntax and semantic rules, by predefined
    operators and identifiers, but also by a 'standard' VHDL package, which
    must be analyzed in the library 'std'.  There are actually two forms of
    package 'standard' : the VHDL-1987 and the VHDL-1993 version.  The VHDL
    reader does NOT work properly without this package.  Therefore, the
    package 'standard' MUST be read in before any other VHDL file!  Also, it
    MUST be read into library 'std'.  Make sure you have a valid version of
    this package available where you application can find it.  Your customers
    will not read this file themselves, so your application needs to do this
    before any other VHDL file is read in (Preferably at start-up).

    To read in the VHDL 'standard' package, do the following :

        vhdl_file f ;
        f.Analyze("standard.vhd", "std") ;

    If omitted, the reader will complain with errors ("bit" is not declared etc.).

    Also, make sure your application has a way to find other 'standard'
    packages when the user requests it (ie. package IEEE 1164). They also need
    to be stored in the libraries that are subscribed by the standards.

    In addition, de-facto standards (std_logic_arith, std_logic_unsigned etc
    by Synopsys) can be read in (Analyze) as other VHDL files, but make sure
    Synopsys synthesis directives are present to direct the functions.

    The Verific VHDL reader does understand all Synopsys and Exemplar directives
    in the de-facto standard packages, but if they are not there, the generated
    logic will be very inefficient and operators will not be recognized.

    **** Analyze / Elaborate ****

    The Read() routine actually goes through two steps, which can also
    be called separately :

    'Analyze()'   - syntax and most semantic checks will be done, resulting
                    in the creation of a parse-trees (per primary unit)

    'Elaborate()' - takes one (primary unit) parse-tree and executes it. This
                    will create a Netlist object that will be stored in the
                    design database.

    If multiple files are read in, it is preferable to first Analyze() every
    file before calling Elaborate().  This will make sure that primary units
    which are instantiated are actually known to the reader.

    Analyze() parses the entire IEEE 1076-1993/1987 VHDL language and stores
    the result in parse-trees.

    Elaborate() supports only a subset of the VHDL language.  The subset is
    restricted to RTL statements (behavioral and structural), and conforms
    with the subsets parsed by popular logic synthesis tools.  The elaborator
    does NOT do anything with timing information and other 'non-synthesizable'
    constructs in the VHDL file.  For details on the exact subset supported
    in a particular release of the code, please contact us at info@verific.com.

    Read() actually calls Analyze() on a file, followed by ElaborateAll(),
    so EVERY primary unit read in gets elaborated and will create a Netlist
    object in the database.

    **** Line/Filename information ****

    For the logic that is created by the elaborator, every Instance, Port,
    Netlist, and Cell that is created is annotated with LineFile information.
    This will make the line number and file name from where the object was
    created available to applications that operate on the database.  The
    LineFile info is stored in a field of the (DesignObj) object.  We also
    have a compile-time switch which allows column information to also be
    stored in the LineFile object.

    **** Print a parse-tree ****

    PrettyPrint() will dump the parse-tree for a primary unit into a file
    with VHDL syntax.  Since the parse-tree preserves all VHDL file
    information (except for pre-processor statements and comments), the
    pretty-print version will look very similar to the original design.

    **** Pragmas ****

    The VHDL reader does read certain Synopsys/Exemplar synthesis directives
    (full_case/parallel_case) which require a mechanism unlike standard syntax
    parsing.  For that, the Analyze() command uses the Get/SetPragma() routines.
    These routines should not be used by an application.

    The Verific VHDL reader does understand all Synopsys and Exemplar directives
    that define 'short-cuts' for high-level functions.

    **** Access to all analyzed units ****

    If you need to write an application function on the parse-trees, we
    recommend you write it as a member function of the parse-tree object
    classes.  We also provide a Visitor base class (a Visitor is a well
    known SW design for traversing data-structures) from which you can
    derive from.

    However, if you just need 'shallow' access to the analyzed unit
    parse-trees (for example, to find out which units have been analyzed,
    or what their names are), there are some accessor methods.

    Verific stores analyzed parse-trees per design units (entity, architecture,
    package, configuration and package body) into a structure  of multiple
    libraries.  Libraries contain primary units, who in turn contain secondary
    units.

    To get a handle to a library (of unit parse-trees) by name, use the
    (static) function GetLibrary().  For example, to see if the standard
    package (or any vhdl file) is already analyzed in the library "std",
    do this :

        VhdlLibrary *lib = vhdl_file::GetLibrary("std") ;

    If 'lib' is NULL, there is nothing analyzed in the 'std' library.
    The script 'tclmain/start.tcl' can be used to analyze all (defacto)
    standard VHDL packages.

    You can get the (last set) work library with this routine :

        VhdlLibrary *lib = vhdl_file::GetWorkLib() ;

    If you just want to iterate over all libraries that are analyzed, use
    the iterator :

        MapIter mi ;
        VhdlLibrary *lib ;
        FOREACH_VHDL_LIBRARY(mi, lib) {
            // do something with lib
        }

    Once you have a VhdlLibrary* to work with, make sure to include the
    VhdlUnits.h file, so you can actually do something with the VhdlLibrary*.

    For example, the following piece of code iterates over all secondary
    units in all libraries, and prints the name of all architectures that
    were ever analyzed :

        #include "LineFile.h"    // line/file utility
        #include "VhdlUnits.h"   // Get access to internals of VhdlUnits/VhdlLibrary
        #include "VhdlIdDef.h"   // Get access to intermals of VhdlIdDef (identifier parse-tree nodes)
            ...
        MapIter m1, m2, m3 ;
        VhdlLibrary *lib ;
        VhdlPrimaryUnit *primunit ;
        VhdlSecondaryUnit *secunit ;
        FOREACH_VHDL_LIBRARY(m1, lib) {
            // Iterate over all (primary unit) parse-trees that were analyzed
            FOREACH_VHDL_PRIMARY_UNIT(lib, m2, primunit) {
                // Iterate over all (secondary unit) parse-trees analyzed under this primary unit
                FOREACH_VHDL_SECONDARY_UNIT(primunit, m3, secunit) {
                    // Rule out any secondary unit that is not a Architecture :
                    if (!secunit->Id()->IsArchitecture()) continue ;

                    // Print name :
                    ::printf("Architecture %s\n",  secunit->Id()->Name()) ;

                    // Print line/file of where the architecture was declared :
                    linefile_type linefile = secunit->Id()->Linefile() ;
                    if (linefile) {
                        ::printf(" was declared in file %s, line %d\n",
                        LineFile::GetFileName(linefile),
                        LineFile::GetLineNo(linefile)) ;
                    }
                }
            }
        }

    You can get most information you need from analyzed design units by writing
    such application code.  If you need detailed access into the parse-trees,
    please write new member functions on the parse-tree nodes themselves.

    Lastly, the general VHDL API will expand over time, and we always do
    our best to support backward compatibility between releases.
*/
class VFC_DLL_PORT vhdl_file
{
public:
    vhdl_file() { } ;
    ~vhdl_file() { } ;

private:
    // Prevent compiler from defining the following
    vhdl_file(const vhdl_file &) ;            // Purposely leave unimplemented
    vhdl_file& operator=(const vhdl_file &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // VHDL supported dialect enum
    enum { VHDL_93=0, VHDL_87=1, VHDL_2K=2, VHDL_2008=3, VHDL_PSL=6, UNDEFINED=7 } ;

    // Analyze a file (with design unit(s)) into library 'work_lib'
    static unsigned      Analyze(const char *file_name, const char *lib_name="work", unsigned vhdl_mode=VHDL_93) ;

    // VIPER #6576: Analyze given string into a VHDL expression (returns a VhdlExpression):
    // Caller of the function is responsible to call TypeInfer() on the returned expression after setting
    // proper VhdlNode::_present_scope pointer to the scope where this expression is supposed to be used.
    static VhdlExpression *AnalyzeExpr(const char *expr, unsigned vhdl_mode=VHDL_93, const linefile_type line_file=0) ;

    // Top-level vhdl_file reset routine (calls all appropriate reset/cleanup routines)
    static void          Reset() ;

    // Call this API to reset the parser to go to the initial state.
    // It will delete all data-structure (including parse trees) created on vhdl_file.
    // Note that it does not reset flag setting like SetIgnoreTranslateOff().
    // Does not delete the netlist database created with RTL elaboration, if any, either.
    static void          ResetParser() ;

    // Elaborate the indicated unit (entity/configuration) from the top down. Optionally specify architecture and char*generic_name->char*generic_value settings
    // Static elaboration : If elaborated unit is needed after static elaboration,
    // one can use the following API call sequence instead of 'Elaborate' API
    // 1. Get the library whose primary unit is to be elaborated
    // VhdlLibrary *work_lib = vhdl_file::GetLibrary(lib_name,1) ;
    //
    // 2. Get the primary unit to be elaborated from library :
    //  VhdlPrimaryUnit *prim_unit = work_lib-> GetPrimUnit(unit_name) ;
    //
    // 3. Validate generics provided by user : 'generics' is same as
    // passed in vhdl-file::Elaborate routine :
    // Array *actual_generics = (generics) ?  prim_unit->CreateActualGenerics(generics) : 0 ;
    //
    // 4. Elaborate the primary unit : 'architecture_name' can be null.
    // 'elaborated_unit' is the elaborated primary unit :
    // VhdlPrimaryUnit *elaborated_unit = prim_unit->StaticElaborate(architecture_name, actual_generics,0,0,0) ;
    //
    // 5. Delete binding information from elaborated configuration :
    // work_lib->DeleteElaboratedBindings() ;
    static unsigned      Elaborate(const char *unit_name, const char *lib_name="work", const char *architecture_name=0, const Map *generics=0, unsigned static_only=0) ;

    // Elaborate all (not yet elaborated) units (entity/configurations) in work_lib
    static unsigned      ElaborateAll(const char *lib_name="work", unsigned static_only=0) ;

    // VIPER #2762 :-L options.
    // Used to specify libraries where to search instantiated units during elaboration
    static void              AddLOption(const char *libname) ; // -L
    static Set *             GetAllLOptions() { return _ordered_libs ; } // Return set of L options
    static void              RemoveAllLOptions() ; // Clear out -L settings: to be called after each elaborate command
    static VhdlPrimaryUnit * GetUnitFromLOptions(const char *unit_name, unsigned restore = 1, unsigned exclude_verilog = 0) ;  // Get argument specified unit searching in -L options in order

    // VIPER : 2947/4152 - APIs for customer to control/access loop limit
    static unsigned      GetLoopLimit()                 { return RuntimeFlags::GetVar("vhdl_loop_limit") ; }
    static void          SetLoopLimit(unsigned limit)   { (void) RuntimeFlags::SetVar("vhdl_loop_limit", limit) ; }

    // Loop limit variable is sticky across multiple elaboration runs
    // User might set a new value using SetLoopLimit or Reset this variable
    // to use the default limits (by LOOPLIMIT #defines)
    static void          ResetLoopLimit()               { (void) RuntimeFlags::SetVar("vhdl_loop_limit", 0) ;}

    // Viper #4950 : dynamic control for error message : array size is
    // larger than 2**_max_array_size (VHDL-1525)
    static void          SetMaxArraySize(unsigned size) { (void) RuntimeFlags::SetVar("vhdl_max_array_size", size) ; }
    static unsigned      GetMaxArraySize()              { return RuntimeFlags::GetVar("vhdl_max_array_size") ; }

    // VIPER #7125: APIs for customer to control/access stack limit:
    static unsigned      GetMaxStackDepth()                { return RuntimeFlags::GetVar("vhdl_max_stack_depth") ; }
    static void          SetMaxStackDepth(unsigned depth)  { (void) RuntimeFlags::SetVar("vhdl_max_stack_depth", depth) ; }

    static unsigned      Copy(const char *unit_name, const char *from_library_name , const char *to, unsigned save_restore) ;

    // Pretty-print an analyzed design unit
    static unsigned      PrettyPrint(const char *file_name, const char *unit_name, const char *lib_name="work") ;

    // Clean up the parse-trees
    static unsigned      RemoveUnit(const char *lib_name, const char *unit_name) ;       // Cleans out specified parse-tree
    static void          RemoveAllUnits(unsigned packages_too=1) ;                       // Cleans out ALL analyzed parse-trees (including all packages if 'packages_too' is 1)
    static void          RemoveAllUnits(const char *lib_name, unsigned packages_too=1) ; // Cleans out ALL analyzed parse-trees in specified library (including all packages if 'packages_too' is 1)
    // The following routines should eventually be removed, and are only being kept around for backward compatibility.
    // Please use the above Remove* routines instead.
    static unsigned      RemoveModule(const char *lib_name, const char *unit_name)       { return RemoveUnit(lib_name, unit_name) ; }
    static void          RemoveAllModules(unsigned packages_too=1)                       { RemoveAllUnits(packages_too) ; }
    static void          RemoveAllModules(const char *lib_name, unsigned packages_too=1) { RemoveAllUnits(lib_name, packages_too) ; }

    // Analyze/elaborate all units in the file into library 'work_lib' in one command
    static unsigned      Read(const char *file_name, const char *lib_name="work", unsigned vhdl_mode=VHDL_93, unsigned static_only=0) ;

    // Register a function of yours with profile 'unsigned interrupt_func()'
    // which should return 1 if you want Verific to stop executing, 0 if you want us to proceed.
    // We will call this function before elaboration of any statement or module-item,
    // so your registered routine better be fast :
    static void          RegisterInterrupt(unsigned(*interrupt_func)(linefile_type)) ; // Register your interrupt-check routine.
    static unsigned      CheckForInterrupt(linefile_type linefile) ; // Called by verific to check if interrupt is needed (by calling your function)

    // Interrupt the elaborator. This routine issues an 'user-interrupt' error and will exit gracefully
    static void          Interrupt() ;

    static unsigned      IgnoreDeadAssignment() { return _ignore_dead_assignment ; } // Viper #5700

    static void          SetIgnoreDeadAssignment(unsigned value) { _ignore_dead_assignment = value ; } // Viper #5700

    // Register a function of yours with profile 'unsigned compile_as_blackbox(const char *)'
    // which should return 1 if you want the unit by that name to be elaborated as a black-box,
    // and return 0 if you want the unit to be compiled as normal.
    // We will call this function once per unit, at the start of the first elaboration call.
    // So your routine does not need to be fast :
    static void          RegisterCompileAsBlackbox(unsigned(*compile_as_blackbox_func)(const char *)) ;
    static unsigned      CheckCompileAsBlackbox(const char *unit_name) ; // Called by verific to check if unit should be compiled as black-box.
    // The direct (no callback) way of setting an entity to be compiled as black-box is
    // to find the primary unit by iterating over the VhdlLibraries and calling GetPrimUnit(), and
    // then call VhdlPrimaryUnit::SetCompileAsBlackbox() on the primary unit.

    // Control if we should ignore translate_off pragma's during parsing :
    // (1 is ignore, 0 is don't-ignore (skip translate_off->translate_on blocks)
    static void          SetIgnoreTranslateOff(unsigned ignore_pragma) ;
    static void          SetIgnoreAllPragmas(unsigned ignore_pragma) ;
    static unsigned      GetIgnoreAllPragmas(void) ;

    //CARBON_BEGIN
    static Array *       PragmaTriggers() { return _pragma_triggers; } // Array of 'const char*' of the presently active pragma triggers 
    static void          AddPragmaTrigger(const char *pragma) ;
    static unsigned      IsPragmaTrigger(const char *id) ;
    static void          RemoveAllPragmaTriggers() ; // Clear out all pragma triggers
    //CARBON_END
    
    // Return the name of the top-level unit (entity or configuration)
    // that is not yet elaborated. Presently, this just returns the last analyzed unit.
    static const char *  TopUnit(const char *lib_name="work") ;

    // Access to VhdlLibrary parse-trees and VHDL library management.
    // Used by Verific internally. Note that this does not contain the alias libraries.
    static Map *         AllLibraries()   { return _all_libraries ; /* guaranteed to be there */ } // Get all libraries name->VhdlLibrary Map
    static VhdlLibrary * GetWorkLib()     { return _work_lib ; }  // Get the present (last used) work library
    static void          SetWorkLib(VhdlLibrary *l) { _work_lib = l ; } // VIPER #2995 : Set working library
    static VhdlLibrary * GetLibrary(const char *name, unsigned create_if_needed=0) ;
    static void          DeleteLibrary(const char *name) ;

    // VIPER #1777, 1781, 2023 and 3570: VHDL library aliasing:
    static unsigned      SetLibraryAlias(const char *alias_name, const char *target_lib) ;
    static unsigned      UnsetLibraryAlias(const char *alias_name) ;
    static void          UnsetAllLibraryAlias() ;

    // Time resolution...
    // Routine to allow user to set time resolution. The default value of
    // time resolution is "1 fs" as specified in standard package. Using
    // this routine user can change the default time resolution
    static void          SetTimeResolution(int scale /* must be 1, 10 or 100 */, const char *unit /*ms, us, ns, ps etc.. */) ;

    // Returns the user specified scaling factor of time resolution.
    // Default return value is '1' for default resolution "1 fs"
    static int           GetTimeScale()   { return _scale_factor_of_timeunit ; }

    // Returns the user specified time resolution unit.
    // Default return value is "fs" for default resolution "1 fs".
    static const char *  GetTimeUnit()    { return _time_unit ? _time_unit : "fs" ; }

// Runtime switch to control RAM extraction
public:
    static unsigned      ExtractMPRams()                    { return RuntimeFlags::GetVar("vhdl_extract_multiport_rams") ; }
    static void          SetExtractMPRams(unsigned val)     { (void) RuntimeFlags::SetVar("vhdl_extract_multiport_rams",val) ; if (val) (void) RuntimeFlags::SetVar("vhdl_extract_dualport_rams",0) ; } // Run-time switch to extract multi-port RAMs 1==on, 0==off. Make sure that dual- and multi-port ram extraction are not on simultaneously.
    static unsigned      ExtractRams()                      { return RuntimeFlags::GetVar("vhdl_extract_dualport_rams") ; }
    static void          SetExtractRams(unsigned val)       { (void) RuntimeFlags::SetVar("vhdl_extract_dualport_rams",val) ; if (val) (void) RuntimeFlags::SetVar("vhdl_extract_multiport_rams",0) ; } // Run-time switch to extract dual-port RAMs  1==on, 0==off. Make sure that dual- and multi-port ram extraction are not on simultaneously.

    static unsigned      GetMinimumRamSize()                { return RuntimeFlags::GetVar("vhdl_minimum_ram_size") ; } // Viper #3903
    static void          SetMinimumRamSize(unsigned size)   { (void) RuntimeFlags::SetVar("vhdl_minimum_ram_size",size) ; } // Viper #3903

// VIPER #6633: Generate explict state machine
public:
    static unsigned      GenerateExplicitStateMachines(const char *unit_name, const char *lib_name, unsigned reset) ;
    static unsigned      GenerateExplicitStateMachine(VhdlPrimaryUnit *unit, unsigned reset) ;

#ifdef VHDL_FLEX_READ_FROM_STREAM
    // VIPER #5726: Read from streams:
    static void            RegisterFlexStreamCallBack(verific_stream *(*stream_func)(const char *file_name)) ;
    static verific_stream *GetUserFlexStream(const char *file_name) ;
#endif

public:
    // Persistence (Binary save/restore of VHDL design units to a binary file off-line)

    // Explicitly link a VHDL library to a directory
    static unsigned      AddLibraryPath(const char *lib_name, const char *dir_name) ;
    static char *        GetLibraryPath(const char *lib_name) ;
    static Map *         GetAllLibraryPaths() { return _map_library_paths ; } // Can return NULL!
    static unsigned      RemoveLibraryPath(const char *lib_name) ;
    static void          ResetAllLibraryPaths() ;

    // Set default directory (prefix) for VHDL libraries which do not have an explicit link
    static unsigned      SetDefaultLibraryPath(const char *path) ;
    static char *        GetDefaultLibraryPath() ;
    static void          ClearDefaultLibraryPath() ;

    // Save/Restore a single VHDL design unit.
    static unsigned      Save(const char *lib_name, const char *unit_name) ; // Does not delete the parse tree of the unit
    static unsigned      Restore(const char *lib_name, const char *unit_name, unsigned silent=0) ;
    // Used internally (check if unit is stored off-line under a library path) :
// retired 12/12 : static unsigned      IsVDBValid(const char *lib_name, const char *unit_name) ; // Obsolete 12/1/12 : GetVDBSignature should be used instead
    // Get conformance signature for specified VDB
    static unsigned long GetVDBSignature(const char *lib_name, const char *unit_name) ;

#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // VIPER 2944 : library-wide save/restore files.
    static unsigned      SaveLib(const char *lib_name) ; // Save entire library to a library-wide dump file.
    static unsigned      RestoreLib(const char *lib_name) ; // Restore an entire library from a library-wide dump file
    static long          PosInLibFile(const char *lib_name, const char *unit_name) ; // find position of unit in a lib-wide dump file. Or return 0 if unit is not there.
    static void          ClearLibUnitMap() ; // clear internal tables used to speedup lookups in library-wide save/restore files
    static unsigned      RestoreLibUnitMap(const char *lib_name) ; // Populate the internal tables used to speedup lookups in library-wide save/restore files for this library from the saved side file
#endif

private :
    // The following is used only by AddLibraryPath() and SetDefaultLibraryPath()
    static unsigned      VerifyAndModifyPath(char **path) ;

public:
    /* Protected VHDL
      VHDL LRM 1076_2008 Section 24.1

      WARNING: Use of the following API, SetTickProtectObject, will enable
      Verific parser read in the protected VHDL and generate regular parse
      tree, using the decryption algorithm provided in the Protect argument.
      THE PARSE TREE NODES CARRY THE INFORMATION WHETHER IT CAME FROM PROTECTED RTL.

      User applications should take every possible measures against
      any leakage of the protected HDL code from the parse-tree data structures as a result
      of use of API SetTickProtectObject.

      For example, user applications should not use parse-tree PrettyPrint on protected
      areas of the parse tree data structures.

      Application which export the parse tree in binary (vdb) form MUST encrypt output file,
      by providing an encoding/decoding algorithm to the API SetProtectionObject.
      This will assure that only the same user application can understand and decrypt(restore)
      the encrypted binary dump.

      In either case, Verific is not responsible and cannot be held liable for any protection violations.
     */

    static Protect *     GetTickProtectObject()                 { return _tick_protect ; }
    static void          SetTickProtectObject(Protect *protect) { _tick_protect = protect ; }

    // Set/Get the protection object for user encryption/decryption of text/vdb dump files
    static Protect *     GetProtectionObject()                  { return _protect ; }
    static void          SetProtectionObject(Protect *protect)  { _protect = protect ; }

//#*# VERIFIC_START DOCUMENT_INTERNAL

// ***************** INTERNAL USE ONLY **************************

    // Internal use only : Interaction with Verilog :
    static VhdlPrimaryUnit *  GetVhdlUnitFromVerilog(const char* lib_name, const char* module_name, unsigned from_l_options = 0) ;
    static VeriModule *       GetVerilogModuleFromlib(const char* library_name, const char* module_name, unsigned from_l_options = 0) ; //Search the Verilog module and return.
    static char*              CreateVhdlEntityName(const char *module_name) ;

    // From the lexer (used while building parse-tree)
    // Not for general usage.
    static linefile_type GetLineFile() ;            // Get linefile info for last token parsed.
    static linefile_type GetRuleLineFile() ;        // Get linefile info for last syntax rule parsed

    static void          SetPragma(const char *name, const char *value) ;
    static const char *  GetPragma(const char *name) ;

    static Array *       TakeComments() ;
    static void          SetComments(Array *comments) ;

    // Viper #7160 / 7142 : 2008 LRM 5.2.6 / 5.3.2.4 : Need to know if we are analyzing std.standard
    static unsigned      IsInsideStdPackageAnalysis() ;

    // Viper #6473: prune logic
    static void          SetPruneLogicForIntegerConstraint(unsigned value) { _prune_logic_for_integer_constraint = value ; }
    static unsigned      GetPruneLogicForIntegerConstraint() { return _prune_logic_for_integer_constraint ; }

    // VIPER #6576: Set the parsed expression. Verific Internal routine used from yacc.
    static void          SetParsedExpr(VhdlExpression *expr)  { _parsed_expr = expr ; }

private:
    // Call Flex
    static unsigned      StartFlex(const char *file_name) ;
    static unsigned      StartFlexExpr(const char *expr, const linefile_type line_file=0) ; // VIPER #6576: VHDL expression parsing
    static unsigned      EndFlex() ;

    // Call Yacc
    static void          StartYacc() ;
    static void          EndYacc() ;
    static int           Parse() ;

public:
    // Set mode of analyzer (87, 93, PSL dialect) while parsing.
    static unsigned         IsVhdl87()     { return (_analysis_mode==VHDL_87) ? 1 : 0 ; }  // Test if we are in Vhdl87 mode.
    static unsigned         IsVhdl93()     { return (_analysis_mode==VHDL_93 || _analysis_mode==VHDL_PSL) ? 1 : 0 ; }           // Test if we are in Vhdl93 mode.(or PSL)
    static unsigned         IsVhdlPsl()    { return (_analysis_mode==VHDL_PSL) ? 1 : 0 ; } // Test if we are in PSL mode
    static unsigned         IsVhdl2K()     { return (_analysis_mode==VHDL_2K || _analysis_mode==VHDL_PSL) ? 1 : 0 ; } // Test if we are in Vhdl 2000 mode
    static unsigned         IsVhdl2008()   { return (_analysis_mode==VHDL_2008 || _analysis_mode==VHDL_PSL) ? 1 : 0 ; } // Test if we are in Vhdl 2000 mode
    static void             SetVhdl87()    { _analysis_mode = VHDL_87 ; }
    static void             SetVhdl93()    { _analysis_mode = VHDL_93 ; }
    static void             SetVhdl2K()    { _analysis_mode = VHDL_2K ; }
    static void             SetVhdl2008()  { _analysis_mode = VHDL_2008 ; }
    static void             SetVhdlPsl()   { _analysis_mode = VHDL_PSL ; }

    // Get/Set analysis mode (VIPER #6937)
    static unsigned         GetAnalysisMode() { return _analysis_mode ; }
    static void             SetAnalysisMode(unsigned d) { _analysis_mode = d ; }

private:
    // Functions, registered by the user :
    static unsigned    (*_interrupt_func)(linefile_type) ;
    static unsigned    (*_compile_as_blackbox_func)(const char *) ;

    static unsigned     _analysis_mode ;       // Flag to indicate if we are parsing '87 or '93 or 2000, or PSL dialect.
    static unsigned     _prune_logic_for_integer_constraint ; // Viper #6473

    static VhdlExpression *_parsed_expr ;   // VIPER #6576: Stores the parsed expression for VHDL expression parser

    // Library search path
    static char         *_default_library_path ;
    static Map          *_map_library_paths ; // char* library name -> char* directory name
#ifdef VHDL_USE_LIB_WIDE_SAVERESTORE
    // VIPER 2944 : library-wide save/restore files
    static Map          *_lib_unit_map ; // Map of lib_name->(Map of unitnames->pos_in_libfile) to quickly check if a unit is in a lib-wide map file.
#endif

    static Map          *_all_libraries ; // Map of name->VhdlLibrary of all libraries of analyzed primary units
    static VhdlLibrary  *_work_lib ;      // Last set work library

    static int           _scale_factor_of_timeunit ; // 1/10/100 scaling factor of time resolution
    static char         *_time_unit ;                // Time unit specification in char * form. Can be "fs"/"ms"/"s" etc.
    static Set          *_ordered_libs ;   // Set of library names in char* form  (VIPER #2762)
//    static unsigned      _loop_limit ;    // VIPER : 2947/4152 - Runtime control of loop limit
//    static unsigned      _max_stack_depth ;   // VIPER #7125: Runtime control of stack depth limit
//    static unsigned      _max_array_size ; // Viper #4950 : dynamic control for error message : array size is larger than 2**_max_array_size (VHDL-1525)
    static Map          *_library_aliases ; // char *alias name -> char *target library name

//    static unsigned  _extract_mp_rams ; // VIPER 6212 ; run-time switch to enable muulti-port RAM extraction
//    static unsigned  _extract_rams ; // VIPER 6212 ; run-time switch to enable dual-port RAM extraction

//    static unsigned      _minimum_ram_size ; // Viper #3903
    static unsigned      _ignore_dead_assignment ; // Viper #5700

    static Protect      *_tick_protect ; // The object for the class that defines encryption/decryption algorithms under `protect envelope in the RTL LRM 1076_2008 Section 24.1
    static Protect      *_protect ; // The object for the class that defines encryption/decryption algorithms

#ifdef VHDL_FLEX_READ_FROM_STREAM
    static verific_stream *(*_stream_callback_func)(const char *file_name) ; // VIPER #5726: Read from streams
#endif
public :
    static VhdlPrimaryUnit *_top_unit ; // VIPER #6793 : Store top level unit
public:
    static const char*   _top_unit_name ; // Help for external name resolving. Top unit name if supplied. RD: Should eliminate this static var.

    //CARBON_BEGIN
    static Array        *_pragma_triggers ; // List of custom pragma trigger names (char*)
    //CARBON_END
//#*# VERIFIC_END DOCUMENT_INTERNAL

} ; // class vhdl_file

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_FILE_H_

