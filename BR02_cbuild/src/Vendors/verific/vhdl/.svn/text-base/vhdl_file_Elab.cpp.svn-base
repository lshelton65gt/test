/*
 *
 * [ File Version : 1.70 - 2014/01/20 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#include "vhdl_file.h" // Compile flags are defined within here

#include "Message.h"
#include "Array.h"
#include "Map.h"

#include "VhdlUnits.h"
#include "VhdlIdDef.h"

#ifdef VERIFIC_NAMESPACE
using namespace Verific ;
#endif

unsigned
vhdl_file::Elaborate(const char *unit_name, const char *lib_name, const char *architecture_name, const Map *generics, unsigned static_only)
{
    // If lib_name is not set, store in library "work"
    if (!lib_name) lib_name = "work" ;

    // Clear some static stuff :
    Message::ClearErrorCount() ;
    VhdlNode::ClearNamedPath() ;
    VhdlNode::ClearStructuralStack() ;

    // Set style of elaboration : static or 'RTL' :
    if (static_only) {
        VhdlNode::SetStaticElab() ;
    } else {
        VhdlNode::SetRtlElab() ;
    }

    // Find/Set the work library where design units will be compiled into.
    _work_lib = GetLibrary(lib_name,1) ;

    // Get the unit :
    VhdlPrimaryUnit *unit = _work_lib->GetPrimUnit(unit_name) ;

    if (!unit) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("unit ",unit_name, " is not yet analyzed") ;
        VhdlTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Comment("unit %s is not yet analyzed", unit_name) ;

        return 0 ;
    }
    if (!unit->Id()->IsEntity() && !unit->Id()->IsConfiguration()) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("unit ",unit_name," is not an entity or configuration") ;
        VhdlTreeNode tmp_print_node ;
        tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

        tmp_print_node.Comment("unit %s is not an entity or configuration", unit_name) ;

        return 0 ;
    }

    // Create actual generics if needed
    Array *actual_generics = (generics) ? unit->CreateActualGenerics(generics) : 0 ;

    // Elaborate the unit
    //VhdlDesignUnit *ps_unit = unit->GetSecondaryUnit(architecture_name) ;
    //if (!ps_unit) ps_unit = unit ;

    vhdl_file::_top_unit_name = unit_name ;

    if (static_only) {
        VhdlNode::SetConstNets() ;
        if (unit->HasUnconstrainedUninitializedPorts()) { // Changed for Viper # 3543
        // if (unit->HasUnconstrainedPorts()) {
            // VIPER #3129 : Issue error if top level unit contains unconstrained ports
            unit->Error("design unit %s contains unconstrained port(s)", unit->Name()) ;
        } else {
            (void) unit->StaticElaborate(architecture_name, actual_generics,0,0,0) ;
            // Static elaboration can leave behind some constant nets in terms of value (VhdlValue)
            // stored in VhdlIdDef. So do not reset(delete) constant nets after elaboration
            // It may lead to memory corruption. This will definitely leave insignificant
            // amount of memory hanging. This scenario may not occur i.e. static elaboration
            // will not leave behind constant nets, but still do not delete nets for double
            // safety.
            //VhdlNode::ResetConstNets() ;
            // Delete binding information from elaborated configurations
            _work_lib->DeleteElaboratedBindings() ;
        }
    } else {
    }

    vhdl_file::_top_unit_name = 0 ;

    // Cleanup actual_generics :
    VhdlTreeNode *node ;
    unsigned i ;
    FOREACH_ARRAY_ITEM(actual_generics, i, node) delete node ;
    delete actual_generics ;
    VhdlTreeNode::CleanConfigNumMap() ;
    VhdlTreeNode::CleanUnitNumMap() ; // VIPER #7054

    return (Message::ErrorCount()) ? 0 : 1 ;
}

unsigned
vhdl_file::ElaborateAll(const char *lib_name, unsigned static_only)
{
    // If lib_name is not set, store in library named "work"
    if (!lib_name) lib_name = "work" ;

    // Clear some static stuff, just in case it's left-over :
    Message::ClearErrorCount() ;
    VhdlNode::ClearNamedPath() ;
    VhdlNode::ClearStructuralStack() ;

    // Set style of elaboration : static or 'RTL' :
    if (static_only) {
        VhdlNode::SetStaticElab() ;
    } else {
        VhdlNode::SetRtlElab() ;
    }

    // Find/Set the work library where design units will be compiled into.
    _work_lib = GetLibrary(lib_name,1) ;

    if (static_only) {
        VhdlNode::SetConstNets() ;
        // When user do not specify any top level design unit,
        // last analyzed design unit will be considered as top
        // level unit. That top level design unit will be
        // statically elaborated.
        VhdlPrimaryUnit *unit = _work_lib ? _work_lib->GetPrimUnit(TopUnit(lib_name)) : 0 ;
        // Only entity and configuration can be considered for elaboration
        if (unit && !unit->IsVerilogModule() && unit->Id() && (unit->Id()->IsEntity() || unit->Id()->IsConfiguration())) {
            if (unit->HasUnconstrainedUninitializedPorts()) { // Changed for Viper # 3543
            // if (unit->HasUnconstrainedPorts()) {
                // VIPER #3129 : Issue error if top level unit contains unconstrained ports
                unit->Error("design unit %s contains unconstrained port(s)", unit->Name()) ;
            } else if (!unit->HasUninitializedGenerics()) {
                (void) unit->StaticElaborate(0/*architecture*/,0/*generics*/,0,0,0) ;
                // Static elaboration can leave behind some constant nets in terms of value (VhdlValue)
                // stored in VhdlIdDef. So do not reset(delete) constant nets after elaboration
                // It may lead to memory corruption. This will definitely leave insignificant
                // amount of memory hanging. This scenario may not occur i.e. static elaboration
                // will not leave behind constant nets, but still do not delete nets for double
                // safety.
                //VhdlNode::ResetConstNets() ;
                // Delete binding information from elaborated configurations
                if (_work_lib) _work_lib->DeleteElaboratedBindings() ;
            }
        }
        return (Message::ErrorCount()) ? 0 : 1 ;
    }

    VhdlTreeNode::CleanUnitNumMap() ; // VIPER #7054

    return (Message::ErrorCount()) ? 0 : 1 ;
}

unsigned
vhdl_file::Read(const char *file_name, const char *lib_name, unsigned vhdl_mode, unsigned static_only)
{
    // Read the named file and return apointer to the first (executed) unit in there

    // If lib_name is not set, store in library "work"
    if (!lib_name) lib_name = "work" ;

    // Set Vhdl 93 or Vhdl 87 mode, for the analyzer
    if (vhdl_mode==vhdl_file::VHDL_87) {
        vhdl_file::SetVhdl87() ;
    } else if (vhdl_mode==vhdl_file::VHDL_2K) {
        vhdl_file::SetVhdl2K() ;
    } else if (vhdl_mode==vhdl_file::VHDL_2008) {
        // VIPER #8374: Add VHDL-2008 in vhdl_file::Read() too:
        vhdl_file::SetVhdl2008() ;
    } else {
        vhdl_file::SetVhdl93() ;
    }

    // Set style of elaboration : static or 'RTL' :
    if (static_only) {
        VhdlNode::SetStaticElab() ;
    } else {
        VhdlNode::SetRtlElab() ;
    }

    // Clear some static stuff, just in case it's left-over :
    Message::ClearErrorCount() ;
    VhdlNode::ClearNamedPath() ;
    VhdlNode::ClearStructuralStack() ;

    // Find/Set the work library where design units will be compiled into.
    _work_lib = GetLibrary(lib_name,1) ;

    // Start the analyzer :
    if (!StartFlex(file_name)) return 0 ;

    // Viper: 3142 - all verific msg should have msg-id
    // Message::PrintLine("Reading VHDL file ",file_name) ;
    VhdlTreeNode tmp_print_node ;
    tmp_print_node.SetLinefile(0) ; // to avoid stickey linefile setting in constructor

    tmp_print_node.Comment("Reading VHDL file %s", file_name) ;

    StartYacc() ;
    (void) Parse() ;
    EndYacc() ;
    (void) EndFlex() ;

    if (Message::ErrorCount()) {
        // Viper: 3142 - all verific msg should have msg-id
        // Message::PrintLine("VHDL file ", file_name, " ignored due to errors") ;
        tmp_print_node.Comment("VHDL file %s ignored due to errors", file_name) ;

        // Units with errors will have been deleted already
        return 0 ; // Error occurred during parsing
    }

    // Now call ElaborateAll
    return vhdl_file::ElaborateAll(lib_name, static_only) ;
}
