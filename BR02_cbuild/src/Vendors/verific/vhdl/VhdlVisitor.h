/*
 *
 * [ File Version : 1.46 - 2014/03/07 ]
 *
 * (c) Copyright 1999 - 2014 Verific Design Automation Inc.
 * All rights reserved.
 *
 * This source code belongs to Verific Design Automation Inc.
 * It is considered trade secret and confidential, and is not to be used
 * by parties who have not received written authorization
 * from Verific Design Automation Inc.
 *
 * Only authorized users are allowed to use, copy and modify
 * this software provided that the above copyright notice
 * remains in all copies of this software.
 *
 *
*/

#ifndef _VERIFIC_VHDL_VISITOR_H_
#define _VERIFIC_VHDL_VISITOR_H_

#include "VerificSystem.h"
#include "VhdlCompileFlags.h"

#ifdef VERIFIC_NAMESPACE
namespace Verific { // start definitions in verific namespace
#endif

// VIPER #4959: Define visitor class with different method names for
// different classes to improve functionality of the visitor pattern.
//
// The macro 'VHDL_VISIT' defines the header of the visitor routine:
//     T is the type, ie, the class for which it is being defined
//     N is the name of the object of type T inside the visitor routine
// The macro 'VHDL_VISIT_NODE' defines the call to the visitor routine:
//    T is the type, ie, the class for which it is being called
//    N is the name of the object of type T with which it is called
// The macro 'VHDL_ACCEPT' defines the body of the Accept routine of a class:
//    T is the type, ie, the class to which this Accept routine belongs
//    V is the name of the object of Visitor with which it is called
#ifdef VHDL_SPECIALIZED_VISITORS
    #define VHDL_VISIT(T,N)       Visit##T(T &N)
    #define VHDL_VISIT_NODE(T,N)  Visit##T(N)
#else
    #define VHDL_VISIT(T,N)       Visit(T &N)
    #define VHDL_VISIT_NODE(T,N)  Visit((T &)N)
#endif
#define VHDL_ACCEPT(T,V)          (V).VHDL_VISIT_NODE(T,(*this))

// The following are forward declarations of all parse-tree classes that
// the visitor method of VhdlVisitor class accepts:

// Forward declarations from VhdlTreeNode.h -------------
class VhdlTreeNode ;                     class VhdlCommentNode ;
// Forward declarations from VhdlUnits.h -------------
class VhdlDesignUnit ;                   class VhdlPrimaryUnit ;              class VhdlEntityDecl ;
class VhdlConfigurationDecl ;            class VhdlPackageDecl ;              class VhdlSecondaryUnit ;
class VhdlArchitectureBody ;             class VhdlPackageBody ;              class VhdlContextDecl ;
class VhdlPackageInstantiationDecl ;
// Forward declarations from VhdlDeclaration.h -------------
class VhdlDeclaration ;                  class VhdlTypeDef ;                  class VhdlScalarTypeDef ;
class VhdlArrayTypeDef ;                 class VhdlRecordTypeDef ;            class VhdlAccessTypeDef ;
class VhdlFileTypeDef ;                  class VhdlEnumerationTypeDef ;       class VhdlPhysicalTypeDef ;
class VhdlElementDecl ;                  class VhdlPhysicalUnitDecl ;         class VhdlUseClause ;
class VhdlLibraryClause ;                class VhdlInterfaceDecl ;            class VhdlSubprogramDecl ;
class VhdlSubprogramBody ;               class VhdlSubtypeDecl ;              class VhdlFullTypeDecl ;
class VhdlIncompleteTypeDecl ;           class VhdlConstantDecl ;             class VhdlSignalDecl ;
class VhdlVariableDecl ;                 class VhdlFileDecl ;                 class VhdlAliasDecl ;
class VhdlComponentDecl ;                class VhdlAttributeDecl ;            class VhdlAttributeSpec ;
class VhdlConfigurationSpec ;            class VhdlDisconnectionSpec ;        class VhdlGroupTemplateDecl ;
class VhdlGroupDecl ;                    class VhdlProtectedTypeDef ;         class VhdlProtectedTypeDefBody ;
class VhdlContextReference ;             class VhdlSubprogInstantiationDecl ; class VhdlInterfaceTypeDecl ;
class VhdlInterfaceSubprogDecl ;         class VhdlInterfacePackageDecl ;
// Forward declarations from VhdlExpression.h -------------
class VhdlDiscreteRange ;                class VhdlExpression ;               class VhdlSubtypeIndication ;
class VhdlExplicitSubtypeIndication ;    class VhdlRange ;                    class VhdlBox ;
class VhdlAssocElement ;                 class VhdlOperator ;                 class VhdlAllocator ;
class VhdlAggregate ;                    class VhdlQualifiedExpression ;      class VhdlElementAssoc ;
class VhdlWaveformElement ;              class VhdlDefault ;                  class VhdlRecResFunctionElement ;
class VhdlInertialElement ;
// Forward declarations from VhdlIdDef.h -------------
class VhdlIdDef ;                        class VhdlLibraryId ;                class VhdlGroupId ;
class VhdlGroupTemplateId ;              class VhdlAttributeId ;              class VhdlComponentId ;
class VhdlAliasId ;                      class VhdlFileId ;                   class VhdlVariableId ;
class VhdlSignalId ;                     class VhdlConstantId ;               class VhdlTypeId ;
class VhdlUniversalInteger ;             class VhdlUniversalReal ;            class VhdlAnonymousType ;
class VhdlSubtypeId ;                    class VhdlSubprogramId ;             class VhdlOperatorId ;
class VhdlInterfaceId ;                  class VhdlEnumerationId ;            class VhdlElementId ;
class VhdlPhysicalUnitId ;               class VhdlEntityId ;                 class VhdlArchitectureId ;
class VhdlConfigurationId ;              class VhdlPackageId ;                class VhdlPackageBodyId ;
class VhdlLabelId ;                      class VhdlBlockId ;                  class VhdlProtectedTypeId ;
class VhdlProtectedTypeBodyId ;          class VhdlContextId ;                class VhdlPackageInstElement ;
class VhdlGenericTypeId ;
// Forward declarations from VhdlMisc.h -------------
class VhdlSignature ;                    class VhdlFileOpenInfo ;             class VhdlBindingIndication ;
class VhdlIterScheme ;                   class VhdlWhileScheme ;              class VhdlForScheme ;
class VhdlIfScheme ;                     class VhdlDelayMechanism ;           class VhdlTransport ;
class VhdlInertialDelay ;                class VhdlOptions ;                  class VhdlConditionalWaveform ;
class VhdlSelectedWaveform ;             class VhdlBlockGenerics ;            class VhdlBlockPorts ;
class VhdlCaseStatementAlternative ;     class VhdlElsif ;                    class VhdlEntityClassEntry ;
class VhdlSelectedExpression ;           class VhdlConditionalExpression ;    class VhdlElsifElseScheme ;
class VhdlCaseItemScheme ;
// Forward declarations from VhdlName.h -------------
class VhdlName ;                         class VhdlDesignator ;               class VhdlLiteral ;
class VhdlAll ;                          class VhdlOthers ;                   class VhdlUnaffected ;
class VhdlInteger ;                      class VhdlReal ;                     class VhdlStringLiteral ;
class VhdlBitStringLiteral ;             class VhdlCharacterLiteral ;         class VhdlPhysicalLiteral ;
class VhdlNull ;                         class VhdlIdRef ;                    class VhdlSelectedName ;
class VhdlIndexedName ;                  class VhdlSignaturedName ;           class VhdlAttributeName ;
class VhdlOpen ;                         class VhdlEntityAspect ;             class VhdlInstantiatedUnit ;
class VhdlArrayResFunction ;             class VhdlRecordResFunction ;        class VhdlExternalName ;
// Forward declarations from VhdlConfiguration.h -------------
class VhdlConfigurationItem ;            class VhdlBlockConfiguration ;       class VhdlComponentConfiguration ;
// Forward declarations from VhdlSpecification.h -------------
class VhdlSpecification ;                class VhdlProcedureSpec ;            class VhdlFunctionSpec ;
class VhdlComponentSpec ;                class VhdlGuardedSignalSpec ;        class VhdlEntitySpec ;
// Forward declarations from VhdlStatement.h -------------
class VhdlStatement ;                    class VhdlNullStatement ;            class VhdlReturnStatement ;
class VhdlExitStatement ;                class VhdlNextStatement ;            class VhdlLoopStatement ;
class VhdlCaseStatement ;                class VhdlIfStatement ;              class VhdlVariableAssignmentStatement ;
class VhdlSignalAssignmentStatement ;    class VhdlForceAssignmentStatement ; class VhdlReleaseAssignmentStatement ;
class VhdlWaitStatement ;                class VhdlReportStatement ;
class VhdlAssertionStatement ;           class VhdlProcessStatement ;         class VhdlBlockStatement ;
class VhdlGenerateStatement ;            class VhdlProcedureCallStatement ;   class VhdlSelectedSignalAssignment ;
class VhdlConditionalSignalAssignment ;  class VhdlComponentInstantiationStatement ; class VhdlConditionalVariableAssignmentStatement ;
class VhdlSelectedVariableAssignmentStatement ; class VhdlIfElsifGenerateStatement ; class VhdlCaseGenerateStatement ;
class VhdlConditionalForceAssignmentStatement ; class VhdlSelectedForceAssignment ;

class Map ;
class Array ;

/* -------------------------------------------------------------- */

/*
    A Visitor class lets you define a new operation on a tree (or data structure)
    without changing the classes of the elements on which it operates (tree nodes).
    In essence, it lets you keep related operations together by defining them in
    one class. To get this to work, the following must occur:

    1) Declare a abstract base Visitor class which contains a "void VHDL_VISIT(<type>&)"
       method for every <type> of element (VhdlNode) the Visitor will traverse.
       This has already been done, and is located here in this file (VhdlVisitor.h).

    2) Declare a "void Accept(VhdlVisitor &)" method for every element (VhdlTreeNode) class.

    3) To create your particular Visitor class, derive from VhdlVisitor, and
       implement only the VHDL_VISIT() methods needed for your particular algorithm.

    4) Please note that starting from April 2009 release VHDL_VISIT is a macro defined above.
       Depending on the compile switch VHDL_SPECIALIZED_VISITORS the definition of
       the macro VHDL_VISIT changes. Please look into VhdlCompileFlags.h for more details.
*/
class VFC_DLL_PORT VhdlVisitor
{
protected:
    VhdlVisitor() { } ;

public:
    virtual ~VhdlVisitor() { } ;

private:
    // Prevent compiler from defining the following
    VhdlVisitor(const VhdlVisitor &) ;            // Purposely leave unimplemented
    VhdlVisitor& operator=(const VhdlVisitor &) ; // Purposely leave unimplemented

public:
    // MM macro for operator new/delete overriding (defined in VerificSystem.h)
    INSERT_MM_HOOKS

    // The following class definitions can be found in VhdlTreeNode.h
    virtual void VHDL_VISIT(VhdlTreeNode, node) ;
    virtual void VHDL_VISIT(VhdlCommentNode, node) ;

    // The following class definitions can be found in VhdlUnits.h
    virtual void VHDL_VISIT(VhdlDesignUnit, node) ;
    virtual void VHDL_VISIT(VhdlPrimaryUnit, node) ;
    virtual void VHDL_VISIT(VhdlSecondaryUnit, node) ;
    virtual void VHDL_VISIT(VhdlEntityDecl, node) ;
    virtual void VHDL_VISIT(VhdlConfigurationDecl, node) ;
    virtual void VHDL_VISIT(VhdlPackageDecl, node) ;
    virtual void VHDL_VISIT(VhdlContextDecl, node) ;
    virtual void VHDL_VISIT(VhdlPackageInstantiationDecl, node) ;
    virtual void VHDL_VISIT(VhdlArchitectureBody, node) ;
    virtual void VHDL_VISIT(VhdlPackageBody, node) ;

    // The following class definitions can be found in VhdlConfiguration.h
    virtual void VHDL_VISIT(VhdlConfigurationItem, node) ;
    virtual void VHDL_VISIT(VhdlBlockConfiguration, node) ;
    virtual void VHDL_VISIT(VhdlComponentConfiguration, node) ;

    // The following class definitions can be found in VhdlDeclaration.h
    virtual void VHDL_VISIT(VhdlDeclaration, node) ;
    virtual void VHDL_VISIT(VhdlTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlScalarTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlArrayTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlRecordTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeDefBody, node) ;
    virtual void VHDL_VISIT(VhdlAccessTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlFileTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlEnumerationTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalTypeDef, node) ;
    virtual void VHDL_VISIT(VhdlElementDecl, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalUnitDecl, node) ;
    virtual void VHDL_VISIT(VhdlUseClause, node) ;
    virtual void VHDL_VISIT(VhdlContextReference, node) ;
    virtual void VHDL_VISIT(VhdlLibraryClause, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceDecl, node) ;
    virtual void VHDL_VISIT(VhdlSubprogramDecl, node) ;
    virtual void VHDL_VISIT(VhdlSubprogramBody, node) ;
    virtual void VHDL_VISIT(VhdlSubtypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlFullTypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlIncompleteTypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlConstantDecl, node) ;
    virtual void VHDL_VISIT(VhdlSignalDecl, node) ;
    virtual void VHDL_VISIT(VhdlVariableDecl, node) ;
    virtual void VHDL_VISIT(VhdlFileDecl, node) ;
    virtual void VHDL_VISIT(VhdlAliasDecl, node) ;
    virtual void VHDL_VISIT(VhdlComponentDecl, node) ;
    virtual void VHDL_VISIT(VhdlAttributeDecl, node) ;
    virtual void VHDL_VISIT(VhdlAttributeSpec, node) ;
    virtual void VHDL_VISIT(VhdlConfigurationSpec, node) ;
    virtual void VHDL_VISIT(VhdlDisconnectionSpec, node) ;
    virtual void VHDL_VISIT(VhdlGroupTemplateDecl, node) ;
    virtual void VHDL_VISIT(VhdlGroupDecl, node) ;
    virtual void VHDL_VISIT(VhdlSubprogInstantiationDecl, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceTypeDecl, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceSubprogDecl, node) ;
    virtual void VHDL_VISIT(VhdlInterfacePackageDecl, node) ;

    // The following class definitions can be found in VhdlExpression.h
    virtual void VHDL_VISIT(VhdlExpression, node) ;
    virtual void VHDL_VISIT(VhdlDiscreteRange, node) ;
    virtual void VHDL_VISIT(VhdlSubtypeIndication, node) ;
    virtual void VHDL_VISIT(VhdlExplicitSubtypeIndication, node) ;
    virtual void VHDL_VISIT(VhdlRange, node) ;
    virtual void VHDL_VISIT(VhdlBox, node) ;
    virtual void VHDL_VISIT(VhdlAssocElement, node) ;
    virtual void VHDL_VISIT(VhdlInertialElement, node) ;
    virtual void VHDL_VISIT(VhdlOperator, node) ;
    virtual void VHDL_VISIT(VhdlAllocator, node) ;
    virtual void VHDL_VISIT(VhdlAggregate, node) ;
    virtual void VHDL_VISIT(VhdlQualifiedExpression, node) ;
    virtual void VHDL_VISIT(VhdlElementAssoc, node) ;
    virtual void VHDL_VISIT(VhdlRecResFunctionElement, node) ;
    virtual void VHDL_VISIT(VhdlWaveformElement, node) ;
    virtual void VHDL_VISIT(VhdlDefault, node) ;

    // The following class definitions can be found in VhdlIdDef.h
    virtual void VHDL_VISIT(VhdlIdDef, node) ;
    virtual void VHDL_VISIT(VhdlLibraryId, node) ;
    virtual void VHDL_VISIT(VhdlGroupId, node) ;
    virtual void VHDL_VISIT(VhdlGroupTemplateId, node) ;
    virtual void VHDL_VISIT(VhdlAttributeId, node) ;
    virtual void VHDL_VISIT(VhdlComponentId, node) ;
    virtual void VHDL_VISIT(VhdlAliasId, node) ;
    virtual void VHDL_VISIT(VhdlFileId, node) ;
    virtual void VHDL_VISIT(VhdlVariableId, node) ;
    virtual void VHDL_VISIT(VhdlSignalId, node) ;
    virtual void VHDL_VISIT(VhdlConstantId, node) ;
    virtual void VHDL_VISIT(VhdlTypeId, node) ;
    virtual void VHDL_VISIT(VhdlGenericTypeId, node) ;
    virtual void VHDL_VISIT(VhdlUniversalInteger, node) ;
    virtual void VHDL_VISIT(VhdlUniversalReal, node) ;
    virtual void VHDL_VISIT(VhdlAnonymousType, node) ;
    virtual void VHDL_VISIT(VhdlSubtypeId, node) ;
    virtual void VHDL_VISIT(VhdlSubprogramId, node) ;
    virtual void VHDL_VISIT(VhdlOperatorId, node) ;
    virtual void VHDL_VISIT(VhdlInterfaceId, node) ;
    virtual void VHDL_VISIT(VhdlEnumerationId, node) ;
    virtual void VHDL_VISIT(VhdlElementId, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalUnitId, node) ;
    virtual void VHDL_VISIT(VhdlEntityId, node) ;
    virtual void VHDL_VISIT(VhdlArchitectureId, node) ;
    virtual void VHDL_VISIT(VhdlConfigurationId, node) ;
    virtual void VHDL_VISIT(VhdlPackageId, node) ;
    virtual void VHDL_VISIT(VhdlPackageBodyId, node) ;
    virtual void VHDL_VISIT(VhdlContextId, node) ;
    virtual void VHDL_VISIT(VhdlLabelId, node) ;
    virtual void VHDL_VISIT(VhdlBlockId, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeId, node) ;
    virtual void VHDL_VISIT(VhdlProtectedTypeBodyId, node) ;
    virtual void VHDL_VISIT(VhdlPackageInstElement, node) ;

    // The following class definitions can be found in VhdlMisc.h
    virtual void VHDL_VISIT(VhdlSignature, node) ;
    virtual void VHDL_VISIT(VhdlFileOpenInfo, node) ;
    virtual void VHDL_VISIT(VhdlBindingIndication, node) ;
    virtual void VHDL_VISIT(VhdlIterScheme, node) ;
    virtual void VHDL_VISIT(VhdlWhileScheme, node) ;
    virtual void VHDL_VISIT(VhdlForScheme, node) ;
    virtual void VHDL_VISIT(VhdlIfScheme, node) ;
    virtual void VHDL_VISIT(VhdlElsifElseScheme, node) ;
    virtual void VHDL_VISIT(VhdlCaseItemScheme, node) ;
    virtual void VHDL_VISIT(VhdlDelayMechanism, node) ;
    virtual void VHDL_VISIT(VhdlTransport, node) ;
    virtual void VHDL_VISIT(VhdlInertialDelay, node) ;
    virtual void VHDL_VISIT(VhdlOptions, node) ;
    virtual void VHDL_VISIT(VhdlConditionalWaveform, node) ;
    virtual void VHDL_VISIT(VhdlSelectedWaveform, node) ;
    virtual void VHDL_VISIT(VhdlConditionalExpression, node) ;
    virtual void VHDL_VISIT(VhdlSelectedExpression, node) ;
    virtual void VHDL_VISIT(VhdlBlockGenerics, node) ;
    virtual void VHDL_VISIT(VhdlBlockPorts, node) ;
    virtual void VHDL_VISIT(VhdlCaseStatementAlternative, node) ;
    virtual void VHDL_VISIT(VhdlElsif, node) ;
    virtual void VHDL_VISIT(VhdlEntityClassEntry, node) ;

    // The following class definitions can be found in VhdlName.h
    virtual void VHDL_VISIT(VhdlName, node) ;
    virtual void VHDL_VISIT(VhdlDesignator, node) ;
    virtual void VHDL_VISIT(VhdlArrayResFunction, node) ;
    virtual void VHDL_VISIT(VhdlRecordResFunction, node) ;
    virtual void VHDL_VISIT(VhdlLiteral, node) ;
    virtual void VHDL_VISIT(VhdlAll, node) ;
    virtual void VHDL_VISIT(VhdlOthers, node) ;
    virtual void VHDL_VISIT(VhdlUnaffected, node) ;
    virtual void VHDL_VISIT(VhdlInteger, node) ;
    virtual void VHDL_VISIT(VhdlReal, node) ;
    virtual void VHDL_VISIT(VhdlStringLiteral, node) ;
    virtual void VHDL_VISIT(VhdlBitStringLiteral, node) ;
    virtual void VHDL_VISIT(VhdlCharacterLiteral, node) ;
    virtual void VHDL_VISIT(VhdlPhysicalLiteral, node) ;
    virtual void VHDL_VISIT(VhdlNull, node) ;
    virtual void VHDL_VISIT(VhdlIdRef, node) ;
    virtual void VHDL_VISIT(VhdlSelectedName, node) ;
    virtual void VHDL_VISIT(VhdlIndexedName, node) ;
    virtual void VHDL_VISIT(VhdlSignaturedName, node) ;
    virtual void VHDL_VISIT(VhdlAttributeName, node) ;
    virtual void VHDL_VISIT(VhdlOpen, node) ;
    virtual void VHDL_VISIT(VhdlEntityAspect, node) ;
    virtual void VHDL_VISIT(VhdlInstantiatedUnit, node) ;
    virtual void VHDL_VISIT(VhdlExternalName, node) ;

    // The following class definitions can be found in VhdlSpecification.h
    virtual void VHDL_VISIT(VhdlSpecification, node) ;
    virtual void VHDL_VISIT(VhdlProcedureSpec, node) ;
    virtual void VHDL_VISIT(VhdlFunctionSpec, node) ;
    virtual void VHDL_VISIT(VhdlComponentSpec, node) ;
    virtual void VHDL_VISIT(VhdlGuardedSignalSpec, node) ;
    virtual void VHDL_VISIT(VhdlEntitySpec, node) ;

    // The following class definitions can be found in VhdlStatement.h
    virtual void VHDL_VISIT(VhdlStatement, node) ;
    virtual void VHDL_VISIT(VhdlNullStatement, node) ;
    virtual void VHDL_VISIT(VhdlReturnStatement, node) ;
    virtual void VHDL_VISIT(VhdlExitStatement, node) ;
    virtual void VHDL_VISIT(VhdlNextStatement, node) ;
    virtual void VHDL_VISIT(VhdlLoopStatement, node) ;
    virtual void VHDL_VISIT(VhdlCaseStatement, node) ;
    virtual void VHDL_VISIT(VhdlIfStatement, node) ;
    virtual void VHDL_VISIT(VhdlVariableAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlSignalAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlForceAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlReleaseAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlWaitStatement, node) ;
    virtual void VHDL_VISIT(VhdlReportStatement, node) ;
    virtual void VHDL_VISIT(VhdlAssertionStatement, node) ;
    virtual void VHDL_VISIT(VhdlProcessStatement, node) ;
    virtual void VHDL_VISIT(VhdlBlockStatement, node) ;
    virtual void VHDL_VISIT(VhdlGenerateStatement, node) ;
    virtual void VHDL_VISIT(VhdlProcedureCallStatement, node) ;
    virtual void VHDL_VISIT(VhdlSelectedSignalAssignment, node) ;
    virtual void VHDL_VISIT(VhdlConditionalSignalAssignment, node) ;
    virtual void VHDL_VISIT(VhdlConditionalForceAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlSelectedVariableAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlConditionalVariableAssignmentStatement, node) ;
    virtual void VHDL_VISIT(VhdlComponentInstantiationStatement, node) ;
    virtual void VHDL_VISIT(VhdlIfElsifGenerateStatement, node) ;
    virtual void VHDL_VISIT(VhdlCaseGenerateStatement, node) ;
    virtual void VHDL_VISIT(VhdlSelectedForceAssignment, node) ;

protected:
    virtual void TraverseNode(VhdlTreeNode *node) ;
    virtual void TraverseAttributes(const Map *attrs) ;
    virtual void TraverseArray(const Array *array) ;
} ; // class VhdlVisitor

/* -------------------------------------------------------------- */

#ifdef VERIFIC_NAMESPACE
} // end definitions in verific namespace
#endif

#endif // #ifndef _VERIFIC_VHDL_VISITOR_H_

